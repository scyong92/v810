package com.axi.v810.gui.service;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 * @author Erica Wheatcroft
 */
public class TestResultModel extends DefaultSortTableModel
{
  // the maximum number of tests we will store
  private final int _MAX_TESTS = 10000;

  private List<Result> _results = new ArrayList<Result>();
  private String[] _testResultColumnNames =
                                                  { StringLocalizer.keyToString("SERVICE_RESULTS_TABLE_NUMBER_KEY"),
                                                  StringLocalizer.keyToString("SERVICE_RESULTS_TABLE_NAME_KEY"),
                                                  StringLocalizer.keyToString("SERVICE_RESULTS_TABLE_STATUS_KEY"),
                                                  StringLocalizer.keyToString("SERVICE_RESULTS_TABLE_MESSAGE_KEY"),
                                                  StringLocalizer.keyToString("SERVICE_RESULTS_TABLE_ACTIONS_KEY")};

   
  private static int _minimumCDNABenchmarkLoopCount = 5;  
  private static String _benchmarkResultTableStatus = "";
  
  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  TestResultModel()
  {  
    _benchmarkResultTableStatus = _testResultColumnNames[2] + " (AvgTime ms - LoopCount)";
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_CDNA_BENCHMARK))
    {
      if (_benchmarkResultTableStatus.equalsIgnoreCase(_testResultColumnNames[2])==false)
      {
        _testResultColumnNames[2] = _benchmarkResultTableStatus;
      }
    }
    _minimumCDNABenchmarkLoopCount = Config.getInstance().getIntValue(SoftwareConfigEnum.MINIMUM_CDNA_BENCHMARK_LOOP_COUNT);
  }

  /**
   * @author Erica Wheatcroft
   */
  void populateTable(List<Result> results)
  {
    Assert.expect(results != null);
    _results = results;

    fireTableDataChanged();
  }

  /**
   * @author Erica Wheatcroft
   */
  void populateTable(Result result)
  {
    Assert.expect(result != null);

    // lets check to see if we are at our limit.. if so lets remove the first one and add the new result
    // once removed its gone..
    if(_results.size() > _MAX_TESTS)
    {
      Result removedResult = _results.remove(0);
      removedResult = null;
    }
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_CDNA_BENCHMARK))
    {
      //Benchmark Result
      try
      {
        if (result.getStatus() == HardwareTaskStatusEnum.NOT_RUN)
        {
          //Log the fact that the task passed. The _name value is already localized
          HardwareTask.getLoggerForBenchmark().append(result.getResultName() + " - " + StringLocalizer.keyToString("CD_NOT_RUN_KEY") + " ( " + result.getTaskTime() + " ms - " + result.getLoopCount() + ") ");           
        }        
        else if (result.getStatus() == HardwareTaskStatusEnum.PASSED)
        {
          //Log the fact that the task passed. The _name value is already localized
          HardwareTask.getLoggerForBenchmark().append(result.getResultName() + " - " + StringLocalizer.keyToString("CD_PASSED_KEY") + " ( " + result.getTaskTime() + " ms - " + result.getLoopCount() + ") ");
          if(_minimumCDNABenchmarkLoopCount>0)
          {
            //Accept the benchmark data, when data counts > _minimumCDNABenchmarkLoopCount
            if ((result.getLoopCount() % _minimumCDNABenchmarkLoopCount) == 0)
            {
              BenchmarkJsonFileTimingLogger.getInstance().updateBenchmarkTiming(result.getBenchmarkFileEnum(), result.getTaskTime());
            }
          }
        }
        else
        {
          //Log the fact that the task passed. The _name value is already localized
          HardwareTask.getLoggerForBenchmark().append(result.getResultName() + " - " + StringLocalizer.keyToString("CD_FAILED_KEY") + " ( " + result.getTaskTime() + " ms - " + result.getLoopCount() + ") ");
        }
      }
      catch (XrayTesterException ex)
      {
      }
    }           
    _results.add(result);
    fireTableDataChanged();
  }

  /**
   * Called when preparing to start another test execution run.  Clears the model's data in preparation of new data coming in.
   *
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  void clearCompletedTests()
  {
    _results.clear();
  }

  /**
   * Get the current column count -- from AbstractTableModel
   *
   * @return The integer specifying the column count.
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  public int getColumnCount()
  {
    Assert.expect(_testResultColumnNames != null);
    return _testResultColumnNames.length;
  }

  /**
   * Get the current row count -- from AbstractTableModel
   *
   * @return The integer specifying the row count.
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  public int getRowCount()
  {
    if (_results == null)
      return 0;

    return _results.size();
  }

  /**
   * Get the value at row/column -- from AbstractTableModel
   * I use col+1 when getting the column data because the first piece of info in the column
   * list is the reference to the test itself.  See TestableAdapter.extractInformation.
   *
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0, "Row Index is negative!");
    Assert.expect(columnIndex >= 0, "Column Index is negative!");

    Object returnValue = null;
    Result currentResult = _results.get(rowIndex);
    Assert.expect(currentResult != null);

    switch(columnIndex)
    {
      case 0: // test result #
        return rowIndex + 1;
      case 1: // name of test ran
        return StringUtil.format(currentResult.getResultName(), currentResult.getResultName().length());
      case 2: // status of restuls pass, fail, etc
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_CDNA_BENCHMARK))
        {
          //Add in execution time and loop count for each task
          String statusResult = currentResult.getStatus().toString() + " ( " + Long.toString(currentResult.getTaskTime()) + " ms - " + currentResult.getLoopCount() + ") ";
          return StringUtil.format(statusResult, statusResult.length());
        }
        else
        {
          return StringUtil.format(currentResult.getStatus().toString(), currentResult.getStatus().toString().length());

        }
      case 3: // this will return empty string if the result passed if not the expection string
        return StringUtil.format(currentResult.getTaskStatusMessage(), currentResult.getTaskStatusMessage().length());
      case 4: // this will return an action if applicabel
        return StringUtil.format(currentResult.getSuggestedUserActionMessage(), currentResult.getSuggestedUserActionMessage().length());
      default:
        Assert.expect(false," column index not defined");
    }

    Assert.expect(returnValue != null);
    return returnValue;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setValueAt(Object value, int rowIndex, int columnIndex)
  {
    Assert.expect(false, "nothing can be edited");
  }

  /**
   * Nothing is editable for the table
   *
   * @param rowIndex the row whose value to be queried
   * @param columnIndex the column whose value to be queried
   * @return true if the cell is editable
   * @author Erica Wheatcroft
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return false;
  }


  /**
   * Get the current column name -- from AbstractTableModel
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return _testResultColumnNames[columnIndex];
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isSortable(int col)
  {
    // all columns are sortable so always return true
    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void sortColumn(int col, boolean ascending)
  {

  }

  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  HardwareTaskStatusEnum getTestResultStatusAtRow(int rowIndex)
  {
    Assert.expect(rowIndex >= 0, "Row Index is negative!");

    Result currentResult = _results.get(rowIndex);
    Assert.expect(currentResult != null);

    return currentResult.getStatus();
  }

  /**
   * @author Erica Wheatcroft
   */
  Result getResultAtRow(int rowIndex)
  {
    Assert.expect(rowIndex >= 0, "Row Index is negative!");

    Result currentResult = _results.get(rowIndex);
    Assert.expect(currentResult != null);

    return currentResult;
  }
}
