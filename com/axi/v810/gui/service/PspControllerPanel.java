package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.psp.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author lee-herng.cheah
 */
public class PspControllerPanel extends JPanel implements TaskPanelInt, Observer
{
    private SubsystemStatusAndControlTaskPanel _parent = null;
    private MainMenuGui _mainUI = null;
    private JButton _initializeMicroControllerButton = new JButton();
    private JButton _turnAllProjectorOnButton = new JButton();
    private JButton _turnAllProjectorOffButton = new JButton();
    private JButton _getMicroControllerDescriptionButton = new JButton();
    private JButton _initializeProjectorButton = new JButton();
    private JButton _testFringePatternButton = new JButton();
    private JButton _testFringePatternShiftOneButton = new JButton();
    private JButton _testFringePatternShiftTwoButton = new JButton();
    private JButton _testFringePatternShiftThreeButton = new JButton();
    private JButton _testFringePatternShiftFourButton = new JButton();    
    private JButton _testWhiteLightButton = new JButton();
    private JButton _closeTestFringePatternButton = new JButton();

    private ProgressDialog _progressDialog = null;
    private PspMicroController _pspMicroController = null;
    private AbstractProjector _abstractProjector = null;
    
    private JScrollPane _statusPaneScrollPane = new JScrollPane();
    private JTextArea _statusPane = new JTextArea();
    
    private boolean _initializedMicroController = false;
    private boolean _initializedProjector = false;
    private boolean _isTurnAllProjectorOn = false;
    private boolean _isTurnAllProjectorOff = false;
    private boolean _isFringePatternUnderTesting = false;
    private boolean _isFringePatternShiftOneUnderTesting = false;
    private boolean _isFringePatternShiftTwoUnderTesting = false;
    private boolean _isFringePatternShiftThreeUnderTesting = false;
    private boolean _isFringePatternShiftFourUnderTesting = false;
    private boolean _isWhiteLightUnderTesting = false;

    private HardwareWorkerThread _workerThread = HardwareWorkerThread.getInstance();
    private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
    
    FringePatternDialog _fringePatternDialog;

    private ActionListener _initializeMicroControllerButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
          createMicroControllerProgressDialog();
          initializeMicroController();
          _progressDialog.setVisible(true);
          _progressDialog = null;
        }
    };
    
    private ActionListener _turnAllProjectorOnButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
          createMicroControllerProgressDialog();
          turnAllProjectorOn();
          _progressDialog.setVisible(true);
          _progressDialog = null;
        }
    };
    
    private ActionListener _turnAllProjectorOffButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
          createMicroControllerProgressDialog();
          turnAllProjectorOff();
          _progressDialog.setVisible(true);
          _progressDialog = null;
        }
    };
    
    private ActionListener _getMicroControllerDescriptionButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
          getMicroControllerStatus();
        }
    };
    
    private ActionListener _initializeProjectorButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
          createProjectorProgressDialog();
          initializeFringePattern();
          _progressDialog.setVisible(true);
          _progressDialog = null;
        }
    };
    
    private ActionListener _testFringePatternShiftOneButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
          testFringePatternShiftOne();
        }
    };
    
    private ActionListener _testFringePatternShiftTwoButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
          testFringePatternShiftTwo();
        }
    };
    
    private ActionListener _testFringePatternShiftThreeButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
          testFringePatternShiftThree();
        }
    };
    
    private ActionListener _testFringePatternShiftFourButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
          testFringePatternShiftFour();
        }
    };
    
    private ActionListener _closeTestFringePatternButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
          closeTestFringePattern();          
        }
    };
    
    private ActionListener _testWhiteLightButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
          testWhiteLight();
        }
    };

    /**
     * @author Cheah Lee Herng
     */
    private void createMicroControllerProgressDialog()
    {
        _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                             StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                             StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_INITIALIZING_KEY"),
                                             StringLocalizer.keyToString("SERVICE_MICROCONTROLLER_INITIALIZED_KEY"),
                                             StringLocalizer.keyToString("GUI_EXIT_BUTTON_KEY"),
                                             StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                             0,
                                             100,
                                             true);

        _progressDialog.pack();
        _progressDialog.setSize(new Dimension(500, 175));
        SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void createProjectorProgressDialog()
    {
        _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                             StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                             StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_INITIALIZING_KEY"),
                                             StringLocalizer.keyToString("SERVICE_PROJECTOR_INITIALIZED_KEY"),
                                             StringLocalizer.keyToString("GUI_EXIT_BUTTON_KEY"),
                                             StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                             0,
                                             100,
                                             true);

        _progressDialog.pack();
        _progressDialog.setSize(new Dimension(500, 175));
        SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());
    }

    /**
     * @author Cheah Lee Herng
     */
    public PspControllerPanel(SubsystemStatusAndControlTaskPanel parent)
    {
        Assert.expect(parent != null);
        _parent = parent;

        // create the ui components
        createPanel();                
    }

    /**
     * @author Cheah Lee Herng
     */
    public void start()
    {
        AbstractMicroController abstractMicroController = AbstractMicroController.getInstance();
        Assert.expect(abstractMicroController instanceof PspMicroController);
        _pspMicroController = (PspMicroController)abstractMicroController;
        _abstractProjector = AbstractProjector.getInstance();

        addListeners();

        startObserving();
        
        disableButtons(false);
    }

    /**
     * @author Cheah Lee Herng
     */
    public void finish()
    {
        removeListeners();

        stopObserving();
    }

    /**
     * @author Cheah Lee Herng
     */
    public boolean isReadyToFinish()
    {
        return true;
    }

    /**
     * @author Cheah Lee Herng
     */
    private void addListeners()
    {
        _initializeMicroControllerButton.addActionListener(_initializeMicroControllerButtonActionListener);
        _turnAllProjectorOnButton.addActionListener(_turnAllProjectorOnButtonActionListener);
        _turnAllProjectorOffButton.addActionListener(_turnAllProjectorOffButtonActionListener);
        _getMicroControllerDescriptionButton.addActionListener(_getMicroControllerDescriptionButtonActionListener);
        _initializeProjectorButton.addActionListener(_initializeProjectorButtonActionListener);        
        _testFringePatternShiftOneButton.addActionListener(_testFringePatternShiftOneButtonActionListener);
        _testFringePatternShiftTwoButton.addActionListener(_testFringePatternShiftTwoButtonActionListener);
        _testFringePatternShiftThreeButton.addActionListener(_testFringePatternShiftThreeButtonActionListener);
        _testFringePatternShiftFourButton.addActionListener(_testFringePatternShiftFourButtonActionListener);        
        _testWhiteLightButton.addActionListener(_testWhiteLightButtonActionListener);
        _closeTestFringePatternButton.addActionListener(_closeTestFringePatternButtonActionListener);
    }

    /**
     * @author Cheah Lee Herng
     */
    private void removeListeners()
    {
        _initializeMicroControllerButton.removeActionListener(_initializeMicroControllerButtonActionListener);
        _turnAllProjectorOnButton.removeActionListener(_turnAllProjectorOnButtonActionListener);
        _turnAllProjectorOffButton.removeActionListener(_turnAllProjectorOffButtonActionListener);
        _getMicroControllerDescriptionButton.removeActionListener(_getMicroControllerDescriptionButtonActionListener);
        _initializeProjectorButton.removeActionListener(_initializeProjectorButtonActionListener);        
        _testFringePatternShiftOneButton.removeActionListener(_testFringePatternShiftOneButtonActionListener);
        _testFringePatternShiftTwoButton.removeActionListener(_testFringePatternShiftTwoButtonActionListener);
        _testFringePatternShiftThreeButton.removeActionListener(_testFringePatternShiftThreeButtonActionListener);
        _testFringePatternShiftFourButton.removeActionListener(_testFringePatternShiftFourButtonActionListener);        
        _testWhiteLightButton.removeActionListener(_testWhiteLightButtonActionListener);
        _closeTestFringePatternButton.removeActionListener(_closeTestFringePatternButtonActionListener);
    }

    /**
     * @author Cheah Lee Herng
     */
    private void startObserving()
    {
        ProgressObservable.getInstance().addObserver(this);
        HardwareObservable.getInstance().addObserver(this);
        FringePatternObservable.getInstance().addObserver(this);
    }

    /**
    * @author Cheah Lee Herng
    */
    private void stopObserving()
    {
        ProgressObservable.getInstance().deleteObserver(this);
        HardwareObservable.getInstance().deleteObserver(this);
        FringePatternObservable.getInstance().deleteObserver(this);
    }

    /**
     * @author Cheah Lee Herng
     */
    private void createPanel()
    {
        setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));
        setLayout(new BorderLayout());

        JPanel northPanel = new JPanel(new FlowLayout());

        JPanel innerMicroControllerPanel = new JPanel();
        innerMicroControllerPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), "MicroController"),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        innerMicroControllerPanel.setLayout(new FlowLayout());
        _initializeMicroControllerButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INITIALIZE_BUTTON_TEXT_KEY"));
        _turnAllProjectorOnButton.setText(StringLocalizer.keyToString("SERVICE_UI_MICROCONTROLLER_TURN_ALL_PROJECTOR_ON_BUTTON_TEXT_KEY"));
        _turnAllProjectorOffButton.setText(StringLocalizer.keyToString("SERVICE_UI_MICROCONTROLLER_TURN_ALL_PROJECTOR_OFF_BUTTON_TEXT_KEY"));
        _getMicroControllerDescriptionButton.setText(StringLocalizer.keyToString("SERVICE_UI_MICROCONTROLLER_MICROCONTROLLER_DESCRIPTION_BUTTON_TEXT_KEY"));
        innerMicroControllerPanel.add(_initializeMicroControllerButton);
        innerMicroControllerPanel.add(_turnAllProjectorOnButton);
        innerMicroControllerPanel.add(_turnAllProjectorOffButton);
        innerMicroControllerPanel.add(_getMicroControllerDescriptionButton);
        
        JPanel innerDlpProjectorPanel = new JPanel();
        innerDlpProjectorPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), "DLP Projector"),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        innerDlpProjectorPanel.setLayout(new FlowLayout());
        _initializeProjectorButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INITIALIZE_BUTTON_TEXT_KEY"));        
        _testFringePatternShiftOneButton.setText(StringLocalizer.keyToString("SERVICE_UI_DLP_PROJECTOR_TEST_FRINGE_PATTERN_SHIFT_ONE_BUTTON_TEXT_KEY"));
        _testFringePatternShiftTwoButton.setText(StringLocalizer.keyToString("SERVICE_UI_DLP_PROJECTOR_TEST_FRINGE_PATTERN_SHIFT_TWO_BUTTON_TEXT_KEY"));
        _testFringePatternShiftThreeButton.setText(StringLocalizer.keyToString("SERVICE_UI_DLP_PROJECTOR_TEST_FRINGE_PATTERN_SHIFT_THREE_BUTTON_TEXT_KEY"));
        _testFringePatternShiftFourButton.setText(StringLocalizer.keyToString("SERVICE_UI_DLP_PROJECTOR_TEST_FRINGE_PATTERN_SHIFT_FOUR_BUTTON_TEXT_KEY"));        
        _testWhiteLightButton.setText(StringLocalizer.keyToString("SERVICE_UI_DLP_PROJECTOR_TEST_WHITE_LIGHT_BUTTON_TEXT_KEY"));
        _closeTestFringePatternButton.setText(StringLocalizer.keyToString("SERVICE_UI_DLP_PROJECTOR_CLOSE_BUTTON_TEXT_KEY"));
        innerDlpProjectorPanel.add(_initializeProjectorButton);        
        innerDlpProjectorPanel.add(_testFringePatternShiftOneButton);
        innerDlpProjectorPanel.add(_testFringePatternShiftTwoButton);
        innerDlpProjectorPanel.add(_testFringePatternShiftThreeButton);
        innerDlpProjectorPanel.add(_testFringePatternShiftFourButton);        
        innerDlpProjectorPanel.add(_testWhiteLightButton);
        innerDlpProjectorPanel.add(_closeTestFringePatternButton);

        JPanel pspControllerPanel = new JPanel(new VerticalFlowLayout(1, 5));
        pspControllerPanel.add(innerMicroControllerPanel);
        pspControllerPanel.add(innerDlpProjectorPanel);
        northPanel.add(pspControllerPanel);
        
        JPanel statusPanel = new JPanel(new BorderLayout());
        statusPanel.setBorder(BorderFactory.createCompoundBorder(
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
                             StringLocalizer.keyToString("SERIVE_UI_MOTION_CONTROL_MESSAGES_KEY")),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        statusPanel.add(_statusPaneScrollPane, BorderLayout.CENTER);
        _statusPaneScrollPane.setOpaque(false);
        _statusPaneScrollPane.getViewport().add(_statusPane);

        add(northPanel, BorderLayout.NORTH);
        add(statusPanel, BorderLayout.CENTER);
    }

    /**
     * @author Cheah Lee Herng
     */
    private void initializeMicroController()
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              // Make sure PSP Micro-Controller is initialized
              _pspMicroController.isStartupRequired();
                
              _pspMicroController.startup();

              // set the flag
              _initializedMicroController = true;
              _isTurnAllProjectorOn = true;
              
              // set the state of the buttons
              disableButtons(false); 
                
//                long startProcessInMiliseconds = System.currentTimeMillis();
//                
//                // FOR TESTING PURPOSE
//                com.axi.util.image.Image imageToAnalyze = XrayImageIoUtil.loadPngImage("C:\\Users\\Administrator\\Desktop\\optical_camera_magnification_reference_plane_image_1_2_1.png");
//                //com.axi.util.image.Image imageToAnalyze = XrayImageIoUtil.loadPngImage("C:\\Users\\Administrator\\Desktop\\optical_camera_magnification_reference_plane_image_0_1_1_Camera_1.png");
//                
//                // Apply Median Filter
//                int kernelWidth = 25;
//                int kernelHeight = 25;
//                com.axi.util.image.RegionOfInterest regionOfInterestToApplyMedian = com.axi.util.image.RegionOfInterest.createRegionFromImage(imageToAnalyze);
//                regionOfInterestToApplyMedian.setWidthKeepingSameMinX(imageToAnalyze.getWidth() - kernelWidth + 1);
//                regionOfInterestToApplyMedian.setHeightKeepingSameMinY(imageToAnalyze.getHeight() - kernelHeight + 1);
//                com.axi.util.image.Image aggregateMedianImage = com.axi.util.image.Filter.median(imageToAnalyze, regionOfInterestToApplyMedian, kernelWidth, kernelHeight);
//                
//                // Save Median filter image
//                XrayImageIoUtil.savePngImage(aggregateMedianImage, "C:\\medianFilterImage.png");
//                
//                // Apply Sobel Edge Detection
//                com.axi.util.image.RegionOfInterest regionOfInterestToApplySobelEdgeDetection = com.axi.util.image.RegionOfInterest.createRegionFromImage(aggregateMedianImage);
//                com.axi.util.image.Image sobelEdgeDetectionImage = Filter.convolveSobelCompass(aggregateMedianImage, regionOfInterestToApplySobelEdgeDetection);
//                
//                // Save Sobel edge filter image
//                XrayImageIoUtil.savePngImage(sobelEdgeDetectionImage, "C:\\sobelEdgeImage.png");
//                
//                long middleProcessInMiliseconds = System.currentTimeMillis();
//                long totalMiddleProcessTimeInMiliseconds = middleProcessInMiliseconds - startProcessInMiliseconds;
//                System.out.println("LEEHERNG: Total elapsed time for middle processing in miliseconds = " + totalMiddleProcessTimeInMiliseconds);
                
//                // Apply Canny Edge algorithm
//                com.axi.util.image.CannyEdgeDetector detector = new com.axi.util.image.CannyEdgeDetector();
//                detector.setLowThreshold(3.5f);
//                detector.setHighThreshold(4f);
//                detector.setSourceImage(aggregateMedianImage.getBufferedImage());
//                detector.process();
//                java.awt.image.BufferedImage edges = detector.getEdgesImage();
                
//                // Convert source image to grayscale image
//                java.awt.image.ColorConvertOp op = new java.awt.image.ColorConvertOp(java.awt.color.ColorSpace.getInstance(java.awt.color.ColorSpace.CS_GRAY), null);
//                java.awt.image.BufferedImage grayImage = op.filter(edges, null);
                
//                XrayImageIoUtil.savePngImage(edges, "C:\\edgeImage.png");
                
//                // Method 1
//                com.axi.util.image.HoughCircleTransform houghCircleTransform = new com.axi.util.image.HoughCircleTransform(sobelEdgeDetectionImage.getBufferedImage());
//                houghCircleTransform.applyHough();
//                java.awt.image.BufferedImage houghCircleImage = houghCircleTransform.getDest();
//                
//                // Save Hough Circle Image
//                XrayImageIoUtil.savePngImage(houghCircleImage, "C:\\houghCircleImage.png");
//                
//                for(Map.Entry<Point, Integer> entry : houghCircleTransform.getCenterPointToRadiusMap().entrySet())
//                {
//                    Point centerPoint = entry.getKey();
//                    Integer radius = entry.getValue();
//                    
//                    System.out.println("LEEHERNG: Center Point (" + centerPoint.getX() + "," + centerPoint.getY() + ") with radius " + radius);
//                }
                
//                // Method 2
//                com.axi.util.image.HoughCircleTransform houghCircleTransform = new com.axi.util.image.HoughCircleTransform(140);
//                houghCircleTransform.setMinCircleIntensity(20);
//                houghCircleTransform.processImage(sobelEdgeDetectionImage);
//                java.util.List<com.axi.util.image.HoughCircle> houghCircles = houghCircleTransform.getCirclesByRelativeIntensity(0.5);
//                System.out.println("LEEHERNG: Total Circle = " + houghCircles.size());
                
//                // Method 3
//                int width = sobelEdgeDetectionImage.getWidth();
//                int height = sobelEdgeDetectionImage.getHeight();
//                int radius = 124;
//                int[] orig = new int[width * height];
//                int lines = 1;
//                
//                java.awt.image.PixelGrabber grabber = new java.awt.image.PixelGrabber(sobelEdgeDetectionImage.getBufferedImage(), 0, 0, width, height, orig, 0, width);
//                try {
//			grabber.grabPixels();
                
//                  // Method 4
//                  int[] circleRadius = com.axi.util.image.ImageFeatureExtraction.findCircleRadius("C:\\sobelEdgeImage.png", 124, 134);
//                  for(int i = 0; i < circleRadius.length; ++i)
//                  {
//                      System.out.println("LEEHERNG: Radius detected = " + circleRadius[i]);
//                  }
                
//		}
//		catch(InterruptedException e2) {
//			System.out.println("error: " + e2);
//		}
//                com.axi.util.image.circleHough circleHoughObject = new com.axi.util.image.circleHough();
//                circleHoughObject.init(orig,width,height, radius);
//                circleHoughObject.setLines(lines);
//                orig = circleHoughObject.process();
//                java.awt.Image overlayImage = createImage(new java.awt.image.MemoryImageSource(width, height, circleHoughObject.overlayImage(sobelEdgeDetectionImage.getBufferedImage(), orig), 0, width));
//                
//                com.axi.util.image.BufferedImageBuilder bufferedImageBuilder = new com.axi.util.image.BufferedImageBuilder();
//                java.awt.image.BufferedImage bufferedImage = bufferedImageBuilder.bufferImage(overlayImage);
//                XrayImageIoUtil.savePngImage(bufferedImage, "C:\\houghImage.png");
                
//                long endProcessInMiliseconds = System.currentTimeMillis();
//                long totalElapesedTimeInMiliseconds = endProcessInMiliseconds - startProcessInMiliseconds;
//                System.out.println("LEEHERNG: Total elapsed time in miliseconds = " + totalElapesedTimeInMiliseconds);
                
//                AbstractOpticalCamera abstractOpticalCamera = AbstractOpticalCamera.getInstance(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
//                uEyeOpticalCamera uEyeOpticalCamera = (uEyeOpticalCamera)abstractOpticalCamera;
//                
//                // Prepare camere in hardware trigger mode
//                uEyeOpticalCamera.enableHardwareTrigger();
//
//                // Disable optical camera auto parameter
//                uEyeOpticalCamera.enableAutoGain(false);
//                uEyeOpticalCamera.enableAutoSensorGain(false);            
//                uEyeOpticalCamera.enableGainBoost(false);
//
//                int masterGainFactor = uEyeOpticalCamera.getMasterGainFactor();
//                System.out.println("LEEHERNG: masterGainFactor = " + masterGainFactor);
//                
//                uEyeOpticalCamera.acquireOfflineImages("C:\\gain_" + masterGainFactor + ".png");
//                
//                // Get the calibrated optical camera gain factor and set to optical camera
//                uEyeOpticalCamera.setMasterGainFactor(110);
//                
//                int newMasterGainFactor = uEyeOpticalCamera.getMasterGainFactor();
//                System.out.println("LEEHERNG: newMasterGainFactor = " + newMasterGainFactor);
//                
//                uEyeOpticalCamera.acquireOfflineImages("C:\\gain_" + newMasterGainFactor + ".png");
            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  handleException(xte);
                  _progressDialog.setVisible(false);
                  _progressDialog = null;
                }
              });
            }
          }
        });
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void turnAllProjectorOn()
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _pspMicroController.on(); 
                            
              _isTurnAllProjectorOn = true;
              _isTurnAllProjectorOff = false;
              
              // set the state of the buttons
              disableButtons(false);              
            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  handleException(xte);
                  _progressDialog.setVisible(false);
                  _progressDialog = null;
                }
              });
            }
          }
        });
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void turnAllProjectorOff()
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _pspMicroController.off(); 
              
              _isTurnAllProjectorOn = false;
              _isTurnAllProjectorOff = true;
              
              // set the state of the buttons
              disableButtons(false);              
            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  handleException(xte);
                  _progressDialog.setVisible(false);
                  _progressDialog = null;
                }
              });
            }
          }
        });
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void initializeFringePattern()
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _abstractProjector.startup();

              // set the flag
              _initializedProjector = true;
              
              // set the state of the buttons
              disableButtons(false);              
            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  handleException(xte);
                  _progressDialog.setVisible(false);
                  _progressDialog = null;
                }
              });
            }
          }
        });
    }    
    
    /**
     * @author Cheah Lee Herng
     */
    private void testFringePatternShiftOne()
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _abstractProjector.projectFringePatternShiftOne(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
              
              _isFringePatternShiftOneUnderTesting = true;
              
              // set the state of the buttons
              disableButtons(false);              
            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  handleException(xte);
                  _progressDialog.setVisible(false);
                  _progressDialog = null;
                }
              });
            }
          }
        });
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void testFringePatternShiftTwo()
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _abstractProjector.projectFringePatternShiftTwo(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
              
              _isFringePatternShiftTwoUnderTesting = true;
              
              // set the state of the buttons
              disableButtons(false);              
            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  handleException(xte);
                  _progressDialog.setVisible(false);
                  _progressDialog = null;
                }
              });
            }
          }
        });
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void testFringePatternShiftThree()
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _abstractProjector.projectFringePatternShiftThree(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
              
              _isFringePatternShiftThreeUnderTesting = true;
              
              // set the state of the buttons
              disableButtons(false);              
            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  handleException(xte);
                  _progressDialog.setVisible(false);
                  _progressDialog = null;
                }
              });
            }
          }
        });
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void testFringePatternShiftFour()
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _abstractProjector.projectFringePatternShiftFour(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
              
              _isFringePatternShiftFourUnderTesting = true;
              
              // set the state of the buttons
              disableButtons(false);              
            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  handleException(xte);
                  _progressDialog.setVisible(false);
                  _progressDialog = null;
                }
              });
            }
          }
        });
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void closeTestFringePattern()
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _abstractProjector.shutdown();
              
              _isFringePatternUnderTesting = false;
              _isFringePatternShiftOneUnderTesting = false;
              _isFringePatternShiftTwoUnderTesting = false;
              _isFringePatternShiftThreeUnderTesting = false;
              _isFringePatternShiftFourUnderTesting = false;
              _isWhiteLightUnderTesting = false;
              
              // set the state of the buttons
              disableButtons(false);              
            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  handleException(xte);
                  _progressDialog.setVisible(false);
                  _progressDialog = null;
                }
              });
            }
          }
        });
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void testWhiteLight()
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _abstractProjector.projectWhiteLight(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
              
              _isWhiteLightUnderTesting = true;
              
              // set the state of the buttons
              disableButtons(false);              
            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  handleException(xte);
                  _progressDialog.setVisible(false);
                  _progressDialog = null;
                }
              });
            }
          }
        });
    }    

    /**
     * @author Cheah Lee Herng
     */
    private void disableButtons(boolean configuringHardware)
    {
        if (configuringHardware)
        {
            // then disable everything but the cancel button should be enabled
            _initializeMicroControllerButton.setEnabled(false);
            _testFringePatternButton.setEnabled(false);
            _testFringePatternShiftOneButton.setEnabled(false);
            _testFringePatternShiftTwoButton.setEnabled(false);
            _testFringePatternShiftThreeButton.setEnabled(false);
            _testFringePatternShiftFourButton.setEnabled(false);
            _closeTestFringePatternButton.setEnabled(false);
        }
        else
        {
            // Fringe Pattern portion
            if (_initializedProjector == false)
            {                                
                _testFringePatternButton.setEnabled(false);
                _testFringePatternShiftOneButton.setEnabled(false);
                _testFringePatternShiftTwoButton.setEnabled(false);
                _testFringePatternShiftThreeButton.setEnabled(false);
                _testFringePatternShiftFourButton.setEnabled(false);
                _closeTestFringePatternButton.setEnabled(false);
                _testWhiteLightButton.setEnabled(false);                
            }            
            else if (_isFringePatternUnderTesting ||
                    _isWhiteLightUnderTesting ||
                    _isFringePatternShiftOneUnderTesting ||
                    _isFringePatternShiftTwoUnderTesting ||
                    _isFringePatternShiftThreeUnderTesting ||
                    _isFringePatternShiftFourUnderTesting)
            {
                _initializeMicroControllerButton.setEnabled(true);
                _initializeProjectorButton.setEnabled(false);
                _testFringePatternButton.setEnabled(false);
                _testFringePatternShiftOneButton.setEnabled(false);
                _testFringePatternShiftTwoButton.setEnabled(false);
                _testFringePatternShiftThreeButton.setEnabled(false);
                _testFringePatternShiftFourButton.setEnabled(false);
                _closeTestFringePatternButton.setEnabled(true);
                _testWhiteLightButton.setEnabled(false);                
            }            
            else
            {
                _initializeMicroControllerButton.setEnabled(true);
                _initializeProjectorButton.setEnabled(true);
                _testFringePatternButton.setEnabled(true);
                _testFringePatternShiftOneButton.setEnabled(true);
                _testFringePatternShiftTwoButton.setEnabled(true);
                _testFringePatternShiftThreeButton.setEnabled(true);
                _testFringePatternShiftFourButton.setEnabled(true);
                _closeTestFringePatternButton.setEnabled(false);
                _testWhiteLightButton.setEnabled(true);                
            }
            
            // Micro-Controller portion
            if (_initializedMicroController == false)
            {
                _initializeMicroControllerButton.setEnabled(true);
                _turnAllProjectorOnButton.setEnabled(false);
                _turnAllProjectorOffButton.setEnabled(false);
                _getMicroControllerDescriptionButton.setEnabled(false);
            }
            else if (_isTurnAllProjectorOn)
            {
                _initializeMicroControllerButton.setEnabled(true);
                _turnAllProjectorOnButton.setEnabled(false);
                _turnAllProjectorOffButton.setEnabled(true);
                _getMicroControllerDescriptionButton.setEnabled(true);
            }
            else if (_isTurnAllProjectorOff)
            {
                _initializeMicroControllerButton.setEnabled(true);
                _turnAllProjectorOnButton.setEnabled(true);
                _turnAllProjectorOffButton.setEnabled(false);
                _getMicroControllerDescriptionButton.setEnabled(false);
            }
            else
            {
                _initializeMicroControllerButton.setEnabled(true);
                _turnAllProjectorOnButton.setEnabled(true);
                _turnAllProjectorOffButton.setEnabled(false);
                _getMicroControllerDescriptionButton.setEnabled(true);
            }
        }
    }

    /**
     * @author Cheah Lee Herng
     */
    private void handleException(XrayTesterException xte)
    {
        _parent.handleXrayTesterException(xte);
    }

    /**
     * @author Cheah Lee Herng
     */
    public synchronized void update(final Observable observable, final Object arg)
    {
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {              
            if (observable instanceof ProgressObservable)
            {
              Assert.expect(arg instanceof ProgressReport);
              ProgressReport progressReport = (ProgressReport)arg;
              int percentDone = progressReport.getPercentDone();

              // don't indicate 100% until event actually completes
              if (percentDone == 100)
                percentDone = 99;

              if (_progressDialog != null)
                _progressDialog.updateProgressBarPrecent(percentDone);
            }
            else if (observable instanceof HardwareObservable)
            {
              if (arg instanceof HardwareEvent)
              {
                HardwareEvent event = (HardwareEvent)arg;
                HardwareEventEnum eventEnum = event.getEventEnum();
                if (eventEnum instanceof MicroControllerEventEnum)
                {
                  if (event.isStart() == false && eventEnum.equals(MicroControllerEventEnum.ON))
                  {
                     _progressDialog.updateProgressBarPrecent(100);
                  }
                  else if (event.isStart() == false && eventEnum.equals(MicroControllerEventEnum.OFF))
                  {
                     _progressDialog.updateProgressBarPrecent(100);
                  }                  
                }
                else if (eventEnum instanceof ProjectorEventEnum)
                {
                  if (event.isStart() == false && eventEnum.equals(ProjectorEventEnum.INITIALIZE))
                  {
                     _progressDialog.updateProgressBarPrecent(100);
                  }
                }
              }
            }
            else if (observable instanceof FringePatternObservable)
            {
                if (arg instanceof FringePatternInfo)
                {
                    FringePatternInfo fringePatternInfo = (FringePatternInfo)arg;
                    final OpticalCameraIdEnum cameraId = fringePatternInfo.getCameraId();
                    FringePatternEnum fringePatternEnum = fringePatternInfo.getFringePatternEnum();
                
                    if (fringePatternEnum.equals(FringePatternEnum.INITIALIZE))
                    {
                        if (_fringePatternDialog != null)
                        {
                            _fringePatternDialog.dispose();
                            _fringePatternDialog = null;
                        }
                        
                        // Get FringePattern instance
                        _fringePatternDialog = new FringePatternDialog(_mainUI, "Fringe Pattern", cameraId);
                    }
                    else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_WHITE_LIGHT))
                    {
                        if (SwingUtilities.isEventDispatchThread())
                          _fringePatternDialog.displayWhiteLight(cameraId);
                        else
                        {
                          SwingUtils.invokeLater(new Runnable()
                          {
                            public void run()
                            {
                              _fringePatternDialog.displayWhiteLight(cameraId);
                            }
                          });
                        }
                    }
                    else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_NO_LIGHT))
                    {
                        if (SwingUtilities.isEventDispatchThread())
                          _fringePatternDialog.closeFringePattern();
                        else
                        {
                          SwingUtils.invokeLater(new Runnable()
                          {
                            public void run()
                            {
                              _fringePatternDialog.closeFringePattern();
                            }
                          });
                        }
                    }
                    else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_ONE))
                    {
                        if (SwingUtilities.isEventDispatchThread())
                          _fringePatternDialog.displayFringePatternShiftOne(cameraId);
                        else
                        {
                          SwingUtils.invokeLater(new Runnable()
                          {
                            public void run()
                            {
                              _fringePatternDialog.displayFringePatternShiftOne(cameraId);
                            }
                          });
                        }
                    }
                    else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_TWO))
                    {
                        if (SwingUtilities.isEventDispatchThread())
                          _fringePatternDialog.displayFringePatternShiftTwo(cameraId);
                        else
                        {
                          SwingUtils.invokeLater(new Runnable()
                          {
                            public void run()
                            {
                              _fringePatternDialog.displayFringePatternShiftTwo(cameraId);
                            }
                          });
                        }
                    }
                    else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_THREE))
                    {
                        if (SwingUtilities.isEventDispatchThread())
                          _fringePatternDialog.displayFringePatternShiftThree(cameraId);
                        else
                        {
                          SwingUtils.invokeLater(new Runnable()
                          {
                            public void run()
                            {
                              _fringePatternDialog.displayFringePatternShiftThree(cameraId);
                            }
                          });
                        }
                    }
                    else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_FOUR))
                    {
                        if (SwingUtilities.isEventDispatchThread())
                          _fringePatternDialog.displayFringePatternShiftFour(cameraId);
                        else
                        {
                          SwingUtils.invokeLater(new Runnable()
                          {
                            public void run()
                            {
                              _fringePatternDialog.displayFringePatternShiftFour(cameraId);
                            }
                          });
                        }
                    }
                    else if (fringePatternEnum.equals(FringePatternEnum.FINISH_DISPLAY_FRINGE_PATTERN))
                    {
                      try
                      {
                        PspImageAcquisitionEngine.getInstance().handlePostImageDisplay(PspModuleEnum.FRONT);
                      }
                      catch (final XrayTesterException xte)
                      {
                        SwingUtils.invokeLater(new Runnable()
                        {
                          public void run()
                          {
                            handleException(xte);
                            _progressDialog.setVisible(false);
                            _progressDialog = null;
                          }
                        });
                      }
                    }
                    else
                        Assert.expect(false, "invalid display mode");
                }
            }
            else
              Assert.expect(false);
          }
        });
    }
    
    /**
     * Retrieve MicroController information
     * 
     * @author Cheah Lee Herng
     */
    private void getMicroControllerStatus()
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          java.util.List<HardwareConfigurationDescriptionInt> descriptions = new ArrayList<HardwareConfigurationDescriptionInt>();

          clearStatusPane();
          descriptions = _pspMicroController.getConfigurationDescription();
          Assert.expect(descriptions != null);

          displayDescription(descriptions);
        }
      });
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    private void displayDescription(java.util.List<HardwareConfigurationDescriptionInt> descriptions)
    {
      StringBuffer statusMessageBuffer = new StringBuffer();
      for (HardwareConfigurationDescriptionInt configuration : descriptions)
      {
        LocalizedString localizedString = new LocalizedString(configuration.getKey(),
                                                              configuration.getParameters().toArray());
        String message = StringUtil.format(StringLocalizer.keyToString(localizedString), 50);
        statusMessageBuffer.append(message);
        statusMessageBuffer.append("\n");
      }
      _statusPane.setText(statusMessageBuffer.toString());
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void clearStatusPane()
    {
      try
      {
        _statusPane.getDocument().remove(0, _statusPane.getDocument().getLength());
      }
      catch (javax.swing.text.BadLocationException ex)
      {
        // this should not happen
        Assert.expect(false);
      }
    }
}
