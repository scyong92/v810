package com.axi.v810.gui.psp;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalLiveVideoDialog extends EscapeDialog
{    
  private AbstractOpticalCamera _abstractOpticalCamera;
  private PspLiveVideoPanel _pspLiveVideoPanel;               

  private String _actionButtonText;
  private boolean _displayFringePatternPanel;
  private boolean _displayGridPanel;
  private boolean _allowSurfaceMapPointSelection;

  private OpticalLiveVideoInteractionInterface _opticalLiveVideoInstance;

  JButton _performActionButton = new JButton();
  JButton _clearButton = new JButton();
  JButton _cancelButton = new JButton();

  java.awt.Panel _horizontalCross = new java.awt.Panel();
  java.awt.Panel _verticalCross = new java.awt.Panel();

  private int _width;
  private int _height;

  // Special handling of point selection in Optical Measurement Repeatability test
//  private int _pointX = -1;
//  private int _pointY = -1;
  private java.util.List<Point2D> _points = new java.util.ArrayList<Point2D>();  
  
  private final int _MAX_SURFACE_POINTS_ROI_WIDTH_IN_PIXEL = AbstractOpticalCamera.getRoiWidth();
  private final int _MAX_SURFACE_POINTS_ROI_HEIGHT_IN_PIXEL = AbstractOpticalCamera.getRoiHeight();

  // Allow user to select point location
  private Panel _pointSelectionPanel = new java.awt.Panel();
  private Panel _point2SelectionPanel = new java.awt.Panel();
  private Panel _point3SelectionPanel = new java.awt.Panel();
  private Panel _point4SelectionPanel = new java.awt.Panel();
  private Panel _point5SelectionPanel = new java.awt.Panel();
  private Panel _point6SelectionPanel = new java.awt.Panel();
    
  /**
   * @author Cheah Lee Herng 
   */
  public OpticalLiveVideoDialog()
  {
    try
    {
      setupDialogProperty();
      jbInit();
      startOpticalLiveVideo();
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }        
  }

  /**
   * @author Cheah Lee Herng 
   */
  public OpticalLiveVideoDialog(Frame parent, 
                                String title, 
                                OpticalCameraIdEnum cameraId, 
                                String actionButtonText,
                                OpticalLiveVideoInteractionInterface opticalLiveVideoInstance,
                                boolean displayFringePatternPanel,
                                boolean displayGridPanel,
                                boolean allowSurfaceMapPointSelection) throws XrayTesterException
  {
    super(parent, title, true);        
    Assert.expect(cameraId != null);
    Assert.expect(actionButtonText != null);
    Assert.expect(opticalLiveVideoInstance != null);

    _abstractOpticalCamera = OpticalCameraArray.getCamera(cameraId);
    //_abstractOpticalCamera.setUseLargerFov(false);

    _opticalLiveVideoInstance = opticalLiveVideoInstance;        
    _actionButtonText = actionButtonText;
    _displayFringePatternPanel = displayFringePatternPanel;
    _displayGridPanel = displayGridPanel;
    _allowSurfaceMapPointSelection = allowSurfaceMapPointSelection;

    setupDialogProperty();
    jbInit();
    pack();
    setupWindowHandle();        
    addActionListeners();

    // Finally, show the live video
    startOpticalLiveVideo();

    if (_displayGridPanel)
    {
      drawCrossHairOnLiveVideo();
    }

    if (_allowSurfaceMapPointSelection)
    {
      _pspLiveVideoPanel.addMouseListener(new MouseAdapter()
      {
        public void mousePressed(MouseEvent mouseEvent)
        {
          handleMousePressed(mouseEvent);
        }
      });

      // Disable button by default
      _performActionButton.setEnabled(false);
    }
  }
    
  /**
   * @author Cheah Lee Herng
  */
  public void setVisible(boolean visible)
  {
    super.setVisible(visible);
  }
    
  /**
   * @author Cheah Lee Herng
   */
  private void jbInit() throws XrayTesterException
  {
    this.setResizable(false);

    // Initialize point selection panel
    this.add(_pointSelectionPanel, BorderLayout.CENTER);
    this.add(_point2SelectionPanel, BorderLayout.CENTER);
    this.add(_point3SelectionPanel, BorderLayout.CENTER);
    this.add(_point4SelectionPanel, BorderLayout.CENTER);
    this.add(_point5SelectionPanel, BorderLayout.CENTER);
    this.add(_point6SelectionPanel, BorderLayout.CENTER);

    JPanel parentPanel = new JPanel(new BorderLayout());
    JPanel navigationPanel = new JPanel(new BorderLayout());
    JPanel buttonPanel = new JPanel(new FlowLayout());

    if (_displayGridPanel)
    {
      _horizontalCross = new java.awt.Panel();
      _verticalCross = new java.awt.Panel();

      navigationPanel.add(_horizontalCross, BorderLayout.WEST);
      navigationPanel.add(_verticalCross, BorderLayout.WEST);
    }

    _pspLiveVideoPanel = new PspLiveVideoPanel(_abstractOpticalCamera, MathUtilEnum.NANOMETERS);
    _pspLiveVideoPanel.setPreferredSize(new Dimension(_width, _height));
    _pspLiveVideoPanel.setBounds(0, 0, _width, _height);        
    navigationPanel.add(_pspLiveVideoPanel, BorderLayout.WEST);

    _performActionButton.setText(_actionButtonText);
    _performActionButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try
        {
          performActionButton_actionPerformed(e);
        }
        catch (XrayTesterException xte)
        {
          Assert.logException(xte);
        }
      }
    });

    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try
        {
          cancelButton_actionPerformed(e);
        }
        catch (XrayTesterException xte)
        {
          Assert.logException(xte);
        }
      }
    });

    StageNavigationPanel stageNavigationPanel = new StageNavigationPanel();
    navigationPanel.add(stageNavigationPanel, BorderLayout.CENTER);                               

    if (_displayFringePatternPanel)
    {
      FringePatternPanel fringePatternPanel = new FringePatternPanel();
      navigationPanel.add(fringePatternPanel, BorderLayout.EAST);
    }        

    _performActionButton.setSize(50, 100);
    _cancelButton.setSize(50, 100);        
    buttonPanel.add(_performActionButton); 
    buttonPanel.add(_cancelButton);

    parentPanel.add(navigationPanel, BorderLayout.NORTH);
    parentPanel.add(buttonPanel, BorderLayout.SOUTH);

    //this.add(navigationPanel, BorderLayout.EAST);
    this.add(parentPanel, BorderLayout.CENTER);
  }
    
  /**
   * @author Cheah Lee Herng 
   */
  private void setupWindowHandle() throws XrayTesterException
  {
    Assert.expect(_abstractOpticalCamera != null);

    int windowHandle = _abstractOpticalCamera.getWindowHandle(_pspLiveVideoPanel);      
    _pspLiveVideoPanel.setParentWindowHandle(windowHandle);
  }  
    
  /**
   * @author Cheah Lee Herng
   */
  private void addActionListeners()
  {
    addCloseDialogActionListeners();
  }
    
  /**
   * @author Cheah Lee Herng
   */
  private void addCloseDialogActionListeners()
  {
    Assert.expect(_abstractOpticalCamera != null);

    this.addWindowListener(new WindowAdapter()
    {
      public void windowClosing(WindowEvent we)
      {
        _abstractOpticalCamera.setStopLiveVideoBoolean(true);
        _opticalLiveVideoInstance.cancelLiveVideo();
      }
    });
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void unInitialize() throws XrayTesterException
  {
    Assert.expect(_abstractOpticalCamera != null);
    
    try
    {
      _abstractOpticalCamera.setStopLiveVideoBoolean(true);
      exitOpticalCamera();
    }
    finally
    {
      _opticalLiveVideoInstance.setHardwareShutdownComplete();
    }
  }
    
  /**
   * @author Cheah Lee Herng 
   */
  private void performActionButton_actionPerformed(ActionEvent e) throws XrayTesterException
  {
    Assert.expect(_opticalLiveVideoInstance != null);  
    _abstractOpticalCamera.setStopLiveVideoBoolean(true);

    if (_allowSurfaceMapPointSelection)
      _opticalLiveVideoInstance.setSurfaceMapPoint(_points);

    _opticalLiveVideoInstance.runNextOpticalCamera();
  }
    
  /**
   * @author Cheah Lee Herng
   */
  private void cancelButton_actionPerformed(ActionEvent e) throws XrayTesterException
  {
    Assert.expect(_opticalLiveVideoInstance != null);
    
    _abstractOpticalCamera.setStopLiveVideoBoolean(true);
    _opticalLiveVideoInstance.cancelLiveVideo();
  }
    
  /**
   * @author Cheah Lee Herng
   */
  private void startOpticalLiveVideo() throws XrayTesterException
  {
    Assert.expect(_abstractOpticalCamera != null);

    _abstractOpticalCamera.initialize();
    _abstractOpticalCamera.setStopLiveVideoBoolean(false);
    _abstractOpticalCamera.displayLiveVideo();
  }
    
  /**
   * @author Cheah Lee Herng
   */
  private void exitOpticalCamera() throws XrayTesterException
  {
    Assert.expect(_abstractOpticalCamera != null);
    _abstractOpticalCamera.exitCamera();
    this.dispose();
  }
    
  /**
   * @author Jack Hwee
   */
  private void drawCrossHairOnLiveVideo() 
  {
    int centerLocationXInPixel = (int)(Math.ceil(_width / 2));
    int centerLocationYInPixel = (int)(Math.ceil(_height / 2));

    _horizontalCross.setLocation(0, centerLocationYInPixel);
    _horizontalCross.setBackground(Color.YELLOW);              
    _horizontalCross.setSize(new Dimension(_width, 2));
    _horizontalCross.repaint();

    _verticalCross.setLocation(centerLocationXInPixel, 0);
    _verticalCross.setBackground(Color.YELLOW);              
    _verticalCross.setSize(new Dimension(2, _height));
    _verticalCross.repaint();  
  }
    
  /**
   * @author Cheah Lee Herng
   */
  private void setupDialogProperty()
  {     
    _width   = PspHardwareEngine.getInstance().getImageWidth();
    _height  = PspHardwareEngine.getInstance().getImageHeight();
  }
  
  /**   
   * @author Cheah Lee Herng
   */ 
  public void handleMousePressed(MouseEvent mouseEvent)
  {
    int pointX = _pspLiveVideoPanel.getXLocation();
    int pointY = _pspLiveVideoPanel.getYLocation();
    
    // Calculate the dimension
    int halfOfSurfacePointRoiWidth = (_MAX_SURFACE_POINTS_ROI_WIDTH_IN_PIXEL / 2) - 1;
    int halfOfSurfacePointRoiHeight = (_MAX_SURFACE_POINTS_ROI_HEIGHT_IN_PIXEL / 2) - 1;
    
    int totalPoint = _points.size();
    
    // Draw Point #1
    if (totalPoint == 0)
    {            
      _pointSelectionPanel.setLocation(pointX, pointY);
      _pointSelectionPanel.setBackground(getSurfaceMapPointColour(totalPoint));
      _pointSelectionPanel.setSize(new Dimension(_MAX_SURFACE_POINTS_ROI_WIDTH_IN_PIXEL, _MAX_SURFACE_POINTS_ROI_HEIGHT_IN_PIXEL));
      _pointSelectionPanel.setBounds(pointX - halfOfSurfacePointRoiWidth, pointY - halfOfSurfacePointRoiHeight, _MAX_SURFACE_POINTS_ROI_WIDTH_IN_PIXEL, _MAX_SURFACE_POINTS_ROI_HEIGHT_IN_PIXEL);
      _pointSelectionPanel.repaint();
      
      _points.add(new Point(pointX, pointY));
    }
    else if (totalPoint == 1)
    {      
      _point2SelectionPanel.setLocation(pointX, pointY);
      _point2SelectionPanel.setBackground(getSurfaceMapPointColour(totalPoint));
      _point2SelectionPanel.setSize(new Dimension(_MAX_SURFACE_POINTS_ROI_WIDTH_IN_PIXEL, _MAX_SURFACE_POINTS_ROI_HEIGHT_IN_PIXEL));
      _point2SelectionPanel.setBounds(pointX - halfOfSurfacePointRoiWidth, pointY - halfOfSurfacePointRoiHeight, _MAX_SURFACE_POINTS_ROI_WIDTH_IN_PIXEL, _MAX_SURFACE_POINTS_ROI_HEIGHT_IN_PIXEL);
      _point2SelectionPanel.repaint();
      
      _points.add(new Point(pointX, pointY));
    }
    else if (totalPoint == 2)
    {      
      _point3SelectionPanel.setLocation(pointX, pointY);
      _point3SelectionPanel.setBackground(getSurfaceMapPointColour(totalPoint));
      _point3SelectionPanel.setSize(new Dimension(_MAX_SURFACE_POINTS_ROI_WIDTH_IN_PIXEL, _MAX_SURFACE_POINTS_ROI_HEIGHT_IN_PIXEL));
      _point3SelectionPanel.setBounds(pointX - halfOfSurfacePointRoiWidth, pointY - halfOfSurfacePointRoiHeight, _MAX_SURFACE_POINTS_ROI_WIDTH_IN_PIXEL, _MAX_SURFACE_POINTS_ROI_HEIGHT_IN_PIXEL);
      _point3SelectionPanel.repaint();
      
      _points.add(new Point(pointX, pointY));
    }
    else if (totalPoint == 3)
    {      
      _point4SelectionPanel.setLocation(pointX, pointY);
      _point4SelectionPanel.setBackground(getSurfaceMapPointColour(totalPoint));
      _point4SelectionPanel.setSize(new Dimension(_MAX_SURFACE_POINTS_ROI_WIDTH_IN_PIXEL, _MAX_SURFACE_POINTS_ROI_HEIGHT_IN_PIXEL));
      _point4SelectionPanel.setBounds(pointX - halfOfSurfacePointRoiWidth, pointY - halfOfSurfacePointRoiHeight, _MAX_SURFACE_POINTS_ROI_WIDTH_IN_PIXEL, _MAX_SURFACE_POINTS_ROI_HEIGHT_IN_PIXEL);
      _point4SelectionPanel.repaint();
      
      _points.add(new Point(pointX, pointY));
    }
    else if (totalPoint == 4)
    {      
      _point5SelectionPanel.setLocation(pointX, pointY);
      _point5SelectionPanel.setBackground(getSurfaceMapPointColour(totalPoint));
      _point5SelectionPanel.setSize(new Dimension(_MAX_SURFACE_POINTS_ROI_WIDTH_IN_PIXEL, _MAX_SURFACE_POINTS_ROI_HEIGHT_IN_PIXEL));
      _point5SelectionPanel.setBounds(pointX - halfOfSurfacePointRoiWidth, pointY - halfOfSurfacePointRoiHeight, _MAX_SURFACE_POINTS_ROI_WIDTH_IN_PIXEL, _MAX_SURFACE_POINTS_ROI_HEIGHT_IN_PIXEL);
      _point5SelectionPanel.repaint();
      
      _points.add(new Point(pointX, pointY));
    }
    else if (totalPoint == 5)
    {      
      _point6SelectionPanel.setLocation(pointX, pointY);
      _point6SelectionPanel.setBackground(getSurfaceMapPointColour(totalPoint));
      _point6SelectionPanel.setSize(new Dimension(_MAX_SURFACE_POINTS_ROI_WIDTH_IN_PIXEL, _MAX_SURFACE_POINTS_ROI_HEIGHT_IN_PIXEL));
      _point6SelectionPanel.setBounds(pointX - halfOfSurfacePointRoiWidth, pointY - halfOfSurfacePointRoiHeight, _MAX_SURFACE_POINTS_ROI_WIDTH_IN_PIXEL, _MAX_SURFACE_POINTS_ROI_HEIGHT_IN_PIXEL);
      _point6SelectionPanel.repaint();
      
      _points.add(new Point(pointX, pointY));
    }

    // Enable button after point selection
    if (_points.size() < AbstractSurfaceMapSectionPanel.MAXIMUM_NUMBER_OF_SURFACE_MAP_POINTS)    
      _performActionButton.setEnabled(false);
    else
      _performActionButton.setEnabled(true);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private Color getSurfaceMapPointColour(int totalPoint)
  {
    Color color = Color.RED;
    switch (totalPoint % AbstractSurfaceMapSectionPanel.MAXIMUM_NUMBER_OF_SURFACE_MAP_POINTS)
    {
      case 0:
        color = Color.YELLOW;
        break;
      case 1:
        color = Color.YELLOW;
        break;
      case 2:
        color = Color.RED;
        break;
      case 3:
        color = Color.RED;
        break;
      case 4:
        color = Color.YELLOW;
        break;
      case 5:
        color = Color.YELLOW;
        break;
      default:
        color = Color.YELLOW;
        break;
    }
    return color;
  }
}
