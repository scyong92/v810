package com.axi.v810.gui.psp;

import java.awt.*;
import java.awt.image.*;
import java.util.*;
import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 *
 * @author Ying-Huan.Chu
 */
public abstract class AbstractSurfaceMapSectionPanel extends JLayeredPane
{
  protected static final int LIVE_VIEW_INSPECTABLE_OFFSET = PspLiveVideoPanel.LIVE_VIEW_INSPECTABLE_OFFSET;
  protected static final int IMAGE_SECTION_WIDTH = PspHardwareEngine.getInstance().getImageWidth() / 2;
  protected static final int IMAGE_SECTION_HEIGHT = PspHardwareEngine.getInstance().getImageHeight() / 2;
  protected static final int MAXIMUM_NUMBER_OF_SURFACE_MAP_POINTS = 6;
  
  protected SurfaceMapSectionPanelResolutionEnum _surfaceMapImageSectionPanelEnum;
  protected java.util.List<SurfaceMapPointPanel> _surfaceMapPoints;
  protected boolean _isSectionEnabled;
  
  /**
   * Constructor for Small FOV
   * @author Ying-Huan.Chu
   */
  public AbstractSurfaceMapSectionPanel(SurfaceMapSectionPanelResolutionEnum surfaceMapImageSectionPanelEnum)
  {
    _surfaceMapImageSectionPanelEnum = surfaceMapImageSectionPanelEnum;
    _surfaceMapPoints = new ArrayList<SurfaceMapPointPanel>();
    _isSectionEnabled = true;
  }
  
  /**
   * Constructor for Larger FOV
   * @author Ying-Huan.Chu
   */
  public AbstractSurfaceMapSectionPanel(SurfaceMapSectionPanelResolutionEnum surfaceMapImageSectionPanelEnum, boolean isEnabled)
  {
    _surfaceMapImageSectionPanelEnum = surfaceMapImageSectionPanelEnum;
    _surfaceMapPoints = new ArrayList<SurfaceMapPointPanel>();
    _isSectionEnabled = isEnabled;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public SurfaceMapSectionPanelResolutionEnum getSurfaceMapImageSectionPanelEnum()
  {
    return _surfaceMapImageSectionPanelEnum;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setSectionEnabled(boolean isEnabled)
  {
    if (_isSectionEnabled != isEnabled)
    {
      _isSectionEnabled = isEnabled;
      repaint();
    }
  }
   
  /**
   * @author Ying-Huan.Chu
   */
  public boolean isSectionEnabled()
  {
    return _isSectionEnabled;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void addSurfaceMapPoint(Point positionInPixel)
  {
    Assert.expect(positionInPixel != null);
    Assert.expect(_surfaceMapPoints != null);
    
    //Point positionInImageSectionInPixel = translatePointFromImagePixelToImageSectionPixel(positionInPixel); // for Larger FOV
    SurfaceMapPointPanel surfaceMapPoint = new SurfaceMapPointPanel(positionInPixel, getSurfaceMapPointColour());
    surfaceMapPoint.setPositionInImageInPixel(positionInPixel);
    _surfaceMapPoints.add(surfaceMapPoint);
    add(surfaceMapPoint, JLayeredPane.PALETTE_LAYER);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void clearSurfaceMapPoints()
  {
    Assert.expect(_surfaceMapPoints != null);
    
    for (SurfaceMapPointPanel surfaceMapPoint : _surfaceMapPoints)
    {
      remove(surfaceMapPoint);
    }
    _surfaceMapPoints.clear();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getNumberOfSurfaceMapPoints()
  {
    Assert.expect(_surfaceMapPoints != null);
    
    return _surfaceMapPoints.size();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public java.util.List<SurfaceMapPointPanel> getSurfaceMapPoints()
  {
    Assert.expect(_surfaceMapPoints != null);
    
    return _surfaceMapPoints;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getMaximumSurfaceMapPointsPerSection()
  {
    return MAXIMUM_NUMBER_OF_SURFACE_MAP_POINTS;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private Color getSurfaceMapPointColour()
  {
    int numberOfSurfaceMapPoints = getNumberOfSurfaceMapPoints();
    Color color = Color.RED;
    switch (numberOfSurfaceMapPoints % MAXIMUM_NUMBER_OF_SURFACE_MAP_POINTS)
    {
      case 0:
        color = Color.RED;
        break;
      case 1:
        color = Color.BLUE;
        break;
      case 2:
        color = Color.GREEN.darker();
        break;
      case 3:
        color = Color.MAGENTA.darker();
        break;
      case 4:
        color = Color.ORANGE.darker();
        break;
      case 5:
        color = Color.CYAN.darker();
        break;
      default:
        color = Color.RED;
        break;
    }
    return color;
  }
  
  /**
   * This function is only applicable for Larger FOV. Note that Larger FOV is currently disabled.
   * Thins function translates SurfaceMap point from image location to section location.
   * 
   * @author Ying-Huan.Chu
   * @param pointInImageInPixel the point in pixel in image.
   * @return the translated point from position in image to position in section.
   */
  public Point translatePointFromImagePixelToImageSectionPixel(Point pointInImageInPixel)
  {
    Point pointInImageSectionInPixel = new Point(pointInImageInPixel.x, pointInImageInPixel.y);
    if (_surfaceMapImageSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.ALL_MODE_LOW_RES))
    {
      // do nothing
    }
    else if (_surfaceMapImageSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.VIDEO_MODE_HIGH_RES))
    {
      // do nothing
    }
    else if (_surfaceMapImageSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_TOP_LEFT_SECTION))
    {
      // do nothing
    }
    else if (_surfaceMapImageSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_TOP_RIGHT_SECTION))
    {
      int x = pointInImageInPixel.x - IMAGE_SECTION_WIDTH;
      pointInImageSectionInPixel.setLocation(x, pointInImageInPixel.y);
    }
    else if (_surfaceMapImageSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_BOTTOM_LEFT_SECTION))
    {
      int y = pointInImageInPixel.y - IMAGE_SECTION_HEIGHT;
      pointInImageSectionInPixel.setLocation(pointInImageInPixel.x, y);
    }
    else if (_surfaceMapImageSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_BOTTOM_RIGHT_SECTION))
    {
      int x = pointInImageInPixel.x - IMAGE_SECTION_WIDTH;
      int y = pointInImageInPixel.y - IMAGE_SECTION_HEIGHT;
      pointInImageSectionInPixel.setLocation(x, y);
    }
    else
      Assert.expect(false, "Invalid Section Enum.");
    return pointInImageSectionInPixel;
  }
  
  /**
   * @author Ying-Huan.Chu
   * @return true if section contains the coordinate passed in. (with LIVE_VIEW_INSPECTABLE_OFFSET considered)
   */
  public boolean contains(int x, int y)
  {
    Rectangle bound = this.getBounds();
    if ((x < (bound.getMaxX() - LIVE_VIEW_INSPECTABLE_OFFSET)) &&
        (x > (bound.getMinX() + LIVE_VIEW_INSPECTABLE_OFFSET)) &&
        (y < (bound.getMaxY() - LIVE_VIEW_INSPECTABLE_OFFSET)) &&
        (y > (bound.getMinY() + LIVE_VIEW_INSPECTABLE_OFFSET)))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  public abstract SurfaceMapSectionTypeEnum getSurfaceMapSectionTypeEnum();
  public abstract void setContent(BufferedImage bufferedImage);
  protected abstract void paintComponent(Graphics g);
}
