package com.axi.v810.gui.psp;

import com.axi.util.Enum;

/**
 * @author Cheah Lee Herng
 */
public class StageNavigationMoveEnum extends Enum
{
    private static int _index = 0;
    public static StageNavigationMoveEnum COARSE_MOVE = new StageNavigationMoveEnum(++_index);
    public static StageNavigationMoveEnum FINE_MOVE = new StageNavigationMoveEnum(++_index);
    public static StageNavigationMoveEnum STAGE_MOVE = new StageNavigationMoveEnum(++_index);
    
    /**
     * FringePatternEnum
     *
     * @author Cheah Lee Herng
    */
    protected StageNavigationMoveEnum(int id)
    {
        super(id);
    }
}
