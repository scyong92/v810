/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.psp;

import com.axi.guiUtil.*;
import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jack Hwee
 */
public class AddCoordinateDialog extends EscapeDialog
{
  java.util.List<PanelCoordinate> _selectedPoints = null; 
  BoardType _currentBoardType;
  NumericTextField _xCoordinateTextField = new NumericTextField();
  NumericTextField _yCoordinateTextField = new NumericTextField();
  private List<PanelCoordinate> _panelCoordinate = new ArrayList<PanelCoordinate>();  // List of ComponentType objects
  private PanelCoordinate _addedCoordinate = null;
  
    /**
   * @author Jack Hwee
   */
  public AddCoordinateDialog(Frame parent, String title, OpticalRegion opticalRegion, BoardType currentBoardType) throws XrayTesterException
  {
    super(parent, title, true);
    Assert.expect(parent != null);
    Assert.expect(title != null);
    Assert.expect(currentBoardType != null);
    
    _addedCoordinate = null;
    _currentBoardType = currentBoardType;
    
    if (_currentBoardType.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
         _panelCoordinate = _currentBoardType.getPanel().getPanelSurfaceMapSettings().getOpticalRegion().getPointPanelCoordinates();
    }
    else
    {
        Board board = opticalRegion.getBoards().iterator().next();
        _panelCoordinate = board.getBoardSurfaceMapSettings().getOpticalRegion().getPointPanelCoordinates();
    }
    jbInit();
    pack();
    
  }

    /**
   * @author Jack Hwee
   */
  public void setVisible(boolean visible)
  {
    super.setVisible(visible);
  }

  /**
   * @author Jack Hwee
   */
  private void jbInit() throws XrayTesterException
  {     
     // this.setResizable(false);
     // this.setTitle("Add Points");
      this.setLayout(new BorderLayout());
    
      JPanel xCoordinatePanel = new JPanel();
      xCoordinatePanel.setLayout(new BorderLayout());
      xCoordinatePanel.setMinimumSize(new Dimension(405, 27));
      
      JPanel yCoordinatePanel = new JPanel();
      yCoordinatePanel.setLayout(new BorderLayout());
      yCoordinatePanel.setMinimumSize(new Dimension(405, 27));
      
      JPanel addCoordinatePanel = new JPanel();
      addCoordinatePanel.setLayout(new BorderLayout());
      addCoordinatePanel.setMinimumSize(new Dimension(405, 27));

    //  addCoordinateDialog.setPreferredSize(new Dimension(400, 400));
    //  addCoordinateDialog.setBounds(0, 0, 400, 400);
      
      _xCoordinateTextField = new NumericTextField();
      _xCoordinateTextField.setHorizontalAlignment(SwingConstants.LEFT);
      _xCoordinateTextField.setPreferredSize(new Dimension(220, 30));
      xCoordinatePanel.add(_xCoordinateTextField, BorderLayout.EAST);     
    
      JLabel xCoordinateLabel = new JLabel();
      xCoordinateLabel.setText("X Coordinate (Nanometers): ");
      xCoordinatePanel.add(xCoordinateLabel, BorderLayout.WEST);
     
      
      _yCoordinateTextField = new NumericTextField();
      _yCoordinateTextField.setHorizontalAlignment(SwingConstants.LEFT);
      _yCoordinateTextField.setPreferredSize(new Dimension(220, 30));
      yCoordinatePanel.add(_yCoordinateTextField, BorderLayout.EAST);  
      
      JLabel yCoordinateLabel = new JLabel();
      yCoordinateLabel.setText("Y Coordinate (Nanometers): ");
      yCoordinatePanel.add(yCoordinateLabel, BorderLayout.WEST);
      
      JButton addButton = new JButton();
      addButton.setText(StringLocalizer.keyToString("PSP_ADD_POINTS_BUTTON_KEY"));
      addButton.addActionListener(new ActionListener() 
      {

          public void actionPerformed(ActionEvent e) 
          {
              addButton_actionPerformed();
          }
      });
      
      addCoordinatePanel.add(addButton, BorderLayout.CENTER);
      this.add(xCoordinatePanel, BorderLayout.NORTH);
      this.add(yCoordinatePanel, BorderLayout.CENTER);
      this.add(addCoordinatePanel, BorderLayout.SOUTH);
    
  }
  
    /**
     * @author Jack Hwee
     */
    private void addButton_actionPerformed() 
    {
      int x;
      int y;
      
        try 
        {
           if(_xCoordinateTextField.getText().isEmpty() == false)
           {
            if(_yCoordinateTextField.getText().isEmpty() == false)
            {
            x = StringUtil.convertStringToInt(_xCoordinateTextField.getText());
            y = StringUtil.convertStringToInt(_yCoordinateTextField.getText());
            
            PanelCoordinate panelCoordinate = new PanelCoordinate(x, y);

            if (x >= 0 && y >= 0)
            {
               if (_panelCoordinate.contains(panelCoordinate) == false)
               {
               // _currentBoardType.getPanel().getPanelSurfaceMapSettings().getOpticalRegion().addSurfaceMapPoints(x, y, true);
                 PanelCoordinate coor = new PanelCoordinate(x,  y);
                 _addedCoordinate = coor;
                // _currentBoardType.getPanel().getPanelSurfaceMapSettings().getOpticalRegion().setCoordinate(coor);
               }
               else
                {
                String message = "X coordinate and Y coordinate already exist. \n"
                        + "Please enter another value for both.";

                JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                        message,
                        StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                        JOptionPane.ERROR_MESSAGE);

                return;
                 }           
            }
            else 
            {
                String message = "X coordinate or Y coordinate cannot be blank or negative values. \n"
                        + "Please re-enter value for both.";

                JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                        message,
                        StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                        JOptionPane.ERROR_MESSAGE);

                return;
            }
            
            this.dispose();
          }
           else 
          {
                String message = "X coordinate or Y coordinate cannot be blank. \n"
                        + "Please enter value for both.";

                JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                        message,
                        StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                        JOptionPane.ERROR_MESSAGE);

                return;
          }
         }
          else 
          {
                String message = "X coordinate or Y coordinate cannot be blank. \n"
                        + "Please enter value for both.";

                JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                        message,
                        StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                        JOptionPane.ERROR_MESSAGE);

                return;
          }
        } 
        catch (BadFormatException ex) 
        {
            // Shouldn't ever occur.
        System.out.println( "first string: " +  _xCoordinateTextField.getText() );
        System.out.println( "second string: " + _yCoordinateTextField.getText() );
        Assert.logException(ex);
        }            
    }
    
  /*
   * @author Jack Hwee
   */
  public PanelCoordinate getAddedCoordinate()
  {
     return _addedCoordinate;
  }
  
  /*
   * @author Jack Hwee
   */
  public boolean isAddedCoordinateAvailable()
  {
     if (_addedCoordinate != null)
       return true;
     else
       return false;          
  }
     
     
   
}
