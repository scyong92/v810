package com.axi.v810.gui.psp;

import java.awt.*;
import java.awt.image.*;
import javax.swing.*;

import com.axi.util.*;

/**
 *
 * @author Ying-Huan.Chu
 */
public class SurfaceMapImageSectionPanel extends AbstractSurfaceMapSectionPanel
{
  private JLabel _image = null;
  
  /**
   * Constructor for Small FOV
   * @author Ying-Huan.Chu
   */
  public SurfaceMapImageSectionPanel(SurfaceMapSectionPanelResolutionEnum surfaceMapImageSectionPanelEnum)
  {
    super(surfaceMapImageSectionPanelEnum);
  }
  
  /**
   * Constructor for Larger FOV
   * @author Ying-Huan.Chu
   */
  protected SurfaceMapImageSectionPanel(SurfaceMapSectionPanelResolutionEnum surfaceMapImageSectionPanelEnum, boolean isEnabled)
  {
    super(surfaceMapImageSectionPanelEnum, isEnabled);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public SurfaceMapSectionTypeEnum getSurfaceMapSectionTypeEnum()
  {
    return SurfaceMapSectionTypeEnum.IMAGE;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  protected void paintComponent(Graphics g)
  {
    if (_isSectionEnabled)
    {
      Graphics2D g2d = (Graphics2D) g;
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
    }
    else
    {
      Graphics2D g2d = (Graphics2D) g;
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f));
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setContent(BufferedImage bufferedImage)
  {
    Assert.expect(bufferedImage != null);
    
    if (_image == null)
    {
      _image = new JLabel();
      _image.setIcon(new ImageIcon(bufferedImage));
      _image.setBounds(bufferedImage.getMinX(), bufferedImage.getMinY(), bufferedImage.getWidth(), bufferedImage.getHeight());
      add(_image, JLayeredPane.DEFAULT_LAYER);
    }
    else
    {
      ImageIcon icon = new ImageIcon(bufferedImage);
      icon.getImage().flush();
      _image.setIcon(icon);
      updateUI();
      revalidate();
      repaint();
    }
  }
}
