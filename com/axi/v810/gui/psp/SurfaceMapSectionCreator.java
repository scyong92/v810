package com.axi.v810.gui.psp;

import java.awt.image.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 *
 * @author Ying-Huan.Chu
 */
public class SurfaceMapSectionCreator
{
  // Small FOV
  private AbstractSurfaceMapSectionPanel _imageSectionPanel;
  private AbstractSurfaceMapSectionPanel _videoSectionPanel;
  
  // Larger FOV
  private AbstractSurfaceMapSectionPanel _largerFovImageSectionPanelTopLeft;
  private AbstractSurfaceMapSectionPanel _largerFovImageSectionPanelTopRight;
  private AbstractSurfaceMapSectionPanel _largerFovImageSectionPanelBottomLeft;
  private AbstractSurfaceMapSectionPanel _largerFovImageSectionPanelBottomRight;
  private AbstractSurfaceMapSectionPanel _largerFovVideoSectionPanel;
  
  private SurfaceMapFovTypeEnum _fovTypeEnum;
  private SurfaceMapSectionTypeEnum _currentSectionTypeEnum = SurfaceMapSectionTypeEnum.IMAGE;
  private SurfaceMapSectionPanelResolutionEnum _currentSectionPanelEnum;
  
  /**
   * @author Ying-Huan.Chu
   */
  public SurfaceMapSectionCreator(SurfaceMapFovTypeEnum surfaceMapFovTypeEnum)
  {
    _fovTypeEnum = surfaceMapFovTypeEnum;
    jbInit();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void jbInit()
  {
    if (_fovTypeEnum.equals(SurfaceMapFovTypeEnum.SMALL_FOV)) // Small FOV
    {
      _imageSectionPanel = new SurfaceMapImageSectionPanel(SurfaceMapSectionPanelResolutionEnum.ALL_MODE_LOW_RES);
      _videoSectionPanel = new SurfaceMapVideoSectionPanel(SurfaceMapSectionPanelResolutionEnum.ALL_MODE_LOW_RES);
      
      _currentSectionPanelEnum = SurfaceMapSectionPanelResolutionEnum.ALL_MODE_LOW_RES;
    }
    else if (_fovTypeEnum.equals(SurfaceMapFovTypeEnum.LARGE_FOV)) // Larger FOV
    {
      _largerFovImageSectionPanelTopLeft = new SurfaceMapImageSectionPanel(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_TOP_LEFT_SECTION, true);
      _largerFovImageSectionPanelTopRight = new SurfaceMapImageSectionPanel(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_TOP_RIGHT_SECTION, false);
      _largerFovImageSectionPanelBottomLeft = new SurfaceMapImageSectionPanel(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_BOTTOM_LEFT_SECTION, false);
      _largerFovImageSectionPanelBottomRight = new SurfaceMapImageSectionPanel(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_BOTTOM_RIGHT_SECTION, false);
      _largerFovVideoSectionPanel = new SurfaceMapVideoSectionPanel(SurfaceMapSectionPanelResolutionEnum.VIDEO_MODE_HIGH_RES, true);
      
      _currentSectionPanelEnum = SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_TOP_LEFT_SECTION;
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void setSurfaceMapImageSection()
  {
    Assert.expect(_imageSectionPanel != null);
    Assert.expect(_videoSectionPanel != null);
    
    // If user moved from Live View mode to Z-Height mode, we need to move the Surface Map Points from VideoSection to ImageSection.
    // 1. Clear the points in ImageSection first.
    // 2. Copy all the points from VideoSection to ImageSection.
    // 3. Set the visibility of ImageSection to true.
    // 4. Set the visibility of VideoSection to false.
    // if we are switching from video mode to image mode, we need to clear the points in image mode and copy the points from video mode to image mode.
    if (_currentSectionTypeEnum.equals(SurfaceMapSectionTypeEnum.VIDEO))
    {
      _imageSectionPanel.clearSurfaceMapPoints();
      for (SurfaceMapPointPanel surfaceMapPointPanel : _videoSectionPanel.getSurfaceMapPoints())
      {
        _imageSectionPanel.addSurfaceMapPoint(surfaceMapPointPanel.getPositionInImageInPixel());
      }
    }
    _currentSectionTypeEnum = SurfaceMapSectionTypeEnum.IMAGE;
    _imageSectionPanel.setVisible(true);
    _videoSectionPanel.setVisible(false);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void setSurfaceMapVideoSection()
  {
    Assert.expect(_imageSectionPanel != null);
    Assert.expect(_videoSectionPanel != null);
    
    // If user moved from Z-Height mode to Live View mode, we need to move the Surface Map Points from ImageSection to VideoSection.
    // 1. Clear the points in VideoSection first.
    // 2. Copy all the points from ImageSection to VideoSection.
    // 3. Set the visibility of VideoSection to true.
    // 4. Set the visibility of ImageSection to false.
    // if we are switching from image mode to video mode, we need to clear the points in video mode and copy the points from image mode to video mode.
    if (_currentSectionTypeEnum.equals(SurfaceMapSectionTypeEnum.IMAGE))
    {
      _videoSectionPanel.clearSurfaceMapPoints();
      for (SurfaceMapPointPanel surfaceMapPointPanel : _imageSectionPanel.getSurfaceMapPoints())
      {
        _videoSectionPanel.addSurfaceMapPoint(surfaceMapPointPanel.getPositionInImageInPixel());
      }
    }
    _currentSectionTypeEnum = SurfaceMapSectionTypeEnum.VIDEO;
    _videoSectionPanel.setVisible(true);
    _imageSectionPanel.setVisible(false);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public AbstractSurfaceMapSectionPanel getCurrentSurfaceMapSection()
  {
    if (_fovTypeEnum.equals(SurfaceMapFovTypeEnum.SMALL_FOV))
    {
      return getCurrentSmallFovSurfaceMapSection();
    }
    else // Larger FOV
    {
      return getCurrentLargerFovSurfaceMapSection();
    }
  }

  /**
   * @author Ying-Huan.Chu
   */
  private AbstractSurfaceMapSectionPanel getCurrentSmallFovSurfaceMapSection()
  {
    if (_currentSectionTypeEnum.equals(SurfaceMapSectionTypeEnum.IMAGE))
      return _imageSectionPanel;
    else
      return _videoSectionPanel;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private AbstractSurfaceMapSectionPanel getCurrentLargerFovSurfaceMapSection()
  {
    if (_currentSectionTypeEnum.equals(SurfaceMapSectionTypeEnum.IMAGE))
    {
      if (_currentSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_TOP_LEFT_SECTION))
        return _largerFovImageSectionPanelTopLeft;
      else if (_currentSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_TOP_RIGHT_SECTION))
        return _largerFovImageSectionPanelTopRight;
      else if (_currentSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_BOTTOM_LEFT_SECTION))
        return _largerFovImageSectionPanelBottomLeft;
      else // Bottom-right
        return _largerFovImageSectionPanelBottomRight;
    }
    else
      return _largerFovVideoSectionPanel;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private java.util.List<AbstractSurfaceMapSectionPanel> getAllLargerFovSurfaceMapSections()
  {
    java.util.List<AbstractSurfaceMapSectionPanel> _surfaceMapSections = new ArrayList<>();
    _surfaceMapSections.add(_largerFovImageSectionPanelTopLeft);
    _surfaceMapSections.add(_largerFovImageSectionPanelTopRight);
    _surfaceMapSections.add(_largerFovImageSectionPanelBottomLeft);
    _surfaceMapSections.add(_largerFovImageSectionPanelBottomRight);
    _surfaceMapSections.add(_largerFovVideoSectionPanel);
    
    return _surfaceMapSections;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void startImageMode(BufferedImage bufferedImage)
  {
    setSurfaceMapImageSection();
    if (_fovTypeEnum.equals(SurfaceMapFovTypeEnum.SMALL_FOV))
    {
      if (XrayTester.getInstance().isSimulationModeOn() == false)
      {
        Assert.expect(bufferedImage != null);
        
        _imageSectionPanel.setContent(bufferedImage);
      }
      _currentSectionPanelEnum = SurfaceMapSectionPanelResolutionEnum.ALL_MODE_LOW_RES;
    }
    else if (_fovTypeEnum.equals(SurfaceMapFovTypeEnum.LARGE_FOV))
    {
      if (XrayTester.getInstance().isSimulationModeOn() == false)
      {
        Assert.expect(bufferedImage != null);

        int sectionWidth = MathUtil.roundDoubleToInt(bufferedImage.getWidth() * 0.5);
        int sectionHeight = MathUtil.roundDoubleToInt(bufferedImage.getHeight() * 0.5);

        java.awt.image.BufferedImage section1 = bufferedImage.getSubimage(0, 0, sectionWidth, sectionHeight); // top-left
        _largerFovImageSectionPanelTopLeft.setContent(section1);

        java.awt.image.BufferedImage section2 = bufferedImage.getSubimage(sectionWidth, 0, sectionWidth, sectionHeight); // top-right
        _largerFovImageSectionPanelTopRight.setContent(section2);

        java.awt.image.BufferedImage section3 = bufferedImage.getSubimage(0, sectionHeight, sectionWidth, sectionHeight); // bottom-left
        _largerFovImageSectionPanelBottomLeft.setContent(section3);

        java.awt.image.BufferedImage section4 = bufferedImage.getSubimage(sectionWidth, sectionHeight, sectionWidth, sectionHeight); // bottom-right
        _largerFovImageSectionPanelBottomRight.setContent(section4);
      }

      _currentSectionPanelEnum = SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_TOP_LEFT_SECTION;
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void startVideoMode()
  {
    setSurfaceMapVideoSection();
    if (_fovTypeEnum.equals(SurfaceMapFovTypeEnum.SMALL_FOV))
    {
      _currentSectionPanelEnum = SurfaceMapSectionPanelResolutionEnum.ALL_MODE_LOW_RES;
    }
    else // Larger FOV
    {
      _currentSectionPanelEnum = SurfaceMapSectionPanelResolutionEnum.VIDEO_MODE_HIGH_RES;
    }
  }
  
  /**
   * This function is only for Larger FOV.
   * @author Ying-Huan.Chu
   */
  public void skipToPreviousSection()
  {
    // top-left section is the first image section in Larger FOV. Do nothing if user clicks 'Previous Section' button when current selected section is the top-left section.
    if (_currentSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_TOP_LEFT_SECTION) == false)
    {
      if (_currentSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_TOP_RIGHT_SECTION))
        _currentSectionPanelEnum = SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_TOP_LEFT_SECTION;
      else if (_currentSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_BOTTOM_LEFT_SECTION))
        _currentSectionPanelEnum = SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_TOP_RIGHT_SECTION;
      else if (_currentSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_BOTTOM_RIGHT_SECTION))
        _currentSectionPanelEnum = SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_BOTTOM_LEFT_SECTION;
      
      for (AbstractSurfaceMapSectionPanel sectionPanel : getAllLargerFovSurfaceMapSections())
      {
        if (sectionPanel.getSurfaceMapImageSectionPanelEnum().equals(_currentSectionPanelEnum))
        {
          sectionPanel.setSectionEnabled(true);
        }
        else
        {
          sectionPanel.setSectionEnabled(false);
        }
      }
    }
  }
  
  /**
   * This function is for Larger FOV.
   * @author Ying-Huan.Chu
   */
  public void skipToNextSection()
  {
    // bottom-right section is the last image section in Larger FOV. Do nothing if user clicks 'Next Section' button when current selected section is the bottom-right section.
    if (_currentSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_BOTTOM_RIGHT_SECTION) == false)
    {
      if (_currentSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_BOTTOM_LEFT_SECTION))
        _currentSectionPanelEnum = SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_BOTTOM_RIGHT_SECTION;
      else if (_currentSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_TOP_RIGHT_SECTION))
        _currentSectionPanelEnum = SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_BOTTOM_LEFT_SECTION;
      else if (_currentSectionPanelEnum.equals(SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_TOP_LEFT_SECTION))
        _currentSectionPanelEnum = SurfaceMapSectionPanelResolutionEnum.IMAGE_MODE_HIGH_RES_TOP_RIGHT_SECTION;
      
      for (AbstractSurfaceMapSectionPanel sectionPanel : getAllLargerFovSurfaceMapSections())
      {
        if (sectionPanel.getSurfaceMapImageSectionPanelEnum().equals(_currentSectionPanelEnum))
        {
          sectionPanel.setSectionEnabled(true);
        }
        else
        {
          sectionPanel.setSectionEnabled(false);
        }
      }
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public AbstractSurfaceMapSectionPanel getSurfaceMapSectionPanel(int x, int y)
  {
    if (_fovTypeEnum.equals(SurfaceMapFovTypeEnum.LARGE_FOV))
    {
      if (_currentSectionTypeEnum.equals(SurfaceMapSectionTypeEnum.IMAGE))
      {
        if ((x >= 0 && x <= AbstractSurfaceMapSectionPanel.IMAGE_SECTION_WIDTH) &&
            (y >= 0 && y <= AbstractSurfaceMapSectionPanel.IMAGE_SECTION_HEIGHT)) // top-left
        {
          return _largerFovImageSectionPanelTopLeft;
        }
        else if ((x >= AbstractSurfaceMapSectionPanel.IMAGE_SECTION_WIDTH && x <= (AbstractSurfaceMapSectionPanel.IMAGE_SECTION_WIDTH * 2)) &&
                 (y >= 0 && y <= AbstractSurfaceMapSectionPanel.IMAGE_SECTION_HEIGHT)) // top-right
        {
          return _largerFovImageSectionPanelTopRight;
        }
        else if ((x >= 0 && x <= AbstractSurfaceMapSectionPanel.IMAGE_SECTION_WIDTH) &&
                 (y >= AbstractSurfaceMapSectionPanel.IMAGE_SECTION_HEIGHT && y <= (AbstractSurfaceMapSectionPanel.IMAGE_SECTION_HEIGHT * 2))) // bottom-left
        {
          return _largerFovImageSectionPanelBottomLeft;
        }
        else // bottom-right
        {
          return _largerFovImageSectionPanelBottomRight;
        }
      }
      else // video mode
      {
        return _largerFovVideoSectionPanel;
      }
    }
    else // smaller FOV
    {
      if (_currentSectionTypeEnum.equals(SurfaceMapSectionTypeEnum.IMAGE))
        return _imageSectionPanel;
      else // video mode
        return _videoSectionPanel;
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public java.util.List<SurfaceMapPointPanel> getAllSurfaceMapPoints()
  {
    java.util.List<SurfaceMapPointPanel> surfaceMapPoints = new ArrayList<>();
    if (_fovTypeEnum.equals(SurfaceMapFovTypeEnum.LARGE_FOV)) // Larger FOV
    {
      if (_currentSectionTypeEnum.equals(SurfaceMapSectionTypeEnum.IMAGE))
      {
        surfaceMapPoints.addAll(_largerFovImageSectionPanelTopLeft.getSurfaceMapPoints());
        surfaceMapPoints.addAll(_largerFovImageSectionPanelTopRight.getSurfaceMapPoints());
        surfaceMapPoints.addAll(_largerFovImageSectionPanelBottomLeft.getSurfaceMapPoints());
        surfaceMapPoints.addAll(_largerFovImageSectionPanelBottomRight.getSurfaceMapPoints());
      }
      else // video mode
      {
        surfaceMapPoints.addAll(_largerFovVideoSectionPanel.getSurfaceMapPoints());
      }
    }
    else // Small Fov
    {
      if (_currentSectionTypeEnum.equals(SurfaceMapSectionTypeEnum.IMAGE))
      {
        surfaceMapPoints.addAll(_imageSectionPanel.getSurfaceMapPoints());
      }
      else // video mode
      {
        surfaceMapPoints.addAll(_videoSectionPanel.getSurfaceMapPoints());
      }
    }
    return surfaceMapPoints;
  }
}
