/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.psp;

/**
 *
 * @author Ying-Huan.Chu
 */
public class SurfaceMapFovTypeEnum extends com.axi.util.Enum
{
  private String _name;
  private static int _index = -1;
  
  public static SurfaceMapFovTypeEnum SMALL_FOV = new SurfaceMapFovTypeEnum(++_index, "Small FOV");
  public static SurfaceMapFovTypeEnum LARGE_FOV = new SurfaceMapFovTypeEnum(++_index, "Large FOV");
  
  /**
   * SurfaceMapFovTypeEnum Constructor
   * @author Ying-Huan.Chu
   */
  private SurfaceMapFovTypeEnum(int id, String name)
  {
    super(id);
    _name = name;
  }
  
  /**
   * @author Ying-Huan.Chu
   * @return get the name of the SurfaceMapFovTypeEnum
   */
  public String getName()
  {
    return _name;
  }
}
