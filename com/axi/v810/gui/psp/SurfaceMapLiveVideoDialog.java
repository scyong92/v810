package com.axi.v810.gui.psp;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.io.*;
import java.util.*;
import javax.imageio.*;
import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.ethernetBasedLightEngine.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class SurfaceMapLiveVideoDialog extends EscapeDialog
{
  private final PointToPointMotionProfileEnum _motionProfile = PointToPointMotionProfileEnum.PROFILE1;
  private static final int WAITING_FOR_HEIGHT_MAP_MAXIMUM_DURATION_IN_MILLISECONDS = 15000; // 15 seconds
  
  private AbstractOpticalCamera _abstractOpticalCamera;
  private PspLiveVideoPanel _pspLiveVideoPanel;
  private EthernetBasedLightEngineProcessor _eblep = EthernetBasedLightEngineProcessor.getInstance();

  JLabel _pointsLocationLabel1 = new JLabel();
  JLabel _pointsLocationLabel2 = new JLabel();
  JLabel _pointsLocationLabel3 = new JLabel();
  JLabel _pointsLocationLabel4 = new JLabel();
  JLabel _pointsLocationLabel5 = new JLabel();
  JLabel _pointsLocationLabel6 = new JLabel();
  
  JButton _saveButton = new JButton();
  JButton _clearPointsButton = new JButton();
  JButton _previousSectionButton = new JButton();
  JButton _nextSectionButton = new JButton();
  JButton _skipButton = new JButton();
  JButton _cancelButton = new JButton();
  JButton _previousButton = new JButton();
  
  JCheckBox _showColourMapCheckBox = new JCheckBox();
  
  JLabel _colourMapBarLabel = new JLabel();
  JLabel _level1ColorUpperRangeLabel = new JLabel();
  JLabel _level2ColorUpperRangeLabel = new JLabel();
  JLabel _level3ColorUpperRangeLabel = new JLabel();
  JLabel _level4ColorUpperRangeLabel = new JLabel();
  JLabel _level5ColorUpperRangeLabel = new JLabel();
  JLabel _level5ColorLowerRangeLabel = new JLabel();
  
  private JLabel _adjustBrightnessLabel = new JLabel();
  private JComboBox _adjustBrightnessComboBox = new JComboBox();
  
  private SurfaceMapSectionTypeEnum _currentSectionTypeEnum = SurfaceMapSectionTypeEnum.IMAGE;
  private JLabel _displayOptionLabel = new JLabel();
  private JRadioButton _displayLiveViewRadioButton = new JRadioButton();
  private JRadioButton _displayZHeightViewRadioButton = new JRadioButton();

  private MainMenuGui _mainUI = MainMenuGui.getInstance();
  private SurfaceMapping _surfaceMapping = SurfaceMapping.getInstance();
  private TestExecution _testExecution = TestExecution.getInstance();
  
  private BoardSurfaceMapSettings _boardSurfaceMapSettings = null; 
  private PanelSurfaceMapSettings _panelSurfaceMapSettings = null;
  private Project _project;
  private boolean _isPanelSurfaceMap = true;
  private OpticalRegion _opticalRegion = null;
  private OpticalCameraRectangle _opticalCameraRectangle = null;
  private PspAutoPopulatePanel _pspAutoPopulatePanel = PspAutoPopulatePanel.getInstance();
  private java.util.List<com.axi.v810.business.panelDesc.Component>  _component =  new ArrayList<com.axi.v810.business.panelDesc.Component>();
  private boolean _simulationMode = false;
  private boolean _cancelSurfaceMapping = false;
  private AffineTransform _transformFromRendererCoords = new AffineTransform();
  private Board _board;
  private boolean _updateSurfaceMapData = false;
  private boolean _inAlignmentSurfaceMap = false;
  private String _currentAlignmentGroupForSurfaceMap = "0";
  private java.util.List<Board> _coveredBoards = new java.util.ArrayList<>();
  private OpticalProgramGeneration _opticalProgramGeneration = OpticalProgramGeneration.getInstance();
  private int _width;
  private int _height;
  private java.util.Map<Point, Double> _pointPositionInPixelToZHeightInNanometerMap = new java.util.LinkedHashMap<Point, Double>();
  private double _zoomFactor = 0;
  
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  
  private MathUtilEnum _unitEnum = MathUtilEnum.MILLIMETERS;
  private boolean _isUseLargerFOV = false;
  private boolean _goToPreviousRectangle = false;
  private boolean _isLiveViewMode = false;
  private boolean _isPreviousButtonVisible = false;
  private XrayTesterException _exception;

  // This flag is used to check if a z-height map is returned from the native side when running Surface Map.
  private static boolean _isSurfaceMappingSuccess = false;
  
  private static HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  private int _dragStartingPointXInPixel = 0;
  private int _dragStartingPointYInPixel = 0;
  private int _dragDistanceXPerPixelInNanometer = 0;
  private int _dragDistanceYPerPixelInNanometer = 0;
  private Rectangle _maximumAllowableRegion;
  private Rectangle _originalOpticalCameraRectangleRegionInNanometer;
  private TestSubProgram _surfaceMapTestSubProgram;
  
  // XCR-2620 Recipe Compatibility
  private java.util.Map<PanelCoordinate, Point> _panelCoordinateToNonAdjustedPointMap = new java.util.LinkedHashMap<PanelCoordinate, Point>();
  private java.util.Map<PanelCoordinate, Point> _panelCoordinateToAdjustedPointMap = new java.util.LinkedHashMap<PanelCoordinate, Point>();
  
  /**
   * @author Cheah Lee Herng
   */
  public SurfaceMapLiveVideoDialog() throws XrayTesterException
  {
    setupDialogProperty();
    jbInit();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public SurfaceMapLiveVideoDialog(Frame parent, 
                                   String title, 
                                   Board board, 
                                   java.util.List<Board> coveredBoards, 
                                   OpticalRegion opticalRegion,
                                   OpticalCameraRectangle opticalCameraRectangle,
                                   boolean simulationMode, 
                                   boolean updateSurfaceMapData,
                                   MathUtilEnum unitEnum,
                                   boolean isLiveViewMode,
                                   boolean isPreviousButtonVisible) throws XrayTesterException
  {
    super(parent, title, true);
    Assert.expect(parent != null);
    Assert.expect(title != null);
    Assert.expect(board != null);
    Assert.expect(coveredBoards != null);
    Assert.expect(opticalRegion != null);
    Assert.expect(opticalCameraRectangle != null);
    Assert.expect(unitEnum != null);

    _exception = null;
    _board = board;
    _coveredBoards = coveredBoards;

    if (board.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      _panelSurfaceMapSettings = board.getPanel().getPanelSurfaceMapSettings();
      _isPanelSurfaceMap = true;
    }
    else
    {
      _boardSurfaceMapSettings = board.getBoardSurfaceMapSettings();
      _isPanelSurfaceMap = false;
    }

    if (_surfaceMapping.getPspModuleEnum().equals(PspModuleEnum.FRONT))
    {
      _abstractOpticalCamera = OpticalCameraArray.getCamera(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
    }
    else
    {
      _abstractOpticalCamera = OpticalCameraArray.getCamera(OpticalCameraIdEnum.OPTICAL_CAMERA_2);
    }

    _opticalRegion = opticalRegion;
    _opticalCameraRectangle = opticalCameraRectangle;
    _originalOpticalCameraRectangleRegionInNanometer = new Rectangle(opticalCameraRectangle.getRegion().getRectangle2D().getBounds());
    _surfaceMapTestSubProgram = SurfaceMapping.getInstance().getSurfaceMapTestSubProgram(_opticalCameraRectangle);
    _project = Project.getCurrentlyLoadedProject();
    _simulationMode = simulationMode;
    _updateSurfaceMapData = updateSurfaceMapData;
    _cancelSurfaceMapping = false;
    _inAlignmentSurfaceMap = false;
    _pointPositionInPixelToZHeightInNanometerMap.clear();
    _unitEnum = unitEnum;
    _isLiveViewMode = isLiveViewMode;
    if (_isLiveViewMode)
    {
      _isPreviousButtonVisible = isPreviousButtonVisible;
    }
    else
    {
      _isPreviousButtonVisible = false;
    }
    _isUseLargerFOV = _abstractOpticalCamera.isLargerFov();
    _previousSectionButton.setVisible(_isUseLargerFOV);
    _nextSectionButton.setVisible(_isUseLargerFOV);
    
    setupDialogProperty();
    jbInit();
   
    this.addWindowListener(new WindowAdapter()
    {
      public void windowClosing(WindowEvent we)
      {
        if (_simulationMode == false)
        {
          _abstractOpticalCamera.setStopLiveVideoBoolean(true);
          resetOpticalCameraRectangleToOriginalPosition();
          _cancelSurfaceMapping = true;
          _surfaceMapping.cancelRunToNextRegion();
          endSurfaceMapping(true);
        }
        else
        {
          _abstractOpticalCamera.setStopLiveVideoBoolean(true);
          _cancelSurfaceMapping = true;
          _surfaceMapping.cancelRunToNextRegion();
          dispose();
        }
      }
    });
    
    if (_abstractOpticalCamera.isHeightMapValueAvailable())
    {
      _pspLiveVideoPanel.setHeightMapArray(_abstractOpticalCamera.getHeightMapValue(), _width, _height);
    }
    //Jack Hwee - display image instead of live view
    java.awt.image.BufferedImage myPicture = null;
    if (simulationMode == false)
    {
      String imageFullPath = FileName.getAlignmentOpticalImageFullPath("temp", 0);

      if (FileUtil.exists(imageFullPath))
      {
        try
        {
          myPicture = javax.imageio.ImageIO.read(new java.io.File(imageFullPath));
        }
        catch (IOException ex)
        {
          ex.printStackTrace();
        }
      }
    }
    _pspLiveVideoPanel.setPreferredSize(new Dimension(_width, _height));
    _pspLiveVideoPanel.setBounds(0, 0, _width, _height);
    _pspLiveVideoPanel.startImageMode(myPicture);

    pack();
//    setupWindowHandle();
//    // Finally, show the live video
//    startOpticalLiveVideo();
    if (isEditMode())
    {
      setupSurfaceMapPoints();
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setVisible(boolean visible)
  {
    super.setVisible(visible);
  }

  /**
   * @author Cheah Lee Herng
   */
  private void jbInit() throws XrayTesterException
  {
    this.setResizable(false);
    this.setTitle(StringLocalizer.keyToString("PSP_SURFACE_MAP_LIVE_VIEW_DIALOG_TITLE_KEY"));
    Border _mainBorder = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(115, 114, 105),new Color(165, 163, 151)),BorderFactory.createEmptyBorder(5,5,5,5));

    JPanel coordinatePanel = new JPanel();
    JPanel coordinatePanel2 = new JPanel();
  
    _pspLiveVideoPanel = new PspLiveVideoPanel(_abstractOpticalCamera, _unitEnum, _isLiveViewMode);
    _pspLiveVideoPanel.setPreferredSize(new Dimension(_width, _height));
    _pspLiveVideoPanel.setBounds(0, 0, _width, _height);

    this.add(_pspLiveVideoPanel, BorderLayout.CENTER);
    
    GridLayout colourMapInfoLayout = new GridLayout();
    colourMapInfoLayout.setVgap(78);
    colourMapInfoLayout.setColumns(1);
    colourMapInfoLayout.setRows(6);
    JPanel colourMapInfoPanel = new JPanel(colourMapInfoLayout);
    colourMapInfoPanel.add(_level1ColorUpperRangeLabel);
    colourMapInfoPanel.add(_level2ColorUpperRangeLabel);
    colourMapInfoPanel.add(_level3ColorUpperRangeLabel);
    colourMapInfoPanel.add(_level4ColorUpperRangeLabel);
    colourMapInfoPanel.add(_level5ColorUpperRangeLabel);
    colourMapInfoPanel.add(_level5ColorLowerRangeLabel);
    JPanel colourMapPanel = new JPanel(new FlowLayout());
    colourMapPanel.add(_colourMapBarLabel);
    colourMapPanel.add(colourMapInfoPanel);
    this.add(colourMapPanel, BorderLayout.EAST);
   
    coordinatePanel.setLayout(new BoxLayout (coordinatePanel, BoxLayout.X_AXIS));
    coordinatePanel.setBorder(_mainBorder);
    coordinatePanel2.setLayout(new BoxLayout (coordinatePanel2, BoxLayout.X_AXIS));
    coordinatePanel2.setBorder(_mainBorder);
    _pointsLocationLabel1.setText("Point 1: " + "X: " + "Y: ");
    _pointsLocationLabel2.setText("Point 2: " + "X: " + "Y: ");
    _pointsLocationLabel3.setText("Point 3: " + "X: " + "Y: ");
    _pointsLocationLabel4.setText("Point 4: " + "X: " + "Y: ");
    _pointsLocationLabel5.setText("Point 5: " + "X: " + "Y: ");
    _pointsLocationLabel6.setText("Point 6: " + "X: " + "Y: ");
    coordinatePanel.add(Box.createRigidArea(new Dimension(10, 5)));
    coordinatePanel.add(_pointsLocationLabel1, BorderLayout.CENTER);
    coordinatePanel.add(Box.createRigidArea(new Dimension(30, 5)));
    coordinatePanel.add(_pointsLocationLabel2, BorderLayout.CENTER);
    coordinatePanel.add(Box.createRigidArea(new Dimension(50, 5)));
    coordinatePanel.add(_pointsLocationLabel3, BorderLayout.CENTER);
    coordinatePanel2.add(Box.createRigidArea(new Dimension(10, 5)));
    coordinatePanel2.add(_pointsLocationLabel4, BorderLayout.CENTER);
    coordinatePanel2.add(Box.createRigidArea(new Dimension(30, 5)));
    coordinatePanel2.add(_pointsLocationLabel5, BorderLayout.CENTER);
    coordinatePanel2.add(Box.createRigidArea(new Dimension(50, 5)));
    coordinatePanel2.add(_pointsLocationLabel6, BorderLayout.CENTER);
    
    if (_updateSurfaceMapData || _isLiveViewMode == false)
    {
      _saveButton.setText(StringLocalizer.keyToString("PSP_SAVE_RECTANGLE_BUTTON_KEY"));
    }
    else
    {
      _saveButton.setText(StringLocalizer.keyToString("PSP_NEXT_RECTANGLE_BUTTON_KEY"));
    }
    _saveButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveButton_actionPerformed(e);
      }
    });
    _saveButton.setEnabled(false);

    _clearPointsButton.setText(StringLocalizer.keyToString("PSP_CLEAR_POINTS_BUTTON_KEY"));
    _clearPointsButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        clearPointsButton_actionPerformed(e);
      }
    });

    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    
    _previousSectionButton.setText(StringLocalizer.keyToString("PSP_PREVIOUS_SECTION_BUTTON_KEY"));
    _previousSectionButton.setVisible(_isUseLargerFOV);
    _previousSectionButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        previousSection_actionPerformed(e);
      }
    });
    
    _nextSectionButton.setText(StringLocalizer.keyToString("PSP_NEXT_SECTION_BUTTON_KEY"));
    _nextSectionButton.setVisible(_isUseLargerFOV);
    _nextSectionButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        nextSection_actionPerformed(e);
      }
    });
    
    _skipButton.setText(StringLocalizer.keyToString("PSP_SKIP_SURFACE_MAP_BUTTON_KEY"));
    _skipButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try
        {
          skipButton_actionPerformed(e);
        }
        catch (XrayTesterException xte)
        {
          _mainUI.handleXrayTesterException(xte);
        }
      }
    });
    
    _showColourMapCheckBox.setText(StringLocalizer.keyToString("PSP_DISPLAY_COLOURMAP_SURFACE_MAP_BUTTON_KEY"));
    _showColourMapCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showColourMapCheckBox_actionPerformed(e);
      }
    });
    
    _previousButton.setText(StringLocalizer.keyToString("PSP_PREVIOUS_RECTANGLE_BUTTON_KEY"));
    _previousButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        previousRectangleButton_actionPerformed(e);
      }
    });
    _previousButton.setEnabled(_isPreviousButtonVisible);
    
    // Adjust Brightness Label
    _adjustBrightnessLabel.setText(StringLocalizer.keyToString("PSP_ADJUST_BRIGHTNESS_LABEL_KEY"));
    // Adjust Brightness Combobox
    _adjustBrightnessComboBox.addItem(EthernetBasedLightEngineCurrentSettingEnum.CURRENT_350mA);
    _adjustBrightnessComboBox.addItem(EthernetBasedLightEngineCurrentSettingEnum.CURRENT_495mA);
    _adjustBrightnessComboBox.addItem(EthernetBasedLightEngineCurrentSettingEnum.CURRENT_635mA);
    _adjustBrightnessComboBox.setSelectedItem(EthernetBasedLightEngineCurrentSettingEnum.getEnum(_project.getPanel().getPspSettings().getCurrentSetting()));
    _adjustBrightnessComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustBrightnessComboBox_actionPerformed();
      }
    });
    
    _displayOptionLabel.setText(StringLocalizer.keyToString("PSP_DISPLAY_MODE_KEY"));
    _displayLiveViewRadioButton.setText(StringLocalizer.keyToString("PSP_DISPLAY_LIVE_VIEW_MODE_KEY"));
    _displayLiveViewRadioButton.setSelected(false);
    _displayLiveViewRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        displayLiveViewRadioButton_actionPerformed();
      }
    });
    
    _displayZHeightViewRadioButton.setText(StringLocalizer.keyToString("PSP_DISPLAY_ZHEIGHT_MODE_KEY"));
    _displayZHeightViewRadioButton.setSelected(true);
    _displayZHeightViewRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        displayZHeightViewRadioButton_actionPerformed();
      }
    });
    
    JPanel _editButtonPanel = new JPanel();
    FlowLayout _editButtonPanelFlowLayout = new FlowLayout();
    _editButtonPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _editButtonPanelFlowLayout.setHgap(10);
    _editButtonPanel.setLayout(_editButtonPanelFlowLayout);
    _editButtonPanel.add(_previousButton, null);
    _editButtonPanel.add(_saveButton, null);
    _editButtonPanel.add(_clearPointsButton, null);
    //_editButtonPanel.add(_previousSectionButton, null);
    //_editButtonPanel.add(_nextSectionButton, null);
    _editButtonPanel.add(_skipButton, null);
    _editButtonPanel.add(_cancelButton, null);
    //_editButtonPanel.add(_showColourMapCheckBox, null);

    JPanel adjustBrightnessPanel = new JPanel();
    FlowLayout adjustBrightnessFlowLayout = new FlowLayout();
    adjustBrightnessFlowLayout.setAlignment(FlowLayout.LEFT);
    adjustBrightnessFlowLayout.setHgap(10);
    adjustBrightnessPanel.setLayout(adjustBrightnessFlowLayout);
    adjustBrightnessPanel.add(_adjustBrightnessLabel, null);
    adjustBrightnessPanel.add(_adjustBrightnessComboBox, null);
    
    JPanel displayModePanel = new JPanel();
    FlowLayout displayModeFlowLayout = new FlowLayout();
    displayModeFlowLayout.setAlignment(FlowLayout.LEFT);
    displayModeFlowLayout.setHgap(10);
    displayModePanel.setLayout(displayModeFlowLayout);
    displayModePanel.add(_displayOptionLabel, null);
    displayModePanel.add(_displayZHeightViewRadioButton, null);
    displayModePanel.add(_displayLiveViewRadioButton, null);
    
    JPanel displayModeAndAdjustBrightnessPanel = new JPanel();
    FlowLayout displayModeAndAdjustBrightnessFlowLayout = new FlowLayout();
    displayModeAndAdjustBrightnessFlowLayout.setAlignment(FlowLayout.LEFT);
    displayModeAndAdjustBrightnessFlowLayout.setHgap(10);
    displayModeAndAdjustBrightnessPanel.setLayout(displayModeAndAdjustBrightnessFlowLayout);
    displayModeAndAdjustBrightnessPanel.add(displayModePanel, null);
    displayModeAndAdjustBrightnessPanel.add(adjustBrightnessPanel, null);
    
    JPanel pointsPanel = new JPanel();
    pointsPanel.setLayout(new BorderLayout());
    pointsPanel.setBorder(_mainBorder);
    pointsPanel.add(coordinatePanel, BorderLayout.NORTH);
    pointsPanel.add(coordinatePanel2, BorderLayout.CENTER);

    JPanel functionPanel = new JPanel();
    functionPanel.setLayout(new BorderLayout());
    functionPanel.setBorder(_mainBorder);
    functionPanel.add(pointsPanel, BorderLayout.NORTH);
    functionPanel.add(displayModeAndAdjustBrightnessPanel, BorderLayout.CENTER);
    functionPanel.add(_editButtonPanel, BorderLayout.SOUTH);

    this.add(functionPanel, BorderLayout.SOUTH);

    _pspLiveVideoPanel.addMouseListener(new MouseAdapter()
    {
      public void mousePressed(MouseEvent mouseEvent)
      {
        handleMousePressed(mouseEvent);
      }
      public void mouseReleased(MouseEvent mouseEvent)
      {
        handleMouseReleased(mouseEvent);
      }
    });
  }

  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  public void handleMousePressed(MouseEvent mouseEvent)
  {
    if ((mouseEvent.getModifiers() & MouseEvent.BUTTON1_MASK) == MouseEvent.BUTTON1_MASK)
    {
      int x = mouseEvent.getX();
      int y = mouseEvent.getY();
      if (_pspLiveVideoPanel.isWithinEnabledSurfaceMapImageSection(x, y));
      {
        _pspLiveVideoPanel.addSurfaceMapPoint(x, y);
        _saveButton.setEnabled(true);
        // put the point with 0 z-height into the map first (to handle simulation mode) - we will do the z-height calculation and replace this entry afterwards.
        _pointPositionInPixelToZHeightInNanometerMap.put(new Point(x, y), 0.0);
        updateXYLocationLabels();
      }
    }
    else if ((mouseEvent.getModifiers() & MouseEvent.BUTTON3_MASK) == MouseEvent.BUTTON3_MASK)
    {
      _dragStartingPointXInPixel = mouseEvent.getX();
      _dragStartingPointYInPixel = mouseEvent.getY();
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void handleMouseReleased(final MouseEvent mouseEvent)
  {
    if (_displayLiveViewRadioButton.isSelected() == false) // ignore right-click operation when in z-height mode. 
      return;
    
    if ((mouseEvent.getModifiers() & MouseEvent.BUTTON3_MASK) == MouseEvent.BUTTON3_MASK)
    {
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            if ((mouseEvent.getModifiers() & MouseEvent.BUTTON3_MASK) == MouseEvent.BUTTON3_MASK)
            {
              int dragEndingPointX = mouseEvent.getX();
              int dragEndingPointY = mouseEvent.getY();
              int totalDragXDistanceTravelledInPixel = dragEndingPointX  - _dragStartingPointXInPixel;
              int totalDragYDistanceTravelledInPixel = dragEndingPointY - _dragStartingPointYInPixel;
              int totalDragXDistanceTravelledInNanometer = totalDragXDistanceTravelledInPixel * _dragDistanceXPerPixelInNanometer;
              int totalDragYDistanceTravelledInNanometer = totalDragYDistanceTravelledInPixel * _dragDistanceYPerPixelInNanometer;
              
              Rectangle currentRegionRectangleInNanometer = _opticalCameraRectangle.getRegion().getRectangle2D().getBounds();   
              if (_simulationMode == false)
              { 
                Rectangle rectangleAfterStageMovementInNanometer = getFinalRectangleAfterStageMovement(currentRegionRectangleInNanometer, totalDragXDistanceTravelledInNanometer, totalDragYDistanceTravelledInNanometer);
                
                // If user moves the OpticalCameraRectangle too much (out of allowable region boundaries), we should adjust it back to the minimum/maximum position it can be moved to.
                if (_maximumAllowableRegion.contains(rectangleAfterStageMovementInNanometer) == false)
                {
                  int minXInNanometer = MathUtil.roundDoubleToInt(rectangleAfterStageMovementInNanometer.getMinX());
                  int minYInNanometer = MathUtil.roundDoubleToInt(rectangleAfterStageMovementInNanometer.getMinY());
                
                  if (rectangleAfterStageMovementInNanometer.getMaxY() > _maximumAllowableRegion.getMaxY())
                    minYInNanometer = Math.max(0, MathUtil.roundDoubleToInt(_maximumAllowableRegion.getMaxY() - _opticalCameraRectangle.getRegion().getHeight()));
                  if (rectangleAfterStageMovementInNanometer.getMinY() < _maximumAllowableRegion.getMinY())
                    minYInNanometer = MathUtil.roundDoubleToInt(_maximumAllowableRegion.getMinY());
                  
                  if (rectangleAfterStageMovementInNanometer.getMaxX() > _maximumAllowableRegion.getMaxX())
                    minXInNanometer = Math.max(0, MathUtil.roundDoubleToInt(_maximumAllowableRegion.getMaxX() - _opticalCameraRectangle.getRegion().getWidth()));
                  if (rectangleAfterStageMovementInNanometer.getMinX() < _maximumAllowableRegion.getMinX())
                    minXInNanometer = MathUtil.roundDoubleToInt(_maximumAllowableRegion.getMinX());
                  
                  rectangleAfterStageMovementInNanometer.setBounds(minXInNanometer, 
                                                                   minYInNanometer, 
                                                                   MathUtil.roundDoubleToInt(rectangleAfterStageMovementInNanometer.getWidth()), 
                                                                   MathUtil.roundDoubleToInt(rectangleAfterStageMovementInNanometer.getHeight()));
                }

                int opticalFiducialStageCoordinateLocationXInNanometers = -1;
                int opticalFiducialStageCoordinateLocationYInNanometers = -1;
                if (_surfaceMapping.getPspModuleEnum().equals(PspModuleEnum.FRONT))
                {
                  opticalFiducialStageCoordinateLocationXInNanometers = PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers();
                  opticalFiducialStageCoordinateLocationYInNanometers = PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers();
                }
                else
                {
                  opticalFiducialStageCoordinateLocationXInNanometers = PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers();
                  opticalFiducialStageCoordinateLocationYInNanometers = PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers();
                }
                
                PanelCoordinate newPanelCoordinate = new PanelCoordinate(MathUtil.roundDoubleToInt(rectangleAfterStageMovementInNanometer.getCenterX()), MathUtil.roundDoubleToInt(rectangleAfterStageMovementInNanometer.getCenterY()));
                StagePosition stagePosition = OpticalMechanicalConversions.convertPanelCoordinateToStageCoordinate(_surfaceMapTestSubProgram, 
                                                                  newPanelCoordinate, 
                                                                  _surfaceMapTestSubProgram.getAggregateRuntimeAlignmentTransform(), 
                                                                  _surfaceMapTestSubProgram.getPanelLocationInSystem(),
                                                                  PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationXInNanometers(),
                                                                  PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationYInNanometers(),
                                                                  opticalFiducialStageCoordinateLocationXInNanometers,
                                                                  opticalFiducialStageCoordinateLocationYInNanometers,
                                                                  new BooleanRef());
                // Finally, move the OpticalCameraRectangle to the adjusted stage position.
                PspImageAcquisitionEngine.getInstance().moveStage(stagePosition);

                _opticalCameraRectangle.setRegion(MathUtil.roundDoubleToInt(rectangleAfterStageMovementInNanometer.getMinX()), 
                                                  MathUtil.roundDoubleToInt(rectangleAfterStageMovementInNanometer.getMinY()), 
                                                  MathUtil.roundDoubleToInt(rectangleAfterStageMovementInNanometer.getWidth()), 
                                                  MathUtil.roundDoubleToInt(rectangleAfterStageMovementInNanometer.getHeight()));
              }
            }
          }
          catch (XrayTesterException xte)
          {
            _mainUI.handleXrayTesterException(xte);
          }
        }
      });
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private Rectangle getMaximumAllowableRegion()
  {
    Map<Rectangle, PspModuleEnum> allowableRegionToPspModuleEnumMap = PspAutoPopulatePanel.getAllowableRegionToPspModuleEnumMap();
    Rectangle maximumAllowableRegion = null;
    Rectangle currentRegionRectangle = _opticalCameraRectangle.getRegion().getRectangle2D().getBounds();
    // find the region that contains the rectangle
    for (Map.Entry<Rectangle, PspModuleEnum> entry : allowableRegionToPspModuleEnumMap.entrySet())
    {
      Rectangle allowableRegion = entry.getKey();
      if (allowableRegion.contains(currentRegionRectangle.getCenterX(), currentRegionRectangle.getCenterY()))
      {
        maximumAllowableRegion = allowableRegion;
        break;
      }
    }
    
    Assert.expect(maximumAllowableRegion != null);
    
    
    double minXOffset = (_opticalCameraRectangle.getRegion().getRectangle2D().getMinX() < maximumAllowableRegion.getMinX()) ? (maximumAllowableRegion.getMinX() - _opticalCameraRectangle.getRegion().getRectangle2D().getMinX()) : 0;
    double minYOffset = (_opticalCameraRectangle.getRegion().getRectangle2D().getMinY() < maximumAllowableRegion.getMinY()) ? (maximumAllowableRegion.getMinY() - _opticalCameraRectangle.getRegion().getRectangle2D().getMinY()) : 0;
    double widthOffset = (maximumAllowableRegion.getMaxX() < _opticalCameraRectangle.getRegion().getRectangle2D().getMaxX()) ? (_opticalCameraRectangle.getRegion().getRectangle2D().getMaxX() - maximumAllowableRegion.getMaxX()) : 0;
    double heightOffset = (maximumAllowableRegion.getMaxY() < _opticalCameraRectangle.getRegion().getRectangle2D().getMaxY()) ? (_opticalCameraRectangle.getRegion().getRectangle2D().getMaxY() - maximumAllowableRegion.getMaxY()) : 0;
    maximumAllowableRegion.setBounds(MathUtil.roundDoubleToInt(maximumAllowableRegion.getMinX() - minXOffset),
                                     MathUtil.roundDoubleToInt(maximumAllowableRegion.getMinY() - minYOffset),
                                     MathUtil.roundDoubleToInt(maximumAllowableRegion.getWidth() + widthOffset), 
                                     MathUtil.roundDoubleToInt(maximumAllowableRegion.getHeight() + heightOffset));
    
    return maximumAllowableRegion;
  }

  /**
   * @author Cheah Lee Herng
   */
  private double getOpticalCameraGainFactor(OpticalCameraIdEnum cameraId)
  {
    Assert.expect(cameraId != null);

    double cameraGainFactor = 0.0;
    if (cameraId.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1))
        cameraGainFactor = Config.getInstance().getDoubleValue(PspCalibEnum.OPTICAL_CAMERA_1_GAIN_FACTOR);
    else if (cameraId.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_2))
        cameraGainFactor = Config.getInstance().getDoubleValue(PspCalibEnum.OPTICAL_CAMERA_2_GAIN_FACTOR);
    else
        Assert.expect(false, "Invalid camera id.");
    return cameraGainFactor;
  }

  /**
   * @author Jack Hwee
   */
  private void clearPointsButton_actionPerformed(ActionEvent e)
  {
    AbstractSurfaceMapSectionPanel surfaceMapImageSection = _pspLiveVideoPanel.getCurrentEnabledSurfaceMapSection();
    if (surfaceMapImageSection != null)
    {
      surfaceMapImageSection.clearSurfaceMapPoints();
      clearPointsLocationLabels();
      surfaceMapImageSection.repaint();
    }
    if (_pspLiveVideoPanel.getAllSurfaceMapPoints().isEmpty())
    {
      _saveButton.setEnabled(false);
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void clearPointsLocationLabels()
  {
    _pointsLocationLabel1.setText("Point 1: " + "X:" + "0" + "  " + "Y:" + "0");
    _pointsLocationLabel2.setText("Point 2: " + "X:" + "0" + "  " + "Y:" + "0");
    _pointsLocationLabel3.setText("Point 3: " + "X:" + "0" + "  " + "Y:" + "0");
    _pointsLocationLabel4.setText("Point 4: " + "X:" + "0" + "  " + "Y:" + "0");
    _pointsLocationLabel5.setText("Point 5: " + "X:" + "0" + "  " + "Y:" + "0");
    _pointsLocationLabel6.setText("Point 6: " + "X:" + "0" + "  " + "Y:" + "0");

    _pointsLocationLabel1.setForeground(Color.BLACK);
    _pointsLocationLabel2.setForeground(Color.BLACK);
    _pointsLocationLabel3.setForeground(Color.BLACK);
    _pointsLocationLabel4.setForeground(Color.BLACK);
    _pointsLocationLabel5.setForeground(Color.BLACK);
    _pointsLocationLabel6.setForeground(Color.BLACK);
  }

  /**
   * @author Jack Hwee
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    if (_simulationMode == false)
    {
      try
      {
        //check projector connection
        checkProjectorConnectivity();
        
        _abstractOpticalCamera.setStopLiveVideoBoolean(true);
        resetOpticalCameraRectangleToOriginalPosition();
        _cancelSurfaceMapping = true;
        _surfaceMapping.cancelRunToNextRegion();
        endSurfaceMapping(true);
      }
      catch (XrayTesterException xte)
      {
        try
        {
          exitOpticalCamera();
        }
        finally
        {
          dispose();
        }
        _exception = xte;
      }
    }
    else
    {
      _abstractOpticalCamera.setStopLiveVideoBoolean(true);
      _cancelSurfaceMapping = true;
      _surfaceMapping.cancelRunToNextRegion();
      dispose();
    }
  }
  
  /**
   * @author Jack Hwee
   */
  public boolean isCancelSurfaceMapping()
  {
    return _cancelSurfaceMapping;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void previousSection_actionPerformed(ActionEvent e)
  {
    _pspLiveVideoPanel.skipToPreviousSection();
    updateXYLocationLabels();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void nextSection_actionPerformed(ActionEvent e)
  {
    _pspLiveVideoPanel.skipToNextSection();
    updateXYLocationLabels();
  }
  
  /**
   * @author Jack Hwee
   */
  private void skipButton_actionPerformed(ActionEvent e) throws XrayTesterException
  {
    try
    {
      //check projector connection
      checkProjectorConnectivity();
      
      _abstractOpticalCamera.setStopLiveVideoBoolean(true);
      resetOpticalCameraRectangleToOriginalPosition();

      if (_inAlignmentSurfaceMap == false)
      {
        updateSurfaceMapData(false);
      }

      _surfaceMapping.runToNextRegion();

      clearPointsButton_actionPerformed(e);

      if (_simulationMode == false)
      {
        endSurfaceMapping(true);
      }
      else
      {
        dispose();
      }
    }
    catch (XrayTesterException xte)
    {
      try
      {
        exitOpticalCamera();
      }
      finally
      {
        dispose();
      }
      _exception = xte;
    }
  }
  
  /**
   * Display Optical Image with Colour Map filter
   * @author Ying-Huan.Chu
   */
  private void showColourMapCheckBox_actionPerformed(ActionEvent e)
  {
    if (_showColourMapCheckBox.isSelected())
    {
      String imageFullPath = FileName.getAlignmentOpticalImageFullPath("temp", 0);
      String zHeightImageFullPath = FileName.getAlignmentOpticalImageFullPath("temp", 1);
      String colourMapImageFullPath = FileName.getAlignmentOpticalImageFullPath("temp", 2);
      String colourMapBarImageFullPath = FileName.getPSPColourMapBarImageFullPath();
      float[] zHeightMap = _pspLiveVideoPanel.getZHeightMapArray();
      
//      if (zHeightMap != null && zHeightMap.length > 0)
//      {
//        // PSP Height Outlier checking
//        float[] zHeightArrayInMillimetersFromPspAfterOutlierRemoval = new float[zHeightMap.length];      
//        float median = StatisticsUtil.median(zHeightMap);
//        float min = median - (Math.abs(0.20f * zHeightMap[ProfileUtil.getMinValueIndex(zHeightMap)]));
//        float max = median + (0.20f * zHeightMap[ProfileUtil.findMaxValue(zHeightMap)]);
//
//        for (int counter = 0; counter < zHeightMap.length; ++counter)
//        {
//          boolean valueOutsideQuartileRange = true;
//          if ((zHeightMap[counter] < max) && (zHeightMap[counter] > min))
//          {
//            zHeightArrayInMillimetersFromPspAfterOutlierRemoval[counter] = zHeightMap[counter];
//            valueOutsideQuartileRange = false;
//          }
//          // setting outlier's value to median's value
//          if (valueOutsideQuartileRange == true)
//          {
//            zHeightArrayInMillimetersFromPspAfterOutlierRemoval[counter] = median;
//          }
//        }
//
//        // Calculate colour range for Colour Map after removing Z-Height map outliers
//        float minAfterOutlierRemoval = zHeightArrayInMillimetersFromPspAfterOutlierRemoval[
//                                       ProfileUtil.getMinValueIndex(zHeightArrayInMillimetersFromPspAfterOutlierRemoval)];
//        float maxAfterOutlierRemoval = zHeightArrayInMillimetersFromPspAfterOutlierRemoval[
//                                       ProfileUtil.findMaxValue(zHeightArrayInMillimetersFromPspAfterOutlierRemoval)];
//
//        float[] colourMapRangeLevel = new float[4];
//        float colourMapRangePortion = (maxAfterOutlierRemoval + Math.abs(minAfterOutlierRemoval))/6;
//        for (int i = 0; i < colourMapRangeLevel.length; ++i)
//        {
//          colourMapRangeLevel[i] = minAfterOutlierRemoval + colourMapRangePortion * (i+1);
//        }
//
//        DecimalFormat decimalFormat = new DecimalFormat("#.##");
//        String level1ColourUpperRange = decimalFormat.format(maxAfterOutlierRemoval);
//        String level2ColourUpperRange = decimalFormat.format(colourMapRangeLevel[3]);
//        String level3ColourUpperRange = decimalFormat.format(colourMapRangeLevel[2]);
//        String level4ColourUpperRange = decimalFormat.format(colourMapRangeLevel[1]);
//        String level5ColourUpperRange = decimalFormat.format(colourMapRangeLevel[0]);
//        String level5ColourLowerRange = decimalFormat.format(minAfterOutlierRemoval);
//
//        _level1ColorUpperRangeLabel.setText(String.valueOf(level1ColourUpperRange));
//        _level2ColorUpperRangeLabel.setText(String.valueOf(level2ColourUpperRange));
//        _level3ColorUpperRangeLabel.setText(String.valueOf(level3ColourUpperRange));
//        _level4ColorUpperRangeLabel.setText(String.valueOf(level4ColourUpperRange));
//        _level5ColorUpperRangeLabel.setText(String.valueOf(level5ColourUpperRange));
//        _level5ColorLowerRangeLabel.setText(String.valueOf(level5ColourLowerRange));

        //if (_simulationMode == false)
        {
          if (FileUtil.exists(imageFullPath))
          {
            // Jack Hwee - display image instead of live view
            java.awt.image.BufferedImage srcBufferedImage = null;
            java.awt.image.BufferedImage zHeightBufferedImage = null;
            java.awt.image.BufferedImage bufferedImage = null;
            java.awt.image.BufferedImage colourMapBar = null;
            try
            {
              srcBufferedImage = javax.imageio.ImageIO.read(new java.io.File(imageFullPath));
              //com.axi.util.image.Image srcImage = com.axi.util.image.Image.createFloatImageFromBufferedImage(srcBufferedImage);
              //int bufferWidth = srcImage.getWidth();
              //int bufferHeight = srcImage.getHeight();
              //com.axi.util.image.Image zHeightImage = com.axi.util.image.Image.createFloatImageFromArray(bufferWidth, bufferHeight, zHeightArrayInMillimetersFromPspAfterOutlierRemoval);
              //ImageIoUtil.saveImage(zHeightImage, zHeightImageFullPath);
              ImageEnhancer.colourMap(colourMapImageFullPath, imageFullPath, ColourMapTypeEnum.FIRE);

              // overlay image
              zHeightBufferedImage = javax.imageio.ImageIO.read(new java.io.File(colourMapImageFullPath));

              int w = Math.max(srcBufferedImage.getWidth(), zHeightBufferedImage.getWidth());
              int h = Math.max(srcBufferedImage.getHeight(), zHeightBufferedImage.getHeight());
              bufferedImage = new java.awt.image.BufferedImage(w, h, java.awt.image.BufferedImage.TYPE_INT_ARGB);

              for (int y = 0; y < srcBufferedImage.getHeight(); y++) 
              {
                for (int x = 0; x < srcBufferedImage.getWidth(); x++) 
                {
                  Color c = new Color(srcBufferedImage.getRGB(x, y));
                  Color maskC = new Color(zHeightBufferedImage.getRGB(x, y));
                  Color maskedColor = new Color(maskC.getRed(), maskC.getGreen(), maskC.getBlue(), c.getRed());
                  bufferedImage.setRGB(x, y, maskedColor.getRGB());
                }
              }
              ImageIO.write(bufferedImage, "PNG", new File(FileName.getAlignmentOpticalImageFullPath("temp", 3)));
              bufferedImage = javax.imageio.ImageIO.read(new java.io.File(FileName.getAlignmentOpticalImageFullPath("temp", 3)));
              //colourMapBar = javax.imageio.ImageIO.read(new java.io.File(colourMapBarImageFullPath));
            }
            catch (IOException ex)
            {
              ex.printStackTrace();
            }
            if (bufferedImage != null)
            {
              _pspLiveVideoPanel.setPreferredSize(new Dimension(_width + 1, _height + 24));
              _pspLiveVideoPanel.setBounds(0, 0, _width + 1, _height + 24);
              _pspLiveVideoPanel.startImageMode(bufferedImage);
              if (colourMapBar != null)
              {
                _pspLiveVideoPanel.setPreferredSize(new Dimension(_width + 1 + 120, _height + 24));
                _pspLiveVideoPanel.setBounds(0, 0, _width + 1 + 120, _height + 24);
                _colourMapBarLabel.setIcon(new ImageIcon(colourMapBar));
              }
            }
          }
        }
      }
//    }
//    else
//    {
//      java.awt.image.BufferedImage bufferedImage = null;
//      try
//      {
//        bufferedImage = javax.imageio.ImageIO.read(new java.io.File(FileName.getAlignmentOpticalImageFullPath("temp", 0)));
//        _pspLiveVideoPanel.setPreferredSize(new Dimension(_width + 1, _height + 24));
//        _pspLiveVideoPanel.setBounds(0, 0, _width + 1, _height + 24);
//        _pspLiveVideoPanel.startImageMode(bufferedImage);
//      }
//      catch (IOException ex)
//      {
//        ex.printStackTrace();
//      }
//    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void previousRectangleButton_actionPerformed(ActionEvent e)
  {
    try
    {
      //check projector connection
      checkProjectorConnectivity();
      
      // for Sentech Optical camera
      _abstractOpticalCamera.setStopLiveVideoBoolean(true);
      resetOpticalCameraRectangleToOriginalPosition();
      _goToPreviousRectangle = true;

      _surfaceMapping.runToNextRegion();

    if(_simulationMode == false)
        endSurfaceMapping(true);
      else
        dispose();
    }
    catch (XrayTesterException xte)
    {
      try
      {
        exitOpticalCamera();
      }
      finally
      {
        dispose();
      }
      _exception = xte;
    }
  }

  /**
   * When call this function, a BusyCancelDialog will pop up.
   * This is so that to make sure user does not feel the system is hang
   * when system is actually performing Surface Map update.
   * 
   * @author Cheah Lee Herng
   */
  private void updateSurfaceMapData(final boolean isSave)
  {
    final BusyCancelDialog busyDialog = new BusyCancelDialog(
                                                            (Frame)getOwner(),
                                                            StringLocalizer.keyToString("PSP_UPDATE_DATA_KEY"),
                                                            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                            true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, (Frame) getOwner());
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (_isPanelSurfaceMap) // panel-based
          {
            if (isEditMode()) // Handle edit existing surface map points
            {
              // Before do anything, clear all the existing points
              _opticalCameraRectangle.clearPointPanelCoordinateList();
                          
              OpticalRegion opticalRegion = _project.getTestProgram().getOpticalRegion(_opticalRegion.getName());
              if (opticalRegion == null)
              {
                for (com.axi.v810.business.panelDesc.Component component : _pspAutoPopulatePanel.getAllComponents())
                {
                  if (_pspAutoPopulatePanel.isComponentUsableInSurfaceMap(component))
                  {
                    component.getComponentType().setToUsePspResult(true);
                    _opticalRegion.addComponent(component);

                  }
                }
                _project.getPanel().getPanelSurfaceMapSettings().removeSurfaceMapRegion(_opticalRegion);
                _project.getPanel().getPanelSurfaceMapSettings().addOpticalRegion(_opticalRegion);

                // Special to handle multi-board but using panelAlignment
                if (_project.getPanel().isMultiBoardPanel())
                {
                  if (_updateSurfaceMapData)
                  {
                    for (Board board : _coveredBoards)
                    {
                      _opticalRegion.setBoard(board);
                    }
                  }
                }
                else
                {
                  _opticalRegion.setBoard(_board);
                }
                _opticalRegion.addOpticalCameraRectangle(_opticalCameraRectangle);
                boolean isCheckLatestTestProgram = _project.isCheckLatestTestProgram();

                _project.setCheckLatestTestProgram(false);
                _opticalProgramGeneration.addPanelOpticalRegion(_project.getTestProgram(), _opticalRegion, _opticalCameraRectangle);
                _project.setCheckLatestTestProgram(isCheckLatestTestProgram);              
              }
              
              // Update inspectable flag
              if (_isLiveViewMode)
              {
                _opticalCameraRectangle.setInspectableBool(isSave);
              }
              else
              {
                BooleanRef isEnabled = new BooleanRef(isSave);
                _commandManager.trackState(new UndoState());
                _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_TOGGLE_OPTICAL_CAMERA_RECTANGLE_KEY"));
                try
                {
                  _commandManager.execute(new SurfaceMapPanelToggleOpticalCameraRectangleCommand(_opticalCameraRectangle, isEnabled));
                }
                catch (XrayTesterException ex)
                {
                  MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                    ex.getLocalizedMessage(),
                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                    true);
                }
                _commandManager.endCommandBlock();
              }
              
              // Update newly chosen surface map points
              editSurfaceMapPoints(_opticalRegion, _opticalCameraRectangle);
            }
            else
            {
              _project.getPanel().getPanelSurfaceMapSettings().removeSurfaceMapRegion(_opticalRegion);
              //_project.getPanel().getPanelSurfaceMapSettings().addOpticalRegion(_opticalRegion);

              // Special to handle multi-board but using panelAlignment
              if (_project.getPanel().isMultiBoardPanel())
              {
                if (_updateSurfaceMapData)
                {
                  for (Board board : _coveredBoards)
                  {
                    _opticalRegion.setBoard(board);
                  }
                }
              }
              else
              {
                _opticalRegion.setBoard(_board);
              }

              _opticalCameraRectangle.setInspectableBool(isSave);
              double zHeightInNanometer = 0;
              double totalZHeightInNanometer = 0;
              _opticalRegion.addOpticalCameraRectangle(_opticalCameraRectangle);
              
              if (isSave)
              {
                saveSurfaceMapPoints(_opticalCameraRectangle, _board);
              }
              boolean isCheckLatestTestProgram = _project.isCheckLatestTestProgram();
              _project.setCheckLatestTestProgram(false);
              _opticalProgramGeneration.addPanelOpticalRegion(_project.getTestProgram(), _opticalRegion, _opticalCameraRectangle);
              _project.setCheckLatestTestProgram(isCheckLatestTestProgram);
              
              processSurfaceMapPoints(_opticalCameraRectangle, true);
              totalZHeightInNanometer = getTotalZHeightInNanometer();
              int totalPointCount = _pspLiveVideoPanel.getAllSurfaceMapPoints().size();
              if (totalPointCount != 0)
                zHeightInNanometer = totalZHeightInNanometer / totalPointCount;
              
              _opticalCameraRectangle.setZHeightInNanometer(zHeightInNanometer);
              _opticalCameraRectangle.setInspectableBool(isSave);
              _opticalRegion.addOpticalCameraRectangle(_opticalCameraRectangle);
              _opticalCameraRectangle.setBoard(_board);
         
              if (_updateSurfaceMapData)
              {
                updateMeshTriangleData();
                for (com.axi.v810.business.panelDesc.Component component : _pspAutoPopulatePanel.getAllComponents())
                {
                  if (_pspAutoPopulatePanel.isComponentUsableInSurfaceMap(component))
                  {
                    component.getComponentType().setToUsePspResult(true);
                    _opticalRegion.addComponent(component);
                  
                  }        
                }
              }
              try
              {
                _commandManager.execute(new SurfaceMapPanelAddOpticalRegionCommand(_project, _panelSurfaceMapSettings, _opticalRegion));
              }
              catch (XrayTesterException ex)
              {
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  ex.getLocalizedMessage(),
                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                  true);
              }
            }
          }
          else // board-based
          {
            if (isEditMode())
            {
              // Before do anything, clear all the existing points
              _opticalCameraRectangle.clearPointPanelCoordinateList();

              _opticalRegion.setBoard(_board);
              _board.getBoardSurfaceMapSettings().addOpticalRegion(_opticalRegion);
              
              //_opticalCameraRectangle.setInspectableBool(isSave);
              _opticalRegion.addOpticalCameraRectangle(_opticalCameraRectangle);
              _opticalRegion.addBoardOpticalCameraRectangle(_board, _opticalCameraRectangle);

              boolean isCheckLatestTestProgram = _project.isCheckLatestTestProgram();
              _project.setCheckLatestTestProgram(false);

              _opticalProgramGeneration.addBoardOpticalRegion(_project.getTestProgram(), _opticalRegion, _board);
              _project.setCheckLatestTestProgram(isCheckLatestTestProgram);
              
              for (Board board : _pspAutoPopulatePanel.getBoardsToApplyOpticalCameraRectangles())
              {
                for (com.axi.v810.business.panelDesc.Component component : _pspAutoPopulatePanel.getBoardComponentList(board))
                {
                  if (_pspAutoPopulatePanel.isComponentUsableInSurfaceMap(component) && component.getBoard().equals(board))
                  {
                    component.getComponentType().setToUsePspResult(true);                    
                    if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
                      board.getBoardSurfaceMapSettings().getOpticalRegion().addComponent(board, component);
                  }
                }
              }
              
              // Update inspectable flag
              if (_isLiveViewMode)
              {
                _opticalCameraRectangle.setInspectableBool(isSave);
              }
              else
              {
                BooleanRef isEnabled = new BooleanRef(isSave);
                _commandManager.trackState(new UndoState());
                _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_TOGGLE_OPTICAL_CAMERA_RECTANGLE_KEY"));
                try
                {
                  _commandManager.execute(new SurfaceMapBoardToggleOpticalCameraRectangleCommand(_opticalCameraRectangle, isEnabled));
                }
                catch (XrayTesterException ex)
                {
                  MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                    ex.getLocalizedMessage(),
                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                    true);
                }
                _commandManager.endCommandBlock();
              }
              
              // Update newly chosen surface map points
              editSurfaceMapPoints(_opticalRegion, _opticalCameraRectangle);
            }
            else
            {
              _opticalRegion.setBoard(_board);
              //_board.getBoardSurfaceMapSettings().addOpticalRegion(_opticalRegion);
              double zHeightInNanometer = 0;
              double totalZHeightInNanometer = 0;
              
              _opticalCameraRectangle.setInspectableBool(isSave);
              if (isSave)
              {
                saveSurfaceMapPoints(_opticalCameraRectangle, _board);
              }
              _opticalRegion.addOpticalCameraRectangle(_opticalCameraRectangle);
              _opticalRegion.addBoardOpticalCameraRectangle(_board, _opticalCameraRectangle);
              
              boolean isCheckLatestTestProgram = _project.isCheckLatestTestProgram();
              _project.setCheckLatestTestProgram(false);
              
              _project.setCheckLatestTestProgram(isCheckLatestTestProgram);
              
              processSurfaceMapPoints(_opticalCameraRectangle, false);
              totalZHeightInNanometer = getTotalZHeightInNanometer();
              int totalPointCount = _pspLiveVideoPanel.getAllSurfaceMapPoints().size();
              if (totalPointCount != 0)
                zHeightInNanometer = totalZHeightInNanometer / totalPointCount;
              
              _opticalCameraRectangle.setZHeightInNanometer(zHeightInNanometer);
              _opticalCameraRectangle.setInspectableBool(isSave);
              _opticalRegion.addBoardOpticalCameraRectangle(_board, _opticalCameraRectangle);
              updateMeshTriangleData();
              
              if (_updateSurfaceMapData)
              {
                //updateMeshTriangleData();
                for (com.axi.v810.business.panelDesc.Component component : _pspAutoPopulatePanel.getBoardComponentList(_board))
                {
                  if (_pspAutoPopulatePanel.isComponentUsableInSurfaceMap(component) && component.getBoard().equals(_board))
                  {
                    component.getComponentType().setToUsePspResult(true);
                    _opticalRegion.addComponent(_board, component);
                  }
                }
              }
              try
              {
                _commandManager.execute(new SurfaceMapBoardAddOpticalRegionCommand(_project, _boardSurfaceMapSettings, _opticalRegion));
              }
              catch (XrayTesterException ex)
              {
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  ex.getLocalizedMessage(),
                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                  true);
              }
            }
          }
          
          // Invalidate checksum in order to force optical path generation
          _project.getTestProgram().invalidateOpticalCheckSum();
          _project.getTestProgram().invalidateAlignmentOpticalCheckSum();
        }
        finally
        {
          busyDialog.dispose();
        }
      }
    });
    busyDialog.setVisible(true);
  }

   /**
   * @author Jack Hwee
   * Save selected points for GSM
   */
  private void saveButton_actionPerformed(ActionEvent e)
  {
    try
    {
      //check projector connection
      checkProjectorConnectivity();
      
      // for Sentech Optical camera
      _abstractOpticalCamera.setStopLiveVideoBoolean(true);
      if (_inAlignmentSurfaceMap == false)
      {
        updateSurfaceMapData(true);
        if (_updateSurfaceMapData)
        _mainUI.getMainMenuBar().saveProject();
      }
      else
      {
        if (_isPanelSurfaceMap)
        {
          updateAlignmentSurfaceMapData(_board, true);
          //if (_updateSurfaceMapData)
          _mainUI.getMainMenuBar().saveProject();
        }
        else
        {
          for (Board board : _project.getPanel().getBoards())
          {
            updateAlignmentSurfaceMapData(board, true);
          }
          //if (_updateSurfaceMapData)
          _mainUI.getMainMenuBar().saveProject();
        }
      }
      //updateSurfaceMapData(true);
      //if (_updateSurfaceMapData)
      //  _mainUI.getMainMenuBar().saveProject();
      _surfaceMapping.runToNextRegion();
      clearPointsButton_actionPerformed(e);

      if(_simulationMode == false)
        endSurfaceMapping(true);
      else
        dispose();
    }
    catch (XrayTesterException xte)
    {
      try
      {
        exitOpticalCamera();
      }
      finally
      {
        dispose();
      }
      _exception = xte;
    }
  }

  /**
   * @author Jack Hwee
   */
  public void endSurfaceMapping(final boolean shouldDisposeDialog)
  {
    HardwareWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          PspImageAcquisitionEngine.getInstance().handlePostOpticalView(PspModuleEnum.FRONT, OpticalShiftNameEnum.WHITE_LIGHT);
        }
        catch (XrayTesterException xte)
        {
          _mainUI.handleXrayTesterException(xte);
        }
        finally
        {
          try
          {
            exitOpticalCamera();
          }
          finally
          {
            if (shouldDisposeDialog)
              dispose();
          }
        }
      }
    });
  }
  
  /**
   * @author Jack Hwee
   */
  public void addComponent(com.axi.v810.business.panelDesc.Component component)
  {
    Assert.expect(component != null);
    Assert.expect(_component != null);

    if (_component.contains(component) == false)
    {
      _component.add(component);
    }
  }
   
  /**
   * @author Jack Hwee
   */
  public Point2D convertVideoPixelToPanelCoordinate(int xCoordinateFromVideo, int yCoordinateFromVideo, int xCoordinateFromPanel, int yCoordinateFromPanel)
  {
    double zoomFactor = 0;
    if (_inAlignmentSurfaceMap == false)
    {
      _transformFromRendererCoords = _pspAutoPopulatePanel.getCurrentAffineTransformForOpticalRegion();
      if (_project.getPanel().getPanelSettings().isPanelBasedAlignment())
        zoomFactor = _pspAutoPopulatePanel.getPanelGraphicsEngine().getCurrentZoomFactor();
      else
        zoomFactor = _pspAutoPopulatePanel.getBoardGraphicsEngine().getCurrentZoomFactor();
    }
    else       
    {
      _transformFromRendererCoords = _surfaceMapping.getCurrentAffineTransformForOpticalRegion();    
      zoomFactor = _zoomFactor;
    }
    
    double xCoordinate;
    double yCoordinate;
    if (_project.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      xCoordinate = Math.round((xCoordinateFromPanel) + ((xCoordinateFromVideo * zoomFactor) / OpticalRegionGeneration.getCameraViewFactor()));   
      yCoordinate = Math.round((yCoordinateFromPanel) + ((yCoordinateFromVideo * zoomFactor) / OpticalRegionGeneration.getCameraViewFactor())) ;  
    }
    else
    {
      xCoordinate = Math.round((xCoordinateFromPanel) + ((xCoordinateFromVideo * zoomFactor) / OpticalRegionGeneration.getCameraViewFactor()));   
      yCoordinate = Math.round((yCoordinateFromPanel) + ((yCoordinateFromVideo * zoomFactor) / OpticalRegionGeneration.getCameraViewFactor())) ;  
    }
    Point point = new Point();
    point.setLocation(xCoordinate, yCoordinate);
    Point2D graphicCoordInNanoMeters =  convertFromPixelToRendererCoordinates((int) point.getX(), (int) point.getY());
    return graphicCoordInNanoMeters;
  }

  /**
   * Given x,y coordinates in pixels, return out a Point in Renderer coordinates.
   * @return a Point in Renderer coordinates transformed from pixel coordinates
   * @author Bill Darbie
   */
  public Point2D convertFromPixelToRendererCoordinates(int xInPixels, int yInPixels)
  {
    if (_inAlignmentSurfaceMap == false)
      _transformFromRendererCoords = _pspAutoPopulatePanel.getCurrentAffineTransformForOpticalRegion();
    else       
      _transformFromRendererCoords = _surfaceMapping.getCurrentAffineTransformForOpticalRegion();    
    
    Assert.expect(_transformFromRendererCoords != null);

    Point2D point = new Point2D.Double(xInPixels, yInPixels);
    AffineTransform inverse = null;
    try
    {
      inverse = _transformFromRendererCoords.createInverse();
    }
    catch (NoninvertibleTransformException ex)
    {
      ex.printStackTrace();
      Assert.expect(false);
    }
   
    inverse.transform(point, point);
 
    return point;
  }
  
  /**
   * @author Jack Hwee
   */
  public Point2D calculateOffsetForOtherBoard(Board board, Rectangle rect)
  {
    Assert.expect(board != null);
    Assert.expect(rect != null);

    TestSubProgram alignmentTestSubProgram = null;
    TestSubProgram prevAlignmentTestSubProgram = null;
    Board firstBoard = _project.getPanel().getBoards().get(0);
    if (_project.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      for (TestSubProgram testSubProgram : _project.getTestProgram().getAllInspectableTestSubPrograms())
      {
        if (testSubProgram.isSubProgramPerformAlignment())
        {
          alignmentTestSubProgram = testSubProgram;
          prevAlignmentTestSubProgram = testSubProgram;
          break;
        }
      }
    }
    else
    {
      alignmentTestSubProgram = _project.getTestProgram().getTestSubProgram(board);
      prevAlignmentTestSubProgram = _project.getTestProgram().getTestSubProgram(firstBoard);
    }
    Assert.expect(alignmentTestSubProgram != null);
    Assert.expect(prevAlignmentTestSubProgram != null);

    java.util.List<PanelRectangle> allowableAlignmentRegionInNanoMeters = new ArrayList<PanelRectangle>();
    java.util.List<AlignmentGroup> alignmentGroupNames = new ArrayList<AlignmentGroup>();
    Point2D point = new Point2D.Double(0, 0);

    if (_project.getPanel().getPanelSettings().isLongPanel() && _project.getPanel().getBoards().get(0).isLongBoard())
    {
      java.util.List<PanelRectangle> previousAllowableAlignmentRegionInNanoMeters = new ArrayList<PanelRectangle>();
      for (AlignmentGroup alignmentGroup : _project.getTestProgram().getAlignmentGroups())
      {
        if (alignmentGroup.getPads().get(0).getComponent().getBoard().equals(board))
        {
          allowableAlignmentRegionInNanoMeters.add(_project.getTestProgram().getAlignmentRegion(alignmentGroup).getAllowableAlignmentRegionInNanoMeters());
          alignmentGroupNames.add(alignmentGroup);
        }
        if (alignmentGroup.getPads().get(0).getComponent().getBoard().equals(firstBoard))
        {
          previousAllowableAlignmentRegionInNanoMeters.add(_project.getTestProgram().getAlignmentRegion(alignmentGroup).getAllowableAlignmentRegionInNanoMeters());
        }
      }

      Point2D pointInPixels = _surfaceMapping.convertFromPixelToRendererCoordinates((int) rect.getCenterX(), (int) rect.getCenterY());
     
      Rectangle region1 = allowableAlignmentRegionInNanoMeters.get(0).getRectangle2D().getBounds();
      Rectangle region2 = allowableAlignmentRegionInNanoMeters.get(1).getRectangle2D().getBounds();
      Rectangle region3 = allowableAlignmentRegionInNanoMeters.get(2).getRectangle2D().getBounds();
      Rectangle region4 = allowableAlignmentRegionInNanoMeters.get(3).getRectangle2D().getBounds();
      Rectangle region5 = allowableAlignmentRegionInNanoMeters.get(4).getRectangle2D().getBounds();
      Rectangle region6 = allowableAlignmentRegionInNanoMeters.get(5).getRectangle2D().getBounds();

      AlignmentGroup alignmentGroup1 = alignmentGroupNames.get(0);
      AlignmentGroup alignmentGroup2 = alignmentGroupNames.get(1);
      AlignmentGroup alignmentGroup3 = alignmentGroupNames.get(2);
      AlignmentGroup alignmentGroup4 = alignmentGroupNames.get(3);
      AlignmentGroup alignmentGroup5 = alignmentGroupNames.get(4);
      AlignmentGroup alignmentGroup6 = alignmentGroupNames.get(5);

      Rectangle previousRegion1 = previousAllowableAlignmentRegionInNanoMeters.get(0).getRectangle2D().getBounds();
      Rectangle previousRegion2 = previousAllowableAlignmentRegionInNanoMeters.get(1).getRectangle2D().getBounds();
      Rectangle previousRegion3 = previousAllowableAlignmentRegionInNanoMeters.get(2).getRectangle2D().getBounds();
      Rectangle previousRegion4 = previousAllowableAlignmentRegionInNanoMeters.get(3).getRectangle2D().getBounds();
      Rectangle previousRegion5 = previousAllowableAlignmentRegionInNanoMeters.get(4).getRectangle2D().getBounds();
      Rectangle previousRegion6 = previousAllowableAlignmentRegionInNanoMeters.get(5).getRectangle2D().getBounds();

      if (previousRegion1.contains(pointInPixels))
      {
        int xOffset = (int) region1.getMinX() - (int) previousRegion1.getMinX();
        int yOffset = (int) region1.getMinY() - (int) previousRegion1.getMinY();

        point.setLocation(xOffset, yOffset);
        _currentAlignmentGroupForSurfaceMap = alignmentGroup1.getName();
      }
      else if (previousRegion2.contains(pointInPixels))
      {
        int xOffset = (int) region2.getMinX() - (int) previousRegion2.getMinX();
        int yOffset = (int) region2.getMinY() - (int) previousRegion2.getMinY();

        point.setLocation(xOffset, yOffset);
        _currentAlignmentGroupForSurfaceMap = alignmentGroup2.getName();
      }
      else if (previousRegion3.contains(pointInPixels))
      {
        int xOffset = (int) region3.getMinX() - (int) previousRegion3.getMinX();
        int yOffset = (int) region3.getMinY() - (int) previousRegion3.getMinY();

        point.setLocation(xOffset, yOffset);
        _currentAlignmentGroupForSurfaceMap = alignmentGroup3.getName();
      }
      else if (previousRegion4.contains(pointInPixels))
      {
        int xOffset = (int) region4.getMinX() - (int) previousRegion4.getMinX();
        int yOffset = (int) region4.getMinY() - (int) previousRegion4.getMinY();

        point.setLocation(xOffset, yOffset);
        _currentAlignmentGroupForSurfaceMap = alignmentGroup4.getName();
      }
      else if (previousRegion5.contains(pointInPixels))
      {
        int xOffset = (int) region5.getMinX() - (int) previousRegion5.getMinX();
        int yOffset = (int) region5.getMinY() - (int) previousRegion5.getMinY();

        point.setLocation(xOffset, yOffset);
        _currentAlignmentGroupForSurfaceMap = alignmentGroup5.getName();
      }
      else if (previousRegion6.contains(pointInPixels))
      {
        int xOffset = (int) region6.getMinX() - (int) previousRegion6.getMinX();
        int yOffset = (int) region6.getMinY() - (int) previousRegion6.getMinY();

        point.setLocation(xOffset, yOffset);
        _currentAlignmentGroupForSurfaceMap = alignmentGroup6.getName();
      }
      previousAllowableAlignmentRegionInNanoMeters.clear();
    }
    else
    {
      for (ReconstructionRegion alignmentRegion : alignmentTestSubProgram.getSortedAlignmentRegions())
      {
        allowableAlignmentRegionInNanoMeters.add(alignmentRegion.getAllowableAlignmentRegionInNanoMeters());
        alignmentGroupNames.add(alignmentRegion.getAlignmentGroup());
      }

      java.util.List<PanelRectangle> previousAllowableAlignmentRegionInNanoMeters = new ArrayList<PanelRectangle>();
      for (ReconstructionRegion alignmentRegion : prevAlignmentTestSubProgram.getSortedAlignmentRegions())
      {
        previousAllowableAlignmentRegionInNanoMeters.add(alignmentRegion.getAllowableAlignmentRegionInNanoMeters());
      }

      Point2D pointInPixels = _surfaceMapping.convertFromPixelToRendererCoordinates((int) rect.getCenterX(), (int) rect.getCenterY());
     
      Rectangle region1 = allowableAlignmentRegionInNanoMeters.get(0).getRectangle2D().getBounds();
      Rectangle region2 = allowableAlignmentRegionInNanoMeters.get(1).getRectangle2D().getBounds();
      Rectangle region3 = allowableAlignmentRegionInNanoMeters.get(2).getRectangle2D().getBounds();

      AlignmentGroup alignmentGroup1 = alignmentGroupNames.get(0);
      AlignmentGroup alignmentGroup2 = alignmentGroupNames.get(1);
      AlignmentGroup alignmentGroup3 = alignmentGroupNames.get(2);

      Rectangle previousRegion1 = previousAllowableAlignmentRegionInNanoMeters.get(0).getRectangle2D().getBounds();
      Rectangle previousRegion2 = previousAllowableAlignmentRegionInNanoMeters.get(1).getRectangle2D().getBounds();
      Rectangle previousRegion3 = previousAllowableAlignmentRegionInNanoMeters.get(2).getRectangle2D().getBounds();

      //Point2D point = new Point2D.Double(0, 0);

      if (previousRegion1.contains(pointInPixels))
      {
        int xOffset = (int) region1.getMinX() - (int) previousRegion1.getMinX();
        int yOffset = (int) region1.getMinY() - (int) previousRegion1.getMinY();

        point.setLocation(xOffset, yOffset);
        _currentAlignmentGroupForSurfaceMap = alignmentGroup1.getName();
      }
      else if (previousRegion2.contains(pointInPixels))
      {
        int xOffset = (int) region2.getMinX() - (int) previousRegion2.getMinX();
        int yOffset = (int) region2.getMinY() - (int) previousRegion2.getMinY();

        point.setLocation(xOffset, yOffset);
        _currentAlignmentGroupForSurfaceMap = alignmentGroup2.getName();
      }
      else if (previousRegion3.contains(pointInPixels))
      {
        int xOffset = (int) region3.getMinX() - (int) previousRegion3.getMinX();
        int yOffset = (int) region3.getMinY() - (int) previousRegion3.getMinY();

        point.setLocation(xOffset, yOffset);
        _currentAlignmentGroupForSurfaceMap = alignmentGroup3.getName();
      }
      previousAllowableAlignmentRegionInNanoMeters.clear();
    }
    allowableAlignmentRegionInNanoMeters.clear();
    alignmentGroupNames.clear();
    return point;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void updateXYLocationLabels()
  {
    clearPointsLocationLabels();
    AbstractSurfaceMapSectionPanel surfaceMapImageSection = _pspLiveVideoPanel.getCurrentEnabledSurfaceMapSection();
    if (surfaceMapImageSection != null)
    {
      for (int i = 0; i < surfaceMapImageSection.getSurfaceMapPoints().size(); ++i)
      {
        SurfaceMapPointPanel surfaceMapPoint = surfaceMapImageSection.getSurfaceMapPoints().get(i);
        JLabel pointsLocationLabel = getSurfaceMapPointLocationLabel(i);
        pointsLocationLabel.setText("Point " + (i+1) + ": X:" + surfaceMapPoint.getPositionInImageInPixel().x + "  " + 
                                    "Y:" + surfaceMapPoint.getPositionInImageInPixel().y);
        pointsLocationLabel.setForeground(surfaceMapPoint.getColour());
        _saveButton.setEnabled(true);
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng
   * @author Ying-Huan.Chu
   */
  private void setupSurfaceMapPoints()
  {
    Assert.expect(_opticalCameraRectangle != null);
    
    if (_panelCoordinateToNonAdjustedPointMap != null) _panelCoordinateToNonAdjustedPointMap.clear();
    if (_panelCoordinateToAdjustedPointMap != null) _panelCoordinateToAdjustedPointMap.clear();
    
    int opticalFiducialStageCoordinateLocationXInNanometers = -1;
    int opticalFiducialStageCoordinateLocationYInNanometers = -1;
    if (_surfaceMapping.getPspModuleEnum().equals(PspModuleEnum.FRONT))
    {
      opticalFiducialStageCoordinateLocationXInNanometers = PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers();
      opticalFiducialStageCoordinateLocationYInNanometers = PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers();
    }
    else
    {
      opticalFiducialStageCoordinateLocationXInNanometers = PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers();
      opticalFiducialStageCoordinateLocationYInNanometers = PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers();
    }
    
    java.util.List<PanelCoordinate> invalidPointPanelCoordinates = new ArrayList<>();
    for (PanelCoordinate pointPanelCoordinate : _opticalCameraRectangle.getPanelCoordinateList())
    {
      // Get non-adjusted pixel
      Point nonAdjustedPixelCoordinate = OpticalMechanicalConversions.convertPanelCoordinateInNanometerToNonAdjustedPixel(_opticalCameraRectangle, pointPanelCoordinate);
      
      // Get adjusted pixel
      Point adjustedPixelCoordinate = OpticalMechanicalConversions.convertPanelCoordinateInNanometerToAdjustedPixel(_surfaceMapTestSubProgram,
                                                                                                                    _opticalCameraRectangle, 
                                                                                                                    pointPanelCoordinate,
                                                                                                                    _surfaceMapTestSubProgram.getAggregateRuntimeAlignmentTransform(),
                                                                                                                    PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationXInNanometers(),
                                                                                                                    PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationYInNanometers(),
                                                                                                                    opticalFiducialStageCoordinateLocationXInNanometers,
                                                                                                                    opticalFiducialStageCoordinateLocationYInNanometers);
      int xInPixel = (int)(adjustedPixelCoordinate.getX());
      int yInPixel = (int)(adjustedPixelCoordinate.getY());
      
      if (xInPixel >= 0 && 
          yInPixel >= 0 && 
          xInPixel <= PspHardwareEngine.getInstance().getImageWidth() &&  
          yInPixel <= PspHardwareEngine.getInstance().getImageHeight())
      {
        _pointPositionInPixelToZHeightInNanometerMap.put(new Point(xInPixel, yInPixel), 0.0);
        _pspLiveVideoPanel.setupSurfaceMapPoint(xInPixel, yInPixel);
        
        if (_panelCoordinateToNonAdjustedPointMap.containsKey(pointPanelCoordinate) == false)
          _panelCoordinateToNonAdjustedPointMap.put(pointPanelCoordinate, nonAdjustedPixelCoordinate);
        
        if (_panelCoordinateToAdjustedPointMap.containsKey(pointPanelCoordinate) == false)
          _panelCoordinateToAdjustedPointMap.put(pointPanelCoordinate, adjustedPixelCoordinate);
      }
      else
        invalidPointPanelCoordinates.add(pointPanelCoordinate);
    }
    _opticalCameraRectangle.removePointPanelCoordinates(invalidPointPanelCoordinates);
    
    updateXYLocationLabels();
    
    if (_pspLiveVideoPanel.getAllSurfaceMapPoints().isEmpty() == false)
      _saveButton.setEnabled(true);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private javax.swing.JLabel getSurfaceMapPointLocationLabel(int index)
  {
    if (index == 0)
      return _pointsLocationLabel1;
    else if (index == 1)
      return _pointsLocationLabel2;
    else if (index == 2)
      return _pointsLocationLabel3;
    else if (index == 3)
      return _pointsLocationLabel4;
    else if (index == 4)
      return _pointsLocationLabel5;
    else if (index == 5)
      return _pointsLocationLabel6;
    else
      Assert.expect(false, "Invalid surface map point location label index");
    
    // Default return PointLocationLabel1
    return _pointsLocationLabel1;
  }
  
  /**
   * @author Cheah Lee Herng
   * @author Ying-Huan.Chu
   */
  private boolean isEditMode()
  {
    if (_isPanelSurfaceMap)
    {
      if (_panelSurfaceMapSettings != null)
      {
        if (_panelSurfaceMapSettings.hasOpticalRegion())
        {
          for (OpticalRegion opticalRegion : _panelSurfaceMapSettings.getOpticalRegions())
          {
            if (opticalRegion.getOpticalCameraRectangle(_opticalCameraRectangle.getName()) != null)
            {
              // we check if the OpticalRegion contains the OpticalCameraRectangle. If yes, it means we are currently in Edit Mode.
              return true;
            }
          }
        }
      }
    }
    else // board-based
    {
      if (_boardSurfaceMapSettings != null)
      {
        if (_boardSurfaceMapSettings.hasOpticalRegion())
        {
          for (OpticalRegion opticalRegion : _boardSurfaceMapSettings.getOpticalRegions())
          {
            if (opticalRegion.getOpticalCameraRectangle(_boardSurfaceMapSettings.getBoard(), _opticalCameraRectangle.getName()) != null)
            {
              return true;
            }
          }
        }
      }
    }
    return false;
  }
  
  /**
   * Re-add point position to OpticalCameraRectangle which is 
   * decided by the count parameter.
   * 
   * @author Cheah Lee Herng 
   */
  private void editSurfaceMapPoints(OpticalRegion opticalRegion, 
                                    OpticalCameraRectangle opticalCameraRectangle)
  {
    Assert.expect(opticalRegion != null);
    Assert.expect(opticalCameraRectangle != null);
    
    double zHeightInNanometer = 0;
    double totalZHeightInNanometer = 0;
    if (_project == null)
    {
      _project = Project.getCurrentlyLoadedProject();
    }
    boolean isPanelBased = _project.getPanel().getPanelSettings().isPanelBasedAlignment();
    Board board = _project.getPanel().getBoards().get(0);
    if (isPanelBased == false)
    {
      board = _project.getPanel().getBoard(opticalCameraRectangle.getBoard().getName());
    }
    if (_pspLiveVideoPanel.getAllSurfaceMapPoints().size() > 0)
    {
      saveSurfaceMapPoints(opticalCameraRectangle, _board);
    }
    processSurfaceMapPoints(opticalCameraRectangle, isPanelBased);
    totalZHeightInNanometer = getTotalZHeightInNanometer();
    
    int totalPointCount = _pspLiveVideoPanel.getAllSurfaceMapPoints().size();
    if (totalPointCount != 0)
      zHeightInNanometer = totalZHeightInNanometer / totalPointCount;
    
    opticalCameraRectangle.setZHeightInNanometer(zHeightInNanometer);
    if (isPanelBased == false)
      _opticalRegion.addOpticalCameraRectangle(opticalCameraRectangle);
    else
      _opticalRegion.addBoardOpticalCameraRectangle(board, opticalCameraRectangle);
  }
  
  /**
   * Common function which calls addSurfaceMapPoints for Panel-based / Board-based.
   * @author Ying-Huan.Chu
   */
  private void addSurfaceMapPoints(OpticalRegion opticalRegion, 
                                   Board board,
                                   Point2D point,
                                   boolean isVerified,
                                   boolean isPanelBased)
  {
    if (isPanelBased)
    {
      opticalRegion.addSurfaceMapPoints((int)point.getX(), (int)point.getY(), isVerified);
    }
    else // Board-based
    {
      opticalRegion.addSurfaceMapPoints(board, (int)point.getX(), (int)point.getY());
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setupDialogProperty()
  {
    _width = PspHardwareEngine.getInstance().getImageWidth();
    _height = PspHardwareEngine.getInstance().getImageHeight();
    
    boolean isLargerFov = _abstractOpticalCamera.isLargerFov();
    int imageWidthInPixel = PspHardwareEngine.getInstance().getImageWidth();
    int imageHeightInPixel = PspHardwareEngine.getInstance().getImageHeight();
    int imageActualWidthInNanometer = isLargerFov? PspHardwareEngine.getInstance().getLargerFovActualImageWidthInNanometer() : PspHardwareEngine.getInstance().getActualImageWidthInNanometer();
    int imageActualHeightInNanometer = isLargerFov? PspHardwareEngine.getInstance().getLargerFovActualImageHeightInNanometer() : PspHardwareEngine.getInstance().getActualImageHeightInNanometer(); 
    _dragDistanceXPerPixelInNanometer = imageActualWidthInNanometer / imageWidthInPixel;
    _dragDistanceYPerPixelInNanometer = imageActualHeightInNanometer / imageHeightInPixel;
    _maximumAllowableRegion = getMaximumAllowableRegion();
  }
  
  /**
   * When call this function, a BusyCancelDialog will pop up. This is so that to
   * make sure user does not feel the system is hang when system is actually
   * performing Surface Map update.
   *
   * @author Jack Hwee
   */
  private void updateAlignmentSurfaceMapData(final Board board, final boolean isSave)
  {
//    final BusyCancelDialog busyDialog = new BusyCancelDialog(
//            (Frame) getOwner(),
//            StringLocalizer.keyToString("PSP_UPDATE_DATA_KEY"),
//            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
//            true);
//    busyDialog.pack();
//    SwingUtils.centerOnComponent(busyDialog, (Frame) getOwner());
//    SwingWorkerThread.getInstance().invokeLater(new Runnable()
//    {
//      public void run()
//      {
//        try
//        {
//          if (_isPanelSurfaceMap)
//          {
//            _project.getPanel().getPanelAlignmentSurfaceMapSettings().removeOpticalRegion(_currentAlignmentGroupForSurfaceMap, _opticalRegion);
//            _project.getPanel().getPanelAlignmentSurfaceMapSettings().addOpticalRegion(_currentAlignmentGroupForSurfaceMap, _opticalRegion);
//
//            String regionPositionName = _opticalRegion.getRegion().getMinX() + "_" + _opticalRegion.getRegion().getMinY() + "_" + _opticalRegion.getRegion().getWidth() + "_" + _opticalRegion.getRegion().getHeight();
//
//            _board = board;
//
//            _opticalRegion.setAlignmentGroupName(_currentAlignmentGroupForSurfaceMap);
//            _opticalRegion.setBoard(_board);
//
//            double zHeightInNanometer = 0;
//            double totalZHeightInNanometer = 0;
//            PanelAlignmentSurfaceMapSettings panelAlignmentSurfaceMapSettings = _project.getPanel().getPanelAlignmentSurfaceMapSettings();
//            
//            _opticalCameraRectangle.setInspectableBool(isSave);
//            _opticalRegion.addOpticalCameraRectangle(_opticalCameraRectangle);
//            
//            _project.getTestProgram().addAlignmentOpticalRegion(_opticalRegion);
//            
//            processSurfaceMapPoints(_opticalCameraRectangle, true);
//            totalZHeightInNanometer = getTotalZHeightInNanometer();
//            int totalPointCount = _pspLiveVideoPanel.getAllSurfaceMapPoints().size();
//            if (totalPointCount != 0)
//              zHeightInNanometer = totalZHeightInNanometer / totalPointCount;
//            
//            _opticalCameraRectangle.setZHeightInNanometer(zHeightInNanometer);
//            _opticalRegion.addOpticalCameraRectangle(_opticalCameraRectangle);
//          }
//          else // board-based
//          {
//            OpticalRegion opticalRegion = null;
//            BoardAlignmentSurfaceMapSettings boardAlignmentSurfaceMapSettings = board.getBoardAlignmentSurfaceMapSettings();
//            Point2D opticalRegionOffsetPoint = calculateOffsetForOtherBoard(board, rect);
//            if (boardAlignmentSurfaceMapSettings.getOpticalRegions().isEmpty() == false
//                    && boardAlignmentSurfaceMapSettings.isAlignmentOpticalRegionExists(_currentAlignmentGroupForSurfaceMap))
//            {
//              opticalRegion = board.getBoardAlignmentSurfaceMapSettings().getAlignmentOpticalRegion(_currentAlignmentGroupForSurfaceMap);
//            }
//            else
//            {
//              opticalRegion = new OpticalRegion();
//              opticalRegion.setAlignmentGroupName(_currentAlignmentGroupForSurfaceMap);
//              opticalRegion.setBoard(board);
//              opticalRegion.setRegion(_opticalRegion.getRegion().getMinX() + (int) opticalRegionOffsetPoint.getX(), _opticalRegion.getRegion().getMinY() + (int) opticalRegionOffsetPoint.getY(), _opticalRegion.getRegion().getWidth(), _opticalRegion.getRegion().getHeight());
//            }
//            double zHeightInNanometer = 0;
//            double totalZHeightInNanometer = 0;
//
//            boardAlignmentSurfaceMapSettings.addOpticalRegion(_currentAlignmentGroupForSurfaceMap, opticalRegion);
//
//            String regionPositionName = "Board_" + _currentAlignmentGroupForSurfaceMap + "_" + opticalRegion.getRegion().getMinX() + "_" + opticalRegion.getRegion().getMinY() + "_" + opticalRegion.getRegion().getWidth() + "_" + opticalRegion.getRegion().getHeight();
//
//            _opticalCameraRectangle.setInspectableBool(isSave);
//            opticalRegion.addOpticalCameraRectangle(_opticalCameraRectangle);
//            
//            _project.getTestProgram().addAlignmentOpticalRegion(opticalRegion, board);
//
//            processSurfaceMapPoints(_opticalCameraRectangle, false);
//            totalZHeightInNanometer = getTotalZHeightInNanometer();
//
//            int totalPointCount = _pspLiveVideoPanel.getAllSurfaceMapPoints().size();
//            if (totalPointCount != 0)
//              zHeightInNanometer = totalZHeightInNanometer / totalPointCount;
//            
//            _opticalCameraRectangle.setZHeightInNanometer(zHeightInNanometer);
//            opticalRegion.addBoardOpticalCameraRectangle(board, _opticalCameraRectangle);
//          }
//
//        }
//        finally
//        {
//          busyDialog.dispose();
//        }
//      }
//    });
//    busyDialog.setVisible(true);
  }
   
  /**
   * 
   * @author Jack Hwee
   */
  private void updateMeshTriangleData()
  {
    if (_isPanelSurfaceMap)
    {
      if (_project.getPanel().hasPanelMeshSettings())
        _project.getPanel().getPanelMeshSettings().clear();
      java.util.List<MeshTriangle> meshTriangleList = MeshUtil.getInstance().createMesh(_opticalRegion.getEnabledOpticalCameraRectangles());
      _opticalRegion.clearAllMeshTriangles();
      _project.getPanel().getPanelMeshSettings().clear();
      for (MeshTriangle meshTriangle: meshTriangleList)
      {
        _project.getPanel().getPanelMeshSettings().addMeshTriangle(meshTriangle);
        _opticalRegion.addMeshTriangle(meshTriangle);
      }
    }
    else // Board-based
    {     
      if (_board.hasBoardMeshSettings())
        _board.getBoardMeshSettings().clear();
      java.util.List<MeshTriangle> meshTriangleList = MeshUtil.getInstance().createMesh(_opticalRegion.getEnabledBoardOpticalCameraRectangles(_board));
      _opticalRegion.clearAllMeshTriangles();
      _board.getBoardMeshSettings().clear();
      for (MeshTriangle meshTriangle: meshTriangleList)
      {
        _board.getBoardMeshSettings().addMeshTriangle(meshTriangle);  
        _opticalRegion.addMeshTriangle(meshTriangle);
      }
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   * @author Cheah Lee Herng
   */
  private void processSurfaceMapPoints(OpticalCameraRectangle opticalCameraRectangle, boolean isPanelBased)
  {
    java.util.List<PanelCoordinate> unModifiedPixelPoint = new ArrayList<PanelCoordinate>();
    java.util.List<SurfaceMapPointPanel> modifiedPixelPoint = new ArrayList<SurfaceMapPointPanel>();
    
    for (SurfaceMapPointPanel surfaceMapPoint : _pspLiveVideoPanel.getAllSurfaceMapPoints())
    {
      boolean isModified = true;
      
      double x = surfaceMapPoint.getPositionInImageInPixel().getX();
      double y = surfaceMapPoint.getPositionInImageInPixel().getY();
      
      // Look-up for modified pixel
      if (_panelCoordinateToAdjustedPointMap != null)
      {
        for(Map.Entry<PanelCoordinate, Point> entry : _panelCoordinateToAdjustedPointMap.entrySet())
        {
          Point adjustedPoint = entry.getValue();
          
          if (x == adjustedPoint.getX() && y == adjustedPoint.getY())
          {
            unModifiedPixelPoint.add(entry.getKey());
            isModified = false;
            break;
          }
        }
        
        if (isModified)        
          modifiedPixelPoint.add(surfaceMapPoint);
      }
      else              
        modifiedPixelPoint.add(surfaceMapPoint);
    }
    
    // Sanity check
    Assert.expect(_pspLiveVideoPanel.getAllSurfaceMapPoints().size() == (modifiedPixelPoint.size() + unModifiedPixelPoint.size()));
    
    // Add modified pixel
    for(SurfaceMapPointPanel surfaceMapPoint : modifiedPixelPoint)
    {
      Point2D point = OpticalMechanicalConversions.convertPixelCoordinateToPanelCoordinateInNanometer(surfaceMapPoint.getPositionInImageInPixel().getX(), 
                                                                                                      surfaceMapPoint.getPositionInImageInPixel().getY(), 
                                                                                                      opticalCameraRectangle);
      addSurfaceMapPoints(_opticalRegion, _board, point, true, isPanelBased);
      opticalCameraRectangle.addPointPanelCoordinate((int) point.getX(), (int) point.getY());
    }
    
    // Add un-modified pixel
    for(PanelCoordinate panelCoordinate : unModifiedPixelPoint)
    {
      if (_panelCoordinateToNonAdjustedPointMap.containsKey(panelCoordinate))
      {
        Point2D point = new Point(panelCoordinate.getX(), panelCoordinate.getY());
        addSurfaceMapPoints(_opticalRegion, _board, point, true, isPanelBased);
        opticalCameraRectangle.addPointPanelCoordinate((int) point.getX(), (int) point.getY());
      }
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private double getTotalZHeightInNanometer()
  {
    double totalZHeightInNanometer = 0;
    for (SurfaceMapPointPanel surfaceMapPoint : _pspLiveVideoPanel.getAllSurfaceMapPoints())
    {
      totalZHeightInNanometer += _pointPositionInPixelToZHeightInNanometerMap.get(surfaceMapPoint.getPositionInImageInPixel());
    }
    return totalZHeightInNanometer;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean isPreviousRectangle()
  {
    return _goToPreviousRectangle;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setPreviousRectangle(boolean flag)
  {
    _goToPreviousRectangle = flag;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void setupWindowHandle() throws XrayTesterException
  {
    Assert.expect(_abstractOpticalCamera != null);

    int windowHandle = _abstractOpticalCamera.getWindowHandle(_pspLiveVideoPanel);      
    _pspLiveVideoPanel.setParentWindowHandle(windowHandle);
  }
  
  /**
   * @author Cheah Lee Herng
   * @author Ying-Huan.Chu
   */
  private void startOpticalLiveVideo()
  {
    Assert.expect(_abstractOpticalCamera != null);
    
    _pspLiveVideoPanel.startVideoMode();
    if (_simulationMode == false)
    {
      HardwareWorkerThread.getInstance().invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            PspModuleEnum moduleEnum = (_abstractOpticalCamera.getOpticalCameraIdEnum().equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1))? PspModuleEnum.FRONT : PspModuleEnum.REAR;
            PspImageAcquisitionEngine.getInstance().handleActualOpticalView(moduleEnum, OpticalShiftNameEnum.WHITE_LIGHT);
            adjustBrightness(_project.getPanel().getPspSettings().getCurrentSetting());
            _abstractOpticalCamera.setStopLiveVideoBoolean(false);
            _abstractOpticalCamera.displayLiveVideo();
          }
          catch (XrayTesterException ex)
          {
            _exception = ex;
            dispose();
          }
        }
      });
    }
  }
  
  /**
   * @author Cheah Lee Herng
   * @author Ying-Huan.Chu
   */
  public void exitOpticalCamera()
  {
    if (_simulationMode == false)
    {
      HardwareWorkerThread.getInstance().invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _abstractOpticalCamera.setStopLiveVideoBoolean(true);
            _abstractOpticalCamera.exitCamera();
          }
          catch (XrayTesterException ex)
          {
            _mainUI.handleXrayTesterException(ex);
          }
        }
      });
    }
  }
  
  /**
   * This function is called whenever user switches from Live View mode to Z-Height mode.
   * @author Ying-Huan.Chu
   */
  private void doRegionSurfaceMapping(final OpticalCameraRectangle opticalCameraRectangle, final Board board)
  {
    final BusyCancelDialog busyDialog = new BusyCancelDialog(
                          _mainUI,
                          StringLocalizer.keyToString("PSP_UPDATE_DATA_KEY"),
                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                          true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, _mainUI);
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          try
          {
            _testExecution.doRegionSurfaceMapping(opticalCameraRectangle, board);
            _isSurfaceMappingSuccess = true;
          }
          catch (XrayTesterException ex)
          {
            _mainUI.handleXrayTesterException(ex);
            _isSurfaceMappingSuccess = false;
          }

          if (_isSurfaceMappingSuccess && _simulationMode == false)
          {
            _isSurfaceMappingSuccess = false;
            AbstractOpticalCamera abstractOpticalCamera;
            if (_surfaceMapping.getPspModuleEnum().equals(PspModuleEnum.FRONT))
              abstractOpticalCamera = OpticalCameraArray.getCamera(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
            else
              abstractOpticalCamera = OpticalCameraArray.getCamera(OpticalCameraIdEnum.OPTICAL_CAMERA_2);

            int waitedTimeInMilliseconds = 0;
            _isSurfaceMappingSuccess = abstractOpticalCamera.isHeightMapValueAvailable();
            while(_isSurfaceMappingSuccess == false && waitedTimeInMilliseconds < WAITING_FOR_HEIGHT_MAP_MAXIMUM_DURATION_IN_MILLISECONDS)
            {
              try
              {
                Thread.sleep(100);  
                waitedTimeInMilliseconds += 100;
              }
              catch (InterruptedException ix)
              {
                // don't expect this, but don't really care how sleep returns either
              }
              _isSurfaceMappingSuccess = abstractOpticalCamera.isHeightMapValueAvailable();
            }
          }
        }
        finally
        {
          if (_abstractOpticalCamera.isHeightMapValueAvailable())
          {
            _pspLiveVideoPanel.setHeightMapArray(_abstractOpticalCamera.getHeightMapValue(), _width, _height);
          }
          //Jack Hwee - display image instead of live view
          String imageFullPath = FileName.getAlignmentOpticalImageFullPath("temp", 0);
          java.awt.image.BufferedImage myPicture = null;
          if (_simulationMode == false)
          {
            if (FileUtil.exists(imageFullPath))
            {
              try
              {
                myPicture = javax.imageio.ImageIO.read(new java.io.File(imageFullPath));
              }
              catch (IOException ex)
              {
                ex.printStackTrace();
              }
            }
          }
          
          _pspLiveVideoPanel.setPreferredSize(new Dimension(_width, _height));
          _pspLiveVideoPanel.setBounds(0, 0, _width, _height);
          _pspLiveVideoPanel.startImageMode(myPicture);

          pack();
          busyDialog.dispose();
        }
      }
    });
    busyDialog.setVisible(true);
  }
  
  /**
   * @author Jack Hwee
   */
  private void saveSurfaceMapPoints(final OpticalCameraRectangle opticalCameraRectangle, final Board board)
  {
    Assert.expect(opticalCameraRectangle != null);
    Assert.expect(board != null);
    
    // do surface mapping only when it's currently displaying Live View mode.
    if (_displayLiveViewRadioButton.isSelected())
    {
      try
      {
        _testExecution.doRegionSurfaceMapping(opticalCameraRectangle, board);
        _isSurfaceMappingSuccess = true;
        if (_isSurfaceMappingSuccess && _simulationMode == false)
        {
          // wait for height map
          int waitedTimeInMilliseconds = 0;
          while(_abstractOpticalCamera.isHeightMapValueAvailable() == false && waitedTimeInMilliseconds < WAITING_FOR_HEIGHT_MAP_MAXIMUM_DURATION_IN_MILLISECONDS)
          {
            try
            {
              Thread.sleep(100);  
              waitedTimeInMilliseconds += 100;
            }
            catch (InterruptedException ix)
            {
              // don't expect this, but don't really care how sleep returns either
            }
          }
        }
      }
      catch (XrayTesterException ex)
      {
        _mainUI.handleXrayTesterException(ex);
        _isSurfaceMappingSuccess = false;
      }
    }
    if (_abstractOpticalCamera.isHeightMapValueAvailable())
    {
      _pspLiveVideoPanel.setHeightMapArray(_abstractOpticalCamera.getHeightMapValue(), _width, _height);
      _pointPositionInPixelToZHeightInNanometerMap.clear();
      for (SurfaceMapPointPanel surfaceMapPoint : _pspLiveVideoPanel.getAllSurfaceMapPoints())
      {
        Point pointPositionInPixel = surfaceMapPoint.getPositionInImageInPixel();
        _pointPositionInPixelToZHeightInNanometerMap.put(surfaceMapPoint.getPositionInImageInPixel(), _pspLiveVideoPanel.getFinalZHeightInNanometer(pointPositionInPixel.x, pointPositionInPixel.y));
      }
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private Rectangle getFinalRectangleAfterStageMovement(Rectangle rectangleInNanometer, int totalDragXDistanceTravelledInNanometer, int totalDragYDistanceTravelledInNanometer)
  {
    // get the final rectangle position in panel coordinate (in nanometer) after all stage movements.
    Rectangle rectangleAfterStageMovementInNanometer = new Rectangle();
    rectangleAfterStageMovementInNanometer.setBounds(MathUtil.roundDoubleToInt(rectangleInNanometer.getMinX()) - totalDragXDistanceTravelledInNanometer,
                                                     MathUtil.roundDoubleToInt(rectangleInNanometer.getMinY()) + totalDragYDistanceTravelledInNanometer,
                                                     rectangleInNanometer.width,
                                                     rectangleInNanometer.height);
    return rectangleAfterStageMovementInNanometer;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void resetOpticalCameraRectangleToOriginalPosition()
  {
    _opticalCameraRectangle.setRegion(_originalOpticalCameraRectangleRegionInNanometer.x, 
                                      _originalOpticalCameraRectangleRegionInNanometer.y, 
                                      _originalOpticalCameraRectangleRegionInNanometer.width, 
                                      _originalOpticalCameraRectangleRegionInNanometer.height);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void adjustBrightnessComboBox_actionPerformed()
  {
    final BusyCancelDialog busyDialog = new BusyCancelDialog((Frame)getOwner(),
                                                              StringLocalizer.keyToString("PSP_ADJUSTING_BRIGHTNESS_KEY"),
                                                              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                              true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, (Frame) getOwner());
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          EthernetBasedLightEngineCurrentSettingEnum currentSettingEnum = (EthernetBasedLightEngineCurrentSettingEnum)_adjustBrightnessComboBox.getSelectedItem();
          int selectedCurrentSetting = currentSettingEnum.getCurrent();
          _project.getPanel().getPspSettings().setCurrentSetting(selectedCurrentSetting);
          adjustBrightness(selectedCurrentSetting);
        }
        finally
        {
          busyDialog.dispose();
        }
      }
    });
    busyDialog.setVisible(true);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void adjustBrightness(int currentSetting)
  {
    if (_simulationMode == false)
    {
      try
      {
        if (_currentSectionTypeEnum.equals(SurfaceMapSectionTypeEnum.VIDEO))
        {
          // Live View mode
          _testExecution.adjustCurrent(currentSetting);
        }
        else
        {
          // Z-Height mode
          _testExecution.acquireSingleImage(currentSetting);
          // replace the current image with latest image.
          String imageFullPath = FileName.getAlignmentOpticalImageFullPath("temp", 0);
          java.awt.image.BufferedImage opticalImage = null;
          if (FileUtil.exists(imageFullPath))
          {
            try
            {
              opticalImage = javax.imageio.ImageIO.read(new java.io.File(imageFullPath));
            }
            catch (IOException ex)
            {
              ex.printStackTrace();
            }
          }
          _pspLiveVideoPanel.startImageMode(opticalImage);
        }
      }
      catch(XrayTesterException xte)
      {
        _mainUI.handleXrayTesterException(xte);
      }
    }
  }

  /**
   * This function is executed whenever the 'Live View' radio button is selected.
   * @author Ying-Huan.Chu
   */
  private void displayLiveViewRadioButton_actionPerformed()
  {
    // Live View mode
    // if current selection is Live View mode, do nothing.
    if (_currentSectionTypeEnum.equals(SurfaceMapSectionTypeEnum.VIDEO))
    {
      _displayLiveViewRadioButton.setSelected(true);
      return;
    }
    
    _displayZHeightViewRadioButton.setSelected(!_displayLiveViewRadioButton.isSelected());
    _currentSectionTypeEnum = SurfaceMapSectionTypeEnum.VIDEO;
    _abstractOpticalCamera.setHeightMapValueAvailable(false);

    try
    {
      setupWindowHandle();
    }
    catch(XrayTesterException xte)
    {
      try
      {
        exitOpticalCamera();
      }
      finally
      {
        dispose();
      }
      _exception = xte;
    }
    
    startOpticalLiveVideo();
  }
  
  /**
   * This function is executed whenever the 'Z-Height View' radio button is selected.
   * @author Ying-Huan.Chu
   */
  private void displayZHeightViewRadioButton_actionPerformed()
  {
    // Z-Height mode
    // if current selection is Z-Height mode, do nothing.
    if (_currentSectionTypeEnum.equals(SurfaceMapSectionTypeEnum.IMAGE))
    {
      _displayZHeightViewRadioButton.setSelected(true);
      return;
    }
    
    _displayLiveViewRadioButton.setSelected(!_displayZHeightViewRadioButton.isSelected());
    _currentSectionTypeEnum = SurfaceMapSectionTypeEnum.IMAGE;
    
    try
    {
      if (_simulationMode == false)
      {
        _abstractOpticalCamera.setStopLiveVideoBoolean(true);
      }
    }
    finally
    {
      doRegionSurfaceMapping(_opticalCameraRectangle, _board);
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean hasException()
  {
    if (_exception == null)
    {
      return false;
    }
    else
    {
      return true;
    }
  }
  
  /**
   * @author Kok Chun, Tan
   * @return 
   */
  public XrayTesterException getException()
  {
    Assert.expect(_exception != null);
    return _exception;
  }
  
  /**
   * check projector connection
   * @author Kok Chun, Tan
   */
  private void checkProjectorConnectivity() throws XrayTesterException
  {
    try
    {
      // try to turn off LED, projector is disconnect if cannot turn off.
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP))
      {
        _eblep.isConnected();
      }
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
  }
}

