package com.axi.v810.gui.psp;

import com.axi.guiUtil.PairLayout;
import com.axi.guiUtil.VerticalFlowLayout;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class responsible for coordinating stage navigation.
 * The display of optical live video panel is based on panel coordinate.
 * 
 * @author Cheah Lee Herng
 */
public class StageNavigationPanel extends JPanel
{
    private final int _COARSE_MAX_X_STAGE_MOVEMENT_IN_NANOMETERS = Config.getInstance().getIntValue(HardwareConfigEnum.STAGE_NAVIGATION_COARSE_MOVE_STEP_SIZE_IN_NANOMETERS);
    private final int _COARSE_MAX_Y_STAGE_MOVEMENT_IN_NANOMETERS = Config.getInstance().getIntValue(HardwareConfigEnum.STAGE_NAVIGATION_COARSE_MOVE_STEP_SIZE_IN_NANOMETERS);
    private final int _FINE_MAX_X_STAGE_MOVEMENT_IN_NANOMETERS = Config.getInstance().getIntValue(HardwareConfigEnum.STAGE_NAVIGATION_FINE_MOVE_STEP_SIZE_IN_NANOMETERS);
    private final int _FINE_MAX_Y_STAGE_MOVEMENT_IN_NANOMETERS = Config.getInstance().getIntValue(HardwareConfigEnum.STAGE_NAVIGATION_FINE_MOVE_STEP_SIZE_IN_NANOMETERS);
    
    private HardwareWorkerThread _workerThread = HardwareWorkerThread.getInstance3();
    
    //Motion profile to use when moving stage, use the fastest
    private PointToPointMotionProfileEnum _motionProfile = PointToPointMotionProfileEnum.PROFILE1;
    
    private JButton _coarseMoveUpButton = new JButton();
    private JButton _coarseMoveDownButton = new JButton();
    private JButton _coarseMoveLeftButton = new JButton();
    private JButton _coarseMoveRightButton = new JButton();
    
    private JButton _fineMoveUpButton = new JButton();
    private JButton _fineMoveDownButton = new JButton();
    private JButton _fineMoveLeftButton = new JButton();
    private JButton _fineMoveRightButton = new JButton();
    
    private JLabel _stageCoordinateXLabel = new JLabel();
    private JLabel _stageCoordinateYLabel = new JLabel();
    private JTextField _stageCoordinateXInNanometersTextField = new JTextField();
    private JTextField _stageCoordinateYInNanometersTextField = new JTextField();
    private JButton _moveStageButton = new JButton();
    private JButton _resetButton = new JButton();
    
    private static int _minXInNanometers = -1;
    private static int _maxXInNanometers = -1;
    private static int _minYInNanometers = -1;
    private static int _maxYInNanometers = -1;
    
    private static int _originalStageXPositionInNanometers = -1;
    private static int _originalStageYPositionInNanometers = -1;
    
    private StageNavigationMoveEnum _stageNavigationMoveEnum;
    private boolean _isSimulationOn = XrayTester.getInstance().isSimulationModeOn();
    
    private ActionListener _coarseMoveUpButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {
              _stageNavigationMoveEnum = StageNavigationMoveEnum.COARSE_MOVE;  
              moveRightButton_actionPerformed(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }
    };
    
    private ActionListener _coarseMoveDownButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {
              _stageNavigationMoveEnum = StageNavigationMoveEnum.COARSE_MOVE;
              moveLeftButton_actionPerformed(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }
    };
    
    private ActionListener _coarseMoveLeftButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {
              _stageNavigationMoveEnum = StageNavigationMoveEnum.COARSE_MOVE;
              moveUpButton_actionPerformed(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }
    };
    
    private ActionListener _coarseMoveRightButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {
              _stageNavigationMoveEnum = StageNavigationMoveEnum.COARSE_MOVE;
              moveDownButton_actionPerformed(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }
    };
    
    private ActionListener _fineMoveUpButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {
              _stageNavigationMoveEnum = StageNavigationMoveEnum.FINE_MOVE;  
              moveRightButton_actionPerformed(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }
    };
    
    private ActionListener _fineMoveDownButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {
              _stageNavigationMoveEnum = StageNavigationMoveEnum.FINE_MOVE;
              moveLeftButton_actionPerformed(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }
    };
    
    private ActionListener _fineMoveLeftButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {
              _stageNavigationMoveEnum = StageNavigationMoveEnum.FINE_MOVE;
              moveUpButton_actionPerformed(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }
    };
    
    private ActionListener _fineMoveRightButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {
              _stageNavigationMoveEnum = StageNavigationMoveEnum.FINE_MOVE;
              moveDownButton_actionPerformed(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }
    };
    
    private ActionListener _moveStageButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {
              _stageNavigationMoveEnum = StageNavigationMoveEnum.STAGE_MOVE;
              moveStageButton_actionPerformed(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }
    };
    
    private ActionListener _resetButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {
              _stageNavigationMoveEnum = StageNavigationMoveEnum.STAGE_MOVE;
              resetButton_actionPerformed(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }
    };
    
    private FocusListener _stageCoordinateXTextFieldFocusListener = new FocusListener()
    {
        public void focusLost(FocusEvent e) 
        {
            try
            {
                stageCoordinateXTextField_focusLost(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }

        public void focusGained(FocusEvent e) 
        {
            // Do nothing
        }
    };
    
    private FocusListener _stageCoordinateYTextFieldFocusListener = new FocusListener()
    {
        public void focusLost(FocusEvent e) 
        {
            try
            {
                stageCoordinateYTextField_focusLost(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }

        public void focusGained(FocusEvent e) 
        {
            // Do nothing
        }
    };
    
    /**
     * @author Cheah Lee Herng
     */
    public StageNavigationPanel() throws XrayTesterException
    {
        jbInit();
        initializeStagePositionLimitAndActualStagePosition();
    }
    
    /**
     * @author Cheah Lee Herng
    */
    public void setVisible(boolean visible)
    {
        super.setVisible(visible);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void jbInit()
    {
        this.setSize(300, 400);
        setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));
        
        JPanel navigationPanel = new JPanel(new VerticalFlowLayout(1, 5));
        
        JPanel innerCoarseMovePanel = new JPanel();
        innerCoarseMovePanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), "Coarse Move"),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        innerCoarseMovePanel.setLayout(new BorderLayout());
        _coarseMoveUpButton.setText(StringLocalizer.keyToString("GUI_MOVE_UP_BUTTON_KEY"));
        _coarseMoveDownButton.setText(StringLocalizer.keyToString("GUI_MOVE_DOWN_BUTTON_KEY"));
        _coarseMoveRightButton.setText(StringLocalizer.keyToString("GUI_MOVE_RIGHT_BUTTON_KEY"));
        _coarseMoveLeftButton.setText(StringLocalizer.keyToString("GUI_MOVE_LEFT_BUTTON_KEY"));        
        innerCoarseMovePanel.add(_coarseMoveUpButton, BorderLayout.NORTH);
        innerCoarseMovePanel.add(_coarseMoveDownButton, BorderLayout.SOUTH);
        innerCoarseMovePanel.add(_coarseMoveRightButton, BorderLayout.EAST);
        innerCoarseMovePanel.add(_coarseMoveLeftButton, BorderLayout.WEST);
        
        JPanel innerFineMovePanel = new JPanel();
        innerFineMovePanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), "Fine Move"),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        innerFineMovePanel.setLayout(new BorderLayout());
        _fineMoveUpButton.setText(StringLocalizer.keyToString("GUI_MOVE_UP_BUTTON_KEY"));
        _fineMoveDownButton.setText(StringLocalizer.keyToString("GUI_MOVE_DOWN_BUTTON_KEY"));
        _fineMoveRightButton.setText(StringLocalizer.keyToString("GUI_MOVE_RIGHT_BUTTON_KEY"));
        _fineMoveLeftButton.setText(StringLocalizer.keyToString("GUI_MOVE_LEFT_BUTTON_KEY"));               
        innerFineMovePanel.add(_fineMoveUpButton, BorderLayout.NORTH);
        innerFineMovePanel.add(_fineMoveDownButton, BorderLayout.SOUTH);
        innerFineMovePanel.add(_fineMoveRightButton, BorderLayout.EAST);
        innerFineMovePanel.add(_fineMoveLeftButton, BorderLayout.WEST);
        
        JPanel stageMovePanel = new JPanel();
        stageMovePanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), "Stage Move"),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        stageMovePanel.setLayout(new PairLayout(10, 10, false));       
        _stageCoordinateXLabel.setText(StringLocalizer.keyToString("GUI_POSITION_X_KEY"));
        _stageCoordinateXInNanometersTextField.setColumns(10);
        _stageCoordinateYLabel.setText(StringLocalizer.keyToString("GUI_POSITION_Y_KEY"));
        _stageCoordinateYInNanometersTextField.setColumns(10);
        _moveStageButton.setText(StringLocalizer.keyToString("GUI_MOVE_KEY"));
        _resetButton.setText(StringLocalizer.keyToString("GUI_RESET_KEY"));
     
        stageMovePanel.add(_stageCoordinateXLabel);
        stageMovePanel.add(_stageCoordinateXInNanometersTextField);
        stageMovePanel.add(_stageCoordinateYLabel);
        stageMovePanel.add(_stageCoordinateYInNanometersTextField);
     
        
        navigationPanel.add(innerCoarseMovePanel);
        navigationPanel.add(innerFineMovePanel);
        navigationPanel.add(stageMovePanel);
        navigationPanel.add(_moveStageButton);
        navigationPanel.add(_resetButton);
        
        add(navigationPanel, BorderLayout.SOUTH); 
        
        // Add button action listeners
        addListeners();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void start()
    {
        // Do nothing
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void finish()
    {
        removeListeners();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void initializeStagePositionLimitAndActualStagePosition() throws XrayTesterException
    { 
        if (_isSimulationOn == false)
        {
            //Initialize the limits for the point-to-point moves in X
            _minXInNanometers = PanelPositioner.getInstance().getXaxisMinimumPositionLimitInNanometers();
            _maxXInNanometers = PanelPositioner.getInstance().getXaxisMaximumPositionLimitInNanometers();

            //Initialize the limits for the point-to-point moves in Y
            _minYInNanometers = PanelPositioner.getInstance().getYaxisMinimumPositionLimitInNanometers();
            _maxYInNanometers = PanelPositioner.getInstance().getYaxisMaximumPositionLimitInNanometers();
            
            _originalStageXPositionInNanometers = PanelPositioner.getInstance().getXaxisActualPositionInNanometers();
            _originalStageYPositionInNanometers = PanelPositioner.getInstance().getYaxisActualPositionInNanometers();
        }
        else
        {
            _minXInNanometers = -1;
            _maxXInNanometers = -1;
            _minYInNanometers = -1;
            _maxYInNanometers = -1;
            
            _originalStageXPositionInNanometers = -1;
            _originalStageYPositionInNanometers = -1;
        }
    }
    
    /**
    * @author Cheah Lee Herng
    */
    private void moveUpButton_actionPerformed(ActionEvent e) throws XrayTesterException
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          { 
            try
            {
              if(_isSimulationOn == false && PanelPositioner.getInstance().isStartupRequired() == false)
              {
                  // Make sure we check the stage limit before move
                  if (isStageAbleToMoveUp())
                      moveStageUp();
              }
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
          }
        });      
    }
    
    /**
    * @author Cheah Lee Herng
    */
    private void moveDownButton_actionPerformed(ActionEvent e) throws XrayTesterException
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              if(_isSimulationOn == false && PanelPositioner.getInstance().isStartupRequired() == false)
              {
                  // Make sure we check the stage limit before move
                  if (isStageAbleToMoveDown())
                      moveStageDown();
              }
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
          }
        });      
    }
    
    /**
    * @author Cheah Lee Herng
    */
    private void moveLeftButton_actionPerformed(ActionEvent e) throws XrayTesterException
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              if(_isSimulationOn == false && PanelPositioner.getInstance().isStartupRequired() == false)
              {
                  // Make sure we check the stage limit before move
                  if (isStageAbleToMoveLeft())
                      moveStageLeft();
              }
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
          }
        });      
    }

    /**
    * @author Cheah Lee Herng
    */
    private void moveRightButton_actionPerformed(ActionEvent e) throws XrayTesterException
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              if(_isSimulationOn == false && PanelPositioner.getInstance().isStartupRequired() == false)
              {
                  // Make sure we check the stage limit before move
                  if (isStageAbleToMoveRight())
                      moveStageRight();
              }
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
          }
        });      
    }
    
    /**
    * @author Cheah Lee Herng
    */
    private void moveStageButton_actionPerformed(ActionEvent e) throws XrayTesterException
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              if(_isSimulationOn == false && PanelPositioner.getInstance().isStartupRequired() == false)
              {
                  String xPositionInNanometersString = _stageCoordinateXInNanometersTextField.getText();
                  String yPositionInNanometersString = _stageCoordinateYInNanometersTextField.getText();
                  
                  int xPositionInNanometers = Integer.parseInt(xPositionInNanometersString);
                  int yPositionInNanometers = Integer.parseInt(yPositionInNanometersString);
                  
                  moveStage(xPositionInNanometers, yPositionInNanometers, _motionProfile);
              }
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
          }
        });      
    }
    
    /**
    * @author Cheah Lee Herng
    */
    private void resetButton_actionPerformed(ActionEvent e) throws XrayTesterException
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              if(_isSimulationOn == false && PanelPositioner.getInstance().isStartupRequired() == false)
              {                  
                  moveStage(_originalStageXPositionInNanometers, _originalStageYPositionInNanometers, _motionProfile);
              }
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
          }
        });      
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    private void stageCoordinateXTextField_focusLost(FocusEvent e) throws XrayTesterException
    {   
        String currentValueString = _stageCoordinateXInNanometersTextField.getText();
        int currentValue = Integer.parseInt(currentValueString);
        
        if (currentValue < _minXInNanometers || 
            currentValue > _maxXInNanometers)
        {
            // the value entered is not between the min and max defined by the panel positioner
            String message = StringLocalizer.keyToString(new LocalizedString("GUI_INVALID_STAGE_X_POSITION_KEY",
                                                         new Object[]{_minXInNanometers, _maxXInNanometers}));            
            
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                          message,
                                          StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                          JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    private void stageCoordinateYTextField_focusLost(FocusEvent e) throws XrayTesterException
    {
        String currentValueString = _stageCoordinateXInNanometersTextField.getText();
        int currentValue = Integer.parseInt(currentValueString);
        
        if (currentValue < _minYInNanometers || 
            currentValue > _maxYInNanometers)
        {
            // the value entered is not between the min and max defined by the panel positioner
            String message = StringLocalizer.keyToString(new LocalizedString("GUI_INVALID_STAGE_Y_POSITION_KEY",
                                                         new Object[]{_minYInNanometers, _maxYInNanometers}));            
            
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                          message,
                                          StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                          JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void addListeners()
    {
        _coarseMoveUpButton.addActionListener(_coarseMoveUpButtonActionListener);
        _coarseMoveDownButton.addActionListener(_coarseMoveDownButtonActionListener);
        _coarseMoveLeftButton.addActionListener(_coarseMoveLeftButtonActionListener);
        _coarseMoveRightButton.addActionListener(_coarseMoveRightButtonActionListener);
        
        _fineMoveUpButton.addActionListener(_fineMoveUpButtonActionListener);
        _fineMoveDownButton.addActionListener(_fineMoveDownButtonActionListener);
        _fineMoveLeftButton.addActionListener(_fineMoveLeftButtonActionListener);
        _fineMoveRightButton.addActionListener(_fineMoveRightButtonActionListener);
        
        _moveStageButton.addActionListener(_moveStageButtonActionListener);
        _resetButton.addActionListener(_resetButtonActionListener);
        
        _stageCoordinateXInNanometersTextField.addFocusListener(_stageCoordinateXTextFieldFocusListener);
        _stageCoordinateYInNanometersTextField.addFocusListener(_stageCoordinateYTextFieldFocusListener);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void removeListeners()
    {
        _coarseMoveUpButton.removeActionListener(_coarseMoveUpButtonActionListener);
        _coarseMoveDownButton.removeActionListener(_coarseMoveDownButtonActionListener);
        _coarseMoveLeftButton.removeActionListener(_coarseMoveLeftButtonActionListener);
        _coarseMoveRightButton.removeActionListener(_coarseMoveRightButtonActionListener);
        
        _fineMoveUpButton.removeActionListener(_fineMoveUpButtonActionListener);
        _fineMoveDownButton.removeActionListener(_fineMoveDownButtonActionListener);
        _fineMoveLeftButton.removeActionListener(_fineMoveLeftButtonActionListener);
        _fineMoveRightButton.removeActionListener(_fineMoveRightButtonActionListener);
        
        _moveStageButton.removeActionListener(_moveStageButtonActionListener);
        _resetButton.removeActionListener(_resetButtonActionListener);
        
        _stageCoordinateXInNanometersTextField.removeFocusListener(_stageCoordinateXTextFieldFocusListener);
        _stageCoordinateYInNanometersTextField.removeFocusListener(_stageCoordinateYTextFieldFocusListener);
    }
    
    /**
    * @author Cheah Lee Herng 
    */
    private int getMoveLeftStagePositionInNanometers() throws XrayTesterException
    {
      int stepSizeInNanometers = -1;
      if (_stageNavigationMoveEnum == StageNavigationMoveEnum.COARSE_MOVE)
          stepSizeInNanometers = _COARSE_MAX_X_STAGE_MOVEMENT_IN_NANOMETERS;
      else if (_stageNavigationMoveEnum == StageNavigationMoveEnum.FINE_MOVE)
          stepSizeInNanometers = _FINE_MAX_X_STAGE_MOVEMENT_IN_NANOMETERS;
      else
          Assert.expect(false);
        
      int xPositionInNanometers = PanelPositioner.getInstance().getXaxisActualPositionInNanometers();
      return xPositionInNanometers - stepSizeInNanometers;
    }

    /**
    * @author Cheah Lee Herng 
    */
    private int getMoveRightStagePositionInNanometers() throws XrayTesterException
    {
      int stepSizeInNanometers = -1;
      if (_stageNavigationMoveEnum == StageNavigationMoveEnum.COARSE_MOVE)
          stepSizeInNanometers = _COARSE_MAX_X_STAGE_MOVEMENT_IN_NANOMETERS;
      else if (_stageNavigationMoveEnum == StageNavigationMoveEnum.FINE_MOVE)
          stepSizeInNanometers = _FINE_MAX_X_STAGE_MOVEMENT_IN_NANOMETERS;
      else
          Assert.expect(false);
        
      int xPositionInNanometers = PanelPositioner.getInstance().getXaxisActualPositionInNanometers();
      return xPositionInNanometers + stepSizeInNanometers;
    }

    /**
    * @author Cheah Lee Herng 
    */
    private int getMoveUpStagePositionInNanometers() throws XrayTesterException
    {
      int stepSizeInNanometers = -1;
      if (_stageNavigationMoveEnum == StageNavigationMoveEnum.COARSE_MOVE)
          stepSizeInNanometers = _COARSE_MAX_Y_STAGE_MOVEMENT_IN_NANOMETERS;
      else if (_stageNavigationMoveEnum == StageNavigationMoveEnum.FINE_MOVE)
          stepSizeInNanometers = _FINE_MAX_Y_STAGE_MOVEMENT_IN_NANOMETERS;
      else
          Assert.expect(false);
        
      int yPositionInNanometers = PanelPositioner.getInstance().getYaxisActualPositionInNanometers();
      return yPositionInNanometers + stepSizeInNanometers;
    }

    /**
    * @author Cheah Lee Herng 
    */
    private int getMoveDownStagePositionInNanometers() throws XrayTesterException
    {
      int stepSizeInNanometers = -1;
      if (_stageNavigationMoveEnum == StageNavigationMoveEnum.COARSE_MOVE)
          stepSizeInNanometers = _COARSE_MAX_Y_STAGE_MOVEMENT_IN_NANOMETERS;
      else if (_stageNavigationMoveEnum == StageNavigationMoveEnum.FINE_MOVE)
          stepSizeInNanometers = _FINE_MAX_Y_STAGE_MOVEMENT_IN_NANOMETERS;
      else
          Assert.expect(false);
        
      int yPositionInNanometers = PanelPositioner.getInstance().getYaxisActualPositionInNanometers();
      return yPositionInNanometers - stepSizeInNanometers;
    }
    
    /**
    * @author Cheah Lee Herng
    */
    private boolean isStageAbleToMoveLeft() throws XrayTesterException
    {
      boolean isStageAbleToMoveLeft = false;
      if (getMoveLeftStagePositionInNanometers() < _minXInNanometers)
          isStageAbleToMoveLeft = false;
      else
          isStageAbleToMoveLeft = true;
      return isStageAbleToMoveLeft;
    }

    /**
    * @author Cheah Lee Herng
    */
    private boolean isStageAbleToMoveRight() throws XrayTesterException
    {
      boolean isStageAbleToMoveRight = false;
      if (getMoveRightStagePositionInNanometers() > _maxXInNanometers)
          isStageAbleToMoveRight = false;
      else
          isStageAbleToMoveRight = true;
      return isStageAbleToMoveRight;
    }

    /**
    * @author Cheah Lee Herng
    */
    private boolean isStageAbleToMoveUp() throws XrayTesterException
    {      
      boolean isStageAbleToMoveUp = false;
      if (getMoveUpStagePositionInNanometers() > _maxYInNanometers)
          isStageAbleToMoveUp = false;
      else
          isStageAbleToMoveUp = true;
      return isStageAbleToMoveUp;
    }

    /**
    * @author Cheah Lee Herng
    */
    private boolean isStageAbleToMoveDown() throws XrayTesterException
    {
      boolean isStageAbleToMoveDown = false;
      if (getMoveDownStagePositionInNanometers() < _minYInNanometers)
          isStageAbleToMoveDown = false;
      else
          isStageAbleToMoveDown = true;
      return isStageAbleToMoveDown;
    }
    
    /**
    * @author Cheah Lee Herng 
    */
    private void moveStageLeft() throws  XrayTesterException
    {
      int yPositionInNanometers = PanelPositioner.getInstance().getYaxisActualPositionInNanometers();      
      int moveLeftPositionInNanometers = getMoveLeftStagePositionInNanometers();
      moveStage(moveLeftPositionInNanometers, yPositionInNanometers, _motionProfile);
    }

    /**
    * @author Cheah Lee Herng 
    */
    private void moveStageRight() throws  XrayTesterException
    {
      int yPositionInNanometers = PanelPositioner.getInstance().getYaxisActualPositionInNanometers();
      int moveRightPositionInNanometers = getMoveRightStagePositionInNanometers();
      moveStage(moveRightPositionInNanometers, yPositionInNanometers, _motionProfile);
    }

    /**
    * @author Cheah Lee Herng 
    */
    private void moveStageUp() throws  XrayTesterException
    {
      int xPositionInNanometers = PanelPositioner.getInstance().getXaxisActualPositionInNanometers();
      int moveUpPositionInNanometers = getMoveUpStagePositionInNanometers();
      moveStage(xPositionInNanometers, moveUpPositionInNanometers, _motionProfile);
    }

    /**
    * @author Cheah Lee Herng 
    */
    private void moveStageDown() throws  XrayTesterException
    {
      int xPositionInNanometers = PanelPositioner.getInstance().getXaxisActualPositionInNanometers();
      int moveDownPositionInNanometers = getMoveDownStagePositionInNanometers();
      moveStage(xPositionInNanometers, moveDownPositionInNanometers, _motionProfile);
    }
    
    /**
    * @author Cheah Lee Herng
    */
    private void moveStage(int xPositionToMoveTo,
                           int yPositionToMoveTo,
                           PointToPointMotionProfileEnum profile) throws XrayTesterException
    {
        StagePositionMutable stagePosition = new StagePositionMutable();

        //Set the stage position we are moving to
        stagePosition.setXInNanometers(xPositionToMoveTo);
        stagePosition.setYInNanometers(yPositionToMoveTo);
        
        // Save off the previously loaded motion profile
        MotionProfile originalMotionProfile = PanelPositioner.getInstance().getActiveMotionProfile();

        //Set the motion profile we will use
        PanelPositioner.getInstance().setMotionProfile(new MotionProfile(profile));
        
        //Move to the new location using the profile
        PanelPositioner.getInstance().pointToPointMoveAllAxes(stagePosition);
        
        // Now that we are done with the move, restore the motion profile.
        PanelPositioner.getInstance().setMotionProfile(originalMotionProfile);
    }
}
