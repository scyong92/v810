/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.gui.psp;

import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

/**
 *
 * @author Ying-Huan.Chu
 */
public class SurfaceMapCopyRectanglesToBoardDialog extends JDialog
{
  private Project _project;
  private PspAutoPopulatePanel _pspAutoPopulatePanel;
  
  private JPanel _mainPanel;
  
  private JPanel _boardsToCopyFromPanel;
  private BorderLayout _boardsToCopyFromPanelLayout;
  private JLabel _boardsToCopyFromLabel;
  private JComboBox _boardsToCopyFromComboBox;
  
  private JPanel _boardsToCopyToPanel;
  private JLabel _boardsToCopyToLabel;
  private JList _boardsToCopyToList;
  
  private JPanel _buttonPanel;
  private JButton _okButton;
  private JButton _cancelButton;
  private boolean _isUserClickOk = false;
  
  private Board _boardToCopyFrom;
  
  /**
   * @author Ying-Huan.Chu
   */
  public SurfaceMapCopyRectanglesToBoardDialog(JFrame parent, String title)
  {
    super(parent, title, true);
    jbInit();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void jbInit()
  {
    _project = Project.getCurrentlyLoadedProject();
    _pspAutoPopulatePanel = PspAutoPopulatePanel.getInstance();
    
    _boardsToCopyFromLabel = new JLabel(StringLocalizer.keyToString("PSP_COPY_FROM_BOARD_KEY"));
    _boardsToCopyFromComboBox = new JComboBox();
    for (Board board : _pspAutoPopulatePanel.getBoardsWithOpticalCameraRectangles())
    {
      _boardsToCopyFromComboBox.addItem(board);
    }
    _boardsToCopyFromComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        boardsToCopyFromComboBox_actionPerformed();
      }
    });
    _boardsToCopyFromPanel = new JPanel();
    _boardsToCopyFromPanelLayout = new BorderLayout();
    _boardsToCopyFromPanelLayout.setVgap(-1);
    _boardsToCopyFromPanel.setLayout(_boardsToCopyFromPanelLayout);
    _boardsToCopyFromPanel.setBorder(BorderFactory.createEmptyBorder(10,10,20,10));
    _boardsToCopyFromPanel.add(_boardsToCopyFromLabel, BorderLayout.NORTH);
    _boardsToCopyFromPanel.add(_boardsToCopyFromComboBox, BorderLayout.SOUTH);
    
    _boardsToCopyToLabel = new JLabel(StringLocalizer.keyToString("PSP_COPY_TO_BOARDS_KEY"));
    _boardsToCopyToList = new JList();
    _boardsToCopyToList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    updateBoardToCopyToList();
    _boardsToCopyToPanel = new JPanel();
    _boardsToCopyToPanel.setLayout(new BorderLayout());
    _boardsToCopyToPanel.setBorder(BorderFactory.createEmptyBorder(10,10,20,10));
    _boardsToCopyToPanel.add(_boardsToCopyToLabel, BorderLayout.NORTH);
    _boardsToCopyToPanel.add(_boardsToCopyToList, BorderLayout.CENTER);
    
    _okButton = new JButton(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed();
      }
    });
    _cancelButton = new JButton(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new ActionListener() 
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });
    _buttonPanel = new JPanel();
    _buttonPanel.setLayout(new FlowLayout());
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,10,20,10));
    _buttonPanel.add(_okButton);
    _buttonPanel.add(_cancelButton);
    
    _mainPanel = new JPanel();
    _mainPanel.setLayout(new BorderLayout());
    _mainPanel.setBorder(BorderFactory.createEmptyBorder(10,10,20,10));
    _mainPanel.add(_boardsToCopyFromPanel, BorderLayout.WEST);
    _mainPanel.add(_boardsToCopyToPanel, BorderLayout.EAST);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    
    this.add(_mainPanel);
    this.setSize(new Dimension(500,500));
    this.pack();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void boardsToCopyFromComboBox_actionPerformed()
  {
    updateBoardToCopyToList();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void updateBoardToCopyToList()
  {
    java.util.List<Board> boardsToCopyToList = new ArrayList(_project.getPanel().getBoards());
    _boardToCopyFrom = (Board)_boardsToCopyFromComboBox.getSelectedItem();
    boardsToCopyToList.remove(_boardToCopyFrom);
    Board[] boards = new Board[boardsToCopyToList.size()];
    for (int i = 0; i < boardsToCopyToList.size(); ++i)
    {
      boards[i] = boardsToCopyToList.get(i);
    }
    _boardsToCopyToList.setListData(boards);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void okButton_actionPerformed()
  {
    if (_boardsToCopyToList.getSelectedValuesList().size() > 0)
    {
      _isUserClickOk = true;
      dispose();
    }
    else
    {
      JOptionPane.showMessageDialog(_pspAutoPopulatePanel,
                                    StringLocalizer.keyToString("PSP_BOARDS_TO_COPY_TO_CANNOT_EMPTY_MESSAGE_KEY"),
                                    StringLocalizer.keyToString("PSP_BOARD_CANNOT_EMPTY_TITLE_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean isUserClickedOk()
  {
    return _isUserClickOk;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public Board getBoardToCopyFrom()
  {
    return _boardToCopyFrom;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public java.util.List<Board> getBoardsToCopyTo()
  {
    return _boardsToCopyToList.getSelectedValuesList();
  }
}
