package com.axi.v810.gui.psp;

import com.axi.util.Enum;

/**
 * @author Cheah Lee Herng
 */
public class FringePatternDisplayModeEnum extends Enum
{
    private static int _index = 0;    
    public static FringePatternDisplayModeEnum WHITE_LIGHT = new FringePatternDisplayModeEnum(++_index);
    public static FringePatternDisplayModeEnum FRINGE_PATTERN_SHIFT_ONE = new FringePatternDisplayModeEnum(++_index);
    public static FringePatternDisplayModeEnum FRINGE_PATTERN_SHIFT_TWO = new FringePatternDisplayModeEnum(++_index);
    public static FringePatternDisplayModeEnum FRINGE_PATTERN_SHIFT_THREE = new FringePatternDisplayModeEnum(++_index);
    public static FringePatternDisplayModeEnum FRINGE_PATTERN_SHIFT_FOUR = new FringePatternDisplayModeEnum(++_index);
    
    /**
     * FringePatternEnum
     *
     * @author Cheah Lee Herng
    */
    protected FringePatternDisplayModeEnum(int id)
    {
        super(id);
    }
}
