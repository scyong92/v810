/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.psp;

import java.awt.*;
import com.axi.util.*;

/**
 * Surface Map Point class.
 * @author Ying-Huan.Chu
 */
public class SurfaceMapPointPanel extends java.awt.Panel
{
  private final int MAX_SURFACE_MAP_POINTS_ROI_WIDTH_IN_PIXEL = 10;
  private final int MAX_SURFACE_MAP_POINTS_ROI_HEIGHT_IN_PIXEL = 10;
  private final int HALF_OF_SURFACE_MAP_POINT_ROI_WIDTH = (MAX_SURFACE_MAP_POINTS_ROI_WIDTH_IN_PIXEL / 2) - 1;
  private final int HALF_OF_SURFACE_MAP_POINT_ROI_HEIGHT = (MAX_SURFACE_MAP_POINTS_ROI_HEIGHT_IN_PIXEL / 2) - 1;
  
  private Color _colour;
  private Dimension _surfaceMapPointDimension = new Dimension(MAX_SURFACE_MAP_POINTS_ROI_WIDTH_IN_PIXEL, MAX_SURFACE_MAP_POINTS_ROI_HEIGHT_IN_PIXEL);
  private Point _positionInImageSectionInPixel;
  private Point _positionInPspLiveVideoPanelInPixel;
  
  /**
   * @author Ying-Huan.Chu
   */
  public SurfaceMapPointPanel(Point positionInPixel, Color color)
  {
    Assert.expect(positionInPixel != null);
    Assert.expect(color != null);
    Assert.expect(positionInPixel.x >= 0);
    Assert.expect(positionInPixel.y >= 0);

    _positionInImageSectionInPixel = positionInPixel;
    _colour = color;
    jbInit();
  }
  
  private void jbInit()
  {
    this.setLocation(_positionInImageSectionInPixel);
    this.setBackground(_colour);
    this.setSize(_surfaceMapPointDimension);
    this.setBounds(_positionInImageSectionInPixel.x - HALF_OF_SURFACE_MAP_POINT_ROI_WIDTH, 
                   _positionInImageSectionInPixel.y - HALF_OF_SURFACE_MAP_POINT_ROI_HEIGHT, 
                   MAX_SURFACE_MAP_POINTS_ROI_WIDTH_IN_PIXEL, 
                   MAX_SURFACE_MAP_POINTS_ROI_HEIGHT_IN_PIXEL);
    this.setVisible(true);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public Color getColour()
  {
    return _colour;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setColour(Color colour)
  {
    _colour = colour;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setPositionInImageInPixel(Point positionInImageInPixel)
  {
    Assert.expect(positionInImageInPixel != null);
    
    //_positionInPspLiveVideoPanelInPixel = positionInImageInPixel;
    _positionInImageSectionInPixel = positionInImageInPixel;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public Point getPositionInImageInPixel()
  {
    //return _positionInPspLiveVideoPanelInPixel;
    return _positionInImageSectionInPixel;
  }
}
