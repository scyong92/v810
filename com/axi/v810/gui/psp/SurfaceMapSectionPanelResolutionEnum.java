/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.gui.psp;

/**
 * SurfaceMapSectionPanelResolutionEnum class - defines the name of SurfaceMapImageSectionPanel.
 * @author Ying-Huan.Chu
 */
public class SurfaceMapSectionPanelResolutionEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  
  public static SurfaceMapSectionPanelResolutionEnum ALL_MODE_LOW_RES = new SurfaceMapSectionPanelResolutionEnum(++_index);
  public static SurfaceMapSectionPanelResolutionEnum VIDEO_MODE_HIGH_RES = new SurfaceMapSectionPanelResolutionEnum(++_index); // for video mode
  public static SurfaceMapSectionPanelResolutionEnum IMAGE_MODE_HIGH_RES_TOP_LEFT_SECTION = new SurfaceMapSectionPanelResolutionEnum(++_index);
  public static SurfaceMapSectionPanelResolutionEnum IMAGE_MODE_HIGH_RES_TOP_RIGHT_SECTION = new SurfaceMapSectionPanelResolutionEnum(++_index);
  public static SurfaceMapSectionPanelResolutionEnum IMAGE_MODE_HIGH_RES_BOTTOM_LEFT_SECTION = new SurfaceMapSectionPanelResolutionEnum(++_index);
  public static SurfaceMapSectionPanelResolutionEnum IMAGE_MODE_HIGH_RES_BOTTOM_RIGHT_SECTION = new SurfaceMapSectionPanelResolutionEnum(++_index);
  
  /**
   * SurfaceMapImageSectionPanelEnum Constructor
   * @author Ying-Huan.Chu
   */
  private SurfaceMapSectionPanelResolutionEnum(int id)
  {
    super(id);
  }
}
