package com.axi.v810.gui.psp;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class FringePatternPanel extends JPanel
{
    private JButton _increaseIntensityRatioButton = new JButton();
    private JButton _decreaseIntensityRatioButton = new JButton();
    private JButton _increaseGlobalIntensityButton = new JButton();
    private JButton _decreaseGlobalIntensityButton = new JButton();
    
    private ActionListener _increaseIntensityRatioButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {              
              increaseIntensityRatioButton_actionPerformed(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }
    };
    
    private ActionListener _decreaseIntensityRatioButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {              
              decreaseIntensityRatioButton_actionPerformed(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }
    };
    
    private ActionListener _increaseGlobalIntensityButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {              
              increaseGlobalIntensityButton_actionPerformed(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }
    };
    
    private ActionListener _decreaseGlobalIntensityButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {              
              decreaseGlobalIntensityButton_actionPerformed(e);
            }
            catch (XrayTesterException xte)
            {
              Assert.logException(xte);
            }
        }
    };
    
    /**
    * @author Cheah Lee Herng
    */
    private void increaseIntensityRatioButton_actionPerformed(ActionEvent e) throws XrayTesterException
    {
        FringePattern.increaseIntensityRatio();
    }
    
    /**
    * @author Cheah Lee Herng
    */
    private void decreaseIntensityRatioButton_actionPerformed(ActionEvent e) throws XrayTesterException
    {
        FringePattern.decreaseIntensityRatio();
    }
    
    /**
    * @author Cheah Lee Herng
    */
    private void increaseGlobalIntensityButton_actionPerformed(ActionEvent e) throws XrayTesterException
    {
        FringePattern.increaseGlobalIntensity();
    }
    
    /**
    * @author Cheah Lee Herng
    */
    private void decreaseGlobalIntensityButton_actionPerformed(ActionEvent e) throws XrayTesterException
    {
        FringePattern.decreaseGlobalIntensity();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public FringePatternPanel() throws XrayTesterException
    {
        jbInit();        
    }
    
    /**
     * @author Cheah Lee Herng
    */
    public void setVisible(boolean visible)
    {
        super.setVisible(visible);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void jbInit()
    {
        this.setSize(200, 200);
        setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));
        
        JPanel fringePatternPanel = new JPanel(new VerticalFlowLayout(1, 5));
        
        JPanel innerIntensityRatioPanel = new JPanel();
        innerIntensityRatioPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), "Intensity Ratio"),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        innerIntensityRatioPanel.setLayout(new BorderLayout());
        _increaseIntensityRatioButton.setText(StringLocalizer.keyToString("GUI_INCREASE_INTENSITY_RATIO_BUTTON_KEY"));
        _decreaseIntensityRatioButton.setText(StringLocalizer.keyToString("GUI_DECREASE_INTENSITY_RATIO_BUTTON_KEY"));        
        innerIntensityRatioPanel.add(_decreaseIntensityRatioButton, BorderLayout.EAST);
        innerIntensityRatioPanel.add(_increaseIntensityRatioButton, BorderLayout.WEST);
        
        /*
        JPanel innerGlobalIntensityPanel = new JPanel();
        innerGlobalIntensityPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), "Global Intensity"),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        innerGlobalIntensityPanel.setLayout(new BorderLayout());
        _increaseGlobalIntensityButton.setText(StringLocalizer.keyToString("GUI_INCREASE_INTENSITY_RATIO_BUTTON_KEY"));
        _decreaseGlobalIntensityButton.setText(StringLocalizer.keyToString("GUI_DECREASE_INTENSITY_RATIO_BUTTON_KEY"));        
        innerGlobalIntensityPanel.add(_decreaseGlobalIntensityButton, BorderLayout.EAST);
        innerGlobalIntensityPanel.add(_increaseGlobalIntensityButton, BorderLayout.WEST);
         */
        
        fringePatternPanel.add(innerIntensityRatioPanel);
        //fringePatternPanel.add(innerGlobalIntensityPanel);
        
        add(fringePatternPanel, BorderLayout.SOUTH);
        
        _increaseIntensityRatioButton.addActionListener(_increaseIntensityRatioButtonActionListener);
        _decreaseIntensityRatioButton.addActionListener(_decreaseIntensityRatioButtonActionListener);
//        _increaseGlobalIntensityButton.addActionListener(_increaseGlobalIntensityButtonActionListener);
//        _decreaseGlobalIntensityButton.addActionListener(_decreaseGlobalIntensityButtonActionListener);
    }  
}
