package com.axi.v810.gui.psp;

import java.awt.*;
import javax.media.opengl.awt.*;
import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class FringePatternDialog extends JDialog
{
    private final int _MAX_WIDTH_IN_PIXEL = 640;
    private final int _MAX_HEIGHT_IN_PIXEL = 480;
    
    private FringePattern _fringePattern;
    private GLCanvas _glCanvas;
    private int _dialogStartingPositionX;
    private double _cameraIntensityRatio = 1.0;
    private int _fringePatternOffset = 0;
    private double _globalIntensity = FringePattern.DEFAULT_GLOBAL_INTENSITY;
    
//    private Robot _robot;
    
    /**
     * @author Cheah Lee Herng
     */
    public FringePatternDialog(Frame parent, String title, OpticalCameraIdEnum cameraId)
    {        
        super(parent, title, false);
        
        setupFringePatternProperties(cameraId);

        if(XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
          _fringePattern = new FringePattern(_fringePatternOffset, FringePattern.DEFAULT_XXL_PERIOD, _cameraIntensityRatio, _globalIntensity);
        else
          _fringePattern = new FringePattern(_fringePatternOffset, FringePattern.DEFAULT_PERIOD, _cameraIntensityRatio, _globalIntensity);
        _glCanvas = _fringePattern.getGLCanvas();
        _dialogStartingPositionX = Toolkit.getDefaultToolkit().getScreenSize().width;
        
        _glCanvas.addGLEventListener(_fringePattern);
        _glCanvas.requestFocus();
        
        Assert.expect(_glCanvas != null);
        
//        try
//        {
//            _robot = new Robot();
//        }
//        catch (AWTException e)
//        {
//            Assert.expect(false);
//        }
        
        // Position fringe pattern dialog to 2nd projector
        this.setLocation(_dialogStartingPositionX, 0);
                
        jbInt();;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void jbInt()
    {
        Assert.expect(_fringePattern != null);
        
        // Fringe Pattern size must be set to 640 x 480.
        // This is the DLP projector maximum resolution.
        this.setSize(_MAX_WIDTH_IN_PIXEL, _MAX_HEIGHT_IN_PIXEL);
        
//        _glCanvas.addMouseMotionListener(new MouseMotionAdapter()
//        {
//            public void mouseMoved(MouseEvent mouseEvent)
//            {
//                handleMouseMoved(mouseEvent);
//            }
//        });
        
        this.add(_glCanvas);
        this.setUndecorated(true);
    }
    
//    /**
//     * @author Cheah Lee Herng 
//     */
//    private void handleMouseMoved(MouseEvent mouseEvent)
//    {
//        Component component = mouseEvent.getComponent();
//        Point point = mouseEvent.getPoint();
//        
//        if (component.getBounds().contains(point))
//        {
//            _robot.mouseMove(_dialogStartingPositionX - 1, mouseEvent.getY());
//        }
//    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void displayWhiteLight(OpticalCameraIdEnum cameraId)
    {
        Assert.expect(cameraId != null);
        Assert.expect(_glCanvas != null);
        
        setupFringePatternProperties(cameraId);
        FringePattern.setIntensityRatio(_cameraIntensityRatio);
        FringePattern.setOffset(_fringePatternOffset);
        
        _fringePattern.setDisplayMode(FringePatternDisplayModeEnum.WHITE_LIGHT);
        this.setVisible(true);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void closeFringePattern()
    {
        Assert.expect(_glCanvas != null);
        
//        _fringePattern.dispose(_glCanvas); 
        FringePattern.setFinishDisplay(false);
        
        if (this.isVisible())
        {
            this.setVisible(false);
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void displayFringePatternShiftOne(OpticalCameraIdEnum cameraId)
    {
        Assert.expect(cameraId != null);
        Assert.expect(_glCanvas != null);
        
        setupFringePatternProperties(cameraId);
        FringePattern.setIntensityRatio(_cameraIntensityRatio);
        FringePattern.setOffset(_fringePatternOffset);
        
        _fringePattern.setDisplayMode(FringePatternDisplayModeEnum.FRINGE_PATTERN_SHIFT_ONE);        
        this.setVisible(true);        
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void displayFringePatternShiftTwo(OpticalCameraIdEnum cameraId)
    {
        Assert.expect(cameraId != null);
        Assert.expect(_glCanvas != null);
        
        setupFringePatternProperties(cameraId);
        FringePattern.setIntensityRatio(_cameraIntensityRatio);
        FringePattern.setOffset(_fringePatternOffset);
        
        _fringePattern.setDisplayMode(FringePatternDisplayModeEnum.FRINGE_PATTERN_SHIFT_TWO);        
        this.setVisible(true);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void displayFringePatternShiftThree(OpticalCameraIdEnum cameraId)
    {
        Assert.expect(cameraId != null);
        Assert.expect(_glCanvas != null);
        
        setupFringePatternProperties(cameraId);
        FringePattern.setIntensityRatio(_cameraIntensityRatio);
        FringePattern.setOffset(_fringePatternOffset);
        
        _fringePattern.setDisplayMode(FringePatternDisplayModeEnum.FRINGE_PATTERN_SHIFT_THREE);        
        this.setVisible(true);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void displayFringePatternShiftFour(OpticalCameraIdEnum cameraId)
    {
        Assert.expect(cameraId != null);
        Assert.expect(_glCanvas != null);
        
        setupFringePatternProperties(cameraId);
        FringePattern.setIntensityRatio(_cameraIntensityRatio);
        FringePattern.setOffset(_fringePatternOffset);
        
        _fringePattern.setDisplayMode(FringePatternDisplayModeEnum.FRINGE_PATTERN_SHIFT_FOUR);        
        this.setVisible(true);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    private void setupFringePatternProperties(OpticalCameraIdEnum cameraId)
    {
        Assert.expect(cameraId != null);
        
        if (cameraId.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1))
        {
            _cameraIntensityRatio = Config.getInstance().getDoubleValue(PspCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_1_INTENSITY_RATIO);
            _fringePatternOffset = Config.getInstance().getIntValue(PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_1_OFFSET);
            _globalIntensity = Config.getInstance().getDoubleValue(PspCalibEnum.CAL_FRINGE_PATTERN_CAMERA_1_GLOBAL_INTENSITY);
        }
        else if (cameraId.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_2))
        {
            _cameraIntensityRatio = Config.getInstance().getDoubleValue(PspCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_2_INTENSITY_RATIO);
            _fringePatternOffset = Config.getInstance().getIntValue(PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_2_OFFSET);
            _globalIntensity = Config.getInstance().getDoubleValue(PspCalibEnum.CAL_FRINGE_PATTERN_CAMERA_2_GLOBAL_INTENSITY);
        }
        else
            Assert.expect(false, "Invalid camera Id");
    }
}
