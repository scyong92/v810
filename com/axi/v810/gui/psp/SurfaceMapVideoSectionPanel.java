package com.axi.v810.gui.psp;

import java.awt.*;
import java.awt.image.*;

/**
 *
 * @author Ying-Huan.Chu
 */
public class SurfaceMapVideoSectionPanel extends AbstractSurfaceMapSectionPanel
{
  /**
   * Constructor for Small FOV
   * @author Ying-Huan.Chu
   */
  protected SurfaceMapVideoSectionPanel(SurfaceMapSectionPanelResolutionEnum surfaceMapImageSectionPanelEnum)
  {
    super(surfaceMapImageSectionPanelEnum);
  }
  
  /**
   * Constructor for Larger FOV
   * @author Ying-Huan.Chu
   */
  public SurfaceMapVideoSectionPanel(SurfaceMapSectionPanelResolutionEnum surfaceMapImageSectionPanelEnum, boolean isEnabled)
  {
    super(surfaceMapImageSectionPanelEnum, isEnabled);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public SurfaceMapSectionTypeEnum getSurfaceMapSectionTypeEnum()
  {
    return SurfaceMapSectionTypeEnum.VIDEO;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  protected void paintComponent(Graphics g)
  {
    // do nothing.
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setContent(BufferedImage bufferedImage)
  {
    // not used.
  }
}
