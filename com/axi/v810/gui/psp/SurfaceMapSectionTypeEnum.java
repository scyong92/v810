package com.axi.v810.gui.psp;

/**
 * SurfaceMapSectionTypeEnum class
 * @author Ying-Huan.Chu
 */
public class SurfaceMapSectionTypeEnum extends com.axi.util.Enum
{
  private String _name;
  private static int _index = -1;
  
  public static SurfaceMapSectionTypeEnum IMAGE = new SurfaceMapSectionTypeEnum(++_index, "Image");
  public static SurfaceMapSectionTypeEnum VIDEO = new SurfaceMapSectionTypeEnum(++_index, "Video");
  
  /**
   * SurfaceMapSectionTypeEnum Constructor
   * @author Ying-Huan.Chu
   */
  private SurfaceMapSectionTypeEnum(int id, String name)
  {
    super(id);
    _name = name;
  }
  
  /**
   * @author Ying-Huan.Chu
   * @return get the name of the SurfaceMapSectionTypeEnum
   */
  public String getName()
  {
    return _name;
  }
}
