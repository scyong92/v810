package com.axi.v810.gui.psp;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.hardware.*;

/**
 * The panel that displays the image and video captured by the PSP Camera. 
 * Please note that this class must only 'extends' java.awt.Panel as this class requires the usage of Window Handle 
 * and if JPanel is used instead, the live video (when running Optical System Fiducial Adjustment)  will not be able to be displayed.
 * 
 * @author Cheah Lee Herng
 */
public class PspLiveVideoPanel extends java.awt.Panel
{
  public static final int LIVE_VIEW_INSPECTABLE_OFFSET = 20;
  private AbstractOpticalCamera _abstractOpticalCamera;
  private MathUtilEnum _unitEnum;
  private int _windowHandle = 0;
  private BorderLayout _inputPanelBorderLayout = new BorderLayout();
  private int _xLocation = 0;
  private int _yLocation = 0;
  java.awt.Label xyLocationLabel = new java.awt.Label();
  private float[] _heightMapInMillimeter;
  
  private JPanel _surfaceMapImagePanel = new JPanel();
  private GridLayout _surfaceMapImagePanelLayout = new GridLayout(1, 1);
  private SurfaceMapFovTypeEnum _surfaceMapFovTypeEnum;
  private SurfaceMapSectionCreator _surfaceMapSectionCreator;
  
  /**
   * @author Cheah Lee Herng
   */
  public PspLiveVideoPanel(AbstractOpticalCamera abstractOpticalCamera, MathUtilEnum unitEnum)
  {
    Assert.expect(abstractOpticalCamera != null);
    Assert.expect(unitEnum != null);
    
    _abstractOpticalCamera = abstractOpticalCamera;
    _unitEnum = unitEnum;

    this.setLayout(_inputPanelBorderLayout);
    // Uncomment these if Larger FOV is enabled.
//    if (abstractOpticalCamera.isLargerFov())
//      _surfaceMapFovTypeEnum = SurfaceMapFovTypeEnum.LARGE_FOV;
//    else
      _surfaceMapFovTypeEnum = SurfaceMapFovTypeEnum.SMALL_FOV;
    
    _surfaceMapSectionCreator = new SurfaceMapSectionCreator(_surfaceMapFovTypeEnum);
    
    // Uncomment these if Larger FOV is enabled.
    /*if (_surfaceMapFovTypeEnum.equals(SurfaceMapFovTypeEnum.LARGE_FOV))
    {
      _surfaceMapImagePanelLayout.setRows(2);
      _surfaceMapImagePanelLayout.setColumns(2);
    }
    else
    {
      _surfaceMapImagePanelLayout.setRows(1);
      _surfaceMapImagePanelLayout.setColumns(1);
    }
    _surfaceMapImagePanel.setLayout(_surfaceMapImagePanelLayout);
    this.add(_surfaceMapImagePanel, BorderLayout.CENTER);*/
    
//    xyLocationLabel.setText("X:" + _xLocation + "  " + "Y:" + _yLocation);
    this.add(xyLocationLabel, BorderLayout.SOUTH);
    xyLocationLabel.setVisible(false);

    setCursor(new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR));
    
    addMouseMotionListener(new MouseMotionAdapter()
    {
      public void mouseMoved(MouseEvent mouseEvent)
      {
        handleMouseMoved(mouseEvent);
      }
    });
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public PspLiveVideoPanel(AbstractOpticalCamera abstractOpticalCamera, MathUtilEnum unitEnum, boolean isLiveViewMode)
  {
    Assert.expect(abstractOpticalCamera != null);
    Assert.expect(unitEnum != null);
    
    _abstractOpticalCamera = abstractOpticalCamera;
    _unitEnum = unitEnum;

    this.setLayout(_inputPanelBorderLayout);
    // Uncomment these if Larger FOV is enabled.
//    if (abstractOpticalCamera.isLargerFov())
//      _surfaceMapFovTypeEnum = SurfaceMapFovTypeEnum.LARGE_FOV;
//    else
      _surfaceMapFovTypeEnum = SurfaceMapFovTypeEnum.SMALL_FOV;
    
    _surfaceMapSectionCreator = new SurfaceMapSectionCreator(_surfaceMapFovTypeEnum);
    // Uncomment these if Larger FOV is enabled.
    /*if (_isLargerFov)
    {
      _surfaceMapImagePanelLayout.setRows(2);
      _surfaceMapImagePanelLayout.setColumns(2);
    }
    else
    {
      _surfaceMapImagePanelLayout.setRows(1);
      _surfaceMapImagePanelLayout.setColumns(1);
    }
    _surfaceMapImagePanel.setLayout(_surfaceMapImagePanelLayout);
    this.add(_surfaceMapImagePanel, BorderLayout.CENTER);*/
    
    xyLocationLabel.setText("X:" + _xLocation + "  " + "Y:" + _yLocation);
    this.add(xyLocationLabel, BorderLayout.SOUTH);

    setCursor(new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR));
    
    addMouseMotionListener(new MouseMotionAdapter()
    {
      public void mouseMoved(MouseEvent mouseEvent)
      {
        handleMouseMoved(mouseEvent);
      }
    });
  }
  
  /**
   * @author Ying-Huan.Chu
   * @param x x-coordinate of the point.
   * @param y y-coordinate of the point.
   * @return true if the point is within the enabled SurfaceMapImageSection, return false otherwise.
   */
  public boolean isWithinEnabledSurfaceMapImageSection(int x, int y)
  {
    AbstractSurfaceMapSectionPanel surfaceMapSectionPanel = getCurrentEnabledSurfaceMapSection();
    if (surfaceMapSectionPanel != null)
    {
      return (surfaceMapSectionPanel.contains(x, y));
    }
    else
    {
      return false;
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void handleMouseMoved(MouseEvent mouseEvent)
  {
    _xLocation = mouseEvent.getX();
    _yLocation = mouseEvent.getY();
    if (isWithinEnabledSurfaceMapImageSection(_xLocation, _yLocation))
    {
      if (_heightMapInMillimeter != null && _abstractOpticalCamera.isHeightMapValueAvailable())
      {
        double convertedAverageZHeight  = getConvertedAverageZHeight(_xLocation, _yLocation);
        double convertedFinalZHeight    = getConvertedFinalZHeight(_xLocation, _yLocation);

        xyLocationLabel.setText("X:" + _xLocation + "  Y:" + _yLocation + " Z-Height from PSP (" + _unitEnum.toString() 
                + "): " + convertedAverageZHeight + "  Final Z-height (" + _unitEnum.toString() + "): " + convertedFinalZHeight);
      }
      else
      {
        //Kok Chun, Tan - Do not show Z-Height information
        xyLocationLabel.setText("X:" + _xLocation + "  Y:" + _yLocation);
      }
      setCursor(new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR));
    }
    else
    {
      setCursor(null);
    }
  }
 
  /**
   * @author Jack Hwee
   */
  public int getXLocation()
  {
    return _xLocation;
  }

  /**
   * @author Jack Hwee
   */
  public int getYLocation()
  {
    return _yLocation;
  }
  
  /**
   * @author Jack Hwee
   */
  public double getAverageZHeightInNanometer(int x, int y)
  {
    if (_heightMapInMillimeter == null)
      return 0;
    
    return calculateAverageZHeightInNanometer(x, y);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public double getFinalZHeightInNanometer(int x, int y)
  {
    double averageZHeightInNanometer = getAverageZHeightInNanometer(x, y);
    return calculateFinalZHeightInNanometer(averageZHeightInNanometer);
  }
  
  /**
   * This function returns a 4-decimal Z-height from height map.
   * 
   * @author Cheah Lee Herng 
   */
  public double getConvertedAverageZHeight(int x, int y)
  {
    double zHeightInNanometers = getAverageZHeightInNanometer(x, y);
    double convertedZHeight = MathUtil.convertUnits(zHeightInNanometers, MathUtilEnum.NANOMETERS, _unitEnum);
    return MathUtil.roundToPlaces(convertedZHeight, 4);
  }
  
  /**
   * This function returns a 4-decimal final Z-height (taking offset into
   * consideration).
   * 
   * @author Cheah Lee Herng 
   */
  public double getConvertedFinalZHeight(int x, int y)
  {
    double finalZHeightInNanometers = getFinalZHeightInNanometer(x, y);
    double convertedZHeight = MathUtil.convertUnits(finalZHeightInNanometers, MathUtilEnum.NANOMETERS, _unitEnum);
    return MathUtil.roundToPlaces(convertedZHeight, 4);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setParentWindowHandle(int windowHandle)
  {
    _windowHandle = windowHandle;
  }

  /**
   * @author Cheah Lee Herng
   */
  public int getParentWindowHandle()
  {
    return _windowHandle;
  }

  /**
   * @author Jack Hwee
   */
  public void setHeightMapArray(float[] array, final int rows, final int cols)
  {
    if (array.length != (rows*cols))
    {
      System.out.println("array.length " + array.length);
      throw new IllegalArgumentException("Invalid array length");
    }
    
    if (_heightMapInMillimeter != null)
      _heightMapInMillimeter = null;
    
    _heightMapInMillimeter = array;
    
    int pixelAverage = 100 / 2;
    for (int row = pixelAverage; row < rows-pixelAverage; ++row)
    {
      for (int col = pixelAverage; col < cols-pixelAverage; ++col)
      {
        float totalZHeightValues = 0;
        float averageZHeightValue = 0;
        float zHeightValue = 0;
        int coordinateLocation = row + (PspHardwareEngine.getInstance().getImageWidth() * col);
        for(int i = coordinateLocation - pixelAverage; i < coordinateLocation + pixelAverage; ++i)
        {
          // XCR-3182 System crash when save rectangle
          if (i >= _heightMapInMillimeter.length)
            break;
          
          zHeightValue = _heightMapInMillimeter[i];
          totalZHeightValues += zHeightValue;
        }
        averageZHeightValue = totalZHeightValues / (pixelAverage * 2);
        
        // XCR-3182 System crash when save rectangle
        if (coordinateLocation < _heightMapInMillimeter.length)
          _heightMapInMillimeter[coordinateLocation] = averageZHeightValue;
      }
    }
  }
  
   /**
   * @author Jack Hwee
   * Average the single pixel z-height with 10x10 pixel range
   */
  private double calculateAverageZHeightInNanometer(final int positionX, final int positionY)
  {
    setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
    double totalZHeightInNanometer = 0;
    double averageZHeightInNanometer = 0;
    double zHeightInNanometer = 0;
    
    int offsetX = Math.max(positionX - (int)(PspHardwareEngine.getInstance().getImageRoiWidth() * 0.5), 0);
    int offsetY = Math.max(positionY - (int)(PspHardwareEngine.getInstance().getImageRoiHeight() * 0.5), 0);
    
    int startX  = offsetX + (Math.max(offsetY - 1, 0) * PspHardwareEngine.getInstance().getImageWidth());    
    
    for(int row=1; row <= PspHardwareEngine.getInstance().getImageRoiHeight(); ++row)
    {
      for(int col = startX; col < (startX + PspHardwareEngine.getInstance().getImageRoiWidth()); ++col)
      {
        // XCR-3182 System crash when save rectangle
        if (col >= _heightMapInMillimeter.length)
          break;
        
        zHeightInNanometer = MathUtil.convertMillimetersToNanometers(_heightMapInMillimeter[col]);
        totalZHeightInNanometer += zHeightInNanometer;
      }
      startX += (row * PspHardwareEngine.getInstance().getImageWidth());
    }
    
    averageZHeightInNanometer = totalZHeightInNanometer / 
                                (PspHardwareEngine.getInstance().getImageRoiWidth() * PspHardwareEngine.getInstance().getImageRoiHeight());                                 
    return averageZHeightInNanometer;
  }
  
   /**
   * @author Jack Hwee
   */
  private double calculateFinalZHeightInNanometer(double zHeightInNanometersFromPsp)
  {
    return (zHeightInNanometersFromPsp + 
            PspEngine.getInstance().getOffset(PspEngine.getInstance().getPspModuleEnum(_abstractOpticalCamera.getOpticalCameraIdEnum())));    
  }
  
  /**
   * Get Z-Height map array.
   * @author Ying-Huan.Chu
   */
  public float[] getZHeightMapArray()
  {
    return _heightMapInMillimeter;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void startImageMode(java.awt.image.BufferedImage bufferedImage)
  {
    _surfaceMapSectionCreator.startImageMode(bufferedImage);
    
    add(_surfaceMapSectionCreator.getCurrentSurfaceMapSection());
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void startVideoMode()
  {
    _surfaceMapSectionCreator.startVideoMode();
    
    add(_surfaceMapSectionCreator.getCurrentSurfaceMapSection());
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void skipToPreviousSection()
  {
    // top-left section is the first image section in Larger FOV. Do nothing if user clicks 'Previous Section' button when current selected section is the top-left section.
    _surfaceMapSectionCreator.skipToPreviousSection();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void skipToNextSection()
  {
    // bottom-right section is the last image section in Larger FOV. Do nothing if user clicks 'Next Section' button when current selected section is the bottom-right section.
    _surfaceMapSectionCreator.skipToNextSection();
  }
  
  /**
   * @author Ying-Huan.Chu
   * @return the SurfaceMapImageSection that's currently enabled.
   */
  public AbstractSurfaceMapSectionPanel getCurrentEnabledSurfaceMapSection()
  {
    return _surfaceMapSectionCreator.getCurrentSurfaceMapSection();
  }
  
  /**
   * Add Surface Map Point to UI.
   * @author Ying-Huan.Chu
   * @param x x-coordinate of the point.
   * @param y y-coordinate of the point.
   */
  public void addSurfaceMapPoint(int x, int y)
  {
    AbstractSurfaceMapSectionPanel surfaceMapSectionPanel = getCurrentEnabledSurfaceMapSection();
    if (surfaceMapSectionPanel != null) // sanity check
    {
      if (surfaceMapSectionPanel.contains(x, y))
      {
        if (surfaceMapSectionPanel.getNumberOfSurfaceMapPoints() < AbstractSurfaceMapSectionPanel.MAXIMUM_NUMBER_OF_SURFACE_MAP_POINTS)
        {
          Point positionInPixel = new Point(x, y);
          surfaceMapSectionPanel.addSurfaceMapPoint(positionInPixel);
        }
      }
    }
  }
  
  /**
   * This function is called when the system is loading saved Surface Map points to the UI.
   * @author Ying-Huan.Chu
   * @param x x-coordinate of the point.
   * @param y y-coordinate of the point.
   */
  public void setupSurfaceMapPoint(int x, int y)
  {
    AbstractSurfaceMapSectionPanel opticalImageSection = getSurfaceMapImageSection(x, y);
    if (opticalImageSection != null) // sanity check
    {
      Point positionInPixel = new Point(x, y);
      opticalImageSection.addSurfaceMapPoint(positionInPixel);
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   * @param x x-coordinate of the point.
   * @param y y-coordinate of the point.
   * @return the SurfaceMapImageSection the Point(x,y) is in.
   */
  public AbstractSurfaceMapSectionPanel getSurfaceMapImageSection(int x, int y)
  {
    return _surfaceMapSectionCreator.getSurfaceMapSectionPanel(x, y);
  }
  
  /**
   * @author Ying-Huan.Chu
   * @return all Surface Map points defined in all SurfaceMapImageSections.
   */
  public java.util.List<SurfaceMapPointPanel> getAllSurfaceMapPoints()
  {
    return _surfaceMapSectionCreator.getAllSurfaceMapPoints();
  }
}
