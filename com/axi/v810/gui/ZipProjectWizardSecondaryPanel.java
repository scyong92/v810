package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.LoginManager;
import com.axi.v810.util.*;

/**
 * This panel will allow the user to select if they would like to include verification images and which image sets to include
 * in their zip file. This panel is the second panel in the zip project wizard.
 *
 * @author Erica Wheatcroft
 */
public class ZipProjectWizardSecondaryPanel extends WizardPanel implements Observer
{
  private JLabel _currentZipSizeLabel = new JLabel();
  private JLabel _availableDestinationSpaceLabel = new JLabel();
  private JCheckBox _addVerificationImagesCheckbox;
  private JCheckBox _addFocusImageSetCheckbox;
  private JScrollPane _availableImageSetsScrollBar;
  private JSortTable _availavbleImageSetsTable;
  private AvailableAndSelectedImageSetsTableModel _tableModel = new AvailableAndSelectedImageSetsTableModel(true);
  private JButton _selectAllButton;

  private JPanel _buttonPanel = null;
  private JPanel _imageSetsPanel = null;
  private JPanel _availableImageSetsPanel = null;
  private JPanel _zipDetailsPanel = null;
  private JPanel _verificationImagesPanel = null;
  private CompoundBorder _zipDetailsBorder = BorderFactory.createCompoundBorder(new TitledBorder(
      BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_DETAILS_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(5,5,5,5));
  //zip history log file - hsia-fen.tan
  private CompoundBorder _zipDetailsBorder2 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
           StringLocalizer.keyToString("ZIP_PROJECT_ADD_HISTORY_LOG_INSTRUCTION_KEY")),BorderFactory.createEmptyBorder(2, 5, 2, 5));

  private ProjectArchiver _archiver = null;

  private WizardDialog _dialog = null;

  private static final String _MB_LABEL = StringLocalizer.keyToString("ZIP_MB_UNITS_KEY");
  
  //add history log file
  private JCheckBox _addHistoryLogCheckbox;
  private JPanel _zipHistoryLog = null;
  private String projName;
  private JPanel historyFilePanel = null;
  private UserTypeEnum _currentUserType = null;

  /**
   * @author Erica Wheatcroft
   */
  public ZipProjectWizardSecondaryPanel(WizardDialog dialog, ProjectArchiver archiver)
  {
    super(dialog);

    Assert.expect(dialog != null);
    Assert.expect(archiver != null);

    _dialog = dialog;
    _archiver = archiver;

    createPanel();

    GuiObservable.getInstance().addObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null, "Name of panel is null");
    Assert.expect(name.length() > 0, "Name of panel is an empty string");
    _name = name;
  }

  /**
   * @author Erica Wheatcroft
   */
  public String getWizardName()
  {
    Assert.expect(_name != null, "Name of panel is not set");
    return _name;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    // do nothing
  }

  /**
   * @author Erica Wheatcroft
   */
  public void removeObservers()
  {
    // remove the observer so that we will not get events while we are not active.
    GuiObservable.getInstance().deleteObserver(this);
  }


  /**
   * @author Erica Wheatcroft
   */
  public void populatePanelWithData()
  {

    // set the title
   _dialog.changeTitle(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_PANEL2_TITLE_KEY"));

    // set up the border
    _zipDetailsBorder = BorderFactory.createCompoundBorder(new TitledBorder(
      BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_DETAILS_PANEL_TITLE_KEY") + " "  + _archiver.getProjectName()),
      BorderFactory.createEmptyBorder(2,5,2,5));
    _zipDetailsPanel.setBorder(_zipDetailsBorder);

    // first populate the current size of the zip.
    updateCurrentZipSize();

    // second populate the amount of space left on the destination
    calculateAvailableSpaceOnDestination();

    // third figure out how big the verification images are if they exist
    populateVerificationImagesPanel();

    // forth populate table with image sets if they exist.
    populateAvailableImageSetsPanel();
    
    //last populate history log if they exist -hsia-fen.tan
    populateHistoryLogFile();

    // validate the panel to set the state of the navigation buttons
    validatePanel();
  }

    /**
   * @author hsia-fen.tan
   * check whether history log file exist
   */
  private void populateHistoryLogFile()
  {
   Assert.expect(_archiver.hasProjectName());
   projName=_archiver.getProjectName();
   java.util.List<String> Historyfile = FileName.getHistoryFiles(projName);
   historyFilePanel.removeAll();
   _currentUserType=LoginManager.getInstance().getUserTypeFromLogIn();
    if (_currentUserType.equals(UserTypeEnum.ADMINISTRATOR))
    {
      if (Historyfile == null || Historyfile.size() == 0)
      {
        JLabel noHistoryLogLabel = new JLabel(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_NO_HISTORY_LOG_FILE_EXIST_KEY"));
        historyFilePanel.add(noHistoryLogLabel, BorderLayout.WEST);
      }
      else
      {
        _addHistoryLogCheckbox.setText(StringLocalizer.keyToString("ZIP_PROJECT_ADD_HISTORY_LOG_FILE_INSTRUCTION_KEY"));
        historyFilePanel.add(_addHistoryLogCheckbox, BorderLayout.CENTER);
      }
    }
    else
    {
      JLabel noHistoryLogLabel = new JLabel(StringLocalizer.keyToString("ZIP_PROJECT_ONLY_ADMINISTRATOR_ALLOW_ZIP_FILE_KEY"));
      historyFilePanel.add(noHistoryLogLabel, BorderLayout.WEST);
    }
    
    repaint();
    revalidate();
  }
  
  /**
   * @author Erica Wheatcroft
   */
  private void populateAvailableImageSetsPanel()
  {
    Assert.expect(_archiver.hasProjectName());
    if(((ZipProjectWizardDialog)_dialog).repopulatePanels() == true)
    {
      String projectName = _archiver.getProjectName();

      // remove anything that was previously displayed.
      _imageSetsPanel.removeAll();

      // first check to see if for the project the user has selected if there exists verification images.
      boolean exceptionOccurred = false;
      java.util.List<ImageSetData> imageSets = null;
      boolean autoFocusImageSetExists = false;
      boolean imageSetsExist = false;
      try
      {
        imageSets = ImageManager.getInstance().getCompatibleImageSetData(projectName);
        autoFocusImageSetExists = ImageManager.getInstance().doAdjustFocusImagesExist(projectName);
      }
      catch (DatastoreException de)
      {
        exceptionOccurred = true;
      }

      if (imageSets == null || exceptionOccurred || imageSets.isEmpty())
      {
        JLabel noImagesExistLabel = new JLabel(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_NO_IMAGES_SETS_EXIST_KEY"));
        noImagesExistLabel.setHorizontalAlignment(JLabel.CENTER);
        _imageSetsPanel.add(noImagesExistLabel, BorderLayout.CENTER);
        imageSetsExist = false;
      }
      else if (imageSets.isEmpty() == false && exceptionOccurred == false)
      {
        _imageSetsPanel.add(_availableImageSetsPanel, BorderLayout.CENTER);
        _imageSetsPanel.add(_buttonPanel, BorderLayout.SOUTH);
        _tableModel.populateWithImageSetData(imageSets, _archiver.getImageSetsToArchive());
        setupTable();
        imageSetsExist = true;
      }

      createButtonPanel(imageSetsExist, autoFocusImageSetExists);

      repaint();
      revalidate();
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void calculateAvailableSpaceOnDestination()
  {
    Assert.expect(_archiver.hasDestinationFilename());
    String destinationFileName = _archiver.getDestinationFilename();

    String parentDirectory = FileUtil.getAbsolutePathRoot(destinationFileName);
    Assert.expect(parentDirectory != null);

    long sizeAvailableInMB = FileUtil.getAvailableDiskSpaceInBytes(parentDirectory) / (1024 * 1024);

    _availableDestinationSpaceLabel.setText( StringLocalizer.keyToString("ZIP_PROJECT_AVAILABLE_SPACE_KEY") + " " + sizeAvailableInMB + " " + _MB_LABEL);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateCurrentZipSize()
  {
    double currentZipSizeInBytes = _archiver.getApproximateZippedSizeInBytes();
    double megaBytes = MathUtil.convertBytesToMegabytes((int)currentZipSizeInBytes);
    _currentZipSizeLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_APPROXIMATE_ZIP_SIZE_KEY") + " " + MathUtil.roundToPlaces(megaBytes, 1) + " " + _MB_LABEL);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateVerificationImagesPanel()
  {
    Assert.expect(_archiver.hasProjectName());
    String projectName = _archiver.getProjectName();

    // remove anything that was previously displayed.
    _verificationImagesPanel.removeAll();

    // first check to see if for the project the user has selected if there exists verification images.

    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    if(ImageManager.getInstance().hasValidVerificationImages(projectName, true, false))
    {
      double currentVerificationCompressedSize = _archiver.getCompressedVerificationImagesSizeInBytes();
      double compressedSizeRounded = MathUtil.roundToPlaces(MathUtil.convertBytesToMegabytes((int)currentVerificationCompressedSize), 1);
      _addVerificationImagesCheckbox.setText( StringLocalizer.keyToString(
                                               new LocalizedString("ZIP_PROJECT_ADD_VERIFICATION_IMAGES_KEY",
                                                                   new Object[]{compressedSizeRounded, _MB_LABEL})));
      _verificationImagesPanel.add(_addVerificationImagesCheckbox);
    }
    else
    {
      JLabel noImagesExistLabel = new JLabel(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_NO_VERIFICATION_IMAGES_EXIST_KEY"));
      _verificationImagesPanel.add(noImagesExistLabel);
    }

    repaint();
    revalidate();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void validatePanel()
  {
    if (_archiver.hasProjectName() && _archiver.hasDestinationFilename())
    {
      // if we have gotten this far then we know the user has specified all the necessary information. so we can fire
      // the button state event next can be set to enabled, cancel can be enabled, back is enabled, and generate is enabled.
      WizardButtonState buttonState = new WizardButtonState(true, true, false, true);
      _observable.updateButtonState(buttonState);
    }
  }

  /**
  * @author Erica Wheatcroft
  */
 private void setupTable()
 {
   TableColumn test = _availavbleImageSetsTable.getColumnModel().getColumn(0);
   CheckCellEditor cellEditor = new CheckCellEditor(_availavbleImageSetsTable.getBackground(), _availavbleImageSetsTable.getForeground());
   test.setCellEditor( cellEditor );

   TableColumn selectedImageSetsNameCol = _availavbleImageSetsTable.getColumnModel().getColumn(1);
   ImageSetRenderer imageSetRenderForSelectedImageSets = new ImageSetRenderer();
   selectedImageSetsNameCol.setCellRenderer(imageSetRenderForSelectedImageSets);

   TableColumn selectedImageSetsDateCol = _availavbleImageSetsTable.getColumnModel().getColumn(2);
   DefaultTableCellRenderer selectedImageSetsDateColumnCenterRenderer = new DefaultTableCellRenderer();
   selectedImageSetsDateColumnCenterRenderer.setHorizontalAlignment( JLabel.CENTER );
   selectedImageSetsDateCol.setCellRenderer(selectedImageSetsDateColumnCenterRenderer);

   TableColumn selectedImageSetsPopulatedCol = _availavbleImageSetsTable.getColumnModel().getColumn(3);
   DefaultTableCellRenderer selectedImageSetsPopulatedColumnCenterRenderer = new DefaultTableCellRenderer();
   selectedImageSetsPopulatedColumnCenterRenderer.setHorizontalAlignment( JLabel.CENTER );
   selectedImageSetsPopulatedCol.setCellRenderer(selectedImageSetsPopulatedColumnCenterRenderer);

   TableColumn selectedImageSetsPercentCompatibleCol = _availavbleImageSetsTable.getColumnModel().getColumn(4);
   NumericRenderer selectedImageSetsPercentCompatibleColCenterRenderer = new NumericRenderer(0, JLabel.CENTER);
   selectedImageSetsPercentCompatibleCol.setCellRenderer(selectedImageSetsPercentCompatibleColCenterRenderer);


   // set up column widths
   ColumnResizer.adjustColumnPreferredWidths(_availavbleImageSetsTable, true);

   int padding = 175;
   int origTableWidth = _availavbleImageSetsTable.getMinimumSize().width;
   int scrollHeight = _availavbleImageSetsTable.getPreferredSize().height;
   int tableHeight = _availavbleImageSetsTable.getPreferredSize().height;
   int prefHeight = scrollHeight;
   if (tableHeight < scrollHeight)
   {
     prefHeight = tableHeight;
     Dimension prefScrollSize = new Dimension(origTableWidth + padding, prefHeight);
     _availavbleImageSetsTable.setPreferredScrollableViewportSize(prefScrollSize);
   }

   // now lets set up the % of relestate for each column.
   // set up column widths
   int iTotalColumnWidth = _availavbleImageSetsTable.getColumnModel().getTotalColumnWidth();
   _availavbleImageSetsTable.getColumnModel().getColumn(0).setPreferredWidth((int)(iTotalColumnWidth * .15));
   _availavbleImageSetsTable.getColumnModel().getColumn(1).setPreferredWidth((int)(iTotalColumnWidth * .40));
   _availavbleImageSetsTable.getColumnModel().getColumn(2).setPreferredWidth((int)(iTotalColumnWidth * .20));
   _availavbleImageSetsTable.getColumnModel().getColumn(3).setPreferredWidth((int)(iTotalColumnWidth * .10));
   _availavbleImageSetsTable.getColumnModel().getColumn(4).setPreferredWidth((int)(iTotalColumnWidth * .15));

 }

 /**
  * @author Erica Wheatcroft
  */
  private void createPanel()
  {
    setLayout(new BorderLayout(5,5));
    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    _zipDetailsPanel = new JPanel(new GridLayout(0,2,5,5));
    _zipDetailsPanel.setBorder(_zipDetailsBorder);
    JPanel currentZipSizePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _currentZipSizeLabel.setFont(FontUtil.getBoldFont(_currentZipSizeLabel.getFont(),Localization.getLocale()));
    currentZipSizePanel.add(_currentZipSizeLabel);
    _zipDetailsPanel.add(currentZipSizePanel);
    JPanel availableDesinationSpacePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    _availableDestinationSpaceLabel.setFont(FontUtil.getBoldFont(_availableDestinationSpaceLabel.getFont(),Localization.getLocale()));
    availableDesinationSpacePanel.add(_availableDestinationSpaceLabel);
    _zipDetailsPanel.add(availableDesinationSpacePanel);
    add(_zipDetailsPanel, BorderLayout.NORTH);

    JPanel centerPanel = new JPanel(new BorderLayout(5, 5));

    _imageSetsPanel = new JPanel(new BorderLayout(5,5));
    _imageSetsPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
        StringLocalizer.keyToString("ZIP_PROJECT_ADD_IMAGE_SETS_INSTRUCTION_KEY")),BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    _availableImageSetsPanel = new JPanel(new BorderLayout());
    _availableImageSetsScrollBar = new JScrollPane();
    _availableImageSetsPanel.add(_availableImageSetsScrollBar, BorderLayout.CENTER);
    _availavbleImageSetsTable = new JSortTable();
    _availableImageSetsScrollBar.getViewport().add(_availavbleImageSetsTable);
    _availavbleImageSetsTable.setModel(_tableModel);
    _imageSetsPanel.add(_availableImageSetsPanel, BorderLayout.CENTER);
    centerPanel.add(_imageSetsPanel, BorderLayout.CENTER);

    _addFocusImageSetCheckbox = new JCheckBox("Add Focus Image Set");

    _selectAllButton = new JButton();
    createButtonPanel(true, true);
    _verificationImagesPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _verificationImagesPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
       StringLocalizer.keyToString("ZIP_PROJECT_ADD_VERIFICATION_IMAGES_INSTRUCTION_KEY") ),BorderFactory.createEmptyBorder(2, 5, 2, 5)));
    _addVerificationImagesCheckbox = new JCheckBox();
    _verificationImagesPanel.add(_addVerificationImagesCheckbox);

    centerPanel.add(_verificationImagesPanel, BorderLayout.NORTH);
    add(centerPanel, BorderLayout.CENTER);
   
    //add history log -hsia-fen.tan   
    _zipHistoryLog = new JPanel(new GridLayout(0, 2, 5, 5));
    _zipHistoryLog.setBorder(_zipDetailsBorder2);
    historyFilePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _addHistoryLogCheckbox = new JCheckBox();
    _zipHistoryLog.add(historyFilePanel);
    add(_zipHistoryLog, BorderLayout.SOUTH);
     
    _selectAllButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        // the user wants to select all the image sets listed.
        _tableModel.selectAllImageSets();
      }
    });
    _addVerificationImagesCheckbox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if(_addVerificationImagesCheckbox.isSelected())
        {
          // tell the archiver to include verification images
          _archiver.setIncludeVerificationImages(true);
          updateCurrentZipSize();
//          // now calculate how much and add to the zip
//          _currentZipSizeInBytes += calculateVerificationImagesSizeInBytes();
        }
        else
        {
          // tell the archiver to NOT include verification images
          _archiver.setIncludeVerificationImages(false);
          updateCurrentZipSize();
//          // now calculate how much to subtract from the zip
//          _currentZipSizeInBytes -= calculateVerificationImagesSizeInBytes();

        }
      }
    });
    _addFocusImageSetCheckbox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_addFocusImageSetCheckbox.isSelected())
        {
          // tell the archiver to include verification images
          _archiver.setIncludeFocusImageSet(true);
          updateCurrentZipSize();
          // now calculate how much and add to the zip
//          _currentZipSizeInBytes += calculateFocusImageSetInBytes();
        }
        else
        {
          // tell the archiver to NOT include verification images
          _archiver.setIncludeFocusImageSet(false);
          updateCurrentZipSize();
          // now calculate how much to subtract from the zip
//          _currentZipSizeInBytes -= calculateFocusImageSetInBytes();

        }
      }
    });
      
    //zip history log file -hsia-fen.tan
      _addHistoryLogCheckbox.addActionListener(new ActionListener()
      {
      public void actionPerformed(ActionEvent e)
      {
          if(_addHistoryLogCheckbox.isSelected())
          {
          // tell the archiver to include history log file
          _archiver.setIncludeHistoryLogFile(true);
          updateCurrentZipSize();
           }
           else
           {
             // tell the archiver to NOT include history log file
            _archiver.setIncludeHistoryLogFile(false);
            updateCurrentZipSize();
          }
      }
     });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createButtonPanel(boolean inspectionImageSetsExist, boolean focusImageSetExists)
  {
    // first remove everything
    if(_buttonPanel != null)
    {
      _imageSetsPanel.remove(_buttonPanel);
      _buttonPanel.removeAll();
    }

    _buttonPanel = new JPanel(new BorderLayout(0,0));
    _selectAllButton.setText(StringLocalizer.keyToString("ZIP_PROJECT_SELECT_ALL_BUTTON_KEY"));
//    if(focusImageSetExists)
//    {
//      JPanel focusImageSetPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
//      focusImageSetPanel.add(_addFocusImageSetCheckbox);
//      _buttonPanel.add(focusImageSetPanel, BorderLayout.WEST);
//    }
    if(inspectionImageSetsExist)
    {
      JPanel selectAllPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
      selectAllPanel.add(_selectAllButton);
      _buttonPanel.add(selectAllPanel, BorderLayout.EAST);
    }

     _imageSetsPanel.add(_buttonPanel, BorderLayout.SOUTH);
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(Observable observable, Object object)
  {
    Assert.expect(observable != null, " observable is null");

    // this should be an update from something already on the swing thread. if not then assert.
    Assert.expect(SwingUtilities.isEventDispatchThread(), "update not from a swing thread");

    if (observable instanceof GuiObservable)
    {
      GuiEventEnum imageSetEnum = ((GuiEvent)object).getGuiEventEnum();
      if (imageSetEnum instanceof ImageSetEventEnum)
      {
        if ((imageSetEnum.equals(ImageSetEventEnum.IMAGE_SET_SELECTED_FOR_ARCHIVE)) ||
            (imageSetEnum.equals(ImageSetEventEnum.IMAGE_SET_NOT_SELECTED_FOR_ARCHIVE)))
        {
          Assert.expect(object != null, "Object is null");
          Assert.expect(object instanceof GuiEvent, "Object is not of type Gui Event");
          Assert.expect(((GuiEvent)object).getSource() instanceof java.util.List, "not of type image set data");
          java.util.List<ImageSetData> imageSets = (java.util.List)((GuiEvent)object).getSource();
          _archiver.clearInspectionImageSets();
          for(ImageSetData imageSet : imageSets)
            _archiver.addInspectionImageSet(imageSet);
          updateCurrentZipSize();
        }
//        else if(imageSetEnum.equals(ImageSetEventEnum.IMAGE_SET_NOT_SELECTED_FOR_ARCHIVE))
//        {
//          Assert.expect(object != null, "Object is null");
//          Assert.expect(object instanceof GuiEvent, "Object is not of type Gui Event");
//          Assert.expect(((GuiEvent)object).getSource() instanceof java.util.List, "not of type image set data");
//          java.util.List<ImageSetData> imageSets = (java.util.List)((GuiEvent)object).getSource();
//          _archiver.clearInspectionImageSets();
//          for (ImageSetData imageSet : imageSets)
//            _archiver.addInspectionImageSet(imageSet);
//          calculateCurrentZipSize();
//        }
//        else
//          Assert.expect(false); // no other events should be sent as we have limited notifications
      }
    }
    else
      Assert.expect(false);
  }
}
