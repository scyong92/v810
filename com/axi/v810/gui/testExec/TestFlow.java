package com.axi.v810.gui.testExec;

import javax.swing.*;

import com.axi.v810.business.testExec.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;
import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 * A common interface to all the test execution modes
 * Known implementing classes: PanelBypassMode, TestDevelopmentMode, ProductionMode
 * @author Andy Mechtenberg
 */
abstract class TestFlow
{
  private static TestFlow _panelBypassFlow;
  private static TestFlow _testDevelopmentFlow;
  private static TestFlow _productionFlow;

  protected static JFrame _frame;
  protected static TestExecPanel _testExecPanel;
  protected static TestExecution _testExecution;
  protected static InspectionEngine _inspectionEngine = null;

  protected HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  protected SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
  private SwingWorkerThread _abortWorkerThread = SwingWorkerThread.getInstance2();
  protected int _demoSerialNumber = 1;

  protected volatile boolean _abort = false;

  /**
   * @author Andy Mechtenberg
   */
  static synchronized TestFlow getInstance(boolean productionTest, JFrame frame, TestExecPanel testExecPanel)
  {
    Assert.expect(frame != null);
    Assert.expect(testExecPanel != null);

    _frame = frame;
    _testExecPanel = testExecPanel;

    _testExecution = TestExecution.getInstance();
    _inspectionEngine = InspectionEngine.getInstance();

    TestFlow testFlow;
    if (productionTest)
    {
      if (_productionFlow == null)
        _productionFlow = new ProductionFlow();
      testFlow = _productionFlow;
    }
    else // non-production mode
    {
      // first, disable all the GUI controls that don't make sense for this mode
      // mainly, this is the serial number queuing stuff
      if (_panelBypassFlow == null)
        _panelBypassFlow = new PanelBypassFlow();
      testFlow = _panelBypassFlow;
    }
    return testFlow;
  }

  /**
    * @author Andy Mechtenberg
    */
   public void abort()
   {
     _abort = true;
     _abortWorkerThread.invokeLater(new Runnable()
     {
       public void run()
       {
         try
         {
           _testExecution.abort();
         }
         catch (final XrayTesterException ex)
         {
           SwingUtils.invokeLater(new Runnable()
           {
             public void run()
             {
               _testExecPanel.displayError(ex);
             }
           });
         }
       }
     });
   }

  abstract void execute() throws XrayTesterException;

  /**
   * @author Andy Mechtenberg
   */
  protected String generateDemoSerialNumber()
  {
    String demoStr = StringLocalizer.keyToString("TESTEXEC_GUI_DEMO_SERIAL_PREFIX_KEY") + _demoSerialNumber;
    _demoSerialNumber++;
    return demoStr;
  }

  /**
   * @author Kok Chun, Tan
   */
  abstract protected void loadVariationSetting(String name);
  
  /**
   * @author Kok Chun, Tan
   */
  abstract protected String getVariationName();
}
