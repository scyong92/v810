/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.testExec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 *
 * @author Jack Hwee
 *  
 */
public class FalseCallTriggeringRemarkDialog extends EscapeDialog
{
  private JFrame _parent;
  
  private JPanel _dialogPanel;

  private JPanel _northPanel;
  private JLabel _warningLabel;
  
  private JTextField _remarkTextField;

  private JPanel _southPanel;
  private JPanel _buttonPanel;
  private JButton _okButton;
  private JButton _cancelButton;
  private String _userInputRemark;
  private JDialog _this;
  
  //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
  private boolean _isSaveButtonPerformed = false;
  private Pattern _falseCallTriggeringLogPattern = Pattern.compile("^([^,]+)\\,([^,]+)\\,([^,]*)\\,([^,]+)\\,(-?\\d+)\\,(-?\\d+)\\,(-?\\d+)\\,([^,]+)\\,(-?\\d+)\\,([^,]+)\\,([^,]+)$"); 
  private final static int _FALSE_CALL_TRIGGERING_LOG_LINE_NUMBER_LIMIT = 300;
  
  /**
   * @param parent
   * @param title
   * @param project
   * @param ex
   * @param font
   */
  public FalseCallTriggeringRemarkDialog(JFrame parent, String title, String warningMessage)
  {
    super(parent, title, true);

    Assert.expect(parent != null);
    Assert.expect(title != null);
  
    _parent = parent;
    _warningLabel = new JLabel(warningMessage);
  
    jbInit();
    pack();
  }

  /**
   * @author Jack Hwee
   */
  private void jbInit()
  {
     _dialogPanel = new JPanel(new BorderLayout(40, 10));
     _dialogPanel.setBorder(BorderFactory.createEmptyBorder(10, 30, 10, 30));
     getContentPane().add(_dialogPanel);

     _northPanel = new JPanel(new BorderLayout(30, 10));
     _dialogPanel.add(_northPanel, BorderLayout.NORTH);
     
     _warningLabel.setForeground(Color.BLACK);
     _warningLabel.setFont(FontUtil.getBoldFont(_warningLabel.getFont(),Localization.getLocale()));
     _warningLabel.setHorizontalAlignment(SwingConstants.CENTER);
     _northPanel.add(_warningLabel, BorderLayout.NORTH);
     
     _this = this;

     _remarkTextField =  new JTextField();
     _remarkTextField.addActionListener(new java.awt.event.ActionListener()
     {
       public void actionPerformed(ActionEvent e)
       {
         //textField_actionPerformed(e);
       }
     });
     _remarkTextField.addKeyListener(new KeyAdapter()
     {
       public void keyReleased(KeyEvent e)
       {
         //someKeyReleased(e);
       }
     });
     
     _northPanel.add(_remarkTextField, BorderLayout.CENTER);
   
     _southPanel = new JPanel(new BorderLayout());
     _dialogPanel.add(_southPanel, BorderLayout.SOUTH);

     _buttonPanel = new JPanel(new FlowLayout());
     _southPanel.add(_buttonPanel, BorderLayout.CENTER);

     _okButton = new JButton(StringLocalizer.keyToString("GUI_SAVE_BUTTON_KEY"));
    
     _okButton.addActionListener(new java.awt.event.ActionListener()
     {
       public void actionPerformed(ActionEvent e)
       {
          saveButton_actionPerformed();
          dispose();
       }
     });
      
     _buttonPanel.add(_okButton);

     _cancelButton = new JButton(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
   
     _cancelButton.addActionListener(new java.awt.event.ActionListener()
     {
       public void actionPerformed(ActionEvent e)
       {
           _userInputRemark = " ";
           dispose();
       }
     });
     
     _buttonPanel.add(_cancelButton);
     
     _remarkTextField.addKeyListener(new java.awt.event.KeyAdapter()
     {
        public void keyPressed(KeyEvent ke)
        {
          if (ke.getKeyCode() == KeyEvent.VK_ENTER)
          {
             saveButton_actionPerformed();
             dispose();
          }
        }
     });
     
     setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
     pack();
     
     //SwingUtils.centerOnComponent(this, _parent);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  private void saveButton_actionPerformed()
  {
    _userInputRemark = _remarkTextField.getText();
     
    if(_userInputRemark.replace(" ","").length() == 0)
    {
      MessageDialog.showErrorDialog(_this, 
                                    StringLocalizer.keyToString("CFGUI_FCT_REMARK_CANNOT_TO_BLANK_ERROR_MESSAGE_KEY"),
                                    StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"), 
                                    true);
      return;
    }
    
    _isSaveButtonPerformed = true;
      
    String falseCallTriggeringLogFileName = FileName.getFalseCallTriggeringLogFullPath(FileName.getFalseCallTriggeringLogLatestFileVersion());
    String errorMessage = "";
    boolean exception = false;
    int lineNumber = 0;
    CSVFileWriterAxi falseCallTriggeringLogFile = new CSVFileWriterAxi(FalseCallMonitoring.getInstance().getFalseCallTriggeringLogColumnHeaderList());
    
    try
    {
      if (FalseCallMonitoring.getInstance().isEnforceWriteRemark())
        FalseCallMonitoring.getInstance().setFalseCallRemarkWrittenNeeded(false);
      
      
      Path readerPath = Paths.get(falseCallTriggeringLogFileName);
      Charset charset = Charset.forName("UTF-8");
      BufferedReader reader = Files.newBufferedReader(readerPath, charset);
      LineNumberReader lineReader = new LineNumberReader(reader);       
      String line;
      String lastLine = null;

      while ((line = lineReader.readLine()) != null) 
      {
        lastLine = line;
        lineNumber++;
      }

      reader.close();
      lineReader.close(); 
      
      SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMdd_HH-mm-ss");
      String datetime = dateTimeFormat.format(new Date());
          
      if (FalseCallMonitoring.getInstance().getCSVFileWriterAxi() != null)
      {
        //CSVFileWriterAxi falseCallTriggeringLogFile = FalseCallMonitoring.getInstance().getCSVFileWriterAxi();
        falseCallTriggeringLogFile.open(falseCallTriggeringLogFileName, true);
        java.util.LinkedList<String> previousLogDataList = FalseCallMonitoring.getInstance().getLogFileData();

        if (previousLogDataList != null && previousLogDataList.isEmpty() == false)
        {
          previousLogDataList.removeLast();
          previousLogDataList.add(_userInputRemark);
          falseCallTriggeringLogFile.writeDataLine(previousLogDataList);
        }
      }
      else
      {
        Matcher matcher = _falseCallTriggeringLogPattern.matcher(lastLine);
        matcher.reset(lastLine); 

        if (matcher.find() == false) 
        {
          throw new BadFormatException(new LocalizedString("CFGUI_FCT_BAD_FORMAT_ERROR_MESSAGE_KEY", null));
        }

        String readTimeStamp = matcher.group(1);
        String readProject = matcher.group(2);
        String readSerialNumber = matcher.group(3);
        String readDefectType = matcher.group(4);
        String readTotalPinsProcessed = matcher.group(5);
        String readTotalDefectivePins = matcher.group(6);
        String readPPMPins = matcher.group(7);
        String readFailMode = matcher.group(8);
        String readMaximumNumberOfErrors = matcher.group(9);
        String readUserName = matcher.group(10);

        falseCallTriggeringLogFile.open(falseCallTriggeringLogFileName, true);

        falseCallTriggeringLogFile.writeDataLine(Arrays.asList(readTimeStamp, readProject, readSerialNumber, readDefectType, readTotalPinsProcessed, 
                                                               readTotalDefectivePins, readPPMPins, readFailMode, readMaximumNumberOfErrors, 
                                                               readUserName, _userInputRemark));
      }
      
      if(lineNumber > _FALSE_CALL_TRIGGERING_LOG_LINE_NUMBER_LIMIT)
      {
        FileUtilAxi.rename(falseCallTriggeringLogFileName, FileName.getFalseCallTriggeringBackUpLogFullPath(datetime));
      }
    }
    catch (IOException io)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(falseCallTriggeringLogFileName);
      dex.initCause(io);
      exception = true;
      errorMessage = dex.getMessage();
    }
    catch (BadFormatException bex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(falseCallTriggeringLogFileName, lineNumber);
      dex.initCause(bex);
      exception = true;
      errorMessage = dex.getMessage();
    }
    catch (DatastoreException dex)
    {
      exception = true;
      errorMessage = dex.getMessage();
    }
    finally
    {
      falseCallTriggeringLogFile.close();
    }
    
    if (exception)
    {
      MessageDialog.showErrorDialog(_this, 
                                    errorMessage,
                                    StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"), 
                                    true);
    }
  }
  
 /**
   * @author Jack Hwee
   */
  public String getUserInputRemark()
  {
      return _userInputRemark;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public boolean isSaveButtonPerformed()
  {
    return _isSaveButtonPerformed;
  }
}
