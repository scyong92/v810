package com.axi.v810.gui.testExec;

import com.axi.util.*;

/**
 * Tnis class holds the current demo mode state information.
 * Demo mode is looping until aborted or until the demo count is achieved.
 * @author Andy Mechtenberg
 */
public class DemoMode
{
  private boolean _demoModeEnabled = false;
  private int _numDemoModeTests = -1;

  /**
   * When the system is in demo mode it will loop over a panel test until the user aborts
   * the tests.  Thisinvolves using auto-generated user input like serial numbers so the test will
   * keep going without user intervention.
   *
   * @author Andy Mechtenberg
   */
  public void enableDemoMode()
  {
    _demoModeEnabled = true;
    _numDemoModeTests = -1; // -1 means to run an infinite number of tests
  }

  /**
   * When the system is in demo mode it will loop over a panel test for the number of
   * tests passed in or until the user aborts the tests.  This
   * involves using auto-generated user input like serial numbers so the test will
   * keep going without user intervention.
   *
   * @author Andy Mechtenberg
   */
  public void enableDemoMode(int numDemoModeTests)
  {
    Assert.expect(numDemoModeTests > 0);

    _demoModeEnabled = true;
    _numDemoModeTests = numDemoModeTests;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void disableDemoMode()
  {
    _demoModeEnabled = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isDemoModeEnabled()
  {
    return _demoModeEnabled;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean shouldRunInfiniteNumberOfDemoModeTests()
  {
    if (_numDemoModeTests == -1)
      return true;

    return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getNumDemoModeTests()
  {
    Assert.expect(_numDemoModeTests != -1);

    return _numDemoModeTests;
  }

}
