package com.axi.v810.gui.testExec;

import java.awt.event.*;
import java.util.*;
import java.io.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.shopFloorSystem.*;

/**
 * This class handles the user flow and calls to the business layer for all of Prodution Test execution
 * @author Andy Mechtenberg
 */
public class ProductionFlow extends TestFlow
{
  private Project _project;
  private PanelHandler _panelHandler;
  private MainMenuGui _mainUI;
  private ScriptRunner _scriptRunner;

  private boolean _exceptionThrown = false;
  private XrayTesterException _xrayTesterException;
  private Config _config;
  private SerialNumberProcessor _serialNumberProcessor;

  private SerialNumberManager _serialNumberManager;

  private BarcodeReaderManager _barCodeReaderManager;
  private boolean _isDemoModeOn = false;
  private boolean _runInfiniteNumberOfDemoModeTests = false;
  private int _demoCount = 0;
  private List<String> _serialNumbers;
  private String _variationName;
  private List<Pair<String, String>> _boardNameToSerialNumberMap;
  private String _firstSerialNumber;
  private PanelSerialNumberData _panelSerialNumberData;

  private boolean _isAutomaticBarCodeInstalled = false;
  private boolean _barCodeReadCancel = false;

  // serial number variables
  private boolean _useSerialNumbersForEachBoard = false;  // 2
  private boolean _preSelectPanel = false;                // 3
  private boolean _useFirstSerialNumberForProjectLookupOnly = false;  //5
  private boolean _useSerialNumberToFindProject = false;
  
  //bypass mode variable
  private boolean _isBypassWithSerialControlMode = false;
  private boolean _isBypassMode = false;

  private ProductionTuneManager _productionTuneManager;
  
  //XCR1144
  //Chnee Khang Wah, 09-Dec-2010, Jabil board drop issue
  private PanelPositioner _panelPositioner;
  private boolean _isPersistentPanelPresent = false;
  private static WorkerThread _interlockPollingThread;
  private static HardwareWorkerThread _hardwareWorkerThread2;
  private Interlock _interlock;
  private HTubeXraySource _hTubeXraySource = null;
  //XCR1144 - end

  private ProjectErrorLogUtil _projectErrorLogUtil; 
  
  //Siew Yeng - XCR1725 - Customizable Serial Number Mapping
  private SerialNumberMappingManager _serialNumberMappingManager;
  private boolean _barcodeReaderScanDuringBoardLoading = false;
  private BooleanLock _waitingForLoadPanel = new BooleanLock(false);
  private DigitalIo _digitalIo;
  private SwingWorkerThread _busyDialogWorkerThread = SwingWorkerThread.getInstance();
  private static transient ProjectObservable _projectObservable;
  //Khaw Chek Hau - XCR2654: CAMX Integration
  private AbstractShopFloorSystem _shopFloorSystem = AbstractShopFloorSystem.getInstance();
  private boolean _isPreSelectPanelSet = false;
  
  private XrayTesterException _scriptException = null;
  
  /**
   * @author Kok Chun, Tan
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }
  
  /**
   * @author Andy Mechtenberg
   */
  ProductionFlow()
  {
    // All variable initialization takes place in execute() because this constructor may be called
    // only once, and these variables need to be initialized for each invokation of the test exec gui

    _config = Config.getInstance();
    _serialNumberManager = SerialNumberManager.getInstance();

    _barCodeReaderManager = BarcodeReaderManager.getInstance();
    _panelHandler = PanelHandler.getInstance();
    _mainUI = MainMenuGui.getInstance();
    _scriptRunner = ScriptRunner.getInstance();
    _productionTuneManager = ProductionTuneManager.getInstance();
	
    //XCR1144
    //Chnee Khang Wah, 09-Dec-2010, Jabil board drop issue
    _panelPositioner = PanelPositioner.getInstance() ;
    _interlock = Interlock.getInstance();
    _digitalIo = DigitalIo.getInstance();
    _interlockPollingThread = new WorkerThread("Interlock Polling Thread");
    _hardwareWorkerThread2 = HardwareWorkerThread.getInstance();

    AbstractXraySource xraySource = AbstractXraySource.getInstance();
    Assert.expect(xraySource instanceof HTubeXraySource);
    _hTubeXraySource = (HTubeXraySource)xraySource;
    //XCR1144 - end
    
    // Bee Hoon, XCR1650 - Open recipe and immediately save recipe, it will crash
    _projectErrorLogUtil = ProjectErrorLogUtil.getInstance();
    
    //Siew Yeng - XCR1725 - Customizable Serial Number Mapping
    _serialNumberMappingManager = SerialNumberMappingManager.getInstance();
  }

  /**
   * Runs the UI through production test mode.  Demo mode is also supported here, but not image collection.
   * @author Andy Mechtenberg
   */
  void execute() throws XrayTesterException
  {
//    DemoMode demoMode = _testExecPanel.getDemoMode();
    _isDemoModeOn = _testExecution.isDemoModeEnabled();
    _runInfiniteNumberOfDemoModeTests = true;//demoMode.shouldRunInfiniteNumberOfDemoModeTests();
//    if (_runInfiniteNumberOfDemoModeTests == false)
//      _demoCount = demoMode.getNumDemoModeTests();
    _isBypassWithSerialControlMode = _testExecution.isBypassWithSerialControlModeEnabled();
    _isBypassMode = _testExecution.isBypassModeEnabled();
    _demoSerialNumber = 1;
    _serialNumbers = new ArrayList<String>();
    _firstSerialNumber = "";
    _panelSerialNumberData = null;
    _abort = false;
    _exceptionThrown = false;
    _xrayTesterException = null;
    _isPersistentPanelPresent = false; //XCR1144, Chnee Khang Wah, 09-Dec-2010, Jabil board drop issue
    _serialNumberProcessor = _testExecPanel.setupInterfaceForProductionMode();
    
    _barcodeReaderScanDuringBoardLoading = false;
    if (_barCodeReaderManager.getBarcodeReaderAutomaticReaderEnabled())
    {
      _isAutomaticBarCodeInstalled = true;
      
      //XCR1713 - Barcode config used in production is not the one selected in options
      //Siew Yeng - load correct config before start
      _barCodeReaderManager.load(_barCodeReaderManager.getBarcodeReaderConfigurationNameToUse());
      
      //Siew Yeng - XCR1726 - Support for multi-drop barcode reader setup
      if(_barCodeReaderManager.getBarcodeReaderScanMethod().equals(BarcodeReaderScanMethodEnum.SCAN_DURING_BOARD_LOADING.toString()))
        _barcodeReaderScanDuringBoardLoading = true; //Scan barcode on the fly
    }
    else
      _isAutomaticBarCodeInstalled = false;
    
    // update all serial number configuration variables
    gatherSerialNumberHandlingOptions();

    // all right!  Start the flow.
    if (_preSelectPanel)
    {
      // load the panel to be used for the entire production flow session
      int value = _testExecPanel.choosePanelProgram(true, false, false);
      if (value == JOptionPane.CANCEL_OPTION)
      {
        _testExecPanel.exitTestExecution();
        return;
      }
      else // the user chose and loaded a program
      {
        _isPreSelectPanelSet = true;
        _project = Project.getCurrentlyLoadedProject();
      }
    }
    startProductionTest();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void gatherSerialNumberHandlingOptions() throws XrayTesterException
  {
    List warnings = new ArrayList();

    _preSelectPanel = false;

    _preSelectPanel = _serialNumberManager.isTestExecPreSelectingProject();
    _useSerialNumbersForEachBoard =  _serialNumberManager.isUseUniqueSerialNumberPerBoardEnabled();
    _useFirstSerialNumberForProjectLookupOnly = _serialNumberManager.isUseFirstSerialNumberForProjectLookupOnlyEnabled();
    _useSerialNumberToFindProject = _serialNumberManager.isUseSerialNumberToFindProjectEnabled();
  }

  /**
   * This method will collect data for starting a panel production test
   * This method must be called on the Swing thread
   * @author Andy Mechtenberg
   * @edited by Kee Chin Seong - Updating Surface Map Data
   */
  private void startProductionTest() throws XrayTesterException
  {
    //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
    if (FalseCallMonitoring.getInstance().isFalseCallMonitoringModeEnabled() && 
          FalseCallMonitoring.getInstance().isEnforceWriteRemark() && 
          FalseCallMonitoring.getInstance().isFalseCallRemarkWrittenNeeded())
    {
      FalseCallTriggeringRemarkDialog remarkDialog = new FalseCallTriggeringRemarkDialog(_mainUI, 
                                                                                         StringLocalizer.keyToString("CFGUI_FCT_REMARK_TITLE_KEY"), 
                                                                                         StringLocalizer.keyToString("CFGUI_FCT_PRODUCTION_DIALOG_WARNING_MESSAGE_KEY"));
      SwingUtils.centerOnComponent(remarkDialog, _mainUI);
      remarkDialog.setVisible(true);
      
      if (remarkDialog.isSaveButtonPerformed() == false)
      {
        _testExecPanel.exitTestExecution();
        return;
      }
    }
    
    _testExecPanel.resetForProductionTest();

    // is there a panel already in the machine?  If so, test that program.  What program is it??  Good question.  We'll
    // ask the panel loader server
    // this should only happen the first time in, as at the end of inspectPanel, the panel is ejected in production mode
    // checks if system has started up with panel loaded.
    boolean panelLoaded = false;
    if(Project.isCurrentProjectLoaded())
    {               
      boolean hasHighMagComponent = _testExecution.containHighMagInspectionInCurrentLoadedProject();
      panelLoaded = _testExecution.isPanelLoaded(hasHighMagComponent, Project.getCurrentlyLoadedProject().getName());
    }
    else
    {
      panelLoaded = _testExecution.isPanelLoaded(false, "");
    }
    
    
  
    
    // sanity check here -- if pre-select, and there's a panel in, it should match the preselected panel, otherwise bad things
    // will happen

    if (panelLoaded && _preSelectPanel)
    {
      // _project should be set (during the pre-select)
      String loadedProjectName = _panelHandler.getLoadedPanelProjectName();
      if (loadedProjectName.equalsIgnoreCase(_project.getName()) == false)
      {
        LocalizedString message = new LocalizedString("TESTEXEC_GUI_PRESELECT_NOT_LOADED_KEY", new Object[]
                                                      {_project.getName(), loadedProjectName});
        MessageDialog.showErrorDialog(_mainUI,
                                      StringUtil.format(StringLocalizer.keyToString(message), 50),
                                      StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                      true,
                                      FontUtil.getMediumFont());
        _testExecPanel.exitTestExecution();
        return;
      }
    }
    else if (panelLoaded && Project.isCurrentProjectLoaded())
    {
        // CR1013 fix by LeeHerng - Add a checking here to make sure panel name matches project name
        String projectName = Project.getCurrentlyLoadedProject().getName();
        _testExecution.checkThatCurrentlyLoadedPanelProjectNameMatchesProjectName(projectName);
    }
    else if(Project.isCurrentProjectLoaded() == false && panelLoaded)
    {
      _testExecution.checkIsThatCurrentlyLoadedPanelProjectRecipesExist(_panelHandler.getLoadedPanelProjectName());
    }

    if (_config.getBooleanValue(SoftwareConfigEnum.OVERRIDE_PANEL_EJECT_FOR_TEST_EXEC))
    {
      //Siew Yeng - XCR1757 - do not eject panel if using runtime manual alignment transform
      if(_testExecution.isAlignmentFailAndRerunAlignment() == false)
      {
        panelLoaded = false;
        //Siew Yeng - XCR1789 
        // - clear abort flag to fix system shows no response when choose to continue production after abort
        _testExecution.clearExitAndAbortFlags();
      }
    }

    if (panelLoaded)
    {
      String projectName = _panelHandler.getLoadedPanelProjectName();
      // debug code
      if (projectName.equals("simulatedPanelName"))
        projectName = "MOTO_CELL_81";

      // what if Pre-select is on, but doesn't match this guy?  Well, currently we ignore the pre-select
      // choice and go on to this.  Dunno what the "right" behavior is.

      // now, ask testExecution if they know the serial numbers for this specific loaded panel
      List<String> previousSerialNumbers = new ArrayList<String> ();
      Map<String, List<String>> serialNumberToVariationNameMap = _testExecution.getSerialNumbersUsedInLastPanelTest();
      String variationName = VariationSettingManager.ALL_LOADED;
      if (serialNumberToVariationNameMap.isEmpty() == false)
      {
        for (String name : serialNumberToVariationNameMap.keySet())
        {
          variationName = name;
          break;
        }
        previousSerialNumbers.addAll(serialNumberToVariationNameMap.get(variationName));
      }
      if (previousSerialNumbers.isEmpty() || previousSerialNumbers.get(0).length() == 0)// there were none
      {
        // XCR-3122 Unable to realign on runtime manual alignment if using blank serial number
        boolean continueProductionTest = false;
        if (_exceptionThrown && 
            _xrayTesterException != null && 
            _xrayTesterException instanceof AlignmentBusinessException && 
            Project.isCurrentProjectLoaded())
        {
          TestProgram testProgram = Project.getCurrentlyLoadedProject().getTestProgram();
          if (testProgram.isRealignPerformed())
          {
            continueProductionTest = true;
          }
        }
        
        if (continueProductionTest == false)
        {
          // ask the user if they want to use blank or eject the panel
          Object[] options = {StringLocalizer.keyToString("TESTEXEC_GUI_UNLOAD_PANEL_KEY"),
                              StringLocalizer.keyToString("TESTEXEC_GUI_USE_BLANK_SERIAL_NUMBERS_KEY"),
                              StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")};

          _mainUI.changeLightStackToStandby();
          int choice = ChoiceInputDialog.showOptionDialog(_frame,
                                                          StringLocalizer.keyToString("TESTEXEC_GUI_NO_SERIAL_NUMBERS_KEY"),
                                                          StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                                          options,
                                                          options[0],
                                                          FontUtil.getMediumFont());
          if ((choice == JOptionPane.CANCEL_OPTION) || (choice == JOptionPane.CLOSED_OPTION))
          {
            _testExecPanel.exitTestExecution();
            return;
          }
          else if (choice == JOptionPane.YES_OPTION) // first option, which is to unload the panel
          {
            _testExecPanel.unloadPanelFromMachine(TestResultsEnum.NOT_TESTED);
            // unload panel and now get new serial numbers
            startProductionTest();
            return;
          }
          else
          {
            // XCR-3122 Unable to realign on runtime manual alignment if using blank serial number
            if (processBlankSerialNumber(projectName) == false)
            {
              _testExecPanel.exitTestExecution();
              return;
            }
          }
        }
        else
        {
          if (processBlankSerialNumber(projectName) == false)
          {
            _testExecPanel.exitTestExecution();
            return;
          }
        }
      }
      else
      {
        // this code block (with better messages) will ask the user if they want to test the panel
        // in the machine, because we have serial numbers
//        int answerValue = ChoiceInputDialog.showConfirmDialog(_frame,
//                                                      StringLocalizer.keyToString("TESTEXEC_GUI_CONTINUE_TESTING_KEY"),
//                                                      StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
//                                                      FontUtil.getMediumFont());
//        if (answerValue != JOptionPane.YES_OPTION)
//        {
//          _testExecPanel.exitTestExecution();
//          return;
//        }
        _variationName = variationName;
        _serialNumbers = previousSerialNumbers;
      }

      //set this flag to false bacause the panel already sleected.
      _isPreSelectPanelSet = false;
      // then move on to acutally inspect the panel
      _panelSerialNumberData = new PanelSerialNumberData(projectName, _serialNumbers, _variationName);
      inspectPanelOnHardwareThread();
    }
    else // panel is NOT loaded
    {
      //XCR-2436, Bugs on production mode run:  Too many times software ask for board serial number
      _panelHandler.checkPanelIsLoading();
      if(_isAutomaticBarCodeInstalled)
      {
        //Siew Yeng - XCR1725 - Cuztomizable Serial Number Mapping
        try
        {
          _serialNumberMappingManager.checkIfSerialNumberMappingAndBarcodeReaderSetupAreTally();
        }
        catch(XrayTesterException xte)
        {
          MessageDialog.showErrorDialog(_frame, 
                                        xte.getLocalizedMessage(), 
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        true);
          _testExecPanel.exitTestExecution();
          return;
        }
      }
      
      if(_barcodeReaderScanDuringBoardLoading)
      {
        _barCodeReaderManager.setScannerIdsNeeded(_serialNumberMappingManager.getBarcodeReadersNeeded());

        _serialNumbers.clear();
        inspectPanelOnHardwareThread();
      }
      else
      {
        if (_preSelectPanel && _testExecPanel.isQueuedSerialNumberAvailable() == false)
        {
          if (_isPreSelectPanelSet)
          {
            _isPreSelectPanelSet = false;
          }
          else
          {
            if (ProjectReader.getInstance().readProjectPreSelectVariation(_project.getName()) == false)
            {
              //pop out choose variation dialog
              List<String> variationList = VariationSettingManager.getInstance().getProjectNameToVariationNamesMap(_project.getName());
              if (variationList.isEmpty() == false)
              {
                int value = _testExecPanel.choosePanelProgram(false, false, true);
                if (value == JOptionPane.CANCEL_OPTION)
                {
                  _testExecPanel.exitTestExecution();
                  return;
                }
              }
            }
          }
        }
        // we need to collect serial numbers and possibly load panel programs, then execute a test
        if (gatherNewSerialNumbers())
          inspectPanelOnHardwareThread();
        else
        {
          // need to tell the difference between cancelling out of entering serial numbers and aborting out
          // of production test
          if (_firstSerialNumber.length() > 0) // we are aborting if never entered first number
            startProductionTest();
        }
      }
    }
  }

  /**
   * XCR-3465, Software crash when run production
   * @author Kee Chin Seong
   */
  private void setupSufaceMapSettingForProject()
  {
    final BusyCancelDialog busyDialog = new BusyCancelDialog(_mainUI,
                                                             StringLocalizer.keyToString("PSP_UPDATE_DATA_KEY"),
                                                             StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                             true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, _mainUI);
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _project.setupSurfaceMapIfNeeded();
        }
        finally
        {
          busyDialog.dispose();
        }
      }
    });
    busyDialog.setVisible(true);
  }
  
  /**
   * This will determine if the user aborted the last test or allowed it
   * to complete.  If abort, then we'll ask the user if they mean to exit
   * production test or not
   * This method must run on the swing thread
   * @author Andy Mechtenberg
   * @authro Kee Chin Seong - Added Loaded Panel Checking in order not always unload the panel
   */
  private boolean continueProductionTest() throws XrayTesterException
  {
    boolean continueTesting;
    
    if (_abort)  // user abort overrides other considerations
    {
      // first, ask the user if they wish to continue in production mode
      _mainUI.changeLightStackToStandby();
      int answerValue = ChoiceInputDialog.showConfirmDialog(_frame,
                                                            StringLocalizer.keyToString("TESTEXEC_GUI_CONTINUE_TESTING_KEY"),
                                                            StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                                            FontUtil.getMediumFont());
      if (answerValue == JOptionPane.YES_OPTION)
      {
        // ok, they want to keep going in production mode.
        // chin-seong,kee - add this key checking to make sure there wont be double unload
        if(_config.getBooleanValue(SoftwareConfigEnum.EJECT_PANEL_IF_ALIGNMENT_FAILS) == true && _testExecution.isPanelLoaded(false, _project.getName()))
            _testExecPanel.unloadPanelFromMachine(TestResultsEnum.NOT_TESTED);
        //Siew Yeng - XCR1757 - unload panel if exception
        else if(_testExecution.isAlignmentFailAndRerunAlignment() && _testExecution.isPanelLoaded(false, _project.getName()))
          _testExecPanel.unloadPanelFromMachine(TestResultsEnum.NOT_TESTED);
        _abort = false;
        continueTesting = true;
      }
      else
        continueTesting = false;
    }
    else if (_isDemoModeOn)  // now check demo mode and the demo mode counter
    {
      if (_runInfiniteNumberOfDemoModeTests)
        continueTesting = true;
      else // this case is for a counted number of demo loops
      {
        _demoCount--;
        if (_demoCount == 0)
          continueTesting = false;
        else
          continueTesting = true;
      }
    }
    else  // default -- no demo mode, no abort.  In production mode we keep going.
      continueTesting = true;

    return continueTesting;
  }

  /**
   * @author Andy Mechtenberg
   * @editedBy Kee Chin Seong
   */
  private void inspectPanelOnHardwareThread() throws XrayTesterException
  {
    _exceptionThrown = false;
    _xrayTesterException = null;
    
    if(_barcodeReaderScanDuringBoardLoading && _serialNumbers.isEmpty()) 
    {
      //Siew Yeng - XCR1726 - Support for multi-drop barcode reader setup
      _project = Project.getCurrentlyLoadedProject();

      if(_project.isSystemTypeSettingConfigCompatibleWithSystem() == false)
      {
        LocalizedString errorMessage = new LocalizedString("MMGUI_TESTEXEC_CANNOT_LOAD_PROJECT_DIFFERENT_SYSTEM_TYPE_KEY", null);
        MessageDialog.showErrorDialog(null,
                                      StringUtil.format(StringLocalizer.keyToString(errorMessage), 50),
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
        _testExecPanel.exitTestExecution();
        return;
      }
      
      MainMenuGui.getInstance().generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("ATGUI_GENERATING_TEST_PROGRAM_KEY"),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        _project,
        FontUtil.getMediumFont());
      
      // Added by Jack Hwee - to handle when all components are set to no test / no load.
      //                    - error will be prompt and no production cannot proceed.
      if(_project.getTestProgram().getAllInspectableTestSubPrograms().isEmpty() == true)
      {    
        JOptionPane.showMessageDialog(_mainUI,
                                      StringLocalizer.keyToString("GUI_UNABLE_CREATE_ALIGNMENT_POINT_DUE_TO_NO_TEST_SUBPROGRAM_KEY"),
                                      StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                      JOptionPane.ERROR_MESSAGE);

        _testExecPanel.exitTestExecution();
        return;
      }
    
      if (UnitTest.unitTesting() == false)
      {
        Date date = new Date();
        System.out.println("Production inspection Date: " + date);
      }

      LocalizedString startMessage = new LocalizedString("TESTEXEC_GUI_INSPECTION_START_KEY", new Object[]{_project.getName()});
      _testExecPanel.addToInformationPane(StringLocalizer.keyToString(startMessage));
    
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _waitingForLoadPanel.setValue(true);
            _barCodeReaderManager.triggerBarcodeReader();
            TestExecution.getInstance().loadPanelIntoMachine(_project.getTestProgram(),false,false);
          }
          catch (PanelInterferesWithAdjustmentAndConfirmationTaskException panelInTheWayException)
          {
            // this should not happen as Grayscale should be run everytime we unload the panel.
            _exceptionThrown = true;
            _xrayTesterException = panelInTheWayException;
          }
          catch (final XrayTesterException ex)
          {
            // any exception is treated like a user abort
            // we display the exception, and ask the user if they wish to continue testingsiao 
            _exceptionThrown = true;
            _xrayTesterException = ex;
          }
          finally
          {
            _waitingForLoadPanel.setValue(false);
          }
        }
      });
      
      try
      {
        _waitingForLoadPanel.waitUntilFalse();
        
        if(_exceptionThrown == false)
        {
          if(gatherNewSerialNumbers() == false)
          {
            //do not run inspection if serial numbers are invalid
            throw new XrayTesterException(new LocalizedString("TESTEXEC_GUI_BARCODE_NOREAD_DETECTED_KEY", new Object[]{_serialNumbers}));
          }
          else
          {
            displayVariationName();
            processSerialNumbers();
            displaySerialNumbers();
          }
        }
      }
      catch (InterruptedException ex)
      {
         //Do nothing ...
      }
      catch(XrayTesterException xte)
      {
        _exceptionThrown = true;
        _xrayTesterException = xte;
      }
    }
    else //Siew Yeng - default / scan barcode before board loading
    {
  //    System.out.println("Serial Numbers: " + _serialNumbers);
      // the worker thread does the work of running the test (or tests if in demo mode)
      if (_testExecPanel.loadSpecificProgram(_panelSerialNumberData.getProjectName()) == false)
      {
        // this means the load of the project failed.  It's already displayed why, so we move
        // on to the next serial number
        // ask the user if they wish to continue testing
        int answerValue = ChoiceInputDialog.showConfirmDialog(_frame,
                                                               StringLocalizer.keyToString("TESTEXEC_GUI_CONTINUE_TESTING_KEY"),
                                                               StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                                               FontUtil.getMediumFont());
        if (answerValue == JOptionPane.YES_OPTION)
        {
          //XCR-3364
          if (ProjectDatabase.isAutoUpdateLocalProjectEnabled())
          {
            if (Project.isCurrentProjectLoaded() == false)
            {
              BooleanRef abortedDuringLoad = new BooleanRef();
              Project.load(_panelSerialNumberData.getProjectName(), abortedDuringLoad);
              _project = Project.getCurrentlyLoadedProject();
              ProjectDatabase.setContinueWithoutDatabase(true);
            }
          }
          // ok, they want to keep going in production mode.
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                startProductionTest();
              }
              catch (XrayTesterException ex)
              {
                _testExecPanel.displayError(ex);
                _testExecPanel.exitTestExecution();
              }
            }
          });
        }
        else
        {
          // no, they don't want to continue
          _testExecPanel.exitTestExecution();
          // Bee Hoon, XCR1650  - Force to close the recipe if the recipe contains error
          if(_projectErrorLogUtil.getIsLandPatternNameNotFoundError())
            _mainUI.getMainMenuBar().projectClose(true);
          
          //XCR-3364
          if (ProjectDatabase.isAutoUpdateLocalProjectEnabled())
          {
            if (Project.isCurrentProjectLoaded() == false)
            {
              BooleanRef abortedDuringLoad = new BooleanRef();
              Project.load(_panelSerialNumberData.getProjectName(), abortedDuringLoad);
              _project = Project.getCurrentlyLoadedProject();
              ProjectDatabase.setContinueWithoutDatabase(false);
            }
          }
        }
        return;
      }
      _project = Project.getCurrentlyLoadedProject();
      
      //set to false, so that the selected variation in production will not override the enable variation in panel setup when save project.
      _project.setIsUpdateComponentNoLoadSetting(false);
      // Kok Chun, Tan - XCR-3290 - Do final checking here, so that we can handle user load from older than 5.8 version recipe.
      // if pre select variation, it is always highest priority.
      String preSelectedVariationName = _serialNumberProcessor.getPreSelectVariationName(_project.getName());
      if (preSelectedVariationName.isEmpty() == false)
        _panelSerialNumberData.setVariationName(preSelectedVariationName);
      //load variation
      //handle auto match serial number and select manually
      String variationName = _panelSerialNumberData.getVariationName();
      
      // XCR-2954 Variation is Loaded Everytime During Production Run - Kee Chin Seong
      // You might have Selected SAME settins before, so there is no require SETUP any more!
      if(_project.getSelectedVariationName().equals(variationName) == false)
        loadVariationSetting(variationName);
      else
          _variationName = variationName;
      
      if(_project.isSystemTypeSettingConfigCompatibleWithSystem() == false)
      {
        LocalizedString errorMessage = new LocalizedString("MMGUI_TESTEXEC_CANNOT_LOAD_PROJECT_DIFFERENT_SYSTEM_TYPE_KEY", null);
        MessageDialog.showErrorDialog(null,
                                      StringUtil.format(StringLocalizer.keyToString(errorMessage), 50),
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
        _testExecPanel.exitTestExecution();
        return;
      }

      processSerialNumbers();
//      _serialNumbers = _panelSerialNumberData.getSerialNumbers();
//
//      // set up the map from board name to board serial number
//      _boardNameToSerialNumberMap = new ArrayList<Pair<String, String>>();
//      List<Board> boards = _project.getPanel().getBoards();
//      int numBoards = _project.getPanel().getNumBoards();
//      for(int i = 0; i < numBoards; i++)
//      {
//        String boardSerialNumber;
//        if (_useSerialNumbersForEachBoard)
//          boardSerialNumber = _serialNumbers.get(i);
//        else
//        {
//          if (i == 0)
//            boardSerialNumber = _serialNumbers.get(0);
//          else
//            boardSerialNumber = "";
//        }
//        _boardNameToSerialNumberMap.add(new Pair<String, String>(boards.get(i).getName(), boardSerialNumber));
//  //      _boardNameToSerialNumberMap.put(boards.get(i).getName(), boardSerialNumber);
//      }
//
//      // the first serial number in the list is the panel serial number
//      // first, show it on screen if we already have a test program
//      _testExecPanel.displaySerialNumber((String)(_serialNumbers.get(0)));
      MainMenuGui.getInstance().generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("ATGUI_GENERATING_TEST_PROGRAM_KEY"),
          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
          _project,
          FontUtil.getMediumFont());
      _exceptionThrown = false;
      _xrayTesterException = null;

      // Added by Jack Hwee - to handle when all components are set to no test / no load.
      //                    - error will be prompt and no production cannot proceed.
      if(_project.getTestProgram().getAllInspectableTestSubPrograms().isEmpty() == true)
      {    
        JOptionPane.showMessageDialog(_mainUI,
                                      StringLocalizer.keyToString("GUI_UNABLE_CREATE_ALIGNMENT_POINT_DUE_TO_NO_TEST_SUBPROGRAM_KEY"),
                                      StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                      JOptionPane.ERROR_MESSAGE);

        _testExecPanel.exitTestExecution();
        return;
      }

      if (UnitTest.unitTesting() == false)
      {
        Date date = new Date();
        System.out.println("Production inspection Date: " + date);
      }
      //XCR-3465, Software crash when run production
      setupSufaceMapSettingForProject();
      //auto save if project has been modified. 
      //After changing varaition, test program need to generate, so the project need to save it.
      if (_project.hasBeenModified())
      {
        _project.fastSave();
      }

      LocalizedString startMessage = new LocalizedString("TESTEXEC_GUI_INSPECTION_START_KEY", new Object[]{_project.getName()});
      _testExecPanel.addToInformationPane(StringLocalizer.keyToString(startMessage));

      displayVariationName();
      displaySerialNumbers();
//      for (String serialNum : _serialNumbers)
//      {
//        LocalizedString serialNumberMessage = new LocalizedString("TESTEXEC_GUI_INSPECTION_START_SERIAL_NUMBER_KEY", new Object[]{serialNum});
//        _testExecPanel.addToInformationPane(StringLocalizer.keyToString(serialNumberMessage));
//      }
//      _testExecPanel.addToInformationPane("");
    }
    int toleranceInNanometers = _panelHandler.getToleranceInNanometersForRailWidth();
    
    if (MathUtil.fuzzyEquals(_project.getPanel().getWidthAfterAllRotationsInNanoMeters()+PanelHandler.getPanelWidthClearanceInNanoMeters(), _panelHandler.getRailWidthInNanoMeters(),toleranceInNanometers))
    {
    
    }
    else
    {
      _panelHandler.setKeepOuterBarrierOpenForLoadingPanel(false);
    }
    
    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        _testExecPanel.setTestInProgress(true);
        if(_exceptionThrown == false)
        {
          try
          {
            //XCR-3001, Kok Chun, Tan - 16/10/2015
            // initializa board to reset all variable to make sure xout flag is correct.
            for (Board board : _project.getPanel().getBoards())
            {
              board.resetXout();
            }

            if ((_isBypassWithSerialControlMode && _isBypassMode) || _project.isProjectBasedBypassMode())
            {
               final int panelWidth = _project.getPanel().getWidthAfterAllRotationsInNanoMeters();
               final int panelLength = _project.getPanel().getLengthAfterAllRotationsInNanoMeters(); 
              _testExecution.bypassPanel(panelWidth, panelLength, true, _boardNameToSerialNumberMap);
            }
            else
            {
              // Make sure the panel inspection settings are in the correct state before we start.
              PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
              panelInspectionSettings.clearAllInspectionSettings();
              panelInspectionSettings.setInspectedSubtypes(_project.getPanel());
              _inspectionEngine.setPanelInspectionSettings(panelInspectionSettings);

              TestExecution.getInstance().testPanelForProduction(_project.getTestProgram(), _serialNumbers.get(0), _boardNameToSerialNumberMap);
            }
          }
          catch (PanelInterferesWithAdjustmentAndConfirmationTaskException panelInTheWayException)
          {
            // this should not happen as Grayscale should be run everytime we unload the panel.
            _exceptionThrown = true;
            _xrayTesterException = panelInTheWayException;
          }
          catch (final XrayTesterException ex)
          {
            // any exception is treated like a user abort
            // we display the exception, and ask the user if they wish to continue testing
            _exceptionThrown = true;
            _xrayTesterException = ex;
          }
        }

        // wait here if there's no serial numbers in the queue, but the processor is in the middle
        // of processing some
        if (_testExecPanel.isQueuedSerialNumberAvailable() == false)
        {
          while (_serialNumberProcessor.isProcessingSerialNumbers())
          {
            try
            {
              synchronized (this)
              {
                wait(200);
              }
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
        }
        // when one panel test is complete, put this call back on the swing thread
        // in order to loop back and do the next one
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              // if we threw an exception, do not continue.
              if (_exceptionThrown)
              {
                // Kok Chun, Tan - XCR3051 - reset the flag if have exception
                TestProgram testProgram = _project.getTestProgram();
                testProgram.resetIsTestSubProgramDone();
                
                LocalizedString text = _xrayTesterException.getLocalizedString();

                // handle the three alignment exceptions special from other exceptions
                if (_xrayTesterException instanceof AlignmentBusinessException)
                {
                  AlignmentBusinessException alignmentException = (AlignmentBusinessException)_xrayTesterException;
                  if (_testExecution.shouldPanelBeEjectedIfAlignmentFails() == false &&
                      _testExecution.isAlignmentFailAndUnloadPanel() == false &&
                    _testExecution.isAlignmentFailAndRerunAlignment() == false)
                  {
                    AlignmentFailedDialog alignmentFailedDialog = new AlignmentFailedDialog(_frame,
                        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                        _project,
                        _xrayTesterException,
                        FontUtil.getMediumFont());
                    SwingUtils.centerOnComponent(alignmentFailedDialog, _frame);
                    // turn on the red light
                    _testExecPanel.changeLightStackToError();
                    alignmentFailedDialog.setVisible(true);
                      //Chin Seong, Kee - New UI will be shown before throw to here in order to "pause" the process but not pausing hardware layer
                    
                    //Siew Yeng - XCR1757 - runtime manual alignment
                    if(alignmentFailedDialog.isReAlignAction())
                    {
                      _testExecution.setAlignmentFailedAndRerunAlignment(true);
                      
                      boolean done = performRuntimeManualAlignment(alignmentException.getTestSubProgram());
                      if(done)
                      {
                        //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
                        _project.getTestProgram().setRerunningProduction(true);
                        //Khaw Chek Hau - XCR2546: Production Hang (Fail to control UI) after cancel production with misalignment 
                        _testExecPanel.setTestInProgress(false);
                        return;
                      }
                    }
                  }
                  else if(_testExecution.isAlignmentFailAndRerunAlignment())
                  {
                    //Siew Yeng - XCR1757 - runtime manual alignment
                    boolean done = performRuntimeManualAlignment(alignmentException.getTestSubProgram());
                    if(done)
                    {
                      //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
                      _project.getTestProgram().setRerunningProduction(true);
                      //Khaw Chek Hau - XCR2546: Production Hang (Fail to control UI) after cancel production with misalignment 
                      _testExecPanel.setTestInProgress(false);
                   
                      //Khaw Chek Hau - XCR2662: Production inspection time only display for misalignment board
                      TestExecution.getInstance().startFullInspectionTimer();

                      return;
                    }
                  }
                  else
                  {
                    // we ejected the panel on alignment failure, so I'll want to display the error in the text area
                    // chin-seong, kee - we show the message once enough, show the message right after we show the exception UI .
                    //_testExecPanel.addToInformationPane(_xrayTesterException.getMessage());
                   //chin-seong, kee - unload the panel from here, before the dialog show up for continue testing 
                    _testExecPanel.unloadPanelFromMachine(TestResultsEnum.NOT_TESTED);
                    //chin-seong, kee - exception no need to reset. this will cause whole v810i keep repeating the alignment process.
                                        //when running production, v810i will remember the state which state has been stopped,
                                        //because we fail at alignment, so when we continue, there will be loop forever on alignment there
                                         // after the test is run, we need to know if the user aborted or not.
                                        // This can get complicated due to alignment recovery options, etc.  The TestExecution
                                        // class keeps track better than the UI layer, so we'll ask it for what happened.
                    //_exceptionThrown = false; // we already handled it, so reset this.
                    _testExecution.setAlignmentFailAndUnloadPanel(false);
                  }
                }
                //XCR1144, XCR3565
                // Kok Chun, Tan - check downstream sensor signal
                //Chnee Khang Wah, 09-Dec-2010, Jabil board drop issue
                else if ((_xrayTesterException instanceof HardwareTimeoutException && 
                         text.equals(HardwareTimeoutExceptionEnum.getLocalizedString(HardwareTimeoutExceptionEnum.WAIT_FOR_DOWNSTREAM_SENSOR_SIGNAL_CLEAR)) == false) ||
                         text.equals(PanelPositionerExceptionEnum.getLocalizedString(PanelPositionerExceptionEnum.PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_DURING_LOAD)))
                {
                  Interlock.getInstance().setServicePanelActivation(true);
                  _testExecPanel.displayError(_xrayTesterException);
                  //Once detect Exception, 
                  //1. shut down the x-ray source.
                  _hardwareWorkerThread2.invokeLater(new Runnable()
                  {
                    public void run()
                    {
                      try
                      {
                        _hTubeXraySource.powerOff();
                      }
                      catch (XrayTesterException ex)
                      {
                        //do nothing
                      }

                      //Release front panel magnet
                      try
                      {
                        _digitalIo.turnFrontPanelLeftDoorLockOff();
                        _digitalIo.turnFrontPanelRightDoorLockOff();
                      }
                      catch (XrayTesterException xte)
                      {
                        //do nothing
                      }
                    }
                  });

                  //2. Assume board is already drop into machine.
                  if(_xrayTesterException instanceof HardwareTimeoutException)
                  {
                     HardwareTimeoutException ex = (HardwareTimeoutException) _xrayTesterException;
                     System.out.println(ex.equals(HardwareTimeoutExceptionEnum.WAIT_FOR_DOWNSTREAM_SYSTEM_TO_ACCEPT_PANEL));
                     if(ex.getExceptionEnum().equals(HardwareTimeoutExceptionEnum.WAIT_FOR_DOWNSTREAM_SYSTEM_TO_ACCEPT_PANEL) == true)
                        _isPersistentPanelPresent = false;
                     else
                        _isPersistentPanelPresent = true;
                  }
                  else
                    _isPersistentPanelPresent = true;  

                  //3. Make sure user open top hatch (broke the interlock).
                  _interlockPollingThread.invokeLater(new Runnable()
                  {

                    public void run()
                    {
                      while (_isPersistentPanelPresent == true)
                      {
                        try
                        {
                          if (_interlock.isInterlockOpen() == true)
                          {
                            _isPersistentPanelPresent = false;
                          }
                        }
                        catch (XrayTesterException ex)
                        {
                          //do nothing
                        }
                      }
                    }
                  });

                  //4.  The following message will only clear after the interlock have been broke
                  while(_isPersistentPanelPresent==true)
                  {
                      if(text.equals(PanelPositionerExceptionEnum.getLocalizedString(PanelPositionerExceptionEnum.PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_DURING_LOAD)))
                      {
                          String message = StringLocalizer.keyToString("HW_PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_EXCEPTION_AND_WAIT_FOR_REMOVE_KEY");
                          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                        message,
                                        StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                        JOptionPane.ERROR_MESSAGE);
                      }
                      else
                      {
                         String message = StringLocalizer.keyToString("HW_WAIT_FOR_PANEL_TO_BE_IN_PLACE_TO_DETECT_A_PANEL_TIMEOUT_KEY");
                         JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                        message,
                                        StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                        JOptionPane.ERROR_MESSAGE);
                      }
                  }
                }
                else if(text.equals(PanelPositionerExceptionEnum.getLocalizedString(PanelPositionerExceptionEnum.POSSIBLE_PANEL_INSIDE_MACHINE)) ||
                        text.equals(PanelPositionerExceptionEnum.getLocalizedString(PanelPositionerExceptionEnum.POSSIBLE_PANEL_LOAD_UNSUCCESFUL)))
                {
                  _testExecPanel.displayError(_xrayTesterException);
                }
                //XCR1144 - end
                else
                {
                  _testExecPanel.displayError(_xrayTesterException);
                  
                  //Siew Yeng - XCR1726
                  if(_barcodeReaderScanDuringBoardLoading)
                  {
                    _testExecPanel.unloadPanelFromMachine(TestResultsEnum.NOT_TESTED);
                  }
                }
              }
              //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
              //If there is no exception throwed, reset all setInspectionDone boolean to false
              else
              {
                TestProgram testProgram = _project.getTestProgram();
                for (TestSubProgram subProgram : testProgram.getAllTestSubPrograms())
                {
                  subProgram.setInspectionDone(false);
                }
              }

              _testExecPanel.setTestInProgress(false);
              Interlock.getInstance().setServicePanelActivation(false);
              // If false call triggering is enabled and it is triggered, just exitTestExecution
              // XCR-3328 Defective pins has exceeded the maximum number of error allowed is prompted even the production is aborted
              if (_testExecution.didUserAbort() == false && 
                  _exceptionThrown == false &&
                  _config.getBooleanValue(SoftwareConfigEnum.FALSE_CALL_MONITORING_ENABLED) == true && 
                  FalseCallMonitoring.getInstance().isFalseCallTriggeringTriggered() == true)
              {   
                _testExecPanel.handleFalseCallTriggering(_inspectionEngine.getNumberOfProcessedJoints(),
                                                         _inspectionEngine.getNumberOfDefectiveJoints(), 
                                                         _project.getName(),
                                                         _inspectionEngine.getInspectionStartTimeInMillis());
                _abort = true;
                _testExecPanel.exitTestExecution();
              }
              else
              {
                  // after the test is run, we need to know if the user aborted or not.
                  // This can get complicated due to alignment recovery options, etc.  The TestExecution
                  // class keeps track better than the UI layer, so we'll ask it for what happened.
                  _abort = _testExecution.didUserAbort() || _exceptionThrown;
                  if (continueProductionTest())
                    startProductionTest();
                  else
                  {
                    // this look weird, but if the use chose not to continue testing, we'll set the
                    // abort flag to allow us to exit properly
                    _abort = true;
                    _testExecPanel.exitTestExecution();
                  }
              }
            }
            catch (XrayTesterException ex)
            {
              _testExecPanel.displayError(ex);
              _testExecPanel.exitTestExecution();
            }
          }
        });
      }
    });
    
    //Khaw Chek Hau - XCR3609: Production Future Serial Number Cursor Not Lock
    _testExecPanel.futureSerialNumberPanelRequestFocus();
  }

  /**
   * This method collects all serial numbers
   * It will collect them in the following order
   * PreScan In Barcode Check for InlineMode 
   * 1.  From the Automatic Barcode Reader
   * 2.  From the Queue in Test Exec Panel, if they exist
   * 3.  From the user directly, via a dialog box
   * @return false if the user doesn't enter all serial number properly
   * @return true if all data is collect such that an inspection can begin
   * @author Andy Mechtenberg
   * @edited Kee Chin Seong
   */
  private boolean gatherNewSerialNumbers() throws XrayTesterException
  {
    _serialNumbers.clear();
    _firstSerialNumber = "";

    //Check for any buffer data barcode left during production.
    //if serial number empty with prescan running Wait until it done.
    if(_testExecution.isPreScanBarCodeScannerRunning() == true && 
       _testExecution.getPreScanSerialNumbers().isEmpty() == true &&
       _isAutomaticBarCodeInstalled)
    { 
       waitForPreScanBarcodeTaskFinished();
       if(_testExecution.didUserAbort())
         return false;
    }
    
    //when this stage, means the pre scan has been activated finished, 
    //but we never know whether the list is filled with barcode or not,
    //so double check on the list will needed for last check.
    if(_testExecution.getPreScanSerialNumbers().isEmpty() == false &&
       _isAutomaticBarCodeInstalled)
    {
      _serialNumbers = _testExecution.getPreScanSerialNumbers();
      _firstSerialNumber = (String)_serialNumbers.get(0);
      _panelSerialNumberData = _serialNumberProcessor.processAutomaticBarcodeSerialNumbers(_serialNumbers);

      _testExecution.getPreScanSerialNumbers().clear();

      return _panelSerialNumberData.isValid();
    }
    else
    {
      if (_isAutomaticBarCodeInstalled)
      {
        if (gatherAutomaticBarcodeSerialNumbers() == false)
          return false;
        else // validate the serial numbers
        {
          // set this to trigger the rest of the class that we got a serial number -- allows correct flow
          if (_serialNumbers.isEmpty())
          {
            _testExecPanel.exitTestExecution();
            return false;
          }
          _firstSerialNumber = (String)_serialNumbers.get(0);
          _panelSerialNumberData = _serialNumberProcessor.processAutomaticBarcodeSerialNumbers(_serialNumbers);
          // run the pre-inspection script here

          return _panelSerialNumberData.isValid();
        }
      }
      else if (_testExecPanel.isQueuedSerialNumberAvailable())
      {
        // get any queued up one or from user or from the automatic bar code reader
        _panelSerialNumberData = _testExecPanel.getQueuedSerialNumberData();

        //Khaw Chek Hau - XCR2501 : preInspection Script is not executed for future serial number production run and change the flow
        _firstSerialNumber = _panelSerialNumberData.getSerialNumbers().get(0);
     
        if (_serialNumberProcessor.isSerialNumberValid(_firstSerialNumber) == false)
          _panelSerialNumberData = new PanelSerialNumberData();

        //Khaw Chek Hau - XCR2654: CAMX Integration
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
        {
          try
          {
            _shopFloorSystem.getSerialNumber(_panelSerialNumberData.getSerialNumbers().get(0));
          }
          catch (DatastoreException ex)
          {
            String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("SHOP_FLOOR_ERROR_MESSAGE_KEY_KEY");
            MessageDialog.showErrorDialog(_frame,
              errorMsg,
              StringLocalizer.keyToString("SHOP_FLOOR_EVENT_TITLE_KEY"),
              true);
          }
        }

        if (_panelSerialNumberData.isValid() == false)
        {
          return false;
        }

        return true;
      }
      else if (gatherFirstSerialNumber() == false)  // this handles both demo mode and keyboard input
      {
        return false;
      }
    }
    _panelSerialNumberData = _serialNumberProcessor.processKeyboardSerialNumber(_firstSerialNumber, true);
    if (_panelSerialNumberData.isValid() == false && _isDemoModeOn)
    {
      // first, ask the user if they wish to continue in production mode
      _mainUI.changeLightStackToStandby();
      int answerValue = ChoiceInputDialog.showConfirmDialog(_testExecPanel,
        StringLocalizer.keyToString("TESTEXEC_GUI_CONTINUE_TESTING_KEY"),
        StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
        FontUtil.getMediumFont());
      if (answerValue != JOptionPane.YES_OPTION)
      {
        _firstSerialNumber = ""; // allow us to break out of a infinite serial number loop
        _testExecPanel.exitTestExecution();
      }
      return false; // we cannot inspect, so return false
    }
    
    return _panelSerialNumberData.isValid();
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void waitForPreScanBarcodeTaskFinished()
  {
    final BusyCancelDialog busyCancelDialog = creatingBusyCancelDialog();
    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        _testExecution.waitPreScanFinished();
        busyCancelDialog.dispose();
      }
    });
    busyCancelDialog.setVisible(true);
    if (_barCodeReadCancel)
    {
      _testExecPanel.exitTestExecution();
    }
  }

  /**
   * @return true if automatic barcode were read successfully (user did not cancel)
   * Sets the _serialNumbers list variables with all barcode reads
   * @author Andy Mechtenberg
   */
  private boolean gatherAutomaticBarcodeSerialNumbers()
  {
    //Siew Yeng - XCR1726 - Support for multi-drop barcode reader setup
    if(_barcodeReaderScanDuringBoardLoading)
    {
      try
      {
        _serialNumbers = _testExecution.getSerialNumbers();
      }
      catch (final XrayTesterException ex)
      {
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            _testExecPanel.displayError(ex);
          }
        });
      }
      return true;
    }
    
    // get the serial numbers from the automatic barcode reader
    _barCodeReadCancel = false;
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_frame,
      StringLocalizer.keyToString("TESTEXC_GUI_WAITING_FOR_BARCODE_KEY"),
      StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
      StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
      true);
    // set up the cancel dialog
    busyCancelDialog.addCancelActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        // If the user clicks cancel, then cancel the
        // read and hide the busy/cancel dialog
        if (_testExecPanel.confirmAction(StringLocalizer.keyToString("TESTEXEC_GUI_CONFIRM_ABORT_KEY"), true))
        {
          _barCodeReadCancel = true;
          abort();
          busyCancelDialog.dispose();
        }
        else
          busyCancelDialog.enableCancelButton();
      }
    });
    SwingUtils.centerOnComponent(busyCancelDialog, _frame);

    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _serialNumbers = _testExecution.getSerialNumbers();
          busyCancelDialog.dispose();
        }
        catch (final XrayTesterException ex)
        {
          // let's treat an exception as a cancel
          _barCodeReadCancel = true;
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              busyCancelDialog.dispose();
              _testExecPanel.displayError(ex);
            }
          });
        }
      }
    });
    busyCancelDialog.setVisible(true);
    if (_barCodeReadCancel)
    {
      _testExecPanel.exitTestExecution();
      return false;
    }
    else  // user didn't cancel, so everything must be OK
      return true;
  }
  
  /*
   * @author Kee Chin Seong
   */
  private BusyCancelDialog creatingBusyCancelDialog()
  {
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_frame,
                                              StringLocalizer.keyToString("TESTEXC_GUI_WAITING_FOR_BARCODE_KEY"),
                                              StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                              StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                              true);
    // set up the cancel dialog
    busyCancelDialog.addCancelActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        // If the user clicks cancel, then cancel the
        // read and hide the busy/cancel dialog
        if (_testExecPanel.confirmAction(StringLocalizer.keyToString("TESTEXEC_GUI_CONFIRM_ABORT_KEY"), true))
        {
          _barCodeReadCancel = true;
          abort();
          busyCancelDialog.dispose();
        }
        else
          busyCancelDialog.enableCancelButton();
      }
    });
    SwingUtils.centerOnComponent(busyCancelDialog, _frame);
    
    return busyCancelDialog;
  }

  /**
   * This will collect a panel serial number using this heirarchy:
   * 1. Auto generate if in demo mode
   * 2. From the keyboard, via a modal dialog
   * Sets the _firstSerialNumber variable
   * @return false if the user aborts this operation and production test should end
   * @author Andy Mechtenberg
   * @author Lim Boon Hong. Add to display image of board serial number
   * @edited by kee chin seong - Still reading barcode during production although already disable automatic disable barcode
   */
  private boolean gatherFirstSerialNumber() throws XrayTesterException
  {
    String prompt;
    if (_useFirstSerialNumberForProjectLookupOnly)
      prompt = StringLocalizer.keyToString("TESTEXEC_GUI_CAD_MATCH_NUMBER_PROMPT_KEY");
    else
      prompt = StringLocalizer.keyToString("TESTEXEC_GUI_SERIAL_NUMBER_PROMPT_KEY");

    if (_isDemoModeOn)
    {
      _firstSerialNumber = generateDemoSerialNumber();

      return true;
    }
    if (_isAutomaticBarCodeInstalled)
    {
      Assert.expect(false, "Automatic Barcode reads should not use this method");
    }
    else
    {
      //To display image of board serial number by Lim Boon Hong
      if(_project != null)
        _testExecPanel.displayPanelImage(_project.getName(), _project.getPanel().getBoards().get(0).getName());
    
      int value = _testExecPanel.promptUserForSerialNumber(prompt, false);
      if (value == JOptionPane.CANCEL_OPTION)
      {
        if (_testExecPanel.confirmAction(StringLocalizer.keyToString("TESTEXEC_GUI_CONFIRM_ABORT_KEY"), true))
        {
          _testExecPanel.exitTestExecution();
          return false;
        }
        else
        {
          // the user chose NOT to abort, but get another serial number
          return gatherFirstSerialNumber();
        }
      }
      _firstSerialNumber = _testExecPanel.getUserEnteredSerialNumber(); // this get's the data from the dialog box
            
      //Khaw Chek Hau - XCR2654: CAMX Integration
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
      {
        try
        {
          _shopFloorSystem.getSerialNumber(_firstSerialNumber);
        }
        catch (DatastoreException ex)
        {
          String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("SHOP_FLOOR_ERROR_MESSAGE_KEY_KEY");
          MessageDialog.showErrorDialog(_frame,
                  errorMsg,
                  StringLocalizer.keyToString("SHOP_FLOOR_EVENT_TITLE_KEY"),
                  true);
        }
      }
      
      if (_firstSerialNumber.equals(TestExecPanel._TRY_AGAIN))
      {
        // blank (empty string) serial numbers are not allowed in production mode
        // we don't warn them here -- all they see is the dialog up again
        return gatherFirstSerialNumber();
      }
    }
    
    return true;
  }

  /**
   * @author ??
   */
  public void clearProjectInfo()
  {
    _project = null;
  }

  /*
   * Taken out from inspectPanelOnHardwareThread() for reuse purpose
   * @author Phang Siew Yeng
   */
  private void processSerialNumbers() throws XrayTesterException
  {
    _serialNumbers = _panelSerialNumberData.getSerialNumbers();

    // set up the map from board name to board serial number
    _boardNameToSerialNumberMap = new ArrayList<Pair<String, String>>();
    List<Board> boards = _project.getPanel().getBoards();
    int numBoards = _project.getPanel().getNumBoards();
    for(int i = 0; i < numBoards; i++)
    {
      String boardSerialNumber;
      if (_useSerialNumbersForEachBoard)
        boardSerialNumber = _serialNumbers.get(i);
      else
      {
        if (i == 0)
          boardSerialNumber = _serialNumbers.get(0);
        else
          boardSerialNumber = "";
      }
      _boardNameToSerialNumberMap.add(new Pair<String, String>(boards.get(i).getName(), boardSerialNumber));
    }
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_frame,
                                                                  StringLocalizer.keyToString("TEXTEXEC_GUI_EXECUTING_PRE_SCRIPT_MESSAGE_KEY"),
                                                                  StringLocalizer.keyToString("TEXTEXEC_GUI_EXECUTING_PRE_SCRIPT_KEY"),
                                                                  true);
    SwingUtils.centerOnComponent(busyCancelDialog, _frame);
    
    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _scriptRunner.runPreInspectionScript(_serialNumbers.get(0), _serialNumbers, Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PRE_BOARD_SERIAL_NUMBER_INSPECTION_SCRIPT));
          busyCancelDialog.dispose();
        }
        catch (XrayTesterException e)
        {
          _scriptException = e;
        }
      }
    });
    busyCancelDialog.setVisible(true);
    if (_scriptException != null)
    {
      XrayTesterException ex = _scriptException;
      _scriptException = null;
      throw ex;
    }
    
    // the first serial number in the list is the panel serial number
    // first, show it on screen if we already have a test program
    _testExecPanel.displaySerialNumber((String)(_serialNumbers.get(0)));
  }
  
  /*
   * Taken out from inspectPanelOnHardwareThread() for reuse purpose
   * @author Phang Siew Yeng
   */
  private void displaySerialNumbers()
  {
    for (String serialNum : _serialNumbers)
    {
      LocalizedString serialNumberMessage = new LocalizedString("TESTEXEC_GUI_INSPECTION_START_SERIAL_NUMBER_KEY", new Object[]{serialNum});
      _testExecPanel.addToInformationPane(StringLocalizer.keyToString(serialNumberMessage));
    }
    _testExecPanel.addToInformationPane("");
  }
  
  /*
   * Perform runtime manual alignment transform
   * @return true if runtime manual alignment completed
   * @author Phang Siew Yeng
   */
  public boolean performRuntimeManualAlignment(TestSubProgram testSubProgram) throws XrayTesterException
  {
    TestExecManualAlignmentDialog testExecManualAlignmentDialog = new TestExecManualAlignmentDialog(
                        _frame,
                        StringLocalizer.keyToString("TESTEXEC_GUI_RUNTIME_MANUAL_ALIGNMENT_KEY"),
                        true,
                      testSubProgram);
                      SwingUtils.centerOnComponent(testExecManualAlignmentDialog, _frame);
                      testExecManualAlignmentDialog.setVisible(true);
                      
    if(testExecManualAlignmentDialog.isPerformedRuntimeManualAlignment())
    {
      if (continueProductionTest())
      {
        //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
        testSubProgram.getTestProgram().setRealignPerformed(true);
        testSubProgram.setInspectionDone(false);
        startProductionTest();
      }
       return true;
    }
    else if(testExecManualAlignmentDialog.isExceptionOccured())
      return false;
    
    _testExecPanel.unloadPanelFromMachine(TestResultsEnum.NOT_TESTED);
    _testExecution.setAlignmentFailedAndRerunAlignment(false);
    
    return false;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  protected void loadVariationSetting(final String variationName)
  {
    Assert.expect(variationName != null);
    
    // if no project loaded in production, no need to do anything.
    if (_project == null)
      return;

    final BusyCancelDialog busyDialog = new BusyCancelDialog(_mainUI,
      StringLocalizer.keyToString("MMGUI_SETUP_VARIATION_SETTING_KEY"),
      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
      true);

    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, _mainUI);

    _busyDialogWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        Map<String, java.util.List<String>> boardTypeToRefDesMap = new LinkedHashMap<>();
        List<VariationSetting> noLoadSettingVariationList = _project.getPanel().getPanelSettings().getAllVariationSettings();
        boolean isSetupVariationSetting = false;

        _variationName = variationName;
        _project.setSelectedVariationName(_variationName);

        if (_project.enablePreSelectVariation() == false || _useSerialNumberToFindProject || _useFirstSerialNumberForProjectLookupOnly)
        {
          //if no variation name, then need to set all to load and inspect
          if (_variationName.equals(VariationSettingManager.ALL_LOADED))
          {
            isSetupVariationSetting = false;
          }
          else
          {
            //get selected variation
            for (VariationSetting variation : noLoadSettingVariationList)
            {
              if (_variationName.equals(variation.getName()))
              {
                isSetupVariationSetting = true;
                boardTypeToRefDesMap = variation.getBoardTypeToRefDesMap();
                break;
              }
            }
          }
          VariationSettingManager.getInstance().setupVariationSetting(isSetupVariationSetting, _project, boardTypeToRefDesMap);
        }
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            busyDialog.setVisible(false);
          }
        });
      }
    });
    busyDialog.setVisible(true);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void displayVariationName()
  {
    LocalizedString startMessage = new LocalizedString("TESTEXEC_GUI_VARIATION_NAME_KEY", new Object[]{_variationName});
    _testExecPanel.addToInformationPane(StringLocalizer.keyToString(startMessage));
  }
  
  /**
   * @author Kok Chun, Tan
   */
  protected String getVariationName()
  {
    return _variationName; 
  }
  
  /**
   * XCR-3122 Unable to realign on runtime manual alignment if using blank serial number
   * @author Cheah Lee Herng
   */
  private boolean processBlankSerialNumber(String projectName) throws XrayTesterException
  {
    Assert.expect(projectName != null);
    
    boolean isContinue = true;
    boolean preSelectVariation = ProjectReader.getInstance().readProjectPreSelectVariation(projectName);
    // use blank serial numbers
    // use a blank serial number, because either we're using blanks anyway or the held number is already good.
    _scriptRunner.runPreInspectionScript("", new ArrayList<String>(), false);
    _serialNumbers.add(_scriptRunner.getPanelSerialNumber());

    int numBoards = _testExecPanel.getNumberOfBoards(projectName);
    if (_useSerialNumbersForEachBoard && numBoards > 1)
    {
      for (int i = 1; i < numBoards; i++)
        _serialNumbers.add("");
    }

    // XCR - 3262 - Kok Chun, Tan
    // No need to match any serial number matching, because we already know the loaded board project. 
    // It is no point to match others project using serial number.
    if (_preSelectPanel)
    {
      _variationName = _testExecPanel.getCurrentVariation();
    }
    else if (preSelectVariation)
    {
      _variationName = _serialNumberProcessor.getPreSelectVariationName(projectName);
    }
    else
    {
      //pop out choose variation dialog when user click use blank serial number
      List<String> variationList = VariationSettingManager.getInstance().getProjectNameToVariationNamesMap(projectName);
      if (variationList.isEmpty() == false)
      {
        int value = _testExecPanel.choosePanelProgram(true, false, false, projectName);
        if (value == JOptionPane.CANCEL_OPTION)
        {
          isContinue = false;
        }
        else
        {
          _variationName = _testExecPanel.getCurrentVariation();
        }
      }
      //XCR-3337, Assert when using Blank serial number
      else
      {
        _variationName = VariationSettingManager.ALL_LOADED;
      }
    }
    return isContinue;
  }
}
