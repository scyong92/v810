package com.axi.v810.gui.testExec;

import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class handles serial number processing for Production Flow
 * There should be two main entry points, one for non-automatic barcode
 * and one for automatic barcodes
 * The Keyboard Entry (non-automatic barcode) accepts 1 serial number
 * and may prompt users for additional ones
 * The automatic barcode one will accepts a list of serial numbers
 * @author Andy Mechtenberg
 */
public class SerialNumberProcessor
{
  private TestExecPanel _testExecPanel;
  private SerialNumberToProjectName _serialNumberToProjectName;
  private SerialNumberManager _serialNumberManager;
  private BarcodeReaderManager _barCodeReaderManager;
  private static TestExecution _testExecution;
  private MainMenuGui _mainUI;

  private volatile boolean _processingSerialNumbers = false;
  private List<String> _validatedSerialNumbers;
  private String _projectName;
  private String _variationName;
  private int _numberOfBoards;
  private List<String> _boardNames;
  private String _previousSerialNumber;  // used for detecting consecutive duplicates

  private PanelSerialNumberData _invalidSerialNumber = new PanelSerialNumberData();
  private boolean _isAutomaticBarCodeInstalled = false;
  private boolean _isProcessingFirstSerialNumber = false;

  // serial number variables
  private boolean _isDemoModeOn = false;
  private boolean _useSerialNumberToFindProject = false;  // 1
  private boolean _useSerialNumbersForEachBoard = false;  // 2
  private boolean _preSelectPanel = false;                // 3
  private boolean _checkForDuplicateSerialNumbers = false; // 4
  private boolean _useFirstSerialNumberForProjectLookupOnly = false;  //5
  private boolean _validateBoardSerialNumbers = false;  // 6
  private boolean _onlyAllowNewSerialNumberWhenNoProjectMatch = false; // 7
  private boolean _checkAutomaticBarcodesForMisreads = false; // 8
  private String _noBarCodeReadMessage;
  
  private boolean _isFutureSerialNumberInput = false;

  /**
   * @author Andy Mechtenberg
   */
  SerialNumberProcessor(TestExecPanel testExecPanel) throws XrayTesterException
  {
    Assert.expect(testExecPanel != null);
    _testExecPanel = testExecPanel;  // needed for child dialog centering (this is the parent)

    _serialNumberManager = SerialNumberManager.getInstance();
    _barCodeReaderManager = BarcodeReaderManager.getInstance();
    _testExecution = TestExecution.getInstance();
    _mainUI = MainMenuGui.getInstance();
    _serialNumberToProjectName = new SerialNumberToProjectName();
    gatherSerialNumberHandlingOptions();

    if (_barCodeReaderManager.getBarcodeReaderAutomaticReaderEnabled())
      _isAutomaticBarCodeInstalled = true;
    else
      _isAutomaticBarCodeInstalled = false;

    _previousSerialNumber = "";  // initialize
  }

  /**
   * If return the empty list, then the panel was rejected and no serial numbers were accepted
   * Else, return a list of validated serial numbers, the first might be the CAD match number
   * @param serialNumber the serial number to process
   * @author Andy Mechtenberg
   */
  PanelSerialNumberData processKeyboardSerialNumber(String serialNumber, boolean isProcessingFirstSerialNumber) throws XrayTesterException
  {
    try
    {
      _processingSerialNumbers = true;
      _isProcessingFirstSerialNumber = isProcessingFirstSerialNumber;
      Assert.expect(serialNumber != null);
      // initialize variables for processing
      _validatedSerialNumbers = new ArrayList<String>();
      _projectName = "";
//    _validatedSerialNumbers.add(serialNumber);

      if (_preSelectPanel)
      {
        // figure out the preselect program
        _projectName = Project.getCurrentlyLoadedProject().getName();
      }

      // first step is determine if we need to "validate" this serial number
      if (_useFirstSerialNumberForProjectLookupOnly) // we don't "validate" these numbers
      {
        if (_preSelectPanel == false)
        {
          if (doesSerialNumberMatchProgramName(serialNumber) == false)
          {
            showRemovePanelFromLineWarning();
            return _invalidSerialNumber;
          }
          // check if the variation exist. If not, let user select.
          if (doesVariationExist() == false)
          {
            showRemovePanelFromLineWarning();
            return _invalidSerialNumber;
          }
        }
        if (_testExecPanel.okToUpdateImage())
          _testExecPanel.displayPanelImage(_projectName);
        // in the case of preselect cad used with "use first for cad match",  we just throw away the cad match number and
        // collect the first serial number
        // Now, get the REAL panel serial number.
        _validatedSerialNumbers.clear();
        LocalizedString prompt = new LocalizedString("TESTEXEC_GUI_SERIAL_NUMBER_FOR_PANEL_PROMPT_KEY",
          new Object[]
          {_projectName});
        String panelSerialNumber = gatherSerialNumber(StringLocalizer.keyToString(prompt));
        if (panelSerialNumber.equals("")) // user failed to enter one (cancelled out, most likely)
        {
          showRemovePanelFromLineWarning();
          return _invalidSerialNumber;
        }        
        serialNumber = panelSerialNumber;
      }
      // display this new REAL one onscreen
      // validate this serial number
      //Khaw Chek Hau - XCR2501 : preInspection Script is not executed for future serial number production run and change the flow
      //If the S/N is entered in future serial number text field, don't check isSerialNumberValid
      if (isProcessingFirstSerialNumber)
      {
        if (isSerialNumberValid(serialNumber))
          _validatedSerialNumbers.add(serialNumber);
        else
        {
          return _invalidSerialNumber;
        }
      }
      else 
        _validatedSerialNumbers.add(serialNumber);  
      
      // ok.  At this point, we have a Valid Serial Number!  If CAD Match number is on, we also have
      // a Project.  Next up (See flowchart) is to determine if we use this serial number to match cad
      // and do so.
      // It's at THIS point that we merge the automatic barcode stuff and the keyboard entry stuff
      // into a common flow.  At this point, you'll start seeing "if (automatic barcode)" type code.

      if (ableToCollectProjectAndBoardSerialNumbers() == false)
      {
        return _invalidSerialNumber;
      }

      // well, we made it!  We should have a valid project and a validated list of serial numbers
      Assert.expect(_projectName != null);
      Assert.expect(_projectName.length() > 0);
      Assert.expect(_validatedSerialNumbers.isEmpty() == false);
      
      PanelSerialNumberData panelSerialNumberData = new PanelSerialNumberData(_projectName, _validatedSerialNumbers, _variationName);
      return panelSerialNumberData;
    }
    finally
    {
      _processingSerialNumbers = false;
    }
  }

  /**
   * If return the empty list, then the panel was rejected and no serial numbers were accepted
   * Else, return a list of validated serial numbers, the first might be the CAD match number
   * @param serialNumbers a List of String representing all the collected serial number from the automatic barcode readers
   * @author Andy Mechtenberg
   */
  PanelSerialNumberData processAutomaticBarcodeSerialNumbers(List<String> serialNumbers) throws XrayTesterException
  {

    Assert.expect(serialNumbers != null);
    Assert.expect(serialNumbers.size() > 0);
    _validatedSerialNumbers = new ArrayList<String>();
    _projectName = "";

    if (_preSelectPanel ||
        _barCodeReaderManager.getBarcodeReaderScanMethod().equals(BarcodeReaderScanMethodEnum.SCAN_DURING_BOARD_LOADING.toString()))
    {
      // figure out the preselect program
      _projectName = Project.getCurrentlyLoadedProject().getName();
    }

    int numSerialNumbers = serialNumbers.size();
    for (int i = 0; i < numSerialNumbers; i++)
    {
      String currentSerialNumber = (String)serialNumbers.get(i);
      if (_checkAutomaticBarcodesForMisreads)
      {
        if (currentSerialNumber.equals(_noBarCodeReadMessage))
        {
          //Siew Yeng - XCR1726 - Support for multi-drop barcode reader setup
          if(_barCodeReaderManager.getBarcodeReaderScanMethod().equals(BarcodeReaderScanMethodEnum.SCAN_DURING_BOARD_LOADING.toString()))
          {
            return _invalidSerialNumber;
          }
          else
          {
            // notify the user and remove panel from line
            MessageDialog.showErrorDialog(_testExecPanel,
                                          new LocalizedString("TESTEXEC_GUI_BARCODE_READ_ERROR_KEY", new Object[]{new Integer(i)}),
                                          StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                          true,
                                          FontUtil.getMediumFont());
          }
          showRemovePanelFromLineWarning();
          return _invalidSerialNumber;
        }
      }
      if (isSerialNumberValid(currentSerialNumber) == false)
      {
        showRemovePanelFromLineWarning();
        return _invalidSerialNumber;
      }
    }
    
    // at this point, all incoming serial numbers have passed the validity check and are not consecutive duplicates
    _validatedSerialNumbers.addAll(serialNumbers);

    // we now, pick the up the rest of the processing at the cad matching portion of the flow.  This method is
    // used by both automatic barcodes and manual barcodes
    if (ableToCollectProjectAndBoardSerialNumbers() == false)
      return _invalidSerialNumber;
    
    //Siew Yeng - XCR1725 - Customizable Serial number Mapping
    _validatedSerialNumbers = SerialNumberMappingManager.getInstance().mapSerialNumbers(_validatedSerialNumbers);
    
    PanelSerialNumberData panelSerialNumberData = new PanelSerialNumberData(_projectName, _validatedSerialNumbers, _variationName);
    return panelSerialNumberData;
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean isProcessingSerialNumbers()
  {
    return _processingSerialNumbers;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void gatherSerialNumberHandlingOptions() throws XrayTesterException
  {
    _useSerialNumberToFindProject = _serialNumberManager.isUseSerialNumberToFindProjectEnabled();
    _useSerialNumbersForEachBoard = _serialNumberManager.isUseUniqueSerialNumberPerBoardEnabled();

    _preSelectPanel = _serialNumberManager.isTestExecPreSelectingProject();
    _checkForDuplicateSerialNumbers = _serialNumberManager.isCheckForDuplicateSerialNumbersEnabled();
    _useFirstSerialNumberForProjectLookupOnly = _serialNumberManager.isUseFirstSerialNumberForProjectLookupOnlyEnabled();
    _validateBoardSerialNumbers = _serialNumberManager.isValidateBoardSerialNumbersEnabled();

    _onlyAllowNewSerialNumberWhenNoProjectMatch = _serialNumberManager.areOnlyNewSerialNumbersAllowedWhenCadMatchFails();
    _checkAutomaticBarcodesForMisreads = _serialNumberManager.isBarcodeScannerReadErrorCheckEnabled();
    _noBarCodeReadMessage = _barCodeReaderManager.getBarcodeReaderNoReadMessage();
    DemoMode demoMode = _testExecPanel.getDemoMode();
    _isDemoModeOn = demoMode.isDemoModeEnabled();
  }

  /**
   * Pops up a dialog to prompt the user to enter a number.
   * The prompt allows us to use this for CAD match numbers as well as panel serial numbers
   * @author Andy Mechtenberg
   */
  String gatherSerialNumber(String prompt)
  {
    String panelSerialNumber = "";

    int value = _testExecPanel.promptUserForSerialNumber(prompt, false);
    if (value == JOptionPane.CANCEL_OPTION)
    {
      // we could be prompting for serial numbers, but they may cancel out of the panel serial number
      String confirmMessage = StringLocalizer.keyToString("TESTEXEC_GUI_CONFIRM_CANCEL_KEY");

      if (_testExecPanel.confirmAction(confirmMessage, true))
      {
        return "";  // return CANCEL_OPTION
      }
      else
        return gatherSerialNumber(prompt);
    }
    panelSerialNumber = _testExecPanel.getUserEnteredSerialNumber(); // this get's the data from the dialog box
    if (panelSerialNumber.equals(TestExecPanel._TRY_AGAIN))
    {
      // blank (empty string) serial numbers are not allowed in production mode
      // we don't warn them here -- all they see is the dialog up again
      return gatherSerialNumber(prompt);
    }
    return panelSerialNumber;
  }

  /**
   * @author Andy Mechtenberg
   * @author Lim Boon Hong. Add to display image of board serial number
   */
  private boolean gatherBoardSerialNumbers(List<String> serialNumbers)
  {
    Assert.expect(_numberOfBoards > 1);  // we never collect board serial numbers for single board panels
    Assert.expect(_numberOfBoards == _boardNames.size());
    Assert.expect(serialNumbers != null);
    String serialNumber;

    for (int i = 2; i <= _numberOfBoards; i++)
    {
      //To display image of board serial number by Lim Boon Hong
      _testExecPanel.displayPanelImage(_projectName, _boardNames.get(i-1));

      // get any queued up one or from user or from the automatic bar code reader
      // get the serial numbers from the automatic barcode reader
      LocalizedString boardPrompt = new LocalizedString("TESTEXEC_GUI_SERIAL_NUMBER_FOR_BOARD_PROMPT_KEY",
                                                        new Object[]
                                                        {_boardNames.get(i-1)});
      int value = _testExecPanel.promptUserForSerialNumber(StringLocalizer.keyToString(boardPrompt), true);
      if (value == JOptionPane.CANCEL_OPTION)
      {
        return false;
      }
      serialNumber = _testExecPanel.getUserEnteredSerialNumber();
      if (serialNumber.equals(TestExecPanel._TRY_AGAIN))
        i--; // loop back and try again
      else
      {
        // ok, we got a board serial number.  Now to do certain checks on it
        // first check is validation, if certain options are true
        _previousSerialNumber = (String)serialNumbers.get(serialNumbers.size() - 1);
        if (isSerialNumberValid(serialNumber))
          serialNumbers.add(serialNumber);
        else
          i--;
      }
    }

    //To display image of the first board by Lim Boon Hong
    _testExecPanel.displayPanelImage(_projectName, _boardNames.get(0));    

    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean isSerialNumberValid(String serialNumber)
  {
    Assert.expect(serialNumber != null);

    boolean isSerialNumberValid = true;
    if (_validateBoardSerialNumbers)
    {
      try
      {
        isSerialNumberValid = _serialNumberManager.isSerialNumberValid(serialNumber);
      }
      catch (XrayTesterException ex)
      {
        _testExecPanel.displayError(ex);
        _testExecPanel.exitTestExecution();
        return false;
      }
      if (isSerialNumberValid == false)
      {
        _mainUI.changeLightStackToStandby();
        // pop up a warning dialog to tell them we don't have a valid serial number
        MessageDialog.showErrorDialog(_testExecPanel,
                                      new LocalizedString("TESTEXEC_GUI_ERROR_INVALID_SERIAL_NUMBER_KEY", null),
                                      StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                      true,
                                      FontUtil.getMediumFont());
        _testExecPanel.changeLightStackToInspectionInProgess();
        return false;
      }
    }
    // we next check for duplicates, unless we've thrown it out already
    if (isSerialNumberValid && _checkForDuplicateSerialNumbers)
    {
      if (_previousSerialNumber.equalsIgnoreCase(serialNumber))
      {
        _mainUI.changeLightStackToStandby();
        // pop up a warning dialog to tell them we found duplicate serial numbers
        MessageDialog.showWarningDialog(_testExecPanel,
                                        StringLocalizer.keyToString("TESTEXEC_GUI_WARNING_DUPLICATE_SERIAL_NUMBER_KEY"),
                                        StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                        true,
                                        FontUtil.getMediumFont());

        // ok, now we have to ask the user if this is ok
        int choice = ChoiceInputDialog.showConfirmDialog(_testExecPanel,
                                                         StringLocalizer.keyToString("TESTEXEC_GUI_ACCEPT_DUPLICATE_SERIAL_NUMBER_KEY"),
                                                         StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                                         FontUtil.getMediumFont());
        _testExecPanel.changeLightStackToInspectionInProgess();
        if (choice == JOptionPane.YES_OPTION)
          return true;
        else
          return false;
      }
      else
      {
        _previousSerialNumber = serialNumber;
        return true;
      }
    }
    else
      return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  void showRemovePanelFromLineWarning() throws XrayTesterException
  {
    _mainUI.changeLightStackToStandby();
    if (_isAutomaticBarCodeInstalled)
    {
      MessageDialog.showWarningDialog(_testExecPanel,
                                      StringLocalizer.keyToString("TESTEXEC_GUI_REMOVE_BOARD_AUTOMATIC_READER_KEY"),
                                      StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                      true,
                                      FontUtil.getMediumFont());
    }
    else
    {
      MessageDialog.showWarningDialog(_testExecPanel,
                                      StringLocalizer.keyToString("TESTEXEC_GUI_REMOVE_BOARD_KEY"),
                                      StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                      true,
                                      FontUtil.getMediumFont());
    }
    _testExecPanel.updatePanelImage();
    _testExecPanel.changeLightStackToInspectionInProgess();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void displayBoardToSerialNumberMismatchError(int numberOfSerialNumbers, int totalBoards) throws XrayTesterException
  {
    MessageDialog.showErrorDialog(_testExecPanel,
                                  new LocalizedString("TESTEXEC_GUI_SERIAL_NUMBER_MISMATCH_BOARDS_KEY",
                                                      new Object[] {new Integer(numberOfSerialNumbers),
                                                      new Integer(totalBoards)}),
                                  StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                  true,
                                  FontUtil.getMediumFont());

    showRemovePanelFromLineWarning();
  }

  /**
   * This method will do the logic of looping over user entered panel serial numbers
   * until it can find a panel program it matches
   * @return false if the user needs a new serial number
   * @author Andy Mechtenberg
   */
  private boolean doesSerialNumberMatchProgramName(String panelSerialNumber) throws XrayTesterException
  {
    Assert.expect(panelSerialNumber != null);
    _projectName = "";

    _projectName = _serialNumberToProjectName.getProjectNameFromSerialNumber(panelSerialNumber);

    if (_projectName != "")
      _variationName = _serialNumberManager.getVariationNameFromSerialNumber(panelSerialNumber);
    // ok, we know which project to try to use.  But, before we load it, or try to read the
    // panel file to get the number of boards, we should check to see that the project does indeed
    // exist.
    return getProjectFromUser(panelSerialNumber);
  }

  /**
   * @author Andy Mechtenberg
   */
  private boolean getProjectFromUser(String panelSerialNumber) throws XrayTesterException
  {
    boolean doesProjectExist = true;

    if ((_projectName != "") && ((Project.doesProjectExistLocally(_projectName) == false)))
    {
      // denotes the serial number matched some project name, but the project did not exist
      doesProjectExist = false;
    }

    // there are two problems that we need to handle.  The serial number did not match anything, or the project that
    // it matched is not loadable (ndfs/rtfs do not exist)
    if (_projectName.equals("") || (doesProjectExist == false))
    {
      // no match. Now we ask if they want to re-enter serial number or choose a program manually
      // based, of course, off an undocumented autoui.cfg option (woo)
      LocalizedString matchingErrorStr;
      if (_projectName.equals(""))
        matchingErrorStr = new LocalizedString("TESTEXEC_GUI_NO_MATCH_FOUND_KEY", new Object[] {panelSerialNumber});
      else
        matchingErrorStr = new LocalizedString("TESTEXEC_GUI_PROJECT_DOES_NOT_EXIST_KEY", new Object[] {_projectName});

  //        Object[] options = {"Enter New Serial Number", "Pick Panel Name"};

      // there's a config option that supresses the users ability to pick a project manually when
      // the serial number does not match.  Check for that here.
      if (_onlyAllowNewSerialNumberWhenNoProjectMatch)
      {
        _mainUI.changeLightStackToStandby();
        MessageDialog.showErrorDialog(_testExecPanel,
                                      matchingErrorStr,
                                      StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                      true,
                                      FontUtil.getMediumFont());
        _testExecPanel.changeLightStackToInspectionInProgess();

        // loop back to enter a new serial number
        return false;
      }

      // combine the no match message with the choice so the user only has to interact with ONE dialog instead
      // of two
      // offer the choice of entering a new serial number or picking a program manually
      Object[] options =
        {StringLocalizer.keyToString("TESTEXEC_GUI_ENTER_NEW_SERIAL_NUMBER_KEY"),
        StringLocalizer.keyToString("TESTEXEC_GUI_PICK_PANEL_PROGRAM_KEY")};

      _mainUI.changeLightStackToStandby();
      int choice = ChoiceInputDialog.showOptionDialog(_testExecPanel,
                                                      StringLocalizer.keyToString(matchingErrorStr),
                                                      StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                                      options,
                                                      options[1],
                                                      FontUtil.getMediumFont());
      _testExecPanel.changeLightStackToInspectionInProgess();

      // YES means to enter a new serial number.
      // The Closed option means they exited the dialog without choosing, which we'll treat as enter a new number
      if ((choice == JOptionPane.YES_OPTION) || (choice == JOptionPane.CLOSED_OPTION))
      {
        return false;
      }
      else // no option which is mapped to pick a new project
      {
        // now, they'll pick a program manually, since there was no match
        int value = _testExecPanel.choosePanelProgram(false, false, false);
        if (value == JOptionPane.CANCEL_OPTION)
        {
          return false;
        }
        else
        {
          _projectName = _testExecPanel.getLastChosenProject();
          _numberOfBoards = _testExecPanel.getNumberOfBoards(_projectName);
          _variationName = _testExecPanel.getCurrentVariation();
          _boardNames = _testExecPanel.getBoardNames(_projectName);
          return true;
        }
      }
    }
    else
    {
      // we have a match.  Set _projectName
      return true;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private boolean ableToCollectProjectAndBoardSerialNumbers() throws XrayTesterException
  {
    String tentativeSerialNumber = "";
    
    if (_useFirstSerialNumberForProjectLookupOnly == false)
    {
      _variationName = VariationSettingManager.ALL_LOADED;
    }
    //Truth table to decide if we should go for the match using the serial numbers:
    //
    //  Pre-select   Automatic Barcode    Use First to Match only     Result (try to match?)
    //-----------------------------------------------------------------------
    //      0              0                       0                    1
    //      0              0                       1                    0
    //      0              1                       0                    1
    //      0              1                       1                    1  (automatic overrides use 1st to match)
    //      1              0                       0                    0
    //      1              0                       1                    0
    //      1              1                       0                    0
    //      1              1                       1                    0
    //
    // this boils down to (using boolean algebra) result = P' (A + U')
    if ((_preSelectPanel == false) &&
        (_isAutomaticBarCodeInstalled || (_useFirstSerialNumberForProjectLookupOnly == false)))
    {
      if (_useSerialNumberToFindProject)
      {
        if (_isAutomaticBarCodeInstalled)
        {
          // the end result of this block of code is have the _validSerialNumbers list set properly, and the proper
          // project name set
          // automatic barcodes are different than keyboard in that ALL barcodes are read in at once and saved
          // in the _serialNumbers list.  To determine which program to load, we try each serial number, one at
          // a time until we find a match.
          // Then, if not using board-based serial numbers, we re-set the list to hold just that one.  If using
          // board-based, then confirm the list is just long enough to cover all the boards
          _projectName = "";
          Iterator it = _validatedSerialNumbers.iterator();
          while (it.hasNext())
          {
            tentativeSerialNumber = (String)it.next();

            _projectName = _serialNumberToProjectName.getProjectNameFromSerialNumber(tentativeSerialNumber);
            if (_projectName.equals("") == false) // got a match, don't try any more
              break;
          }
          // we know that tentativeSerialNumber either will not match or will generate a match
          // so we'll call this method which will handle either case and
          // allows us to re-use the "does not match" part of the serial number flow
          // -- displays the error message
          // -- asks the user (based on an option) if they want to pick a panel or get a new serial number
          if (getProjectFromUser(tentativeSerialNumber) == false)
          {
            // warn to remove panel from line
            showRemovePanelFromLineWarning();
            return false;
          }
          _variationName = getVariationName(tentativeSerialNumber, _projectName);
        }
        else  // keyboard input -- need to match on the first serial number ONLY
        {
          Assert.expect(_validatedSerialNumbers.size() == 1);
          String panelSerialNumber = (String)_validatedSerialNumbers.get(0);
          if (doesSerialNumberMatchProgramName(panelSerialNumber) == false)
          {
            // warn to remove panel from line
            showRemovePanelFromLineWarning();
            return false;
          }
          // check if the variation exist. If not, let user select.
          if (doesVariationExist() == false)
          {
            showRemovePanelFromLineWarning();
            return false;
          }
        }
      }
      else // user needs to pick the program EACH TIME!
      {
          // CR1013 fix by LeeHerng - Add a checking here to make sure project name match with loaded panel name.
          boolean isProjectNameVerifiedOk = false;
          while (isProjectNameVerifiedOk == false)
          {
            int value = JOptionPane.OK_OPTION;
            if(_projectName.equals(""))
            { 
              // load the panel to be used for the entire production flow session
              value = _testExecPanel.choosePanelProgram(false, false, false);
            }
              if (value == JOptionPane.CANCEL_OPTION)
              {
                  // warn to remove panel from line
                  showRemovePanelFromLineWarning();
                  isProjectNameVerifiedOk = true;
                  return false;
              }
              else // the user chose a program
              {
                if(_projectName.equals(""))
                  _projectName = _testExecPanel.getLastChosenProject();

                  // CR1048 fix by LeeHerng - Add a checking to differenciate between first time-entered serial
                  // number and subsequent serial number. And also, if we turn on OVERRIDE_PANEL_EJECT_FOR_TEST_EXEC,
                  // we will still need to perform project name matching checking.
                  if (_isProcessingFirstSerialNumber ||
                      Config.getInstance().getBooleanValue(SoftwareConfigEnum.OVERRIDE_PANEL_EJECT_FOR_TEST_EXEC))
                  {
                    boolean hasHighMagComponent = TestExecution.getInstance().containHighMagInspectionInCurrentLoadedProject();
                      boolean isPanelLoaded = TestExecution.getInstance().isPanelLoaded(hasHighMagComponent, _projectName);
                      if (isPanelLoaded)
                      {
                          String loadedPanelProjectName = PanelHandler.getInstance().getLoadedPanelProjectName();
                          boolean isProjectNameMatchWithPanelName = TestExecution.getInstance().isProjectNameMatchWithPanelName(_projectName, loadedPanelProjectName);
                          if (isProjectNameMatchWithPanelName)
                              isProjectNameVerifiedOk = true;
                          else
                          {
                                // ask the user if they want to use panel project or eject the panel
                                Object[] options = {StringLocalizer.keyToString("TESTEXEC_GUI_UNLOAD_PANEL_KEY"),
                                                    StringLocalizer.keyToString("TESTEXEC_GUI_USE_PANEL_PROJECT_KEY"),
                                                    StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")};

                                _mainUI.changeLightStackToStandby();
                                int choice = ChoiceInputDialog.showOptionDialog(_testExecPanel,
                                                                                StringLocalizer.keyToString("TESTEXEC_GUI_PANEL_PROJECT_NAME_DOES_NOT_MATCH_PROJECT_NAME_KEY"),
                                                                                StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                                                                options,
                                                                                options[0],
                                                                                FontUtil.getMediumFont());
                                if ((choice == JOptionPane.CANCEL_OPTION) || (choice == JOptionPane.CLOSED_OPTION))
                                {
                                    isProjectNameVerifiedOk = false;
                                    return false;//Punit.2839: returns false on cancel or close
                                }
                                else if (choice == JOptionPane.YES_OPTION) // first option, which is to unload the panel
                                {
                                  _mainUI.unloadPanel();
                                  isProjectNameVerifiedOk = false;
                                }
                                else
                                {
                                    // use panel project
                                    _projectName = loadedPanelProjectName;
                                    isProjectNameVerifiedOk = true;
                                }
                          }
                      }
                      else
                          isProjectNameVerifiedOk = true;
                  }
                  else
                      isProjectNameVerifiedOk = true;
              }
          }
          boolean preSelectVariation = ProjectReader.getInstance().readProjectPreSelectVariation(_projectName);
          if (preSelectVariation == false || 
              (_isAutomaticBarCodeInstalled && _useFirstSerialNumberForProjectLookupOnly && preSelectVariation == false))
          {
            _variationName = _testExecPanel.getCurrentVariation();
          }
      }
    }
    else if (_preSelectPanel && ProjectReader.getInstance().readProjectPreSelectVariation(_projectName) == false)
    {
      List<String> variationList = VariationSettingManager.getInstance().getProjectNameToVariationNamesMap(_projectName);
      if (_isFutureSerialNumberInput)
      {
        //pop out choose variation dialog
        if (variationList.isEmpty() == false)
        {
          int value = _testExecPanel.choosePanelProgram(false, false, true);
          if (value == JOptionPane.CANCEL_OPTION)
          {
            return false;
          }
          else
          {
            _variationName = _testExecPanel.getCurrentVariation();
          }
        }
      }
      else
      {
        if (variationList.isEmpty())
        {
          _variationName = VariationSettingManager.ALL_LOADED;
        }
        else
        {
          _variationName = _testExecPanel.getCurrentVariation();
        }
      }
    }
    boolean preSelectVariation = ProjectReader.getInstance().readProjectPreSelectVariation(_projectName);
    if ((preSelectVariation && _useSerialNumberToFindProject == false && _useFirstSerialNumberForProjectLookupOnly == false) 
        || (preSelectVariation && _isAutomaticBarCodeInstalled && _useFirstSerialNumberForProjectLookupOnly))
    {
      String panelSerialNumber = (String) _validatedSerialNumbers.get(0);
      _variationName = getVariationName(panelSerialNumber, _projectName);
    }
    
    Assert.expect(_projectName != null);
    Assert.expect(_projectName.length() > 0);
    if (_testExecPanel.okToUpdateImage())
      _testExecPanel.displayPanelImage(_projectName);

    _numberOfBoards = _testExecPanel.getNumberOfBoards(_projectName);
    _boardNames = _testExecPanel.getBoardNames(_projectName);
    int numberOfSerialNumbers = _validatedSerialNumbers.size();

    if (_isAutomaticBarCodeInstalled)
    {
      // do some sanity checks.  Make sure the number of serial numbers is appropriate for the
      // program loaded and the options selected
      // a few things to do:  if using serial number to match cad, the one that DID match the CAD is the
      // the panel serial number.  That's stored in the variable tentativeSerialNumber
      if (_useSerialNumbersForEachBoard)
      {
        if (numberOfSerialNumbers != _numberOfBoards)
        {
          displayBoardToSerialNumberMismatchError(numberOfSerialNumbers, _numberOfBoards);
          showRemovePanelFromLineWarning();
          return false;
        }
      }
      if (tentativeSerialNumber.equals("")) // we didn't use a serial number to match cad
      {
        // there's one weird case.  You might have 6 serial numbers and 6 boards, but are NOT using
        // board serial numbers.  In that case, we need ONE serial number.  We'll pick the first one
        if ((_useSerialNumbersForEachBoard == false) && (numberOfSerialNumbers > 1))
        {
          String firstSerialNumber = (String)_validatedSerialNumbers.get(0);
          _validatedSerialNumbers.clear();
          _validatedSerialNumbers.add(firstSerialNumber);
        }
      }
      else // we DID use a serial number to match cad (and tentativeSerialNumber is the one that did match)
      {
        if (_useSerialNumbersForEachBoard == false) // theres just the need for a panel serial number.  Use the one that matched the cad
        {
          _validatedSerialNumbers.clear();
          _validatedSerialNumbers.add(tentativeSerialNumber);
        }
      }
    }
    else // not using the automatic barcode, so gather board serial numbers if necessary
    {
      if ((_isDemoModeOn == false) && (_useSerialNumbersForEachBoard) && (_numberOfBoards > 1) &&
          (_isAutomaticBarCodeInstalled == false))
      {
        // in this case, we need to gather all the board serial numbers
        // for some reason, legacy autotest validates Board Serial Numbers only IF we're using the useFirstSerialNumber to match cad.
        // I don't understandy why that is, but that's what autotest does.
        // We're NOT doing that here, after some discussion
//        boolean validateBoardSerialNumbers = _validateBoardSerialNumbers && _useFirstSerialNumberForProjectLookupOnly;

        if (gatherBoardSerialNumbers(_validatedSerialNumbers) == false) // if false, the user aborted asnd we should move to next panel
        {
          // show the warning because we've already collected some serial numbers
          showRemovePanelFromLineWarning();
          return false;
        }
      }
    }
    return true;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public String getVariationName(String serialNumber, String projectName) throws DatastoreException, HardwareException
  {
    Assert.expect(serialNumber != null);
    Assert.expect(projectName != null);
      
    String variationName = "";
    boolean isVariationEnable = false;
    Map<String, Pair<String, String>> variationNameToEnabledMap = VariationSettingManager.getInstance().getVariationNamesToEnabledMap(projectName);
    
    if (_preSelectPanel == false && _useSerialNumberToFindProject)
    {
      variationName = _serialNumberManager.getVariationNameFromSerialNumber(serialNumber);
    }
    
    boolean preSelectVariation = ProjectReader.getInstance().readProjectPreSelectVariation(projectName);
    if ((preSelectVariation && _useSerialNumberToFindProject == false && _useFirstSerialNumberForProjectLookupOnly == false) 
        || (preSelectVariation && _isAutomaticBarCodeInstalled && _useFirstSerialNumberForProjectLookupOnly))
    {
      for (Map.Entry<String, Pair<String, String>> variationToEnabledSet : variationNameToEnabledMap.entrySet())
      {
        String name = variationToEnabledSet.getKey();
        Pair<String, String> pair = variationToEnabledSet.getValue();
        boolean isEnabled = Boolean.parseBoolean(pair.getFirst());
        if (isEnabled)
        {
          isVariationEnable = true;
          variationName = name;
          break;
        }
      }
      //if not variation is enable in panel setup tab, set the variation name to empty.
      if (isVariationEnable == false)
      {
        variationName = VariationSettingManager.ALL_LOADED;
      }
    }
    return variationName;
  }
  
  /**
   * @author Kok Chun, Tan
   * if user key in the future serial number in production, this key will be true
   */
  public void setFutureSerialNumberInput(boolean isFutureSerialNumberInput)
  {
    _isFutureSerialNumberInput = isFutureSerialNumberInput;
  }
  
    /**
   * @author Kok Chun, Tan
   */
  public String getPreSelectVariationName(String projectName) throws DatastoreException, HardwareException
  {
    Assert.expect(projectName != null);
    
    String variationName = "";
    boolean preSelectVariation = ProjectReader.getInstance().readProjectPreSelectVariation(projectName);
    if (preSelectVariation)
    {
      boolean isVariationEnable = false;
      Map<String, Pair<String, String>> variationNameToEnabledMap = VariationSettingManager.getInstance().getVariationNamesToEnabledMap(projectName);

      for (Map.Entry<String, Pair<String, String>> variationToEnabledSet : variationNameToEnabledMap.entrySet())
      {
        String name = variationToEnabledSet.getKey();
        Pair<String, String> pair = variationToEnabledSet.getValue();
        boolean isEnabled = Boolean.parseBoolean(pair.getFirst());
        if (isEnabled)
        {
          isVariationEnable = true;
          variationName = name;
          break;
        }
      }
      //if not variation is enable in panel setup tab, set the pre select to all loaded.
      if (isVariationEnable == false)
      {
        variationName = VariationSettingManager.ALL_LOADED;
      }
    }
    return variationName;
  }
  
  /**
   * Check variation name exist when using CAD match / serial number match project name
   * @author Kok Chun, Tan
   */
  public boolean doesVariationExist() throws DatastoreException
  {
    // no need check if preselect variation
    boolean preSelectVariation = ProjectReader.getInstance().readProjectPreSelectVariation(_projectName);
    if (preSelectVariation)
      return true;
    
    // if variation does not exist throw error. This may due to user is using CAD match/serial number match project name. 
    // The user does not update the matching table after change the variation name.
    List<String> variationNames = VariationSettingManager.getInstance().getProjectNameToVariationNamesMap(_projectName);
    if (_variationName.equals(VariationSettingManager.ALL_LOADED) == false && variationNames.contains(_variationName) == false)
    {
      // prompt error message to notice user
      MessageDialog.showWarningDialog(_testExecPanel,
                                      StringLocalizer.keyToString(new LocalizedString("TESTEXEC_GUI_VARIATION_DOES_NOT_EXIST_KEY", new Object[]{_variationName, _projectName})),
                                      StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                      true,
                                      FontUtil.getMediumFont());
      // let user choose one
      int value = _testExecPanel.choosePanelProgram(false, false, false, _projectName);
      if (value == JOptionPane.CANCEL_OPTION)
      {
        return false;
      }
      else
      {
        _variationName = _testExecPanel.getCurrentVariation();
      }
    }
    return true;
  }
}
