package com.axi.v810.gui.testExec;

import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

public class TestExecEnvironment extends AbstractEnvironmentPanel
{
  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getTestExecutionInstance();
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private TestExecPersistance _guiPersistSettings;
  private TaskPanelScreenEnum _currentScreen = null;
  private TestExecPanel _testExecPanel = null;

  // Not user visible strings -- used to switch in the CardLayout between different tasks
  private final static String _OPERATOR_PANEL = "Operator Panel";

  public TestExecEnvironment(MainMenuGui mainUI)
  {
    super(mainUI);
    // single task panel requires simple navigation but requires
    // task panel to be initialized
    _navigationPanel = new TestExecNavigationPanel(this);
   try
    {
      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public com.axi.v810.gui.undo.CommandManager getCommandManager()
  {
    return _commandManager;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit() throws Exception
  {
//    _centerPanel = new PanelFaker("c:/temp/TestExecPanel.jpg");
//    this.add(_centerPanel, BorderLayout.CENTER);

    // initialize panels if needed
    _testExecPanel = new TestExecPanel(this);
    _centerPanel.add(_testExecPanel, _OPERATOR_PANEL);
//    _centerPanel.add(_operatorPanel, _OPERATOR_PANEL);
    _mainCardLayout.first(_centerPanel);

    // define custom menus and toolBar items
    // they are not added until start()
    // <none currently defined>

    _currentScreen = TaskPanelScreenEnum.NULL_SCREEN;
  }

  /**
   * change to a different screen.
   * @author Andy Mechtenberg
   */
  public void switchToScreen(TaskPanelScreenEnum screen)
  {
    Assert.expect(screen != null);
    Assert.expect(screen.equals(screen.OPERATOR_PANEL));
//    switchToTask(_centerPanel, _operatorPanel, _OPERATOR_PANEL);
    switchToTask(_centerPanel, _testExecPanel, _OPERATOR_PANEL);
  }

  /**
   * called when a navigation button is clicked.
   * @param buttonName String
   * @author George Booth
   */
  public void navButtonClicked(String buttonName)
  {
    // buttonName will be "" for single task
    TaskPanelChangeScreenCommand command = new TaskPanelChangeScreenCommand(this, _currentScreen,
        TaskPanelScreenEnum.OPERATOR_PANEL);
    _commandManager.execute(command);
    _currentScreen = TaskPanelScreenEnum.OPERATOR_PANEL;
    _guiObservable.stateChanged(null, TaskPanelScreenEnum.OPERATOR_PANEL);
  }

  /**
   * The environment is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("TestExec().start()");

    // use the common status bar
//    _mainUI.addStatusBar();
//    _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_EMPTY_STATUS_KEY") +
//                             ": displaying c:/temp/TestExecPanel.jpg");

    // start the task
    readPersistance();
    _navigationPanel.selectDefaultTask();
  }

  /**
   * User has requested an environment change. Is it OK to leave this environment?
   * @return true if environment can be exited
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    return _testExecPanel.isReadyToFinish();
  }

  /**
   * The environment is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    // finish current task
    finishCurrentTaskPanel();
    // allow restart with default task even if it was the current task
    _currentScreen = TaskPanelScreenEnum.NULL_SCREEN;
    _navigationPanel.clearCurrentNavigation();

//    System.out.println("TestExec().finish()\n");
    // remove status bar
    _mainUI.removeStatusBar();
  }

  /**
   * This will read in the persist data
   * @author Andy Mechtenberg
   */
  private TestExecPersistance readPersistance()
  {
    _guiPersistSettings = new TestExecPersistance();
    _guiPersistSettings = (TestExecPersistance)_guiPersistSettings.readSettings();
    return _guiPersistSettings;
  }

  /**
   * @author Andy Mechtenberg
   */
  public TestExecPersistance getPersistance()
  {
    Assert.expect(_guiPersistSettings != null);
    return _guiPersistSettings;
  }

  /**
   * This would write individual screen's persistance data, if any.
   * Called from the Main Menu when exiting.
   * @author Andy Mechtenberg
   */
  public void writePersistance()
  {
    if (_guiPersistSettings != null)
      _guiPersistSettings.writeSettings();
  }

  /**
   * @author Poh Kheng
   */
  public void clearProjectInfo()
  {
    _testExecPanel.clearProjectInfo();
  }

}
