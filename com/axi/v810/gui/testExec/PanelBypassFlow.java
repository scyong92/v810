package com.axi.v810.gui.testExec;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * This class handles the Panel Bypass Mode test execution mode
 * @author Andy Mechtenberg
 */
public class PanelBypassFlow extends TestFlow implements NumericInputDialogOutOfRangeInt
{
  private PanelHandler _panelHandler;

  /**
   * @author Andy Mechtenberg
   */
  PanelBypassFlow()
  {
    _panelHandler = PanelHandler.getInstance();
    // All variable initialization takes place in execute() because this constructor may be called
    // only once, and these variables need to be initialized for each invokation of the test exec gui
  }

  /**
   * Runs the UI through bypass mode.  All other modes are ignored in this mode.
   * @author Andy Mechtenberg
   */
  void execute()
  {
    // we need access to stuff like testExec, hardware config, software config, etc.  So let's grab the interfaces here
    _testExecPanel.setupInterfaceForBypassMode();
    double minPanelWidthInMils = 0.0;
    double maxPanelWidthInMils = 0.0;

    maxPanelWidthInMils = MathUtil.convertNanoMetersToMils(_panelHandler.getMaximumPanelWidthInNanoMeters());
    minPanelWidthInMils = MathUtil.convertNanoMetersToMils(_panelHandler.getMinimumPanelWidthInNanoMeters());

    int initialValueInMils = 12000;  // default value if no project is loaded (a loaded project is NOT a requirement for bypass)

    if (Project.isCurrentProjectLoaded())
    {
      Project project = Project.getCurrentlyLoadedProject();
      int widthInNanoMeters = project.getPanel().getWidthAfterAllRotationsInNanoMeters();
      initialValueInMils = (int)MathUtil.convertNanoMetersToMils((float)widthInNanoMeters);
    }

    try
    {
      if (XrayActuator.isXRayCylinderInstalled() == true && 
         ((XrayActuator.getInstance().isUp() == false) || (XrayActuator.getInstance().isDown() == true)))
      {
        String message = StringLocalizer.keyToString("TESTEXEC_GUI_XRAY_CYLINDER_MOVE_UP_WARNING_KEY");

        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      message,
                                      StringLocalizer.keyToString("TEGUI_ENV_NAME_KEY"),
                                      JOptionPane.WARNING_MESSAGE);
        HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
        {
          public void run() throws XrayTesterException
          {
            try
            {
              XrayActuator.getInstance().up(false);
            }
            catch (XrayTesterException xte)
            {
              System.out.println("ERROR:Xray cylinder failed to move up during PanelBypassFlow during Production.");
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(), xte.getMessage(), new String("PanelBypassFlow during Production") ,JOptionPane.WARNING_MESSAGE);
            }
          }
        });
      }
      else if (XrayActuator.isZAxisMotorInstalled() == true && 
              ( (XrayActuator.getInstance().isHome() == false) || XrayActuator.getInstance().isDown() || XrayActuator.getInstance().isUp()))
      {
        String message = StringLocalizer.keyToString("TESTEXEC_GUI_XRAY_Z_AXIS_MOVE_HOME_WARNING_KEY");

        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      message,
                                      StringLocalizer.keyToString("TEGUI_ENV_NAME_KEY"),
                                      JOptionPane.WARNING_MESSAGE);
        HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
        {
          public void run() throws XrayTesterException
          {
            try
            {
              XrayActuator.getInstance().home(false,false);
            }
            catch (XrayTesterException xte)
            {
              System.out.println("ERROR:Xray Z Axis failed to move home during PanelBypassFlow during Production.");
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(), xte.getMessage(), new String("PanelBypassFlow during Production") ,JOptionPane.WARNING_MESSAGE);
            }
          }
        });
      }
    }
    catch (XrayTesterException xte)
    {
      System.out.println("ERROR:Unable to retrieve Xray cylinder state during PanelBypassFlow during Production.");
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(), xte.getMessage(), new String("PanelBypassFlow during Production") ,JOptionPane.WARNING_MESSAGE);
    }

    String intialValueStr = String.valueOf(initialValueInMils);
    NumericInputDialog panelWidthDialog = new NumericInputDialog(_frame,
                                                                 StringLocalizer.keyToString("TESTEXEC_GUI_PANEL_WIDTH_TITLE_KEY"),
                                                                 StringLocalizer.keyToString("TESTEXEC_GUI_PANEL_WIDTH_MESSAGE_KEY"),
                                                                 StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                                                 StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                                                 intialValueStr,
                                                                 minPanelWidthInMils,
                                                                 maxPanelWidthInMils,
                                                                 this,
                                                                 FontUtil.getMediumFont());
    MainMenuGui.getInstance().changeLightStackToStandby();
    int returnValue = panelWidthDialog.showDialog();

    if (returnValue == JOptionPane.OK_OPTION)
    {
      final int panelWidthInMils = panelWidthDialog.getIntValue();
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            //dummy serial number
            java.util.List<Pair<String, String>> boardNameToSerialNumberPairs = new java.util.ArrayList<Pair<String, String>> ();
            Pair<String, String> dummyBoardNameToSerialNumberPair = new Pair<String, String> ("1", "1");
            boardNameToSerialNumberPairs.add(dummyBoardNameToSerialNumberPair);
            _testExecPanel.setTestInProgress(true);
            _testExecution.bypassPanel((int)MathUtil.convertMilsToNanoMeters(panelWidthInMils),(int)MathUtil.convertMilsToNanoMeters(panelWidthInMils), false, boardNameToSerialNumberPairs);
            _testExecPanel.setTestInProgress(false);
          }
          catch (final XrayTesterException xte)
          {
            _testExecPanel.setTestInProgress(false);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _testExecPanel.displayError(xte);
              }
            });
          }
          // when the testing is over, we always exit test execution
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              _testExecPanel.exitTestExecution();
            }
          });

        }
      });
    }
    else if (returnValue == JOptionPane.CANCEL_OPTION)
    {
      // exit altogether from Test Execution
      _testExecPanel.exitTestExecution();
    }
    else
      Assert.expect(false, "Unknown return value: " + returnValue);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getIntValueOutOfRangeLocalizedString(String value, double min, double max)
  {
    LocalizedString localizedString = new LocalizedString("EX_INT_WITHIN_RANGE_EXPECTED_KEY",
                                                          new Object[]{value,
                                                                       new Double(min),
                                                                       new Double(max)});
    return StringLocalizer.keyToString(localizedString);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getErrorTitle()
  {
    return StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY");
  }

  /**
   * @author Kok Chun, Tan
   */
  protected void loadVariationSetting(String name)
  {
    // do nothing
  }

  /**
   * @author Kok Chun, Tan
   */
  protected String getVariationName()
  {
    return "";
  }
}
