/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.gui.testExec;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 *
 * @author Ying-Huan.Chu
 */
public class ProductionResultTableModel extends DefaultSortTableModel
{
  private static final int _INSPECTION_START_TIME_COLUMN_INDEX = 0;
  private static final int _PROJECT_NAME_COLUMN_INDEX = 1;
  private static final int _SERIAL_NUMBER_COLUMN_INDEX = 2;
  private static final int _PANEL_PINS_PROCESSED_COLUMN_INDEX = 3;
  private static final int _PANEL_PINS_DEFECTIVE_COLUMN_INDEX = 4;
  private static final int _PANEL_COMPONENTS_DEFECTIVE_COLUMN_INDEX = 5;
  private static final int _PANEL_LOADING_TIME_COLUMN_INDEX = 6;
  private static final int _INSPECTION_TIME_COLUMN_INDEX = 7;
  private static final int _PANEL_UNLOADING_TIME_COLUMN_INDEX = 8;
  private static final int _INSPECTION_STATUS_COLUMN_INDEX = 9;
  private static final int _INSPECTION_CALL_RATE_COLUMN_INDEX = 10;
  
  private static final String _INSPECTION_START_TIME = StringLocalizer.keyToString("PRT_INSPECTION_START_TIME_COLUMN_KEY");
  private static final String _PROJECT_NAME = StringLocalizer.keyToString("PRT_PROJECT_NAME_COLUMN_KEY");
  private static final String _SERIAL_NUMBER = StringLocalizer.keyToString("PRT_SERIAL_NUMBER_COLUMN_KEY");
  private static final String _PANEL_PINS_PROCESSED = StringLocalizer.keyToString("PRT_PANEL_PINS_PROCESSED_COLUMN_KEY");
  private static final String _PANEL_PINS_DEFECTIVE = StringLocalizer.keyToString("PRT_PANEL_PINS_DEFECTIVE_COLUMN_KEY");
  private static final String _PANEL_COMPONENTS_DEFECTIVE = StringLocalizer.keyToString("PRT_PANEL_COMPONENTS_DEFECTIVE_COLUMN_KEY");
  private static final String _PANEL_LOADING_TIME = StringLocalizer.keyToString("PRT_PANEL_LOADING_TIME_COLUMN_KEY");
  private static final String _INSPECTION_TIME = StringLocalizer.keyToString("PRT_INSPECTION_TIME_COLUMN_KEY");
  private static final String _PANEL_UNLOADING_TIME = StringLocalizer.keyToString("PRT_PANEL_UNLOADING_TIME_COLUMN_KEY");
  private static final String _INSPECTION_STATUS = StringLocalizer.keyToString("PRT_INSPECTION_STATUS_COLUMN_KEY");
  private static final String _INSPECTION_CALL_RATE = StringLocalizer.keyToString("PRT_INSPECTION_CALL_RATE_COLUMN_KEY");
  
  private String[] _columnLabels = {_INSPECTION_START_TIME,
                                    _PROJECT_NAME,
                                    _SERIAL_NUMBER,
                                    _PANEL_PINS_PROCESSED,
                                    _PANEL_PINS_DEFECTIVE,
                                    _PANEL_COMPONENTS_DEFECTIVE,
                                    _PANEL_LOADING_TIME,
                                    _INSPECTION_TIME,
                                    _PANEL_UNLOADING_TIME,
                                    _INSPECTION_STATUS,
                                    _INSPECTION_CALL_RATE
                                   };
  private List<ProductionResult> _productionResults;
  private static ProductionResultComparator _productionResultComparator;
  
  static
  {
    _productionResultComparator = new ProductionResultComparator(true, ProductionResultComparatorEnum.INSPECTION_START_TIME);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public ProductionResultTableModel()
  {
    _productionResults = new ArrayList<>();
  }
  
  /**
   * Get the number of columns in this table.
   * We must override this function in order to allow this table model to function properly.
   * @author Ying-Huan.Chu
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }
  
  /**
   * Get the name of the column (column header).
   * We must override this function in order to allow this table model to function properly.
   * @author Ying-Huan.Chu
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0 && rowIndex < getRowCount());
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    
    ProductionResult productionResult = _productionResults.get(rowIndex);
    switch (columnIndex)
    {
      case _INSPECTION_START_TIME_COLUMN_INDEX:
        return productionResult.getInspectionStartTime();
      case _PROJECT_NAME_COLUMN_INDEX:
        return productionResult.getProjectName();
      case _SERIAL_NUMBER_COLUMN_INDEX:
        return productionResult.getSerialNumber();
      case _PANEL_PINS_PROCESSED_COLUMN_INDEX:
        return productionResult.getPanelPinsProcessed();
      case _PANEL_PINS_DEFECTIVE_COLUMN_INDEX:
        return productionResult.getPanelPinsDefective();
      case _PANEL_COMPONENTS_DEFECTIVE_COLUMN_INDEX:
        return productionResult.getPanelComponentsDefective();
      case _PANEL_LOADING_TIME_COLUMN_INDEX:
        return productionResult.getInspectionPanelLoadingTime();
      case _INSPECTION_TIME_COLUMN_INDEX:
        return productionResult.getInspectionTime();
      case _PANEL_UNLOADING_TIME_COLUMN_INDEX:
        return productionResult.getInspectionPanelUnloadingTime();
      case _INSPECTION_STATUS_COLUMN_INDEX:
        return productionResult.getInspectionStatus();
      case _INSPECTION_CALL_RATE_COLUMN_INDEX:
        return productionResult.getInspectionCallRate();
      default:
        return "";
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return false;
  }
  
  /**
   * Get the number of rows in this table.
   * We must override this function in order to allow this table model to function properly.
   * @author Ying-Huan.Chu
   */
  public int getRowCount()
  {
    if (_productionResults == null)
      return 0;
    else
      return _productionResults.size();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void sortColumn(int column, boolean ascending)
  {
    switch(column)
    {
      case _INSPECTION_START_TIME_COLUMN_INDEX :  
      {
        _productionResultComparator.setAscending(ascending);
        _productionResultComparator.setProductionResultComparatorEnum(ProductionResultComparatorEnum.INSPECTION_START_TIME);
        Collections.sort(_productionResults, _productionResultComparator);
        break;
      }
      case _PROJECT_NAME_COLUMN_INDEX :  
      {
        _productionResultComparator.setAscending(ascending);
        _productionResultComparator.setProductionResultComparatorEnum(ProductionResultComparatorEnum.PROJECT_NAME);
        Collections.sort(_productionResults, _productionResultComparator);
        break;
      }
      case _SERIAL_NUMBER_COLUMN_INDEX :  
      {
        _productionResultComparator.setAscending(ascending);
        _productionResultComparator.setProductionResultComparatorEnum(ProductionResultComparatorEnum.SERIAL_NUMBER);
        Collections.sort(_productionResults, _productionResultComparator);
        break;
      }
      case _PANEL_PINS_PROCESSED_COLUMN_INDEX :  
      {
        _productionResultComparator.setAscending(ascending);
        _productionResultComparator.setProductionResultComparatorEnum(ProductionResultComparatorEnum.PANEL_PINS_PROCESSED);
        Collections.sort(_productionResults, _productionResultComparator);
        break;
      }
      case _PANEL_PINS_DEFECTIVE_COLUMN_INDEX :  
      {
        _productionResultComparator.setAscending(ascending);
        _productionResultComparator.setProductionResultComparatorEnum(ProductionResultComparatorEnum.PANEL_PINS_DEFECTIVE);
        Collections.sort(_productionResults, _productionResultComparator);
        break;
      }
      case _PANEL_COMPONENTS_DEFECTIVE_COLUMN_INDEX : 
      {
        _productionResultComparator.setAscending(ascending);
        _productionResultComparator.setProductionResultComparatorEnum(ProductionResultComparatorEnum.PANEL_COMPONENTS_DEFECTIVE);
        Collections.sort(_productionResults, _productionResultComparator);
        break;
      }
      //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
      case _PANEL_LOADING_TIME_COLUMN_INDEX :  
      {
        _productionResultComparator.setAscending(ascending);
        _productionResultComparator.setProductionResultComparatorEnum(ProductionResultComparatorEnum.PANEL_LOADING_TIME);
        Collections.sort(_productionResults, _productionResultComparator);
        break;
      }
      case _INSPECTION_TIME_COLUMN_INDEX :  
      {
        _productionResultComparator.setAscending(ascending);
        _productionResultComparator.setProductionResultComparatorEnum(ProductionResultComparatorEnum.INSPECTION_TIME);
        Collections.sort(_productionResults, _productionResultComparator);
        break;
      }
      //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
      case _PANEL_UNLOADING_TIME_COLUMN_INDEX :  
      {
        _productionResultComparator.setAscending(ascending);
        _productionResultComparator.setProductionResultComparatorEnum(ProductionResultComparatorEnum.PANEL_UNLOADING_TIME);
        Collections.sort(_productionResults, _productionResultComparator);
        break;
      }
      case _INSPECTION_STATUS_COLUMN_INDEX : 
      {
        _productionResultComparator.setAscending(ascending);
        _productionResultComparator.setProductionResultComparatorEnum(ProductionResultComparatorEnum.INSPECTION_STATUS);
        Collections.sort(_productionResults, _productionResultComparator);
        break;
      }
      //Khaw Chek Hau - XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
      case _INSPECTION_CALL_RATE_COLUMN_INDEX : 
      {
        _productionResultComparator.setAscending(ascending);
        _productionResultComparator.setProductionResultComparatorEnum(ProductionResultComparatorEnum.INSPECTION_CALL_RATE);
        Collections.sort(_productionResults, _productionResultComparator);
        break;
      }
      default:
        Assert.expect(false, "column does not exist in table model");
        break;
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   * @param productionResult the ProductionResult object to be added into the Production Result Table.
   */
  public void addProductionResult(ProductionResult productionResult)
  {
    Assert.expect(productionResult != null);
    
    // we have to create a new ProductionResult object because the same ProductionResult object is reused in TestExecPanel.java.
    //Khaw Chek Hau - XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
    //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
    ProductionResult newProductionResult = new ProductionResult(productionResult.getProjectName(), 
                                                                productionResult.getSerialNumber(), 
                                                                productionResult.getPanelPinsProcessed(), 
                                                                productionResult.getPanelPinsDefective(), 
                                                                productionResult.getPanelComponentsDefective(),
                                                                productionResult.getInspectionStartTime(),
                                                                productionResult.getInspectionTime(), 
                                                                productionResult.getInspectionStatus(),
                                                                productionResult.getInspectionCallRate(),
                                                                productionResult.getInspectionPanelLoadingTime(),
                                                                productionResult.getInspectionPanelUnloadingTime());
    
    _productionResults.add(newProductionResult);
    fireTableDataChanged();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removeProductionResult(ProductionResult productionResult)
  {
    Assert.expect(productionResult != null);
    
    _productionResults.remove(productionResult);
    fireTableDataChanged();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removeRow(int index)
  {
    Assert.expect(index >= 0 && index < getRowCount());
    
    _productionResults.remove(index);
    fireTableDataChanged();
  }
  
  /**
   * Remove all Production Results.
   * @author Ying-Huan.Chu
   */
  public void removeAllProductionResult()
  {
    _productionResults.clear();
    fireTableDataChanged();
  }
  
  /**
   * Remove the oldest Production Result from this table.
   * @author Ying-Huan.Chu
   */
  public void removeOldestProductionResult()
  {
    if (_productionResults.size() > 1)
    {
      List<ProductionResult> productionResultSortedWithInspectionStartTime = new ArrayList<>(_productionResults);
      Collections.sort(productionResultSortedWithInspectionStartTime, new ProductionResultComparator(true, ProductionResultComparatorEnum.INSPECTION_START_TIME));
      _productionResults.remove(productionResultSortedWithInspectionStartTime.get(0));
      productionResultSortedWithInspectionStartTime.clear();
    }
    else
    {
      _productionResults.remove(0);
    }
    fireTableDataChanged();
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
   */
  public List<ProductionResult> getProductionResult()
  {
    return _productionResults;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
   */
  public int getPanelLoadingTimeColumnIndex()
  {
    return _PANEL_LOADING_TIME_COLUMN_INDEX;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
   */
  public int getPanelUnloadingTimeColumnIndex()
  {
    return _PANEL_UNLOADING_TIME_COLUMN_INDEX;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
   */
  public String[] getColumnIdentifiers()
  {
    return _columnLabels;
  }
}
