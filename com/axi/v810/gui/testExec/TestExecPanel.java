package com.axi.v810.gui.testExec;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.io.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.psp.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.shopFloorSystem.*;

/**
 * This is a standalone panel that will fit into any JFrame.  Currently used by the TestExecFrame
 * frame or the MainMenu frame.
 * @author Andy Mechtenberg
 */
public class TestExecPanel extends AbstractTaskPanel implements Observer
{
  private static final int _RUNTIME_TEXT_LIMIT = 250000;  // 250,000 chars
  private static final int _RUNTIME_TEXT_REMOVAL_AMOUNT = 10000;  // 10,000 chars to be removed at a chunk
  private static final int _NUMBER_OF_QUEUED_SERIAL_NUMBERS = 5; // the number we'll show without needed to scroll
  
  //Ying-Huan.Chu [XCR-2173] - Display Production Results in table form in Production page.
  private static final int _PRODUCTION_RESULT_TABLE_MAXIMUM_ROWS_TO_KEEP_SELECTION_1 = 10;
  private static final int _PRODUCTION_RESULT_TABLE_MAXIMUM_ROWS_TO_KEEP_SELECTION_2 = 50;
  private static final int _PRODUCTION_RESULT_TABLE_MAXIMUM_ROWS_TO_KEEP_SELECTION_3 = 100;
  private static final int _PRODUCTION_RESULT_TABLE_MAXIMUM_ROWS_TO_KEEP_SELECTION_4 = 200;
  //Khaw Chek Hau - XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
  private static final String _PRODUCTION_RESULT_TABLE_MAXIMUM_ROWS_TO_KEEP_SELECTION_5 = StringLocalizer.keyToString("PRT_NO_LIMIT_LABEL_KEY");

  private static final int _REALIGN_IMAGE = 1;
  private static final int _XEDOUT_BOARD  = 2;
  private static final int _UNLOAD_PANEL  = 3;

  private static final boolean _DEVELOPER_DEBUG = Config.isDeveloperDebugModeOn();
  private static final boolean _DEVELOPER_PERFORMANCE = Config.isDeveloperPerformanceModeOn();

  private TestExecEnvironment _testExecEnv;
  private TestExecPersistance _persistSettings;

  private boolean _productionTest = true;
  private boolean _performingStartup = false;
  private LightStack _lightStack;
  private XrayTester _xRayTester;
  private PanelHandler _panelHandler;
  private TestExecution _testExecution;
  private InspectionEngine _inspectionEngine;
  private TestExecInspectionObserver _testExecInspectionObserver;
  private ProgressObservable _progressObservable = ProgressObservable.getInstance();
  private HardwareObservable _hardwareObservable = HardwareObservable.getInstance();
  private DemoMode _demoMode;
  private boolean _testInProgress = false;
  private TestFlow _currentTestAction;
  private SerialNumberProcessor _serialNumberProcessor;
  private static Config _config = Config.getInstance();
  private SerialNumberToProjectName _serialNumberToProjectName;
  private ResultsProcessingErrorDialog _resultsProcessingErrorDialog = null;

  /** @todo MDW - remove later. This is to help with debugging the random spikes in false failures. */
  private static boolean _FLAG_CALL_RATE_SPIKES = _config.getBooleanValue(SoftwareConfigEnum.FLAG_CALL_RATE_SPIKES);
  private static double _MAX_ALLOWABLE_CALL_RATE_DEVIATION_FRACTION_FROM_BASELINE =
      _config.getDoubleValue(SoftwareConfigEnum.MAX_ALLOWABLE_CALL_RATE_DEVIATION_FRACTION_FROM_BASELINE);
  private CSVFileWriterAxi _callRateDataWriter;
  private static final int _UNINITIALIZED_VALUE = -1;
  private int _baselineNumberOfDefectiveJoints = _UNINITIALIZED_VALUE;
  private int _baselineNumberOfDefectiveComponents = _UNINITIALIZED_VALUE;

  // config values used for production mode only
  private boolean _displayLastImageInQueue = false;

  private XrayTesterException _xrayTesterException;

  private int _observerCount = 0;

  // auto review defects parameters
  private boolean _shouldRunReviewDefects = false;
  private int _autoReviewDefectsThreshold = 1;

  // progress bar data
  private int _currentlyAligningGroup = 0;
  private int _totalEventsForProgressBar = 0;
  private int _currentSurfaceMapPoints = 0;
  private int _totalSurfaceMapPoints = 0;
  private int _currentAlignmentSurfaceMapPoints = 0;
  private int _totalAlignmentSurfaceMapPoints = 0;

  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
  private SwingWorkerThread _swingWorkerThread2 = SwingWorkerThread.getInstance2();

  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();  // used for light stack calls

  static final String _TRY_AGAIN = "";
  private String _userEnteredSerialNumber;
  private String _currentSerialNumber = "";
  private String _nextSerialNumber = "";

  private String _currentActionMessage = "";

  private boolean _atLeastOneImageWasInspected = false;
  private boolean _lastTestPass = false;
  private boolean _doingViewInspection = false;
  private boolean _panelUnloadInProgress = false;

  private Project _project = null;
  private String _lastChosenProject;
  private String _currentVariation = VariationSettingManager.ALL_LOADED;

  // string messages
//  private String _currentUnits = "";// = StringLocalizer.keyToString("TESTEXEC_GUI_CURRENT_UNITS_KEY");
//  private String _voltageUnits = "";// = StringLocalizer.keyToString("TESTEXEC_GUI_VOLTAGE_UNITS_KEY");
  private String _WAITING_FOR_SERIAL_NUMBER_MESSAGE = "";// = StringLocalizer.keyToString("TESTEXEC_GUI_WAITING_FOR_SERIAL_NUMBER_KEY");
  private DecimalFormat _format = new DecimalFormat(".00");

  private boolean _displayedAlgorithmMessageInProductionMode = false;

  private MainMenuGui _mainUI;

  // These are the GUI control classes to build the UI
  private JPanel _statusPanel = new JPanel();
  private Border _statusCellBorder;
  private BorderLayout _testExecPanelBorderLayout = new BorderLayout();
  private JLabel _testExecHeaderLabel = new JLabel();
  private JPanel _mainContentPanel = new JPanel();
  private BorderLayout _mainContentBorderLayout = new BorderLayout();
  private JPanel _serialNumberImagePanel = new JPanel();
  private JPanel _testMessageAbortPanel = new JPanel();
  private JScrollPane _testMessageScrollPane = new JScrollPane();
  private BorderLayout _testMessageAbortBorderLayout = new BorderLayout();
  private JTextPane  _testMessageTextArea = new JTextPane(new DefaultStyledDocument());
  private JPanel _abortPanel = new JPanel();
  private JButton _abortButton = new JButton();
  private JLabel _axiImageLabel = new JLabel();
  private JLabel _currentActionLabel = new JLabel();
  private BorderLayout _serialNumberImageBorderLayout = new BorderLayout();
  private JPanel _programInfoPanel = new JPanel();
  private JPanel _queuedSerialNumberImagePanel = new JPanel();
  private JLabel _currentProgramVersionLabel = new JLabel();
  private JLabel _programVersionLabel = new JLabel();
  private JLabel _currentBoardLabel = new JLabel();
  private JLabel _boardLabel = new JLabel();
  private JLabel _currentPanelProgramLabel = new JLabel();
  private JLabel _panelProgramLabel = new JLabel();
  private JLabel _currentSerialNumberLabel = new JLabel();
  private JLabel _serialNumberLabel = new JLabel();
  private GridLayout _programInfoGridLayout = new GridLayout();
  private Border _lightGrayLineBorder;
  private BorderLayout _queuedSerialNumberImageBorderLayout = new BorderLayout();
  private JPanel _queuedSerialNumberPanel = new JPanel();
  private BorderLayout _queuedSerialNumberBorderLayout = new BorderLayout();
  private JLabel _nextSerialNumbersLabel = new JLabel();
  private JScrollPane _nextSerialNumberScrollPane = new JScrollPane();
  private JPanel _serialNumberClearPanel = new JPanel();
  private JButton _clearAllSerialNumberButton = new JButton();
  private FlowLayout _serialNumberClearFlowLayout = new FlowLayout();
//  private JTextArea _nextSerialNumberTextArea = new JTextArea();
  private DefaultListModel _nextSerialNumberListModel = new DefaultListModel();
  private JList _nextSerialNumberList = new JList(_nextSerialNumberListModel);
  private JPanel _actionLabelPanel = new JPanel();
  private FlowLayout _actionLabelFlowLayout = new FlowLayout();
  private JLabel _actionLabel = new JLabel();
  private BorderLayout _abortBorderLayout = new BorderLayout();
  private BorderLayout _statusPanelBorderLayout = new BorderLayout();
  private JPanel _statusCellPanel = new JPanel();
  private JLabel _typedSerialNumberLabel = new JLabel();
  private JPanel _statusFillerPanel = new JPanel();
  private FlowLayout _statusCellFlowLayout = new FlowLayout();
  private JProgressBar _currentActionProgressBar = new JProgressBar();
  private JButton _clearSerialNumberButton = new JButton();
  private JLabel _statusLabel = new JLabel(" ");
//  private XrayStatusPanel _xrayStatusPanel;
  private JSplitPane _mainSplitPane = new JSplitPane();
  private JPanel _futureSerialNumberPanel = new JPanel();
  private BorderLayout _futureSerialNumberPanelBorderLayout = new BorderLayout();
  private JTextField _futureSerialNumberTextField = new JTextField();
  private Border _futureSerialNumberPanelBorder;

  private InspectionEventObservable _inspectionObservable = InspectionEventObservable.getInstance();
  private ResultsProcessingObservable _resultsProcessingObservable = ResultsProcessingObservable.getInstance();
  private JPanel _panelLoadingImagePanel = new JPanel();
  private ImageDisplayPanel _panelImagePanel = new ImageDisplayPanel();
  private BorderLayout _panelLoadingImagePanelBorderLayout = new BorderLayout();
  private JLabel _eastUpArrowLabel = new JLabel(Image5DX.getImageIcon(Image5DX.UP_ARROW));
  private JLabel _westUpArrowLabel = new JLabel(Image5DX.getImageIcon(Image5DX.UP_ARROW));
  private JPanel _serialNumberClearInnerPanel = new JPanel();
  private GridLayout _serialNumberClearInnerPanelGridLayout = new GridLayout();
  private JLabel _imageExplanationLabel = new JLabel();
  
  //XCR1144
  //Chnee Khang Wah, 09-Dec-2010, Jabil board drop issue
  private ProgressDialog _shutdownProgressDialog;
  
  private FringePatternDialog _fringePatternDialog;
  private FringePatternObservable _fringePatternObservable = FringePatternObservable.getInstance();
 
  private UserTypeEnum _startAs;
  private FailFalseCallTriggeringLoginManager _failFalseCallTriggeringLoginManager;
  private String _userInputRemarkForFalseCallTriggeringLog;
  private CSVFileWriterAxi _falseCallTriggeringLogFile;
  
  //Ying-Huan.Chu [XCR-2173] - Display Production Results in table form in Production page.
  private JSortTable _productionResultTable = new JSortTable();
  private ProductionResultTableModel _productionResultTableModel = new ProductionResultTableModel();
  private ProductionResult _productionResult = new ProductionResult();
  private CollapsiblePanel _productionResultTableCollapsiblePanel = new CollapsiblePanel();
  private JPanel _productionResultPanel = new JPanel();
  private JButton _clearAllProductionResultButton = new JButton(StringLocalizer.keyToString("PRT_CLEAR_ALL_PRODUCTION_RESULTS_BUTTON_KEY"));
  private JButton _exportProductionResultButton = new JButton(StringLocalizer.keyToString("PRT_EXPORT_PRODUCTION_RESULTS_BUTTON_KEY"));
  private JLabel _maximumNumberOfRowToKeepLabel = new JLabel(StringLocalizer.keyToString("PRT_MAXIMUM_NUMBER_OF_ROW_TO_KEEP_LABEL_KEY"));
  private JComboBox _maximumNumberOfRowsToKeepComboBox;
  private static int _maximumNumberOfRowToKeep = _PRODUCTION_RESULT_TABLE_MAXIMUM_ROWS_TO_KEEP_SELECTION_1;
  private static boolean _isProductionResultTableVisible = true;
  
  private AbstractShopFloorSystem _shopFloorSystem = AbstractShopFloorSystem.getInstance();
  
  //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
  private MachineUtilizationReportSystem _machineUtilizationReportSystem = MachineUtilizationReportSystem.getInstance();

  //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
  private VOneMachineStatusMonitoringSystem _vOneMachineStatusMonitoringSystem = VOneMachineStatusMonitoringSystem.getInstance();
  
  //Khaw Chek Hau - XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
  private JLabel _averageOverallInspectionCallRateLable = new JLabel(StringLocalizer.keyToString("PRT_OVERALL_AVERAGE_INSPECTION_CALL_RATE_LABEL_KEY") + "   ");
  private JLabel _averageOverallInspectionCallRate = new JLabel("0");

  /**
   * The constructor called the application's Main -- it will create it's own server
   * @author Andy Mechtenberg
   */
  public TestExecPanel(TestExecEnvironment envPanel)
  {
    super(envPanel);
    Assert.expect(envPanel != null);
//    _envPanel = envPanel;
    _testExecEnv = envPanel;
    _mainUI = MainMenuGui.getInstance();
    
    jbInit();
    getInterfaces();
    _currentPanelProgramLabel.setText(null);
    _currentBoardLabel.setText(null);
    _currentProgramVersionLabel.setText(null);
  }

  /**
   * @author Andy Mechtenberg
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ResultsProcessingObservable)
        {
          if (_resultsProcessingErrorDialog.isVisible() == false)
          {
            SwingUtils.centerOnComponent(_resultsProcessingErrorDialog, _mainUI);
            _resultsProcessingErrorDialog.setVisible(true);
          }

          if (object instanceof java.util.List)
          {
            java.util.List<DatastoreException> listOfDatastoreExceptions = (java.util.List<DatastoreException>)object;

            _resultsProcessingErrorDialog.appendDatastoreExceptions(listOfDatastoreExceptions);
          }
          else if (object instanceof java.util.Map)
          {
            _resultsProcessingErrorDialog.setFailureList((java.util.Map<Pair<Target, InspectionInformation>, java.util.List < DatastoreException >> )object);
          }
        }
        else if (observable instanceof ProgressObservable)
        {
          if (object instanceof ProgressReport)
          {
            ProgressReport progressReport = (ProgressReport)object;
            int percentDone = progressReport.getPercentDone();
            // don't indicate 100% until event actually completes
            //             if (percentDone == 100)
            //             {
            //               percentDone = 99;
            //             }
            String reporterAction = progressReport.getAction();
            //             addToInformationPane(reporterAction);
            if (_performingStartup == false)
            {
              _currentActionLabel.setText(reporterAction);
              _currentActionProgressBar.setValue(percentDone);
              _currentActionProgressBar.setString(percentDone + " " + StringLocalizer.keyToString("GUI_PERCENT_COMPLETE_KEY"));
            }
			
			//XCR1144, Chnee Khang Wah, 09-Dec-2010, Jabil board drop issue
            if(_shutdownProgressDialog != null)
                _shutdownProgressDialog.updateProgressBarPrecent(percentDone, reporterAction);
            //               _autoAdjustProgressDialog.updateProgressBarPrecent(_lastPercentDone, reporterAction);
          }
        }
        else if (observable instanceof HardwareObservable)
        {
          if (object instanceof XrayTesterException)
          {
            XrayTesterException xte = (XrayTesterException)object;
            MessageDialog.showErrorDialog(_mainUI,
                                          xte,
                                          StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                          true,
                                          FontUtil.getMediumFont());
            _mainUI.changeLightStackToStandby();
          }
          else if (object instanceof HardwareEvent)
          {
            HardwareEvent event = (HardwareEvent)object;
            HardwareEventEnum eventEnum = event.getEventEnum();
            // Note: The MainMenuGui hardware observer is removed if the Service or Test Exec environment is active.
            // For hardware event debug output to be seen, the following call must be made here.
            if (_DEVELOPER_DEBUG || _DEVELOPER_PERFORMANCE)
            {
              HardwareEventDebug.decodeAndOutputHardwareEvent(event);
            }
            if (eventEnum instanceof HardwareTaskEventEnum)
            {
              HardwareTaskEventEnum calEventEnum = (HardwareTaskEventEnum)eventEnum;
              if (calEventEnum.equals(HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT))
              {
                if (event.isStart())
                {
                  // about to get progress observable events -- save the current action message so we can restore it later
                  _currentActionMessage = _currentActionLabel.getText();
                }
                else
                {
                  // end event -- revert message back to original
                  _currentActionLabel.setText(_currentActionMessage);
                  _currentActionProgressBar.setValue(0);
                  _currentActionProgressBar.setString(0 + " " + StringLocalizer.keyToString("GUI_PERCENT_COMPLETE_KEY"));
                }
              }
            }
			//XCR1144
			//Chnee Khang Wah, 09-Dec-2010, Jabil board drop issue
            else if (eventEnum instanceof XraySourceEventEnum)
            {
                XraySourceEventEnum  xraySourceEventEnum = (XraySourceEventEnum)eventEnum;

                 if(xraySourceEventEnum.equals(XraySourceEventEnum.POWER_OFF))
                  {
                      if (event.isStart())
                      {
                          _shutdownProgressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                          StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
                          StringLocalizer.keyToString("HW_XRAY_SOURCE_POWER_OFF_PROGRESS_REPORTER_KEY"),
                          StringLocalizer.keyToString("HW_XRAY_SOURCE_POWER_OFF_PROGRESS_REPORTER_KEY"),
                          StringLocalizer.keyToString("MMGUI_START_UP_CLOSE_MESSAGE_KEY"),
                          StringLocalizer.keyToString("GUI_PERCENT_COMPLETE_KEY"),
                          0, 100, true);

                          _shutdownProgressDialog.pack();
                          SwingUtils.centerOnComponent(_shutdownProgressDialog, MainMenuGui.getInstance());
                          _shutdownProgressDialog.setVisible(true);
                      }
                      else
                      {
                          if(_shutdownProgressDialog != null)
                          {
                            _shutdownProgressDialog.dispose();
                            _shutdownProgressDialog = null;
                          }
                      }
                  }
            }
            //XCR1144 - end
          }
        }
        else if (observable instanceof FringePatternObservable)
        {
            if (object instanceof FringePatternInfo)
            {
                FringePatternInfo fringePatternInfo = (FringePatternInfo)object;
                final OpticalCameraIdEnum cameraId = fringePatternInfo.getCameraId();
                FringePatternEnum fringePatternEnum = fringePatternInfo.getFringePatternEnum();
                
                if (fringePatternEnum.equals(FringePatternEnum.INITIALIZE))
                {
                    if (_fringePatternDialog != null)
                    {
                        _fringePatternDialog.dispose();
                        _fringePatternDialog = null;
                    }
                    
                    // Get FringePattern instance
                    _fringePatternDialog = new FringePatternDialog(_mainUI, "Fringe Pattern", cameraId);
                }
                else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_WHITE_LIGHT))
                {
                    if (SwingUtilities.isEventDispatchThread())
                      _fringePatternDialog.displayWhiteLight(cameraId);
                    else
                    {
                      SwingUtils.invokeLater(new Runnable()
                      {
                        public void run()
                        {
                          _fringePatternDialog.displayWhiteLight(cameraId);
                        }
                      });
                    }
                }
                else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_NO_LIGHT))
                {
                    if (SwingUtilities.isEventDispatchThread())
                      _fringePatternDialog.closeFringePattern();
                    else
                    {
                      SwingUtils.invokeLater(new Runnable()
                      {
                        public void run()
                        {
                          _fringePatternDialog.closeFringePattern();
                        }
                      });
                    }
                }
                else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_ONE))
                {
                    if (SwingUtilities.isEventDispatchThread())
                      _fringePatternDialog.displayFringePatternShiftOne(cameraId);
                    else
                    {
                      SwingUtils.invokeLater(new Runnable()
                      {
                        public void run()
                        {
                          _fringePatternDialog.displayFringePatternShiftOne(cameraId);
                        }
                      });
                    }
                }
                else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_TWO))
                {
                    if (SwingUtilities.isEventDispatchThread())
                      _fringePatternDialog.displayFringePatternShiftTwo(cameraId);
                    else
                    {
                      SwingUtils.invokeLater(new Runnable()
                      {
                        public void run()
                        {
                          _fringePatternDialog.displayFringePatternShiftTwo(cameraId);
                        }
                      });
                    }
                }
                else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_THREE))
                {
                    if (SwingUtilities.isEventDispatchThread())
                      _fringePatternDialog.displayFringePatternShiftThree(cameraId);
                    else
                    {
                      SwingUtils.invokeLater(new Runnable()
                      {
                        public void run()
                        {
                          _fringePatternDialog.displayFringePatternShiftThree(cameraId);
                        }
                      });
                    }
                }
                else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_FOUR))
                {
                    if (SwingUtilities.isEventDispatchThread())
                      _fringePatternDialog.displayFringePatternShiftFour(cameraId);
                    else
                    {
                      SwingUtils.invokeLater(new Runnable()
                      {
                        public void run()
                        {
                          _fringePatternDialog.displayFringePatternShiftFour(cameraId);
                        }
                      });
                    }
                }
                else if (fringePatternEnum.equals(FringePatternEnum.FINISH_DISPLAY_FRINGE_PATTERN))
                {
                  try
                  {
                    PspImageAcquisitionEngine.getInstance().handlePostImageDisplay(PspModuleEnum.FRONT);
                  }
                  catch (XrayTesterException ex)
                  {
                    _mainUI.closeAllDialogs();
                    MessageDialog.showErrorDialog(_mainUI,
                                                  ex.getMessage(),
                                                  StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
                                                  true,
                                                  FontUtil.getMediumFont());
                    _mainUI.changeLightStackToStandby();
                    exitTestExecution();
                    return;
                  }
                }
                else
                    Assert.expect(false, "invalid display mode");
            }
        }
        //XCR- 3436
        else if (observable instanceof ConfigObservable)
        {
          if (object instanceof ConfigEnum)
          {
            ConfigEnum configEnum = (ConfigEnum)object;
            if (configEnum instanceof UISettingCustomizationConfigEnum)
            {
              UISettingCustomizationConfigEnum customiseSettingConfigEnum = (UISettingCustomizationConfigEnum)configEnum;
              if (customiseSettingConfigEnum.equals(UISettingCustomizationConfigEnum.CUSTOMIZE_MAIN_UI) ||
                  customiseSettingConfigEnum.equals(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_VERSION))
              {
                updateProgramInfoPanel();
              }
            }
          }
        }
      }
    });
  }

  /**
   * Task is ready to start
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("TestExecPanel().start()");
    
    _persistSettings = _testExecEnv.getPersistance();
    int dividerLocation = _persistSettings.getSplitPanePosition();
    if (dividerLocation > 0)
      _mainSplitPane.setDividerLocation(dividerLocation);

    // clear all panel inspection settings
    PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
    panelInspectionSettings.clearAllInspectionSettings();
    _inspectionEngine.setPanelInspectionSettings(panelInspectionSettings);
    
    //Khaw Chek Hau - XCR3774: Display load & unload time during production
    int columnWidth = 0;
    int maxWidth = 0;
    int minWidth = 0;
    
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.TEST_EXEC_DISPLAY_PRODUCTION_PANEL_LOADING_AND_UNLOADING_TIME))
    {
      columnWidth = _productionResultTable.getWidth() / _productionResultTable.getColumnCount();
      maxWidth = Integer.MAX_VALUE;
      minWidth = 15;
    }
    
    _productionResultTable.getColumnModel().getColumn(_productionResultTableModel.getPanelLoadingTimeColumnIndex()).setMinWidth(minWidth);
    _productionResultTable.getColumnModel().getColumn(_productionResultTableModel.getPanelLoadingTimeColumnIndex()).setMaxWidth(maxWidth);
    _productionResultTable.getColumnModel().getColumn(_productionResultTableModel.getPanelLoadingTimeColumnIndex()).setPreferredWidth(columnWidth);
    _productionResultTable.getColumnModel().getColumn(_productionResultTableModel.getPanelLoadingTimeColumnIndex()).setWidth(columnWidth);
    
    _productionResultTable.getColumnModel().getColumn(_productionResultTableModel.getPanelUnloadingTimeColumnIndex()).setMinWidth(minWidth);
    _productionResultTable.getColumnModel().getColumn(_productionResultTableModel.getPanelUnloadingTimeColumnIndex()).setMaxWidth(maxWidth);
    _productionResultTable.getColumnModel().getColumn(_productionResultTableModel.getPanelUnloadingTimeColumnIndex()).setPreferredWidth(columnWidth);
    _productionResultTable.getColumnModel().getColumn(_productionResultTableModel.getPanelUnloadingTimeColumnIndex()).setWidth(columnWidth);

    if (_resultsProcessingErrorDialog == null)
      _resultsProcessingErrorDialog = new ResultsProcessingErrorDialog(_mainUI, StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"), FontUtil.getMediumFont());
    SwingUtils.centerOnComponent(_resultsProcessingErrorDialog, _mainUI);

    // setup the inspection observer
    if (_testExecInspectionObserver == null)
      _testExecInspectionObserver = new TestExecInspectionObserver(this);
    _inspectionObservable.addObserver(_testExecInspectionObserver);
    
    _resultsProcessingObservable.addObserver(this);
    _observerCount++;
    Assert.expect(_observerCount == 1);

    JRootPane rootPane = _mainUI.getRootPane();
    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    rootPane.registerKeyboardAction(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        handleEscKey();
      }
    }, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
    _abortButton.setEnabled(true);
    _futureSerialNumberTextField.setEnabled(true); //Siew Yeng - XCR-2034

    // putting this on the swing thread, but later so this method can return and the environment is able
    // to complete it's switch
    // If we add a start button, then this stuff would be in it's action listener
    // NOT using SwingUtils on purpose to allow the threading to work
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        _mainUI.disableMenuAndToolBars();
        clearAllSerialNumberButton_actionPerformed();
        _testMessageTextArea.setText(null);
        _currentSerialNumberLabel.setText(null);
        _currentPanelProgramLabel.setText(null);
        _currentBoardLabel.setText(null);
        _currentProgramVersionLabel.setText(null);
        _panelImagePanel.clearImage();

        _progressObservable.addObserver(TestExecPanel.this);
        _hardwareObservable.addObserver(TestExecPanel.this);
        _fringePatternObservable.addObserver(TestExecPanel.this);
        boolean productionTest = (_testExecution.isBypassModeEnabled() == false) || _testExecution.isBypassWithSerialControlModeEnabled(); // false means bypass mode (the only other mode supported)
        DemoMode demoMode = new DemoMode();
        try
        {
          _testExecution.clearExitAndAbortFlags();
          _performingStartup = true;
          _mainUI.performStartupIfNeeded(FontUtil.getMediumFont());
          _performingStartup = false;
          if (XrayTester.getInstance().isStartupRequired())
          {
            exitTestExecution();
            return;
          }
        }
        catch (PanelInterferesWithAdjustmentAndConfirmationTaskException panelInTheWayException)
        {
          _performingStartup = false;
          // well, we can't complete startup with a panel in the machine, so we'd better give the operator
          // the opportunity to unload the panel and have startup complete without a panel.
          // ask the user if they want to use blank or eject the panel
          Object[] options =
                             {StringLocalizer.keyToString("TESTEXEC_GUI_UNLOAD_PANEL_KEY"),
                             StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")};


          int choice = ChoiceInputDialog.showOptionDialog(_mainUI,
              StringUtil.format(StringLocalizer.keyToString("MMGUI_GRAYSCALE_NEEDS_PANEL_REMOVED_KEY"), 50),
              StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
              options,
              options[0],
              FontUtil.getMediumFont());
          _mainUI.changeLightStackToStandby();
          if ((choice == JOptionPane.NO_OPTION) || (choice == JOptionPane.CLOSED_OPTION))
          {
            exitTestExecution();
            return;
          }
          else if (choice == JOptionPane.YES_OPTION) // first option, which is to unload the panel
          {
            unloadPanelFromMachine(TestResultsEnum.NOT_TESTED);
            try
            {
              _mainUI.performStartupIfNeeded(FontUtil.getMediumFont());
            }
            catch (XrayTesterException ex)
            {
              _mainUI.closeAllDialogs();
              MessageDialog.showErrorDialog(_mainUI,
                                            ex.getMessage(),
                                            StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
                                            true,
                                            FontUtil.getMediumFont());
              _mainUI.changeLightStackToStandby();
              exitTestExecution();
              return;
            }
            // unload panel and now get new serial numbers
//            startProductionTest();
//            return;
          }

        }
        catch (XrayTesterException xte)
        {
          _performingStartup = false;
          _mainUI.closeAllDialogs();

          // XCR1144, Chnee Khang Wah, 15-Feb-2011
          if(xte instanceof PanelPositionerException)
              _mainUI.handleXrayTesterException(xte);
          else
          MessageDialog.showErrorDialog(_mainUI,
                                        xte.getMessage(),
                                        StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
                                        true,
                                        FontUtil.getMediumFont());

          _mainUI.changeLightStackToStandby();

          exitTestExecution();
          return;
        }

        setCurrentMode(productionTest, demoMode);
        startCurrentMode();
      }
    });

  }

  /**
   * Can the environment be swapped out
   * @return true if environment can be swapped
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
//    System.out.println("TestExecPanel().isReadyToFinish()");
    if (_testInProgress)
      return false;
    else
      return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void getInterfaces()
  {
    _inspectionEngine = InspectionEngine.getInstance();
    _testExecution = TestExecution.getInstance();
    _lightStack = LightStack.getInstance();
    _xRayTester = XrayTester.getInstance();
    _panelHandler = PanelHandler.getInstance();
  }

  /**
   * @author Andy Mechtenberg
   */
  void loadTestProgramInformation(Project project)
  {
    // a load is done right before a test is being run
    // only show image if we show the current one or list is empty
    boolean showImage = true;
    if (_displayLastImageInQueue && (_nextSerialNumberListModel.isEmpty() == false))
      showImage = false;
    updateScreenData(project, _currentSerialNumber, showImage);
  }

  /**
   * This method should be called to display all error message that might occur.
   * @author Andy Mechtenberg
   */
  void displayError(XrayTesterException xte)
  {
    // Give MainMenuGui a change to close the Auto Adjust progress dialog if it is still open.
    // The exception may prevent the normal hardware finish event from occurring.
    
    _mainUI.handleXrayTesterException(xte);
    _mainUI.changeLightStackToStandby();

    if (_testInProgress)
      addToInformationPane(StringLocalizer.keyToString("TESTEXEC_GUI_TEST_ABORTING_KEY") + "\n");
  }

  /**
   * @author Andy Mechtenberg
   */
  public void handleKeyStroke(KeyEvent keyEvent)
  {
    int retCode = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0).getKeyCode();

    if (_productionTest)
    {
      if (keyEvent.getKeyCode() == retCode)
      {
        // hitting return basically means we've finished queueing up the serial number and to add it to the list
        _nextSerialNumber = _futureSerialNumberTextField.getText();
        if (_nextSerialNumber.length() > 0)
        {
          try
          {
//            serialNumbersForQueue = _productionModeSerialNumberProcessor.processKeyboardSerialNumber(_nextSerialNumber);
            _serialNumberProcessor.setFutureSerialNumberInput(true);
            PanelSerialNumberData panelSerialNumberData = _serialNumberProcessor.processKeyboardSerialNumber(_nextSerialNumber, false);
            if (panelSerialNumberData.isValid())
              addSerialNumbersToQueue(panelSerialNumberData);
          }
          catch (XrayTesterException ex)
          {
            displayError(ex);
            return;
          }
          finally
          {
            _serialNumberProcessor.setFutureSerialNumberInput(false);
          }
//          _nextSerialNumberListModel.addElement(_nextSerialNumber);
//          _nextSerialNumberList.ensureIndexIsVisible(_nextSerialNumberListModel.size() - 1);
          _nextSerialNumber = "";
          _futureSerialNumberTextField.setText(null);
        }
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleEscKey()
  {
//    System.out.println("ESC KEY");
    // method is called when the ESC key has been hit
    if (_productionTest)
    {
      // if there is a serial number queueing up, then the first ESC will clear that out.
      // Otherwise, ESC means we want to abort the test
      _nextSerialNumber = _futureSerialNumberTextField.getText();
      if (_nextSerialNumber.length() > 0)
      {
        _nextSerialNumber = "";
        _futureSerialNumberTextField.setText(_nextSerialNumber);
      }
      else
      {
        // the keystroke handler for non-production mode is different.  We don't deal with queued serial numbers, so all we
        // want to pay attention do is the ESCAPE for aborting the test in progress
        if (_testInProgress)
          abortCurrentTestAction();
        else
        {
          /** changes to force ESC to exit production mode */
          JRootPane rootPane = _mainUI.getRootPane();
          KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
          rootPane.unregisterKeyboardAction(stroke);
          _mainUI.getMainToolBar().switchToAXIHome();
        }
        //          else if (confirmAction(StringLocalizer.keyToString("TESTEXEC_GUI_CONFIRM_EXIT_KEY"), true))
        //            exitTestExecution();
      }

    }
    else
    {
      if (_testInProgress)
        abortCurrentTestAction();
      else
        _mainUI.getMainToolBar().switchToAXIHome();
//          exitTestExecution();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void addSerialNumbersToQueue(PanelSerialNumberData panelSerialNumberData)
  {
    Assert.expect(panelSerialNumberData != null);

    // now add the serial numbers indented
    Iterator it = panelSerialNumberData.getSerialNumbers().iterator();
    int totalNumber = panelSerialNumberData.getSerialNumbers().size() + 2;  // one for the Panel Name as an item and one for the Variation Name as an item
    String projectName = panelSerialNumberData.getProjectName();
    String variationName = panelSerialNumberData.getVariationName();
    SerialNumberQueueListItem panelNameListItem = new SerialNumberQueueListItem(projectName, totalNumber);
    _nextSerialNumberListModel.addElement(panelNameListItem);
    SerialNumberQueueListItem variationNameListItem = new SerialNumberQueueListItem(projectName, variationName, 1, totalNumber);
    _nextSerialNumberListModel.addElement(variationNameListItem);
    int serialNumberCount = 2;
    while (it.hasNext())
    {
      String serialNumber = (String)it.next();
      SerialNumberQueueListItem serialNumberListItem = new SerialNumberQueueListItem(serialNumber, variationName, projectName, serialNumberCount++, totalNumber);
      _nextSerialNumberListModel.addElement(serialNumberListItem);
    }
    _nextSerialNumberList.ensureIndexIsVisible(_nextSerialNumberListModel.size() - 1);

    // update the panel image displayed if the option is set to be the image of the last item on the queue
    if (_displayLastImageInQueue)
    {
      _imageExplanationLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_DISPLAY_LAST_IMAGE_ON_QUEUE_KEY"));
      displayPanelImage(projectName);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  void resetForProductionTest()
  {
    _abortButton.setEnabled(true);
    _futureSerialNumberTextField.setEnabled(true); //Siew Yeng - XCR-2034
    _currentActionProgressBar.setValue(0);
    _currentActionProgressBar.setString("0" + " " + StringLocalizer.keyToString("GUI_PERCENT_COMPLETE_KEY"));
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean abortCurrentTestAction()
  {
    boolean abortEnabled = _abortButton.isEnabled();
    if ((abortEnabled) && (confirmAction(StringLocalizer.keyToString("TESTEXEC_GUI_CONFIRM_ABORT_KEY"), false)))
    {
      // don't allow the user to abort twice
      _abortButton.setEnabled(false);
      _futureSerialNumberTextField.setEnabled(false); //Siew Yeng - XCR-2034
      addToInformationPane(StringLocalizer.keyToString("TESTEXEC_GUI_TEST_ABORTING_KEY") + "\n");
      _currentTestAction.abort();
      return true;
    }
    return false;
  }

  /**
   * This pops up a modal confirm dialog to ensure the user wants to leave.
   * @return true if they really want to leave, false otherwise
   * @author Andy Mechtenberg
   */
  boolean confirmAction(String confirmMessage, boolean changeLight)
  {
    Assert.expect(confirmMessage != null);

    if (changeLight)
      _mainUI.changeLightStackToStandby();
    int choice = ChoiceInputDialog.showConfirmDialog(_mainUI,
                                                     confirmMessage,
                                                     StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                                     FontUtil.getMediumFont());
    if (changeLight)
      changeLightStackToInspectionInProgess();
    if (choice == JOptionPane.YES_OPTION)
      return true;
    else
      return false;
  }

  /**
   * Called after frame has been constructed.  This sets up some user interface customizations all in one place
   *
   * @author Andy Mechtenberg
   */
  public void setGuiCustomizations(int splitPanePosition)
  {
    // other specific customizations
    _mainSplitPane.setDividerLocation(splitPanePosition);
    _mainUI.repaint(); // neccessary to make all the text visible in the controls.  Mainly for the simulation indicator.
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    _futureSerialNumberPanelBorder = BorderFactory.createEmptyBorder(5, 0, 1, 5);
    Assert.expect(SwingUtilities.isEventDispatchThread());
    _statusCellBorder = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(99, 99, 99),new Color(142, 142, 142)),BorderFactory.createEmptyBorder(0,2,0,2));
    _lightGrayLineBorder = BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.lightGray,1),BorderFactory.createEmptyBorder(0,2,0,0));
    this.setBorder(null);
//    setPreferredSize(new Dimension(_defaultWidth, _defaultHeight));
    setLayout(_testExecPanelBorderLayout);
    _statusPanel.setLayout(_statusPanelBorderLayout);

    _testExecHeaderLabel.setText(" ");
    _testExecHeaderLabel.setBackground(SystemColor.info);

    _testExecHeaderLabel.setOpaque(true);
    _testExecHeaderLabel.setToolTipText("");
    _testExecHeaderLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _mainContentPanel.setLayout(_mainContentBorderLayout);
    _testMessageAbortPanel.setLayout(_testMessageAbortBorderLayout);
    _testMessageTextArea.setEditable(false);
    _testMessageTextArea.setFocusable(false);
    // this is the only component that gets focus
    // so we'll handle all keystrokes from here
    _futureSerialNumberTextField.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyPressed(KeyEvent e)
      {
        handleKeyStroke(e);
      }
    });

    _testMessageTextArea.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyPressed(KeyEvent e)
      {
        handleKeyStroke(e);
      }
    });

//    _testMessageTextArea.setText("** Message from algorithm PCAP_SPC in View: 120, Joint: 22 \n Slug center " +
//    "located incorrectly.  Check slug prfofile size (TANT_SLUG_LEN)!  " +
//    "\n\nGenerating results...\nBoard Test Done Pins Processed:  4730  Pins " +
//    "Defective: 2386\nTiming Statistics for test of FAMILIES_ALL_RLV board " +
//    "# 1\nAlign - 00:00:03.6  Map - 00:00:01.1  Test Views - 00:0033.8" +
//    "\n**********  FAIL  **********");
    _abortPanel.setLayout(_abortBorderLayout);

    _abortButton.setFocusable(false);
    _abortButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        abortButton_actionPerformed(e);
      }
    });
    _abortButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    _abortButton.setHorizontalTextPosition(SwingConstants.CENTER);


    _currentActionLabel.setBackground(SystemColor.info);
    _currentActionLabel.setBorder(null);
    _currentActionLabel.setOpaque(true);
    _currentActionLabel.setText(" ");//Testing panel FAMILIES_ALL_RLV Serial No. 1234567");

    _testMessageAbortBorderLayout.setVgap(5);
    _testMessageAbortPanel.setBorder(BorderFactory.createEmptyBorder(0,5,5,0));
    _serialNumberImagePanel.setLayout(_serialNumberImageBorderLayout);
    _currentProgramVersionLabel.setBackground(Color.white);
    _currentProgramVersionLabel.setBorder(_lightGrayLineBorder);
    _currentProgramVersionLabel.setOpaque(true);
    _programVersionLabel.setBorder(_lightGrayLineBorder);
    _programVersionLabel.setOpaque(true);

    _currentBoardLabel.setBackground(Color.white);
    _currentBoardLabel.setBorder(_lightGrayLineBorder);
    _currentBoardLabel.setOpaque(true);
    _boardLabel.setBorder(_lightGrayLineBorder);
    _boardLabel.setOpaque(true);

    _currentPanelProgramLabel.setBackground(Color.white);
    _currentPanelProgramLabel.setBorder(_lightGrayLineBorder);
    _currentPanelProgramLabel.setOpaque(true);
    _currentPanelProgramLabel.setRequestFocusEnabled(false);
    _panelProgramLabel.setBorder(_lightGrayLineBorder);
    _panelProgramLabel.setOpaque(true);

    _currentSerialNumberLabel.setBackground(Color.white);
    _currentSerialNumberLabel.setBorder(_lightGrayLineBorder);
    _currentSerialNumberLabel.setOpaque(true);
    _serialNumberLabel.setBorder(_lightGrayLineBorder);
    _serialNumberLabel.setOpaque(true);


    // I had to add this in when moving to the XP look and feel.  The labels are too squished together
    // and this separates them by 2 extra pixels.  Normal label spacing is font height plus 3, so I put
    // in 5 and it looks pretty good.  Note, this doesn't work as well with 1.4.1, or the pre-XP look and feel
    FontMetrics fm = getFontMetrics(getFont());
    int prefHeight = fm.getHeight() + 5;
    int prefWidth = fm.stringWidth(_serialNumberLabel.getText()) + 5;
    _serialNumberLabel.setPreferredSize(new Dimension(prefWidth, prefHeight));
    _serialNumberImageBorderLayout.setHgap(0);
    _serialNumberImageBorderLayout.setVgap(10);
    _mainContentBorderLayout.setHgap(10);
    _queuedSerialNumberImagePanel.setLayout(_queuedSerialNumberImageBorderLayout);
    _queuedSerialNumberPanel.setLayout(_queuedSerialNumberBorderLayout);

    _clearAllSerialNumberButton.setFocusable(false);

    _clearAllSerialNumberButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        clearAllSerialNumberButton_actionPerformed();
      }
    });

    _clearSerialNumberButton.setFocusable(false);

    _clearSerialNumberButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        clearSerialNumberButton_actionPerformed();
      }
    });
    _serialNumberClearPanel.setLayout(_serialNumberClearFlowLayout);
    _serialNumberClearFlowLayout.setAlignment(FlowLayout.CENTER);
    _nextSerialNumberList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
    _nextSerialNumberList.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent lse)
      {
        serialNumberListSelectionChanged(lse);
      }
    });
//    _nextSerialNumberList.setVisibleRowCount(5);
    _nextSerialNumberList.setFocusable(false);
    _actionLabelPanel.setLayout(_actionLabelFlowLayout);
    _actionLabelFlowLayout.setAlignment(FlowLayout.RIGHT);

    _mainContentPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
    _statusPanel.setBorder(null);
    _typedSerialNumberLabel.setBorder(_statusCellBorder);

    _statusCellPanel.setLayout(_statusCellFlowLayout);
    _statusCellFlowLayout.setAlignment(FlowLayout.LEFT);
    _statusCellFlowLayout.setHgap(0);
    _statusCellFlowLayout.setVgap(0);
    _statusFillerPanel.setBorder(BorderFactory.createLoweredBevelBorder());
    _currentActionProgressBar.setStringPainted(true);
    _currentActionProgressBar.setMinimum(0);
    _currentActionProgressBar.setMaximum(100);

    _futureSerialNumberPanel.setLayout(_futureSerialNumberPanelBorderLayout);

    _futureSerialNumberTextField.setText("");
    _futureSerialNumberTextField.setEditable(true);
    _futureSerialNumberTextField.setFocusable(true);
    _futureSerialNumberTextField.requestFocus();

    Font largeFont = FontUtil.getExtraLargeFont(Font.BOLD);
    boolean useLargeFont = true;

    // font sizing
    Font displayFont;
    Font displayFontBold;
    if (useLargeFont)
    {
      displayFont = FontUtil.getMediumFont();
      displayFontBold = FontUtil.getMediumFont(Font.BOLD);
    }
    else
    {
      displayFont = FontUtil.getSmallFont();
      displayFontBold = FontUtil.getSmallFont(Font.BOLD);
    }

    _futureSerialNumberTextField.setFont(largeFont);
    _futureSerialNumberPanelBorderLayout.setVgap(5);
    _futureSerialNumberPanel.setBorder(_futureSerialNumberPanelBorder);
    _panelImagePanel.setBorder(BorderFactory.createLoweredBevelBorder());
    _panelLoadingImagePanel.setLayout(_panelLoadingImagePanelBorderLayout);
    _serialNumberClearInnerPanel.setLayout(_serialNumberClearInnerPanelGridLayout);
    _serialNumberClearInnerPanelGridLayout.setHgap(5);
    _imageExplanationLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _imageExplanationLabel.setHorizontalTextPosition(SwingConstants.CENTER);

    this.add(_statusPanel, BorderLayout.SOUTH);
    _statusFillerPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
    _statusFillerPanel.add(_statusLabel);
    _statusPanel.add(_statusCellPanel,  BorderLayout.WEST);

//    _statusCellPanel.add(_typedSerialNumberLabel, null);
    _statusPanel.add(_statusFillerPanel,  BorderLayout.CENTER);
//    _xrayStatusPanel = new XrayStatusPanel(displayFont);
//    _xrayStatusPanel.setBorder(_statusCellBorder);
//    _statusPanel.add(_xrayStatusPanel,  BorderLayout.EAST);
    this.add(_testExecHeaderLabel, BorderLayout.NORTH);
    this.add(_mainContentPanel, BorderLayout.CENTER);
//    _mainContentPanel.add(_serialNumberImagePanel, BorderLayout.WEST);
//    _serialNumberImagePanel.add(_programInfoPanel, BorderLayout.NORTH);
    updateProgramInfoPanel();
    _serialNumberImagePanel.add(_queuedSerialNumberImagePanel,  BorderLayout.CENTER);
    _queuedSerialNumberPanel.add(_nextSerialNumbersLabel,  BorderLayout.NORTH);
    _queuedSerialNumberPanel.add(_nextSerialNumberScrollPane,  BorderLayout.CENTER);
    _nextSerialNumberScrollPane.getViewport().add(_nextSerialNumberList, null);
    _queuedSerialNumberPanel.add(_serialNumberClearPanel, BorderLayout.SOUTH);
    _serialNumberClearPanel.add(_serialNumberClearInnerPanel);
    _serialNumberClearInnerPanel.add(_clearSerialNumberButton);
    _serialNumberClearInnerPanel.add(_clearAllSerialNumberButton);
    _queuedSerialNumberImagePanel.add(_actionLabelPanel,  BorderLayout.SOUTH);
    _actionLabelPanel.add(_actionLabel, null); //    _mainContentPanel.add(_testMessageAbortPanel, BorderLayout.CENTER);
    _mainSplitPane.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
    _mainSplitPane.add(_serialNumberImagePanel, JSplitPane.TOP);
    _mainSplitPane.add(_testMessageAbortPanel, JSplitPane.BOTTOM);

    _testMessageAbortPanel.add(_testMessageScrollPane,  BorderLayout.CENTER);
    _testMessageAbortPanel.add(_abortPanel, BorderLayout.NORTH);
    _testMessageScrollPane.getViewport().add(_testMessageTextArea, null);
    // Ying-Huan.Chu (XCR-2173)
    _clearAllProductionResultButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        clearAllProductionResultButton_actionPerformed();
      }
    });
    _exportProductionResultButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exportProductionResultButton_actionPerformed();
      }
    });

    //Khaw Chek Hau - XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table    
    JPanel productionResultCombinedPanel = new JPanel(new GridLayout(2,1));
    
    JPanel productionResultButtonPanel = new JPanel(new GridLayout(1,2));
    productionResultButtonPanel.add(_maximumNumberOfRowToKeepLabel);
    _maximumNumberOfRowsToKeepComboBox = new JComboBox(new Object[]{_PRODUCTION_RESULT_TABLE_MAXIMUM_ROWS_TO_KEEP_SELECTION_1, 
                                                                    _PRODUCTION_RESULT_TABLE_MAXIMUM_ROWS_TO_KEEP_SELECTION_2,
                                                                    _PRODUCTION_RESULT_TABLE_MAXIMUM_ROWS_TO_KEEP_SELECTION_3,
                                                                    _PRODUCTION_RESULT_TABLE_MAXIMUM_ROWS_TO_KEEP_SELECTION_4,
                                                                    _PRODUCTION_RESULT_TABLE_MAXIMUM_ROWS_TO_KEEP_SELECTION_5});
    _maximumNumberOfRowsToKeepComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        maximumNumberOfRowsToKeepComboBox_actionPerformed();
      }
    });
    productionResultButtonPanel.add(_maximumNumberOfRowsToKeepComboBox);
    productionResultButtonPanel.add(_clearAllProductionResultButton);
    productionResultButtonPanel.add(_exportProductionResultButton);
    
    //Khaw Chek Hau - XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
    JPanel productionResultInspectionCallRatePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    productionResultInspectionCallRatePanel.add(_averageOverallInspectionCallRateLable);
    productionResultInspectionCallRatePanel.add(_averageOverallInspectionCallRate);
    productionResultCombinedPanel.add(productionResultInspectionCallRatePanel);
    productionResultCombinedPanel.add(productionResultButtonPanel);
    
    _productionResultTable.setEnabled(false);
    _productionResultTable.setModel(_productionResultTableModel);
    JScrollPane productionResultTableScrollPane = new JScrollPane();
    productionResultTableScrollPane.getViewport().add(_productionResultTable, null);
    _productionResultTableCollapsiblePanel.setTitle(StringLocalizer.keyToString("PRT_TABLE_TITLE_KEY"));
    _productionResultTableCollapsiblePanel.add(_productionResultTable.getTableHeader(), BorderLayout.NORTH);
    _productionResultTableCollapsiblePanel.add(productionResultTableScrollPane, BorderLayout.CENTER);
    _productionResultTableCollapsiblePanel.add(productionResultCombinedPanel, BorderLayout.SOUTH);
    _productionResultTableCollapsiblePanel.setComponentVisible(false);
    
    _abortPanel.add(_productionResultPanel, BorderLayout.SOUTH);
    _abortPanel.add(_abortButton,  BorderLayout.EAST);
    
    _productionResultPanel.setLayout(new BorderLayout());
    if (_isProductionResultTableVisible)
    {
      _productionResultPanel.add(_productionResultTableCollapsiblePanel, BorderLayout.CENTER);
    }
    _productionResultPanel.add(_currentActionLabel, BorderLayout.SOUTH);
    _testMessageAbortPanel.add(_productionResultPanel,  BorderLayout.SOUTH);
    _mainContentPanel.add(_currentActionProgressBar,  BorderLayout.SOUTH);
    _mainContentPanel.add(_mainSplitPane, java.awt.BorderLayout.CENTER);
    _abortPanel.add(_futureSerialNumberPanel, java.awt.BorderLayout.CENTER);
    _futureSerialNumberPanel.add(_typedSerialNumberLabel, java.awt.BorderLayout.NORTH);
    _futureSerialNumberPanel.add(_futureSerialNumberTextField, java.awt.BorderLayout.CENTER);
    _queuedSerialNumberImagePanel.add(_panelLoadingImagePanel, java.awt.BorderLayout.CENTER);
    _panelLoadingImagePanel.add(_panelImagePanel, java.awt.BorderLayout.CENTER);
    _queuedSerialNumberImagePanel.add(_queuedSerialNumberPanel, java.awt.BorderLayout.NORTH);
    _eastUpArrowLabel.setBorder(BorderFactory.createEmptyBorder(0,3,0,3));
    _westUpArrowLabel.setBorder(BorderFactory.createEmptyBorder(0,3,0,3));
    _panelLoadingImagePanel.add(_eastUpArrowLabel, java.awt.BorderLayout.EAST);
    _panelLoadingImagePanel.add(_westUpArrowLabel, java.awt.BorderLayout.WEST);
    _panelLoadingImagePanel.add(_imageExplanationLabel, java.awt.BorderLayout.NORTH);

    _clearAllSerialNumberButton.setFont(displayFont);
    _clearSerialNumberButton.setFont(displayFont);
    _statusLabel.setFont(displayFont);
    _currentProgramVersionLabel.setFont(displayFont);
    _programVersionLabel.setFont(displayFont);
    _currentBoardLabel.setFont(displayFont);
    _boardLabel.setFont(displayFont);
    _currentPanelProgramLabel.setFont(displayFont);
    _panelProgramLabel.setFont(displayFont);
    _currentSerialNumberLabel.setFont(displayFont);
    _serialNumberLabel.setFont(displayFont);
    _nextSerialNumbersLabel.setFont(displayFont);
    _nextSerialNumberList.setFont(displayFont);
    _typedSerialNumberLabel.setFont(displayFontBold);
    _currentActionProgressBar.setFont(displayFont);
    fm = getFontMetrics(displayFont);
    prefHeight = fm.getHeight() + 3;   // 3 is the spacing inbetween lines for this font size
    // font height for 5 visible rows is about right
    _nextSerialNumberScrollPane.setPreferredSize(new Dimension(0, prefHeight * _NUMBER_OF_QUEUED_SERIAL_NUMBERS));

    if (useLargeFont)
    {
      Font biggerFont =  FontUtil.getLargeFont();
      _testMessageTextArea.setFont(biggerFont);
    }

    if (useLargeFont)
    {
      // for the bigger font, use these settings
      Insets abortButtonInsets = new Insets(17, 16, 17, 16); // I believe the default is 2,16,2,16.  Small fonts use 8,16,8,16
      _abortButton.setMargin(abortButtonInsets);
    }
    else
    {
      // for the bigger font, use these settings
      Insets abortButtonInsets = new Insets(8, 16, 8, 16); // I believe the default is 2,16,2,16.  Small fonts use 8,16,8,16
      _abortButton.setMargin(abortButtonInsets);
    }
    _currentActionLabel.setFont(FontUtil.getMediumFont(Font.BOLD));
    _actionLabel.setFont(FontUtil.getMediumFont());
    _testExecHeaderLabel.setFont(FontUtil.getMediumFont(Font.BOLD));
    _abortButton.setFont(FontUtil.getMediumFont(Font.BOLD));
    _imageExplanationLabel.setFont(FontUtil.getMediumFont());

//    _currentUnits = StringLocalizer.keyToString("TESTEXEC_GUI_CURRENT_UNITS_KEY");
//    _voltageUnits = StringLocalizer.keyToString("TESTEXEC_GUI_VOLTAGE_UNITS_KEY");
    _WAITING_FOR_SERIAL_NUMBER_MESSAGE = StringLocalizer.keyToString("TESTEXEC_GUI_WAITING_FOR_SERIAL_NUMBER_KEY");
    _abortButton.setText(StringLocalizer.keyToString("TESTEXEC_GUI_ABORT_KEY"));
    _abortButton.setIcon(Image5DX.getImageIcon(Image5DX.TE_ABORT));
    _programVersionLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_PROGRAM_VERSION_KEY"));
    _boardLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_BOARD_LABEL_KEY"));
    _panelProgramLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_PROGRAM_LABEL_KEY"));
    _serialNumberLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_SERIAL_NUMBER_LABEL_KEY"));
    _nextSerialNumbersLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_NEXT_SERIAL_NUMBER_LABEL_KEY"));
    _clearAllSerialNumberButton.setText(StringLocalizer.keyToString("TESTEXEC_GUI_CLEAR_ALL_SERIAL_NUMBER_KEY"));
    _clearSerialNumberButton.setText(StringLocalizer.keyToString("TESTEXEC_GUI_CLEAR_SERIAL_NUMBER_KEY"));
    _actionLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_CURRENT_ACTION_LABEL_KEY"));
    _typedSerialNumberLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_FUTURE_SERIAL_NUMBER_LABEL_KEY") + ": ");
    _imageExplanationLabel.setText("");
    ConfigObservable.getInstance().addObserver(this);
  }

  /**
   * The environment is about to be swapped out and should perform cleanup
   * Re-enable all the controls on the GUI and save its persisted information to the persist file
   * @author Andy Mechtenberg
   */
  public void finish()
  {
    if (_currentTestAction instanceof ProductionFlow && Project.isCurrentProjectLoaded())
    {
      _project = Project.getCurrentlyLoadedProject();
      _project.setIsUpdateComponentNoLoadSetting(false);
      
      //load to original variation setting
      if (_project.getEnabledVariationName().equals(_currentTestAction.getVariationName()) == false)
      {
        _currentTestAction.loadVariationSetting(_project.getEnabledVariationName());
      }
      //auto save if project has been modified. 
      //After changing varaition, test program need to generate, so the project need to save it.
      try
      {
        if (_project.hasBeenModified())
        {
          _project.fastSave();
        }
      }
      catch (DatastoreException dex)
      {
        displayError(dex);
      }      
      finally
      {
        //set to true after original variation is loaded.
        _project.setIsUpdateComponentNoLoadSetting(true);
      }
    }
//    System.out.println("TestExecPanel().finish()");
    _mainUI.enableMenuAndToolBars();
    // Save the size and position of the UI for next invokation
    TestExecPersistance testExecPersistance = new TestExecPersistance();
    testExecPersistance = testExecPersistance.readSettings();  // keep existing settings
    // now, we'll override those that have changed
    testExecPersistance.setSplitPanePosition(_mainSplitPane.getDividerLocation());
    testExecPersistance.writeSettings();
    /** Delete test execution observer */
    _inspectionObservable.deleteObserver(_testExecInspectionObserver);
    _progressObservable.deleteObserver(this);
    _hardwareObservable.deleteObserver(this);
    _fringePatternObservable.deleteObserver(this);
    _resultsProcessingObservable.deleteObserver(this);
    _observerCount--;
    Assert.expect(_observerCount == 0);
  }

  /**
   * This must be called on the Swing thread!
   * @author Andy Mechtenberg
   */
  public void dispose()
  {
    // any memory clean up goes here
  }

  /**
   * Loads the passed in program name into the Java server and updates the UI with it's data
   * @author Andy Mechtenberg
   */
  boolean loadSpecificProgram(final String projectName)
  {
    // at this point either no project is loaded or a different project needs to be loaded
    boolean isProjectLoaded = false;

    try
    {
      // check to see if it exists first

      if (((ProjectDatabase.isAutoUpdateLocalProjectEnabled() == false) && (Project.doesProjectExistLocally(projectName) == false)) ||
          ((ProjectDatabase.isAutoUpdateLocalProjectEnabled()) && (Project.doesProjectExistLocally(projectName) == false) && (ProjectDatabase.doesProjectExist(projectName) == false)))
      {
        MessageDialog.showErrorDialog(_mainUI,
                                      new LocalizedString("TESTEXEC_GUI_PROJECT_DOES_NOT_EXIST_KEY", new Object[] {projectName}),
                                      StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
                                      true,
                                      FontUtil.getMediumFont());
        return isProjectLoaded;
      }
      BooleanRef abortedDuringLoad = new BooleanRef();
      _mainUI.getMainMenuBar().loadProjectForProductionTest(projectName, FontUtil.getMediumFont(), abortedDuringLoad);
      if(abortedDuringLoad.getValue())
        isProjectLoaded = false;
      else
        isProjectLoaded = true;
      abortedDuringLoad = null;
    }
    catch (XrayTesterException ex)
    {
      //Kok Chun, Tan - 9/3/2015
      //XCR-2580 - close project if unable to load recipe in production tab
      _mainUI.getMainMenuBar().projectClose(true);
      isProjectLoaded = false;
      MessageDialog.showErrorDialog(_mainUI,
                                    ex.getMessage(),
                                    StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
                                    true,
                                    FontUtil.getMediumFont());
      _mainUI.changeLightStackToStandby();
    }

    if (isProjectLoaded)
    {
      /** @todo MDW - remove this later.  Debug code. */
      if (_FLAG_CALL_RATE_SPIKES)
      {
        if (_project != Project.getCurrentlyLoadedProject())
        {
          _baselineNumberOfDefectiveJoints = _UNINITIALIZED_VALUE;
          _baselineNumberOfDefectiveComponents = _UNINITIALIZED_VALUE;
        }

        try
        {
          String callRateSpikeDataFileName = Directory.getTempDir() + File.separator + "callRateSpikeData.csv";
          _callRateDataWriter = new CSVFileWriterAxi(Arrays.asList("Project", "Inspection Start", "Defective Joints", "Defective Components"));
          _callRateDataWriter.open(callRateSpikeDataFileName, true);
          _inspectionEngine.initializeFailedJointsAndComponentsCSVFileWriter();
        }
        catch (DatastoreException dex)
        {
          displayError(dex);
        }

      }

      _project = Project.getCurrentlyLoadedProject();
      loadTestProgramInformation(_project);
    }

    return isProjectLoaded;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public int choosePanelProgram(boolean alsoLoad, boolean panelProgram, boolean preSelectPanel, String projName) throws DatastoreException
  {
    int value = chooseProgram(alsoLoad, panelProgram, preSelectPanel, projName);
    
    return value;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public int choosePanelProgram(boolean alsoLoad, boolean panelProgram, boolean preSelectPanel) throws DatastoreException
  {
    // no project name is pass in, display all project or only pre selected project
    String projName = "";
    int value = chooseProgram(alsoLoad, panelProgram, preSelectPanel, projName);
    
    return value;
  }

  /**
   * @author Andy Mechtenberg
   * @author Kee Chin Seong
   */
  int chooseProgram(boolean alsoLoad, boolean panelProgram, boolean preSelectPanel, String projName) throws DatastoreException
  {
    Map<Object, java.util.List<Object>> projectNameToVariationNamesMap = new LinkedHashMap<Object, java.util.List<Object>> ();
    java.util.List<String> projects = new ArrayList<String>();
    Object allLoaded = VariationSettingManager.ALL_LOADED;
    boolean isLeftColumnEnable = true;
    boolean isRightColumnEnable = true;

    if (preSelectPanel)
    {
      //XCR-2980 - Variation dialog keep prompting even user has choose pre select variation
      if(_currentVariation.matches("") == false && _lastChosenProject.matches("") == false)
         return JOptionPane.OK_OPTION;
      
      String preSelectProjectName = Project.getCurrentlyLoadedProject().getName();
      projects.add(preSelectProjectName);
    }
    else if (projName.isEmpty() == false)
    {
      projects.add(projName);
    }
    else
    {
      projects.addAll(FileName.getProjectNames());
    }
    
    for (String project : projects)
    {
      java.util.List<Object> variationNames = new ArrayList<Object>();
      Map<String, Pair<String, String>> variationNameToEnabledMap = VariationSettingManager.getInstance().getVariationNamesToEnabledMap(project);
      boolean preSelectVariation = ProjectReader.getInstance().readProjectPreSelectVariation(project);

      for (Map.Entry<String, Pair<String, String>> variationToEnabledSet : variationNameToEnabledMap.entrySet())
      {
        Object name = variationToEnabledSet.getKey();
        Pair<String, String> pair = variationToEnabledSet.getValue();

        boolean isEnabled = Boolean.parseBoolean(pair.getFirst());
        boolean isChoosableInProd = Boolean.parseBoolean(pair.getSecond());

        if (preSelectVariation)
        {
          if (isEnabled && variationNames.contains(name) == false)
          {
            variationNames.add(name);
          }
        }
        else
        {
          if (isChoosableInProd && variationNames.contains(name) == false)
          {
            if (variationNames.contains(allLoaded) == false)
            {
              variationNames.add(allLoaded);
            }
            variationNames.add(name);
          }
        }
      }

      //Punit: XCR-3040 
      if(Project.isProjectNameValid(project))
      {
        Object projectName = project;
        if (projectNameToVariationNamesMap.containsKey(projectName) == false)
        {
          projectNameToVariationNamesMap.put(projectName, variationNames);
        }
      }
    }

    _lastChosenProject = "";
    _currentVariation = VariationSettingManager.ALL_LOADED;
    MultipleChoiceInputDialog panelChoiceDialog = new MultipleChoiceInputDialog(_mainUI,
                                                                                StringLocalizer.keyToString("TESTEXEC_GUI_PANEL_TITLE_KEY"),
                                                                                StringLocalizer.keyToString("TESTEXEC_GUI_PANEL_MESSAGE_KEY"),
                                                                                StringLocalizer.keyToString("TESTEXEC_GUI_PANEL_CHOOSE_VARIATION_MESSAGE_KEY"),
                                                                                StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                                                                StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                                                                projectNameToVariationNamesMap,
                                                                                isLeftColumnEnable,
                                                                                isRightColumnEnable);
    SwingUtils.centerOnComponent(panelChoiceDialog, _mainUI);
    if (_lightStack != null)
      _mainUI.changeLightStackToStandby();

    int returnValue = panelChoiceDialog.showDialog(FontUtil.getMediumFont());
    if (_lightStack != null)
      changeLightStackToInspectionInProgess();

    if (returnValue == JOptionPane.OK_OPTION)
    {
      _lastChosenProject = (String)panelChoiceDialog.getLeftColumnSelectedValue();
      if (projectNameToVariationNamesMap.get(_lastChosenProject).isEmpty() == false)
      {
        _currentVariation = (String)panelChoiceDialog.getRightColumnSelectedValue();
      }
      //clear all variabless
      panelChoiceDialog = null;
      projectNameToVariationNamesMap.clear();
      projectNameToVariationNamesMap = null;
      projects.clear();
      projects = null;
      displayPanelImage(_lastChosenProject);
      
      if (alsoLoad)
        if (loadSpecificProgram(_lastChosenProject) == false)
        {
          // it failed 
          // if cause by invalid database directory , promtp to ask user want to continue or not;
          if (ProjectDatabase.isAutoUpdateLocalProjectEnabled())
          {
            if (Project.isCurrentProjectLoaded() == false)
            {
              int answerValue = ChoiceInputDialog.showConfirmDialog(_mainUI,
                                                                   StringLocalizer.keyToString("TESTEXEC_GUI_CONTINUE_TESTING_KEY"),
                                                                   StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                                                   FontUtil.getMediumFont());

              if (answerValue == JOptionPane.YES_OPTION)
              {
                BooleanRef abortedDuringLoad = new BooleanRef();
                Project.load(_lastChosenProject, abortedDuringLoad);
                _project = Project.getCurrentlyLoadedProject();
                ProjectDatabase.setContinueWithoutDatabase(true);
                return JOptionPane.OK_OPTION;
              }
              else
                return JOptionPane.CANCEL_OPTION;
            }
          }
          else 
          {
            return JOptionPane.CANCEL_OPTION;
          }
        }
    }
    else
    {
      //clear all variables
      panelChoiceDialog = null;
      projectNameToVariationNamesMap.clear();
      projectNameToVariationNamesMap = null;
      projects.clear();
      projects = null;
      return JOptionPane.CANCEL_OPTION;
    }
    
    //clear all variables
    panelChoiceDialog = null;
    return JOptionPane.OK_OPTION;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  int choosePanelVariation(java.util.List<String> variations, String projectName) throws DatastoreException
  {
    Assert.expect(variations != null);
    Assert.expect(projectName != null);
    
    _currentVariation = VariationSettingManager.ALL_LOADED;
    java.util.List<String> variationList = new ArrayList<String> ();
    variationList.add(VariationSettingManager.ALL_LOADED);
    variationList.addAll(variations);
    Object[] possibleValues = variationList.toArray();
    LocalizedString currentProject = new LocalizedString("TESTEXEC_GUI_VARIATION_MESSAGE_KEY",
                                                        new Object[]
                                                        {projectName});
    ChoiceInputDialog variationChoiceDialog = new ChoiceInputDialog(_mainUI,
                                                                StringLocalizer.keyToString("TESTEXEC_GUI_VARIATION_TITLE_KEY"),
                                                                StringLocalizer.keyToString(currentProject),
                                                                StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                                                StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                                                possibleValues);
    SwingUtils.centerOnComponent(variationChoiceDialog, _mainUI);
    if (_lightStack != null)
      _mainUI.changeLightStackToStandby();

    int returnValue = variationChoiceDialog.showDialog(FontUtil.getMediumFont());
    if (_lightStack != null)
      changeLightStackToInspectionInProgess();

    if (returnValue == JOptionPane.OK_OPTION)
    {
      String selectedVariationName = (String)variationChoiceDialog.getSelectedValue();
      _currentVariation = selectedVariationName;
    }
    else
    {
      variationChoiceDialog = null;
      return JOptionPane.CANCEL_OPTION;
    }
    variationChoiceDialog = null;
    return JOptionPane.OK_OPTION;
  }

  /**
   * @author Andy Mechtenberg
   */
  String getLastChosenProject()
  {
    Assert.expect(_lastChosenProject != null);
    Assert.expect(_lastChosenProject.length() > 0);
    return _lastChosenProject;
  }

  /**
   * Returns the number of boards for the project without doing a full java load
   * @author Andy Mechtenberg
   */
  int getNumberOfBoards(String projectName) throws XrayTesterException
  {
    ProjectSummaryReader projectSummaryReader = ProjectSummaryReader.getInstance();
    ProjectSummary projectSummary = projectSummaryReader.read(projectName);
    return projectSummary.getNumBoards();
  }

  /**
   * @author Andy Mechtenberg
   */
  java.util.List<String> getBoardNames(String projectName) throws XrayTesterException
  {
    ProjectSummaryReader projectSummaryReader = ProjectSummaryReader.getInstance();
    ProjectSummary projectSummary = projectSummaryReader.read(projectName);
    return projectSummary.getBoardNames();
  }

  /**
   * This method pops up a dialog to capture a serial number from the user.  Must be called on the swing thread.
   * The requireNonEmptySerialNumber is for board-based serial numbers, and will not accept an empty string as the
   * serial number.  Also, this flag will prompt the user if they really want to cancel out.
   * @return the OK_OPTION or CANCEL_OPTION from the JOptionPane
   * Sets the _serialNumber private member variable.
   * @author Andy Mechtenberg
   */
  int promptUserForSerialNumber(String prompt, boolean individualBoardSerialNumber)
  {
    Assert.expect(prompt != null);
    _userEnteredSerialNumber = null;
    StringInputDialog serialNumberDialog = new StringInputDialog(_mainUI,
                                                                 StringLocalizer.keyToString("TESTEXEC_GUI_SERIAL_NUMBER_TITLE_KEY"),
                                                                 prompt);
    _mainUI.changeLightStackToStandby();
    notifyUserWaitingForSerialNumber();
    int value = serialNumberDialog.showDialog(FontUtil.getMediumFont());
    changeLightStackToInspectionInProgess();
    if (value == JOptionPane.OK_OPTION)
    {
      _userEnteredSerialNumber = serialNumberDialog.getStringInput();
      notifyUserSerialNumberAccepted();
      if (individualBoardSerialNumber && (_userEnteredSerialNumber.equals(_TRY_AGAIN)))
      {
        // tell the user that they must enter a non-null serial number
        MessageDialog.showErrorDialog(_mainUI,
                                      new LocalizedString("TESTEXEC_GUI_SERIAL_NUMBER_REQUIRED_KEY", null),
                                      StringLocalizer.keyToString("TESTEXEC_GUI_SERIAL_NUMBER_TITLE_KEY"),
                                      true,
                                      FontUtil.getMediumFont());
        _mainUI.changeLightStackToStandby();
      }
      // get cad match if enabled
      return value;
    }
    else if (value == JOptionPane.CANCEL_OPTION) // this is the CANCEL option
    {
      //Ask if they want to really cancel, or keep entering serial number
      if (individualBoardSerialNumber)
      {
        String confirmMessage = StringLocalizer.keyToString("TESTEXEC_GUI_CONFIRM_CANCEL_KEY");
        if (confirmAction(confirmMessage, true))
        {
          return value;  // return CANCEL_OPTION
        }
        else  // the user didn't want to abort, so they get to try again
        {
          _userEnteredSerialNumber = _TRY_AGAIN;
          return JOptionPane.OK_OPTION;
        }
      }
      else
        return value;
    }
    else
    {
      Assert.expect(false);
      return value;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  String getUserEnteredSerialNumber()
  {
    Assert.expect(_userEnteredSerialNumber != null);

    return _userEnteredSerialNumber;
  }

  /**
    * @author Andy Mechtenberg
    */
   DemoMode getDemoMode()
   {
     Assert.expect(_demoMode != null);
     return _demoMode;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setCurrentMode(boolean productionTest, DemoMode demoMode)
  {
    Assert.expect(demoMode != null);

    _productionTest = productionTest;
    _demoMode = demoMode;
  }

  /**
   * This method needs to be called on the Swing Thread
   * This will start the TextExec GUI down the current mode of operation.
   * Modes supported:
   * Production Test
   * Panel Test
   * Bypass Mode
   * Image Collection for Panel/Board/Component/Pin/Family/Subtype
   * Demo Mode
   * @author Andy Mechtenberg
   */
  public void startCurrentMode()
  {
    Assert.expect(SwingUtilities.isEventDispatchThread());

    String machineID = String.valueOf(_xRayTester.getSerialNumber());
    String versionNumber = Version.getVersion();

    LocalizedString localizedHeaderStr = new LocalizedString("TESTEXEC_GUI_HEADER_KEY",
        new Object[]
        {machineID, versionNumber});
    String headerStr = StringLocalizer.keyToString(localizedHeaderStr);
    if (_xRayTester.isSimulationModeOn())
    {
      headerStr = StringLocalizer.keyToString("TESTEXEC_GUI_SIMULATION_KEY") + " " + headerStr;
      _testExecHeaderLabel.setForeground(Color.RED);
      String message = StringLocalizer.keyToString("TESTEXEC_GUI_SIMULATION_INSTRUCTIONS_KEY");
      // this message is longer, so don't call "addToInformationPane" as that is set to string format at 50 char limit.
      Document doc = _testMessageTextArea.getDocument();
      try
      {
        doc.insertString(doc.getLength(), message, null);
        doc.insertString(doc.getLength(), "\n", null);
      }
      catch(BadLocationException be)
      {

      }
//      _testMessageTextArea.append(message);
//      _testMessageTextArea.append("\n");
    }
    
    //Siew Yeng - XCR1745 - Semi-automated mode
    if(_testExecution.isSemiAutomatedModeEnabled())
    {
      headerStr = StringLocalizer.keyToString("TESTEXEC_GUI_SEMI_AUTOMATED_KEY") + " " + headerStr;

      String message = StringLocalizer.keyToString("TESTEXEC_GUI_SEMI_AUTOMATED_INSTRUCTIONS_KEY");
      Document doc = _testMessageTextArea.getDocument();
      try
      {
        doc.insertString(doc.getLength(), message, null);
        doc.insertString(doc.getLength(), "\n", null);
      }
      catch(BadLocationException be)
      {

      }
    }
    
    _testExecHeaderLabel.setText(headerStr);
    if (ProjectDatabase.isAutoUpdateLocalProjectEnabled())
      _statusLabel.setText(StringLocalizer.keyToString("HP_AUTO_PULL_ON_KEY"));
    else
      _statusLabel.setText(StringLocalizer.keyToString("HP_AUTO_PULL_OFF_KEY"));

//    // decide now if we'll be runing review defects after an inspection
//    Config softwareConfig = Config.getInstance();
//
//    boolean supervisorAutoReview = false;//softwareConfig.getBooleanValue(SoftwareConfigEnum.SUPERVISOR_AUTO_REVIEW);
//    boolean operatorAutoReview = false;//softwareConfig.getBooleanValue(SoftwareConfigEnum.OPERATOR_AUTO_REVIEW);
//    _autoReviewDefectsThreshold = 0;//softwareConfig.getIntValue(SoftwareConfigEnum.AUTO_REVIEW_THRESHOLD);
//    if (_productionTest && operatorAutoReview)
//      _shouldRunReviewDefects = true;
//    else if (supervisorAutoReview)
//      _shouldRunReviewDefects = true;
//    else
//      _shouldRunReviewDefects = false;
//
//    if (_shouldRunReviewDefects)
//      _testMode.enablePauseAfterBoardInspection();
//    else
//      _testMode.disablePauseAfterBoardInspection();

//    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
//      StringLocalizer.keyToString("TESTEXEC_GUI_INITIALIZE_SYSTEM_KEY"),
//      StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
//      true);
//    SwingUtils.setFont(busyCancelDialog, FontUtil.getMediumFont());
//    busyCancelDialog.pack();
//    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
    _xrayTesterException = null;
    _panelUnloadInProgress = false;
//    _swingWorkerThread.invokeLater(new Runnable()
//    {
//      public void run()
//      {
//        try
//        {
//          /** @todo What about this? */
//          _testExecution.initSystemForTest();
//        }
//        catch (final XrayTesterException ex)
//        {
//          _xrayTesterException = ex;
//          SwingUtils.invokeLater(new Runnable()
//          {
//            public void run()
//            {
//              displayError(ex);
//              _parent.exitApp();
//              return;
//            }
//          });
//        }
//        finally
//        {
//          // wait until visible
//          while (busyCancelDialog.isVisible() == false)
//          {
//            try
//            {
//              Thread.sleep(200);
//            }
//            catch (InterruptedException ex)
//            {
//              // do nothing
//            }
//          }
//          busyCancelDialog.dispose();
//        }
//      }
//    });
//    busyCancelDialog.setVisible(true);
    // if this exception is not null, that means we caught one above and already exited the UI
    if (_xrayTesterException != null)
      return;


    _currentTestAction = TestFlow.getInstance(_productionTest, _mainUI, this);
    setupInterfaceForProductionMode();
    try
    {
      _currentTestAction.execute();
    }
    catch (XrayTesterException ex)
    {
      displayError(ex);
      exitTestExecution();
    }
  }

  /**
   * In Test Development mode, serial number queueing is disabled, so we'll disable the controls
   * for clearing them out
   * @author Andy Mechtenberg
   */
  SerialNumberProcessor setupInterfaceForTestDevelopmentMode()
  {
    _clearAllSerialNumberButton.setEnabled(false);
    _clearSerialNumberButton.setEnabled(false);

    _abortPanel.remove(_futureSerialNumberPanel);
    _testMessageTextArea.setFocusable(true);
    _testMessageTextArea.requestFocus();
    try
    {
      _serialNumberProcessor = new SerialNumberProcessor(this);
    }
    catch (XrayTesterException ex)
    {
      displayError(ex);
      exitTestExecution();
    }

    repaint();
    return _serialNumberProcessor;
  }

  /**
   * In Test Development mode, serial number queueing is disabled, so we'll disable the controls
   * for clearing them out
   * @author Andy Mechtenberg
   */
  void setupInterfaceForBypassMode()
  {
    _clearAllSerialNumberButton.setEnabled(false);
    _clearSerialNumberButton.setEnabled(false);

    _abortPanel.remove(_futureSerialNumberPanel);
    _testMessageTextArea.setFocusable(true);
    _testMessageTextArea.requestFocus();
    repaint();
  }

  /**
   * @author Andy Mechtenberg
   */
   SerialNumberProcessor setupInterfaceForProductionMode()
   {
     _clearAllSerialNumberButton.setEnabled(true);
     _clearSerialNumberButton.setEnabled(true);

     boolean barCodeReadersEnabled = BarcodeReaderManager.getInstance().getBarcodeReaderAutomaticReaderEnabled();
     if ( barCodeReadersEnabled || _demoMode.isDemoModeEnabled())
     {
       // if using the automatic barcode reader, DO NOT display the next serial number queue because
       // it won't be used.
       _abortPanel.remove(_futureSerialNumberPanel);
       _clearAllSerialNumberButton.setEnabled(false);
       _clearSerialNumberButton.setEnabled(false);
     }
     else
     {
       // CR1054 fix by LeeHerng - Make sure we make future serial number panel visible
       // if we do not enable barcode reader
       _abortPanel.add(_futureSerialNumberPanel, java.awt.BorderLayout.CENTER);

       _futureSerialNumberTextField.requestFocus();
     }
     try
     {
       _serialNumberProcessor = new SerialNumberProcessor(this);
       gatherSerialNumberHandlingOptions();
     }
     catch (XrayTesterException ex)
     {
       displayError(ex);
       exitTestExecution();
     }
     repaint();
     return _serialNumberProcessor;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void gatherSerialNumberHandlingOptions() throws XrayTesterException
  {
    java.util.List warnings = new ArrayList();

    _displayLastImageInQueue = SerialNumberManager.getInstance().isTestExecDisplayingLastCadImageInQueue();
  }

  /**
   * @author Andy Mechtenberg
   */
  void setTestInProgress(boolean testInProgress)
  {
    _testInProgress = testInProgress;

    // if a test is in progress, we want to disable the image window, otherwise enable it
    if (_testInProgress)
    {
      _atLeastOneImageWasInspected = false;
      _lastTestPass = false;
      _doingViewInspection = false;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isTestInProgress()
  {
    return _testInProgress;
  }

  /**
   * Update the panel program specific data as well as the status bar stuff
   * @author Andy Mechtenberg
   */
  void updateScreenData(Project project, String serialNumber, boolean updatePanelImage)
  {
    Assert.expect(project != null);
    Assert.expect(serialNumber != null);
    _project = project;
    _currentSerialNumber = serialNumber;

    _currentPanelProgramLabel.setText(_project.getName());
    _currentBoardLabel.setText(String.valueOf(_project.getPanel().getNumBoards()));
    _currentSerialNumberLabel.setText(_currentSerialNumber);
    _currentProgramVersionLabel.setText(new Double(_project.getVersion()).toString());
    if (updatePanelImage)
    {
      displayPanelImage(_project.getName());
    }
    _productionResult.setProjectName(_project.getName());
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean okToUpdateImage()
  {
    if (_displayLastImageInQueue)
      return true;
    else if (_nextSerialNumberListModel.isEmpty() && (_testInProgress == false))
      return true;
    else
      return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  void updatePanelImage()
  {
    if ((_displayLastImageInQueue) && (_nextSerialNumberListModel.isEmpty() == false))
    {
      // get the last one in the list and display it
      SerialNumberQueueListItem listItem = (SerialNumberQueueListItem)_nextSerialNumberListModel.lastElement();
      displayPanelImage(listItem.getProjectName());
    }
    else if (_project != null)  // no project may be loaded
      displayPanelImage(_project.getName());
  }

  /**
   * @author Andy Mechtenberg
   */
  void displayPanelImage(String projectName)
  {
    Assert.expect(projectName != null);
    Image image = null;
    _panelImagePanel.setRotateImage(false);
    PanelHandlerPanelFlowDirectionEnum flowDirection = _panelHandler.getPanelFlowDirection();
    boolean flipPanelImage = flowDirection == PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT;
    boolean imageAvailable = FileName.hasPanelImage(projectName, flipPanelImage);
    if (flipPanelImage == false)
    {
      image = FileName.getPanelImage(projectName, false);
    }
    else
    {
      if (imageAvailable)
      {
        // rotate image 180 degrees if handler is right-to-left
        _panelImagePanel.setRotateImage(true);
      }
      else
      {
        // don't rotate image if we are going to display textual "no image"
        _panelImagePanel.setRotateImage(false);
      }
      image = FileName.getPanelImage(projectName, true);
    }

    if (image != null)
    {
      _panelImagePanel.setImage(image);
    }

    if (imageAvailable == false)
    {
      // notify the user how to generate an image if they want to
      addToInformationPane(StringUtil.format(StringLocalizer.keyToString("TESTEXEC_GUI_NO_IMAGE_INSTRUCTIONS_KEY"), 45));
    }
  }

   /**
   * @author Lim Boon Hong
   */
  void displayPanelImage(String projectName, String boardName)
  {
    Assert.expect(projectName != null);
    Image image = null;
    _panelImagePanel.setRotateImage(false);
    PanelHandlerPanelFlowDirectionEnum flowDirection = _panelHandler.getPanelFlowDirection();
    boolean flipPanelImage = flowDirection == PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT;
    boolean imageAvailable = FileName.hasPanelImage(projectName, flipPanelImage);
    if (flipPanelImage == false)
    {
     
      image = FileName.getPanelImage(projectName, boardName, false);
    }
    else
    {
      if (imageAvailable)
      {
        // rotate image 180 degrees if handler is right-to-left
        _panelImagePanel.setRotateImage(true);
      }
      else
      {
        // don't rotate image if we are going to display textual "no image"
        _panelImagePanel.setRotateImage(false);
      }
      image = FileName.getPanelImage(projectName, boardName, true);
    }

    if (image != null)
    {
      _panelImagePanel.setImage(image);
    }

    if (imageAvailable == false)
    {
      // notify the user how to generate an image if they want to
      addToInformationPane(StringUtil.format(StringLocalizer.keyToString("TESTEXEC_GUI_NO_IMAGE_INSTRUCTIONS_KEY"), 45));
    }
  }
  
  /**
   * @author Andy Mechtenberg
   */
  void displaySerialNumber(String serialNumber)
  {
    Assert.expect(serialNumber != null);
    _currentSerialNumber = serialNumber;
    _currentSerialNumberLabel.setText(_currentSerialNumber);
    _productionResult.setSerialNumber(_currentSerialNumber);
  }

  /**
   * @author Andy Mechtenberg
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  void displayTestResultsDetails(TestExecutionTimer timer)
  {
    Assert.expect(timer != null);
    // if the current board program hasn't been set, most likely the test was aborted or suffered an exception
    // very early in the process and there's no point in displaying the times.  We'll just do nothing
    if (_project == null)
      return;

    //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
    String productionPanelLoadingTimeStr = StringUtil.convertMilliSecondsToElapsedTime(_panelHandler.getProductionPanelLoadingTimeInMilis(), true);  
    _productionResult.setInspectionPanelLoadingTime(productionPanelLoadingTimeStr);
    
    //Khaw Chek Hau - XCR3774: Display load & unload time during production
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.TEST_EXEC_DISPLAY_PRODUCTION_PANEL_LOADING_AND_UNLOADING_TIME))
    {
      LocalizedString productionPanelLoadingTimeLocalizedString= new LocalizedString("TESTEXEC_GUI_PANEL_LOADING_TIME_KEY",
          new Object[]
          {productionPanelLoadingTimeStr});

      addToInformationPane(StringLocalizer.keyToString(productionPanelLoadingTimeLocalizedString));
    }
    
    String fullInspectionTimeStr = StringUtil.convertMilliSecondsToElapsedTime(timer.getElapsedFullInspectionTimeInMillis(), true);
    LocalizedString fullInspectionTimeLocalizedString = new LocalizedString("TESTEXEC_GUI_VIEWS_TIME_KEY",
        new Object[]
        {fullInspectionTimeStr});

//    String timingMetricString = StringLocalizer.keyToString(fullInspectionTimeLocalizedString) + "\n";
//    String timingMetricString = MainMenuGui.getInstance().getTimingMetricsString(timer);
    String timingMetricString = "";
    if(_DEVELOPER_PERFORMANCE)
    {
      timingMetricString = MainMenuGui.getInstance().getTimingMetricsString(timer);
    }
    else
    {
      timingMetricString = StringLocalizer.keyToString(fullInspectionTimeLocalizedString);
    }
    addToInformationPane(timingMetricString);
    _productionResult.setInspectionTime(fullInspectionTimeStr);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  void displayTestResultsStatus()
  {
    //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
    String productionPanelUnloadingTimeStr = StringUtil.convertMilliSecondsToElapsedTime(_panelHandler.getProductionPanelUnloadingTimeInMilis(), true);
    _productionResult.setInspectionPanelUnloadingTime(productionPanelUnloadingTimeStr);
    
    if (_productionTest && _atLeastOneImageWasInspected && 
          Config.getInstance().getBooleanValue(SoftwareConfigEnum.TEST_EXEC_DISPLAY_PRODUCTION_PANEL_LOADING_AND_UNLOADING_TIME))
    {
      LocalizedString productionPanelUnloadingTimeLocalizedString= new LocalizedString("TESTEXEC_GUI_PANEL_UNLOADING_TIME_KEY",
          new Object[]
          {productionPanelUnloadingTimeStr});

      addToInformationPane(StringLocalizer.keyToString(productionPanelUnloadingTimeLocalizedString));
    }
    
    // if the last test was aborted before view inspection, we'll output an aborted message instead
    // of pass or fail
    // Except in production mode, where the Abort message will happen regardless of when the abort happened
    if ((_productionTest) && _testExecution.didUserAbort())
    {
      addToInformationPane("\n" + StringLocalizer.keyToString("TESTEXEC_GUI_TEST_ABORTED_KEY") + "\n");
      _productionResult.setInspectionStatusAborted();
    }
    else if (_testExecution.didUserAbort() && (_atLeastOneImageWasInspected == false))
    {
      addToInformationPane("\n" + StringLocalizer.keyToString("TESTEXEC_GUI_TEST_ABORTED_KEY") + "\n");
      _productionResult.setInspectionStatusAborted();
    }
    else if (_atLeastOneImageWasInspected)
    {
      //System.out.println("Time in millis: " + durationInMillis);
      if (_lastTestPass)
      {
        addToInformationPane("\n" + StringLocalizer.keyToString("TESTEXEC_GUI_TEST_PASSED_KEY") + "\n");
        _productionResult.setInspectionStatusPassed();
      }
      else
      {
        addToInformationPane("\n" + StringLocalizer.keyToString("TESTEXEC_GUI_TEST_FAILED_KEY") + "\n");
        _productionResult.setInspectionStatusFailed();
      }
      addToProductionResultTable();
    }
    _displayedAlgorithmMessageInProductionMode = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void clearAllSerialNumberButton_actionPerformed()
  {
    _nextSerialNumberListModel.clear();
    // since we're emptying the queue, if the display last image in queue option is on, we need to chage the image
    // to be the panel current loaded
    _imageExplanationLabel.setText("");
    if (_displayLastImageInQueue && (_project != null))
    {
      displayPanelImage(_project.getName());
    }
  }

  private void serialNumberListSelectionChanged(ListSelectionEvent lse)
  {
    if (lse.getValueIsAdjusting())
      return;
    if (_nextSerialNumberList.getSelectedIndex() == -1) // no selection
      return;

    // now, get the value selected, and automatically select all the one associated with that panel
    SerialNumberQueueListItem listItem = (SerialNumberQueueListItem)_nextSerialNumberList.getSelectedValue();
    int selectedIndex = _nextSerialNumberList.getSelectedIndex();
    java.util.List allIndexes = listItem.getIndexesForAllPanelEntries(selectedIndex);
    int fromIndex = ((Integer)allIndexes.get(0)).intValue();
    int toIndex = ((Integer)allIndexes.get(allIndexes.size() -1 )).intValue();
    _nextSerialNumberList.setSelectionInterval(fromIndex, toIndex);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void clearSerialNumberButton_actionPerformed()
  {
    int selectedIndex = _nextSerialNumberList.getSelectedIndex();
    if (selectedIndex >= 0)
    {
      SerialNumberQueueListItem listItem = (SerialNumberQueueListItem)_nextSerialNumberListModel.getElementAt(selectedIndex);
      java.util.List allIndexes = listItem.getIndexesForAllPanelEntries(selectedIndex);
      int fromIndex = ((Integer)allIndexes.get(0)).intValue();
      int toIndex = ((Integer)allIndexes.get(allIndexes.size() -1 )).intValue();
      _nextSerialNumberListModel.removeRange(fromIndex, toIndex);
      if (_displayLastImageInQueue && (toIndex > _nextSerialNumberList.getMaxSelectionIndex()))
      {
        // this means we've deleted the last image on queue.  We may have to update the image!
        // first, find the new last thing on queue
        if (_nextSerialNumberListModel.isEmpty() == false)
        {
          listItem = (SerialNumberQueueListItem)_nextSerialNumberListModel.lastElement();
          displayPanelImage(listItem.getProjectName());
        }
        else
        {
          // the list now empty.  Do the same as when we delete all
          _imageExplanationLabel.setText("");
          if (_project != null)
          {
            displayPanelImage(_project.getName());
          }
        }
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean isQueuedSerialNumberAvailable()
  {
    return (_nextSerialNumberListModel.isEmpty() == false);
  }

  /**
   * Returns back the first item in the queue, where an item is an instance of PanelSerialNumberData
   * This will remove more than one element from the queue.
   * @author Andy Mechtenberg
   */
  PanelSerialNumberData getQueuedSerialNumberData()
  {
    Assert.expect(_nextSerialNumberListModel.isEmpty() == false);
    // we're going to remove elements from the list, so clear any current selections, as the action listener on it
    // won't be able to handle it when we remove stuff one by one.
    _nextSerialNumberList.clearSelection();
    String projectName = "";
    String variationName = "";
    java.util.List<String> serialNumbers = new ArrayList<String>();
    // get first item, must be panel name
    SerialNumberQueueListItem queueItem = (SerialNumberQueueListItem)_nextSerialNumberListModel.get(0);
    java.util.List items = queueItem.getIndexesForAllPanelEntries(0);
    Iterator it = items.iterator();
    while(it.hasNext())
    {
      Integer index = (Integer)it.next();
      // when we do a remove 0th element, all other elements move up
      queueItem = (SerialNumberQueueListItem)_nextSerialNumberListModel.remove(0);
      if (index.intValue() == 0)
      {
        projectName = queueItem.getProjectName();
      }
      else if (index.intValue() == 1)
      {
        variationName = queueItem.getVariationName();
      }
      else
      {
        serialNumbers.add(queueItem.getSerialNumber());
      }
    }
    if (_nextSerialNumberListModel.isEmpty())
    {
      // the list now empty.  Do the same as when we delete all
      _imageExplanationLabel.setText("");

    }
    return new PanelSerialNumberData(projectName, serialNumbers, variationName);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void abortButton_actionPerformed(ActionEvent e)
  {
    abortCurrentTestAction();
  }

  /**
   * @author Andy Mechtenberg
   */
  void addToInformationPane(String message)
  {
    Assert.expect(message != null);

    int total = _testMessageTextArea.getDocument().getLength();
    Document doc = _testMessageTextArea.getDocument();
    if (total > _RUNTIME_TEXT_LIMIT)
    {
      try
      {
        doc.remove(0, _RUNTIME_TEXT_REMOVAL_AMOUNT);
    }
      catch(BadLocationException be)
      {

      }
//      _testMessageTextArea.replaceRange(null, 0, _RUNTIME_TEXT_REMOVAL_AMOUNT);
    }
    try
    {
      doc.insertString(doc.getLength(), StringUtil.format(message, 50), null);
      doc.insertString(doc.getLength(), "\n", null);
    }
    catch(BadLocationException be)
    {
    }
//    _testMessageTextArea.append(StringUtil.format(message, 50));
//    _testMessageTextArea.append("\n");
  }

  /**
   * @author Chong, Wei Chin
   */
  void addToInformationPaneWithWarning(String message)
  {
    Assert.expect(message != null);

    int total = _testMessageTextArea.getDocument().getLength();
    Document doc = _testMessageTextArea.getDocument();
    if (total > _RUNTIME_TEXT_LIMIT)
    {
      try
      {
        doc.remove(0, _RUNTIME_TEXT_REMOVAL_AMOUNT);
      }
      catch(BadLocationException be)
      {

      }
//      _testMessageTextArea.replaceRange(null, 0, _RUNTIME_TEXT_REMOVAL_AMOUNT);
    }
    try
    {
      StyleContext styleContext = StyleContext.getDefaultStyleContext();
      AttributeSet warningAttributeSet = styleContext.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, Color.RED);

      doc.insertString(doc.getLength(), message, warningAttributeSet);
      doc.insertString(doc.getLength(), "\n", null);
    }
    catch(BadLocationException be)
    {
    }
//    _testMessageTextArea.append("<font size=5 face=arial color=red>" + StringUtil.format(message, 50) + "</font>");
//    _testMessageTextArea.append("\n");
  }

  /**
   * @author Andy Mechtenberg
   */
  void addAlgorithmMessageToInformationPane(String message)
  {
    Assert.expect(message != null);
    // Algorithm messages are suppressed when in production mode, displayed otherwise
    // If in production mode, we'll put up a generic "there were errors" message, but only once
    // per panel test
    if (_productionTest)
    {
        // Turn off this message by default, because CM's customer like to ask about this, and competitor is using this to attack.
//      if (_displayedAlgorithmMessageInProductionMode == false)
//      {
//        addToInformationPane(StringLocalizer.keyToString("TESTEXEC_ALGORITHM_WARNINGS_KEY"));
//        _displayedAlgorithmMessageInProductionMode = true;
//      }
      if (_DEVELOPER_DEBUG)
      {
        System.out.println("Information Pane in Test Execution - START");
        System.out.println(message);
        System.out.flush();
        System.out.println("Information Pane in Test Execution - END");
      }
    }
    else
    {
      addToInformationPane(message);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  void handleImageDoneEvent(int imageNum)
  {
//    _testMessageTextArea.append("View " + viewNum + " done\n");
    LocalizedString currentActionLocalizedString = null;
    currentActionLocalizedString = new LocalizedString("TESTEXEC_GUI_TESTING_PANEL_KEY",
        new Object[] {_project.getName()});

    // For throughput optimization, the panel will unload as soon as the hardware is done scanning. There will
    // be a lag as classification continues to process images.  During that overlap, I want the progress bar
    // to continue, but leave the message at the unload so the user is directed to unload the panel
    if (_panelUnloadInProgress == false)
      _currentActionLabel.setText(StringLocalizer.keyToString(currentActionLocalizedString));

    int percentDone = (imageNum * 100)/ _totalEventsForProgressBar;
    _currentActionProgressBar.setValue(percentDone);
    _currentActionProgressBar.setString(percentDone + " " + StringLocalizer.keyToString("GUI_PERCENT_COMPLETE_KEY"));
    if (_doingViewInspection)
    {
      _atLeastOneImageWasInspected = true;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  void handlePanelLoadingIntoMachine()
  {
    _currentActionLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_LOADING_PANEL_KEY"));
    _panelUnloadInProgress = false;
    //Khaw Chek Hau - XCR3609: Production Future Serial Number Cursor Not Lock
    futureSerialNumberPanelRequestFocus();
  }

  /**
   * @author Andy Mechtenberg
   */
  void handlePanelLoadedIntoMachine()
  {
    _currentActionLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_PANEL_LOADED_KEY"));
    _panelUnloadInProgress = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  void handlePanelUnloadingFromMachine()
  {
    _currentActionLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_UNLOADING_PANEL_KEY"));
    _panelUnloadInProgress = true;
  }

  /**
   * @author Andy Mechtenberg
   */
  void handlePanelUnloadedFromMachine()
  {
    _currentActionLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_PANEL_UNLOADED_KEY"));
    _panelUnloadInProgress = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  void handleRuntimeAlignmentStarted()
  {
    _currentActionLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_RUNTIME_ALIGNMENT_KEY"));
    _currentActionProgressBar.setValue(0);
    _currentActionProgressBar.setString(0 + " " + StringLocalizer.keyToString("GUI_PERCENT_COMPLETE_KEY"));
    _currentlyAligningGroup = 0;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  void handleRuntimeSurfaceMapStarted(int totalSurfaceMapPoints)
  {
    _currentActionLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_RUNTIME_SURFACE_MAP_KEY"));
    _currentActionProgressBar.setValue(0);
    _currentActionProgressBar.setString(0 + " " + StringLocalizer.keyToString("GUI_PERCENT_COMPLETE_KEY"));
    _currentSurfaceMapPoints = 0;
    _totalSurfaceMapPoints = totalSurfaceMapPoints;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  void handleRuntimeAlignmentSurfaceMapStarted(int totalSurfaceMapPoints)
  {
    _currentActionLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_RUNTIME_ALIGNMENT_SURFACE_MAP_KEY"));
    _currentActionProgressBar.setValue(0);
    _currentActionProgressBar.setString(0 + " " + StringLocalizer.keyToString("GUI_PERCENT_COMPLETE_KEY"));
    _currentAlignmentSurfaceMapPoints = 0;
    _totalAlignmentSurfaceMapPoints = totalSurfaceMapPoints;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  void handleRuntimeSurfaceMapInProgress()
  {
    _currentActionLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_RUNTIME_SURFACE_MAP_KEY"));
    _currentSurfaceMapPoints++;
    if (_totalSurfaceMapPoints <= 0) _totalSurfaceMapPoints = 1;
    int percentDone = (_currentSurfaceMapPoints * 100) / _totalSurfaceMapPoints; 
    _currentActionProgressBar.setValue(percentDone);
    _currentActionProgressBar.setString(percentDone + " " + StringLocalizer.keyToString("GUI_PERCENT_COMPLETE_KEY"));
  }
  
  /**
   * @author Cheah Lee Herng
   */
  void handleRuntimeAlignmentSurfaceMapInProgress()
  {
    _currentActionLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_RUNTIME_ALIGNMENT_SURFACE_MAP_KEY"));
    _currentAlignmentSurfaceMapPoints++;
    if (_totalAlignmentSurfaceMapPoints <= 0) _totalAlignmentSurfaceMapPoints = 1;
    int percentDone = (_currentAlignmentSurfaceMapPoints * 100) / _totalAlignmentSurfaceMapPoints; 
    _currentActionProgressBar.setValue(percentDone);
    _currentActionProgressBar.setString(percentDone + " " + StringLocalizer.keyToString("GUI_PERCENT_COMPLETE_KEY"));
  }

  /**
   * @author Andy Mechtenberg
   */
  void handleRuntimeAlignmentInProgress()
  {
    _currentActionLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_RUNTIME_ALIGNMENT_KEY"));
    _currentlyAligningGroup++;
    int percentDone = (_currentlyAligningGroup * 100) / 3;
    _currentActionProgressBar.setValue(percentDone);
    _currentActionProgressBar.setString(percentDone + " " + StringLocalizer.keyToString("GUI_PERCENT_COMPLETE_KEY"));
  }

  /**
   * @author Andy Mechtenberg
   */
  void handleBeginInspection(int total)
  {
    _totalEventsForProgressBar = total;
    String projectName = _project.getName();

    LocalizedString currentActionLocalizedString = null;
    currentActionLocalizedString = new LocalizedString("TESTEXEC_GUI_TESTING_PANEL_KEY",
        new Object[] {projectName});
    _doingViewInspection = true;

    Assert.expect(currentActionLocalizedString != null);
    _currentActionLabel.setText(StringLocalizer.keyToString(currentActionLocalizedString));

    _currentActionProgressBar.setValue(0);
    _currentActionProgressBar.setString(0 + " " + StringLocalizer.keyToString("GUI_PERCENT_COMPLETE_KEY"));
    
    //Khaw Chek Hau - XCR3609: Production Future Serial Number Cursor Not Lock
    futureSerialNumberPanelRequestFocus();
  }

  /**
   * @author Andy Mechtenberg
   */
  void handleInspectionResults(int pinsProcessed,
                               int pinsDefective,
                               int componentsDefective)
  {
    Assert.expect(pinsProcessed >= 0);
    Assert.expect(pinsDefective >= 0);
    Assert.expect(componentsDefective >= 0);

    LocalizedString pinsProcessedString = new LocalizedString("TESTEXEC_GUI_PINS_PROCESSED_KEY",
                                                              new Object[]{new Integer(pinsProcessed)});
    LocalizedString pinsDefectiveString = new LocalizedString("TESTEXEC_GUI_PINS_DEFECTIVE_KEY",
                                                              new Object[]{new Integer(pinsDefective)});
    LocalizedString componentsDefectiveString = new LocalizedString("TESTEXEC_GUI_COMPONENTS_DEFECTIVE_KEY",
                                                                    new Object[]{new Integer(componentsDefective)});

    addToInformationPane(StringLocalizer.keyToString(pinsProcessedString) + "\n" + StringLocalizer.keyToString(pinsDefectiveString));
    _productionResult.setPanelPinsProcessed(pinsProcessed);
    _productionResult.setPanelPinsDefective(pinsDefective);
    _productionResult.setPanelComponentsDefective(componentsDefective);
    _productionResult.setInspectionStartTime(StringUtil.convertMilliSecondsToTimeStamp(_inspectionEngine.getInspectionStartTimeInMillis()));
    
    //Khaw Chek Hau - XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
    int inspectionCallRate = MathUtil.roundDoubleToInt(((double)pinsDefective/(double)pinsProcessed) * (double)1000000); 
    _productionResult.setInspectionCallRate(inspectionCallRate);
    
    if (componentsDefective > 0)
      addToInformationPane(StringLocalizer.keyToString(componentsDefectiveString));
    addToInformationPane("");

    /** @todo: MDW - remove later.  We either need to set our baseline defect rate or make sure we didn't spike
     * too high above baseline. */
    if (_FLAG_CALL_RATE_SPIKES)
    {
      // Log the call rate data.
      long inspectionStartTimeInMillis = _inspectionEngine.getInspectionStartTimeInMillis();
      _callRateDataWriter.writeDataLine(Arrays.asList(_project.getName(),
                                                      String.valueOf(inspectionStartTimeInMillis),
                                                      String.valueOf(pinsDefective),
                                                      String.valueOf(componentsDefective)));

      // Print out the manual alignment transform(s).
      TestProgram testProgram = _project.getTestProgram();
      for (TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
      {
        PanelLocationInSystemEnum panelLocationInSystem = testSubProgram.getPanelLocationInSystem();
        AffineTransform coreManualTransform = testSubProgram.getCoreManualAlignmentTransform();
        AffineTransform aggregateManualTransform = testSubProgram.getAggregateManualAlignmentTransform();

        System.out.println("[mdw] " +
                           panelLocationInSystem.getName() +
                           " side core manual xform: " +
                           coreManualTransform +
                           ".  Inspection start: " +
                           inspectionStartTimeInMillis);
        System.out.println("[mdw] " +
                           panelLocationInSystem.getName() +
                           " side aggregate manual xform: " +
                           aggregateManualTransform +
                           ".  Inspection start: " +
                           inspectionStartTimeInMillis);
      }

      if ((_baselineNumberOfDefectiveJoints == _UNINITIALIZED_VALUE) &&
          (_baselineNumberOfDefectiveComponents == _UNINITIALIZED_VALUE))
      {
        _baselineNumberOfDefectiveJoints = Math.max(pinsDefective, 1);
        _baselineNumberOfDefectiveComponents = Math.max(componentsDefective, 1);
      }
      else if ((_baselineNumberOfDefectiveJoints != _UNINITIALIZED_VALUE) &&
               (_baselineNumberOfDefectiveComponents != _UNINITIALIZED_VALUE))
      {
        double defectiveJointDeviationFractionFromBaseline =
            (pinsDefective - _baselineNumberOfDefectiveJoints) / _baselineNumberOfDefectiveJoints;
        double defectiveComponentDeviationFractionFromBaseline =
            (componentsDefective - _baselineNumberOfDefectiveComponents) / _baselineNumberOfDefectiveComponents;

        if ((defectiveJointDeviationFractionFromBaseline > _MAX_ALLOWABLE_CALL_RATE_DEVIATION_FRACTION_FROM_BASELINE) ||
            (defectiveComponentDeviationFractionFromBaseline > _MAX_ALLOWABLE_CALL_RATE_DEVIATION_FRACTION_FROM_BASELINE))
        {
          // We have a spike!
          // Write it to the log, and save the alignment images.
          System.out.println("[mdw] Call rate spike!  Inspection start: " + inspectionStartTimeInMillis);

          try
          {
            Alignment.getInstance().saveCallRateSpikeAlignmentImages(_project.getTestProgram(), inspectionStartTimeInMillis);
          }
          catch (DatastoreException dex)
          {
            displayError(dex);
          }
        }
      }
      else
      {
        // Shouldn't reach this point...must be a bug elsewhere in my code.
        Assert.expect(false, "Baseline defect levels in bad state!");
      }
    }

    // special case -- the IAS inspected at least one view, but wasn't able to finish processing that view
    // so, I'll reset the atleastOneViewProcessed back to false so the user isn't told the test passed
    if (pinsProcessed == 0)
      _atLeastOneImageWasInspected = false;

    if ((pinsDefective > 0) || (componentsDefective > 0))
      _lastTestPass = false;
    else
      _lastTestPass = true;

//    /** determine our Review Defects mode (cut for genesis 1) */
//    if (false)//_testMode.isPauseAfterBoardInspectionEnabled() && _testExecution.didTestCompleteWithValidResults())
//    {
//      if ((pinsDefective > 0) && (pinsDefective >= _autoReviewDefectsThreshold))
//      {
//        _abortButton.setEnabled(false);
//        _currentActionLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_RUNNING_REVIEW_DEFECTS_KEY"));
//        /** @todo Run Review Defects HERE!!!!!! */
//      }
//      else
//      {
//        // no defects, so we didn't run review defects.  Time to continue testing
//        _abortButton.setEnabled(true);
//
//        _testExecution.inspectNextBoardAfterPause();
//      }
//    }
  }

  /**
   * Tells the user the test has complete.
   * This is only called from Test Development Mode (bypass and production don't do this).
   * @author Andy Mechtenberg
   */
  void notifyUserTestIsComplete()
  {
    _currentActionLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_TEST_COMPLETE_KEY"));
    _abortButton.setEnabled(false);
    _futureSerialNumberTextField.setEnabled(false); //Siew Yeng - XCR-2034
  }

  /**
   * @author Andy Mechtenberg
   */
  private void notifyUserWaitingForSerialNumber()
  {
    _currentActionLabel.setText(_WAITING_FOR_SERIAL_NUMBER_MESSAGE);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void notifyUserSerialNumberAccepted()
  {
    _currentActionLabel.setText(StringLocalizer.keyToString("TESTEXEC_GUI_SERIAL_NUMBER_ACCEPTED_KEY"));
  }

  /**
   * This will block until the test has finshed.
   * @author Andy Mechtenberg
   */
  public void waitForTestToFinish()
  {
    if (_testInProgress)
    {
      _mainUI.setCursor(new Cursor(Cursor.WAIT_CURSOR));
      //wait for test to end
      while(_testInProgress)
      {
        try
        {
          Thread.sleep(100);
        }
        catch (InterruptedException ex)
        {
          // do nothing
        }
      }
      _mainUI.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void putInDisabledState(boolean disabled)
  {
    Assert.expect(EventQueue.isDispatchThread());

    if (disabled)
    {
      _abortButton.setEnabled(false);
      _futureSerialNumberTextField.setEnabled(false); //Siew Yeng - XCR-2034
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
    }
    else
    {
      _abortButton.setEnabled(true);
      _futureSerialNumberTextField.setEnabled(true);//Siew Yeng - XCR-2034
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
  }

  /**
   * All exits should use this method to exit.  This will perform all cleanups necessary
   * for a safe exit.
   * @author Andy Mechtenberg
   */
  public void exitTestExecution()
  {
    putInDisabledState(true);
    
    _panelHandler.setKeepOuterBarrierOpenForLoadingPanel(false);
    
    try
    {
      if (LeftOuterBarrier.getInstance().isOpen() || RightOuterBarrier.getInstance().isOpen())
      {
        _hardwareWorkerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _hardwareObservable.setEnabled(false);

              _panelHandler.closeAllOuterBarriersInParallel();

              _hardwareObservable.setEnabled(true);
            }
            catch (XrayTesterException e)
            {
              _mainUI.handleXrayTesterException(e);
            }
          }
        });
      }
    }
    catch (XrayTesterException e)
    {
      _mainUI.handleXrayTesterException(e);
    }
    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        waitForTestToFinish();
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            putInDisabledState(false);
            if (_mainUI.getCurrentUserType().equals(UserTypeEnum.OPERATOR))
            {
              _mainUI.logout();
            }
            else
            {
              /** changes to force ESC to exit production mode */
              _mainUI.enableMainToolBar();
              notifyUserTestIsComplete();
//              _mainUI.getMainToolBar().switchToAXIHome();
            }

            /** @todo MDW - remove later.  Debug code. */
            if (_FLAG_CALL_RATE_SPIKES)
            {
              if (_callRateDataWriter != null)
              {
                _callRateDataWriter.close();
                _inspectionEngine.closeFailedJointsAndComponentsCSVFileWriter();
              }
            }
          }
        });
      }
    });
    //XCR-3364
    ProjectDatabase.setContinueWithoutDatabase(false);
  }

//  /**
//   * Puts the lightstack to yellow.  Typically used right before asking the user
//   * a question with a dialog box.  Yellow on the stack means the machine is paused
//   * waiting for some input before continuing.
//   * @author Andy Mechtenberg
//   */
//  void changeLightStackToStandby()
//  {
//    // If the Digital io server is down or hung, this call may take a long time before
//    // timing out.  Therefore, I'll put it on a worker thread, but I don't really care to wait
//    // until it completes.  Normally, this will work right away
//    _hardwareWorkerThread.invokeLater(new Runnable()
//    {
//      public void run()
//      {
//        try
//        {
//          _lightStack.standyMode();
//        }
//        catch (final XrayTesterException ex)
//        {
//          SwingUtils.invokeLater(new Runnable()
//          {
//            public void run()
//            {
//              displayError(ex);
//            }
//          });
//        }
//      }
//    });
//  }

  /**
   * Puts the lightstack to green.
   * @author Bill Darbie
   */
  void changeLightStackToInspectionInProgess()
  {
    if (_testInProgress)
    {
      // If the Galil1 server is down or hung, this call may take a long time before
      // timing out.  Therefore, I'll put it on a worker thread, but I don't really care to wait
      // until it completes.  Normally, this will work right away
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _lightStack.hardwareInUseMode();
          }
          catch (final XrayTesterException ex)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                displayError(ex);
              }
            });
          }
        }
      });
    }
  }

  /**
   * Puts the lightstack to red.
   * @author Bill Darbie
   */
  void changeLightStackToError()
  {
    // If the Digital io server is down or hung, this call may take a long time before
    // timing out.  Therefore, I'll put it on a worker thread, but I don't really care to wait
    // until it completes.  Normally, this will work right away
    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _lightStack.errorMode();
          
          //Khaw Chek Hau - XCR2654: CAMX Integration
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
          {
            try
            {
              _shopFloorSystem.equipmentError();
              _shopFloorSystem.equipmentAlarm();
            }
            catch (DatastoreException ex)
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                      ex.getLocalizedMessage() + StringLocalizer.keyToString("SHOP_FLOOR_ERROR_MESSAGE_KEY_KEY"),
                      StringLocalizer.keyToString("SHOP_FLOOR_EVENT_TITLE_KEY"),
                      true);
            }
          }
          
          //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT) && _machineUtilizationReportSystem.isFaultExist() == false)
          {
            try
            { 
              _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.MACHINE_ERROR_HAPPEN);  
            }
            catch (DatastoreException ex)
            {
              DatastoreException datastoreException = (DatastoreException) ex;
              MessageInspectionEvent event = new MachineUtilizationReportMessageInspectionEvent(datastoreException);
              InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
              inspectionEventObservable.sendEventInfo(event);
            }
          }
          
          //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT) && _vOneMachineStatusMonitoringSystem.isFaultExist() == false)
          {
            try
            {
              _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.MACHINE_ERROR_START);
            }
            catch (DatastoreException ex)
            {
              DatastoreException datastoreException = (DatastoreException) ex;
              MessageInspectionEvent event = new VOneMachineStatusMonitoringMessageInspectionEvent(datastoreException);
              InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
              inspectionEventObservable.sendEventInfo(event);
            }
          }
        }
        catch (final XrayTesterException ex)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              displayError(ex);
            }
          });
        }
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  void unloadPanelFromMachine(final TestResultsEnum testResultsEnum)
  {
    Assert.expect(testResultsEnum != null);

    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
      StringLocalizer.keyToString("TESTEXEC_GUI_WAITING_FOR_UNLOAD_PANEL_KEY"),
      StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
      true);
    SwingUtils.setFont(busyCancelDialog, FontUtil.getMediumFont());
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.OVERRIDE_PANEL_EJECT_FOR_TEST_EXEC) == false)
            _testExecution.unloadPanelFromMachine(TestResultsEnum.NOT_TESTED);
//          _testExecution.unloadPanelFromMachine(testResultsEnum);
        }
        catch (final XrayTesterException ex)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              displayError(ex);
            }
          });
        }
        finally
        {
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }

          }
          busyCancelDialog.dispose();
        }
      }
    });
    busyCancelDialog.setVisible(true);
  }

  /**
   *
   */
  public void clearProjectInfo ()
  {
    _project = null;
    if(_currentTestAction instanceof ProductionFlow)
    {
      ProductionFlow productionFlow = (ProductionFlow)_currentTestAction;
      productionFlow.clearProjectInfo();
    }
    if (_testExecution != null)
    {
      _testExecution.clearProjectInfo();
    }
  }

  /**
   * Chin Seong, Kee, Manual Alignment fixed
   * @author Chong Wei Chin
   */
  void handleAlignmentFail(final MessageInspectionEvent event)
  {
    Assert.expect(_project != null);

    if (_mainUI.isPanelInLegalState() == false)
      return;

    if (_testInProgress) //Chin Seong Kee - Aboring the Machine from continue moving/doing next step, no need check abort button is enabled or not
    {
       // don't allow the user to abort twice
      _abortButton.setEnabled(false);
      _futureSerialNumberTextField.setEnabled(false); //Siew Yeng - XCR-2034
//      addToInformationPane(StringLocalizer.keyToString("TESTEXEC_GUI_TEST_ABORTING_KEY") + "\n");
//      _currentTestAction.abort(); // Abort the machine
    }
    
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        boolean xoutFeature = false;
        if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
          xoutFeature = true;

        if(xoutFeature == false &&
          _testExecution.shouldPanelBeEjectedIfAlignmentFails() == true)
        {
          _inspectionEngine.continueWithAlignmentInput();
//          _inspectionEngine.feedbackUserInput();
          _abortButton.setEnabled(true);
          _futureSerialNumberTextField.setEnabled(true); //Siew Yeng - XCR-2034
          
          return;
        }

        FailAlignmentDialog failAlignmentDialog = new FailAlignmentDialog(_mainUI, StringLocalizer.keyToString("GUI_FAIL_ALIGNMENT_TITLE_KEY"), _project, event, xoutFeature);
        SwingUtils.centerOnComponent(failAlignmentDialog, _mainUI);
        failAlignmentDialog.setVisible(true);
        if (failAlignmentDialog.isXoutAction())
        {
          _inspectionEngine.setCurrentBoardIsXout();
        }
        else if(failAlignmentDialog.isUnloadBoardAction())
        {
          _testExecution.setAlignmentFailAndUnloadPanel(true);
        }
        else if(failAlignmentDialog.isReAlignAction())
        {
          //Siew Yeng - XCR1757
          _testExecution.setAlignmentFailedAndRerunAlignment(true);
        }
        else
        {
          addToInformationPane(StringLocalizer.keyToString("TESTEXEC_GUI_TEST_ABORTING_KEY") + "\n");
          _currentTestAction.abort();
        }
        _inspectionEngine.continueWithAlignmentInput();
        _abortButton.setEnabled(true);
        _futureSerialNumberTextField.setEnabled(true); //Siew Yeng - XCR-2034
      }
    });
  }
  
   /**
   * @author Jack Hwee
   */
  void handleFalseCallTriggering(int pinsProcessed,
                                 int pinsDefective,
                                 String projectName,
                                 long inspectionStartTime)
  {
     if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.FALSE_CALL_MONITORING_ENABLED) == true)
     {
         Assert.expect(pinsDefective >= 0);
         Assert.expect(pinsProcessed >= 0);
         Assert.expect(inspectionStartTime > 0);
      
         if (isFalseCallTriggeringTriggered())
         {
           // XCR-3328 Allow False Call Triggering to Remove indictment.xml
           if (FalseCallMonitoring.getInstance().isAutoDeleteProductionResults())
           {
             while(ResultsProcessor.isResultProcessDone() == false)
             {
               try
               {
                 Thread.sleep(200);
               }
               catch(InterruptedException ie)
               {
                // do nothing
               }
             }
             
             ResultsProcessor.getInstance().enforceFalseCallMonitoringDeletionPolicy(projectName, inspectionStartTime);
           }
           
           triggerFalseCallTriggering(pinsProcessed, pinsDefective);
         }
     }
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  private void triggerFalseCallTriggering(int pinsProcessed,
                                          int pinsDefective) 
  {
    Assert.expect(pinsDefective >= 0);
    Assert.expect(pinsProcessed >= 0);
      
    _currentTestAction.abort();
    LocalizedString falseCallTriggeringWarningLS = null;
    int errorCode = FalseCallMonitoring.getFalseCallTriggeringError();
    float ppmPin = ((float) pinsDefective / (float) pinsProcessed) * 1000000;
    int defectNumber = 0;
      
    if (Config.getInstance().getIntValue(FalseCallTriggeringConfigEnum.DEFECT_THRESHOLD_TYPE) == FalseCallTriggeringConfigEnum.getDefectivePinsNumberThresholdType())
      defectNumber = pinsDefective;
    else if (Config.getInstance().getIntValue(FalseCallTriggeringConfigEnum.DEFECT_THRESHOLD_TYPE) == FalseCallTriggeringConfigEnum.getDefectivePPMPinThresholdType())
      defectNumber = Math.round(ppmPin); 
    
    if (errorCode == FalseCallMonitoring.getMaxErrorTriggerErrorCode())
    {
      falseCallTriggeringWarningLS = new LocalizedString("TESTEXEC_GUI_FALSE_CALL_TRIGGER_MAX_ERROR_ERROR_LOG_KEY", new Object[] { String.valueOf(defectNumber),
                                                                                                                                   String.valueOf(FalseCallMonitoring.getInstance().getMaximumErrorTrigger())});
    }
    else if (errorCode == FalseCallMonitoring.getConsecutiveCountTriggerErrorCode())
    {
      falseCallTriggeringWarningLS = new LocalizedString("TESTEXEC_GUI_FALSE_CALL_TRIGGER_CONSECUTIVE_COUNT_ERROR_LOG_KEY", new Object[] { String.valueOf(defectNumber),
                                                                                                                                           String.valueOf(FalseCallMonitoring.getInstance().getCurrentDynamicCallDefectThreshold()),
                                                                                                                                           String.valueOf(FalseCallMonitoring.getInstance().getConsecutiveCount())});
    }
    else if (errorCode == FalseCallMonitoring.getTotalSampleTriggerErrorCode())
    {
      falseCallTriggeringWarningLS = new LocalizedString("TESTEXEC_GUI_FALSE_CALL_TRIGGER_SAMPLE_TRIGGERING_ERROR_LOG_KEY", new Object[] { String.valueOf(defectNumber),
                                                                                                                                           String.valueOf(FalseCallMonitoring.getInstance().getCurrentDynamicCallDefectThreshold()),
                                                                                                                                           String.valueOf(FalseCallMonitoring.getInstance().getTotalSampleTriggering()),
                                                                                                                                           String.valueOf(FalseCallMonitoring.getInstance().getTotalInspection())});
    }

    Assert.expect(falseCallTriggeringWarningLS != null);

    String falseCallTriggeringWarning = StringLocalizer.keyToString(falseCallTriggeringWarningLS);
    writeFalseCallTriggeringLog(pinsProcessed, pinsDefective, Math.round(ppmPin), falseCallTriggeringWarning);
    
    JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                  StringLocalizer.keyToString(falseCallTriggeringWarningLS) + "\n" + StringLocalizer.keyToString("TESTEXEC_GUI_FALSE_CALL_TRIGGER_PROCEED_TO_ENGINEER_LOGIN_ERROR_MESSAGE_KEY"),
                                  StringLocalizer.keyToString("TESTEXEC_GUI_FALSE_CALL_TRIGGERING_DIALOG_KEY"),
                                  JOptionPane.WARNING_MESSAGE);
      
    if (FalseCallMonitoring.getInstance().isEnforceWriteRemark())
    {
      try
      {
        FalseCallMonitoring.getInstance().setFalseCallRemarkWrittenNeeded(true);
      }
      catch (DatastoreException dex)
      {
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      dex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                      JOptionPane.ERROR_MESSAGE);
      }
    }
      
	if (FalseCallMonitoring.getInstance().isEnforceAdministratorLogin())
      falseCallTriggeringLogin();

    exitTestExecution();
  }
  
   /**
   * @author Jack Hwee
   */
  public boolean isFalseCallTriggeringTriggered()
  {
      return  FalseCallMonitoring.getInstance().isFalseCallTriggeringTriggered();
  }
  
   /**
   * @author Jack Hwee
   */
  private void falseCallTriggeringLogin()
  {
      // get user accounts information
      _failFalseCallTriggeringLoginManager = FailFalseCallTriggeringLoginManager.getInstance();
      
      try
      {
        _failFalseCallTriggeringLoginManager.readUserAccounts();
      }
      catch (XrayTesterException xte)
      {
        _mainUI.handleXrayTesterException(xte);
      }

      _startAs = UserTypeEnum.LOGIN;

      if (_startAs.equals(UserTypeEnum.LOGIN))
      {
        login();
      }
  }
  
   /**
   * @author Jack Hwee
   */
  public void login()
  {
    // prompt for new login
    if (_failFalseCallTriggeringLoginManager.promptForLogIn(_mainUI))
    {
      _startAs = _failFalseCallTriggeringLoginManager.getUserTypeFromLogIn();
      
      Assert.expect(_startAs.equals(UserTypeEnum.DEVELOPER) || _startAs.equals(UserTypeEnum.ADMINISTRATOR));
      //setSchedulerState(true);
      //setUserEnvironment(_startAs);
    }
    else
    {
      // user can exit from login
     // makeActive(false);
      
      // user can decide not to exit
      login();
    }
  }
  
    
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  private void writeFalseCallTriggeringLog(int pinsProcessed, int pinsDefective, int ppmPin, String falseCallTriggeringWarning)
  {
    Assert.expect(pinsDefective >= 0);
    Assert.expect(pinsProcessed >= 0);

    String falseCallTriggeringLogFileName = FileName.getFalseCallTriggeringLogFullPath(FileName.getFalseCallTriggeringLogLatestFileVersion());
    String defectType;

    if (Config.getInstance().getIntValue(FalseCallTriggeringConfigEnum.DEFECT_THRESHOLD_TYPE) == FalseCallTriggeringConfigEnum.getDefectivePinsNumberThresholdType())
    {
      defectType = StringLocalizer.keyToString("CFGUI_DEFECT_CALL_TYPE_KEY");
    }
    else
    {
      defectType = StringLocalizer.keyToString("CFGUI_DEFECT_PPM_PIN_TYPE_KEY");
    }
      
    _falseCallTriggeringLogFile = new CSVFileWriterAxi(FalseCallMonitoring.getInstance().getFalseCallTriggeringLogColumnHeaderList());
    
    try 
    {
      _falseCallTriggeringLogFile.open(falseCallTriggeringLogFileName, true);

      // Log the call rate data.
      String timeStampString = StringUtil.convertMilliSecondsToTimeAndDate(_inspectionEngine.getInspectionStartTimeInMillis());
      String currentUserName = LoginManager.getInstance().getUserNameFromLogIn(); 

      java.util.LinkedList<String> dataList = new LinkedList<String>();
      dataList.addAll(Arrays.asList(timeStampString, 
                                    _project.getName(), 
                                    _currentSerialNumber, 
                                    defectType,           
                                    String.valueOf(pinsProcessed), 
                                    String.valueOf(pinsDefective), 
                                    String.valueOf(ppmPin), 
                                    StringLocalizer.keyToString(falseCallTriggeringWarning), 
                                    String.valueOf(FalseCallMonitoring.getInstance().getMaximumErrorTrigger()), 
                                    currentUserName, 
                                    _userInputRemarkForFalseCallTriggeringLog));
      
      _falseCallTriggeringLogFile.writeDataLine(dataList);
      FalseCallMonitoring.getInstance().setCSVFileWriterAxi(_falseCallTriggeringLogFile);
      FalseCallMonitoring.getInstance().setLogFileData(dataList);
      dataList.clear();
    } 
    catch (DatastoreException dex)
    {
      displayError(dex);
    }
    finally
    {
      _falseCallTriggeringLogFile.close();
    }
  }
  
   /**
   * @author Jack Hwee
   */
  public CSVFileWriterAxi getFalseCallTriggeringLogFile()
  {
      return _falseCallTriggeringLogFile;
  }
  
  /**
   * XCR-2173 - Display Production Results in table form in Production page.
   * This function adds production result data to Production Result table.
   * 
   * @author Ying-Huan.Chu
   */
  private void addToProductionResultTable()
  {
    //Khaw Chek Hau - XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
    if (_maximumNumberOfRowsToKeepComboBox.getSelectedItem() instanceof Integer)
    {
      if (_productionResultTable.getRowCount() >= _maximumNumberOfRowToKeep)
      {
        _productionResultTableModel.removeOldestProductionResult();
      }
    }
    
    _productionResultTableModel.addProductionResult(_productionResult);
    populateAverageOverallInspectionCallRate();
  }
  
  /**
   * XCR-2173 - Display Production Results in table form in Production page.
   * This function updates the maximum number of rows to keep so that the Production Result Table's maximum row count 
   * is the same as the value selected in the combo box.
   * 
   * @author Ying-Huan.Chu
   */
  private void maximumNumberOfRowsToKeepComboBox_actionPerformed()
  {
    //Khaw Chek Hau - XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
    if (_maximumNumberOfRowsToKeepComboBox.getSelectedItem() instanceof Integer)
    {
      _maximumNumberOfRowToKeep = (int)_maximumNumberOfRowsToKeepComboBox.getSelectedItem();
      while (_productionResultTable.getRowCount() > _maximumNumberOfRowToKeep)
      {
        _productionResultTableModel.removeOldestProductionResult();
      }
    }
    populateAverageOverallInspectionCallRate();
  }
  
  /**
   * XCR-2173 - Display Production Results in table form in Production page.
   * This function clears (removes all rows) in the Production Result Table.
   * @author Ying-Huan.Chu
   */
  private void clearAllProductionResultButton_actionPerformed()
  {
    _productionResultTableModel.removeAllProductionResult();
    populateAverageOverallInspectionCallRate();
  }

  /**
   * XCR-2173 - Display Production Results in table form in Production page.
   * This function prompts a FileChooserDialog and allows user to select the directory and file name for the CSV file.
   * @author Ying-Huan.Chu
   */
  private void exportProductionResultButton_actionPerformed()
  {
    //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
    if (_productionResultTableModel.getProductionResult().isEmpty())
    {
      MessageDialog.showErrorDialog(this, 
                                    StringLocalizer.keyToString("PRT_CSV_FILE_EXPORT_ERROR_MESSAGE_KEY"), 
                                    StringLocalizer.keyToString("PRT_CSV_FILE_EXPORT_ERROR_TITLE_KEY"), 
                                    true);
      
      return;
    }
    
    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    FileFilterUtil csvFileFilter = new FileFilterUtil(FileName.getCommaSeparatedValueFileExtension(), StringLocalizer.keyToString("RDGUI_GENERATE_REPORT_FILE_DESCRIPTION_KEY"));
    fileChooser.setFileFilter(csvFileFilter);
    if (fileChooser.showSaveDialog(this) != JFileChooser.APPROVE_OPTION)
    {
      return;
    }
    File selectedFile = fileChooser.getSelectedFile();
    String selectedFileName = selectedFile.getPath();
    String seelctedFileExtension = FileUtilAxi.getExtension(selectedFileName);
    if (seelctedFileExtension.equalsIgnoreCase(FileName.getCommaSeparatedValueFileExtension()) == false)
    {
      selectedFileName += FileName.getCommaSeparatedValueFileExtension();
    }
    exportProductionResultToCSVFile(selectedFileName);
  }
  
  /**
   * XCR-2173 - Display Production Results in table form in Production page.
   * This function exports the Production Result Table to CSV file.
   * @author Ying-Huan.Chu
   */
  private void exportProductionResultToCSVFile(String fileFullPath)
  {
    TableModelReportWriter reportWriter = new TableModelReportWriter();
    try
    {
      //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
      boolean displayPanelLoadingTime = Config.getInstance().getBooleanValue(SoftwareConfigEnum.TEST_EXEC_DISPLAY_PRODUCTION_PANEL_LOADING_AND_UNLOADING_TIME);

      if (displayPanelLoadingTime == false)
      {
        DefaultSortTableModel productionResultTableModelClone = new DefaultSortTableModel();
        String[] columnIdentifiers = _productionResultTableModel.getColumnIdentifiers();
        int panelLoadingTimeColumnIndex = _productionResultTableModel.getPanelLoadingTimeColumnIndex();
        int panelUnloadingTimeColumnIndex = _productionResultTableModel.getPanelUnloadingTimeColumnIndex();

        java.util.List<String> columnIdentifierList = new ArrayList<String>(Arrays.asList(columnIdentifiers));
        columnIdentifierList.remove(panelUnloadingTimeColumnIndex);
        columnIdentifierList.remove(panelLoadingTimeColumnIndex);
        columnIdentifiers = columnIdentifierList.toArray(new String[0]);

        productionResultTableModelClone.setColumnIdentifiers(columnIdentifiers);

        for (int row = 0; row < _productionResultTableModel.getRowCount(); row++)
        {
          java.util.List<String> dataRow = new ArrayList<String>();
          for (int col = 0; col < _productionResultTableModel.getColumnCount(); col++)
          {
            if (col == panelLoadingTimeColumnIndex || col == panelUnloadingTimeColumnIndex)
              continue;

            dataRow.add(String.valueOf(_productionResultTableModel.getValueAt(row, col)));
          }
          productionResultTableModelClone.addRow(dataRow.toArray());
        }

        reportWriter.writeCSVFile(fileFullPath, StringLocalizer.keyToString("PRT_CSV_FILE_REPORT_TITLE_KEY"), productionResultTableModelClone);
      }
      else
      {
        reportWriter.writeCSVFile(fileFullPath, StringLocalizer.keyToString("PRT_CSV_FILE_REPORT_TITLE_KEY"), _productionResultTableModel);
      }
      
      MessageDialog.showInformationDialog(this, 
                                          StringLocalizer.keyToString(new LocalizedString("PRT_CSV_FILE_EXPORT_SUCCESS_MESSAGE_KEY", new Object[]{fileFullPath})),
                                          StringLocalizer.keyToString("PRT_CSV_FILE_EXPORT_SUCCESS_TITLE_KEY"));
    }
    catch(IOException ioe)
    {
      MessageDialog.reportIOError(this, ioe.getLocalizedMessage());
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public String getCurrentVariation()
  {
    Assert.expect(_currentVariation != null);
    return _currentVariation;
  }
  
  /**
   * author weng-jian.eoh
   * @return 
   */
  private boolean isShowVersion()
  {
    boolean isShowVersion = true;
//    try
//    {
//      Config.getInstance().load(FileName.getCustomizeConfigurationFullPath(
//        Config.getInstance().getStringValue(SoftwareConfigEnum.CUSTOMIZE_SETTING_CONFIGURATION_NAME_TO_USE)));
//    }
//    catch (DatastoreException ex)
//    {
//      MessageDialog.showErrorDialog(_mainUI,
//                                     ex.getMessage(),
//                                     StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
//                                     true,
//                                     FontUtil.getMediumFont());
//    }
    boolean isCustomiseSettingOn = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_MAIN_UI);
    if (isCustomiseSettingOn == false )
    {
      return false;
    }
    else
    {
      isShowVersion = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_VERSION);
      return isShowVersion;
    }
  }
  
  /**
   * XCR-3436
   * @author Weng-Jian.eoh
   */
  private void updateProgramInfoPanel()
  {
    try
    {
      CustomizationSettingReader.getInstance().load(FileName.getCustomizeConfigurationFullPath(
        Config.getInstance().getStringValue(SoftwareConfigEnum.CUSTOMIZE_SETTING_CONFIGURATION_NAME_TO_USE)));
    }
    catch (DatastoreException ex)
    {
      MessageDialog.showErrorDialog(
        this,
        StringUtil.format(ex.getMessage(), 50),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    }
    
    if (_programInfoPanel != null)
    {
      _serialNumberImagePanel.remove(_programInfoPanel);
      _programInfoPanel.removeAll();
    }
    _programInfoPanel.setLayout(_programInfoGridLayout);
    _programInfoGridLayout.setColumns(2);
    if (isShowVersion() == true)
    {
      _programInfoGridLayout.setRows(4);
    }
    else 
      _programInfoGridLayout.setRows(3);
    
    _programInfoPanel.setBorder(BorderFactory.createLoweredBevelBorder());
    _serialNumberImagePanel.add(_programInfoPanel, BorderLayout.NORTH);
    _programInfoPanel.add(_serialNumberLabel, null);
    _programInfoPanel.add(_currentSerialNumberLabel, null);
    _programInfoPanel.add(_panelProgramLabel, null);
    _programInfoPanel.add(_currentPanelProgramLabel, null);
    _programInfoPanel.add(_boardLabel, null);
    _programInfoPanel.add(_currentBoardLabel, null);
    if (isShowVersion() == true)
    {
      _programInfoPanel.add(_programVersionLabel, null);
      _programInfoPanel.add(_currentProgramVersionLabel, null);
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
   */
  private void populateAverageOverallInspectionCallRate()
  {
    int averageOverallInspectionCallRate = 0;
    int overallPanelPinsProcessedNumber = 0;
    int overallPanelPinsDefectiveNumber = 0;
    
    java.util.List<ProductionResult> productionResults = _productionResultTableModel.getProductionResult();
            
    if (productionResults.isEmpty() == false)
    {
      for (ProductionResult productionResult : productionResults)
      {
        overallPanelPinsProcessedNumber = overallPanelPinsProcessedNumber + productionResult.getPanelPinsProcessed();
        overallPanelPinsDefectiveNumber = overallPanelPinsDefectiveNumber + productionResult.getPanelPinsDefective();
      }
      
      averageOverallInspectionCallRate = MathUtil.roundDoubleToInt(((double)overallPanelPinsDefectiveNumber/(double)overallPanelPinsProcessedNumber) * (double)1000000); 
    }
    
    _averageOverallInspectionCallRate.setText(String.valueOf(averageOverallInspectionCallRate));
  }

  /**
   * XCR3609: Production Future Serial Number Cursor Not Lock
   * @author Khaw Chek Hau
   */
  public void futureSerialNumberPanelRequestFocus()
  {
    boolean barCodeReadersEnabled = BarcodeReaderManager.getInstance().getBarcodeReaderAutomaticReaderEnabled();
    if (_futureSerialNumberPanel.isEnabled() && barCodeReadersEnabled == false && _demoMode.isDemoModeEnabled() == false)
    {
      _futureSerialNumberTextField.requestFocus();
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3761 : Skip generating 2D verification image if the panel is not unloaded
   */
  public void setPanelRemainsWithoutUnloadedFor2DVerificationImage(boolean isPanelRemainsWithoutUnloaded)
  {
    if (_project != null)
      _project.getTestProgram().setPanelRemainsWithoutUnloadedFor2DVerificationImage(isPanelRemainsWithoutUnloaded);
  }
}
