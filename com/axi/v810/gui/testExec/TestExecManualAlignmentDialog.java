package com.axi.v810.gui.testExec;

import java.awt.*;
import javax.swing.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 *
 * @author siew-yeng.phang
 */
public class TestExecManualAlignmentDialog extends JDialog
{
  private MainMenuGui _mainUI;
  private Frame _frame;
  private Alignment _alignment = null;
  private ImageManagerPanel _imagePanel;
  private AlignmentImagePanel _alignmentImagePanel;
  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  private TestExecution _testExecution;
  private XrayTesterException _exception;
  
  private JPanel _mainPanel = new JPanel(); 
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private JLabel _title = new JLabel();
  private JPanel _graphicEnginePanel = new JPanel();
  private JPanel _bottomPanel = new JPanel();
  private VerticalFlowLayout _bottomPanelVerticalFlowLayout = new VerticalFlowLayout();
  
  private JLabel _statusLabel = new JLabel();
  private JPanel _statusPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private FlowLayout _buttonPanelFlowLayout = new FlowLayout(FlowLayout.LEFT);
  
  private JPanel _innerButtonsPanel = new JPanel();
  private GridLayout _innerButtonsPanelGridLayout = new GridLayout();
  private JButton _cancelButton = new JButton();
  private JButton _nextButton = new JButton();
  
  private boolean _isPerformedRuntimeManualAlignment = false;
  private int _runTimeCount = 0;
  private java.util.List<AlignmentGroup> _alignmentGroups = Collections.synchronizedList(new LinkedList<AlignmentGroup>());

  private TestSubProgram _subProgram;
  
  /**
   * @author Phang Siew Yeng
   */
  public TestExecManualAlignmentDialog(Frame frame, String title, boolean modal, TestSubProgram subProgram)
  {
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);

    _frame = frame;
    _mainUI = MainMenuGui.getInstance();
    _testExecution = TestExecution.getInstance();
    _subProgram = subProgram;
    
    _imagePanel = new ImageManagerPanel(false);
    _alignmentImagePanel = _imagePanel.getAlignmentImagePanel();
    
    jbInit();
    pack();
  }

  /*
   * @author Siew Yeng
   */
  private void jbInit()
  {
    this.setPreferredSize(new Dimension(700, 700));
    getContentPane().add(_mainPanel);
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _graphicEnginePanel.setBorder(BorderFactory.createCompoundBorder(
        BorderFactory.createEtchedBorder(Color.white, new Color(165, 163, 151)),
        BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _graphicEnginePanel.setLayout(new BorderLayout(5, 0));
    
    _innerButtonsPanel.add(_nextButton, null);
    _innerButtonsPanel.add(_cancelButton, null);
    _innerButtonsPanel.setLayout(_innerButtonsPanelGridLayout);
    _cancelButton.setEnabled(false);
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        manualAlignmentCancelButton_actionPerformed(e);
      }
    });

    _nextButton.setEnabled(false);
    _nextButton.setText(StringLocalizer.keyToString("MMGUI_ALIGN_MANUAL_ALIGN_DONE_BUTTON_KEY"));
    _nextButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        manualAlignmentNextButton_actionPerformed(e);
      }
    });
    _innerButtonsPanelGridLayout.setHgap(15);
    _innerButtonsPanelGridLayout.setVgap(15);
    _innerButtonsPanel.setOpaque(false);
    
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    
    _graphicEnginePanel.add(_imagePanel);
    _buttonPanel.add(_innerButtonsPanel);
    
    _bottomPanel.setLayout(_bottomPanelVerticalFlowLayout);
    _statusPanel.add(_statusLabel);
    _bottomPanel.add(_title);
    _bottomPanel.add(_statusPanel);
    _bottomPanel.add(_innerButtonsPanel);

    _mainPanel.add(_graphicEnginePanel,BorderLayout.CENTER);
    _mainPanel.add(_bottomPanel,BorderLayout.SOUTH);
    _runTimeCount = 1;
    
    addWindowListener(new WindowAdapter()
    {
      public void windowClosing(WindowEvent we)
      {
        cancelAction();
      }
    });
    
    runAlignment(_subProgram);
  }
 
  /*
   * @author Siew Yeng
   */
  private void runAlignment(final TestSubProgram subProgram)
  {
    Assert.expect(subProgram != null);

    _alignment = Alignment.getInstance();
    
    SwingWorkerThread.getInstance3().invokeLater(new Runnable()
    {
      public void run()
      {
        final BooleanLock alignmentComfirmationLock = new BooleanLock(false);
        _exception = null;

        if(_subProgram.hasBoard()) //board-based alignment
        {
          _title.setText(StringLocalizer.keyToString(new LocalizedString("TESTEXEC_GUI_RUNTIME_MANUAL_ALIGNMENT_BOARD_INFO_KEY", 
                                                    new Object[] {_subProgram.getBoard().getName(),
                                                                  _subProgram.getPanelLocationInSystem().toString(),
                                                                  _subProgram.getMagnificationType().toString()
                                                                  })));
        }
        else
        {
          _title.setText(StringLocalizer.keyToString(new LocalizedString("TESTEXEC_GUI_RUNTIME_MANUAL_ALIGNMENT_PANEL_INFO_KEY", 
                                                    new Object[] {_subProgram.getPanelLocationInSystem().toString(),
                                                                  _subProgram.getMagnificationType().toString()
                                                                  })));
        }
        
        alignmentComfirmationLock.setValue(false);

        if(subProgram.isLowMagnification())
        {
          _imagePanel.displayAlignmentImagePanel();
          _alignmentImagePanel = _imagePanel.getAlignmentImagePanel();
        }

        if(subProgram.isHighMagnification())
        {
          _imagePanel.displayAlignmentImagePanelForHighMag();  
          _alignmentImagePanel = _imagePanel.getAlignmentImagePanelForHighMag();
        }
                    
        try
        {
          _alignmentGroups.clear();
          for (AlignmentGroup alignmentGroup : subProgram.getAlignmentGroups())
          {
            _alignmentGroups.add(alignmentGroup);
          }
          _mainUI.generateVerificationImagesForManualAlignment(subProgram, false);
          
          updateStatusLabel();
          _nextButton.setEnabled(true);
          _cancelButton.setEnabled(true);
        }
        catch (XrayTesterException ex)
        {
          _mainUI.handleXrayTesterException(ex);
          return;
        }

        _hardwareWorkerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              subProgram.setIsUsingRuntimeManualAlignmentTransform(true);
              _testExecution.runManualAlignment(subProgram);
            }
            catch (final AlignmentBusinessException ex1)
            {
              _exception = ex1;
              _isPerformedRuntimeManualAlignment = false;
              handleAlignmentError(ex1);
            }
            catch (final XrayTesterException ex)
            {
              _exception = ex;
              _isPerformedRuntimeManualAlignment = false;
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  MessageDialog.showErrorDialog(_mainUI,
                                                ex,
                                                StringLocalizer.keyToString("TESTEXEC_GUI_RUNTIME_MANUAL_ALIGNMENT_KEY"),
                                                true);
                }
              });
            }
            finally
            {                
              alignmentComfirmationLock.setValue(true);
            }
          }
        });

        try
        {
          alignmentComfirmationLock.waitUntilTrue();
        }
        catch (InterruptedException ex1)
        {
          Assert.logException(ex1);
        }
        finally
        {
          dispose();
        }
      }
    });
  }
  
  /**
   * @author Siew Yeng
   */
  private void handleAlignmentError(final XrayTesterException alignmentException)
  {
    //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
    TestProgram testProgram = _subProgram.getTestProgram();
    testProgram.setRealignPerformed(false);
    testProgram.setRerunningProduction(false);
    
    for (TestSubProgram subProgram : testProgram.getAllTestSubPrograms())
    {
      subProgram.setInspectionDone(false);
    }

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
         AlignmentFailedDialog alignmentFailedDialog = new AlignmentFailedDialog(_mainUI,
            StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
            _subProgram.getTestProgram().getProject(),
            alignmentException);
        SwingUtils.centerOnComponent(alignmentFailedDialog, _mainUI);
        alignmentFailedDialog.setVisible(true);
        dispose();
      }
    });
  }
  
  /**
   * @author Andy Mechtenberg
   */
  private void manualAlignmentNextButton_actionPerformed(ActionEvent e)
  {
    Point2D point = _alignmentImagePanel.getOffsetRelativeToPanelInNanoMeters();
    _alignment.imageManuallyAligned(point.getX(), point.getY());
    updateStatusLabel();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void manualAlignmentCancelButton_actionPerformed(ActionEvent e)
  {
    cancelAction();
  }
  
  /*
   * @author Siew Yeng
   */
  private void cancelAction()
  {
    _isPerformedRuntimeManualAlignment = false;
    _alignment.cancelManualAlignment();
  }
  
  /*
   * @author Siew Yeng
   */
  private void updateStatusLabel()
  {
    if(_alignmentGroups.isEmpty())
      return;
    
    String currentGroup = _alignmentGroups.remove(0).getName();

    LocalizedString currentAlignmentStatus = new LocalizedString("MMGUI_ALIGN_STATUS_KEY", new Object[]
        {currentGroup});
    _statusLabel.setText(StringLocalizer.keyToString(currentAlignmentStatus));
    
    _runTimeCount++;
    if(_runTimeCount == 3)
      _isPerformedRuntimeManualAlignment = true;
  }
  
  /*
   * @author Siew Yeng
   */
  public boolean isPerformedRuntimeManualAlignment()
  {
    return _isPerformedRuntimeManualAlignment;
  }
  
  /*
   * @author Siew Yeng
   */
  public boolean isExceptionOccured()
  {
    if(_exception != null)
      return true;
    
    return false;
  }
}
