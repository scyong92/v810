/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.gui.testExec;

import com.axi.v810.util.*;
import java.util.*;

/**
 *
 * @author Ying-Huan.Chu
 */
public class ProductionResult
{
  private String _projectName;
  private String _serialNumber;
  private int _panelPinsProcessed;
  private int _panelPinsDefective;
  private int _panelComponentsDefective;
  private String _inspectionStartTime;
  private String _inspectionTime;
  private String _inspectionStatus;

  //Khaw Chek Hau - XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
  private int _inspectionCallRate;

  //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
  private String _inspectionPanelLoadingTime;
  private String _inspectionPanelUnloadingTime;
  
  /**
   * @author Ying-Huan.Chu
   */
  public ProductionResult()
  {
    // do nothing
  }
  
  /**
   * @author Ying-Huan.Chu
   * @author Khaw Chek Hau
   * @XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
   * @XCR3774: Display load & unload time during production
   */
  public ProductionResult(String projectName, 
                          String serialNumber, 
                          int panelPinsProcessed, 
                          int panelPinsDefective, 
                          int panelComponentsDefective, 
                          String inspectionStartTime, 
                          String inspectionTime, 
                          String inspectionStatus, 
                          int inspectionCallRate,
                          String inspectionPanelLoadingTime,
                          String inspectionPanelUnloadingTime)
  {
    _projectName = projectName;
    _serialNumber = serialNumber;
    _panelPinsProcessed = panelPinsProcessed;
    _panelPinsDefective = panelPinsDefective;
    _panelComponentsDefective = panelComponentsDefective;
    _inspectionStartTime = inspectionStartTime;
    _inspectionTime = inspectionTime;
    _inspectionStatus = inspectionStatus;
    _inspectionCallRate = inspectionCallRate;
    _inspectionPanelLoadingTime = inspectionPanelLoadingTime;
    _inspectionPanelUnloadingTime = inspectionPanelUnloadingTime;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public String getProjectName()
  {
    return _projectName;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setProjectName(String projectName)
  {
    _projectName = projectName;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public String getSerialNumber()
  {
    return _serialNumber;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setSerialNumber(String serialNumber)
  {
    _serialNumber = serialNumber;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getPanelPinsProcessed()
  {
    return _panelPinsProcessed;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setPanelPinsProcessed(int panelPinsProcessed)
  {
    _panelPinsProcessed = panelPinsProcessed;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getPanelPinsDefective()
  {
    return _panelPinsDefective;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setPanelPinsDefective(int panelPinsDefective)
  {
    _panelPinsDefective = panelPinsDefective;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getPanelComponentsDefective()
  {
    return _panelComponentsDefective;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setPanelComponentsDefective(int panelComponentsDefective)
  {
    _panelComponentsDefective = panelComponentsDefective;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public String getInspectionStartTime()
  {
    return _inspectionStartTime;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setInspectionStartTime(String inspectionStartTime)
  {
    _inspectionStartTime = inspectionStartTime;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public String getInspectionTime()
  {
    return _inspectionTime;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setInspectionTime(String inspectionTime)
  {
    _inspectionTime = inspectionTime;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public String getInspectionStatus()
  {
    return _inspectionStatus;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
   */
  public void setInspectionCallRate(int inspectionCallRate)
  {
    _inspectionCallRate = inspectionCallRate;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
   */
  public int getInspectionCallRate()
  {
    return _inspectionCallRate;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setInspectionStatusAborted()
  {
    _inspectionStatus = StringLocalizer.keyToString("PRT_INSPECTION_STATUS_ABORTED_KEY");
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setInspectionStatusPassed()
  {
    _inspectionStatus = StringLocalizer.keyToString("PRT_INSPECTION_STATUS_PASSED_KEY");
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setInspectionStatusFailed()
  {
    _inspectionStatus = StringLocalizer.keyToString("PRT_INSPECTION_STATUS_FAILED_KEY");
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  public void setInspectionPanelLoadingTime(String inspectionPanelLoadingTime)
  {
    _inspectionPanelLoadingTime = inspectionPanelLoadingTime;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  public String getInspectionPanelLoadingTime()
  {
    return _inspectionPanelLoadingTime;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  public void setInspectionPanelUnloadingTime(String inspectionPanelUnloadingTime)
  {
    _inspectionPanelUnloadingTime = inspectionPanelUnloadingTime;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  public String getInspectionPanelUnloadingTime()
  {
    return _inspectionPanelUnloadingTime;
  }
}
