/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.gui.testExec;

/**
 *
 * @author ying-huan.chu
 */
public class ProductionResultComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  
  public static ProductionResultComparatorEnum PROJECT_NAME = new ProductionResultComparatorEnum(++_index);
  public static ProductionResultComparatorEnum SERIAL_NUMBER = new ProductionResultComparatorEnum(++_index);
  public static ProductionResultComparatorEnum PANEL_PINS_PROCESSED = new ProductionResultComparatorEnum(++_index);
  public static ProductionResultComparatorEnum PANEL_PINS_DEFECTIVE = new ProductionResultComparatorEnum(++_index);
  public static ProductionResultComparatorEnum PANEL_COMPONENTS_DEFECTIVE = new ProductionResultComparatorEnum(++_index);
  public static ProductionResultComparatorEnum INSPECTION_START_TIME = new ProductionResultComparatorEnum(++_index);
  public static ProductionResultComparatorEnum INSPECTION_TIME = new ProductionResultComparatorEnum(++_index);
  public static ProductionResultComparatorEnum INSPECTION_STATUS = new ProductionResultComparatorEnum(++_index);
  //Khaw Chek Hau - XCR3518: Show each inspection's call rate and overall average inspection call rate in production result table
  public static ProductionResultComparatorEnum INSPECTION_CALL_RATE = new ProductionResultComparatorEnum(++_index);
  //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
  public static ProductionResultComparatorEnum PANEL_LOADING_TIME = new ProductionResultComparatorEnum(++_index);
  public static ProductionResultComparatorEnum PANEL_UNLOADING_TIME = new ProductionResultComparatorEnum(++_index);
  
  /**
   * @author Laura Cormos
   */
  private ProductionResultComparatorEnum(int id)
  {
    super(id);
  }
}
