package com.axi.v810.gui.testExec;

import java.awt.*;
import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.testDev.TestDevPersistance;
import com.axi.v810.util.FileUtilAxi;

import com.axi.v810.gui.mainMenu.MainMenuGui;
import javax.swing.JOptionPane;
import com.axi.v810.util.*;

/**
 * This class handles all the persisted aspectes of the Main menu GUI.  Currently, that
 * is only frame size and position.  Other persistance objects may extend this class and
 * persist more specific information for their own GUIs
 * @author Andy Mechtenberg
 */
public class TestExecPersistance implements Serializable
{
  private int _splitPanePosition = -1;

  /**
   * @author Andy Mechtenberg
   */
  public TestExecPersistance()
  {
    // do nothing
  }

  /**
   * @author Andy Mechtenberg
   */
  public TestExecPersistance readSettings()
  {
    TestExecPersistance persistance = null;
    try
    {
      if (FileUtilAxi.exists(FileName.getTestExecPersistFullPath()))
        persistance = (TestExecPersistance) FileUtilAxi.loadObjectFromSerializedFile(FileName.getTestExecPersistFullPath());
      else
        persistance = new TestExecPersistance();
    }
    catch(DatastoreException de)
    {
      // do nothing..
      persistance = new TestExecPersistance();
    }

    return persistance;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void writeSettings()
  {
    try
    {
      FileUtilAxi.saveObjectToSerializedFile(this, FileName.getTestExecPersistFullPath());
    }
    catch (DatastoreException de)
    {
      // Assert.logException(de);
      String message = de.getLocalizedMessage();
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
              message,
              StringLocalizer.keyToString("TEGUI_ENV_NAME_KEY"),
              JOptionPane.WARNING_MESSAGE);

    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getSplitPanePosition()
  {
    return _splitPanePosition;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setSplitPanePosition(int splitPanePosition)
  {
    _splitPanePosition = splitPanePosition;
  }
}
