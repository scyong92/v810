/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.gui.testExec;

import com.axi.util.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 *
 * @author ying-huan.chu
 */
public class ProductionResultComparator implements Comparator<ProductionResult>
{
  private AlphaNumericComparator _alphaComparator;
  private ProductionResultComparatorEnum _comparingAttribute;
  private boolean _ascending;

  /**
   * @author Laura Cormos
   */
  public ProductionResultComparator(boolean ascending, ProductionResultComparatorEnum comparingAttribute)
  {
    Assert.expect(comparingAttribute != null);
    _ascending = ascending;
    _comparingAttribute = comparingAttribute;
    _alphaComparator = new AlphaNumericComparator(_ascending);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int compare(ProductionResult lhs, ProductionResult rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    String lhsString;
    String rhsString;
    
    if (_comparingAttribute.equals(ProductionResultComparatorEnum.PROJECT_NAME))
    {
      lhsString = lhs.getProjectName();
      rhsString = rhs.getProjectName();
      _alphaComparator.setAscending(_ascending);
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(ProductionResultComparatorEnum.SERIAL_NUMBER))
    {
      lhsString = lhs.getSerialNumber();
      rhsString = rhs.getSerialNumber();
      _alphaComparator.setAscending(_ascending);
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(ProductionResultComparatorEnum.PANEL_PINS_PROCESSED))
    {
      lhsString = String.valueOf(lhs.getPanelPinsProcessed());
      rhsString = String.valueOf(rhs.getPanelPinsProcessed());
      _alphaComparator.setAscending(_ascending);
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(ProductionResultComparatorEnum.PANEL_PINS_DEFECTIVE))
    {
      lhsString = String.valueOf(lhs.getPanelPinsDefective());
      rhsString = String.valueOf(rhs.getPanelPinsDefective());
      _alphaComparator.setAscending(_ascending);
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(ProductionResultComparatorEnum.PANEL_COMPONENTS_DEFECTIVE))
    {
      lhsString = String.valueOf(lhs.getPanelComponentsDefective());
      rhsString = String.valueOf(rhs.getPanelComponentsDefective());
      _alphaComparator.setAscending(_ascending);
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(ProductionResultComparatorEnum.INSPECTION_START_TIME))
    {
      lhsString = lhs.getInspectionStartTime();
      rhsString = rhs.getInspectionStartTime();
      _alphaComparator.setAscending(_ascending);
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(ProductionResultComparatorEnum.INSPECTION_TIME))
    {
      lhsString = lhs.getInspectionTime();
      rhsString = rhs.getInspectionTime();
      _alphaComparator.setAscending(_ascending);
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(ProductionResultComparatorEnum.INSPECTION_STATUS))
    {
      lhsString = lhs.getInspectionStatus();
      rhsString = rhs.getInspectionStatus();
      _alphaComparator.setAscending(_ascending);
      return _alphaComparator.compare(lhsString, rhsString);
    }
    //Khaw Chek Hau - XCR3518: Show each inspection's call rate and overall average inspection call rate in production result tables
    else if (_comparingAttribute.equals(ProductionResultComparatorEnum.INSPECTION_CALL_RATE))
    {
      lhsString = String.valueOf(lhs.getInspectionCallRate());
      rhsString = String.valueOf(rhs.getInspectionCallRate());
      _alphaComparator.setAscending(_ascending);
      return _alphaComparator.compare(lhsString, rhsString);
    }
    //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
    else if (_comparingAttribute.equals(ProductionResultComparatorEnum.PANEL_LOADING_TIME))
    {
      lhsString = lhs.getInspectionPanelLoadingTime();
      rhsString = rhs.getInspectionPanelLoadingTime();
      _alphaComparator.setAscending(_ascending);
      return _alphaComparator.compare(lhsString, rhsString);
    }
    //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
    else if (_comparingAttribute.equals(ProductionResultComparatorEnum.PANEL_UNLOADING_TIME))
    {
      lhsString = lhs.getInspectionPanelUnloadingTime();
      rhsString = rhs.getInspectionPanelUnloadingTime();
      _alphaComparator.setAscending(_ascending);
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else
    {
      Assert.expect(false, "Please add your new production result attribute to sort on.");
      return 0;
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setAscending(boolean ascending)
  {
    _ascending = ascending;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setProductionResultComparatorEnum(ProductionResultComparatorEnum productionResultComparatorEnum)
  {
    Assert.expect(productionResultComparatorEnum != null);
    _comparingAttribute = productionResultComparatorEnum;
  }
}
