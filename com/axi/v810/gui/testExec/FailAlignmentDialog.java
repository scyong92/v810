/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.testExec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;

/**
 *
 * @author chin-seong.kee
 *  Chin Seong, Kee, Manual Alignment fixed - this dialog will be shown up when the alginment FAIL in production
 */
public class FailAlignmentDialog extends EscapeDialog
{
  private static final int _REALIGN_IMAGE = 1;
  private static final int _XEDOUT_BOARD = 2;
  private static final int _UNLOAD_PANEL = 3;
  private String _messageException = null;
  private int _userChoice = _UNLOAD_PANEL;
  private Project _project;
  private JFrame _parent;
  private AlignmentBusinessException _alignException = null;
  private AlignmentFailedWaitForUserInputEvent _alignmentFailedEvent = null;
  private boolean _enableXout = false;

  /**
   * @param parent
   * @param title
   * @param project
   * @param ex
   * @param font
   */
  public FailAlignmentDialog(JFrame parent, String title, Project project, MessageInspectionEvent ex)
  {
    super(parent, title, true);

    Assert.expect(parent != null);
    Assert.expect(title != null);
    Assert.expect(project != null);
    Assert.expect(ex != null);

    _project = project;
    _parent = parent;
    _messageException = ex.getMessage();
    _alignmentFailedEvent = (AlignmentFailedWaitForUserInputEvent) ex;
    _alignException = (AlignmentBusinessException) _alignmentFailedEvent.getAlignmentException();
    _enableXout = false;

    jbInit();
    pack();
  }

  /**
   * @param parent
   * @param title
   * @param project
   * @param ex
   * @param font
   * @author Wei Chin
   */
  public FailAlignmentDialog(JFrame parent, String title, Project project, MessageInspectionEvent ex, boolean enableXout)
  {
    super(parent, title, true);

    Assert.expect(parent != null);
    Assert.expect(title != null);
    Assert.expect(project != null);
    Assert.expect(ex != null);

    _project = project;
    _parent = parent;
    _messageException = ex.getMessage();
    _alignmentFailedEvent = (AlignmentFailedWaitForUserInputEvent) ex;
    _alignException = (AlignmentBusinessException) _alignmentFailedEvent.getAlignmentException();

    _enableXout = enableXout;
    jbInit();
    pack();
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void jbInit()
  {
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    setResizable(false);  // do this so no frame icon is displayed

    JButton realignButton = new JButton(StringLocalizer.keyToString("GUI_REALIGN_BUTTON_KEY"));
    JButton XedoutButton = new JButton(StringLocalizer.keyToString("GUI_XEDOUT_BUTTON_KEY"));
    JButton cancelButton = new JButton(StringLocalizer.keyToString("GUI_UNLOAD_BUTTON_KEY"));
    JButton showImageButton = new JButton(StringLocalizer.keyToString("MMGUI_ALIGNMENT_SHOW_IMAGES_KEY"));

    XedoutButton.setMargin(new Insets(12, 100, 12, 100));
    cancelButton.setMargin(new Insets(12, 100, 12, 100));
    realignButton.setMargin(new Insets(12, 100, 12, 100));
    showImageButton.setMargin(new Insets(12, 100, 12, 100));

    showImageButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showImageDialog();
      }
    });

    XedoutButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _userChoice = _XEDOUT_BOARD;
        dispose();
      }
    });

    cancelButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        _userChoice = _UNLOAD_PANEL;
        dispose();
      }
    });

    realignButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _userChoice = _REALIGN_IMAGE;
        dispose();
      }
    });

    JPanel mainPanel = new JPanel(new BorderLayout(0, 5));
    JPanel buttonPanel = new JPanel(new BorderLayout(0, 5));
    JPanel innerMainButtonPanel = new JPanel(new BorderLayout(0, 5));
    innerMainButtonPanel.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
    JPanel innerTopButtonPanel = new JPanel(new VerticalFlowLayout());
    innerTopButtonPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
    //Siew Yeng - XCR1757
    innerTopButtonPanel.add(realignButton);
    if(_enableXout)
      innerTopButtonPanel.add(XedoutButton);
    JPanel innerBottomButtonPanel = new JPanel(new VerticalFlowLayout());
    innerBottomButtonPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
    innerBottomButtonPanel.add(cancelButton);
    innerBottomButtonPanel.add(showImageButton);
    innerMainButtonPanel.add(innerTopButtonPanel, BorderLayout.CENTER);
    innerMainButtonPanel.add(innerBottomButtonPanel, BorderLayout.SOUTH);
    buttonPanel.add(innerMainButtonPanel, BorderLayout.CENTER);

    JPanel exceptionMessagePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    exceptionMessagePanel.setBorder(BorderFactory.createLineBorder(Color.black, 1));
    JLabel label = new JLabel("<html><b>" + StringUtil.format(_messageException, 30, "<br>") + "</b></html>");
    exceptionMessagePanel.add(label);
    buttonPanel.add(exceptionMessagePanel, BorderLayout.WEST);

    JPanel centerPanel = new JPanel(new BorderLayout(0, 5));
    JPanel inputReportFilePanel = new JPanel(new BorderLayout(0, 5));
    inputReportFilePanel.setBorder(BorderFactory.createLineBorder(Color.black, 1));

    //ImageIcon icon = new ImageIcon(_alignException.getFailAlignmentImagePath());
    //XCR1707 - System crash if get null fail alignment image path - Siew Yeng
    ImageIcon icon;
    String failAlignmentImagePath = _alignException.getFailAlignmentImagePath();
    if(failAlignmentImagePath == null)
      icon = new ImageIcon();
    else
      icon = new ImageIcon(failAlignmentImagePath);
    
    JLabel failAlignmentImageLabel = new JLabel(icon, JLabel.CENTER);

    inputReportFilePanel.add(failAlignmentImageLabel, BorderLayout.CENTER);

    centerPanel.add(inputReportFilePanel, BorderLayout.NORTH);

    mainPanel.add(buttonPanel, BorderLayout.SOUTH);
    mainPanel.add(centerPanel, BorderLayout.CENTER);
    mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 5, 10));
    this.getContentPane().add(mainPanel, BorderLayout.CENTER);
    this.getRootPane().setDefaultButton(XedoutButton);
  }

  /**
   *
   */
  private void showImageDialog()
  {
    AlignmentImageDialog alignmentImageDialog = new AlignmentImageDialog(_parent,
      StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
      _project);
    SwingUtils.centerOnComponent(alignmentImageDialog, _parent);
    alignmentImageDialog.setVisible(true);
  }

  /**
   * @return
   */
  public int getUserInput()
  {
    return _userChoice;
  }

  /**
   * @return
   */
  public boolean isXoutAction()
  {
    return (_userChoice == _XEDOUT_BOARD);
  }

  /**
   * @return
   */
  public boolean isUnloadBoardAction()
  {
    return (_userChoice == _UNLOAD_PANEL);
  }

  /**
   * @return
   */
  public boolean isReAlignAction()
  {
    return (_userChoice == _REALIGN_IMAGE);
  }
}
