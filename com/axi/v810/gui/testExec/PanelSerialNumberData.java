package com.axi.v810.gui.testExec;

import java.util.*;

import com.axi.util.*;

/**
 * This class holds the full panel's worth of serial numbers
 * and the panel name assigned to them
 * @author Andy Mechtenberg
 */
public class PanelSerialNumberData
{
  private String _projectName;
  private List<String> _serialNumbers;
  private String _variationName;

  /**
   * Create an invalid set
   * @author Andy Mechtenberg
   */
  PanelSerialNumberData()
  {
    this("", new ArrayList<String>(), "");
  }

  /**
  * @author Kok Chun, Tan
  **/
  PanelSerialNumberData(String projectName, List<String> serialNumbers, String variationName)
  {
    Assert.expect(projectName != null);
    Assert.expect(serialNumbers != null);

    _projectName = projectName;
    _serialNumbers = new ArrayList<String>();
    _serialNumbers.addAll(serialNumbers);
    _variationName = variationName;
  }

  /**
   * @author Andy Mechtenberg
   */
  String getProjectName()
  {
    Assert.expect(_projectName != null);
    return _projectName;
  }

  /**
   * @author Andy Mechtenberg
   */
  List<String> getSerialNumbers()
  {
    Assert.expect(_serialNumbers != null);
    Assert.expect(_serialNumbers.size() > 0);
    return _serialNumbers;
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean isValid()
  {
    if (_serialNumbers.isEmpty())
      return false;
    else
      return true;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  String getVariationName()
  {
    Assert.expect(_variationName != null);
    
    return _variationName;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setVariationName(String name)
  {
    Assert.expect(name != null && name.isEmpty() == false);
    _variationName = name;
  }
}
