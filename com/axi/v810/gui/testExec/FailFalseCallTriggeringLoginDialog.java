/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.testExec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;

/**
 *
 * @author Jack Hwee
 *  
 */
public class FailFalseCallTriggeringLoginDialog extends EscapeDialog
{
  private static final int _REALIGN_IMAGE = 1;
  private static final int _XEDOUT_BOARD = 2;
  private static final int _UNLOAD_PANEL = 3;
  private String _messageException = null;
  private int _userChoice = _UNLOAD_PANEL;
  private Project _project;
  private JFrame _parent;
  private AlignmentBusinessException _alignException = null;
  private AlignmentFailedWaitForUserInputEvent _alignmentFailedEvent = null;

  private JPanel _dialogPanel;

  private JPanel _northPanel;
  private JLabel _warningLabel;
  private JLabel _promptLabel;

  private JPanel _centerPanel;
  private JPanel _labelPanel;
  private JLabel _userNameLabel;
  private JLabel _passwordLabel;
  private JPanel _entryPanel;
  private JTextField _userName;
  private JPasswordField _password;

  private JPanel _southPanel;
  private JPanel _buttonPanel;
  private JButton _okButton;
  private JButton _cancelButton;

  private boolean _ok = false;

  /**
   * @param parent
   * @param title
   * @param project
   * @param ex
   * @param font
   */
  public FailFalseCallTriggeringLoginDialog(JFrame parent)
  {
    super(parent, StringLocalizer.keyToString("TESTEXEC_GUI_FALSE_CALL_TRIGGERING_DIALOG_KEY"), true);

    Assert.expect(parent != null);
   
    _parent = parent;
    
    _dialogPanel = new JPanel(new BorderLayout(20, 10));
     _dialogPanel.setBorder(BorderFactory.createEmptyBorder(10, 30, 10, 30));
     getContentPane().add(_dialogPanel);

     _northPanel = new JPanel(new BorderLayout(10, 10));
     _dialogPanel.add(_northPanel, BorderLayout.NORTH);

     // caps lock warning label (nothing is seen if caps lock is off)
     _warningLabel = new JLabel(" ");
     _warningLabel.setForeground(Color.RED);
     _warningLabel.setFont(FontUtil.getBoldFont(_warningLabel.getFont(),Localization.getLocale()));
     _warningLabel.setHorizontalAlignment(SwingConstants.CENTER);
     _northPanel.add(_warningLabel, BorderLayout.NORTH);

     _promptLabel = new JLabel(StringLocalizer.keyToString("TESTEXEC_GUI_FALSE_CALL_TRIGGERING_LOGIN_DIALOG_MESSAGE_KEY"));
     _promptLabel.setFont(FontUtil.getBoldFont(_promptLabel.getFont(),Localization.getLocale()));
     _northPanel.add(_promptLabel, BorderLayout.CENTER);
     
     JLabel promptLabel2 = new JLabel(StringLocalizer.keyToString("TESTEXEC_GUI_FALSE_CALL_TRIGGERING_LOGIN_DIALOG_SECOND_MESSAGE_KEY"));
     promptLabel2.setFont(FontUtil.getBoldFont(promptLabel2.getFont(),Localization.getLocale()));
     _northPanel.add(promptLabel2, BorderLayout.SOUTH);

     _centerPanel = new JPanel();
     _centerPanel.setLayout(new BorderLayout(10, 10));
     _dialogPanel.add(_centerPanel, BorderLayout.CENTER);

     _labelPanel = new JPanel(new GridLayout(2, 1, 10, 10));
     _centerPanel.add(_labelPanel, BorderLayout.WEST);

     _userNameLabel = new JLabel(StringLocalizer.keyToString("LOGIN_USER_NAME_KEY"));
     _labelPanel.add(_userNameLabel);
     _passwordLabel = new JLabel(StringLocalizer.keyToString("LOGIN_PASSWORD_KEY"));
     _labelPanel.add(_passwordLabel);

     _entryPanel = new JPanel(new GridLayout(2, 1, 10, 10));
     _centerPanel.add(_entryPanel, BorderLayout.CENTER);

     _userName =  new JTextField();
     _userName.addActionListener(new java.awt.event.ActionListener()
     {
       public void actionPerformed(ActionEvent e)
       {
         textField_actionPerformed(e);
       }
     });
     _userName.addKeyListener(new KeyAdapter()
     {
       public void keyReleased(KeyEvent e)
       {
         someKeyReleased(e);
       }
     });
     _entryPanel.add(_userName);
     _password = new JPasswordField();
     _passwordLabel.setPreferredSize(_userNameLabel.getSize());
     _password.addActionListener(new java.awt.event.ActionListener()
     {
       public void actionPerformed(ActionEvent e)
       {
         textField_actionPerformed(e);
       }
     });
     _password.addKeyListener(new KeyAdapter()
     {
       public void keyReleased(KeyEvent e)
       {
         someKeyReleased(e);
       }
     });
     _entryPanel.add(_password);

     _southPanel = new JPanel(new BorderLayout());
     _dialogPanel.add(_southPanel, BorderLayout.SOUTH);

     _buttonPanel = new JPanel(new FlowLayout());
     _southPanel.add(_buttonPanel, BorderLayout.CENTER);

     _okButton = new JButton(StringLocalizer.keyToString("LOGIN_LOGIN_BUTTON_KEY"));
    
     _okButton.addActionListener(new java.awt.event.ActionListener()
     {
       public void actionPerformed(ActionEvent e)
       {
          _ok = true;
          dispose();
       }
     });
      
     _buttonPanel.add(_okButton);

//     _cancelButton = new JButton(StringLocalizer.keyToString("LOGIN_EXIT_BUTTON_KEY"));
//   
//     _cancelButton.addActionListener(new java.awt.event.ActionListener()
//     {
//       public void actionPerformed(ActionEvent e)
//       {
//           dispose();
//       }
//     });
//     
//     _buttonPanel.add(_cancelButton);
     setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
     pack();
     
     SwingUtils.centerOnComponent(this, parent);

     // there is a bug with getLockingKeyState that causes it not to work after a window is deactivated.
     // this is a workaround for that bug
     addWindowListener(new WindowAdapter()
     {
       public void windowActivated(WindowEvent e)
       {
         if (e.getID() == WindowEvent.WINDOW_ACTIVATED)
         {
           try
           {
             Robot robot = new Robot();
             robot.keyPress(KeyEvent.VK_CONTROL);
             robot.keyRelease(KeyEvent.VK_CONTROL);
             Thread.sleep(50);
             checkCapsLock();
           }
           catch (Exception ex)
           {
             // do nothing
           }

         }

       }
     });
     
     this.addKeyListener(new java.awt.event.KeyAdapter()
     {
        public void keyPressed(KeyEvent ke)
        {
          if (ke.getKeyCode() == KeyEvent.VK_ENTER)
          {
              _ok = true;
              dispose();
          }
        }
     });
   
//    jbInit();
//    pack();
  }

   /**
    * @author George Booth
    */
   public boolean showDialog(LoginInfo info)
   {
     checkCapsLock();
     _userName.setText(info.userName);
     _password.setText("");
     _ok = false;
     _userName.requestFocusInWindow();
     setVisible(true);
     if (_ok)
     {
       info.userName = _userName.getText();
       if (_password !=  null)
       {
         info.password = new String(_password.getPassword());
       }
     }
     return _ok;
   }
   
    /**
    * display caps-lock warning if needed
    * @author Jack Hwee
    */
   void checkCapsLock()
   {
     boolean capsLock = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);
     if (capsLock)
     {
       _warningLabel.setText(StringLocalizer.keyToString("LOGIN_CAPS_LOCK_WARNING_KEY"));
     }
     else
     {
       _warningLabel.setText(" ");
     }
   }
   
    /**
    * monitor the caps-lock key
    * @author George Booth
    */
   void someKeyReleased(KeyEvent e)
   {
     if (e.getKeyCode() == KeyEvent.VK_CAPS_LOCK)
     {
       checkCapsLock();
     }
   }
   
     /**
    * @author George Booth
    */
   public void textField_actionPerformed(ActionEvent e)
   {
     _ok = true;
     dispose();
   }
   
   

}
