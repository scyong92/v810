package com.axi.v810.gui.testExec;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.*;
import java.util.List;
import java.util.Map;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Locale;
import java.text.NumberFormat;

/**
 * A dialog to display any results processing errors
 * @author Andy Mechtenberg
 */
public class ResultsProcessingErrorDialog extends JDialog
{
  private Font _font = null;
  private JLabel _failedMessageLabel;
  private JTextArea _failureMessageTextArea;
  private JScrollPane _textAreaScrollPane = new JScrollPane();

  /**
   * @author Andy Mechtenberg
   */
  public ResultsProcessingErrorDialog(JFrame parent, String title)
  {
    super(parent, title, false);
    Assert.expect(parent != null);
    Assert.expect(title != null);

    jbInit();
    setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    pack();
  }

  /**
   * @author Andy Mechtenberg
   */
  public ResultsProcessingErrorDialog(JFrame parent, String title, Font font)
  {
    super(parent, title, false);
    Assert.expect(parent != null);
    Assert.expect(title != null);
    Assert.expect(font != null);

    _font = font;

    jbInit();
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    SwingUtils.setFont(this, _font);
    _failedMessageLabel.setFont(FontUtil.getBoldFont(_failedMessageLabel.getFont(),Localization.getLocale()));
    pack();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    JButton retryButton = new JButton(StringLocalizer.keyToString("TESTEXEC_GUI_RETRY_KEY"));

    retryButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        // first option, which is to retry
        ResultsProcessor.getInstance().userWantsToRetryFailedCopies();
        _failureMessageTextArea.setText(null);
        dispose();
      }
    });

    JButton ignoreButton = new JButton(StringLocalizer.keyToString("TESTEXEC_GUI_IGNORE_KEY"));
    ignoreButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        // first option, which is to retry
        ResultsProcessor.getInstance().userWantsToIgnoreFailedCopies();
        _failureMessageTextArea.setText(null);
        dispose();
      }
    });

    JPanel mainPanel = new JPanel(new BorderLayout(0, 10));
    JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    JPanel innerButtonPanel = new JPanel(new GridLayout(1, 2, 10, 0));
    innerButtonPanel.add(ignoreButton);
    innerButtonPanel.add(retryButton);
    buttonPanel.add(innerButtonPanel);
    mainPanel.add(buttonPanel, BorderLayout.SOUTH);

    _failedMessageLabel = new JLabel(StringLocalizer.keyToString("TESTEXEC_RESULT_PROCESSING_ERROR_KEY"));
    _failedMessageLabel.setFont(FontUtil.getBoldFont(_failedMessageLabel.getFont(),Localization.getLocale()));
    JPanel failedMessagePanel = new JPanel();
    failedMessagePanel.add(_failedMessageLabel);
    _failureMessageTextArea = new JTextArea();
    _failureMessageTextArea.setEditable(false);
    _failureMessageTextArea.setOpaque(true);
    _failureMessageTextArea.setFont(UIManager.getFont("Button.messageFont"));

    _textAreaScrollPane.getViewport().add(_failureMessageTextArea);
    _failureMessageTextArea.setColumns(60);
    _failureMessageTextArea.setRows(20);
    _failureMessageTextArea.setLineWrap(true);

    mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 10));

    this.getContentPane().add(mainPanel, BorderLayout.CENTER);
    mainPanel.add(_textAreaScrollPane, java.awt.BorderLayout.CENTER);
    mainPanel.add(failedMessagePanel, java.awt.BorderLayout.NORTH);
    this.getRootPane().setDefaultButton(retryButton);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void appendMessage(String message)
  {
    Assert.expect(message != null);
    _failureMessageTextArea.append(message);
  }

  /**
   * @author Andy Mechtenberg
   * @author Patrick Lacz
   */
  public void appendDatastoreExceptions(java.util.List<DatastoreException> listOfDatastoreExceptions)
  {
    Assert.expect(listOfDatastoreExceptions != null);

    String totalMessage = StringLocalizer.keyToString("TESTEXEC_RESULT_PROCESSING_ERROR_KEY") + "\n\n";
    appendMessage(totalMessage);
    for (DatastoreException datastoreException : listOfDatastoreExceptions)
    {
      appendMessage(datastoreException.getMessage() + "\n\n");
    }
  }


  /**
   * @author Patrick Lacz
   */
  public void setFailureList(Map<Pair<Target, InspectionInformation>, List<DatastoreException>> failureMap)
  {
    Assert.expect(failureMap != null);

    _failureMessageTextArea.setText(null);

    for (Map.Entry<Pair<Target,InspectionInformation>, List<DatastoreException>> entry : failureMap.entrySet())
    {
      Target failingTarget = entry.getKey().getFirst();
      InspectionInformation inspectionInformation = entry.getKey().getSecond();
      List<DatastoreException> listOfDatastoreExceptions = entry.getValue();

      String panelSerialNumber = inspectionInformation.getPanelSerialNumber();
      if (panelSerialNumber.length() == 0)
        panelSerialNumber = "";

      String timeStampString = StringUtil.convertMilliSecondsToTimeAndDate(inspectionInformation.getInspectionStartTime());
      LocalizedString localizedMessagePrefix = new LocalizedString("TESTEXEC_RESULT_PROCESSING_ERROR_WITH_DETAIL_KEY",
                                                        new Object[]{failingTarget.getParentRule().getName(),
                                                                     inspectionInformation.getProjectName(),
                                                                     panelSerialNumber,
                                                                     timeStampString});

      String messagePrefix = StringLocalizer.keyToString(localizedMessagePrefix);

      appendMessage(messagePrefix);
      for (DatastoreException datastoreException : listOfDatastoreExceptions)
      {
        appendMessage(datastoreException.getMessage() + "\n\n");
      }
    }
  }
}
