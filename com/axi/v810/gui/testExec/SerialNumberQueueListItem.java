package com.axi.v810.gui.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * Instances of this object are put into the serial number queue and they
 * need to be aware of how many of themselves it takes to fully define a
 * set of serial numbers for a panel
 * @author Andy Mechtenberg
 */
public class SerialNumberQueueListItem
{
  private static final String _SERIAL_NUMBER_INDENT = "    ";
  private String _serialNumber;
  private String _projectName;
  private int _totalNumberOfItemsForPanel;
  private int _itemNumber;  // board number 0 means it's the project name
  private String _variationName;

  /**
   * @param projectName the Name of the project
   * @param totalNumber the total number of list entries for this panel, including panel name
   * @author Andy Mechtenberg
   */
  SerialNumberQueueListItem(String projectName, int totalNumber)
  {
    this(projectName, "", 0, totalNumber);
  }

  /**
   * @author Kok Chun, Tan
   */
  SerialNumberQueueListItem(String projectName, String variationName, int itemNumber, int totalNumber)
  {
    this("", variationName, projectName, itemNumber, totalNumber);
  }

  /**
   * @param projectName the Project Name to display in the list
   * @param serialNumber a serial number for the project
   * @param itemNumber the board number for the display unit (0 == panel name)
   * @param totalNumber the total number of list entries for this panel, including panel name
   * @author Andy Mechtenberg
   */
  SerialNumberQueueListItem(String serialNumber, String variationName, String projectName, int itemNumber, int totalNumber)
  {
    Assert.expect(serialNumber != null);
    Assert.expect(variationName != null);
    Assert.expect(projectName != null);
    Assert.expect(itemNumber >= 0 && itemNumber < totalNumber);
    Assert.expect(totalNumber > 1);  // must be at least 2 (name and serial number)

    _serialNumber = serialNumber;
    _projectName = projectName;
    _itemNumber = itemNumber;
    _totalNumberOfItemsForPanel = totalNumber;
    _variationName = variationName;
  }

  /**
   * @param selectedIndex the index in the List of this item
   * @return a List of Integers for thos JList indexes corresponding to all the panel entries
   * @author Andy Mechtenberg
   */
  List getIndexesForAllPanelEntries(int selectedIndex)
  {
    List indexList = new ArrayList();
    int startIndex = selectedIndex - _itemNumber;
    for (int i = 0; i < _totalNumberOfItemsForPanel; i++)
    {
      Integer integer = new Integer(i + startIndex);
      indexList.add(integer);
    }
    return indexList;
  }

  /**
   * @return the project name for this entry, regardless of whether it is the project name or the serial number
   * @author Andy Mechtenberg
   */
  String getProjectName()
  {
    Assert.expect(_projectName != null);
    return _projectName;
  }

  /**
   * @return the project name for this entry, regardless of whether it is the project name or the serial number
   * @author Andy Mechtenberg
   */
  String getSerialNumber()
  {
    Assert.expect(_serialNumber != null);
    return _serialNumber;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  String getVariationName()
  {
    Assert.expect(_variationName != null);
    return _variationName;
  }

  /**
   * @return A string to display to the user for this list entry
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    if (_itemNumber == 0) // panel name
      return "\"" + _projectName + "\"";
    else if (_itemNumber == 1)
    {
      //Kok Chun, Tan - XCR-3290 - Do not show variation in future serial number table. So that, we can change to correct variation when project is loaded.
//      if (_variationName.isEmpty() == false)
//      {
//        return "\"" + _variationName + "\"";
//      }
//      return "\"" + VariationSettingManager.ALL_LOADED +"\"";
      return "";
    }
    else
      return _SERIAL_NUMBER_INDENT + _serialNumber;
  }
}



