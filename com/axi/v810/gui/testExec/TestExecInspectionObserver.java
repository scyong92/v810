package com.axi.v810.gui.testExec;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * This class handles the observer updates coming from a test inspection in progress
 * @author Andy Mechtenberg
 */
public class TestExecInspectionObserver implements Observer
{
  private TestExecPanel _testExecPanel;
  private int _currentImageNumber = 0;

  /**
   * @author Andy Mechtenberg
   */
  public TestExecInspectionObserver(TestExecPanel testExecPanel)
  {
    Assert.expect(testExecPanel != null);
    _testExecPanel = testExecPanel;
  }

 /**
  * This must by synchronous - do not make any calls back into the server on this thread
  * or you will get a deadlock situation
  *
  * @author Andy Mechtenberg
  */
  public synchronized void update(Observable observable, final Object object)
  {
    Assert.expect(object != null);
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        InspectionEvent inspectionEvent = (InspectionEvent)object;
        InspectionEventEnum eventEnum = inspectionEvent.getInspectionEventEnum();
        if (eventEnum.equals(InspectionEventEnum.INSPECTION_STARTED))
        {
          StartedInspectionEvent startedInspectionEvent = (StartedInspectionEvent)inspectionEvent;
          _currentImageNumber = 0;
          int totalImages = startedInspectionEvent.getNumberOfImagesToInspect();
          _testExecPanel.handleBeginInspection(totalImages);

        }
        else if (eventEnum.equals(InspectionEventEnum.DONE_INSPECTING_IMAGE))
        {
          _testExecPanel.handleImageDoneEvent(++_currentImageNumber);
        }

        if (inspectionEvent instanceof AlgorithmMessageInspectionEvent)
        {
          AlgorithmMessageInspectionEvent event = (AlgorithmMessageInspectionEvent)inspectionEvent;
          _testExecPanel.addAlgorithmMessageToInformationPane(event.getMessage());
        }
        else if (inspectionEvent instanceof IncompleteProgramAdjustmentInspectionEvent)
        {
          IncompleteProgramAdjustmentInspectionEvent event = (IncompleteProgramAdjustmentInspectionEvent)inspectionEvent;
          _testExecPanel.addToInformationPane(event.getMessage());
        }
        else if (inspectionEvent instanceof MessageInspectionEvent)
        {
          MessageInspectionEvent event = (MessageInspectionEvent)inspectionEvent;
          // Chin Seong, Kee, Manual Alignment fixed
          if (event instanceof AlignmentFailedWaitForUserInputEvent)
          {
            // Chin Seong, Kee, Manual Alignment fixed
            //AlignmentFailedWaitForUserInputEvent alginmentFaileEvent = (AlignmentFailedWaitForUserInputEvent)event;
            _testExecPanel.handleAlignmentFail(event);
          }
          else if (event instanceof AlignmentFailedMessageInspectionEvent)
          {
            // Chin Seong, Kee, Manual Alignment fixed - Move the add information to textexec pane inside here so that
            //                   when fail alignment, there WONT be show the message twice on the
            //                   text exec board.
            _testExecPanel.addToInformationPaneWithWarning(event.getMessage());
            // Save the failing alignment images.
            AlignmentFailedMessageInspectionEvent alignmentFailedEvent = (AlignmentFailedMessageInspectionEvent)event;
            AlignmentBusinessException alignmentException = alignmentFailedEvent.getAlignmentException();
            try
            {
              Alignment.getInstance().saveFailingAlignmentImages(alignmentException.getTestSubProgram(),
                                                                 System.currentTimeMillis());
            }
            catch (XrayTesterException xtex)
            {
              // There was a problem while saving the failing alignment images.  Display the error.
              MessageDialog.showErrorDialog(_testExecPanel,
                                            xtex,
                                            StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                                            true,
                                            FontUtil.getMediumFont());
            }
          }
          //Khaw Chek Hau - XCR2654: CAMX Integration
          else if (event instanceof ShopFloorMessageInspectionEvent)
          {
            String errorMsg = event.getMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("SHOP_FLOOR_ERROR_MESSAGE_KEY_KEY");
            MessageDialog.showErrorDialog(_testExecPanel,
                                          errorMsg,
                                          StringLocalizer.keyToString("SHOP_FLOOR_EVENT_TITLE_KEY"),
                                          true,
                                          FontUtil.getMediumFont());
          }
          //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
          else if (event instanceof MachineUtilizationReportMessageInspectionEvent)
          {
            String errorMsg = event.getMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_ERROR_MESSAGE_KEY");
            MessageDialog.showErrorDialog(_testExecPanel,
                                          errorMsg,
                                          StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_EVENT_TITLE_KEY"),
                                          true,
                                          FontUtil.getMediumFont());
          }
          //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
          else if (event instanceof VOneMachineStatusMonitoringMessageInspectionEvent)
          {
            String errorMsg = event.getMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_MESSAGE_KEY");
            MessageDialog.showErrorDialog(_testExecPanel,
                                          errorMsg,
                                          StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_EVENT_TITLE_KEY"),
                                          true,
                                          FontUtil.getMediumFont());
          }          
        }
        else if (eventEnum.equals(InspectionEventEnum.INSPECTION_COMPLETED))
        {
          CompletedInspectionEvent event = (CompletedInspectionEvent)inspectionEvent;
          int pinsProcessed = event.getNumberOfProcessedJoints();
          int pinsDefective = event.getNumberOfDefectiveJoints();
          int componentsDefective = event.getNumberOfDefectiveComponents();

          _testExecPanel.handleInspectionResults(pinsProcessed, pinsDefective, componentsDefective);

          TestExecutionTimer timer = event.getTestExecutionTimer();
          _testExecPanel.displayTestResultsDetails(timer);
        }
        //Khaw Chek Hau - XCR3774: Display load & unload time during production
        else if (eventEnum.equals(InspectionEventEnum.INSPECTION_PRODUCTION_RESULT_STATUS))
        {
          _testExecPanel.displayTestResultsStatus();
        }
        else if (eventEnum.equals(InspectionEventEnum.PANEL_LOADING_INTO_MACHINE))//(inspectionEvent instanceof PanelLoadingIntoMachineInspectionEvent)
        {
          _testExecPanel.handlePanelLoadingIntoMachine();
        }
        else if (eventEnum.equals(InspectionEventEnum.PANEL_LOADED_INTO_MACHINE))//(inspectionEvent instanceof PanelLoadedIntoMachineInspectionEvent)
        {
          _testExecPanel.handlePanelLoadedIntoMachine();
        }
        else if (eventEnum.equals(InspectionEventEnum.PANEL_UNLOADING_FROM_MACHINE)) //(inspectionEvent instanceof PanelUnloadingFromMachineInspectionEvent)
        {
          _testExecPanel.handlePanelUnloadingFromMachine();
        }
        else if (eventEnum.equals(InspectionEventEnum.PANEL_UNLOADED_FROM_MACHINE)) //(inspectionEvent instanceof PanelUnloadedFromMachineInspectionEvent)
        {
          _testExecPanel.handlePanelUnloadedFromMachine();
          //Khaw Chek Hau - XCR3761 : Skip generating 2D verification image if the panel is not unloaded
          _testExecPanel.setPanelRemainsWithoutUnloadedFor2DVerificationImage(false);
        }
        else if (eventEnum.equals(InspectionEventEnum.RUNTIME_ALIGNMENT_STARTED)) //(inspectionEvent instanceof PanelUnloadedFromMachineInspectionEvent)
        {
          _testExecPanel.handleRuntimeAlignmentStarted();
        }
        else if (eventEnum.equals(InspectionEventEnum.RUNTIME_SURFACE_MAP_STARTED))
        {
          SurfaceMapInspectionEvent surfaceMapInspectionEvent = (SurfaceMapInspectionEvent)inspectionEvent;
          int totalNumberOfPointToPointScans = surfaceMapInspectionEvent.getTotalNumberOfPointToPointScans();
            
          _testExecPanel.handleRuntimeSurfaceMapStarted(totalNumberOfPointToPointScans);
        }
        else if (eventEnum.equals(InspectionEventEnum.RUNTIME_ALIGNMENT_SURFACE_MAP_STARTED))
        {
          SurfaceMapInspectionEvent surfaceMapInspectionEvent = (SurfaceMapInspectionEvent)inspectionEvent;
          int totalNumberOfPointToPointScans = surfaceMapInspectionEvent.getTotalNumberOfPointToPointScans();
            
          _testExecPanel.handleRuntimeAlignmentSurfaceMapStarted(totalNumberOfPointToPointScans);
        }
        else if (eventEnum.equals(InspectionEventEnum.ALIGNED_ON_IMAGE)) //(inspectionEvent instanceof PanelUnloadedFromMachineInspectionEvent)
        {
          _testExecPanel.handleRuntimeAlignmentInProgress();
        }
        else if (eventEnum.equals(InspectionEventEnum.SURFACE_MAP_ON_OPTICAL_IMAGE))
        {
          _testExecPanel.handleRuntimeSurfaceMapInProgress();
        }
        else if (eventEnum.equals(InspectionEventEnum.ALIGNMENT_SURFACE_MAP_ON_OPTICAL_IMAGE))
        {
          _testExecPanel.handleRuntimeAlignmentSurfaceMapInProgress();
        }
      }
    });
  }
}
