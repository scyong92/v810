package com.axi.v810.gui;

/**
 * GUI tab enum for Cad, Image and Chart tab in TestDev Screen
 * @author swee-yee.wong
 */
//XCR-2348 Change Event that causes verification image warning
public class CadImageTabEnum extends GuiEventEnum
{
  private static int _index = -1;

  public static CadImageTabEnum CAD_WINDOW = new CadImageTabEnum(++_index);
  public static CadImageTabEnum IMAGE_WINDOW = new CadImageTabEnum(++_index);
  public static CadImageTabEnum CHART_WINDOW = new CadImageTabEnum(++_index);

  /**
   * @author swee-yee.wong
   */
  private CadImageTabEnum(int id)
  {
    super(id);
  }
}
