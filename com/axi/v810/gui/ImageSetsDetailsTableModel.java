package com.axi.v810.gui;

import java.text.*;
import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * <p>Title: ImageSetsDetailsTableModel</p>
 *
 * <p>Description: The table model for the Image Set Details Table.  </p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Erica Wheatcroft

 */
public class ImageSetsDetailsTableModel extends DefaultSortTableModel
{
  private final String[] _tableColumns = {StringLocalizer.keyToString("IMD_TABLE_PANEL_PROGRAM_NAME_COLUMN_KEY"),
                                          StringLocalizer.keyToString("IMD_TABLE_PANEL_PROGRAM_VERSION_COLUMN_KEY"),
                                          "Date Created",
                                          StringLocalizer.keyToString("IMD_TABLE_USER_DESCRIPTION_COLUMN_KEY"),
                                          StringLocalizer.keyToString("IMD_TABLE_TYPE_COLUMN_KEY"),
                                          StringLocalizer.keyToString("IMD_TABLE_POPULATED_COLUMN_KEY")};

   private List<ImageSetData> _imageSets = new ArrayList<ImageSetData>();

   private final String _YES = StringLocalizer.keyToString("GUI_YES_BUTTON_KEY");
   private final String _NO = StringLocalizer.keyToString("GUI_NO_BUTTON_KEY");

   private JSortTable _table = null;

   private ImageSetComparator _imageSetComparator = new ImageSetComparator();

  /**
   * @author Erica Wheatcroft
   */
  public ImageSetsDetailsTableModel(JSortTable table)
  {
    Assert.expect(table != null, "Table is null");
    _table = table;
  }

  /**
   * @author Erica Wheatcroft
   */
  public List<ImageSetData> getImageSets()
  {
    Assert.expect(_imageSets != null, "Image Set Data is null");
    return _imageSets;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void populateTableWithData(List<ImageSetData> imageSets)
  {
    Assert.expect(imageSets != null, "Image Set Data is null");

    _imageSets = imageSets;
    fireTableDataChanged();
  }

  /**
   * @return the number of columns in the model
   * @author Erica Wheatcroft
   */
  public int getColumnCount()
  {
    Assert.expect(_tableColumns != null);
    return _tableColumns.length;
  }

  /**
   * @param columnIndex the index of the column
   * @return the name of the column
   * @author Erica Wheatcroft
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return(String)_tableColumns[columnIndex];
  }

  /**
   * @return the number of rows in the model
   * @author Erica Wheatcroft
   */
  public int getRowCount()
  {
    if(_imageSets == null)
      return 0;

    return _imageSets.size();
  }

  /**
   * @param rowIndex the row whose value is to be queried
   * @param columnIndex the column whose value is to be queried
   * @return the value Object at the specified cell
   * @author Erica Wheatcroft
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_imageSets != null, "image set data is null");
    ImageSetData selectedImageSet = _imageSets.get(rowIndex);

    Assert.expect(selectedImageSet != null, "image set data for selected row is null");
    Object returnValue = null;

    switch(columnIndex)
    {
      case 0:
         returnValue = selectedImageSet.getProjectName();
        break;
      case 1:
        returnValue = selectedImageSet.getTestProgramVersionNumber();
        break;
      case 2:
        long dateInMils = selectedImageSet.getDateInMils();
        Date date = new Date(dateInMils);
        DateFormat dateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
        returnValue = dateFormat.format(date);
       break;

      case 3:
        returnValue = selectedImageSet.getUserDescription();
        break;
      case 4:
        returnValue = selectedImageSet.getImageSetTypeEnum().getName();
        break;
      case 5:
        if(selectedImageSet.isBoardPopulated())
          returnValue = _YES;
        else
          returnValue = _NO;
        break;
      default:
        Assert.expect(false, "invalid switch option");
    }

    Assert.expect(returnValue != null , "return value was not set");
    return returnValue;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void sortColumn(int col, boolean ascending)
  {
    _imageSetComparator.setAscending(ascending);

    if (col == 0)
    {
      // the user wants to sort the image set data by name
      _imageSetComparator.compareImageSetProjectNames();
    }
    else if (col == 1)
    {
      // the user wants to sort the image set data by version
      _imageSetComparator.compareImageSetVersions();
    }
    else if (col == 2)
    {
      // the user wants to sort the image set data by created date
      _imageSetComparator.compareImageSetDates();
    }
    else if (col == 3)
    {
      // the user wants to sort the image set data by comments
      _imageSetComparator.compareImageSetDescriptions();
    }
    else if (col == 4)
    {
      // the user wants to sort the image set data by type
      _imageSetComparator.compareImageSetTypes();
    }
    else if (col == 5)
    {
      // the user wants to sort the image set data by populated vs unpopulated
      _imageSetComparator.compareImageSetPopulated();
    }
    else
      Assert.expect(false, "invalid column number specified");

    // now sort the list
    Collections.sort(_imageSets, _imageSetComparator);

  }



  /**
   * Nothing is editable for the two tables so this will always return false.
   *
   * @param rowIndex the row whose value to be queried
   * @param columnIndex the column whose value to be queried
   * @return true if the cell is editable
   * @author Erica Wheatcroft
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(columnIndex >= 0, "column index is negative");

    if( columnIndex == 3) // the user can only edit column 4 everything else is not editable.
      return true;

    return false;

  }

  /**
   * @author Erica Wheatcroft
   */
  public void removeImageSet(ImageSetData imageSet)
  {
    Assert.expect(_imageSets != null, "Image set data is null");
    Assert.expect(_imageSets.contains(imageSet), "image set to remove is not found");
    _imageSets.remove(imageSet);
    fireTableStructureChanged();
  }

  /**
   * @param imageSets List of image sets to delete all at once.
   * @author Erica Wheatcroft
   */
  public void removeImageSets(List<ImageSetData> imageSets)
  {
    Assert.expect(_imageSets != null, "Image set data is null");
    Assert.expect(imageSets != null, "Image set data to delete is null");

    for(ImageSetData data : imageSets)
    {
      Assert.expect(_imageSets.contains(data), "Image set to delete does not exist");
      _imageSets.remove(data);
    }

    fireTableStructureChanged();
  }

  /**
   * @author Erica Wheatcroft
   */
  public int getImageSetRowIndex(ImageSetData imageSetToSelect)
  {
    Assert.expect(_imageSets != null, "Image set data is null");

    int index = -1;

    if(_imageSets.contains(imageSetToSelect))
    {
      index = _imageSets.indexOf(imageSetToSelect);
      Assert.expect(index >= 0, "Index is negative");

    }

    return index;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setValueAt(Object value, int rowIndex, int columnIndex)
  {
    Assert.expect(value != null, "value is null");
    Assert.expect(rowIndex >= 0, "Row Index is negative!");
    Assert.expect(columnIndex >= 0, "Column Index is negative!");

    ImageSetData imageSetData = _imageSets.get(rowIndex);

    if (columnIndex == 3)
    {
      String originalUserDescription = imageSetData.getUserDescription();
      String userDescription = (String)value;
      imageSetData.setUserDescription(userDescription);
      ImageSetInfoWriter writer = new ImageSetInfoWriter();
      try
      {
        // lets write out the new description
        writer.write(imageSetData);
      }
      catch (DatastoreException de)
      {
        imageSetData.setUserDescription(originalUserDescription);
        MainMenuGui.getInstance().handleXrayTesterException(de);
      }
    }
    else
      Assert.expect(false); // only columns 0 and 3 are editable.
  }


  /**
   * @return List of Image Set Data Objects that the user has currently selected.
   * @author Erica Wheatcroft
   */
  public List<ImageSetData> getSelectedImageSets()
  {
    Assert.expect(_table != null, "Table is null");

    List<ImageSetData> selectedImageSets = new ArrayList<ImageSetData>();

    int[] selectedRowIndices = _table.getSelectedRows();

    for(int index : selectedRowIndices)
      selectedImageSets.add((ImageSetData) _imageSets.get(index));

    return selectedImageSets;
  }
  
  /**
   * sheng-chuan
   */
  public void insertImageSetListData(ImageSetData imageSetData)
  {
    if(_imageSets.contains(imageSetData) == false)
    {
      _imageSets.add(imageSetData);
      fireTableRowsInserted(_imageSets.size(), _imageSets.size());
    }
  }
}
