package com.axi.v810.gui.cadCreator;

import java.awt.event.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.ChoiceInputDialog.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.business.*;
import com.axi.v810.business.cadcreator.CadCreatorSelectedAreaSettings;
import com.axi.v810.business.panelDesc.Board;
import com.axi.v810.business.panelDesc.BoardEventEnum;
import com.axi.v810.business.panelDesc.BoardType;
import com.axi.v810.business.panelDesc.JointTypeEnumAssignment;
import com.axi.v810.business.panelDesc.SideBoardType;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.DatastoreException;
import com.axi.v810.datastore.config.Config;
import com.axi.v810.datastore.config.SoftwareConfigEnum;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

import java.awt.*;
import java.util.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;
/**
 *
 * @author chin-seong.kee
 */
public class CadCreatorPanel extends JPanel implements Observer
{
  private final static int _CAD_CREATOR_PANEL = 0;
  private final static int _CAD_CREATOR_LANDPATTERN = 1;
  
  private final static String _CAD_GRAPHICS_ENGINE = "Cad";
  private final static String _LANDPATTERN_GRAPHICS_ENGINE = "Land Pattern";
  
  private V810CadCreatorEnvironment _cadCreatorEnv;
  private Project _project = null;
  
  private static GraphicsEngine _graphicsEngine = null;
  private static CreatePanelGraphicsEngineSetup _createPanelGraphicsEngineSetup = null;
 
  private static CadCreatorPanel _cadCreatorPanel;
  
  private static JSplitPane _contentPane;
  
  private JPanel _cadPanel;
  private JTabbedPane _graphicsEngineTabbedPane;
  
  private JPanel _defineBoardPanel;
  private LandPatternCreator _landPatternCreatorPanel;
  private ComponentCreator _componentCreatorPanel;
  
  private JPanel _defineBoardButtonPanel;
 
  private JTabbedPane _editCadTabbedPane = new JTabbedPane();
  private BorderLayout _editCadBorderLayout = new BorderLayout();
  
  private MainMenuGui _mainUI = MainMenuGui.getInstance();
  private PanelHandler _panelHandler = PanelHandler.getInstance();
  private TestProgram _newCadTestProgram = null;
  
  private JButton _duplicateBoardButton = null;
  private JButton _createBoardUsingSelectedAreaButton =  null;
  private JButton _deleteBoardButton = null;
  private JButton _doneButton = null;
  private JButton _saveProjButton = null;
 
  private com.axi.v810.gui.testDev.CreateCadPanelLayoutTable _createCadPanelLayoutTable;
  
  private JScrollPane _tablePanelLayoutScroll = new JScrollPane();
  
  private CreateNewCadPanel _panelCadPanel;
  private CreateNewCadPanel _landPatternPanel;
    
  private final int _CAD_CREATOR = 0;
  private final int _CAD_CREATOR_LAND_PATTERNS = 1;
  private final int _CAD_CREATOR_CREATE_COMPONENTS = 2;
  
  private boolean _isEmptyProject = true;

  /*
   * @author Kee Chin Seong 
   */
  private CadCreatorPanel()
  {
  }
  
  /*
   * @author Kee Chin Seong 
   */
  public void CadCreatorPanel(CreateNewCadPanel panelCadPanel)
  {
    Assert.expect(panelCadPanel != null);

    _panelCadPanel = panelCadPanel;
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
  
  /*
   * @author Kee Chin Seong 
   */
  public void setCadCreatorEnvironment(V810CadCreatorEnvironment env)
  {
    Assert.expect(env != null);
            
    _cadCreatorEnv = env;
  }  
 
  /*
   * @author Kee Chin Seong
   */
  private void updateBoardData(ProjectChangeEvent projectChangeEvent)
  {
    Assert.expect(projectChangeEvent != null);
    ProjectChangeEventEnum eventEnum = projectChangeEvent.getProjectChangeEventEnum();
    Assert.expect(eventEnum instanceof BoardEventEnum);
    final BoardEventEnum boardEventEnum = (BoardEventEnum)eventEnum;
    
    boolean createOrDestroy = false;
    
    if (boardEventEnum.equals(BoardEventEnum.CREATE_OR_DESTROY) || boardEventEnum.equals(BoardEventEnum.ADD_OR_REMOVE))
      createOrDestroy = true;

    if(_createCadPanelLayoutTable.isEditing())
    {
      _createCadPanelLayoutTable.cancelCellEditing();
    }
    
    if (createOrDestroy || boardEventEnum.equals(BoardEventEnum.NAME))
    {
      _createCadPanelLayoutTable.setData(_project);
      _createCadPanelLayoutTable.setPanelBasedAlignmentMethodSelected(true);
      _createCadPanelLayoutTable.fireTableDataChanged();
    }

    _createCadPanelLayoutTable.clearSelection();
    final Board board = (Board)projectChangeEvent.getSource();
    final int index = _createCadPanelLayoutTable.getIndexForBoard(board);
    
    if (index >= 0)
    {
      _createCadPanelLayoutTable.fireTableRowsUpdated(index, index);
      _createCadPanelLayoutTable.addRowSelectionInterval(index, index);
    }
  }
  
  @Override
  public void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        Assert.expect(observable != null);
        Assert.expect(argument != null);

        if (observable instanceof ProjectObservable)
        {
          ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)argument;
          ProjectChangeEventEnum projectChangeEventEnum = projectChangeEvent.getProjectChangeEventEnum();
          
          if (projectChangeEventEnum instanceof BoardEventEnum)
          {
            updateBoardData(projectChangeEvent);
          }
        }
        else
        {
          Assert.expect(false, "No update code for observable: " + observable.getClass().getName());
        }
      }
    });
  }
  
  /*
   * @author Kee Chin Seong 
   */
  private void jbInit()
  {
    setLayout(_editCadBorderLayout);
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

    _cadPanel = new JPanel(new GridLayout(1,1));
    _cadPanel.setPreferredSize(new Dimension(1000, 600));
    
    _graphicsEngineTabbedPane = new JTabbedPane();
    _graphicsEngineTabbedPane.setPreferredSize(new Dimension(1000, 600));
    
    _defineBoardPanel = new JPanel();
    _landPatternCreatorPanel = new LandPatternCreator(this);
    _componentCreatorPanel = new ComponentCreator(this);
    
    _defineBoardPanel.setLayout(new BorderLayout());
    
    _defineBoardPanel.setPreferredSize(new Dimension(600, 600));
    _landPatternCreatorPanel.setPreferredSize(new Dimension(600, 600));
    _componentCreatorPanel.setPreferredSize(new Dimension(600, 600));
      
    _editCadTabbedPane = new JTabbedPane();
    
    _editCadTabbedPane.add(_defineBoardPanel, StringLocalizer.keyToString("CC_DEFINE_BOARD_KEY"));
    _editCadTabbedPane.add(_landPatternCreatorPanel, StringLocalizer.keyToString("CC_LAND_PATTERN_CREATOR_KEY"));
    _editCadTabbedPane.add(_componentCreatorPanel, StringLocalizer.keyToString("CC_COMPONENT_CREATOR_KEY"));
    
    _editCadTabbedPane.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        editCadTabbedPane_stateChanged(e);
      }
    });
    
    _duplicateBoardButton = new JButton(StringLocalizer.keyToString("CC_DUPLICATE_BOARD_KEY"));
    _duplicateBoardButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        duplicateBoard_actionPerformed(); 
      }
    });
    
    _createBoardUsingSelectedAreaButton = new JButton(StringLocalizer.keyToString("CC_GENERATE_BOARD_KEY"));
    _createBoardUsingSelectedAreaButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        createBoard_actionPerformed(); 
      }
    });
    
    //Saving the progress of the CAD
    _saveProjButton = new JButton(StringLocalizer.keyToString("CC_SAVE_PROGRESS_KEY"));
    _saveProjButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveCadCreatorProject_actionPerformed(); 
      }
    });
    
    //Done Button - Compiling as complete program
    _doneButton = new JButton(StringLocalizer.keyToString("CC_NEXT_BUTTON_KEY"));
    _doneButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveAsCompleteProject_actionPerformed();
      }
    });
    
    _deleteBoardButton = new JButton(StringLocalizer.keyToString("CC_DELETE_BOARD_KEY"));
    _deleteBoardButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteBoard_actionPerformed(); 
      }
    });
    
    ListSelectionListener listSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent event)
      {
        boardSelectionUpdated(event);
      }
    };
    JPanel mainPanel = new JPanel();
    JPanel topPanel = new JPanel();
    
    mainPanel.setLayout(new BorderLayout());
    topPanel.setLayout(new PairLayout());
    
    topPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
                                                                           new Color(142, 142, 142)),
                                                                           StringLocalizer.keyToString("CC_SAVE_PROGRESS_KEY")),
                                                                           BorderFactory.createEmptyBorder(0, 5, 10, 5)));
    
    topPanel.add(_saveProjButton);
    topPanel.add(_doneButton);
    
    mainPanel.add(topPanel, BorderLayout.NORTH);
    
    _createCadPanelLayoutTable = new com.axi.v810.gui.testDev.CreateCadPanelLayoutTable();
    
    _tablePanelLayoutScroll.setBorder(null);
    _tablePanelLayoutScroll.setOpaque(false);
    
    _tablePanelLayoutScroll.getViewport().add(_createCadPanelLayoutTable.getTable(), null);
    _tablePanelLayoutScroll.getViewport().setOpaque(false);
    
    _createCadPanelLayoutTable.getSelectionModel().addListSelectionListener(listSelectionListener);
    
    //set up table row height based on the size of the font
    FontMetrics fm = getFontMetrics(getFont());
    int prefHeight = fm.getHeight() + MainMenuGui.TABLE_ROW_HEIGHT_EXTRA;
    
    // its okay to do set size here.. bill just doesn't like it on the whole panel.
    int headerHeight = prefHeight + 5;
    Dimension prefScrollSize = new Dimension(200, (prefHeight * 10) + headerHeight);  // 10 rows for tables
    _tablePanelLayoutScroll.setPreferredSize(prefScrollSize);
            
    _defineBoardButtonPanel = new JPanel();
    _defineBoardButtonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    _defineBoardButtonPanel.setLayout(new GridLayout(20, 20, 3, 3));
    
    _defineBoardButtonPanel.add(_createBoardUsingSelectedAreaButton);
    _defineBoardButtonPanel.add(_duplicateBoardButton);
    _defineBoardButtonPanel.add(_deleteBoardButton);
    
    _defineBoardPanel.add(_defineBoardButtonPanel, BorderLayout.CENTER);
    _defineBoardPanel.add(_tablePanelLayoutScroll, BorderLayout.NORTH);
     
    _contentPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, mainPanel, _graphicsEngineTabbedPane);
    _contentPane.setContinuousLayout(true);
        
    //add(_editCadTabbedPane, BorderLayout.CENTER);
    mainPanel.add(_editCadTabbedPane, BorderLayout.CENTER);
    add(_contentPane, BorderLayout.CENTER);

    _createCadPanelLayoutTable.setName(".createCadPanelLayoutTable");
    
    _landPatternPanel = new CreateNewCadPanel(null);
    
    //_cadPanel.setLayout(new CardLayout());
    
    _graphicsEngineTabbedPane.add(_panelCadPanel, _CAD_GRAPHICS_ENGINE);
    _graphicsEngineTabbedPane.add(_landPatternPanel, _LANDPATTERN_GRAPHICS_ENGINE);
    //_editCadTabbedPane.add(_componentCreatorPanel, "Component Creator");
    
    _graphicsEngineTabbedPane.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        editCadTabbedPane_stateChanged(e);
      }
    });
    //_cadPanel.add(_panelCadPanel, _CAD_CREATOR_PANEL);
    //_cadPanel.add(_landPatternPanel, _CAD_CREATOR_LANDPATTERN);
    
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void duplicateBoard_actionPerformed()
  {
    DuplicateBoardCountInputDialog dialog = new DuplicateBoardCountInputDialog(MainMenuGui.getInstance());
    dialog.showDialog();
    
    for(int i = 0; i < dialog.getBoardCount(); i ++)
       createBoard();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void deleteBoard_actionPerformed()
  {
    java.util.List<Board> boardsToDelete = _createCadPanelLayoutTable.getSelectedBoards();
    if (boardsToDelete.isEmpty())
      return;  
    
    com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_DELETE_BOARD_KEY"));
    try
    {
      for(Board boardToDelete : boardsToDelete)
        com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().execute(new RemoveBoardCommand(boardToDelete));
      
      if(BarcodeReaderManager.getInstance().getBarcodeReaderAutomaticReaderEnabled() &&
      SerialNumberMappingManager.getInstance().isSerialNumberMappingFileExists())
      {
        com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().execute(new EditSerialNumberMappingCommand(SerialNumberMappingManager.getInstance().getDefaultSerialNumberMappingDataList()));
        com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().execute(new EditSerialNumberMappingBarcodeConfigNameCommand(BarcodeReaderManager.getInstance().getBarcodeReaderConfigurationNameToUse()));
      }
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI,
                                    ex,
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
    }
    com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().endCommandBlock();
  }
          
  /**
   * @author Kee Chin Seong
   */
  public void createBoard_actionPerformed()
  {
    createBoard();
    
    _landPatternCreatorPanel.populateProject(_project);
    _componentCreatorPanel.populateProjectData(_project, _panelCadPanel);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void doneEditBoard_actionPerformed()
  {
    _editCadTabbedPane.setSelectedIndex(_CAD_CREATOR_LANDPATTERN);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void saveAsCompleteProject_actionPerformed()
  {
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
                                                             StringLocalizer.keyToString("CAD_SAVE_AS_COMPLETE_PROJECT_KEY"),
                                                             StringLocalizer.keyToString("CAD_SAVE_AS_COMPLETE_PROJECT_KEY"),
                                                             true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
    SwingWorkerThread.getInstance2().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
           assignAll();
           _project.convertCadCreatorProjectToV810Project();
        }
        catch(DatastoreException ex)
        {
        }
        finally
        {
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
                 // do nothing 
            }
          }
          busyCancelDialog.dispose();
        }
      }
    });
    busyCancelDialog.setVisible(true);  
  }
  
  
  /*
   * @author Kee Chin Seong
   */
  private void assignPinOrientationsPitchAndInterPadDistancesAndPadOneAndWidthLengthAndCoord()
  {
    for (com.axi.v810.business.panelDesc.LandPattern landPattern : _project.getPanel().getAllLandPatterns())
      landPattern.assignAll();
  }

  /*
   * @author Kee Chin Seong
   */
  private void assignCompPackages() throws DatastoreException
  {
    // load the rules for joint type assignment first
    JointTypeEnumAssignment.getInstance().load();
    for (com.axi.v810.business.panelDesc.ComponentType componentType : _project.getPanel().getComponentTypes())
    {
      componentType.assignOrModifyCompPackageAndSubtypeAndJointTypeEnums();
    }
  }

  /*
   * @author Kee Chin Seong
   */
  private void assignAlignmentGroups()
  {
    if(_project.getPanel().getPanelSettings().isRightAlignmentGroupsCreated() == false &&
       _project.getPanel().getPanelSettings().isLeftAlignmentGroupsCreated())
       _project.getPanel().getPanelSettings().assignAlignmentGroups();
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void assignAll() throws DatastoreException
  {
    // find ComponentTypes that should really be Fiducials
    // assign component packages
    assignCompPackages();
    // assign pad orientations, pitch and interpad distances
    assignPinOrientationsPitchAndInterPadDistancesAndPadOneAndWidthLengthAndCoord();
    // assign AlignmentGroups
    assignAlignmentGroups();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void saveCadCreatorProject_actionPerformed()
  {
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
                                                             StringLocalizer.keyToString("CC_SAVE_PROGRESS_KEY"),
                                                             StringLocalizer.keyToString("CC_SAVE_PROGRESS_KEY"),
                                                             true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
    SwingWorkerThread.getInstance2().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
           _project.saveCreatorProject();
        }
        catch(DatastoreException ex)
        {
           System.out.println("Unable to save the project");
        }
        finally
        {
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
                 // do nothing 
            }
          }
          busyCancelDialog.dispose();
        }
      }
    });
    busyCancelDialog.setVisible(true);  
  }
 
  
  /**
   * @author Kee Chin Seong
   */
  public void createBoard()
  {  
    if(CadCreatorSelectedAreaSettings.getInstance().getCadCreatorROI().getHeightInNanoMeter() <= 0)
    {
       MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                     "Board Is not Drawn Yet",
                                     StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                     true);
       return;
    }
    
    ProjectObservable.getInstance().setEnabled(false);
    int boardTypeNumber = 0;
         
    BoardType boardTypeToAdd = null;
    
    for(BoardType boardType : _project.getPanel().getBoardTypes())
    {
      if(boardType.getName().matches("boardType" + boardTypeNumber) == true)
      {
        boardTypeNumber ++; 
      }
    }
          
    boardTypeToAdd = new BoardType();
    boardTypeToAdd.setLengthInNanoMeters(CadCreatorSelectedAreaSettings.getInstance().getCadCreatorROI().getHeightInNanoMeter());
    boardTypeToAdd.setWidthInNanoMeters(CadCreatorSelectedAreaSettings.getInstance().getCadCreatorROI().getWidthInNanoMeter());
    boardTypeToAdd.setPanel(_project.getPanel());
    boardTypeToAdd.setName("boardType" + boardTypeNumber);  
          
    //Creating Top side board
    SideBoardType sideBoardType1 = new SideBoardType();
    boardTypeToAdd.setSideBoardType1(sideBoardType1);
    sideBoardType1.setBoardType(boardTypeToAdd);
    
    //Assuming bottom has bottom side too !
    SideBoardType sideBoardType2 = new SideBoardType();
    boardTypeToAdd.setSideBoardType2(sideBoardType2);
    sideBoardType2.setBoardType(boardTypeToAdd);
    
    int xPosInNanoMeters = (int)CadCreatorSelectedAreaSettings.getInstance().getDrawShape().getBounds2D().getX();
    int yPosInNanoMeters = (int)CadCreatorSelectedAreaSettings.getInstance().getDrawShape().getBounds2D().getY();
          
    ProjectObservable.getInstance().setEnabled(true);
          
    try
    {
       com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_CREATE_BOARD_KEY"));
       com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().execute(new CreateBoardCommand(_project.getPanel(),
                                                                               boardTypeToAdd,
                                                                               new PanelCoordinate(xPosInNanoMeters, yPosInNanoMeters),
                                                                               0,
                                                                               false));

       com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().endCommandBlock();
     }
     catch (XrayTesterException ex)
     {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex,
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
     }
     finally
     {
       GuiObservable.getInstance().stateChanged(null, GraphicsControlEnum.BOARD_NAME_VISIBLE);
     }
  }
  
 /**
   * @author Kee Chin Seong
   */
  public void switchToPanelCadPanel()
  {
    Assert.expect(_panelCadPanel != null);
    Assert.expect(_cadPanel != null);

    _graphicsEngineTabbedPane.setSelectedIndex(_CAD_CREATOR_PANEL);
  }
  
  
  /**
   * @author Kee Chin Seong
   */
  public void switchToLandPatternCadPanel()
  {
    Assert.expect(_panelCadPanel != null);
    Assert.expect(_cadPanel != null);

    _graphicsEngineTabbedPane.setSelectedIndex(_CAD_CREATOR_LANDPATTERN);

  }        
  
  /**
   * @author  Kee Chin Seong
   */
  private void boardSelectionUpdated(ListSelectionEvent event)
  {
    Assert.expect(event != null);

    _createCadPanelLayoutTable.setSelectedBoards(_createCadPanelLayoutTable.getSelectedBoards());
    GuiObservable.getInstance().stateChanged(_createCadPanelLayoutTable.getSelectedBoards(), SelectionEventEnum.BOARD_INSTANCE);

    if (_createCadPanelLayoutTable.getSelectedBoards().isEmpty())
      _deleteBoardButton.setEnabled(false);
    else
      _deleteBoardButton.setEnabled(true);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void populateWithProjectData(final Project project, boolean isEmptyProject)
  {
    Assert.expect(project != null);

    _project = project;    
    _isEmptyProject = isEmptyProject;
    
    _createCadPanelLayoutTable.setData(project);

    ProjectObservable.getInstance().addObserver(this);
    
    _editCadTabbedPane.setSelectedIndex(_CAD_CREATOR);
    
    switchToPanelCadPanel();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void unpopulate()
  {
    _project = null;
    
    if(_landPatternCreatorPanel.isReadyToFinish())
       _landPatternCreatorPanel.finish();
    
    _componentCreatorPanel.unpopulate();
    
    _isEmptyProject = true;
    
    ProjectObservable.getInstance().deleteObserver(this);
  }
  
   /**
   * @author chin-seong,kee
   */
  public void createCadCreatorPanel(CreateNewCadPanel panelCadPanel)
  {
    Assert.expect(panelCadPanel != null);

    _panelCadPanel = panelCadPanel;
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /*
   * @author Kee Chin Seong 
   */
  public static CadCreatorPanel getInstance()
  {
    if (_cadCreatorPanel == null)
    {
      _cadCreatorPanel = new CadCreatorPanel();
    }

    return _cadCreatorPanel;
  }
  
  /*
   * @author Kee Chin Seong 
   */
  public void setEnableSelectRegionToggleButton(boolean enable)
  {
    Assert.expect(_panelCadPanel != null);

    _panelCadPanel.setEnableSelectRegionToggleButton(enable);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void drawCadPanel(Project project)
  {
    Assert.expect(project != null);

    _project = project;
    _panelCadPanel.drawCadPanel(_project);
    _panelCadPanel.scaleDrawing();
    _panelCadPanel.setGroupSelectMode(false); 
  }
  
  /*
   * chin-seong, kee
   */
  public Project getProject()
  {
    Assert.expect(_project != null);
    return _project;
  }

  /*
   * @author Kee Chin Seong 
   */
  public CadCreatorPanel getCadCreatorPanel()
  {
    return this;
  }
  
  /*
   * @author Kee Chin Seong 
   */
  public void initializeGraphicsEngine()
  {
    if (_createPanelGraphicsEngineSetup == null)
    {
      _createPanelGraphicsEngineSetup = _panelCadPanel.getCreatePanelGraphicsEngineSetup();
    }

    if (_graphicsEngine == null)
    {
      _graphicsEngine = _createPanelGraphicsEngineSetup.getGraphicsEngine();
    }
    _graphicsEngine.invalidate();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void generateVerificationImgesForNewProjectFittingCad()
  {
    if(_project == null)
    {
     _project = Project.getCurrentlyLoadedProject();
     _project.setCheckLatestTestProgram(false);
    }
    
    _newCadTestProgram = VirtualLiveManager.getInstance().createTestProgramForVerificationImages(_project);
    
     SwingWorkerThread.getInstance().invokeLater(new Runnable()
     {
       public void run()
       {
         try
         {
           if(_isEmptyProject == true)
              _mainUI.performStartupIfNeeded();

           if (_panelHandler.isPanelLoaded() && 
              (_panelHandler.getLoadedPanelWidthInNanoMeters() != _project.getPanel().getWidthInNanoMeters() || 
               _panelHandler.getLoadedPanelLengthInNanoMeters() != _project.getPanel().getLengthInNanoMeters()||
               _panelHandler.getLoadedPanelProjectName().equalsIgnoreCase(_project.getName()) == false))
           {
             // Prompt Message to unload board when panel width and length not match
             JOptionPane.showMessageDialog(_mainUI,
                                           StringLocalizer.keyToString("VL_PANEL_NOT_MATCH_TO_GENERATE_VERIFICATION_IMAGES_KEY"),
                                           StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                           JOptionPane.ERROR_MESSAGE);
                
              HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
              {
                public void run()
                {
                  try
                  {
                    // Kok Chun, Tan - XCR-2766 - Panel clear extra delay is not take affect if board is loaded
                    if (Project.isCurrentProjectLoaded() && 
                        (_panelHandler.getLoadedPanelProjectName().equalsIgnoreCase(Project.getCurrentlyLoadedProject().getName())))
                    {
                      _panelHandler.unloadPanel(Project.getCurrentlyLoadedProject().getPanelExtraClearDelayInMiliSeconds());
                    }
                    else
                    {
                      _panelHandler.unloadPanel();
                    }
                  }
                  catch (XrayTesterException ex)
                  {
                    MainMenuGui.getInstance().handleXrayTesterException(ex);
                  }
                }
              });
            }
            else
            {
               for (TestSubProgram testSubProgram : _newCadTestProgram.getTestSubPrograms())
               {
                 try
                 {
                   _mainUI.generateVerificationImagesForManualAlignment(testSubProgram, false);  
                 }   
                 catch(XrayTesterException ex)
                 {
                   _mainUI.handleXrayTesterException(ex);
                 }     
               }
                
              displayVerificationImagesFittingCad();
           }
         } 
         catch (XrayTesterException ex) 
         {
            _mainUI.handleXrayTesterException(ex);
         }
         finally
         {
           _project.setCheckLatestTestProgram(true);
         }           
       }         
     });      
     _landPatternCreatorPanel.setLandPatternGraphicsPanel(_landPatternPanel);
     if(_isEmptyProject == false)
     {
       _landPatternCreatorPanel.populateProject(_project);
       _componentCreatorPanel.populateProjectData(_project, _panelCadPanel);
     }
   }
  
  /**
   * @author Kee Chin Seong
   * @param e 
   */
  public void displayVerificationImagesFittingCad()
  { 
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
                                                             StringLocalizer.keyToString("CAD_CREATOR_STITCH_VERIFICATION_IMAGES_MESSAGE_KEY"),
                                                             StringLocalizer.keyToString("CAD_CREATOR_STITCH_VERIFICATION_IMAGES_TITLE_KEY"),
                                                             true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
    SwingWorkerThread.getInstance2().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
           if(_newCadTestProgram == null)
              _newCadTestProgram = VirtualLiveManager.getInstance().createTestProgramForVerificationImages(_project);
          //if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) == false)
          //{
           ImageManager.getInstance().stitchVerificationImage(_project.getName(), true);
           _panelCadPanel.getCreatePanelGraphicsEngineSetup().loadVirtualLiveVerificationImage(_project.getName(), true);
          //}
        }
        catch (Exception e)
        {
        }
        finally
        {
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
                 // do nothing 
            }
          }
          busyCancelDialog.dispose();
        }
      }
    });
    busyCancelDialog.setVisible(true);
    
    _landPatternCreatorPanel.setLandPatternGraphicsPanel(_landPatternPanel);
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void editCadTabbedPane_stateChanged(ChangeEvent e)
  {
    if (_editCadTabbedPane.getSelectedIndex() == _CAD_CREATOR) // verify cad tab
    { 
      switchToPanelCadPanel();
    }
    else if (_editCadTabbedPane.getSelectedIndex() == _CAD_CREATOR_LAND_PATTERNS) // land pattern tab
    {
      _landPatternPanel.setEnableSelectRegionToggleButton(true);
      _landPatternCreatorPanel.populateProject(_project);
      _landPatternCreatorPanel.setPanelCadGraphicsEngineSetup(_createPanelGraphicsEngineSetup);
      _landPatternCreatorPanel.start();
    }
    else if(_editCadTabbedPane.getSelectedIndex() ==  _CAD_CREATOR_CREATE_COMPONENTS) //Create Component Tab
    {
      _componentCreatorPanel.populateProjectData(_project, _panelCadPanel);
      switchToPanelCadPanel();
    }
  }
}
