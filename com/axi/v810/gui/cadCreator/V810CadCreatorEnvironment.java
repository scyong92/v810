package com.axi.v810.gui.cadCreator;

import java.awt.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.guiUtil.ChoiceInputDialog.*;
import com.axi.guiUtil.*;
import com.axi.v810.business.cadcreator.CadCreatorSelectedAreaSettings;
import com.axi.v810.datastore.DatastoreException;
import com.axi.v810.datastore.Directory;
import com.axi.v810.gui.drawCad.*;


/**
 *
 * @author Chin-Seong.Kee
 */
public class V810CadCreatorEnvironment extends AbstractEnvironmentPanel
{
  private com.axi.v810.gui.undo.CommandManager _commandManager =
          com.axi.v810.gui.undo.CommandManager.getCadCreatorInstance();
  
  private TaskPanelScreenEnum _currentScreen = TaskPanelScreenEnum.NULL_SCREEN;
  //private TaskPanelScreenEnum _nextScreen;
  private boolean _environmentIsActive = false;
  
  private final static String _CAD_CREATOR_PANEL = "CadCreatorPanel";
  
  private String[] _buttonNames =
  {
    StringLocalizer.keyToString("CFGUI_SW_OPTIONS_BUTTON_LABEL_KEY")
  };
  private CreateNewCadPanel _panelCadPanel;
  public javax.swing.Timer _t = null;
  public javax.swing.Timer timer = null;
  public String _action = "display";
  public String _selectedSavedImageDir = "";
  private Project _project;
  
  private CadCreatorPersistance _guiPersistSettings = null;
  
  MainMenuGui _mainUI = null;
  
  public V810CadCreatorEnvironment(MainMenuGui mainUI, String projectName, CreateNewCadPanel panelCadPanel)
  {
    super(mainUI);
    Assert.expect(panelCadPanel != null);

    _mainUI = mainUI;
    _panelCadPanel = panelCadPanel;
    CadCreatorPanel.getInstance().createCadCreatorPanel(_panelCadPanel);
    CadCreatorPanel cadCreatorPanel = CadCreatorPanel.getInstance().getCadCreatorPanel();
    _centerPanel.add(cadCreatorPanel, _CAD_CREATOR_PANEL);
    
    //saving the environment object.
    cadCreatorPanel.setCadCreatorEnvironment(this);
    
    _mainCardLayout.first(_centerPanel);
    _currentScreen = TaskPanelScreenEnum.NULL_SCREEN;
  }

  /*
   * @uathor Kee Chin Seong
   */
  public com.axi.v810.gui.undo.CommandManager getCommandManager()
  {
    return _commandManager;
  }

  /*
   * @uathor Kee Chin Seong
   */
  MainUIToolBar getCreateCadToolBar()
  {
    return _envToolBar;
  }

  /*
   * @uathor Kee Chin Seong
   */
  public void populateWithProjectData(final Project project, boolean isEmptyProject)
  {
    _project = project;
    CadCreatorPanel.getInstance().populateWithProjectData(project, isEmptyProject);
  }

  /*
   * @uathor Kee Chin Seong
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * The environment is about to be exited and should perform any cleanup needed
   * @author Chin Seong , Kee
   */
  public void finish()
  {
    setCadCreatorPanelUIPersist();
     
    _environmentIsActive = false;
    finishCurrentTaskPanel();
    _currentScreen = TaskPanelScreenEnum.NULL_SCREEN;
    
    removeGraphicEngineObserver();

    unpopulate();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void unloadCurrentLoadedCadCreatorProject()
  {
    Assert.expect(_project != null);
    
    try
    {
      _project.destroyTestProgram();
      _project.unloadCurrentProject();
    }
    catch(DatastoreException ex)
    {
      System.out.println(ex.getMessage());
    }
    finally
    {
      _mainUI.getTestDev().unpopulate();
      unpopulate();
    }
  }


  public void navButtonClicked(String buttonName)
  {
    // throw new UnsupportedOperationException("Not supported yet.");
  }

  public void switchToScreen(TaskPanelScreenEnum newScreen)
  {
    // throw new UnsupportedOperationException("Not supported yet.");
      System.out.println("Switching Screeen");
  }
  
    /**
   * This will read in the persist data
   * @author Kee Chin Seong
   */
  private CadCreatorPersistance readPersistance()
  {
    if(_guiPersistSettings == null)
    {
      _guiPersistSettings = new CadCreatorPersistance();
    }
    _guiPersistSettings = (CadCreatorPersistance)_guiPersistSettings.readSettings();
    return _guiPersistSettings;
  }

  /**
   * @author Kee Chin Seong
   */
  public CadCreatorPersistance getPersistance()
  {
    if(_guiPersistSettings == null)
      readPersistance();
    
    return _guiPersistSettings;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void writePersistance()
  {
    if (_guiPersistSettings != null)
      _guiPersistSettings.writeSettings();
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setCadCreatorPanelUIPersist()
  {
    getPersistance();
    writePersistance();
  }

  /**
   * @author Kee Chin Seong - Make use of the switch task loading perisistant file
   */
  public void start()
  {
    if(_project != null)
    {      
      if(_panelCadPanel.isCadDrawed())
      {
        CadCreatorPanel.getInstance().initializeGraphicsEngine();
      }
      else
      {
        readPersistance();
        final BusyCancelDialog dlg = new BusyCancelDialog(
                _mainUI,
                StringLocalizer.keyToString("MM_GUI_LOADING_USER_INTERFACE_KEY"),
                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                true);
        dlg.pack();
        SwingUtils.centerOnComponent(dlg,_mainUI);
        dlg.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        _mainUI.setCursor(new Cursor(Cursor.WAIT_CURSOR));

        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            dlg.paint(dlg.getGraphics());
            CadCreatorPanel.getInstance().drawCadPanel(_project);
            CadCreatorPanel.getInstance().initializeGraphicsEngine();
            
            // wait until dlg is visible
            while (dlg.isVisible() == false)
            {
              try
              {
                Thread.sleep(200);
              }
              catch (InterruptedException ex)
              {
                // do nothing
              }
            }
            dlg.setVisible(false);
            _mainUI.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
          }
        });
        dlg.setVisible(true);
      }
      CadCreatorSelectedAreaSettings.getInstance().setShape(_project.getPanel().getShapeInNanoMeters()); 
    }
    addGraphicEngineObserver();
    
    //_panelCadPanel.getPanelGraphicsEngineSetup().getGraphicsEngine().setGroupSelectMode(false);
    CadCreatorPanel.getInstance().setEnableSelectRegionToggleButton(true);
    
    if(_project.areVerificationImagesValid(false) == false)
       CadCreatorPanel.getInstance().generateVerificationImgesForNewProjectFittingCad();
    else
       CadCreatorPanel.getInstance().displayVerificationImagesFittingCad();
  }

  /*
   * @uathor Kee Chin Seong
   */
  public void unpopulate()
  {
    _project = null;
    CadCreatorPanel.getInstance().unpopulate();
  }
  
  /*
   * @uathor Kee Chin Seong
   */
  private void removeGraphicEngineObserver()
  {
    _panelCadPanel.removePanelGraphicObserver();
    _panelCadPanel.removeLandPatternGraphicsObserver();
  }
  
  /*
   * @uathor Kee Chin Seong
   */
  private void addGraphicEngineObserver()
  {
    _panelCadPanel.addPanelGraphicObserver();
    _panelCadPanel.addLandPatternGraphicObersver();
  }
    
}
