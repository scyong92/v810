package com.axi.v810.gui.cadCreator;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 *
 * @author Administrator
 */
public class ComponentLandPatternTableModel extends DefaultSortTableModel
{
  private List<Component> _componentList = new ArrayList<Component>();  // List of ComponentType objects

  private Collection<Component> _currentlySelectedComponents = new ArrayList<Component>();
  private final String[] _componentTableColumns =
  {
    StringLocalizer.keyToString("Ref Des"),
    StringLocalizer.keyToString("X"),
    StringLocalizer.keyToString("Y"),
    StringLocalizer.keyToString("Package"),
    StringLocalizer.keyToString("Rotation"),
    StringLocalizer.keyToString("Land Pattern"),
  };
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;  
  private Panel _panel = null;
  
  private static ComponentTypeComparator _componentTypeComparator;
  
  static
  {
    _componentTypeComparator = new ComponentTypeComparator(true, ComponentTypeComparatorEnum.REFERENCE_DESIGNATOR);
  }
  
  /**
   * @author Chin-Seong, Kee
   */
  public ComponentLandPatternTableModel()
  {
    // do nothing
  }

  /**
   * @author Chin-Seong, Kee
   */
  void clear()
  {
    _componentList.clear();
    _currentlySelectedComponents.clear();
    _currentSortColumn = 0;
    _currentSortAscending = true;
  }

  /**
   * @author Chin-Seong, Kee
   */
  void populateComponentPopulatedLandPatternWithPanelData(Project project)
  {
    _componentList.clear();
    for(Component comp : project.getPanel().getComponents())
    {
      if(comp.getLandPattern().getName().matches(StringLocalizer.keyToString("CAD_UNPOPULATED_LAND_PATTERN_KEY")) == false)
        _componentList.add(comp);
    }
    _panel = project.getPanel();
    sortColumn(_currentSortColumn, _currentSortAscending);

    // components may have been deleted, so if they were previously selected, we need to remove them.
    Iterator<Component> it = _currentlySelectedComponents.iterator();
    while(it.hasNext())
    {
      if (this._componentList.contains(it.next()) == false)
        it.remove();
    }
  }
  
  /**
   * @author Chin-Seong, Kee
   */
  void populateComponentUnpopulatedLandPatternWithPanelData(Project project)
  {
    for(Component comp : project.getPanel().getComponents())
    {
      if(comp.getLandPattern().getName().matches(StringLocalizer.keyToString("CAD_UNPOPULATED_LAND_PATTERN_KEY")) == true)
        _componentList.add(comp);
    }
    _panel = project.getPanel();
    sortColumn(_currentSortColumn, _currentSortAscending);

    // components may have been deleted, so if they were previously selected, we need to remove them.
    Iterator<Component> it = _currentlySelectedComponents.iterator();
    while(it.hasNext())
    {
      if (this._componentList.contains(it.next()) == false)
        it.remove();
    }
  }

  /**
   * @author Chin-Seong, Kee
   */
  void updateSpecificComponent(Component component)
  {
    int componentRow = _componentList.indexOf(component);
    fireTableRowsUpdated(componentRow, componentRow);
  }
  
  /**
   * @author Kee Chin Seong
   */
  void unpopulate()
  {
    _currentlySelectedComponents.clear();
    _componentList.clear();
    fireTableDataChanged();
  }

  /**
   * @author Chin-Seong, Kee
   */
  int getRowForComponent(Component component)
  {
    return _componentList.lastIndexOf(component);
  }

  /**
   * @author Chin-Seong, Kee
   */
  public int getColumnCount()
  {
    return _componentTableColumns.length;
  }

  /**
   * @author Chin-Seong, Kee
   */
  public int getRowCount()
  {
    if (_componentList == null)
      return 0;
    else
      return _componentList.size();
  }

  /**
   * @author Chin-Seong, Kee
   */
  public Class getColumnClass(int c)
  {
    return getValueAt(0, c).getClass();
  }

  /**
   * @author Chin-Seong, Kee
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_componentTableColumns[columnIndex];
  }

  /**
   * @author Chin-Seong, Kee
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    // column 0 is component name - not editable
    // column 1 is package - not editable
    // column 2 is joint type -- only editable if not "MIXED"
    // column 3 is subtype - always editable
    // column 4 is auto focus - always editable
    // column 5 is artifact compensation - only editable if not "MIXED"
    // column 6 is signal compensation - only editable if not "MIXED"

    if (columnIndex == 0)
      return false;
    if (columnIndex == 1)
      return false;
    if (columnIndex == 2)
      return false;
    if (columnIndex == 3)
      return false;
    if (columnIndex == 4)
      return false;
    if (columnIndex == 5)
        return true;

    return true;
  }

  /**
   * @author Chin-Seong, Kee
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Component comp = _componentList.get(rowIndex);
    //CompPackage compPackage = comp.getCompPackage();
    if (columnIndex == 0) // component name
      return comp.getReferenceDesignator();
    else if (columnIndex == 1) // X
    {
      return comp.getCoordinateRelativeToCadOriginInNanoMeters().getX();
    }
    else if (columnIndex == 2) // Y
    {
      return comp.getCoordinateRelativeToCadOriginInNanoMeters().getY();
    }
    else if(columnIndex == 3) // Rotation
    {
      return comp.getComponentType().getCompPackage().getLongName();
    }
    else if(columnIndex == 4) // Rotation
    {
      return comp.getDegreesRotationAfterAllRotations();
    }
    else if (columnIndex == 5) // LandPattern
    {
      return comp.getLandPattern();
    }
    return null;
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    if (columnIndex == 0) // column 0 is component name -- not editable
      Assert.expect(false);
    
    else if (columnIndex == 5) // component land Pattern
    {
       try
       {
            Component component = getComponentAt(rowIndex);
            ComponentType componentType = component.getComponentType();
            
            LandPattern chosenLandPattern = _panel.getLandPattern((String)aValue);

            _commandManager.beginCommandBlock(StringLocalizer.keyToString(new LocalizedString("CAD_CHANGE_LP_FOR_COMPONENT_KEY",
                                                                                                  new Object[]{component.getReferenceDesignator()})));
            _commandManager.execute(new RemoveComponentTypeCommand(component.getComponentType()));
            _commandManager.execute(new CreateComponentTypeCommand(componentType.getSideBoardType(),
                                                                       componentType.getReferenceDesignator(),
                                                                       chosenLandPattern,
                                                                       componentType.getCoordinateInNanoMeters(),
                                                                       componentType.getDegreesRotationRelativeToBoard()));
            _commandManager.endCommandBlock();
        }
        catch (Exception ex)
        {

        }
    }
  }

  /**
   * @author Chin-Seong, Kee
   */
  void setSelectedComponentTypes(Collection<Component> componentTypes)
  {
    Assert.expect(componentTypes != null);
    _currentlySelectedComponents = componentTypes;
  }

  /**
   * @author Chin-Seong, Kee
   */
  Collection<Component> getSelectedComponentTypes()
  {
    Assert.expect(_currentlySelectedComponents != null);
    return _currentlySelectedComponents;
  }
  
  /*
   * @author Kee Chin Seong
   */
  void addPopulatedComponent(Component component)
  {
    Assert.expect(component != null);
    Assert.expect(_componentList != null);
    
    _componentList.add(component);
    
    sortColumn(_currentSortColumn, _currentSortAscending);
    
    // components may have been deleted, so if they were previously selected, we need to remove them.
    Iterator<Component> it = _currentlySelectedComponents.iterator();
    while(it.hasNext())
    {
      if (this._componentList.contains(it.next()) == false)
        it.remove();
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  void addUnpopulatedComponent(Component component)
  {
    Assert.expect(component != null);
    Assert.expect(_componentList != null);
    
    _componentList.add(component);
    
    sortColumn(_currentSortColumn, _currentSortAscending);
    
    // components may have been deleted, so if they were previously selected, we need to remove them.
    Iterator<Component> it = _currentlySelectedComponents.iterator();
    while(it.hasNext())
    {
      if (this._componentList.contains(it.next()) == false)
        it.remove();
    }
  }


  /**
   * @author Chin-Seong, Kee
   */
  Component getComponentAt(int row)
  {
    return _componentList.get(row);
  }
  
  /*
   * @author Kee Chin Seong
   */
  boolean removeComponent(Component component)
  {
    Assert.expect(component != null);
    Assert.expect(_componentList != null);
    
    return _componentList.remove(component);   
  }
  
  /**
   * @author Kee Chin Seong
   */
  public int getRowForDataStartingWith(String key)
  {
    key = key.toLowerCase();
    int index = -1;
    for (Component comp :_componentList)
    {
      index++;
      String compName = comp.getReferenceDesignator().toLowerCase();
      if (compName.startsWith(key))
      {
        return index;
      }
    }
    return -1;
  }

  /**
   * @author Kee Chin Seong
   */
  public void clearCurrentlySelectedComponents()
  {
    _currentlySelectedComponents.clear();
    fireTableDataChanged();
  }

  /**
   * @author Kee Chin Seong
   */
  public Collection<Component> getCurrentlySelectedComponents()
  {
    Assert.expect(_currentlySelectedComponents != null);

    return _currentlySelectedComponents;
  }
}
