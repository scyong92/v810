package com.axi.v810.gui.cadCreator;

import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import java.util.List;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.Project;
import com.axi.v810.util.StringLocalizer;
import java.util.ArrayList;
import javax.swing.JComboBox;
/**
 *
 * @author Kee Chin Seong
 */
public class ComponentLandPatternTable extends JSortTable
{
  private static final int _REF_DES_INDEX = 0;
  private static final int _PAD_X_INDEX = 1;
  private static final int _PAD_Y_INDEX = 2;
  private static final int _PACKAGE_INDEX = 3;
  private static final int _PAD_ROTATION_INDEX = 4;
  private static final int _LAND_PATTERN_INDEX = 5;
  
  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _REF_DES_COLUMN_WIDTH_PERCENTAGE = .222;
  private static final double _PAD_X_COLUMN_WIDTH_PERCENTAGE = .222;
  private static final double _PAD_Y_COLUMN_WIDTH_PERCENTAGE = .222;
  private static final double _PAD_ROTATION_COLUMN_WIDTH_PERCENTAGE = .222;
  private static final double _LAND_PATTERN_COLUMN_WIDTH_PERCENTAGE = .222;
  private static final double _PACKAGE_COLUMN_WIDTH_PERCENTAGE = .222;
  
  private List<com.axi.v810.business.panelDesc.Component>_componentList = new ArrayList<com.axi.v810.business.panelDesc.Component>();
  
  private ComponentLandPatternTableModel _tableModel;
  
  /**
   * Default constructor.
   * @author Kee Chin Seong
   */
  public ComponentLandPatternTable(ComponentLandPatternTableModel tableModel)
  {
    Assert.expect(tableModel != null);
    _tableModel = tableModel;
    super.setModel(tableModel);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public ComponentLandPatternTableModel getTableModel()
  {
    Assert.expect(_tableModel != null);
    
    return _tableModel;
  }
  
  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Kee Chin Seong
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_REF_DES_INDEX).setPreferredWidth((int)(totalColumnWidth * _REF_DES_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PAD_X_INDEX).setPreferredWidth((int)(totalColumnWidth * _PAD_X_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PAD_Y_INDEX).setPreferredWidth((int)(totalColumnWidth * _PAD_Y_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PAD_ROTATION_INDEX).setPreferredWidth((int)(totalColumnWidth * _PAD_ROTATION_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_LAND_PATTERN_INDEX).setPreferredWidth((int)(totalColumnWidth * _LAND_PATTERN_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PACKAGE_INDEX).setPreferredWidth((int)(totalColumnWidth * _PACKAGE_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * @author Kee Chin Seong
   */
  public void setEditorsAndRenderers(List<LandPattern> landPatternList)
  {
    JComboBox componentSubtypeComboBox = new JComboBox();
    ComponentLandPatternCellEditor componentSubtypeCellEditor = new ComponentLandPatternCellEditor(componentSubtypeComboBox, _tableModel, landPatternList);
    columnModel.getColumn(_LAND_PATTERN_INDEX).setCellEditor(componentSubtypeCellEditor);

  }
  
  /*
   * @author Kee Chin Seong
   */
  public void populateComponent(List<com.axi.v810.business.panelDesc.Component>componentList)
  {
    Assert.expect(componentList != null);
    
    for(Component comp : componentList)
    {
      if(comp.getLandPattern().getName().matches(StringLocalizer.keyToString("CAD_UNPOPULATED_LAND_PATTERN_KEY")) == false)
        _componentList.add(comp);
    }
  }

}
