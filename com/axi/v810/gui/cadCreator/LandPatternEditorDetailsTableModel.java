package com.axi.v810.gui.cadCreator;

import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;
import java.awt.geom.*;

/**
 * This class is the table model for land pattern details information.
 * @author Kee Chin Seong
 * @author Kee Chin Seong
 */
class LandPatternEditorDetailsTableModel extends DefaultSortTableModel
{
  private MainMenuGui _mainUI;
  private Project _project;

  // command manager for undo functionality
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private List<LandPatternPad> _pads = new ArrayList<LandPatternPad>();  // list of LandPatternPads
  private static final String _PAD_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_PAD_KEY");
  private static final String _XLOC_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_X_LOC_KEY");
  private static final String _YLOC_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_Y_LOC_KEY");
  private static final String _DX_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_DX_KEY");
  private static final String _DY_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_DY_KEY");
  private static final String _ROTATION_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_ROTATION_KEY");
  private static final String _SHAPE_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUPI_SHAPE_KEY");
  private static final String _TYPE_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUPI_LLP_TYPE_KEY");
  private static final String _HOLE_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUPI_HOLE_DIAM_KEY");
  private final String[] _tableColumns = {_PAD_COLUMN_HEADING,
                                         _XLOC_COLUMN_HEADING,
                                         _YLOC_COLUMN_HEADING,
                                         _DX_COLUMN_HEADING,
                                         _DY_COLUMN_HEADING,
                                         _ROTATION_COLUMN_HEADING,
                                         _SHAPE_COLUMN_HEADING,
                                         _TYPE_COLUMN_HEADING,
                                         _HOLE_COLUMN_HEADING};

  List<LandPatternPad> _selectedPads = new ArrayList<LandPatternPad>();

  /**
   * @author Kee Chin Seong
   */
  LandPatternEditorDetailsTableModel(MainMenuGui mainUI)
  {
    Assert.expect(mainUI != null);
    _mainUI = mainUI;
    //_mainUI = _testDev.getMainUI();
  }

  /**
   * @author Kee Chin Seong
   */
  void populateWithLandPattern(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);
    _project = landPattern.getPanel().getProject();
    _pads = landPattern.getLandPatternPads();
    Collections.sort(_pads, new AlphaNumericComparator());
    fireTableDataChanged();
  }

  /**
   * @author Kee Chin Seong
   */
  LandPatternPad getLandPatternPadAt(int row)
  {
    Assert.expect(row >= 0 && row < _pads.size());
    Assert.expect(_pads != null);
    return (LandPatternPad)_pads.get(row);
  }

  /**
   * @author Kee Chin Seong
   */
  int getRowForLandPatternPad(LandPatternPad landPatternPad)
  {
    Assert.expect(_pads != null);
    return _pads.indexOf(landPatternPad);
  }

  /**
   * @author Kee Chin Seong
   */
  void clear()
  {
    _pads.clear();
    _selectedPads.clear();
    fireTableDataChanged();
    _project = null;
  }

  /**
   * @author Kee Chin Seong
   */
  public void sortColumn(int column, boolean ascending)
  {
    switch(column)
    {
      case 0: // name
        Collections.sort(_pads, new LandPatternPadComparator(ascending, LandPatternPadComparatorEnum.NAME));
        break;
      case 1: // x location
        Collections.sort(_pads, new LandPatternPadComparator(ascending, LandPatternPadComparatorEnum.X_COORDINATE));
        break;
      case 2: // y location
        Collections.sort(_pads, new LandPatternPadComparator(ascending, LandPatternPadComparatorEnum.Y_COORDINATE));
        break;
      case 3: // width
        Collections.sort(_pads, new LandPatternPadComparator(ascending, LandPatternPadComparatorEnum.WIDTH));
        break;
      case 4: // length
        Collections.sort(_pads, new LandPatternPadComparator(ascending, LandPatternPadComparatorEnum.LENGTH));
        break;
      case 5: // rotation
        Collections.sort(_pads, new LandPatternPadComparator(ascending, LandPatternPadComparatorEnum.DEGREES_ROTATION));
        break;
      case 6: // shape
        Collections.sort(_pads, new LandPatternPadComparator(ascending, LandPatternPadComparatorEnum.SHAPE));
        break;
      case 7: // type
        Collections.sort(_pads, new LandPatternPadComparator(ascending, LandPatternPadComparatorEnum.TYPE));
        break;
      case 8: // hole
        Collections.sort(_pads, new LandPatternPadComparator(ascending, LandPatternPadComparatorEnum.HOLE));
        break;
      default:
        Assert.expect(false, "column does not exist in table model");
        break;
    }
  }

  /**
   * @author Kee Chin Seong
   */
  public int getColumnCount()
  {
    Assert.expect(_tableColumns != null);
    return _tableColumns.length;
  }

  /**
   * @author Kee Chin Seong
   */
  public int getRowCount()
  {
    if (_pads == null)
      return 0;
    else
      return _pads.size();
  }

  /**
   * @author Kee Chin Seong
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(_tableColumns != null);
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return(String)_tableColumns[columnIndex];
  }

  /**
   * @author Kee Chin Seong
   * @author Kee Chin Seong
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if (_selectedPads.contains(_pads.get(rowIndex)) == false)
      return false;

    // does not make sense to change the name of multiple pads to the same name
    if (_selectedPads.size() > 1)
      if (columnIndex == 0)
        return false;

    if (columnIndex == 8)
    {
      LandPatternPad pad = _pads.get(rowIndex);
      if (pad.isThroughHolePad())
        return true;
      else
        return false;
    }
    return true;
  }

  /**
   * @author Kee Chin Seong
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_pads != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    LandPatternPad pad = (LandPatternPad)_pads.get(rowIndex);
    switch(columnIndex)
    {
      case 0:
        return pad.getName();
      case 1:
        int x = pad.getCoordinateInNanoMeters().getX();
        return new Double(MathUtil.convertUnits(x, MathUtilEnum.NANOMETERS, _project.getDisplayUnits()));
      case 2:
        int y = pad.getCoordinateInNanoMeters().getY();
        return new Double(MathUtil.convertUnits(y, MathUtilEnum.NANOMETERS, _project.getDisplayUnits()));
      case 3:
        int dx = pad.getWidthInNanoMeters();
        return new Double(MathUtil.convertUnits(dx, MathUtilEnum.NANOMETERS, _project.getDisplayUnits()));
      case 4:
        int dy = pad.getLengthInNanoMeters();
        return new Double(MathUtil.convertUnits(dy, MathUtilEnum.NANOMETERS, _project.getDisplayUnits()));
      case 5:
        double degree = pad.getDegreesRotation();
        return new Double(degree).toString();
      case 6:
        ShapeEnum shapeEnum = pad.getShapeEnum();
        if (shapeEnum.equals(ShapeEnum.CIRCLE))
          return StringLocalizer.keyToString("CAD_CIRCLE_SHAPE_KEY");
        else
          return StringLocalizer.keyToString("CAD_RECT_SHAPE_KEY");
      case 7:
        if (pad.isSurfaceMountPad())
          return StringLocalizer.keyToString("CAD_SM_KEY");
        else
          return StringLocalizer.keyToString("CAD_TH_KEY");
      case 8:
        if (pad.isThroughHolePad())
        {
          ////int holeDiam = ((ThroughHoleLandPatternPad)pad).getHoleDiameterInNanoMeters();
          return new Double(MathUtil.convertUnits(0, MathUtilEnum.NANOMETERS, _project.getDisplayUnits()));
        }
        else
        {
          return 0.0;
        }
      default:
        Assert.expect(false);
        return null;
    }
  }

  /**
   * @author Kee Chin Seong
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    Assert.expect(_pads != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    LandPatternPad landPatternPad = (LandPatternPad)_pads.get(rowIndex);
    String valueStr;
    Double value;
    double valueInNanos;
    int valueInNanosInt = -1;

    switch(columnIndex)
    {
      case 0: // pad name
        Assert.expect(aValue instanceof String);
        valueStr = (String)aValue;
        // only change if different
        String currentName = landPatternPad.getName();
        if (currentName.equalsIgnoreCase(valueStr) == false)
        {
          if (landPatternPad.getLandPattern().isLandPatternPadNameValid(valueStr) == false)
          {
                String illegalChars = landPatternPad.getLandPattern().getLandPatternPadNameIllegalChars();
            LocalizedString message = null;
            if (illegalChars.equals(" "))
              message = new LocalizedString("CAD_INVALID_LAND_PATTERN_PAD_NAME_SPACES_KEY", new Object[]
                                            {valueStr});
            else
              message = new LocalizedString("CAD_INVALID_LAND_PATTERN_PAD_NAME_OTHER_CHARS_KEY", new Object[]
                                              {valueStr, illegalChars});
            JOptionPane.showMessageDialog(_mainUI,
                                          StringLocalizer.keyToString(message),
                                          StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                          JOptionPane.INFORMATION_MESSAGE);
            return;
          }
          else if (landPatternPad.getLandPattern().isLandPatternPadNameDuplicate(valueStr))
          {
            JOptionPane.showMessageDialog(_mainUI,
                                          StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_LPP_NAME_KEY",
                                                                                           new Object[]{valueStr})),
                                          StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                          JOptionPane.INFORMATION_MESSAGE);
            return;
          }
          try
          {
            _commandManager.execute(new LandPatternPadSetNameCommand(landPatternPad, valueStr));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, ex.getLocalizedMessage(),
                                                                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
        }
        break;
      case 1: // X location
        Assert.expect(aValue instanceof Number);
        value = ((Number)aValue).doubleValue();
        valueInNanos = MathUtil.convertUnits(value, _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
        // if this land pattern is used by 2-pin devices of res, cap or pcap joint type, check for overlapping pads.
        if (ProgramVerificationUtil.is2PinDeviceLandPattern(landPatternPad.getLandPattern()))
        {
          if (ProgramVerificationUtil.does2PinDeviceHaveOverlappingPads(landPatternPad,
                                                valueInNanos,
                                                landPatternPad.getCoordinateInNanoMeters().getY(),
                                                landPatternPad.getWidthInNanoMeters(),
                                                landPatternPad.getLengthInNanoMeters()))
          {
            showOverlappingPadsErrorMessage();
            return;
          }
        }

        ComponentCoordinate oldCoordinateInNanometers = landPatternPad.getCoordinateInNanoMeters();
        // the cast to int below limits the value of the coordinate to max/min int value if the
        // given value was greater
        oldCoordinateInNanometers.setX((int)MathUtil.convertUnits(value.doubleValue(),
                                                                  _project.getDisplayUnits(),
                                                                  MathUtilEnum.NANOMETERS));
        try
        {
          _commandManager.execute(new LandPatternPadSetCoordinateInNanoMetersCommand(landPatternPad, oldCoordinateInNanometers));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, ex.getLocalizedMessage(),
                                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      case 2: // Y location
        Assert.expect(aValue instanceof Number);
        value = ((Number)aValue).doubleValue();
        valueInNanos = MathUtil.convertUnits(value, _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
        // if this land pattern is used by 2-pin devices of res, cap or pcap joint type, check for overlapping pads.
        if (ProgramVerificationUtil.is2PinDeviceLandPattern(landPatternPad.getLandPattern()))
        {
          if (ProgramVerificationUtil.does2PinDeviceHaveOverlappingPads(landPatternPad,
                                                landPatternPad.getCoordinateInNanoMeters().getX(),
                                                valueInNanos,
                                                landPatternPad.getWidthInNanoMeters(),
                                                landPatternPad.getLengthInNanoMeters()))
          {
            showOverlappingPadsErrorMessage();
            return;
          }
        }

        oldCoordinateInNanometers = landPatternPad.getCoordinateInNanoMeters();
        // the cast to int below limits the value of the coordinate to max/min int value if the
        // given value was greater
        oldCoordinateInNanometers.setY((int)MathUtil.convertUnits(value.doubleValue(),
                                                                  _project.getDisplayUnits(),
                                                                  MathUtilEnum.NANOMETERS));
        try
        {
          _commandManager.execute(new LandPatternPadSetCoordinateInNanoMetersCommand(landPatternPad, oldCoordinateInNanometers));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, ex.getLocalizedMessage(),
                                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      case 3: // X dimension (width)
        Assert.expect(aValue instanceof Number);
        value = ((Number)aValue).doubleValue();
        valueInNanos = MathUtil.convertUnits(value, _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
        try
        {
          valueInNanosInt = MathUtil.convertDoubleToInt(valueInNanos);
          if (valueInNanosInt == 0)
          {
            MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI,
                                                                StringLocalizer.keyToString(new LocalizedString(
                                                                    "CAD_TOO_SMALL_VALUE_FOR_PAD_X_DIMENSION_MSG_KEY",
                                                                    new Object[] {Double.toString(value)})),
                                                                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            return;
          }
        }
        catch (ValueOutOfRangeException ex)
        {
          MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, ex.getLocalizedMessage(),
                                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          return;
        }

        // if this land pattern is used by 2-pin devices of res, cap or pcap joint type, check for overlapping pads.
        if (ProgramVerificationUtil.is2PinDeviceLandPattern(landPatternPad.getLandPattern()))
        {
          if (ProgramVerificationUtil.does2PinDeviceHaveOverlappingPads(landPatternPad,
                                                landPatternPad.getCoordinateInNanoMeters().getX(),
                                                landPatternPad.getCoordinateInNanoMeters().getY(),
                                                valueInNanosInt,
                                                landPatternPad.getLengthInNanoMeters()))
          {
            showOverlappingPadsErrorMessage();
            return;
          }
        }

        if (valueInNanos <= 0)
        {
          MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, 
                                                              StringLocalizer.keyToString("CAD_NON_POSITIVE_VALUE_FOR_PAD_X_DIMENSION_MSG_KEY"),
                                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          return;
        }
        // pads greater than the panel dimensions are absurd
        else if (valueInNanos > landPatternPad.getLandPattern().getPanel().getWidthInNanoMeters())
        {
          // error out, pad is way too big
          MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, 
                                                              StringLocalizer.keyToString("CAD_PAD_DIMENSION_EXCEEDS_PANEL_DIMENSION_KEY"),
                                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          return;
        }

        // pads greater than 1" are too big and cannot be tested as such
        else if (valueInNanos > MathUtil.convertMilsToNanoMeters(1000))
        {
          // put a warning that this pad cannot be tested
          int response = JOptionPane.showConfirmDialog(_mainUI,
              StringLocalizer.keyToString("CAD_PAD_DIMENSION_IS_TOO_BIG_KEY"),
              StringLocalizer.keyToString("PDD_CONFIRM_DELETE_TITLE_KEY"),
             JOptionPane.YES_NO_OPTION);
          if (response == JOptionPane.NO_OPTION)
            return;
        }

        try
        {
          _commandManager.execute(new LandPatternPadSetWidthInNanoMetersCommand(landPatternPad, valueInNanosInt));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, ex.getLocalizedMessage(),
                                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      case 4: // Y dimension (length)
        Assert.expect(aValue instanceof Number);
        value = ((Number)aValue).doubleValue();
        valueInNanos = MathUtil.convertUnits(value, _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
        try
        {
          valueInNanosInt = MathUtil.convertDoubleToInt(valueInNanos);
          if (valueInNanosInt == 0)
          {
            MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI,
                                                                StringLocalizer.keyToString(new LocalizedString(
                                                                    "CAD_TOO_SMALL_VALUE_FOR_PAD_Y_DIMENSION_MSG_KEY", 
                                                                    new Object[]{Double.toString(value)})),
                                                                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            return;
          }
        }
        catch (ValueOutOfRangeException ex)
        {
          MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, ex.getLocalizedMessage(),
                                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          return;
        }

        // if this land pattern is used by 2-pin devices of res, cap or pcap joint type, check for overlapping pads.
        if (ProgramVerificationUtil.is2PinDeviceLandPattern(landPatternPad.getLandPattern()))
        {
          if (ProgramVerificationUtil.does2PinDeviceHaveOverlappingPads(landPatternPad,
                                                landPatternPad.getCoordinateInNanoMeters().getX(),
                                                landPatternPad.getCoordinateInNanoMeters().getY(),
                                                landPatternPad.getWidthInNanoMeters(),
                                                valueInNanosInt))
          {
            showOverlappingPadsErrorMessage();
            return;
          }
        }

        if (value <= 0)
        {
          MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, 
                                                              StringLocalizer.keyToString("CAD_NON_POSITIVE_VALUE_FOR_PAD_Y_DIMENSION_MSG_KEY"),
                                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          return;
        }
        // pads greater than the panel dimensions are absurd
        else if (valueInNanos > landPatternPad.getLandPattern().getPanel().getWidthInNanoMeters())
        {
          // error out, pad is way too big
          MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, 
                                                              StringLocalizer.keyToString("CAD_PAD_DIMENSION_EXCEEDS_PANEL_DIMENSION_KEY"),
                                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          return;
        }

        // pads greater than 1" are too big and cannot be tested as such
        else if (valueInNanos > MathUtil.convertMilsToNanoMeters(1000))
        {
          // put a warning that this pad cannot be tested
          int response = JOptionPane.showConfirmDialog(_mainUI,
                                                       StringLocalizer.keyToString("CAD_PAD_DIMENSION_IS_TOO_BIG_KEY"),
                                                       StringLocalizer.keyToString("PDD_CONFIRM_DELETE_TITLE_KEY"),
                                                       JOptionPane.YES_NO_OPTION);
          if (response == JOptionPane.NO_OPTION)
            return;
        }

        try
        {
          _commandManager.execute(new LandPatternPadSetLengthInNanoMetersCommand(landPatternPad, valueInNanosInt));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, ex.getLocalizedMessage(),
                                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      case 5: // rotation
        Assert.expect(aValue instanceof String);
        valueStr = (String)aValue;
        double degrees;
        try
        {
          degrees = Double.parseDouble(valueStr);
        }
        catch (NumberFormatException ex)
        {
          fireTableCellUpdated(rowIndex, columnIndex);
          break;
        }

        try
        {
          _commandManager.execute(new LandPatternPadSetDegreesRotationCommand(landPatternPad, degrees));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, ex.getLocalizedMessage(),
                                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      case 6: // Shape
        Assert.expect(aValue instanceof String);
        valueStr = (String)aValue;
        if (valueStr.equals(StringLocalizer.keyToString("CAD_CIRCLE_SHAPE_KEY")))
          try
          {
            _commandManager.execute(new LandPatternPadSetShapeEnumCommand(landPatternPad, ShapeEnum.CIRCLE));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, ex.getLocalizedMessage(),
                                                                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
        else if (valueStr.equals(StringLocalizer.keyToString("CAD_RECT_SHAPE_KEY")))
          try
          {
            _commandManager.execute(new LandPatternPadSetShapeEnumCommand(landPatternPad, ShapeEnum.RECTANGLE));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, ex.getLocalizedMessage(),
                                                                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
        break;
      case 7: // pad type (SM or TH)
        Assert.expect(aValue instanceof String);
        valueStr = (String)aValue;
        // only change the pads that are of a different type than the type requested
        if ((valueStr.equals(StringLocalizer.keyToString("CAD_SM_KEY")) && landPatternPad.isThroughHolePad()) ||
            (valueStr.equals(StringLocalizer.keyToString("CAD_TH_KEY")) && landPatternPad.isSurfaceMountPad()))
        {
          try
          {
            _commandManager.execute(new LandPatternSwapLandPatternPadTypeCommand(landPatternPad));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, ex.getLocalizedMessage(),
                                                                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
        }
        break;
      case 8: // TH pad hole diameter
        Assert.expect(aValue instanceof Number);
        value = ((Number)aValue).doubleValue();
        Assert.expect(landPatternPad instanceof ThroughHoleLandPatternPad);
        value = MathUtil.convertUnits(value.doubleValue(), _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
        // give error message if hole diameter is less than 1 mil
        if (value < MathUtil.convertMilsToNanoMetersInteger(1))
        {
          MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, 
                                                              StringLocalizer.keyToString("CAD_NON_POSITIVE_VALUE_FOR_TH_HOLE_DIAMETER_MSG_KEY"),
                                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          return;
        }
        // this hole is definitely too big, it could swallow the whole panel!!
        else if (value > landPatternPad.getLandPattern().getPanel().getWidthInNanoMeters()||
                 value > landPatternPad.getLandPattern().getPanel().getLengthInNanoMeters())
        {
          MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, 
                                                              StringLocalizer.keyToString("CAD_TH_HOLE_DIAMETER_BIGGER_THAN_PANEL_KEY"),
                                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          return;
        }
        /*try
        {
          //  to do : need create width length
          //_commandManager.execute(new ThroughHoleLandPatternPadStttetHoleDiameterCommand((ThroughHoleLandPatternPad)landPatternPad,
          //    value.intValue()));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI, ex.getLocalizedMessage(),
                                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }*/

        break;
      default:
        Assert.expect(false);
    }
  }

  /**
   * This determines whether the shapes (bounding rectangle) of 2 LPP in a 2 pin LP are overlapping with the new
   *  LP dimensions and location.
   * @author Kee Chin Seong
   */
  boolean does2PinDeviceHaveOverlappingPads(LandPatternPad landPatternPad,
                                            double newXCoordInNanoMeters,
                                            double newYCoordInNanoMeters,
                                            int newXDimInNanoMeters,
                                            int newYDimInNanoMeters)
  {
    LandPattern landPattern = landPatternPad.getLandPattern();
    Assert.expect(landPattern.getLandPatternPads().size() == 2);
    LandPatternPad otherLPP = null;
    for (LandPatternPad pad : landPattern.getLandPatternPads())
    {
      if (pad != landPatternPad)
        otherLPP = pad;
    }
    Assert.expect(otherLPP != null);
    java.awt.Shape thisLandPatternPadShape = landPatternPad.getHypotheticalShapeInNanoMeters(newXCoordInNanoMeters,
                                                                                             newYCoordInNanoMeters,
                                                                                             newXDimInNanoMeters,
                                                                                             newYDimInNanoMeters);
    java.awt.Shape otherLandPatternPadShape = otherLPP.getShapeInNanoMeters();
    if (thisLandPatternPadShape.intersects((Rectangle2D)otherLandPatternPadShape.getBounds2D()))
      return true;
    return false;
  }

  /**
   * @author Kee Chin Seong
   */
  void showOverlappingPadsErrorMessage()
  {
    // give error message about the given pad overlapping the other pad.
    MessageDialog.showErrorDialogInSwingUtilitiesThread(_mainUI,
                                                        StringLocalizer.keyToString("CAD_OVERLAPPING_PAD_ERROR_MSG_KEY"),
                                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
  }

  /**
   * @author Kee Chin Seong
   * @param landPatternPad The pad to be removed from this table model
   * @return true if the pad was found in the list, false otherwise
   */
  boolean deleteLandPatternPad(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);
    Assert.expect(_pads != null);

    if (_pads.remove(landPatternPad))
    {
      fireTableDataChanged();
      return true;
    }
    return false;
  }

  /**
   * @author Kee Chin Seong
   */
  public void addLandPatternPad(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);
    Assert.expect(_pads != null);

    // only add a pad if it's not already there.
    if (_pads.contains(landPatternPad) == false)
      _pads.add(landPatternPad);
    Collections.sort(_pads, new AlphaNumericComparator());
    fireTableDataChanged();
  }

  /**
   * @author Kee Chin Seong
   */
  public void setSelectedPads(List<LandPatternPad> selectedPads)
  {
    Assert.expect(selectedPads != null);
    _selectedPads = selectedPads;
  }

  /**
   * @author Kee Chin Seong
   */
  void setProject(Project project)
  {
    Assert.expect(project != null);
    _project = project;
  }

  /**
   * @author Kee Chin Seong
   */
  List<LandPatternPad> getPads()
  {
    Assert.expect(_pads != null);
    return _pads;
  }
}