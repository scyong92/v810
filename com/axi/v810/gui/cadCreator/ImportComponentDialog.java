/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.cadCreator;

import com.axi.guiUtil.*;
import com.axi.util.Assert;
import com.axi.util.LocalizedString;
import com.axi.util.MathUtil;
import com.axi.util.MathUtilEnum;
import com.axi.util.WorkerThread;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.MessageDialog;
import com.axi.v810.gui.undo.CreateComponentTypeCommand;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.lang.String;
import java.util.*;
import javax.swing.*;
import javax.swing.border.Border;

/**
 *
 * @author chin-seong, kee
 */
public class ImportComponentDialog extends EscapeDialog
{
   private Frame _parentFrame = null;
   
   private JPanel _mainPanel;
   private JPanel _topInnerPanel;
   private JPanel _centerInnerPanel;
   private JPanel _bottomInnerPanel;
   
   private Border _panelBorder;
   
   private JTextArea _readDataReadOnlyTextArea;
   private JScrollPane _readDataScrollPane;
   private JScrollPane _resultsScrollPane;
   
   private JButton _browsePathButton;
   private JButton _readFileButton;
   private JButton _setAsHeaderButton;
   private JButton _okButton;
   private JButton _cancelButton;
   private JTextField _selectedFilePathTextField;
   
   private JComboBox _seperateSymbolChooseComboBox = new JComboBox();
   private JComboBox _scaleChooseComboBox = new JComboBox();
   private JComboBox _angleChooseComboBox = new JComboBox();
   private JComboBox _unitsChooseComboBox = new JComboBox();

   private JLabel _seperateSymbolChooserLbl = new JLabel("Seperate by: ");
   private JLabel _scaleLabel = new JLabel("Scale: ");
   private JLabel _angleLabel = new JLabel("Angle: ");
   private JLabel _unitsLabel = new JLabel("Units: ");
   
   private GerbelDataTableModel _gerbelDataTableModel;
   private GerbelDataTable _gerbelDataTable;
   
   private java.util.List<String> _dataInGerbel = new ArrayList<String>();
   private java.util.List<java.util.List<String>> _splittedData = new ArrayList<java.util.List<String>>();
   private int _headerLineEnd = 0;
   
   private LandPattern _useLandPattern = null;
   private com.axi.v810.business.panelSettings.Project _project = null;
   
   private BusyCancelDialog busyCancelDialog = null;
   
   private WorkerThread _importComponentThread = new WorkerThread("Importing Component ...");
   
   private ComponentLandPatternTable _populateComponentLandPatternTable;
   private ComponentLandPatternTable _unpopulateComponentLandPatternTable;
   
   private int _returnValue;
   private String _dataRead;
   
  /**
   * @author chin-seong, kee
   */
  public ImportComponentDialog(Frame parent, String title, com.axi.v810.business.panelSettings.Project project, 
                               LandPattern usedLandPattern, ComponentLandPatternTable populateComponentLandPatternTable,
                               ComponentLandPatternTable unpopulateComponentLandPatternTable)
  {
     super(parent, title, true);
     
     Assert.expect(parent != null);
    
    _parentFrame = parent;
    _useLandPattern = usedLandPattern;
    _project = project;
    
    
    _populateComponentLandPatternTable = populateComponentLandPatternTable;
    _unpopulateComponentLandPatternTable = unpopulateComponentLandPatternTable;
    
    jbInit();
  } 
  
  /*
   * @author Kee Chin Seong
   */
  public ImportComponentDialog(Frame parent, String title, com.axi.v810.business.panelSettings.Project project, 
                               ComponentLandPatternTable populateComponentLandPatternTable,
                               ComponentLandPatternTable unpopulateComponentLandPatternTable)
  {
    super(parent, title, true);
     
    Assert.expect(parent != null);
    
    _parentFrame = parent;
    _project = project;
    
    _populateComponentLandPatternTable = populateComponentLandPatternTable;
    _unpopulateComponentLandPatternTable = unpopulateComponentLandPatternTable;
    
    jbInit();
  } 
  
  /**
  * @author chin-seong, kee
  */
  private void jbInit()
  {
    _mainPanel = new JPanel();
    _topInnerPanel = new JPanel();
    _centerInnerPanel = new JPanel();
    _bottomInnerPanel = new JPanel();
    
    _readDataReadOnlyTextArea = new JTextArea();
    _readDataScrollPane = new JScrollPane(_readDataReadOnlyTextArea);
    
    _mainPanel.setLayout(new BorderLayout());
    _topInnerPanel.setLayout(new BorderLayout());
    _centerInnerPanel.setLayout(new BorderLayout());
    _bottomInnerPanel.setLayout(new BorderLayout());
    
    JPanel topInnerButtonPanel = new JPanel();
    JPanel topInnerDataPanel = new JPanel();
    
    topInnerButtonPanel.setLayout(new PairLayout());
    topInnerDataPanel.setLayout(new PairLayout());
    
    _browsePathButton = new JButton(StringLocalizer.keyToString("PLWK_BROWSE_BUTTON_KEY"));
    _browsePathButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        browseButton_actionPerformed();
      }
    });
    
    _readFileButton= new JButton(StringLocalizer.keyToString("PLWK_DONE_BUTTON_KEY"));
    _readFileButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        readGerbelFile_actionPerformed();
      }
    });
    
    topInnerButtonPanel.add(_browsePathButton);
    topInnerButtonPanel.add(_readFileButton);
    
    _selectedFilePathTextField = new JTextField();
    _selectedFilePathTextField.setPreferredSize(new Dimension(700, 100)); 
    
    topInnerDataPanel.add(_selectedFilePathTextField);
    topInnerDataPanel.add(topInnerButtonPanel);
    
    _topInnerPanel.add(topInnerDataPanel, BorderLayout.NORTH);
    
    _readDataReadOnlyTextArea.setBorder(_panelBorder);
    _readDataReadOnlyTextArea.setEditable(true);
    _readDataReadOnlyTextArea.setName(".readDataTextArea");
    
    _topInnerPanel.add(_readDataScrollPane, BorderLayout.CENTER);
    _topInnerPanel.setPreferredSize(new Dimension(900, 300)); 
    
    //setup combobx
    _seperateSymbolChooseComboBox = new JComboBox();
    
    _seperateSymbolChooseComboBox.addItem(", [comma]\t\t");
    _seperateSymbolChooseComboBox.addItem("  [tab]\t\t");
    _seperateSymbolChooseComboBox.addItem("  [space]\t\t");
    
    _seperateSymbolChooseComboBox.setEnabled(false);
    
    _seperateSymbolChooseComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _headerLineEnd = 0;
        seperateDataWithSymbol_actionPerformed();
      }
    });
   
    JPanel innerChooserPanel = new JPanel();
    JPanel innerAngleChooserPanel = new JPanel();
    JPanel innerScaleChooserPanel = new JPanel();
    JPanel innerUnitsChooserPanel = new JPanel();
    JPanel innerSubMainChooserPanel = new JPanel();
    JPanel innerSubMain2ChooserPanel = new JPanel();
    JPanel innerSeperateSymbolChooserPanel = new JPanel();
    
    innerChooserPanel.setLayout(new PairLayout());
    innerSeperateSymbolChooserPanel.setLayout(new PairLayout());
    innerAngleChooserPanel.setLayout(new PairLayout());
    innerScaleChooserPanel.setLayout(new PairLayout());
    innerUnitsChooserPanel.setLayout(new PairLayout());
    innerSubMainChooserPanel.setLayout(new PairLayout());
    innerSubMain2ChooserPanel.setLayout(new PairLayout());
    
    innerSeperateSymbolChooserPanel.add(_seperateSymbolChooserLbl);
    innerSeperateSymbolChooserPanel.add(_seperateSymbolChooseComboBox);
    innerUnitsChooserPanel.add(_unitsLabel);
    innerUnitsChooserPanel.add(_unitsChooseComboBox);
    innerScaleChooserPanel.add(_scaleLabel);
    innerScaleChooserPanel.add(_scaleChooseComboBox);
    innerAngleChooserPanel.add(_angleLabel);
    innerAngleChooserPanel.add(_angleChooseComboBox);
    
    innerSubMainChooserPanel.add(innerSeperateSymbolChooserPanel);
    innerSubMainChooserPanel.add(innerUnitsChooserPanel);
    innerSubMain2ChooserPanel.add(innerScaleChooserPanel);
    innerSubMain2ChooserPanel.add(innerAngleChooserPanel);
    
    innerChooserPanel.add(innerSubMainChooserPanel);
    innerChooserPanel.add(innerSubMain2ChooserPanel);
            
    _centerInnerPanel.add(innerChooserPanel, BorderLayout.CENTER);

    _gerbelDataTableModel = new GerbelDataTableModel();     
    _gerbelDataTable = new GerbelDataTable(_gerbelDataTableModel);
    
    _resultsScrollPane = new JScrollPane(_gerbelDataTable);
    
    _setAsHeaderButton = new JButton("Set As Header");
    _setAsHeaderButton.setEnabled(false);
    _setAsHeaderButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _headerLineEnd = _gerbelDataTable.getSelectedRow();
        seperateDataWithSymbol_actionPerformed();
      }
    });
    
    _okButton = new JButton("OK");
    _okButton.setEnabled(false);
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        createComponent_actionPerformed();
      }
    });
    
    _cancelButton = new JButton("Cancel");
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelAction();
      }
    });

    JPanel innerBottomButtonSetPanel = new JPanel();
    innerBottomButtonSetPanel.setLayout(new BorderLayout());
    innerBottomButtonSetPanel.add(_setAsHeaderButton, BorderLayout.CENTER);
    innerBottomButtonSetPanel.add(_okButton, BorderLayout.SOUTH);
    
    _bottomInnerPanel.add(_resultsScrollPane, BorderLayout.CENTER);
    _bottomInnerPanel.add(innerBottomButtonSetPanel, BorderLayout.EAST);
    
    _topInnerPanel.setBorder(BorderFactory.createTitledBorder(StringLocalizer.keyToString("CC_READ_GERBEL_KEY")));
    _centerInnerPanel.setBorder(BorderFactory.createTitledBorder(StringLocalizer.keyToString("CC_DATA_EXTRACT_SETTINGS_KEY")));
    _bottomInnerPanel.setBorder(BorderFactory.createTitledBorder(StringLocalizer.keyToString("CC_EXTRACED_DATA_KEY")));
    _mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    
    _mainPanel.add(_topInnerPanel, BorderLayout.NORTH);
    _mainPanel.add(_centerInnerPanel, BorderLayout.CENTER);
    _mainPanel.add(_bottomInnerPanel, BorderLayout.SOUTH);
    
    getContentPane().add(_mainPanel);
    //setPreferredSize(new Dimension(1000, 900)); 
    pack();
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void browseButton_actionPerformed()
  {
    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

    if (fileChooser.showSaveDialog(this) != JFileChooser.APPROVE_OPTION)
    {
      return;
    }
    File selectedFile = fileChooser.getSelectedFile();
    String selectedFileName = selectedFile.getPath();

    _selectedFilePathTextField.setText(selectedFileName);
  }
 
  
  /*
   * @Author Kee Chin Seong
   */
  private void createComponent_actionPerformed()
  {    
    busyCancelDialog = new BusyCancelDialog(this,
                                             StringLocalizer.keyToString("CAD_CREATOR_IMPORT_COMPONENT_MESSAGE_KEY"),
                                             StringLocalizer.keyToString("CAD_CREATOR_IMPORT_COMPONENT_TITLE_KEY"),
                                             true);

    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, this);
    _importComponentThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
            com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().beginCommandBlock("abc");
            for (int count = _headerLineEnd; count < _splittedData.size(); count ++)
            {
                if(_headerLineEnd == -1)
                {
                   JOptionPane.showMessageDialog(_parentFrame,
                                                  StringLocalizer.keyToString("CAD_CREATOR_EMPTY_FILE_IS_IMPORTED_MESSAGE_KEY"),
                                                  StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                  JOptionPane.INFORMATION_MESSAGE);
                    
                    com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().endCommandBlock();
                    return; 
                }
                try
                {
                    Double.parseDouble(_splittedData.get(count).get(6).trim());
                }
                catch(NumberFormatException e)
                {
                  /// pop dialog about duplicate name
                  JOptionPane.showMessageDialog(_parentFrame,
                                                  StringLocalizer.keyToString("CAD_CREATOR_INVALID_FORMAT_FILE_IMPORTED_KEY"),
                                                  StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                  JOptionPane.INFORMATION_MESSAGE);
                  com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().endCommandBlock();
                  return;
                }
                String refDes = _splittedData.get(count).get(0).trim();
                if (refDes.length() > 0)
                {
                  LocalizedString message = null;
                  if(_project.getPanel().getBoardTypes().size() <= 0)
                  {
                    JOptionPane.showMessageDialog(_parentFrame,
                                                  StringLocalizer.keyToString("CAD_CREATOR_BOARD_IS_NOT_DEFINED_MESSAGE_KEY"),
                                                  StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                  JOptionPane.INFORMATION_MESSAGE);
                    
                    com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().endCommandBlock();
                    return;
                  }
                  BoardType boardType = _project.getPanel().getBoardTypes().get(0);

                  if (boardType.isComponentTypeReferenceDesignatorValid(refDes) == false)
                  {
                    // pop dialog about invalid name
                    String illegalChars = boardType.getComponentTypeReferenceDesignatorIllegalChars();
                    
                    if (illegalChars.equals(" "))
                      message = new LocalizedString("CAD_INVALID_COMPONENT_NAME_SPACES_KEY", new Object[]
                                                    {refDes});
                    else
                      message = new LocalizedString("CAD_INVALID_COMPONENT_NAME_OTHER_CHARS_KEY", new Object[]
                                                    {refDes, illegalChars});
                    JOptionPane.showMessageDialog(_parentFrame,
                                                  StringLocalizer.keyToString(message),
                                                  StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                  JOptionPane.INFORMATION_MESSAGE);
                    com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().endCommandBlock();
                    return;
                  }
                  else if (boardType.isComponentTypeReferenceDesignatorDuplicate(refDes))
                  {
                      // pop dialog about duplicate name
                      JOptionPane.showMessageDialog(_parentFrame,
                                                    StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
                                                    new Object[]
                                                    {refDes})),
                                                    StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                    JOptionPane.INFORMATION_MESSAGE);
                    com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().endCommandBlock();
                    return;
                  }
                  else
                  {
                    
                    LandPattern landPattern = null;//_useLandPattern;
                    
                    for(LandPattern lp : _project.getPanel().getLandPatterns())
                    {
                        if(_splittedData.get(count).size() < 2)
                        {
                            // pop dialog about duplicate name
                            JOptionPane.showMessageDialog(_parentFrame,
                                                    StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
                                                    new Object[]
                                                    {refDes})),
                                                    StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                    JOptionPane.INFORMATION_MESSAGE);
                            com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().endCommandBlock();
                            return;
                        }
                            
                       if(lp.getName().indexOf(_splittedData.get(count).get(2)) >= 0)
                       {
                          landPattern = lp;
                           break;
                       }
                    }
                    if(landPattern == null)
                       landPattern = _project.getPanel().getLandPattern(StringLocalizer.keyToString("CAD_UNPOPULATED_LAND_PATTERN_KEY"));
                           
                    BoardCoordinate newCoordinate = new BoardCoordinate((int)MathUtil.convertUnits(Double.parseDouble(_splittedData.get(count).get(4).trim()), MathUtilEnum.MILLIMETERS, MathUtilEnum.NANOMETERS), 
                                                                        (int)MathUtil.convertUnits(Double.parseDouble(_splittedData.get(count).get(5).trim()), MathUtilEnum.MILLIMETERS, MathUtilEnum.NANOMETERS));
                    
                    double rotation =  0;
                    try
                    {
                        Double.parseDouble(_splittedData.get(count).get(6).trim());
                    }
                    catch(NumberFormatException e)
                    {
                      /// pop dialog about duplicate name
                      JOptionPane.showMessageDialog(_parentFrame,
                                                StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
                                                new Object[]
                                                {refDes})),
                                                StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                JOptionPane.INFORMATION_MESSAGE);
                      com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().endCommandBlock();
                      return;
                    }
                    String boardSide = (_splittedData.get(count).get(7).trim().equals(StringLocalizer.keyToString("CAD_BOARD_SIDE_TOP_LABEL_KEY"))) ? 
                                        StringLocalizer.keyToString("CAD_SIDE1_KEY") : StringLocalizer.keyToString("CAD_SIDE2_KEY");
                    
                    SideBoardType sideBoardType;

                    if (boardSide.equals(StringLocalizer.keyToString("CAD_SIDE1_KEY")))
                    {
                      if (boardType.sideBoardType1Exists() == false)
                        boardType.getBoards().get(0).createSecondSideBoard();
                      sideBoardType = boardType.getSideBoardType1();
                    }
                    else
                    {
                      if (boardType.sideBoardType2Exists() == false)
                        boardType.getBoards().get(0).createSecondSideBoard();
                      sideBoardType = boardType.getSideBoardType2();
                    }

                    try
                    {
                      //_commandManager.trackState(getCurrentUndoState());
                      com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().execute(new CreateComponentTypeCommand(sideBoardType, refDes, landPattern, newCoordinate, rotation));
                    }
                    catch (XrayTesterException ex)
                    {
                      MessageDialog.showErrorDialog(_parentFrame, ex.getLocalizedMessage(),
                                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
                    }
                  }
                }
            }
            com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().endCommandBlock();
        }
        finally
        {
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
       }
       busyCancelDialog.setVisible(false);
     }
    }
   });
    busyCancelDialog.setVisible(true);
  }

  
  /*
   * @author Kee Chin Seong
   */
  private void seperateDataWithSymbol_actionPerformed()
  {
    String regex = "";
    _splittedData.clear();
    
    if(_seperateSymbolChooseComboBox.getSelectedIndex() == 0)
       regex = ",";
    else if(_seperateSymbolChooseComboBox.getSelectedIndex() == 1)
       regex = " ";
    else
       regex = "\t";
        
    for(int count = _headerLineEnd; count < _dataInGerbel.size(); count++)
    {
      String str = _dataInGerbel.get(count);
      String[] splittedData = str.split(regex);
      
      for(String splitResults : splittedData)
      {
        splitResults.trim();
      }
      java.util.List<String> results = new ArrayList<String>();
      java.util.Collections.addAll(results, splittedData);
      
      _splittedData.add(results);
    }
    
    _gerbelDataTableModel.populateGerbelData(_splittedData);
    _gerbelDataTableModel.fireTableDataChanged();
  }

  /*
   * @author Kee Chin Seong
   */
  private void readGerbelFile_actionPerformed()
  {
    _dataInGerbel.clear();
    _dataRead = "";
    
    busyCancelDialog = new BusyCancelDialog(this,
                                             StringLocalizer.keyToString("CAD_CREATOR_IMPORT_COMPONENT_MESSAGE_KEY"),
                                             StringLocalizer.keyToString("CAD_CREATOR_IMPORT_COMPONENT_TITLE_KEY"),
                                             true);

    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, this);
    _importComponentThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try 
        {
          BufferedReader in = new BufferedReader(new FileReader(_selectedFilePathTextField.getText()));
          String str;

          while ((str = in.readLine()) != null) 
          {
            _dataInGerbel.add(str);

            _dataRead = _dataRead + str;
            _dataRead = _dataRead + "\n";
          }
           in.close();
        }
        catch(IOException e) 
        {
        }
        finally
        {
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.setVisible(false);
        }
      }
    });
    busyCancelDialog.setVisible(true);

    _readDataReadOnlyTextArea.setText(_dataRead);
    _okButton.setEnabled(true);
    _setAsHeaderButton.setEnabled(true);
    _seperateSymbolChooseComboBox.setEnabled(true);
  }
  
  
  /**
   * @author Kee Chin Seong
   */
  private void cancelAction()
  {
    _returnValue = JOptionPane.CANCEL_OPTION;
    dispose();
  }
  
  /**
   * @return int
   * @author chin-seong.kee
   */
  public int showDialog()
  {
    SwingUtils.centerOnComponent(this, _parentFrame);
    setVisible(true);
    return _returnValue;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public synchronized void update(final Observable observable, final Object object)
  {
      
  }
}
