package com.axi.v810.gui.cadCreator;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import java.util.List;

/**
 *
 * @author Kee Chin Seong
 */
public class ComponentLandPatternCellEditor extends DefaultCellEditor
{
  private ComponentLandPatternTableModel _componentTableModel;
  private List<LandPattern> _landPattern;

  /**
   * @author Kee Chin Seong
   */
  public ComponentLandPatternCellEditor(JComboBox comboBox, ComponentLandPatternTableModel componentTableModel, List<LandPattern> landPattern)
  {
    super(comboBox);

    Assert.expect(componentTableModel != null);
    _componentTableModel = componentTableModel;
    _landPattern = landPattern;
  }

  /*
   * @author Kee Chin Seong 
   */
  public java.awt.Component getTableCellEditorComponent(JTable table, Object value, boolean isSelectable, int row, int column)
  {
    // get the editor (should be a JComboBox as was specified in the constructor)
    // then modify the contents of the combo box to list only the valid choices for this row in the table
    int selectedRows[] = table.getSelectedRows();

    java.awt.Component editor = super.getTableCellEditorComponent(table, value, isSelectable, row, column);
    JComboBox comboBox = (JComboBox)editor;
    comboBox.removeAllItems();
    // do the single selection first - easier if we get this out of the way
    if (selectedRows.length >= 0)
    {
      for(LandPattern lp : _landPattern)
        comboBox.addItem(lp.getName());
    }

    return comboBox;
  }
}
