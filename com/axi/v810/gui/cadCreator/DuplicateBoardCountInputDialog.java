package com.axi.v810.gui.cadCreator;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.v810.util.*;

/**
 *
 * @author chin seong, kee
 */
public class DuplicateBoardCountInputDialog extends EscapeDialog 
{
   private JPanel _dialogPanel;

  private JPanel _northPanel;
  private JLabel _warningLabel;
  private JLabel _promptLabel;

  private JPanel _centerPanel;
  private JPanel _labelPanel;
  private JLabel _boardCounterLabel;

  private JPanel _entryPanel;
  private JTextField _boardCountInput;


  private JPanel _southPanel;
  private JPanel _buttonPanel;
  private JButton _okButton;
  private JButton _cancelButton;

  private boolean _ok = false;
  
  private JFrame _frame = null;
  private int _boardCount = 0;
  
  public DuplicateBoardCountInputDialog(JFrame parent)
  {
     super(parent, StringLocalizer.keyToString("DUPLICATE_BOARD_COUNT_PROMPT_TITLE_KEY"), true);

     _frame = parent;
     _dialogPanel = new JPanel(new BorderLayout(20, 10));
     _dialogPanel.setBorder(BorderFactory.createEmptyBorder(10, 30, 10, 30));
     getContentPane().add(_dialogPanel);

     _northPanel = new JPanel(new BorderLayout(10, 10));
     _dialogPanel.add(_northPanel, BorderLayout.NORTH);

     // caps lock warning label (nothing is seen if caps lock is off)
     _warningLabel = new JLabel(" ");
     _warningLabel.setForeground(Color.RED);
     _warningLabel.setFont(FontUtil.getBoldFont(_warningLabel.getFont(),Localization.getLocale()));
     _warningLabel.setHorizontalAlignment(SwingConstants.CENTER);
     _northPanel.add(_warningLabel, BorderLayout.NORTH);

     _promptLabel = new JLabel(StringLocalizer.keyToString("DUPLICATE_BOARD_COUNT_PROMPT_LABEL_KEY"));
     _northPanel.add(_promptLabel, BorderLayout.CENTER);

     _centerPanel = new JPanel();
     _centerPanel.setLayout(new BorderLayout(5, 5));
     _dialogPanel.add(_centerPanel, BorderLayout.CENTER);

     _labelPanel = new JPanel(new GridLayout(1, 1, 5, 5));
     _centerPanel.add(_labelPanel, BorderLayout.WEST);

     _boardCounterLabel = new JLabel(StringLocalizer.keyToString("DUPLICATE_BOARD_COUNT_INPUT_KEY"));
     _labelPanel.add(_boardCounterLabel);

     _entryPanel = new JPanel(new GridLayout(1, 1, 5, 5));
     _centerPanel.add(_entryPanel, BorderLayout.CENTER);

     _boardCountInput =  new JTextField();
     _entryPanel.add(_boardCountInput);

     _southPanel = new JPanel(new BorderLayout());
     _dialogPanel.add(_southPanel, BorderLayout.SOUTH);

     _buttonPanel = new JPanel(new FlowLayout());
     _southPanel.add(_buttonPanel, BorderLayout.CENTER);

     _okButton = new JButton(StringLocalizer.keyToString("DUPLICATE_BOARD_OK_KEY"));
     _okButton.addActionListener(new java.awt.event.ActionListener()
     {
       public void actionPerformed(ActionEvent e)
       {
         _boardCount = Integer.parseInt(_boardCountInput.getText());
         dispose();
       }
     });
     _buttonPanel.add(_okButton);

     _cancelButton = new JButton(StringLocalizer.keyToString("DUPLICATE_BOARD_EXIT_KEY"));
     _cancelButton.addActionListener(new java.awt.event.ActionListener()
     {
       public void actionPerformed(ActionEvent e)
       {
         _boardCount = 0;
         dispose();
       }
     });
     _buttonPanel.add(_cancelButton);
     setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
     pack();
     SwingUtils.centerOnComponent(this, parent);

     // there is a bug with getLockingKeyState that causes it not to work after a window is deactivated.
     // this is a workaround for that bug
     addWindowListener(new WindowAdapter()
     {
       public void windowActivated(WindowEvent e)
       {
         if (e.getID() == WindowEvent.WINDOW_ACTIVATED)
         {
           try
           {
             Robot robot = new Robot();
             robot.keyPress(KeyEvent.VK_CONTROL);
             robot.keyRelease(KeyEvent.VK_CONTROL);
             Thread.sleep(50);
           }
           catch (Exception ex)
           {
             // do nothing
           }

         }

       }
     });
  }
  
  /**
   * @author chin-seong.kee
   */
  public int showDialog()
  {
    SwingUtils.centerOnComponent(this, _frame);
    setVisible(true);
    return 0;
  }
  
  /*
   * @author chin-seong,kee
   *  @return Value of the Board Count
   */
  public int getBoardCount()
  {
    return _boardCount;
  }
}
