package com.axi.v810.gui.cadCreator;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;
/**
 *
 * <p>Title: PadArrayTable</p>
 * <p>Description: Table for adding an array of land pattern pads</p>
 * <p>Company:  Vitrox Technogies</p>
 * @author Kee Chin Seong
 * @version 1.0
 */
class PadArrayTable extends JTable
{
  //index numbers for the columns (indexes are hard coded, so moving the columns does not work)
  public static final int ROWS_COLUMN_INDEX = 0;
  public static final int COLUMNS_COLUMN_INDEX = 1;
  public static final int DX_COLUMN_INDEX = 2;
  public static final int DY_COLUMN_INDEX = 3;
  public static final int ROTATION_COLUMN_INDEX = 4;
  public static final int SHAPE_COLUMN_INDEX = 5;
  public static final int TYPE_COLUMN_INDEX = 6;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double ROWS_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double COLUMNS_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double DX_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double DY_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double SHAPE_COLUMN_WIDTH_PERCENTAGE = .20;
  private static final double TYPE_COLUMN_WIDTH_PERCENTAGE = .20;

  private NumericRenderer _centerRenderer = new NumericRenderer(0, JLabel.CENTER);
  // this is used for num rows and num columns of pads so is not dependent of project units
  // maximum number of pads 22500
  private NumericEditor _centerIntNumericEditor = new NumericEditor(getBackground(),
                                                                    getForeground(),
                                                                    JTextField.CENTER,
                                                                    0,
                                                                    0,
                                                                    150);
  // this is used for the pad x and y dimensions
  private NumericEditor _centerDoubleNumericEditor;
  private ShapeCellEditor _shapeEditor = new ShapeCellEditor();
  private SMorTHPadTypeCellEditor _typeEditor = new SMorTHPadTypeCellEditor();
  private RotationCellEditor _rotationEditor;

  /**
   * Default constructor.
   * @author Kee Chin Seong
   */
  public PadArrayTable(Component parent, MathUtilEnum displayUnits)
  {
    Assert.expect(parent != null);
    // set max acceptable value to the project units equivalent of MAX INT value in nanometers
    _centerDoubleNumericEditor = new NumericEditor(getBackground(),
                                                   getForeground(),
                                                   JTextField.CENTER,
                                                   0,
                                                   MathUtil.convertUnits(2, // using 2 nanos here to get something slightly more than 0
                                                                         MathUtilEnum.NANOMETERS,
                                                                         displayUnits),
                                                   MathUtil.convertUnits(Integer.MAX_VALUE,
                                                                         MathUtilEnum.NANOMETERS,
                                                                         displayUnits));
    setRowSelectionAllowed(false);
    setColumnSelectionAllowed(false);
    getTableHeader().setReorderingAllowed(false);
    setDecimalPlacesToDisplay(MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits));
    _rotationEditor = new RotationCellEditor(parent, true);
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Kee Chin Seong
   */
  public void setPreferredColumnWidths()
  {
    final TableColumnModel columnModel = this.getColumnModel();

    final int totalColumnWidth = columnModel.getTotalColumnWidth();

    int rowsColWidth = (int)(totalColumnWidth * ROWS_COLUMN_WIDTH_PERCENTAGE);
    int columnsColWidth = (int)(totalColumnWidth * COLUMNS_COLUMN_WIDTH_PERCENTAGE);
    int dxColWidth = (int)(totalColumnWidth * DX_COLUMN_WIDTH_PERCENTAGE);
    int dyColWidth = (int)(totalColumnWidth * DY_COLUMN_WIDTH_PERCENTAGE);
    int shapeColWidth = (int)(totalColumnWidth * SHAPE_COLUMN_WIDTH_PERCENTAGE);
    int typeColWidth = (int)(totalColumnWidth * TYPE_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(ROWS_COLUMN_INDEX).setPreferredWidth(rowsColWidth);
    columnModel.getColumn(COLUMNS_COLUMN_INDEX).setPreferredWidth(columnsColWidth);
    columnModel.getColumn(DX_COLUMN_INDEX).setPreferredWidth(dxColWidth);
    columnModel.getColumn(DY_COLUMN_INDEX).setPreferredWidth(dyColWidth);
    columnModel.getColumn(SHAPE_COLUMN_INDEX).setPreferredWidth(shapeColWidth);
    columnModel.getColumn(TYPE_COLUMN_INDEX).setPreferredWidth(typeColWidth);

  }

  /**
   * Overridden method. Gets the editor for the specified row and column.  This
   * method should only be called for cells that are editible.
   * @param row index of the row for the cell
   * @param column index of the column for the cell
   * @return The appropriate editor for the cell specified
   * @author Kee Chin Seong
   */
  public TableCellEditor getCellEditor(final int row, final int column )
  {
    Assert.expect(row >= 0);
    Assert.expect(column >= 0);

    TableCellEditor editor = null;

    //all rows in a given column use the same editor
    switch(column)
    {
      case ROWS_COLUMN_INDEX:
        editor = _centerIntNumericEditor;
        break;
      case COLUMNS_COLUMN_INDEX:
        editor = _centerIntNumericEditor;
        break;
      case DX_COLUMN_INDEX:
        editor = _centerDoubleNumericEditor;
        break;
      case DY_COLUMN_INDEX:
        editor = _centerDoubleNumericEditor;
        break;
      case ROTATION_COLUMN_INDEX:
        editor = _rotationEditor;
        break;
      case SHAPE_COLUMN_INDEX:
        editor = _shapeEditor;
        break;
      case TYPE_COLUMN_INDEX:
        editor = _typeEditor;
        break;
      default:
        Assert.expect(false);
        //TableCellEditor cellEditor = this.getDefaultEditor(getValueAt(row, column).getClass());
        //return cellEditor;
        break;
    }

    Assert.expect(editor != null);
    return editor;
  }

  /**
   * Overridden method.  Returns the renderer for the specified cell.
   * This is for data display only, not for editing
   * @param row specified row for the cell
   * @param column specified column for the cell
   * @return The appropriate cell renderer for the specified cell.
   * @author Kee Chin Seong
   */
  public TableCellRenderer getCellRenderer(final int row, final int column)
  {
    Assert.expect(row >= 0);
    Assert.expect(column >= 0);

    TableCellRenderer renderer = null;

    switch(column)
    {
       case ROWS_COLUMN_INDEX:
        renderer = _centerRenderer;
        break;
      case COLUMNS_COLUMN_INDEX:
        renderer = _centerRenderer;
        break;
      case DX_COLUMN_INDEX:
        renderer = _centerRenderer;
        break;
      case DY_COLUMN_INDEX:
        renderer = _centerRenderer;
        break;
      case ROTATION_COLUMN_INDEX:
        renderer = _centerRenderer;
        break;
      case SHAPE_COLUMN_INDEX:
        renderer = _centerRenderer;
        break;
      case TYPE_COLUMN_INDEX:
        renderer = _centerRenderer;
        break;
      default:
        Assert.expect(false);
    }

    Assert.expect(renderer != null);
    return renderer;
  }

  /**
   * The decimal places to be displayed can change based on the measurement value that
   * is being displayed.  This method changes the renderers/editors to display the appropriate
   * number of decimal places.
   * @param decimalPlaces Number of decimal places to be displayed for all measurement values in the table.
   * @author Kee Chin Seong
   */
  public void setDecimalPlacesToDisplay(int decimalPlaces)
  {
    Assert.expect(decimalPlaces >= 0);

    _centerDoubleNumericEditor.setDecimalPlaces(decimalPlaces);
  }
}
