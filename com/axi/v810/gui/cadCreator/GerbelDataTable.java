package com.axi.v810.gui.cadCreator;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;

/**
 * @author chin-seong.kee
 */
public class GerbelDataTable extends JSortTable
{
  public static final int _LINE_NUMBER_INDEX = 0;
  public static final int _REF_DES_INDEX = 1;
  public static final int _X_INDEX = 2;
  public static final int _Y_INDEX = 3;
  public static final int _PART_NUMBER_INDEX = 4;
  public static final int _PACKAGE_STYLE_INDEX = 5;
  public static final int _ANGLE_INDEX = 6;
  public static final int _FEEDER_INDEX = 7; 
  public static final int _SIDE_INDEX = 8;
  
  private final double _LINE_NUMBER_COLUMN_WIDTH_PERCENTAGE = 0.1;
  private final double _REF_DES_COLUMN_WIDTH_PERCENTAGE = 0.3;
  private final double _X_COLUMN_WIDTH_PERCENTAGE = 0.2;
  private final double _Y_COLUMN_WIDTH_PERCENTAGE = 0.1;
  private final double _PART_NUMBER_COLUMN_WIDTH_PERCENTAGE = 0.1;
  private final double _PACKAGE_STYLE_COLUMN_WIDTH_PERCENTAGE = 0.1;
  private final double _ANGLE_COLUMN_WIDTH_PERCENTAGE = 0.1;
  private final double _FEEDER_COLUMN_WIDTH_PERCENTAGE = 0.1;
  private final double _SIDE_COLUMN_WIDTH_PERCENTAGE = 0.1;
  

  private GerbelDataTableModel _gerbelDataTableModel;

  /**
   * @param model
   * @author Chin Seong
   */
  public GerbelDataTable(GerbelDataTableModel model)
  {
    Assert.expect(model != null);

    super.setModel(model);
    
    _gerbelDataTableModel = model;
   
    jbInit();
  }

  private void jbInit()
  {
    setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    setSurrendersFocusOnKeystroke(false);
    sizeColumnsToFit(-1);
    setEnabled(true);
  }
  
  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Chin Seong, Kee
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_LINE_NUMBER_INDEX).setPreferredWidth((int)(totalColumnWidth * _LINE_NUMBER_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_REF_DES_INDEX).setPreferredWidth((int)(totalColumnWidth * (_REF_DES_COLUMN_WIDTH_PERCENTAGE)));
    columnModel.getColumn(_X_INDEX).setPreferredWidth((int)(totalColumnWidth * _X_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_Y_INDEX).setPreferredWidth((int)(totalColumnWidth * _Y_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PART_NUMBER_INDEX).setPreferredWidth((int)(totalColumnWidth * _PART_NUMBER_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PACKAGE_STYLE_INDEX).setPreferredWidth((int)(totalColumnWidth * _PACKAGE_STYLE_COLUMN_WIDTH_PERCENTAGE)); 
    columnModel.getColumn(_ANGLE_INDEX).setPreferredWidth((int)(totalColumnWidth * _ANGLE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_FEEDER_INDEX).setPreferredWidth((int)(totalColumnWidth * _FEEDER_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_SIDE_INDEX).setPreferredWidth((int)(totalColumnWidth * _SIDE_COLUMN_WIDTH_PERCENTAGE));
  }
  /**
   * @return
   * @author Chin Seong, Kee
   */
  public void setModel(GerbelDataTableModel gerbelTableModel)
  {
    Assert.expect(gerbelTableModel != null);

    _gerbelDataTableModel = gerbelTableModel;
    super.setModel(_gerbelDataTableModel);
  }

  /**
   * @return
   * @author Chin Seong, Kee
   */
  public GerbelDataTableModel getTableModel()
  {
    Assert.expect(_gerbelDataTableModel != null);
    return _gerbelDataTableModel;
  }

  /**
   * Overriding this method allows us to have one cell's value change apply
   * to every selected row in the table.  Multi-cell editing requires this.
   * @author Chin Seong, Kee
   */
  public void setValueAt(Object value, int row, int column)
  {
    // if there is no value, exit out here -- there is no work to do
    if (value == null)
      return;
  }
}
