package com.axi.v810.gui.cadCreator;

import javax.swing.*;
import com.axi.v810.util.*;

/**
 *
 * @author Kee Chin Seong
 */
class SMorTHPadTypeCellEditor extends DefaultCellEditor
{
  //Shape strings
  private final String _SURFACEMOUNT = StringLocalizer.keyToString("CAD_SM_KEY");
  private final String _THROUGHHOLE = StringLocalizer.keyToString("CAD_TH_KEY");

  JComboBox _comboBox = null;

  /**
   * @author Kee Chin Seong
   * Constructor.
   * Initializes the drop down box with the shape values
   */
  public SMorTHPadTypeCellEditor()
  {
    super(new JComboBox());
    _comboBox = (JComboBox)getComponent();

    _comboBox.addItem(_SURFACEMOUNT);
    _comboBox.addItem(_THROUGHHOLE);
 }
}

