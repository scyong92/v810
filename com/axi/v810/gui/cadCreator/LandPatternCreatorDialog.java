package com.axi.v810.gui.cadCreator;


import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.ProjectChangeEvent;
import com.axi.v810.business.ProjectChangeEventEnum;
import com.axi.v810.business.ProjectObservable;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.GuiObservable;
import com.axi.v810.gui.MessageDialog;
import com.axi.v810.gui.SelectionEventEnum;
import com.axi.v810.gui.SwingWorkerThread;
import com.axi.v810.gui.drawCad.CreateNewCadPanel;
import com.axi.v810.gui.drawCad.LandPatternPadRenderer;
import com.axi.v810.gui.mainMenu.MainMenuGui;
import com.axi.v810.gui.testDev.EditHoleLocationDialog;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.text.DecimalFormat;
import java.util.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author chin-seong, kee
 * Screen Shot will be imported to here (dragged region) to create the land pattern
 */
public class LandPatternCreatorDialog extends JFrame implements Observer
{
  private Project _project;
  private com.axi.guiUtil.Renderer _landPatternRenderer = null;
  
  private JPanel _mainPanel = null;
  private JPanel _rightPanel = null;
  private JPanel _leftPanel = null;
  private JPanel _buttonPanel = null;
  
  private JPanel _leftCenterPanel = null;
  private JPanel _leftBottomPanel = null;

  private JCheckBox _drawRectangle = new JCheckBox("Rectangle");
  private JCheckBox _drawCircle = new JCheckBox("Circle");
  private ButtonGroup _buttonGroup = new ButtonGroup();
          
  private JButton _createPad = new JButton("Create Pad")
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };
  private JButton _deletePad = new JButton("Delete Pad");
  private JButton _addLandPatternPadInArray = new JButton("Add Pad Aray");
  private JButton _exitButton = new JButton("Exit");
  
  private GraphicsEngine _graphicsEngine = null;
 
  private CreateNewCadPanel _cadCreatorGraphicPanel = null;
  private Frame _parentFrame = null;
  
  private JSplitPane _contentPane;
  
  private LandPatternEditorDetailsTableModel _landPatternDetailsTableModel;
  private LandPatternEditorDetailsTable _landPatternDetailsTable;
  
  private LandPattern _currentLandPattern = null;
  private int _firstRow = -1, _lastRow = -1;
  
  private JScrollPane _landPatternDetailsScrollPane = new JScrollPane();
  
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  private SelectedRendererObservable _landPatternGraphicsSelectedRendererObservable;
  
  private LandPatternPad _landPatternPadFoundOnRightClick = null;
  
  private JFrame _frame = null;
  private LandPatternCreator _lpCreator = null;
 
  /**
   * @author chin-seong, kee
   */
  public LandPatternCreatorDialog(Frame parent, String title, Project project, 
                                  com.axi.guiUtil.Renderer renderer, GraphicsEngine graphicsEngine)
  {
    Assert.expect(parent != null);
    Assert.expect(title != null);
    Assert.expect(project != null);
    
    _parentFrame = parent;
    _project = project;
    _landPatternRenderer = renderer;
    _frame = this;
   
    jbInit();
 
    _landPatternGraphicsSelectedRendererObservable = _cadCreatorGraphicPanel.getLandPatternEditorGraphicsEngineSetup().getSelectedRendererObservable();
    _graphicsEngine = _cadCreatorGraphicPanel.getLandPatternEditorGraphicsEngineSetup().getGraphicsEngine();
    
    _projectObservable.addObserver(this);
    _guiObservable.addObserver(this);
    _landPatternGraphicsSelectedRendererObservable.addObserver(this);
    _graphicsEngine.getMouseClickObservable().addObserver(this);
     
  }
  
  /**
   * @author chin-seong, kee
   */
  public LandPatternCreatorDialog(Frame parent, String title, Project project, LandPattern landPattern,
                                  GraphicsEngine graphicsEngine)
  {
    Assert.expect(parent != null);
    Assert.expect(title != null);
    Assert.expect(project != null);
    Assert.expect(landPattern != null);
    
    _parentFrame = parent;
    _project = project;
    _currentLandPattern = landPattern;
    _frame = this;
   
    jbInit();
 
    _landPatternGraphicsSelectedRendererObservable = _cadCreatorGraphicPanel.getLandPatternEditorGraphicsEngineSetup().getSelectedRendererObservable();
    _graphicsEngine = _cadCreatorGraphicPanel.getLandPatternEditorGraphicsEngineSetup().getGraphicsEngine();
    
    _projectObservable.addObserver(this);
    _guiObservable.addObserver(this);
    _landPatternGraphicsSelectedRendererObservable.addObserver(this);
    _graphicsEngine.getMouseClickObservable().addObserver(this);
    
    setupCellEditorsAndRenderers();
    _landPatternDetailsTableModel.populateWithLandPattern(_currentLandPattern);
  }
  
  /*
  * @author Kee Chin Seong
  */
  public void displayLandPattern()
  {
    _cadCreatorGraphicPanel.displayLandPattern(_currentLandPattern);
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void setupCellEditorsAndRenderers()
  {
    Set<Double> customRotationValues = new HashSet<Double>();
    for (LandPatternPad lpPad : _currentLandPattern.getLandPatternPads())
    {
       customRotationValues.add(lpPad.getDegreesRotation());
    }
    _landPatternDetailsTable.setEditorsAndRenderers(customRotationValues);
  }
 
  /*
   * @author Kee Chin Seong
   */
  public void setLandPatternCreatorPanel(LandPatternCreator lpCreator)
  {
     Assert.expect(lpCreator != null);
     
     _lpCreator = lpCreator;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean promptLandPatternCreatorSettings()
  {
    String newLandPatternName = JOptionPane.showInputDialog(this, StringLocalizer.keyToString("CAD_ENTER_ITEM_NAME_KEY"),
    StringLocalizer.keyToString("CAD_NEW_LP_TITLE_KEY"), JOptionPane.INFORMATION_MESSAGE);
    if (newLandPatternName != null) // null means the dialog was canceled
    {
       com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
       while (newLandPatternName != null && (panel.isLandPatternNameValid(newLandPatternName) == false || panel.isLandPatternNameDuplicate(newLandPatternName)))
       {
         if(panel.isLandPatternNameValid(newLandPatternName) == false)
         {
           String illegalChars = panel.getLandPatternNameIllegalChars();
           LocalizedString message = null;
           if (illegalChars.equals(" "))
             message = new LocalizedString("CAD_INVALID_LAND_PATTERN_NAME_SPACES_KEY", new Object[]
                                           {newLandPatternName});
           else
             message = new LocalizedString("CAD_INVALID_LAND_PATTERN_NAME_OTHER_CHARS_KEY", new Object[]
                                           {newLandPatternName, illegalChars});

             newLandPatternName = JOptionPane.showInputDialog(this,
                                                              StringLocalizer.keyToString(message),
                                                              StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                              JOptionPane.INFORMATION_MESSAGE);
          }
          else if (panel.isLandPatternNameDuplicate(newLandPatternName))
          {
              
            if (panel.getAllLandPatterns() == null || panel.getAllLandPatterns().size() == 0)
            {
              //noProjectsMessage();
              return false;
            }
            String title = StringLocalizer.keyToString("MM_GUI_LOAD_PROJECT_TITLE_KEY");
            String message = StringLocalizer.keyToString("MM_GUI_LOAD_PROJECT_MESSAGE_KEY");
            Object[] possibleValues = panel.getAllLandPatterns().toArray();
            ChoiceInputDialog panelChoiceDialog = new ChoiceInputDialog(
                _parentFrame,
                title,
                message,
                StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                possibleValues);
            SwingUtils.centerOnComponent(panelChoiceDialog, _parentFrame);
            int returnValue = panelChoiceDialog.showDialog();
            if (returnValue == JOptionPane.OK_OPTION)
            {
              _currentLandPattern = (LandPattern)panelChoiceDialog.getSelectedValue();
              setupCellEditorsAndRenderers();
              _cadCreatorGraphicPanel.displayLandPattern(_currentLandPattern);
              _landPatternDetailsTableModel.populateWithLandPattern(_currentLandPattern);
              //_cadCreatorGraphicPanel.getLandPatternEditorGraphicsEngineSetup().setRendererShape(Project.getCurrentlyLoadedProject().getPanel().getShapeInNanoMeters());
              //_landPatternEditorrGraphicsEngineSetup.displayCroppedArea(_landPatternRenderer);
              return false;
            }
          }
        }
        if (newLandPatternName == null)
          return false; // dialog was canceled without a name entered

        try
        {
           //_commandManager.trackState(getCurrentUndoState());
           _commandManager.execute(new CreateLandPatternCommand(panel, newLandPatternName, false));
        }
        catch (XrayTesterException ex)
        {
           MessageDialog.showErrorDialog(_parentFrame,ex.getLocalizedMessage(),
                                         StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
        }
     }
    else
       return false;
    
    return true;
  }
  
  
  /*
   * @author Kee chin SEong
   */
  private void handleSelectedRendererEvent(Object object)
  {
    // We receive from the update, a collection of selected objects.
    /** Warning "unchecked cast" approved for cast from Object type
     *  when sent from SelectedRendererObservable notifyAll.  */
    for (com.axi.guiUtil.Renderer renderer : ((Collection < com.axi.guiUtil.Renderer > )object))
    {
       if (renderer instanceof LandPatternPadRenderer)
       {
          LandPatternPadRenderer padRenderer = (LandPatternPadRenderer)renderer;
          LandPatternPad landPatternPad = padRenderer.getLandPatternPad();
          int rowPosition = _landPatternDetailsTableModel.getRowForLandPatternPad(landPatternPad);
          _landPatternDetailsTable.addRowSelectionInterval(rowPosition, rowPosition);
          
          _landPatternPadFoundOnRightClick = landPatternPad;
       }
    }
    // after everything is selected, scroll to make the first selection visible.
    SwingUtils.scrollTableToShowSelection(_landPatternDetailsTable);
  }
  
  /*
   * @author KEe Chin Seong
   */
  private void deletePad()
  {
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_frame,
      StringLocalizer.keyToString("CAD_BUSYCANCELDIALOG_DELETE_PADS_MESSAGE_KEY"),
      StringLocalizer.keyToString("CAD_BUSYCANCELDIALOG_DELETE_PADS_TITLE_KEY"),
      true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _frame);
    busyCancelDialog.setCursor(new Cursor(Cursor.WAIT_CURSOR));
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        java.util.List<LandPatternPad> selectedPads = new ArrayList<LandPatternPad>();
        try
        {
          ProjectObservable.getInstance().setEnabled(false);
          if (_landPatternDetailsTable.getSelectedRow() != -1)
          {
            int selectedRows[] = _landPatternDetailsTable.getSelectedRows();
            //java.util.List<LandPatternPad> selectedPads = new ArrayList<LandPatternPad>();
            // first gather all the selected pads since the table model will be modified as deletion happens.
            LandPattern currentLandPattern = _landPatternDetailsTableModel.getLandPatternPadAt(selectedRows[0]).getLandPattern();
            int maxPadsToDelete = selectedRows.length;
            int landPatternPadNum = currentLandPattern.getLandPatternPads().size();
            Assert.expect(maxPadsToDelete <= landPatternPadNum);
            // don't allow the last pad to be deleted, basically don't leave an empty landpattern
            if (maxPadsToDelete == landPatternPadNum)
            {
              maxPadsToDelete--;
            }

            for (int i = 0; i < maxPadsToDelete; i++)
            {
              selectedPads.add(_landPatternDetailsTableModel.getLandPatternPadAt(selectedRows[i]));
            }
            cancelEditingIfNecessary(_landPatternDetailsTable);
            //_commandManager.trackState(getCurrentUndoState());
            _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_DESTROY_LANDPATTERN_PAD_KEY"));

            try
            {
              _commandManager.execute(new RemoveLandPatternPadCommand(selectedPads));
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(_frame, ex.getLocalizedMessage(),
                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            }

            _commandManager.endCommandBlock();
            //setDeletePadButtonState(currentLandPattern);
          }
        }
        finally
        {
          ProjectObservable.getInstance().setEnabled(true);          
          ProjectObservable.getInstance().stateChanged(this, selectedPads, null, LandPatternPadEventEnum.ADD_OR_REMOVE_LIST);
          
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              busyCancelDialog.dispose();
            }
          });
        }
      }
    });
    busyCancelDialog.setVisible(true);
  }
  
  /**
   * Override so we can clear out the _project reference
   * @author chin-seong, kee
   */
  private void createdPad() //throws XrayTesterException
  {
     if(_cadCreatorGraphicPanel.getLandPatternRenderer() == null)
     {
        JOptionPane.showMessageDialog(this,
                                     StringLocalizer.keyToString("CC_NO_SHAPE_DRAWED_ON_CROPPED_IMAGE_ERR_KEY"),
                                     StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                     JOptionPane.OK_OPTION);
       return;
     }
     try
     {
        java.awt.Shape shape = _cadCreatorGraphicPanel.getCurrentDragShape();
     
        ComponentCoordinate compCoordinate = new ComponentCoordinate((int)shape.getBounds2D().getCenterX(), (int)shape.getBounds2D().getCenterY());
        java.util.List compCoordinateArray = new ArrayList<ComponentCoordinate>();
        compCoordinateArray.add(compCoordinate);
        
        if(shape instanceof Rectangle2D)
           com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().execute(new CreateSurfaceMountLandPatternPadCommand(
                                                                              _currentLandPattern, compCoordinateArray,
                                                                              0.0, (int)shape.getBounds2D().getHeight(), (int)shape.getBounds2D().getWidth(), ShapeEnum.RECTANGLE));
       else
           com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().execute(new CreateSurfaceMountLandPatternPadCommand(
                                                                                 _currentLandPattern, compCoordinateArray,
                                                                                 0.0, (int)shape.getBounds2D().getHeight(), (int)shape.getBounds2D().getWidth(), ShapeEnum.CIRCLE));

      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(_frame, ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
      }
  }
  

  /**
   * Override so we can clear out the _project reference
   * @author chin-seong, kee
   */
  public void setVisible(boolean visible)
  {
    super.setVisible(visible);
  }
  
  /**
   * jbInit
   * @author chin-seong, kee
   */
  private void jbInit()
  {
    _cadCreatorGraphicPanel = new CreateNewCadPanel(_landPatternRenderer);
    //_cadCreatorGraphicPanel.drawCadPanel(_project);
    
    setLayout(new BorderLayout());
    
    _leftPanel = new JPanel();
    _rightPanel = new JPanel(); 
    _buttonPanel = new JPanel();
    _mainPanel = new JPanel();
    
    _leftCenterPanel = new JPanel();
    _leftBottomPanel = new JPanel();;
    
    //add(_mainPanel, BorderLayout.CENTER);
    
    add(_leftPanel, BorderLayout.EAST);
    add(_rightPanel, BorderLayout.WEST);
    //getContentPane().add(_buttonPanel,BorderLayout.WEST);
    
    _leftPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    _rightPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

    _leftPanel.setBorder(BorderFactory.createTitledBorder("Land Pattern Edit"));
    _rightPanel.setBorder(BorderFactory.createTitledBorder(""));
    _buttonPanel.setBorder(BorderFactory.createTitledBorder("Draw Shape"));
            
    _rightPanel.setLayout(new BorderLayout());
    _rightPanel.add(_cadCreatorGraphicPanel, BorderLayout.CENTER);
    
    _buttonPanel.setLayout(new BorderLayout());
    _buttonPanel.add(_drawRectangle, BorderLayout.CENTER);
    _buttonPanel.add(_drawCircle, BorderLayout.SOUTH);
    
    _leftPanel.setLayout(new BorderLayout());
    _leftPanel.add(_buttonPanel, BorderLayout.NORTH);
    _leftPanel.add(_leftCenterPanel, BorderLayout.CENTER);
    _leftPanel.add(_leftBottomPanel, BorderLayout.SOUTH);
    
    _buttonGroup.add(_drawRectangle);
    _buttonGroup.add(_drawCircle);
    
    //by default is RECTANGLE
    _drawRectangle.setSelected(true);
    
    _leftBottomPanel.setLayout(new GridLayout(2,2,10,10));
    
    _leftBottomPanel.add(_createPad);
    _leftBottomPanel.add(_deletePad);
    _leftBottomPanel.add(_addLandPatternPadInArray);
    _leftBottomPanel.add(_exitButton);
    
    _leftBottomPanel.setBorder(BorderFactory.createTitledBorder(""));
    
    _leftCenterPanel.add(_landPatternDetailsScrollPane);
    
    _drawRectangle.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _cadCreatorGraphicPanel.enabledRectangleDragShape(); 
      }
    });
    
    _drawCircle.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _cadCreatorGraphicPanel.enabledOvalDragShape();
      }
    });
    
    _createPad.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
         createdPad();
      }
    });
    
    _deletePad.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
         deletePad();
      }
    });
    
    _addLandPatternPadInArray.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
         createLandPatternArray();
      }
    });
    
    _exitButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
         cancelAction();
      }
    });
    
    _landPatternDetailsTableModel = new LandPatternEditorDetailsTableModel(MainMenuGui.getInstance());
    _landPatternDetailsTable = new LandPatternEditorDetailsTable(this)
    {
      // this overload allows multiple rows to be edited at once, for any one column
      public void setValueAt(Object value, int row, int column)
      {
        //_currentLandPattern = _landPatterns.get(_selectLandPatternComboBox.getSelectedIndex()-1);
        final int rows[] = getSelectedRows();
        if (rows.length > 0)
        {
          _firstRow = rows[0];
          _lastRow = rows[rows.length-1];
        }
        final int col = convertColumnIndexToModel(column);
       
        if (column == 0)
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_NAME_KEY"));
        else if (column == 1 || column == 2)
        {
          DecimalFormat twoDecimalPlaces = new DecimalFormat("#######.####");

          if(value instanceof Long)
          {
             value = Double.parseDouble(value.toString());
          }

          if((Double)value == Double.parseDouble(twoDecimalPlaces.format((Double)this.getValueAt(row, column))))
              return;

          Double longValue = (Double)value;

          Object[] options = {StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_ACTUAL_LOCATION_OPTION_KEY"), 
                              StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_OFFSET_FROM_ACTUAL_LOCATION_OPTION_KEY")}; 
          //chin-seong, kee - this is for Offset X, Y use, when need handle multiple pad in the sametime
          //                  for e.g. BGA can mutliple select whole region which user desired and make the offset
          //if (valStr.equalsIgnoreCase(StringLocalizer.keyToString("CAD_LAND_PATTERN_SET_LOCATION_KEY")))
          //{
          String prompt = StringLocalizer.keyToString(new LocalizedString("MMGUI_PAD_LAND_PATTERN_LOCATION_OR_OFFSET_KEY",
                                                  new Object[]{longValue}));
          String title = (column == 1) ? StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_LOCATION_OR_OFFSET_X_TITLE_KEY") :
                                          StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_LOCATION_OR_OFFSET_Y_TITLE_KEY");

          ChoiceRadioButtonDialog loadDialog = new ChoiceRadioButtonDialog(_frame,
                                                        title,
                                                        prompt,
                                                        StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                                        StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                                        options,
                                                        null);

          int retVal = loadDialog.showDialog();

          if (retVal == JOptionPane.OK_OPTION) 
          {
            if(loadDialog.getSelectedValue().equals(StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_ACTUAL_LOCATION_OPTION_KEY")) == true)
             this.setIsOffsetBeenSet(false);
            else if(loadDialog.getSelectedValue().equals(StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_OFFSET_FROM_ACTUAL_LOCATION_OPTION_KEY")) == true)
             this.setIsOffsetBeenSet(true);
          }
          else // the use hit cancel or entered an empty string -- change nothing
             return;


          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_COORD_NM_KEY"));
        }
        else if (column == 3)
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_WIDTH_NM_KEY"));
        else if (column == 4)
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_LENGTH_NM_KEY"));
        else if (column == 5)
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_ROTATION_KEY"));
        else if (column == 6)
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_SHAPE_KEY"));
        else if (column == 7)
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SWAP_TYPE_KEY"));
        else if (column == 8)
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_HOLE_DIAM_KEY"));
        
        // if user is setting multiple rows of column other than swapping TH/SM type
        if (col != 7 && _currentLandPattern.getLandPatternPads().size() <= 200)
        {
          for (int i = 0; i < rows.length; ++i)
          {
            if (col == 8)
            {
              // do not set value for SM pads
              if (_landPatternDetailsTableModel.getLandPatternPadAt(rows[i]).isSurfaceMountPad())
                continue;
            }
            //chin-seong, kee - this is for Offset X, Y use, when need handle multiple pad in the sametime
            //                  for e.g. BGA can mutliple select whole region which user desired and make the offset
            if (this.isOffsetBeenSet() == true)
            {
              _landPatternDetailsTableModel.setValueAt((Double)_landPatternDetailsTableModel.getValueAt(rows[i], col) - (Double)value, rows[i], col);
            }
            else
            {
              _landPatternDetailsTableModel.setValueAt(value, rows[i], col);
            }
            if (col!= 0 && col != 5 && col != 6 && col != 7) // these columns are DropDownList - Kee Chin Seong, column 0 is not allowed too !!
            {
              if (Float.parseFloat(_landPatternDetailsTableModel.getValueAt(rows[i], col).toString()) != Float.parseFloat(value.toString()))
                continue; // break from the loop as the newly assigned value isn't successfully successfully. - Ying-Huan.Chu
            }
          }
          _commandManager.endCommandBlockIfNecessary();
        }
        else 
        {
          //_projectObservable.setEnabled(false);
          final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_frame,
                                      StringLocalizer.keyToString("ECP_BUSYCANCELDIALOG_SETTING_TABLE_VALUES_MESSAGE_KEY"),
                                      StringLocalizer.keyToString("ECP_BUSYCANCELDIALOG_SETTING_TABLE_VALUES_TITLE_KEY"),
                                      true);
          busyCancelDialog.pack();
          SwingUtils.centerOnComponent(busyCancelDialog, _frame);
          busyCancelDialog.setCursor(new Cursor(Cursor.WAIT_CURSOR));
          final Object finalValue = value;
          SwingWorkerThread.getInstance().invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                for (int i = 0; i < rows.length; i++)
                {
                  if (col == 8)
                  {
                    // do not set value for SM pads
                    if (_landPatternDetailsTableModel.getLandPatternPadAt(rows[i]).isSurfaceMountPad())
                      continue;
                  }
                  //chin-seong, kee - this is for Offset X, Y use, when need handle multiple pad in the sametime
                  //                  for e.g. BGA can mutliple select whole region which user desired and make the offset
                  if(_landPatternDetailsTable.isOffsetBeenSet() == true)
                  {
                    _landPatternDetailsTableModel.setValueAt((Double)_landPatternDetailsTableModel.getValueAt(rows[i], col) - (Double)finalValue, rows[i], col);
                  }
                  else
                  {
                    _landPatternDetailsTableModel.setValueAt(finalValue, rows[i], col);
                  }
                  if (col != 0 && col != 5 && col != 6 && col != 7) // these columns are DropDownList
                  {
                    // Kee Chin Seong - need to check is NOT COLUMN 0 also as this column is allow string input
                    if (Float.parseFloat(_landPatternDetailsTableModel.getValueAt(rows[i], col).toString()) != Float.parseFloat(finalValue.toString()))
                      continue; // break from the loop as the newly assigned value isn't successfully set. - Ying-Huan.Chu
                  }
                }
              }
              finally
              {
                // wait until visible
                _commandManager.endCommandBlockIfNecessary();
                while (busyCancelDialog.isVisible() == false)
                {
                  try
                  {
                    Thread.sleep(200);
                  }
                  catch (InterruptedException ex)
                  {
                    // do nothing
                  }
                }
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    busyCancelDialog.dispose();
                  }
                });
              }
            }
          });
          busyCancelDialog.setVisible(true);
          // delete current land pattern
          _currentLandPattern.assignAll();
         // _projectObservable.setEnabled(true);
          
          // create new land pattern
//          _projectObservable.stateChanged(_currentLandPattern, null, _currentLandPattern, LandPatternEventEnum.CREATE_OR_DESTROY);
          


        }
        this.setIsOffsetBeenSet(false);
      }
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = getSelectedRows();
        java.util.List<Object> selectedData = new ArrayList<Object>();
        for (int i = 0; i < selectedRows.length; i++)
        {
          selectedData.add(_landPatternDetailsTableModel.getLandPatternPadAt(selectedRows[i]));
        }

        super.mouseReleased(event);

        // now, reselect everything
        _landPatternDetailsTable.clearSelection();
        Iterator it = selectedData.iterator();
        while (it.hasNext())
        {
          int row;
          Object obj = it.next();
          row = _landPatternDetailsTableModel.getRowForLandPatternPad((LandPatternPad)obj);
          addRowSelectionInterval(row, row);
        }
      }

    };
    
    addWindowListener(new java.awt.event.WindowAdapter() 
    {
        public void windowClosing(java.awt.event.WindowEvent windowEvent) 
        {
            cancelAction();
        }
    });
    _landPatternDetailsTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    //chin-seong, kee - this is for Offset X, Y use, when need handle multiple pad in the sametime
    //                  for e.g. BGA can mutliple select whole region which user desired and make the offset
    _landPatternDetailsTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        //multiEditTableMouseReleased(e, _landPatternDetailsTable);
      }
    });
    _landPatternDetailsTable.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        //pad_keyTyped(e);
      }
    });
    _landPatternDetailsTable.setModel(_landPatternDetailsTableModel);
    _landPatternDetailsScrollPane.getViewport().add(_landPatternDetailsTable, null);
    // add the listener for a table row selection
    ListSelectionModel rowSM = _landPatternDetailsTable.getSelectionModel();
    rowSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();

        if (lsm.isSelectionEmpty() || lsm.getValueIsAdjusting())
        {
          // disable edit menu item when no pads are selected
          //editHoleLocationMenuItemSetEnabled(false);
          //makePadOneMenuItemSetEnabled(false);
        }
        else
        {
          // we want to select the pad(s) in the graphics window corresponding to the newly selected table row(s)
          int selectedRows[] = _landPatternDetailsTable.getSelectedRows();
          java.util.List<LandPatternPad> landPatternPads = new ArrayList<LandPatternPad>();
          for (int i = 0; i < selectedRows.length; i++)
          {
            LandPatternPad landPatternPad = _landPatternDetailsTableModel.getLandPatternPadAt(selectedRows[i]);
            landPatternPads.add(landPatternPad);
          }
          _landPatternDetailsTableModel.setSelectedPads(landPatternPads);

          // enable edit menu item when a pad is selected
          if (landPatternPads.size() == 1)
          {
            //makePadOneMenuItemSetEnabled(true);
            //if (landPatternPads.get(0) instanceof ThroughHoleLandPatternPad)
              //editHoleLocationMenuItemSetEnabled(true);
          }
          else
          {
            //editHoleLocationMenuItemSetEnabled(false);
            //makePadOneMenuItemSetEnabled(false);
          }

          //if (_shouldInteractWithGraphics)
          _guiObservable.stateChanged(landPatternPads, SelectionEventEnum.LANDPATTERN_PAD);
        }
      }
    });
    _contentPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, _leftPanel, _rightPanel);
    _contentPane.setContinuousLayout(true);
    _contentPane.setDividerLocation(500);

    //add(_editCadTabbedPane, BorderLayout.CENTER);
    add(_contentPane, BorderLayout.CENTER);
    setSize(new Dimension(1400,900));
    
    _cadCreatorGraphicPanel.setEnableSelectRegionToggleButton(true);
 
    //pack();
  }
  
 
  /*
   * @author Kee Chin Seongn
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof SelectedRendererObservable)
           handleSelectedRendererEvent(object);
        else if (observable instanceof ProjectObservable)
           handleProjectDataEvent(object);
        else if (observable instanceof MouseClickObservable)
           handleMouseClickEvent(object);
        else if (observable instanceof GuiObservable)
           _cadCreatorGraphicPanel.displayLandPattern(_currentLandPattern);
          //handleGuiChangesEvent(object);
        else if (observable instanceof com.axi.v810.gui.undo.CommandManager)
            System.out.println("HandleUndoState");
          //handleUndoState(object);
        else
          Assert.expect(false);
      }
    });
  }
  
  private void handleProjectDataEvent(Object object)
  {
    Assert.expect(object != null);

    if (object instanceof ProjectChangeEvent)
    {
       ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)object;
       ProjectChangeEventEnum projectChangeEventEnum = projectChangeEvent.getProjectChangeEventEnum();
       if ((projectChangeEventEnum instanceof LandPatternEventEnum) ||
            (projectChangeEventEnum instanceof LandPatternPadEventEnum) ||
            (projectChangeEventEnum instanceof ThroughHoleLandPatternPadEventEnum) ||
            (projectChangeEventEnum instanceof ComponentEventEnum) ||
            (projectChangeEventEnum instanceof ComponentTypeEventEnum) ||
            (projectChangeEventEnum instanceof FiducialEventEnum) ||
            (projectChangeEventEnum instanceof FiducialTypeEventEnum) ||
            (projectChangeEventEnum instanceof ProjectEventEnum) ||
            (projectChangeEventEnum instanceof ProjectStateEventEnum))
       {
         // anything here should cause an update
         updateData(projectChangeEvent);
       }
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void updateData(ProjectChangeEvent projectChangeEvent)
  {
    Assert.expect(projectChangeEvent != null);

    ProjectChangeEventEnum projectChangeEventEnum = projectChangeEvent.getProjectChangeEventEnum();

    if (projectChangeEventEnum instanceof LandPatternEventEnum)
    {
      LandPatternEventEnum landPatternEventEnum = (LandPatternEventEnum)projectChangeEventEnum;
      if (landPatternEventEnum.equals(LandPatternEventEnum.CREATE_OR_DESTROY) || landPatternEventEnum.equals(LandPatternEventEnum.ADD_OR_REMOVE))
      {
        Object oldValue = projectChangeEvent.getOldValue();
        Object newValue = projectChangeEvent.getNewValue();
        if (newValue != null) // this is a create event
        {
          Assert.expect(oldValue == null);
          Assert.expect(newValue instanceof LandPattern);
          LandPattern newLandPattern = (LandPattern)newValue;
          
          _currentLandPattern = newLandPattern;
          setupCellEditorsAndRenderers();
          _landPatternDetailsTableModel.populateWithLandPattern(_currentLandPattern);
          _cadCreatorGraphicPanel.displayLandPattern(_currentLandPattern);
          _cadCreatorGraphicPanel.displayCroppedArea(null);
          
        }
        if (oldValue != null) // this is a destroy event
        {
          Assert.expect(newValue == null);
          Assert.expect(oldValue instanceof LandPattern);
          LandPattern oldLandPattern = (LandPattern)oldValue;
        }
      }
      else
      {
      }
    }
    else if (projectChangeEventEnum instanceof LandPatternPadEventEnum)
    {
      LandPatternPadEventEnum landPatternPadEventEnum = (LandPatternPadEventEnum)projectChangeEventEnum;
      Object oldValue = projectChangeEvent.getOldValue();
      Object newValue = projectChangeEvent.getNewValue();

      if (landPatternPadEventEnum.equals(LandPatternPadEventEnum.CREATE_OR_DESTROY) || landPatternPadEventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE))
      {
        if (newValue != null) // this is a create event
        {
          Assert.expect(oldValue == null);
          Assert.expect(newValue instanceof LandPatternPad);
          LandPatternPad newLandPatternPad = (LandPatternPad)newValue;
          _landPatternDetailsTableModel.addLandPatternPad(newLandPatternPad);
        }
        if (oldValue != null) // this is a destroy event
        {
          Assert.expect(newValue == null);
          Assert.expect(oldValue instanceof LandPatternPad);
          LandPatternPad oldLandPatternPad = (LandPatternPad)oldValue;
          _landPatternDetailsTableModel.deleteLandPatternPad(oldLandPatternPad);
        }
      }
      else if (landPatternPadEventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE_LIST))
      {
        if (newValue != null) // this is a create event
        {
          Assert.expect(oldValue == null);
          Assert.expect(newValue instanceof ArrayList);
          java.util.List<LandPatternPad> landPatternPads = (java.util.ArrayList<LandPatternPad>) newValue;

          for (LandPatternPad landPatternPad : landPatternPads)
          {
            _landPatternDetailsTableModel.addLandPatternPad(landPatternPad);
          }
        }
        if (oldValue != null) // this is a destroy event
        {
          Assert.expect(newValue == null);
          Assert.expect(oldValue instanceof ArrayList);
          java.util.List<LandPatternPad> landPatternPads = (java.util.ArrayList<LandPatternPad>) oldValue;

          for (LandPatternPad landPatternPad : landPatternPads)
          {
            _landPatternDetailsTableModel.deleteLandPatternPad(landPatternPad);
          }
        }
      }
      else
      {
        Object source = projectChangeEvent.getSource();
        if(source instanceof LandPatternPad)
        {
          Assert.expect(source != null);
         
          if ((_firstRow >= 0) && (_lastRow >= 0)) // java will wipe out the cell editors if a table is asked to update a negative row range
            _landPatternDetailsTableModel.fireTableRowsUpdated(_firstRow, _lastRow);
          //if (_landPatternTabIsVisible)
          //  _testDev.landPatternScaleDrawing();
        }
      }
    }
    else if (projectChangeEventEnum instanceof ThroughHoleLandPatternPadEventEnum)
    {
      ThroughHoleLandPatternPadEventEnum throughHoleLandPatternPadEventEnum = (ThroughHoleLandPatternPadEventEnum)projectChangeEventEnum;
      if (throughHoleLandPatternPadEventEnum.equals(ThroughHoleLandPatternPadEventEnum.HOLE_DIAMETER) ||
          throughHoleLandPatternPadEventEnum.equals(ThroughHoleLandPatternPadEventEnum.HOLE_COORDINATE))
      {
        Object source = projectChangeEvent.getSource();
        Assert.expect(source instanceof ThroughHoleLandPatternPad);
        Assert.expect(source != null);
        ThroughHoleLandPatternPad landPatternPad = (ThroughHoleLandPatternPad)source;

        int row = _landPatternDetailsTableModel.getRowForLandPatternPad(landPatternPad);
        if (row >= 0) // java will wipe out the cell editors if a table is asked to update a negative row range
          _landPatternDetailsTableModel.fireTableRowsUpdated(row, row);
        //if (_landPatternTabIsVisible)
        ///  _testDev.landPatternScaleDrawing();
      }
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void handleMouseClickEvent(final Object object)
  {
    Assert.expect(object != null);

    MouseEvent mouseEvent = (MouseEvent)object;
    final int pixelX = mouseEvent.getX();
    final int pixelY = mouseEvent.getY();
    if (SwingUtilities.isLeftMouseButton(mouseEvent))
    {
      // do nothing (there is no special handling of left mouse clicks)
    }
    else if (SwingUtilities.isRightMouseButton(mouseEvent))
    {
      // handle right click with a popup menu
      JPopupMenu popup = new JPopupMenu("Actions"); // this name is never seen by the users
      
      // handle any right mouse clicks for the LandPattern graphics engine
      // we'll give an option to set Pad One and also, for TH landpatterns, to change the x,y coords of the hole
      if (_landPatternPadFoundOnRightClick != null)
      {
        JMenuItem makePadOneCompMenuItem = createMakePadOneMenuItem(_landPatternPadFoundOnRightClick);
        JMenuItem createLandPatternInArray = createLandPatternPadInArrays();
        
        popup.add(makePadOneCompMenuItem);
        popup.add(createLandPatternInArray);
        if (_landPatternPadFoundOnRightClick instanceof ThroughHoleLandPatternPad)
        {
          JMenuItem editHoleLocationCompMenuItem = createEditHoleLocationMenuItem((ThroughHoleLandPatternPad)_landPatternPadFoundOnRightClick);
          popup.add(editHoleLocationCompMenuItem);
        }
        popup.show(mouseEvent.getComponent(), pixelX, pixelY);
      }
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private JMenuItem createMakePadOneMenuItem(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    JMenuItem makePadOneMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("CAD_ASSIGN_PAD_ONE_MENU_KEY",
                                                                                                 new Object[]{landPatternPad.getName()})));
    makePadOneMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setLandPatternPadOne(_landPatternPadFoundOnRightClick);
      }
    });
    return makePadOneMenuItem;
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void setLandPatternPadOne(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    try
    {
      _commandManager.execute(new LandPatternPadSetPadOneCommand(landPatternPad));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_frame, ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private JMenuItem createLandPatternPadInArrays()
  {
    JMenuItem editHoleLocationMenuItem = new JMenuItem(StringLocalizer.keyToString("APA_GUI_ADD_PAD_ARRAY_TITLE_KEY"));
    editHoleLocationMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        createLandPatternArray();
      }
    });
    return editHoleLocationMenuItem;
  }
  
  /*
   * @author Kee Chin Seong 
   */
  private void createLandPatternArray()
  {
     if(_landPatternPadFoundOnRightClick == null)
     {
        JOptionPane.showMessageDialog(this,
                                     StringLocalizer.keyToString("CC_PAD_ORIGIN_WAS_NOT_DEFINED_ERR_KEY"),
                                     StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                     JOptionPane.OK_OPTION);
       return;
     }
     AddPadArrayEditorDialog dlg = new AddPadArrayEditorDialog(_frame, _currentLandPattern, _landPatternPadFoundOnRightClick, _project);
     SwingUtils.centerOnComponent(dlg, _frame);
     dlg.setVisible(true);
     dlg.dispose();
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void showEditHoleLocationDialog(ThroughHoleLandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    //show the edit location dialog
    EditHoleLocationDialog dlg = new EditHoleLocationDialog(_frame, _project, landPatternPad);
    SwingUtils.centerOnComponent(dlg, _frame);
    dlg.setVisible(true);
    dlg.clear();
    dlg.dispose();
  }
  
  /*
   * @author Kee Chin Seong
   */
  private JMenuItem createEditHoleLocationMenuItem(final ThroughHoleLandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    JMenuItem editHoleLocationMenuItem = new JMenuItem(StringLocalizer.keyToString("CAD_EDIT_HOLE_LOCATIION_MENU_KEY"));
    editHoleLocationMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showEditHoleLocationDialog(landPatternPad);
      }
    });
    return editHoleLocationMenuItem;
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void cancelEditingIfNecessary(JTable table)
  {
    // cancel cell editing in case user deletes the row while editing is still going on.
    if (table.isEditing())
      table.getCellEditor().cancelCellEditing();
    _commandManager.endCommandBlockIfNecessary();
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void cancelAction()
  {
    _cadCreatorGraphicPanel.getLandPatternEditorGraphicsEngineSetup().finish();
    ProjectObservable.getInstance().deleteObserver(this);
    _guiObservable.deleteObserver(this);
    _landPatternGraphicsSelectedRendererObservable.deleteObserver(this);
    
    //_lpCreator.addGraphicsEngineObservers();

    dispose();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public CreateNewCadPanel getLandPatternGraphicsPanel()
  {
     Assert.expect(_cadCreatorGraphicPanel != null);
     
     return _cadCreatorGraphicPanel;
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void removeUnusedLandPattern()
  {
    if (_currentLandPattern != null)
    {
      if (_currentLandPattern.isInUse())
      {
        JOptionPane.showMessageDialog(this,
                                      StringLocalizer.keyToString(new LocalizedString("CAD_CANT_DELETED_USED_LP_KEY",
                                                                                      new Object[]{_currentLandPattern.getName()})),
                                      StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                      JOptionPane.INFORMATION_MESSAGE);
        return;
      }
      else if (_currentLandPattern.getLandPatternPads().isEmpty() == false)
      {
       // _commandManager.trackState(getCurrentUndoState());
        _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_BLOCK_DELETE_NON_EMPTY_LAND_PATTERN_KEY"));
        try
        {
          _commandManager.execute(new RemoveLandPatternCommand(_currentLandPattern));
          _currentLandPattern = null;
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_frame, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
        }
        _commandManager.endCommandBlock();
      }
      else
      {
        try
        {
          //_commandManager.trackState(getCurrentUndoState());
          _commandManager.execute(new RemoveLandPatternCommand(_currentLandPattern));
          _currentLandPattern = null;
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_frame, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
        }
      }
    }
  }
}
