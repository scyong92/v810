package com.axi.v810.gui.cadCreator;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelDesc.Component;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.AddComponentDialog;
import com.axi.v810.gui.undo.CreateComponentTypeCommand;
import com.axi.v810.util.*;

/**
 * This is the Cad Creator Component Tool
 * @author Chin Seong - Kee
 */
public class ComponentCreator extends JPanel implements Observer
{
   // menu items
   private MainMenuGui _mainUI;
   private Project _project = null;

   // every time a Renderer is selected the Observable will update any Observers to tell them it was selected
   private GuiObservable _guiObservable = GuiObservable.getInstance();
   private ProjectObservable _projectObservable = ProjectObservable.getInstance();
   
   private SelectedRendererObservable _SelectedRendererObservable; 
   
   // command manager for undo functionality
   private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
   
   private CadCreatorPanel _cadCreatorPanel = null;
   private CreateNewCadPanel _paneCadPanel = null;
   
   private static CreatePanelGraphicsEngineSetup _createPanelGraphicsEngineSetup = null;

   private JPanel _mainPanel;
   private JPanel _innerTopPanel;
   private JPanel _innerCenterPanel;
   private JPanel _innerBottomPanel;
   
   private JButton _importComponentButton;
   private JButton _manualAddComponentButton;
   
   private ComponentLandPatternTableModel _populateComponentLandPatternTableModel;
   private ComponentLandPatternTable _populatedComponentLandPatternTable;
   
   private ComponentLandPatternTableModel _unpopulateComponentLandPatternTableModel;
   private ComponentLandPatternTable _unpopulatedComponentLandPatternTable;
   
   private JScrollPane _populatedComponentScrollPane = new JScrollPane();
   private JScrollPane _unpopulatedComponentScrollPane = new JScrollPane();
   
   // listener for row table selection
   private ListSelectionListener _componentTableListSelectionListener;
   private ListSelectionListener _unpopulatedComponentTableListSelectionListener;
   
   private boolean _active = false;
   
   /**
   * @author Kee Chin Seong
   */
  public ComponentCreator(CadCreatorPanel cadCreatorPanel)
  {
    Assert.expect(cadCreatorPanel != null);

    _cadCreatorPanel = cadCreatorPanel;
    
    jbInit();
  }
  
  /**
   * @author Kee Chin Seong
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
         if (observable instanceof SelectedRendererObservable)
            handleSelectionEvent(object);
         if (observable instanceof ProjectObservable)
            handleProjectDataEvent(object);
      }
    });
  }
  
  /*
   * @author Kee Chin Seong 
   */
  private void handleSelectionEvent(Object object)
  {
    // ok.  We got notice that something was selected.
    // We receive from the update, a collection of selected objects.  We'll only pay attention to
    // the ones this screen is interested in
    Collection selections = (Collection)object;
    Iterator it = selections.iterator();

      java.util.List < com.axi.v810.business.panelDesc.Component > components = new ArrayList<com.axi.v810.business.panelDesc.Component>();
      java.util.List<Pad> pads = new ArrayList<Pad>();
      ListSelectionModel populatedComponentRowSelectionModel = _populatedComponentLandPatternTable.getSelectionModel();
      ListSelectionModel unpopulatedComponentRowSelectionModel = _unpopulatedComponentLandPatternTable.getSelectionModel();
      
      populatedComponentRowSelectionModel.setValueIsAdjusting(true);
      unpopulatedComponentRowSelectionModel.setValueIsAdjusting(true);
      
      while (it.hasNext())
      {
        com.axi.guiUtil.Renderer renderer = (com.axi.guiUtil.Renderer)it.next();
        // viewing component mode -- pay attention to ComponentRenderers and PadRenderers
        // get the component(s) selected (if any) and highlight the row in the table cooresponding to that component
        // if a pad was also selected, highlight the pad in the details table
        if (renderer instanceof ComponentRenderer)
        {
          ComponentRenderer compRenderer = (ComponentRenderer)renderer;
          components.add(compRenderer.getComponent());
        }

        if (renderer instanceof PadRenderer)
        {
          PadRenderer padRenderer = (PadRenderer)renderer;
          pads.add(padRenderer.getPad());
          components.add(padRenderer.getPad().getComponent());
        }
      }

      // if the renderer selected was not a component or a pad, we really don't care
      // and can exit this method at this point.
      if (components.isEmpty() && pads.isEmpty())
      {
        return;
      }
      
      // we're going to select new components if a component renderer came through, otherwise do not change the
      // selection
      if (components.isEmpty() == false)
      {
        if (_populatedComponentLandPatternTable.isEditing())
          _populatedComponentLandPatternTable.getCellEditor().cancelCellEditing();
        _populatedComponentLandPatternTable.clearSelection();
        
        if (_unpopulatedComponentLandPatternTable.isEditing())
          _unpopulatedComponentLandPatternTable.getCellEditor().cancelCellEditing();
        _unpopulatedComponentLandPatternTable.clearSelection();
      }
      // go through all the component renderers and select them in the table
      Set<BoardType> boardTypesNeeded = new HashSet<BoardType>();
      for (com.axi.v810.business.panelDesc.Component component : components)
      {
        BoardType boardTypeForComponent = component.getBoard().getBoardType();
        boardTypesNeeded.add(boardTypeForComponent);
      }
      for (com.axi.v810.business.panelDesc.Component component : components)
      {
        if(component.getLandPattern().getName().matches(StringLocalizer.keyToString("CAD_UNPOPULATED_LAND_PATTERN_KEY")))
        {
            int rowPosition = _unpopulatedComponentLandPatternTable.getTableModel().getRowForComponent(component);
            if (rowPosition != -1) // -1 measn it does not exist in the table.  This can happen with more than one board type
              _unpopulatedComponentLandPatternTable.addRowSelectionInterval(rowPosition, rowPosition);
        }
        else
        {
            int rowPosition = _populateComponentLandPatternTableModel.getRowForComponent(component);
            if (rowPosition != -1) // -1 measn it does not exist in the table.  This can happen with more than one board type
              _populatedComponentLandPatternTable.addRowSelectionInterval(rowPosition, rowPosition);
        }
      }

      // only scroll to first one selected
      populatedComponentRowSelectionModel.setValueIsAdjusting(false);
      unpopulatedComponentRowSelectionModel.setValueIsAdjusting(false);
      SwingUtils.scrollTableToShowSelection(_unpopulatedComponentLandPatternTable);   
      SwingUtils.scrollTableToShowSelection(_populatedComponentLandPatternTable);   
 }  
  
  /*
   * @author Kee Chin Seong
   */
  private void handleProjectDataEvent(Object object)
  {
    Assert.expect(object != null);

    if (object instanceof ProjectChangeEvent)
    {
        ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)object;
        ProjectChangeEventEnum projectChangeEventEnum = projectChangeEvent.getProjectChangeEventEnum();
        if ((projectChangeEventEnum instanceof LandPatternEventEnum) ||
            (projectChangeEventEnum instanceof LandPatternPadEventEnum) ||
            (projectChangeEventEnum instanceof ThroughHoleLandPatternPadEventEnum) ||
            (projectChangeEventEnum instanceof ComponentEventEnum) ||
            (projectChangeEventEnum instanceof ComponentTypeEventEnum) ||
            (projectChangeEventEnum instanceof FiducialEventEnum) ||
            (projectChangeEventEnum instanceof FiducialTypeEventEnum) ||
            (projectChangeEventEnum instanceof ProjectEventEnum) ||
            (projectChangeEventEnum instanceof ProjectStateEventEnum))
        {
          // anything here should cause an update
          updateData(projectChangeEvent);
        }
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void updateData(ProjectChangeEvent projectChangeEvent)
  {
    Assert.expect(projectChangeEvent != null);

    ProjectChangeEventEnum projectChangeEventEnum = projectChangeEvent.getProjectChangeEventEnum();
    
    if (projectChangeEventEnum instanceof ComponentTypeEventEnum)
    {
      ComponentTypeEventEnum componentTypeEventEnum = (ComponentTypeEventEnum)projectChangeEventEnum;
      if (componentTypeEventEnum.equals(ComponentTypeEventEnum.CREATE_OR_DESTROY) || componentTypeEventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE))
      {
        Object oldValue = projectChangeEvent.getOldValue();
        Object newValue = projectChangeEvent.getNewValue();
        if (newValue != null) // this is a create event, make sure the components table model gets updated
        {
          Assert.expect(oldValue == null);
          Assert.expect(newValue instanceof ComponentType);
          
          // boolean needToRemove = false;
          //ComponentType componentType = (ComponentType)oldValue;
          ComponentType compType = (ComponentType)newValue;
          
          for(Component component : compType.getComponents())
          {
            //   System.out.println("landPattern " + component.getLandPattern().getName());
            if(component.getLandPattern().getName().matches(StringLocalizer.keyToString("CAD_UNPOPULATED_LAND_PATTERN_KEY")))
              _unpopulateComponentLandPatternTableModel.addPopulatedComponent(component);
            else
              _populateComponentLandPatternTableModel.addUnpopulatedComponent(component); 
              
            int row = _populateComponentLandPatternTableModel.getRowForComponent(component);
            if (row >= 0) // java will wipe out the cell editors if a table is asked to update a negative row range
                _populateComponentLandPatternTableModel.fireTableRowsUpdated(row, row);
              
            row = _unpopulateComponentLandPatternTableModel.getRowForComponent(component);
            if (row >= 0) // java will wipe out the cell editors if a table is asked to update a negative row range
               _unpopulateComponentLandPatternTableModel.fireTableRowsUpdated(row, row);
            }
        }
        if (oldValue != null) // this is a destroy event, make sure the components table model gets updated
        {
          Assert.expect(newValue == null);
          Assert.expect(oldValue instanceof ComponentType);
          
          boolean needToRemove = false;
          ComponentType componentType = (ComponentType)oldValue;
          ComponentType compType = (ComponentType)newValue;
          
          if(componentType != null && compType != null && componentType != compType)
             needToRemove = true;
          else if(componentType != null && compType == null)
             needToRemove = true;
          
          if(needToRemove == true)
          {
            for(Component component : componentType.getComponents())
            {
              if(component.getLandPattern().getName().matches(StringLocalizer.keyToString("CAD_UNPOPULATED_LAND_PATTERN_KEY")))
                 _unpopulateComponentLandPatternTableModel.removeComponent(component);
              else
                 _populateComponentLandPatternTableModel.removeComponent(component);
            }
          }
        }
      }
      else if (componentTypeEventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST))
      {
        Object oldValue = projectChangeEvent.getOldValue();
        Object newValue = projectChangeEvent.getNewValue();
        // this is a create event, make sure the components table model gets updated
        if (oldValue == null && newValue != null)
        {
          java.util.List<ComponentType> componentTypeList = (java.util.List<ComponentType>) newValue;
          //_populateComponentLandPatternTableModel.addComponentTypes(componentTypeList);
        }
        // this is a destroy event, make sure the components table model gets updated
        if (oldValue != null && newValue == null)
        {
          java.util.List<ComponentType> componentTypeList = (java.util.List<ComponentType>) oldValue;
          //_populateComponentLandPatternTableModel.deleteComponentsForComponentType(componentTypeList);
        }
      }
      else // for any other event, need to update the other rows in the table affected by the change (for multiple board panels)
      {
        Object source = projectChangeEvent.getSource();
        Assert.expect(source instanceof ComponentType);
        Assert.expect(source != null);
        ComponentType componentType = (ComponentType)source;
        for (com.axi.v810.business.panelDesc.Component component : componentType.getComponents())
        {
          int row = _populateComponentLandPatternTableModel.getRowForComponent(component);
          if (row >= 0) // java will wipe out the cell editors if a table is asked to update a negative row range
            _populateComponentLandPatternTableModel.fireTableRowsUpdated(row, row);
        }
      }  
      _unpopulatedComponentLandPatternTable.getTableModel().fireTableDataChanged();
      _populatedComponentLandPatternTable.getTableModel().fireTableDataChanged();
    }
  }
 
  /*
   * @Author Kee Chin Seong
   */
  public void populateTable()
  {
      int[] selectedRows = _populatedComponentLandPatternTable.getSelectedRows();
      java.util.List<ComponentType> selectedComponents = new ArrayList<ComponentType>();
      for (int i = 0; i < selectedRows.length; i++)
      {
        selectedComponents.add(_populateComponentLandPatternTableModel.getComponentAt(selectedRows[i]).getComponentType());
      }

      if (selectedComponents.isEmpty() == false)
        _guiObservable.stateChanged(selectedComponents, SelectionEventEnum.COMPONENT_TYPE);
      
      SwingUtils.scrollTableToShowSelection(_populatedComponentLandPatternTable);
      
      selectedRows = _unpopulatedComponentLandPatternTable.getSelectedRows();
      java.util.List<ComponentType> unpopulateUnselectedComponents = new ArrayList<ComponentType>();
      
      for (int i = 0; i < selectedRows.length; i++)
        unpopulateUnselectedComponents.add(_unpopulateComponentLandPatternTableModel.getComponentAt(selectedRows[i]).getComponentType());

      if (unpopulateUnselectedComponents.isEmpty() == false)
        _guiObservable.stateChanged(unpopulateUnselectedComponents, SelectionEventEnum.COMPONENT_TYPE);
      
      SwingUtils.scrollTableToShowSelection(_unpopulatedComponentLandPatternTable);
     
      _populatedComponentLandPatternTable.setEditorsAndRenderers(_project.getPanel().getLandPatterns());
      _unpopulatedComponentLandPatternTable.setEditorsAndRenderers(_project.getPanel().getLandPatterns());
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void populateProjectData(Project project, CreateNewCadPanel panelCadPanel)
  {
    Assert.expect(project != null);
    
    _project = project;
    
    _populatedComponentLandPatternTable.scrollRectToVisible(new Rectangle(0,0,0,0));
    _unpopulatedComponentLandPatternTable.scrollRectToVisible(new Rectangle(0,0,0,0));
    
    _populateComponentLandPatternTableModel.populateComponentPopulatedLandPatternWithPanelData(_project);
    _unpopulateComponentLandPatternTableModel.populateComponentUnpopulatedLandPatternWithPanelData(_project);
    
    _createPanelGraphicsEngineSetup = panelCadPanel.getCreatePanelGraphicsEngineSetup();
    _SelectedRendererObservable = _createPanelGraphicsEngineSetup.getGraphicsEngine().getSelectedRendererObservable();
    
    _unpopulatedComponentLandPatternTable.getTableModel().fireTableDataChanged();
    _populatedComponentLandPatternTable.getTableModel().fireTableDataChanged();
    
    _active = true;
      
    addObservers();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void removeObservers()
  {
    _guiObservable.deleteObserver(this);
    _projectObservable.deleteObserver(this);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void addObservers()
  {
    _guiObservable.addObserver(this);
    _projectObservable.addObserver(this);
    _SelectedRendererObservable.addObserver(this);
  }
  
   /*
   * @author Kee Chin Seong
   */
  private void jbInit()
  {
    _mainPanel = new JPanel();
    _innerTopPanel = new JPanel();
    _innerCenterPanel = new JPanel();
    _innerBottomPanel = new JPanel();
    
    _importComponentButton = new JButton(StringLocalizer.keyToString("GUI_FILE_IMPORT_MENU_KEY"));
    _manualAddComponentButton = new JButton("Add Component");
    
    //Setting up the main layout
    _mainPanel.setLayout(new BorderLayout());
    _mainPanel.add(_innerTopPanel, BorderLayout.NORTH);
    _mainPanel.add(_innerCenterPanel, BorderLayout.CENTER);
    _mainPanel.add(_innerBottomPanel, BorderLayout.SOUTH);
    
    //Setup Top Layout
    _innerTopPanel.setLayout(new BorderLayout());
    _innerTopPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
                                                                                 new Color(142, 142, 142)),
                                                                                StringLocalizer.keyToString("CAD_POPULATED_COMPONENT_KEY")),
                                                                                BorderFactory.createEmptyBorder(0, 5, 10, 5)));
    
    //
    _importComponentButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _active = false;
        ImportComponentDialog dlg = new ImportComponentDialog(MainMenuGui.getInstance(), "Import Component", _project, 
                                                              _populatedComponentLandPatternTable, _unpopulatedComponentLandPatternTable);
        int retVal = dlg.showDialog();
        if (retVal == JOptionPane.OK_OPTION || retVal == JOptionPane.CANCEL_OPTION) 
        {
          _active = true;
        }
      }
    });
    
    _manualAddComponentButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
         AddComponentDialog addComponentDialog = new AddComponentDialog(MainMenuGui.getInstance(),
                                                                   StringLocalizer.keyToString("CAD_ADD_COMPONENT_DIALOG_TITLE_KEY"),
                                                                   true,
                                                                   _project.getPanel().getLandPatterns(),
                                                                   _project);
        SwingUtils.centerOnComponent(addComponentDialog, MainMenuGui.getInstance());
        addComponentDialog.setVisible(true);

        showAndAddComponent(addComponentDialog);
        //showAndAddComponent(addComponentDialog);
      }
    });
    
    _populateComponentLandPatternTableModel = new ComponentLandPatternTableModel();
    
    _populatedComponentLandPatternTable = new ComponentLandPatternTable(_populateComponentLandPatternTableModel)
    {        
       /**
       * We override this to make sure the selected rows are still the proper selected
       * rows after a sort.
       * @author Kee Chin Seong
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = _populatedComponentLandPatternTable.getSelectedRows();
        java.util.List<Object> selectedData = new ArrayList<Object>();
        for(int i = 0; i < selectedRows.length; i++)
        {
          selectedData.add(_populateComponentLandPatternTableModel.getComponentAt(selectedRows[i]));
        }
        ListSelectionModel rowSM = _populatedComponentLandPatternTable.getSelectionModel();
        rowSM.removeListSelectionListener(_componentTableListSelectionListener); // add the listener for a details table row selection

        super.mouseReleased(event);

        _populatedComponentLandPatternTable.clearSelection();
        // now, reselect everything

        Iterator it = selectedData.iterator();
        while (it.hasNext())
        {
          int row;
          Object obj = it.next();
          row = _populateComponentLandPatternTableModel.getRowForComponent((com.axi.v810.business.panelDesc.Component)obj);
          _populatedComponentLandPatternTable.addRowSelectionInterval(row, row);
        }
        SwingUtils.scrollTableToShowSelection(_populatedComponentLandPatternTable);
        rowSM.addListSelectionListener(_componentTableListSelectionListener); // add the listener for a details table row selection
      }
    };    
    _populatedComponentLandPatternTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
    _populatedComponentLandPatternTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _populatedComponentLandPatternTable.setSurrendersFocusOnKeystroke(false);
    _populatedComponentLandPatternTable.sizeColumnsToFit(-1);
    
    // add the listener for a table row selection
    _populatedComponentLandPatternTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        //multiEditTableMouseReleased(e, _populatedComponentLandPatternTable);
      }
    });
    
    _componentTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        populateTable();
      }
    };
    
    _unpopulatedComponentTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        populateTable();
      }
    };
    
    ListSelectionModel rowSM = _populatedComponentLandPatternTable.getSelectionModel();
    rowSM.addListSelectionListener(_componentTableListSelectionListener); // add the listener for a details table row selection
    _populatedComponentScrollPane.getViewport().add(_populatedComponentLandPatternTable, null);

    _innerTopPanel.add(_populatedComponentScrollPane, BorderLayout.NORTH);
    
    //Setup Middle Layout
    _innerCenterPanel.setLayout(new BorderLayout());
    _innerCenterPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
                                                                                 new Color(142, 142, 142)),
                                                                                StringLocalizer.keyToString("CAD_UNPOPULATED_COMPONENT_KEY")),
                                                                                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    
    _unpopulateComponentLandPatternTableModel = new ComponentLandPatternTableModel();
    _unpopulatedComponentLandPatternTable = new ComponentLandPatternTable(_unpopulateComponentLandPatternTableModel)
    {        
       /**
       * We override this to make sure the selected rows are still the proper selected
       * rows after a sort.
       * @author Kee Chin Seong
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = _unpopulatedComponentLandPatternTable.getSelectedRows();
        java.util.List<Object> selectedData = new ArrayList<Object>();
        for(int i = 0; i < selectedRows.length; i++)
        {
          selectedData.add(_unpopulateComponentLandPatternTableModel.getComponentAt(selectedRows[i]));
        }
        ListSelectionModel rowSM = _unpopulatedComponentLandPatternTable.getSelectionModel();
        rowSM.removeListSelectionListener(_unpopulatedComponentTableListSelectionListener); // add the listener for a details table row selection

        super.mouseReleased(event);

        _populatedComponentLandPatternTable.clearSelection();
        // now, reselect everything

        Iterator it = selectedData.iterator();
        while (it.hasNext())
        {
          int row;
          Object obj = it.next();
          row = _unpopulateComponentLandPatternTableModel.getRowForComponent((com.axi.v810.business.panelDesc.Component)obj);
          _unpopulatedComponentLandPatternTable.addRowSelectionInterval(row, row);
        }
        SwingUtils.scrollTableToShowSelection(_unpopulatedComponentLandPatternTable);
        rowSM.addListSelectionListener(_unpopulatedComponentTableListSelectionListener); // add the listener for a details table row selection
      }
    };    
    _unpopulatedComponentLandPatternTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
    _unpopulatedComponentLandPatternTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _unpopulatedComponentLandPatternTable.setSurrendersFocusOnKeystroke(false);
    _unpopulatedComponentLandPatternTable.sizeColumnsToFit(-1);
    
    // add the listener for a table row selection
    _unpopulatedComponentLandPatternTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        //multiEditTableMouseReleased(e, _populatedComponentLandPatternTable);
      }
    });

    rowSM = _unpopulatedComponentLandPatternTable.getSelectionModel();
    rowSM.addListSelectionListener(_unpopulatedComponentTableListSelectionListener); // add the listener for a details table row selection
    
    //_unpopulatedComponentScrollPane.set
    _unpopulatedComponentScrollPane.getViewport().add(_unpopulatedComponentLandPatternTable, null);
    _innerCenterPanel.add(_unpopulatedComponentScrollPane, BorderLayout.CENTER);
     
    _importComponentButton.setBounds(50, 50, 80, 25);;
    _manualAddComponentButton.setBounds(50, 50, 80, 25);;
    
    //Setup Bottom Layout
    _innerBottomPanel.setLayout(new BoxLayout(_innerBottomPanel, BoxLayout.X_AXIS));
    _innerBottomPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
                                                                                 new Color(142, 142, 142)),
                                                                                ""),
                                                                                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    
    _innerBottomPanel.add(_importComponentButton);
    _innerBottomPanel.add(_manualAddComponentButton);
    
    setLayout(new BorderLayout());
    add(_mainPanel);
    //pack();    
  }
  
  /*
   * @author Kee Chin Seong 
   */
  private void showAndAddComponent(AddComponentDialog addComponentDialog)
  {
    Assert.expect(addComponentDialog != null);

    if (addComponentDialog.userClickedOk())
    {
      String refDes = addComponentDialog.getReferenceDesignator();
      if (refDes.length() > 0)
      {
        BoardType boardType = addComponentDialog.getBoardType();

        if (boardType.isComponentTypeReferenceDesignatorValid(refDes) == false)
        {
          // pop dialog about invalid name
          String illegalChars = boardType.getComponentTypeReferenceDesignatorIllegalChars();
          LocalizedString message = null;
          if (illegalChars.equals(" "))
            message = new LocalizedString("CAD_INVALID_COMPONENT_NAME_SPACES_KEY", new Object[]
                                          {refDes});
          else
            message = new LocalizedString("CAD_INVALID_COMPONENT_NAME_OTHER_CHARS_KEY", new Object[]
                                          {refDes, illegalChars});
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString(message),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
          return;
        }
        else if (boardType.isComponentTypeReferenceDesignatorDuplicate(refDes))
        {
          // pop dialog about duplicate name
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
              new Object[]
              {refDes})),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
          return;
        }
        else
        {
          LandPattern landPattern = (LandPattern)addComponentDialog.getLandPattern();
          BoardCoordinate newCoordinate = addComponentDialog.getBoardCoordinate();
          double rotation = addComponentDialog.getRotation();
          String boardSide = addComponentDialog.getBoardSide();
          SideBoardType sideBoardType;

          if (boardSide.equals(StringLocalizer.keyToString("CAD_SIDE1_KEY")))
          {
            if (boardType.sideBoardType1Exists() == false)
              boardType.getBoards().get(0).createSecondSideBoard();
            sideBoardType = boardType.getSideBoardType1();
          }
          else
          {
            if (boardType.sideBoardType2Exists() == false)
              boardType.getBoards().get(0).createSecondSideBoard();
            sideBoardType = boardType.getSideBoardType2();
          }

          // add new rotation to custom values, if needed
         // _newComponentDetailsTable.addEditorCustomValue(new Double(rotation));

          try
          {
           // _commandManager.trackState(getCurrentUndoState());
            _commandManager.execute(new CreateComponentTypeCommand(sideBoardType, refDes, landPattern, newCoordinate, rotation));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          }
        }
      }
    }
    addComponentDialog.clear();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void unpopulate()
  {
    _populatedComponentLandPatternTable.clearSelection();
    _populatedComponentLandPatternTable.resetSortHeader();
    
    _unpopulatedComponentLandPatternTable.clearSelection();
    _unpopulatedComponentLandPatternTable.resetSortHeader();
    
    _populateComponentLandPatternTableModel.unpopulate();
    _unpopulateComponentLandPatternTableModel.unpopulate();
  }
}
