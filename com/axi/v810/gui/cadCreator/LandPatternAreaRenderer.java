package com.axi.v810.gui.cadCreator;

import com.axi.guiUtil.*;
import com.axi.util.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

/**
 * This class is responsible for drawing the Board outline.
 *
 * @author Kee Chin Seong
 */
public class LandPatternAreaRenderer extends ShapeRenderer
{
  /**
   * @author Kee Chin Seong
   */
  private boolean _isNeedConvertBackToNM;
  private Shape _areaShape = null;
  
  public LandPatternAreaRenderer(Shape shape, int enlargeWidth, boolean isNeedConvertBackToNM)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(shape != null);
    
    _isNeedConvertBackToNM = isNeedConvertBackToNM;
    
    _areaShape = shape;
    
    refreshData(shape.getBounds2D().getMinX() - enlargeWidth, shape.getBounds2D().getMinY() - enlargeWidth, shape.getBounds2D().getWidth() + (enlargeWidth * 2), shape.getBounds2D().getHeight() + (enlargeWidth * 2));
  }
  
  /*
   * @author Kee Chin Seong
   */
  public Shape getAreaShape()
  {
    Assert.expect(_areaShape != null);
    
    return _areaShape;
  }

  /*
   * @Author Kee Chin Seong
   */
  protected void refreshData(double x, double y, double w, double h)
  {
    java.awt.Shape shape = null;
    
    if(_isNeedConvertBackToNM == true)
    {
      if(_areaShape instanceof Rectangle2D)
         shape = new Rectangle2D.Double(x * 25400, y * 25400, 
                                        w * 25400, h  * 25400);
      else
         shape = new java.awt.geom.Ellipse2D.Double(x * 25400, y * 25400, 
                                                    w * 25400, h  * 25400);
    }
    else 
    {
      if(_areaShape instanceof Rectangle2D)
         shape = new Rectangle2D.Double(x, y, w, h); 
      else
         shape = new java.awt.geom.Ellipse2D.Double(x, y, w, h); 
    }
    
    AffineTransform transform = AffineTransform.getTranslateInstance(0, 0);
    shape = transform.createTransformedShape(shape);
    initializeShape(shape);
  }

  @Override
  protected void refreshData()
  {
    //
  }


}
