package com.axi.v810.gui.cadCreator;

import java.awt.*;
import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;


/**
 *
 * @author chin-seong.kee
 * Currently, that is only frame size and position.  Other persistance objects may extend this class and
 * persist more specific information for their own GUIs
 */
public class CadCreatorPersistance implements Serializable
{
  private int _cadSplitPanePosition = -1;
  private int _contentSplitPanePosition = -1;
  private int _rightSplitPanePosition = -1;

  /**
   * @author chin-seong.kee
   */
  public CadCreatorPersistance()
  {
    // do nothing
  }

  /**
   * @author chin-seong.kee
   */
  public CadCreatorPersistance readSettings()
  {
    CadCreatorPersistance persistance = null;
    try
    {
      if (FileUtilAxi.exists(FileName.getCadCreatorPersistFullPath()))
        persistance = (CadCreatorPersistance) FileUtilAxi.loadObjectFromSerializedFile(FileName.getCadCreatorPersistFullPath());
      else
        persistance = new CadCreatorPersistance();
    }
    catch(DatastoreException de)
    {
      // do nothing..
      persistance = new CadCreatorPersistance();
    }

    return persistance;
  }

  /**
   * @author chin-seong.kee
   */
  public void writeSettings()
  {
    try
    {
      FileUtilAxi.saveObjectToSerializedFile(this, FileName.getCadCreatorPersistFullPath());
    }
    catch(DatastoreException de)
    {
      Assert.logException(de);
    }
  }

  /**
   * @author chin-seong.kee
   */
  public int getCadSplitPanePosition()
  {
    return _cadSplitPanePosition;
  }

  /**
   * @author chin-seong.kee
   */
  public void setCadSplitPanePosition(int splitPanePosition)
  {
    _cadSplitPanePosition = splitPanePosition;
  }
  
  /**
   * @author chin-seong.kee
   */
  public int getRightSplitPanePosition()
  {
    return _rightSplitPanePosition;
  }

  /**
   * @author chin-seong.kee
   */
  public void setRightSplitPanePosition(int splitPanePosition)
  {
    _rightSplitPanePosition = splitPanePosition;
  }
  
  /**
   * @author chin-seong.kee
   */
  public int getContentSplitPanePosition()
  {
    return _contentSplitPanePosition;
  }

  /**
   * @author chin-seong.kee
   */
  public void setContentSplitPanePosition(int splitPanePosition)
  {
    _contentSplitPanePosition = splitPanePosition;
  }
}
