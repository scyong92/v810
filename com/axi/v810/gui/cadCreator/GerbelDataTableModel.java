package com.axi.v810.gui.cadCreator;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * This class is the data model for the package table for the Groupings panel and it also
 * converts to the component table model in the groupings panel
 * @author Chin Seong
 */
public class GerbelDataTableModel extends DefaultSortTableModel
{
  private List<List<String>> _data = new ArrayList<List<String>>();

  private final String[] _gerbelDataTableColumns =
  {
    "#",
    "Ref Des",
    "X",
    "Y",
    "Part #",
    "Pkg Style",
    "Angle",
    "Feeder",
    "Side"
  };
  
  /**
   * @author Chin Seong
   */
  public GerbelDataTableModel()
  {
    // do nothing
  }

  /**
   * @author Chin Seong
   */
  void clear()
  {
    _data.clear();
  }

  /**
   * @author Chin Seong
   */
  void populateGerbelData(List<List<String>> data)
  {
    _data = data;
  }

   /**
   * @author Chin Seong
   */
  void updateSpecificRow(String specificData)
  {
    int row = _data.indexOf(specificData);
    fireTableRowsUpdated(row, row);
  }

  /**
   * @author Chin Seong
   */
  int getRowSpecificData(String specificData)
  {
    return _data.lastIndexOf(specificData);
  }

  /**
   * @author Chin Seong
   */
  public int getColumnCount()
  {
    return _gerbelDataTableColumns.length;
  }

  /**
   * @author Chin Seong
   */
  public int getRowCount()
  {
    if(_data == null)
      return 0;
    else
      return _data.size();
  }

  /**
   * @author Chin Seong
   */
  public Class getColumnClass(int c)
  {
    return getValueAt(0, c).getClass();
  }

  /**
   * @author Chin Seong
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());

    return (String)_gerbelDataTableColumns[columnIndex];
  }

  /**
   * @author Kee Chin Seong 
   * Always Return False as it just for Viewer
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return false;
  }

  /**
   * @author Chin Seong
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    List<String> data = getDataAt(rowIndex);
    
    if(columnIndex == 0)
       return rowIndex;
    
    if(data.size() + 1 < _gerbelDataTableColumns.length)
    {
      if(data.size() < columnIndex)
        return " ";
      else
        data.get(columnIndex - 1);
    }
    return data.get(columnIndex - 1);
  }

  /**
   * @author Chin Seong
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    fireTableCellUpdated(rowIndex, columnIndex);  
  }

  /**
   * @author Chin Seong
   */
  List<String> getDataAt(int row)
  {
    return _data.get(row);
  }
}
