package com.axi.v810.gui.cadCreator;


import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.LandPatternLibrary.ExportLandPatternWizardDialog;
import com.axi.v810.gui.LandPatternLibrary.ImportLandPatternWizardDialog;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * This is the Cad Creator Land Pattern Tool
 * @author Chin Seong, Kee
 */
public class LandPatternCreator extends JPanel implements Observer
{
  private Color _labelColor = new Color(0,70,213); //trying to match the blue of the panel titles

  // menu items
  private MainMenuGui _mainUI;
  private boolean _shouldInteractWithGraphics = false;
  private java.util.List<LandPattern> _landPatterns;
  private Project _project = null;

  // every time a Renderer is selected the Observable will update any Observers to tell them it was selected
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private SelectedRendererObservable _landPatternGraphicsSelectedRendererObservable;

  // command manager for undo functionality
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  // actionlisteners
  private ActionListener _selectLandPatternActionListener;

  // GUI elements
  private BorderLayout _editCadBorderLayout = new BorderLayout();
  private JPanel _editLandPatternPanel = new JPanel();
  private JPanel _editLandPatternDetailsPanel = new JPanel();
  private BorderLayout _editLandPatternDetailsPanelBorderLayout = new BorderLayout();
  private JPanel _editLandPatternInfoPanel = new JPanel();
  private BorderLayout _editLandPatternInfoPanelBorderLayout = new BorderLayout();
  private BorderLayout _editLandPatternPanelBorderLayout = new BorderLayout();
 
  private JPanel _landPatternSelectPanel = new JPanel();
  private JPanel _landPatternDetailsPanel = new JPanel();
  private JPanel _landPatternActionsPanel = new JPanel();
  private BorderLayout _landPatternDetailsPanelBorderLayout = new BorderLayout();
  private GridLayout _landPatternActionsPanelGridLayout = new GridLayout();
  private JButton _cropImageToLandPatternEditor = new JButton();
  private JButton _copyToButton = new JButton();
  private JButton _editSelectedLandPatternButton = new JButton();
  private JScrollPane _landPatternDetailsScrollPane = new JScrollPane();
  private LandPatternEditorDetailsTableModel _landPatternDetailsTableModel;
  private LandPatternEditorDetailsTable _landPatternDetailsTable;
  private JComboBox _selectLandPatternComboBox = new JComboBox();
  private JLabel _selectLandPatternLabel = new JLabel();

  private JPanel _boardAndLandPatternSelectPanel = new JPanel();
  private BorderLayout _boardAndLandPatternSelectPanelBorderLayout = new BorderLayout();

  private JPanel _landPatternActionsWrapperPanel = new JPanel();
  private FlowLayout _landPatternActionsWrapperPanelFlowLayout = new FlowLayout();
  private JButton _deleteUnusedLandPatternButton = new JButton();
  private JTabbedPane _editCadTabbedPane = new JTabbedPane();
  private JButton _exportLandPatternsButton = new JButton();
  private JButton _importLandPatternsButton = new JButton();
  private JLabel _howToEditLandPatternPad = new JLabel();

  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  private LandPattern _currentLandPattern;
  private LandPatternPad _currentLandPatternPad;
  private int _firstRow = -1, _lastRow = -1;

  public boolean _active = false;
  
  private CadCreatorPanel _cadCreatorPanel = null;
  private static CreatePanelGraphicsEngineSetup _createPanelGraphicsEngineSetup = null;
  private CreateNewCadPanel _lpGraphicsPanel = null;
  private GraphicsEngine _graphicsEngine = null;
  
  private JButton _completeButton = new JButton();
  
  /**
   * @author Kee Chin Seong
   */
  public LandPatternCreator(CadCreatorPanel cadCreatorPanel)
  {
    Assert.expect(cadCreatorPanel != null);

    _cadCreatorPanel = cadCreatorPanel;
   
    jbInit();
  }

  /**
    * Determine if the given land pattern is used by a 2 pin device (cap, res or pcap)
    * @author Kee Chin Seong
    */
   public static boolean is2PinDeviceLandPattern(LandPattern landPattern)
   {
     Assert.expect(landPattern != null);

     if (landPattern.getLandPatternPads().size() == 2)
     {
       java.util.List<ComponentType> compTypesForLP = landPattern.getComponentTypes();
       for (ComponentType ct : compTypesForLP)
       {
         java.util.List<JointTypeEnum> jointTypeEnumsForCompType = ct.getJointTypeEnums();
         for (JointTypeEnum jte : jointTypeEnumsForCompType)
         {
           if (ImageAnalysis.isSpecialTwoPinComponent(jte))
           {
             return true;
           }
         }
       }
     }
     return false;
   }

  /**
   * @author Kee Chin Seong
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof SelectedRendererObservable)
          handleSelectedRendererEvent(object);
        else if (observable instanceof ProjectObservable)
          handleProjectDataEvent(object);
        else if (observable instanceof MouseClickObservable)
          handleMouseClickEvent(object);
        else if (observable instanceof GuiObservable)
          handleGuiChangesEvent(object);
        else if (observable instanceof com.axi.v810.gui.undo.CommandManager)
          handleUndoState(object);
        else
          Assert.expect(false);
      }
    });
  }

  /**
   * @author Kee Chin Seong
   */
  private void handleUndoState(final Object stateObject)
  {
    
  }

  /**
   * @author Kee Chin Seong
   */
  private void handleSelectedRendererEvent(Object object)
  {
   // We receive from the update, a collection of selected objects.
    /** Warning "unchecked cast" approved for cast from Object type
     *  when sent from SelectedRendererObservable notifyAll.  */
    for (com.axi.guiUtil.Renderer renderer : ((Collection < com.axi.guiUtil.Renderer > )object))
    {
       if (renderer instanceof LandPatternPadRenderer)
       {
          LandPatternPadRenderer padRenderer = (LandPatternPadRenderer)renderer;
          LandPatternPad landPatternPad = padRenderer.getLandPatternPad();
          int rowPosition = _landPatternDetailsTableModel.getRowForLandPatternPad(landPatternPad);
          _landPatternDetailsTable.addRowSelectionInterval(rowPosition, rowPosition);
          
          _currentLandPatternPad = landPatternPad;
       }
    }
    // after everything is selected, scroll to make the first selection visible.
    SwingUtils.scrollTableToShowSelection(_landPatternDetailsTable); 
  }

  /**
   * @author Kee Chin Seong
   */
  private void handleProjectDataEvent(Object object)
  {
    Assert.expect(object != null);

    if (object instanceof ProjectChangeEvent)
    {
       ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)object;
       ProjectChangeEventEnum projectChangeEventEnum = projectChangeEvent.getProjectChangeEventEnum();
       if ((projectChangeEventEnum instanceof LandPatternEventEnum) ||
            (projectChangeEventEnum instanceof LandPatternPadEventEnum) ||
            (projectChangeEventEnum instanceof ThroughHoleLandPatternPadEventEnum) ||
            (projectChangeEventEnum instanceof ComponentEventEnum) ||
            (projectChangeEventEnum instanceof ComponentTypeEventEnum) ||
            (projectChangeEventEnum instanceof FiducialEventEnum) ||
            (projectChangeEventEnum instanceof FiducialTypeEventEnum) ||
            (projectChangeEventEnum instanceof ProjectEventEnum) ||
            (projectChangeEventEnum instanceof ProjectStateEventEnum))
        {
          // anything here should cause an update
          updateData(projectChangeEvent);
        }
     }
  }

  /**
   * @author Kee Chin Seong
   */
  private void handleMouseClickEvent(final Object object)
  {
    
  }

  /**
   * @author Kee Chin Seong
   */
  private void handleGuiChangesEvent(final Object object)
  {
    
  }

  /**
   * @author Kee Chin Seong
   */
  private void updateProjectData()
  {
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void copyToButton_actionPerformed(ActionEvent e)
  {
    Object obj = _selectLandPatternComboBox.getSelectedItem();
    if (obj instanceof LandPattern)
    {
      LandPattern originalLandPattern = (LandPattern)obj;
      String landPatName = JOptionPane.showInputDialog(this,
                                                       StringLocalizer.keyToString("CAD_ENTER_ITEM_NAME_KEY"),
                                                       StringLocalizer.keyToString("CAD_COPY_DIALOG_TITLE_KEY"),
                                                       JOptionPane.INFORMATION_MESSAGE);
      if (landPatName != null) // null means the dialog was canceled
      {
        com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
        if (panel.isLandPatternNameValid(landPatName) == false)
        {
          String illegalChars = panel.getLandPatternNameIllegalChars();
          LocalizedString message = null;
          if (illegalChars.equals(" "))
            message = new LocalizedString("CAD_INVALID_LAND_PATTERN_NAME_SPACES_KEY", new Object[]
                                          {landPatName});
          else
            message = new LocalizedString("CAD_INVALID_LAND_PATTERN_NAME_OTHER_CHARS_KEY", new Object[]
                                          {landPatName, illegalChars});

          JOptionPane.showMessageDialog(this,
                                        StringLocalizer.keyToString(message),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
          return;
        }
        else if (panel.isLandPatternNameDuplicate(landPatName))
        {
          JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("CAD_LAND_PATTERN_NAME_DUPLICATE_KEY"),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
          return;
        }
        else
        {
          // this call generates a "new LP created" event whose servicing will update the lp combo box
          LandPatternCopyCommand landPatternCopyCommand = new LandPatternCopyCommand(originalLandPattern, landPatName);
          try
          {
            //_commandManager.trackState(getCurrentUndoState());
            _commandManager.execute(landPatternCopyCommand);
            LandPattern newLandPattern = landPatternCopyCommand.getNewLandPattern();
            _landPatternDetailsTableModel.populateWithLandPattern(newLandPattern);
            if (newLandPattern.getLandPatternPads().size() > 0)
            {
              _lpGraphicsPanel.displayLandPattern(newLandPattern);
            }
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          }
        }
      }
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  private void updateData(ProjectChangeEvent projectChangeEvent)
  {
    Assert.expect(projectChangeEvent != null);

    ProjectChangeEventEnum projectChangeEventEnum = projectChangeEvent.getProjectChangeEventEnum();

    if (projectChangeEventEnum instanceof LandPatternEventEnum)
    {
      LandPatternEventEnum landPatternEventEnum = (LandPatternEventEnum)projectChangeEventEnum;
      if (landPatternEventEnum.equals(LandPatternEventEnum.CREATE_OR_DESTROY) || landPatternEventEnum.equals(LandPatternEventEnum.ADD_OR_REMOVE))
      {
        Object oldValue = projectChangeEvent.getOldValue();
        Object newValue = projectChangeEvent.getNewValue();
        if (newValue != null) // this is a create event
        {
          Assert.expect(oldValue == null);
          Assert.expect(newValue instanceof LandPattern);
          LandPattern newLandPattern = (LandPattern)newValue;
          
          _currentLandPattern = newLandPattern;
          
          _landPatternDetailsTableModel.populateWithLandPattern(_currentLandPattern);
          
          _landPatterns.add(newLandPattern);
          Collections.sort(_landPatterns, new LandPatternAlphaNumericComparator(true));
          populateLandPatternComboBox();
          
        }
        if (oldValue != null) // this is a destroy event
        {
          Assert.expect(newValue == null);
          Assert.expect(oldValue instanceof LandPattern);
          LandPattern oldLandPattern = (LandPattern)oldValue;
          
          _selectLandPatternComboBox.removeItem(oldLandPattern);
        }
      }
      else
      {
      }
    }
    else if (projectChangeEventEnum instanceof LandPatternPadEventEnum)
    {
      LandPatternPadEventEnum landPatternPadEventEnum = (LandPatternPadEventEnum)projectChangeEventEnum;
      Object oldValue = projectChangeEvent.getOldValue();
      Object newValue = projectChangeEvent.getNewValue();

      if (landPatternPadEventEnum.equals(LandPatternPadEventEnum.CREATE_OR_DESTROY) || landPatternPadEventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE))
      {
        if (newValue != null) // this is a create event
        {
          Assert.expect(oldValue == null);
          Assert.expect(newValue instanceof LandPatternPad);
          LandPatternPad newLandPatternPad = (LandPatternPad)newValue;
          _landPatternDetailsTableModel.addLandPatternPad(newLandPatternPad);
        }
        if (oldValue != null) // this is a destroy event
        {
          Assert.expect(newValue == null);
          Assert.expect(oldValue instanceof LandPatternPad);
          LandPatternPad oldLandPatternPad = (LandPatternPad)oldValue;
          _landPatternDetailsTableModel.deleteLandPatternPad(oldLandPatternPad);
        }
      }
      else if (landPatternPadEventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE_LIST))
      {
        if (newValue != null) // this is a create event
        {
          Assert.expect(oldValue == null);
          Assert.expect(newValue instanceof ArrayList);
          java.util.List<LandPatternPad> landPatternPads = (java.util.ArrayList<LandPatternPad>) newValue;

          for (LandPatternPad landPatternPad : landPatternPads)
          {
            _landPatternDetailsTableModel.addLandPatternPad(landPatternPad);
          }
        }
        if (oldValue != null) // this is a destroy event
        {
          Assert.expect(newValue == null);
          Assert.expect(oldValue instanceof ArrayList);
          java.util.List<LandPatternPad> landPatternPads = (java.util.ArrayList<LandPatternPad>) oldValue;

          for (LandPatternPad landPatternPad : landPatternPads)
          {
            _landPatternDetailsTableModel.deleteLandPatternPad(landPatternPad);
          }
        }
      }
      else
      {
        Object source = projectChangeEvent.getSource();
        if(source instanceof LandPatternPad)
        {
          Assert.expect(source != null);
         
          if ((_firstRow >= 0) && (_lastRow >= 0)) // java will wipe out the cell editors if a table is asked to update a negative row range
            _landPatternDetailsTableModel.fireTableRowsUpdated(_firstRow, _lastRow);
          //if (_landPatternTabIsVisible)
          //  _testDev.landPatternScaleDrawing();
        }
      }
    }
    else if (projectChangeEventEnum instanceof ThroughHoleLandPatternPadEventEnum)
    {
      ThroughHoleLandPatternPadEventEnum throughHoleLandPatternPadEventEnum = (ThroughHoleLandPatternPadEventEnum)projectChangeEventEnum;
      if (throughHoleLandPatternPadEventEnum.equals(ThroughHoleLandPatternPadEventEnum.HOLE_DIAMETER) ||
          throughHoleLandPatternPadEventEnum.equals(ThroughHoleLandPatternPadEventEnum.HOLE_COORDINATE))
      {
        Object source = projectChangeEvent.getSource();
        Assert.expect(source instanceof ThroughHoleLandPatternPad);
        Assert.expect(source != null);
        ThroughHoleLandPatternPad landPatternPad = (ThroughHoleLandPatternPad)source;

        int row = _landPatternDetailsTableModel.getRowForLandPatternPad(landPatternPad);
        if (row >= 0) // java will wipe out the cell editors if a table is asked to update a negative row range
          _landPatternDetailsTableModel.fireTableRowsUpdated(row, row);;
      }
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void jbInit()
  {
    setLayout(_editCadBorderLayout);
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    add(_editLandPatternPanel, BorderLayout.CENTER);

    _editLandPatternPanel.setLayout(_editLandPatternPanelBorderLayout);
    _editLandPatternPanelBorderLayout.setVgap(10);
    _editLandPatternPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
                                                                                                                         new Color(142, 142, 142)),
                                                                                        StringLocalizer.keyToString("CAD_EDIT_OR_CREATE_LP_PANEL_BORDER_KEY")),
                                                                       BorderFactory.createEmptyBorder(0, 5, 10, 5)));
    _editLandPatternPanel.add(_editLandPatternDetailsPanel, BorderLayout.CENTER);
    _editLandPatternPanel.add(_editLandPatternInfoPanel, BorderLayout.SOUTH);

    _editLandPatternDetailsPanel.setLayout(_editLandPatternDetailsPanelBorderLayout);
    _editLandPatternDetailsPanel.add(_boardAndLandPatternSelectPanel, BorderLayout.NORTH);
    _editLandPatternDetailsPanel.add(_landPatternDetailsPanel, BorderLayout.CENTER);
    _editLandPatternDetailsPanel.add(_landPatternActionsWrapperPanel, BorderLayout.SOUTH);

    _boardAndLandPatternSelectPanel.setLayout(_boardAndLandPatternSelectPanelBorderLayout);
    _boardAndLandPatternSelectPanel.add(_landPatternSelectPanel, BorderLayout.CENTER);

    _landPatternSelectPanel.setLayout(new BorderLayout());
    _landPatternSelectPanel.add(_selectLandPatternLabel, BorderLayout.WEST);
    _selectLandPatternLabel.setText(StringLocalizer.keyToString("CAD_SELECT_LP_LABEL_KEY"));

    JPanel holderPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    holderPanel.add(_selectLandPatternComboBox);
    _selectLandPatternComboBox.setMaximumRowCount(30);
    _landPatternSelectPanel.add(holderPanel, BorderLayout.CENTER);

    _selectLandPatternActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        landPatternSelectComboBox_actionPerformed(e);
      }
    };

    _landPatternDetailsPanel.setLayout(_landPatternDetailsPanelBorderLayout);
    _landPatternDetailsPanel.add(_landPatternDetailsScrollPane, BorderLayout.CENTER);
    
    _landPatternDetailsTableModel = new LandPatternEditorDetailsTableModel(MainMenuGui.getInstance());
    _landPatternDetailsTable = new LandPatternEditorDetailsTable(this)
    {
      // this overload allows multiple rows to be edited at once, for any one column
      public void setValueAt(Object value, int row, int column)
      {
        // do nothing
      }
      /**
       * We override this to make sure the selected rows are still the proper selected
       * rows after a sort.
       * @author Kee Chin Seong
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = getSelectedRows();
        java.util.List<Object> selectedData = new ArrayList<Object>();
        for (int i = 0; i < selectedRows.length; i++)
        {
          selectedData.add(_landPatternDetailsTableModel.getLandPatternPadAt(selectedRows[i]));
        }

        super.mouseReleased(event);

        // now, reselect everything
        _landPatternDetailsTable.clearSelection();
        Iterator it = selectedData.iterator();
        while (it.hasNext())
        {
          int row;
          Object obj = it.next();
          row = _landPatternDetailsTableModel.getRowForLandPatternPad((LandPatternPad)obj);
          addRowSelectionInterval(row, row);
        }
      }

    };
    _landPatternDetailsTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    _landPatternDetailsTable.setModel(_landPatternDetailsTableModel);
    _landPatternDetailsScrollPane.getViewport().add(_landPatternDetailsTable, null);
    // add the listener for a table row selection
    ListSelectionModel rowSM = _landPatternDetailsTable.getSelectionModel();
    rowSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();

        if (lsm.isSelectionEmpty() || lsm.getValueIsAdjusting())
        {
            // do nothing
        }
        else
        {
          // we want to select the pad(s) in the graphics window corresponding to the newly selected table row(s)
          int selectedRows[] = _landPatternDetailsTable.getSelectedRows();
          java.util.List<LandPatternPad> landPatternPads = new ArrayList<LandPatternPad>();
          for (int i = 0; i < selectedRows.length; i++)
          {
            LandPatternPad landPatternPad = _landPatternDetailsTableModel.getLandPatternPadAt(selectedRows[i]);
            landPatternPads.add(landPatternPad);
          }
          _landPatternDetailsTableModel.setSelectedPads(landPatternPads);
          _lpGraphicsPanel.getLandPatternEditorGraphicsEngineSetup().displayLandPattern(_currentLandPattern);
          _guiObservable.stateChanged(landPatternPads, SelectionEventEnum.LANDPATTERN_PAD);
        }
      }
    });

    _landPatternActionsWrapperPanel.add(_landPatternActionsPanel);
    _landPatternActionsWrapperPanel.setLayout(_landPatternActionsWrapperPanelFlowLayout);
    _landPatternActionsWrapperPanelFlowLayout.setAlignment(FlowLayout.LEFT);

    _landPatternActionsPanel.setLayout(_landPatternActionsPanelGridLayout);
    _landPatternActionsPanelGridLayout.setColumns(2);
    _landPatternActionsPanelGridLayout.setHgap(10);
    _landPatternActionsPanelGridLayout.setRows(0);
    _landPatternActionsPanelGridLayout.setVgap(10);

    _landPatternActionsPanel.add(_cropImageToLandPatternEditor, null);
    _cropImageToLandPatternEditor.setText(StringLocalizer.keyToString("CAD_CROP_IMAGE_TO_LP_EDITOR_KEY"));
    _cropImageToLandPatternEditor.setToolTipText(StringLocalizer.keyToString("CAD_CROP_IMAGE_TOOL_TIP_KEY"));
    _cropImageToLandPatternEditor.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cropImageForLandPatternEditor();
      }
    });
    _landPatternActionsPanel.add(_editSelectedLandPatternButton, null);
    _editSelectedLandPatternButton.setText(StringLocalizer.keyToString("CAD_EDIT_SELECTED_LAND_PATTERN_KEY"));
    _editSelectedLandPatternButton.setEnabled(false);
    _editSelectedLandPatternButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        editSelectedLandPattern();
      }
    });
    _landPatternActionsPanel.add(_exportLandPatternsButton, null);
    _exportLandPatternsButton.setText(StringLocalizer.keyToString("CAD_EXPORT_LAND_PATTERNS_KEY"));
    _exportLandPatternsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ////To do : Export Land PAttern.
        ExportLandPatternWizardDialog dialog = new ExportLandPatternWizardDialog(MainMenuGui.getInstance(), true, _project);
        dialog.pack();
        SwingUtils.centerOnComponent(dialog, MainMenuGui.getInstance());
        dialog.setVisible(true);
        dialog.dispose();
      }
    });
    
    _landPatternActionsPanel.add(_importLandPatternsButton, null);
    _importLandPatternsButton.setText(StringLocalizer.keyToString("CAD_IMPORT_LAND_PATTERNS_KEY"));
    _importLandPatternsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ////To do : Import Land PAttern.
        ImportLandPatternWizardDialog dialog = new ImportLandPatternWizardDialog(MainMenuGui.getInstance(), true, _project);
        dialog.pack();
        SwingUtils.centerOnComponent(dialog, MainMenuGui.getInstance());
        dialog.setVisible(true);
        dialog.dispose();  
      }
    });
    
    _landPatternActionsPanel.add(_copyToButton, null);
    _copyToButton.setText(StringLocalizer.keyToString("CAD_COPY_LP_TO_KEY"));
    _copyToButton.setEnabled(false);
    _copyToButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        copyToButton_actionPerformed(e);
      }
    });
    
    
    _landPatternActionsPanel.add(_deleteUnusedLandPatternButton, null);
    _deleteUnusedLandPatternButton.setText(StringLocalizer.keyToString("CAD_DELETE_LP_KEY"));
    _deleteUnusedLandPatternButton.setEnabled(false);
    _deleteUnusedLandPatternButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteUnusedLandPatternButton_actionPerformed(e);
      }
    });
    
    _editLandPatternInfoPanel.setLayout(_editLandPatternInfoPanelBorderLayout);
    _editLandPatternInfoPanel.add(_howToEditLandPatternPad);
    _howToEditLandPatternPad.setForeground(_labelColor);
    _howToEditLandPatternPad.setText(StringLocalizer.keyToString("CAD_EDIT_LPP_INSTRUCTIONS_LABEL_KEY"));

    _selectLandPatternComboBox.setName(".selectLandPatternComboBox");
    _cropImageToLandPatternEditor.setName(".addPadButton");
    _editSelectedLandPatternButton.setName(".editSleectedLandPatternButton");
    _exportLandPatternsButton.setName(".exportLandPatternsButton");
    _importLandPatternsButton.setName(".importLandPatternsButton");
    _copyToButton.setName(".copyToButton");
    _deleteUnusedLandPatternButton.setName(".deleteLandPatternButton");

    _landPatterns = new ArrayList<LandPattern>();
    Collections.sort(_landPatterns, new LandPatternAlphaNumericComparator(true));
    populateLandPatternComboBox();
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void deleteUnusedLandPatternButton_actionPerformed(ActionEvent e)
  {
    Object entry = _selectLandPatternComboBox.getSelectedItem();
    if (entry instanceof LandPattern == false)
      return;
    LandPattern landPattern = (LandPattern)_selectLandPatternComboBox.getSelectedItem();
    if (landPattern != null)
    {
      if (landPattern.isInUse())
      {
        JOptionPane.showMessageDialog(this,
                                      StringLocalizer.keyToString(new LocalizedString("CAD_CANT_DELETED_USED_LP_KEY",
                                                                                      new Object[]{landPattern.getName()})),
                                      StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                      JOptionPane.INFORMATION_MESSAGE);
        return;
      }
      else if (landPattern.getLandPatternPads().isEmpty() == false)
      {
        //_commandManager.trackState(getCurrentUndoState());
        _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_BLOCK_DELETE_NON_EMPTY_LAND_PATTERN_KEY"));
        try
        {
          _commandManager.execute(new RemoveLandPatternCommand(landPattern));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
        }
        _commandManager.endCommandBlock();
      }
      else
      {
        try
        {
          //_commandManager.trackState(getCurrentUndoState());
          _commandManager.execute(new RemoveLandPatternCommand(landPattern));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
        }
      }
    }
    
    if(_selectLandPatternComboBox.getItemCount() == 0)
    {
       _landPatternDetailsTableModel.clear();
       
      _copyToButton.setEnabled(false);
      _deleteUnusedLandPatternButton.setEnabled(false);
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setLandPatternGraphicsPanel(CreateNewCadPanel lpGraphicsPanel)
  {
    Assert.expect(lpGraphicsPanel != null);
    
    if(_lpGraphicsPanel == null)
       _lpGraphicsPanel = lpGraphicsPanel;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void populateProject(Project project)
  {
    Assert.expect(project != null);
    
    _project = project;
    _landPatterns = project.getPanel().getLandPatterns();
    populateLandPatternComboBox();
    
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setPanelCadGraphicsEngineSetup(CreatePanelGraphicsEngineSetup createPanelGraphicsEngineSetup)
  {
    Assert.expect(createPanelGraphicsEngineSetup != null);
    
    _createPanelGraphicsEngineSetup = createPanelGraphicsEngineSetup;
  }
  
   /*
   * @author Kee Chin Seong
   */
  public void setActive(boolean isActive)
  {
    _active = isActive;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void start()
  {
    //_cadCreatorPanel.switchToLandPatternCadPanel();
    _projectObservable.addObserver(this);
    
    _landPatternGraphicsSelectedRendererObservable = _lpGraphicsPanel.getLandPatternEditorGraphicsEngineSetup().getSelectedRendererObservable();
    _graphicsEngine = _lpGraphicsPanel.getLandPatternEditorGraphicsEngineSetup().getGraphicsEngine();
    
    setActive(true);
    addGraphicsEngineObservers();
    
    String unpopulatedLandPattern = StringLocalizer.keyToString("CAD_UNPOPULATED_LAND_PATTERN_KEY").trim();
    if(unpopulatedLandPattern != null && (_project.getPanel().isLandPatternNameValid(unpopulatedLandPattern) == true && _project.getPanel().isLandPatternNameDuplicate(unpopulatedLandPattern) == false))
    {
        try
        {
          _commandManager.execute(new CreateLandPatternCommand(_project.getPanel(), unpopulatedLandPattern, true)); 
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
        }
    } 
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void addGraphicsEngineObservers()
  {
    _landPatternGraphicsSelectedRendererObservable.addObserver(this);
    _lpGraphicsPanel.getLandPatternEditorGraphicsEngineSetup().addObservers();
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void editSelectedLandPattern()
  {
    //_lpGraphicsPanel.getLandPatternEditorGraphicsEngineSetup().removeObservers();
    //_projectObservable.deleteObserver(this);
    LandPatternCreatorDialog landPatternCreatorDialog = new LandPatternCreatorDialog(MainMenuGui.getInstance(),
            "Land Pattern Editor",
            _project,  _currentLandPattern, _graphicsEngine);
    SwingUtils.centerOnComponent(landPatternCreatorDialog, MainMenuGui.getInstance());
    
    landPatternCreatorDialog.setLandPatternCreatorPanel(this);
    landPatternCreatorDialog.displayLandPattern();
    landPatternCreatorDialog.setVisible(true);
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void cropImageForLandPatternEditor()
  {
    //_lpGraphicsPanel.getLandPatternEditorGraphicsEngineSetup().removeObservers();
    //_projectObservable.deleteObserver(this);
    if(_createPanelGraphicsEngineSetup.getScreenShot() == null)
    {
       JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                     StringLocalizer.keyToString("CC_NO_IMAGE_CROPPED_KEY"),
                                     StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                     JOptionPane.OK_OPTION);
       return;
    }
    _createPanelGraphicsEngineSetup.saveDraggedArea();
    LandPatternCreatorDialog landPatternCreatorDialog = new LandPatternCreatorDialog(MainMenuGui.getInstance(),
                                                                                     "Land Pattern Editor",
                                                                                     _project, _createPanelGraphicsEngineSetup.getScreenShot(), _graphicsEngine);
    if(landPatternCreatorDialog.promptLandPatternCreatorSettings() == false)
       return;
    
    SwingUtils.centerOnComponent(landPatternCreatorDialog, MainMenuGui.getInstance());
    
    landPatternCreatorDialog.setLandPatternCreatorPanel(this);
    landPatternCreatorDialog.setVisible(true);
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void populateLandPatternComboBox()
  {
    _selectLandPatternComboBox.removeActionListener(_selectLandPatternActionListener);
    _selectLandPatternComboBox.removeAllItems();
    boolean flag = _shouldInteractWithGraphics;
    _shouldInteractWithGraphics = false;
    _shouldInteractWithGraphics = flag;
    for (LandPattern lp : _landPatterns)
    {
      _selectLandPatternComboBox.addItem(lp);
    }
    
    if(_landPatterns.size() > 0)
    {
     // _lpGraphicsPanel.displayLandPattern(_landPatterns.get(0));
    }
    _selectLandPatternComboBox.addActionListener(_selectLandPatternActionListener);
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void cancelEditingIfNecessary(JTable table)
  {
    // cancel cell editing in case user deletes the row while editing is still going on.
    if (table.isEditing())
      table.getCellEditor().cancelCellEditing();
    _commandManager.endCommandBlockIfNecessary();
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void landPatternSelectComboBox_actionPerformed(ActionEvent e)
  {
    if(_active == false)
      return;
    
    cancelEditingIfNecessary(_landPatternDetailsTable);
    Object selectedLp = _selectLandPatternComboBox.getSelectedItem();
    if ((selectedLp != null) && ((selectedLp instanceof String) == false)) // this is NOT the "new . . ." case
    {
      _deleteUnusedLandPatternButton.setEnabled(true);
      _copyToButton.setEnabled(true);
      _editSelectedLandPatternButton.setEnabled(true);
      
      LandPattern lp = (LandPattern)_selectLandPatternComboBox.getSelectedItem();
      
      _currentLandPattern = lp;
      
      _lpGraphicsPanel.displayLandPattern(_currentLandPattern);
      _landPatternDetailsTableModel.populateWithLandPattern(_currentLandPattern);
    }
    else if (selectedLp != null) // creating a new land pattern
    {
      if (selectedLp instanceof String)
      {
        // new land pattern.
        if (_shouldInteractWithGraphics)
        {
          String newLandPatternName = JOptionPane.showInputDialog(this, StringLocalizer.keyToString("CAD_ENTER_ITEM_NAME_KEY"),
              StringLocalizer.keyToString("CAD_NEW_LP_TITLE_KEY"), JOptionPane.INFORMATION_MESSAGE);
          if (newLandPatternName != null) // null means the dialog was canceled
          {
            com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
            _deleteUnusedLandPatternButton.setEnabled(false);
            _copyToButton.setEnabled(false);
            while (newLandPatternName != null && (panel.isLandPatternNameValid(newLandPatternName) == false || panel.isLandPatternNameDuplicate(newLandPatternName)))
            {
              if (panel.isLandPatternNameValid(newLandPatternName) == false)
              {
                String illegalChars = panel.getLandPatternNameIllegalChars();
                LocalizedString message = null;
                if (illegalChars.equals(" "))
                  message = new LocalizedString("CAD_INVALID_LAND_PATTERN_NAME_SPACES_KEY", new Object[]
                                                {newLandPatternName});
                else
                  message = new LocalizedString("CAD_INVALID_LAND_PATTERN_NAME_OTHER_CHARS_KEY", new Object[]
                                                {newLandPatternName, illegalChars});

                newLandPatternName = JOptionPane.showInputDialog(this,
                                                                 StringLocalizer.keyToString(message),
                                                                 StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                                 JOptionPane.INFORMATION_MESSAGE);
              }
              else if (panel.isLandPatternNameDuplicate(newLandPatternName))
              {
                newLandPatternName = JOptionPane.showInputDialog(this,
                                                        StringLocalizer.keyToString("CAD_LAND_PATTERN_NAME_DUPLICATE_KEY"),
                                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                        JOptionPane.INFORMATION_MESSAGE);
              }
            }
            if (newLandPatternName == null)
              return; // dialog was canceled without a name entered

            try
            {
              _landPatternDetailsTableModel.clear();

              //_commandManager.trackState(getCurrentUndoState());
              _commandManager.execute(new CreateLandPatternCommand(panel, newLandPatternName, true));

              //_testDev.changeGraphicsToLandPattern(panel.getLandPattern(newLandPatternName));
              _landPatternDetailsTableModel.setProject(_project);
              _deleteUnusedLandPatternButton.setEnabled(true);
              _copyToButton.setEnabled(true);
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            }
          }
        }
      }
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void finish()
  {
    _project = null;

    _selectLandPatternComboBox.removeAllItems();
    if (_landPatterns != null)
      _landPatterns.clear();
    _selectLandPatternComboBox.removeAllItems();
    _landPatternDetailsTable.resetEditorsAndRenderers();
    _landPatternDetailsTableModel.clear();
    _landPatternDetailsTable.resetSortHeader();
    
    _createPanelGraphicsEngineSetup.clearVirtualLiveVerificationImageDisplay();
    _lpGraphicsPanel.getLandPatternEditorGraphicsEngineSetup().clearGraphics();
    
    _projectObservable.deleteObserver(this);
    
    _landPatternGraphicsSelectedRendererObservable.deleteObserver(this);
    _guiObservable.deleteObserver(this);
  }
  
  public boolean isReadyToFinish()
  {
    return _active;
  }
}
