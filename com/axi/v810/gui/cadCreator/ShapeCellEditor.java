package com.axi.v810.gui.cadCreator;

import javax.swing.*;
import com.axi.v810.util.*;

/*
 * @author Kee Chin Seong
 */
class ShapeCellEditor extends DefaultCellEditor
{
  //Shape strings
  private final String _CIRCLE = StringLocalizer.keyToString("CAD_CIRCLE_SHAPE_KEY");
  private final String _RECTANGLE = StringLocalizer.keyToString("CAD_RECT_SHAPE_KEY");

  JComboBox _comboBox = null;

  /**
   * @author Kee Chin Seong
   * Constructor.
   * Initializes the drop down box with the shape values
   */
  public ShapeCellEditor()
  {
    super(new JComboBox());
    _comboBox = (JComboBox)getComponent();

    _comboBox.addItem(_CIRCLE);
    _comboBox.addItem(_RECTANGLE);
  }
}
