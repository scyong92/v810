package com.axi.v810.gui;

import com.axi.util.Enum;

/**
 * The base class for all gui events
 * @author George A. David
 */
public class GuiEventEnum extends com.axi.util.Enum
{
  /**
   * @author George A. David
   */
  protected GuiEventEnum(int id)
  {
    super(id);
  }
}
