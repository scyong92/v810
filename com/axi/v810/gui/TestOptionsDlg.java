package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import java.text.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
* This class contains all the GUI elements for the dialog box that allows the user to change test options such as
* Loop count, loop forever/finite setting.  Used in conjunction with the TestOptions class which holds the data.
*
* @author Andy Mechtenberg
*/
public class TestOptionsDlg extends JDialog
{
  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private JButton _cancelButton = new JButton();
  private JLabel _loopCountLabel = new JLabel();
  private DecimalFormat _loopCountFormat = new DecimalFormat();
  private NumericRangePlainDocument _loopCountDoc = new NumericRangePlainDocument();
  private NumericTextField _loopCountField = new NumericTextField();
  private JButton _OKButton = new JButton();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _choicesPanel = new JPanel();
  private JRadioButton _useLoopForever = new JRadioButton();
  private JRadioButton _useLoopCount = new JRadioButton();
  private Box _loopChoiceBox;
  private ButtonGroup _buttonGroup;

  private TestOptions _testOptions;

  private static final int _MIN_LOOP_COUNT = 1;
  private JPanel _buttonInnerPanel = new JPanel();
  private GridLayout _buttonInnerGridLayout = new GridLayout();

  /**
   * The constructor for this modal dialog needs a title and parent.
   *
   * @author Andy Mechtenberg
   * @param frame The parent frame for this dialog
   * @param title The string containing the title of the dialog
   * @param modal A boolean specifying the dialogs modality.  Should be TRUE for this dialog.
   */
  public TestOptionsDlg(Frame frame, String title, boolean modal, TestOptions testOptions)
  {
    super(frame, title, modal);
    try
    {
      _testOptions = testOptions;
      jbInit();
      pack();
    }
    catch(RuntimeException ex)
    {
      Assert.logException(ex);
      ex.printStackTrace();
    }
  }

  /**
   * The default constructor (should not be used)
   *
   * @author Andy Mechtenberg
   *
   */
  TestOptionsDlg()
  {
    this(null, "", false, new TestOptions());
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    _loopChoiceBox = Box.createVerticalBox();
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _loopCountFormat.setParseIntegerOnly(true);
    _loopCountField.setDocument(_loopCountDoc);
    _loopCountField.setFormat(_loopCountFormat);
    _loopCountDoc.setRange(new IntegerRange(_MIN_LOOP_COUNT, Integer.MAX_VALUE));

    _OKButton.setMnemonic('O');
    _OKButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _OKButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    _loopCountLabel.setText(StringLocalizer.keyToString("GUI_ENTER_LOOP_COUNT_KEY"));
    _loopCountField.setPreferredSize(new Dimension(50, 21));
    _loopCountField.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        loopCountField_actionPerformed(e);
      }
    });
    _cancelButton.setMnemonic('C');
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });

    _useLoopForever.setText(StringLocalizer.keyToString("GUI_LOOP_CONTINUOUSLY_LABEL_KEY"));
    _useLoopForever.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        useLoopForever_actionPerformed(e);
      }
    });
    _useLoopCount.setText(StringLocalizer.keyToString("GUI_USE_LOOP_COUNT_KEY"));
    _useLoopCount.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        useLoopCount_actionPerformed(e);
      }
    });
    _buttonInnerPanel.setLayout(_buttonInnerGridLayout);
    _buttonInnerGridLayout.setHgap(10);
    getContentPane().add(_mainPanel);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    _buttonPanel.add(_buttonInnerPanel);
    _buttonInnerPanel.add(_OKButton);
    _buttonInnerPanel.add(_cancelButton);
    _mainPanel.add(_choicesPanel, BorderLayout.NORTH);
    _choicesPanel.add(_loopChoiceBox, null);
    _loopChoiceBox.add(_useLoopForever, null);
    _loopChoiceBox.add(_useLoopCount, null);
    _choicesPanel.add(_loopCountLabel, null);
    _choicesPanel.add(_loopCountField, null);

    _buttonGroup = new ButtonGroup();
    _buttonGroup.add(_useLoopForever);
    _buttonGroup.add(_useLoopCount);
    if (_testOptions.doLoopForever())
    {
      _loopCountField.setEnabled(false);
      _loopCountField.setBackground(Color.lightGray);
      _useLoopForever.setSelected(true);
      _useLoopCount.setSelected(false);
    }
    else
    {
      _loopCountField.setEnabled(true);
      _loopCountField.setBackground(Color.white);
      _useLoopForever.setSelected(false);
      _useLoopCount.setSelected(true);
    }
    _loopCountField.setText(new Integer(_testOptions.getLoopCount()).toString());

    // apm -- added this, then removed because the default behavior is better.
    // default -- ENTER activates the current default button - either CANCEL or OK
    // new -- ENTER when either radio control is active will select that radio button and exit.
//    KeyStroke enter = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true);
//
//    _useLoopForever.registerKeyboardAction(new ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        System.out.println("User hit ENTER when loop forever had focus");
//        useLoopForever_actionPerformed(e);
//        _useLoopForever.setSelected(true);
//        okButton_actionPerformed(e);
//      }
//    }, enter, JComponent.WHEN_FOCUSED);
//
//    _useLoopCount.registerKeyboardAction(new ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        System.out.println("User hit ENTER when use loop count had focus");
//        useLoopCount_actionPerformed(e);
//        _useLoopCount.setSelected(true);
//        okButton_actionPerformed(e);
//      }
//    }, enter, JComponent.WHEN_FOCUSED);
//
  }

  /**
   * @author Andy Mechtenberg
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    if (!_testOptions.doLoopForever())
    {
      if (!checkLoopCountValidity())
        return;
    }

    _testOptions.setLoopForever(_useLoopForever.isSelected());

    try
    {
      _testOptions.setLoopCount(new Integer(_loopCountField.getText()).intValue());
    }
    catch (NumberFormatException ex)
    {
      JOptionPane.showMessageDialog(this, "You must enter an integer greater than 0.", "Test Options", JOptionPane.ERROR_MESSAGE);
      _loopCountField.setText(new Integer(_testOptions.getLoopCount()).toString());
    }

    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    if (e.getActionCommand().equals(StringLocalizer.keyToString("GUI_LOOP_CONTINUOUSLY_LABEL_KEY")))
    {
      useLoopForever_actionPerformed(e);
    }
    if (e.getActionCommand().equals("Use Loop Count"))
    {
      useLoopCount_actionPerformed(e);
    }
    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void useLoopForever_actionPerformed(ActionEvent e)
  {
    _loopCountField.setEnabled(false);
    _loopCountField.setBackground(Color.lightGray);
    if (!checkLoopCountValidity())
       return;

  }

  /**
   * @author Andy Mechtenberg
   */
  private void useLoopCount_actionPerformed(ActionEvent e)
  {
    _loopCountField.setEnabled(true);
    _loopCountField.setBackground(Color.white);
    if (!checkLoopCountValidity())
      return;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void loopCountField_actionPerformed(ActionEvent e)
  {
    if (!_testOptions.doLoopForever())
    {
      if (!checkLoopCountValidity())
        return;
    }

    _testOptions.setLoopForever(_useLoopForever.isSelected());

    try
    {
      _testOptions.setLoopCount(new Integer(_loopCountField.getText()).intValue());
    }
    catch (NumberFormatException ex)
    {
      JOptionPane.showMessageDialog(this, "You must enter an integer greater than 0.", "Test Options", JOptionPane.ERROR_MESSAGE);
      _loopCountField.setText(new Integer(_testOptions.getLoopCount()).toString());
    }

    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  private boolean checkLoopCountValidity()
  {
    boolean isValidNumber = false;

    try
    {
      // try to convert the field text to an integer.  if it fails, catch the exception and inform the user
      Integer integer = new Integer(_loopCountField.getText());
      if (integer.intValue() >= _MIN_LOOP_COUNT)
        isValidNumber = true;
    }
    catch (NumberFormatException e)
    {
      // do nothing, flag is already false
    }

    if (isValidNumber == false)
    {
      JOptionPane.showMessageDialog(this, "You must enter an integer greater than 0.", "Test Options", JOptionPane.ERROR_MESSAGE);
      _loopCountField.setText(new Integer(_testOptions.getLoopCount()).toString());
    }

    return isValidNumber;
  }
}

