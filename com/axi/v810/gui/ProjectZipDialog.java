package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.filechooser.FileFilter;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * Zip up all project files for a selected AXI project.
 * @author Laura Cormos
 */
public class ProjectZipDialog extends JDialog
{
  private static boolean _runFromMain = false;

  private static final String _assertLogFile = "ProjectZipDialog";
  private static final String _appName = StringLocalizer.keyToString("PZD_GUI_NAME_KEY");

  // gui components

  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private JPanel _projectSelectionPanel = new JPanel();
  private BorderLayout _projectSelectionPanelBorderLayout = new BorderLayout();
  private JPanel _projectsDirPanel = new JPanel();
  private BorderLayout _projectsDirPanelBorderLayout = new BorderLayout();
  private JLabel _projectsDirLabel = new JLabel();
  private JLabel _projectsDirLocationLabel = new JLabel();
  private JScrollPane _projectListScrollPane = new JScrollPane();
  private DefaultListModel _projectListModel = new DefaultListModel();
  private JList _projectList = new JList(_projectListModel);
  private JPanel _controlsPanel = new JPanel();
  private BorderLayout _controlsPanelBorderLayout = new BorderLayout();
  private JPanel _southControlsPanel = new JPanel();
  private FlowLayout _southControlsPanelFlowLayout = new FlowLayout();
  private JPanel _buttonPanel = new JPanel();
  private GridLayout _buttonPanelGridLayout = new GridLayout();
  private JButton _zipButton = new JButton();
  private JButton _exitButton = new JButton();
  private JPanel _zipFilePanel = new JPanel();
  private BorderLayout _zipFilePanelBorderLayout = new BorderLayout();
  private JLabel _zipFileLabel = new JLabel();
  private JButton _directoryBrowseButton = new JButton();
  private JTextField _zipFileTextField = new JTextField();
  private JFileChooser _fileChooser = null;
  private Border _empty55105SpaceBorder;
  private Border _empty5505SpaceBorder;
  private KeyListener _escapeKeyListener = null;
  private KeyListener _enterKeyListener = null;

  private String _outputDirectory = null;
  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();

  /**
   * @author Laura Cormos
   */
  public ProjectZipDialog( Frame parentFrame, boolean modal )
  {
    super(parentFrame, _appName, modal);

    try
    {
      jbInit();
    }
    catch(RuntimeException e)
    {
      Assert.logException(e);
    }

    populateProjectList();
    _outputDirectory = Directory.getTempDir();
  }

  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _runFromMain = true;
          MainMenuGui mainMenu = MainMenuGui.getInstance();
          mainMenu.start();
          ProjectZipDialog projectZipDialog = new ProjectZipDialog(mainMenu, true);
          projectZipDialog.pack();
          projectZipDialog.setVisible(true);
        }
        catch (RuntimeException re)
        {
          re.printStackTrace();
          Assert.logException(re);
        }
      }
    });
  }

  private void jbInit()
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _empty55105SpaceBorder = BorderFactory.createEmptyBorder(5,5,10,5);
    _controlsPanel.setLayout(_controlsPanelBorderLayout);
    _zipButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _zipButton.setText(StringLocalizer.keyToString("PZD_GUI_ZIP_BUTTON_KEY"));
    _zipButton.setEnabled(false);
    _zipButton.setMnemonic((StringLocalizer.keyToString("GUI_ZIP_BUTTON_MNEMONIC_KEY")).charAt(0));
    _zipButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        zipButton_actionPerformed();
      }
    });
    _exitButton.setActionCommand("exitButton");
    _exitButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _exitButton.setText(StringLocalizer.keyToString("GUI_EXIT_BUTTON_KEY"));
    _exitButton.setMnemonic((StringLocalizer.keyToString("GUI_EXIT_BUTTON_MNEMONIC_KEY")).charAt(0));
    _exitButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exitButton_actionPerformed(e);
      }
    });
    _southControlsPanel.setLayout(_southControlsPanelFlowLayout);

    setResizable(true);
    _projectSelectionPanel.setLayout(_projectSelectionPanelBorderLayout);
    _projectSelectionPanelBorderLayout.setHgap(5);
    _projectSelectionPanelBorderLayout.setVgap(5);
    _projectList.setLayoutOrientation(JList.VERTICAL_WRAP);
    _projectList.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        _projectList_mouseReleased(e);
      }
    });
    _projectList.setFixedCellWidth(120);
    _projectList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
    _projectList.addListSelectionListener(new javax.swing.event.ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        projectList_valueChanged(e);
      }
    });
    _buttonPanel.setLayout(_buttonPanelGridLayout);
    _buttonPanelGridLayout.setColumns(2);
    _buttonPanelGridLayout.setHgap(10);
    _buttonPanelGridLayout.setVgap(20);
    _zipFileLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _zipFileLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _zipFileLabel.setLabelFor(_zipFileTextField);
    _zipFileLabel.setText(StringLocalizer.keyToString("PZD_GUI_ZIP_FILE_LABEL_KEY"));
    _directoryBrowseButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _directoryBrowseButton.setText(StringLocalizer.keyToString("GUI_BROWSE_BUTTON_KEY"));
    _directoryBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        zipFileBrowseButton_actionPerformed(e);
      }
    });
    _zipFileTextField.setHorizontalAlignment(SwingConstants.LEFT);
    _zipFileTextField.setPreferredSize(new Dimension(220, 20));
    _zipFileTextField.setText(_outputDirectory);
    _zipFileTextField.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyTyped(KeyEvent e)
      {
        zipFileTextField_keyTyped(e);
      }
    });
    _empty5505SpaceBorder = BorderFactory.createEmptyBorder(5, 5, 0, 5);
    _zipFilePanelBorderLayout.setHgap(10);
    _zipFilePanel.setLayout(_zipFilePanelBorderLayout);
    _zipFilePanel.setBorder(_empty55105SpaceBorder);
    _zipFilePanel.setMinimumSize(new Dimension(405, 31));
    _projectsDirLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _projectsDirLabel.setHorizontalTextPosition(SwingConstants.LEFT);
    _projectsDirLabel.setText(StringLocalizer.keyToString("PZD_GUI_PROJECTS_DIR_LABEL_KEY"));
    _projectsDirLocationLabel.setText(Directory.getProjectsDir());
    _projectSelectionPanel.setBorder(_empty5505SpaceBorder);
    _projectsDirPanelBorderLayout.setHgap(5);
    _mainPanel.add(_controlsPanel, BorderLayout.SOUTH);
    _controlsPanel.add(_southControlsPanel,  BorderLayout.SOUTH);
    _southControlsPanel.add(_buttonPanel, null);
    _buttonPanel.add(_zipButton, null);
    _buttonPanel.add(_exitButton, null);
    _controlsPanel.add(_zipFilePanel,  BorderLayout.CENTER);
    _zipFilePanel.add(_zipFileLabel, BorderLayout.WEST);
    _zipFilePanel.add(_zipFileTextField, BorderLayout.CENTER);
    _zipFilePanel.add(_directoryBrowseButton, BorderLayout.EAST);
    _mainPanel.add(_projectSelectionPanel, BorderLayout.CENTER);
    _projectListScrollPane.getViewport().add(_projectList, null);
    _projectsDirPanel.setLayout(_projectsDirPanelBorderLayout);
    _projectSelectionPanel.add(_projectsDirPanel, BorderLayout.NORTH);
    _projectsDirPanel.add(_projectsDirLabel, BorderLayout.WEST);
    _projectsDirPanel.add(_projectsDirLocationLabel, BorderLayout.CENTER);
    _projectSelectionPanel.add(_projectListScrollPane, BorderLayout.CENTER);
    getContentPane().add(_mainPanel, BorderLayout.CENTER);
    _escapeKeyListener = new KeyAdapter()
    {
      public void keyPressed(KeyEvent e)
      {
        checkForEscapeKeyPressed(e);
      }
    };
    addKeyListener(_escapeKeyListener);
    _projectList.addKeyListener(_escapeKeyListener);
    _zipFileTextField.addKeyListener(_escapeKeyListener);
    _enterKeyListener = new KeyAdapter()
    {
      public void keyPressed(KeyEvent e)
      {
        checkForEnterKeyPressed(e);
      }
    };
    _zipFileTextField.addKeyListener(_enterKeyListener);
  }

  /**
   * @author Vincent Wong
   */
  void projectList_valueChanged(ListSelectionEvent e)
  {
    Assert.expect(_projectList != null);

    if (_projectList.getSelectedIndex() != -1)
    {
      if (_outputDirectory != null)
      {
        String zipFileName = _outputDirectory + File.separator + _projectList.getSelectedValue().toString() + FileName.getZipFileExtension();

        _zipFileTextField.setText(zipFileName);
        _zipButton.setEnabled(true);
      }
      else
      {
        _zipFileTextField.setText("");
        _zipButton.setEnabled(false);
      }
    }
    else
    {
      if (_outputDirectory != null)
      {
        _zipFileTextField.setText(_outputDirectory);
        _zipButton.setEnabled(true);
      }
      else
      {
        _zipFileTextField.setText("");
        _zipButton.setEnabled(false);
      }
    }
  }

  /**
   * @author Vincent Wong
   */
  void zipFileBrowseButton_actionPerformed(ActionEvent e)
  {
    String outputDirectory = selectDirectory();

    if (outputDirectory.length() > 0)
    {
      outputDirectory = FileUtil.removeEndFileSeparator(outputDirectory);
      _outputDirectory = outputDirectory;

      String zipFileName = null;

      if (_projectList.getSelectedIndex() != -1)
      {
        zipFileName = outputDirectory + File.separator + _projectList.getSelectedValue().toString() + FileName.getZipFileExtension();
      }
      else
      {
        zipFileName = outputDirectory;
      }

      _zipFileTextField.setText(zipFileName);
      _zipButton.setEnabled(true);
    }
  }

  /**
   * The zip procedure goes here.
   * @author Vincent Wong
   */
  void zipButton_actionPerformed()
  {
    Assert.expect(_zipFileTextField != null);
    String zipFileName = _zipFileTextField.getText();

    if ( zipFileName.length() == 0 )
    {
      JOptionPane.showMessageDialog( this, StringLocalizer.keyToString("PZD_FILE_NOT_SPECIFIED_KEY"),
                                     _appName, JOptionPane.ERROR_MESSAGE );
      _zipButton.setEnabled(false);
      _zipFileTextField.setText(_outputDirectory);
      return;
    }

    String extension = FileUtil.getExtension(zipFileName).toLowerCase();

    if ( extension.endsWith( FileName.getZipFileExtension().toLowerCase()) == false )
    {
      JOptionPane.showMessageDialog( this, StringLocalizer.keyToString("PZD_INVALID_FILE_EXTENSION_KEY"),
                                     _appName, JOptionPane.ERROR_MESSAGE );
      _zipButton.setEnabled(false);
      return;
    }

    String baseName = FileUtil.getNameWithoutExtension(zipFileName);

    if ( FileName.hasIllegalChars(baseName) )
    {
      JOptionPane.showMessageDialog( this, StringLocalizer.keyToString("PZD_INVALID_FILE_NAME_KEY"),
                                     _appName, JOptionPane.ERROR_MESSAGE );
      _zipButton.setEnabled(false);
      return;
    }

    String parent = FileUtil.getParent(zipFileName);

    if ( parent.length() == 0 ) // need to specify full path.
    {
      JOptionPane.showMessageDialog( this, StringLocalizer.keyToString("PZD_DIRECTORY_NOT_SPECIFIED_KEY"),
                                     _appName, JOptionPane.ERROR_MESSAGE );
      _zipButton.setEnabled(false);
      return;
    }

    File dir = new File(parent);

    if ( dir.exists() == false ) // path does not exist.
    {
      JOptionPane.showMessageDialog( this, StringLocalizer.keyToString("PZD_DIRECTORY_DOES_NOT_EXIST_KEY"),
                                     _appName, JOptionPane.ERROR_MESSAGE );
      _zipButton.setEnabled(false);
      return;
    }

    if (_projectList.getSelectedValue() == null) // nothing was selected from the projects list
    {
      JOptionPane.showMessageDialog( this, StringLocalizer.keyToString("PZD_PROJECT_NOT_SELECTED_KEY"),
                                     _appName, JOptionPane.ERROR_MESSAGE );
      _zipButton.setEnabled(false);
      return;
    }
    zipFile(_projectList.getSelectedValue().toString(), zipFileName);
  }

  /**
   * When the exit button is pressed the Project Zip Tool will exit.
   * @author Vincent Wong
   */
  private void exitButton_actionPerformed(ActionEvent e)
  {
    exitDialog();
  }

  /**
   * Exit this if the escape key is pressed
   * @author Vincent Wong
   */
  private void checkForEscapeKeyPressed(KeyEvent e)
  {
    if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      exitDialog();
  }

  private void checkForEnterKeyPressed(KeyEvent e)
  {
    if (e.getKeyCode() == KeyEvent.VK_ENTER)
      zipButton_actionPerformed();
  }
  /**
   * @author Vincent Wong
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if ( e.getID() == WindowEvent.WINDOW_CLOSING )
      exitDialog();
  }

  /**
   * @author Laura Cormos
   */
  private void exitDialog()
  {
    if (_runFromMain)
      System.exit(0);
    else
    {
      setVisible(false);
      dispose();
    }
  }

  /**
   * @author Laura Cormos
   */
  private void populateProjectList()
  {
    java.util.List<String> projectsList = FileName.getProjectNames();

    boolean projectsListIsEmpty = true;
    if (projectsList != null)
    {
      for (String projectName : projectsList)
      {
        projectsListIsEmpty = false;
        File projectDir = new File(Directory.getProjectDir(projectName));
        File[] dirs = projectDir.listFiles();
        // do not add empty project directories to the list of available project because the FileUtils.zip utility will
        // throw an exception while trying to create an empty zip file. Rather than catch the exception we'll just
        // not going to show those directories.
        if (dirs.length != 0)
          _projectListModel.addElement(new File(projectName).getName());
      }
      if (projectsListIsEmpty == true)
      {
        _projectList.setPreferredSize(new Dimension(250,50));
        _projectsDirLocationLabel.setText(_projectsDirLocationLabel.getText()+"       ("
                                          +StringLocalizer.keyToString("PZD_NO_PROJECTS_AVAILABLE_KEY")+")");
      }
    }
  }

  /**
   * This method displays a file chooser and return the directory selected by the user.
   * @author Vincent Wong
   */
  private String selectDirectory()
  {
    String dirName = null;

    if ( _fileChooser == null )
    {
       _fileChooser = new JFileChooser();

       // initialize the JFIle Dialog.
       FileFilter fileFilter = _fileChooser.getAcceptAllFileFilter();
       _fileChooser.removeChoosableFileFilter(fileFilter);

       _fileChooser.setFileHidingEnabled(false);
       _fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
       _fileChooser.setSelectedFile(new File(_outputDirectory));
    }

    if ( _fileChooser.showDialog(this,StringLocalizer.keyToString("GUI_SELECT_FOLDER_KEY")) != JFileChooser.APPROVE_OPTION )
      return "";

    File selectedFile = _fileChooser.getSelectedFile();

    if (selectedFile.exists() == true)
    {
      if (selectedFile.isDirectory() == true)
        dirName = selectedFile.getPath();

      dirName = dirName.replace('/', '\\');

      if (dirName.endsWith("\\.")) // current directory
        dirName = dirName.substring(0, dirName.length()-2);
      else if (dirName.endsWith("\\"))
        dirName = dirName.substring(0, dirName.length()-1);
    }

    return dirName;
  }

  /**
   * If user double clicks on a project name, start the zip process automatically
   * @author Laura Cormos
   */
  public void _projectList_mouseReleased(MouseEvent e)
  {
    if(e.getClickCount() == 2)
    {
      //the user has double clicked
      zipButton_actionPerformed();
    }
  }

 /**
   * @author Laura Cormos
   */
  private void zipFile(final String projectName, final String zipFileName)
  {
    File zipFile = new File(zipFileName);

    if (zipFile.exists())
      if (zipFile.isFile())
        if ( JOptionPane.showConfirmDialog( this,StringLocalizer.keyToString("PZD_FILE_EXISTS_OVERWRITE_KEY"),
                                            StringLocalizer.keyToString("PZD_FILE_ALREADY_EXISTS_KEY"),
                                            JOptionPane.YES_NO_OPTION,
                                            JOptionPane.QUESTION_MESSAGE ) == JOptionPane.NO_OPTION )
          return;

    // Change the cursor temporarily and wait for the zip to finish.
    setCursor(new Cursor(Cursor.WAIT_CURSOR));
    setEnabled(false); // do not allow user's input during zipping


    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          FileUtilAxi.zip(Directory.getProjectDir(projectName), zipFileName, Directory.getProjectDir(projectName));
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
              setEnabled(true);

              JOptionPane.showMessageDialog(ProjectZipDialog.this, StringLocalizer.keyToString("PZD_ZIP_COMPLETED_KEY"),
                                            _appName, JOptionPane.INFORMATION_MESSAGE);
              // notify Home panel that project info may have changed
              MainMenuGui.getInstance().updateHomePanel();
            }
          });
        }
        catch (final DatastoreException ie)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
              setEnabled(true); // allow user to press a button
              MessageDialog.reportIOError(ProjectZipDialog.this, ie.getLocalizedMessage());
            }
          });
        }
      }
    });
  }


  /**
   * This method is overwritten from ancenstor Component. It's meant to enable/disable all controls on the dialog while
   * something else is going on
   * @author Laura Cormos
   */
  public void setEnabled(boolean b)
  {
    _exitButton.setEnabled(b);
    _zipButton.setEnabled(b);
    _directoryBrowseButton.setEnabled(b);
  }

  /**
   * @author Vincent Wong
   */
  private void zipFileTextField_keyTyped(KeyEvent e)
  {
    _zipButton.setEnabled(true);
  }

}

