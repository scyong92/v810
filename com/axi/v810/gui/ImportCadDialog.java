/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.mainMenu.*;
import static com.axi.v810.gui.mainMenu.MainMenuGui.finishLicenseMonitor;
import static com.axi.v810.gui.mainMenu.MainUIMenuBar.getAllCadTypeNames;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

/**
 * XCR-3661 Able to convert CAD to NDF or V810 recipes
 *
 * @author weng-jian.eoh
 */
public class ImportCadDialog extends EscapeDialog
{

  private JFrame _mainUI;
  // either OK or CANCEL depending on how the user exits
  private int _returnValue;

  private Map<String, String> _cadenceProjectNameToFilePathMap;
  private Map<String, String> _gencadProjectNameToFilePathMap;
  private Map<String, String> _fabmasterProjectNameToFilePathMap;
  private Map<String, String> _odbProjectNameToFilePathMap;

  private Object _selectedRecipe = null;
  private String _selectedRecipeFilePath = null;
  private String _newProjectName = null;
  private String _instruction;
  private String _recipeNameLabel = null;
  private java.util.List<Object> _intialThresholdsFileItems;
  private java.util.List<Object> _intialRecipeSettingFileItems;
  private java.util.List<Object> _jointTypeAssignmentItems;
  private java.util.List<Object> _cadTypeItems;
  private java.util.List<Object> _systemTypeItems;
  private java.util.List<Object> _systemMagItems;
  private JLabel _intialThresholdLabel;
  private JLabel _intialRecipeSettingLabel;
  private JLabel _jointTypeRuleLabel;
  private JLabel _cadTypeLabel;
  private JLabel _systemTypeLabel;
  private JLabel _systemMagLabel;
  private Object _selectedThresholdSet;
  private Object _selectedRecipeSetting;
  private Object _selectedJointTypeAssignment;
  private Object _selectedCadType;
  private Object _selectedSystemType;
  private Object _selectedSystemMagnicification;
  private Object _defaultIntialThreshold;
  private Object _defaultRecipeSetting;
  private Object _defaultJointTypeAssignemnt;
  private Object _defaultCadType;
  private Object _currentSystemType;
  private Object _currentSystemMagnification;

  private Font _buttonFont;
  private Font _messageFont;

  private JPanel _mainPanel = new JPanel();
  private JPanel _messagePanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();

  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private FlowLayout _messagePanelFlowLayout = new FlowLayout();
  private FlowLayout _buttonPanelFlowLayout = new FlowLayout();
  private GridLayout _innerButtonPanelGridLayout = new GridLayout();
  private JLabel _messageLabel = new JLabel();
  private JPanel _inputPanel = new JPanel();
  private DefaultListModel _cadenceChoiceListModel = new DefaultListModel();
  private DefaultListModel _gencadChoiceLlistModel = new DefaultListModel();
  private DefaultListModel _odbChoiceListModel = new DefaultListModel();
  private DefaultListModel _fabmasterChoiceListModel = new DefaultListModel();
  private JScrollPane _choicesScrollPane = new JScrollPane();
  private BorderLayout _inputPanelBorderLayout = new BorderLayout();
  private Border _innerButtonPanelBorder = BorderFactory.createEmptyBorder(5, 10, 0, 10);
  private Border _mainPanelBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
  private JPanel _additionalControlsPanel = new JPanel();
  private BorderLayout _additionalControlsBorderLayout = new BorderLayout();
  private JPanel _additionalControlsInnerPanel = new JPanel();
  private BorderLayout _textFieldBorderLayout = new BorderLayout();
  private JLabel _additionalTextFieldLabel = new JLabel();

  private JList _currentJList;

  private JButton _okButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }

  };
  private JButton _cancelButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  private JList _cadenceChoiceList = new JList(_cadenceChoiceListModel)
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  //Jack Hwee - add new choice list for swithching
  private JList _gencadChoiceList = new JList(_gencadChoiceLlistModel)
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  private JList _odbChoiceList = new JList(_odbChoiceListModel)
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  private JList _fabmasterChoiceList = new JList(_fabmasterChoiceListModel)
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  private JTextField _recipeNameTextField = new JTextField()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
      if (e.getKeyCode() == KeyEvent.VK_ENTER)
      {
        okAction();
      }

    }
  };

  private JComboBox _intialThresholdComboBox = new JComboBox()
  {
    public void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };
  
  private JComboBox _intialRecipeSettingComboBox = new JComboBox()
  {
    public void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  private JComboBox _jointTypeRuleComboBox = new JComboBox()
  {
    public void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  //Jack Hwee - Add combobox to change the choice list
  private JComboBox _cadComboBox = new JComboBox()
  {
    public void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  private JComboBox _systemTypeComboBox = new JComboBox()
  {
    public void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  private JComboBox _systemMagnificationComboBox = new JComboBox()
  {
    public void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  private ImportCadDialog()
  {
    // do nothing
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  private void createDialog()
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _innerButtonPanelGridLayout.setHgap(5);
    _messagePanel.setLayout(_messagePanelFlowLayout);
    _messagePanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _messagePanelFlowLayout.setHgap(0);
    _messagePanelFlowLayout.setVgap(0);
    _cancelButton.setFont(_buttonFont);
    _okButton.setFont(_buttonFont);
    _cancelButton.setMargin(new Insets(2, 8, 2, 8));
    _okButton.setMargin(new Insets(2, 8, 2, 8));
    _messageLabel.setFont(_messageFont);
    _messageLabel.setText(_instruction);
    _inputPanel.setLayout(_inputPanelBorderLayout);
    _buttonPanelFlowLayout.setHgap(0);
    _buttonPanelFlowLayout.setVgap(0);
    _innerButtonPanel.setBorder(_innerButtonPanelBorder);
    _mainPanel.setBorder(_mainPanelBorder);
    _mainPanelBorderLayout.setVgap(5);
    _additionalControlsPanel.setLayout(_additionalControlsBorderLayout);
    _additionalControlsInnerPanel.setLayout(_textFieldBorderLayout);

    getContentPane().add(_mainPanel);
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    _buttonPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _innerButtonPanel.setLayout(_innerButtonPanelGridLayout);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _innerButtonPanel.add(_okButton);
    _innerButtonPanel.add(_cancelButton);
    _additionalControlsPanel.add(_additionalControlsInnerPanel, BorderLayout.NORTH);
    _messagePanel.add(_messageLabel);
    _buttonPanel.add(_innerButtonPanel);
    _mainPanel.add(_inputPanel, BorderLayout.CENTER);

    _choicesScrollPane.getViewport().add(_cadenceChoiceList);
    _currentJList = _cadenceChoiceList;
    _inputPanel.add(_choicesScrollPane, BorderLayout.CENTER);
    _cadenceChoiceList.setVisibleRowCount(10);
    _cadenceChoiceList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _cadenceChoiceList.addMouseListener(new MouseAdapter()
    {

      public void mousePressed(MouseEvent e)
      {
        // if the user double clicks on a entry, automatically select it and exit the dialog
        if (e.getClickCount() == 1)
        {
          int index = _cadenceChoiceList.locationToIndex(e.getPoint());
          if (index != -1)
          {
            _selectedRecipe = _cadenceChoiceList.getModel().getElementAt(index);
            if (_selectedRecipe != null)
            {
              _recipeNameTextField.setText(_selectedRecipe.toString());
            }
          }
        }
        else if (e.getClickCount() == 2 && _cadenceChoiceListModel.isEmpty() == false)
        {
          int index = _cadenceChoiceList.locationToIndex(e.getPoint());
          if (index != -1)
          {
            _selectedRecipe = _cadenceChoiceList.getModel().getElementAt(index);
            _newProjectName = _selectedRecipe.toString();
            _returnValue = JOptionPane.OK_OPTION;
            dispose();
          }
        }
      }
    });

    //Jack Hwee - add new choice list for swithching
    _gencadChoiceList.setVisibleRowCount(10);
    _gencadChoiceList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _gencadChoiceList.addMouseListener(new MouseAdapter()
    {

      public void mousePressed(MouseEvent e)
      {
        // if the user double clicks on a entry, automatically select it and exit the dialog
        if (e.getClickCount() == 1)
        {
          int index = _gencadChoiceList.locationToIndex(e.getPoint());
          if (index != -1)
          {
            _selectedRecipe = _gencadChoiceList.getModel().getElementAt(index);
            if (_selectedRecipe != null)
            {
              _recipeNameTextField.setText(_selectedRecipe.toString());
            }

          }
        }
        else if (e.getClickCount() == 2 && _gencadChoiceLlistModel.isEmpty() == false)
        {
          int index = _gencadChoiceList.locationToIndex(e.getPoint());
          if (index != -1)
          {
            _selectedRecipe = _gencadChoiceList.getModel().getElementAt(index);
            _newProjectName = _selectedRecipe.toString();
            _returnValue = JOptionPane.OK_OPTION;
            dispose();
          }
        }
      }
    });

    _odbChoiceList.setVisibleRowCount(10);
    _odbChoiceList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _odbChoiceList.addMouseListener(new MouseAdapter()
    {

      public void mousePressed(MouseEvent e)
      {
        // if the user double clicks on a entry, automatically select it and exit the dialog
        if (e.getClickCount() == 1)
        {
          int index = _odbChoiceList.locationToIndex(e.getPoint());
          if (index != -1)
          {
            _selectedRecipe = _odbChoiceList.getModel().getElementAt(index);
            if (_selectedRecipe != null)
            {
              _recipeNameTextField.setText(_selectedRecipe.toString());
            }

          }
        }
        else if (e.getClickCount() == 2 && _odbChoiceListModel.isEmpty() == false)
        {
          int index = _odbChoiceList.locationToIndex(e.getPoint());
          if (index != -1)
          {
            _selectedRecipe = _odbChoiceList.getModel().getElementAt(index);
            _newProjectName = _selectedRecipe.toString();
            _returnValue = JOptionPane.OK_OPTION;
            dispose();
          }
        }
      }
    });

    _gencadChoiceList.setVisibleRowCount(10);
    _gencadChoiceList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _gencadChoiceList.addMouseListener(new MouseAdapter()
    {

      public void mousePressed(MouseEvent e)
      {
        // if the user double clicks on a entry, automatically select it and exit the dialog
        if (e.getClickCount() == 1)
        {
          int index = _fabmasterChoiceList.locationToIndex(e.getPoint());
          if (index != -1)
          {
            _selectedRecipe = _fabmasterChoiceList.getModel().getElementAt(index);
            if (_selectedRecipe != null)
            {
              _recipeNameTextField.setText(_selectedRecipe.toString());
            }

          }
        }
        else if (e.getClickCount() == 2 && _fabmasterChoiceListModel.isEmpty() == false)
        {
          int index = _fabmasterChoiceList.locationToIndex(e.getPoint());
          if (index != -1)
          {
            _selectedRecipe = _fabmasterChoiceList.getModel().getElementAt(index);
            _newProjectName = _selectedRecipe.toString();
            _returnValue = JOptionPane.OK_OPTION;
            dispose();
          }
        }
      }
    });

    _cadenceChoiceList.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      // this ensures that a selected item is visible.  At jdk1.5, this is the default behavior, so this could be removed.

      public void valueChanged(ListSelectionEvent e)
      {
        if (e.getValueIsAdjusting() == false && _cadenceChoiceListModel.isEmpty() == false)
        {
          int index = _cadenceChoiceList.getSelectedIndex();
          _cadenceChoiceList.ensureIndexIsVisible(index);
          _recipeNameTextField.setText(_cadenceChoiceList.getSelectedValue().toString());
        }
      }
    });

    //Jack Hwee - add new choice list for switching
    _gencadChoiceList.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      // this ensures that a selected item is visible.  At jdk1.5, this is the default behavior, so this could be removed.

      public void valueChanged(ListSelectionEvent e)
      {
        if (e.getValueIsAdjusting() == false && _gencadChoiceLlistModel.isEmpty() == false)
        {
          int index = _gencadChoiceList.getSelectedIndex();
          _gencadChoiceList.ensureIndexIsVisible(index);
          _recipeNameTextField.setText(_gencadChoiceList.getSelectedValue().toString());
        }
      }
    });

    _odbChoiceList.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      // this ensures that a selected item is visible.  At jdk1.5, this is the default behavior, so this could be removed.

      public void valueChanged(ListSelectionEvent e)
      {
        if (e.getValueIsAdjusting() == false && _odbChoiceListModel.isEmpty() == false)
        {
          int index = _odbChoiceList.getSelectedIndex();
          _odbChoiceList.ensureIndexIsVisible(index);
          _recipeNameTextField.setText(_odbChoiceList.getSelectedValue().toString());
        }
      }
    });

    _fabmasterChoiceList.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      // this ensures that a selected item is visible.  At jdk1.5, this is the default behavior, so this could be removed.

      public void valueChanged(ListSelectionEvent e)
      {
        if (e.getValueIsAdjusting() == false && _fabmasterChoiceListModel.isEmpty() == false)
        {
          int index = _fabmasterChoiceList.getSelectedIndex();
          _fabmasterChoiceList.ensureIndexIsVisible(index);
          _recipeNameTextField.setText(_fabmasterChoiceList.getSelectedValue().toString());
        }
      }
    });

    _additionalControlsPanel.add(_buttonPanel, BorderLayout.SOUTH);
    _mainPanel.add(_additionalControlsPanel, BorderLayout.SOUTH);
    _mainPanel.add(_messagePanel, BorderLayout.NORTH);

    _additionalControlsInnerPanel.add(_recipeNameTextField, BorderLayout.CENTER);
    _additionalControlsInnerPanel.add(_additionalTextFieldLabel, BorderLayout.NORTH);
    if (_cadenceChoiceList.getSelectedValue() != null)
    {
      _recipeNameTextField.setText(_cadenceChoiceList.getSelectedValue().toString());
    }
    _additionalTextFieldLabel.setText(_recipeNameLabel);
    _recipeNameTextField.addFocusListener(new FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        _recipeNameTextField.selectAll();
      }

      public void focusLost(FocusEvent e)
      {
        String text = _recipeNameTextField.getText();
        if (text != null || text.equalsIgnoreCase("") == false)
        {
          _newProjectName = text;
          // now enable the ok button
          _okButton.setEnabled(true);
        }
      }
    });

    _recipeNameTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        String text = _recipeNameTextField.getText();
        if (text != null || text.equalsIgnoreCase("") == false)
        {
          _newProjectName = text;
          // now enable the ok button
          _okButton.setEnabled(true);
        }
      }
    });

    JPanel comboboxPanel = new JPanel(new BorderLayout());
    comboboxPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
    comboboxPanel.add(_intialThresholdLabel, BorderLayout.NORTH);
    comboboxPanel.add(_intialThresholdComboBox, BorderLayout.CENTER);

    JPanel combobox2Panel = new JPanel(new BorderLayout());
    combobox2Panel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
    combobox2Panel.add(_intialRecipeSettingLabel, BorderLayout.NORTH);
    combobox2Panel.add(_intialRecipeSettingComboBox, BorderLayout.CENTER);
    
    JPanel combobox3Panel = new JPanel(new BorderLayout());
    combobox3Panel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
    combobox3Panel.add(_jointTypeRuleLabel, BorderLayout.NORTH);
    combobox3Panel.add(_jointTypeRuleComboBox, BorderLayout.CENTER);
    //Jack Hwee - Add combobox for new choice list
    
    JPanel combobox4Panel = new JPanel(new BorderLayout());
    combobox4Panel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
    combobox4Panel.add(_cadTypeLabel, BorderLayout.NORTH);
    combobox4Panel.add(_cadComboBox, BorderLayout.CENTER);

    JPanel combineComboboxPanel2 = null;

    try
    {
      if (LicenseManager.isOfflineProgramming())
      {
        combineComboboxPanel2 = new JPanel(new BorderLayout());
        JPanel combobox5Panel = new JPanel(new BorderLayout());
        combobox5Panel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
        combobox5Panel.add(_systemTypeLabel, BorderLayout.NORTH);
        combobox5Panel.add(_systemTypeComboBox, BorderLayout.CENTER);
        combineComboboxPanel2.add(combobox5Panel, BorderLayout.NORTH);

        if (LicenseManager.getInstance().getSupportedSystemType().contains(SystemTypeEnum.S2EX.getName()))
        {
          JPanel combobox6Panel = new JPanel(new BorderLayout());
          combobox6Panel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
          combobox6Panel.add(_systemMagLabel, BorderLayout.NORTH);
          combobox6Panel.add(_systemMagnificationComboBox, BorderLayout.CENTER);
          combineComboboxPanel2.add(combobox6Panel, BorderLayout.CENTER);
        }
      }
    }
    catch (final BusinessException e)
    {
      finishLicenseMonitor();
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
            e.getMessage(),
            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
            true);
          MainMenuGui.getInstance().exitV810(false);
        }
      });
    }

    JPanel combineComboboxPanel = new JPanel(new BorderLayout());
    combineComboboxPanel.add(comboboxPanel, BorderLayout.NORTH);
    combineComboboxPanel.add(combobox2Panel, BorderLayout.CENTER);

    JPanel combineComboboxPanel1 = new JPanel(new BorderLayout());
    combineComboboxPanel1.add(combobox3Panel, BorderLayout.NORTH);
    combineComboboxPanel1.add(combobox4Panel, BorderLayout.CENTER);

    JPanel bothComboboxPanel = new JPanel(new BorderLayout());
    bothComboboxPanel.add(combineComboboxPanel, BorderLayout.NORTH);
    bothComboboxPanel.add(combineComboboxPanel1, BorderLayout.CENTER);
    if (combineComboboxPanel2 != null)
    {
      bothComboboxPanel.add(combineComboboxPanel2, BorderLayout.SOUTH);
    }

    _additionalControlsInnerPanel.add(bothComboboxPanel, BorderLayout.SOUTH);

    for (Object item : _intialThresholdsFileItems)
    {
      _intialThresholdComboBox.addItem(item);
    }
    for (Object item : _intialRecipeSettingFileItems)
    {
      _intialRecipeSettingComboBox.addItem(item);
    }
    for (Object item : _jointTypeAssignmentItems)
    {
      _jointTypeRuleComboBox.addItem(item);
    }
    for (Object item : _cadTypeItems)
    {
      _cadComboBox.addItem(item);
    }
    for (Object item : _systemTypeItems)
    {
      _systemTypeComboBox.addItem(item);
    }
    for (Object item : _systemMagItems)
    {
      _systemMagnificationComboBox.addItem(item);
    }

    _intialThresholdComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedThresholdSet = _intialThresholdComboBox.getSelectedItem();
      }
    });

    _intialThresholdComboBox.setSelectedItem(_defaultIntialThreshold);
    
    _intialRecipeSettingComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedRecipeSetting = _intialRecipeSettingComboBox.getSelectedItem();
      }
    });

    _intialRecipeSettingComboBox.setSelectedItem(_defaultRecipeSetting);

    _jointTypeRuleComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedJointTypeAssignment = _jointTypeRuleComboBox.getSelectedItem();
      }
    });

    _jointTypeRuleComboBox.setSelectedItem(_defaultJointTypeAssignemnt);

    //Jack Hwee - Add combobox for new choice list
    _cadComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedCadType = _cadComboBox.getSelectedItem();
        switcthChoiceList();
      }
    });

    _cadComboBox.setSelectedItem(_defaultCadType);

    _systemTypeComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedSystemType = _systemTypeComboBox.getSelectedItem();
        if (_selectedSystemType.equals(SystemTypeEnum.S2EX.getName()))
        {
          _systemMagLabel.setEnabled(true);
          _systemMagnificationComboBox.setEnabled(true);
          _selectedSystemMagnicification = _systemMagnificationComboBox.getSelectedItem();
        }
        else
        {
          _systemMagLabel.setEnabled(false);
          _systemMagnificationComboBox.setEnabled(false);
        }
        switcthChoiceList();
      }
    });

    _systemTypeComboBox.setSelectedItem(_currentSystemType);

    _systemMagnificationComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_systemMagnificationComboBox.isEnabled())
        {
          _selectedSystemMagnicification = _systemMagnificationComboBox.getSelectedItem();
        }
        switcthChoiceList();
      }
    });

    _systemMagnificationComboBox.setSelectedItem(_currentSystemMagnification);

    if (_currentSystemType.equals(SystemTypeEnum.S2EX.getName()) == false)
    {
      _systemMagLabel.setEnabled(false);
      _systemMagnificationComboBox.setEnabled(false);
    }

    getRootPane().setDefaultButton(_okButton);
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okAction();
      }
    });

  }

  /**
   * @return int
   * @author Andy Mechtenberg
   */
  public int showDialog()
  {
    SwingUtils.centerOnComponent(this, _mainUI);
    setVisible(true);
    return _returnValue;
  }

  /**
   * @param font Font
   * @return int
   * @author Andy Mechtenberg
   */
  public int showDialog(Font font)
  {
    Assert.expect(font != null);
    SwingUtils.setFont(this, font);
    pack();
    SwingUtils.centerOnComponent(this, _mainUI);

    setVisible(true);
    return _returnValue;
  }

  /**
   * @param e WindowEvent
   * @author Andy Mechtenberg
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    int eventId = e.getID();
    if (eventId == WindowEvent.WINDOW_CLOSING)
    {
      cancelAction();
    }
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public Object getSelectedRecipe()
  {
    return _selectedRecipe;
  }

  public String getSelectedPath()
  {
    return _selectedRecipeFilePath;
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public String getNewProjectName()
  {
    return _newProjectName;
  }

  /**
   * @author Laura Cormos
   */
  public Object getSelectedThresholdsSet()
  {
    return _selectedThresholdSet;
  }
  
  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public Object getSelectedRecipeSettingSet()
  {
    return _selectedRecipeSetting;
  }
  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public Object getSelectedJointTypeAssignment()
  {
    return _selectedJointTypeAssignment;
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public Object getSelectedCadType()
  {
    return _selectedCadType;
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public Object getSelectedSystemType()
  {
    return _selectedSystemType;
  }

  /**
   * XCR-3589 Combo STD and XXL software GUI
   *
   * @author weng-jian.eoh
   * @return
   */
  public Object getSelectedSystemMagnification()
  {
    return _selectedSystemMagnicification;
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  private void okAction()
  {
    java.util.List<Object> cadTypeName = _cadTypeItems;
    Object[] cadTypeItem = cadTypeName.toArray();
    if (_selectedCadType.toString().equals(StringLocalizer.keyToString("MMGUI_SELECT_CADENCE_ALLEGRO_TYPE_KEY")))
    {
      _selectedRecipe = _cadenceChoiceList.getSelectedValue();
      _selectedRecipeFilePath = _cadenceProjectNameToFilePathMap.get(_selectedRecipe);
    }
    else if (_selectedCadType.toString().equals(StringLocalizer.keyToString("MMGUI_SELECT_GENCAD_TYPE_KEY")))
    {
      _selectedRecipe = _gencadChoiceList.getSelectedValue();
      _selectedRecipeFilePath = _gencadProjectNameToFilePathMap.get(_selectedRecipe);
    }
    else if (_selectedCadType.toString().equals(StringLocalizer.keyToString("MMGUI_SELECT_ODB_TYPE_KEY")))
    {
      _selectedRecipe = _odbChoiceList.getSelectedValue();
      _selectedRecipeFilePath = _odbProjectNameToFilePathMap.get(_selectedRecipe);
    }
    else
    {
      _selectedRecipe = _fabmasterChoiceList.getSelectedValue();
      _selectedRecipeFilePath = _fabmasterProjectNameToFilePathMap.get(_selectedRecipe);
    }

    _newProjectName = _recipeNameTextField.getText();
    _returnValue = JOptionPane.OK_OPTION;

    dispose();
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    cancelAction();
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  private void cancelAction()
  {
    _returnValue = JOptionPane.CANCEL_OPTION;
    _selectedRecipe = null;
    dispose();
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  void switcthChoiceList()
  {
    String cadType = (String) _selectedCadType;

    java.util.List<Object> cadTypeItem = _cadTypeItems;
    Object[] cadTypeComboItem = cadTypeItem.toArray();

    if (cadType.equals(StringLocalizer.keyToString("MMGUI_SELECT_CADENCE_ALLEGRO_TYPE_KEY")))
    {
      _choicesScrollPane.getViewport().remove(_currentJList);
      _choicesScrollPane.getViewport().add(_cadenceChoiceList);
      _currentJList = _cadenceChoiceList;
      if (_cadenceChoiceListModel.isEmpty() == false)
      {
        _recipeNameTextField.setText(_cadenceChoiceList.getSelectedValue().toString());
        _okButton.setEnabled(true);
      }
      else
      {
        _recipeNameTextField.setText("");
        _okButton.setEnabled(false);
      }
    }
    else if (cadType.equals(StringLocalizer.keyToString("MMGUI_SELECT_GENCAD_TYPE_KEY")))
    {
      _choicesScrollPane.getViewport().remove(_currentJList);
      _choicesScrollPane.getViewport().add(_gencadChoiceList);
      _currentJList = _gencadChoiceList;
      if (_gencadChoiceLlistModel.isEmpty() == false)
      {
        _recipeNameTextField.setText(_gencadChoiceList.getSelectedValue().toString());
        _okButton.setEnabled(true);
      }
      else
      {
        _recipeNameTextField.setText("");
        _okButton.setEnabled(false);
      }
    }
    else if (cadType.equals(StringLocalizer.keyToString("MMGUI_SELECT_ODB_TYPE_KEY")))
    {
      _choicesScrollPane.getViewport().remove(_currentJList);
      _choicesScrollPane.getViewport().add(_odbChoiceList);
      _currentJList = _odbChoiceList;
      if (_odbChoiceListModel.isEmpty() == false)
      {
        _recipeNameTextField.setText(_odbChoiceList.getSelectedValue().toString());
        _okButton.setEnabled(true);
      }
      else
      {
        _recipeNameTextField.setText("");
        _okButton.setEnabled(false);
      }
    }

    else if (cadType.equals(StringLocalizer.keyToString("MMGUI_SELECT_FABMASTER_TYPE_KEY")))
    {
      _choicesScrollPane.getViewport().remove(_currentJList);
      _choicesScrollPane.getViewport().add(_fabmasterChoiceList);
      _currentJList = _fabmasterChoiceList;
      if (_fabmasterChoiceListModel.isEmpty() == false)
      {
        _recipeNameTextField.setText(_fabmasterChoiceList.getSelectedValue().toString());
        _okButton.setEnabled(true);
      }
      else
      {
        _recipeNameTextField.setText("");
        _okButton.setEnabled(false);
      }
    }
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public ImportCadDialog(JFrame frame, String title)
  {
    super(frame, title, true);

    Assert.expect(frame != null);

    _cadenceProjectNameToFilePathMap = FileName.getCadenceAllegroFileNames(Directory.getCadDirectory());
    _gencadProjectNameToFilePathMap = FileName.getGencadFileNames(Directory.getCadDirectory());
    _fabmasterProjectNameToFilePathMap = FileName.getFabmasterFileNames(Directory.getCadDirectory());
    _odbProjectNameToFilePathMap = FileName.getODBFileNames(Directory.getCadDirectory());

    _recipeNameLabel = StringLocalizer.keyToString("HP_PROJECTS_TABLE_PROJECT_NAME_COLUMN_KEY");

    _intialThresholdLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SELECT_INITIAL_THRESHOLDS_ON_IMPORT_KEY"));

    java.util.List<String> initialThresholdsFileNames = FileName.getAllInitialThresholdSetNames();
    String initialThresholdsFileUsed = ProjectInitialThresholds.getInstance().getInitialThresholdsFileNameWithoutExtension();

    if (initialThresholdsFileNames.contains(initialThresholdsFileUsed) == false)
    {
      initialThresholdsFileUsed = ProjectInitialThresholds.getInstance().getSystemDefaultSetName();
    }

    _intialThresholdsFileItems = new java.util.ArrayList<Object>();
    for (int i = 0; i < initialThresholdsFileNames.size(); i++)
    {
      _intialThresholdsFileItems.add(initialThresholdsFileNames.get(i));
    }

    _defaultIntialThreshold = initialThresholdsFileUsed;
    
    _intialRecipeSettingLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SELECT_INITIAL_RECIPE_SETTING_ON_IMPORT_KEY"));

    java.util.List<String> initialRecipeSettingFileName = FileName.getAllInitialRecipeSettingNames();
    String intialRecipeSettingFileUsed = ProjectInitialRecipeSettings.getInstance().getInitialRecipeSettingWithoutExtension();

    if (initialRecipeSettingFileName.contains(initialThresholdsFileUsed) == false)
    {
      intialRecipeSettingFileUsed = ProjectInitialRecipeSettings.getInstance().getSystemDefaultSetName();
    }

    _intialRecipeSettingFileItems = new java.util.ArrayList<Object>();
    for (int i = 0; i < initialRecipeSettingFileName.size(); i++)
    {
      _intialRecipeSettingFileItems.add(initialRecipeSettingFileName.get(i));
    }

    _defaultRecipeSetting = intialRecipeSettingFileUsed;
    
    _jointTypeRuleLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SELECT_JOINT_TYPE_RULE_SET_ON_IMPORT_KEY"));

    java.util.List<String> jointTypeAssignmentFileNames = FileName.getJointTypeAssignmentFiles();
    String jointTypeFileUsed = JointTypeEnumAssignment.getInstance().getJointTypeAssignmentConfigFileNameWithoutExtension();
    // just in case someone deleted the file to use from disk, add it to the list to show which file we'll attempt to use
    // and later the file not found exception will be caught and dealt with.
    if (jointTypeAssignmentFileNames.contains(jointTypeFileUsed) == false)
    {
      jointTypeAssignmentFileNames.add(jointTypeFileUsed);
    }

    _jointTypeAssignmentItems = new java.util.ArrayList<Object>();
    for (int i = 0; i < jointTypeAssignmentFileNames.size(); i++)
    {
      _jointTypeAssignmentItems.add(jointTypeAssignmentFileNames.get(i));
    }

    _defaultJointTypeAssignemnt = jointTypeFileUsed;

    _cadTypeLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SELECT_CAD_TYPE_KEY"));

    java.util.List<String> _cadTypeItem = getAllCadTypeNames();

    _cadTypeItems = new java.util.ArrayList<Object>();
    for (int i = 0; i < _cadTypeItem.size(); i++)
    {
      _cadTypeItems.add(_cadTypeItem.get(i));
    }

    _defaultCadType = _cadTypeItems.get(0);

    _systemTypeLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SELECT_SYSTEM_TYPE_ON_IMPORT_KEY"));

    java.util.List<String> supportedSystemType = new ArrayList<String>();
    try
    {
      supportedSystemType = LicenseManager.getInstance().getSupportedSystemType();
    }
    catch (final BusinessException e)
    {
      finishLicenseMonitor();
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
            e.getMessage(),
            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
            true);
          MainMenuGui.getInstance().exitV810(false);
        }
      });
    }

    _systemTypeItems = new java.util.ArrayList<Object>();
    for (int i = 0; i < supportedSystemType.size(); i++)
    {
      _systemTypeItems.add(supportedSystemType.get(i));
    }
    _currentSystemType = XrayTester.getSystemType().getName();
    if (_currentSystemType.equals(SystemTypeEnum.THROUGHPUT.getName()))
    {
      _currentSystemType = SystemTypeEnum.STANDARD.getName().toUpperCase();
    }

    _systemMagLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SELECT_LOW_MAG_ON_IMPORT_KEY"));

    _systemMagItems = new java.util.ArrayList<Object>();
    String[] systemLowMag = SystemConfigEnum.getLowMagnificationNames();
    for (int i = 0; i < systemLowMag.length; i++)
    {
      _systemMagItems.add(systemLowMag[i]);
    }

    _currentSystemMagnification = Config.getSystemCurrentLowMagnification();

    _mainUI = frame;
    _instruction = StringLocalizer.keyToString("MM_GUI_IMPORT_CAD_MESSAGE_KEY");
    _buttonFont = FontUtil.getDialogButtonFont();
    _messageFont = FontUtil.getDialogMessageFont();

    if (_cadenceProjectNameToFilePathMap.isEmpty() == false)
    {
      _okButton.setEnabled(_cadenceProjectNameToFilePathMap.size() > 0);
    }
    else
    {
      _okButton.setEnabled(true);
    }

    if (_gencadProjectNameToFilePathMap.isEmpty() == false)
    {
      _okButton.setEnabled(_gencadProjectNameToFilePathMap.size() > 0);
    }
    else
    {
      _okButton.setEnabled(true);
    }

    if (_fabmasterProjectNameToFilePathMap.isEmpty() == false)
    {
      _okButton.setEnabled(_fabmasterProjectNameToFilePathMap.size() > 0);
    }
    else
    {
      _okButton.setEnabled(true);
    }

    if (_odbProjectNameToFilePathMap.isEmpty() == false)
    {
      _okButton.setEnabled(_odbProjectNameToFilePathMap.size() > 0);
    }
    else
    {
      _okButton.setEnabled(true);
    }

    if (_cadenceProjectNameToFilePathMap.isEmpty() == false)
    {
      java.util.List<String> projectNames = new ArrayList<String>();
      projectNames.addAll(_cadenceProjectNameToFilePathMap.keySet());
      Collections.sort(projectNames, new CaseInsensitiveStringComparator());
      for (int i = 0; i < projectNames.size(); i++)
      {
        _cadenceChoiceListModel.addElement(projectNames.get(i));
      }
    }

    if (_gencadProjectNameToFilePathMap.isEmpty() == false)
    {
      java.util.List<String> projectNames = new ArrayList<String>();
      projectNames.addAll(_gencadProjectNameToFilePathMap.keySet());
      Collections.sort(projectNames, new CaseInsensitiveStringComparator());
      for (int i = 0; i < projectNames.size(); i++)
      {
        _gencadChoiceLlistModel.addElement(projectNames.get(i));
      }
    }

    if (_fabmasterProjectNameToFilePathMap.isEmpty() == false)
    {
      java.util.List<String> projectNames = new ArrayList<String>();
      projectNames.addAll(_fabmasterProjectNameToFilePathMap.keySet());
      Collections.sort(projectNames, new CaseInsensitiveStringComparator());
      for (int i = 0; i < projectNames.size(); i++)
      {
        _fabmasterChoiceListModel.addElement(projectNames.get(i));
      }
    }

    if (_odbProjectNameToFilePathMap.isEmpty() == false)
    {
      java.util.List<String> projectNames = new ArrayList<String>();
      projectNames.addAll(_odbProjectNameToFilePathMap.keySet());
      Collections.sort(projectNames, new CaseInsensitiveStringComparator());
      for (int i = 0; i < projectNames.size(); i++)
      {
        _odbChoiceListModel.addElement(projectNames.get(i));
      }
    }

    _cadenceChoiceList.setSelectedIndex(0);
    _gencadChoiceList.setSelectedIndex(0);
    _fabmasterChoiceList.setSelectedIndex(0);
    _odbChoiceList.setSelectedIndex(0);

    createDialog();
    pack();
  }
}
