package com.axi.v810.gui;

/**
 * Encapsulates all events of the TestDev gui that an observer of a GuiUpdateObservable
 * might care about.
 * @author George A. David
 */
public class ScreenChangeEventEnum extends GuiEventEnum
{
  private static int _index = -1;

  public static ScreenChangeEventEnum TEST_DEV = new ScreenChangeEventEnum(++_index);
  public static ScreenChangeEventEnum PACKAGE_LIBRARY = new ScreenChangeEventEnum(++_index);

  /**
   * @param id the unique id for any member of this class
   * @author George A. David
   */
  protected ScreenChangeEventEnum(int id)
  {
    super(id);
  }
}
