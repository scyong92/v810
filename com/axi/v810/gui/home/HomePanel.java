package com.axi.v810.gui.home;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;
import java.util.logging.*;

/**
 *
 * <p>Title: HomePanel</p>
 *
 * <p>Description: This is the default and only task for the Home
 * Environment for Genesis system.
 * Its purpose is to display useful information about the system to
 * the user. </p>
 * <p>The four main areas are:<br>
 * 1. System status<br>
 * 2. Available projects<br>
 * 3. Selected project status<br>
 * 4. Selected project graphics
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */
public class HomePanel extends AbstractTaskPanel implements Observer
{
  private Project _project = null;
  private XrayTester _xrayTester;
  private AbstractXraySource _xraySource;
  private PanelHandler _panelHandler;
  private LoginManager _loginManager = LoginManager.getInstance();
  private BarcodeReaderManager _barCodeManager = BarcodeReaderManager.getInstance();

  private ProjectObservable _projectObservable = null;
  private DefectPackagerObservable _defectPackagerObservable = null;

  private JPanel _systemSummaryPanel = null;
//  private JPanel _systemSummaryNorthPanel = null;
  private Box _systemSummaryBox = null;
  private JPanel _systemTypePanel = null;
  private JPanel _systemStatusPanel = null;
  private JLabel _hardwareSimWarningLabel = null;
  private JLabel _developerDebugWarningLabel = null;
  private JLabel _developerPerformanceWarningLabel = null;
  private JPanel _softwareRevisionPanel = null;
  private JPanel _systemSummaryCenterPanel = null;
  private JPanel _systemSetupPanel = null;
  private JPanel _systemInformationPanel = null;
  private JLabel[] _systemSummaryItemLabels = null;
  private JLabel[] _systemSummaryValueLabels = null;
  private java.util.List<String> _systemSummaryItems = null;

  private final int _SYSTEM_TYPE            = 0;
  private final int _SOFTWARE_REV           = 1;
  private final int _DEFECT_PACKAGER_STATUS = 2;
  private final int _SYSTEM_STATUS          = 3;
  private final int _XRAY_STATUS            = 4;
  private final int _BAR_CODE_MODE          = 5;
  private final int _HANDLER_MODE           = 6;
  private final int _HANDLER_STATUS         = 7;
  private final int _SYSTEM_ID              = 8;
  private final int _USER_ID                = 9;
  private final int _CURRENT_PROJECT        = 10;  private final int _HIGH_MAG_STATUS        = 11;
  private final int _LOW_MAGNIFICATION      = 12; //Ngie Xing, added to show magnification for S2EX
  private final int _HIGH_MAGNIFICATION     = 13; 

  private JPanel _databaseSummaryPanel = null;
  private JPanel _databaseSummaryInnerPanel = null;
  private JLabel[] _databaseSummaryItemLabels = null;
  private JLabel[] _databaseSummaryValueLabels = null;
  private java.util.List<String> _databaseSummaryItems = null;

  private final int _LOCATION                 = 0;
  private final int _NUMBER_OF_PROJECTS       = 1;
  private final int _LAST_UPDATE              = 2;

  private JSplitPane _projectsSplitPane = null;
  private JPanel _projectsTextPanel = null;
  private JPanel _projectsGraphicsPanel = null;
  private final double _projectsTextResizeWeight = 0.67;

  private JPanel _selectProjectPanel = null;
  private JPanel _selectProjectButtonPanel = null;
  private JPanel _selectedProjectButtonPanel = null;
  private JButton _selectedProjectOpenButton = null;
  private JButton _showProjectNotesButton = null;
  private JPanel _projectDatabaseButtonPanel = null;
  private JButton _projectDatabaseAddButton = null;
  private JButton _showDatabaseCommentsButton = null;

  private JPanel _selectWithSerialNumberPanel = null;
  private JLabel _selectWithSerialNumberLabel = null;
  private JTextField _selectWithSerialNumberTextField = null;
  private JButton _selectWithSerialNumberSearchButton = null;

  private JMenu _viewMenu = null;
  private JMenuItem _refreshPanelDataMenuItem = null;

  private JPopupMenu _localProjectsPopupMenu = null;
  private JMenuItem _openSelectedProjectPopupMenuItem = null;
  private JMenuItem _deleteSelectedProjectPopupMenuItem = null;
  private JMenuItem _addDatabaseProjectPopupMenuItem = null;
  private JMenuItem _showProjectNotesPopupMenuItem1 = null;
  private JMenuItem _showDatabaseCommentsPopupMenuItem1 = null;

  private JPopupMenu _projectDatabasePopupMenu = null;
  private JMenuItem _pullDatabaseProjectPopupMenuItem = null;
  private JMenuItem _deleteDatabaseProjectPopupMenuItem = null;
  private JMenuItem _showProjectNotesPopupMenuItem2 = null;
  private JMenuItem _showDatabaseCommentsPopupMenuItem2 = null;
  
  //Show history log folder - added by hsia fen 3 April 2013
  private JMenuItem _showHistoryLogPopupMenuItem1 = null;
  private String projName;
  private UserTypeEnum _currentUserType = null;

  private JTabbedPane _availableProjectsTabbedPane = null;
  private AvailableProjectsTableModel _localProjectsTableModel;
  private AvailableProjectsTable _localProjectsTable;
  private JScrollPane _localProjectsScrollPane = null;
  private ProjectDatabaseTableModel _projectDatabaseTableModel;
  private ProjectDatabaseTable _projectDatabaseTable;
  private JScrollPane _projectDatabaseScrollPane = null;

  private JPanel _lowerSummaryPanel = null;
  private JPanel _projectSummaryPanel = null;
  private JPanel _projectSummaryLeftPanel = null;
  private JPanel _projectSummaryRightPanel = null;
  private JLabel[] _projectSummaryItemLabels = null;
  private JLabel[] _projectSummaryValueLabels = null;
  private java.util.List<String> _projectSummaryItems = null;
  // two panels populated by row
  private final int _NAME                     = 0;
  private final int _CUSTOMER                 = 1;
  private final int _PROGRAMMER               = 2;
  private final int _VERSION                  = 3;
  private final int _MOD_DATE                 = 4;
  private final int _PROJECT_SYSTEM_TYPE      = 5;
//  private final int _TEST_TIME                = 5;
//  private final int            _BOARD_TYPES   = 6;
  private final int            _BOARDS        = 6;
  private final int            _COMPONENTS    = 7;
  private final int            _JOINTS        = 8;
  private final int            _TESTED_JOINTS = 9;
  private final int            _UNTESTABLE_JOINTS = 10;
  private final int            _COVERAGE      = 11;

  private ImageDisplayPanel _imageDisplayPanel;
  private JLabel _westUpArrowLabel = new JLabel(Image5DX.getImageIcon(Image5DX.UP_ARROW));
  private JLabel _eastUpArrowLabel = new JLabel(Image5DX.getImageIcon(Image5DX.UP_ARROW));
  private Image _image;

  private boolean _online = false;
  private boolean _hdwSim = false;
  private boolean _devDebug = false;
  private boolean _devPerformance = false;
  private boolean _active = false;
  private boolean _useBoldFont = false;

  private AbstractProjectDataTableModel _selectedProjectDataTableModel = null;
  private JSortTable _selectedProjectDataTable = null;

  private String _selectedProjectName = null;

  private String _selectedLocalProjectName = null;
  private int _selectedLocalProjectIndex = -1;

  private String _databasePath = null;
  private String _selectedProjectDatabaseName = null;
  private int _selectedProjectDatabaseIndex = -1;

  private String _selectedProjectNotes = null;
  private String _selectedDatabaseComments = null;

  private String _loadedProjectName = null;

  private DateFormat _dateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);

  private JPanel _informationPanel = null;
  private JPanel _northPanel = null;
  
  // Ying-Huan.Chu
  private JPanel _filterPanel = null;
  private JLabel _filterLabel = null;
  private JTextField _filterTextField = null;
  
  //Siew Yeng - XCR1745
  private boolean _isSemiAutomatedMode = false;
  private JLabel _semiAutomatedModeWarningLabel = null;
  private JPanel _centerPanel = null;
  private JPanel _centerOuterPanel = null;
  
  //Khaw Chek Hau - Xfr2511: Error Message Prompted Multiple Times 
  protected ProjectSummaryReader _projectSummaryReader = null;
  
  private boolean _isCustomizeSettingOn = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_MAIN_UI);
  private Map<String,Boolean> _displayLabelInfo = new HashMap<>();
  /**
   * @author George Booth
   */
  public HomePanel(HomeEnvironment envPanel)
  {
    super(envPanel);
    Assert.expect(envPanel != null);

    _projectObservable = ProjectObservable.getInstance();
    _defectPackagerObservable = DefectPackagerObservable.getInstance();
    
    _xrayTester = XrayTester.getInstance();
    _xraySource = AbstractXraySource.getInstance();
    _panelHandler = PanelHandler.getInstance();

    _online = _xrayTester.isHardwareAvailable();

    _hdwSim = _xrayTester.isSimulationModeOn();
    _devDebug = Config.isDeveloperDebugModeOn();
    
    //Siew Yeng - XCR1745 - Semi-automated mode
    _isSemiAutomatedMode = TestExecution.getInstance().isSemiAutomatedModeEnabled();

    // always observer project changes
    _projectObservable.addObserver(this);
    _defectPackagerObservable.addObserver(this);

    // set flag to use bold font highlight only in English
    String lang = Config.getInstance().getStringValue(SoftwareConfigEnum.LANGUAGE);
    _useBoldFont = lang.equalsIgnoreCase("en");
    
    //Khaw Chek Hau - Xfr2511: Error Message Prompted Multiple Times 
    _projectSummaryReader = ProjectSummaryReader.getInstance();

    jbInit();
  }

  /**
   * Initialize the GUI
   * @throws Exception
   * @author George Booth
   */
  private void jbInit()
  {
    // get localizable labels
    _systemSummaryItems = new ArrayList<String>();
    _systemSummaryItems.add(_SYSTEM_TYPE, StringLocalizer.keyToString("HP_SYSTEM_TYPE_LABEL_KEY"));
    _systemSummaryItems.add(_SOFTWARE_REV, StringLocalizer.keyToString("HP_SOFTWARE_REV_LABEL_KEY"));
    _systemSummaryItems.add(_DEFECT_PACKAGER_STATUS, StringLocalizer.keyToString("HP_DEFECT_PACKAGER_STATUS_LABEL_KEY"));
    _systemSummaryItems.add(_SYSTEM_STATUS, StringLocalizer.keyToString("HP_SYSTEM_STATUS_LABEL_KEY"));
    _systemSummaryItems.add(_XRAY_STATUS, StringLocalizer.keyToString("HP_XRAY_STATUS_LABEL_KEY"));
    _systemSummaryItems.add(_BAR_CODE_MODE, StringLocalizer.keyToString("HP_BAR_CODE_MODE_LABEL_KEY"));
    _systemSummaryItems.add(_HANDLER_MODE, StringLocalizer.keyToString("HP_HANDLER_MODE_LABEL_KEY"));
    _systemSummaryItems.add(_HANDLER_STATUS, StringLocalizer.keyToString("HP_HANDLER_STATUS_LABEL_KEY"));
    _systemSummaryItems.add(_SYSTEM_ID, StringLocalizer.keyToString("HP_SYSTEM_ID_LABEL_KEY"));
    _systemSummaryItems.add(_USER_ID, StringLocalizer.keyToString("HP_USER_ID_LABEL_KEY"));
    _systemSummaryItems.add(_CURRENT_PROJECT, StringLocalizer.keyToString("HP_CURRENT_PROJECT_LABEL_KEY"));
    _systemSummaryItems.add(_HIGH_MAG_STATUS, StringLocalizer.keyToString("HP_CURRENT_PROJECT_HIGH_MAG_VERIFICATION_LABEL_KEY"));
    _systemSummaryItems.add(_LOW_MAGNIFICATION, StringLocalizer.keyToString("HP_CURRENT_LOW_MAGNIFICATION_LABEL_KEY"));
    _systemSummaryItems.add(_HIGH_MAGNIFICATION, StringLocalizer.keyToString("HP_CURRENT_HIGH_MAGNIFICATION_LABEL_KEY"));
    
    _projectSummaryItems = new ArrayList<String>();
    _projectSummaryItems.add(_NAME, StringLocalizer.keyToString("HP_NAME_LABEL_KEY"));
    _projectSummaryItems.add(_CUSTOMER, StringLocalizer.keyToString("HP_CUSTOMER_LABEL_KEY"));
    _projectSummaryItems.add(_PROGRAMMER, StringLocalizer.keyToString("HP_PROGRAMMER_LABEL_KEY"));
    _projectSummaryItems.add(_VERSION, StringLocalizer.keyToString("HP_VERSION_LABEL_KEY"));
    _projectSummaryItems.add(_MOD_DATE, StringLocalizer.keyToString("HP_MOD_DATE_LABEL_KEY"));
    _projectSummaryItems.add(_PROJECT_SYSTEM_TYPE, StringLocalizer.keyToString("HP_PROJECT_SYSTEM_TYPE_LABEL_KEY"));
//    _projectSummaryItems.add(_TEST_TIME, StringLocalizer.keyToString("HP_TEST_TIME_LABEL_KEY"));
//    _projectSummaryItems.add(_BOARD_TYPES, StringLocalizer.keyToString("HP_BOARD_TYPES_LABEL_KEY"));
    _projectSummaryItems.add(_BOARDS, StringLocalizer.keyToString("HP_BOARDS_LABEL_KEY"));
    _projectSummaryItems.add(_COMPONENTS, StringLocalizer.keyToString("HP_COMPONENTS_LABEL_KEY"));
    _projectSummaryItems.add(_JOINTS, StringLocalizer.keyToString("HP_JOINTS_LABEL_KEY"));
    _projectSummaryItems.add(_TESTED_JOINTS, StringLocalizer.keyToString("HP_TESTED_JOINTS_LABEL_KEY"));
    _projectSummaryItems.add(_UNTESTABLE_JOINTS, StringLocalizer.keyToString("HP_UNTESTABLE_JOINTS_LABEL_KEY"));
    _projectSummaryItems.add(_COVERAGE, StringLocalizer.keyToString("HP_COVERAGE_LABEL_KEY"));

    _databaseSummaryItems = new ArrayList<String>();
    _databaseSummaryItems.add(_LOCATION, StringLocalizer.keyToString("HP_LOCATION_LABEL_KEY"));
    _databaseSummaryItems.add(_NUMBER_OF_PROJECTS, StringLocalizer.keyToString("HP_NUM_PROJECTS_LABEL_KEY"));
    _databaseSummaryItems.add(_LAST_UPDATE, StringLocalizer.keyToString("HP_LAST_UPDATE_LABEL_KEY"));

    this.setLayout(_taskPanelLayout);

    Border homePanelBorder = BorderFactory.createCompoundBorder(
       BorderFactory.createBevelBorder(
         BevelBorder.LOWERED, Color.white,new Color(182, 182, 182),
         new Color(62, 62, 62),new Color(89, 89, 89)),
       BorderFactory.createEmptyBorder(5, 5, 5, 5));

    // Menus

    _viewMenu = new JMenu();
    _viewMenu.setText(StringLocalizer.keyToString("GUI_VIEW_MENU_KEY"));

    _refreshPanelDataMenuItem = new JMenuItem();
    _refreshPanelDataMenuItem.setText(StringLocalizer.keyToString("GUI_REFRESH_MENU_KEY"));
    _refreshPanelDataMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        // set sort to default
        _localProjectsTable.resetSortHeader();
        _localProjectsTableModel.clear();
        _projectDatabaseTable.resetSortHeader();
        _projectDatabaseTableModel.clear();
        updatePanelData();
      }
    });

    
    // System Summary panel
    setupSystemSummaryPanel();

    _systemSummaryPanel.setBorder(homePanelBorder);
    
    // Projects panel
    // a split pane will be created to hold the project
    // text items on the left and project graphics on the right.
    setupProjectsPanel();
    _projectDatabaseTable.resetSortHeader();
    _projectsSplitPane.setBorder(homePanelBorder);

    _northPanel = new JPanel();
    _northPanel.setLayout(new BorderLayout());
    _northPanel.add(_systemSummaryPanel, BorderLayout.NORTH);
//    _northPanel.add(_selectProjectPanel, BorderLayout.CENTER);
    
    // Ying-Huan.Chu
    _filterLabel = new JLabel();
    _filterLabel.setText(StringLocalizer.keyToString("HP_SEARCH_RECIPE_KEY"));
    _filterLabel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
    _filterTextField = new JTextField();
    _filterTextField.setSize(200, 50);
    _filterTextField.getDocument().addDocumentListener(new DocumentListener()
    {
      public void changedUpdate(DocumentEvent e)
      {
        update();
      }
      public void removeUpdate(DocumentEvent e)
      {
        update();
      }
      public void insertUpdate(DocumentEvent e)
      {
        update();
      }
      public void update() 
      {
        _localProjectsTableModel.clear();
        _localProjectsTableModel.populateFilteredTableData(_filterTextField.getText());
        _projectDatabaseTableModel.clear();
        _projectDatabaseTableModel.populateFilteredTableData(_filterTextField.getText());
      }
    });
    
    _filterPanel = new JPanel(new GridLayout(0,5,1,1));
    _filterPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
    _filterPanel.add(_filterTextField);
    _filterPanel.add(_filterLabel);
    
    JPanel projectPanel = new JPanel();
    projectPanel.setLayout(new BorderLayout());
    projectPanel.add(_filterPanel, BorderLayout.NORTH);
    projectPanel.add(_projectsSplitPane, BorderLayout.CENTER);
    
    this.add(_northPanel, BorderLayout.NORTH);
    this.add(projectPanel, BorderLayout.CENTER);
    this.add(_informationPanel, BorderLayout.WEST);
  }

  /**
   * Handle tab change events for the availableProjects tabbed pane
   * @author George Booth
   */
  private void availableProjectsTabbedPane_stateChanged()
  {
    if (_active == false)
    {
      return;
    }

    int currentTabIndex = _availableProjectsTabbedPane.getSelectedIndex();
    if (currentTabIndex == 0)
    {
      // update Open button label
      _selectedProjectOpenButton.setText(
        StringLocalizer.keyToString("HP_OPEN_PROJECT_BUTTON_KEY"));
      _selectedProjectOpenButton.setToolTipText(
        StringLocalizer.keyToString("HP_OPEN_PROJECT_BUTTON_TOOLTIP_KEY"));

      // update Add button label
      _projectDatabaseAddButton.setText(
        StringLocalizer.keyToString("HP_ADD_TO_DATABASE_BUTTON_KEY"));
      _projectDatabaseAddButton.setToolTipText(
        StringLocalizer.keyToString("HP_ADD_TO_DATABASE_BUTTON_TOOLTIP_KEY"));

      // local projects
      _selectedProjectDataTableModel = _localProjectsTableModel;
      _selectedProjectDataTable = _localProjectsTable;
      enableSerialNumberSearch();
      // select the last local project selection for the summary
      // a table won't fire a selection event if the same row is
      // selected again; force the issue
      if (_selectedLocalProjectIndex > -1)
      {
        setLocalProjectsTableSelection(_selectedLocalProjectIndex);
      }
      else if (_loadedProjectName != null && _selectedProjectDataTableModel.getProjectIndex(_loadedProjectName) != -1) // Ying-Huan.Chu [3rd May 2013]
      {
        // match the loaded project by default
        _selectedLocalProjectIndex = _selectedProjectDataTableModel.getProjectIndex(_loadedProjectName);
        _selectedProjectDataTable.setRowSelectionInterval(
            _selectedLocalProjectIndex, _selectedLocalProjectIndex);
        // selection updates project summary
      }
      else
      {
        // nothing selected or loaded; clear the summary
        clearLocalProjectsTableSelection();
      }
    }
    else if (currentTabIndex == 1)
    {
      // update Open button label
      _selectedProjectOpenButton.setText(
        StringLocalizer.keyToString("HP_PULL_PROJECT_BUTTON_KEY"));
      _selectedProjectOpenButton.setToolTipText(
        StringLocalizer.keyToString("HP_PULL_PROJECT_BUTTON_TOOLTIP_KEY"));

      // update Add button label
      _projectDatabaseAddButton.setText(
        StringLocalizer.keyToString("HP_DELETE_FROM_DATABASE_BUTTON_KEY"));
      _projectDatabaseAddButton.setToolTipText(
        StringLocalizer.keyToString("HP_DELETE_FROM_DATABASE_BUTTON_TOOLTIP_KEY"));

      // project database
      _selectedProjectDataTableModel = _projectDatabaseTableModel;
      _selectedProjectDataTable = _projectDatabaseTable;
      enableSerialNumberSearch();
      // select the last project database selection for the summary
      if (_selectedProjectDatabaseIndex > -1)
      {
        setProjectDatabaseTableSelection(_selectedProjectDatabaseIndex);
      }
      else
      {
        // nothing selected; clear the summary
        clearProjectDatabaseTableSelection();
      }
      // if the project database directory is not valid, warn the user
      if (_projectDatabaseTableModel.isDatabaseValid() == false)
      {
        MessageDialog.showErrorDialog(
            this,
            StringLocalizer.keyToString(new LocalizedString("HP_INVAID_DATABASE_MESSAGE_KEY",
                new Object[] {_databasePath})),
            StringLocalizer.keyToString("HP_INVAID_DATABASE_MESSAGE_TITLE_KEY"),
            true);
      }
    }
    else
    {
      // this should not happen so assert
      Assert.expect(false);
    }

    // make sure data is current
    updatePanelData();
  }

  /**
   * Creates a list selection listener for the available projects table
   * @author George Booth
   */
  public void setupLocalProjectsTableListSelection()
  {
    // add the selection listener for the available projects table
    ListSelectionModel localProjectsTableLSM = _localProjectsTable.getSelectionModel();
    localProjectsTableLSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if (lsm.getValueIsAdjusting())
        {
          // do nothing
        }
        else if (lsm.isSelectionEmpty())
        {
          clearLocalProjectsTableSelection();
        }
        else
        {
          int index = lsm.getAnchorSelectionIndex();
          setLocalProjectsTableSelection(index);
        }
      }
    });
  }

  /**
   * Called when local projects table has no selection
   * @author George Booth
   */
  void clearLocalProjectsTableSelection()
  {
    _selectedLocalProjectName = null;
    _selectedLocalProjectIndex = -1;
    // clear defects details table
    updateProjectSummary(-1);
    // clear graphics
    _imageDisplayPanel.clearImage();
    // disable load button
    _selectedProjectOpenButton.setEnabled(false);
    _showProjectNotesButton.setEnabled(false);
    _projectDatabaseAddButton.setEnabled(false);
    _showDatabaseCommentsButton.setEnabled(false);
  }

  /**
   * Called when local projects table has a selection
   * @author George Booth
   */
  void setLocalProjectsTableSelection(int row)
  {
    _selectedLocalProjectIndex = row;
    updateProjectSummary(row);
    _selectedLocalProjectName = _selectedProjectName;
    _selectedProjectOpenButton.setEnabled(true);
    _showProjectNotesButton.setEnabled(true);
    _projectDatabaseAddButton.setEnabled(_projectDatabaseTableModel.isDatabaseValid());
    // there are no database comments for local projects
    _showDatabaseCommentsButton.setEnabled(false);

    // get values from table data
    AvailableProjectData data = _localProjectsTableModel.getSummaryData(row);
    if (_active)
      showProjectGraphics(data.getProjectName());
  }

  /**
   * Creates a list selection listener for the available projects table
   * @author George Booth
   */
  public void setupProjectDatabaseTableListSelection()
  {
    // add the selection listener for the project database table
    ListSelectionModel projectDatabaseTableLSM = _projectDatabaseTable.getSelectionModel();
    projectDatabaseTableLSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if (lsm.getValueIsAdjusting())
        {
          // do nothing
        }
        else if (lsm.isSelectionEmpty())
        {
          clearProjectDatabaseTableSelection();
        }
        else
        {
          int index = lsm.getAnchorSelectionIndex();
          setProjectDatabaseTableSelection(index);
        }
      }
    });
  }

  /**
   * Called when project database table has no selection
   * @author George Booth
   */
  void clearProjectDatabaseTableSelection()
  {
    _selectedProjectDatabaseIndex = -1;
    _selectedProjectDatabaseName = null;
    // clear defects details table
    updateProjectSummary(-1);
    // clear graphics
    _imageDisplayPanel.clearImage();

    // disable load button
    _selectedProjectOpenButton.setEnabled(false);
    _showProjectNotesButton.setEnabled(false);
    _projectDatabaseAddButton.setEnabled(false);
    _showDatabaseCommentsButton.setEnabled(false);
  }

  /**
   * Called when project database table has a selection
   * @author George Booth
   */
  void setProjectDatabaseTableSelection(int row)
  {
    updateProjectSummary(row);
    _selectedProjectDatabaseIndex = row;
    _selectedProjectDatabaseName = _selectedProjectName;
    _selectedProjectOpenButton.setEnabled(true);
    _showProjectNotesButton.setEnabled(true);
    _projectDatabaseAddButton.setEnabled(true);
    _showDatabaseCommentsButton.setEnabled(true);

    // get values from table data
    AvailableProjectData data = _projectDatabaseTableModel.getSummaryData(row);
    clearProjectGraphics();
  }

  /**
   * Displays the project "photo.jpg", "panel.jpg" or "no panel" image
   * @author George Booth
   */
  void showProjectGraphics(String projectName)
  {
    Assert.expect(projectName != null);
    _imageDisplayPanel.setRotateImage(false);
    PanelHandlerPanelFlowDirectionEnum flowDirection = _panelHandler.getPanelFlowDirection();
    boolean flipPanelImage = flowDirection == PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT;
    if (flipPanelImage == false)
    {
      _image = FileName.getPanelImage(projectName, false);
    }
    else
    {
      if (FileName.hasPanelImage(projectName, true))
      {
        // rotate image 180 degrees if handler is right-to-left
        _imageDisplayPanel.setRotateImage(true);
      }
      else
      {
        // don't rotate image if we are going to display textual "no image"
        _imageDisplayPanel.setRotateImage(false);
      }
      _image = FileName.getPanelImage(projectName, true);
    }

    if (_image != null)
    {
      _imageDisplayPanel.setImage(_image);
    }
  }

  /**
   * Clears the project image area
   * @author George Booth
   */
  void clearProjectGraphics()
  {
    _imageDisplayPanel.clearImage();
  }

  /**
   * @author George Booth
   */
  void enableSerialNumberSearch()
  {
    // enabled if current table has entries
    boolean enabled = (_selectedProjectDataTable != null &&_selectedProjectDataTable.getRowCount() > 0);
    _selectWithSerialNumberLabel.setEnabled(enabled);
    _selectWithSerialNumberTextField.setEnabled(enabled);
    _selectWithSerialNumberSearchButton.setEnabled(enabled);
  }

  /**
   * Matches and selects a project based on serial number
   * @author George Booth
   */
  void selectWithSerialNumberGo()
  {
    String sn = _selectWithSerialNumberTextField.getText();
    if (sn.equals(""))
    {
      // need data
      MessageDialog.showErrorDialog(
          this,
          StringLocalizer.keyToString("HP_NO_SERIAL_NUMBER_MESSAGE_KEY"),
          StringLocalizer.keyToString("HP_NO_SERIAL_NUMBER_MESSAGE_TITLE_KEY"),
          true);
      _selectedProjectDataTable.clearSelection();
    }
    else
    {
      SerialNumberToProjectName numToName = new SerialNumberToProjectName();
      try
      {
        String projName = numToName.getProjectNameFromSerialNumber(sn);
        if (projName.equals("") || _selectedProjectDataTableModel.getProjectIndex(projName) < 0)
        {
          // no match
          MessageDialog.showErrorDialog(
              this,
              StringLocalizer.keyToString("HP_SERIAL_NUMBER_NOT_MATCHED_MESSAGE_KEY"),
              StringLocalizer.keyToString("HP_SERIAL_NUMBER_NOT_MATCHED_MESSAGE_TITLE_KEY"),
              true);
          _selectedProjectDataTable.clearSelection();
        }
        else
        {
          int row = _selectedProjectDataTableModel.getRowForProject(projName);
          if (row > -1)
          {
            _selectedProjectDataTable.setRowSelectionInterval(row, row);
          }
        }
      }
      catch (DatastoreException de)
      {
        MessageDialog.reportIOError(_mainUI, de.getLocalizedMessage());
      }
    }
  }

  /**
   * Set the current project closed and updates status
   * @author George Booth
   */
  public void closeProject()
  {
    _project = null;
    _loadedProjectName = null;
    updateSystemSummary();
  }

  /**
   * @author George Booth
   */
  void openOrPullButtonClicked()
  {
    if (_availableProjectsTabbedPane.getSelectedIndex() == 0)
    {
      openSelectedProject();
    }
    else
    {
      pullSelectedProjectFromDatabase();
    }
  }

  /**
   * Opens the project selected in the available projects table
   * @author George Booth
   */
  void openSelectedProject()
  {
    if (_selectedLocalProjectName != null)
    {
      _mainUI.getMainMenuBar().loadOrImportProject(true, _selectedLocalProjectName);
    }
  }

  /**
   * Pulls the project files from the project database
   * @author George Booth
   */
  void pullSelectedProjectFromDatabase()
  {
    if (_selectedProjectDatabaseName != null)
    {
      // Check if user is pulling the last version in table and a newer version exists in the database then
      // prompt is they want the latest version.  This will update the table before prompting so they can see
      // the new version.
      // This can occur if the database tab was selected when a new version was created on a different computer.
      checkForLatestVersion();

      String projectVersion = _projectDatabaseTableModel.getSummaryData(_selectedProjectDatabaseIndex).getVersion();
      if (_selectedProjectDatabaseName.equals(_loadedProjectName) && _mainUI.isProjectModified())
      {
        if (JOptionPane.showConfirmDialog(
            this,
            StringLocalizer.keyToString(new LocalizedString("HP_CONFIRM_PROJECT_DATABASE_PULL_OVER_CHANGES_KEY",
              new Object[] {_selectedProjectDatabaseName, projectVersion})),
            StringLocalizer.keyToString("HP_CONFIRM_PULL_TITLE_KEY"),
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION)
          return;
      }
      else
      {
        if (JOptionPane.showConfirmDialog(
            this,
            StringLocalizer.keyToString(new LocalizedString("HP_CONFIRM_PROJECT_DATABASE_PULL_KEY",
            new Object[]
            {_selectedProjectDatabaseName, projectVersion})),
            StringLocalizer.keyToString("HP_CONFIRM_PULL_TITLE_KEY"),
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION)
          return;
      }
      // if the currently loaded project is the one being pulled, close it to prevent weirdness
      final String projectToReload = _selectedProjectDatabaseName;
      boolean reloadProjectAfterPull = false;
      if (_selectedProjectDatabaseName.equals(_loadedProjectName))
      {
        // don't prompt for changes, the user has already confirmed
        _mainUI.getMainMenuBar().projectClose(true);
        reloadProjectAfterPull = true;
      }
      // Change the cursor temporarily and wait for the pull to finish.
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      setEnabled(false); // do not allow user's input during pull
      try
      {
        int versionNumber = _projectDatabaseTableModel.getSummaryData(_selectedProjectDatabaseIndex).getDatabaseVersion();
        ProjectDatabase.getProject(_selectedProjectDatabaseName, versionNumber);
      }
      catch(DatastoreException dse)
      {
        MessageDialog.showErrorDialog(
              _mainUI,
              StringUtil.format(dse.getMessage(), 50),
              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
              true);
      }
      finally
      {
        final boolean reloadProject = reloadProjectAfterPull;
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            setEnabled(true);
            updatePanelData();
            if (reloadProject)
            {
              _mainUI.getMainMenuBar().loadOrImportProject(true, projectToReload, projectToReload);
            }
          }
        });
      }
    }
  }

  /**
   * @author George Booth
   */
  void checkForLatestVersion()
  {
    // if user has selected the last version of the project in the table to pull and a later
    // version exists in the database, prompt if they want to pull the latest version
    String projectName = _projectDatabaseTableModel.getProjectNameAt(_selectedProjectDatabaseIndex);
    int selectedVersion = _projectDatabaseTableModel.getProjectVersionAt(_selectedProjectDatabaseIndex);
    int lastTableVersion = _projectDatabaseTableModel.getLastProjectVersion(projectName);
    try
    {
      int lastVersion = ProjectDatabase.getLatestDatabaseVersion(projectName);
      if (selectedVersion == lastTableVersion && lastVersion > lastTableVersion)
      {
        // first update the table
        updatePanelData();
        // prompt for action
        int answer = JOptionPane.showConfirmDialog(
            _mainUI,
            StringLocalizer.keyToString("MMGUI_AUTO_PULL_CONFIRM_KEY"),
            StringLocalizer.keyToString("HP_CONFIRM_PULL_TITLE_KEY"),
            JOptionPane.YES_NO_CANCEL_OPTION);
        if (answer == JOptionPane.YES_OPTION)
        {
          // select the new version for pulling
          int row = _projectDatabaseTableModel.getProjectIndex(projectName, lastVersion);
          _projectDatabaseTable.setRowSelectionInterval(row, row);
          return;
        }
      }
    }
    catch (DatastoreException dse)
    {
      MessageDialog.showErrorDialog(
            _mainUI,
            StringUtil.format(dse.getMessage(), 50),
            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
            true);
    }
  }

  /**
   * @author George Booth
   */
  void addOrDeleteButtonClicked()
  {
    if (_availableProjectsTabbedPane.getSelectedIndex() == 0)
    {
      addSelectedProjectToDatabase();
    }
    else
    {
      deleteSelectedProjectFromDatabase();
    }
  }

  /**
   * Adds the selected project to the project database
   * @author George Booth
   */
  void addSelectedProjectToDatabase()
  {
    if (_selectedLocalProjectName != null)
    {
      // if the project is loaded into memory AND has been modified, should ask user if they want to save
      if ((_project != null) && _selectedLocalProjectName.equals(_project.getName()))
      {
        if (_project.hasBeenModified())
        {
          MessageDialog.showErrorDialog(_mainUI,
                                        StringLocalizer.keyToString("HP_DATABASE_CANNOT_ADD_KEY"),
                                        StringLocalizer.keyToString("HP_DATABASE_CANNOT_ADD_TITLE_KEY"),
                                        true);
          return;
        }
      }

      // create a default database comment
      long currentTime = System.currentTimeMillis();
      String databaseComment = StringLocalizer.keyToString("HP_DEFAULT_DATABASE_COMMENT_KEY") +
                               " " + _dateFormat.format(currentTime);

      StringInputDialog databaseCommentDialog = new StringInputDialog(
          _mainUI,
          StringLocalizer.keyToString("HP_DATABASE_COMMENT_DIALOG_TITLE_KEY"),
          StringLocalizer.keyToString("HP_DATABASE_COMMENT_DIALOG_PROMPT_KEY"));
      int value = databaseCommentDialog.showDialog(databaseComment);
      if (value == JOptionPane.OK_OPTION)
      {
        // Change the cursor temporarily and wait for the delete to finish.
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        setEnabled(false); // do not allow user's input during delete

        final String userDatabaseComment = databaseCommentDialog.getStringInput();
        final BusyCancelDialog busyDialog = new BusyCancelDialog(_mainUI,
                                                               StringLocalizer.keyToString("HP_DATABASE_ADD_IN_PROGRESS_KEY"),
                                                               StringLocalizer.keyToString("HP_DATABASE_ADD_TITLE_KEY"),
                                                               true);
        SwingUtils.centerOnComponent(busyDialog, _mainUI);
        SwingWorkerThread.getInstance().invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              ProjectDatabase.addProject(_selectedLocalProjectName, userDatabaseComment);
            }
            catch (final DatastoreException dse)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  MessageDialog.showErrorDialog(
                      _mainUI,
                      StringUtil.format(dse.getMessage(), 50),
                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                      true);
                }
              });

            }
            finally
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                  setEnabled(true);
                  updatePanelData();
                  while (busyDialog.isVisible() == false)
                  {
                    try
                    {
                      Thread.sleep(200);
                    }
                    catch (Exception ex)
                    {
                      // do nothing
                    }
                  }
                  busyDialog.dispose();
                }
              });
            }
          }
        });
        busyDialog.setVisible(true);
      }
      else if (value == JOptionPane.CANCEL_OPTION) // this is the CANCEL option
      {
        return;
      }
      else
      {
        Assert.expect(false);
      }
    }
  }

  /**
   * Deletes the project files from the project database
   * @author George Booth
   */
  void deleteSelectedProjectFromDatabase()
  {
    if (_selectedProjectDatabaseName != null)
    {
      String projectVersion = _projectDatabaseTableModel.getSummaryData(_selectedProjectDatabaseIndex).getVersion();
      if (JOptionPane.showConfirmDialog(
          this,
          StringLocalizer.keyToString(new LocalizedString("HP_CONFIRM_PROJECT_DATABASE_DELETE_KEY",
          new Object[]
          {_selectedProjectDatabaseName, projectVersion})),
          StringLocalizer.keyToString("PDD_CONFIRM_DELETE_TITLE_KEY"),
          JOptionPane.YES_NO_OPTION,
          JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION)
        return;
      // Change the cursor temporarily and wait for the delete to finish.
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      setEnabled(false); // do not allow user's input during delete
      try
      {
        int versionNumber = _projectDatabaseTableModel.getSummaryData(_selectedProjectDatabaseIndex).getDatabaseVersion();
        int projectSummaryVersion = FileName.getProjectSummaryLatestFileVersion();
        String fileName = null;
        while (projectSummaryVersion > 0)
        {
          fileName = FileName.getProjectDatabaseSummaryFileFullPath(_selectedProjectDatabaseName, versionNumber, projectSummaryVersion);
          if (FileUtilAxi.exists(fileName))
            break;
           --projectSummaryVersion;
        }
        ProjectDatabase.deleteProject(_selectedProjectDatabaseName, versionNumber, projectSummaryVersion);
      }
      catch(DatastoreException dse)
      {
        MessageDialog.showErrorDialog(
              _mainUI,
              StringUtil.format(dse.getMessage(), 50),
              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
              true);
      }
      finally
      {
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            setEnabled(true);
            updatePanelData();
          }
        });
      }
    }
  }

  /**
   * Delete the project selected in the available projects table
   * @author George Booth
   */
  void deleteSelectedProject()
  {
    if (_selectedLocalProjectName != null)
    {
      if (JOptionPane.showConfirmDialog(
          this,
          StringLocalizer.keyToString(new LocalizedString("PDD_CONFIRM_PROJECT_DELETE_KEY",
            new Object[]{_selectedLocalProjectName})),
          StringLocalizer.keyToString("PDD_CONFIRM_DELETE_TITLE_KEY"),
          JOptionPane.YES_NO_OPTION,
          JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION)
        return;
      // Change the cursor temporarily and wait for the delete to finish.
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      setEnabled(false); // do not allow user's input during delete

      final BusyCancelDialog busyDialog = new BusyCancelDialog(MainMenuGui.getInstance(),
                                                               StringLocalizer.keyToString("HP_DELETE_IN_PROGRESS_KEY"),
                                                               StringLocalizer.keyToString("HP_DELETE_TITLE_KEY"),
                                                               true);

      SwingUtils.centerOnComponent(busyDialog, _mainUI);
      SwingWorkerThread.getInstance().invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            Project.delete(_selectedLocalProjectName);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                projectWasDeleted(_selectedLocalProjectName);
              }
            });
          }
          catch (final DatastoreException ie)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                MessageDialog.reportIOError(HomePanel.this, ie.getLocalizedMessage());
              }
            });
          }
          finally
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                setEnabled(true); // allow user to press a button
                while(busyDialog.isVisible() == false)
                {
                  try
                  {
                    Thread.sleep(200);
                  }
                  catch (Exception ex)
                  {
                    // do nothing
                  }
                }
                busyDialog.dispose();
              }
            });

          }
        }
      });
      busyDialog.setVisible(true);
    }
  }

  /**
   * Shows the notes for the project selected in the available projects table
   * @author George Booth
   */
  void showProjectNotes()
  {
    if (_selectedProjectNotes != null)
    {
      MessageDialog.showInformationDialog(
          this,
          _selectedProjectNotes,
          StringLocalizer.keyToString("HP_PROJECT_NOTES_TITLE_KEY"),
          true);
    }
  }
  
   /**
   * Shows list of history log file by right click
   * @author hsia-fen.tan
   */  
  private void showProjectHistory()
  {
      projName = _selectedLocalProjectName;
      java.util.List<String> Historyfile = null;
      String title = null;
      String message = null;
    _currentUserType=LoginManager.getInstance().getUserTypeFromLogIn();
    if (_currentUserType.equals(UserTypeEnum.ADMINISTRATOR))
    {
      Historyfile = FileName.getHistoryFiles(projName);
      if (Historyfile == null || Historyfile.size() == 0)
      {
        MessageDialog.showInformationDialog(
                this,
                "There are no History Log file in this recipe.",
                StringLocalizer.keyToString("HP_PROJECT_HISTORY_LOG_TITLE_KEY"),
                true);
        return;
      }
    }
    else
    {
      MessageDialog.showInformationDialog(
              this,
              "Only administrator can view the history log file!",
              StringLocalizer.keyToString("HP_PROJECT_HISTORY_LOG_TITLE_KEY"),
              true);
      return;
    }
 

      title = StringLocalizer.keyToString("HP_PROJECT_HISTORY_LOG_TITLE_KEY");
      message = StringLocalizer.keyToString("HP_GUI_VIEW_HISTORY_FILE_MESSAGE_KEY");
      Object[] possibleValues = Historyfile.toArray();

      SelectItemDialog importDialog = new SelectItemDialog(
              _mainUI,
              title,
              message,
              StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
              StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
              possibleValues,
              StringLocalizer.keyToString("HP_HISTORY_TABLE_HISTORY_FILE_NAME_COLUMN_KEY"));

      int returnValue = importDialog.showDialog();

      if (returnValue == JOptionPane.OK_OPTION)
      {
        final String fileToLoad = (String) importDialog.getSelectedValue();
        final String fullPath = Directory.getHistoryFileDir(projName) + File.separator + fileToLoad;
        try
        {
          showHistoryContent(fullPath);
        }
        catch (DatastoreException ex)
        {
          Logger.getLogger(HomePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
  
  private void showHistoryContent(String file) throws DatastoreException
  {
     String inputFileText = FileReaderUtilAxi.readFileContents(file);

     TextDialog HistoryLogContent = new TextDialog(_mainUI, inputFileText, StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"), StringLocalizer.keyToString("HP_PROJECT_HISTORY_LOG_TITLE_KEY"), 100, 30, true);
     SwingUtils.centerOnComponent(HistoryLogContent, _mainUI);
     HistoryLogContent.setVisible(true);
  }

  /**
   * Shows the database comments for the project selected in the available projects table
   * @author George Booth
   */
  void showDatabaseComments()
  {
    if (_selectedDatabaseComments != null)
    {
      MessageDialog.showInformationDialog(
          this,
          _selectedDatabaseComments,
          StringLocalizer.keyToString("HP_DATABASE_COMMENTS_TITLE_KEY"),
          true);
    }
  }

  /**
   * Updates the currently loaded project
   * @author George Booth
   */
  void updateCurrentProject() throws DatastoreException
  {
    _loadedProjectName = _project.getName();

    if (_active == false)
      return;

    int index = _localProjectsTableModel.getProjectIndex(_loadedProjectName);
    _selectedLocalProjectIndex = index;
    // Update the summary data for the loaded project so data is current
    AvailableProjectData data = new AvailableProjectData(_project);
    if (data.getSystemType().equals(SystemTypeEnum.THROUGHPUT.getName()))
    {
      data.setSystemType(SystemTypeEnum.STANDARD.getName().toUpperCase());
    }
    //Khaw Chek Hau - XCR2051 - Resolve assert issue when corrupted recipe is opened
    if (index == -1) 
    { 
      try
      {
        //Khaw Chek Hau - XCR2511: Error Message Prompted Multiple Times 
        ProjectSummary projectSummary = _projectSummaryReader.read(_loadedProjectName);
      }
      catch (DatastoreException de)
      {
        //Khaw Chek Hau - XCR2603: Corrupted recipe unable to load
        _project.setSummaryUpdateNeeded(true);
        throw de;
      }
    }

    _localProjectsTableModel.updateSummaryData(index, data);
  
    updateSystemSummary();
    if (_availableProjectsTabbedPane.getSelectedIndex() == 0)
    {
      _localProjectsTable.setRowSelectionInterval(index, index);
    }
  }

  /**
   * A project was deleted. Update project tables. If project
   * was currently loaded project, clear the project summary.
   * @author George Booth
   */
  public void projectWasDeleted(String projectName)
  {
    Assert.expect(projectName != null);

    try
    {
      if (projectName.equalsIgnoreCase(_loadedProjectName))
      {
        _loadedProjectName = null;
      }

      if (_active == false)
        return;

      if (_loadedProjectName != null)
      {
        int index = _localProjectsTableModel.getProjectIndex(_loadedProjectName);
        _selectedLocalProjectIndex = index;
        // Update the summary data for the loaded project so data is current
        AvailableProjectData data = new AvailableProjectData(_project);
        _localProjectsTableModel.updateSummaryData(index, data);

        updateSystemSummary();
        if (_availableProjectsTabbedPane.getSelectedIndex() == 0)
        {
          _localProjectsTable.setRowSelectionInterval(index, index);
        }
      }
      updatePanelData();
      updateStatus();
    }
    catch (DatastoreException ex)
    {
      //Khaw Chek Hau - Xfr2511: Error Message Prompted Multiple Times
      MainMenuGui mainUI = MainMenuGui.getInstance();
      MessageDialog.showErrorDialog(
        mainUI,
        StringUtil.format(ex.getMessage(), 50),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
      return;
    }
  }

  /**
   * update the System Summary data fields
   * @author George Booth
   */
  void updateSystemSummary()
  {
    if (!_active)
      return;
    // System ID
    String hostname = StringLocalizer.keyToString("HP_SYSTEM_ID_UNKNOWN_KEY");
    try
    {
      java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
      if (localMachine != null)
      {
        hostname = localMachine.getHostName();
      }
    }
    catch(java.net.UnknownHostException uhe)
    {
      hostname = StringLocalizer.keyToString("HP_SYSTEM_ID_UNKNOWN_KEY");
    }

    if (_online)
    {
      // System Type
      String systemType = _xrayTester.getMachineDescription();
//      _systemSummaryValueLabels[_SYSTEM_TYPE].setText(systemType + " (" + XrayTester.getSystemType().getName() + ")");
      _systemSummaryValueLabels[_SYSTEM_TYPE].setText(systemType);
      // System ID
      _systemSummaryValueLabels[_SYSTEM_ID].setText(hostname);
      // Software Revision
      _systemSummaryValueLabels[_SOFTWARE_REV].setText(Version.getVersion());
      
      checkDefectPackagerQueryStatus();
      
      // System Status
      String systemStatus = StringLocalizer.keyToString("HP_SYSTEM_STATUS_OFF_VALUE_KEY");
      Color systemStatusColor = Color.RED;
      try
      {
        if (_xrayTester.isStartupRequired() == false)
        {
          systemStatus = StringLocalizer.keyToString("HP_SYSTEM_STATUS_NORMAL_VALUE_KEY");
          systemStatusColor = Color.BLACK;
        }
      }
      catch (XrayTesterException xte) {
        // do nothing; an exception is never thrown
      }
      _systemSummaryValueLabels[_SYSTEM_STATUS].setText(systemStatus);
      _systemSummaryValueLabels[_SYSTEM_STATUS].setForeground(systemStatusColor);
      // X-ray Status
      String xrayStatus = StringLocalizer.keyToString("HP_XRAY_STATUS_OFF_VALUE_KEY");
      Color xrayStatusColor = Color.RED;
      try
      {
        if (_xraySource.isStartupRequired() == false)
        {
          xrayStatus = StringLocalizer.keyToString("HP_XRAY_STATUS_ON_VALUE_KEY");
          xrayStatusColor = Color.BLACK;
        }
      }
      catch (XrayTesterException xte)
      {
        // do nothing; an exception is never thrown
      }
//      _systemSummaryValueLabels[_XRAY_STATUS].setText(xrayStatus);
//      _systemSummaryValueLabels[_XRAY_STATUS].setForeground(xrayStatusColor);
      // User Account
      String userName = _loginManager.getUserNameFromLogIn();
      Color userNameColor = Color.BLACK;
      if (userName.length() == 0)
      {
        userName = StringLocalizer.keyToString("HP_USER_NOT_LOGGED_IN_VALUE_KEY");
        userNameColor = Color.RED;
      }
      _systemSummaryValueLabels[_USER_ID].setText(userName);
      _systemSummaryValueLabels[_USER_ID].setForeground(userNameColor);
      // Handler Mode
      String handlerMode = "";
      PanelHandlerPanelLoadingModeEnum loadingMode = _panelHandler.getPanelLoadingMode();
      if (loadingMode == PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH)
      {
        handlerMode = StringLocalizer.keyToString("HP_HANDLER_MODE_FLOW_THROUGH_VALUE_KEY");
      }
      else
      {
        handlerMode = StringLocalizer.keyToString("HP_HANDLER_MODE_PASS_BACK_VALUE_KEY");
      }
      PanelHandlerPanelFlowDirectionEnum flowDirection = _panelHandler.getPanelFlowDirection();
      if (flowDirection == PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT)
      {
        handlerMode = handlerMode + ", " +
                      StringLocalizer.keyToString("HP_HANDLER_FLOW_RIGHT_LEFT_VALUE_KEY");
      }
      else
      {
        handlerMode = handlerMode + ", " +
                      StringLocalizer.keyToString("HP_HANDLER_FLOW_LEFT_RIGHT_VALUE_KEY");
      }
      boolean smema = _panelHandler.isInLineModeEnabled();
      if (smema)
      {
        handlerMode = handlerMode + ", " +
                      StringLocalizer.keyToString("HP_HANDLER_MODE_SMEMA_VALUE_KEY");
      }
        _systemSummaryValueLabels[_HANDLER_MODE].setText(handlerMode);
      // Bar Code Reader
      String barCodeMode = StringLocalizer.keyToString("HP_BAR_CODE_MODE_MANUAL_VALUE_KEY");
      if (_barCodeManager.getBarcodeReaderAutomaticReaderEnabled())
      {
        barCodeMode = StringLocalizer.keyToString("HP_BAR_CODE_MODE_AUTOMATIC_VALUE_KEY");
      }
      _systemSummaryValueLabels[_BAR_CODE_MODE].setText(barCodeMode);
      // Current Project
      if (_loadedProjectName == null)
      {
        String notLoaded = StringLocalizer.keyToString("HP_CURRENT_PROJECT_NOT_LOADED_VALUE_KEY");
        _systemSummaryValueLabels[_CURRENT_PROJECT].setText(notLoaded);
        _systemSummaryValueLabels[_CURRENT_PROJECT].setForeground(Color.RED);
      }
      else
      {
        _systemSummaryValueLabels[_CURRENT_PROJECT].setText(_loadedProjectName);
        _systemSummaryValueLabels[_CURRENT_PROJECT].setForeground(Color.BLACK);
      }
      
      //Ngie Xing, add in value lable for current low and high magnifications
      if(XrayTester.isS2EXEnabled())
      {
        String currentLowMag = Config.getSystemCurrentLowMagnification();
        String currentHighMag = Config.getSystemCurrentHighMagnification();

        _systemSummaryValueLabels[_LOW_MAGNIFICATION].setText(currentLowMag);
        _systemSummaryValueLabels[_HIGH_MAGNIFICATION].setText(currentHighMag);
      }
      else
      {
        _systemSummaryValueLabels[_LOW_MAGNIFICATION].setText("");
        _systemSummaryValueLabels[_HIGH_MAGNIFICATION].setText("");
      }
      
      // Panel Status
      String panelStatus = StringLocalizer.keyToString("HP_HANDLER_STATUS_UNKNOWN_VALUE_KEY");
      Color panelStatusColor = Color.RED;

      // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
      try
      {
        if (_panelHandler.isPanelLoaded())
        {
          String loadedPanelName = _panelHandler.getLoadedPanelProjectName();
          if (loadedPanelName.equals(""))
          {
            panelStatus = StringLocalizer.keyToString("HP_HANDLER_STATUS_LOADED_UNKNOWN_VALUE_KEY");
            panelStatusColor = Color.RED;
          }
          else
          {
            panelStatus = StringLocalizer.keyToString("HP_HANDLER_STATUS_LOADED_VALUE_KEY") +
                          " (" + loadedPanelName + ")";
            panelStatusColor = Color.BLACK;
          }
        }
        else
        {
          panelStatus = StringLocalizer.keyToString("HP_HANDLER_STATUS_NOT_LOADED_VALUE_KEY");
          panelStatusColor = Color.RED;
        }
      }
      catch (XrayTesterException xte)
      {
        MessageDialog.showErrorDialog(
              _mainUI,
              StringUtil.format(xte.getMessage(), 50),
              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
              true);
      }
      _systemSummaryValueLabels[_HANDLER_STATUS].setText(panelStatus);
      _systemSummaryValueLabels[_HANDLER_STATUS].setForeground(panelStatusColor);
      
      String safetyLevelSensorScanStatus = StringLocalizer.keyToString("HP_CURRENT_PROJECT_HIGH_MAG_NOT_TESTED_KEY");
      if (_panelHandler.getPassedSafetyLevelSensorScan())
      {
        safetyLevelSensorScanStatus = StringLocalizer.keyToString("HP_CURRENT_PROJECT_HIGH_MAG_TESTED_AND_PASSED_KEY");;
      }
      _systemSummaryValueLabels[_HIGH_MAG_STATUS].setText(safetyLevelSensorScanStatus);
      _systemSummaryValueLabels[_HIGH_MAG_STATUS].setForeground(panelStatusColor);
    }
    else
    {
      // offline TDW
      // System Type
      String systemType ="";
      try
      {
        systemType = _xrayTester.getWorkstationDescription();
      }
      catch (XrayTesterException e)
      {
        _mainUI.handleLicenseGeneralException(e);
      }
      _systemSummaryValueLabels[_SYSTEM_TYPE].setText(systemType);
      // System ID
      _systemSummaryValueLabels[_SYSTEM_ID].setText(hostname);
      // Software Revision
      _systemSummaryValueLabels[_SOFTWARE_REV].setText(Version.getVersion());
      
      checkDefectPackagerQueryStatus();
      
      // User Account
      String userName = _loginManager.getUserNameFromLogIn();
      Color userNameColor = Color.BLACK;
      if (userName.length() == 0)
      {
        userName = StringLocalizer.keyToString("HP_USER_NOT_LOGGED_IN_VALUE_KEY");
        userNameColor = Color.RED;
      }
      _systemSummaryValueLabels[_USER_ID].setText(userName);
      _systemSummaryValueLabels[_USER_ID].setForeground(userNameColor);
      // Current Project
      if (_loadedProjectName == null)
      {
        String notLoaded = StringLocalizer.keyToString("HP_CURRENT_PROJECT_NOT_LOADED_VALUE_KEY");
        _systemSummaryValueLabels[_CURRENT_PROJECT].setText(notLoaded);
        _systemSummaryValueLabels[_CURRENT_PROJECT].setForeground(Color.RED);
      }
      else
      {
        _systemSummaryValueLabels[_CURRENT_PROJECT].setText(_loadedProjectName);
        _systemSummaryValueLabels[_CURRENT_PROJECT].setForeground(Color.BLACK);
      }
      
      //Ngie Xing, add in value lable for current low and high magnifications
      if(XrayTester.isS2EXEnabled())
      {
        String currentLowMag = Config.getSystemCurrentLowMagnification();
        String currentHighMag = Config.getSystemCurrentHighMagnification();

        _systemSummaryValueLabels[_LOW_MAGNIFICATION].setText(currentLowMag);
        _systemSummaryValueLabels[_HIGH_MAGNIFICATION].setText(currentHighMag);
      }
      else
      {       
        _systemSummaryValueLabels[_LOW_MAGNIFICATION].setText("");
        _systemSummaryValueLabels[_HIGH_MAGNIFICATION].setText("");
      }
    }
  }

  /**
   * update the Project Summary data fields
   * @author George Booth
   */
  void updateProjectSummary(int row)
  {
    if (row < 0)
    {
      // clear all fields
      for (int i = 0; i < _projectSummaryValueLabels.length; i++)
      {
        _projectSummaryValueLabels[i].setText(" ");
      }
      _selectedProjectName = null;
    }
    else
    {
      AvailableProjectData data = _selectedProjectDataTableModel.getSummaryData(row);
      _selectedProjectName = data.getProjectName();
      // Panel Name
      _projectSummaryValueLabels[_NAME].setText(data.getProjectName());
      // Customer
      _projectSummaryValueLabels[_CUSTOMER].setText(data.getCustomerName());
      // Programmer
      _projectSummaryValueLabels[_PROGRAMMER].setText(data.getProgrammerName());
      // Version
      _projectSummaryValueLabels[_VERSION].setText(data.getVersion());
      // Last Modification
      _projectSummaryValueLabels[_MOD_DATE].setText(data.getModificationTime());
      // Project System Type
      _projectSummaryValueLabels[_PROJECT_SYSTEM_TYPE].setText(data.getSystemType());
      // Estimated Test Time not used in version 1
      DecimalFormat oneDecimalPlace = new DecimalFormat("###.#");
      String formattedTestTime = oneDecimalPlace.format(data.getEstimatedTestTime());
//      _projectSummaryValueLabels[_TEST_TIME].setText(formattedTestTime +
//          " " + StringLocalizer.keyToString("HP_TEST_TIME_UNITS_KEY"));
//      _projectSummaryValueLabels[_TEST_TIME].setText(StringLocalizer.keyToString("HP_NO_ESTIMATED_TEST_TIME_KEY"));

      // Number of Board Types not used in version 1
//      _projectSummaryValueLabels[_BOARD_TYPES].setText(""+data.getNumberOfBoardTypes());
      // Number of Boards
      _projectSummaryValueLabels[_BOARDS].setText(""+data.getNumberOfBoards());
      // Number of Components
      _projectSummaryValueLabels[_COMPONENTS].setText(""+data.getNumberOfComponents());
      // Number of Joints
      _projectSummaryValueLabels[_JOINTS].setText(""+data.getNumberOfJoints());
      // Number of Joints Tested
      _projectSummaryValueLabels[_TESTED_JOINTS].setText(""+data.getNumberOfTestedJoints());
      // Number of untestable Joints, Khang-Wah, 2011-09-08
      _projectSummaryValueLabels[_UNTESTABLE_JOINTS].setText(""+data.getNumberOfUnTestableJoints());
      // Test Coverage
      DecimalFormat twoDecimalPlaces = new DecimalFormat("###.##");
      String formattedCoverage = twoDecimalPlaces.format(data.getTestCoverage());
      _projectSummaryValueLabels[_COVERAGE].setText(formattedCoverage + "%");

      // Project notes and database comments, in case the user asks for them
      _selectedProjectNotes = data.getProjectNotes();
      _selectedDatabaseComments = data.getDatabaseComment();
    }
  }

  /**
   * update the Project Summary data fields
   * @author George Booth
   */
  void updateDatabaseSummary()
  {
    // Database location
    _databaseSummaryValueLabels[_LOCATION].setText(_databasePath);
    // Number of projects
    _databaseSummaryValueLabels[_NUMBER_OF_PROJECTS].setText(""+_projectDatabaseTableModel.getNumberOfProjects());
    // Last update
    File databaseDirectory = new File(_databasePath);
    long lastModified = databaseDirectory.lastModified();
    _databaseSummaryValueLabels[_LAST_UPDATE].setText(_dateFormat.format(lastModified));
  }

  /**
   * @author George Booth
   */
  public synchronized void update(final Observable observable, final Object arg)
  {
    Assert.expect(observable != null);

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if(observable instanceof ProjectObservable)
        {
          handleProjectObservable(arg);
        }
        else if(observable instanceof DefectPackagerObservable)
        {
          checkDefectPackagerQueryStatus();
        }
        else if (observable instanceof ConfigObservable)
        {
          if (arg instanceof ConfigEnum)
          {
            ConfigEnum configEnum = (ConfigEnum)arg;
            if (configEnum instanceof UISettingCustomizationConfigEnum)
            {
              UISettingCustomizationConfigEnum customiseSettingConfigEnum = (UISettingCustomizationConfigEnum)configEnum;
              if (customiseSettingConfigEnum.equals(UISettingCustomizationConfigEnum.CUSTOMIZE_MAIN_UI) ||
                  customiseSettingConfigEnum.equals(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_CUSTOMER_NAME) ||
                  customiseSettingConfigEnum.equals(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_MODIFIED_DATE) ||
                  customiseSettingConfigEnum.equals(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_NUMBERING) ||
                  customiseSettingConfigEnum.equals(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_SYSTEMTYPE) ||
                  customiseSettingConfigEnum.equals(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_VARIATION) ||
                  customiseSettingConfigEnum.equals(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_VERSION)  )
              {
                updateProjectTable();
                updateProjectDatabaseTable();
                updateProjectSummaryPanel();
              }
            }
          }
        }
        else
        {
          Assert.expect(false, "No update code for observable: " + observable.getClass().getName());
        }
      }
    });
  }

  /**
   * @author George Booth
   */
  private void handleProjectObservable(Object arg)
  {
    Assert.expect(arg != null);
    if (arg instanceof ProjectChangeEvent)
    {

      ProjectChangeEvent event = (ProjectChangeEvent)arg;
      ProjectChangeEventEnum eventEnum = event.getProjectChangeEventEnum();
      if (eventEnum instanceof ProjectEventEnum)
      {
        Project project = null;
        String oldValue = null;
        Object source = event.getSource();
        if (source instanceof Project)
        {
          project = (Project)source;
        }
        else
        {
          // special case - Project.delete() returns Class, get oldValue for project name
          oldValue = (String)event.getOldValue();
        }
        ProjectEventEnum projectEventEnum = (ProjectEventEnum)eventEnum;
        if ((projectEventEnum.equals(ProjectEventEnum.SLOW_LOAD)) || (projectEventEnum.equals(ProjectEventEnum.FAST_LOAD)))
        {
          // project was loaded - select it and show summary
          _project = project;
          if (_filterTextField.getText().isEmpty())
            _localProjectsTableModel.populateTableData();
          else
            _localProjectsTableModel.populateFilteredTableData(_filterTextField.getText());
          try
          {
            updateCurrentProject();
          }
          catch(DatastoreException ex)
          {
            //Khaw Chek Hau - Xfr2511: Error Message Prompted Multiple Times
            MainMenuGui mainUI = MainMenuGui.getInstance();
            MessageDialog.showErrorDialog(
                    mainUI,
                    StringUtil.format(ex.getMessage(), 50),
                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                    true);
            return;
          }
          updateStatus();
          if (projectEventEnum.equals(ProjectEventEnum.SLOW_LOAD))
          {
            SwingWorkerThread.getInstance().invokeLater(new Runnable()
            {
              public void run()
              {
                if (_project != null)
                  DrawCadPanel.createPanelImage(_project);
              }
            });
          }
        }
        else if (projectEventEnum.equals(ProjectEventEnum.SAVE))
        {
          _project = project;

          //Khaw Chek Hau -XCR2501: Assert when use serial number matching with preInspection script
          if (_mainUI.isHomeCurrentEnvironment())
          {
            // SAVE is either Save() or SaveAs()
            // project was added to disk - re-populate table
            if (_filterTextField.getText().isEmpty())
              _localProjectsTableModel.populateTableData();
            else
              _localProjectsTableModel.populateFilteredTableData(_filterTextField.getText());
            try
            {
              updateCurrentProject();
            }
            catch(DatastoreException ex)
            {
              return;
            }
            updateStatus();
          }
        }
        else if (projectEventEnum.equals(ProjectEventEnum.NAME))
        {
          // project was renamed
          // will be caught by SAVE above
        }
        else if (projectEventEnum.equals(ProjectEventEnum.IMPORT_PROJECT_FROM_NDFS))
        {
          // project was added to disk - re-populate table
          _project = project;
          if (_filterTextField.getText().isEmpty())
            _localProjectsTableModel.populateTableData();
          else
            _localProjectsTableModel.populateFilteredTableData(_filterTextField.getText());
          try
          {
            updateCurrentProject();
          }
          catch(DatastoreException ex)
          {
            return;
          }
          updateStatus();
        }
        else if (projectEventEnum.equals(ProjectEventEnum.DELETE))
        {
          // ProjectEventEnum.DELETE is not generated via the GUI.
          // Utilities>Delete Project calls projectWasDeleted() through MainUI
        }
        else if (projectEventEnum.equals(ProjectEventEnum.PANEL_IMAGE_CREATION))
        {
          if ((_active) && (_selectedLocalProjectName != null) && (_selectedLocalProjectName.equalsIgnoreCase(project.getName())))
            showProjectGraphics(project.getName());
        }
        else
        {
          // do nothing, already handled project events of interest
        }
      }
    }
  }

  /**
   * Called when an XrayTestException has been seen
   * @author George Booth
   */
  void xRayTesterExceptionThrown()
  {
    updateSystemSummary();
  }

  /**
   * Called when a hardware request is completed
   * @author George Booth
   */
  void systemRequestCompleted()
  {
    updateSystemSummary();
  }

  /**
   * @author George Booth
   */
  void updatePanelData()
  {
    try
    {
      // populate system summary data
      updateSystemSummary();

      // Capture the selected project name so it can be reselected
      String selectedProjectName = null;
      if (_selectedLocalProjectIndex > -1)
      {
        selectedProjectName = _localProjectsTableModel.getProjectNameAt(_selectedLocalProjectIndex);
      }

      // update available projects table
      if (_filterTextField.getText().isEmpty())
        _localProjectsTableModel.populateTableData();
      else
        _localProjectsTableModel.populateFilteredTableData(_filterTextField.getText());

      if (_availableProjectsTabbedPane.getSelectedIndex() == 0)
      {
        // Update the summary data for the loaded project so data is current
        if (_project != null && _loadedProjectName != null)
        {
          AvailableProjectData data = new AvailableProjectData(_project);
          int index = _localProjectsTableModel.getProjectIndex(_loadedProjectName);
          if (index > -1)
          {
            _localProjectsTableModel.updateSummaryData(index, data);
          }
          else
          {
            // project has mysteriously disappeared - bad user, BAD!
            projectWasDeleted(_loadedProjectName);
            _mainUI.getMainMenuBar().projectClose(true);
          }
        }

        // reselect the selected project if there was one and it's still in the table
        if (selectedProjectName != null)
        {
          _selectedLocalProjectIndex = _localProjectsTableModel.getProjectIndex(selectedProjectName);
          if (_selectedLocalProjectIndex > -1)
          {
            _localProjectsTable.setRowSelectionInterval(_selectedLocalProjectIndex, _selectedLocalProjectIndex);
          }
        }
      }

      // Capture the selected database project name so it can be reselected
      String selectedDatabaseProjectName = null;
      int selectedDatabaseVersion = -1;
      if (_selectedProjectDatabaseIndex > -1)
      {
        selectedDatabaseProjectName = _projectDatabaseTableModel.getProjectNameAt(_selectedProjectDatabaseIndex);
        selectedDatabaseVersion = _projectDatabaseTableModel.getProjectVersionAt(_selectedProjectDatabaseIndex);
      }

      // populate project database table
      if (_filterTextField.getText().isEmpty())
        _projectDatabaseTableModel.populateTableData();
      else
        _projectDatabaseTableModel.populateFilteredTableData(_filterTextField.getText());

      if (_availableProjectsTabbedPane.getSelectedIndex() == 1)
      {
        // reselect the selected project if there was one and it's still in the table
        if (selectedDatabaseProjectName != null)
        {
          _selectedProjectDatabaseIndex = _projectDatabaseTableModel.getProjectIndex(selectedDatabaseProjectName, selectedDatabaseVersion);
          if (_selectedProjectDatabaseIndex > -1)
          {
            _projectDatabaseTable.setRowSelectionInterval(_selectedProjectDatabaseIndex, _selectedProjectDatabaseIndex);
          }
        }
      }

      updateDatabaseSummary();
      updateStatus();
    }
    catch (DatastoreException ex)
    {
      //Khaw Chek Hau - Xfr2511: Error Message Prompted Multiple Times
      MainMenuGui mainUI = MainMenuGui.getInstance();
      MessageDialog.showErrorDialog(
        mainUI,
        StringUtil.format(ex.getMessage(), 50),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
      return;
    }
  }

  /**
   * @author George Booth
   */
  void updateStatus()
  {
    int numProjects = _localProjectsTable.getRowCount();
    String message = null;
    if (numProjects == 1)
    {
      message = StringLocalizer.keyToString("HP_PROJECT_AVAILABLE_KEY");
    }
    else
    {
      message = StringLocalizer.keyToString(new LocalizedString("HP_PROJECTS_AVAILABLE_KEY",
                                                                new Object[] {numProjects}));
    }
    if (ProjectDatabase.isAutoUpdateLocalProjectEnabled())
      message = message + "  " + StringLocalizer.keyToString("HP_AUTO_PULL_ON_KEY");
    else
      message = message + "  " + StringLocalizer.keyToString("HP_AUTO_PULL_OFF_KEY");
    _mainUI.setStatusBarText(message);
    // enable serial number controls
    enableSerialNumberSearch();
  }

  /**
   * Task is ready to start
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("AXIHomePanel().start()");

    _active = true;

    // set up menus
    super.startMenusAndToolBar();
    _menuBar.addMenu(_menuRequesterID, 2, _viewMenu);
    _menuBar.addMenuItem(_menuRequesterID, _viewMenu, _refreshPanelDataMenuItem);

    // refresh config states
    _online =_xrayTester.isHardwareAvailable();
    _hdwSim = _xrayTester.isSimulationModeOn();
    _devDebug = Config.isDeveloperDebugModeOn();
    _devPerformance = Config.isDeveloperPerformanceModeOn();
    
    //Siew Yeng - XCR1745 - Semi-automated mode
    _isSemiAutomatedMode = TestExecution.getInstance().isSemiAutomatedModeEnabled();

    // show/hide labels if config states have changed
    _systemSummaryValueLabels[_SYSTEM_STATUS].setVisible(_online);
    _hardwareSimWarningLabel.setVisible(_hdwSim);
    _developerDebugWarningLabel.setVisible(_devDebug);
    _developerPerformanceWarningLabel.setVisible(_devPerformance);
    _semiAutomatedModeWarningLabel.setVisible(_isSemiAutomatedMode);
    
    // get current projects path, update tab tooltip
    _availableProjectsTabbedPane.setToolTipTextAt(0,
    StringLocalizer.keyToString(new LocalizedString("HP_AVAILABLE_PROJECTS_TAB_TOOLTIP_KEY",
          new Object[] {Directory.getProjectsDir()})));

    // get current database path, remove spurious "/../", update tab tooltip
    _databasePath = Directory.shortenPath(Directory.getProjectDatabaseDir());
    _availableProjectsTabbedPane.setToolTipTextAt(1,
      StringLocalizer.keyToString(new LocalizedString("HP_PROJECT_DATABASE_TAB_TOOLTIP_KEY",
          new Object[] {_databasePath})));

    // update summary and panel data
    availableProjectsTabbedPane_stateChanged();
    
//    resetTableColumnPersistenceIfNeeded();
    
   // update column widths the user set last
    _localProjectsTable.setPreferredColumnWidths(
      _mainUI.getPersistSettings().getLocalProjectsTableColumnWidths());
    _projectDatabaseTable.setPreferredColumnWidths(
      _mainUI.getPersistSettings().getProjectDatabaseTableColumnWidths());
    _localProjectsTable.setEditorsAndRenderers();

    // set projects split pane divider to last valid location
    int dividerLocation  = _mainUI.getPersistSettings().getHomePanelDivider();
    if (dividerLocation > -1)
    {
      _projectsSplitPane.setDividerLocation(dividerLocation);
    }
    
    ConfigObservable.getInstance().addObserver(this);
  }
  
  /**
   * sheng chuan
   */
  void resetTableColumnPersistenceIfNeeded()
  {
    int[] persistentLocalProjectTableWidth = _mainUI.getPersistSettings().getLocalProjectsTableColumnWidths();
    int[] persistentDatabaseProjectTableWidth = _mainUI.getPersistSettings().getProjectDatabaseTableColumnWidths();
    //to handle persistent file not valid
    if(persistentLocalProjectTableWidth != null)
    {
      if (persistentLocalProjectTableWidth.length < _selectedProjectDataTable.getColumnCount())
      {
        if (_selectedProjectDataTable.getColumnCount() == 7)
        {
          _mainUI.getPersistSettings().setLocalProjectsTableColumnWidths(new int[]
          {
            30,
            persistentLocalProjectTableWidth[0],
            50,
            persistentLocalProjectTableWidth[1],
            persistentLocalProjectTableWidth[2],
            persistentLocalProjectTableWidth[3],
            90,
          });
        }
      }
    }      
    
    if(persistentDatabaseProjectTableWidth != null)
    {
      if (persistentDatabaseProjectTableWidth.length < _selectedProjectDataTable.getColumnCount())
      {
        if (_selectedProjectDataTable.getColumnCount() == 5)
        {
          _mainUI.getPersistSettings().setProjectDatabaseTableColumnWidths(new int[]
          {
            30,
            persistentDatabaseProjectTableWidth[0],
            persistentDatabaseProjectTableWidth[1],
            persistentDatabaseProjectTableWidth[2],
            persistentDatabaseProjectTableWidth[3],
          });
        }
      }
    }
  }

  /**
   * Can the environment be swapped out
   * @return true if environment can be swapped
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
//     System.out.println("HomePanel().isReadyToFinish()");
    return true;
  }

  /**
   * The environment is about to be swapped out and should perform cleanup
   * @author George Booth
   */
  public void finish()
  {
//    System.out.println("AXIHomePanel().finish()");

    // remove menus
    super.finishMenusAndToolBar();

    _mainUI.getPersistSettings().setHomePanelDivider(_projectsSplitPane.getDividerLocation());
    _mainUI.getPersistSettings().setLocalProjectsTableColumnWidths(_localProjectsTable.getColumnWidths());
    _mainUI.getPersistSettings().setProjectDatabaseTableColumnWidths(_projectDatabaseTable.getColumnWidths());

    _active = false;
  }

  // ------------------------------------------
  // Helper methods to construct GUI components
  // ------------------------------------------

  /**
   * Sets up the System Summary panel
   * @author George Booth
   */
  void setupSystemSummaryPanel()
  {
    _informationPanel = new JPanel();
    _informationPanel.setLayout(new BoxLayout(_informationPanel,BoxLayout.PAGE_AXIS));

    setupSelectProjectPanel();

    _informationPanel.add(_selectProjectPanel);

    int itemCount = _systemSummaryItems.size();
    _systemSummaryItemLabels = new JLabel[itemCount];
    _systemSummaryValueLabels = new JLabel[itemCount];

    _systemSummaryPanel = new JPanel();
    _systemSummaryPanel.setLayout(new BorderLayout());

    _systemSummaryBox = Box.createHorizontalBox();
    _systemSummaryPanel.add(_systemSummaryBox, BorderLayout.NORTH);

    _systemTypePanel = new JPanel(new PairLayout(10,0));
    addSystemSummaryItem(_systemTypePanel, _SYSTEM_TYPE);
    //Ngie Xing
    addSystemSummaryItem(_systemTypePanel, _LOW_MAGNIFICATION);
    addSystemSummaryItem(_systemTypePanel, _HIGH_MAGNIFICATION);

    _systemSummaryBox.add(_systemTypePanel);
    _systemSummaryBox.add(Box.createGlue());

    // system status and warnings
    // labels are set visible in start() if needed
    _systemStatusPanel = new JPanel();
    _systemStatusPanel.setLayout(new BorderLayout());
    _systemSummaryBox.add(_systemStatusPanel);
    _systemSummaryBox.add(Box.createGlue());

    _systemSummaryValueLabels[_SYSTEM_STATUS] = new JLabel("");
    if (_useBoldFont)
    {
      _systemSummaryValueLabels[_SYSTEM_STATUS].setFont(FontUtil.getMediumFont(Font.BOLD));
    }
    _systemSummaryValueLabels[_SYSTEM_STATUS].setHorizontalAlignment(SwingConstants.CENTER);
//    _systemStatusPanel.add(_systemSummaryValueLabels[_SYSTEM_STATUS], BorderLayout.NORTH);

    _hardwareSimWarningLabel = new JLabel();
    if (_useBoldFont)
    {
      _hardwareSimWarningLabel.setFont(FontUtil.getBoldFont(_hardwareSimWarningLabel.getFont(),Localization.getLocale()));
    }
    _hardwareSimWarningLabel.setText("Warning: Hardware Simulation is enabled");
    _hardwareSimWarningLabel.setForeground(Color.RED);
    _hardwareSimWarningLabel.setHorizontalAlignment(SwingConstants.CENTER);
//    _systemStatusPanel.add(_hardwareSimWarningLabel, BorderLayout.CENTER);
    
    _semiAutomatedModeWarningLabel = new JLabel();
    if (_useBoldFont)
    {
      _semiAutomatedModeWarningLabel.setFont(FontUtil.getBoldFont(_semiAutomatedModeWarningLabel.getFont(),Localization.getLocale()));
    }
    _semiAutomatedModeWarningLabel.setText("Warning: Semi Automated mode is enabled");
    _semiAutomatedModeWarningLabel.setForeground(Color.RED);
    _semiAutomatedModeWarningLabel.setHorizontalAlignment(SwingConstants.CENTER);

    _developerDebugWarningLabel = new JLabel();
    if (_useBoldFont)
    {
      _developerDebugWarningLabel.setFont(FontUtil.getBoldFont(_developerDebugWarningLabel.getFont(),Localization.getLocale()));
    }
    _developerDebugWarningLabel.setText("Warning: Developer Debug Mode is enabled");
    _developerDebugWarningLabel.setForeground(Color.RED);
    _developerDebugWarningLabel.setHorizontalAlignment(SwingConstants.CENTER);
//    _systemStatusPanel.add(_developerDebugWarningLabel, BorderLayout.NORTH);

    _developerPerformanceWarningLabel = new JLabel();
    if (_useBoldFont)
    {
      _developerPerformanceWarningLabel.setFont(FontUtil.getBoldFont(_developerPerformanceWarningLabel.getFont(), Localization.getLocale()));
    }
    _developerPerformanceWarningLabel.setText("Warning: Developer Performance Mode is enabled");
    _developerPerformanceWarningLabel.setForeground(Color.RED);
    _developerPerformanceWarningLabel.setHorizontalAlignment(SwingConstants.CENTER);
//    _systemStatusPanel.add(_developerPerformanceWarningLabel, BorderLayout.SOUTH);
    
    //Siew Yeng - XCR1745 - semi automated mode
    _centerPanel = new JPanel();
    _centerPanel.setLayout(new VerticalFlowLayout(VerticalFlowLayout.CENTER,VerticalFlowLayout.TOP,0,0));
    _centerPanel.add(_developerDebugWarningLabel);
    _centerPanel.add(_hardwareSimWarningLabel);
    _centerPanel.add(_semiAutomatedModeWarningLabel);
    _centerPanel.add(_developerPerformanceWarningLabel);
    
    _centerOuterPanel = new JPanel();
    _centerOuterPanel.add(_centerPanel, BorderLayout.CENTER);
    _systemStatusPanel.add(_centerOuterPanel, BorderLayout.CENTER);
    
    _softwareRevisionPanel = new JPanel(new PairLayout(10,0));
    addSystemSummaryItem(_softwareRevisionPanel, _SOFTWARE_REV);
    addSystemSummaryItem(_softwareRevisionPanel, _DEFECT_PACKAGER_STATUS);
    _systemSummaryBox.add(_softwareRevisionPanel);

    _systemSummaryCenterPanel = new JPanel();
    if (_online)
    {
      _systemSummaryCenterPanel.setLayout(new GridLayout(3, 1));
    }
    else
    {
      _systemSummaryCenterPanel.setLayout(new GridLayout(2, 1));
    }
//    _systemSummaryCenterPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
//    _systemSummaryPanel.add(_systemSummaryCenterPanel, BorderLayout.CENTER);
    _informationPanel.add(_systemSummaryCenterPanel);

    _systemInformationPanel = new JPanel();
    _systemInformationPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(
            Color.white, new Color(165, 163, 151)),
              StringLocalizer.keyToString("HP_SYSTEM_INFO_PANEL_KEY")),
        BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    _systemInformationPanel.setLayout(new PairLayout(10,0));
    addSystemSummaryItem(_systemInformationPanel, _SYSTEM_ID);
    addSystemSummaryItem(_systemInformationPanel, _USER_ID);
    addSystemSummaryItem(_systemInformationPanel, _CURRENT_PROJECT);
    _systemSummaryCenterPanel.add(_systemInformationPanel); //, BorderLayout.WEST);

    // only show hardware status if on-line
    if (_online)
    {
      _systemSetupPanel = new JPanel();
      _systemSetupPanel.setBorder(BorderFactory.createCompoundBorder(
          new TitledBorder(BorderFactory.createEtchedBorder(
              Color.white, new Color(165, 163, 151)),
                           StringLocalizer.keyToString("HP_SYSTEM_SETUP_PANEL_KEY")),
          BorderFactory.createEmptyBorder(5, 5, 5, 5)));
      _systemSetupPanel.setLayout(new PairLayout(10, 0));
      addSystemSummaryItem(_systemSetupPanel, _BAR_CODE_MODE);
      addSystemSummaryItem(_systemSetupPanel, _HANDLER_MODE);
      addSystemSummaryItem(_systemSetupPanel, _HANDLER_STATUS);
      addSystemSummaryItem(_systemSetupPanel, _HIGH_MAG_STATUS);
      _systemSummaryCenterPanel.add(_systemSetupPanel); //, BorderLayout.CENTER);
    }

    // project database panel
//    _systemSummaryEastPanel = new JPanel();
//    _systemSummaryEastPanel.setLayout(new BorderLayout());
//    _systemSummaryEastPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
//    _systemSummaryPanel.add(_systemSummaryEastPanel, BorderLayout.EAST);

    _databaseSummaryPanel = new JPanel();
    _databaseSummaryPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(
            Color.white, new Color(165, 163, 151)),
              StringLocalizer.keyToString("HP_DATABASE_SUMMARY_PANEL_KEY")),
        BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    _databaseSummaryPanel.setLayout(new BorderLayout(10,0));
    _systemSummaryCenterPanel.add(_databaseSummaryPanel); //, BorderLayout.EAST);
//    _systemSummaryEastPanel.add(_databaseSummaryPanel, BorderLayout.CENTER);

    itemCount = _databaseSummaryItems.size();
    _databaseSummaryItemLabels = new JLabel[itemCount];
    _databaseSummaryValueLabels = new JLabel[itemCount];

    int panels = 1; // only 1 group
    int rowsPerPanel = (itemCount / panels);

    // left summary items
    _databaseSummaryInnerPanel = new JPanel(new PairLayout(10,0));
    int row = 0;                  // trap for no items
    while (row < rowsPerPanel && row < itemCount)
    {
      addDatabaseSummaryItem(_databaseSummaryInnerPanel, row);
      row++;
    }
    _databaseSummaryPanel.add(_databaseSummaryInnerPanel, BorderLayout.WEST);


        // project and database summaries
    _lowerSummaryPanel = new JPanel();
    _lowerSummaryPanel.setLayout(new BorderLayout(0,0));  //20,0));
//    _projectsTextPanel.add(_lowerSummaryPanel, BorderLayout.SOUTH);
    _informationPanel.add(_lowerSummaryPanel);

    // selected project summary
    updateProjectSummaryPanel();

  }
  /**
   * Adds a system summary item and an empty value to a PairLayout panel
   * @author George Booth
   */
  void addSystemSummaryItem(JPanel panel, int index)
  {
    String itemName = _systemSummaryItems.get(index);
    // create item label and empty value
    _systemSummaryItemLabels[index] = new JLabel(itemName + ": ");
    if (itemName.equals(""))
    {
      _systemSummaryItemLabels[index].setText("");
    }
    _systemSummaryValueLabels[index] = new JLabel("");
    if (_useBoldFont)
    {
      _systemSummaryValueLabels[index].setFont(FontUtil.getBoldFont(_systemSummaryValueLabels[index].getFont(),Localization.getLocale()));
    }
    // add to PairLayout panel
    panel.add(_systemSummaryItemLabels[index]);
    panel.add(_systemSummaryValueLabels[index]);
  }

  /**
   * Adds a database summary item to the Database Summary panel
   * @author George Booth
   */
  void addDatabaseSummaryItem(JPanel panel, int index)
  {
    String itemName = _databaseSummaryItems.get(index);
    // create item label and empty value
    _databaseSummaryItemLabels[index] = new JLabel(itemName + ": ");
    if (itemName.equals(""))
    {
      _databaseSummaryItemLabels[index].setText("");
    }
    _databaseSummaryValueLabels[index] = new JLabel("");
    if (_useBoldFont)
    {
      _databaseSummaryValueLabels[index].setFont(FontUtil.getBoldFont(_databaseSummaryValueLabels[index].getFont(),Localization.getLocale()));
    }
    // add to item PairLayout panel
    panel.add(_databaseSummaryItemLabels[index]);
    panel.add(_databaseSummaryValueLabels[index]);
  }

  /**
   * @author Chong, Wei Chin
   */
  private void setupSelectProjectPanel()
  {
    // select project
    _selectProjectPanel = new JPanel();
    _selectProjectPanel.setLayout(new BoxLayout(_selectProjectPanel, BoxLayout.PAGE_AXIS));
//    _projectsTextPanel.add(_selectProjectPanel, BorderLayout.NORTH);
//    _informationPanel.add(_selectProjectPanel);

    // button panel
    _selectProjectButtonPanel = new JPanel(new GridLayout(2,1,2,2));
    _selectProjectPanel.add(_selectProjectButtonPanel);
    _selectProjectPanel.add(Box.createRigidArea(new Dimension(0,10)));;
    // open, show notes
    _selectedProjectButtonPanel = new JPanel();
    _selectedProjectButtonPanel.setLayout(new GridLayout(1,2,5,0)); //BorderLayout());
    _selectedProjectButtonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    _selectProjectButtonPanel.add(_selectedProjectButtonPanel);

    _selectedProjectOpenButton = new JButton(
      StringLocalizer.keyToString("HP_OPEN_PROJECT_BUTTON_KEY"));
    _selectedProjectOpenButton.setToolTipText(
      StringLocalizer.keyToString("HP_OPEN_PROJECT_BUTTON_TOOLTIP_KEY"));
    _selectedProjectOpenButton.setEnabled(false);
    _selectedProjectOpenButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        openOrPullButtonClicked();
      }
    });
    _selectedProjectButtonPanel.add(_selectedProjectOpenButton);

    _showProjectNotesButton = new JButton(
      StringLocalizer.keyToString("HP_SHOW_PROJECT_NOTES_BUTTON_KEY"));
    _showProjectNotesButton.setToolTipText(
      StringLocalizer.keyToString("HP_SHOW_PROJECT_NOTES_BUTTON_TOOLTIP_KEY"));
    _showProjectNotesButton.setEnabled(false);
    _showProjectNotesButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showProjectNotes();
      }
    });
    _selectedProjectButtonPanel.add(_showProjectNotesButton);

    // add, show comments
    _projectDatabaseButtonPanel = new JPanel();
    _projectDatabaseButtonPanel.setLayout(new GridLayout(1,2,5,0)); //BorderLayout());
    _projectDatabaseButtonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    _selectProjectButtonPanel.add(_projectDatabaseButtonPanel);

    _projectDatabaseAddButton = new JButton(
      StringLocalizer.keyToString("HP_ADD_TO_DATABASE_BUTTON_KEY"));
    _projectDatabaseAddButton.setToolTipText(
      StringLocalizer.keyToString("HP_ADD_TO_DATABASE_BUTTON_TOOLTIP_KEY"));
    _projectDatabaseAddButton.setEnabled(false);
    _projectDatabaseAddButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addOrDeleteButtonClicked();
      }
    });
    _projectDatabaseButtonPanel.add(_projectDatabaseAddButton);

    _showDatabaseCommentsButton = new JButton(
      StringLocalizer.keyToString("HP_SHOW_DATABASE_COMMENTS_BUTTON_KEY"));
    _showDatabaseCommentsButton.setToolTipText(
      StringLocalizer.keyToString("HP_SHOW_DATABASE_COMMENTS_BUTTON_TOOLTIP_KEY"));
    _showDatabaseCommentsButton.setEnabled(false);
    _showDatabaseCommentsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showDatabaseComments();
      }
    });
    _projectDatabaseButtonPanel.add(_showDatabaseCommentsButton);

    // select project with serial number
    _selectWithSerialNumberPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _selectWithSerialNumberPanel.setBorder(BorderFactory.createTitledBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(
            Color.white, new Color(165, 163, 151)),
              StringLocalizer.keyToString("HP_SERIAL_NUMBER_PANEL_KEY"))));
    _selectProjectPanel.add(_selectWithSerialNumberPanel);

    _selectWithSerialNumberLabel = new JLabel(
      StringLocalizer.keyToString("HP_SERIAL_NUMBER_LABEL_KEY"));
    _selectWithSerialNumberPanel.add(_selectWithSerialNumberLabel);

    _selectWithSerialNumberTextField = new JTextField("");
    _selectWithSerialNumberTextField.setColumns(15);
    _selectWithSerialNumberTextField.setToolTipText(
      StringLocalizer.keyToString("HP_SERIAL_NUMBER_TOOLTIP_KEY"));
    _selectWithSerialNumberTextField.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectWithSerialNumberGo();
      }
     });
    _selectWithSerialNumberPanel.add(_selectWithSerialNumberTextField);

    _selectWithSerialNumberSearchButton = new JButton(
      StringLocalizer.keyToString("HP_SERIAL_NUMBER_BUTTON_KEY"));
    _selectWithSerialNumberSearchButton.setIcon(Image5DX.getImageIcon(Image5DX.FIND));
    _selectWithSerialNumberSearchButton.setToolTipText(
        StringLocalizer.keyToString("HP_SERIAL_NUMBER_BUTTON_TOOLTIP_KEY"));
    _selectWithSerialNumberSearchButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectWithSerialNumberGo();
      }
    });
    _selectWithSerialNumberPanel.add(_selectWithSerialNumberSearchButton);
  }

  /**
   * Sets up the Projects panel
   * @author George Booth
   */
  private void setupProjectsPanel()
  {
    // upper side of scroll pane
    _projectsTextPanel = new JPanel();
    _projectsTextPanel.setLayout(new BorderLayout());

    // available projects and project database tabbed pane
    _availableProjectsTabbedPane = new JTabbedPane();
    _availableProjectsTabbedPane.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        availableProjectsTabbedPane_stateChanged();
      }
    });
//    _projectsTextPanel.add(_availableProjectsTabbedPane, BorderLayout.CENTER);

    createPopupMenuItems();
    createLocalProjectsPopupMenu();
    createProjectDatabasePopupMenu();
    
    updateProjectTable();
    updateProjectDatabaseTable();
    
    // selected project graphics
    _projectsGraphicsPanel = new JPanel();
    _projectsGraphicsPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(
            Color.white, new Color(165, 163, 151)),
              StringLocalizer.keyToString("HP_PROJECT_GRAPHICS_PANEL_KEY")),
        BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _projectsGraphicsPanel.setLayout(new BorderLayout(5, 0));

    // load direction arrows
    _projectsGraphicsPanel.add(_westUpArrowLabel, BorderLayout.WEST);
    _projectsGraphicsPanel.add(_eastUpArrowLabel, BorderLayout.EAST);
    // panel image
    _imageDisplayPanel = new ImageDisplayPanel();
    _projectsGraphicsPanel.add(_imageDisplayPanel, BorderLayout.CENTER);
    
    // Create a split pane with the two panels in it.
    _projectsSplitPane = new JSplitPane(
        JSplitPane.VERTICAL_SPLIT,
        _availableProjectsTabbedPane,
        _projectsGraphicsPanel);
    // no divider arrows
    _projectsSplitPane.setOneTouchExpandable(false);
    // give the text half more space by default
    _projectsSplitPane.setResizeWeight(_projectsTextResizeWeight);

    //Provide minimum sizes for the two components in the split pane
    _projectsTextPanel.setMinimumSize(new Dimension(50, 50));
    _projectsGraphicsPanel.setMinimumSize(new Dimension(50, 50));
  }

  /**
   * Adds a project summary item to the Projects Summary panel
   * @author George Booth
   */
  void addProjectSummaryItem(JPanel panel, int index)
  {
    String itemName = _projectSummaryItems.get(index);
    // create item label and empty value
    boolean displayItem = true;
    if (_isCustomizeSettingOn)
    {
      if (_displayLabelInfo.containsKey(itemName))
      {
        displayItem = _displayLabelInfo.get(itemName);
      }
    }
    else 
    {
      if (_displayLabelInfo.containsKey(itemName))
      {
        displayItem = false;
      }
    }

    _projectSummaryItemLabels[index] = new JLabel(itemName);
    if (itemName.equals(""))
    {
      _projectSummaryItemLabels[index].setText("");
    }
    _projectSummaryValueLabels[index] = new JLabel("");
    if (_useBoldFont)
    {
      _projectSummaryValueLabels[index].setFont(FontUtil.getBoldFont(_projectSummaryValueLabels[index].getFont(),Localization.getLocale()));
    }
    // add to item PairLayout panel
    if (displayItem == true)
    {    
      panel.add(_projectSummaryItemLabels[index]);
      panel.add(_projectSummaryValueLabels[index]);
    }     
  }

  void createLocalProjectsPopupMenu()
  {
    _localProjectsPopupMenu = new JPopupMenu();
    _localProjectsPopupMenu.add(_openSelectedProjectPopupMenuItem);
    _localProjectsPopupMenu.add(_deleteSelectedProjectPopupMenuItem);
    _localProjectsPopupMenu.add(_addDatabaseProjectPopupMenuItem);
    _localProjectsPopupMenu.add(_showProjectNotesPopupMenuItem1);
    _localProjectsPopupMenu.add(_showHistoryLogPopupMenuItem1);
  }

  void createProjectDatabasePopupMenu()
  {
    _projectDatabasePopupMenu = new JPopupMenu();
    _projectDatabasePopupMenu.add(_pullDatabaseProjectPopupMenuItem);
    _projectDatabasePopupMenu.add(_deleteDatabaseProjectPopupMenuItem);
    _projectDatabasePopupMenu.add(_showProjectNotesPopupMenuItem2);
    _projectDatabasePopupMenu.add(_showDatabaseCommentsPopupMenuItem2);
  }

  void createPopupMenuItems()
  {
    _openSelectedProjectPopupMenuItem = new JMenuItem(StringLocalizer.keyToString("HP_OPEN_PROJECT_BUTTON_KEY"));
    _openSelectedProjectPopupMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        openSelectedProject();
      }
     });

    _deleteSelectedProjectPopupMenuItem = new JMenuItem(StringLocalizer.keyToString("HP_DELETE_PROJECT_MENU_KEY"));
    _deleteSelectedProjectPopupMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteSelectedProject();
      }
     });

    _addDatabaseProjectPopupMenuItem = new JMenuItem(StringLocalizer.keyToString("HP_ADD_TO_DATABASE_BUTTON_KEY"));
    _addDatabaseProjectPopupMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addSelectedProjectToDatabase();
      }
     });

    _pullDatabaseProjectPopupMenuItem = new JMenuItem(StringLocalizer.keyToString("HP_PULL_PROJECT_BUTTON_KEY"));
    _pullDatabaseProjectPopupMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        pullSelectedProjectFromDatabase();
      }
     });

    _deleteDatabaseProjectPopupMenuItem = new JMenuItem(StringLocalizer.keyToString("HP_DELETE_FROM_DATABASE_BUTTON_KEY"));
    _deleteDatabaseProjectPopupMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteSelectedProjectFromDatabase();
      }
     });

    //show project's history log - hsia-fen.tan
    _showHistoryLogPopupMenuItem1 = new JMenuItem(StringLocalizer.keyToString("HP_SHOW_PROJECT_HISTORY_LOG_BUTTON_KEY"));
    _showHistoryLogPopupMenuItem1.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
          showProjectHistory();       
      }
     });


    _showProjectNotesPopupMenuItem1 = new JMenuItem(StringLocalizer.keyToString("HP_SHOW_PROJECT_NOTES_BUTTON_KEY"));
    _showProjectNotesPopupMenuItem1.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showProjectNotes();
      }
     });

    _showDatabaseCommentsPopupMenuItem1 = new JMenuItem(StringLocalizer.keyToString("HP_SHOW_DATABASE_COMMENTS_BUTTON_KEY"));
    _showDatabaseCommentsPopupMenuItem1.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showDatabaseComments();
      }
     });

    _showProjectNotesPopupMenuItem2 = new JMenuItem(StringLocalizer.keyToString("HP_SHOW_PROJECT_NOTES_BUTTON_KEY"));
    _showProjectNotesPopupMenuItem2.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showProjectNotes();
      }
     });

    _showDatabaseCommentsPopupMenuItem2 = new JMenuItem(StringLocalizer.keyToString("HP_SHOW_DATABASE_COMMENTS_BUTTON_KEY"));
    _showDatabaseCommentsPopupMenuItem2.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showDatabaseComments();
      }
     });
  }
  
  /**
   * @author Kee, Chin Seong
   * Pooling Defect Packager Status Real Time.
   */
  void checkDefectPackagerQueryStatus()
  {
    final Observer thisObserver = this;
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
      {
        public void run() 
        {
          _defectPackagerObservable.deleteObserver(thisObserver);
          if(DefectPackagerUtil.queryService().length() > 0)
          {
            if(DefectPackagerUtil.queryService().matches(DefectPackagerUtil._DEFECT_PACKAGER_STARTED))
            {
              _systemSummaryValueLabels[_DEFECT_PACKAGER_STATUS].setText(StringLocalizer.keyToString("GUI_DEFECT_PACKAGER_RUNNING_KEY"));
              _defectPackagerObservable.stateChanged(DefectPackagerTaskEventEnum.DEFECT_PACKAGER_RUNNING);
            }
            else
            {
              _systemSummaryValueLabels[_DEFECT_PACKAGER_STATUS].setText(StringLocalizer.keyToString("GUI_DEFECT_PACKAGER_NOT_RUNNING_KEY"));
              _defectPackagerObservable.stateChanged(DefectPackagerTaskEventEnum.DEFECT_PACKAGER_STOPPED);
            }
          }
          else
          {
            _systemSummaryValueLabels[_DEFECT_PACKAGER_STATUS].setText(StringLocalizer.keyToString("GUI_DEFECT_PACKAGER_NOT_INSTALLED_KEY"));
            _defectPackagerObservable.stateChanged(DefectPackagerTaskEventEnum.DEFECT_PACKAGER_NOT_INSTALLED);
          }
          _defectPackagerObservable.addObserver(thisObserver);
        }
       }        
    );    
  } 
  
  /**
   * XCR-3436
   * @author weng-jian.eoh
   */
  public void  updateProjectTable()
  {
    try
    {
      CustomizationSettingReader.getInstance().load(FileName.getCustomizeConfigurationFullPath(
        Config.getInstance().getStringValue(SoftwareConfigEnum.CUSTOMIZE_SETTING_CONFIGURATION_NAME_TO_USE)));
    }
    catch (DatastoreException ex)
    {
      MessageDialog.showErrorDialog(
        this,
        StringUtil.format(ex.getMessage(), 50),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    }
    if (_localProjectsTable != null)
    {
      _availableProjectsTabbedPane.removeAll();
      _localProjectsTable.removeAll();
    }
    _isCustomizeSettingOn = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_MAIN_UI);
    _localProjectsTableModel = new AvailableProjectsTableModel();
    _localProjectsTable = new AvailableProjectsTable()
    {
      /**
       * Overriding this method allows us to make sure the selected rows
       * are still the proper selected rows after a sort.
       * @author George Booth
       */
      public void mouseReleased(MouseEvent event)
      {
        // make sure selection is correct after a sort
        String selectedProjectName = _selectedProjectName;
        super.mouseReleased(event);
        int row = _localProjectsTableModel.getRowForProject(selectedProjectName);
        if (row > -1)
        {
          _localProjectsTable.setRowSelectionInterval(row, row);
        }
      }
    };
    _localProjectsTable.resetSortHeader();
    _localProjectsTable.setModel(_localProjectsTableModel);
    _localProjectsTableModel.populateTableData();
    setupLocalProjectsTableListSelection();
    // load project with enter key
    _localProjectsTable.addKeyListener(new KeyAdapter()
    {
      // consume enter key events to avoid default "next row" behavior
      public void keyPressed(KeyEvent e)
      {
        if (e.getKeyCode() == KeyEvent.VK_ENTER)
        {
          e.consume();
        }
      }
      public void keyReleased(KeyEvent e)
      {
        if (e.getKeyCode() == KeyEvent.VK_ENTER)
        {
          e.consume();
          openSelectedProject();
        }
      }
    });
    // load project with double click
    _localProjectsTable.addMouseListener(new MouseAdapter()
    {
      // open project on double-click
      public void mouseClicked(MouseEvent e)
      {
        int clickCount = e.getClickCount();
        if (clickCount == 2 && e.isPopupTrigger() == false)
        {
          openSelectedProject();
        }
      }
      // show project menu on right click
      public void mouseReleased(MouseEvent e)
      {
        if (e.isPopupTrigger())
        {
          int selectedRowIndex = _localProjectsTable.rowAtPoint(e.getPoint());
          if (selectedRowIndex == -1)
          {
            // user dragged cursor out of the table
            return;
          }
          // disable delete if selected project is loaded
          String thisProject = _localProjectsTableModel.getProjectNameAt(selectedRowIndex);
          _deleteSelectedProjectPopupMenuItem.setEnabled(thisProject.equalsIgnoreCase(_loadedProjectName) == false);

          // show the popup menu
          if(selectedRowIndex >= 0)
          {
            _localProjectsTable.setRowSelectionInterval(selectedRowIndex, selectedRowIndex);
            _localProjectsPopupMenu.show(e.getComponent(), e.getX(), e.getY());
          }
        }
      }
    });
    _localProjectsScrollPane = new JScrollPane();
    _localProjectsScrollPane.getViewport().add(_localProjectsTable);
    _availableProjectsTabbedPane.addTab(
      StringLocalizer.keyToString("HP_AVAILABLE_PROJECTS_TAB_KEY"),
      null,
      _localProjectsScrollPane,
      StringLocalizer.keyToString(new LocalizedString("HP_AVAILABLE_PROJECTS_TAB_TOOLTIP_KEY",
            new Object[] {Directory.getProjectsDir()})));
    
  }
  
  /**
   * XCR-3436
   * @author weng-jian.eoh
   */
  public void updateProjectDatabaseTable()
  {
    try
    {
      CustomizationSettingReader.getInstance().load(FileName.getCustomizeConfigurationFullPath(
        Config.getInstance().getStringValue(SoftwareConfigEnum.CUSTOMIZE_SETTING_CONFIGURATION_NAME_TO_USE)));
    }
    catch (DatastoreException ex)
    {
      MessageDialog.showErrorDialog(
        this,
        StringUtil.format(ex.getMessage(), 50),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    }
    if (_projectDatabaseTable != null)
    {
      _projectDatabaseTable.removeAll();
    }
    _projectDatabaseTableModel = new ProjectDatabaseTableModel();
    _projectDatabaseTable = new ProjectDatabaseTable()
    {
      /**
       * Overriding this method allows us to make sure the selected rows
       * are still the proper selected rows after a sort.
       * @author George Booth
       */
      public void mouseReleased(MouseEvent event)
      {
        // make sure selection is correct after a sort
        String selectedProjectDatabaseName = _selectedProjectDatabaseName;
        super.mouseReleased(event);
        int row = _projectDatabaseTableModel.getRowForProject(selectedProjectDatabaseName);
        if (row > -1)
        {
          _projectDatabaseTable.setRowSelectionInterval(row, row);
        }
      }
    };
    _projectDatabaseTable.setModel(_projectDatabaseTableModel);
    setupProjectDatabaseTableListSelection();
    _projectDatabaseTableModel.populateTableData();
    // pull project with enter key
    _projectDatabaseTable.addKeyListener(new KeyAdapter()
    {
      // consume enter key events to avoid default "next row" behavior
      public void keyPressed(KeyEvent e)
      {
        if (e.getKeyCode() == KeyEvent.VK_ENTER)
        {
          e.consume();
        }
      }
      public void keyReleased(KeyEvent e)
      {
        if (e.getKeyCode() == KeyEvent.VK_ENTER)
        {
          e.consume();
          pullSelectedProjectFromDatabase();
        }
      }
    });
    // pull project files with double click
    _projectDatabaseTable.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent e)
      {
        int clickCount = e.getClickCount();
        if (clickCount == 2 && e.isPopupTrigger() == false)
        {
          pullSelectedProjectFromDatabase();
        }
      }
      // show project menu on right click
      public void mouseReleased(MouseEvent e)
      {
        if (e.isPopupTrigger())
        {
          int selectedRowIndex = _projectDatabaseTable.rowAtPoint(e.getPoint());
          if (selectedRowIndex == -1)
          {
            // user dragged cursor out of the table
            return;
          }

          // show the popup menu
          if(selectedRowIndex >= 0)
          {
            _projectDatabaseTable.setRowSelectionInterval(selectedRowIndex, selectedRowIndex);
            _projectDatabasePopupMenu.show(e.getComponent(), e.getX(), e.getY());
          }
        }
      }
    });
    _projectDatabaseScrollPane = new JScrollPane();
    _projectDatabaseScrollPane.getViewport().add(_projectDatabaseTable);
    _availableProjectsTabbedPane.addTab(
      StringLocalizer.keyToString("HP_PROJECT_DATABASE_TAB_KEY"),
      null,
      _projectDatabaseScrollPane,
      StringLocalizer.keyToString(new LocalizedString("HP_PROJECT_DATABASE_TAB_TOOLTIP_KEY",
            new Object[] {_databasePath})));
  }
  
  /**
   * XCR-3436
   * @autrhor weng-jian.eoh
   */
  public void updateProjectSummaryPanel()
  {
    try
    {
      CustomizationSettingReader.getInstance().load(FileName.getCustomizeConfigurationFullPath(
        Config.getInstance().getStringValue(SoftwareConfigEnum.CUSTOMIZE_SETTING_CONFIGURATION_NAME_TO_USE)));
    }
    catch (DatastoreException ex)
    {
      MessageDialog.showErrorDialog(
        this,
        StringUtil.format(ex.getMessage(), 50),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    }
    _isCustomizeSettingOn = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_MAIN_UI);
    if (_projectSummaryPanel != null)
    {
      _lowerSummaryPanel.remove(_projectSummaryPanel);
      _projectSummaryPanel.removeAll();
    }
    int itemCount;
    setupDisplayLabel();
    _projectSummaryPanel = new JPanel();
    _projectSummaryPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(
            Color.white, new Color(165, 163, 151)),
              StringLocalizer.keyToString("HP_PROJECT_SUMMARY_PANEL_KEY")),
        BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _projectSummaryPanel.setLayout(new BorderLayout(0,10));
    _lowerSummaryPanel.add(_projectSummaryPanel, BorderLayout.CENTER);

    itemCount = _projectSummaryItems.size();
    _projectSummaryItemLabels = new JLabel[itemCount];
    _projectSummaryValueLabels = new JLabel[itemCount];

    int panels = 2; // left and right summary groups
    int rowsPerPanel = (itemCount / panels);

    // upper summary items
    _projectSummaryLeftPanel = new JPanel(new PairLayout(10,0));
    int row = 0;                  // trap for no items
    while (row < rowsPerPanel && row < itemCount)
    {
      addProjectSummaryItem(_projectSummaryLeftPanel, row);
      row++;
    }
    _projectSummaryPanel.add(_projectSummaryLeftPanel, BorderLayout.NORTH);

    // lowwer summary items
    _projectSummaryRightPanel = new JPanel(new PairLayout(10,0));
    while (row < itemCount)
    {
      addProjectSummaryItem(_projectSummaryRightPanel, row);
      row++;
    }
    _projectSummaryPanel.add(_projectSummaryRightPanel, BorderLayout.CENTER);
  }
  
  /**
   * XCR-3436
   * @author weng-jian.eoh
   */
  private void setupDisplayLabel()
  {
    boolean displayVersion = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_VERSION);
    boolean displayCustomerName = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_CUSTOMER_NAME);
    boolean displaySystemType = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_SYSTEMTYPE);
    boolean displayLastModified = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_MODIFIED_DATE);
    boolean allBooleanValue[] = {
      displayVersion,
      displayCustomerName,
      displayLastModified,
      displaySystemType
    };
    
    String allLabel[] = {
      StringLocalizer.keyToString("HP_VERSION_LABEL_KEY"),
      StringLocalizer.keyToString("HP_CUSTOMER_LABEL_KEY"),
      StringLocalizer.keyToString("HP_MOD_DATE_LABEL_KEY"),
      StringLocalizer.keyToString("HP_PROJECT_SYSTEM_TYPE_LABEL_KEY")
    } ;
    
    int count = allLabel.length;
    
    for (int i = 0 ; i < count; i++)
    {
       _displayLabelInfo.put(allLabel[i], allBooleanValue[i]);
    }
  }
}

