package com.axi.v810.gui.home;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: AvailableProjectsTableModel</p>
 *
 * <p>Description:
 * This class is the table model for the Available Projects table in the AXI Home panel.
 * This model gets the summary data for projects in the local "projects" directory.</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class AvailableProjectsTableModel extends AbstractProjectDataTableModel
{  
  private boolean _isCustomizeDisplayColumnOn = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_MAIN_UI);
  private boolean _isShowNumbering = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_NUMBERING);
  private boolean _isShowRecipeName = true;
  private boolean _isShowVariation = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_VARIATION);
  private boolean _isShowCustomerName = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_CUSTOMER_NAME);
  private boolean _isShowVersion = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_VERSION);
  private boolean _isShowLastModifiedDate = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_MODIFIED_DATE);
  private boolean _isShowSystemType = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_SYSTEMTYPE);
  
  private LinkedList<String> _projectDataTableColumnNames = new LinkedList<String>();
  
  private HashMap<String, Pair<Integer, Boolean>> _defaultColumnInfoMap = new LinkedHashMap<String, Pair<Integer, Boolean>>()
  {
    {
      put(StringLocalizer.keyToString("HP_PROJECTS_TABLE_PROJECT_NO_COLUMN_KEY"), new Pair<Integer, Boolean>(0, _isShowNumbering));
      put(StringLocalizer.keyToString("HP_PROJECTS_TABLE_PROJECT_NAME_COLUMN_KEY"), new Pair<Integer, Boolean>(1, _isShowRecipeName));
      put(StringLocalizer.keyToString("HP_PROJECTS_TABLE_VARIATION_NAME_KEY"), new Pair<Integer, Boolean>(2, _isShowVariation));
      put(StringLocalizer.keyToString("HP_PROJECTS_TABLE_CUSTOMER_COLUMN_KEY"), new Pair<Integer, Boolean>(3, _isShowCustomerName));
      put(StringLocalizer.keyToString("HP_PROJECTS_TABLE_VERSION_COLUMN_KEY"), new Pair<Integer, Boolean>(4, _isShowVersion));
      put(StringLocalizer.keyToString("HP_PROJECTS_TABLE_LAST_MODIFIED_COLUMN_KEY"), new Pair<Integer, Boolean>(5, _isShowLastModifiedDate));
      put(StringLocalizer.keyToString("HP_PROJECTS_TABLE_LAST_PROJECT_SYSTEM_TYPE_KEY"), new Pair<Integer, Boolean>(6, _isShowSystemType));
    }
  };
  /**
   * @author George Booth
   */
  public AvailableProjectsTableModel()
  {
    super();
    setupDisplayColumn();
  }
  
  /**
   * @author George Booth
   */
  public int getColumnCount()
  {
    return _projectDataTableColumnNames.size();
  }

  /**
   * @author George Booth
   */
  public String getColumnName(int column)
  {
    Assert.expect(column > -1 && column < getColumnCount());

    return _projectDataTableColumnNames.get(column);
  }

  /**
   * @author George Booth
   */
  void populateTableData()
  {
    // get available projects from the projects directory
    java.util.List<String> availableProjectNames = FileName.getProjectNames();
    Object[] projectNames = availableProjectNames.toArray();

    _projectData = new LinkedList<AvailableProjectData>();

    // get summary data for each project
    for (int i = 0; i < projectNames.length; i++)
    {
      String projectName = (String)projectNames[i];
      // don't add projects with invalid names
      if (Project.isProjectNameValid(projectName))
      {
        try
        {
          ProjectSummary projectSummary = _projectSummaryReader.read(projectName);
          AvailableProjectData projectData = new AvailableProjectData(projectName, projectSummary);
          if (projectData.getSystemType().equals(SystemTypeEnum.THROUGHPUT.getName()))
          {
            projectData.setSystemType(SystemTypeEnum.STANDARD.getName().toUpperCase());
          }
          _projectData.add(projectData);
        }
        catch (DatastoreException de)
        {
          //Khaw Chek Hau - XCR2511: Error Message Prompted Multiple Times 
          //MainMenuGui mainUI = MainMenuGui.getInstance();
          //MessageDialog.showErrorDialog(
          //    mainUI,
          //    StringUtil.format(de.getMessage(), 50),
          //    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
          //    true);
        }
      }
    }

    sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableDataChanged();
  }
  
  /**
   * Filter recipe names in Available Project Table.
   * @author Ying-Huan.Chu
   */
  void populateFilteredTableData(String filter)
  {
    try
    {
      java.util.List<String> availableProjectNames = new ArrayList<>();      
      for (String projectName : FileName.getProjectNames())
      {
        List<String> variations = VariationSettingManager.getProjectNameToVariationNamesMap(projectName);
        for (String variationName : variations)
        {
          if (variationName.toLowerCase().contains(filter.toLowerCase()))
          {
            if (availableProjectNames.contains(projectName) == false)
            {
              availableProjectNames.add(projectName);
            }
          }
        }
        if (projectName.toLowerCase().contains(filter.toLowerCase()))
        {
          if (availableProjectNames.contains(projectName) == false)
            {
              availableProjectNames.add(projectName);
            }
        }
      }
      Object[] projectNames = availableProjectNames.toArray();

      _projectData = new LinkedList<>();

      // get summary data for each project
      for (int i = 0; i < projectNames.length; i++)
      {
        String projectName = (String)projectNames[i];
        // don't add projects with invalid names
        if (Project.isProjectNameValid(projectName))
        {
          ProjectSummary projectSummary = _projectSummaryReader.read(projectName);
          AvailableProjectData projectData = new AvailableProjectData(projectName, projectSummary);
          if (projectData.getSystemType().equals(SystemTypeEnum.THROUGHPUT.getName()))
          {
            projectData.setSystemType(SystemTypeEnum.STANDARD.getName().toUpperCase());
          }
          _projectData.add(projectData);
        }
      }

      sortColumn(_currentSortColumn, _currentSortAscending);
      fireTableDataChanged();
    }
    catch (DatastoreException de)
    {
      MainMenuGui mainUI = MainMenuGui.getInstance();
          MessageDialog.showErrorDialog(
              mainUI,
              StringUtil.format(de.getMessage(), 50),
              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
              true);
    }
  }
  
  /**
   * XCR-3436
   * @author weng-jian.eoh
   */
  private void setupDisplayColumn()
  {
    try
    {
      CustomizationSettingReader.getInstance().load(FileName.getCustomizeConfigurationFullPath(
        Config.getInstance().getStringValue(SoftwareConfigEnum.CUSTOMIZE_SETTING_CONFIGURATION_NAME_TO_USE)));
    }
    catch (DatastoreException ex)
    {
      MainMenuGui mainUI = MainMenuGui.getInstance();
          MessageDialog.showErrorDialog(
              mainUI,
              StringUtil.format(ex.getMessage(), 50),
              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
              true);
    }
    
    _isCustomizeDisplayColumnOn = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_MAIN_UI);
    _isShowNumbering = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_NUMBERING);
    _isShowRecipeName = true;
    _isShowVariation = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_VARIATION);
    _isShowCustomerName = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_CUSTOMER_NAME);
    _isShowVersion = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_VERSION);
    _isShowLastModifiedDate = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_MODIFIED_DATE);
    _isShowSystemType = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_SYSTEMTYPE);
    
    boolean[] columnDisplayFlag = {
      _isShowNumbering,
      _isShowRecipeName,
      _isShowVariation,
      _isShowCustomerName,
      _isShowVersion,
      _isShowLastModifiedDate,
      _isShowSystemType,
    };
    
    ArrayList<String> keySet = new ArrayList<String>(_defaultColumnInfoMap.keySet());
    for (int i = 0; i <columnDisplayFlag.length;i++)
    {
      Pair<Integer,Boolean> columnInfo = _defaultColumnInfoMap.get(keySet.get(i));
      columnInfo.setSecond(columnDisplayFlag[i]);
      _defaultColumnInfoMap.put(keySet.get(i), columnInfo);
    }
    
    if (_isCustomizeDisplayColumnOn == true)
    {
      for (int i =0 ; i < _defaultColumnInfoMap.keySet().size(); i++)
      {
     
        Pair<Integer,Boolean> columnInfo = (Pair<Integer,Boolean>)_defaultColumnInfoMap.get(keySet.get(i));
        boolean isShow = columnInfo.getSecond();
        if (isShow)
        {
          String colName = keySet.get(i);
          _projectDataTableColumnNames.add(colName);
        }
      }
    }
    else
    {
      _projectDataTableColumnNames.add(StringLocalizer.keyToString("HP_PROJECTS_TABLE_PROJECT_NAME_COLUMN_KEY"));
    }
  }
  
  /**
   * 
   * @param columnName
   * @return 
   */
  public int getDefaultColumnIndex(String columnName)
  {
    Assert.expect(columnName != null);
    Pair<Integer,Boolean> columnInfo = _defaultColumnInfoMap.get(columnName);
    return columnInfo.getFirst();
  }
}
