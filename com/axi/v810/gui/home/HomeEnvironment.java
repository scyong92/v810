package com.axi.v810.gui.home;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;

/**
 * <p>Title: HomeEnvironment</p>
 *
 * <p>Description: The HomeEnvironment class supports a single task -
 * the system and project summaries in the HomePanel.
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class HomeEnvironment extends AbstractEnvironmentPanel
{
  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getHomeInstance();
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private TaskPanelScreenEnum _currentScreen = null;
  private HomePanel _homePanel = null;
  private JMenu _homeMenu = new JMenu();

  // Not user visible strings -- used to switch in the CardLayout between different tasks
  private final static String _AXI_HOME = "Home";

  /**
   * @author George Booth
   */
  public HomeEnvironment(MainMenuGui mainUI)
  {
    super(mainUI);
    // single task panel requires simple navigation but requires
    // task panel to be initialized
    _navigationPanel = new HomeNavigationPanel(this);

    jbInit();
  }

  /**
   * @author Andy Mechtenberg
   */
  public com.axi.v810.gui.undo.CommandManager getCommandManager()
  {
    return _commandManager;
  }

  /**
   * @author George Booth
   */
  private void jbInit()
  {
    // initialize panels
    _homePanel = new HomePanel(this);
    _centerPanel.add(_homePanel, _AXI_HOME);
    _mainCardLayout.first(_centerPanel);

    _currentScreen = TaskPanelScreenEnum.NULL_SCREEN;
  }

  /**
   * change to a different screen.
   * @author George Booth
   */
  public void switchToScreen(TaskPanelScreenEnum screen)
  {
    Assert.expect(screen != null);
    Assert.expect(screen.equals(screen.AXI_HOME));
    switchToTask(_centerPanel, _homePanel, _AXI_HOME);
  }

  /**
   * called when a navigation button is clicked. Called directly here since
   * there is only one panel and no nav buttons
   * @author George Booth
   */
  public void navButtonClicked(String buttonName)
  {
    // buttonName will be "" for single task
    TaskPanelChangeScreenCommand command = new TaskPanelChangeScreenCommand(this, _currentScreen,
        TaskPanelScreenEnum.AXI_HOME);
    _commandManager.execute(command);
    _currentScreen = TaskPanelScreenEnum.AXI_HOME;
    _guiObservable.stateChanged(null, TaskPanelScreenEnum.AXI_HOME);
  }

  /**
   * @author George Booth
   */
  JMenu getHomeMenu()
  {
    return _homeMenu;
  }

  /**
   * @author George Booth
   */
  public void updatePanelData()
  {
    if (_homePanel != null)
    {
      _homePanel.updatePanelData();
    }
  }

  /**
   * @author George Booth
   */
  public void closeProject()
  {
    if (_homePanel != null)
    {
      _homePanel.closeProject();
    }
  }

  /**
   * @author George Booth
   */
  public void projectWasDeleted(String projectName)
  {
    if (_homePanel != null)
    {
      _homePanel.projectWasDeleted(projectName);
    }
  }

  /**
   * @author George Booth
   */
  public void xRayTesterExceptionThrown()
  {
    _homePanel.xRayTesterExceptionThrown();
  }

  /**
   * @author George Booth
   */
  public void systemRequestCompleted()
  {
    _homePanel.systemRequestCompleted();
  }


   /**
   * The environment is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("HomeEnvironment().start()");

    super.startMenusAndToolBar();

    // use the common status bar
    _mainUI.addStatusBar();

    _mainUI.setStatusBarText("");

    // turn on the tool bar for this environment
    addToolBar();

    // start the task
    _navigationPanel.selectDefaultTask();
  }

  /**
   * User has requested an environment change. Is it OK to leave this environment?
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
//    System.out.println("HomeEnvironment().isReadyToFinish()");
    // terminate current panel?
    if (currentTaskPanelReadyToFinish() == false)
    {
      return false;
    }
    // is environment ready to finish?
    if (dataDirty())
    {
      // prompt user to save data
      return true;
    }
    else
    {
      return true;
    }
  }

  /**
   * The environment is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    // finish current task
    finishCurrentTaskPanel();
    // allow restart with default task even if it was the current task
    _currentScreen = TaskPanelScreenEnum.NULL_SCREEN;
    _navigationPanel.clearCurrentNavigation();

//    System.out.println("HomeEnvironment().finish()\n");

    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
    // remove status bar
    _mainUI.removeStatusBar();
  }
}
