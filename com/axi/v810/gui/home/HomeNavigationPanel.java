package com.axi.v810.gui.home;

import com.axi.v810.gui.mainMenu.*;

/**
 *
 * <p>Title: HomeNavigationPanel</p>
 *
 * <p>Description: Extends AbstractNavigationPanel for use in the Home Environment</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class HomeNavigationPanel extends AbstractNavigationPanel
{
  /**
   * @author George Booth
   */
  HomeNavigationPanel(AbstractEnvironmentPanel envPanel)
  {
    super(envPanel);
  }
}
