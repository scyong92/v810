package com.axi.v810.gui.home;

import java.text.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: AvailableProjectData</p>
 *
 * <p>Description:
 * This class contains the table data and and detailed summary data for one entry in the
 * Available Projects or Projects Database tables in the AXI Home panel. The data comes
 * from the project summary file or from the current project loaded in the datastore.</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class AvailableProjectData
{
  private String _projectPath = null;

  // Primary data:
  private String _projectName = null;
  private String _customerName = null;
  private String _version = null;
  private String _modificationTime = null;
  private long _modificationTimeInMillis = -1;

  // Summary data:
  private String _programmerName = null;
  //   number of board type not used in version 1
  private int _numberOfBoardTypes = -1;
  private int _numberOfBoards = -1;
  private int _numberOfComponents = -1;
  private int _numberOfJoints = -1;
  private int _numberOfTestedJoints = -1;
  private int _numberOfUnTestableJoints = -1; //XCR1374, KhangWah, 2011-09-12
  private double _testCoverage = -1.0;
  //   estimated test time not used in version 1
  private double _estimatedTestTime = -1.0;

  // Project notes
  private String _projectNotes = null;
  // Project database comment
  private String _databaseComment = null;
  // project database version
  private int _databaseVersion = 0;

  private DateFormat _dateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
  private String _systemType = null;
  private java.util.List<String> _variationNames = new java.util.ArrayList<String> ();


  /**
   * constructor with ProjectSummary object
   * number of board types not used in version 1
   * estimated test time not used in version 1
   * @author George Booth
   */
  public AvailableProjectData(String projectName, ProjectSummary projectSummary) throws DatastoreException
  {
    Assert.expect(projectName != null);
    Assert.expect(projectSummary != null);

    setProjectPath(Directory.getProjectDir(projectName));
    setProjectName(projectName);
    setCustomerName(projectSummary.getTargetCustomerName());
    setVersion((new Double(projectSummary.getVersion())).toString());
    long date = projectSummary.getLastModificationTimeInMillis();
    _modificationTimeInMillis = date;
    setModificationTime(_dateFormat.format(date));

    setProgrammerName(projectSummary.getProgrammerName());
    setSystemType(projectSummary.getSystemType());
//    setNumberOfBoardTypes(projectSummary.getNumBoardTypes());
    setNumberOfBoards(projectSummary.getNumBoards());
    setNumberOfComponents(projectSummary.getNumComponents());
    setNumberOfJoints(projectSummary.getNumJoints());
    setNumberOfTestedJoints(projectSummary.getNumInspectedJoints());
    setNumberOfUnTestableJoints(projectSummary.getNumUnTestableJoints()); //XCR1374, KhangWah, 2011-09-12
    setTestCoverage(projectSummary.getJointTestCoveragePercent());
//    setEstimatedTestTime(getNumberOfTestedJoints() / 87.0);
    setProjectNotes(projectSummary.getProjectNotes());
    setDatabaseComment("");
    // Kok Chun, Tan - XCR-2519
    // set project name to system type map, so that we can get the system type before the project is loaded.
    //Khaw Chek Hau - XCR3260 : When open recipe, "system type different and v810 need to convert this recipe" message always prompt even recipe same system type with machine
    if (projectSummary.getDatabaseVersion() == 0)
      Project.setProjectNameToSystemType(_projectName, _systemType);
    //set variation names
    java.util.List<String> variations = VariationSettingManager.getProjectNameToVariationNamesMap(getProjectName());
    setVariationNames(variations);
  }

  /**
   * number of board types not used in version 1
   * estimated test time not used in version 1
   * constructor with current datastore values
   * @author George Booth
   */
  public AvailableProjectData(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    String projectName = project.getName();
    setProjectName(projectName);
    setProjectPath(Directory.getProjectDir(projectName));
    setCustomerName(project.getTargetCustomerName());
    setVersion((new Double(project.getVersion())).toString());
    long date = project.getLastModificationTimeInMillis();
    _modificationTimeInMillis = date;
    setModificationTime(_dateFormat.format(date));

    setProgrammerName(project.getProgrammerName());
    setSystemType(project.getSystemType().getName());
    Panel panel = project.getPanel();
//     setNumberOfBoardTypes(panel.getBoardTypes().size());
    setNumberOfBoards(panel.getBoards().size());
    setNumberOfComponents(panel.getNumComponents());
    setNumberOfJoints(panel.getNumJoints());
    //XCR1374, KhangWah, 2011-09-12, gather information of inspected joint and untestable joint.
    panel.gatherJointsCountDetail();
    setNumberOfTestedJoints(panel.getNumInspectedJointsFast()); //XCR1374, KhangWah, 2011-09-12
    setNumberOfUnTestableJoints(panel.getNumUnTestableJointsFast()); //XCR1374, KhangWah, 2011-09-12
    setTestCoverage(100.0 * (double)getNumberOfTestedJoints() / (double)getNumberOfJoints());
//    setEstimatedTestTime(getNumberOfTestedJoints() / 87.0);
    setProjectNotes(project.getNotes());
    setDatabaseComment("");
    // Kok Chun, Tan - XCR-2519
    // set project name to system type map, so that we can get the system type before the project is loaded.
    Project.setProjectNameToSystemType(_projectName, _systemType);
    //set variation names
    java.util.List<String> variations = VariationSettingManager.getProjectNameToVariationNamesMap(getProjectName());
    setVariationNames(variations);
  }

  /**
   * @author George Booth
   */
  void setProjectPath(String projectPath)
  {
    Assert.expect(projectPath != null);
    _projectPath = projectPath;
  }

  /**
   * @author George Booth
   */
  String getProjectPath()
  {
    Assert.expect(_projectPath != null);
    return _projectPath;
  }

  /**
   * @author George Booth
   */
  void setProjectName(String projectName)
  {
    Assert.expect(projectName != null);
    _projectName = projectName;
  }

  /**
   * @author George Booth
   */
  public String getProjectName()
  {
    Assert.expect(_projectName != null);
    return _projectName;
  }

  /**
   * @author George Booth
   */
  void setCustomerName(String customerName)
  {
    Assert.expect(customerName != null);
    _customerName = customerName;
  }

  /**
   * @author George Booth
   */
  public String getCustomerName()
  {
    Assert.expect(_customerName != null);
    if (_customerName.trim().length() == 0)
      return StringLocalizer.keyToString("HP_NO_CUSTOMER_NAME_KEY");
    else
      return _customerName;
  }

  /**
   * @author George Booth
   */
  void setVersion(String version)
  {
    Assert.expect(version != null);
    _version = version;
  }

  /**
   * @author George Booth
   */
  public String getVersion()
  {
    Assert.expect(_version != null);
    return _version;
  }

  /**
   * @author George Booth
   */
  void setModificationTime(String modificationTime)
  {
    Assert.expect(modificationTime != null);
    _modificationTime = modificationTime;
  }

  /**
   * @author George Booth
   */
  public String getModificationTime()
  {
    Assert.expect(_modificationTime != null);
    return _modificationTime;
  }

  /**
   * @author George Booth
   */
  public long getModificationTimeInMillis()
  {
    Assert.expect(_modificationTimeInMillis > -1);
    return _modificationTimeInMillis;
  }

  /**
   * @author George Booth
   */
  void setProgrammerName(String programmerName)
  {
    Assert.expect(programmerName != null);
    _programmerName = programmerName;
  }

  /**
   * @author George Booth
   */
  String getProgrammerName()
  {
    Assert.expect(_programmerName != null);
    if (_programmerName.trim().length() == 0)
      return StringLocalizer.keyToString("HP_NO_PROGRAMMER_NAME_KEY");
    else
      return _programmerName;
  }

  /**
   * number of board types not used in version 1
   * @author George Booth
   */
  void setNumberOfBoardTypes(int numberOfBoardTypes)
  {
    Assert.expect(numberOfBoardTypes > 0);
    _numberOfBoardTypes = numberOfBoardTypes;
  }

  /**
   * number of board types not used in version 1
   * @author George Booth
   */
  int getNumberOfBoardTypes()
  {
//    Assert.expect(_numberOfBoardTypes > 0);
    return _numberOfBoardTypes;
  }

  /**
   * @author George Booth
   */
  void setNumberOfBoards(int numberOfBoards)
  {
    Assert.expect(numberOfBoards >= 0);
    _numberOfBoards = numberOfBoards;
  }

  /**
   * @author George Booth
   */
  int getNumberOfBoards()
  {
    Assert.expect(_numberOfBoards >= 0);
    return _numberOfBoards;
  }

  /**
   * @author George Booth
   */
  void setNumberOfComponents(int numberOfComponents)
  {
    Assert.expect(numberOfComponents >= 0);
    _numberOfComponents = numberOfComponents;
  }

  /**
   * @author George Booth
   */
  int getNumberOfComponents()
  {
    Assert.expect(_numberOfComponents >= 0);
    return _numberOfComponents;
  }

  /**
   * @author George Booth
   */
  void setNumberOfJoints(int numberOfJoints)
  {
    Assert.expect(numberOfJoints >= 0);
    _numberOfJoints = numberOfJoints;
  }

  /**
   * @author George Booth
   */
  int getNumberOfJoints()
  {
    Assert.expect(_numberOfJoints >= 0);
    return _numberOfJoints;
  }

  /**
   * @author George Booth
   */
  void setNumberOfTestedJoints(int numberOfTestedJoints)
  {
    // tested joints can be 0 if all joints are untestable
    Assert.expect(numberOfTestedJoints >= 0);
    _numberOfTestedJoints = numberOfTestedJoints;
  }

  /**
   * @author George Booth
   */
  int getNumberOfTestedJoints()
  {
    // tested joints can be 0 if all joints are untestable
    Assert.expect(_numberOfTestedJoints >= 0);
    return _numberOfTestedJoints;
  }
  
  /**
   * @author Chnee Khang Wah, XCR1374
   */
  void setNumberOfUnTestableJoints(int numberOfUnTestableJoints)
  {
    // tested joints can be 0 if all joints are untestable
    Assert.expect(numberOfUnTestableJoints >= 0);
    _numberOfUnTestableJoints = numberOfUnTestableJoints;
  }
  
  /**
   * @author Chnee Khang Wah, XCR1374
   */
  int getNumberOfUnTestableJoints()
  {
    // tested joints can be 0 if all joints are untestable
    Assert.expect(_numberOfUnTestableJoints >= 0);
    return _numberOfUnTestableJoints;
  }

  /**
   * @author George Booth
   */
  void setTestCoverage(double testCoverage)
  {
    if (testCoverage >= 0.0 && testCoverage <= 100.0)
    {
      _testCoverage = testCoverage;
    }
    else
    {
      _testCoverage = 0.0;
    }
  }

  /**
   * @author George Booth
   */
  double getTestCoverage()
  {
    Assert.expect(_testCoverage >= 0.0 && _testCoverage <= 100.0);
    return _testCoverage;
  }

  /**
   * estimated test time not used in version 1
   * @author George Booth
   */
  void setEstimatedTestTime(double estimatedTestTime)
  {
    Assert.expect(estimatedTestTime >= 0.0);
    _estimatedTestTime = estimatedTestTime;
  }

  /**
   * estimated test time not used in version 1
   * @author George Booth
   */
  double getEstimatedTestTime()
  {
//    Assert.expect(_estimatedTestTime >= 0.0);
    return _estimatedTestTime;
  }

  /**
   * @author George Booth
   */
  void setProjectNotes(String projectNotes)
  {
    Assert.expect(projectNotes != null);
    _projectNotes = projectNotes;
  }

  /**
   * @author George Booth
   */
  String getProjectNotes()
  {
    Assert.expect(_projectNotes != null);
    if (_projectNotes.trim().length() == 0)
      return StringLocalizer.keyToString("HP_NO_PROJECT_NOTES_KEY");
    else
      return _projectNotes;
  }

  /**
   * @author George Booth
   */
  void setDatabaseComment(String databaseComment)
  {
    Assert.expect(databaseComment != null);
    _databaseComment = databaseComment;
  }

  /**
   * @author George Booth
   */
  String getDatabaseComment()
  {
    Assert.expect(_databaseComment != null);
    if (_databaseComment.trim().length() == 0)
      return StringLocalizer.keyToString("HP_NO_DATABSE_COMMENTS_KEY");
    else
      return _databaseComment;
  }

  /**
   * @author George Booth
   */
  void setDatabaseVersion(int databaseVersion)
  {
    _databaseVersion = databaseVersion;
  }

  /**
   * @author George Booth
   */
  public int getDatabaseVersion()
  {
    return _databaseVersion;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setSystemType(String systemType)
  {
    Assert.expect(systemType != null);
    _systemType = systemType;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public String getSystemType()
  {
    Assert.expect(_systemType != null);
    return _systemType;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setVariationNames(java.util.List<String> variations)
  {
    Assert.expect(variations != null);
    
    _variationNames.clear();
    _variationNames.addAll(variations);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public String[] getVariationNames()
  {
    Assert.expect(_variationNames != null);
    return _variationNames.toArray(new String[_variationNames.size()]);
  }
}
