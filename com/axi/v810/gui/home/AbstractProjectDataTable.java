package com.axi.v810.gui.home;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.testDev.*;

/**
 *
 * <p>Title: AbstractProjectDataTable</p>
 *
 * <p>Description: This class displays the information for all the data shown in the project summary tables</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */
public abstract class AbstractProjectDataTable extends JSortTable
{
  //percentage of space in the table given to each column (all add up to 1.0)
  protected static final double _PROJECT_NUMBER_COLUMN_WIDTH_PERCENTAGE = 0.05;
  protected static final double _PROJECT_NAME_COLUMN_WIDTH_PERCENTAGE = 0.30;
  protected static final double _PROJECT_VARIATION_NAME_COLUMN_WIDTH_PERCENTAGE = 0.15;
  protected static final double _CUSTOMER_NAME_COLUMN_WIDTH_PERCENTAGE = 0.15;
  protected static final double _VERSION_COLUMN_WIDTH_PERCENTAGE = 0.10;
  protected static final double _LAST_MODIFIED_COLUMN_WIDTH_PERCENTAGE = 0.15;
  protected static final double _PROJECT_SYSTEM_TYPE_COLUMN_WIDTH_PERCENTAGE = 0.10;

  private boolean _modelSet = false;

  /**
   * @author George Booth
   */
  public AbstractProjectDataTable()
  {
    setRowSelectionAllowed(true);
    setColumnSelectionAllowed(false);
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  }

  abstract void setPreferredColumnWidths(int columnWidths[]);

  /**
   * Get each column's current width so they can be saved in user preferences
   * @author George Booth
   */
  public int[] getColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int columnWidths[] = new int[getColumnCount()];
    for (int i = 0; i < getColumnCount(); i++)
    {
      columnWidths[i] = columnModel.getColumn(i).getWidth();
    }
    return columnWidths;
  }

  /**
   * @author George Booth
   */
  public void clearSelection()
  {
    TableModel model = getModel();
    // this function gets called before the table model is set, during intialization. At that time,
    // the model will be a defaultTableModel so the assert is sure to fail
    if (_modelSet)
    {
      Assert.expect(model instanceof NewComponentTableModel);
      ((NewComponentTableModel)model).clearSelectedComponents();
    }
    super.clearSelection();
  }

  /**
   * @author George Booth
   */
  public void setModel(NewComponentTableModel model)
  {
   Assert.expect(model != null);
   _modelSet = true;
   super.setModel(model);
  }
}
