package com.axi.v810.gui.home;

import javax.swing.table.*;
import java.util.*;

import com.axi.guiUtil.*;
import com.axi.v810.util.*;
import java.awt.event.*;

/**
 *
 * <p>Title: AvailableProjectsTable</p>
 *
 * <p>Description: Implements AbstractProjectDataTable for the Available Projects table.</p>
 * <p>This class is the Available Projects table in the AXI Home panel and
 * displays the summary data for projects in the local "projects" directory.
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */
class AvailableProjectsTable extends AbstractProjectDataTable
{
  private static int _index = -1;
  private static final int _PROJECT_NUMBER_INDEX = ++_index;
  private static final int _PROJECT_NAME_INDEX = ++_index;
  private static final int _PROJECT_VARIATION_NAME = ++_index;
  private static final int _CUSTOMER_NAME_INDEX = ++_index;
  private static final int _VERSION_INDEX = ++_index;
  private static final int _LAST_MODIFIED_INDEX = ++_index;
  private static final int _PROJECT_SYSTEM_TYPE = ++_index;
  
  private static final int _MAXIMUM_COLUMNS_SIZE = 7;
  
  public AvailableProjectsTable()
  {
    super();
  }
  
  /**
   * added by sheng chuan to add new numbering column in table
   * @return 
   */
  public int getSortingColumn()
  {
    return 1;
  }
  
  /**
   * Sets each column's width based defaults or the users last setting
   * @author George Booth
   */
  public void setPreferredColumnWidths(int columnWidths[])
  {
    TableColumnModel columnModel = getColumnModel();

    if (columnWidths == null)
    {
      if (getColumnCount() == _MAXIMUM_COLUMNS_SIZE)
      {
      // use default widths
        int totalColumnWidth = columnModel.getTotalColumnWidth();

        columnModel.getColumn(_PROJECT_NUMBER_INDEX).setPreferredWidth((int)(totalColumnWidth *
            _PROJECT_NUMBER_COLUMN_WIDTH_PERCENTAGE));
        columnModel.getColumn(_PROJECT_NAME_INDEX).setPreferredWidth((int)(totalColumnWidth *
            _PROJECT_NAME_COLUMN_WIDTH_PERCENTAGE));
        columnModel.getColumn(_PROJECT_VARIATION_NAME).setPreferredWidth((int)(totalColumnWidth *
            _PROJECT_VARIATION_NAME_COLUMN_WIDTH_PERCENTAGE));
        columnModel.getColumn(_CUSTOMER_NAME_INDEX).setPreferredWidth((int)(totalColumnWidth *
            _CUSTOMER_NAME_COLUMN_WIDTH_PERCENTAGE));
        columnModel.getColumn(_VERSION_INDEX).setPreferredWidth((int)(totalColumnWidth *
            _VERSION_COLUMN_WIDTH_PERCENTAGE));
        columnModel.getColumn(_LAST_MODIFIED_INDEX).setPreferredWidth((int)(totalColumnWidth *
            _LAST_MODIFIED_COLUMN_WIDTH_PERCENTAGE));
        columnModel.getColumn(_PROJECT_SYSTEM_TYPE).setPreferredWidth((int)(totalColumnWidth *
            _PROJECT_SYSTEM_TYPE_COLUMN_WIDTH_PERCENTAGE));
      }
    }
    else
    { 
      // use last settings
      if (getColumnCount()== columnWidths.length)
      {
        for (int i = 0; i < getColumnCount(); i++)
        {
          columnModel.getColumn(i).setPreferredWidth(columnWidths[i]);
        }       
      }
      else
      {
        if (getColumnCount() == _MAXIMUM_COLUMNS_SIZE )
        {
          int totalColumnWidth = columnModel.getTotalColumnWidth();

          columnModel.getColumn(_PROJECT_NUMBER_INDEX).setPreferredWidth((int)(totalColumnWidth *
              _PROJECT_NUMBER_COLUMN_WIDTH_PERCENTAGE));
          columnModel.getColumn(_PROJECT_NAME_INDEX).setPreferredWidth((int)(totalColumnWidth *
              _PROJECT_NAME_COLUMN_WIDTH_PERCENTAGE));
          columnModel.getColumn(_PROJECT_VARIATION_NAME).setPreferredWidth((int)(totalColumnWidth *
              _PROJECT_VARIATION_NAME_COLUMN_WIDTH_PERCENTAGE));
          columnModel.getColumn(_CUSTOMER_NAME_INDEX).setPreferredWidth((int)(totalColumnWidth *
              _CUSTOMER_NAME_COLUMN_WIDTH_PERCENTAGE));
          columnModel.getColumn(_VERSION_INDEX).setPreferredWidth((int)(totalColumnWidth *
              _VERSION_COLUMN_WIDTH_PERCENTAGE));
          columnModel.getColumn(_LAST_MODIFIED_INDEX).setPreferredWidth((int)(totalColumnWidth *
              _LAST_MODIFIED_COLUMN_WIDTH_PERCENTAGE));
          columnModel.getColumn(_PROJECT_SYSTEM_TYPE).setPreferredWidth((int)(totalColumnWidth *
              _PROJECT_SYSTEM_TYPE_COLUMN_WIDTH_PERCENTAGE));
          
        }
      }
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();
    int index = getColumnIndexIfExist(StringLocalizer.keyToString("HP_PROJECTS_TABLE_VARIATION_NAME_KEY"));
    if (index >= 0)
    {
      columnModel.getColumn(index).setCellRenderer(new MultipleLineCellRenderer());
    }
  }

  /**
   * @author weng-jian.eoh
   * @param colName
   * @return 
   */
  private int getColumnIndexIfExist(String colName)
  {
    for (int i = 0; i < getColumnCount(); i++)
    {
      if (getColumnName(i).equalsIgnoreCase(colName))
      {
        return i;
      }
    }
    return -1;

  }
  /**
   * @author weng-jian.eoh
   * @param row
   * @param column
   * @return 
   */
  public Object getValueAt(int row, int column)
  {
    int origColumnIndex;
    String col = getColumnName(column);
    AvailableProjectsTableModel tableModel = (AvailableProjectsTableModel)getModel();
   origColumnIndex= tableModel.getDefaultColumnIndex(col);
    return tableModel.getValueAt(row, origColumnIndex);
  }
  
  /**
   * @author weng-jian.eoh
   * @param event 
   */
  public void mouseReleased(MouseEvent event)
  {
    if (_resizing)
      return;
    TableColumnModel colModel = getColumnModel();
    int index = colModel.getColumnIndexAtX(event.getX());
    // sometimes this returns -1 when the mouse isn't over column, but was earlier
    if (index < 0)
      return;
    int modelIndex = colModel.getColumn(index).getModelIndex();
    
    SortTableModel model = (SortTableModel)getModel();
    if (model.isSortable(modelIndex))
    {
      // toggle ascension, if already sorted
      if (sortedColumnIndex == index)
      {
        sortedColumnAscending = !sortedColumnAscending;
      }
      sortedColumnIndex = index;
      String col = getColumnName(modelIndex);
      AvailableProjectsTableModel AvailableProjectsTable = (AvailableProjectsTableModel) getModel();
      modelIndex = AvailableProjectsTable.getDefaultColumnIndex(col);
      model.sortColumn(modelIndex, sortedColumnAscending);
      if (getModel() instanceof AbstractTableModel)
      {
        AbstractTableModel m = (AbstractTableModel)getModel();
        m.fireTableDataChanged();
        getTableHeader().repaint();
      }
    }
  }
}
