package com.axi.v810.gui.home;

import java.util.*;

import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * <p>Title: AbstractProjectDataTableModel</p>
 *
 * <p>Description: Abstract class for project data table models used in the AXI Home panel.
 * </p> There are 4 columns:<br>
 * Project Name<br>
 * Customer Name<br>
 * Project Version<br>
 * Last Modification<br>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public abstract class  AbstractProjectDataTableModel extends DefaultSortTableModel
{
  protected ProjectSummaryReader _projectSummaryReader = null;
  protected List<AvailableProjectData> _projectData;

  // default to sort by component, ascending
  protected int _currentSortColumn = 0;
  protected boolean _currentSortAscending = true;

  public AbstractProjectDataTableModel()
  {
    _projectSummaryReader = ProjectSummaryReader.getInstance();
  }

  /**
   * reset to default table sort
   * @author George Booth
   */
  void clear()
  {
    _currentSortColumn = 0;
    _currentSortAscending = true;
  }

  /**
   * The instantiating table model must provide a method to get project data
   * @author George Booth
   */
  abstract void populateTableData();

  /**
   * @author George Booth
   */
  AvailableProjectData getSummaryData(int row)
  {
    Assert.expect(row > -1 && row < _projectData.size());

    return _projectData.get(row);
  }

  /**
   * @return index of project (-1 if not found)
   * @author George Booth
   */
  int getProjectIndex(String projectName)
  {
    Assert.expect(projectName != null);

    int index = -1;
    for (AvailableProjectData data : _projectData)
    {
      index++;
      String thisName = data.getProjectName();
      // project names are case insensitive
      if (thisName.equalsIgnoreCase(projectName))
      {
        return index;
      }
    }
    return -1;
  }

  /**
   * Replace current data at index with new data
   * @author George Booth
   */
  void updateSummaryData(int row, AvailableProjectData data)
  {
    Assert.expect(row > -1 && row < _projectData.size());
    Assert.expect(data != null);

    _projectData.remove(row);
    if (data.getSystemType().equals(SystemTypeEnum.THROUGHPUT.getName()))
    {
      data.setSystemType(SystemTypeEnum.STANDARD.getName().toUpperCase());
    }
    _projectData.add(row, data);
    fireTableDataChanged();
  }

  /**
   * @author George Booth
   */
  public boolean isSortable(int column)
  {
    Assert.expect(column > -1 && column < getColumnCount());

    return true;
  }

  /**
   * @author George Booth
   */
  public void sortColumn(int column, boolean ascending)
  {
    Assert.expect(column > -1 && column < 7);
    _currentSortColumn = column;
    _currentSortAscending = ascending;

    switch (column)
    {
      case 0:
        break;
      case 1: // project name (sort database version first to get correct final order)
        Collections.sort(_projectData, new
          AvailableProjectDataComparator(ascending, AvailableProjectDataComparator.DATABASE_VERSION));
        Collections.sort(_projectData, new
          AvailableProjectDataComparator(ascending, AvailableProjectDataComparator.PROJECT_NAME));
        break;
      case 2: //variation name
        break;
      case 3: // customer name
        Collections.sort(_projectData, new
          AvailableProjectDataComparator(ascending, AvailableProjectDataComparator.CUSTOMER_NAME));
        break;
      case 4: // version
        Collections.sort(_projectData, new
          AvailableProjectDataComparator(ascending, AvailableProjectDataComparator.VERSION));
        break;
      case 5: // modification time
        Collections.sort(_projectData, new
          AvailableProjectDataComparator(ascending, AvailableProjectDataComparator.LAST_MOD));
        break;
      case 6: // system type
        Collections.sort(_projectData, new
          AvailableProjectDataComparator(ascending, AvailableProjectDataComparator.SYSTEM_TYPE));
        break;
      default:
        // this should not happen so assert
        Assert.expect(false);
        return;
    }
  }

  /**
   * @author George Booth
   */
  public int getRowCount()
  {
    if (_projectData == null)
    {
      return 0;
    }
    else
    {
      return _projectData.size();
    }
  }

  /**
   * @author George Booth
   */
  public boolean isCellEditable(int row, int column)
  {
    Assert.expect(row > -1 && row < _projectData.size());
    Assert.expect(column > -1 && column < getColumnCount());

    // the table can not be edited.
    return false;
  }

  /**
   * @author George Booth
   */
  public Object getValueAt(int row, int column)
  {
    Assert.expect(row > -1 && row < _projectData.size());
    Assert.expect(column > -1 && column < 7);

    AvailableProjectData projectData = _projectData.get(row);
    switch(column)
    {
      case 0: return row + 1;
      case 1: return projectData.getProjectName();
      case 2: return projectData.getVariationNames();
      case 3: return projectData.getCustomerName();
      case 4: return projectData.getVersion();
      case 5: return projectData.getModificationTime();
      case 6: return projectData.getSystemType();
      default:
        // this should not happen so assert
        Assert.expect(false);
        return null;
    }
  }

  /**
   * @author George Booth
   */
  public void setValueAt(Object aValue, int row, int column)
  {
    // do nothing
  }

  public int getRowForProject(String projectName)
  {
    // are there any projects in the table?
    if (_projectData == null || _projectData.size() < 1)
      return -1;

    if (projectName == null)
    {
      return -1;
    }
    else
    {
      for (int row = 0; row < _projectData.size(); row++)
      {
        AvailableProjectData projectData = _projectData.get(row);
        if (projectData.getProjectName().equalsIgnoreCase(projectName))
        {
          return row;
        }
      }
    }
    // not found
    Assert.expect(false);
    return -1;
  }

  /**
   * @author George Booth
   */
  public String getProjectNameAt(int row)
  {
    return (String)getValueAt(row, 1);
  }

  /**
   * @author George Booth
   */
  public int getRowForDataStartingWith(String key)
  {
    key = key.toLowerCase();
    int index = -1;
    for (AvailableProjectData projectData : _projectData)
    {
      index++;
      String projectName = projectData.getProjectName().toLowerCase();
      if (projectName.startsWith(key))
      {
        return index;
      }
    }
    return -1;
  }

}
