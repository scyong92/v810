package com.axi.v810.gui.home;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: ProjectDatabaseTableModel</p>
 *
 * <p>Description: </p>
 * This class is the table model for the Project Database table in AXI Home panel.
 * This model gets the summary data for projects in the project database.</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class ProjectDatabaseTableModel extends AbstractProjectDataTableModel
{  
  private boolean _isCustomizeDisplayColumnOn = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_MAIN_UI);
  private boolean _isShowNumbering = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_NUMBERING);
  private boolean _isShowRecipeName = true;
  private boolean _isShowCustomerName = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_CUSTOMER_NAME);
  private boolean _isShowVersion = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_VERSION);
  private boolean _isShowLastModifiedDate = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_MODIFIED_DATE);
  private LinkedList<String> _projectDataTableColumnNames = new LinkedList<String>();
  
  private HashMap<String, Pair<Integer, Boolean>> _defaultColumnInfoMap = new LinkedHashMap<String, Pair<Integer, Boolean>>()
  {
    {
      put(StringLocalizer.keyToString("HP_PROJECTS_TABLE_PROJECT_NO_COLUMN_KEY"), new Pair<Integer, Boolean>(0, _isShowNumbering));
      put(StringLocalizer.keyToString("HP_PROJECTS_TABLE_PROJECT_NAME_COLUMN_KEY"), new Pair<Integer, Boolean>(1, _isShowRecipeName));
      put(StringLocalizer.keyToString("HP_PROJECTS_TABLE_CUSTOMER_COLUMN_KEY"), new Pair<Integer, Boolean>(2, _isShowCustomerName));
      put(StringLocalizer.keyToString("HP_PROJECTS_TABLE_VERSION_COLUMN_KEY"), new Pair<Integer, Boolean>(3, _isShowVersion));
      put(StringLocalizer.keyToString("HP_PROJECTS_TABLE_LAST_MODIFIED_COLUMN_KEY"), new Pair<Integer, Boolean>(4, _isShowLastModifiedDate));
    }
  };
  /**
   * @author George Booth
   */
  public ProjectDatabaseTableModel()
  {
    super();
    setupDisplayColumn();
  }
  
  /**
   * @author George Booth
   */
  public int getColumnCount()
  {
    return _projectDataTableColumnNames.size();
  }

  /**
   * @author George Booth
   */
  public String getColumnName(int column)
  {
    Assert.expect(column > -1 && column < getColumnCount());

    return _projectDataTableColumnNames.get(column);
  }

  /**
   * @author George Booth
   */
  boolean isDatabaseValid()
  {
    // make sure database dir is valid
    String databasePath = getDatabasePath();
    File file = new File(databasePath);
    return file.exists() && file.isDirectory();
  }

  /**
   * @author George Booth
   */
  String getDatabasePath()
  {
    return Directory.getProjectDatabaseDir();
  }

  /**
   * @author George Booth
   */
  void populateTableData()
  {
    // get available projects
    _projectData = new LinkedList<AvailableProjectData>();

    // make sure database dir is valid
    if (isDatabaseValid() == false)
      return;

    try
    {
      List<ProjectSummary> projectSummaries = ProjectDatabase.getProjectSummaries();
      if (projectSummaries == null)
        return;
      for (ProjectSummary summary : projectSummaries)
      {
        AvailableProjectData projectData = new AvailableProjectData(summary.getProjectName(), summary);
        // unique data for project database
        projectData.setDatabaseComment(summary.getDatabaseComment());
        projectData.setDatabaseVersion(summary.getDatabaseVersion());
        if (projectData.getSystemType().equals(SystemTypeEnum.THROUGHPUT.getName()))
        {
          projectData.setSystemType(SystemTypeEnum.STANDARD.getName().toUpperCase());
        }
        _projectData.add(projectData);
      }
    }
    catch(DatastoreException dse)
    {
      MessageDialog.showErrorDialog(
          MainMenuGui.getInstance(),
          StringUtil.format(dse.getMessage(), 50),
          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
          true);
    }

    sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableDataChanged();
  }
  
  /**
   * Filter recipe names in Project Database Table.
   * @author Ying-Huan.Chu
   */
  void populateFilteredTableData(String filter)
  {
    // get available projects
    _projectData = new LinkedList<AvailableProjectData>();

    // make sure database dir is valid
    if (isDatabaseValid() == false)
      return;

    try
    {
      List<ProjectSummary> projectSummaries = ProjectDatabase.getProjectSummaries();
      if (projectSummaries == null)
        return;
      for (ProjectSummary summary : projectSummaries)
      {
        AvailableProjectData projectData = new AvailableProjectData(summary.getProjectName(), summary);
        // unique data for project database
        if (projectData.getProjectName().toLowerCase().startsWith(filter.toLowerCase()))
        {
          projectData.setDatabaseComment(summary.getDatabaseComment());
          projectData.setDatabaseVersion(summary.getDatabaseVersion());
          if (projectData.getSystemType().equals(SystemTypeEnum.THROUGHPUT.getName()))
          {
            projectData.setSystemType(SystemTypeEnum.STANDARD.getName().toUpperCase());
          }
          _projectData.add(projectData);
        }
      }
    }
    catch(DatastoreException dse)
    {
      MessageDialog.showErrorDialog(
          MainMenuGui.getInstance(),
          StringUtil.format(dse.getMessage(), 50),
          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
          true);
    }

    sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableDataChanged();
  }
  
  /**
   * @author George Booth
   */
  public Object getValueAt(int row, int column)
  {
    Assert.expect(row > -1 && row < _projectData.size());
    Assert.expect(column > -1 && column < 5);

    AvailableProjectData projectData = _projectData.get(row);
    switch(column)
    {
      case 0: return row + 1;
      case 1: return projectData.getProjectName() + "  (" + projectData.getDatabaseVersion() + ")";
      case 2: return projectData.getCustomerName();
      case 3: return projectData.getVersion();
      case 4: return projectData.getModificationTime();
      default:
        // this should not happen so assert
        Assert.expect(false);
        return null;
    }
  }

  /**
   * @author George Booth
   */
  int getNumberOfProjects()
  {
    if (_projectData == null)
    {
      return 0;
    }
    else
    {
      return _projectData.size();
    }
  }

  /**
   * @author George Booth
   */
  public String getProjectNameAt(int row)
  {
    AvailableProjectData projectData = _projectData.get(row);
    return projectData.getProjectName();
  }

  /**
   * @author George Booth
   */
  int getProjectVersionAt(int row)
  {
    AvailableProjectData projectData = _projectData.get(row);
    return projectData.getDatabaseVersion();
  }

  /**
   * @author George Booth
   */
  int getProjectIndex(String projectName, int version)
  {
    if (projectName == null)
    {
      return -1;
    }
    else
    {
      for (int row = 0; row < _projectData.size(); row++)
      {
        AvailableProjectData projectData = _projectData.get(row);
        if (projectData.getProjectName().equalsIgnoreCase(projectName) &&
            projectData.getDatabaseVersion() == version)
        {
          return row;
        }
      }
    }
    // not found
    return -1;
  }

  /**
   * @author George Booth
   */
  int getLastProjectVersion(String projectName)
  {
    int lastVersion = -1;
    if (projectName == null)
    {
      return lastVersion;
    }
    else
    {
      for (int row = 0; row < _projectData.size(); row++)
      {
        AvailableProjectData projectData = _projectData.get(row);
        if (projectData.getProjectName().equalsIgnoreCase(projectName) &&
            projectData.getDatabaseVersion() > lastVersion)
        {
          lastVersion = projectData.getDatabaseVersion();
        }
      }
    }
    return lastVersion;
  }
  
  /**
   * XCR-3436
   * @author weng-jian.eoh
   */
  private void setupDisplayColumn()
  {
    try
    {
      CustomizationSettingReader.getInstance().load(FileName.getCustomizeConfigurationFullPath(Config.getInstance().getStringValue(SoftwareConfigEnum.CUSTOMIZE_SETTING_CONFIGURATION_NAME_TO_USE)));
    }
    catch (DatastoreException ex)
    {
      MainMenuGui mainUI = MainMenuGui.getInstance();
          MessageDialog.showErrorDialog(
              mainUI,
              StringUtil.format(ex.getMessage(), 50),
              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
              true);
    }
    
    _isCustomizeDisplayColumnOn = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_MAIN_UI);
    _isShowNumbering = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_NUMBERING);
    _isShowRecipeName = true;
    _isShowCustomerName = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_CUSTOMER_NAME);
    _isShowVersion = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_VERSION);
    _isShowLastModifiedDate = Config.getInstance().getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_MODIFIED_DATE);
    
    boolean[] columnDisplayFlag = {
      _isShowNumbering,
      _isShowRecipeName,
      _isShowCustomerName,
      _isShowVersion,
      _isShowLastModifiedDate,
    };
    
    ArrayList<String> keySet = new ArrayList<String>(_defaultColumnInfoMap.keySet());
    for (int i = 0; i <columnDisplayFlag.length;i++)
    {
      Pair<Integer,Boolean> columnInfo = _defaultColumnInfoMap.get(keySet.get(i));
      columnInfo.setSecond(columnDisplayFlag[i]);
      _defaultColumnInfoMap.put(keySet.get(i), columnInfo);
    }
    
    for (int i = 0; i <columnDisplayFlag.length;i++)
    {
      Pair<Integer,Boolean> columnInfo = _defaultColumnInfoMap.get(keySet.get(i));
      columnInfo.setSecond(columnDisplayFlag[i]);
      _defaultColumnInfoMap.put(keySet.get(i), columnInfo);
    }
    
    if (_isCustomizeDisplayColumnOn == true)
    {
      for (int i =0 ; i < _defaultColumnInfoMap.keySet().size(); i++)
      {
     
        Pair<Integer,Boolean> columnInfo = (Pair<Integer,Boolean>)_defaultColumnInfoMap.get(keySet.get(i));
        boolean isShow = columnInfo.getSecond();
        if (isShow)
        {
          String colName = keySet.get(i);
          _projectDataTableColumnNames.add(colName);
        }
      }
    }
    else
    {
      _projectDataTableColumnNames.add(StringLocalizer.keyToString("HP_PROJECTS_TABLE_PROJECT_NAME_COLUMN_KEY"));
    }
  }
 
  /**
   * 
   * @param columnName
   * @return 
   */
  public int getDefaultColumnIndex(String columnName)
  {
    Assert.expect(columnName != null);
    Pair<Integer,Boolean> columnInfo = _defaultColumnInfoMap.get(columnName);
    return columnInfo.getFirst();
  }
  
  /**
   * XCR-3531 Unable to sort Version and Modified Date in Project Database Panel
   * @author Cheah Lee Herng 
   */
  public void sortColumn(int column, boolean ascending)
  {
    Assert.expect(column > -1 && column < getColumnCount());
    _currentSortColumn = column;
    _currentSortAscending = ascending;

    switch (column)
    {
      case 0:
        break;
      case 1: // project name (sort database version first to get correct final order)
        Collections.sort(_projectData, new
          AvailableProjectDataComparator(ascending, AvailableProjectDataComparator.DATABASE_VERSION));
        Collections.sort(_projectData, new
          AvailableProjectDataComparator(ascending, AvailableProjectDataComparator.PROJECT_NAME));
        break;
      case 2: // customer name
        Collections.sort(_projectData, new
          AvailableProjectDataComparator(ascending, AvailableProjectDataComparator.CUSTOMER_NAME));
        break;
      case 3: // version
        Collections.sort(_projectData, new
          AvailableProjectDataComparator(ascending, AvailableProjectDataComparator.VERSION));
        break;
      case 4: // modification time
        Collections.sort(_projectData, new
          AvailableProjectDataComparator(ascending, AvailableProjectDataComparator.LAST_MOD));
        break;      
      default:
        // this should not happen so assert
        Assert.expect(false);
        return;
    }
  }
}
