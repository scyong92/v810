package com.axi.v810.gui;

/**
 * This class provides the ability to communicate long panel information to the
 * graphics engine.
 *
 * @author Andy Mechtenberg
 */
public class LongPanelAlignmentRegionEnum extends GuiEventEnum
{
  private static int _index = -1;

  public static LongPanelAlignmentRegionEnum TOP = new LongPanelAlignmentRegionEnum(++_index);
  public static LongPanelAlignmentRegionEnum BOTTOM = new LongPanelAlignmentRegionEnum(++_index);

  /**
   * @author Andy Mechtenberg
   */
  private LongPanelAlignmentRegionEnum(int id)
  {
    super(id);
  }
}

