package com.axi.v810.gui.mainMenu;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
 *
 * @author chin-seong.kee
 */
public class VirtualLiveDummyProjectSettingsDialog extends EscapeDialog
{
  private JPanel _dialogPanel;

  private JPanel _northPanel;
  private JLabel _warningLabel;
  private JLabel _promptLabel;

  private JPanel _centerPanel;
  private JPanel _labelPanel;
  private JLabel _dummyProjectWidthLabel;
  private JLabel _dummyProjectLengthLabel;
  private JLabel _dummyProjectThicknessLabel;
  private JPanel _entryPanel;
  private JTextField _panelWidthTextField;
  private JTextField _panelLengthTextField;
  private JTextField _panelThicknessTextField;

  private JPanel _southPanel;
  private JPanel _buttonPanel;
  private JButton _okButton;
  private JButton _cancelButton;

  private NumericRangePlainDocument _panelWidthDocument = new NumericRangePlainDocument();
  private NumericRangePlainDocument _panelLengthDocument = new NumericRangePlainDocument();
  private NumericRangePlainDocument _panelThicknessDocument = new NumericRangePlainDocument();
    
  private double _panelWidth = 0.0;
  private double _panelLength = 0.0;
  private double _panelThickness = 0.0;
  
  private boolean _ok = false;
  private MainMenuGui _mainUI = null; 
  
  //Kee Chin Seong - OKI requirement
  private JRadioButton _milsRadioButton = new JRadioButton();
  private JRadioButton _mmRadioButton = new JRadioButton();
  private JRadioButton _nanometerRadioButton = new JRadioButton();
  
  private ButtonGroup _measurementButtonGroup = new ButtonGroup();
  
  /**
   *  @author chin-seong.kee
   */
  public VirtualLiveDummyProjectSettingsDialog(JFrame parent)
  {
     super(parent, StringLocalizer.keyToString("VP_DUMMY_PROJECT_PROMPT_TITLE_KEY"), true);

     _mainUI = (MainMenuGui)parent;
     
     _dialogPanel = new JPanel(new BorderLayout(50, 10));
     _dialogPanel.setBorder(BorderFactory.createEmptyBorder(10, 30, 10, 30));
     getContentPane().add(_dialogPanel);

     _northPanel = new JPanel(new BorderLayout(10, 10));
     _dialogPanel.add(_northPanel, BorderLayout.NORTH);
     
     //Check the width length is within spec or not
     _warningLabel = new JLabel(" ");
     _warningLabel.setForeground(Color.RED);
     _warningLabel.setFont(FontUtil.getBoldFont(_warningLabel.getFont(),Localization.getLocale()));
     _warningLabel.setHorizontalAlignment(SwingConstants.CENTER);
     _northPanel.add(_warningLabel, BorderLayout.NORTH);

     _promptLabel = new JLabel(StringLocalizer.keyToString("VP_DUMMY_PROJECT_VALUE_INPUT_PROMPT_LABEL_KEY"));
     _northPanel.add(_promptLabel, BorderLayout.CENTER);
     _centerPanel = new JPanel();
     _centerPanel.setLayout(new BorderLayout(10, 10));
     _dialogPanel.add(_centerPanel, BorderLayout.CENTER);

     _labelPanel = new JPanel(new GridLayout(3, 1, 10, 10));
     _centerPanel.add(_labelPanel, BorderLayout.WEST);

     _dummyProjectWidthLabel = new JLabel(StringLocalizer.keyToString("VP_PANEL_WIDTH_KEY"));
     _labelPanel.add(_dummyProjectWidthLabel);
     _dummyProjectLengthLabel = new JLabel(StringLocalizer.keyToString("VP_PANEL_LENGTH_KEY"));
     _labelPanel.add(_dummyProjectLengthLabel);
     _dummyProjectThicknessLabel = new JLabel(StringLocalizer.keyToString("VP_PANEL_THICKNESS_KEY"));
     _labelPanel.add(_dummyProjectThicknessLabel);

     _entryPanel = new JPanel(new GridLayout(3, 1, 40, 10));
     _centerPanel.add(_entryPanel, BorderLayout.CENTER);

     _panelWidthTextField =  new JTextField();
     _entryPanel.add(_panelWidthTextField);
     _panelLengthTextField = new JTextField();
     _entryPanel.add(_panelLengthTextField);
     _panelThicknessTextField = new JTextField();
     _entryPanel.add(_panelThicknessTextField);

     _panelWidthDocument.setRange(new DoubleRange(0.0,Double.MAX_VALUE));
     _panelLengthDocument .setRange(new DoubleRange(0.0,Double.MAX_VALUE));
     
     _panelWidthTextField.setDocument(_panelWidthDocument);
     _panelLengthTextField.setDocument(_panelLengthDocument);
     _panelThicknessTextField.setDocument(_panelThicknessDocument);
     
     _dummyProjectWidthLabel.setPreferredSize(new Dimension(50, 25));
     _dummyProjectLengthLabel.setPreferredSize(new Dimension(50, 25));
     _dummyProjectThicknessLabel.setPreferredSize(new Dimension(75, 25));
     
     _panelWidthTextField.setPreferredSize(new Dimension(200, 25));
     _panelLengthTextField.setPreferredSize(new Dimension(200, 25));
     _panelThicknessTextField.setPreferredSize(new Dimension(200, 25));
     
     _panelWidthTextField.addKeyListener(new KeyAdapter()
     {
       public void keyReleased(KeyEvent e)
       {
         someKeyReleased(e);
       }
     });
     
     _panelLengthTextField.addKeyListener(new KeyAdapter()
     {
       public void keyReleased(KeyEvent e)
       {
         someKeyReleased(e);
       }
     });
     
     _panelThicknessTextField.addKeyListener(new KeyAdapter()
     {
       public void keyReleased(KeyEvent e)
       {
         someKeyReleased(e);
       }
     });
     
     _southPanel = new JPanel(new BorderLayout());
     _dialogPanel.add(_southPanel, BorderLayout.SOUTH);

     _buttonPanel = new JPanel(new FlowLayout());
     _southPanel.add(_buttonPanel, BorderLayout.CENTER);

     _okButton = new JButton(StringLocalizer.keyToString("VL_OK_BUTTON_KEY"));
     _okButton.addActionListener(new ActionListener()
     {
        public void actionPerformed(ActionEvent e)
        {
          _panelWidth = Double.parseDouble(_panelWidthTextField.getText());
          _panelLength = Double.parseDouble(_panelLengthTextField.getText());
          _panelThickness = Double.parseDouble(_panelThicknessTextField.getText());
          checkDummyValidSpec();
          if(_ok == true)
            dispose();
        }
     });
     _buttonPanel.add(_okButton);

     _cancelButton = new JButton(StringLocalizer.keyToString("VL_CANCEL_BUTTON_KEY"));
     _cancelButton.addActionListener(new ActionListener()
     {
        public void actionPerformed(ActionEvent e)
        {
          checkDummyValidSpec();
          dispose();
        }
      });
     _buttonPanel.add(_cancelButton);
     
     _measurementButtonGroup.add(_milsRadioButton);
     _measurementButtonGroup.add(_nanometerRadioButton);
     _measurementButtonGroup.add(_mmRadioButton);
     
     _mmRadioButton.setText("milimeters");
     _nanometerRadioButton.setText("nanometer");
     _milsRadioButton.setText("mils");
     
     JPanel choicePanel = new JPanel();
     
     FlowLayout flowLayout = new FlowLayout();
     flowLayout.setAlignment(FlowLayout.TRAILING);
     
     choicePanel.setLayout(flowLayout);
     choicePanel.setComponentOrientation(
                ComponentOrientation.LEFT_TO_RIGHT);
     
     choicePanel.add(_mmRadioButton);
     choicePanel.add(_nanometerRadioButton);
     choicePanel.add(_milsRadioButton);
     
     _mmRadioButton.setSelected(true);
     
     _centerPanel.add(choicePanel, BorderLayout.SOUTH);
     
     
     
     //Siew Yeng - fix when click cancel button will still proceed to Virtual Live Environment
     getRootPane().setDefaultButton(_okButton);
     setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
     pack();
     SwingUtils.centerOnComponent(this, parent);
   }
  
   /**
    * monitor the Enter key
    * @author George Booth
    */
   void someKeyReleased(KeyEvent e)
   {
     if (e.getKeyCode() == KeyEvent.VK_ENTER)
     {
       checkDummyValidSpec();
     }
   }

   /**
    *  @author chin-seong.kee
    */
   public void textField_actionPerformed(ActionEvent e)
   {
     dispose();
   }
   
   /**
    * display out of spec warning if needed
    * @author Chin Seong, Kee
    * @author Siew Yeng  
    */
   void checkDummyValidSpec()
   {          
     if (_mainUI.isVirtualLiveDummyPanelSizeWithinSpec(getPanelWidthInNanometers(), getPanelLengthInNanometers(), getPanelThicknessInNanometers()) == false)
     {
       _warningLabel.setText(StringLocalizer.keyToString("VL_DUMMY_PROJECT_RECTANGLE_IN_SPEC_WARNING_KEY"));
       pack();
     }
     else
     {
       _warningLabel.setText(" ");
       _ok = true;
     }
   }
   
   public boolean isNeedCreateDummyProject()
   {
     return _ok;
   }
   
   /**    
    * @author Cheah Lee Herng
    */
   public MathUtilEnum getUnitEnum()
   {
     if(_mmRadioButton.isSelected() == true)
       return MathUtilEnum.MILLIMETERS;  
     else if(_nanometerRadioButton.isSelected() == true)
       return MathUtilEnum.NANOMETERS;
     else
       return MathUtilEnum.MILS;
   }
   
   /**    
    * @author Cheah Lee Herng 
    */
   public double getPanelWidthInNanometers()
   {
     double convertedPanelWidthInNanometers = _panelWidth;
     
     MathUtilEnum choice = getUnitEnum();
     if (choice != MathUtilEnum.NANOMETERS)
     {      
       convertedPanelWidthInNanometers = MathUtil.convertUnits(_panelWidth, choice, MathUtilEnum.NANOMETERS);      
     }
     return convertedPanelWidthInNanometers;
   }
   
   /**    
    * @author Cheah Lee Herng 
    */
   public double getPanelLengthInNanometers()
   {
     double convertedPanelLengthInNanometers = _panelLength;
     
     MathUtilEnum choice = getUnitEnum();
     if (choice != MathUtilEnum.NANOMETERS)
     {      
       convertedPanelLengthInNanometers = MathUtil.convertUnits(_panelLength, choice, MathUtilEnum.NANOMETERS);      
     }
     return convertedPanelLengthInNanometers;
   }
   
   /**    
    * @author bee-hoon.goh 
    */
   public double getPanelThicknessInNanometers()
   {
     double convertedPanelThicknessInNanometers = _panelThickness;
     
     MathUtilEnum choice = getUnitEnum();
     if (choice != MathUtilEnum.NANOMETERS)
     {      
       convertedPanelThicknessInNanometers = MathUtil.convertUnits(_panelThickness, choice, MathUtilEnum.NANOMETERS);      
     }
     return convertedPanelThicknessInNanometers;
   }
}
