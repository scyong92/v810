package com.axi.v810.gui.mainMenu;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class handles all the persisted aspectes of the Main menu GUI.  Currently, that
 * is frame size and position, and the center divider and table column widths of the AXI
 * Home panel. Other persistance objects may extend this class and persist more specific
 * information for their own GUIs
 * @author George Booth
 */
public class MainMenuPersistance implements Serializable
{
  private java.awt.Point _framePosition = null;
  private java.awt.Dimension _frameSize = null;
  private int _homePanelDivider = -1;
  private int[] _localProjectsTableColumnWidths = null;
  private int[] _projectDatabaseTableColumnWidths = null;

  /**
   * @author Andy Mechtenberg
   */
  public MainMenuPersistance()
  {
    // do nothing
  }

  /**
   * This deliberately does not use datastore to access the file.
   * @return MainMenuPersistance
   * @author Andy Mechtenberg
   */
  public MainMenuPersistance readSettings()
  {
    MainMenuPersistance persistance = null;
    try
    {
      if (FileUtilAxi.exists(FileName.getMainMenuPersistFullPath()))
        persistance = (MainMenuPersistance) FileUtilAxi.loadObjectFromSerializedFile(FileName.getMainMenuPersistFullPath());
      else
        persistance = new MainMenuPersistance();
    }
    catch(DatastoreException de)
    {
      // do nothing..
      persistance = new MainMenuPersistance();
    }

    return persistance;

  }

  /**
   * This deliberately does not use datastore to access the file.
   * @author Andy Mechtenberg
   */
  public void writeSettings() throws DatastoreException
  {
    FileUtilAxi.saveObjectToSerializedFile(this, FileName.getMainMenuPersistFullPath());
  }

  /**
   * @author Andy Mechtenberg
   */
  public java.awt.Point getFramePosition()
  {
    return _framePosition;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setFramePosition(java.awt.Point position)
  {
    Assert.expect(position != null);
    _framePosition = position;
  }

  /**
   * @author George Booth
   */
  public void setFrameSize(java.awt.Dimension size)
  {
    Assert.expect(size != null);
    _frameSize = size;
  }

  /**
   * @author George Booth
   */
  public java.awt.Dimension getFrameSize()
  {
    return _frameSize;
  }

  /**
   * @author George Booth
   */
  public void setHomePanelDivider(int position)
  {
    _homePanelDivider = position;
  }

  /**
   * @author George Booth
   */
  public int getHomePanelDivider()
  {
    return _homePanelDivider;
  }

  /**
   * @author George Booth
   */
  public void setLocalProjectsTableColumnWidths(int widths[])
  {
    _localProjectsTableColumnWidths = null;
    _localProjectsTableColumnWidths = widths;
  }

  /**
   * @author George Booth
   */
  public int[] getLocalProjectsTableColumnWidths()
  {
    return _localProjectsTableColumnWidths;
  }

  /**
   * @author George Booth
   */
  public void setProjectDatabaseTableColumnWidths(int widths[])
  {
    _projectDatabaseTableColumnWidths = null;
    _projectDatabaseTableColumnWidths = widths;
  }

  /**
   * @author George Booth
   */
  public int[] getProjectDatabaseTableColumnWidths()
  {
    return _projectDatabaseTableColumnWidths;
  }
  
}
