package com.axi.v810.gui.mainMenu;

import com.axi.v810.business.userAccounts.*;
import com.axi.v810.gui.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.util.Assert;

/**
 *
 * <p>Title: LoginManager</p>
 *
 * <p>Description:  This class supports login activities for the v810 application.
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: ViTrox Technogies</p>
 *
 * @author George Booth
 */

public class LoginManager
{
  private static LoginManager _instance = null;
  private static UserAccountsManager _userAccountsManager = null;

  // constants for secret password
  private final long _ONE_DAY_IN_MILLIS = (long)(24 * 60 * 60 * 1000);
  private final long _OBTUSER = 35817L;

  // the user name and password from the current login
  private LoginInfo _loginInfo = null;

  private LoginManager()
  {
    // do nothing
  }

  /**
   * This method gets the one and only instance of the LoginManager object
   *
   * @author George Booth
   */
  public synchronized static LoginManager getInstance()
  {
    if (_instance == null)
    {
      _instance = new LoginManager();

      _userAccountsManager = UserAccountsManager.getInstance();
    }
    return _instance;
  }

  void readUserAccounts() throws XrayTesterException
  {
    _userAccountsManager.readUserAccounts();
  }

  /**
   * @author George Booth
   */
  UserTypeEnum getUserTypeFromRunString(String[] args)
  {
    // default to "start as login"
    if (args.length < 2)
    {
      return UserTypeEnum.LOGIN;
    }

    String start = args[0];
    String user = args[1];
//    System.out.println("starting with \"" + start + " " + user + "\"");

    // force login if start != "-start"
    if (!start.equals("-start"))
    {
      return UserTypeEnum.LOGIN;
    }

    // force login if user is not a valid user type
    if (UserTypeEnum.doesUserTypeExist(user))
    {
      return UserTypeEnum.getUserTypeEnum(user);
    }
    else
    {
      return UserTypeEnum.LOGIN;
    }
  }

  void clearLoginInfo()
  {
    _loginInfo = new LoginInfo("", null);
    _userAccountsManager.clearCurrentUser();
  }

  public void setLoginInfo(String userName)
  {
    _loginInfo = new LoginInfo(userName, null);
    _userAccountsManager.setCurrentUser(userName);
  }

  /**
   * @author George Booth
   */
  boolean promptForLogIn(MainMenuGui mainUI)
  {
    clearLoginInfo();
    LoginDialog login = new LoginDialog(mainUI);
    if (login.showDialog(_loginInfo))
    {
      String loginName = _loginInfo.userName;
      String password = _loginInfo.password;

      // special case -- don't allow an operator login when running offline (TDW)
      if (_userAccountsManager.doesUserExist(loginName)  &&
          _userAccountsManager.getUserTypeFromUserName(loginName).equals(UserTypeEnum.OPERATOR))
      {
        if (XrayTester.getInstance().isHardwareAvailable() == false)
        {
          // don't allow.
          MessageDialog.showErrorDialog(
              mainUI,
              StringLocalizer.keyToString("MMGUI_GUI_OPERATOR_LOGIN_OFFLINE_KEY"),
              StringLocalizer.keyToString("MMGUI_GUI_BAD_LOGIN_TITLE_KEY"));
          // try again
          return promptForLogIn(mainUI);
        }
      }

      // special case -- don't allow an service (or service_expert) login when running offline (TDW)
      if (_userAccountsManager.doesUserExist(loginName) &&
          (_userAccountsManager.getUserTypeFromUserName(loginName).equals(UserTypeEnum.SERVICE) ||
           _userAccountsManager.getUserTypeFromUserName(loginName).equals(UserTypeEnum.SERVICE_EXPERT)))
      {
        if (XrayTester.getInstance().isHardwareAvailable() == false)
        {
          // don't allow.
          MessageDialog.showErrorDialog(
              mainUI,
              StringLocalizer.keyToString("MMGUI_GUI_SERVICE_LOGIN_OFFLINE_KEY"),
              StringLocalizer.keyToString("MMGUI_GUI_BAD_LOGIN_TITLE_KEY"));
          // try again
          return promptForLogIn(mainUI);
        }
      }

      // special case -- allow an administrator login with date-based passwword
      // in case they forgot their own
      if (loginName.equals(UserTypeEnum.ADMINISTRATOR.getName())  && isSecretPassword(password))
      {
        _userAccountsManager.setCurrentUser(UserTypeEnum.ADMINISTRATOR.getName());
        return true;
      }

      if (_userAccountsManager.isValidUserAccount(loginName, password))
      {
        _userAccountsManager.setCurrentUser(loginName);
        return true;
      }
      else
      {
        // not a valid user/password, show message
        MessageDialog.showErrorDialog(
            mainUI,
            StringLocalizer.keyToString("MMGUI_GUI_BAD_LOGIN_KEY"),
            StringLocalizer.keyToString("MMGUI_GUI_BAD_LOGIN_TITLE_KEY"));
        // try again
        return promptForLogIn(mainUI);
      }
    }
    // login dialog cancelled
    return false;
  }

  /**
   * @author George Booth
   */
  public UserTypeEnum getUserTypeFromLogIn()
  {
    Assert.expect(_loginInfo != null);
    return _userAccountsManager.getUserTypeFromUserName(_loginInfo.userName);
  }

  /**
   * @author George Booth
   */
  public String getUserNameFromLogIn()
  {
    if (_loginInfo != null)
    {
      return _loginInfo.userName;
    }
    else
    {
      return "";
    }
  }

  /**
   * If the password encoding is changed, make sure standalone application
   * generatePassword.java is updated. The main() function is a copy of this code.
   *
   * @author George Booth
   */
  String generateSecretPassword()
  {
    // The secret password is date based and only works for the current day.
    // It is derived from the current date in milliseconds. This number is
    // divided by the number of millseconds per day to get the number of days
    // since Jan 1, 1970 to make it manageable (greater than 13000).  An arbitrary
    // large number is added to it and the digits are then scrambled to make it
    // more difficult to decode by a hacker. The password is constrainted to five
    // digits (good for the next 175 years).
    long currentTime = System.currentTimeMillis();
    long passwordNumber = currentTime / _ONE_DAY_IN_MILLIS;
    long obtusePasswordNumber = passwordNumber + _OBTUSER;
    String rawPassword = (new Long(obtusePasswordNumber)).toString();
    // scramble the digits so that the LSD does not change daily.
    String password = rawPassword.substring(2,3) + rawPassword.substring(0,1) + rawPassword.substring(4,5) +
                      rawPassword.substring(1,2) + rawPassword.substring(3,4);
    return password;
  }

  /**
   * @author George Booth
   */
  boolean isSecretPassword(String testPassword)
  {
    // The secret password is date based and only works for the current day.
    // If the reconstituted date number matches the current date number, the
    // user is allowed to login as an admin to reset the admin password.
    try
    {
      if (testPassword.length() != 5)
      {
        return false;
      }
      String rawPassword = testPassword.substring(1,2) + testPassword.substring(3,4) + testPassword.substring(0,1) +
                        testPassword.substring(4,5) + testPassword.substring(2,3);
      long obtusePasswordNumber = new Long(rawPassword);
      long passwordNumber = obtusePasswordNumber - _OBTUSER;
      long currentNumber = System.currentTimeMillis() / _ONE_DAY_IN_MILLIS;
      return (currentNumber == passwordNumber);
    }
    catch(NumberFormatException nfe)
    {
      return false;
    }
  }

}
