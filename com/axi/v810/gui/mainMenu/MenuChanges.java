package com.axi.v810.gui.mainMenu;

import java.util.*;

import javax.swing.*;

/**
 * This class supports menu changes in MainUIMenuBar by defining lists
 * of menu, menu item and menu separator pairs. Each list item pairs
 * a menu with a menu component.
 *
 * @author George Booth
 */

public class MenuChanges
{
  private java.util.List<JMenu> _menus = null;
  private java.util.List<MenuItem> _menuItems = null;
  private java.util.List<Separator> _separators = null;

  /**
   * @author George Booth
   */
  public MenuChanges()
  {
    _menus = new ArrayList<JMenu>();
    _menuItems = new ArrayList<MenuItem>();
    _separators = new ArrayList<Separator>();
  }
  
  /*
   * @author Kee chin Seong
   * - Memory Leak, Free All the Menu
   */
  public void unpopulateAll()
  {
    _menus.clear();
    _menuItems.clear();
    _separators.clear();
    
    _menus = null;
    _menuItems = null;
    _separators = null;
  }

  /**
   * @author George Booth
   */
  public void addMenu(JMenu menu)
  {
    _menus.add(menu);
  }

  /**
   * @author George Booth
   */
  public Iterator<JMenu> getMenus()
  {
    return _menus.iterator();
  }

  /**
   * @author George Booth
   */
  public void addMenuItem(JMenu menu, JMenuItem menuItem)
  {
    _menuItems.add(new MenuItem(menu, menuItem));
  }

  /**
   * @author George Booth
   */
  public Iterator<MenuItem> getMenuItems()
  {
    return _menuItems.iterator();
  }

  /**
   * @author George Booth
   */
  public void addSeparator(JMenu menu, JSeparator separator)
  {
    _separators.add(new Separator(menu, separator));
  }

  /**
   * @author George Booth
   */
  public Iterator<Separator> getSeparators()
  {
    return _separators.iterator();
  }

  /**
   * @author George Booth
   */
  class MenuItem
  {
    private JMenu _menu = null;
    private JMenuItem _menuItem = null;

    /**
     * @author George Booth
     */
    MenuItem(JMenu menu, JMenuItem menuItem)
    {
      _menu = menu;
      _menuItem = menuItem;
    }

    /**
     * @author George Booth
     */
    public JMenu getMenu()
    {
      return _menu;
    }

    /**
     * @author George Booth
     */
    public JMenuItem getMenuItem()
    {
      return _menuItem;
    }
  }

  /**
   * @author George Booth
   */
  class Separator
  {
    private JMenu _menu = null;
    private JSeparator _separator = null;

    /**
     * @author George Booth
     */
    Separator(JMenu menu, JSeparator separator)
    {
      _menu = menu;
      _separator = separator;
    }

    /**
     * @author George Booth
     */
    public JMenu getMenu()
    {
      return _menu;
    }

    /**
     * @author George Booth
     */
    public JSeparator getSeparator()
    {
      return _separator;
    }
  }

}

