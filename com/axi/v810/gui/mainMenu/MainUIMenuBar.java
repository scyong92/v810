package com.axi.v810.gui.mainMenu;

import com.axi.v810.util.InfoHandlerPanel;
import java.awt.*;
import java.awt.Component;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.ndfReaders.*;
import com.axi.v810.datastore.ndfWriters.NdfWriter;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

import com.axi.v810.business.virtualLive.VirtualLiveManager;
import com.axi.v810.datastore.projWriters.*;
import java.util.logging.*;
import com.axi.v810.datastore.shopFloorSystem.*;
import static com.axi.v810.gui.mainMenu.MainMenuGui.finishLicenseMonitor;
import com.axi.v810.gui.virtualLive.*;

/**
 * This is the base menu bar for the Main UI.  It has methods to add
 * other menus and menu times for other environments and tasks
 * @author George Booth
 */
public class MainUIMenuBar extends JMenuBar implements Observer
{
  private XrayTester _xrayTester;
  
  //Project's history log CR- hsia-fen.tan
  private TestExecution _testExecution;
  private HistoryLog _showHistoryLog;
  private ProjectHistoryLog _ProjectHistoryLog;
  private UserTypeEnum _currentUserType = null;
  
  private Config _config = Config.getInstance();
  private Project _project = null;
  private boolean _isProjectLoaded = false;
  
  //shengchuan
  private boolean _isProjectExist = false;
  
  private boolean _isDefectPackagerRunning = false;
  private boolean _isDefectPackagerStopped = false;
  
  private MainMenuGui _mainUI = null;
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getHomeInstance();

  private Font _dialogFont = FontUtil.getDefaultFont();  // font used for busy cancel dialogs (in case other screens like TestExecution need a larger font)
  private ProjectSummaryReader _projectSummaryReader = ProjectSummaryReader.getInstance();

  private JMenu _fileMenu = new JMenu();
  private JMenuItem _openProjectMenuItem = new JMenuItem();
  private JMenuItem _importProjectMenuItem = new JMenuItem();
  private JMenuItem _exportProjectAsNdfMenuItem = new JMenuItem(); //export NDF  - Siew Yeng
  private JMenuItem _saveProjectMenuItem = new JMenuItem();
  private JMenuItem _saveAsProjectMenuItem = new JMenuItem();
  private JMenuItem _closeProjectMenuItem = new JMenuItem();
  private JMenuItem _logoutMenuItem = new JMenuItem();
  private JMenuItem _exitMenuItem = new JMenuItem();

  private JMenu _editMenu = new JMenu();
  private JMenuItem _undoMenuItem = new JMenuItem();
  private JMenuItem _redoMenuItem = new JMenuItem();

  private JMenu _toolsMenu = new JMenu();
  private JMenuItem _imageManagerMenuItem = new JMenuItem();
  private JMenuItem _generateImageSetMenuItem = new JMenuItem();
  private JMenuItem _focusSummaryMenuItem = new JMenuItem();
  private JMenuItem _fixFocusMenuItem = new JMenuItem();
  private JMenuItem _zipMenuItem = new JMenuItem();
  private JMenuItem _testCoverageReportMenuItem = new JMenuItem();
  private JMenuItem _unzipMenuItem = new JMenuItem();
  private JMenuItem _deleteMenuItem = new JMenuItem();
  private JMenuItem _collectDiagnosticPackageWizardMenuItem = new JMenuItem();
  //private JMenuItem _databaseTestMenuItem = new JMenuItem();
  private JMenuItem _importPackageLibraryMenuItem = new JMenuItem();
  private JMenuItem _exportPakcageLibraryMenuItem = new JMenuItem();
  // CAD Creator Import CAD feature - Ying-Huan.Chu
  private JMenuItem _importLandPatternMenuItem = new JMenuItem();
  private JMenuItem _exportLandPatternMenuItem = new JMenuItem();
  
  // BOM Updater menu item
  // private JMenuItem _updateBomMenuItem = new JMenuItem();
  
  //defect Packager toggle indicator
  private JMenuItem _startDefectPackagerMenuItem = new JMenuItem();
  private JMenuItem _stopDefectPackagerMenuItem = new JMenuItem();
  
  //Siew Yeng - convert multiboard to single panel
  private JMenuItem _convertMultiBoardToSinglePanelMenuItem = new JMenuItem();
  
  //Ngie Xing - change system magnification
  private JMenuItem _changeSystemMagnificationMenuItem = new JMenuItem();
  
  //Event Enum for defect Packager
  private DefectPackagerObservable _defectPackagerEventObservable = null;

  private JMenu _actionsMenu = new JMenu();
  private JMenuItem _startupMenuItem = new JMenuItem();
//  private JMenuItem _startupNoCalMenuItem = new JMenuItem();
  private JMenuItem _shutdownMenuItem = new JMenuItem();
//CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
  private JMenuItem _xRayAutoShortTermShutdownMenuItem = new JMenuItem();
  private LegacyXraySource _xraySource = null;
  private HTubeXraySource _hTubeXraySource = null;
  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  boolean xrayShutdownSuccessfully = false;

  private JMenuItem _loadPanelMenuItem = new JMenuItem();
  private JMenuItem _unloadPanelMenuItem = new JMenuItem();
  private JMenuItem _homeRailsMenuItem = new JMenuItem();

  private JMenu _windowMenu = new JMenu();
  private JMenuItem _homeUIMenuItem = new JMenuItem();
  private JMenuItem _testDevUIMenuItem = new JMenuItem();
  private JMenuItem _testExecUIMenuItem = new JMenuItem();
  private JMenuItem _configUIMenuItem = new JMenuItem();
  private JMenuItem _serviceUIMenuItem = new JMenuItem();
  private JMenuItem _virtualLiveUIMenuItem = new JMenuItem();
  
  //Kee Chin Seong - Cad Creator
  private JMenuItem _cadCreatorUIMenuItem = new JMenuItem();

  private JMenu _helpMenu = new JMenu();
  private JMenuItem _mainHelpMenuItem = new JMenuItem();
  private JMenuItem _helpAboutMenuItem = new JMenuItem();
  private JMenuItem _helpInfoMenuItem = new JMenuItem();

  private int _index = 0;
  private final int FILE = _index++;
  private final int FILE_OPEN = _index++;
  private final int FILE_IMPORT = _index++;
  private final int FILE_EXPORT = _index++;
  private final int FILE_SAVE = _index++;
  private final int FILE_SAVE_AS = _index++;
  private final int FILE_CLOSE = _index++;
  private final int FILE_LOGOUT = _index++;
  private final int FILE_EXIT = _index++;

  private final int EDIT = _index++;
  private final int EDIT_UNDO = _index++;
  private final int EDIT_REDO = _index++;

  private final int TOOLS = _index++;
  private final int TOOLS_IMAGE_MANAGER = _index++;
  private final int TOOLS_GENERATE_IMAGE_SET = _index++;
  private final int TOOLS_FOCUS_SUMMARY = _index++;
  private final int TOOLS_FOCUS_ADJUST = _index++;
  private final int TOOLS_TEST_COVERAGE_REPORT = _index++;
  private final int TOOLS_ZIP = _index++;
  private final int TOOLS_UNZIP = _index++;
  private final int TOOLS_DELETE = _index++;
  private final int TOOLS_COLLECT_DIAGNOSTIC_PACKAGE = _index++;
  private final int TOOLS_IMPORT_LIBRARY = _index++;
  private final int TOOLS_EXPORT_LIBRARY = _index++;
  private final int TOOLS_START_DEFECT_PACKAGER = _index++;
  private final int TOOLS_STOP_DEFECT_PACKAGER = _index++;
  private final int TOOLS_UPDATE_BOM = _index++;
  private final int TOOLS_CONVERT_MULTIBOARD_TO_SINGLE_PANEL = _index++;
  private final int TOOLS_CHANGE_SYSTEM_MAGNIFICATION = _index++;

  private final int ACTIONS = _index++;
  private final int ACTIONS_START_UP = _index++;
  private final int ACTIONS_SHUT_DOWN = _index++;
  //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
  private final int ACTIONS_XRAY_AUTO_SHUT_DOWN = _index++;

  private final int ACTIONS_LOAD_PANEL = _index++;
  private final int ACTIONS_UNLOAD_PANEL = _index++;
  private final int ACTIONS_HOME_RAILS = _index++;

  private final int WINDOW = _index++;
  private final int WINDOW_HOME = _index++;
  private final int WINDOW_TEST_DEV = _index++;
  private final int WINDOW_TEST_EXEC = _index++;
  private final int WINDOW_CONFIG = _index++;
  private final int WINDOW_SERVICE = _index++;
  private final int WINDOW_VIRTUAL_LIVE = _index++;
  private final int WINDOW_CAD_CREATOR = _index++;
  
  private final int HELP = _index++;
  private final int HELP_MAIN = _index++;
  private final int HELP_ABOUT = _index++;


  // menu enable array - which menu is enabled for which user
  public final static boolean[][] MENU_ENABLE = {
   //  Admin   Devel   Oper   Expert Service
    {  true,   true,   true,   true,   true, }, // FILE
    {  true,   true,   true,   true,  false, }, // FILE_OPEN
    {  true,   true,  false,   true,  false, }, // FILE_IMPORT
    {  true,   true,  false,   true,  false, }, // FILE_EXPORT - export NDF
    {  true,   true,  false,   true,  false, }, // FILE_SAVE
    {  true,   true,  false,   true,  false, }, // FILE_SAVE_AS
    {  true,   true,  false,   true,  false, }, // FILE_CLOSE
    {  true,   true,   true,   true,   true, }, // FILE_LOGOUT
    {  true,   true,   true,   true,   true, }, // FILE_EXIT

    {  true,   true,  false,   true,  false, }, // EDIT
    {  true,   true,  false,   true,  false, }, // EDIT_UNDO
    {  true,   true,  false,   true,  false, }, // EDIT_REDO

    {  true,   true,  false,   true,  false, }, // TOOLS
    {  true,   true,  false,   true,  false, }, // TOOLS_IMAGE_MANAGER
    {  true,   true,  false,   true,  false, }, // TOOLS_GENERATE_IMAGE_SET
    {  true,   true,  false,   true,  false, }, // TOOLS_FOCUS_SUMMARY
    {  true,   true,  false,   true,  false, }, // TOOLS_FOCUS_ADJUST
    {  true,   true,  false,   true,  false, }, // TOOLS_TEST_COVERAGE_REPORT
    {  true,   true,  false,   true,  false, }, // TOOLS_ZIP
    {  true,   true,  false,   true,  false, }, // TOOLS_UNZIP
    {  true,   true,  false,   true,  false, }, // TOOLS_DELETE
    {  true,   true,  false,   true,  false, }, // TOOLS_COLLECT_DIAGNOSTIC_PACKAGE
//    {  true,   true,  false,   true,  false, }, // TOOLS_DATABASE_TEST
    {  true,   true,  false,   true,  false, }, // TOOLS_IMPORT_LIBRARY
    {  true,   false,  false,   true,  false, }, // TOOLS_EXPORT_LIBRARY
    {  true,   true,  true,    true,  true   }, //TOOLS_UPDATE_BOM
    {  true,   true,  true,    true,  true   }, //TOOLS_START_DEFECT_PACKAGER
    {  true,   true,  true,    true,  true   }, // TOOLS_STOP_DEFECT_PACKAGER
    {  true,   true,  true,    true,  true   }, // TOOLS_CONVERT_MULTIBOARD_TO_SINGLE_PANEL
    {  true,   true,  false,    true,  true   }, // TOOLS_CHANGE_SYSTEM_MAGNIFICATION

    {  true,   true,   true,   true,  false, }, // ACTIONS
    {  true,   true,   true,   true,  false, }, // ACTIONS_START_UP
    {  true,   true,   true,   true,  false, }, // ACTIONS_SHUT_DOWN
    //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
    {  true,   true,   true,   true,  false, }, // ACTIONS_XRAY_AUTO_SHUT_DOWN
    {  true,   true,   true,   true,  false, }, // ACTIONS_LOAD_PANEL
    {  true,   true,   true,   true,  false, }, // ACTIONS_UNLOAD_PANEL
    {  true,   true,   true,   true,  false, }, // ACTIONS_HOME_RAILS

    {  true,   true,  false,   true,   true, }, // WINDOW
    {  true,   true,  false,   true,   true, }, // WINDOW_HOME
    {  true,   true,  false,   true,   true, }, // WINDOW_TEST_DEV
    {  true,   true,  false,   true,   true, }, // WINDOW_TEST_EXEC
    {  true,   true,  false,   true,   true, }, // WINDOW_CONFIG
    {  true,   true,  false,   true,   true, }, // WINDOW_SERVICE
    {  true,   true,  false,   true,   true, }, // WINDOW_VIRTUAL_LIVE
    {  true,   true,  false,   true,   true, }, // WINDOW_CAD_CREATOR

    {  true,   true,   true,   true,   true, }, // HELP
    {  true,   true,   true,   true,   true, }, // HELP_MAIN
    {  true,   true,   true,   true,   true, }, // HELP_ABOUT
 };

  private int _currentUser = 0;

  private UndoKeystrokeHandler _undoKeystrokeHandler;
  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();

  // List of things added to MainUIMenuBar, indexed by _currentRequest
  private java.util.List<MenuChanges> _menuChanges = null;
  private int _currentRequest = -1;
  private int _menuRequesterID = -1;

  private boolean _panelIsLoaded = false;
  private boolean _online = false;

  // Localized strings loaded when menu is initialized
  private String LOGOUT = null;
  private String UNDO = null;
  private String REDO = null;

  // list of current accelerator keys for checking
  private java.util.List<String> _accelerators = null;

  private XrayTesterException _productionLoadException;
  private ProjectErrorLogUtil _projectErrorLogUtil;
  private java.util.List<String> _componentListUsingCompPackage = new ArrayList<String>();
  private SerialNumberManager _serialNumberManager;
  
  //Khaw Chek Hau - XCR2654: CAMX Integration
  private AbstractShopFloorSystem _shopFloorSystem = AbstractShopFloorSystem.getInstance();
  
  //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
  private MachineUtilizationReportSystem _machineUtilizationReportSystem = MachineUtilizationReportSystem.getInstance();
  
  //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
  private VOneMachineStatusMonitoringSystem _vOneMachineStatusMonitoringSystem = VOneMachineStatusMonitoringSystem.getInstance();
  
  //Khaw Chek Hau - XCR3670: V-ONE AXI latest status monitoring log
  private MachineLatestStatusLogWriter _machineLatestStatusLogWriter = MachineLatestStatusLogWriter.getInstance();
  
  private boolean _isConvertedFromCad = false;
  /**
   * @author George Booth
   */
  public MainUIMenuBar(MainMenuGui mainUI)
  {
    Assert.expect(mainUI != null);
    _mainUI = mainUI;
    // initialize the accelerator key set
    _accelerators = new ArrayList<String>();

    // initialize the menu tracking objects
    _menuChanges = new ArrayList<MenuChanges>();
    _currentRequest = -1;

    // jbInit will create each menu item and add appropriate listeners
    // addMenuItems will actually add the menu items to the JMenuBar (which this class derives from)
    jbInit();
    addMenuItems();
    _undoKeystrokeHandler = new UndoKeystrokeHandler(mainUI, _undoMenuItem, _redoMenuItem);

    ProjectObservable.getInstance().addObserver(this);
    ConfigObservable.getInstance().addObserver(this);
    _xrayTester = XrayTester.getInstance();

    _online =_xrayTester.isHardwareAvailable();
    
    //Project's history log CR-hsia-fen.tan
    _testExecution = TestExecution.getInstance();
    _ProjectHistoryLog = ProjectHistoryLog.getInstance();
    _showHistoryLog= HistoryLog.getInstance();
    
    //Kee, Chin Seong - Defect Packager Event Enum Observable
    _defectPackagerEventObservable = DefectPackagerObservable.getInstance();
    _defectPackagerEventObservable.addObserver(this);

    //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
    if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
    {
        AbstractXraySource xraySource = AbstractXraySource.getInstance();
        Assert.expect(xraySource instanceof LegacyXraySource);
        _xraySource = (LegacyXraySource)xraySource;
    }
    else if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
    {
        AbstractXraySource xraySource = AbstractXraySource.getInstance();
        Assert.expect(xraySource instanceof HTubeXraySource );
        _hTubeXraySource = (HTubeXraySource )xraySource;
    }
    
    // Bee Hoon, XCR1650 - Open recipe and immediately save recipe, it will crash
    _projectErrorLogUtil = ProjectErrorLogUtil.getInstance();
    _serialNumberManager = SerialNumberManager.getInstance();
  }

  /**
   * @author George Booth
   */
  public JMenuBar getMainUIMenuBar()
  {
    return this;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setCommandManager(com.axi.v810.gui.undo.CommandManager commandManager)
  {
    Assert.expect(commandManager != null);
    _commandManager = commandManager;
    updateUndoRedoMenuItems();
  }

  /**
   * @author Bill Darbie
   */
  public UndoKeystrokeHandler getUndoKeystrokeHandler()
  {
    Assert.expect(_undoKeystrokeHandler != null);
    return _undoKeystrokeHandler;
  }

  /**
   * @author George Booth
   */
  private void jbInit()
  {
    initFileMenu();
    initEditMenu();
    initToolsMenu();
    initActionsMenu();
    initUtilitiesMenu();
    initWindowMenu();
    initHelpMenu();
  }

  /**
   * @author George Booth
   */
  private void addMenuItems()
  {
    _menuRequesterID = this.startMenuAdd();

    this.addMenu(_menuRequesterID, 0, _fileMenu);
    this.addMenuItem(_menuRequesterID, _fileMenu, _openProjectMenuItem);
    this.addMenuItem(_menuRequesterID, _fileMenu, _importProjectMenuItem);
    this.addMenuItem(_menuRequesterID, _fileMenu, _exportProjectAsNdfMenuItem);
    this.addMenuItem(_menuRequesterID, _fileMenu, _saveProjectMenuItem);
    this.addMenuItem(_menuRequesterID, _fileMenu, _saveAsProjectMenuItem);
    this.addMenuItem(_menuRequesterID, _fileMenu, _closeProjectMenuItem);
    this.addMenuSeparator(_menuRequesterID, _fileMenu);
    this.addMenuItem(_menuRequesterID, _fileMenu, _logoutMenuItem);
    this.addMenuSeparator(_menuRequesterID, _fileMenu);
    this.addMenuItem(_menuRequesterID, _fileMenu, _exitMenuItem);

    this.addMenu(_menuRequesterID, 1, _editMenu);
    this.addMenuItem(_menuRequesterID, _editMenu, _undoMenuItem);
    this.addMenuItem(_menuRequesterID, _editMenu, _redoMenuItem);
    // save menu text without "&" for use in building extended text
    UNDO = _undoMenuItem.getText();
    REDO = _redoMenuItem.getText();

    this.addMenu(_menuRequesterID, 2, _toolsMenu);
    this.addMenuItem(_menuRequesterID, _toolsMenu, _imageManagerMenuItem);
    this.addMenuItem(_menuRequesterID, _toolsMenu, _generateImageSetMenuItem);
//    this.addMenuItem(_menuRequesterID, _toolsMenu, _focusSummaryMenuItem);
//    this.addMenuItem(_menuRequesterID, _toolsMenu, _fixFocusMenuItem);
    this.addMenuItem(_menuRequesterID, _toolsMenu, _zipMenuItem);
    this.addMenuItem(_menuRequesterID, _toolsMenu, _unzipMenuItem);
    this.addMenuItem(_menuRequesterID, _toolsMenu, _testCoverageReportMenuItem);
    this.addMenuItem(_menuRequesterID, _toolsMenu, _deleteMenuItem);
    this.addMenuItem(_menuRequesterID, _toolsMenu, _collectDiagnosticPackageWizardMenuItem);
    //this.addMenuItem(_menuRequesterID, _toolsMenu, _databaseTestMenuItem);
    this.addMenuItem(_menuRequesterID, _toolsMenu, _importPackageLibraryMenuItem);
    this.addMenuItem(_menuRequesterID, _toolsMenu, _exportPakcageLibraryMenuItem);
    
    this.addMenuItem(_menuRequesterID, _toolsMenu, _importLandPatternMenuItem);
    this.addMenuItem(_menuRequesterID, _toolsMenu, _exportLandPatternMenuItem);
    
    //this.addMenuItem(_menuRequesterID, _toolsMenu, _updateBomMenuItem);
    
    this.addMenuItem(_menuRequesterID, _toolsMenu, _startDefectPackagerMenuItem);
    this.addMenuItem(_menuRequesterID, _toolsMenu, _stopDefectPackagerMenuItem);
    this.addMenuItem(_menuRequesterID, _toolsMenu, _convertMultiBoardToSinglePanelMenuItem);
    
    //Ngie Xing, only available for S2 EX
    try
    {
      if (LicenseManager.isOfflineProgramming() ==false)
      {
        if(XrayTester.isS2EXEnabled())
          this.addMenuItem(_menuRequesterID, _toolsMenu, _changeSystemMagnificationMenuItem);
      }
      else
      {
        this.addMenuItem(_menuRequesterID, _toolsMenu, _changeSystemMagnificationMenuItem);
      }
    }
    catch (final BusinessException e)
    {
      finishLicenseMonitor();
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  e.getMessage(),
                  StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                  true);
          MainMenuGui.getInstance().exitV810(false);
        }
      });
    }

    this.addMenu(_menuRequesterID, 3, _actionsMenu);
    this.addMenuSeparator(_menuRequesterID, _actionsMenu);
    this.addMenuItem(_menuRequesterID, _actionsMenu, _startupMenuItem);
//    this.addMenuItem(_menuRequesterID, _actionsMenu, _startupNoCalMenuItem);
    this.addMenuItem(_menuRequesterID, _actionsMenu, _shutdownMenuItem);
    //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
    this.addMenuItem(_menuRequesterID, _actionsMenu, _xRayAutoShortTermShutdownMenuItem);

    this.addMenuSeparator(_menuRequesterID, _actionsMenu);
    this.addMenuItem(_menuRequesterID, _actionsMenu, _loadPanelMenuItem);
    this.addMenuItem(_menuRequesterID, _actionsMenu, _unloadPanelMenuItem);
    this.addMenuSeparator(_menuRequesterID, _actionsMenu);
    this.addMenuItem(_menuRequesterID, _actionsMenu, _homeRailsMenuItem);

    this.addMenu(_menuRequesterID, 4, _windowMenu);
    this.addMenuItem(_menuRequesterID, _windowMenu, _homeUIMenuItem);
    this.addMenuItem(_menuRequesterID, _windowMenu, _testDevUIMenuItem);
    this.addMenuItem(_menuRequesterID, _windowMenu, _testExecUIMenuItem);
    this.addMenuItem(_menuRequesterID, _windowMenu, _configUIMenuItem);
    try
    {
      if (LicenseManager.isV810() &&
          Config.getInstance().getBooleanValue(SoftwareConfigEnum.ONLINE_WORKSTATION) == true)
      {
        this.addMenuItem(_menuRequesterID, _windowMenu, _serviceUIMenuItem);
      }
    }
    catch (BusinessException ex)
    {
      // do nothing
    }
    // only add the VirtualLive menu item if so set in the software.config file
    if((_mainUI.isVirtualLiveModeEnabled()))
    {
      this.addMenuItem(_menuRequesterID, _windowMenu, _virtualLiveUIMenuItem);
    }
    
    this.addMenuItem(_menuRequesterID, _windowMenu, _cadCreatorUIMenuItem);

    this.addMenu(_menuRequesterID, 5, _helpMenu);
    this.addMenuItem(_menuRequesterID, _helpMenu, _mainHelpMenuItem);
    this.addMenuSeparator(_menuRequesterID, _helpMenu);
    this.addMenuItem(_menuRequesterID, _helpMenu, _helpAboutMenuItem);
    this.addMenuSeparator(_menuRequesterID, _helpMenu);
    if (Config.isInfoHandlerEnabled())
    {
      this.addMenuItem(_menuRequesterID, _helpMenu, _helpInfoMenuItem);
    }
  }

  /**
   * @author George Booth
   */
  void initFileMenu()
  {
    _fileMenu.setText(StringLocalizer.keyToString("GUI_FILE_MENU_KEY"));
    _fileMenu.setName("_fileMenu");

    _openProjectMenuItem.setText(StringLocalizer.keyToString("GUI_FILE_OPEN_MENU_KEY"));
    _openProjectMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_MASK));  // CTRL-O (letter O as in Open)
    _openProjectMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        projectOpen();
      }
    });

    _importProjectMenuItem.setText(StringLocalizer.keyToString("GUI_FILE_IMPORT_MENU_KEY"));
    _importProjectMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_I, KeyEvent.CTRL_MASK));  // CTRL-I (letter O as in Open)
    _importProjectMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        importProjectMenuItem_actionPerformed();
      }
    });
    
    //export NDF -  Siew Yeng
    _exportProjectAsNdfMenuItem.setText(StringLocalizer.keyToString("GUI_FILE_EXPORT_MENU_KEY"));
    _exportProjectAsNdfMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_MASK));  // CTRL-E (letter E as in Export)
    _exportProjectAsNdfMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exportProjectAsNdfMenuItem_actionPerformed();
      }
    });

    _saveProjectMenuItem.setText(StringLocalizer.keyToString("GUI_FILE_SAVE_MENU_KEY"));
    _saveProjectMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_MASK)); // CTRL-S
    _saveProjectMenuItem.setEnabled(false);
    _saveProjectMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveProject();
      }
    });

    _saveAsProjectMenuItem.setText(StringLocalizer.keyToString("GUI_FILE_SAVE_AS_MENU_KEY"));
    _saveAsProjectMenuItem.setEnabled(false);
    _saveAsProjectMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveAsProject();
      }
    });

    _closeProjectMenuItem.setText(StringLocalizer.keyToString("GUI_FILE_CLOSE_PROJECT_MENU_KEY"));
    _closeProjectMenuItem.setEnabled(false);
    _closeProjectMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        projectClose(false);
      }
    });

    _logoutMenuItem.setText(StringLocalizer.keyToString("GUI_LOGOUT_MENU_KEY"));
    _logoutMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_L, KeyEvent.CTRL_MASK));  // CTRL-L (letter O as in Open)
    _logoutMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        logout(e);
      }
    });

    _exitMenuItem.setText(StringLocalizer.keyToString("GUI_FILE_EXIT_MENU_KEY"));
    _exitMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _mainUI.makeActive(false);
      }
    });
  }

  /**
   * @author George Booth
   */
  public JMenu getFileMenu()
  {
    return _fileMenu;
  }

  /**
   * @author Laura Cormos
   */
  public void enableImportMenuItem()
  {
    _importProjectMenuItem.setEnabled(true);
  }

  /**
   * @author Laura Cormos
   */
  public void disableImportMenuItem()
  {
    _importProjectMenuItem.setEnabled(false);
  }

  /**
   * @author George Booth
   */
  void initEditMenu()
  {
    _editMenu.setText(StringLocalizer.keyToString("GUI_EDIT_MENU_KEY"));
    _editMenu.setName("_editMenu");

    _undoMenuItem.setText(StringLocalizer.keyToString("GUI_UNDO_MENU_KEY"));
    _undoMenuItem.setEnabled(false);
    _undoMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        undoMenuItem_actionPerformed(e);
      }
    });

    _redoMenuItem.setText(StringLocalizer.keyToString("GUI_REDO_MENU_KEY"));
    _redoMenuItem.setEnabled(false);
    _redoMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        redoMenuItem_actionPerformed(e);
      }
    });
  }

  /**
   * @author George Booth
   */
  public JMenu getEditMenu()
  {
    return _editMenu;
  }

  /**
   * @author George Booth
   */
  void initToolsMenu()
  {
    _toolsMenu.setText(StringLocalizer.keyToString("GUI_TOOLS_MENU_KEY"));
    _toolsMenu.setName("_toolsMenu");


    /*_databaseTestMenuItem.setText(StringLocalizer.keyToString("Library access"));
    _databaseTestMenuItem.setEnabled(false);
    _databaseTestMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        databaseTest_actionPerformed();
      }
    });*/

    _importPackageLibraryMenuItem.setText(StringLocalizer.keyToString("PLWK_IMPORT_LIBRARY_KEY"));
    _importPackageLibraryMenuItem.setEnabled(false);
    _importPackageLibraryMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        importLibrary_actionPerformed();
      }
    });

    _exportPakcageLibraryMenuItem.setText(StringLocalizer.keyToString("PLWK_EXPORT_LIBRARY_KEY"));
    _exportPakcageLibraryMenuItem.setEnabled(false);
    _exportPakcageLibraryMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exportLibrary_actionPerformed();
      }
    });

    _importLandPatternMenuItem.setText(StringLocalizer.keyToString("CAD_CREATOR_IMPORT_LAND_PATTERN_KEY"));
    _importLandPatternMenuItem.setEnabled(false);
    _importLandPatternMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        importLandPattern_actionPerformed();
      }
    });
    
    _exportLandPatternMenuItem.setText(StringLocalizer.keyToString("CAD_CREATOR_EXPORT_LAND_PATTERN_KEY"));
    _exportLandPatternMenuItem.setEnabled(false);
    _exportLandPatternMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exportLandPattern_actionPerformed();
      }
    });

    _collectDiagnosticPackageWizardMenuItem.setText(StringLocalizer.keyToString("GUI_COLLECT_DIAGNOSTIC_PACKAGE_MENU_KEY"));
    _collectDiagnosticPackageWizardMenuItem.setEnabled(false);
    _collectDiagnosticPackageWizardMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        collectDiagnosticPackageMenuItem_actionPerformed();
      }
    });

    _imageManagerMenuItem.setText(StringLocalizer.keyToString("GUI_IMAGE_MANAGER_MENU_KEY"));
    _imageManagerMenuItem.setEnabled(false);
    _imageManagerMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        imageManagerMenuItem_actionPerformed(e);
      }
    });

    _testCoverageReportMenuItem.setText(StringLocalizer.keyToString("IMTB_TEST_COVERAGE_REPORT_SET_MENU_KEY"));
    _testCoverageReportMenuItem.setEnabled(false);
    _testCoverageReportMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
       public void actionPerformed(ActionEvent e)
      {
        testCoverageReportMenuItem_actionPerformed();
      }
    }
    );
    
    _startDefectPackagerMenuItem.setText(StringLocalizer.keyToString("IMTB_START_DEFECT_PACKAGER_SET_MENU_KEY"));
    _startDefectPackagerMenuItem.setEnabled(false);
    _startDefectPackagerMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
       public void actionPerformed(ActionEvent e)
      {
        startDefectPackager_actionPerformed();
      }
    }
    );
    
    //Kee, Chin Seong - For Starting defect packager, sending the command to the services to startup
    _stopDefectPackagerMenuItem.setText(StringLocalizer.keyToString("IMTB_STOP_DEFECT_PACKAGER_SET_MENU_KEY"));
    _stopDefectPackagerMenuItem.setEnabled(false);
    _stopDefectPackagerMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
       public void actionPerformed(ActionEvent e)
      {
        stopDefectPackager_actionPerformed();
      }
    }
    );

    //Kee, Chin Seong - For Stopping defect packager, sending the command to the services to stopped
    _generateImageSetMenuItem.setText(StringLocalizer.keyToString("IMTB_GENERATE_IMAGE_SET_MENU_KEY"));
    _generateImageSetMenuItem.setEnabled(false);
    _generateImageSetMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        generateImageSetMenuItem_actionPerformed();
      }
    });

    _focusSummaryMenuItem.setText(StringLocalizer.keyToString("GUI_FOCUS_SUMMARY_MENU_KEY"));
    _focusSummaryMenuItem.setEnabled(false);
    _focusSummaryMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        focusSummaryMenuItem_actionPerformed();
      }
    });
    _fixFocusMenuItem.setText(StringLocalizer.keyToString("GUI_FOCUS_FIX_MENU_KEY"));
    _fixFocusMenuItem.setEnabled(false);
    _fixFocusMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        fixFocusMenuItem_actionPerformed();
      }
    });

//    _updateBomMenuItem.setText(StringLocalizer.keyToString("GUI_UPDATE_BOM_MENU_KEY"));
//    _updateBomMenuItem.setEnabled(false);
//    _updateBomMenuItem.addActionListener(new java.awt.event.ActionListener()
//    {
//       public void actionPerformed(ActionEvent e)
//      {
//        updateBomMenuItem_actionPerformed(e);
//      }
//    }
//    );
    
    _convertMultiBoardToSinglePanelMenuItem.setText(StringLocalizer.keyToString("GUI_CONVERT_MULTIBOARD_TO_SINGLE_PANEL_MENU_KEY"));
    _convertMultiBoardToSinglePanelMenuItem.setEnabled(false);
    _convertMultiBoardToSinglePanelMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
       public void actionPerformed(ActionEvent e)
      {
        convertMultiBoardToSinglePanelMenuItem_actionPerformed(e);
      }
    }
    );

    
    _changeSystemMagnificationMenuItem.setText(StringLocalizer.keyToString("GUI_CHANGE_SYSTEM_MAGNIFICATION_KEY"));
    _changeSystemMagnificationMenuItem.setEnabled(false);
    _changeSystemMagnificationMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        changeSystemMagnificationMenuItem_actionPerformed();
      }
    });
  }

  /**
   * @author George Booth
   */
  public JMenu getToolsMenu()
  {
    return _toolsMenu;
  }

  /**
   * @author George Booth
   */
  void initActionsMenu()
  {
    _actionsMenu.setText(StringLocalizer.keyToString("GUI_ACTIONS_MENU_KEY"));
    _actionsMenu.setName("_actionsMenu");

    _startupMenuItem.setText(StringLocalizer.keyToString("GUI_START_UP_MENU_KEY"));
    _startupMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_U, KeyEvent.CTRL_MASK));
//    _startupMenuItem.setEnabled(_online);  // not enabled if TDW
    _startupMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        startup();
      }
    });

    /** GLB Remove before shipping - this is for developers only */
//    _startupNoCalMenuItem.setText("Startup (NO Calibrations)");
//    _startupNoCalMenuItem.addActionListener(new java.awt.event.ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        startup(false);
//      }
//    });

    _shutdownMenuItem.setText(StringLocalizer.keyToString("GUI_SHUT_DOWN_MENU_KEY"));
    _shutdownMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_D, KeyEvent.CTRL_MASK));
    _shutdownMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        shutdown(e);
      }
    });

    //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
    _xRayAutoShortTermShutdownMenuItem.setText(StringLocalizer.keyToString("GUI_XRAY_AUTO_SHORT_TERM_SHUTDOWN_MENU_KEY"));
    _xRayAutoShortTermShutdownMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.CTRL_MASK));
    _xRayAutoShortTermShutdownMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        xRayAutoShortTermShutdownMenuItem_actionPerformed();
      }
    });


    _loadPanelMenuItem.setText(StringLocalizer.keyToString("GUI_LOAD_PANEL_MENU_KEY"));
    _loadPanelMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_P, KeyEvent.CTRL_MASK));
    _loadPanelMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        loadPanel();
      }
    });

    _unloadPanelMenuItem.setText(StringLocalizer.keyToString("GUI_UNLOAD_PANEL_MENU_KEY"));
    _unloadPanelMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_MASK));
    _unloadPanelMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        unloadPanel();
      }
    });

    _homeRailsMenuItem.setText(StringLocalizer.keyToString("GUI_HOME_RAILS_MENU_KEY"));
    _homeRailsMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_H, KeyEvent.CTRL_MASK));
    _homeRailsMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        homeRails();
      }
    });
  }

/**
 * @author AnthonyFong
 */
//CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
public void xRayAutoShortTermShutdownMenuItem_actionPerformed()
{
    String message = StringLocalizer.keyToString("MMGUI_XRAY_SHORT_TERM_SHUTDOWN_CONFRIM_KEY");
    int response = JOptionPane.showConfirmDialog(_mainUI,
                                                 StringUtil.format(message, 50),
                                                 StringLocalizer.keyToString("MMGUI_XRAY_SHORT_TERM_SHUTDOWN_CONFRIM_TITLE_KEY"),
                                                 JOptionPane.YES_NO_OPTION,
                                                 JOptionPane.INFORMATION_MESSAGE);

    if (response == JOptionPane.YES_OPTION)
    {
      // short term turn off xrays
      try
      {
        _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
        {
          public void run() throws XrayTesterException
          {
            try
            {
              xrayShutdownSuccessfully = false;
              if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
              {
                _xraySource.offForServiceMode();
              }
              else if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
              {
                _hTubeXraySource.offForServiceMode();
              }
              xrayShutdownSuccessfully = true;
            }
            catch (XrayTesterException xte)
            {
              MainMenuGui.getInstance().handleXrayTesterException(xte);
            }
          }
        });

        if (xrayShutdownSuccessfully)
        {
          message = StringLocalizer.keyToString("MMGUI_XRAY_SHORT_TERM_SHUTDOWN_DONE_KEY");
          JOptionPane.showMessageDialog(_mainUI,
                                        StringUtil.format(message, 50),
                                        StringLocalizer.keyToString("MMGUI_XRAY_SHORT_TERM_SHUTDOWN_CONFRIM_TITLE_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
        }
      }
      catch (XrayTesterException xte)
      {
        MainMenuGui.getInstance().handleXrayTesterException(xte);

      }
  }
 }


  /**
   * @author George Booth
   */
  public JMenu getActionsMenu()
  {
    return _actionsMenu;
  }

  /**
   * @author George Booth
   */
  void initUtilitiesMenu()
  {
//    _utilitiesMenu.setText(StringLocalizer.keyToString("GUI_UTILITIES_MENU_KEY"));
//    _utilitiesMenu.setName("_utilitiesMenu");

    _zipMenuItem.setText(StringLocalizer.keyToString("GUI_ZIP_MENU_KEY"));
    _zipMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        zipMenuItem_actionPerformed(e);
      }
    });

    _unzipMenuItem.setText(StringLocalizer.keyToString("GUI_UNZIP_MENU_KEY"));
    _unzipMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        unzipMenuItem_actionPerformed(e);
      }
    });

    _deleteMenuItem.setText(StringLocalizer.keyToString("GUI_DELETE_MENU_KEY"));
    _deleteMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteMenuItem_actionPerformed(e);
      }
    });
  }

  /**
   * @author George Booth
   */
  void initWindowMenu()
  {
    _windowMenu.setText(StringLocalizer.keyToString("GUI_WINDOW_MENU_KEY"));
    _windowMenu.setName("windowMenu");

    _homeUIMenuItem.setText(StringLocalizer.keyToString("GUI_HOME_UI_MENU_KEY"));
    _homeUIMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        homeUIMenuItem_actionPerformed();
      }
    });

    _testDevUIMenuItem.setText(StringLocalizer.keyToString("GUI_TEST_DEV_UI_MENU_KEY"));
    _testDevUIMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        testDevUIMenuItem_actionPerformed();
      }
    });

    _testExecUIMenuItem.setText(StringLocalizer.keyToString("GUI_TEST_EXEC_UI_MENU_KEY"));
    _testExecUIMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        testExecUIMenuItem_actionPerformed();
      }
    });

    _configUIMenuItem.setText(StringLocalizer.keyToString("GUI_CONFIG_UI_MENU_KEY"));
    _configUIMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        configUIMenuItem_actionPerformed();
      }
    });

    _serviceUIMenuItem.setText(StringLocalizer.keyToString("GUI_SERVICE_UI_MENU_KEY"));
    _serviceUIMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        serviceUIMenuItem_actionPerformed();
      }
    });

    _virtualLiveUIMenuItem.setText(StringLocalizer.keyToString("GUI_VIRTUAL_LIVE_UI_MENU_KEY"));
    _virtualLiveUIMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        virtualLiveUIMenuItem_actionPerformed();
      }
    });
    
    
    //Kee Chin Seong - Cad Creator
    _cadCreatorUIMenuItem.setText(StringLocalizer.keyToString("GUI_CAD_CREATOR_UI_MENU_KEY"));
    _cadCreatorUIMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        virtualLiveUIMenuItem_actionPerformed();
      }
    });
  }

  /**
   * @author George Booth
   */
  void initHelpMenu()
  {
    _helpMenu.setText(StringLocalizer.keyToString("GUI_HELP_MENU_KEY"));
    _helpMenu.setName("_helpMenu");

    _mainHelpMenuItem.setText(StringLocalizer.keyToString("GUI_X6000_HELP_MENU_KEY"));
    _mainHelpMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
    _mainHelpMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        mainHelpMenuItem_actionPerformed(e);
      }
    });

    _helpAboutMenuItem.setText(StringLocalizer.keyToString("GUI_HELP_ABOUT_MENU_KEY"));
    _helpAboutMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        helpAboutMenuItem_actionPerformed(e);
      }
    });

    if (Config.isInfoHandlerEnabled())
    {
      _helpInfoMenuItem.setText(StringLocalizer.keyToString("GUI_HELP_INFO_MENU_KEY"));
      _helpInfoMenuItem.addActionListener(new java.awt.event.ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          helpInfoMenuItem_actionPerformed(e);
        }
      });
    }
  }

  /**
   * @author George Booth
   */
  public JMenu getHelpMenu()
  {
    return _helpMenu;
  }


// ------- MainUIMenuBar methods

  /**
   * Initiate a menu update. All changes are tracked by returned id
   * @return int the id number of this request
   * @author George Booth
   */
  public int startMenuAdd()
  {
    int id = ++_currentRequest;
    MenuChanges newChanges = new MenuChanges();
    _menuChanges.add(newChanges);
    return id;
  }

  /**
   * Uses the windows style of setting menu mnemonics.  The character
   * after an & is used as the shortcut. Spaces are not allowed as shortcuts.
   * The first & is recognized as the mnemoninc flag. A trailing & is ignored.
   * All & are deleted from the text.
   * @author George Booth
   */
  public static char getGenericMnemonic(AbstractButton button)
  {
    // check for "&" indicating the next character is the shortcut
    // a space character is not acceptable (but allowed by Java)
    // remove all "&"s in the text
    char[] oldText = button.getText().toCharArray();
    int length = oldText.length;
    StringBuffer newText = new StringBuffer();
    char shortcut = 0;
    for (int i = 0; i < length; i++)
    {
      if (oldText[i] != '&')
      {
        newText.append(oldText[i]);
      }
      else
      {
        //  first &           not trailing &        not a space
        if (shortcut == 0 && ((i+1) < length) && oldText[i+1] != ' ')
        {
          shortcut = oldText[i+1]; // char after &
        }
      }
    }
    button.setText(newText.toString());
    return shortcut;
  }

  /**
   * Uses the windows style of setting menu mnemonics.  The character
   * after an & is used as the shortcut. Spaces are not allowed as shortcuts.
   * The first & is recognized as the mnemoninc flag. A trailing & is ignored.
   * All & are deleted from the text.
   * @author George Booth
   */
  char getMnemonic(JMenuItem menu)
  {
    // check for "&" indicating the next character is the shortcut
    // a space character is not acceptable (but allowed by Java)
    // remove all "&"s in the text
    char[] oldText = menu.getText().toCharArray();
    int length = oldText.length;
    StringBuffer newText = new StringBuffer();
    char shortcut = 0;
    for (int i = 0; i < length; i++)
    {
      if (oldText[i] != '&')
      {
        newText.append(oldText[i]);
      }
      else
      {
        //  first &           not trailing &        not a space
        if (shortcut == 0 && ((i+1) < length) && oldText[i+1] != ' ')
        {
          shortcut = oldText[i+1]; // char after &
        }
      }
    }
    menu.setText(newText.toString());
    return shortcut;
  }

  /**
   * Checks the menu shortcut is unique in the menu bar.
   * @author George Booth
   */
  void checkMnemonic(JMenu menu) throws DatastoreException
  {
    char shortcut = getMnemonic(menu);
    if (shortcut == 0)
      return;

    Component[] components = this.getComponents();
    for (int i = 0; i < components.length; i++)
    {
      if (components[i] instanceof JMenu)
      {
        JMenu thisMenu = (JMenu)components[i];
        char thisShortcut = (char)thisMenu.getMnemonic();
        if (thisShortcut == shortcut)
        {
          throw new InvalidMenuShortcutInPropertiesDatastoreException(menu.getText(),
              "Alt+" + shortcut, thisMenu.getText());
        }
      }
    }
    menu.setMnemonic(shortcut);
  }

  /**
   * Checks the menu shortcut is unique in the menu context provided.
   * @author George Booth
   */
  void checkMnemonic(JMenuItem menuItem, JMenu menu) throws DatastoreException
  {
    char shortcut = getMnemonic(menuItem);
    if (shortcut == 0)
      return;

    Component[] components = menu.getMenuComponents();
    for (int i = 0; i < components.length; i++)
    {
      if (components[i] instanceof JMenuItem)
      {
        JMenuItem thisMenuItem = (JMenuItem)components[i];
        char thisShortcut = (char)thisMenuItem.getMnemonic();
        if (thisShortcut == shortcut)
        {
          throw new InvalidMenuItemShortcutInPropertiesDatastoreException(menuItem.getText(),
              "Alt+" + shortcut, menu.getText(), thisMenuItem.getText());
        }
      }
    }
    menuItem.setMnemonic(shortcut);
  }

  /**
   * Checks that the accelerator is unique within the entire app.
   * @author George Booth
   */
  void checkAccelerator(JMenuItem menuItem) throws DatastoreException
  {
    KeyStroke keyStroke = menuItem.getAccelerator();
    if (keyStroke != null)
    {
      String keyString = keyStroke.toString();
      if (_accelerators.contains(keyString))
      {
        menuItem.setAccelerator(null);
        throw new InvalidMenuItemAcceleratorInPropertiesDatastoreException(menuItem.getText(),
            keyString);
      }
      else
      {
        _accelerators.add(keyString);
      }
    }
  }

  /**
   * Removes the accelerator, if needed, when menu item is removed
   * @author George Booth
   */
  void removeSavedAccelerator(JMenuItem menuItem)
  {
    KeyStroke keyStroke = menuItem.getAccelerator();
    if (keyStroke != null)
    {
      String keyString = keyStroke.toString();
      if (_accelerators.contains(keyString))
      {
        _accelerators.remove(keyString);
      }
    }
  }

  /**
   * Add a new menu to the menu bar at location = pos (0 based)
   * @author George Booth
   */
  public void addMenu(int id, int pos, JMenu menu)
  {
    Assert.expect(id == _currentRequest);
    Assert.expect(pos > -1);
    Assert.expect(menu != null);

    try
    {
      checkMnemonic(menu);
      checkAccelerator(menu);
    }
    catch (DatastoreException dse)
    {
      MessageDialog.showErrorDialog(
              _mainUI,
              StringUtil.format(dse.getMessage(), 50),
              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
              true);
    }

    // save it
    MenuChanges changes = _menuChanges.get(_currentRequest);
    changes.addMenu(menu);
    this.add(menu, pos);
  }

  /**
   * Add a new menuItem to the end of the menu
   * @author George Booth
   */
  public void addMenuItem(int id, JMenu menu, JMenuItem menuItem)
  {
    Assert.expect(id == _currentRequest);
    Assert.expect(menuItem != null);

    try
    {
      checkMnemonic(menuItem, menu);
      checkAccelerator(menuItem);
    }
    catch (DatastoreException dse)
    {
      MessageDialog.showErrorDialog(
              _mainUI,
              StringUtil.format(dse.getMessage(), 50),
              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
              true);
    }

    // save it
    MenuChanges changes = _menuChanges.get(_currentRequest);
    changes.addMenuItem(menu, menuItem);

    menu.add(menuItem);
  }

  /**
   * Add a new menuItem to the menu at location "pos" (0 based)
   * @author George Booth
   */
  public void addMenuItem(int id, JMenu menu, int pos, JMenuItem menuItem)
  {
    Assert.expect(id == _currentRequest);
    Assert.expect(pos > -1);
    Assert.expect(menuItem != null);

    try
    {
      checkMnemonic(menuItem, menu);
      checkAccelerator(menuItem);
    }
    catch (DatastoreException dse)
    {
      MessageDialog.showErrorDialog(
              _mainUI,
              StringUtil.format(dse.getMessage(), 50),
              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
              true);
    }

    // save it
    MenuChanges changes = _menuChanges.get(_currentRequest);
    changes.addMenuItem(menu, menuItem);

    menu.add(menuItem, pos);
  }

  /**
   * Add a separator to the menu
   * @author George Booth
   */
  public void addMenuSeparator(int id, JMenu menu)
  {
    Assert.expect(id == _currentRequest);

    // save it
    MenuChanges changes = _menuChanges.get(_currentRequest);
    JSeparator separator = new JSeparator();
    changes.addSeparator(menu, separator);
    menu.add(separator);
  }

  /**
   * Add a separator to the menu at location "pos" (0 based)
   * @author George Booth
   */
  public void addMenuSeparator(int id, JMenu menu, int pos)
  {
    Assert.expect(id == _currentRequest);
    Assert.expect(pos > -1);

    // save it
    MenuChanges changes = _menuChanges.get(_currentRequest);
    JSeparator separator = new JSeparator();
    changes.addSeparator(menu, separator);
    menu.add(separator, pos);
  }

  /**
   * removes menu components added by requester
   * @author George Booth
   */
  public void undoMenuChanges(int id)
  {
//    System.out.println(id + " " + _currentRequest);
    Assert.expect(id == _currentRequest);

    MenuChanges changes = _menuChanges.remove(_currentRequest);
    // remove separators
    Iterator<MenuChanges.Separator> separators = changes.getSeparators();
    while (separators != null && separators.hasNext())
    {
      MenuChanges.Separator menuSeparatorPair = separators.next();
      JMenu menu = menuSeparatorPair.getMenu();
      JSeparator separator = menuSeparatorPair.getSeparator();
      menu.remove(separator);
    }
    // remove menuItems
    Iterator<MenuChanges.MenuItem> menuItems = changes.getMenuItems();
    while (menuItems != null && menuItems.hasNext())
    {
      MenuChanges.MenuItem menuItemPair = menuItems.next();
      JMenu menu = menuItemPair.getMenu();
      JMenuItem menuItem = menuItemPair.getMenuItem();
      removeSavedAccelerator(menuItem);
      menu.remove(menuItem);
    }
    // remove menus
    Iterator<JMenu> menus = changes.getMenus();
    while (menus != null && menus.hasNext())
    {
      JMenu menu = menus.next();
      this.remove(menu);
    }
    
    //Kee chin Seong - Memory leak, must unpopulate and free it
    changes.unpopulateAll();
    changes = null;
    //restore requester ID
    --_currentRequest;
  }

  /**
   * Returns to the previous user type
   * @author George Booth
   */
  void logout(ActionEvent e)
  {
    _mainUI.logout();
  }

  /**
   * Starts the x-ray system
   * @author George Booth
   */
  public void startup()
  {
    try
    {
      //require to run all CD&A task when click on startup button
      XrayHardwareAutomatedTaskList.getInstance().setCameraCalibrationsToRunASAP();
      XrayHardwareAutomatedTaskList.getInstance().setAllCalibrationsToRunASAP();
      _mainUI.startXrayTester();
    }
    catch (XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
    }
  }

  /**
   * Stops the x-ray system
   * @author George Booth
   */
  void shutdown(ActionEvent e)
  {
    try
    {
      _mainUI.stopXrayTester();
    }
    catch (XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
    }
  }

  /**
   * Loads a panel into the x-ray system
   * @author George Booth
   */
  private void loadPanel()
  {
    try
    {
      _mainUI.loadPanel();
    }
    catch (XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
    }
  }

  /**
   * Unloads a panel from the x-ray system
   * @author George Booth
   */
  private void unloadPanel()
  {
    try
    {
      _mainUI.unloadPanel();
    }
    catch (XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
    }
  }

  /**
   * Homes the panel guide rails in the x-ray system
   * @author George Booth
   */
  private void homeRails()
  {
    try
    {
      _mainUI.homeRails();
    }
    catch (XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
    }
  }

  /**
   * Saves the currently loaded project
   * @author George Booth
   */
  public boolean saveProject()
  {
    Assert.expect(_project != null);

    if (_mainUI.isPanelInLegalState() == false)
      return false;
    
    //Swee yee Wong - XCR-2837 Top is not integer - dont reset virtual live mode here, it will cause assert during generating virtual live image after recipe changes
    //Swee Yee Wong - XCR-3297 Xray Camera Insufficient Trigger when generate a 3x3 Virtual Live Image
//    if(_mainUI.isVirtualLiveCurrentEnvironment())
//      _mainUI.resetVirtualLiveMode();

    //Siew Yeng - XCR1725 - Customizable serial number mapping
    try
    {
      SerialNumberMappingManager.getInstance().checkIfSerialNumberMappingAndBarcodeReaderSetupAreTally();
    }
    catch(final XrayTesterException xte)
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
        MessageDialog.showErrorDialog(_mainUI, 
                                        xte.getLocalizedMessage(), 
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                        true);
        }
      });
    }
    LocalizedString savingStr = new LocalizedString("MM_GUI_SAVING_PROJECT_KEY", new Object[]{_project.getName()});
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
                                                                   StringLocalizer.keyToString(savingStr),
                                                                   StringLocalizer.keyToString("MM_GUI_SAVE_PROJECT_TITLE_KEY"),
                                                                   true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
    
    _ProjectHistoryLog.append("Recipe is saved");

//    System.out.println("glb - saving project \"" + _project.getName() + "\"");

    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP))
            PspAutoPopulatePanel.getInstance().savePspComponents();
          
          if(_project.isTestProgramValid())
            _project.save();
          else
            _project.fastSave();
          
          if(_mainUI.getTestDev().isTestDevPersistanceExist() == true)
            _mainUI.getTestDev().getPersistance().writeSettings(_project.getName());

          // create the cad image!
          DrawCadPanel.createPanelImage(_project);
        }
        catch(final DatastoreException ex)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              _mainUI.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
              MessageDialog.showErrorDialog(_mainUI,
                                            ex.getLocalizedMessage(),
                                            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                            true);
            }
          });
        }
        finally
        {
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.setVisible(false);
        }
      }
    });
    busyCancelDialog.setVisible(true);
    return true;
  }
  
  /**
   * @author Kee, Chin Seong 
   * To Stop the defect packager, command will be send from here
   */
  void stopDefectPackager()
  {
      final BusyCancelDialog BusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("GUI_STOPPING_DEFECT_PACKAGER_KEY"),
           StringLocalizer.keyToString("CDGUI_HOME_PANEL_TITLE_KEY"),
          true);

      BusyCancelDialog.pack();
      SwingUtils.centerOnComponent(BusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            if(FileUtil.exists(Directory.getDefectPackagerDir() + File.separator + FileName.getDefectPackagerExeFileName()) == true)
            {
                String[] cmd = {Directory.getDefectPackagerDir() + File.separator + FileName.getDefectPackagerExeFileName(), DefectPackagerUtil._STOP_DEFECT_PACKAGER_COMMAND};//new String[4];
                Process stopServiceProcess = Runtime.getRuntime().exec(cmd);//"cmd /c start \"C:\\Program Files\\DefectPackager\\Stop.bat");
                //stopServiceProcess.waitFor();
                //Thread.sleep(1000000); 
            }
            
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                BusyCancelDialog.setVisible(false);
              }
            });
          }
          catch(Exception e)
          {
              // do nothing
          }
        }
      });
      BusyCancelDialog.setVisible(true);
      _defectPackagerEventObservable.stateChanged(DefectPackagerTaskEventEnum.STOP_DEFECT_PACKAGER);
      
      if(DefectPackagerUtil.queryService().length() > 0 )
      {
         JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString("GUI_DEFECT_PACKAGER_HAVE_BEEN_STOPPED_KEY"),
                                        StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
         _mainUI.getEnvToolBar().enableStopDefectPackagerButton(false);
         _mainUI.getEnvToolBar().enableStartDefectPackagerButton(true);
         _stopDefectPackagerMenuItem.setEnabled(false);
      }
      else
      {
         JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString("GUI_DEFECT_PACKAGER_NOT_INSTALLED_WARNING_MESSAGE_KEY"),
                                        StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
      }
  }
  
  /**
   * @author Kee, Chin Seong 
   * To Start the defect packager, command will be send from here
   */
  void startDefectPackager()
  {
      final BusyCancelDialog BusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("GUI_STARTING_DEFECT_PACKAGER_KEY"),
           StringLocalizer.keyToString("CDGUI_HOME_PANEL_TITLE_KEY"),
          true);

      BusyCancelDialog.pack();
      SwingUtils.centerOnComponent(BusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            if(FileUtil.exists(Directory.getDefectPackagerDir() + File.separator + FileName.getDefectPackagerExeFileName()) == true)
            {
                String[] cmd = {Directory.getDefectPackagerDir() + File.separator + FileName.getDefectPackagerExeFileName() , DefectPackagerUtil._START_DEFECT_PACKAGER_COMMAND};//new String[4];
                Process startServiceProcess = Runtime.getRuntime().exec(cmd);//"cmd /c start \"C:\\Program Files\\DefectPackager\\Stop.bat");
            }
            //startServiceProcess.waitFor();
     
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                BusyCancelDialog.setVisible(false);
              }
            });
          }
          catch(Exception e)
          {
              // do nothing
          }
        }
      });
      BusyCancelDialog.setVisible(true);
      _defectPackagerEventObservable.stateChanged(DefectPackagerTaskEventEnum.START_DEFECT_PACKAGER);
      
      if(DefectPackagerUtil.queryService().length() > 0 )
      {
         JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString("GUI_DEFECT_PACKAGER_HAVE_BEEN_STARTED_KEY"),
                                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                    JOptionPane.INFORMATION_MESSAGE);
         _mainUI.getEnvToolBar().enableStopDefectPackagerButton(true);
         _mainUI.getEnvToolBar().enableStartDefectPackagerButton(false);
         _stopDefectPackagerMenuItem.setEnabled(true);
      }
      else
      {
        JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString("GUI_DEFECT_PACKAGER_NOT_INSTALLED_WARNING_MESSAGE_KEY"),
                                        StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
      }
  }
  
  /**
   * Brings up a dialog to allow user to generate the test coverage report
   * for the loaded project
   * @author Chin Seong, Kee
   */
  void generateTestCoverageReport()
  {
      Assert.expect(_project != null);

    if (_mainUI.isPanelInLegalState() == false)
      return;

    String testCoverageReportName = _project.getName();
    
    TestCoverageReportDialog testCoverageReportAsDialog = new TestCoverageReportDialog(_mainUI, StringLocalizer.keyToString("GUI_CONFIRM_TEST_COVERAGE_REPORT_SAVE_TITLE_KEY"), testCoverageReportName);
    SwingUtils.centerOnComponent(testCoverageReportAsDialog, _mainUI);
    testCoverageReportAsDialog.setVisible(true);
    testCoverageReportName = testCoverageReportAsDialog.getReportName();
    final String testCoverageReportExt = testCoverageReportAsDialog.getReportExt();
    final boolean isTxtFile = testCoverageReportAsDialog.isTxtFile();

    if (testCoverageReportName == null) // user cancelled out
      return;
    if (okToSaveReport(testCoverageReportAsDialog.getReportPath() + testCoverageReportAsDialog.getReportExt()) == false)
      return;
    final String reportPath = testCoverageReportAsDialog.getReportPath();

    LocalizedString savingStr = new LocalizedString("MM_GUI_SAVING_PROJECT_KEY",
        new Object[]{reportPath});
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(
      _mainUI,
      StringLocalizer.keyToString(savingStr),
      StringLocalizer.keyToString("MM_GUI_SAVE_AS_PROJECT_TITLE_KEY"),
      StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
      true);
    busyCancelDialog.addCancelActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        //do nothing
      }
    });

    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);

    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if(isTxtFile){
              TestCoverageReportTxtWriter _testCovReportWriter = new TestCoverageReportTxtWriter();
              _testCovReportWriter.open(_project, reportPath + testCoverageReportExt);
              _testCovReportWriter.close();
          }
          else{
              TestCoverageReportCSVWriter _testCovReportWriter = new TestCoverageReportCSVWriter();
              _testCovReportWriter.open(_project, reportPath + testCoverageReportExt);
              _testCovReportWriter.close();
          }

          // repopulate testDev when save is finished
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
                //do nothing
            }
          });
        }
        catch(final DatastoreException ex)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              _mainUI.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
              MessageDialog.showErrorDialog(_mainUI,
                                            ex.getLocalizedMessage(),
                                            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                            true);
            }
          });
        }
        finally
        {
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.setVisible(false);
        }
      }
    });

    busyCancelDialog.setVisible(true);
  }

  /**
 * @author khang shian, sham
 */
void generateLandPatternOverlappingList()
{
  Assert.expect(_project != null);
  String data = "";
  boolean empty = true;
  for (LandPattern landPattern : _project.getPanel().getLandPatterns())
  {
    if (ProgramVerificationUtil.is2PinDeviceLandPattern(landPattern))
    {
      if (ProgramVerificationUtil.is2PinDeviceLandPatternOverlap(landPattern))
      {
        data += landPattern.getName() + "\n\r";
        empty = false;
      }
    }
  }
  if (empty == true)
  {
    data += "No overlap landpattern";
  }
  TextDialog landPatternOverlappingListDialog = new TextDialog(_mainUI, data, StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"), StringLocalizer.keyToString("GUI_LANDPATTERN_OVERLAP_LIST_KEY"), 20, 10, true);
  SwingUtils.centerOnComponent(landPatternOverlappingListDialog, _mainUI);
  landPatternOverlappingListDialog.setVisible(true);

}

  /**
   * Brings up a dialog to allow the user to create a new project
   * @author George Booth
   */
  void saveAsProject()
  {
    //XCR-3467
    if (saveAsProjectWithFileName("") == true)
    {
      loadOrImportProject(true, _project.getName(), _project.getName());
    }
  }
  
  /**
   * Brings up a dialog to allow the user to create a new project
   * - Remove the open project portion, found bugs while opening in slow load.
   * @param filename if filename is empty, brings up a dialog to allow user to set project name
   *                 (original saveAsProject() function)
   * @author George Booth
   * @author Phang Siew Yeng
   * @author Kee Chin Seong
   * @author Chong Wei Chin 
   */
  boolean saveAsProjectWithFileName(String filename)
  {
    Assert.expect(_project != null);

    if (_mainUI.isPanelInLegalState() == false)
      return true;

    String newProjectName = "";
    String newProjectVersion = Version.getVersionNumber();
    boolean shouldSaveImageSets = false;
    SystemTypeEnum selectedSystemType = null;
    String selectedLowMag = Config.getSystemCurrentLowMagnification();
    
    //Swee yee Wong - XCR-2837 Top is not integer - dont reset virtual live mode here, it will cause assert during generating virtual live image after recipe changes
    //Swee Yee Wong - XCR-3297 Xray Camera Insufficient Trigger when generate a 3x3 Virtual Live Image
//    if(_mainUI.isVirtualLiveCurrentEnvironment())
//      _mainUI.resetVirtualLiveMode();
    
    // user-defined project name
    // default saveAsProject()
    if(filename.isEmpty())
    {
      newProjectName = _project.getName();
    
      SaveProjectAsDialog saveProjectAsDialog = new SaveProjectAsDialog(_mainUI, StringLocalizer.keyToString("GUI_CONFIRM_PROJECT_SAVE_TITLE_KEY"), newProjectName);
      SwingUtils.centerOnComponent(saveProjectAsDialog, _mainUI);
      saveProjectAsDialog.setVisible(true);
      newProjectName = saveProjectAsDialog.getNewProjectName();
      shouldSaveImageSets = saveProjectAsDialog.getShouldCopyImageSets();
      newProjectVersion = saveProjectAsDialog.getSelectedVersion();
      selectedSystemType = saveProjectAsDialog.getSelectedSystemType(); 
      selectedLowMag = saveProjectAsDialog.geSelectedSystemMagnification();

      if (newProjectName == null) // user cancelled out
      {
        return false;
      }

      // check for a valid project name
      // keep asking for a valid name until you get one or they cancel out
      while ((newProjectName != null) &&
            (newProjectName.equalsIgnoreCase(_project.getName()) ||
            (_project.isProjectNameValid(newProjectName) == false)))
      {
        LocalizedString message;
        if (newProjectName.equalsIgnoreCase(_project.getName()))
          message = new LocalizedString("GUI_SAVE_AS_PROJECT_SAME_NAME_KEY",
                                        new Object[]{newProjectName});
        else
          message = new LocalizedString("GUI_SAVE_AS_INVALID_PROJECT_NAME_KEY",
                                        new Object[]{newProjectName, _project.getProjectNameIllegalChars()});
        JOptionPane.showMessageDialog(_mainUI,
                                      StringLocalizer.keyToString(message),
                                      StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                      JOptionPane.ERROR_MESSAGE);

        // try again -- bring up the dialog
        saveProjectAsDialog = new SaveProjectAsDialog(_mainUI, StringLocalizer.keyToString("GUI_CONFIRM_PROJECT_SAVE_TITLE_KEY"), newProjectName);
        SwingUtils.centerOnComponent(saveProjectAsDialog, _mainUI);
        saveProjectAsDialog.setVisible(true);
        newProjectName = saveProjectAsDialog.getNewProjectName();
        shouldSaveImageSets = saveProjectAsDialog.getShouldCopyImageSets();
        newProjectVersion = saveProjectAsDialog.getSelectedVersion();
        selectedSystemType = saveProjectAsDialog.getSelectedSystemType();
        selectedLowMag = saveProjectAsDialog.geSelectedSystemMagnification();
      }
       
      if (newProjectName == null) 
      {
        // user cancelled out
        return false;
      }
      if (okToSaveProject(newProjectName) == false)
      {
        return false;
      }
    }
    else // predefined project name
    {
      newProjectName = filename;
      shouldSaveImageSets = false;
      newProjectVersion = Version.getVersionNumber();
      selectedSystemType = XrayTester.getSystemType();
      selectedLowMag = Config.getSystemCurrentLowMagnification();
    
      while (okToSaveProject(newProjectName) == false)
      {
        // try again -- bring up the dialog
        SaveProjectAsDialog saveProjectAsDialog = new SaveProjectAsDialog(_mainUI, StringLocalizer.keyToString("GUI_CONFIRM_PROJECT_SAVE_TITLE_KEY"), newProjectName);
        SwingUtils.centerOnComponent(saveProjectAsDialog, _mainUI);

        saveProjectAsDialog.setEnabledSaveImageSetsCheckBox(false);

        saveProjectAsDialog.setVisible(true);
        newProjectName = saveProjectAsDialog.getNewProjectName();
        
        if (newProjectName == null) // user cancelled out
          return true;
      }
    }
    
    final String projectName = newProjectName;
    final boolean saveImageSets = shouldSaveImageSets;
    final String projectVersion = newProjectVersion;
    final String newLowMagnification = selectedLowMag;
    boolean saveRevisionDifferentWithRecipeRevision =false;
    if (VersionUtil.isRequiredVersion(projectVersion) == false)
    {
      saveRevisionDifferentWithRecipeRevision = true;
      RecipeConverter.getInstance().setConvertRevision(projectVersion);
      RecipeConverter.getInstance().setOrigninalRevision(Version.getVersionNumber());
    }
    
    // if License is not OLP , the selectedtSystemtype is null; 
    if (selectedSystemType != null)
    {
      try
      {

        _project.setSaveAsSystemType(selectedSystemType);   
        
        if (selectedSystemType.equals(SystemTypeEnum.S2EX))
        {
           _project.setSaveAsLowMagnification(newLowMagnification);
        }
        
        _project.setPreviousSystemType(XrayTester.getSystemType());
        
        
        //this code here is to check whether user have decide to save is older version or not
        //if save as older version , system behavior is to reload current project;
        //so do not load new setting in software
        //only reload config value when user save as current software version
        
        if (saveRevisionDifferentWithRecipeRevision == false)
        {
          Config.getInstance().forceLoad(selectedSystemType);
          ProgramGeneration.reintializeDueToSystemTypeChange();
          com.axi.v810.datastore.projReaders.ProjectReader.reintializeDueToSystemTypeChange();
          ProjectWriter.reintializeDueToSystemTypeChange();
          Project.setupProjectReaderWriter();
          if (selectedSystemType.equals(SystemTypeEnum.S2EX))
          {
             enableChangeMagnificationMenu();
            _mainUI.getEnvToolBar().enableChangeSystemMagnificationButton();
            _mainUI.getMainToolBar().enableChangeSystemMagnificationButton();
          }
          else
          {
            disableChangeMagnificationMenu();
            _mainUI.getEnvToolBar().disableChangeSystemMagnificationButton();
            _mainUI.getMainToolBar().disableChangeSystemMagnificationButton();
          }
          MagnificationEnum.resetMagnification();
          _mainUI.getTestDev().refresh();
        }              
      }
      catch (final DatastoreException e)
      {
        _mainUI.handleOfflineProgrammingDataStoreError(e);
        _project.cancelSaveAs();
      }
    } 
    final boolean revertRecipeRevision = saveRevisionDifferentWithRecipeRevision;
    LocalizedString savingStr = new LocalizedString("MM_GUI_SAVING_PROJECT_KEY",
        new Object[]{projectName});
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(
      _mainUI,
      StringLocalizer.keyToString(savingStr),
      StringLocalizer.keyToString("MM_GUI_SAVE_AS_PROJECT_TITLE_KEY"),
      StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
      true);
    busyCancelDialog.addCancelActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _project.cancelSaveAs(); // or some method from Bill
      }
    });
    
    busyCancelDialog.addWindowListener(new WindowAdapter()
    {   
      public void windowClosing(WindowEvent e) 
      {
        _project.cancelSaveAs();
      }
    });
    
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);

    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          RecipeConverter.getInstance().setConvertRecipe(revertRecipeRevision);
          _project.saveAs(projectName, saveImageSets);
          String recipeDirectory = Directory.getProjectDir(projectName);
          if (RecipeConverter.getInstance().isConvertRecipe() == false)
          {
            _mainUI.getTestDev().getPersistance().writeSettings(projectName);
            // create the cad image!
            DrawCadPanel.createPanelImage(_project);
          }
          else
          {
            RecipeConverter.getInstance().convert(recipeDirectory);
          }
        }
        catch (final CouldNotDeleteFileException | XrayTesterException ex)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              _mainUI.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
              MessageDialog.showErrorDialog(_mainUI,
                                            ex.getLocalizedMessage(),
                                            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                            true);
            }
          });
        }
        finally
        {
          RecipeConverter.getInstance().reset();
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.setVisible(false);
        }
      }
    });

    busyCancelDialog.setVisible(true);
    return true;
  }

  /**
   * Brings up a dialog to allow the user to pick which existing project to load
   * @author George Booth
   */
  void projectOpen()
  {
    if (_mainUI.getTestDev().okToCloseProject())
    {
      // true means load project, not import ndfs
      userLoadOrImportProject(true);
      // _mainUI.projectOpen called if successful
    }
  }

  /**
   * User requested the project be closed.  This only sets flags - there is no
   * method to actually remove the project from memory.
   * @author George Booth
   */
  public void projectClose(boolean noUserPrompt)
  {
    try
    {
      //Kok Chun, Tan
      //set this flag to false when unload a new recipe.
      //So that, when save recipe, savePspComponents method in PspAutoPopulatePanel.java will not save the psp components.
      //Until you click into PspAutoPopulatePanel, this flag only will turn true.
      //This is to confirm that the _temporarycomponents in PspAutoPopulatePanel.java have a correct components list.
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP))
        PspAutoPopulatePanel.getInstance().setIsGoIntoPspAutoPopulateBefore(false);
      
      //Swee Yee Wong - XCR-3297 Xray Camera Insufficient Trigger when generate a 3x3 Virtual Live Image
      if(_mainUI.isVirtualLiveCurrentEnvironment())
        _mainUI.resetVirtualLiveMode();
      
      if (noUserPrompt || _mainUI.getTestDev().okToCloseProject())
      {
        _project = null;
        _isProjectLoaded = false;
        _mainUI.projectClose();
      }
      if(noUserPrompt == false)
      {
        _mainUI.getMainToolBar().switchToAXIHome();
        _ProjectHistoryLog.append("Recipe is close.");
      }
      try
      {
        if (LicenseManager.isOfflineProgramming())
        {
          _mainUI.getMainMenuBar().disableChangeMagnificationMenu();
          _mainUI.getMainToolBar().disableChangeSystemMagnificationButton();
          _mainUI.getEnvToolBar().disableChangeSystemMagnificationButton();
        }
      }
      catch (final BusinessException e)
      {
        finishLicenseMonitor();
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
              e.getMessage(),
              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
              true);
            MainMenuGui.getInstance().exitV810(false);
          }
        });  
      }
    }
    catch (XrayTesterException xtex)
    {
      _mainUI.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      MessageDialog.showErrorDialog(_mainUI,
                                    xtex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * Brings up a dialog to allow the user to pick which ndf application to import
   * @author George Booth
   */
  void importProjectMenuItem_actionPerformed()
  {
    if (_mainUI.getTestDev().okToCloseProject())
    {
      // false means import ndfs, not load an existing project
      Object[] options = {StringLocalizer.keyToString("MM_GUI_IMPORT_NDF_KEY"),
                          StringLocalizer.keyToString("MM_GUI_IMPORT_CAD_KEY")}; 
      /*
       * Chin-Seong, Kee - Prompt up user choose whether they wan dummy project or selected project
       */
      String prompt = StringLocalizer.keyToString(new LocalizedString("MM_GUI_IMPORT_PROJECT_SETTING_MESSAGE_KEY",
                                                  new Object[]{}));
          
      ChoiceRadioButtonDialog loadDialog = new ChoiceRadioButtonDialog(MainMenuGui.getInstance(),
                                                       StringLocalizer.keyToString("MM_GUI_IMPORT_TITLE_KEY"),
                                                       prompt,
                                                       StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                                       StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                                       options,
                                                       null);
          
      int retVal = loadDialog.showDialog();
      if (retVal == JOptionPane.OK_OPTION)
      {
        if(loadDialog.getSelectedValue().equals(StringLocalizer.keyToString("MM_GUI_IMPORT_NDF_KEY")) == true)
        {
          userLoadOrImportProject(false);
        }
        else
        {
          userImportCAD();
        }
      }
    }
  }
  
  /*
   * @author chin-seong, kee
   * This dialog bring up to let user choose, to create the CAD in V810
   */
  Project userCreateCadSettings()
  {
      Object[] options = {StringLocalizer.keyToString("MMGUI_CAD_CREATOR_IMPORT_PROJECT_KEY"),
                          StringLocalizer.keyToString("MMGUI_START_EMPTY_PROJECT_KEY")}; 
      /*
       * Chin-Seong, Kee - Prompt up user choose whether they wan dummy project or selected project
       */
      String prompt = StringLocalizer.keyToString(new LocalizedString("MMGUI_CAD_CREATOR_PROJECT_CHOICE_DIALOG_TITLE_KEY",
                                                  new Object[]{}));
          
      ChoiceRadioButtonDialog loadDialog = new ChoiceRadioButtonDialog(MainMenuGui.getInstance(),
                                                       StringLocalizer.keyToString("MMGUI_CAD_CREATOR_PROJECT_CHOICE_DIALOG_TITLE_KEY"),
                                                       prompt,
                                                       StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                                       StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                                       options,
                                                       null);
          
      int retVal = loadDialog.showDialog();
      
      if (retVal == JOptionPane.OK_OPTION) 
      {
         if(loadDialog.getSelectedValue().equals(StringLocalizer.keyToString("MMGUI_START_EMPTY_PROJECT_KEY")) == true)
         {
            CadCreatorProjectSettingsDialog dlg = new CadCreatorProjectSettingsDialog(_mainUI);
            SwingUtils.centerOnComponent(dlg, _mainUI);
            dlg.setVisible(true);

            try
            { 
              if(dlg.isUserCheckOk() == true)
              {
                //to do list :: create a dummy project with panel width height and thickness
                Project newProject = VirtualLiveManager.getInstance().createCadProject(dlg.getUserDefinedProjectName(),
                                                                                         dlg.getPanelWidthInNanometers(), 
                                                                                         dlg.getPanelLengthInNanometers(),
                                                                                         dlg.getPanelThicknessInNanometers());

                //set ast project save version
                newProject.setSoftwareVersionOfProjectLastSave("1.0");
                //set Thickness
                newProject.setThicknessTableVersion(0);
                //load the project to _currentProject
                newProject.loadNewEmptyProject(newProject);
                //setting Inisital Threshold
                newProject.setInitialThresholdsSetName(ProjectInitialThresholds.getInstance().getSystemDefaultSetName());
                //setting Initial Recipe Setting
                newProject.setInitialRecipeSettingSetName(ProjectInitialRecipeSettings.getInstance().getSystemDefaultSetName());
                //Populate it
                MainMenuGui.getInstance().populateCadCreatorProjectData(newProject, true);
                //reassign project 
                //_project = newProject;

                return newProject;
              }
              else 
               return null;
             }
             catch(DatastoreException ex)
             {
               System.out.println(ex.getMessage());
             }
         }
         else
         {
            return userLoadOrImportSavedProgressProject();  
         }
    }
    return null;
  }
  
  /**
   * @author chin-seong, kee
   * This dialog bring up to let user choose, whether in virtual live mode,
   * user wan to create dummy project OR selected project.
   */
  boolean userCreateDummyOrLoadSelectedProject()
  {
      Object[] options = {StringLocalizer.keyToString("MMGUI_VIRTUAL_LIVE_SELECTED_PROJECT_KEY"),
                          StringLocalizer.keyToString("MMGUI_VIRTUAL_LIVE_DUMMY_PROJECT_KEY")}; 
      /*
       * Chin-Seong, Kee - Prompt up user choose whether they wan dummy project or selected project
       */
      String prompt = StringLocalizer.keyToString(new LocalizedString("MMGUI_VIRTUAL_LIVE_PROJECT_CHOICE_DIALOG_KEY",
                                                  new Object[]{}));
          
      ChoiceRadioButtonDialog loadDialog = new ChoiceRadioButtonDialog(MainMenuGui.getInstance(),
                                                       StringLocalizer.keyToString("MMGUI_VIRTUAL_LIVE_PROJECT_CHOICE_DIALOG_TITLE_KEY"),
                                                       prompt,
                                                       StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                                       StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                                       options,
                                                       null);
          
      int retVal = loadDialog.showDialog();
      
      if (retVal == JOptionPane.OK_OPTION) 
      {
         if(loadDialog.getSelectedValue().equals(StringLocalizer.keyToString("MMGUI_VIRTUAL_LIVE_DUMMY_PROJECT_KEY")) == true)
         {
             VirtualLiveDummyProjectSettingsDialog dlg = new VirtualLiveDummyProjectSettingsDialog(_mainUI);
             SwingUtils.centerOnComponent(dlg, _mainUI);
             dlg.setVisible(true);
             
             try
             { 
               if(dlg.isNeedCreateDummyProject() == true)
               {
                 //to do list :: create a dummy project with panel width height and thickness
                 Project dummyProject = VirtualLiveManager.getInstance().createDummyProject(dlg.getPanelWidthInNanometers(), dlg.getPanelLengthInNanometers(), dlg.getPanelThicknessInNanometers());
                
                 // put 5000 ms of extra delay during unload to prevent board chop
                 dummyProject.setPanelExtraClearDelayInMiliSeconds(PanelHandler.getDefaultExtraClearDelayInMiliseconds());
                 
                 //load the project to _currentProject
                 dummyProject.loadVirtualLiveDummyProject(dummyProject);

                 //Populate it
                 MainMenuGui.getInstance().populateVirtualLiveProjectData(dummyProject);
                //VirtualLiveManager.getInstance().createDummyTestProgram(_menuRequesterID, _currentUser, _menuRequesterID, _currentUser, _currentUser, _currentUser, _menuRequesterID, _currentUser, _index, retVal, _currentUser, _currentUser, retVal)
                 return true;
               }
               else 
                 return false;
             }
             catch(DatastoreException ex)
             {
               System.out.println(ex.getMessage());
             }
         }
         else if(loadDialog.getSelectedValue().equals(StringLocalizer.keyToString("MMGUI_VIRTUAL_LIVE_SELECTED_PROJECT_KEY")) == true)
         {
            userLoadOrImportProject(true);
            if(_project == null)
              return false;
            else
              return true;
         }
      }
      else // the use hit cancel or entered an empty string -- change nothing
        return false;
      
      return false;
  }
  
  /*
   * @author Kee Chin Seong
   */
  Project userLoadOrImportSavedProgressProject()
  {
    String title = null;
    String message = null; 
    
    java.util.List<String> panels = null;
    
    panels = FileName.getSavedProgressProjectNames();
    if (panels == null || panels.size() == 0)
    {
      noProjectsMessage();
      return null;
    }
    title = StringLocalizer.keyToString("MM_GUI_LOAD_PROJECT_TITLE_KEY");
    message = StringLocalizer.keyToString("MM_GUI_LOAD_PROJECT_MESSAGE_KEY");
    Object[] possibleValues = panels.toArray();
    ChoiceInputDialog panelChoiceDialog = new ChoiceInputDialog(
        _mainUI,
        title,
        message,
        StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
        StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
        possibleValues);
    SwingUtils.centerOnComponent(panelChoiceDialog, _mainUI);
    int returnValue = panelChoiceDialog.showDialog();
    if (returnValue == JOptionPane.OK_OPTION)
    {
      final String projectToLoad = (String)panelChoiceDialog.getSelectedValue();
      return userLoadSaveProgessEmptyCad(projectToLoad);
    }
    
    return null;
  }
  
  /*
   * @author Kee Chin Seong
   * - Cad Creator Save Progress Load
   */
  Project userLoadSaveProgessEmptyCad(final String projectName)
  {
      ProjectObservable.getInstance().setEnabled(false);
      try
      {
          Project newProject = Project.loadCadCreatorProgram(projectName, BooleanRef.TRUE);
          newProject.loadNewEmptyProject(newProject);
          newProject.setInitialThresholdsSetName(ProjectInitialThresholds.getInstance().getSystemDefaultSetName());
          newProject.setInitialRecipeSettingSetName(ProjectInitialRecipeSettings.getInstance().getSystemDefaultSetName());
          //Populate it
          MainMenuGui.getInstance().populateCadCreatorProjectData(newProject, false);

          ProjectObservable.getInstance().setEnabled(true);

          return newProject;
      }
      catch(XrayTesterException ex)
      {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        ex.getMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                        true);
          ProjectObservable.getInstance().setEnabled(true);
          return null;
      }
  }
  
  /**
   * Brings up a dialog to allow the user to pick which application to load or import
   * based on the passed parameter
   * @author George Booth
   * @author Jack Hwee
   */
  void userLoadOrImportProject(final boolean loadProject)
  {
    // get projects to display (load x5000, import 5DX)
    java.util.List<String> panels = new ArrayList<>();
    java.util.List<String> panels2 = null;
    String title = null;
    String message = null;
    if (loadProject)
    {
      // get x5000 project names
      //Punit: XCR-3040
      for(String project : FileName.getProjectNames())
      {
        if(Project.isProjectNameValid(project))
        {
          panels.add(project);
        }
      }
      if (panels == null || panels.size() == 0)
      {
        noProjectsMessage();
        return;
      }
      title = StringLocalizer.keyToString("MM_GUI_LOAD_PROJECT_TITLE_KEY");
      message = StringLocalizer.keyToString("MM_GUI_LOAD_PROJECT_MESSAGE_KEY");
      Object[] possibleValues = panels.toArray();
      ChoiceInputDialog panelChoiceDialog = new ChoiceInputDialog(
          _mainUI,
          title,
          message,
          StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
          StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
          possibleValues);
      SwingUtils.centerOnComponent(panelChoiceDialog, _mainUI);
      int returnValue = panelChoiceDialog.showDialog();
      if (returnValue == JOptionPane.OK_OPTION)
      {
        final String projectToLoad = (String)panelChoiceDialog.getSelectedValue();
        // check for a valid project name
        if (Project.isProjectNameValid(projectToLoad) == false)
        {
          invalidProjectNameMessage(projectToLoad, false);
          return;  // error, do not continue
        }
        loadOrImportProject(loadProject, projectToLoad, projectToLoad);
      }
    }
    else // doing an IMPORT
    {
      // Jack Hwee- get 5DX project names
      panels2 = FileName.get5dxNdfProjectNames();
      panels = FileName.getNdfProjectNames();

      NdfReader.set5dxProjectFlag(false);
      NdfReader.set5dxNdfFlag(false);
      NdfReader.clear5dxProjectNamesToProjectHashNameMap();

      if ((panels2 == null || panels2.size() == 0) && (panels == null || panels.size() == 0))
      {
        noNdfProjectsMessage();
        return;
      }

      title = StringLocalizer.keyToString("MM_GUI_IMPORT_PROJECT_TITLE_KEY");
      
      ImportNdfDialog importDialog = new ImportNdfDialog(_mainUI,title);      
      int returnValue = importDialog.showDialog();

      if (returnValue == JOptionPane.OK_OPTION)
      {
        try
        {
          if (LicenseManager.isOfflineProgramming())
          {
            String sytemTypeSelected = (String)importDialog.getSelectedSystemType();
            if (sytemTypeSelected.equalsIgnoreCase(SystemTypeEnum.STANDARD.getName()))
            {
              sytemTypeSelected = SystemTypeEnum.THROUGHPUT.getName();
            }
            String selectedLowMag = (String)importDialog.getSelectedSystemMagnification();
            SystemTypeEnum systemType = SystemTypeEnum.getSystemType(sytemTypeSelected);
            try 
            {
              Config.getInstance().forceLoad(systemType);
              ProgramGeneration.reintializeDueToSystemTypeChange();
              com.axi.v810.datastore.projReaders.ProjectReader.reintializeDueToSystemTypeChange();
              ProjectWriter.reintializeDueToSystemTypeChange();
              Project.setupProjectReaderWriter();
              if (systemType.equals(SystemTypeEnum.S2EX))
              {
                Config.getInstance().setValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION, selectedLowMag);
                Config.getInstance().save(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION.getFileName());
                enableChangeMagnificationMenu();
                _mainUI.getMainToolBar().enableChangeSystemMagnificationButton();
              }
              else
              {
                Config.getInstance().setValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION, "M19");
                Config.getInstance().save(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION.getFileName());
                disableChangeMagnificationMenu();
                _mainUI.getEnvToolBar().disableChangeSystemMagnificationButton();
              }
              MagnificationEnum.resetMagnification();
              _mainUI.getTestDev().refresh();
            }
            catch (DatastoreException e)
            {
              _mainUI.handleOfflineProgrammingDataStoreError(e);
              return;
            }
          }
        }
        catch (final BusinessException e)
        {
          finishLicenseMonitor();
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                      e.getMessage(),
                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                      true);
              MainMenuGui.getInstance().exitV810(false);
            }
          });
        }
        
        final String projectToLoad = (String)importDialog.getSelectedRecipe();
        final String choiceList = (String)importDialog.getSelectedNdfType();
        String newProjectName = importDialog.getNewProjectName();
        
        //Set flag
        if(choiceList == StringLocalizer.keyToString("MMGUI_SELECT_5DX_NDF_TYPE_KEY"))
        {
           NdfReader.set5dxProjectFlag(true);
           NdfReader.set5dxProjectNameTo5dxHashNameMap(FileName.get5dxNdfProjectNamesToProjectHashNameMap());
        }
        else
        {
           NdfReader.set5dxProjectFlag(false);
           NdfReader.clear5dxProjectNamesToProjectHashNameMap();
        }

        // check for a valid project name
        if (Project.isProjectNameValid(newProjectName) == false)
        {
          invalidProjectNameMessage(newProjectName, true);
          return;  // error, do not continue
        }

        String initialThresholdsFileName = (String)importDialog.getSelectedThresholdsSet();
        try
        {
          ProjectInitialThresholds.getInstance().setImportInitialThresholdsFileNameWithoutExtension(initialThresholdsFileName);
        }
        catch (DatastoreException ex)
        {
          // something's wrong with software.config. Show the error and stop the import
          MessageDialog.showErrorDialog(_mainUI,
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                        true);

          return;
        }
        
        String initialRecipeSettingFileName = (String)importDialog.getSelectedRecipeSettingSet();
        try
        {
          ProjectInitialRecipeSettings.getInstance().setImportInitialRecipeSettingFileNameWithoutExtension(initialRecipeSettingFileName);
        }
        catch (DatastoreException ex)
        {
          // something's wrong with software.config. Show the error and stop the import
          MessageDialog.showErrorDialog(_mainUI,
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                        true);

          return;
        }
        
        String jointTypeAssignmentFileName = (String)importDialog.getSelectedJointTypeAssignment();
        try
        {
          JointTypeEnumAssignment.getInstance().setJointTypeAssignmentConfigFileNameWithoutExtension(
              jointTypeAssignmentFileName);
        }
        catch (DatastoreException ex)
        {
          // something's wrong with software.config. Show the error and stop the import
          MessageDialog.showErrorDialog(_mainUI,
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                        true);

          return;
        }
                
         // if importing, check if an x6000 project already exists
          // issue warning if the user is trying to import again
          if (okToImportProject(newProjectName) == false) {
            return;
          }
          else {
              if (NdfReader.is5dxProjectFlag() == false){
              String outputDirectory = Directory.getProjectDir(newProjectName);
              File f1 = new File(outputDirectory);
                try
                {
                  FileUtil.delete(outputDirectory);
                }
                catch (CouldNotDeleteFileException ex)
                {
                  ex.printStackTrace();
                }
              f1.delete();
            }
          } 

        loadOrImportProject(loadProject, projectToLoad, newProjectName);
      }
   
    }
  }

  /**
   * @author George Booth
   */
  private void invalidProjectNameMessage(String projectName, boolean insertFixMessage)
  {
    LocalizedString localizedMessage = new LocalizedString("GUI_SAVE_AS_INVALID_PROJECT_NAME_KEY",
        new Object[]{projectName, Project.getProjectNameIllegalChars()});

    String finalMessage = StringLocalizer.keyToString(localizedMessage);
    if (insertFixMessage)
    {
      finalMessage += "\n\n" + StringLocalizer.keyToString("GUI_IMPORT_FIX_PROJECT_NAME_KEY");
    }

    JOptionPane.showMessageDialog(
      _mainUI,
      finalMessage,
      StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
      JOptionPane.ERROR_MESSAGE);
  }

  /**
   * @author George Booth
   */
  void noProjectsMessage()
  {
    LocalizedString projectPathMessage = new LocalizedString("MM_GUI_NO_PROJECTS_KEY",
                                                 new Object[]{Directory.getProjectsDir()});

    MessageDialog.showInformationDialog(
        _mainUI,
        StringLocalizer.keyToString(projectPathMessage),
        StringLocalizer.keyToString("MM_GUI_NO_PROJECTS_TITLE_KEY"),
        true);
  }

  /**
   * @author George Booth
   */
  void noNdfProjectsMessage()
  {
    LocalizedString projectPathMessage = new LocalizedString("MM_GUI_NO_NDF_PROJECTS_KEY",
        new Object[]
        {Directory.getLegacyNdfDir()});
    MessageDialog.showInformationDialog(
        _mainUI,
        StringLocalizer.keyToString(projectPathMessage),
        StringLocalizer.keyToString("MM_GUI_NO_NDF_PROJECTS_TITLE_KEY"),
        true);
  }
  
  void noCADFileMessage()
  {
    LocalizedString projectPathMessage = new LocalizedString("MM_GUI_NO_CAD_PROJECTS_KEY",
        new Object[]
        {Directory.getCadDirectory()});
    MessageDialog.showInformationDialog(
        _mainUI,
        StringLocalizer.keyToString(projectPathMessage),
        StringLocalizer.keyToString("MM_GUI_NO_CAD_PROJECTS_TITLE_KEY"),
        true);
  }

  /**
   * @author Bee Hoon
   */
  void unableToLoadProjectMessage(String projectToLoad)
  {
    LocalizedString projectNotLoadedMessage = new LocalizedString("MM_GUI_UNABLE_TO_LOAD_PROJECT_KEY",
                                              new Object[]{projectToLoad});
    MessageDialog.showErrorDialog(
        _mainUI,
        StringLocalizer.keyToString(projectNotLoadedMessage),
        StringLocalizer.keyToString("MM_GUI_UNABLE_TO_LOAD_PROJECT_TITLE_KEY"),
        true);
  }
  
  /**
   * Loads or imports a project based on the passed parameters
   * @author George Booth
   */
  public void loadOrImportProject(final boolean loadProject, final String projectToLoad)
  {
    Assert.expect(projectToLoad != null);
    if (_mainUI.getTestDev().okToCloseProject())
    {
      loadOrImportProject(loadProject, projectToLoad, projectToLoad);
      
      //Added by Jack Hwee - XCR1165, to check pin orientation which will causes assert when doing inspection
      if (_isProjectLoaded)
      {
        ProgramVerificationUtil.checkAndModifyTwoPinCompPackageOrientation(_project);
        
        //XCR-2579 - perform checking only when project is loaded.
        //Siew Yeng - XCR-2549
        checkAndModifyDynamicRangeOptimizationLevel(_project, Config.getMaximumDynamicRangeOptimizationLevel());
        checkAndModifyNewScanRouteGenerationOption(_project);
      }
      
      if (_isProjectLoaded && 
         ( _config.getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED) == false && _config.getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED) ==false) && 
         _project.getPanel().useVariableMagnification())
      {
        checkAndModifySubtypeMagnification(_project);
      }
    }
  }

  /**
   * Loads or imports a project based on the passed parameters
   * @author George Booth
   */
  public void loadProjectForProductionTest(final String projectToLoad, Font font, final BooleanRef abortedDuringLoad) throws XrayTesterException
  {
    Assert.expect(font != null);
    Font originalFont = _dialogFont;
    _dialogFont = font;

    _productionLoadException = null;
    LocalizedString userMessage = new LocalizedString("MM_GUI_LOADING_PROJECT_KEY",
        new Object[]{projectToLoad});
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(
        _mainUI,
        StringLocalizer.keyToString(userMessage),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    SwingUtils.setFont(busyCancelDialog, _dialogFont);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);

    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          // this will do the auto-pull if necessary
//          BooleanRef abortedDuringLoad = new BooleanRef();
          
          //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT))
          {
            try
            {
              _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.START_CHANGE_OVER);
            }
            catch (DatastoreException ex)
            {
              String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_ERROR_MESSAGE_KEY");
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                            errorMsg,
                                            StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_EVENT_TITLE_KEY"),
                                            true,
                                            FontUtil.getMediumFont());
            }
          }
          
          //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
          {
            try
            {
              _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.PRODUCTION_RUNNING);
            }
            catch (DatastoreException ex)
            {
              String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_MESSAGE_KEY");
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                            errorMsg,
                                            StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_EVENT_TITLE_KEY"),
                                            true,
                                            FontUtil.getMediumFont());
            }
          }
          
          _project = Project.loadIfNotAlreadyLoaded(projectToLoad, abortedDuringLoad);
          if(abortedDuringLoad.getValue())
            _isProjectLoaded = false;
          else
            _isProjectLoaded = true;
        }
        catch (final XrayTesterException ex)
        {
          _productionLoadException = ex;
        }
        finally
        {
          if (_isProjectLoaded)
          {
            //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
            if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT))
            {
              try
              {
                _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.STOP_CHANGE_OVER);
              }
              catch (DatastoreException ex)
              {
                String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_ERROR_MESSAGE_KEY");
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                              errorMsg,
                                              StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_EVENT_TITLE_KEY"),
                                              true,
                                              FontUtil.getMediumFont());
              }
            }

            //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
            if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
            {
              try
              {
                _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING);
                _machineLatestStatusLogWriter.writeMachineLatestStatusLog(MachineLatestStatusElementEventEnum.CURRENT_ACTIVE_PROGRAM, _project.getName());
              }
              catch (DatastoreException ex)
              {
                String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_MESSAGE_KEY");
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                              errorMsg,
                                              StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_EVENT_TITLE_KEY"),
                                              true,
                                              FontUtil.getMediumFont());
              }
            }

            //Khaw Chek Hau - XCR2654: CAMX Integration
            if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
            {
              try
              {
                _shopFloorSystem.recipeLoaded(_project.getName());
              }
              catch (DatastoreException ex)
              {
                String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("SHOP_FLOOR_ERROR_MESSAGE_KEY_KEY");
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                              errorMsg,
                                              StringLocalizer.keyToString("SHOP_FLOOR_EVENT_TITLE_KEY"),
                                              true);
              }
            }
          }
          
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.setVisible(false);
        }
      }
    });
    busyCancelDialog.setVisible(true);
    
    _dialogFont = originalFont;
    if (_productionLoadException != null)
      throw _productionLoadException;
    if(_isProjectLoaded)
      _mainUI.projectOpen(_project);
    
    // Bee Hoon, XCR1650 - Open recipe and immediately save recipe, it will crash
    if(_projectErrorLogUtil.getIsLandPatternNameNotFoundError())
    {  
       LocalizedString message = new LocalizedString("MMGUI_RECIPE_LAND_PATTERN_NAME_NOT_FOUND_ERROR_KEY",
                                   new Object[]{Directory.getProjectDir(_project.getName())});
       int answer = JOptionPane.showConfirmDialog(_mainUI,
                                                 StringLocalizer.keyToString(message),
                                                 StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                 JOptionPane.YES_NO_OPTION,
                                                 JOptionPane.WARNING_MESSAGE);
      if (answer == JOptionPane.YES_OPTION)
      {
        // loop thru all the components to search for any components that using the extra component package
        _componentListUsingCompPackage = _project.getPanel().getComponentListUsingCompPackage(_projectErrorLogUtil.getUnusedCompPackage());
        
        if(_componentListUsingCompPackage != null)
        {
          LocalizedString compPackageFoundMessage = new LocalizedString("MMGUI_RECIPE_COMPONENT_ASSIGNED_TO_EXTRA_COMP_PACKAGE_KEY",
                                                        new Object[]{Directory.getProjectDir(_project.getName())});
          MessageDialog.showWarningDialog(_mainUI, StringLocalizer.keyToString(compPackageFoundMessage), StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          abortedDuringLoad.setValue(true);
          projectClose(false);
        }
        else
        {
          saveProject();
          _projectErrorLogUtil.setIsLandPatternNameNotFoundError(false);
        }
      }
      else if (answer == JOptionPane.NO_OPTION)
      {
        abortedDuringLoad.setValue(true);
        projectClose(true);
      }
    }
    
    //Siew Yeng - XCR1745 - Semi-automated mode
    //Vincent Tan - XCR-3246 - System crash when load different system type recipe at production tab in Semi-Automated mode
    if(_project != null && TestExecution.getInstance().isSemiAutomatedModeEnabled())
    { 
      if(_project.isSemiAutomatedRecipe() == false)
      {
        int answer = JOptionPane.showConfirmDialog(_mainUI,
                                                 StringLocalizer.keyToString("MM_GUI_RECIPE_NOT_SEMI_AUTOMATED_KEY"),
                                                 StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                 JOptionPane.YES_NO_OPTION,
                                                 JOptionPane.QUESTION_MESSAGE);
        
        if(answer == JOptionPane.YES_OPTION)
        {
          for(ComponentType compType : _project.getPanel().getComponentTypes())
          {
            compType.getComponentTypeSettings().setInspected(false);
          }
          saveAsProjectWithFileName(FileName.getSemiAutomatedProjectName(_project.getName()));
        }
        else
        {
          abortedDuringLoad.setValue(true);
          projectClose(true);
        }
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private boolean autoUpdateProject(String projectName, boolean interactWithUser)
  {
    if (ProjectDatabase.isAutoUpdateLocalProjectEnabled())
    {
      try
      {
        if (ProjectDatabase.doesNewerProjectExist(projectName))
        {
          if (interactWithUser)
          {
            // ask user what they want to do
            int answer = JOptionPane.showConfirmDialog(_mainUI,
                StringLocalizer.keyToString("MMGUI_AUTO_PULL_CONFIRM_KEY"),
                StringLocalizer.keyToString("HP_CONFIRM_PULL_TITLE_KEY"),
                JOptionPane.YES_NO_CANCEL_OPTION);
            if (answer == JOptionPane.YES_OPTION)
            {
              // get the new version
              /** @todo put this on a worker thread as it may take a while */
              // use a busy (not cancel) dialog
              ProjectDatabase.getProject(projectName);
              _isProjectLoaded = false;
              return true;
            }
            else if (answer == JOptionPane.NO_OPTION)
            {
              // get the old version -- don't need to do much
              return true;
            }
            else
              return false;
          }
          else
          {
            // get the new version
            ProjectDatabase.getProject(projectName);
            _isProjectLoaded = false;
          }
        }
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(
              _mainUI,
              ex.getMessage(),
              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
              true);
      }
    }
    return true;
  }

  /**
   * Loads or imports a project based on the passed parameters, renaming the project to the new name
   * @author George Booth
   * @author Kee chin Seong
   * - Switch back to AXI HOME
   */
  public void loadOrImportProject(final boolean loadProject, final String projectToLoad, final String newProjectName)
  {
    _mainUI.getMainToolBar().switchToAXIHome();
    
    Assert.expect(projectToLoad != null);
    Assert.expect(newProjectName != null);

    //Kok Chun, Tan
    //set this flag to false when load a new recipe.
    //So that, when save recipe, savePspComponents method in PspAutoPopulatePanel.java will not save the psp components.
    //Until you click into PspAutoPopulatePanel, this flag only will turn true.
    //This is to confirm that the _temporarycomponents in PspAutoPopulatePanel.java have a correct components list.
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP))
      PspAutoPopulatePanel.getInstance().setIsGoIntoPspAutoPopulateBefore(false);
    
    // auto-pull check should happen here -- give the user a prompt, allow them to yes/no/cancel
    if (loadProject)
    {
      if (autoUpdateProject(projectToLoad, true) == false)
        return;
    }
    projectClose(true);

    String title;
    LocalizedString userMessage;
    if (loadProject)
    {
      title = StringLocalizer.keyToString("MM_GUI_LOAD_PROJECT_TITLE_KEY");
      userMessage = new LocalizedString("MM_GUI_LOADING_PROJECT_KEY",
        new Object[]{projectToLoad});
    }
    else
    {
      if (_isConvertedFromCad)
      {
        title = StringLocalizer.keyToString("MM_GUI_IMPORT_CAD_TITLE_KEY");
      }
      else
      {
        title = StringLocalizer.keyToString("MM_GUI_IMPORT_PROJECT_TITLE_KEY");
      }
      userMessage = new LocalizedString("MM_GUI_IMPORTING_PROJECT_KEY",
        new Object[]{projectToLoad});
    }

    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(
      _mainUI,
      StringLocalizer.keyToString(userMessage),
      title,
      true);
    SwingUtils.setFont(busyCancelDialog, _dialogFont);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);

    _mainUI.getTestDev().unpopulate();
//    if(_mainUI.isVirtualLiveModeEnabled())
//    {
//      _mainUI.getVirtualLive().unpopulate();
//    }

    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        BooleanRef userCancel = new BooleanRef();
        try
        {
          if (loadProject)
          {
            //Khaw Chek Hau - XCR2654: CAMX Integration
            if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
            {
              try
              {
                _shopFloorSystem.recipeLoading(projectToLoad);
              }
              catch (final DatastoreException ex)
              {
                SwingUtils.invokeLater(new Runnable() 
                {
                  public void run() 
                  {
                    String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("SHOP_FLOOR_ERROR_MESSAGE_KEY_KEY");
                    MessageDialog.showErrorDialog(_mainUI,
                            errorMsg,
                            StringLocalizer.keyToString("SHOP_FLOOR_EVENT_TITLE_KEY"),
                            false);
                  }
                });
              }
            }
            
            //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
            if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT))
            {
              try
              {
                _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.START_LOADING_RECIPE);
              }
              catch (DatastoreException ex)
              {
                String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_ERROR_MESSAGE_KEY");
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                              errorMsg,
                                              StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_EVENT_TITLE_KEY"),
                                              true,
                                              FontUtil.getMediumFont());
              }
            }
            
            //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
            if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
            {
              try
              {
                _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.RECIPE_LOADING_START);
              }
              catch (DatastoreException ex)
              {
                String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_MESSAGE_KEY");
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                              errorMsg,
                                              StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_EVENT_TITLE_KEY"),
                                              true,
                                              FontUtil.getMediumFont());
              }
            }
            
            _project = Project.load(projectToLoad, userCancel);
          }
          else
          {
            // ignore warnings for version 1
            java.util.List<LocalizedString> warnings = new ArrayList<LocalizedString>();
            _project = Project.importProjectFromNdfs(projectToLoad, newProjectName, warnings);

            // create the cad image on import
            DrawCadPanel.createPanelImage(_project);

//            if (warnings.isEmpty() == false)
//            {
//              System.out.println("***************************************************");
//              System.out.println("MainUIMenuBar: Warnings from import were generated.");
//              System.out.println("***************************************************");
//            }
//            for (LocalizedString warning : warnings)
//            {
//              System.out.println("WARNINGS from the import: " + warning.toString());
//            }

          }
          _isProjectLoaded = true;
          _isProjectExist = true;
          
          //XCR-2069 - Anthony Fong - Fiducial Setting Inconsistent
          if(_project!=null)
          if(_project.getProjectReader()!=null)
          if(_project.getProjectReader().getBoardTypeReader()!=null)
          if(_project.getProjectReader().getBoardTypeReader().getFiducialSettingInconsistent())
          {            
            //Anthony Fong - Fiducial Setting Inconsistent
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {  
                LocalizedString projectNotLoadedMessage = new LocalizedString("MM_GUI_LOAD_PROJECT_WITH_INCONSISTENT_FIDUCIAL_SETTING_KEY",
                                              new Object[]{projectToLoad});
 
                MessageDialog.showWarningDialog(MainMenuGui.getInstance(), 
                                              StringLocalizer.keyToString(projectNotLoadedMessage), 
                                              StringLocalizer.keyToString("MM_GUI_UNABLE_TO_LOAD_PROJECT_TITLE_KEY"),
                                              true);
              }
            });

          }
          //Siew Yeng - XCR1725 - Customizable serial number mapping
          if(_project != null)
          {
            try
            {
              SerialNumberMappingManager.getInstance().checkIfSerialNumberMappingAndBarcodeReaderSetupAreTally();
            }
            catch(final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  MessageDialog.showWarningDialog(MainMenuGui.getInstance(),
                          xte.getLocalizedMessage(),
                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                          true);
                }
              });
            }
          }
          else
          {
            //Khaw Chek Hau - XCR3235: System failed to open converted recipe if the recipe name is same but different letter case
            _isProjectLoaded = false;
          }
        }
        catch(final FileNotFoundDatastoreException ex) // XCR1529, New Scan Route, khang-wah.chnee
        {
          _isProjectLoaded = false;
          _isProjectExist = false;
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              //Swee Yee Wong - not only scan route file not found error will happen here, should print the original error message
//              LocalizedString savingStr = new LocalizedString("MM_GUI_SCAN_ROUTE_FILE_NOT_FOUND_KEY", 
//                                                              new Object[]{ex.getFileName(), FileName.getScanRouteConfigFullPath()});
                
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(), 
                                            ex.getLocalizedMessage(), 
                                            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                            true);
            }
          });
        }
        catch(final DatastoreException ex)
        {
          _isProjectLoaded = false;
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              _mainUI.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
              MessageDialog.showErrorDialog(
                  _mainUI,
                  ex.getLocalizedMessage(),
                  StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                  true);
            }
          });
        }
        //XCR 2289 - catch null pointer exception when load a recipe that is no longer existed in file but still available in list of recipe in homepage
        catch(final NullPointerException npe)
        {
          _isProjectLoaded = false;
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              LocalizedString savingStr = new LocalizedString("TESTEXEC_GUI_PROJECT_DOES_NOT_EXIST_KEY", 
                                                              new Object[]{projectToLoad.toString()});
              
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(), 
                                            StringLocalizer.keyToString(savingStr), 
                                            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                            true);
            }
          });
          return;
        }
        finally
        {
          //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
          if (loadProject && _isProjectLoaded)
          {
            if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT))
            {
              try
              {
                _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.STOP_LOADING_RECIPE);
              }
              catch (DatastoreException ex)
              {
                String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_ERROR_MESSAGE_KEY");
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                              errorMsg,
                                              StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_EVENT_TITLE_KEY"),
                                              true,
                                              FontUtil.getMediumFont());
              }
            }
            
            //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
            if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
            {
              try
              {
                _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.RECIPE_LOADING_END);
                _machineLatestStatusLogWriter.writeMachineLatestStatusLog(MachineLatestStatusElementEventEnum.CURRENT_ACTIVE_PROGRAM, 
                                                                          _project.getName());
              }
              catch (DatastoreException ex)
              {
                String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_MESSAGE_KEY");
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                              errorMsg,
                                              StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_EVENT_TITLE_KEY"),
                                              true,
                                              FontUtil.getMediumFont());
              }
            }
            
            //Khaw Chek Hau - XCR3534: v810 SECS/GEM protocol implementation
            if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
            {
              try
              {
                _shopFloorSystem.recipeLoaded(_project.getName());
              }
              catch (DatastoreException ex)
              {
                String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("SHOP_FLOOR_ERROR_MESSAGE_KEY_KEY");
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                              errorMsg,
                                              StringLocalizer.keyToString("SHOP_FLOOR_EVENT_TITLE_KEY"),
                                              true);
              }
            }
          }
          
          // CR1011 fix by LeeHerng. If user clicks on Cancel button,
          // we track the cancel value and set _isProjectLoaded to false.
          if (userCancel.getValue() == true)
              _isProjectLoaded = false;
          
          //Kok Chun, Tan - 9/3/2015
          //XCR-2580 - close project if user clicked cancel when ask to convert recipe
          if (_project == null)
          {
            _isProjectLoaded = false;
            projectClose(true);
          }

          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.setVisible(false);
        }
      }
    });
    busyCancelDialog.setVisible(true);
    
    if (_isProjectLoaded)
    {
      //Khaw Chek Hau - XCR2913 : Found V810 fail to load NDF with same refdes' pin name
      String ndfWarningLogsFullPath = FileName.getLoadWarningsLogFullPath(_project.getName());

      if (loadProject == false && FileUtil.exists(ndfWarningLogsFullPath))
      {
        LocalizedString ndfImportWarningMessage = new LocalizedString("MMGUI_IMPORT_NDF_WARNING_LOG_KEY",
                                                      new Object[]{ndfWarningLogsFullPath});

        MessageDialog.showWarningDialog(_mainUI, StringLocalizer.keyToString(ndfImportWarningMessage), StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"), true);
      }
      
      //Project's history log CR-hsia-fen.tan
      try
      { 
        //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
        if (_testExecution.isShowHistoryLog() == true)
        {
          if (LoginManager.getInstance().getUserTypeFromLogIn().equals(UserTypeEnum.ADMINISTRATOR))
            _ProjectHistoryLog.openHistoryLog();
        }       
      }
      catch (DatastoreException dex)
      {
        dex.printStackTrace();
      } 
       //Delete history log file-hsia-fen.tan
      if (_testExecution.isDeleteHistoryFile() == true)
      {
        try
        {
          _showHistoryLog.Delete();
        }
        catch (DatastoreException ex)
        {
          Logger.getLogger(MainUIMenuBar.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
       
      _mainUI.projectOpen(_project);
      // if there were any ProjectCompatibilityChanges made, display the information here
      java.util.List<String> messages = Project.getCompatibiltyMessages();
      if (messages.isEmpty() == false)
      {
        String totalMessage = "";
        for (String msg : messages)
          totalMessage += msg;
        if (totalMessage.length() > 0)
          MessageDialog.showInformationDialog(_mainUI, totalMessage, StringLocalizer.keyToString("MM_GUI_LOAD_PROJECT_TITLE_KEY"));
      }
    }
    else if(_isProjectExist)
    {
      //sheng chuan XCR -2289 ,stop user to proceed if recipe is not exist
      return;
    }
    // Bee Hoon, XCR1653 - Prompt message to notify user if no project is loaded or imported
    else
    {
      unableToLoadProjectMessage(projectToLoad);
      return;
    }
    
    // Bee Hoon, XCR1650 - Open recipe and immediately save recipe, it will crash
    if(_projectErrorLogUtil.getIsLandPatternNameNotFoundError())
    {
       LocalizedString message = new LocalizedString("MMGUI_RECIPE_LAND_PATTERN_NAME_NOT_FOUND_ERROR_KEY",
                                    new Object[]{Directory.getProjectDir(_project.getName())});
       int answer = JOptionPane.showConfirmDialog(_mainUI,
                                                 StringLocalizer.keyToString(message),
                                                 StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                 JOptionPane.YES_NO_OPTION,
                                                 JOptionPane.WARNING_MESSAGE);
      if (answer == JOptionPane.YES_OPTION)
      {
        // loop thru all the components to search for any components that using the extra component package
        _componentListUsingCompPackage = _project.getPanel().getComponentListUsingCompPackage(_projectErrorLogUtil.getUnusedCompPackage());
        
        if(_componentListUsingCompPackage != null)
        {
          LocalizedString compPackageFoundMessage = new LocalizedString("MMGUI_RECIPE_COMPONENT_ASSIGNED_TO_EXTRA_COMP_PACKAGE_KEY",
                                                        new Object[]{Directory.getProjectDir(_project.getName())});
          MessageDialog.showWarningDialog(_mainUI, StringLocalizer.keyToString(compPackageFoundMessage), StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          projectClose(false);
        }
        else
        {
          saveProject();
          _projectErrorLogUtil.setIsLandPatternNameNotFoundError(false);
        }        
      }
      else if (answer == JOptionPane.NO_OPTION)
      {
        projectClose(false);
      }
    }
    
    //Siew Yeng - XCR1745 - Semi-automated mode
    if(TestExecution.getInstance().isSemiAutomatedModeEnabled())
    {
      if(_project.isSemiAutomatedRecipe() == false)
      {
        int answer = JOptionPane.showConfirmDialog(_mainUI,
                                                 StringLocalizer.keyToString("MM_GUI_RECIPE_NOT_SEMI_AUTOMATED_KEY"),
                                                 StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                 JOptionPane.YES_NO_OPTION,
                                                 JOptionPane.QUESTION_MESSAGE);
        
        if(answer == JOptionPane.YES_OPTION)
        {
          for(ComponentType compType : _project.getPanel().getComponentTypes())
          {
            compType.getComponentTypeSettings().setInspected(false);
          }
          saveAsProjectWithFileName(FileName.getSemiAutomatedProjectName(_project.getName()));
        }
        else
        {
          projectClose(true);
        }
      }
    }
  }

  /**
   * Check if the projectToLoad is already in x5000 project. If so, confirm
   * that it is ok to re-import. If a file exists with the project name, the \
   * user must remove it before importing.
   * @author George Booth
   */
  boolean okToImportProject(String projectToLoad)
  {
    File projectNameDir = new File(Directory.getProjectDir(projectToLoad));
    if (projectNameDir.exists())
    {
      if (projectNameDir.isDirectory())
      {
        // confirm if the user wants to re-import this project
        return (JOptionPane.showConfirmDialog(
            _mainUI,
            StringLocalizer.keyToString("PDD_CONFIRM_PROJECT_IMPORT_KEY"),
            StringLocalizer.keyToString("PDD_CONFIRM_PROJECT_IMPORT_TITLE_KEY"),
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION);
      }
      else
      {
        // user must remove existing file with project name before importing
        MessageDialog.showErrorDialog(
            _mainUI,
            StringLocalizer.keyToString("PDD_REMOVE_FILE_WITH_PROJECT_NAME_KEY"),
            StringLocalizer.keyToString("PDD_REMOVE_FILE_WITH_PROJECT_NAME_TITLE_KEY"));
        return false;
      }
    }
    else
    {
      // no existing file or directory.
      return true;
    }
  }

  /**
    * Check if the projectToSave is already in x5000 project. If so, confirm
    * that it is ok to save.  This behavior is really only useful during a Save As
    * operation, as a normal save will simply overwrite the previous one.
    * @author George Booth
    */
   private boolean okToSaveProject(String projectToSave)
   {
     File projectNameDir = new File(Directory.getProjectDir(projectToSave));
     if (projectNameDir.exists())
     {
       if (projectNameDir.isDirectory())
       {
         // confirm if the user wants to re-import this project
         return (JOptionPane.showConfirmDialog(
             _mainUI,
             StringLocalizer.keyToString("GUI_CONFIRM_PROJECT_SAVE_KEY"),
             StringLocalizer.keyToString("GUI_CONFIRM_PROJECT_SAVE_TITLE_KEY"),
             JOptionPane.YES_NO_OPTION,
             JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION);
       }
       else
       {
         // user must remove existing file with project name before saving
         LocalizedString message = new LocalizedString("PDD_REMOVE_FILE_WITH_PROJECT_NAME_KEY", new Object[]{Directory.getProjectsDir()});
         MessageDialog.showErrorDialog(
             _mainUI,
             StringLocalizer.keyToString(message),
             StringLocalizer.keyToString("PDD_REMOVE_FILE_WITH_PROJECT_NAME_TITLE_KEY"));
         return false;
       }
     }
     else
     {
       // no existing file or directory.
       return true;
     }
   }


   private boolean okToSaveReport(String reportPath)
   {
     if (FileUtil.exists(reportPath))
     {
         return (JOptionPane.showConfirmDialog(
             _mainUI,
             StringLocalizer.keyToString("GUI_CONFIRM_REPORT_SAVE_KEY"),
             StringLocalizer.keyToString("GUI_CONFIRM_REPORT_OVERWRITE_KEY"),
             JOptionPane.YES_NO_OPTION,
             JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION);
     }
     else
     {
       // no existing file or directory.
       return true;
     }
   }
   
   /**
   * Brings up a dialog to allow the user to pick which recipes to export as ndf
   * @author Siew Yeng
   */
  void exportProjectAsNdfMenuItem_actionPerformed()
  {
    exportProjectAsNdf();
  }
  
  /**
   * Export selected Project as ndf
   * @author Siew Yeng, Phang
   */
  void exportProjectAsNdf()
  {
    String title = StringLocalizer.keyToString("MM_GUI_EXPORT_PROJECT_TITLE_KEY");
    java.util.List<String> projectList = FileName.getProjectNames();
    Object[] possibleValues = projectList.toArray();
 
    ExportNdfDialog exportAsNdf = new ExportNdfDialog(_mainUI,title,possibleValues);
    
    int returnValue = exportAsNdf.showDialog();
    
    if(returnValue == JOptionPane.OK_OPTION)
    {
      final String projectToExport = (String)exportAsNdf.getSelectedRecipe();
      final String fileDestination = (String)exportAsNdf.getFileDestination();

      //Ngie Xing, XCR-2050, After a 5Dx recipe is exported to V810 ndf recipe, software fail to determine it as V810 ndf recipe
      //don't let user import, if a same folder name exist in the destination
      String hashedProjectFolderName = com.axi.v810.util.HashUtil.hashName(projectToExport);
      String dirName = fileDestination + File.separator + hashedProjectFolderName;//HashUtil.hashName(projectToExport);
      if (FileUtilAxi.exists(dirName))
      {
        JOptionPane.showMessageDialog(_mainUI,
          StringLocalizer.keyToString(new LocalizedString("MM_GUI_EXPORT_PROJECT_FOUND_SAME_FOLDER_KEY",
          new Object[]{projectToExport, hashedProjectFolderName})),
          title,
          JOptionPane.OK_OPTION);
        return;
      }
      
      //Siew Yeng - XCR-3524 - System crash when switch tab after export ndf
      if(_isProjectLoaded)
      {
        if(_project.getName().equalsIgnoreCase(projectToExport) == false)
        {
          int response = JOptionPane.showConfirmDialog(_mainUI,
                                                        StringLocalizer.keyToString("MM_GUI_EXPORT_PROJECT_MUST_CLOSE_OPEN_PROJECT_KEY"),
                                                        title,
                                                        JOptionPane.YES_NO_OPTION);
          if (response == JOptionPane.YES_OPTION)
          {
            // if they agree to close the project, do it, then allow export ndf
            projectClose(false);
          }
          else // if the don't agree, don't allow export ndf
            return;
        }
        else
        {
          if (_project.hasBeenModified())
          {
            LocalizedString saveQuestion = new LocalizedString("MM_GUI_SAVE_CHANGES_KEY",
                                                                new Object[] {_project.getName()});
            int answer = JOptionPane.showConfirmDialog(_mainUI,
                                                        StringUtil.format(StringLocalizer.keyToString(saveQuestion), 50),
                                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                        JOptionPane.YES_NO_OPTION,
                                                        JOptionPane.WARNING_MESSAGE);
            if (answer == JOptionPane.YES_OPTION)
            {
              saveProject();
            }
            else
            {
              return;  // chose not to save, so exit this operation
            }
          }
        }
      }
      
      LocalizedString exportStr = new LocalizedString("MM_GUI_EXPORTING_PROJECT_KEY",
        new Object[]{projectToExport});
      final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(
      _mainUI,
      StringLocalizer.keyToString(exportStr),
      StringLocalizer.keyToString("MM_GUI_EXPORT_PROJECT_TITLE_KEY"),
      StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
      true);
      busyCancelDialog.addCancelActionListener(new java.awt.event.ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          //do nothing
        }
      });

      busyCancelDialog.pack();
      SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
      
      _swingWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            NdfWriter ndfWriter = NdfWriter.getInstance();
            ndfWriter.exportProjectToNdf(projectToExport, fileDestination);
          }
          catch(final DatastoreException ex)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _mainUI.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                MessageDialog.showErrorDialog(
                    _mainUI,
                    ex.getLocalizedMessage(),
                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                    true);
              }
            });
          }
          finally
          {
            // wait until visible
            while (busyCancelDialog.isVisible() == false)
            {
              try
              {
                Thread.sleep(200);
              }
              catch (InterruptedException ex)
              {
                // do nothing
              }
            }
            busyCancelDialog.setVisible(false);
          }  
        }
      });  
      busyCancelDialog.setVisible(true);
    }
  }
  
  /**
   * @author George Booth
   */
  public void undoMenuItem_actionPerformed(ActionEvent e)
  {
    _undoKeystrokeHandler.undo(_commandManager);
  }

  /**
   * @author George Booth
   */
  public void redoMenuItem_actionPerformed(ActionEvent e)
  {
    _undoKeystrokeHandler.redo(_commandManager);
  }

  /**
   * @author George Booth
   */
  void mainHelpMenuItem_actionPerformed(ActionEvent e)
  {
    String helpFileName;
    if (_mainUI.getCurrentUserType().equals(UserTypeEnum.OPERATOR))
    {
      helpFileName = FileName.getOperatorHelpFileFullPath();
    }
    else if (_mainUI.getCurrentUserType().equals(UserTypeEnum.SERVICE))
    {
      helpFileName = FileName.getServiceHelpFileFullPath();
    }
    else
    {
      helpFileName = FileName.getHelpFileFullPath();
    }
    Assert.expect(helpFileName != null);

    String plainHelpFileName = helpFileName;
    if (helpFileName.startsWith("file:/"))
    {
      plainHelpFileName = helpFileName.substring(6);
    }
    if (FileUtil.exists(plainHelpFileName))
    {
      try
      {
        BrowserLauncher.openURLInNewWindow(helpFileName);
      }
      catch (IOException ioe)
      {
        MessageDialog.reportIOError(MainMenuGui.getInstance(), ioe.getLocalizedMessage());
      }
    }
    else
    {
      LocalizedString errorMessage = new LocalizedString("GUI_HELP_NOT_FOUND_KEY",
                                                         new Object[]{plainHelpFileName});
      MessageDialog.showErrorDialog(_mainUI,
                                    StringLocalizer.keyToString(errorMessage),
                                    StringLocalizer.keyToString("GUI_HELP_NOT_FOUND_TITLE_KEY"),
                                    true,
                                    _dialogFont);
    }
  }

  /**
   * @author George Booth
   */
  void helpAboutMenuItem_actionPerformed(ActionEvent e)
  {
    String helpAboutString = StringLocalizer.keyToString("GUI_ABOUT_BOX_MAIN_MENU_TITLE_KEY");
    HelpAboutBox dlg = new HelpAboutBox(_mainUI, helpAboutString);
    dlg.pack();
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setModal(true);
    dlg.setVisible(true);
  }
  
  /**
   * @author Swee Yee Wong
   */
  void helpInfoMenuItem_actionPerformed(ActionEvent e)
  {
    String[] exceptionParameters = new String[]
    {
    };
    InfoHandlerPanel.getInstance().executeCommand("DISPLAYALL-ERROR|" + "ALL", exceptionParameters);
  }

  /**
   * @author Erica Wheatcroft
   */
  void collectDiagnosticPackageMenuItem_actionPerformed()
  {
    if(Project.isCurrentProjectLoaded())
    {
      // a project is loaded so lets start the wizard
      // only allow this wizard if the project is not "dirty" -- it must be saved first
      if (_project.hasBeenModified())
      {
        // project has been modified, so we ask if they want to save
        LocalizedString saveQuestion = new LocalizedString("MM_GUI_SAVE_CHANGES_FOR_DIAGNOSTIC_SET_KEY", new Object[]{_project.getName()});
        int answer = JOptionPane.showConfirmDialog(_mainUI,
                                                   StringUtil.format(StringLocalizer.keyToString(saveQuestion), 50),
                                                   StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                   JOptionPane.YES_NO_OPTION,
                                                   JOptionPane.WARNING_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
          _ProjectHistoryLog.append("Recipe is saved");
          if (saveProject() == false)
            return;
        }
        else
          return; // chose not to save, so exit this operation
      }
    }
    else
    {
      // no project is loaded so then notify the user they must load a project before they can start the wizard
      String message = StringLocalizer.keyToString("CANT_START_WIZARD_WITHOUT_PROJECT_LOADED_KEY");
      JOptionPane.showMessageDialog(_mainUI,
                                    message,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    ZipProjectWizardDialog collectDiagnosticPackageWizard = new ZipProjectWizardDialog(_mainUI, true, true);
    collectDiagnosticPackageWizard.pack();
    SwingUtils.centerOnComponent(collectDiagnosticPackageWizard, _mainUI);
    collectDiagnosticPackageWizard.setVisible(true);
  }

  /**
   * @author George Booth
   */
  void imageManagerMenuItem_actionPerformed(ActionEvent e)
    {
      // now lets start the wizard
      ImageManagerDialog dialog = new ImageManagerDialog(_mainUI, true);
      dialog.populateDialogWithData(Directory.getOfflineXrayImageSetRootDirs());
      dialog.pack();
      SwingUtils.centerOnComponent(dialog, MainMenuGui.getInstance());
      dialog.setVisible(true);
      dialog.dispose();
    }

  /**
   * @author Laura Cormos
   * @author Erica Wheatcroft
   */
  void zipMenuItem_actionPerformed(ActionEvent e)
  {
    ZipProjectWizardDialog zipDlg = new ZipProjectWizardDialog(_mainUI, true, false);

    zipDlg.pack();
    SwingUtils.centerOnComponent(zipDlg, _mainUI);
    zipDlg.setVisible(true);
  }

  /**
   * @author Laura Cormos
   */
  void unzipMenuItem_actionPerformed(ActionEvent e)
  {
    // start modal unzip dialog
    ProjectUnzipDialog unzipDlg = new ProjectUnzipDialog(_mainUI, true);
    unzipDlg.pack();
    SwingUtils.centerOnComponent(unzipDlg, _mainUI);
    unzipDlg.setVisible(true);
  }

  /**
   * @author Laura Cormos
   */
  void deleteMenuItem_actionPerformed(ActionEvent e)
  {
    // start modal delete dialog
    ProjectDeleteDialog deleteDlg = new ProjectDeleteDialog(_mainUI, true);
    deleteDlg.pack();
    SwingUtils.centerOnComponent(deleteDlg, _mainUI);
    deleteDlg.setVisible(true);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  void updateBomMenuItem_actionPerformed(ActionEvent e)
  {
    // start modal unzip dialog
    BomUpdaterDialog bomUpdaterDlg = new BomUpdaterDialog(_mainUI, true);
    bomUpdaterDlg.pack();
    SwingUtils.centerOnComponent(bomUpdaterDlg, _mainUI);
    bomUpdaterDlg.setVisible(true);
  }

  /**
   * @author George Booth
   */
  void homeUIMenuItem_actionPerformed()
  {
    _mainUI.getMainToolBar().switchToAXIHome();
  }

  /**
   * @author George Booth
   */
  void testDevUIMenuItem_actionPerformed()
  {
    _mainUI.getMainToolBar().switchToTestDev();
  }

  /**
   * @author George Booth
   */
  void testExecUIMenuItem_actionPerformed()
  {
    _mainUI.getMainToolBar().switchToTestExec();
  }

  /**
   * @author George Booth
   */
  void configUIMenuItem_actionPerformed()
  {
    _mainUI.getMainToolBar().switchToConfigure();
  }

  /**
   * @author George Booth
   * @author Erica Wheatcroft
   */
  void serviceUIMenuItem_actionPerformed()
  {
    // set sim mode if on tdw
    try
    {
      if (XrayTester.getInstance().isHardwareAvailable() == false)
        XrayTester.getInstance().setSimulationMode();
      _mainUI.getMainToolBar().switchToService();
    }
    catch (XrayTesterException xte)
    {
      _mainUI.handleXrayTesterException(xte);
    }
  }

  /**
   * @author Scott Richardson
   */
  void virtualLiveUIMenuItem_actionPerformed()
  {
    _mainUI.getMainToolBar().switchToVirtualLive();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void generateImageSetMenuItem_actionPerformed()
  {
    if (_mainUI.isPanelInLegalState() == false)
      return;

    // lets first see if the test program is valid. If not then we will want to create it and notify that user
    // that we are getting the necessary data ready to display the image set panel. This was added since for some
    // projects the test program generation is a lengthy call.
    if (_project.isTestProgramValid() == false)
    {
      boolean prevCheckLatestTestProgram = _project.isCheckLatestTestProgram();
      _project.setCheckLatestTestProgram(true);
      _mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_IMAGE_SET_GEN_NEEDS_TEST_PROGRAM_KEY"),
                                                    StringLocalizer.keyToString("IMTB_GENERATE_IMAGE_SET_KEY"),
                                                    _project);
      _project.setCheckLatestTestProgram(prevCheckLatestTestProgram);
    }
    if (_project.getPanel().getPanelSettings().isManualAlignmentCompleted())
    {
      // now lets start the wizzard
      _mainUI.generateImageSet();
//      GenerateImageSetWizardDialog dialog = new GenerateImageSetWizardDialog(MainMenuGui.getInstance(), true, _project);
//      SwingUtils.centerOnComponent(dialog, MainMenuGui.getInstance());
//      dialog.pack();
//      dialog.setVisible(true);
//      dialog.dispose();
    }
    else
    {
      String message = StringLocalizer.keyToString("IMTB_GENERATE_IMAGE_SET_NO_ALIGNMENT_KEY");
      // they have not done manual alignment. so tell them they can no start the wizard.
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    message,
                                    StringLocalizer.keyToString("GISWK_TITLE_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      return;
    }

  }

  /**
   * @author Andy Mechtenberg
   */
  private void focusSummaryMenuItem_actionPerformed()
  {
    FocusSummaryDlg dlg;
    FineTuningImageGraphicsEngineSetup ftgre = _mainUI.getTestDev().getFineTuningImageGraphicsEngineSetup();
    if (ftgre.isImageDisplayed())
    {
      ReconstructionRegion rr = ftgre.getCurrentImageRegion();
      dlg = new FocusSummaryDlg(_project, new FocusReconstructionRegionWrapper(rr));
    }
    else
      dlg = new FocusSummaryDlg(_project);

    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    dlg.unpopulate();
    dlg.dispose();
  }

  private void testCoverageReportMenuItem_actionPerformed()
  {
    generateTestCoverageReport();
  }
  
  /**
   * @Author Kee Chin Seong
   * I open up this function purpose is maybe future i have more code to add on i will add inside here
   */
  private void startDefectPackager_actionPerformed()
  {
    startDefectPackager();
  }
  
  /**
   * @Author Kee Chin Seong
   * I open up this function purpose is maybe future i have more code to add on i will add inside here
  */
  private void stopDefectPackager_actionPerformed()
  {
    stopDefectPackager();
  }

  /**
   * @author Phang Siew Yeng
   * - found that .project needs to be removed as this is considered as "new" project.
   *   So it will messed up with the serialized project object and caused some bugs.
   * - commented by Kee Chin Seong.
   */
  private void convertMultiBoardToSinglePanelMenuItem_actionPerformed(ActionEvent e)
  {
    convertMultiBoardToSinglePanel();
  }
  
  /**
   * Merge multiboard to a single board.
   * @author Phang Siew Yeng
   */
  void convertMultiBoardToSinglePanel()
  {
    // XCR1676 - Crash when merge multi-boardType into single panel.
    // In future will need to remove this statement if can support. 
    if (_project.getPanel().getBoardTypes().size() > 1)
    {
      LocalizedString errorMessage = new LocalizedString("MM_GUI_CONVERT_PROJECT_BOARDTYPE_ERROR_TITLE_KEY",
        new Object[]{Version.getVersionNumber()});
      
      String exportStr = StringLocalizer.keyToString(errorMessage);
    
      JOptionPane.showMessageDialog(_mainUI,
                                    exportStr,
                                    StringLocalizer.keyToString("MM_GUI_CONVERT_PROJECT_TITLE_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      
      return;
    }
      
    _mainUI.getMainToolBar().switchToAXIHome();
    
    String previousProjectName = _project.getName();
    
    LocalizedString exportStr = new LocalizedString("MM_GUI_CONVERTING_PROJECT_KEY",
        new Object[]{_project.getName()});
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(
      _mainUI,
      StringLocalizer.keyToString(exportStr),
      StringLocalizer.keyToString("MM_GUI_CONVERT_PROJECT_TITLE_KEY"),
      true);

    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
     
    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          MultiboardToSinglePanelConverter.getInstance().mergeMultiboard(_project);
        }
        finally
        {
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.setVisible(false);
        }  
      }
    });  
    busyCancelDialog.setVisible(true);
        
    //save merged project as a new project
    saveAsProjectWithFileName(FileName.getMergedBoardProjectName(_project.getName()));
    
    if(_project.getName().equals(previousProjectName))
    {
      //user cancel
     projectClose(true);
    }
    else
    {
      //Remove unnecessary files
      String projDir = Directory.getProjectDir(_project.getName());
      java.util.List<String> dontDeleteFilesOrDirectories = new ArrayList<String>();

      //Kee Chin Seong - Remember to remove .project file !
      for(String file : FileUtilAxi.listAllFilesFullPathInDirectory(projDir))
      {
        if(file.endsWith(FileName.getCadFileName(_project.getCadXmlChecksum()))||
          file.endsWith(FileName.getSettingsFileName(_project.getSettingsXmlChecksum())) ||
          file.endsWith(FileName.getProjectAlignmentTransfromSettingsFile(FileName.getProjectAlignmentTransfromSettingsLatestFileVersion())) ||
          file.endsWith(FileName.getProjectBoardSettingsFile(1,FileName.getProjectBoardSettingsLatestFileVersion())) ||
          file.endsWith(FileName.getProjectBoardAlignmentSurfaceMapSettingsFile(1,FileName.getProjectAlignmentSurfaceMapSettingsLatestFileVersion())) ||
          file.endsWith(FileName.getProjectSurfaceSettingsFile(1,FileName.getProjectBoardSurfaceMapSettingsLatestFileVersion())) ||
          file.endsWith(FileName.getProjectPanelAlignmentSurfaceMapSettingsFile(FileName.getProjectPanelAlignmentSurfaceMapSettingsLatestFileVersion())) ||
          file.endsWith(FileName.getProjectPanelSurfaceMapSettingsFile(FileName.getProjectPanelSurfaceMapSettingsLatestFileVersion())) ||
          file.endsWith(FileName.getProjectBoardTypeFile(1,FileName.getProjectBoardTypeLatestFileVersion())) ||
          file.endsWith(FileName.getProjectBoardTypeSettingsFile(1, FileName.getProjectBoardTypeSettingsLatestFileVersion()))||
          file.endsWith(FileName.getProjectComponentNoLoadSettingsFile(FileName.getProjectComponentNoLoadSettingsLatestFileVersion())) ||
          file.endsWith(FileName.getGuiStatePersistFullPath(_project.getName())) ||
          file.endsWith(FileName.getProjectLandPatternFile(FileName.getProjectLandPatternLatestFileVersion())) ||
          file.endsWith(FileName.getProjectCompPackageFile(FileName.getProjectCompPackageLatestFileVersion())) ||
          file.endsWith(FileName.getProjectCompPackageSettingFile(FileName.getProjectCompPackageSettingLatestFileVersion())) ||
          file.endsWith(FileName.getProjectPanelFile(FileName.getProjectPanelLatestFileVersion())) ||
          file.endsWith(FileName.getProjectPanelSettingsFile(FileName.getProjectPanelSettingsLatestFileVersion())) ||
          file.endsWith(FileName.getPanelImageFile()) ||
          file.endsWith(FileName.getPanelFlippedImageFullPath(_project.getName())) ||
          file.endsWith(FileName.getProjectSummaryFile(FileName.getProjectSummaryLatestFileVersion())) ||
          file.endsWith(FileName.getProjectProjectSettingsFile(FileName.getProjectProjectSettingsLatestFileVersion())) ||
          file.endsWith(FileName.getScanRouteConfigFile()) ||
          file.endsWith(FileName.getProjectSubtypeSettingsFile(FileName.getProjectSubtypeSettingsLatestFileVersion()))||
          file.endsWith(FileName.getProjectSubtypeAdvanceSettingsFile(FileName.getProjectSubtypeAdvanceSettingsLatestFileVersion())) ||
          file.endsWith(FileName.getVerificationImageNamesProgramFileName()))

          dontDeleteFilesOrDirectories.add(file);
      }

      try
      {
        if (FileUtilAxi.exists(projDir))
          FileUtilAxi.deleteDirectoryContents(projDir, dontDeleteFilesOrDirectories);
      }
      catch (DatastoreException dex)
      {
        dex.printStackTrace();
      }
      
      //reload the project from fresh, Slow Load
      loadOrImportProject(true, _project.getName(), _project.getName());
    }
  }
  
  /*
   * @author Ngie Xing
   */
  public void changeSystemMagnificationMenuItem_actionPerformed()
  {
    try
    {
      if (LicenseManager.isOfflineProgramming())
      {
        ChangeSystemMagnificationDialog changeSystemMagnificationDiaglog = new ChangeSystemMagnificationDialog(_mainUI, true);
        SwingUtils.centerOnComponent(changeSystemMagnificationDiaglog, _mainUI);
        changeSystemMagnificationDiaglog.showDialog();
      }
      else
      {
        ChangeSystemMagnificationDialog changeSystemMagnificationDiaglog = new ChangeSystemMagnificationDialog(_mainUI, true);
        SwingUtils.centerOnComponent(changeSystemMagnificationDiaglog, _mainUI);
        int value = changeSystemMagnificationDiaglog.showDialog();
        if (value == JOptionPane.OK_OPTION)
          _mainUI.exitV810(true);
      }
    }
    catch (final BusinessException e)
    {
      finishLicenseMonitor();
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  e.getMessage(),
                  StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                  true);
          MainMenuGui.getInstance().exitV810(false);
        }
      });
    }
  }
  
  /**
   * @author Andy Mechtenberg
   */
  private void fixFocusMenuItem_actionPerformed()
  {
    TestProgram testProgram = _mainUI.getTestProgramWithReason(StringLocalizer.keyToString("GUI_FOCUS_NEED_TEST_PROGRAM_KEY"),
                                                               StringLocalizer.keyToString("GUI_FOCUS_FIX_TITLE_KEY"),
                                                               _project);

    java.util.List<ReconstructionRegion> regions = _project.getTestProgram().getUnfocusedReconstructionRegionsToBeFixed();
    if (regions.isEmpty())
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    StringLocalizer.keyToString("GUI_FOCUS_NO_REGIONS_MARKED_FOR_FIX_KEY"),
                                    StringLocalizer.keyToString("GUI_FOCUS_FIX_TITLE_KEY"),
                                    true);
      return;
    }
    testProgram.updateWithAdjustFocusSlices(true);

    boolean isCompatible = false;
    try
    {
      isCompatible = ImageManager.getInstance().doesCompatibleAdjustFocusImagesExist(_project);
    }
    catch (DatastoreException ex)
    {
      // do nothing
      isCompatible = false;
    }

    if ((isCompatible == false) && (XrayTester.getInstance().isHardwareAvailable()))
    {
      int answer = JOptionPane.showConfirmDialog(_mainUI,
                                                 StringLocalizer.keyToString("GUI_FOCUS_GENERATE_FOCUS_SET_KEY"),
                                                 StringLocalizer.keyToString("GUI_FOCUS_GENERATE_FOCUS_SET_TITLE_KEY"),
                                                 JOptionPane.YES_NO_OPTION);
      if (answer == JOptionPane.YES_OPTION)
      {
        try
        {
          _mainUI.generateFocusImageSet();
        }
        catch (XrayTesterException ex)
        {
          _mainUI.handleXrayTesterException(ex);
          return;
        }
      }
    }

    // go ahead and bring up the fix focus dialog regardless of whether the user cancelled or not
    FixFocusDlg dlg = new FixFocusDlg(_project);
    dlg.pack();
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    dlg.unpopulate();
    dlg.dispose();
    testProgram.updateWithAdjustFocusSlices(false);
  }

  /**
   * disable home environment menu item
   * @author George Booth
   */
  void disableHomeMenuItem()
  {
    _homeUIMenuItem.setEnabled(false);
    _testDevUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_TEST_DEV][_currentUser]);
    _testExecUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_TEST_EXEC][_currentUser] && _online);
    _configUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_CONFIG][_currentUser]);
    enableServiceUiMenuItem();
    _virtualLiveUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_VIRTUAL_LIVE][_currentUser]);
    _cadCreatorUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_CAD_CREATOR][_currentUser]);
  }

  /**
   * disable test dev environment menu item
   * @author George Booth
   */
  void disableTestDevMenuItem()
  {
    _homeUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_HOME][_currentUser]);
    _testDevUIMenuItem.setEnabled(false);
    _testExecUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_TEST_EXEC][_currentUser] && _online);
    _configUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_CONFIG][_currentUser]);
    enableServiceUiMenuItem();
    _virtualLiveUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_VIRTUAL_LIVE][_currentUser]);
    _cadCreatorUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_CAD_CREATOR][_currentUser]);
  }

  /**
   * disable test exec environment menu item
   * @author George Booth
   */
  void disableTestExecMenuItem()
  {
    _homeUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_HOME][_currentUser]);
    _testDevUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_TEST_DEV][_currentUser]);
    _testExecUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_TEST_EXEC][_currentUser] && _online);
    _configUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_CONFIG][_currentUser]);
    _virtualLiveUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_VIRTUAL_LIVE][_currentUser]);
    _cadCreatorUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_CAD_CREATOR][_currentUser]);
  }

  /**
   * disable configure environment menu item
   * @author George Booth
   */
  void disableConfigureMenuItem()
  {
    _homeUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_HOME][_currentUser]);
    _testDevUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_TEST_DEV][_currentUser]);
    _testExecUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_TEST_EXEC][_currentUser] && _online);
    _configUIMenuItem.setEnabled(false);
    enableServiceUiMenuItem();
    _virtualLiveUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_VIRTUAL_LIVE][_currentUser]);
    _cadCreatorUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_CAD_CREATOR][_currentUser]);
  }

  /**
   * disable service environment menu item
   * @author George Booth
   */
  void disableServiceMenuItem()
  {
    _homeUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_HOME][_currentUser]);
    _testDevUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_TEST_DEV][_currentUser]);
    _testExecUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_TEST_EXEC][_currentUser] && _online);
    _configUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_CONFIG][_currentUser]);
    _serviceUIMenuItem.setEnabled(false);
    _virtualLiveUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_VIRTUAL_LIVE][_currentUser]);
  }

  /**
   * disable virtual live environment menu item
   * @author George Booth, amended by Scott Richardson
   */
  void disableVirtualLiveMenuItem()
  {
    _homeUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_HOME][_currentUser]);
    _testDevUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_TEST_DEV][_currentUser]);
    _testExecUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_TEST_EXEC][_currentUser] && _online);
    _configUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_CONFIG][_currentUser]);
    enableServiceUiMenuItem();
    _virtualLiveUIMenuItem.setEnabled(false);
    _cadCreatorUIMenuItem.setEnabled(false);
  }
  
  /*
   * @author Kee Chin Seong - Cad Creator
   */
  void disableCadCreatorMenuItem()
  {
    _homeUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_HOME][_currentUser]);
    _testDevUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_TEST_DEV][_currentUser]);
    _testExecUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_TEST_EXEC][_currentUser] && _online);
    _configUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_CONFIG][_currentUser]);
    enableServiceUiMenuItem();
    _virtualLiveUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_VIRTUAL_LIVE][_currentUser]);
    _cadCreatorUIMenuItem.setEnabled(false);
  }

  /**
   * enable/disable menu items based on the current user
   * Actions menu items are only updated if caller requests since it is slow
   * @author George Booth
   */
  public void enableMenuItems(UserTypeEnum currentUser, boolean updateActionsMenu)
  {
    _currentUser = currentUser.getId();
//    System.out.println("current user = " + currentUser.getName() + ", id = " +  currentUser.getId());

    // File menu
    _fileMenu.setEnabled(MENU_ENABLE[FILE][_currentUser]);
    _openProjectMenuItem.setEnabled(MENU_ENABLE[FILE_OPEN][_currentUser]);
    _importProjectMenuItem.setEnabled(MENU_ENABLE[FILE_IMPORT][_currentUser]);
    _exportProjectAsNdfMenuItem.setEnabled(MENU_ENABLE[FILE_EXPORT][_currentUser]); //export NDF - Siew Yeng
    _saveProjectMenuItem.setEnabled(MENU_ENABLE[FILE_SAVE][_currentUser] && _isProjectLoaded);
    _mainUI.getEnvToolBar().enableSaveProjectButton(MENU_ENABLE[FILE_SAVE][_currentUser] && _isProjectLoaded);
    _mainUI.getEnvToolBar().enableGenerateTestCoverageReportButton(MENU_ENABLE[TOOLS_TEST_COVERAGE_REPORT][_currentUser]  && _isProjectLoaded);
    _mainUI.getEnvToolBar().enableTwoPinLandPatternCheckingButton(MENU_ENABLE[TOOLS_TEST_COVERAGE_REPORT][_currentUser] && _isProjectLoaded);
    _saveAsProjectMenuItem.setEnabled(MENU_ENABLE[FILE_SAVE_AS][_currentUser] && _isProjectLoaded);
    _closeProjectMenuItem.setEnabled(MENU_ENABLE[FILE_CLOSE][_currentUser] && _isProjectLoaded);
    _mainUI.getEnvToolBar().enableCloseProjectButton(MENU_ENABLE[FILE_CLOSE][_currentUser] && _isProjectLoaded);
    _logoutMenuItem.setEnabled(MENU_ENABLE[FILE_LOGOUT][_currentUser]);
    _exitMenuItem.setEnabled(MENU_ENABLE[FILE_EXIT][_currentUser]);
    
    // Edit menu
    _editMenu.setEnabled(MENU_ENABLE[EDIT][_currentUser]);
    // only enable undo and redo if there are items that are undo-able (or redo-able)
    if (_commandManager.isUndoAvailable())
    {
      _undoMenuItem.setEnabled(MENU_ENABLE[EDIT_UNDO][_currentUser]);
    }
    else
    {
      _undoMenuItem.setEnabled(false);
      _undoMenuItem.setText(UNDO);
    }
    if (_commandManager.isRedoAvailable())
    {
      _redoMenuItem.setEnabled(MENU_ENABLE[EDIT_REDO][_currentUser]);
    }
    else
    {
      _redoMenuItem.setEnabled(false);
      _redoMenuItem.setText(REDO);
    }


    // Tools menu
    _toolsMenu.setEnabled(MENU_ENABLE[TOOLS][_currentUser]);
    _imageManagerMenuItem.setEnabled(MENU_ENABLE[TOOLS_IMAGE_MANAGER][_currentUser]);
    _generateImageSetMenuItem.setEnabled(MENU_ENABLE[TOOLS_GENERATE_IMAGE_SET][_currentUser]  && _isProjectLoaded );
    _focusSummaryMenuItem.setEnabled(MENU_ENABLE[TOOLS_FOCUS_SUMMARY][_currentUser]  && _isProjectLoaded );
    _fixFocusMenuItem.setEnabled(MENU_ENABLE[TOOLS_FOCUS_ADJUST][_currentUser]  && _isProjectLoaded );
    _zipMenuItem.setEnabled(MENU_ENABLE[TOOLS_ZIP][_currentUser]);
    _testCoverageReportMenuItem.setEnabled(MENU_ENABLE[TOOLS_TEST_COVERAGE_REPORT][_currentUser]  && _isProjectLoaded );
    //  private final int TOOLS_TEST_COVERAGE_REPORT = _index++;
    _unzipMenuItem.setEnabled(MENU_ENABLE[TOOLS_UNZIP][_currentUser]);
    _deleteMenuItem.setEnabled(MENU_ENABLE[TOOLS_DELETE][_currentUser]);
    _collectDiagnosticPackageWizardMenuItem.setEnabled(MENU_ENABLE[TOOLS_COLLECT_DIAGNOSTIC_PACKAGE][_currentUser] && _isProjectLoaded);
    _importPackageLibraryMenuItem.setEnabled(MENU_ENABLE[TOOLS_IMPORT_LIBRARY][_currentUser] && _isProjectLoaded);
    _exportPakcageLibraryMenuItem.setEnabled(MENU_ENABLE[TOOLS_EXPORT_LIBRARY][_currentUser] && _isProjectLoaded);
    
	// XCR-3165 Import Land pattern and Export Land pattern buttons are enabled even there is no recipe loaded
    _importLandPatternMenuItem.setEnabled(_isProjectLoaded);
    _exportLandPatternMenuItem.setEnabled(_isProjectLoaded);
    
    //_updateBomMenuItem.setEnabled(MENU_ENABLE[TOOLS_UPDATE_BOM][_currentUser] && _isProjectLoaded);
    
    if(_isProjectLoaded)
      _convertMultiBoardToSinglePanelMenuItem.setEnabled(MENU_ENABLE[TOOLS_CONVERT_MULTIBOARD_TO_SINGLE_PANEL][_currentUser] && _project.getPanel().isMultiBoardPanel());
    else
      _convertMultiBoardToSinglePanelMenuItem.setEnabled(false);
    
    try
    {
      if (LicenseManager.isOfflineProgramming() ==false)
      {
        if(XrayTester.isS2EXEnabled())
          _changeSystemMagnificationMenuItem.setEnabled(MENU_ENABLE[TOOLS_CHANGE_SYSTEM_MAGNIFICATION][_currentUser]);
      }
      else
      {
        if (XrayTester.getSystemType().equals(SystemTypeEnum.S2EX) && Project.isCurrentProjectLoaded())
          _changeSystemMagnificationMenuItem.setEnabled(MENU_ENABLE[TOOLS_CHANGE_SYSTEM_MAGNIFICATION][_currentUser]);
        else
          _changeSystemMagnificationMenuItem.setEnabled(false);
      }
    }
    catch (final BusinessException e)
    {
      finishLicenseMonitor();
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  e.getMessage(),
                  StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                  true);
          MainMenuGui.getInstance().exitV810(false);
        }
      });
    }

    // Actions menu
    _actionsMenu.setEnabled(MENU_ENABLE[ACTIONS][_currentUser] && _online);
    // getting system status is slow, do only if hardware status may have changed
    if (updateActionsMenu)
    {
      setActionMenuStates();
    }

    if (_mainUI.getStartingUserType().equals(UserTypeEnum.SERVICE))
    {
      // user logged on as service can't navigate out of Service UI
      _windowMenu.setEnabled(false);
      _homeUIMenuItem.setEnabled(false);
      _testDevUIMenuItem.setEnabled(false);
      _testExecUIMenuItem.setEnabled(false);
      _configUIMenuItem.setEnabled(false);
      _serviceUIMenuItem.setEnabled(false);
      _virtualLiveUIMenuItem.setEnabled(false);
      _cadCreatorUIMenuItem.setEnabled(false);
    }
    else
    {
      _windowMenu.setEnabled(MENU_ENABLE[WINDOW][_currentUser]);
      _homeUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_HOME][_currentUser]);
      _testDevUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_TEST_DEV][_currentUser]);
      _testExecUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_TEST_EXEC][_currentUser] && _online);
      _configUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_CONFIG][_currentUser]);
      _serviceUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_SERVICE][_currentUser]);
      _virtualLiveUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_VIRTUAL_LIVE][_currentUser]);
      _cadCreatorUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_CAD_CREATOR][_currentUser]);
    }

    enableServiceUiMenuItem();

    _helpMenu.setEnabled(MENU_ENABLE[HELP][_currentUser]);
    _mainHelpMenuItem.setEnabled(MENU_ENABLE[HELP_MAIN][_currentUser]);
    _helpAboutMenuItem.setEnabled(MENU_ENABLE[HELP_ABOUT][_currentUser]);
  }

  /**
   * Sets the action menu states based on system status
   * @author George Booth
   */
  public void setActionMenuStates()
  {
    // hardware actions are only available on-line
    if (_online)
    {
      // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
      try
      {
//        HardwareWorkerThread.getInstance2().invokeAndWait(new RunnableWithExceptions()
//        {
//          public void run()
//          {
//            try
//            {
//              _panelIsLoaded = PanelHandler.getInstance().isPanelLoaded();
//            }
//            catch(XrayTesterException xte)
//            {
//              _mainUI.handleXrayTesterException(xte);
//            }
//          }
//        });
        
        // Siew Yeng - XCR1426 
        // fix hardwareWorkerThread assert
        try
        {
          _panelIsLoaded = PanelHandler.getInstance().isPanelLoaded();

          // panel can be loaded if project is loaded and a panel is not already loaded
          // system will be started if needed
          _loadPanelMenuItem.setEnabled(MENU_ENABLE[ACTIONS_LOAD_PANEL][_currentUser] && _isProjectLoaded
                                        && (_panelIsLoaded == false));
          _mainUI.getEnvToolBar().enableLoadPanelButton(MENU_ENABLE[ACTIONS_LOAD_PANEL][_currentUser] && _isProjectLoaded
                                        && (_panelIsLoaded == false));

          // panel can be unloaded - doesn't matter if one is loaded
          // system will be started if needed
          _unloadPanelMenuItem.setEnabled(MENU_ENABLE[ACTIONS_UNLOAD_PANEL][_currentUser] && _panelIsLoaded);
          _mainUI.getEnvToolBar().enableUnloadPanelButton(MENU_ENABLE[ACTIONS_UNLOAD_PANEL][_currentUser] && _panelIsLoaded);
          
          if(_xrayTester.isStartupRequiredForMotionRelatedHardware() == false)
          {
            // rails can be homed if started and a panel is not loaded
            _homeRailsMenuItem.setEnabled(MENU_ENABLE[ACTIONS_HOME_RAILS][_currentUser] && (_panelIsLoaded == false));
          }
          else
          {
            _homeRailsMenuItem.setEnabled(false);
          }
        }
        catch(XrayTesterException xte)
        {
          _loadPanelMenuItem.setEnabled(false);
          _mainUI.getEnvToolBar().enableLoadPanelButton(false);
          
          _unloadPanelMenuItem.setEnabled(false);
          _mainUI.getEnvToolBar().enableUnloadPanelButton(false);
          
          _homeRailsMenuItem.setEnabled(false);
        }
        
        _startupMenuItem.setEnabled(MENU_ENABLE[ACTIONS_START_UP][_currentUser]);
        _mainUI.getEnvToolBar().enableStartUpButton(MENU_ENABLE[ACTIONS_START_UP][_currentUser]);
//      _startupNoCalMenuItem.setEnabled(MENU_ENABLE[ACTIONS_START_UP][_currentUser]);
        
        // shut down can be done if started.
        _shutdownMenuItem.setEnabled(MENU_ENABLE[ACTIONS_SHUT_DOWN][_currentUser]);
        _mainUI.getEnvToolBar().enableShutdownButton(MENU_ENABLE[ACTIONS_SHUT_DOWN][_currentUser]);
        
        //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
        AbstractXraySource xraySource = AbstractXraySource.getInstance();
        if (xraySource instanceof LegacyXraySource)
        {
          _xRayAutoShortTermShutdownMenuItem.setEnabled(MENU_ENABLE[ACTIONS_XRAY_AUTO_SHUT_DOWN][_currentUser]);
          _mainUI.getEnvToolBar().enableShortTermXrayShutdownButton(MENU_ENABLE[ACTIONS_XRAY_AUTO_SHUT_DOWN][_currentUser]);
        }
        else if (_hTubeXraySource instanceof HTubeXraySource)
        {
            if(_hTubeXraySource.isStartupRequired() == false)
            {
              _xRayAutoShortTermShutdownMenuItem.setEnabled(MENU_ENABLE[ACTIONS_XRAY_AUTO_SHUT_DOWN][_currentUser]);
              _mainUI.getEnvToolBar().enableShortTermXrayShutdownButton(MENU_ENABLE[ACTIONS_XRAY_AUTO_SHUT_DOWN][_currentUser]);
            }
            else
            {
                _xRayAutoShortTermShutdownMenuItem.setEnabled(false);
                _mainUI.getEnvToolBar().enableShortTermXrayShutdownButton(false);
            }
        }
        else
        {
          _xRayAutoShortTermShutdownMenuItem.setEnabled(false);
          _mainUI.getEnvToolBar().enableShortTermXrayShutdownButton(false);
        }
      }
      catch (XrayTesterException xte)
      {
        _mainUI.handleXrayTesterException(xte);
      }
    }
    else
    {
      _startupMenuItem.setEnabled(false);
      _mainUI.getEnvToolBar().enableStartUpButton(false);
//      _startupNoCalMenuItem.setEnabled(false);
      _shutdownMenuItem.setEnabled(false);
      _mainUI.getEnvToolBar().enableShutdownButton(false);

      //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
      _xRayAutoShortTermShutdownMenuItem.setEnabled(false);
      _mainUI.getEnvToolBar().enableShortTermXrayShutdownButton(false);

      _loadPanelMenuItem.setEnabled(false);
      _mainUI.getEnvToolBar().enableLoadPanelButton(false);

      _unloadPanelMenuItem.setEnabled(false);
      _mainUI.getEnvToolBar().enableUnloadPanelButton(false);
      
      _shutdownMenuItem.setEnabled(false);
      _mainUI.getEnvToolBar().enableShutdownButton(false);
      
      _homeRailsMenuItem.setEnabled(false);
    }
  }

  /**
   * The update function that gets called when an
   * observable class has a state change.
   * @author George A. David
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProjectObservable)
        {
          ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)object;
          ProjectChangeEventEnum projectChangeEventEnum = projectChangeEvent.getProjectChangeEventEnum();
          if (projectChangeEventEnum.equals(ProjectEventEnum.UNLOAD))
            return;

          updateUndoRedoMenuItems();
          //                                            don't update actions menu
          enableMenuItems(_mainUI.getCurrentUserType(), false);
        }
        else if (observable instanceof ConfigObservable)
        {
          updateUndoRedoMenuItems();
          enableMenuItems(_mainUI.getCurrentUserType(), false);  // don't update actions menu
        }
        else if (observable instanceof InspectionEventObservable)
        {
          Assert.expect(object instanceof InspectionEvent, "not an instance of InspectionEventEnum");
          InspectionEventEnum event = ((InspectionEvent)object).getInspectionEventEnum();
          // check to see if the user is starting a test or ending one. then update
          // buttons as needed.
          if (event.equals(InspectionEventEnum.INSPECTION_STARTED))
          {
            // starting a test disable all buttons
            _imageManagerMenuItem.setEnabled(false);
          }
          else if (event.equals(InspectionEventEnum.INSPECTION_COMPLETED))
          {
            _imageManagerMenuItem.setEnabled(MENU_ENABLE[TOOLS_IMAGE_MANAGER][_currentUser]);
          }
        }
        else if (observable instanceof UserAccountsObservable)
        {
          updateUndoRedoMenuItems();
        }
        else if(observable instanceof DefectPackagerObservable)
        {
          updateDefectPackagerStatus(object);
        }
      }
    });
  }
  
  /*
   * @author Kee chin Seong
   * To check the defect packager status when the UI started.
   */
  private void updateDefectPackagerStatus(final Object object)
  {
    if(((DefectPackagerTaskEventEnum)object).equals(DefectPackagerTaskEventEnum.DEFECT_PACKAGER_NOT_INSTALLED) == true ||
      ((DefectPackagerTaskEventEnum)object).equals(DefectPackagerTaskEventEnum.DEFECT_PACKAGER_STOPPED) == true)
    {
      _isDefectPackagerRunning = true;
      _isDefectPackagerStopped = false;
    }
    else if(((DefectPackagerTaskEventEnum)object).equals(DefectPackagerTaskEventEnum.DEFECT_PACKAGER_RUNNING) == true)
    {
      _isDefectPackagerRunning = false; 
      _isDefectPackagerStopped = true;
    }
    
    //Kee chin Seong
    _mainUI.getEnvToolBar().enableStartDefectPackagerButton(MENU_ENABLE[TOOLS_START_DEFECT_PACKAGER][_currentUser] & _isDefectPackagerRunning);
    _mainUI.getEnvToolBar().enableStopDefectPackagerButton(MENU_ENABLE[TOOLS_STOP_DEFECT_PACKAGER][_currentUser] & _isDefectPackagerStopped);
    //Kee Chin Seong, Defect Packager
    _startDefectPackagerMenuItem.setEnabled(MENU_ENABLE[TOOLS_START_DEFECT_PACKAGER][_currentUser] & _isDefectPackagerRunning);
    _stopDefectPackagerMenuItem.setEnabled(MENU_ENABLE[TOOLS_STOP_DEFECT_PACKAGER][_currentUser] & _isDefectPackagerStopped);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void updateUndoRedoMenuItems()
  {
    if (SwingUtilities.isEventDispatchThread())
    {
      // undo first
      StringBuffer undoText = new StringBuffer();
      boolean isAvailable = _commandManager.isUndoAvailable(undoText);
      if (isAvailable == false)
      {
        _undoMenuItem.setEnabled(false);
      }
      else
      {
        _undoMenuItem.setEnabled(MENU_ENABLE[EDIT_UNDO][_currentUser]);
      }
      _undoMenuItem.setText(UNDO + " " + undoText.toString());

      // redo next
      StringBuffer redoText = new StringBuffer();
      isAvailable = _commandManager.isRedoAvailable(redoText);
      if (isAvailable == false)
      {
        _redoMenuItem.setEnabled(false);
      }
      else
      {
        _redoMenuItem.setEnabled(MENU_ENABLE[EDIT_REDO][_currentUser]);
      }
      _redoMenuItem.setText(REDO + " " + redoText.toString());
    }
    else
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        updateUndoRedoMenuItems();
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void enableServiceUiMenuItem()
  {
    if (_online)
      _serviceUIMenuItem.setEnabled(MENU_ENABLE[WINDOW_SERVICE][_currentUser]);
    else
    {
      if (_currentUser == UserTypeEnum.SERVICE.getId())
        _serviceUIMenuItem.setEnabled(true);
      else
        _serviceUIMenuItem.setEnabled(false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void removeProjectObservable(boolean remove)
  {
    if (remove)
    {
      ConfigObservable.getInstance().deleteObserver(this);
      ProjectObservable.getInstance().deleteObserver(this);
      UserAccountsObservable.getInstance().deleteObserver(this);
    }
    else
    {
      ConfigObservable.getInstance().addObserver(this);
      ProjectObservable.getInstance().addObserver(this);
      UserAccountsObservable.getInstance().addObserver(this);
      enableMenuItems(_mainUI.getCurrentUserType(), true);
    }
  }

  void exportLibrary_actionPerformed()
  {
    _mainUI.exportPakageLibary();
  }

  void importLibrary_actionPerformed()
  {
    _mainUI.importPakageLibary();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void importLandPattern_actionPerformed()
  {
    _mainUI.importLandPattern();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void exportLandPattern_actionPerformed()
  {
    _mainUI.exportLandPattern();
  }

 /**
   * @author Jack Hwee
   */
  public static java.util.List<String> getAllNdfTypeNames()
  {
    java.util.List<String> ndfTypeNames = new ArrayList<String>();

        ndfTypeNames.add(StringLocalizer.keyToString("MMGUI_SELECT_V810_NDF_TYPE_KEY"));
        ndfTypeNames.add(StringLocalizer.keyToString("MMGUI_SELECT_5DX_NDF_TYPE_KEY"));

    return ndfTypeNames;
  }
  
  public static java.util.List<String> getAllCadTypeNames()
  {
    java.util.List<String> cadTypeNames = new ArrayList<String>();

    cadTypeNames.add(StringLocalizer.keyToString("MMGUI_SELECT_CADENCE_ALLEGRO_TYPE_KEY"));
    cadTypeNames.add(StringLocalizer.keyToString("MMGUI_SELECT_GENCAD_TYPE_KEY"));
    cadTypeNames.add(StringLocalizer.keyToString("MMGUI_SELECT_ODB_TYPE_KEY"));
    cadTypeNames.add(StringLocalizer.keyToString("MMGUI_SELECT_FABMASTER_TYPE_KEY"));

    return cadTypeNames;
  }

  /**
   * @author sham
   */
  public void disableLoadUnloadMenuItem()
  {
    if(_loadPanelMenuItem != null)
    {
      _loadPanelMenuItem.setEnabled(false);
    }
    if(_unloadPanelMenuItem != null)
    {
      _unloadPanelMenuItem.setEnabled(false);
    }
  }
  
   /**
   * Change all the subtypes to low mag 
   * @author Jack Hwee
   */
  public void checkAndModifySubtypeMagnification(final Project project)
  {
    Assert.expect(project != null);

    String message = StringLocalizer.keyToString("MMGUI_CHANGE_HIGH_MAG_TO_LOW_MAG_SUBTYPE_CONFIRM_KEY");
    int response = JOptionPane.showConfirmDialog(_mainUI,
                                                 StringUtil.format(message, 50),
                                                 StringLocalizer.keyToString("MMGUI_CHANGE_HIGH_MAG_TO_LOW_MAG_SUBTYPE_CONFIRM_TITLE_KEY"),
                                                 JOptionPane.YES_NO_OPTION,
                                                 JOptionPane.INFORMATION_MESSAGE);

    if (response == JOptionPane.YES_OPTION)
    {
      String title, userMessage;
    
      title = StringLocalizer.keyToString("MMGUI_CHANGE_HIGH_MAG_TO_LOW_MAG_SUBTYPE_CONFIRM_TITLE_KEY");
      userMessage = StringLocalizer.keyToString("MMGUI_CHANGING_HIGH_MAG_TO_LOW_MAG_SUBTYPE_KEY");
              
      final BusyCancelDialog convertSubtypeMagnificationDialog = new BusyCancelDialog(
              _mainUI,
              StringLocalizer.keyToString(userMessage),
              title,
              true);
      SwingUtils.setFont(convertSubtypeMagnificationDialog, _dialogFont);
      convertSubtypeMagnificationDialog.pack();
      SwingUtils.centerOnComponent(convertSubtypeMagnificationDialog, _mainUI);
      
      _swingWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          ProgramVerificationUtil.convertSubtypeMagnificationHighToLow(project);
          
          // get back on swing thread
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                Thread.sleep(400);
              }
              catch (InterruptedException ex)
              {
                ex.printStackTrace();
              }
              convertSubtypeMagnificationDialog.dispose();
            }
          });
        } 
      });
      convertSubtypeMagnificationDialog.setVisible(true); 
    }
    else
    {
      return;
    }
  }
  
  /**
   * Get the load status of project 
   * @author Bee Hoon
   */
  public boolean isProjectLoaded()
  {
    return _isProjectLoaded;
  }
  
  /**
   * Change Dynamic range Optimization Level(DRO Level) to maximum limit of dro from config.
   * @author Siew Yeng
   */
  public void checkAndModifyDynamicRangeOptimizationLevel(final Project project, final int maxDroLevel)
  {
    Assert.expect(project != null);

    boolean exceedMaxLimit = false;

    for(Subtype subtype : project.getPanel().getSubtypes())
    {
      SubtypeAdvanceSettings subtypeAdvanceSettings = subtype.getSubtypeAdvanceSettings();
      //Siew Yeng - XCR-3124 - Failed to auto convert maximum DynamicRangeOptimizationLevel
      int droLevel = (int)subtypeAdvanceSettings.getDynamicRangeOptimizationLevel().toDouble();
      if(droLevel > maxDroLevel)
      {
        exceedMaxLimit = true;
        subtypeAdvanceSettings.setDynamicRangeOptimizationLevel(DynamicRangeOptimizationLevelEnum.getDynamicRangeOptimizationLevelToEnumMapValue(maxDroLevel));
      }
    }
    
    if(exceedMaxLimit)
    {
      String message = StringLocalizer.keyToString(new LocalizedString("MMGUI_DRO_LEVEL_EXCEED_MAXIMUM_LIMIT_WARNING_KEY", new Object[]{maxDroLevel}));
      MessageDialog.showWarningDialog(_mainUI,
                                      StringUtil.format(message, 50),
                                      StringLocalizer.keyToString("MMGUI_DRO_LEVEL_EXCEED_MAX_LIMIT_TITLE_KEY"));
    }
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   */
  public void enableChangeMagnificationMenu()
  {
    _changeSystemMagnificationMenuItem.setEnabled(true);
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   */
  public void disableChangeMagnificationMenu()
  {
    _changeSystemMagnificationMenuItem.setEnabled(false);
  }
  
  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public void userImportCAD()
  {
    Map<String,String> cadenceFileMap = FileName.getCadenceAllegroFileNames(Directory.getCadDirectory());
    Map<String,String> fabmasterFileMap = FileName.getFabmasterFileNames(Directory.getCadDirectory());
    Map<String,String> odbFileMap = FileName.getODBFileNames(Directory.getCadDirectory());
    Map<String,String> gencadFileMap = FileName.getGencadFileNames(Directory.getCadDirectory());
    
    if ((cadenceFileMap == null || cadenceFileMap.isEmpty()) &&
        (fabmasterFileMap == null || fabmasterFileMap.isEmpty()) &&
        (odbFileMap == null || odbFileMap.isEmpty()) &&
        (gencadFileMap == null || gencadFileMap.isEmpty()))
    {
      noCADFileMessage();
      return;
    }
    String title = StringLocalizer.keyToString("MM_GUI_IMPORT_CAD_TITLE_KEY");
    String projectName = null;
    ImportCadDialog importDialog = new ImportCadDialog(_mainUI, title);
    int returnValue = importDialog.showDialog();
    if (returnValue == JOptionPane.OK_OPTION)
    {
      String ndfPath = Directory.getLegacyNdfDir();
      try
      {
        try
        {
          if (LicenseManager.isOfflineProgramming())
          {
            String sytemTypeSelected = (String) importDialog.getSelectedSystemType();
            if (sytemTypeSelected.equalsIgnoreCase(SystemTypeEnum.STANDARD.getName()))
            {
              sytemTypeSelected = SystemTypeEnum.THROUGHPUT.getName();
            }
            String selectedLowMag = (String) importDialog.getSelectedSystemMagnification();
            SystemTypeEnum systemType = SystemTypeEnum.getSystemType(sytemTypeSelected);
            try
            {
              Config.getInstance().forceLoad(systemType);
              ProgramGeneration.reintializeDueToSystemTypeChange();
              com.axi.v810.datastore.projReaders.ProjectReader.reintializeDueToSystemTypeChange();
              ProjectWriter.reintializeDueToSystemTypeChange();
              Project.setupProjectReaderWriter();
              if (systemType.equals(SystemTypeEnum.S2EX))
              {
                Config.getInstance().setValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION, selectedLowMag);
                Config.getInstance().save(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION.getFileName());
                enableChangeMagnificationMenu();
                _mainUI.getMainToolBar().enableChangeSystemMagnificationButton();
              }
              else
              {
                Config.getInstance().setValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION, "M19");
                Config.getInstance().save(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION.getFileName());
                disableChangeMagnificationMenu();
                _mainUI.getEnvToolBar().disableChangeSystemMagnificationButton();
              }
              MagnificationEnum.resetMagnification();
              _mainUI.getTestDev().refresh();
            }
            catch (DatastoreException e)
            {
              _mainUI.handleOfflineProgrammingDataStoreError(e);
              return;
            }
          }
        }
        catch (final BusinessException e)
        {
          finishLicenseMonitor();
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                e.getMessage(),
                StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                true);
              MainMenuGui.getInstance().exitV810(false);
            }
          });
        }

        final String projectToLoad = (String) importDialog.getSelectedRecipe();
        projectName = projectToLoad;
        final String projectPath = importDialog.getSelectedPath();
        final String selectedCadType = (String) importDialog.getSelectedCadType();
        String newProjectName = importDialog.getNewProjectName();

        //Set flag
        final String tempnNdfPath = ndfPath;
        LocalizedString savingStr = new LocalizedString("MM_GUI_CONVERTING_CAD_KEY", new Object[]
        {
          projectToLoad
        });
        final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString(savingStr),
          StringLocalizer.keyToString("MM_GUI_CONVERT_CAD_TITLE_KEY"),
          true);
        busyCancelDialog.pack();
        SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
        _swingWorkerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              CadConverter cadConverter = CadConverter.getInstance();
              cadConverter.setCadSystem(selectedCadType);

              cadConverter.convert(projectPath, tempnNdfPath);
            }
            catch (FileNotFoundDatastoreException ex)
            {
              MessageDialog.showErrorDialog(_mainUI,
                ex.getLocalizedMessage(),
                StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                true);

              return;
            }
            catch (BusinessException e)
            {
              MessageDialog.showErrorDialog(_mainUI,
                e.getLocalizedMessage(),
                StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                true);
              
              return;
            }
            finally
            {
              // wait until visible
              while (busyCancelDialog.isVisible() == false)
              {
                try
                {
                  Thread.sleep(200);
                }
                catch (InterruptedException ex)
                {
                  // do nothing
                }
              }
              busyCancelDialog.setVisible(false);
            }
          }
        });
        busyCancelDialog.setVisible(true);

        NdfReader.set5dxProjectFlag(false);
        NdfReader.clear5dxProjectNamesToProjectHashNameMap();

        // check for a valid project name
        if (Project.isProjectNameValid(newProjectName) == false)
        {
          invalidProjectNameMessage(newProjectName, true);
          return;  // error, do not continue
        }

        String initialThresholdsFileName = (String) importDialog.getSelectedThresholdsSet();
        try
        {
          ProjectInitialThresholds.getInstance().setImportInitialThresholdsFileNameWithoutExtension(initialThresholdsFileName);
        }
        catch (DatastoreException ex)
        {
          // something's wrong with software.config. Show the error and stop the import
          MessageDialog.showErrorDialog(_mainUI,
            ex.getLocalizedMessage(),
            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
            true);

          return;
        }
        
        String initialRecipeSettingFileName = (String) importDialog.getSelectedRecipeSettingSet();
        try
        {
          ProjectInitialRecipeSettings.getInstance().setImportInitialRecipeSettingFileNameWithoutExtension(initialRecipeSettingFileName);
        }
        catch (DatastoreException ex)
        {
          // something's wrong with software.config. Show the error and stop the import
          MessageDialog.showErrorDialog(_mainUI,
            ex.getLocalizedMessage(),
            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
            true);

          return;
        }
        
        String jointTypeAssignmentFileName = (String) importDialog.getSelectedJointTypeAssignment();
        try
        {
          JointTypeEnumAssignment.getInstance().setJointTypeAssignmentConfigFileNameWithoutExtension(
            jointTypeAssignmentFileName);
        }
        catch (DatastoreException ex)
        {
          // something's wrong with software.config. Show the error and stop the import
          MessageDialog.showErrorDialog(_mainUI,
            ex.getLocalizedMessage(),
            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
            true);

          return;
        }

        // if importing, check if an x6000 project already exists
        // issue warning if the user is trying to import again
        if (okToImportProject(newProjectName) == false)
        {
          return;
        }
        else if (NdfReader.is5dxProjectFlag() == false)
        {
          String outputDirectory = Directory.getProjectDir(newProjectName);
          File f1 = new File(outputDirectory);
          try
          {
            FileUtil.delete(outputDirectory);
          }
          catch (CouldNotDeleteFileException ex)
          {
            ex.printStackTrace();
          }
          f1.delete();
        }
        
        _isConvertedFromCad = true;
        loadOrImportProject(false, projectToLoad, newProjectName);
      }
      finally
      {
        _isConvertedFromCad = false;
        CadConverter.getInstance().reset();
        try
        {
          if (projectName != null)
          {
            FileUtilAxi.delete(ndfPath + File.separator + com.axi.v810.util.HashUtil.hashName(projectName));
          }
        }
        catch (DatastoreException e)
        {
          //do nothing
        }
      }

    }

  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void checkAndModifyNewScanRouteGenerationOption(final Project project)
  {
    Assert.expect(project != null);

    boolean containMultipleSpeedSubtype = false;

    for(Subtype subtype : project.getPanel().getSubtypes())
    {
      SubtypeAdvanceSettings subtypeAdvanceSettings = subtype.getSubtypeAdvanceSettings();
      if(subtypeAdvanceSettings.getStageSpeedList().size() >= 2)
      {
        containMultipleSpeedSubtype = true;
        break;
      }
    }
    
    if(containMultipleSpeedSubtype && project.isGenerateByNewScanRoute() == false)
    {
      int response = JOptionPane.showConfirmDialog(_mainUI,
                                                   StringLocalizer.keyToString("MMGUI_CHANGE_NEW_SCAN_ROUTE_GENERATION_CONFIRM_KEY"),
                                                   StringLocalizer.keyToString("GUI_WARNING_TITLE_KEY"),
                                                   JOptionPane.YES_NO_OPTION,
                                                   JOptionPane.INFORMATION_MESSAGE);

      if (response == JOptionPane.YES_OPTION)
        project.setGenerateByNewScanRoute(true);
      else
        projectClose(true);
    }
  }
}
