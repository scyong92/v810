package com.axi.v810.gui.mainMenu;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.util.*;

/**
 * This class is for user to choose whether they want to create a new project with different system type
 * or to proceed to load the project with overriding the machine system type
 *
 * @author Poh Kheng
 */
class CreateProjectOptionDialog extends EscapeDialog implements ActionListener
{
  private BorderLayout _mainBorderLayout;
  private JPanel _northPanel;
  private JPanel _centerPanel;
  private JPanel _centerTopPanel;
  private JPanel _centerMiddlePanel;
  private JPanel _centerTopMiddlePanel;
  private JPanel _centerBottomMiddlePanel;
  private JPanel _centerBottomPanel;
  private JPanel _southPanel;
  private JPanel _innerButtonPanel = new JPanel();
  private GridLayout _innerButtonPanelGridLayout = new GridLayout();

  private JLabel _descriptionTitle;
  private JLabel _newProjectLabel;
  private JLabel _createNewProjectLabel;
  private JLabel _selectSystemTypeLabel;
  private JLabel _systemLowMagLabel;
  
  private JComboBox _systemTypeComboBox;
  private JComboBox _systemLowMagnificationComboBox;

  private JTextField _newProjectTextField;

  private JButton _okButton;
  private JButton _cancelButton;
  
  private String _newProjectName;
  private String _currentProjectName;
  private String _systemType;
  private SystemTypeEnum _selectedSystemType;
  private String _selectedSystemLowMagnification;
  private boolean _userCancel = true;
  
  private java.util.List<String> _supportedSystemType = null;
  private final String _M23 = "M23";
  private final String _M19 = "M19";
  private final String _M11 = "M11";
  private final String _M13 = "M13";

  /**
   * @author Kok Chun, Tan
   */
  CreateProjectOptionDialog(Frame parentFrame, boolean modal, String projectName, String systemType)
  {
    super(parentFrame, modal);

    Assert.expect(projectName != null);
    Assert.expect(systemType != null);

    _currentProjectName = projectName;
    _systemType = systemType;
    createPanel();
  }
  
  /**
   * @author weng-jian.eoh
   * @param parentFrame
   * @param modal
   * @param projectName
   * @param systemType
   * @param supportedSystemType
   * 
   * XCR-3589 Combo STD and XXL software GUI
   * added a combobox allow user to select system type user wanted to convert.
   */
  CreateProjectOptionDialog(Frame parentFrame, boolean modal, String projectName, String systemType, java.util.List<String> supportedSystemType)
  {
    super(parentFrame, modal);

    Assert.expect(projectName != null);
    Assert.expect(systemType != null);

    _currentProjectName = projectName;
    _systemType = systemType;
    _supportedSystemType = supportedSystemType;
    createPanel();
  }

  /**
   * @author Poh Kheng
   */
  private void createPanel()
  {
    init();
    setLayout(_mainBorderLayout);

    setTitle(StringLocalizer.keyToString("MMGUI_CREATE_PROJECT_TITLE_KEY"));

    _northPanel.setBackground(Color.WHITE);
    _northPanel.add(_descriptionTitle);

    if (_supportedSystemType != null)
    {
      JPanel systemTypePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
      systemTypePanel.add(_selectSystemTypeLabel);
      systemTypePanel.add(_systemTypeComboBox);
      
      JPanel systemLowMagPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
      if (_supportedSystemType.contains(SystemTypeEnum.S2EX.getName()))
      {
        systemLowMagPanel.add(_systemLowMagLabel);
        systemLowMagPanel.add(_systemLowMagnificationComboBox);
      }
      
      JPanel selectionPanel = new JPanel(new BorderLayout());
      selectionPanel.add(systemTypePanel,BorderLayout.NORTH);
      selectionPanel.add(systemLowMagPanel,BorderLayout.CENTER);
      
      _centerBottomPanel.add(selectionPanel);
    //  _centerBottomPanel.add(_systemTypeComboBox);
    }
    _centerTopMiddlePanel.add(_createNewProjectLabel);
    _centerBottomMiddlePanel.add(_newProjectLabel, FlowLayout.LEFT);
    _centerBottomMiddlePanel.add(_newProjectTextField, FlowLayout.CENTER);
    _centerBottomMiddlePanel.setBorder(BorderFactory.createEmptyBorder(0,20,0,0));
    _centerMiddlePanel.add(_centerTopMiddlePanel,BorderLayout.NORTH);
    _centerMiddlePanel.add(_centerBottomMiddlePanel,BorderLayout.CENTER);
//    _centerBottomPanel.add(_continueToLoadRadioButton);
    _centerPanel.add(_centerTopPanel,BorderLayout.NORTH);
    _centerPanel.add(_centerMiddlePanel,BorderLayout.CENTER);
    _centerPanel.add(_centerBottomPanel,BorderLayout.SOUTH);

    _southPanel.setBorder(BorderFactory.createEtchedBorder());
    _innerButtonPanel.add(_okButton);
    _innerButtonPanel.add(_cancelButton);

    _southPanel.add(_innerButtonPanel);

//    _createNewProjectRadioButton.setSelected(true);

    add(_northPanel,BorderLayout.NORTH);
    add(_centerPanel,BorderLayout.CENTER);
    add(_southPanel,BorderLayout.SOUTH);

    _okButton.addActionListener(this);
    _cancelButton.addActionListener(this);
  }

  private void init()
  {
    _mainBorderLayout = new BorderLayout(5,5);
    _innerButtonPanelGridLayout.setHgap(5);
    LocalizedString titleMessage = new LocalizedString("MMGUI_CREATE_PROJECT_FOR_SYSTEM_TYPES_TITLE_KEY",
                                                       new Object[]{XrayTester.getSystemType().getName()
                                                       , _systemType});

    _descriptionTitle = new JLabel(StringLocalizer.keyToString(titleMessage));
//    _centerTitle = new JLabel(StringLocalizer.keyToString("MMGUI_CREATE_PROJECT_OPTION_TITLE_KEY"));
    _newProjectLabel = new JLabel(StringLocalizer.keyToString("MMGUI_NEW_PROJECT_NAME_KEY"));

    //Siew Yeng - XCR-2992
    LocalizedString selectCreateMessage = new LocalizedString("MMGUI_SELECT_CREATE_PROJECT_KEY",
                                                       new Object[]{Config.getSystemCurrentLowMagnification(),
                                                                    Config.getSystemCurrentHighMagnification()});

    _createNewProjectLabel = new JLabel(StringLocalizer.keyToString(selectCreateMessage));
   
    if (_supportedSystemType != null)
    {
      _selectSystemTypeLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SELECT_SYSTEM_TYPE_KEY"));
      _systemTypeComboBox = new JComboBox();
      for (String systemType : _supportedSystemType)
      {
        _systemTypeComboBox.addItem(systemType);
      }
      String systemTypeName = XrayTester.getSystemType().getName();
      if (systemTypeName.equals(SystemTypeEnum.THROUGHPUT.getName()))
      {
        systemTypeName = SystemTypeEnum.STANDARD.getName().toUpperCase();
      }
      _systemTypeComboBox.setSelectedItem(systemTypeName);

      _systemLowMagnificationComboBox = new JComboBox();
      _systemLowMagLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SELECT_LOW_MAG_KEY"));
      _systemLowMagnificationComboBox.addItem(_M19);
      _systemLowMagnificationComboBox.addItem(_M23);
      if (systemTypeName.equals(SystemTypeEnum.S2EX.getName()))
      {
        String currentLowMag = Config.getSystemCurrentLowMagnification();
        _systemLowMagnificationComboBox.setSelectedItem(currentLowMag);
      }
      else
      {
        _systemLowMagnificationComboBox.setEnabled(false);
      }

      _systemTypeComboBox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          String systemTypeName = (String) _systemTypeComboBox.getSelectedItem();
          if (systemTypeName.equalsIgnoreCase(SystemTypeEnum.STANDARD.getName().toUpperCase()))
          {
            systemTypeName = SystemTypeEnum.THROUGHPUT.getName();
          }
          _selectedSystemType = SystemTypeEnum.getSystemType(systemTypeName);
          if (_selectedSystemType.equals(SystemTypeEnum.S2EX))
          {
            _systemLowMagnificationComboBox.setEnabled(true);
            _selectedSystemLowMagnification = (String) _systemLowMagnificationComboBox.getSelectedItem();
          }
          else
          {
            _systemLowMagnificationComboBox.setEnabled(false);
          }

          setupCreateProjectLabel();
        }
      });

      _systemLowMagnificationComboBox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _selectedSystemLowMagnification = (String) _systemLowMagnificationComboBox.getSelectedItem();

          setupCreateProjectLabel();
        }
      });
    }
//    _continueToLoadRadioButton = new JRadioButton(StringLocalizer.keyToString("MMGUI_SELECT_CONTINUE_TO_LOAD_KEY"));

//    _userChoiceButtonGroup = new ButtonGroup();
//    _userChoiceButtonGroup.add(_createNewProjectRadioButton);
//    _userChoiceButtonGroup.add(_continueToLoadRadioButton);
//    _createNewProjectRadioButton.setSelected(true);

    _okButton = new JButton(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));

    _cancelButton = new JButton(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));

    _innerButtonPanel.setLayout(_innerButtonPanelGridLayout);

    _newProjectTextField =  new JTextField(25);
    _newProjectTextField.setText(_currentProjectName);

    _northPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _centerPanel = new JPanel(new BorderLayout(5,5));
    _centerTopPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _centerMiddlePanel = new JPanel(new BorderLayout(5,5));
    _centerTopMiddlePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _centerBottomMiddlePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _centerBottomPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _centerPanel.setBorder(BorderFactory.createEtchedBorder());
    _southPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

  }


  /**
   * @author Poh Kheng
   */
  private void okButton_actionPerformed()
  {
    _userCancel = false;
    _newProjectName = _newProjectTextField.getText();
    if (_systemTypeComboBox != null)
    {
      String systemTypeName = (String)_systemTypeComboBox.getSelectedItem();
      if (systemTypeName.equalsIgnoreCase(SystemTypeEnum.STANDARD.getName().toUpperCase()))
      {
        systemTypeName = SystemTypeEnum.THROUGHPUT.getName();
      }
      _selectedSystemType = SystemTypeEnum.getSystemType(systemTypeName);
    }
    if (_systemLowMagnificationComboBox != null)
    {
      _selectedSystemLowMagnification = (String)_systemLowMagnificationComboBox.getSelectedItem();
    }
//    if(_createNewProjectRadioButton.isSelected())
//    {
    if (checkIfProjectNameExistAndValid() == false)
    {
      removeAllListeners();
      dispose();
    }
//    }
  }

  /**
   * @author Poh Kheng
   */
  public String getNewProjectName()
  {
    Assert.expect(_newProjectName != null);
    Assert.expect(userCancel() == false);
    return _newProjectName;
  }
  
   /**
   * @author weng-jian.eoh
   */
  public SystemTypeEnum getNewSystemType()
  {
    Assert.expect(userCancel() == false);
    
    return _selectedSystemType;
  }

  public String getLowMag()
  {
    Assert.expect(userCancel() == false);
    
    if (_systemLowMagnificationComboBox.isEnabled())
    {
      return _selectedSystemLowMagnification;
    }
    else
    {
      return null;
    }
  }
  /**
   * @author Poh Kheng
   */
//  public boolean isCreateNewProject()
//  {
//    Assert.expect(userCancel() == false);
//    return _createNewProjectRadioButton.isSelected();
//  }

  /**
   * Fix for CR 1040 (Wei Chin)
   * @author Poh Kheng
   */
//  public boolean isContinueLoadingProject()
//  {
//    return _continueToLoadRadioButton.isSelected();
//  }

  /**
   * @author Poh Kheng
   */
  public boolean userCancel()
  {
    return _userCancel;
  }

  /**
   * @author Poh Kheng
   */
  public boolean checkIfProjectNameExistAndValid()
  {
    Assert.expect(_newProjectName != null);
    // check for a valid project name
    if (Project.isProjectNameValid(_newProjectName) == false)
    {
      LocalizedString message;
        message = new LocalizedString("GUI_SAVE_AS_INVALID_PROJECT_NAME_KEY",
                                      new Object[]
                                      {_newProjectName, " " + FileName.getIllegalChars()});

        MessageDialog.showWarningDialog(this,
                                      StringLocalizer.keyToString(message),
                                      StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"));
        return true;
    }

    // check if project name exist.
    if(_currentProjectName.equals(_newProjectName) == false)
    {
      File projectNameDir = new File(Directory.getProjectDir(_newProjectName));

      if (projectNameDir.exists())
        if (projectNameDir.isDirectory())
          if (JOptionPane.showConfirmDialog(this, StringLocalizer.keyToString("PDD_CONFIRM_PROJECT_IMPORT_KEY"),
                                            StringLocalizer.keyToString("PDD_CONFIRM_PROJECT_IMPORT_TITLE_KEY"),
                                            JOptionPane.YES_NO_OPTION,
                                            JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION)
          {
            return true;
          }
    }

    return false;
  }

  /**
   * @author Poh Kheng
   * @param ae
   */
  public void actionPerformed(ActionEvent ae)
  {
    Object obj = ae.getSource();

    if (obj == _cancelButton)
    {
        cancel();
    }
    else if (obj == _okButton)
    {
        okButton_actionPerformed();
    }
  }

  /**
   * @author Poh Kheng
   */
  private void removeAllListeners()
  {
    _okButton.removeActionListener(this);
    _cancelButton.removeActionListener(this);
  }

  /**
   * @param e
   * @author Cheah Lee Herng
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if(e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      cancel();
    }
    super.processWindowEvent(e);
  }

  /**
   * @author Cheah Lee Herng
   */
  private void cancel()
  {
      _userCancel = true;
      removeAllListeners();
      dispose();
  }
  
   /**
   * @author weng-jian.eoh
   * 
   * to modify magnification due to S2EX have 2 different low magnification
   */
  private void setupCreateProjectLabel()
  {    
    SystemTypeEnum currentSelectedSystemType = SystemTypeEnum.getSystemType((String)_systemTypeComboBox.getSelectedItem());
    
    if (currentSelectedSystemType.equals(SystemTypeEnum.S2EX))
    {
      LocalizedString selectCreateMessage = new LocalizedString("MMGUI_SELECT_CREATE_PROJECT_KEY",
                                                       new Object[]{(String)_systemLowMagnificationComboBox.getSelectedItem(),
                                                                   _M11});

      _createNewProjectLabel.setText(StringLocalizer.keyToString(selectCreateMessage));
    }
    else if (currentSelectedSystemType.equals(SystemTypeEnum.XXL))
    {
      LocalizedString selectCreateMessage = new LocalizedString("MMGUI_SELECT_CREATE_PROJECT_KEY",
                                                       new Object[]{_M19,
                                                                    _M11});

      _createNewProjectLabel.setText(StringLocalizer.keyToString(selectCreateMessage));
    }
    else
    {
      LocalizedString selectCreateMessage = new LocalizedString("MMGUI_SELECT_CREATE_PROJECT_KEY",
                                                       new Object[]{_M19,
                                                                    _M11});

      _createNewProjectLabel.setText(StringLocalizer.keyToString(selectCreateMessage));
    }
  }
}
