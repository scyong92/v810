package com.axi.v810.gui.mainMenu;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;

/**
 * The AbstractNavigationPanel class provides the framework
 * for Genesis navigation button panels. This class supports
 * navigation within environment panels.
 * @author George Booth
*/

public class AbstractNavigationPanel extends JPanel
{
  private JPanel _innerNavigationPanel = null;
  private String _buttonNames[] = null;
  private String _disabledButtonNames[] = null;
  private ColoredButton _buttons[] = null;
  private Border _navigationPanelBorder;

  private AbstractEnvironmentPanel _envPanel = null;
  private boolean _singleTask = true;
  private int _numButtons = 0;
  private String _currentButtonName = null;
  boolean _buttonsEnabled = true;

  /**
   * Define a navigation panel for environments with multiple tasks
   * @author George Booth
   */
  public AbstractNavigationPanel(String[] buttonNames, AbstractEnvironmentPanel envPanel)
  {
    Assert.expect(buttonNames != null);
    Assert.expect(envPanel != null);
    _buttonNames = buttonNames;
    _numButtons = buttonNames.length;
    _envPanel = envPanel;
    _singleTask = false;
    jbInit();
    enableAllButtons();
  }

  /**
   * Define a navigation panel for environments with multiple tasks, where it starts with all buttons disabled
   * @param buttonNames String[]
   * @param disabledButtonNames String[]
   * @param envPanel emvironment panel for this nav panel
   * @author George Booth
   */
  public AbstractNavigationPanel(String[] buttonNames, String[] disabledButtonNames, AbstractEnvironmentPanel envPanel)
  {
    Assert.expect(buttonNames != null);
    Assert.expect(disabledButtonNames != null);
    Assert.expect(buttonNames.length == disabledButtonNames.length);
    Assert.expect(envPanel != null);
    _buttonNames = buttonNames;
    _disabledButtonNames = disabledButtonNames;
    _numButtons = buttonNames.length;
    _envPanel = envPanel;
    _singleTask = false;
    jbInit();
    disableAllButtons(true);
  }

  /**
   * Define a navigation panel for environments with a single tasks
   * @param envPanel emvironment panel for this nav panel
   * @author George Booth
   */
  public AbstractNavigationPanel(AbstractEnvironmentPanel envPanel)
  {
    Assert.expect(envPanel != null);
    _buttonNames = null;
    _numButtons = 0;
    _envPanel = envPanel;
    _singleTask = true;
  }

  /**
   * For navigation panels using enabled and disabled button names, return the current set
   * @author George Booth
   */
  public String[] getCurrentButtonNames()
  {
    if (_buttonsEnabled)
    {
      return _buttonNames;
    }
    else
    {
      return _disabledButtonNames;
    }
  }

  /**
   * @author George Booth
   */
  private void jbInit()
  {
    _navigationPanelBorder = BorderFactory.createCompoundBorder(
        BorderFactory.createBevelBorder(
            BevelBorder.LOWERED, Color.white, new Color(182, 182, 182),
            new Color(62, 62, 62), new Color(89, 89, 89)),
        BorderFactory.createEmptyBorder(1, 1, 1, 1));

    setLayout(new HorizontalFlowLayout(HorizontalFlowLayout.LEFT,HorizontalFlowLayout.TOP,0,0));
//    setBackground(SystemColor.controlShadow);
    setBackground(new Color(200,230,240));
    setBorder(_navigationPanelBorder);

    _innerNavigationPanel = new JPanel();
    _innerNavigationPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
//    _innerNavigationPanel.setLayout(new GridLayout(_numButtons, 1, 0, 15));
//    _innerNavigationPanel.setBackground(SystemColor.controlShadow);
    _innerNavigationPanel.setBackground(new Color(200,230,240));
    add(_innerNavigationPanel);

    _buttons = new ColoredButton[_numButtons];
    for (int i = 0; i < _numButtons; i++)
    {
      _buttons[i] = new ColoredButton();
      _buttons[i].setEnabled(false);
      _buttons[i].setText(_buttonNames[i]);
      _buttons[i].addActionListener(new java.awt.event.ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          navigationButtonClicked(e);
        }
      });
      _innerNavigationPanel.add(_buttons[i]);
    }
  }

  /**
   * @author George Booth
   */
  public void selectDefaultTask()
  {
    if (_singleTask)
    {
      _envPanel.navButtonClicked("");
    }
    else
    {
      boolean currentState = _buttons[0].isEnabled();
      // make sure button[0] is enabled or click is ignored
      _buttons[0].setEnabled(true);
      _buttons[0].doClick();
      // restore the current state
      _buttons[0].setEnabled(currentState);
    }
  }

  /**
   * @author George Booth
   */
  public void disableAllButtons(boolean revertToDefaultButton)
  {
    for (int i = 0; i < _numButtons; i++)
    {
      _buttons[i].setEnabled(false);
      _buttons[i].setText(_disabledButtonNames[i]);
      if (revertToDefaultButton)
        _buttons[i].removeCustomBackground();
    }
    if (revertToDefaultButton)
      _buttons[0].setCustomBackground(Color.yellow);
    _buttonsEnabled = false;
  }

  /**
   * Enables all buttons in the panel.  Ususally called after a project is loaded
   * @author George Booth
   */
  public void enableAllButtons()
  {
    for (int i = 0; i < _numButtons; i++)
    {
      _buttons[i].setEnabled(true);
      _buttons[i].setText(_buttonNames[i]);
    }
    _buttonsEnabled = true;
  }

  /**
   * @author George Booth
   */
  private void navigationButtonClicked(ActionEvent e)
  {
    Assert.expect(e != null);
    Assert.expect(e.getSource() instanceof AbstractButton);
    AbstractButton thisButton = (AbstractButton)e.getSource();
    String name = thisButton.getText();
    // make sure we are comparing to the right set of names
    String buttonNames[] = getCurrentButtonNames();
   int index = -1;
    for (int i = 0; i < _numButtons; i++)
    {
      if (buttonNames[i].equals(name))
      {
        index = i;
        break;
      }
    }
    Assert.expect(index != -1);

    // if the current button was clicked again, we don't want to
    // re-navigate to the same panel because the start() method would be
    // called again
    if (_currentButtonName == null)
    {
      if (_envPanel.currentTaskPanelReadyToFinish())
      {
        _currentButtonName = name;
        _envPanel.navButtonClicked(name);
        highlightButton(_buttons[index]);
      }
    }
    else if (_currentButtonName.equals(name) == false)
    {
      if (_envPanel.currentTaskPanelReadyToFinish())
      {
        _currentButtonName = name;
        _envPanel.navButtonClicked(name);
        highlightButton(_buttons[index]);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void clickButton(String name)
  {
    Assert.expect(name != null);
    for (int i = 0; i < _numButtons; i++)
    {
      if (_buttons[i].getText().equals(name))
      {
        _buttons[i].doClick();
        return;
      }
    }
    Assert.expect(false, "Couldn't find button with name: " + name);
  }

  /**
   * Used when navigating away from an environment to clear out the current
   * button state.
   * @author Andy Mechtenberg
   */
  public void clearCurrentNavigation()
  {
    _currentButtonName = null;
  }

  /**
   * @author George Booth
   */
  public void highlightButton(String name)
  {
    Assert.expect(name != null);
    for (int i = 0; i < _numButtons; i++)
    {
      if (_buttons[i].getText().equals(name))
      {
        highlightButton(_buttons[i]);
        _currentButtonName = _buttons[i].getText();
        return;
      }
    }
    Assert.expect(false, "Couldn't find button with name: " + name);

  }

  /**
   * Highlight this button to allow user to know which screen is active
   * @author George Booth
   */
  private void highlightButton(ColoredButton buttonToHighlight)
  {
    Assert.expect(buttonToHighlight != null);
    // first, de-highlight ALL buttons, then we'll highlight the one passed in
    for (int i = 0; i < _numButtons; i++)
    {
      _buttons[i].removeCustomBackground();
    }

    buttonToHighlight.setCustomBackground(Color.yellow);
    buttonToHighlight.requestFocus();
  }
}
