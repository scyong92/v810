package com.axi.v810.gui.mainMenu;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.util.*;
import com.axi.guiUtil.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

import com.axi.v810.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import static com.axi.v810.gui.mainMenu.MainMenuGui.finishLicenseMonitor;
import com.axi.v810.hardware.*;
import java.util.logging.*;

/**
 * This class is used for switching low and high magnification.
 * 
 * @author ngie-xing.wong
 */
public class ChangeSystemMagnificationDialog extends JDialog
{
  private JPanel _mainPanel = new JPanel();
  
  private Border _warningTitledBorder;
  private JPanel _warningMainPanel = new JPanel();
  private JPanel _warningPanel = new JPanel();
  private JLabel _warningLabel = new JLabel();
  
  private JPanel _lowMagnificationPanel = new JPanel();
  private JLabel _lowMagnificationLabel = new JLabel();
  private JComboBox _lowMagnificationComboBox;
  private JPanel _highMagnificationPanel = new JPanel();
  private JLabel _highMagnificationLabel = new JLabel();
  private JComboBox _highMagnificationComboBox;
  
  private JPanel _okCancelPanel = new JPanel();
  private JButton _restartNowButton = new JButton();
  private JButton _cancelButton = new JButton();
  
  private final String[] _lowMagnifications = SystemConfigEnum.getLowMagnificationNames();
  private final String[] _highMagnifications = SystemConfigEnum.getHighMagnificationNames();
  
  private static String _lowMagOnNextSystemStartUp = Config.getInstance().getSystemCurrentLowMagnification();
  private static String _highMagOnNextSystemStartUp = Config.getInstance().getSystemCurrentHighMagnification();
  private int _returnValue; // either OK or CANCEL depending on how the user exits
  
  private boolean _isOLP;
  private MainMenuGui _mainUI;
  /**
   * @author Ngie Xing
   */
  ChangeSystemMagnificationDialog(Frame frame,
    boolean modal)
  {
    super(frame,
          StringLocalizer.keyToString("GUI_CHANGE_SYSTEM_MAGNIFICATION_KEY"),
          modal);
    
    _mainUI = (MainMenuGui)frame;
    _lowMagOnNextSystemStartUp = Config.getInstance().getSystemCurrentLowMagnification();
    _highMagOnNextSystemStartUp = Config.getInstance().getSystemCurrentHighMagnification();
    jbInit();
    pack();    
  }
  
  /**
   * @author Ngie Xing
   */
  private void jbInit()
  {
    /////init the components for warning text
    try
    {
      _isOLP = LicenseManager.isOfflineProgramming();
    }
    catch (final BusinessException e)
    {
      finishLicenseMonitor();
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  e.getMessage(),
                  StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                  true);
          MainMenuGui.getInstance().exitV810(false);
        }
      });
    }
    
    _warningTitledBorder = BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, ColorUtil.getGrayShadowColor()),
      StringLocalizer.keyToString("GUI_CHANGE_SYSTEM_MAGNIFICATION_WARNING_KEY")),
      BorderFactory.createEmptyBorder(0, 5, 0, 5));
    _warningPanel.setLayout(new BoxLayout(_warningPanel, BoxLayout.X_AXIS));
    _warningPanel.setMinimumSize(new Dimension(430, 100));
    _warningPanel.setPreferredSize(new Dimension(430, 100));
    _warningPanel.setMaximumSize(new Dimension(430, 100));
    _warningPanel.setBorder(_warningTitledBorder);

    if (_isOLP == false)
    {
      _warningLabel.setText(StringLocalizer.keyToString("GUI_CHANGE_SYSTEM_MAGNIFICATION_WARNING_LABEL_KEY"));
    }
    else
    {
      String projectName = Project.getCurrentlyLoadedProject().getName();
      String format ="width:320px ; font-size:13";
      LocalizedString warningMessage = new LocalizedString("GUI_CHANGE_SYSTEM_MAGNIFICATION_WARNING_LABEL_OLP_KEY",new Object[]{format,projectName});
      _warningLabel.setText(StringLocalizer.keyToString(warningMessage));
    }
    
    _warningPanel.add(_warningLabel);
    _warningMainPanel.setLayout(new BoxLayout(_warningMainPanel, BoxLayout.X_AXIS));
    _warningMainPanel.add(Box.createHorizontalGlue());
    _warningMainPanel.add(Box.createRigidArea(new Dimension(10, 0)));
    _warningMainPanel.add(_warningPanel);
    _warningMainPanel.add(Box.createRigidArea(new Dimension(10, 0)));
    _warningMainPanel.add(Box.createHorizontalGlue());
    /////warning text init end

    /////init components for low magnification selection
    _lowMagnificationLabel.setText(StringLocalizer.keyToString("GUI_CHANGE_SYSTEM_MAGNIFICATION_DIALOG_LOW_MAG_KEY"));
    _lowMagnificationComboBox = new JComboBox(_lowMagnifications);
    int lowMagCount = _lowMagnificationComboBox.getItemCount();
    for (int i = 0; i <= lowMagCount; i++)
    {
      if (_lowMagOnNextSystemStartUp.equals(_lowMagnificationComboBox.getItemAt(i)))
      {
        _lowMagnificationComboBox.setSelectedIndex(i);
        break;
      }
    }
    
    _lowMagnificationComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        lowMagnificationComboBox_actionPerformed();
      }
    });

    _lowMagnificationPanel.setLayout(new GridLayout(1, 2, 10 , 10));
    _lowMagnificationPanel.setMinimumSize(new Dimension(200, 30));
    _lowMagnificationPanel.setPreferredSize(new Dimension(200, 30));
    _lowMagnificationPanel.setMaximumSize(new Dimension(220, 30));
    _lowMagnificationPanel.add(_lowMagnificationLabel);
    _lowMagnificationPanel.add(_lowMagnificationComboBox);
    //end

    /////init components for high magnification selection
    _highMagnificationLabel.setText(StringLocalizer.keyToString("GUI_CHANGE_SYSTEM_MAGNIFICATION_DIALOG_HIGH_MAG_KEY"));
    _highMagnificationComboBox = new JComboBox(_highMagnifications);
    //Ngie Xing, disable high magnification combobox until there are more magnifications
    _highMagnificationComboBox.setEnabled(false);
    _highMagnificationComboBox.setRenderer(new DefaultListCellRenderer()
    {
      @Override
      public void paint(Graphics g)
      {
        setForeground(Color.BLACK);
        super.paint(g);
      }
    });

    int highMagCount = _highMagnificationComboBox.getItemCount();
    for (int i = 0; i <= highMagCount; i++)
    {
      if (_highMagOnNextSystemStartUp.equals(_highMagnificationComboBox.getItemAt(i)))
      {
        _highMagnificationComboBox.setSelectedIndex(i);
        break;
      }
    }
    
    _highMagnificationComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        highMagnificationComboBox_actionPerformed();
      }
    });

    _highMagnificationPanel.setLayout(new GridLayout(1, 2, 10 , 10));
    _highMagnificationPanel.setMinimumSize(new Dimension(150, 30));
    _highMagnificationPanel.setPreferredSize(new Dimension(150, 30));
    _highMagnificationPanel.setMaximumSize(new Dimension(220, 30));
    _highMagnificationPanel.add(_highMagnificationLabel);
    _highMagnificationPanel.add(_highMagnificationComboBox);
    /////end

    /////init ok and cancel button    
    _restartNowButton.setEnabled(false);
    _restartNowButton.setPreferredSize(new Dimension(100, 27));
    if (_isOLP == false)
    {
      _restartNowButton.setText(StringLocalizer.keyToString("GUI_CHANGE_SYSTEM_MAGNIFICATION_DIALOG_RESTART_NOW_BUTTON_KEY"));
      _restartNowButton.addActionListener(new java.awt.event.ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          restartNowButton_actionPerformed();
        }
      });
    }
    else
    {
      _restartNowButton.setText(StringLocalizer.keyToString("GUI_CHANGE_SYSTEM_MAGNIFICATION_DIALOG_CONVERT_BUTTON_KEY"));
      _restartNowButton.addActionListener(new java.awt.event.ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          changeProjectLowMagnificationForOfflineProgramming();
        }
      });
    }

    _cancelButton.setPreferredSize(new Dimension(73, 27));
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CHANGE_SYSTEM_MAGNIFICATION_DIALOG_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed();
      }
    });

    FlowLayout okCancelPanelLayout = new FlowLayout(FlowLayout.CENTER, 10, 10);
    _okCancelPanel.setLayout(okCancelPanelLayout);
    _okCancelPanel.add(_restartNowButton);
    _okCancelPanel.add(_cancelButton);
    /////ok and cancel button end

    // set close
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    addWindowListener(new WindowAdapter()
    {
      public void windowClosing(WindowEvent e)
      {
        cancelButton_actionPerformed();
      }
    });

    //add the rest of the components together
    _mainPanel.setLayout(new BoxLayout(_mainPanel, BoxLayout.Y_AXIS));
    _mainPanel.add(Box.createRigidArea(new Dimension(0, 10)));
    _mainPanel.add(_warningMainPanel);

    _mainPanel.add(Box.createRigidArea(new Dimension(5, 15)));
    _mainPanel.add(_lowMagnificationPanel);
    _mainPanel.add(Box.createRigidArea(new Dimension(5, 10)));
    _mainPanel.add(_highMagnificationPanel);

    _mainPanel.add(Box.createRigidArea(new Dimension(5, 10)));
    _mainPanel.add(_okCancelPanel);

    add(_mainPanel);
  }
  
  /*
   * @author Ngie Xing
   */
  private void restartNowButton_actionPerformed()
  {
    try
    {
      //Store the information for the low/ high magnification,
      if (_lowMagOnNextSystemStartUp.equalsIgnoreCase(String.valueOf(_lowMagnificationComboBox.getSelectedItem())) == false)
      {
        System.out.println("System been switch from " + _lowMagOnNextSystemStartUp + " to " + _lowMagnificationComboBox.getSelectedItem());
        _lowMagOnNextSystemStartUp = String.valueOf(_lowMagnificationComboBox.getSelectedItem());
        Config.getInstance().setValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION, _lowMagOnNextSystemStartUp);
        MainMenuGui.getInstance().setIsMagnificationChanged();
      }
      if (_highMagOnNextSystemStartUp.equalsIgnoreCase(String.valueOf(_highMagnificationComboBox.getSelectedItem())) == false)
      {
        System.out.println("System been switch from " + _highMagOnNextSystemStartUp + " to " + _highMagnificationComboBox.getSelectedItem());
        _highMagOnNextSystemStartUp = String.valueOf(_highMagnificationComboBox.getSelectedItem());
        Config.getInstance().setValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_HIGH_MAGNIFICATION, _highMagOnNextSystemStartUp);
        MainMenuGui.getInstance().setIsMagnificationChanged();
      }
      _returnValue = JOptionPane.OK_OPTION;
    }
    catch (DatastoreException ex)
    {
      MainMenuGui.getInstance().handleXrayTesterException(ex);
    }
    finally
    {
      dispose();
    }
  }
  
  /*
   * @author Ngie Xing
   */
  private void cancelButton_actionPerformed()
  {
    _returnValue = JOptionPane.CANCEL_OPTION;
    dispose();
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void lowMagnificationComboBox_actionPerformed()
  {
    if (_lowMagOnNextSystemStartUp.equalsIgnoreCase(String.valueOf(_lowMagnificationComboBox.getSelectedItem())))
      _restartNowButton.setEnabled(false);
    else
      _restartNowButton.setEnabled(true);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void highMagnificationComboBox_actionPerformed()
  {
    if (_highMagOnNextSystemStartUp.equalsIgnoreCase(String.valueOf(_highMagnificationComboBox.getSelectedItem())))
      _restartNowButton.setEnabled(false);
    else
      _restartNowButton.setEnabled(true);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public int showDialog()
  {
    setVisible(true);
    return _returnValue;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * 
   * @author weng-jian.eoh
   */
  private void changeProjectLowMagnificationForOfflineProgramming()
  {
    this.setVisible(false);
    Assert.expect(Project.isCurrentProjectLoaded());
    
    String newLowMagnification = (String)_lowMagnificationComboBox.getSelectedItem();
    Project loadProject = Project.getCurrentlyLoadedProject();
    String projName = loadProject.getName();
    if (loadProject.hasBeenModified())
    {
      LocalizedString saveQuestion = new LocalizedString("MM_GUI_SAVE_CHANGES_KEY",
        new Object[]
        {
          projName
        });
      int answer = JOptionPane.showConfirmDialog(_mainUI,
        StringUtil.format(StringLocalizer.keyToString(saveQuestion), 50),
        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
        JOptionPane.YES_NO_OPTION,
        JOptionPane.WARNING_MESSAGE);
      if (answer == JOptionPane.YES_OPTION)
      {
        loadProject.setProjectLowMagnification(newLowMagnification);
        if (loadProject.getPanel().getPanelSettings().isPanelBasedAlignment())
        {
          loadProject.getPanel().getPanelSettings().clearAllAlignmentTransforms();
        }
        else
        {
          for (Board board : loadProject.getPanel().getBoards())
          {
            board.getBoardSettings().clearAllAlignmentTransforms();
          }
        }
        _mainUI.getMainMenuBar().saveProject();
        _mainUI.getMainMenuBar().loadOrImportProject(true, projName);
      }
    }
    else
    {
      loadProject.setProjectLowMagnification(newLowMagnification);
      if (loadProject.getPanel().getPanelSettings().isPanelBasedAlignment())
      {
        loadProject.getPanel().getPanelSettings().clearAllAlignmentTransforms();
      }
      else
      {
        for (Board board : loadProject.getPanel().getBoards())
        {
          board.getBoardSettings().clearAllAlignmentTransforms();
        }
      }
      _mainUI.getMainMenuBar().saveProject();
      _mainUI.getMainMenuBar().loadOrImportProject(true, projName);
    }
    
    dispose();
  }
 
}
