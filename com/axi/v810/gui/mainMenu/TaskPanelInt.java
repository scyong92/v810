package com.axi.v810.gui.mainMenu;

/**
 *
 * <p>Title: TaskPanelInt</p>
 *
 * <p>Description: This interface defines the methods required to be
 * implemened by a panel supporting a Genesis task. A task is a
 * single GUI screen used to perform a related set of operations. </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 * @version 1.0
 */

public interface TaskPanelInt
{
  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start();

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author George Booth
   */
  public boolean isReadyToFinish();

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish();

}
