package com.axi.v810.gui.mainMenu;

import com.axi.util.Assert;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.axi.v810.business.server.SchedulingTaskServer;
import com.axi.v810.business.server.SchedulerTask;
import com.axi.v810.util.DailyIterator;
/**
 *
 * @author chin-seong.kee
 */
public class AlarmClock 
{
    private final SchedulingTaskServer scheduler = new SchedulingTaskServer();
    private final SimpleDateFormat dateFormat =
        new SimpleDateFormat("dd MMM yyyy HH:mm:ss.SSS");
    private int _hourOfDay, _minute, _second;
    private MainMenuGui _mainUI;

    public AlarmClock(MainMenuGui mainMenu) 
    {
      Assert.expect(mainMenu != null);
      
      //initialize everything in here.
      this._hourOfDay = 0;
      this._minute = 0;
      this._second = 0;
      this._mainUI = mainMenu;
    }

    public void scheduleTask(int hourOfDay, int minute, int second) 
    {
      Assert.expect(_mainUI != null);
      Assert.expect(_hourOfDay >= 0);
      Assert.expect(_minute >= 0);
      Assert.expect(_second >= 0);
      
      _hourOfDay = hourOfDay;
      _minute = minute;
      _second = second;
      
      scheduler.schedule(new SchedulerTask() 
      {
        public void run()
        {
            actionTask();
        }
        private void actionTask() 
        {
          _mainUI.getMainMenuBar().startup();
                //System.out.println("Wake up! " +
                //    "It's " + dateFormat.format(new Date()));
                // Start a new thread to sound an alarm...
        }
      }, new DailyIterator(_hourOfDay, _minute, _second));
    }
    
    public void stop()
    {
      Assert.expect(scheduler != null);
      scheduler.cancel(); 
    }
    
    public void rescheduleTask(int hourOfDay, int minute, int second)
    {
      //do nothing for now
    }
}
