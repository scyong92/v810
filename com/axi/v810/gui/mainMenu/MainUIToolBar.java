package com.axi.v810.gui.mainMenu;


import com.axi.v810.util.InfoHandlerPanel;
import com.axi.guiUtil.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;


import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.config.*;
import com.axi.v810.gui.home.*;
import static com.axi.v810.gui.mainMenu.MainMenuGui.finishLicenseMonitor;
import com.axi.v810.gui.service.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.gui.testExec.*;
import com.axi.v810.gui.virtualLive.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * <p>This creates tool bars for the Main UI or Environment Panels
 *
 * <p>Main UI tool bar buttons are 32x32 pixels and are used to
 * navigate between environments.  There are no other buttons on
 * this tool bar.
 *
 * <p>Environment tool bar buttons are 16x16 pixels and are used to
 * support operations within that environment. Project Open and Project
 * Save buttons are always on the left.
 *
 * @author George Booth
 */

public class MainUIToolBar extends JToolBar implements Observer
{
  private XrayTester _xRayTester = XrayTester.getInstance();
  private AbstractXraySource _xraySource = AbstractXraySource.getInstance();
  private HardwareObservable _hardwareObservable = HardwareObservable.getInstance();
  private WorkerThread _monitorThread = new WorkerThread("Xray Source Monitor");
  private MainMenuGui _mainUI = null;
  private JButton _mainButton = null;
  private JButton _testDevButton = null;
  private JButton _testExecButton = null;
  private JButton _configureButton = null;
  private JButton _servicesButton = null;
  private JButton _virtualLiveButton = null;
  private JButton _openProjectButton = null;
  private JButton _saveProjectButton = null;
  private JButton _generateTestCoverageReport = null;
  private JButton _twoPinLandPatternChecking = null;
  
  private JButton _changeSystemMagnificationButton = null;
  /*
   * @author chin-seong.kee - Cad creator
   */
  private JButton _cadCreatorButton = null;
  
  private JButton _startDefectPackager = null;
  private JButton _stopDefectPackager = null;
  private Component _horizontalBox = null;
  
  private JButton _importNdfButton = null;
  private JButton _closeProjectButton = null;
  private JButton _logoutButton = null;
//  private JButton _undoButton = null;
//  private JButton _redoButton = null;
  private JButton _startupButton = null;
  private JButton _shutdownButton = null;
  private JButton _shortTermXrayShutdownButton = null;
  private JButton _loadPanelButton = null;
  private JButton _unloadPanelButton = null;
  private JButton _shutdownRMSButton = null;
  
  private JButton _reloadConfigButton = null;
  private JButton _errorHandlerButton = null;
  
  private Color _buttonBackground;
  private Color _activeButtonBackground = Color.BLACK;

  // Vector of things added to MainUIToolBar, indexed by _currentRequest
  private java.util.List<ToolBarChanges> _toolBarChanges = null;
  private int _currentRequest = -1;
  
  private final static String _HOME_ENV = "Main";
  private final static String _TEST_DEV_ENV = "Recipes";
  private final static String _TEST_EXEC_ENV = "Production";
  private final static String _CONFIG_ENV = "Options";
  private final static String _VIRTUAL_LIVE_ENV = "VirtualLive";
  private final static String _SERVICE_ENV = "Services";
  private static String _lastVisitPanel = null;

  //sheng chuan
  private JLabel _anodeValueLabel = new JLabel();
  private JLabel _cathodeValueLabel = new JLabel();
  private JLabel _xrayStatusLabel = new JLabel();
  
  private double _currentAnodeValue;
  private double _currentCathodeValue;
  private boolean _monitorOn = false;

  private static final ImageIcon _xrayPowerOnIcon = Image5DX.getImageIcon(Image5DX.XRAY_POWER_ON_STATUS);
  private static final ImageIcon _xrayPowerOffIcon = Image5DX.getImageIcon(Image5DX.XRAY_POWER_OFF_STATUS);
  
  private JPanel _xrayPowerMeterPanel = new JPanel(new FlowLayout());
  
  /**
   * Constructor used by the Main UI
   * @author George Booth
   */
  public MainUIToolBar(MainMenuGui mainUI)
  {
    Assert.expect(mainUI != null);
    _mainUI = mainUI;

    // initialize the toolBar tracking objects
    _toolBarChanges = new Vector<ToolBarChanges>();
    _currentRequest = -1;

    jbInitForMainUI();
  }

  /**
   * Constructor used by Environment Panels
   * @author George Booth
   */
  public MainUIToolBar(MainMenuGui mainUI, AbstractEnvironmentPanel envPanel)
  {
    Assert.expect(mainUI != null);
    _mainUI = mainUI;
    // envPanel is only used to change the signature

    // initialize the toolBar tracking objects
    _toolBarChanges = new Vector<ToolBarChanges>();
    _currentRequest = -1;
    
    _hardwareObservable.addObserver(this);

    jbInitForEnvironment();
  }

  /**
   * Constructor used by Environment Panels that want to start with an empty tool bar
   * (no open/save icons)
   * @author George Booth
   */
  public MainUIToolBar(MainMenuGui mainUI, AbstractEnvironmentPanel envPanel, boolean noToolBar)
  {
    Assert.expect(mainUI != null);
    _mainUI = mainUI;
    // envPanel and noToolBar is only used to change the signature

    // initialize the toolBar tracking objects
    _toolBarChanges = new Vector<ToolBarChanges>();
    _currentRequest = -1;
  }

  /**
   * Initialize the tool bar for use by the Main UI.
   * Provides the 4 environment navigation buttons.
   * @author George Booth
   */
  private void jbInitForMainUI()
  {
    _mainButton = new JButton();
    _buttonBackground = _mainButton.getBackground();
    _mainButton.setIcon(Image5DX.getImageIcon(Image5DX.HOME_ICON));
    _mainButton.setDisabledIcon(Image5DX.getImageIcon(Image5DX.HOME_DISABLE_ICON));
    _mainButton.setText(StringLocalizer.keyToString("GUI_AXI_HOME_BUTTON_KEY"));
    _mainButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _mainButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    _mainButton.setBackground(_activeButtonBackground);
    _mainButton.setToolTipText(StringLocalizer.keyToString("GUI_AXI_HOME_TOOLBAR_TOOLTIP_KEY"));
    _mainButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        _lastVisitPanel = _HOME_ENV;
        switchToAXIHome();
      }
    });

    _testDevButton = new JButton();
    _testDevButton.setIcon(Image5DX.getImageIcon(Image5DX.TEST_DEVELOPMENT_ICON));
    _testDevButton.setText(StringLocalizer.keyToString("GUI_AXI_TEST_DEV_BUTTON_KEY"));
    _testDevButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _testDevButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    _testDevButton.setDisabledIcon(Image5DX.getImageIcon(Image5DX.TEST_DEVELOPMENT_DISABLE_ICON));
    _testDevButton.setToolTipText(StringLocalizer.keyToString("GUI_TEST_DEV_TOOLBAR_TOOLTIP_KEY"));
    _testDevButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _lastVisitPanel = _TEST_DEV_ENV;
        switchToTestDev();
      }
    });

    _testExecButton = new JButton();
    _testExecButton.setIcon(Image5DX.getImageIcon(Image5DX.TEST_EXECUTION_RUN_ICON));
    _testExecButton.setDisabledIcon(Image5DX.getImageIcon(Image5DX.TEST_EXECUTION_RUN_DISABLE_ICON));
    _testExecButton.setText(StringLocalizer.keyToString("GUI_AXI_TEST_EXEC_BUTTON_KEY"));
    _testExecButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _testExecButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    _testExecButton.setToolTipText(StringLocalizer.keyToString("GUI_TEST_EXEC_TOOLBAR_TOOLTIP_KEY"));
    _testExecButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _lastVisitPanel = _TEST_EXEC_ENV;
        switchToTestExec();
      }
    });

    _configureButton = new JButton();
    _configureButton.setIcon(Image5DX.getImageIcon(Image5DX.CONFIG_BUTTON_ICON));
    _configureButton.setDisabledIcon(Image5DX.getImageIcon(Image5DX.CONFIG_DISABLE_BUTTON_ICON));
    _configureButton.setText(StringLocalizer.keyToString("GUI_AXI_CONFIG_BUTTON_KEY"));
    _configureButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _configureButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    _configureButton.setToolTipText(StringLocalizer.keyToString("GUI_CONFIG_TOOLBAR_TOOLTIP_KEY"));
    _configureButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _lastVisitPanel = _CONFIG_ENV;
        switchToConfigure();
      }
    });

    _servicesButton = new JButton();
    _servicesButton.setIcon(Image5DX.getImageIcon(Image5DX.SERVICES_BUTTON_ICON));
    _servicesButton.setDisabledIcon(Image5DX.getImageIcon(Image5DX.SERVICES_DISABLE_BUTTON_ICON));
    _servicesButton.setText(StringLocalizer.keyToString("GUI_AXI_SERVICES_BUTTON_KEY"));
    _servicesButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _servicesButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    _servicesButton.setToolTipText(StringLocalizer.keyToString("GUI_SERVICES_TOOLBAR_TOOLTIP_KEY"));
    _servicesButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _lastVisitPanel = _SERVICE_ENV;
        switchToService();
      }
    });
    //Zi Yang
    _virtualLiveButton = new JButton();
    _virtualLiveButton.setIcon(Image5DX.getImageIcon(Image5DX.VIRTUAL_LIVE_ICON));
    _virtualLiveButton.setDisabledIcon(Image5DX.getImageIcon(Image5DX.SERVICES_DISABLE_BUTTON_ICON));
    _virtualLiveButton.setText(StringLocalizer.keyToString("GUI_AXI_VIRTUAL_LIVE_BUTTON_KEY"));
    _virtualLiveButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _virtualLiveButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    _virtualLiveButton.setToolTipText(StringLocalizer.keyToString("GUI_VIRTUAL_LIVE_TOOLBAR_TOOLTIP_KEY"));
    _virtualLiveButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _lastVisitPanel = _VIRTUAL_LIVE_ENV;
        switchToVirtualLive();
      }
    });
    
    _cadCreatorButton = new JButton();
    _cadCreatorButton.setIcon(Image5DX.getImageIcon(Image5DX.CAD_CREATOR));
    _cadCreatorButton.setDisabledIcon(Image5DX.getImageIcon(Image5DX.TEST_DEVELOPMENT_DISABLE_ICON));
    _cadCreatorButton.setText(StringLocalizer.keyToString("GUI_AXI_CAD_CREATOR_BUTTON_KEY"));
    _cadCreatorButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _cadCreatorButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    _cadCreatorButton.setToolTipText(StringLocalizer.keyToString("GUI_CAD_CREATOR_TOOLBAR_TOOLTIP_KEY"));
    _cadCreatorButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        switchToCadCreator();
      }
    });

    Insets margins = new Insets(0, 2, 2, 0);
    _mainButton.setMargin(margins);
    _testDevButton.setMargin(margins);
    _testExecButton.setMargin(margins);
    _configureButton.setMargin(margins);
    _servicesButton.setMargin(margins);
    _virtualLiveButton.setMargin(margins);
    
    //Cad Creator - Chin Seong Kee
    _cadCreatorButton.setMargin(margins);
    
    addEnvironmentButtons();

    _mainButton.setName(".mainButton");
    _testDevButton.setName(".testDevButton");
    _testExecButton.setName(".testExecButton");
    _configureButton.setName(".configureButton");
    _servicesButton.setName(".servicesButton");
    _virtualLiveButton.setName(".virtualLiveButton");
    
    _cadCreatorButton.setName(".cadCreatorButton");
  }

  /**
   * Initialize the tool bar for use by an Environment panel.
   * Provides the Project Open and Save buttons.
   * @author George Booth
   */
  private void jbInitForEnvironment()
  {
    _openProjectButton = new JButton();
    _openProjectButton.setIcon(Image5DX.getImageIcon(Image5DX.OPEN_FILE));
    _openProjectButton.setToolTipText(StringLocalizer.keyToString("GUI_OPEN_TOOLBAR_TOOLTIP_KEY"));
    _openProjectButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        openProject();
      }
    });

    _saveProjectButton = new JButton();
    _saveProjectButton.setIcon(Image5DX.getImageIcon(Image5DX.SAVE_FILE));
    _saveProjectButton.setToolTipText(StringLocalizer.keyToString("GUI_SAVE_TOOLBAR_TOOLTIP_KEY"));
    _saveProjectButton.setEnabled(false);
    _saveProjectButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveProject();
      }
    });

    _logoutButton = new JButton();
    _logoutButton.setIcon(Image5DX.getImageIcon(Image5DX.LOGOUT_BUTTON));
    _logoutButton.setToolTipText(StringLocalizer.keyToString("GUI_LOGOUT_TOOLBAR_TOOLTIP_KEY"));
    _logoutButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        logout();
      }
    });

    _importNdfButton = new JButton();
    _importNdfButton.setIcon(Image5DX.getImageIcon(Image5DX.IMPORT_NDF));
    _importNdfButton.setToolTipText(StringLocalizer.keyToString("GUI_IMPORT_TOOLBAR_TOOLTIP_KEY"));
    _importNdfButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        importNdf();
      }
    });

    _closeProjectButton = new JButton();
    _closeProjectButton.setIcon(Image5DX.getImageIcon(Image5DX.CLOSE_FILE));
    _closeProjectButton.setToolTipText(StringLocalizer.keyToString("GUI_CLOSE_TOOLBAR_TOOLTIP_KEY"));
    _closeProjectButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        closeProject();
      }
    });

    _startupButton = new JButton();
    _startupButton.setIcon(Image5DX.getImageIcon(Image5DX.STARTUP_BUTTON));
    _startupButton.setToolTipText(StringLocalizer.keyToString("GUI_STARTUP_TOOLBAR_TOOLTIP_KEY"));
    _startupButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        startupMachine();
      }
    });
    
    //Kee Chin Seong - Shut down RMS 
    _shutdownRMSButton = new JButton();
    _shutdownRMSButton.setIcon(Image5DX.getImageIcon(Image5DX.SHUT_DOWN_REMOTE_MOTION_SERVER));
    _shutdownRMSButton.setToolTipText(StringLocalizer.keyToString("GUI_SHUTDOOWN_REMOTE_MOTION_SERVER_TOOLTIP_KEY"));
    _shutdownRMSButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        shutdownRemoteMotionServer();
      }
    });

    _shutdownButton = new JButton();
    _shutdownButton.setIcon(Image5DX.getImageIcon(Image5DX.SHUTDOWN_BUTTON));
    _shutdownButton.setToolTipText(StringLocalizer.keyToString("GUI_SHUTDOWN_TOOLBAR_TOOLTIP_KEY"));
    _shutdownButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        shutdownMachine();
      }
    });

    _shortTermXrayShutdownButton = new JButton();
    _shortTermXrayShutdownButton.setIcon(Image5DX.getImageIcon(Image5DX.SHORTTERM_XRAY_SHUTDOWN_BUTTON));
    _shortTermXrayShutdownButton.setToolTipText(StringLocalizer.keyToString("GUI_XRAY_AUTO_SHORT_TERM_SHUTDOWN_TOOLBAR_TOOLTIP_KEY"));
    _shortTermXrayShutdownButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        shorttermXrayShutdownMachine();
      }
    });

    _loadPanelButton = new JButton();
    _loadPanelButton.setIcon(Image5DX.getImageIcon(Image5DX.LOAD_PANEL_BUTTON));
    _loadPanelButton.setToolTipText(StringLocalizer.keyToString("GUI_LOAD_PANEL_TOOLBAR_TOOLTIP_KEY"));
    _loadPanelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        loadPanel();
      }
    });

    _unloadPanelButton = new JButton();
    _unloadPanelButton.setIcon(Image5DX.getImageIcon(Image5DX.UNLOAD_PANEL_BUTTON));
    _unloadPanelButton.setToolTipText(StringLocalizer.keyToString("GUI_UNLOAD_PANEL_TOOLBAR_TOOLTIP_KEY"));
    _unloadPanelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        unloadPanel();
      }
    });

    _generateTestCoverageReport = new JButton();
    _generateTestCoverageReport.setIcon(Image5DX.getImageIcon(Image5DX.TEST_COVERAGE_REPORT));
    _generateTestCoverageReport.setToolTipText(StringLocalizer.keyToString("GUI_GENERATE_TEST_COVERAGE_REPORT_KEY"));
    _generateTestCoverageReport.setEnabled(false);
    _generateTestCoverageReport.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        generateTestCoverageReport();
      }
    });

  /**
   *add land pattern checking button
   *@author khang shian, sham
   */
  _twoPinLandPatternChecking = new JButton();
  _twoPinLandPatternChecking.setIcon(Image5DX.getImageIcon(Image5DX.TWO_PIN_LAND_PATTERN_CHECKING));
  _twoPinLandPatternChecking.setToolTipText(StringLocalizer.keyToString("GUI_TWO_PIN_LAND_PATTERN_CHECKING_KEY"));
  _twoPinLandPatternChecking.setEnabled(false);
  _twoPinLandPatternChecking.addActionListener(new java.awt.event.ActionListener()
  {

  public void actionPerformed(ActionEvent e)
  {
    twoPinLandPatternChecking();
  }
  });
  
    // Wei Chin
    // re-load software config files
    _reloadConfigButton = new JButton();
    _reloadConfigButton.setIcon(Image5DX.getImageIcon(Image5DX.RUN_TESTS));
    _reloadConfigButton.setToolTipText(StringLocalizer.keyToString("GUI_REFRESH_SOFTWARE_CONFIG_TOOLBAR_TOOLTIP_KEY"));
    _reloadConfigButton.setEnabled(false);
    _reloadConfigButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        reloadConfigActionPerformed();
      }
    });
    
    //Ngie Xing, this function is only available for S2EX based systems
    try
    {
      if ( LicenseManager.isOfflineProgramming() == false)
      {           
        if (XrayTester.isS2EXEnabled())
        {
          _changeSystemMagnificationButton = new JButton();
          _changeSystemMagnificationButton.setIcon(Image5DX.getImageIcon(Image5DX.CHANGE_SYSTEM_MAGNIFICATION));
          _changeSystemMagnificationButton.setToolTipText(StringLocalizer.keyToString("GUI_CHANGE_SYSTEM_MAGNIFICATION_KEY"));
          _changeSystemMagnificationButton.setEnabled(true);
          _changeSystemMagnificationButton.addActionListener(new java.awt.event.ActionListener()
          {
            public void actionPerformed(ActionEvent e)
            {
              showChangeSystemMagnificationDialog();
            }
          });
        }        
      }
      else
      {
        _changeSystemMagnificationButton = new JButton();
        _changeSystemMagnificationButton.setIcon(Image5DX.getImageIcon(Image5DX.CHANGE_SYSTEM_MAGNIFICATION));
        _changeSystemMagnificationButton.setToolTipText(StringLocalizer.keyToString("GUI_CHANGE_SYSTEM_MAGNIFICATION_KEY"));
        _changeSystemMagnificationButton.setEnabled(true);
        _changeSystemMagnificationButton.addActionListener(new java.awt.event.ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            showChangeSystemMagnificationDialog();
          }
        });
      }
    }
    catch (final BusinessException e)
    {
      finishLicenseMonitor();
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  e.getMessage(),
                  StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                  true);
          MainMenuGui.getInstance().exitV810(false);
        }
      });
    }
    


    // Swee Yee
    if (Config.isInfoHandlerEnabled())
    {
      _errorHandlerButton = new JButton();
      _errorHandlerButton.setIcon(Image5DX.getImageIcon(Image5DX.INFO_HANDLER_FRAME_ICON_NAME));
      _errorHandlerButton.setToolTipText(StringLocalizer.infoHandlerKeyToString("GUI_INFO_HANDLER_BUTTON_TOOLTIP_TEXT_KEY"));
      _errorHandlerButton.setEnabled(true);
      _errorHandlerButton.addActionListener(new java.awt.event.ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          String[] exceptionParameters = new String[]
          {
          };
          InfoHandlerPanel.getInstance().executeCommand("DISPLAYALL-ERROR|" + "ALL", exceptionParameters);
        }
      });
    }

    _xrayStatusLabel.setIcon(_xrayPowerOffIcon);
    _anodeValueLabel.setText(" (kV : " + _currentAnodeValue + "  ");
    _cathodeValueLabel.setText("Ua : " + _currentCathodeValue + ") ");

    _xrayPowerMeterPanel.add(_xrayStatusLabel);
    _xrayPowerMeterPanel.add(_anodeValueLabel);
    _xrayPowerMeterPanel.add(_cathodeValueLabel);
    _xrayPowerMeterPanel.setToolTipText(StringLocalizer.keyToString("GUI_XRAY_POWER_KEY"));
    
    //make it invisible by default until it is initialized
    setXrayMeterVisibility(false);

    _startDefectPackager = new JButton();
    _startDefectPackager.setIcon(Image5DX.getImageIcon(Image5DX.START_DEFECT_PACKAGER));
    _startDefectPackager.setToolTipText(StringLocalizer.keyToString("GUI_START_DEFECT_PACKAGER_KEY"));
    _startDefectPackager.setEnabled(false);
    _startDefectPackager.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        startDefectPackager();
      }
    });
    
    _stopDefectPackager = new JButton();
    _stopDefectPackager.setIcon(Image5DX.getImageIcon(Image5DX.STOP_DEFECT_PACKAGER));
    _stopDefectPackager.setToolTipText(StringLocalizer.keyToString("GUI_STOP_DEFECT_PACKAGER_KEY"));
    _stopDefectPackager.setEnabled(false);
    _stopDefectPackager.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        stopDefectPackager();
      }
    });
    
    _horizontalBox = Box.createHorizontalGlue();

    addCommonButtons();

    Insets margins = new Insets(0,2,2,0);
    _openProjectButton.setMargin(margins);
    _saveProjectButton.setMargin(margins);
    _logoutButton.setMargin(margins);
    _importNdfButton.setMargin(margins);
    _closeProjectButton.setMargin(margins);
    _startupButton.setMargin(margins);
    _shutdownButton.setMargin(margins);
    _shutdownRMSButton.setMargin(margins);
    _shortTermXrayShutdownButton.setMargin(margins);
    _loadPanelButton.setMargin(margins);
    _unloadPanelButton.setMargin(margins);
    _generateTestCoverageReport.setMargin(margins);
    _twoPinLandPatternChecking.setMargin(margins);
    _reloadConfigButton.setMargin(margins);
    
    //Ngie Xing
    try
    {
      if ( LicenseManager.isOfflineProgramming() == false)
      {           
        if (XrayTester.isS2EXEnabled())
        {
          _changeSystemMagnificationButton.setMargin(margins);
          _changeSystemMagnificationButton.setName(".changeSystemMagnificationButton");
        }        
      }
      else
      {
        _changeSystemMagnificationButton.setMargin(margins);
        _changeSystemMagnificationButton.setName(".changeSystemMagnificationButton");
      }
    }
    catch (final BusinessException e)
    {
      finishLicenseMonitor();
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  e.getMessage(),
                  StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                  true);
          MainMenuGui.getInstance().exitV810(false);
        }
      });
    }

    if (Config.isInfoHandlerEnabled())
    {
      _errorHandlerButton.setMargin(margins);
    }
    
    _startDefectPackager.setMargin(margins);
    _stopDefectPackager.setMargin(margins);


    _openProjectButton.setName(".openProjectButton");
    _saveProjectButton.setName(".saveProjectButton");
    _logoutButton.setName(".logoutButton");
    _importNdfButton.setName(".importNdfButton");
    _closeProjectButton.setName(".closeProjectButton");
    _startupButton.setName(".startupButton");
    _shutdownButton.setName(".shutdownButton");
    _shutdownRMSButton.setName(".shutdownRemoteMotionServer");
    _shortTermXrayShutdownButton.setName(".shortTermXrayShutdownButton");
    _loadPanelButton.setName(".loadPanelButton");
    _unloadPanelButton.setName(".unloadPanelButton");
    _generateTestCoverageReport.setName(".generateTestCoverageReport");
    _twoPinLandPatternChecking.setName(".twoPinLandPatternChecking");
    _reloadConfigButton.setName(".reloadConfigButton");
    _startDefectPackager.setName(".startDefectPackager");
    _stopDefectPackager.setName(".stopDefectPackager");
  }
  
 /**
  * XCR-3686, Realtime monitoring X-ray Status
  * @author Yong Sheng Chuan
  */
  private void monitorXrayFlux()
  {
    _monitorThread.invokeLater(new Runnable()
    {
      public void run()
      {
        while (_monitorOn)
        {
          try
          {
            if (_xraySource.isStartupRequired() == false)
              updateXraySourceStatus();
            else
            {
              setXrayMeterVisibility(false);
              _monitorOn = false;
              return;
            }
            Thread.sleep(1000);
          }
          catch (InterruptedException ie)
          {
            // do nothing
          }
          catch (XrayTesterException xte)
          {
            //don't show anything, let the panel itself to do it
            _monitorOn = false;
            break;
          }
        }
      }
    });
  }

  /**
   * XCR-3686, Realtime monitoring X-ray Status
   * @author Yong Sheng Chuan
   */
  private void setXrayMeterVisibility(boolean visibility)
  {
    _xrayStatusLabel.setVisible(visibility);
    _anodeValueLabel.setVisible(visibility);
    _cathodeValueLabel.setVisible(visibility);
  }

  /**
   * XCR-3686, Realtime monitoring X-ray Status
   * @author Yong Sheng Chuan
   */
  private void updateXraySourceStatus() throws XrayTesterException
  {
    _currentAnodeValue = _xraySource.getAnodeVoltageInKiloVolts();
    _currentCathodeValue = _xraySource.getCathodeCurrentInMicroAmps();

    _anodeValueLabel.setText("   (kV : " + _currentAnodeValue + "  ");
    _cathodeValueLabel.setText("uA : " + _currentCathodeValue + ") ");

    //Highlight the reading when x ray exist
    if (_currentAnodeValue > 0 || _currentCathodeValue > 0)
    {
      _xrayStatusLabel.setIcon(_xrayPowerOnIcon);
      _xrayStatusLabel.setForeground(Color.RED);
      _anodeValueLabel.setForeground(Color.RED);
      _cathodeValueLabel.setForeground(Color.RED);
    }
    else
    {
      _xrayStatusLabel.setIcon(_xrayPowerOffIcon);
      _xrayStatusLabel.setForeground(Color.BLACK);
      _anodeValueLabel.setForeground(Color.BLACK);
      _cathodeValueLabel.setForeground(Color.BLACK);
    }
  }
  
  /**
   * @author Chin Seong
   */
  public void addDefectPackagerButtons()
  {
    this.add(_horizontalBox, null);
    
    this.add(_xrayPowerMeterPanel,null);
    this.add(_startDefectPackager, null);
    this.add(_stopDefectPackager, null);
  }
  
  /**
   * @author Chin Seong
   */
  public void removeDefectPackagerButtons()
  {
    this.remove(_horizontalBox);
    
    this.remove(_startDefectPackager);
    this.remove(_stopDefectPackager);
  }

  /**
   * add environment buttons to the toolbar
   * @author George Booth
   */
  private void addEnvironmentButtons()
  {
    setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
    add(Box.createRigidArea(new Dimension(0,30)));
    add(_mainButton, null);
    add(Box.createRigidArea(new Dimension(0,30)));
    add(_testDevButton, null);
    add(Box.createRigidArea(new Dimension(0,30)));
    // only add testexec if hardware is available
    if (_xRayTester.isHardwareAvailable())
    {
      add(_testExecButton, null);
      add(Box.createRigidArea(new Dimension(0,30)));
    }

    add(_configureButton, null);
    add(Box.createRigidArea(new Dimension(0, 30)));

    try
    {
      if (LicenseManager.isV810() &&
          Config.getInstance().getBooleanValue(SoftwareConfigEnum.ONLINE_WORKSTATION) == true)
      {
        add(_servicesButton, null);
        add(Box.createRigidArea(new Dimension(0, 30)));
      }
    }
    catch (BusinessException ex)
    {
      //Do nothing
    }
  

    // only add the VirtualLive button if so set in the software.config file
    if(_mainUI.isVirtualLiveModeEnabled())
    {
      add(_virtualLiveButton, null);
      add(Box.createRigidArea(new Dimension(0,30)));
    }
    
    //chin seong - enable cad creator controlled by config
    if(Config.isCadCreatorEnabled() == true)
    {
        add(_cadCreatorButton, null);
        add(Box.createRigidArea(new Dimension(0,30)));
    }
  }

  /**
   * add Common buttons to the toolbar
   * @author Chong, Wei Chin
   */
  private void addCommonButtons()
  {
    this.add(_logoutButton, null);
    this.addSeparator();

    this.add(_importNdfButton, null);
    this.add(_openProjectButton, null);
    this.add(_saveProjectButton, null);
    this.add(_closeProjectButton, null);
    this.addSeparator();

    this.add(_startupButton, null);
    this.add(_shutdownButton, null);
    this.add(_shutdownRMSButton, null);
    this.add(_shortTermXrayShutdownButton, null);
    this.add(_loadPanelButton, null);
    this.add(_unloadPanelButton, null);
    this.addSeparator();

    this.add(_generateTestCoverageReport, null);
    this.add(_twoPinLandPatternChecking, null);
    this.add(_reloadConfigButton, null);
    this.addSeparator();
    
    try
    {
      if ( LicenseManager.isOfflineProgramming() == false)
      {           
        if (XrayTester.isS2EXEnabled())
        {
          this.add(_changeSystemMagnificationButton, null);
          this.addSeparator();
        }
        _shutdownRMSButton.setEnabled(true);
      }
      else
      {
        this.add(_changeSystemMagnificationButton, null);
        this.addSeparator();
        if (XrayTester.getSystemType().equals(SystemTypeEnum.S2EX) && Project.isCurrentProjectLoaded())
        {
          _changeSystemMagnificationButton.setEnabled(true);
        }
        else
        {
          _changeSystemMagnificationButton.setEnabled(false);
        }
        _shutdownRMSButton.setEnabled(false);
      }
    }
    catch (final BusinessException e)
    {
      MainMenuGui.finishLicenseMonitor();
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  e.getMessage(),
                  StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                  true);
          MainMenuGui.getInstance().exitV810(false);
        }
      });
    }
        
    if (Config.isInfoHandlerEnabled())
    {
      this.add(_errorHandlerButton, null);
    }
    this.addSeparator();

    addDefectPackagerButtons();
  }

  /**
   * enables all the tool bar environment buttons
   * @author George Booth
   */
  private void enableAllEnvButtons()
  {
    _mainButton.setEnabled(true);
    _testDevButton.setEnabled(true);
    _testExecButton.setEnabled(true);
    _configureButton.setEnabled(true);
    _servicesButton.setEnabled(true);
    _virtualLiveButton.setEnabled(true);
    _cadCreatorButton.setEnabled(true);

    _mainButton.setBackground(_buttonBackground);
    _testDevButton.setBackground(_buttonBackground);
    _testExecButton.setBackground(_buttonBackground);
    _configureButton.setBackground(_buttonBackground);
    _servicesButton.setBackground(_buttonBackground);
    _virtualLiveButton.setBackground(_buttonBackground);
    _cadCreatorButton.setBackground(_buttonBackground);
  }

  /**
   * enables/disables the save project button
   * @author George Booth
   * @Edited By Kee Chin Seong - Defocus the button so that the 
   *                             space bar will not affecting the button
   */
  public void enableSaveProjectButton(boolean enabled)
  {
    _saveProjectButton.setEnabled(enabled);
    _saveProjectButton.setFocusable(false);
  }

  /**
   * @author :: Kee Chin Seong.
   * To enable the button for generate test coverage report
   * @Edited By Kee Chin Seong - Defocus the button so that the 
   *                             space bar will not affecting the button
   */
  public void enableGenerateTestCoverageReportButton(boolean enabled)
  {
    _generateTestCoverageReport.setEnabled(enabled);
    _generateTestCoverageReport.setFocusable(false);
  }
  
  /*
   * Enable/disable change system magnification button
   * @author Ngie Xing
   * @Edited By Kee Chin Seong - Defocus the button so that the 
   *                             space bar will not affecting the button
   */
  public void enableChangeSystemMagnificationButton(boolean enabled)
  {
    _changeSystemMagnificationButton.setEnabled(enabled);
    _changeSystemMagnificationButton.setFocusable(false);
  }
  
  /*
   * @author Kee, Chin Seong
   * To enable start defect packager button
   & @Edited By Kee Chin Seong - Defocus the button so that the 
   *                             space bar will not affecting the button
   */
  public void enableStartDefectPackagerButton(boolean enabled)
  {
    _startDefectPackager.setEnabled(enabled);
    _startDefectPackager.setFocusable(false);
  }
  
  /*
   * @author Kee, Chin Seong
   * To enable start defect packager button
   * @Edited By Kee Chin Seong - Defocus the button so that the 
   *                             space bar will not affecting the button
   */
  public void enableStopDefectPackagerButton(boolean enabled)
  {
    _stopDefectPackager.setEnabled(enabled);
    _stopDefectPackager.setFocusable(false);
  }

  /**
 * enables/disables the land pattern checking button
 * @author khang shian,sham
 * @Edited By Kee Chin Seong - Defocus the button so that the 
 *                             space bar will not affecting the button
 */
public void enableTwoPinLandPatternCheckingButton(boolean enabled)
{
  _twoPinLandPatternChecking.setEnabled(enabled);
  _twoPinLandPatternChecking.setFocusable(false);
}

/**
   * enables/disables the close project button
   * @author Chong, Wei Chin
   * @Edited By Kee Chin Seong - Defocus the button so that the 
   *                             space bar will not affecting the button
   */
  public void enableCloseProjectButton(boolean enabled)
  {
    _closeProjectButton.setEnabled(enabled);
    _closeProjectButton.setFocusable(false);
  }

  /**
   * enables/disables the close project button
   * @author Chong, Wei Chin
   * @Edited By Kee Chin Seong - Defocus the button so that the 
   *                             space bar will not affecting the button
   */
  public void enableStartUpButton(boolean enabled)
  {
    _startupButton.setEnabled(enabled);
    _startupButton.setFocusable(false);
  }

  /**
   * enables/disables the close project button
   * @author Chong, Wei Chin
   * @Edited By Kee Chin Seong - Defocus the button so that the 
   *                             space bar will not affecting the button
   */
  public void enableShutdownButton(boolean enabled)
  {
    _shutdownButton.setEnabled(enabled);
    _shutdownButton.setFocusable(false);
  }

  /**
   * enables/disables the close project button
   * @author Chong, Wei Chin
   * @Edited By Kee Chin Seong - Defocus the button so that the 
   *                             space bar will not affecting the button
   */
  public void enableShortTermXrayShutdownButton(boolean enabled)
  {
    _shortTermXrayShutdownButton.setEnabled(enabled);
    _shortTermXrayShutdownButton.setFocusable(false);
  }

  /**
   * enables/disables the close project button
   * @author Chong, Wei Chin
   * @Edited By Kee Chin Seong - Defocus the button so that the 
   *                             space bar will not affecting the button
   */
  public void enableLoadPanelButton(boolean enabled)
  {
    _loadPanelButton.setEnabled(enabled);
    _loadPanelButton.setFocusable(false);
  }

  /**
   * enables/disables the close project button
   * @author Chong, Wei Chin
   * @Edited By Kee Chin Seong - Defocus the button so that the 
   *                             space bar will not affecting the button
   */
  public void enableUnloadPanelButton(boolean enabled)
  {
    _unloadPanelButton.setEnabled(enabled);
    _unloadPanelButton.setFocusable(false);
  }

  /**
   * @param enabled
   * 
   * @author Chong, Wei Chin
   */
  public void enableCommonButtons(boolean enabled)
  {
    _importNdfButton.setEnabled(enabled);
    _openProjectButton.setEnabled(enabled);
    _saveProjectButton.setEnabled(enabled);
    _closeProjectButton.setEnabled(enabled);
    _startupButton.setEnabled(enabled);
    _shutdownButton.setEnabled(enabled);
    _shortTermXrayShutdownButton.setEnabled(enabled);
    _loadPanelButton.setEnabled(enabled);
    _unloadPanelButton.setEnabled(enabled);
    _reloadConfigButton.setEnabled(enabled);
  }

  /**
   * Switches to the AXI Home Environment if current environment is ready to finish
   * @author George Booth
   */
  public void switchToAXIHome()
  {
    if (_mainUI.switchToAXIHome())
    {
      enableAllEnvButtons();
      _mainButton.setEnabled(false);
      _mainButton.setBackground(_activeButtonBackground);
      _mainUI.getMainMenuBar().disableHomeMenuItem();
    }
  }

  /**
   * Switches to the Test Development Environment if current environment is ready to finish
   * @author George Booth
   */
  void switchToTestDev()
  {
    if (_mainUI.switchToTestDev())
    {
      enableAllEnvButtons();
      _testDevButton.setEnabled(false);
      _testDevButton.setBackground(_activeButtonBackground);
      _mainUI.getMainMenuBar().disableTestDevMenuItem();
    }
  }

  /**
   * Switches to the Test Exec (Operator) Environment if current environment is ready to finish
   * @author George Booth
   */
  void switchToTestExec()
  {
    if (_mainUI.switchToTestExec())
    {
      enableAllEnvButtons();
      _testExecButton.setEnabled(false);
      _testExecButton.setBackground(_activeButtonBackground);
      _mainUI.getMainMenuBar().disableTestExecMenuItem();
    }
  }

  /**
   * Switches to the Configure Environment if current environment is ready to finish
   * @author George Booth
   */
  void switchToConfigure()
  {
    if (_mainUI.switchToConfigure())
    {
      enableAllEnvButtons();
      _configureButton.setEnabled(false);
      _configureButton.setBackground(_activeButtonBackground);
      _mainUI.getMainMenuBar().disableConfigureMenuItem();
    }
  }

  /**
   * Switches to the Service Environment if current environment is ready to finish
   * @author George Booth
   */
  void switchToService()
  {
    if (_mainUI.switchToService())
    {
      enableAllEnvButtons();
      _servicesButton.setEnabled(false);
      _servicesButton.setBackground(_activeButtonBackground);
      _mainUI.getMainMenuBar().disableServiceMenuItem();
    }
  }

  /**
   * Switches to the Virtual Live Environment if current environment is ready to finish
   * @author Scott Richardson
   */
  void switchToVirtualLive()
  {
    if (_mainUI.switchToVirtualLive())
    {
      enableAllEnvButtons();
      _virtualLiveButton.setEnabled(false);
      _virtualLiveButton.setBackground(_activeButtonBackground);
      _mainUI.getMainMenuBar().disableVirtualLiveMenuItem();
    }
  }
  
  /*
   * @author Kee Chin Seong - Cad Creator
   */
  void switchToCadCreator()
  {
    if( _mainUI.switchToCadCreator())
    {
      enableAllEnvButtons();
      _cadCreatorButton.setEnabled(false);
      _cadCreatorButton.setBackground(_activeButtonBackground);
      _mainUI.getMainMenuBar().disableCadCreatorMenuItem();
    }
  }

  /**
   * Opens a new project
   * @author George Booth
   */
  void openProject()
  {
    _mainUI.getMainMenuBar().projectOpen();
  }

  /**
   * Saves the current project
   * @author George Booth
   */
  void saveProject()
  {
    _mainUI.getMainMenuBar().saveProject();
  }

  /**
   * @author Kee, Chin Seong
   * To Generated loaded test coverage report
   */
  void generateTestCoverageReport()
  {
    _mainUI.getMainMenuBar().generateTestCoverageReport();
  }
  
  /**
   * @author Kee, Chin Seong
   * To Start Defect Packager
  */
  void startDefectPackager()
  {
    _mainUI.getMainMenuBar().startDefectPackager();
  }
  
  /**
   * @author Kee, Chin Seong
   *  To Stop Defect Packager
  */
  void stopDefectPackager()
  {
    _mainUI.getMainMenuBar().stopDefectPackager();
  }

  /**
 * two pin land pattern checking
 * @author khang shian,Sham
 */
void twoPinLandPatternChecking()
{
  _mainUI.getMainMenuBar().generateLandPatternOverlappingList();
}

/**
   * Logout from the system
   * @author Chong, Wei Chin
   */
  void logout()
  {
    _mainUI.logout();
  }

  /**
   * Import NDFs
   * @author Chong, Wei Chin
   */
  void importNdf()
  {
    _mainUI.getMainMenuBar().importProjectMenuItem_actionPerformed();
  }

  /**
   * Close Project
   * @author Chong, Wei Chin
   */
  void closeProject()
  {
    _mainUI.getMainMenuBar().projectClose(false);
  }

  /**
   * Startup Machine
   * @author Chong, Wei Chin
   */
  void startupMachine()
  {
    _mainUI.getMainMenuBar().startup();
  }

  /**
   * Shutdown Machine
   * @author Chong, Wei Chin
   */
  void shutdownMachine()
  {
    try
    {
      _mainUI.stopXrayTester();
    }
    catch (XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
    }
  }
  
  /**
   * Shutdown RMS Machine
   * @author Kee Chin Seong
   */
  void shutdownRemoteMotionServer()
  {
    try
    {
      _mainUI.shutdownRemoteMotionServer();
    }
    catch (XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
    }
  }

  /**
   * ShortTerm Shutdown Xray
   * @author Chong, Wei Chin
   */
  void shorttermXrayShutdownMachine()
  {
    _mainUI.getMainMenuBar().xRayAutoShortTermShutdownMenuItem_actionPerformed();
  }

  /**
   * Load Panel
   * @author Chong, Wei Chin
   */
  void loadPanel()
  {
    try
    {
      _mainUI.loadPanel();
    }
    catch (XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
    }
  }

  /**
   * Unload Panel
   * @author Chong, Wei Chin
   */
  void unloadPanel()
  {
    try
    {
      _mainUI.unloadPanel();
    }
    catch(XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
    }
  }

// ------- MainUIToolBar methods

  /**
   * Initiate a menu update. All chages are tracked by returned id
   * @return int the id number of this request
   * @author George Booth
   */
  public int startToolBarAdd()
  {
    int id = ++_currentRequest;
    ToolBarChanges newChanges = new ToolBarChanges();
    _toolBarChanges.add(newChanges);
    return id;
  }

  /**
   * Add a new component to the end of the tool
   * @author George Booth
   */
  public void addToolBarComponent(int id, Component comp)
  {
    Assert.expect(id == _currentRequest, "id = " + id + " currentRequest = " + _currentRequest);
    Assert.expect(comp != null);

    ToolBarChanges changes = _toolBarChanges.get(_currentRequest);
    changes.addComponent(comp);
    this.add(comp);
  }

  /**
   * Add a new component to the menu bar at location = pos (0 based)
   * @author George Booth
   */
  public void addToolBarComponent(int id, int pos, Component comp)
  {
    Assert.expect(id == _currentRequest, "id = " + id + " currentRequest = " + _currentRequest);
    Assert.expect(pos > -1);
    Assert.expect(comp != null);

    ToolBarChanges changes = _toolBarChanges.get(_currentRequest);
    changes.addComponent(comp);
    this.add(comp, pos);
  }

  /**
   * Add a separator to the toolbar at location "pos" (0 based)
   * @author George Booth
   */
  public void addToolBarSeparator(int id, int pos)
  {
    Assert.expect(id == _currentRequest, "id = " + id + " currentRequest = " + _currentRequest);
    Assert.expect(pos > -1);

    // save it
    ToolBarChanges changes = _toolBarChanges.get(_currentRequest);
    JSeparator separator = new JSeparator();
    changes.addSeparator(separator);
    this.add(separator, pos);
  }

  /**
   * removes toolbar components added by requester
   * @author George Booth
   */
  public void undoToolBarChanges(int id)
  {
    Assert.expect(id == _currentRequest, "id = " + id + " currentRequest = " + _currentRequest);

    ToolBarChanges changes = _toolBarChanges.remove(_currentRequest);
    // remove separators
    Iterator<JSeparator> separators = changes.getSeparators();
    while (separators != null && separators.hasNext())
    {
      JSeparator separator = separators.next();
      this.remove(separator);
    }
    // remove components
    Iterator<Component> components = changes.getComponents();
    while (components != null && components.hasNext())
    {
      Component comp = components.next();
      this.remove(comp);
    }
    
    //Kee chin Seong - Memory leak, must unpopulate and free it
    changes.unpopulateAll();
    changes = null;
    
    this.revalidate();
    this.repaint();
    //restore requester ID
    --_currentRequest;
  }
  
  /**
   * @author sham
   */
  public void disableLoadUnloadButton()
  {
    if(_loadPanelButton != null)
    {
      _loadPanelButton.setEnabled(false);
    }
    if(_unloadPanelButton != null)
    {
      _unloadPanelButton.setEnabled(false);
    }
  }
  
  /**
   * @author Ngie Xing
   */
  public void enableChangeSystemMagnificationButton()
  {
    if(_changeSystemMagnificationButton != null && XrayTester.getSystemType().equals(SystemTypeEnum.S2EX))
    {
      _changeSystemMagnificationButton.setEnabled(true);
    }
  }
  
  /**
   * @author Ngie Xing
   */
  public void disableChangeSystemMagnificationButton()
  {
    if(_changeSystemMagnificationButton != null)
    {
      _changeSystemMagnificationButton.setEnabled(false);
    }
  }
  
  /**
   * @author Wei Chin
   */
  public void reloadConfigActionPerformed()
  {
    try
    {
      Config.getInstance().load(FileName.getSoftwareConfigFullPath());
    }
    catch (com.axi.v810.datastore.DatastoreException ex)
    {
      // do nothing
      System.out.println("reloadConfig Exception :" + ex.getLocalizedMessage());
    }
  }
  
  /*
   * @author Ngie Xing
   */
  public void showChangeSystemMagnificationDialog()
  {
    _mainUI.getMainMenuBar().changeSystemMagnificationMenuItem_actionPerformed();
  }
    
  /**
   * check is last visit page config environment
   * @return 
   */
  public boolean isLastVisitPageConfigEnv()
  {
    return _lastVisitPanel.equals(_CONFIG_ENV);
  }
  /**
   * this function is specially created to bring back user to last visit page if current page and last visit page is different
   * sheng chuan
   */
  public void recoverToLastVisitPage()
  {
    if(_lastVisitPanel.equals(_TEST_DEV_ENV))
      switchToTestDev();
    else if(_lastVisitPanel.equals(_TEST_EXEC_ENV))
      switchToTestExec();
    else if(_lastVisitPanel.equals(_CONFIG_ENV))
      switchToConfigure();
    else if(_lastVisitPanel.equals(_SERVICE_ENV))
      switchToService();
    else if(_lastVisitPanel.equals(_VIRTUAL_LIVE_ENV))
      switchToVirtualLive();
    else
      switchToAXIHome();
  }
  
  /**
   * @author Yong Sheng CHuan
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof HardwareObservable)
          handleHardwareObserver(object);
      }
    });
  }
  
  /**
   * XCR-3686, Realtime monitoring X-ray Status
   * @author Yong Sheng Chuan
   */
  private void handleHardwareObserver(final Object arg)
  {
    if (arg instanceof HardwareEvent)
    {
      HardwareEvent event = (HardwareEvent) arg;
      HardwareEventEnum eventEnum = event.getEventEnum();

      if (event.isStart()== false && (eventEnum instanceof XraySourceEventEnum))
      {
        if (_monitorOn == false)
        {
          setXrayMeterVisibility(true);
          _monitorOn = true;
          monitorXrayFlux();
        }
      }
    }
  }
}

