package com.axi.v810.gui.mainMenu;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.util.*;

/**
 * The AbstractEnvironmentPanel class provides the framework
 * for Genesis environment panels. This class is required to
 * manage Main UI navigation.
 * <P>
 * The instantiating panel needs to implement the following to satisfy
 * the EnvironmentInterface:<BR><BR>
 * public void start()<BR>
 * public boolean isReadyToFinish()<BR>
 * public void finish()<BR>
 * @author George Booth
 */

public abstract class AbstractTaskPanel extends JPanel implements TaskPanelInt
{
  protected MainMenuGui _mainUI = null;
  protected AbstractEnvironmentPanel _envPanel = null;

  protected Border _taskPanelBorder;
  protected BorderLayout _taskPanelLayout = null;

  protected boolean _active = false;
  protected MainUIMenuBar _menuBar = null;
  protected int _menuRequesterID = -1;
  protected MainUIToolBar _envToolBar = null;
  protected int _envToolBarRequesterID = -1;

  /**
   * ONLY for use with JBuilder Designer
   * @author Andy Mechtenberg
   */
  public AbstractTaskPanel()
  {
    Assert.expect(java.beans.Beans.isDesignTime());
    // do nothing.  This is ONLY for use with the JBuilder Designer
  }

  /**
   * @author George Booth
   */
  public AbstractTaskPanel(AbstractEnvironmentPanel envPanel)
  {
    Assert.expect(envPanel != null);
    _envPanel = envPanel;
    _mainUI = _envPanel.getMainUI();
    jbInitialize();
  }

  /**
   * Initialize panel (renamed so it is not overridden)
   */
  private void jbInitialize()
  {
    _taskPanelBorder = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,
                                                                                          Color.white,
                                                                                          new Color(182, 182, 182),
                                                                                          new Color(62, 62, 62)
                                                                                          ,new Color(89, 89, 89)),
                                                          BorderFactory.createEmptyBorder(0, 0, 0, 0));
    _taskPanelLayout = new BorderLayout();
  }

  /**
   * Set up variables so the task panel can add menus and tools bars
   * @author George Booth
   */
  public void startMenusAndToolBar()
  {
    // get menu and toolbar references and requester ID
    _menuBar = _envPanel.getEnvMenuBar();
    _menuRequesterID = _menuBar.startMenuAdd();

    _envToolBar = _envPanel.getEnvToolBar();
    _envToolBarRequesterID = _envToolBar.startToolBarAdd();
  }

  /**
   * @author George Booth
   */
  public void finishMenusAndToolBar()
  {
    // remove custom menus and toolbar components
    _menuBar.undoMenuChanges(_menuRequesterID);
    _envToolBar.undoToolBarChanges(_envToolBarRequesterID);
  }

}
