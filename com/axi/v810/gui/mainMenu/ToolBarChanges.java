package com.axi.v810.gui.mainMenu;

import java.awt.*;
import java.util.*;

import javax.swing.*;

/**
 * This class supports toolbar changes in MainUIToolBar by defining lists
 * of buttons and separators.
 *
 * @author George Booth
 */

public class ToolBarChanges
{
  private Vector<Component> _components = null;
  private Vector<JSeparator> _separators = null;

  public ToolBarChanges()
  {
    _components = new Vector<Component>();
    _separators = new Vector<JSeparator>();
  }
  
  /*
   * Kee Chin Seong
   * - Free All Memory in vector
   */
  public void unpopulateAll()
  {
    _components.clear();
    _separators.clear();
    
    _components = null;
    _separators = null;
  }

  /**
   * @author George Booth
   */
  public void addComponent(Component component)
  {
    _components.add(component);
  }

  /**
   * @author George Booth
   */
  public Iterator<Component> getComponents()
  {
    return _components.iterator();
  }

  /**
   * @author George Booth
   */
  public void addSeparator(JSeparator separator)
  {
    _separators.add(separator);
  }

  /**
   * @author George Booth
   */
  public Iterator<JSeparator> getSeparators()
  {
    return _separators.iterator();
  }

}
