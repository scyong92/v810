package com.axi.v810.gui.mainMenu;

import com.axi.v810.gui.*;

/**
 *
 * <p>Title: EnvironmentInterface</p>
 *
 * <p>Description: This interface defines the methods required to be
 * implemened by a panel supporting a Genesis environment. An environment
 * is a collection of tasks used to perform a related set of operations. </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 * @version 1.0
 */

public interface EnvironmentInt
{
  /**
   * This is called when a task navigation button is clicked.  The
   * instantiating class must implement a method to stop the current
   * task and start a new task matching the buttonName.
   * @param buttonName String
   * @author George Booth
   */
  abstract void navButtonClicked(String buttonName);

  /**
   * This is called by the undo/redo mechanism
   * @author Andy Mechtenberg
   */
  abstract void switchToScreen(TaskPanelScreenEnum newScreen);

  /**
   * The environment is now on top, ready to run and should perform
   * startup operations such as registering listeners, adding context
   * menus and tool bars and starting its default task panel.
   * @author George Booth
   */
  public void start();

  /**
   * The user has requested an environment change. The environment should
   * determine if it is ready to terminate.  Is the current task ready to
   * terminate? Does data need to be saved?  Are there processes on other
   * threads that are not complete?
   * @return true if the environment can be terminated
   * @author George Booth
   */
  public boolean isReadyToFinish();

  /**
   * The environment is about to be terminated and should perform any cleanup needed
   * such as terminating the current task panel, removing context menus and tool bars
   * and unregistering listeners.
   * @author George Booth
   */
  public void finish();

}
