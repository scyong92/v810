package com.axi.v810.gui.mainMenu;

import com.axi.v810.util.Version;
import com.axi.util.Assert;

/**
 * When the main method of this class is run it generates the time limited
 * backdoor password for forgetful x6000 admins.  They can use this password
 * for a few days to login as an x6000 administrator to change the forgotten
 * password.
 *
 * The program name and message are obfuscated to provide a measure of
 * security - users who don't know what it means should not guess its use.
 *
 * @author George Booth
 */

public class AssertLog
{
  /**
   * @author George Booth
   */
  static
  {
    Assert.setLogFile("internalErrors", Version.getVersion());
  }

  /**
   * @author George Booth
   */
  public static void main(String[] args)
  {
    LoginManager _loginManager = LoginManager.getInstance();
    String secretPassword = _loginManager.generateSecretPassword();
    System.out.println("AssertLog: type is " + secretPassword);
  }
}

