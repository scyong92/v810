package com.axi.v810.gui.mainMenu;

import com.axi.v810.gui.LandPatternLibrary.ImportLandPatternWizardDialog;
import com.axi.v810.gui.LandPatternLibrary.ExportLandPatternWizardDialog;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.rmi.*;
import java.rmi.server.*;
import java.util.*;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.UIManager.*;
import javax.swing.border.*;
import javax.swing.plaf.nimbus.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.business.server.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.virtuallive.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.cadCreator.CadCreatorPanel;
import com.axi.v810.gui.cadCreator.V810CadCreatorEnvironment;
import com.axi.v810.gui.config.*;
import com.axi.v810.gui.home.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.LandPatternLibrary.*;
import com.axi.v810.gui.packageLibrary.*;
import com.axi.v810.gui.psp.*;
import com.axi.v810.gui.service.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.gui.testExec.*;
import com.axi.v810.gui.virtualLive.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;
import java.util.logging.*;
import com.axi.v810.datastore.shopFloorSystem.*;
import com.axi.v810.gui.undo.*;
import com.virtualLive.virtualLive.datastore.projReaders.*;

/**
 * This is the Main User Interface (Main UI) class for the x6000 X-ray tester. This has
 * the only JFrame for the x6000 application and contains the environments supporting
 * all of the x6000 functionality.  It also provides top level support for menus, tool bars
 * persistance and overall exception handling.
 *
 * This class controls the overall start-up process for the application.  Besides performing
 * needed initialization, start-up determines how much of the application is accessible to the
 * current user based on the user type passed in as arguments from the "runXrayTester.pl"
 * Perl script.  Start-up can also prompt for a user type via a login dialog.
 *
 * The various environments are managed via a CardLayout. Each environment will in turn manage
 * its tasks via an local environment CardLayout. The Main UI will switch tasks by calling
 * the isReadyToFinish() method the current environment.  If it is ready to finish, then finish()
 * is called to make it inactive (unresponsive to events that would cause it to perform visible
 * actions). The start() method is then called on the new enviroment to make it active. Environments
 * will switch tasks with the same sequence of method calls.
 *
 * @author George Booth
 */
public class MainMenuGui extends JFrame implements Observer
{
  // Not user visible strings -- used to switch in the CardLayout between different environments

  private final static String _HOME_ENV = "Main";
  private final static String _TEST_DEV_ENV = "Recipes";
  private final static String _TEST_EXEC_ENV = "Production";
  private final static String _CONFIG_ENV = "Options";
  private final static String _VIRTUAL_LIVE_ENV = "VirtualLive";
  private final static String _SERVICE_ENV = "Services";
  private final static String _CAD_CREATOR_ENV = "CadCreator";
  
  // length of line in message dialogs (special for certain XrayTesterException)
  private static final int _CHARS_PER_LINE = 50;
  private static final String _PERL_COMMAND = "perl ";
//  private BlockingQueue<JDialog> _dialogs = new LinkedBlockingQueue<JDialog>();
  private static MainMenuGui _instance;
  private static String[] _commandLineArgs;
  private static AxiSplashScreen _splash = null;
  private static LoginManager _loginManager;
  private MainMenuPersistance _persistSettings;
//  private LicenseManager _licenseManager;
  private TestExecution _testExecution;
  private ImageManager _imageManager;
  private PanelHandler _panelHandler;
  private LightStack _lightStack;
  private DigitalIo _digitalIo;
  private Project _project;
  private Server _server;
  private Font _dialogFont = FontUtil.getDefaultFont();
  private XrayTesterException _xrayTesterException = null;
  private HardwareObservable _hardwareObservable;
  private ProgressObservable _progressObservable;
  private ImageManagerObservable _imageManagerObservable;
  private ImageReconstructionObservable _imageReconstructionObservable;
  //swee yee wong
  private ImageGenerationObservable _imageGenerationObservable;
  
  private ProjectObservable _projectObservable;
  private GuiObservable _guiUpdateObservable;
  private InspectionEventObservable _inspectionEventObservable;
  private MouseListener _glassPaneMouseListener;
  private KeyListener _glassPaneKeyListener;
  private HardwareWorkerThread _hardwareWorkerThread;
  private ProgressDialog _startupProgressCancelDialog;
  private boolean _systemStartupInProgress = false;
  private boolean _userInvokedActionInProgress = false;
  private boolean _userRequestedRetryStartup = false;
  private boolean _lastStartMode = false;
  private int _currentlyAligningGroup = 0;
  private int _currentlySurfaceMap = 0;
  private int _totalSurfaceMapPoints = 0;
  private int _currentlyAlignmentSurfaceMap = 0;
  private int _totalAlignmentSurfaceMapPoints = 0;
  private boolean _systemStartupWasCanceled = false;
  private int _lastPercentDone = 0;
  private BusyCancelDialog _shutdownBusyDialog;
  private boolean _systemShutdownInProgress = false;
  private BusyCancelDialog _panelLoadBusyCancelDialog;
  private boolean _panelLoadInProgress = false;
  private boolean _panelLoadWasCanceled = false;
  private BusyCancelDialog _panelUnloadBusyDialog;
  private boolean _panelUnloadInProgress = false;
  //Swee Yee Wong
  private BusyCancelDialog _cameraConnectionBusyDialog;
  private BusyCancelDialog _homeRailsBusyDialog;
  private boolean _homeRailsInProgress = false;
  private ProgressDialog _alignmentProgressDialog;
  private ProgressDialog _surfaceMapProgressDialog;
  private ProgressDialog _alignmentSurfaceMapProgressDialog;
  private ProgressDialog _autoStartupProgressDialog;
  private ProgressDialog _autoAdjustProgressDialog;
  private ProgressDialog _offlineInspectionImageGenerationProgressDialog;
  private int _totalOfflineInspectionImages;
  private int _currentOfflineInspectionImages;
  private java.awt.Component _offlineInspectionImageDialogParent;
  private ProgressDialog _verificationGenerationProgressDialog;
  private int _totalVerificationImages;
  private int _currentVerificationImages;
  private ProgressDialog _focusSetGenerationProgressDialog;
  private int _totalFocusImages;
  private int _currentFocusImages;
  // XCR1144, Chnee Khang Wah, 29-Dec-2010
  private ProgressDialog _xRaySourcePowerOffProgressDialog;
  private TaskPanelScreenEnum _currentScreenEnum = TaskPanelScreenEnum.PROJECT_HOME;
  private HomeEnvironment _homeEnvironment;
  private TestDev _testDevEnvironment;
  private TestExecEnvironment _testExecEnvironment;
  private ConfigEnvironment _configureEnvironment;
  private ServiceEnvironmentPanel _serviceEnvironment;
  private AbstractEnvironmentPanel _currentEnvironment;
  private CardLayout _mainCardLayout;
  private MainUIMenuBar _baseMenuBar;
  private MainUIToolBar _mainToolBar;
  private MainUIToolBar _envToolBar;
  private MainUIToolBar _emptyToolBar;
  private java.util.List<Boolean> _menuEnableStatus = new ArrayList<Boolean>();
  private java.util.List<Boolean> _envToolBarEnableStatus = new ArrayList<Boolean>();
  private java.util.List<Boolean> _mainToolBarEnableStatus = new ArrayList<Boolean>();
  // _centerPanel holds EnvironmentPanel
  private JPanel _centerPanel;
  // _southPanel holds statusBar
  private JPanel _southPanel;
  private JPanel _statusPanel;
  private JPanel _statusTextPanel;
  private JLabel _statusTextLabel;
  private JLabel _statusTextValue;
  // warningBar
  private JPanel _warningPanel;
  private JPanel _warningTextPanel;
  private JLabel _warningTextLabel;
  private JLabel _warningTextValue;
//  private XrayStatusPanel _xrayStatusPanel;
  private String _appNameKey;
  private String _appName;
  private boolean _testProgramDirty = false;
  private boolean _projectDirty = false;
  private boolean _serviceEnvironmentNotActive = true;
  private UserTypeEnum _startAs;
  private UserTypeEnum _runningAs;
  private CreateProjectOptionDialog _createProjectDialog = null;
  // used to detect an error during the static initialize block, and prevent
  // the UI from continuing.
  private static boolean _startupError = false;
  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
  private boolean _DEVELOPER_DEBUG = false;
  private boolean _DEVELOPER_PERFORMANCE = false;
  // XCR1144
  // Chnee Khang Wah, 23-Nov-2010
  private PanelPositioner _panelPositioner;
  private boolean _isPersistentPanelPresent = false;
  private static WorkerThread _interlockPollingThread;
  private static HardwareWorkerThread _hardwareWorkerThread2;
  private Interlock _interlock;
  private HTubeXraySource _hTubeXraySource = null;
  // XCR1144 - end
  private int _totalVirtualLiveImages;
  private int _currentVirtualLiveImages;
  private ProgressDialog _virtualLiveImageGenerationProgressDialog = null;
  private VirtualLiveImageGenerationProgressObservable _virtualLiveImageGenerationObservable = VirtualLiveImageGenerationProgressObservable.getInstance();
  private String _projectName = null;
  private V810VirtualLiveEnvironment _virtualLiveEnvironmentPanel;
  private VirtualLiveImageSetDataXMLWriter _virtualLiveImageSetDataXMLWriter = null;
  private boolean _isVirtualLiveModeEnabled = false;
  private static boolean _enablePsp = false;
  
  /*
   * Kee Chin Seong - Cad Creator 
   */
  private V810CadCreatorEnvironment _cadCreatorEnvironmentPanel;
  
  private FringePatternDialog _fringePatternDialog;
  private FringePatternObservable _fringePatternObservable;
  private AssertObservable _assertObservable;
  //Project's history log CR- hsia-fen.tan
  private static ProjectHistoryLog _fileWriter  = ProjectHistoryLog.getInstance();
  
  private AlarmClock _alarmClock;
  public final static int TABLE_ROW_HEIGHT_EXTRA = 8;  // number of pixels to add to a table row to increase row height

  static private BooleanLock _licenseMonitorStart = null;
  static private BusinessException _currentException = null;
  private final BooleanLock acquireVirtualLiveImagesInProgress = new BooleanLock(false);
  
  // one hour check once. 
  private static final int LICENSE_MONITOR_TIMEOUT_IN_MILISECONDS = 3600000;
  
  private static boolean _terminateIrpsWhenExit = false;
  //Khaw Chek Hau - XCR2654: CAMX Integration
  private AbstractShopFloorSystem _shopFloorSystem = AbstractShopFloorSystem.getInstance();
  
  //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
  private MachineUtilizationReportSystem _machineUtilizationReportSystem = MachineUtilizationReportSystem.getInstance();
  
  //Khaw Chek Hau - XCR2317: Component out of boundary message prompt but no further action allowed
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  
  //Swee-Yee.Wong - XCR-3157
  //IRP error message is prompted if run alignment/fine tuning without run full cdna and without unload the board 1st
  private SystemStatusPersistance _systemStatusPersistance;
  private boolean _isMagnificationChanged = false;

  //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
  private VOneMachineStatusMonitoringSystem _vOneMachineStatusMonitoringSystem = VOneMachineStatusMonitoringSystem.getInstance();
  
  //Khaw Chek Hau - XCR3670: V-ONE AXI latest status monitoring log
  private MachineLatestStatusLogWriter _machineLatestStatusLogWriter = MachineLatestStatusLogWriter.getInstance();
  
  /**
   * @author George Booth
   */
  static
  {
    // this setup assert needs to be here, otherwise if we get an
    // assert in this static initializer, the assert dialog will
    // not pop up. We will pass in a null Jframe for now, in the
    // constructor of this class, we will set it up again with the
    // "this" variable passed in as the Jframe.
    // Please do not place any code before this has the possiblility
    // of asseerting, otherwise you won't get the nice dialog, it will
    // look like the software has hung. - G$
    AssertUtil.setUpAssert(null);

    _startupError = false;
    Assert.setLogFile("internalErrors", Version.getVersion());

    try
    {
      // LeeHerng 7 June 2010 - We show splash screen here before loading config
      // to make it feel like more responsive.
      _splash = new AxiSplashScreen("");
      _splash.started();
      
      startLicenseMonitor();
      
      //XCR3648: Software crash when eject XXL dongle license while loading V810
      if (LicenseWorkerThread.getInstance().hasLicenseException())
        throw LicenseWorkerThread.getInstance().getLicenseException();
      
      XrayTester.setIsOfflineProgramming(LicenseManager.isOfflineProgramming());
      XrayTester.setXXLBased(LicenseManager.isXXLEnabled());
      XrayTester.setS2EXEnabled(LicenseManager.isS2EXEnabled());
      XrayTester.setThroughPutEnabled(LicenseManager.isThroughputEnabled());
      
      if (LicenseManager.isOfflineProgramming())
      {
        changeSystemTypeDefaultConfigIfNecessary();
      }
      boolean english = false;
      
      // BeeHoon- Get the bit size of IRP.exe once the IRP is going to start.
      Config.checkIs64BitsIRP();
      
      // load the config files
      Config config = Config.getInstance();
      config.loadIfNecessary();
      // set the proper language up
      String lang = config.getStringValue(SoftwareConfigEnum.LANGUAGE);
      StringLocalizer.setLocale(new Locale(lang));
      if (lang.equalsIgnoreCase("en"))
      {
        english = true;
      }

      RMISocketFactory.setSocketFactory(new TimeOutFactory(config.getIntValue(HardwareConfigEnum.RMI_TIMEOUT_IN_SECONDS)));

      //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
      setDebugIfNecessary();

      try
      {
        _enablePsp = config.getBooleanValue(SoftwareConfigEnum.ENABLE_PSP);
        _terminateIrpsWhenExit = config.getBooleanValue(SoftwareConfigEnum.TERMINATE_IMAGE_RECONSTRUCTION_PROCESSORS_WHEN_EXIT);
        
        startLicenseMonitor();
        
        //XCR3648: Software crash when eject XXL dongle license while loading V810
        if (LicenseWorkerThread.getInstance().hasLicenseException())
          throw LicenseWorkerThread.getInstance().getLicenseException();
        
        if(SwingUtils.isHDScreenResolutionOrHigher())
        {
          for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
          {
            if ("Nimbus".equals(info.getName()))
            {
              UIManager.setLookAndFeel(info.getClassName());
	      
              // XCR-3099 row is not highlighted when is selected [JList] Vincent Tan 8/12/2015 
              UIDefaults defaults = UIManager.getLookAndFeelDefaults();
              LookAndFeel lookAndFeel = UIManager.getLookAndFeel();
              if (lookAndFeel instanceof NimbusLookAndFeel)
              {
                NimbusLookAndFeel nimbusLookAndFeel = (NimbusLookAndFeel) lookAndFeel;

                defaults.put("List[Selected].textForeground", nimbusLookAndFeel.getDerivedColor("nimbusLightBackground", 0.0f, 0.0f, 0.0f, 0, false));
                defaults.put("List[Selected].textBackground", nimbusLookAndFeel.getDerivedColor("nimbusSelectionBackground", 0.0f, 0.0f, 0.0f, 0, false));
                defaults.put("List[Disabled+Selected].textBackground", nimbusLookAndFeel.getDerivedColor("nimbusSelectionBackground", 0.0f, 0.0f, 0.0f, 0, false));
                defaults.put("List[Disabled].textForeground", nimbusLookAndFeel.getDerivedColor("nimbusDisabledText", 0.0f, 0.0f, 0.0f, 0, false));
                defaults.put("List:\"List.cellRenderer\"[Disabled].background", nimbusLookAndFeel.getDerivedColor("nimbusSelectionBackground", 0.0f, 0.0f, 0.0f, 0, false));
              }
              break;
            }
          }
        }
        setUIFont(new javax.swing.plaf.FontUIResource("SansSerif", Font.PLAIN, 12));
        Font defaultFont =  new Font("SansSerif", Font.PLAIN, 12);
        UIManager.put("OptionPane.messageFont", defaultFont);
        java.awt.Component a = new java.awt.Label();
        FontMetrics fm = a.getFontMetrics(defaultFont);
        int prefHeight = fm.getHeight() + TABLE_ROW_HEIGHT_EXTRA;
        UIManager.put("Table.rowHeight", new Integer(prefHeight));
        UIManager.put("Table:\"Table.cellRenderer\".contentMargins", new Insets( 6, 6, 6, 6 ) );
        UIManager.put("Table.showGrid",true);
        UIManager.put("Table.gridColor",Color.LIGHT_GRAY);
        UIManager.put("CheckBox.background", Color.WHITE);
//        UIManager.put("Label.contentMargins", new Insets( 2, 2, 2, 2 ) );
//        UIManager.put("Label.opaque", true);
      }
      
      //XCR3648: Software crash when eject XXL dongle license while loading V810
      catch (final Exception e)
      {
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            MessageDialog.showErrorDialog(null,
                e.getMessage(),
                StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                true);
            System.exit(-1);
          }
        });
      }

      try
      {
        try
        {
          // allow first time after install setup to happen          
          
          Server.finalizeInstallSettingsIfNecessary();
          Config.getInstance().forceLoad();
        }
        catch (final PreviousConfigDirectoryDoesNotExistDatastoreException ex)
        {
          // just display this message, but keep going because this is normal on the first install from
          // manufacturing
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              MessageDialog.showErrorDialog(null,
                      ex.getMessage(),
                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                      true);
            }
          });
        }
        
        // LeeHerng - Check PSP Configuration
        PspConfiguration.getInstance().checkConfiguration();
        
        // XCR-3175 5.8 installer only support OS windows 8 and above
        // Add new checking to make sure V810 5.8 and above is only allowed to run on 64-bit (a.k.a Series 2) machine
        if (Config.is64bitIrp() == false)
        {
          throw new UnsupportedSystemPlatformException(Version.getVersionNumber());
        }
        
        // XCR-3822 System crash when switch to xray safety test tab
        Config.getInstance().checkXrayActuatorSetting();
      }
      catch (final XrayTesterException dex)
      {
        _startupError = true;
        SwingUtils.invokeLater(new Runnable()
        {

          public void run()
          {
            MessageDialog.showErrorDialog(null,
                    dex.getMessage(),
                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                    true);
            System.exit(-1);
          }
        });
      }

      // tell Swing to use the correct font for JOption panes
      // This call needs to be here to support Chinese and other languages!
      if (english == false)
      {
        UIManager.put("OptionPane.messageFont", new Font("Dialog", Font.PLAIN, 14));
      }  
      
      //XCR-3513 - Only low magnification alignment is running although VM is enable
      XrayTester.setIsVariableMagEnabled(LicenseManager.isVariableMagnificationEnabled());
      // map velocity mapping table for MVEDR
      VelocityMapping.getInstance().populateData();
    }
    
    //XCR3648: Software crash when eject XXL dongle license while loading V810
    catch (final Exception e)
    {
      _startupError = true;
      
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showErrorDialog(null,
              e.getMessage(),
              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
              true);
          System.exit(-1);
        }
      });
    }
  }

  /**
   * @author George Booth
   */
  public static synchronized MainMenuGui getInstance()
  {
    if (_instance == null)
    {
      _instance = new MainMenuGui();
    }

    return _instance;
  }

  /**
   * @author George Booth
   */
  private MainMenuGui()
  {
    AssertUtil.setUpAssert(this);

    _startAs = UserTypeEnum.DEVELOPER;
    _runningAs = UserTypeEnum.DEVELOPER;

    _appNameKey = "MMGUI_GUI_NAME_KEY";
    _appName = StringLocalizer.keyToString(_appNameKey);

    // XCR1144
    // Chnee Khang Wah, 23-Nov-2010
    _panelPositioner = PanelPositioner.getInstance();
    _interlock = Interlock.getInstance();
    _interlockPollingThread = new WorkerThread("Interlock Polling Thread");
    _hardwareWorkerThread2 = HardwareWorkerThread.getInstance();

    AbstractXraySource xraySource = AbstractXraySource.getInstance();
    Assert.expect(xraySource instanceof HTubeXraySource);
    _hTubeXraySource = (HTubeXraySource) xraySource;
    // XCR1144 - end
  }

  /**
   * Do not allow the user access to the ui menu and toolbars only.  Does allow user to interact with
   * the rest of the UI
   * @author Andy Mechtenberg
   */
  public void disableMenuAndToolBars()
  {
    Assert.expect(SwingUtilities.isEventDispatchThread());

    _menuEnableStatus.clear();
    _envToolBarEnableStatus.clear();
    _mainToolBarEnableStatus.clear();

    // start with the menuBar, then find each menu item for each menu
    int menuCount = _baseMenuBar.getMenuCount();
    for (int i = 0; i < menuCount; i++)
    {
      JMenu menu = _baseMenuBar.getMenu(i);
      int menuItemCount = menu.getItemCount();
      for (int j = 0; j < menuItemCount; j++)
      {
        JMenuItem menuItem = menu.getItem(j);
        if (menuItem != null) // null means we hit a separator
        {
          _menuEnableStatus.add(new Boolean(menuItem.isEnabled()));
          _baseMenuBar.getMenu(i).getItem(j).setEnabled(false);
        }
      }
      _menuEnableStatus.add(new Boolean(menu.isEnabled()));
      _baseMenuBar.getMenu(i).setEnabled(false);
    }

    // now, the environments toolbar
    int toolBarCount = _envToolBar.getComponentCount();
    for (int i = 0; i < toolBarCount; i++)
    {
      _envToolBarEnableStatus.add(new Boolean(_envToolBar.getComponent(i).isEnabled()));
      _envToolBar.getComponent(i).setEnabled(false);
    }

    // now, the main toolbar
    toolBarCount = _mainToolBar.getComponentCount();
    for (int i = 0; i < toolBarCount; i++)
    {
      _mainToolBarEnableStatus.add(new Boolean(_mainToolBar.getComponent(i).isEnabled()));
      _mainToolBar.getComponent(i).setEnabled(false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void enableMainToolBar()
  {
    // now, the main toolbar
    int toolBarCount = _mainToolBar.getComponentCount();
    for (int i = 0; i < toolBarCount; i++)
    {
      _mainToolBar.getComponent(i).setEnabled(true);
    }
  }

  /**
   * Allow the user access again to the UI menu and toolbars only.  Does allow user to interact with
   * the rest of the UI
   * @author Andy Mechtenberg
   */
  public void enableMenuAndToolBars()
  {
    // this method should be able to be called even if the menus are already enabled.
    // but, should do nothing in that case
    if (_menuEnableStatus.isEmpty() && _envToolBarEnableStatus.isEmpty() && _mainToolBarEnableStatus.isEmpty())
    {
      return;
    }

    // start with the menuBar, then find each menu item for each menu
    int menuCount = _baseMenuBar.getMenuCount();
    Iterator<Boolean> menuEnabledIt = _menuEnableStatus.iterator();
    for (int i = 0; i < menuCount; i++)
    {
      JMenu menu = _baseMenuBar.getMenu(i);
      int menuItemCount = menu.getItemCount();
      for (int j = 0; j < menuItemCount; j++)
      {
        JMenuItem menuItem = menu.getItem(j);
        if (menuItem != null) // null means we hit a separator
        {
          boolean enabled = menuEnabledIt.next().booleanValue();
          menuItem.setEnabled(enabled);
        }
      }
      boolean enabled = menuEnabledIt.next().booleanValue();
      menu.setEnabled(enabled);
    }
    _menuEnableStatus.clear();

    // menu states may have changed after the enables where saved -
    // set the Action menu based on current user and system states
    _baseMenuBar.setActionMenuStates();

    // now, the environments toolbar
    int toolBarCount = _envToolBar.getComponentCount();
    Assert.expect(_envToolBarEnableStatus.size() == toolBarCount);
    for (int i = 0; i < toolBarCount; i++)
    {
      boolean enabled = _envToolBarEnableStatus.get(i).booleanValue();
      _envToolBar.getComponent(i).setEnabled(enabled);
    }
    _envToolBarEnableStatus.clear();

    // now, the main toolbar
    toolBarCount = _mainToolBar.getComponentCount();
    Assert.expect(_mainToolBarEnableStatus.size() == toolBarCount);
    for (int i = 0; i < toolBarCount; i++)
    {
      boolean enabled = _mainToolBarEnableStatus.get(i).booleanValue();
      _mainToolBar.getComponent(i).setEnabled(enabled);
    }
    _mainToolBarEnableStatus.clear();
  }

  /**
   * @author George Booth
   */
  public TestDev getTestDev()
  {
    return _testDevEnvironment;
  }

  /**
   * @author Scott Richardson
   */
  public V810VirtualLiveEnvironment getVirtualLive()
  {
    return _virtualLiveEnvironmentPanel;
  }

  /**
   * Get the VIRTUAL_LIVE_MODE flag as set in the software.config file.
   * @author Scott Richardson
   * @author Sham khang Shian
   */
  public boolean isVirtualLiveModeEnabled()
  {
    try
    {
      return LicenseManager.isVirtualLiveEnabled();
    }
    catch (BusinessException ex)
    {
      return false;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public TestProgram getTestProgramWithReason(String reasonTestProgramNeeded, String dialogTitle, final Project project)
  {
    Assert.expect(dialogTitle != null);
    Assert.expect(project != null);
    if (project.isTestProgramValid() == false)
    {
      generateTestProgramWithReason(reasonTestProgramNeeded, dialogTitle, project);
    }

    return project.getTestProgram();
  }

  /**
   * @author Andy Mechtenberg
   */
  public TestProgram getTestProgramWithReason(String reasonTestProgramNeeded, String dialogTitle, final Project project, Font font)
  {
    Assert.expect(dialogTitle != null);
    Assert.expect(project != null);
    Assert.expect(font != null);
    Font originalFont = _dialogFont;
    _dialogFont = font;
    TestProgram program = getTestProgramWithReason(reasonTestProgramNeeded, dialogTitle, project);
    _dialogFont = originalFont;
    return program;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void generateTestProgramIfNeededWithReason(String reasonTestProgramNeeded, String dialogTitle, final Project project)
  {
    Assert.expect(dialogTitle != null);
    Assert.expect(project != null);
    if (project.isTestProgramValid() == false)
    {
      generateTestProgramWithReason(reasonTestProgramNeeded, dialogTitle, project);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void generateTestProgramIfNeededWithReason(String reasonTestProgramNeeded, String dialogTitle, final Project project, Font font)
  {
    Assert.expect(dialogTitle != null);
    Assert.expect(project != null);
    Assert.expect(font != null);

    if (project.isTestProgramValid() == false)
    {
      Font originalFont = _dialogFont;
      _dialogFont = font;
      generateTestProgramWithReason(reasonTestProgramNeeded, dialogTitle, project);
      _dialogFont = originalFont;
    }
  }
  
  /*
   * @author Kee Chin Seong
   *  - to have clear cut between MODIFY and GENERATE function, 
   *    but internally is using same function
   */
  public void modifyTestProgramIfNeededWithReason(String reasonTestProgramNeeded, String dialogTitle, final Project project)
  {
    Assert.expect(dialogTitle != null);
    Assert.expect(project != null);
    
    generateTestProgramWithReason(reasonTestProgramNeeded, dialogTitle, project);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void generateTestProgramForAlignmentIfNeededWithReason(String reasonTestProgramNeeded, String dialogTitle, final Project project)
  {
    Assert.expect(dialogTitle != null);
    Assert.expect(project != null);
    if (project.isTestProgramValidIgnoringAlignmentRegions() == false)
    {
      generateTestProgramWithReason(reasonTestProgramNeeded, dialogTitle, project);
    }
  }
  
  /**
   * @author Andy Mechtenberg
   * @edited By Kee Chin Seong - Swing Worker Thread is responding slow, use SwingUtils wil help !
   */
  private void generateTestProgramWithReason(String reasonTestProgramNeeded, String dialogTitle, final Project project)
  {
    Assert.expect(dialogTitle != null);
    Assert.expect(project != null);
    Assert.expect(SwingUtilities.isEventDispatchThread());
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_instance,
            reasonTestProgramNeeded,
            dialogTitle,
            true);

    SwingUtils.setFont(busyCancelDialog, _dialogFont);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _instance);
    SwingUtils.invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          project.getTestProgram();
          try
          {
            // now lets sleep for 2 seconds so that we can make sure the dialog doesn't just flash.
            Thread.sleep(1000);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }

        }
        finally
        {
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.setVisible(false);
        }
      }
    });
    busyCancelDialog.setVisible(true);
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean isPanelSizeWithinSpec(Font font)
  {
    Assert.expect(font != null);
    Font originalFont = _dialogFont;
    _dialogFont = font;
    boolean panelSizeWithinSpec = isPanelSizeWithinSpec();
    _dialogFont = originalFont;
    return panelSizeWithinSpec;
  }

  /**
   * @author George Booth
   */
  public boolean isPanelSizeWithinSpec()
  {
    com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
    final int panelWidth = panel.getWidthAfterAllRotationsInNanoMeters();
    final int panelLength = panel.getLengthAfterAllRotationsInNanoMeters();

    int minPanelWidth = _panelHandler.getMinimumPanelWidthInNanoMeters();
    int maxPanelWidth = _panelHandler.getMaximumPanelWidthInNanoMeters();
    Assert.expect(minPanelWidth < maxPanelWidth, "max panel width less than min?");
    int minPanelLength = _panelHandler.getMinimumPanelLengthInNanoMeters();
    int maxPanelLength = _panelHandler.getMaximumPanelLengthInNanoMeters();
    Assert.expect(minPanelLength < maxPanelLength, "max panel length less than min?");

    // check the safety limit on width
    if (panelWidth < minPanelWidth)
    {
      MathUtilEnum units = _project.getDisplayUnits();
      double actualWidthInUnits = (double) MathUtil.convertUnits((double) panelWidth, MathUtilEnum.NANOMETERS, units);
      double minWidthInUnits = (double) MathUtil.convertUnits((double) minPanelWidth, MathUtilEnum.NANOMETERS, units);
      double maxWidthInUnits = (double) MathUtil.convertUnits((double) maxPanelWidth, MathUtilEnum.NANOMETERS, units);
      LocalizedString errorMessage = new LocalizedString("MMGUI_PANEL_WIDTH_TOO_SMALL_ERROR_KEY",
              new Object[]
              {
                actualWidthInUnits, units, minWidthInUnits, maxWidthInUnits, units
              });
      MessageDialog.showErrorDialog(this, StringLocalizer.keyToString(errorMessage), StringLocalizer.keyToString(_appNameKey), true, _dialogFont);
      return false;
    }
    else if (panelWidth > maxPanelWidth)
    {
      MathUtilEnum units = _project.getDisplayUnits();
      double actualWidthInUnits = (double) MathUtil.convertUnits((double) panelWidth, MathUtilEnum.NANOMETERS, units);
      double minWidthInUnits = (double) MathUtil.convertUnits((double) minPanelWidth, MathUtilEnum.NANOMETERS, units);
      double maxWidthInUnits = (double) MathUtil.convertUnits((double) maxPanelWidth, MathUtilEnum.NANOMETERS, units);
      LocalizedString errorMessage = new LocalizedString("MMGUI_PANEL_WIDTH_TOO_LARGE_ERROR_KEY",
              new Object[]
              {
                actualWidthInUnits, units, minWidthInUnits, maxWidthInUnits, units
              });
      MessageDialog.showErrorDialog(this, StringLocalizer.keyToString(errorMessage), StringLocalizer.keyToString(_appNameKey), true, _dialogFont);
      return false;
    }

    // check the max safety limit on length
    if (panelLength < minPanelLength)
    {
      MathUtilEnum units = _project.getDisplayUnits();
      double actualLengthInUnits = (double) MathUtil.convertUnits((double) panelLength, MathUtilEnum.NANOMETERS, units);
      double minLengthInUnits = (double) MathUtil.convertUnits((double) minPanelLength, MathUtilEnum.NANOMETERS, units);
      double maxLengthInUnits = (double) MathUtil.convertUnits((double) maxPanelLength, MathUtilEnum.NANOMETERS, units);
      LocalizedString errorMessage = new LocalizedString("MMGUI_PANEL_LENGTH_TOO_SMALL_ERROR_KEY",
              new Object[]
              {
                actualLengthInUnits, units, minLengthInUnits, maxLengthInUnits, units
              });
      MessageDialog.showErrorDialog(this, StringLocalizer.keyToString(errorMessage), StringLocalizer.keyToString(_appNameKey), true, _dialogFont);
      return false;
    }
    else if (panelLength > maxPanelLength)
    {
      MathUtilEnum units = _project.getDisplayUnits();
      double actualLengthInUnits = (double) MathUtil.convertUnits((double) panelLength, MathUtilEnum.NANOMETERS, units);
      double minLengthInUnits = (double) MathUtil.convertUnits((double) minPanelLength, MathUtilEnum.NANOMETERS, units);
      double maxLengthInUnits = (double) MathUtil.convertUnits((double) maxPanelLength, MathUtilEnum.NANOMETERS, units);
      LocalizedString errorMessage = new LocalizedString("MMGUI_PANEL_LENGTH_TOO_LARGE_ERROR_KEY",
              new Object[]
              {
                actualLengthInUnits, units, minLengthInUnits, maxLengthInUnits, units
              });
      MessageDialog.showErrorDialog(this, StringLocalizer.keyToString(errorMessage), StringLocalizer.keyToString(_appNameKey), true, _dialogFont);
      return false;
    }

    return true;
  }
  
  /*
   * chin seong, kee
   * added new 3rd parameter - 1 - mm, 2 - nanometer, 3 - mils
   */
  public boolean isVirtualLiveDummyPanelSizeWithinSpec(double panelWidthInNanometers, double panelLengthInNanometers, double panelThicknessInNanometers)
  {
    double minPanelWidthInNanometers = _panelHandler.getMinimumPanelWidthInNanoMeters();
    double maxPanelWidthInNanometers = _panelHandler.getMaximumPanelWidthInNanoMeters();
    double minPanelLengthInNanometers = _panelHandler.getMinimumPanelLengthInNanoMeters();
    double maxPanelLengthInNanometers = _panelHandler.getMaximumPanelLengthInNanoMeters();        
    double minPanelThicknessInNanometers = _panelHandler.getMinimumPanelThicknessInNanoMeters();
    double maxPanelThicknessInNanometers = _panelHandler.getMaximumPanelThicknessInNanoMeters();  
    
    // Sanity Check
    Assert.expect(minPanelWidthInNanometers < maxPanelWidthInNanometers, "max panel width less than min?");
    Assert.expect(minPanelLengthInNanometers < maxPanelLengthInNanometers, "max panel length less than min?"); 
    
    // check the safety limit on width
    if (panelWidthInNanometers < minPanelWidthInNanometers || panelWidthInNanometers > maxPanelWidthInNanometers)
    {
      return false;
    }

    // check the max safety limit on length
    if (panelLengthInNanometers < minPanelLengthInNanometers || panelLengthInNanometers > maxPanelLengthInNanometers)
    {
      return false;
    }
    
    // check the max safety limit on thickness
    if (panelThicknessInNanometers < minPanelThicknessInNanometers || panelThicknessInNanometers > maxPanelThicknessInNanometers)
    {
      return false;
    }

    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean isPanelInLegalState(Font font)
  {
    Assert.expect(font != null);
    Font originalFont = _dialogFont;
    _dialogFont = font;
    boolean panelInLegalState = isPanelInLegalState();
    _dialogFont = originalFont;
    return panelInLegalState;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isPanelInLegalState()
  {
    // we need to check for:
    // - At least one board on the panel
    // - Boards do not extend beyond panel boundary
    // - Boards have not been resized such that components lie outside board boundary
    //     (only if BoardType width/length have changed)
    // - Boards do not overlap each other
    if (_project == null)
    {
      return true;
    }

    if (_project.getPanel().getBoards().isEmpty())
    {
      MessageDialog.showErrorDialog(this,
              StringLocalizer.keyToString("MMGUI_SETUP_PANEL_NO_BOARDS_ERROR_KEY"),
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
              true,
              _dialogFont);
      return false;
    }

    java.util.List<Board> badBoards = _project.getPanel().getBoardsOutsidePanel();
    if (badBoards.isEmpty() == false)
    {
      if (badBoards.size() == 1)
      {
        // single board is bad case
        LocalizedString errorMessage = new LocalizedString("MMGUI_SETUP_PANEL_BOARD_OUTSIDE_PANEL_ERROR_KEY",
                new Object[]
                {
                  badBoards.get(0).getName()
                });
        MessageDialog.showErrorDialog(this,
                StringLocalizer.keyToString(errorMessage),
                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                true,
                _dialogFont);
        return false;
      }
      else
      {
        String badBoardListString = "";
        for (Board badBoard : badBoards)
        {
          badBoardListString = badBoardListString + "\"" + badBoard.getName() + "\", ";
        }
        // drop the last ", " -- 2 chars
        badBoardListString = badBoardListString.substring(0, badBoardListString.length() - 2);
        badBoardListString = badBoardListString + " ";
        LocalizedString errorMessage = new LocalizedString("MMGUI_SETUP_PANEL_BOARDS_OUTSIDE_PANEL_ERROR_KEY",
                new Object[]
                {
                  badBoardListString
                });
        MessageDialog.showErrorDialog(this,
                StringLocalizer.keyToString(errorMessage),
                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                true,
                _dialogFont);
        return false;
      }
    }
    // check for components outside of the panel
    for (BoardType boardType : _project.getPanel().getBoardTypes())
    {
      java.util.List<ComponentType> outsideComponentTypes = boardType.getComponentTypesOutsideBoardType();
      if (outsideComponentTypes.isEmpty() == false)
      {
        String componentList = "";
        for (ComponentType comp : outsideComponentTypes)
        {
          componentList += comp.getReferenceDesignator() + ", ";
        }
        componentList = componentList.substring(0, componentList.lastIndexOf(","));
        LocalizedString errorMessage = new LocalizedString("MMGUI_SETUP_COMPONENTS_OUTSIDE_BOARD_ERROR_KEY",
                new Object[]
                {
                  boardType.getName(), componentList
                });
        MessageDialog.showErrorDialog(this,
                StringLocalizer.keyToString(errorMessage),
                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                true,
                _dialogFont);
        
        //Khaw Chek Hau - XCR2317: Component out of boundary message prompt but no further action allowed
        if ((JOptionPane.showConfirmDialog(
            this,
            StringLocalizer.keyToString(new LocalizedString("MMGUI_CONFIRM_DELETE_OUT_OF_BOUNDARY_COMPONENTS_KEY", null)),
            StringLocalizer.keyToString("MMGUI_CONFIRM_DELETE_OUT_OF_BOUNDARY_COMPONENTS_TITLE_KEY"),
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION))
        {
          //Khaw Chek Hau - XCR3554: Package on package (PoP) development
          java.util.List<String> compPackageOnPackageList = new ArrayList<String>();
          java.util.List<String> componentsRefDesList = new ArrayList<String>();
          
          for (ComponentType componentType : outsideComponentTypes)
          {
            if (componentType.hasCompPackageOnPackage())  
            {
              compPackageOnPackageList.add(componentType.getCompPackageOnPackage().getPOPName());
              componentsRefDesList.add(componentType.getReferenceDesignator());
            }
          }
          
          if (compPackageOnPackageList.isEmpty() == false)
          {
            int status = JOptionPane.CANCEL_OPTION;
            status = JOptionPane.showConfirmDialog(this, 
                                                StringLocalizer.keyToString(new LocalizedString("MMGUI_DELETE_COMPONENTS_WOULD_DELETE_POP_KEY",
                                                                                                 new Object[]{componentsRefDesList, compPackageOnPackageList})),
                                                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), 
                                                JOptionPane.YES_NO_CANCEL_OPTION, 
                                                JOptionPane.QUESTION_MESSAGE);

            if(status != JOptionPane.YES_OPTION)
            {
              return false;
            }   
          }
          
          //Khaw Chek Hau - XCR3424: Assert when switch from Panel Setup to Modify subtypes after delete components lie outside boundary
          for (ComponentType componentTypeToRemove : outsideComponentTypes)
          {
            java.util.List<PadType> padTypes = componentTypeToRemove.getInterferenceAffectedPadTypes();

            for (PadType padType : padTypes)
            {
              padType.deleteComponentTypeThatCausesInterferencePatterns(componentTypeToRemove);
            }

            componentTypeToRemove.remove();
          }
        }
        else
          return false;
      }
      // check for fiducials outside of the panel
      java.util.List<FiducialType> outsideFiducialTypes = boardType.getFiducialTypesOutsideBoardType();
      if (outsideFiducialTypes.isEmpty() == false)
      {
        String fiducialList = "";
        for (FiducialType fid : outsideFiducialTypes)
        {
          fiducialList += fid.getName() + ", ";
        }
        fiducialList = fiducialList.substring(0, fiducialList.lastIndexOf(","));

        LocalizedString errorMessage = new LocalizedString("MMGUI_SETUP_FIDUCIALS_OUTSIDE_BOARD_ERROR_KEY",
                new Object[]
                {
                  boardType.getName(), fiducialList
                });
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                StringLocalizer.keyToString(errorMessage),
                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                true,
                _dialogFont);

        //Khaw Chek Hau - XCR2317: Component out of boundary message prompt but no further action allowed
        if ((JOptionPane.showConfirmDialog(
            this,
            StringLocalizer.keyToString(new LocalizedString("MMGUI_CONFIRM_DELETE_OUT_OF_BOUNDARY_FIDUCIALS_KEY", null)),
            StringLocalizer.keyToString("MMGUI_CONFIRM_DELETE_OUT_OF_BOUNDARY_FIDUCIALS_TITLE_KEY"),
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION))
        {
          try
          {
            for (FiducialType fiducial : outsideFiducialTypes)
            {
              _commandManager.execute(new RemoveFiducialTypeCommand(fiducial));
            }
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(this, ex.getLocalizedMessage(),
            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          }
        }
        else
          return false;
      }
    }
    if (_project.getPanel().getFiducialTypesOutsidePanel().isEmpty() == false)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
              StringLocalizer.keyToString("MMGUI_SETUP_FIDUCIALS_OUTSIDE_PANEL_ERROR_KEY"),
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
              true,
              _dialogFont);
      return false;
    }
    return true;
  }

  /**
   * @author George Booth
   */
  public static void main(final String[] args)
  {
    _commandLineArgs = args;

    SwingUtils.invokeLater(new Runnable()
    {

      public void run()
      {
        Date date = new Date();
        System.out.println("v810 UI Starting at: " + date);
        getInstance();

//        // determine start-up personality
//        _startAs = _loginManager.getUserTypeFromRunString(args);
//
//        if (_startAs == UserTypeEnum.LOGIN)
//        {
//          _startAs = _loginManager.getUserTypeFromFromLogIn(_instance);
//          // user can cancel login
//          if (_startAs == null)
//            System.exit(-1);
//        }

          _instance.start();
      }
    });
  }

  /**
   *  sets the default font for all Swing components.
   *  ex. 
   *   setUIFont (new javax.swing.plaf.FontUIResource
   *    ("Serif",Font.ITALIC,12));
   * @param f
   * @author Chong, Wei Chin
   */
  public static void setUIFont(javax.swing.plaf.FontUIResource f)
  {
    java.util.Enumeration keys = UIManager.getLookAndFeelDefaults().keys();
    while (keys.hasMoreElements())
    {
      Object key = keys.nextElement();
      Object value = UIManager.get(key);
      if (value instanceof javax.swing.plaf.FontUIResource)
      {
        UIManager.put(key, f);
      }
    }
  }

  /**
   * @author George Booth
   */
  public void start()
  {
    Assert.expect(SwingUtilities.isEventDispatchThread());
    if (_startupError)
    {
      return;
    }
    startServer();

    jbInit();
    pack();
    placeOnScreen();

    _server.setServerInitialized();
    _splash.finished();
    
    // require to run full CD&A when start v810
    try
    {
      //Swee Yee Wong - set all CD&A task to run at next startup
      XrayHardwareAutomatedTaskList.getInstance().setCameraCalibrationsToRunASAP();
      XrayHardwareAutomatedTaskList.getInstance().setAllCalibrationsToRunASAP();
    }
    catch (DatastoreException dex)
    {
      handleXrayTesterException(dex);
    }

    // get user accounts information
    _loginManager = LoginManager.getInstance();
    try
    {
      _loginManager.readUserAccounts();
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);
    }
    
    // Ying-Huan.Chu [XCR1661]
    Config config = Config.getInstance();
    if (config.getCorruptedBinaryConfigFileNameList().size() > 0)
    {
      String warningMessage = "";
      String warningTitle = StringLocalizer.keyToString("CORRUPTED_BINARY_CONFIG_FILE_ERROR_TITLE_KEY");
      for (String fileName : Config.getInstance().getCorruptedBinaryConfigFileNameList())
      {
        warningMessage += StringLocalizer.keyToString(new LocalizedString("CORRUPTED_BINARY_CONFIG_FILE_ERROR_MESSAGE_KEY",
                                                      new Object[]{fileName}));
      }
      MessageDialog.showWarningDialog(this, warningMessage, warningTitle, true);
    }
    
    // Chnee Khang Wah, 2015-05-26, log System magnification settings
    if(XrayTester.isS2EXEnabled())
    {
      String currentSystemLowMag =
        Config.getSystemCurrentLowMagnification();
      String currentSystemHighMag =
        Config.getSystemCurrentHighMagnification();
      
      System.out.println("System's magnification settings: Low Mag = "+currentSystemLowMag+" , High Mag = "+currentSystemHighMag+".");
      //Swee-Yee.Wong - XCR-3157
      //IRP error message is prompted if run alignment/fine tuning without run full cdna and without unload the board 1st
      _systemStatusPersistance = new SystemStatusPersistance();
      _systemStatusPersistance = _systemStatusPersistance.getPersistentSystemInformation();
      System.out.println("System's previous magnification settings: Low Mag = "+_systemStatusPersistance.getPrevSystemLowMag()+" , High Mag = "+_systemStatusPersistance.getPrevSystemHighMag()+".");
    }
    
    // Chnee Khang Wah, 2014-06-25, XCR 2008
    // Check for Configuration error
    if (config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
    {
      //Ngie Xing, added checking for XXL and S2EX
      int maxThickness = config.getIntValue(HardwareConfigEnum.PANEL_HANDLER_MAXIMUM_PANEL_THICKNESS_IN_NANOMETERS);
      int maxSliceHeight = config.getIntValue(SoftwareConfigEnum.MAXIMUM_SLICE_HEIGHT_FROM_NOMINAL_SLICE_IN_NANOMETERS);
      int minSliceHeight = config.getIntValue(SoftwareConfigEnum.MINIMUM_SLICE_HEIGHT_FROM_NOMINAL_SLICE_IN_NANOMETERS);
      
      // The following checking is for XXL system only.
      if(XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
      {
        int diff = -(minSliceHeight+maxThickness);
        // typical diff between min slice height and max thickness is about 50mil = 1270000nm
        if(diff < XrayTester.getTypicalDiffBetweenMaxThicknessAndMinSliceHeightForXXL())
        {
          setWarningBarText(StringLocalizer.keyToString("MMGUI_WARNING_MAX_THICKNESS_LARGER_THAN_MIN_SLICE_HEIGHT_KEY"));
          System.out.println("Maximum board thickness is either too near or larger than minimum slice height!");
          System.out.println("The gap between them now is "+diff+"nm, it should be at least "+XrayTester.getTypicalDiffBetweenMaxThicknessAndMinSliceHeightForXXL()+"nm");
        }
      }
      // The following checking is for standard system only.
      else
      {
        int diff = maxSliceHeight - maxThickness;

        // typical diff between max slice height and max thickness is about 70.5mil = 1790700nm
        if(diff < XrayTester.getTypicalDiffBetweenMaxThicknessAndMaxSliceHeight())
        {
          setWarningBarText(StringLocalizer.keyToString("MMGUI_WARNING_MAX_THICKNESS_LARGER_THAN_MAX_SLICE_HEIGHT_KEY"));
          System.out.println("Maximum board thickness is either too near or larger than maximum slice height!");
          System.out.println("The gap between them now is "+diff+"nm, it should be at least "+XrayTester.getTypicalDiffBetweenMaxThicknessAndMaxSliceHeight()+"nm");
        }
      }
    }
    
    // determine start-up personality from run string
    _startAs = _loginManager.getUserTypeFromRunString(_commandLineArgs);
    // for release 1.0, always prompt for a login
    _startAs = UserTypeEnum.LOGIN;
    
    //Khaw Chek Hau - XCR2654: CAMX Integration
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
    {
      try
      {
        _shopFloorSystem.executeCAMxProgram();
        _shopFloorSystem.softwarePowerUp();
      }
      catch (IOException ioe)
      {
          String errorMsg = ioe.getLocalizedMessage();
          MessageDialog.showErrorDialog(MainMenuGui.this,
                  errorMsg,
                  StringLocalizer.keyToString("SHOP_FLOOR_EVENT_TITLE_KEY"),
                  true);
      }
      catch (DatastoreException ex)
      {
        String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("SHOP_FLOOR_ERROR_MESSAGE_KEY_KEY");
        MessageDialog.showErrorDialog(MainMenuGui.this,
                errorMsg,
                StringLocalizer.keyToString("SHOP_FLOOR_EVENT_TITLE_KEY"),
                true);
      }
    } 
    
    //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT))
    {
      try
      {
        _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.CHECK_SOFTWARE_PREVIOUS_STATUS);
        _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.SOFTWARE_POWER_ON);
      }
      catch (DatastoreException ex)
      {
        String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_ERROR_MESSAGE_KEY");
        MessageDialog.showErrorDialog(MainMenuGui.this,
                                      errorMsg,
                                      StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_EVENT_TITLE_KEY"),
                                      true,
                                      FontUtil.getMediumFont());
      }
    }
    
    //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
    {
      try
      {
        _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING);
      }
      catch (DatastoreException ex)
      {
        String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_MESSAGE_KEY");
        MessageDialog.showErrorDialog(MainMenuGui.this,
                                      errorMsg,
                                      StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_EVENT_TITLE_KEY"),
                                      true,
                                      FontUtil.getMediumFont());
      }
    }

    if (_startAs.equals(UserTypeEnum.LOGIN))
    {
      login();
    }
  }

  /**
   * @author George Booth
   */
  private void jbInit()
  {
    addComponentListener(new ComponentAdapter()
    {

      public void componentResized(ComponentEvent e)
      {
        super.componentResized(e);
        setPreferredSize(getSize());
      }
    });

    _glassPaneMouseListener = new java.awt.event.MouseAdapter()
    {
      // do nothing -- this is used to disable the main frame from mouse clicks
    };
    getGlassPane().addMouseListener(_glassPaneMouseListener);

    _glassPaneKeyListener = new java.awt.event.KeyAdapter()
    {
      // do nothing -- this is used to disable the main frame from keys
    };
    getGlassPane().addKeyListener(_glassPaneKeyListener);

    setResizable(true);

    _baseMenuBar = new MainUIMenuBar(this);
    setJMenuBar(_baseMenuBar);

    getContentPane().setLayout(new BorderLayout());

    _mainToolBar = new MainUIToolBar(this);
    getContentPane().add(_mainToolBar, BorderLayout.WEST);
    _mainToolBar.setOrientation(SwingConstants.VERTICAL);
    _mainToolBar.setVisible(false);

    _envToolBar = new MainUIToolBar(this, null);

    _emptyToolBar = new MainUIToolBar(this, null, false);

    _centerPanel = new JPanel();
    _mainCardLayout = new CardLayout();
    _centerPanel.setLayout(_mainCardLayout);
    getContentPane().add(_centerPanel, BorderLayout.CENTER);

    _homeEnvironment = new HomeEnvironment(this);
    _centerPanel.add(_homeEnvironment, _HOME_ENV);
    _mainCardLayout.first(_centerPanel);

//    TestDev testDev = new TestDev(MainMenuGui.this);
//    _testDevEnvironment = testDev.getTestDevPanel();
    _testDevEnvironment = new TestDev(this);
    _centerPanel.add(_testDevEnvironment, _TEST_DEV_ENV);

    _testExecEnvironment = new TestExecEnvironment(this);
    _centerPanel.add(_testExecEnvironment, _TEST_EXEC_ENV);

    _configureEnvironment = new ConfigEnvironment(this);
    _centerPanel.add(_configureEnvironment, _CONFIG_ENV);

    _serviceEnvironment = new ServiceEnvironmentPanel(this);
    _centerPanel.add(_serviceEnvironment, _SERVICE_ENV);

// IF so set in the software.config file, instantiate VirtualLive
    if (isVirtualLiveModeEnabled())
    {
      _virtualLiveEnvironmentPanel = new V810VirtualLiveEnvironment(this, null, _testDevEnvironment.getPanelCadPanel());
      _centerPanel.add(_virtualLiveEnvironmentPanel, _VIRTUAL_LIVE_ENV);
    }
    
    _cadCreatorEnvironmentPanel = new V810CadCreatorEnvironment(this, null, _testDevEnvironment.getCreateNewCadPanel());
    _centerPanel.add(_cadCreatorEnvironmentPanel, _CAD_CREATOR_ENV);

    // status bar is not added unless requested
    _southPanel = new JPanel(new BorderLayout(5, 0));
    _southPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

    _statusPanel = new JPanel();
    _statusPanel.setLayout(new BorderLayout());
    //_southPanel.add(_statusPanel, BorderLayout.CENTER);
    _southPanel.add(_statusPanel, BorderLayout.CENTER);
    
    Border _statusOuterBorder = BorderFactory.createBevelBorder(
            BevelBorder.LOWERED, Color.white, Color.white,
            new Color(99, 99, 99), new Color(142, 142, 142));
    Border _statusInnerBorder = BorderFactory.createEmptyBorder(0, 5, 0, 0);
    Border _statusBorder = BorderFactory.createCompoundBorder(_statusOuterBorder, _statusInnerBorder);
    
    _statusTextPanel = new JPanel(new PairLayout(10, 0));
    //_statusTextPanel.setBorder(_statusBorder);
    _statusPanel.add(_statusTextPanel, BorderLayout.CENTER);
    
    _statusTextLabel = new JLabel();
    _statusTextLabel.setText(StringLocalizer.keyToString("ATGUI_EMPTY_STATUS_KEY") + ":");
    _statusTextPanel.add(_statusTextLabel);
    _statusTextValue = new JLabel();
    _statusTextValue.setText("System Initialized");
    _statusTextValue.setFont(FontUtil.getBoldFont(_statusTextValue.getFont(), Localization.getLocale()));
    _statusTextPanel.add(_statusTextValue);

//    _xrayStatusPanel = new XrayStatusPanel();
//    _xrayStatusPanel.setBorder(_statusBorder);
//    _xrayStatusPanel.setMinimumSize(new Dimension(100,5));
//    _xrayStatusPanel.setPreferredSize(new Dimension(200,5));
//    _statusPanel.add(_xrayStatusPanel, BorderLayout.EAST);

    _warningPanel = new JPanel();
    _warningPanel.setLayout(new BorderLayout());
    _southPanel.add(_warningPanel, BorderLayout.EAST);

    _warningTextPanel = new JPanel(new PairLayout(10, 0));
    //_warningTextPanel.setBorder(_statusBorder);
    _warningPanel.add(_warningTextPanel, BorderLayout.CENTER);
    
    _warningTextLabel = new JLabel();
    _warningTextLabel.setText(StringLocalizer.keyToString("MMGUI_WARNING_EMPTY_KEY") + ":");
    _warningTextLabel.setFont(FontUtil.getBoldFont(_warningTextLabel.getFont(), Localization.getLocale()));
    _warningTextLabel.setForeground(Color.red);
    _warningTextPanel.add(_warningTextLabel);
    _warningTextValue = new JLabel();
    _warningTextValue.setText("-- ");
    _warningTextValue.setFont(FontUtil.getBoldFont(_warningTextValue.getFont(), Localization.getLocale()));
    _warningTextValue.setForeground(Color.red);
    _warningTextPanel.add(_warningTextValue);
    
    _warningPanel.setVisible(false);
      
    addStatusBar();

    java.awt.Image image = Image5DX.getImage(Image5DX.FRAME_X5000);
    setIconImage(image);
    setTitle(_appName);

    _persistSettings = new MainMenuPersistance();
    _persistSettings = _persistSettings.readSettings();
    _startAs = UserTypeEnum.DEVELOPER;
    _runningAs = _startAs;
    setUserEnvironment(_startAs);
    if (Config.isInfoHandlerEnabled())
    {
      ErrorHandlerEnum.saveFullEnumListToFile();
    }
  }

  /**
   * @author George Booth
   */
  private void setUserEnvironment(UserTypeEnum userType)
  {
    boolean updateActionMenu = true;
    if (userType.equals(UserTypeEnum.ADMINISTRATOR))
    {
      _baseMenuBar.enableMenuItems(userType, updateActionMenu);
      _mainToolBar.setVisible(true);
      _mainToolBar.switchToAXIHome();
    }
    else if (userType.equals(UserTypeEnum.DEVELOPER))
    {
      _baseMenuBar.enableMenuItems(userType, updateActionMenu);
      _mainToolBar.setVisible(true);
      _mainToolBar.switchToAXIHome();
    }
    else if (userType.equals(UserTypeEnum.OPERATOR))
    {
      _baseMenuBar.enableMenuItems(userType, updateActionMenu);
      _mainToolBar.setVisible(false);
      _mainToolBar.switchToTestExec();
    }
    else if (userType.equals(UserTypeEnum.SERVICE_EXPERT))
    {
      _baseMenuBar.enableMenuItems(userType, updateActionMenu);
      _mainToolBar.setVisible(true);
      _mainToolBar.switchToService();
    }
    else if (userType.equals(UserTypeEnum.SERVICE))
    {
      _baseMenuBar.enableMenuItems(userType, updateActionMenu);
      _mainToolBar.setVisible(false);
      _baseMenuBar.serviceUIMenuItem_actionPerformed();
    }
    else
    {
      Assert.expect(false, "No start code for userType = " + userType.getName());
    }
  }

  /**
   * @author George Booth
   */
  boolean switchToAXIHome()
  {
    _runningAs = _startAs;
    setSchedulerState(true);
    return switchToEnvironment(_HOME_ENV, _homeEnvironment, _baseMenuBar);
  }

  /**
   * @author George Booth
   */
  boolean switchToTestDev()
  {
    _runningAs = _startAs;
    _testDevEnvironment.setEnableSelectRegionToggleButton(false);
    _testDevEnvironment.switchToTestDevModeDisplay();
    
    //Janan XCR-3837: Display untestable Area on virtual live CAD
    _testDevEnvironment.setEnableDisplayUntestableAreaToggleButton(true);
	
    if (switchToEnvironment(_TEST_DEV_ENV, _testDevEnvironment, _baseMenuBar))
    {
      setSchedulerState(true);
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * @author George Booth
   */
  boolean switchToTestExec()
  {
    _runningAs = _startAs;
    if (_testDevEnvironment.okToCloseProject())
    {
      setSchedulerState(true);
      return switchToEnvironment(_TEST_EXEC_ENV, _testExecEnvironment, _baseMenuBar);
    }
    else
    {
      return false;
    }
  }

  /**
   * @author George Booth
   */
  boolean switchToConfigure()
  {
    _runningAs = _startAs;
    setSchedulerState(true);
    return switchToEnvironment(_CONFIG_ENV, _configureEnvironment, _baseMenuBar);
  }
  
  /**
   * @author Chin-Seong.Kee
   */
  boolean switchToCadCreator()
  { 
     _baseMenuBar.projectClose(true);
     Project proj = null;
     
     if(_baseMenuBar.userCreateCadSettings() != null)
     {
        _testDevEnvironment.switchToCadCreatorModeDisplay();
        CadCreatorPanel.getInstance().switchToPanelCadPanel();
        _runningAs = _startAs;

        setSchedulerState(false);

        return switchToEnvironment(_CAD_CREATOR_ENV, _cadCreatorEnvironmentPanel, _baseMenuBar);
     }
     return false;
  }

  /**
   * @author Scott Richardson
   * @author sham khang shian
   * @author chin seong kee
   */
  boolean switchToVirtualLive()
  {
    Assert.expect(_virtualLiveEnvironmentPanel != null);
    
    if (_project == null)
    {
      if(_baseMenuBar.userCreateDummyOrLoadSelectedProject() == true)
      {
        _testDevEnvironment.switchToVirtualLiveModeDisplay();
        VirtualLivePanel.getInstance().addPanelCadPanel();
        
        if (isVirtualLiveModeEnabled())
        {
          _runningAs = _startAs;
          setSchedulerState(true);
          return switchToEnvironment(_VIRTUAL_LIVE_ENV, _virtualLiveEnvironmentPanel, _baseMenuBar);
        }
        else
        {
          return false;
        }
      }
    } 
    else
    {
      //Kee Chin Seong - Set the reset for some key boolean in order getting correct settings
      if(_project != null)
        _project.setCheckLatestTestProgram(true);
      
       _testDevEnvironment.switchToVirtualLiveModeDisplay();
        VirtualLivePanel.getInstance().addPanelCadPanel();
        
      if (isVirtualLiveModeEnabled())
      {
        _runningAs = _startAs;
        setSchedulerState(true);
        return switchToEnvironment(_VIRTUAL_LIVE_ENV, _virtualLiveEnvironmentPanel, _baseMenuBar);
      }
      else
      {
        return false;
      }
    }
    
       return false;
  }

  /**
   * @author George Booth
   */
  boolean switchToService()
  {
    _runningAs = UserTypeEnum.SERVICE;
    setSchedulerState(true);
    return switchToEnvironment(_SERVICE_ENV, _serviceEnvironment, _baseMenuBar);
  }

  /**
   * @author George Booth
   */
  boolean switchToEnvironment(String envName, AbstractEnvironmentPanel envPanel,
          JMenuBar menuBar)
  {
    Assert.expect(envName != null);
    Assert.expect(envPanel != null);
    Assert.expect(menuBar != null);

        //sham
    if(_project != null)
      _project.setCheckLatestTestProgram(true);
  
    if (_currentEnvironment != null)
    {
      if (_currentEnvironment.isReadyToFinish())
      {
        _currentEnvironment.finish();
      }
      else
      {
        return false;
      }
    }
    setJMenuBar(menuBar);
    _mainCardLayout.show(_centerPanel, envName);
    _currentEnvironment = envPanel;
    _baseMenuBar.setCommandManager(_currentEnvironment.getCommandManager());
    _serviceEnvironmentNotActive = _currentEnvironment != _serviceEnvironment;
    
    //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
    boolean isCurrentProgrammingEnvironment = false;
    if (_currentEnvironment == _testDevEnvironment || _currentEnvironment == _configureEnvironment 
          || _currentEnvironment == _virtualLiveEnvironmentPanel)
    {
      isCurrentProgrammingEnvironment = true;
    }

	//Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT))
    {
      try
      {
        if (isCurrentProgrammingEnvironment == false && _machineUtilizationReportSystem.isProgramming())
        {
          _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.STOP_PROGRAMMING);
        }
        else if (_machineUtilizationReportSystem.isServicing())
        {
          _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.STOP_SERVICE);
        }
        
        if (isCurrentProgrammingEnvironment && _machineUtilizationReportSystem.isProgramming() == false)
        {
          _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.START_PROGRAMMING);
        }
        else if (_currentEnvironment == _serviceEnvironment && _machineUtilizationReportSystem.isServicing() == false)
        {
          _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.START_SERVICE);
        }
      }
      catch (DatastoreException ex)
      {
        String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_ERROR_MESSAGE_KEY");
        MessageDialog.showErrorDialog(MainMenuGui.this,
                                      errorMsg,
                                      StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_EVENT_TITLE_KEY"),
                                      true,
                                      FontUtil.getMediumFont());
      }
    } 
	
    //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
    {
      try
      {
        if (isCurrentProgrammingEnvironment == false && _vOneMachineStatusMonitoringSystem.isProgramming())
        {
          _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.PROGRAMMING_STOP);
        }
        else if (_vOneMachineStatusMonitoringSystem.isServicing())
        {
          _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.SERVICE_STOP);
        }
        
        if (isCurrentProgrammingEnvironment && _vOneMachineStatusMonitoringSystem.isProgramming() == false)
        {
          _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.PROGRAMMING_START);
        }
        else if (_currentEnvironment == _serviceEnvironment && _vOneMachineStatusMonitoringSystem.isServicing() == false)
        {
          _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.SERVICE_START);
        }
      }
      catch (DatastoreException ex)
      {
        String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_MESSAGE_KEY");
        MessageDialog.showErrorDialog(MainMenuGui.this,
                                      errorMsg,
                                      StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_EVENT_TITLE_KEY"),
                                      true,
                                      FontUtil.getMediumFont());
      }
    }
    
    if (_currentEnvironment == _testExecEnvironment)
    {
      if (_testDevEnvironment.areClosingProject() && (_project.hasBeenModified() == true))
      {
        _baseMenuBar.projectClose(true);
        // Added by Jack Hwee - If user don't want to save the modified project, screen should switch back to AXI home with project is closed.
        _baseMenuBar.removeProjectObservable(true);
        _mainToolBar.switchToAXIHome();
        return false;       
      }
      // menu is disabled, so turn off it's project observer
      _baseMenuBar.removeProjectObservable(true);
    }
    else
    {
      _baseMenuBar.removeProjectObservable(false);
    }

    if (_currentEnvironment == _serviceEnvironment || _currentEnvironment == _testExecEnvironment)
    {
      //Ngie Xing, disable change system magnification button
      _envToolBar.disableChangeSystemMagnificationButton();
      _hardwareObservable.deleteObserver(this);
      _progressObservable.deleteObserver(this);
      _inspectionEventObservable.deleteObserver(this);
      _fringePatternObservable.deleteObserver(this);
    }
    else
    {
      //Ngie Xing, enable change system magnification button
      try
      {
        if (LicenseManager.isOfflineProgramming() == false)
        {
          _envToolBar.enableChangeSystemMagnificationButton();
        }
        else
        {
          if (XrayTester.getSystemType().equals(SystemTypeEnum.S2EX) && Project.isCurrentProjectLoaded())
          {
            _envToolBar.enableChangeSystemMagnificationButton();
          }
        }
      }
      catch (BusinessException e){}
      
      _hardwareObservable.addObserver(this);
      _progressObservable.addObserver(this);
      _inspectionEventObservable.addObserver(this);
      _fringePatternObservable.addObserver(this);
    }

    _currentEnvironment.start();

    if (envName.equalsIgnoreCase(_SERVICE_ENV))
    {
      Interlock.getInstance().setServicePanelActivation(true);
    }
    else
    {
      Interlock.getInstance().setServicePanelActivation(false);
    }
    if (_currentEnvironment==_virtualLiveEnvironmentPanel)
    {
      _envToolBar.disableLoadUnloadButton();
      _baseMenuBar.disableLoadUnloadMenuItem();
    }


    return true;
  }

  /**
   * Puts the frame in the proper position and size, based on the persist settings
   * The persist settings must have already been read in.
   * @author George Booth
   */
  private void placeOnScreen()
  {
    //Position to place where it last was, if no last info, center on screen
    if ((_persistSettings.getFramePosition() == null) || (_persistSettings.getFrameSize() == null))
    {
      Dimension defaultAppSize = new Dimension(1024, 768);
      setSize(defaultAppSize);
      setPreferredSize(defaultAppSize);
      SwingUtils.centerOnScreen(MainMenuGui.this);
    }
    else
    {
      setSize(_persistSettings.getFrameSize());
      setPreferredSize(_persistSettings.getFrameSize());
      setLocation(_persistSettings.getFramePosition());
    }
    makeActive(true);
  }

  /**
   * @author George Booth
   */
  private void writeGuiPersistanceFile()
  {
    if(_persistSettings == null)
      return;
//    Assert.expect(_persistSettings != null);

    _persistSettings.setFramePosition(getLocation());
    _persistSettings.setFrameSize(getSize());

    //other persistance settings are updated with finish() methods
    try
    {
      _persistSettings.writeSettings();
    }
    catch (DatastoreException de)
    {
      JOptionPane.showMessageDialog(
              _instance,
              de.getLocalizedMessage(),
              StringLocalizer.keyToString(new LocalizedString("MMGUI_GUI_NAME_KEY", null)),
              JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * @author George Booth
   */
  public MainMenuPersistance getPersistSettings()
  {
    Assert.expect(_persistSettings != null);
    return _persistSettings;
  }

  /**
   * @param isEnabled 
   */
  public void setSchedulerState(boolean isEnabled)
  {
      if(isEnabled == true)
      {
        if(_alarmClock == null)
        {
            //create instance for schduler
          _alarmClock = new AlarmClock(this);
        }
        //now ,we set athe alarm 
        if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.V810_AUTO_START_UP_ENABLED))
        {
           _alarmClock.scheduleTask(Config.getInstance().getIntValue(SoftwareConfigEnum.V810_AUTO_START_UP_HOUR), 
                                    Config.getInstance().getIntValue(SoftwareConfigEnum.V810_AUTO_START_UP_MINUTES), 0);
        }
      }
      else
      {
        //kill the schduler thread and free the memory.
        if(_alarmClock != null)
        {
            _alarmClock.stop();
            _alarmClock = null;
        }
      }
  }
  
  
  public AlarmClock getSchedulerAlarm()      
  {
    return _alarmClock;
  }
  
  /**
   * @author George Booth
   */
  public void login()
  {
    // prompt for new login
    if (_loginManager.promptForLogIn(_instance))
    {
      _startAs = _loginManager.getUserTypeFromLogIn();
      _runningAs = _startAs;
      
      //Khaw Chek Hau - XCR2654: CAMX Integration
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
      {
        try
        {
          _shopFloorSystem.setLoginUserName(_loginManager.getUserNameFromLogIn());
          _shopFloorSystem.login();
        }
        catch (DatastoreException ex)
        {
          String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("SHOP_FLOOR_ERROR_MESSAGE_KEY_KEY");
          MessageDialog.showErrorDialog(MainMenuGui.this,
                  errorMsg,
                  StringLocalizer.keyToString("SHOP_FLOOR_EVENT_TITLE_KEY"),
                  true);
        }
      }
      
      //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT))
      {
        try
        {
          _machineUtilizationReportSystem.setLoginUserType(UserAccountsManager.getCurrentUserType());  
          _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.LOGIN);
        }
        catch (DatastoreException ex)
        {
          String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_ERROR_MESSAGE_KEY");
          MessageDialog.showErrorDialog(MainMenuGui.this,
                                        errorMsg,
                                        StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_EVENT_TITLE_KEY"),
                                        true,
                                        FontUtil.getMediumFont());
        }
      }
      
      //Khaw Chek Hau - XCR3670: V-ONE AXI latest status monitoring log
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
      {
        try
        {
          _machineLatestStatusLogWriter.writeMachineLatestStatusLog(MachineLatestStatusElementEventEnum.LOGIN_USER_NAME, 
                                                                    _loginManager.getUserNameFromLogIn());
          
          _machineLatestStatusLogWriter.writeMachineLatestStatusLog(MachineLatestStatusElementEventEnum.SOFTWARE_VERSION, 
                                                                    Version.getVersion());
        }
        catch (DatastoreException ex)
        {
          String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_MESSAGE_KEY");
          MessageDialog.showErrorDialog(MainMenuGui.this,
                                        errorMsg,
                                        StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_EVENT_TITLE_KEY"),
                                        true,
                                        FontUtil.getMediumFont());
        }
      }
      
      //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
      _fileWriter.setCurrentUserName(_loginManager.getUserNameFromLogIn());
      
      setSchedulerState(true);
      setUserEnvironment(_startAs);
    }
    else
    {
      // user can exit from login
      makeActive(false);
      
      // user can decide not to exit
      login();
    }
  }

  /**
   * @author George Booth
   */
  public void logout()
  {
    if (_testDevEnvironment.okToCloseProject())
    {
      // project was saved if needed, now close it
      _baseMenuBar.projectClose(true);
    }
    else
    {
      return;
    }

    // clear the undo stacks when a user logs out (test dev is done by the project close above)
    com.axi.v810.gui.undo.CommandManager.getConfigInstance().clear();
    com.axi.v810.gui.undo.CommandManager.getHomeInstance().clear();
    com.axi.v810.gui.undo.CommandManager.getServiceInstance().clear();
    
    //Khaw Chek Hau - XCR2654: CAMX Integration
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
    {
      try
      {
        _shopFloorSystem.softwarePowerOff();
      }
      catch (DatastoreException ex)
      {
        String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("SHOP_FLOOR_ERROR_MESSAGE_KEY_KEY");
        MessageDialog.showErrorDialog(
                MainMenuGui.this,
                errorMsg,
                StringLocalizer.keyToString("SHOP_FLOOR_EVENT_TITLE_KEY"),
                true);
      }
    }

    //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT))
    {
      try
      {
        _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.LOG_OUT);
      }
      catch (DatastoreException ex)
      {
        String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_ERROR_MESSAGE_KEY");
        MessageDialog.showErrorDialog(MainMenuGui.this,
                                      errorMsg,
                                      StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_EVENT_TITLE_KEY"),
                                      true,
                                      FontUtil.getMediumFont());
      }
    }
    
    //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
    {
      try
      {
        _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING);
      }
      catch (DatastoreException ex)
      {
        String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_MESSAGE_KEY");
        MessageDialog.showErrorDialog(MainMenuGui.this,
                                      errorMsg,
                                      StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_EVENT_TITLE_KEY"),
                                      true,
                                      FontUtil.getMediumFont());
      }
    }

    // no current user
    _loginManager.clearLoginInfo();

    // switch to AXI Home for login background
    _mainToolBar.switchToAXIHome();

    //Scheduler stopped 
    setSchedulerState(false);
    
    // prompt for next login or exit
    login();
  }

  /**
   * Return the start up user type
   * @author George Booth
   */
  public UserTypeEnum getStartingUserType()
  {
    return _startAs;
  }

  /**
   * Return the runningAs user type
   * @author George Booth
   */
  public UserTypeEnum getCurrentUserType()
  {
    return _runningAs;
  }

  /**
   * @author George Booth
   */
  public MainUIMenuBar getMainMenuBar()
  {
    Assert.expect(_baseMenuBar != null);

    return _baseMenuBar;
  }

  /**
   * returns the Main UI tool bar
   * @author George Booth
   */
  public MainUIToolBar getMainToolBar()
  {
    Assert.expect(_mainToolBar != null);

    return _mainToolBar;
  }

  /**
   * returns the environment tool bar
   * @author George Booth
   */
  public MainUIToolBar getEnvToolBar()
  {
    Assert.expect(_envToolBar != null);

    return _envToolBar;
  }

  /**
   * returns the empty environment tool bar
   * @author George Booth
   */
  public MainUIToolBar getEmptyToolBar()
  {
    Assert.expect(_emptyToolBar != null);

    return _emptyToolBar;
  }

  /**
   * Adds the common status bar
   * @author George Booth
   */
  public void addStatusBar()
  {
    Assert.expect(_southPanel != null);

    this.getContentPane().add(_southPanel, BorderLayout.SOUTH);
  }

  /**
   * Removes the common status bar
   * @author George Booth
   */
  public void removeStatusBar()
  {
    Assert.expect(_southPanel != null);

    this.getContentPane().remove(_southPanel);
  }

  /**
   * Adds text to status bar
   * @author George Booth
   */
  public void setStatusBarText(String message)
  {
    Assert.expect(message != null);
    // a zero length message will cause the status bar to disappear
    if (message.length() == 0)
    {
      message = " ";
    }
    _statusTextValue.setText(message);
  }
  
  /**
   * Adds text to warning bar
   * @author George Booth
   */
  public void setWarningBarText(String message)
  {
    Assert.expect(message != null);
    // a zero length message will cause the status bar to disappear
    if (message.length() == 0)
    {
      message = " ";
      _warningPanel.setVisible(false);
    }
    else
      _warningPanel.setVisible(true);
    
    message = message + " ";
    _warningTextValue.setText(message);
  }

  /**
   * @author George Booth
   * Kok Chun, Tan - XCR-3088 - make sure only one thread can enter here at the same time.
   */
  public synchronized void closeAllDialogs()
  {
    developerDebug("closeAutoAdjustProgressDialog()");

    // Close the progress dialog if it is not null
    // The bracketing event that normally closes the dialog may not be seen
    // if an exception is thrown
    closeProgressDialog(_startupProgressCancelDialog, true);
    closeProgressDialog(_autoAdjustProgressDialog, true);
    closeProgressDialog(_verificationGenerationProgressDialog, true);
    closeProgressDialog(_focusSetGenerationProgressDialog, true);
    closeProgressDialog(_offlineInspectionImageGenerationProgressDialog, true);
    closeProgressDialog(_alignmentProgressDialog, true);
    closeProgressDialog(_xRaySourcePowerOffProgressDialog, true); // XCR1144, Chnee Khang Wah, 29-dec-2010
    closeProgressDialog(_virtualLiveImageGenerationProgressDialog, true);
    closeProgressDialog(_alignmentSurfaceMapProgressDialog, true);
    closeProgressDialog(_surfaceMapProgressDialog, true);

    _startupProgressCancelDialog = null;
    _autoAdjustProgressDialog = null;
    _verificationGenerationProgressDialog = null;
    _focusSetGenerationProgressDialog = null;
    _offlineInspectionImageGenerationProgressDialog = null;
    _alignmentProgressDialog = null;
    _xRaySourcePowerOffProgressDialog = null; // XCR1144, Chnee Khang Wah, 29-dec-2010
    _virtualLiveImageGenerationProgressDialog = null;
    _alignmentSurfaceMapProgressDialog = null;
    _surfaceMapProgressDialog = null;

    closeBusyCancelDialog(_shutdownBusyDialog, true);
    closeBusyCancelDialog(_panelLoadBusyCancelDialog, true);
    closeBusyCancelDialog(_panelUnloadBusyDialog, true);
    closeBusyCancelDialog(_homeRailsBusyDialog, true);
    closeBusyCancelDialog(_cameraConnectionBusyDialog, true);

    _shutdownBusyDialog = null;
    _panelLoadBusyCancelDialog = null;
    _panelUnloadBusyDialog = null;
    _homeRailsBusyDialog = null;
    _cameraConnectionBusyDialog = null;
    
    _systemStartupInProgress = false;
  }

  /**
   * @author Andy Mechtenberg
   */
//  public void
  /**
   * @author George Booth
   */
  public void handleXrayTesterException(final XrayTesterException xrayTesterException)
  {
    if (SwingUtilities.isEventDispatchThread() == false)
    {
      SwingUtils.invokeLater(new Runnable()
      {

        public void run()
        {
          developerDebug("call handleXrayTesterException() on swing thread");
          handleXrayTesterException(xrayTesterException);
        }
      });
    }
    else
    {
      developerDebug("handleXrayTesterException()");

      // Close the progress dialog if it is not null
      // The bracketing event that normally closes the dialog may not be seen
      // if an exception is thrown
//      closeProgressDialog(_autoAdjustProgressDialog, false);
//      _autoAdjustProgressDialog = null;
      closeAllDialogs();
      
      Interlock.getInstance().setServicePanelActivation(true);
      
      // don't show exceptions if Service UI is active - it will handle them itself
      if (_serviceEnvironmentNotActive)
      {
        developerDebug("Showing xrayTesterException");
        MessageDialog.showErrorDialog(
                MainMenuGui.this,
                xrayTesterException.getMessage(),
                StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                true);
        changeLightStackToStandby();
      }
      else
      {
        developerDebug("Ignoring xrayTesterException with Service UI active");
      }

      //generate image failure due to grayscale adjustment fail will enter the following exception
      if(xrayTesterException instanceof PanelInterferesWithAdjustmentAndConfirmationTaskException)
        Assert.expectWithErrorPrompt(false, ErrorHandlerEnum.HW_COMM_SET_PIP_HWL_CONFIRM_PANEL_INSIDE);

      // XCR1144, Chnee Khang Wah, 10-Jan-2011
      if (xrayTesterException instanceof PanelPositionerException)
      {
        LocalizedString text = xrayTesterException.getLocalizedString();

        if (text.equals(PanelPositionerExceptionEnum.getLocalizedString(PanelPositionerExceptionEnum.POSSIBLE_PANEL_INSIDE_MACHINE)) ||
            text.equals(PanelPositionerExceptionEnum.getLocalizedString(PanelPositionerExceptionEnum.POSSIBLE_PANEL_LOAD_UNSUCCESFUL)))
        {
          int answerValue = ChoiceInputDialog.showConfirmDialog(MainMenuGui.getInstance(),
                  StringLocalizer.keyToString("HW_PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_AND_ASK_FOR_BOARD_STATUS_KEY"),
                  StringLocalizer.keyToString("MMGUI_START_UP_NOTIFICATION_TITLE_KEY"),
                  FontUtil.getMediumFont());

          if (answerValue == JOptionPane.YES_OPTION)
          {
            //Release front panel magnet
            try
            {
              _digitalIo.turnFrontPanelLeftDoorLockOff();
              _digitalIo.turnFrontPanelRightDoorLockOff();
            }
            catch (XrayTesterException xte)
            {
              //do nothing
            }

            String message = StringLocalizer.keyToString("HW_PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_AND_ASK_FOR_REMOVE_AND_RESTART_KEY");
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                    message,
                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                    JOptionPane.ERROR_MESSAGE);
          }
          else
          {
            if(text.equals(PanelPositionerExceptionEnum.getLocalizedString(PanelPositionerExceptionEnum.POSSIBLE_PANEL_INSIDE_MACHINE)))
              _digitalIo.setByPassPanelClearSensor(true);
            
            if(text.equals(PanelPositionerExceptionEnum.getLocalizedString(PanelPositionerExceptionEnum.POSSIBLE_PANEL_LOAD_UNSUCCESFUL)))
              _panelHandler.resetPanelLoadingStatus();
          }
        }
        else if (text.equals(PanelPositionerExceptionEnum.getLocalizedString(PanelPositionerExceptionEnum.PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_DURING_LOAD)))
        {
          //Once detect Exception,
          //1. shut down the x-ray source.
          _hardwareWorkerThread2.invokeLater(new Runnable()
          {

            public void run()
            {
              try
              {
                _hTubeXraySource.powerOff();
              }
              catch (XrayTesterException ex)
              {
                //do nothing
              }

              //Release front panel magnet
              try
              {
//                _interlock.shutdown();
                _digitalIo.turnFrontPanelLeftDoorLockOff();
                _digitalIo.turnFrontPanelRightDoorLockOff();
              }
              catch (XrayTesterException xte)
              {
                //do nothing
              }
            }
          });

          //2. Assume board is already drop into machine.
          _isPersistentPanelPresent = true;

          //3. Make sure user open top hatch (broke the interlock).
          _interlockPollingThread.invokeLater(new Runnable()
          {

            public void run()
            {
              while (_isPersistentPanelPresent == true)
              {
                try
                {
                  if (_interlock.isInterlockOpen() == true)
                  {
                    _isPersistentPanelPresent = false;
                  }
                }
                catch (XrayTesterException ex)
                {
                  //do nothing
                }
              }
            }
          });

          //4.  The following message will only clear after the interlock have been broke
          while (_isPersistentPanelPresent == true)
          {
            String message = StringLocalizer.keyToString("HW_PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_EXCEPTION_AND_WAIT_FOR_REMOVE_KEY");
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                    message,
                    StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                    JOptionPane.ERROR_MESSAGE);
          }
        }
      }
      else if(xrayTesterException instanceof PanelInterferesWithAdjustmentAndConfirmationTaskException)
      {
        closeAllDialogs();

          // its time for the system to run grayscale and there is a panel in the system. so lets prompt the user to
        // unload the board after unload we will reload the panel and then start the image collection.
        // prompt the user to unload
        //               String message = "In order to generate images, the system needs to perform a grayscale adjustment. " +
        //                                "To complete this adjustment, the panel needs to be unloaded. Would you like to continue " +
        //                                "and unload the panel?";
        String message = StringLocalizer.keyToString("MMGUI_STARTUP_ADJUSTMENT_NEEDS_PANEL_REMOVED_KEY");

        int response = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
          StringUtil.format(message, 50),
          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
          JOptionPane.YES_NO_OPTION,
          JOptionPane.ERROR_MESSAGE);

        if (response == JOptionPane.YES_OPTION)
        {
          try
          {
            // unload the panel
            unloadPanel();

            // now that the panel is unloaded, do a startup to ensure all calibrations/adjustments are run
            startXrayTester();

//              // if a startup is still needed, don't try to load -- it's probably been canceled out
//              if (XrayTester.getInstance().isStartupRequired() == false)
//              {
//                // reload the panel
//                _mainUI.loadPanel();
//              }
            // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
            // now check to verify that the user has not cancelled the load of the panel.
            if (PanelHandler.getInstance().isPanelLoaded())
            {
              // re-run startup
              startXrayTester();
              return;
            }
            else
            {
              // return out do not complete operation.
              return;
            }

          }
          catch (XrayTesterException ex)
          {
            handleXrayTesterException(ex);
          }
        }
        else
        {
          // the user does not want to unload. Therefore we will just exit the operation
          return;
        }
        return;
      }
      
      else if (xrayTesterException.getLocalizedMessage().indexOf(
        StringLocalizer.keyToString("HW_XRAY_SOURCE_HARDWARE_REPORTED_INTERLOCK_1_OFF_ERROR_KEY")) != -1)
      {
        developerDebug("Showing interlock xrayTesterException");
        // XCR-3773 System crash when interlock break
        String message = xrayTesterException.getLocalizedMessage() + "\n\n"
          + StringLocalizer.keyToString("MMGUI_CONTINUE_STARTUP_MESSAGE_KEY");
        String formattedLocalMessage = StringUtil.format(message, _CHARS_PER_LINE);
        Object[] options = new Object[2];
        options[0] = StringLocalizer.keyToString("MMGUI_CONTINUE_STARTUP_BUTTON_KEY");
        options[1] = StringLocalizer.keyToString("MMGUI_CANCEL_STARTUP_BUTTON_KEY");
        int answer = JOptionPane.showOptionDialog(
          _instance,
          formattedLocalMessage,
          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
          JOptionPane.NO_OPTION,
          JOptionPane.ERROR_MESSAGE,
          null,
          options,
          options[0]);
        if (answer == JOptionPane.YES_OPTION)
        {
          if (_systemStartupInProgress)
          {
            // allow retry from startup()
            developerDebug("User requested continue startup");
            _userRequestedRetryStartup = true;
          }
          else
          {
            // restart it
            developerDebug("User requested restart startup");
            try
            {
              startXrayTester();
            }
            catch (XrayTesterException ext)
            {
              _xrayTesterException = ext;
            }
          }
        }
        else
        {
          developerDebug("User canceled startup");
        }
      }
      else
      {
        _xrayTesterException = xrayTesterException;
      }
      // XCR1144 - end
      if(XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
      {
        if (xrayTesterException instanceof PanelHandlerException)
        {
          PanelHandlerException panelHandlerException = (PanelHandlerException) xrayTesterException;
          if(panelHandlerException.getExceptionType().equals(PanelHandlerExceptionEnum.PANEL_COMPONENT_HEIGHT_EXCEED_MAX_SAFETY_LEVEL))
          {
            try
            {
              unloadPanel();
            }
            catch(XrayTesterException xrayTesterException2)
            {
              handleXrayTesterException(xrayTesterException2);
            }
          }
        }
      }
      Interlock.getInstance().setServicePanelActivation(false);
    }
  }
  
  public void handleLicenseGeneralException(final XrayTesterException e)
  {
    finishLicenseMonitor();
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                e.getMessage(),
                StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                true);
        MainMenuGui.getInstance().exitV810(false);
      }
    });
  }

  /**
   * Puts the lightstack to yellow
   * @author Andy Mechtenberg
   */
  public void changeLightStackToStandby()
  {
    _hardwareWorkerThread.invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          if ((_digitalIo != null) && (_lightStack != null))
          {
            if (_digitalIo.isStartupRequired() == false)
            {
              _lightStack.standyMode();
            }
          }
        }
        catch (final XrayTesterException ex)
        {
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              MessageDialog.showErrorDialog(
                      MainMenuGui.this,
                      ex.getMessage(),
                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                      true);
            }
          });
        }
      }
    });
  }

  /**
   * @author George Booth
   */
  private void createShutdownBusyDialog()
  {
    developerDebug("openShutdownBusyDialog()");

    _shutdownBusyDialog = new BusyCancelDialog(
            _instance,
            StringLocalizer.keyToString("MMGUI_SHUT_DOWN_BUSY_MESSAGE_KEY"),
            StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
            true);

    _shutdownBusyDialog.pack();
    SwingUtils.centerOnComponent(_shutdownBusyDialog, _instance);
  }

  /**
   * @author George Booth
   */
  private void createPanelLoadBusyCancelDialog()
  {
    developerDebug("openPanelLoadBusyCancelDialog()");
    _panelLoadWasCanceled = false;

    _panelLoadBusyCancelDialog = new BusyCancelDialog(
            _instance,
            StringLocalizer.keyToString("MMGUI_PANEL_LOAD_BUSY_MESSAGE_KEY"),
            StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
            StringLocalizer.keyToString("MMGUI_PANEL_LOAD_CANCEL_MESSAGE_KEY"),
            true);

    _panelLoadBusyCancelDialog.addCancelActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        developerDebug("Panel load was canceled");
        _panelLoadWasCanceled = true;
        hardwareAbort();
      }
    });

    _panelLoadBusyCancelDialog.pack();
    SwingUtils.centerOnComponent(_panelLoadBusyCancelDialog, _instance);
  }

  /**
   * @author George Booth
   */
  private void createPanelUnloadBusyDialog()
  {
    developerDebug("openPanelUnloadBusyDialog()");

    _panelUnloadBusyDialog = new BusyCancelDialog(
            _instance,
            StringLocalizer.keyToString("MMGUI_PANEL_UNLOAD_BUSY_MESSAGE_KEY"),
            StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
            true);

    _panelUnloadBusyDialog.pack();
    SwingUtils.centerOnComponent(_panelUnloadBusyDialog, _instance);
  }
  
  /**
   * @author Swee Yee Wong
   */
  private void createCameraConnectionBusyDialog()
  {
    developerDebug("cameraConnectionBusyDialog()");

    _cameraConnectionBusyDialog = new BusyCancelDialog(
            _instance,
            StringLocalizer.keyToString("MMGUI_CAMERA_CONNECTION_BUSY_MESSAGE_KEY"),
            StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
            true);

    _cameraConnectionBusyDialog.pack();
    SwingUtils.centerOnComponent(_cameraConnectionBusyDialog, _instance);
  }

  /**
   * @author George Booth
   */
  private void createHomeRailsBusyDialog()
  {
    developerDebug("openHomeRailsBusyDialog()");

    _homeRailsBusyDialog = new BusyCancelDialog(
            _instance,
            StringLocalizer.keyToString("MMGUI_HOME_RAILS_BUSY_MESSAGE_KEY"),
            StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
            true);

    _homeRailsBusyDialog.pack();
    SwingUtils.centerOnComponent(_homeRailsBusyDialog, _instance);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void createAlignmentProgressCancelDialog()
  {
    developerDebug("createAlignmentProgressCancelDialog()");

    _alignmentProgressDialog = new ProgressDialog(
            this,
            StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
            StringLocalizer.keyToString("MMGUI_RUNTIME_ALIGNMENT_KEY"),
            StringLocalizer.keyToString("MMGUI_RUNTIME_ALIGNMENT_KEY"),
            StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
            //      StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
            StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
            0, 100, true);


    // if using a progress cancel, add this code in.  Right now, alignment is not cancellable,
    // so we're using a progress dialog with no cancel

//    _alignmentProgressDialog.addCancelActionListener(new java.awt.event.ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        developerDebug("Cancelling Runtime Alignment");
//        String reporterAction = StringUtil.format(StringLocalizer.keyToString("TESTEXEC_GUI_TEST_ABORTING_KEY"), 50);
//        _alignmentProgressDialog.updateProgressBar(_lastPercentDone, reporterAction);
//        hardwareAbort();
////        closeProgressDialog(_alignmentProgressDialog, true);
//      }
//    });

    _alignmentProgressDialog.pack();
    SwingUtils.centerOnComponent(_alignmentProgressDialog, _instance);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void createSurfaceMapProgressCancelDialog()
  {
    developerDebug("createSurfaceMapProgressCancelDialog()");
    
    _surfaceMapProgressDialog = new ProgressDialog(
            this,
            StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
            StringLocalizer.keyToString("MMGUI_RUNTIME_SURFACE_MAP_KEY"),
            StringLocalizer.keyToString("MMGUI_RUNTIME_SURFACE_MAP_KEY"),
            StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
            //      StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
            StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
            0, 100, true);
    
    _surfaceMapProgressDialog.pack();
    SwingUtils.centerOnComponent(_surfaceMapProgressDialog, _instance);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void createAlignmentSurfaceMapProgressCancelDialog()
  {
    developerDebug("createAlignmentSurfaceMapProgressCancelDialog()");
    
    _alignmentSurfaceMapProgressDialog = new ProgressDialog(
            this,
            StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
            StringLocalizer.keyToString("MMGUI_RUNTIME_ALIGNMENT_SURFACE_MAP_KEY"),
            StringLocalizer.keyToString("MMGUI_RUNTIME_ALIGNMENT_SURFACE_MAP_KEY"),
            StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
            //      StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
            StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
            0, 100, true);
    
    _alignmentSurfaceMapProgressDialog.pack();
    SwingUtils.centerOnComponent(_alignmentSurfaceMapProgressDialog, _instance);
  }

  // XCR1144, Chnee Khang Wah, 29-dec-2010
  /**
   * @author Chnee Khang Wah
   */
  private void createxRaySourcePowerOffProgressDialog()
  {
    developerDebug("createxRaySourcePowerOffProgressDialog()");

    _xRaySourcePowerOffProgressDialog = new ProgressDialog(
            this,
            StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
            StringLocalizer.keyToString("HW_XRAY_SOURCE_POWER_OFF_PROGRESS_REPORTER_KEY"),
            StringLocalizer.keyToString("HW_XRAY_SOURCE_POWER_OFF_PROGRESS_REPORTER_KEY"),
            StringLocalizer.keyToString("MMGUI_START_UP_CLOSE_MESSAGE_KEY"),
            StringLocalizer.keyToString("GUI_PERCENT_COMPLETE_KEY"),
            0, 100, true);

    _xRaySourcePowerOffProgressDialog.pack();
    SwingUtils.centerOnComponent(_xRaySourcePowerOffProgressDialog, _instance);
  }

  /**
   * @author George Booth
   */
  private void closeBusyCancelDialog(BusyCancelDialog dialog, boolean force)
  {
    developerDebug("closeBusyCancelDialog()");
    if (dialog != null)
    {
      // wait until visible if not forced
      if (force == false)
      {
        while (dialog.isVisible() == false)
        {
          try
          {
            Thread.sleep(200);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }
        }
      }
      dialog.dispose();
      dialog = null;
    }
  }

  /**
   * @author George Booth
   */
  private void createStartupProgressCancelDialog()
  {
    developerDebug("createStartupProgressCancelDialog()");

    _systemStartupWasCanceled = false;

    _startupProgressCancelDialog = new ProgressDialog(
            this,
            StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
            StringLocalizer.keyToString("MMGUI_START_UP_BUSY_MESSAGE_KEY"),
            StringLocalizer.keyToString("MMGUI_START_UP_RUNNING_MESSAGE_KEY"),
            StringLocalizer.keyToString("MMGUI_START_UP_CLOSE_MESSAGE_KEY"),
            StringLocalizer.keyToString("MMGUI_START_UP_CANCEL_MESSAGE_KEY"),
            StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
            0, 100, true);

    _startupProgressCancelDialog.addCancelActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
//        JButton cancelButton = (JButton)e.getSource();
//        cancelButton.setEnabled(false);
        developerDebug("System startup was canceled");
        if (_startupProgressCancelDialog != null)
        {
          String reporterAction = StringLocalizer.keyToString("MMGUI_STARTUP_BEING_CANCELED_BUTTON_KEY");
          _startupProgressCancelDialog.updateProgressBarPrecent(_lastPercentDone, reporterAction);
        }

        _systemStartupWasCanceled = true;
        hardwareAbort();
      }
    });

    SwingUtils.setFont(_startupProgressCancelDialog, _dialogFont);
    _startupProgressCancelDialog.pack();
    SwingUtils.centerOnComponent(_startupProgressCancelDialog, _instance);
  }

  /**
   * @author George Booth
   */
  private void createAutoStartupProgressDialog()
  {
    developerDebug("createAutoStartupProgressDialog()");

    //Only create a new dialog box if the old one was nulled out
    //This avoids orphaning a dialog box
    if (_autoStartupProgressDialog == null)
    {
      _autoStartupProgressDialog = new ProgressDialog(
              this,
              StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
              StringLocalizer.keyToString("MMGUI_START_UP_BUSY_MESSAGE_KEY"),
              StringLocalizer.keyToString("MMGUI_START_UP_RUNNING_MESSAGE_KEY"),
              StringLocalizer.keyToString("MMGUI_START_UP_CLOSE_MESSAGE_KEY"),
              StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
              0, 100, true);

      SwingUtils.setFont(_autoStartupProgressDialog, _dialogFont);
      _autoStartupProgressDialog.pack();
      SwingUtils.centerOnComponent(_autoStartupProgressDialog, _instance);
    }
  }

  /**
   * @author George Booth
   */
  private void createAutoAdjustProgressDialog()
  {
    developerDebug("createAutoAdjustProgressDialog()");

    //Only create a new dialog box if the old one was nulled out
    //This avoids orphaning a dialog box
    if (_autoAdjustProgressDialog == null)
    {
      _autoAdjustProgressDialog = new ProgressDialog(
              this,
              StringLocalizer.keyToString("MMGUI_HARDWARE_BUSY_TITLE_KEY"),
              StringLocalizer.keyToString("MMGUI_AUTO_ADJUST_BUSY_MESSAGE_KEY"),
              StringLocalizer.keyToString("MMGUI_AUTO_ADJUST_RUNNING_MESSAGE_KEY"),
              StringLocalizer.keyToString("MMGUI_AUTO_ADJUST_CLOSE_MESSAGE_KEY"),
              StringLocalizer.keyToString("MMGUI_AUTO_ADJUST_PERCENT_COMPLETE_MESSAGE_KEY"),
              0, 100, true);

      SwingUtils.setFont(_autoAdjustProgressDialog, _dialogFont);
      _autoAdjustProgressDialog.pack();
      SwingUtils.centerOnComponent(_autoAdjustProgressDialog, _instance);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void createOfflineInspectionImageGenerationProgressDialog()
  {
    developerDebug("openOfflineInspectionImageGenerationProgressDialog");
    String userMessage;
    if (XrayTester.getInstance().isSimulationModeOn())
    {
      userMessage = "Generating FAKE Inspection Images..."; // not localized because it's only for sim mode
    }
    else
    {
      userMessage = StringLocalizer.keyToString("GISK_PROGRESS_BAR_TITLE_KEY") + " " + _project.getName() + "..";
    }

    _offlineInspectionImageGenerationProgressDialog = new ProgressDialog(this,
            StringLocalizer.keyToString("GISWK_TITLE_KEY"),
            userMessage,
            StringLocalizer.keyToString("GISK_PROGRESS_BAR_CANCEL_TEXT_KEY"),
            StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
            StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY") + " ",
            StringLocalizer.keyToString("GISK_SAVING_STATUS_STRING_TEXT_KEY"),
            0,
            _totalOfflineInspectionImages,
            true);


    _offlineInspectionImageGenerationProgressDialog.addCancelActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        developerDebug("Cancelling Verification Image Generation");
        String reporterAction = StringUtil.format(StringLocalizer.keyToString("GISWK_GEN_CANCEL_MESSAGE_KEY"), 50);
        _offlineInspectionImageGenerationProgressDialog.updateProgressBar(_currentOfflineInspectionImages, reporterAction);
        hardwareAbort();
//        closeProgressDialog(_offlineInspectionImageGenerationProgressDialog, true);
      }
    });

    _offlineInspectionImageGenerationProgressDialog.pack();
    _offlineInspectionImageGenerationProgressDialog.setSize(new Dimension(500, 175));
    SwingUtils.centerOnComponent(_offlineInspectionImageGenerationProgressDialog, _offlineInspectionImageDialogParent);
    _currentOfflineInspectionImages = 0;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void createVerificationGenerationProgressDialog()
  {
    developerDebug("openVerificationGenerationProgressDialog");
    String userMessage;
    if (XrayTester.getInstance().isSimulationModeOn())
    {
      userMessage = "Generating FAKE Verification Images...";  // not localized because it's only for sim mode
    }
    else
    {
      userMessage = StringLocalizer.keyToString("MMGUI_ALIGN_GENERATING_VERIFICATION_IMAGES_KEY");
    }

    _verificationGenerationProgressDialog = new ProgressDialog(
            this,
            StringLocalizer.keyToString("MMGUI_GENERATE_VERIFICATION_IMAGE_TITLE_KEY"),
            userMessage,
            StringLocalizer.keyToString("MMGUI_VERFICIATION_IMAGES_GENERATED_KEY"),
            StringLocalizer.keyToString("MMGUI_VERIFICATION_IMAGES_COMPLETE_KEY"),
            StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
            StringLocalizer.keyToString("GUI_PERCENT_COMPLETE_KEY"),
            0, 100, true);

    _verificationGenerationProgressDialog.addCancelActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        developerDebug("Cancelling Verification Image Generation");
        String reporterAction = StringLocalizer.keyToString("GUI_VERIFICATION_CANCELLING_KEY");
        _verificationGenerationProgressDialog.updateProgressBarPrecent(_lastPercentDone, reporterAction);

        hardwareAbort();
      }
    });

    _verificationGenerationProgressDialog.pack();
    SwingUtils.centerOnComponent(_verificationGenerationProgressDialog, _instance);
    _currentVerificationImages = 0;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void createFocusSetGenerationProgressDialog()
  {
    developerDebug("openFocusSetGenerationProgressDialog()");
    _focusSetGenerationProgressDialog = new ProgressDialog(
            this,
            StringLocalizer.keyToString("GUI_FOCUS_GENERATE_FOCUS_SET_TITLE_KEY"),
            StringLocalizer.keyToString("GUI_FOCUS_GENERATING_FOCUS_SET_KEY"),
            StringLocalizer.keyToString("GUI_FOCUS_SET_GENERATED_KEY"),
            StringLocalizer.keyToString("MMGUI_AUTO_ADJUST_CLOSE_MESSAGE_KEY"),
            StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
            StringLocalizer.keyToString("GUI_PERCENT_COMPLETE_KEY"),
            0, 100, true);

    _focusSetGenerationProgressDialog.addCancelActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        developerDebug("Cancelling Focus Set Generation");
        String reporterAction = StringLocalizer.keyToString("GUI_FOCUS_CANCELLING_FOCUS_SET_KEY");
        _focusSetGenerationProgressDialog.updateProgressBarPrecent(_lastPercentDone, reporterAction);

        hardwareAbort();
      }
    });

    _focusSetGenerationProgressDialog.pack();
    SwingUtils.centerOnComponent(_focusSetGenerationProgressDialog, _instance);
    _currentFocusImages = 0;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void closeImageGenerationProgressDialog()
  {
    closeProgressDialog(_offlineInspectionImageGenerationProgressDialog, true);
  }

  /**
   * @author George Booth
   */
  private synchronized void closeProgressDialog(ProgressDialog dialog, boolean force)
  {
    developerDebug("closeProgressDialog()");
    if (dialog != null)
    {
      // wait until visible unless forced
      if (force == false)
      {
        while (dialog.isVisible() == false)
        {
          try
          {
            Thread.sleep(200);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }
        }
      }
      dialog.dispose();
//      if (dialog == _startupProgressCancelDialog ||
//          dialog == _autoAdjustProgressDialog)
//      {
//        developerDebug("delete observer _progressObservable");
//        _progressObservable.deleteObserver(this);
//      }
//      if (dialog == _focusSetGenerationProgressDialog ||
//          dialog == _offlineInspectionImageGenerationProgressDialog)
//      {
//        developerDebug("delete observer _imageManagerObservable");
//        _imageManagerObservable.deleteObserver(this);
//        _imageGenerationObservable.deleteObserver(this);
//      }
//      if (dialog == _verificationGenerationProgressDialog)
//      {
//        developerDebug("delete observer _imageGenerationObservable");
//        _imageGenerationObservable.deleteObserver(this);
//      }
    }
  }

  /**
   * Calling into hardware layer to have the hardware stop what they are doing
   * and return system to a known good state.
   * @author Rex Shang
   */
  private void hardwareAbort()
  {
    try
    {
      _testExecution.abort();
    }
    catch (XrayTesterException e)
    {
      handleXrayTesterException(e);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public void performStartupIfNeeded() throws XrayTesterException
  {
    // we need these observers to make the startup work (progress dialog updates)
    // but ONLY when doing the startup, not all the time like for the other environments
    if (_currentEnvironment == _serviceEnvironment || _currentEnvironment == _testExecEnvironment)
    {
      _hardwareObservable.addObserver(this);
      _progressObservable.addObserver(this);
      _fringePatternObservable.addObserver(this);
    }

    try
    {
      startXrayTester();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    finally
    {
      if (_currentEnvironment == _serviceEnvironment || _currentEnvironment == _testExecEnvironment)
      {
        _hardwareObservable.deleteObserver(this);
        _progressObservable.deleteObserver(this);
        _fringePatternObservable.deleteObserver(this);
      }
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public void performStartupIfNeeded(Font font) throws XrayTesterException
  {
    Assert.expect(font != null);
    Font originalFont = _dialogFont;
    _dialogFont = font;
    try
    {
      performStartupIfNeeded();
    }
    finally
    {
      _dialogFont = originalFont;
    }
  }
  
   /**
   * @author Erica Wheatcroft
   */
  public void performStartupIfNeededForSurfaceMapping() throws XrayTesterException
  {
      try
      {        
          if (XrayTester.getInstance().isStartupRequired())
          {
            return;
          }
          performStartupIfNeeded();
      }
      catch (final XrayTesterException xte)
      {
          handleXrayTesterException(xte);
          return;
      }
  }

  /**
   * @author Andy Mechtenberg
   */
//  private void addDialogToQueue(JDialog dialog)
//  {
//    Assert.expect(dialog !=  null);
//    Assert.expect(SwingUtilities.isEventDispatchThread());
//    if (_dialogs.isEmpty())
//    {
//      _dialogs.add(dialog);
//    }
//    else
//    {
//      // something in -- assumption is it's visible
//      // so remove it, add in the new one and only then setVisible to false
//      try
//      {
//        final JDialog visibleDialog = _dialogs.take();
//        _dialogs.put(dialog);
//        visibleDialog.dispose();
//      }
//      catch (InterruptedException ex)
//      {
//        Assert.expect(false);
//      }
//    }
//  }
  /**
   * @author Andy Mechtenberg
   */
  public void generateOfflineInspectionImages(java.awt.Component parent, final ImageSetData imageSetData) throws XrayTesterException
  {
    Assert.expect(parent != null);
    Assert.expect(imageSetData != null);
    _offlineInspectionImageDialogParent = parent;
    final TestProgram testProgram = getTestProgramWithReason(StringLocalizer.keyToString("MMGUI_ALIGNMENT_NEEDS_TEST_PROGRAM_KEY"),
            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
            _project);

    if (UnitTest.unitTesting() == false)
    {
      Date date = new Date();
      System.out.println("Date: " + date);
    }

    _totalOfflineInspectionImages = _imageManager.getNumberOfInspectionImagesToSave(imageSetData, testProgram);

    _imageGenerationObservable.addObserver(this);
    _imageManagerObservable.addObserver(this);

    final GuiWaitUtil waitUtil = new GuiWaitUtil();
    
    //Swee-Yee.Wong - XCR-3157
    //IRP error message is prompted if run alignment/fine tuning without run full cdna and without unload the board 1st
    if(XrayTester.isS2EXEnabled())
    {
      if (_systemStatusPersistance!= null)
      {
        if(_systemStatusPersistance.isSystemMagnificationReconfigured())
          performStartupIfNeeded();
      }
    }

    // generate verification images on a separate thread
    _xrayTesterException = null;
    HardwareWorkerThread.getInstance().invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          if (imageSetData.getImageSetCollectionTypeEnum().equals(ImageSetCollectionTypeEnum.PRECISION))
          {
            _testExecution.testPanelForPrecisionTuningImageAcquisition(testProgram, imageSetData);
          }
          else
          {
            _testExecution.acquireOfflineImages(testProgram, imageSetData);
          }
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              if (_testExecution.didUserAbort())
              {
                closeAllDialogs();
              }
              else
              {
                GuiObservable.getInstance().stateChanged(imageSetData, ImageSetEventEnum.IMAGE_SET_CREATED);
              }
            }
          });
        }
        catch (final XrayTesterException ex)
        {
          _xrayTesterException = ex;
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              closeAllDialogs();
            }
          });
        }
        finally
        {
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              waitUtil.clearWhenReady();
            }
          });
        }
      }
    });

    waitUtil.wait(this);
    _imageManagerObservable.deleteObserver(this);
    _imageGenerationObservable.deleteObserver(this);

    if (_xrayTesterException != null)
    {
      throw _xrayTesterException;
    }
  }

  /**
   * @author Yong Sheng Chuan
   */
  public void generateImagesForExposureLearning(java.awt.Component parent, final TestProgram testProgram, final ImageSetData imageSetData) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(imageSetData != null);
    
    Assert.expect(parent != null);
    
    _offlineInspectionImageDialogParent = parent;
    
    if (UnitTest.unitTesting() == false)
    {
      Date date = new Date();
      System.out.println("Date: " + date);
    }

    _totalOfflineInspectionImages = _imageManager.getNumberOfInspectionImagesToSave(imageSetData, testProgram);

    _imageGenerationObservable.addObserver(this);
    _imageManagerObservable.addObserver(this);

    final GuiWaitUtil waitUtil = new GuiWaitUtil();
    
    //Swee-Yee.Wong - XCR-3157
    //IRP error message is prompted if run alignment/fine tuning without run full cdna and without unload the board 1st
    if(XrayTester.isS2EXEnabled())
    {
      if(_systemStatusPersistance.isSystemMagnificationReconfigured())
        performStartupIfNeeded();
    }

    // generate verification images on a separate thread
    _xrayTesterException = null;
    HardwareWorkerThread.getInstance().invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          _testExecution.acquireOfflineImages(testProgram, imageSetData);
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              closeAllDialogs();
              if (_testExecution.didUserAbort() == false)
                GuiObservable.getInstance().stateChanged(imageSetData, ImageSetEventEnum.IMAGE_SET_CREATED);
            }
          });
        }
        catch (final XrayTesterException ex)
        {
          _xrayTesterException = ex;
        }
        finally
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              closeAllDialogs();
              waitUtil.clearWhenReady();
            }
          });
        }
      }
    });

    waitUtil.wait(this);
    _imageManagerObservable.deleteObserver(this);
    _imageGenerationObservable.deleteObserver(this);

    if (_xrayTesterException != null)
    {
      throw _xrayTesterException;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void generateVerificationImagesForManualAlignment(final TestSubProgram subProgram, final boolean useRFP) throws XrayTesterException
  {
    if (UnitTest.unitTesting() == false)
    {
      Date date = new Date();
      System.out.println("Date: " + date);
    }

    final BooleanLock acquireVerificationImagesInProgress = new BooleanLock(false);
//    final GuiWaitUtil waiUtilGui = new GuiWaitUtil();
    _imageGenerationObservable.addObserver(this);
    
    //Swee Yee Wong - XCR-3157
    //IRP error message is prompted if run alignment/fine tuning without run full cdna and without unload the board 1st
    if(XrayTester.isS2EXEnabled())
    {
      if(_systemStatusPersistance.isSystemMagnificationReconfigured())
        performStartupIfNeeded();
    }

    // generate verification images on a separate thread
    _xrayTesterException = null;
    HardwareWorkerThread.getInstance().invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          _testExecution.acquireVerificationImagesForManualAlignment(subProgram, useRFP);
        }
        catch (final XrayTesterException ex)
        {
          _xrayTesterException = ex;
        }
        finally
        {
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
//              waiUtilGui.clearWhenReady();
              acquireVerificationImagesInProgress.setValue(true);
              closeAllDialogs();
            }
          });
        }
      }
    });

//    waiUtilGui.wait(this);
    // Block until image acquisition is complete.
    try
    {
      acquireVerificationImagesInProgress.waitUntilTrue();
    }
    catch (InterruptedException iex)
    {
      // Do nothing ...
    }
    _imageGenerationObservable.deleteObserver(this);
    if (_xrayTesterException != null)
    {
      throw _xrayTesterException;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void generateVerificationImagesWithAlignment() throws XrayTesterException
  {
    final TestProgram testProgram = getTestProgramWithReason(StringLocalizer.keyToString("MMGUI_ALIGNMENT_NEEDS_TEST_PROGRAM_KEY"),
            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
            _project);

    if (UnitTest.unitTesting() == false)
    {
      Date date = new Date();
      System.out.println("Date: " + date);
    }

    final GuiWaitUtil waitUtil = new GuiWaitUtil();
    _imageGenerationObservable.addObserver(this);

    // XCR-3199 Xray system start up when generate verification image
    //Swee-Yee.Wong - XCR-3157
    //IRP error message is prompted if run alignment/fine tuning without run full cdna and without unload the board 1st
    if(XrayTester.isS2EXEnabled())
    {
      if(_systemStatusPersistance.isSystemMagnificationReconfigured())
        performStartupIfNeeded();
    }

    // generate verification images on a separate thread
    _xrayTesterException = null;
    HardwareWorkerThread.getInstance().invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          _testExecution.alignAndAcquireVerificationImages(testProgram);
        }
        catch (final XrayTesterException ex)
        {
          _xrayTesterException = ex;
        }
        finally
        {
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              waitUtil.clearWhenReady();
              closeAllDialogs();
            }
          });
        }
      }
    });

    waitUtil.wait(this);
    _imageGenerationObservable.deleteObserver(this);
    if (_xrayTesterException != null)
    {
      throw _xrayTesterException;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void generateFocusImageSet() throws XrayTesterException
  {
    // prior to the dialog being invoked, let's see if we need to generate the focus image set
    generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("GUI_FOCUS_NEED_TEST_PROGRAM_KEY"),
            StringLocalizer.keyToString("GUI_FOCUS_FIX_TITLE_KEY"),
            _project);
    final TestProgram testProgram = _project.getTestProgram();

    _imageManagerObservable.addObserver(this);
    _imageGenerationObservable.addObserver(this);

    final GuiWaitUtil waitUtil = new GuiWaitUtil();

    _totalFocusImages = testProgram.getNumberOfInspectedInspectionImages();
    _xrayTesterException = null;
    HardwareWorkerThread.getInstance().invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          _testExecution.acquireAdjustFocusImages(testProgram);
        }
        catch (final XrayTesterException ex)
        {
          _xrayTesterException = ex;
        }
        finally
        {
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              waitUtil.clearWhenReady();
              closeAllDialogs();
            }
          });
        }
      }
    });

    waitUtil.wait(this);
    _imageManagerObservable.deleteObserver(this);
    _imageGenerationObservable.deleteObserver(this);
    if (_xrayTesterException != null)
    {
      throw _xrayTesterException;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void generateImageSet()
  {
    // we must have saved/up-to-date project in order to generate an image set that we know is compatible.
    // so, here, if the project is "dirty" then ask the user to save.  If they save, then move on to generate the
    // image set, if not do nothing here.
    if (_project.hasBeenModified())
    {
      LocalizedString saveQuestion = new LocalizedString("MM_GUI_SAVE_CHANGES_FOR_IMAGE_GENERATION_KEY",
              new Object[]
              {
                _project.getName()
              });
      int answer = JOptionPane.showConfirmDialog(this,
              StringUtil.format(StringLocalizer.keyToString(saveQuestion), _CHARS_PER_LINE),
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
              JOptionPane.YES_NO_OPTION,
              JOptionPane.WARNING_MESSAGE);
      if (answer == JOptionPane.YES_OPTION)
      {
        getMainMenuBar().saveProject();
      }
      else
      {
        return;  // chose not to save, so exit this operation
      }
    }
    GenerateImageSetWizardDialog dialog = new GenerateImageSetWizardDialog(MainMenuGui.getInstance(), true, _project);
    dialog.pack();
    SwingUtils.centerOnComponent(dialog, this);
    dialog.setVisible(true);
    dialog.dispose();
  }

  /**
   * Starts the X-ray tester hardware
   * @author George Booth
   * @author Ronald Lim
   */
  public void startXrayTester() throws XrayTesterException
  {
    developerDebug("startXrayTester()");
    
    _systemStartupInProgress = true;
    _userInvokedActionInProgress = true;
    createStartupProgressCancelDialog();
    _xrayTesterException = null;
    _hardwareWorkerThread.invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          _testExecution.startup();
        }
        catch (final XrayTesterException ex)
        {
          _xrayTesterException = ex;
        }
        finally
        {
          // get back on swing thread
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              if (_systemStartupWasCanceled)
              {
                developerDebug("Start xray tester canceled");
                _systemStartupWasCanceled = false;
              }
              else
              {
                //Khaw Chek Hau - XCR2654: CAMX Integration
                if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
                {
                  try
                  {
                    _shopFloorSystem.startUpCompleted();
                    _shopFloorSystem.startUpCompletedWithUserId();
                  }
                  catch (DatastoreException ex)
                  {
                    String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("SHOP_FLOOR_ERROR_MESSAGE_KEY_KEY");
                    MessageDialog.showErrorDialog(MainMenuGui.this,
                            errorMsg,
                            StringLocalizer.keyToString("SHOP_FLOOR_EVENT_TITLE_KEY"),
                            true);
                  }
                }

                developerDebug("Start xray tester completed");
              }
              closeProgressDialog(_startupProgressCancelDialog, false);
              _startupProgressCancelDialog = null;
              _homeEnvironment.systemRequestCompleted();
              if (_serviceEnvironmentNotActive)
              {
                _baseMenuBar.setActionMenuStates();
              }
              _systemStartupInProgress = false;

              XrayCameraArray _xrayCameraArray = XrayCameraArray.getInstance();

              try
              {
                // load the config files
                Config config = Config.getInstance();
                if (!config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
                {
                  if (_xrayCameraArray.updateCameraFirmwareRequired())
                  {
                    MessageDialog.showWarningDialog(null,
                            StringLocalizer.keyToString(new LocalizedString("MMGUI_START_UP_NOTIFICATION_MESSAGE_KEY", new Object[]
                            {
                              Version.getVersionNumber()
                            })),
                            StringLocalizer.keyToString("MMGUI_START_UP_NOTIFICATION_TITLE_KEY"),
                            true);
                  }
                }
              }
              catch (XrayTesterException xte)
              {
                _xrayTesterException = xte;
              }
            }
          });
        }
      }
    });
    _startupProgressCancelDialog.setVisible(true);

    if (_userRequestedRetryStartup)
    {
      developerDebug("Restart due to interlock exception");
      _userRequestedRetryStartup = false;
      startXrayTester();
    }
    _userInvokedActionInProgress = false;
    if (_xrayTesterException != null)
    {
      throw _xrayTesterException;
    }
  }
  
  /**
   * @author Kee Chin Seong
   * - Shutdown Remotion Motion Server
   * @throws XrayTesterException 
   */
  public void shutdownRemoteMotionServer() throws XrayTesterException
  {
    int choice = ChoiceInputDialog.showConfirmDialog(MainMenuGui.this,
              StringLocalizer.keyToString("TESTEXEC_GUI_CONFIRM_SHUTDOWN_WHOLE_SYSTEM_KEY"),
              _appName);
    if (choice == JOptionPane.YES_OPTION)
    {
        stopXrayTester();
        
        final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(this,
            StringLocalizer.keyToString("SERVICE_SHUTDOWN_RMS_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
            true);

        hardwareBusyCancelDialog.pack();
        SwingUtils.centerOnComponent(hardwareBusyCancelDialog, this);

        _hardwareWorkerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              RemoteMotionServerExecution.getInstance().shutdownRemoteMotionServer();
              shutdownEntireV810();

              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  hardwareBusyCancelDialog.setVisible(false);
                }
              });
            }
            catch (final IOException xte)
            {
                //do nothing
            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  hardwareBusyCancelDialog.setVisible(false);
                  handleXrayTesterException(xte);
                  return;
                }
              });
            }
         }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  }
  
  /**
   * @author Kee Chin Seong
   * @throws IOException 
   */
  private void shutdownEntireV810() throws IOException
  {
    String command = "shutdown";
    String arguments = "-s -t 1";

    Runtime runtime = Runtime.getRuntime();
    try
    {
      Process proc = runtime.exec("shutdown -s -t 0");
    }
    catch(IOException ioe)
    {
      throw ioe;
    }
  }

  /**
   * Stops the X-ray tester hardware
   * @author George Booth
   */
  public void stopXrayTester() throws XrayTesterException
  {
    developerDebug("stopXrayTester()");
    
     
    _systemShutdownInProgress = true;
    createShutdownBusyDialog();
    _userInvokedActionInProgress = true;
    _xrayTesterException = null;
    _hardwareWorkerThread.invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          _testExecution.shutdown();
        }
        catch (XrayTesterException ex)
        {
          _xrayTesterException = ex;
//          handleXrayTesterException(ex);
        }
        finally
        {
          // get back on swing thread
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              developerDebug("Stop xray tester completed");
              closeBusyCancelDialog(_shutdownBusyDialog, false);
              _shutdownBusyDialog = null;
              // RFH - if an exception is encountered, close all dialogs to prevent
              // possible thread deadlock when two dialog boxes are open
              if (_xrayTesterException != null)
              {
                MainMenuGui.this.closeAllDialogs();
              }

              if (_serviceEnvironmentNotActive)
              {
                _baseMenuBar.setActionMenuStates();
              }
              _homeEnvironment.systemRequestCompleted();
              _systemShutdownInProgress = false;
            }
          });
        }
      }
    });
    _shutdownBusyDialog.setVisible(true);
    _userInvokedActionInProgress = false;
    if (_xrayTesterException != null)
    {
      throw _xrayTesterException;
    }
  }

  /**
   * Loads the panel for the current project
   * @author George Booth
   */
  public void loadPanel() throws XrayTesterException
  {
    developerDebug("loadPanel()");
    Assert.expect(_project != null);

//    // sanity check that panel dimensions are within spec of the panel handler
    // this check is done down in test execution -- check for width, length and thickness
//    if (isPanelSizeWithinSpec() == false)
//    {
//      return;
//    }

    // check if the panel/board definitions are ok -- this call will pop up a dialog if something is bad
    if (isPanelInLegalState() == false)
    {
      return;
    }

    _panelLoadInProgress = true;

    _userInvokedActionInProgress = true;
    createPanelLoadBusyCancelDialog();
    _xrayTesterException = null;
    _hardwareWorkerThread.invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          _testExecution.loadPanelIntoMachine(_project.getTestProgram(), false);
        }
        catch (XrayTesterException e)
        {
          _xrayTesterException = e;
        }
        finally
        {
          // get back on swing thread
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              closeBusyCancelDialog(_panelLoadBusyCancelDialog, false);
              _panelLoadBusyCancelDialog = null;

              // RFH - if an exception is encountered, close all dialogs to prevent
              // possible thread deadlock when two dialog boxes are open
              if (_xrayTesterException != null)
              {
                MainMenuGui.this.closeAllDialogs();
              }

              if (_panelLoadWasCanceled)
              {
                developerDebug("Load panel canceled");
                _baseMenuBar.setActionMenuStates();
              }
              else
              {
                developerDebug("Load panel completed");
                _baseMenuBar.setActionMenuStates();
              }

              _homeEnvironment.systemRequestCompleted();
              _panelLoadInProgress = false;
            }
          });
        }
      }
    });
    _panelLoadBusyCancelDialog.setVisible(true);

    _panelLoadWasCanceled = false;
    _userInvokedActionInProgress = false;
    if (_xrayTesterException != null)
    {
      throw _xrayTesterException;
    }
  }

  /**
   * Unloads the panel
   * @author George Booth
   */
  public void unloadPanel() throws XrayTesterException
  {
    developerDebug("unloadPanel()");
    
    _panelUnloadInProgress = true;
    _userInvokedActionInProgress = true;
    createPanelUnloadBusyDialog();
    _xrayTesterException = null;
    _hardwareWorkerThread.invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          _testExecution.unloadPanelFromMachine(TestResultsEnum.NOT_TESTED);
        }
        catch (XrayTesterException e)
        {
          _xrayTesterException = e;
//          handleXrayTesterException(e);
        }
        finally
        {
          // get back on swing thread
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              developerDebug("Unload panel completed");
              closeBusyCancelDialog(_panelUnloadBusyDialog, false);
              _panelUnloadBusyDialog = null;

              // RFH - if an exception is encountered, close all dialogs to prevent
              // possible thread deadlock when two dialog boxes are open
              if (_xrayTesterException != null)
              {
                MainMenuGui.this.closeAllDialogs();
              }

              _baseMenuBar.setActionMenuStates();
              _homeEnvironment.systemRequestCompleted();
              _panelUnloadInProgress = false;
              
              // added by Ying-Huan.Chu
              if (TestDev.isInSurfaceMappingMode())
              {
                _testExecution.resetLastPerformedAlignmentTestSubProgramId();
              }
            }
          });
        }
      }
    });
    _panelUnloadBusyDialog.setVisible(true);
    _userInvokedActionInProgress = false;
    if (_xrayTesterException != null)
    {
      throw _xrayTesterException;
    }
  }

  /**
   * Homes the rails
   * @author George Booth
   */
  void homeRails() throws XrayTesterException
  {
    developerDebug("homeRails()");
    _homeRailsInProgress = true;
    createHomeRailsBusyDialog();
    _userInvokedActionInProgress = true;
    _xrayTesterException = null;
    _hardwareWorkerThread.invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          _panelHandler.homeStageRails();
        }
        catch (XrayTesterException e)
        {
          _xrayTesterException = e;
//          handleXrayTesterException(e);
        }
        finally
        {
          // get back on swing thread
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              developerDebug("Home rails completed");
              closeBusyCancelDialog(_homeRailsBusyDialog, false);
              _homeRailsBusyDialog = null;

              // RFH - if an exception is encountered, close all dialogs to prevent
              // possible thread deadlock when two dialog boxes are open
              if (_xrayTesterException != null)
              {
                MainMenuGui.this.closeAllDialogs();
              }
              _homeRailsInProgress = false;
            }
          });
        }
      }
    });
    _homeRailsBusyDialog.setVisible(true);
    _userInvokedActionInProgress = false;
    if (_xrayTesterException != null)
    {
      throw _xrayTesterException;
    }
  }

  /**
   * This method will display the host ID.
   * @author Erica Wheatcroft
   */
  public void displayHostId()
  {
//    String hostId = _licenseManager.getHostId();
//    MessageDialog.showInformationDialog(
//      this,
//      StringLocalizer.keyToString(new LocalizedString("MMGUI_HOST_ID_LABEL_KEY",
//        new Object[]{ hostId })),
//      StringLocalizer.keyToString(_appNameKey),
//      true);
  }

  /**
   * This method will generate the timing metric string as well as print the string to the the sytem log file.
   * This code should not be used when we ship. Its for debugging purposes.
   *
   * @author Erica Wheatcroft
   */
  public String getTimingMetricsString(TestExecutionTimer timer)
  {
    Assert.expect(timer != null);

    long fullInspectionTime = timer.getElapsedFullInspectionTimeInMillis();
    long beatRateTime = timer.getElapsedBeatRateTimeInMillis();
    long panelLoadTime = timer.getElapsedPanelLoadTimeInMillis();
    long irpAndCameraInitializeTime = timer.getElapsedIrpAndCameraInitializeTimeInMillis();
    long scanPassGeneratorTime = timer.getElapsedScanPassGeneratorTimeInMillis();
    long initStageScanTimer = timer.getElapsedInitStageScanPassTimeInMillis();
    long motionStartupTimer = timer.getElapsedMotionStartupTimeInMillis();
    long allHardwareStartupTimer = timer.getElapsedAllHardwareStartupTimeInMillis();
    long xrayOnTimer = timer.getElapsedXrayOnTimeInMillis();
    long xrayOffTimer = timer.getElapsedXrayOffTimeInMillis();
    long calibrationTimer = timer.getElapsedCalibrationTimeInMillis();
    long imageAcquistionTime = timer.getElapsedImageAcquisitionTimeInMillis();
    long stageScanTime = timer.getElapsedStageScanTimeInMillis();
    long stagePauseTime = timer.getElapsedStagePausedTimeInMillis();
    long irpPauseTime = timer.getElapsedIrpPausedTimeInMillis();
    long reReconstructTime = timer.getElapsedReReconstructTimeInMillis();
    long reReconstructCount = timer.getReReconstructCount();
    long reReconstructAcquireTime = timer.getElapsedReReconstructAcquireTimeInMillis();
    long reReconstructIntervalTime = timer.getElapsedReReconstructIntervalTimeInMillis();
    long reReconstructSlicesCount = timer.getReReconstructAcquireSlicesCount();
    long alignmentTime = timer.getElapsedAlignmentTimeInMillis();
    long surfaceMappingTime = timer.getElapsedSurfaceMappingTimeInMillis();
    long jointInspectionTime = timer.getElapsedJointInspectionTimeInMillis();
    long repairImageSave = timer.getElapsedImageSaveTimeInMillis();
    long xmlResultsWriterTime = timer.getElapsedXmlWriterTimeInMillis();
    long binaryResultsWriterTime = timer.getElapsedInspectionResultsWriterTimeInMillis();
    long panelUnloadTime = timer.getElapsedPanelUnloadTimeInMillis();

    String fullInspectionTimeStr = StringUtil.convertMilliSecondsToElapsedTime(fullInspectionTime, true);
    String beatRateTimeStr = StringUtil.convertMilliSecondsToElapsedTime(beatRateTime, true);
    String panelLoadTimeStr = StringUtil.convertMilliSecondsToElapsedTime(panelLoadTime, true);
    String irpAndCameraInitializeTimeStr = StringUtil.convertMilliSecondsToElapsedTime(irpAndCameraInitializeTime, true);
    String scanPassGeneratorTimeStr = StringUtil.convertMilliSecondsToElapsedTime(scanPassGeneratorTime, true);
    String initStageScanTimeStr = StringUtil.convertMilliSecondsToElapsedTime(initStageScanTimer, true);
    String motionStartupTimeStr = StringUtil.convertMilliSecondsToElapsedTime(motionStartupTimer, true);
    String allHardwareStartupTimeStr = StringUtil.convertMilliSecondsToElapsedTime(allHardwareStartupTimer, true);
    String xrayOnTimeStr = StringUtil.convertMilliSecondsToElapsedTime(xrayOnTimer, true);
    String xrayOffTimeStr = StringUtil.convertMilliSecondsToElapsedTime(xrayOffTimer, true);
    String calibrationTimeStr = StringUtil.convertMilliSecondsToElapsedTime(calibrationTimer, true);
    String imageAcquisitionTimeStr = StringUtil.convertMilliSecondsToElapsedTime(imageAcquistionTime, true);
    String stageScanTimeStr = StringUtil.convertMilliSecondsToElapsedTime(stageScanTime, true);
    String stagePauseTimeStr = StringUtil.convertMilliSecondsToElapsedTime(stagePauseTime, true);
    String irpPauseTimeStr = StringUtil.convertMilliSecondsToElapsedTime(irpPauseTime, true);
    String reReconstructStr = StringUtil.convertMilliSecondsToElapsedTime(reReconstructTime, true) + "  count: "
            + reReconstructCount;
    String reReconstructAcquireStr = StringUtil.convertMilliSecondsToElapsedTime(reReconstructAcquireTime, true)
            + "  slices: " + reReconstructSlicesCount;
    String reReconstructIntervalStr = StringUtil.convertMilliSecondsToElapsedTime(reReconstructIntervalTime, true);
    String alignmentTimeStr = StringUtil.convertMilliSecondsToElapsedTime(alignmentTime, true);
    String surfaceMappingTimeStr = StringUtil.convertMilliSecondsToElapsedTime(surfaceMappingTime, true);
    String jointInspectionTimeStr = StringUtil.convertMilliSecondsToElapsedTime(jointInspectionTime, true);
    String repairImageSaveTimeStr = StringUtil.convertMilliSecondsToElapsedTime(repairImageSave, true);
    String xmlResultsWriterTimeStr = StringUtil.convertMilliSecondsToElapsedTime(xmlResultsWriterTime, true);
    String binaryResultsWriterTimeStr = StringUtil.convertMilliSecondsToElapsedTime(binaryResultsWriterTime, true);
    String panelUnloadTimeStr = StringUtil.convertMilliSecondsToElapsedTime(panelUnloadTime, true);

    LocalizedString fullInspectionTimeLocalizedString = new LocalizedString("TESTEXEC_GUI_VIEWS_TIME_KEY",
            new Object[]
            {
              fullInspectionTimeStr
            });

    LocalizedString timingHeaderString = new LocalizedString("TESTEXEC_GUI_TIMING_HEADER_KEY",
            new Object[]
            {
              _project.getName()
            });

    String timingMetricString = StringLocalizer.keyToString(timingHeaderString) + "\n";
    timingMetricString += StringLocalizer.keyToString(fullInspectionTimeLocalizedString) + "\n";
    timingMetricString += "   Beat Rate:\t\t" + beatRateTimeStr + "\n";
    timingMetricString += "   Panel Load:\t\t" + panelLoadTimeStr + "\n";
    timingMetricString += "   IRP and Camera initialize:\t\t" + irpAndCameraInitializeTimeStr + "\n";
    timingMetricString += "   Scan Pass Generation:\t" + scanPassGeneratorTimeStr + "\n";
    timingMetricString += "   Stage Scan initialize:\t" + initStageScanTimeStr + "\n";
    timingMetricString += "   motionStartup:\t" + motionStartupTimeStr + "\n";
    timingMetricString += "   allHardwareStartup:\t" + allHardwareStartupTimeStr + "\n";
    timingMetricString += "   xray on:\t\t" + xrayOnTimeStr + "\n";
    timingMetricString += "   xray off:\t\t" + xrayOffTimeStr + "\n";
    timingMetricString += "   Calibration:\t\t" + calibrationTimeStr + "\n";
    timingMetricString += "   Image Acquisition:\t" + imageAcquisitionTimeStr + "\n";
    timingMetricString += "   Stage Scan:\t\t" + stageScanTimeStr + "\n";
    timingMetricString += "   Stage Pause:\t\t" + stagePauseTimeStr + "\n";
    timingMetricString += "   IRP Pause:\t\t" + irpPauseTimeStr + "\n";
    timingMetricString += "   ReReconstruct time:\t" + reReconstructStr + "\n";
    timingMetricString += "   ReReconstruct acquire time:\t" + reReconstructAcquireStr + "\n";
    timingMetricString += "   ReReconstructInterval time:\t" + reReconstructIntervalStr + "\n";
    timingMetricString += "   Alignment:\t\t" + alignmentTimeStr + "\n";
    timingMetricString += "   Surface Mapping:\t\t" + surfaceMappingTimeStr + "\n";
    timingMetricString += "   Joint Inspection:\t" + jointInspectionTimeStr + "\n";
    timingMetricString += "   Repair Image Save:\t" + repairImageSaveTimeStr + "\n";
    timingMetricString += "   XML Results Save:\t" + xmlResultsWriterTimeStr + "\n";
    timingMetricString += "   Binary Results Save:\t" + binaryResultsWriterTimeStr + "\n";
    timingMetricString += "   Panel Unload:\t" + panelUnloadTimeStr + "\n";

    // also print to log file
    System.out.println("\n\nInspection times:");
    System.out.println(timingMetricString);

    return timingMetricString;
  }

  /**
   * @author Steve Anonson
   */
  private void startServer()
  {
    _server = null;
    _hardwareWorkerThread = HardwareWorkerThread.getInstance();

    try
    {
      _server = Server.getInstance();
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);
      System.exit(-1);
    }    
    // server has loaded config, OK to check for debug now
    _DEVELOPER_DEBUG = Config.isDeveloperDebugModeOn();
    _DEVELOPER_PERFORMANCE = Config.isDeveloperPerformanceModeOn();
    // open performance log file if enabled
    if (_DEVELOPER_PERFORMANCE)
    {
      try
      {
        PerformanceLogUtil.getInstance().openPerformanceLog();
      }
      catch (DatastoreException dse)
      {
        JOptionPane.showMessageDialog(
                _instance,
                dse.getLocalizedMessage(),
                StringLocalizer.keyToString(new LocalizedString("MMGUI_GUI_NAME_KEY", null)),
                JOptionPane.ERROR_MESSAGE);
      }
    }

    _projectObservable = ProjectObservable.getInstance();
    _guiUpdateObservable = GuiObservable.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
    _progressObservable = ProgressObservable.getInstance();
    _imageManagerObservable = ImageManagerObservable.getInstance();
    _imageGenerationObservable = ImageGenerationObservable.getInstance();
    _imageReconstructionObservable = ImageReconstructionObservable.getInstance();
    _inspectionEventObservable = InspectionEventObservable.getInstance();
    _fringePatternObservable = FringePatternObservable.getInstance();
    _assertObservable = AssertObservable.getInstance();

    _projectObservable.addObserver(this);
    _guiUpdateObservable.addObserver(this);
    _hardwareObservable.addObserver(this);
    _progressObservable.addObserver(this);
    _fringePatternObservable.addObserver(this);
    _assertObservable.addObserver(this);
    _inspectionEventObservable.addObserver(this);
    _inspectionEventObservable.addObserver(new AlignmentVerificationObserver());
    _imageReconstructionObservable.addObserver(this);

    // progressObservable not monitored unless hardware is starting

    _testExecution = TestExecution.getInstance();
    _imageManager = ImageManager.getInstance();
    _panelHandler = PanelHandler.getInstance();
    _lightStack = LightStack.getInstance();
    _digitalIo = DigitalIo.getInstance();

    try
    {
      _server.go();
      FileUtilAxi.deleteDirectoryContents(Directory.getOnlineXrayImagesDir());
      
    // XCR 3656: System crash when load S2EX V810 application if opticalStopControllerEndStopperInstalled=false
    //check the optical stop sensor settings in hanrdware config
      _digitalIo.checkOpticalStopSensorSettings();
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);
      System.exit(-1);
    }
    catch (RemoteException ex)
    {
      MessageDialog.showErrorDialog(
              this,
              ex.getLocalizedMessage(),
              StringLocalizer.keyToString("MD_CONFIGURATION_ERROR_TITLE_KEY"));
      System.exit(-1);
    }
  }

  /**
   * Test program status has changed
   * @author George Booth
   */
  private void setTestProgramDirtyFlag(boolean dirty)
  {
    //Khaw Chek Hau - XCR2654: CAMX Integration
    if (dirty && Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
    {
      try
      {
        _shopFloorSystem.recipeModified();
      }
      catch (DatastoreException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.this,
                ex.getLocalizedMessage(),
                StringLocalizer.keyToString("SHOP_FLOOR_EVENT_TITLE_KEY"),
                true);
      }
    }
    
    _testProgramDirty = dirty;
    updateTitle();
  }

  /**
   * Project status has changed
   * @author George Booth
   */
  private void setProjectDirtyFlag(boolean dirty)
  {
    _projectDirty = dirty;
    updateTitle();
  }

  /**
   * @author George Booth
   */
  public boolean isProjectModified()
  {
    return _projectDirty || _testProgramDirty;
  }

  /**
   * @author George Booth
   */
  public String getCurrentProjectName()
  {
    // XCR-2955 Able to Save Image for Dummy Virtual Live Project - Chin Seong
    if(Project.isUsingVirtualDummyProject())
    {
      return Project.getCurrentlyLoadedProject().getName();
    }
    else
    {
        if (_project != null)
        {
          return _project.getName();
        }
        else
        {
          return "<none>";
        }
    }
  }

  /**
   * An external event has affected Home panel data
   * @author George Booth
   */
  public void updateHomePanel()
  {
    if (_homeEnvironment != null)
    {
      _homeEnvironment.updatePanelData();
    }
  }

  /**
   * User has opened or imported a project.
   * @author George Booth
   * @author Wei Chin, Chong (initialThreshold)
   */
  void projectOpen(Project project)
  {
    Assert.expect(project != null);
    _project = project;
    setProjectDirtyFlag(false);
    updateTitle();
    try
    {
      // set the initial thresholds here
      ProjectInitialThresholds.getInstance().setInitialThresholdsFileFullPath(project.getInitialThresholdsSetFullPath());
      ProjectInitialRecipeSettings.getInstance().setInitialRecipeSettingFileFullPath(project.getInitialThresholdsSetFullPath());
    }
    catch (DatastoreException ex)
    {
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
              ex.getLocalizedMessage(),
              StringLocalizer.keyToString(new LocalizedString("MM_GUI_TDT_NAME_KEY", null)),
              JOptionPane.ERROR_MESSAGE);
      try
      {
        _project.setInitialThresholdsSetName(ProjectInitialThresholds.getInstance().getSystemDefaultSetName());
        _project.setInitialRecipeSettingSetName(ProjectInitialRecipeSettings.getInstance().getSystemDefaultSetName());
      }
      catch (DatastoreException dex2)
      {
        // do nothing right now -- we just displayed a message
      }
    }

    // populate will be done on a panel by panel when each start() is called
    // TestDev provides its own busy dialog for graphics panel init
    _testDevEnvironment.populateWithProjectData(_project);
     if(isVirtualLiveModeEnabled())
       populateVirtualLiveProjectData(_project);
    if (_currentEnvironment != _testExecEnvironment)
    {
      _baseMenuBar.enableMenuItems(_runningAs, true);
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void populateCadCreatorProjectData(Project project, boolean isEmptyProject)
  {
    _cadCreatorEnvironmentPanel.populateWithProjectData(project, isEmptyProject);  
  }
  
  public void populateVirtualLiveProjectData(Project project)
  {
    _virtualLiveEnvironmentPanel.populateWithProjectData(project);
  }

  /**
   * @author George Booth
   */
  public void projectWasDeleted(String projectName)
  {
    if (_homeEnvironment != null)
    {
      _homeEnvironment.projectWasDeleted(projectName);
    }
  }

  /**
   * User requested the project be closed.  This only sets flags - there is no
   * method to actually remove the project from memory.
   * @author George Booth
   */
  void projectClose() throws XrayTesterException
  {
    _project.unloadCurrentProject();
    _project = null;
    setProjectDirtyFlag(false);
    updateTitle();
    _homeEnvironment.closeProject();
    // unpopulate will be done on a panel by panel immediately
    _testDevEnvironment.unpopulate();
    if(_virtualLiveEnvironmentPanel != null)
      _virtualLiveEnvironmentPanel.unpopulate();

    ImageAcquisitionEngine.getInstance().clearProjectInfo();
    _testExecEnvironment.clearProjectInfo();
    // clear the undo stack when a project is unpopulated
    com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().clear();


    if (_currentEnvironment != _testExecEnvironment)
    {
      //                                       update actions menu
      _baseMenuBar.enableMenuItems(_runningAs, true);
    }
    
    // Bee Hoon - XCR 1650, Make sure only switch to main home page when recipe is closed at TestDevEnvironment
    if(_currentEnvironment == _testDevEnvironment)
      switchToAXIHome();
  }

  /**
   * update the title of the gui
   * @author George A. David
   */
  private void updateTitle()
  {
    String title = _currentScreenEnum.getName();

    if (_serviceEnvironmentNotActive)
    {
      if (_project != null)
      {
        title += " - " + _project.getName();
        if (isProjectModified())
        {
          title += " *";
        }
        try
        {
          if (LicenseManager.isOfflineProgramming())
          {
            if (XrayTester.getSystemType().equals(SystemTypeEnum.THROUGHPUT))
            {
              title +=" - "+SystemTypeEnum.STANDARD.getName().toUpperCase();
            }
            else
            {
              title +=" - "+XrayTester.getSystemType();
            }
          }
        }
        catch (final BusinessException e)
        {
          finishLicenseMonitor();
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                      e.getMessage(),
                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                      true);
              MainMenuGui.getInstance().exitV810(false);
            }
          });
        }
      }
    }
    setTitle(title);
  }

  /**
   * append the specifed string to the panel title (used by XraySafetyTestTaskPanel)
   * @author George Booth
   */
  public void AppendToTitle(String titleMessage)
  {
    Assert.expect(titleMessage != null);
    String title = _currentScreenEnum.getName() + titleMessage;
    setTitle(title);
  }

  /**
   * @author George Booth
   */
  public synchronized void update(final Observable observable, final Object arg)
  {
    SwingUtils.invokeLater(new Runnable()
    {

      public void run()
      {
        if (observable instanceof ProgressObservable)
        {
          handleProgressObservable(arg);
        }
        else if (observable instanceof ProjectObservable)
        {
          handleProjectObservable(arg);
        }
        else if (observable instanceof GuiObservable)
        {
          handleGuiObservable(arg);
        }
        else if (observable instanceof HardwareObservable)
        {
          handleHardwareObservable(arg);
        }
        else if (observable instanceof ImageManagerObservable)
        {
          handleImageManagerObservable(arg);
        }
        else if (observable instanceof ImageGenerationObservable)
        {
          handleImageGenerationObservable(arg);
        }
        else if (observable instanceof ImageReconstructionObservable)
        {
          handleImageReconstructionObservable(arg);
        }
        else if (observable instanceof InspectionEventObservable)
        {
          handleInspectionEventObservable(arg);
        }
        else if (observable instanceof VirtualLiveImageGenerationProgressObservable)
        {
          handleVirtualLiveImageGenerationObservable(arg);
        }
        else if (observable instanceof FringePatternObservable)
        {
            if (arg instanceof FringePatternInfo)
            {
                FringePatternInfo fringePatternInfo = (FringePatternInfo)arg;
                final OpticalCameraIdEnum cameraId = fringePatternInfo.getCameraId();
                FringePatternEnum fringePatternEnum = fringePatternInfo.getFringePatternEnum();
                
                if (fringePatternEnum.equals(FringePatternEnum.INITIALIZE))
                {
                    if (_fringePatternDialog != null)
                    {
                        _fringePatternDialog.dispose();
                        _fringePatternDialog = null;
                    }
                    
                    // Get FringePattern instance
                    _fringePatternDialog = new FringePatternDialog(MainMenuGui.getInstance(), "Fringe Pattern", cameraId);
                }
                else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_WHITE_LIGHT))
                {
                    if (SwingUtilities.isEventDispatchThread())
                      _fringePatternDialog.displayWhiteLight(cameraId);
                    else
                    {
                      SwingUtils.invokeLater(new Runnable()
                      {
                        public void run()
                        {
                          _fringePatternDialog.displayWhiteLight(cameraId);
                        }
                      });
                    }
                }
                else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_NO_LIGHT))
                {
                    if (SwingUtilities.isEventDispatchThread())
                      _fringePatternDialog.closeFringePattern();
                    else
                    {
                      SwingUtils.invokeLater(new Runnable()
                      {
                        public void run()
                        {
                          _fringePatternDialog.closeFringePattern();
                        }
                      });
                    }
                }
                else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_ONE))
                {
                    if (SwingUtilities.isEventDispatchThread())
                      _fringePatternDialog.displayFringePatternShiftOne(cameraId);
                    else
                    {
                      SwingUtils.invokeLater(new Runnable()
                      {
                        public void run()
                        {
                          _fringePatternDialog.displayFringePatternShiftOne(cameraId);
                        }
                      });
                    }
                }
                else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_TWO))
                {
                    if (SwingUtilities.isEventDispatchThread())
                      _fringePatternDialog.displayFringePatternShiftTwo(cameraId);
                    else
                    {
                      SwingUtils.invokeLater(new Runnable()
                      {
                        public void run()
                        {
                          _fringePatternDialog.displayFringePatternShiftTwo(cameraId);
                        }
                      });
                    }
                }
                else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_THREE))
                {
                    if (SwingUtilities.isEventDispatchThread())
                      _fringePatternDialog.displayFringePatternShiftThree(cameraId);
                    else
                    {
                      SwingUtils.invokeLater(new Runnable()
                      {
                        public void run()
                        {
                          _fringePatternDialog.displayFringePatternShiftThree(cameraId);
                        }
                      });
                    }
                }
                else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_FOUR))
                {
                    if (SwingUtilities.isEventDispatchThread())
                      _fringePatternDialog.displayFringePatternShiftFour(cameraId);
                    else
                    {
                      SwingUtils.invokeLater(new Runnable()
                      {
                        public void run()
                        {
                          _fringePatternDialog.displayFringePatternShiftFour(cameraId);
                        }
                      });
                    }
                }
                else if (fringePatternEnum.equals(FringePatternEnum.FINISH_DISPLAY_FRINGE_PATTERN))
                {
                  try
                  {
                    PspImageAcquisitionEngine.getInstance().handlePostImageDisplay(PspModuleEnum.FRONT);
                  }
                  catch (XrayTesterException ex)
                  {
                    handleXrayTesterException(ex);
                    return;
                  }
                }
                else
                    Assert.expect(false, "invalid display mode");
            }
        }
        else if (observable instanceof AssertObservable)
        {
          handleAssertObservable();
        }
        else
        {
          Assert.expect(false, "No update code for observable: " + observable.getClass().getName());
        }
      }
    });
  }

  /**
   * @author George A. David
   */
  private void notifyUserOfProjectWithMismatchingSystemType(ProjectChangeEvent event)
  {
    Pair<String, String> projectNameToSystemTypePair = (Pair<String, String>)event.getSource();
    
    if (XrayTester.isHardwareAvailable())
    {
      if (isTestExecCurrentEnvironment())
      {
        try
        {
          LocalizedString errorMessage = new LocalizedString("MMGUI_TESTEXEC_CANNOT_LOAD_PROJECT_DIFFERENT_SYSTEM_TYPE_KEY", null);//,
//                                                           new Object[]{XrayTester.getSystemType().getName()});
          MessageDialog.showErrorDialog(null,
            StringUtil.format(StringLocalizer.keyToString(errorMessage), _CHARS_PER_LINE),
            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
            true);
        }
        finally
        {
          getMainMenuBar().projectClose(true);
          Project.userResponseToSystemTypesDiffer(false, false);         
        }
      }
      else
      {
        _createProjectDialog = new CreateProjectOptionDialog(this, true,
                projectNameToSystemTypePair.getFirst(),
                projectNameToSystemTypePair.getSecond());
        _createProjectDialog.pack();
        SwingUtils.centerOnComponent(_createProjectDialog, this);
        _createProjectDialog.setVisible(true);
        boolean userCancel = _createProjectDialog.userCancel();

        if (userCancel)
        {
          Project.userResponseToSystemTypesDiffer(false, false);
        }
        else
        {
          final String projectName = _createProjectDialog.getNewProjectName();
          // Fix for CR 1040 (Wei Chin)
          // this is continue loading the file without changing the system type
          // Do not allow user to continue without saving as new project.
//          if (_createProjectDialog.isContinueLoadingProject())
//          {
//            Project.userResponseToSystemTypesDiffer(true, false, projectName);
//          }
//          else // this would create the project with a different system type
//          {
          Project.userResponseToSystemTypesDiffer(false, true, projectName);
//          }
        }
      }
    }
    else
    {

      LocalizedString warningMessage = new LocalizedString("MMGUI_SYSTEM_TYPES_DIFFER_KEY", null);
      int answer = JOptionPane.showConfirmDialog(this,
              StringUtil.format(StringLocalizer.keyToString(warningMessage), _CHARS_PER_LINE),
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
              JOptionPane.YES_NO_OPTION,
              JOptionPane.WARNING_MESSAGE);

      if (answer == JOptionPane.YES_OPTION)
      {
        _createProjectDialog = new CreateProjectOptionDialog(this, true,
                projectNameToSystemTypePair.getFirst(),
                projectNameToSystemTypePair.getSecond());
        _createProjectDialog.pack();
        SwingUtils.centerOnComponent(_createProjectDialog, this);
        _createProjectDialog.setVisible(true);
        boolean userCancel = _createProjectDialog.userCancel();

        if (userCancel)
        {
          Project.userResponseToSystemTypesDiffer(false, false);
        }
        else
        {
          final String projectName = _createProjectDialog.getNewProjectName();
          Project.userResponseToSystemTypesDiffer(false, true, projectName);
        }
      }
      else
      {
        Project.userResponseToSystemTypesDiffer(false, false);
      }

    }
  }
  
  /**
   * @author Ngie Xing
   */
  private void notifyUserOfProjectWithMismatchLowMagnification(Project project, String projectMagnification, String systemMagnification)
  {
    Object[] lowMagnifications =
    {
      projectMagnification, systemMagnification
    };
    
    Object[] options =
    {
      StringLocalizer.keyToString("MMGUI_MAGNIFICATION_CHANGED_PROCEED_BUTTON_KEY"),
      StringLocalizer.keyToString("MMGUI_MAGNIFICATION_CHANGED_CANCEL_BUTTON_KEY")
    };    
          
    int response = JOptionPane.showOptionDialog(this,
      StringLocalizer.keyToString(new LocalizedString("MMGUI_LOW_MAGNIFICATION_CHANGED_WARNING_MESSAGE_KEY", lowMagnifications)),
      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
      JOptionPane.YES_NO_OPTION,
      JOptionPane.QUESTION_MESSAGE,
      null,
      options,
      options[0]);
    
    if (response == JOptionPane.YES_OPTION)
    {
      //prompt user for shorts learning choice only when config is enabled
      //else let the user to delete from Initial Tuning manually 
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_DELETE_SHORTS_LEARNING_DURING_PROJECT_LOAD))
        notifyUserShortsLearnedDataInvalidIfExist(project.getName());

      Project.userResponseToProceedWithProjectLoad(true);
    }
    else
    {
      getMainMenuBar().projectClose(true);
      Project.userResponseToProceedWithProjectLoad(false);  
    }
  }
  
  /**
   * @author Ngie Xing
   */
  private void notifyUserOfProjectWithMismatchHighMagnification(Project project, String projectMagnification, String systemMagnification)
  {
    Object[] highMagnifications =
    {
      projectMagnification, systemMagnification
    };
    
    Object[] options =
    {
      StringLocalizer.keyToString("MMGUI_MAGNIFICATION_CHANGED_PROCEED_BUTTON_KEY"),
      StringLocalizer.keyToString("MMGUI_MAGNIFICATION_CHANGED_CANCEL_BUTTON_KEY")
    };    
          
    int response = JOptionPane.showOptionDialog(this,
      StringLocalizer.keyToString(new LocalizedString("MMGUI_HIGH_MAGNIFICATION_CHANGED_WARNING_MESSAGE_KEY", highMagnifications)),
      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
      JOptionPane.YES_NO_OPTION,
      JOptionPane.QUESTION_MESSAGE,
      null,
      options,
      options[0]);
    
    if (response == JOptionPane.YES_OPTION)
    {
      //prompt user for shorts learning choice only when config is enabled
      //else let the user to delete from Initial Tuning manually 
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_DELETE_SHORTS_LEARNING_DURING_PROJECT_LOAD))
        notifyUserShortsLearnedDataInvalidIfExist(project.getName());

      Project.userResponseToProceedWithProjectLoad(true);
    }
    else
    {
      getMainMenuBar().projectClose(true);
      Project.userResponseToProceedWithProjectLoad(false);
    }
  }
  
  /**
   * @author Swee Yee
   */
  private void notifyUserOfNewerSolderThicknessVersionExists(String projectSolderThisknessVersion, String latestSolderThisknessVersion)
  {
    Object[] solderThisknessVersion =
    {
      projectSolderThisknessVersion, latestSolderThisknessVersion
    };
    
    Object[] options =
    {
      StringLocalizer.keyToString("MMGUI_PROCEED_WITH_DIFFER_SOLDER_THICKNESS_VERSION_YES_BUTTON_KEY"),
      StringLocalizer.keyToString("MMGUI_PROCEED_WITH_DIFFER_SOLDER_THICKNESS_VERSION_NO_BUTTON_KEY")
    };    
          
    int response = JOptionPane.showOptionDialog(this,
      StringLocalizer.keyToString(new LocalizedString("MMGUI_NEWER_SOLDER_THICKNESS_VERSION_EXISTS_WARNING_MESSAGE_KEY", solderThisknessVersion)),
      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
      JOptionPane.YES_NO_OPTION,
      JOptionPane.QUESTION_MESSAGE,
      null,
      options,
      options[0]);
    
    if (response == JOptionPane.NO_OPTION)
    {
      Project.userResponseToProceedWithDifferSolderThicknessVersion(true);
    }
    else
    {
      Project.userResponseToProceedWithDifferSolderThicknessVersion(false);
    }
  }
    
  /**
   * @author Swee Yee
   */
  private void notifyUserOfAlternativeSolderThicknessVersionExists(String projectSolderThisknessVersion, String latestSolderThisknessVersion)
  {
    Object[] solderThisknessVersion =
    {
      projectSolderThisknessVersion, latestSolderThisknessVersion
    };
    
    Object[] options =
    {
      StringLocalizer.keyToString("MMGUI_PROCEED_WITH_DIFFER_SOLDER_THICKNESS_VERSION_YES_BUTTON_KEY"),
      StringLocalizer.keyToString("MMGUI_PROCEED_WITH_DIFFER_SOLDER_THICKNESS_VERSION_NO_BUTTON_KEY")
    };    
          
    int response = JOptionPane.showOptionDialog(this,
      StringLocalizer.keyToString(new LocalizedString("MMGUI_ALTERNATIVE_SOLDER_THICKNESS_VERSION_EXISTS_WARNING_MESSAGE_KEY", solderThisknessVersion)),
      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
      JOptionPane.YES_NO_OPTION,
      JOptionPane.QUESTION_MESSAGE,
      null,
      options,
      options[0]);
    
    if (response == JOptionPane.YES_OPTION)
    {
      Project.userResponseToProceedWithDifferSolderThicknessVersion(true);
    }
    else
    {
      Project.userResponseToProceedWithDifferSolderThicknessVersion(false);
    }
  }
  
  private void notifyUserShortsLearnedDataInvalidIfExist(String projectName)
  {
    if (Project.doesShortLearningFileExist(projectName)
      || Project.doesShortLearningTempProjOpenFileExist(projectName))
    {
      //the learned data for Shorts may be invalid, prompt user whether to delete the learned data or not
      int delete = JOptionPane.showConfirmDialog(this,
        StringLocalizer.keyToString("MMGUI_SHORTS_LEARN_DATA_INVALID_WARNING_MESSAGE_KEY"),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        JOptionPane.YES_NO_OPTION);

      if (delete == JOptionPane.YES_OPTION)
        Project.deleteAndRemoveAllShortsLearningData();
    }
  }
  
  /**
   * @author George Booth
   */
  private void handleProgressObservable(Object arg)
  {
    Assert.expect(arg instanceof ProgressReport);
    if (arg instanceof ProgressReport)
    {
      ProgressReport progressReport = (ProgressReport) arg;
      int percentDone = progressReport.getPercentDone();
      // don't indicate 100% until event actually completes
      if (percentDone == 100)
      {
        percentDone = 99;
      }
      if (_systemStartupWasCanceled == false)
      {
        _lastPercentDone = percentDone;
      }
      String reporterAction = progressReport.getAction();
      if (_systemStartupWasCanceled)
      {
        reporterAction = StringLocalizer.keyToString("MMGUI_START_UP_CANCELING_MESSAGE_KEY");  // "Canceling startup ..."
      }

      if (_startupProgressCancelDialog != null)
      {
        _startupProgressCancelDialog.updateProgressBarPrecent(_lastPercentDone, reporterAction);
      }
      else if (_autoAdjustProgressDialog != null)
      {
        _autoAdjustProgressDialog.updateProgressBarPrecent(_lastPercentDone, reporterAction);
      }
      else if (_autoStartupProgressDialog != null)
      {
        _autoStartupProgressDialog.updateProgressBarPrecent(_lastPercentDone, reporterAction);
      }
      // XCR1144, Chnee Khang Wah, 29-Dec-2010
      else if (_xRaySourcePowerOffProgressDialog != null)
      {
        _xRaySourcePowerOffProgressDialog.updateProgressBarPrecent(_lastPercentDone, reporterAction);
      }
      // XCR1144 - end
      else
      {
        developerDebug("Progress events seen with no observer");
        // do nothing - dialog was closed but events haven't stopped
//        System.out.println("  reporterAction = " + reporterAction + " %done = " + _lastPercentDone);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleImageManagerObservable(Object arg)
  {
    Assert.expect(arg instanceof ImageManagerEventEnum);

    ImageManagerEventEnum imageManagerEventEnum = (ImageManagerEventEnum) arg;
    if (_focusSetGenerationProgressDialog != null)
    {
      if (imageManagerEventEnum.equals(ImageManagerEventEnum.ADJUST_FOCUS_IMAGE_SAVED))
      {
        _currentFocusImages++;
        _lastPercentDone = (_currentFocusImages * 100) / _totalFocusImages;
        _focusSetGenerationProgressDialog.updateProgressBarPrecent(_lastPercentDone);
      }
    }
    if (_offlineInspectionImageGenerationProgressDialog != null)
    {
      if (imageManagerEventEnum.equals(ImageManagerEventEnum.INSPECTION_IMAGE_SAVED))
      {
        _currentOfflineInspectionImages++;
        _lastPercentDone = (_currentOfflineInspectionImages * 100) / _totalOfflineInspectionImages;
        _offlineInspectionImageGenerationProgressDialog.updateProgressBar(_currentOfflineInspectionImages); //.updateProgressBarPrecent(_lastPercentDone);
        //_offlineInspectionImageGenerationProgressDialog.requestFocus();
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   * @author Tan Hock Zoon
   */
  private void handleImageGenerationObservable(Object arg)
  {
    Assert.expect(arg instanceof ImageGenerationEvent);
    ImageGenerationEvent imageGenerationEvent = (ImageGenerationEvent) arg;
    ImageGenerationEventEnum imageGenerationEventEnum = imageGenerationEvent.getImageGenerationEventEnum();

    if (imageGenerationEventEnum.equals(ImageGenerationEventEnum.BEGIN_ACQUIRE_VERIFICATION_IMAGES))
    {
      Assert.expect(arg instanceof ImageGenerationEvent);
      developerDebug("BEGIN_ACQUIRE_VERIFICATION_IMAGES");
      _totalVerificationImages = imageGenerationEvent.getNumEventsToExpect();
      createVerificationGenerationProgressDialog();
      _verificationGenerationProgressDialog.setVisible(true);
    }
    if (imageGenerationEventEnum.equals(ImageGenerationEventEnum.END_ACQUIRE_VERIFICATION_IMAGES))
    {
      Assert.expect(arg instanceof ImageGenerationEvent);
      developerDebug("END_ACQUIRE_VERIFICATION_IMAGES");
    }
    else if (imageGenerationEventEnum.equals(ImageGenerationEventEnum.VERIFICATION_IMAGE_ACQUIRED))
    {
      _currentVerificationImages++;
      _lastPercentDone = (_currentVerificationImages * 100) / _totalVerificationImages;
      if (_lastPercentDone == 100)
      {
        _lastPercentDone = 99;
      }

      if (_verificationGenerationProgressDialog != null)
      {
        _verificationGenerationProgressDialog.updateProgressBarPrecent(_lastPercentDone);
      }
    }
    if (imageGenerationEventEnum.equals(ImageGenerationEventEnum.BEGIN_ACQUIRE_FOCUS_SET_IMAGES))
    {
      Assert.expect(arg instanceof ImageGenerationEvent);
      createFocusSetGenerationProgressDialog();
      _focusSetGenerationProgressDialog.setVisible(true);
    }
    if (imageGenerationEventEnum.equals(ImageGenerationEventEnum.BEGIN_ACQUIRE_OFFLINE_IMAGES))
    {
      Assert.expect(arg instanceof ImageGenerationEvent);
      developerDebug("BEGIN_ACQUIRE_OFFLINE_IMAGES");
      createOfflineInspectionImageGenerationProgressDialog();
      _offlineInspectionImageGenerationProgressDialog.setVisible(true);
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  private void handleImageReconstructionObservable(Object arg)
  {
    Assert.expect(arg instanceof ImageReconstructionEvent);
    ImageReconstructionEvent imageReconstructionEvent = (ImageReconstructionEvent) arg;
    ImageReconstructionEventEnum imageReconstructionEventEnum = imageReconstructionEvent.getImageReconstructionEventEnum();

    //Khaw Chek Hau - XCR3609: Production Future Serial Number Cursor Not Lock
    if (isTestExecCurrentEnvironment() == false)
    {
      if (imageReconstructionEventEnum.equals(ImageReconstructionEventEnum.BEGIN_IRP_CONNECT_TO_CAMERAS))
      {
        if (_cameraConnectionBusyDialog == null)
        {
          createCameraConnectionBusyDialog();
          _cameraConnectionBusyDialog.setVisible(true);
        }
      }
      else if (imageReconstructionEventEnum.equals(ImageReconstructionEventEnum.END_IRP_CONNECT_TO_CAMERAS))
      {
        closeBusyCancelDialog(_cameraConnectionBusyDialog, false);
        _cameraConnectionBusyDialog = null;
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleInspectionEventObservable(Object arg)
  {
    Assert.expect(arg instanceof InspectionEvent);

    InspectionEvent inspectionEvent = (InspectionEvent) arg;
    InspectionEventEnum inspectionEventEnum = inspectionEvent.getInspectionEventEnum();

    if (inspectionEventEnum.equals(InspectionEventEnum.RUNTIME_ALIGNMENT_STARTED))
    {
      developerDebug("RUNTIME_ALIGNMENT_STARTED");
      if (_alignmentProgressDialog == null)
      {
        _currentlyAligningGroup = 0;
        // open alignment status dialog
        createAlignmentProgressCancelDialog();
        _alignmentProgressDialog.setVisible(true);
      }
    }
    else if (inspectionEventEnum.equals(InspectionEventEnum.RUNTIME_ALIGNMENT_COMPLETED))
    {
      developerDebug("RUNTIME_ALIGNMENT_COMPLETED");
      if (_alignmentProgressDialog != null)
      {
        // close alignment dialog
        closeProgressDialog(_alignmentProgressDialog, false);
        _alignmentProgressDialog = null;
      }
    }
    else if (inspectionEventEnum.equals(InspectionEventEnum.ALIGNED_ON_IMAGE))
    {
      developerDebug("ALIGNED_ON_IMAGE");
      if (_alignmentProgressDialog == null)
      {
        _currentlyAligningGroup = 1;

        // open alignment status dialog
        createAlignmentProgressCancelDialog();
        _lastPercentDone = (_currentlyAligningGroup * 100) / 3;
        _alignmentProgressDialog.updateProgressBarPrecent(_lastPercentDone);
        _alignmentProgressDialog.setVisible(true);
      }
      else
      {
        _currentlyAligningGroup++;
        _lastPercentDone = (_currentlyAligningGroup * 100) / 3;
        if (_lastPercentDone == 100)
        {
          _lastPercentDone = 99;
        }
        if (_alignmentProgressDialog != null)
        {
          _alignmentProgressDialog.updateProgressBarPrecent(_lastPercentDone);
        }
      }
    }
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    else if (inspectionEventEnum.equals(InspectionEventEnum.ALIGNED_ON_2D_IMAGE))
    {
      developerDebug("ALIGNED_ON_IMAGE");
      if (_alignmentProgressDialog == null)
      {
        _currentlyAligningGroup = 1;

        // open alignment status dialog
        createAlignmentProgressCancelDialog();
        _lastPercentDone = (_currentlyAligningGroup * 100) / 3;
        _alignmentProgressDialog.updateProgressBarPrecent(_lastPercentDone);
        _alignmentProgressDialog.setVisible(true);
      }
      else
      {
        _currentlyAligningGroup++;
        _lastPercentDone = (_currentlyAligningGroup * 100) / 3;
        if (_lastPercentDone == 100)
        {
          _lastPercentDone = 99;
        }
        if (_alignmentProgressDialog != null)
        {
          _alignmentProgressDialog.updateProgressBarPrecent(_lastPercentDone);
        }
      }
    }
    else if (inspectionEventEnum.equals(InspectionEventEnum.RUNTIME_SURFACE_MAP_STARTED))
    {
        developerDebug("RUNTIME_SURFACE_MAP_STARTED");
        if (_surfaceMapProgressDialog == null)
        {
           _currentlySurfaceMap = 0;
           
           SurfaceMapInspectionEvent surfaceMapInspectionEvent = (SurfaceMapInspectionEvent)inspectionEvent;
           _totalSurfaceMapPoints = surfaceMapInspectionEvent.getTotalNumberOfPointToPointScans();
           
           // open surface map dialog
           createSurfaceMapProgressCancelDialog();
           _surfaceMapProgressDialog.setVisible(true);
        }
    }
    else if (inspectionEventEnum.equals(InspectionEventEnum.RUNTIME_SURFACE_MAP_COMPLETED))
    {
        developerDebug("RUNTIME_SURFACE_MAP_COMPLETED");
        if (_surfaceMapProgressDialog != null)
        {
            // close surface map dialog
            closeProgressDialog(_surfaceMapProgressDialog, false);
            _surfaceMapProgressDialog = null;
        }
    }
    else if (inspectionEventEnum.equals(InspectionEventEnum.SURFACE_MAP_ON_OPTICAL_IMAGE))
    {
        developerDebug("SURFACE_MAP_ON_OPTICAL_IMAGE");
        if (_surfaceMapProgressDialog == null)
        {
            _currentlySurfaceMap = 1;
            if (_totalSurfaceMapPoints <= 0) _totalSurfaceMapPoints = 1;

            // open surface map status dialog
            createSurfaceMapProgressCancelDialog();
            _lastPercentDone = (_currentlySurfaceMap * 100) / _totalSurfaceMapPoints;
            _surfaceMapProgressDialog.updateProgressBarPrecent(_lastPercentDone);
            _surfaceMapProgressDialog.setVisible(true);
        }
        else
        {
            _currentlySurfaceMap++;
            _lastPercentDone = (_currentlySurfaceMap * 100) / _totalSurfaceMapPoints;
            if (_lastPercentDone == 100)
            {
              _lastPercentDone = 99;
            }
            if (_surfaceMapProgressDialog != null)
            {
              _surfaceMapProgressDialog.updateProgressBarPrecent(_lastPercentDone);
            }
        }
    }
    else if (inspectionEventEnum.equals(InspectionEventEnum.RUNTIME_ALIGNMENT_SURFACE_MAP_STARTED))
    {
        developerDebug("RUNTIME_ALIGNMENT_SURFACE_MAP_STARTED");
        if (_alignmentSurfaceMapProgressDialog == null)
        {
           _currentlyAlignmentSurfaceMap = 0;
           
           SurfaceMapInspectionEvent surfaceMapInspectionEvent = (SurfaceMapInspectionEvent)inspectionEvent;
           _totalAlignmentSurfaceMapPoints = surfaceMapInspectionEvent.getTotalNumberOfPointToPointScans();
          
           // open surface map dialog
           createAlignmentSurfaceMapProgressCancelDialog();
           _alignmentSurfaceMapProgressDialog.setVisible(true);
        }
    }
    else if (inspectionEventEnum.equals(InspectionEventEnum.RUNTIME_ALIGNMENT_SURFACE_MAP_COMPLETED))
    {
        developerDebug("RUNTIME_ALIGNMENT_SURFACE_MAP_COMPLETED");
        if (_alignmentSurfaceMapProgressDialog != null)
        {
            // close surface map dialog
            closeProgressDialog(_alignmentSurfaceMapProgressDialog, false);
            _alignmentSurfaceMapProgressDialog = null;
        }
    }
    else if (inspectionEventEnum.equals(InspectionEventEnum.ALIGNMENT_SURFACE_MAP_ON_OPTICAL_IMAGE))
    {
        developerDebug("ALIGNMENT_SURFACE_MAP_ON_OPTICAL_IMAGE");
        if (_alignmentSurfaceMapProgressDialog == null)
        {
            _currentlyAlignmentSurfaceMap = 1;
            if (_totalAlignmentSurfaceMapPoints <= 0) _totalAlignmentSurfaceMapPoints = 1;

            // open surface map status dialog
            createSurfaceMapProgressCancelDialog();
            _lastPercentDone = (_currentlyAlignmentSurfaceMap * 100) / _totalAlignmentSurfaceMapPoints;
            _alignmentSurfaceMapProgressDialog.updateProgressBarPrecent(_lastPercentDone);
            _alignmentSurfaceMapProgressDialog.setVisible(true);
        }
        else
        {
            _currentlyAlignmentSurfaceMap++;
            _lastPercentDone = (_currentlyAlignmentSurfaceMap * 100) / _totalAlignmentSurfaceMapPoints;
        
            if (_lastPercentDone == 100 || _lastPercentDone > 100)
            {
              _lastPercentDone = 99;
            }
            if (_alignmentSurfaceMapProgressDialog != null)
            {
              _alignmentSurfaceMapProgressDialog.updateProgressBarPrecent(_lastPercentDone);
            }
        }
    }
    //Khaw Chek Hau - XCR3761 : Skip generating 2D verification image if the panel is not unloaded
    else if (inspectionEventEnum.equals(InspectionEventEnum.PANEL_UNLOADED_FROM_MACHINE))
    {
      if (_project != null)
      {
        _project.getTestProgram().setPanelRemainsWithoutUnloadedFor2DVerificationImage(false);
      }
    }
  }

  /**
   * @author George Booth
   */
  private void handleProjectObservable(Object arg)
  {
    Assert.expect(arg != null);
    if (arg instanceof ProjectChangeEvent)
    {
      ProjectChangeEvent event = (ProjectChangeEvent) arg;
      ProjectChangeEventEnum eventEnum = event.getProjectChangeEventEnum();
      if (eventEnum instanceof ProjectStateEventEnum)
      {
        String value = System.getenv("AUTOMATIC_TEST");
        if ((value == null) || (value.equalsIgnoreCase("true") == false))
        {
          ProjectStateEventEnum psee = (ProjectStateEventEnum) eventEnum;
          // debug
          if (TestDev.isInDeveloperDebugMode())
          {
            decodeAndOutputProjectStateChangeEvent(psee);
          }

          if (psee.equals(ProjectStateEventEnum.TEST_PROGRAM_INVALID))
          {
            setTestProgramDirtyFlag(true);
          }
          if (psee.equals(ProjectStateEventEnum.TEST_PROGRAM_VALID))
          {
            setTestProgramDirtyFlag(false);
          }
          if (psee.equals(ProjectStateEventEnum.PROJECT_MINOR_STATE_INVALID))
          {
            setProjectDirtyFlag(true);
          }
          if (psee.equals(ProjectStateEventEnum.PROJECT_MINOR_STATE_VALID))
          {
            setProjectDirtyFlag(false);
          }
          if (psee.equals(ProjectStateEventEnum.PROJECT_MAJOR_STATE_INVALID))
          {
            setProjectDirtyFlag(true);
          }
          if (psee.equals(ProjectStateEventEnum.PROJECT_MAJOR_STATE_VALID))
          {
            setProjectDirtyFlag(false);
          }
          if (psee.equals(ProjectStateEventEnum.VERIFICATION_IMAGES_INVALID))
          {
          }
          if (psee.equals(ProjectStateEventEnum.VERIFICATION_IMAGES_VALID))
          {
          }
          if (psee.equals(ProjectStateEventEnum.ALIGNMENT_TRANSFORM_INVALID))
          {
          }
          if (psee.equals(ProjectStateEventEnum.ALIGNMENT_TRANSFORM_VALID))
          {
          }
        }
      }
      if (eventEnum instanceof ProjectEventEnum)
      {
        ProjectEventEnum projectEventEnum = (ProjectEventEnum) eventEnum;
        // debug
        if (TestDev.isInDeveloperDebugMode())
        {
          decodeAndOutputProjectEvent(projectEventEnum);
        }

        if (projectEventEnum.equals(ProjectEventEnum.FAST_LOAD)
                || projectEventEnum.equals(ProjectEventEnum.SLOW_LOAD)
                || projectEventEnum.equals(ProjectEventEnum.IMPORT_PROJECT_FROM_NDFS))
        {
          setTestProgramDirtyFlag(false);
          setProjectDirtyFlag(false);
        }
        else if (projectEventEnum == ProjectEventEnum.SAVE)
        {
          setTestProgramDirtyFlag(false);
          setProjectDirtyFlag(false);
        }
        else if (projectEventEnum.equals(ProjectEventEnum.NAME))
        {
          updateTitle();
        }
        else if (projectEventEnum.equals(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_SYSTEM_TYPE))
        {
          // check license type 
          boolean isOLP = false;
          try
          {
            isOLP = LicenseManager.isOfflineProgramming();
          }
          catch (final BusinessException e)
          {
            finishLicenseMonitor();
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                        e.getMessage(),
                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                        true);
                MainMenuGui.getInstance().exitV810(false);
              }
            });
          }
          //if license type not olp, notify user mismatch and ask permision to convert
          if (isOLP == false)
          {
            notifyUserOfProjectWithMismatchingSystemType(event);
          }
          else
          {
            // check selected project system type and supported system type;
            // if system type is supported, direct load new system type config setting in software
            // reset magnification value to prevent image not compability and reset graphic engine to set new mag setting.
            Pair<String, String> projectNameToSystemType = (Pair<String,String>)event.getSource();
            String systemType = projectNameToSystemType.getSecond();
            SystemTypeEnum systemTypeEnum = SystemTypeEnum.getSystemType(systemType);
            if (LicenseManager.checkSupportedSystemTypeForOfflineProgramming(systemTypeEnum))
            {
              try
              {
                Config.getInstance().forceLoad(systemTypeEnum); 
                ProgramGeneration.reintializeDueToSystemTypeChange();
                com.axi.v810.datastore.projReaders.ProjectReader.reintializeDueToSystemTypeChange();
                ProjectWriter.reintializeDueToSystemTypeChange();
                Project.setupProjectReaderWriter();
                if (systemTypeEnum.equals(SystemTypeEnum.S2EX))
                {
                  this.getMainMenuBar().enableChangeMagnificationMenu();
                  this.getMainToolBar().enableChangeSystemMagnificationButton();
                  _envToolBar.enableChangeSystemMagnificationButton();
                }
                else
                {
                  this.getMainToolBar().disableChangeSystemMagnificationButton();
                  this.getMainMenuBar().disableChangeMagnificationMenu();
                  _envToolBar.disableChangeSystemMagnificationButton();
                }
                MagnificationEnum.resetMagnification();
                this.getTestDev().refresh();     
                Project.userResponseToSystemTypesDiffer(true, false);  
              }
              catch (final DatastoreException e)
              {
                handleOfflineProgrammingDataStoreError(e);    
                Project.userResponseToSystemTypesDiffer(false, false); 
              }
            }
            // project system type is not supported in license. ask user permision to convert to user selected system type
            else
            {
              notifyUserSystemTypeUnsupportedOfflineProgrammingSystemType(event);
            }
          }
        }
        else if (projectEventEnum.equals(ProjectEventEnum.LOADING_LONG_PANEL_WITH_DIFFERENT_SYSTEM_TYPE))
        {
          // Gives warning message that v810 handles long panel differently,
          // which is always split long panel into half
          LocalizedString alignmentPointsMessage = new LocalizedString("MMGUI_DIFFERENT_ALIGNMENT_POINTS_WARNING_MESSAGE_KEY", null);
          JOptionPane.showConfirmDialog(this,
                  StringUtil.format(StringLocalizer.keyToString(alignmentPointsMessage), _CHARS_PER_LINE),
                  StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                  JOptionPane.DEFAULT_OPTION,
                  JOptionPane.WARNING_MESSAGE);
        }
        else if (projectEventEnum.equals(ProjectEventEnum.LIBRARY_IMPORT_COMPLETE) || projectEventEnum.equals(ProjectEventEnum.IMPORT_LAND_PATTERN_COMPLETE))
        {
          setTestProgramDirtyFlag(false);
          setProjectDirtyFlag(false);
        }
        else if (projectEventEnum.equals(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_MAGNIFICATION_FOR_OFFLINE_PROGRAMMING))
        {
          try
          {
            String projectMagnification = (String)event.getSource();     
            Config.getInstance().setValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION,projectMagnification);
            Config.getInstance().forceLoad(XrayTester.getSystemType());
            ProgramGeneration.reintializeDueToSystemTypeChange();
            com.axi.v810.datastore.projReaders.ProjectReader.reintializeDueToSystemTypeChange();
            ProjectWriter.reintializeDueToSystemTypeChange();
            Project.setupProjectReaderWriter();
            if (XrayTester.getSystemType().equals(SystemTypeEnum.S2EX))
            {
              this.getMainMenuBar().enableChangeMagnificationMenu();
              this.getMainToolBar().enableChangeSystemMagnificationButton();
              _envToolBar.enableChangeSystemMagnificationButton();
            }
            else
            {
              this.getMainToolBar().disableChangeSystemMagnificationButton();
              this.getMainMenuBar().disableChangeMagnificationMenu();
              _envToolBar.disableChangeSystemMagnificationButton();
            }
            MagnificationEnum.resetMagnification();
            this.getTestDev().refresh();                
            Project.userResponseToProceedWithProjectLoad(true);
          }
          catch (final DatastoreException e)
          {
            handleOfflineProgrammingDataStoreError(e);
            Project.userResponseToProceedWithProjectLoad(false);
          }
        }
        else if (projectEventEnum.equals(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_LOW_MAGNIFICATION)) //Ngie Xing
        {
          notifyUserOfProjectWithMismatchLowMagnification((Project) event.getSource(),
          (String) event.getOldValue(), (String) event.getNewValue());
        }
        else if (projectEventEnum.equals(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_HIGH_MAGNIFICATION)) //Ngie Xing
        {
          notifyUserOfProjectWithMismatchHighMagnification((Project) event.getSource(),
            (String) event.getOldValue(), (String) event.getNewValue());
        }
        else if (projectEventEnum.equals(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_THICKNESS_TABLE_VERSION)) //Swee Yee
        {
          notifyUserOfNewerSolderThicknessVersionExists((String) event.getOldValue(), (String) event.getNewValue());
        }
        else if (projectEventEnum.equals(ProjectEventEnum.LOADING_PROJECT_WITH_ALTERNATIVE_THICKNESS_TABLE_VERSION)) //Swee Yee
        {
          notifyUserOfAlternativeSolderThicknessVersionExists((String) event.getOldValue(), (String) event.getNewValue());
        }
      }
      if (eventEnum instanceof CompPackageEventEnum)
      {
        CompPackageEventEnum compPackageEventEnum = (CompPackageEventEnum) eventEnum;
        if (compPackageEventEnum.equals(CompPackageEventEnum.EXPORT_COMPLETE_ENUM))
        {
          setProjectDirtyFlag(true);
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  private void handleGuiObservable(Object arg)
  {
    Assert.expect(arg != null);
    if (arg instanceof GuiEvent)
    {
      GuiEvent event = (GuiEvent) arg;
      GuiEventEnum eventEnum = event.getGuiEventEnum();
      if (eventEnum instanceof TaskPanelScreenEnum)
      {
        _currentScreenEnum = (TaskPanelScreenEnum) eventEnum;
        updateTitle();
      }
    }
  }

  /**
   * @author George Booth
   */
  private void handleHardwareObservable(Object arg)
  {
    Assert.expect(arg != null);
    if (arg instanceof XrayTesterException)
    {
      developerDebug("XrayTesterException seen in handleHardwareObservable()");
      //  not calling handleXrayTesterException() here as it closes all open dialogs
      // we don't want to close them if we got this exception notified to us
      if (_serviceEnvironmentNotActive)
      {
        developerDebug("Showing xrayTesterException");
        MessageDialog.showErrorDialog(
                MainMenuGui.this,
                ((XrayTesterException) arg).getMessage(),
                StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                true);
        changeLightStackToStandby();
      }

    }
    // Hardware events
    else if (arg instanceof HardwareEvent)
    {
      HardwareEvent event = (HardwareEvent) arg;
      HardwareEventEnum eventEnum = event.getEventEnum();
      // Note: The MainMenuGui hardware observer is removed if the Service or Test Exec environment is active.
      // This code should be duplicated in the appropriate classes for hardware events to be seen.
      if (_DEVELOPER_DEBUG || _DEVELOPER_PERFORMANCE)
      {
        HardwareEventDebug.decodeAndOutputHardwareEvent(event);
      }

      // Xray Tester events
      if (eventEnum instanceof XrayTesterEventEnum)
      {
        XrayTesterEventEnum xrayTesterEventEnum = (XrayTesterEventEnum) eventEnum;
        if (xrayTesterEventEnum.equals(XrayTesterEventEnum.STARTUP))
        {
          if (event.isStart())
          {
            _systemStartupInProgress = true;

            if (_userInvokedActionInProgress == false)
            {
              developerDebug("MainMenuGui got a startup START event");
              createStartupProgressCancelDialog();
              _startupProgressCancelDialog.setVisible(true);
            }
          }
          else  // end event
          {
            if (_userInvokedActionInProgress == false)
            {
              developerDebug("MainMenuGui got a startup FINISH event");
              closeProgressDialog(_startupProgressCancelDialog, false);
              _startupProgressCancelDialog = null;
            }
            _systemStartupInProgress = false;
            //XCR-3409, Failed to run CDNA although already select there are No board in the system
            _digitalIo.setByPassPanelClearSensor(false);
          }
        }
        else if (xrayTesterEventEnum.equals(XrayTesterEventEnum.SHUTDOWN))
        {
          if (event.isStart())
          {
            if (_userInvokedActionInProgress == false)
            {
              developerDebug("MainMenuGui got a shutdown START event");
              _systemShutdownInProgress = true;
              createShutdownBusyDialog();
              _shutdownBusyDialog.setVisible(true);
            }
          }
          else
          {
            if (_userInvokedActionInProgress == false)
            {
              developerDebug("MainMenuGui got a shutdown FINISH event");
              closeBusyCancelDialog(_shutdownBusyDialog, false);
              _shutdownBusyDialog = null;
              _systemShutdownInProgress = false;
            }
          }
        }
      }
      // Xray Source Events
      else if (eventEnum instanceof XraySourceEventEnum)
      {
        // handled in XrayStatusPanel
        // XCR1144, Chnee Khang Wah, 29-Dec-2010
        XraySourceEventEnum xraySourceEventEnum = (XraySourceEventEnum) eventEnum;

        if (xraySourceEventEnum.equals(XraySourceEventEnum.POWER_OFF))
        {
          if (event.isStart())
          {
            createxRaySourcePowerOffProgressDialog();
          }
          else
          {
            if (_xRaySourcePowerOffProgressDialog != null)
            {
              _xRaySourcePowerOffProgressDialog.dispose();
              _xRaySourcePowerOffProgressDialog = null;
            }
          }
        }
        // XCR1144 - end
      }
      // Digital IO Events
      else if (eventEnum instanceof DigitalIoEventEnum)
      {
        DigitalIoEventEnum digitalIoEventEnum = (DigitalIoEventEnum) eventEnum;
        if (digitalIoEventEnum.equals(DigitalIoEventEnum.INITIALIZE))
        {
          // don't do subsystem dialog if main startup dialog is already open
          if (_startupProgressCancelDialog == null)
          {
            if (event.isStart())
            {
              developerDebug("MainMenuGui DigitalIoEventEnum.INITIALIZE START event");
              // something has triggered a subsystem init before startup
              developerDebug("MainMenuGui subsystem auto startup");
              createAutoStartupProgressDialog();
              _autoStartupProgressDialog.setVisible(true);
            }
            else
            {
              developerDebug("MainMenuGui DigitalIoEventEnum.INITIALIZE FINISH event");
              developerDebug("MainMenuGui subsystem auto startup finished");
              closeProgressDialog(_autoStartupProgressDialog, false);
              _autoStartupProgressDialog = null;
            }
          }
        }
      }
      // Panel Positioner Events
      else if (eventEnum instanceof PanelPositionerEventEnum)
      {
        PanelPositionerEventEnum panelPositionerEventEnum = (PanelPositionerEventEnum) eventEnum;
        if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.INITIALIZE))
        {
          // don't do subsystem dialog if main startup dialog is already open
          if (_startupProgressCancelDialog == null)
          {
            if (event.isStart())
            {
              developerDebug("MainMenuGui PanelPositionerEventEnum.INITIALIZE START event");
              // something has triggered a subsystem init before startup
              developerDebug("MainMenuGui subsystem auto startup");
              createAutoStartupProgressDialog();
              _autoStartupProgressDialog.setVisible(true);
            }
            else
            {
              developerDebug("MainMenuGui PanelPositionerEventEnum.INITIALIZE FINISH event");
              developerDebug("MainMenuGui subsystem auto startup finished");
              closeProgressDialog(_autoStartupProgressDialog, false);
              _autoStartupProgressDialog = null;
            }
          }
        }
      }
      // Motion Controller initialize events are seen during Panel Handler init
      else if (eventEnum instanceof MotionControlEventEnum)
      {
        // do nothing
      }
      // Panel Handler Events
      else if (eventEnum instanceof PanelHandlerEventEnum)
      {
        PanelHandlerEventEnum panelHandlerEventEnum = (PanelHandlerEventEnum) eventEnum;
        if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.INITIALIZE))
        {
          if (event.isStart())
          {
            if (_startupProgressCancelDialog == null)
            {
              developerDebug("MainMenuGui PanelHandlerEventEnum.INITIALIZE START event");
              // something has triggered a subsystem init before startup
              developerDebug("MainMenuGui subsystem auto startup");
              createAutoStartupProgressDialog();
              _autoStartupProgressDialog.setVisible(true);
            }
          }
          else
          {
            developerDebug("MainMenuGui PanelHandlerEventEnum.INITIALIZE FINISH event");
            developerDebug("MainMenuGui subsystem auto startup finished");
            closeProgressDialog(_autoStartupProgressDialog, false);
            _autoStartupProgressDialog = null;
          }
        }
        else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.LOAD_PANEL))
        {
          if (event.isStart())
          {
            _panelLoadInProgress = true;
            if (_userInvokedActionInProgress == false)
            {
              developerDebug("MainMenuGui panel load START event");
              createPanelLoadBusyCancelDialog();
              _panelLoadBusyCancelDialog.setVisible(true);
            }
          }
          else
          {
            if (_userInvokedActionInProgress == false)
            {
              developerDebug("MainMenuGui panel load FINISH event");
              closeBusyCancelDialog(_panelLoadBusyCancelDialog, false);
              _panelLoadBusyCancelDialog = null;
              _homeEnvironment.systemRequestCompleted();
              _panelLoadInProgress = false;
            }
            _baseMenuBar.setActionMenuStates();
          }
        }
        else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.UNLOAD_PANEL))
        {
          if (event.isStart())
          {
            if (_userInvokedActionInProgress == false)
            {
              developerDebug("MainMenuGui panel unload START event");
            }
          }
          else
          {
            if (_userInvokedActionInProgress == false)
            {
              developerDebug("MainMenuGui panel unload FINISH event");
            }
            _baseMenuBar.setActionMenuStates();
          }
        }
        else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.HOME_STAGE_RAILS))
        {
          if (event.isStart())
          {
            // do nothing -- all HOME_STAGE_RAILS worth showing to user are invoked from the UI menu
          }
          else
          {
            // do nothing -- all HOME_STAGE_RAILS worth showing to user are invoked from the UI menu
          }
        }
      }
      // Cal Events
      else if (eventEnum instanceof HardwareTaskEventEnum)
      {
        HardwareTaskEventEnum calEventEnum = (HardwareTaskEventEnum) eventEnum;
        if (calEventEnum.equals(HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT))
        {
          if (event.isStart())
          {
            if (_systemStartupInProgress)
            {
              developerDebug("Ignoring HARDWARE_TASK_EXECUTION_EVENT started event during system startup");
            }
            else if (_serviceEnvironmentNotActive == false)
            {
              developerDebug("Ignoring HARDWARE_TASK_EXECUTION_EVENT started event with Service UI active");
            }
            else
            {
              developerDebug("HARDWARE_TASK_EXECUTION_EVENT started event" + event.getSource());
              createAutoAdjustProgressDialog();
              _autoAdjustProgressDialog.setVisible(true);
            }
          }
          else
          {
            developerDebug("HARDWARE_TASK_EXECUTION_EVENT finished event" + event.getSource());
            closeProgressDialog(_autoAdjustProgressDialog, false);
            _autoAdjustProgressDialog = null;
          }
        }
      }
    }
  }

  /**
   * @author George Booth
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      // call requestFocus here for force any focuslost listeners to be invoked before we exit
//      this.requestFocus();
      if (_currentEnvironment != _testExecEnvironment)
      {
        makeActive(false);
      }
      else
      {
        // in test execution, ignore the attempt to close this window if a test is running
        if (_currentEnvironment.isReadyToFinish())
        {
          makeActive(false);
        }
        else
        {
          // we now know that we're in the Test Execution and a test is running, so inform them why we're not exiting
          MessageDialog.showErrorDialog(this,
                  StringLocalizer.keyToString("GUI_TEST_RUNNING_ON_EXIT_KEY"),
                  StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"),
                  true,
                  FontUtil.getMediumFont());
        }
      }
    }
  }

  /**
   * @author George Booth
   */
  public void makeActive(final boolean visible)
  {
    String currentUserName = LoginManager.getInstance().getUserNameFromLogIn();
    Assert.expect(SwingUtilities.isEventDispatchThread());

    if (visible)
    {
      setVisible(true);
    }
    else
    {
      int choice = ChoiceInputDialog.showConfirmDialog(MainMenuGui.this,
              StringLocalizer.keyToString("TESTEXEC_GUI_CONFIRM_EXIT_KEY"),
              _appName);
      if (choice == JOptionPane.YES_OPTION)
      {
        _fileWriter.appendCloseV810(currentUserName);
        //Khaw Chek Hau - XCR2654: CAMX Integration
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
        {
          try
          {
            _shopFloorSystem.softwarePowerOff();
          }
          catch (DatastoreException ex)
          {
            MessageDialog.showErrorDialog(
                    MainMenuGui.this,
                    ex.getLocalizedMessage() + StringLocalizer.keyToString("SHOP_FLOOR_ERROR_MESSAGE_KEY_KEY"),
                    StringLocalizer.keyToString("SHOP_FLOOR_EVENT_TITLE_KEY"),
                    true);
          }
        }
        
        //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT))
        {
          try
          {
            _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.SOFTWARE_POWER_OFF);
          }
          catch (DatastoreException ex)
          {
            String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_ERROR_MESSAGE_KEY");
            MessageDialog.showErrorDialog(MainMenuGui.this,
                                          errorMsg,
                                          StringLocalizer.keyToString("MACHINE_UTILIZATION_REPORT_EVENT_TITLE_KEY"),
                                          true,
                                          FontUtil.getMediumFont());
          }
        }
        
        //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
        {
          try
          {
            _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.SOFTWARE_OFF);
          }
          catch (DatastoreException ex)
          {
            String errorMsg = ex.getLocalizedMessage() + System.getProperty("line.separator") + System.getProperty("line.separator") + StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_MESSAGE_KEY");
            MessageDialog.showErrorDialog(MainMenuGui.this,
                                          errorMsg,
                                          StringLocalizer.keyToString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_EVENT_TITLE_KEY"),
                                          true,
                                          FontUtil.getMediumFont());
          }
        }
    
        exitV810(false);
      }
    }
  }

  /**
   * @author George Booth
   */
  private void decodeAndOutputProjectStateChangeEvent(ProjectStateEventEnum psee)
  {
    System.out.print("glb - ProjectStateEventEnum.");
    if (psee.equals(ProjectStateEventEnum.TEST_PROGRAM_VALID))
    {
      System.out.println("TEST_PROGRAM_VALID");
    }
    else if (psee.equals(ProjectStateEventEnum.TEST_PROGRAM_INVALID))
    {
      System.out.println("TEST_PROGRAM_INVALID");
    }
    else if (psee.equals(ProjectStateEventEnum.PROJECT_MAJOR_STATE_VALID))
    {
      System.out.println("PROJECT_MAJOR_STATE_VALID");
    }
    else if (psee.equals(ProjectStateEventEnum.PROJECT_MAJOR_STATE_INVALID))
    {
      System.out.println("PROJECT_MAJOR_STATE_INVALID");
    }
    else if (psee.equals(ProjectStateEventEnum.PROJECT_MINOR_STATE_VALID))
    {
      System.out.println("PROJECT_MINOR_STATE_VALID");
    }
    else if (psee.equals(ProjectStateEventEnum.PROJECT_MINOR_STATE_INVALID))
    {
      System.out.println("PROJECT_MINOR_STATE_INVALID");
    }
    else if (psee.equals(ProjectStateEventEnum.VERIFICATION_IMAGES_VALID))
    {
      System.out.println("VERIFICATION_IMAGES_VALID");
    }
    else if (psee.equals(ProjectStateEventEnum.VERIFICATION_IMAGES_INVALID))
    {
      System.out.println("VERIFICATION_IMAGES_INVALID");
    }
    else if (psee.equals(ProjectStateEventEnum.ALIGNMENT_TRANSFORM_VALID))
    {
      System.out.println("MANUAL_ALIGNMENT_VALID");
    }
    else if (psee.equals(ProjectStateEventEnum.ALIGNMENT_TRANSFORM_INVALID))
    {
      System.out.println("MANUAL_ALIGNMENT_INVALID");
    }
    else if (psee.equals(ProjectStateEventEnum.SHORT_LEARNING_FILE_RECORDS_VALID))
    {
      System.out.println("SHORT_LEARNING_VALID");
    }
    else if (psee.equals(ProjectStateEventEnum.SHORT_LEARNING_FILE_RECORDS_INVALID))
    {
      System.out.println("SHORT_LEARNING_INVALID");
    }
    else if (psee.equals(ProjectStateEventEnum.EXPECTED_IMAGE_LEARNING_FILE_RECORDS_VALID))
    {
      System.out.println("SHORT_LEARNING_VALID");
    }
    else if (psee.equals(ProjectStateEventEnum.EXPECTED_IMAGE_LEARNING_FILE_RECORDS_INVALID))
    {
      System.out.println("SHORT_LEARNING_INVALID");
    }
    else if (psee.equals(ProjectStateEventEnum.SUBTYPE_LEARNING_FILE_VALID))
    {
      System.out.println("SUBTYPE_LEARNING_FILE_VALID");
    }
    else if (psee.equals(ProjectStateEventEnum.SUBTYPE_LEARNING_FILE_INVALID))
    {
      System.out.println("SUBTYPE_LEARNING_FILE_INVALID");
    }
    else
    {
      System.out.println(psee.toString());
    }
  }

  /**
   * @author George Booth
   */
  private void decodeAndOutputProjectEvent(ProjectEventEnum pee)
  {
    System.out.print("glb - ProjectEventEnum.");
    if (pee.equals(ProjectEventEnum.CREATE_OR_DESTROY))
    {
      System.out.println("CREATE_OR_DESTROY");
    }
    else if (pee.equals(ProjectEventEnum.SLOW_LOAD))
    {
      System.out.println("SLOW_LOAD");
    }
    else if (pee.equals(ProjectEventEnum.FAST_LOAD))
    {
      System.out.println("FAST_LOAD");
    }
    else if (pee.equals(ProjectEventEnum.SAVE))
    {
      System.out.println("SAVE");
    }
    else if (pee.equals(ProjectEventEnum.DELETE))
    {
      System.out.println("DELETE");
    }
    else if (pee.equals(ProjectEventEnum.IMPORT_PROJECT_FROM_NDFS))
    {
      System.out.println("IMPORT_PROJECT_FROM_NDFS");
    }
    else if (pee.equals(ProjectEventEnum.NAME))
    {
      System.out.println("NAME");
    }
    else if (pee.equals(ProjectEventEnum.PANEL))
    {
      System.out.println("PANEL");
    }
    else if (pee.equals(ProjectEventEnum.PROJECT_TEST_PROGRAM_VERSION))
    {
      System.out.println("PROJECT_TEST_PROGRAM_VERSION");
    }
    else if (pee.equals(ProjectEventEnum.PROJECT_MINOR_VERSION))
    {
      System.out.println("PROJECT_MINOR_VERSION");
    }
    else if (pee.equals(ProjectEventEnum.SOFTWARE_VERSION_CREATION))
    {
      System.out.println("SOFTWARE_VERSION_CREATION");
    }
    else if (pee.equals(ProjectEventEnum.NOTES))
    {
      System.out.println("NOTES");
    }
    else if (pee.equals(ProjectEventEnum.DISPLAY_UNITS))
    {
      System.out.println("DISPLAY_UNITS");
    }
    else if (pee.equals(ProjectEventEnum.READY_FOR_PRODUCTION))
    {
      System.out.println("READY_FOR_PRODUCTION");
    }
    else if (pee.equals(ProjectEventEnum.TEST_PROGRAM_UPDATED_WITH_ALIGNMENT_REGIONS))
    {
      System.out.println("TEST_PROGRAM_UPDATED_WITH_ALIGNMENT_REGIONS");
    }
    else if (pee.equals(ProjectEventEnum.TEST_PROGRAM_GENERATED))
    {
      System.out.println("TEST_PROGRAM_GENERATED");
    }
    else if (pee.equals(ProjectEventEnum.VERIFICATION_IMAGES_GENERATED))
    {
      System.out.println("VERIFICATION_IMAGES_GENERATED");
    }
    else if (pee.equals(ProjectEventEnum.LAST_MODIFICATION_TIME))
    {
      System.out.println("LAST_MODIFICATION_TIME");
    }
    else if (pee.equals(ProjectEventEnum.TARGET_CUSTOMER_NAME))
    {
      System.out.println("TARGET_CUSTOMER_NAME");
    }
    else if (pee.equals(ProjectEventEnum.PROGRAMMER_NAME))
    {
      System.out.println("PROGRAMMER_NAME");
    }
    else if (pee.equals(ProjectEventEnum.PANEL_IMAGE_CREATION))
    {
      System.out.println("PANEL_IMAGE_CREATION");
    }
    else if (pee.equals(ProjectEventEnum.DELETE_SHORTS_LEARNING))
    {
      System.out.println("DELETE_SHORTS_LEARNING");
    }
    else if (pee.equals(ProjectEventEnum.DELETE_EXPECTED_IMAGE_LEARNING))
    {
      System.out.println("DELETE_EXPECTED_IMAGE_LEARNING");
    }
    //ShengChuan - Clear Tombstone
    else if (pee.equals(ProjectEventEnum.DELETE_EXPECTED_IMAGE_TEMPLATE_LEARNING))
    {
      System.out.println("DELETE_EXPECTED_IMAGE_TEMPLATE_LEARNING");
    }
    else if (pee.equals(ProjectEventEnum.DELETE_SUBTYPE_LEARNING))
    {
      System.out.println("DELETE_SUBTYPE_LEARNING");
    }
    else if (pee.equals(ProjectEventEnum.LIBRARY_IMPORT_COMPLETE))
    {
      System.out.println("LIBRARY_IMPORT_COMPLETE");
    }
    else if (pee.equals(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_SYSTEM_TYPE))
    {
      System.out.println("LOADING_PROJECT_WITH_DIFFERENT_SYSTEM_TYPE");
    }
    else if (pee.equals(ProjectEventEnum.LOADING_LONG_PANEL_WITH_DIFFERENT_SYSTEM_TYPE))
    {
      System.out.println("LOADING_LONG_PANEL_WITH_DIFFERENT_SYSTEM_TYPE");
    }
    else if (pee.equals(ProjectEventEnum.SYSTEM_TYPE))
    {
      System.out.println("SYSTEM_TYPE");
    }
    else if (pee.equals(ProjectEventEnum.SCAN_PATH_METHOD))
    {
      System.out.println("SCAN_PATH_METHOD");
    }
    else if (pee.equals(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_LOW_MAGNIFICATION))
    {
      System.out.println("LOADING_PROJECT_WITH_DIFFERENT_LOW_MAGNIFICATION");
    }
    else if (pee.equals(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_HIGH_MAGNIFICATION))
    {
      System.out.println("LOADING_PROJECT_WITH_DIFFERENT_HIGH_MAGNIFICATION");
    }
    else if (pee.equals(ProjectEventEnum.LOW_MAGNIFICATION_CHANGED))
    {
      System.out.println("LOW_MAGNIFICATION_CHANGED");
    }
    else if (pee.equals(ProjectEventEnum.HIGH_MAGNIFICATION_CHANGED))
    {
      System.out.println("HIGH_MAGNIFICATION_CHANGED");
    }
    else if (pee.equals(ProjectEventEnum.SET_THICKNESS_TABLE_VERSION))
    {
      System.out.println("SET_THICKNESS_TABLE_VERSION");
    }
    else if (pee.equals(ProjectEventEnum.SET_USE_NEW_SCAN_ROUTE_GENERATION))
    {
      System.out.println("SET_USE_NEW_SCAN_ROUTE_GENERATION");
    }
    else if (pee.equals(ProjectEventEnum.SET_USE_VARIABLE_DIVN))
    {
      System.out.println("SET_USE_VARIABLE_DIVN");
    }
    else if (pee.equals(ProjectEventEnum.SET_DYNAMIC_RANGE_OPTIMIZATION_VERSION))
    {
      System.out.println("SET_DYNAMIC_RANGE_OPTIMIZATION_VERSION");
    }
    else if (pee.equals(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_THICKNESS_TABLE_VERSION))
    {
      System.out.println("LOADING_PROJECT_WITH_DIFFERENT_THICKNESS_TABLE_VERSION");
    }
    else if (pee.equals(ProjectEventEnum.LOADING_PROJECT_WITH_ALTERNATIVE_THICKNESS_TABLE_VERSION))
    {
      System.out.println("LOADING_PROJECT_WITH_ALTERNATIVE_THICKNESS_TABLE_VERSION");
    }
    else if (pee.equals(ProjectEventEnum.IMPORT_LAND_PATTERN_COMPLETE))
    {
      System.out.println("LAND_PATTERN_IMPORT_COMPLETE");
    }
    else
    {
      System.out.println(pee.toString());
    }
  }

  /**
   * @author George Booth
   */
  private void developerDebug(String message)
  {
    if (_DEVELOPER_DEBUG)
    {
      System.out.println("MainMenuGui - " + message);
    }
  }

  /**
   * @author Laura Cormos
   */
  public void displayWarningForZeroNeighborsToPredictFrom()
  {
    JOptionPane.showMessageDialog(this,
            StringUtil.format(StringLocalizer.keyToString("ATGUI_ZERO_NEIGHBORS_TO_PREDICT_FROM_WARNING"), 50),
            StringLocalizer.keyToString("ATGUI_GUI_NAME_KEY"),
            JOptionPane.WARNING_MESSAGE);
  }

  /**
   * @author George A. David
   */
  public boolean isTestExecCurrentEnvironment()
  {
    return _currentEnvironment == _testExecEnvironment;
  }

  /**
   * @author George A. David
   */
  public boolean isServiceCurrentEnvironment()
  {
    return _currentEnvironment == _serviceEnvironment;
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public boolean isHomeCurrentEnvironment()
  {
    return _currentEnvironment == _homeEnvironment;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void exportPakageLibary()
  {
    if (_project != null)
    {
      ExportLibraryWizardDialog dialog = new ExportLibraryWizardDialog(MainMenuGui.getInstance(), true, _project);
      dialog.pack();
      SwingUtils.centerOnComponent(dialog, this);
      dialog.setVisible(true);
      dialog.dispose();
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  public void importPakageLibary()
  {
    if (_project != null)
    {
      ImportWizardDialog dialog = new ImportWizardDialog(MainMenuGui.getInstance(), true, _project);
      dialog.pack();
      SwingUtils.centerOnComponent(dialog, this);
      dialog.setVisible(true);
      dialog.dispose();
    }
  }
  
  
  /**
   * @author Ying-Huan.Chu
   */
  public void importLandPattern()
  {
    if (_project != null)
    {
      ImportLandPatternWizardDialog dialog = new ImportLandPatternWizardDialog(MainMenuGui.getInstance(), true, _project);
      dialog.pack();
      SwingUtils.centerOnComponent(dialog, this);
      dialog.setVisible(true);
      dialog.dispose();
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void exportLandPattern()
  {
    if (_project != null)
    {
      ExportLandPatternWizardDialog dialog = new ExportLandPatternWizardDialog(MainMenuGui.getInstance(), true, _project);
      dialog.pack();
      SwingUtils.centerOnComponent(dialog, this);
      dialog.setVisible(true);
      dialog.dispose();
    }
  }

  /**
   * @author sham khang shian
   */
  private void handleVirtualLiveImageGenerationObservable(Object arg)
  {
    Assert.expect(arg instanceof VirtualLiveImageGenerationEvent);
    
    VirtualLiveImageGenerationEvent imageGenerationEvent = (VirtualLiveImageGenerationEvent) arg;
    VirtualLiveImageGenerationEventEnum imageGenerationEventEnum = imageGenerationEvent.getVirtualLiveImageGenerationEventEnum();
    if (imageGenerationEventEnum.equals(VirtualLiveImageGenerationEventEnum.BEGIN_ACQUIRE_VIRTUALLIVE_IMAGES))
    {
      Assert.expect(arg instanceof VirtualLiveImageGenerationEvent);
      _totalVirtualLiveImages = imageGenerationEvent.getNumEventsToExpect();
      createVirtualLiveImageGenerationProgressDialog();
      _virtualLiveImageGenerationProgressDialog.setVisible(true);
    }
    if (imageGenerationEventEnum.equals(VirtualLiveImageGenerationEventEnum.END_ACQUIRE_VIRTUALLIVE_IMAGES))
    {
      Assert.expect(arg instanceof VirtualLiveImageGenerationEvent);
    }
    else if (imageGenerationEventEnum.equals(VirtualLiveImageGenerationEventEnum.VIRTUALLIVE_IMAGE_ACQUIRED))
    {
      _currentVirtualLiveImages++;
      _lastPercentDone = (_currentVirtualLiveImages * 100) / _totalVirtualLiveImages;
      if (_lastPercentDone == 100)
      {
        _lastPercentDone = 99;
      }
      if (_virtualLiveImageGenerationProgressDialog != null)
      {
        _virtualLiveImageGenerationProgressDialog.updateProgressBarPrecent(_lastPercentDone);
      }
    }

    if (imageGenerationEventEnum.equals(VirtualLiveImageGenerationEventEnum.ALIGNMENT_FAIL))
    {
      _xrayTesterException = imageGenerationEvent.getXrayTesterException();
      if (_xrayTesterException != null)
      {
        if (_xrayTesterException instanceof AlignmentAlgorithmFailedToLocatePadsBusinessException
                || _xrayTesterException instanceof AlignmentAlgorithmFailedToCalculateTransformBusinessException
                || _xrayTesterException instanceof AlignmentAlgorithmResultOutOfBoundsBusinessException)
        {
          VirtualLiveImageGenerationSettings.getInstance().setMultipleSelectComponent(false);
          VirtualLivePanel.getInstance().clearComponentList();
          handleAlignmentError(_xrayTesterException);
        }
        else
        {
          VirtualLiveImageGenerationSettings.getInstance().setMultipleSelectComponent(false);
          VirtualLivePanel.getInstance().clearComponentList();
          handleError(this, _xrayTesterException);
        }
      }
    }

    if (imageGenerationEventEnum.equals(VirtualLiveImageGenerationEventEnum.XRAYEXCEPTION_WHEN_GENERATE_VIRTUALLIVE_IMAGES))
    {
      _xrayTesterException = imageGenerationEvent.getXrayTesterException();
      handleError(this, _xrayTesterException);
    }


  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void handleAssertObservable()
  {
    // When assert happen, we want to make sure we explicitly 
    // shut down Psp-related hardware before exit the system.
    if (_enablePsp)
    {
      shutdownPspHardware();
    }
    
    _assertObservable.deleteObserver(this);

    // since we had an assert, there is really no need to continue, so exit the application
    System.exit(1);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void shutdownPspHardware()
  {
    try
    {
      PspHardwareEngine.getInstance().shutdown();
    }
    catch(XrayTesterException ex)
    {
      // Do nothing
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  private void handleAlignmentError(final XrayTesterException alignmentException)
  {
    Assert.expect(alignmentException != null);
    AlignmentFailedDialog alignmentFailedDialog = new AlignmentFailedDialog(this,
            StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
            _project,
            alignmentException);
    SwingUtils.centerOnComponent(alignmentFailedDialog, this);
    alignmentFailedDialog.setVisible(true);
  }
  
  /**
   * @author sham
   */
  private void handleVirtualLiveXMLException(final DatastoreException exception)
  {
    Assert.expect(exception != null);
  
    MessageDialog.showErrorDialog(this, exception, StringLocalizer.keyToString("GUI_VIRTUAL_LIVE_UI_MENU_KEY"), true);
  }

  /**
   * @author Cheah Lee Herng
   */
  private void handleError(JFrame frame, final XrayTesterException exception)
  {
    MessageDialog.showErrorDialog(frame, exception, StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"), true);
  }

  /**
   * @author Cheah Lee Herng
   */
  private void createVirtualLiveImageGenerationProgressDialog()
  {
    _virtualLiveImageGenerationProgressDialog = new ProgressDialog(MainMenuGui.getInstance(),
            StringLocalizer.keyToString("GUI_VIRTUAL_LIVE_UI_MENU_KEY"),
            StringLocalizer.keyToString("VIRTUAL_LIVE_GENERATE_IMAGES_WAITING_KEY"),
            StringLocalizer.keyToString("VIRTUAL_LIVE_GENERATE_IMAGES_COMPLETE_KEY"),
            StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
            StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
            0,
            100,
            true);
    _virtualLiveImageGenerationProgressDialog.pack();
    _virtualLiveImageGenerationProgressDialog.addCancelActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent ae)
      {
        hardwareAbort();
      }
    });
    _currentVirtualLiveImages = 0;
    SwingUtils.centerOnComponent(_virtualLiveImageGenerationProgressDialog, this);
  }
  
  /*
   * @author Kee Chin Seong
   * - Pulled out from main function because of HardwareWorkerThread
   */
  private TestProgram createVirtualLiveTestProgram(int x, int y, int w, int h, String componentName, int enlargeUncertaintyInNanoMeter, int zSizeInNanoMeter, int integrationLevel, int bypassAlignment, String magnificationKey, int minZHeightInNanoMeters, int maxZHeightInNanoMeters, int userGain, StageSpeedEnum stageSpeed)
  {
    //clear previous displayed image
    _virtualLiveEnvironmentPanel.clearVirtualLiveImagePanel();
    //generate virtualLive test program
    TestProgram virtualLiveTestProgram = null;
    
    if(x == 0 && y == 0 && w ==0 && h == 0)
    {
       MessageDialog.showErrorDialog(
                MainMenuGui.this,
                StringLocalizer.keyToString("VL_OUT_OF_BOUND_KEY"),
                StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                true);
       return null;
    }
    
    if (bypassAlignment == 1)
    {
      VirtualLiveManager.getInstance().setVirtualLiveWithAlignment(false);
    }
    else
    {
      VirtualLiveManager.getInstance().setVirtualLiveWithAlignment(true);
    }
    if (VirtualLiveManager.getInstance().isVirtualLiveWithAlignment() && Project.isUsingVirtualDummyProject() == false) // bee-hoon.goh must not be dummy board when using virtual live with alignment
    {
      try
      {
        virtualLiveTestProgram = VirtualLiveManager.getInstance().createTestProgram(Project.getCurrentlyLoadedProject(), x, y, w, h, enlargeUncertaintyInNanoMeter, zSizeInNanoMeter, integrationLevel, magnificationKey, minZHeightInNanoMeters, maxZHeightInNanoMeters, userGain, stageSpeed);
      }
      catch (NoAlignmentBusinessException ex)
      {
        handleXrayTesterException(ex);
        return null;
      }
    }
    else
    {
      virtualLiveTestProgram = VirtualLiveManager.getInstance().createTestProgramWithoutAlignment(Project.getCurrentlyLoadedProject(), x, y, w, h, enlargeUncertaintyInNanoMeter, zSizeInNanoMeter, integrationLevel, magnificationKey, minZHeightInNanoMeters, maxZHeightInNanoMeters, userGain, stageSpeed);
    }

    // Lee Herng 9 Jan 2013 - We don't want destroy TestProgram here because it will
    // re-generate TestProgram when calling acquireVirtualLiveImages() function
    //Project.getCurrentlyLoadedProject().destroyTestProgram();
    
    if (Project.getCurrentlyLoadedProject().hasBeenModified() && Project.isUsingVirtualDummyProject() == false)
    {
      LocalizedString saveQuestion = new LocalizedString("MM_GUI_SAVE_CHANGES_FOR_IMAGE_GENERATION_KEY",
              new Object[]
              {
                Project.getCurrentlyLoadedProject().getName()
              });
      int answer = JOptionPane.showConfirmDialog(this,
              StringUtil.format(StringLocalizer.keyToString(saveQuestion), _CHARS_PER_LINE),
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
              JOptionPane.YES_NO_OPTION,
              JOptionPane.WARNING_MESSAGE);
      if (answer == JOptionPane.YES_OPTION)
      {
        getMainMenuBar().saveProject();
      }
    }

    //write virtualLive ImageSet Data into XML
    long currentTime = System.currentTimeMillis();
    VirtualLiveImageGeneration.getInstance().setImageSetName(Directory.getDirName(currentTime));
    VirtualLiveReconstructedImagesData.getInstance().setImageSetDir(Directory.getVirtualLiveImageSetPath(Project.getCurrentlyLoadedProject().getName(),Directory.getDirName(currentTime)));
    String imageSetDataXMLFilePath = FileName.getVirtualLiveImageSetDataXMLFilePath(Project.getCurrentlyLoadedProject().getName());
    _virtualLiveImageSetDataXMLWriter = VirtualLiveImageSetDataXMLWriter.getInstance();
    if(VirtualLiveImageGenerationSettings.getInstance().isNeedSaveStitchImage())
    {
      _virtualLiveImageSetDataXMLWriter.createVirtualLiveImageSetDataXMLWriter(imageSetDataXMLFilePath);
    }
    _virtualLiveImageSetDataXMLWriter.addImageSetData(VirtualLiveImageSetDataXMLWriter.X_CENTER, String.valueOf((x + w / 2)));
    _virtualLiveImageSetDataXMLWriter.addImageSetData(VirtualLiveImageSetDataXMLWriter.Y_CENTER, String.valueOf((y + h / 2)));
    _virtualLiveImageSetDataXMLWriter.addImageSetData(VirtualLiveImageSetDataXMLWriter.WIDTH, String.valueOf(w));
    _virtualLiveImageSetDataXMLWriter.addImageSetData(VirtualLiveImageSetDataXMLWriter.HEIGHT, String.valueOf(h));
    if (componentName != "" && componentName != null)
    {
      _virtualLiveImageSetDataXMLWriter.addImageSetData(VirtualLiveImageSetDataXMLWriter.COMPONENT_NAME, String.valueOf(componentName));
    }
    _virtualLiveImageSetDataXMLWriter.addImageSetData(VirtualLiveImageSetDataXMLWriter.ENLARGE_UNCERTAINTY, String.valueOf(enlargeUncertaintyInNanoMeter));
    _virtualLiveImageSetDataXMLWriter.addImageSetData(VirtualLiveImageSetDataXMLWriter.Z_SIZE, String.valueOf(zSizeInNanoMeter));
    _virtualLiveImageSetDataXMLWriter.addImageSetData(VirtualLiveImageSetDataXMLWriter.INTEGRATION_LEVEL, String.valueOf(integrationLevel));
    _virtualLiveImageSetDataXMLWriter.addImageSetData(VirtualLiveImageSetDataXMLWriter.BYPASS_ALIGNMENT, String.valueOf(bypassAlignment));
    _virtualLiveImageSetDataXMLWriter.addImageSetData(VirtualLiveImageSetDataXMLWriter.MAGNIFICATION_KEY, String.valueOf(magnificationKey));
    _virtualLiveImageSetDataXMLWriter.addImageSetData(VirtualLiveImageSetDataXMLWriter.MIN_Z_HEIGHT_KEY, String.valueOf(minZHeightInNanoMeters));
    _virtualLiveImageSetDataXMLWriter.addImageSetData(VirtualLiveImageSetDataXMLWriter.MAX_Z_HEIGHT_KEY, String.valueOf(maxZHeightInNanoMeters));
    _virtualLiveImageSetDataXMLWriter.addImageSetData(VirtualLiveImageSetDataXMLWriter.GAIN_KEY, String.valueOf(userGain));
    _virtualLiveImageSetDataXMLWriter.addImageSetData(VirtualLiveImageSetDataXMLWriter.STAGE_SPEED_KEY, String.valueOf(stageSpeed.toDouble()));
    _xrayTesterException = null;
    
    
    //set Xray tube voltage and current once finished startup performance
    //setXrayTubeVoltageAndCurrent(xrayTubeVoltage, xrayTubeCurrent);
    
    acquireVirtualLiveImagesInProgress.setValue(false);
    _virtualLiveImageGenerationObservable.addObserver(this);
    _imageGenerationObservable.addObserver(this);    
   
   return virtualLiveTestProgram;
  }
  
  public void checkMachineStartupStatus()
  {
    // first check to make sure we have done a startup on the hardware
    try
    {
      performStartupIfNeeded();
      if (XrayTester.getInstance().isStartupRequired())
      {
        return;
      }
    }
    catch (final XrayTesterException xte)
    {
      handleXrayTesterException(xte);
      return;
    }
  }

  /**
   * created by :
   *  @author sham khang shian
   * edited by :
   *  @author chin-seong, kee
   */
  public void generateVirtualLiveImage(int x, int y, int w, int h, String componentName, int enlargeUncertaintyInNanoMeter, int zSizeInNanoMeter, int integrationLevel, int bypassAlignment, String magnificationKey, int minZHeightInNanoMeters, int maxZHeightInNanoMeters, int userGain, StageSpeedEnum stageSpeed)
  {
    Assert.expect(x >= 0);
    Assert.expect(y >= 0);
    Assert.expect(w >= 0);
    Assert.expect(h >= 0);
    Assert.expect(enlargeUncertaintyInNanoMeter >= 0);
    Assert.expect(zSizeInNanoMeter > 0);
    Assert.expect(integrationLevel > 0 && integrationLevel <= 8);
    Assert.expect(magnificationKey != null);
    
    final int xLoc = x;
    final int yLoc = y;
    final int width = w;
    final int height = h;
    final String compName = componentName;
    final int enlargeUncertainty = enlargeUncertaintyInNanoMeter;
    final int zSize = zSizeInNanoMeter;
    final int intergrationLvl = integrationLevel;
    final int isBypassAlignment = bypassAlignment;
    final String magnification = magnificationKey;
    final int minZHeight = minZHeightInNanoMeters;
    final int maxZHeight = maxZHeightInNanoMeters;
    final int cameraGain = userGain;
    final StageSpeedEnum stageMoveSpeed = stageSpeed;
    
    try
    {
      //Swee-Yee.Wong - XCR-3157
  	  //IRP error message is prompted if run alignment/fine tuning without run full cdna and without unload the board 1st
      if(XrayTester.isS2EXEnabled())
      {
        if(_systemStatusPersistance.isSystemMagnificationReconfigured())
          performStartupIfNeeded();
      }
    }
    catch (XrayTesterException ex)
    {
      handleXrayTesterException(ex);
      return;
    }
    
    _xrayTesterException = null;
    // Set current mode as Virtual Live mode
    VirtualLiveManager.getInstance().setVirtualLiveMode(true);
    
    // Create Virtual Live TestProgram
    final TestProgram testProgram = createVirtualLiveTestProgram(xLoc, yLoc, width, height, compName, enlargeUncertainty, zSize, 
                                                                 intergrationLvl, isBypassAlignment, magnification, minZHeight,
                                                                 maxZHeight, cameraGain, stageMoveSpeed);

    if(testProgram == null)
      return;
    
    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        { 
          //Swee Yee Wong - XCR-2837 Assert When Viewing Result in Fine Tuning/ Virtual live
          VirtualLivePanel.getInstance().enableImageGenerationButton(false);
          
         //Kee Chin Seong ::  Sometimes when machine has occured some exception
          //                   there might have chance go in here, so the test Program is NULL b
          //                   because we've clear the test Program and so no IRE used will cause creash
          //                   hardwareworker thread will be straight running.
          if(testProgram == null)
          {
              createVirtualLiveTestProgram(xLoc, yLoc, width, height, compName, enlargeUncertainty, zSize, 
                                           intergrationLvl, isBypassAlignment, magnification, minZHeight,
                                           maxZHeight, cameraGain, stageMoveSpeed);
          }
          // HERE IS WHERE THE IMPORTANT WORK IS DONE
          TestProgram virtualLiveTestProgram=testProgram;
          TestExecution.getInstance().acquireVirtualLiveImages(virtualLiveTestProgram);
          acquireVirtualLiveImagesInProgress.setValue(true);
        }
        catch (final XrayTesterException ex)
        {
          _xrayTesterException = ex;
          acquireVirtualLiveImagesInProgress.setValue(true);

          //Ngie Xing, XCR-2382, System hung when run alignment
          //resetVirtualLiveMode();

          //XCR-2211, this progress bar might have chance to be disposed even before display and will cause program to infinite waiting visible(true).
          //so we will wait until visible true
          closeProgressDialog(_autoAdjustProgressDialog, false);
          _autoAdjustProgressDialog = null;
          closeProgressDialog(_virtualLiveImageGenerationProgressDialog, false);
          _virtualLiveImageGenerationProgressDialog = null;
          
          if (testProgram != null)
          {
            testProgram.clear(VirtualLiveManager.getInstance().isVirtualLiveWithAlignment());
          }
        }
        finally
        {
          if(_xrayTesterException == null)
          {
            try
            {
              acquireVirtualLiveImagesInProgress.waitUntilTrue();
            }
            catch (InterruptedException iex)
            {
              // Do nothing ...
            }
          }
          else
          {
            if (_xrayTesterException instanceof PanelPositionerException)
            {
              handleXrayTesterException(_xrayTesterException);
            }
            //error while generating virtual image andd board inside
            else if(_xrayTesterException instanceof PanelInterferesWithAdjustmentAndConfirmationTaskException)
              handleXrayTesterException(_xrayTesterException);
            else
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  MessageDialog.showErrorDialog(_instance, _xrayTesterException, StringLocalizer.keyToString("GUI_VIRTUAL_LIVE_UI_MENU_KEY"), true);
                }
              });
            }
          }
          //XCR-3797, Failed to cancel CDNA task after start up
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              closeAllDialogs();
            }
          });
          _virtualLiveImageGenerationObservable.deleteObserver(getInstance());
          if(_xrayTesterException == null)
          {
            if(VirtualLiveImageGenerationSettings.getInstance().isNeedSaveStitchImage())
            {
              try
              {
                _virtualLiveImageSetDataXMLWriter.writeToXMLFile();
              }
              catch (DatastoreException ex)
              {
                handleVirtualLiveXMLException(ex);
              }
            }

            _virtualLiveImageSetDataXMLWriter.deleteVirtualLiveImageSetDataXMLWriter();
            VirtualLiveImageProducer.getInstance().getVirtualLiveMemoryManagedImageMap().clear();  
            
            if (testProgram != null)
            {
              testProgram.clear(VirtualLiveManager.getInstance().isVirtualLiveWithAlignment());
            }

            //XCR-3810, Software hung when generate Virtual Live images for multiple components
            if(VirtualLivePanel.getInstance().getComponentList().isEmpty() == false &&
               VirtualLiveImageGenerationSettings.getInstance().isMultipleSelectComponentOn() == true)
               VirtualLivePanel.getInstance().generateVirtualLiveImage();        
            else
            {
              //XCR-3858, Virtual Live Images Set Settings Incomplete message is prompted when generate virtual live images for multiple components
              //here will notify observer to draw virtual live image and display zSlider
              VirtualLiveReconstructedImagesData.getInstance().setOnlineReconstructedImagesData(2, 1);
              // Swee Yee Wong XCR-3354 - Insufficient trigger error when run alignment at Alignment setup tab after alignment failed at VL
              resetVirtualLiveMode();
              //Swee Yee Wong - XCR-2837 Assert When Viewing Result in Fine Tuning/ Virtual live
              VirtualLivePanel.getInstance().enableImageGenerationButton(true);
            }
          }
          else
          {
            // Swee Yee Wong XCR-3354 - Insufficient trigger error when run alignment at Alignment setup tab after alignment failed at VL
            resetVirtualLiveMode();
            //Swee Yee Wong - XCR-2837 Assert When Viewing Result in Fine Tuning/ Virtual live
            VirtualLivePanel.getInstance().enableImageGenerationButton(true);
          }
        }
      }
    });
  }
  
  /*
   * @author Kee chin Seong - Reset the Virtual Live Mode
   * @author Kee Chin Seong - In order let IRP get the new TestProgram when in production Mode
   *                          we need to invalidate the checksum for safety purpose.
   */
  public void resetVirtualLiveMode()
  {
      VirtualLiveManager.getInstance().setVirtualLiveMode(false);
      _imageGenerationObservable.deleteObserver(this);
      //Swee Yee Wong - XCR-3297 Xray Camera Insufficient Trigger when generate a 3x3 Virtual Live Image
      //If recipe is closed, just ignore it.
      if(Project.isUsingVirtualDummyProject() == false && _project != null)
      {
        _project.resetTestProgram();
      }
  }

  /*
   * @author sham khang shian
   */
  public boolean isVirtualLiveCurrentEnvironment()
  {
    return _currentEnvironment == _virtualLiveEnvironmentPanel;
  }
  
  /*
   * @author KEe Chin Seong
   */
  public boolean isCadCreatorCurrentEnvironment()
  {
    return _currentEnvironment == _cadCreatorEnvironmentPanel;
  }

  /**
   * @author Jack Hwee
   */
  public boolean beforeGenerateSurfaceMapping(java.awt.Component parent) throws XrayTesterException
  {
    Assert.expect(parent != null);

    final TestProgram testProgram = getTestProgramWithReason(StringLocalizer.keyToString("MMGUI_ALIGNMENT_NEEDS_TEST_PROGRAM_KEY"),
                                                             StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                             _project);
    boolean hasHighMagComponent = _testExecution.containHighMagInspectionInCurrentLoadedProject();

    if (_currentEnvironment == _serviceEnvironment || _currentEnvironment == _testExecEnvironment)
    {
      _hardwareObservable.addObserver(this);
      _progressObservable.addObserver(this);
      _fringePatternObservable.addObserver(this);
    }
    try
    {
      startXrayTester();
      //XCR-3797, Failed to cancel CDNA task after start up
      if (TestExecution.getInstance().didUserAbort())
        return false;
    }
    catch (XrayTesterException xte)
    {
      if (xte instanceof HardwareTaskExecutionException)
      {
        if (_testExecution.isPanelLoaded(hasHighMagComponent, testProgram.getProject().getName()))
        {
          throw new PanelInterferesWithAdjustmentAndConfirmationTaskException("");
        }
      }
      else
        throw xte;
      return false;
    }
    finally
    {
      if (_currentEnvironment == _serviceEnvironment || _currentEnvironment == _testExecEnvironment)
      {
        _hardwareObservable.deleteObserver(this);
        _progressObservable.deleteObserver(this);
        _fringePatternObservable.deleteObserver(this);
      }
    }
    
    if (_systemStartupWasCanceled == false)
    {
      if (_testExecution.isPanelLoaded(hasHighMagComponent, testProgram.getProject().getName()) == false)
      {
        loadPanel();
        _testExecution.resetLastPerformedAlignmentTestSubProgramId();
      }
      else
      {
        _testExecution.checkThatCurrentlyLoadedPanelProjectNameMatchesProjectName(_project.getTestProgram());
        _testExecution.checkThatCurrentlyLoadedPanelMatchesProjectPanelSize(_project.getTestProgram());
        _testExecution.checkThatManualAlignmentHasBeenPerformed(testProgram);
      }
      
      if (_testExecution.isPanelLoaded(hasHighMagComponent, testProgram.getProject().getName()))
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      return false;
    }
  }

  /**
   * @author Wei Chin
   */
  static public void startLicenseMonitor()
  {
    if(_licenseMonitorStart == null)
      _licenseMonitorStart = new BooleanLock(true);
    
    LicenseWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      { 
        while(_licenseMonitorStart.isTrue())
        {
          try
          {
            LicenseManager.checkThatValidLicenseExists();
            // one hour check 
            Thread.sleep(LICENSE_MONITOR_TIMEOUT_IN_MILISECONDS);
          }
          catch(InterruptedException ie)
          {
            // do nothing
          }
          catch (final BusinessException ex)
          {
            finishLicenseMonitor();
            
            //XCR3648: Software crash when eject XXL dongle license while loading V810
            LicenseWorkerThread.getInstance().setLicenseException(ex);
          }
        }
      }      
    });
  }
  
  /**
   * @author Wei Chin
   */
  static public void finishLicenseMonitor()
  {
    _licenseMonitorStart.setValue(false);
    LicenseManager.releaseLicense();
    LicenseWorkerThread.getInstance().stopLicenseWorkerThread();
  }
  
  /**
   * @author Wei Chin
   */
  public void exitV810(boolean isRestartV810)
  {
    //Ngie Xing
    if (XrayTester.isS2EXEnabled())
    {
      //Swee-Yee.Wong - XCR-3157
      //IRP error message is prompted if run alignment/fine tuning without run full cdna and without unload the board 1st
      try
      {
        //Swee Yee Wong
        if (_systemStatusPersistance != null)
          _systemStatusPersistance.savePersistentSystemInformation();
      }
      catch (DatastoreException de)
      {
        //do nothing
      }

      Config.getInstance().deleteSystemTypeConfigBinaryFileIfExist();
    }
   
    setSchedulerState(false);
    // check test dev - it's special
    if (_testDevEnvironment != null)
    {
      if (_testDevEnvironment.okToCloseProject() == false)
      {
        return;
      }
    }
    // other environments do this at isReadyToFinish()
    if (_currentEnvironment != null)
    {
      if (_currentEnvironment.isReadyToFinish())
      {
        _currentEnvironment.finish();
        _currentEnvironment = null;
      }
      else
      {
        return;
      }
    }
    writeGuiPersistanceFile();

    // try to stop RMS before exit
    try
    {
      if( RemoteMotionServerExecution.getInstance().isSimulationModeOn() )
        RemoteMotionServerExecution.getInstance().stopRMS();

      if (_enablePsp)
      {
        PspHardwareEngine.getInstance().shutdown();
      }
      
      if (_terminateIrpsWhenExit)
      {
        try 
        {
          Runtime.getRuntime().exec("taskkill /F /IM IRP.exe");
        } 
        catch (IOException ex) 
        {
          // do nothing
        }   
      }

      //Khaw Chek Hau - XCR2654: CAMX Integration
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
      {
        try
        {
          _shopFloorSystem.terminateCAMxProgram();
        }
        catch (IOException ex)
        {
          //do nothing
        }
      }
    }
    catch(XrayTesterException xe)
    {
      // do nothing
    }        

    //XCR3514: Software crashed when eject dongle licence during loading V810 application
    if (_assertObservable != null)
    {
      _assertObservable.deleteObserver(this);
    }
    
    finishLicenseMonitor();
    
    if (isRestartV810)
    {
      final ArrayList<String> command = new ArrayList<String>();
      command.add(_PERL_COMMAND);
      command.add(FileName.getRunXrayTesterFullPath());

      final ProcessBuilder builder = new ProcessBuilder(command);
      try
      {
        builder.start();
      }
      catch (IOException ex)
      {
        String errorMsg = ex.getLocalizedMessage();
        MessageDialog.showErrorDialog(MainMenuGui.this,
                                      errorMsg,
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }
    System.exit(0);
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR2183: Standardize all print out when developer debug is true
   */
  static public void setDebugIfNecessary()
  {
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.MEMORY_UTIL_DEBUG))
      MemoryUtil.setDebug(true);
    
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.THIN_PLATE_SPLINE_DEBUG))
      ThinPlateSpline.setDebug(true);   
  }

  /*
   * chin-seong, kee - virtual live set Xray Source Voltage and Current
   */
  public void setXrayTubeVoltageAndCurrent(double voltage, double current)
  {
    Assert.expect(_hTubeXraySource != null);
    
    try
    {
      _hTubeXraySource.setXraySourceParametersForServiceMode(voltage, current);
    }
    catch (XrayTesterException ex)
    {
                //do nothing
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setIsMagnificationChanged()
  {
    _isMagnificationChanged = true;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean isMagnificationChanged()
  {
    return _isMagnificationChanged;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @param event 
   * 
   * this function is to notify user project system type is different with software supported system type
   * ask user permission to convert project to other system type
   */
  private void notifyUserSystemTypeUnsupportedOfflineProgrammingSystemType(ProjectChangeEvent event)
  {
    Pair<String, String> projectNameToSystemTypePair = (Pair<String, String>)event.getSource();
    java.util.List<String> supportedSystemType = null;
    try
    {
      supportedSystemType = LicenseManager.getInstance().getSupportedSystemType();
    }
    catch (final BusinessException e)
    {
      finishLicenseMonitor();
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
            e.getMessage(),
            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
            true);
          MainMenuGui.getInstance().exitV810(false);
        }
      });
    }
    String supportedSystemTypeName = StringUtil.join(supportedSystemType, ",");
    String projectSystemTypeName = projectNameToSystemTypePair.getSecond();
    String [] argument = {projectSystemTypeName,supportedSystemTypeName};
    LocalizedString warningMessage = new LocalizedString("MMGUI_SYSTEM_TYPES_DIFFER_OLP_KEY", argument);//,
    int answer = JOptionPane.showConfirmDialog(this,
              StringUtil.format(StringLocalizer.keyToString(warningMessage), _CHARS_PER_LINE),
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
              JOptionPane.YES_NO_OPTION,
              JOptionPane.WARNING_MESSAGE);
    
    if (answer == JOptionPane.YES_OPTION)
    {
        _createProjectDialog = new CreateProjectOptionDialog(this, true,
                projectNameToSystemTypePair.getFirst(),
                projectNameToSystemTypePair.getSecond(),
                supportedSystemType);
        _createProjectDialog.pack();
        SwingUtils.centerOnComponent(_createProjectDialog, this);
        _createProjectDialog.setVisible(true);
        boolean userCancel = _createProjectDialog.userCancel();
        
        if (userCancel)
        {
          Project.userResponseToSystemTypesDiffer(false, false);
        }
        else
        {
          try
          {
            final String projectName = _createProjectDialog.getNewProjectName();
            SystemTypeEnum systemType = _createProjectDialog.getNewSystemType();
            String lowMag = _createProjectDialog.getLowMag();
            
            Config.getInstance().forceLoad(systemType);
            ProgramGeneration.reintializeDueToSystemTypeChange();
            com.axi.v810.datastore.projReaders.ProjectReader.reintializeDueToSystemTypeChange();
            ProjectWriter.reintializeDueToSystemTypeChange();
            Project.setupProjectReaderWriter();
            if (systemType.equals(SystemTypeEnum.S2EX))
            {
              Config.getInstance().setValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION, lowMag);
              Config.getInstance().save(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION.getFileName());
            }
            
            if (systemType.equals(SystemTypeEnum.S2EX))
            {
              this.getMainMenuBar().enableChangeMagnificationMenu();
              this.getEnvToolBar().enableChangeSystemMagnificationButton();
              this.getMainToolBar().enableChangeSystemMagnificationButton();
            }
            else
            {
              this.getMainMenuBar().disableChangeMagnificationMenu();
              this.getEnvToolBar().disableChangeSystemMagnificationButton();
              this.getMainToolBar().disableChangeSystemMagnificationButton();
            }
            MagnificationEnum.resetMagnification();
            this.getTestDev().refresh();  
            Project.userResponseToDifferentSystemTypeOLP(false, true, projectName,true);
          }
          catch (final DatastoreException e)
          {
            handleOfflineProgrammingDataStoreError(e);            
            Project.userResponseToSystemTypesDiffer(false, false);
          }
        }
      }
      else
      {
        Project.userResponseToSystemTypesDiffer(false, false);
      }
  }
  
  public void handleOfflineProgrammingDataStoreError(DatastoreException e)
  {
    MessageDialog.showErrorDialog(_instance,
      e.getLocalizedMessage(),
      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
      true);

    try
    {
      Config.getInstance().forceLoad();
    }
    catch (DatastoreException ex)
    {

    }
    if (XrayTester.getSystemType().equals(SystemTypeEnum.S2EX))
    {
      this.getMainMenuBar().enableChangeMagnificationMenu();
      this.getMainToolBar().enableChangeSystemMagnificationButton();
      _envToolBar.enableChangeSystemMagnificationButton();
    }
    else
    {
      this.getMainToolBar().disableChangeSystemMagnificationButton();
      this.getMainMenuBar().disableChangeMagnificationMenu();
      _envToolBar.disableChangeSystemMagnificationButton();
    }
    MagnificationEnum.resetMagnification();
    this.getTestDev().refresh();
  }
  
  
  private static void changeSystemTypeDefaultConfigIfNecessary() throws BusinessException
  {
    if (LicenseManager.isXXLEnabled() && FileUtilAxi.exists(Directory.getConfigDirWithSystemType(SystemTypeEnum.XXL)))
    {
      XrayTester.changeSystemType(SystemTypeEnum.XXL);
    }
    else if (LicenseManager.isS2EXEnabled() && FileUtilAxi.exists(Directory.getConfigDirWithSystemType(SystemTypeEnum.S2EX)))
    {
      XrayTester.changeSystemType(SystemTypeEnum.S2EX);
    }
    else if (LicenseManager.isThroughputEnabled() && FileUtilAxi.exists(Directory.getConfigDirWithSystemType(SystemTypeEnum.THROUGHPUT)))
    {
      XrayTester.changeSystemType(SystemTypeEnum.THROUGHPUT);
    }
    else
    {
      if (LicenseManager.isXXLEnabled())
      {
        XrayTester.changeSystemType(SystemTypeEnum.XXL);
      }
      else if (LicenseManager.isS2EXEnabled())
      {
        XrayTester.changeSystemType(SystemTypeEnum.S2EX);
      }
      else if (LicenseManager.isThroughputEnabled())
      {
        XrayTester.changeSystemType(SystemTypeEnum.THROUGHPUT);
      }
    }
  }
}
