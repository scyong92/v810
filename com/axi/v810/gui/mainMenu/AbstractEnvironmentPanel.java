package com.axi.v810.gui.mainMenu;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.util.*;

/**
 *
 * <p>Title: AbstractEnvironmentPanel</p>
 *
 * <p>Description: This class provides the framework
 * for Genesis environment panels. The Main UI uses this class
 * to manage navigation between environments. </p>
 *
 * <p>The instantiating panel needs to implement the following to satisfy
 * the Environment interface:<BR>
 * public void navButtonClicked(String buttonName)<BR>
 * public void start()<BR>
 * public boolean isReadyToFinish()<BR>
 * public void finish()<BR></p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 * @version 1.0
 */

public abstract class AbstractEnvironmentPanel extends JPanel implements EnvironmentInt
{
  protected MainMenuGui _mainUI = null;
  protected JPanel _environmentPanel = null;
  protected JPanel _centerPanel = null;
  protected CardLayout _mainCardLayout = null;
  protected AbstractTaskPanel _currentTaskPanel = null;
  protected AbstractNavigationPanel _navigationPanel = null;

  protected Border _environmentBorder;
  protected BorderLayout _environmentLayout = null;

  protected MainUIMenuBar _envMenuBar = null;
  protected int _menuRequesterID = -1;

  protected MainUIToolBar _envToolBar = null;
  protected int _envToolBarRequesterID = -1;

  protected String[] _buttonNames = null;
  private boolean _dataDirty = false;


  /**
   * @author George Booth
   */
  public AbstractEnvironmentPanel(MainMenuGui mainUI)
  {
    Assert.expect(mainUI != null);
    _mainUI = mainUI;
    jbInitialize();
  }

  /**
   * @author Andy Mechtenberg
   */
  public abstract com.axi.v810.gui.undo.CommandManager getCommandManager();

  /**
   * @author George Booth
   */
  public MainMenuGui getMainUI()
  {
    return _mainUI;
  }

  /**
   * @author George Booth
   */
  public MainUIMenuBar getEnvMenuBar()
  {
    return _envMenuBar;
  }

  /**
   * @author George Booth
   */
  public MainUIToolBar getEnvToolBar()
  {
    return _envToolBar;
  }

  /**
   * @author George Booth
   */
  protected void dataDirty(boolean dirty)
  {
    _dataDirty = dirty;
  }

  /**
   * @author George Booth
   */
  protected boolean dataDirty()
  {
    return _dataDirty;
  }

  /**
   * Initialize panel (renamed so it is not overridden)
   * @author George Booth
   */
  private void jbInitialize()
  {
    this.setLayout(new BorderLayout());

    _environmentLayout = new BorderLayout();
    _environmentPanel = new JPanel();
    _environmentPanel.setLayout(_environmentLayout);
    _environmentBorder = BorderFactory.createEmptyBorder(5, 5, 0, 5);
    _environmentPanel.setBorder(_environmentBorder);
    this.add(_environmentPanel, BorderLayout.CENTER);

    _centerPanel = new JPanel();

    _mainCardLayout = new CardLayout(0, 0);
    _centerPanel.setLayout(_mainCardLayout);

    _environmentPanel.add(_centerPanel, BorderLayout.CENTER);
  }

  /**
   * Adds the navigation panel to the left of the panel
   * @author George Booth
   */
  protected void setNavigationPanel(AbstractNavigationPanel navigationPanel)
  {
    Assert.expect(navigationPanel != null);
    _environmentPanel.add(navigationPanel, BorderLayout.NORTH);
  }

  /**
   * Adds the environment tool bar to the top of the panel
   * @author George Booth
   */
  protected void addToolBar()
  {
    Assert.expect(_envToolBar != null);
    _mainUI.add(_envToolBar, BorderLayout.NORTH);
//    this.add(_envToolBar, BorderLayout.NORTH);
  }

  /**
   * Adds the environment status bar to the bottom of the panel
   * @author George Booth
   */
  protected void addStatusBar(JPanel statusBar)
  {
    Assert.expect(statusBar != null);
    this.add(statusBar, BorderLayout.SOUTH);
  }


  /**
   * Called from the enviroment panel to switch to a new task
   * @author George Booth
   */
  protected void switchToTask(AbstractTaskPanel taskPanel, String taskName)
  {
    Assert.expect(taskPanel != null);
    Assert.expect(taskName != null);

    if (_currentTaskPanel != null)
    {
      if (_currentTaskPanel.isReadyToFinish())
      {
        _currentTaskPanel.finish();
      }
      else
      {
        return;
      }
    }
    _mainCardLayout.show(_centerPanel, taskName);
    _currentTaskPanel = taskPanel;
    _currentTaskPanel.start();
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void switchToTask(JPanel cardLayoutPanel, AbstractTaskPanel taskPanel, String taskName)
  {
    Assert.expect(cardLayoutPanel != null);
    Assert.expect(taskPanel != null);
    Assert.expect(taskName != null);
    if (_currentTaskPanel != null)
    {
      if (_currentTaskPanel.isReadyToFinish())
      {
        _currentTaskPanel.finish();
      }
      else
      {
        return;
      }
    }
    CardLayout cardLayout = (CardLayout)(cardLayoutPanel.getLayout());
    cardLayout.show(cardLayoutPanel, taskName);
    _currentTaskPanel = taskPanel;
    _currentTaskPanel.start();
  }

  /**
   * Called from the enviroment panel to initialize the environment
   * @author George Booth
   */
  public void startMenusAndToolBar()
  {
    _envMenuBar = _mainUI.getMainMenuBar();
    _menuRequesterID = _envMenuBar.startMenuAdd();

    _envToolBar = _mainUI.getEnvToolBar();
    _envToolBarRequesterID = _envToolBar.startToolBarAdd();
  }

  /**
   * Called from the enviroment panel to initialize the environment
   * with an empty toolbar
   * @author George Booth
   */
  public void startMenusAndEmptyToolBar()
  {
    _envMenuBar = _mainUI.getMainMenuBar();
    _menuRequesterID = _envMenuBar.startMenuAdd();

    _envToolBar = _mainUI.getEmptyToolBar();
    _envToolBarRequesterID = _envToolBar.startToolBarAdd();
  }

  /**
   * Called from the enviroment panel to initialize the environment
   * with a toolbar with disable the previous added buttons...
   * @author Chong, Wei Chin
   */
  public void startMenusAndDisableToolBar()
  {
    _envMenuBar = _mainUI.getMainMenuBar();
    _menuRequesterID = _envMenuBar.startMenuAdd();

    _envToolBar = _mainUI.getEnvToolBar();

    _envToolBar.enableCommonButtons(false);
    _envToolBarRequesterID = _envToolBar.startToolBarAdd();
  }

  /**
   * Called from the enviroment panel to determine is the current task
   * is ready to terminate
   * @return true if task is ready to finish
   * @author George Booth
   */
  protected boolean currentTaskPanelReadyToFinish()
  {
    if (_currentTaskPanel == null)
    {
      return true;
    }
    else
    {
      return _currentTaskPanel.isReadyToFinish();
    }
  }

  /**
   * Called from the enviroment panel to finish the current task when
   * terminating the environment
   * @return true if task is ready to finish
   * @author George Booth
   */
  protected boolean finishCurrentTaskPanel()
  {
    if (_currentTaskPanel != null)
    {
      if (_currentTaskPanel.isReadyToFinish())
      {
        _currentTaskPanel.finish();
        _currentTaskPanel = null;
        return true;
      }
      else
      {
        // should not happen
        Assert.expect(false);
      }
    }
    // no current task
    return true;
  }

  /**
   * Called from the enviroment panel to clean up the environment
   * @author George Booth
   */
  public void finishMenusAndToolBar()
  {
    // remove custom menus and toolbar components
    _envMenuBar.undoMenuChanges(_menuRequesterID);
    _envToolBar.undoToolBarChanges(_envToolBarRequesterID);
    _envToolBar.enableCommonButtons(true);
  }

}
