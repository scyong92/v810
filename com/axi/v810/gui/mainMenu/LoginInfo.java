package com.axi.v810.gui.mainMenu;

/**
 *
 * <p>Title: LoginInfo</p>
 *
 * <p>Description: Data class for the AXI LoginDialog</p>
 *
 * @author George Booth
 * @version 1.0
 */


public class LoginInfo
{
  public String userName = null;
  public String password = null;
  public LoginInfo(String u, String p)
  {
    userName = u;
    password = p;
  }
}
