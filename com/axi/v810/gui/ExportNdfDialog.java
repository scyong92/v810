/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 *
 * @author siew-yeng.phang
 */
public class ExportNdfDialog extends EscapeDialog
{
  // parent
  private JFrame _frame;
  
  private int _returnValue;
  private Object _selectedRecipe;
  
  private DefaultListModel _choicesListModel = new DefaultListModel();
  private JScrollPane _choicesScrollPane = new JScrollPane();
  private JTextField _destinationTextField = new JTextField();
  private JButton _fileDestinationButton = new JButton();
  private JList _choicesList = new JList(_choicesListModel)
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        _returnValue = JOptionPane.CANCEL_OPTION;
        _selectedRecipe = null;
        dispose();
      }
    }
  };
  
  // default destination directory..
  private String _defaultDestinationDirectoryText = Directory.getLegacyNdfDir();

  public ExportNdfDialog(JFrame frame, String title, Object[] possibleValues)
  {
    super(frame, title, true);
    
    _frame = frame;
    
    for (int i = 0; i < possibleValues.length; i++)
      _choicesListModel.addElement(possibleValues[i]);

    _choicesList.setSelectedIndex(0);
    
    createDialog();
    pack();
  }
  
  private void createDialog()
  {
    JPanel mainPanel = new JPanel(new BorderLayout(0, 5));
    JPanel centerPanel = new JPanel(new BorderLayout(0,5));
    JPanel exportMessagePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel destinationPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER,0,0));
    JPanel innerButtonPanel = new JPanel(new GridLayout(1, 2, 5, 0));
    
    JLabel exportMessageLabel = new JLabel(StringLocalizer.keyToString("MM_GUI_EXPORT_AS_NDF_MESSAGE_KEY"));
    JLabel destinationLabel = new JLabel(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_DESTINATION_LABEL_KEY"));
    
    JButton exportButton = new JButton(StringLocalizer.keyToString("GUI_EXPORT_BUTTON_KEY"));
    JButton cancelButton = new JButton(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));

    cancelButton.setFont(FontUtil.getDialogButtonFont());
    exportButton.setFont(FontUtil.getDialogButtonFont());
    cancelButton.setMargin(new Insets(2, 8, 2, 8));
    exportButton.setMargin(new Insets(2, 8, 2, 8));
    exportMessageLabel.setFont(FontUtil.getDialogMessageFont());    
    innerButtonPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 0, 10));
    mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    
    exportMessagePanel.add(exportMessageLabel);
    
    getContentPane().add(mainPanel);
    innerButtonPanel.add(exportButton);
    innerButtonPanel.add(cancelButton);
    buttonPanel.add(innerButtonPanel);
    mainPanel.add(buttonPanel,BorderLayout.SOUTH);
    mainPanel.add(centerPanel,BorderLayout.CENTER);
    destinationPanel.add(destinationLabel);
    _destinationTextField.setColumns(23);
    _destinationTextField.setEditable(true);
    _destinationTextField.setText(_defaultDestinationDirectoryText);
    _fileDestinationButton.setText("...");
    destinationPanel.add(_destinationTextField);
    destinationPanel.add(_fileDestinationButton);
    centerPanel.add(destinationPanel,BorderLayout.SOUTH);
    _choicesScrollPane.getViewport().add(_choicesList);
    centerPanel.add(_choicesScrollPane, BorderLayout.CENTER);
    _choicesList.setVisibleRowCount(10);
    _choicesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _choicesList.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      // this ensures that a selected item is visible.  At jdk1.5, this is the default behavior, so this could be removed.

      public void valueChanged(ListSelectionEvent e)
      {
        if (e.getValueIsAdjusting() == false && _choicesListModel.isEmpty() == false)
        {
          int index = _choicesList.getSelectedIndex();
          _choicesList.ensureIndexIsVisible(index);
          //_destinationTextField.setText(_choicesList.getSelectedValue().toString());

        }
      }
      
    });
    _choicesList.addMouseListener(new MouseAdapter()
    {
      public void mousePressed(MouseEvent e)
      {
        // if the user double clicks on a entry, automatically select it and exit the dialog
        if (e.getClickCount() == 2 && _choicesListModel.isEmpty() == false)
        {
          int index = _choicesList.locationToIndex(e.getPoint());
          if (index != -1)
          {
            _selectedRecipe = _choicesList.getSelectedValue();
            _returnValue = JOptionPane.OK_OPTION;
            dispose();
          }
        }
      }
    });
    
    _fileDestinationButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        findDirectoryButton_actionPerformed();
      }
    });
    getRootPane().setDefaultButton(exportButton);
    exportButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exportButton_actionPerformed();
      }
    });
    cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed();
      }
    });
  }
   
  private void exportButton_actionPerformed()
  {
    _selectedRecipe = _choicesList.getSelectedValue();
    _returnValue = JOptionPane.OK_OPTION;
    dispose();
  }
   
  private void cancelButton_actionPerformed()
  {
    _selectedRecipe = null;
    _returnValue = JOptionPane.CANCEL_OPTION;
    dispose();
  }  
  
    /**
   * @return int
   * @author Andy Mechtenberg
   */
  public int showDialog()
  {
    SwingUtils.centerOnComponent(this, _frame);
    setVisible(true);
    return _returnValue;
  }
  
  /**
   * @param e WindowEvent
   * @author Andy Mechtenberg
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    int eventId = e.getID();
    if (eventId == WindowEvent.WINDOW_CLOSING)
    {
      _returnValue = JOptionPane.CANCEL_OPTION;
      _selectedRecipe = null;
      dispose();
    }
  }
  
  /**
   * @author Chin-Seong, Kee
   */
  private void findDirectoryButton_actionPerformed()
  {
    JFileChooser fileChooser = null;
    if (fileChooser == null)
    {
      fileChooser = new JFileChooser()
      {
        public void approveSelection()
        {
          File selectedFile = getSelectedFile();
          if (selectedFile.exists())
            super.approveSelection();
          else
          {
            String message = StringLocalizer.keyToString(new LocalizedString("FILE_DIALOG_SELECTION_DOES_NOT_EXIST_KEY", new Object[]
                {selectedFile}));
            JOptionPane.showMessageDialog(this, StringUtil.format(message, 50));
          }
        }
      };
    }
    fileChooser.setApproveButtonText(StringLocalizer.keyToString("ZIP_PROEJECT_WIZARD_SELECT_BUTTON_KEY"));
    fileChooser.setCurrentDirectory(new File(_defaultDestinationDirectoryText));
    fileChooser.setDialogTitle(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_SELECT_DESTINATION_LABEL_KEY"));
    fileChooser.setDialogType(JFileChooser.CUSTOM_DIALOG);
    fileChooser.setMultiSelectionEnabled(false);
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

    int returnValue = fileChooser.showDialog(this, StringLocalizer.keyToString("ZIP_PROEJECT_WIZARD_SELECT_BUTTON_KEY"));
    if (returnValue == JFileChooser.APPROVE_OPTION)
    {
      // the user has selected a new directory.
      File selectedDirectory = fileChooser.getSelectedFile();
      Assert.expect(selectedDirectory.isDirectory());

      _defaultDestinationDirectoryText = selectedDirectory.getAbsolutePath();
      _destinationTextField.setText(_defaultDestinationDirectoryText);
    }
  }
  
  /**
   * @author Siew Yeng, Phang
   */
  public Object getSelectedRecipe()
  {
    return _selectedRecipe;
  }
  
   /**
   * @author Siew Yeng, Phang
   */
  public String getFileDestination()
  {
    return _defaultDestinationDirectoryText;
  }
}
