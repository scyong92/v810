package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
  * This class implements a splash screen with the application name at the
  * top, Agilent logo in the middle and a progress indicator at the bottom.
  * The application name is passed in through the constructor.
  *
  * @author Steve Anonson
  */
public class AxiSplashScreen extends JWindow
{
  private final static int _DELAY = 150;
  private final static int _MAX_DOTS = 50;  // maximum number of dot in the animation
  private final static int _MIN_SHOW_TIME = 1000;//2000;  // minimum time to show the screen to avoid flashing
  private String _appName = null;
  private int _dots = 0;
  private String _loadingStr = StringLocalizer.keyToString("GUI_LOADING_MESSAGE_KEY");
  private boolean _usingTimer = false;
  private Timer _timer = null;

  private Image _baseImage;
  private BufferedImage _splashImage;


  private JPanel _borderPanel = new JPanel();
  private BorderLayout _borderPanelLayout = new BorderLayout();
  private JLabel _centerLabel = new JLabel();
  private JLabel _southLabel = new JLabel();
  private JLabel _northLabel = new JLabel();
  private Border _southLabBorder = null;
  private Border _northLabBorder = null;
  private Border _borderPanelBorder = null;

  /**
    * Constructor to create a splash screen.
    *
    * @param appName - String containing the name of the application
    * @author Steve Anonson
    */
  public AxiSplashScreen( String appName )
  {
    _appName = appName;
    jbInit();
    ImageIcon icon = Image5DX.getImageIcon( Image5DX.AXI_X5000_SPLASH );
    if (icon != null)
      _centerLabel.setIcon(icon);
    else
      _centerLabel.setText(" AXI System ");

    this.setCursor( new Cursor(Cursor.WAIT_CURSOR) );
    this.pack();
    this.centerOnScreen();
    _usingTimer = true;
    // Create a Timer that will call the updateScreen() method
    _timer = new Timer(_DELAY, new ActionListener()
    {
      public void actionPerformed(ActionEvent evt)
      {
        SwingUtils.invokeLater( new Runnable()
        {
          public void run()
          {
            updateScreen();
          }
        } );
      }
    } );  // end new Timer
  }

  /**
    * Constructor with no framing around the image
    *
    * @author Steve Anonson
    */
  public AxiSplashScreen()
  {
    _baseImage = Image5DX.getImage(Image5DX.AXI_X5000_SPLASH);
    createShadowPicture();
    this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
    this.centerOnScreen();
    _usingTimer = false;
  }

  public void paint(Graphics g)
  {
    if (_appName != null)
      super.paint(g);
    else
    {
      if (_splashImage != null)
      {
        g.drawImage(_splashImage, 0, 0, null);
      }
    }
  }


  /**
   * @author Andy Mechtenberg
   */
  private void createShadowPicture()
  {
    int width = _baseImage.getWidth(null);
    int height = _baseImage.getHeight(null);

    setSize(new Dimension(width, height));
    setLocationRelativeTo(null);
    Rectangle windowRect = getBounds();

    _splashImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    Graphics2D g2 = (Graphics2D)_splashImage.getGraphics();

    try
    {
      Robot robot = new Robot(getGraphicsConfiguration().getDevice());
      BufferedImage capture = robot.createScreenCapture(new Rectangle(windowRect.x, windowRect.y,
          windowRect.width, windowRect.height));
      g2.drawImage(capture, null, 0, 0);
    }
    catch (AWTException e)
    {
      Assert.expect(false);
    }

// this code was for drawing a shadow of limited transpar
//    BufferedImage shadow = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
//    Graphics g = shadow.getGraphics();
//    g.setColor(new Color(0.0f, 0.0f, 0.0f, 0.0f)); // final param of 0.0f makes it totally transparent
//    g.fillRoundRect(6, 6, width, height, 12, 12);
//    g.fillRect(0, 0, width, height);

//    g2.drawImage(shadow, getBlurOp(7), 0, 0);
    g2.drawImage(_baseImage, 0, 0, this);
  }

  /**
   * Used to create a blur effect for any shadowing that might happen.  This is currently not used in our splash screens
   * @author Andy Mechtenberg
   */
  private ConvolveOp getBlurOp(int size)
  {
    float[] data = new float[size * size];
    float value = 1 / (float)(size * size);
    for (int i = 0; i < data.length; i++)
    {
      data[i] = value;
    }
    return new ConvolveOp(new Kernel(size, size, data));
  }

  /**
    * This method pops up the splash screen and starts the loadins animation timer.
    *
    * @author Steve Anonson
    */
  public void started()
  {
    setVisible(true);
    if (_usingTimer)
    {
      _timer.start();
      // Make sure screen appears a minimum amount of time to avoid flashing.
      try
      {
        Thread.sleep(_MIN_SHOW_TIME);
      }
      catch (InterruptedException e)
      {
        /* do nothing */
      }
    }
  }

  /**
    * This method stops the animation timers and destroys the screen.
    *
    * @author Steve Anonson
    */
  public void finished()
  {
    if (_usingTimer)
    {
      int showTime = _dots * _DELAY;
      if (showTime < _MIN_SHOW_TIME)
      {
        // Make sure screen appears a minimum amount of time to avoid flashing.
        try
        {
          Thread.sleep(_MIN_SHOW_TIME - showTime);
        }
        catch (InterruptedException e)
        {
          // do nothing
        }
      }
      _timer.stop();
    }
    setVisible(false);
    dispose();
  }

  /**
    * This method creats the contents of the splash screen.  It consists of
    * a label at the top to display the application name, the Agilent logo in
    * a label in the middle and a loading animation in a label at the bottom.
    *
    * @author Steve Anonson
    */
  private void jbInit()
  {
    _southLabBorder = BorderFactory.createEmptyBorder(4,0,0,0);
    _northLabBorder = BorderFactory.createEmptyBorder(0,0,4,0);
    _borderPanelBorder = BorderFactory.createEmptyBorder(4,4,4,4);
//    Color backgroundColor = new Color(50, 85, 145);
    this.getContentPane().setBackground(CommonUtil._SECONDARY_COLOR_BACKGROUND);
    _borderPanel.setBackground(CommonUtil._SECONDARY_COLOR_BACKGROUND);
    _borderPanel.setBorder(_borderPanelBorder);
    _borderPanel.setLayout(_borderPanelLayout);
    if (_appName != null)
    {
      _northLabel.setText(_appName);
    }
    _centerLabel.setForeground(Color.black);
    _centerLabel.setBorder(BorderFactory.createRaisedBevelBorder());
    _centerLabel.setBackground(Color.WHITE);
    _centerLabel.setFont(new java.awt.Font("Serif", 1, 48));
    _centerLabel.setOpaque(true);
    _centerLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _southLabel.setForeground(Color.BLACK);
    _southLabel.setBorder(_southLabBorder);
    _southLabel.setFont(new java.awt.Font("SansSerif", 1, 12));
    _southLabel.setText(_loadingStr);
    _northLabel.setForeground(Color.BLACK);
    _northLabel.setBorder(_northLabBorder);
    _northLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _northLabel.setBackground(Color.WHITE);
    _northLabel.setFont(new java.awt.Font("SansSerif", 1, 24));
    this.getContentPane().add(_borderPanel, BorderLayout.CENTER);
    _borderPanel.add(_centerLabel, BorderLayout.CENTER);
    _borderPanel.add(_southLabel, BorderLayout.SOUTH);
    _borderPanel.add(_northLabel, BorderLayout.NORTH);
  }

  /**
    * This method positions the splash screen in the middle of the display screen.
    *
    * @author Steve Anonson
    */
  private void centerOnScreen()
  {
    // Position the dialog in the center of the screen.
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = getSize();
    if (frameSize.height > screenSize.height)
      frameSize.height = screenSize.height;
    if (frameSize.width > screenSize.width)
      frameSize.width = screenSize.width;
    setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
  }

  /**
    * This method handles the animation in the bottom label.  The word "Loading"
    * appears with dot "." added after it.  When 20 dots are reached it reset to
    * one dot again.
    *
    * @author Steve Anonson
    */
  private void updateScreen()
  {
    _dots++;
    if (_dots < 0)
      _dots = 1;
    if ((_dots % _MAX_DOTS) == 1)
      _loadingStr = StringLocalizer.keyToString("GUI_LOADING_MESSAGE_KEY") + " .";
    else
      _loadingStr = _loadingStr + " .";
    _southLabel.setText(_loadingStr);
    if (_dots > _MAX_DOTS)
      _dots = 0;
  }
/*
  public static void main(String[] args)
  {
    com.axi.v810.util.AssertUtil.setUpAssert("AgilentSplashScreen", null);
    AgilentSplashScreen splashScreen = new AgilentSplashScreen("Splash Screen Test");
//    AgilentSplashScreen splashScreen = new AgilentSplashScreen();
    splashScreen.invokedStandalone = true;
    splashScreen.started();
//    splashScreen.finished();
  }
  private boolean invokedStandalone = false;
*/
}
