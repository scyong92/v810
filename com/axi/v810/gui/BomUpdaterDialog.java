package com.axi.v810.gui;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.filechooser.FileFilter;

/**
 * This is a dialog to allow user to upload text file (with extension .txt)
 * so that they can quickly set the components to Test and No Test.
 * 
 * It is simlar to NoLoad settings file but now they just need to put in a
 * simple text file.
 * 
 * @author Cheah Lee Herng
 */
public class BomUpdaterDialog extends EscapeDialog
{
    private static boolean _runFromMain = false;
    
    private static final String _appName = StringLocalizer.keyToString("BUD_GUI_NAME_KEY");
    private static final String _assertLogFile = "BomUpdaterDialog";
    private static final String _textFileFilterDescription = StringLocalizer.keyToString("BUD_ZIP_FILE_DESCRIPTION_KEY");
    
    // gui components
    private JPanel _actionPanel = new JPanel();
    private JButton _updateButton = new JButton();
    private JButton _exitButton = new JButton();
    private JFileChooser _setComponentToTestFileChooser = null;
    private JFileChooser _setComponentToNoTestFileChooser = null;
    
    private String _setComponentToTestInputDirectory = null;
    private String _setComponentToNoTestInputDirectory = null;
    private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
    
    private JRadioButton _chooseSetComponentToTestFileRadioButton = new JRadioButton("", true); //Ying-Huan.Chu
    private JPanel _chooseSetComponentToTestFilePanel = new JPanel();
    private BorderLayout _chooseSetComponentToTestFilePanelBorderLayout = new BorderLayout();
    private JLabel _setComponentToTestFileLabel = new JLabel();
    private JButton _setComponentToTestFileBrowseButton = new JButton();
    private JTextField _setComponentToTestFileTextField = new JTextField();
    private BorderLayout _setComponentToTestSelectionPanelBorderLayout = new BorderLayout();
    private JPanel _setComponentToTestSelectionPanel = new JPanel();
    
    private JRadioButton _chooseSetComponentToNoTestFileRadioButton = new JRadioButton("", false); //Ying-Huan.Chu
    private JPanel _chooseSetComponentToNoTestFilePanel = new JPanel();
    private BorderLayout _chooseSetComponentToNoTestFilePanelBorderLayout = new BorderLayout();
    private JLabel _setComponentToNoTestFileLabel = new JLabel();
    private JButton _setComponentToNoTestFileBrowseButton = new JButton();
    private JTextField _setComponentToNoTestFileTextField = new JTextField();
    private BorderLayout _setComponentToNoTestSelectionPanelBorderLayout = new BorderLayout();
    private JPanel _setComponentToNoTestSelectionPanel = new JPanel();
    
    private Border _empty55105SpaceBorder;
    private Border _empty5555SpaceBorder;
    private JPanel _buttonPanel = new JPanel();
    private GridLayout _buttonPanelGridLayout = new GridLayout();
    private FlowLayout _actionPanelFlowLayout = new FlowLayout();
    private JPanel _mainPanel = new JPanel();
    
    /**
     * @author Cheah Lee Herng
    */
    public BomUpdaterDialog(Frame parentFrame, boolean modal)
    {
        super(parentFrame, _appName, modal);
        Assert.expect(parentFrame != null);
        try
        {
          jbInit();
        }
        catch(RuntimeException e)
        {
          Assert.logException(e);
        }
    }
    
    /**
     * @author Cheah Lee Herng
    */
    public static void main(String[] args)
    {
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _runFromMain = true;
              MainMenuGui mainMenu = MainMenuGui.getInstance();
              mainMenu.start();
              BomUpdaterDialog bomUpdaterDialog = new BomUpdaterDialog(mainMenu, true);
              bomUpdaterDialog.pack();
              bomUpdaterDialog.setVisible(true);
            }
            catch (RuntimeException re)
            {
              re.printStackTrace();
              Assert.logException(re);
            }
          }
        });
    }
    
    /**
     * @author Cheah Lee Herng
    */
    private void jbInit()
    {
        _empty55105SpaceBorder = BorderFactory.createEmptyBorder(5,5,10,5);
        _empty5555SpaceBorder = BorderFactory.createEmptyBorder(5,5,5,5);
        
        _updateButton.setHorizontalTextPosition(SwingConstants.CENTER);
        _updateButton.setText(StringLocalizer.keyToString("BUD_GUI_UPDATE_BUTTON_KEY"));
        _updateButton.setEnabled(false);
        _updateButton.addActionListener(new java.awt.event.ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            updateButton_actionPerformed(e);
          }
        });
        _exitButton.setHorizontalTextPosition(SwingConstants.CENTER);
        _exitButton.setText(StringLocalizer.keyToString("GUI_EXIT_BUTTON_KEY"));
        _exitButton.setMnemonic((StringLocalizer.keyToString("GUI_EXIT_BUTTON_MNEMONIC_KEY")).charAt(0));
        _exitButton.addActionListener(new java.awt.event.ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            exitButton_actionPerformed(e);
          }
        });
        _actionPanel.setLayout(_actionPanelFlowLayout);
        setResizable(true);
        _actionPanel.setMinimumSize(new Dimension(405, 37));
        _chooseSetComponentToTestFilePanel.setLayout(_chooseSetComponentToTestFilePanelBorderLayout);
        _setComponentToTestFileLabel.setHorizontalAlignment(SwingConstants.LEFT);
        _setComponentToTestFileLabel.setHorizontalTextPosition(SwingConstants.CENTER);
        _setComponentToTestFileLabel.setLabelFor(_setComponentToTestFileTextField);
        _setComponentToTestFileLabel.setText(StringLocalizer.keyToString("BUD_GUI_SET_COMPONENT_TO_TEST_LABEL_KEY"));
        _setComponentToTestFileBrowseButton.setHorizontalTextPosition(SwingConstants.CENTER);
        _setComponentToTestFileBrowseButton.setText(StringLocalizer.keyToString("GUI_BROWSE_BUTTON_KEY"));
        _setComponentToTestFileBrowseButton.addActionListener(new java.awt.event.ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            setComponentToTestFileBrowseButton_actionPerformed(e);
          }
        });
        
        _setComponentToTestFileTextField.setText("");
        _setComponentToTestFileTextField.setHorizontalAlignment(SwingConstants.LEFT);
        _setComponentToTestFileTextField.setPreferredSize(new Dimension(220, 20));
        _setComponentToTestFileTextField.setEditable(false);
        _setComponentToTestSelectionPanelBorderLayout.setHgap(10);
        _setComponentToTestSelectionPanelBorderLayout.setVgap(10);
        _setComponentToTestSelectionPanel.setLayout(_setComponentToTestSelectionPanelBorderLayout);
        _setComponentToTestSelectionPanel.setBorder(_empty55105SpaceBorder);
        _setComponentToTestSelectionPanel.setMinimumSize(new Dimension(405, 27));
        
        _chooseSetComponentToNoTestFilePanel.setLayout(_chooseSetComponentToNoTestFilePanelBorderLayout);
        _setComponentToNoTestFileLabel.setHorizontalAlignment(SwingConstants.LEFT);
        _setComponentToNoTestFileLabel.setHorizontalTextPosition(SwingConstants.CENTER);
        _setComponentToNoTestFileLabel.setLabelFor(_setComponentToNoTestFileTextField);
        _setComponentToNoTestFileLabel.setText(StringLocalizer.keyToString("BUD_GUI_SET_COMPONENT_TO_NO_TEST_LABEL_KEY"));
        _setComponentToNoTestFileBrowseButton.setHorizontalTextPosition(SwingConstants.CENTER);
        _setComponentToNoTestFileBrowseButton.setText(StringLocalizer.keyToString("GUI_BROWSE_BUTTON_KEY"));
        _setComponentToNoTestFileBrowseButton.setEnabled(false); //Ying-Huan.Chu
        _setComponentToNoTestFileBrowseButton.addActionListener(new java.awt.event.ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            setComponentToNoTestFileBrowseButton_actionPerformed(e);
          }
        });
        
        _setComponentToNoTestFileTextField.setText("");
        _setComponentToNoTestFileTextField.setHorizontalAlignment(SwingConstants.LEFT);
        _setComponentToNoTestFileTextField.setPreferredSize(new Dimension(220, 20));
        _setComponentToNoTestFileTextField.setEnabled(false); //Ying-Huan.Chu
        _setComponentToNoTestFileTextField.setEditable(false);
        _setComponentToNoTestSelectionPanelBorderLayout.setHgap(10);
        _setComponentToNoTestSelectionPanelBorderLayout.setVgap(10);
        _setComponentToNoTestSelectionPanel.setLayout(_setComponentToNoTestSelectionPanelBorderLayout);
        _setComponentToNoTestSelectionPanel.setBorder(_empty55105SpaceBorder);
        _setComponentToNoTestSelectionPanel.setMinimumSize(new Dimension(405, 27));
        
        _buttonPanel.setLayout(_buttonPanelGridLayout);
        _buttonPanelGridLayout.setColumns(2);
        _buttonPanelGridLayout.setHgap(10);
        _mainPanel.setLayout(new BorderLayout(5,10));
        
        _buttonPanel.add(_updateButton, null);
        _buttonPanel.add(_exitButton, null);
        _actionPanel.add(_buttonPanel, null);
        
        _setComponentToTestSelectionPanel.add(_setComponentToTestFileLabel, BorderLayout.WEST);
        _setComponentToTestSelectionPanel.add(_setComponentToTestFileBrowseButton, BorderLayout.EAST);
        _setComponentToTestSelectionPanel.add(_setComponentToTestFileTextField, BorderLayout.CENTER);
        _setComponentToNoTestSelectionPanel.add(_setComponentToNoTestFileLabel, BorderLayout.WEST);
        _setComponentToNoTestSelectionPanel.add(_setComponentToNoTestFileBrowseButton, BorderLayout.EAST);
        _setComponentToNoTestSelectionPanel.add(_setComponentToNoTestFileTextField, BorderLayout.CENTER);
        _mainPanel.add(_actionPanel,  BorderLayout.SOUTH);
        
        getContentPane().add(_mainPanel,  BorderLayout.CENTER);
        
        //Ying-Huan.Chu
        _chooseSetComponentToTestFileRadioButton.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            _chooseSetComponentToTestFileRadioButton.setSelected(true);
            _setComponentToTestFileBrowseButton.setEnabled(true);
            _setComponentToTestFileTextField.setEnabled(true);
            
            _chooseSetComponentToNoTestFileRadioButton.setSelected(false);
            _setComponentToNoTestFileBrowseButton.setEnabled(false);
            _setComponentToNoTestFileTextField.setText("");
            _setComponentToNoTestFileTextField.setEnabled(false);
          }
        });
        
        //Ying-Huan.Chu
        _chooseSetComponentToNoTestFileRadioButton.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            _chooseSetComponentToTestFileRadioButton.setSelected(false);
            _setComponentToTestFileBrowseButton.setEnabled(false);
            _setComponentToTestFileTextField.setText("");
            _setComponentToTestFileTextField.setEnabled(false);
            
            _chooseSetComponentToNoTestFileRadioButton.setSelected(true);
            _setComponentToNoTestFileBrowseButton.setEnabled(true);
            _setComponentToNoTestFileTextField.setEnabled(true);
          }
        });
        
        _chooseSetComponentToTestFilePanel.add(_chooseSetComponentToTestFileRadioButton, BorderLayout.WEST);
        _chooseSetComponentToTestFilePanel.add(_setComponentToTestSelectionPanel, BorderLayout.EAST);
        _chooseSetComponentToNoTestFilePanel.add(_chooseSetComponentToNoTestFileRadioButton, BorderLayout.WEST);
        _chooseSetComponentToNoTestFilePanel.add(_setComponentToNoTestSelectionPanel, BorderLayout.EAST);
        
        _mainPanel.add(_chooseSetComponentToTestFilePanel, BorderLayout.NORTH);
        _mainPanel.add(_chooseSetComponentToNoTestFilePanel, BorderLayout.CENTER);
        _mainPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    }
    
    /**
     * @author Cheah Lee Herng
    */
    private void updateButton_actionPerformed(ActionEvent e)
    {
        Assert.expect(_setComponentToTestFileTextField != null);
        Assert.expect(_setComponentToNoTestFileTextField != null);
        
        String setComponentToTestFileName = null;
        String setComponentToNoTestFileName = null;
        
        if (_setComponentToTestFileTextField != null)
            setComponentToTestFileName = _setComponentToTestFileTextField.getText();
        
        if (_setComponentToNoTestFileTextField != null)
            setComponentToNoTestFileName = _setComponentToNoTestFileTextField.getText();
        
        if ( setComponentToTestFileName.length() == 0 && setComponentToNoTestFileName.length() == 0)
        {
          JOptionPane.showMessageDialog( this, StringLocalizer.keyToString("BUD_FILE_NOT_SPECIFIED_KEY"),
                                         _appName, JOptionPane.ERROR_MESSAGE );

          _updateButton.setEnabled(false);
          return;
        }
        
        updateBom(setComponentToTestFileName, setComponentToNoTestFileName);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void updateBom(final String setComponentToTestFileName, final String setComponentToNoTestFileName)
    {
        boolean setComponentToTest = false;
        boolean setComponentToNoTest = false;
        
        boolean isFileNameValid = true;
        if (setComponentToTestFileName.length() > 0)
        {
            isFileNameValid = isFileNameValid(setComponentToTestFileName, true);
            if (isFileNameValid)
                setComponentToTest = true;
        }
        
        if (isFileNameValid && setComponentToNoTestFileName.length() > 0)
        {
            isFileNameValid = isFileNameValid(setComponentToNoTestFileName, false);
            if (isFileNameValid)
                setComponentToNoTest = true;
        }
        
        if (isFileNameValid == false)
            return;
        
        // check to see if a project is loaded. Only loaded project can update BOM.
        if (Project.isCurrentProjectLoaded() == false)
        {
          LocalizedString message = new LocalizedString("BUD_PROJECT_NOT_LOADED_KEY", new Object[]{});
          JOptionPane.showMessageDialog(this, StringUtil.format(StringLocalizer.keyToString(message), 50),
                                      _appName, JOptionPane.ERROR_MESSAGE);

          _updateButton.setEnabled(false);
          return;
        }
        
        final boolean finalSetComponentToTest = setComponentToTest;
        final boolean finalSetComponentToNoTest = setComponentToNoTest;
        
        final BusyCancelDialog busyDialog = new BusyCancelDialog(
                                                            (Frame)getOwner(),
                                                            StringLocalizer.keyToString("BUD_UPDATING_KEY"),
                                                            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                            true);
        busyDialog.pack();
        SwingUtils.centerOnComponent(busyDialog, (Frame)getOwner());
        _swingWorkerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              BomUpdateFileReader bomUpdaterFileReader = BomUpdateFileReader.getInstance();
              
              if (finalSetComponentToTest)
              {
                bomUpdaterFileReader.readDataFile(Project.getCurrentlyLoadedProject(), setComponentToTestFileName, true);
              }
              
              if (finalSetComponentToNoTest)
              {
                bomUpdaterFileReader.readDataFile(Project.getCurrentlyLoadedProject(), setComponentToNoTestFileName, false); 
              }
                
              if (bomUpdaterFileReader.getMissingReferenceDesignators().isEmpty() == false)
              {
                new ItemListBoxDialog(MainMenuGui.getInstance(),
                                      StringLocalizer.keyToString("BUD_MISSING_COMPONENT_KEY"),
                                      StringLocalizer.keyToString("BUD_MISSING_COMPONENT_MESSAGE_KEY"),
                                      StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                      bomUpdaterFileReader.getMissingReferenceDesignators());
              }
              
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  busyDialog.dispose();
                  JOptionPane.showMessageDialog(BomUpdaterDialog.this, StringLocalizer.keyToString("BUD_UPDATE_COMPLETED_KEY"),
                                                _appName, JOptionPane.INFORMATION_MESSAGE);
                }
              });
            }
            catch (final DatastoreException ie)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                  setEnabled(true); // allow user to press a button
                  MessageDialog.reportIOError(BomUpdaterDialog.this, ie.getLocalizedMessage());
                }
              });
            }
            finally
            {
                _setComponentToTestFileTextField.setText("");
                _setComponentToNoTestFileTextField.setText("");
                
                if (busyDialog.isVisible())
                    busyDialog.dispose();
            }
          }
        });
        busyDialog.setVisible(true);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    private boolean isFileNameValid(final String textFileName, boolean setComponentToTest)
    {
        Assert.expect(textFileName != null);
        
        String extension = FileUtil.getExtension(textFileName).toLowerCase();
        
        if ( extension.endsWith( FileName.getTextFileExtension().toLowerCase()) == false )
        {
          if (setComponentToTest)
          {
              JOptionPane.showMessageDialog( this, StringLocalizer.keyToString("BUD_INVALID_SET_COMPONENT_TO_TEST_FILE_EXTENSION_KEY"),
                                         _appName, JOptionPane.ERROR_MESSAGE );
          }
          else
          {
              JOptionPane.showMessageDialog( this, StringLocalizer.keyToString("BUD_INVALID_SET_COMPONENT_TO_NO_TEST_FILE_EXTENSION_KEY"),
                                         _appName, JOptionPane.ERROR_MESSAGE );
          }

          _updateButton.setEnabled(false);
          return false;
        }
        
        String parent = FileUtil.getParent(textFileName);
        
        if (parent.length() == 0) // need to specify full path.
        {
          if (setComponentToTest)
          {
              JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("BUD_SET_COMPONENT_TO_TEST_DIRECTORY_NOT_SPECIFIED_KEY"),
                      _appName, JOptionPane.ERROR_MESSAGE);
          }
          else
          {
              JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("BUD_SET_COMPONENT_TO_NO_TEST_DIRECTORY_NOT_SPECIFIED_KEY"),
                      _appName, JOptionPane.ERROR_MESSAGE);
          } 

          _updateButton.setEnabled(false);
          return false;
        }
        
        File inFile = new File(textFileName);
        
        if (inFile.exists() == false)
        {
          if (setComponentToTest)
          {
              JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("BUD_SET_COMPONENT_TO_TEST_FILE_DOES_NOT_EXIST_KEY"),
                      _appName, JOptionPane.ERROR_MESSAGE);
          }
          else
          {
              JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("BUD_SET_COMPONENT_TO_NO_TEST_FILE_DOES_NOT_EXIST_KEY"),
                      _appName, JOptionPane.ERROR_MESSAGE);
          }
            
          _updateButton.setEnabled(false);
          return false;
        }
        return true;
    }
    
    /**
     * @author Cheah Lee Herng
    */
    private void exitButton_actionPerformed(ActionEvent e)
    {
        exitDialog();
    }
    
    /**
     * @author Cheah Lee Herng
    */
    void setComponentToTestFileBrowseButton_actionPerformed(ActionEvent e)
    {
        String fileNameFullPath = selectSetComponentToTestFile();

        if (fileNameFullPath.length() > 0)
        {
          _setComponentToTestInputDirectory = FileUtil.getParent(fileNameFullPath);
          _setComponentToTestFileTextField.setText(fileNameFullPath);
          _updateButton.setEnabled(true); 
        }
    }
    
    /**
     * @author Cheah Lee Herng
    */
    void setComponentToNoTestFileBrowseButton_actionPerformed(ActionEvent e)
    {
        String fileNameFullPath = selectSetComponentToNoTestFile();

        if (fileNameFullPath.length() > 0)
        {
          _setComponentToNoTestInputDirectory = FileUtil.getParent(fileNameFullPath);
          _setComponentToNoTestFileTextField.setText(fileNameFullPath);
          _updateButton.setEnabled(true); 
        }
    }
    
    /**
     * @author Cheah Lee Herng
    */
    private void exitDialog()
    {
        if (_runFromMain)
          System.exit(0);
        else
        {
          setVisible(false);
          dispose();
        }
    }
    
    /**
     * This method displays a file chooser and return the file selected by the user.
     * @author Cheah Lee Herng
    */
    private String selectSetComponentToTestFile()
    {
        if ( _setComponentToTestFileChooser == null )
        {
           _setComponentToTestFileChooser = new JFileChooser()
           {
             public void approveSelection()
             {
               File selectedFile = getSelectedFile();
               if (selectedFile.exists())
                 super.approveSelection();
               else
               {
                 String message = StringLocalizer.keyToString(new LocalizedString("FILE_DIALOG_SELECTION_DOES_NOT_EXIST_KEY", new Object[]{selectedFile}));
                 JOptionPane.showMessageDialog(this, StringUtil.format(message, 50));
               }
             }
           };

           //_setComponentToTestFileChooser.setCurrentDirectory(new File(Directory.getLegacyNdfDir()));

           // initialize the JFIle Dialog.
           FileFilter fileFilter = _setComponentToTestFileChooser.getAcceptAllFileFilter();
           _setComponentToTestFileChooser.removeChoosableFileFilter(fileFilter);

           // create and instance of Zip File Filter
           FileFilterUtil textFileFilter = new FileFilterUtil(FileName.getTextFileExtension(), _textFileFilterDescription);

           _setComponentToTestFileChooser.addChoosableFileFilter(textFileFilter);
           _setComponentToTestFileChooser.setFileFilter(textFileFilter); // default to new txt file
        }

        if ( _setComponentToTestFileChooser.showDialog(this, StringLocalizer.keyToString("GUI_SELECT_FILE_KEY")) != JFileChooser.APPROVE_OPTION )
          return "";

        File selectedFile = _setComponentToTestFileChooser.getSelectedFile();

        if (selectedFile.exists())
        {
          if (selectedFile.isFile())
            return selectedFile.getPath();
          else
            return "";
        }
        else
          return "";
    }
    
    /**
     * This method displays a file chooser and return the file selected by the user.
     * @author Cheah Lee Herng
    */
    private String selectSetComponentToNoTestFile()
    {
        if ( _setComponentToNoTestFileChooser == null )
        {
          // start in the same directory where the zip dialog puts zipped projects by default
           _setComponentToNoTestFileChooser = new JFileChooser()
           {
             public void approveSelection()
             {
               File selectedFile = getSelectedFile();
               if (selectedFile.exists())
                 super.approveSelection();
               else
               {
                 String message = StringLocalizer.keyToString(new LocalizedString("FILE_DIALOG_SELECTION_DOES_NOT_EXIST_KEY", new Object[]{selectedFile}));
                 JOptionPane.showMessageDialog(this, StringUtil.format(message, 50));
               }
             }
           };

           _setComponentToNoTestFileChooser.setCurrentDirectory(new File(Directory.getLegacyNdfDir()));

           // initialize the JFIle Dialog.
           FileFilter fileFilter = _setComponentToNoTestFileChooser.getAcceptAllFileFilter();
           _setComponentToNoTestFileChooser.removeChoosableFileFilter(fileFilter);

           // create and instance of Zip File Filter
           FileFilterUtil textFileFilter = new FileFilterUtil(FileName.getTextFileExtension(), _textFileFilterDescription);

           _setComponentToNoTestFileChooser.addChoosableFileFilter(textFileFilter);
           _setComponentToNoTestFileChooser.setFileFilter(textFileFilter); // default to new txt file
        }

        if ( _setComponentToNoTestFileChooser.showDialog(this, StringLocalizer.keyToString("GUI_SELECT_FILE_KEY")) != JFileChooser.APPROVE_OPTION )
          return "";

        File selectedFile = _setComponentToNoTestFileChooser.getSelectedFile();

        if (selectedFile.exists())
        {
          if (selectedFile.isFile())
            return selectedFile.getPath();
          else
            return "";
        }
        else
          return "";
    }
}
