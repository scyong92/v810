package com.axi.v810.gui.monitorGamma;

import java.io.*;
import javax.swing.*;

import com.axi.v810.datastore.*;
import com.axi.util.*;
import com.axi.guiUtil.SwingUtils;

/**
 * This class is not used in the 5DX anymore. It was created before
 * sendMessageToServer was used to launch the GUI. It is kept here for
 * reference purpose only.
 *
 * @author Vincent Wong
 */
public class SetMonitorGamma
{
  public static void main( final String[] args )
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          UIManager.setLookAndFeel(
              UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception e)
        {
          Assert.logException(e);
        }

        if (args.length == 0)
        {
          MonitorGammaGui monitorGammaGui = new MonitorGammaGui();
          monitorGammaGui.start();
          monitorGammaGui.setVisible(true);
        }
        else if (args.length == 1)
        {
          try
          {
            MonitorGammaGui monitorGammaGui = new MonitorGammaGui(args[0]);
            monitorGammaGui.start();
            monitorGammaGui.setVisible(true);
          }
          catch (DatastoreException ex)
          {
            // do nothing
          }
        }
        else
        {
          System.out.println(
              "Usage: java SetMonitorGamma [config_file]");
          System.exit(0);
        }
      }
    });
  }
}
