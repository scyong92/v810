package com.axi.v810.gui.monitorGamma;

import java.awt.*;
import javax.swing.*;

/**
 *  This class creates a panel with strips of alternate black and white.
 *  The average gray level is therefore 50%.
 *
 *  @author Vincent Wong
 */
public class DitherPanel extends JPanel
{
  /** Create a panel with strips of alternate black and white.
   *
   *  @author Vincent Wong
   */
  public DitherPanel()
  {
    super();
  }

  /*
   *  @author Vincent Wong
   */
  public void paint( Graphics graphics )
  {
    Dimension size = getSize();

    int	top    = 0;
    int	bottom = size.height-1;
    int	left   = 0;
    int	right  = size.width-1;

    graphics.setColor( Color.black );

    for ( int y = top; y <= bottom; y +=2 )
      graphics.drawLine( left, y, right, y );

    graphics.setColor( Color.white );

    for ( int y = top+1; y <= bottom; y +=2 )
      graphics.drawLine( left, y, right, y );
  }
}
