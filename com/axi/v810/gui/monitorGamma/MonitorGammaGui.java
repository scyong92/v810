package com.axi.v810.gui.monitorGamma;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.monitorGamma.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;


/**
 * This class contains the GUI of the Display Adjustment Utility.
 * The instance of the class is associated with a configuration file.
 * In xRayTest, MonitorGammaGUI() is used. The configuration file is
 * defined internally. In other environment such as the ART,
 * MonitorGammaGUI( String filename ) should be used instead.
 *
 * @author Vincent Wong
 */
public class MonitorGammaGui extends JFrame
{
  private final static double _MAXGRAY = 255.;

  private String _filename;

  private double _gamma;
  private double _gammaSaved;
  private MonitorGamma _monitorGamma;
  private JPanel _contentPane;
  private JPanel _displayPanel = new JPanel();
  private JPanel _ditherPanel = new DitherPanel();
  private JPanel _grayPanel = new JPanel();
  private GridLayout _gridLayout1 = new GridLayout();
  private JPanel _buttonPanel = new JPanel();
  private JButton _setButton = new JButton();
  private GridLayout _gridLayout2 = new GridLayout();
  private JPanel _controlPanel = new JPanel();
  private JButton _cancelButton = new JButton();
  private JLabel _hintLabel = new JLabel();
  private BorderLayout _borderLayout2 = new BorderLayout();
  private BorderLayout _borderLayout1 = new BorderLayout();
  private JSlider _slider = new JSlider();

  /**
   *  User-defined storage file GUI constructor. Use start() to create the GUI.
   *
   *  @param filename is the full pathname of the storage file of the setting.
   *  @author Vincent Wong
   */
  public MonitorGammaGui( String filename ) throws DatastoreException
  {
    Assert.expect( filename != null );

    _filename = filename; // save it for error display purpose
    _monitorGamma = new MonitorGamma( filename );
    _gamma = _monitorGamma.getValue(); // let the caller handles the exception
    _gammaSaved = _gamma; // gamma value saved in the system
  }

  /**
   *  5DX GUI constructor. Storage file is internally defined.
   *
   *  @author Vincent Wong
   */
  public MonitorGammaGui()
  {
    String filename = FileName.getDisplayConfigFullPath();
    Assert.expect( filename != null );

    _filename = filename; // save it for error display purpose
    _monitorGamma = new MonitorGamma( filename );

    try
    {
      _gamma = _monitorGamma.getValue();
      _gammaSaved = _gamma; // gamma value saved in the system
    }
    catch (DatastoreException de)
    {
      MessageDialog.showErrorDialog(this, de.getLocalizedMessage(), StringLocalizer.keyToString("GAMMAGUI_GUI_NAME_KEY"));
      stop();
    }
  }

  /**
   *  Create the GUI but make it invisible.
   *
   *  @author Vincent Wong
   */
  public void start()
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);

        try
        {
          jbInit();
          restoreSetting();
        }
        catch (Exception e)
        {
          Assert.logException(e);
          stop();
        }

        pack();

        // Center the window
        SwingUtils.centerOnScreen(MonitorGammaGui.this);

        stop(); // stop for now
      }
    });
  }

  /**
   *  Make the GUI invisible.
   *
   *  @author Vincent Wong
   */
  public void stop()
  {
    setVisible( false );

    //----------------------------------------
    // Set the config file read-only so that the C++-side knows when
    // to stop reading for the latest value.
    //
    _monitorGamma.setReadOnly();
    dispose();
  }

  /**
   *  Set GUI visibility. If visible, bring it to front as well.
   *
   *  @author Vincent Wong
   */
  public void setVisible(boolean visible)
  {
    super.setVisible(visible);

    if ( visible )
      toFront(); // bring the window to the front
  }

  /** Component initialization */
  private void jbInit() throws Exception
  {
    _contentPane = (JPanel)this.getContentPane();
    _contentPane.setLayout(_borderLayout2);
    this.setSize(new Dimension(421, 318));
    this.setTitle("Display Adjustment Utility");
    this.addWindowListener(new java.awt.event.WindowAdapter()
    {
      public void windowClosing(WindowEvent e)
      {
        this_windowClosing(e);
      }
    });
    this.setIconImage(
      new ImageIcon(Image5DX.class.getResource(Image5DX.FRAME_X5000)).getImage());
    this.rootPane.setDefaultButton( _setButton );
    _displayPanel.setBorder(BorderFactory.createLoweredBevelBorder());
    _displayPanel.setMinimumSize(new Dimension(24, 220));
    _displayPanel.setPreferredSize(new Dimension(390, 220));
    _displayPanel.setLayout(_gridLayout1);
    _ditherPanel.setRequestFocusEnabled(false);
    _grayPanel.setRequestFocusEnabled(false);
    _buttonPanel.setLayout(_gridLayout2);
    _setButton.setMaximumSize(new Dimension(73, 27));
    _setButton.setMinimumSize(new Dimension(73, 27));
    _setButton.setPreferredSize(new Dimension(73, 27));
    _setButton.setToolTipText("Save current setting and exit");
    _setButton.setMnemonic('0');
    _setButton.setText("Set");
    _setButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setButton_actionPerformed(e);
      }
    });
    _controlPanel.setLayout(_borderLayout1);
    _cancelButton.setToolTipText("Discard changes and exit");
    _cancelButton.setMnemonic('0');
    _cancelButton.setText("Cancel");
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _hintLabel.setBorder(BorderFactory.createEtchedBorder());
    _hintLabel.setMaximumSize(new Dimension(381, 21));
    _hintLabel.setPreferredSize(new Dimension(381, 21));
    _hintLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _hintLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _hintLabel.setText(
      "Move the slider until both panes appear to have the same brightness.");
    _controlPanel.setMinimumSize(new Dimension(100, 50));
    _controlPanel.setPreferredSize(new Dimension(100, 50));
    _slider.setMaximum( 255 );
    _slider.setPreferredSize(new Dimension(230, 24));
    _slider.setMinimumSize(new Dimension(50, 24));
    _slider.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        slider_stateChanged(e);
      }
    });
    _contentPane.add(_displayPanel, BorderLayout.CENTER);
    _displayPanel.add(_ditherPanel, null);
    _displayPanel.add(_grayPanel, null);
    _contentPane.add(_controlPanel, BorderLayout.SOUTH);
    _controlPanel.add(_hintLabel, BorderLayout.SOUTH);
    _controlPanel.add(_buttonPanel, BorderLayout.EAST);
    _buttonPanel.add(_setButton, null);
    _buttonPanel.add(_cancelButton, null);
    _controlPanel.add(_slider, BorderLayout.CENTER);
  }

  /**
   *  Restore the GUI to reflect the stored setting.
   *
   *  @author Vincent Wong
   */
  private void restoreSetting()
  {
    int max = (int)(_MAXGRAY * Math.pow(0.5, 1.0 / MonitorGamma.getMaxGamma()));
    int min = (int)(_MAXGRAY * Math.pow(0.5, 1.0 / MonitorGamma.getMinGamma()));
    int gray = (int)(_MAXGRAY * Math.pow(0.5, 1.0 / _gammaSaved));

    _slider.setMaximum( max );
    _slider.setMinimum( min );
    _slider.setValue( gray );
    _grayPanel.setBackground( new Color(gray, gray, gray) );
  }

  /**
   *  Slider changes the gray panel.
   *
   *  @author Vincent Wong
   */
  private void slider_stateChanged(ChangeEvent e)
  {
    int sliderValue = _slider.getValue();

    //----------------------------------------
    // Avoid getting gamma value out of range due to round-off error.
    //
    if ( sliderValue <= _slider.getMinimum() )
      _gamma = MonitorGamma.getMinGamma();
    else if ( sliderValue >= _slider.getMaximum() )
      _gamma = MonitorGamma.getMaxGamma();
    else
      _gamma = Math.log(0.5) / Math.log((double)sliderValue / _MAXGRAY);

    Color BackgroundColor = new Color(sliderValue, sliderValue, sliderValue);
    _grayPanel.setBackground(BackgroundColor);

    try
    {
      _monitorGamma.setValue( _gamma );
      _gammaSaved = _gamma;
    }
    catch ( DatastoreException de )
    {
      // do nothing
      // let the setButton handles it.
    }
  }

  /**
   *  Set button saves the setting.
   *
   *  @author Vincent Wong
   */
  private void setButton_actionPerformed(ActionEvent e)
  {
    try
    {
      _monitorGamma.setValue( _gamma );
      _gammaSaved = _gamma;
    }
    catch (DatastoreException de)
    {
      MessageDialog.showErrorDialog(this, de.getLocalizedMessage(), StringLocalizer.keyToString("GAMMAGUI_GUI_NAME_KEY"));
    }

    stop();
  }

  /**
   *  Cancel button closes the GUI.
   *
   *  @author Vincent Wong
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    setVisible( false ); // do not show the restoration in progress
    restoreSetting();
    stop();
  }

  /**
   *  When the window is closing, restore the gamma setting.
   *  This is necessary because the user may adjust slider bar and then close the window.
   *
   *  @author Vincent Wong
   */
  private void this_windowClosing(WindowEvent e)
  {
    setVisible( false ); // do not show the restoration in progress
    restoreSetting();
    stop();
  }
}
