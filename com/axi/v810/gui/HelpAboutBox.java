package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import com.axi.v810.images.*;
import com.axi.v810.business.license.*;
import com.axi.v810.gui.mainMenu.MainUIMenuBar;

/**
 * @author Erica Wheatcroft
 */
public class HelpAboutBox extends JDialog implements ActionListener
{
  private JPanel _mainPanel = new JPanel();
  private JPanel _centerPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _imagePanel = new JPanel();
  private JPanel _labelPanel = new JPanel();
  private JButton _okButton = new JButton();
  private JLabel _axiImageLabel = new JLabel();
  private ImageIcon _imageIcon;
  private JLabel _titleLabel = new JLabel();
  private JLabel _versionLabel = new JLabel();
  private JLabel _copyrightLabel = new JLabel();
  private JLabel _axiLabel = new JLabel();
  private JLabel _installedDateLabel = new JLabel();
//  private JLabel _macAddressLabel = new JLabel();
  private BorderLayout _mainBorderLayout = new BorderLayout();
  private BorderLayout _centerBorderLayout = new BorderLayout();
  private GridLayout _labelGridLayout = new GridLayout();
  private String _version = StringLocalizer.keyToString("GUI_ABOUT_BOX_VERSION_KEY") + ": " + Version.getVersion();
  private String _copyright = Version.getCopyRight();
  private String _comments = StringLocalizer.keyToString("GUI_ABOUT_BOX_COMMENT_KEY");
  private String _installDate = StringLocalizer.keyToString("GUI_ABOUT_BOX_INSTALLED_DATE_KEY") + ": ";
//  private String _macAddress = StringLocalizer.keyToString("GUI_ABOUT_BOX_MAC_ADDRESS_KEY") + ": ";
  private JPanel _emptyBufferPanel = new JPanel();
  private FlowLayout _imageFlowLayout = new FlowLayout();
  private String _product =StringLocalizer.keyToString("GUI_ABOUT_BOX_MAIN_PRODUCT_NAME_KEY");
  private String _appName;
//  private LicenseManager _licenseManager;

  /**
   * @param frame Frame
   * @param appName String
   * @author Erica Wheatcroft
   */
  public HelpAboutBox(Frame frame, String appName)
  {
    super(frame);

    //set the app name for the caller of this dialog
    _appName = appName;

    // start the license manager so that we can get the mac address
//    _licenseManager = LicenseManager.getInstance();
//    _macAddress += _licenseManager.getHostId();

    //now get the installation date from the Software Config Properties.
    Config config = Config.getInstance();
    _installDate += config.getStringValue(SoftwareConfigEnum.INSTALLATION_DATE);

    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try
    {
      jbInit();
    }
    catch(RuntimeException e)
    {
      e.printStackTrace();
    }
    pack();
  }

  /**
   * @throws RuntimeException
   * @author Erica Wheatcroft
   */
  private void jbInit() throws RuntimeException
  {
    _imageIcon = Image5DX.getImageIcon( Image5DX.AXI );
    this.setTitle(StringLocalizer.keyToString("GUI_ABOUT_BOX_TITLE_KEY") + " " + _appName);
    setResizable(false);
    _mainPanel.setLayout(_mainBorderLayout);
    _centerPanel.setLayout(_centerBorderLayout);
    _imagePanel.setLayout(_imageFlowLayout);
    _imagePanel.setBorder(new EmptyBorder(6, 10, 6, 10));
    _labelGridLayout.setRows(6);
    _labelGridLayout.setColumns(1);
    _titleLabel.setText(_product);
    _versionLabel.setText(_version);
    _copyrightLabel.setText(_copyright);
    _axiLabel.setText(_comments);
    _labelPanel.setLayout(_labelGridLayout);
    _labelPanel.setBorder(new EmptyBorder(6, 5, 6, 5));
    _installedDateLabel.setText(_installDate);
//    _macAddressLabel.setText(_macAddress);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
//    _okButton.setMnemonic(MainUIMenuBar.getGenericMnemonic(_okButton));
    _okButton.addActionListener(this);
    _imageFlowLayout.setHgap(0);
    _imageFlowLayout.setVgap(0);
    _imagePanel.add(_axiImageLabel, null);
    _centerPanel.add(_imagePanel, BorderLayout.WEST);
    this.getContentPane().add(_mainPanel, null);
    _labelPanel.add(_titleLabel, null);
    _labelPanel.add(_versionLabel, null);
//    _labelPanel.add(_macAddressLabel, null);
    _labelPanel.add(_installedDateLabel, null);
    _labelPanel.add(_copyrightLabel, null);
    _labelPanel.add(_axiLabel, null);
    _centerPanel.add(_emptyBufferPanel, BorderLayout.EAST);
    _centerPanel.add(_labelPanel, BorderLayout.CENTER);
    _buttonPanel.add(_okButton, null);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    _mainPanel.add(_centerPanel, BorderLayout.CENTER);
    _axiImageLabel.setIcon(_imageIcon);
  }

  /**
   * @param e WindowEvent
   * @author Erica Wheatcroft
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if(e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      cancel();
    }
    super.processWindowEvent(e);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void cancel()
  {
    setVisible(false);
  }

  /**
   * @param e WindowEvent
   * @author Erica Wheatcroft
   */
  public void actionPerformed(ActionEvent e)
  {
    if(e.getSource() == _okButton)
    {
      cancel();
    }
  }
}
