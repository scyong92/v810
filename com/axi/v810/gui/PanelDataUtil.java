package com.axi.v810.gui;

import java.util.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.algoTuner.*;

/**
 * This is a general interface to the business for common, complex bussiness access
 * routines
 *
 * @author Andy Mechtenberg
 */

public class PanelDataUtil
{
  private static PanelDataUtil _instance;

  /**
   * @author Andy Mechtenberg
   */
  public static synchronized PanelDataUtil getInstance()
  {
    if (_instance == null)
      _instance = new PanelDataUtil();

    return _instance;
  }

  /**
   * @author Andy Mechtenberg
   */
  private PanelDataUtil()
  {
    // do nothing
  }

  /**
   * @author Andy Mechtenberg
   */
  public static void populateComboBoxWithJointTypes(BoardType boardType, JComboBox comboBox)
  {
    Assert.expect(boardType != null);
    Assert.expect(comboBox != null);

    comboBox.removeAllItems();
    comboBox.addItem(new JointTypeChoice()); // the ALL case

    List<JointTypeEnum> jointTypes = boardType.getInspectedJointTypeEnums();
    for (JointTypeEnum jte : jointTypes)
    {
      JointTypeChoice jointTypeChoice = new JointTypeChoice(jte);
      comboBox.addItem(jointTypeChoice);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public static void populateComboBoxWithJointTypes(Panel panel, JComboBox comboBox)
  {
    Assert.expect(panel != null);
    Assert.expect(comboBox != null);

    comboBox.removeAllItems();
    comboBox.addItem(new JointTypeChoice()); // the ALL case

    List<JointTypeEnum> jointTypes = panel.getInspectedJointTypeEnums();
    for (JointTypeEnum jte : jointTypes)
    {
      JointTypeChoice jointTypeChoice = new JointTypeChoice(jte);
      comboBox.addItem(jointTypeChoice);
    }

  }

  /**
   * @author Andy Mechtenberg
   */
  public static void populateComboBoxWithSubtypes(BoardType boardType, JointTypeChoice jointTypeChoice, JComboBox comboBox)
  {
    Assert.expect(boardType != null);
    Assert.expect(comboBox != null);

    comboBox.removeAllItems();
    comboBox.addItem(new SubtypeChoice());  // the ALL choice

    // if joint type is "all" or null, then don't fill in the subtype box
    if ((jointTypeChoice == null) || (jointTypeChoice.isAllFamilies()))
       return;

    List<Subtype> subtypes = boardType.getInspectedSubtypes(jointTypeChoice.getJointTypeEnum());

    for (Subtype caf : subtypes)
    {
      SubtypeChoice subtypeChoice = new SubtypeChoice(caf);
      comboBox.addItem(subtypeChoice);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public static void populateComboBoxWithSubtypes(Panel panel, JointTypeChoice jointTypeChoice, JComboBox comboBox)
  {
    Assert.expect(panel != null);
    Assert.expect(comboBox != null);

    comboBox.removeAllItems();
    comboBox.addItem(new SubtypeChoice());  // the ALL choice

    // if joint type is "all" or null, then don't fill in the subtype box
    if ((jointTypeChoice == null) || (jointTypeChoice.isAllFamilies()))
       return;

    List<Subtype> subtypes = panel.getInspectedSubtypes(jointTypeChoice.getJointTypeEnum());

    for (Subtype caf : subtypes)
    {
      SubtypeChoice subtypeChoice = new SubtypeChoice(caf);
      comboBox.addItem(subtypeChoice);
    }
  }


  /**
   * Determines if the subtype exists in the board type
   * @author Andy Mechtenberg
   */
  public static boolean doesSubtypeExist(BoardType boardType, SubtypeChoice subtypeChoice)
  {
    Assert.expect(subtypeChoice != null);

    if (subtypeChoice.isAllSubtypes())
      return true;
    else
    {
      Subtype targetSubtype = subtypeChoice.getSubtype();
      List<Subtype> allSubtypes = boardType.getInspectedSubtypes(targetSubtype.getJointTypeEnum());
      // I could use the "contains" method to determine, but that does an object compare, which
      // often doesn't work because the datastore list is re-created differently than the persist file
      // So, I'll use a custom compare which will just check the family and name
      for (Subtype subtype : allSubtypes)
      {
        if (subtype.equals(targetSubtype))
          return true;
      }
      return false;
    }
  }
}
