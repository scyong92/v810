package com.axi.v810.gui;

import java.awt.*;
import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: XrayStatusPanel</p>
 *
 * <p>Description: Utility class to provide an X-ray source status indicator.
 * <p>Known users: MainMenuGUI, TestExec
 * <p>This class is an observer of HardwareObservable and monitors XraySource events.
 * When an event occurs, XraySource.areXraysOn() is checked to determine the status.
 * <P>The instantiating class should set borders and sizes as appropriate. It can also
 * provide an optional Font through the constructor.
 * <P>The following status is indicated:<br>
 * Xray status: OFF (black)<br>
 * Xray status: ON (green)
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: AXI</p>
 *
 * @author George Booth
 */

public class XrayStatusPanel extends JPanel implements Observer
{
  private HardwareObservable _hardwareObservable;

  private JLabel _statusXrayLabel;
  private JLabel _statusXrayValue;

  private String _xRaysNotReadyMessage = StringLocalizer.keyToString("HP_XRAY_STATUS_NOT_READY_VALUE_KEY");
  private String _xRaysReadyMessage = StringLocalizer.keyToString("HP_XRAY_STATUS_READY_VALUE_KEY");
  private String _xRaysOffMessage = StringLocalizer.keyToString("HP_XRAY_STATUS_OFF_VALUE_KEY");
  private String _xRaysOnMessage = StringLocalizer.keyToString("HP_XRAY_STATUS_ON_VALUE_KEY");

  private Color _xRaysNotReadyColor = Color.RED;
  private Color _xRaysReadyColor = Color.BLACK;
  private Color _xRaysOffColor = Color.BLACK;
  private Color _xRaysOnColor = new Color(51, 153, 102);  // nice shade of green

  /**
   * @author George Booth
   */
  public XrayStatusPanel()
  {
    jbInit(null);
  }

  /**
   * @author George Booth
   */
  public XrayStatusPanel(Font font)
  {
    jbInit(font);
  }

  /**
   * @author George Booth
   */
  private void jbInit(Font font)
  {
//    _hardwareObservable = HardwareObservable.getInstance();
//    _hardwareObservable.addObserver(this);

    setLayout(new PairLayout(10, 0));

    _statusXrayLabel = new JLabel();
    if (font != null)
    {
      _statusXrayLabel.setFont(font);
    }
    _statusXrayLabel.setText(StringLocalizer.keyToString("HP_XRAY_STATUS_LABEL_KEY") + ": ");
    add(_statusXrayLabel);

    _statusXrayValue = new JLabel();
    if (font != null)
    {
      _statusXrayValue.setFont(font);
    }
    _statusXrayValue.setText(_xRaysOffMessage);
    _statusXrayValue.setFont(FontUtil.getBoldFont(_statusXrayValue.getFont(),Localization.getLocale()));
    _statusXrayValue.setForeground(_xRaysOffColor);
    add(_statusXrayValue);
  }

  /**
   * @author George Booth
   */
  public synchronized void update(final Observable observable, final Object arg)
  {
    Assert.expect(observable != null);

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if(observable instanceof HardwareObservable)
        {
          handleHardwareObservable(arg);
        }
        else
        {
          Assert.expect(false, "No update code for observable: " + observable.getClass().getName());
        }
      }
    });
  }

  /**
 * @author George Booth
 */
private void handleHardwareObservable(Object arg)
{
  if (arg instanceof HardwareEvent)
  {
    HardwareEvent event = (HardwareEvent)arg;
    HardwareEventEnum eventEnum = event.getEventEnum();
    // Xray Source Events
    if(eventEnum instanceof XraySourceEventEnum)
    {
      // something happened, check if x-rays are on or off
      try
      {
        boolean on = XraySource.getInstance().areXraysOn();
        if (on)
        {
//          System.out.println(" XrayStatusPanel() x-rays are ON");
          _statusXrayValue.setText(_xRaysOnMessage);
          _statusXrayValue.setForeground(_xRaysOnColor);
        }
        else
        {
//          System.out.println(" XrayStatusPanel() x-rays are OFF");
          _statusXrayValue.setText(_xRaysOffMessage);
          _statusXrayValue.setForeground(_xRaysOffColor);
        }
      }
      catch (XrayTesterException xte)
      {
        // do nothing
      }
      XraySourceEventEnum xraySourceEventEnum = (XraySourceEventEnum)eventEnum;
      if(xraySourceEventEnum.equals(xraySourceEventEnum.STARTUP))
      {
        if (event.isStart() == false)
        {
//          System.out.println(" XrayStatusPanel().XRAY_SOURCE_STARTUP finished event");
//          _statusXrayValue.setText(_xRaysReadyMessage);
//          _statusXrayValue.setForeground(_xRaysReadyColor);
        }
      }
      else if(xraySourceEventEnum.equals(xraySourceEventEnum.SHUTDOWN))
      {
        if (event.isStart() == false)
        {
//          System.out.println(" XrayStatusPanel().XRAY_SOURCE_SHUTDOWN finished event");
//          _statusXrayValue.setText(_xRaysNotReadyMessage);
//          _statusXrayValue.setForeground(_xRaysNotReadyColor);
        }
      }
      else if(xraySourceEventEnum.equals(xraySourceEventEnum.ON))
      {
        if (event.isStart() == false)
        {
//          System.out.println(" XrayStatusPanel().XRAY_SOURCE_ON finished event");
//          _statusXrayValue.setText(_xRaysOnMessage);
//          _statusXrayValue.setForeground(_xRaysOnColor);
        }
      }
      else if(xraySourceEventEnum.equals(xraySourceEventEnum.OFF))
      {
        if (event.isStart() == false)
        {
//          System.out.println(" XrayStatusPanel().XRAY_SOURCE_OFF finished event");
//          _statusXrayValue.setText(_xRaysOffMessage);
//          _statusXrayValue.setForeground(_xRaysOffColor);
        }
      }
    }
  }
}

}
