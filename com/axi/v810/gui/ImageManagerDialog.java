package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;
import com.axi.v810.gui.imagePane.ImageSetEventEnum;


/**
 * <p>Title: ImageManagerDialog</p>
 *
 * <p>Description: This panel will be responsible for dislaying detailed image set information for all image Sets that
 * exist in a directory. The panel will first populate it self with image set data found in the available image Set
 * directories specified by the user through the CONFIG gui. The user will then have an option to load other directories.</p>
 *
 * <p>Copyright: Copyright (c) 205</p>
 *
 * <p>Company: AXI</p>
 *
 * @author Erica Wheatcroft
 */
public class ImageManagerDialog extends JDialog
{
  private JPanel _imageSetTablePanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _imageSetTableAndDetailsPanel = new JPanel();
  private JPanel _imageSetDetailsPanel = new JPanel();
  private JPanel _deleteButtonPanel = new JPanel();
  private JPanel _details1Panel = new JPanel();
  private JPanel _details2Panel = new JPanel();
  private JPanel _ImageSetInformationPanel = new JPanel();
  private JPanel _imageSetLoadedStatusPanel = new JPanel();

  private BorderLayout _mainBorderLayout = new BorderLayout();
  private BorderLayout _imageSetTablePanelBorderLayout = new BorderLayout();
  private FlowLayout _deleteButtonPanelFlowLayout = new FlowLayout();
  private FlowLayout _totalLoadedImageSetFlowLayout = new FlowLayout();
  private FlowLayout _buttonPanelFlowLayout = new FlowLayout();
  private GridLayout _imageSetDetailsPanelGridLayout = new GridLayout();
  private GridLayout _imageSetTableAndDetailsPanelGridLayout = new GridLayout();
  private GridLayout __ImageSetInformationPanelGridLayout = new GridLayout();
  private PairLayout _details1PanelPairLayout = new PairLayout();
  private PairLayout _details2PanelPairLayout = new PairLayout();

  private JScrollPane _imageSetTableScrollPane = new JScrollPane();
  private JSortTable _imageSetsDetailsTable = new JSortTable();
  private ImageSetsDetailsTableModel _imageSetsDetailsTableModel = new ImageSetsDetailsTableModel(_imageSetsDetailsTable);
  private JSplitPane _imageSetTableAndDetailsSplitPane = new JSplitPane();
  private JButton _deleteButton = new JButton();
  private JButton _okayButton = new JButton();
  private JLabel _locationLabel = new JLabel();
  private JLabel _imageSetSystemDescription = new JLabel();
  private JLabel _descriptionLabel = new JLabel();
  private JLabel _numberOfImagesLabel = new JLabel();
  private JLabel _numberLabel = new JLabel();
  private JLabel _imageSetSizeLabel = new JLabel();
  private JLabel _sizeLabel = new JLabel();
  private JLabel _imageSetLocationLabel = new JLabel();
  private JLabel _dateCreatedLabel = new JLabel();
  private JLabel _actualPercentageCompatiableWithLocalProjectLabel = new JLabel();
  private JLabel _compatibleWithLocalProjectLabel = new JLabel();
  private JLabel _percentCompatibleLabel = new JLabel();
  private JLabel _actualPercentCompatibleLabel = new JLabel();
  private JLabel _imageSetMachineSerialNumber = new JLabel();
  private JLabel _createdOnMachineLabel = new JLabel();
  private JLabel _imageSetDateCreatedLabel = new JLabel();
  private JLabel _totalImageSetLoadedLabel = new JLabel();

  private static final String _ALL = StringLocalizer.keyToString("ATGUI_ALL_KEY");
  private static final long _MAX_AMOUNT_OF_BYTES_TO_DELETE_WITH_CURSOR = 30000000;

  private ImageManager _imageManager = ImageManager.getInstance();
  protected static WorkerThread _imageSetUpdateThread = new WorkerThread("UpdateImageSetThread");
  
  private BooleanLock _waitForFirstImageSetTobeLoadedLock = new BooleanLock();
  
  private static int _totalSearchedImageSet;
  private boolean _continueToUpdateImageSet = true;

  /**
   * @author Erica Wheatcroft
   */
  public ImageManagerDialog(Frame parent, boolean modal)
  {
    super(parent, modal);
    _continueToUpdateImageSet = true;
  }

  /**
   * This will populate the dialog with all available image sets found in all defined image set root directories. Then it
   * will select the specified image set
   *
   * @author Erica Wheatcroft
   */
  public void populateDialogWithImageSet(ImageSetData imageSetData)
  {
    Assert.expect(imageSetData != null, "Image set to dislpay is null");

    _waitForFirstImageSetTobeLoadedLock.setValue(false);
    java.util.List<ImageSetData> imageSets = new ArrayList<ImageSetData>();
    //immediately add in the image set first
    imageSets.add(imageSetData);
    updateImageSetData(imageSets, imageSetData.getProjectName());
    waitUntilImageSetIsLoaded();
    // first populate the dialog with all the image set roots that are defined.
    populateDialogWithData(Directory.getOfflineXrayImageSetRootDirs());
  }

  /**
   * @param imageSetRootDirectories a list of image set root directories where the dialog will look for image sets in
   * @author Erica Wheatcroft
   */
  public void populateDialogWithData(java.util.List<String> imageSetRootDirectories)
  {
    Assert.expect(imageSetRootDirectories != null, "directory list is empty");

    _totalSearchedImageSet = 0;
    _waitForFirstImageSetTobeLoadedLock.setValue(false);
    java.util.List<ImageSetData> availableImageSets = new ArrayList<ImageSetData>();
    java.util.List<String> projectNameList = FileName.getProjectNames();
    
    // get the available set of image Sets from the image manager
    for(String projectName : projectNameList)
    {
      try
      {
        // add it to the master list.
        _totalSearchedImageSet += _imageManager.getImageSetData(projectName, false).size();
      }
      catch(DatastoreException de)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      de.getLocalizedMessage(),
                                      StringLocalizer.keyToString("IMD_TITLE_KEY"),
                                      true);
      }
    }
    
    //now update the image set compatibility
    for(String projectName : projectNameList)
    {
      try
      {
        availableImageSets = _imageManager.getImageSetData(projectName, false);
      
        updateImageSetData(availableImageSets, projectName);
      }
      catch(DatastoreException de)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      de.getLocalizedMessage(),
                                      StringLocalizer.keyToString("IMD_TITLE_KEY"),
                                      true);
      }
    }
    
    if(_totalSearchedImageSet > 0)
      waitUntilImageSetIsLoaded();
    else
      _waitForFirstImageSetTobeLoadedLock.setValue(true);
    
    _imageSetsDetailsTable.resetSortHeader();

    // now create the gui components
    createPanel();

    // now populate the details panel with the first entry in the table
    if(availableImageSets.size() > 0)
      _imageSetsDetailsTable.setRowSelectionInterval(0,0);
    java.util.List<ImageSetData> selectedImageSets = _imageSetsDetailsTableModel.getSelectedImageSets();
    populateImageSetDetailsPanel(selectedImageSets);

    pack();
  }
  
  /**
   * update the image set compatibility status
   * @param imageSets
   * @param projectName
   * @param totalRecipesSearched
   * @param totalRecipeAvailable 
   */
  private void updateImageSetData(final java.util.List<ImageSetData> imageSets,
    final String projectName)
  {
    
    _imageSetUpdateThread.invokeLater(new Runnable()
    {
      public void run()
      {
        for (ImageSetData imageSetData : imageSets)
        {
          if(_continueToUpdateImageSet == false)
            break;
          try
          {
            //calculate the compatibility percentage of every image set
            imageSetData.setPercentOfCompatibleImages(_imageManager.calculateImageDataCompatibility(imageSetData, projectName));
            //set the image set loaded status to true
            imageSetData.setImageSetDataLoadStatus(true);
            //allow the population of tablemodel to be continue
            if(_waitForFirstImageSetTobeLoadedLock.isFalse())
              _waitForFirstImageSetTobeLoadedLock.setValue(true);
            
            _imageSetsDetailsTableModel.insertImageSetListData(imageSetData);
            
            _totalImageSetLoadedLabel.setText(_imageSetsDetailsTableModel.getRowCount()+" / "+_totalSearchedImageSet +" of Image Set Searched");
            
            //sleep 10 ms to allow EDT Update UI
            Thread.sleep(10);
          }
          catch (final DatastoreException de)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  de.getLocalizedMessage(),
                  StringLocalizer.keyToString("IMD_TITLE_KEY"),
                  true);
              }
            });
          }
          catch (InterruptedException ex)
          {
            //do nothing
          }
        }
      }
    });
  }
  

  /**
   * @author Erica Wheatcroft
   */
  private void populateImageSetDetailsPanel(java.util.List<ImageSetData> imageSets)
  {
    Assert.expect(imageSets != null, "image sets are null");

    String nAString = StringLocalizer.keyToString("IMD_NOT_APPLICABLE_KEY");

    if(imageSets.size() == 1)
    {
      ImageSetData selectedImageSet = imageSets.get(0);

      _imageSetSystemDescription.setText(selectedImageSet.getSystemDescription());
      int numImages = selectedImageSet.getNumberOfImagesSaved();
      _numberOfImagesLabel.setText(((Integer)selectedImageSet.getNumberOfImagesSaved()).toString());

      _imageSetLocationLabel.setText(Directory.getXrayImagesDir(selectedImageSet.getProjectName()));
      long sizeOfSelectedImageSetsInBytes = selectedImageSet.getEstimatedSizeInBytes(false);
      double megaBytes = MathUtil.convertBytesToMegabytes((int)sizeOfSelectedImageSetsInBytes);
      _imageSetSizeLabel.setText(MathUtil.roundToPlaces(megaBytes, 1) + " " + StringLocalizer.keyToString("ZIP_MB_UNITS_KEY"));

      _imageSetMachineSerialNumber.setText(selectedImageSet.getMachineSerialNumber());

       long dateInMils = selectedImageSet.getDateInMils();
       Date date = new Date(dateInMils);
       DateFormat dateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
       _imageSetDateCreatedLabel.setText(dateFormat.format(date));

       // now lets figure out if there exists on disk a project that is compatible with this project.
       String projectName = selectedImageSet.getProjectName();
       if(Project.doesProjectExistLocally(projectName))
       {
         _actualPercentageCompatiableWithLocalProjectLabel.setText( projectName );
         double actualPercentage = selectedImageSet.getPercentOfCompatibleImages();
         if( ((int) actualPercentage == 99) || ((int) actualPercentage == 0))
         {
           // so we don't want to round if the % is 99 or 0
           int temp = (int) (actualPercentage * Math.pow(10, 2));
           _actualPercentCompatibleLabel.setText((double)temp / 100 + "%");
         }
         else
           _actualPercentCompatibleLabel.setText(Integer.toString((int)MathUtil.roundToPlaces(actualPercentage, 0)) + "%");
       }
       else
       {
         // the project does not exist locally notify the user.
         _actualPercentageCompatiableWithLocalProjectLabel.setText(  StringLocalizer.keyToString("IMD_NO_PROJECT_FOUND_KEY") );
         _actualPercentageCompatiableWithLocalProjectLabel.setText( nAString );
       }

    }
    else
    {

      // the user has selected more than one or the table is empty. change the way this panel will look
      _imageSetSystemDescription.setText(nAString);
      _numberOfImagesLabel.setText(nAString);
      _imageSetLocationLabel.setText(nAString);
      _imageSetSizeLabel.setText(nAString);
      _actualPercentCompatibleLabel.setText(nAString);
      _actualPercentageCompatiableWithLocalProjectLabel.setText(nAString);
      _imageSetMachineSerialNumber.setText(nAString);
      _imageSetDateCreatedLabel.setText(nAString);

    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setLayout(_mainBorderLayout);
    setTitle(StringLocalizer.keyToString("IMD_TITLE_KEY"));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    _imageSetTablePanel.setLayout(_imageSetTablePanelBorderLayout);
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    _imageSetTableAndDetailsSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
    _imageSetTableAndDetailsPanel.setLayout(_imageSetTableAndDetailsPanelGridLayout);
    _imageSetTablePanel.setBorder(null);
    _deleteButtonPanel.setLayout(_deleteButtonPanelFlowLayout);
    _deleteButtonPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _imageSetLoadedStatusPanel.setLayout(_totalLoadedImageSetFlowLayout);
    _totalLoadedImageSetFlowLayout.setAlignment(FlowLayout.LEFT);
    
    _imageSetDetailsPanel.setLayout(_imageSetDetailsPanelGridLayout);
    //added by sheng chuan
    _ImageSetInformationPanel.setLayout(__ImageSetInformationPanelGridLayout);
    
    _imageSetDetailsPanelGridLayout.setColumns(2);
    _deleteButton.setText(StringLocalizer.keyToString("IMD_DELETE_IMAGE_SET_KEY"));
    _deleteButton.setEnabled(false);
    _okayButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _buttonPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _locationLabel.setFont(new java.awt.Font("SansSerif", Font.BOLD, 12));
    _locationLabel.setText(StringLocalizer.keyToString("IMD_LOCATION_KEY"));
    _descriptionLabel.setFont(new java.awt.Font("SansSerif", Font.BOLD, 12));
    _descriptionLabel.setText(StringLocalizer.keyToString("IMD_DESCRIPTION_KEY"));
    _numberLabel.setFont(new java.awt.Font("SansSerif", Font.BOLD, 12));
    _numberLabel.setText(StringLocalizer.keyToString("IMD_NUMBER_OF_IMAGES_KEY"));
    _sizeLabel.setFont(new java.awt.Font("SansSerif", Font.BOLD, 12));
    _sizeLabel.setText(StringLocalizer.keyToString("IMD_SIZE_KEY"));
    _dateCreatedLabel.setFont(new java.awt.Font("SansSerif", Font.BOLD, 12));
    _dateCreatedLabel.setText(StringLocalizer.keyToString("IMD_DATE_CREATED_KEY"));
    _compatibleWithLocalProjectLabel.setFont(new java.awt.Font("SansSerif", Font.BOLD, 12));
    _compatibleWithLocalProjectLabel.setText(StringLocalizer.keyToString("IMD_COMPATIBLE_WITH_PROJECT_KEY"));
    _percentCompatibleLabel.setFont(new java.awt.Font("SansSerif", Font.BOLD, 12));
    _percentCompatibleLabel.setText(StringLocalizer.keyToString("IMD_COMPATIBLE_WITH_PROJECT_PERCENT_KEY"));
    _createdOnMachineLabel.setFont(new java.awt.Font("SansSerif", Font.BOLD, 12));
    _createdOnMachineLabel.setText(StringLocalizer.keyToString("IMD_MACHINE_CREATED_KEY"));
    _imageSetDetailsPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
                                                                                       new Color(148, 145, 140)), StringLocalizer.keyToString("IMD_BORDER_TITLE_KEY")),
                                                                                       BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    _imageSetTableAndDetailsPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
    _imageSetTableAndDetailsSplitPane.add(_imageSetTablePanel, JSplitPane.TOP);
    _imageSetsDetailsTable.setModel(_imageSetsDetailsTableModel);
    _imageSetsDetailsTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    getContentPane().add(_imageSetTableAndDetailsPanel, BorderLayout.CENTER);
    _imageSetTableAndDetailsPanel.add(_imageSetTableAndDetailsSplitPane);
    getContentPane().add(_buttonPanel, BorderLayout.SOUTH);
    _buttonPanel.add(_okayButton);
    _imageSetTablePanel.add(_imageSetTableScrollPane, BorderLayout.CENTER);
    _imageSetTablePanel.add(_ImageSetInformationPanel, BorderLayout.SOUTH);
    
    _ImageSetInformationPanel.add(_imageSetLoadedStatusPanel);
    _ImageSetInformationPanel.add(_deleteButtonPanel);
    _imageSetLoadedStatusPanel.add(_totalImageSetLoadedLabel);
    _deleteButtonPanel.add(_deleteButton);
    _imageSetTableAndDetailsSplitPane.add(_imageSetDetailsPanel, JSplitPane.BOTTOM);
    _details1Panel.setLayout(_details1PanelPairLayout);
    _details2Panel.setLayout(_details2PanelPairLayout);
    _imageSetDetailsPanel.add(_details1Panel);

    _details1Panel.add(_locationLabel);
    _details1Panel.add(_imageSetLocationLabel);
    _details1Panel.add(_sizeLabel);
    _details1Panel.add(_imageSetSizeLabel);
    _details1Panel.add(_numberLabel);
    _details1Panel.add(_numberOfImagesLabel);
    _details1Panel.add(_descriptionLabel);
    _details1Panel.add(_imageSetSystemDescription);

    _details2Panel.add(_dateCreatedLabel);
    _details2Panel.add(_imageSetDateCreatedLabel);
    _details2Panel.add(_createdOnMachineLabel);
    _details2Panel.add(_imageSetMachineSerialNumber);
    _details2Panel.add(_compatibleWithLocalProjectLabel);
    _details2Panel.add(_actualPercentageCompatiableWithLocalProjectLabel);
    _details2Panel.add(_percentCompatibleLabel);
    _details2Panel.add(_actualPercentCompatibleLabel);

    _imageSetDetailsPanel.add(_details2Panel);
    _imageSetTableScrollPane.getViewport().add(_imageSetsDetailsTable);
    _imageSetTableAndDetailsSplitPane.setResizeWeight(1);

    setUpTable();

    _deleteButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteSelectedImageSets();
      //  setUpTable();
      }
    });

    ListSelectionModel imageSetsDetailsTableTablerowSelectionModel = _imageSetsDetailsTable.getSelectionModel();
    imageSetsDetailsTableTablerowSelectionModel.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if(lsm.isSelectionEmpty() == false && lsm.getValueIsAdjusting() == false)
        {
          _deleteButton.setEnabled(true);
          java.util.List<ImageSetData> selectedImageSets = _imageSetsDetailsTableModel.getSelectedImageSets();
          populateImageSetDetailsPanel(selectedImageSets);
        }
      }
    });

    _imageSetsDetailsTable.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent evt)
      {
        if (evt.isPopupTrigger() == false)
        {
          java.util.List<ImageSetData> selectedImageSets = _imageSetsDetailsTableModel.getSelectedImageSets();
          populateImageSetDetailsPanel(selectedImageSets);
        }
      }
    });

    _okayButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });

  }

  /**
   * This method will delete all selected image Sets from disk.
   * @author Erica Wheatcroft
   */
  private void deleteSelectedImageSets()
  {
    String message = StringLocalizer.keyToString("IMD_CONFIRM_IMAGE_SET_DELETION_KEY");
    int response = JOptionPane.showConfirmDialog(this,
                                                 message,
                                                 StringLocalizer.keyToString("IMD_TITLE_KEY"),
                                                 JOptionPane.YES_NO_OPTION);
    if (response == JOptionPane.YES_OPTION)
    {
      // okay the user wants to delete the selected image sets
      final java.util.List<ImageSetData> selectedImageSetsToDelete = _imageSetsDetailsTableModel.getSelectedImageSets();

      // figure out how big the selected image sets are on disk. this will determine if we use a cursor or a modal
      // dialog.
      long sizeOfSelectedImageSets = getSizeOfSelectedImageSets(selectedImageSetsToDelete);
      boolean selectedImageSetSizeIsLargerThanMax = false;

      if(sizeOfSelectedImageSets >= _MAX_AMOUNT_OF_BYTES_TO_DELETE_WITH_CURSOR)
        selectedImageSetSizeIsLargerThanMax = true;

      final boolean useBusyDialog = selectedImageSetSizeIsLargerThanMax;

      // notify any one who cares that we have deleted image sets.
      GuiObservable.getInstance().stateChanged(selectedImageSetsToDelete, ImageSetEventEnum.IMAGE_SET_DELETED);

      setCursor(new Cursor(Cursor.WAIT_CURSOR));

      enableGui(false);

      final BusyCancelDialog dialog = new BusyCancelDialog(this,
                                      StringLocalizer.keyToString("IMD_DELETING_IMAGE_SETS_KEY"),
                                      StringLocalizer.keyToString("IMD_TITLE_KEY"),
                                      true);
      dialog.pack();
      SwingUtils.centerOnComponent(dialog, this);
      dialog.setCursor(new Cursor(Cursor.WAIT_CURSOR));

      SwingWorkerThread.getInstance().invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            for (final ImageSetData imageSet : selectedImageSetsToDelete)
            {
              _imageManager.deleteImages(imageSet);
              --_totalSearchedImageSet;
            }
          }
          catch (final DatastoreException de)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                              de.getLocalizedMessage(),
                                              StringLocalizer.keyToString("IMD_TITLE_KEY"),
                                              true);
              }
            });
          }
          finally
          {
            if (useBusyDialog)
            {
              // wait until visible
              while (dialog.isVisible() == false)
              {
                try
                {
                  Thread.sleep(200);
                }
                catch (InterruptedException ex)
                {
                  // do nothing
                }

              }

              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  dialog.dispose();
                }
              });
            }
          }

          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              // okay we are finished lets set the default cursor back
              // select the first on if applicable.

              // lets first remove all the image sets from the table model
              _imageSetsDetailsTableModel.removeImageSets(selectedImageSetsToDelete);
              _totalImageSetLoadedLabel.setText(_imageSetsDetailsTableModel.getRowCount()+" / "+_totalSearchedImageSet +" of Image Set Searched");
              enableGui(true);
              setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
              if (_imageSetsDetailsTableModel.getRowCount() > 0)
                _imageSetsDetailsTable.setRowSelectionInterval(0, 0);
              else
              {
                // if not make sure that the delete button is not enabled
                _deleteButton.setEnabled(false);
              }
            }
          });
        }
      });

      if(useBusyDialog)
        dialog.setVisible(true);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void setUpTable()
  {
    // first set up the column renderers on the columns centered.

    TableColumn versionCol = _imageSetsDetailsTable.getColumnModel().getColumn(1);
    DefaultTableCellRenderer versionColumnCenterRenderer = new DefaultTableCellRenderer();
    versionColumnCenterRenderer.setHorizontalAlignment(JLabel.CENTER);
    versionCol.setCellRenderer(versionColumnCenterRenderer);

    TableColumn typeCol = _imageSetsDetailsTable.getColumnModel().getColumn(4);
    DefaultTableCellRenderer typeColumnCenterRenderer = new DefaultTableCellRenderer();
    typeColumnCenterRenderer.setHorizontalAlignment(JLabel.CENTER);
    typeCol.setCellRenderer(versionColumnCenterRenderer);

    TableColumn populatedBoardCol = _imageSetsDetailsTable.getColumnModel().getColumn(5);
    DefaultTableCellRenderer populatedBoardColCenterRenderer = new DefaultTableCellRenderer();
    populatedBoardColCenterRenderer.setHorizontalAlignment(JLabel.CENTER);
    populatedBoardCol.setCellRenderer(populatedBoardColCenterRenderer);


    // now adjust the column sizes of the tables to suit their contents
    ColumnResizer.adjustColumnPreferredWidths(_imageSetsDetailsTable, true);

    // set up the scroll pane and the table size so we don't have a bunch of blank space if not needed.
    int origTableWidth = _imageSetsDetailsTable.getMinimumSize().width;
    int padding = 175;
    int scrollHeight = _imageSetTableScrollPane.getPreferredSize().height;
    
    Dimension prefScrollSize = new Dimension(origTableWidth + padding, scrollHeight);
    _imageSetsDetailsTable.setPreferredScrollableViewportSize(prefScrollSize);

    _imageSetTableAndDetailsSplitPane.resetToPreferredSizes();

  }

  /**
   * this method will disable or enable the gui.
   * @author Erica Wheatcroft
   */
  private void enableGui(boolean enable)
  {
    setEnabled(enable);
    _imageSetsDetailsTable.setEnabled(enable);
    _deleteButton.setEnabled(enable);
    _okayButton.setEnabled(enable);
  }

  /**
   * @author Erica Wheatcroft
   */
  private int getSizeOfSelectedImageSets(java.util.List<ImageSetData> imageSets)
  {
    int size = 0;

    if(imageSets == null)
      return size;

    if(imageSets.size() == 0)
      return size;

    for(ImageSetData imageSet : imageSets)
    {
//      String imageSetDirectory = Directory.getXrayInspectionImagesDir(imageSet.getProjectName(), imageSet.getImageSetName());
//      size += FileUtil.getSizeOfDirectoryContentsInBytes(imageSetDirectory);
      size += imageSet.getEstimatedSizeInBytes(false);
    }

    return size;
  }
  
  /**
   * sheng chuan to stop the thread upon dispose
   */
  public void dispose()
  {
    _continueToUpdateImageSet = false;
    super.dispose();
  }
  
  /**
   * wait until at least 1 image set is fully loaded
   */
  public void waitUntilImageSetIsLoaded()
  {
    try
    {
      _waitForFirstImageSetTobeLoadedLock.waitUntilTrue();
    }
    catch (InterruptedException ex)
    {
      //do nothing
    }
  }
}
