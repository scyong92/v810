package com.axi.v810.gui.algoTuner;

import com.axi.v810.business.panelDesc.ComponentType;
import com.axi.util.*;

/**
 * This class tracks the state of the current tuner test selection
 * Either a test is in joint type/subtype mode or component/pin mode
 * @author Andy Mechtenberg
 */
public class TunerTestState
{
  private boolean _jointTypeTest;
  private JointTypeChoice _jointTypeChoice;
  private SubtypeChoice _subtypeChoice;
  private ComponentType _componentType;
  private PadChoice _padChoice;

  /**
   * @author Andy Mechtenberg
   */
  TunerTestState(JointTypeChoice jointTypeChoice, SubtypeChoice subtypeChoice)
  {
    Assert.expect(jointTypeChoice != null);
    Assert.expect(subtypeChoice != null);
    _jointTypeTest = true;
    _jointTypeChoice = jointTypeChoice;
    _subtypeChoice = subtypeChoice;
  }

  /**
   * @author Andy Mechtenberg
   */
  TunerTestState(ComponentType componentType, PadChoice padChoice, JointTypeChoice jointTypeChoice, SubtypeChoice subtypeChoice)
  {
    Assert.expect(componentType != null);
    Assert.expect(padChoice != null);
    Assert.expect(jointTypeChoice != null);
    Assert.expect(subtypeChoice != null);

    _jointTypeTest = false;
    _componentType = componentType;
    _padChoice = padChoice;

    // also set the joint type/subtype here, as most component pins map to a certain on.  Mixed ones don't, so the ALL choice will be set
    _jointTypeChoice = jointTypeChoice;
    _subtypeChoice = subtypeChoice;
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean isJointTypeTest()
  {
    return _jointTypeTest;
  }

  /**
   * @author Andy Mechtenberg
   */
  JointTypeChoice getJointTypeChoice()
  {
    Assert.expect(_jointTypeChoice != null);
    return _jointTypeChoice;
  }

  /**
   * @author Andy Mechtenberg
   */
  SubtypeChoice getSubtypeChoice()
  {
    Assert.expect(_subtypeChoice != null);
    return _subtypeChoice;
  }

  /**
   * @author Andy Mechtenberg
   */
  ComponentType getComponentType()
  {
    Assert.expect(_jointTypeTest == false);
    Assert.expect(_componentType != null);
    return _componentType;
  }

  /**
   * @author Andy Mechtenberg
   */
  PadChoice getPadChoice()
  {
    Assert.expect(_jointTypeTest == false);
    Assert.expect(_padChoice != null);
    return _padChoice;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    String desc;
    if (_jointTypeTest)
      desc = "Joint Type/Subtype";
    else
      desc = "Component/pin";

    desc += " Joint Type: " + _jointTypeChoice + " Subtype: " + _subtypeChoice + " Component: " + _componentType + " pin: " + _padChoice;
    return desc;
  }
}
