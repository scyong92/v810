package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.awt.event.*;
import java.util.List;

import javax.swing.*;

import com.axi.util.*;

import com.axi.v810.business.imageAcquisition.ImageAcquisitionEngine;
import com.axi.v810.business.testExec.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
* This class contains all the GUI elements for the dialog box that allows the user to change test options such as
* Loop count, loop forever/finite setting.  Used in conjunction with the TestOptions class which holds the data.
*
* @author Andy Mechtenberg
*/
public class ImageGenerationOptionsDlg extends JDialog
{
  private XrayTester _xRayTester;
  private ImageAcquisitionModeEnum _defaultMode;
  private ImageAcquisitionModeEnum _newMode;
  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private JButton _cancelButton = new JButton();
  private JButton _OkButton = new JButton();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _choicesPanel = new JPanel();
//  private JRadioButton _alwaysUseHardware = new JRadioButton();
//  private JRadioButton _useImageSets = new JRadioButton();
//  private JRadioButton _useHardwareOnlyWhenNecessary = new JRadioButton();
  private Box _generationChoiceBox;
  private ButtonGroup _buttonGroup;

  private JPanel _buttonInnerPanel = new JPanel();
  private GridLayout _buttonInnerGridLayout = new GridLayout();

  /**
  * The constructor for this modal dialog needs a title and parent.
  *
  * @author Andy Mechtenberg
  * @param frame The parent frame for this dialog
  * @param title The string containing the title of the dialog
  * @param modal A boolean specifying the dialogs modality.  Should be TRUE for this dialog.
  */
  public ImageGenerationOptionsDlg(Frame frame, String title, boolean modal, ImageAcquisitionModeEnum defaultMode)
  {
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(defaultMode != null);
    _xRayTester = XrayTester.getInstance();
    _defaultMode = defaultMode;
    _newMode = defaultMode;
    try
    {
      jbInit();
      pack();
    }
    catch(RuntimeException ex)
    {
      Assert.logException(ex);
      ex.printStackTrace();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    _generationChoiceBox = Box.createVerticalBox();
    _mainPanel.setLayout(_mainPanelBorderLayout);

    _OkButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _OkButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed();
      }
    });
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed();
      }
    });
    _buttonGroup = new ButtonGroup();

//    _alwaysUseHardware.setText(StringLocalizer.keyToString("ATGUI_IMAGE_GENERATION_ALWAYS_USE_HARDWARE_KEY"));
//    _useImageSets.setText(StringLocalizer.keyToString("ATGUI_IMAGE_GENERATION_ALWAYS_USE_IMAGE_SETS_KEY"));
//    _useHardwareOnlyWhenNecessary.setText(StringLocalizer.keyToString("ATGUI_IMAGE_GENERATION_USE_HARDWARE_WHEN_NECESSARY_KEY"));

    _buttonInnerPanel.setLayout(_buttonInnerGridLayout);
    _buttonInnerGridLayout.setHgap(10);
    getContentPane().add(_mainPanel);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    _buttonPanel.add(_buttonInnerPanel);
    _buttonInnerPanel.add(_OkButton);
    _buttonInnerPanel.add(_cancelButton);
    _mainPanel.add(_choicesPanel, BorderLayout.NORTH);
    _choicesPanel.add(_generationChoiceBox, null);

    List<ImageAcquisitionModeEnum> supportedModes = ImageAcquisitionModeEnum.getAllImageAcquisitionModes();
    for(ImageAcquisitionModeEnum mode : supportedModes)
    {
      JRadioButton radioButton = new JRadioButton(mode.getName());
      if (_xRayTester.isSimulationModeOn() &&
          ImageAcquisitionEngine.getInstance().isOfflineReconstructionEnabled() &&
          (mode.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS) ||
           mode.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS)))
      {
        radioButton.setEnabled(true);
      }

      _generationChoiceBox.add(radioButton);
      _buttonGroup.add(radioButton);
      if (mode.equals(_defaultMode))
        radioButton.setSelected(true);
      radioButton.addActionListener(new java.awt.event.ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          radioButton_actionPerformed(e);
        }
      });
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void radioButton_actionPerformed(ActionEvent e)
  {
    JRadioButton radioButton = (JRadioButton)e.getSource();
    String modeName = radioButton.getText();
    _newMode = ImageAcquisitionModeEnum.getImageAcquistionMode(modeName);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void okButton_actionPerformed()
  {
    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void cancelButton_actionPerformed()
  {
    _newMode = _defaultMode;
    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  ImageAcquisitionModeEnum getSelectedMode()
  {
    return _newMode;
  }
}

