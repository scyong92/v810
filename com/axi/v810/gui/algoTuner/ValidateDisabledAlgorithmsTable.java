/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.algoTuner;

import com.axi.guiUtil.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import com.axi.util.*;

/**
 *
 * @author weng-jian.eoh
 */
public class ValidateDisabledAlgorithmsTable extends JTable
{
  public static final int _NUMBER_VALUE_INDEX = 0;
  public static final int _JOINTTYPE_VALUE_INDEX = 1;
  public static final int _SUBTYPE_VALUE_INDEX = 2;
  public static final int _DISABLED_ALGO_VALUE_INDEX = 3;
  
  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _NUMBER_VALUE_COLUMN_WIDTH_PERCENTAGE = .01;
  private static final double _JOINTTYPE_VALUE_COLUMN_WIDTH_PERCENTAGE = .19;
  private static final double _SUBTYPE_VALUE_COLUMN_WIDTH_PERCENTAGE = .50;
  private static final double _DISABLED_ALGO_VALUE_COLUMN_WIDTH_PERCENTAGE = .30;
  
  /**
  *
  * @author weng-jian.eoh
  */
  public ValidateDisabledAlgorithmsTable(TableModel tableModel)
  {
    super(tableModel);
    getTableHeader().setReorderingAllowed(false);
    setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    setPreferredColumnWidths();
  }
  
  /**
   * @author weng-jian.eoh
   * @param cellRender 
   */
  public void renderColumns(TableCellRenderer cellRender)
  {
    for (int i = 0; i < this.getModel().getColumnCount(); i++)
    {
      renderColumn(this.getColumnModel().getColumn(i), cellRender);
    }
  }
  
  /**
   * @author weng-jian.eoh
   * @param col
   * @param cellRender 
   */
  public void renderColumn(TableColumn col, TableCellRenderer cellRender)
  {
    try
    {
      col.setCellRenderer(cellRender);
    }
    catch(Exception e)
    {
      Assert.expect(false,e.getMessage());
    }
  }
  
  /**
   * @author weng-jian.eoh
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();
    
    columnModel.getColumn(_NUMBER_VALUE_INDEX).setPreferredWidth((int)(totalColumnWidth * _NUMBER_VALUE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_JOINTTYPE_VALUE_INDEX).setPreferredWidth((int)(totalColumnWidth * _JOINTTYPE_VALUE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_SUBTYPE_VALUE_INDEX).setPreferredWidth((int)(totalColumnWidth * _SUBTYPE_VALUE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_DISABLED_ALGO_VALUE_INDEX).setPreferredWidth((int)(totalColumnWidth * _DISABLED_ALGO_VALUE_COLUMN_WIDTH_PERCENTAGE));    
  }
  
  /**
   * @author weng-jian.eoh
   * @param e
   * @return 
   */
  public String getToolTipText(MouseEvent e)
  {
    Point p = e.getPoint();
    int row = rowAtPoint(p);
    int col = columnAtPoint(p);

    if (row >= 0 && col >= 0)
    {
      return getValueAt(row, col).toString();
    }
    else
    {
      return null;
    }
  }
}
