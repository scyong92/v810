package com.axi.v810.gui.algoTuner;

import com.axi.util.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;
import com.axi.v810.business.testResults.MeasurementUnitsEnum;

/**
 *
 * <p>Title: ReviewDefectsReportEntry</p>
 *
 * <p>Description:  This class holds data for the ReviewDefectsReportTableModel.  It is a combination
 * of joint information and defect details and is a flattened data structure for ease of use.
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class ReviewDefectsReportEntry
{
 private Long _runId;
 private String _boardSerialNumber;
 private String _boardName;
 private String _referenceDesignator;
 private String _pinName;
 private String _jointType;
 private String _subtype;
 private String _side;
 private Integer _numDefects;
 private String _indictmentName;
 private String _indictmentType;
 private String _measurementName;
 private String _measurementValue;
 private String _measurementUnits;
 private String _sliceName;

 private final String _TOP_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_TOP_LABEL_KEY");
 private final String _BOTTOM_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_BOTTOM_LABEL_KEY");

  /**
   * @author George Booth
   */
  public ReviewDefectsReportEntry(JointResultsEntry jointResultsEntry)
  {
    Assert.expect(jointResultsEntry != null);

    _runId = jointResultsEntry.getRunStartTime();
    _boardSerialNumber = jointResultsEntry.getBoardSerialNumber();
    _boardName = jointResultsEntry.getBoardName();
    _referenceDesignator = jointResultsEntry.getReferenceDesignator();
    _pinName = jointResultsEntry.getPadName();
    _jointType = jointResultsEntry.getJointTypeName();
    _subtype = jointResultsEntry.getSubtypeName();
    if (jointResultsEntry.isTopSide())
      _side = _TOP_SIDE;
    else
      _side = _BOTTOM_SIDE;
    _numDefects = jointResultsEntry.getNumberOfDefects();
  }

  /**
   * @author George Booth
   */
  public void setIndictmentDetails(IndictmentDetailsEntry indictmentDetailsEntry)
  {
    Assert.expect(indictmentDetailsEntry != null);

    _indictmentType = indictmentDetailsEntry.getIndictmentType();
    if (_indictmentType.equals(StringLocalizer.keyToString("RDGUI_FAILED_LABEL_KEY")))
    {
      _indictmentName = indictmentDetailsEntry.getIndictment().getName();
    }
    else
    {
      _indictmentName = " ";
    }

    MeasurementResult measurementResult = indictmentDetailsEntry.getMeasurement();
    _measurementName = measurementResult.getMeasurementEnum().getName();

    // format measurement value as needed
    Object value = measurementResult.getValue();
    MeasurementUnitsEnum measurementUnitsEnum = measurementResult.getMeasurementUnitsEnum();

    Pair<MeasurementUnitsEnum, Object> convertedValue = MeasurementUnitsEnum.convertToProjectUnitsIfNecessary(measurementUnitsEnum, value);
    measurementUnitsEnum = convertedValue.getFirst();
    value = convertedValue.getSecond();
    // truncate to a reasonable number of decimal points.
    _measurementValue = MeasurementUnitsEnum.formatNumberIfNecessary(measurementUnitsEnum, value);

    // the units of the measurment
    _measurementUnits = measurementUnitsEnum.getName();

    _sliceName = measurementResult.getSliceNameEnum().getName();
  }

  /**
   * @author George Booth
   */
  public Long getRunId()
  {
    return _runId;
  }
  
 /**
   * @author Jack Hwee
   */
  public String getBoardSerialNumber()
  {
    return _boardSerialNumber;
  }

  /**
   * @author George Booth
   */
  public String getBoardName()
  {
    return _boardName;
  }

  /**
   * @author George Booth
   */
  public String getReferenceDesignator()
  {
    return _referenceDesignator;
  }

  /**
   * @author George Booth
   */
  public String getPinName()
  {
    return _pinName;
  }

  /**
   * @author George Booth
   */
  public String getJointType()
  {
    return _jointType;
  }

  /**
   * @author George Booth
   */
  public String getSubtype()
  {
    return _subtype;
  }

  /**
   * @author George Booth
   */
  public String getSide()
  {
    return _side;
  }

  /**
   * @author George Booth
   */
  public Integer getNumDefects()
  {
    return _numDefects;
  }

  /**
   * @author George Booth
   */
  public String getIndictmentName()
  {
    return _indictmentName;
  }

  /**
   * @author George Booth
   */
  public String getIndictmentType()
  {
    return _indictmentType;
  }

  /**
   * @author George Booth
   */
  public String getMeasurementName()
  {
    return _measurementName;
  }

  /**
   * @author George Booth
   */
  public String getMeasurementValue()
  {
    return _measurementValue;
  }

  /**
   * @author George Booth
   */
  public String getMeasurementUnits()
  {
    return _measurementUnits;
  }

  /**
   * @author George Booth
   */
  public String getSliceName()
  {
    return _sliceName;
  }

}
