package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * This is the gui class for all algorithm configuration
 * @author Andy Mechtenberg
 */
public class AlgorithmConfigurationDlg extends JDialog
{
//  private static final int _MAXIMUM_COMBOBOX_ROWCOUNT = 20;  // the maximum size for the combo boxes in test selection

  private final String _DIAGNOSTICS_EMPTY_MESSAGE = StringLocalizer.keyToString("ATGUI_NO_DIAGNOSTICS_KEY");
  private final String _ONOFF_EMPTY_MESSAGE = StringLocalizer.keyToString("ATGUI_NO_DISABLED_KEY");

  // we don't want to localize these because they MUST match the actual algorithm names in the IAS
  private final Collection<AlgorithmEnum> _commonAlgorithms = InspectionFamily.getAlgorithmEnumsSharedByAllInspectionFamilies();

  private AlgorithmConfiguration _algorithmConfiguration;

  private DefaultListModel _diagnosticsListModel;
  private DefaultListModel _onOffListModel;

  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private JPanel _jointTypeAlgorithmSelectPanel = new JPanel();
  private JPanel _okCancelPanel = new JPanel();
  private GridLayout _jointTypeAlgorithmSelectPanelGridLayout = new GridLayout();
  private JPanel _jointTypeSelectPanel = new JPanel();
  private JPanel _algorithmSelectPanel = new JPanel();
  private FlowLayout _jointTypeSelectPanelFlowLayout = new FlowLayout();
  private FlowLayout _algorithmSelectPanelFlowLayout = new FlowLayout();
  private JComboBox _jointTypeComboBox = new JComboBox();
  private JLabel _jointTypeLabel = new JLabel();
  private JComboBox _algorithmComboBox = new JComboBox();
  private JLabel _algorithmLabel = new JLabel();
  private FlowLayout _okCancelPanelFlowLayout = new FlowLayout();
  private JButton _cancelButton = new JButton();
  private JButton _okButton = new JButton();
  private JTabbedPane _configurationsTabbedPane = new JTabbedPane();
  private JPanel _diagnosticsPanel = new JPanel();
  private JPanel _onOffPanel = new JPanel();
  private JPanel _diagnosticsOnPanel = new JPanel();
  private JPanel _addRemoveDiagnosticsButtonPanel = new JPanel();
  private JPanel _diagnosticsDefinitionPanel = new JPanel();
  private BorderLayout _diagnosticsDefinitionPanelBorderLayout = new BorderLayout();
  private GridLayout _addRemoveDiagnosticsButtonPanelGridLayout = new GridLayout();
  private JButton _addDiagnosticsButton = new JButton();
  private JButton _removeAllDiagnosticsButton = new JButton();
  private JButton _removeDiagnosticsButton = new JButton();
  private Border _etchedBorder2;
  private TitledBorder _diagnosticsOnTitledBorder;
  private BorderLayout _diagnosticsOnPanelBorderLayout = new BorderLayout();
  private JScrollPane _diagnosticsOnScrollPane = new JScrollPane();
  private JList _diagnosticsList = new JList();
  private BorderLayout _onOffOnPanelBorderLayout = new BorderLayout();
  private JList _onOffList = new JList();
  private JScrollPane _onOffOnScrollPane = new JScrollPane();
  private JPanel _onOffOnPanel = new JPanel();
  private GridLayout _addRemoveOnOffPanelGridLayout = new GridLayout();
  private JButton _addOnOffButton = new JButton();
  private JButton _removeAllOnOffButton = new JButton();
  private JButton _removeOnOffButton = new JButton();
  private JPanel _addRemoveOnOffButtonPanel = new JPanel();
  private JPanel _disabledExplanationPanel = new JPanel();
  private TitledBorder _onOffTitledBorder;

  private BoardType _boardType = null;
  private PanelDataAdapter _panelDataAdapter = null;
  private JFrame _frame = null;
  private JTextArea _disabledExplanationTextArea = new JTextArea();
  private Border _disabledExplanationPanelBorder;
  private BorderLayout _disabledExplanationBorderLayout = new BorderLayout();
  private JTextArea _diagnosticsExplanationTextArea = new JTextArea();
  private JPanel _okCancelInnerPanel = new JPanel();
  private GridLayout _okCancelInnerGridLayout = new GridLayout();

  /**
   * @author Andy Mechtenberg
   */
  AlgorithmConfigurationDlg(JFrame frame, String title, boolean modal, BoardType boardType, PanelDataAdapter panelDataAdapter)
  {
    // must do this first -- compiler requires it.
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(boardType != null);
    Assert.expect(panelDataAdapter != null);

    _boardType = boardType;
    _panelDataAdapter = panelDataAdapter;
    _frame = frame;

    // Assume the persistance file exists, so read it in.  We will save it on OK,
    // and will not save it on CANCEL
    _algorithmConfiguration = AlgorithmConfiguration.readSettingsPersistanceFile();
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit() throws Exception
  {
    _diagnosticsListModel = new DefaultListModel();
    _diagnosticsList = new JList(_diagnosticsListModel);
    _onOffListModel = new DefaultListModel();
    _onOffList = new JList(_onOffListModel);

    _etchedBorder2 = BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134));
    _diagnosticsOnTitledBorder = new TitledBorder(_etchedBorder2,StringLocalizer.keyToString("ATGUI_DIAGNOSTICS_ON_KEY"));

    _onOffTitledBorder = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),StringLocalizer.keyToString("ATGUI_DISABLED_ON_KEY"));
    _disabledExplanationPanelBorder = BorderFactory.createEmptyBorder(10,0,0,10);
    _onOffTitledBorder.setTitleColor(Color.red);
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _jointTypeAlgorithmSelectPanel.setLayout(_jointTypeAlgorithmSelectPanelGridLayout);
    _jointTypeAlgorithmSelectPanelGridLayout.setRows(2);
    _jointTypeSelectPanel.setLayout(_jointTypeSelectPanelFlowLayout);
    _algorithmSelectPanel.setLayout(_algorithmSelectPanelFlowLayout);
    _jointTypeSelectPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _algorithmSelectPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _jointTypeLabel.setPreferredSize(new Dimension(65, 17));
    _jointTypeLabel.setText(StringLocalizer.keyToString("ATGUI_FAMILY_KEY") + ":");
    _algorithmLabel.setPreferredSize(new Dimension(65, 17));
    _algorithmLabel.setText(StringLocalizer.keyToString("ATGUI_ALGORITHM_KEY") + ":");
    _okCancelPanel.setLayout(_okCancelPanelFlowLayout);
    _okCancelPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _okCancelPanelFlowLayout.setHgap(20);
    _okCancelPanelFlowLayout.setVgap(10);
    _cancelButton.setMnemonic(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_MNEMONIC_KEY").charAt(0));
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
//    _okButton.setNextFocusableComponent(_cancelButton);
    _okButton.setMnemonic(StringLocalizer.keyToString("GUI_OK_BUTTON_MNEMONIC_KEY").charAt(0));
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    _diagnosticsDefinitionPanel.setLayout(_diagnosticsDefinitionPanelBorderLayout);
    _diagnosticsDefinitionPanel.setBorder(_disabledExplanationPanelBorder);
    _diagnosticsDefinitionPanel.setPreferredSize(new Dimension(195, 285));
    _addRemoveDiagnosticsButtonPanel.setLayout(_addRemoveDiagnosticsButtonPanelGridLayout);
    _addDiagnosticsButton.setMnemonic(StringLocalizer.keyToString("ATGUI_ADD_MNEMONIC_KEY").charAt(0));
    _addDiagnosticsButton.setText(StringLocalizer.keyToString("ATGUI_ADD_KEY"));
    _addDiagnosticsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addDiagnosticsButton_actionPerformed(e);
      }
    });
//    _removeAllDiagnosticsButton.setNextFocusableComponent(_okButton);
    _removeAllDiagnosticsButton.setMnemonic(StringLocalizer.keyToString("ATGUI_REMOVE_ALL_MNEMONIC_KEY").charAt(0));
    _removeAllDiagnosticsButton.setText(StringLocalizer.keyToString("ATGUI_REMOVE_ALL_KEY"));
    _removeAllDiagnosticsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        removeAllDiagnosticsButton_actionPerformed(e);
      }
    });
//    _removeDiagnosticsButton.setNextFocusableComponent(_removeAllDiagnosticsButton);
    _removeDiagnosticsButton.setMnemonic(StringLocalizer.keyToString("ATGUI_REMOVE_MNEMONIC_KEY").charAt(0));
    _removeDiagnosticsButton.setText(StringLocalizer.keyToString("ATGUI_REMOVE_KEY"));
    _removeDiagnosticsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        removeDiagnosticsButton_actionPerformed(e);
      }
    });
    _addRemoveDiagnosticsButtonPanelGridLayout.setRows(4);
    _addRemoveDiagnosticsButtonPanelGridLayout.setVgap(10);
    _diagnosticsOnPanel.setBorder(_diagnosticsOnTitledBorder);
    _diagnosticsOnPanel.setPreferredSize(new Dimension(195, 285));
    _diagnosticsOnPanel.setLayout(_diagnosticsOnPanelBorderLayout);
    _onOffOnPanel.setLayout(_onOffOnPanelBorderLayout);
    _onOffOnPanel.setBorder(_onOffTitledBorder);
    _onOffOnPanel.setPreferredSize(new Dimension(195, 285));
    _addRemoveOnOffPanelGridLayout.setVgap(10);
    _addRemoveOnOffPanelGridLayout.setRows(4);
    _addOnOffButton.setMnemonic(StringLocalizer.keyToString("ATGUI_ADD_MNEMONIC_KEY").charAt(0));
    _addOnOffButton.setText(StringLocalizer.keyToString("ATGUI_ADD_KEY"));
    _addOnOffButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addOnOffButton_actionPerformed(e);
      }
    });
    _removeAllOnOffButton.setMnemonic(StringLocalizer.keyToString("ATGUI_REMOVE_ALL_MNEMONIC_KEY").charAt(0));
    _removeAllOnOffButton.setText(StringLocalizer.keyToString("ATGUI_REMOVE_ALL_KEY"));
    _removeAllOnOffButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        removeAllOnOffButton_actionPerformed(e);
      }
    });
    _removeOnOffButton.setMnemonic(StringLocalizer.keyToString("ATGUI_REMOVE_MNEMONIC_KEY").charAt(0));
    _removeOnOffButton.setText(StringLocalizer.keyToString("ATGUI_REMOVE_KEY"));
    _removeOnOffButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        removeOnOffButton_actionPerformed(e);
      }
    });
    _addRemoveOnOffButtonPanel.setLayout(_addRemoveOnOffPanelGridLayout);
    _disabledExplanationPanel.setBorder(_disabledExplanationPanelBorder);
    _disabledExplanationPanel.setPreferredSize(new Dimension(195, 285));
    _disabledExplanationPanel.setLayout(_disabledExplanationBorderLayout);
    _diagnosticsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _diagnosticsList.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyPressed(KeyEvent e)
      {
        diagnosticsList_keyPressed(e);
      }
    });
    _diagnosticsList.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent lse)
      {
        diagnosticListSelectionChanged(lse);
      }
    });

    _onOffList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _onOffList.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyPressed(KeyEvent e)
      {
        onOffList_keyPressed(e);
      }
    });
    _onOffList.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent lse)
      {
        onOffListSelectionChanged(lse);
      }
    });
    _jointTypeComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jointTypeComboBox_actionPerformed(e);
      }
    });
    _algorithmComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        algorithmComboBox_actionPerformed(e);
      }
    });
    _disabledExplanationTextArea.setWrapStyleWord(true);
    _disabledExplanationTextArea.setLineWrap(true);
    _disabledExplanationTextArea.setText(StringLocalizer.keyToString("ATGUI_DISABLED_EXPLANATION_KEY"));
    _disabledExplanationTextArea.setBackground(Color.lightGray);
    _disabledExplanationTextArea.setEditable(false);
    _disabledExplanationTextArea.setFont(new java.awt.Font("Dialog", 0, 12));

    _diagnosticsExplanationTextArea.setWrapStyleWord(true);
    _diagnosticsExplanationTextArea.setLineWrap(true);
    _diagnosticsExplanationTextArea.setText(StringLocalizer.keyToString("ATGUI_DIAGNOSTICS_EXPLANATION_KEY"));
    _diagnosticsExplanationTextArea.setBackground(Color.lightGray);
    _diagnosticsExplanationTextArea.setEditable(false);
    _diagnosticsExplanationTextArea.setFont(new java.awt.Font("Dialog", 0, 12));
    _okCancelInnerPanel.setLayout(_okCancelInnerGridLayout);
    _okCancelInnerGridLayout.setHgap(20);
    _okCancelInnerGridLayout.setVgap(0);


    getContentPane().add(_mainPanel);
    _mainPanel.add(_jointTypeAlgorithmSelectPanel, BorderLayout.NORTH);
    _jointTypeAlgorithmSelectPanel.add(_jointTypeSelectPanel, null);
    _jointTypeSelectPanel.add(_jointTypeLabel, null);
    _jointTypeSelectPanel.add(_jointTypeComboBox, null);
    _jointTypeAlgorithmSelectPanel.add(_algorithmSelectPanel, null);
    _algorithmSelectPanel.add(_algorithmLabel, null);
    _algorithmSelectPanel.add(_algorithmComboBox, null);
    _mainPanel.add(_okCancelPanel, BorderLayout.SOUTH);
    _okCancelPanel.add(_okCancelInnerPanel);
    _okCancelInnerPanel.add(_okButton);
    _okCancelInnerPanel.add(_cancelButton);
    _mainPanel.add(_configurationsTabbedPane, BorderLayout.CENTER);
    _configurationsTabbedPane.add(_diagnosticsPanel, StringLocalizer.keyToString("ATGUI_DIAGNOSTICS_KEY"));
    _addRemoveDiagnosticsButtonPanel.add(_addDiagnosticsButton, null);
    _addRemoveDiagnosticsButtonPanel.add(_removeDiagnosticsButton, null);
    _addRemoveDiagnosticsButtonPanel.add(_removeAllDiagnosticsButton, null);
    _diagnosticsOnPanel.add(_diagnosticsOnScrollPane, BorderLayout.CENTER);
    _diagnosticsOnScrollPane.getViewport().add(_diagnosticsList, null);
    _configurationsTabbedPane.add(_onOffPanel, StringLocalizer.keyToString("ATGUI_DISABLED_KEY"));
    _onOffPanel.add(_disabledExplanationPanel, null);
    _disabledExplanationPanel.add(_disabledExplanationTextArea, BorderLayout.NORTH);
    _onOffPanel.add(_addRemoveOnOffButtonPanel, null);
    _addRemoveOnOffButtonPanel.add(_addOnOffButton, null);
    _addRemoveOnOffButtonPanel.add(_removeOnOffButton, null);
    _addRemoveOnOffButtonPanel.add(_removeAllOnOffButton, null);
    _onOffPanel.add(_onOffOnPanel, null);
    _onOffOnPanel.add(_onOffOnScrollPane, BorderLayout.CENTER);
    _onOffOnScrollPane.getViewport().add(_onOffList, null);
    _diagnosticsPanel.add(_diagnosticsDefinitionPanel, null);
    _diagnosticsDefinitionPanel.add(_diagnosticsExplanationTextArea, java.awt.BorderLayout.NORTH);
    _diagnosticsPanel.add(_addRemoveDiagnosticsButtonPanel, null);
    _diagnosticsPanel.add(_diagnosticsOnPanel, null);
    _algorithmComboBox.setPreferredSize(new Dimension(180, CommonUtil._COMBO_BOX_HEIGHT));
    _jointTypeComboBox.setPreferredSize(new Dimension(180, CommonUtil._COMBO_BOX_HEIGHT));

    _configurationsTabbedPane.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        populateAlgorithmComboBoxWithJointType((JointTypeChoice)_jointTypeComboBox.getSelectedItem());
      }
    });

    populateFamilyAlgorithms();
    updateDiagnosticsList();
    updateOnOffList();
    setAllButtonsEnableState();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateFamilyAlgorithms()
  {
    PanelDataUtil.populateComboBoxWithJointTypes(_boardType, _jointTypeComboBox);
    _jointTypeComboBox.setMaximumRowCount(CommonUtil._MAXIMUM_COMBOBOX_ROWCOUNT);
//    _jointTypeComboBox.removeAllItems();
//
//    java.util.List<JointTypeEnum> jointTypes = _panelDataAdapter.getJointTypeEnums();
//
//    _jointTypeComboBox.addItem(new JointTypeChoice()); // the ALL case
//    for (JointTypeEnum jte : jointTypes)
//    {
//      JointTypeChoice jointTypeChoice = new JointTypeChoice(jte);
//      _jointTypeComboBox.addItem(jointTypeChoice);
//    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateAlgorithmComboBoxWithJointType(JointTypeChoice jointTypeChoice)
  {
    Assert.expect(jointTypeChoice != null);

    boolean disabledAlgosSelected = _configurationsTabbedPane.getSelectedIndex() == 1;

    _algorithmComboBox.removeAllItems();
    _algorithmComboBox.addItem(new AlgorithmChoice());  // the ALL case

    if (jointTypeChoice.isAllFamilies())
    {
      // This is a hardcoded list of common algorithms we want to set for all families
      for(AlgorithmEnum algorithmEnum : _commonAlgorithms)
      {
        if ((disabledAlgosSelected == false) || (algorithmEnum.equals(AlgorithmEnum.MEASUREMENT) == false))
        {
          AlgorithmChoice algoChoice = new AlgorithmChoice(algorithmEnum);
          _algorithmComboBox.addItem(algoChoice);
        }
      }
    }
    else
    {
      java.util.List<AlgorithmEnum> algos = _panelDataAdapter.getAlgorithms(jointTypeChoice.getJointTypeEnum());
      for (AlgorithmEnum ae : algos)
      {
        if ((disabledAlgosSelected == false) || (ae.equals(AlgorithmEnum.MEASUREMENT) == false))
        {
          AlgorithmChoice algoChoice = new AlgorithmChoice(ae);
          _algorithmComboBox.addItem(algoChoice);
        }
      }
    }
    _algorithmComboBox.setMaximumRowCount(CommonUtil._MAXIMUM_COMBOBOX_ROWCOUNT);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setAllButtonsEnableState()
  {
    // diagnostic tab
    // if there is not current selection, do not enable the remove button
    if (_diagnosticsList.getSelectedIndex() == -1)
    {
      _addDiagnosticsButton.setEnabled(true);
      _removeDiagnosticsButton.setEnabled(false);
    }
    else
    {
      _addDiagnosticsButton.setEnabled(false);
      _removeDiagnosticsButton.setEnabled(true);
    }

    if (_diagnosticsListModel.getSize() == 1)
    {
      Object element = _diagnosticsListModel.getElementAt(0);
      if (element instanceof String) // No Diagnostics message is a string
      {
        _addDiagnosticsButton.setEnabled(true);
        _removeAllDiagnosticsButton.setEnabled(false);
        _removeDiagnosticsButton.setEnabled(false);
      }
      else
      {
        _removeAllDiagnosticsButton.setEnabled(true);
//        _removeDiagnosticsButton.setEnabled(true);
      }
    }

    // on off tab
    // if there is not current selection, do not enable the remove button
    if (_onOffList.getSelectedIndex() == -1)
      _removeOnOffButton.setEnabled(false);
    else
      _removeOnOffButton.setEnabled(true);
    if (_onOffListModel.getSize() == 1)
    {
      Object element = _onOffListModel.getElementAt(0);
      if (element instanceof String)  // the no on/off is string
      {
        _removeAllOnOffButton.setEnabled(false);
        _removeOnOffButton.setEnabled(false);
      }
      else
      {
        _removeAllOnOffButton.setEnabled(true);
        _removeOnOffButton.setEnabled(true);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateDiagnosticsList()
  {
    // initial list setup goes here
    _diagnosticsListModel.clear();
    if (_algorithmConfiguration.anyDiagnosticsEnabled())
    {
      for (JointTypeAlgorithmPair jtap: _algorithmConfiguration.getDiagnosticFamilyAlgorithmPairs())
      {
        _diagnosticsListModel.addElement(jtap);
      }
    }

    if (_diagnosticsListModel.size() == 0)
    {
      _diagnosticsListModel.addElement(_DIAGNOSTICS_EMPTY_MESSAGE);  // this is a string
    }
    setAllButtonsEnableState();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateOnOffList()
  {
    // initial list setup goes here
    _onOffListModel.clear();
    if (_algorithmConfiguration.anyAlgorithmDisabled())
    {
      for (JointTypeAlgorithmPair jtap: _algorithmConfiguration.getDisabledFamilyAlgorithmPairs())
      {
        _onOffListModel.addElement(jtap);
      }
    }

    if (_onOffListModel.size() == 0)
    {
      _onOffListModel.addElement(_ONOFF_EMPTY_MESSAGE);
    }
    setAllButtonsEnableState();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    // save the newly updated _algorithmConfiguration to disk before exiting
    try
    {
      _algorithmConfiguration.writeSettingsPersistanceFile();
      dispose();
    }
    catch (FileNotFoundException fnfex)
    {
      LocalizedString localString = new LocalizedString("ATGUI_SETTINGS_NOT_SAVED_KEY", new Object[]{fnfex.getMessage()});
      JOptionPane.showMessageDialog(_frame, StringLocalizer.keyToString(localString),
                                    StringLocalizer.keyToString("ATGUI_SETTINGS_NOT_SAVED_TITLE_KEY"), JOptionPane.ERROR_MESSAGE);
    }
    catch (IOException ex)
    {
      LocalizedString localString = new LocalizedString("ATGUI_SETTINGS_NOT_SAVED_KEY", new Object[]{ex.getMessage()});
      JOptionPane.showMessageDialog(_frame, StringLocalizer.keyToString(localString),
                                    StringLocalizer.keyToString("ATGUI_SETTINGS_NOT_SAVED_TITLE_KEY"), JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    // Don't save the current settings to disk.  This effectively throws them away
    dispose();
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  private void addDiagnosticsButton_actionPerformed(ActionEvent e)
  {
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    AlgorithmChoice algoChoice = (AlgorithmChoice)_algorithmComboBox.getSelectedItem();

    JointTypeAlgorithmPair jtap = new JointTypeAlgorithmPair(jointTypeChoice, algoChoice);
    _algorithmConfiguration.addDiagnostics(jtap);

    updateDiagnosticsList();
    _diagnosticsList.setSelectedValue(jtap, true);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void removeCurrentDiagnosticSetting()
  {
    int index = _diagnosticsList.getSelectedIndex();
    if (index == -1)  // no current selection, do nothing
      return;

    Object element = _diagnosticsList.getSelectedValue();
    if (element instanceof String)  // the No settings message is a string
      return;

    Assert.expect(element instanceof JointTypeAlgorithmPair);
    JointTypeAlgorithmPair jtap = (JointTypeAlgorithmPair)element;

    _algorithmConfiguration.removeDiagnostic(jtap);

    updateDiagnosticsList();

    if ((_diagnosticsListModel.getSize() == 1) && (_diagnosticsListModel.contains(_DIAGNOSTICS_EMPTY_MESSAGE)))
      _diagnosticsList.clearSelection();  // if list model is empty, no selection
    else
    {
      if (index == _diagnosticsListModel.getSize())
        index--;
      _diagnosticsList.setSelectedIndex(index);
    }
  }


  /**
   * @author Andy Mechtenberg
   */
  private void removeCurrentOnOffSetting()
  {
    int index = _onOffList.getSelectedIndex();
    if (index == -1)  // no current selection, do nothing
      return;

    Object element = _onOffList.getSelectedValue();
    if (element instanceof String) // the No settings message
      return;

    Assert.expect(element instanceof JointTypeAlgorithmPair);
    JointTypeAlgorithmPair jtap = (JointTypeAlgorithmPair)element;

    _algorithmConfiguration.removeDisabled(jtap);
    updateOnOffList();

    if ((_onOffListModel.getSize() == 1) && (_onOffListModel.contains(_ONOFF_EMPTY_MESSAGE)))
      _onOffList.clearSelection();  // if list model is empty, no selection
    else
    {
      if (index == _onOffListModel.getSize())
        index--;
      _onOffList.setSelectedIndex(index);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void removeDiagnosticsButton_actionPerformed(ActionEvent e)
  {
    removeCurrentDiagnosticSetting();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void removeAllDiagnosticsButton_actionPerformed(ActionEvent e)
  {
    _algorithmConfiguration.removeAllDiagnostics();
    updateDiagnosticsList();
  }
  /**
   * @author Andy Mechtenberg
   */
  private void addOnOffButton_actionPerformed(ActionEvent e)
  {
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    AlgorithmChoice algoChoice = (AlgorithmChoice)_algorithmComboBox.getSelectedItem();

    JointTypeAlgorithmPair jtap = new JointTypeAlgorithmPair(jointTypeChoice, algoChoice);
    _algorithmConfiguration.addDisabled(jtap);
    updateOnOffList();
    _onOffList.setSelectedValue(jtap, true);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void removeOnOffButton_actionPerformed(ActionEvent e)
  {
    removeCurrentOnOffSetting();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void removeAllOnOffButton_actionPerformed(ActionEvent e)
  {
    _algorithmConfiguration.removeAllDisabled();
    updateOnOffList();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jointTypeComboBox_actionPerformed(ActionEvent e)
  {
    populateAlgorithmComboBoxWithJointType((JointTypeChoice)_jointTypeComboBox.getSelectedItem());
    setAllButtonsEnableState();
    _diagnosticsList.clearSelection();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void algorithmComboBox_actionPerformed(ActionEvent e)
  {
    setAllButtonsEnableState();
    _diagnosticsList.clearSelection();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void diagnosticListSelectionChanged(ListSelectionEvent lse)
  {
    if (lse.getValueIsAdjusting())
      return;

    setAllButtonsEnableState();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void onOffListSelectionChanged(ListSelectionEvent lse)
  {
    if (lse.getValueIsAdjusting())
      return;

    setAllButtonsEnableState();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void diagnosticsList_keyPressed(KeyEvent e)
  {
    if (e.getKeyCode() == KeyEvent.VK_DELETE)
      removeCurrentDiagnosticSetting();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void onOffList_keyPressed(KeyEvent e)
  {
    if (e.getKeyCode() == KeyEvent.VK_DELETE)
      removeCurrentOnOffSetting();
  }
}
