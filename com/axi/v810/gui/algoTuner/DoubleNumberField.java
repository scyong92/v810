package com.axi.v810.gui.algoTuner;

import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.swing.*;
import javax.swing.text.*;

import com.axi.util.*;

/**
 * This class handles the editing of double value thresholds
 * @author Andy Mechtenberg
 */
public class DoubleNumberField extends JTextField
{
  // this is key value to denote the user deleted the number here,
  // keying off this allows us to reset the value back to the default (back in AlgoTunerGUI.java)
  static final double EMPTY_VALUE = 99999999.9;
  private Toolkit _toolkit;
  private NumberFormat _doubleFormatter;

  /**
   * @author Andy Mechtenberg
   */
  public DoubleNumberField(double value, int columns)
  {
    super(columns);
    _toolkit = Toolkit.getDefaultToolkit();
    _doubleFormatter = NumberFormat.getNumberInstance(Locale.US);
    _doubleFormatter.setParseIntegerOnly(false);
    setHorizontalAlignment(JTextField.CENTER);
    setValue(value);
  }

  /**
   * @author Andy Mechtenberg
   */
  public double getValue()
  {
    double retVal = EMPTY_VALUE;
    try
    {
      retVal = _doubleFormatter.parse(getText()).doubleValue();
    }
    catch (ParseException e)
    {
      //toolkit.beep();
      // If there is a parse exception, send back the EMPTY_VALUE which will have the
      // effect of returning the cell to the default.
      // The valid cases where this exception is thrown is if the user entered just a . or - or deleted the entire value
      retVal = EMPTY_VALUE;  // do this on purpose, so deleting the contents can be flagged, and reset back to default
    }
    return retVal;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setValue(double value)
  {
    setText(_doubleFormatter.format(value));
  }

  /**
   * @author Andy Mechtenberg
   */
  protected Document createDefaultModel()
  {
    return new DoubleNumberDocument();
  }

  /**
   * @author Andy Mechtenberg
   */
  protected class DoubleNumberDocument extends PlainDocument
  {
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
    {
      char[] source = str.toCharArray();
      char[] result = new char[source.length];
      int j = 0;

      for (int i = 0; i < result.length; i++)
      {
        if (Character.isDigit(source[i]) || source[i] == '.' || source[i] == '-')
          result[j++] = source[i];
        else
        {
          // do nothing.  The beep is annoying
//          toolkit.beep();
//          System.err.println("insertString: " + source[i]);
        }
      }
      super.insertString(offs, new String(result, 0, j), a);
    }
  }
}
