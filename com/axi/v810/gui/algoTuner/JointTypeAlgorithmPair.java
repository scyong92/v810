package com.axi.v810.gui.algoTuner;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * This class is a convenient holder for a family+algo
 * @author Andy Mechtenberg
 */
public class JointTypeAlgorithmPair implements Comparator<JointTypeAlgorithmPair>, Serializable
{
  private JointTypeChoice _jointTypeChoice;
  private AlgorithmChoice _algorithmChoice;

  /**
   * @author Andy Mechtenberg
   */
  JointTypeAlgorithmPair() // by default, the ALL-ALL case
  {
    _jointTypeChoice = new JointTypeChoice();
    _algorithmChoice = new AlgorithmChoice();
  }

  /**
   * @author Andy Mechtenberg
   */
  JointTypeAlgorithmPair(JointTypeChoice jointTypeChoice, AlgorithmChoice algorithmChoice)
  {
    Assert.expect(jointTypeChoice != null);
    Assert.expect(algorithmChoice != null);

    _jointTypeChoice = jointTypeChoice;
    _algorithmChoice = algorithmChoice;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    return _jointTypeChoice.toString() + " - " + _algorithmChoice.toString();
  }

  /**
   * @author Andy Mechtenberg
   */
  JointTypeChoice getJointTypeChoice()
  {
    Assert.expect(_jointTypeChoice != null);

    return _jointTypeChoice;
  }

  /**
   * @author Andy Mechtenberg
   */
  AlgorithmChoice getAlgorithmChoice()
  {
    Assert.expect(_algorithmChoice != null);

    return _algorithmChoice;
  }

  /**
   * @author Andy Mechtenberg
   */
  String uniqueString()
  {
    Assert.expect(_jointTypeChoice != null);
    Assert.expect(_algorithmChoice != null);

    return new String(_jointTypeChoice.toString() + _algorithmChoice.toString());
  }

  /**
   * @author Andy Mechtenberg
   */
  public int hashCode()
  {
    return this.uniqueString().hashCode();
  }

  /**
   * @author Andy Mechtenberg
   */
  public int compare(JointTypeAlgorithmPair fap1, JointTypeAlgorithmPair fap2)
  {
    String s1 = fap1.uniqueString();
    String s2 = fap2.uniqueString();

    int len1 = s1.length();
    int len2 = s2.length();
    for (int i=0, n=Math.min(len1, len2); i<n; i++)
    {
      char c1 = s1.charAt(i);
      char c2 = s2.charAt(i);
      if (c1 != c2)
        return c1 - c2;
    }
    return len1 - len2;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean equals(Object rhs)
  {
    if (rhs == null)
      return false;

    if (rhs == this)
      return true;

    JointTypeAlgorithmPair fap = (JointTypeAlgorithmPair)rhs;

    return fap.uniqueString().equals(this.uniqueString());
  }
}
