package com.axi.v810.gui.algoTuner;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;

import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.gui.*;
import java.io.*;

/**
 * This is a helper class used by Review Defects and Review Measurements to display the
 * the image of a joint selected by the user.
 * @author George Booth
 */
public class JointImageViewer
{
  private Map<Long, Pair<String, Boolean>> _runIdToImageSetNameMap;

  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private JointResultsManager _jointResultsManager = JointResultsManager.getInstance();

  /**
   * @author George Booth
   */
  public JointImageViewer(List<PanelResults> panelResultsList)
  {
    // create map of run IDs to image set names
    _runIdToImageSetNameMap = new HashMap<Long, Pair<String, Boolean>>();
    setPanelResultsList(panelResultsList);
  }
  
  public void setPanelResultsList(List<PanelResults> panelResultsList)
  {
    for (PanelResults panelResults : panelResultsList)
    {
      String noImageSetFlag = "";
      /* uncomment to add no image set flag
        String noImageSetFlag = StringLocalizer.keyToString("RDGUI_NO_IMAGE_SET_KEY");
        String imageSetName = panelResults.getImageSetName();
        if (imageSetName.length() > 0)
        {
          String imageSetFilePath = Directory.getXrayInspectionImagesDir(_projectName, imageSetName);
          if (FileUtilAxi.exists(imageSetFilePath))
          {
            noImageSetFlag = "";
          }
        }
       */
      panelResults.setInformationFlag(noImageSetFlag);

      Pair<String, Boolean> pair = new Pair<String, Boolean>(panelResults.getImageSetName(),
                                                             new Boolean(panelResults.wereImagesCollectedOnline()));
      _runIdToImageSetNameMap.put(new Long(panelResults.getInspectionStartTimeInMillis()), pair);
    }
  }
  /**
   * @author George Booth
   */
  public void clearImageSetNameMap()
  {
    if (_runIdToImageSetNameMap != null)
      _runIdToImageSetNameMap.clear();
  }

  /**
 * @author George Booth
 */
  public ImageSetData getImageSetDataForRunID(long runId)
  {
    ImageSetData imageSetData = null;
    if (_runIdToImageSetNameMap.containsKey(runId))
    {
      Pair<String, Boolean> pair = _runIdToImageSetNameMap.get(runId);
      String imageSetName = pair.getFirst();
      boolean onLineTestDevlopement = pair.getSecond().booleanValue();
      boolean readImageSetInfoSuccess = false; // Ying-Huan.Chu
      if (imageSetName.length() > 0)
      {
        // Cheah Lee Herng
        String inspectionImagesDir = Directory.getXrayInspectionImagesDir(Project.getCurrentlyLoadedProject().getName());
        String imageInfoFileName = inspectionImagesDir + File.separator + imageSetName + File.separator + FileName.getImageSetInfoFileName();
        
        try
        {
          ImageSetInfoReader imageSetInfoReader = new ImageSetInfoReader();
          imageSetInfoReader.parseFile(imageInfoFileName);
          imageSetData = imageSetInfoReader.getImageSetData();
          readImageSetInfoSuccess = true;
        }
        catch(DatastoreException ex)
        {
          // Handle file not found error
          readImageSetInfoSuccess = false;
        }
        
        // If we could not read from ImageInfo file, we will initialize a new ImageSetData object 
        if (readImageSetInfoSuccess == false)
        {
          imageSetData = new ImageSetData();
        }
        
        imageSetData.setProjectName(Project.getCurrentlyLoadedProject().getName());
        imageSetData.setImageSetName(imageSetName);
        try
        {
          long timeInMils = StringUtil.convertStringToTimeInMilliSeconds(imageSetName);
          imageSetData.setDateInMils(timeInMils);
        }
        catch (BadFormatException bfe)
        {
          Assert.expect(false, "Image Set name is not in date-time format");
        }
        imageSetData.setIsOnlineTestDevelopment(onLineTestDevlopement);
      }
    }

    return imageSetData;
  }

  /**
   * Ask image pane to show joint image and highlight selected pad
   *
   * @author George Booth
   * @author Lim, Lay Ngor - Fix bug: Show non-resize & non-enhance image & roi location for short  
   * base on isSelectedItemAbleToPerformResize.
   */
  void showImageAndPad(long runId, JointResultsEntry jointResultsEntry, SliceNameEnum selectedSliceNameEnum, JointTypeEnum selectedJointTypeEnum,
    boolean isSelectedItemAbleToPerformResize)
  {
    Assert.expect(jointResultsEntry != null);
    //selectedSliceNameEnum is allowed to be null.
    
//    displayJointResults(jointResultsEntry);

    boolean showImage = true;
    ImageSetData imageSetData = getImageSetDataForRunID(runId);

//    System.out.println("glb - joint type = " + jointResultsEntry.getJointTypeName());
    String boardName = jointResultsEntry.getBoardName();
    String referenceDesignator = jointResultsEntry.getReferenceDesignator();
    String padName = jointResultsEntry.getPadName();
    boolean componentLevelTest = jointResultsEntry.isComponentLevelTest();

//    System.out.println("\nglb - show image for = " + boardName + ":" + referenceDesignator + "." + padName);
//    System.out.println("glb - componentLevelTest = " + componentLevelTest);

    ComponentResults componentResults = null;
    boolean componentResultsExist = _jointResultsManager.hasComponentResults(boardName, referenceDesignator);
//    System.out.println("glb - componentResultsExist = " + componentResultsExist);
    if (componentResultsExist)
    {
      componentResults = _jointResultsManager.getComponentResults(boardName, referenceDesignator);
//      displayComponentResults(componentResults);
    }

    Project project = Project.getCurrentlyLoadedProject();
    TestProgram testProgram = project.getTestProgram();
    Board board = project.getPanel().getBoard(boardName);
    com.axi.v810.business.panelDesc.Component component = board.getComponent(referenceDesignator);
    
    componentResultsExist = componentResultsExist && component.isInspected();
    Pad pad = null;
    //XCR-3455, Wrong image display if pad 1 is different joint type
    if (padName.equals(JointResultsEntry.NO_NAME))
    {
      if(jointResultsEntry.getSubtypeName().equalsIgnoreCase(JointResultsEntry._MIXED))
      {
        //XCR-3462, Software crashed after change joint type in a pad from mixed joint types. 
        if (selectedJointTypeEnum != null)
          pad = component.getPadOne(selectedJointTypeEnum);
        else if (jointResultsEntry.getJointTypeEnums().isEmpty() == false)
          pad = component.getPadOne(jointResultsEntry.getJointTypeEnums().iterator().next());
        else
          pad = component.getPadOne();
      }
      else
        pad = component.getPadOne();
    }
    else
      pad = component.getPad(padName);

    //Khaw Chek Hau - XCR2918 : Assert In Fine Tuning - Results tab after changed SM to TH on one of the pad
    //Do not show image if one of the pad's jointInspectionData of component is not exist due to
    //the pad has been modified by user, as it causes assert in getJointInspectionData method
    JointInspectionData jointInspectionData = null;
    JointInspectionData jointInspectionDataForComponent = null;
    List <JointInspectionData> componentPadJointInspectionDataList = new ArrayList <JointInspectionData>();
    
    if (component.isInspected())
    {
      for (Pad padForComponent : component.getPads())
      {
        if (padForComponent.isInspected())
        {
          for (TestSubProgram subProgram : testProgram.getAllTestSubProgramsIncludedNoInspectableTestSubPrograms())
          {
            if (subProgram.hasInspectionPad(padForComponent))
            {
              jointInspectionDataForComponent = subProgram.getJointInspectionData(padForComponent);
              break;
            }
          }

          if (jointInspectionDataForComponent == null)
          {
            showImage = false; 
            componentPadJointInspectionDataList.clear();
            break;
          }
          else
          {
            if (padForComponent.equals(pad))
              jointInspectionData = jointInspectionDataForComponent;

            if (componentPadJointInspectionDataList.contains(jointInspectionDataForComponent) == false)
              componentPadJointInspectionDataList.add(jointInspectionDataForComponent);
          }
          jointInspectionDataForComponent = null;
        }
      }
    }

    ReconstructedImages reconstructedImages = null;
    //Khaw Chek Hau - XCR2918 : Assert In Fine Tuning - Results tab after changed SM to TH on one of the pad
    if (showImage == false || imageSetData == null || jointInspectionData == null)
    {
      showImage = false;
    }
    else
    {
      ReconstructionRegion reconstructionRegion = jointInspectionData.getInspectionRegion();
      try
      {
        reconstructedImages = ImageManager.getInstance().loadInspectionImages(imageSetData, reconstructionRegion);
        reconstructedImages.ensureInspectedSlicesAreReadyForInspection();
      }
      catch (DatastoreException de)
      {
        // might not be any images
        showImage = false;
      }
      if ((reconstructedImages == null) || (reconstructedImages.getNumberOfSlices() == 0))
      {
        showImage = false;
      }
    }

    if (showImage)
    {
      SliceNameEnum sliceNameEnum = null;
      if (selectedSliceNameEnum == null)
      {
        // the user has "All" slices selected - use first reconstructed slice
        sliceNameEnum = reconstructedImages.getFirstSlice().getSliceNameEnum();
//        System.out.println("glb - default slice name = " + sliceNameEnum.getName());
      }
      else
      {
        sliceNameEnum = selectedSliceNameEnum;
//        System.out.println("glb - selected slice name = " + sliceNameEnum.getName());
      }
      
      //Broken Pin handling
      if(sliceNameEnum == SliceNameEnum.FOREIGN_INCLUSION)
        sliceNameEnum = SliceNameEnum.CAMERA_0;
      
      ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSliceIgnoreNullImages(sliceNameEnum);
      if (reconstructedSlice == null)
      {
        showImage = false;
      }

      if (showImage)
      {
        ImagePadSelection imagePadSelection = new ImagePadSelection(referenceDesignator + "-" + padName);
        //Lim, Lay Ngor - Fix bug: Set non-resize & non-enhance image for short base on isSelectedItemAbleToPerformResize.
        imagePadSelection.setImage(reconstructedImages, sliceNameEnum, isSelectedItemAbleToPerformResize);
        imagePadSelection.setReconstructionRegion(jointInspectionData.getInspectionRegion());
        imagePadSelection.setSliceNameEnum(reconstructedSlice.getSliceNameEnum());
        imagePadSelection.setImageSetData(imageSetData);
        imagePadSelection.setJointInspectionData(jointInspectionData);
        
        // get roi based on CAD only
        //Lim, Lay Ngor - Fix bug: Get non-resize & non-enhance roi location for short base on isSelectedItemAbleToPerformResize.
        RegionOfInterest cadLocatedRoi = jointInspectionData.getOrthogonalRegionOfInterestInPixels(isSelectedItemAbleToPerformResize);
//        if (cadLocatedRoi != null)
//        {
//          System.out.println("glb - CAD ROI center = " + cadLocatedRoi.getCenterX() + ", " +
//                             cadLocatedRoi.getCenterY());
//        }

        // get roi based on component results
        RegionOfInterest locatedRoi = null;
        RegionOfInterest wholeComponentLocatedRoi = null;
        if (componentResultsExist)
        {
//          if (locatedRoi != null)
//            System.out.println("glb - located ROI center = " + locatedRoi.getCenterX() + ", " + locatedRoi.getCenterY());
          
          // show complete component region of interest
          //Lim, Lay Ngor - Fix bug: Get non-resize & non-enhance roi location for short base on isSelectedItemAbleToPerformResize.
          if (componentLevelTest)
            locatedRoi = getComponentRegionOfInterestAtMeasuredLocation(jointInspectionData, isSelectedItemAbleToPerformResize);
          else
            locatedRoi = getRegionOfInterestAtMeasuredLocation(jointInspectionData, jointResultsEntry, componentResults,
              isSelectedItemAbleToPerformResize);
        }
        else
        {
//          System.out.println("glb - no component results, using CAD ROI center");
          locatedRoi = cadLocatedRoi;
        }

        // get roi based on Locator if data is available
        if (Locator.hasJointLocationMeasurements(jointInspectionData, sliceNameEnum))
        {
          //Lim, Lay Ngor - Fix bug: Get non-resize & non-enhance roi location for short base on isSelectedItemAbleToPerformResize.
          RegionOfInterest realLocatedRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData, isSelectedItemAbleToPerformResize);
//          if (realLocatedRoi != null)
//            System.out.println("glb - Locator ROI center = " + realLocatedRoi.getCenterX() + ", " +
//                               realLocatedRoi.getCenterY());
        }
        
        if (locatedRoi == null)
          showImage = false;

        if (showImage)
        {
          boolean passingTest = true;
          if (componentLevelTest)
          {
            Assert.expect(componentResults != null);
            passingTest = componentResults.passed();
          }
          else
          {
            passingTest = jointResultsEntry.getJointResults().passed();
          }

          MeasurementRegionDiagnosticInfo regionDiagnosticInfo = null;
          if (passingTest)
          {
            regionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(locatedRoi,
                MeasurementRegionEnum.PASSING_JOINT_REGION);
          }
          else
          {
            regionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(locatedRoi,
                MeasurementRegionEnum.FAILING_JOINT_REGION);
          }
          
          // bee-hoon.goh
          //Khaw Chek Hau - XCR2918 : Assert In Fine Tuning - Results tab after changed SM to TH on one of the pad
          if (component.isInspected())
          {
            for (JointInspectionData currentJointInspectionData : componentPadJointInspectionDataList)
            {
              currentJointInspectionData.addJointReconstructionRegionToPanelRect();
            }

            wholeComponentLocatedRoi = jointInspectionData.getWholeComponentOrthogonalRegionOfInterestInPixels(true);
            MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo = null;
            if (passingTest)
            {
              componentRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(wholeComponentLocatedRoi,
                  MeasurementRegionEnum.PASSING_JOINT_REGION);
            }
            else
            {
              componentRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(wholeComponentLocatedRoi,
                  MeasurementRegionEnum.FAILING_JOINT_REGION);
            }
            imagePadSelection.setComponentMeasurementDiagInfo(componentRegionDiagnosticInfo);
          }
          
          if (regionDiagnosticInfo != null)
          {
            imagePadSelection.setMeasurementDiagInfo(regionDiagnosticInfo);
            if (componentLevelTest)
            {
              _guiObservable.stateChanged(imagePadSelection, SelectionEventEnum.INSPECTION_IMAGE_COMPONENT_SELECTION);
            }
            else
            {
              _guiObservable.stateChanged(imagePadSelection, SelectionEventEnum.INSPECTION_IMAGE_PAD_SELECTION);
            }
          }
        }
      }
      if (reconstructedImages != null)
        reconstructedImages.decrementReferenceCount();
    }
    else
    {
//      System.out.println("glb - sorry, no image");
      _guiObservable.stateChanged(null, SelectionEventEnum.INSPECTION_IMAGE_NO_IMAGE_SELECTION);
    }
  }
  
  private List<ReconstructedImages> getAllSliceReconstructedImages(ImageSetData imageSetData, ReconstructionRegion reconstructionRegion)
  {
     List<ReconstructedImages> reconstructedImagesList = new ArrayList<ReconstructedImages>();
     try
     {
       ReconstructedImages reconstructedImages;
       reconstructedImages = ImageManager.getInstance().loadInspectionImages(imageSetData, reconstructionRegion);
       reconstructedImages.ensureInspectedSlicesAreReadyForInspection();
       
       if(reconstructedImagesList.contains(reconstructedImages) == false)
        reconstructedImagesList.add(reconstructedImages);
     }
     catch (DatastoreException de)
     {
     }
     
     return reconstructedImagesList;
  }

  /**
   * Leveraged from Locator to work with joint and component results rather than joint inspection data
   * George Booth
   * @author Lim, Lay Ngor - Fix bug: Return non-resize & non-enhance roi location for short  
   * base on isSelectedItemAbleToPerformResize.
   */
  private RegionOfInterest getRegionOfInterestAtMeasuredLocation(JointInspectionData jointInspectionData,
      JointResultsEntry jointResultsEntry, ComponentResults componentResults, boolean isSelectedItemAbleToPerformResize)
  {
    RegionOfInterest jointRoi = null;
    if (InspectionFamily.isSpecialTwoPinDevice(jointInspectionData.getInspectionFamilyEnum()) == false)
    {
//      System.out.println("glb - using joint results for ROI");
      // create a copy to avoid modifying the data
      jointRoi = new RegionOfInterest(jointInspectionData.getOrthogonalRegionOfInterestInPixels(isSelectedItemAbleToPerformResize));

      // the measured location lies at the center of the joint.
      //Khaw Chek Hau - XCR-3800 : CAD locator's location is incorrect if "Resize" algorithm setting is only turned
      ImageCoordinate jointCenter = getMeasuredLocation(jointInspectionData.getSubtype(), jointResultsEntry.getAllMeasurementResults(), 
        isSelectedItemAbleToPerformResize);
      jointRoi.setCenterXY(jointCenter.getX(), jointCenter.getY());
    }
    else
    {
      Assert.expect(componentResults != null);
//      System.out.println("glb - using component results for ROI");
      // figure out where the component was measured, and place a joint region relative to that.
      ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
      //Khaw Chek Hau - XCR-3800 : CAD locator's location is incorrect if "Resize" algorithm setting is only turned
      ImageCoordinate componentCenter = getMeasuredLocation(jointInspectionData.getSubtype(), componentResults.getAllMeasurementResults(),
        isSelectedItemAbleToPerformResize);

      jointRoi = jointInspectionData.getOrthogonalRegionOfInterestInPixels(isSelectedItemAbleToPerformResize);

      RegionOfInterest componentRoi = componentInspectionData.getOrthogonalComponentRegionOfInterest(isSelectedItemAbleToPerformResize);
      int differenceFromComponentRoiX = componentRoi.getCenterX() - jointRoi.getCenterX();
      int differenceFromComponentRoiY = componentRoi.getCenterY() - jointRoi.getCenterY();

      jointRoi.setCenterXY(componentCenter.getX(), componentCenter.getY());
      jointRoi.translateXY(-differenceFromComponentRoiX, -differenceFromComponentRoiY);
    }
    return jointRoi;
  }

  /**
   * Show complete component region of interest
   *
   * @author Bee Hoon,Goh
   * @author Lim, Lay Ngor - Fix bug: Return non-resize & non-enhance roi location 
   * for short base on isSelectedItemAbleToPerformResize.
   */
  private RegionOfInterest getComponentRegionOfInterestAtMeasuredLocation(JointInspectionData jointInspectionData, 
    boolean isSelectedItemAbleToPerformResize)
  {
    Assert.expect(jointInspectionData != null);
    ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
    RegionOfInterest componentRoi = componentInspectionData.getOrthogonalComponentRegionOfInterest(isSelectedItemAbleToPerformResize);

    return componentRoi;
  }
  
  /**
   * George Booth
   * XCR-3458, Wrong locator when highlight short relevant measurement
   * @author Lim, Lay Ngor - Fix bug: Return relevant measurementLocation base on isSelectedItemAbleToPerformResize.
   * if isSelectedItemAbleToPerformResize is false, we need to return non-resize & non-enhance location.
   */
  private ImageCoordinate getMeasuredLocation(Subtype subtype, List<MeasurementResult> measurementList, boolean isSelectedItemAbleToPerformResize)
  {
    Assert.expect(measurementList != null);
    Assert.expect(subtype != null);

    float xMeasurement = -1;
    float yMeasurement = -1;
    float lastResizeScaleFactor = 1;  
    List<Float> xMeasurementList = new ArrayList<Float>();
    List<Float> yMeasurementList = new ArrayList<Float>();
    //In this way we only get the last available value to be set into x & y location.
    for (MeasurementResult measurementResult : measurementList)
    {
      if (measurementResult.getMeasurementEnum().equals(MeasurementEnum.LOCATOR_X_LOCATION))
      {
        xMeasurementList.add(measurementResult.getValue());
        //        System.out.println("  x location = " + xMeasurement);
      }
      if (measurementResult.getMeasurementEnum().equals(MeasurementEnum.LOCATOR_Y_LOCATION))
      {
        yMeasurementList.add(measurementResult.getValue());
        //        System.out.println("  y location = " + yMeasurement);
      }
      
      //Khaw Chek Hau - XCR-3800 : CAD locator's location is incorrect if "Resize" algorithm setting is only turned on after "Run Test"
      if (measurementResult.getMeasurementEnum().equals(MeasurementEnum.IMAGE_RESIZE_SCALE))
      {
        lastResizeScaleFactor = measurementResult.getValue();
      }
    }

    if(xMeasurementList.isEmpty() == false && xMeasurementList.isEmpty() == false)
    {
      if (isSelectedItemAbleToPerformResize)
      {
        xMeasurement = Collections.max(xMeasurementList);
        yMeasurement = Collections.max(yMeasurementList);
        
        //Khaw Chek Hau - XCR-3800 : CAD locator's location is incorrect if "Resize" algorithm setting is only turned
        xMeasurement = xMeasurement / lastResizeScaleFactor;
        yMeasurement = yMeasurement / lastResizeScaleFactor;
        
        int currentResizeScaleFactor = AlgorithmUtil.getResizeFactor(subtype);
       
        xMeasurement = xMeasurement * (float) currentResizeScaleFactor;
        yMeasurement = yMeasurement * (float) currentResizeScaleFactor;
      }
      else
      {
        xMeasurement = Collections.min(xMeasurementList);
        yMeasurement = Collections.min(yMeasurementList);
      }
    }
    return new ImageCoordinate(Math.round(xMeasurement), Math.round(yMeasurement));
  }

  /**
   * For debug use only
   * George Booth
   */
  void displayJointResults(JointResultsEntry jointResultsEntry)
  {
    System.out.println("glb - joint results for " + jointResultsEntry.getBoardName() +
                       ":" + jointResultsEntry.getReferenceDesignator() +
                       "-" + jointResultsEntry.getPadName() + " passed = " + jointResultsEntry.passed());
    System.out.println("      all measurements");
    List<MeasurementResult> allMeasurementsList = jointResultsEntry.getAllMeasurementResults();
    for (MeasurementResult measurementResult : allMeasurementsList)
    {
      System.out.println("        measurement enum = " + measurementResult.getMeasurementEnum().getName());
    }
    System.out.println("      failed measurements");
    List<MeasurementResult> failedMeasurementList = jointResultsEntry.getFailingMeasurementResults();
    for (MeasurementResult measurementResult : failedMeasurementList)
    {
      System.out.println("        measurement enum = " + measurementResult.getMeasurementEnum().getName());
    }
    System.out.println("      related measurements");
    List<MeasurementResult> relatedMeasurementList = jointResultsEntry.getRelatedMeasurementResults();
    for (MeasurementResult measurementResult : relatedMeasurementList)
    {
      System.out.println("        measurement enum = " + measurementResult.getMeasurementEnum().getName());
    }
    List<IndictmentResult> indictmentResults = jointResultsEntry.getIndictmentResults();
    for (IndictmentResult indictmentResult : indictmentResults)
    {
      System.out.println("      indictment = " + indictmentResult.getIndictmentEnum().getName());
      System.out.println("        failed measurements");
      failedMeasurementList = indictmentResult.getFailingMeasurementResults();
      for (MeasurementResult measurementResult : failedMeasurementList)
      {
        System.out.println("          measurement enum = " + measurementResult.getMeasurementEnum().getName());
      }
      System.out.println("        related measurements");
      relatedMeasurementList = indictmentResult.getRelatedMeasurementResults();
      for (MeasurementResult measurementResult : relatedMeasurementList)
      {
        System.out.println("          measurement enum = " + measurementResult.getMeasurementEnum().getName());
      }
    }
  }

  /**
   * For debug use only
   * George Booth
   */
  void displayComponentResults(ComponentResults componentResults)
  {
    System.out.println("glb - component results for " + componentResults.getComponentName().getBoardName() +
                       ":" + componentResults.getComponentName().getReferenceDesignator() +
                       " passed = " + componentResults.passed());
    System.out.println("      all measurements");
    List<MeasurementResult> allMeasurementsList = componentResults.getAllMeasurementResults();
    for (MeasurementResult measurementResult : allMeasurementsList)
    {
      System.out.println("        measurement enum = " + measurementResult.getMeasurementEnum().getName() +
                         " value = " + measurementResult.getValue());
    }
    System.out.println("      failed measurements");
    List<MeasurementResult> failedMeasurementList = componentResults.getFailingMeasurementResults();
    for (MeasurementResult measurementResult : failedMeasurementList)
    {
      System.out.println("        measurement enum = " + measurementResult.getMeasurementEnum().getName() +
                         " value = " + measurementResult.getValue());
    }
    System.out.println("      related measurements");
    List<MeasurementResult> relatedMeasurementList = componentResults.getRelatedMeasurementResults();
    for (MeasurementResult measurementResult : relatedMeasurementList)
    {
      System.out.println("        measurement enum = " + measurementResult.getMeasurementEnum().getName() +
                         " value = " + measurementResult.getValue());
    }
    List<IndictmentResult> indictmentResults = componentResults.getIndictmentResults();
    for (IndictmentResult indictmentResult : indictmentResults)
    {
      System.out.println("      indictment = " + indictmentResult.getIndictmentEnum().getName());
      System.out.println("        failed measurements");
      failedMeasurementList = indictmentResult.getFailingMeasurementResults();
      for (MeasurementResult measurementResult : failedMeasurementList)
      {
        System.out.println("        measurement enum = " + measurementResult.getMeasurementEnum().getName() +
                           " value = " + measurementResult.getValue());
      }
      System.out.println("        related measurements");
      relatedMeasurementList = indictmentResult.getRelatedMeasurementResults();
      for (MeasurementResult measurementResult : relatedMeasurementList)
      {
        System.out.println("        measurement enum = " + measurementResult.getMeasurementEnum().getName() +
                           " value = " + measurementResult.getValue());
      }
    }
  }

}
