package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class constructs the copy thresholds dialog class
 * @author Andy Mechtenberg
 */
public class CopyThresholdsDlg extends JDialog
{
  private BoardType _boardType;
  private PanelDataAdapter _panelDataAdapter = null;
  private JointTypeEnum _jointTypeEnum = null;
  private Subtype _subtype = null;
  private Algorithm _algorithm = null;
  private AlgorithmSetting _algorithmSetting = null;

  private JPanel _mainDlgPanel = new JPanel();
  private BorderLayout _mainDlgPanelBorderLayout = new BorderLayout();
  private TitledBorder _toSubypteTitledBorder;
  private Border _emptySpace010010Border;
  private Border _emptySpace510510Border;
  private Border _thresholdToCopyTitledBorder;
  private JPanel _centerPanel = new JPanel();
  private DefaultListModel _subtypeListModel = new DefaultListModel();
  private Border _loweredBeveledBorder;
  private JRadioButton _singleThresholdRadioButton = new JRadioButton();
  private JRadioButton _entireAlgorithmRadioButton = new JRadioButton();
  private FlowLayout _currentFamilyPanelFlowLayout = new FlowLayout();
  private FlowLayout _currentAlgorithmPanelFlowLayout = new FlowLayout();
  private FlowLayout _currentThresholdPanelFlowLayout = new FlowLayout();
  private JRadioButton _entireJointTypeRadioButton = new JRadioButton();
  private JPanel _thresholdsToCopyPanel = new JPanel();
  private JPanel _currentAlgorithmPanel = new JPanel();
  private JPanel _currentThresholdPanel = new JPanel();
  private JPanel _entireAlgorithmPanel = new JPanel();
  private JPanel _entireFamilyPanel = new JPanel();
  private JLabel _currentFamilyLabel = new JLabel();
  private JPanel _singleThresholdPanel = new JPanel();
  private JLabel _currentAlgorithmLabel = new JLabel();
  private JLabel _currentThresholdLabel = new JLabel();
  private JPanel _thresholdSelectMainPanel = new JPanel();
  private BorderLayout _thresholdSelectMainPanelBorderLayout = new BorderLayout();
  private BorderLayout _entireFamilyPanelBorderLayout = new BorderLayout();
  private BorderLayout _entireAlgorithmPanelBorderLayout = new BorderLayout();
  private BorderLayout _singleThresholdPanelBorderLayout = new BorderLayout();
  private BorderLayout _thresholdsToCopyPanelBorderLayout = new BorderLayout();
  private JPanel _thresholdSelectPanel = new JPanel();
  private JPanel _currentFamilyPanel = new JPanel();
  private GridLayout _thresholdSelectPanelGridLayout = new GridLayout();
  private JPanel _subtypeSelectPanel = new JPanel();
  private JPanel _toSubtypePanel = new JPanel();
  private BorderLayout _subtypeSelectBorderLayout = new BorderLayout();
  private JList _subtypeList = new JList(_subtypeListModel);
  private JPanel _allSubtypesPanel = new JPanel();
  private JScrollPane _subtypeListScrollPane = new JScrollPane();
  private BorderLayout _toSubtypeBorderLayout = new BorderLayout();
  private JButton _allSubtypesButton = new JButton();
  private JPanel _bottomPanel = new JPanel();
  private JButton _okButton = new JButton();
  private JPanel _okCancelPanel = new JPanel();
  private FlowLayout _okCancelPanelFlowLayout = new FlowLayout();
  private JPanel _okCancelSubPanel = new JPanel();
  private BorderLayout _okCancelSubPanelBorderLayout = new BorderLayout();
  private JButton _cancelButton = new JButton();
  private BorderLayout _bottomPanelBorderLayout = new BorderLayout();
  private JLabel _statusReferenceLabel = new JLabel();
  private BorderLayout _statusPanelBorderLayout = new BorderLayout();
  private JLabel _currentSubtypeLabel = new JLabel();
  private JPanel _statusPanel = new JPanel();
  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  //Ngie Xing, XCR1796, APR 2014, Copy Algorithm Enabled Status
  private JCheckBox _copyEnabledStatusAlsoCheckBox = new JCheckBox();
  private JCheckBox _copyEnabledStatusOnlyCheckBox = new JCheckBox();
  private TitledBorder _copyEnabledStatusBorder;
  private JPanel _copyEnabledStatusPanel = new JPanel();

  /**
   * @author Andy Mechtenberg
   */
  CopyThresholdsDlg(Frame frame,
                    String title,
                    boolean modal,
                    PanelDataAdapter panelDataAdapter,
                    BoardType boardType,
                    JointTypeEnum jointTypeEnum,
                    Subtype subtype,
                    Algorithm algorithm,
                    AlgorithmSetting algorithmSetting)
  {
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(panelDataAdapter != null);
    Assert.expect(boardType != null);
    Assert.expect(jointTypeEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(algorithm != null);
    Assert.expect(algorithmSetting != null);
    _boardType = boardType;
    _panelDataAdapter = panelDataAdapter;
    _jointTypeEnum = jointTypeEnum;
    _subtype = subtype;
    _algorithm = algorithm;
    _algorithmSetting = algorithmSetting;

    jbInit();
    pack();
    // To make the dialog look good, I want the panels to be the size of the largest (threshold)
    // The second pack after the setPreferredSize will ensure this
    _subtypeSelectPanel.setPreferredSize(_thresholdSelectMainPanel.getPreferredSize());
    pack();
  }

  /**
   * @author Laura Cormos
   */
  CopyThresholdsDlg(Frame frame,
                    String title,
                    boolean modal,
                    PanelDataAdapter panelDataAdapter,
                    BoardType boardType,
                    JointTypeEnum jointTypeEnum,
                    Subtype subtype,
                    AlgorithmSetting algorithmSetting)
  {
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(panelDataAdapter != null);
    Assert.expect(boardType != null);
    Assert.expect(jointTypeEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(algorithmSetting != null);
    _boardType = boardType;
    _panelDataAdapter = panelDataAdapter;
    _jointTypeEnum = jointTypeEnum;
    _subtype = subtype;
    _algorithmSetting = algorithmSetting;

    jbInit();
    pack();
    // To make the dialog look good, I want the panels to be the size of the largest (threshold)
    // The second pack after the setPreferredSize will ensure this
    _subtypeSelectPanel.setPreferredSize(_thresholdSelectMainPanel.getPreferredSize());
    pack();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    _toSubypteTitledBorder = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,ColorUtil.getGrayShadowColor()),
                                              StringLocalizer.keyToString("ATGUI_TO_SUBTYPE_KEY") + ":");
    _emptySpace010010Border = BorderFactory.createCompoundBorder(_toSubypteTitledBorder,BorderFactory.createEmptyBorder(0,10,0,10));
    _emptySpace510510Border = BorderFactory.createEmptyBorder(5,10,5,10);
    _thresholdToCopyTitledBorder = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,ColorUtil.getGrayShadowColor()),StringLocalizer.keyToString("ATGUI_THRESHOLDS_TO_COPY_KEY") + ":"),BorderFactory.createEmptyBorder(0,5,0,5));

    _subtypeList = new JList(_subtypeListModel);

    java.util.List<Subtype> subtypes = _boardType.getInspectedSubtypes(_jointTypeEnum);
    for(Subtype subtype : subtypes)
    {
      if (subtype !=_subtype)  // don't add it to the list if it's the current subtype
        _subtypeListModel.addElement(subtype);
    }
    //Ngie Xing add, set a default selected subtype to the first one in the list
    _subtypeList.setSelectedIndex(0);
    //_subtypeListModel.addElement("VeryLongPackageName.FollowedByALongSubtypeNameThatsNowEvenLongerThanItWasBefore");
    _loweredBeveledBorder = BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,ColorUtil.getGrayShadowColor(),ColorUtil.getGrayInnerShadowColor());
    _mainDlgPanel.setLayout(_mainDlgPanelBorderLayout);
    _mainDlgPanelBorderLayout.setHgap(10);
    _centerPanel.setLayout(_centerPanelBorderLayout);
    //jLabel2.setText("SOIC24.Thick (17)");
    _centerPanel.setBorder(_emptySpace510510Border);
    _singleThresholdRadioButton.setSelected(true);
    _singleThresholdRadioButton.setText(StringLocalizer.keyToString("ATGUI_SINGLE_THRESHOLD_KEY") + ":");
    //Ngie Xing, XCR1796, APR 2014, Copy Algorithm Enabled Status
    _singleThresholdRadioButton.addItemListener(new java.awt.event.ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if (e.getStateChange() == ItemEvent.SELECTED)
        {
          disableCopyEnabledCheckBox();
        }
      }
    });
    if (_algorithm != null)
      _entireAlgorithmRadioButton.setText(StringLocalizer.keyToString("ATGUI_ENTIRE_ALGORITHM_KEY") + ":");
    else
      _entireAlgorithmRadioButton.setText(StringLocalizer.keyToString("ATGUI_ALL_SLICEHEIGHT_THRESHOLDS_KEY") + ":");
    //Ngie Xing, XCR1796, APR 2014, Copy Algorithm Enabled Status
    _entireAlgorithmRadioButton.addItemListener(new java.awt.event.ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if (e.getStateChange() == ItemEvent.SELECTED)
        {
          enableCopyEnabledCheckBox();
        }
      }
    });
    _currentFamilyPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _currentFamilyPanelFlowLayout.setHgap(25);
    _currentAlgorithmPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _currentAlgorithmPanelFlowLayout.setHgap(25);
    _currentThresholdPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _currentThresholdPanelFlowLayout.setHgap(25);
    _entireJointTypeRadioButton.setText(StringLocalizer.keyToString("ATGUI_ENTIRE_FAMILY_KEY") + ":");
    //Ngie Xing, XCR1796, APR 2014, Copy Algorithm Enabled Status
    _entireJointTypeRadioButton.addItemListener(new java.awt.event.ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if (e.getStateChange() == ItemEvent.SELECTED)
        {
          enableCopyEnabledCheckBox();
        }
      }
    });
    _thresholdsToCopyPanel.setLayout(_thresholdsToCopyPanelBorderLayout);
    _currentAlgorithmPanel.setLayout(_currentAlgorithmPanelFlowLayout);
    _currentThresholdPanel.setLayout(_currentThresholdPanelFlowLayout);
    _entireAlgorithmPanel.setLayout(_entireAlgorithmPanelBorderLayout);
    _entireFamilyPanel.setLayout(_entireFamilyPanelBorderLayout);
    _currentFamilyLabel.setForeground(Color.blue);
    _currentFamilyLabel.setText(_subtype.toString());
    _singleThresholdPanel.setLayout(_singleThresholdPanelBorderLayout);
    _currentAlgorithmLabel.setForeground(Color.blue);
    if (_algorithm != null)
      _currentAlgorithmLabel.setText(_algorithm.getAlgorithmEnum().toString());
    else
      _currentAlgorithmLabel.setText(_subtype.toString());
    _currentThresholdLabel.setForeground(Color.blue);
    _currentThresholdLabel.setText(_algorithmSetting.getName());
    _thresholdSelectMainPanel.setLayout(_thresholdSelectMainPanelBorderLayout);
    _thresholdSelectMainPanel.setBorder(_thresholdToCopyTitledBorder);
//    _thresholdSelectMainPanel.setPreferredSize(new Dimension(200, 205));
    _thresholdSelectPanel.setLayout(_thresholdSelectPanelGridLayout);
    _currentFamilyPanel.setLayout(_currentFamilyPanelFlowLayout);
    _thresholdSelectPanelGridLayout.setRows(3);
    _thresholdSelectPanelGridLayout.setVgap(10);
    _subtypeSelectPanel.setLayout(_subtypeSelectBorderLayout);
    _subtypeSelectPanel.setBorder(_emptySpace010010Border);
//    _subtypeSelectPanel.setPreferredSize(new Dimension(200, 205));
    _toSubtypePanel.setLayout(_toSubtypeBorderLayout);
    _subtypeSelectBorderLayout.setHgap(5);
    _subtypeSelectBorderLayout.setVgap(5);
    _toSubtypeBorderLayout.setVgap(5);
    _allSubtypesButton.setPreferredSize(new Dimension(100, 22));
    _allSubtypesButton.setMnemonic(StringLocalizer.keyToString("ATGUI_ALL_SUBTYPES_MNEMONIC_KEY").charAt(0));
    _allSubtypesButton.setText(StringLocalizer.keyToString("ATGUI_ALL_SUBTYPES_KEY"));
    _allSubtypesButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        allSubtypesButton_actionPerformed(e);
      }
    });
    _okButton.setPreferredSize(new Dimension(73, 27));
    _okButton.setMnemonic(StringLocalizer.keyToString("GUI_OK_BUTTON_MNEMONIC_KEY").charAt(0));
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    _okCancelPanel.setLayout(_okCancelPanelFlowLayout);
    _okCancelPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _okCancelPanelFlowLayout.setHgap(10);
    _okCancelSubPanel.setLayout(_okCancelSubPanelBorderLayout);
    _okCancelSubPanelBorderLayout.setHgap(10);
    _cancelButton.setMnemonic(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_MNEMONIC_KEY").charAt(0));
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _bottomPanel.setLayout(_bottomPanelBorderLayout);
    _statusReferenceLabel.setText(StringLocalizer.keyToString("ATGUI_CURRENT_SUBTYPE_KEY") + ":  ");
    _currentSubtypeLabel.setFont(new java.awt.Font("Dialog", 1, 12));
    _currentSubtypeLabel.setToolTipText("");
    _currentSubtypeLabel.setHorizontalTextPosition(SwingConstants.LEADING);
    _currentSubtypeLabel.setText(_subtype.toString());
    _statusPanel.setLayout(_statusPanelBorderLayout);
    _statusPanel.setBorder(_loweredBeveledBorder);
    _statusPanel.setPreferredSize(new Dimension(200, 21));
    getContentPane().add(_mainDlgPanel);
    _mainDlgPanel.add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.add(_thresholdsToCopyPanel, BorderLayout.WEST);
    _thresholdsToCopyPanel.add(_thresholdSelectMainPanel, BorderLayout.CENTER);
    _thresholdSelectMainPanel.add(_thresholdSelectPanel, BorderLayout.NORTH);
    _thresholdSelectPanel.add(_singleThresholdPanel, null);
    _singleThresholdPanel.add(_singleThresholdRadioButton, BorderLayout.CENTER);
    _singleThresholdPanel.add(_currentThresholdPanel, BorderLayout.SOUTH);
    _currentThresholdPanel.add(_currentThresholdLabel, null);
    _thresholdSelectPanel.add(_entireAlgorithmPanel, null);
    _entireAlgorithmPanel.add(_entireAlgorithmRadioButton, BorderLayout.CENTER);
    _entireAlgorithmPanel.add(_currentAlgorithmPanel, BorderLayout.SOUTH);
    _currentAlgorithmPanel.add(_currentAlgorithmLabel, null);
    _thresholdSelectPanel.add(_entireFamilyPanel, null);
    _entireFamilyPanel.add(_entireJointTypeRadioButton, BorderLayout.CENTER);
    _entireFamilyPanel.add(_currentFamilyPanel, BorderLayout.SOUTH);
    _currentFamilyPanel.add(_currentFamilyLabel, null);
    _centerPanel.add(_toSubtypePanel, BorderLayout.CENTER);
    _toSubtypePanel.add(_subtypeSelectPanel, BorderLayout.CENTER);
    _subtypeSelectPanel.add(_subtypeListScrollPane, BorderLayout.CENTER);
    _subtypeSelectPanel.add(_allSubtypesPanel, BorderLayout.SOUTH);
    _allSubtypesPanel.add(_allSubtypesButton, null);
    _subtypeListScrollPane.getViewport().add(_subtypeList, null);
    _mainDlgPanel.add(_bottomPanel, BorderLayout.SOUTH);
    _bottomPanel.add(_okCancelPanel, BorderLayout.CENTER);
    _okCancelPanel.add(_okCancelSubPanel, null);
    _okCancelSubPanel.add(_okButton, BorderLayout.WEST);
    _okCancelSubPanel.add(_cancelButton, BorderLayout.EAST);
    _bottomPanel.add(_statusPanel, BorderLayout.SOUTH);
    _statusPanel.add(_statusReferenceLabel, BorderLayout.WEST);
    _statusPanel.add(_currentSubtypeLabel, BorderLayout.CENTER);

    // custom stuff goes here
    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(_singleThresholdRadioButton);
    buttonGroup.add(_entireAlgorithmRadioButton);
    buttonGroup.add(_entireJointTypeRadioButton);

    //Ngie Xing, XCR1796, APR 2014, Copy Algorithm Enabled Status
    ///only one of the checkboxes can be selected
    _copyEnabledStatusOnlyCheckBox.setText(StringLocalizer.keyToString("ATGUI_COPY_ALGORITHM_ENABLED_STATUS_ONLY_KEY"));
    _copyEnabledStatusOnlyCheckBox.setSelected(false);
    _copyEnabledStatusOnlyCheckBox.setEnabled(false);
    _copyEnabledStatusOnlyCheckBox.addItemListener(new java.awt.event.ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if (e.getStateChange() == ItemEvent.SELECTED)
          _copyEnabledStatusAlsoCheckBox.setSelected(false);
      }
    });
    _copyEnabledStatusAlsoCheckBox.setText(StringLocalizer.keyToString("ATGUI_COPY_ALGORITHM_ENABLED_STATUS_ALSO_KEY"));
    _copyEnabledStatusAlsoCheckBox.setSelected(true);
    _copyEnabledStatusAlsoCheckBox.setEnabled(false);
    _copyEnabledStatusAlsoCheckBox.addItemListener(new java.awt.event.ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if (e.getStateChange() == ItemEvent.SELECTED)
          _copyEnabledStatusOnlyCheckBox.setSelected(false);
      }
    });

    _copyEnabledStatusPanel.setLayout(new BoxLayout(_copyEnabledStatusPanel, BoxLayout.Y_AXIS));
    _copyEnabledStatusPanel.setBorder(_copyEnabledStatusBorder);
    _copyEnabledStatusPanel.setPreferredSize(new Dimension(300, 45));
    _copyEnabledStatusPanel.add(_copyEnabledStatusOnlyCheckBox);
    _copyEnabledStatusPanel.add(_copyEnabledStatusAlsoCheckBox);
    _copyEnabledStatusPanel.add(Box.createRigidArea(new Dimension(0, 5)));

    JPanel entireCopyEnabledPanel = new JPanel();
    entireCopyEnabledPanel.setLayout(new BoxLayout(entireCopyEnabledPanel, BoxLayout.X_AXIS));
    entireCopyEnabledPanel.add(Box.createRigidArea(new Dimension(15, 0)));
    entireCopyEnabledPanel.add(_copyEnabledStatusPanel);
    _bottomPanel.add(entireCopyEnabledPanel, BorderLayout.WEST);
  }
  
  /**
   * @author Ngie Xing
   * XCR1796, APR 2014, Copy Algorithm Enabled Status
   */
  private void disableCopyEnabledCheckBox()
  {
    _copyEnabledStatusAlsoCheckBox.setEnabled(false);
    _copyEnabledStatusOnlyCheckBox.setEnabled(false);
  }
  
  /**
   * @author Ngie Xing
   * XCR1796, APR 2014, Copy Algorithm Enabled Status
   */
  private void enableCopyEnabledCheckBox()
  {
    _copyEnabledStatusAlsoCheckBox.setEnabled(true);
    _copyEnabledStatusOnlyCheckBox.setEnabled(true);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    setCursor(new Cursor(Cursor.WAIT_CURSOR));

    Object subtypes[] = _subtypeList.getSelectedValues();
    for (int i = 0; i < subtypes.length; i++)
    {
      Subtype targetSubtype = (Subtype)subtypes[i];
      //System.out.println(targetSubtype);
      if (_singleThresholdRadioButton.isSelected())
      {
        copyThreshold(_subtype, targetSubtype, _algorithmSetting);
      }
      else if (_entireAlgorithmRadioButton.isSelected())
      {
        // iterator over all the thresholds for the base algorithm (_algo), and copy the value from target subtype
        // to source subtype
        List<AlgorithmSetting> allAlgorithmSettings = new ArrayList<AlgorithmSetting>();
        if (_algorithm != null)
        {
          //Ngie Xing, XCR1796, APR 2014, Copy Algorithm Enabled Status
          if (_copyEnabledStatusAlsoCheckBox.isSelected())
          {
            enableAlgorithm(targetSubtype);
          }
          else if (_copyEnabledStatusOnlyCheckBox.isSelected())
          {
            enableAlgorithm(targetSubtype);
            continue;
          }
          
          //Ngie Xing, XCR1792 - Able to Copy PTH and PRESSFIT Barrel Slices Number
          allAlgorithmSettings = _panelDataAdapter.getAllThresholdsExcludeSliceHeight(_subtype, _subtype.getInspectionFamilyEnum(), _algorithm);
        }
        else
        {
          Collection<AlgorithmSettingEnum> algoSettingsEnums = UserDefinedSliceheights.getUserDefinedSliceheightSettings(_subtype.getJointTypeEnum());
          for(AlgorithmSettingEnum algoSettingEnum : algoSettingsEnums)
            allAlgorithmSettings.add(_subtype.getAlgorithmSetting(algoSettingEnum));
        }
        for (AlgorithmSetting algorithmSetting : allAlgorithmSettings)
        {
          copyThreshold(_subtype, targetSubtype, algorithmSetting);
        }
      }
      else if (_entireJointTypeRadioButton.isSelected())
      {
        // same as the entire algorithm case, except we add a iterator over all algorithms, then iterate over
        // all thresholds in each algorithm.
        java.util.List<Algorithm> allAlgos = _panelDataAdapter.getAlgorithms(_subtype); // returns a list string algo names
        for (Algorithm algorithm : allAlgos)
        {
          //Ngie Xing, XCR1796, APR 2014, Copy Algorithm Enabled Status
          if (_copyEnabledStatusAlsoCheckBox.isSelected())
            enableAlgorithm(targetSubtype, algorithm);
          else if (_copyEnabledStatusOnlyCheckBox.isSelected())
          {
            enableAlgorithm(targetSubtype, algorithm);
            continue;
          }
          java.util.List<AlgorithmSetting> allAlgorithmSettings = _panelDataAdapter.getAllThresholds(_subtype, _subtype.getInspectionFamilyEnum(), algorithm);
          for (AlgorithmSetting algorithmSetting : allAlgorithmSettings)
          {
            copyThreshold(_subtype, targetSubtype, algorithmSetting);
          }
        }
      }
      else
        Assert.expect(false);  // can't have all three unselected
    }
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    dispose();
  }
  
  /**
   * @author Wong Ngie Xing
   * XCR1796, APR 2014, copy algorithm enabled status
   */
  private void enableAlgorithm(Subtype targetSubtype)
  {
    if (_subtype.getEnabledAlgorithmEnums().contains(_algorithm.getAlgorithmEnum()))
      targetSubtype.setAlgorithmEnabled(_algorithm.getAlgorithmEnum(), true);
    else
      targetSubtype.setAlgorithmEnabled(_algorithm.getAlgorithmEnum(), false);
  }
  private void enableAlgorithm(Subtype targetSubtype, Algorithm algorithm)
  {
    if (_subtype.getEnabledAlgorithmEnums().contains(algorithm.getAlgorithmEnum()))
      targetSubtype.setAlgorithmEnabled(algorithm.getAlgorithmEnum(), true);
    else
      targetSubtype.setAlgorithmEnabled(algorithm.getAlgorithmEnum(), false);
  }

  /**
   * @author Andy Mechtenberg
   */
  void allSubtypesButton_actionPerformed(ActionEvent e)
  {
    _subtypeList.setSelectionInterval(0, _subtypeListModel.getSize()-1);
  }

  /**
   * @author Andy Mechtenberg
   */
  void copyThreshold(Subtype sourceSubtype, Subtype targetSubtype, AlgorithmSetting algorithmSetting)
  {
    Assert.expect(sourceSubtype != null);
    Assert.expect(targetSubtype != null);

    Assert.expect(algorithmSetting != null);
    Serializable sourceValue = sourceSubtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum());
    Serializable targetValue = targetSubtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum());
    // only change the value if it different
    if (sourceValue.equals(targetValue) == false)
      targetSubtype.setTunedValue(algorithmSetting.getAlgorithmSettingEnum(), sourceValue);
  }

}
