package com.axi.v810.gui.algoTuner;

import java.util.*;

import org.jfree.data.statistics.*;
import org.jfree.data.xy.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * Data set used to draw a X-Plot.
 * @author Rex Shang
 */
class JointResultsEntryXPlotDataSet extends AbstractXYDataset
{
  private List<JointResultsEntry> _jointResultsEntryList;
  private List<Double> _dataPoints;
  private List<String> _formattedValues;
  private List<Integer> _selectedRow = new ArrayList<Integer>(); 

  private MeasurementEnum _measurementType;
  private SliceNameEnum _sliceType;

  private double _mean;
  private double _standardDeviation;
  private double _median;

  private MeasurementUnitsEnum _measurementUnit;

  /**
   * Constructor used to build an empty chart.
   * @author Rex Shang
   */
  JointResultsEntryXPlotDataSet()
  {
    this(null, null, null);
  }

  /**
   * @author Rex Shang
   */
  JointResultsEntryXPlotDataSet(List<JointResultsEntry> resultsList, MeasurementEnum measurementType,
      SliceNameEnum sliceType)
  {
    _jointResultsEntryList = resultsList;
    _measurementType = measurementType;
    _sliceType = sliceType;

    if (_jointResultsEntryList == null)
      _jointResultsEntryList = new ArrayList<JointResultsEntry>();

    if(_dataPoints == null)
      _dataPoints = new ArrayList<Double>();
    
    if(_formattedValues == null)
      _formattedValues = new ArrayList<String>();

    populatePlotDataList();
    calculateStatistics();
  }

  /**
   * @author Rex Shang
   */
  private void populatePlotDataList()
  {
    int i = 0;
    // Find the value we need to chart.
    List<String> selectedHighlightJoint = null;
    if (Project.isCurrentProjectLoaded())
    {
      try
      {
        MeasurementJointHighlightSettingsReader defectiveJointReader = new MeasurementJointHighlightSettingsReader(Project.getCurrentlyLoadedProject().getName());
        selectedHighlightJoint = defectiveJointReader.getDefectiveSetting();
      }
      catch (DatastoreException e)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    e.getLocalizedMessage(),
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
      }
    }
    for (JointResultsEntry jointResult : _jointResultsEntryList)
    {
      List<MeasurementResult> measurementResults = jointResult.getAllMeasurementResults();
      boolean valueFound = false;
      
      for (MeasurementResult measurementResult : measurementResults)
      {
        if (measurementResult.getMeasurementEnum().equals(_measurementType)
            && measurementResult.getSliceNameEnum().equals(_sliceType))
        {
          Number value = measurementResult.getValue();
          MeasurementUnitsEnum measurementUnitsEnum = measurementResult.getMeasurementUnitsEnum();

          // conditionally convert to mils based on the project's preferred units.
          Pair<MeasurementUnitsEnum, Object>
              convertedValue = MeasurementUnitsEnum.convertToProjectUnitsIfNecessary(measurementUnitsEnum, value);
          measurementUnitsEnum = convertedValue.getFirst();
          value = (Number)convertedValue.getSecond();
          String valueString = MeasurementUnitsEnum.formatNumberIfNecessary(measurementUnitsEnum, value);
          valueString = MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(measurementUnitsEnum, valueString);
          
          String boardNameAndComponentNameAndJointName = jointResult.getBoardName()+","+measurementResult.getComponentName()+","+measurementResult.getPadName();
          
          if (selectedHighlightJoint != null && selectedHighlightJoint.contains(boardNameAndComponentNameAndJointName))
          {
            _selectedRow.add(i);
          }
          _dataPoints.add(value.doubleValue());
          _formattedValues.add(valueString);

          if (_measurementUnit == null)
            _measurementUnit = measurementUnitsEnum;

          valueFound = true;
          break;
        }
      }

      if (valueFound == false)
      {
        _dataPoints.add(Double.NaN);
        _formattedValues.add(Double.toString(Double.NaN));
      }
      i++;
    }
  }

  /**
   * @author Rex Shang
   */
  private void calculateStatistics()
  {
    // Check to see if we have any valid histogram data.
    if (_dataPoints.size() > 0)
    {
      // Convert List<Double> to double[] since our super.addSeries() can't
      // take a list...
      double[] dataSerie = new double[_dataPoints.size()];
      for (int i = 0; i < _dataPoints.size(); i++)
        dataSerie[i] = _dataPoints.get(i).doubleValue();

      // Do statistical calculation.
      _mean = Statistics.calculateMean(_dataPoints);
      _standardDeviation = Statistics.getStdDev(_dataPoints.toArray(new Double[0]));
      _median = Statistics.calculateMedian(_dataPoints);
    }
    else
    {
      _mean = Double.NaN;
      _standardDeviation = Double.NaN;
      _median = Double.NaN;
    }
  }

  /**
   * We will always have 1 serie of data.
   * @author Rex Shang
   */
  public int getSeriesCount()
  {
    return 1;
  }

  /**
   * To render tool tip, we need to have a name for our serie.
   * @author Rex Shang
   */
  public Comparable getSeriesKey(int series)
  {
    return _measurementType.toString();
  }

  /**
   * @author Rex Shang
   */
  public int getItemCount(int series)
  {
    return _dataPoints.size();
  }

  /**
   * For X-Plot, our X value is essentially the order number of this data point.
   * @author Rex Shang
   */
  public Number getX(int series, int item)
  {
    return item;
  }

  /**
   * Find the measurement value.
   * @author Rex Shang
   */
  public Number getY(int series, int item)
  {
    return _dataPoints.get(item);
  }

  /**
   * Find the formated measurement value with unit.
   * @author Rex Shang
   */
  public String getFormattedValue(int series, int item)
  {
    return _formattedValues.get(item);
  }

  /**
   * @author Rex Shang
   */
  JointResultsEntry getJointResultsEntry(int series, int item)
  {
    return _jointResultsEntryList.get(item);
  }

  /**
   * Get the mean value of this data set.
   * @author Rex Shang
   */
  double getMean()
  {
    return _mean;
  }

  /**
   * Get the median value of this data set.
   * @author Rex Shang
   */
  double getMedian()
  {
    return _median;
  }

  /**
   * Get the standard deviation of this data set.
   * @author Rex Shang
   */
  double getStandardDeviation()
  {
    return _standardDeviation;
  }

  /**
   * Get the general measurement unit.
   * @author Rex Shang
   */
  MeasurementUnitsEnum getMeasurementUnitsEnum()
  {
    return _measurementUnit;
  }

  void clearDataSet()
  {
    if(_dataPoints.size() > 0)
      _dataPoints.clear();
    if(_formattedValues.size() > 0)
    _formattedValues.clear();
  }
  
  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public List<Integer> getFilterComponentRow()
  {
    return _selectedRow;
  }
}
