package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.filechooser.FileFilter;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * <p>ReviewDefectsPanel</p>
 *
 * <p>This class will be responsible for displaying the Review Defects panel
 * within the algorithm tuner. Revie Defects operates only on joint results from the last
 * insepction run. There are two modes within Review Defects. <br>
 * 1) displaying failing joints and their failing and related measurements <br>
 * 2) displaying passing joints and their measurements.</p>
 * <P>The main mode will always be to show failing joints. If the user would like to switch to the
 * other mode, they will need to click the "Passing Defects" radio button over the tables.</p>
 * <P>Review Defects allows the user to filter the data via selections of board, joint type,
 * subtype, component, pad, slice names and indictments.
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class ReviewDefectsPanel extends AbstractTunerTaskPanel implements Observer
{
  private JScrollPane _jointSelectionScrollPane = null;
  private JSplitPane _reviewDefectsSplitPane = null;

  // Joint Selection panel ---------------------------
  private JPanel _jointSelectionPanel = null;

  private JScrollPane _inspectionRunListScrollPane = null;
  private InspectionRunList _inspectionRunList = null;

  private JPanel _dataSelectionPanel = null;

  private JPanel _indictmentTypePanel = null;
  private JCheckBox[] _indictmentsCheckBox = null;

  private JPanel _dataSouthPanel = null;
  private JPanel _sliceNamePanel = null;
  private JLabel _sliceNameLabel = null;
  private JComboBox _sliceNameComboBox = null;

  // Defect Information panel ---------------------------
  private JPanel _defectInformationPanel = null;

  private Box _pinCountBox = null;
  private JLabel _testedPinsLabel = null;
  private ButtonGroup _pinsButtonGroup = null;
  private JRadioButton _passingPinsRadioButton = null;
  private JRadioButton _defectivePinsRadioButton = null;

  private JSplitPane _defectInformationSplitPane = null;

  private JointInformationTableModel _jointInformationTableModel;
  private JointInformationTable _jointInformationTable;
  private JScrollPane _jointInformationTableScrollPane = new JScrollPane();

  private DefectDetailsTableModel _defectDetailsTableModel;
  private JSortTable _defectDetailsTable = null;
  private JScrollPane _defectDetailsTableScrollPane = new JScrollPane();
  private JLabel _useReviewMeasurementsLabel = null;

  // Defect Control menus and toolbar buttons
  private JMenuItem _deleteSelectedInspectionRunsMenuItem = null;
  private JMenuItem _generateReportMenuItem = null;
  private JMenu _optionsMenu = new JMenu();
  private JMenuItem _focusControlMenuItem = new JMenuItem();

  private JButton _generateReportButton = null;
  private MainUIMenuBar _menuBar = null;
  private int _menuRequesterID = -1;
  private MainUIToolBar _envToolBar = null;
  private int _envToolBarRequesterID = -1;

  private MainMenuGui _mainUI = null;
  private TestDev _testDev = null;
  private TestDevPersistance _tunerPersistSettings = null;
  private ProjectPersistance _projectPersistSettings = null;

  private Font _boldFont = null;

  private final int _INSPECTION_LIST_VISIBLE_ROWS = 5;
  private final int _JOINT_SELECTION_MINIMUM_HEIGHT = 70;
  private final int _JOINT_INFORMATION_MINIMUM_HEIGHT = 50;
  private final int _DEFECT_DETAILS_MINIMUM_HEIGHT = 30;
  private final double _FAILING_PINS_RESIZE_WEIGHT = 0.66;
  private boolean _showPassingJoints = false;
  private int _reviewDefectsResizeDivider = -1;
  private int _failingPinsResizeDivider = -1;
  private int _passingPinsResizeDivider = -1;
  private JointResultsEntry _selectedPassingJointResults = null;
  private JointResultsEntry _selectedFailingJointResults = null;
  private SliceNameChoice _selectedPassingSliceName = new SliceNameChoice();
  private SliceNameChoice _selectedFailingSliceName = new SliceNameChoice();

  private InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
  private GuiObservable _guiObservable = GuiObservable.getInstance();

  // UI is the current UI (viewable)
  private boolean _reviewDefectsActive = false;
  // UI has completed initializing
  private boolean _reviewDefectsReady = false;
  // UI can update graphics
  private boolean _shouldInteractWithGraphics = false;
  // A component is being initialized
  private boolean _initializing = false;

  private IndictmentEnum[] _INDICTMENTS = {
     IndictmentEnum.OPEN,
     IndictmentEnum.SHORT,
     IndictmentEnum.MISSING,
     IndictmentEnum.VOIDING,
     IndictmentEnum.INSUFFICIENT,
     IndictmentEnum.MISALIGNMENT,
     IndictmentEnum.COMPONENT_VOIDING,
     IndictmentEnum.EXCESS,
     IndictmentEnum.INDIVIDUAL_VOIDING,
     IndictmentEnum.DUMMY
  };
  private final int _OPEN_CALL = 0;
  private final int _SHORT_CALL = 1;
  private final int _MISSING_CALL = 2;
  private final int _VOIDING_CALL = 3;
  private final int _INSUFFICIENT_CALL = 4;
  private final int _MISALIGNMENT_CALL = 5;
  private final int _COMPONENT_VOIDING_CALL = 6;
  private final int _EXCESS_CALL = 7;
  private final int _INDIVIDUAL_VOIDING_CALL = 8; //@author Jack Hwee- individual void
  private final int _DUMMY_CALL = 9;

  private String _projectName = null;
  // board selection from Algo Tuner
  private String _selectedBoardName = null;
  // test selections from AlgoTuner
  private boolean _viewByJointType = true;
  private JointTypeEnum _selectedJointType = null;
  private Subtype _selectedSubtype = null;
  private ComponentType _selectedComponentType = null;
  private PadType _selectedPad = null;
  // inspection runs from this panel
  private List<Long> _selectedInspectionRuns;
  // slice and indictments from this panel
  private SliceNameEnum _selectedSlice = null;
  private java.util.ArrayList<IndictmentEnum> _selectedIndictments = null;
  // Joint image viewer
  private JointImageViewer _jointImageViewer = null;

  // for Generate Reports
  private JFileChooser _fileChooser = null;
  private String _inputDirectory = null;
  private static final String _reportFileFilterDescription =
      StringLocalizer.keyToString("RDGUI_GENERATE_REPORT_FILE_DESCRIPTION_KEY");
  
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private int _NUM_JOINTS_TO_UPDATE_POP_UP_PROGRESS_BAR = 100;
  
  private ChangeSubtypeDialog _changeSubtypeDialog = null;
  //Wong Ngie Xing
  private JMenu _runMenu = new JMenu();
  private JMenuItem _startTestMenuItem = new JMenuItem();
  
  private AlgoTunerGui _algoTunerGui;
  /**
   * @author George Booth
   */
  ReviewDefectsPanel(TestDev testDev,AlgoTunerGui algoTunnerGui)
  {
    Assert.expect(testDev != null);

    _mainUI = MainMenuGui.getInstance();
    _testDev = testDev;
    _selectedInspectionRuns = new ArrayList<Long>();
    _inspectionEventObservable.addObserver(this);
    _algoTunerGui = algoTunnerGui;

    jbInit();
  }

  /**
   * This string will get displayed as the tab name when added to a tabbed pane.
   * @author Rex Shang
   */
  public String getTunerTaskName()
  {
    return StringLocalizer.keyToString("ATGUI_REVIEW_DEFECTS_KEY");
  }

  /**
   * @author George Booth
   */
  void unpopulate()
  {
    if (_inspectionRunList != null)
      _inspectionRunList.unpopulate();
    if (_jointImageViewer != null)
      _jointImageViewer.clearImageSetNameMap();
    if (_jointInformationTable != null)
      _jointInformationTable.resetSortHeader();
    if (_jointInformationTableModel != null)
      _jointInformationTableModel.clear();
    if (_defectDetailsTable != null)
      _defectDetailsTable.clearSelection();
    _selectedSubtype = null;
    _selectedComponentType = null;
    _selectedPad = null;
  }

  /**
   * @author George Booth
   */
  private void populatePanelWithData()
  {
//    System.out.println("populatePanelWithData()");
    _projectName = Project.getCurrentlyLoadedProject().getName();

    _inspectionRunList.populate(_projectName, _selectedInspectionRuns);
    _selectedInspectionRuns = _inspectionRunList.getSelectedInspectionRuns();
    updateInspectionRun();

    // set up joint image viewer
    _jointImageViewer = new JointImageViewer(_inspectionRunList.getPanelResultsList());

    // set up the tables
    updateReviewDefectsData();

    // ok to tell graphics to highlight selected items
//    _shouldInteractWithGraphics = true;

    // reselect current row selection to cause graphics to highlight selected component
    if (_jointInformationTable.getRowCount() > 0)
    {
      int row = _jointInformationTable.getSelectedRow();
      if (row > -1)
      {
        _jointInformationTable.clearSelection();
        _jointInformationTable.setRowSelectionInterval(row, row);
        SwingUtils.scrollTableToShowSelection(_jointInformationTable);
      }
    }

    // set table header sizes after tables are displayed
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        _jointInformationTableModel.setColumnWidths(_jointInformationTable);
        _defectDetailsTableModel.setColumnWidths(_defectDetailsTable);
        _jointInformationTable.requestFocus();
      }
    });

  }

  /**
   * @author George Booth
   */
  private void jbInit()
  {
    _guiObservable.addObserver(this);

    // set up main panel
    this.setLayout(new BorderLayout());

    // set up Joint Selection panel
    setupJointSelectionPanel();

    _jointSelectionScrollPane = new JScrollPane();
    _jointSelectionScrollPane.getViewport().add(_jointSelectionPanel);

    // set up Defect Information panel
    setupDefectInformationPanel();

    //Create a split pane with the top and bottom panels in it.
    _reviewDefectsSplitPane = new JSplitPane(
        JSplitPane.VERTICAL_SPLIT,
        _jointSelectionScrollPane,
        _defectInformationPanel);
    this.add(_reviewDefectsSplitPane, BorderLayout.CENTER);

    //Provide minimum sizes for top component in the split pane
    _jointSelectionScrollPane.setMinimumSize(
        new Dimension(100, _JOINT_SELECTION_MINIMUM_HEIGHT));


    // set up Review Defects menus and tool bar buttons
    setupReviewDefectsMenus();


    // add the selection listener for pin information table
    ListSelectionModel pinInformationTableLSM = _jointInformationTable.getSelectionModel();
    pinInformationTableLSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        jointInformationTable_valueChanged(e);
      }
    });

    // add the selection listener for defect details information table
    ListSelectionModel testDetailsTableLSM = _defectDetailsTable.getSelectionModel();
    testDetailsTableLSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        defectDetailsTable_valueChanged(e);
      }
    });
  }

  /**
   * @author George Booth
   * @author Lim, Lay Ngor - Fix bug: Show non-resize & non-enhance image & roi location for short
   * base on user selected indictmentEnum type in Results Tab. But due to this is the selection on 
   * overall table, we will show the first Defect call image & location as default.
   */
  private void jointInformationTable_valueChanged(ListSelectionEvent e)
  {
	if (_reviewDefectsActive == false) // Ying-Huan.Chu
      return;
	
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    if (lsm.getValueIsAdjusting())
    {
      // do nothing
    }
    else if (lsm.isSelectionEmpty())
    {
      // clear defects details table
      updateDefectDetailsTable(null);
      // clear graphics
      _focusControlMenuItem.setEnabled(false);
      _guiObservable.stateChanged(null, SelectionEventEnum.INSPECTION_IMAGE_CLEAR_SELECTION);
      _guiObservable.stateChanged(null,HighlightEventEnum.CLEAR_HIGHLIGHTS);
    }
    else
    {
      // turn off event processing until graphics had a chance to update
      _jointInformationTable.processSelections(false);
      int row = lsm.getAnchorSelectionIndex();
      // show joint details
      JointResultsEntry jointResultsEntry =
          _jointInformationTableModel.getJointResultsEntryAt(row);
      setSelectedJoint(row);
      updateDefectDetailsTable(jointResultsEntry);

      if (_shouldInteractWithGraphics)
      {
        long runId = _jointInformationTableModel.getRunIdAt(row);
        //  get component results for comp
        String boardName = jointResultsEntry.getBoardName();

        // show the first slice in the defect details
        SliceNameEnum sliceName = null;
        boolean isSelectedMeasurementEnumRowAbleToPerformResize = true;
        if (_defectDetailsTable.getRowCount() > 0)
        {
          sliceName = _defectDetailsTableModel.getSliceNameEnum(0);
          //Lim, Lay Ngor - Fix bug: Show non-resize & non-enhance image & roi location for short
          //base on user selected measurementEnum type in Results Tab. But due to this is the selection on 
          //overall table, we will show the first Defect call image & location as default.
          if(_defectDetailsTableModel.getSelectedJointDetails(0).hasMeasurementResult())
          {
            MeasurementEnum selectedMeasurementEnum = _defectDetailsTableModel.getSelectedJointDetails(0).getMeasurement().getMeasurementEnum();
            isSelectedMeasurementEnumRowAbleToPerformResize = 
              AlgorithmUtil.isMeasurementEnumAbleToPerformResize(selectedMeasurementEnum);
          }
        }

        // show selected joint in image window
//        _jointImageViewer.showImageAndPad(runId, jointResultsEntry, _selectedSlice, componentResults);
        _jointImageViewer.showImageAndPad(runId, jointResultsEntry, sliceName, _selectedJointType, isSelectedMeasurementEnumRowAbleToPerformResize);

        // highlight selected component in CAD view
        Board board = Project.getCurrentlyLoadedProject().getPanel().getBoard(boardName);
        java.util.List<Board> selectedBoards = new ArrayList<Board>();
        selectedBoards.add(board);
        _guiObservable.stateChanged(selectedBoards, SelectionEventEnum.BOARD_INSTANCE);
        // get selected component
        java.util.List<ComponentType> selectedComponents = new ArrayList<ComponentType>();
        ComponentType compType = _jointInformationTableModel.getComponentTypeAt(row);
        selectedComponents.add(compType);
        _guiObservable.stateChanged(selectedComponents, SelectionEventEnum.COMPONENT_TYPE);

        // wait for graphics
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            _jointInformationTable.processSelections(true);
          }
        });
      }
    }
  }

  /**
   * @author George Booth
   */
  public void defectDetailsTable_valueChanged(ListSelectionEvent e)
  {
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    if (lsm.getValueIsAdjusting())
    {
      // do nothing
    }
    else if (lsm.isSelectionEmpty())
    {
      _useReviewMeasurementsLabel.setVisible(false);
    }
    else
    {
      int index = lsm.getAnchorSelectionIndex();

      // Get subtype, measurement type and slice name from the tables.
      IndictmentDetailsEntry indictment = _defectDetailsTableModel.getDetailsAt(index);

      // show selected joint/slice in image window
      JointResultsEntry jointResultsEntry = _selectedFailingJointResults;
      if (_showPassingJoints)
      {
        jointResultsEntry = _selectedPassingJointResults;
      }
      // get run Id for selected joint
      long runId = _jointInformationTableModel.getRunIdAt(_jointInformationTable.getSelectedRow());
      // get slice name for selected details
      SliceNameEnum sliceName = _defectDetailsTableModel.getSliceNameEnum(index);

      // show selected joint in image window
      //Lim, Lay Ngor - Fix bug: Show non-resize & non-enhance image & roi location for short
      //base on user selected measurementEnum type in Results Tab
      boolean isSelectedMeasurementEnumRowAbleToPerformResize = true;
      if(_defectDetailsTableModel.getSelectedJointDetails(0).hasMeasurementResult())
      {
        MeasurementEnum selectedMeasurementEnum = _defectDetailsTableModel.getSelectedJointDetails(index).getMeasurement().getMeasurementEnum();
        isSelectedMeasurementEnumRowAbleToPerformResize = AlgorithmUtil.isMeasurementEnumAbleToPerformResize(selectedMeasurementEnum);
      }
      _jointImageViewer.showImageAndPad(runId, jointResultsEntry, sliceName, _selectedJointType, isSelectedMeasurementEnumRowAbleToPerformResize);

      _useReviewMeasurementsLabel.setVisible(true);

      // Generate new GUI event
      MeasurementSelection measurementSelection = new MeasurementSelection(
          _selectedInspectionRuns,
          jointResultsEntry.getSubtypeName(),
          indictment.getMeasurement().getMeasurementEnum(),
          indictment.getMeasurement().getSliceNameEnum());
      _guiObservable.stateChanged(measurementSelection, SelectionEventEnum.MEASUREMENT_SELECTION);
    }
  }

  /**
   * @author George Booth
   */
  private void updateJointInformationTable()
  {
//    System.out.println("updateJointInformationTable()");

    // reapply local filter
    _jointInformationTableModel.updateJointResultsForNewFilterSelection(
        _selectedSlice, _selectedIndictments);

    // reload the table with either pass or failing joints
    _jointInformationTableModel.populateWithFilteredJoints(_showPassingJoints);
    updatePinCounts();

    // enable menus and buttons if there is data
    _generateReportMenuItem.setEnabled(_jointInformationTable.getRowCount() > 0);
    _generateReportButton.setEnabled(_jointInformationTable.getRowCount() > 0);

    // select the last joint selected by the user if still in table
    int selectedRow = -1;
    if (_showPassingJoints)
    {
      selectedRow = _jointInformationTableModel.getRowForJointResultsEntry(_selectedPassingJointResults);
    }
    else
    {
      selectedRow = _jointInformationTableModel.getRowForJointResultsEntry(_selectedFailingJointResults);
    }
    if (_jointInformationTable.getRowCount() == 0)
    {
      _jointInformationTable.clearSelection();
    }
    else
    {
      if (selectedRow > -1)
      {
        _jointInformationTable.setRowSelectionInterval(selectedRow, selectedRow);
      }
      else
      {
        // select first row
        _jointInformationTable.setRowSelectionInterval(0, 0);
      }
      SwingUtils.scrollTableToShowSelection(_jointInformationTable);
    }
    // update pin counts with new data
    updatePinCounts();
    // update list of slice names with new data
    updateSliceNames();
  }

  /**
   * @author George Booth
   */
  void setSelectedJoint(int row)
  {
    if (_showPassingJoints)
    {
      _selectedPassingJointResults = _jointInformationTableModel.getJointResultsEntryAt(row);
    }
    else
    {
      _selectedFailingJointResults = _jointInformationTableModel.getJointResultsEntryAt(row);
    }
  }

  /**
   * @author George Booth
   */
  private void updatePinCounts() {
    Integer numOfFailingJoints = new Integer(
        _jointInformationTableModel.getFailingJointCount());
    // set the number of defective pins
    _defectivePinsRadioButton.setText(StringLocalizer.keyToString("RDGUI_FAILING_PINS_LABEL_KEY") +
                                      " " + numOfFailingJoints.toString());

    Integer numOfPassingJoints = new Integer(
        _jointInformationTableModel.getPassingJointCount());
    // set the number of passing pins
    _passingPinsRadioButton.setText(StringLocalizer.keyToString("RDGUI_PASSING_PINS_LABEL_KEY") +
                                    " " + numOfPassingJoints.toString());

    Integer numOfTested = new Integer(
        _jointInformationTableModel.getTotalJointCount());
    _testedPinsLabel.setText(StringLocalizer.keyToString("RDGUI_TESTED_PINS_LABEL_KEY") +
                             " " + numOfTested.toString());
  }

  /**
   * @author George Booth
   */
  void updateSliceNames()
  {
    // initialize the slice names combo box
    _initializing = true;

    SliceNameChoice selectedSliceName = _selectedFailingSliceName;
    if (_showPassingJoints)
    {
      selectedSliceName = _selectedPassingSliceName;
    }

    // reselect the selected slice name if is still there, else "all"
    _sliceNameComboBox.removeAllItems();

    // Add "ALL" slice type.
    _sliceNameComboBox.addItem(new SliceNameChoice());

    List<SliceNameEnum> sliceNameEnums =
      _jointInformationTableModel.getSliceNames(_showPassingJoints);
    if (sliceNameEnums != null)
    {
      for (SliceNameEnum sliceNameEnum : sliceNameEnums)
      {
        _sliceNameComboBox.addItem(new SliceNameChoice(sliceNameEnum));
      }
    }

    // Re-select the selectedSliceName in newly populated combo box.
    SliceNameChoice toBeSelected = new SliceNameChoice();
    if (selectedSliceName.isAllFamilies() == false &&
        sliceNameEnums != null &&
        sliceNameEnums.contains(selectedSliceName.getSliceNameEnum()))
    {
      // Select the specific type if it is available in the list.
      toBeSelected = selectedSliceName;
    }
    _sliceNameComboBox.setSelectedItem(toBeSelected);

    setSliceName();

    _initializing = false;
  }

  /**
   * Enable any indictment checkboxes that are in failed joints and update quantities.
   * Update _desiredIndictments.
   * @author George Booth
   */
  private void updateIndictmentCheckboxes() {
    // update checkboxes as needed
    _selectedIndictments = new ArrayList<IndictmentEnum>();
    for (int i = 0; i < _INDICTMENTS.length; i++) {
      IndictmentEnum indictment = _INDICTMENTS[i];
      int count = _jointInformationTableModel.getIndictmentCount(indictment);
      if (count > 0)
      {
        String previousText = _indictmentsCheckBox[i].getText();
        boolean previousIndictments = previousText.indexOf("(") > 0;
        _indictmentsCheckBox[i].setText(indictment.getName() + " (" + count + ")");
        // if there were no previous indictments of this type, select it
        // otherwise leave the selection as the user last set it
        if (previousIndictments == false)
        {
          _indictmentsCheckBox[i].setSelected(true);
        }
        if (_indictmentsCheckBox[i].isSelected())
        {
          _selectedIndictments.add(indictment);
        }
        _indictmentsCheckBox[i].setEnabled(true);
      }
      else
      {
        _indictmentsCheckBox[i].setText(indictment.getName());
        // no indictment of this type - it should not be selected
        _indictmentsCheckBox[i].setSelected(false);
        _indictmentsCheckBox[i].setEnabled(false);
      }
    }
  }

  /**
   * @author George Booth
   */
  private void updateDefectDetailsTable(JointResultsEntry selectedJointResultEntry)
  {
    if(_showPassingJoints)
    {
      _defectDetailsTableModel.populateWithPassingJoints(selectedJointResultEntry,
          _selectedSlice, _selectedIndictments);
    }
    else
    {
      _defectDetailsTableModel.populateWithDefectiveJoints(selectedJointResultEntry,
          _selectedSlice, _selectedIndictments);
    }
  }

  /**
   * @author George Booth
   */
  private void sliceNameComboBox_actionPerformed() {
    if (_reviewDefectsReady == false || _initializing)
      return;

//    System.out.println("\nsliceNameComboBox_actionPerformed()");
    setSliceName();

    updateJointInformationTable();
  }

  /**
   * @author George Booth
   */
  private void setSliceName()
  {
    SliceNameChoice sliceName = (SliceNameChoice)_sliceNameComboBox.getSelectedItem();
    if (_showPassingJoints)
    {
      _selectedPassingSliceName = sliceName;
    }
    else
    {
      _selectedFailingSliceName = sliceName;
    }

    if (sliceName.isAllFamilies())
    {
      _selectedSlice = null;
    }
    else
    {
      _selectedSlice = sliceName.getSliceNameEnum();
    }
  }

  /**
   * @author George Booth
   */
  private void indictmentCheckBox_actionPerformed() {
//    System.out.println("\nindictmentCheckBox_actionPerformed()");
    // create set of enabled checkboxes
    _selectedIndictments = new ArrayList<IndictmentEnum>();
    for (int i = 0; i < _INDICTMENTS.length; i ++)
    {
      IndictmentEnum indictment = _INDICTMENTS[i];
      if (_indictmentsCheckBox[i].isSelected())
      {
        _selectedIndictments.add(indictment);
      }
    }
    updateJointInformationTable();
  }

  /**
   * @author George Booth
   */
  private void passingPinsRadioButton_actionPerformed()
  {
    // don't respond to redundant button click
    if (_showPassingJoints)
    {
      return;
    }

//    System.out.println("\npassingPinsRadioButton_actionPerformed()");
    // save current failing pins divider position
    _failingPinsResizeDivider = _defectInformationSplitPane.getDividerLocation();
    // restore split pane divider position
    if (_passingPinsResizeDivider > -1)
    {
      // make sure it is still OK if user shrunk the split plane size
      if (_defectInformationSplitPane.getHeight() > (_passingPinsResizeDivider + _DEFECT_DETAILS_MINIMUM_HEIGHT + 6))
      {
        _defectInformationSplitPane.setDividerLocation(_passingPinsResizeDivider);
      }
      else
      {
        // make it minimum
        _defectInformationSplitPane.setDividerLocation(_defectInformationSplitPane.getHeight() -
            _DEFECT_DETAILS_MINIMUM_HEIGHT - 6);  // -6 is empirical
          _passingPinsResizeDivider = _defectInformationSplitPane.getDividerLocation();
      }
    }
    _showPassingJoints = true;
    updateJointInformationTable();
  }

  /**
   * @author George Booth
   */
  private void defectivePinsRadioButton_actionPerformed()
  {
    // don't respond to redundant button click
    if (_showPassingJoints == false)
    {
      return;
    }

//    System.out.println("\ndefectivePinsRadioButton_actionPerformed()");
    // save current passing pins divider position
    _passingPinsResizeDivider = _defectInformationSplitPane.getDividerLocation();
    // restore split pane divider position
    if (_failingPinsResizeDivider > -1)
    {
      // make sure it is still OK if user shrunk the split plane size (+6 is empirical)
      if (_defectInformationSplitPane.getHeight() > (_failingPinsResizeDivider + _DEFECT_DETAILS_MINIMUM_HEIGHT + 6))
      {
        _defectInformationSplitPane.setDividerLocation(_failingPinsResizeDivider);
      }
      else
      {
        // make it minimum
        _defectInformationSplitPane.setDividerLocation(_defectInformationSplitPane.getHeight() -
            _DEFECT_DETAILS_MINIMUM_HEIGHT - 6);
          _failingPinsResizeDivider = _defectInformationSplitPane.getDividerLocation();
      }
    }
    _showPassingJoints = false;
    updateJointInformationTable();
  }

  /**
   * Update the selected row based on the row deleted.
   * Select the next row if there are rows below the deleted row.
   * Select the previous row if the deleted row was the last row.
   * Select no rows if the table is now empty.
   * @author George Booth
   */
  int updateSelectedRow(int deletedRow)
  {
    int lastRow = _jointInformationTable.getRowCount() - 1;

    // normally select the row after the one deleted (deletedRow is
    // the next row after the deletion)
    if (deletedRow < lastRow)
    {
      return deletedRow;
    }
    else
    {
      // last row was deleted, select previous row
      // returns -1 if table is now empty
      return deletedRow - 1;
    }
  }

  /**
   * @author George Booth
   */
  private void updateReviewDefectsData()
  {

//    System.out.println("updateReviewDefectsData()");
    // update the tables
    updateJointInformationTable();

    // update the results with current filters
    updateResultsWithAlgoTunerFilter();

    // update the screen
    validate();
    repaint();
  }

  /**
   * A selection was made in the AlgoTuner panel/board combo box
   * @author George Booth
   */
  void newBoardSelection(Object selectedItem)
  {
//    System.out.println("newBoardSelection()");
    if (selectedItem != null && selectedItem instanceof Board)
    {
      _selectedBoardName = ((Board)selectedItem).getName();
    }
    else // must be entire panel, set board name to null (means "all")
    {
      _selectedBoardName = null;
    }

    updateResultsWithAlgoTunerFilter();
  }

  /**
   * @author George Booth
   */
  void setCurrentTestSelection(TunerTestState tunerTestState)
  {
//    System.out.println("\nsetCurrentTestSelection()");
    if (tunerTestState.isJointTypeTest())
    {
      _viewByJointType = true;
      // Note, These are coming from combo boxes and could be null.
      if (tunerTestState.getJointTypeChoice() == null ||
          tunerTestState.getJointTypeChoice().isAllFamilies())
        _selectedJointType = null;
      else
        _selectedJointType = tunerTestState.getJointTypeChoice().getJointTypeEnum();

      if (tunerTestState.getSubtypeChoice() == null ||
          tunerTestState.getSubtypeChoice().isAllSubtypes())
        _selectedSubtype = null;
      else
        _selectedSubtype = tunerTestState.getSubtypeChoice().getSubtype();
      
      if(_selectedSubtype != null)
      {
        _jointInformationTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
      }
      else
      {
        _jointInformationTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      }
    }
    else
    {
      _viewByJointType = false;
      // Note, These are coming from combo boxes and could be null.
      _selectedComponentType = tunerTestState.getComponentType();

      if (tunerTestState.getPadChoice() == null ||
          tunerTestState.getPadChoice().isAllPads())
        _selectedPad = null;
      else
        _selectedPad = tunerTestState.getPadChoice().getPadType();
      
      _jointInformationTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    updateResultsWithAlgoTunerFilter();
  }

  /**
   * @author George Booth
   */
  void updateResultsWithAlgoTunerFilter()
  {
//    System.out.println("updateResultsWithAlgoTunerFilter()");
    if (_reviewDefectsReady)
    {
      // filter by the Algo Tuner selections
      if (_viewByJointType)
      {
        _jointInformationTableModel.updateJointResultsForNewFilterSelection(
            _selectedBoardName, _selectedJointType, _selectedSubtype);
      }
      else
      {
        _jointInformationTableModel.updateJointResultsForNewFilterSelection(
            _selectedBoardName, _selectedComponentType, _selectedPad);
      }

      _jointInformationTableModel.updateJointResultsForNewFilterSelection(
            _selectedSlice, _selectedIndictments);

      // update slice names with new data
      _jointInformationTableModel.updateSliceNameForNewFilterSelection();
      updateSliceNames();
      // update indictment counts with new data
      _jointInformationTableModel.updateIndictmentCountForNewFilterSelection();
      updateIndictmentCheckboxes();

      // displat new data
      updateJointInformationTable();
    }
  }

  /**
   * @author George Booth
   */
  void updateInspectionRun()
  {
    if (_reviewDefectsReady)
    {
//      System.out.println("updateInspectionRun()");
      _projectName = Project.getCurrentlyLoadedProject().getName();
      _deleteSelectedInspectionRunsMenuItem.setEnabled(_selectedInspectionRuns.size() > 0);
      _jointInformationTableModel.updateJointResultsForNewInspectionRun(_projectName, _selectedInspectionRuns);
      if (JointResultsManager.getInstance().jointResultsReadCancelled())
      {
        // clear selections (show nothing)
        _inspectionRunList.clearSelection();
        return;
      }
      // now, make sure only these results are selected in the _inspectionRunList
      List<Long> completedResults = JointResultsManager.getInstance().getCompletedResultsList();
      List<Long> currentSelectedRuns = _inspectionRunList.getSelectedInspectionRuns();
      // if all them did not get loaded, the size of these won't match.  In that case, update the visible selection
      // to reflect what DID get loaded.
      if (currentSelectedRuns.size() != completedResults.size())
        _inspectionRunList.setSelectedInspectionRuns(completedResults);
            
      // reapply AlgoTuner filters
      updateResultsWithAlgoTunerFilter();
    }
  }

  /**
   * @author George Booth
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    Assert.expect(observable != null);

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof GuiObservable)
        {
          final GuiEvent guiEvent = (GuiEvent)object;
          final GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
          if (guiEventEnum instanceof ImageSetEventEnum)
          {
            ImageSetEventEnum imageSetSelectionEventEnum = (ImageSetEventEnum)guiEventEnum;
          }
          else if (guiEventEnum instanceof SelectionEventEnum)
          {
            SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
            if (selectionEventEnum.equals(SelectionEventEnum.TUNER_TEST_SELECTION))
            {
              // ok, something changed in the tuner selection (joint type, subtype, component, pin)
              // get the data from the object argument
              TunerTestState tunerTestState = (TunerTestState)guiEvent.getSource();
              setCurrentTestSelection(tunerTestState);
            }
            else if (selectionEventEnum.equals(SelectionEventEnum.TUNER_BOARD_SELECTION))
            {
              // get the data from the object argument
              newBoardSelection(guiEvent.getSource());
            }
            else if (selectionEventEnum.equals(SelectionEventEnum.INSPECTION_RUN_SELECTION))
            {
//              System.out.println("RD: SelectionEventEnum.INSPECTION_RUN_SELECTION seen");
              // the Review Defects UI is not active until first event is seen
              if (_reviewDefectsActive)
              {
                _reviewDefectsReady = true;
                _shouldInteractWithGraphics = true;
              }
              // track inspection run selections
              InspectionRunSelection inspectionRunSelection = (InspectionRunSelection)guiEvent.getSource();
              _selectedInspectionRuns = inspectionRunSelection.getInspectionRuns();
              // update the UI with new selected inspection runs if ready
              updateInspectionRun();
              // inspectionRunList has disabled selection processing; it MUST be re-enabled
              // after all processing is finished
              _guiObservable.stateChanged(null, SelectionEventEnum.INSPECTION_RUN_SELECTION_PROCESSING_COMPLETE);
            }
          }
          else if (guiEventEnum instanceof GuiUpdateEventEnum)
          {
            GuiUpdateEventEnum updateEventEnum = (GuiUpdateEventEnum)guiEventEnum;
            if (updateEventEnum.equals(GuiUpdateEventEnum.INSPECTION_RUNS_UPDATED))
            {
//              System.out.println("RD: GuiUpdateEventEnum.INSPECTION_RUNS_UPDATED seen");
              InspectionRunSelection inspectionRunSelection = (InspectionRunSelection)guiEvent.getSource();
              _selectedInspectionRuns = inspectionRunSelection.getInspectionRuns();
              // update the UI with new inspection runs list
              populatePanelWithData();
            }
          }
        }
        else if (observable instanceof InspectionEventObservable)
        {
          InspectionEvent event = (InspectionEvent)object;
          InspectionEventEnum eventEnum = event.getInspectionEventEnum();
          if (eventEnum.equals(InspectionEventEnum.INSPECTION_STARTED))
          {
            // don't care
          }
          else if (eventEnum.equals(InspectionEventEnum.INSPECTION_COMPLETED))
          {
//            System.out.println("RD: InspectionEventEnum.INSPECTION_COMPLETED seen");
            // new inspection results cause the data to be refreshed with last run displayed by default
            _selectedInspectionRuns = null;
//            _inspectionRunList.clearSelection();
          }
        }
        else
        {
          Assert.expect(false, "No observer found.");
        }
      }
    });
  }

  /**
   * Write defect report to selected file
   * @author George Booth
   */
  private void generateReport(String reportFileName)
  {
    // create report data
    ReviewDefectsReportTableModel reportTableModel = new ReviewDefectsReportTableModel();
    // do the table data as it is currently sorted
    for (int row = 0; row < _jointInformationTable.getRowCount(); row++)
    {
      // joint result
      JointResultsEntry jointResultsEntry = _jointInformationTableModel.getJointResultsEntryAt(row);

      // result details (one entry per detail)
      DefectDetailsTableModel detailsTableModel = new DefectDetailsTableModel();
      if(_showPassingJoints)
        detailsTableModel.populateWithPassingJoints(jointResultsEntry, _selectedSlice, _selectedIndictments);
      else
        detailsTableModel.populateWithDefectiveJoints(jointResultsEntry, _selectedSlice, _selectedIndictments);

      for (int detailsRow = 0; detailsRow < detailsTableModel.getRowCount(); detailsRow++)
      {
        reportTableModel.addJointData(jointResultsEntry, detailsTableModel.getDetailsAt(detailsRow));
      }
    }

    // write report
    String reportTitle = StringLocalizer.keyToString("RDGUI_GENERATE_REPORT_TITLE_KEY");
    TableModelReportWriter reportWriter = new TableModelReportWriter();
    try
    {
      reportWriter.writeCSVFile(reportFileName, reportTitle, reportTableModel);
    }
    catch(IOException ioe)
    {
      MessageDialog.reportIOError(this, ioe.getLocalizedMessage());
      return;
    }
  }

  /**
   * Handle the Generate Report button action
   * @author George Booth
   */
  private void generateReport_actionPerformed()
  {
    String reportFileName = selectFile();

    if (reportFileName.length() > 0)
    {
      _inputDirectory = FileUtil.getParent(reportFileName);

      generateReport(reportFileName);
    }
  }

  /**
   * This method displays a file chooser and returns the file selected by the user.  Returns a
   * zero-length string if there is no selection.
   * @author George Booth
   */
  private String selectFile()
  {
     _fileChooser = new JFileChooser(_inputDirectory);

     // initialize the JFIle Dialog.
     FileFilter fileFilter = _fileChooser.getAcceptAllFileFilter();
     _fileChooser.removeChoosableFileFilter(fileFilter);

     // create and instance of Text File Filter
     String commaSeparatedValueFileExtension = FileName.getCommaSeparatedValueFileExtension();
     FileFilterUtil reportFileFilter = new FileFilterUtil(commaSeparatedValueFileExtension, _reportFileFilterDescription);

     _fileChooser.addChoosableFileFilter(reportFileFilter);
     _fileChooser.setFileFilter(reportFileFilter); // default to new report file

    if ( _fileChooser.showDialog(this, StringLocalizer.keyToString("GUI_SELECT_FILE_KEY")) != JFileChooser.APPROVE_OPTION )
      return "";

    File selectedFile = _fileChooser.getSelectedFile();
    String selectedFileName = selectedFile.getPath();
    if (selectedFileName.endsWith(commaSeparatedValueFileExtension) == false)
    {
      selectedFileName = selectedFileName + commaSeparatedValueFileExtension;
    }

    return selectedFileName;
  }

  /**
   * Sets up the Defect Summary Panel
   * @author George Booth
   */
  private void setupJointSelectionPanel() {
    _jointSelectionPanel = new JPanel();
    _jointSelectionPanel.setLayout(new BorderLayout());

    // inspection runs list
    _inspectionRunList = new InspectionRunList(_INSPECTION_LIST_VISIBLE_ROWS);
    _inspectionRunListScrollPane = new JScrollPane();
    _inspectionRunListScrollPane.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(
      Color.white,new Color(165, 163, 151)),
      StringLocalizer.keyToString("RMGUI_INSPECTION_RUN_TITLE_KEY")));
    _inspectionRunListScrollPane.getViewport().add(_inspectionRunList);
    _jointSelectionPanel.add(_inspectionRunListScrollPane, BorderLayout.CENTER);

    // Data selection panel
    _dataSelectionPanel = new JPanel();
    _dataSelectionPanel.setLayout(new BorderLayout());
    _jointSelectionPanel.add(_dataSelectionPanel, BorderLayout.SOUTH);

    // Defect Type panel
    _indictmentTypePanel = new JPanel();
    _indictmentTypePanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(
      Color.white,new Color(165, 163, 151)),
      StringLocalizer.keyToString("RDGUI_PIN_DEFECT_CALLS_LABEL_KEY")));
    _indictmentTypePanel.setLayout(new GridLayout(2, 0, 0, 0));
    _dataSelectionPanel.add(_indictmentTypePanel, BorderLayout.NORTH);

    // type checkboxes
    _selectedIndictments = new ArrayList<IndictmentEnum>();
    _indictmentsCheckBox = new JCheckBox[_INDICTMENTS.length];
    for (int i = 0; i < _INDICTMENTS.length; i++)
    {
      _selectedIndictments.add(_INDICTMENTS[i]);
      _indictmentsCheckBox[i] = new JCheckBox();
      _indictmentsCheckBox[i].setText(_INDICTMENTS[i].getName());
      _indictmentsCheckBox[i].setSelected(false);
      _indictmentsCheckBox[i].setEnabled(false);
      _indictmentsCheckBox[i].addActionListener(new ActionListener()
        { public void actionPerformed(ActionEvent e) {
          indictmentCheckBox_actionPerformed();
        }});
    }
    _indictmentTypePanel.add(_indictmentsCheckBox[_OPEN_CALL], null);
    _indictmentTypePanel.add(_indictmentsCheckBox[_SHORT_CALL], null);
    _indictmentTypePanel.add(_indictmentsCheckBox[_MISSING_CALL], null);
    _indictmentTypePanel.add(_indictmentsCheckBox[_VOIDING_CALL], null);
    _indictmentTypePanel.add(_indictmentsCheckBox[_INSUFFICIENT_CALL], null);
    _indictmentTypePanel.add(_indictmentsCheckBox[_MISALIGNMENT_CALL], null);
    _indictmentTypePanel.add(_indictmentsCheckBox[_COMPONENT_VOIDING_CALL], null);
    _indictmentTypePanel.add(_indictmentsCheckBox[_EXCESS_CALL], null);
    _indictmentTypePanel.add(_indictmentsCheckBox[_INDIVIDUAL_VOIDING_CALL], null); //@author Jack Hwee- individual void
    _indictmentTypePanel.add(_indictmentsCheckBox[_DUMMY_CALL], null); // Siew Yeng - XCR1745 - semi-automated mode

    // slice name combobox
    _dataSouthPanel = new JPanel();
    _dataSouthPanel.setLayout(new BorderLayout());
    _dataSouthPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    _dataSelectionPanel.add(_dataSouthPanel, BorderLayout.SOUTH);

    _sliceNamePanel = new JPanel();
    _sliceNamePanel.setLayout(new PairLayout(10, 0));
    _dataSouthPanel.add(_sliceNamePanel, BorderLayout.WEST);

    _sliceNameLabel = new JLabel();
    _sliceNameLabel.setText(StringLocalizer.keyToString("RDGUI_SLICE_NAME_LABEL_KEY"));
    _sliceNamePanel.add(_sliceNameLabel);

    _sliceNameComboBox = new JComboBox();
    _sliceNameComboBox.setPreferredSize(new Dimension(150, CommonUtil._COMBO_BOX_HEIGHT));
    _sliceNameComboBox.addActionListener(new ActionListener()
      { public void actionPerformed(ActionEvent e) {
        sliceNameComboBox_actionPerformed();
      }});
    _sliceNamePanel.add(_sliceNameComboBox);
  }

  /**
   * Sets up the Defect Information Panel
   * @author George Booth
   */
  private void setupDefectInformationPanel() {
    _defectInformationPanel = new JPanel();
    // no border
    _defectInformationPanel.setLayout(new BorderLayout());

    _pinCountBox = Box.createHorizontalBox();
    _defectInformationPanel.add(_pinCountBox, BorderLayout.NORTH);

    // number of tested pins
    _testedPinsLabel = new JLabel();
    _testedPinsLabel.setText(
      StringLocalizer.keyToString("RDGUI_TESTED_PINS_LABEL_KEY") + " 0");
    Font f = _testedPinsLabel.getFont();
    _boldFont = FontUtil.getBoldFont(f,Localization.getLocale());
    _testedPinsLabel.setFont(_boldFont);
    _pinCountBox.add(_testedPinsLabel);
    _pinCountBox.add(Box.createGlue());

    _pinsButtonGroup = new ButtonGroup();

    // number of passing pins
    _passingPinsRadioButton = new JRadioButton();
    _passingPinsRadioButton.setText(
      StringLocalizer.keyToString("RDGUI_PASSING_PINS_LABEL_KEY") + " 0");
    _passingPinsRadioButton.setFont(_boldFont);
    _passingPinsRadioButton.setSelected(_showPassingJoints);
    _passingPinsRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        passingPinsRadioButton_actionPerformed();
      }
    });
    _pinsButtonGroup.add(_passingPinsRadioButton);
    _pinCountBox.add(_passingPinsRadioButton);
    _pinCountBox.add(Box.createGlue());

    // number of defective pins
    _defectivePinsRadioButton = new JRadioButton();
    _defectivePinsRadioButton.setText(
      StringLocalizer.keyToString("RDGUI_FAILING_PINS_LABEL_KEY") + " 0");
    _defectivePinsRadioButton.setFont(_boldFont);
    _defectivePinsRadioButton.setSelected(!_showPassingJoints);
    _defectivePinsRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        defectivePinsRadioButton_actionPerformed();
      }
    });
    _pinsButtonGroup.add(_defectivePinsRadioButton);
    _pinCountBox.add(_defectivePinsRadioButton);

    // Pin Information table
    _jointInformationTableModel = new JointInformationTableModel();
    _jointInformationTable = new JointInformationTable(_jointInformationTableModel);
    _jointInformationTable.setRowSelectionAllowed(true);
    _jointInformationTable.setColumnSelectionAllowed(false);
    _jointInformationTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _jointInformationTableScrollPane = new JScrollPane();
    _jointInformationTableScrollPane.getViewport().add(_jointInformationTable);
    _jointInformationTable.addMouseListener(new MouseAdapter()
    {
      // select subtype on double-click
      public void mouseClicked(MouseEvent e)
      {
        int clickCount = e.getClickCount();
        if (clickCount == 2 && e.isPopupTrigger() == false)
        {
          int selectedRowIndex = _jointInformationTable.rowAtPoint(e.getPoint());
          String subtypeName = (String)_jointInformationTableModel.getValueAt(selectedRowIndex, _jointInformationTableModel._SUBTYPE);
          // don't do the double click if the subypte is "mixed" -- can't pick just the one in this case (because there is more than one)
          if (subtypeName != StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY"))
            autoSelectSubtype();
        }
      }

      // show context menu on right click
      public void mouseReleased(MouseEvent e)
      {
        // if we're viewing by component/pin, the context menu makes no sense as we're already filtered
        // down to a subytpe
        if (_viewByJointType == false)
          return;
        if (e.isPopupTrigger())
        {
          handleJointInformationTablePopupMenu(e);
        }
      }
    });

    // Defect Details table
    _defectDetailsTableModel = new DefectDetailsTableModel();
    _defectDetailsTable = new JSortTable(_defectDetailsTableModel);
    _defectDetailsTable.setRowSelectionAllowed(true);
    _defectDetailsTable.setColumnSelectionAllowed(false);
    _defectDetailsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _defectDetailsTableScrollPane = new JScrollPane();
    _defectDetailsTableScrollPane.getViewport().add(_defectDetailsTable);

    //Ngie Xing, April 2014
    //highlight/select threshold in threshold table when a failed joint defect test in defectDetailsTable is selected
    _defectDetailsTable.addMouseListener(new MouseAdapter()
    {
      //select subtype on double-click
      public void mouseClicked(MouseEvent e)
      {
        int clickCount = e.getClickCount();
        if (clickCount == 2 && e.isPopupTrigger() == false)
        {
          int selectedDefectsTableRowIndex = _defectDetailsTable.getSelectedRow();
          IndictmentDetailsEntry jointDetails = _defectDetailsTableModel.getSelectedJointDetails(selectedDefectsTableRowIndex);

          if (jointDetails.getIndictmentType().equals(StringLocalizer.keyToString("RDGUI_FAILED_LABEL_KEY")))
          {
            int selectedJointInformationTableRowIndex = _jointInformationTable.getSelectedRow();
            String subtypeName = (String) _jointInformationTableModel.getValueAt(selectedJointInformationTableRowIndex, _jointInformationTableModel._SUBTYPE);
            
            //Ngie Xing, Sept 2014, removed checking for _selectedSubtype
            //from setCurrentTestSelection if Subtype Choice is All, _selectedSubtype is null 
//            if (_selectedSubtype == null)
//            {
              // don't do the double click if the subypte is "mixed" -- can't pick just the one in this case (because there is more than one)            
              if (subtypeName != StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY"))//_selectedSubtype==null)
              {
                autoSelectSubtype();
              }
//            }

            AlgorithmEnum algorithmEnum;
            //NOTE: will need to change the way of getting AlgorithmEnum in the future
            //1. The ideal way should be getting AlgorithmEnum from MeasurementResult directly
            //   algorithmEnum = meaurementResult.getAlgorithmEnum();
            //2. if Defect Call/IndicmentEnum is MISSING, the AlgorithmEnum seen will be MEASUREMENT
            //   this is wrong, it should be in OPEN instead, mapping seems to be correct
            //   so manually set AlgorithmEnum as OPEN for now
            //3. For Indictment:OPEN, SliceName:Pad, MeasurementEnum: Center to Heel Thickness Percent,
            //   The AlgorithmEnum obtained from MeasurementResult.getAlgorithmEnum() is MEASUREMENT (WRONG)
            //   This behaviour is similar to the above but is seen for this specific AlgorithmSetting only
            IndictmentEnum selectedIndictmentEnum = jointDetails.getIndictment();
            //use mapping instead?
            if (selectedIndictmentEnum.equals(IndictmentEnum.OPEN)
              || selectedIndictmentEnum.equals(IndictmentEnum.MISSING))
            {
              algorithmEnum = AlgorithmEnum.OPEN;
            }
            else if (selectedIndictmentEnum.equals(IndictmentEnum.MISALIGNMENT))
            {
              algorithmEnum = AlgorithmEnum.MISALIGNMENT;
            }
            else if (selectedIndictmentEnum.equals(IndictmentEnum.INDIVIDUAL_VOIDING)
              || selectedIndictmentEnum.equals(IndictmentEnum.COMPONENT_VOIDING))
            {
              algorithmEnum = AlgorithmEnum.VOIDING;
            }
            else if (selectedIndictmentEnum.equals(IndictmentEnum.INSUFFICIENT))
            {
              algorithmEnum = AlgorithmEnum.INSUFFICIENT;
            }
            else if (selectedIndictmentEnum.equals(IndictmentEnum.DUMMY))
            {
              return; //Do nothing if it's a Dummy defect
            }
            else
            {
              algorithmEnum = jointDetails.getMeasurement().getAlgorithmEnum();
            }

            //because of the behaviour above the a different AlgorithmEnum is manually set and passed to algoTunerGui
            //will only need to pass MeasurementResult after fix
            Pair<AlgorithmEnum, com.axi.v810.datastore.testResults.MeasurementResult> pair = new Pair<AlgorithmEnum, com.axi.v810.datastore.testResults.MeasurementResult>(algorithmEnum, jointDetails.getMeasurement());
            _guiObservable.stateChanged(pair, SelectionEventEnum.DEFECT_DETAILS_SELECTION);
          }
        }
      }
    });
    //Create a split pane with the two scroll panes in it.
    _defectInformationSplitPane = new JSplitPane(
      JSplitPane.VERTICAL_SPLIT,
      _jointInformationTableScrollPane,
      _defectDetailsTableScrollPane);
    // give the pin table more space by default
    _defectInformationSplitPane.setResizeWeight(_FAILING_PINS_RESIZE_WEIGHT);

    //Provide minimum sizes for the two components in the split pane
    _jointInformationTableScrollPane.setMinimumSize(
      new Dimension(100, _JOINT_INFORMATION_MINIMUM_HEIGHT));
    _defectDetailsTableScrollPane.setMinimumSize(
      new Dimension(100, _DEFECT_DETAILS_MINIMUM_HEIGHT));

    _defectInformationPanel.add(_defectInformationSplitPane, BorderLayout.CENTER);

    _useReviewMeasurementsLabel = new JLabel();
    _useReviewMeasurementsLabel.setText(StringLocalizer.keyToString("RDGUI_MORE_INFORMATION_KEY"));
    _useReviewMeasurementsLabel.setVisible(false);
    _defectInformationPanel.add(_useReviewMeasurementsLabel, BorderLayout.SOUTH);
  }

  /**
   * Sets up the Defect Control Panel
   * @author George Booth
   */
  private void setupReviewDefectsMenus()
  {
    _deleteSelectedInspectionRunsMenuItem = new JMenuItem();
    _deleteSelectedInspectionRunsMenuItem.setText(StringLocalizer.keyToString("RDGUI_DELETE_INSPECTION_RUNS_MENU_KEY"));
    _deleteSelectedInspectionRunsMenuItem.setEnabled(false);
    _deleteSelectedInspectionRunsMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _inspectionRunList.deleteSelectedInspectionRuns();
      }
    });

    _generateReportMenuItem = new JMenuItem();
    _generateReportMenuItem.setText(StringLocalizer.keyToString("RDGUI_GENERATE_REPORT_MENU_KEY"));
    _generateReportMenuItem.setEnabled(false);
    _generateReportMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        generateReport_actionPerformed();
      }
    });

    _generateReportButton = new JButton();
//    _generateReportButton.setText(StringLocalizer.keyToString("RDGUI_GENERATE_REPORT_BUTTON_KEY"));
    _generateReportButton.setIcon(Image5DX.getImageIcon(Image5DX.GENERATE_REPORT));
    _generateReportButton.setToolTipText(StringLocalizer.keyToString("RDGUI_GENERATE_REPORT_BUTTON_TOOLTIP_KEY"));
    _generateReportButton.setEnabled(false);
    _generateReportButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        generateReport_actionPerformed();
      }
    });

    _optionsMenu = new JMenu();
    _optionsMenu.setText(StringLocalizer.keyToString("GUI_OPTIONS_MENU_KEY"));
    _focusControlMenuItem = new JMenuItem();
    _focusControlMenuItem.setText(StringLocalizer.keyToString("ATGUI_ADJUST_FOCUS_MENU_KEY"));
    _focusControlMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
    _focusControlMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _testDev.configureFocusControl_actionPerformed();
      }
    });   

    //Wong Ngie Xing
    //Added Menu Item in order to call Start Test from ReviewDefectsPanel
    _startTestMenuItem.setText(StringLocalizer.keyToString("ATGUI_START_TEST_MENU_KEY"));
    _startTestMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
    _startTestMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        startTestFromOtherPanel();
      }
    });
    _runMenu.setActionCommand(StringLocalizer.keyToString("ATGUI_RUN_MENU_KEY"));
    _runMenu.setText(StringLocalizer.keyToString("ATGUI_RUN_MENU_KEY"));
  }
  
  /*
   * @author Wong Ngie Xing
   */
  private void startTestFromOtherPanel()
  {
    _guiObservable.stateChanged(this, GuiUpdateEventEnum.TUNER_INSPECTION_START_FROM_OTHER_PANEL);
  }

  /**
   * @author George Booth
   */
  private void getPersistanceSettings()
  {
    _tunerPersistSettings = _testDev.getPersistance();
    _projectPersistSettings = _testDev.getPersistance().getProjectPersistance();

    _showPassingJoints = _projectPersistSettings.getPassingPinInformationFlag();

    _passingPinsResizeDivider = _tunerPersistSettings.getPassingPinsResizeDivider();

    _failingPinsResizeDivider = _tunerPersistSettings.getFailingPinsResizeDivider();
    // set divider to last location
    if (_showPassingJoints)
    {
      _passingPinsRadioButton.setSelected(true);
      if (_passingPinsResizeDivider != -1)
      {
        _defectInformationSplitPane.setDividerLocation(_passingPinsResizeDivider);
      }
    }
    else
    {
      _defectivePinsRadioButton.setSelected(true);
      if (_failingPinsResizeDivider != -1)
      {
        _defectInformationSplitPane.setDividerLocation(_failingPinsResizeDivider);
      }
    }

    _reviewDefectsResizeDivider = _tunerPersistSettings.getReviewDefectsResizeDivider();
    if (_reviewDefectsResizeDivider != -1)
    {
      _reviewDefectsSplitPane.setDividerLocation(_reviewDefectsResizeDivider);
    }
  }

  /**
   * Update the Review Defects section of algo tuner persistance
   * @author George Booth
   */
  void updatePersistanceSettings()
  {
    // get current location depending on what's shown
    if (_showPassingJoints)
    {
      _passingPinsResizeDivider = _defectInformationSplitPane.getDividerLocation();
    }
    else
    {
      _failingPinsResizeDivider = _defectInformationSplitPane.getDividerLocation();
    }
    _projectPersistSettings.setPassingPinInformationFlag(_showPassingJoints);
    _tunerPersistSettings.setPassingPinsResizeDivider(_passingPinsResizeDivider);
    _tunerPersistSettings.setFailingPinsResizeDivider(_failingPinsResizeDivider);

    _reviewDefectsResizeDivider = _reviewDefectsSplitPane.getDividerLocation();
    _tunerPersistSettings.setReviewDefectsResizeDivider(_reviewDefectsResizeDivider);
  }

  /**
   * Review Defects is starting
   * @author George Booth
   */
  void start()
  {
//    System.out.println("\nReviewDefectsPanel.start()");

    _reviewDefectsActive = true;

    // get menu and toolbar references and requester ID
    _menuBar = _mainUI.getMainMenuBar();
    _menuRequesterID = _menuBar.startMenuAdd();

    _envToolBar = _mainUI.getEnvToolBar();
    _envToolBarRequesterID = _envToolBar.startToolBarAdd();

    // add menu items
    JMenu editMenu = _menuBar.getEditMenu();
    _menuBar.addMenuItem(_menuRequesterID, editMenu, _deleteSelectedInspectionRunsMenuItem);

    JMenu toolsMenu = _menuBar.getToolsMenu();
    _menuBar.addMenuItem(_menuRequesterID, toolsMenu, _generateReportMenuItem);

    //Wong Ngie Xing
    _menuBar.addMenu(_menuRequesterID, 5, _runMenu);
    _menuBar.addMenuItem(_menuRequesterID, _runMenu, _startTestMenuItem);

    // add an Options menu, with Adjust Focus underneath. . .
    // new options menu (after run)
//    _menuBar.addMenu(_menuRequesterID, 5, _optionsMenu);
//    _menuBar.addMenuItem(_menuRequesterID, _optionsMenu, _focusControlMenuItem);

    // add toolbar buttons
    _envToolBar.removeDefectPackagerButtons();
    _envToolBar.addToolBarComponent(_envToolBarRequesterID, _generateReportButton);
    _envToolBar.addDefectPackagerButtons();

    // show image pane
    _testDev.showImageWindow();

    //Siew Yeng - XCR1745 - Semi-Automated mode
    if(TestExecution.getInstance().isSemiAutomatedModeEnabled())
    {
      _indictmentsCheckBox[_DUMMY_CALL].setSelected(true);
      _indictmentsCheckBox[_DUMMY_CALL].setVisible(true);
    }
    else
    {
      _indictmentsCheckBox[_DUMMY_CALL].setSelected(false);
      _indictmentsCheckBox[_DUMMY_CALL].setVisible(false);
    }
    
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(MainMenuGui.getInstance(),
            StringLocalizer.keyToString("CFGUI_RESULTS_PANEL_TITLE_KEY"),
            StringLocalizer.keyToString("RDGUI_GENERATE_INSPECTION_RUNS_LIST_TITLE_KEY"),
            true);

    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog,this);
    SwingWorkerThread.getInstance2().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
    		getPersistanceSettings();
        }
        finally
        {
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          try
          {
            SwingUtils.invokeAndWait(new Runnable()
            {
              public void run()
              {
                busyCancelDialog.dispose();
              }
            });
          }
          catch (InterruptedException ie)
          {
            // do nothing
          }
          catch (java.lang.reflect.InvocationTargetException ite)
          {
            // do nothing
          }
        }
      }
    });
    busyCancelDialog.setVisible(true);
    populatePanelWithData();

    // wait for 1st selection event from InspectionRunList in update() to enable UI
//    System.out.println("  start() complete");
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isReadyToFinish()
  {
    // do not allow user to leave if in the middle of an inspection
    return true;
  }

  /**
   * Review Defects is finished
   * @author George Booth
   */
  void finish()
  {
//    System.out.println("ReviewDefectsPanel.finish()");
    _menuBar.undoMenuChanges(_menuRequesterID);
    _envToolBar.undoToolBarChanges(_envToolBarRequesterID);

    updatePersistanceSettings();

//    _inspectionEventObservable.deleteObserver(this);

    if (_jointInformationTableModel != null)
      _jointInformationTableModel.clear();
    
    _reviewDefectsActive = false;
    _reviewDefectsReady = false;
    _shouldInteractWithGraphics = false;
  }

  /**
   * @param e 
   */
  private void handleJointInformationTablePopupMenu(MouseEvent e)
  {
    final int selectedRowIndex = _jointInformationTable.rowAtPoint(e.getPoint());
    if (selectedRowIndex == -1)
    {
      // user dragged cursor out of the table
      return;
    }

    // show the popup menu
    if (selectedRowIndex >= 0)
    {
//      _jointInformationTable.setRowSelectionInterval(selectedRowIndex, selectedRowIndex);
      JPopupMenu popupMenu = new JPopupMenu();
      JMenu setSubtypeMenu = new JMenu(StringLocalizer.keyToString("RDGUI_SELECT_SUBTYPE_MAIN_MENU_KEY"));
      JMenuItem switchSubtypeMenuItem = new JMenuItem(StringLocalizer.keyToString("RDGUI_SELECT_SUBTYPE_MENU_KEY"));
      JMenuItem switchBackSubtypeMenuItem = new JMenuItem(StringLocalizer.keyToString("RDGUI_SELECT_ALL_RESULTS_MENU_KEY"));
      JMenuItem changeSubtypeMenuItem = new JMenuItem(StringLocalizer.keyToString("RDGUI_CHANGE_SUBTYPE_MENU_KEY"));
      JMenuItem addComponentJointMenuItem = new JMenuItem(StringLocalizer.keyToString("RDGUI_ADD_COMPONENT_DEFECT_JOINT_MENU_KEY"));
      
      // Wei Chin : XCR 1665 
      JMenu testType = new JMenu(StringLocalizer.keyToString("RDGUI_SELECT_TEST_TYPE_MAIN_MENU_KEY"));
      JMenuItem setToNoLoadMenuItem = new JMenuItem(StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOLOAD_KEY"));
      JMenuItem setToNoTestMenuItem = new JMenuItem(StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOTEST_KEY"));
      JMenuItem setRestoreSubtypeMenuItem = new JMenuItem(StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_RESTORE_KEY"));
      
      //Kee Chin Seong - To ease Programmer, Focusing also is needed here to save time to find the Modify subtype component
      //                 and set focusing method
      JMenu focusType = new JMenu(StringLocalizer.keyToString("RDGUI_SELECT_FOCUS_TYPE_MAIN_MENU_KEY"));
      JMenuItem autoFocusMenuItem = new JMenuItem(StringLocalizer.keyToString("MMGUI_GLOBAL_SURFACE_MODEL_USE_AF_KEY"));
      JMenuItem gsmMenuItem = new JMenuItem(StringLocalizer.keyToString("MMGUI_GLOBAL_SURFACE_MODEL_USE_GSM_KEY"));
      JMenuItem rfpMenuItem = new JMenuItem(StringLocalizer.keyToString("MMGUI_GLOBAL_SURFACE_MODEL_USE_RFB_KEY"));
            
      switchSubtypeMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          autoSelectSubtype();
        }
      });
      switchBackSubtypeMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          // this reverts back to All-All (the Choice objects with no param is this choice)
          TunerTestState tunerTestState = new TunerTestState(new JointTypeChoice(), new SubtypeChoice());
          _guiObservable.stateChanged(tunerTestState, SelectionEventEnum.SUBTYPE_SELECTION);
        }
      });
      
      addComponentJointMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          addComponentJointMenuItemActionPerformed(e);
        }
      });
      changeSubtypeMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          changeSubtypeMenuItemActionPerformed(e);
        }
      });
      
      autoFocusMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setComponentFocusingMethodTypeProcess(GlobalSurfaceModelEnum.AUTOFOCUS);
        }
      });
      gsmMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setComponentFocusingMethodTypeProcess(GlobalSurfaceModelEnum.GLOBAL_SURFACE_MODEL);
        }
      });
      rfpMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setComponentFocusingMethodTypeProcess(GlobalSurfaceModelEnum.USE_RFB);
        }
      });
      
      setToNoLoadMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setLoadStatusActionPerformed(false);
        }
      });
      
      setToNoTestMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setTestStatusActionPerformed(false);
        }
      });
      
      setRestoreSubtypeMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setTestStatusActionPerformed(true);
          setLoadStatusActionPerformed(true);
        }
      });
      
      String subtypeName = (String)_jointInformationTableModel.getValueAt(selectedRowIndex, _jointInformationTableModel._SUBTYPE);
      String jointTypeName = (String)_jointInformationTableModel.getValueAt(selectedRowIndex, _jointInformationTableModel._JOINT_TYPE);
      if (subtypeName.equalsIgnoreCase(StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY")) ||
          jointTypeName.equalsIgnoreCase(StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY")))
      {
        switchSubtypeMenuItem.setEnabled(false); // disable this menu if the subtype is "mixed"
        changeSubtypeMenuItem.setEnabled(false);
      }
      popupMenu.add(changeSubtypeMenuItem);
      popupMenu.addSeparator();
      popupMenu.add(addComponentJointMenuItem);
      
      setSubtypeMenu.add(switchSubtypeMenuItem);
      setSubtypeMenu.add(changeSubtypeMenuItem);
      popupMenu.add(switchBackSubtypeMenuItem);
      popupMenu.addSeparator();
      popupMenu.add(setSubtypeMenu);
      
      //Kee Chin Seong - for Test Type Menu
      testType.add(setToNoLoadMenuItem);
      testType.add(setToNoTestMenuItem);
      popupMenu.addSeparator();
      popupMenu.add(testType);
      
      //Kee Chin Seong - For Focus Method Menu
      focusType.add(autoFocusMenuItem);
      focusType.add(gsmMenuItem);
      focusType.add(rfpMenuItem);
      popupMenu.addSeparator();
      popupMenu.add(focusType);
      
      List<PadType> padTypes = getSelectedPadTypes();
      int sizeOfPadtypes = padTypes.size();
      boolean differentSubtype = false;
      boolean hasNoTestPad = false;
      boolean hasNoLoadComponent = false;
      for(PadType padtype : padTypes)
      {
        if(padtype.getComponentType().isLoaded() == false)
        {
          hasNoLoadComponent = true;
          break;
        }
        if(padtype.isInspected() == false)
        {
          hasNoTestPad = true;
          break;
        }
        if(sizeOfPadtypes == 1)
        {
          if(padtype.getSubtype().toString().equals(subtypeName) == false)
          {
            differentSubtype = true;
          }
        }
      }
      if(hasNoLoadComponent)
      {
        setToNoLoadMenuItem.setEnabled(false);
        setToNoTestMenuItem.setEnabled(false);
        changeSubtypeMenuItem.setEnabled(false);
        testType.add(setRestoreSubtypeMenuItem);
      }
      else if(hasNoTestPad)
      {
        setToNoTestMenuItem.setEnabled(false);
        changeSubtypeMenuItem.setEnabled(false);
        testType.add(setRestoreSubtypeMenuItem);
      }
      else if(differentSubtype)
      {
        changeSubtypeMenuItem.setEnabled(false);
      }
      padTypes.clear();
      padTypes = null;

      popupMenu.show(e.getComponent(), e.getX(), e.getY());
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void autoSelectSubtype()
  {
    // only one selection is allowed
    // find the jointType / subtype so we can switch to them
    int selectedRowIndex = _jointInformationTable.getSelectedRow();
    if(selectedRowIndex < 0)
      return;
    
    String boardName = (String)_jointInformationTableModel.getValueAt(selectedRowIndex, _jointInformationTableModel._BOARD_NAME);
    String jointTypeName = (String)_jointInformationTableModel.getValueAt(selectedRowIndex, _jointInformationTableModel._JOINT_TYPE);
    String subtypeName = (String)_jointInformationTableModel.getValueAt(selectedRowIndex, _jointInformationTableModel._SUBTYPE);
    JointTypeEnum jointTypeEnum = JointTypeEnum.getJointTypeEnum(jointTypeName);
    List<Subtype> subtypes = Project.getCurrentlyLoadedProject().getPanel().getBoard(boardName).getSubtypes(jointTypeEnum);
    Subtype subtype = null;
    for (Subtype s : subtypes)
    {
      if (s.getShortName().equalsIgnoreCase(subtypeName))
      {
        subtype = s;
        break;
      }
    }
    
    ///Kee Chin Seong - If Subtype is null means the subtype has no exist anymore. 
    ///                 Prompt user to let them know
    if(subtype != null)
    {
      TunerTestState tunerTestState = new TunerTestState(new JointTypeChoice(jointTypeEnum), new SubtypeChoice(subtype));
      _guiObservable.stateChanged(tunerTestState, SelectionEventEnum.SUBTYPE_SELECTION);
    }
    else
    {
      MessageDialog.showErrorDialog(this, StringLocalizer.keyToString("RMGUI_SUBTYPE_NOT_EXIST_ERROR_KEY"), StringLocalizer.keyToString("RMGUI_CHANGE_SUBTYPE_DIALOG_KEY"), true);
    }
  }
  
  /**
   * @param e 
   * @author Wei Chin
   */
  private void changeSubtypeMenuItemActionPerformed(ActionEvent e)
  {
    if(_changeSubtypeDialog == null)
    {
      _changeSubtypeDialog = new ChangeSubtypeDialog(_mainUI, StringLocalizer.keyToString("RMGUI_CHANGE_SUBTYPE_DIALOG_KEY"));
      SwingUtils.centerOnComponent(_changeSubtypeDialog, _mainUI);
    }
    
    Project currentProject = Project.getCurrentlyLoadedProject();
    String boardName = "";
    String jointTypeName = "";
    String subtypeName = "";
    int selectedRowIndex = _jointInformationTable.getSelectedRow();
    boardName = (String)_jointInformationTableModel.getValueAt(selectedRowIndex, _jointInformationTableModel._BOARD_NAME);
    jointTypeName = (String)_jointInformationTableModel.getValueAt(selectedRowIndex, _jointInformationTableModel._JOINT_TYPE);
    subtypeName = (String)_jointInformationTableModel.getValueAt(selectedRowIndex, _jointInformationTableModel._SUBTYPE);
    List<PadType> padTypes = getSelectedPadTypes();

    JointTypeEnum jointTypeEnum = JointTypeEnum.getJointTypeEnum(jointTypeName);
    List<Subtype> subtypes = currentProject.getPanel().getBoard(boardName).getSubtypes(jointTypeEnum);
    Subtype subtype = null;
    for (Subtype s : subtypes)
    {
      if (s.getShortName().equalsIgnoreCase(subtypeName))
      {
        subtype = s;
        break;
      }
    }
    if(subtype != null)
    {
      _changeSubtypeDialog.populateData(currentProject, padTypes, subtype);
      _changeSubtypeDialog.setVisible(true);
      if(_changeSubtypeDialog.changeIsComplete())
      {
        // do nothing
      }
    }
    else
    {
      MessageDialog.showErrorDialog(this, StringLocalizer.keyToString("RMGUI_SUBTYPE_NOT_EXIST_ERROR_KEY"), StringLocalizer.keyToString("RMGUI_CHANGE_SUBTYPE_DIALOG_KEY"), true);
    }
    padTypes.clear();
    padTypes = null;
  }
  
  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  private void addComponentJointMenuItemActionPerformed(ActionEvent e)
  {
    int selectedRowIndex = _jointInformationTable.getSelectedRow();
    if (selectedRowIndex < 0)
    {
      return;
    }

    String boardName = (String) _jointInformationTableModel.getValueAt(selectedRowIndex, _jointInformationTableModel._BOARD_NAME);
    String jointName = (String) _jointInformationTableModel.getValueAt(selectedRowIndex, _jointInformationTableModel._PIN_NAME);
    String componentName = (String) _jointInformationTableModel.getValueAt(selectedRowIndex, _jointInformationTableModel._COMPONENT_NAME);

    String boardNameComponentNameJointName = boardName + "," + componentName + "," + jointName;
    try
    {
      MeasurementJointHighlightSettingsReader reader = new MeasurementJointHighlightSettingsReader(_projectName);
      List<String> defectiveComponentList = reader.getDefectiveSetting();

      if (defectiveComponentList.contains(boardNameComponentNameJointName) == false)
      {
        defectiveComponentList.add(boardNameComponentNameJointName);
      }

      MeasurementJointHighlightSettingsWriter writter = new MeasurementJointHighlightSettingsWriter(_projectName);
      writter.saveSettings(defectiveComponentList);
    }
    catch (DatastoreException ex)
    {
      MessageDialog.showErrorDialog(_mainUI,
        ex.getLocalizedMessage(),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    }
    catch (BusinessException x)
    {
      MessageDialog.showErrorDialog(_mainUI,
        x.getLocalizedMessage(),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    }
        
    _algoTunerGui.repopulateChart();
    
    
  }
  /**
   * @author Wei Chin
   */
  private void setLoadStatusActionPerformed(final boolean loadStatus)
  {
     // set the pads  to no test
    final List<ComponentType> componentTypes = getSelectedComponentTypes();
    final int componentTypesSize = componentTypes.size();
    final ProgressDialog progressDialog = createChangeTestProgressDialog(componentTypesSize);
    
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
         int i = 1;
        _commandManager.beginCommandBlock(StringLocalizer.keyToString("MMGUI_NO_LOAD_GROUP_UNDO_KEY"));
          for(ComponentType componentType :componentTypes)
          {
            try
            {
              _commandManager.execute(new ComponentTypeSetLoadedCommand(componentType, loadStatus));
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                            ex.getLocalizedMessage(),
                                            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                            true);
            }
            finally
            {
              if(componentTypesSize > _NUM_JOINTS_TO_UPDATE_POP_UP_PROGRESS_BAR)
              {
                progressDialog.updateProgressBar(i++);
              }
            }
          }
          componentTypes.clear();
        _commandManager.endCommandBlock();
      }
    });
    
    if(componentTypesSize > _NUM_JOINTS_TO_UPDATE_POP_UP_PROGRESS_BAR)
    {
      progressDialog.setVisible(true);
    }
  }
  
  /**
   * @author Wei Chin
   */
  private void setTestStatusActionPerformed(final boolean testStatus)
  {
    // set the pads  to no test
    final List<PadType> padTypes = getSelectedPadTypes();
    final int padSize = padTypes.size();
    final ProgressDialog progressDialog = createChangeTestProgressDialog(padSize);
    
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        _commandManager.beginCommandBlock(StringLocalizer.keyToString("MMGUI_NO_TEST_GROUP_UNDO_KEY"));
        int i = 1;
        for(PadType padType :padTypes)
        {
          try
          {
            if(ImageAnalysis.isSpecialTwoPinComponent(padType.getJointTypeEnum()) == false)
            {
              _commandManager.execute(new PadTypeSetTestedCommand(padType, testStatus));
            }
            else
            {
              _commandManager.execute(new ComponentTypeSetTestedCommand(padType.getComponentType(), testStatus));
            }
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
          }
          finally
          {
            if(padSize> _NUM_JOINTS_TO_UPDATE_POP_UP_PROGRESS_BAR)
            {
              progressDialog.updateProgressBar(i++);
            }            
          }
        }
        padTypes.clear();
        _commandManager.endCommandBlock();
      }      
    });
    if(padSize > _NUM_JOINTS_TO_UPDATE_POP_UP_PROGRESS_BAR)
    {
      progressDialog.setVisible(true);
    }
//    padTypes.clear();
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  private List<PadType> getSelectedPadTypes()
  {
    List<PadType> padTypes = new ArrayList();
    // only one selection is allowed
    // find the jointType / subtype so we can switch to them
    
    Project currentProject = Project.getCurrentlyLoadedProject();
      
    String boardName = "";
    String jointTypeName = "";
    for(int index : _jointInformationTable.getSelectedRows())
    {
      boolean isSpecialTwoPinComponent = false;
      boardName = (String)_jointInformationTableModel.getValueAt(index, _jointInformationTableModel._BOARD_NAME);
      jointTypeName = (String)_jointInformationTableModel.getValueAt(index, _jointInformationTableModel._JOINT_TYPE);
      String refDes = (String)_jointInformationTableModel.getValueAt(index, _jointInformationTableModel._COMPONENT_NAME);
      String padName = (String)_jointInformationTableModel.getValueAt(index, _jointInformationTableModel._PIN_NAME);
      
      // Kok Chun, Tan - XCR3486
      // It is possible to get a mixed joint type from joint information table model. Direct get from component type.
      for (JointTypeEnum jointTypeEnum : currentProject.getPanel().getBoard(boardName).getComponentType(refDes).getJointTypeEnums())
      {
        if (ImageAnalysis.isSpecialTwoPinComponent(jointTypeEnum))
        {
          isSpecialTwoPinComponent = true;
          break;
        }
      }

      if(padName.equals( StringLocalizer.keyToString("RMGUI_COMPONENT_LEVEL_KEY")) || isSpecialTwoPinComponent)
      {
        padTypes.addAll(currentProject.getPanel().getBoard(boardName).getComponentType(refDes).getPadTypes());
      }
      else
      {
        PadType padType = currentProject.getPanel().getBoard(boardName).getPadType(refDes, padName);
        if(padTypes.contains(padType) == false)
          padTypes.add(padType);
      }
    }
    return padTypes;
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  private List<ComponentType> getSelectedComponentTypes()
  {
    List<ComponentType> componentTypes = new ArrayList();
    // only one selection is allowed
    // find the jointType / subtype so we can switch to them
    
    Project currentProject = Project.getCurrentlyLoadedProject();
      
    String boardName = "";
    for(int index : _jointInformationTable.getSelectedRows())
    {
      boardName = (String)_jointInformationTableModel.getValueAt(index, _jointInformationTableModel._BOARD_NAME);
      String refDes = (String)_jointInformationTableModel.getValueAt(index, _jointInformationTableModel._COMPONENT_NAME);
      ComponentType currentComponentType = currentProject.getPanel().getBoard(boardName).getComponentType(refDes);
      if(componentTypes.contains(currentComponentType) == false)
        componentTypes.add(currentComponentType);
    }
    return componentTypes;
  }
  
  /*
   * @author Kee Chin Seong - Select AutoFocus / GSM / RFP Focusing Method
   */
  private void setComponentFocusingMethodTypeProcess(final GlobalSurfaceModelEnum focusMethod)
  {
    // set the pads  to no test
    final List<ComponentType> componentTypes = getSelectedComponentTypes();
    final int componentTypesSize = componentTypes.size();
    final ProgressDialog progressDialog = createChangeTestProgressDialog(componentTypesSize);

    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        int i = 1;
        try
        {
          _commandManager.beginCommandBlock("MMGUI_NO_TEST_GROUP_UNDO_KEY");
          for (ComponentType componentType : componentTypes)
          {
            try
            {
              _commandManager.execute(new ComponentTypeSetGlobalSurfaceModelCommand(componentType, focusMethod));
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                ex.getLocalizedMessage(),
                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                true);
            }
            finally
            {
              if (componentTypesSize > _NUM_JOINTS_TO_UPDATE_POP_UP_PROGRESS_BAR)
              {
                progressDialog.updateProgressBar(i++);
              }
            }
          }
          _commandManager.endCommandBlock();
        }
        finally
        {
          componentTypes.clear();
        }
      }
    });
    if (componentTypesSize > _NUM_JOINTS_TO_UPDATE_POP_UP_PROGRESS_BAR)
    {
      progressDialog.setVisible(true);
    }
  }
  
  /**
   * @author Wei Chin, Chong
   */
  private ProgressDialog createChangeTestProgressDialog( int numberOfPads)
  {
    ProgressDialog progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                         StringLocalizer.keyToString("RMGUI_UPDATE_TEST_STATUS_KEY"),
                                         StringLocalizer.keyToString("RMGUI_UPDATE_WAITING_KEY"),
                                         StringLocalizer.keyToString("RMGUI_UPDATE_COMPLETE_KEY"),
                                         StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                         StringLocalizer.keyToString("ATGUI_JOINT_KEY"),
                                         0,
                                         numberOfPads,
                                         true);
    progressDialog.pack();
    SwingUtils.centerOnComponent(progressDialog, this);
    return progressDialog;
  }
}
