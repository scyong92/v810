package com.axi.v810.gui.algoTuner;

import java.util.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.MaskImageCompatibilityCheck;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.mainMenu.*;

/**
 * Description: Contains the data for the panel layout table.
 * @author Carli Connally
 */
class MaskImageStatusTableModel extends AbstractTableModel
{
  private MainMenuGui _mainUI;
  //String labels retrieved from the localization properties file
  private static final String _SUBTYPE_INDEX = "No.";
  private static final String _SUBTYPE_NAME = "Subtype Name";
  private static final String _MASK_IMAGE_RESOLUTION_SIZE = "Mask Image Resolution";
  private static final String _MASK_IMAGE_STATUS = "Mask Image Usable";
  
  private static final String _MASK_IMAGE_USABLE = "Usable";
  private static final String _MASK_IMAGE_NOT_USABLE = "Not Usable";
 
  private String[] _columnLabels = { _SUBTYPE_INDEX,
                                     _SUBTYPE_NAME,
                                     _MASK_IMAGE_RESOLUTION_SIZE,                                
                                     _MASK_IMAGE_STATUS
                                   };

  private Project _project = null;
  private Subtype _subtype = null;
  private List<Subtype> _subtypes;
  private Map<String, Subtype> _subSubtypeNameToParentMap = new TreeMap<String, Subtype>();
 
  private static com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  /**
   * Constructor.
   * @author Carli Connally
   */
  public MaskImageStatusTableModel ()
  {
    _mainUI = MainMenuGui.getInstance();
    // do nothing
  }

  /**
   * @author Andy Mechtenberg
   */
  void clearProjectData()
  {
    _project = null;
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Carli Connally
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }
  
  /**
   * @author Andy Mechtenberg
   */
  public int getRowCount()
  {
    if (_subtypes == null)
      return 0;
    else
      return _subtypes.size();
  }

  /**
   * Overridden method.
   * @param column Index of the column
   * @return Column label for the column specified
   * @author Carli Connally
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @param columnIndex Index of the column
   * @return Class data type for the column specified
   * @author Carli Connally
 
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }    */

  /**
   * Overridden method.  Gets the values from the project object.
   * @param rowIndex Index of the row
   * @param columnIndex Index of the column
   * @return Object containing the value
   * @author Carli Connally
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_project != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;
    
    Subtype subtype = _subtypes.get(rowIndex);
 
     if (columnIndex == 0) // number
      return rowIndex + 1;
    else if (columnIndex == 1) // subytpe
    {
      String subtypeName = subtype.getShortName();
      //Siew Yeng - XCR-2764 - Sub-subtype feature
      if(subtype.isSubSubtype())
      {
        subtypeName = _subSubtypeNameToParentMap.get(subtype.getLongName()).getShortName() + ": " + subtypeName;
      }
        
      return subtypeName;  
    }
    else if (columnIndex == 2) // image dimension
    {
       DoubleCoordinate dimension = MaskImageCompatibilityCheck.getInstance().getSubtypeToImageResolutionBoolMap().get(subtype);

       if (dimension != null)
          return  (int)dimension.getX() + " x " + (int)dimension.getY();
       else
          return "";
    }
    else if (columnIndex == 3) // mask image usability
    {
       Map<Subtype, Boolean> mapEntry = MaskImageCompatibilityCheck.getInstance().getSubtypeToImageResolutionCompatibilityBoolMap();
       
       boolean bool = false;
       
       if (mapEntry.containsKey(subtype))
          bool = MaskImageCompatibilityCheck.getInstance().getSubtypeToImageResolutionCompatibilityBoolMap().get(subtype);
       else
          return "";

       if (bool == true)
          return _MASK_IMAGE_USABLE;
       else
       {
          DoubleCoordinate dimension = MaskImageCompatibilityCheck.getInstance().getSubtypeToImageResolutionMap().get(subtype);
          return _MASK_IMAGE_NOT_USABLE + "(" + (int)dimension.getX() + "x" + (int)dimension.getY() + ")";
       }
    }

    return null;
  }

  /**
   * Overridden method.
   * @param rowIndex  Index of the row
   * @param columnIndex  Index of the column
   * @return boolean - whether the cell is editable
   * @author Carli Connally
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    boolean isEditable = false;

    return isEditable;
  }

  /**
   * Initialize the table with data from a new project
   * @param project Project data
   * @author Carli Connally
   */
  public void setData(Project project, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(project != null);

    _project = project;
    
    _subtypes = new ArrayList();//_project.getPanel().getInspectedSubtypes(jointTypeEnum);
   
    //Siew Yeng - XCR-2764 - Sub-subtype feature
    for(Subtype subtype : _project.getPanel().getInspectedSubtypes(jointTypeEnum))
    {
      _subtypes.add(subtype);
      if(subtype.hasSubSubtypes())
      {
        for(Subtype subSubtype : subtype.getSubSubtypes())
        {
          _subtypes.add(subSubtype);
          if(_subSubtypeNameToParentMap.containsKey(subSubtype.getLongName()) == false)
            _subSubtypeNameToParentMap.put(subSubtype.getLongName(), subtype);
        }
      }
    }
    fireTableDataChanged();
  }
  
   /**
   * Initialize the table with data from a new project
   * @param project Project data
   * @author Carli Connally
   */
  public void setSubtype(Subtype subtype)
  {
    _subtypes.add(subtype);
  }
  
  /**
   * Initialize the table with data from a new project
   * @param project Project data
   * @author Carli Connally
   */
  public java.util.List<Subtype> getSubtypes()
  {
    return _subtypes;
  }
  
  /**
   * @author Siew Yeng
   */
  public Subtype getSubtype(int row)
  {
    Subtype subtype = _subtypes.get(row);
    
    if(subtype.isSubSubtype())
      return _subSubtypeNameToParentMap.get(subtype.getLongName());
    
    return subtype;
  }
  
   /**
   * Initialize the table with data from a new project
   * @param project Project data
   * @author Carli Connally
   */
  public void clearSubtypes()
  {
    if (_subtypes.isEmpty() == false)
        _subtypes.clear();
  }
  

  /**
   * Overridden method.  Saves the data entered by the user in the table
   * to the project object.
   * @param object Object containing the data to be saved to the model
   * @param rowIndex Index of the row
   * @param columnIndex Index of the column
   * @author Carli Connally
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Subtype subtype = _subtypes.get(rowIndex);

    switch(columnIndex)
    {
      case MaskImageStatusTable._SUBTYPE_INDEX:
      {
        break;
      }
      case MaskImageStatusTable._SUBTYPE_NAME:
      {
         break;
      }
      default:
      {
        Assert.expect(false);
      }
    }
  }
  
  /**
   * @author Siew Yeng
   */
  public void clear()
  {
    clearProjectData();
    _subtypes.clear();
    _subSubtypeNameToParentMap.clear();
  }
}
