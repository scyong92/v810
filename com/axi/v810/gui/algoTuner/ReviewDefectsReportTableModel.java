package com.axi.v810.gui.algoTuner;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;
import javax.swing.table.DefaultTableModel;
import com.axi.guiUtil.DefaultSortTableModel;

/**
 *
 * <p>Title: ReviewDefectsReportTableModel</p>
 *
 * <p>Description:  This class is a special table model for report generation. It will populate itself
 * with data that is to be put in the Review Defects report.  The data is a combination of the Joint
 * Information table and the Defect Details table. There will be one line per defect detail in the
 * table model.  This table model is then sent to the TableModelCSVReportWriter to be output into
 * a comma separated values (.csv) file for importing by Excel. There will be 13 columns:<br>
 * (from the Joint Information Table)<br>
 * Run ID<br>
 * Board Serial Number<br>
 * Board Name<br>
 * Component<br>
 * Pin<br>
 * Family<br>
 * Subtype<br>
 * Side<br>
 * Number of defects on the pin<br>
 * (from the Defect Details Table)<br>
 * Indictment Name<br>
 * Indictment Type (Passed, Failed, Related)<br>
 * Measurement Name<br>
 * Measurement Value<br>
 * Measurement Units<br>
 * Slice<br></p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

class ReviewDefectsReportTableModel extends DefaultSortTableModel
{
  private ArrayList<ReviewDefectsReportEntry> _jointResultDetails = null;

  private final String[] _reportColumnNames =
    {
      StringLocalizer.keyToString("RMGUI_RUN_ID_COLUMN_KEY"),
      StringLocalizer.keyToString("CFGUI_SERIAL_NUMBER_CONDITION_KEY"),
      StringLocalizer.keyToString("RDGUI_BOARD_NAME_COLUMN_KEY"),
      StringLocalizer.keyToString("RMGUI_COMPONENT_NAME_COLUMN_KEY"),
      StringLocalizer.keyToString("RMGUI_PIN_NAME_COLUMN_KEY"),
      StringLocalizer.keyToString("RDGUI_FAMILY_COLUMN_KEY"),
      StringLocalizer.keyToString("RDGUI_SUBTYPE_COLUMN_KEY"),
      StringLocalizer.keyToString("RDGUI_SIDE_COLUMN_KEY"),
      StringLocalizer.keyToString("RDGUI_NUM_DEFECT_CALLS_COLUMN_KEY"),
      StringLocalizer.keyToString("RDGUI_DEFECT_CALL_COLUMN_KEY"),
      StringLocalizer.keyToString("RDGUI_FAILED_RELATED_COLUMN_KEY"),
      StringLocalizer.keyToString("RDGUI_MEASUREMENT_COLUMN_KEY"),
      StringLocalizer.keyToString("RDGUI_VALUE_COLUMN_KEY"),
      StringLocalizer.keyToString("RDGUI_UNITS_COLUMN_KEY"),
      StringLocalizer.keyToString("RDGUI_SLICE_COLUMN_KEY")
    };

  /**
   * @author George Booth
   */
  public ReviewDefectsReportTableModel()
  {
    _jointResultDetails = new ArrayList<ReviewDefectsReportEntry>();
  }

  /**
   * @author George Booth
   */
  void addJointData(JointResultsEntry jointResultsEntry, IndictmentDetailsEntry indictmentDetailsEntry)
  {
    ReviewDefectsReportEntry reportEntry = new ReviewDefectsReportEntry(jointResultsEntry);
    reportEntry.setIndictmentDetails(indictmentDetailsEntry);
    _jointResultDetails.add(reportEntry);
  }

  /**
   * @author George Booth
   */
  public int getColumnCount()
  {
    return _reportColumnNames.length;
  }

  /**
   * @author George Booth
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_reportColumnNames[columnIndex];
  }

  /**
   * @author George Booth
   */
  public int getRowCount()
  {
    if (_jointResultDetails == null)
      return 0;
    else
      return _jointResultDetails.size();
  }

  /**
   * @author George Booth
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    ReviewDefectsReportEntry reportEntry = _jointResultDetails.get(rowIndex);

    switch (columnIndex)
    {
      case 0: return reportEntry.getRunId();
      case 1: return reportEntry.getBoardSerialNumber();
      case 2: return reportEntry.getBoardName();
      case 3: return reportEntry.getReferenceDesignator();
      case 4: return reportEntry.getPinName();
      case 5: return reportEntry.getJointType();
      case 6: return reportEntry.getSubtype();
      case 7: return reportEntry.getSide();
      case 8: return reportEntry.getNumDefects();
      case 9: return reportEntry.getIndictmentName();
      case 10: return reportEntry.getIndictmentType();
      case 11: return reportEntry.getMeasurementName();
      case 12: return reportEntry.getMeasurementValue();
      case 13: return reportEntry.getMeasurementUnits();
      case 14: return reportEntry.getSliceName();
      default:
        // this should not happen if so then assert
        Assert.expect(false);
        return null;
    }
  }

}
