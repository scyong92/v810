package com.axi.v810.gui.algoTuner;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.business.testProgram.*;

/**
 * This class tracks the state of the detailed measurement selection made
 * in ReviewDefectsPanel.  It is used by ReviewMeasurementsPanel to populate
 * similar measurements.
 * @author Rex Shang
 */
public class MeasurementSelection
{
  private String _subtypeName;
  private MeasurementEnum _measurementType;
  private SliceNameEnum _sliceName;
  private List<Long> _inspectionRuns;

  /**
   * @author Rex Shang
   */
  MeasurementSelection(List<Long> inspectionRuns, String subtypeName, MeasurementEnum measurementType, SliceNameEnum sliceName)
  {
    Assert.expect(inspectionRuns != null);
    Assert.expect(inspectionRuns.size() > 0);
    Assert.expect(subtypeName != null);
    Assert.expect(measurementType != null);
    Assert.expect(sliceName != null);

    _inspectionRuns = inspectionRuns;
    _subtypeName = subtypeName;
    _measurementType = measurementType;
    _sliceName = sliceName;
  }

  /**
   * @author Rex Shang
   */
  List<Long> getInspectionRuns()
  {
    Assert.expect(_inspectionRuns != null);
    Assert.expect(_inspectionRuns.size() > 0);
    return _inspectionRuns;
  }

  /**
   * @author Rex Shang
   */
  MeasurementEnum getMeasurementType()
  {
    Assert.expect(_measurementType != null);
    return _measurementType;
  }

  /**
   * @author Rex Shang
   */
  String getSubtypeName()
  {
    Assert.expect(_subtypeName != null);
    return _subtypeName;
  }

  /**
   * @author Rex Shang
   */
  SliceNameEnum getSliceName()
  {
    Assert.expect(_sliceName != null);
    return _sliceName;
  }

}
