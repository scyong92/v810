package com.axi.v810.gui.algoTuner;

import org.jfree.chart.labels.*;
import org.jfree.data.xy.*;

/**
 * This tool tip generator is used to populate the pop up message once the user
 * stopped mouse on a chart data point.
 * @author Rex Shang
 * @version 1.0
 */
class JointResultsEntryHistogramToolTipGenerator extends StandardXYToolTipGenerator
{
  private MeasurementValueFormat _format;

  /**
   * @author Rex Shang
   */
  JointResultsEntryHistogramToolTipGenerator()
  {
    _format = MeasurementValueFormat.getInstance();
  }

  /**
   * Generates the tool tip text for an item in a dataset.
   *
   * @param dataset  the dataset (<code>null</code> not permitted).
   * @param series  the series index (zero-based).
   * @param item  the item index (zero-based).
   *
   * @return The tooltip text (possibly <code>null</code>).
   */
  public String generateToolTip(XYDataset dataset, int series, int item)
  {
    JointResultsEntryHistogramDataSet histogramDataSet = (JointResultsEntryHistogramDataSet) dataset;

    String tooltip = "[" + _format.format(histogramDataSet.getStartXValue(series, item));
    tooltip += ", " + _format.format(histogramDataSet.getEndXValue(series, item)) + "]";
    tooltip += ": " + _format.format(histogramDataSet.getY(series, item));

    return tooltip;
  }
}
