package com.axi.v810.gui.algoTuner;

import java.util.*;

import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: JointInformationTableModel</p>
 *
 * <p>Description:  This class is the table model for the pin information table in Review Defects.
 * This table model is responsible for getting and filtering the data. There will be 8 columns:<br>
 * Run ID<br>
 * Board Name<br>
 * Component<br>
 * Pin<br>
 * Family<br>
 * Subtype<br>
 * Side<br>
 * Number of defects on the pin<br></p>
 *<p>The list of tested joint results will come from the Joint Results Manager. It is filtered
 * in various ways to create the table data.
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class JointInformationTableModel extends DefaultSortTableModel
{
  // The order listed here dictates the order of the table columns.
  public static final int _RUN_ID = 0;
  public static final int _BOARD_NAME = 1;
  public static final int _COMPONENT_NAME = 2;
  public static final int _PIN_NAME = 3;
  public static final int _JOINT_TYPE = 4;
  public static final int _SUBTYPE = 5;
  public static final int _SIDE = 6;
  public static final int _NUM_DEFECTS = 7;

  // the column names for this table
  private final String[] _jointInformationTableColumnNames = {
                       StringLocalizer.keyToString("RMGUI_RUN_ID_COLUMN_KEY"),
                       StringLocalizer.keyToString("RDGUI_BOARD_NAME_COLUMN_KEY"),
                       StringLocalizer.keyToString("RMGUI_COMPONENT_NAME_COLUMN_KEY"),
                       StringLocalizer.keyToString("RMGUI_PIN_NAME_COLUMN_KEY"),
                       StringLocalizer.keyToString("RDGUI_FAMILY_COLUMN_KEY"),
                       StringLocalizer.keyToString("RDGUI_SUBTYPE_COLUMN_KEY"),
                       StringLocalizer.keyToString("RDGUI_SIDE_COLUMN_KEY"),
                       StringLocalizer.keyToString("RDGUI_NUM_DEFECT_CALLS_COLUMN_KEY")};

  // default column width = 75.  Make sure sum of desired widths is <= 450
  // or new widths won't be accepted.  The odd sizes here are selected so that
  // a couple of characters of each colunm name are visible at 1024x768 resolution
  private final int[] _jointInformationTableColumnWidths = {
      70,   // Run ID
      57,   // Board Name
      70,   // Component
      40,   // Pin
      67,   // Family
      67,   // Subtype
      49,   // Side
      30    // # Calls
  };

  private final String _TOP_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_TOP_LABEL_KEY");
  private final String _BOTTOM_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_BOTTOM_LABEL_KEY");

  private JointResultsManager _jointResultsManager = JointResultsManager.getInstance();
  // Lists of all joints from last inspection run
  private List<JointResultsEntry> _allPassingJointResults;
  private List<JointResultsEntry> _allFailingJointResults;

  // Lists of joints from last inspection run filtered by board and comp/pad or joint/subtype
  private List<JointResultsEntry> _filteredPassingJointResults;
  private List<JointResultsEntry> _filteredFailingJointResults;

  // Lists of joints from last inspection run filtered by above and slice and indictments
  private List<JointResultsEntry> _displayedPassingJointResults;
  private List<JointResultsEntry> _displayedFailingJointResults;

  // This list is our data model
  private List<JointResultsEntry> _filteredJointResults;

  // Lists of passing/failing slice names
  private List<SliceNameEnum> _passingSliceNames;
  private List<SliceNameEnum> _failingSliceNames;

  private int _passingJointCount = 0;
  private int _failingJointCount = 0;

  // array lists for indictment counting
  private java.util.List<IndictmentEnum> _selectedIndictments = null;
  private ArrayList<IndictmentEnum> _indictmentArray = null;
  private int[] _jointIndictmentCount = null;
  private int[] _totalIndictmentCount = null;

  // default to sort by pin(^), component(^), board(^), run(V)
  private int _currentSortColumn = -1;
  private boolean _currentSortAscending = true;

  /**
   * @author George Booth
   */
  public JointInformationTableModel()
  {
    // init indictment counters
    List<IndictmentEnum> indictmentEnums = IndictmentEnum.getAllIndictmentEnums();
    _indictmentArray = new ArrayList<IndictmentEnum>(indictmentEnums);
    _jointIndictmentCount = new int[indictmentEnums.size()];
    _totalIndictmentCount = new int[indictmentEnums.size()];
    for (int i = 0; i < _indictmentArray.size(); i++)
    {
      _jointIndictmentCount[i] = 0;
      _totalIndictmentCount[i] = 0;
    }
  }

  /**
   * Get new joint results for the last inspection run
   * @author George Booth
   */
  public void updateJointResultsForNewInspectionRun(String projectName, List<Long> selectedInspectionRuns)
  {
    Assert.expect(projectName != null);

    _allPassingJointResults = new ArrayList<JointResultsEntry>();
    _allFailingJointResults = new ArrayList<JointResultsEntry>();

    List<JointResultsEntry> allResults =
      _jointResultsManager.getAllResultsFromRuns(projectName, selectedInspectionRuns);

    for (JointResultsEntry jointResultsEntry : allResults)
    {
      // only add component results if there was a component level test
      if (jointResultsEntry.isJointResult() ||
          jointResultsEntry.isComponentLevelTest())
      {
        if (jointResultsEntry.passed())
        {
          _allPassingJointResults.add(jointResultsEntry);
        }
        else
        {
          _allFailingJointResults.add(jointResultsEntry);
        }
      }
    }
    // release list of all joints
    allResults = null;
  }

  /**
   * Filter the passing and failing joint results based on current filter selection using
   * joint type and subtype along with boardName, slice and indictments.
   * Any null parameter implies "All"
   * @author George Booth
   */
  void updateJointResultsForNewFilterSelection(String boardName, JointTypeEnum jointType, Subtype subtype)
  {
    _passingJointCount = 0;
    _failingJointCount = 0;

    // filter for passing and failing to get joint counts
    _filteredPassingJointResults = getResultsForJointTypeAndSubtype(
      true, boardName, jointType, subtype);

    _filteredFailingJointResults = getResultsForJointTypeAndSubtype(
      false, boardName, jointType, subtype);
  }

  /**
    * Filter the passing and failing joint results based on current filter selection using
    * component type and padType along with boardName, slice and indictments.
    * Any null parameter implies "All"
    * @author George Booth
    */
   void updateJointResultsForNewFilterSelection(String boardName, ComponentType componentType, PadType padtype)
   {
     _passingJointCount = 0;
     _failingJointCount = 0;

     // filter for passing and failing to get joint counts
     _filteredPassingJointResults = getResultsForComponentTypeAndPad(
       true, boardName, componentType, padtype);

     _filteredFailingJointResults = getResultsForComponentTypeAndPad(
       false, boardName, componentType, padtype);
   }

   /**
     * Filter the passing and failing joint results based on current filter selection using
     * slice and indictments.
     * Any null parameter implies "All"
     * @author George Booth
     */
    void updateJointResultsForNewFilterSelection(SliceNameEnum slice, java.util.List<IndictmentEnum> selectedIndictments)
    {
      _passingJointCount = 0;
      _failingJointCount = 0;

      // filter for passing and failing to get joint counts
      _displayedPassingJointResults = getPassingResultsForSlice(slice);

      _displayedFailingJointResults = getFailingResultsForSliceAndIndictments(slice, selectedIndictments);
      // update with unique indictments
      for (JointResultsEntry jointResultsEntry : _displayedFailingJointResults)
      {
        int num = 0;
        num = getNumberOfUniqueIndictments(jointResultsEntry);
        jointResultsEntry.setNumberOfDefects(num);
      }
    }

  /**
   * Populate the table with filtered passing or failing joint results
   * @author George Booth
   */
  void populateWithFilteredJoints(boolean passing)
  {
    if (passing)
    {
      _filteredJointResults = _displayedPassingJointResults;
    }
    else
    {
      _filteredJointResults = _displayedFailingJointResults;
    }

    // presort by last user sort directive or default
    if (_filteredJointResults != null)
    {
//      Collections.sort(_filteredJointResults, new
//                       JointResultsEntryComparator(true, JointResultsEntryComparatorEnum.COMPONENT_NAME));
      if (_currentSortColumn > -1)
      {
        sortColumn(_currentSortColumn, _currentSortAscending);
      }
      else
      {
        // default to sort by pin(^), component(^), board(^), run(V)
        sortColumn(_PIN_NAME, true);
        sortColumn(_COMPONENT_NAME, true);
        sortColumn(_BOARD_NAME, true);
        sortColumn(_RUN_ID, false);
        // keep default until user changes it
        _currentSortColumn = -1;
        _currentSortAscending = true;
      }
    }

    fireTableDataChanged();
  }

  /**
   * Get pass/fail results based on boardName, joint type, subtype, slice and indictments
   * Any null parameter implies "all"
   * @author George Booth
   */
  public ArrayList<JointResultsEntry> getResultsForJointTypeAndSubtype(
      boolean passing,
      String boardName,
      JointTypeEnum jointType,
      Subtype subtype)
  {
    ArrayList<JointResultsEntry> filteredJointResults = new ArrayList<JointResultsEntry>();

    List<JointResultsEntry> selectedJointResultsEntries = _allFailingJointResults;
    if (passing)
    {
      selectedJointResultsEntries = _allPassingJointResults;
    }

    if (selectedJointResultsEntries != null)
    {
      for (JointResultsEntry jointResultsEntry : selectedJointResultsEntries)
      {
        if (boardName == null ||
            boardName.equals(jointResultsEntry.getBoardName()))
        {
          //Siew Yeng - XCR-2535 - Inconsistent display of Component Voiding Area at Measurement Type for mix Subtype
          if (jointType == null ||
              jointType.getName().equals(jointResultsEntry.getJointTypeName()) ||
             (jointResultsEntry.getJointTypeName().equals(JointResultsEntry._MIXED) && jointResultsEntry.getJointTypeEnums().contains(jointType)))
          {
            //XCR-3462, Software crashed after change joint type in a pad from mixed joint types. 
            if(jointResultsEntry.getSubtypeName().equalsIgnoreCase(JointResultsEntry._MIXED) && jointResultsEntry.getSubtypes().isEmpty())
              continue;

            if (subtype == null ||
                subtype.getShortName().equals(jointResultsEntry.getSubtypeName()) ||
               (jointResultsEntry.getSubtypeName().equals(JointResultsEntry._MIXED) && jointResultsEntry.getSubtypes().contains(subtype)))
            {
              filteredJointResults.add(jointResultsEntry);
            }
          }
        }
      }
    }
    return filteredJointResults;
  }

  /**
   * Get pass/fail results based on boardName, component type, padType, slice and indictments
   * Any null parameter implies "all"
   * @author George Booth
   */
  public ArrayList<JointResultsEntry> getResultsForComponentTypeAndPad(
      boolean passing,
      String boardName,
      ComponentType componentType,
      PadType padType)
  {
    ArrayList<JointResultsEntry> filteredJointResults = new ArrayList<JointResultsEntry>();

    List<JointResultsEntry> selectedJointResultsEntries = _allFailingJointResults;
    if (passing)
    {
      selectedJointResultsEntries = _allPassingJointResults;
    }

    if (selectedJointResultsEntries != null)
    {
      for (JointResultsEntry jointResultsEntry : selectedJointResultsEntries)
      {
        if (boardName == null ||
            boardName.equals(jointResultsEntry.getBoardName()))
        {
          if (componentType == null ||
              componentType.getReferenceDesignator().equals(jointResultsEntry.getReferenceDesignator()))
          {
            //XCR-3462, Software crashed after change joint type in a pad from mixed joint types. 
            if(jointResultsEntry.getSubtypeName().equalsIgnoreCase(JointResultsEntry._MIXED) && jointResultsEntry.getSubtypes().isEmpty())
              continue;
            
            //Siew Yeng - XCR-2535 - Inconsistent display of Component Voiding Area at Measurement Type for mix Subtype
            if (padType == null ||
                padType.getName().equals(jointResultsEntry.getPadName()) ||
                jointResultsEntry.getSubtypes().contains(padType.getSubtype()))
            {
              filteredJointResults.add(jointResultsEntry);
            }
          }
        }
      }
    }
    return filteredJointResults;
  }

  /**
   * Get passing results based on slice.
   * Any null parameter implies "all"
   * @author George Booth
   */
  public ArrayList<JointResultsEntry> getPassingResultsForSlice(SliceNameEnum slice)
  {
    ArrayList<JointResultsEntry> filteredJointResults = new ArrayList<JointResultsEntry>();

    if (_filteredPassingJointResults != null)
    {
      for (JointResultsEntry jointResultsEntry : _filteredPassingJointResults)
      {
        // only count and add joint once
        boolean jointCounted = false;
        boolean jointAdded = false;

        List<MeasurementResult> measurementResults = jointResultsEntry.getAllMeasurementResults();
        for (MeasurementResult measureResult : measurementResults)
        {
          if (slice == null ||
              slice.equals(measureResult.getSliceNameEnum()))
          {
            // count this joint only once
            if (jointCounted == false)
            {
              _passingJointCount++;
              jointCounted = true;
            }

            // add this joint only once
            if (jointAdded == false && jointResultsEntry.passed())
            {
              filteredJointResults.add(jointResultsEntry);
              jointAdded = true;
              break;
            }
          }
          if (jointAdded)
            break;
        }
      }
    }
    return filteredJointResults;
  }

  /**
   * Get failing results based on slice and selected indictments.
   * Any null parameter implies "all"
   * @author George Booth
   */
  public ArrayList<JointResultsEntry> getFailingResultsForSliceAndIndictments(
      SliceNameEnum slice,
      java.util.List<IndictmentEnum> indictments)
  {
    _selectedIndictments = indictments;

    ArrayList<JointResultsEntry> filteredJointResults = new ArrayList<JointResultsEntry>();

    if (_filteredFailingJointResults != null)
    {
      for (JointResultsEntry jointResultsEntry : _filteredFailingJointResults)
      {
        // only count and add joint once
        boolean jointCounted = false;
        boolean jointAdded = false;

        List<IndictmentResult> indictmentResults = jointResultsEntry.getIndictmentResults();
        for (IndictmentResult indictmentResult : indictmentResults)
        {
          if (_selectedIndictments == null ||
              _selectedIndictments.contains(indictmentResult.getIndictmentEnum()))
          {
            for (MeasurementResult measurementResult : indictmentResult.getFailingMeasurementResults())
            {
              if (slice == null ||
                  slice.equals(measurementResult.getSliceNameEnum()))
              {
                // count this joint only once
                if (jointCounted == false)
                {
                  _failingJointCount++;
                  jointCounted = true;
                }

                // add this joint only once
                if (jointAdded == false)
                {
                  filteredJointResults.add(jointResultsEntry);
                  jointAdded = true;
                  break;
                }
              }
              if (jointAdded)
                break;
            }
          }
        }
      }
    }
    return filteredJointResults;
  }

  /**
   * @author George Booth
   */
  int getPassingJointCount()
  {
    return _passingJointCount;
  }

  /**
   * @author George Booth
   */
  int getFailingJointCount()
  {
    return _failingJointCount;
  }

  /**
   * @author George Booth
   */
  int getTotalJointCount()
  {
    return _passingJointCount + _failingJointCount;
  }


  /**
   * get slices for passing and failing results filtered by boardName and joint/subtype or comp/pad
   * @author George Booth
   */
  public void updateSliceNameForNewFilterSelection()
  {
    _passingSliceNames = getSliceNames(_filteredPassingJointResults);
    _failingSliceNames = getSliceNames(_filteredFailingJointResults);
  }

  /**
   * get slices for results
   *
   * @author George Booth
   */
  List<SliceNameEnum> getSliceNames(List<JointResultsEntry> filteredJointResults)
  {
    List<SliceNameEnum> sliceNames = new ArrayList<SliceNameEnum>();

    if (filteredJointResults != null)
    {
      for (JointResultsEntry jointResultsEntry : filteredJointResults)
      {
        List<MeasurementResult> measurementResults = jointResultsEntry.getAllMeasurementResults();
        for (MeasurementResult measurementResult : measurementResults)
        {
          SliceNameEnum sliceNameEnum = measurementResult.getSliceNameEnum();
          if (sliceNames.contains(sliceNameEnum) == false)
          {
            sliceNames.add(sliceNameEnum);
          }
        }
      }
    }
    return sliceNames;
  }

  /**
   * get slices for passing or failing filtered results
   * @author George Booth
   */
  public List<SliceNameEnum> getSliceNames(boolean passing)
  {
    if (passing)
    {
      return _passingSliceNames;
    }
    else
    {
      return _failingSliceNames;
    }
  }

  /**
   * get indictment counts for failing results filtered by boardName and joint/subtype or comp/pad
   * @author George Booth
   */
  public void updateIndictmentCountForNewFilterSelection()
  {
    for (int i = 0; i < _indictmentArray.size(); i++)
    {
      _totalIndictmentCount[i] = 0;
    }

    // sum the unique indictment count per joint across all joints
    for (JointResultsEntry jointResultsEntry : _filteredFailingJointResults)
    {
      // ignore selected indictments
      getNumberOfIndictments(jointResultsEntry, null);

      // count the number of unique defects, not the total
      for (int i = 0; i < _indictmentArray.size(); i++)
      {
        if (_jointIndictmentCount[i] > 0)
        {
          _totalIndictmentCount[i]++;
          // set joint count to 0 to avoid looping excesively
          _jointIndictmentCount[i] = 0;
        }
      }
    }
  }

  /**
   * Returns count of an indictment for filtered joint results
   * @author George Booth
   */
  int getIndictmentCount(IndictmentEnum indictment)
  {
    int index = _indictmentArray.indexOf(indictment);
    return _totalIndictmentCount[index];
  }

  /**
   * Returns count of unique indictments for a joint result entry
   * @author George Booth
   */
  int getNumberOfUniqueIndictments(JointResultsEntry jointResultsEntry)
  {
    // filter with selected indictments
    getNumberOfIndictments(jointResultsEntry, _selectedIndictments);

    // count the number of unique defects, not the total
    int defectCount = 0;
    for (int i = 0; i < _indictmentArray.size(); i++)
    {
      if (_jointIndictmentCount[i] > 0)
      {
        defectCount++;
        // set joint count to 0 to avoid looping excesively
        _jointIndictmentCount[i] = 0;
      }
    }

    return defectCount;
  }

  /**
   * Dtermines count of indictments for a joint result
   * @author George Booth
   */
  void getNumberOfIndictments(JointResultsEntry jointResultsEntry, java.util.List<IndictmentEnum> selectedIndictments)
  {
/*
  _jointIndictmentCount[] should always be zero'd by calling routine
  to avoid doing this over and over:
    for (int i = 0; i < _indictmentArray.size(); i++)
    {
       _jointIndictmentCount[i] = 0;
    }
*/
    List<IndictmentResult> indictmentResults = jointResultsEntry.getIndictmentResults();
    for (IndictmentResult indictment : indictmentResults)
    {
      IndictmentEnum thisIndictment = indictment.getIndictmentEnum();
      if (selectedIndictments == null ||
          selectedIndictments.contains(thisIndictment))
      {
        int index = _indictmentArray.indexOf(thisIndictment);
        _jointIndictmentCount[index]++;
      }
    }
  }

  /**
   * Adjusts table column widths
   * @param myTable JSortTable
   * @author George Booth
   */
  void setColumnWidths(JSortTable myTable)
  {
    Assert.expect(myTable != null);

    TableColumn column = null;
    int columnCount = this.getColumnCount();
    for (int i = 0; i < columnCount; i++)
    {
      column = myTable.getColumnModel().getColumn(i);
      column.setPreferredWidth(_jointInformationTableColumnWidths[i]);
    }
  }

  /**
   * @author George Booth
   */
  JointResultsEntry getJointResultsEntryAt(int row)
  {
    Assert.expect(row >= 0 && row < _filteredJointResults.size());

    return _filteredJointResults.get(row);
  }

  /**
   * @author George Booth
   */
  int getRowForJointResultsEntry(JointResultsEntry jointResultsEntry)
  {
    if (jointResultsEntry == null || _filteredJointResults.size() == 0)
    {
      return - 1;
    }

    int row = -1;
    for (JointResultsEntry entry : _filteredJointResults)
    {
      row++;
      if (entry.getRunStartTime().longValue() == jointResultsEntry.getRunStartTime().longValue() &&
          entry.getBoardName().equals(jointResultsEntry.getBoardName()) &&
          entry.getReferenceDesignator().equals(jointResultsEntry.getReferenceDesignator()) &&
          entry.getPadName().equals(jointResultsEntry.getPadName()))
      {
        return row;
      }
    }
    return -1;
  }

  /**
   * Return the component type at the index
   * @author George Booth
   */
  ComponentType getComponentTypeAt(int row)
  {
    Assert.expect(row >= 0 && row < _filteredJointResults.size());

    Project project = Project.getCurrentlyLoadedProject();
    // get board reference from board name
    Board board = project.getPanel().getBoard((String)getValueAt(row, _BOARD_NAME));

    String refDesignator = _filteredJointResults.get(row).getReferenceDesignator();
    Component comp = board.getComponent(refDesignator);
    ComponentType compType = comp.getComponentType();
    return compType;
  }

  /**
   * @author George Booth
   */
  Long getRunIdAt(int row)
  {
    Assert.expect(row >= 0 && row < _filteredJointResults.size());

    return _filteredJointResults.get(row).getRunStartTime();
  }

  /**
   * @author George Booth
   */
  public int getColumnCount()
  {
    return _jointInformationTableColumnNames.length;
  }

  /**
   * @author George Booth
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return _jointInformationTableColumnNames[columnIndex];
  }

  /**
   * @author George Booth
   */
  public boolean isSortable(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    switch (columnIndex)
    {
      default:
        return true;
    }
  }

  /**
   * @author George Booth
   */
  public void sortColumn(int columnIndex, boolean ascending)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    _currentSortColumn = columnIndex;
    _currentSortAscending = ascending;

    switch (columnIndex)
    {
      case _RUN_ID:
        Collections.sort(_filteredJointResults, new JointResultsEntryComparator(
            ascending, JointResultsEntryComparatorEnum.RUN_ID));
        break;
      case _BOARD_NAME:
        Collections.sort(_filteredJointResults, new
          JointResultsEntryComparator(ascending, JointResultsEntryComparatorEnum.BOARD_NAME));
        break;
      case _COMPONENT_NAME:
        Collections.sort(_filteredJointResults, new
          JointResultsEntryComparator(ascending, JointResultsEntryComparatorEnum.COMPONENT_NAME));
        break;
      case _PIN_NAME:
        Collections.sort(_filteredJointResults, new
          JointResultsEntryComparator(ascending, JointResultsEntryComparatorEnum.PIN_NAME));
        break;
      case _JOINT_TYPE:
        Collections.sort(_filteredJointResults, new
          JointResultsEntryComparator(ascending, JointResultsEntryComparatorEnum.JOINT_TYPE));
        break;
      case _SUBTYPE:
        Collections.sort(_filteredJointResults, new
          JointResultsEntryComparator(ascending, JointResultsEntryComparatorEnum.SUBTYPE));
        break;
      case _SIDE:
        Collections.sort(_filteredJointResults, new
          JointResultsEntryComparator(ascending, JointResultsEntryComparatorEnum.SIDE));
        break;
      case _NUM_DEFECTS: // # Defect Calls
        Collections.sort(_filteredJointResults, new
          JointResultsEntryComparator(ascending, JointResultsEntryComparatorEnum.NUM_DEFECTS));
        break;
      default:
        // this should not happen if so then assert
        Assert.expect(false);
        return;
     }
  }

  /**
   * @author George Booth
   */
  public int getRowCount()
  {
    if (_filteredJointResults == null)
      return 0;
    return _filteredJointResults.size();
  }

  /**
   * @author George Booth
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return false;
  }

  /**
   * @author George Booth
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    JointResultsEntry jointResultsEntry = _filteredJointResults.get(rowIndex);
    switch(columnIndex)
    {
      case _RUN_ID:
        return jointResultsEntry.getRunStartTime();
      case _BOARD_NAME:
        return jointResultsEntry.getBoardName();
      case _COMPONENT_NAME:
        return jointResultsEntry.getReferenceDesignator();
      case _PIN_NAME:
        return jointResultsEntry.getPadName();
      case _JOINT_TYPE:
        return jointResultsEntry.getJointTypeName();
      case _SUBTYPE:
        return jointResultsEntry.getSubtypeName();
      case _SIDE:
        if (jointResultsEntry.isTopSide())
          return _TOP_SIDE;
        else
          return _BOTTOM_SIDE;
      case _NUM_DEFECTS:
        return new Integer(jointResultsEntry.getNumberOfDefects());
      default:
        // this should not happen if so then assert
        Assert.expect(false);
        return null;
    }
  }

  /**
   * @author George Booth
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    // do nothing
  }

  /**
   * @author George Booth
   */
  void clear()
  {
    _jointResultsManager.clearComponentResults();

    if (_allPassingJointResults != null)
      _allPassingJointResults.clear();
    if (_allFailingJointResults != null)
      _allFailingJointResults.clear();
    if (_filteredPassingJointResults != null)
      _filteredPassingJointResults.clear();
    if (_filteredFailingJointResults != null)
      _filteredFailingJointResults.clear();
    if (_displayedPassingJointResults != null)
      _displayedPassingJointResults.clear();
    if (_displayedFailingJointResults != null)
      _displayedFailingJointResults.clear();
    if (_filteredJointResults != null)
      _filteredJointResults.clear();
    if (_passingSliceNames != null)
      _passingSliceNames.clear();
    if (_failingSliceNames != null)
      _failingSliceNames.clear();
    if (_selectedIndictments != null)
      _selectedIndictments.clear();
    // don't clear _indictmentArray - it comes from static data
    if (_jointIndictmentCount != null)
      for (int i = 0; i < _indictmentArray.size(); i++)
        _jointIndictmentCount[i] = 0;
    if (_totalIndictmentCount != null)
      for (int i = 0; i < _indictmentArray.size(); i++)
        _totalIndictmentCount[i] = 0;

    _currentSortColumn = -1;
    _currentSortAscending = true;
  }


}
