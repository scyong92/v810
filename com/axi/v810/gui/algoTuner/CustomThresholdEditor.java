package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

import com.axi.util.*;

/**
 * A cell editor customized for thresholds
 * @author Andy Mechtenberg
 */
public class CustomThresholdEditor implements TableCellEditor
{
  private TableCellEditor _editor, _defaultEditor;
  private Hashtable<Integer, TableCellEditor> _editors;
  private JTable _table;

  /**
   * @author Andy Mechtenberg
   */
  public CustomThresholdEditor(JTable table)
  {
    Assert.expect(table != null);
    _table = table;
    _editors = new Hashtable<Integer, TableCellEditor>();
    _defaultEditor = new DefaultCellEditor(new JTextField());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setEditorAt(int row, TableCellEditor editor)
  {
    Assert.expect(editor != null);
    _editors.put(new Integer(row), editor);
  }

  /**
   * @author Andy Mechtenberg
   */
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
  {
    return _editor.getTableCellEditorComponent(table, value, isSelected, row, column);
  }

  /**
   * @author Andy Mechtenberg
   */
  public Object getCellEditorValue()
  {
    return _editor.getCellEditorValue();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isCellEditable(EventObject anEvent)
  {
    if(anEvent instanceof MouseEvent)
    {
      selectEditor((MouseEvent) anEvent);  // sets _editor
    }
    else
    {
      selectEditor(null);  // sets _editor
    }
    return _editor.isCellEditable(anEvent);
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean shouldSelectCell(EventObject anEvent)
  {
    if(anEvent instanceof MouseEvent)
    {
      selectEditor((MouseEvent) anEvent);  // sets _editor
    }
    else
    {
      selectEditor(null);  // sets _editor
    }
    boolean should = _editor.shouldSelectCell(anEvent);
    return should;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean stopCellEditing()
  {
    return _editor.stopCellEditing();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void cancelCellEditing()
  {
    _editor.cancelCellEditing();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void addCellEditorListener(CellEditorListener l)
  {
    _editor.addCellEditorListener(l);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void removeCellEditorListener(CellEditorListener l)
  {
    _editor.removeCellEditorListener(l);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void selectEditor(MouseEvent e)
  {
    int row;
    if (e == null)
    {
      row = _table.getSelectionModel().getAnchorSelectionIndex();
    }
    else
    {
      row = _table.rowAtPoint(e.getPoint());
    }
    //System.out.println("Select Editor at Row: " + row);
    _editor = (TableCellEditor)_editors.get(new Integer(row));
    if (_editor == null)
      _editor = _defaultEditor;

    DefaultCellEditor dce = (DefaultCellEditor)_editor;
    Component field = dce.getComponent();
    if (field instanceof IntegerNumberField)
    {
      IntegerNumberField inf = (IntegerNumberField)field;
      // Set to BOLD font
      inf.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
    }
    if (field instanceof FloatNumberField)
    {
      FloatNumberField fnf = (FloatNumberField)field;
      // Set to BOLD font
      fnf.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
    }
  }
}
