package com.axi.v810.gui.algoTuner;

import java.util.*;

import org.jfree.data.statistics.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.gui.*;

/**
 * This class represents the data used to chart a Histogram.
 * @author Rex Shang
 */
class JointResultsEntryHistogramDataSet extends HistogramDataset
{
  private List<JointResultsEntry> _jointResultsEntryList;

  // Variables used to find the relavent value in _jointEntryList.
  private MeasurementEnum _measurementType;
  private SliceNameEnum _sliceType;

  // Statistical data.
  private int _binCount;
  private double _mean;
  private double _standardDeviation;
  private double _median;

  private MeasurementUnitsEnum _measurementUnit;

  /**
   * Constructor used to build an empty looking histogram chart.
   * @author Rex Shang
   */
  JointResultsEntryHistogramDataSet()
  {
    this(null, null, null, 1);
  }

  /**
   * @author Rex Shang
   */
  JointResultsEntryHistogramDataSet(List<JointResultsEntry> resultsList,
      MeasurementEnum measurementType,
      SliceNameEnum sliceType,
      int binCount)
  {
    _jointResultsEntryList = resultsList;
    _measurementType = measurementType;
    _sliceType = sliceType;
    _binCount = binCount;

    if (_jointResultsEntryList == null)
      _jointResultsEntryList = new ArrayList<JointResultsEntry>();

    addToHistogramData();
  }

  /**
   * Use _jointEntryList to construct a new set of histogram data.
   * @author Rex Shang
   */
  private void addToHistogramData()
  {
    ArrayList<Double> dataPoints = new ArrayList<Double>();

    // Find the value we need to chart.
    for (JointResultsEntry jointResult : _jointResultsEntryList)
    {
      List<MeasurementResult> measurementResults = jointResult.getAllMeasurementResults();
      for (MeasurementResult measurementResult : measurementResults)
      {
        if (measurementResult.getMeasurementEnum().equals(_measurementType)
            && measurementResult.getSliceNameEnum().equals(_sliceType))
        {
          Number value = measurementResult.getValue();
          MeasurementUnitsEnum measurementUnitsEnum = measurementResult.getMeasurementUnitsEnum();

          // conditionally convert to mils based on the project's preferred units.
          Pair<MeasurementUnitsEnum, Object> convertedValue = MeasurementUnitsEnum.convertToProjectUnitsIfNecessary(measurementUnitsEnum, value);
          measurementUnitsEnum = convertedValue.getFirst();
          value = (Number)convertedValue.getSecond();

          if (_measurementUnit == null)
            _measurementUnit = measurementUnitsEnum;

          dataPoints.add(value.doubleValue());
          break;
        }
      }
    }

    // Check to see if we have any valid histogram data.
    if (dataPoints.size() > 0)
    {
      // Convert List<Double> to double[] since our super.addSeries() can't
      // take a list...
      double[] dataSerie = new double[dataPoints.size()];
      for (int i = 0; i < dataPoints.size(); i++)
        dataSerie[i] = dataPoints.get(i).doubleValue();

      // We only have one serie, put our measurement name on the serie.
      addSeries(_measurementType, dataSerie, _binCount);

      // Do statistical calculation.
      _mean = Statistics.calculateMean(dataPoints);
      _standardDeviation = Statistics.getStdDev(dataPoints.toArray(new Double[0]));
      _median = Statistics.calculateMedian(dataPoints);
    }
    else
    {
      _mean = Double.NaN;
      _standardDeviation = Double.NaN;
      _median = Double.NaN;
    }
  }

  /**
   * Get the mean value of this data set.
   * @author Rex Shang
   */
  double getMean()
  {
    return _mean;
  }

  /**
   * Get the median value of this data set.
   * @author Rex Shang
   */
  double getMedian()
  {
    return _median;
  }

  /**
   * Get the standard deviation of this data set.
   * @author Rex Shang
   */
  double getStandardDeviation()
  {
    return _standardDeviation;
  }

  /**
   * Get the general measurement unit.
   * @author Rex Shang
   */
  MeasurementUnitsEnum getMeasurementUnitsEnum()
  {
    return _measurementUnit;
  }

}
