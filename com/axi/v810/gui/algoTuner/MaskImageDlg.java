package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * This class constructs subtype masking dialog
 * @author Jack Hwee
 */
public class MaskImageDlg extends JDialog
{
  private PanelDataAdapter _panelDataAdapter = null;
  private JointTypeEnum _jointTypeEnum = null;
  private Subtype _subtype = null;
  private Project _project =  null;

  private JPanel _mainDlgPanel = new JPanel();
  private BorderLayout _mainDlgPanelBorderLayout = new BorderLayout();
  private TitledBorder _subtypeMaskingTitledBorder;
  private Border _emptySpace010010Border;
  private Border _emptySpace510510Border;
  private Border _optionsMainPanelBorder;
  private JPanel _centerPanel = new JPanel();
  private JRadioButton _allSubtypeRadioButton = new JRadioButton();
  private JRadioButton _onlyMaskSubtypeRadioButton = new JRadioButton();
  private FlowLayout _withoutMaskSubtypeEmptyPanelFlowLayout = new FlowLayout();
  private FlowLayout _onlyMaskSubtypeEmptyPanelFlowLayout = new FlowLayout();
  private FlowLayout _allSubtypeEmptyPanelFlowLayout = new FlowLayout();
  private FlowLayout _taskButtonPanelFlowLayout = new FlowLayout();
  private JRadioButton _withoutMaskSubtypeRadioButton = new JRadioButton();
  private JPanel _optionsOuterPanel = new JPanel();
  private JPanel _onlyMaskSubtypeEmptyPanel = new JPanel();
  private JPanel _allSubtypeEmptyPanel = new JPanel();
  private JPanel _onlyMaskSubtypePanel = new JPanel();
  private JPanel _withoutMaskSubtypePanel = new JPanel();
  private JLabel _withoutMaskSubtypeEmptyLabel = new JLabel();
  private JPanel _allSubtypePanel = new JPanel();
  private JLabel _onlyMaskSubtypeEmptyLabel = new JLabel();
  private JLabel _allSubtypeEmptyLabel = new JLabel();
  private JPanel _optionsMainPanel = new JPanel();
  private BorderLayout _optionsMainPanelBorderLayout = new BorderLayout();
  private BorderLayout _withoutMaskSubtypePanelBorderLayout = new BorderLayout();
  private BorderLayout _onlyMaskSubtypePanelBorderLayout = new BorderLayout();
  private BorderLayout _allSubtypePanelBorderLayout = new BorderLayout();
  private BorderLayout _optionsOuterPanelBorderLayout = new BorderLayout();
  private JPanel _optionsPanel = new JPanel();
  private JPanel _withoutMaskSubtypeEmptyPanel = new JPanel();
  private GridLayout _optionsPanelGridLayout = new GridLayout();
  private JPanel _subtypesListPanel = new JPanel();
  private JPanel _subtypesListOuterPanel = new JPanel();
  private BorderLayout _subtypeListPanelBorderLayout = new BorderLayout();
  private JPanel _taskButtonsPanel = new JPanel();
  private BorderLayout _subtypeListOuterPanelBorderLayout = new BorderLayout();
  private JButton _allSubtypesButton = new JButton();
  private JButton _deselectAllSubtypesButton = new JButton();
  private JButton _showMaskImageButton = new JButton();
  private JButton _goToDirButton = new JButton();
  private JPanel _bottomPanel = new JPanel();
  private JButton _createMaskButton = new JButton();
  private JPanel _okCancelPanel = new JPanel();
  private FlowLayout _okCancelPanelFlowLayout = new FlowLayout();
  private JPanel _okCancelSubPanel = new JPanel();
  private BorderLayout _okCancelSubPanelBorderLayout = new BorderLayout();
  private JButton _okButton = new JButton();
  private BorderLayout _bottomPanelBorderLayout = new BorderLayout();
  private BorderLayout _centerPanelBorderLayout = new BorderLayout();

  private java.util.List<ImageSetData> _selectedImageSets = new LinkedList<ImageSetData>();
  
  private MaskImageStatusTable _maskImageStatusTable = new MaskImageStatusTable();
  private MaskImageStatusTableModel _maskImageStatusTableModel;
  private JScrollPane _tablePanelLayoutScroll = new JScrollPane();
   // listener for row table selection
  private ListSelectionListener _subtypesTableListSelectionListener;
  private java.util.List<Subtype> _subtypes = new LinkedList<Subtype>();

  /**
   * @author Andy Mechtenberg
   */
  MaskImageDlg(Frame frame,
                    String title,
                    boolean modal,
                    PanelDataAdapter panelDataAdapter,
                    BoardType boardType,
                    JointTypeEnum jointTypeEnum,
                    Subtype subtype,
                    Algorithm algorithm,
                    AlgorithmSetting algorithmSetting)
  {
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(panelDataAdapter != null);
    Assert.expect(boardType != null);
    Assert.expect(jointTypeEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(algorithm != null);
    Assert.expect(algorithmSetting != null);
    
    _panelDataAdapter = panelDataAdapter;
    _jointTypeEnum = jointTypeEnum;
    _subtype = subtype;

    jbInit();
    pack();
    // To make the dialog look good, I want the panels to be the size of the largest (threshold)
    // The second pack after the setPreferredSize will ensure this
    _subtypesListPanel.setPreferredSize(_optionsMainPanel.getPreferredSize());
    pack();
  }

  /**
   * @author Jack Hwee
   */
  public MaskImageDlg(Frame frame,
                    String title,
                    boolean modal,
                    Project project,
                    Subtype subtype,
                    java.util.List<ImageSetData> selectedImageSets)
  {
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(project != null);
    Assert.expect(subtype != null);
    Assert.expect(selectedImageSets != null);
    _project = project;
    _jointTypeEnum = subtype.getJointTypeEnum();
    _subtype = subtype;
    _selectedImageSets = selectedImageSets;
    
    //populate mask image status table
    _maskImageStatusTableModel = new MaskImageStatusTableModel();
    _maskImageStatusTableModel.setData(_project, _jointTypeEnum);

    jbInit();
    pack();
    // To make the dialog look good, I want the panels to be the size of the largest (threshold)
    // The second pack after the setPreferredSize will ensure this
    _subtypesListPanel.setPreferredSize(_optionsMainPanel.getPreferredSize());
    pack();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
     this.setPreferredSize(new Dimension(900, 500));
    _subtypeMaskingTitledBorder = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,ColorUtil.getGrayShadowColor()),
                                              StringLocalizer.keyToString("ATGUI_THRESHOLD_MASK_IMAGE_KEY"));
    _emptySpace010010Border = BorderFactory.createCompoundBorder(_subtypeMaskingTitledBorder,BorderFactory.createEmptyBorder(0,10,0,10));
    _emptySpace510510Border = BorderFactory.createEmptyBorder(5,10,5,10);
    _optionsMainPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,ColorUtil.getGrayShadowColor())),BorderFactory.createEmptyBorder(0,5,0,5));
    
    _mainDlgPanel.setLayout(_mainDlgPanelBorderLayout);
    _mainDlgPanelBorderLayout.setHgap(10);
    _centerPanel.setLayout(_centerPanelBorderLayout);
    
    _centerPanel.setBorder(_emptySpace510510Border);
    _allSubtypeRadioButton.setSelected(true);
    _allSubtypeRadioButton.setText(StringLocalizer.keyToString("MMGUI_SUBTYPE_MASKING_ALL_SUBTYPE_LABEL_KEY"));
    _allSubtypeRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        allSubtypeRadioButtonRadioButton_actionPerformed(e);
      }
    });
  
    _onlyMaskSubtypeRadioButton.setText(StringLocalizer.keyToString("MMGUI_SUBTYPE_MASKING_SUBTYPE_WITH_MASKING_ONLY_LABEL_KEY"));
    _onlyMaskSubtypeRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        onlyMaskSubtypeRadioButton_actionPerformed(e);
      }
    });
     
    _withoutMaskSubtypeEmptyPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _withoutMaskSubtypeEmptyPanelFlowLayout.setHgap(25);
    _onlyMaskSubtypeEmptyPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _onlyMaskSubtypeEmptyPanelFlowLayout.setHgap(25);
    _allSubtypeEmptyPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _allSubtypeEmptyPanelFlowLayout.setHgap(25);
    _withoutMaskSubtypeRadioButton.setText(StringLocalizer.keyToString("MMGUI_SUBTYPE_MASKING_SUBTYPE_WITHOUT_MASKING_ONLY_LABEL_KEY"));
    _withoutMaskSubtypeRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        withoutMaskSubtypeRadioButton_actionPerformed(e);
      }
    });
    _optionsOuterPanel.setLayout(_optionsOuterPanelBorderLayout);
    _onlyMaskSubtypeEmptyPanel.setLayout(_onlyMaskSubtypeEmptyPanelFlowLayout);
    _allSubtypeEmptyPanel.setLayout(_allSubtypeEmptyPanelFlowLayout);
    _onlyMaskSubtypePanel.setLayout(_onlyMaskSubtypePanelBorderLayout);
    _withoutMaskSubtypePanel.setLayout(_withoutMaskSubtypePanelBorderLayout);
    _allSubtypePanel.setLayout(_allSubtypePanelBorderLayout);
    _optionsMainPanel.setLayout(_optionsMainPanelBorderLayout);
    _optionsMainPanel.setBorder(_optionsMainPanelBorder);
    _optionsPanel.setLayout(_optionsPanelGridLayout);
    _withoutMaskSubtypeEmptyPanel.setLayout(_withoutMaskSubtypeEmptyPanelFlowLayout);
    _optionsPanelGridLayout.setRows(3);
    _optionsPanelGridLayout.setVgap(10);
    _subtypesListPanel.setLayout(_subtypeListPanelBorderLayout);
    _subtypesListPanel.setBorder(_emptySpace010010Border);
    _subtypesListOuterPanel.setLayout(_subtypeListOuterPanelBorderLayout);
    _subtypeListPanelBorderLayout.setHgap(5);
    _subtypeListPanelBorderLayout.setVgap(5);
    _subtypeListOuterPanelBorderLayout.setVgap(5);

    _allSubtypesButton.setText(StringLocalizer.keyToString("ATGUI_ALL_SUBTYPES_KEY"));
    _allSubtypesButton.setPreferredSize(new Dimension(100, _allSubtypesButton.getPreferredSize().height));
    _allSubtypesButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        allSubtypesButton_actionPerformed(e);
      }
    });
    
    _deselectAllSubtypesButton.setText(StringLocalizer.keyToString("MMGUI_SUBTYPE_MASKING_SUBTYPE_DESELECT_ALL_BUTTON_KEY"));
    _deselectAllSubtypesButton.setPreferredSize(new Dimension(100, _deselectAllSubtypesButton.getPreferredSize().height));
    _deselectAllSubtypesButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deselectAllSubtypesButton_actionPerformed(e);
      }
    });
    
    _showMaskImageButton.setText(StringLocalizer.keyToString("ATGUI_SHOW_MASK_IMAGE_KEY"));
    _showMaskImageButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showMaskImageButton_actionPerformed(e);
      }
    });
    _showMaskImageButton.setEnabled(false);

    _goToDirButton.setText(StringLocalizer.keyToString("MMGUI_SUBTYPE_MASKING_SUBTYPE_GO_TO_DIRECTORY_BUTTON_KEY"));
    _goToDirButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        goToDirButton_actionPerformed(e);
      }
    });
    
    _createMaskButton.setMnemonic(StringLocalizer.keyToString("MMGUI_SUBTYPE_MASKING_SUBTYPE_CREATE_MASK_BUTTON_KEY").charAt(7));
    _createMaskButton.setText(StringLocalizer.keyToString("MMGUI_SUBTYPE_MASKING_SUBTYPE_CREATE_MASK_BUTTON_KEY"));
    _createMaskButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        createMaskButton_actionPerformed(e);
      }
    });
    _okCancelPanel.setLayout(_okCancelPanelFlowLayout);
    _okCancelPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _okCancelPanelFlowLayout.setHgap(10);
    _okCancelSubPanel.setLayout(_okCancelSubPanelBorderLayout);
    _okCancelSubPanelBorderLayout.setHgap(10);
    _okButton.setMnemonic(StringLocalizer.keyToString("GUI_OK_BUTTON_MNEMONIC_KEY").charAt(0));
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _bottomPanel.setLayout(_bottomPanelBorderLayout);
    
    getContentPane().add(_mainDlgPanel);
    _mainDlgPanel.add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.add(_optionsOuterPanel, BorderLayout.WEST);
    _optionsOuterPanel.add(_optionsMainPanel, BorderLayout.CENTER);
    _optionsMainPanel.add(_optionsPanel, BorderLayout.NORTH);
    
    _optionsPanel.add(_allSubtypePanel, null);
    _allSubtypePanel.add(_allSubtypeRadioButton, BorderLayout.CENTER);
    _allSubtypePanel.add(_allSubtypeEmptyPanel, BorderLayout.SOUTH);
    _allSubtypeEmptyPanel.add(_allSubtypeEmptyLabel, null);
    
    _optionsPanel.add(_onlyMaskSubtypePanel, null);
    _onlyMaskSubtypePanel.add(_onlyMaskSubtypeRadioButton, BorderLayout.CENTER);
    _onlyMaskSubtypePanel.add(_onlyMaskSubtypeEmptyPanel, BorderLayout.SOUTH);
    _onlyMaskSubtypeEmptyPanel.add(_onlyMaskSubtypeEmptyLabel, null);
    
    _optionsPanel.add(_withoutMaskSubtypePanel, null);
    _withoutMaskSubtypePanel.add(_withoutMaskSubtypeRadioButton, BorderLayout.CENTER);
    _withoutMaskSubtypePanel.add(_withoutMaskSubtypeEmptyPanel, BorderLayout.SOUTH);
    _withoutMaskSubtypeEmptyPanel.add(_withoutMaskSubtypeEmptyLabel, null);
    
    _centerPanel.add(_subtypesListOuterPanel, BorderLayout.CENTER);
    _subtypesListOuterPanel.add(_subtypesListPanel, BorderLayout.CENTER);
    _subtypesListPanel.add(_taskButtonsPanel, BorderLayout.SOUTH);
    
    _taskButtonsPanel.setLayout(_taskButtonPanelFlowLayout);
    _taskButtonPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _taskButtonPanelFlowLayout.setHgap(0);
    _taskButtonPanelFlowLayout.setVgap(0);
    
//    _taskButtonsPanel.add(_allSubtypesButton, null);
//    _taskButtonsPanel.add(_deselectAllSubtypesButton, null);
    _taskButtonsPanel.add(_createMaskButton, null);
    _taskButtonsPanel.add(_showMaskImageButton, null);
    _taskButtonsPanel.add(_goToDirButton, null);
    
    _mainDlgPanel.add(_bottomPanel, BorderLayout.SOUTH);
    _bottomPanel.add(_okCancelPanel, BorderLayout.CENTER);
    _okCancelPanel.add(_okCancelSubPanel, null);
//    _okCancelSubPanel.add(_createMaskButton, BorderLayout.WEST);
    _okCancelSubPanel.add(_okButton, BorderLayout.EAST);
//    _bottomPanel.add(_statusPanel, BorderLayout.SOUTH);

    _subtypesListPanel.add(_tablePanelLayoutScroll, BorderLayout.CENTER);
    _tablePanelLayoutScroll.getViewport().add(_maskImageStatusTable, null);
   
    _maskImageStatusTable.setModel(_maskImageStatusTableModel);
     
    // custom stuff goes here
    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(_allSubtypeRadioButton);
    buttonGroup.add(_onlyMaskSubtypeRadioButton);
    buttonGroup.add(_withoutMaskSubtypeRadioButton);
    
    //set up table row height based on the size of the font
    int prefHeight = _maskImageStatusTable.getRowHeight();
    _maskImageStatusTable.setPreferredColumnWidths();
    _maskImageStatusTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _maskImageStatusTable.setVisible(true);

    // its okay to do set size here.. bill just doesn't like it on the whole panel.
    int headerHeight = prefHeight + 5;
    Dimension prefScrollSize = new Dimension(200, (prefHeight * 10) + headerHeight);  // 10 rows for tables
    _tablePanelLayoutScroll.setPreferredSize(prefScrollSize);
    
    _subtypesTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        subtypesTableListSelectionChanged(e);
      }
    };
    
    ListSelectionModel rowSM = _maskImageStatusTable.getSelectionModel();
    rowSM.addListSelectionListener(_subtypesTableListSelectionListener); 
  }

  /**
   * @author Andy Mechtenberg
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void createMaskButton_actionPerformed(ActionEvent e)
  {
    setCursor(new Cursor(Cursor.WAIT_CURSOR));
 
    int[] selectedSubtypeRows = _maskImageStatusTable.getSelectedRows();
    _subtypes = _maskImageStatusTableModel.getSubtypes();
    
   // Object subtypes[] = _subtypeList.getSelectedValues();
    
    for (int i = 0; i < selectedSubtypeRows.length; i++)
    {
      Subtype targetSubtype = _subtypes.get(selectedSubtypeRows[i]);

      int answer = JOptionPane.OK_OPTION;
      if (MaskImageCompatibilityCheck.getInstance().getSubtypeToImageResolutionCompatibilityBoolMap().containsKey(targetSubtype))
      {
        String choiceMessage = StringLocalizer.keyToString(new LocalizedString("ATGUI_CREATE_MASK_IMAGE_WARNING_KEY", new Object[]{targetSubtype.getShortName()}));
        answer = ChoiceInputDialog.showConfirmDialog(MainMenuGui.getInstance(),
                                                     StringUtil.format(choiceMessage, 50),
                                                     StringLocalizer.keyToString("MMGUI_SUBTYPE_MASKING_SUBTYPE_CREATE_MASK_IMAGE_WARNING_TITLE_KEY"));
      }
      
      if (answer == JOptionPane.OK_OPTION)
      {      
        //Siew Yeng - delete existing mask before create a new mask to make sure mask image with wrong rotation will be removed.
        //            Only one mask image for each subtype.
        java.util.List<String> maskImagePathList = FileUtil.listFiles(Directory.getAlgorithmLearningDir(targetSubtype.getPanel().getProject().getName()), 
                                                                  FileName.getMaskImagePatternString(targetSubtype.getShortName()));
        try
        {
          if(maskImagePathList.isEmpty() == false)
          {
            FileUtil.delete(maskImagePathList);
          }

          //Siew Yeng - XCR-2764 - Sub-subtype feature
          if(targetSubtype.isSubSubtype())
            MaskImage.getInstance().maskImage(_project, _maskImageStatusTableModel.getSubtype(selectedSubtypeRows[i]), targetSubtype, _selectedImageSets); 
          else
            MaskImage.getInstance().maskImage(_project, targetSubtype, _selectedImageSets);           
        }
        catch (final XrayTesterException xte)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              displayError(xte);
            }
          });
        }
        catch(final CouldNotDeleteFileException ex) 
        {
          CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(maskImagePathList.get(0));
          dex.initCause(ex);
          displayError(dex);
        }  
      }
    }
    
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    //Siew Yeng - XCR-2764 - Sub-subtype feature
    try
    {
      MaskImageCompatibilityCheck.getInstance().checkMaskImageResolution(_project, _selectedImageSets);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
              ex,
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
              true);
    }
    _maskImageStatusTableModel.fireTableDataChanged();
    
    for(int i=0; i < selectedSubtypeRows.length; i++)
    {
      _maskImageStatusTable.addRowSelectionInterval(selectedSubtypeRows[i], selectedSubtypeRows[i]);
    }
//    dispose();
  }
  
  /**
   * @author Jack Hwee
   */
  private void allSubtypeRadioButtonRadioButton_actionPerformed(ActionEvent e)
  {
    _maskImageStatusTableModel.clearSubtypes();
  
    java.util.List<Subtype> subtypes = _project.getPanel().getInspectedSubtypes(_jointTypeEnum);
 
    for(Subtype subtype : subtypes)
    {
     // if (subtype !=_subtype)  
      _maskImageStatusTableModel.setSubtype(subtype);   
      //Siew Yeng - XCR-2764 - Sub-subtype feature
      if(subtype.hasSubSubtypes())
      {
        for(Subtype subSubtype : subtype.getSubSubtypes())
        {
          _maskImageStatusTableModel.setSubtype(subSubtype);    
        }
      }
    }
   
    _maskImageStatusTableModel.fireTableDataChanged();
  }
  
  /**
   * @author Jack Hwee
   */
  private void onlyMaskSubtypeRadioButton_actionPerformed(ActionEvent e)
  {
    _maskImageStatusTableModel.clearSubtypes();

    java.util.List<Subtype> subtypes = _project.getPanel().getInspectedSubtypes(_jointTypeEnum);

    for(Subtype subtype : subtypes)
    {
      if(checkSubtypeMaskImage(subtype))
      {
        _maskImageStatusTableModel.setSubtype(subtype);      
      }
      
      //Siew Yeng - XCR-2764 - Sub-subtype feature
      if(subtype.hasSubSubtypes())
      {
        for(Subtype subSubtype : subtype.getSubSubtypes())
        {
          if(checkSubtypeMaskImage(subSubtype))
          {
            _maskImageStatusTableModel.setSubtype(subSubtype);    
          }
        }
      }
    }

    _maskImageStatusTableModel.fireTableDataChanged();
  }
  
  /**
   * @author Jack Hwee
   */
  private void withoutMaskSubtypeRadioButton_actionPerformed(ActionEvent e)
  {
    _maskImageStatusTableModel.clearSubtypes();
       
    java.util.List<Subtype> subtypes = _project.getPanel().getInspectedSubtypes(_jointTypeEnum);

    for(Subtype subtype : subtypes)
    {
      if(checkSubtypeMaskImage(subtype) == false)
      {
        _maskImageStatusTableModel.setSubtype(subtype);      
      }
      
      //Siew Yeng - XCR-2764 - Sub-subtype feature
      if(subtype.hasSubSubtypes())
      {
        for(Subtype subSubtype : subtype.getSubSubtypes())
        {
          if(checkSubtypeMaskImage(subSubtype) == false)
          {
            _maskImageStatusTableModel.setSubtype(subSubtype);    
          }
        }
      }
    }
  
    _maskImageStatusTableModel.fireTableDataChanged();
  }
  
   /**
   * @author Andy Mechtenberg
   */
  private void displayError(XrayTesterException xte)
  {
    MessageDialog.showErrorDialog(MainMenuGui.getInstance(), xte.getLocalizedMessage(),
                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
  }

  /**
   * @author Andy Mechtenberg
   */
  void allSubtypesButton_actionPerformed(ActionEvent e)
  {
    _maskImageStatusTable.setRowSelectionInterval(0, _maskImageStatusTable.getRowCount() - 1);
  }
  
   /**
   * @author Jack Hwee
   */
  void deselectAllSubtypesButton_actionPerformed(ActionEvent e)
  {
    _maskImageStatusTable.clearSelection();
  }
  
  /**
   * @author Jack Hwee
   */
  void showMaskImageButton_actionPerformed(ActionEvent e)
  {
    int[] selectedSubtypeRows = _maskImageStatusTable.getSelectedRows();
    _subtypes = _maskImageStatusTableModel.getSubtypes();
     
    Subtype targetSubtype = null;

    if (selectedSubtypeRows.length == 0)
    {
      targetSubtype = _subtype;
    }
    else if (selectedSubtypeRows.length == 1)
    {
      targetSubtype = _subtypes.get(selectedSubtypeRows[0]);
    }

    if (targetSubtype != null)
    {
      //String maskImagePath = Directory.getAlgorithmLearningDir(targetSubtype.getPanel().getProject().getName()) + File.separator + "mask#" + targetSubtype.getShortName() + ".png";
      //Siew Yeng - XCR-2843 - masking rotation
      java.util.List<String> maskImagePathList = FileUtil.listFiles(Directory.getAlgorithmLearningDir(_project.getName()), 
                                                FileName.getMaskImagePatternString(targetSubtype.getShortName()));
      
      if (maskImagePathList.isEmpty() == false)
      {
        String maskImagePath = maskImagePathList.get(0);
        //Siew Yeng - XCR-3101 - Mask image is not refresh after edit 
        try 
        {
          Icon icon = new ImageIcon(javax.imageio.ImageIO.read(new File(maskImagePath)));

          JOptionPane.showMessageDialog(
          MainMenuGui.getInstance(),
          targetSubtype.getShortName(), 
          StringLocalizer.keyToString("ATGUI_SHOW_MASK_IMAGE_DIALOG_TITLE_KEY"),
          JOptionPane.INFORMATION_MESSAGE,
          icon);
        } 
        catch (IOException ex) 
        {
          System.out.println("Faile to load mask image: " + maskImagePath);
        }
      }
      else
      {
        JOptionPane.showMessageDialog(
        MainMenuGui.getInstance(),
        StringLocalizer.keyToString("ATGUI_NO_MASK_IMAGE_MESSAGE_KEY"), 
        StringLocalizer.keyToString("ATGUI_SHOW_MASK_IMAGE_DIALOG_TITLE_KEY"),
        JOptionPane.INFORMATION_MESSAGE
        );
      }
    }
    else
    {
      JOptionPane.showMessageDialog(
      MainMenuGui.getInstance(),
      StringLocalizer.keyToString("ATGUI_NO_MASK_IMAGE_MESSAGE_KEY"), 
      StringLocalizer.keyToString("ATGUI_SHOW_MASK_IMAGE_DIALOG_TITLE_KEY"),
      JOptionPane.INFORMATION_MESSAGE
      );
    }
  }
  
  /**
   * @author Jack Hwee
   */
  private void subtypesTableListSelectionChanged(ListSelectionEvent e)
  {
    int[] selectedSubtypeRows = _maskImageStatusTable.getSelectedRows();

    if (selectedSubtypeRows.length == 1)
    {
      _showMaskImageButton.setEnabled(true);
    }
    else
    {
      _showMaskImageButton.setEnabled(false);
    } 
  }
  
  /**
   * @author Jack Hwee
   */
  void goToDirButton_actionPerformed(ActionEvent e)
  {
    String maskImageDirectory = Directory.getAlgorithmLearningDir(_project.getName());
    try 
    {
      BrowserLauncher.openURLInNewWindow(maskImageDirectory);
    } 
    catch (IOException ioe)
    {
      MessageDialog.reportIOError(MainMenuGui.getInstance(), ioe.getLocalizedMessage());
    }
  }
  
  /**
   *  @author Jack Hwee
   */
  private boolean checkSubtypeMaskImage(Subtype subtype)
  {
//    boolean bool = false;
//    String maskImagePath = Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()) + java.io.File.separator + "mask#" + subtype.getShortName() + ".png";
    //Siew Yeng - XCR-2843 - masking rotation
    java.util.List<String> maskImagePathList = FileUtil.listFiles(Directory.getAlgorithmLearningDir(_project.getName()), 
                                                FileName.getMaskImagePatternString(subtype.getShortName()));
    
    if(maskImagePathList.isEmpty() == false)
    {
      return true;
    }

    return false;
  }

  /**
   * @author Siew Yeng
   */
  public void clear()
  {
    _subtypes.clear();
    _maskImageStatusTableModel.clear();
  }
}

