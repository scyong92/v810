package com.axi.v810.gui.algoTuner;

import java.util.*;

import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * Table model used to display joint inspection measurement information.
 * The entries in this table is also used to chart.
 * @author Rex Shang
 */
public class ReviewMeasurementsTableModel extends DefaultSortTableModel
{
  // Variables used for print debug messages.
  private String _me = "ReviewResultsTableModel";

  // The order listed here dictates the order of the table columns.
  private static int _index = -1;
  public static final int RUN_ID = ++_index;
  public static final int COMPONENT_NAME = ++_index;
  public static final int PIN_NAME = ++_index;
  public static final int MEASUREMENT = ++_index;
  public static final int PASS_FAIL = ++_index;
  public static final int COMPONENT_LOCATION = ++_index;
  public static final int BOARD_NAME = ++_index;
  public static final int SLICE_NAME = ++_index;
  public static final int MEASUREMENT_NAME = ++_index;
  public static final int MEASUREMENT_UNIT = ++_index;
  
  // Added by Lee Herng 10 Feb 2014 - To handle different 
  private Map<Integer,Pair<String,Integer>> _columnInfo = new HashMap<Integer,Pair<String,Integer>>();

  // The column names and width for this table.
  private ArrayList<String> _columnNames;
  private ArrayList<Integer> _columnWidths;

  // JointResultsManager is used to get data to populate our model.
  private JointResultsManager _testedJointsManager;

  // The list is essentially our data model.
  private List<JointResultsEntry> _testedJoints = new ArrayList<JointResultsEntry>();

  // Static strings used to fill the table cell.
  private final static String _TOP = StringLocalizer.keyToString("ATGUI_TOP_SIDE_KEY"); // "(top)"
  private final static String _BOTTOM = StringLocalizer.keyToString("ATGUI_BOTTOM_SIDE_KEY"); // "(bot)"
  private final static String _PASS = StringLocalizer.keyToString("RMGUI_PASS_VALUE_KEY"); // "Pass"
  private final static String _FAIL = StringLocalizer.keyToString("RMGUI_FAIL_VALUE_KEY"); // "Fail"

  // List of variables to filter our data selection.
  private String _projectName;
  private List<Long> _inspectionRuns;
  private Subtype _subtype;
  private MeasurementEnum _measurementType;
  private SliceNameEnum _sliceType;

  private boolean _isSortInAscendingOrder;
  private JointResultsEntryComparatorEnum _sortComparatorType;

  private boolean _isDisplayAllMeasurements;
  
  /**
   * @author Rex Shang
   */
  ReviewMeasurementsTableModel()
  {
    _testedJointsManager = JointResultsManager.getInstance();
    _isDisplayAllMeasurements = false;

    setupColumns();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  ReviewMeasurementsTableModel(boolean isDisplayAllMeasurements)
  {        
    _testedJointsManager = JointResultsManager.getInstance();
    _isDisplayAllMeasurements = isDisplayAllMeasurements;
    
    setupColumns();
  }

  /**
   * Set up column heading and width.
   * @author Rex Shang
   */
  private void setupColumns()
  {
    setupColumnList();
    
    int columnSize = _columnInfo.size();
    
    // Pre-populate _columnNames with "" and then fill with real data.  This
    // way, we can adjust colunn order by just change the column variables.
    _columnNames = new ArrayList<String>(columnSize);
    _columnWidths = new ArrayList<Integer>(columnSize);
    for (int i = 0; i < columnSize; i++)
    {
      _columnNames.add("");
      _columnWidths.add(new Integer(0));
    }
    for(Map.Entry<Integer,Pair<String,Integer>> entry : _columnInfo.entrySet())
    {
      int index = entry.getKey().intValue();
      Pair<String,Integer> columnInfoPair = entry.getValue();
      
      _columnNames.set(index, columnInfoPair.getFirst());
      _columnWidths.set(index, columnInfoPair.getSecond());
    }

    // Check to see if width is exceeding the limit.
    int totalWidth = 0;
    for (Integer columnWidth : _columnWidths)
    {
      totalWidth += columnWidth.intValue();
    }
    Assert.expect(totalWidth <= 450, "Column width is exceeding the limit");

    _isSortInAscendingOrder = true;
    _sortComparatorType = JointResultsEntryComparatorEnum.DEFAULT;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setupColumnList()
  {    
    if (_isDisplayAllMeasurements)
    {      
      _columnInfo.put(COMPONENT_NAME, new Pair<String,Integer>(StringLocalizer.keyToString("RMGUI_COMPONENT_NAME_COLUMN_KEY"), new Integer(60)));
      _columnInfo.put(PIN_NAME, new Pair<String,Integer>(StringLocalizer.keyToString("RMGUI_PIN_NAME_COLUMN_KEY"), new Integer(50)));
      _columnInfo.put(SLICE_NAME, new Pair<String,Integer>(StringLocalizer.keyToString("RMGUI_SLICE_NAME_COLUMN_KEY"), new Integer(10)));
      _columnInfo.put(MEASUREMENT_NAME, new Pair<String,Integer>(StringLocalizer.keyToString("RMGUI_MEASUREMENT_NAME_COLUMN_KEY"), new Integer(10)));
      _columnInfo.put(MEASUREMENT_UNIT, new Pair<String,Integer>(StringLocalizer.keyToString("RMGUI_MEASUREMENT_UNIT_COLUMN_KEY"), new Integer(10)));
      _columnInfo.put(BOARD_NAME, new Pair<String,Integer>(StringLocalizer.keyToString("RDGUI_BOARD_NAME_COLUMN_KEY"), new Integer(60)));
      _columnInfo.put(RUN_ID, new Pair<String,Integer>(StringLocalizer.keyToString("RMGUI_RUN_ID_COLUMN_KEY"), new Integer(80)));
      _columnInfo.put(COMPONENT_LOCATION, new Pair<String,Integer>(StringLocalizer.keyToString("RDGUI_SIDE_COLUMN_KEY"), new Integer(50)));
      _columnInfo.put(PASS_FAIL, new Pair<String,Integer>(StringLocalizer.keyToString("RMGUI_PASS_FAIL_COLUMN_KEY"), new Integer(50)));
      _columnInfo.put(MEASUREMENT, new Pair<String,Integer>(StringLocalizer.keyToString("RMGUI_MEASUREMENT_VALUE_COLUMN_KEY"), new Integer(70)));
    }
    else
    {
      // Default column width = 75.  Make sure sum of desired widths is <= 450
      // or new widths won't be accepted.
      _columnInfo.put(COMPONENT_NAME, new Pair<String,Integer>(StringLocalizer.keyToString("RMGUI_COMPONENT_NAME_COLUMN_KEY"), new Integer(60)));
      _columnInfo.put(PIN_NAME, new Pair<String,Integer>(StringLocalizer.keyToString("RMGUI_PIN_NAME_COLUMN_KEY"), new Integer(50)));
      _columnInfo.put(BOARD_NAME, new Pair<String,Integer>(StringLocalizer.keyToString("RDGUI_BOARD_NAME_COLUMN_KEY"), new Integer(60)));
      _columnInfo.put(RUN_ID, new Pair<String,Integer>(StringLocalizer.keyToString("RMGUI_RUN_ID_COLUMN_KEY"), new Integer(80)));
      _columnInfo.put(COMPONENT_LOCATION, new Pair<String,Integer>(StringLocalizer.keyToString("RDGUI_SIDE_COLUMN_KEY"), new Integer(50)));
      _columnInfo.put(PASS_FAIL, new Pair<String,Integer>(StringLocalizer.keyToString("RMGUI_PASS_FAIL_COLUMN_KEY"), new Integer(50)));
      _columnInfo.put(MEASUREMENT, new Pair<String,Integer>(StringLocalizer.keyToString("RMGUI_MEASUREMENT_VALUE_COLUMN_KEY"), new Integer(70)));
    }
  }

  /**
   * @author Rex Shang
   */
  void populateWithTestedJoints(String projectName, List<Long> inspectionRuns, Subtype subtype, MeasurementEnum measurementType,
      SliceNameEnum sliceType)
  {
    Assert.expect(projectName != null);
    Assert.expect(inspectionRuns != null);
    Assert.expect(subtype != null);
    Assert.expect(measurementType != null);
    Assert.expect(sliceType != null);

    _projectName = projectName;
    _inspectionRuns = inspectionRuns;
    _subtype = subtype;
    _measurementType = measurementType;
    _sliceType = sliceType;

    debug(_me + "populateWithTestedJoints(" + _subtype + ")");

    _testedJoints = _testedJointsManager.getResultsBySubtypeMeasurementTypeSliceName(
      _projectName, _inspectionRuns, _subtype, _measurementType, _sliceType);

    // Update name for MEASUREMENT column with measurement unit.
    setColumnNameForMeasurementColumn();

    sortTableData();

    // In order to update the new MEASUREMENT column name, we need to update
    // the whole table structure instead of just table data.
    fireTableStructureChanged();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void populateWithSingleSliceAllMeasurementTestedJoints(String projectName, 
                                                                List<Long> inspectionRuns, 
                                                                Subtype subtype, 
                                                                MeasurementEnum measurementType, 
                                                                SliceNameEnum sliceType)
  {
    Assert.expect(projectName != null);
    Assert.expect(inspectionRuns != null);
    Assert.expect(subtype != null);
    Assert.expect(measurementType != null);
    Assert.expect(sliceType != null);

    _projectName = projectName;
    _inspectionRuns = inspectionRuns;
    _subtype = subtype;
    _measurementType = measurementType;
    _sliceType = sliceType;

    debug(_me + "populateWithTestedJoints(" + _subtype + ")");

    _testedJoints.addAll(_testedJointsManager.getResultsBySubtypeMeasurementTypeSliceName(
        _projectName, _inspectionRuns, _subtype, _measurementType, _sliceType));    

    sortTableData();

    // In order to update the new MEASUREMENT column name, we need to update
    // the whole table structure instead of just table data.
    fireTableStructureChanged();
  }

  /**
   * User prefer to see the unit associated with a measurement.  This method
   * is used to update the MEASUREMENT column heading with project unit.
   * @author Rex Shang
   */
  private void setColumnNameForMeasurementColumn()
  {
    for (JointResultsEntry entry : _testedJoints)
    {
      List<MeasurementResult> measurementResults = entry.getAllMeasurementResults();
      for (MeasurementResult measurementResult : measurementResults)
      {
        if (measurementResult.getMeasurementEnum().equals(_measurementType) && measurementResult.getSliceNameEnum().equals(_sliceType))
        {
          MeasurementUnitsEnum measurementUnitsEnum = measurementResult.getMeasurementUnitsEnum();
          if (measurementUnitsEnum != null)
          {
            Object value = measurementResult.getValue();

            Pair<MeasurementUnitsEnum, Object> convertedValue = MeasurementUnitsEnum.convertToProjectUnitsIfNecessary(measurementUnitsEnum, value);
            measurementUnitsEnum = convertedValue.getFirst();

            if (measurementUnitsEnum.getName().length() != 0)
            {
              _columnNames.set(MEASUREMENT, StringLocalizer.keyToString("RMGUI_MEASUREMENT_VALUE_COLUMN_KEY")
                               + " (" + measurementUnitsEnum.getName() + ")");
            }
            return;
          }
        }
      }
    }
  }

  /**
   * @author Rex Shang
   */
  void clearTableModel()
  {
    debug(_me + "clearTestedJoints()");

    _testedJoints.clear();
    _testedJointsManager.clearComponentResults();

    _columnNames.set(MEASUREMENT, StringLocalizer.keyToString("RMGUI_MEASUREMENT_VALUE_COLUMN_KEY"));
    fireTableStructureChanged();
    _subtype = null;
    
    _columnInfo.clear();
  }

  /**
   * Use by table to adjusts column widths.
   * @author Rex Shang
   */
  void setColumnWidths(JSortTable myTable)
  {
    Assert.expect(myTable != null);

    for (int i = 0; i < getColumnCount(); i++)
    {
      TableColumn column = myTable.getColumnModel().getColumn(i);
      column.setPreferredWidth(_columnWidths.get(i).intValue());
    }
  }

  /**
   * @param row the entry we need to get
   * @return a particular tested joint result indicated by the row number.
   * @author Rex Shang
   */
  JointResultsEntry getTestedJointResultAt(int row)
  {
    Assert.expect(row >= 0 && row < _testedJoints.size());

    debug(_me + "getTestedJointResultAt(" + row + ")");

    return _testedJoints.get(row);
  }

  /**
   * @return the column count for this table model
   * @author Rex Shang
   */
  public int getColumnCount()
  {
    return _columnNames.size();
  }

  /**
   * @author Rex Shang
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return _columnNames.get(columnIndex);
  }

  /**
   * Determines if a column is sortable.
   * @author Rex Shang
   */
  public boolean isSortable(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return true;
  }

  /**
   * Provide sorting for the various columns.
   * @author Rex Shang
   */
  public void sortColumn(int columnIndex, boolean ascending)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    debug(_me + "ascending = " + ascending + "; column = " + columnIndex);

    _isSortInAscendingOrder = ascending;
    if (columnIndex == COMPONENT_NAME)
    {
      _sortComparatorType = JointResultsEntryComparatorEnum.COMPONENT_NAME;
    }
    else if (columnIndex == PIN_NAME)
    {
      _sortComparatorType = JointResultsEntryComparatorEnum.PIN_NAME;
    }
    else if (columnIndex == BOARD_NAME)
    {
      _sortComparatorType = JointResultsEntryComparatorEnum.BOARD_NAME;
    }
    else if (columnIndex == RUN_ID)
    {
      _sortComparatorType = JointResultsEntryComparatorEnum.RUN_ID;
    }
    else if (columnIndex == COMPONENT_LOCATION)
    {
      _sortComparatorType = JointResultsEntryComparatorEnum.SIDE;
    }
    else if (columnIndex == PASS_FAIL)
    {
      _sortComparatorType = JointResultsEntryComparatorEnum.PASS_FAIL;
    }
    else if (columnIndex == MEASUREMENT)
    {
      _sortComparatorType = JointResultsEntryComparatorEnum.MEASUREMENT_VALUE;
    }
    else if (columnIndex == SLICE_NAME)
    {
      // Do nothing
    }
    else if (columnIndex == MEASUREMENT_NAME)
    {
      // Do nothing
    }
    else if (columnIndex == MEASUREMENT_UNIT)
    {
      // Do nothing
    }
    else
    {
      // this should not happen if so then assert
      Assert.expect(false, "Column does not exist.");
    }

    sortTableData();
    fireTableDataChanged();
  }

  /**
   * @author Rex Shang
   */
  private void sortTableData()
  {
    // Bypass sort when there are less than 2 items.
    if (getRowCount() > 1)
    {
      if (_sortComparatorType.equals(JointResultsEntryComparatorEnum.MEASUREMENT_VALUE))
        Collections.sort(_testedJoints, new JointResultsEntryComparator(_isSortInAscendingOrder, _sortComparatorType, _measurementType, _sliceType));
      else
        Collections.sort(_testedJoints, new JointResultsEntryComparator(_isSortInAscendingOrder, _sortComparatorType));
    }
  }

  /**
   * @return the row count for the currently loaded data
   * @author Rex Shang
   */
  public int getRowCount()
  {
    if (_testedJoints == null)
      return 0;
    else
      return _testedJoints.size();
  }

  /**
   * @return this method will always return false since the table can not be edited.
   * @author Rex Shang
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return false;
  }

  /**
   * @param rowIndex the row index
   * @param columnIndex the column index
   * @return the Tested Joint Result found at a given row index and column index.
   * @author Rex Shang
   * @author Patrick Lacz
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    JointResultsEntry jointResult = _testedJoints.get(rowIndex);
    if (columnIndex == COMPONENT_NAME)
    {
      return jointResult.getReferenceDesignator();
    }
    else if (columnIndex == PIN_NAME)
    {
      return jointResult.getPadName();
    }
    else if (columnIndex == BOARD_NAME)
    {
      return jointResult.getBoardName();
    }
    else if (columnIndex == RUN_ID)
    {
      return jointResult.getRunStartTime();
    }
    else if (columnIndex == COMPONENT_LOCATION)
    {
      if (jointResult.isTopSide())
        return _TOP;
      else
        return _BOTTOM;
    }
    else if (columnIndex == PASS_FAIL)
    {
      if (jointResult.passed())
        return _PASS;
      else
        return _FAIL;
    }
    else if (columnIndex == MEASUREMENT)
    {
      // Find the measurement result specified by measurement type and slice type.
      List<MeasurementResult> measurementResults = jointResult.getAllMeasurementResults();
      for (MeasurementResult measurementResult : measurementResults)
      {        
        if ((measurementResult.getMeasurementEnum().toString().equalsIgnoreCase(getValueAt(rowIndex,MEASUREMENT_NAME).toString()) && 
             measurementResult.getSliceNameEnum().toString().equalsIgnoreCase(getValueAt(rowIndex,SLICE_NAME).toString())))
        {
          MeasurementUnitsEnum measurementUnitsEnum = measurementResult.getMeasurementUnitsEnum();
          Object value = measurementResult.getValue();

          Pair<MeasurementUnitsEnum, Object> convertedValue = MeasurementUnitsEnum.convertToProjectUnitsIfNecessary(measurementUnitsEnum, value);
          measurementUnitsEnum = convertedValue.getFirst();
          value = convertedValue.getSecond();

          String valueString = MeasurementUnitsEnum.formatNumberIfNecessary(measurementUnitsEnum, value);

          return valueString;
//          return MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(measurementUnitsEnum, valueString);
        }
      }
      return new Double(Double.NaN);
    }
    else if (columnIndex == SLICE_NAME)
    {
      if (jointResult.hasSliceName())
        return jointResult.getSliceName();
      return "";
    }
    else if (columnIndex == MEASUREMENT_NAME)
    {
      if (jointResult.hasMeasurementName())
        return jointResult.getMeasurementName();
      return "";
    }
    else if (columnIndex == MEASUREMENT_UNIT)
    {
      if (jointResult.hasMeasurementUnit())
        return jointResult.getMeasurementUnit();
      return "";
    }
    else
    {
      // this should not happen if so then assert
      Assert.expect(false);
      return null;
    }
  }


  /**
   * Editing of the table is not allowed.  This method does nothing.
   * @author Rex Shang
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    // Do nothing.
  }

  /**
   * Return the list of joint results that is currently in our table.
   * @author Rex Shang
   */
  List<JointResultsEntry> getJointEntries()
  {
    return _testedJoints;
  }

  /**
   * @author George Booth
   */
  JointResultsEntry getJointResultsEntryAt(int row)
  {
    Assert.expect(row >= 0 && row < _testedJoints.size());

    return _testedJoints.get(row);
  }

  /**
   * @author George Booth
   */
  int getRowForJointResultsEntry(JointResultsEntry jointResultsEntry)
  {
    if (jointResultsEntry == null || _testedJoints.size() == 0)
    {
      return - 1;
    }
//    System.out.println("looking for " + jointResultsEntry.getReferenceDesignator() + " " + jointResultsEntry.getPadName());
    int row = -1;
    for (JointResultsEntry entry : _testedJoints)
    {
      row++;
//      System.out.println("testing " + entry.getReferenceDesignator() + " " + entry.getPadName());
      if (entry.getRunStartTime().longValue() == jointResultsEntry.getRunStartTime().longValue() &&
          entry.getBoardName().equals(jointResultsEntry.getBoardName()) &&
          entry.getReferenceDesignator().equals(jointResultsEntry.getReferenceDesignator()) &&
          entry.getPadName().equals(jointResultsEntry.getPadName()))
      {
        return row;
      }
    }
    return -1;
  }

  /**
   * @author George Booth
   */
  Long getRunIdAt(int row)
  {
    Assert.expect(row >= 0 && row < _testedJoints.size());

    return _testedJoints.get(row).getRunStartTime();
  }

  /**
   * Return the component type at the index
   * @author George Booth
   */
  ComponentType getComponentTypeAt(int row)
  {
    Assert.expect(row >= 0 && row < _testedJoints.size());

    Project project = Project.getCurrentlyLoadedProject();
    // get board reference from board name
    Board board = project.getPanel().getBoard((String)getValueAt(row, BOARD_NAME));

    String refDesignator = _testedJoints.get(row).getReferenceDesignator();
    Component comp = board.getComponent(refDesignator);
    ComponentType compType = comp.getComponentType();
    return compType;
  }

  /**
   * @author Rex Shang
   */
  private void debug(String message)
  {
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.REVIEW_MEASUREMENT_PANEL_DEBUG))    
      System.out.println(_me + "." + message);
  }
}
