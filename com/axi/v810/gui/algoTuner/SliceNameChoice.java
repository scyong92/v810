package com.axi.v810.gui.algoTuner;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

/**
 * This class contains the user choices for SliceNameEnum.
 * The choices are either a particular SliceNameEnum or "All"
 * @author Rex Shang
 */
public class SliceNameChoice implements Serializable
{
  SliceNameEnum _sliceNameEnum;
  private boolean _all;

  /**
   * @author Rex Shang
   */
  public SliceNameChoice(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    _sliceNameEnum = sliceNameEnum;
    _all = false;
  }

  /**
   * @author Rex Shang
   */
  public SliceNameChoice()
  {
    _all = true;
  }

  /**
   * @author Rex Shang
   */
  public boolean isAllFamilies()
  {
    return _all;
  }

  /**
   * @author Rex Shang
   */
  public SliceNameEnum getSliceNameEnum()
  {
    Assert.expect(_all == false);
    Assert.expect(_sliceNameEnum != null);
    return _sliceNameEnum;
  }

  /**
   * Objects are equal if they specify the same algorithm family
   * @author Rex Shang
   */
  public boolean equals(Object rhs)
  {
    if (rhs == null)
      return false;

    if (rhs == this)
      return true;

    if (rhs instanceof SliceNameChoice)
    {
      SliceNameChoice afc = (SliceNameChoice)rhs;

      // if both specify ALL, then they are equal
      if((_all) && (afc._all))
        return true;
      else if(_all != afc._all) // if one specifies all, and the other does not, they are not equal
        return false;
      else
      {
        // if they both specify the same specific enum, they are equal
        if(_sliceNameEnum.equals(afc._sliceNameEnum))
          return true;
        else
          return false;
      }
    }
    else
      return false;
  }

  /**
   * @author Rex Shang
   */
  public int hashcode()
  {
    if (_all)
      return -1;
    else
      return _sliceNameEnum.hashCode();
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    if (_all)
      return StringLocalizer.keyToString("ATGUI_ALL_KEY");
    else
      return _sliceNameEnum.toString();
  }

}
