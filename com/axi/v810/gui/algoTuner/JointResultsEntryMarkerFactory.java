package com.axi.v810.gui.algoTuner;

import org.jfree.chart.plot.*;
import org.jfree.ui.TextAnchor;
import java.awt.Color;
import org.jfree.ui.RectangleAnchor;

/**
 * This factory class is used to generate markers to be displayed in
 * JointResultsEntry plots.
 * @author Rex Shang
 */
public class JointResultsEntryMarkerFactory
{
  private final static Color _solidColor = Color.BLACK;
  private final static Color _transparentGrayColor;

  /**
   * @author Rex Shang
   */
  static
  {
    Color grayColor = Color.GRAY;
    _transparentGrayColor = new Color(grayColor.getRed(), grayColor.getGreen(), grayColor.getBlue(), 64);
  }

  /**
   * Never instanciate.
   * @author Rex Shang
   */
  private JointResultsEntryMarkerFactory()
  {
  }

  /**
   * @author Rex Shang
   */
  static ValueMarker getValueMarker(double value, String label)
  {
    ValueMarker myMarker = new ValueMarker(value);
    myMarker.setPaint(_solidColor);
    myMarker.setLabelTextAnchor(TextAnchor.BOTTOM_LEFT);
    myMarker.setLabel(label);

    return myMarker;
  }

  /**
   * @author Rex Shang
   */
  static IntervalMarker getIntervalMarker(double valueLow, double valueHigh, String label,
    RectangleAnchor labelAnchor, TextAnchor labelTextAnchor)
  {
    IntervalMarker myMarker = new IntervalMarker(valueLow, valueHigh);

    myMarker.setLabelAnchor(labelAnchor);
    myMarker.setLabelTextAnchor(labelTextAnchor);
//    myMarker.setLabelTextAnchor(TextAnchor.TOP_LEFT);
    myMarker.setLabel(label);
    myMarker.setPaint(_transparentGrayColor);

    return myMarker;
  }
}
