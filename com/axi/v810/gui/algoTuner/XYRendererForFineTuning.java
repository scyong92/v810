/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.algoTuner;

import java.awt.*;
import org.jfree.chart.renderer.xy.*;

/**
 * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
 *
 * @author weng-jian.eoh
 */
public class XYRendererForFineTuning extends XYLineAndShapeRenderer
{

  private java.util.List<Integer> _selectedRow;

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public XYRendererForFineTuning(boolean lines, boolean shapes, java.util.List<Integer> filterRow)
  {
    super(lines, shapes);
    _selectedRow = filterRow;
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  @Override
  public Paint getItemPaint(int row, int col)
  {
    if (_selectedRow.contains(col))
    {
      return Color.CYAN;
    }
    else
    {
      return super.getItemPaint(row, col);
    }
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  @Override
  protected void drawFirstPassShape(Graphics2D g2, int pass, int series,
    int item, Shape shape)
  {
    g2.setStroke(getItemStroke(series, item));
    Color c1 = Color.RED;
    Color c2 = Color.RED;
    GradientPaint linePaint = new GradientPaint(0, 0, c1, 0, 300, c2);
    g2.setPaint(linePaint);
    g2.draw(shape);
  }

}
