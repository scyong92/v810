package com.axi.v810.gui.algoTuner;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;
import com.axi.guiUtil.*;

/**
 * @author Andy Mechtenberg
 */
class DiagnosticTextInfoObserver implements Observer
{
  private RunInspectionPanel _mainFrame = null;

  /**
   * @author Andy Mechtenberg
   */
  public DiagnosticTextInfoObserver()
  {
    // do nothing
  }

  /**
   * @author Andy Mechtenberg
   */
  public DiagnosticTextInfoObserver(RunInspectionPanel gui)
  {
    Assert.expect(gui != null);
    _mainFrame = gui;
  }

 /**
  * This must by synchronous - do not make any calls back into the server on this thread
  * or you will get a deadlock situation
  *
  * @author Peter Esbensen
  * @author Matt Wharton
  */
  public synchronized void update(final Observable observable, final Object arg)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        // get the image and "paint" the component with it
        Assert.expect(observable != null);
        Assert.expect(arg != null);

        if (arg instanceof AlgorithmDiagnosticInspectionEvent)
        {
          AlgorithmDiagnosticInspectionEvent algorithmDiagnosticEvent = (AlgorithmDiagnosticInspectionEvent)arg;
          if (algorithmDiagnosticEvent.hasAlgorithmDiagnosticMessage())
          {
            AlgorithmDiagnosticMessage algorithmDiagnosticMessage = algorithmDiagnosticEvent.getAlgorithmDiagnosticMessage();
            if (algorithmDiagnosticMessage.hasTextualDiagnosticInfo())
            {
              for (DiagnosticInfo diagnosticInfo : algorithmDiagnosticMessage.getDiagnosticInfoList())
              {
                if (diagnosticInfo instanceof TextualDiagnosticInfo)
                {
                  TextualDiagnosticInfo textualDiagnosticInfo = (TextualDiagnosticInfo)diagnosticInfo;
                  _mainFrame.addInformationMessage(StringLocalizer.keyToString(textualDiagnosticInfo.getDiagnosticText()));
                }
              }

            }
          }
        }
        else if (arg instanceof MessageInspectionEvent)
        {
          MessageInspectionEvent messageInspectionEvent = (MessageInspectionEvent)arg;
          _mainFrame.addInformationMessage(messageInspectionEvent.getMessage());
        }
      }
    });
  }
}
