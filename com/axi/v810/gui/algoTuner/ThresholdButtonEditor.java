package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;

/**
 * This class handles those thresholds which are edited via a popup dialog
 * @author Andy Mechtenberg
 */
public class ThresholdButtonEditor extends DefaultCellEditor
{
  protected JButton button;
  private String _label;
  private boolean _isPushed;
  private JFrame _frame;
  private PanelDataAdapter _panelDataAdapter;
  private AlgorithmEnum _algorithm;
  private String _threshName;
  private Point _editLocationPoint;

  /**
   * The specialThreshold parameter specifies whether we create an intelligent dialog special for EXCLUDE_REGIONS thresholds
   * which in template short and solderball algorithms.  Right now, (6/19/2001) there is only 1 special case, if we need to
   * add more in future, change this boolean to an enumerated type.
   * @author Andy Mechtenberg
   */
  public ThresholdButtonEditor(JCheckBox checkBox, JFrame frame, PanelDataAdapter panelDataAdapter,
                               AlgorithmEnum algoEnum, String threshName)
  {
    super(checkBox);

    Assert.expect(checkBox != null);
    Assert.expect(frame != null);
    Assert.expect(panelDataAdapter != null);
    Assert.expect(algoEnum != null);
    Assert.expect(threshName != null);

    _frame = frame;
    _panelDataAdapter = panelDataAdapter;
    _algorithm = algoEnum;
    _threshName = threshName;
    button = new JButton();
    button.setOpaque(true);
    button.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        fireEditingStopped();
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  public Component getTableCellEditorComponent(JTable table, Object value,
       boolean isSelected, int row, int column)
  {
    if (isSelected)
    {
      button.setForeground(table.getSelectionForeground());
      button.setBackground(table.getSelectionBackground());
    }
    else
    {
      button.setForeground(table.getForeground());
      button.setBackground(table.getBackground());
    }
    _label = (value ==null) ? "" : value.toString();
    button.setText( _label );
    _isPushed = true;
    Point tp = table.getLocationOnScreen();
    Point cp = table.getCellRect(row, column, true).getLocation();
//    System.out.println("Table location is " + tp.x + " " + tp.y);
//    System.out.println("Cell location is " + cp.x + " " + cp.y);
    _editLocationPoint = new Point(tp.x + cp.x, tp.y + cp.y);
    return button;
  }

  /**
   * @author Andy Mechtenberg
   */
  public Object getCellEditorValue()
  {
    if (_isPushed)
    {
      ThresholdSelectDlg dlg = null;
      /** @todo PE fix this!
      if (_panelDataAdapter.getThresholdLegacyName(_algorithm, _threshName).equalsIgnoreCase("EXCLUDE_REGIONS"))
      {
        if (_algorithm.toString().equalsIgnoreCase("Short"))
          dlg = new ExcludeRegionsThresholdShortSelectDlg(_frame, "", true);
        else if (_algorithm.toString().equalsIgnoreCase("Solderball"))
          dlg = new ExcludeRegionsThresholdSolderballSelectDlg(_frame, "", true);
        else
          Assert.expect(false);  // There should be no exlude_regions thresholds outside of short and solderball
      }
      else  // normal generic check-box -- all options valid
        dlg = new ThresholdSelectDlg(_frame, "", true, _panelDataAdapter.getAvailableThresholdValues(_algorithm, _threshName));
      */

      dlg.setCurrentSettings((java.util.List<String>)_panelDataAdapter.getThresholdValue(_algorithm, _threshName));
      dlg.setLocation(_editLocationPoint);
      //System.out.println("Before show");
      dlg.setResizable(false);

      dlg.setVisible(true);

      return dlg.getListOfSettings();
    }
    _isPushed = false;

    // if not pushed, then return the current settings
    return _panelDataAdapter.getThresholdValue(_algorithm, _threshName);
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean stopCellEditing()
  {
    //System.out.println("stopCellEditing");
    _isPushed = false;
    return super.stopCellEditing();
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void fireEditingStopped()
  {
    //System.out.println("Fire!");
    super.fireEditingStopped();
  }
}
