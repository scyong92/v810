/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.algoTuner;

import com.axi.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

/**
 *
 * @author weng-jian.eoh
 */
public class MeasurementJointHighlightTable extends JTable
{

  public static final int _BOARD_NAME_VALUE_INDEX = 0;
  public static final int _COMPONENT_NAME_VALUE_INDEX = 1;
  public static final int _JOINT_PAD_VALUE_INDEX = 2;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _BOARD_NAME_VALUE_COLUMN_WIDTH_PERCENTAGE = .20;
  private static final double _COMPONENT_NAME_VALUE_COLUMN_WIDTH_PERCENTAGE = .50;
  private static final double _JOINT_PAD_VALUE_COLUMN_WIDTH_PERCENTAGE = .30;

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public MeasurementJointHighlightTable(TableModel tableModel)
  {
    super(tableModel);
    getTableHeader().setReorderingAllowed(false);
    setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    setPreferredColumnWidths();
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public void renderColumns(TableCellRenderer cellRender)
  {
    for (int i = 0; i < this.getModel().getColumnCount(); i++)
    {
      renderColumn(this.getColumnModel().getColumn(i), cellRender);
    }
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public void renderColumn(TableColumn col, TableCellRenderer cellRender)
  {
    try
    {
      col.setCellRenderer(cellRender);
    }
    catch (Exception e)
    {
      Assert.expect(false, e.getMessage());
    }
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_BOARD_NAME_VALUE_INDEX).setPreferredWidth((int) (totalColumnWidth * _BOARD_NAME_VALUE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_COMPONENT_NAME_VALUE_INDEX).setPreferredWidth((int) (totalColumnWidth * _COMPONENT_NAME_VALUE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_JOINT_PAD_VALUE_INDEX).setPreferredWidth((int) (totalColumnWidth * _JOINT_PAD_VALUE_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public String getToolTipText(MouseEvent e)
  {
    Point p = e.getPoint();
    int row = rowAtPoint(p);
    int col = columnAtPoint(p);

    if (row >= 0 && col >= 0)
    {
      return getValueAt(row, col).toString();
    }
    else
    {
      return null;
    }
  }
}
