package com.axi.v810.gui.algoTuner;

import java.util.*;

import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * <p>Description:  This class is the table model for the defect details table in Review Defects.
 * This table model is responsible for getting and filtering the data. There will be 5 columns:<br>
 * Indictment Name<br>
 * Indictment Type (Passed, Failed, Related)<br>
 * Measurement Name<br>
 * Measurement Value<br>
 * Slice<br></p>
 *<p>The indictment details will be extracted from the tested joint result from the Joint
 * Inforamtion Table.
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class DefectDetailsTableModel extends DefaultSortTableModel
{
  private boolean _debug = false;
  private String _me = "DefectDetailsTableModel().";

  // the column names for this table
  private final String[] _defectDetailsTableColumnNames =
  {
    StringLocalizer.keyToString("RDGUI_DEFECT_CALL_COLUMN_KEY"),
    StringLocalizer.keyToString("RDGUI_FAILED_RELATED_COLUMN_KEY"),
    StringLocalizer.keyToString("RDGUI_MEASUREMENT_COLUMN_KEY"),
    StringLocalizer.keyToString("RDGUI_VALUE_COLUMN_KEY"),
    StringLocalizer.keyToString("RDGUI_SLICE_COLUMN_KEY")
  };
  // default column width = 75.  Make sure sum of desired widths is <= 300
  // or new widths won't be accepted
  private final int[] _defectDetailsTableColumnWidths =
  {
     50,   // Defect Call
     50,   // Type (Failed or Related)
    140,   // Measurement
     40,   // Value
     70    // Slice Name
  };

  // List of indictment details
  private ArrayList<IndictmentDetailsEntry> _jointResultDetails = null;
  // passing or failing data?
  private boolean _failingData = true;

  // Measurements the user does not need to see
  private List<MeasurementEnum> nonVisibleMeasurements = MeasurementEnum.getNonCustomerVisibleMeasurements();

  // default to sort by component, ascending
  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;

  /**
   * @author George Booth
   */
  public DefectDetailsTableModel()
  {
    // do nothing.
  }

  /**
   * This method will get the data for this table model for failing joints. It is based on the
   * current selected joint result from the pin information table.
   * If data is null, the table is cleared.
   * @author George Booth
   */
  void populateWithDefectiveJoints(JointResultsEntry currentJointResultEntry, SliceNameEnum slice,
                                   List<IndictmentEnum> selectedIndictments)
  {
    _jointResultDetails = getDefectiveJoints(currentJointResultEntry, slice, selectedIndictments);
    _failingData = true;
    fireTableDataChanged();
  }

  /**
   * This method will get a list of indictment details for failing joints. It is based on the
   * current selected joint result from the pin information table.
   * @author George Booth
   */
  ArrayList<IndictmentDetailsEntry> getDefectiveJoints(JointResultsEntry currentJointResultsEntry, SliceNameEnum slice,
                                   List<IndictmentEnum> selectedIndictments)
  {
    ArrayList<IndictmentDetailsEntry> jointResultDetails = new ArrayList<IndictmentDetailsEntry>();

    if (currentJointResultsEntry != null)
    {
      List<IndictmentResult> indictments = currentJointResultsEntry.getIndictmentResults();

      // now create a failed tested joint detail for each indictment.
      int indictmentKey = -1;
      for (IndictmentResult indictment : indictments)
      {
        if (selectedIndictments == null ||
            selectedIndictments.contains(indictment.getIndictmentEnum()))
        {
          // inictmentKey is used to group all measurements for an idictment
          ++indictmentKey;
          // get the name of the indictment so we can create a list of failing joints for this
          IndictmentEnum indictmentEnum = indictment.getIndictmentEnum();

          // get the failed measurements for this indictment and create one failed tested joint detail for
          // each failing measurement and related measurement.
          List<MeasurementResult> measurements =
              new LinkedList<MeasurementResult>(indictment.getFailingMeasurementResults());
          for (MeasurementResult failedJointMeasurement : measurements)
          {
            // don't show neasurements the user doesn't care about
            if (nonVisibleMeasurements.contains(failedJointMeasurement.getMeasurementEnum()) == false)
            {
              // check for selected slice
              if (slice == null ||
                  slice.equals(failedJointMeasurement.getSliceNameEnum()))
              {
                // now for each failing measurement create an indictment details entry
                IndictmentDetailsEntry defectInformation = new IndictmentDetailsEntry();
                defectInformation.setIndictmentKey(indictmentKey);
                defectInformation.setIndictment(indictmentEnum);
                defectInformation.setFailingMeasurement();
                defectInformation.setMeasurement(failedJointMeasurement);
                jointResultDetails.add(defectInformation);
              }
            }
          }

          measurements = new LinkedList<MeasurementResult>(indictment.getRelatedMeasurementResults());
          for (MeasurementResult relatedJointMeasurement : measurements)
          {
            // don't show neasurements the user doesn't care about
            if (nonVisibleMeasurements.contains(relatedJointMeasurement.getMeasurementEnum()) == false)
            {
              // check for selected slice
              if (slice == null ||
                  slice.equals(relatedJointMeasurement.getSliceNameEnum()))
              {
                // now for each related measurement create an indictment details entry
                IndictmentDetailsEntry defectInformation = new IndictmentDetailsEntry();
                defectInformation.setIndictmentKey(indictmentKey);
                defectInformation.setIndictment(indictmentEnum);
                defectInformation.setRelatedMeasurement();
                defectInformation.setMeasurement(relatedJointMeasurement);
                jointResultDetails.add(defectInformation);
              }
            }
          }
        }
      }
    }
    return jointResultDetails;
  }
  
  /**
   * @author Ngie Xing, April 2014
   */
  public IndictmentDetailsEntry getSelectedJointDetails(int selectedRow) 
  {
    Assert.expect(_jointResultDetails!=null);
    IndictmentDetailsEntry jointResultDetail= _jointResultDetails.get(selectedRow);
    return jointResultDetail;
  }

  /**
   * This method will get the data for this table model for passing joints. It is based on the
   * current selected joint result from the pin information table.
   * If data is null, the table is cleared.
   * @author George Booth
   */
  void populateWithPassingJoints(JointResultsEntry currentJointResultsEntry, SliceNameEnum slice,
                                   List<IndictmentEnum> selectedIndictments)
  {
    _jointResultDetails = getPassingJoints(currentJointResultsEntry, slice);

    _failingData = false;
    fireTableDataChanged();
  }

  /**
   * This method will get a list of measurement details for passing joints. It is based on the
   * current selected joint result from the pin information table.
   * @author George Booth
   */
  ArrayList<IndictmentDetailsEntry> getPassingJoints(JointResultsEntry currentJointResultsEntry, SliceNameEnum slice)
  {
    ArrayList<IndictmentDetailsEntry> jointResultDetails = new ArrayList<IndictmentDetailsEntry>();

    if (currentJointResultsEntry != null)
    {
      List<MeasurementResult> measurements = currentJointResultsEntry.getAllMeasurementResults();

      // create a joint detail for each measurement.
      for (MeasurementResult jointMeasurement : measurements)
      {
        // don't show neasurements the user doesn't care about
        if (nonVisibleMeasurements.contains(jointMeasurement.getMeasurementEnum()) == false)
        {
          // check for selected slice
          if (slice == null ||
              slice.equals(jointMeasurement.getSliceNameEnum()))
          {
            IndictmentDetailsEntry defectInformation = new IndictmentDetailsEntry();
            defectInformation.setPassingMeasurement();
            defectInformation.setMeasurement(jointMeasurement);
            jointResultDetails.add(defectInformation);
          }
        }
      }
    }
    return jointResultDetails;
  }

  /**
   * @author George Booth
   */
  public void setColumnWidths(JSortTable myTable) {
    Assert.expect(myTable != null);

    TableColumn column = null;
    // set all columns except for last - it takes up the rest of the space
    int columnCount = this.getColumnCount();
    for (int i = 0; i < columnCount - 1; i++)
    {
      column = myTable.getColumnModel().getColumn(i);
      column.setPreferredWidth(_defectDetailsTableColumnWidths[i]);
    }
  }

  /**
   * @author George Booth
   */
  public int getColumnCount()
  {
    return _defectDetailsTableColumnNames.length;
  }

  /**
   * @author George Booth
   */
  public int getRowCount()
  {
    if(_jointResultDetails == null)
      return 0;
    else
      return _jointResultDetails.size();
  }

  /**
   * @author George Booth
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_defectDetailsTableColumnNames[columnIndex];
  }

  /**
   * @author George Booth
   */
  public boolean isSortable(int columnIndex)
  {
    if (_failingData)
    {
      // data is ordered by failed and related measurements; not sortable
      return false;
    }
    else
    {
      if (columnIndex == 0 || columnIndex == 1)
      {
        // passing indictment is always blank
        // passing type is always "Passed"
        return false;
      }
      else
      {
        return true;
      }
    }
  }

  /**
   * @author George Booth
   */
  public void sortColumn(int columnIndex, boolean ascending)
  {
    if (_failingData)
    {
      return;
    }

    _currentSortColumn = columnIndex;
    _currentSortAscending = ascending;

    // sort passing data
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());

    switch (columnIndex)
    {
      case 0: // Indictment name
        Collections.sort(_jointResultDetails,
          new IndictmentDetailsComparator(ascending, IndictmentDetailsComparatorEnum.INDICTMENT));
        break;
      case 1: // Indictment type
        Collections.sort(_jointResultDetails,
          new IndictmentDetailsComparator(ascending, IndictmentDetailsComparatorEnum.TYPE));
        break;
      case 2: // Measurement name
        Collections.sort(_jointResultDetails,
          new IndictmentDetailsComparator(ascending, IndictmentDetailsComparatorEnum.MEASUREMENT));
        break;
      case 3: // Measurement value
        Collections.sort(_jointResultDetails,
          new IndictmentDetailsComparator(ascending, IndictmentDetailsComparatorEnum.VALUE));
        break;
      case 4: // Slice
        Collections.sort(_jointResultDetails,
          new IndictmentDetailsComparator(ascending, IndictmentDetailsComparatorEnum.SLICE));
        break;
      default:
        // this should not happen if so then assert
        Assert.expect(false);
        return;
    }
  }

  /**
   * @author George Booth
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return false;
  }

  /**
   * @author George Booth
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    IndictmentDetailsEntry defectInformation = _jointResultDetails.get(rowIndex);

    switch(columnIndex)
    {
      case 0:
        if (defectInformation.getIndictmentKey() == -1)
        {
          // no indictment
          return " ";
        }
        else
        {
          int lastIndictmentKey = -1;
          if (rowIndex > 0)
          {
            IndictmentDetailsEntry lastDefect = _jointResultDetails.get(rowIndex - 1);
            lastIndictmentKey = lastDefect.getIndictmentKey();
          }
          if (lastIndictmentKey != defectInformation.getIndictmentKey())
          {
            return defectInformation.getIndictment().getName();
          }
          else
          {
            return " ";
          }
        }
      case 1: return defectInformation.getIndictmentType();
      case 2: return defectInformation.getMeasurement().getMeasurementEnum().getName();
      case 3:

        Object value = defectInformation.getMeasurement().getValue();
        MeasurementUnitsEnum measurementUnitsEnum = defectInformation.getMeasurement().getMeasurementUnitsEnum();

        Pair<MeasurementUnitsEnum, Object> convertedValue = MeasurementUnitsEnum.convertToProjectUnitsIfNecessary(measurementUnitsEnum, value);
        measurementUnitsEnum = convertedValue.getFirst();
        value = convertedValue.getSecond();

        // truncate to a reasonable number of decimal points.
        String valueString = MeasurementUnitsEnum.formatNumberIfNecessary(measurementUnitsEnum, value);

        // add some indication of the units to the field
        return MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(measurementUnitsEnum, valueString);

      case 4: return defectInformation.getMeasurement().getSliceNameEnum().getName();
      default:
        // this should not happen if so then assert
        Assert.expect(false);
        return null;
    }
  }



  /**
   * @author George Booth
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    // do nothing
  }

  /**
   * @author George Booth
   */
  IndictmentDetailsEntry getDetailsAt(int index)
  {
    Assert.expect(index >= 0 && index < _jointResultDetails.size());

    return _jointResultDetails.get(index);
  }

  /**
   * @author George Booth
   */
  SliceNameEnum getSliceNameEnum(int index)
  {
    Assert.expect(index >= 0 && index < _jointResultDetails.size());

    IndictmentDetailsEntry defectInformation = _jointResultDetails.get(index);
    return defectInformation.getMeasurement().getSliceNameEnum();
  }

  /**
   * @author George Booth
   */
  void clear()
  {
    _jointResultDetails = null;

    _currentSortColumn = -1;
    _currentSortAscending = true;
  }
}
