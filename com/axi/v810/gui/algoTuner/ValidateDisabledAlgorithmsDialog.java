/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.algoTuner;

import com.axi.guiUtil.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import java.util.*;
import javax.swing.event.*;


/**
 * A dialog to display the disabled algorithm for subtype
 *
 * @author weng-jian.eoh
 */
public class ValidateDisabledAlgorithmsDialog extends JDialog
{

  private JFrame _frame = null;
  private JPanel _mainPanel = new JPanel();
  private JPanel _okButtonPanel = new JPanel();
  private JScrollPane _disabledAlgorithmScrollPane;
  private JButton _okButton;
  private BorderLayout _mainPanelBorderLayout;
  private FlowLayout _okButtonPanelFlowLayout;
  private ValidateDisabledAlgorithmsTable _disableAlgorithmTable;
  private ValidateDisabledAlgorithmsTableModel _disableAlgorithmTableModel;
  
  private java.util.List<Subtype> _allSubtype;
  
  /**
   * @author weng-jian.eoh
   * @param frame
   * @param title
   * @param modal
   * @param boardType 
   */
  ValidateDisabledAlgorithmsDialog(JFrame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);

    _frame = frame;
    this.setResizable(false);
    createPanel();
  }

  /**
   * @author weng-jian.eoh
   * @throws Exception 
   */
  public void createPanel()
  {
    jbInit();
    
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _okButtonPanel.setLayout(_okButtonPanelFlowLayout);
    _okButtonPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _okButtonPanelFlowLayout.setHgap(20);
    _okButtonPanelFlowLayout.setVgap(10);
    _okButton.setMnemonic(StringLocalizer.keyToString("GUI_OK_BUTTON_MNEMONIC_KEY").charAt(0));
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });
    _okButtonPanel.add(_okButton);
    _disabledAlgorithmScrollPane.getViewport().add(_disableAlgorithmTable);
    _mainPanel.add(_disabledAlgorithmScrollPane, BorderLayout.NORTH);
    _mainPanel.add(_okButtonPanel, BorderLayout.SOUTH);
    getContentPane().add(_mainPanel);
    pack();
  }
  
  /**
   * @author weng-jian.eoh
   * @throws Exception
   */
  private void jbInit()
  {
    _mainPanel = new JPanel();
    _okButtonPanel = new JPanel();
    _mainPanelBorderLayout = new BorderLayout();
    _okButtonPanelFlowLayout = new FlowLayout(FlowLayout.LEFT, 0, 0);
    _okButton = new JButton();
    _disabledAlgorithmScrollPane = new JScrollPane();
    _disableAlgorithmTableModel = new ValidateDisabledAlgorithmsTableModel();
    _disableAlgorithmTable = new ValidateDisabledAlgorithmsTable(_disableAlgorithmTableModel);
  }
  
  /**
   * @author weng-jian.eoh
   * 
   */
  public void populateDialogWithData(BoardType boardType)
  {
    Assert.expect(boardType != null);
    
    _allSubtype = boardType.getInspectedSubtypes();
    
    populateTable();
    
  }
  /**
   * @author weng-jian.eoh
   * 
   */
  private void populateTable()
  {
    _disableAlgorithmTableModel.populateDisplayData(_allSubtype);
  }
}
