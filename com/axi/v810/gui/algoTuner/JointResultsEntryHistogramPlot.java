package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.util.List;

import org.jfree.chart.axis.*;
import org.jfree.chart.plot.*;
import org.jfree.chart.renderer.xy.*;
import org.jfree.ui.*;

import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * This class is used to generate a JFreeChart plot that will display a historgram
 * for mulitple JointResultsEntry.
 * @author Rex Shang
 */
class JointResultsEntryHistogramPlot extends JointResultsEntryXYPlot
{
  private List<JointResultsEntry> _jointResultsEntries;
  private MeasurementEnum _measurementType;
  private SliceNameEnum _sliceType;

  private JointResultsEntryHistogramDataSet _dataSet;
  private XYItemRenderer _histogramRenderer;

  private int _numberOfBinsForHistogram;
  //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma
  private SigmaTypeEnum _sigmaType;  
  
  public final static int CENTER = 1;
  public final static int LEFT = 2;
  public final static int RIGHT = 3;  

  /**
   * @author Rex Shang
   */
  JointResultsEntryHistogramPlot()
  {
    //Lim, Lay Ngor - XCR 1749 - pass in the sigma type selection from user.
    this(null, null, null, 1, null);
  }

  /**
   * @author Rex Shang
   * @author Lim, Lay Ngor - XCR1749 add sigma type to input parameter
   */
  JointResultsEntryHistogramPlot(List<JointResultsEntry> jointResultsEntries, MeasurementEnum measurementType,
      SliceNameEnum sliceType, int numberOfBinsForHistogram, SigmaTypeEnum sigmaType)
  {
    _jointResultsEntries = jointResultsEntries;
    _measurementType = measurementType;
    _sliceType = sliceType;
    _numberOfBinsForHistogram = numberOfBinsForHistogram;
    //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma
    _sigmaType = sigmaType;    

    drawPlot();
  }

  /**
   * Return the title of the plot.
   * @author Rex Shang
   */
  String getTitle()
  {
    String title;
    String chartType = ReviewMeasurementsChartTypeEnum.HISTOGRAM.toString();

    // Check to see if we have real data.  If not, display "Empty Chart" as title.
    if (_dataSet.getSeriesCount() > 0 && _dataSet.getItemCount(0) > 0)
      title = _measurementType + " " + chartType;
    else
      title = StringLocalizer.keyToString("RMGUI_CHART_EMPTY_CHART_TITLE_KEY") + " " + chartType;

    return title;
  }

  /**
   * Customize our plot.
   * @author Rex Shang
   */
  private void drawPlot()
  {
    _dataSet = new JointResultsEntryHistogramDataSet(
        _jointResultsEntries, _measurementType, _sliceType, _numberOfBinsForHistogram);
    setDataset(_dataSet);

    _histogramRenderer = new XYBarRenderer();
    _histogramRenderer.setToolTipGenerator(new JointResultsEntryHistogramToolTipGenerator());
    setRenderer(_histogramRenderer);

    // Get statistical data.
    double mean = _dataSet.getMean();
    double sigma = _dataSet.getStandardDeviation();
    double median = _dataSet.getMedian();
    
    //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma - START         
    final double threeSigma = 3 * sigma;
    final double fiveSigma = 5 * sigma;    
    Color blue1 = new Color(149, 179, 215, 128);     
    Color blue2 = new Color(191, 209, 231, 128);    
    Color blue3 = new Color(220, 230, 241, 128);    
    // Draw interval marker for different sigma type selection from user.
    addIntervalMarker(mean - sigma, mean + sigma, 1, sigma, blue1, CENTER);
    if(_sigmaType == SigmaTypeEnum.THREE_SIGMA || _sigmaType == SigmaTypeEnum.FIVE_SIGMA)
    {
      addIntervalMarker(mean - threeSigma, mean - sigma, 3, threeSigma, blue2, LEFT);
      addIntervalMarker(mean + sigma, mean + threeSigma, 3, threeSigma, blue2, RIGHT);
    }
    if(_sigmaType == SigmaTypeEnum.FIVE_SIGMA)
    {
      addIntervalMarker(mean - fiveSigma, mean - threeSigma, 5, fiveSigma, blue3, LEFT);
      addIntervalMarker(mean + threeSigma, mean + fiveSigma, 5, fiveSigma, blue3, RIGHT);
    }
    //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma - END
    
    //Draw line marker.
    Marker meanMarker = JointResultsEntryMarkerFactory.getValueMarker(
        mean,
        StringLocalizer.keyToString("RMGUI_CHART_PANEL_MEAN_LABEL_KEY") + " " + getFormattedValueString(mean));
    //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma 
    //avoid overlapping labels with median marker, move the mean marker to center area.
    meanMarker.setLabelAnchor(RectangleAnchor.TOP_RIGHT);   
    meanMarker.setLabelTextAnchor(TextAnchor.TOP_LEFT);
    addDomainMarker(meanMarker, Layer.FOREGROUND);    

    Marker medianMarker = JointResultsEntryMarkerFactory.getValueMarker(
        median,
        StringLocalizer.keyToString("RMGUI_CHART_PANEL_MEDIAN_LABEL_KEY") + " " + getFormattedValueString(median));
    medianMarker.setPaint(Color.BLUE);
    medianMarker.setLabelPaint(Color.BLUE);
    medianMarker.setLabelAnchor(RectangleAnchor.CENTER);   
    medianMarker.setLabelTextAnchor(TextAnchor.TOP_LEFT);
    addDomainMarker(medianMarker, Layer.FOREGROUND);

    setRangeCrosshairLockedOnData(false);
    setDomainCrosshairLockedOnData(false);

    // Set up plot axis.
    String xAxisLabel = StringLocalizer.keyToString("RMGUI_CHART_HISTOGRAM_X_AXIS_LABEL_KEY");
    MeasurementUnitsEnum measurementUnit = _dataSet.getMeasurementUnitsEnum();
    if (measurementUnit != null && measurementUnit.getName().length() != 0)
    {
      xAxisLabel += " (" + measurementUnit.getName() + ")";
    }
    NumberAxis xAxis = new NumberAxis(xAxisLabel);

    xAxis.setAutoRangeIncludesZero(false);
    setDomainAxis(xAxis);

    NumberAxis yAxis = new NumberAxis(StringLocalizer.keyToString("RMGUI_CHART_HISTOGRAM_Y_AXIS_LABEL_KEY"));
    yAxis.setAutoRangeIncludesZero(false);
    setRangeAxis(yAxis);
  }

  /**
   * Add Interval Domain Marker for Histogram chart.
   * @author Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma
   */
  void addIntervalMarker(double start, double end, int sigmaType, double sigmaValue, Color color, int side)
  {
    String label = 
      StringLocalizer.keyToString("RMGUI_CHART_PANEL_STANDARD_DEVIATION_LABEL_KEY") + " " + getFormattedValueString(sigmaValue);
    if(sigmaType != 1)
      label = getFormattedValueString(sigmaType) + " " + 
        StringLocalizer.keyToString("RMGUI_CHART_PANEL_SIGMA_LABEL_KEY") + " " + getFormattedValueString(sigmaValue);
    
    //To determine the label base on the interval side, this is to avoid label overlapping.
    //side = CENTER
    RectangleAnchor labelAnchor = RectangleAnchor.BOTTOM;
    TextAnchor labelTextAnchor = TextAnchor.BOTTOM_CENTER;
    if(side == LEFT)
    {
      labelAnchor = RectangleAnchor.BOTTOM_RIGHT;
      labelTextAnchor = TextAnchor.BOTTOM_RIGHT;       
    }
    else if(side == RIGHT)
    {
      labelAnchor = RectangleAnchor.BOTTOM_LEFT;
      labelTextAnchor = TextAnchor.BOTTOM_LEFT;      
    }
    
    //This is the workaround for displaying the label on top of the chart but draw the interval area behind the chart.
    //We draw it twice.
    Marker backgroundMarker = JointResultsEntryMarkerFactory.getIntervalMarker(start, end, label, labelAnchor, labelTextAnchor);
    Marker foregroundMarker = JointResultsEntryMarkerFactory.getIntervalMarker(start, end, label, labelAnchor, labelTextAnchor);

    backgroundMarker.setPaint(color);
    addDomainMarker(backgroundMarker, Layer.BACKGROUND);//display the marker at background
    
    Color fullTransparent = new Color(149, 179, 215, 0);  
    foregroundMarker.setPaint(fullTransparent);    
    addDomainMarker(foregroundMarker, Layer.FOREGROUND);//display the text label at foreground with full transparent interval
  }
  
  /**
   * Get the general measurement unit.
   * @author Rex Shang
   */
  protected MeasurementUnitsEnum getMeasurementUnitsEnum()
  {
    return _dataSet.getMeasurementUnitsEnum();
  }

  /**
   * @author Rex Shang
   */
  String getStatisticalInformationString()
  {
    String statisticsInfo = StringLocalizer.keyToString("RMGUI_CHART_PANEL_MEAN_LABEL_KEY")
                            + " " + getFormattedValueString(_dataSet.getMean()) + ", "
                            + StringLocalizer.keyToString("RMGUI_CHART_PANEL_STANDARD_DEVIATION_LABEL_KEY")
                            + " " + getFormattedValueString(_dataSet.getStandardDeviation()) + ", "
    //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma - START
                            + "3 " + StringLocalizer.keyToString("RMGUI_CHART_PANEL_SIGMA_LABEL_KEY")
                            + " " + getFormattedValueString(_dataSet.getStandardDeviation() * 3) + ", "
                            + "5 " + StringLocalizer.keyToString("RMGUI_CHART_PANEL_SIGMA_LABEL_KEY")
                            + " " + getFormattedValueString(_dataSet.getStandardDeviation() * 5) + ", "  
    //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma - END
                            + StringLocalizer.keyToString("RMGUI_CHART_PANEL_MEDIAN_LABEL_KEY")
                            + " " + getFormattedValueString(_dataSet.getMedian());

    return statisticsInfo;
  }

}
