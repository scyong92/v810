package com.axi.v810.gui.algoTuner;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.util.*;

/**
 * This class contains the user choices for Algorithm
 * The choices are either a particular AlgorithmEnum or "All"
 * @author Andy Mechtenberg
 */
class AlgorithmChoice implements Serializable
{
  private AlgorithmEnum _algorithmEnum;
  private boolean _all;

  /**
   * @author Andy Mechtenberg
   */
  public AlgorithmChoice(AlgorithmEnum algorithmEnum)
  {
    Assert.expect(algorithmEnum != null);
    _algorithmEnum = algorithmEnum;
    _all = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public AlgorithmChoice()
  {
    _all = true;
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean isAllFamilies()
  {
    return _all;
  }

  /**
   * @author Andy Mechtenberg
   */
  AlgorithmEnum getAlgorithmEnum()
  {
    Assert.expect(_all == false);
    Assert.expect(_algorithmEnum != null);
    return _algorithmEnum;
  }

  /**
   * Objects are equal if they specify the same algorithm
   * @author Andy Mechtenberg
   */
  public boolean equals(Object rhs)
  {
    if (rhs == null)
      return false;

    if (rhs == this)
      return true;

    if (rhs instanceof AlgorithmChoice)
    {
      AlgorithmChoice ac = (AlgorithmChoice)rhs;

      // if both specify ALL, then they are equal
      if((_all) && (ac._all))
        return true;
      else if(_all != ac._all) // if one specifies all, and the other does not, they are not equal
        return false;
      else
      {
        // if they both specify the same specific algorithm, they are equal
        if(_algorithmEnum.equals(ac._algorithmEnum))
          return true;
        else
          return false;
      }
    }
    else  // not an instance of AlgorithmChoice
      return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int hashcode()
  {
    if (_all)
      return -1;
    else
      return _algorithmEnum.hashCode();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    if (_all)
      return StringLocalizer.keyToString("ATGUI_ALL_KEY");
    else
      return _algorithmEnum.toString();
  }

}
