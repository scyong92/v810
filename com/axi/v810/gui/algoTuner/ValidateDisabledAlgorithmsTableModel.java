/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.algoTuner;

import java.util.*;
import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author weng-jian.eoh
 */
public class ValidateDisabledAlgorithmsTableModel extends DefaultSortTableModel
{

  private List<List<Object>> _displayData = null; // a list of lists
  private final String[] _validateDisabledAlgoColumns =
  { 
    StringLocalizer.keyToString("ATGUI_DISABLE_ALGO_TABLE_NUMBER_KEY"),
    StringLocalizer.keyToString("ATGUI_DISABLE_ALGO_TABLE_JOINTTYPE_KEY"),
    StringLocalizer.keyToString("ATGUI_DISABLE_ALGO_TABLE_SUBTYPE_KEY"),
    StringLocalizer.keyToString("ATGUI_DISABLE_ALGO_TABLE_DISABLEDALGO_KEY")
  };

  public static final int _NUMBER_COLUMN = 0;
  public static final int _JOINTTYPE_COLUMN = 1;
  public static final int _SUBTYPE_COLUMN = 2;
  public static final int _DISABLED_ALGO_COLUMN = 3;

  /**
   * @author weng-jian.eoh
   */
  public ValidateDisabledAlgorithmsTableModel()
  {
    _displayData = new LinkedList<List<Object>>();
  }

  /**
   * @author weng-jian.eoh
   * @param subTypeList
   */
  public void populateDisplayData(List<Subtype> subTypeList)
  {
    Assert.expect(subTypeList != null);

    _displayData.clear();
    int index = 0;
    for (Subtype subType : subTypeList)
    {
      java.util.List<AlgorithmEnum> allAlgorithmEnums = subType.getAlgorithmEnums();
      java.util.List<AlgorithmEnum> algorithmEnums = subType.getEnabledAlgorithmEnums();
      allAlgorithmEnums.removeAll(algorithmEnums);
      if (allAlgorithmEnums.isEmpty() == false)
      {
        index++;
        List<Object> disabledAlgoList = new LinkedList<Object>();
        disabledAlgoList.add(index);
        disabledAlgoList.add(subType.getJointTypeEnum().getName());
        disabledAlgoList.add(subType.getShortName());
        List<String> algoNameList = new ArrayList<>();
        for (AlgorithmEnum algoEnum : allAlgorithmEnums)
        {
          algoNameList.add(algoEnum.getName());
        }
        disabledAlgoList.add(StringUtil.join(algoNameList, "; "));
        _displayData.add(disabledAlgoList);
      }
    }
    sortColumn(_JOINTTYPE_COLUMN, true);
  }

  /**
   * @author weng-jian.eoh
   * @return
   */
  public int getRowCount()
  {
    if (_displayData == null)
    {
      return 0;
    }
    else
    {
      return _displayData.size();
    }
  }

  /**
   * @author weng-jian.eoh
   */
  public int getColumnCount()
  {
    return _validateDisabledAlgoColumns.length;
  }

  /**
   * @author weng-jian.eoh
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(_validateDisabledAlgoColumns != null);
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());

    return _validateDisabledAlgoColumns[columnIndex];
  }

  /**
   * @author weng-jian.eoh
   * @param row
   * @param col
   * @return
   */
  public Object getValueAt(int row, int col)
  {
    List<Object> rowData = _displayData.get(row);
    Object data = null;
    data = rowData.get(col);

    // return original value if the data is empty
    if (data instanceof String)
    {
      String emptyString = (String) data;
      if (emptyString.length() <= 0)
      {
        return data;
      }
    }
    return data;
  }

  /**
   * @author weng-jian.eoh
   * @param row
   * @param col
   * @return
   */
  public boolean isCellEditable(int row, int col)
  {
    return false;
  }
}
