package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.plaf.basic.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.gui.testExec.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class RunInspectionPanel extends AbstractTunerTaskPanel implements Observer
{
  private static final int _RUNTIME_TEXT_LIMIT = 250000;  // 250,000 chars
  private static final int _RUNTIME_TEXT_REMOVAL_AMOUNT = 10000;  // 10,000 chars to be removed at a chunk

  private MainUIMenuBar _menuBar = null;
  private int _menuRequesterID = -1;

  private boolean _visible = false;

  private Object _defaultF6Action;
  private Object _defaultF8Action;

  private TestDev _testDev;
  private Project _project;

  private boolean _jointTypeTestMode;
  private JointTypeChoice _currentJointTypeChoice;
  private SubtypeChoice _currentSubtypeChoice;
  private ComponentType _currentComponentType;
  private PadChoice _currentPadChoice;

  private boolean _ableToRunTests = true;
  private ProjectPersistance _projectPersistanceSettings;// = new ProjectPersistance();

  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();

  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
  private SwingWorkerThread _swingWorkerThread1 = SwingWorkerThread.getInstance2();

  private static final String _appName = StringLocalizer.keyToString("ATGUI_GUI_NAME_KEY");
  private boolean _inRunMode = false;
  private boolean _inspectionPaused = true;
  private boolean _firstInspectionImage = true;
  private long _startTime;

  private DecimalFormat _fromBoxFormat = new DecimalFormat("#####");
  private DecimalFormat _toBoxFormat = new DecimalFormat("#####");

  private PanelDataAdapter _panelDataAdapter = null;
  private PanelInspectionAdapter _panelInspectionAdapter = null;
  private DiagnosticTextInfoObserver _diagnosticTextInfoObserver = new DiagnosticTextInfoObserver(this);
  private InspectionEventObserver _inspectionEventObserver = new InspectionEventObserver(this);
  private InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
  private HardwareObservable _hardwareObservable = HardwareObservable.getInstance();
  private GuiObservable _guiObservable = GuiObservable.getInstance();

  private TestOptions _loopOptions = new TestOptions();
  private int _numberOfLoops = 1;
  private boolean _loopForever = false;
  private boolean _userRequestedAbort;
  private boolean _suppressNoImageMessages = false;
  private ImageAcquisitionModeEnum _imageAcquisitionMode;
  private java.util.List<ImageSetData> _selectedImageSets = new LinkedList<ImageSetData>();
  private TestExecution _testExecutionEngine = null;
  private FineTuningImageGraphicsEngineSetup _fineTuningImageGraphicsEngineSetup;
  private AlgorithmConfiguration _algorithmConfiguration = new AlgorithmConfiguration();
  private ResultsProcessingErrorDialog _resultsProcessingErrorDialog = null;
  private XrayTesterException _xrayTesterException;
  private boolean _testComplete;
  private boolean _collectingPrecisionImageSet = false;

  private boolean _autoResume = false; /*Khang Wah (21-Oct-2009)*/

  // images for icons, etc. . .
  private ImageIcon _playEnabledPauseDisabled;
  private ImageIcon _playDisabledPauseEnabled;
  private ImageIcon _startImage;
  private ImageIcon _stopImage;
  private ImageIcon _runToBreakImage;
  private ImageIcon _runToJointImage;
  private ImageIcon _runToInspectionRegionImage;
  private ImageIcon _runToSliceImage;

  private MainMenuGui _mainUI = null;

  // menu stuff
  private JMenuItem _nextJointMenuItem = new JMenuItem();
  private JMenu _viewMenu = new JMenu();
  private JMenuItem _startTestMenuItem = new JMenuItem();
  private JMenuItem _nextMeasurementMenuItem = new JMenuItem();
  private JMenu _fileMenu = new JMenu();
  private JMenu _runMenu = new JMenu();
  private JMenuItem _playPauseMenuItem = new JMenuItem();
  private JMenuItem _nextInspectionRegionMenuItem = new JMenuItem();
  private JMenuItem _stopMenuItem = new JMenuItem();
  private JCheckBoxMenuItem _withDiagnosticsCheckBoxMenuItem = new JCheckBoxMenuItem();
  private JMenu _optionsMenu = new JMenu();
  private JMenuItem _nextSliceMenuItem = new JMenuItem();
  private JMenuItem _algorithmConfigurationMenuItem = new JMenuItem();
  private JMenuItem _imageGenerationOptionMenuItem = new JMenuItem();
  private JMenuItem _loopControlMenuItem = new JMenuItem();
  private JMenuItem _focusControlMenuItem = new JMenuItem();

  private JCheckBoxMenuItem _withAlgorithmsDisabledCheckBoxMenuItem = new JCheckBoxMenuItem();

  private JMenuItem _clearMessageMenuItem = new JMenuItem();
  private JMenu _toolsMenu = new JMenu();
  private JMenuItem _copyThresholdsMenuItem = new JMenuItem();

  private JToolBar _frameToolBar = new JToolBar(StringLocalizer.keyToString("ATGUI_MAIN_TOOLBAR_NAME_KEY"));
  private BorderLayout _runInspectionPanelBorderLayout = new BorderLayout();
  private JPanel _runControlsPanel = new JPanel();
  private BorderLayout _runControlsPanelBorderLayout = new BorderLayout();
  private JPanel _runOptionsPanel = new JPanel();
  private JButton _configureAlgorithmsButton = new JButton();
  private JPanel _algCheckBoxesPanel = new JPanel();
  private JLabel _runDescriptionLabel = new JLabel();
  private BorderLayout _runControlPanelBorderLayout = new BorderLayout();
  private JCheckBox _measurementDiagnosticsCheckBox = new JCheckBox();
  private JCheckBox _shortDiagnosticsCheckBox = new JCheckBox();
  private JCheckBox _diagnosticsCheckBox = new JCheckBox();
  private JButton _debugStopButton = new JButton();
  private JButton _stopButton = new JButton();
  private JTextField _runDescriptionTextField = new JTextField();
  private BorderLayout _runDescriptionBorderLayout = new BorderLayout();
  private BorderLayout _algConfigPanelBorderLayout = new BorderLayout();
  private FlowLayout _debugRunButtonsPanelFlowLayout = new FlowLayout();
  private FlowLayout _runButtonsPanelFlowLayout = new FlowLayout();
  private BorderLayout _runOptionsPanelBorderLayout = new BorderLayout();
  private JPanel _runControlPanel = new JPanel();
  private JPanel _stopButtonPanel = new JPanel();
  private JToolBar _runControlToolBar = new JToolBar(StringLocalizer.keyToString("ATGUI_RUN_CONTROL_TOOLBAR_NAME_KEY"));
  private JPanel _configureButtonPanel = new JPanel();
  private JPanel _algConfigPanel = new JPanel();
  private JPanel _runDescriptionPanel = new JPanel();
  private JPanel _debugRunButtonsPanel = new JPanel();
  private JPanel _runButtonsPanel = new JPanel();
  private Border _beveled1001010SpaceBorder;
  private Border _runOptionsTitledBaseBorder;
  private TitledBorder _runOptionsPanelTitledBorder;
  private Border _empty100510SpaceBorder;
  private FlowLayout _configureButtonPanelFlowLayout = new FlowLayout();
  private JPanel _frameToolBarPanel = new JPanel();
  private BorderLayout _frameToolBarPanelBorderLayout = new BorderLayout();
  private JPanel _mainToolBarButtonPanel = new JPanel();
  private FlowLayout _mainToolBarButtonPanelFlowLayout = new FlowLayout();

  private JPanel _testInfoPanel = new JPanel();

  private BorderLayout _testInfoPanelBorderLayout = new BorderLayout();
  private Border _empty51050SpaceBorder;

  private GridLayout _algCheckBoxesPanelGridLayout = new GridLayout();
  private JCheckBox _algorithmsDisabledCheckBox = new JCheckBox();
  private JPanel _currentRunStatePanel = new JPanel();
  private GridLayout _currentRunStatePanelGridLayout = new GridLayout();
  private JLabel _jointTypeRuntimeInfoLabel = new JLabel();
  private JLabel _jointTypeRuntimeLabel = new JLabel();
  private JLabel _subtypeRuntimeInfoLabel = new JLabel();
  private JLabel _algorithmRuntimeInfoLabel = new JLabel();
  private JLabel _sliceRuntimeInfoLabel = new JLabel();
  private JLabel _componentRuntimeLabel = new JLabel();
  private JLabel _pinRuntimeLabel = new JLabel();
  private JLabel _componentRuntimeInfoLabel = new JLabel();
  private JLabel _pinRuntimeInfoLabel = new JLabel();
  private JLabel _subtypeRuntimeLabel = new JLabel();
  private JLabel _algorithmRuntimeLabel = new JLabel();
  private JLabel _sliceRuntimeLabel = new JLabel();
  private JButton _nextMeasurementButton = new JButton();
  private JButton _nextSliceButton = new JButton();
  private JButton _nextInspectionRegionButton = new JButton();
  private JPanel _nextButtonsPanel = new JPanel();
  private JPanel _goButtonsPanel = new JPanel();
  private JButton _nextJointButton = new JButton();
  private JButton _playPauseButton = new JButton();
  private JButton _startTestButton = new JButton();
  private JButton _debugStartTestButton = new JButton();

  private JScrollPane _runtimeInfoScrollPane = new JScrollPane();
  private JTextArea _runtimeInfoTextArea = new JTextArea();
  private boolean _needToGenerateTestProgramForAll = true;
  private boolean _isTestStartedFromOtherPanel = false;
  
  private boolean _isSubtypeCreatedOrDestroyed = false; //Siew Yeng - XCR-2879

  public RunInspectionPanel(TestDev testDev, PanelDataAdapter panelDataAdapter, PanelInspectionAdapter panelInspectionAdapter)
  {
    Assert.expect(testDev != null);
    Assert.expect(panelDataAdapter != null);
    _testDev = testDev;
    _panelDataAdapter = panelDataAdapter;
    _panelInspectionAdapter = panelInspectionAdapter;
    _mainUI = _testDev.getMainUI();
    jbInit();
    // add this as an observer of the gui
    GuiObservable.getInstance().addObserver(this);
    ProjectObservable.getInstance().addObserver(this);
  }

  /**
   * Constructor.  Set up the splash screen so it can pre-load it's gif
   *
   * @author Andy Mechtenberg
   */
  private RunInspectionPanel()
  {
    // do nothing
    jbInit();
  }

  /**
   * This string will get displayed as the tab name when added to a tabbed pane.
   * @author Rex Shang
   */
  public String getTunerTaskName()
  {
    return StringLocalizer.keyToString("ATGUI_RUN_CONTROL_KEY");
  }

  /**
   * @author Andy Mechtenberg
   */
  void unpopulate()
  {
    _currentComponentType = null;
    _currentPadChoice = null;
    _currentJointTypeChoice = null;
    _currentSubtypeChoice = null;
//    _project = null;
  }

  /**
   * @author Andy Mechtenberg
   */
  void handleHardwareError(HardwareException he)
  {
    MessageDialog.showErrorDialog(_mainUI,
                                  he.getLocalizedMessage(),
                                  StringLocalizer.keyToString("ATGUI_GUI_NAME_KEY"),
                                  true);
    _inRunMode = false;
    changeFrontPanelControls(_inRunMode);
    _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_HARDWARE_ERROR_KEY"));
  }

  /**
   * @author Andy Mechtenberg
   */
  void handleDatastoreError(DatastoreException de)
  {
    MessageDialog.showErrorDialog(_mainUI,
                                  de.getLocalizedMessage(),
                                  StringLocalizer.keyToString("ATGUI_GUI_NAME_KEY"),
                                  true);
    _inRunMode = false;
    changeFrontPanelControls(_inRunMode);
    _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_HARDWARE_ERROR_KEY"));
  }

  /**
   * @author Bill Darbie
   */
  void handleBusinessError(BusinessException be)
  {
    MessageDialog.showErrorDialog(_mainUI,
                                  be.getLocalizedMessage(),
                                  StringLocalizer.keyToString("ATGUI_GUI_NAME_KEY"),
                                  true);
    _inRunMode = false;
    changeFrontPanelControls(_inRunMode);
    if (be instanceof NoAlignmentBusinessException)
      _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_NO_ALIGNMENT_KEY"));
    else if (be instanceof NoSurfaceMapBusinessException)
      _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_NO_SURFACE_MAP_KEY"));
    else
      _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_ERROR_KEY"));
  }

  /**
   * This method should be called to display all error message that might occur.
   * @author Erica Wheatcroft
   */
  void displayError(XrayTesterException xte)
  {
//    MessageDialog.showErrorDialog(_mainUI,
//                                  xte.getLocalizedMessage(),
//                                  StringLocalizer.keyToString("ATGUI_GUI_NAME_KEY"),
//                                  true);
    _mainUI.handleXrayTesterException(xte);
    _inRunMode = false;
    changeFrontPanelControls(_inRunMode);
    if (xte instanceof NoAlignmentBusinessException)
      _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_NO_ALIGNMENT_KEY"));
    else if (xte instanceof NoSurfaceMapBusinessException)
      _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_NO_SURFACE_MAP_KEY"));
    else
      _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_ERROR_KEY"));
  }

  /**
   * @author Andy Mechtenberg
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
         if (observable instanceof ResultsProcessingObservable)
         {
           if (_resultsProcessingErrorDialog.isVisible() == false)
           {
             SwingUtils.centerOnComponent(_resultsProcessingErrorDialog, _mainUI);
             _resultsProcessingErrorDialog.setVisible(true);
           }

           if (object instanceof java.util.List)
           {
             java.util.List<DatastoreException> listOfDatastoreExceptions = (java.util.List<DatastoreException>)object;

             _resultsProcessingErrorDialog.appendDatastoreExceptions(listOfDatastoreExceptions);
           }
           else if (object instanceof java.util.Map)
           {
             _resultsProcessingErrorDialog.setFailureList((java.util.Map<Pair<Target, InspectionInformation>, java.util.List<DatastoreException>>)object);
           }
         }
        else if (observable instanceof HardwareObservable)
          handleHardwareEvent(object);
        else if (observable instanceof ProjectObservable)
        {
          ProjectChangeEvent event = (ProjectChangeEvent)object;
          ProjectChangeEventEnum eventEnum = event.getProjectChangeEventEnum();
          if (eventEnum instanceof ProjectEventEnum)
          {
            ProjectEventEnum projectEventEnum = (ProjectEventEnum)eventEnum;
            if (projectEventEnum.equals(ProjectEventEnum.FAST_LOAD) ||
                projectEventEnum.equals(ProjectEventEnum.SLOW_LOAD) ||
                projectEventEnum.equals(ProjectEventEnum.IMPORT_PROJECT_FROM_NDFS))
            {
              Project project = (Project)event.getSource();
              initializeBecauseOfNewProject(project);
            }
          }
          else if (eventEnum instanceof SubtypeEventEnum)
          {
            SubtypeEventEnum subtypeEventEnum = (SubtypeEventEnum)eventEnum;
            if (subtypeEventEnum.equals(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_SHORT))
            {
              Subtype subtype = (Subtype)event.getSource();
              if (_visible)
              {
                LocalizedString message = new LocalizedString("ATGUI_DELETING_LEARNED_SHORT_DATA_KEY",
                    new Object[]
                    {subtype.getJointTypeEnum(), subtype.toString()});
                addInformationMessage(StringUtil.format(StringLocalizer.keyToString(message) + "\n\n", 60));
              }
            }
            if (subtypeEventEnum.equals(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_EXPECTED_IMAGE))
            {
              Subtype subtype = (Subtype)event.getSource();
              if (_visible)
              {
                LocalizedString message = new LocalizedString("ATGUI_DELETING_LEARNED_EXPECTED_IMAGE_DATA_KEY",
                    new Object[]
                    {subtype.getJointTypeEnum(), subtype.toString()});
                addInformationMessage(StringUtil.format(StringLocalizer.keyToString(message) + "\n\n", 60));
              }
            }
            //Siew Yeng - XCR-2879
            if(subtypeEventEnum.equals(SubtypeEventEnum.CREATE_OR_DESTROY))
            {
              _isSubtypeCreatedOrDestroyed = true;
            }
          }
        }
        else if (observable instanceof GuiObservable)
        {
          GuiEvent guiEvent = (GuiEvent)object;
          GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
          if (guiEventEnum instanceof ImageSetEventEnum)
          {
            ImageSetEventEnum imageSetSelectionEventEnum = (ImageSetEventEnum)guiEventEnum;
            if (imageSetSelectionEventEnum.equals(ImageSetEventEnum.IMAGE_SET_NOT_SELECTED))
            {
              _selectedImageSets = new LinkedList<ImageSetData>();
              if (_imageAcquisitionMode.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS))
                enableAbilityToRunTests(false);
            }
            if (imageSetSelectionEventEnum.equals(ImageSetEventEnum.IMAGE_SET_SELECTED))
            {
              _imageAcquisitionMode = ImageAcquisitionModeEnum.USE_IMAGE_SETS;
              _selectedImageSets = (java.util.List<ImageSetData>)guiEvent.getSource();

                enableAbilityToRunTests(true);

              if (_visible)
                updateStatusWithImageGeneration();
            }
          }
          if (guiEventEnum instanceof SelectionEventEnum)
          {
            SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
            if (selectionEventEnum.equals(SelectionEventEnum.TUNER_TEST_SELECTION))
            {
              // ok, something changed in the tuner selection (joint type, subtype, component, pin)
              // get the data from the object argument
              TunerTestState tunerTestState = (TunerTestState)guiEvent.getSource();
              if (tunerTestState.isJointTypeTest())
              {
                setCurrentTestSelection(tunerTestState.getJointTypeChoice(),
                                        tunerTestState.getSubtypeChoice());
              }
              else
              {
                setCurrentTestSelection(tunerTestState.getComponentType(),
                                        tunerTestState.getPadChoice());
              }
            }
          }
          if (guiEventEnum instanceof GuiUpdateEventEnum)
          {
            GuiUpdateEventEnum guiUpdateEventEnum = (GuiUpdateEventEnum)guiEventEnum;
            if (guiUpdateEventEnum.equals(GuiUpdateEventEnum.TUNER_INSPECTION_START))
            {
              _inRunMode = true;
              changeFrontPanelControls(true);
            }
            else if (guiUpdateEventEnum.equals(GuiUpdateEventEnum.TUNER_INSPECTION_FINISH))
            {
              _inRunMode = false;
              changeFrontPanelControls(false);
              //XCR-2029, Applicable Start Test Shortcut Key for Whole Fine Tuning Page, Ngie Xing
              if (_isTestStartedFromOtherPanel)
              {
                _isTestStartedFromOtherPanel = false;
                _guiObservable.stateChanged(null, GuiUpdateEventEnum.TUNER_INSPECTION_FINISHED_FROM_OTHER_PANEL);
              }
            }
          }

        }
        else
          Assert.expect(false);
      }
    });

  }

  /**
   * The only hardware events we care about is panel load and unload
   * @author Andy Mechtenberg
   */
  private void handleHardwareEvent(final Object arg)
  {
    Assert.expect(arg != null);
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (arg instanceof HardwareEvent)
        {
          HardwareEvent event = (HardwareEvent)arg;
          HardwareEventEnum eventEnum = event.getEventEnum();
          // Xray Tester events
          if (eventEnum instanceof PanelHandlerEventEnum)
          {
            PanelHandlerEventEnum panelHandlerEventEnum = (PanelHandlerEventEnum)eventEnum;
            if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.LOAD_PANEL))
            {
              if (event.isStart() == false)
              {
                determineAbilityToRunTests();
              }
            }
            else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.UNLOAD_PANEL))
            {
              if (event.isStart() == false)
              {
                determineAbilityToRunTests();
              }
            }
          }
        }
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void determineAbilityToRunTests()
  {
    boolean ableToRunTests = true;

    if (_imageAcquisitionMode.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS))
    {
      if (_selectedImageSets.isEmpty())
        ableToRunTests = false;
    }
    else // all other modes require hardware
    {
      if (XrayTester.isHardwareAvailable() && _project.isSystemTypeSettingConfigCompatibleWithSystem())// && PanelHandler.getInstance().isPanelLoaded())
        ableToRunTests = true;
      else
        ableToRunTests = false;
      _selectedImageSets.clear();
      _guiObservable.stateChanged(null, ImageSetEventEnum.IMAGE_SET_CLEAR_ALL);
    }
    enableAbilityToRunTests(ableToRunTests);
  }

  /**
   * This call will change the GUI to not allow tests to be run.
   * The main reason for this is a board is probably not nloaded into the 5dx.
   * On the TDW, this should never be called.
   * @author Andy Mechtenberg
   */
  private void enableAbilityToRunTests(boolean enableTests)
  {
      /*Khang Wah (21-Oct-2009)*/
      if(_autoResume)
      {
        _autoResume = false;
        return;
      }

    _ableToRunTests = enableTests;

    _debugStartTestButton.setEnabled(_ableToRunTests);
    _startTestButton.setEnabled(_ableToRunTests);
    _startTestMenuItem.setEnabled(_ableToRunTests);
    
    _clearMessageMenuItem.setEnabled(_ableToRunTests);

    if (enableTests == false)
    {
      String noTestExplanation;
      if (XrayTester.isHardwareAvailable())
        noTestExplanation = StringLocalizer.keyToString("ATGUI_NO_TESTS_ONLINE_EXPLANATION_KEY");
      else
        noTestExplanation = StringLocalizer.keyToString("ATGUI_NO_TESTS_OFFLINE_EXPLANATION_KEY");
//      noTestExplanation = StringUtil.format(noTestExplanation, 30);
      _debugStartTestButton.setToolTipText(noTestExplanation);
      _startTestButton.setToolTipText(noTestExplanation);
//      _runtimeInfoTextArea.setText(noTestExplanation);
    }
    else
    {
      _debugStartTestButton.setToolTipText(StringLocalizer.keyToString("ATGUI_START_TEST_MENU_KEY") + " (F5)");
      _startTestButton.setToolTipText(StringLocalizer.keyToString("ATGUI_START_TEST_TOOLTIP_KEY") + " (F5)");
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateStatusWithImageGeneration()
  {
    _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_IMAGE_GENERATION_TITLE_KEY") + ": " + _imageAcquisitionMode.getName());
  }

  /**
   * Component initialization
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    // do this right away or there is trouble accessing keyboard menu shortcuts
    JPopupMenu.setDefaultLightWeightPopupEnabled(false);
    int vgap = 5;

    _testExecutionEngine = TestExecution.getInstance();

    _fromBoxFormat.setParseIntegerOnly(true);
    _toBoxFormat.setParseIntegerOnly(true);

    _empty100510SpaceBorder = BorderFactory.createEmptyBorder(0,10,5,10);
    _empty51050SpaceBorder = BorderFactory.createEmptyBorder(5,10,5,0);

    // border creation
    _beveled1001010SpaceBorder = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(134, 134, 134),new Color(93, 93, 93)),BorderFactory.createEmptyBorder(0,10,10,10));
    _runOptionsTitledBaseBorder = BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134));
    _runOptionsPanelTitledBorder = new TitledBorder(_runOptionsTitledBaseBorder,StringLocalizer.keyToString("ATGUI_TEST_SETTINGS_KEY") + ": ");

    // load images right away
    createMenus();

    // this is the upper left icon
//    _frame.setIconImage( Image5DX.getImage(Image5DX.FRAME_5DX) );

    _playEnabledPauseDisabled = Image5DX.getImageIcon(Image5DX.AT_PLAY_ENABLED_PAUSE_DISABLED);
    _playDisabledPauseEnabled = Image5DX.getImageIcon(Image5DX.AT_PLAY_DISABLED_PAUSE_ENABLED);
    _startImage = Image5DX.getImageIcon(Image5DX.AT_START);
    _stopImage = Image5DX.getImageIcon(Image5DX.AT_STOP);
    _runToBreakImage = Image5DX.getImageIcon(Image5DX.AT_RUN_TO_BREAK);
    _runToJointImage = Image5DX.getImageIcon(Image5DX.AT_RUN_TO_JOINT);
    _runToInspectionRegionImage = Image5DX.getImageIcon(Image5DX.AT_RUN_TO_INSPECTION_REGION);
    _runToSliceImage = Image5DX.getImageIcon(Image5DX.AT_RUN_TO_SLICE);

    this.setLayout(_runInspectionPanelBorderLayout);

    _runOptionsPanel.setLayout(_runOptionsPanelBorderLayout);
    _runOptionsPanel.setBorder(_runOptionsPanelTitledBorder);
    _runControlsPanel.setLayout(_runControlsPanelBorderLayout);
    _testInfoPanel.setLayout(_testInfoPanelBorderLayout);// was Cap// was LOCATOR// was C34// was SOIC.Top (8)// was 29// was 2

    initTestSetting();

    _debugStopButton.setEnabled(false);
    _debugStopButton.setToolTipText(StringLocalizer.keyToString("ATGUI_STOP_TOOLTIP_KEY") + " (Escape)");
    _debugStopButton.setIcon(_stopImage);
    _debugStopButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        stopTest_actionPerformed(e);
      }
    });

    _stopButton.setEnabled(false);
    _stopButton.setToolTipText(StringLocalizer.keyToString("ATGUI_STOP_TOOLTIP_KEY") + " (Escape)");
    _stopButton.setIcon(_stopImage);
    _stopButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        stopTest_actionPerformed(e);
      }
    });

    _currentRunStatePanel.setLayout(_currentRunStatePanelGridLayout);
    _currentRunStatePanelGridLayout.setColumns(3);

    _currentRunStatePanel.setBackground(SystemColor.info);
    _currentRunStatePanel.setBorder(_empty51050SpaceBorder);
    _jointTypeRuntimeInfoLabel.setText(StringLocalizer.keyToString("ATGUI_FAMILY_KEY") + ":");
    _jointTypeRuntimeLabel.setFont(FontUtil.getBoldFont(_jointTypeRuntimeLabel.getFont(),Localization.getLocale()));
    _jointTypeRuntimeLabel.setText(" ");
    _subtypeRuntimeInfoLabel.setText(StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY") + ":");
    _algorithmRuntimeInfoLabel.setText(StringLocalizer.keyToString("ATGUI_ALGORITHM_KEY") + ":");
    _sliceRuntimeInfoLabel.setText(StringLocalizer.keyToString("ATGUI_SLICE_KEY") + ":");
    _componentRuntimeLabel.setFont(FontUtil.getBoldFont(_componentRuntimeLabel.getFont(),Localization.getLocale()));
    _componentRuntimeLabel.setText(" ");
    _pinRuntimeLabel.setFont(FontUtil.getBoldFont(_pinRuntimeLabel.getFont(),Localization.getLocale()));
    _pinRuntimeLabel.setText(" ");
    _componentRuntimeInfoLabel.setText(StringLocalizer.keyToString("ATGUI_COMPONENT_KEY") + ":");
    _pinRuntimeInfoLabel.setText(StringLocalizer.keyToString("ATGUI_PIN_KEY") + ":");
    _subtypeRuntimeLabel.setFont(FontUtil.getBoldFont(_subtypeRuntimeLabel.getFont(),Localization.getLocale()));
    _subtypeRuntimeLabel.setText(" ");
    _algorithmRuntimeLabel.setFont(FontUtil.getBoldFont(_algorithmRuntimeLabel.getFont(),Localization.getLocale()));
    _algorithmRuntimeLabel.setText(" ");
    _sliceRuntimeLabel.setFont(FontUtil.getBoldFont(_sliceRuntimeLabel.getFont(),Localization.getLocale()));
    _sliceRuntimeLabel.setText(" ");

    _nextMeasurementButton.setEnabled(false);
    _nextMeasurementButton.setToolTipText(StringLocalizer.keyToString("ATGUI_NEXT_BREAK_TOOLTIP_KEY") + " (F6)");
    _nextMeasurementButton.setIcon(_runToBreakImage);
    _nextMeasurementButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        nextMeasurement_actionPerformed(e);
      }
    });
    _nextSliceButton.setEnabled(false);
    _nextSliceButton.setToolTipText(StringLocalizer.keyToString("ATGUI_NEXT_SLICE_TOOLTIP_KEY") + " (F8)");
    _nextSliceButton.setIcon(_runToSliceImage);
    _nextSliceButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        nextSliceButton_actionPerformed();
      }
    });
    _nextInspectionRegionButton.setEnabled(false);
    _nextInspectionRegionButton.setToolTipText(StringLocalizer.keyToString("ATGUI_NEXT_INSPECTION_REGION_TOOLTIP_KEY") + " (F9)");
    _nextInspectionRegionButton.setIcon(_runToInspectionRegionImage);
    _nextInspectionRegionButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        nextInspectionRegionButton_actionPerformed();
      }
    });
    _nextButtonsPanel.setLayout(new GridLayout());
    _goButtonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER,5,0));

    _nextJointButton.setEnabled(false);
    _nextJointButton.setToolTipText(StringLocalizer.keyToString("ATGUI_NEXT_JOINT_TOOLTIP_KEY") + " (F7)");
    _nextJointButton.setIcon(_runToJointImage);
    _nextJointButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        nextJointButton_actionPerformed();
      }
    });
    _playPauseButton.setEnabled(false);
    _playPauseButton.setToolTipText(StringLocalizer.keyToString("ATGUI_PAUSE_TOOLTIP_KEY") + " (F12)");
//    _pauseButton.setIcon(_pauseImage);
    _playPauseButton.setIcon(_playEnabledPauseDisabled);
    _playPauseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
//        System.out.println("Pause button size: " + _pauseButton.getPreferredSize());
        playPauseButton_actionPerformed();
      }
    });

//    _debugStartTestButton.setMargin(new Insets(2, 40, 2, 40)); // this allows the run test button to be larger than the icon would normally set it to.
    _debugStartTestButton.setToolTipText(StringLocalizer.keyToString("ATGUI_START_TEST_MENU_KEY") + " (F5)");
    _debugStartTestButton.setIcon(_startImage);
    _debugStartTestButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        startTest_actionPerformed(false);
      }
    });
    _startTestButton.setMargin(new Insets(2, 20, 2, 20)); // this allows the run test button to be larger than the icon would normally set it to.
    _startTestButton.setToolTipText(StringLocalizer.keyToString("ATGUI_START_TEST_TOOLTIP_KEY") + " (F5)");
    _startTestButton.setIcon(_startImage);
    _startTestButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        startTest_actionPerformed(false);
      }
    });

    _runtimeInfoTextArea.setEditable(false);
    this.add(_runControlsPanel, BorderLayout.NORTH);
    _runControlToolBar.setMargin(new Insets(0,0,0,0));
    _runControlsPanel.add(_runControlToolBar, BorderLayout.SOUTH);

    // non-debug version (used when diags are not on)
    _runButtonsPanel.add(_startTestButton);
    _runButtonsPanel.add(_stopButton);
//    _runControlToolBar.add(_runButtonsPanel, null);

    // deubg version
//    _runControlToolBar.add(_debugRunButtonsPanel, null);

    _debugRunButtonsPanel.add(_stopButtonPanel);
    _stopButtonPanel.add(_debugStopButton, null);
    _debugRunButtonsPanel.add(_goButtonsPanel);
//    _goButtonsPanel.add(_debugStartTestButton, null);
    _goButtonsPanel.add(_playPauseButton, null);
    _goButtonsPanel.add(_nextButtonsPanel, null);
    _nextButtonsPanel.add(_nextMeasurementButton, null);
    _nextButtonsPanel.add(_nextJointButton, null);
    _nextButtonsPanel.add(_nextSliceButton, null);
    _nextButtonsPanel.add(_nextInspectionRegionButton, null);
    _runControlToolBar.add(_runButtonsPanel, null);

    this.add(_testInfoPanel, BorderLayout.CENTER);
    _testInfoPanel.add(_currentRunStatePanel,  BorderLayout.NORTH);

    JPanel jointTypeSubtypeRuntimePairPanel = new JPanel();
    jointTypeSubtypeRuntimePairPanel.setBackground(SystemColor.info);
    jointTypeSubtypeRuntimePairPanel.setLayout(new PairLayout(5,5));
    JPanel algorithmViewRuntimePairPanel = new JPanel();
    algorithmViewRuntimePairPanel.setBackground(SystemColor.info);
    algorithmViewRuntimePairPanel.setLayout(new PairLayout(5,5));
    JPanel componentPinRuntimePairPanel = new JPanel();
    componentPinRuntimePairPanel.setBackground(SystemColor.info);
    componentPinRuntimePairPanel.setLayout(new PairLayout(5,5));

    jointTypeSubtypeRuntimePairPanel.add(_jointTypeRuntimeInfoLabel);
    jointTypeSubtypeRuntimePairPanel.add(_jointTypeRuntimeLabel);
    jointTypeSubtypeRuntimePairPanel.add(_subtypeRuntimeInfoLabel);
    jointTypeSubtypeRuntimePairPanel.add(_subtypeRuntimeLabel);

    algorithmViewRuntimePairPanel.add(_algorithmRuntimeInfoLabel);
    algorithmViewRuntimePairPanel.add(_algorithmRuntimeLabel);
    algorithmViewRuntimePairPanel.add(_sliceRuntimeInfoLabel);
    algorithmViewRuntimePairPanel.add(_sliceRuntimeLabel);

    componentPinRuntimePairPanel.add(_componentRuntimeInfoLabel);
    componentPinRuntimePairPanel.add(_componentRuntimeLabel);
    componentPinRuntimePairPanel.add(_pinRuntimeInfoLabel);
    componentPinRuntimePairPanel.add(_pinRuntimeLabel);

    _currentRunStatePanel.add(jointTypeSubtypeRuntimePairPanel, null);
    _currentRunStatePanel.add(algorithmViewRuntimePairPanel, null);
    _currentRunStatePanel.add(componentPinRuntimePairPanel, null);

    _frameToolBar.add(_frameToolBarPanel, null);
    _frameToolBarPanel.add(_mainToolBarButtonPanel, BorderLayout.WEST);

    // this is the size used if the .persist file is missing
    this.setPreferredSize(new Dimension(540, 1000));  // for 1600x1200

    _runtimeInfoScrollPane.getViewport().add(_runtimeInfoTextArea, null);
    _testInfoPanel.add(_runtimeInfoScrollPane, BorderLayout.CENTER);
    _configureAlgorithmsButton.setName(".configureAlgorithmsButton");
    _measurementDiagnosticsCheckBox.setName(".measurementDiagnosticsCheckBox");
    _shortDiagnosticsCheckBox.setName(".shortDiagnosticsCheckBox");
    _diagnosticsCheckBox.setName(".diagnosticsCheckBox");
    _debugStopButton.setName(".debugStopButton");
    _stopButton.setName(".stopButton");
    _runDescriptionTextField.setName(".runDescriptionTextField");


    _algorithmsDisabledCheckBox.setName(".algorithmsDisabledButton");
    _nextMeasurementButton.setName(".nextMeasurementButton");
    _nextSliceButton.setName(".nextSliceButton");
    _nextInspectionRegionButton.setName(".nextInspectionRegionButton");
    _nextJointButton.setName(".nextJointButton");
    _playPauseButton.setName(".playPauseButton");
    _startTestButton.setName(".startTestButton");
    _debugStartTestButton.setName(".debugStartTestButton");


    _runtimeInfoTextArea.setName(".runtimeInfoTextArea");
  }

  /**
   * @author Chong, Wei Chin
   */
  private void initTestSetting()
  {
    _configureAlgorithmsButton.setText(StringLocalizer.keyToString("ATGUI_CONFIGURE_BUTTON_KEY"));
    _configureAlgorithmsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        configureAlgorithmsButton_actionPerformed(e);
      }
    });

    _measurementDiagnosticsCheckBox.setText(StringLocalizer.keyToString("ATGUI_MEAS_DIAGNOSTICS_CHECKBOX_KEY"));
    _measurementDiagnosticsCheckBox.setToolTipText(StringLocalizer.keyToString("ATGUI_MEAS_DIAGNOSTICS_CHECKBOX_TOOLTIP_KEY"));
    _measurementDiagnosticsCheckBox.setMnemonic(StringLocalizer.keyToString("ATGUI_MEAS_DIAGNOSTICS_CHECKBOX_MNEMONIC_KEY").charAt(0));
    _measurementDiagnosticsCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        withMeasurementDiagnostics_actionPerformed(e);
      }
    });

    _shortDiagnosticsCheckBox.setText(StringLocalizer.keyToString("ATGUI_SHORT_DIAGNOSTICS_CHECKBOX_KEY"));
    _shortDiagnosticsCheckBox.setToolTipText(StringLocalizer.keyToString("ATGUI_SHORT_DIAGNOSTICS_CHECKBOX_TOOLTIP_KEY"));
    _shortDiagnosticsCheckBox.setMnemonic(StringLocalizer.keyToString("ATGUI_SHORT_DIAGNOSTICS_CHECKBOX_MNEMONIC_KEY").charAt(0));
    _shortDiagnosticsCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        withShortDiagnostics_actionPerformed(e);
      }
    });

    _diagnosticsCheckBox.setMnemonic(StringLocalizer.keyToString("ATGUI_DIAGNOSTICS_CHECKBOX_MNEMONIC_KEY").charAt(0));
    _diagnosticsCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        withDiagnostics_actionPerformed(e);
      }
    });
    _diagnosticsCheckBox.setText(StringLocalizer.keyToString("ATGUI_DIAGNOSTICS_CHECKBOX_KEY"));
    _diagnosticsCheckBox.setToolTipText(StringLocalizer.keyToString("ATGUI_DIAGNOSTICS_CHECKBOX_TOOLTIP_KEY") + " (F3)");

        // set one of these labels to the preferred size of the other.  To do that, we need to know which is bigger.
    _algorithmsDisabledCheckBox.setMnemonic(StringLocalizer.keyToString("ATGUI_DISABLED_CHECKBOX_MNEMONIC_KEY").charAt(0));
    _algorithmsDisabledCheckBox.setText(StringLocalizer.keyToString("ATGUI_DISABLED_CHECKBOX_KEY"));
    _algorithmsDisabledCheckBox.setToolTipText(StringLocalizer.keyToString("ATGUI_DISABLED_CHECKBOX_TOOLTIP_KEY"));
    _algorithmsDisabledCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        withAlgorithmsDisabled_actionPerformed(e);
      }
    });


    _runDescriptionLabel.setText(StringLocalizer.keyToString("ATGUI_RUN_DESCRIPTION_KEY") + ":");
    _runDescriptionBorderLayout.setHgap(10);

    _runControlPanel.setLayout(_runControlPanelBorderLayout);
    _algConfigPanel.setLayout(_algConfigPanelBorderLayout);
    _runDescriptionPanel.setLayout(_runDescriptionBorderLayout);
    _debugRunButtonsPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _debugRunButtonsPanel.setLayout(_debugRunButtonsPanelFlowLayout);
    _runButtonsPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _runButtonsPanel.setLayout(_runButtonsPanelFlowLayout);
    _runControlsPanel.setBorder(_beveled1001010SpaceBorder);
    _runControlsPanelBorderLayout.setHgap(10);
    _runControlsPanelBorderLayout.setVgap(10);

    _runDescriptionPanel.setBorder(_empty100510SpaceBorder);
    _algConfigPanel.setBorder(_empty100510SpaceBorder);
    _algCheckBoxesPanel.setLayout(_algCheckBoxesPanelGridLayout);
    _configureButtonPanel.setLayout(_configureButtonPanelFlowLayout);
    _configureButtonPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _configureButtonPanelFlowLayout.setHgap(0);
    _frameToolBarPanel.setLayout(_frameToolBarPanelBorderLayout);


    _mainToolBarButtonPanel.setLayout(_mainToolBarButtonPanelFlowLayout);
    _mainToolBarButtonPanelFlowLayout.setHgap(0);
    _mainToolBarButtonPanelFlowLayout.setVgap(0);

    _algCheckBoxesPanelGridLayout.setColumns(3);
    _algCheckBoxesPanelGridLayout.setRows(2);
    _algCheckBoxesPanelGridLayout.setHgap(3);

    JPanel innerConfigAlgorithmButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT,0,0));
    innerConfigAlgorithmButtonPanel.add(_configureAlgorithmsButton);
    _algCheckBoxesPanel.add(_measurementDiagnosticsCheckBox, 0);
    _algCheckBoxesPanel.add(_shortDiagnosticsCheckBox, 1);
    _algCheckBoxesPanel.add(new JLabel(" "), 2);
    _algCheckBoxesPanel.add(_diagnosticsCheckBox, 3);
    _algCheckBoxesPanel.add(_algorithmsDisabledCheckBox, 4);
    _algCheckBoxesPanel.add(innerConfigAlgorithmButtonPanel, 5);

    _runOptionsPanel.add(_runDescriptionPanel, BorderLayout.SOUTH);
    _runDescriptionPanel.add(_runDescriptionLabel, BorderLayout.WEST);
    _runDescriptionPanel.add(_runDescriptionTextField, BorderLayout.CENTER);

    _runOptionsPanel.add(_algConfigPanel, BorderLayout.NORTH);
    _algConfigPanel.add(_algCheckBoxesPanel, BorderLayout.WEST);
    _runControlsPanel.add(_runOptionsPanel, BorderLayout.CENTER);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void createMenus()
  {
    // we have to create a new one every time (like the toolbar) because it get's used up by the TestDev

    // menu stuff
    _nextJointMenuItem = new JMenuItem();
    _nextSliceMenuItem = new JMenuItem();
    _viewMenu = new JMenu();
    _startTestMenuItem = new JMenuItem();
    _nextMeasurementMenuItem = new JMenuItem();
    _fileMenu = new JMenu();
    _runMenu = new JMenu();
    _playPauseMenuItem = new JMenuItem();

    _nextInspectionRegionMenuItem = new JMenuItem();
    _stopMenuItem = new JMenuItem();
    _withDiagnosticsCheckBoxMenuItem = new JCheckBoxMenuItem();
    _optionsMenu = new JMenu();
    _algorithmConfigurationMenuItem = new JMenuItem();
    _loopControlMenuItem = new JMenuItem();
    _focusControlMenuItem = new JMenuItem();
    _withAlgorithmsDisabledCheckBoxMenuItem = new JCheckBoxMenuItem();
    _clearMessageMenuItem = new JMenuItem();
    _toolsMenu = new JMenu();
    _copyThresholdsMenuItem = new JMenuItem();

    _nextJointMenuItem.setEnabled(false);
    _nextJointMenuItem.setText(StringLocalizer.keyToString("ATGUI_NEXT_JOINT_MENU_KEY"));
    _nextJointMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0));
    _nextJointMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        nextJointButton_actionPerformed();
      }
    });

    _nextSliceMenuItem.setEnabled(false);
    _nextSliceMenuItem.setText(StringLocalizer.keyToString("ATGUI_NEXT_SLICE_MENU_KEY"));
    _nextSliceMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0));
    _nextSliceMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        nextSliceButton_actionPerformed();
      }
    });

    _viewMenu.setText(StringLocalizer.keyToString("GUI_VIEW_MENU_KEY"));
    _startTestMenuItem.setText(StringLocalizer.keyToString("ATGUI_START_TEST_MENU_KEY"));
    _startTestMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
    _startTestMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        startTest_actionPerformed(false);
      }
    });
    _nextMeasurementMenuItem.setEnabled(false);
    _nextMeasurementMenuItem.setText(StringLocalizer.keyToString("ATGUI_NEXT_BREAK_MENU_KEY"));
    _nextMeasurementMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0));
    _nextMeasurementMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        nextMeasurement_actionPerformed(e);
      }
    });
    _fileMenu.setText(StringLocalizer.keyToString("GUI_FILE_MENU_KEY"));
    _runMenu.setActionCommand(StringLocalizer.keyToString("ATGUI_RUN_MENU_KEY"));
    _runMenu.setText(StringLocalizer.keyToString("ATGUI_RUN_MENU_KEY"));
    _playPauseMenuItem.setEnabled(false);
    _playPauseMenuItem.setText(StringLocalizer.keyToString("ATGUI_PLAY_PAUSE_MENU_KEY"));
    _playPauseMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0));
    _playPauseMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        playPauseButton_actionPerformed();
      }
    });

    _nextInspectionRegionMenuItem.setEnabled(false);
    _nextInspectionRegionMenuItem.setText(StringLocalizer.keyToString("ATGUI_NEXT_INSPECTION_REGION_MENU_KEY"));
    _nextInspectionRegionMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_F9, 0));
    _nextInspectionRegionMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        nextInspectionRegionButton_actionPerformed();
      }
    });
    _stopMenuItem.setEnabled(false);
    _stopMenuItem.setText(StringLocalizer.keyToString("ATGUI_STOP_MENU_KEY"));
    _stopMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
    _stopMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        stopTest_actionPerformed(e);
      }
    });

    _withDiagnosticsCheckBoxMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
    _withDiagnosticsCheckBoxMenuItem.setText(StringLocalizer.keyToString("ATGUI_DIAGNOSTICS_MENU_KEY"));
    _withDiagnosticsCheckBoxMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        withDiagnostics_actionPerformed(e);
      }
    });

    _optionsMenu.setText(StringLocalizer.keyToString("GUI_OPTIONS_MENU_KEY"));
    _algorithmConfigurationMenuItem.setText(StringLocalizer.keyToString("ATGUI_TEST_SETTINGS_MENU_KEY"));
    _algorithmConfigurationMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        configureAlgorithmsButton_actionPerformed(e);
      }
    });

    _imageGenerationOptionMenuItem.setText(StringLocalizer.keyToString("ATGUI_IMAGE_GENERATION_MENU_KEY"));
    _imageGenerationOptionMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        configureImageGeneration_actionPerformed();
      }
    });

    _loopControlMenuItem.setText(StringLocalizer.keyToString("ATGUI_LOOP_CONTROL_MENU_KEY"));
    _loopControlMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        configureLoopControl_actionPerformed();
      }
    });

    _focusControlMenuItem.setText(StringLocalizer.keyToString("ATGUI_ADJUST_FOCUS_MENU_KEY"));
    _focusControlMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
    _focusControlMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _testDev.configureFocusControl_actionPerformed();
      }
    });

    _withAlgorithmsDisabledCheckBoxMenuItem.setText(StringLocalizer.keyToString("ATGUI_DISABLED_MENU_KEY"));
    _withAlgorithmsDisabledCheckBoxMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        withAlgorithmsDisabled_actionPerformed(e);
      }
    });
    _clearMessageMenuItem.setText(StringLocalizer.keyToString("ATGUI_CLEAR_MESSAGES_MENU_KEY"));
    _clearMessageMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        clearMessageMenuItem_actionPerformed();
      }
    });
    _toolsMenu.setText(StringLocalizer.keyToString("ATGUI_TOOLS_MENU_KEY"));

    _viewMenu.add(_clearMessageMenuItem);

    _runMenu.add(_startTestMenuItem);
    _runMenu.add(_nextMeasurementMenuItem);
    _runMenu.add(_nextJointMenuItem);
    _runMenu.add(_nextSliceMenuItem);
    _runMenu.add(_nextInspectionRegionMenuItem);
    _runMenu.add(_playPauseMenuItem);
    _runMenu.add(_stopMenuItem);
    _runMenu.addSeparator();
    _optionsMenu.add(_algorithmConfigurationMenuItem);
    _optionsMenu.add(_imageGenerationOptionMenuItem);
    _optionsMenu.add(_loopControlMenuItem);
    _optionsMenu.add(_withDiagnosticsCheckBoxMenuItem);
    _optionsMenu.add(_withAlgorithmsDisabledCheckBoxMenuItem);

    _toolsMenu.add(_copyThresholdsMenuItem);

    // there are a few menu items that need to by synced up
    // Deal with diagnostics check box and menu item
    if (_diagnosticsCheckBox.isSelected())
      _withDiagnosticsCheckBoxMenuItem.setSelected(true);

    if (_algorithmsDisabledCheckBox.isSelected())
      _withAlgorithmsDisabledCheckBoxMenuItem.setSelected(true);
  }

  /**
   * @author George Booth
   */
  private void addInspectionMenus()
  {
    // menu stuff
    _menuBar = _mainUI.getMainMenuBar();
    _menuRequesterID = _menuBar.startMenuAdd();

    // new view menu (after edit menu)
    _menuBar.addMenu(_menuRequesterID, 2, _viewMenu);
    _menuBar.addMenuItem(_menuRequesterID, _viewMenu, _clearMessageMenuItem);

    // new run menu (after utilities)
    _menuBar.addMenu(_menuRequesterID, 6, _runMenu);
    _menuBar.addMenuItem(_menuRequesterID, _runMenu, _startTestMenuItem);
    _menuBar.addMenuItem(_menuRequesterID, _runMenu, _nextMeasurementMenuItem);
    _menuBar.addMenuItem(_menuRequesterID, _runMenu, _nextJointMenuItem);
    _menuBar.addMenuItem(_menuRequesterID, _runMenu, _nextSliceMenuItem);
    _menuBar.addMenuItem(_menuRequesterID, _runMenu, _nextInspectionRegionMenuItem);
    _menuBar.addMenuItem(_menuRequesterID, _runMenu, _playPauseMenuItem);
    _menuBar.addMenuItem(_menuRequesterID, _runMenu, _stopMenuItem);

    // new options menu (after run)
    _menuBar.addMenu(_menuRequesterID, 7, _optionsMenu);
    _menuBar.addMenuItem(_menuRequesterID, _optionsMenu, _algorithmConfigurationMenuItem);
    _menuBar.addMenuItem(_menuRequesterID, _optionsMenu, _imageGenerationOptionMenuItem);
    if (XrayTester.isHardwareAvailable() == false)
      _imageGenerationOptionMenuItem.setEnabled(false);
    // apm -- removed 1/29/07 as user adjusted focus was cut
//    _menuBar.addMenuItem(_menuRequesterID, _optionsMenu, _focusControlMenuItem);
    _menuBar.addMenuItem(_menuRequesterID, _optionsMenu, _loopControlMenuItem);
    _menuBar.addMenuItem(_menuRequesterID, _optionsMenu, _withDiagnosticsCheckBoxMenuItem);
    _menuBar.addMenuItem(_menuRequesterID, _optionsMenu, _withAlgorithmsDisabledCheckBoxMenuItem);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void start()
  {
    // I need to remove F6 and F8 from the uimanager split pane because my menu items
    // for next measurement and next slice are assigned to F6 and F8.
    // I'll put them back in the finish so other screens aren't affect by this
    UIDefaults uiDefaults = UIManager.getDefaults();
    InputMap inputMap = (InputMap)uiDefaults.get("SplitPane.ancestorInputMap");
    _defaultF6Action = inputMap.get(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0));
    _defaultF8Action = inputMap.get(KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0));
    inputMap.remove(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0));
    inputMap.remove(KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0));

//    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0);
//    _mainUI.getRootPane().registerKeyboardAction(_adjustFocusListener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

    addInspectionMenus();

    Assert.expect(_inspectionEventObservable != null);
    Assert.expect(_inspectionEventObserver != null);
    Assert.expect(_diagnosticTextInfoObserver != null);

    _inspectionEventObservable.addObserver(_inspectionEventObserver);
    _inspectionEventObservable.addObserver(_diagnosticTextInfoObserver);
    ResultsProcessingObservable.getInstance().addObserver(this);

    _hardwareObservable.addObserver(this);
    getPersistanceData();
    setProjectSpecificPersistance();
    updateStatusWithImageGeneration();

    // show image pane
    _testDev.showImageWindow();
    // if there is an image in the image window, enable the focus control menu item
    _fineTuningImageGraphicsEngineSetup = _testDev.getFineTuningImageGraphicsEngineSetup();
    if (_fineTuningImageGraphicsEngineSetup.isImageDisplayed())
      _focusControlMenuItem.setEnabled(true);
    else
      _focusControlMenuItem.setEnabled(false);

    // show/hide image pane diagnostics panel
    _guiObservable.stateChanged(new Boolean(anyDiagnosticsSelected()), SelectionEventEnum.INSPECTION_DIAGNOSTICS_SELECTION);
    determineAbilityToRunTests();
    _visible = true;

    // results procecessing error dialog, if needed
    if (_resultsProcessingErrorDialog == null)
    {
      _resultsProcessingErrorDialog = new ResultsProcessingErrorDialog(_mainUI, StringLocalizer.keyToString("TESTEXEC_GUI_NAME_KEY"));
      SwingUtils.centerOnComponent(_resultsProcessingErrorDialog, _mainUI);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  void initializeBecauseOfNewProject(Project project)
  {
    Assert.expect(project != null);
    _project = project;

    // reset image acqusition mode when new project is loaded
//    if (XrayTester.getInstance().isSimulationModeOn())
//      _imageAcquisitionMode = ImageAcquisitionModeEnum.USE_IMAGE_SETS;
//    else
//    {
      if (XrayTester.isHardwareAvailable())
        _imageAcquisitionMode = ImageAcquisitionModeEnum.USE_HARDWARE_WHEN_NECESSARY;
      else
        _imageAcquisitionMode = ImageAcquisitionModeEnum.USE_IMAGE_SETS;
//    }

    // some of the checkboxs may have been set by the persist data from the last run,
    // so write corrent algo configuration data now.
    _panelInspectionAdapter.setAlgorithmInspectionSettings(_algorithmConfiguration,
        _diagnosticsCheckBox.isSelected(),
        _measurementDiagnosticsCheckBox.isSelected(),
        _shortDiagnosticsCheckBox.isSelected(),
        _algorithmsDisabledCheckBox.isSelected(),
        _imageAcquisitionMode);

    _inRunMode = false;
    if (_visible)
    {
      changeFrontPanelControls(_inRunMode);
    }
    clearAllMessages();
    _loopOptions = new TestOptions(); // reset all looping when a project is loaded

    determineAbilityToRunTests();
  //    enableAbilityToRunTests(_panelInspectionAdapter.isPanelLoadedIntoMachine());
    //_panelDataAdapter.waitForPanelLoadToFinish();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void saveUIState()
  {
    _projectPersistanceSettings.setMeasDiagsChecked(_measurementDiagnosticsCheckBox.isSelected());
    _projectPersistanceSettings.setShortDiagsChecked(_shortDiagnosticsCheckBox.isSelected());
    _projectPersistanceSettings.setDiagsChecked(_diagnosticsCheckBox.isSelected());
    _projectPersistanceSettings.setAlgorithmsDisabledChecked(_algorithmsDisabledCheckBox.isSelected());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void getPersistanceData()
  {
    // create the testoptions instance
    _projectPersistanceSettings = _testDev.getPersistance().getProjectPersistance();
    _algorithmConfiguration = AlgorithmConfiguration.readSettingsPersistanceFile();
  }

  /**
   * @author Erica Wheatcroft
   */
  UndoState getCurrentUndoState()
  {
    UndoState undoState = new UndoState();

    return undoState;
  }

  /**
   * @author Andy Mechtenberg
   */
  void setProjectSpecificPersistance()
  {
// Deal with diagnostics check box and menu item

    if (_projectPersistanceSettings.getMeasDiagsChecked())
      _measurementDiagnosticsCheckBox.setSelected(true);

    if (_projectPersistanceSettings.getShortDiagsChecked())
      _shortDiagnosticsCheckBox.setSelected(true);

    if (_projectPersistanceSettings.getDiagsChecked() && _algorithmConfiguration.anyDiagnosticsEnabled())
    {
      _diagnosticsCheckBox.setSelected(true);
      _withDiagnosticsCheckBoxMenuItem.setSelected(true);
    }
    else
    {
      _diagnosticsCheckBox.setSelected(false);
      _withDiagnosticsCheckBoxMenuItem.setSelected(false);
    }

    if (_projectPersistanceSettings.getAlgorithmsDisabledChecked() && _algorithmConfiguration.anyAlgorithmDisabled())
    {
      _algorithmsDisabledCheckBox.setSelected(true);
      _withAlgorithmsDisabledCheckBoxMenuItem.setSelected(true);
    }
    else
    {
      _algorithmsDisabledCheckBox.setSelected(false);
      _withAlgorithmsDisabledCheckBoxMenuItem.setSelected(false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  void setCurrentTestSelection(JointTypeChoice jointTypeChoice,
                               SubtypeChoice subtypeChoice)
  {
    // Note, These are coming from combo boxes and could be null.

    _jointTypeTestMode = true;
    _currentJointTypeChoice = jointTypeChoice;
    _currentSubtypeChoice = subtypeChoice;
    setRunDescription();

  }

  /**
   * @author Andy Mechtenberg
   */
  void setCurrentTestSelection(ComponentType componentType,
                               PadChoice padChoice)
  {
    // Note, These are coming from combo boxes and could be null.

    _jointTypeTestMode = false;
    _currentComponentType = componentType;
    _currentPadChoice = padChoice;
    setRunDescription();
    
  }

  /**
   * @author Andy Mechtenberg
   */
  void setRunDescription()
  {
    if (_runDescriptionTextField.isEnabled() == false) // if disabled, do not update.  This means a test is running
      return;
    if (_jointTypeTestMode) // joint type/subtype
    {
      _runDescriptionTextField.setText(StringLocalizer.keyToString("ATGUI_FAMILY_KEY") + ":  " + _currentJointTypeChoice + "   " + StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY") + ":  " + _currentSubtypeChoice );
    }
    else // component/pin
    {
      _runDescriptionTextField.setText(StringLocalizer.keyToString("ATGUI_COMPONENT_KEY") + ":  " + _currentComponentType + "   " + StringLocalizer.keyToString("ATGUI_PIN_KEY") + ":  " + _currentPadChoice );
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  void changeFrontPanelControls(boolean isTestRunning)
  {
    if (isTestRunning)
      _mainUI.disableMenuAndToolBars();
    else
      _mainUI.enableMenuAndToolBars();

    boolean diagnosticsOn = anyDiagnosticsSelected();
    // These controls are ENABLED during a test run
    _nextMeasurementButton.setEnabled(isTestRunning && diagnosticsOn);
    _nextJointButton.setEnabled(isTestRunning && diagnosticsOn);
    _nextInspectionRegionButton.setEnabled(isTestRunning && diagnosticsOn);
    _nextSliceButton.setEnabled(isTestRunning && diagnosticsOn);
    _playPauseButton.setEnabled(isTestRunning && diagnosticsOn);
    _debugStopButton.setEnabled(isTestRunning);
    _stopButton.setEnabled(isTestRunning);

    _nextMeasurementMenuItem.setEnabled(isTestRunning && diagnosticsOn);
    _nextJointMenuItem.setEnabled(isTestRunning && diagnosticsOn);
    _nextSliceMenuItem.setEnabled(isTestRunning && diagnosticsOn);
    _nextInspectionRegionMenuItem.setEnabled(isTestRunning && diagnosticsOn);
    _playPauseMenuItem.setEnabled(isTestRunning && diagnosticsOn);
    _stopMenuItem.setEnabled(isTestRunning);

    _runMenu.setEnabled(true);

    // These controls are DISABLED during a test run
    if (_ableToRunTests)
    {
      _debugStartTestButton.setEnabled(isTestRunning == false);
      _startTestButton.setEnabled(isTestRunning == false);
      _startTestMenuItem.setEnabled(isTestRunning == false);
    }
//    _saveMenuItem.setEnabled(isTestRunning == false);

    _measurementDiagnosticsCheckBox.setEnabled(isTestRunning == false);
    _shortDiagnosticsCheckBox.setEnabled(isTestRunning == false);
    _diagnosticsCheckBox.setEnabled(isTestRunning == false);
    _withDiagnosticsCheckBoxMenuItem.setEnabled(isTestRunning == false);
    _algorithmsDisabledCheckBox.setEnabled(isTestRunning == false);
    _withAlgorithmsDisabledCheckBoxMenuItem.setEnabled(isTestRunning == false);
    _configureAlgorithmsButton.setEnabled(isTestRunning == false);
    _algorithmConfigurationMenuItem.setEnabled(isTestRunning == false);
    if (XrayTester.isHardwareAvailable())
      _imageGenerationOptionMenuItem.setEnabled(isTestRunning == false);
    else
      _imageGenerationOptionMenuItem.setEnabled(false);
    _loopControlMenuItem.setEnabled(isTestRunning == false);

    if (isTestRunning == false)
    {
      if (_fineTuningImageGraphicsEngineSetup.isImageDisplayed())
        _focusControlMenuItem.setEnabled(true);
      else
        _focusControlMenuItem.setEnabled(false);
    }
    if (isTestRunning)
      _firstInspectionImage = true;

    _runDescriptionTextField.setEnabled(isTestRunning == false);

    // also turn off editing of any threshold tables here
    // find all the table models currently there, and send them the is not editable message

    // If we are re-enabling the run description text field, then update it's contents.  During a test,
    // this field is disabled, so I don't update it.  After the test is over, the user may have browsed around, so
    // let's update it.
    if (isTestRunning == false)
    {
      setRunDescription();
      _inRunMode = false;
      _runControlToolBar.setVisible(false);
      _runControlToolBar.removeAll();
      _runControlToolBar.add(_runButtonsPanel, null);
      _runControlToolBar.setVisible(true);
    }
    else
    {
      _inRunMode = true;
      _firstInspectionImage = true;
      _inspectionPaused = true;
      _playPauseButton.setIcon(_playEnabledPauseDisabled);
      _playPauseButton.setToolTipText(StringLocalizer.keyToString("ATGUI_PLAY_TOOLTIP_KEY") + " (F12)");

      // at the beginning of each and every test we put the run controls into NON-DIAG mode
      // They will be switched to diag more at the first image that comes up if diags are on
      // this prevents them from being visible while the system generates images, which can take a while
      _runControlToolBar.setVisible(false);
      _runControlToolBar.removeAll();
      _runControlToolBar.add(_runButtonsPanel, null);
      _runControlToolBar.setVisible(true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void withMeasurementDiagnostics_actionPerformed(ActionEvent e)
  {
    _guiObservable.stateChanged(new Boolean(anyDiagnosticsSelected()), SelectionEventEnum.INSPECTION_DIAGNOSTICS_SELECTION);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void withShortDiagnostics_actionPerformed(ActionEvent e)
  {
    _guiObservable.stateChanged(new Boolean(anyDiagnosticsSelected()), SelectionEventEnum.INSPECTION_DIAGNOSTICS_SELECTION);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void withDiagnostics_actionPerformed(ActionEvent e)
  {
    // this can be reached via the menu item or the front panel check box.
    // one would be set by Swing, so we need to set the other to keep them in sync
    if (e.getSource().equals(_diagnosticsCheckBox))
    {
      _withDiagnosticsCheckBoxMenuItem.setSelected(_diagnosticsCheckBox.isSelected());
    }
    if (e.getSource().equals(_withDiagnosticsCheckBoxMenuItem))
    {
      _diagnosticsCheckBox.setSelected(_withDiagnosticsCheckBoxMenuItem.isSelected());
    }
    // don't allow it to be turned ON if there are no valid settings
    if (_diagnosticsCheckBox.isSelected() && (_algorithmConfiguration.anyDiagnosticsEnabled() == false))
    {
      MessageDialog.showErrorDialog(
                  _mainUI,
                  StringLocalizer.keyToString("ATGUI_NO_DIAGNOSTICS_DEFINED_KEY"),
                  StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                  true);
      _diagnosticsCheckBox.setSelected(false);
      _withDiagnosticsCheckBoxMenuItem.setSelected(false);
      return;
    }
    _guiObservable.stateChanged(new Boolean(anyDiagnosticsSelected()), SelectionEventEnum.INSPECTION_DIAGNOSTICS_SELECTION);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void withAlgorithmsDisabled_actionPerformed(ActionEvent e)
  {
    // this can be reached via the menu item or the front panel check box.
    // one would be set by Swing, so we need to set the other to keep them in sync
    if (e.getSource().equals(_algorithmsDisabledCheckBox))
    {
      _withAlgorithmsDisabledCheckBoxMenuItem.setSelected(_algorithmsDisabledCheckBox.isSelected());
    }
    if (e.getSource().equals(_withAlgorithmsDisabledCheckBoxMenuItem))
    {
      _algorithmsDisabledCheckBox.setSelected(_withAlgorithmsDisabledCheckBoxMenuItem.isSelected());
    }
    // don't allow it to be turned ON if there are no valid settings
    if (_algorithmsDisabledCheckBox.isSelected() && (_algorithmConfiguration.anyAlgorithmDisabled() == false))
    {
      MessageDialog.showErrorDialog(
                  _mainUI,
                  StringLocalizer.keyToString("ATGUI_NO_ALGORITHMS_DISABLED_DEFINED_KEY"),
                  StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                  true);
      _algorithmsDisabledCheckBox.setSelected(false);
      _withAlgorithmsDisabledCheckBoxMenuItem.setSelected(false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setStartAndAbortState(boolean testStarting)
  {
    _startTestButton.setEnabled(testStarting == false);
    _startTestMenuItem.setEnabled(testStarting == false);
    _stopButton.setEnabled(testStarting);
    _stopMenuItem.setEnabled(testStarting);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void initializeForTesting()
  {
    try
    {
      SwingUtils.invokeAndWait(new Runnable()
      {
        public void run()
        {
//          _inRunMode = true;
//          _firstInspectionImage = true;
//          _inspectionPaused = true;
//          _playPauseButton.setIcon(_playEnabledPauseDisabled);
//          _playPauseButton.setToolTipText(StringLocalizer.keyToString("ATGUI_PLAY_TOOLTIP_KEY") + " (F12)");
//
//          // at the beginning of each and every test we put the run controls into NON-DIAG mode
//          // They will be switched to diag more at the first image that comes up if diags are on
//          // this prevents them from being visible while the system generates images, which can take a while
//          _runControlToolBar.setVisible(false);
//          _runControlToolBar.removeAll();
//          _runControlToolBar.add(_runButtonsPanel, null);
//          _runControlToolBar.setVisible(true);

//          if (_diagnosticsCheckBox.isSelected())
//          {
//            _runControlToolBar.removeAll();
//            _runControlToolBar.add(_debugRunButtonsPanel, null);
//          }
//          else
//          {
//            _runControlToolBar.removeAll();
//            _runControlToolBar.add(_runButtonsPanel, null);
//
//          }

          _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_RUNNING_KEY"));

          // clear out any previous runtime information
          _sliceRuntimeLabel.setText(" ");
          _jointTypeRuntimeLabel.setText(" ");
          _algorithmRuntimeLabel.setText(" ");
          _componentRuntimeLabel.setText(" ");
          _pinRuntimeLabel.setText(" ");
          _subtypeRuntimeLabel.setText(" ");

          // set up our inspection settings params
          _panelInspectionAdapter.setAlgorithmInspectionSettings(_algorithmConfiguration,
                                                                 _diagnosticsCheckBox.isSelected(),
                                                                 _measurementDiagnosticsCheckBox.isSelected(),
                                                                 _shortDiagnosticsCheckBox.isSelected(),
                                                                 _algorithmsDisabledCheckBox.isSelected(),
                                                                 _imageAcquisitionMode);

          // scroll to bottom
          _runtimeInfoTextArea.setCaretPosition(_runtimeInfoTextArea.getDocument().getLength());
          LocalizedString headerString;
          Object selectedBoardOrPanel = _panelInspectionAdapter.getSelectedPanelOrBoard();
          if (selectedBoardOrPanel instanceof Board)
          {
            Board selectedBoard = (Board)selectedBoardOrPanel;
            headerString = new LocalizedString("ATGUI_RUNTIME_HEADER_BOARD_KEY", new Object[]{selectedBoard.getName(), _project.getName()});
          }
          else // must be entire panel
          {
            headerString = new LocalizedString("ATGUI_RUNTIME_HEADER_PANEL_KEY", new Object[]{_project.getName()});
          }
          addInformationMessage(StringLocalizer.keyToString(headerString) + "\n");
          addInformationMessage(StringLocalizer.keyToString("ATGUI_RUN_DESCRIPTION_KEY") + ": " +
                                _runDescriptionTextField.getText() + "\n");

          _startTime = System.currentTimeMillis();
          _suppressNoImageMessages = false;
        }
      });
    }
    catch (InvocationTargetException ex)
    {
      // do nothing
    }
    catch (InterruptedException ex)
    {
      // do nothing
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  void displayImageSetInfo(ImageSetData imageSetData)
  {
    Assert.expect(imageSetData != null);
    long dateInMils = imageSetData.getDateInMils();
    String imageSetDateString = StringUtil.convertMilliSecondsToTimeAndDate(dateInMils);

    String userDescription = imageSetData.getUserDescription();
    String description;
    if (userDescription.length() == 0)
      description = imageSetData.getSystemDescription();
    else
      description = imageSetData.getSystemDescription() + " (" + imageSetData.getUserDescription() + ")";

    LocalizedString info = new LocalizedString("ATGUI_IMAGE_SET_INFO_KEY", new Object[]{description, imageSetDateString});
    addInformationMessage(StringLocalizer.keyToString(info) + "\n");

    // now, we want to suppress "no image found" messages if the image set was generated via the Production Tune process
    // we'll set a flag here based on the image set description, and use that later for other messages.
    String productionTuneDescriptionTemplate = StringLocalizer.keyToString("Production Tune Defects");
    if (description.startsWith(productionTuneDescriptionTemplate.substring(0, 10)))
      _suppressNoImageMessages = true;
    else
      _suppressNoImageMessages = false;
  }

  /**
   * @author Matt Wharton
   */
  private void setupInspectionFilters()
  {
    if (_jointTypeTestMode) // jointType/subtype
    {
      if (_currentJointTypeChoice.isAllFamilies()) // all families is the same as a board or panel test
      {
        _panelInspectionAdapter.setupForFullTest();
      }
      else if (_currentSubtypeChoice.isAllSubtypes()) // this must be jointType-all test
      {
        _panelInspectionAdapter.setupForJointTypeTest(_currentJointTypeChoice.getJointTypeEnum());
      }
      else
      {
        _panelInspectionAdapter.setupForSubtypeTest(_currentSubtypeChoice.getSubtype());
      }
    }
    else // component/pin
    {
      if (_currentPadChoice.isAllPads())
      {
        _panelInspectionAdapter.setupForComponentTest(_currentComponentType);
      }
      else
      {
        _panelInspectionAdapter.setupForPadTest(_currentPadChoice.getPadType());
      }
    }
  }

  /**
   * @author Wong Ngie Xing, modify from private to public
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  public void startTest_actionPerformed(boolean isTestStartedFromOtherPanel)
  {
    _userRequestedAbort = false;
    _isTestStartedFromOtherPanel = isTestStartedFromOtherPanel;
    setStartAndAbortState(true);

    // first lets check to makse sure we have done an startup on the hardware
    if (_imageAcquisitionMode.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS) ||
        (_imageAcquisitionMode.equals(ImageAcquisitionModeEnum.USE_HARDWARE_WHEN_NECESSARY)))
    {
      try
      {
        _mainUI.performStartupIfNeeded();
        if (XrayTester.getInstance().isStartupRequired())
        {
          setStartAndAbortState(false);
          return;
        }
        boolean hasHighMagComponent = _testExecutionEngine.containHighMagInspectionInCurrentLoadedProject();
        if (_testExecutionEngine.isPanelLoaded(hasHighMagComponent, "") == false)
        {
          // Only load the panel if we're in "use hardware always" mode or if we're in "use hardware when necessary"
          // mode and we're missing images.
          boolean needToLoadPanel = (_imageAcquisitionMode.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS)) ||
              ((_imageAcquisitionMode.equals(ImageAcquisitionModeEnum.USE_HARDWARE_WHEN_NECESSARY) &&
                _testExecutionEngine.needToUseHardwareToGenerateTemporaryOnlineImageSet(_project.getTestProgram())));
          if (needToLoadPanel)
          {
            _mainUI.loadPanel();

            if (_testExecutionEngine.isPanelLoaded(hasHighMagComponent, "") == false)
            {
              setStartAndAbortState(false);
              return;
            }
          }
        }
      }
      catch (XrayTesterException xte)
      {
        _mainUI.handleXrayTesterException(xte);
        setStartAndAbortState(false);
        return;
      }
    }

    // Jack Hwee - only changing subtype slice height setup is allow to igrone new test program generation, other changes will need to re-generate.
    if (_project.isTestProgramValidIgnoringTunedSliceHeightChanges() == false)
    {
      //XCR1716 - System crash when regenerate test program
      //Siew Yeng - need to check if "All" is selected
      if(_jointTypeTestMode)
      {
        //Siew Yeng - XCR-2879
        if(_isSubtypeCreatedOrDestroyed || _currentSubtypeChoice.isAllSubtypes() || _currentJointTypeChoice.isAllFamilies())
          _needToGenerateTestProgramForAll = true;
        else
          _needToGenerateTestProgramForAll = false;
      }
      else
      {
        if(_isSubtypeCreatedOrDestroyed || _currentPadChoice.isAllPads())
          _needToGenerateTestProgramForAll = true;
        else
          _needToGenerateTestProgramForAll = false;
      }
    }
    
    // Jack Hwee - don't need to re-generate the test program if user is fine tuning subtype slice height setup.
    if (_needToGenerateTestProgramForAll || (_imageAcquisitionMode.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS) ||
        (_imageAcquisitionMode.equals(ImageAcquisitionModeEnum.USE_HARDWARE_WHEN_NECESSARY)))) 
    {
      // we should generate the testprogram here in case it's needed.  Sometimes threshold changes (focus related) can cause the program
      // to become invalid, so if we generate it here, it'll be fine when we get in other code.  That code can't generate it because it needs to
      // be generate on the swing thread.
      _project.setCheckLatestTestProgram(true);
      _mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("ATGUI_GENERATING_TEST_PROGRAM_KEY"),
                                                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                                      _project);
      _project.setCheckLatestTestProgram(false);
      _isSubtypeCreatedOrDestroyed = false;
    }

    _guiObservable.stateChanged(null, GuiUpdateEventEnum.TUNER_INSPECTION_START);
    _numberOfLoops = _loopOptions.getLoopCount();
    _loopForever = _loopOptions.doLoopForever();

    _swingWorkerThread1.invokeLater(new Runnable()
    {
      public void run()
      {
        do
        {
          // Set up the test program with the correct filters based on the user's specifications.
          setupInspectionFilters();

          initializeForTesting();
          _xrayTesterException = null;
          _testComplete = false;
          _hardwareWorkerThread.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                _panelInspectionAdapter.runFilteredTest(_runDescriptionTextField.getText());
              }
              catch (XrayTesterException ex)
              {
                _xrayTesterException = ex;
              }
              finally
              {
                _testComplete = true;
                //Siew Yeng
                MaskImage.getInstance().clearMaskImageMap();
              }
            }
          });
          // this thread needs to wait until the hardware thread finishes
          while (_testComplete == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (Exception ex)
            {

            }
          }
          // check for exceptions here
          if (_xrayTesterException != null)
          {
            if (_xrayTesterException instanceof PanelInterferesWithAdjustmentAndConfirmationTaskException)
            {
              // this call needs to BLOCK (but not block the hardware thread or the swing thread)
              boolean continueWithTest = handlePanelInterferesWithAdjustmentException();
              if (continueWithTest == false)
              {
                _loopForever = false;
                _numberOfLoops = 0;
              }
            }
            else
            {
              // if an exception is thrown, bail out of looping
              _loopForever = false;
              _numberOfLoops = 0;

              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  displayError(_xrayTesterException);
                }
              });

              // If alignment failed, we want to save the failing alignment images.
              if (_xrayTesterException instanceof AlignmentBusinessException)
              {
                AlignmentBusinessException alignmentException = (AlignmentBusinessException)_xrayTesterException;
                final TestSubProgram testSubProgram = alignmentException.getTestSubProgram();

                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    try
                    {
                      Alignment.getInstance().saveFailingAlignmentImages(testSubProgram, System.currentTimeMillis());
                    }
                    catch (XrayTesterException xtex)
                    {
                      displayError(xtex);
                    }
                  }
                });
              }
            }
          }
          else
            _numberOfLoops--;
        }
        while ((_userRequestedAbort == false) && ((_loopForever) || (_numberOfLoops > 0)));
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            //Kee Chin Seong - clean off all filters before regenerate / reset tst program
            //or ele will causing alot trouble in v810 
            _project.getTestProgram().clearFilters();
            _guiObservable.stateChanged(null, GuiUpdateEventEnum.TUNER_INSPECTION_FINISH);
          }
        });

      }
    });
//    MaskImage.getInstance().clearMaskImageMap();
    _needToGenerateTestProgramForAll = true;
  }

  /**
   * @author Erica Wheatcroft
   */
  private boolean handlePanelInterferesWithAdjustmentException()
  {
    _testComplete = false;
    final BooleanRef loadCompleted = new BooleanRef(false);
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        _mainUI.closeAllDialogs();
        // its time for the system to run grayscale and there is a panel in the system. so lets prompt the user to
        // unload the board after unload we will reload the panel and then start inspection again.

        // prompt the user to unload
//        String message = "In order to generate images, the system needs to perform a grayscale adjustment. " +
//                         "To complete this adjustment, the panel needs to be unloaded. Would you like to continue " +
//                         "and unload the panel?";

        String message = StringLocalizer.keyToString("MMGUI_GRAYSCALE_NEEDS_PANEL_REMOVED_KEY");
        int response = JOptionPane.showConfirmDialog(_mainUI,
            StringUtil.format(message, 50),
            StringLocalizer.keyToString("ATGUI_GUI_NAME_KEY"),
            JOptionPane.YES_NO_OPTION,
            JOptionPane.ERROR_MESSAGE);

        if (response == JOptionPane.YES_OPTION)
        {
          try
          {
            /*Khang Wah (21-Oct-2009)*/
              _autoResume = true;

            // unload the panel
            _mainUI.unloadPanel();

            // now that the panel is unloaded, do a startup to ensure all calibrations/adjustments are run
            _mainUI.performStartupIfNeeded();

            // if a startup is still needed, don't try to load -- it's probably been canceled out
            if (XrayTester.getInstance().isStartupRequired() == false)
            {
              /*Khang Wah (21-Oct-2009)*/
              _autoResume = true;
              
              // reload the panel
              _mainUI.loadPanel();
            }

            // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
            // now check to verify that the user has not cancelled the load of the panel.
            if (PanelHandler.getInstance().isPanelLoaded())
            {
              // re-run the inspection request.
              loadCompleted.setValue(true);
              _testComplete = true;
              return;
            }
            else
            {
              // return out do not complete operation.
              loadCompleted.setValue(false);
              _testComplete = true;
              return;
            }
          }
          catch (XrayTesterException ex)
          {
            _mainUI.handleXrayTesterException(ex);
            _testComplete = true;
            return;
          }
        }
        else
        {
          // the user does not want to unload. Therefore we will just exit the operation
          loadCompleted.setValue(false);

          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString("ATGUI_INSPECTION_NOT_COMPLETE_KEY"),
                                        StringLocalizer.keyToString("ATGUI_GUI_NAME_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
          _testComplete = true;
          return;
        }
      }
    });

    while(_testComplete == false)
    {
      try
      {
        Thread.sleep(200);
      }
      catch (Exception ex)
      {
        // do nothing
      }
    }

    return loadCompleted.getValue();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void abortTest()
  {
    //System.out.println("Aborting!");
    _mainUI.setCursor(new Cursor(Cursor.WAIT_CURSOR));  // this gets reset when the test has finished aborting
    _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_ABORT_KEY"));
    addInformationMessage(StringLocalizer.keyToString("ATGUI_RUNTIME_ABORT_KEY") + "\n");
    _userRequestedAbort = true;
    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _testExecutionEngine.abort();
        }
        catch (XrayTesterException xtex)
        {
          displayError(xtex);
        }
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void stopTest_actionPerformed(ActionEvent e)
  {
    abortTest();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void configureAlgorithmsButton_actionPerformed(ActionEvent e)
  {
    BoardType currentBoardType = _testDev.getCurrentBoardType();
    AlgorithmConfigurationDlg dlg = new AlgorithmConfigurationDlg(_mainUI,
                                                                  StringLocalizer.keyToString("ATGUI_TEST_SETTINGS_TITLE_KEY"),
                                                                  true,
                                                                  currentBoardType,
                                                                  _panelDataAdapter);
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    _algorithmConfiguration = AlgorithmConfiguration.readSettingsPersistanceFile();

    if (_diagnosticsCheckBox.isSelected() && (_algorithmConfiguration.anyDiagnosticsEnabled() == false))
    {
      _diagnosticsCheckBox.setSelected(false);
      _withDiagnosticsCheckBoxMenuItem.setSelected(false);
    }

    if (_algorithmsDisabledCheckBox.isSelected() && (_algorithmConfiguration.anyAlgorithmDisabled() == false))
    {
      _algorithmsDisabledCheckBox.setSelected(false);
      _withAlgorithmsDisabledCheckBoxMenuItem.setSelected(false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void configureImageGeneration_actionPerformed()
  {
    ImageGenerationOptionsDlg imageGenOptionsDlg = new ImageGenerationOptionsDlg(_mainUI,
        StringLocalizer.keyToString("ATGUI_IMAGE_GENERATION_TITLE_KEY"),
        true,
        _imageAcquisitionMode);
    imageGenOptionsDlg.pack();
    SwingUtils.centerOnComponent(imageGenOptionsDlg, _mainUI);
    imageGenOptionsDlg.setVisible(true);
    _imageAcquisitionMode = imageGenOptionsDlg.getSelectedMode();
    determineAbilityToRunTests();
    updateStatusWithImageGeneration();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void configureLoopControl_actionPerformed()
  {
    TestOptionsDlg testOptionsDlg = new TestOptionsDlg(MainMenuGui.getInstance(), StringLocalizer.keyToString("GUI_LOOP_OPTIONS_TITLE_KEY"), true, _loopOptions);

    testOptionsDlg.pack();
    SwingUtils.centerOnComponent(testOptionsDlg, _mainUI);
    testOptionsDlg.setVisible(true);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void turnAllDiagnosticsOff()
  {
    if (_measurementDiagnosticsCheckBox.isSelected())
      _measurementDiagnosticsCheckBox.doClick();
    if (_shortDiagnosticsCheckBox.isSelected())
      _shortDiagnosticsCheckBox.doClick();
    if (_diagnosticsCheckBox.isSelected())
      _diagnosticsCheckBox.doClick();
    if (_algorithmsDisabledCheckBox.isSelected())
      _algorithmsDisabledCheckBox.doClick();
    _panelInspectionAdapter.clearAlgorithmInspectionSettings();
  }

  /**
   * @author Andy Mechtenberg
   */
  void addInformationMessage(String message)
  {
    // supress ALL messages if we are collecting a precision image set
    if (_collectingPrecisionImageSet == false)
    {
      String noImageMessage = StringLocalizer.keyToString("BUS_NO_COMPATIBLE_IMAGE_AVAILABLE_FOR_REGION_WARNING_KEY");
      if ((_suppressNoImageMessages) && (message.startsWith(noImageMessage.substring(0, 30))))
        return;

      int total = _runtimeInfoTextArea.getDocument().getLength();
      if (total > _RUNTIME_TEXT_LIMIT)
      {
        _runtimeInfoTextArea.replaceRange(null, 0, _RUNTIME_TEXT_REMOVAL_AMOUNT);
      }
      _runtimeInfoTextArea.append(message);
      _runtimeInfoTextArea.setCaretPosition(_runtimeInfoTextArea.getDocument().getLength());
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private boolean anyDiagnosticsSelected()
  {
    return _measurementDiagnosticsCheckBox.isSelected() || _shortDiagnosticsCheckBox.isSelected() || _diagnosticsCheckBox.isSelected();
  }

  /**
   * @author Andy Mechtenberg
   */
  void handleInspectingImage()
  {
    // only do this for the FIRST image that comes up during the inpsection
    if (_firstInspectionImage)
    {
      _firstInspectionImage = false;
      _runControlToolBar.setVisible(false);
      if (anyDiagnosticsSelected())
      {
        _runControlToolBar.removeAll();
        _runControlToolBar.add(_debugRunButtonsPanel, null);
      }
      else
      {
        _runControlToolBar.removeAll();
        _runControlToolBar.add(_runButtonsPanel, null);

      }
      _runControlToolBar.setVisible(true);
    }
  }

  /**
   * @author Laura Cormos
   */
  void handleBeginCollectingPrecisionImageSet()
  {
    // set this flag to know to supress ALL messages until the collection is done.
    _collectingPrecisionImageSet = true;
  }

  /**
   * @author Andy Mechtenberg
   */
  void handlePauseEvent()
  {
    setUIToEnablePlay();
    _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_PAUSED_KEY"));
  }

  /**
   * @author Andy Mechtenberg
   */
  void handleResumeEvent()
  {
    setUIToEnablePause();
    _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_RUNNING_KEY"));
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  private void nextMeasurement_actionPerformed(ActionEvent e)
  {
    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        _testExecutionEngine.runToNextDiagnosticEvent();
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  void handleCompletedInspection(boolean success)
  {
    if (!success)
    {
      MessageDialog.showErrorDialog(
                  _mainUI,
                  StringLocalizer.keyToString("ATGUI_FAILED_INSPECTION_KEY"),
                  StringLocalizer.keyToString("ATGUI_GUI_NAME_KEY"),
                  true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  void handleDetailedInformation(boolean topSide, String jointType, String algorithm, String sliceName, String component, String pin, String subtype)
  {
    // the pair layout used for these labels does some weird things when the empty string is set.
    // So, we replace the empty string with a single space.
    if (jointType == "")
      jointType = " ";
    if (algorithm == "")
      algorithm = " ";
    if (sliceName == "")
      sliceName = " ";
    if (component == "")
      component = " ";
    if (pin == "")
      pin = " ";
    if (subtype == "")
      subtype = " ";
    _sliceRuntimeLabel.setText(sliceName);
    _sliceRuntimeLabel.setToolTipText(sliceName);
//    _viewRuntimeLabel.setText(String.valueOf(inspectionRegionId) + " " + side);
    _jointTypeRuntimeLabel.setText(jointType);
    _jointTypeRuntimeLabel.setToolTipText(jointType);

    _algorithmRuntimeLabel.setText(algorithm);
    _algorithmRuntimeLabel.setToolTipText(algorithm);

    _componentRuntimeLabel.setText(component);
    _componentRuntimeLabel.setToolTipText(component);

    _pinRuntimeLabel.setText(pin);
    _pinRuntimeLabel.setToolTipText(pin);

    _subtypeRuntimeLabel.setText(subtype);
    _subtypeRuntimeLabel.setToolTipText(subtype);
  }

  /**
   * @author Andy Mechtenberg
   */
  void handleInspectionResults(int pinsProcessed, int pinsDefective, int componentsDefective, long inspectionTimeInMillis)
  {
//    long endTime = System.currentTimeMillis();
//    long durationInMillis = endTime - _startTime;

    String timeElapsed = StringUtil.convertMilliSecondsToElapsedTime(inspectionTimeInMillis, true);

    // print out the adjusted time, and add in the tenths of a second after the .
    LocalizedString completeStr = new LocalizedString("ATGUI_RUNTIME_COMPLETE_KEY", new Object[]{timeElapsed});
    addInformationMessage(StringLocalizer.keyToString(completeStr));

    //System.out.println("Time in millis: " + durationInMillis);

    //_runtimeInfoTextArea.append("Inspection Complete - " + hoursStr + ":" + minuteStr + ":" + secondStr + "." + tenthsStr.charAt(0));
    LocalizedString summaryStr = new LocalizedString("ATGUI_RUNTIME_SUMMARY_KEY", new Object[]{
              new Integer(pinsProcessed), new Integer(pinsDefective), new Integer(componentsDefective)});
    addInformationMessage("\n" + StringLocalizer.keyToString(summaryStr) + "\n");
    addInformationMessage("\n");

//    _inRunMode = false;
//    _runControlToolBar.setVisible(false);
//    _runControlToolBar.removeAll();
//    _runControlToolBar.add(_runButtonsPanel, null);
//    _runControlToolBar.setVisible(true);
    _mainUI.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_TEST_COMPLETE_KEY"));
    updateStatusWithImageGeneration();

    // turn on off suppression of messages if this was a precision image set collection
    if (_collectingPrecisionImageSet == true)
      _collectingPrecisionImageSet = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  void handleViewDoneEvent(int viewNum)
  {
    LocalizedString viewStr = new LocalizedString("ATGUI_RUNTIME_VIEW_KEY", new Object[]{new Integer(viewNum)});
    addInformationMessage(StringLocalizer.keyToString(viewStr) + "\n");
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  private void nextJointButton_actionPerformed()
  {
    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        _testExecutionEngine.runToNextJoint();
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  private void nextInspectionRegionButton_actionPerformed()
  {
    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        _testExecutionEngine.runToNextInspectionRegion();
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  private void nextSliceButton_actionPerformed()
  {
    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        _testExecutionEngine.runToNextSlice();
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void playPauseButton_actionPerformed()
  {
    if (_inspectionPaused)  // toggle to play
    {
      _playPauseButton.setIcon(_playDisabledPauseEnabled);
      _playPauseButton.setToolTipText(StringLocalizer.keyToString("ATGUI_PAUSE_TOOLTIP_KEY") + " (F12)");
      _testExecutionEngine.continueInspection();
      _inspectionPaused = false;
    }
    else // toggle back to pause
    {
      _playPauseButton.setIcon(_playEnabledPauseDisabled);
      _playPauseButton.setToolTipText(StringLocalizer.keyToString("ATGUI_PLAY_TOOLTIP_KEY") + " (F12)");
      _testExecutionEngine.pauseInspection();
      _inspectionPaused = true;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setUIToEnablePause()
  {
    _playPauseButton.setIcon(_playDisabledPauseEnabled);
    _playPauseButton.setToolTipText(StringLocalizer.keyToString("ATGUI_PAUSE_TOOLTIP_KEY") + " (F12)");
    _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_RUNNING_KEY"));
    _inspectionPaused = false;
    _optionsMenu.setEnabled(false);
    _focusControlMenuItem.setEnabled(false);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setUIToEnablePlay()
  {
    _playPauseButton.setIcon(_playEnabledPauseDisabled);
    _playPauseButton.setToolTipText(StringLocalizer.keyToString("ATGUI_PLAY_TOOLTIP_KEY") + " (F12)");
    _inspectionPaused = true;
    if (_fineTuningImageGraphicsEngineSetup.isImageDisplayed())
    {
      _optionsMenu.setEnabled(true);
      _focusControlMenuItem.setEnabled(true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void clearAllMessages()
  {
    _runtimeInfoTextArea.setText(null);
    _sliceRuntimeLabel.setText(" ");
    _jointTypeRuntimeLabel.setText(" ");
    _algorithmRuntimeLabel.setText(" ");
    _componentRuntimeLabel.setText(" ");
    _pinRuntimeLabel.setText(" ");
    _subtypeRuntimeLabel.setText(" ");
  }

  /**
   * @author Andy Mechtenberg
   */
  void clearMessageMenuItem_actionPerformed()
  {
    clearAllMessages();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isReadyToFinish()
  {
    // do not allow user to leave if in the middle of an inspection
    if (_inRunMode)
    {
      MessageDialog.showErrorDialog(
                  _mainUI,
                  StringLocalizer.keyToString("GUI_TEST_RUNNING_ON_EXIT_KEY"),
                  StringLocalizer.keyToString("ATGUI_GUI_NAME_KEY"),
                  true);
      return false;
    }
    else
      return true;
  }

  /**
   * Make sure Review Defects is finished and removed before leaving the tuner
   * Unselect the button
   * @author George Booth
   */
  public void finish()
  {
    UIDefaults uiDefaults = UIManager.getDefaults();
    InputMap inputMap = (InputMap)uiDefaults.get("SplitPane.ancestorInputMap");
    inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), _defaultF6Action);
    inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0), _defaultF8Action);

    _menuBar.undoMenuChanges(_menuRequesterID);
    saveUIState();
    turnAllDiagnosticsOff();
    Assert.expect(_inspectionEventObservable != null);
    Assert.expect(_inspectionEventObserver != null);
    Assert.expect(_diagnosticTextInfoObserver != null);

    _inspectionEventObservable.deleteObserver(_inspectionEventObserver);
    _inspectionEventObservable.deleteObserver(_diagnosticTextInfoObserver);
    ResultsProcessingObservable.getInstance().deleteObserver(this);

    _hardwareObservable.deleteObserver(this);
    BasicToolBarUI basicToolbarUI = (BasicToolBarUI)_runControlToolBar.getUI();
    if (basicToolbarUI.isFloating())
      basicToolbarUI.setFloating(false, null); // tell it to dock, so the point param is irrelevant
//    _mainUI.setStatusBarText(" ");
    _testDev.setUnitsInStatus();
    _visible = false;
  }
  
   /**
   * XCR-3589 Combo STD and XXL software GUI
   *
   * @author weng-jian.eoh
   */
  public void disable()
  {
    Assert.expect(_inspectionEventObservable != null);
    Assert.expect(_inspectionEventObserver != null);
    Assert.expect(_diagnosticTextInfoObserver != null);

    _inspectionEventObservable.deleteObserver(_inspectionEventObserver);
    _inspectionEventObservable.deleteObserver(_diagnosticTextInfoObserver);
    ResultsProcessingObservable.getInstance().deleteObserver(this);

    _hardwareObservable.deleteObserver(this);
    GuiObservable.getInstance().deleteObserver(this);
  }
}
