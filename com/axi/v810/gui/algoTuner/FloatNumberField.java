package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.text.*;

/**
 * This class handles the editing of float value thresholds
 * @author Andy Mechtenberg
 */
public class FloatNumberField extends JTextField
{
  // this is key value to denote the user deleted the number here,
  // keying off this allows us to reset the value back to the default (back in AlgoTunerGUI.java)
  public static final float EMPTY_VALUE = 99999999.9f;
  private Toolkit _toolkit;
  private NumberFormat _floatFormatter;

  /**
   * @author Andy Mechtenberg
   */
  public FloatNumberField(float value, int columns)
  {
    super(columns);
    _toolkit = Toolkit.getDefaultToolkit();
    _floatFormatter = NumberFormat.getNumberInstance(Locale.US);
    _floatFormatter.setParseIntegerOnly(false);
    setHorizontalAlignment(JTextField.CENTER);
    setValue(value);
  }

  /**
   * @author Andy Mechtenberg
   */
  public float getValue()
  {
    float retVal = EMPTY_VALUE;
    try
    {
      retVal = _floatFormatter.parse(getText()).floatValue();
    }
    catch (ParseException e)
    {
      //toolkit.beep();
      // If there is a parse exception, send back the EMPTY_VALUE which will have the
      // effect of returning the cell to the default.
      // The valid cases where this exception is thrown is if the user entered just a . or - or deleted the entire value
      retVal = EMPTY_VALUE;  // do this on purpose, so deleting the contents can be flagged, and reset back to default
    }
    return retVal;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setValue(float value)
  {
    setText(_floatFormatter.format(value));
  }

  /**
   * @author Andy Mechtenberg
   */
  protected Document createDefaultModel()
  {
    return new floatNumberDocument();
  }

  /**
   * @author Andy Mechtenberg
   */
  protected class floatNumberDocument extends PlainDocument
  {
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
    {
      char[] source = str.toCharArray();
      char[] result = new char[source.length];
      int j = 0;

      for (int i = 0; i < result.length; i++)
      {
        if (Character.isDigit(source[i]) || source[i] == '.' || source[i] == '-')
          result[j++] = source[i];
        else
        {
          // do nothing.  The beep is annoying
//          toolkit.beep();
//          System.err.println("insertString: " + source[i]);
        }
      }
      super.insertString(offs, new String(result, 0, j), a);
    }
  }
}
