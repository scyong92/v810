package com.axi.v810.gui.algoTuner;

import java.util.*;

import com.axi.v810.util.*;

/**
 * This class is used to enumerate the types of chart we support in review
 * measurments.
 * @author Rex Shang
 */
class ReviewMeasurementsChartTypeEnum extends com.axi.util.Enum
{
  private static Map<String, ReviewMeasurementsChartTypeEnum> _stringToEnumMap =
      new HashMap<String,ReviewMeasurementsChartTypeEnum>();

  private String _name;
  private static int _index = -1;

  public final static ReviewMeasurementsChartTypeEnum X_PLOT =
      new ReviewMeasurementsChartTypeEnum(++_index, StringLocalizer.keyToString("RMGUI_CHART_TYPE_X_PLOT_ENUM_KEY"));

  public final static ReviewMeasurementsChartTypeEnum HISTOGRAM =
      new ReviewMeasurementsChartTypeEnum(++_index, StringLocalizer.keyToString("RMGUI_CHART_TYPE_HISTOGRAM_ENUM_KEY"));

  /**
   * @author Rex Shang
   */
  private ReviewMeasurementsChartTypeEnum(int id, String name)
  {
    super(id);
    _name = name;

    _stringToEnumMap.put(name, this);
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }

  /**
   * @author Rex Shang
   */
  static Collection<ReviewMeasurementsChartTypeEnum> getAllTypes()
  {
    return _stringToEnumMap.values();
  }
}
