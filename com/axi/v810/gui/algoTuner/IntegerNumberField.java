package com.axi.v810.gui.algoTuner;


import java.awt.Toolkit;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import javax.swing.*;
import javax.swing.text.*;

import com.axi.util.*;

/**
 * This class handles the editing of integer value thresholds
 * @author Andy Mechtenberg
 */
public class IntegerNumberField extends JTextField
{
  public static final int EMPTY_VALUE = 99999999;
  private Toolkit _toolkit;
  private NumberFormat _integerFormatter;

  /**
   * @author Andy Mechtenberg
   */
  public IntegerNumberField(int value, int columns)
  {
    super(columns);
    _toolkit = Toolkit.getDefaultToolkit();
    _integerFormatter = NumberFormat.getNumberInstance(Locale.US);
    _integerFormatter.setParseIntegerOnly(true);
    setHorizontalAlignment(JTextField.CENTER);
    setValue(value);
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getValue()
  {
    int retVal = EMPTY_VALUE;
    try
    {
      retVal = _integerFormatter.parse(getText()).intValue();
    }
    catch (ParseException e)
    {
      // This should never happen because insertString allows
      // only properly formatted data to get in the field.
      //toolkit.beep();
      // If there is a parse exception, send back the EMPTY_VALUE which will have the
      // effect of returning the cell to the default.
      // The valid cases where this exception is thrown is if the user entered just a - or deleted the entire value
      retVal = EMPTY_VALUE;  // do this on purpose, so deleting the contents can be flagged, and reset back to default
    }
    return retVal;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setValue(int value)
  {
    setText(_integerFormatter.format(value));
  }

  /**
   * @author Andy Mechtenberg
   */
  protected Document createDefaultModel()
  {
    return new IntegerNumberDocument();
  }

  /**
   * @author Andy Mechtenberg
   */
  protected class IntegerNumberDocument extends PlainDocument
  {
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
    {
      char[] source = str.toCharArray();
      char[] result = new char[source.length];
      int j = 0;

      for (int i = 0; i < result.length; i++)
      {
        if ((Character.isDigit(source[i])) || (source[i] == '-'))
          result[j++] = source[i];
        else
        {
          // do nothing.  The beep is annoying
//          toolkit.beep();
//          System.err.println("insertString: " + source[i]);
        }
      }
      super.insertString(offs, new String(result, 0, j), a);
    }
  }
}
