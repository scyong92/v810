package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.FontUtil;
import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.util.*;

/**
 * Cell renderer customized to thresholds
 * @author Andy Mechtenberg
 */
public class CustomThresholdRenderer extends JLabel implements TableCellRenderer
{
  private Algorithm _algorithm;
  // if true, use currently loaded project (which must exist!).  If false, use InitialThresholds.java (initial thresholds feature)
  private boolean _projectAsUnitSource = true;

  private Hashtable<Integer, TableCellRenderer> _renderers;
  private TableCellRenderer _renderer, _defaultRenderer;
  private Font _boldFont;
  private Font _regularFont;
  private Subtype _subtype;

  private final Color _VALUE_IS_DEFAULT_BACKGROUND_COLOR = Color.white;
  private final Color _VALUE_IS_MODIFIED_BACKGROUND_COLOR = new Color(255, 255, 153); // light yellow
  private final Color _VALUE_IS_MODIFIED_BACKGROUND_2_COLOR = new Color(255, 255, 100); // dark yellow  
  private final Color _VALUE_IS_IMPORTED_BACKGROUND_COLOR = new Color(255, 153, 0); // light orange
  private final Color _VALUE_IS_IMPORTED_BACKGROUND_2_COLOR = new Color(255, 100, 0); // dark orange
  // other possibilities: (ie, for "value is learned" or similar)
  //setBackground(new Color(204, 255, 255));  // light blue
  //setBackground(new Color(204, 255, 204));  // light green


  /**
   * @author Andy Mechtenberg
   */
  public CustomThresholdRenderer(Algorithm algorithm, Subtype subtype)
  {
    Assert.expect(algorithm != null);
    _algorithm = algorithm;
    _renderers = new Hashtable<Integer, TableCellRenderer>();
    _defaultRenderer = new DefaultTableCellRenderer();
    _regularFont = this.getFont();
    _boldFont = FontUtil.getBoldFont(this.getFont(),Localization.getLocale());
    _subtype = subtype;
  }

  /**
   * Set to true to use the currently loaded project (which must exist!).

   * Set to false to use InitialThresholds.java (initial thresholds feature)
   * Defaults to TRUE (using the currently loaded Project)
   * @author Andy Mechtenberg
   */
   public void setProjectAsUnitSource(boolean projectAsUnitSource)
   {
     _projectAsUnitSource = projectAsUnitSource;
   }

  /**
   * @author Andy Mechtenberg
   */
  void add(int row, TableCellRenderer renderer)
  {
    Assert.expect(renderer != null);
    _renderers.put(new Integer(row), renderer);
  }

  /**
   * @author Andy Mechtenberg
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    setFont(_regularFont);
    setOpaque(true);
    _renderer = _renderers.get(new Integer(row));
    if (_renderer != null)
    {
      //_renderer = _defaultRenderer;
      return _renderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }
    setOpaque(true);
    if (isSelected)
    {
      setForeground(table.getSelectionForeground());
      setBackground(table.getSelectionBackground());
    }
    else
    {
      setForeground(table.getForeground());
      setBackground(table.getBackground());
    }

    if (hasFocus)
    {
      //setBorder(BorderFactory.createLineBorder(Color.red, 4));  // apm for debug of focus
      setBorder(BorderFactory.createLineBorder(Color.yellow, 1));
    }
    else
      setBorder(null);

    if (value != null)
    {
      Object cellObject = table.getModel().getValueAt(row, ThresholdTableModel.NAME_COLUMN);
      MeasurementUnitsEnum unitsEnum = null;
      AlgorithmSetting algorithmSetting = null;
      if (cellObject instanceof AlgorithmSetting)
      {
        algorithmSetting = (AlgorithmSetting)cellObject;
        unitsEnum = algorithmSetting.getUnits(_algorithm.getVersion());
        MathUtilEnum displayUnits;
        if (_projectAsUnitSource)
          displayUnits = Project.getCurrentlyLoadedProject().getDisplayUnits();
        else
          displayUnits = InitialThresholds.getInstance().getCurrentUnits();

        String valueText = MeasurementUnitsEnum.formatNumberIfNecessary(unitsEnum, value);
        String valueWithUnitsLabelText = valueText;

        if (column > ThresholdTableModel.NAME_COLUMN)  //don't put the units on the name column
          valueWithUnitsLabelText = AlgorithmSettingUtil.getAlgorithmSettingDisplayText(value, unitsEnum, displayUnits);

        setText(valueWithUnitsLabelText);

        String unitsName = unitsEnum.getName();
        if (unitsName.length() > 0)
        {
          setToolTipText(valueText + " [" + AlgorithmSettingUtil.getAlgorithmSettingDisplayUnitsEnum(unitsEnum, displayUnits).getName() + "]");
        }
        else
        {
          setToolTipText(valueText);
        }
        if (column == ThresholdTableModel.VALUE_COLUMN)
        {
          setForeground(Color.black);
          Object defaultValue = table.getValueAt(row, ThresholdTableModel.DEFAULT_COLUMN);
          Object setValue = table.getValueAt(row, ThresholdTableModel.VALUE_COLUMN);

          if (defaultValue.toString().equals(setValue.toString()))
          {
//            setBackground(_VALUE_IS_DEFAULT_BACKGROUND_COLOR);
//            if (_subtype != null)
//            {
//              if (_subtype.isImportedPackageLibrary())
//              {
//                if (_subtype.isImportedValue(algorithmSetting.getAlgorithmSettingEnum(), getSerializedValue( setValue , algorithmSetting)))
//                  setBackground(_VALUE_IS_IMPORTED_BACKGROUND_COLOR);
//              }
//            }
          }
          else
          {
            if( row%2  != 0 )
              setBackground(_VALUE_IS_MODIFIED_BACKGROUND_COLOR);
            else
              setBackground(_VALUE_IS_MODIFIED_BACKGROUND_2_COLOR);

            if (_subtype != null)
            {
              if (_subtype.isImportedPackageLibrary())
              {
                if (_subtype.isImportedValue(algorithmSetting.getAlgorithmSettingEnum(), getSerializedValue( setValue , algorithmSetting)))
                {
                  if( row%2  != 0 )
                    setBackground(_VALUE_IS_IMPORTED_BACKGROUND_COLOR);
                  else
                    setBackground(_VALUE_IS_IMPORTED_BACKGROUND_2_COLOR);
                }
              }
            }
          }  
        }
        setEnabled(true);
        
        //Siew Yeng - XCR-2764 - Sub-subtype feature
        if(_subtype != null && _subtype.isSubSubtype())
        {
          AlgorithmSetting algoSetting = (AlgorithmSetting)table.getModel().getValueAt(row, ThresholdTableModel.NAME_COLUMN);
          if(_algorithm.getAlgorithmSettingEnumsIgnoreListForSubSubtype() != null)
          {
            if(_algorithm.getAlgorithmSettingEnumsIgnoreListForSubSubtype().contains(algoSetting.getAlgorithmSettingEnum()))
              setEnabled(false);
          }
        }          
      }
      else
      {
        setText((String)value);
        this.setFont(_boldFont);
        setBackground(Color.gray);
      }
    }
    return this;
  }

  /**
   * @author Wei Chin, Chong
   */
  private Serializable getSerializedValue(Object tunedValue, AlgorithmSetting algorithmSetting)
  {
    Assert.expect(tunedValue != null);
    Serializable value = null;

    if (tunedValue instanceof Integer)
      value = (Integer)tunedValue;

    else if (tunedValue instanceof String)
      value = (String)tunedValue;

    else if (tunedValue instanceof Float)
    {
      Float tunedValueFloat = (Float)tunedValue;

      MeasurementUnitsEnum measurementUnitsEnum = _subtype.getAlgorithmSettingUnitsEnum(algorithmSetting.getAlgorithmSettingEnum());
      if (measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS) &&
              Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
      {
        // convert from mils (mils is what the user has input)
        tunedValueFloat = MathUtil.convertMilsToMillimeters(tunedValueFloat.floatValue());
      }
      else if (measurementUnitsEnum.equals(MeasurementUnitsEnum.SQUARE_MILLIMETERS) &&
              Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
      {
        // convert from sq mils (sq mils is what the user has input)
        tunedValueFloat = MathUtil.convertSquareMilsToSquareMillimeters(tunedValueFloat.floatValue());
      }
      else if (measurementUnitsEnum.equals(MeasurementUnitsEnum.SQUARE_MILLIMETERS) &&
              Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
      {
        // convert from cu mils (cu mils is what the user has input)
        tunedValueFloat = MathUtil.convertCubicMilsToCubicMillimeters(tunedValueFloat.floatValue());
      }

     tunedValueFloat = MathUtil.roundToPlaces(tunedValueFloat, 4);
      value = tunedValueFloat;
    }
    else
      Assert.expect(false, "unexpect serialize object : " + tunedValue);

    return value;
  }

}
