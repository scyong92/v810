package com.axi.v810.gui.algoTuner;

import java.io.*;
import java.net.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This is the interface class between the Tuner GUI and the panel
 * description interfaces.
 *
 * It keeps up-to-date references to the currently active information in the
 * Tuner GUI.  So the _family will always reflect the currently active
 * family from the Tuner front panel.
 *
 * @author Andy Mechtenberg
 */
public class PanelDataAdapter
{
  private static boolean _standAloneMode = true;

  private boolean _panelDataDirty = false;  // dirty flag -- only true if ANY panel data, thresholds (value or comment) modified but not saved
  private boolean _thresholdsDirty = false;  // dirty flag -- only true if thresholds (value or comment) modified but not saved

  private AlgoTunerGui _mainFrame = null;

  private Project _project = null;

  // temp variables needed to fake real data.
  // used for _simMode data
  private int _thresholdIndex = 0;
  private Serializable _heelSearchTechniqueValue = new String("Profile Start");
  private Serializable _heelSizeValue = new Double(0.4);
  private Serializable _pinLengthValue = new Double(0.0);
  private Serializable _centerPostionValue = new Double(0.5);
  private Serializable _heelSearchDistanceValue = new Double(2.0);
  private Serializable _learnedJointEnableValue = new Boolean(false);
  private Serializable _sideProfileShiftValue = new Integer(1);
  private Serializable _omitLearnedItemsValue = new LinkedList();

  private static String _projectToLoad = "FAMILIES_ALL_RLV";
//  private static String _panelToLoad = "MOTO_SPEED";
//  private static String _panelToLoad = "NEPCON_WIDE";
//  private static String _panelToLoad = "EVEI00XX02";
  /**
   * @author Andy Mechtenberg
   */
  public PanelDataAdapter(AlgoTunerGui mainFrame)
  {
    Assert.expect(mainFrame != null);
    _mainFrame = mainFrame;
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean areThresholdsDirty()
  {
    // use this flag instead of the _thresholdsDirty flag because that flag is really for the legacy software
    // this one is in general about saving the entire panel
    return _panelDataDirty;
  }

  /**
   * Saves the current panel, both the .panel file and the NDF/RTF thresholds file.
   * Must be called before any panel/board inspection so the IAS has access to the thresholds.
   * @author Andy Mechtenberg
   */
  void saveProjectData()
  {
    try
    {
      if (_thresholdsDirty || _panelDataDirty) // only save if you have to
      {
        _project.save();
      }
      _thresholdsDirty = false;
      _panelDataDirty = false;
    }
    catch (DatastoreException dioe)
    {
      _mainFrame.handleDatastoreError(dioe);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  void setSubtypeComment(Subtype subtype, String comment)
  {
    Assert.expect(subtype != null);

    subtype.setUserComment(comment);
    _thresholdsDirty = true;
    _panelDataDirty = true;
  }

  /**
   * @return a list of AlgorithmEnums for the passed in family
   * @author Andy Mechtenberg
   */
  List<AlgorithmEnum> getAlgorithms(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Collection<Algorithm> algorithms = InspectionFamily.getInstance(jointTypeEnum).getAlgorithms();
    List<AlgorithmEnum> algorithmEnums = new ArrayList<AlgorithmEnum>();
    for (Algorithm algorithm : algorithms)
    {
      algorithmEnums.add(algorithm.getAlgorithmEnum());
    }
    return algorithmEnums;
  }

  /**
   * return a List of AlgorithmEnums
   * @author Andy Mechtenberg
   */
  List<Algorithm> getAlgorithms(Subtype currentSubtype)
  {
    Assert.expect(currentSubtype != null);

    return currentSubtype.getAlgorithms();
  }

  /**
   * @author Andy Mechtenberg
   */
  List<AlgorithmSetting> getThresholds(Subtype subtype,
                                       InspectionFamilyEnum inspectionFamilyEnum,
                                       AlgorithmEnum algoEnum,
                                       boolean standard)
  {
    Assert.expect(subtype != null);
    Assert.expect(inspectionFamilyEnum !=  null);
    Assert.expect(algoEnum != null);

    AlgorithmSettingTypeEnum typeEnumToDisplay;
    if (standard)
      typeEnumToDisplay = AlgorithmSettingTypeEnum.STANDARD;
    else
      typeEnumToDisplay = AlgorithmSettingTypeEnum.ADDITIONAL;

    Algorithm algorithm = InspectionFamily.getAlgorithm(inspectionFamilyEnum, algoEnum);
    List<AlgorithmSetting> settings = algorithm.getAlgorithmSettings(subtype.getJointTypeEnum());

    // now, remove settings that are HIDDEN or don't match the incoming "standard" parameter
    Iterator<AlgorithmSetting> it = settings.iterator();
    while(it.hasNext())
    {
      AlgorithmSetting algoSetting = it.next();
      AlgorithmSettingTypeEnum typeEnum = subtype.getAlgorithmSettingTypeEnum(algoSetting.getAlgorithmSettingEnum());
      if (typeEnum.equals(AlgorithmSettingTypeEnum.HIDDEN))
        it.remove();
      else if (!typeEnum.equals(typeEnumToDisplay))
        it.remove();
    }

    Collections.sort(settings, new AlgorithmSettingDisplayOrderComparator());
    return settings;
  }

  /**
   * @author Andy Mechtenberg
   */
  List<AlgorithmSetting> getSliceSetupThresholds(Subtype subtype,
                                                 InspectionFamilyEnum inspectionFamilyEnum,
                                                 boolean standard)
  {
    Assert.expect(subtype != null);
    Assert.expect(inspectionFamilyEnum !=  null);

    List<Algorithm> algorithms = getAlgorithms(subtype);
    List<AlgorithmSetting> sliceRelatedSettings = new ArrayList<AlgorithmSetting>();

    for (Algorithm algorithm : algorithms)
    {      
      List<AlgorithmSetting> settings = algorithm.getAlgorithmSettings(subtype.getJointTypeEnum());

      for (AlgorithmSetting as : settings)
      {
        if (standard)   // want standard settings
        {
          if (as.getAlgorithmSettingTypeEnum(algorithm.getVersion()).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT))
            sliceRelatedSettings.add(as);
        }
        else            // want additional settings
        {
          if (as.getAlgorithmSettingTypeEnum(algorithm.getVersion()).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL))
            sliceRelatedSettings.add(as);
        }
      }
    }

    Collections.sort(sliceRelatedSettings, new AlgorithmSettingDisplayOrderComparator());
    return sliceRelatedSettings;
  }


  /**
   * @author Andy Mechtenberg
   */
  List<AlgorithmSetting> getAllThresholds(Subtype subtype, InspectionFamilyEnum inspectionFamilyEnum, Algorithm algorithm)
  {
    Assert.expect(subtype != null);
    Assert.expect(inspectionFamilyEnum !=  null);
    Assert.expect(algorithm != null);
    
    List<AlgorithmSetting> settings = algorithm.getAlgorithmSettings(subtype.getJointTypeEnum());

    Collections.sort(settings, new AlgorithmSettingDisplayOrderComparator());
    return settings;
  }

  /**
   * @author Wong Ngie Xing
   * XCR1792 - Able to Copy PTH and PRESSFIT Barrel Slices Number
   */
  List<AlgorithmSetting> getAllThresholdsExcludeSliceHeight(Subtype subtype, InspectionFamilyEnum inspectionFamilyEnum, Algorithm algorithm)
  {
    Assert.expect(subtype != null);
    Assert.expect(inspectionFamilyEnum !=  null);
    Assert.expect(algorithm != null);
    
    List<AlgorithmSetting> settings = algorithm.getAlgorithmSettingsExcludingSliceHeight(subtype.getJointTypeEnum());

    Collections.sort(settings, new AlgorithmSettingDisplayOrderComparator());
    return settings;
  }

  /**
   * @author Andy Mechtenberg
   */
  Serializable getThresholdValue(AlgorithmEnum algoEnum, String threshName)
  {
    /**
     * @todo PE: fix this!
     * get the Setting values by calling Subtype.getSettingValue(SettingEnum);
     * old code follows:

    Map<String, CustomThreshold> thresholdNameToThresholdMap = _algoToSettingMapMap.get(algoEnum);
    Assert.expect(thresholdNameToThresholdMap != null);
    CustomThreshold ct = thresholdNameToThresholdMap.get(threshName);
    Assert.expect(ct != null, "Cannot find " + algoEnum + " " + threshName);
    return ct.getValue();
   */
    return new Integer(0);
  }

  /**
   * @author Andy Mechtenberg
   */
  Object getDefaultThresholdValue(AlgorithmEnum algoEnum, String threshName)
  {
    Assert.expect(algoEnum != null);
    Assert.expect(threshName != null);

    /**
     * @todo PE:  fix this
     * get the default threshold value by calling Algorithm.getSettingDefaultValue(SettingEnum)
     * old code follows:

    Map<String, CustomThreshold> thresholdNameToThresholdMap = _algoToSettingMapMap.get(algoEnum);
    Assert.expect(thresholdNameToThresholdMap != null);
    CustomThreshold ct = thresholdNameToThresholdMap.get(threshName);
    Assert.expect(ct != null);
    return ct.getDefaultValue();
    */

   return new Integer(0);
  }


  /**
   * @author Andy Mechtenberg
   */
  URL convertKeyToURL(String key)
  {
    //System.out.println("descriptionURLKey: " + key);
    URL docsURL = null;

    if (key.length() == 0) // empty string
      return docsURL;

    String valueOfKey = StringLocalizer.keyToString(key);
    if (valueOfKey.length() == 0)
      return docsURL;

    try
    {
      docsURL = new URL(Directory.getDocsDir() + File.separator + valueOfKey);
    }
    catch (MalformedURLException ex)
    {
      //System.out.println("MalformedURLException for key: " + key);
      docsURL = null;
    }
    return docsURL;
  }
}
