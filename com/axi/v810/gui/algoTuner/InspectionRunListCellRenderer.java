package com.axi.v810.gui.algoTuner;

import java.awt.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.datastore.testResults.*;

/**
 * This class is used to render a line of inspection run information.
 * @author Rex Shang
 */
class InspectionRunListCellRenderer extends JLabel implements ListCellRenderer
{

  /**
   * @author Rex Shang
   */
  InspectionRunListCellRenderer()
  {
  }

  /**
   * Override super class to yield the desired format.
   * @author Rex Shang
   */
  public Component getListCellRendererComponent(
      JList list,
      Object value, // value to display
      int index, // cell index
      boolean isSelected, // is the cell selected
      boolean cellHasFocus) // the list and the cell have the focus
  {
    PanelResults panelResults = (PanelResults)value;

    String displayString = "";
    if (panelResults != null)
    {
      displayString = panelResults.getInspectionStartTimeInMillis()
                      + "  "
                      + StringUtil.convertMilliSecondsToTimeAndDate(panelResults.getInspectionStartTimeInMillis())
                      + "  "
                      + panelResults.getSerialNumberOrDescription();

      String imageSetString = panelResults.getImageSetDescription() + "  " + panelResults.getInformationFlag();
      if (imageSetString.trim().length() > 0)
      {
        displayString += ";  " + imageSetString;
      }
    }
    setText(displayString);

    if (isSelected)
    {
      setBackground(list.getSelectionBackground());
      setForeground(list.getSelectionForeground());
    }
    else
    {
      setBackground(list.getBackground());
      setForeground(list.getForeground());
    }
    setEnabled(list.isEnabled());
    setFont(list.getFont());
//    setHorizontalAlignment(TRAILING);

    setOpaque(true);
    return this;
  }
}
