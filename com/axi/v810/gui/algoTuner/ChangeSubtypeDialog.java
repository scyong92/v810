/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 *
 * @author wei-chin.chong
 */
public class ChangeSubtypeDialog extends JDialog
{
  private JPanel _mainPanel = new JPanel();
  private JPanel _northPanel = new JPanel();
  private JPanel _centerPanel = new JPanel();
  private JPanel _southPanel = new JPanel();
  
  private BorderLayout _mainBorderLayout = new BorderLayout(5,5);
  private PairLayout _northPairLayout = new PairLayout();
  private VerticalFlowLayout _centerVerticalFlowLayout = new VerticalFlowLayout();
  private FlowLayout _southFlowLayout = new FlowLayout(FlowLayout.CENTER, 10, 2);
    
  private JRadioButton _createNewSubtypeRadioButton = new JRadioButton();
  private JRadioButton _changeToExistingSubtypeRadioButton = new JRadioButton();
  private ButtonGroup _swicthSubtypeOptionButtonGroup = new ButtonGroup();
  
  private JComboBox _listOfExistingSubtypeComboBox = new JComboBox();
  private JTextField _newSubtypeTextField = new JTextField();
  
  private JCheckBox _copyAllThresholdCheckBox = new JCheckBox();
  
  private Subtype _currentSubtype;  
  
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  
  // Ref Des
  private JLabel _refDesLabel = new JLabel();
  private JLabel _refDesDetails = new JLabel();
  // Pad Name
  private JLabel _padNameLabel = new JLabel();
  private JLabel _padNameDetails = new JLabel();
  // Current Subtype
  private JLabel _currentSubtypeLabel = new JLabel();
  private JLabel _currentSubtypeDetails = new JLabel();
  // Existing Subtype
  private JLabel _existingSubtypeLabel = new JLabel();
  private JLabel _exitingSubtypeDetails = new JLabel();
  // Change To... Label
  private JLabel _changeToLabel = new JLabel();

  // [XCR-2622] - Ying-Huan.Chu - Add Extra Subtype Information During New Subtype Creation.
  // Camera Gain
  private JLabel _cameraGainLabel = new JLabel();
  private JComboBox _cameraGainComboBox = new JComboBox();
  private UserGainEnum _selectedCameraGain = null;
  // Interference Compensation
  private JLabel _interferenceCompensationLabel = new JLabel();
  private JComboBox _interferenceCompensationComboBox = new JComboBox();
  private BooleanRef _isInteferenceCompensationEnabled = null;
  // Integration Level
  private JLabel _integrationLevelLabel = new JLabel();
  private JComboBox _integrationLevelComboBox = new JComboBox();
  private SignalCompensationEnum _selectedIntegrationLevel = null;
  // Magnification Type
  private JLabel _magnificationTypeLabel = new JLabel();
  private JComboBox _magnificationTypeComboBox = new JComboBox();
  private MagnificationTypeEnum _selectedMagnificationType = null;
  // DRO Level
  private JLabel _dROLevelLabel = new JLabel();
  private JComboBox _dROLevelComboBox = new JComboBox();
  private DynamicRangeOptimizationLevelEnum _selectedDROLevel = null;
  
  private Project _currentProject = null;
  private java.util.List<PadType> _padTypes = null;
  
  private final String _UNDERSCORE  = "_";
  
  private boolean changeIsComplete = false;
  
  //Khaw Chek Hau - XCR-3330 : Failed to change existing subtype by using new subtype dialog
  private boolean _isExistingSubtypeUsed = false;
  private String _newSubtypeShortName;
  
  /**
   * @author Wei Chin
   */
  public ChangeSubtypeDialog(Frame parent, String title)
  {
    super(parent, title, true);
    Assert.expect(parent != null);
    Assert.expect(title != null);
    
    jbInit();
    setPreferredSize(new Dimension(360,550));
    pack();
  }
  
  /**
   * @author Wei Chin
   */
  private void jbInit()
  {
    add(_mainPanel);
    _mainPanel.setLayout(_mainBorderLayout);
    _mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    
    _mainPanel.add(_northPanel, BorderLayout.NORTH);
    _mainPanel.add(_centerPanel, BorderLayout.CENTER);
    _mainPanel.add(_southPanel, BorderLayout.SOUTH);
    
    _northPanel.setLayout(_northPairLayout);
    _centerPanel.setLayout(_centerVerticalFlowLayout);
    _southPanel.setLayout(_southFlowLayout);    
    
    _refDesLabel.setText(StringLocalizer.keyToString("RMGUI_REFDES_KEY") + " : ");
    _refDesLabel.setFont(FontUtil.getBoldFont(_refDesLabel.getFont(), Localization.getLocale()));
    _northPanel.add(_refDesLabel);
    _northPanel.add(_refDesDetails);    
    
    _padNameLabel.setText(StringLocalizer.keyToString("RMGUI_PAD_NAME_KEY") + " : ");
    _padNameLabel.setFont(FontUtil.getBoldFont(_padNameLabel.getFont(), Localization.getLocale()));
    _northPanel.add(_padNameLabel);
    _northPanel.add(_padNameDetails);
    
    _currentSubtypeLabel.setText(StringLocalizer.keyToString("RMGUI_CURRENT_SUBTYPE_KEY") + " : ");
    _currentSubtypeLabel.setFont(FontUtil.getBoldFont(_currentSubtypeLabel.getFont(), Localization.getLocale()));
    _northPanel.add(_currentSubtypeLabel);
    _northPanel.add(_currentSubtypeDetails);
    
    _changeToLabel.setText(StringLocalizer.keyToString("RMGUI_CHANGETO_KEY"));
    _changeToLabel.setFont(FontUtil.getBoldFont(_changeToLabel.getFont(), Localization.getLocale()));
    
    _centerPanel.add(_changeToLabel);
    _createNewSubtypeRadioButton.setText(StringLocalizer.keyToString("RMGUI_CHANGETO_NEW_SUBTYPE_KEY") + " : ");
    _changeToExistingSubtypeRadioButton.setText(StringLocalizer.keyToString("RMGUI_CHANGETO_EXISTING_SUBTYPE_KEY") + " : ");
    _createNewSubtypeRadioButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if(e.getStateChange() == ItemEvent.SELECTED)
        {
          _newSubtypeTextField.setEditable(true);
          _newSubtypeTextField.setEnabled(true);
          _copyAllThresholdCheckBox.setEnabled(true);
          _listOfExistingSubtypeComboBox.setEnabled(false);
        }
      }
    });
    
    _newSubtypeTextField.setColumns(20);
    JPanel _newSubtypePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
    _newSubtypePanel.add(_createNewSubtypeRadioButton);
    _newSubtypePanel.add(_newSubtypeTextField);
    _centerPanel.add(_newSubtypePanel);
    
    JPanel _newSubtypeOptionPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
    _copyAllThresholdCheckBox.setText(StringLocalizer.keyToString("RMGUI_COPYALL_THRESHOLDS_SUBTYPE_KEY"));
    _newSubtypeOptionPanel.add(_copyAllThresholdCheckBox);
    _centerPanel.add(_newSubtypeOptionPanel);
    
    _changeToExistingSubtypeRadioButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if(e.getStateChange() == ItemEvent.SELECTED)
        {
          _listOfExistingSubtypeComboBox.setEnabled(true);
          _newSubtypeTextField.setEditable(false);
          _newSubtypeTextField.setEnabled(false);
          _copyAllThresholdCheckBox.setEnabled(false);
        }
      }
    });
    
    _listOfExistingSubtypeComboBox.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if(e.getStateChange() == ItemEvent.SELECTED)
        {
          if(e.getItem() instanceof Subtype)
          {
            Subtype existingSubtype = (Subtype)e.getItem();
            if(existingSubtype != null)
            {
              String details =StringLocalizer.keyToString(new LocalizedString("RMGUI_CURRRENT_SUBTYPE_DETAILS_KEY", 
                new String[]{existingSubtype.getSubtypeAdvanceSettings().getSignalCompensation().toString(), 
                  existingSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState().toString(),
                  existingSubtype.getSubtypeAdvanceSettings().getUserGain().toString(), 
                  existingSubtype.getSubtypeAdvanceSettings().getMagnificationType().toString() }));
              _exitingSubtypeDetails.setText(details);
            }
          }
        }
      }
    });
    
    // Camera Gain
    JPanel cameraGainPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
    _cameraGainLabel.setText(StringLocalizer.keyToString("RMGUI_CAMERA_GAIN_KEY") + " : ");
    _cameraGainLabel.setFont(FontUtil.getBoldFont(_cameraGainLabel.getFont(), Localization.getLocale()));
    for (UserGainEnum userGain : UserGainEnum.getOrderedUserGain())
    {
      _cameraGainComboBox.addItem(userGain);
    }
    cameraGainPanel.add(_cameraGainLabel);
    cameraGainPanel.add(_cameraGainComboBox);
    _centerPanel.add(cameraGainPanel);
    
    // Interference Compensation
    JPanel interferenceCompensationPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
    _interferenceCompensationLabel.setText(StringLocalizer.keyToString("RMGUI_USE_INTERFERENCE_COMPENSATION_KEY") + " : ");
    _interferenceCompensationLabel.setFont(FontUtil.getBoldFont(_interferenceCompensationLabel.getFont(), Localization.getLocale()));
    for (ArtifactCompensationStateEnum artifactCompensationStateEnum :  ArtifactCompensationStateEnum.getOrderedArtifactCompensationState())
    {
      if (artifactCompensationStateEnum.equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false)
        _interferenceCompensationComboBox.addItem(artifactCompensationStateEnum);
    }
    _interferenceCompensationComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        interferenceCompensationComboBox_actionPerformed(e);
      }
    });
    interferenceCompensationPanel.add(_interferenceCompensationLabel);
    interferenceCompensationPanel.add(_interferenceCompensationComboBox);
    _centerPanel.add(interferenceCompensationPanel);
    
    // Integration Level
    JPanel integrationLevelPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
    _integrationLevelLabel.setText(StringLocalizer.keyToString("RMGUI_INTEGRATION_LEVEL_KEY") + " : ");
    _integrationLevelLabel.setFont(FontUtil.getBoldFont(_integrationLevelLabel.getFont(), Localization.getLocale()));
    for (SignalCompensationEnum signalCompensationEnum : SignalCompensationEnum.getOrderedSignalCompensations())
    {
      _integrationLevelComboBox.addItem(signalCompensationEnum);
    }
    disableIntegrationLevelComboBoxIfNecessary();
    integrationLevelPanel.add(_integrationLevelLabel);
    integrationLevelPanel.add(_integrationLevelComboBox);
    _centerPanel.add(integrationLevelPanel);
    
    // Magnification Type
    JPanel magnificationTypePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
    _magnificationTypeLabel.setText(StringLocalizer.keyToString("RMGUI_MAGNIFICATION_KEY") + " : ");
    _magnificationTypeLabel.setFont(FontUtil.getBoldFont(_magnificationTypeLabel.getFont(), Localization.getLocale()));
    for (Map.Entry<String, MagnificationTypeEnum> magnificationType : MagnificationTypeEnum.getMagnificationToEnumMap().entrySet())
    {
      _magnificationTypeComboBox.addItem(magnificationType.getValue());
    }
    magnificationTypePanel.add(_magnificationTypeLabel);
    magnificationTypePanel.add(_magnificationTypeComboBox);
    _centerPanel.add(magnificationTypePanel);
    
    // DRO Level
    JPanel dROPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
    _dROLevelLabel.setText(StringLocalizer.keyToString("RMGUI_DRO_LEVEL_KEY") + " : ");
    _dROLevelLabel.setFont(FontUtil.getBoldFont(_dROLevelLabel.getFont(), Localization.getLocale()));
    for (DynamicRangeOptimizationLevelEnum dROLevelEnum : DynamicRangeOptimizationLevelEnum.getOrderedDynamicRangeOptimizationLevel())
    {
      _dROLevelComboBox.addItem(dROLevelEnum);
    }
    dROPanel.add(_dROLevelLabel);
    dROPanel.add(_dROLevelComboBox);
    _centerPanel.add(dROPanel);
    
    JPanel _exitingSubtypePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
    _exitingSubtypePanel.add(_changeToExistingSubtypeRadioButton);
    _exitingSubtypePanel.add(_listOfExistingSubtypeComboBox);
    _centerPanel.add(_exitingSubtypePanel);    
    _swicthSubtypeOptionButtonGroup.add(_createNewSubtypeRadioButton);
    _swicthSubtypeOptionButtonGroup.add(_changeToExistingSubtypeRadioButton);
    
    _createNewSubtypeRadioButton.setSelected(true);
    _createNewSubtypeRadioButton.doClick();
    
    JPanel _exstingSubtypeAdvanceSettingsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
    _exstingSubtypeAdvanceSettingsPanel.add(_exitingSubtypeDetails);
    String details =StringLocalizer.keyToString(new LocalizedString("RMGUI_CURRRENT_SUBTYPE_DETAILS_KEY", 
                new String[]{"", "", "",""}));
    _exitingSubtypeDetails.setText(details);
    //_centerPanel.add(_exstingSubtypeAdvanceSettingsPanel);
    
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okActionPerformed(e);
      }
    });
    
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelActionPerformed(e);
      }
    });
    
    JComponent [] jComponents = {_okButton, _cancelButton};  
    SwingUtils.makeAllSameLength(jComponents);
    _southPanel.add(_okButton);
    _southPanel.add(_cancelButton);
  }
  
  /**
   * @author Wei Chin
   */
  public void populateData(Project project, java.util.List<PadType> padTypes, Subtype subtype)
  {
    Assert.expect(project != null);
    Assert.expect(padTypes != null);
    Assert.expect(subtype != null);
    
    _padTypes = padTypes;
    _currentSubtype = subtype;    
    _currentProject = project;
    
    setComboBoxesDefaultSelections();
    disableInterferenceCompensationComboBoxIfNecessary();
    
    ComponentType currentComponentType = null;
    
    if(_padTypes.size() > 1)
    {
      String refDes = "";
      String padName = "";
      
      for(PadType padType :padTypes)
      {
        if(currentComponentType == null || (currentComponentType.equals(padType.getComponentType()) == false) )
        {
          currentComponentType = padType.getComponentType();
          refDes = refDes + padType.getComponentType() + ", ";
        }
        padName = padName + padType.getName() + ", ";
      }
      _refDesDetails.setText(refDes);
      _padNameDetails.setText(padName);
    }
    else
    {
      currentComponentType = _padTypes.get(0).getComponentType();
      _refDesDetails.setText(currentComponentType.getReferenceDesignator());
      _padNameDetails.setText(_padTypes.get(0).getName());
    }
    _currentSubtypeDetails.setText(subtype.getShortName());
    _newSubtypeTextField.setText(subtype.getShortName());
    //Khaw Chek Hau - XCR-3330 : Failed to change existing subtype by using new subtype dialog
    _newSubtypeShortName = subtype.getShortName();
    
    _listOfExistingSubtypeComboBox.removeAllItems();
    
    //Khaw Chek Hau - XCR-3330 : Failed to change existing subtype by using new subtype dialog
    java.util.List<Subtype> usedSubtypes = currentComponentType.getSideBoardType().getBoardType().getSubtypesIncludingUnused(subtype.getJointTypeEnum());
//    for(Subtype existingSubtype : project.getPanel().getSubtypesUnsorted(subtype.getLandPattern(), subtype.getJointTypeEnum()))
    for(Subtype existingSubtype : usedSubtypes)
    {
      if(existingSubtype.equals(subtype) == false)
      {
        _listOfExistingSubtypeComboBox.addItem(existingSubtype);
      }
    }
    if(_listOfExistingSubtypeComboBox.getItemCount() == 0)
    {
      _changeToExistingSubtypeRadioButton.setEnabled(false);
      _listOfExistingSubtypeComboBox.setEnabled(false);
      _listOfExistingSubtypeComboBox.addItem(StringLocalizer.keyToString("RMGUI_NO_EXIST_SUBTYPE_KEY"));
    }
    else
    {
      _changeToExistingSubtypeRadioButton.setEnabled(true);
    }
  }
  
  /**
   * @param e 
   * @author Wei Chin
   */
  public void okActionPerformed(ActionEvent e)
  {
    Subtype subtype = null;    
    if(_createNewSubtypeRadioButton.isSelected())
    {
      String  newShortName = _newSubtypeTextField.getText();
      PadType onePadType = _padTypes.get(0);
      String  newLongName = newShortName + _UNDERSCORE + onePadType.getJointTypeEnum().getNameWithoutSpaces();
      if(_newSubtypeTextField.getText().isEmpty())
      {
        MessageDialog.showErrorDialog(this, StringLocalizer.keyToString("RMGUI_NEW_SUBTYPE_EMPTY_ERROR_KEY"), StringLocalizer.keyToString("RMGUI_CHANGE_SUBTYPE_DIALOG_KEY"), true);
        return;
      }
      else
      { 
        if( _currentProject.getPanel().doesSubtypeExist(newLongName) || _currentProject.getPanel().doesSubtypeShortNameAndJointTypeExist(newShortName,  onePadType.getJointTypeEnum()))
        {
          MessageDialog.showErrorDialog(this, StringLocalizer.keyToString(new LocalizedString("RMGUI_NEW_SUBTYPE_EXIST_ERROR_KEY", new String[]{newShortName,onePadType.getJointTypeEnum().getName()})), StringLocalizer.keyToString("RMGUI_CHANGE_SUBTYPE_DIALOG_KEY"), true);
          return;
        }
        else
        {
          // start create subtype
          subtype = _currentProject.getPanel().createSubtype(newShortName, onePadType.getLandPatternPad().getLandPattern(), onePadType.getJointTypeEnum());

          if(_copyAllThresholdCheckBox.isSelected())
          {
            // skip notify the observer
            ProjectObservable.getInstance().setEnabled(false);
            try
            {
              for(AlgorithmSettingEnum algorithmSettingEnum : _currentSubtype.getTunedAlgorithmSettingEnums())
              {
                Subtype.copyThreshold(_currentSubtype, subtype, _currentSubtype.getAlgorithmSetting(algorithmSettingEnum));
              }
            }
            finally
            {
              ProjectObservable.getInstance().setEnabled(true);
            }
//            String oriMaskImagePath = Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()) + java.io.File.separator + "mask#" + _currentSubtype.getShortName() + ".png";
//            String newMaskImagePath = Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()) + java.io.File.separator + "mask#" + newShortName + ".png";
              
            //Siew Yeng - XCR-2843 - masking rotation
            java.util.List<String> files = FileUtil.listFiles(Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()), 
                                            FileName.getMaskImagePatternString(_currentSubtype.getShortName()));
            if(files.isEmpty() == false)
            {
              String oriMaskImagePath = files.get(0);
              String rotationString = oriMaskImagePath.substring(oriMaskImagePath.lastIndexOf(FileName.getMaskImageRotationSeparator()) + 1, oriMaskImagePath.length() - FileName.getMaskImageExtension().length());
              
              try
              {
                double rotation = Double.parseDouble(rotationString);
                String newMaskImagePath = FileName.getMaskImageWithRotationFullPath(subtype.getPanel().getProject().getName(), newShortName, rotation);
                FileUtil.copy(oriMaskImagePath, newMaskImagePath);
              }
              catch(NumberFormatException nfe)
              {
                NumberFormatException numFormatException = new NumberFormatException(rotationString);
                numFormatException.initCause(nfe);
                numFormatException.printStackTrace();
              }
              catch(IOException io)
              {
                // do nothing
              }
            }
          }
          //Kee Chin Seong - Added the Magnification 
          subtype.getSubtypeAdvanceSettings().setMagnificationType((MagnificationTypeEnum)_magnificationTypeComboBox.getSelectedItem());
          subtype.getSubtypeAdvanceSettings().setCanUseVariableMagnification(_currentSubtype.getSubtypeAdvanceSettings().canUseVariableMagnification());
          subtype.getSubtypeAdvanceSettings().setSignalCompensation((SignalCompensationEnum)_integrationLevelComboBox.getSelectedItem());
          subtype.getSubtypeAdvanceSettings().setIsInterferenceCompensatable(_currentSubtype.getSubtypeAdvanceSettings().isIsInterferenceCompensatable());
          if(_currentSubtype.getSubtypeAdvanceSettings().isIsInterferenceCompensatable())
          {
            boolean isInterferenceCompensated = _interferenceCompensationComboBox.getSelectedItem().equals(ArtifactCompensationStateEnum.COMPENSATED);
            ArtifactCompensationStateEnum artifactCompensationStateEnum = isInterferenceCompensated? ArtifactCompensationStateEnum.COMPENSATED : ArtifactCompensationStateEnum.NOT_COMPENSATED;
            subtype.getSubtypeAdvanceSettings().setArtifactCompensationState(artifactCompensationStateEnum);
          }
          subtype.getSubtypeAdvanceSettings().setUserGain((UserGainEnum)_cameraGainComboBox.getSelectedItem());
          subtype.getSubtypeAdvanceSettings().setDynamicRangeOptimizationLevel((DynamicRangeOptimizationLevelEnum)_dROLevelComboBox.getSelectedItem());
          //subtype.getSubtypeAdvanceSettings().setUserGain(_currentSubtype.getSubtypeAdvanceSettings().getUserGain());  
          
          //Khaw Chek Hau - XCR-3330 : Failed to change existing subtype by using new subtype dialog
          setExistingSubtypeUsed(false);
        }
      }
    }
    else if(_changeToExistingSubtypeRadioButton.isSelected())
    {
      subtype= (Subtype)_listOfExistingSubtypeComboBox.getSelectedItem();
      //Khaw Chek Hau - XCR-3330 : Failed to change existing subtype by using new subtype dialog
      setExistingSubtypeUsed(true);
    }
    
    //Khaw Chek Hau - XCR-3330 : Failed to change existing subtype by using new subtype dialog
    _newSubtypeShortName = subtype.getShortName();
    
    ProjectObservable.getInstance().setEnabled(false);
    try
    {
      // assign PadTypeSettings
      for(PadType padType : _padTypes)
      {
        padType.getPadTypeSettings().setSubtype(subtype);
      }
    }
    finally
    {
      ProjectObservable.getInstance().setEnabled(true);
    }
    ProjectObservable.getInstance().stateChanged(this, _currentSubtype, subtype, PadTypeSettingsEventEnum.SUBTYPE);
    changeIsComplete = true;
    setVisible(false);
  }
  
   /**
   * @param e 
   * @author Wei Chin
   */
  public void cancelActionPerformed(ActionEvent e)
  {
    changeIsComplete= false;
    dispose();
  }

  /**
   * @return the changeIsComplete
   */
  public boolean changeIsComplete()
  {
    return changeIsComplete;
  }

  /**
   * @author Ying-Huan.Chu
   */
  private void interferenceCompensationComboBox_actionPerformed(ActionEvent e)
  {
    disableIntegrationLevelComboBoxIfNecessary();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void disableIntegrationLevelComboBoxIfNecessary()
  {
    boolean isInterferenceNotCompensatable = ((ArtifactCompensationStateEnum)_interferenceCompensationComboBox.getSelectedItem()).equals(ArtifactCompensationStateEnum.NOT_APPLICABLE);
    _integrationLevelComboBox.setEnabled(!isInterferenceNotCompensatable);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void disableInterferenceCompensationComboBoxIfNecessary()
  {
    if (_currentSubtype.getSubtypeAdvanceSettings().isIsInterferenceCompensatable() == false)
      _interferenceCompensationComboBox.setSelectedItem(ArtifactCompensationStateEnum.NOT_APPLICABLE.toString());
    else
      _interferenceCompensationComboBox.setSelectedItem(ArtifactCompensationStateEnum.NOT_COMPENSATED.toString());
    _interferenceCompensationComboBox.setEnabled(_currentSubtype.getSubtypeAdvanceSettings().isIsInterferenceCompensatable());
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void setComboBoxesDefaultSelections()
  {
    Assert.expect(_currentSubtype != null);
    
    _cameraGainComboBox.setSelectedItem(_currentSubtype.getSubtypeAdvanceSettings().getUserGain());
    _interferenceCompensationComboBox.setSelectedItem(_currentSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState());
    _integrationLevelComboBox.setSelectedItem(_currentSubtype.getSubtypeAdvanceSettings().getSignalCompensation());
    _magnificationTypeComboBox.setSelectedItem(_currentSubtype.getSubtypeAdvanceSettings().getMagnificationType());
    _dROLevelComboBox.setSelectedItem(_currentSubtype.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel());
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public String getNewSubtypeName()
  {
    return _newSubtypeShortName;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR-3330 : Failed to change existing subtype by using new subtype dialog
   */
  public boolean isExistingSubtypeUsed()
  {
    return _isExistingSubtypeUsed;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3330 : Failed to change existing subtype by using new subtype dialog
   */
  private void setExistingSubtypeUsed(boolean isExistingSubtypeUsed)
  {
    _isExistingSubtypeUsed = isExistingSubtypeUsed;
  }
}
