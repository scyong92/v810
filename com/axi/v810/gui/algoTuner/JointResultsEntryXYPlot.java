package com.axi.v810.gui.algoTuner;

import org.jfree.chart.plot.XYPlot;

import com.axi.v810.business.testResults.*;

/**
 * @author Rex Shang
 */
abstract class JointResultsEntryXYPlot extends XYPlot
{
  /**
   * Get the Title of this plot.
   * @return String
   */
  abstract String getTitle();

  /**
   * Get the statistical information of this plot.
   * @author Rex Shang
   */
  abstract String getStatisticalInformationString();

  /**
   * @return MeasurementUnitsEnum if there is one, null otherwise.
   * @author Rex Shang
   */
  abstract protected MeasurementUnitsEnum getMeasurementUnitsEnum();

  /**
   * Used by derived classes to format value.
   * @author Rex Shang
   */
  protected String getFormattedValueString(double value)
  {
    MeasurementUnitsEnum measurementUnit = getMeasurementUnitsEnum();
    String myString;

    if (measurementUnit != null)
    {
      myString = MeasurementUnitsEnum.formatNumberIfNecessary(measurementUnit, value);
    }
    else
    {
      myString = MeasurementValueFormat.getInstance().format(value);
    }

    return myString;
  }
  
  /**
   * @author Chin Seong
   */
  public void clear()
  {
    // do nothing
  }

}
