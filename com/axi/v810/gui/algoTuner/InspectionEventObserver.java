package com.axi.v810.gui.algoTuner;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Matt Wharton
 */
class InspectionEventObserver implements Observer
{
  private final String _TOP = StringLocalizer.keyToString("ATGUI_TOP_SIDE_KEY"); // "(top)"
  private final String _BOTTOM = StringLocalizer.keyToString("ATGUI_BOTTOM_SIDE_KEY"); // "(bot)"
  private RunInspectionPanel _runInspectionPanel = null;

  /**
   * @author Andy Mechtenberg
   */
  public InspectionEventObserver(RunInspectionPanel runInspectionPanel)
  {
    Assert.expect(runInspectionPanel != null);
    _runInspectionPanel = runInspectionPanel;
  }

 /**
  * @param observable Not used
  * @param arg is an InspectionEvent object
  * @author Peter Esbensen
  */
  public synchronized void update(Observable observable, final Object arg)
  {
    // get the image and "paint" the component with it
    Assert.expect(observable != null);
    Assert.expect(arg instanceof InspectionEvent);

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        InspectionEvent event = (InspectionEvent)arg;
        InspectionEventEnum eventEnum = event.getInspectionEventEnum();
        if (eventEnum.equals(InspectionEventEnum.INSPECTION_STARTED))
        {
//          _runInspectionPanel.changeFrontPanelControls(true);
          StartedInspectionEvent startInspectionEvent = (StartedInspectionEvent)event;
          if (startInspectionEvent.hasImageSetData())
            _runInspectionPanel.displayImageSetInfo(startInspectionEvent.getImageSetData());
        }
        else if (eventEnum.equals(InspectionEventEnum.BEGIN_INSPECTING_IMAGE))
        {
          _runInspectionPanel.handleInspectingImage();
        }
        else if (eventEnum.equals(InspectionEventEnum.BEGIN_COLLECT_PRECISION_IMAGE_SET))
        {
          _runInspectionPanel.handleBeginCollectingPrecisionImageSet();
        }
        else if (eventEnum.equals(InspectionEventEnum.INSPECTION_PAUSED))
        {
          _runInspectionPanel.handlePauseEvent();
        }
        else if (eventEnum.equals(InspectionEventEnum.INSPECTION_RESUMED))
        {
          _runInspectionPanel.handleResumeEvent();
        }
        else if (eventEnum.equals(InspectionEventEnum.INSPECTION_COMPLETED))
        {
//          _runInspectionPanel.changeFrontPanelControls(false);
          Assert.expect(event instanceof CompletedInspectionEvent);
          CompletedInspectionEvent inspectionCompleteEvent = (CompletedInspectionEvent)event;
          long inspectionTimeInMillis = inspectionCompleteEvent.getInspectionTimeInMillis();
          int numberOfProcessedJoints = inspectionCompleteEvent.getNumberOfProcessedJoints();
          int numberOfDefectiveJoints = inspectionCompleteEvent.getNumberOfDefectiveJoints();
          int numberOfDefectiveComponents = inspectionCompleteEvent.getNumberOfDefectiveComponents();
          _runInspectionPanel.handleInspectionResults(numberOfProcessedJoints, numberOfDefectiveJoints,
              numberOfDefectiveComponents, inspectionTimeInMillis);
        }
        else if (eventEnum.equals(InspectionEventEnum.DONE_INSPECTING_IMAGE))
        {
          // Do nothing...the image pane handles displaying the image.
        }
        else if (eventEnum.equals(InspectionEventEnum.BEGIN_INSPECTING_JOINT) ||
                 eventEnum.equals(InspectionEventEnum.DONE_INSPECTING_JOINT))
        {
          // Do nothing...
        }
        else if (eventEnum.equals(InspectionEventEnum.POSTING_DIAGNOSTIC))
        {
          Assert.expect(event instanceof AlgorithmDiagnosticInspectionEvent);

          AlgorithmDiagnosticInspectionEvent algorithmDiagEvent = (AlgorithmDiagnosticInspectionEvent)event;

          AlgorithmDiagnosticMessage algorithmDiagnosticMessage = algorithmDiagEvent.getAlgorithmDiagnosticMessage();

          String algorithmName = "";
          if (algorithmDiagnosticMessage.hasAlgorithm())
          {
            Algorithm algorithm = algorithmDiagnosticMessage.getAlgorithm();
            algorithmName = StringLocalizer.keyToString(algorithm.getName());
          }

          String sliceName = "";
          if (algorithmDiagnosticMessage.hasSliceNameEnum())
          {
            SliceNameEnum sliceNameEnum = algorithmDiagnosticMessage.getSliceNameEnum();
            sliceName = sliceNameEnum.getName();
          }

          String jointTypeName = "";
          String componentName = "";
          String padName = "";
          String subtypeName = "";
          boolean topSide = algorithmDiagnosticMessage.getInspectionRegion().isTopSide();

          if (algorithmDiagnosticMessage.hasSubtype())
          {
            Subtype subtype = algorithmDiagnosticMessage.getSubtype();
            subtypeName = subtype.getShortName();
            jointTypeName = StringLocalizer.keyToString(subtype.getJointTypeEnum().getName());
          }

          if (algorithmDiagnosticMessage.hasJointInspectionData())
          {
            JointInspectionData jointInspectionData = algorithmDiagnosticMessage.getJointInspectionData();
            Pad pad = jointInspectionData.getPad();
            componentName = pad.getComponent().getReferenceDesignator();
            padName = pad.getName();
          }
          else
          {
            ReconstructionRegion inspectionRegion = algorithmDiagnosticMessage.getInspectionRegion();
            if (inspectionRegion.hasComponent())
            {
              componentName = inspectionRegion.getComponent().getReferenceDesignator();
            }
          }
          String side = topSide ? _TOP : _BOTTOM;  //"(top)" : "(bot)";
          sliceName  = sliceName + " " + side;
          if (Config.isDeveloperDebugModeOn())
          {
            int focusOffsetInNanoMeters = algorithmDiagnosticMessage.getFocusConfirmationOffsetInNanoMeters();
            float focusOffsetInMils = MathUtil.convertNanoMetersToMils((float)focusOffsetInNanoMeters);
            sliceName = sliceName + "(" + focusOffsetInMils + ")";
          }

          _runInspectionPanel.handleDetailedInformation(topSide, jointTypeName, algorithmName, sliceName,
                                                        componentName, padName, subtypeName);
        }
      }
    });
  }
}
