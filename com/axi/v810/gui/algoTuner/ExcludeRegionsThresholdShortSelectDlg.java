package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * This class handles the little popup threshold editing dialog
 * tailored to the exclude regions threshold
 * @author Andy Mechtenberg
 */
public class ExcludeRegionsThresholdShortSelectDlg extends ThresholdSelectDlg
{
  //The text here MUST match the getAvailableValues call on the Threshold
  // I've hardcoded this because I need to name the check boxes independently
  // in order to enforce the on/off rules outlined below.
  private final String _noOverLapText = "No Overlap";
  private final String _forceLeftOnText = "Force Left On";
  private final String _forceAllOnText = "Force All On";
  private final String _headOffText = "Head Off";
  private final String _tailOffText = "Tail Off";
  private final String _rightOffText = "Right Off";
  private final String _leftOffText = "Left Off";

  private JFrame _frame;

  private JPanel _checkBoxPanel = new JPanel();
  private GridLayout _checkBoxPanelGridLayout = new GridLayout();

  private JPanel _doneButtonPanel = new JPanel();
  private JButton _doneButton = new JButton();

  private JCheckBox _noOverLapCheckBox = new JCheckBox(_noOverLapText);
  private JCheckBox _forceLeftOnCheckBox = new JCheckBox(_forceLeftOnText);
  private JCheckBox _forceAllOnCheckBox = new JCheckBox(_forceAllOnText);
  private JCheckBox _headOffCheckBox = new JCheckBox(_headOffText);
  private JCheckBox _tailOffCheckBox = new JCheckBox(_tailOffText);
  private JCheckBox _rightOffCheckBox = new JCheckBox(_rightOffText);
  private JCheckBox _leftOffCheckBox = new JCheckBox(_leftOffText);

  /**
   * @author Andy Mechtenberg
   */
  ExcludeRegionsThresholdShortSelectDlg(JFrame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);
    _frame = frame;

    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit() throws Exception
  {
    _checkBoxPanel.setLayout(_checkBoxPanelGridLayout);
    _checkBoxPanelGridLayout.setRows(4);
    _doneButton.setPreferredSize(new Dimension(64, 23));
    _doneButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _doneButton.setMnemonic(StringLocalizer.keyToString("ATGUI_DONE_BUTTON_MNEMONIC_KEY").charAt(0));
    _doneButton.setText(StringLocalizer.keyToString("ATGUI_DONE_BUTTON_KEY"));
    _doneButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doneButton_actionPerformed(e);
      }
    });
    this.getRootPane().setDefaultButton(_doneButton);
    getContentPane().add(_checkBoxPanel);
    // set text on the jCheckBoxes

    _checkBoxPanel.add(_noOverLapCheckBox, null);
    _checkBoxPanel.add(_forceLeftOnCheckBox, null);
    _checkBoxPanel.add(_forceAllOnCheckBox, null);
    _checkBoxPanel.add(_headOffCheckBox, null);
    _checkBoxPanel.add(_tailOffCheckBox, null);
    _checkBoxPanel.add(_rightOffCheckBox, null);
    _checkBoxPanel.add(_leftOffCheckBox, null);

    // add the action listeners for the checkboxes
    _noOverLapCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        checkBox_actionPerformed(e);
      }
    });
    _forceLeftOnCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        checkBox_actionPerformed(e);
      }
    });
    _forceAllOnCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        checkBox_actionPerformed(e);
      }
    });
    _headOffCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        checkBox_actionPerformed(e);
      }
    });
    _tailOffCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        checkBox_actionPerformed(e);
      }
    });
    _rightOffCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        checkBox_actionPerformed(e);
      }
    });
    _leftOffCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        checkBox_actionPerformed(e);
      }
    });

    this.getContentPane().add(_doneButtonPanel, BorderLayout.SOUTH);
    _doneButtonPanel.add(_doneButton, null);
  }

  /**
   * @author Andy Mechtenberg
   */
  void doneButton_actionPerformed(ActionEvent e)
  {
    // this just turned off --  make sure something else is on, if not, don't allow it to turn off
    if (atLeastOneChecked() == false)
    {
      _noOverLapCheckBox.setSelected(true);
    }
    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  java.util.List<String> getListOfSettings()
  {
    java.util.List<String> list = new ArrayList<String>();

    Component[] components = _checkBoxPanel.getComponents();
    for (int i = 0; i < components.length; i++)
    {
      Component comp = components[i];
      if (comp instanceof JCheckBox)
      {
        JCheckBox box = (JCheckBox)comp;
        if (box.isSelected())
          list.add(box.getText());
      }
    }
    return list;
  }

  /**
   * @author Andy Mechtenberg
   */
  void setCurrentSettings(java.util.List<String> settings)
  {
    Assert.expect(settings != null);
    if (settings.size() == 0)
      return;
    Component[] components = _checkBoxPanel.getComponents();
    for(String setting : settings)
    {
      for(int i = 0; i < components.length; i++)
      {
        Component comp = components[i];
        if (comp instanceof JCheckBox)
        {
          JCheckBox box = (JCheckBox)comp;
          if (box.getText().equalsIgnoreCase(setting))
            box.setSelected(true);
        }
      }
    }
    updateWithNewSettings();
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean atLeastOneChecked()
  {
    if (_noOverLapCheckBox.isSelected() ||
        _forceLeftOnCheckBox.isSelected() ||
        _forceAllOnCheckBox.isSelected() ||
        _headOffCheckBox.isSelected() ||
        _tailOffCheckBox.isSelected() ||
        _rightOffCheckBox.isSelected() ||
        _leftOffCheckBox.isSelected() )
        return true;
      else
        return false;
  }

  /**
   * Ok, here are the rules:
   * If No Overlap is on:  Force Left On and Force All On cannot be on
   * With Force Left On:  Right Off and Left Off cannot be on.  Head and Tail can be
   * With Force All On:  Right Off, Left Off, Head Off, Tail Off cannot be on
   * At least one check box must be on (No Overlap by default)
   */

  /**
   * @author Andy Mechtenberg
   */
  void updateWithNewSettings()
  {
    if (_noOverLapCheckBox.isSelected())
    {
      // just turned on
      _forceLeftOnCheckBox.setSelected(false);
      _forceLeftOnCheckBox.setEnabled(false);
      _forceAllOnCheckBox.setSelected(false);
      _forceAllOnCheckBox.setEnabled(false);

    }
    else
    {
      _forceLeftOnCheckBox.setEnabled(true);
      _forceAllOnCheckBox.setEnabled(true);
    }

    if (_forceAllOnCheckBox.isSelected())
    {
      // with force all on, none of the offs can be on
      _rightOffCheckBox.setSelected(false);
      _rightOffCheckBox.setEnabled(false);

      _leftOffCheckBox.setSelected(false);
      _leftOffCheckBox.setEnabled(false);

      _headOffCheckBox.setSelected(false);
      _headOffCheckBox.setEnabled(false);

      _tailOffCheckBox.setSelected(false);
      _tailOffCheckBox.setEnabled(false);

      _forceLeftOnCheckBox.setSelected(false);
      _forceLeftOnCheckBox.setEnabled(false);
    }
    else
    {
      _rightOffCheckBox.setEnabled(true);
      _leftOffCheckBox.setEnabled(true);
      _headOffCheckBox.setEnabled(true);
      _tailOffCheckBox.setEnabled(true);
      if (!_noOverLapCheckBox.isSelected())
        _forceLeftOnCheckBox.setEnabled(true);
    }

    if (_forceLeftOnCheckBox.isSelected())
    {
      // with force left on, right off and left off cannot be on.  Head and tail can be turned on
      _rightOffCheckBox.setSelected(false);
      _rightOffCheckBox.setEnabled(false);
      _leftOffCheckBox.setSelected(false);
      _leftOffCheckBox.setEnabled(false);

      if (!_forceAllOnCheckBox.isSelected())
      {
        _headOffCheckBox.setEnabled(true);
        _tailOffCheckBox.setEnabled(true);
      }
    }
    else if (!_forceAllOnCheckBox.isSelected())
    {
      // if force left is off, right off and left on can be on, as can head and tail
      _rightOffCheckBox.setEnabled(true);
      _leftOffCheckBox.setEnabled(true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  void checkBox_actionPerformed(ActionEvent e)
  {
    updateWithNewSettings();
  }
}

