/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.algoTuner;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
 *
 * @author weng-jian.eoh
 */
public class MeasurementJointHighlightTableModel extends DefaultSortTableModel
{

  private List<List<Object>> _displayData = null; // a list of lists
  private final String[] _defectiveComponentJointColumns =
  {
    StringLocalizer.keyToString("ATGUI_DEFECTIVE_COMPONENT_BOARD_NAME_KEY"),
    StringLocalizer.keyToString("ATGUI_DEFECTIVE_COMPONENT_COMPONENT_NAME_KEY"),
    StringLocalizer.keyToString("ATGUI_DEFECTIVE_COMPONENT_PAD_NAME_KEY")
  };

  public static final int _BOARD_NAME_COLUMN = 0;
  public static final int _COMPONENT_NAME_COLUMN = 1;
  public static final int _PAD_NAME_COLUMN = 2;

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public MeasurementJointHighlightTableModel()
  {
    _displayData = new LinkedList<List<Object>>();
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public void populateDisplayData(List<String> defectiveComponentJointList)
  {
    Assert.expect(defectiveComponentJointList != null);

    _displayData.clear();
    if (defectiveComponentJointList.isEmpty() == false)
    {
      for (String defectiveComponentData : defectiveComponentJointList)
      {
        String[] compData = defectiveComponentData.split(",");

        String boardName = compData[0];
        String compName = compData[1];
        String padName = compData[2];

        List<Object> defectiveComponentList = new LinkedList<Object>();
        defectiveComponentList.add(boardName);
        defectiveComponentList.add(compName);
        defectiveComponentList.add(padName);
        _displayData.add(defectiveComponentList);
      }
    }
    sortColumn(_BOARD_NAME_COLUMN, true);
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public int getRowCount()
  {
    if (_displayData == null)
    {
      return 0;
    }
    else
    {
      return _displayData.size();
    }
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public int getColumnCount()
  {
    return _defectiveComponentJointColumns.length;
  }

  /**
   * @author weng-jian.eoh
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(_defectiveComponentJointColumns != null);
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());

    return _defectiveComponentJointColumns[columnIndex];
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public Object getValueAt(int row, int col)
  {
    List<Object> rowData = _displayData.get(row);
    Object data = null;
    data = rowData.get(col);

    // return original value if the data is empty
    if (data instanceof String)
    {
      String emptyString = (String) data;
      if (emptyString.length() <= 0)
      {
        return data;
      }
    }
    return data;
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public boolean isCellEditable(int row, int col)
  {
    return false;
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public void removeRow(int row)
  {
    _displayData.remove(row);
    fireTableRowsDeleted(row, row);
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public void clear()
  {
    _displayData.clear();
    fireTableDataChanged();
  }
  
  public void removeRowBasedOnData(LinkedList<List<String>> deleteData)
  {
    for (List<String> data : deleteData)
    {
      String boardName = data.get(_BOARD_NAME_COLUMN);
      String padName = data.get(_PAD_NAME_COLUMN);
      String componentName = data.get(_COMPONENT_NAME_COLUMN);
      
      for (List<Object> currentData : _displayData)
      {
        if (currentData.get(_PAD_NAME_COLUMN).equals(padName) && 
            currentData.get(_BOARD_NAME_COLUMN).equals(boardName) && 
            currentData.get(_COMPONENT_NAME_COLUMN).equals(componentName))
        {
          _displayData.remove(currentData);
          break;
        }
      }
    }
    fireTableDataChanged();
  }
}
