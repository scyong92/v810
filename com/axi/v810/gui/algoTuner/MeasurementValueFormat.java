package com.axi.v810.gui.algoTuner;

import java.text.*;

/**
 * This class is used to format a measurement value.
 * @author Rex Shang
 */
class MeasurementValueFormat
{
  private static MeasurementValueFormat _instance = null;
  private NumberFormat _numberFormat = null;

  /**
   * Use getInstance()
   * @author Rex Shang
   */
  private MeasurementValueFormat()
  {
    _numberFormat = NumberFormat.getInstance();
    _numberFormat.setMaximumFractionDigits(3);
  }

  /**
   * returns an instance of this class.
   * @author Rex Shang
   */
  static MeasurementValueFormat getInstance()
  {
    if (_instance == null)
    {
      _instance = new MeasurementValueFormat();
    }

    return _instance;
  }

  /**
   * Formats a number.
   * @author Rex Shang
   */
  String format(Number number)
  {
    return _numberFormat.format(number.doubleValue());
  }

}
