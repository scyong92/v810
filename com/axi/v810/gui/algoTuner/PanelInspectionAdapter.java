package com.axi.v810.gui.algoTuner;

import java.util.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * This handles the interface adaption for all inspection related calls
 * @author Andy Mechtenberg
 */
class PanelInspectionAdapter implements Observer
{
  private AlgoTunerGui _algoTunerGui = null;
  private TestExecution _testExecution = null;
  private InspectionEngine _inspectionEngine = null;
  private ImageAcquisitionModeEnum _imageAcquisitionMode;

  private List<ImageSetData> _selectedImageSets = null;

  /**
   * @author Andy Mechtenberg
   */
  PanelInspectionAdapter(AlgoTunerGui mainFrame)
  {
    Assert.expect(mainFrame != null);

    _algoTunerGui = mainFrame;
    _testExecution = TestExecution.getInstance();
    _inspectionEngine = InspectionEngine.getInstance();

    GuiObservable.getInstance().addObserver(this);
  }


  /**
   * XCR1598 Fine Tuning Stop/Abort Operation Delay (2013 Mar)
   * @author Anthony Fong
   */
  public void abort()
  {
      try
      {
          _testExecution.abort();
          _inspectionEngine.abort();
      }
      catch (XrayTesterException xtex)
      {
          // displayError(xtex);
      }
  }

  /**
   * @author Andy Mechtenberg
   */
  void clearAlgorithmInspectionSettings()
  {
    PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
    panelInspectionSettings.clearAllInspectionSettings();
    _inspectionEngine.setPanelInspectionSettings(panelInspectionSettings);
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  void setAlgorithmInspectionSettings(AlgorithmConfiguration algorithmConfiguration,
                                      boolean diagsChecked,
                                      boolean measurementDiagsChecked,
                                      boolean shortDiagsChecked,
                                      boolean disableAlgsChecked,
                                      ImageAcquisitionModeEnum imageAcquisitionMode)
  {
    Assert.expect(algorithmConfiguration != null);
    Assert.expect(imageAcquisitionMode != null);

    _imageAcquisitionMode = imageAcquisitionMode;

    PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
    panelInspectionSettings.clearAllInspectionSettings();

    if (measurementDiagsChecked)
      panelInspectionSettings.setAllJointTypesSingleAlgorithmDiagnosticsEnabled(AlgorithmEnum.MEASUREMENT, true); // (All-meas)

    if (shortDiagsChecked)
      panelInspectionSettings.setAllJointTypesSingleAlgorithmDiagnosticsEnabled(AlgorithmEnum.SHORT, true); // (All-meas)

    // First (if "Diagnostics" is checked), set up the list of algorithms with diagnostics enabled.
    if (diagsChecked)
    {
      for (JointTypeAlgorithmPair jtap : algorithmConfiguration.getDiagnosticFamilyAlgorithmPairs())
      {
        JointTypeChoice jointTypeChoice = jtap.getJointTypeChoice();
        AlgorithmChoice algorithmChoice = jtap.getAlgorithmChoice();

        // now, call the appropriate business layer call (All-All) (Family-All)(All-Algo) (Family-Algo) are the choices
        if (jointTypeChoice.isAllFamilies())
        {
          if (algorithmChoice.isAllFamilies())
          {
            panelInspectionSettings.setAllJointTypesAllAlgorithmsDiagnosticsEnabled(true); // (All-All)
          }
          else
          {
            panelInspectionSettings.setAllJointTypesSingleAlgorithmDiagnosticsEnabled(algorithmChoice.getAlgorithmEnum(),
                                                                                      true); // (All-algo)
          }
        }
        else // family was a specific one
        {
          if (algorithmChoice.isAllFamilies())
          {
            panelInspectionSettings.setSingleJointTypeAllAlgorithmsDiagnosticsEnabled(jointTypeChoice.getJointTypeEnum(),
                                                                                      true); // (Family-All)
          }
          else
          {
            panelInspectionSettings.setSingleJointTypeSingleAlgorithmDiagnosticsEnabled(jointTypeChoice.getJointTypeEnum(),
                                                                                        algorithmChoice.getAlgorithmEnum(),
                                                                                        true); // (Family-Algo)
          }
        }
      }
    }

    // Next (if "Disabled Algorithms" is checked), we set up the list of disabled algorithms.
    if (disableAlgsChecked)
    {
      for (JointTypeAlgorithmPair jtap : algorithmConfiguration.getDisabledFamilyAlgorithmPairs())
      {
        JointTypeChoice jointTypeChoice = jtap.getJointTypeChoice();
        AlgorithmChoice algorithmChoice = jtap.getAlgorithmChoice();

        // now, call the appropriate business layer call (All-All) (Family-All)(All-Algo) (Family-Algo) are the choices
        if (jointTypeChoice.isAllFamilies())
        {
          if (algorithmChoice.isAllFamilies())
          {
            panelInspectionSettings.setAllJointTypesAllAlgorithmsDisabled(true); // (All-All)
          }
          else
          {
            panelInspectionSettings.setAllJointTypesSingleAlgorithmDisabled(algorithmChoice.getAlgorithmEnum(), true); // (All-algo)
          }
        }
        else // family was a specific one
        {
          if (algorithmChoice.isAllFamilies())
          {
            panelInspectionSettings.setSingleJointTypeAllAlgorithmsDisabled(jointTypeChoice.getJointTypeEnum(), true); // (Family-All)
          }
          else
          {
            panelInspectionSettings.setSingleJointTypeSingleAlgorithmDisabled(jointTypeChoice.getJointTypeEnum(),
                                                                              algorithmChoice.getAlgorithmEnum(),
                                                                              true); // (Family-Algo)
          }
        }
      }
    }

    // Set the panel inspection settings in the InspectionEngine.
    _inspectionEngine.setPanelInspectionSettings(panelInspectionSettings);
  }

  /**
   * @author Andy Mechtenberg
   */
  Object getSelectedPanelOrBoard()
  {
    Assert.expect(_algoTunerGui != null);
    return _algoTunerGui.getSelectedPanelOrBoardInstance();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void addBoardFilterIfNecessary(TestProgram testProgram)
  {
    Assert.expect(_algoTunerGui != null);
    Assert.expect(testProgram != null);

    if (_algoTunerGui.isBoardInstanceSelected())
    {
      Board boardInstance = _algoTunerGui.getSelectedBoardInstance();
      testProgram.addFilter(boardInstance);
    }
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  void setupForFullTest()
  {
    MainMenuGui.getInstance().getTestProgramWithReason(StringLocalizer.keyToString("ATGUI_GENERATING_TEST_PROGRAM_KEY"),
                                                       StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                       Project.getCurrentlyLoadedProject());

    Project.getCurrentlyLoadedProject().getTestProgram().clearFilters();
    Project.getCurrentlyLoadedProject().getTestProgram().addFilter(true);
    Project.getCurrentlyLoadedProject().getTestProgram().setUseOnlyInspectedRegionsInScanPath(true);
    addBoardFilterIfNecessary(Project.getCurrentlyLoadedProject().getTestProgram());

    // Specify which subtypes are actually going to be inspected.
    Project project = Project.getCurrentlyLoadedProject();
    Panel panel = project.getPanel();
    PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
    panelInspectionSettings.setInspectedSubtypes(panel);
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  void setupForBoardTest(Board board)
  {
    Assert.expect(board != null);
//    XCR1677 - When fine tuning subype, need to regenerate test program everytime slice setup is changed.
//    MainMenuGui.getInstance().getTestProgramWithReason(StringLocalizer.keyToString("ATGUI_GENERATING_TEST_PROGRAM_KEY"),
//                                                       StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
//                                                       Project.getCurrentlyLoadedProject());

    Project.getCurrentlyLoadedProject().getTestProgram().clearFilters();
    Project.getCurrentlyLoadedProject().getTestProgram().addFilter(true);
    Project.getCurrentlyLoadedProject().getTestProgram().addFilter(board);
    Project.getCurrentlyLoadedProject().getTestProgram().setUseOnlyInspectedRegionsInScanPath(true);

    // Specify which subtypes are actually going to be inspected.
    PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
    panelInspectionSettings.setInspectedSubtypes(board);
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  void setupForJointTypeTest(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    MainMenuGui.getInstance().getTestProgramWithReason(StringLocalizer.keyToString("ATGUI_GENERATING_TEST_PROGRAM_KEY"),
                                                       StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                       Project.getCurrentlyLoadedProject());

    Project.getCurrentlyLoadedProject().getTestProgram().clearFilters();
    Project.getCurrentlyLoadedProject().getTestProgram().addFilter(true);
    Project.getCurrentlyLoadedProject().getTestProgram().addFilter(jointTypeEnum);
    Project.getCurrentlyLoadedProject().getTestProgram().setUseOnlyInspectedRegionsInScanPath(true);
    addBoardFilterIfNecessary(Project.getCurrentlyLoadedProject().getTestProgram());

    // Specify which subtypes are actually going to be inspected.
    PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
    panelInspectionSettings.setInspectedSubtypes(jointTypeEnum);
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  void setupForSubtypeTest(Subtype subtype)
  {
    Assert.expect(subtype != null);
//    XCR1677 - When fine tuning subype, need to regenerate test program everytime slice setup is changed.
//    MainMenuGui.getInstance().getTestProgramWithReason(StringLocalizer.keyToString("ATGUI_GENERATING_TEST_PROGRAM_KEY"),
//                                                       StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
//                                                       Project.getCurrentlyLoadedProject());

    Project.getCurrentlyLoadedProject().getTestProgram().clearFilters();
    Project.getCurrentlyLoadedProject().getTestProgram().addFilter(true);
    Project.getCurrentlyLoadedProject().getTestProgram().addFilter(subtype);
    Project.getCurrentlyLoadedProject().getTestProgram().setUseOnlyInspectedRegionsInScanPath(true);
    addBoardFilterIfNecessary(Project.getCurrentlyLoadedProject().getTestProgram());

    // Specify which subtypes are actually going to be inspected.
    PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
    panelInspectionSettings.setInspectedSubtypes(subtype);
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  void setupForComponentTest(ComponentType componentType)
  {
    Assert.expect(componentType != null);

    MainMenuGui.getInstance().getTestProgramWithReason(StringLocalizer.keyToString("ATGUI_GENERATING_TEST_PROGRAM_KEY"),
                                                       StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                       Project.getCurrentlyLoadedProject());
    Project.getCurrentlyLoadedProject().getTestProgram().clearFilters();
    Project.getCurrentlyLoadedProject().getTestProgram().addFilter(true);
    Project.getCurrentlyLoadedProject().getTestProgram().addFilter(componentType);
    Project.getCurrentlyLoadedProject().getTestProgram().setUseOnlyInspectedRegionsInScanPath(true);
    addBoardFilterIfNecessary(Project.getCurrentlyLoadedProject().getTestProgram());

    // Specify which subtypes are actually going to be inspected.
    PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
    panelInspectionSettings.setInspectedSubtypes(componentType.getSubtypes());
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  void setupForPadTest(PadType padType)
  {
    Assert.expect(padType != null);
//    XCR1677 - When fine tuning subype, need to regenerate test program everytime slice setup is changed.
//    MainMenuGui.getInstance().getTestProgramWithReason(StringLocalizer.keyToString("ATGUI_GENERATING_TEST_PROGRAM_KEY"),
//                                                       StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
//                                                       Project.getCurrentlyLoadedProject());

    Project.getCurrentlyLoadedProject().getTestProgram().clearFilters();
    Project.getCurrentlyLoadedProject().getTestProgram().addFilter(true);
    Project.getCurrentlyLoadedProject().getTestProgram().addFilter(padType);
    Project.getCurrentlyLoadedProject().getTestProgram().setUseOnlyInspectedRegionsInScanPath(true);
    addBoardFilterIfNecessary(Project.getCurrentlyLoadedProject().getTestProgram());

    // Specify which subtypes are actually going to be inspected.
    PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
    panelInspectionSettings.setInspectedSubtypes(padType.getSubtype());
  }

  /**
   * @author Matt Wharton
   */
  void runFilteredTest(String description) throws XrayTesterException
  {
    Assert.expect(description != null);
//    XCR1677 - When fine tuning subype, need to regenerate test program everytime slice setup is changed.
//    MainMenuGui.getInstance().getTestProgramWithReason(StringLocalizer.keyToString("ATGUI_GENERATING_TEST_PROGRAM_KEY"),
//                                                       StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
//                                                       Project.getCurrentlyLoadedProject());

    inspect(Project.getCurrentlyLoadedProject().getTestProgram(), description);
  }

  /**
   * @author Andy Mechtenberg
   * @author Patrick Lacz
   */
  private void inspect(TestProgram testProgram, String description) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(description != null);
    Assert.expect(_imageAcquisitionMode != null);

    if (UnitTest.unitTesting() == false)
    {
      Date date = new Date();
      System.out.println("Tuner inspection Date: " + date);
    }

    if (_imageAcquisitionMode.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS))
    {
      _testExecution.inspectPanelForTestDevelopment(testProgram, description);
    }
    else if (_imageAcquisitionMode.equals(ImageAcquisitionModeEnum.USE_HARDWARE_WHEN_NECESSARY))
    {
      _testExecution.inspectPanelForTestDevelopmentUsingHardwareWhenNecessary(testProgram, description);
    }
    else if (_imageAcquisitionMode.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS))
    {
      Assert.expect(_selectedImageSets != null);
      _testExecution.inspectPanelForTestDevelopmentUsingImageSetData(testProgram, _selectedImageSets, description);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(Observable observable, Object object)
  {
    Assert.expect(observable != null);

    // this should be an update from something already on the swing thread. if not then assert.
    Assert.expect(SwingUtilities.isEventDispatchThread(), "update not from a swing thread");

    if (observable instanceof GuiObservable)
    {
      if(object instanceof GuiEvent)
      {
        GuiEvent guiEvent = (GuiEvent)object;
        GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
        if (guiEventEnum instanceof ImageSetEventEnum)
        {
          ImageSetEventEnum imageSetEventEnum = (ImageSetEventEnum)guiEventEnum;
          if (imageSetEventEnum.equals(ImageSetEventEnum.IMAGE_SET_NOT_SELECTED))
          {
            _selectedImageSets = new LinkedList<ImageSetData>();
          }
          if (imageSetEventEnum.equals(ImageSetEventEnum.IMAGE_SET_SELECTED))
          {
            _selectedImageSets = (java.util.List<ImageSetData>)guiEvent.getSource();
          }
        }
      }
    }
    else
      Assert.expect(false);
  }
}
