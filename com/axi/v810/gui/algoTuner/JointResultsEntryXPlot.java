package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.util.List;

import org.jfree.chart.axis.*;
import org.jfree.chart.plot.*;
import org.jfree.chart.renderer.xy.*;
import org.jfree.ui.*;

import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * This class is used to generate a JFreeChart plot that will display a X-Plot
 * for mulitple JointResultsEntry.
 * @author Rex Shang
 */
class JointResultsEntryXPlot extends JointResultsEntryXYPlot
{
  private List<JointResultsEntry> _jointResultsEntries;
  private List<Integer> _filterRow;
  private MeasurementEnum _measurementType;
  private SliceNameEnum _sliceType;
  //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma
  private SigmaTypeEnum _sigmaType;

  private JointResultsEntryXPlotDataSet _dataSet;
  private XYItemRenderer _xPlotRenderer;

  /**
   * @author Rex Shang
   */
  JointResultsEntryXPlot()
  {
    //Lim, Lay Ngor - XCR 1749 - pass in the sigma type selection from user.    
    this(null, null, null, null);
  }

  /**
   * @author Rex Shang
   * @author Lim, Lay Ngor - XCR1749 add sigma type to input parameter
   */
  JointResultsEntryXPlot(List<JointResultsEntry> jointResultsEntries, MeasurementEnum measurementType, 
    SliceNameEnum sliceType, SigmaTypeEnum sigmaType)
  {
    _jointResultsEntries = jointResultsEntries;
    _measurementType = measurementType;
    _sliceType = sliceType;
    //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma
    _sigmaType = sigmaType;

    drawPlot();
  }

  /**
   * Returns our title so it can be displayed in JFreeChart.
   * @author Rex Shang
   */
  String getTitle()
  {
    String title;
    String chartType = ReviewMeasurementsChartTypeEnum.X_PLOT.toString();

    // Check to see if we have real data.  If not, display "Empty Chart" as title.
    if (_dataSet.getSeriesCount() > 0 && _dataSet.getItemCount(0) > 0)
      title = _measurementType + " " + chartType;
    else
      title = StringLocalizer.keyToString("RMGUI_CHART_EMPTY_CHART_TITLE_KEY") + " " + chartType;

    return title;
  }

  /**
   * Customize our plot.
   * @author Rex Shang
   */
  private void drawPlot()
  {
    _dataSet = new JointResultsEntryXPlotDataSet(_jointResultsEntries, _measurementType, _sliceType);
    setDataset(_dataSet);
    
    _filterRow = _dataSet.getFilterComponentRow();
    _xPlotRenderer = new XYRendererForFineTuning(true, true,_filterRow);
    _xPlotRenderer.setToolTipGenerator(new JointResultsEntryXPlotToolTipGenerator());
    setRenderer(_xPlotRenderer);

    // Get statistical data.
    double mean = _dataSet.getMean();
    double sigma = _dataSet.getStandardDeviation();
    double median = _dataSet.getMedian();

    //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma - START     
    final double threeSigma = 3 * sigma;
    final double fiveSigma = 5 * sigma;    
    Color blue1 = new Color(149, 179, 215, 128);     
    Color blue2 = new Color(191, 209, 231, 128);    
    Color blue3 = new Color(220, 230, 241, 128);    
    // Draw interval marker for different sigma type selection from user.
    addIntervalMarker(mean - sigma, mean + sigma, 1, sigma, blue1);
    if(_sigmaType == SigmaTypeEnum.THREE_SIGMA || _sigmaType == SigmaTypeEnum.FIVE_SIGMA)
    {
      addIntervalMarker(mean - threeSigma, mean - sigma, 3, threeSigma, blue2);
      addIntervalMarker(mean + sigma, mean + threeSigma, 3, threeSigma, blue2);
    }
    if(_sigmaType == SigmaTypeEnum.FIVE_SIGMA)
    {
      addIntervalMarker(mean - fiveSigma, mean - threeSigma, 5, fiveSigma, blue3);
      addIntervalMarker(mean + threeSigma, mean + fiveSigma, 5, fiveSigma, blue3);
    }
    //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma - END
    
    //Draw line marker.    
    Marker meanMarker = JointResultsEntryMarkerFactory.getValueMarker(
        mean,
        StringLocalizer.keyToString("RMGUI_CHART_PANEL_MEAN_LABEL_KEY") + " " + getFormattedValueString(mean));

    Marker medianMarker = JointResultsEntryMarkerFactory.getValueMarker(
        median,
        StringLocalizer.keyToString("RMGUI_CHART_PANEL_MEDIAN_LABEL_KEY") + " " + getFormattedValueString(median));
    medianMarker.setPaint(Color.BLUE);
    medianMarker.setLabelPaint(Color.BLUE);

    // avoid writing the two values on top of each other
    if (Double.isNaN(sigma))
    {
      sigma = 0.001; // really any value will do. The data is probably not very interesting
    }
    if (Math.abs(median - mean) < sigma / 3)
    {
      if (median < mean)
      {
        medianMarker.setLabelAnchor(RectangleAnchor.BOTTOM_LEFT);
        medianMarker.setLabelTextAnchor(TextAnchor.TOP_LEFT);
      }
      else
      {
        meanMarker.setLabelAnchor(RectangleAnchor.BOTTOM_LEFT);
        meanMarker.setLabelTextAnchor(TextAnchor.TOP_LEFT);
      }
    }

    medianMarker.setLabelAnchor(RectangleAnchor.CENTER);
    meanMarker.setLabelAnchor(RectangleAnchor.BOTTOM_LEFT);
    
    addRangeMarker(medianMarker, Layer.FOREGROUND);
    addRangeMarker(meanMarker, Layer.FOREGROUND);

    // Set up plot axis.
    NumberAxis xAxis = new NumberAxis(StringLocalizer.keyToString("RMGUI_CHART_X_PLOT_X_AXIS_LABEL_KEY"));
    xAxis.setAutoRangeIncludesZero(false);
    setDomainAxis(xAxis);

    String yAxisLabel = StringLocalizer.keyToString("RMGUI_CHART_X_PLOT_Y_AXIS_LABEL_KEY");
    MeasurementUnitsEnum measurementUnit = _dataSet.getMeasurementUnitsEnum();
    if (measurementUnit != null && measurementUnit.getName().length() != 0)
    {
      yAxisLabel += " (" + measurementUnit.getName() + ")";
    }
    NumberAxis yAxis = new NumberAxis(yAxisLabel);
    yAxis.setAutoRangeIncludesZero(false);
    setRangeAxis(yAxis);
  }

  /**
   * Add Interval Domain Marker for X-Plot chart.
   * @author Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma
   */
  void addIntervalMarker(double start, double end, int sigmaType, double sigmaValue, Color color)
  {
    String label = 
      StringLocalizer.keyToString("RMGUI_CHART_PANEL_STANDARD_DEVIATION_LABEL_KEY") + " " + getFormattedValueString(sigmaValue);
    if(sigmaType != 1)
      label = getFormattedValueString(sigmaType) + " " + 
        StringLocalizer.keyToString("RMGUI_CHART_PANEL_SIGMA_LABEL_KEY") + " " + getFormattedValueString(sigmaValue);
    
    //This is the workaround for displaying the label on top of the chart but draw the interval area behind the chart.
    //We draw it twice.    
    RectangleAnchor labelAnchor = RectangleAnchor.TOP_RIGHT;
    TextAnchor labelTextAnchor = TextAnchor.TOP_RIGHT;     
    Marker backgroundMarker = JointResultsEntryMarkerFactory.getIntervalMarker(start, end, label, labelAnchor, labelTextAnchor);
    Marker foregroundMarker = JointResultsEntryMarkerFactory.getIntervalMarker(start, end, label, labelAnchor, labelTextAnchor);

    backgroundMarker.setPaint(color);    
    addRangeMarker(backgroundMarker, Layer.BACKGROUND);//display the marker at background
    
    Color fulltransparent = new Color(149, 179, 215, 0);    
    foregroundMarker.setPaint(fulltransparent);    
    addRangeMarker(foregroundMarker, Layer.FOREGROUND);//display the text label at foreground with full transparent interval
  }
  
  /**
   * Draw a threshold line with its label.
   * @author Rex Shang
   */
  void drawThreashold(String thresholdName, double thresholdValue)
  {
    Marker thresholdMarker = new ValueMarker(thresholdValue);
    thresholdMarker.setLabelAnchor(RectangleAnchor.TOP);
    thresholdMarker.setLabelTextAnchor(TextAnchor.BOTTOM_LEFT);
    thresholdMarker.setLabel(StringLocalizer.keyToString("RMGUI_CHART_PANEL_THRESHOLD_LABEL_KEY")
                             + ": " + thresholdName + " " + thresholdValue);
    thresholdMarker.setPaint(Color.BLUE);
    addRangeMarker(thresholdMarker, Layer.BACKGROUND);
  }

  /**
   * Get the general measurement unit.
   * @author Rex Shang
   */
  protected MeasurementUnitsEnum getMeasurementUnitsEnum()
  {
    return _dataSet.getMeasurementUnitsEnum();
  }

  /**
   * @author Rex Shang
   */
  String getStatisticalInformationString()
  {
    String statisticsInfo = StringLocalizer.keyToString("RMGUI_CHART_PANEL_MEAN_LABEL_KEY")
                            + " " + getFormattedValueString(_dataSet.getMean()) + ", "
                            + StringLocalizer.keyToString("RMGUI_CHART_PANEL_STANDARD_DEVIATION_LABEL_KEY")
                            + " " + getFormattedValueString(_dataSet.getStandardDeviation()) + ", "
    //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma - START
                            + "3 " + StringLocalizer.keyToString("RMGUI_CHART_PANEL_SIGMA_LABEL_KEY")
                            + " " + getFormattedValueString(_dataSet.getStandardDeviation() * 3) + ", "
                            + "5 " + StringLocalizer.keyToString("RMGUI_CHART_PANEL_SIGMA_LABEL_KEY")
                            + " " + getFormattedValueString(_dataSet.getStandardDeviation() * 5) + ", "  
    //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma - END      
                            + StringLocalizer.keyToString("RMGUI_CHART_PANEL_MEDIAN_LABEL_KEY")
                            + " " + getFormattedValueString(_dataSet.getMedian());

    return statisticsInfo;
  }
  
  public void clear()
  {
    _dataSet.clearDataSet();
    _dataSet = null;
  }

}
