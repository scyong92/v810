package com.axi.v810.gui.algoTuner;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;

/**
 * This class displays the information for the board instances on a panel.
 * All cells are editable in-line except for the board type.
 * @author Carli Connally
 */
class MaskImageStatusTable extends JTable
{

  public static final int _SUBTYPE_INDEX = 0;
  public static final int _SUBTYPE_NAME = 1;
  public static final int _MASK_IMAGE_RESOLUTION_SIZE = 2;
  public static final int _MASK_IMAGE_STATUS = 3;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _BOARD_NAME_COLUMN_WIDTH_PERCENTAGE = .10;
  private static final double _BOARD_TYPE_COLUMN_WIDTH_PERCENTAGE = .40;
  private static final double _BOARD_ROTATION_COLUMN_WIDTH_PERCENTAGE = .20;
  private static final double _BOARD_FLIP_COLUMN_WIDTH_PERCENTAGE = .20;

  private NumericRenderer _numericRenderer = new NumericRenderer(2, JLabel.RIGHT);

  private NumericEditor _xNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2, 0.0, Double.MAX_VALUE);
  private NumericEditor _yNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2, 0.0, Double.MAX_VALUE);
//  private BoardAndPanelRotationCellEditor _rotationEditor = new BoardAndPanelRotationCellEditor();
  private CheckCellEditor _checkEditor = new CheckCellEditor(getBackground(), getForeground());


  /**
   * Default constructor.
   * @author Carli Connally
   */
  public MaskImageStatusTable ()
  {
    // do nothing
    getTableHeader().setReorderingAllowed(false);
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Carli Connally
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int boardNameColumnWidth      = (int)(totalColumnWidth * _BOARD_NAME_COLUMN_WIDTH_PERCENTAGE);
    int boardTypeColumnWidth      = (int)(totalColumnWidth * _BOARD_TYPE_COLUMN_WIDTH_PERCENTAGE);
    int boardRotColumnWidth      = (int)(totalColumnWidth * _BOARD_ROTATION_COLUMN_WIDTH_PERCENTAGE);
    int boardFlipColumnWidth      = (int)(totalColumnWidth * _BOARD_FLIP_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_SUBTYPE_INDEX).setPreferredWidth(boardNameColumnWidth);
    columnModel.getColumn(_MASK_IMAGE_RESOLUTION_SIZE).setPreferredWidth(boardRotColumnWidth);
    columnModel.getColumn(_SUBTYPE_NAME).setPreferredWidth(boardTypeColumnWidth);
    columnModel.getColumn(_MASK_IMAGE_STATUS).setPreferredWidth(boardFlipColumnWidth);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };
    columnModel.getColumn(_SUBTYPE_INDEX).setCellEditor(stringEditor);
    columnModel.getColumn(_SUBTYPE_NAME).setCellEditor(stringEditor);
     
    columnModel.getColumn(_MASK_IMAGE_STATUS).setCellEditor(_checkEditor);
    columnModel.getColumn(_MASK_IMAGE_STATUS).setCellRenderer(new CheckCellRenderer());
  }


  /**
   * The decimal places to be displayed can change based on the measurement value that
   * is being displayed.  This method changes the renderers/editors to display the appropriate
   * number of decimal places.
   * @param decimalPlaces Number of decimal places to be displayed for all measurement values in the table.
   * @author Carli Connally
   */
  public void setDecimalPlacesToDisplay(int decimalPlaces)
  {
    Assert.expect(decimalPlaces >= 0);

    _xNumericEditor.setDecimalPlaces(decimalPlaces);
    _yNumericEditor.setDecimalPlaces(decimalPlaces);
    _numericRenderer.setDecimalPlacesToDisplay(decimalPlaces);
  }
}
