package com.axi.v810.gui.algoTuner;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.testResults.*;

/**
 *
 * <p>Title: InspectionRunListData</p>
 *
 * <p>Description: Singleton to hold the shared inspection results for Review Defects
 * and Review Measurements Inspection Run List.</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 * @version 1.0
 */

class InspectionRunListData
{
  private static InspectionRunListData _instance = null;

  private List<PanelResults> _panelResultsList;
  private List<Long> _selectedInspectionRuns = new ArrayList<Long>();
  private boolean _newDataNeeded = true;

  /**
   * This method gets the one and only instance of the InspectionRunListData object
   *
   * @author George Booth
   */
  public synchronized static InspectionRunListData getInstance()
  {
    if (_instance == null)
    {
      _instance = new InspectionRunListData();
    }
    return _instance;
  }

  /**
   * @author George Booth
   */
  private InspectionRunListData()
  {
    _panelResultsList = new ArrayList<PanelResults>();
    _selectedInspectionRuns = new ArrayList<Long>();
  }

  /**
   * @author George Booth
   */
  void setPanelResultsList(List<PanelResults> panelResultsList)
  {
    Assert.expect(panelResultsList != null);
    _panelResultsList = panelResultsList;
  }

  /**
   * @author George Booth
   */
  List<PanelResults> getPanelResultsList()
  {
    Assert.expect(_panelResultsList != null);
    return _panelResultsList;
  }

  /**
   * @author George Booth
   */
  void clearPanelResultsList()
  {
    Assert.expect(_panelResultsList != null);
    _panelResultsList.clear();
  }

  /**
   * @author George Booth
   */
  int getPanelResultsListSize()
  {
    Assert.expect(_panelResultsList != null);
    return _panelResultsList.size();
  }

  /**
   * @author George Booth
   */
  Object[] getPanelResultsListArray()
  {
    Assert.expect(_panelResultsList != null);
    return _panelResultsList.toArray();
  }

  /**
   * @author George Booth
   */
  Long getPanelResultsInspectionStartTimeInMillis(int index)
  {
    return _panelResultsList.get(index).getInspectionStartTimeInMillis();
  }

  /**
   * @author George Booth
   */
  String getImageSetDescriptionForRunId(Long runId)
  {
    for (PanelResults panelResults : _panelResultsList)
    {
      if (panelResults.getInspectionStartTimeInMillis() == runId.longValue())
      {
        return panelResults.getImageSetDescription();
      }
    }
    // not found
    return "";
  }

  /**
   * @author George Booth
   */
  void setSelectedInspectionRuns(List<Long> selectedInspectionRuns)
  {
    Assert.expect(selectedInspectionRuns != null);
    _selectedInspectionRuns = selectedInspectionRuns;
  }

  /**
   * @author George Booth
   */
  void addSelectedInspectionRun(Long runID)
  {
    Assert.expect(runID != null);
    Assert.expect(_selectedInspectionRuns != null);
    _selectedInspectionRuns.add(runID);
  }

  /**
   * @author George Booth
   */
  List<Long> getSelectedInspectionRuns()
  {
    Assert.expect(_selectedInspectionRuns != null);
    return _selectedInspectionRuns;
  }

  /**
   * @author George Booth
   */
  void clearSelectedInspectionRuns()
  {
    Assert.expect(_selectedInspectionRuns != null);
    _selectedInspectionRuns.clear();
  }

  /**
   * @author George Booth
   */
  public void newDataNeeded(boolean isNeeded)
  {
    _newDataNeeded = isNeeded;
  }

  /**
   * @author George Booth
   */
  public boolean isNewDataNeeded()
  {
    return _newDataNeeded;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getIndexOf(long inspectionRun)
  {
    for(PanelResults panelResult : _panelResultsList)
    {
      if (panelResult.getInspectionStartTimeInMillis() == inspectionRun)
        return _panelResultsList.indexOf(panelResult);
    }
    Assert.expect(false, "unable to find results for " + inspectionRun);
    return -1;
  }
}
