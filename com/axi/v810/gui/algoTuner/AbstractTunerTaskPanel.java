package com.axi.v810.gui.algoTuner;

import javax.swing.*;

/**
 * @author Rex Shang
 */
abstract class AbstractTunerTaskPanel extends JPanel
{
  void unpopulate() {};
  abstract void start();
  abstract boolean isReadyToFinish();
  abstract void finish();
  abstract String getTunerTaskName();

  /**
   * Overriding the getName() in java.awt.Component.
   * This string will get displayed as the tab name when added to a tabbed pane.
   * @author Rex Shang
   */
  public String getName()
  {
    return getTunerTaskName();
  }
}
