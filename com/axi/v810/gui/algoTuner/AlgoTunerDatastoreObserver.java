package com.axi.v810.gui.algoTuner;

import java.io.*;
import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 * This class handles the observer updates coming from a test inspection in progress
 * @author Andy Mechtenberg
 */
public class AlgoTunerDatastoreObserver implements Observer
{
  private AlgoTunerGui _algoTunerGui = null;

  /**
   * @author Andy Mechtenberg
   */
  public AlgoTunerDatastoreObserver()
  {
    // do nothing
  }

  /**
   * @author Andy Mechtenberg
   */
  public AlgoTunerDatastoreObserver(AlgoTunerGui gui)
  {
    Assert.expect(gui != null);
    _algoTunerGui = gui;
  }

 /**
  * This must by synchronous - do not make any calls back into the server on this thread
  * or you will get a deadlock situation
  *
  * @author Andy Mechtenberg
  */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProjectObservable)
        {
          if (argument instanceof ProjectChangeEvent)
          {
            ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)argument;
            ProjectChangeEventEnum eventEnum = projectChangeEvent.getProjectChangeEventEnum();
            if (eventEnum instanceof SubtypeEventEnum)
            {
              SubtypeEventEnum subtypeEventEnum = (SubtypeEventEnum)eventEnum;
              if (subtypeEventEnum.equals(SubtypeEventEnum.TUNED_VALUE) ||
                  subtypeEventEnum.equals(SubtypeEventEnum.TUNED_SLICE_HEIGHT_VALUE))
              {
                Subtype subtype = (Subtype)projectChangeEvent.getSource();
                Pair<AlgorithmSettingEnum, Serializable> pair = (Pair<AlgorithmSettingEnum, Serializable>)projectChangeEvent.getNewValue();
                AlgorithmSettingEnum algorithmSettingEnum = pair.getFirst();
                _algoTunerGui.handleThresholdsChangedEvent(subtype, algorithmSettingEnum);
              }
              else if(subtypeEventEnum.equals(SubtypeEventEnum.VOID_METHOD))
              {
                Subtype subtype = (Subtype)projectChangeEvent.getSource();
                Pair<AlgorithmSettingEnum, Serializable> pair = (Pair<AlgorithmSettingEnum, Serializable>)projectChangeEvent.getNewValue();
                AlgorithmSettingEnum algorithmSettingEnum = pair.getFirst();
                _algoTunerGui.handleVodingThresholdChangedEvent(subtype, algorithmSettingEnum);
              }
              else if(subtypeEventEnum.equals(SubtypeEventEnum.MASK_IMAGE))
              {
                Subtype subtype = (Subtype)projectChangeEvent.getSource();
                Pair<AlgorithmSettingEnum, Serializable> pair = (Pair<AlgorithmSettingEnum, Serializable>)projectChangeEvent.getNewValue();
                AlgorithmSettingEnum algorithmSettingEnum = pair.getFirst();
                _algoTunerGui.handleVodingThresholdChangedEvent(subtype, algorithmSettingEnum);
              }
              else if (subtypeEventEnum.equals(SubtypeEventEnum.ALGORITHM_ENABLED))
              {
                Subtype subtype = (Subtype)projectChangeEvent.getSource();
                Pair<AlgorithmEnum, Boolean> pair = (Pair<AlgorithmEnum, Boolean>)projectChangeEvent.getNewValue();
                _algoTunerGui.handleAlgorithmEnumEnabled(subtype, pair.getFirst(), pair.getSecond().booleanValue());
              }
              else if (subtypeEventEnum.equals(SubtypeEventEnum.LEARN) ||
                       subtypeEventEnum.equals(SubtypeEventEnum.SET_LEARNED_SETTINGS_TO_DEFAULTS) ||
                       subtypeEventEnum.equals(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_ALL_ALGORITHMS_EXCEPT_SHORT))
              {
                _algoTunerGui.handleThresholdsChangedEvent(); // happens outside of the tuner
              }
              else if (subtypeEventEnum.equals(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_SHORT))
              {
                // do nothing -- shorts data is not displayed in the tuner
              }
              else if (subtypeEventEnum.equals(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_EXPECTED_IMAGE))
              {
                // do nothing -- expected image data is not displayed in the tuner
              }
            }
            else if (eventEnum instanceof PadTypeSettingsEventEnum)
            {
              PadTypeSettingsEventEnum padTypeSettingsEvent = (PadTypeSettingsEventEnum)eventEnum;
              if (padTypeSettingsEvent.equals(PadTypeSettingsEventEnum.SUBTYPE))
              {
                _algoTunerGui.saveUIState();
                _algoTunerGui.saveActiveUIState();
                _algoTunerGui.updateWithProjectData();
              }
            }
            else if (eventEnum instanceof ProjectStateEventEnum)
            {
              ProjectStateEventEnum projectStateEventEnum = (ProjectStateEventEnum)eventEnum;
              if (projectStateEventEnum.equals(ProjectStateEventEnum.SHORT_LEARNING_FILE_RECORDS_INVALID))
              {
                // do something later?
              }
              if (projectStateEventEnum.equals(ProjectStateEventEnum.SHORT_LEARNING_FILE_RECORDS_VALID))
              {
                // do something later?
              }
              if (projectStateEventEnum.equals(ProjectStateEventEnum.EXPECTED_IMAGE_LEARNING_FILE_RECORDS_INVALID))
              {
                // do something later?
              }
              if (projectStateEventEnum.equals(ProjectStateEventEnum.EXPECTED_IMAGE_LEARNING_FILE_RECORDS_VALID))
              {
                // do something later?
              }
            }
            else if (eventEnum instanceof ProjectEventEnum)
            {
              ProjectEventEnum projectEventEnum = (ProjectEventEnum)eventEnum;

              if (projectEventEnum.equals(ProjectEventEnum.LIBRARY_IMPORT_COMPLETE) || projectEventEnum.equals(ProjectEventEnum.IMPORT_LAND_PATTERN_COMPLETE))
              {
                _algoTunerGui.updateWithProjectData();
              }

            }
          }
        }
        else
          Assert.expect(false, "AlgoTunerDatastoreObserver: Unexpected observerable: " + observable.getClass().getName());
      }
    });
  }
}
