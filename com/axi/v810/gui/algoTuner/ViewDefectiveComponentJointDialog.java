/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.algoTuner;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.List;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

/**
 *
 * @author weng-jian.eoh
 */
public class ViewDefectiveComponentJointDialog extends JDialog
{

  private JFrame _frame = null;
  private JPanel _mainPanel = new JPanel();
  private JPanel _okButtonPanel = new JPanel();
  private JScrollPane _defectiveComponentJointScrollPane;
  private JButton _deleteButton;
  private JButton _okButton;
  private BorderLayout _mainPanelBorderLayout;
  private FlowLayout _okButtonPanelFlowLayout;
  private MeasurementJointHighlightTable _defectComponentJointTable;
  private MeasurementJointHighlightTableModel _defectComponentJointTableModel;

  private java.util.List<String> _defectiveComponentData;

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public ViewDefectiveComponentJointDialog(JFrame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);

    _frame = frame;
    this.setResizable(false);
    createPanel();

  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public void createPanel()
  {
    jbInit();

    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _okButtonPanel.setLayout(_okButtonPanelFlowLayout);
    _okButtonPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _okButtonPanelFlowLayout.setHgap(20);
    _okButtonPanelFlowLayout.setVgap(10);

    _okButton.setMnemonic(StringLocalizer.keyToString("GUI_OK_BUTTON_MNEMONIC_KEY").charAt(0));
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });

    _deleteButton.setMnemonic(StringLocalizer.keyToString("GUI_DELETE_BUTTON_MNEMONIC_KEY").charAt(0));
    _deleteButton.setText(StringLocalizer.keyToString("GUI_DELETE_ALL_BUTTON_KEY"));
    _deleteButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteAllRow();
      }
    });

    _okButtonPanel.add(_deleteButton);
    _okButtonPanel.add(_okButton);

    _defectiveComponentJointScrollPane.getViewport().add(_defectComponentJointTable);
    _mainPanel.add(_defectiveComponentJointScrollPane, BorderLayout.NORTH);
    _mainPanel.add(_okButtonPanel, BorderLayout.SOUTH);
    getContentPane().add(_mainPanel);
    pack();
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  private void jbInit()
  {
    _mainPanel = new JPanel();
    _okButtonPanel = new JPanel();
    _mainPanelBorderLayout = new BorderLayout();
    _okButtonPanelFlowLayout = new FlowLayout(FlowLayout.LEFT, 0, 0);
    _okButton = new JButton();
    _deleteButton = new JButton();
    _defectiveComponentJointScrollPane = new JScrollPane();
    _defectComponentJointTableModel = new MeasurementJointHighlightTableModel();
    _defectComponentJointTable = new MeasurementJointHighlightTable(_defectComponentJointTableModel);

    _defectComponentJointTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _defectComponentJointTable.addMouseListener(new MouseAdapter()
    {
      // show context menu on right click
      public void mouseReleased(MouseEvent e)
      {
        if (e.isPopupTrigger())
        {
          handleDefectiveComponentJointPopupMenu(e);
        }
      }
    });
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public void populateDialogWithData(java.util.List<String> defectiveComponentJointList)
  {
    Assert.expect(defectiveComponentJointList != null);

    _defectiveComponentData = defectiveComponentJointList;

    populateTable();

  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  private void populateTable()
  {
    _defectComponentJointTableModel.populateDisplayData(_defectiveComponentData);
  }

  private void handleDefectiveComponentJointPopupMenu(MouseEvent e)
  {
    final int selectedRowIndex = _defectComponentJointTable.rowAtPoint(e.getPoint());
    if (selectedRowIndex == -1)
    {
      // user dragged cursor out of the table
      return;
    }

    // show the popup menu
    if (selectedRowIndex >= 0)
    {
//      _jointInformationTable.setRowSelectionInterval(selectedRowIndex, selectedRowIndex);
      JPopupMenu popupMenu = new JPopupMenu();
      JMenuItem deleteMenu = new JMenuItem("Delete");
      
      final int[] selectedRows = _defectComponentJointTable.getSelectedRows();
      deleteMenu.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          if (selectedRows.length == 1)
          {
            deleteSelectedRow(selectedRowIndex);
          }
          else
          {
            deleteMultipleRows();
          }
        }
      });

      popupMenu.add(deleteMenu);

      popupMenu.show(e.getComponent(), e.getX(), e.getY());
    }

  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  private void deleteSelectedRow(int row)
  {
    String boardName = (String) _defectComponentJointTableModel.getValueAt(row, _defectComponentJointTableModel._BOARD_NAME_COLUMN);
    String componentName = (String) _defectComponentJointTableModel.getValueAt(row, _defectComponentJointTableModel._COMPONENT_NAME_COLUMN);
    String padName = (String) _defectComponentJointTableModel.getValueAt(row, _defectComponentJointTableModel._PAD_NAME_COLUMN);

    String boardNameAndComponentNameAndPadName = boardName + "," + componentName + "," + padName;
    try
    {
      MeasurementJointHighlightSettingsReader reader = new MeasurementJointHighlightSettingsReader(Project.getCurrentlyLoadedProject().getName());
      java.util.List<String> defectiveComponentList = reader.getDefectiveSetting();

      if (defectiveComponentList.contains(boardNameAndComponentNameAndPadName))
      {
        defectiveComponentList.remove(boardNameAndComponentNameAndPadName);
      }

      MeasurementJointHighlightSettingsWriter writter = new MeasurementJointHighlightSettingsWriter(Project.getCurrentlyLoadedProject().getName());

      writter.saveSettings(defectiveComponentList);
    }
    catch (DatastoreException ex)
    {
      MessageDialog.showErrorDialog(_frame,
        ex.getLocalizedMessage(),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    }
    catch (BusinessException x)
    {
      MessageDialog.showErrorDialog(_frame,
        x.getLocalizedMessage(),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    }

    int modelIndex = _defectComponentJointTable.convertRowIndexToModel(row); // converts the row index in the view to the appropriate index in the model
    MeasurementJointHighlightTableModel model = (MeasurementJointHighlightTableModel) _defectComponentJointTable.getModel();
    model.removeRow(modelIndex);

  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  private void deleteAllRow()
  {
    try
    {
      MeasurementJointHighlightSettingsReader reader = new MeasurementJointHighlightSettingsReader(Project.getCurrentlyLoadedProject().getName());
      java.util.List<String> defectiveComponentList = reader.getDefectiveSetting();

      defectiveComponentList.clear();

      MeasurementJointHighlightSettingsWriter writter = new MeasurementJointHighlightSettingsWriter(Project.getCurrentlyLoadedProject().getName());

      writter.saveSettings(defectiveComponentList);
    }
    catch (DatastoreException ex)
    {
      MessageDialog.showErrorDialog(_frame,
        ex.getLocalizedMessage(),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    }
    catch (BusinessException x)
    {
      MessageDialog.showErrorDialog(_frame,
        x.getLocalizedMessage(),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    }

    MeasurementJointHighlightTableModel model = (MeasurementJointHighlightTableModel) _defectComponentJointTable.getModel();
    model.clear();

  }
  
  private void deleteMultipleRows()
  {
    int[] rows = _defectComponentJointTable.getSelectedRows();
    java.util.LinkedList<java.util.List<String>> deleteData = new LinkedList<java.util.List<String>>();
    for (int row : rows)
    {
      String boardName = (String) _defectComponentJointTableModel.getValueAt(row, _defectComponentJointTableModel._BOARD_NAME_COLUMN);
      String componentName = (String) _defectComponentJointTableModel.getValueAt(row, _defectComponentJointTableModel._COMPONENT_NAME_COLUMN);
      String padName = (String) _defectComponentJointTableModel.getValueAt(row, _defectComponentJointTableModel._PAD_NAME_COLUMN);
      
      java.util.List<String> data = new java.util.ArrayList<String>();
      data.add(boardName);
      data.add(componentName);
      data.add(padName);
      deleteData.add(data);
      String boardNameAndComponentNameAndPadName = boardName + "," + componentName + "," + padName;
      try
      {
        MeasurementJointHighlightSettingsReader reader = new MeasurementJointHighlightSettingsReader(Project.getCurrentlyLoadedProject().getName());
        java.util.List<String> defectiveComponentList = reader.getDefectiveSetting();

        if (defectiveComponentList.contains(boardNameAndComponentNameAndPadName))
        {
          defectiveComponentList.remove(boardNameAndComponentNameAndPadName);
        }

        MeasurementJointHighlightSettingsWriter writter = new MeasurementJointHighlightSettingsWriter(Project.getCurrentlyLoadedProject().getName());

        writter.saveSettings(defectiveComponentList);
      }
      catch (DatastoreException ex)
      {
        MessageDialog.showErrorDialog(_frame,
          ex.getLocalizedMessage(),
          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
          true);
      }
      catch (BusinessException x)
      {
        MessageDialog.showErrorDialog(_frame,
          x.getLocalizedMessage(),
          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
          true);
      }
    }
     MeasurementJointHighlightTableModel model = (MeasurementJointHighlightTableModel) _defectComponentJointTable.getModel();
     model.removeRowBasedOnData(deleteData);
    
  }
}
