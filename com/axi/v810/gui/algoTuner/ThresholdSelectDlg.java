package com.axi.v810.gui.algoTuner;

import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import com.axi.util.*;

/**
 * Generic dialog for threshold selection
 * @author Andy Mechtenberg
 */
class ThresholdSelectDlg extends JDialog
{
  private JFrame _frame;
  private java.util.List<String> _choices = null;

  private JPanel _checkBoxPanel = new JPanel();
  private GridLayout _checkBoxPanelGridLayout = new GridLayout();

  private JPanel _doneButtonPanel = new JPanel();
  private JButton _doneButton = new JButton();

  /**
   * @author Andy Mechtenberg
   */
  ThresholdSelectDlg(JFrame frame, String title, boolean modal, java.util.List<String> choices)
  {
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(choices != null);
    _frame = frame;
    _choices = choices;
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * Used by the two custom ones for exclude regions
   * @author Andy Mechtenberg
   */
  protected ThresholdSelectDlg(JFrame frame, String title, boolean modal)
  {
    super(frame, title, modal);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit() throws Exception
  {
    _checkBoxPanel.setLayout(_checkBoxPanelGridLayout);
    _checkBoxPanelGridLayout.setRows((_choices.size() +1) / 2);
    _doneButton.setPreferredSize(new Dimension(64, 23));
    _doneButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _doneButton.setMnemonic('D');
    _doneButton.setText("Done");
    _doneButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doneButton_actionPerformed(e);
      }
    });
//    _doneButton.setDefaultCapable(true);
    this.getRootPane().setDefaultButton(_doneButton);
    getContentPane().add(_checkBoxPanel);
    // set text on the jCheckBoxes
    for (String choice : _choices)
    {
      JCheckBox checkBox = new JCheckBox(choice);
      _checkBoxPanel.add(checkBox, null);
    }
    this.getContentPane().add(_doneButtonPanel, BorderLayout.SOUTH);
    _doneButtonPanel.add(_doneButton, null);
  }

  /**
   * @author Andy Mechtenberg
   */
  void doneButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  java.util.List<String> getListOfSettings()
  {
    java.util.List<String> list = new ArrayList<String>();

    Component[] components = _checkBoxPanel.getComponents();
    for (int i = 0; i < components.length; i++)
    {
      Component comp = components[i];
      if (comp instanceof JCheckBox)
      {
        JCheckBox box = (JCheckBox)comp;
        if (box.isSelected())
          list.add(box.getText());
      }
    }
    return list;
  }

  /**
   * @author Andy Mechtenberg
   */
  void setCurrentSettings(java.util.List<String> settings)
  {
    Assert.expect(settings != null);
    if (settings.size() == 0)
      return;
    Component[] components = _checkBoxPanel.getComponents();
    for (String setting : settings)
    {
      for(int i = 0; i < components.length; i++)
      {
        Component comp = components[i];
        if (comp instanceof JCheckBox)
        {
          JCheckBox box = (JCheckBox)comp;
          if (box.getText().equalsIgnoreCase(setting))
            box.setSelected(true);
        }
      }
    }
  }
}
