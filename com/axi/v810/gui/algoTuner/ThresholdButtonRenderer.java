package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.imageAnalysis.*;

/**
 * Private class used only the the ThresholdButtonRenderer
 * @author Andy Mechtenberg
 */
class MultiSelectPanel extends JPanel
{
  private JLabel _label = new JLabel("(All)");
  private JButton _button;

  /**
   * @author Andy Mechtenberg
   */
  MultiSelectPanel()
  {
    setLayout(new BorderLayout());
    _label.setBackground(Color.white);
    _label.setOpaque(true);
    add(_label, BorderLayout.CENTER);
    _button = new JButton("...");
    //add(button, BorderLayout.EAST);
    _button.setPreferredSize(new Dimension(15,15));
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setText(String text)
  {
    _label.setText(text);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void addButton()
  {
    add(_button, BorderLayout.EAST);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void removeButton()
  {
    remove(_button);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setMultiSelectBackgroundColor(Color color)
  {
    _label.setBackground(color);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setMultiSelectForegroundColor(Color color)
  {
    _label.setForeground(color);
  }
}

/**
 * Class to render the customer button that edits thresholds
 * @author Andy Mechtenberg
 */
public class ThresholdButtonRenderer extends MultiSelectPanel implements TableCellRenderer
{
  private AlgorithmEnum _algorithm = null;
  private String _threshName = null;
  private PanelDataAdapter _panelDataAdapter = null;

  /**
   * @author Andy Mechtenberg
   */
  public ThresholdButtonRenderer(AlgorithmEnum algoEnum, String threshName, PanelDataAdapter panelDataAdapter)
  {
    Assert.expect(algoEnum != null);
    Assert.expect(threshName != null);
    Assert.expect(panelDataAdapter != null);
    _algorithm = algoEnum;
    _threshName = threshName;
    _panelDataAdapter = panelDataAdapter;
    setOpaque(true);
  }

  /**
   * @author Andy Mechtenberg
   */
  public Component getTableCellRendererComponent(JTable table, Object value,
       boolean isSelected, boolean hasFocus, int row, int column)
  {
    boolean isEditable = table.isCellEditable(row, column);

    setOpaque(true);
    if (isEditable)
    {
      if (isSelected)
        addButton();
      else
        removeButton();
    }

    if (isSelected)
    {
      setForeground(table.getSelectionForeground());
      setBackground(table.getSelectionBackground());
    }
    else
    {
      setForeground(table.getForeground());
      setBackground(UIManager.getColor("Button.background"));
    }


    if (!isEditable)  // if default column -- not editable, but we want an intelligent tooltip
    {
      if (value instanceof String)
      {
        setText((String)value);
        setToolTipText((String)value);
      }
      else
      {
        java.util.List defaults = (java.util.List)_panelDataAdapter.getDefaultThresholdValue(_algorithm, _threshName);
        /** @todo PE fix this!
        Vector maxVals = _panelDataAdapter.getAvailableThresholdValues(_algorithm, _threshName);
        setToolTipText(defaults.toString());
        if (_panelDataAdapter.getThresholdLegacyName(_algorithm, _threshName).equalsIgnoreCase("EXCLUDE_REGIONS"))
        {
          if (defaults.size() == 1)
          {
            String valstr = (String)defaults.get(0);
            if (valstr.equalsIgnoreCase("Force All On"))
            {
              setText("Force All On");
            }
            else if (valstr.equalsIgnoreCase("No Overlap"))
            {
              setText("No Overlap");
            }
            else if (valstr.equalsIgnoreCase("Force Left On"))
            {
              setText("Force Left On");
            }
            else
            {
              setText("Partial");
            }
          }
          else
          {
            setText("Partial");
          }
        }
        else
        {
          if (maxVals.size() == defaults.size())
          {
              setText("All");
          }
          else if (defaults.size() == 0)
          {
            setText("None");
            setToolTipText("None");
          }
          else
          {
            setText("Partial");
          }
        }
        */
      }

      // since the MultiSelectDlg class is really what is being rendered, I'm calling functions in
      // that class to set foreground and background colors
      if (isSelected)
      {
        setMultiSelectBackgroundColor(table.getSelectionBackground());
        setMultiSelectForegroundColor(table.getSelectionForeground());
      }
      else
      {
        setMultiSelectBackgroundColor(table.getBackground());
        setMultiSelectForegroundColor(table.getForeground());
      }
      return this;
    }

    // at this point if cell is editable
    // value is either a String or a linked List
    if (value instanceof String)
      setText((String)value);
    else
    {
      /** @todo PE fix this!
      Vector maxVals = _panelDataAdapter.getAvailableThresholdValues(_algorithm, _threshName);
      java.util.List val = (java.util.List)value;
      setToolTipText(val.toString());
      if (_panelDataAdapter.getThresholdLegacyName(_algorithm, _threshName).equalsIgnoreCase("EXCLUDE_REGIONS"))
      {
        if (val.size() == 1)
        {
          String valstr = (String)val.get(0);
          if (valstr.equalsIgnoreCase("Force All On"))
          {
            setText("Force All On");
          }
          else if (valstr.equalsIgnoreCase("No Overlap"))
          {
            setText("No Overlap");
          }
          else if (valstr.equalsIgnoreCase("Force Left On"))
          {
            setText("Force Left On");
          }
          else
          {
            setText("Partial");
          }
        }
        else
        {
          setText("Partial");
        }
      }
      else  // not the exclude regions threshold
      {
        if (maxVals.size() == val.size())
        {
          setText("All");
        }
        else if (val.size() == 0)
        {
          setText("None");
          setToolTipText("None");
        }
        else
        {
          setText("Partial");
        }
      }
      */
    }



    // need to set the background to yellow if value is different from default.  Since both are lists,
    // this is a bit involved.
    java.util.List<String> valList = (java.util.List<String>)_panelDataAdapter.getThresholdValue(_algorithm, _threshName);
    java.util.List<String> defaultValList = (java.util.List<String>)_panelDataAdapter.getDefaultThresholdValue(_algorithm, _threshName);

    if (!compareLists(valList, defaultValList))
    {
      //setBackground(new Color(204, 255, 255));  // light blue
      //setBackground(new Color(204, 255, 204));  // light green
      setMultiSelectBackgroundColor(new Color(255, 255, 153));  // light yellow.  Also set in class CustomThresholdRenderer
    }
    else
      setMultiSelectBackgroundColor(Color.white);

    return this;
  }

  /**
   * @author Andy Mechtenberg
   */
  private boolean compareLists(java.util.List<String> valList, java.util.List<String> defaultValList)
  {
    Assert.expect(valList != null);
    Assert.expect(defaultValList != null);
    if (valList.size() != defaultValList.size())
      return false;

    // if sizes are equal, compare contents one by one
    Iterator<String> vit = valList.iterator();
    Iterator<String> dvit = defaultValList.iterator();
    for (;vit.hasNext() && dvit.hasNext();)
    {
      String vstr = vit.next();
      String dvstr = dvit.next();
      if (!vstr.equals(dvstr))
        return false;
    }
    return true;

  }
}
