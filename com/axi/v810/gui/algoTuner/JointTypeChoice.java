package com.axi.v810.gui.algoTuner;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This class contains the user choices for Algorithm Family
 * The choices are either a particular AlgorithmFamilyEnum or "All"
 * @author Andy Mechtenberg
 */
public class JointTypeChoice implements Serializable
{
  private JointTypeEnum _jointTypeEnum;
  private boolean _all;

  /**
   * @author Andy Mechtenberg
   */
  public JointTypeChoice(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);
    _jointTypeEnum = jointTypeEnum;
    _all = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public JointTypeChoice()
  {
    _all = true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isAllFamilies()
  {
    return _all;
  }

  /**
   * @author Andy Mechtenberg
   */
  public JointTypeEnum getJointTypeEnum()
  {
    Assert.expect(_all == false);
    Assert.expect(_jointTypeEnum != null);
    return _jointTypeEnum;
  }

  /**
   * Objects are equal if they specify the same algorithm family
   * @author Andy Mechtenberg
   */
  public boolean equals(Object rhs)
  {
    if (rhs == null)
      return false;

    if (rhs == this)
      return true;

    if (rhs instanceof JointTypeChoice)
    {
      JointTypeChoice afc = (JointTypeChoice)rhs;

      // if both specify ALL, then they are equal
      if((_all) && (afc._all))
        return true;
      else if(_all != afc._all) // if one specifies all, and the other does not, they are not equal
        return false;
      else
      {
        // if they both specify the same specific algorithm family, they are equal
        if(_jointTypeEnum.equals(afc._jointTypeEnum))
          return true;
        else
          return false;
      }
    }
    else  // not an instance of AlgorithmFamilyChoice
      return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int hashcode()
  {
    if (_all)
      return -1;
    else
      return _jointTypeEnum.hashCode();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    if (_all)
      return StringLocalizer.keyToString("ATGUI_ALL_KEY");
    else
      return _jointTypeEnum.toString();
  }

}
