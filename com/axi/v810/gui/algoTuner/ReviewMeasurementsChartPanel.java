package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.border.*;

import org.jfree.chart.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * This panel is used to display different kinds of statistical charts in
 * the purpose of fine tuning test thresholds.
 * The data used here is from ReviewMeasurementsTableModel.
 * @author Rex Shang
 */
class ReviewMeasurementsChartPanel extends JPanel
{
  // Variables used to print debug message.
  private static final boolean _debug = false;
  private static final String _me = "ReviewMeasurementsChartPanel";

  // JFreeChart related variables.
  private JointResultsEntryXYPlot _plot;
  private JFreeChart _chart;
  private ChartPanel _chartContentPanel;
  private final boolean _SHOW_CHART_LEGEND = false;

  // Joint entry information that will be used to generate _dataSet.
  private List<JointResultsEntry> _jointEntries;
  private MeasurementEnum _measurementType;
  private SliceNameEnum _sliceType;

  // Swing components.
  private JPanel _chartControlPanel = new JPanel();
  private JPanel _chartTypeChoicePanel = new JPanel();
  private JPanel _histogramBinChoicePanel = new JPanel();
  private NumericTextField _numberOfBinTextField = new NumericTextField();
  private JLabel _numberOfBinLabel = new JLabel();
  private CardLayout _chartTypeChoiceExtraPanelCardLayout = new CardLayout();
  private JPanel _chartTypeChoiceExtraPanel = new JPanel();
  private JPanel _xPlotInformationPanel = new JPanel();
  private JLabel _chartTypeLabel = new JLabel();
  private JComboBox _chartTypeComboBox = new JComboBox();
  private JLabel _statisticsInformationLabel = new JLabel();
  private ActionListener _chartTypeComboBoxListener;
  //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma - START
  private JPanel _sigmaTypeChoicePanel = new JPanel();  
  private JLabel _sigmaTypeLabel = new JLabel();
  private JComboBox _sigmaTypeComboBox = new JComboBox();
  private ActionListener _sigmaTypeComboBoxListener;
  
  // Variables used to set the sigma type of the chart.
  private static final SigmaTypeEnum _DEFAULT_SIGMA_TYPE = SigmaTypeEnum.ONE_SIGMA;
  private static final SigmaTypeEnum _MAXIMUM_SIGMA_TYPE = SigmaTypeEnum.FIVE_SIGMA;
  private SigmaTypeEnum _sigmaType = _DEFAULT_SIGMA_TYPE;  
  //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma - END
  
  private DecimalFormat _numberOfBinDecimalFormat;

  // Variables used to set the number of bins for historgram chart.
  private static final int _DEFAULT_NUMBER_OF_BINS_FOR_HISTOGRAM = 30;
  private static final int _MAXIMUM_NUMBER_OF_BINS_FOR_HISTOGRAM = 10000;
  private int _numberOfBinsForHistogram = _DEFAULT_NUMBER_OF_BINS_FOR_HISTOGRAM;

  // Variable used to determine if re-draw is necessary.
  private boolean _isLastChartEmpty = false;
  private ReviewMeasurementsChartTypeEnum _lastSelectedChartType = null;
  private Object _XYPlotobj = new Object();

  /**
   * @author Rex Shang
   */
  ReviewMeasurementsChartPanel()
  {
    _jointEntries = null;
    _measurementType = null;
    _sliceType = null;

    jbInit();
  }

  /**
   * Invoked when new set of filtering information is available to draw a new chart.
   * @author Rex Shang
   */
  void populateDataSet(List<JointResultsEntry> jointEntries, MeasurementEnum measurementType, SliceNameEnum sliceType)
  {
    Assert.expect(jointEntries != null);
    Assert.expect(measurementType != null);
    Assert.expect(sliceType != null);

    _jointEntries = jointEntries;
    _measurementType = measurementType;
    _sliceType = sliceType;

    drawChart();
  }

  /**
   * Invoked when no valid data is available and empty chart is needed.
   * @author Rex Shang
   */
  void clearDataSet()
  {
    _jointEntries = null;
    _measurementType = null;
    _sliceType = null;
    
    clear();
    drawChart();
  }
  /**
   * Chin Seong
   */
  void clear()
  {
    synchronized (_XYPlotobj)
    {
      if (_plot != null)
      {
        _plot.clear();
      }
    }
  }

   /* We may have to draw empty chart multiple times before valid data shows
   * up.  This method is used by drawChart() to determine whether the new
   * draw is needed.
   * @return boolean true if new draw will be performed.
   * @author Rex Shang
   */
  private boolean isRedrawNeeded()
  {
    boolean currentChartEmpty = false;
    if (_jointEntries == null || _measurementType == null || _sliceType == null)
      currentChartEmpty = true;

    if (_isLastChartEmpty
        && currentChartEmpty
        && _lastSelectedChartType.equals(_chartTypeComboBox.getSelectedItem()))
      return false;

    return true;
  }

  /**
   * @author Rex Shang
   */
  private void drawChart()
  {
    synchronized (_XYPlotobj)
    {
      if (isRedrawNeeded() == false)
        return;

      ReviewMeasurementsChartTypeEnum chartType = (ReviewMeasurementsChartTypeEnum)_chartTypeComboBox.getSelectedItem();
      if (chartType == null || ReviewMeasurementsChartTypeEnum.X_PLOT.equals(chartType))
        _plot = new JointResultsEntryXPlot(_jointEntries, _measurementType, _sliceType, _sigmaType);
      else if (ReviewMeasurementsChartTypeEnum.HISTOGRAM.equals(chartType))
        _plot = new JointResultsEntryHistogramPlot(_jointEntries, _measurementType, _sliceType, _numberOfBinsForHistogram, _sigmaType);
      else
        Assert.expect(false, "Chart type is not available");

      _chart = new JFreeChart(_plot.getTitle(), JFreeChart.DEFAULT_TITLE_FONT, _plot, _SHOW_CHART_LEGEND);

      // Set the information label.
      _statisticsInformationLabel.setText(_plot.getStatisticalInformationString());

      _chartContentPanel.setChart(_chart);
    }
  }

  /**
   * Lay out the UI.
   * @author Rex Shang
   */
  private void jbInit()
  {
    _numberOfBinDecimalFormat = new DecimalFormat("#########");
    _numberOfBinDecimalFormat.setParseIntegerOnly(true);

    // Set up chart choice combo box.
    _chartTypeComboBoxListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        chartTypeComboBoxActionPerformed(e);
      }
    };
    _chartTypeChoiceExtraPanel.setLayout(_chartTypeChoiceExtraPanelCardLayout);
    _chartTypeLabel.setText(StringLocalizer.keyToString("RMGUI_CHART_TYPE_LABEL_KEY"));
    _chartTypeComboBox.addActionListener(_chartTypeComboBoxListener);

    _numberOfBinTextField.setFormat(_numberOfBinDecimalFormat);
    _numberOfBinTextField.setColumns(5);
    _numberOfBinTextField.setHorizontalAlignment(NumericTextField.TRAILING);
    _numberOfBinTextField.setValue(_DEFAULT_NUMBER_OF_BINS_FOR_HISTOGRAM);
    _numberOfBinTextField.setToolTipText(StringLocalizer.keyToString("RMGUI_CHART_PANEL_HISTOGRAM_BIN_TOOL_TIP_KEY"));
    _numberOfBinTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        numberOfBinTextFieldActionPerformed(e);
      }
    });
    _numberOfBinLabel.setText(StringLocalizer.keyToString("RMGUI_CHART_PANEL_HISTOGRAM_BIN_LABEL_KEY"));
    
    //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma - START   
    _sigmaTypeLabel.setText(StringLocalizer.keyToString("RMGUI_CHART_PANEL_SIGMA_LABEL_KEY"));  
    _sigmaTypeComboBoxListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        sigmaTypeComboBoxActionPerformed(e);
      }
    };

    _sigmaTypeChoicePanel.add(_sigmaTypeLabel);
    _sigmaTypeChoicePanel.add(_sigmaTypeComboBox);
    //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma - END          

    _chartTypeChoicePanel.add(_chartTypeLabel);
    _chartTypeChoicePanel.add(_chartTypeComboBox);
    //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma       
    _chartTypeChoicePanel.add(_sigmaTypeChoicePanel);
    _chartTypeChoicePanel.add(_chartTypeChoiceExtraPanel);

    _histogramBinChoicePanel.add(_numberOfBinLabel);
    _histogramBinChoicePanel.add(_numberOfBinTextField);

    _chartTypeChoiceExtraPanel.add(_xPlotInformationPanel, ReviewMeasurementsChartTypeEnum.X_PLOT.toString());
    _chartTypeChoiceExtraPanel.add(_histogramBinChoicePanel, ReviewMeasurementsChartTypeEnum.HISTOGRAM.toString());

    _chartContentPanel = new ChartPanel(null);
    _chartContentPanel.setBorder(new EtchedBorder());
    // Call drawChart() to draw the chart, then set the chart in _chartContentPanel.
    // If there is no chart there, setMouseZoomable will throw exception.
    drawChart();
    _chartContentPanel.setMouseZoomable(true, false);
    _chartContentPanel.setDisplayToolTips(true);

    _chartControlPanel.setLayout(new BorderLayout());
    _chartControlPanel.add(_chartTypeChoicePanel, BorderLayout.WEST);

    this.setLayout(new BorderLayout());
    this.add(_chartContentPanel, BorderLayout.CENTER);
    this.add(_chartControlPanel, BorderLayout.NORTH);

    populateChartTypeComboBox();
    //Lim, Lay Ngor - XCR 1749 - Add 3-Sigma & 5-Sigma    
    populateSigmaTypeComboBox();      
  }

  /**
   * When number of bins used for histogram is changed, we need to draw a new chart.
   * @author Rex Shang
   */
  private void numberOfBinTextFieldActionPerformed(ActionEvent e)
  {
    try
    {
      int numOfBin = _numberOfBinTextField.getNumberValue().intValue();

      if (numOfBin < 1)
      {
        _numberOfBinsForHistogram = _DEFAULT_NUMBER_OF_BINS_FOR_HISTOGRAM;
        _numberOfBinTextField.setText(String.valueOf(_numberOfBinsForHistogram));
      }
      else if (numOfBin > _MAXIMUM_NUMBER_OF_BINS_FOR_HISTOGRAM)
      {
        _numberOfBinsForHistogram = _MAXIMUM_NUMBER_OF_BINS_FOR_HISTOGRAM;
        _numberOfBinTextField.setText(String.valueOf(_numberOfBinsForHistogram));
      }
      else
        _numberOfBinsForHistogram = numOfBin;

      drawChart();
    }
    catch (ParseException pe)
    {
      _numberOfBinTextField.setText(String.valueOf(_numberOfBinsForHistogram));
    }
  }

  /**
   * When sigma type selection is changed, we need to draw a new chart.
   * @author Lim, Lay Ngor
   */
  private void sigmaTypeComboBoxActionPerformed(ActionEvent e)
  {
	//Update _sigmaType value.
    _sigmaType = (SigmaTypeEnum)_sigmaTypeComboBox.getSelectedItem();    
    drawChart();
  }
  
  /**
   * Used for testing only.
   * @author Rex Shang
   */
  public static void main(String[] args)
  {
    JPanel panel = new ReviewMeasurementsChartPanel();

    JFrame f = new JFrame("Table of results");
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.getContentPane().add(panel);
    f.setSize(400, 300);
    f.setLocation(300, 300);
    f.setVisible(true);
  }

  /**
   * Invoked when user selects a different chart type.
   * @author Rex Shang
   */
  private void chartTypeComboBoxActionPerformed(ActionEvent e)
  {
    ReviewMeasurementsChartTypeEnum chartType = (ReviewMeasurementsChartTypeEnum)_chartTypeComboBox.getSelectedItem();
    Assert.expect(chartType != null, "Null chart type selected");

    debug("chartTypeComboBoxActionPerformed: " + chartType.toString());
    _chartTypeChoiceExtraPanelCardLayout.show(_chartTypeChoiceExtraPanel, chartType.toString());

    if (ReviewMeasurementsChartTypeEnum.X_PLOT.equals(chartType))
    {
      _xPlotInformationPanel.add(_statisticsInformationLabel);
    }
    else
    {
      _histogramBinChoicePanel.add(_statisticsInformationLabel);
    }

    drawChart();
    _lastSelectedChartType = chartType;
  }

  /**
   * Populate the chart type options.  The list is provided by the ReviewMeasurementsChartTypeEnum.
   * @author Rex Shang
   */
  private void populateChartTypeComboBox()
  {
    // Disable listener so we would not get unwanted events during combo box
    // modification.
    _chartTypeComboBox.removeActionListener(_chartTypeComboBoxListener);

    Collection<ReviewMeasurementsChartTypeEnum> chartTypes =
        ReviewMeasurementsChartTypeEnum.getAllTypes();
    for (ReviewMeasurementsChartTypeEnum chartType : chartTypes)
    {
      _chartTypeComboBox.addItem(chartType);
    }

    // Re-enable listener.
    _chartTypeComboBox.addActionListener(_chartTypeComboBoxListener);
    _chartTypeComboBox.setSelectedItem(ReviewMeasurementsChartTypeEnum.X_PLOT);
  }

  /**
   * Populate the sigma type options.  The list is provided by the SigmaTypeEnum.
   * @author Lim, Lay Ngor
   */
  private void populateSigmaTypeComboBox()
  {
    // Disable listener so we would not get unwanted events during combo box
    // modification.
    _sigmaTypeComboBox.removeActionListener(_sigmaTypeComboBoxListener);

    Collection<SigmaTypeEnum> sigmaTypes = SigmaTypeEnum.getAllTypes();
    for (SigmaTypeEnum sigmaType : sigmaTypes)
    {
      _sigmaTypeComboBox.addItem(sigmaType);
    }

    // Re-enable listener.
    _sigmaTypeComboBox.addActionListener(_sigmaTypeComboBoxListener);
    _sigmaTypeComboBox.setSelectedItem(SigmaTypeEnum.ONE_SIGMA);
  }
  
  /**
   * @author Rex Shang
   */
  private void debug(String message)
  {
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.REVIEW_MEASUREMENT_PANEL_DEBUG))
      System.out.println(_me + "." + message);
  }

}
