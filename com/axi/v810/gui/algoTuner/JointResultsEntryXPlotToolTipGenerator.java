package com.axi.v810.gui.algoTuner;

import org.jfree.chart.labels.*;
import org.jfree.data.xy.*;

import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * This tool tip generator is used to populate the pop up message once the user
 * stopped mouse on a chart data point.
 * @author Rex Shang
 */
class JointResultsEntryXPlotToolTipGenerator extends StandardXYToolTipGenerator
{
  // Static strings used to fill the tool tip.
  private final static String _TOP = StringLocalizer.keyToString("ATGUI_TOP_SIDE_KEY"); // "(top)"
  private final static String _BOTTOM = StringLocalizer.keyToString("ATGUI_BOTTOM_SIDE_KEY"); // "(bot)"
  private final static String _PASS = StringLocalizer.keyToString("RMGUI_PASS_VALUE_KEY"); // "Pass"
  private final static String _FAIL = StringLocalizer.keyToString("RMGUI_FAIL_VALUE_KEY"); // "Fail"

  // use to get image set description from panel results
  private InspectionRunListData _inspectionRunListData = InspectionRunListData.getInstance();

  /**
   * @author Rex Shang
   */
  JointResultsEntryXPlotToolTipGenerator()
  {
  }

  /**
   * Generates the tool tip text for an item in a dataset.
   *
   * @param dataset  the dataset (<code>null</code> not permitted).
   * @param series  the series index (zero-based).
   * @param item  the item index (zero-based).
   *
   * @return The tooltip text (possibly <code>null</code>).
   * @author Rex Shang
   */
  public String generateToolTip(XYDataset dataset, int series, int item)
  {
    JointResultsEntryXPlotDataSet jointResultsDataSet = (JointResultsEntryXPlotDataSet) dataset;
    JointResultsEntry jointResults = jointResultsDataSet.getJointResultsEntry(series, item);

    String tooltip = jointResults.getRunStartTime().toString();

    tooltip += ", " + jointResultsDataSet.getFormattedValue(series, item);
    tooltip += ", " + jointResults.getReferenceDesignator();
    if (jointResults.getPadName().equals(JointResultsEntry.NO_NAME) == false)
      tooltip += "-" + jointResults.getPadName();
    tooltip += "," + jointResults.getBoardName();

    if (jointResults.passed())
      tooltip += ", " + _PASS;
    else
      tooltip += ", " + _FAIL;

    if (jointResults.isTopSide())
      tooltip += ", " + _TOP;
    else
      tooltip += ", " + _BOTTOM;

    String imageSetDescription = _inspectionRunListData.getImageSetDescriptionForRunId(jointResults.getRunStartTime());
    if (imageSetDescription.length() > 0)
      tooltip += "; " + imageSetDescription;

    return tooltip;
  }
}
