package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * This class constructs the delete learned data dialog
 * @author Andy Mechtenberg
 */
public class DeleteLearnedDataDlg extends JDialog
{
  private JPanel _centerPanel = new JPanel();
  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  private JPanel _okCancelPanel = new JPanel();
  private JPanel _deleteSelectPanel = new JPanel();
  private JPanel _jointTypeSubtypePanel = new JPanel();
  private GridLayout _jointTypeSubtypePanelGridLayout = new GridLayout();
  private JButton _cancelButton = new JButton();
  private JButton _okButton = new JButton();
  private Border _etched0505Border;
  private GridLayout _deleteSelectPanelGridLayout = new GridLayout();
  private FlowLayout _okCancelPanelFlowLayout = new FlowLayout();
  private JPanel _jointTypePanel = new JPanel();
  private JPanel _subtypePanel = new JPanel();
  private JComboBox _jointTypeComboBox = new JComboBox();
  private JLabel _jointTypeLabel = new JLabel();
  private JComboBox _subtypeComboBox = new JComboBox();
  private JLabel _subtypeLabel = new JLabel();
  private JCheckBox _deleteLearnedSubtypeDataCheckBox = new JCheckBox();
  private Border _emptySpace0505Border;
  private BorderLayout _jointTypePanelBorderLayout = new BorderLayout();
  private Border _emptySpace5050Border;
  private BorderLayout _subtypePanelBorderLayout = new BorderLayout();
  private Border _emptySpace5000Border;
  private JCheckBox _deleteLearnedJointDataCheckBox = new JCheckBox();

  private PanelDataUtil _panelDataUtil = null;
  private BoardType _boardType;
  private JPanel _okCancelInnerPanel = new JPanel();
  private GridLayout _okCancelInnerGridLayout = new GridLayout();

  /**
   * @author Andy Mechtenberg
   */
  public DeleteLearnedDataDlg(Frame frame, String title, boolean modal, BoardType boardType, PanelDataUtil panelDataUtil)
  {
    super(frame, title, modal);

    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(boardType != null);
    Assert.expect(panelDataUtil != null);
    _boardType = boardType;
    _panelDataUtil = panelDataUtil;

    jbInit();
    pack();
  }


  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    _etched0505Border = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,ColorUtil.getGrayShadowColor()),BorderFactory.createEmptyBorder(0,5,0,5));
    _emptySpace0505Border = BorderFactory.createEmptyBorder(0,5,0,5);
    _emptySpace5050Border = BorderFactory.createEmptyBorder(5,0,5,0);
    _emptySpace5000Border = BorderFactory.createEmptyBorder(5,0,0,0);
    _centerPanel.setLayout(_centerPanelBorderLayout);
    _jointTypeSubtypePanel.setLayout(_jointTypeSubtypePanelGridLayout);
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.setMnemonic(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_MNEMONIC_KEY").charAt(0));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _okButton.setMnemonic(StringLocalizer.keyToString("GUI_OK_BUTTON_MNEMONIC_KEY").charAt(0));
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    _deleteSelectPanel.setBorder(_etched0505Border);
    _deleteSelectPanel.setLayout(_deleteSelectPanelGridLayout);
    _deleteSelectPanelGridLayout.setRows(2);
    _deleteSelectPanelGridLayout.setVgap(5);
    _okCancelPanel.setLayout(_okCancelPanelFlowLayout);
    _okCancelPanelFlowLayout.setHgap(15);
    _jointTypeSubtypePanelGridLayout.setRows(2);
    _jointTypeSubtypePanelGridLayout.setColumns(1);
//    _jointTypeLabel.setPreferredSize(new Dimension(55, 17));
    _jointTypeLabel.setText(StringLocalizer.keyToString("ATGUI_FAMILY_KEY") + ":");
    _subtypeLabel.setPreferredSize(new Dimension(55, 17));
    _subtypeLabel.setText(StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY") + ":");

    // set one of these labels to the preferred size of the other.  To do that, we need to know which is bigger.
    Dimension jointTypeDimension = _jointTypeLabel.getPreferredSize();
    Dimension subtypeDimension = _subtypeLabel.getPreferredSize();
    if (jointTypeDimension.width > subtypeDimension.width)
      _subtypeLabel.setPreferredSize(jointTypeDimension);
    else
      _jointTypeLabel.setPreferredSize(subtypeDimension);

    _jointTypePanel.setLayout(_jointTypePanelBorderLayout);
    _subtypePanel.setLayout(_subtypePanelBorderLayout);
    _jointTypeComboBox.setPreferredSize(new Dimension(120, CommonUtil._COMBO_BOX_HEIGHT));
    _jointTypeComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jointTypeComboBox_actionPerformed(e);
      }
    });
    _subtypeComboBox.setPreferredSize(new Dimension(120, CommonUtil._COMBO_BOX_HEIGHT));
    _deleteLearnedSubtypeDataCheckBox.setText(StringLocalizer.keyToString("ATGUI_DELETE_LEARNED_SUBTYPE_KEY"));
    _deleteLearnedSubtypeDataCheckBox.setMnemonic(StringLocalizer.keyToString("ATGUI_DELETE_LEARNED_SUBTYPE_MNEMONIC_KEY").charAt(0));
    _deleteLearnedSubtypeDataCheckBox.setSelected(true);
    _centerPanel.setBorder(_emptySpace0505Border);
    _centerPanelBorderLayout.setVgap(3);
    _jointTypePanel.setBorder(_emptySpace5050Border);
    _subtypePanel.setBorder(_emptySpace5050Border);
    _okCancelPanel.setBorder(_emptySpace5000Border);
    this.setResizable(false);
    _deleteLearnedJointDataCheckBox.setText(StringLocalizer.keyToString("ATGUI_DELETE_LEARNED_JOINT_KEY"));
    _deleteLearnedJointDataCheckBox.setMnemonic(StringLocalizer.keyToString("ATGUI_DELETE_LEARNED_JOINT_MNEMONIC_KEY").charAt(0));
    _deleteLearnedJointDataCheckBox.setSelected(true);
    _jointTypePanelBorderLayout.setHgap(5);
    _subtypePanelBorderLayout.setHgap(5);
    _okCancelInnerPanel.setLayout(_okCancelInnerGridLayout);
    _okCancelInnerGridLayout.setHgap(15);
    getContentPane().add(_centerPanel);
    _centerPanel.add(_okCancelPanel, BorderLayout.SOUTH);
    _okCancelPanel.add(_okCancelInnerPanel);
    _okCancelInnerPanel.add(_okButton);
    _okCancelInnerPanel.add(_cancelButton);
    _centerPanel.add(_deleteSelectPanel, BorderLayout.CENTER);
    _deleteSelectPanel.add(_deleteLearnedJointDataCheckBox, null);
    _deleteSelectPanel.add(_deleteLearnedSubtypeDataCheckBox, null);
    _centerPanel.add(_jointTypeSubtypePanel, BorderLayout.NORTH);
    _jointTypeSubtypePanel.add(_jointTypePanel, null);
    _jointTypePanel.add(_jointTypeLabel, BorderLayout.WEST);
    _jointTypePanel.add(_jointTypeComboBox, BorderLayout.CENTER);
    _jointTypeSubtypePanel.add(_subtypePanel, null);
    _subtypePanel.add(_subtypeLabel, BorderLayout.WEST);
    _subtypePanel.add(_subtypeComboBox, BorderLayout.CENTER);
    _panelDataUtil.populateComboBoxWithJointTypes(_boardType, _jointTypeComboBox);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    setCursor(new Cursor(Cursor.WAIT_CURSOR));
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  void jointTypeComboBox_actionPerformed(ActionEvent e)
  {
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    _panelDataUtil.populateComboBoxWithSubtypes(_boardType, jointTypeChoice, _subtypeComboBox);
  }
}
