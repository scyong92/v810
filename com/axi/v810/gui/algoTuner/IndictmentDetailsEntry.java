package com.axi.v810.gui.algoTuner;

import com.axi.util.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.util.*;

 /**
 *
 * <p>Title: IndictmentDetailsEntry</p>
 *
 * <p>Description: Helper class for Review Defects DefectDetailsTableModel.  This class
 * encapsulates indictment details.
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class IndictmentDetailsEntry
{
  // indictment key
  private int _indictmentKey = -1;
  // indictment
  private IndictmentEnum _indictment = null;
  // the type of measurement
  private boolean _passingMeasurement = false;
  private boolean _failingMeasurement = false;
  private boolean _relatedMeasurement = false;
  // the measurement results
  private MeasurementResult _measurement = null;

  /**
   * @author George Booth
   */
  public IndictmentDetailsEntry()
  {
    // do nothing
  }

  /**
   * @author George Booth
   */
  void setIndictmentKey(int key)
  {
    _indictmentKey = key;
  }

  /**
   * @author George Booth
   */
  public int getIndictmentKey()
  {
    return _indictmentKey;
  }

  /**
   * @author George Booth
   */
  void setIndictment(IndictmentEnum indictment)
  {
    Assert.expect(indictment != null);
    _indictment = indictment;
  }

  /**
   * @author George Booth
   */
  public IndictmentEnum getIndictment()
  {
    Assert.expect(_indictment != null);
    return _indictment;
  }

  /**
   * @author George Booth
   */
  void setPassingMeasurement()
  {
    _passingMeasurement = true;
    _failingMeasurement = false;
    _relatedMeasurement = false;
  }

  /**
   * @author George Booth
   */
  void setFailingMeasurement()
  {
    _passingMeasurement = false;
    _failingMeasurement = true;
    _relatedMeasurement = false;
  }

  /**
   * @author George Booth
   */
  void setRelatedMeasurement()
  {
    _passingMeasurement = false;
    _failingMeasurement = false;
    _relatedMeasurement = true;
  }

  /**
   * @author George Booth
   */
  public String getIndictmentType()
  {
    if (_passingMeasurement)
    {
      return StringLocalizer.keyToString("RDGUI_PASSED_LABEL_KEY");
    }
    else if (_failingMeasurement)
    {
      return StringLocalizer.keyToString("RDGUI_FAILED_LABEL_KEY");
    }
    else if (_relatedMeasurement)
    {
      return StringLocalizer.keyToString("RDGUI_RELATED_LABEL_KEY");
    }
    else
      return " ";
  }

  /**
   * @author George Booth
   */
  public void setMeasurement(MeasurementResult measurement)
  {
    Assert.expect(measurement != null);
    _measurement = measurement;
  }

  /**
   * @author George Booth
   */
  public MeasurementResult getMeasurement()
  {
    Assert.expect(_measurement != null);
    return _measurement;
  }
  
  /**
   * @author Lim, Lay Ngor
   */
  public boolean hasMeasurementResult()
  {
    return (_measurement != null);
  }
}
