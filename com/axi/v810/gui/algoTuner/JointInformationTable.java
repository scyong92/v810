package com.axi.v810.gui.algoTuner;

import com.axi.guiUtil.*;
import com.axi.util.*;

/**
 * This class displays the information for all the joints in Review Defects
 * @author George Booth
 */
class JointInformationTable extends JSortTable
{

  /**
   * @author George Booth
   */
  public JointInformationTable(JointInformationTableModel model)
  {
    Assert.expect(model != null);
    super.setModel(model);
  }

}
