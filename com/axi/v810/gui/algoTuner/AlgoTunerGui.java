package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.awt.Component;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.largePad.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

//  Function Key Usage:
//     F1  -- Help
//     F2  -- Toggle both Joint/Subtype Learning
//     F3  -- Toggle Diagnostics
//     Ctrl + F2 -- Help on selected Algorithm

/**
 * This class is handles the main Gui elements of the Algorithm Tuner
 * @author Andy Mechtenberg
 */
public class AlgoTunerGui implements Observer
{
  private static final Color _activeTabColor = Color.lightGray;//Color.green.darker();
  private static final Color _activeTabTextColor = Color.black;//Color.green.darker();
  private static final Color _defaultTabColor = Color.gray;  //Color.lightGray;
  private static final Color _defaultTabTextColor = Color.gray;//lightGray;

  private static com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
//  private InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  private GuiObservable _guiObservable = GuiObservable.getInstance();
//  private SelectedRendererObservable _selectedRendererObservable;

  private boolean _active = false;
  private int _sendEvents = 0;

  private TestDev _testDev;
  private Project _project;
  private BoardType _currentBoardType;
  private ComponentType _currentComponentType;

  private TestDevPersistance _tunerPersistSettings = new TestDevPersistance();
  private ProjectPersistance _projectPersistanceSettings;// = new ProjectPersistance();
  private int _currentSelectedAlgorithmTab;  // holds the current tab (used when changing tabs to keep track of the old one)
  private int _currentSelectedTaskTab;
  private int _prevSelectedIndex;

  private boolean _inRunMode = false;

  private PanelDataAdapter _panelDataAdapter = null;
  private PanelInspectionAdapter _panelInspectionAdapter = null;
//  private DiagnosticTextInfoObserver _diagnosticTextInfoObserver = new DiagnosticTextInfoObserver(this);
  private AlgoTunerGuiObserver _algoTunerGuiObserver = null;
//  private InspectionEventObserver _inspectionEventObserver = new InspectionEventObserver(this);
  private AlgoTunerDatastoreObserver _algoTunerDatastoreObserver = null;
  private Map<JointTypeChoice, SubtypeChoice> _jointTypeToSubtypeMap = new HashMap<JointTypeChoice, SubtypeChoice>(20);
  private Map<URL, JDialog> _detailedDescDialogMap = new HashMap<URL, JDialog>(10);


  // images for icons, etc. . .
  private ImageIcon _copyThresholdsImage;
  private ImageIcon _updateNominalsImage;
  private ImageIcon _validateDisabledAlgoImage;
  private ImageIcon _viewMeasurementJointImage;

  private Object _defaultF2Action;  // we need to remove the original action for F2, then restore when we leave.

  private ActionListener _jointTypeComboBoxActionListener;
  private ChangeListener _tunerTaskChangeListener;

  private MainMenuGui _mainUI = null;

  private JPanel _centerPanel = new JPanel();

  private JMenu _toolsMenu = new JMenu();
  private JMenuItem _validateDisabledAlgoMenuItem = new JMenuItem();
  private JMenuItem _viewMeasurementJointHighlightMenuItem = new JMenuItem();
  private JMenuItem _copyThresholdsMenuItem = new JMenuItem();
  private JMenuItem _updateNominalsMenuItem = new JMenuItem();
  private JMenuItem _thresholdsDetailInfoMenuItem = new JMenuItem();
  
  private java.util.List<ImageSetData> _selectedImageSets = new LinkedList<ImageSetData>();
  private StatisticalTuningEngine _statisticalTuningEngine = StatisticalTuningEngine.getInstance();
  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();

  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  private Border _empty0550SpaceBorder;
  private Border _empty05010SpaceBorder;
  private JPanel _componentPinTestOuterPanel = new JPanel();
  private JPanel _boardSelectPanel = new JPanel();
  private JPanel _boardPanelTestPanel = new JPanel();
  private JPanel _testTypePanel = new JPanel();
  private JPanel _testControlPanel = new JPanel();
  private JPanel _testTypeControlPanel = new JPanel();
  private JPanel _testControlBoardPanel = new JPanel();
  private JLabel _testTypeLabel = new JLabel();
  private JLabel _panelBoardLabel = new JLabel();
  private BorderLayout _testControlBorderLayout = new BorderLayout();
  private FlowLayout _testControlBoardFlowLayout = new FlowLayout();
  private BorderLayout _testTypePanelBorderLayout = new BorderLayout();
  private JComboBox _testModeSelectComboBox = new JComboBox();
  private JButton _validateDisabledAlgoButton = new JButton();
  private JButton _viewDefectiveComponentJointButton = new JButton();
  private JButton _copyThresholdsButton = new JButton();
  private JButton _updateNominalsButton = new JButton();
  private JSplitPane _testInfoAndThresholdsSplitPane = new JSplitPane();
  private JPanel _thresholdInfoPanel = new JPanel();

  private BorderLayout _thresholdInfoPanelBorderLayout = new BorderLayout();
  private JPanel _thresholdsTypePanel = new JPanel();
  private JTabbedPane _thresholdsTabbedPane = new JTabbedPane();
  private JPanel _thresholdDescriptionPanel = new JPanel();
  private BorderLayout _thresholdsTypePanelBorderLayout = new BorderLayout();
  private JPanel _jointTypeSubtypeReferencePanel = new JPanel();
  private JPanel _standardAdvancedSelectPanel = new JPanel();
  private GridLayout _jointTypeSubtypeReferencePanelGridLayout = new GridLayout();
  private JPanel _jointTypeReferencePanel = new JPanel();
  private JPanel _subtypeReferencePanel = new JPanel();
  private JLabel _jointTypeReferenceLabel = new JLabel();
  private JLabel _jointTypeReferenceInfoLabel = new JLabel();
  private JLabel _subtypeReferenceLabel = new JLabel();
  private JLabel _subtypeReferenceInfoLabel = new JLabel();
  private Border _empty5555SpaceBorder;
  private FlowLayout _standardAdvancedSelectFlowLayout = new FlowLayout();
  private JRadioButton _advancedThresholdsRadioButton = new JRadioButton();
  private JRadioButton _standardThresholdsRadioButton = new JRadioButton();
  private BorderLayout _thresholdDescriptionPanelBorderLayout = new BorderLayout();
  private BorderLayout _thresholdDescriptionAndCommentPanelBorderLayout = new BorderLayout();
  private JPanel _thresholdDescriptionAndCommentPanel = new JPanel();
  private JPanel _thresholdCommentPanel = new JPanel();
  private JPanel _showDescriptionPanel = new JPanel();
  private JPanel _totalDescriptionPanel = new JPanel();
  private BorderLayout _thresholdCommentPanelBorderLayout = new BorderLayout();
  private JLabel _thresholdCommentLabel = new JLabel();
  private JTextField _thresholdCommentTextField = new JTextField();
  private JLabel _thresholdSimpleDescriptionLabel = new JLabel();
  private JEditorPane _thresholdSimpleDescriptionValue = new JEditorPane();
  private Border _empty105510SpaceBorder;
  private JToggleButton _showDescriptionToggleButton = new JToggleButton();
  private BorderLayout _totalDescriptionPanelBorderLayout = new BorderLayout();
  private JPanel _descriptionAndImagePanel = new JPanel();
  private JPanel _moreInfoPanel = new JPanel();
  private JButton _moreInfoButton = new JButton();
  private BorderLayout _descriptionAndImageBorderLayout = new BorderLayout();
  private JPanel _descriptionPanel = new JPanel();
  private BorderLayout _simpleDescriptionPanelBorderLayout = new BorderLayout();
  private JPanel _simpleDescriptionPanel = new JPanel();
  private JPanel _imagePanel = new JPanel();
  private BorderLayout _descriptionPanelBorderLayout = new BorderLayout();
  private FlowLayout _imagePanelFlowLayout = new FlowLayout();
  private JLabel _imageLabel = new JLabel();
  private JScrollPane _descriptionScrollPane = new JScrollPane();
  private JEditorPane _descriptionEditorPane = new JEditorPane();
  private BorderLayout _subtypeReferencePanelBorderLayout = new BorderLayout();
  private BorderLayout _jointTypeReferencePanelBorderLayout = new BorderLayout();
  private JLabel _componentLabel = new JLabel();
  private JLabel _pinLabel = new JLabel();
  private JPanel _componentPinLabelPanel = new JPanel();
  private JPanel _componentPinSelectPanel = new JPanel();
  private GridLayout _componentPinLabelPanelGridLayout = new GridLayout();
  private GridLayout _componentPinComboBoxPanelGridLayout = new GridLayout();
  private JPanel _componentPinComboBoxPanel = new JPanel();
  private BorderLayout _componentPinSelectPanelBorderLayout = new BorderLayout();
  private JComboBox _componentComboBox = new JComboBox();
  private JLabel _jointTypeInfoInfoLabel = new JLabel();
  private JLabel _subtypeInfoInfoLabel = new JLabel();
  private JLabel _jointTypeInfoLabel = new JLabel();
  private JLabel _subtypeInfoLabel = new JLabel();
  private JPanel _jointTypeSubtypeInfoLabelPanel = new JPanel();
  private GridLayout _currentJointTypeSubtypeInfoPanelGridLayout = new GridLayout();
  private JPanel _currentJointTypeSubtypeInfoPanel = new JPanel();
  private GridLayout _jointTypeSubtypeInfoLabelPanelGridLayout = new GridLayout();
  private JPanel _jointTypeSubtypeInfoPanel = new JPanel();
  private BorderLayout _jointTypeSubtypeInfoPanelBorderLayout = new BorderLayout();
  private JLabel _subtypeCommentLabel = new JLabel();
  private JTextField _subtypeCommentTextField = new JTextField();
  private JPanel _subtypeCommentPanel = new JPanel();
  private JPanel _jointTypeSubtypeComboBoxPanel = new JPanel();
  private JPanel _subtypeCommentTextFieldPanel = new JPanel();
  private JLabel _subtypeLabel = new JLabel();
  private JLabel _jointTypeLabel = new JLabel();
  private VariableWidthComboBox _jointTypeComboBox = new VariableWidthComboBox();
  private VariableWidthComboBox _subtypeComboBox = new VariableWidthComboBox();
  private JPanel _jointTypeSubtypeLabelPanel = new JPanel();
  private GridLayout _jointTypeSubtypeComboBoxPanelGridLayout = new GridLayout();
  private GridLayout _jointTypeSubtypeLabelPanelGridLayout = new GridLayout();
  private JPanel _subtypeTestPanel = new JPanel();
  private JPanel _subtypeCommentLabelPanel = new JPanel();
  private BorderLayout _subtypeTestPanelBorderLayout = new BorderLayout();
  private BorderLayout _subtypeCommentPanelBorderLayout = new BorderLayout();
  private BorderLayout _subtypeCommentLabelPanelBorderLayout = new BorderLayout();
  private BorderLayout _subtypeCommentTextFieldPanelBorderLayout = new BorderLayout();
  private BorderLayout _jointTypeSubtypePanelBorderLayout = new BorderLayout();
  private JPanel _jointTypeSubtypePanel = new JPanel();
  private JComboBox _padComboBox = new JComboBox();

  private JTabbedPane _tunerTaskPane = new JTabbedPane();

  // Tuner task panels
  private AbstractTunerTaskPanel _runInspectionPanel;
  private AbstractTunerTaskPanel _reviewDefectsPanel;
  private AbstractTunerTaskPanel _reviewMeasurementsPanel;

  // added the following for Review Defects
  private JPanel _boardTestOptionsPanel = new JPanel();
  private JCheckBox _testAllBoardsCheckBox = new JCheckBox();
  private GridLayout _boardTestOptionsPanelGridLayout = new GridLayout();
  private GridLayout _boardSelectPanelGridLayout = new GridLayout();
  private JPanel _boardInstancePanel = new JPanel();
  private JLabel _boardLabel = new JLabel();
  private JLabel _boardNumberLabel = new JLabel();
  private JPanel _boardInstanceSelectPanel = new JPanel();
  private JComboBox _boardSelectComboBox = new JComboBox();
  private BorderLayout _boardInstanceSelectPanelBorderLayout = new BorderLayout();
  private FlowLayout _boardInstancePanelFlowLayout = new FlowLayout();
  private FlowLayout _boardPanelTestPanelFlowLayout = new FlowLayout();
  private JSplitPane _thresholdDetailsAndDescriptionSplitPane = new JSplitPane();
  private JPanel _thresholdDetailsPanel = new JPanel();
  private BorderLayout _thresholdDetailsPanelBorderLayout = new BorderLayout();
  private BorderLayout _componentPinTestOuterPanelBorderLayout = new BorderLayout();
  private JCheckBox _showThresholdDescriptionCheckBox = new JCheckBox();

  private JButton _subtypeAdvanceSettingsButton; //added by Chin Seong
  private AbstractTunerTaskPanel _testStartedPanel; //added by Ngie Xing
  private final String _SWITCH_TO_INSPECTION_PANEL_KEY = "switchToInspectionPanel";
  private final String _SWITCH_TO_DEFECTS_PANEL_KEY = "switchToDefectsPanel";
  private final String _SWITCH_TO_MEASUREMENT_PANEL_KEY = "switchToMeasurementPanel";
  
  //Siew Yeng - XCR-2764 - sub-subtype feature
  private static String _PARENT_SUBTYPE_TEXT = StringLocalizer.keyToString("AGUI_PARENT_SUBTYPE_LABEL_KEY");
  
  private JButton _addOrRemoveSubRegionButton = new JButton();
  private JComboBox _subRegionComboBox = new JComboBox();
  private ActionListener _subRegionComboBoxActionListener;
  private int _currentSelectedSubRegion;

  /**
   * For use with JBuilder's designer only
   * @author Andy Mechtenberg
   */
  private AlgoTunerGui(boolean designer)
  {
    jbInit();
  }

  /**
   * Constructor.
   *
   * @author Andy Mechtenberg
   */
  public AlgoTunerGui()
  {
    // do nothing
    
    // When the Project previous has saved checked image set, this observer will be and triggered the button enable
//    _commandManager.addObserver(this);
   // Swee Yee Wong - remove addobserver in constructor
   // _guiObservable.addObserver(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  void handleHardwareError(HardwareException he)
  {
    MessageDialog.showErrorDialog(_mainUI,
                                  he.getLocalizedMessage(),
                                  StringLocalizer.keyToString("ATGUI_GUI_NAME_KEY"),
                                  true);
    _inRunMode = false;
    changeFrontPanelControls(_inRunMode);
    _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_HARDWARE_ERROR_KEY"));
  }

  /**
   * @author Andy Mechtenberg
   */
  void handleDatastoreError(DatastoreException de)
  {
    MessageDialog.showErrorDialog(_mainUI,
                                  de.getLocalizedMessage(),
                                  StringLocalizer.keyToString("ATGUI_GUI_NAME_KEY"),
                                  true);
    _inRunMode = false;
    changeFrontPanelControls(_inRunMode);
    _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_HARDWARE_ERROR_KEY"));
  }

  /**
   * @author Bill Darbie
   */
  void handleBusinessError(BusinessException be)
  {
    MessageDialog.showErrorDialog(_mainUI,
                                  be.getLocalizedMessage(),
                                  StringLocalizer.keyToString("ATGUI_GUI_NAME_KEY"),
                                  true);
    _inRunMode = false;
    changeFrontPanelControls(_inRunMode);
    if (be instanceof NoAlignmentBusinessException)
      _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_NO_ALIGNMENT_KEY"));
    else if (be instanceof NoSurfaceMapBusinessException)
      _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_NO_SURFACE_MAP_KEY"));
    else
      _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_ERROR_KEY"));
  }

  /**
   * This method should be called to display all error message that might occur.
   * @author Erica Wheatcroft
   */
  void displayError(XrayTesterException xte)
  {
    MessageDialog.showErrorDialog(_mainUI,
                                  xte.getLocalizedMessage(),
                                  StringLocalizer.keyToString("ATGUI_GUI_NAME_KEY"),
                                  true);
    _inRunMode = false;
    changeFrontPanelControls(_inRunMode);
    if (xte instanceof NoAlignmentBusinessException)
      _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_NO_ALIGNMENT_KEY"));
    else if (xte instanceof NoSurfaceMapBusinessException)
      _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_NO_SURFACE_MAP_KEY"));
    else
      _mainUI.setStatusBarText(StringLocalizer.keyToString("ATGUI_STATUS_ERROR_KEY"));
  }

  /**
   * The Tuner is a listener to UNDO (for changing thresholds)
   * @author Andy Mechtenberg
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
//        if (observable instanceof SelectedRendererObservable)
//          handleSelectionEvent(object);
        if (observable instanceof GuiObservable)
        {
          GuiEvent guiEvent = (GuiEvent)object;
          GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
          if (guiEventEnum instanceof GuiUpdateEventEnum)
          {
            GuiUpdateEventEnum guiUpdateEventEnum = (GuiUpdateEventEnum)guiEventEnum;
            if (guiUpdateEventEnum.equals(GuiUpdateEventEnum.TUNER_INSPECTION_START))
            {
              _inRunMode = true;
              changeFrontPanelControls(true);
            }
            else if (guiUpdateEventEnum.equals(GuiUpdateEventEnum.TUNER_INSPECTION_FINISH))
            {
              _inRunMode = false;
              changeFrontPanelControls(false);
            }
            //XCR-2029, Applicable Start Test Shortcut Key for Whole Fine Tuning Page, Ngie Xing
            else if (guiUpdateEventEnum.equals(GuiUpdateEventEnum.TUNER_INSPECTION_START_FROM_OTHER_PANEL))
            {
              _testStartedPanel = (AbstractTunerTaskPanel) guiEvent.getSource();
              _tunerTaskPane.setSelectedComponent(_runInspectionPanel);
              RunInspectionPanel runInspectionPanel = (RunInspectionPanel) _tunerTaskPane.getSelectedComponent();
              runInspectionPanel.startTest_actionPerformed(true);
            }
            ////XCR-2029, Ngie Xing add
            else if (guiUpdateEventEnum.equals(GuiUpdateEventEnum.TUNER_INSPECTION_FINISHED_FROM_OTHER_PANEL))
            {
              if (_testStartedPanel != null && _testStartedPanel != _runInspectionPanel)
              {
                _tunerTaskPane.setSelectedComponent(_testStartedPanel);
              }
              _testStartedPanel = null;
            }
          }
          else if (guiEventEnum instanceof ImageSetEventEnum)
          {
            ImageSetEventEnum imageSetEventEnum = (ImageSetEventEnum)guiEventEnum;
            if (imageSetEventEnum.equals(ImageSetEventEnum.IMAGE_SET_NOT_SELECTED))
            {
              _selectedImageSets.clear();
              updateToolBarButtonState();
            }
            if (imageSetEventEnum.equals(ImageSetEventEnum.IMAGE_SET_SELECTED))
            {
              Assert.expect(guiEvent.getSource() instanceof java.util.List);
              _selectedImageSets = (java.util.List<ImageSetData>)guiEvent.getSource();
              updateToolBarButtonState();
            }
          }
          else if (guiEventEnum instanceof SelectionEventEnum)
          {
            SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
            if (selectionEventEnum.equals(SelectionEventEnum.SUBTYPE_SELECTION))
            {
              TunerTestState tunerTestState = (TunerTestState)guiEvent.getSource();
              setSubtypeSelection(tunerTestState);
            }
            //Ngie Xing, April 2014
            //added to highlight/select thresholds table when a Defect detail in ReviewDefectsPanel is selected
            else if (selectionEventEnum.equals(SelectionEventEnum.DEFECT_DETAILS_SELECTION))
            {
              Pair<AlgorithmEnum, MeasurementResult> pair = (Pair<AlgorithmEnum, MeasurementResult>) guiEvent.getSource();
              AlgorithmEnum algorithmEnum = pair.getFirst();
              MeasurementResult selectedMeasurementResult = pair.getSecond();

              SubtypeChoice subtypeChoice = (SubtypeChoice) _subtypeComboBox.getSelectedItem();
              //Lim, Lay Ngor - temporary handling short - Joint==All so that won't crashed.
              //NX - this is to handle components with mixed JointType and/or Subtype
              if(subtypeChoice.isAllSubtypes())//do nothing
                return;
              Subtype selectedSubtype = subtypeChoice.getSubtype();
              JointTypeChoice jointTypeChoice = (JointTypeChoice) _jointTypeComboBox.getSelectedItem();

              InspectionFamily inspectionFamily = selectedSubtype.getInspectionFamily();
              java.util.List<AlgorithmSetting> algorithmSettings = null;
              AlgorithmSetting selectedAlgorithmSetting = null;

              Algorithm algorithm = InspectionFamily.getAlgorithm(inspectionFamily.getInspectionFamilyEnum(), algorithmEnum);
              
              //Short Algorithm must be handled differently
              //This might not be enough to pinpoint all algorithms accurately, needs improvement in the future
              if (algorithmEnum == AlgorithmEnum.SHORT)
              {
                algorithmSettings = algorithm.getAlgorithmSettingsExcludingSliceHeight(jointTypeChoice.getJointTypeEnum());

                for (AlgorithmSetting algorithmSetting : algorithmSettings)
                {
                  if (algorithmSetting.getName().contains(selectedMeasurementResult.getMeasurementEnum().getName()))
                  {
                    selectedAlgorithmSetting = algorithmSetting;
                    break;
                  }
                }
              }
              else
              {
                //this line will not get any algorithm settings for SHORT
                algorithmSettings = inspectionFamily.getAssociatedAlgorithmSettings(jointTypeChoice.getJointTypeEnum(),
                  selectedMeasurementResult.getSliceNameEnum(), selectedMeasurementResult.getMeasurementEnum());

                //There are 2 Algorithm Setting in with the same SliceNameEnum and MeasurementEnum from different Algorithm
                //we only want the Algorithm Setting from the same Algorithm selected from the list
                int numberOfAssociatedAlgorithmSettings = algorithmSettings.size();
                for (int i = numberOfAssociatedAlgorithmSettings - 1; i > -1; i--)
                {
                  if (algorithm.getAlgorithmSettings(jointTypeChoice.getJointTypeEnum()).contains(algorithmSettings.get(i)) == false)
                  {
                    algorithmSettings.remove(algorithmSettings.get(i));
                  }
                }
//              Assert.expect(algorithmSettings.size() != 0);

                //if there are 2 in the list of AlgorithmSetting, defect call could fail at either min or max or even both
                //check wether the Defect Call comes from min or max
                if (algorithmSettings.size() > 1)
                {
                  //Assert.expect(algorithmSettings.size() == 2);
                  for (AlgorithmSetting algorithmSetting : algorithmSettings)
                  {
                    if (algorithmSetting.getName().toLowerCase().contains("min"))
                    {
                      float minValue = (float) selectedSubtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum());
                      if (selectedMeasurementResult.getValue() < minValue)
                      {
                        selectedAlgorithmSetting = algorithmSetting;
                      }
                    }
                    else if (algorithmSetting.getName().toLowerCase().contains("max"))
                    {
                      float maxValue = (Float) selectedSubtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum());
                      if (selectedMeasurementResult.getValue() > maxValue)
                      {
                        selectedAlgorithmSetting = algorithmSetting;
                      }
                    }
                  }
                }
                else if (algorithmSettings.size() == 1)
                {
                  selectedAlgorithmSetting = algorithmSettings.get(0);
                }

                //for the case where algorithm settings have the same slice and measurement, 
                //but does not have minimum and maximum for their names
                //check whether contains both slice and measurement
                if (selectedAlgorithmSetting == null)
                {
                  for (AlgorithmSetting algorithmSetting : algorithmSettings)
                  {
                    if (algorithmSetting.getName().toLowerCase().contains(selectedMeasurementResult.getSliceNameEnum().getName())
                      && algorithmSetting.getName().toLowerCase().contains(selectedMeasurementResult.getMeasurementEnum().getName()))
                    {
                      selectedAlgorithmSetting = algorithmSetting;
                    }
                  }
                }
              }
              
              //if still could not find algorithm setting, do nothing in order to avoid crash
              if (selectedAlgorithmSetting == null)
              {
                for (int i = 0; i < _thresholdsTabbedPane.getTabCount(); i++)
                {
                  if (_thresholdsTabbedPane.getTitleAt(i).equals(algorithmEnum.getName()))
                  {
                    _thresholdsTabbedPane.setSelectedIndex(i);
                  }
                }
                return;
              }
              //Assert.expect(selectedAlgorithmSetting != null);
              
              //check if algorithm setting type is standard or additional
              if (selectedAlgorithmSetting.getAlgorithmSettingTypeEnum(algorithm.getVersion()).equals(AlgorithmSettingTypeEnum.STANDARD)
                && (_standardThresholdsRadioButton.isSelected() == false))
              {
                _standardThresholdsRadioButton.doClick();
              }
              if (selectedAlgorithmSetting.getAlgorithmSettingTypeEnum(algorithm.getVersion()).equals(AlgorithmSettingTypeEnum.ADDITIONAL)
                && (_advancedThresholdsRadioButton.isSelected() == false))
              {
                _advancedThresholdsRadioButton.doClick();
              }
          
              //finally show the selected algorithm setting in the thresholds table
              boolean finished = false;
              for (int i = 0; i < _thresholdsTabbedPane.getTabCount() && finished == false; i++)
              {
                if (_thresholdsTabbedPane.getTitleAt(i).equals(algorithmEnum.getName()))//selectedMeasurementResult.getAlgorithmEnum().getName()))
                {
                  _thresholdsTabbedPane.setSelectedIndex(i);
                  JTable table = findCurrentTable();
                  ThresholdTableModel tableModel = (ThresholdTableModel) table.getModel();
                  for (int j = 0; j < tableModel.getRowCount(); j++)
                  {
                    String algorithmSettingFromTable = tableModel.getValueAt(j, ThresholdTableModel.NAME_COLUMN).toString();
                    if (algorithmSettingFromTable.equals(selectedAlgorithmSetting.getAlgorithmSettingEnum().getName()))
                    {
                      table.setRowSelectionInterval(j, j);
                      finished = true;
                      break;
                    }
                  }
                }
              }
            }
          }

        }
        else if (observable instanceof com.axi.v810.gui.undo.CommandManager)
        {
          UndoState stateObject = (UndoState)object;
          int testMode = stateObject.getTestModeSelection();
          _testModeSelectComboBox.setSelectedIndex(testMode);

          // We have check to make sure all components/pins/joint type/subtypes still exist in the data
          // Even though the panel name and board name haven't changed, the data underneath might.  If
          // we don't check, then we'll start throwing asserts if a component was deleted, for example.

          // we have to setup component/pin first, because this may change the joint type/subtype setting
          String persistedComponent = stateObject.getTunerComponent();
          String persistedPad = stateObject.getTunerPad();
          int count = _componentComboBox.getItemCount();
          if (count > 0)
            _componentComboBox.setSelectedIndex(0);
          for (int i = 0; i < count; i++)
          {
            ComponentType compType = (ComponentType)_componentComboBox.getItemAt(i);
            if (persistedComponent.equals(compType.toString()))
            {
              _componentComboBox.setSelectedIndex(i);
              break;
            }
          }
          count = _padComboBox.getItemCount();
          if (count > 0)
            _padComboBox.setSelectedIndex(0);
          for (int i = 0; i < count; i++)
          {
            PadChoice padChoice = (PadChoice)_padComboBox.getItemAt(i);
            if (persistedPad.equals(padChoice.toString()))
            {
              _padComboBox.setSelectedIndex(i);
              break;
            }
          }
          // now joint type/ subtype may be re-set to the persistant values
          if (testMode != 1) // 1 is component/pin.  Setting comp/pin will automatically set joint type/ subtype
          {
            _jointTypeComboBox.setSelectedIndex(0);
            if(stateObject.hasTunerJointType())
            {
              String persistJointTypeString = stateObject.getTunerJointType();
              count = _jointTypeComboBox.getItemCount();
              for (int i = 0; i < count; i++)
              {
                JointTypeChoice jtc = (JointTypeChoice)_jointTypeComboBox.getItemAt(i);
                if (persistJointTypeString.equals(jtc.toString()))
                {
                  _jointTypeComboBox.setSelectedIndex(i);
                  break;
                }
              }
              
              String persistSubtypeString = stateObject.getTunerSubtype();

              // now, do the same for the subtype combo box
              _subtypeComboBox.setSelectedIndex(0);
              count = _subtypeComboBox.getItemCount();
              for (int i = 0; i < count; i++)
              {
                SubtypeChoice caf = (SubtypeChoice)_subtypeComboBox.getItemAt(i);
                if (persistSubtypeString.equals(caf.toString()))
                {
                  _subtypeComboBox.setSelectedIndex(i);
                  break;
                }
              }
            }
          }
          else
            setInfoJointTypeSubtype(); // This is based on component/pin, which we just set

          // ok, now all the threshold tables should be visible.  Time to set the algo and radio button
          if (stateObject.getTunerBasicThresholds())
          {
            _standardThresholdsRadioButton.doClick();
          }
          else
            _advancedThresholdsRadioButton.doClick();
          _thresholdsTabbedPane.setSelectedIndex(stateObject.getTunerThresholdTabIndex());
          Component comp = _thresholdsTabbedPane.getSelectedComponent();
          Assert.expect(comp instanceof JPanel);
          JPanel compPanel = (JPanel)comp;
          Component[] componentArray = compPanel.getComponents();
          for (int compIndex = 0; compIndex < componentArray.length; compIndex++)
          {
            Component potentialComp = componentArray[compIndex];
            if (potentialComp instanceof JScrollPane)
            {
              JScrollPane jsp = (JScrollPane)potentialComp;
              Component comp1 = (JTable)jsp.getViewport().getView();
              if (comp1 == null)
                return;
              else
              {
                JTable tt = (JTable)comp1;
                int thresholdRow = stateObject.getTunerThresholdRow();
                if (thresholdRow < 0)
                  return;
                tt.setRowSelectionInterval(thresholdRow, thresholdRow);
                TableCellEditor tce = tt.getCellEditor();
                if (tce != null)
                  tce.cancelCellEditing();
                tt.requestFocus();
              }
            }
          }
        }
        else
          Assert.expect(false);
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  void setSubtypeSelection(TunerTestState tunerTestState)
  {
//    System.out.println("\nsetCurrentTestSelection()");
//    _sendEvents--;
    int index = _testModeSelectComboBox.getSelectedIndex();
    if (index == 0) // jointType/subtype
    {
      if (tunerTestState.isJointTypeTest())
      {
        JointTypeChoice jointTypeChoice = tunerTestState.getJointTypeChoice();
        SubtypeChoice subtypeChoice = tunerTestState.getSubtypeChoice();
        _jointTypeComboBox.setSelectedItem(jointTypeChoice);
        _subtypeComboBox.setSelectedItem(subtypeChoice);
      }
    }
//    _sendEvents++;
  }


//  /**
//   * @author Andy Mechtenberg
//   */
//  private void handleSelectionEvent(Object object)
//  {
//    // if we are disabled, then no selections should be listened to
//    if (_componentComboBox.isEnabled() == false)
//      return;
//
//    // ok.  We got notice that something was selected.
//    Collection selections = (Collection)object;
//    Iterator it = selections.iterator();
//    ComponentType selectedComponentType = null;
//    PadType selectedPadType = null;
//    while (it.hasNext())
//    {
//      com.axi.guiUtil.Renderer renderer = (com.axi.guiUtil.Renderer)it.next();
//      if (renderer instanceof PadRenderer)
//      {
//        PadRenderer padRenderer = (PadRenderer)renderer;
//        selectedPadType = padRenderer.getPad().getPadType();
//        selectedComponentType = selectedPadType.getComponentType();
//        break;
//      }
//    }
//
//    if (selectedPadType == null)
//    {
//      it = selections.iterator();
//      while (it.hasNext())
//      {
//        com.axi.guiUtil.Renderer renderer = (com.axi.guiUtil.Renderer)it.next();
//        // viewing component mode -- pay attention to ComponentRenderers and PadRenderers
//        // get the component(s) selected (if any) and highlight the row in the table cooresponding to that component
//        // if a pad was also selected, highlight the pad in the details table
//        if (renderer instanceof ComponentRenderer)
//        {
//          ComponentRenderer compRenderer = (ComponentRenderer)renderer;
//          selectedComponentType = compRenderer.getComponent().getComponentType();
//          break;
//        }
//      }
//    }
//    // if the renderer selected was not a component or a pad, we really don't care
//    // and can exit this method at this point.
//    if ((selectedComponentType == null) && (selectedPadType == null))
//    {
//      return;
//    }
//
//    // at this point we have either a component or a component and pad
//    if (selectedPadType != null)
//    {
//      _componentComboBox.setSelectedItem(selectedComponentType);
//      _padComboBox.setSelectedItem(selectedPadType);
//    }
//    else
//    {
//      _componentComboBox.setSelectedItem(selectedComponentType);
//    }
//
//  }

  /**
   * Component initialization
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    // do this right away or there is trouble accessing keyboard menu shortcuts
    JPopupMenu.setDefaultLightWeightPopupEnabled(false);
    int vgap = 5;

    _jointTypeComboBoxActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jointTypeComboBox_actionPerformed(e);
      }
    };

    _tunerTaskChangeListener = new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        tunerTaskPane_stateChanged(e);
      }
    };
    _panelDataAdapter = new PanelDataAdapter(this);
    _panelInspectionAdapter = new PanelInspectionAdapter(this);

    _empty5555SpaceBorder = BorderFactory.createEmptyBorder(5,5,5,5);
    _empty105510SpaceBorder = BorderFactory.createEmptyBorder(5,10,5,10);

    // border creation
    _empty05010SpaceBorder = BorderFactory.createEmptyBorder(0,5,5,10);
    _empty0550SpaceBorder = BorderFactory.createEmptyBorder(0,5,5,0);

    // load images right away
    _copyThresholdsImage = Image5DX.getImageIcon(Image5DX.AT_COPY_THRESHOLDS);
    _updateNominalsImage = Image5DX.getImageIcon(Image5DX.CT_FULL_IMAGE);
    _validateDisabledAlgoImage = Image5DX.getImageIcon(Image5DX.AT_VALIDATE_DISABLED_ALGO);
    _viewMeasurementJointImage = Image5DX.getImageIcon(Image5DX.AT_VIEW_MEASUREMENT_JOINT);
    createMenus();
    // this is the upper left icon
//    _frame.setIconImage( Image5DX.getImage(Image5DX.FRAME_5DX) );
    _centerPanel.setLayout(_centerPanelBorderLayout);
    _componentPinTestOuterPanel.setBorder(_empty0550SpaceBorder);
    _componentPinTestOuterPanel.setLayout(_componentPinTestOuterPanelBorderLayout);
    _boardSelectPanel.setLayout(_boardSelectPanelGridLayout);
    _boardPanelTestPanel.setLayout(_boardPanelTestPanelFlowLayout);

    _testTypePanel.setLayout(_testTypePanelBorderLayout);
    _boardPanelTestPanel.setPreferredSize(new Dimension(237, 72));
    // this combobox is going in a tool bar, and I need to control the height of that box very carefully so the toolbar
    // doesn't grow - this causes a jitter when switching into or out of the tuner panel.
    // So, to make sure the length is controlled by the contents (for localization), I'll calculate the max width based on the
    // strings I'm putting in, and set the height to 21 pixels.
    int maxStringLength = 0;
    FontMetrics fm = _testModeSelectComboBox.getFontMetrics(_testModeSelectComboBox.getFont());
    String[] items = {StringLocalizer.keyToString("ATGUI_FAMILY_KEY") + "/" + StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY"),
                      StringLocalizer.keyToString("ATGUI_COMPONENT_KEY") + "/" + StringLocalizer.keyToString("ATGUI_PIN_KEY")};
//                      StringLocalizer.keyToString("ATGUI_BOARD_KEY") + "/" + StringLocalizer.keyToString("ATGUI_PANEL_KEY")};
    for(int i = 0; i < items.length; i++)
    {
      _testModeSelectComboBox.addItem(items[i]); // add the item to the combo box here
      int length = fm.stringWidth(items[i]);
      if (length > maxStringLength) // update max length
        maxStringLength = length;
    }
//    _testModeSelectComboBox.setPreferredSize(new Dimension(maxStringLength + 30, CommonUtil._COMBO_BOX_HEIGHT));  // the +30 is to make room for the dropdown control
//    _testModeSelectComboBox.setMaximumSize(_testModeSelectComboBox.getPreferredSize());
    _testModeSelectComboBox.setToolTipText(StringLocalizer.keyToString("ATGUI_TEST_MODE_TOOLTIP_KEY"));
    _testModeSelectComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        testModeSelectComboBox_actionPerformed(e);
      }
    });

    _validateDisabledAlgoButton.setToolTipText(StringLocalizer.keyToString("ATGUI_VALIDATE_DISABLED_ALGO_TOOLTIP_KEY"));
    _validateDisabledAlgoButton.setIcon(_validateDisabledAlgoImage);
    _validateDisabledAlgoButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        validateDisabledAlgoButton_actionPerformed();
      }
    });
    
    _viewDefectiveComponentJointButton.setToolTipText(StringLocalizer.keyToString("ATGUI_VIEW_MEASUREMENT_JOINT_HIGLIGHT_SETTING_KEY"));
    _viewDefectiveComponentJointButton.setIcon(_viewMeasurementJointImage);
    _viewDefectiveComponentJointButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        viewMeasurementJointHighlightButton_actinPerformed();
      }
    });
    
      //apm for TDT
  //    _copyThresholdsButton.setPreferredSize(new Dimension(50, 31));
    _copyThresholdsButton.setToolTipText(StringLocalizer.keyToString("ATGUI_COPY_THRESHOLDS_TOOLTIP_KEY"));
    _copyThresholdsButton.setIcon(_copyThresholdsImage);
    _copyThresholdsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        copyThresholdsButton_actionPerformed();
      }
    });
    //apm for TDT
//    _copyThresholdsButton.setMaximumSize(_copyThresholdsButton.getPreferredSize());

    _updateNominalsButton.setToolTipText(StringLocalizer.keyToString("ATGUI_UPDATE_NOMINALS_TOOLTIP_KEY"));
    _updateNominalsButton.setIcon(_updateNominalsImage);
    _updateNominalsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        updateNominalsButton_actionPerformed();
      }
    });

    _testInfoAndThresholdsSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);

    _thresholdInfoPanel.setLayout(_thresholdInfoPanelBorderLayout);
    _thresholdsTypePanel.setLayout(_thresholdsTypePanelBorderLayout);
    _jointTypeSubtypeReferencePanel.setLayout(_jointTypeSubtypeReferencePanelGridLayout);
    _jointTypeSubtypeReferencePanelGridLayout.setRows(2);
    _jointTypeSubtypeReferencePanelGridLayout.setVgap(5);
    _jointTypeReferenceLabel.setFont(FontUtil.getBoldFont(_jointTypeReferenceLabel.getFont(),Localization.getLocale()));
    _jointTypeReferenceLabel.setText(StringLocalizer.keyToString("ATGUI_ALL_KEY"));
    _jointTypeReferenceInfoLabel.setText(StringLocalizer.keyToString("ATGUI_FAMILY_KEY") + ":");
    _subtypeReferenceLabel.setFont(FontUtil.getBoldFont(_subtypeReferenceLabel.getFont(),Localization.getLocale()));
    _subtypeReferenceLabel.setText(StringLocalizer.keyToString("ATGUI_ALL_KEY"));
    _subtypeReferenceInfoLabel.setText(StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY") + ":");
    // set one of these labels to the preferred size of the other.  To do that, we need to know which is bigger.
    Dimension jointTypeDimension = _jointTypeReferenceInfoLabel.getPreferredSize();
    Dimension subtypeDimension = _subtypeReferenceInfoLabel.getPreferredSize();
    if (jointTypeDimension.width > subtypeDimension.width)
      _subtypeReferenceInfoLabel.setPreferredSize(_jointTypeReferenceInfoLabel.getPreferredSize());
    else
      _jointTypeReferenceInfoLabel.setPreferredSize(_subtypeReferenceInfoLabel.getPreferredSize());

    _jointTypeReferencePanel.setLayout(_jointTypeReferencePanelBorderLayout);
    _subtypeReferencePanel.setLayout(_subtypeReferencePanelBorderLayout);
    _thresholdsTypePanel.setBorder(_empty5555SpaceBorder);
    _standardAdvancedSelectPanel.setLayout(_standardAdvancedSelectFlowLayout);
    _standardAdvancedSelectFlowLayout.setAlignment(FlowLayout.LEFT);
    _advancedThresholdsRadioButton.setText(StringLocalizer.keyToString("ATGUI_ADDITIONAL_THRESHOLDS_KEY"));
    _advancedThresholdsRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        advancedThresholdsRadioButton_actionPerformed(e);
      }
    });
    _standardThresholdsRadioButton.setText(StringLocalizer.keyToString("ATGUI_BASIC_THRESHOLDS_KEY"));
    _standardThresholdsRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        standardThresholdsRadioButton_actionPerformed(e);
      }
    });
    //XCR-1169 Threshold Description
    _showThresholdDescriptionCheckBox.setText(StringLocalizer.keyToString("ATGUI_SHOW_DESCRIPTION_BUTTON_KEY"));
    _showThresholdDescriptionCheckBox.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        if (_showThresholdDescriptionCheckBox.isSelected())
        {
          //_simpleDescriptionPanel.setVisible(true);
          _thresholdDescriptionAndCommentPanel.setVisible(true);
          _thresholdDescriptionAndCommentPanel.updateUI();
        }
        else
        {
          //_simpleDescriptionPanel.setVisible(false);
          _thresholdDescriptionAndCommentPanel.setVisible(false);
          _thresholdDescriptionAndCommentPanel.updateUI();
        }
      }
    });

    _jointTypeSubtypeReferencePanel.setPreferredSize(new Dimension(200, 39));
    _thresholdDescriptionPanel.setLayout(_thresholdDescriptionPanelBorderLayout);
    _thresholdCommentPanel.setLayout(_thresholdCommentPanelBorderLayout);
    _thresholdCommentLabel.setText(StringLocalizer.keyToString("ATGUI_THRESHOLD_COMMENT_LABEL_KEY") + ":");
    //XCR-1169 Threshold Desc
    _thresholdSimpleDescriptionLabel.setText(StringLocalizer.keyToString("ATGUI_THRESHOLD_DESCRIPTION_LABEL_KEY") + ":  ");
    _thresholdCommentPanel.setBorder(_empty105510SpaceBorder);
    _thresholdCommentPanelBorderLayout.setHgap(5);
    _showDescriptionToggleButton.setText(StringLocalizer.keyToString("ATGUI_SHOW_DESCRIPTION_BUTTON_KEY"));
    _showDescriptionToggleButton.setSelected(true);
    _showDescriptionToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showDescriptionToggleButton_actionPerformed();
      }
    });
    _totalDescriptionPanel.setLayout(_totalDescriptionPanelBorderLayout);
    _moreInfoButton.setText(StringLocalizer.keyToString("ATGUI_MORE_INFO_BUTTON_KEY"));
    _moreInfoButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        moreInfoButton_actionPerformed(e);
      }
    });
    _descriptionAndImagePanel.setBorder(BorderFactory.createLoweredBevelBorder());
    _descriptionAndImagePanel.setLayout(_descriptionAndImageBorderLayout);
    _descriptionPanel.setLayout(_descriptionPanelBorderLayout);
    //XCR-1169 Threshold Description
    _simpleDescriptionPanel.setLayout(_simpleDescriptionPanelBorderLayout);
    _simpleDescriptionPanel.setBorder(_empty105510SpaceBorder);
    _simpleDescriptionPanel.setVisible(true);

    _imagePanel.setLayout(_imagePanelFlowLayout);
    _imagePanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _imagePanelFlowLayout.setHgap(0);
    _imagePanelFlowLayout.setVgap(0);
    _descriptionEditorPane.setContentType("text/html");
    _descriptionEditorPane.setBackground(SystemColor.info);
    _descriptionEditorPane.setEditable(false);
    _imagePanel.setBackground(SystemColor.info);
    _descriptionScrollPane.setBorder(null);
    _jointTypeReferencePanelBorderLayout.setHgap(5);
    _subtypeReferencePanelBorderLayout.setHgap(5);
    _thresholdsTabbedPane.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        thresholdsTabbedPane_stateChanged(e);
      }
    });
    _thresholdCommentTextField.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyTyped(KeyEvent ke)
      {
        updateThresholdComment();
      }
      public void keyPressed(KeyEvent ke)
      {
        updateThresholdComment();
      }
      public void keyReleased(KeyEvent ke)
      {
        updateThresholdComment();
      }
    });
    _descriptionPanel.setBackground(SystemColor.info);
    _imageLabel.setBorder(BorderFactory.createLineBorder(Color.black));
    _componentLabel.setText(StringLocalizer.keyToString("ATGUI_COMPONENT_KEY") + ":");
    _componentPinLabelPanel.setLayout(_componentPinLabelPanelGridLayout);
    _componentPinSelectPanel.setLayout(_componentPinSelectPanelBorderLayout);
    //_pinLabel.setPreferredSize(new Dimension(68, 17));
    _pinLabel.setText(StringLocalizer.keyToString("ATGUI_PIN_KEY") + ":");
    _componentPinLabelPanelGridLayout.setColumns(1);
    _componentPinLabelPanelGridLayout.setRows(2);
    _componentPinLabelPanelGridLayout.setVgap(vgap);
    _componentPinComboBoxPanelGridLayout.setColumns(1);
    _componentPinComboBoxPanelGridLayout.setRows(2);
    _componentPinComboBoxPanelGridLayout.setVgap(vgap);
    _componentPinComboBoxPanel.setLayout(_componentPinComboBoxPanelGridLayout);
    _componentPinSelectPanelBorderLayout.setHgap(5);
    _componentComboBox.setPreferredSize(new Dimension(150, CommonUtil._COMBO_BOX_HEIGHT));
    _componentComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        componentComboBox_actionPerformed(e);
      }
    });
    _componentComboBox.setEditable(true);
//    _jointTypeInfoInfoLabel.setPreferredSize(new Dimension(47, 17));
    _jointTypeInfoInfoLabel.setText(StringLocalizer.keyToString("ATGUI_FAMILY_KEY") + ":");
    _subtypeInfoInfoLabel.setText(StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY") + ":");
    _jointTypeInfoLabel.setFont(FontUtil.getBoldFont(_jointTypeInfoLabel.getFont(),Localization.getLocale()));
    _subtypeInfoLabel.setFont(FontUtil.getBoldFont(_subtypeInfoLabel.getFont(),Localization.getLocale()));
    _jointTypeSubtypeInfoLabelPanel.setLayout(_jointTypeSubtypeInfoLabelPanelGridLayout);
    _currentJointTypeSubtypeInfoPanelGridLayout.setColumns(1);
    _currentJointTypeSubtypeInfoPanelGridLayout.setRows(2);
    _currentJointTypeSubtypeInfoPanelGridLayout.setVgap(vgap);
    _currentJointTypeSubtypeInfoPanel.setLayout(_currentJointTypeSubtypeInfoPanelGridLayout);
    _jointTypeSubtypeInfoLabelPanelGridLayout.setColumns(1);
    _jointTypeSubtypeInfoLabelPanelGridLayout.setRows(2);
    _jointTypeSubtypeInfoLabelPanelGridLayout.setVgap(vgap);
    _jointTypeSubtypeInfoPanel.setLayout(_jointTypeSubtypeInfoPanelBorderLayout);
    _jointTypeSubtypeInfoPanelBorderLayout.setHgap(10);
    _subtypeCommentLabel.setText(StringLocalizer.keyToString("ATGUI_SUBTYPE_COMMENT_KEY") + ":");
    _subtypeCommentLabel.setVerticalAlignment(SwingConstants.BOTTOM);
    _subtypeCommentLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
//    _subtypeCommentTextField.setPreferredSize(new Dimension(100, 21));
    _subtypeCommentPanel.setLayout(_subtypeCommentPanelBorderLayout);
    _jointTypeSubtypeComboBoxPanel.setLayout(_jointTypeSubtypeComboBoxPanelGridLayout);
    _subtypeCommentTextFieldPanel.setLayout(_subtypeCommentTextFieldPanelBorderLayout);
    _subtypeLabel.setText(StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY") + ":");
    _jointTypeLabel.setText(StringLocalizer.keyToString("ATGUI_FAMILY_KEY") + ":");

    _subtypeComboBox.setPreferredSize(new Dimension(150, CommonUtil._COMBO_BOX_HEIGHT));
    _jointTypeComboBox.setPreferredSize(new Dimension(150, CommonUtil._COMBO_BOX_HEIGHT));

    _subtypeComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        subtypeComboBox_actionPerformed(e);
      }
    });
    _jointTypeSubtypeLabelPanel.setLayout(_jointTypeSubtypeLabelPanelGridLayout);
    _jointTypeSubtypeComboBoxPanelGridLayout.setColumns(1);
    _jointTypeSubtypeComboBoxPanelGridLayout.setRows(2);
    _jointTypeSubtypeComboBoxPanelGridLayout.setVgap(vgap);
    _jointTypeSubtypeLabelPanelGridLayout.setColumns(1);
    _jointTypeSubtypeLabelPanelGridLayout.setRows(2);
    _jointTypeSubtypeLabelPanelGridLayout.setVgap(vgap);
    _subtypeTestPanel.setLayout(_subtypeTestPanelBorderLayout);
    _subtypeTestPanel.setBorder(_empty05010SpaceBorder);
//    _subtypeTestPanel.setPreferredSize(new Dimension(237, 72));
    _subtypeCommentLabelPanel.setLayout(_subtypeCommentLabelPanelBorderLayout);
    _subtypeTestPanelBorderLayout.setHgap(10);
    _subtypeCommentPanelBorderLayout.setHgap(10);
    _jointTypeSubtypePanelBorderLayout.setHgap(5);
    _jointTypeSubtypePanel.setLayout(_jointTypeSubtypePanelBorderLayout);
    _padComboBox.setPreferredSize(new Dimension(100, CommonUtil._COMBO_BOX_HEIGHT));
    _padComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        padComboBox_actionPerformed(e);
      }
    });

    _testAllBoardsCheckBox.setText(StringLocalizer.keyToString("ATGUI_TEST_ALL_BOARDS_KEY"));
    _boardTestOptionsPanel.setLayout(_boardTestOptionsPanelGridLayout);
    _boardTestOptionsPanelGridLayout.setColumns(1);
    _boardTestOptionsPanelGridLayout.setRows(2);
    _boardTestOptionsPanelGridLayout.setVgap(10);
    _boardSelectPanelGridLayout.setColumns(1);
    _boardSelectPanelGridLayout.setRows(2);
    _boardSelectPanelGridLayout.setVgap(10);
    _boardLabel.setText(StringLocalizer.keyToString("ATGUI_BOARD_KEY") + ":");
    _boardNumberLabel.setText("1  of  2");  // temporary to let the pack work
    _boardInstanceSelectPanel.setLayout(_boardInstanceSelectPanelBorderLayout);
    _boardInstancePanel.setLayout(_boardInstancePanelFlowLayout);
    _boardInstancePanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _boardInstancePanelFlowLayout.setVgap(0);
    _boardPanelTestPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _boardPanelTestPanelFlowLayout.setVgap(5);
    _boardTestOptionsPanel.setBorder(BorderFactory.createEmptyBorder(0,15,0,0));
    //_testControlsPanel.add(_testTypePanel, BorderLayout.NORTH);

    _testTypeLabel.setText(StringLocalizer.keyToString("ATGUI_TEST_TYPE_KEY"));
    FlowLayout testTypeControlFlowLayout = new FlowLayout();
    testTypeControlFlowLayout.setAlignment(FlowLayout.LEFT);
    _testTypeControlPanel.setLayout(testTypeControlFlowLayout);
    _componentPinTestOuterPanelBorderLayout.setHgap(5);
    _testTypeControlPanel.add(_testTypeLabel);
    _testTypeControlPanel.add(_testModeSelectComboBox);
    _testModeSelectComboBox.setPreferredSize(_jointTypeComboBox.getPreferredSize());
    _testControlPanel.setLayout(_testControlBorderLayout);
    _testControlPanel.add(_testTypeControlPanel, BorderLayout.WEST);
    _testControlBoardFlowLayout.setAlignment(FlowLayout.LEFT);
    _testControlBoardFlowLayout.setHgap(5);
    _testControlBoardPanel.setLayout(_testControlBoardFlowLayout);
    _panelBoardLabel.setText(StringLocalizer.keyToString("ATGUI_PANEL_BOARD_KEY"));
    _testControlBoardPanel.add(_panelBoardLabel);
    _testControlBoardPanel.add(_boardSelectComboBox);
    _testControlPanel.add(_testControlBoardPanel, BorderLayout.CENTER);
    _testTypePanel.add(_testControlPanel, BorderLayout.NORTH);
    _testTypePanel.add(_subtypeTestPanel,  BorderLayout.CENTER);
    _subtypeCommentPanel.add(_subtypeCommentTextFieldPanel, BorderLayout.SOUTH);
    _subtypeCommentTextFieldPanel.add(_subtypeCommentTextField, BorderLayout.CENTER);
    _subtypeCommentPanel.add(_subtypeCommentLabelPanel, BorderLayout.CENTER);
    _subtypeCommentLabelPanel.add(_subtypeCommentLabel, BorderLayout.SOUTH);
    _subtypeTestPanel.add(_jointTypeSubtypePanel, BorderLayout.WEST);
    _subtypeTestPanel.add(_subtypeCommentPanel, BorderLayout.CENTER);
    _jointTypeSubtypeComboBoxPanel.add(_jointTypeComboBox, null);
    _jointTypeSubtypeComboBoxPanel.add(_subtypeComboBox, null);
    _jointTypeSubtypePanel.add(_jointTypeSubtypeLabelPanel, BorderLayout.WEST);
    _jointTypeSubtypePanel.add(_jointTypeSubtypeComboBoxPanel, BorderLayout.CENTER);
    _jointTypeSubtypeLabelPanel.add(_jointTypeLabel, null);
    _jointTypeSubtypeLabelPanel.add(_subtypeLabel, null);

        // added by Chin Seong
    _subtypeAdvanceSettingsButton = new JButton("...");
    _subtypeAdvanceSettingsButton.setToolTipText(StringLocalizer.keyToString("ATGUI_SUBTYPE_ADVANCED_SETTINGS_BUTTON_KEY"));   
//    JPanel testPanel = new JPanel();
//    testPanel.setLayout(new BorderLayout());
//    testPanel.add(_subtypeAdvanceSettingsButton, BorderLayout.EAST);
//    testPanel.add(_testTypePanel, BorderLayout.NORTH);
    _subtypeCommentTextFieldPanel.add(_subtypeAdvanceSettingsButton, BorderLayout.EAST);
    _subtypeAdvanceSettingsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        subtypeAdvancedSettingsButton_actionPerformed(e);
      }
    });

    _centerPanel.add(_testTypePanel, BorderLayout.NORTH);

    _centerPanel.add(_testInfoAndThresholdsSplitPane, BorderLayout.CENTER);

    _runInspectionPanel = new RunInspectionPanel(_testDev, _panelDataAdapter, _panelInspectionAdapter);
    _reviewDefectsPanel = new ReviewDefectsPanel(_testDev,this);
    _reviewMeasurementsPanel = new ReviewMeasurementsPanel(_testDev);

    // Add listener to tab pane before we add the tab.  This way, the first tab
    // will generate event and AbstractTunerTaskPanel.start() will get invoked.
    _currentSelectedTaskTab = 0;
    _tunerTaskPane.addTab(StringLocalizer.keyToString("ATGUI_RUN_CONTROL_KEY"), Image5DX.getImageIcon(Image5DX.RUN_TESTS), _runInspectionPanel);
    _tunerTaskPane.addTab(StringLocalizer.keyToString("ATGUI_REVIEW_DEFECTS_KEY"), Image5DX.getImageIcon(Image5DX.REVIEW_DEFECTS), _reviewDefectsPanel);
    _tunerTaskPane.addTab(StringLocalizer.keyToString("ATGUI_REVIEW_MEASUREMENT_KEY"), Image5DX.getImageIcon(Image5DX.REVIEW_MEASUREMENTS), _reviewMeasurementsPanel);

    //Ngie Xing add, April 2014
    //XCR1795 to add shortcut keys for switching between panels in _tunerTaskPane
    setupTunerTaskPaneKeyStrokeAction();

    _testInfoAndThresholdsSplitPane.add(_tunerTaskPane, JSplitPane.TOP);
    _testInfoAndThresholdsSplitPane.add(_thresholdInfoPanel, JSplitPane.BOTTOM);
    _thresholdsTypePanel.add(_jointTypeSubtypeReferencePanel, BorderLayout.CENTER);
    _jointTypeSubtypeReferencePanel.add(_jointTypeReferencePanel, null);
    _jointTypeReferencePanel.add(_jointTypeReferenceInfoLabel, BorderLayout.WEST);
    _jointTypeReferencePanel.add(_jointTypeReferenceLabel, BorderLayout.CENTER);
    _jointTypeSubtypeReferencePanel.add(_subtypeReferencePanel, null);
    _subtypeReferencePanel.add(_subtypeReferenceInfoLabel, BorderLayout.WEST);
    _subtypeReferencePanel.add(_subtypeReferenceLabel, BorderLayout.CENTER);
    _thresholdsTypePanel.add(_standardAdvancedSelectPanel, BorderLayout.EAST);
    //XCR-1169 Threshold Description
    _standardAdvancedSelectPanel.add(_showThresholdDescriptionCheckBox, null);
    _standardAdvancedSelectPanel.add(_standardThresholdsRadioButton, null);
    _standardAdvancedSelectPanel.add(_advancedThresholdsRadioButton, null);

    //XCR-1169 Threshold Description
    _thresholdDetailsPanel.setLayout(_thresholdDetailsPanelBorderLayout);
    _thresholdDetailsPanel.add(_thresholdsTypePanel, BorderLayout.NORTH);
    _thresholdDetailsPanel.add(_thresholdsTabbedPane, BorderLayout.CENTER);

    _thresholdDescriptionAndCommentPanel.setLayout(_thresholdDescriptionAndCommentPanelBorderLayout);
    _thresholdDescriptionAndCommentPanel.add(_simpleDescriptionPanel, BorderLayout.NORTH);
    _thresholdDescriptionAndCommentPanel.add(_thresholdCommentPanel, BorderLayout.SOUTH);
    _thresholdDescriptionAndCommentPanel.setVisible(false);
    _thresholdDetailsPanel.add(_thresholdDescriptionAndCommentPanel, BorderLayout.SOUTH);

    _thresholdCommentPanel.add(_thresholdCommentLabel, BorderLayout.WEST);
    _thresholdCommentPanel.add(_thresholdCommentTextField, BorderLayout.CENTER);
//    _thresholdDescriptionPanel.add(_showDescriptionPanel, BorderLayout.NORTH);
    _showDescriptionPanel.add(_showDescriptionToggleButton, null);

    _thresholdDescriptionPanel.add(_totalDescriptionPanel, BorderLayout.CENTER);
    //XCR-1169 Threshold Description
    _thresholdSimpleDescriptionValue.setEditable(false);

    _totalDescriptionPanel.add(_descriptionAndImagePanel, BorderLayout.CENTER);
    //XCR-1169 Threshold Description
    _simpleDescriptionPanel.add(_thresholdSimpleDescriptionLabel, BorderLayout.WEST);
    _simpleDescriptionPanel.add(_thresholdSimpleDescriptionValue, BorderLayout.CENTER);

    _descriptionAndImagePanel.add(_descriptionPanel, BorderLayout.CENTER);
    _descriptionPanel.add(_descriptionScrollPane, BorderLayout.CENTER);

    _descriptionScrollPane.getViewport().add(_descriptionEditorPane, null);
    _descriptionAndImagePanel.add(_imagePanel, BorderLayout.EAST);
    _imagePanel.add(_imageLabel, null);
    _totalDescriptionPanel.add(_moreInfoPanel, BorderLayout.SOUTH);
    _moreInfoPanel.add(_moreInfoButton, null);
    _componentPinComboBoxPanel.add(_componentComboBox, null);
    _componentPinComboBoxPanel.add(_padComboBox, null);
    _componentPinLabelPanel.add(_componentLabel, null);
    _componentPinLabelPanel.add(_pinLabel, null);
    _componentPinSelectPanel.add(_componentPinComboBoxPanel, BorderLayout.CENTER);
    _componentPinSelectPanel.add(_componentPinLabelPanel, BorderLayout.WEST);
    _jointTypeSubtypeInfoLabelPanel.add(_jointTypeInfoInfoLabel, null);
    _jointTypeSubtypeInfoLabelPanel.add(_subtypeInfoInfoLabel, null);
    _jointTypeSubtypeInfoPanel.add(_currentJointTypeSubtypeInfoPanel, BorderLayout.CENTER);
    _currentJointTypeSubtypeInfoPanel.add(_jointTypeInfoLabel, null);
    _currentJointTypeSubtypeInfoPanel.add(_subtypeInfoLabel, null);
    _jointTypeSubtypeInfoPanel.add(_jointTypeSubtypeInfoLabelPanel, BorderLayout.WEST);
    
    _boardPanelTestPanel.add(_boardSelectPanel, null);
    _boardSelectPanel.add(_boardInstancePanel, null);
    _boardInstancePanel.add(_boardLabel, null);
    _boardInstancePanel.add(_boardNumberLabel, null);
    _boardSelectPanel.add(_boardInstanceSelectPanel, null);
//    _boardInstanceSelectPanel.add(_boardSelectComboBox, BorderLayout.NORTH);
    _boardSelectComboBox.setPreferredSize(new Dimension(150,CommonUtil._COMBO_BOX_HEIGHT));
    _boardSelectComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        boardComboBox_actionPerformed(e);
      }
    });

    _boardPanelTestPanel.add(_boardTestOptionsPanel, null);
    _boardTestOptionsPanel.add(_testAllBoardsCheckBox, null);
//    _boardTestOptionsPanel.add(_useFreshImagesCheckBox, null);
    ButtonGroup standardAdvancedThresholdsGroup = new ButtonGroup();
    standardAdvancedThresholdsGroup.add(_standardThresholdsRadioButton);
    standardAdvancedThresholdsGroup.add(_advancedThresholdsRadioButton);
    // APM - removing threshold description for 1.0
//    _thresholdDetailsAndDescriptionSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
//    _thresholdDetailsAndDescriptionSplitPane.add(_thresholdDetailsPanel, JSplitPane.TOP);
//    _thresholdDetailsAndDescriptionSplitPane.add(_thresholdDescriptionPanel, JSplitPane.BOTTOM);
//    _thresholdInfoPanel.add(_thresholdDetailsAndDescriptionSplitPane, java.awt.BorderLayout.CENTER);
    _thresholdInfoPanel.add(_thresholdDetailsPanel, java.awt.BorderLayout.CENTER);
    _componentPinTestOuterPanel.add(_componentPinSelectPanel, java.awt.BorderLayout.WEST);
    _componentPinTestOuterPanel.add(_jointTypeSubtypeInfoPanel, java.awt.BorderLayout.CENTER);
    _standardThresholdsRadioButton.setSelected(true);

    // this is the size used if the .persist file is missing
    _centerPanel.setPreferredSize(new Dimension(540, 1000));  // for 1600x1200

    _subtypeCommentTextField.addFocusListener(new java.awt.event.FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
//        System.out.println("_subtypeCommentTextField Lost Focus @ " + System.currentTimeMillis());
        subtypeCommentTextField_focusLost(e);
      }
    });
    _subtypeCommentTextField.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        subtypeCommentTextField_actionPerformed(e);
      }
    });
      
    //Siew Yeng - XCR-2764 - Sub-subtype feature
    _subRegionComboBoxActionListener = new java.awt.event.ActionListener() 
    {
      public void actionPerformed(ActionEvent e) 
      {
        subSubtypeComboBox_actionPerformed(e);
      }
    };

    _testModeSelectComboBox.setName(".testModeSelectComboBox");
    _validateDisabledAlgoButton.setName(".validateDisabledAlgoButton");
    _viewDefectiveComponentJointButton.setName(".viewDefectiveComponentJointButton");
    _copyThresholdsButton.setName(".copyThresholdsButton");
    _advancedThresholdsRadioButton.setName(".advancedThresholdsRadioButton");
    _standardThresholdsRadioButton.setName(".standardThresholdsRadioButton");
    _thresholdCommentTextField.setName(".thresholdCommentTextField");
    _showDescriptionToggleButton.setName(".showDescriptionToggleButton");
    _moreInfoButton.setName(".moreInfoButton");
    _componentComboBox.setName(".componentComboBox");
    _subtypeCommentTextField.setName(".subtypeCommentTextField");
    _jointTypeComboBox.setName(".jointTypeComboBox");
    _padComboBox.setName(".padComboBox");
    _boardSelectComboBox.setName(".boardSelectComboBox");
    _testAllBoardsCheckBox.setName(".testAllBoardsCheckBox");
    //XCR-1169 Threshold desc
    _showThresholdDescriptionCheckBox.setName(".showThresholdDescriptionCheckBox");
    _thresholdSimpleDescriptionValue.setName(".thresholdSimpleDescriptionValue");
    _showThresholdDescriptionCheckBox.setName("showThresholdDescriptionCheckBox");
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTestDev(TestDev testDev)
  {
    Assert.expect(testDev != null);
    _testDev = testDev;
    _mainUI = _testDev.getMainUI();
  }

  /**
   * @author Andy Mechtenberg
   */
  public JPanel getAlgorithmTunerPanel()
  {
    // get an instance to the tested joints manager. this is needed for review defects -eew
    jbInit();
    setGuiCustomizations();
    return _centerPanel;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void createMenus()
  {
    // menu stuff
    _toolsMenu = new JMenu();
    _validateDisabledAlgoMenuItem = new JMenuItem();
    _viewMeasurementJointHighlightMenuItem = new JMenuItem();
    _copyThresholdsMenuItem = new JMenuItem();
    _updateNominalsMenuItem = new JMenuItem();
    _thresholdsDetailInfoMenuItem = new JMenuItem();

    _toolsMenu.setText(StringLocalizer.keyToString("ATGUI_TOOLS_MENU_KEY"));
    
    _validateDisabledAlgoMenuItem.setText(StringLocalizer.keyToString("ATGUI_VALIDATE_DISABLED_ALGO_MENU_KEY"));
    _validateDisabledAlgoMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        validateDisabledAlgoButton_actionPerformed();
      }
    });
    
    _viewMeasurementJointHighlightMenuItem.setText(StringLocalizer.keyToString("ATGUI_VIEW_MEASUREMENT_JOINT_HIGHLIGHT_SETTING_KEY"));
    _viewMeasurementJointHighlightMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        viewMeasurementJointHighlightButton_actinPerformed();
      }
    });
    
    _copyThresholdsMenuItem.setText(StringLocalizer.keyToString("ATGUI_COPY_THRESHOLDS_MENU_KEY"));
    _copyThresholdsMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        copyThresholdsButton_actionPerformed();
      }
    });

    _updateNominalsMenuItem.setText(StringLocalizer.keyToString("ATGUI_UPDATE_NOMINALS_MENU_KEY"));
    //XCR-3169: Request Shortcut Key for Update Nominal Button
    _updateNominalsMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_F4, KeyEvent.CTRL_DOWN_MASK));
    _updateNominalsMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        updateNominalsButton_actionPerformed();
      }
    });

    _thresholdsDetailInfoMenuItem.setText(StringLocalizer.keyToString("ATGUI_ALGORITHM_HELP_MENU_KEY"));
    _thresholdsDetailInfoMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_F2, KeyEvent.CTRL_DOWN_MASK));
    _thresholdsDetailInfoMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        thresholdSimpleDescriptionButton_actionPerformed();
      }
    });
  }

  /**
   * @author George Booth
   */
  public void addTunerMenus(MainUIMenuBar menuBar, int menuRequesterID)
  {
    // tools menu addition
    JMenu toolsMenu = menuBar.getToolsMenu();
    menuBar.addMenuItem(menuRequesterID, toolsMenu, _validateDisabledAlgoMenuItem);
    menuBar.addMenuItem(menuRequesterID, toolsMenu, _viewMeasurementJointHighlightMenuItem);
    menuBar.addMenuItem(menuRequesterID, toolsMenu, _copyThresholdsMenuItem);
    menuBar.addMenuItem(menuRequesterID, toolsMenu, _updateNominalsMenuItem);
    menuBar.addMenuItem(menuRequesterID, toolsMenu, _thresholdsDetailInfoMenuItem);
  }

  /**
   * @author George Booth
   */
  public void addTunerToolBar(MainUIToolBar toolBar, int toolBarRequesterID)
  {
    toolBar.addToolBarComponent(toolBarRequesterID, _validateDisabledAlgoButton);
    toolBar.addToolBarComponent(toolBarRequesterID, _viewDefectiveComponentJointButton);
    toolBar.addToolBarComponent(toolBarRequesterID, _copyThresholdsButton);
    toolBar.addToolBarComponent(toolBarRequesterID, _updateNominalsButton);
    toolBar.removeDefectPackagerButtons();
    toolBar.addDefectPackagerButtons();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _project = null;
    _currentBoardType = null;
    _jointTypeComboBox.removeActionListener(_jointTypeComboBoxActionListener);
    _tunerTaskPane.removeChangeListener(_tunerTaskChangeListener);
    // other specific customizations
    _boardSelectComboBox.removeAllItems();
    _jointTypeComboBox.removeAllItems();
    _subtypeComboBox.removeAllItems();
    _subtypeCommentTextField.setText("");

    _componentComboBox.removeAllItems();
    _componentComboBox.getEditor().setItem("");

    _padComboBox.removeAllItems();

    _jointTypeToSubtypeMap.clear();

    _thresholdsTabbedPane.removeAll();
    _currentComponentType = null;
    _currentSelectedTaskTab = 0;
    _tunerTaskPane.setSelectedIndex(_currentSelectedTaskTab);

    _projectObservable.deleteObserver(_algoTunerDatastoreObserver);
    //_guiObservable.deleteObserver(_algoTunerGuiObserver);

    if (_runInspectionPanel != null)
      _runInspectionPanel.unpopulate();

    if (_reviewDefectsPanel != null)
      _reviewDefectsPanel.unpopulate();

    if (_reviewMeasurementsPanel != null)
      _reviewMeasurementsPanel.unpopulate();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void populateWithProjectData(Project project, BoardType currentBoardType)
  {
    Assert.expect(project != null);
    Assert.expect(currentBoardType != null);
    _project = project;

    _currentBoardType = currentBoardType;

    // setup the panel program data changing observer
    _algoTunerDatastoreObserver = new AlgoTunerDatastoreObserver(AlgoTunerGui.this);

    _algoTunerGuiObserver = new AlgoTunerGuiObserver(AlgoTunerGui.this);
    // add this as an observer of the gui
    //_guiObservable.addObserver(_algoTunerGuiObserver);

    _jointTypeComboBox.addActionListener(_jointTypeComboBoxActionListener);
    _tunerTaskPane.addChangeListener(_tunerTaskChangeListener);

    populateTunerWithProjectData();

    _projectObservable.addObserver(_algoTunerDatastoreObserver);
  }

  /**
   * Update with project data -- can happen because some other screen changed data that the tuner will display
   * @author Andy Mechtenberg
   */
  public void updateWithProjectData()
  {
    _currentBoardType = _testDev.getCurrentBoardType();
    populateTunerWithProjectData();
    if(_currentBoardType.getName().equals(_projectPersistanceSettings.getTunerTypeBoardName()))
      setPanelSpecificPersistance();
    else
    {
      setNewProjectState();
      _jointTypeToSubtypeMap.clear();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void start()
  {
//    subscribeToInspectionEvents();
    _active = true;

    UIDefaults uiDefaults = UIManager.getDefaults();
    InputMap inputMap = (InputMap)uiDefaults.get("Table.ancestorInputMap");
    _defaultF2Action = inputMap.get(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
    inputMap.remove(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));

    getPersistanceData();
    setGuiCustomizations();
    if(_currentBoardType.getName().equals(_projectPersistanceSettings.getTunerTypeBoardName()))
      setPanelSpecificPersistance();
    else
    {
      setNewProjectState();
      _jointTypeToSubtypeMap.clear();
    }

    _testDev.addChartPanel();

    ((AbstractTunerTaskPanel) _tunerTaskPane.getSelectedComponent()).start();
    
    //Chin Seong, Kee - Add this observer when the Project has first generated image set.
    _commandManager.addObserver(this);
    _guiObservable.addObserver(this);
//    _selectedRendererObservable = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getSelectedRendererObservable();
//    _selectedRendererObservable.addObserver(this);
  }

  public void saveFineTuningPanel(int spliterHeight, int spliterWidth, int spliterLocation)
  {
    _tunerPersistSettings.setFineTuningSpliterHeight(spliterHeight);
    _tunerPersistSettings.setFineTuningSpliterWidth(spliterWidth);
    _tunerPersistSettings.setFineTuningSpliterLocation(spliterLocation);
  }

  public int getFineTuningSpliterHeight()
  {
    return _tunerPersistSettings.getFineTuningSpliterHeight();
  }

  public int getFineTuningSpliterWidth()
  {
    return _tunerPersistSettings.getFineTuningSpliterWidth();
  }

  public int getFineTuningSpliterLocation()
  {
    return _tunerPersistSettings.getFineTuningSpliterLocation();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void saveUIState()
  {
    _tunerPersistSettings.setTestInfoToThresholdDividerLocation(_testInfoAndThresholdsSplitPane.getDividerLocation());
    _tunerPersistSettings.setThresholdDetailsAndDescriptionDividerLocation(_thresholdDetailsAndDescriptionSplitPane.getDividerLocation());
    _tunerPersistSettings.setShowDescriptionsOn(_showDescriptionToggleButton.isSelected());

    // panel specific stuff
    if ((_project != null) && (_projectPersistanceSettings != null))
    {
      _projectPersistanceSettings.setTunerBoardTypeName(_currentBoardType.getName());
      _projectPersistanceSettings.setTestModeSelection(_testModeSelectComboBox.getSelectedIndex());

      _projectPersistanceSettings.setTunerJointType(((JointTypeChoice)_jointTypeComboBox.getSelectedItem()).toString());
      _projectPersistanceSettings.setTunerSubtype(((SubtypeChoice)_subtypeComboBox.getSelectedItem()).toString());

      if (_componentComboBox.getItemCount() > 0)
        _projectPersistanceSettings.setTunerComponent(((ComponentType)_componentComboBox.getSelectedItem()).toString());
      if (_padComboBox.getItemCount() > 0)
        _projectPersistanceSettings.setTunerPad(((PadChoice)_padComboBox.getSelectedItem()).toString());
    }
  }
  
    /**
   * @author Jack Hwee
   */
  public void saveActiveUIState()
  {
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();

    SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    
    Object panelBoardChoice = _boardSelectComboBox.getSelectedItem();
    
    if ((jointTypeChoice != null) && (subtypeChoice != null) && (panelBoardChoice != null))
    {
      _testDev.setActiveTunerSubtype(jointTypeChoice, subtypeChoice, panelBoardChoice);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void getPersistanceData()
  {
    // create the testoptions instance
    _tunerPersistSettings = _testDev.getPersistance();
    _projectPersistanceSettings = _testDev.getPersistance().getProjectPersistance();
  }

  /**
   * @author Erica Wheatcroft
   */
  UndoState getCurrentUndoState(int thresholdRow, Algorithm algorithm, AlgorithmSetting algorithmSetting)
  {
    Assert.expect(algorithm != null);
    Assert.expect(algorithmSetting != null);

    int undoTabIndex = -1;
    String algorithmName = "";

    // find the index for the passed in algorithm
    // if the algorithmSetting is a slice setup, then look for that index
    if (algorithmSetting.getAlgorithmSettingTypeEnum(algorithm.getVersion()).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT) ||
       (algorithmSetting.getAlgorithmSettingTypeEnum(algorithm.getVersion()).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL)))
      algorithmName = StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY");
    else
      algorithmName = algorithm.getName();

    for (int i = 0; i < _thresholdsTabbedPane.getTabCount(); i++)
    {
      if (_thresholdsTabbedPane.getTitleAt(i).equals(algorithmName))
        undoTabIndex = i;
    }


    UndoState undoState = new UndoState();
    undoState.setTestModeSelection(_testModeSelectComboBox.getSelectedIndex());

    undoState.setTunerJointType(((JointTypeChoice)_jointTypeComboBox.getSelectedItem()).toString());
    undoState.setTunerSubtype(((SubtypeChoice)_subtypeComboBox.getSelectedItem()).toString());

    if (_componentComboBox.getItemCount() > 0)
      undoState.setTunerComponent(((ComponentType)_componentComboBox.getSelectedItem()).toString());
    undoState.setTunerPad(((PadChoice)_padComboBox.getSelectedItem()).toString());

    undoState.setTunerThresholdTabIndex(undoTabIndex);
    undoState.setTunerBasicThresholds(_standardThresholdsRadioButton.isSelected());

    undoState.setTunerThresholdRow(thresholdRow);

    return undoState;
  }

  /**
   * @author Erica Wheatcroft
   */
  UndoState getCurrentUndoState(int thresholdRow)
  {
    UndoState undoState = new UndoState();
    undoState.setTestModeSelection(_testModeSelectComboBox.getSelectedIndex());

    undoState.setTunerJointType(((JointTypeChoice)_jointTypeComboBox.getSelectedItem()).toString());
    undoState.setTunerSubtype(((SubtypeChoice)_subtypeComboBox.getSelectedItem()).toString());

    if (_componentComboBox.getItemCount() > 0)
      undoState.setTunerComponent(((ComponentType)_componentComboBox.getSelectedItem()).toString());
    undoState.setTunerPad(((PadChoice)_padComboBox.getSelectedItem()).toString());

    undoState.setTunerThresholdTabIndex(_thresholdsTabbedPane.getSelectedIndex());
    undoState.setTunerBasicThresholds(_standardThresholdsRadioButton.isSelected());

    undoState.setTunerThresholdRow(thresholdRow);

    return undoState;
  }

  /**
  * Called after frame has been constructed.  This sets up some user interface customizations all in one place
  *
  * @author Andy Mechtenberg
  */
  private void setGuiCustomizations()
  {
    // other specific customizations
    _testInfoAndThresholdsSplitPane.setDividerLocation(_tunerPersistSettings.getTestInfoToThresholdDividerLocation());
    int dividerLocation = _tunerPersistSettings.getThresholdDetailsAndDescriptionDividerLocation();
    if (dividerLocation != -1)
      _thresholdDetailsAndDescriptionSplitPane.setDividerLocation(dividerLocation);
//    _showDescriptionToggleButton.setSelected(_tunerPersistSettings.getShowDescriptionsOn());
//    showDescriptionToggleButton_actionPerformed();
  }

  /**
  * Called after frame has been constructed.  This sets up some user interface customizations all in one place
  *
  * @author Andy Mechtenberg
  */
  private void populateTunerWithProjectData()
  {
    // other specific customizations
    populateBoardComboBox();
    populateJointTypeComboBox();
    populateSubtypeComboBox();
//    System.out.println("PopulateTunerWithProjectData");
//    setRunDescription();
    setSubtypeComment();

    populateComponentComboBox();
    populatePadComboBox();

    populateThresholdDataTables();
//    repaint();  // neccessary to make all the text visible in the controls.  Mainly for the simulation indicator.
  }

  /**
   * XCR1795, Added April 2014, Adds shortcut keys for Inspection, ReviewDefects and ReviewMeasurements Panel
   *
   * @author Ngie Xing
   */
  private void setupTunerTaskPaneKeyStrokeAction()
  {
    KeyStroke switchToInspectionPanelKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_1, KeyEvent.ALT_DOWN_MASK);
    KeyStroke switchToDefectsPanelKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_2, KeyEvent.ALT_DOWN_MASK);
    KeyStroke SwitchToMeasurementPanelKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_3, KeyEvent.ALT_DOWN_MASK);

    InputMap inputMap = _tunerTaskPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
    ActionMap actionMap = _tunerTaskPane.getActionMap();
    inputMap.remove(switchToInspectionPanelKeyStroke);
    inputMap.remove(switchToDefectsPanelKeyStroke);
    inputMap.remove(SwitchToMeasurementPanelKeyStroke);
    inputMap.put(switchToInspectionPanelKeyStroke, _SWITCH_TO_INSPECTION_PANEL_KEY);
    inputMap.put(switchToDefectsPanelKeyStroke, _SWITCH_TO_DEFECTS_PANEL_KEY);
    inputMap.put(SwitchToMeasurementPanelKeyStroke, _SWITCH_TO_MEASUREMENT_PANEL_KEY);

    actionMap.put(_SWITCH_TO_INSPECTION_PANEL_KEY, new javax.swing.AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        _tunerTaskPane.setSelectedComponent(_runInspectionPanel);
      }
    });
    actionMap.put(_SWITCH_TO_DEFECTS_PANEL_KEY, new javax.swing.AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        _tunerTaskPane.setSelectedComponent(_reviewDefectsPanel);
      }
    });
    actionMap.put(_SWITCH_TO_MEASUREMENT_PANEL_KEY, new javax.swing.AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        _tunerTaskPane.setSelectedComponent(_reviewMeasurementsPanel);
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setNewProjectState()
  {
    _testModeSelectComboBox.setSelectedIndex(0);
    if (_jointTypeComboBox.getItemCount() > 0)
      _jointTypeComboBox.setSelectedIndex(0);
    if (_subtypeComboBox.getItemCount() > 0)
      _subtypeComboBox.setSelectedIndex(0);
    if (_componentComboBox.getItemCount() > 0) // if box has components, do something
      _componentComboBox.setSelectedIndex(0);
    if (_padComboBox.getItemCount() > 0)
      _padComboBox.setSelectedIndex(0);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setPanelSpecificPersistance()
  {
    int testMode = _projectPersistanceSettings.getTestModeSelection();
    _sendEvents--;
    _testModeSelectComboBox.setSelectedIndex(testMode);

    // We have check to make sure all components/pins/joint type/subtypes still exist in the data
    // Even though the panel name and board name haven't changed, the data underneath might.  If
    // we don't check, then we'll start throwing asserts if a component was deleted, for example.

    // we have to setup component/pin first, because this may change the joint type/subtype setting
    String persistedComponent = _projectPersistanceSettings.getTunerComponent();
    String persistedPad = _projectPersistanceSettings.getTunerPad();
    int count = _componentComboBox.getItemCount();
    if (count > 0)
      _componentComboBox.setSelectedIndex(0);
    for (int i = 0; i < count; i++)
    {
      ComponentType compType = (ComponentType)_componentComboBox.getItemAt(i);
      if (persistedComponent.equals(compType.toString()))
      {
        _componentComboBox.setSelectedIndex(i);
        break;
      }
    }

    count = _padComboBox.getItemCount();
    if (count > 0)
      _padComboBox.setSelectedIndex(0);
    for (int i = 0; i < count; i++)
    {
      PadChoice padChoice = (PadChoice)_padComboBox.getItemAt(i);
      if (persistedPad.equals(padChoice.toString()))
      {
        _padComboBox.setSelectedIndex(i);
        break;
      }
    }
    _sendEvents++;
    // now joint type/ subtype may be re-set to the persistant values
    if (testMode != 1) // 1 is component/pin.  Setting comp/pin will automatically set joint type/ subtype
    {
      if (_testDev.doesActiveJointTypeSubtypePanelBoardChoiceExist())
      {
        JointTypeChoice jointTypeChoice = _testDev.getActiveJointTypeChoice();
        SubtypeChoice subtypeChoice = _testDev.getActiveSubtypeChoice();
        _jointTypeComboBox.setSelectedItem(jointTypeChoice);
        _subtypeComboBox.setSelectedItem(subtypeChoice);
        
        if (_testDev.isBoardInstanceSelected())
        {
          Board activeBoard = _testDev.getActiveBoardChoice();
          _boardSelectComboBox.setSelectedItem(activeBoard);
        }
        else
          _boardSelectComboBox.setSelectedIndex(0);
      }
      else
      {

        _sendEvents--;
        int selectIndex = -1;
        String persistJointTypeString = _projectPersistanceSettings.getTunerJointType();
        String persistSubtypeString = _projectPersistanceSettings.getTunerSubtype();
        count = _jointTypeComboBox.getItemCount();
        for (int i = 0; i < count; i++)
        {
          JointTypeChoice jtc = (JointTypeChoice)_jointTypeComboBox.getItemAt(i);
          if (persistJointTypeString.equals(jtc.toString()))
          {
            selectIndex = i;
            _jointTypeComboBox.setSelectedIndex(i);
            break;
          }
        }
        if (selectIndex == -1)
          _jointTypeComboBox.setSelectedIndex(0);
        _sendEvents++;
        // now, do the same for the subtype combo box
        selectIndex = -1;
        count = _subtypeComboBox.getItemCount();
        for (int i = 0; i < count; i++)
        {
          SubtypeChoice caf = (SubtypeChoice)_subtypeComboBox.getItemAt(i);
          if (persistSubtypeString.equals(caf.toString()))
          {
            selectIndex = i;
            _subtypeComboBox.setSelectedIndex(i);
            break;
          }
        }
        if (selectIndex == -1)
          _subtypeComboBox.setSelectedIndex(0);
      }
    }
    else
      setInfoJointTypeSubtype();  // This is based on component/pin, which we just set


  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateJointTypeComboBox()
  {
    _jointTypeComboBox.setPopupWidthToDefault();
    _jointTypeComboBox.removeAllItems();
    _sendEvents--;
    PanelDataUtil.populateComboBoxWithJointTypes(_currentBoardType, _jointTypeComboBox);
    _sendEvents++;
//    _jointTypeComboBox.addItem(new JointTypeChoice()); // the ALL case
//
//    java.util.List<JointTypeEnum> jointTypes = _panelDataAdapter.getJointTypeEnums();
//    for (JointTypeEnum jte : jointTypes)
//    {
//      JointTypeChoice jointTypeChoice = new JointTypeChoice(jte);
//      _jointTypeComboBox.addItem(jointTypeChoice);
//    }
//    _jointTypeComboBox.setKeySelectionManager(_jointTypeSelectionManager);
//    _jointTypeComboBox.setMaximumRowCount(CommonUtil._MAXIMUM_COMBOBOX_ROWCOUNT);

    FontMetrics fontMetrics = _jointTypeComboBox.getFontMetrics(_jointTypeComboBox.getFont());
    int maxStringLength = 0;

    java.util.List<JointTypeEnum> jointTypes = _currentBoardType.getInspectedJointTypeEnums();

    for (JointTypeEnum jointType : jointTypes)
    {
      int itemStringLength = fontMetrics.stringWidth(jointType.getName());
      if (maxStringLength < itemStringLength)
        maxStringLength = itemStringLength;
    }
    _jointTypeComboBox.setMaximumRowCount(CommonUtil._MAXIMUM_COMBOBOX_ROWCOUNT);

    // set the popup width to the max of the strings only if that's longer than the box itself
    if ((maxStringLength + VariableWidthComboBox.widthExtension) <= _jointTypeComboBox.getWidth())
      _jointTypeComboBox.setPopupWidthToDefault();
    else
      _jointTypeComboBox.setPopupWidth(maxStringLength + VariableWidthComboBox.widthExtension); // add a bit more so the last char is not RIGHT at the popup right side.

  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateSubtypeComboBox()
  {
    _subtypeComboBox.setPopupWidthToDefault();

    // if joint type is "all" or null, then don't fill in the subtype box
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    _sendEvents--;
    PanelDataUtil.populateComboBoxWithSubtypes(_currentBoardType, jointTypeChoice, _subtypeComboBox);
    _sendEvents++;
    if ((jointTypeChoice == null) || (jointTypeChoice.isAllFamilies()))
       return;

    FontMetrics fontMetrics = _subtypeComboBox.getFontMetrics(_subtypeComboBox.getFont());
    int maxStringLength = 0;

    java.util.List<Subtype> subtypes = _currentBoardType.getInspectedSubtypes(jointTypeChoice.getJointTypeEnum());

    for (Subtype subtype : subtypes)
    {
      int itemStringLength = fontMetrics.stringWidth(subtype.toString());
      if (maxStringLength < itemStringLength)
        maxStringLength = itemStringLength;
    }
    _subtypeComboBox.setMaximumRowCount(CommonUtil._MAXIMUM_COMBOBOX_ROWCOUNT);

    // set the popup width to the max of the strings only if that's longer than the box itself
    int w = _subtypeComboBox.getWidth();
    if ((maxStringLength + VariableWidthComboBox.widthExtension) <= _subtypeComboBox.getWidth())
      _subtypeComboBox.setPopupWidthToDefault();
    else
      _subtypeComboBox.setPopupWidth(maxStringLength + VariableWidthComboBox.widthExtension); // add a bit more so the last char is not RIGHT at the popup right side.
  }

  /**
   * @author Andy Mechtenberg
   */
  private void notifyOfTestSelection()
  {
    int index = _testModeSelectComboBox.getSelectedIndex();
    ComponentType currentComponentType = (ComponentType)_componentComboBox.getSelectedItem();
    PadChoice currentPadChoice = (PadChoice)_padComboBox.getSelectedItem();
    JointTypeChoice currentJointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    SubtypeChoice currentSubtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();

    TunerTestState tunerTestState = null;

    if ((currentJointTypeChoice == null) || (currentSubtypeChoice == null))
      return;

    if (index == 0)// joint type
    {
      tunerTestState = new TunerTestState(currentJointTypeChoice, currentSubtypeChoice);
    }
    else if ((index == 1) && (currentComponentType != null) &&(currentPadChoice != null))  // component/pin
    {
      tunerTestState = new TunerTestState(currentComponentType, currentPadChoice, currentJointTypeChoice, currentSubtypeChoice);
    }
    else if (index > 1)
      Assert.expect(false, "unexpected index: " + index);

    if (_active && (_sendEvents >= 0) && (tunerTestState != null))
    {
//      System.out.println("Sending Event: " + tunerTestState);
      _guiObservable.stateChanged(tunerTestState, SelectionEventEnum.TUNER_TEST_SELECTION);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setSubtypeComment()
  {
    _subtypeCommentTextField.setText("");
    SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    if ((subtypeChoice != null) && (subtypeChoice.isAllSubtypes() == false))  // if not equal to "All"
    {
      String comment = subtypeChoice.getSubtype().getUserComment();
      _subtypeCommentTextField.setText(comment);
      _subtypeCommentTextField.setEnabled(true);
    }
    else
    {
      _subtypeCommentTextField.setEnabled(false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setInfoJointTypeSubtype()
  {
    ComponentType componentType = (ComponentType)_componentComboBox.getSelectedItem();
    PadChoice padChoice = (PadChoice)_padComboBox.getSelectedItem();

    // the combo box may be in the process of filling itself in, so component or pin may be null
    if ((componentType == null) || (padChoice == null))
      return;

    // deal with the mixed joint type and subtype cases up front, as they're tricky
    if (padChoice.isAllPads())
    {
      if (componentType.getCompPackage().usesOneJointTypeEnum() == false)
      {
        _jointTypeInfoLabel.setText(StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY"));
        _sendEvents--;
        _jointTypeComboBox.setSelectedIndex(0);  // ALL Case
        _subtypeComboBox.setSelectedIndex(0); // also the ALL Case
        _sendEvents++;
      }
      if (componentType.getComponentTypeSettings().usesOneSubtype() == false)
      {
        _subtypeInfoLabel.setText(StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY"));
        if (componentType.getCompPackage().usesOneJointTypeEnum())
        {
          JointTypeEnum jointTypeEnum = componentType.getCompPackage().getJointTypeEnum();
          // we can either iterate through the combo box, and find the right guy to set it to, or just call setSelectedItem
          // if we call setSelectedItem, then JointTypeChoice must override equals and hashcode for it to work
          _sendEvents--;
          int count = _jointTypeComboBox.getItemCount();
          for (int i = 0; i < count; i++)
          {
            JointTypeChoice jtc = (JointTypeChoice)_jointTypeComboBox.getItemAt(i);
            if (jtc.isAllFamilies() == false)
            {
              if (jointTypeEnum.equals(jtc.getJointTypeEnum()))
              {
                _jointTypeComboBox.setSelectedIndex(i);
                break;
              }
            }
          }
          _subtypeComboBox.setSelectedIndex(0);  // this set's the subtype to ALL
          _sendEvents++;
          _jointTypeInfoLabel.setText(jointTypeEnum.toString());
        }
        notifyOfTestSelection();
        return;
      }
    }

    Subtype subtype;
    JointTypeEnum jointTypeEnum;
    if (padChoice.isAllPads())
    {
      // use the first pad in this case
      PadType firstPadType = componentType.getPadTypeOne();
      subtype = firstPadType.getSubtype();
    }
    else
    {
      subtype = padChoice.getPadType().getSubtype();
    }

    jointTypeEnum = subtype.getJointTypeEnum();
    _jointTypeInfoLabel.setText(jointTypeEnum.toString());
    _subtypeInfoLabel.setText(subtype.toString());

    // we can either iterate through the combo box, and find the right guy to set it to, or just call setSelectedItem
    // if we call setSelectedItem, then JointTypeChoice must override equals and hashcode for it to work
    _sendEvents--;
    int count = _jointTypeComboBox.getItemCount();
    for(int i = 0; i < count; i++)
    {
      JointTypeChoice jtc = (JointTypeChoice)_jointTypeComboBox.getItemAt(i);
      if (jtc.isAllFamilies() == false)
      {
        if (jointTypeEnum.equals(jtc.getJointTypeEnum()))
        {
          _jointTypeComboBox.setSelectedIndex(i);
          break;
        }
      }
    }
    // now, do the same for the subtype combo box
    count = _subtypeComboBox.getItemCount();
    for(int i = 0; i < count; i++)
    {
      SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getItemAt(i);
      if (subtypeChoice.isAllSubtypes() == false)
      {
        if (subtype.equals(subtypeChoice.getSubtype()))
        {
          _subtypeComboBox.setSelectedIndex(i);
          break;
        }
      }
    }
    _sendEvents++;
    notifyOfTestSelection();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateComponentComboBox()
  {
    // Tried using this -- it didn't work because each component that gets
    // selected, updated the threshold table, which removes focus from the combo box
    // which means you can't type ahead.
//    _componentComboBox.setKeySelectionManager(_componentSelectionManager);
    _sendEvents--;
    _componentComboBox.removeAllItems();
    java.util.List<ComponentType> componentTypes = _currentBoardType.getInspectedComponentTypes();
    for (ComponentType componentType : componentTypes)
    {
      _componentComboBox.addItem(componentType);
    }
    _componentComboBox.setMaximumRowCount(CommonUtil._MAXIMUM_COMBOBOX_ROWCOUNT);
    if (componentTypes.isEmpty())
      _componentComboBox.setEnabled(false);
    else
      _componentComboBox.setEnabled(true);
    _sendEvents++;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populatePadComboBox()
  {
    ComponentType componentType = (ComponentType)_componentComboBox.getSelectedItem();
    // component could be null if the combo box is empty (which happens when repopulating the control if the project changes)
    if (componentType == null)
      return;
    _sendEvents--;
    _padComboBox.removeAllItems();
    _padComboBox.addItem(new PadChoice());  // the ALL case
    java.util.List<PadType> padTypes =componentType.getPadTypes();
    for (PadType padType : padTypes)
    {
      PadChoice padChoice = new PadChoice(padType);
      _padComboBox.addItem(padChoice);
    }
    _padComboBox.setMaximumRowCount(CommonUtil._MAXIMUM_COMBOBOX_ROWCOUNT);
    if (padTypes.isEmpty())
      _padComboBox.setEnabled(false);
    _sendEvents++;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setJointTypeSubtypeTableReference()
  {
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    if (jointTypeChoice == null)  // combo box was empty, so we cannot continue
      return;
    SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    if (subtypeChoice == null)  // combo box was empty, so we cannot continue
      return;
    String subtypeString = subtypeChoice.toString();
    _subtypeComboBox.setToolTipText(subtypeString);

    _jointTypeReferenceLabel.setText(jointTypeChoice.toString());
    _subtypeReferenceLabel.setText(subtypeString);
    _subtypeReferenceLabel.setToolTipText(subtypeString);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateBoardComboBox()
  {
    _boardSelectComboBox.removeAllItems();

    _boardSelectComboBox.addItem(StringLocalizer.keyToString("ATGUI_PANEL_KEY"));

    java.util.List<Board> boardInstances = _project.getPanel().getBoards();
    // if only one board instance, no need to put it in the boardinstance combobox
    // as the panel entry is enough
    if (boardInstances.size() > 1)
    {
      // sort boards based on name
      Collections.sort(boardInstances, new AlphaNumericComparator());
      for (Board board : boardInstances)
      {
        if (board.getBoardType().equals(_currentBoardType))
          _boardSelectComboBox.addItem(board);
      }
    }
    // this combobox is going in a tool bar, and I need to control the height of that box very carefully so the toolbar
    // doesn't grow - this causes a jitter when switching into or out of the tuner panel.
    // So, to make sure the length is controlled by the contents (for localization), I'll calculate the max width based on the
    // strings I'm putting in, and set the height to 21 pixels.
//    int maxStringLength = 0;
//    FontMetrics fm = _boardSelectComboBox.getFontMetrics(_boardSelectComboBox.getFont());
//    for (int i = 0; i < _boardSelectComboBox.getItemCount(); i++)
//    {
//      int length = fm.stringWidth(_boardSelectComboBox.getItemAt(i).toString());
//      if (length > maxStringLength) // update max length
//        maxStringLength = length;
//    }
//    _boardSelectComboBox.setPreferredSize(new Dimension(maxStringLength + 30, CommonUtil._COMBO_BOX_HEIGHT)); // the +30 is to make room for the dropdown control
//    _boardSelectComboBox.setMaximumSize(_boardSelectComboBox.getPreferredSize());
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jointTypeComboBox_actionPerformed(ActionEvent e)
  {
    JointTypeChoice newJointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    SubtypeChoice subtypeChoice = (SubtypeChoice)_jointTypeToSubtypeMap.get(newJointTypeChoice);
    populateSubtypeComboBox();
//    System.out.println("JointTypeComboBox action performed");
//    setRunDescription();
    setJointTypeSubtypeTableReference();

    // since they changed the jointType, look up the subypte they were last at (if any) and change the subtype to it.
    //System.out.println("Remembered Subtype is " + subtype);
    _sendEvents--;
    if ((subtypeChoice != null) && (PanelDataUtil.doesSubtypeExist(_currentBoardType, subtypeChoice)))
      _subtypeComboBox.setSelectedItem(subtypeChoice);
    _sendEvents++;

    notifyOfTestSelection();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void subtypeComboBox_actionPerformed(ActionEvent e)
  {
    setJointTypeSubtypeTableReference();
    setSubtypeComment();

    // get current algo (if any), so we can set the new threshold table to that algo
    // this is usefull when changing subtypes
    //System.out.println("--Subtype Combo box action performed FAMILY: " + _jointTypeComboBox.getSelectedItem() + " Subtype is " + _subtypeComboBox.getSelectedItem());
    populateThresholdDataTables();

    // now reset to the last active tab (if doesn't exist now, it will default to the first tab)
    if (doesCurrentAlgorithmExist() &&
        _thresholdsTabbedPane.getTitleAt(_thresholdsTabbedPane.getSelectedIndex()).equalsIgnoreCase(StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY")) == false)
    {
      AlgorithmEnum algoEnum = getCurrentAlgorithm().getAlgorithmEnum();

      for(int i = 0; i < _thresholdsTabbedPane.getTabCount(); i++)
      {
        String tabName = _thresholdsTabbedPane.getTitleAt(i);
        if (algoEnum.toString().equalsIgnoreCase(tabName))
          _thresholdsTabbedPane.setSelectedIndex(i);
      }
    }

    // remember this pair (jointType/subtype), so next time the jointType is changed we can keep the last subtype the user was working on
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();

    // don't remember jointType because they aren't real ones -- just added for test purposes
    if ((jointTypeChoice != null) && (jointTypeChoice.isAllFamilies() == false) && (subtypeChoice != null))// && (!subtypeChoice.isAllSubtypes()))
    {
      _jointTypeToSubtypeMap.put(jointTypeChoice, subtypeChoice);
      //System.out.println("Remembering " + jointType + " " + subtype);
    }
    notifyOfTestSelection();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void testModeSelectComboBox_actionPerformed(ActionEvent e)
  {
//    int widthAdjust = 5; // to line up the different boxes
    int index = _testModeSelectComboBox.getSelectedIndex();
    if (index == 0) // jointType/subtype
    {
      _testTypePanel.removeAll();
      _testTypePanel.add(_testControlPanel, BorderLayout.NORTH);
      _testTypePanel.add(_subtypeTestPanel, BorderLayout.CENTER);

//      Dimension preferredSize = _jointTypeLabel.getPreferredSize();
//      preferredSize.width = preferredSize.width + widthAdjust;
//      _testTypeLabel.setPreferredSize(preferredSize);
      JLabel[] labels = new JLabel[]{_testTypeLabel, _jointTypeLabel, _subtypeLabel};
      SwingUtils.makeAllSameLength(labels);

//      _testModeSelectComboBox.setPreferredSize(_jointTypeComboBox.getPreferredSize());

      _testTypePanel.revalidate();
      _testTypePanel.repaint();
//      System.out.println("Test Mode Combo Box action Performed (index = 0)");
//      setRunDescription();
    }
    else if (index == 1) // component/pin
    {
      _testTypePanel.removeAll();
      _testTypePanel.add(_testControlPanel, BorderLayout.NORTH);
      _testTypePanel.add(_componentPinTestOuterPanel, BorderLayout.CENTER);

//      Dimension preferredSize = _componentLabel.getPreferredSize();
//      preferredSize.width = preferredSize.width + widthAdjust;
//      _testTypeLabel.setPreferredSize(preferredSize);
      JLabel[] labels = new JLabel[]{_testTypeLabel, _componentLabel, _pinLabel};
      SwingUtils.makeAllSameLength(labels);

//      _testModeSelectComboBox.setPreferredSize(_componentComboBox.getPreferredSize());
      _testTypePanel.revalidate();
      _testTypePanel.repaint();
      _sendEvents--;
      setInfoJointTypeSubtype();
      _sendEvents++;
    }
    notifyOfTestSelection();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateToolBarButtonState()
  {
    JointTypeChoice currentJointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    SubtypeChoice currentSubtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();

    if ((currentJointTypeChoice == null) || (currentJointTypeChoice.isAllFamilies()) || (currentSubtypeChoice == null) || (currentSubtypeChoice.isAllSubtypes()))
    {

      _copyThresholdsButton.setEnabled(false);
      _copyThresholdsMenuItem.setEnabled(false);
    }
    else
    {
      _copyThresholdsButton.setEnabled(true);
      _copyThresholdsMenuItem.setEnabled(true);
    }

    // now deal with the update nominals -- we need to update it's tooltip to explain why it's disabled, so
    // this will be in it's own logic section
    boolean usingImageSets = (_selectedImageSets.isEmpty() == false);
    if (usingImageSets == false)
    {
      // disable the controls, and set up the tooltip
      _updateNominalsButton.setEnabled(false);
      _updateNominalsMenuItem.setEnabled(false);
      _updateNominalsButton.setToolTipText(StringLocalizer.keyToString("ATGUI_UPDATE_NOMINALS_TOOLTIP_NO_IMAGE_SETS_KEY"));
    }
    else
    {
      _updateNominalsButton.setEnabled(true);
      _updateNominalsMenuItem.setEnabled(true);
      _updateNominalsButton.setToolTipText(StringLocalizer.keyToString("ATGUI_UPDATE_NOMINALS_TOOLTIP_KEY"));
    }
  }

  /**
   * Set up the correct slice height based on selected jointtype/subtype
   * @author Laura Cormos
   */
  private void populateSliceHeightTab()
  {
    // add the last tab for slice height settings
    JPanel panel = new JPanel();
    BorderLayout panelBorderLayout = new BorderLayout();
    panel.setLayout(panelBorderLayout);

    JointTypeChoice currentJointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    SubtypeChoice currentSubtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    _standardThresholdsRadioButton.setEnabled(true);
    _advancedThresholdsRadioButton.setEnabled(true);

    if (currentJointTypeChoice != null && !currentJointTypeChoice.isAllFamilies())
    {
      if (currentSubtypeChoice != null && !currentSubtypeChoice.isAllSubtypes())
      {
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(currentJointTypeChoice.getJointTypeEnum());
        Algorithm algorithm = InspectionFamily.getAlgorithm(inspectionFamily.getInspectionFamilyEnum(), AlgorithmEnum.MEASUREMENT);
        panel.removeAll();
        if (_standardThresholdsRadioButton.isSelected())
        {
          ThresholdTableModel threshTableModel = new ThresholdTableModel(_mainUI,
                                                                         this,
                                                                         _panelDataAdapter,
                                                                         currentSubtypeChoice.getSubtype(),
                                                                         true); // standard settings
          //table model cannot be empty, all joint types have at least one slice height algorithm setting
          Assert.expect(threshTableModel.getRowCount() > 0);

          JScrollPane jsp = new JScrollPane();
          JTable tt = new JTable(threshTableModel);
          jsp.getViewport().add(tt);
          panel.add(jsp, BorderLayout.CENTER);
          // add label to explain that changes to slice height settings will invalidate image sets.
          JPanel warningLabelPanel = new JPanel();
          JLabel warningLabel = new JLabel(StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_INVALIDATES_IMAGE_SETS_WARNING_KEY"));
          warningLabel.setFont(FontUtil.getBoldFont(warningLabel.getFont(),Localization.getLocale()));
          warningLabelPanel.add(warningLabel);
          panel.add(warningLabelPanel, BorderLayout.NORTH);
          _thresholdsTabbedPane.add(panel, StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY"));

          setupTableCustomizations(currentSubtypeChoice.getSubtype(), tt);

          setupTableEditorsAndRenderers(algorithm, currentSubtypeChoice.getSubtype(), tt);
          //Wong Ngie Xing edit, MAY 2014, uncommented the next line 
          tt.changeSelection(0, 1, false, false);
          threshTableModel.setTestRunning(_inRunMode);//Siew Yeng - XCR-3220
        }
        else
        {
          ThresholdTableModel threshTableModel = new ThresholdTableModel(_mainUI,
                                                                         this,
                                                                         _panelDataAdapter,
                                                                         currentSubtypeChoice.getSubtype(),
                                                                         false);  // additional settings
          //table model cannot be empty, all joint types have at least one additional slice height algorithm setting
          Assert.expect(threshTableModel.getRowCount() > 0);

          JScrollPane jsp = new JScrollPane();
          JTable tt = new JTable(threshTableModel);
          jsp.getViewport().add(tt);
          panel.add(jsp, BorderLayout.CENTER);
          // add label to explain that changes to slice height settings will invalidate image sets.
          JPanel warningLabelPanel = new JPanel();
          JLabel warningLabel = new JLabel(StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_INVALIDATES_IMAGE_SETS_WARNING_KEY"));
          warningLabel.setFont(FontUtil.getBoldFont(warningLabel.getFont(),Localization.getLocale()));
          warningLabelPanel.add(warningLabel);
          panel.add(warningLabelPanel, BorderLayout.NORTH);
          _thresholdsTabbedPane.add(panel, StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY"));

          setupTableCustomizations(currentSubtypeChoice.getSubtype(), tt);
          setupTableEditorsAndRenderers(algorithm, currentSubtypeChoice.getSubtype(), tt);
          threshTableModel.setTestRunning(_inRunMode);//Siew Yeng - XCR-3220
//          tt.changeSelection(0, 1, false, false);
          /***
          JLabel label = new JLabel(StringLocalizer.keyToString("ATGUI_NO_THRESHOLDS_KEY"));
          JPanel innerPanel = new JPanel();
          innerPanel.setBorder(new EtchedBorder());
          innerPanel.add(label);
          panel.add(innerPanel, BorderLayout.CENTER);
          _thresholdsTabbedPane.add(panel, StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY"));
          ***/
        }
      }
    }
  }

  /**
   * Takes the algos specified by the jointType and adds tabs/tables to the correct panel
   * @author Andy Mechtenberg
   */
  private void populateThresholdTables()
  {
    // clear out all the tabs and the tables in them in preparation for creating new ones
    _thresholdsTabbedPane.removeAll();

    JointTypeChoice currentJointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    SubtypeChoice currentSubtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();

    updateToolBarButtonState();

    if ((currentJointTypeChoice == null) || (currentJointTypeChoice.isAllFamilies()) || (currentSubtypeChoice == null) || (currentSubtypeChoice.isAllSubtypes()))
    {
      setThresholdTablesToEmpty();
      return;
    }

    _standardThresholdsRadioButton.setEnabled(true);
    _advancedThresholdsRadioButton.setEnabled(true);
    boolean standardThresholds = _standardThresholdsRadioButton.isSelected();

    //System.out.println("Populate Threshold Table");
    // populate the threshold tabs and tables with them with Datastore data
    Subtype currentSubtype = currentSubtypeChoice.getSubtype();
    java.util.List<Algorithm> algorithms = _panelDataAdapter.getAlgorithms(currentSubtype);
    if (algorithms.isEmpty())
    {
      setThresholdTablesToEmpty();
      return;
    }
    for (Algorithm algorithm : algorithms)
    {
      AlgorithmEnum algoEnum = algorithm.getAlgorithmEnum();
      ThresholdTableModel threshTableModel = new ThresholdTableModel(_mainUI,
                                                                     this,
                                                                     _panelDataAdapter,
                                                                     currentSubtypeChoice.getSubtype(),
                                                                     algoEnum,
                                                                     standardThresholds);
      JPanel enableCheckBoxPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
      // create the enable check box
      JCheckBox enableCheckBox = new JCheckBox(StringLocalizer.keyToString("GUI_ENABLED_KEY"));
      enableCheckBox.setName(algoEnum.toString());
      // now, we need to determine if this algorithm is enabled or not to set the initial state of the checkbox
      java.util.List<AlgorithmEnum> enabledAlgorithms = currentSubtype.getEnabledAlgorithmEnums();

      if (enabledAlgorithms.contains(algoEnum))
        enableCheckBox.setSelected(true);
      else
        enableCheckBox.setSelected(false);

      enableCheckBox.addActionListener(new java.awt.event.ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          enableAlgorithmCheckBox_actionPerformed(e);
        }
      });

      enableCheckBoxPanel.add(enableCheckBox);
      enableCheckBoxPanel.add(new JLabel());
      
      // Jack Hwee
      JButton showMaskImageButton = new JButton(StringLocalizer.keyToString("ATGUI_SHOW_MASK_IMAGE_KEY"));
     
      if (currentSubtype.getInspectionFamilyEnum().equals(InspectionFamilyEnum.LARGE_PAD) && algoEnum.equals(AlgorithmEnum.VOIDING))
      { // System.out.println(currentSubtype.getInspectionFamily().getOrderedInspectionSlices(currentSubtype));       
        _subRegionComboBox.removeActionListener(_subRegionComboBoxActionListener);
        enableCheckBoxPanel.add(showMaskImageButton);
         
        final Subtype currentSubtypeForMask = currentSubtype;
        showMaskImageButton.addActionListener(new java.awt.event.ActionListener()
        {   
          public void actionPerformed(ActionEvent e)
          {
            enableShowMaskImageButton_actionPerformed(e, currentSubtypeForMask);
          }
        });
         
        //Siew Yeng - XCR-2764 - Sub-subtype feature
        JPanel subRegionPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JLabel subRegionLabel = new JLabel(StringLocalizer.keyToString("ATGUI_SUB_REGION_LABEL_KEY"));
        
        _addOrRemoveSubRegionButton = new JButton();
        _addOrRemoveSubRegionButton.setText(StringLocalizer.keyToString("ATGUI_ADD_SUB_REGION_BUTTON_KEY"));
        
        setEnabledSubRegion(currentSubtypeChoice.getSubtype());

        enableCheckBoxPanel.add(_addOrRemoveSubRegionButton);
        enableCheckBoxPanel.add(subRegionPanel);
        subRegionPanel.add(subRegionLabel);
        subRegionPanel.add(_subRegionComboBox);
        
        populateSubSutypeComboBox(currentSubtype);

        _addOrRemoveSubRegionButton.addActionListener(new java.awt.event.ActionListener() 
        {
          public void actionPerformed(ActionEvent e) 
          {
            addSubRegionButton_actionPerformed(e);
          }
        });
        _subRegionComboBox.addActionListener(_subRegionComboBoxActionListener);
      }

      if (threshTableModel.getRowCount() > 0)  // don't add the table if there are no thresholds
      {
        JPanel tablePanel = new JPanel();
        BorderLayout tablePanelBorderLayout = new BorderLayout();
        tablePanel.setLayout(tablePanelBorderLayout);
        JScrollPane jsp = new JScrollPane();

        //_thresholdsTabbedPane.setSelectedComponent(jsp);
        JTable tt = new JTable(threshTableModel);
        jsp.getViewport().add(tt);
        tablePanel.add(jsp, BorderLayout.CENTER);
        if (algoEnum.equals(AlgorithmEnum.MEASUREMENT) == false)
        {
          tablePanel.add(enableCheckBoxPanel, BorderLayout.NORTH);
        }
        _thresholdsTabbedPane.add(tablePanel, algoEnum.toString());
        setupTableCustomizations(currentSubtypeChoice.getSubtype(), tt);
        setupTableEditorsAndRenderers(algorithm, currentSubtypeChoice.getSubtype(), tt);
        tt.changeSelection(0, 1, false, false);
        threshTableModel.setTestRunning(_inRunMode);
      }
      else //This is the NO THRESHOLDS case. For Standard, add the tab with a "No Thresholds" message. For Additional, do not add the tab
      {
        JLabel label = new JLabel(StringLocalizer.keyToString("ATGUI_NO_THRESHOLDS_KEY"));
        JPanel panel = new JPanel();
        BorderLayout panelBorderLayout = new BorderLayout();
        panel.setLayout(panelBorderLayout);
        if (algoEnum.equals(AlgorithmEnum.MEASUREMENT) == false)
        {
          panel.add(enableCheckBox, BorderLayout.NORTH);
        }
        JPanel innerPanel = new JPanel();
        innerPanel.setBorder(new EtchedBorder());
        innerPanel.add(label);
        panel.add(innerPanel, BorderLayout.CENTER);
        _thresholdsTabbedPane.add(panel, algoEnum.toString());
      }

      if (enableCheckBox.isSelected() == false)
      {
        _thresholdsTabbedPane.setForegroundAt(algorithms.indexOf(algorithm), _defaultTabTextColor);
      }
      else
      {
        Color normalColor = _thresholdsTabbedPane.getForeground();
        _thresholdsTabbedPane.setForegroundAt(algorithms.indexOf(algorithm), normalColor);
      }
    }

    _thresholdsTabbedPane.requestFocus();
    //_thresholdsTabbedPane.setSelectedIndex(0);  // reset to the first tab
    // reset the description to the selected TAB, this was reset by the last table as it
    // was created because I do a tt.changeSelection above.

    _currentSelectedAlgorithmTab = _thresholdsTabbedPane.getSelectedIndex();
    if (_currentSelectedAlgorithmTab < 0)  // no tabs
      return;
//    for (int i = 0; i < _thresholdsTabbedPane.getTabCount(); i++)
//    {
//      _thresholdsTabbedPane.setBackgroundAt(i, _defaultTabColor);
//      _thresholdsTabbedPane.setForegroundAt(i, _defaultTabTextColor);
//    }
//    _thresholdsTabbedPane.setBackgroundAt(_currentSelectedTab, _activeTabColor);
//    _thresholdsTabbedPane.setForegroundAt(_currentSelectedTab, _activeTabTextColor);

    if (doesCurrentTableExist())
    {
      JTable table = findCurrentTable();
      updateThresholdDescription(currentSubtypeChoice.getSubtype(), table, table.getSelectedRow());
    }

    _mainUI.validate();
  }

  /**
   * @author Andy Mechtenberg
   */
  private boolean doesCurrentTableExist()
  {
    // now, we need to select the ROW that this threshold is on
    Component comp = _thresholdsTabbedPane.getSelectedComponent();
    Assert.expect(comp instanceof JPanel);
    JPanel compPanel = (JPanel)comp;
    Component[] componentArray = compPanel.getComponents();
    for (int compIndex = 0; compIndex < componentArray.length; compIndex++)
    {
      Component potentialComp = componentArray[compIndex];
      if (potentialComp instanceof JScrollPane)
      {
        JScrollPane jsp = (JScrollPane)potentialComp;
        Component comp1 = (JTable)jsp.getViewport().getView();
        if (comp1 == null)
          return false;
        else
        {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  private JTable findCurrentTable()
  {
    JTable table = null;
  // now, we need to select the ROW that this threshold is on

    Component comp = _thresholdsTabbedPane.getSelectedComponent();
    Assert.expect(comp instanceof JPanel);
    JPanel compPanel = (JPanel)comp;
    Component[] componentArray = compPanel.getComponents();
    for (int compIndex = 0; compIndex < componentArray.length; compIndex++)
    {
      Component potentialComp = componentArray[compIndex];
      if (potentialComp instanceof JScrollPane)
      {
        JScrollPane jsp = (JScrollPane)potentialComp;
        Component comp1 = (JTable)jsp.getViewport().getView();
        if (comp1 == null)
          table = null;
        else
        {
          table = (JTable)comp1;
        }
      }
    }
    Assert.expect(table != null);
    return table;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void enableAlgorithmCheckBox_actionPerformed(ActionEvent e)
  {
    // only the visible algorithm can have this action listener activated.
    int selectedIndex = _thresholdsTabbedPane.getSelectedIndex();
    String tabName = _thresholdsTabbedPane.getTitleAt(selectedIndex);
    JCheckBox checkBox = (JCheckBox)e.getSource();
    AlgorithmEnum algorithmEnum = AlgorithmEnum.getAlgorithmEnum(tabName);
    SubtypeChoice currentSubtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    Subtype currentSubtype = currentSubtypeChoice.getSubtype();

    SubtypeSetAlgorithmEnabledCommand setCommand = new SubtypeSetAlgorithmEnabledCommand(currentSubtype, algorithmEnum, checkBox.isSelected());
    try
    {
      _commandManager.trackState(getCurrentUndoState(-1));  // no rows for this type of command
      _commandManager.execute(setCommand); //, new TestDevChangeScreenCommand(_testDev, TestDevScreenEnum.PANEL_SETUP));
    }
    catch (XrayTesterException ex)
    {
      Assert.logException(ex);
    }

    if (checkBox.isSelected() == false)
    {
      _thresholdsTabbedPane.setForegroundAt(selectedIndex, _defaultTabTextColor);
    }
    else
    {
      Color normalColor = _thresholdsTabbedPane.getForeground();
      _thresholdsTabbedPane.setForegroundAt(selectedIndex, normalColor);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setThresholdTablesToEmpty()
  {
    _standardThresholdsRadioButton.setSelected(true); // revert to standard because we're changing to a new jointType/subtype
    _standardThresholdsRadioButton.setEnabled(false);
    _advancedThresholdsRadioButton.setEnabled(false);
    JLabel label = new JLabel(StringLocalizer.keyToString("ATGUI_MUST_HAVE_FAMILY_SUBTYPE_KEY"));
    JPanel panel = new JPanel();
    panel.add(label);
    _thresholdsTabbedPane.add(panel, StringLocalizer.keyToString("ATGUI_NO_THRESHOLDS_KEY"));

    _descriptionEditorPane.setContentType("text/html");
    setThresholdDescriptionText(StringLocalizer.keyToString("ATGUI_NO_THRESHOLD_DESCRIPTION_KEY"));

    _imageLabel.setIcon(null);
    _moreInfoButton.setEnabled(false);
    _thresholdCommentTextField.setText("");
    _thresholdCommentTextField.setEnabled(false);  // disable the control, can't set a threshold comment if no comment
    //XCR-1169 desc
    _thresholdSimpleDescriptionValue.setEnabled(false);
    _thresholdSimpleDescriptionValue.setText("");
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setupTableCustomizations(final Subtype subtype, final JTable tt)
  {
    tt.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    for (int i = 0; i < tt.getColumnCount(); i++)
    {
//      System.out.println("SETUP: Setting table column " + i + " to width " + _tunerPersistSettings.getTableColumnWidth(i));
      tt.getColumnModel().getColumn(i).setPreferredWidth(_tunerPersistSettings.getTableColumnWidth(i));
    }

    tt.getColumnModel().addColumnModelListener(getThresholdTableColumnModelListener(tt));
    tt.getTableHeader().setReorderingAllowed(false);

//    tt.setRowSelectionAllowed(false);
//    tt.setColumnSelectionAllowed(false);

    // add the listener for table column selection
    ListSelectionModel colSM = tt.getColumnModel().getSelectionModel();
    colSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        //ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        //int selectedColumn = lsm.getMinSelectionIndex();
        tt.setColumnSelectionInterval(ThresholdTableModel.VALUE_COLUMN, ThresholdTableModel.VALUE_COLUMN);  // don't allow any column but 1 -- the value column
      }
    });

    // add the listener for a table row selection
    ListSelectionModel rowSM = tt.getSelectionModel();
    rowSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        //System.out.println("List Value Changed changed");
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();

        if (lsm.isSelectionEmpty() || lsm.getValueIsAdjusting())
        {
          // do nothing
        }
        else
        {
          int selectedRow = lsm.getMinSelectionIndex();
          //System.out.println("List Selection Changed To Row: " + selectedRow);
          //CustomThresholdEditor tce = (CustomThresholdEditor)tt.getColumn(tt.getModel().getColumnName(1)).getCellEditor();
          tt.changeSelection(selectedRow, ThresholdTableModel.VALUE_COLUMN, false, false);
          updateThresholdDescription(subtype, tt, selectedRow);
          if (!tt.isEditing())
          {
            tt.editCellAt(selectedRow, ThresholdTableModel.VALUE_COLUMN);
            Component comp = tt.getEditorComponent();
            // do a request focus on the cell editor lets the default behaviour of selection all the text happen
            if (comp instanceof JButton)
            {
              tt.getCellEditor(selectedRow, ThresholdTableModel.VALUE_COLUMN).stopCellEditing();
            }
            if (comp instanceof JTextField)
              comp.requestFocus();
          }
        }
      }
    });

    tt.addMouseListener(new MouseAdapter()
    {
      // show context menu on right click
      public void mouseReleased(MouseEvent e)
      {
        if (e.isPopupTrigger())
        {
          handleThresholdTablePopupMenu(e, tt);
        }
      }

      public void mouseClicked(MouseEvent e)
      {
        int clickCount = e.getClickCount();
        if (clickCount == 2 && e.isPopupTrigger() == false)
        {
          // do nothing if we're not in the Measurements Tab (tab 2)
          int selectedTab = _tunerTaskPane.getSelectedIndex();
          if (selectedTab < 0 || selectedTab < 2)
            return;
          if (doesCurrentAlgorithmExist() &&   // make sure we're not in slice heights -- no measurements for that. (it's not a "real" algorithm)
              (_thresholdsTabbedPane.getSelectedIndex() != getTabIndexForTabTitle(StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY"))))
            sendInformationAboutThresholdMeasurement(tt);
        }
      }

    });
  }

  /**
   * @author Andy Mechtenberg, George A. David
   */
  private void setupTableEditorsAndRenderers(Algorithm algorithm, final Subtype subtype, final JTable tt)
  {
    Assert.expect(subtype != null);
    Assert.expect(tt != null);

    int rows = tt.getRowCount();
    final CustomThresholdEditor rowEditor = new CustomThresholdEditor(tt);
    tt.getColumn(tt.getModel().getColumnName(1)).setCellEditor(rowEditor);

    for (int i = 0; i < rows; i++)
    {
//      String threshName = "a";
      final AlgorithmSetting algorithmSetting = (AlgorithmSetting)tt.getModel().getValueAt(i, 0);  // 0 column is the AlgorithmSetting object
      Object value = subtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum());
      Assert.expect(value != null);
      if (value instanceof Integer)
      {
        //System.out.println("Integer");
        final Integer defaultVal = (Integer)subtype.getAlgorithmSettingDefaultValue(algorithmSetting.getAlgorithmSettingEnum());
        final IntegerNumberField integerField = new IntegerNumberField(0, 5);
        DefaultCellEditor integerEditor = new DefaultCellEditor(integerField)
        {
          public Object getCellEditorValue()
          {
            int intvalue = integerField.getValue();
            if (intvalue == IntegerNumberField.EMPTY_VALUE)
            {
              intvalue = defaultVal.intValue();
            }
            return new Integer(intvalue);
            //return new Integer(integerField.getValue());
          }
        };
        integerEditor.setClickCountToStart(1);
        integerField.addFocusListener(new java.awt.event.FocusAdapter()
        {
          public void focusGained(FocusEvent e)
          {
           integerField.selectAll();
          }
          public void focusLost(FocusEvent e)
          {
            // This was an experiment to handle full keyboard navigation.  Didn't work, but I'll keep the code here
            // commented out to record what I tried.
//            if(e.isTemporary())
//              return;
//            System.out.println("Lost Focus @ " + e.paramString());
            //This focus listener was added to catch the event of losing
            //focus when another part of the GUI is clicked on, say the Run button.
            //However, this is also called when the table loses focus, but that is already
            //handled correctly by swing. The problem arises when using the up/down arrow
            //keys to navigate the table. When the key is pressed, Swing tells the cell
            //to stop editing and focuses the next cell and begins editing it. Then
            //this also gets called because the previous cell lost focus. At this
            //point though, Swing has already correctly handled the event. If we allow
            //it to continue, this code will stop the new cell from editing  This is
            //not the behavior we want. We only want to run this code if this FocusEvent was
            //initiated by another component.
            //George A. David
            if (tt.isEditing() && e.getOppositeComponent() != tt)
              rowEditor.stopCellEditing();
          }
        });
        rowEditor.setEditorAt(i, integerEditor);
      }
      else if (value instanceof Float)
      {
        final Float defaultVal = (Float)subtype.getAlgorithmSettingDefaultValue(algorithmSetting.getAlgorithmSettingEnum());
        final FloatNumberField floatField = new FloatNumberField(0, 5);
        DefaultCellEditor floatEditor = new DefaultCellEditor(floatField)
        {
          public Object getCellEditorValue()
          {
            float floatvalue = floatField.getValue();
            if (floatvalue == FloatNumberField.EMPTY_VALUE)
            {
              floatvalue = defaultVal.floatValue();
              MeasurementUnitsEnum measurementUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(algorithmSetting.getAlgorithmSettingEnum());
              if (measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS) &&
                  Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
              {
                // convert from mils (mils is what the user has input)
                floatvalue = MathUtil.convertMillimetersToMils(floatvalue);
              }
            }
            return new Float(floatvalue);
            //return new Float(floatField.getValue());
          }
        };
        floatEditor.setClickCountToStart(1);
        floatField.addFocusListener(new java.awt.event.FocusAdapter()
        {
          public void focusGained(FocusEvent e)
          {
            floatField.selectAll();
          }
          public void focusLost(FocusEvent e)
          {
//            System.out.println("Lost Focus: " + e.paramString());
            //This focus listener was added to catch the event of losing
            //focus when another part of the GUI is clicked on, say the Run button.
            //However, this is also called when the table loses focus, but that is already
            //handled correctly by swing. The problem arises when using the up/down arrow
            //keys to navigate the table. When the key is pressed, Swing tells the cell
            //to stop editing and focuses the next cell and begins editing it. Then
            //this also gets called because the previous cell lost focus. At this
            //point though, Swing has already correctly handled the event. If we allow
            //it to continue, this code will stop the new cell from editing  This is
            //not the behavior we want. We only want to run this code if this FocusEvent was
            //initiated by another component.
            //George A. David
            if (tt.isEditing() && e.getOppositeComponent() != tt)
              rowEditor.stopCellEditing();
          }
        });
        // This code does not work, because I think of a bug in Swing
        // I want to leave it here, if they fix the swing bug, I can uncomment it.
//        floatField.addKeyListener(new java.awt.event.KeyAdapter()
//        {
//          public void keyTyped(KeyEvent e)
//          {
//            System.out.println("---- KEY TYPED!");
//          }
//          public void keyPressed(KeyEvent e)
//          {
//            int selectedRow = tt.getEditingRow();
//            System.out.println("Double Editor --- Editing row is " + selectedRow);
//            System.out.println("Double Editor --- user PRESSED a key");
//            if (e.getKeyCode() == KeyEvent.VK_ENTER)
//            {
//              System.out.println("Double Editor --- user PRESSED ENTER");
//              if (tt.isEditing())
//              {
//                System.out.println("Double Editor --- Stop Cell Editing");
//                rowEditor.stopCellEditing();
//                tt.changeSelection(selectedRow+1, 1, false, false);
//                //tt.editCellAt(selectedRow+1, 1);
//              }
//              else
//                System.out.println("Double Editor == tt was NOT editing");
//            }
//          }
//        });
        rowEditor.setEditorAt(i, floatEditor);
      }
      else if (value instanceof Boolean)
      {
        // use a combo box with "Yes" and "No" in it
//        final JComboBox comboBox = new JComboBox();
        final VariableWidthComboBox comboBox = new VariableWidthComboBox();
        comboBox.setPopupWidthToDefault();
        // set the popup width to the max of the strings only if that's longer than the box itself
        java.util.List<String> availableValues = new ArrayList<String>();
        availableValues.add(StringLocalizer.keyToString("GUI_YES_BUTTON_KEY"));
        availableValues.add(StringLocalizer.keyToString("GUI_NO_BUTTON_KEY"));
        FontMetrics fontMetrics = comboBox.getFontMetrics(comboBox.getFont());
        int maxStringLength = 0;
        for (String valueString : availableValues)
        {
          int itemStringLength = fontMetrics.stringWidth(valueString);
          if (maxStringLength < itemStringLength)
            maxStringLength = itemStringLength;
        }
        if ((maxStringLength + VariableWidthComboBox.widthExtension) <= comboBox.getWidth())
          comboBox.setPopupWidthToDefault();
        else
          comboBox.setPopupWidth(maxStringLength + VariableWidthComboBox.widthExtension); // add a bit more so the last char is not RIGHT at the popup right side.

        comboBox.addItem(StringLocalizer.keyToString("GUI_YES_BUTTON_KEY"));
        comboBox.addItem(StringLocalizer.keyToString("GUI_NO_BUTTON_KEY"));
        comboBox.getInputMap().put(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true), "enableComboBox");
        comboBox.getActionMap().put("enableComboBox", new EnableComboBoxAction());
        rowEditor.setEditorAt(i, new DefaultCellEditor(comboBox));
      }
      else if ((value instanceof String) || (value instanceof java.util.List))  // must be string value
      {
//        if (_panelDataAdapter.areMultipleThresholdValuesAllowed(algoEnum, threshName))
//        {
//          // create button linked to a dialog
//          rowEditor.setEditorAt(i, new ThresholdButtonEditor(new JCheckBox(), _frame, _panelDataAdapter, algoEnum, threshName));
//
//          CustomThresholdRenderer dlgRenderer = new CustomThresholdRenderer(algoEnum, _panelDataAdapter);
//          CustomThresholdRenderer defaultDlgRenderer = new CustomThresholdRenderer(algoEnum, _panelDataAdapter);
//          ThresholdButtonRenderer buttonRenderer = new ThresholdButtonRenderer(algoEnum, threshName, _panelDataAdapter);
//          ThresholdButtonRenderer defaultButtonRenderer = new ThresholdButtonRenderer(algoEnum, threshName, _panelDataAdapter);
//          dlgRenderer.add(i, buttonRenderer);
//          defaultDlgRenderer.add(i, defaultButtonRenderer);
//          tt.getColumn(StringLocalizer.keyToString("ATGUI_THRESHOLD_TABLE_VALUE_KEY")).setCellRenderer(dlgRenderer);
//          tt.getColumn(StringLocalizer.keyToString("ATGUI_THRESHOLD_TABLE_DEFAULT_KEY")).setCellRenderer(defaultDlgRenderer);
//        }
//        else
//        {
          // create combo box for this cell's editor
          java.util.List<String> availableValues = (java.util.List<String>)subtype.getAlgorithmSettingValuesList(algorithmSetting.getAlgorithmSettingEnum());
          final VariableWidthComboBox comboBox = new VariableWidthComboBox(availableValues.toArray());
          
          //Siew Yeng - XCR-2764 - remove variable pad method for subSubtype
          if(algorithm.getAlgorithmEnum().equals(AlgorithmEnum.VOIDING) &&
             subtype.getInspectionFamilyEnum().equals(InspectionFamilyEnum.LARGE_PAD) && 
             algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_METHOD) &&
             subtype.isSubSubtype())
          {
            comboBox.removeItem(LargePadVoidingAlgorithm._VARIABLE_PAD_METHOD);
          }
          comboBox.setPopupWidthToDefault();
          // set the popup width to the max of the strings only if that's longer than the box itself
          FontMetrics fontMetrics = comboBox.getFontMetrics(comboBox.getFont());
          int maxStringLength = 0;
          for (String valueString : availableValues)
          {
            int itemStringLength = fontMetrics.stringWidth(valueString);
            if (maxStringLength < itemStringLength)
              maxStringLength = itemStringLength;
          }
          if ((maxStringLength + VariableWidthComboBox.widthExtension) <= comboBox.getWidth())
            comboBox.setPopupWidthToDefault();
          else
            comboBox.setPopupWidth(maxStringLength + VariableWidthComboBox.widthExtension); // add a bit more so the last char is not RIGHT at the popup right side.

//          final JComboBox comboBox = new JComboBox(availableValues.toArray());
          comboBox.getInputMap().put(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true), "enableComboBox");
          comboBox.getActionMap().put("enableComboBox", new EnableComboBoxAction());
          rowEditor.setEditorAt(i, new DefaultCellEditor(comboBox));
//        }
      }
      else
        Assert.expect(false);
    }
    tt.setDefaultRenderer(Object.class, new CustomThresholdRenderer(algorithm, subtype));
  }

  /**
   * Updates the threshold description, image and user comment (if any)
   * @author Andy Mechtenberg
   */
  private void updateThresholdDescription(Subtype subtype, JTable tt, int selectedRow)
  {
    if (selectedRow < 0)  // no selection, so put the empty threshold message
    {
      _descriptionEditorPane.setContentType("text/html");
      setThresholdDescriptionText(StringLocalizer.keyToString("ATGUI_NO_THRESHOLD_DESCRIPTION_KEY"));

      _imageLabel.setIcon(null);
      _moreInfoButton.setEnabled(false);
      _thresholdSimpleDescriptionValue.setText("");
      _thresholdCommentTextField.setText("");
      _thresholdCommentTextField.setEnabled(false);  // disable the control, can't set a threshold comment if no comment
      return;
    }
    AlgorithmSetting algorithmSetting = (AlgorithmSetting)tt.getModel().getValueAt(selectedRow, 0);  // 0 column is the AlgorithmSetting object
    //System.out.println("Updating Threshold Description for " + threshName);
    if (subtype.doesAlgorithmSettingDescriptionURLExist(algorithmSetting.getAlgorithmSettingEnum()))
    {
      URL threshDesc = subtype.getAlgorithmSettingDescriptionURL(algorithmSetting.getAlgorithmSettingEnum());
      if (FileUtilAxi.exists(threshDesc.getFile()))
      {
        _descriptionEditorPane.setContentType("text/html");
        try
        {
          setThresholdDescriptionPage(threshDesc);
        }
        catch (XrayTesterException ex1)
        {
          _descriptionEditorPane.setContentType("text/html");
          setThresholdDescriptionText("Threshold does NOT have a current HTML description file:<br> <b>" + threshDesc.getFile() + "</b>");
        }
      }
      else
      {
        _descriptionEditorPane.setContentType("text/plain");
        setThresholdDescriptionText("Threshold does description file does not exist: " + threshDesc.getFile());
      }
    }
    else
    {
      _descriptionEditorPane.setContentType("text/plain");
      setThresholdDescriptionText("Threshold does NOT have a valid key defined for its description.");
    }

    if (subtype.doesAlgorithmSettingImageURLExist(algorithmSetting.getAlgorithmSettingEnum()))
    {
      URL imageURL = subtype.getAlgorithmSettingImageURL(algorithmSetting.getAlgorithmSettingEnum());
      ImageIcon imageIcon = new ImageIcon(imageURL);
      _imageLabel.setIcon(imageIcon);
    }

    if (subtype.doesAlgorithmSettingDetailedDescriptionURLExist(algorithmSetting.getAlgorithmSettingEnum()))
    {
      // get the contents to see if the target exists
      URL detailedThreshDesc = subtype.getAlgorithmSettingDetailedDescriptionURL(algorithmSetting.getAlgorithmSettingEnum());
      try
      {
        Object content = detailedThreshDesc.getContent();  // if there is content, enable the button.  The catch handles the no content case
        _moreInfoButton.setEnabled(true);
      }
      catch (IOException ex)
      {
        _moreInfoButton.setEnabled(false);  // we have no more info for this threshold
      }

    }
    else
      _moreInfoButton.setEnabled(false);

    // lastly, do the user threshold comment
    _thresholdCommentTextField.setEnabled(true);
    _thresholdCommentTextField.setText(subtype.getAlgorithmSettingUserComment(algorithmSetting.getAlgorithmSettingEnum()));
    //XCR-1169 Threshold Description
    _thresholdSimpleDescriptionValue.setEnabled(true);
    _thresholdSimpleDescriptionValue.setText(EnumDescriptionStringLookup.getInstance().getAlgorithmSettingString(algorithmSetting.getAlgorithmSettingEnum()));
  }

  /**
   * @author Andy Mechtenberg
   */
  private void changeFrontPanelControls(boolean isTestRunning)
  {
    // enable/disable tabbed panel
    _tunerTaskPane.setEnabled(isTestRunning == false);
    // enable/disable combo boxes
    _testModeSelectComboBox.setEnabled(isTestRunning == false);
    _componentComboBox.setEnabled(isTestRunning == false);
    _padComboBox.setEnabled(isTestRunning == false);
    _jointTypeComboBox.setEnabled(isTestRunning == false);
    _subtypeComboBox.setEnabled(isTestRunning == false);
    _boardSelectComboBox.setEnabled(isTestRunning == false);
    // enable/disable subtype comment box as appropriate
    SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    if ((subtypeChoice != null) && (subtypeChoice.isAllSubtypes() == false))  // if not equal to "All"
    {
      _subtypeCommentTextField.setEnabled(isTestRunning == false);
      _thresholdCommentTextField.setEnabled(isTestRunning == false);
      _thresholdSimpleDescriptionValue.setEnabled(isTestRunning == false);

      setEnabledSubRegion(subtypeChoice.getSubtype());
    }
    else
    {
      _subtypeCommentTextField.setEnabled(false);
      _thresholdCommentTextField.setEnabled(false);
      _thresholdSimpleDescriptionValue.setEnabled(false);
    }
    // also turn off editing of any threshold tables here
    // find all the table models currently there, and send them the is not editable message
    for (int i = 0; i < _thresholdsTabbedPane.getTabCount(); i++)
    {
      Component comp = _thresholdsTabbedPane.getComponentAt(i);
      Assert.expect(comp instanceof JPanel);
      JPanel compPanel = (JPanel)comp;
      Component[] componentArray = compPanel.getComponents();
      for (int compIndex = 0; compIndex < componentArray.length; compIndex++)
      {
        Component potentialComp = componentArray[compIndex];
        if (potentialComp instanceof JScrollPane)
        {
          JScrollPane jsp = (JScrollPane)potentialComp;
          Component comp1 = jsp.getViewport().getView();
          if (comp1 instanceof JTable)
          {
            JTable table = (JTable)comp1;
            ThresholdTableModel ttm = (ThresholdTableModel)table.getModel();
            TableCellEditor tce = table.getCellEditor();
            if (tce != null)
              tce.stopCellEditing();

            ttm.setTestRunning(_inRunMode);
          }
        }
      }
    }

    // If we are re-enabling the run description text field, then update it's contents.  During a test,
    // this field is disabled, so I don't update it.  After the test is over, the user may have browsed around, so
    // let's update it.
    if (isTestRunning == false)
    {
      notifyOfTestSelection();
    }
    if (isTestRunning)
    {
//      _mainUI.disableMenuAndToolBars();
      _testDev.disableTaskPanelNavigation();
    }
    else
    {
//      _mainUI.enableMenuAndToolBars();
      _testDev.enableTaskPanelNavigation();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void moreInfoButton_actionPerformed(ActionEvent e)
  {
    AlgorithmSetting algorithmSetting = getCurrentThreshold();
    SubtypeChoice currentSubtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();

    if (doesCurrentAlgorithmExist() == false)
      return;

    final URL detailedThreshDesc = currentSubtypeChoice.getSubtype().getAlgorithmSettingDetailedDescriptionURL(algorithmSetting.getAlgorithmSettingEnum());

    // Since these dialog are non-modal, we need to keep track of them.  If one for a URL is already up,
    // and the more info button is pressed, we don't want to create a new one, just bring the existing one to front
    // check to see if the dialog is already up, by looking for it in the map
    JDialog oldDlg = (JDialog)_detailedDescDialogMap.get(detailedThreshDesc);
    if (oldDlg == null)  // it's not already up, so bring it up
    {
      // bring up the url in a non-modal dialog
      JEditorPane textarea = null;
      final JDialog dlg = new JDialog(_mainUI, algorithmSetting.getName(), false);

      dlg.getContentPane().setLayout(new BorderLayout());
      try
      {
        textarea = new JEditorPane(detailedThreshDesc);
      }
      catch (IOException ioex)
      {
        // page must not have existed.  return.
        return;
      }

      textarea.setEditable(false);

      // do this so the return key exits the dialog.  The JEditorPane grabs all keyboard input.
      textarea.addKeyListener(new java.awt.event.KeyAdapter()
      {
        public void keyPressed(KeyEvent ke)
        {
          if (ke.getKeyCode() == KeyEvent.VK_ENTER)
            dlg.dispose();
        }
      });
      JScrollPane jsp = new JScrollPane(textarea);

      jsp.setPreferredSize(new Dimension(500, 300));
      dlg.getContentPane().add(jsp, BorderLayout.CENTER);
      JButton okButton = new JButton(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
      okButton.setPreferredSize(new Dimension(73, 27));
      okButton.setMnemonic(StringLocalizer.keyToString("GUI_OK_BUTTON_MNEMONIC_KEY").charAt(0));
      okButton.addActionListener(new java.awt.event.ActionListener()
      {
        public void actionPerformed(ActionEvent ae)
        {
          dlg.dispose();
        }
      });
      dlg.addWindowListener(new WindowAdapter()
      {
        public void windowClosed(WindowEvent we)
        {
          //System.out.println("Closed!");
          _detailedDescDialogMap.remove(detailedThreshDesc);
        }
      });
      JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
      southPanel.add(okButton);
      dlg.getContentPane().add(southPanel, BorderLayout.SOUTH);
      dlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

      dlg.pack();
      SwingUtils.centerOnComponent(dlg, _mainUI);
      textarea.setCaretPosition(0);  // this will scroll to top
//      okButton.setNextFocusableComponent(okButton);
//      okButton.requestFocus();
      dlg.getRootPane().setDefaultButton(okButton);
      _detailedDescDialogMap.put(detailedThreshDesc, dlg);
      dlg.setVisible(true);
    }
    else // dialog alreay up
    {
      oldDlg.toFront();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void showDescriptionToggleButton_actionPerformed()
  {
    if (_showDescriptionToggleButton.isSelected())
    {
      _thresholdDescriptionPanel.add(_totalDescriptionPanel, BorderLayout.CENTER);
    }
    else // hide _totalDescriptionPanel
    {
      _thresholdDescriptionPanel.remove(_totalDescriptionPanel);
    }
    _mainUI.validate();
    _mainUI.repaint();
  }
  
  /**
   * @author Wei Chin
   */
  private void thresholdSimpleDescriptionButton_actionPerformed()
  {
    SubtypeChoice currentSubtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    // first capture the current tab's table column widths, and set them to the persistance
    // then set the new tab to those widths
    int selectedTab = _thresholdsTabbedPane.getSelectedIndex();
    if (selectedTab < 0)  // no tabs
      return;

    if (doesCurrentTableExist())
    {
      // Wei Chin > XCR-3267 : Assert when click on CTRL+F2 without select a subtype first
      Subtype subtype = currentSubtypeChoice.getSubtype();

      JTable table = findCurrentTable();
      AlgorithmSetting algorithmSetting = (AlgorithmSetting)table.getModel().getValueAt(table.getSelectedRow(), 0);  // 0 column is the AlgorithmSetting object
      //System.out.println("Updating Threshold Description for " + threshName);
      if (subtype.doesAlgorithmSettingDescriptionURLExist(algorithmSetting.getAlgorithmSettingEnum()))
      {
        URL threshDesc = subtype.getAlgorithmSettingDescriptionURL(algorithmSetting.getAlgorithmSettingEnum());
        try
        {
          BrowserLauncher.openURLInNewWindow(threshDesc);
        }
        catch(Exception e)
        {
          // do nothing
        }      
      }
    } 
  }
  
  /**
   * @author weng-jian.eoh
   */
  private void validateDisabledAlgoButton_actionPerformed()
  {
    ValidateDisabledAlgorithmsDialog dlg = new ValidateDisabledAlgorithmsDialog(_mainUI,
      StringLocalizer.keyToString("ATGUI_VALIDATE_DISABLED_ALGO_TITLE_KEY"),
      true);
    dlg.populateDialogWithData(_currentBoardType);
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
  }
  
  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  private void viewMeasurementJointHighlightButton_actinPerformed()
  {
    java.util.List<String> defectiveComponentList = null;
    try
    {
      MeasurementJointHighlightSettingsReader reader = new MeasurementJointHighlightSettingsReader(_project.getName());
      defectiveComponentList = reader.getDefectiveSetting();
    }
    catch (DatastoreException e)
    {
      MessageDialog.showErrorDialog(_mainUI,
        e.getLocalizedMessage(),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
      return;
    }
    ViewDefectiveComponentJointDialog dlg = new ViewDefectiveComponentJointDialog(_mainUI,
      StringLocalizer.keyToString("ATGUI_VIEW_DEFECTIVE_COMPONENT_TITLE_KEY"), true);

    dlg.populateDialogWithData(defectiveComponentList);
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    
    ReviewMeasurementsPanel panel = (ReviewMeasurementsPanel) _reviewMeasurementsPanel;
    panel.repopulate();

  }
  
  /**
   * @author Andy Mechtenberg
   */
  private void copyThresholdsButton_actionPerformed()
  {
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();

    boolean currentAlgoExists = doesCurrentAlgorithmExist();
    // find the current threshold, if any
    AlgorithmSetting algorithmSetting = getCurrentThreshold();
    // if any of these are not specified, we cannot copy, so end with an error
    if ((jointTypeChoice == null) || (subtypeChoice == null) || (currentAlgoExists == false))
    {
      JOptionPane.showMessageDialog(_mainUI, StringLocalizer.keyToString("ATGUI_UNABLE_TO_COPY_THRESHOLDS_INCOMPLETE_DATA_KEY"),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"), JOptionPane.ERROR_MESSAGE);
      return;
    }
    if (algorithmSetting == null)
    {
      JOptionPane.showMessageDialog(_mainUI, StringLocalizer.keyToString("ATGUI_UNABLE_TO_COPY_THRESHOLDS_NO_THRESHOLD_SELECTED_KEY"),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"), JOptionPane.ERROR_MESSAGE);
      return;
    }
    // Also, if there is only 1 subtype for this jointType, there can be no copying
    // this is a separate check so we can output a different error message
    int totalSubtypes = _currentBoardType.getInspectedSubtypes(jointTypeChoice.getJointTypeEnum()).size();
    if (totalSubtypes < 2)
    {
      JOptionPane.showMessageDialog(_mainUI, StringLocalizer.keyToString("ATGUI_UNABLE_TO_COPY_THRESHOLDS_ONE_THRESHOLD_KEY"),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"), JOptionPane.ERROR_MESSAGE);
      return;
    }

    // remove the observer here so that when the copy dialog is done we don't change to the copied to subtype
    _projectObservable.deleteObserver(_algoTunerDatastoreObserver);
    CopyThresholdsDlg dlg = null;
    if (_thresholdsTabbedPane.getSelectedIndex() == getTabIndexForTabTitle(StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY")))
    {
      dlg = new CopyThresholdsDlg(_mainUI,
                                  StringLocalizer.keyToString("ATGUI_COPY_THRESHOLDS_DIALOG_TITLE_KEY"),
                                  true,
                                  _panelDataAdapter,
                                  _currentBoardType,
                                  jointTypeChoice.getJointTypeEnum(),
                                  subtypeChoice.getSubtype(),
                                  algorithmSetting);
    }
    else
    {
      dlg = new CopyThresholdsDlg(_mainUI,
                                  StringLocalizer.keyToString("ATGUI_COPY_THRESHOLDS_DIALOG_TITLE_KEY"),
                                  true,
                                  _panelDataAdapter,
                                  _currentBoardType,
                                  jointTypeChoice.getJointTypeEnum(),
                                  subtypeChoice.getSubtype(),
                                  getCurrentAlgorithm(),
                                  algorithmSetting);

    }
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setModal(true);
    dlg.setVisible(true);
    // add the observer back in afer the copy operation is complete
    _projectObservable.addObserver(_algoTunerDatastoreObserver);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateNominalsButton_actionPerformed()
  {
    final SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    final JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    if (subtypeChoice == null)
    {
      JOptionPane.showMessageDialog(_mainUI, StringLocalizer.keyToString("ATGUI_UNABLE_TO_COPY_THRESHOLDS_INCOMPLETE_DATA_KEY"),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"), JOptionPane.ERROR_MESSAGE);
      return;
    }

    if (jointTypeChoice.isAllFamilies())
    {
      // iterate through all joint types, and all subtypes within a joint type
      String choiceMessage = StringLocalizer.keyToString("ATGUI_UPDATE_NOMINALS_ALL_JOINT_TYPE_WARNING_KEY");
      int answer = ChoiceInputDialog.showConfirmDialog(_mainUI,
                                                       StringUtil.format(choiceMessage, 50),
                                                       StringLocalizer.keyToString("ATGUI_UPDATE_NOMINALS_DIALOG_TITLE_KEY"));
      if (answer == JOptionPane.OK_OPTION)
      {
        java.util.List<JointTypeEnum> allJointTypes = _currentBoardType.getInspectedJointTypeEnums();
        for(JointTypeEnum jointTypeEnum : allJointTypes)
        {
          java.util.List<Subtype> subtypes = _currentBoardType.getInspectedSubtypes(jointTypeEnum);
          for (Subtype subtype : subtypes)
          {
            updateNominalsForSubtype(subtype);
          }
        }
        return;
      }
      else
        return;
    }
    else if (subtypeChoice.isAllSubtypes())
    {
      String choiceMessage = StringLocalizer.keyToString("ATGUI_UPDATE_NOMINALS_ALL_SUBTYPE_WARNING_KEY");
      int answer = ChoiceInputDialog.showConfirmDialog(_mainUI,
                                                       StringUtil.format(choiceMessage, 50),
                                                       StringLocalizer.keyToString("ATGUI_UPDATE_NOMINALS_DIALOG_TITLE_KEY"));
      if (answer == JOptionPane.OK_OPTION)
      {
        java.util.List<Subtype> subtypes = _currentBoardType.getInspectedSubtypes(jointTypeChoice.getJointTypeEnum());
        for(Subtype subtype : subtypes)
        {
          updateNominalsForSubtype(subtype);
        }
        return;
      }
      else
        return;
    }
    else
      updateNominalsForSubtype(subtypeChoice.getSubtype());
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateNominalsForSubtype(final Subtype subtype)
  {
    Assert.expect(subtype != null);

    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    // ok, we have a subtype.  We use the StatisticalTuningEngine to update the nominals based on current image set selection
    // we should also have a list of image sets (_selectedImageSets)
    // first, put up a busyDialog (not cancellable), then invoke on a worker thread the update nominals
    LocalizedString msg = new LocalizedString("ATGUI_UPDATE_NOMINAL_PROGRESS_MESSAGE_KEY",
                                              new Object[]{jointTypeEnum, subtype.toString()});
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
                                                             StringLocalizer.keyToString(msg),
                                                             StringLocalizer.keyToString("ATGUI_UPDATE_NOMINALS_DIALOG_TITLE_KEY"),
                                                             true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          // User may want filter by boards. Make sure we only filter board selection here.
          Board selectedBoard = null;
          if (isBoardInstanceSelected())
          {
            selectedBoard = getSelectedBoardInstance();
          }
          
          _statisticalTuningEngine.updateNominals(_project,
                                                  subtype,
                                                  selectedBoard,
                                                  _selectedImageSets);
        }
        catch (final XrayTesterException xte)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              displayError(xte);
            }
          });
        }
        finally
        {
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.dispose();
        }
      }
    });
    busyCancelDialog.setVisible(true);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void componentComboBox_actionPerformed(ActionEvent e)
  {
    Object selectedObject = _componentComboBox.getSelectedItem();
//    Object selectedObject = _componentComboBox.getEditor().getItem();
    if (selectedObject instanceof String) // user typed in something, so now we need to find the componentType, if it exists
    {
      String selectedName = (String)selectedObject;
      //Ngie Xing, XCR-2077, Crash when switch tab from Fine Tuning to other environments
      //make sure that the user does not typed in whitespaces
      if (selectedName.trim().length() == 0)
      {
        //change back to previous selected component
        if (_currentComponentType == null)
          _componentComboBox.setSelectedIndex(1);
        else
          _componentComboBox.setSelectedItem(_currentComponentType);
        return;
      }
      int itemCount = _componentComboBox.getItemCount();
      for (int i = 0; i < itemCount; i++)
      {
        ComponentType comboBoxEntry = (ComponentType)_componentComboBox.getItemAt(i);
        if (comboBoxEntry.toString().equalsIgnoreCase(selectedName))
        {
          _componentComboBox.setSelectedIndex(i);
          return;
        }
      }
      // couldn't find it -- revert to previous selected comp
      if (_currentComponentType == null)
        _componentComboBox.setSelectedIndex(1);
      else
        _componentComboBox.setSelectedItem(_currentComponentType);
      return;
    }
    _currentComponentType = (ComponentType)_componentComboBox.getSelectedItem();
    populatePadComboBox();
    if (_componentComboBox.isShowing())
      setInfoJointTypeSubtype();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void padComboBox_actionPerformed(ActionEvent e)
  {
    if (_padComboBox.isShowing())
      setInfoJointTypeSubtype();
  }

  Object getSelectedPanelOrBoardInstance()
  {
    return _boardSelectComboBox.getSelectedItem();
  }

   /**
   * @author Chin Seong
    * Create a dialog which contain Subtype Advanced Settings
   */
  private void subtypeAdvancedSettingsButton_actionPerformed(ActionEvent e)
  {
    SubtypeAdvanceSettingDialog dlg = null;

    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    
    // get it from config later on
    boolean isVariableMagnification = false;
    
    try
    {
      if(LicenseManager.isVariableMagnificationEnabled())
      {
        isVariableMagnification = true;
      }
    }
    catch(BusinessException be)
    {
      // do nothing
    }
    
    if (jointTypeChoice == null)  // combo box was empty, so we cannot continue
      return;
    else if(jointTypeChoice.isAllFamilies()) //if combo box selected "ALL" joint type
    {
      if (isBoardInstanceSelected()) // check is multiboard or not
      {
       Board selection = getSelectedBoardInstance();
       
       dlg = new SubtypeAdvanceSettingDialog(selection.getInspectedSubtypes(), _currentBoardType, _mainUI,
                                              StringLocalizer.keyToString("LOOP_DLG_FINE_TUNING_SUBTYPE_ADVANCED_SETTINGS_KEY"), isVariableMagnification);
      }
      else  //not multiboard
      {
       dlg = new SubtypeAdvanceSettingDialog(_project.getPanel().getInspectedSubtypes(), _currentBoardType, _mainUI,
                                              StringLocalizer.keyToString("LOOP_DLG_FINE_TUNING_SUBTYPE_ADVANCED_SETTINGS_KEY"), isVariableMagnification);
      }

    }
    else
    {
        SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
        if (subtypeChoice == null)  // combo box was empty, so we cannot continue
          return;
        else if(subtypeChoice.isAllSubtypes()) //all subtype chosen
        {
          dlg = new SubtypeAdvanceSettingDialog(getAllSubtypeInComboBox(), _currentBoardType, _mainUI,
                                                 StringLocalizer.keyToString("LOOP_DLG_FINE_TUNING_SUBTYPE_ADVANCED_SETTINGS_KEY"), isVariableMagnification);
        }
        else //only 1 subtype being choosed
        {
          dlg = new SubtypeAdvanceSettingDialog(subtypeChoice.getSubtype(), _currentBoardType, _mainUI,
                                                 StringLocalizer.keyToString("LOOP_DLG_FINE_TUNING_SUBTYPE_ADVANCED_SETTINGS_KEY"), isVariableMagnification);
        }
    }
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
  }

  /**
   * @author Chin Seong
   */
  private java.util.List<Subtype> getAllSubtypeInComboBox()
  {
    java.util.List<Subtype> subtypes = new ArrayList<Subtype>();

    for(int i = 1; i < _subtypeComboBox.getItemCount(); i ++)
    {
      SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getItemAt(i);
      subtypes.add(subtypeChoice.getSubtype());
    }
    return subtypes;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void boardComboBox_actionPerformed(ActionEvent e)
  {
    //This combo box is queried during an inspection to see the state
    // do nothing for algoTuner but notify other interested parties.
    notifyOfBoardSelection();
  }

  /**
   * @author George Booth
   */
  private void notifyOfBoardSelection()
  {
    Object selectedItem = _boardSelectComboBox.getSelectedItem();

    if (_active && (_sendEvents >= 0) && selectedItem != null)
    {
      _guiObservable.stateChanged(selectedItem, SelectionEventEnum.TUNER_BOARD_SELECTION);
    }
  }


  /**
   * @author Andy Mechtenberg
   */
  boolean isBoardInstanceSelected()
  {
    Object selection = _boardSelectComboBox.getSelectedItem();
    if (selection instanceof Board)
      return true;
    else
      return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  Board getSelectedBoardInstance()
  {
    Object selection = _boardSelectComboBox.getSelectedItem();
    Assert.expect(selection instanceof Board);
    return (Board)selection;
  }

  /**
   * This save the current threshold tables 5 column widths
   * This should only be done if the GUI is visible (or the component) otherwise we'll save
   * bad values
   * @author Andy Mechtenberg
   */
  private void saveCurrentTabWidths(JTable table)
  {
    // obsolete code -- keeping this because it's nice to know how to get at a table if you need to.
//    int numTabs = _thresholdsTabbedPane.getTabCount();
//    int currentSelectedTab = _thresholdsTabbedPane.getSelectedIndex();
//    if ((currentSelectedTab < 0) || (numTabs <= _currentSelectedTab))
//      return;
//    //System.out.println("Save Current Tab Widths: Algo is " + algo);
//    Component comp = _thresholdsTabbedPane.getComponentAt(currentSelectedTab);
//    if (comp instanceof JScrollPane)
//    {
//      JScrollPane jsp = (JScrollPane)comp;
//      Component comp1 = jsp.getViewport().getView();
//      if (comp1 instanceof JTable)
//      {
//        JTable table = (JTable)comp1;
//      }
//    }

    if (table.isValid() == false)
      return;
    for (int i = 0; i < table.getColumnCount(); i++)
    {
//      System.out.println("Setting persist file data for column " + i + " to width " + table.getColumnModel().getColumn(i).getWidth());
      _tunerPersistSettings.setTableColumnWidth(i, table.getColumnModel().getColumn(i).getWidth());
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void tunerTaskPane_stateChanged(ChangeEvent e)
  {
    int selectedTab = _tunerTaskPane.getSelectedIndex();
    if (selectedTab < 0 || selectedTab == _currentSelectedTaskTab)
      return;

    AbstractTunerTaskPanel oldPanel = (AbstractTunerTaskPanel)_tunerTaskPane.getComponentAt(_currentSelectedTaskTab);
    oldPanel.finish();

    AbstractTunerTaskPanel newPanel = (AbstractTunerTaskPanel) _tunerTaskPane.getSelectedComponent();
    newPanel.start();

    _currentSelectedTaskTab = selectedTab;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void thresholdsTabbedPane_stateChanged(ChangeEvent e)
  {
    SubtypeChoice currentSubtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    // first capture the current tab's table column widths, and set them to the persistance
    // then set the new tab to those widths
    int selectedTab = _thresholdsTabbedPane.getSelectedIndex();
    if (selectedTab < 0)  // no tabs
      return;
    //bSystem.out.println("TAB STATE CHANGE");
//    saveCurrentTabWidths();
//    for (int i = 0; i < _thresholdsTabbedPane.getTabCount(); i++)
//    {
//      _thresholdsTabbedPane.setBackgroundAt(i, _defaultTabColor);
//      _thresholdsTabbedPane.setForegroundAt(i, _defaultTabTextColor);
//    }
//
//    _thresholdsTabbedPane.setBackgroundAt(selectedTab, _activeTabColor);
//    _thresholdsTabbedPane.setForegroundAt(selectedTab, _activeTabTextColor);
    //System.out.println("***************  Tabbed Pane State Changed to " + algo);
    if (doesCurrentTableExist())
    {
      JTable table = findCurrentTable();
      updateThresholdDescription(currentSubtypeChoice.getSubtype(), table, table.getSelectedRow());
      for (int i = 0; i < table.getColumnCount(); i++)
      {
//        System.out.println("TAB CHANGE: Setting table column " + i + " to width " + _tunerPersistSettings.getTableColumnWidth(i));
        table.getColumnModel().getColumn(i).setPreferredWidth(_tunerPersistSettings.getTableColumnWidth(i));
      }
    }
    else
    {
      updateThresholdDescription(null, null, -1);
      return;
    }

//    Component comp = _thresholdsTabbedPane.getSelectedComponent();
//    /** @todo APM Cannot just check for scroll pane anymore */
//    if ((comp instanceof JScrollPane) == false)
//    {
//      //System.out.println("Tabbed component is not a scroll pane");
//      updateThresholdDescription(null, null, -1);
//      return;
//    }
//    JScrollPane jsp = (JScrollPane)comp;
//    Component comp1 = (JTable)jsp.getViewport().getView();
//    if (comp1 == null)
//      return;
//    else
//    {
//      JTable tt = (JTable)comp1;
//      int selectedRow = tt.getSelectedRow();
      // commented out because I select the first row when the table is created, so this selection should not be necessary
//      if (selectedRow < 0)
//        tt.addRowSelectionInterval(0, 0);  // select the first Row
//      else
//        tt.changeSelection(selectedRow, 1, false, false);
//      updateThresholdDescription(currentSubtypeChoice.getSubtype(), tt, selectedRow);
//      for (int i = 0; i < tt.getColumnCount(); i++)
//      {
//        System.out.println("TAB CHANGE: Setting table column " + i + " to width " + _tunerPersistSettings.getTableColumnWidth(i));
//        tt.getColumnModel().getColumn(i).setPreferredWidth(_tunerPersistSettings.getTableColumnWidth(i));
//      }
//    }
    _currentSelectedAlgorithmTab = selectedTab;
    
    //Siew Yeng - XCR-2764 - Sub-subtype feature
    if(_currentSelectedAlgorithmTab == getTabIndexForTabTitle(StringLocalizer.keyToString("ATGUI_VOIDING_TAB_TITLE_KEY")))
      populateSubSutypeComboBox(currentSubtypeChoice.getSubtype());
  }

  /**
   * @author Andy Mechtenberg
   */
  private static class EnableComboBoxAction extends AbstractAction
  {
    public void actionPerformed(ActionEvent e)
    {
      JComboBox comboBox = (JComboBox)e.getSource();
      comboBox.requestFocus();
      comboBox.showPopup();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void subtypeCommentTextField_actionPerformed(ActionEvent e)
  {
    updateSubtypeComment();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void subtypeCommentTextField_focusLost(FocusEvent e)
  {
    updateSubtypeComment();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateSubtypeComment()
  {
    SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    if (subtypeChoice == null)
      return;  // If it's null, then that means we haven't even populated yet.
    if (subtypeChoice.isAllSubtypes())
      return;
    String comment = _subtypeCommentTextField.getText();

    String originalComment = subtypeChoice.getSubtype().getUserComment();
    if (originalComment.equals(comment))
      return; // do not set if the same

    _panelDataAdapter.setSubtypeComment(subtypeChoice.getSubtype(), comment);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateThresholdComment()
  {
    if (_thresholdCommentTextField.isEnabled() == false)
      return; // do nothing if not enabled.
    SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    if (doesCurrentAlgorithmExist() == false)
      return;

    AlgorithmSetting algorithmSetting = getCurrentThreshold();
    if (algorithmSetting == null)
      return;
    String comment = _thresholdCommentTextField.getText();
    String originalComment = subtypeChoice.getSubtype().getAlgorithmSettingUserComment(algorithmSetting.getAlgorithmSettingEnum());
    if (originalComment.equals(comment))
      return; // do not set if the same

    subtypeChoice.getSubtype().setAlgorithmSettingUserComment(algorithmSetting.getAlgorithmSettingEnum(), comment);

    //System.out.println("Updating Threshold comment to " + comment);
  }

  /**
   * @author Andy Mechtenberg
   */
  private boolean doesCurrentAlgorithmExist()
  {
    boolean exists = false;

    int selectedTab = _thresholdsTabbedPane.getSelectedIndex();
    if (selectedTab >= 0)  // there are tabs
    {
      String algoName = _thresholdsTabbedPane.getTitleAt(selectedTab);
      if (algoName.equalsIgnoreCase(StringLocalizer.keyToString("ATGUI_NO_THRESHOLDS_KEY")) == false)// &&
//          algoName.equalsIgnoreCase(StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY")) == false)
        exists = true;
    }

    return exists;
  }

  /**
   * @author Andy Mechtenberg
   */
  private Algorithm getCurrentAlgorithm()
  {
    AlgorithmEnum algoEnum = null;

    int selectedTab = _thresholdsTabbedPane.getSelectedIndex();
    if (selectedTab >= 0)  // there are tabs
    {
      String algoName = _thresholdsTabbedPane.getTitleAt(selectedTab);
      if (algoName.equalsIgnoreCase(StringLocalizer.keyToString("ATGUI_NO_THRESHOLDS_KEY")) == false)
        algoEnum = AlgorithmEnum.getAlgorithmEnum(algoName);
    }
    Assert.expect(algoEnum != null);
    SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    Assert.expect(subtypeChoice != null);
    Assert.expect(subtypeChoice.isAllSubtypes() == false);
    Algorithm algorithm = InspectionFamily.getAlgorithm(subtypeChoice.getSubtype().getInspectionFamilyEnum(), algoEnum);
    return algorithm;
  }

  /**
   * @author Andy Mechtenberg
   */
  private AlgorithmSetting getCurrentThreshold()
  {
    AlgorithmSetting algorithmSetting = null;
    Component comp = _thresholdsTabbedPane.getComponentAt(_thresholdsTabbedPane.getSelectedIndex());

    if (doesCurrentTableExist())
    {
      JTable table = findCurrentTable();
      algorithmSetting = (AlgorithmSetting)table.getModel().getValueAt(table.getSelectedRow(), 0);
      return algorithmSetting;
    }
    return algorithmSetting;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void standardThresholdsRadioButton_actionPerformed(ActionEvent e)
  {
    // probably there were no standard thresholds at all
    if (doesCurrentAlgorithmExist() == false &&
        _thresholdsTabbedPane.getTitleAt(_thresholdsTabbedPane.getSelectedIndex()).equalsIgnoreCase(StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY")) == false)
      return;
    // remember the current one, we can set it active after repopulation
    int selectedIndex = _thresholdsTabbedPane.getSelectedIndex();
    int selectedSubRegion = _subRegionComboBox.getSelectedIndex();//Siew Yeng
    populateThresholdDataTables();

    // now reset to the last active tab (if doesn't exist now, it will default to the first tab
    if (selectedIndex >= 0)
      _thresholdsTabbedPane.setSelectedIndex(selectedIndex);
    
    //Siew Yeng - XCR-2764 - Sub-subtype feature
    if(_thresholdsTabbedPane.getTitleAt(_thresholdsTabbedPane.getSelectedIndex()).equalsIgnoreCase(StringLocalizer.keyToString("ATGUI_VOIDING_TAB_TITLE_KEY")) == true)
    {
      if(selectedSubRegion >= 0)
      _subRegionComboBox.setSelectedIndex(selectedSubRegion);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void advancedThresholdsRadioButton_actionPerformed(ActionEvent e)
  {
    // remember the current one, we can set it active after repopulation
    int selectedIndex = _thresholdsTabbedPane.getSelectedIndex();
    int selectedSubRegion = _subRegionComboBox.getSelectedIndex();//Siew Yeng
    populateThresholdDataTables();

    // now reset to the last active tab (if doesn't exist now, it will default to the first tab
    if (selectedIndex >= 0)
      _thresholdsTabbedPane.setSelectedIndex(selectedIndex);
    
    //Siew Yeng - XCR-2764 - Sub-subtype feature
    if(_thresholdsTabbedPane.getTitleAt(_thresholdsTabbedPane.getSelectedIndex()).equalsIgnoreCase(StringLocalizer.keyToString("ATGUI_VOIDING_TAB_TITLE_KEY")) == true)
    {
      if(selectedSubRegion >= 0)
        _subRegionComboBox.setSelectedIndex(selectedSubRegion);
    }
  }

  /**
   * This gets called from the AlgoTunerDatastoreObserver when a thresholds modified event
   * is observed.
   * Only gets called as a results of threshold learning activities, therefore should not
   * be called when the currently selected tab is the "Slice Height" tab
   * @author Andy Mechtenberg
   */
  void handleThresholdsChangedEvent()
  {
    // remember the current Tab selected so we can re-select when the tables are regenerated
    // get current algo (if any), so we can set the new threshold table to that algo
    // this is usefull when changing subtypes
    populateThresholdDataTables();

    // now reset to the last active tab (if doesn't exist now, it will default to the first tab
//    if (doesCurrentAlgorithmExist() &&
//        _thresholdsTabbedPane.getTitleAt(_thresholdsTabbedPane.getSelectedIndex()).equalsIgnoreCase(StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY")) == false)
//    {
//      AlgorithmEnum algoEnum = getCurrentAlgorithm();
//      for (int i = 0; i < _thresholdsTabbedPane.getTabCount(); i++)
//      {
//        String tabName = _thresholdsTabbedPane.getTitleAt(i);
//        if (algoEnum.toString().equalsIgnoreCase(tabName))
//          _thresholdsTabbedPane.setSelectedIndex(i);
//      }
//    }
  }

  /**
   * This method needs automatically naviage the user to the correct subtype/joint type
   * and then algorithm tab, and then the correct row in the table
   * This is called from the UNDO stuff, so a lot of automatic navigation should be taking place
   * @author Andy Mechtenberg
   */
  void handleThresholdsChangedEvent(Subtype subtype, AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(algorithmSettingEnum != null);

    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    JointTypeChoice jointTypeChoice = new JointTypeChoice(jointTypeEnum);
    SubtypeChoice subtypeChoice = new SubtypeChoice(subtype);

    if (_jointTypeComboBox.getSelectedItem().equals(jointTypeChoice) == false)
      _jointTypeComboBox.setSelectedItem(jointTypeChoice);
    if (_subtypeComboBox.getSelectedItem().equals(subtypeChoice) == false)
      _subtypeComboBox.setSelectedItem(subtypeChoice);

    // if the changed setting enum is of type SLICE_HEIGHT or SLICE_HEIGHT_ADDITIONAL,
    // pop the slice height tab up and leave.
    final AlgorithmSettingTypeEnum algorithmSettingTypeEnum = subtype.getAlgorithmSettingTypeEnum(algorithmSettingEnum);
    if (algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT) ||
        algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL))
    {
      int index = getTabIndexForTabTitle(StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY"));
      if (index < 0)
        return;
      _thresholdsTabbedPane.setSelectedIndex(index);
      if (algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT))
        _standardThresholdsRadioButton.doClick();
      else   // algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL)
        _advancedThresholdsRadioButton.doClick();
      return;
    }   
    // if change to Misalignment tab, perform auto switching of thresholds between Asymmetry and Slug Edge Region Method.
    else if (algorithmSettingEnum.equals(AlgorithmSettingEnum.PCAP_MISALIGNMENT_POLARITY_CHECK_METHODS))
    {
      int index = getTabIndexForTabTitle(StringLocalizer.keyToString("ATGUI_MISALIGNMENT_TAB_TITLE_KEY"));
      if (index < 0)
        return;
      _thresholdsTabbedPane.setSelectedIndex(index);  
      if (algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.STANDARD))
        _standardThresholdsRadioButton.doClick();
      return;
    }// if change to Voiding tab, perform auto switching of thresholds between Standard and Customize in Untested Border Size Method.
    else if (algorithmSettingEnum.equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_METHOD))
    {
      int index = getTabIndexForTabTitle(StringLocalizer.keyToString("ATGUI_VOIDING_TAB_TITLE_KEY"));
      if (index < 0)
        return;
      _thresholdsTabbedPane.setSelectedIndex(index);  
      if (algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.ADDITIONAL))
        _advancedThresholdsRadioButton.doClick();
      return;
    }
    else if (algorithmSettingEnum.equals(AlgorithmSettingEnum.GULLWING_MEASUREMENT_VOID_COMPENSATION_METHOD))
    {
      int index = getTabIndexForTabTitle(AlgorithmEnum.MEASUREMENT.getName());
      if (index < 0)
        return;
      _thresholdsTabbedPane.setSelectedIndex(index);  
      if (algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.STANDARD))
        _standardThresholdsRadioButton.doClick();
      return;
    }
    else if (algorithmSettingEnum.equals(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_METHOD) || 
             algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_WETTING_COVERAGE) ||
             algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_MEASUREMENT))
    {
      int index = getTabIndexForTabTitle(IndictmentEnum.INSUFFICIENT.getName());
      if (index < 0)
        return;
      _thresholdsTabbedPane.setSelectedIndex(index);  
      if (algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.STANDARD))
        _standardThresholdsRadioButton.doClick();
      return;
    }
    //Siew Yeng - XCR-2388 - Image Enhancer - Add threshold for Clahe and Background Filter
    else if (algorithmSettingEnum.equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_ENABLE) ||
            algorithmSettingEnum.equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_CLAHE_ENABLE) ||
            algorithmSettingEnum.equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_ENABLE) ||
            algorithmSettingEnum.equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_ENABLE) ||
            algorithmSettingEnum.equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_ENABLE))
    {
      int index = getTabIndexForTabTitle(AlgorithmEnum.MEASUREMENT.getName());
      if (index < 0)
        return;
      _thresholdsTabbedPane.setSelectedIndex(index);  
      if (algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.ADDITIONAL))
        _advancedThresholdsRadioButton.doClick();
      return;
    }
    else if (algorithmSettingEnum.equals(AlgorithmSettingEnum.CHIP_OPEN_DETECTION_METHOD))
    {
      int index = getTabIndexForTabTitle(AlgorithmEnum.OPEN.getName());
      if (index < 0)
        return;
      _thresholdsTabbedPane.setSelectedIndex(index);  
      if (algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.ADDITIONAL))
        _advancedThresholdsRadioButton.doClick();
      return;
    }
    //Lim, Lay Ngor - Opaque Tombstone - Enable Fillet Width Detection
    else if (algorithmSettingEnum.equals(AlgorithmSettingEnum.CHIP_OPEN_ENABLE_OPAQUE_FILLET_WIDTH_DETECTION))
    {
      int index = getTabIndexForTabTitle(AlgorithmEnum.OPEN.getName());
      if (index < 0)
        return;
      _thresholdsTabbedPane.setSelectedIndex(index);  
      if (algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.ADDITIONAL))
        _advancedThresholdsRadioButton.doClick();
      return;
    }
    else if(algorithmSettingEnum.equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER))
    {
      int index = getTabIndexForTabTitle(IndictmentEnum.VOIDING.getName());
      if (index < 0)
        return;
      _thresholdsTabbedPane.setSelectedIndex(index);  
      if (algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.STANDARD))
        _standardThresholdsRadioButton.doClick();
      return;
    }
    //Siew Yeng - XCR-2764 - Sub-subtype feature
    else if(algorithmSettingEnum.equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VERSION))
    {
      setEnabledSubRegion(subtype);
    }
    else if (algorithmSettingEnum.equals(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_DETECTION_METHOD))
    {
      int index = getTabIndexForTabTitle(AlgorithmEnum.MEASUREMENT.getName());
      if (index < 0)
        return;
      _thresholdsTabbedPane.setSelectedIndex(index);  
      if (algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.STANDARD))
        _standardThresholdsRadioButton.doClick();
      else if (algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.ADDITIONAL))
        _advancedThresholdsRadioButton.doClick();
      return;
    }
    else if (algorithmSettingEnum.equals(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_4_CORNERS)) //Siew Yeng - XCR-3318 - Oval PTH
    {
      int index = getTabIndexForTabTitle(AlgorithmEnum.SHORT.getName());
      if (index < 0)
        return;
      _thresholdsTabbedPane.setSelectedIndex(index);  
      if (algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.ADDITIONAL))
        _advancedThresholdsRadioButton.doClick();
      return;
    }

    // ok, now we need to select the correct algorithm tab.
    java.util.List<AlgorithmEnum> algorithmEnums = subtype.getAlgorithmEnums();
    for (AlgorithmEnum algorithmEnum : algorithmEnums)
    {
      Algorithm algorithm = InspectionFamily.getAlgorithm(subtype.getInspectionFamilyEnum(), algorithmEnum);
      java.util.List<AlgorithmSetting> algorithmSettings = algorithm.getAlgorithmSettings(subtype.getJointTypeEnum());
      for (AlgorithmSetting potentialAlgorithmSetting : algorithmSettings)
      {
        if (potentialAlgorithmSetting.getAlgorithmSettingEnum().equals(algorithmSettingEnum))
        {
          AlgorithmSettingTypeEnum typeEnum = subtype.getAlgorithmSettingTypeEnum(algorithmSettingEnum);
          if(typeEnum.equals(AlgorithmSettingTypeEnum.STANDARD) && (_standardThresholdsRadioButton.isSelected() == false))
            _standardThresholdsRadioButton.doClick();
          if(typeEnum.equals(AlgorithmSettingTypeEnum.ADDITIONAL) && (_advancedThresholdsRadioButton.isSelected() == false))
            _advancedThresholdsRadioButton.doClick();

          int index = algorithmEnums.indexOf(algorithmEnum);
          if (index < 0)
            return;
          _thresholdsTabbedPane.setSelectedIndex(index);
        }
      }
    }
  }
  
  void handleVodingThresholdChangedEvent(Subtype subtype, AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(algorithmSettingEnum != null);

    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    JointTypeChoice jointTypeChoice = new JointTypeChoice(jointTypeEnum);
    SubtypeChoice subtypeChoice = new SubtypeChoice(subtype);

    if (_jointTypeComboBox.getSelectedItem().equals(jointTypeChoice) == false)
      _jointTypeComboBox.setSelectedItem(jointTypeChoice);
    if (_subtypeComboBox.getSelectedItem().equals(subtypeChoice) == false)
      _subtypeComboBox.setSelectedItem(subtypeChoice);

    // if the changed setting enum is of type SLICE_HEIGHT or SLICE_HEIGHT_ADDITIONAL,
    // pop the slice height tab up and leave.
    final AlgorithmSettingTypeEnum algorithmSettingTypeEnum = subtype.getAlgorithmSettingTypeEnum(algorithmSettingEnum);
    if(algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.STANDARD))
    {
      int index = getTabIndexForTabTitle(StringLocalizer.keyToString("ATGUI_VOIDING_TAB_TITLE_KEY"));
      if (index < 0)
        return;
      _thresholdsTabbedPane.setSelectedIndex(index);
      if (algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.STANDARD))
        _standardThresholdsRadioButton.doClick();
      else   // algorithmSettingTypeEnum.equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL)
        _advancedThresholdsRadioButton.doClick();
      return;
    }

    // ok, now we need to select the correct algorithm tab.
    java.util.List<AlgorithmEnum> algorithmEnums = subtype.getAlgorithmEnums();
    for (AlgorithmEnum algorithmEnum : algorithmEnums)
    {
      Algorithm algorithm = InspectionFamily.getAlgorithm(subtype.getInspectionFamilyEnum(), algorithmEnum);
      java.util.List<AlgorithmSetting> algorithmSettings = algorithm.getAlgorithmSettings(subtype.getJointTypeEnum());
      for (AlgorithmSetting potentialAlgorithmSetting : algorithmSettings)
      {
        if (potentialAlgorithmSetting.getAlgorithmSettingEnum().equals(algorithmSettingEnum))
        {
          AlgorithmSettingTypeEnum typeEnum = subtype.getAlgorithmSettingTypeEnum(algorithmSettingEnum);
          if(typeEnum.equals(AlgorithmSettingTypeEnum.STANDARD) && (_standardThresholdsRadioButton.isSelected() == false))
            _standardThresholdsRadioButton.doClick();
          if(typeEnum.equals(AlgorithmSettingTypeEnum.ADDITIONAL) && (_advancedThresholdsRadioButton.isSelected() == false))
            _advancedThresholdsRadioButton.doClick();

          int index = algorithmEnums.indexOf(algorithmEnum);
          if (index < 0)
            return;
          _thresholdsTabbedPane.setSelectedIndex(index);
        }
      }
    }
  }



  /**
   * @author Andy Mechtenberg
   */
  void handleAlgorithmEnumEnabled(Subtype subtype, AlgorithmEnum algorithmEnum, boolean enabled)
  {
    Assert.expect(subtype != null);
    Assert.expect(algorithmEnum != null);

    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    JointTypeChoice jointTypeChoice = new JointTypeChoice(jointTypeEnum);
    SubtypeChoice subtypeChoice = new SubtypeChoice(subtype);

    if (subtypeChoice.getSubtype() != subtype)
    {
      _jointTypeComboBox.setSelectedItem(jointTypeChoice);
      _subtypeComboBox.setSelectedItem(subtypeChoice);
    }

    // ok, now we need to select the correct algorithm tab.
    java.util.List<AlgorithmEnum> algorithmEnums = subtype.getAlgorithmEnums();
    int index = algorithmEnums.indexOf(algorithmEnum);
    if ((index < 0) || (index >= _thresholdsTabbedPane.getTabCount()))
      return;
    _thresholdsTabbedPane.setSelectedIndex(index);

    Component comp = _thresholdsTabbedPane.getSelectedComponent();
    Assert.expect(comp instanceof JPanel);
    JPanel compPanel = (JPanel)comp;
    Component[] componentArray = compPanel.getComponents();
    for (int compIndex = 0; compIndex < componentArray.length; compIndex++)
    {
      Component potentialComp = componentArray[compIndex];
      if (potentialComp instanceof JCheckBox)
      {
        JCheckBox checkBox = (JCheckBox)potentialComp;
        checkBox.setSelected(enabled);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isReadyToFinish()
  {
    // do not allow user to leave if in the middle of an inspection
    AbstractTunerTaskPanel oldPanel = (AbstractTunerTaskPanel)_tunerTaskPane.getComponentAt(_currentSelectedTaskTab);
    return oldPanel.isReadyToFinish();
//    if (_inRunMode)
//      return false;
//    else
//      return true;
  }

  /**
   * Make sure Review Defects is finished and removed before leaving the tuner
   * Unselect the button
   * @author George Booth
   */
  public void finish()
  {
    UIDefaults uiDefaults = UIManager.getDefaults();
    InputMap inputMap = (InputMap)uiDefaults.get("Table.ancestorInputMap");
    inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0), _defaultF2Action);

    saveUIState();
    AbstractTunerTaskPanel oldPanel = (AbstractTunerTaskPanel)_tunerTaskPane.getComponentAt(_currentSelectedTaskTab);
    oldPanel.finish();
//    unsubscribeFromInspectionEvents();
    _testDev.removeChartPanel();
    _commandManager.deleteObserver(this);
    _guiObservable.deleteObserver(this);
//    _selectedRendererObservable.deleteObserver(this);

    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();

    SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    
    Object panelBoardChoice = _boardSelectComboBox.getSelectedItem();


    if ((jointTypeChoice != null) && (subtypeChoice != null) && (panelBoardChoice != null))
    {
      _testDev.setActiveTunerSubtype(jointTypeChoice, subtypeChoice, panelBoardChoice);
    }


    _active = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setThresholdDescriptionText(String text)
  {
    javax.swing.text.Document document = _descriptionEditorPane.getEditorKit().createDefaultDocument();
    _descriptionEditorPane.setDocument(document);
    _descriptionEditorPane.setText(text);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setThresholdDescriptionPage(URL page) throws XrayTesterException
  {
    javax.swing.text.Document document = _descriptionEditorPane.getEditorKit().createDefaultDocument();
    _descriptionEditorPane.setDocument(document);
    try
    {
      _descriptionEditorPane.setPage(page);
    }
    catch (IOException ex)
    {
      DatastoreException dex = new CannotReadDatastoreException(page.getPath());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private TableColumnModelListener getThresholdTableColumnModelListener(final JTable table)
  {
    return new TableColumnModelListener()
    {
      // It is necessary to keep the table since it is not possible
      // to determine the table from the event's source
      JTable _table = table;
      public void columnAdded(TableColumnModelEvent e)
      {
        // do nothing
      }

      public void columnRemoved(TableColumnModelEvent e)
      {
        // do nothing
      }

      public void columnMoved(TableColumnModelEvent e)
      {
        // do nothing
      }

      public void columnMarginChanged(ChangeEvent e)
      {
        saveCurrentTabWidths(_table);
      }

      public void columnSelectionChanged(ListSelectionEvent e)
      {
        // do nothing
      }
    };
  }

  /**
   * @author Laura Cormos
   */
  private int getTabIndexForTabTitle(String title)
  {
    Assert.expect(title != null);

    for (int i = 0; i < _thresholdsTabbedPane.getTabCount(); i++)
    {
      if (_thresholdsTabbedPane.getTitleAt(i).equals(title))
        return i;
    }
    // should not get here
    Assert.expect(true);
    return -1;
  }

  /**
   * @author Laura Cormos
   */
  private void savePreviouslySelectedTab()
  {
    //retrieve the last known selected index from the Tabbedpane.
    _prevSelectedIndex = _thresholdsTabbedPane.getSelectedIndex();

    //verify if the prevSelectedIndex is equal to -1,
    //then set the value to 0 to indicate the first item in the tabbedpane.
    if (_prevSelectedIndex == -1)
      _prevSelectedIndex = 0;
  }

  /**
   * @author Laura Cormos
   */
  private void restorePreviouslySelectedTab()
  {
    //set the selected index to the previous selection
    if (_prevSelectedIndex >= _thresholdsTabbedPane.getTabCount())
      return;
    _thresholdsTabbedPane.setSelectedIndex(_prevSelectedIndex);

  }

  /**
   * @author Laura Cormos
   */
  private void populateThresholdDataTables()
  {
    // the order of these methods is important
    savePreviouslySelectedTab();
    populateThresholdTables();
    populateSliceHeightTab();
    restorePreviouslySelectedTab();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleThresholdTablePopupMenu(MouseEvent e, final JTable table)
  {
    final int selectedRowIndex = table.rowAtPoint(e.getPoint());
    if (selectedRowIndex == -1)
    {
      // user dragged cursor out of the table
      return;
    }
    // do nothing if we're not in the Measurements Tab (tab 2)
    int selectedTab = _tunerTaskPane.getSelectedIndex();
    if (selectedTab < 0 || selectedTab < 2)
      return;

    // show the popup menu
    if (selectedRowIndex >= 0)
    {
      table.setRowSelectionInterval(selectedRowIndex, selectedRowIndex);
      JPopupMenu popupMenu = new JPopupMenu();

      // first determine if there is an associated measurement for this threshold
      // now, must find the correct AlgorithmSetting object
      ThresholdTableModel thresholdTableModel = (ThresholdTableModel)table.getModel();
      final AlgorithmSetting algorithmSetting = (AlgorithmSetting)thresholdTableModel.getValueAt(selectedRowIndex, 0); // 0 column is the AlgorithmSetting object
      JMenuItem thresholdMenuItem = new JMenuItem();
      if (doesCurrentAlgorithmExist() &&   // make sure we're not in slice heights -- no measurements for that. (it's not a "real" algorithm)
          (_thresholdsTabbedPane.getSelectedIndex() != getTabIndexForTabTitle(StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY"))))
      {
        JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
        Assert.expect(jointTypeChoice.isAllFamilies() == false);
        JointTypeEnum jointTypeEnum = jointTypeChoice.getJointTypeEnum();
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

        if (inspectionFamily.doesAssociatedSliceMeasurementPairExist(jointTypeEnum, algorithmSetting))
        {
          java.util.List<Pair<SliceNameEnum, MeasurementEnum>> sliceMeasurementPairs = inspectionFamily.getAssociatedSliceMeasurementPairs(jointTypeEnum, algorithmSetting);
          for(final Pair<SliceNameEnum, MeasurementEnum> sliceMeasurementPair : sliceMeasurementPairs)
          {
            LocalizedString chartMeasurement = new LocalizedString("ATGUI_CHART_MEASUREMENT_FOR_THRESHOLD_KEY", new Object[]{sliceMeasurementPair.getSecond(), sliceMeasurementPair.getFirst()});
            JMenuItem chartMeasMenuItem = new JMenuItem();
            chartMeasMenuItem.setText(StringLocalizer.keyToString(chartMeasurement));
            popupMenu.add(chartMeasMenuItem);
            chartMeasMenuItem.addActionListener(new ActionListener()
            {
              public void actionPerformed(ActionEvent e)
              {
                _guiObservable.stateChanged(sliceMeasurementPair, SelectionEventEnum.SLICE_MEASUREMENT_SELECTION);
                //sendInformationAboutThresholdMeasurement(table, sliceMeasurementPair);
              }
            });
          }
        }
        else
        {
          thresholdMenuItem.setText(StringLocalizer.keyToString("ATGUI_THRESHOLD_NO_MEASUREMENT_KEY"));
          thresholdMenuItem.setEnabled(false);
          popupMenu.add(thresholdMenuItem);
        }
      }
      else
      {
        // current algo does not exist -- most likely due to the slice height tab being active
        thresholdMenuItem.setText(StringLocalizer.keyToString("ATGUI_THRESHOLD_NO_MEASUREMENT_KEY"));
        thresholdMenuItem.setEnabled(false);
        popupMenu.add(thresholdMenuItem);
      }
      popupMenu.show(e.getComponent(), e.getX(), e.getY());
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void sendInformationAboutThresholdMeasurement(JTable table)
  {
    int selectedRowIndex = table.getSelectedRow();
    ThresholdTableModel thresholdTableModel = (ThresholdTableModel)table.getModel();
    // now, must find the correct AlgorithmSetting object
    final AlgorithmSetting algorithmSetting = (AlgorithmSetting)thresholdTableModel.getValueAt(selectedRowIndex, 0); // 0 column is the AlgorithmSetting object
    AlgorithmEnum algorithmEnum = getCurrentAlgorithm().getAlgorithmEnum();
    algorithmSetting.getName();
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    Assert.expect(jointTypeChoice.isAllFamilies() == false);
    JointTypeEnum jointTypeEnum = jointTypeChoice.getJointTypeEnum();
    InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

    if (inspectionFamily.doesAssociatedSliceMeasurementPairExist(jointTypeEnum, algorithmSetting))
    {
      java.util.List<Pair<SliceNameEnum, MeasurementEnum>> sliceMeasurementPairs = inspectionFamily.getAssociatedSliceMeasurementPairs(jointTypeEnum, algorithmSetting);

      // special case code for Chip/Insufficient/Minimum Fillet Thickness -- I need to manually pick the slice
      // if Joint Type is Resistor, choose the Clear slice
      // if Joint Type is Capacitor, choose the Opaque slice
//      if ((inspectionFamily instanceof ChipInspectionFamily) &&
//          (algorithmEnum.equals(AlgorithmEnum.INSUFFICIENT)) &&
//          (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.CHIP_INSUFFICIENT_MINIMUM_FILLET_THICKNESS)))
//      {
//        if (jointTypeEnum.equals(JointTypeEnum.CAPACITOR))
//          sliceMeasurementPair.setFirst(SliceNameEnum.OPAQUE_CHIP_PAD);
//        if (jointTypeEnum.equals(JointTypeEnum.RESISTOR))
//          sliceMeasurementPair.setFirst(SliceNameEnum.CLEAR_CHIP_PAD);
//      }
//            System.out.println("Right click on Threshold: " + inspectionFamily.getName() + " " + algorithmEnum.getName() + " " + algorithmSetting.getName());
//            System.out.println("Slice enum: " + sliceMeasurementPair.getFirst());
//            System.out.println("Measurement enum: " + sliceMeasurementPair.getSecond());

      // in this case, (double-click) we will just pick the first in the list
      Assert.expect(sliceMeasurementPairs != null);
      Assert.expect(sliceMeasurementPairs.isEmpty() == false);
      _guiObservable.stateChanged(sliceMeasurementPairs.get(0), SelectionEventEnum.SLICE_MEASUREMENT_SELECTION);
    }

    //retrieve the last known selected index from the Tabbedpane.
    _prevSelectedIndex = _thresholdsTabbedPane.getSelectedIndex();

    //verify if the prevSelectedIndex is equal to -1,
    //then set the value to 0 to indicate the first item in the tabbedpane.
    if (_prevSelectedIndex == -1)
      _prevSelectedIndex = 0;
  }
  
  /**
   * @author Jack Hwee
   */
  private void enableShowMaskImageButton_actionPerformed(ActionEvent e, Subtype currentSubtypeForMask)
  {
    //Siew Yeng - XCR-2764 - Sub-subtype feature
    Subtype subtype = currentSubtypeForMask;
    int subtypeIndex = _subRegionComboBox.getSelectedIndex();
    if(subtypeIndex > 0)
      subtype = (Subtype)_subRegionComboBox.getSelectedItem();
     
    //Siew Yeng - XCR-2843 - masking rotation
    java.util.List<String> files = FileUtil.listFiles(Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()), 
                                            FileName.getMaskImagePatternString(subtype.getShortName()));
    if(files.isEmpty() == false)
    {
      //String maskImagePath = Directory.getAlgorithmLearningDir(currentSubtypeForMask.getPanel().getProject().getName()) + File.separator + "mask#" + subtype.getShortName() + ".png";
      String maskImagePath = files.get(0);
      
      //Siew Yeng - XCR-3101 - Mask image is not refresh after edit 
      try 
      {
        Icon icon = new ImageIcon(javax.imageio.ImageIO.read(new File(maskImagePath)));

        JOptionPane.showMessageDialog(
        MainMenuGui.getInstance(),
        currentSubtypeForMask.getShortName(), 
        StringLocalizer.keyToString("ATGUI_SHOW_MASK_IMAGE_DIALOG_TITLE_KEY"),
        JOptionPane.INFORMATION_MESSAGE,
        icon);
      } 
      catch (IOException ex) 
      {
        System.out.println("Faile to load mask image: " + maskImagePath);
      }
    }
    else
    {
      JOptionPane.showMessageDialog(
      MainMenuGui.getInstance(),
      StringLocalizer.keyToString("ATGUI_NO_MASK_IMAGE_MESSAGE_KEY"), 
      StringLocalizer.keyToString("ATGUI_SHOW_MASK_IMAGE_DIALOG_TITLE_KEY"),
      JOptionPane.INFORMATION_MESSAGE
      );
    }
  }
  
  /**
   * @author Siew Yeng
   */
  private void addSubRegionButton_actionPerformed(ActionEvent e) 
  {    
    SubtypeChoice currentSubtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    Subtype subtype = currentSubtypeChoice.getSubtype();
    
    Algorithm algorithm = getCurrentAlgorithm();
    
    if(_addOrRemoveSubRegionButton.getText().equalsIgnoreCase(StringLocalizer.keyToString("ATGUI_ADD_SUB_REGION_BUTTON_KEY")))
    {
      if(subtype.hasSubSubtypes() && subtype.getSubSubtypes().size() == Subtype._MAX_SUB_REGION)
      {
        MessageDialog.showWarningDialog(_mainUI, 
                                        StringLocalizer.keyToString("ATGUI_TOTAL_SUBREGION_EXCEED_LIMIT_MESSAGE_KEY"), 
                                        StringLocalizer.keyToString("ATGUI_SUBREGION_TITLE_KEY"));
        return;
      }

      Object retVal = JOptionPane.showInputDialog(_mainUI,
                                                  StringLocalizer.keyToString("AGUI_NEW_SUBREGION_LABEL_KEY"),
                                                  StringLocalizer.keyToString("ATGUI_SUBREGION_TITLE_KEY"),
                                                  JOptionPane.QUESTION_MESSAGE);
      if(retVal == null)
        return;
      
      String subRegionName = (String)retVal;
      String subRegionLongName = subRegionName + "_" + subtype.getLongName();
      
      //Siew Yeng - XCR-3556 - Software crashed when add new subregion with space or blank string
      if (subtype.getPanel().isSubtypeNameValid(subRegionName) == false)
      {
        LocalizedString message;
        String illegalSubtypeCharacters = subtype.getPanel().getSubtypeNameIllegalChars();
        if (illegalSubtypeCharacters.equals(" "))
        {
          message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_ONLY_SPACES_KEY",
                                        new Object[]
                                        {subRegionName});
        }
        else
        {
          message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_OTHER_CHARS_KEY",
                                        new Object[]
                                        {subRegionName, illegalSubtypeCharacters});
        }
        
        JOptionPane.showMessageDialog(_mainUI,
                                      StringLocalizer.keyToString(message),
                                      StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                      JOptionPane.ERROR_MESSAGE);

        return;
      }

      if(subtype.doesSubSubtypeExist(subRegionLongName) || subtype.getPanel().doesSubtypeExist(subRegionLongName))
      {
        MessageDialog.showErrorDialog(_mainUI, 
                                      StringLocalizer.keyToString(new LocalizedString("RMGUI_NEW_SUBTYPE_EXIST_ERROR_KEY", 
                                                                  new String[]{subRegionName,subtype.getJointTypeEnum().getName()})), 
                                      StringLocalizer.keyToString("ATGUI_SUBREGION_TITLE_KEY"), 
                                      true);
        return;
      }
      
      Subtype subSubtype = Subtype.createNewSubtype(subtype.getPanel(), subRegionLongName, subRegionName, subtype.getJointTypeEnum(), algorithm, true);
      java.util.List<AlgorithmSettingEnum> algoSettingEnumIgnoreList = algorithm.getAlgorithmSettingEnumsIgnoreListForSubSubtype();
      if(algoSettingEnumIgnoreList != null)
      {
        _projectObservable.setEnabled(false);
        for(AlgorithmSettingEnum algoSettingEnum : algoSettingEnumIgnoreList)
        {
          Subtype.copyThreshold(subtype, subSubtype, subtype.getAlgorithmSetting(algoSettingEnum));
        }
        _projectObservable.setEnabled(true);
      }
      
      SubtypeAddSubSubtypeCommand setCommand = new SubtypeAddSubSubtypeCommand(subtype, subSubtype);
      try
      {
        _commandManager.trackState(getCurrentUndoState(-1));  
        _commandManager.execute(setCommand);
      }
      catch (XrayTesterException ex)
      {
        //do nothing
      }
    }
    else
    {
      SubtypeRemoveSubSubtypeCommand setCommand = new SubtypeRemoveSubSubtypeCommand(subtype, (Subtype)_subRegionComboBox.getSelectedItem());
      try
      {
        _commandManager.trackState(getCurrentUndoState(-1));  
        _commandManager.execute(setCommand);
      }
      catch (XrayTesterException ex)
      {
        //do nothing
      }
    }
    populateSubSutypeComboBox(subtype);
  }
  
  /**
   * @author Siew Yeng
   */
  private void populateSubSutypeComboBox(Subtype subtype)
  {
    _currentSelectedSubRegion = _subRegionComboBox.getSelectedIndex();
    Subtype currentselectedSubRegion = null;
    if(_currentSelectedSubRegion > 0)
      currentselectedSubRegion = (Subtype) _subRegionComboBox.getSelectedItem();
    
    _subRegionComboBox.removeAllItems();  
    
    _subRegionComboBox.addItem(_PARENT_SUBTYPE_TEXT);
    
    if(subtype.hasSubSubtypes())
    {
      _subRegionComboBox.setEnabled(true);
      for(Subtype subSubtype : subtype.getSubSubtypes())
      {
        _subRegionComboBox.addItem(subSubtype);
      }
      _subRegionComboBox.setMaximumRowCount(Subtype._MAX_SUB_REGION);
      
      if(currentselectedSubRegion != null && subtype.doesSubSubtypeExist(currentselectedSubRegion.getLongName()))
      {
        _subRegionComboBox.setSelectedItem(currentselectedSubRegion);
      }
    }
    else
    {
      _subRegionComboBox.setEnabled(false);
    }
  }
  
  /**
   * @author Siew Yeng
   */
  private void subSubtypeComboBox_actionPerformed(ActionEvent e)
  {
    if(_subRegionComboBox.getSelectedIndex() < 0)
      return;

    if (doesCurrentTableExist() && 
        _thresholdsTabbedPane.getTitleAt(_thresholdsTabbedPane.getSelectedIndex()).equalsIgnoreCase(StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY")) == false)
    {
      SubtypeChoice currentSubtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
      Subtype currentSubtype = currentSubtypeChoice.getSubtype();
      Subtype subtype;

      if(_subRegionComboBox.getSelectedIndex() == 0)
      {
        //parent subtype
        subtype = currentSubtype;
        _addOrRemoveSubRegionButton.setText(StringLocalizer.keyToString("ATGUI_ADD_SUB_REGION_BUTTON_KEY"));
      }
      else
      {
        //Sub-subtype
        subtype = (Subtype)_subRegionComboBox.getSelectedItem();
        _addOrRemoveSubRegionButton.setText(StringLocalizer.keyToString("ATGUI_REMOVE_SUB_REGION_BUTTON_KEY"));
      }
      
      Algorithm currentAlgorithm = getCurrentAlgorithm();
      if(subtype.getAlgorithms().contains(currentAlgorithm) == false)
        return;
      
      JScrollPane jsp = null;
      Component comp = _thresholdsTabbedPane.getSelectedComponent();
      Assert.expect(comp instanceof JPanel);
      JPanel compPanel = (JPanel)comp;
      Component[] componentArray = compPanel.getComponents();
      for (Component potentialComp : componentArray) 
      {
        if (potentialComp instanceof JScrollPane)
        {
          jsp = (JScrollPane)potentialComp;
        }
      }
      
      ThresholdTableModel threshTableModel = new ThresholdTableModel(_mainUI,
                                                                    this,
                                                                    _panelDataAdapter,
                                                                    subtype,
                                                                    currentAlgorithm.getAlgorithmEnum(),
                                                                    _standardThresholdsRadioButton.isSelected());
      //table model cannot be empty, all joint types have at least one slice height algorithm setting
      Assert.expect(threshTableModel.getRowCount() > 0);

      JTable tt = new JTable(threshTableModel);
      jsp.getViewport().removeAll();
      jsp.getViewport().add(tt);
      setupTableCustomizations(subtype, tt);
      setupTableEditorsAndRenderers(currentAlgorithm, subtype, tt);
      threshTableModel.setTestRunning(_inRunMode);
    }
  }
  
  /**
   * @author Siew Yeng 
   * @edited By Kee Chin Seong - Adding in Circular Void checking
   */
  private void setEnabledSubRegion(Subtype subtype) 
  {
    //only large pad family will have this subregion feature and algorithm(voiding) version
    if(subtype.getInspectionFamilyEnum().equals(InspectionFamilyEnum.LARGE_PAD) == false)
      return;
    
    int version = Integer.parseInt((String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VERSION));
    //Siew Yeng - XCR-3100 - Variable pad method should not allow to add subregion
    if(version == 1 || _inRunMode || LargePadVoidingAlgorithm.isVariablePadMethod(subtype) || LargePadVoidingAlgorithm.isUsingCircularVoidMethod(subtype))
    {
      _subRegionComboBox.setEnabled(false);
      _addOrRemoveSubRegionButton.setEnabled(false);
    }
    else
    {
      _subRegionComboBox.setEnabled(true);
      _addOrRemoveSubRegionButton.setEnabled(true);
    }
  }
  
  /**
   * @author Swee Yee Wong
   * XCR-2734	Update Nominals button suddenly disabled.
   * update selected image sets
   */
  public void updateSelectedImageSets(java.util.List<ImageSetData> selectedImageSets)
  {
    Assert.expect(selectedImageSets != null);
    _selectedImageSets = selectedImageSets;
    updateToolBarButtonState();
  }
  
   /**
   * @author weng-jian.eoh
   * 
   * XCR-3589 Combo STD and XXL software GUI
   */
  public void disable()
  {
    RunInspectionPanel oldPanel = (RunInspectionPanel)_tunerTaskPane.getComponentAt(_currentSelectedTaskTab);
    oldPanel.disable();
    _commandManager.deleteObserver(this);
    _guiObservable.deleteObserver(this);
  }
  
  public void repopulateChart()
  {
    ReviewMeasurementsPanel panel = (ReviewMeasurementsPanel) _reviewMeasurementsPanel;
    panel.repopulate();
  }
}
