package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: InspectionRunList</p>
 *
 * <p>Description: Creates and maintains a list of Inspection runs which correlate to panel results
 * files, one inspection run per panel results file.
 *
 * <p>The list displays:<br>
 * Run Id - the run start time in milliseconds since time began of the time<br>
 * Time/Date - the Time and date of the start of the run<br>
 * Inspection Type - the Joint Type/Subtype or Component/Joint of the inspection<br>
 * Description - the user description connected with the image set used for the inspection
 * run or other information
 *
 * <p>The list supports multiple selection
 *
 * <p>The class maintains a List<PanelResults> (summary of panel results) which is used as a
 * source of information to get Joint and Component results from the inspection runs.  The
 * project name and Run Id is used by the JointResultsManager to get the actual results.
 *
 * <p>Some inspection runs may not have an associated image set.  These will have the comment
 * "no image set" added to the end of the Inspection run information. Note that images collected
 * from the hardware in Fine Tuning or from production runs are transient and are marked "runtime
 * image set" - they will only be available until another hardware collection is done.
 *
 * <p>Results file may be deleted via the Inspection Run list.  The user will select one or more
 * inspection runs then use right-click to bring up the Delete Selected Inspection Results popup menu.
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class InspectionRunList extends JList implements Observer
{
  private InspectionRunListData _data = null;
  private JPopupMenu _inspectionRunListPopupMenu = null;
  private JMenuItem _deleteSelectedInspectionRunsPopupMenuItem = null;
  private ListSelectionListener _inspectionRunListSelectionListener;
  private ListSelectionModel _listSelectionModel;

//  private List<PanelResults> _panelResultsList;
//  private List<Long> _selectedInspectionRuns = new ArrayList<Long>();

  private String _projectName = null;
  private int _visibleRows = -1;
  private boolean _deleteInProgress = false;

  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
  private BusyCancelDialog _panelResultsBusyDialog;

  /**
   * @author George Booth
   */
  public InspectionRunList(int visibleRows)
  {
    Assert.expect(visibleRows > 1);
    _visibleRows = visibleRows;
    _data = InspectionRunListData.getInstance();
    _inspectionEventObservable.addObserver(this);
    jbInit();
  }

  /**
   * @author George Booth
   */
  void jbInit()
  {
    _guiObservable.addObserver(this);

    setVisibleRowCount(_visibleRows);
    setCellRenderer(new InspectionRunListCellRenderer());

    _inspectionRunListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        inspectionRunListValueChanged(e);
      }
    };
    addListSelectionListener(_inspectionRunListSelectionListener);

    _listSelectionModel = getSelectionModel();

    // add a popup menu to delete selected inspection runs results files
    _deleteSelectedInspectionRunsPopupMenuItem = new JMenuItem();
    _deleteSelectedInspectionRunsPopupMenuItem.setText(
      StringLocalizer.keyToString("RDGUI_DELETE_INSPECTION_RUNS_POPUP_MENU_KEY"));
    _deleteSelectedInspectionRunsPopupMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteSelectedInspectionRuns();
      }
    });

    _inspectionRunListPopupMenu = new JPopupMenu();
    _inspectionRunListPopupMenu.add(_deleteSelectedInspectionRunsPopupMenuItem);

    // add a mouse listener for the popup menu
    this.addMouseListener(new MouseAdapter()
    {
      // show project menu on right click
      public void mouseReleased(MouseEvent e)
      {
        if (e.isPopupTrigger())
        {
          int[] selectedRowIndexes = getSelectedIndices();
          // get closest index to point
          int selectedIndex = locationToIndex(e.getPoint());
          // make sure point is actually in that cell
          Rectangle rect = getCellBounds(selectedIndex, selectedIndex);
          if (rect.contains(e.getPoint()) == false)
          {
            // user dragged cursor out of the table
            return;
          }
          else
          {
            // if the cell is in the current selection, keep the current selection
            boolean indexNotSelected = true;
            for (int index : selectedRowIndexes)
            {
              if (index == selectedIndex)
              {
                indexNotSelected = false;
              }
            }
            if (indexNotSelected)
            {
              setSelectedIndex(selectedIndex);
              selectedRowIndexes = getSelectedIndices();
            }
          }

          // show the popup menu
          if (selectedRowIndexes.length > 0)
          {
            _inspectionRunListPopupMenu.show(e.getComponent(), e.getX(), e.getY());
          }
        }
      }
    });

  }

  /**
   * @author George Booth
   */
  private void createPanelResultsBusyDialog()
  {
    _panelResultsBusyDialog = new BusyCancelDialog(
        MainMenuGui.getInstance(),
        StringLocalizer.keyToString("RDGUI_GENERATE_INSPECTION_RUNS_LIST_KEY"),
        StringLocalizer.keyToString("RDGUI_GENERATE_INSPECTION_RUNS_LIST_TITLE_KEY"),
        true);

    _panelResultsBusyDialog.pack();
    SwingUtils.centerOnComponent(_panelResultsBusyDialog, MainMenuGui.getInstance());
  }

  /**
   * @author George Booth
   */
  private void closePanelResultsBusyDialog()
  {
    if (_panelResultsBusyDialog != null)
    {
      // wait until visible if not forced
      while (_panelResultsBusyDialog.isVisible() == false)
      {
        try
        {
          Thread.sleep(200);
        }
        catch (InterruptedException ex)
        {
          // do nothing
        }
      }
      _panelResultsBusyDialog.dispose();
      _panelResultsBusyDialog = null;
    }
  }

  /**
   * @author George Booth
   */
  public void populate(String projectName, List<Long> selectedInspectionRuns)
  {
    Assert.expect(projectName != null);
    _projectName = projectName;
    //Kok Chun, Tan - XCR-2022
    //create busy dialog here, so that the user will not feel the software hang
    createPanelResultsBusyDialog();
    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
//    System.out.println("initializing inspection run list");
        if (_deleteInProgress)
        {
//      System.out.println("delete in progress - list not initialized");
          return;
        }

        removeListSelectionListener(_inspectionRunListSelectionListener);

        final List<Long> inspectionRuns = InspectionResultsReader.getInspectionStartTimesInMillis(_projectName);
        if (inspectionRuns.size() != _data.getPanelResultsListSize())
          _data.newDataNeeded(true);

        // only update list of results if needed (x6000 just started, a new inspection was done or result files were deleted)
        if (_data.isNewDataNeeded() == false)
        {
//      System.out.println("new data not needed - list not re-initialized");
        }
        else
        {
//      System.out.println("new data needed - list is being initialized");

          // clear old data before reading new
          _data.clearPanelResultsList();
          setListData(_data.getPanelResultsListArray());

          TimerUtil timer = new TimerUtil();
          timer.start();

          timer.stop();
//          System.out.println("glb time to get inspection run list: " + timer.getElapsedTimeInMillis());
          timer.reset();
          timer.start();

          _data.setPanelResultsList(JointResultsManager.getInstance().getPanelResultsList(_projectName, inspectionRuns));

          timer.stop();
          _data.newDataNeeded(false);
        }
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            closePanelResultsBusyDialog();
          }
        });
      }
    });
    _panelResultsBusyDialog.setVisible(true);

    // show data
    setListData(_data.getPanelResultsListArray());

    addListSelectionListener(_inspectionRunListSelectionListener);

    // check if previous selections, if any, are in current results
    List<Integer> indices = new ArrayList<Integer>();
    if (selectedInspectionRuns != null && _data.getPanelResultsListSize() > 0)
    {
      for (int i = 0; i < _data.getPanelResultsListSize(); i++)
      {
        for (Long inspectionRun : selectedInspectionRuns)
        {
          if (_data.getPanelResultsInspectionStartTimeInMillis(i) == inspectionRun.longValue())
          {
            indices.add(new Integer(i));
          }
        }
      }
    }

    // make sure _selectedInspectionRuns is updated here in case the calling routine
    // asks for them before the selection event has had a chance to propagate
    _data.clearSelectedInspectionRuns();
    if (_data.getPanelResultsListSize() > 0 && indices.size() > 0)
    {
      // reselect previous selections
//      System.out.println("\n------ reselect previous selections");
      int[] intArray = new int[indices.size()];
      for (int i = 0; i < indices.size(); i++)
      {
        intArray[i] = indices.get(i);
        _data.addSelectedInspectionRun(_data.getPanelResultsInspectionStartTimeInMillis(i));
      }
      int numIndices = intArray.length;
      // Note: it would be convenient to use setSelectedIndices(intArray) here.  However,
      // this method produces multiple calls to valueChanged(), one for each index.
      // The first call will have intArray[0] selected,
      // the second call will have intArray[0] and intArray[1] selected, etc.
      // Mutliple calls will produce mutliple INSPECTION_RUN_SELECTION events, which is evil.
      // The following implements the method with isAdjusting() set true while the indices are
      // selected.  Setting isAdjusting() to false triggers the call to valueChanged().
//      _listSelectionModel.clearSelection();
//      int size = getModel().getSize();
//      _listSelectionModel.setValueIsAdjusting(true);
//      for(int i = 0; i < numIndices; i++)
//      {
//        if (intArray[i] < size) {
//          _listSelectionModel.addSelectionInterval(intArray[i], intArray[i]);
//        }
//      }
      // make sure only one event is seen by valueChanged()
      _listSelectionModel.setValueIsAdjusting(true);
      setSelectedIndices(intArray);
      _listSelectionModel.setValueIsAdjusting(false);
    }
    else if (_data.getPanelResultsListSize() > 0 && indices.size() < 1)
    {
      // select last run
//      System.out.println("\n------ select last run");
      _data.addSelectedInspectionRun(_data.getPanelResultsInspectionStartTimeInMillis(0));
      setSelectedIndex(0);
    }
    else
    {
      // no results
//      System.out.println("\n------ no results");
      clearSelection();
      // inform UI of no results
      InspectionRunSelection inspectionRunSelection = new InspectionRunSelection(_data.getSelectedInspectionRuns());
      _guiObservable.stateChanged(inspectionRunSelection, SelectionEventEnum.INSPECTION_RUN_SELECTION);
    }
  }

  /**
   * @author George Booth
   */
  public void unpopulate()
  {
    removeAll();
    _data.clearPanelResultsList();
    _data.clearSelectedInspectionRuns();
    _data.newDataNeeded(true);
  }

  /**
   * @author George Booth
   */
  public List<PanelResults> getPanelResultsList()
  {
    return _data.getPanelResultsList();
  }

  /**
   * @author George Booth
   */
  public List<Long> getSelectedInspectionRuns()
  {
    return _data.getSelectedInspectionRuns();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setSelectedInspectionRuns(List<Long> selectedRuns)
  {
    Assert.expect(selectedRuns != null);
    
    // the listener is removed here so these changes don't force a re-read of all the results files
    // the listeners is NOT added back at the end of this method because the events generated by doing
    // the clearSelection and re-selection will be queued up in the bowels of Swing, and if I add the
    // listener back in at the end of this method, by the time Swing processes the events, the listener
    // will be back, and we will re-read resulst.
    // I put the listener back in the update() method, when respoding to INSPECTION_RUN_SELECTION_PROCESSING_COMPLETE event
    // which should really mark the end of the whole "read result files" operation
    removeListSelectionListener(_inspectionRunListSelectionListener);
        
    clearSelection();
    for(long selectedRun : selectedRuns)
    {
      int listIndex = _data.getIndexOf(selectedRun);
      addSelectionInterval(listIndex, listIndex);
   }
    _data.setSelectedInspectionRuns(selectedRuns);        
  }
  
  /**
   * @author George Booth
   */
  private void inspectionRunListValueChanged(ListSelectionEvent e)
  {
//    System.out.println("inspectionRunListValueChanged()");

    if (e.getValueIsAdjusting())
    {
//      System.out.println("  selection is adjusting");
      // Do nothing.
    }
    else
    {
//      JList inspectionRunList = (JList)e.getSource();
//      final ListSelectionModel selectionModel = inspectionRunList.getSelectionModel();      
      if (_listSelectionModel.isSelectionEmpty())
      {
//        System.out.println("  selection is empty");
        // Clear list data.
        _data.clearSelectedInspectionRuns();
      }
      else
      {
        // ignore events while UI is procesing
        // UI MUST tell us when it is done
//        System.out.println("  disabling selection processing");
        _listSelectionModel.setValueIsAdjusting(true);

        int minIndex = _listSelectionModel.getMinSelectionIndex();
        int maxIndex = _listSelectionModel.getMaxSelectionIndex();
        _data.clearSelectedInspectionRuns();
        for (int index = minIndex; index <= maxIndex; index++)
        {
          if (isSelectedIndex(index))
          {
            Long run = ((PanelResults)getModel().getElementAt(index)).getInspectionStartTimeInMillis();
            _data.addSelectedInspectionRun(run);
//            System.out.println("------ selected index = " + index + "; run date = " + new Date(run));
          }
        }
      }

      // send event that a new selection has been made
      InspectionRunSelection inspectionRunSelection = new InspectionRunSelection(_data.getSelectedInspectionRuns());
      _guiObservable.stateChanged(inspectionRunSelection, SelectionEventEnum.INSPECTION_RUN_SELECTION);

//  ReviewDefectsPanel:  updateInspectionRun();
//  ReviewMeasurementsPanel:  populateSliceNameComboBox();

    }
  }

  /**
   * @author George Booth
   */
  void deleteSelectedInspectionRuns()
  {
//    System.out.println("deleteSelectedInspectionRuns()");
    if (JOptionPane.showConfirmDialog(
      MainMenuGui.getInstance(),
      StringLocalizer.keyToString(new LocalizedString("RDGUI_CONFIRM_INSPECTION_RUNS_DELETE_KEY", null)),
      StringLocalizer.keyToString("RDGUI_CONFIRM_INSPECTION_RUNS_DELETE_TITLE_KEY"),
      JOptionPane.YES_NO_OPTION,
      JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION)
    {
      return;
    }

    // ignore changes until we are done
    removeListSelectionListener(_inspectionRunListSelectionListener);

    final int[] selectedRowIndexes = this.getSelectedIndices();

    // we can't delete an inspection run if it is currently selected.
    clearSelection();

    // get the inspection runs to delete
    long[] runsToDelete = new long[selectedRowIndexes.length];
    int i = 0;
    for (int index : selectedRowIndexes)
    {
      runsToDelete[i++] = _data.getPanelResultsInspectionStartTimeInMillis(index);
    }
    _deleteInProgress = true;

    // delete panel results to free their memory; they are renewed after the delete
    _data.clearPanelResultsList();

    final long[] tempRunsToDelete = runsToDelete;
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        for (int j = 0; j < tempRunsToDelete.length; j++)
        {
          long startTime = tempRunsToDelete[j];
//          System.out.println("  results file " + j + "; startTime = " + startTime);
          // delete results file
          boolean resultsFileDeleted = false;
          String resultsFile = FileName.getResultsFileFullPath(_projectName, startTime);

          try
          {
            if (FileUtilAxi.exists(resultsFile))
            {
              FileUtilAxi.delete(resultsFile);
              resultsFileDeleted = true;
            }
          }
          catch (DatastoreException dse)
          {
//            System.out.println("DatastoreException = " + dse.toString());
            MessageDialog.reportIOError(null, dse.getLocalizedMessage());
          }

          if (resultsFileDeleted)
          {
            String resultsDir = Directory.getInspectionResultsDir(_projectName, startTime);
//            System.out.println("  Deleting " + resultsDir);

            try
            {
              if (FileUtilAxi.exists(resultsDir))
                FileUtilAxi.delete(resultsDir);
            }
            catch (DatastoreException dse)
            {
//              System.out.println("DatastoreException = " + dse.toString());
              MessageDialog.reportIOError(null, dse.getLocalizedMessage());
            }
          }
        }
        _deleteInProgress = false;

        addListSelectionListener(_inspectionRunListSelectionListener);

        // send event that the inspection run list has been changed
//        InspectionRunSelection inspectionRunSelection = new InspectionRunSelection(_data.getSelectedInspectionRuns());
//        _guiObservable.stateChanged(inspectionRunSelection, GuiUpdateEventEnum.INSPECTION_RUNS_UPDATED);
        // refresh results list, select first run by default
        _data.newDataNeeded(true);
        populate(_projectName, null);
      }
    });

    //  ReviewDefectsPanel:  populatePanelWithData();
    //  ReviewMeasurementsPanel:  populateInspectionRunList();
  }

  /**
   * @author George Booth
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    Assert.expect(observable != null);

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof GuiObservable)
        {
          final GuiEvent guiEvent = (GuiEvent)object;
          final GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
          if (guiEventEnum instanceof SelectionEventEnum)
          {
            SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
            if (selectionEventEnum.equals(SelectionEventEnum.INSPECTION_RUN_SELECTION_PROCESSING_COMPLETE))
            {
//              System.out.println("IRP: SelectionEventEnum.INSPECTION_RUN_SELECTION_PROCESSING_COMPLETE seen");
//              System.out.println("enable selection processing");
              // re-enable events
              _listSelectionModel.setValueIsAdjusting(false);
              int numberOfListeners = getListSelectionListeners().length;
              if (numberOfListeners == 0)              
                addListSelectionListener(_inspectionRunListSelectionListener);              
            }
          }
        }
        else if (observable instanceof InspectionEventObservable)
        {
          InspectionEvent event = (InspectionEvent)object;
          InspectionEventEnum eventEnum = event.getInspectionEventEnum();
          if (eventEnum.equals(InspectionEventEnum.INSPECTION_STARTED))
          {
            // don't care
          }
          else if (eventEnum.equals(InspectionEventEnum.INSPECTION_COMPLETED))
          {
//            System.out.println("IRL: InspectionEventEnum.INSPECTION_COMPLETED seen");
            // new inspection results cause the data to be refreshed with last run displayed by default
            // when the next populate() is invoked
            _data.newDataNeeded(true);
          }
        }
        else
        {
          Assert.expect(false, "No observer found.");
        }
      }
    });
  }

  /**
   * debugging utility
   * @author George Booth
   */
  void displaySelectedInspectionRuns(String source)
  {
    displaySelectedInspectionRuns(source, _data.getSelectedInspectionRuns());
  }

  /**
   * debugging utility
   * @author George Booth
   */
  void displaySelectedInspectionRuns(String source, List<Long> selectedInspectionRuns)
  {
    System.out.print(source + " selected inspection runs (size=" + selectedInspectionRuns.size() + ") ");
    for (Long runId : selectedInspectionRuns)
    {
      System.out.print(runId + " ");
    }
    System.out.println();
  }
}
