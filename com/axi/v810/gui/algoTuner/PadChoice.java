package com.axi.v810.gui.algoTuner;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * This class contains the user choices for Algorithm Family
 * The choices are either a particular AlgorithmFamilyEnum or "All"
 * @author Andy Mechtenberg
 */
class PadChoice implements Serializable
{
  private PadType _padType;
  private boolean _all;

  /**
   * @author Andy Mechtenberg
   */
  public PadChoice(PadType padType)
  {
    Assert.expect(padType != null);
    _padType = padType;
    _all = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public PadChoice()
  {
    _all = true;
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean isAllPads()
  {
    return _all;
  }

  /**
   * @author Andy Mechtenberg
   */
  PadType getPadType()
  {
    Assert.expect(_all == false);
    Assert.expect(_padType != null);
    return _padType;
  }

  /**
   * Objects are equal if they specify the same algorithm family
   * @author Andy Mechtenberg
   */
  public boolean equals(Object rhs)
  {
    if (rhs == null)
      return false;

    if (rhs == this)
      return true;

    if (rhs instanceof PadChoice)
    {
      PadChoice afc = (PadChoice)rhs;

      // if both specify ALL, then they are equal
      if((_all) && (afc._all))
        return true;
      else if(_all != afc._all) // if one specifies all, and the other does not, they are not equal
        return false;
      else
      {
        // if they both specify the same specific algorithm family, they are equal
        if(_padType.equals(afc._padType))
          return true;
        else
          return false;
      }
    }
    else  // not an instance of AlgorithmFamilyChoice
      return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int hashcode()
  {
    if (_all)
      return -1;
    else
      return _padType.hashCode();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    if (_all)
      return StringLocalizer.keyToString("ATGUI_ALL_KEY");
    else
      return _padType.toString();
  }
}
