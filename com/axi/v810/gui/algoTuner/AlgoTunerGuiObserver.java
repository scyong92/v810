package com.axi.v810.gui.algoTuner;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;

/**
 * @author Andy Mechtenberg
 */
class AlgoTunerGuiObserver implements Observer
{
  private AlgoTunerGui _algoTunerGui = null;

  /**
   * @author Andy Mechtenberg
   */
  public AlgoTunerGuiObserver(AlgoTunerGui gui)
  {
    Assert.expect(gui != null);
    _algoTunerGui = gui;
  }

 /**
  * This must by synchronous - do not make any calls back into the server on this thread
  * or you will get a deadlock situation
  *
  * @author Peter Esbensen
  * @author Matt Wharton
  */
  public synchronized void update(Observable observable, final Object arg)
  {
    // get the image and "paint" the component with it
    Assert.expect(observable != null);
    Assert.expect(arg != null);
    Assert.expect(observable instanceof GuiObservable);
    Assert.expect(arg instanceof GuiEvent);
    final GuiEvent guiEvent = (GuiEvent)arg;
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
        if (guiEventEnum instanceof SelectionEventEnum)
        {
          SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
          if (selectionEventEnum.equals(SelectionEventEnum.BOARD_TYPE))
          {
            _algoTunerGui.updateWithProjectData();
          }
        }
      }
    });
  }
}
