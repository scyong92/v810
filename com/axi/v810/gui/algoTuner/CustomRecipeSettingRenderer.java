package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.FontUtil;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.config.*;
import com.axi.v810.util.*;
import java.awt.Component;
import java.util.List;

/**
 * XCR-2895 : Initial recipes setting to speed up the programming time
 *
 * @author weng-jian.eoh
 * @return
 */
public class CustomRecipeSettingRenderer extends JLabel implements TableCellRenderer
{
  private Hashtable<Integer, TableCellRenderer> _renderers;
  private TableCellRenderer _renderer, _defaultRenderer;
  private Font _boldFont;
  private Font _regularFont;

  private final Color _VALUE_IS_DEFAULT_BACKGROUND_COLOR = Color.white;
  private final Color _VALUE_IS_MODIFIED_BACKGROUND_COLOR = new Color(255, 255, 153); // light yellow
  private final Color _VALUE_IS_MODIFIED_BACKGROUND_2_COLOR = new Color(255, 255, 100); // dark yellow  



  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public CustomRecipeSettingRenderer()
  {
    _renderers = new Hashtable<Integer, TableCellRenderer>();
    _defaultRenderer = new DefaultTableCellRenderer();
    _regularFont = this.getFont();
    _boldFont = FontUtil.getBoldFont(this.getFont(),Localization.getLocale());
  }


  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  void add(int row, TableCellRenderer renderer)
  {
    Assert.expect(renderer != null);
    _renderers.put(new Integer(row), renderer);
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    setFont(_regularFont);
    setOpaque(true);
    _renderer = _renderers.get(new Integer(row));
    if (_renderer != null)
    {
      //_renderer = _defaultRenderer;
      return _renderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }
    setOpaque(true);
    if (isSelected)
    {
      setForeground(table.getSelectionForeground());
      setBackground(table.getSelectionBackground());
    }
    else
    {
      setForeground(table.getForeground());
      setBackground(table.getBackground());
    }

    if (hasFocus)
    {
      //setBorder(BorderFactory.createLineBorder(Color.red, 4));  // apm for debug of focus
      setBorder(BorderFactory.createLineBorder(Color.yellow, 1));
    }
    else
    {
      setBorder(null);
    }

    if (value != null)
    {
      Object cellObject = table.getModel().getValueAt(row, InitialRecipeSettingTableModel.NAME_COLUMN);
      RecipeAdvanceSettingEnum recipeSetting = null;

      recipeSetting = RecipeAdvanceSettingEnum.getRecipeSettingEnumByName((String)cellObject);
      
      String valueText = "";
      if (value instanceof String)
      {
        valueText = (String) value;
      }
      else if (value instanceof List)
      {
        valueText = ((List<String>)value).toString();
      }
      setText(valueText);

      setToolTipText(valueText);

      if (column == InitialRecipeSettingTableModel.VALUE_COLUMN)
      {
        setForeground(Color.black);
        Object defaultValue = recipeSetting.getDefaultValue();
        Object setValue = table.getValueAt(row, InitialRecipeSettingTableModel.VALUE_COLUMN);
        
        if (defaultValue.toString().equals(setValue.toString()))
        {
//            setBackground(_VALUE_IS_DEFAULT_BACKGROUND_COLOR);
//            if (_subtype != null)
//            {
//              if (_subtype.isImportedPackageLibrary())
//              {
//                if (_subtype.isImportedValue(algorithmSetting.getAlgorithmSettingEnum(), getSerializedValue( setValue , algorithmSetting)))
//                  setBackground(_VALUE_IS_IMPORTED_BACKGROUND_COLOR);
//              }
//            }
        }
        else if (row % 2 != 0)
        {
          setBackground(_VALUE_IS_MODIFIED_BACKGROUND_COLOR);
        }
        else
        {
          setBackground(_VALUE_IS_MODIFIED_BACKGROUND_2_COLOR);
        }
      }
      setEnabled(true);   
    }
    return this;
  }

}
