package com.axi.v810.gui.algoTuner;

import java.util.*;

import com.axi.v810.util.*;

/**
 * This class is used to enumerate the types of sigma we support in review
 * measurements.
 * @author Lim, Lay Ngor
 */
class SigmaTypeEnum extends com.axi.util.Enum
{
  private static Map<String, SigmaTypeEnum> _stringToEnumMap = new LinkedHashMap<String,SigmaTypeEnum>();

  private String _name;
  private static int _index = -1;

  public final static SigmaTypeEnum ONE_SIGMA = new SigmaTypeEnum(++_index, "1");
  public final static SigmaTypeEnum THREE_SIGMA = new SigmaTypeEnum(++_index, "3");
  public final static SigmaTypeEnum FIVE_SIGMA = new SigmaTypeEnum(++_index, "5");

  /**
   * @author Lim, Lay Ngor
   */
  private SigmaTypeEnum(int id, String name)
  {
    super(id);
    _name = name;
    _stringToEnumMap.put(name, this);
  }

  /**
   * @author Lim, Lay Ngor
   */
  public String toString()
  {
    return _name;
  }

  /**
   * @author Lim, Lay Ngor
   */
  static Collection<SigmaTypeEnum> getAllTypes()
  {
    return _stringToEnumMap.values();
  }
}
