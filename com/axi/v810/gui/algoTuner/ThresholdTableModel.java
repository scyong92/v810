package com.axi.v810.gui.algoTuner;

import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.gullwing.*;
import com.axi.v810.business.imageAnalysis.largePad.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * The model for all threshold tables
 * @author Andy Mechtenberg
 */
public class ThresholdTableModel extends AbstractTableModel
{
  private JFrame _frame = null;
  private AlgoTunerGui _algoTunerGui;
  private PanelDataAdapter _panelDataAdapter = null;
  private static com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private Subtype _subtype;
  private InspectionFamilyEnum _inspectionFamilyEnum = null;
  private AlgorithmEnum _algorithmEnum = null;
  private List<List<Object>> _displayData = null; // a list of lists
  protected final String[] _thresholdColumns = {StringLocalizer.keyToString("ATGUI_THRESHOLD_TABLE_NAME_KEY"),
                                                StringLocalizer.keyToString("ATGUI_THRESHOLD_TABLE_VALUE_KEY"),
                                                StringLocalizer.keyToString("ATGUI_THRESHOLD_TABLE_DEFAULT_KEY"),
                                                StringLocalizer.keyToString("ATGUI_THRESHOLD_TABLE_LOWLIMIT_KEY"),
                                                StringLocalizer.keyToString("ATGUI_THRESHOLD_TABLE_HIGHLIMIT_KEY")};

  public static final int NAME_COLUMN = 0;
  public static final int VALUE_COLUMN = 1;
  public static final int DEFAULT_COLUMN = 2;
  public static final int LOWLIMIT_COLUMN = 3;
  public static final int HIGHLIMIT_COLUMN = 4;

  private boolean _testRunning = false;
  private boolean _setValueValid = true;
  //append history log -hsia-fen.tan
  private static transient ProjectHistoryLog _fileWriter;
  private boolean _isSliceSetuptab = false;

  /**
   * @author Andy Mechtenberg
   */
   public ThresholdTableModel()
   {
     _fileWriter = ProjectHistoryLog.getInstance();
     // do nothing
   }
  
  /**
   * @author Andy Mechtenberg
   */
  public ThresholdTableModel(JFrame frame,
                             AlgoTunerGui testDev,
                             PanelDataAdapter panelDataAdapter,
                             Subtype subtype,
                             AlgorithmEnum algorithmEnum,
                             boolean standard)
  {
    Assert.expect(frame != null);
    Assert.expect(testDev != null);
    Assert.expect(panelDataAdapter != null);
    _panelDataAdapter = panelDataAdapter;
    _frame = frame;
    _algoTunerGui = testDev;
    _subtype = subtype;
    _inspectionFamilyEnum = _subtype.getInspectionFamilyEnum();
    _algorithmEnum = algorithmEnum;
    _displayData = new LinkedList<List<Object>>();
    
    List<AlgorithmSetting> algorithmBarrelSlice = new ArrayList<AlgorithmSetting>();
    
    boolean isMedianMethod = false;
    boolean isSlugDetection = false;
    boolean isFloodFillMethod = false;
    boolean isUntestedBorderCustomizeMethod = false;
    boolean isGaussianVoidCompensationMethod = false;
    int  sliceValue = 0;

    List<AlgorithmSetting> algorithmSettings = _panelDataAdapter.getThresholds(_subtype,
                                                                               _inspectionFamilyEnum,
                                                                               _algorithmEnum,
                                                                               standard);
    for (AlgorithmSetting algorithmSetting : algorithmSettings)
    { 
      if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_METHOD))
      {
        AddSliceHeightRow(algorithmSetting);
        if (_subtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum()).toString().matches("Median Method"))
        {
          isMedianMethod = true;
          isFloodFillMethod = false;
        }
          
        if (_subtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum()).toString().matches("FloodFill Method"))
        {
          isMedianMethod = false;
          isFloodFillMethod = true;
        }
      }        
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES))
      {
        if(LargePadVoidingAlgorithm.isFloodFillMethod(_subtype)|| LargePadVoidingAlgorithm.isThicknessMethod(_subtype))
          AddSliceHeightRow(algorithmSetting);        
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD))
      {
        if(isMedianMethod == true && isFloodFillMethod == false)
          AddSliceHeightRow(algorithmSetting);
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_PAD) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_LOWERPAD) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_MIDBALL) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_PACKAGE))
      {
        if(isMedianMethod == false && isFloodFillMethod == false)
          AddSliceHeightRow(algorithmSetting);
      }
      //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
      else if (algorithmSetting.getName().startsWith("Maximum Difference from Nominal Solder Signal - Barrel") ||
               algorithmSetting.getName().startsWith("Maximum Excess Solder Signal - Barrel") ||
               algorithmSetting.getName().startsWith("Nominal Solder Signal - Barrel") ||
               algorithmSetting.getName().startsWith("Maximum Difference from Region Solder Signal - Barrel") ||
               algorithmSetting.getName().startsWith("Nominal Corrected Greylevel - Barrel") ||
               algorithmSetting.getName().startsWith("Maximum Nominal Difference - Barrel") ||
               algorithmSetting.getName().startsWith("Maximum Region Difference - Barrel"))
      {
        sliceValue = Integer.parseInt(_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_NUMBER_OF_SLICE).toString());
        algorithmBarrelSlice.add(algorithmSetting);
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD))
      {
        if(isMedianMethod == false && isFloodFillMethod == true)
          AddSliceHeightRow(algorithmSetting);
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THICKNESS_THRESHOLD_PAD) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_PERCENTILE) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_REGION_SIZE_PAD) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NOISE_REDUCTION))
      {
        if(LargePadVoidingAlgorithm.isThicknessMethod(_subtype))
        {
          AddSliceHeightRow(algorithmSetting);
        }
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER)||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD_FOR_LAYER_ONE) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_ADDITIONAL) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_USING_MULTI_THRESHOLD) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_USING_FIT_POLYNOMIAL)||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NOISE_REDUCTION_FOR_FLOODFILL))
      {
        if(LargePadVoidingAlgorithm.isFloodFillMethod(_subtype))
        {
          AddSliceHeightRow(algorithmSetting);
        }
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.SHARED_VOIDING_DETECTION_SENSITIVITY)||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.SHARED_VOIDING_REGION_REFERENCE_POSITION) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.SHARED_VOIDING_INNER_EDGE_POSITION) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.SHARED_VOIDING_OUTER_EDGE_POSITION) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.SHARED_VOIDING_GREY_LEVEL_NOMINAL))
      {
        if(LargePadVoidingAlgorithm.isUsingCircularVoidMethod(_subtype))
        {
          AddSliceHeightRow(algorithmSetting);
        }
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_TWO))
      {
        if(LargePadVoidingAlgorithm.isFloodFillMethod(_subtype))
        {
          int numberOfImageLayer = Integer.parseInt(_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER).toString());
          
          if( numberOfImageLayer == 2 || numberOfImageLayer == 3)
          {
            AddSliceHeightRow(algorithmSetting);
          }
        }
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_THREE))
      {
        if(LargePadVoidingAlgorithm.isFloodFillMethod(_subtype))
        {
          if(Integer.parseInt(_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER).toString()) == 3)
          {
            AddSliceHeightRow(algorithmSetting);
          }
        }
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_BORDER_SENSIVITY) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_VOIDING_SENSIVITY))
      {        
        if (LargePadVoidingAlgorithm.isVariablePadMethod(_subtype))
          AddSliceHeightRow(algorithmSetting);
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_GAUSSIAN_BLUR))
      {
        //Siew Yeng - XCR-3564 - Gaussian blur threshold missing from Variable Pad Void Method for Single Pad
        if (LargePadVoidingAlgorithm.isVariablePadMethod(_subtype) || LargePadVoidingAlgorithm.isFloodFillMethod(_subtype))
          AddSliceHeightRow(algorithmSetting);
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_ENABLE_SUBREGION))
      {
        if(_subtype.isSubSubtype())
          AddSliceHeightRow(algorithmSetting);
      }
      else if (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GULLWING_MEASUREMENT_VOID_COMPENSATION_METHOD))
      {
        AddSliceHeightRow(algorithmSetting);
        String voidDetectionMethod = _subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_VOID_COMPENSATION_METHOD).toString();
        if (voidDetectionMethod.equals(GullwingMeasurementAlgorithm.VOID_COMPENSATION_METHOD_NONE))
          isGaussianVoidCompensationMethod = false;
        else if (voidDetectionMethod.equals(GullwingMeasurementAlgorithm.VOID_COMPENSATION_METHOD_GAUSSIAN))
          isGaussianVoidCompensationMethod = true;
      }
      else if (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_LOCATOR_METHOD))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES).toString().equalsIgnoreCase("True") &&
            (LargePadVoidingAlgorithm.isFloodFillMethod(_subtype) || LargePadVoidingAlgorithm.isThicknessMethod(_subtype)))
          AddSliceHeightRow(algorithmSetting);
      }
      else if ((algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_AREA_METHOD_SENSITIVITY)) 
              || (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD))
              || (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_UNTESTED_BORDER_SIZE)))
      {
        String insufficientDetectionMethod = _subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_METHOD).toString();
        if (insufficientDetectionMethod.equals(LargePadInsufficientAlgorithm._AREA_METHOD))
          AddSliceHeightRow(algorithmSetting);
      }
      else if (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GULLWING_MEASUREMENT_SMOOTH_SENSITIVITY))
      {
        if (isGaussianVoidCompensationMethod)
          AddSliceHeightRow(algorithmSetting);
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_ENABLE_JOINT_BASED_EDGE_DETECTION))
      {
        if(subtype.getPanel().getProject().isBGAInspectionRegionSetTo1X1())
          AddSliceHeightRow(algorithmSetting);
      }
      else if (algorithmSetting.isSliceSpecific())
      {      
          if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE))
          {
            sliceValue = Integer.parseInt(_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE).toString());
          
            List<SliceNameEnum> slices = new ArrayList<SliceNameEnum>();
            switch(sliceValue)
            {
                case 1:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    break;
                case 2:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    break;
                case 3:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    break;
                case 4:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    break;
                case 5:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    break;
                case 6:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
                    break;
                case 7:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
                    break;
                case 8:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
                    break;
                case 9:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
                    break;
                case 10:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
                    break;
                case 11:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
                    break;
                case 12:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
                    break;
                case 13:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
                    break;
                case 14:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
                    break;
                case 15:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
                    break;
                case 16:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
                    break;
                case 17:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
                    break;
                case 18:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
                    break;
                case 19:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
                    break;
                case 20:
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
                    slices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
                    break;
            }

            if (slices.isEmpty() == false && slices.contains(algorithmSetting.getSpecificSlice()))
                AddSliceHeightRow(algorithmSetting);
          }
          else if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.LARGE_PAD_NUMBER_OF_USER_DEFINED_SLICE))
          {
            sliceValue = Integer.parseInt(_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_NUMBER_OF_USER_DEFINED_SLICE).toString());
          
            List<SliceNameEnum> slices = new ArrayList<SliceNameEnum>();
            switch(sliceValue)
            {
                case 1:
                    slices.add(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1);
                    break;
                case 2:
                    slices.add(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1);
                    slices.add(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2);
                    break; 
            }

            if (slices.isEmpty() == false && slices.contains(algorithmSetting.getSpecificSlice()))
                AddSliceHeightRow(algorithmSetting);
          }       
      } 
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.PCAP_MISALIGNMENT_POLARITY_CHECK_METHODS))
      {
        AddSliceHeightRow(algorithmSetting);     
        if (_subtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum()).toString().equalsIgnoreCase("Slug Edge Method"))
          isSlugDetection = true;
      }
      else if (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_THRESHOLD) || 
              (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_LENGTH_ACROSS)) ||
              (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_LENGTH_ALONG)) ||
              (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_POSITION)) )           
      {
        if(isSlugDetection == true)
            AddSliceHeightRow(algorithmSetting);
      } 
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_METHOD))
      {
        AddSliceHeightRow(algorithmSetting);     
        if (_subtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum()).toString().equalsIgnoreCase("Customize"))
          isUntestedBorderCustomizeMethod = true;
      }
      else if (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_LEFT_SIDE_OFFSET) || 
              (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_BOTTOM_SIDE_OFFSET)) ||
              (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_RIGHT_SIDE_OFFSET)) ||
              (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_TOP_SIDE_OFFSET)) )           
      {
        if(isUntestedBorderCustomizeMethod == true)
          AddSliceHeightRow(algorithmSetting);
      }
      else if (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE) )           
      {
        if(isUntestedBorderCustomizeMethod == false)
          AddSliceHeightRow(algorithmSetting);
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_ITERATOR) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_WIDTH) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_LENGTH))
      {
        if(_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_ENABLE).toString().matches("True"))
          AddSliceHeightRow(algorithmSetting);
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_CLAHE_BLOCK_SIZE))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_CLAHE_ENABLE).toString().matches("True"))
          AddSliceHeightRow(algorithmSetting);
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_LARGESCALE) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SATURATEVALUE) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SMALLSCALE) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SUPPRESSSTRIPES) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_TOLERANCEOFDIRECTION))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_ENABLE).toString().matches("True"))
          AddSliceHeightRow(algorithmSetting);
      }
      //Lim, Lay Ngor - Clear Tombstone - Upper fillet thickness method
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_EDGE_SEARCH_THICKNESS_PERCENT))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_DETECTION_METHOD).toString().equalsIgnoreCase("Thickness"))
          AddSliceHeightRow(algorithmSetting);
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_SEARCH_DISTANCE_FROM_EDGE))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_DETECTION_METHOD).toString().equalsIgnoreCase("Thickness"))
          AddSliceHeightRow(algorithmSetting);
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_LOCATION_OFFSET))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_DETECTION_METHOD).toString().equalsIgnoreCase("BestSignal"))
          AddSliceHeightRow(algorithmSetting);
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_DETECTION_METHOD).toString().equalsIgnoreCase("BestSignal"))
          AddSliceHeightRow(algorithmSetting);
      }      
      //Lim, Lay Ngor - Clear Tombstone - Secondary Matching Threshold Display Base on Selected Method - START
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_SOLDER_AREA_PERCENTAGE))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_DETECTION_METHOD).toString().equalsIgnoreCase("Solder Area"))
          AddSliceHeightRow(algorithmSetting);
      }   
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_ROUNDNESS_PERCENTAGE))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_DETECTION_METHOD).toString().equalsIgnoreCase("Roundness Calculation"))
          AddSliceHeightRow(algorithmSetting);
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_MATCHING_PERCENTAGE))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_DETECTION_METHOD).toString().equalsIgnoreCase("Template Match"))
          AddSliceHeightRow(algorithmSetting);
      }
      //XCR-3264 Diagnostic Image for Tombstone is not able to send to VVTS 
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.SAVE_DIAGNOSTIC_IMAGES))
      {
        if(_subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.CHIP_OPEN_DETECTION_METHOD))
        {
          if(_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_DETECTION_METHOD).toString().equalsIgnoreCase("Disable") == false)
            AddSliceHeightRow(algorithmSetting);
        }
        else
        {
          AddSliceHeightRow(algorithmSetting);
        }
      }
      //Lim, Lay Ngor - Clear Tombstone - Secondary Matching Threshold Display Base on Selected Method - END
      //Lim, Lay Ngor - Opaque Tombstone - Enable Fillet Width Detection - START   
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_FILLET_WIDTH_JOINT_RATIO))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_ENABLE_OPAQUE_FILLET_WIDTH_DETECTION).toString().equalsIgnoreCase("True"))
          AddSliceHeightRow(algorithmSetting);
      }   
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_PARTIAL_FILLET_THICKNESS_PERCENTAGE))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_ENABLE_OPAQUE_FILLET_WIDTH_DETECTION).toString().equalsIgnoreCase("True"))
          AddSliceHeightRow(algorithmSetting);
      }
      //Lim, Lay Ngor - Opaque Tombstone - Enable Fillet Width Detection - END 
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_MEASUREMENT)||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_VOIDING_PERCENTAGE) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE)||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DEGREE_COVERAGE)||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE_SENSITIVITY))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_WETTING_COVERAGE).toString().equalsIgnoreCase("True"))
          AddSliceHeightRow(algorithmSetting);
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN)||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_VOID_VOLUME_PERCENTAGE))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_MEASUREMENT).toString().equalsIgnoreCase("True"))
          AddSliceHeightRow(algorithmSetting);
      }
      //Siew Yeng - XCR-3318 - Oval PTH
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_INNER_CORNER))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_4_CORNERS).toString().equalsIgnoreCase("False"))
          AddSliceHeightRow(algorithmSetting);
      }
      // Kok Chun, Tan - Motion Blur
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_DIRECTION) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_MASK_SCALE) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_GRAYLEVEL_OFFSET) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_ITERATION))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_ENABLE).toString().equalsIgnoreCase("True"))
          AddSliceHeightRow(algorithmSetting);
      }
      // Kok Chun, Tan - Shading Removal
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_DIRECTION) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_BLUR_DISTANCE) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_KEEP_OUT_DISTANCE) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_DESIRED_BACKGROUND) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_FILTERING_TECHNIQUE))
      {
        if (_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_ENABLE).toString().equalsIgnoreCase("True"))
          AddSliceHeightRow(algorithmSetting);
      }
      else
          AddSliceHeightRow(algorithmSetting);
   /*          if (algorithmSetting.getName().matches("Void Thickness Threshold - Pad") == false &&
            algorithmSetting.getName().matches("Void Thickness Threshold - Midball") == false &&
            algorithmSetting.getName().matches("Void Thickness Threshold - Package") == false)
           AddSliceHeightRow(algorithmSetting);*/
    }
    
    if (algorithmBarrelSlice.size() > 0)
    {
        for(int counter = 0; counter < sliceValue; counter ++)
           AddSliceHeightRow(algorithmBarrelSlice.get(counter));
        
        algorithmBarrelSlice.clear();
        algorithmBarrelSlice = null;
    }
  }

  /**
   * This constructor is to be used specifically for the slice height algorithm settings ONLY!!
   * @author Laura Cormos
   */
  public ThresholdTableModel(JFrame frame,
                             AlgoTunerGui testDev,
                             PanelDataAdapter panelDataAdapter,
                             Subtype subtype,
                             boolean standard)
  {
    Assert.expect(frame != null);
    Assert.expect(testDev != null);
    Assert.expect(panelDataAdapter != null);
    _panelDataAdapter = panelDataAdapter;
    _frame = frame;
    _algoTunerGui = testDev;
    _subtype = subtype;
    _algorithmEnum = AlgorithmEnum.MEASUREMENT; // all slice setup guys are this
    _inspectionFamilyEnum = _subtype.getInspectionFamilyEnum();
    _displayData = new LinkedList<List<Object>>();
    _isSliceSetuptab = true;
   
    List<AlgorithmSetting> algorithmSettings = _panelDataAdapter.getSliceSetupThresholds(_subtype,
                                                                                         _inspectionFamilyEnum,
                                                                                         standard);
    ArrayList<AlgorithmSetting> algorithmBarrelSlice = new ArrayList<AlgorithmSetting>();

    Collections.sort(algorithmSettings, new AlgorithmSettingDisplayOrderComparator());
    boolean isMils = false;
    int sliceValue = 0;
    boolean defineOffsetForPadAndPackage = false;
    for (AlgorithmSetting algorithmSetting : algorithmSettings)
    {
      //System.out.println(algorithmSetting);
      if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.SLICEHEIGHT_WORKING_UNIT))
      {
          AddSliceHeightRow(algorithmSetting);
          if(_subtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum()).toString().matches("Thickness"))
            isMils = true;
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT))
      {
          if(isMils == false)
            AddSliceHeightRow(algorithmSetting);
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT_IN_THICKNESS) ||
              algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT_IN_THICKNESS))
      {
          if(isMils == true)
            AddSliceHeightRow(algorithmSetting);
      }
      else if(algorithmSetting.getName().startsWith("Barrel Slice Height") && 
              algorithmSetting.getName().contains("Thickness") == false )
      {
          if(isMils == false)
          {
              algorithmBarrelSlice.add(algorithmSetting);
          }
      }
      else if(algorithmSetting.getName().startsWith("Barrel Slice Height Thickness"))
      {
          if(isMils == true)
          {
              algorithmBarrelSlice.add(algorithmSetting);
          }
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.USER_DEFINED_NUMBER_OF_SLICE))//Number Of Slice
      {
          //_subtype.getAlgorithmSettingValue
          sliceValue = Integer.parseInt(_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_NUMBER_OF_SLICE).toString());
          AddSliceHeightRow(algorithmSetting);
      }
	  else if (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE))//Number Of User Defined Slice for Grid Array
      {
          sliceValue = Integer.parseInt(_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE).toString());
          AddSliceHeightRow(algorithmSetting);
      }
      else if (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.ADVANCED_GULLWING_USER_DEFINED_NUMBER_OF_SLICE))//Number Of User Defined Slice
      {
          sliceValue = 1;//Integer.parseInt(_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_USER_DEFINED_NUMBER_OF_SLICE).toString());
          //AddSliceHeightRow(algorithmSetting);
          algorithmBarrelSlice.add(algorithmSetting);
      }
      else if (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) || 
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_20) )
      {
        algorithmBarrelSlice.add(algorithmSetting);
      }
      else if (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_USER_DEFINED_SLICE_1) ||
               algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_USER_DEFINED_SLICE_2) )
      {          
          if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.LARGE_PAD_NUMBER_OF_USER_DEFINED_SLICE))
          {
            sliceValue = Integer.parseInt(_subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_NUMBER_OF_USER_DEFINED_SLICE).toString());
          
            switch(sliceValue)
            {
                case 1:
                    if (algorithmSetting.getName().matches(AlgorithmSettingEnum.LARGE_PAD_USER_DEFINED_SLICE_1.getName()))
                       algorithmBarrelSlice.add(algorithmSetting);
                    break;
                case 2:
                     algorithmBarrelSlice.add(algorithmSetting);
                    break; 
            }
          }         
      } 
      //Swee Yee Wong
      else if (algorithmSetting.getName().matches("Define Offset for Pad and Package"))
      {
        if(_subtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum()).toString().matches("On"))
          defineOffsetForPadAndPackage = true;
        AddSliceHeightRow(algorithmSetting);
      }
      else if (algorithmSetting.getName().matches("Midball To Edge Offset"))
      {
        if(defineOffsetForPadAndPackage == true)
          AddSliceHeightRow(algorithmSetting);
      }
      else
          AddSliceHeightRow(algorithmSetting);
    }

    for(int counter = 0; counter < sliceValue; counter ++)
       AddSliceHeightRow(algorithmBarrelSlice.get(counter));
    //System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
  }

  /**
   * 
   * @param algorithmSetting 
   */
  private void AddSliceHeightRow(AlgorithmSetting algorithmSetting)
  {
      List<Object> threshList = new LinkedList<Object>();
      Serializable value = _subtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum());
      if (value instanceof Boolean)
      {
        Boolean bool = (Boolean)value;
        if (bool.booleanValue())
          value = new String("Yes");
        else
          value = new String("No");
      }

      Object defaultValue = _subtype.getAlgorithmSettingDefaultValue(algorithmSetting.getAlgorithmSettingEnum());
      if (defaultValue instanceof Boolean)
      {
        Boolean bool = (Boolean)defaultValue;
        if (bool.booleanValue())
          defaultValue = new String("Yes");
        else
          defaultValue = new String("No");
      }

      Object minValue = new String("");
      Object maxValue = new String("");
      if ((value instanceof Integer) || (value instanceof Float))
      {
        minValue = _subtype.getAlgorithmSettingMinimumValue(algorithmSetting.getAlgorithmSettingEnum());
        maxValue = _subtype.getAlgorithmSettingMaximumValue(algorithmSetting.getAlgorithmSettingEnum());
      }
      threshList.add(algorithmSetting);
      threshList.add(value);
      threshList.add(defaultValue);
      threshList.add(minValue);
      threshList.add(maxValue);
      _displayData.add(threshList);
  }

  /**
  * Get the current column count -- from AbstractTableModel
  *
  * @author Andy Mechtenberg
  * @return The integer specifying the column count.
  */
  // from AbstractTableModel
  public int getColumnCount()
  {
    return _thresholdColumns.length;
  }

  /**
  * Get the current column name -- from AbstractTableModel
  *
  * @author Andy Mechtenberg
  * @return The string specifying the column header name
  * @param col Column number
  */
  // from AbstractTableModel
  public String getColumnName(int col)
  {
    Assert.expect(col >= 0 && col < getColumnCount());
    return (String)_thresholdColumns[col];
  }

  /**
  * Get the value at row/column -- from AbstractTableModel
  *
  * @author Andy Mechtenberg
  * @return The object at the specified row/column
  * @param row Row number
  * @param col Column number
  */
  // from AbstractTableModel
  public Object getValueAt(int row, int col)
  {
    List<Object> rowData = _displayData.get(row);
    Object data = null;
    data = rowData.get(col);

    if (col > NAME_COLUMN)
    {
      // check to see if we should be returning metric or imperial units.
      // we will convert to Mils if the units are millimeters
      AlgorithmSetting algorithmSetting = (AlgorithmSetting) rowData.get(NAME_COLUMN);
      MeasurementUnitsEnum algorithmSettingUnitsEnum = _subtype.getAlgorithmSettingUnitsEnum(algorithmSetting.getAlgorithmSettingEnum());
      MathUtilEnum projectUnits = Project.getCurrentlyLoadedProject().getDisplayUnits();
      data = AlgorithmSettingUtil.getAlgorithmSettingDisplayValue(data, algorithmSettingUnitsEnum, projectUnits);
    }

    return data;
  }

  /**
   * OK -- this is a problem.  Over in AlgoTunerGUI, I have a focus listener on the cell editor for the table.  If
   * that editor loses focus, it calls the stopCellEditing function.  (I need to do that so accept editor values
   * if another GUI button is clicked)  That function will eventually trickle down
   * to this setValueAt function.  If the value is out of range, then the user will see 2 JOptionPanes.  Why?
   * Consider this scenario:
   * The user types in an out-of-range value, and hits enter.
   *    This function (setValueAt) is called, and then the JOptionPane pops up.
   *    The very fact that a JOptionPane pops up causes the cell to lose focus, and then it wants to stopCellEditing
   *    That call to stop cell editing will also pop up a JOptionPane, and the user sees 2.
   *
   * Possible solutions -- throw an exception.  Problem is, who would catch it?  This is all part of the
   * swing architecture, and it wont' know about a ValueOutOfRange exception
   * A kludgey solution is to use a flag variable that makes sure only one of these is called per value/row/column
   * The kludgey solution is currently a GO.
   * @author Andy Mechtenberg
   */
  public void setValueAt(Object value, int row, int col)
  {
   //get original threshold value- hsia-fen.tan 
    Object oldValue=getValueAt(row,col); 
    
    if (!_setValueValid) return;  // exit if it's not valid
    _setValueValid = false;
    /** @todo Replace this will an Assert when datastore is more complete  */
    if (value == null)
      return;

    // Check to see if the incoming new value is the same as the old one.  If so, do no more work
    if (value.equals(getValueAt(row,col)))
    {
      //System.out.println("New value " + value + " old value " + getValueAt(row,col));
      _setValueValid = true;
      return;
    }

    // A bug can arise where the value type does not match the cell's value type.  ComboBoxes cause this, but I don't
    // know why.  This check should fix it.
    if (value.getClass() != getValueAt(row, col).getClass())
    {
      //System.out.println("New value class" + value.getClass() + " old value " + getValueAt(row,col).getClass());
      _setValueValid = true;
      return;
    }

    List<Object> rowData = _displayData.get(row);
    final AlgorithmSetting algorithmSetting = (AlgorithmSetting)rowData.get(NAME_COLUMN);
    String valStr = value.toString();

    if (value instanceof Integer)
    {
      try
      {
        Integer integerVal = new Integer(valStr);
        // check to see if it's in the right range (between min and max, inclusive)
        Integer min = (Integer)_subtype.getAlgorithmSettingMinimumValue(algorithmSetting.getAlgorithmSettingEnum());
        Integer max = (Integer)_subtype.getAlgorithmSettingMaximumValue(algorithmSetting.getAlgorithmSettingEnum());
        boolean isDependentAlgorithmSettingEnumConditionMatch = true;
        if (algorithmSetting.hasDependentAlgorithmSettingEnum())
            isDependentAlgorithmSettingEnumConditionMatch = _subtype.isDependentAlgorithmSettingEnumConditionMatch(value,
                                                                                                                   algorithmSetting.getDependentAlgorithmSettingEnum(),
                                                                                                                   algorithmSetting.getAlgorithmSettingComparatorEnum());
        if (isDependentAlgorithmSettingEnumConditionMatch == false)
        {
            SwingUtils.invokeLater(new Runnable()
            {
                public void run()
                {
                  LocalizedString localString = new LocalizedString("ATGUI_THRESHOLD_DEPENDENT_CONDITION_FAILED_KEY", new Object[]{algorithmSetting.getAlgorithmSettingComparatorEnum().getName(),
                                                                                                                                   algorithmSetting.getDependentAlgorithmSettingEnum().getName()});
                  JOptionPane.showMessageDialog(_frame, StringLocalizer.keyToString(localString),
                                                StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                JOptionPane.ERROR_MESSAGE);
                }
            });
        }
        else if ( (integerVal.intValue() <= max.intValue()) && (integerVal.intValue() >= min.intValue()))
        {
          SubtypeSetTunedValueCommand setCommand = new SubtypeSetTunedValueCommand(_subtype, algorithmSetting.getAlgorithmSettingEnum(), integerVal);
          try
          {
            Algorithm algorithm = InspectionFamily.getAlgorithm(_inspectionFamilyEnum, _algorithmEnum);
            //get measurement unit -hsia-fen.tan
            MeasurementUnitsEnum measurementUnitsEnum = _subtype.getAlgorithmSettingUnitsEnum(algorithmSetting.getAlgorithmSettingEnum());
            _commandManager.trackState(_algoTunerGui.getCurrentUndoState(row, algorithm, algorithmSetting));
            _commandManager.execute(setCommand);//, new TestDevChangeScreenCommand(_testDev, TestDevScreenEnum.PANEL_SETUP));
            rowData.set(col, integerVal);      
           _fileWriter.appendThreshold(_subtype,algorithmSetting, _algorithmEnum,integerVal,measurementUnitsEnum);           
          }
          catch (XrayTesterException ex)
          {
            Assert.expect(false);
          }
          finally
          {
            if (setCommand != null)
              setCommand = null;
          }
          rowData.set(VALUE_COLUMN, integerVal);
        }
        else
        {
          // Here I want to display a error dialog.  For some reason, even though this is on the AWT-Event queue
          // if this method is called when the user types in a too large value, then clicks on some other window (so focus
          // is lost), this will hang unless I call invokeLater.
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              JOptionPane.showMessageDialog(_frame, StringLocalizer.keyToString("ATGUI_THRESHOLD_OUT_OF_RANGE_KEY"),
                                            StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"), JOptionPane.ERROR_MESSAGE);
            }
          });
        }
      }
      catch (NumberFormatException nfe)
      {
        // shouldn't ever happen because the IntegerNumberField class will make sure only integers get put here
        Assert.expect(false);
      }
    }
    else if (value instanceof Float)
    {
      try
      {
        Float floatVal = new Float(valStr);

        // @author Patrick Lacz
        // we handle algorithm settings using millimeters in a special way - the project's display units may convert entry/display to mils.
        MeasurementUnitsEnum measurementUnitsEnum = _subtype.getAlgorithmSettingUnitsEnum(algorithmSetting.getAlgorithmSettingEnum());
        if (measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS) &&
            Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
        {
          // convert from mils (mils is what the user has input)
          floatVal = MathUtil.convertMilsToMillimeters(floatVal.floatValue());
        }
        else if (measurementUnitsEnum.equals(MeasurementUnitsEnum.SQUARE_MILLIMETERS) &&
            Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
        {
          // convert from sq mils (sq mils is what the user has input)
          floatVal = MathUtil.convertSquareMilsToSquareMillimeters(floatVal.floatValue());
        }
        else if (measurementUnitsEnum.equals(MeasurementUnitsEnum.SQUARE_MILLIMETERS) &&
            Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
        {
          // convert from cu mils (cu mils is what the user has input)
          floatVal = MathUtil.convertCubicMilsToCubicMillimeters(floatVal.floatValue());
        }

        // check to see if it's in the right range (between min and max, inclusive)
        Float min = (Float)_subtype.getAlgorithmSettingMinimumValue(algorithmSetting.getAlgorithmSettingEnum());
        Float max = (Float)_subtype.getAlgorithmSettingMaximumValue(algorithmSetting.getAlgorithmSettingEnum());
        boolean isDependentAlgorithmSettingEnumConditionMatch = true;
        if (algorithmSetting.hasDependentAlgorithmSettingEnum())
            isDependentAlgorithmSettingEnumConditionMatch = _subtype.isDependentAlgorithmSettingEnumConditionMatch(value,
                                                                                                                   algorithmSetting.getDependentAlgorithmSettingEnum(),
                                                                                                                   algorithmSetting.getAlgorithmSettingComparatorEnum());
        if (isDependentAlgorithmSettingEnumConditionMatch == false)
        {
            SwingUtils.invokeLater(new Runnable()
            {
                public void run()
                {
                  LocalizedString localString = new LocalizedString("ATGUI_THRESHOLD_DEPENDENT_CONDITION_FAILED_KEY", new Object[]{algorithmSetting.getAlgorithmSettingComparatorEnum().getName(),
                                                                                                                                   algorithmSetting.getDependentAlgorithmSettingEnum().getName()});
                  JOptionPane.showMessageDialog(_frame, StringLocalizer.keyToString(localString),
                                                StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                JOptionPane.ERROR_MESSAGE);
                }
            });
        }
        else if((floatVal.floatValue() <= max.floatValue()) && (floatVal.floatValue() >= min.floatValue()))
        {
          //Siew Yeng - XCR-2168
          if(algorithmSetting.getAlgorithmSettingEnum() == AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_COMPONENT_PERCENT_PAD)
          {
            Set<Subtype> affectedSubtypes = new HashSet();
            
            for(ComponentType compType : _subtype.getComponentTypes())
            {
              affectedSubtypes.addAll(compType.getSubtypes(_inspectionFamilyEnum));
            }
            
            if(affectedSubtypes.isEmpty() == false)
            {
              int status = JOptionPane.OK_OPTION;
              //Siew Yeng - XCR-2539 - Warning message prompt multiple time when change Component Voiding Threshold
              if(affectedSubtypes.size() > 1)
              {
                status = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(), 
                StringLocalizer.keyToString("ATGUI_CHANGING_THRESHOLDS_AFFECT_OTHER_SUBTYPES_KEY"),
                StringLocalizer.keyToString("MM_GUI_TDT_TITLE_TUNER_KEY"), 
                JOptionPane.OK_CANCEL_OPTION, 
                JOptionPane.QUESTION_MESSAGE);
              }
              
              if(status == JOptionPane.CLOSED_OPTION || status == JOptionPane.CANCEL_OPTION)
              {
                //Siew Yeng - XCR-2540 - Component Voiding Threshold cannot be set anymore after cancel to change threshold once
                _setValueValid = true;
                affectedSubtypes.clear();
                return;
              }
              else if(status == JOptionPane.OK_OPTION)
              {
                try
                {
                  Algorithm algorithm = InspectionFamily.getAlgorithm(_inspectionFamilyEnum, _algorithmEnum);
                  UndoState undoState = _algoTunerGui.getCurrentUndoState(row, algorithm, algorithmSetting);
                  _commandManager.trackState(undoState);
                  _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_SET_VALUE_KEY"));
                  for(Subtype subtype : affectedSubtypes)
                  {
                    SubtypeSetTunedValueCommand setCommand = new SubtypeSetTunedValueCommand(subtype, algorithmSetting.getAlgorithmSettingEnum(), floatVal);
                    _commandManager.execute(setCommand);//, new TestDevChangeScreenCommand(_testDev, TestDevScreenEnum.PANEL_SETUP));

                    //Siew Yeng - XCR-2764 - Sub-subtype feature
                    if(subtype.hasSubSubtypes())
                    {
                      List<AlgorithmSettingEnum> ignoreList = algorithm.getAlgorithmSettingEnumsIgnoreListForSubSubtype();
                      if(ignoreList != null && ignoreList.contains(algorithmSetting.getAlgorithmSettingEnum()))
                      {
                        for(Subtype subSubtype: subtype.getSubSubtypes())
                        {
                          setCommand = new SubtypeSetTunedValueCommand(subSubtype, algorithmSetting.getAlgorithmSettingEnum(), floatVal);
                          _commandManager.execute(setCommand);
                        }
                      }
                    }
                    //append threshold value into history log-hsia-fen.tan
                    _fileWriter.appendThreshold(subtype, algorithmSetting,_algorithmEnum, (Serializable) oldValue, (Serializable)floatVal);
                    
                  }
                  affectedSubtypes.clear();
                  _commandManager.endCommandBlock();
                  rowData.set(col, floatVal);
                }
                catch (XrayTesterException ex)
                {
                  // shouldn't ever happen - Siew Yeng - copy from else condition below
                  Assert.expect(false);
                }
              }
            }
          }
          else
          {
            SubtypeSetTunedValueCommand setCommand = new SubtypeSetTunedValueCommand(_subtype, algorithmSetting.getAlgorithmSettingEnum(), floatVal);
            try
            {
              Algorithm algorithm = InspectionFamily.getAlgorithm(_inspectionFamilyEnum, _algorithmEnum);
              UndoState undoState = _algoTunerGui.getCurrentUndoState(row, algorithm, algorithmSetting);
              _commandManager.trackState(undoState);
              _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_SET_VALUE_KEY"));
              _commandManager.execute(setCommand);//, new TestDevChangeScreenCommand(_testDev, TestDevScreenEnum.PANEL_SETUP));
              rowData.set(col, floatVal);
              //Siew Yeng - XCR-2764 - Sub-subtype feature
              if(_subtype.hasSubSubtypes())
              {
                List<AlgorithmSettingEnum> ignoreList = algorithm.getAlgorithmSettingEnumsIgnoreListForSubSubtype();
                if(ignoreList != null && ignoreList.contains(algorithmSetting.getAlgorithmSettingEnum()))
                {
                  for(Subtype subSubtype: _subtype.getSubSubtypes())
                  {
                    setCommand = new SubtypeSetTunedValueCommand(subSubtype, algorithmSetting.getAlgorithmSettingEnum(), floatVal);
                    _commandManager.execute(setCommand);
                  }
                }
              }
              _commandManager.endCommandBlock();
              //append threshold value into history log-hsia-fen.tan
              if (_isSliceSetuptab == false)
                 _fileWriter.appendThreshold(_subtype, algorithmSetting,_algorithmEnum, (Serializable) oldValue, (Serializable)floatVal);
              else
                  _fileWriter.appendThresholdSliceSetup(_subtype, algorithmSetting, (Serializable) oldValue, (Serializable)floatVal);
            }
            catch (XrayTesterException ex)
            {
              Assert.expect(false);
            }
            finally
            {
              if (setCommand != null)
                setCommand = null;
            }
          }
        }
        else
        {
          // Here I want to display a error dialog.  For some reason, even though this is on the AWT-Event queue
          // if this method is called when the user types in a too large value, then clicks on some other window (so focus
          // is lost), this will hang unless I call invokeLater.
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              JOptionPane.showMessageDialog(_frame, StringLocalizer.keyToString("ATGUI_THRESHOLD_OUT_OF_RANGE_KEY"),
                                            StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                            JOptionPane.ERROR_MESSAGE);
            }
          });
//          JOptionPane.showMessageDialog(_frame, StringLocalizer.keyToString("ATGUI_THRESHOLD_OUT_OF_RANGE_KEY"),
//                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"), JOptionPane.ERROR_MESSAGE);
        }
      }
      catch (NumberFormatException nfe)
      {
        // shouldn't ever happen because the FloatNumberField class will make sure floats get put here
        Assert.expect(false);
      }
    }
    else if (value instanceof String)
    {
//      rowData.set(VALUE_COLUMN, value); //Siew Yeng
      // a Boolean type threshold will come in to here as a string, because it's represented with a "Yes" or "No"
      // in the table.  So if the value is a string with contents "Yes" or "No", convert it back to a boolean when setting
      // the value
      if ((valStr.equalsIgnoreCase("Yes")) || (valStr.equalsIgnoreCase("No")))
      {
        if (valStr.equalsIgnoreCase("Yes"))
        {
          SubtypeSetTunedValueCommand setCommand = new SubtypeSetTunedValueCommand(_subtype, algorithmSetting.getAlgorithmSettingEnum(), new Boolean(true));
          try
          {
            Algorithm algorithm = InspectionFamily.getAlgorithm(_inspectionFamilyEnum, _algorithmEnum);
            _commandManager.trackState(_algoTunerGui.getCurrentUndoState(row, algorithm, algorithmSetting));
            _commandManager.execute(setCommand);//, new TestDevChangeScreenCommand(_testDev, TestDevScreenEnum.PANEL_SETUP));
            rowData.set(col, new Boolean(true));
            _fileWriter.appendThreshold(_subtype,algorithmSetting, _algorithmEnum,new Boolean(true));
          }
          catch (XrayTesterException ex)
          {
            Assert.expect(false);
          }
          finally
          {
            if (setCommand != null)
              setCommand = null;
          }
        }
        else
        {
          SubtypeSetTunedValueCommand setCommand = new SubtypeSetTunedValueCommand(_subtype, algorithmSetting.getAlgorithmSettingEnum(), new Boolean(false));
          try
          {
            Algorithm algorithm = InspectionFamily.getAlgorithm(_inspectionFamilyEnum, _algorithmEnum);
            _commandManager.trackState(_algoTunerGui.getCurrentUndoState(row, algorithm, algorithmSetting));
            _commandManager.execute(setCommand);//, new TestDevChangeScreenCommand(_testDev, TestDevScreenEnum.PANEL_SETUP));
            rowData.set(col, new Boolean(false));
            _fileWriter.appendThreshold(_subtype,algorithmSetting, _algorithmEnum,new Boolean(false));
          }
          catch (XrayTesterException ex)
          {
            Assert.expect(false);
          }
          finally
          {
            if (setCommand != null)
              setCommand = null;
          }
        }
      }
      else if ((algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES)))
      {
        //XCR-3456, Able to copy threshold for "masking void method" to true even target subtype do not have mask image
        boolean isFalse = Boolean.parseBoolean((String)value) == false;
        if (isFalse || checkIfMaskImageExist(_subtype))
        {
          SubtypeSetTunedValueCommand setCommand = new SubtypeSetTunedValueCommand(_subtype, algorithmSetting.getAlgorithmSettingEnum(), (String)value);
          try
          {
            Algorithm algorithm = InspectionFamily.getAlgorithm(_inspectionFamilyEnum, _algorithmEnum);
            _commandManager.trackState(_algoTunerGui.getCurrentUndoState(row, algorithm, algorithmSetting));
            _commandManager.execute(setCommand);//, new TestDevChangeScreenCommand(_testDev, TestDevScreenEnum.PANEL_SETUP));
            
            rowData.set(col, (String)value);
            _fileWriter.appendThreshold(_subtype,algorithmSetting, _algorithmEnum,(Serializable)oldValue,(Serializable)value);
          }
          catch (XrayTesterException ex)
          {
            Assert.expect(false);
          }
          finally
          {
            if (setCommand != null)
              setCommand = null;
          }
        }
      }
      else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.STITCH_COMPONENT_IMAGE_AT_VVTS))
      {
        //Siew Yeng - XCR-3094
        Set<Subtype> affectedSubtypes = new HashSet(); 
        for(ComponentType compType : _subtype.getComponentTypes())
        {
          affectedSubtypes.addAll(compType.getSubtypes(_inspectionFamilyEnum));
        }
        
        if(affectedSubtypes.isEmpty() == false)
        {
          int status = JOptionPane.OK_OPTION;
          if(affectedSubtypes.size() > 1)
          {
            String message = StringLocalizer.keyToString("ATGUI_CHANGING_THRESHOLDS_AFFECT_OTHER_SUBTYPES_KEY");
            
            //Siew Yeng - XCR-3210 - BGA 1x1 with mixed subtypes need to have same number of user-defined slice
            //remove this to allow to have different user defined slice for mixed subtypes
//            if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE) &&
//              _subtype.getPanel().getProject().isBGAInspectionRegionSetTo1X1())
//            {
//              message += StringLocalizer.keyToString(new LocalizedString("ATGUI_MANUAL_CHANGE_OFFSET_FROM_MIDBALL_FOR_OTHER_SUBTYPES_KEY", 
//                                                                          new Object[] {affectedSubtypes}));
//            }
            
            
            status = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(), 
            message,
            StringLocalizer.keyToString("MM_GUI_TDT_TITLE_TUNER_KEY"), 
            JOptionPane.OK_CANCEL_OPTION, 
            JOptionPane.QUESTION_MESSAGE);
          }

          if(status == JOptionPane.CLOSED_OPTION || status == JOptionPane.CANCEL_OPTION)
          {
            _setValueValid = true;
            affectedSubtypes.clear();
            return;
          }
          else if(status == JOptionPane.OK_OPTION)
          {
            try
            {
              Algorithm algorithm = InspectionFamily.getAlgorithm(_inspectionFamilyEnum, _algorithmEnum);
              UndoState undoState = _algoTunerGui.getCurrentUndoState(row, algorithm, algorithmSetting);
              _commandManager.trackState(undoState);
              _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_SET_VALUE_KEY"));
              for(Subtype subtype : affectedSubtypes)
              {
                SubtypeSetTunedValueCommand setCommand = new SubtypeSetTunedValueCommand(subtype, algorithmSetting.getAlgorithmSettingEnum(), (String)value);
                _commandManager.execute(setCommand);//, new TestDevChangeScreenCommand(_testDev, TestDevScreenEnum.PANEL_SETUP));

                //append threshold value into history log-hsia-fen.tan
                _fileWriter.appendThreshold(subtype, algorithmSetting,_algorithmEnum, (Serializable) oldValue, (Serializable)value);
              }
              affectedSubtypes.clear();
              _commandManager.endCommandBlock();
              rowData.set(col, (String)value);
            }
            catch (XrayTesterException ex)
            {
              // shouldn't ever happen - Siew Yeng - copy from else condition below
              Assert.expect(false);
            }
          }
        }
      }
      else
      {
        //Siew Yeng - XCR-2764 - Sub-subtype feature
        if (algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_METHOD))
        {
          String voidMethod = (String)value;
          if(_subtype.hasSubSubtypes() && voidMethod.equals(LargePadVoidingAlgorithm._VARIABLE_PAD_METHOD))
          {
            SwingUtils.invokeLater(new Runnable()
            {
               public void run()
               {
                 MessageDialog.showWarningDialog(MainMenuGui.getInstance(),
                                                 StringLocalizer.keyToString("ATGUI_VARIABLE_PAD_METHOD_NOT_SUPPORT_SUBREGION_WARNING_MESSAGE_KEY"), 
                                                 StringLocalizer.keyToString("ATGUI_SUBREGION_TITLE_KEY"));
               }
            });   
            _setValueValid = true;
            return;
          }
        }
        else if(algorithmSetting.getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VERSION))
        {
          //Siew Yeng - XCR-3107 - Warning message should be prompt if user trying to use subregion with algorithm version 1
          if(valStr.equals("1") && _subtype.hasSubSubtypes())
          {
            int status = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(), 
                StringLocalizer.keyToString("ATGUI_LARGEPAD_VOIDING_VERSION_NOT_SUPPORT_SUBREGION_FEATURE_KEY"),
                StringLocalizer.keyToString("MM_GUI_TDT_TITLE_TUNER_KEY"), 
                JOptionPane.OK_CANCEL_OPTION, 
                JOptionPane.QUESTION_MESSAGE);

            if(status == JOptionPane.CANCEL_OPTION || status == JOptionPane.CLOSED_OPTION)
            {
              _setValueValid = true;
              return;
            } 
          }
        }
        
        SubtypeSetTunedValueCommand setCommand = new SubtypeSetTunedValueCommand(_subtype, algorithmSetting.getAlgorithmSettingEnum(), (String)value);
        try
        {
          Algorithm algorithm = InspectionFamily.getAlgorithm(_inspectionFamilyEnum, _algorithmEnum);
          _commandManager.trackState(_algoTunerGui.getCurrentUndoState(row, algorithm, algorithmSetting));
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_SET_VALUE_KEY"));
          _commandManager.execute(setCommand);//, new TestDevChangeScreenCommand(_testDev, TestDevScreenEnum.PANEL_SETUP));
          if (_subtype.getPanel().doAllSubtypesUsePredictiveSliceHeight())
          {
            // pop a warning that the project is in a state that will not allow any images to be generated by predicting from neighbors
           MainMenuGui.getInstance().displayWarningForZeroNeighborsToPredictFrom();
          }
          rowData.set(col, (String)value);
          
          //Siew Yeng - XCR-2764 - Sub-subtype feature
          if(_subtype.hasSubSubtypes())
          {
            List<AlgorithmSettingEnum> ignoreList = algorithm.getAlgorithmSettingEnumsIgnoreListForSubSubtype();
            if(ignoreList != null && ignoreList.contains(algorithmSetting.getAlgorithmSettingEnum()))
            {
              for(Subtype subSubtype: _subtype.getSubSubtypes())
              {
                setCommand = new SubtypeSetTunedValueCommand(subSubtype, algorithmSetting.getAlgorithmSettingEnum(), (String)value);
                _commandManager.execute(setCommand);
              }
            }
          }
          _commandManager.endCommandBlock();
           if (_isSliceSetuptab == false)
             _fileWriter.appendThreshold(_subtype,algorithmSetting,_algorithmEnum,(Serializable)oldValue,(Serializable)value);
           else
             _fileWriter.appendThresholdSliceSetup(_subtype,algorithmSetting,(Serializable)oldValue,(Serializable)value);
        }
        catch (XrayTesterException ex)
        {
          Assert.expect(false);
        }
        finally
        {
          if (setCommand != null)
            setCommand = null;
        }
      }
    }
    else if (value instanceof java.util.List)// it's a list of string
    {
      rowData.set(VALUE_COLUMN, value);
      _subtype.setTunedValue(algorithmSetting.getAlgorithmSettingEnum(), (ArrayList)value);
    }
    else
      Assert.expect(false);

    _setValueValid = true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isCellEditable(int row, int col)
  {
    if (_testRunning)
      return false;

    //Siew Yeng - XCR-2764 - Sub-subtype feature
    if(_subtype.isSubSubtype())
    {
      List<Object> rowData = _displayData.get(row);
      AlgorithmSetting algorithmSetting = (AlgorithmSetting) rowData.get(NAME_COLUMN);
      Algorithm algorithm = InspectionFamily.getAlgorithm(_inspectionFamilyEnum, _algorithmEnum);
      if(algorithm.getAlgorithmSettingEnumsIgnoreListForSubSubtype() != null)
      {
        if(algorithm.getAlgorithmSettingEnumsIgnoreListForSubSubtype().contains(algorithmSetting.getAlgorithmSettingEnum())&& 
           col == VALUE_COLUMN)
        {
          return false;
        }
      }
    }
    
    if (col == VALUE_COLUMN)
      return true;
    else
      return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getRowCount()
  {
    if (_displayData == null)
      return 0;
    else
      return _displayData.size();
  }

  /**
   * @author Andy Mechtenberg
   */
  void setTestRunning(boolean testRunning)
  {
    _testRunning = testRunning;
  }
  
  /**
   * @author Jack Hwee
   */
  private boolean checkIfMaskImageExist(Subtype subtype)
  {
    boolean maskImageExist = false;
     
    if (subtype.checkIfMaskImageExist())
    {
      maskImageExist = true;
    }
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          String message = StringLocalizer.keyToString("ATGUI_THRESHOLD_NO_MASK_IMAGE_KEY");
          JOptionPane.showMessageDialog(
          MainMenuGui.getInstance(),
          message, StringLocalizer.keyToString("ATGUI_SHOW_MASK_IMAGE_DIALOG_TITLE_KEY"),
          JOptionPane.DEFAULT_OPTION);
        }
      });        
//         List<Object> rowData = _displayData.get(2);
//         rowData.set(1, "False");
    }
    return maskImageExist;
  }
}
