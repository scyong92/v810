package com.axi.v810.gui.algoTuner;

import java.awt.image.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.gridArray.*;
import com.axi.v810.business.imageAnalysis.exposedPad.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 *
 * <p>Title: ImagePadSelection</p>
 *
 * <p>Description: Class to hold a pad reference and inspection image for the
 * FineTuningImageGraphicsEngineSetup to display.  It is used by Review Defects
 * to display the joint inspection image and to highlight the selected pad.
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class ImagePadSelection
{
  String _jointName = null;
  private BufferedImage _bufferedImage = null;
  private ReconstructionRegion _reconstructionRegion = null;
  private SliceNameEnum _sliceNameEnum = null;
  private MeasurementRegionDiagnosticInfo _measurementDiagInfo = null;

  private Map<SliceNameEnum, BufferedImage>_allSliceBufferedImage = new HashMap<SliceNameEnum, BufferedImage>();
  // Ying-Huan.Chu
  private List<Float>_zHeightInNanometerList = new ArrayList<>();
  private List<Float>_sharpnessList = new ArrayList<>();
  private int _zHeightInNanos;
  
  // bee-hoon.goh
  private Component _component = null;
  private ImageSetData _imageSetDir = null;
  private JointInspectionData _jointInspectionData = null;
  private MeasurementRegionDiagnosticInfo _componentMeasurementDiagInfo = null;
  
  /**
   * @author George Booth
   */
  public ImagePadSelection(String jointName)
  {
    Assert.expect(jointName != null);
    _jointName = jointName;
  }

  /**
   * @author George Booth
   */
  public String getJointName()
  {
    return _jointName;
  }
  
  public Map<SliceNameEnum, BufferedImage> getAllSliceBufferedImage()
  {
    return _allSliceBufferedImage;
  }
  
  public BufferedImage getBufferedImageBySlice(SliceNameEnum sliceNameEnum)
  {
    return _allSliceBufferedImage.get(sliceNameEnum);
  }
  
  /**
   * @author George Booth
   */
  public void setImage(Image image)
  {
    Assert.expect(image != null);
    Image imageCopy = new Image(image);
    ImageEnhancement.autoEnhanceContrastInPlace(imageCopy);
    _bufferedImage = imageCopy.getBufferedImage();
    imageCopy.decrementReferenceCount();
  }

  /**
   * @author George Booth
   * - implemented auto enhance contrast.
   * @author Wei Chin, Chong
   * - implemented Contrast Enhancement (Histogram equalization) feature based on threshold.
   * @author Chin Seong, Kee
   * - Implement setImage for all slice, so that all slice images can be saved at once.
   * @author Seng Yew, Lim
   * - Refactor setImage to accept sliceNameEnum as null
   * - Check if needed first before preparing correctedImages container. Not efficient.
   * @author Khang Wah, Chnee
   * - remove 2.5d images from any contrast enhancement activity
   * @author Lim, Lay Ngor - Fix bug: Set non-resize & non-enhance image for short  
   * base on isAbleToPerformResize.
   */
  public void setImage(ReconstructedImages reconstructedImages, SliceNameEnum sliceNameEnum, boolean isAbleToPerformResize)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(sliceNameEnum != null);

    Map<SliceNameEnum, Image> correctedImages = new HashMap<SliceNameEnum, Image>();

    //Lim, Lay Ngor - Fix bug: Set non-resize & non-enhance image for short  
    //base on isAbleToPerformResize.
    if(isAbleToPerformResize == true)
    {
      for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
      {
        correctedImages.put(reconstructedSlice.getSliceNameEnum(), new Image(reconstructedSlice.getOrthogonalImage()));
      }
    }
    else
    {
      for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
      {
        correctedImages.put(reconstructedSlice.getSliceNameEnum(), new Image(reconstructedSlice.getOrthogonalImageWithoutEnhanced()));
      }      
    }

    ImageEnhancement.autoEnhanceContrastInPlace(ImageManager.getInstance().excludeUnnecessaryImageFromEnhancement(correctedImages).values());

    // wei chin added if enhancement Needed.
    boolean needToEnhanceImage = false;
    for(Subtype subtype: reconstructedImages.getReconstructionRegion().getSubtypes() )
    {
      if(subtype.getInspectionFamilyEnum().equals(InspectionFamilyEnum.GRID_ARRAY))
      {
        if( GridArrayMeasurementAlgorithm.isContrastEnhancementEnabled(subtype) )
        {
          needToEnhanceImage = true;
          break;
        }
      }
      else if(subtype.getInspectionFamilyEnum().equals(InspectionFamilyEnum.EXPOSED_PAD))
      {
        if( ExposedPadMeasurementAlgorithm.isContrastEnhancementEnabled(subtype) )
        {
          needToEnhanceImage = true;
          break;
        }
      }
    }

    if(needToEnhanceImage)
    {
      for(ReconstructedSlice slice : reconstructedImages.getReconstructedSlices())
      {
        // Khang Wah, to remove 2.5d image from any contrast enhancement activity
        if(slice.getSliceNameEnum().isMultiAngleSlice()==false)
        AlgorithmUtil.enhanceContrastByEqualizingHistogram(correctedImages.get(slice.getSliceNameEnum()));
      }
    }

    _bufferedImage = correctedImages.get(sliceNameEnum).getBufferedImage();
    for(ReconstructedSlice slice : reconstructedImages.getReconstructedSlices())
    {
      _allSliceBufferedImage.put(slice.getSliceNameEnum(), correctedImages.get(slice.getSliceNameEnum()).getBufferedImage());
      //Ying-Huan.Chu
      if (slice.getZHeightArray().isEmpty() == false && slice.getSharpnessArray().isEmpty() == false && slice.getSliceNameEnum().equals(sliceNameEnum))
      {
        _zHeightInNanometerList = slice.getZHeightArray();
        _sharpnessList = slice.getSharpnessArray();
        _zHeightInNanos = slice.getHeightInNanometers();
      }
    }

    // bee-hoon.goh
    _component = reconstructedImages.getReconstructionRegion().getComponent();
    
    for (Image image : correctedImages.values())
      image.decrementReferenceCount();
  }


  /**
   * @author George Booth
   */
  public BufferedImage getBufferedImage()
  {
    Assert.expect(_bufferedImage != null);

    return _bufferedImage;
  }

  /**
   * @author George Booth
   */
  void setReconstructionRegion(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);

    _reconstructionRegion = reconstructionRegion;
  }

  /**
   * @author George Booth
   */
  public ReconstructionRegion getReconstructionRegion()
  {
    Assert.expect(_reconstructionRegion != null);

    return _reconstructionRegion;
  }

  /**
   * @author George Booth
   */
  void setSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);

    _sliceNameEnum = sliceNameEnum;
  }

  /**
   * @author George Booth
   */
  public SliceNameEnum getSliceNameEnum()
  {
    Assert.expect(_sliceNameEnum != null);

    return _sliceNameEnum;
  }

  /**
   * @author George Booth
   */
  void setMeasurementDiagInfo(MeasurementRegionDiagnosticInfo measurementDiagInfo)
  {
    Assert.expect(measurementDiagInfo != null);

    _measurementDiagInfo = measurementDiagInfo;
  }

  /**
   * @author George Booth
   */
  public MeasurementRegionDiagnosticInfo getMeasurementDiagInfo()
  {
    Assert.expect(_measurementDiagInfo != null);

    return _measurementDiagInfo;
  }

  /**
   * Get image z-height list for Sharpness Profile.
   * @author Ying-Huan.Chu
   */
  public List<Float> getZHeightInNanometerList()
  {
    Assert.expect(_zHeightInNanometerList != null);
    return _zHeightInNanometerList;
  }
  
  /**
   * Get image sharpness list for Sharpness Profile.
   * @author Ying-Huan.Chu
   */
  public List<Float> getSharpnessList()
  {
    Assert.expect(_sharpnessList != null);
    return _sharpnessList;
  }
  
  /**
   * Get z-height with the maximum sharpness in nanometer as specified in the image header for Sharpness Profile.
   * @author Ying-Huan.Chu
   */
  public int getZHeightInNanos()
  {
    return _zHeightInNanos;
  }
  
  /**
   * Get selected component for component images stitching
   * @author bee-hoon.goh
   */
  public Component getSelectedComponent()
  {
    Assert.expect(_component != null);
    
    return _component;
  }
  
  /**
   * @author bee-hoon.goh
   */
  public ImageSetData getImageSetData() 
  {
    return _imageSetDir;
  }

  /**
   * @author bee-hoon.goh
   */
  public void setImageSetData(ImageSetData imageSetDir) 
  {  
    _imageSetDir = imageSetDir;
  }

  /**
   * @author bee-hoon.goh
   */
  public MeasurementRegionDiagnosticInfo getComponentMeasurementDiagInfo()
  {
    Assert.expect(_componentMeasurementDiagInfo != null);

    return _componentMeasurementDiagInfo;
  }
  
  /**
   * @author bee-hoon.goh
   */
  void setComponentMeasurementDiagInfo(MeasurementRegionDiagnosticInfo componentMeasurementDiagInfo)
  {
    Assert.expect(componentMeasurementDiagInfo != null);

    _componentMeasurementDiagInfo = componentMeasurementDiagInfo;
  }
  
    /**
   * @author bee-hoon.goh
   */
  public JointInspectionData setJointInspectionData() 
  {
    return _jointInspectionData;
  }

  /**
   * @author bee-hoon.goh
   */
  public void setJointInspectionData(JointInspectionData jointInspectionData) 
  {  
    _jointInspectionData = jointInspectionData;
  }
}
