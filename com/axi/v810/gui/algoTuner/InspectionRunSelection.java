package com.axi.v810.gui.algoTuner;

import java.util.*;

import com.axi.util.*;

public class InspectionRunSelection
{
  private List<Long> _inspectionRuns;

  /**
   * @author George Booth
   */
  InspectionRunSelection(List<Long> inspectionRuns)
  {
    Assert.expect(inspectionRuns != null);

    _inspectionRuns = inspectionRuns;
  }

  /**
   * @author George Booth
   */
  List<Long> getInspectionRuns()
  {
    Assert.expect(_inspectionRuns != null);
    List<Long> inspectionRuns = new ArrayList<Long>();
    for (Long runID : _inspectionRuns)
    {
      inspectionRuns.add(runID);
    }
    return inspectionRuns;
  }

  /**
   * @author George Booth
   */
  int getInspectionRunsCount()
  {
    Assert.expect(_inspectionRuns != null);
    return _inspectionRuns.size();
  }
}
