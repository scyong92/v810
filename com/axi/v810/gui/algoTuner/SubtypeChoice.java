package com.axi.v810.gui.algoTuner;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class contains the user choices for Subtype
 * The choices are either a particular Subtype or "All"
 * @author Andy Mechtenberg
 */
public class SubtypeChoice implements Serializable
{
  private Subtype _subtype;
  private boolean _all;

  /**
   * @author Andy Mechtenberg
   */
  public SubtypeChoice(Subtype subtype)
  {
    Assert.expect(subtype != null);
    _subtype = subtype;
    _all = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public SubtypeChoice()
  {
    _all = true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isAllSubtypes()
  {
    return _all;
  }

  /**
   * @author Andy Mechtenberg
   */
  public Subtype getSubtype()
  {
    Assert.expect(_all == false);
    Assert.expect(_subtype != null);
    return _subtype;
  }

  /**
   * Objects are equal if they specify the same custom algorithm family
   * @author Andy Mechtenberg
   */
  public boolean equals(Object rhs)
  {
    if (rhs == null)
      return false;

    if (rhs == this)
      return true;

    if (rhs instanceof SubtypeChoice)
    {
      SubtypeChoice caf = (SubtypeChoice)rhs;

      // if both specify ALL, then they are equal
      if((_all) && (caf._all))
        return true;
      else if(_all != caf._all) // if one specifies all, and the other does not, they are not equal
        return false;
      else
      {
        // if they both specify the same specific custom algorithm family, they are equal
        if (_subtype.equals(caf._subtype))
          return true;
        else
          return false;
      }
    }
    else  // not an instance of CustomAlgorithmFamilyChoice
      return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int hashcode()
  {
    if (_all)
      return -1;
    else
      return _subtype.hashCode();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    if (_all)
      return StringLocalizer.keyToString("ATGUI_ALL_KEY");
    else
      return _subtype.getShortName();
  }
}
