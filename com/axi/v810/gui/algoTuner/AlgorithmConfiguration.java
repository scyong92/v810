package com.axi.v810.gui.algoTuner;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.FileUtilAxi;
import com.axi.v810.util.*;

/**
 * This class handles the data aspect of all algorithm configuration
 * @author Andy Mechtenberg
 * @author Matt Wharton
 */
public class AlgorithmConfiguration implements Serializable
{
  private SortedSet<JointTypeAlgorithmPair> _jointTypesAndAlgorithmsWithDiagnostics = null;
  private SortedSet<JointTypeAlgorithmPair> _disabledJointTypesAndAlgorithms = null;

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  public AlgorithmConfiguration()
  {
    // initialize all the data structures with a JointTypeAlgorithmPair, which implements Comparator.
    // This way, we won't get any duplicate entries based on the Family-Algo
    _jointTypesAndAlgorithmsWithDiagnostics = new TreeSet<JointTypeAlgorithmPair>(new JointTypeAlgorithmPair());
    _disabledJointTypesAndAlgorithms = new TreeSet<JointTypeAlgorithmPair>(new JointTypeAlgorithmPair());
  }

  /**
   * This deliberately does not use datastore to access the file.
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  static AlgorithmConfiguration readSettingsPersistanceFile()
  {
    // create the settings instance
    AlgorithmConfiguration algoConfig;
    try
    {
      if (FileUtilAxi.exists(FileName.getAlgorithmDiagnosticSettingPersistFullPath()))
      {
        algoConfig = (AlgorithmConfiguration)FileUtilAxi.loadObjectFromSerializedFile(FileName.getAlgorithmDiagnosticSettingPersistFullPath());

        // Make sure we still have Comparators attached to our TreeSets after de-serializing.
        // Recreate the sets if not.
        if (algoConfig._jointTypesAndAlgorithmsWithDiagnostics.comparator() == null)
        {
          algoConfig._jointTypesAndAlgorithmsWithDiagnostics = new TreeSet<JointTypeAlgorithmPair>(new JointTypeAlgorithmPair());
        }
        if (algoConfig._disabledJointTypesAndAlgorithms.comparator() == null)
        {
          algoConfig._disabledJointTypesAndAlgorithms = new TreeSet<JointTypeAlgorithmPair>(new JointTypeAlgorithmPair());
        }
      }
      else
        algoConfig = new AlgorithmConfiguration();
    }
    catch(XrayTesterException e)  // the file serialization must have changed
    {
      algoConfig = new AlgorithmConfiguration();
      try
      {
        algoConfig.writeSettingsPersistanceFile();
      }
      catch (Exception ex)
      {
        // do nothing, as this is just to re-set the file.  It's ok if it fails
      }

    }
    return algoConfig;
  }

  /**
   * This deliberately does not use datastore to access the file.
   * @author Andy Mechtenberg
   */
  void writeSettingsPersistanceFile() throws FileNotFoundException, IOException
  {
    ObjectOutputStream outputStream = null;
    FileOutputStream out = new FileOutputStream(FileName.getAlgorithmDiagnosticSettingPersistFullPath());
    outputStream = new ObjectOutputStream(out);
    outputStream.writeObject(this);
    outputStream.flush();
    if (outputStream != null)
      outputStream.close();
  }

  /**
   * Adds the joint type/algo and diagnostic settings to the diagnostics map
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  void addDiagnostics(JointTypeAlgorithmPair jtap)
  {
    Assert.expect(jtap != null);
    Assert.expect(_jointTypesAndAlgorithmsWithDiagnostics != null);

    _jointTypesAndAlgorithmsWithDiagnostics.add(jtap);
  }

  /**
   * Adds the joint type/algo pair to the algorithm off set
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  void addDisabled(JointTypeAlgorithmPair jtap)
  {
    Assert.expect(jtap != null);
    Assert.expect(_disabledJointTypesAndAlgorithms != null);

    _disabledJointTypesAndAlgorithms.add(jtap);
  }

  /**
   * TRUE if any settings in the diagnostics set
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  boolean anyDiagnosticsEnabled()
  {
    Assert.expect(_jointTypesAndAlgorithmsWithDiagnostics != null);

    return (_jointTypesAndAlgorithmsWithDiagnostics.isEmpty() == false);
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  void removeDiagnostic(JointTypeAlgorithmPair jtap)
  {
    Assert.expect(jtap != null);
    Assert.expect(_jointTypesAndAlgorithmsWithDiagnostics != null);

    _jointTypesAndAlgorithmsWithDiagnostics.remove(jtap);
  }

  /**
   * This returns the data in the Diagnotics set in a List.
   * @return a List of FamilyAlgorithmPairs
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  Collection<JointTypeAlgorithmPair> getDiagnosticFamilyAlgorithmPairs()
  {
    Assert.expect(_jointTypesAndAlgorithmsWithDiagnostics != null);

    return _jointTypesAndAlgorithmsWithDiagnostics;
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  void removeAllDiagnostics()
  {
    Assert.expect(_jointTypesAndAlgorithmsWithDiagnostics != null);

    _jointTypesAndAlgorithmsWithDiagnostics.clear();
  }

  /**
   * TRUE if any settings in the disabled set
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  boolean anyAlgorithmDisabled()
  {
    Assert.expect(_disabledJointTypesAndAlgorithms != null);

    return (_disabledJointTypesAndAlgorithms.isEmpty() == false);
  }

  /**
   * This returns the data in the On Off set in a List.
   * @return a List of FamilyAlgorithmPairs
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  Collection<JointTypeAlgorithmPair> getDisabledFamilyAlgorithmPairs()
  {
    Assert.expect(_disabledJointTypesAndAlgorithms != null);

    return _disabledJointTypesAndAlgorithms;
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  void removeDisabled(JointTypeAlgorithmPair jtap)
  {
    Assert.expect(jtap != null);
    Assert.expect(_disabledJointTypesAndAlgorithms != null);

    _disabledJointTypesAndAlgorithms.remove(jtap);
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  void removeAllDisabled()
  {
    Assert.expect(_disabledJointTypesAndAlgorithms != null);

    _disabledJointTypesAndAlgorithms.clear();
  }
}
