package com.axi.v810.gui.algoTuner;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.filechooser.FileFilter;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * The panel is used to display similar measurement data used to finetune a
 * threashold.
 * @author Rex Shang
 */
public class ReviewMeasurementsPanel extends AbstractTunerTaskPanel implements Observer
{
  private String _me = "ReviewMeasurementsPanel";

  private MainMenuGui _mainUI;
  private TestDev _testDev;

  private MainUIMenuBar _menuBar;
  private int _menuRequesterID;
  private MainUIToolBar _envToolBar;
  private int _envToolBarRequesterID;

  private JMenuItem _deleteSelectedInspectionRunsMenuItem;
  private JMenuItem _generateReportMenuItem;
  private JButton _generateReportButton;

  private JScrollPane _measurementTableScrollPane;
  private JPanel _measurementFilterPanel;
  private JComboBox _measurementTypeComboBox;
  private JComboBox _sliceNameComboBox;
  private JLabel _measurementTypeLabel;
  private JLabel _sliceNameLabel;
  private JScrollPane _inspectionRunListScrollPane;
  private JSplitPane _inspectionRunListSplitPane;
  private InspectionRunList _inspectionRunList;
  private JPanel _northPanel;
  private JPanel _anotherPanel;
  private JPanel _testDevChartPanel;
  private JLabel _mustSelectJointTypeAndSubtypeLabel;

  private ReviewMeasurementsChartPanel _reviewMeasurementsChartPanel;

  private ReviewMeasurementsTableModel _reviewMeasurementsTableModel;
  private JSortTable _reviewMeasurementsTable;

  private ActionListener _measurementTypeComboBoxListener;
  private ActionListener _sliceNameComboBoxListener;

  private int _reviewMeasurementsResizeDivider = -1;
  private final int _INSPECTION_LIST_VISIBLE_ROWS = 5;

  // for Generate Reports
  private JFileChooser _fileChooser = null;
  private String _inputDirectory = null;
  private static final String _reportFileFilterDescription =
      StringLocalizer.keyToString("RDGUI_GENERATE_REPORT_FILE_DESCRIPTION_KEY");

  private static final int _TABLE_INFORMATION_MINIMUM_HEIGHT = 100;

  private JointResultsManager _jointResultsManager;

  private JointTypeEnum _jointType;
  private Subtype _subtype;
  private MeasurementEnum _measurementType;
  private SliceNameEnum _sliceName;
  private String _projectName;
  private boolean _currentPanelIsActive;
  private List<Long> _selectedInspectionRuns;
  private boolean _shouldInteractWithGraphics = false;
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
  // Joint image viewer
  private JointImageViewer _jointImageViewer;
  private JointResultsEntry _selectedJointResultsEntry;
  private JointResultsEntry _lastSelectedJointResultsEntry;

  // Measurement selection made in ReviewDefectsPanel.
  private MeasurementSelection _measurementSelection;

  // measurement selections made in the threshold table -- assume current subtype and inspection run
  private Pair<SliceNameEnum, MeasurementEnum> _measurementSlicePair;
  
  //Ying-Huan.Chu
  private List<MeasurementEnum> _measurementEnumList = new ArrayList<MeasurementEnum>();   

  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  
  private int _NUM_JOINTS_TO_UPDATE_POP_UP_PROGRESS_BAR = 100;
  //Wong Ngie Xing
  private JMenu _runMenu = new JMenu();
  private JMenuItem _startTestMenuItem = new JMenuItem();

  //Ying-Huan.Chu [XCR-2626] - Display New Subtype Dialog in Measurement tab
  private ChangeSubtypeDialog _changeSubtypeDialog = null;

  /**
   * @author Rex Shang
   */
  public ReviewMeasurementsPanel(TestDev testDev)
  {
    Assert.expect(testDev != null);

    debug("ReviewMeasurementsPanel(TestDev)");

    _testDev = testDev;

    _menuRequesterID = -1;
    _envToolBarRequesterID = -1;

    _mainUI = MainMenuGui.getInstance();

    _jointType = null;
    _subtype = null;
    _measurementType = null;
    _sliceName = null;
    _projectName = null;
    _selectedInspectionRuns = new ArrayList<Long>();
    _measurementSelection = null;
    _measurementSlicePair = null;

    _jointResultsManager = JointResultsManager.getInstance();

    jbInit();

    _reviewMeasurementsTableModel.addTableModelListener(new TableModelListener()
    {
      public void tableChanged(TableModelEvent e)
      {
        reviewMeasurementsTableModelChanged(e);
      }
    });

    // Add this as an observer of the gui so we can listen to Review Defect
    // table selections even when we are not in view.
    _guiObservable.addObserver(this);
    _inspectionEventObservable.addObserver(this);
  }

  /**
   * Used for prototyping and testing.
   * @author Rex Shang
   */
  private ReviewMeasurementsPanel(String projectName, Subtype subtype, MeasurementEnum measurementType,
                            SliceNameEnum sliceName)
  {
    debug("ReviewMeasurementsPanel()");

    _projectName = projectName;
    _subtype = subtype;
    _measurementType = measurementType;
    _sliceName = sliceName;

    jbInit();
  }

  /**
   * @author Andy Mechtenberg
   */
  void unpopulate()
  {
    _subtype = null;
    _reviewMeasurementsTableModel.clearTableModel();
    if (_inspectionRunList != null)
      _inspectionRunList.unpopulate();
    if (_jointImageViewer != null)
      _jointImageViewer.clearImageSetNameMap();
  }

  /**
   * This string will get displayed as the tab name when added to a tabbed pane.
   * @author Rex Shang
   */
  public String getTunerTaskName()
  {
    return StringLocalizer.keyToString("ATGUI_REVIEW_MEASUREMENT_KEY");
  }

  /**
   * @author Rex Shang
   */
  private void jbInit()
  {
    //Wong Ngie Xing
    //Added Menu Item in order to call Start Test from ReviewMeasurementsPanel
    _startTestMenuItem.setText(StringLocalizer.keyToString("ATGUI_START_TEST_MENU_KEY"));
    _startTestMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
    _startTestMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        startTestFromOtherPanel();
      }
    });
    _runMenu.setActionCommand(StringLocalizer.keyToString("ATGUI_RUN_MENU_KEY"));
    _runMenu.setText(StringLocalizer.keyToString("ATGUI_RUN_MENU_KEY"));

    _reviewMeasurementsChartPanel = new ReviewMeasurementsChartPanel();

    _deleteSelectedInspectionRunsMenuItem = new JMenuItem();
    _deleteSelectedInspectionRunsMenuItem.setText(StringLocalizer.keyToString("RDGUI_DELETE_INSPECTION_RUNS_MENU_KEY"));
    _deleteSelectedInspectionRunsMenuItem.setEnabled(false);
    _deleteSelectedInspectionRunsMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _inspectionRunList.deleteSelectedInspectionRuns();
      }
    });

    _generateReportMenuItem = new JMenuItem();
    _generateReportMenuItem.setText(StringLocalizer.keyToString("RMGUI_GENERATE_REPORT_MENU_KEY"));
    _generateReportMenuItem.setEnabled(false);
    _generateReportMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        generateReport_actionPerformed();
      }
    });

    _generateReportButton = new JButton();
//    _generateReportButton.setText(StringLocalizer.keyToString("RMGUI_GENERATE_REPORT_BUTTON_KEY"));
    _generateReportButton.setIcon(Image5DX.getImageIcon(Image5DX.GENERATE_REPORT));
    _generateReportButton.setToolTipText(StringLocalizer.keyToString("RMGUI_GENERATE_REPORT_BUTTON_TOOLTIP_KEY"));
    _generateReportButton.setEnabled(false);
    _generateReportButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        generateReport_actionPerformed();
      }
    });

    _sliceNameLabel = new JLabel(StringLocalizer.keyToString("RMGUI_SLICE_NAME_LABEL_KEY"));
    _sliceNameComboBoxListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        sliceNameComboBoxActionPerformed(e);
      }
    };
    _sliceNameComboBox = new JComboBox();
    FontMetrics fm = _sliceNameComboBox.getFontMetrics(_sliceNameComboBox.getFont());
    _sliceNameComboBox.setMinimumSize(new Dimension(150, fm.getHeight()));
    _sliceNameComboBox.addActionListener(_sliceNameComboBoxListener);

    _measurementTypeLabel = new JLabel(StringLocalizer.keyToString("RMGUI_MEASUREMENT_TYPE_LABEL_KEY"));
    _measurementTypeComboBoxListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        measurementTypeComboBoxActionPerformed(e);
      }
    };
    _measurementTypeComboBox = new JComboBox();
    _measurementTypeComboBox.setMinimumSize(new Dimension(150, fm.getHeight()));
    _measurementTypeComboBox.addActionListener(_measurementTypeComboBoxListener);
//    _measurementTypeComboBox.setMaximumRowCount(30);

    _measurementFilterPanel = new JPanel();
    _measurementFilterPanel.setLayout(new PairLayout());
    _measurementFilterPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    _measurementFilterPanel.add(_sliceNameLabel);
    _measurementFilterPanel.add(_sliceNameComboBox);
    _measurementFilterPanel.add(_measurementTypeLabel);
    _measurementFilterPanel.add(_measurementTypeComboBox);

    _inspectionRunList = new InspectionRunList(_INSPECTION_LIST_VISIBLE_ROWS);
    _inspectionRunListScrollPane = new JScrollPane();
    _inspectionRunListScrollPane.setBorder(new TitledBorder(new EtchedBorder(),
        StringLocalizer.keyToString("RMGUI_INSPECTION_RUN_TITLE_KEY")));
    _inspectionRunListScrollPane.getViewport().add(_inspectionRunList);

    _reviewMeasurementsTableModel = new ReviewMeasurementsTableModel();
    _reviewMeasurementsTable = new JSortTable(_reviewMeasurementsTableModel);
    // add the selection listener for pin information table
    ListSelectionModel reviewMeasurementsTableLSM = _reviewMeasurementsTable.getSelectionModel();
    reviewMeasurementsTableLSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        reviewMeasurementsTable_valueChanged(e);
      }
    });
    _reviewMeasurementsTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _reviewMeasurementsTable.addMouseListener(new MouseAdapter()
    {
      // show context menu on right click
      public void mouseReleased(MouseEvent e)
      {
        if (e.isPopupTrigger())
        {
          handleJointInformationTablePopupMenu(e);
        }
      }
    });

    _reviewMeasurementsTableModel.setColumnWidths(_reviewMeasurementsTable);

    _mustSelectJointTypeAndSubtypeLabel = new JLabel(StringLocalizer.keyToString("ATGUI_MUST_HAVE_FAMILY_SUBTYPE_KEY"));
    _mustSelectJointTypeAndSubtypeLabel.setHorizontalAlignment(JLabel.CENTER);

    _measurementTableScrollPane = new JScrollPane();
    _measurementTableScrollPane.getViewport().add(_reviewMeasurementsTable);

    _anotherPanel = new JPanel();
    _anotherPanel.setLayout(new BorderLayout());
    _anotherPanel.add(_measurementFilterPanel, BorderLayout.WEST);

    _northPanel = new JPanel();
    _northPanel.setLayout(new BorderLayout());
    _northPanel.add(_inspectionRunListScrollPane, BorderLayout.CENTER);
    _northPanel.add(_anotherPanel, BorderLayout.SOUTH);

    _inspectionRunListSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
    _inspectionRunListSplitPane.add(_northPanel, JSplitPane.TOP);
    _inspectionRunListSplitPane.add(_measurementTableScrollPane, JSplitPane.BOTTOM);

    this.setLayout(new BorderLayout());
    this.add(_inspectionRunListSplitPane, BorderLayout.CENTER);
  }
  
  /*
   * @author Wong Ngie Xing
   */
  private void startTestFromOtherPanel()
  {
    _guiObservable.stateChanged(this, GuiUpdateEventEnum.TUNER_INSPECTION_START_FROM_OTHER_PANEL);
  }

  /**
   * @author Rex Shang
   */
  private void getPersistanceSettings()
  {
    TestDevPersistance tunerPersistSettings = _testDev.getPersistance();
    ProjectPersistance projectPersistSettings = _testDev.getPersistance().getProjectPersistance();

    _projectName = Project.getCurrentlyLoadedProject().getName();
    debug("projectPersistSettings.getProjectName() = " + _projectName);

    MeasurementEnum measurementType = projectPersistSettings.getMeasurementType();
    SliceNameEnum sliceName = projectPersistSettings.getSliceName();
    if (measurementType != null && sliceName != null)
    {
      _measurementType = measurementType;
      debug("projectPersistSettings.getMeasurementName() = " + _measurementType.getName());

      _sliceName = projectPersistSettings.getSliceName();
      debug("projectPersistSettings.getSliceName() = " + _sliceName.getName());
    }

    _reviewMeasurementsResizeDivider = tunerPersistSettings.getReviewMeasurementsResizeDivider();
  }

  /**
   * Update the Review Measurement section of algo tuner persistance
   * @author Rex Shang
   */
  void updatePersistanceSettings()
  {
    debug("updatePersistanceSettings(<AlgoTunerPersistance>)");

    TestDevPersistance tunerPersistSettings = _testDev.getPersistance();
    ProjectPersistance projectPersistSettings = _testDev.getPersistance().getProjectPersistance();

    projectPersistSettings.setMeasurementType(_measurementType);
    projectPersistSettings.setSliceName(_sliceName);

    _reviewMeasurementsResizeDivider = _inspectionRunListSplitPane.getDividerLocation();
    tunerPersistSettings.setReviewMeasurementsResizeDivider(_reviewMeasurementsResizeDivider);
  }

  /**
   * @author Rex Shang
   */
  private void populateMeasurementTypeComboBox()
  {
    // Remove action listener before we modify the combo box.
    _measurementTypeComboBox.removeActionListener(_measurementTypeComboBoxListener);

    if (_measurementTypeComboBox.getItemCount() > 0)
      _measurementTypeComboBox.removeAllItems();

    Set<MeasurementEnum> jointMeasurements = null;
    if (_selectedInspectionRuns != null && _jointType != null && _subtype != null && _sliceName != null)
    {
      if (_sliceName.equals(SliceNameEnum.ALL_SLICE) == false)
      {
        // A specific joint type and subtype is given -- do subtype measurement.
        jointMeasurements =
            _jointResultsManager.getMeasurementTypes(_projectName, _selectedInspectionRuns, _subtype, _sliceName);

        for (MeasurementEnum nonVisible : MeasurementEnum.getNonCustomerVisibleMeasurements())
        {
          jointMeasurements.remove(nonVisible);
        }
      }
    }

    if (jointMeasurements == null || jointMeasurements.isEmpty())
    {
      if (_sliceName != null && _sliceName.equals(SliceNameEnum.ALL_SLICE))
      {
        _measurementType = MeasurementEnum.ALL_MEASUREMENT;
        _measurementTypeComboBox.addItem(MeasurementEnum.ALL_MEASUREMENT);
      }
      else
      {
        _measurementType = null;
      }
    }
    else
    {
      if (_measurementEnumList != null)
        _measurementEnumList.clear();
      
      // Ying-Huan.Chu : add 'All' option for Measurement Type.      
      _measurementTypeComboBox.addItem(MeasurementEnum.ALL_MEASUREMENT);
      for (MeasurementEnum measurementEnum : jointMeasurements)
      {
        _measurementTypeComboBox.addItem(measurementEnum);
        _measurementEnumList.add(measurementEnum);
      }

      // Check to see if our _measurementType is a valid option.
      if (_measurementType == null || jointMeasurements.contains(_measurementType) == false)
        _measurementType = jointMeasurements.iterator().next();
    }

    // Re-enable action listener.
    _measurementTypeComboBox.addActionListener(_measurementTypeComboBoxListener);
    _measurementTypeComboBox.setSelectedItem(_measurementType);
  }

  /**
   * @author Rex Shang
   */
  private void populateSliceNameComboBox()
  {
    // Remove action listener before we modify the combo box.
    _sliceNameComboBox.removeActionListener(_sliceNameComboBoxListener);

    if (_sliceNameComboBox.getItemCount() > 0)
      _sliceNameComboBox.removeAllItems();

    Set<SliceNameEnum> jointSliceNames = null;
    if (_selectedInspectionRuns != null && _jointType != null && _subtype != null)
    {
      // A specific joint type and subtype is given -- do subtype slices.
      jointSliceNames =
          _jointResultsManager.getSliceNames(_projectName, _selectedInspectionRuns, _subtype);
    }

    if (jointSliceNames == null || jointSliceNames.size() == 0)
    {
      _sliceName = null;
    }
    else
    {      
      _sliceNameComboBox.addItem(SliceNameEnum.ALL_SLICE);
      for (SliceNameEnum sliceName : jointSliceNames)
      {
        _sliceNameComboBox.addItem(sliceName);
      }

      // Check to see if our _sliceName is a valid option.
      if (_sliceName == null || jointSliceNames.contains(_sliceName) == false)
        _sliceName = jointSliceNames.iterator().next();
    }

    // Re-enable action listener.
    _sliceNameComboBox.addActionListener(_sliceNameComboBoxListener);
    _sliceNameComboBox.setSelectedItem(_sliceName);
  }

  /**
   * @author Rex Shang
   */
  private void populateTableModel()
  {
    _selectedInspectionRuns = _inspectionRunList.getSelectedInspectionRuns();

    if (_reviewMeasurementsTableModel.getRowCount() > 0)
      _reviewMeasurementsTableModel.clearTableModel();

    _measurementTableScrollPane.getViewport().removeAll();

    if (_subtype != null && _measurementType != null && _sliceName != null
        && _selectedInspectionRuns != null && _selectedInspectionRuns.size() > 0)
    {
      _reviewMeasurementsTableModel.populateWithTestedJoints(
          _projectName, _selectedInspectionRuns, _subtype, _measurementType, _sliceName);
      if (JointResultsManager.getInstance().jointResultsReadCancelled())
      {
        // clear selections (show nothing)
        _inspectionRunList.clearSelection();
        return;
      }

      if (_reviewMeasurementsTableModel.getRowCount() > 0)
        _measurementTableScrollPane.getViewport().add(_reviewMeasurementsTable);
      else
      {
        _mustSelectJointTypeAndSubtypeLabel.setText(StringLocalizer.keyToString("RMGUI_NO_MEASUREMENT_DATA_FOUND_KEY"));
        _measurementTableScrollPane.getViewport().add(_mustSelectJointTypeAndSubtypeLabel);
      }
    }
    else
    {
      if (_selectedInspectionRuns.size() < 1)
      {
        _mustSelectJointTypeAndSubtypeLabel.setText(StringLocalizer.keyToString("RMGUI_MUST_HAVE_INPSECTION_RUN_KEY"));
        _measurementTableScrollPane.getViewport().add(_mustSelectJointTypeAndSubtypeLabel);
      }
      else if (_selectedInspectionRuns == null)
      {
        _mustSelectJointTypeAndSubtypeLabel.setText(StringLocalizer.keyToString("RMGUI_MUST_SELECT_INPSECTION_RUN_KEY"));
        _measurementTableScrollPane.getViewport().add(_mustSelectJointTypeAndSubtypeLabel);
      }
      else if (_subtype == null)
      {
        _mustSelectJointTypeAndSubtypeLabel.setText(StringLocalizer.keyToString("ATGUI_MUST_HAVE_FAMILY_SUBTYPE_KEY"));
        _measurementTableScrollPane.getViewport().add(_mustSelectJointTypeAndSubtypeLabel);
      }
      else if (_measurementType == null || _sliceName == null)
      {
        _mustSelectJointTypeAndSubtypeLabel.setText(StringLocalizer.keyToString("RMGUI_NO_MEASUREMENT_DATA_FOUND_KEY"));
        _measurementTableScrollPane.getViewport().add(_mustSelectJointTypeAndSubtypeLabel);
      }
      else
      {
        Assert.expect(false, "Review Measurement table must be display-able.");
      }
    }
  }

  /**
   * We observe project load status and GUI selections from other panel.
   * @author Rex Shang
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof GuiObservable)
        {
          final GuiEvent guiEvent = (GuiEvent)object;
          final GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
          if (guiEventEnum instanceof SelectionEventEnum)
          {
            SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
            if (selectionEventEnum.equals(SelectionEventEnum.TUNER_TEST_SELECTION))
            {
//              System.out.println("RM: SelectionEventEnum.TUNER_TEST_SELECTION seen");
              debug("tuner test state changed");
              // ok, something changed in the tuner selection (joint type, subtype, component, pin)
              // get the data from the object argument
              TunerTestState tunerTestState = (TunerTestState)guiEvent.getSource();
              setCurrentTestSelection(tunerTestState);
            }
            else if (selectionEventEnum.equals(SelectionEventEnum.MEASUREMENT_SELECTION))
            {
              debug("measurement selection state changed");
//              System.out.println("RM: SelectionEventEnum.MEASUREMENT_SELECTION seen");
              _measurementSelection = (MeasurementSelection)guiEvent.getSource();
            }
            else if (selectionEventEnum.equals(SelectionEventEnum.SLICE_MEASUREMENT_SELECTION))
            {
              debug("threshold measurement slice selection state changed");
              _measurementSlicePair = (Pair<SliceNameEnum, MeasurementEnum>)guiEvent.getSource();
              // See if a measurement selection is made from Review Defects panel while
              // we are not active.
              MeasurementEnum measurementType = _measurementSlicePair.getSecond();
              SliceNameEnum sliceName = _measurementSlicePair.getFirst();
              // now, the slice specified may not be available -- check first
              for(int i = 0; i < _sliceNameComboBox.getItemCount(); i++)
              {
                if (_sliceNameComboBox.getItemAt(i) == sliceName)
                {
                  _sliceNameComboBox.setSelectedItem(sliceName);
                  _measurementTypeComboBox.setSelectedItem(measurementType);
                }
              }
              if (_sliceNameComboBox.getSelectedItem() != sliceName)
              {
                // there is a special case for chip, MeasurementEnum.CHIP_MEASUREMENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL --
                // it might exist on the either the Opaque slice or the Clear slice.
                // right now, we default to the Opaque slice for CAP and Clear for Resistor (see special code in AlgoTunerGui)
                // but for Caps, it might be on the Clear instead.  So, here, we'll detect if we found it in the above code,
                // but if not -- we'll switch the slice to the other and try again.

                // first, see if this is the special case we care about -- check for measurementEnum value
                if (measurementType.equals(MeasurementEnum.CHIP_MEASUREMENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL))
                {
                  // first, swap the slice name
                  if (sliceName.equals(SliceNameEnum.OPAQUE_CHIP_PAD))
                    sliceName = SliceNameEnum.CLEAR_CHIP_PAD;
                  else if (sliceName.equals(SliceNameEnum.CLEAR_CHIP_PAD))
                    sliceName = SliceNameEnum.OPAQUE_CHIP_PAD;

                  // now, do the same logic to set the slice name combo box, and then the measurement combo box
                  for (int i = 0; i < _sliceNameComboBox.getItemCount(); i++)
                  {
                    if (_sliceNameComboBox.getItemAt(i) == sliceName)
                    {
                      _sliceNameComboBox.setSelectedItem(sliceName);
                      _measurementTypeComboBox.setSelectedItem(measurementType);
                    }
                  }
                }

              }

            }
            else if (selectionEventEnum.equals(SelectionEventEnum.INSPECTION_RUN_SELECTION))
            {
//              System.out.println("RM: SelectionEventEnum.INSPECTION_RUN_SELECTION seen");
              InspectionRunSelection inspectionRunSelection = (InspectionRunSelection)guiEvent.getSource();
              _selectedInspectionRuns = inspectionRunSelection.getInspectionRuns();
              // update the UI with new selected inspection runs
              if (active())
              {
                _deleteSelectedInspectionRunsMenuItem.setEnabled(_selectedInspectionRuns.size() > 0);
                _projectName = Project.getCurrentlyLoadedProject().getName();
                _reviewMeasurementsChartPanel.clearDataSet();
                populateSliceNameComboBox();
                // inspectionRunList has disabled selection processing; it MUST be re-enabled
                // after all processing is finished
                _guiObservable.stateChanged(null, SelectionEventEnum.INSPECTION_RUN_SELECTION_PROCESSING_COMPLETE);
              }
            }
          }
          else if (guiEventEnum instanceof GuiUpdateEventEnum)
          {
            GuiUpdateEventEnum updateEventEnum = (GuiUpdateEventEnum)guiEventEnum;
            if (updateEventEnum.equals(GuiUpdateEventEnum.INSPECTION_RUNS_UPDATED))
            {
//              System.out.println("RM: GuiUpdateEventEnum.INSPECTION_RUNS_UPDATED seen");
              InspectionRunSelection inspectionRunSelection = (InspectionRunSelection)guiEvent.getSource();
              _selectedInspectionRuns = inspectionRunSelection.getInspectionRuns();
              _deleteSelectedInspectionRunsMenuItem.setEnabled(_selectedInspectionRuns.size() > 0);
              // update the UI with new inspection runs list
              _projectName = Project.getCurrentlyLoadedProject().getName();
              _inspectionRunList.populate(_projectName, _selectedInspectionRuns);
            }
          }
        }
        else if (observable instanceof InspectionEventObservable)
        {
          InspectionEvent event = (InspectionEvent)object;
          InspectionEventEnum eventEnum = event.getInspectionEventEnum();
          if (eventEnum.equals(InspectionEventEnum.INSPECTION_STARTED))
          {
            // don't care
          }
          else if (eventEnum.equals(InspectionEventEnum.INSPECTION_COMPLETED))
          {
//            System.out.println("RM: InspectionEventEnum.INSPECTION_COMPLETED seen");
            // new inspection results cause the data to be refreshed with last run displayed by default
            _selectedInspectionRuns = null;
//            _inspectionRunList.clearSelection();
          }
        }
        else
          Assert.expect(false, "No observer found.");
      }
    });
  }

  /**
   * Invoked by the observer when selection changed in the AlgoTunerGui.
   * @author Rex Shang
   */
  private void setCurrentTestSelection(TunerTestState tunerTestState)
  {
    JointTypeChoice jointTypeChoice = tunerTestState.getJointTypeChoice();
    SubtypeChoice subtypeChoice = tunerTestState.getSubtypeChoice();

    // Note, These are coming from combo boxes and could be null.
    if (jointTypeChoice == null || jointTypeChoice.isAllFamilies())
      _jointType = null;
    else
      _jointType = jointTypeChoice.getJointTypeEnum();

    if (subtypeChoice == null || subtypeChoice.isAllSubtypes())
      _subtype = null;
    else
      _subtype = subtypeChoice.getSubtype();

    if (active())
    {
      populateSliceNameComboBox();
      
      if (_jointType == null || _subtype == null)
        enableReportButtons(false);
    }
  }

  /**
   * Handle the Generate Report button action
   * @author Rex Shang
   */
  private void generateReport_actionPerformed()
  {
    debug("generateReport_actionPerformed()");
    String reportFileName;
    if (_measurementType == MeasurementEnum.ALL_MEASUREMENT ||
        _sliceName == SliceNameEnum.ALL_SLICE) // Ying-Huan.Chu
    {
      reportFileName = selectFolder();
    }
    else
    {
      reportFileName = selectFile();
    }

    if (reportFileName.length() > 0)
    {
      _inputDirectory = FileUtil.getParent(reportFileName);

      generateReport(reportFileName);
    }
  }

  /**
   * This method displays a file chooser and returns the file selected by the user.  Returns a
   * zero-length string if there is no selection.
   * @author George Booth
   */
  private String selectFile()
  {
    _fileChooser = new JFileChooser(_inputDirectory);

    // initialize the JFIle Dialog.
    FileFilter fileFilter = _fileChooser.getAcceptAllFileFilter();
    _fileChooser.removeChoosableFileFilter(fileFilter);

    // create and instance of Text File Filter
    String commaSeparatedValueFileExtension = FileName.getCommaSeparatedValueFileExtension();
    FileFilterUtil reportFileFilter = new FileFilterUtil(commaSeparatedValueFileExtension, _reportFileFilterDescription);

    _fileChooser.addChoosableFileFilter(reportFileFilter);
    _fileChooser.setFileFilter(reportFileFilter); // default to new report file

    if (_fileChooser.showDialog(this, StringLocalizer.keyToString("GUI_SELECT_FILE_KEY")) !=
        JFileChooser.APPROVE_OPTION)
      return "";

    File selectedFile = _fileChooser.getSelectedFile();
    String selectedFileName = selectedFile.getPath();
    if (!selectedFileName.endsWith(commaSeparatedValueFileExtension))
    {
      selectedFileName = selectedFileName + commaSeparatedValueFileExtension;
    }

    return selectedFileName;
  }
  
  /**
   * This method displays a folder chooser and returns the folder selected by the user.  Returns a
   * zero-length string if there is no selection.
   * @author Ying-Huan.Chu
   */
  private String selectFolder()
  {
    _fileChooser = new JFileChooser(_inputDirectory);

    _fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

    if (_fileChooser.showDialog(this, StringLocalizer.keyToString("GUI_SELECT_FOLDER_KEY")) !=
        JFileChooser.APPROVE_OPTION)
      return "";

    File selectedFile = _fileChooser.getSelectedFile();
    String selectedFileName = selectedFile.getPath();
    
    return selectedFileName;
  }

  /**
   * Write defect report to selected file
   * @author George Booth
   */
  private void generateReport(String reportFileName)
  {
    debug(_me + "generateReport(" + reportFileName + ")");

    String reportTitle = StringLocalizer.keyToString("RMGUI_MEASUREMENT_TYPE_LABEL_KEY")
                         + " " + _measurementType
                         + StringLocalizer.keyToString("TABLE_MODEL_CSV_REPORT_DELIMITER_KEY")
                         + StringLocalizer.keyToString("RMGUI_SLICE_NAME_LABEL_KEY")
                         + " " + _sliceName;

    // write the table data as it is currently sorted
    TableModelReportWriter reportWriter = new TableModelReportWriter();
    try
    {
      // Added by Lee Herng 16 Dec 2013 - To handle all JointTypeEnum and All MeasurementEnum
      if (_sliceName == SliceNameEnum.ALL_SLICE)
      {
        // Ying-Huan.Chu
        generateAllMeasurementsForAllSlices(reportWriter, reportTitle, reportFileName);               
      }
      else
      {
        if (_measurementType == MeasurementEnum.ALL_MEASUREMENT)
        {
          // Ying-Huan.Chu
          generateAllMeasurementsForSingleSlice(_sliceName, reportWriter, reportTitle, reportFileName);
        }
        else
        {
          reportWriter.writeCSVFile(reportFileName, reportTitle, _reviewMeasurementsTableModel);
        }
      }
    }
    catch(IOException ioe)
    {
      MessageDialog.reportIOError(this, ioe.getLocalizedMessage());
      return;
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void generateAllMeasurementsForAllSlices(TableModelReportWriter reportWriter, 
                                                   String reportTitle, 
                                                   String reportFileName) throws IOException
  {    
    Assert.expect(reportWriter != null);
    Assert.expect(reportTitle != null);
    Assert.expect(reportFileName != null);
    
    ReviewMeasurementsTableModel measurementsTableModel = new ReviewMeasurementsTableModel(true);
    for(int j = 0; j < _sliceNameComboBox.getItemCount(); ++j)
    {
      SliceNameEnum sliceNameEnum = (SliceNameEnum)_sliceNameComboBox.getItemAt(j);
      if (sliceNameEnum.equals(SliceNameEnum.ALL_SLICE))
        continue;
      
      List<MeasurementEnum> measurementEnums = new ArrayList<>(_jointResultsManager.getMeasurementTypes(_projectName, 
                                                                                                        _selectedInspectionRuns, 
                                                                                                        _subtype, 
                                                                                                        sliceNameEnum));
      for (MeasurementEnum nonVisible : MeasurementEnum.getNonCustomerVisibleMeasurements())
      {
        measurementEnums.remove(nonVisible);
      }
      
      for (int i = 0; i < measurementEnums.size(); ++i)
      {
        measurementsTableModel.populateWithSingleSliceAllMeasurementTestedJoints(_projectName, _selectedInspectionRuns, _subtype, measurementEnums.get(i), sliceNameEnum);
      }
    }    
    
    // Start output result
    String filename = FileName.getReviewMeasurementFileNameFullPath(reportFileName, _projectName, _subtype.getShortName(), _jointType.getName());    
    reportWriter.writeCSVFile(filename, reportTitle, measurementsTableModel);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void generateAllMeasurementsForSingleSlice(SliceNameEnum sliceNameEnum,
                                                     TableModelReportWriter reportWriter, 
                                                     String reportTitle, 
                                                     String reportFileName) throws IOException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(reportWriter != null);
    Assert.expect(reportTitle != null);
    Assert.expect(reportFileName != null);
    
    ReviewMeasurementsTableModel measurementsTableModel = new ReviewMeasurementsTableModel(true);
    for (int i = 0; i < _measurementEnumList.size(); ++i)
    {
      measurementsTableModel.populateWithSingleSliceAllMeasurementTestedJoints(_projectName, _selectedInspectionRuns, _subtype, _measurementEnumList.get(i), sliceNameEnum);
    }    
    
    // Start output result
    String filename = FileName.getReviewMeasurementFileNameFullPath(reportFileName, _projectName, _subtype.getShortName(), _jointType.getName()); 
    reportWriter.writeCSVFile(filename, reportTitle, measurementsTableModel);
  }

  /**
   * Used for testing only.
   * @author Rex Shang
   */
  public static void main(String[] args)
  {
    try
    {
      Config.getInstance().loadIfNecessary();
    }
    catch (Exception e)
    {
      e.printStackTrace();
      Assert.expect(false);
    }

    Subtype mysubType = new Subtype();
    JPanel panel = new ReviewMeasurementsPanel(
        "FAMILIES_ALL_RLV",
        mysubType,
        MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL,
        SliceNameEnum.PAD);

    JFrame f = new JFrame("Table of results");
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.getContentPane().add(panel);
    f.setSize(400, 300);
    f.setLocation(300, 300);
    f.setVisible(true);
  }

  /**
   * Is Review Measurement active?
   * @author Rex Shang
   */
  boolean active()
  {
    return _currentPanelIsActive;
  }

  /**
   * Review Measurement is starting
   * @author Rex Shang
   */
  void start()
  {
//    System.out.println("\nReviewMeasurementsPanel.start()");

    // get menu and toolbar references and requester ID
    _menuBar = _mainUI.getMainMenuBar();
    _menuRequesterID = _menuBar.startMenuAdd();

    _envToolBar = _mainUI.getEnvToolBar();
    _envToolBarRequesterID = _envToolBar.startToolBarAdd();

    // add menu items
    JMenu editMenu = _menuBar.getEditMenu();
    _menuBar.addMenuItem(_menuRequesterID, editMenu, _deleteSelectedInspectionRunsMenuItem);

    JMenu toolsMenu = _menuBar.getToolsMenu();
    _menuBar.addMenuItem(_menuRequesterID, toolsMenu, _generateReportMenuItem);

    //Ngie Xing
    _menuBar.addMenu(_menuRequesterID, 5, _runMenu);
    _menuBar.addMenuItem(_menuRequesterID, _runMenu, _startTestMenuItem);

    // add toolbar buttons
    _envToolBar.removeDefectPackagerButtons();
    _envToolBar.addToolBarComponent(_envToolBarRequesterID, _generateReportButton);
    _envToolBar.addDefectPackagerButtons();

    getPersistanceSettings();

    _projectName = Project.getCurrentlyLoadedProject().getName();

    if (_reviewMeasurementsResizeDivider != -1)
    {
      _inspectionRunListSplitPane.setDividerLocation(_reviewMeasurementsResizeDivider);
    }

    // See if a measurement selection is made from Review Defects panel while
    // we are not active.
    if (_measurementSelection != null)
    {
      String subtypeNameInReviewDefectsPanel = _measurementSelection.getSubtypeName();
      // Check to see if the measurement selection data matches current subtype.
      //Siew Yeng - XCR-2535 - Inconsistent display of Component Voiding Area at Measurement Type for mix Subtype
      if ((_subtype != null && _subtype.getShortName().equals(subtypeNameInReviewDefectsPanel)) ||
          ((_subtype != null && subtypeNameInReviewDefectsPanel.equals(JointResultsEntry._MIXED)) && 
              (_jointType != null && _subtype.getJointTypeEnum() == _jointType)))
      {
        _selectedInspectionRuns = _measurementSelection.getInspectionRuns();
        _measurementType = _measurementSelection.getMeasurementType();
        _sliceName = _measurementSelection.getSliceName();
        // clear current selection
        _selectedJointResultsEntry = null;
      }
    }

    // No need to explicitly call populateMeasruementTypeComboBox, populateSliceNameComboBox
    // and populateTableModel.  It is done through listeners.
    _inspectionRunList.populate(_projectName, _selectedInspectionRuns);

    // set up joint image viewer
    //Chin Seong, Kee - Memory Leak Gingko Project
    if(_jointImageViewer == null)
      _jointImageViewer = new JointImageViewer(_inspectionRunList.getPanelResultsList());
    else
      _jointImageViewer.setPanelResultsList(_inspectionRunList.getPanelResultsList());

    _testDevChartPanel = _testDev.getChartPanel();
    Assert.expect(_testDevChartPanel != null);
    _testDevChartPanel.setLayout(new BorderLayout());
    _testDevChartPanel.add(_reviewMeasurementsChartPanel, BorderLayout.CENTER);

    _testDev.showChartWindow();

    // ok to tell graphics to highlight selected items
    _shouldInteractWithGraphics = true;

    _currentPanelIsActive = true;

    // reselect last result or select row 0 after everything else is setup
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        int row = -1;
        if (_lastSelectedJointResultsEntry != null)
        {
          row = _reviewMeasurementsTableModel.getRowForJointResultsEntry(_lastSelectedJointResultsEntry);
        }
        if (row > -1 && _reviewMeasurementsTable.getRowCount() > row)
        {
          _reviewMeasurementsTable.setRowSelectionInterval(row, row);
        }
        else
        {
          _reviewMeasurementsTable.clearSelection();
        }
      }
    });
  }

  /**
   * Since we don't allow any edits, we should be able to let go anytime.
   * @author Rex Shang
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * Called from AlgoTunerGui to give this class the last chance to clean up.
   * @author Rex Shang
   */
  void finish()
  {
//    System.out.println("ReviewMeasurementsPanel.finish()");

    updatePersistanceSettings();

    // save the last selection
    _lastSelectedJointResultsEntry = _selectedJointResultsEntry;

    // Nullify the critical variables so table can not be populated while we
    // are not in-focus.
    _measurementType = null;
    _sliceName = null;
//    _selectedInspectionRuns.clear();
    _measurementSelection = null;
    _measurementSlicePair = null;

    _menuBar.undoMenuChanges(_menuRequesterID);
    _envToolBar.undoToolBarChanges(_envToolBarRequesterID);

    _currentPanelIsActive = false;
  }

  /**
   * Invoked when slice typs is changed.  This is where we would update
   * the table data.
   * @author Rex Shang
   */
  private void sliceNameComboBoxActionPerformed(ActionEvent e)
  {
    _sliceName = (SliceNameEnum)_sliceNameComboBox.getSelectedItem();

    if (_sliceName == null)
      debug("_sliceNameComboBox_actionPerformed: null slice name selected");
    else
      debug("_sliceNameComboBox_actionPerformed: " + _sliceName.toString());

    populateMeasurementTypeComboBox();
  }

  /**
   * Invoked when measurement type is changed.  This is where we would update
   * the table data.
   * @author Rex Shang
   */
  private void measurementTypeComboBoxActionPerformed(ActionEvent e)
  {
    _measurementType = (MeasurementEnum)_measurementTypeComboBox.getSelectedItem();

    if (_measurementType == null)
      debug("_measurementTypeComboBox_actionPerformed: null measurement type selected");
    else
      debug("_measurementTypeComboBox_actionPerformed: " + _measurementType.toString());

    populateTableModel();
  }

  /**
   * Invoked when table model is changed.  This is where we update chart and
   * other stuff related to the table data.
   * @author Rex Shang
   * @author Seng-Yew Lim
   */
  private void reviewMeasurementsTableModelChanged(TableModelEvent e)
  {
    // User might select a specific subtype and have valid _sliceName & _measurementType
    // but later might change subtype to ALL, which will invalidates _sliceName & _measurementType
    // When subtype is changed to ALL, _sliceName will be null in populateSliceNameComboBox(...),
    // so use _sliceName as an indicator to invalidate the review measurements display.
    if (_sliceName == null)
    {
      _measurementType = null;
      _measurementTypeComboBox.removeAllItems();
      if (_reviewMeasurementsTableModel.getRowCount() > 0)
        _reviewMeasurementsTableModel.clearTableModel();
      _reviewMeasurementsChartPanel.clearDataSet();
      enableReportButtons(false);
      return;
    }
    ReviewMeasurementsTableModel tableModel = (ReviewMeasurementsTableModel)e.getSource();
    if (tableModel.getRowCount() > 0)
    {
      _reviewMeasurementsChartPanel.populateDataSet(tableModel.getJointEntries(), _measurementType, _sliceName);
      _reviewMeasurementsTable.setRowSelectionInterval(0, 0);
      enableReportButtons(true);
    }
    else
    {
      _reviewMeasurementsChartPanel.clearDataSet();
      _reviewMeasurementsTable.clearSelection();
      
      if (_measurementType == null ||
          (_measurementType != null && _measurementType.equals(MeasurementEnum.ALL_MEASUREMENT) == false))
      {
        enableReportButtons(false);
      }
    }
  }

  /**
   * Invoked when table is changed.  This is where we update the image pane.
   * @author George Booth
   */
  public void reviewMeasurementsTable_valueChanged(ListSelectionEvent e)
  {
    if (_currentPanelIsActive == false)
      return;

    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    if (lsm.getValueIsAdjusting())
    {
      // do nothing
    }
    else if (lsm.isSelectionEmpty())
    {
//      System.out.println("selection is empty");
      // clear graphics
//      _focusControlMenuItem.setEnabled(false);
      _guiObservable.stateChanged(null, SelectionEventEnum.INSPECTION_IMAGE_CLEAR_SELECTION);
      _selectedJointResultsEntry = null;
    }
    else
    {
      // turn off event processing until graphics had a chance to update
      _reviewMeasurementsTable.processSelections(false);
      int row = lsm.getAnchorSelectionIndex();
//      System.out.println("selection is row " + row);
      // show joint details
      JointResultsEntry jointResultsEntry =
          _reviewMeasurementsTableModel.getJointResultsEntryAt(row);
      _selectedJointResultsEntry = jointResultsEntry;
//      System.out.println("selected joint = " + _selectedJointResultsEntry.getReferenceDesignator() + " " + _selectedJointResultsEntry.getPadName());

      if (_shouldInteractWithGraphics)
      {
        long runId = _reviewMeasurementsTableModel.getRunIdAt(row);
        //  get component results for comp
        String boardName = jointResultsEntry.getBoardName();

        // show selected joint in image window
        //Lim, Lay Ngor - Fix bug: Show non-resize & non-enhance image & roi location for short
        //base on selected measurement type in Measurements Tab.
        boolean isSelectedMeasurementEnumTypeAbleToPerformResize = true;
        if(jointResultsEntry.hasMeasurementEnum())
        {
          isSelectedMeasurementEnumTypeAbleToPerformResize = 
            AlgorithmUtil.isMeasurementEnumAbleToPerformResize(jointResultsEntry.getMeasurementEnum());
        }
        _jointImageViewer.showImageAndPad(runId, jointResultsEntry, _sliceName, _jointType, isSelectedMeasurementEnumTypeAbleToPerformResize);

        // highlight selected component in CAD view
        Board board = Project.getCurrentlyLoadedProject().getPanel().getBoard(boardName);
        java.util.List<Board> selectedBoards = new ArrayList<Board>();
        selectedBoards.add(board);
        _guiObservable.stateChanged(selectedBoards, SelectionEventEnum.BOARD_INSTANCE);
        // get selected component
        java.util.List<ComponentType> selectedComponents = new ArrayList<ComponentType>();
        ComponentType compType = _reviewMeasurementsTableModel.getComponentTypeAt(row);
        selectedComponents.add(compType);
        _guiObservable.stateChanged(selectedComponents, SelectionEventEnum.COMPONENT_TYPE);

        // wait for graphics
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            _reviewMeasurementsTable.processSelections(true);
          }
        });
      }
    }
  }
  
  /**
   * @param e 
   * @author Kee Chin Seong - Added No Load , Set to No Test component pop up menu
   *                          ease for programming time.
   */
  private void handleJointInformationTablePopupMenu(MouseEvent e)
  {
    final int selectedRowIndex = _reviewMeasurementsTable.rowAtPoint(e.getPoint());
    if (selectedRowIndex == -1)
    {
      // user dragged cursor out of the table
      return;
    }

    // show the popup menu
    if (selectedRowIndex >= 0)
    {
      JPopupMenu popupMenu = new JPopupMenu();
      JMenu setSubtypeMenu = new JMenu(StringLocalizer.keyToString("RDGUI_SELECT_SUBTYPE_MAIN_MENU_KEY"));            
      JMenuItem changeSubtypeMenuItem = new JMenuItem(StringLocalizer.keyToString("RDGUI_CHANGE_SUBTYPE_MENU_KEY"));
      JMenuItem addComponentJointMenuItem = new JMenuItem(StringLocalizer.keyToString("RDGUI_ADD_COMPONENT_DEFECT_JOINT_MENU_KEY"));
      
      // Wei Chin : XCR 1665 
      JMenu testType = new JMenu(StringLocalizer.keyToString("RDGUI_SELECT_TEST_TYPE_MAIN_MENU_KEY"));
      JMenuItem setToNoLoadMenuItem = new JMenuItem(StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOLOAD_KEY"));
      JMenuItem setToNoTestMenuItem = new JMenuItem(StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOTEST_KEY"));
      JMenuItem setRestoreSubtypeMenuItem = new JMenuItem(StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_RESTORE_KEY"));      
      
      //Kee Chin Seong - To ease Programmer, Focusing also is needed here to save time to find the Modify subtype component
      //                 and set focusing method
      JMenu focusType = new JMenu(StringLocalizer.keyToString("RDGUI_SELECT_FOCUS_TYPE_MAIN_MENU_KEY"));
      JMenuItem autoFocusMenuItem = new JMenuItem(StringLocalizer.keyToString("MMGUI_GLOBAL_SURFACE_MODEL_USE_AF_KEY"));
      JMenuItem gsmMenuItem = new JMenuItem(StringLocalizer.keyToString("MMGUI_GLOBAL_SURFACE_MODEL_USE_GSM_KEY"));
      JMenuItem rfpMenuItem = new JMenuItem(StringLocalizer.keyToString("MMGUI_GLOBAL_SURFACE_MODEL_USE_RFB_KEY"));
      
      addComponentJointMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          addComponentJointMenuItemActionPerformed(e);
        }
      });
      
      autoFocusMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setComponentFocusingMethodTypeProcess(GlobalSurfaceModelEnum.AUTOFOCUS);
        }
      });
      gsmMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setComponentFocusingMethodTypeProcess(GlobalSurfaceModelEnum.GLOBAL_SURFACE_MODEL);
        }
      });
      rfpMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setComponentFocusingMethodTypeProcess(GlobalSurfaceModelEnum.USE_RFB);
        }
      });
      
      setToNoLoadMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setLoadStatusActionPerformed(false);
        }
      });
      
      setToNoTestMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setTestStatusActionPerformed(false);
        }
      });
      
      setRestoreSubtypeMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setTestStatusActionPerformed(true);
          setLoadStatusActionPerformed(true);
        }
      });     
      
      changeSubtypeMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          changeSubtypeMenuItemActionPerformed(e);
        }
      });
      
      popupMenu.add(addComponentJointMenuItem);
      popupMenu.addSeparator();
      setSubtypeMenu.add(changeSubtypeMenuItem);      
      popupMenu.add(setSubtypeMenu);
      
      //Kee Chin Seong - for Test Type Menu
      testType.add(setToNoLoadMenuItem);
      testType.add(setToNoTestMenuItem);
      popupMenu.addSeparator();
      popupMenu.add(testType);
      
      //Kee Chin Seong - For Focus Method Menu
      focusType.add(autoFocusMenuItem);
      focusType.add(gsmMenuItem);
      focusType.add(rfpMenuItem);
      popupMenu.addSeparator();
      popupMenu.add(focusType);
      
      List<PadType> padTypes = getSelectedPadTypes();
      boolean hasNoTestPad = false;
      boolean hasNoLoadComponent = false;
      for(PadType padtype : padTypes)
      {
        if(padtype.getComponentType().isLoaded() == false)
        {
          hasNoLoadComponent = true;
          break;
        }
        if(padtype.isInspected() == false)
        {
          hasNoTestPad = true;
          break;
        }
      }
      if(hasNoLoadComponent)
      {
        setToNoLoadMenuItem.setEnabled(false);
        setToNoTestMenuItem.setEnabled(false);
        testType.add(setRestoreSubtypeMenuItem);
      }
      else if(hasNoTestPad)
      {
        setToNoTestMenuItem.setEnabled(false);
        testType.add(setRestoreSubtypeMenuItem);
      }
      padTypes.clear();
      padTypes = null;

      popupMenu.show(e.getComponent(), e.getX(), e.getY());    
    }
  }
 
  /**
   * @return 
   * @author Wei Chin
   */
  private List<PadType> getSelectedPadTypes()
  {
    List<PadType> padTypes = new ArrayList();
    // only one selection is allowed
    // find the jointType / subtype so we can switch to them
    
    Project currentProject = Project.getCurrentlyLoadedProject();
      
    String boardName = "";
    String jointTypeName = _subtype.getJointTypeEnum().getName();
    if(_reviewMeasurementsTable.getSelectedRowCount() == 1)
    {
      int selectedRowIndex = _reviewMeasurementsTable.getSelectedRow();
      boardName = (String)_reviewMeasurementsTableModel.getValueAt(selectedRowIndex, ReviewMeasurementsTableModel.BOARD_NAME);
      String refDes = (String)_reviewMeasurementsTableModel.getValueAt(selectedRowIndex, ReviewMeasurementsTableModel.COMPONENT_NAME);
      String padName = (String)_reviewMeasurementsTableModel.getValueAt(selectedRowIndex, ReviewMeasurementsTableModel.PIN_NAME);
      JointTypeEnum jointTypeEnum = JointTypeEnum.getJointTypeEnum(jointTypeName);
      if(padName.equals( StringLocalizer.keyToString("RMGUI_COMPONENT_LEVEL_KEY")) || 
         ImageAnalysis.isSpecialTwoPinComponent(jointTypeEnum))
      {
        padTypes = currentProject.getPanel().getBoard(boardName).getComponentType(refDes).getPadTypes();
      }
      else
      {
        PadType padType = currentProject.getPanel().getBoard(boardName).getPadType(refDes, padName);
        padTypes.add(padType);
      }
    }
    else
    {
      for(int index : _reviewMeasurementsTable.getSelectedRows())
      {
        boardName = (String)_reviewMeasurementsTableModel.getValueAt(index, ReviewMeasurementsTableModel.BOARD_NAME);
        String refDes = (String)_reviewMeasurementsTableModel.getValueAt(index, ReviewMeasurementsTableModel.COMPONENT_NAME);
        String padName = (String)_reviewMeasurementsTableModel.getValueAt(index, ReviewMeasurementsTableModel.PIN_NAME);
        JointTypeEnum jointTypeEnum = JointTypeEnum.getJointTypeEnum(jointTypeName);
        if(padName.equals( StringLocalizer.keyToString("RMGUI_COMPONENT_LEVEL_KEY")) || 
          ImageAnalysis.isSpecialTwoPinComponent(jointTypeEnum))
        {
          padTypes.addAll(currentProject.getPanel().getBoard(boardName).getComponentType(refDes).getPadTypes());
        }
        else
        {
          PadType padType = currentProject.getPanel().getBoard(boardName).getPadType(refDes, padName);
          if(padTypes.contains(padType) == false)
            padTypes.add(padType);
        }
      }
    }
    return padTypes;
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  private List<ComponentType> getSelectedComponentTypes()
  {
    List<ComponentType> componentTypes = new ArrayList();
    // only one selection is allowed
    // find the jointType / subtype so we can switch to them
    
    Project currentProject = Project.getCurrentlyLoadedProject();
      
    String boardName = "";
    if(_reviewMeasurementsTable.getSelectedRowCount() == 1)
    {
      int selectedRowIndex = _reviewMeasurementsTable.getSelectedRow();
      boardName = (String)_reviewMeasurementsTableModel.getValueAt(selectedRowIndex, ReviewMeasurementsTableModel.BOARD_NAME);
      String refDes = (String)_reviewMeasurementsTableModel.getValueAt(selectedRowIndex, ReviewMeasurementsTableModel.COMPONENT_NAME);
      componentTypes.add(currentProject.getPanel().getBoard(boardName).getComponentType(refDes));
    }
    else
    {
      for(int index : _reviewMeasurementsTable.getSelectedRows())
      {
        boardName = (String)_reviewMeasurementsTableModel.getValueAt(index, ReviewMeasurementsTableModel.BOARD_NAME);
        String refDes = (String)_reviewMeasurementsTableModel.getValueAt(index, ReviewMeasurementsTableModel.COMPONENT_NAME);
        componentTypes.add(currentProject.getPanel().getBoard(boardName).getComponentType(refDes));
      }
    }
    return componentTypes;
  }
  
  /**
   * @author Wei Chin
   */
  private void setLoadStatusActionPerformed(final boolean loadStatus)
  {
     // set the pads  to no test
    final List<ComponentType> componentTypes = getSelectedComponentTypes();
    final int componentTypesSize = componentTypes.size();
    final ProgressDialog progressDialog = createChangeTestProgressDialog(componentTypesSize);
    
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
         int i = 1;
        _commandManager.beginCommandBlock(StringLocalizer.keyToString("MMGUI_NO_LOAD_GROUP_UNDO_KEY"));
          for(ComponentType componentType :componentTypes)
          {
            try
            {
              _commandManager.execute(new ComponentTypeSetLoadedCommand(componentType, loadStatus));
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                            ex.getLocalizedMessage(),
                                            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                            true);
            }
            finally
            {
              if(componentTypesSize > _NUM_JOINTS_TO_UPDATE_POP_UP_PROGRESS_BAR)
              {
                progressDialog.updateProgressBar(i++);
              }
            }
          }
          componentTypes.clear();
        _commandManager.endCommandBlock();
      }
    });
    
    if(componentTypesSize > _NUM_JOINTS_TO_UPDATE_POP_UP_PROGRESS_BAR)
    {
      progressDialog.setVisible(true);
    }
  }
  
  /**
   * @author Wei Chin
   */
  private void setTestStatusActionPerformed(final boolean testStatus)
  {
    // set the pads  to no test
    final List<PadType> padTypes = getSelectedPadTypes();
    final int padSize = padTypes.size();
    final ProgressDialog progressDialog = createChangeTestProgressDialog(padSize);
    
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        _commandManager.beginCommandBlock(StringLocalizer.keyToString("MMGUI_NO_TEST_GROUP_UNDO_KEY"));
        int i = 1;
        for(PadType padType :padTypes)
        {
          try
          {
            if(ImageAnalysis.isSpecialTwoPinComponent(padType.getJointTypeEnum()) == false)
            {
              _commandManager.execute(new PadTypeSetTestedCommand(padType, testStatus));
            }
            else
            {
              _commandManager.execute(new ComponentTypeSetTestedCommand(padType.getComponentType(), testStatus));
            }
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
          }
          finally
          {
            if(padSize> _NUM_JOINTS_TO_UPDATE_POP_UP_PROGRESS_BAR)
            {
              progressDialog.updateProgressBar(i++);
            }            
          }
        }
        padTypes.clear();
        _commandManager.endCommandBlock();
      }      
    });
    if(padSize > _NUM_JOINTS_TO_UPDATE_POP_UP_PROGRESS_BAR)
    {
      progressDialog.setVisible(true);
    }
//    padTypes.clear();
  }
  
  /*
   * @author Kee Chin Seong - Select AutoFocus / GSM / RFP Focusing Method
   */
  private void setComponentFocusingMethodTypeProcess(final GlobalSurfaceModelEnum focusMethod)
  {
    // set the pads  to no test
    final List<ComponentType> componentTypes = getSelectedComponentTypes();
    final int componentTypesSize = componentTypes.size();
    final ProgressDialog progressDialog = createChangeTestProgressDialog(componentTypesSize);

    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        int i = 1;
        try
        {
          _commandManager.beginCommandBlock("MMGUI_NO_TEST_GROUP_UNDO_KEY");
          for (ComponentType componentType : componentTypes)
          {
            try
            {
              _commandManager.execute(new ComponentTypeSetGlobalSurfaceModelCommand(componentType, focusMethod));
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                ex.getLocalizedMessage(),
                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                true);
            }
            finally
            {
              if (componentTypesSize > _NUM_JOINTS_TO_UPDATE_POP_UP_PROGRESS_BAR)
              {
                progressDialog.updateProgressBar(i++);
              }
            }
          }
          _commandManager.endCommandBlock();
        }
        finally
        {
          componentTypes.clear();
        }
      }
    });
    if (componentTypesSize > _NUM_JOINTS_TO_UPDATE_POP_UP_PROGRESS_BAR)
    {
      progressDialog.setVisible(true);
    }
  }
  
  /**
   * @author Wei Chin, Chong
   */
  private ProgressDialog createChangeTestProgressDialog( int numberOfPads)
  {
    ProgressDialog progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                         StringLocalizer.keyToString("RMGUI_UPDATE_TEST_STATUS_KEY"),
                                         StringLocalizer.keyToString("RMGUI_UPDATE_WAITING_KEY"),
                                         StringLocalizer.keyToString("RMGUI_UPDATE_COMPLETE_KEY"),
                                         StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                         StringLocalizer.keyToString("ATGUI_JOINT_KEY"),
                                         0,
                                         numberOfPads,
                                         true);
    progressDialog.pack();
    SwingUtils.centerOnComponent(progressDialog, this);
    return progressDialog;
  }
  
  /**
   * @author Rex Shang
   */
  private void debug(String message)
  {
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.REVIEW_MEASUREMENT_PANEL_DEBUG))
      System.out.println(_me + "." +  message);
  }

  /**
   * @author Cheah Lee Herng 
   */
  private void enableReportButtons(boolean enable)
  {
    if (enable)
    {
      _generateReportButton.setEnabled(true);
      _generateReportMenuItem.setEnabled(true);
    }
    else
    {
      _generateReportButton.setEnabled(false);
      _generateReportMenuItem.setEnabled(false);
    }
  }
  
  /**
   * @param e 
   * @author Wei Chin
   */
  private void changeSubtypeMenuItemActionPerformed(ActionEvent e)
  {
    if(_changeSubtypeDialog == null)
    {
      _changeSubtypeDialog = new ChangeSubtypeDialog(_mainUI, StringLocalizer.keyToString("RMGUI_CHANGE_SUBTYPE_DIALOG_KEY"));
      SwingUtils.centerOnComponent(_changeSubtypeDialog, _mainUI);
    }
    
    Project currentProject = Project.getCurrentlyLoadedProject();
    String boardName = "";
    String jointTypeName = "";
    String subtypeName = "";
    int selectedRowIndex = _reviewMeasurementsTable.getSelectedRow();
    boardName = (String)_reviewMeasurementsTableModel.getValueAt(selectedRowIndex, _reviewMeasurementsTableModel.BOARD_NAME);
    jointTypeName = _jointType.getName();//(String)_reviewMeasurementsTableModel.getValueAt(selectedRowIndex, _reviewMeasurementsTableModel.JOINT_TYPE);
    subtypeName = _subtype.getShortName();//(String)_jointInformationTableModel.getValueAt(selectedRowIndex, _jointInformationTableModel._SUBTYPE);
    List<PadType> padTypes = getSelectedPadTypes();

    JointTypeEnum jointTypeEnum = JointTypeEnum.getJointTypeEnum(jointTypeName);
    List<Subtype> subtypes = currentProject.getPanel().getBoard(boardName).getSubtypes(jointTypeEnum);
    Subtype subtype = null;
    for (Subtype s : subtypes)
    {
      if (s.getShortName().equalsIgnoreCase(subtypeName))
      {
        subtype = s;
        break;
      }
    }
    if(subtype != null)
    {
      _changeSubtypeDialog.populateData(currentProject, padTypes, subtype);
      _changeSubtypeDialog.setVisible(true);
      if(_changeSubtypeDialog.changeIsComplete())
      {
        // do nothing
      }
    }
    else
    {
      MessageDialog.showErrorDialog(this, StringLocalizer.keyToString("RMGUI_SUBTYPE_NOT_EXIST_ERROR_KEY"), StringLocalizer.keyToString("RMGUI_CHANGE_SUBTYPE_DIALOG_KEY"), true);
    }
    padTypes.clear();
    padTypes = null;
  }
  
  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  private void addComponentJointMenuItemActionPerformed(ActionEvent e)
  {
    int selectedRowIndex = _reviewMeasurementsTable.getSelectedRow();
    if (selectedRowIndex < 0)
    {
      return;
    }

    String boardName = (String) _reviewMeasurementsTableModel.getValueAt(selectedRowIndex, _reviewMeasurementsTableModel.BOARD_NAME);
    String jointName = (String) _reviewMeasurementsTableModel.getValueAt(selectedRowIndex, _reviewMeasurementsTableModel.PIN_NAME);
    String componentName = (String) _reviewMeasurementsTableModel.getValueAt(selectedRowIndex, _reviewMeasurementsTableModel.COMPONENT_NAME);

    String boardNameAndComponentNameAndJointName = boardName + "," + componentName + "," + jointName;
    try
    {
      MeasurementJointHighlightSettingsReader reader = new MeasurementJointHighlightSettingsReader(_projectName);
      List<String> defectiveComponentList = reader.getDefectiveSetting();

      if (defectiveComponentList.contains(boardNameAndComponentNameAndJointName) == false)
      {
        defectiveComponentList.add(boardNameAndComponentNameAndJointName);
        MeasurementJointHighlightSettingsWriter writter = new MeasurementJointHighlightSettingsWriter(_projectName);

        writter.saveSettings(defectiveComponentList);
      }
    }
    catch (DatastoreException ex)
    {
      MessageDialog.showErrorDialog(_mainUI,
        ex.getLocalizedMessage(),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    }
    catch (BusinessException x)
    {
      MessageDialog.showErrorDialog(_mainUI,
        x.getLocalizedMessage(),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    }
    _reviewMeasurementsChartPanel.populateDataSet(_reviewMeasurementsTableModel.getJointEntries(), _measurementType, _sliceName);
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public void repopulate()
  {
    if (_measurementType == null || _sliceName == null)
    {
      getPersistanceSettings();
    }
    if (_measurementType != null && _sliceName != null)
      _reviewMeasurementsChartPanel.populateDataSet(_reviewMeasurementsTableModel.getJointEntries(), _measurementType, _sliceName);
  }
}
