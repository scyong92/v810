package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.logging.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * <p>Title: GenerateImageSetWizardDialog</p>
 *
 * <p>Description: This is the wizard that will be responsible for allow the user to generate image sets.</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: AXI</p>
 *
 * @author Erica Wheatcroft
 */
public class GenerateImageSetWizardDialog extends WizardDialog
{

  private final static String _CHOICE_PANEL = "choicePanel";
  private final static String _VARIATION_PANEL = "variationPanel";
  private final static String _MAIN_PANEL = "mainPanel";
  private final static String _PRECISION_SECONDARY_PANEL = "precisionSecondaryPanel";
  private final static String _SECONDARY_PANEL = "secondaryPanel";
  private final static String _SUMMARY_PANEL = "summaryPanel";
  private MainMenuGui _mainUI;
  private WizardPanel _currentVisiblePanel = null;
  private Project _project = null;
  private ImageManager _imageManager;
  private ImageSetData _imageSetData = null;
  private boolean _saveOnlyTestedDevices = false;
  private boolean _runtimeWarningsGenerated = false;
  private java.util.List<String> _eventMessages = new ArrayList<String>();
  // hack members.. will be removed before we ship
  private int _distanceBetweenSlicesInMils = -1;
  private int _numberOfSlices = -1;
  private SwingWorkerThread _busyDialogWorkerThread = SwingWorkerThread.getInstance();

  /**
   * @author Erica Wheatcroft
   */
  public GenerateImageSetWizardDialog(Frame parent,
    boolean modal,
    Project project)
  {
    super(parent,
      modal,
      StringLocalizer.keyToString("WIZARD_NEXT_KEY"),
      StringLocalizer.keyToString("WIZARD_CANCEL_KEY"),
      StringLocalizer.keyToString("WIZARD_FINISH_KEY"),
      StringLocalizer.keyToString("WIZARD_BACK_KEY"));

    // note - NOT adding asserts as super class checks for valid parameters.
    Assert.expect(project != null, "Project is null");
    _project = project;
    _imageManager = ImageManager.getInstance();
    _mainUI = MainMenuGui.getInstance();
    initWizard();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setSaveOnlyTestedDevices(boolean saveOnlyTestedDevices)
  {
    _saveOnlyTestedDevices = saveOnlyTestedDevices;
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean saveOnlyTestedDevices()
  {
    return _saveOnlyTestedDevices;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _project = null;
    _imageSetData = null;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void showAdditionalInfo()
  {
    JPanel dataPanel = new JPanel(new PairLayout());
    dataPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    dataPanel.add(new JLabel("Distance Between Slices in mils:"));
    final JTextField textField = new JTextField();
    textField.setColumns(20);
    dataPanel.add(textField);
    dataPanel.add(new JLabel("# of slices to create:"));
    final JTextField textField2 = new JTextField();
    textField2.setColumns(20);
    dataPanel.add(textField2);

    textField2.addFocusListener(new FocusListener()
    {

      public void focusGained(FocusEvent evt)
      {
      }

      public void focusLost(FocusEvent evt)
      {
        if (textField2.getText() != null && textField2.getText().equalsIgnoreCase("") == false
          && textField2.getText().equalsIgnoreCase("0"))
        {
          _numberOfSlices = Integer.parseInt(textField2.getText());
        }
      }
    });

    textField.addFocusListener(new FocusListener()
    {

      public void focusGained(FocusEvent evt)
      {
      }

      public void focusLost(FocusEvent evt)
      {
        if (textField.getText() != null && textField.getText().equalsIgnoreCase("") == false
          && textField.getText().equalsIgnoreCase("0"))
        {
          _distanceBetweenSlicesInMils = Integer.parseInt(textField.getText());
        }
      }
    });


    final EscapeDialog dialog = new EscapeDialog(this, "Please specify", true);
    dialog.setLayout(new BorderLayout());
    dialog.setResizable(false);
    dialog.add(dataPanel, BorderLayout.CENTER);
    dialog.pack();
    dialog.setSize(new Dimension(425, 130));
    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

    // this is a complete hack and will not ship.. this code is for autofocus folks.
    JPanel buttonPanel = new JPanel(new FlowLayout());
    JButton okButton = new JButton("Ok");
    JButton cancelButton = new JButton("Cancel");
    okButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        if (_numberOfSlices > 0 && _distanceBetweenSlicesInMils > 0)
        {
          TestProgram testProgram = _mainUI.getTestProgramWithReason(StringLocalizer.keyToString("MMGUI_IMAGE_SET_GEN_NEEDS_TEST_PROGRAM_KEY"),
            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
            _project);
          ProgramGeneration.getInstance().generateDebugSlices(testProgram, _numberOfSlices,
            (int) MathUtil.convertUnits(_distanceBetweenSlicesInMils, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS));
        }

        dialog.dispose();
      }
    });
    cancelButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        dialog.dispose();
      }
    });

    buttonPanel.add(okButton);
    buttonPanel.add(cancelButton);
    dialog.add(buttonPanel, BorderLayout.SOUTH);
    SwingUtils.centerOnComponent(dialog, this);
    dialog.setVisible(true);
    dialog.dispose();


  }

  /**
   * @author Erica Wheatcroft
   */
  private void initWizard()
  {
    // change the title
    changeTitle(StringLocalizer.keyToString("GISWK_TITLE_KEY"));
    _imageSetData = new ImageSetData();
    java.util.List<WizardPanel> panels = new ArrayList<WizardPanel>();

    // create the panels
    GenerateImageSetWizardChoiceOptionsPanel choicePanel = new GenerateImageSetWizardChoiceOptionsPanel(_project, _imageSetData, this);
    choicePanel.setWizardName(_CHOICE_PANEL);
    panels.add(choicePanel);
    choicePanel.populatePanelWithData();
    
    GenerateImageSetWizardVariationOptionPanel variationPanel = new GenerateImageSetWizardVariationOptionPanel(_project, _imageSetData, this);
    variationPanel.setWizardName(_VARIATION_PANEL);
    panels.add(variationPanel);

    GenerateImageSetWizardMainOptionsPanel mainPanel = new GenerateImageSetWizardMainOptionsPanel(_project, _imageSetData, this);
    mainPanel.setWizardName(_MAIN_PANEL); // not a user defined string no localization needed
    panels.add(mainPanel);
    // now lets populate the mainPanel
//    mainPanel.populatePanelWithData();

    GenerateImageSetWizardPrecisionSecondaryPanel precisionSecondaryPanel = new GenerateImageSetWizardPrecisionSecondaryPanel(_project, _imageSetData, this);
    precisionSecondaryPanel.setWizardName(_PRECISION_SECONDARY_PANEL); // not a user defined string no localization needed
    panels.add(precisionSecondaryPanel);
//    precisionSecondaryPanel.populatePanelWithData();

    GenerateImageSetWizardSecondaryOptionsPanel secondaryPanel = new GenerateImageSetWizardSecondaryOptionsPanel(_project, _imageSetData, this);
    secondaryPanel.setWizardName(_SECONDARY_PANEL);// not a user defined string no localization needed
    panels.add(secondaryPanel);

    GenerateImageSetWizardSummaryPanel summaryPanel = new GenerateImageSetWizardSummaryPanel(_project, _imageSetData, this);
    summaryPanel.setWizardName(_SUMMARY_PANEL);// not a user defined string no localization needed
    panels.add(summaryPanel);

    // set the panels
    setContentPanels(panels);
    setCurrentVisiblePanel(_CHOICE_PANEL);

    setPreferredSize(new Dimension(575, 400));

    InspectionEventObservable.getInstance().addObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   * @author khang-shian.sham
   */
  protected void handleCompleteProcessButtonEvent()
  {
    Assert.expect(_currentVisiblePanel != null, "current visible panel is not set");
    Assert.expect(_imageSetData != null, "image set data is not set");

    //delete project good images(by sham)
    if (_imageSetData.isSaveGoodImages())
    {
      Assert.expect(_project != null);
      if (FileUtil.existsDirectory(Directory.getGoodImagesDir() + File.separator + _project.getName()))
      {
        int result = JOptionPane.showConfirmDialog(this,
          StringUtil.format(StringLocalizer.keyToString("GISWK_GIMG_DEL_COMFIRMATION_KEY"), 50),
          StringLocalizer.keyToString("GISWK_TITLE_KEY"),
          JOptionPane.OK_CANCEL_OPTION);
        if (result == 0)
        {
          try
          {
            FileUtil.delete(Directory.getGoodImagesDir() + File.separator + _project.getName());
          }
          catch (CouldNotDeleteFileException ex)
          {
            //do nothing
          }
        }
        else
        {
          return;
        }

      }
    }

    // lets change the buttons state to all disabled so that the user does not think the buttons are active when they are not.
    WizardButtonState buttonState = new WizardButtonState(false, false, false, false);
    WizardPanelObservable.getInstance().updateButtonState(buttonState);

    final TestProgram testProgram = _mainUI.getTestProgramWithReason(StringLocalizer.keyToString("MMGUI_IMAGE_SET_GEN_NEEDS_TEST_PROGRAM_KEY"),
      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), _project);

    _imageSetData.setProjectName(_project.getName());
    _imageSetData.setTestProgramVersionNumber(_project.getTestProgramVersion());
    long timeInMils = System.currentTimeMillis();
    _imageSetData.setImageSetName(Directory.getDirName(timeInMils));
    _imageSetData.setDateInMils(timeInMils);
    _imageSetData.setGenerateMultiAngleImages(_project.isGenerateMultiAngleImage());

    int numberOfImagesSaved = _imageManager.getNumberOfInspectionImagesToSave(_imageSetData, testProgram);
    double numberOfMBNeeded = numberOfImagesSaved * 0.010;

    if (numberOfImagesSaved == 0)
    {
      // the user has created a test program that will create 0 images.
      String message = StringLocalizer.keyToString("GISWK_EMPTY_IMAGE_SET_KEY");
      // check to see if the reason the images set produces zero images is that the subtype selected
      // is also one for which predictive slice height is turned ON.
      String additionalMessage = null;
      if (_imageSetData.hasImageSetTypeEnum() && _imageSetData.getImageSetTypeEnum().equals(ImageSetTypeEnum.SUBTYPE))
      {
        Subtype subtype = _imageSetData.getSubtypeToAcquireImagesFor();
        if (subtype.usePredictiveSliceHeight())
        {
          additionalMessage = StringLocalizer.keyToString(new LocalizedString("GISWK_EMPTY_IMAGE_SET_FOR_PREDICTIVE_SLICE_HEIGHT_SUBTYPE_KEY",
            new Object[]
            {
              subtype.getShortName(),
              subtype.getJointTypeEnum().getName()
            }));
        }
      }
      if (additionalMessage != null)
      {
        message += additionalMessage;
      }
      JOptionPane.showMessageDialog(this,
        StringUtil.format(message, 50),
        StringLocalizer.keyToString("GISWK_TITLE_KEY"),
        JOptionPane.ERROR_MESSAGE);
      buttonState = new WizardButtonState(false, true, true, true);
      WizardPanelObservable.getInstance().updateButtonState(buttonState);

      return;
    }

    String absolutePathRoot = FileUtil.getAbsolutePathRoot(Directory.getXrayInspectionImagesDir(_imageSetData.getProjectName(),
      _imageSetData.getImageSetName()));
    double availableDiskSpaceInMB = FileUtil.getAvailableDiskSpaceInBytes(absolutePathRoot) / (1024 * 1024);
    availableDiskSpaceInMB = MathUtil.roundToPlaces(availableDiskSpaceInMB, 1);
    if (numberOfMBNeeded > availableDiskSpaceInMB)
    {
      // the user does not have enough space to create this image set.
      // notify the user
      String message = StringLocalizer.keyToString(
        new LocalizedString("GISK_NOT_ENOUGH_SPACE_KEY",
        new Object[]
        {
          availableDiskSpaceInMB, numberOfMBNeeded
        }));
      JOptionPane.showMessageDialog(this, message, StringLocalizer.keyToString("GISWK_TITLE_KEY"), JOptionPane.ERROR_MESSAGE);
    }
    else
    {
      // this is a hack in order to help the auto focus guys
      if (TestDev.isInDeveloperDebugMode()
        && (_imageSetData.getImageSetTypeEnum().equals(ImageSetTypeEnum.COMPONENT)
        || _imageSetData.getImageSetTypeEnum().equals(ImageSetTypeEnum.PIN)))
      {
        showAdditionalInfo();
      }
    }
    try
    {
      _mainUI.generateOfflineInspectionImages(this, _imageSetData);
    }
    catch (PanelInterferesWithAdjustmentAndConfirmationTaskException panelInTheWayException)
    {
      SwingUtils.invokeLater(new Runnable()
      {

        public void run()
        {
          _mainUI.closeAllDialogs();

          // its time for the system to run grayscale and there is a panel in the system. so lets prompt the user to
          // unload the board after unload we will reload the panel and then start the image collection.

          // prompt the user to unload
//               String message = "In order to generate images, the system needs to perform a grayscale adjustment. " +
//                                "To complete this adjustment, the panel needs to be unloaded. Would you like to continue " +
//                                "and unload the panel?";
          String message = StringLocalizer.keyToString("MMGUI_GRAYSCALE_NEEDS_PANEL_REMOVED_KEY");

          int response = JOptionPane.showConfirmDialog(_mainUI,
            StringUtil.format(message, 50),
            StringLocalizer.keyToString("GISWK_TITLE_KEY"),
            JOptionPane.YES_NO_OPTION,
            JOptionPane.ERROR_MESSAGE);

          if (response == JOptionPane.YES_OPTION)
          {
            try
            {
              // unload the panel
              _mainUI.unloadPanel();

              // now that the panel is unloaded, do a startup to ensure all calibrations/adjustments are run
              _mainUI.performStartupIfNeeded();

              // if a startup is still needed, don't try to load -- it's probably been canceled out
              if (XrayTester.getInstance().isStartupRequired() == false)
              {
                // reload the panel
                _mainUI.loadPanel();
              }

              // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
              // now check to verify that the user has not cancelled the load of the panel.
              if (PanelHandler.getInstance().isPanelLoaded())
              {
                // re-run the image collection request.
                handleCompleteProcessButtonEvent();
                return;
              }
              else
              {
                // return out do not complete operation.
                WizardButtonState buttonState = new WizardButtonState(false, true, true, true);
                WizardPanelObservable.getInstance().updateButtonState(buttonState);
                _currentVisiblePanel.validatePanel();
                return;
              }
            }
            catch (XrayTesterException ex)
            {
              _mainUI.handleXrayTesterException(ex);
            }
          }
          else
          {
            // the user does not want to unload. Therefore we will just exit the operation
            JOptionPane.showMessageDialog(_mainUI,
              StringLocalizer.keyToString("GISK_NOT_COMPLETED_KEY"),
              StringLocalizer.keyToString("GISWK_TITLE_KEY"),
              JOptionPane.INFORMATION_MESSAGE);
            WizardButtonState buttonState = new WizardButtonState(false, true, true, true);
            WizardPanelObservable.getInstance().updateButtonState(buttonState);
            _currentVisiblePanel.validatePanel();
            return;
          }
        }
      });
      return;
    }
    catch (final XrayTesterException ex)
    {
      SwingUtils.invokeLater(new Runnable()
      {

        public void run()
        {
          _mainUI.handleXrayTesterException(ex);
          WizardButtonState buttonState = new WizardButtonState(false, true, true, true);
          WizardPanelObservable.getInstance().updateButtonState(buttonState);
          _currentVisiblePanel.validatePanel();
        }
      });
      return;
    }
    if (TestExecution.getInstance().didUserAbort())
    {
      _mainUI.closeImageGenerationProgressDialog();
      buttonState = new WizardButtonState(false, true, true, true);
      WizardPanelObservable.getInstance().updateButtonState(buttonState);
      return;
    }
    // now clear the filters on the test program.
    testProgram.clearFilters();

    try
    {
      if (_project.enablePreSelectVariation() == false)
      {
        //load original variation
        loadVariationSetting(_project.getEnabledVariationName());
      }
      
      if (_project.hasBeenModified())
      {
        try
        {
          _project.fastSave();
        }
        catch (DatastoreException ex)
        {
          //nothing
        }
      }
    }
    finally
    {
      //set to true after original variation is loaded.
      _project.setIsUpdateComponentNoLoadSetting(true);
    }
    

    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  void displayTimeMetrics(TestExecutionTimer timer)
  {
    Assert.expect(timer != null);

    String messageToDisplay = MainMenuGui.getInstance().getTimingMetricsString(timer);
    JOptionPane.showMessageDialog(MainMenuGui.getInstance(), messageToDisplay, "Timing Metrics:", JOptionPane.PLAIN_MESSAGE);
  }

  /**
   * This method will implement the next action performed
   * @author Erica Wheatcroft
   */
  protected void handleNextScreenButtonEvent()
  {
    Assert.expect(_currentVisiblePanel != null, "current visible panel is not set");
    Assert.expect(_imageSetData != null, "image set data is not set");

    // lets change the buttons state to all disabled so that the user does not think the buttons are active when they are not.
    WizardButtonState buttonState = new WizardButtonState(false, false, false, false);
    WizardPanelObservable.getInstance().updateButtonState(buttonState);

    String nextPanelName = null;
    String currentPanelName = _currentVisiblePanel.getWizardName();
    if (currentPanelName.equals(_CHOICE_PANEL))
    {
      try
      {
        java.util.List<String> variations = VariationSettingManager.getInstance().getProjectNameToVariationNamesMap(_project.getName());
        if (variations.isEmpty() || _project.enablePreSelectVariation())
        {
          _imageSetData.setVariationName(VariationSettingManager.ALL_LOADED);
          
          // the user is on the image collection choice panel and depending on the choice, they either go on to
          // the main panel (if fine tuning selected) or to the intermediate panel (if precision tuning selected)
          if (_imageSetData.getImageSetCollectionTypeEnum().equals(ImageSetCollectionTypeEnum.FINE_TUNING))
          {
            // switch to main panel
            nextPanelName = _MAIN_PANEL;
          }
          else if (_imageSetData.getImageSetCollectionTypeEnum().equals(ImageSetCollectionTypeEnum.PRECISION))
          {
            // switch to intermediate panel for precision tuning choice branch
            nextPanelName = _PRECISION_SECONDARY_PANEL;
          }
          else
          {
            Assert.expect(false, "Image set collection type not set!");
          }
        }
        else
        {
          nextPanelName = _VARIATION_PANEL;
        }
      }
      catch (DatastoreException ex)
      {
        //nothing
      }
    }
    else if (currentPanelName.equals(_VARIATION_PANEL))
    {
      if (_project.enablePreSelectVariation() == false)
      {
        boolean prevCheckLatestTestProgram = _project.isCheckLatestTestProgram();

        //set to false, so that the selected variation in production will not override the enable variation in panel setup when save project.
        _project.setIsUpdateComponentNoLoadSetting(false);
        //setup variation
        loadVariationSetting(_imageSetData.getVariationName());

        //need to regenerate test program after change variation.
        _project.setCheckLatestTestProgram(true);
        _mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_IMAGE_SET_GEN_NEEDS_TEST_PROGRAM_KEY"),
          StringLocalizer.keyToString("IMTB_GENERATE_IMAGE_SET_KEY"),
          _project);
        _project.setCheckLatestTestProgram(prevCheckLatestTestProgram);
        
        //auto save if project has been modified. 
        //After changing varaition, test program need to generate, so the project need to save it.
        if (_project.hasBeenModified())
        {
          try
          {
            _project.fastSave();
          }
          catch (DatastoreException ex)
          {
            //nothing
          }
        }
      }
      
      // the user is on the image collection choice panel and depending on the choice, they either go on to
      // the main panel (if fine tuning selected) or to the intermediate panel (if precision tuning selected)
      if (_imageSetData.getImageSetCollectionTypeEnum().equals(ImageSetCollectionTypeEnum.FINE_TUNING))
      {
        // switch to main panel
        nextPanelName = _MAIN_PANEL;
      }
      else if (_imageSetData.getImageSetCollectionTypeEnum().equals(ImageSetCollectionTypeEnum.PRECISION))
      {
        // switch to intermediate panel for precision tuning choice branch
        nextPanelName = _PRECISION_SECONDARY_PANEL;
      }
      else
      {
        Assert.expect(false, "Image set collection type not set!");
      }
    }
    else if (currentPanelName.equals(_MAIN_PANEL))
    {
      // the user is currently on the main panel now if they have selected a PANEL image set then they will need to go
      // the summary screen
      if (_imageSetData.getImageSetTypeEnum().equals(ImageSetTypeEnum.PANEL))
      {
        // switch to the summay screen
        nextPanelName = _SUMMARY_PANEL;
      }
      else
      {
        // switch to the secondary panel
        nextPanelName = _SECONDARY_PANEL;
      }
    }
    // check to see if the user is on the seconday panel
    else if (currentPanelName.equals(_SECONDARY_PANEL) || currentPanelName.equals(_PRECISION_SECONDARY_PANEL))
    {
      // then switch to the summary panel
      nextPanelName = _SUMMARY_PANEL;
    }
    else
    {
      // temporary fix for user click the UI too fast (Wei Chin)
      return;
      // they better not be on the summary and press next!
//      Assert.expect(false, " on summary screen next button should not be enabled");
    }

    CardLayout layout = (CardLayout) _contentPanel.getLayout();
    layout.show(_contentPanel, nextPanelName);
    setCurrentVisiblePanel(nextPanelName);
    _currentVisiblePanel.populatePanelWithData();
    pack();
  }

  /**
   * this method will implement the previous action performed.
   * @author Erica Wheatcroft
   */
  protected void handlePreviousScreenButtonEvent()
  {
    Assert.expect(_currentVisiblePanel != null, "current visible panel is not set");
    Assert.expect(_imageSetData != null, "image set data is not set");

    // lets change the buttons state to all disabled so that the user does not think the buttons are active when they are not.
    WizardButtonState buttonState = new WizardButtonState(false, false, false, false);
    WizardPanelObservable.getInstance().updateButtonState(buttonState);

    String previousPanelName = null;
    String currentPanelName = _currentVisiblePanel.getWizardName();
    // check to see if the user is on the seconday panel
    if (currentPanelName.equals(_SECONDARY_PANEL))
    {
      // then switch to the main panel
      previousPanelName = _MAIN_PANEL;
    }
    // check to see if the user is on the summary panel
    else if (currentPanelName.equals(_SUMMARY_PANEL))
    {
      if (_imageSetData.getImageSetCollectionTypeEnum().equals(ImageSetCollectionTypeEnum.FINE_TUNING))
      {
        // so check to see if they are doing a panel test and if so then we need to switch back to the main screen
        if (_imageSetData.getImageSetTypeEnum() == ImageSetTypeEnum.PANEL)
        {
          // switch to the main screen
          previousPanelName = _MAIN_PANEL;
        }
        else
        {
          // switch to the secondary panel
          previousPanelName = _SECONDARY_PANEL;
        }
      }
      else if (_imageSetData.getImageSetCollectionTypeEnum().equals(ImageSetCollectionTypeEnum.PRECISION))
      {
        previousPanelName = _PRECISION_SECONDARY_PANEL;
      }
    }
    else if (currentPanelName.equals(_PRECISION_SECONDARY_PANEL) || currentPanelName.equals(_MAIN_PANEL))
    {
      try
      {
        java.util.List<String> variations = VariationSettingManager.getInstance().getProjectNameToVariationNamesMap(_project.getName());
        if (variations.isEmpty() || _project.enablePreSelectVariation())
        {
          previousPanelName = _CHOICE_PANEL;
        }
        else
        {
          previousPanelName = _VARIATION_PANEL;
        }
      }
      catch (DatastoreException ex)
      {
        //nothing
      }
    }
    else if (currentPanelName.equals(_VARIATION_PANEL))
    {
      previousPanelName = _CHOICE_PANEL;
    }
    else
    {
      // the user is on the main screen that is very bad!
      Assert.expect(false, " on first screen previous button should not be enabled");
    }

    CardLayout layout = (CardLayout) _contentPanel.getLayout();
    layout.show(_contentPanel, previousPanelName);
    setCurrentVisiblePanel(previousPanelName);
    _currentVisiblePanel.populatePanelWithData();
    pack();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void setCurrentVisiblePanel(String currentVisiblePanelName)
  {
    Assert.expect(currentVisiblePanelName != null, "current visible panel name is null");
    Assert.expect(_contentPanels != null, "content panels are null");

    _currentVisiblePanel = null;

    // iterate through the list and find the panel with the name sent in
    for (WizardPanel panel : _contentPanels)
    {
      if (panel.getWizardName().equals(currentVisiblePanelName))
      {
        _currentVisiblePanel = panel;
        break;
      }
    }

    Assert.expect(_currentVisiblePanel != null, "Current visible panel is null and was not found");

  }

  /**
   * @author Erica Wheatcroft
   */
  protected void confirmExitOfDialog()
  {
    // confirm with the user if they want to cancel the dialog

    int response = ChoiceInputDialog.showConfirmDialog(MainMenuGui.getInstance(),
      StringLocalizer.keyToString("GISWK_CONFIRM_EXIT_OF_GUI_KEY"),
      StringLocalizer.keyToString("GISWK_CONFIRM_EXIT_OF_GUI_DIALOG_TITLE_KEY"));


    if (response == JOptionPane.NO_OPTION || response == JOptionPane.CLOSED_OPTION)
    {
      return;
    }
    
    //load original variation setting
    try
    {
      if (_project.enablePreSelectVariation() == false)
      {
        //load original variation
        loadVariationSetting(_project.getEnabledVariationName());
      }
      
      if (_project.hasBeenModified())
      {
        try
        {
          _project.fastSave();
        }
        catch (DatastoreException ex)
        {
          //nothing
        }
      }
    }
    finally
    {
      //set to true after original variation is loaded.
      _project.setIsUpdateComponentNoLoadSetting(true);
    }

    // the user wants to cancel.
    dispose();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void dispose()
  {
    if (_runtimeWarningsGenerated)
    {
      displayWarnings();
    }
    _runtimeWarningsGenerated = false;

    InspectionEventObservable.getInstance().deleteObserver(this);
    unpopulate();
    super.dispose();
  }

  /**
   * this method will handle update calls from observables.
   *
   * @author Laura Cormos
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    // need to make sure the parent class' update method is called also, since this update overwrites it.
    super.update(observable, object);

    SwingUtils.invokeLater(new Runnable()
    {

      public void run()
      {
        if (observable instanceof InspectionEventObservable)
        {
          InspectionEvent inspectionEvent = (InspectionEvent) object;
          if (inspectionEvent instanceof IncompleteProgramAdjustmentInspectionEvent)
          {
            IncompleteProgramAdjustmentInspectionEvent event = (IncompleteProgramAdjustmentInspectionEvent) inspectionEvent;
            _runtimeWarningsGenerated = true;
            _eventMessages.add(event.getMessage());
          }
        }
      }
    });
  }

  /**
   * @author Laura Cormos
   */
  private void displayWarnings()
  {
    Assert.expect(_eventMessages != null);

    for (String message : _eventMessages)
    {
      TextDialog textDialog = new TextDialog(MainMenuGui.getInstance(),
        message,
        StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
        StringLocalizer.keyToString("GISWK_TITLE_KEY"),
        true);
      SwingUtils.centerOnComponent(textDialog, this);
      textDialog.setVisible(true);
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void loadVariationSetting(final String variationName)
  {
    Assert.expect(variationName != null);

    final BusyCancelDialog busyDialog = new BusyCancelDialog(_mainUI,
      StringLocalizer.keyToString("MMGUI_SETUP_VARIATION_SETTING_KEY"),
      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
      true);

    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, _mainUI);

    _busyDialogWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        Map<String, java.util.List<String>> boardTypeToRefDesMap = new LinkedHashMap<>();
        java.util.List<VariationSetting> noLoadSettingVariationList = _project.getPanel().getPanelSettings().getAllVariationSettings();
        boolean isSetupVariationSetting = false;
        
        _project.setSelectedVariationName(variationName);

        //if no variation name, then need to set all to load and inspect
        if (variationName.equals(VariationSettingManager.ALL_LOADED))
        {
          isSetupVariationSetting = false;
        }
        else
        {
          //get selected variation
          for (VariationSetting variation : noLoadSettingVariationList)
          {
            if (variationName.equals(variation.getName()))
            {
              isSetupVariationSetting = true;
              boardTypeToRefDesMap = variation.getBoardTypeToRefDesMap();
              break;
            }
          }
        }
        VariationSettingManager.getInstance().setupVariationSetting(isSetupVariationSetting, _project, boardTypeToRefDesMap);
        
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            busyDialog.setVisible(false);
          }
        });
      }
    });
    busyDialog.setVisible(true);
  }
}
