package com.axi.v810.gui.imagePane;

import java.awt.*;

import com.axi.guiUtil.*;
import com.axi.util.*;

/**
 * This class draws a Region of Interest
 *
 * @author George A. David
 */
public class RegionOfInterestRenderer extends ShapeRenderer
{
  private Shape _shape;
  private int _strokeSize;

  /**
   * @author Andy Mechtenberg
   */
  public RegionOfInterestRenderer(Shape shape)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(shape != null);

    _shape = shape;
    _strokeSize = 1;
    refreshData();
  }

  /**
   * @author Andy Mechtenberg
   */
  public RegionOfInterestRenderer(Shape shape, int strokeSize)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(shape != null);
    Assert.expect(strokeSize > 0);

    _shape = shape;
    _strokeSize = strokeSize;
    refreshData();
  }

  /**
   * This method does the actual drawing on the screen.
   * @author Bill Darbie
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;
    Stroke origStroke = graphics2D.getStroke();
    graphics2D.setStroke(new BasicStroke(_strokeSize));
    super.paintComponent(graphics);
    graphics2D.setStroke(origStroke);
  }

  /**
   * Return the name of the panel
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    return "region of interest";
  }

  /**
   * @author Bill Darbie
   */
  protected void refreshData()
  {
    Assert.expect(_shape != null);

    initializeShape(_shape);
  }
}
