package com.axi.v810.gui.imagePane;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.util.*;
import com.axi.v810.gui.mainMenu.MainMenuGui;

/**
 * <p>Title: AvailableAndSelectedImageSetsTableModel</p>
 *
 * <p>Description: This class is the table model for both the Available and Selected Image set Tables.</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: AXI</p>
 *
 * @author Erica Wheatcroft
 */
public class AvailableAndSelectedImageSetsTableModel extends DefaultSortTableModel
{
  private final String[] _tableColumns = {StringLocalizer.keyToString("SELECTED_COLUMN_KEY"),
                                          StringLocalizer.keyToString("SELECTED_IMAGE_SET_NAME_COLUMN_KEY"),
                                          StringLocalizer.keyToString("SELECTED_DATE_CREATED_COLUMN_KEY"),
                                          StringLocalizer.keyToString("IMD_TABLE_USER_DESCRIPTION_COLUMN_KEY"),
                                          StringLocalizer.keyToString("SELECTED_TEST_PROGRAM_VERSION_COLUMN_KEY"),
                                          StringLocalizer.keyToString("SELECTED_COMPATIBILITY_PERCENT_COLUMN_KEY")};

  private Map<ImageSetData, Boolean> _imageSetDataToSelectedMap = new HashMap<ImageSetData,Boolean>();
  private List<ImageSetData> _availableImageSets = new ArrayList<ImageSetData>();
  private Set<ImageSetData> _selectedImageSets = new HashSet<ImageSetData>();

  private final String _YES = StringLocalizer.keyToString("GUI_YES_BUTTON_KEY");
  private final String _NO = StringLocalizer.keyToString("GUI_NO_BUTTON_KEY");

  private ImageSetComparator _imageSetComparator = new ImageSetComparator();

  private ProjectPersistance _persistance = null;

  // if this is set to true then the normal updates will not be fired.
  // false if we only want to notify one observer
  private boolean _limitNotifications = false;

  /**
   * @author Erica Wheatcroft
   */
  public AvailableAndSelectedImageSetsTableModel(boolean limitNotifications)
  {
    _limitNotifications = limitNotifications;
    if(_limitNotifications == false)
      _persistance = MainMenuGui.getInstance().getTestDev().getPersistance().getProjectPersistance();
    _imageSetComparator.compareImageSetDates();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void populateWithImageSetData(List<ImageSetData> availableImageSets, List<ImageSetData> selectedImageSets)
  {
    Assert.expect(availableImageSets != null, "available image sets is null");
    Assert.expect(selectedImageSets != null, "selected image sets is null");
    _availableImageSets = availableImageSets;

    _imageSetComparator.compareImageSetDates();
    Collections.sort(_availableImageSets, _imageSetComparator);

    _selectedImageSets.clear();
    _selectedImageSets.addAll(selectedImageSets);

    _imageSetDataToSelectedMap.clear();
    for(ImageSetData imageSet : _availableImageSets)
      _imageSetDataToSelectedMap.put(imageSet, new Boolean(false));

    boolean oneImageSetSelected = false;
    for(ImageSetData selectedImageSet : _selectedImageSets)
    {
      _imageSetDataToSelectedMap.put(selectedImageSet, new Boolean(true));
      oneImageSetSelected = true;
    }

    if(oneImageSetSelected)
    {
      if(_limitNotifications == false)
      {
        List<ImageSetData> sortedImageSets = new ArrayList<ImageSetData>(_selectedImageSets);
        Collections.sort(sortedImageSets, _imageSetComparator);
        GuiObservable.getInstance().stateChanged(sortedImageSets, ImageSetEventEnum.IMAGE_SET_SELECTED);
      }
    }

    fireTableDataChanged();
  }

  /**
   * Returns the number of columns in the model.
   *
   * @return the number of columns in the model
   * @author Erica Wheatcroft
   */
  public int getColumnCount()
  {
    Assert.expect(_tableColumns != null);
    return _tableColumns.length;
  }

  /**
   * Returns the name of the column at <code>columnIndex</code>.
   *
   * @param columnIndex the index of the column
   * @return the name of the column
   * @author Erica Wheatcroft
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return _tableColumns[columnIndex];
  }

  /**
   * @return the number of rows in the model
   * @author Erica Wheatcroft
   */
  public int getRowCount()
  {
    if(_availableImageSets == null)
      return 0;

    return _availableImageSets.size();
  }

  /**
   * @param rowIndex the row whose value is to be queried
   * @param columnIndex the column whose value is to be queried
   * @return the value Object at the specified cell
   * @author Erica Wheatcroft
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_availableImageSets != null, "image set data is null");
    Assert.expect(_imageSetDataToSelectedMap != null, "image set data to selected map is null");
    Assert.expect(rowIndex >= 0 , "Row Index is negative!");
    Assert.expect(columnIndex >= 0, "Column Index is negative!");

    ImageSetData imageSetData = _availableImageSets.get(rowIndex);
    Object returnValue = null;
    switch(columnIndex)
    {
      case 0:
        Assert.expect(_imageSetDataToSelectedMap.containsKey(imageSetData), "selected image set does not exist in map");
        returnValue = _imageSetDataToSelectedMap.get(imageSetData);
        break;
      case 1:
        // get the image set system description
        returnValue = imageSetData.getSystemDescription();
        break;
      case 2:
        long dateInMils = imageSetData.getDateInMils();
        returnValue = StringUtil.convertMilliSecondsToTimeAndDate(dateInMils);
        break;
      case 3:
        // get the user description
        returnValue = imageSetData.getUserDescription();
        break;
      case 4:
        // get the test program version
        returnValue = imageSetData.getTestProgramVersionNumber();
        break;
      case 5:
        // display the compatible %
        double actualPercentage = imageSetData.getPercentOfCompatibleImages();
        if( ((int) actualPercentage == 99) || ((int) actualPercentage == 0))
        {
          int temp = (int) (actualPercentage * 100);
//          returnValue = (double)temp / 100 ;
          // we need to return the same type for this column all the time -- keep it a string then.
          // if we return a double for one row and a string for another, that causes Swing to get cranky
          returnValue = Double.toString((double)temp/100);
        }
        else
          returnValue = Integer.toString((int)MathUtil.roundToPlaces(actualPercentage, 0));
        break;
      default:
        Assert.expect(false, "only 5 columns exist!!");
    }

    Assert.expect(returnValue != null, "return value is null");
    return returnValue;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setValueAt(Object value, int rowIndex, int columnIndex)
  {
    Assert.expect(value != null, "value is null");
    Assert.expect(rowIndex >= 0 , "Row Index is negative!");
    Assert.expect(columnIndex >= 0, "Column Index is negative!");

    ImageSetData imageSetData = _availableImageSets.get(rowIndex);

    if(columnIndex == 0)
    {
      Assert.expect(value instanceof Boolean, "not a boolean");
      Assert.expect(_imageSetDataToSelectedMap.containsKey(imageSetData), "selected image set does not exist in map");
      Boolean selected = (Boolean)value;
      _imageSetDataToSelectedMap.put(imageSetData, selected);
      int currentNumberOfSelectedImageSets = _selectedImageSets.size();
      if (selected)
      {
        // lets check to see if there are any image sets already selected if not then we should notify anyone who care
        // that there are image sets ready for inspection.
        _selectedImageSets.add(imageSetData);
        notifyImageSetSelection();
        if (_limitNotifications == false)
          _persistance.addSelectedImageSetName(imageSetData.getImageSetName());
      }
      else
      {
        currentNumberOfSelectedImageSets--;
        _selectedImageSets.remove(imageSetData);
        notifyImageSetSelection();
        if (_limitNotifications == false)
          _persistance.removeSelectedImageSetName(imageSetData.getImageSetName());
      }
    }
    else if(columnIndex == 3)
    {
      String originalUserDescription = imageSetData.getUserDescription();
      String userDescription = (String)value;
      imageSetData.setUserDescription(userDescription);
      ImageSetInfoWriter writer = new ImageSetInfoWriter();
      try
      {
        // lets write out the new description
        writer.write(imageSetData);
      }
      catch(DatastoreException de)
      {
        imageSetData.setUserDescription(originalUserDescription);
        MainMenuGui.getInstance().handleXrayTesterException(de);
      }
    }
    else
      Assert.expect(false); // only columns 0 and 3 are editable.
  }

  /**
   * @author Andy Mechtenberg
   */
  private void notifyImageSetSelection()
  {
    List<ImageSetData> sortedImageSets = new ArrayList<ImageSetData>(_selectedImageSets);
    Collections.sort(sortedImageSets, _imageSetComparator);

    if (_limitNotifications == false)
    {
      if (_selectedImageSets.isEmpty())
      {
        _persistance.clearSelectedImageSets();
        GuiObservable.getInstance().stateChanged(sortedImageSets, ImageSetEventEnum.IMAGE_SET_NOT_SELECTED);
      }
      else
      {
        GuiObservable.getInstance().stateChanged(sortedImageSets, ImageSetEventEnum.IMAGE_SET_SELECTED);
      }
    }
    else
    {
      if (_selectedImageSets.isEmpty())
      {
        if (_limitNotifications == false)
          _persistance.clearSelectedImageSets();
        GuiObservable.getInstance().stateChanged(sortedImageSets, ImageSetEventEnum.IMAGE_SET_NOT_SELECTED_FOR_ARCHIVE);
      }
      else
      {
        GuiObservable.getInstance().stateChanged(sortedImageSets, ImageSetEventEnum.IMAGE_SET_SELECTED_FOR_ARCHIVE);
      }
    }
  }

  /**
   * Nothing is editable for the two tables so this will always return false.
   *
   * @param rowIndex the row whose value to be queried
   * @param columnIndex the column whose value to be queried
   * @return true if the cell is editable
   * @author Erica Wheatcroft
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(columnIndex >= 0, "column index is negative");

    if(columnIndex == 0 || columnIndex == 3) // the user can only edit column 1 & 4 everything else is not editable.
      return true;

    return false;
  }

  /**
   * @author Erica Wheatcroft
   */
  public List<ImageSetData> getImageSets()
  {
    Assert.expect(_availableImageSets != null, "Image Set Data is null");
    return _availableImageSets;
  }

  /**
   * @author Erica Wheatcroft
   */
  public ImageSetData getSelectedImageSet(int rowIndex)
  {
    Assert.expect(rowIndex >= 0, "row index is negative");
    Assert.expect(_availableImageSets != null, "Image set data is null");
    Assert.expect(rowIndex <= _availableImageSets.size(), "row index is greater than the number of elements");
    return _availableImageSets.get(rowIndex);
  }

  /**
   * @author Erica Wheatcroft
   */
  public void removeRow(int rowIndex)
  {
    Assert.expect(rowIndex >= 0, "row index is negative");
    Assert.expect(_availableImageSets != null, "Image set data is null");
    Assert.expect(rowIndex <= _availableImageSets.size(), "row index is greater than the number of elements");

    ImageSetData removedObject = _availableImageSets.remove(rowIndex);

    Assert.expect(removedObject != null , "nothing was removed");
    Assert.expect(_imageSetDataToSelectedMap.containsKey(removedObject), "image set to remove is not found in map");

    Boolean isSelected = _imageSetDataToSelectedMap.get(removedObject);
    _imageSetDataToSelectedMap.remove(removedObject);

    if(isSelected)
    {
      _selectedImageSets.remove(removedObject);
      notifyImageSetSelection();
    }
    fireTableDataChanged();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void insertRow(ImageSetData imageSetData, boolean isSelected)
  {
    Assert.expect(imageSetData != null, "image run to add is null");

    if(_availableImageSets == null)
      _availableImageSets = new ArrayList<ImageSetData>();

    _availableImageSets.add(imageSetData);

    // XCR-3152 Run test button is gray out even one of the image set is checked
    if(isSelected)
      _selectedImageSets.add(imageSetData);

    _imageSetDataToSelectedMap.put(imageSetData,new Boolean(isSelected));

    fireTableRowsInserted(_availableImageSets.size(), _availableImageSets.size());
  }


  /**
   * @author Erica Wheatcroft
   */
  public void sortColumn(int col, boolean ascending)
  {
    _imageSetComparator.setAscending(ascending);
    if(col == 0)
    {
      // the user wants to sort the image sets by what they have selected.
      _imageSetComparator.compareImageSetSelectedStates(_imageSetDataToSelectedMap);
    }
    else if(col == 1)
    {
      // the user wants to sort by the image set name
      _imageSetComparator.compareImageSetNames();
    }
    else if (col == 2)
    {
      // the user wants to sort the image set data by date
      _imageSetComparator.compareImageSetDates();
    }
    else if (col == 3)
    {
      // the user wants to sort the image set data by user description
      _imageSetComparator.compareImageSetDescriptions();
    }
    else if(col == 4)
    {
      // the user wants to sort the test program version

    }
    else if (col == 5)
    {
      // the user wants to switch by what the compatible % is
      _imageSetComparator.compareImageSetCompatablePercents();
    }
    else
      Assert.expect(false,"invalid column number specified");

    // now sort the list
    Collections.sort(_availableImageSets, _imageSetComparator);
    notifyImageSetSelection();
  }

  /**
   * This method will return true if the image set data is found within the list of image sets for the table model, false
   * otherwise
   *
   * @author Erica Wheatcroft
   */
  public boolean contains(ImageSetData imageSetData)
  {
    if(imageSetData == null)
      return false;

    if(_availableImageSets == null)
      return false;

    return _availableImageSets.contains(imageSetData);
  }

  /**
   * @author Erica Wheatcroft
   */
  public void removeImageSet(ImageSetData imageSet)
  {
    Assert.expect(_availableImageSets != null, "Image set data is null");
    Assert.expect(_availableImageSets.contains(imageSet), "image set to remove is not found");
    Assert.expect(_imageSetDataToSelectedMap.containsKey(imageSet), "image set to remove is not found in map");
    _availableImageSets.remove(imageSet);
    _selectedImageSets.remove(imageSet);
    notifyImageSetSelection();

    _imageSetDataToSelectedMap.remove(imageSet);
    fireTableDataChanged();
  }

  // This method returns the Class object of the first
  // cell in specified column in the table model.
  // Unless this method is overridden, all values are
  // assumed to be the type Object.
  public Class getColumnClass(int columnIndex)
  {
    Object o = getValueAt(0, columnIndex);
    if (o == null)
    {
      return Object.class;
    }
    else
    {
      return o.getClass();
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public void selectAllImageSets()
  {
    Assert.expect(_imageSetDataToSelectedMap != null, " image set to selected state map is null");
    _imageSetDataToSelectedMap.clear();

    _selectedImageSets.clear();
    if(_limitNotifications == false)
      _persistance.clearSelectedImageSets();
    for(ImageSetData imageSet : _availableImageSets)
    {
      _imageSetDataToSelectedMap.put(imageSet, new Boolean(true));
      _selectedImageSets.add(imageSet);
      if(_limitNotifications == false)
        _persistance.addSelectedImageSetName(imageSet.getImageSetName());
    }

    notifyImageSetSelection();
    fireTableDataChanged();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void clearSelectedImageSets()
  {
    Assert.expect(_imageSetDataToSelectedMap != null, " image set to selected state map is null");
    _imageSetDataToSelectedMap.clear();

    if (getRowCount() > 0)
    {
      for (ImageSetData imageSet : _availableImageSets)
        _imageSetDataToSelectedMap.put(imageSet, new Boolean(false));
      _selectedImageSets.clear();
      notifyImageSetSelection();
      fireTableDataChanged();
    }
  }

  /**
   * sheng chuan
   * @return 
   */
  public boolean isImageSetEmpty()
  {
    Assert.expect(_imageSetDataToSelectedMap != null, " image set to selected state map is null");
    if(_imageSetDataToSelectedMap.isEmpty())
      return true;
    else
      return false;
  }

}
