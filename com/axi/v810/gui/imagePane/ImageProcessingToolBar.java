/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.imagePane;

import com.axi.guiUtil.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*; //for subtype
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*; //for magnification Enum
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * This class is the main tool bar for all image processing/image adjustment during fine tuning.
 * 
 * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
 *  Copy and modify from FineTuningImagePanel for code centralization.
 */
public class ImageProcessingToolBar extends JToolBar
{
  private GraphicsEngine _graphicsEngine;
  private int _currentDisplayLayer;
  private int _previousDisplayLayer;
  Subtype _currentSubtype = null;

  private JButton _increaseBrightnessButton = new JButton();
  private JButton _decreaseBrightnessButton = new JButton();
  private JButton _increaseContrastButton = new JButton();
  private JButton _decreaseContrastButton = new JButton();
  private JButton _shiftBackwardNormalizeHistogramButton = new JButton();
  private JButton _shiftForwardNormalizeHistogramButton = new JButton();
  private JButton _histogramEqualizationButton = new JButton();
  private JButton _revertButton = new JButton();
  private JToggleButton _CLAHEToggleButton = new JToggleButton();
  private JToggleButton _removeArtifactToggleButton = new JToggleButton();
  JToggleButton _postProcessingDialogToggleButton = new JToggleButton();
  private JToggleButton _FFTBandPassFilterToggleButton = new JToggleButton();
  private JToggleButton _motionBlurToggleButton = new JToggleButton();
  private JToggleButton _InvertImageToggleButton = new JToggleButton();  
  
  private MainMenuGui _mainUI = null;
  private PostProcessingDialog _postProcessingDialog = null;  

  /**
   * @author Lim, Lay Ngor
   */
  public ImageProcessingToolBar(GraphicsEngine graphicsEngine, 
    int imageLayer)
  {
    Assert.expect(graphicsEngine != null);
    
    _graphicsEngine = graphicsEngine;
    _currentDisplayLayer = imageLayer;
    _previousDisplayLayer = _currentDisplayLayer;
    jbInit();
    _mainUI = MainMenuGui.getInstance();
  }

  /**
   * @author Lim, Lay Ngor
   */
  private void jbInit()
  {
    createPostProcessingToolBar();
  }

  /**
   * @author Lim, Lay Ngor
   */
  private void createPostProcessingToolBar()
  {
    _increaseBrightnessButton.setIcon(Image5DX.getImageIcon(Image5DX.DC_INCREASE_BRIGHTNESS));
    _increaseBrightnessButton.setToolTipText(StringLocalizer.keyToString("DCGUI_BRIGHTNESS_INCREASE_KEY"));
    _increaseBrightnessButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        increaseBrightnessButton_actionPerformed();
      }
    });

    _decreaseBrightnessButton.setIcon(Image5DX.getImageIcon(Image5DX.DC_DECREASE_BRIGHTNESS));
    _decreaseBrightnessButton.setToolTipText(StringLocalizer.keyToString("DCGUI_BRIGHTNESS_DECREASE_KEY"));
    _decreaseBrightnessButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        decreaseBrightnessButton_actionPerformed();
      }
    });

    _increaseContrastButton.setIcon(Image5DX.getImageIcon(Image5DX.DC_INCREASE_CONTRAST));
    _increaseContrastButton.setToolTipText(StringLocalizer.keyToString("DCGUI_CONTRAST_INCREASE_KEY"));
    _increaseContrastButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        increaseContrastButton_actionPerformed();
      }
    });

    _decreaseContrastButton.setIcon(Image5DX.getImageIcon(Image5DX.DC_DECREASE_CONTRAST));
    _decreaseContrastButton.setToolTipText(StringLocalizer.keyToString("DCGUI_CONTRAST_DECREASE_KEY"));
    _decreaseContrastButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        decreaseContrastButton_actionPerformed();
      }
    });

    _shiftBackwardNormalizeHistogramButton.setIcon(Image5DX.getImageIcon(Image5DX.DC_ENHANCEMENT_INCREASE));
    _shiftBackwardNormalizeHistogramButton.setToolTipText(StringLocalizer.keyToString("DCGUI_SHIFT_BACKWARD_NORMALIZE_HISTOGRAM_KEY"));
    _shiftBackwardNormalizeHistogramButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        increaseDesiredMaxValueButton_actionPerformed();
      }
    });

    _shiftForwardNormalizeHistogramButton.setIcon(Image5DX.getImageIcon(Image5DX.DC_ENHANCEMENT_DECREASE));
    _shiftForwardNormalizeHistogramButton.setToolTipText(StringLocalizer.keyToString("DCGUI_SHIFT_FORWARD_NORMALIZE_HISTOGRAM_KEY"));
    _shiftForwardNormalizeHistogramButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        decreaseDesiredMaxValueButton_actionPerformed();
      }
    });

    _histogramEqualizationButton.setIcon(Image5DX.getImageIcon(Image5DX.DC_ENHANCEMENT));
    _histogramEqualizationButton.setToolTipText(StringLocalizer.keyToString("DCGUI_NORMALIZE_HISTOGRAM_KEY"));
    _histogramEqualizationButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        histogramEqualizationButton_actionPerformed();
      }
    });

    _revertButton.setIcon(Image5DX.getImageIcon(Image5DX.DC_ORIGINAL));
    _revertButton.setToolTipText(StringLocalizer.keyToString("DCGUI_RESET_TO_ORIGINAL_KEY"));
    _revertButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        revertButton_actionPerformed();
      }
    });
    
    //Change the Enhancer 1, 2 & 3 to its real name so that user able to reuse it at pre-process menu.
    _CLAHEToggleButton.setIcon(Image5DX.getImageIcon(Image5DX.APPLY_CLAHE));
    _CLAHEToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_CLAHE_KEY"));
    _CLAHEToggleButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        imagePostProcessingAction(getImageEnhancerList(), false);
      }
    });
    
    _removeArtifactToggleButton.setIcon(Image5DX.getImageIcon(Image5DX.APPLY_REMOVE_ARTIFACT));
    _removeArtifactToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_REMOVE_ARTIFACT_KEY"));
    _removeArtifactToggleButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        imagePostProcessingAction(getImageEnhancerList(), false);
      }
    });
    
    _FFTBandPassFilterToggleButton.setIcon(Image5DX.getImageIcon(Image5DX.APPLY_FFTBANDPASS_FILTER));
    _FFTBandPassFilterToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_FFTBANDPASS_FILTER_KEY"));
    _FFTBandPassFilterToggleButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        imagePostProcessingAction(getImageEnhancerList(), false);
      }
    });
    
    _motionBlurToggleButton.setIcon(Image5DX.getImageIcon(Image5DX.APPLY_MOTION_BLUR_FILTER));
    _motionBlurToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_MOTION_BLUR_FILTER_KEY"));
    _motionBlurToggleButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        imagePostProcessingAction(getImageEnhancerList(), false);
      }
    });
    
    _InvertImageToggleButton.setIcon(Image5DX.getImageIcon(Image5DX.APPLY_INVERT_IMAGE));
    _InvertImageToggleButton.setToolTipText(StringLocalizer.keyToString("Invert Image"));
    _InvertImageToggleButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        imagePostProcessingAction(getImageEnhancerList(), false);
      }
    });
    
    _postProcessingDialogToggleButton.setIcon(Image5DX.getImageIcon(Image5DX.TOGGLE_POST_PROCESSING_EXPERT));
    _postProcessingDialogToggleButton.setToolTipText(StringLocalizer.keyToString("Post Processing - Expert Mode"));
    _postProcessingDialogToggleButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        postProcessingDialogToggleButton_actionPerformed();
      }
    });
    
    add(_increaseBrightnessButton);
    add(_decreaseBrightnessButton);
    addSeparator();

    add(_increaseContrastButton);
    add(_decreaseContrastButton);
    addSeparator();

    add(_shiftBackwardNormalizeHistogramButton);
    add(_histogramEqualizationButton);
    add(_shiftForwardNormalizeHistogramButton);
    addSeparator();

    add(_revertButton);
    addSeparator();
    
    add(_InvertImageToggleButton);
    add(_CLAHEToggleButton);
    add(_removeArtifactToggleButton);
    add(_FFTBandPassFilterToggleButton);
//    add(_motionBlurToggleButton);
    addSeparator();
    
    //Temporary not support the expert post processing dialog until further notice.
//    add(_postProcessingDialogToggleButton);
//    addSeparator();

    setVisible(false);//will visible upon user click on the toolBarButton
    setOrientation(JToolBar.HORIZONTAL);
  }

  /**
   * @author Chong, Wei Chin
   */
  void increaseBrightnessButton_actionPerformed()
  {
    Assert.expect(_graphicsEngine != null);
    _graphicsEngine.increaseBrightnessForRendererPanel(_currentDisplayLayer);
  }

  /**
   * @author Chong, Wei Chin
   */
  void decreaseBrightnessButton_actionPerformed()
  {
    Assert.expect(_graphicsEngine != null);
    _graphicsEngine.decreaseBrightnessForRendererPanel(_currentDisplayLayer);
  }

  /**
   * @author Chong, Wei Chin
   */
  void increaseContrastButton_actionPerformed()
  {
    Assert.expect(_graphicsEngine != null);
    _graphicsEngine.increaseContrastForRendererPanel(_currentDisplayLayer);
  }

  /**
   * @author Chong, Wei Chin
   */
  void decreaseContrastButton_actionPerformed()
  {
    Assert.expect(_graphicsEngine != null);
    _graphicsEngine.decreaseContrastForRendererPanel(_currentDisplayLayer);
  }

  /**
   * @author Chong, Wei Chin
   */
  void increaseDesiredMaxValueButton_actionPerformed()
  {
    Assert.expect(_graphicsEngine != null);
    _graphicsEngine.increaseNormalizeForRendererPanel(_currentDisplayLayer);
  }

  /**
   * @author Chong, Wei Chin
   */
  void decreaseDesiredMaxValueButton_actionPerformed()
  {
    Assert.expect(_graphicsEngine != null);
    _graphicsEngine.decreaseNormalizeForRendererPanel(_currentDisplayLayer);
  }

  /**
   * @author Chong, Wei Chin
   */
  void histogramEqualizationButton_actionPerformed()
  {
    Assert.expect(_graphicsEngine != null);
    _graphicsEngine.equalizeGrayScaleForRendererPanel(_currentDisplayLayer);
  }

  /**
   * @author Chong, Wei Chin
   */
  void revertButton_actionPerformed()
  {
    Assert.expect(_graphicsEngine != null);
    _graphicsEngine.revertToOriginalBufferedImageForRendererPanel(_currentDisplayLayer);
    resetAllImageProcessingButton();
  }

  /**
   * @author Lim, Lay Ngor
   */
  private void resetAllImageProcessingButton()
  {
    _CLAHEToggleButton.setSelected(false);
    _removeArtifactToggleButton.setSelected(false);
    _FFTBandPassFilterToggleButton.setSelected(false);
    _motionBlurToggleButton.setSelected(false);
    _InvertImageToggleButton.setSelected(false);
    
    List<ImageEnhancerBase> enhancerList = new ArrayList<ImageEnhancerBase>();
    enhancerList.clear();
  }
  
 /**
  * @author Lim, Lay Ngor
  */
  public void imagePostProcessingAction(List<ImageEnhancerBase> enhancerList, boolean withPreviousLayer)
  {
    Assert.expect(_graphicsEngine != null);    
    if(withPreviousLayer)
      _graphicsEngine.imagePostProcessingWithSettingForRendererPanel(
        _currentDisplayLayer, enhancerList, _previousDisplayLayer);
    else
      _graphicsEngine.imagePostProcessingForRendererPanel(_currentDisplayLayer, enhancerList);
  }
  
  /**
   * To get the list of Image Enhancer object which is selected on GUI.
   * @return The list of Image Enhancer object which is selected on GUI.
   * @author Lim, Lay Ngor
   */
  public List<ImageEnhancerBase> getImageEnhancerList()
  {
    int blockSize = (int)getBlockSize();
    if(blockSize == -1)
      blockSize = 30;//use default block size!
    
    List<ImageEnhancerBase> enhancerList = new ArrayList<ImageEnhancerBase>();
    if(_CLAHEToggleButton.isSelected() == true)
    {
      ImageEnhancerBase enhancer = new CLAHE(blockSize, 256, 3, true);
      enhancerList.add(enhancer);
    }
    
    if(_removeArtifactToggleButton.isSelected() == true)
    {
      ImageEnhancerBase enhancer = new RemoveArtifactByBoxFilter(blockSize, blockSize, 1);
      enhancerList.add(enhancer);
    }
    
    if(_FFTBandPassFilterToggleButton.isSelected() == true)
    {
      ImageEnhancerBase enhancer = new FFTBandPassFilter
        (blockSize + 5, 3, 5.0, FilterDirectionModeEnum.None, true, 5, InterpolationModeEnum.Linear);
      enhancerList.add(enhancer);
    }
    
    if(_motionBlurToggleButton.isSelected() == true)
    {
      ImageEnhancerBase enhancer = new MotionBlur(60, 80, FilterDirectionModeEnum.Horizontal, 1);
      enhancerList.add(enhancer);
    }
    
    if(_InvertImageToggleButton.isSelected() == true)
    {
      ImageEnhancerBase enhancer = new InvertImage();
      enhancerList.add(enhancer);
    }
    
    return enhancerList;
  }
    
 /**
  * Get the component block size for image enhancer block radius use.
  * @author Lim, Lay Ngor
  */
  public double getBlockSize()
  {
    if (_currentSubtype == null || _currentSubtype.getPads().isEmpty())
    {
      if(Config.isDeveloperDebugModeOn())//Print out only when it is developer debug mode.
        System.out.println("Failed to get block size. Subtype is null.");
      
      return -1;
    }

    final double width = _currentSubtype.getPads().get(0).getShapeRelativeToPanelInNanoMeters().getBounds().getWidth();
    final double height = _currentSubtype.getPads().get(0).getShapeRelativeToPanelInNanoMeters().getBounds().getHeight();
    
    double blockSize;
    if (height > width)
      blockSize = width;
    else
      blockSize = height;
    
    double NANOMETERS_PER_PIXEL = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
    
    if(_currentSubtype.getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
      NANOMETERS_PER_PIXEL = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
    
    int scaleFactor = 1;
    if(useResize(_currentSubtype))
      scaleFactor = (Integer) _currentSubtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_SCALE);
    
    final double PIXELS_PER_NANOMETER = scaleFactor * 1.0 / NANOMETERS_PER_PIXEL;
    
    blockSize = blockSize * PIXELS_PER_NANOMETER;
    
    final int BLOCK_SIZE_TOLERANCE_VALUE = 2;
    blockSize = blockSize + BLOCK_SIZE_TOLERANCE_VALUE;
    
    return blockSize;
  }
  
 /**
  * @author Lim, Lay Ngor
  */
  private static boolean useResize(Subtype subtype)
  {
    boolean useResize = false;
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_ENABLE))
    {
        java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_ENABLE);
        Assert.expect(value instanceof String);
        if (value.equals("False"))
          useResize = false;
        else if (value.equals("True"))
          useResize = true;
        else
          Assert.expect(false, "Unexpected Resize value.");
    }
    return useResize;
  }
  
 /**
  * @author Lim, Lay Ngor
  */
  public void setpostProcessingDialogToggleButtonSelected(boolean isSelected)
  {
    _postProcessingDialogToggleButton.setSelected(isSelected);
  }
  
 /**
  * Show the post processing expert dialog.
  * @author Lim, Lay Ngor
  */
  private void postProcessingDialogToggleButton_actionPerformed()
  {
    if (_postProcessingDialogToggleButton.isSelected())
    {
      if (_postProcessingDialog == null)
      {
        _postProcessingDialog = new PostProcessingDialog(_mainUI,
          StringLocalizer.keyToString("IMTB_IMAGE_PROCESSING_EXPERT_DIALOG_KEY"),
          false,//set dialog to ModalityType.MODELESS 
          this);
      }

      int y_position = (int) (_postProcessingDialog.getParent().getSize().height * 0.4);
      int x_position = (int) (_postProcessingDialog.getParent().getSize().width * 0.05);
      _postProcessingDialog.setLocation(x_position, y_position);
      //SwingUtils.centerOnComponent(_postProcessingDialog, _mainUI);      
    }
    
    if(_postProcessingDialog != null)
      _postProcessingDialog.setVisible(_postProcessingDialogToggleButton.isSelected());    
  }

  /**
   * Update image at Fine tune GUI together with all image processing setting as
   * previous renderer. Update the current subtype and previous image renderer
   * for post processing use.
   *
   * @param subtype - Use for retrieving the component size for image enhancer block radius use.
   * @param previousImageRenderer - Use for retrieving the previous image renderer pointer to get the previous 
   *                                                          image BrightnessAndContrast Setting so that we can apply the same setting 
   *                                                          to current image.
   * @author Lim, Lay Ngor
   */
  public void setCurrentSubtypeAndRenderer(Subtype subtype, InspectionImageRenderer previousImageRenderer)
  {
    Assert.expect(_graphicsEngine != null);        
    _currentSubtype = subtype;
    //enhance image everytime subtype is set
    if (_postProcessingDialogToggleButton.isSelected())
    {
      if (_postProcessingDialog != null)
      {
        List<ImageEnhancerBase> enhancerList = _postProcessingDialog.getPostProcessingEnhancerList();
        imagePostProcessingAction(enhancerList, false);
      }
    }
    else
    {
      _graphicsEngine.imagePostProcessingWithSettingForRendererPanel(
      _currentDisplayLayer, getImageEnhancerList(), previousImageRenderer.getBrightnessAndContrastSetting());
    }
  }
  
  /**
   *  Update the Virtual Live image layer with post processing applied to display GUI.
   * @author Lim, Lay Ngor
   */
  public void updateCurrentDisplayLayer(int currentDisplayLayer)
  {
    //Update VL latest image layer
    _previousDisplayLayer = _currentDisplayLayer;
    _currentDisplayLayer = currentDisplayLayer;    
    List<ImageEnhancerBase> enhancerList = null;

    if (_postProcessingDialogToggleButton.isSelected() && _postProcessingDialog != null)
      enhancerList = _postProcessingDialog.getPostProcessingEnhancerList();
    
    //If post processing dialog did not trigger, use back default value.
    if(enhancerList == null)
      enhancerList = getImageEnhancerList();
    
    imagePostProcessingAction(enhancerList, true);
  }
  
  /**
   * XCR2125: Update the current and previous Virtual Live image layer without post processing 
   * applied to display GUI.
   * This function will reset all the image processing button and revert all the images back 
   * to original buffer image. The reason is because VL too slow in apply certain post 
   * processing to large image. (e.g. image size larger than 2568 x 1945 will be sluggish 
   * when we drag the VL z height).
   * Note: call updateCurrentDisplayLayer() if user wish to apply post processing to updated 
   * layer image.
   * The Post processing that consume computation time is: All "Normalize Histogram" related 
   * processing, CLAHE, RemoveArtifact & FFTBandPassFilter.
   * @author Lim, Lay Ngor
   */
  public void revertCurrentAndPreviousDisplayLayerToOriginal(int currentDisplayLayer)
  {
    //Update VL latest image layer but remove the post processing feature.
    _previousDisplayLayer = _currentDisplayLayer;
    //1. revert the previous layer to original buffer image
    revertButton_actionPerformed();  
    //2. update _currentDisplayLayer to latest layer
    _currentDisplayLayer = currentDisplayLayer;       
    //3. Revert the latest layer 
    revertButton_actionPerformed();
  }
}

