package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.Shape;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.MagnificationTypeEnum;
import com.axi.v810.hardware.*;


/**
 * This class is responsible for drawing the refernece designator
 *
 * @author George A. David
 */
class JointTypeRenderer extends TextRenderer
{
  private com.axi.v810.business.panelDesc.Component _component;
  private Pad _pad;
  private boolean _useAlignedShape = false;

  // This get's complicated.  I want the joint type to be displayed directly under the ref des or the
  // pad name, and I don't want the distance between the name and joint type to change as the zoom
  // level changes.  A normal TextRenderer, when not centered, will move as the zoom level moves,
  // and I can't do a multi-line thing TextRenderer either.
  // So, here's my trick:  I define this TextRenderer to be centered (just like ReferenceDesignatorRenderer or
  // PadNameRenderer), but of a very large font.  The, override paintComponent, and change the font size to a
  // much smaller one, and center it in X in the rectangle created by using the large font.  That way the
  // relationship of where we paint it doesn't change in relation to the center of the component or pad.
  private Font _sizeFont = FontUtil.getRendererFont(48);
  private Font _actualFont = FontUtil.getRendererFont(12);

  /**
   * @author George A. David
   */
  JointTypeRenderer(com.axi.v810.business.panelDesc.Component component, boolean useAlignedShape)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(component != null);
    _component = component;
    _useAlignedShape = useAlignedShape;

    // set a font to determine the size of the rectangle
    setFont(_sizeFont);  // very large

    refreshData();
  }

  /**
   * @author George A. David
   */
  JointTypeRenderer(Pad pad, boolean useAlignedShape)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(pad != null);
    _pad = pad;
    _useAlignedShape = useAlignedShape;

    // set a font to determine the size of the rectangle
    setFont(_sizeFont);  // very large

    refreshData();
  }

  /**
   * @author George A. David
   * @EditedBy Kee Chin Seong
   */
  protected void refreshData()
  {
    String jointTypeString = "";
    Shape shape = null;
    if (_component != null)
    {
      for (JointTypeEnum jointTypeEnum : _component.getJointTypeEnums())
      {
        jointTypeString = jointTypeString + jointTypeEnum + " ";
      }
      // remove the trailing space
      jointTypeString = jointTypeString.substring(0, jointTypeString.length() - 1);
      if (_useAlignedShape)
        shape = _component.getAlignedShapeRelativeToPanelInNanoMeters();
      else
        shape = _component.getShapeRelativeToPanelInNanoMeters();
    }
    else  // pad case
    {
      if (_pad.getPackagePin().getCompPackage().usesOneJointTypeEnum())
      {
        jointTypeString = "";  // no need to show it at the pad level if all are the same!
      }
      else
        jointTypeString = _pad.getJointTypeEnum().getName();

      if (_useAlignedShape)
        shape = _pad.getAlignedShapeRelativeToPanelInNanoMeters();
      else
        shape = _pad.getShapeRelativeToPanelInNanoMeters();
    }

    Rectangle2D bounds = shape.getBounds2D();
    int nanoMetersPerPixel = 0;
    
    //Kee Chin Seong - Get the High Mag Or Low Mag AffineTransform in order Get the correct Scale
    
    if(_pad != null)
    {
       if(_pad.getComponent().getBoard().getPanel().getProject().getTestProgram().isUsingOneMagnification() == false)
       {
          nanoMetersPerPixel = MagnificationEnum.NOMINAL.getNanoMetersPerPixel(); 
       }
       else
       {
          if(_pad.getSubtype().getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
             nanoMetersPerPixel = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
          else
             nanoMetersPerPixel = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();  
       }
    }
    else
    {
       if(_component.isComponentUseSingleMagnification() == false || 
          _component.getBoard().getPanel().getProject().getTestProgram().isUsingOneMagnification() == false)
       {
         nanoMetersPerPixel = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
       }
       else
       {
           if(_component.getPadOne().getSubtype().getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
              nanoMetersPerPixel = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();  
           else
              nanoMetersPerPixel = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
       }
    }
    
    initializeText(jointTypeString,
                       bounds.getCenterX() / nanoMetersPerPixel,
                       (bounds.getCenterY() / nanoMetersPerPixel),
                       true);
    // center this text -- sets the relative position
   
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void paintComponent(Graphics graphics)
  {
    String text = getText();
    Font origGraphicsFont = graphics.getFont();
    Color origColor = graphics.getColor();
    Font rendererFont = getFont();
    FontMetrics fontMetrics = getFontMetrics(rendererFont);
    graphics.setFont(_actualFont);
    Assert.expect(text != null);

    // the Height is the max height needed to display any text in this font (ascent + descent + 1)
    // basically, the height of the bounding rectangle.
    // DrawString, however, needs the X, Y of the leftmost BASELINE point, so we need to subtract the max descent
    // from this Y otherwise any character below the baseline will be cut off.
    // we subtract another one, to better center it, and the height stuff added one as well. (Which is doesn't need to
    // do for text, but is needed for other shapes)

    FontMetrics acutalFontMetrics = getFontMetrics(FontUtil.getRendererFont(12));
    int textWidth = acutalFontMetrics.stringWidth(text);
    int xpos = getWidth()/2 - textWidth / 2;

    graphics.drawString(text, xpos, getHeight() - fontMetrics.getMaxDescent() - 1);


    graphics.setFont(origGraphicsFont);
    graphics.setColor(origColor);
  }
}
