package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.hardware.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.MagnificationTypeEnum;


/**
 * This class is responsible for drawing the refernece designator
 *
 * @author George A. David
 */
public class PadNameRenderer extends TextRenderer
{
  private Pad _pad;
  private boolean _useAlignedShape;
  private boolean _useMagnification = true;

  
  /**
   * @author Wei Chin
   */
  public PadNameRenderer(Pad pad, boolean useAlignedShape, boolean useMagnification)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(pad != null);
    _pad = pad;
    _useAlignedShape = useAlignedShape;
    _useMagnification = useMagnification;

    // set a default font
    setFont(FontUtil.getRendererFont(16));

    refreshData();
  }
  
  /**
   * @author George A. David
   */
  public PadNameRenderer(Pad pad, boolean useAlignedShape)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(pad != null);
    _pad = pad;
    _useAlignedShape = useAlignedShape;
    _useMagnification = true;

    // set a default font
    setFont(FontUtil.getRendererFont(16));

    refreshData();
  }

  /**
   * @author George A. David
   * @EditedBy Kee Chin Seong
   */
  protected void refreshData()
  {
    String padName = _pad.getName();

    Shape shape = null;
    if(_useAlignedShape)
      shape = _pad.getAlignedShapeRelativeToPanelInNanoMeters();
    else
      shape = _pad.getShapeRelativeToPanelInNanoMeters();

    Rectangle2D bounds = shape.getBounds2D();

    //Kee Chin Seong - Get the High Mag Or Low Mag AffineTransform in order Get the correct Scale
    if(_useMagnification)
    {
       if(_pad.getComponent().getBoard().getPanel().getProject().getTestProgram().isUsingOneMagnification() == false)
         initializeText(padName, bounds.getCenterX() / MagnificationEnum.NOMINAL.getNanoMetersPerPixel(), bounds.getCenterY() / MagnificationEnum.NOMINAL.getNanoMetersPerPixel(), true);
       else
       {
           if(_pad.getSubtype().getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
           {
              initializeText(padName, bounds.getCenterX() / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel(), bounds.getCenterY() / MagnificationEnum.NOMINAL.getNanoMetersPerPixel(), true);
           }
           else      
           {
              initializeText(padName, bounds.getCenterX() / MagnificationEnum.NOMINAL.getNanoMetersPerPixel(), bounds.getCenterY() / MagnificationEnum.NOMINAL.getNanoMetersPerPixel(), true);
           }
       }
    }
    else
      initializeText(padName, bounds.getCenterX(), bounds.getCenterY(), true);
  }

}
