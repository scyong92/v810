package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.util.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.algoTuner.AlgoTunerGui;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.util.*;

/**
 * @author Erica Wheatcroft
 */
public class ImageManagerPanel extends JPanel implements Observer
{
  private ImageManagerToolBar _toolBar = null;
  private ImagePanel _imagePanel = null;
  private BorderLayout _mainBorderLayout = new BorderLayout();
  private JSplitPane _splitPane = null;
  private SelectImageSetPanel _selectImageSetPanel;
  private java.util.List<ImageSetData> _selectedImageSets = new LinkedList<ImageSetData>();

  private boolean _isSelectImageSetPanelVisible = false;
  private Project _project = null;
  private ImageManager _imageManager = ImageManager.getInstance();
  private boolean _isGenerateLatestTestProgram;

  /**
   * @author Erica Wheatcroft
   * @author Scott Richardson
   */
  public ImageManagerPanel(boolean virtualLiveMode)
  {
    _toolBar = new ImageManagerToolBar(this);
    try
    {
      if (LicenseManager.isOfflineProgramming())
      {
        _toolBar.setAllowGenerateImageSetWizard(false);
      }
    }
    catch (final BusinessException e)
    {
      
    }
    _imagePanel = new ImagePanel(_toolBar);
    GuiObservable.getInstance().addObserver(this);
    createPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setLayout(_mainBorderLayout);
    add(_toolBar, java.awt.BorderLayout.NORTH);
    add(_imagePanel, java.awt.BorderLayout.CENTER);
  }

  /**
   * @author Erica Wheatcroft
   */
  public void displayAlignmentImagePanel()
  {
    _toolBar.setImagePanelGraphicsEngineSetup(_imagePanel.getAlignmentImagePanel().getAlignmentImageGraphicsEngineSetup());
    _imagePanel.displayAlignmentImagePanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void displayFineTuningImagePanel()
  {
    _toolBar.setImagePanelGraphicsEngineSetup(_imagePanel.getFineTuningImagePanel().
                                              getFineTuningImageGraphicsEngineSetup());
    _imagePanel.displayFineTuningImagePanel();
  }

  /**
   * @author George A. David
   */
  public void finishFineTuningImagePanel()
  {
    _imagePanel.finishFineTuningImagePanel();
  }

  public void saveAlgoTunerInstance(AlgoTunerGui algoTunerGui)
  {
      _imagePanel.saveAlgoTunerInstance(algoTunerGui);
  }

  /**
   * @author Erica Wheatcroft
   */
  public void displayVerificationImagePanel(boolean checkLatestestProgram)
  {
    _toolBar.setImagePanelGraphicsEngineSetup(_imagePanel.getVerificationImagePanel().
                                              getVerificationImageGraphicsEngineSetup());
    _imagePanel.displayVerificationImagePanel(checkLatestestProgram);
  }

  /**
   * @author George A. David
   */
  public void finishVerificationImagePanel()
  {
    _imagePanel.finishVerificationImagePanel();
  }
  
   /**
   * @author weng-jian.eoh
   * XCR-3589 Combo STD and XXL software GUI
   */
  public void finishAlignmentImagePanel()
  {
    _imagePanel.finishAlignmentImagePanel();
  }

  /**
   * @param project Project
   * @author Erica Wheatcroft
   */
  public void populateWithProjectData(Project project)
  {
    Assert.expect(project != null, "Project is null");
    _project = project;
    
    try
    {
      createImageSetPanelAndSeperator(true);
      displaySelectImageSetPanel(false);
    }
    catch(XrayTesterException ex)
    {
      System.out.println(ex.getLocalizedMessage());
    }
    
    _toolBar.populateWithProjectData(_project);
  }

 /**
  * edit by sheng chuan because image set can be configured to load in fast or slow mode
  * set true to force image set fully load before display
  * set false to enable image set parallel with load and display
  */
  public void createImageSetPanelAndSeperator(boolean needToWaitImageDataCollection) throws XrayTesterException
  {   
     _selectImageSetPanel = new SelectImageSetPanel();
     _selectImageSetPanel.populateWithProject(_project, needToWaitImageDataCollection);
     // Swee Yee Wong - XCR-2734	Update Nominals button suddenly disabled.
     _selectedImageSets.clear();
     _selectedImageSets.addAll(_selectImageSetPanel.getSelectedImageSets());
     
     if(_splitPane == null)
       _splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, _selectImageSetPanel, _imagePanel);
     else
     {
       _splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
       _splitPane.setLeftComponent(_selectImageSetPanel);
       _splitPane.setRightComponent(_imagePanel);
     }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _project = null;
    _imagePanel.unpopulate();
    _toolBar.resetForNewProject();
    // _toolBar.unpopulate();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void displaySelectImageSetPanel(boolean showPanel) throws XrayTesterException
  {
    if (showPanel)
    {
     /*_selectImageSetPanel = new SelectImageSetPanel();
      _selectImageSetPanel.populateWithProject(_project);
     _splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, _selectImageSetPanel, _imagePanel);*/
      createImageSetPanelAndSeperator(false);
      removeAll();
      setLayout(_mainBorderLayout);
      add(_toolBar, java.awt.BorderLayout.NORTH);
      add(_splitPane, java.awt.BorderLayout.CENTER);

      repaint();
      revalidate();

      _isSelectImageSetPanelVisible = true;
    }
    else
    {
      _isSelectImageSetPanelVisible = false;
      removeAll();
      _selectImageSetPanel.clear();
      _selectImageSetPanel = null;
      setLayout(_mainBorderLayout);
      add(_toolBar, java.awt.BorderLayout.NORTH);
      add(_imagePanel, java.awt.BorderLayout.CENTER);
      repaint();
      revalidate();
    }

  }

  /**
   * @author Erica Wheatcroft
   */
  boolean isSelectImageSetPanelVisible()
  {
    return _isSelectImageSetPanelVisible;
  }

  /**
   * @author George A. David
   */
  public AlignmentImagePanel getAlignmentImagePanel()
  {
    return _imagePanel.getAlignmentImagePanel();
  }

  /**
   * @author George A. David
   */
  public VerificationImagePanel getVerificationImagePanel()
  {
    return _imagePanel.getVerificationImagePanel();
  }

  /**
   * @author Andy Mechtenberg
   */
  public FineTuningImageGraphicsEngineSetup getFineTuningGraphicsEngineSetup()
  {
    return _imagePanel.getFineTuningImagePanel().getFineTuningImageGraphicsEngineSetup();
  }


  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(Observable observable, Object object)
  {
    Assert.expect(observable != null, " observable is null");

    // this should be an update from something already on the swing thread. if not then assert.
    Assert.expect(SwingUtilities.isEventDispatchThread(), "update not from a swing thread");

    if (observable instanceof GuiObservable)
    {
      if (object instanceof GuiEvent)
      {
        GuiEventEnum guiEventEnum = ((GuiEvent)object).getGuiEventEnum();
        if (guiEventEnum instanceof ImageSetEventEnum)
        {
          if (guiEventEnum.equals(ImageSetEventEnum.IMAGE_SET_CREATED))
          {
            if (_isSelectImageSetPanelVisible)
            {
              Assert.expect(_selectImageSetPanel != null);
              Assert.expect(_splitPane != null);
              _splitPane.setDividerLocation(_splitPane.getDividerLocation() + _selectImageSetPanel.getTableRowHeight());
            }
          }
        }
        else if (guiEventEnum instanceof TaskPanelScreenEnum)
        {
         /* if (_project != null)
          {
            if (guiEventEnum.equals(TaskPanelScreenEnum.ALGORITHM_TUNER) ||
                guiEventEnum.equals(TaskPanelScreenEnum.INITIAL_TUNE))
            {
              // lets figure out if the user has selected any image sets for this project
              java.util.List<ImageSetData> selectedImageSets = new ArrayList<ImageSetData>();
              ProjectPersistance persistance = MainMenuGui.getInstance().getTestDev().getPersistance().
                                               getProjectPersistance();
              java.util.List<String> savedImageSetNames = persistance.getSelectedImageSetNames();
              if (savedImageSetNames.size() == 0)
              {
                // since nothing was persisted then fire an event indicating nothins was selected.
                GuiObservable.getInstance().stateChanged(selectedImageSets, ImageSetEventEnum.IMAGE_SET_NOT_SELECTED);
                return;
              }

              // okay we have some persisted image sets lets get a test program and make sure they are still compatible
              MainMenuGui.getInstance().generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_IMAGE_SET_GEN_NEEDS_TEST_PROGRAM_KEY"),
                                                                              StringLocalizer.keyToString("IMTB_GENERATE_IMAGE_SET_KEY"),
                                                                              _project);

              _project.getTestProgram().clearFilters();

              try
              {
                java.util.List<ImageSetData> compatibleImageSets = new ArrayList<ImageSetData>(_imageManager.
                    getCompatibleImageSetData(_project));
                if (savedImageSetNames.size() > 0)
                {
                  for (String imageSetName : savedImageSetNames)
                  {
                    for (ImageSetData imageSetData : compatibleImageSets)
                    {
                      if (imageSetData.getImageSetName().equalsIgnoreCase(imageSetName))
                      {
                        selectedImageSets.add(imageSetData);
                        break;
                      }
                    }
                  }
                }
                if (selectedImageSets.isEmpty() == false)
                {
                  GuiObservable.getInstance().stateChanged(selectedImageSets, ImageSetEventEnum.IMAGE_SET_SELECTED);
                }
              }
              catch (DatastoreException de)
              {
                // ignore this exception. if there is a problem when the user goes to the select image set panel they will be
                // notified of the exact problem.
              }


            }
            else
            {
              // we are not on the tuner of initial tunning screens.. so ignore this update
            }
          }*/
          }
        }
      }
    else
    {
      Assert.expect(false);
    }
  }
  
  /**
   * @author Wei Chin
   */
  public void displayAlignmentImagePanelForHighMag()
  {
    _toolBar.setImagePanelGraphicsEngineSetup(_imagePanel.getAlignmentImagePanelForHighMag().getAlignmentImageGraphicsEngineSetup());
    _imagePanel.displayAlignmentImagePanelForHighMag();
  }

  /**
   * @author Wei Chin
   */
  public void displayVerificationImagePanelForHighMag(boolean checkLatestestProgram)
  {
    _toolBar.setImagePanelGraphicsEngineSetup(_imagePanel.getVerificationImagePanelForHighMag().
                                              getVerificationImageGraphicsEngineSetup());
    _imagePanel.displayVerificationImagePanelForHighMag(checkLatestestProgram);
  }

  /**
   * @author Wei Chin
   */
  public void finishVerificationImagePanelForHighMag()
  {
    _imagePanel.finishVerificationImagePanelForHighMag();
  }
  
  /**
   * @author Wei Chin
   */
  public AlignmentImagePanel getAlignmentImagePanelForHighMag()
  {
    return _imagePanel.getAlignmentImagePanelForHighMag();
  }

  /**
   * @author Wei Chin
   */
  public VerificationImagePanel getVerificationImagePanelForHighMag()
  {
    return _imagePanel.getVerificationImagePanelForHighMag();
  }
  
  /**
   * @author Swee Yee Wong
   * get selectedImageSets once image panel is populated
   */
  public java.util.List<ImageSetData> getSelectedImageSets()
  {
    return _selectedImageSets;
  }

  /**
   * XCR-3589 Combo STD and XXL software GUI
   *
   * @author weng-jian.eoh
   *
   */
  public void removeObservable()
  {
    GuiObservable.getInstance().deleteObserver(this);
  }
  
  public ImageManagerToolBar getImageManagerToolBar()
  {
    return _toolBar;
  }
}
