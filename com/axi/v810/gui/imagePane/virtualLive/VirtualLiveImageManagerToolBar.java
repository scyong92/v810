package com.axi.v810.gui.imagePane.virtualLive;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.axi.v810.gui.imagePane.*;
import com.axi.v810.images.*;

/**
 * @author Scott Richardson
 */
public class VirtualLiveImageManagerToolBar extends ImageManagerToolBar
{
  private JButton _translateUpZAxisButton = new JButton();
  private JButton _translateDownZAxisButton = new JButton();

  /**
   * @author Scott Richardson
   */
  public VirtualLiveImageManagerToolBar(ImageManagerPanel parent)
  {
    super(parent);

    _translateUpZAxisButton.setToolTipText("Move Up in the Z-Axis");
    _translateUpZAxisButton.setIcon(Image5DX.getImageIcon(Image5DX.DC_VIEW_BOTTOMSIDE));
    _translateUpZAxisButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        translateUpInZAxis_actionPerformed(e);
      }
    });

    _translateDownZAxisButton.setToolTipText("Move Down in the Z-Axis");
    _translateDownZAxisButton.setIcon(Image5DX.getImageIcon(Image5DX.DC_VIEW_TOPSIDE));
    _translateDownZAxisButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        translateDownInZAxis_actionPerformed(e);
      }
    });

    _translateUpZAxisButton.setName(".translateUpZAxisButton");
    _translateDownZAxisButton.setName(".translateDownZAxisButton");

    _translateDownZAxisButton.setFocusable(false);
    _translateUpZAxisButton.setFocusable(false);
    _translateUpZAxisButton.setEnabled(true);
    _translateDownZAxisButton.setEnabled(true);

    add(_translateUpZAxisButton);
    add(_translateDownZAxisButton);
  }

  /**
   * @author Scott Richardson
   */
  public void translateUpInZAxis_actionPerformed(ActionEvent e)
  {
    _graphicsEngineSetup.translateUpInZAxis();
  }

  /**
   * @author Scott Richardson
   */
  public void translateDownInZAxis_actionPerformed(ActionEvent e)
  {
    _graphicsEngineSetup.translateDownInZAxis();
  }

  /**
   * @author Scott Richardson
   */
  protected void createToolBar()
  {
    removeAll();

    Dimension separatorSpacing = new Dimension(10, 25); // 10 is width, 25 is height

    add(_zoomInButton);
    add(_zoomOutButton);
    add(_zoomRectangleToggleButton);
    add(_dragToggleButton);
    add(_resetGraphicsButton);
    addSeparator(separatorSpacing);

    add(_measureToggleButton);
    add(_toggleGraphicsToggleButton);
    addSeparator(separatorSpacing);
  }
}
