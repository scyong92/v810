package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.virtuallive.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.virtualLive.*;
import com.axi.v810.util.*;


/**
 * <p>Title: SelectImageSetPanel</p>
 *
 * <p>Description:  This panel will allow the user to select one virtual live image set to display.</p>
 *
 * @author sham
 */
public class VirtualLiveImageSetSelectPanel extends JPanel implements Observer
{
  private String _projectName;
  private VirtualLiveImageSetTableModel _selectImageSetModel = null;
  private static JTable _selectImageSetTable;
  private JScrollPane _selectImageSetPane;
  private ArrayList<String> _directoryName;
  private ArrayList<String> _virtualLiveImageSetDate;
  private JPanel _buttonPanel;
  private JButton _deleteImageSetButton;
  private ArrayList<Vector> _rows;
  private String _columns[] =
  {
    StringLocalizer.keyToString("VIRTUAL_LIVE_DISPLAY_IMAGE_SET_COLUMN_KEY"),
    StringLocalizer.keyToString("SELECTED_IMAGE_SET_NAME_COLUMN_KEY"),
    StringLocalizer.keyToString("SELECTED_DATE_CREATED_COLUMN_KEY"),
    " "
  };
  private int _totalRows = 0;
  private static int _selectedIndex = -1;
  private static int _previousSelectedIndex = -1;
  private boolean _offlineDisplayMode;
  private JLabel _EMPTY_IMAGE_SET=new JLabel(StringLocalizer.keyToString("SELECTED_NO_IMAGE_SETS_EXIST_KEY"));

  /**
   * @author sham
   */
  public VirtualLiveImageSetSelectPanel()
  {
    add(_EMPTY_IMAGE_SET);
  }

  /**
   * @author sham
   */
  public void populateWithData(String projectName)
  {
    //Siew Yeng - XCR-3284
    GuiObservable.getInstance().addObserver(this);
    if(_selectImageSetModel==null)
    {
      
      _projectName = projectName;

      String virtualLiveImagesDir = Directory.getXrayVirtualLiveImagesDir(_projectName);
      Collection<String> directoryNameList = FileUtil.listAllSubDirectoriesInDirectory(virtualLiveImagesDir);
      _directoryName = (ArrayList<String>) directoryNameList;
      _totalRows = _directoryName.size();
      if(_totalRows > 0)
      {
        _virtualLiveImageSetDate = new ArrayList<String>();
        for (int i = 0; i < _totalRows; i++)
        {
          try
          {
            _virtualLiveImageSetDate.add(StringUtil.convertMilliSecondsToTimeAndDate(StringUtil.convertStringToTimeInMilliSeconds(_directoryName.get(i))));
          }
          catch (BadFormatException ex)
          {
            _virtualLiveImageSetDate.add(" ");
          }
        }
        createTableModal();
        createSortTable();
        createVirtualLiveSelectImageSetPanel();
      }
      else
      {
        removeAll();
        _selectedIndex=-1;
        _previousSelectedIndex=-1;
        add(_EMPTY_IMAGE_SET);
      }
    }
  }

  /**
   * @author sham
   */
  private void createTableModal()
  {
    Vector data;
    _rows = new ArrayList<Vector>();
    for (int i = 0; i < _totalRows; i++)
    {
      //Siew Yeng - XCR-3284 - add radio button at 1st column
      data = new Vector(_columns.length);
      data.addElement(Boolean.FALSE);
      data.addElement(_directoryName.get(i));
      data.addElement(_virtualLiveImageSetDate.get(i));
      data.addElement(Boolean.FALSE);
      _rows.add(data);
    }
    _selectImageSetModel = new VirtualLiveImageSetTableModel(_rows,_columns);
  }
  
  /**
   * @author Ying-Huan.Chu
   * @author Siew Yeng - added parameter imageSetName
   */
  private void virtualLiveImageSetTableSelectionChanged(String imageSetName)
  {
    if(_selectImageSetModel.getRowCount() < 1)
    {
      _deleteImageSetButton.setEnabled(false);
      return;
    }
    if(_selectImageSetTable.getSelectedRow() < 0)
    {
      _selectedIndex = 0;
    }
    else
    {
      _selectedIndex = _selectImageSetTable.getSelectedRow();
    }
    
    String imageSetDataFilePath = FileName.getVirtualLiveImageSetDataXMLFile(_projectName,imageSetName);
    String imageSetDir = Directory.getVirtualLiveImageSetPath(_projectName,imageSetName);
    VirtualLiveImageSetDataXMLReader virtualliveImageSetDataXMLReader = new VirtualLiveImageSetDataXMLReader(imageSetDataFilePath);
    if (FileUtilAxi.exists(imageSetDataFilePath) && isVirtualLiveImageGenerationDataValid(virtualliveImageSetDataXMLReader, imageSetDir))
    {
      setVirtualLiveImageGenerationData(virtualliveImageSetDataXMLReader, imageSetDir);
    }
    else
    {
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                        StringUtil.format(StringLocalizer.keyToString("VIRTUAL_LIVE_IMAGESET_INCOMPLETE_KEY"), 50),
                                        StringLocalizer.keyToString("VP_RECONSTRUCTED_IMAGE_KEY"),
                                        JOptionPane.WARNING_MESSAGE);
      // Ying-Huan.Chu
      if (_previousSelectedIndex > -1)
      {
        _selectImageSetTable.addRowSelectionInterval(_previousSelectedIndex, _previousSelectedIndex);
      }
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private boolean isVirtualLiveImageGenerationDataValid(VirtualLiveImageSetDataXMLReader virtualliveImageSetDataXMLReader, String imageSetDir)
  {
    if (FileUtilAxi.existsDirectory(imageSetDir))
    {
      java.util.List<String> imageNameList = virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.IMAGE_NAME);
      java.util.List<String> imageNamesOnDisk = new ArrayList<>(FileUtilAxi.listAllFilesInDirectory(imageSetDir));

      if((imageNameList.isEmpty()) || (imageNameList.size() != (imageNamesOnDisk.size() - 1)))
      {
        return false;
      }
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * @author sham
   */
  private void createSortTable()
  {
    //Siew Yeng - XCR-3284 - remove all mouse listener and list selection listener
    //                     - graphic engine update will be based on observable
    _selectImageSetTable = new JSortTable(_selectImageSetModel);
    _selectImageSetTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _selectImageSetTable.setRowSelectionAllowed(true);
    _selectImageSetTable.setColumnSelectionAllowed(false);
    
    setupTableRendererAndEditor();
    
    _selectImageSetPane = new JScrollPane(_selectImageSetTable);
    _selectImageSetPane.setPreferredSize(new Dimension(500,500));
  }
  
   /**
   * @author Siew Yeng
   */
  private void setupTableRendererAndEditor()
  {
    //renderer for radio button at 1st column
    TableColumn test = _selectImageSetTable.getColumnModel().getColumn(0);
    test.setCellEditor(new RadioButtonCellEditor(_selectImageSetTable.getBackground(),_selectImageSetTable.getForeground()));
    test.setCellRenderer(new RadioButtonCellRenderer());
  }
  
  /**
   * @author sham
   */
  private void createVirtualLiveSelectImageSetPanel()
  {
    _deleteImageSetButton = new JButton(StringLocalizer.keyToString("IMD_DELETE_IMAGE_SET_KEY"));
    _deleteImageSetButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        _offlineDisplayMode = false;
        handleDeleteActionPerformed();
      }
    });
    _buttonPanel = new JPanel();
    _buttonPanel.add(_deleteImageSetButton);
    setLayout(new BorderLayout());
    setSize(new Dimension(800, 600));
    
    add(_selectImageSetPane,BorderLayout.CENTER);
    add(_buttonPanel,BorderLayout.PAGE_END);
    //Lim, Lay Ngor - set etched border to optimise the ImageSet Border Size on display.
    Border etchedBorder = BorderFactory.createEtchedBorder();
    this.setBorder(BorderFactory.createTitledBorder(etchedBorder, StringLocalizer.keyToString("SELECT_IMAGE_SET_PANEL_NAME_KEY")));
    updateUI();
  }

  /**
   * @author sham
   */
  private void handleDeleteActionPerformed()
  {
    ArrayList<Vector> deletedRows = new ArrayList<Vector>();
    Vector data;
    for (int i = 0; i < _totalRows; i++)
    {
      //Siew Yeng - XCR-3284
      if(_selectImageSetTable.getValueAt(i,3).equals(new Boolean(true)))
      {
        data = new Vector(_columns.length);
        data.addElement(_selectImageSetTable.getValueAt(i,0));
        data.addElement(_selectImageSetTable.getValueAt(i,1).toString());
        data.addElement(_selectImageSetTable.getValueAt(i,2).toString());
        data.addElement(_selectImageSetTable.getValueAt(i,3));
        deletedRows.add(data);
      }
    }
    if(deletedRows.size() > 0)
    {
      String message = StringLocalizer.keyToString("IMD_CONFIRM_IMAGE_SET_DELETION_KEY");
      int response = JOptionPane.showConfirmDialog(this,
              message,
              StringLocalizer.keyToString("SELECT_IMAGE_SET_PANEL_NAME_KEY"),
              JOptionPane.YES_NO_OPTION);
      if(response == JOptionPane.YES_OPTION)
      {
        for (Vector deletedRow : deletedRows)
        {
          try
          {
            FileUtil.delete(Directory.getVirtualLiveImageSetPath(_projectName,deletedRow.elementAt(1).toString()));
          }
          catch (CouldNotDeleteFileException ex)
          {
            //
          }
        }
        _totalRows = _totalRows - deletedRows.size();
        _selectImageSetModel.deleteRows(deletedRows);
        setupTableRendererAndEditor();
        revalidate();
        repaint();
      }

    }
    else
    {
      JOptionPane.showMessageDialog(null,StringLocalizer.keyToString("VP_DELETE_EMPTY_IMAGE_SET_MSG_KEY"));
    }
  }

  /**
   * @author sham
   */
  public int getNumberOfRows()
  {
    return _totalRows;
  }

  /**
   * @author sham
   */
  public void addVirtualLiveImageSetRow(String virtualLiveImageSetName)
  {
    _totalRows++;
    _selectImageSetModel.addRow(virtualLiveImageSetName);
//    _selectImageSetTable.setModel(_selectImageSetModel);
//    _selectImageSetTable.updateUI();
    //Siew Yeng - XCR-3284 - select the new image set generated
    revalidate();
    repaint();
    
    _selectImageSetTable.addRowSelectionInterval(_totalRows-1, _totalRows-1);
    SwingUtils.scrollTableToShowSelection(_selectImageSetTable);
  }

  /**
   * @author sham
   */
  public void setOfflineDisplaytMode(boolean offlineDisplayMode)
  {
    _offlineDisplayMode = offlineDisplayMode;
  }

  /**
   * @author sham
   */
  public boolean isOfflineDisplayMode()
  {
    return _offlineDisplayMode;
  }

  /**
   * @author sham
   */
  public void unpopulate()
  {
    //Siew Yeng - XCR-3284
    if(_selectImageSetTable != null)
      _selectImageSetTable.clearSelection();
    
    if(_selectImageSetModel != null)
    {
      _selectImageSetModel.clear();
      _selectImageSetModel = null;
    }
    removeAll();
    _selectedIndex=-1;
    _previousSelectedIndex=-1;
    GuiObservable.getInstance().deleteObserver(this);
  }

  /**
   * @author sham
   */
  private static void setVirtualLiveImageGenerationData(VirtualLiveImageSetDataXMLReader virtualliveImageSetDataXMLReader,String imageSetDir)
  {
    Assert.expect(virtualliveImageSetDataXMLReader != null);
    Assert.expect(imageSetDir != null);
    
    int xCenter;
    int yCenter;
    int width;
    int height;
    int enlargeUncertaintyInNanoMeter;
    int zSizeInNanoMeter;
    int integrationLevel;
    String componentName;
    int bypassAlignment;
    String magnificationKey;
    int minZHeightInNanoMeters;
    int maxZHeightInNanoMeters;
    int userGain;

    if(virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.X_CENTER).size() > 0)
    {
      xCenter = Integer.parseInt((virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.X_CENTER)).get(0));
    }
    else
    {
      xCenter = 0;
    }
    if (virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.Y_CENTER).size() > 0)
    {
      yCenter = Integer.parseInt((virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.Y_CENTER)).get(0));
    }
    else
    {
      yCenter = 0;
    }
    if (virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.WIDTH).size() > 0)
    {
      width = Integer.parseInt((virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.WIDTH)).get(0));
    }
    else
    {
      width = 0;
    }
    if (virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.HEIGHT).size() > 0)
    {
      height = Integer.parseInt((virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.HEIGHT)).get(0));
    }
    else
    {
      height = 0;
    }
    if (virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.ENLARGE_UNCERTAINTY).size() > 0)
    {
      enlargeUncertaintyInNanoMeter = Integer.parseInt((virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.ENLARGE_UNCERTAINTY)).get(0));
    }
    else
    {
      enlargeUncertaintyInNanoMeter = 0;
    }
    if (virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.Z_SIZE).size() > 0)
    {
      zSizeInNanoMeter = Integer.parseInt((virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.Z_SIZE)).get(0));
    }
    else
    {
      zSizeInNanoMeter = 0;
    }
    if (virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.INTEGRATION_LEVEL).size() > 0)
    {
      integrationLevel = Integer.parseInt((virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.INTEGRATION_LEVEL)).get(0));
    }
    else
    {
      integrationLevel = 1;
    }
    if (virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.COMPONENT_NAME).size() > 0)
    {
      componentName = (virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.COMPONENT_NAME)).get(0);
    }
    else
    {
      componentName = " ";
    }
    if (virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.BYPASS_ALIGNMENT).size() > 0)
    {
      bypassAlignment = Integer.parseInt((virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.BYPASS_ALIGNMENT)).get(0));
    }
    else
    {
      bypassAlignment = 0;
    }
    if (virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.MAGNIFICATION_KEY).size() > 0)
    {
      magnificationKey = (virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.MAGNIFICATION_KEY)).get(0);
    }
    else
    {
      magnificationKey = "0_LOW";
    }
    if (virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.MIN_Z_HEIGHT_KEY).size() > 0)
    {
      minZHeightInNanoMeters = Integer.parseInt((virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.MIN_Z_HEIGHT_KEY)).get(0));
    }
    else
    {
      minZHeightInNanoMeters = 0;
    }
    if (virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.MAX_Z_HEIGHT_KEY).size() > 0)
    {
      maxZHeightInNanoMeters = Integer.parseInt((virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.MAX_Z_HEIGHT_KEY)).get(0));
    }
    else
    {
      maxZHeightInNanoMeters = 0;
    }
    if (virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.GAIN_KEY).size() > 0)
    {
      userGain = Integer.parseInt((virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.GAIN_KEY)).get(0));
    }
    else
    {
      userGain = 1;
    }
    VirtualLiveImageGenerationSettings.getInstance().disable();
    VirtualLiveImageGenerationSettings.getInstance().setCenterXInNanoMeter(xCenter);
    VirtualLiveImageGenerationSettings.getInstance().setCenterYInNanoMeter(yCenter);
    VirtualLiveImageGenerationSettings.getInstance().setWidthInNanoMeter(width);
    VirtualLiveImageGenerationSettings.getInstance().setHeightInNanoMeter(height);
    VirtualLiveImageGenerationSettings.getInstance().setEnlargeUncertaintyInNanoMeter(enlargeUncertaintyInNanoMeter);
    VirtualLiveImageGenerationSettings.getInstance().setZStepSizeInNanoMeter(zSizeInNanoMeter);
    VirtualLiveImageGenerationSettings.getInstance().setIntegrationLevel(integrationLevel);
    VirtualLiveImageGenerationSettings.getInstance().setComponentRefDes(componentName);
    VirtualLiveImageGenerationSettings.getInstance().setBypassAlignment(bypassAlignment);
    VirtualLiveImageGenerationSettings.getInstance().setMagnificationKey(magnificationKey);
    VirtualLiveImageGenerationSettings.getInstance().setMinZHeightInNanoMeter(minZHeightInNanoMeters);
    VirtualLiveImageGenerationSettings.getInstance().setMaxZHeightInNanoMeter(maxZHeightInNanoMeters);
    VirtualLiveImageGenerationSettings.getInstance().setUserGain(userGain);
    VirtualLiveImageGenerationSettings.getInstance().enable();
    //update cad panel and virtualLive info panel
    VirtualLiveImageGenerationSettings.getInstance().setObservable();

    java.util.List<String> imageNameList = virtualliveImageSetDataXMLReader.getData(VirtualLiveImageSetDataXMLWriter.IMAGE_NAME);
    
    //update virtualLiveImage panel
    VirtualLiveReconstructedImagesData.getInstance().setOfflineReconstructedImagesData(imageNameList, virtualliveImageSetDataXMLReader.getCurrentXMLVersion(), imageSetDir, 0);
    _previousSelectedIndex = _selectedIndex;
  }
  
  /**
   * Checks if a row is selected in the Virtual Live Image Set Table.
   * @author Ying-Huan.Chu
   */
  public boolean isRowSelectedInSelectImageSetTable()
  {
    if (_selectImageSetTable != null)
    {
      if (_selectImageSetTable.getSelectedRow() > -1)
      {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Clear Virtual Live Image Set Table selection.
   * @author Ying-Huan.Chu
   */
  public void clearVirtualLiveImageSetTableSelection()
  {
    if (_selectImageSetTable != null)
      _selectImageSetTable.clearSelection();
  }

  /**
   * @author Siew Yeng
   */
  public void update(Observable observable, Object object) 
  {
    if (observable instanceof GuiObservable)
    {
      GuiEventEnum imageSetEnum = ((GuiEvent)object).getGuiEventEnum();
      if (imageSetEnum instanceof ImageSetEventEnum)
      {
        if(imageSetEnum.equals(ImageSetEventEnum.VIRTUAL_LIVE_IMAGE_SET_SELECTED))
        {
          GuiEvent guiEvent = (GuiEvent)object;
          virtualLiveImageSetTableSelectionChanged((String)guiEvent.getSource());
        }
      }
    }
  }
}
