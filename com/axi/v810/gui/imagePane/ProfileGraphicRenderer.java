package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This class draws a Region of Interest
 *
 * @author George A. David
 */
public class ProfileGraphicRenderer extends ShapeRenderer
{
  private java.awt.Shape _shape;

  /**
   * @author George A. David
   */
  ProfileGraphicRenderer(java.awt.Shape shape)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(shape != null);

    _shape = shape;
    refreshData();
  }

  /**
   * @author George A. David
   */
  public String toString()
  {
    return "profile graphic";
  }

  /**
   * @author George A. David
   */
  protected void refreshData()
  {
    Assert.expect(_shape != null);

    initializeShape(_shape);
  }
}
