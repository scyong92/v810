package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.geom.*;
import java.awt.image.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.MagnificationTypeEnum;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * This class draws an verification image
 * @author George A. David
 */
public class VerificationImageRenderer extends BufferedImageRenderer
{
  private ImageManager _imageManager = ImageManager.getInstance();
  private ReconstructionRegion _verificationRegion;
  private Point2D _pixelCoordinates;

  /**
   * @author George A. David
   */
  public VerificationImageRenderer(String projectName, ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(projectName != null);

    _verificationRegion = reconstructionRegion;
    try
    {
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      if (reconstructionRegion.getTestSubProgram().getTestProgram().isManual2DAlignmentInProgress())
        _bufferedImage = _imageManager.load2DVerificationImage(projectName, _verificationRegion);
      else
        _bufferedImage = _imageManager.loadVerificationImage(projectName, _verificationRegion);
    }
    catch(DatastoreException dex)
    {
      displayAlternateImage();
    }
    if( _verificationRegion.getTestSubProgram().getMagnificationType().equals(MagnificationTypeEnum.LOW) )
    {
      _pixelCoordinates = new Point2D.Double(_verificationRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinX() / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel(),
                                           _verificationRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinY() / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
    }
    else
    {
      _pixelCoordinates = new Point2D.Double(_verificationRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinX() / (double)MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel(),
                                           _verificationRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinY() / (double)MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel());
    }
    refreshData();
  }

  /**
   * @author George A. David
   */
  protected void displayAlternateImage()
  {
    // ok, we couldn't load the image
    // create an image with an error
    ImageRectangle rect = _verificationRegion.getRegionRectangleInPixels();
    _bufferedImage = new BufferedImage(rect.getWidth(), rect.getHeight(), BufferedImage.TYPE_INT_RGB);
    Graphics2D graphics = _bufferedImage.createGraphics();
    graphics.setColor(Color.black);
    graphics.fillRect(0, 0, rect.getWidth(), rect.getHeight());
    graphics.setColor(Color.white);
    graphics.setFont(FontUtil.getRendererFont(20));
    float border = rect.getWidth() * 0.10f;
    graphics.drawString(StringLocalizer.keyToString("VISGUI_ERROR_LOADING_IMAGE_KEY"), border, rect.getHeight() / 2.0f);
  }

  /**
   * @author George A. David
   */
  protected void refreshData()
  {
    initializeImageWithTranslation(_bufferedImage, _pixelCoordinates.getX(), _pixelCoordinates.getY());
  }
}
