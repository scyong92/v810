package com.axi.v810.gui.imagePane;

import java.awt.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.Assert;

/**
 * This class is the main panel for the image pane display during fine tuning.
 * @author George A. David
 */
public class FineTuningProfileGraphicPanel extends JPanel
{
  private FineTuningProfileGraphicsEngineSetup _fineTuningProfileGraphicsEngineSetup = null;
  JScrollPane _fineTuningProfileScrollPane;
  private GraphicsEngine _graphicsEngine = null;

  /**
   * @author George A. David
   */
  public FineTuningProfileGraphicPanel()
  {
    _fineTuningProfileGraphicsEngineSetup = new FineTuningProfileGraphicsEngineSetup();
    _graphicsEngine = _fineTuningProfileGraphicsEngineSetup.getGraphicsEngine();
    jbInit();
  }
  /**
   * @author Anthony Fong //Anthony01005
   */
  public FineTuningProfileGraphicsEngineSetup getFineTuningProfileGraphicsEngineSetup()
  {
    Assert.expect(_fineTuningProfileGraphicsEngineSetup != null);
    return _fineTuningProfileGraphicsEngineSetup;
  }
  /**
   * @author George A. David
   */
  private void jbInit()
  {
    // panel preferred size set in FineTuningPanel
    setLayout(new BorderLayout());
    setBackground(Color.lightGray);

    //Anthony01005
    //_graphicsEngine.setPreferredSize(new Dimension(0, 50));
    _graphicsEngine.setPreferredSize(this.getSize());

    _fineTuningProfileScrollPane = new JScrollPane();
    _fineTuningProfileScrollPane.getViewport().add(_graphicsEngine);
    add(_fineTuningProfileScrollPane, BorderLayout.CENTER);

    _fineTuningProfileGraphicsEngineSetup.setHorizontalScrollBar(_fineTuningProfileScrollPane.getHorizontalScrollBar());
    //Anthony01005
    _fineTuningProfileGraphicsEngineSetup.setGraphicsEngineParent(_fineTuningProfileScrollPane);
    //_fineTuningProfileGraphicsEngineSetup._graphicsEngineParent=_fineTuningProfileScrollPane;
  }

  /**
   * @author Andy Mechtenberg
   */
  void display()
  {
    Assert.expect(_fineTuningProfileGraphicsEngineSetup != null);

    //Anthony01005
    _fineTuningProfileGraphicsEngineSetup.getGraphicsEngine().setSize(this.getSize());

    _fineTuningProfileGraphicsEngineSetup.display();
  }

  /**
   * @author Andy Mechtenberg
   */
  void finish()
  {
    Assert.expect(_fineTuningProfileGraphicsEngineSetup != null);
    _fineTuningProfileGraphicsEngineSetup.finish();
  }


  /**
   * @author Andy Mechtenberg
   */
  void unpopulate()
  {
    _fineTuningProfileGraphicsEngineSetup.unpopulate();
  }

  /**
   * force scroll bar to show leftmost (newest) profile
   * @author George Booth
   */
  void scrollToNewProfile()
  {
    _fineTuningProfileScrollPane.getHorizontalScrollBar().setValue(0);
  }
}
