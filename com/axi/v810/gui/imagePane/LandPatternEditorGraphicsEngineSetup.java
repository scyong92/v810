package com.axi.v810.gui.imagePane;


import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.cadcreator.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelDesc.LandPatternPadEventEnum;
import com.axi.v810.business.panelDesc.ThroughHoleLandPatternPadEventEnum;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.cadCreator.LandPatternAreaRenderer;
import com.axi.v810.gui.drawCad.CreateNewCadPanel;
import com.axi.v810.gui.drawCad.LandPatternPadRenderer;
import com.axi.v810.hardware.*;
import com.axi.v810.util.StringLocalizer;
import java.awt.Shape;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import javax.imageio.ImageIO;
import javax.swing.*;

/*
 * The LandPatternEditorGraphicsEngineSetup class sets up the graphics engine
 * for image display during fine tuning.
 * @author Chin-Seong, Kee
 */
public class LandPatternEditorGraphicsEngineSetup implements Observer, ImagePanelGraphicsEngineSetupInt
{
  private static final int _BORDER_SIZE_IN_PIXELS = 50;
  private static final double _ZOOM_FACTOR = 1.2;

  private GraphicsEngine _graphicsEngine;
  private ImageManagerToolBar _toolBar;

  private boolean _isImageStayPersistent;

  // Graphic engine layers
  private int _landPatternRendererLayer;
  private int _croppedImageRendererLayer;
  private int _croppedImageOutlierLayer;
  private int _textLayer;
  private int _draggedLandPatternLayer;

  private java.util.List<Integer> _diagnosticLayers = new LinkedList<Integer>();
  private java.util.List<Integer> _allLayers = new LinkedList<Integer>();

  private SortedMap<MeasurementRegionEnum, Integer> _CroppedImageToGraphicsLayerNumberMap = new TreeMap<MeasurementRegionEnum, Integer>();
  private InspectionEventObservable _inspectionObservable = null;
  private GuiObservable _guiObservable = null;

  private boolean _isGraphicsEngineReady = false;
  
  com.axi.guiUtil.Renderer _render;

  private String _currentJointName = "";
  
  private boolean _isGraphicsShouldBeFitToScreen = false;
  private InspectionImageRenderer _inspectionImageRenderer = null;
  private LandPattern _currentLandPattern = null;
  private LandPatternAreaRenderer _currentDraggedAreaRenderer = null;
  
  private Map<LandPatternPad, LandPatternPadRenderer> _landPatternPadToLandPatternPadRendererMap = new HashMap<LandPatternPad, LandPatternPadRenderer>();
  private CreateNewCadPanel _createNewCadPanel = null;
  
  private CroppedLandPatternImageRenderer _croppedLandPatternRenderer = null;
  
  /**
   * @author Chin-Seong, Kee
   */
  public LandPatternEditorGraphicsEngineSetup(CreateNewCadPanel createNewCadPanel, GraphicsEngine graphicsEngine)
  {
    //Assert.expect(toolBar != null);

    //_toolBar = toolBar;
    _createNewCadPanel = createNewCadPanel;
    _graphicsEngine = graphicsEngine;
    _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
    _graphicsEngine.setShouldGraphicsBeCentered(true);
    //_graphicsEngine.reset();
    _graphicsEngine.setAxisInterpretation(true, false);
    _graphicsEngine.setPixelBorderSize(_BORDER_SIZE_IN_PIXELS);
    //_graphicsEngine.setShouldGraphicsBeFitToScreen(false);
 //   _graphicsEngine.setMouseEventsEnabled(true);
//    _graphicsEngine.fitGraphicsToScreen();
    _graphicsEngine.mark();
    _graphicsEngine.invalidate();

    _inspectionObservable = InspectionEventObservable.getInstance();
    _guiObservable = GuiObservable.getInstance();
    
    createLayers();
    setVisibility();
    setColors();
  }
  
  /**
   * @author Chin-Seong, Kee
   */
  private void setVisibility()
  {
    _graphicsEngine.setVisible(_landPatternRendererLayer, true);
    _graphicsEngine.setVisible(_croppedImageRendererLayer, true);
    _graphicsEngine.setVisible(_textLayer, true);
    _graphicsEngine.setVisible(_draggedLandPatternLayer, true);
    _graphicsEngine.setVisible(_croppedImageOutlierLayer, true);
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void setUpToolBar()
  {
    _toolBar.setAllowDragGraphics(true);
    _toolBar.setAllowMeasurements(_isGraphicsEngineReady);
    _toolBar.setAllowResetGraphics(_isGraphicsEngineReady);
    _toolBar.setAllowToggleGraphics(_isGraphicsEngineReady);
    _toolBar.setAllowToggleImageStay(_isGraphicsEngineReady);
    //Anthony01005
    _toolBar.setAllowToggleImageFitToScreen(_isGraphicsEngineReady);

    _toolBar.setAllowViewBottomSide(false);
    _toolBar.setAllowViewTopSide(false);

    _toolBar.setAllowZoomIn(_isGraphicsEngineReady);
    _toolBar.setAllowZoomOut(_isGraphicsEngineReady);
    _toolBar.setAllowZoomRectangle(_isGraphicsEngineReady);
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void setToolBarEnabled(boolean enabled)
  {
    _toolBar.setAllowDragGraphics(enabled);
    _toolBar.setAllowMeasurements(enabled);
    _toolBar.setAllowToggleGraphics(enabled);
    _toolBar.setAllowToggleImageStay(enabled);
    //Anthony01005
    _toolBar.setAllowToggleImageFitToScreen(enabled);
    _toolBar.setAllowResetGraphics(enabled);

    _toolBar.setAllowZoomIn(enabled);
    _toolBar.setAllowZoomOut(enabled);
    _toolBar.setAllowZoomRectangle(enabled);
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void clearRenderers()
  {
    _graphicsEngine.removeRenderers(_allLayers);
    setColors();
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void display()
  {
    setUpToolBar();
    MathUtilEnum displayUnits = Project.getCurrentlyLoadedProject().getDisplayUnits();
    if(displayUnits.equals(MathUtilEnum.NANOMETERS) == false)
      _graphicsEngine.setMeasurementInfo(MathUtil.convertUnits(MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel(), MathUtilEnum.NANOMETERS, displayUnits),
                                         MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits), MeasurementUnits.getMeasurementUnitString(displayUnits));
    _graphicsEngine.setAxisInterpretation(true, false);
    _graphicsEngine.setMouseEventsEnabled(true);
  }

  /*
   * @author Kee Chin Seong
   */
  public void displayCroppedArea(com.axi.guiUtil.Renderer imgRenderer, BufferedImage img)
  {
    if(imgRenderer != null && img != null || _croppedLandPatternRenderer != null)
    {
        _render = imgRenderer;
        
        Shape shape = new Rectangle2D.Double(0, 0, (int)imgRenderer.getBounds().getWidth(), (int)imgRenderer.getBounds().getHeight());

        if(_croppedLandPatternRenderer == null)
           _croppedLandPatternRenderer = new CroppedLandPatternImageRenderer(imgRenderer, img);
        
        LandPatternAreaRenderer test = new LandPatternAreaRenderer(shape, 0, true);
        LandPatternDraggedShapeSettings.getInstance().setCroppedShape(shape);
        
        _graphicsEngine.addRenderer(_croppedImageOutlierLayer, _croppedLandPatternRenderer);
        
        refreshGraphicsEngine();
    }
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void unpopulate()
  {
    _graphicsEngine.removeRenderers(_allLayers);
    setColors();
    finish();
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void finish()
  {
    //setToolBarEnabled(false);
    _inspectionObservable.deleteObserver(this);
    _guiObservable.deleteObserver(this);
    ProjectObservable.getInstance().deleteObserver(this);
    _graphicsEngine.getSelectedRendererObservable().deleteObserver(this);
    _graphicsEngine.getSelectedOpticalRegionRendererObservable().deleteObserver(this);
    _graphicsEngine.getDragOpticalRegionObservable().deleteObserver(this);
    _graphicsEngine.getUnselectedOpticalRegionRendererObservable().deleteObserver(this);   
    _graphicsEngine.getUnselectedDragOpticalRegionRendererObservable().deleteObserver(this);
    _graphicsEngine.getDisplayPadNameObservable().deleteObserver(this);
    _graphicsEngine.getSelectedAreaObservable().deleteObserver(this);
    LandPatternDraggedShapeSettings.getInstance().deleteObserver(this);
    
    _croppedLandPatternRenderer = null;
  }
  
  
  /*
   * @author Kee Chin Seong
   */
  public void setRendererShape(java.awt.Shape shape)
  {
    LandPatternAreaRenderer test = new LandPatternAreaRenderer(shape, 0, true);
    LandPatternDraggedShapeSettings.getInstance().setCroppedShape(shape);
    
    _graphicsEngine.addRenderer(_croppedImageRendererLayer, test);
    refreshGraphicsEngine();
  }

  /**
   * @author Chin-Seong, Kee
   */
  public GraphicsEngine getGraphicsEngine()
  {
    Assert.expect(_graphicsEngine != null);

    return _graphicsEngine;
  }


  /*
   * @author Kee Chin Seong
   */
  public String getCurrentJointName()
  {
    Assert.expect(_currentJointName.equals("") == false);
    return _currentJointName;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public SelectedRendererObservable getSelectedRendererObservable()
  {
     return _graphicsEngine.getSelectedRendererObservable(); 
  }
  
  /**
   * @author Chin-Seong, Kee
   */
  public void addObservers()
  {
    // add this as an observer of the project
    ProjectObservable.getInstance().addObserver(this);
    // add this as an observer of the gui
    _guiObservable.addObserver(this);
    // add this as an observer of the engine
    _graphicsEngine.getSelectedRendererObservable().addObserver(this);
    // add this as an observer of the psp selected region
    _graphicsEngine.getSelectedOpticalRegionRendererObservable().addObserver(this);
    // add this as an observer of the psp drag region
    _graphicsEngine.getDragOpticalRegionObservable().addObserver(this);
    // add this as an observer of the psp selected region
    _graphicsEngine.getUnselectedOpticalRegionRendererObservable().addObserver(this);
    
    _graphicsEngine.getUnselectedDragOpticalRegionRendererObservable().addObserver(this);
    
    _graphicsEngine.getDisplayPadNameObservable().addObserver(this);
    _graphicsEngine.getSelectedAreaObservable().addObserver(this);

    LandPatternDraggedShapeSettings.getInstance().addObserver(this);
    
    _inspectionObservable.addObserver(this);
    
    _guiObservable.addObserver(this);
  }
  
  
  /**
   * @author Chin-Seong, Kee
   */
  public void removeObservers()
  {
     // remove this as an observer of the project data
     ProjectObservable.getInstance().deleteObserver(this);
     // remove this as an observer of the gui
     _guiObservable.deleteObserver(this);
     // remove this as an observer of the engine
     _graphicsEngine.getSelectedRendererObservable().deleteObserver(this);
     // remove this as a observer of the psp selected region
     _graphicsEngine.getSelectedOpticalRegionRendererObservable().deleteObserver(this);
     // remove this as a observer of the psp drag region
     _graphicsEngine.getDragOpticalRegionObservable().deleteObserver(this);
     // remove this as a observer of the psp selected region
     _graphicsEngine.getUnselectedOpticalRegionRendererObservable().deleteObserver(this);
     
     _graphicsEngine.getUnselectedDragOpticalRegionRendererObservable().deleteObserver(this);
     
     _graphicsEngine.getDisplayPadNameObservable().deleteObserver(this);    
     _graphicsEngine.getSelectedAreaObservable().deleteObserver(this);
     
     LandPatternDraggedShapeSettings.getInstance().deleteObserver(this);
  }
  
  /**
   * @author Chin-Seong, Kee
   */
  public void drawLandPatternAreaDisplay()
  {
    Assert.expect(_graphicsEngine != null);

    LandPatternDraggedShapeSettings.getInstance().setObservable();
  }
  
  
  /**
   * @author Chin-Seong, Kee
   */
  private void handleSelectedAreaObservable(Shape virtualLiveSelectedArea)
  {
    Rectangle rect = virtualLiveSelectedArea.getBounds();
    if (rect.getMinX() < 0 || rect.getMinY() < 0)
      return;
   
    LandPatternDraggedShapeSettings.getInstance().setLandPatternSelectedArea(virtualLiveSelectedArea);
    drawLandPatternAreaDisplay();
  }
  
  /**
   * Use to draw shape that actual used for virtualLive generation
   *
   * @author chin-seong.kee
   */
  private void handleLandPatternDraggedAreaObservable(final Object argument)
  {
    Assert.expect(argument != null);

    _graphicsEngine.clearCrossHairs();
    java.awt.Shape shape = (java.awt.Shape) argument;
    if(shape != null)
    {
      if(shape.getBounds2D().getWidth() != 0 && shape.getBounds2D().getHeight() != 0)
      {
        _graphicsEngine.removeRenderers(_draggedLandPatternLayer);
        LandPatternAreaRenderer draggedAreaRenderer = new LandPatternAreaRenderer(shape, 0, false);
        _graphicsEngine.addRenderer(_draggedLandPatternLayer, draggedAreaRenderer);
        _currentDraggedAreaRenderer = draggedAreaRenderer;
        //refreshGraphicsEngine();
        //_graphicsEngine.fitRectangleInRendererCoordinatesToScreen(draggedAreaRenderer.getBounds().getBounds2D());
      }
    }
    
    //_graphicsEngine.setAxisInterpretation(true, true);
    //_graphicsEngine.center();
    //_graphicsEngine.repaint();
  }


  /**
   * @author Chin-Seong, Kee
   */
  private void handleUpdate(final Observable observable, final Object argument)
  {
     if(observable instanceof ProjectObservable)
     {
        if(argument instanceof ProjectChangeEvent)
        {
            ProjectChangeEvent event = (ProjectChangeEvent)argument;
            ProjectChangeEventEnum eventEnum = ((ProjectChangeEvent)argument).getProjectChangeEventEnum();
            if (eventEnum instanceof LandPatternPadEventEnum || eventEnum instanceof ThroughHoleLandPatternPadEventEnum)
            {
                if (eventEnum.equals(LandPatternPadEventEnum.CREATE_OR_DESTROY) || eventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE))
                {
                  Object oldValue = event.getOldValue();
                  Object newValue = event.getNewValue();
                  if (newValue != null) // create case
                  {
//                  System.out.println("############ Got a LandPatternPadEventEnum.CREATE event");
                    Assert.expect(oldValue == null);
                    Assert.expect(newValue instanceof LandPatternPad);
                    LandPatternPad landPatternPad = (LandPatternPad)newValue;
                    LandPatternPadRenderer landPatternPadRend = new LandPatternPadRenderer(landPatternPad);
                    LandPatternPadRenderer prev = _landPatternPadToLandPatternPadRendererMap.put(landPatternPad, landPatternPadRend);
                    // another renderer for this LPP has been previously associated with it,
                    // so we need to remove it from the graphics engine
                    if (prev != null)
                      _graphicsEngine.removeRenderer(prev);
                    _currentLandPattern = landPatternPad.getLandPattern();
                    _graphicsEngine.addRenderer(_landPatternRendererLayer, landPatternPadRend);
                    _graphicsEngine.validateData(_landPatternRendererLayer);
                    //_graphicsEngine.removeRenderers(_croppedImageRendererLayer);
                    
                    refreshGraphicsEngine();
                  }
                  else if (oldValue != null) // destroy case
                  {
                    Assert.expect(newValue == null);
                    Assert.expect(oldValue instanceof LandPatternPad);
                    LandPatternPad landPatternPad = (LandPatternPad)oldValue;
                    LandPatternPadRenderer prev = _landPatternPadToLandPatternPadRendererMap.remove(landPatternPad);
                    if(prev != null)
                    {
                      com.axi.guiUtil.Renderer renderer = getRendererForLandPatternPad(landPatternPad);
                      
                      Assert.expect(renderer == prev);
                      _graphicsEngine.removeRenderer(renderer); // this also repaints the deleted renderer 
                      
                      refreshGraphicsEngine();
                    }
                 }
                  else
                  {
                    Assert.expect(false, "Both oldValue and newValue are null for LandPatternPadEvenEnum.CREATE_OR_DESTROY event");
                    return;
                  }
               }
                else if (eventEnum.equals(LandPatternPadEventEnum.PAD_ONE) == false &&
                         eventEnum.equals(LandPatternPadEventEnum.PITCH) == false &&
                         eventEnum.equals(LandPatternPadEventEnum.INTERPAD_DISTANCE) == false &&
                         eventEnum.equals(LandPatternPadEventEnum.PAD_ORIENTATION) == false && 
                         eventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE_LIST) == false)
                {
                  LandPatternPad landPatternPad = (LandPatternPad)event.getSource();

                  com.axi.guiUtil.Renderer landPatternPadRenderer = getRendererForLandPatternPad(landPatternPad);
                  landPatternPadRenderer.validateData();
                  if (eventEnum.equals(LandPatternPadEventEnum.NAME))
                  {
                    _createNewCadPanel.setDescription(getDescription((LandPatternPadRenderer)landPatternPadRenderer));
                  }
                  refreshGraphicsEngine();
                }
                else if (eventEnum.equals(LandPatternPadEventEnum.PAD_ONE))
                {
                   LandPatternPad landPatternPad = (LandPatternPad)event.getSource();
                   for (LandPatternPad lp : landPatternPad.getLandPattern().getLandPatternPads())
                   {
                     com.axi.guiUtil.Renderer landPatternPadRenderer = getRendererForLandPatternPad(lp);
                     landPatternPadRenderer.validateData();
                   }
                }
                else if(eventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE_LIST))
                {
                  Object oldValue = event.getOldValue();
                  Object newValue = event.getNewValue();
                  
                  if (newValue != null) // create case
                  {;
                    Assert.expect(oldValue == null);
                    Assert.expect(newValue instanceof ArrayList );
                    java.util.List <LandPatternPad> landPatternPads = (java.util.ArrayList <LandPatternPad>)newValue;
                    
                    for (LandPatternPad landPatternPad : landPatternPads)
                    {
                      LandPatternPadRenderer landPatternPadRend = new LandPatternPadRenderer(landPatternPad);
                      LandPatternPadRenderer prev = _landPatternPadToLandPatternPadRendererMap.put(landPatternPad, landPatternPadRend);
                      // another renderer for this LPP has been previously associated with it,
                      // so we need to remove it from the graphics engine
                      if (prev != null)
                        _graphicsEngine.removeRenderer(prev);
                      
                      _graphicsEngine.addRenderer(_landPatternRendererLayer, landPatternPadRend);
                      _graphicsEngine.removeRenderers(_croppedImageRendererLayer);
                      _graphicsEngine.removeRenderers(_croppedImageOutlierLayer);
                    }
                    _graphicsEngine.validateData(_landPatternRendererLayer);
                    refreshGraphicsEngine();
                  }
                  else if (oldValue != null) // destroy case
                  {
                    Assert.expect(newValue == null);
                    Assert.expect(oldValue instanceof ArrayList );
                    
                    java.util.List <LandPatternPad> landPatternPads = (java.util.ArrayList <LandPatternPad>)oldValue;

                    for (LandPatternPad landPatternPad : landPatternPads)
                    {
                      LandPatternPadRenderer prev = _landPatternPadToLandPatternPadRendererMap.remove(landPatternPad);
                      if (prev != null)
                      {
                        com.axi.guiUtil.Renderer renderer = getRendererForLandPatternPad(landPatternPad);
                        Assert.expect(renderer == prev);
                        _graphicsEngine.removeRenderer(renderer); // this also repaints the deleted renderer (or lack of)
                      }
                    }
                    refreshGraphicsEngine();
                 }
              }
              _graphicsEngine.removeRenderers(_draggedLandPatternLayer);
          }
       }
     }
     else if (observable instanceof SelectedRendererObservable)
     {
        boolean descriptionIsSet = false;
        _graphicsEngine.clearHighlightedRenderers();
        _graphicsEngine.clearSelectedRenderers();
        //_graphicsEngine.clear
        
        for (com.axi.guiUtil.Renderer renderer :
             ((Collection<com.axi.guiUtil.Renderer>)argument))
        {
            // if it's a PackagePin, highlight it
          if (renderer instanceof LandPatternPadRenderer)
          {
              _graphicsEngine.setSelectedRenderer(renderer);

              // if more than one land pattern pad was selected, we set the status bar descriptive text to nothing
              // so, we'll set it the first time, and if we get another, clear it out.
              if (descriptionIsSet)
              {
                _createNewCadPanel.setDescription(" ");  // if we use null, the entire status bar goes away, so we'll use an empty space instead
              }
              else
              {
                _createNewCadPanel.setDescription(getDescription((LandPatternPadRenderer)renderer));
                descriptionIsSet = true;
              }
          }
       }
       _graphicsEngine.repaint();
     }
     else if (observable instanceof LandPatternDraggedShapeSettings)
     {
       handleLandPatternDraggedAreaObservable(argument);
     }
     else if (observable instanceof SelectedAreaObservable)
     {
       if(argument instanceof Shape)
       {
         handleSelectedAreaObservable((Shape)argument);
       }
     }
     else if (observable instanceof GuiObservable)
     {
        handleGuiEvent((GuiEvent)argument);
     }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private com.axi.guiUtil.Renderer getRendererForLandPatternPad(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);
    Collection<com.axi.guiUtil.Renderer> renderers =
      _graphicsEngine.getRenderers(_landPatternRendererLayer);
    for (com.axi.guiUtil.Renderer renderer : renderers)
    {
      if (renderer instanceof LandPatternPadRenderer)
      {
        LandPatternPadRenderer tempPadRenderer = (LandPatternPadRenderer)renderer;
        if (landPatternPad == tempPadRenderer.getLandPatternPad())
        {
          return tempPadRenderer;
        }
      }
      else
        Assert.expect(false, "Unexpected renderer: " + renderer);
    }
    Assert.expect(false, "Could not find renderer for land pattern pad " + landPatternPad);
    return null;

  }
  
  
  /*
   * @author Kee Chin Seong
   */
  private void handleGuiEvent(GuiEvent guiEvent)
  {
    Assert.expect(guiEvent != null);

    GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
    if(guiEventEnum instanceof SelectionEventEnum)
    {
      SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
      if(selectionEventEnum.equals(SelectionEventEnum.LANDPATTERN_PAD))
      {
        _graphicsEngine.clearSelectedRenderers();
        /** Warning "unchecked cast" approved for cast from Object types */
        java.util.List<LandPatternPad> pads = (java.util.List<LandPatternPad>)guiEvent.getSource();
        java.util.List<com.axi.guiUtil.Renderer> renderers = getRenderers(pads);
        _graphicsEngine.setSelectedRenderers(renderers);
        if (renderers.size() == 1)
        {
          com.axi.guiUtil.Renderer renderer = renderers.get(0);
          Assert.expect(renderer instanceof LandPatternPadRenderer);
          _createNewCadPanel.setDescription(getDescription((LandPatternPadRenderer)renderer));
        }
        else // clear the status line for multiple selected pads
        {
          _createNewCadPanel.setDescription(" ");
        }
      }   
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private String getDescription(LandPatternPadRenderer renderer)
  {
    Assert.expect(renderer != null);

    LandPatternPad landPatternPad = ((LandPatternPadRenderer)renderer).getLandPatternPad();
    String description;
    if (landPatternPad instanceof ThroughHoleLandPatternPad)
    {
      ThroughHoleLandPatternPad thLandPatternPad = (ThroughHoleLandPatternPad)landPatternPad;
      MathUtilEnum displayUnits = thLandPatternPad.getLandPattern().getPanel().getProject().getDisplayUnits();
      description = StringLocalizer.keyToString(new LocalizedString("GUI_TH_LAND_PATTERN_PAD_SUMMARY_KEY",
                                                                    new Object[]
                                                                    {landPatternPad.getName(),
                                                                    MathUtil.convertUnits(thLandPatternPad.getHoleCoordinateInNanoMeters().getX(),
                                                                                          MathUtilEnum.NANOMETERS,
                                                                                          displayUnits),
                                                                    MathUtil.convertUnits(thLandPatternPad.getHoleCoordinateInNanoMeters().getY(),
                                                                                          MathUtilEnum.NANOMETERS,
                                                                                          displayUnits)}));
    }
    else
      description = StringLocalizer.keyToString(new LocalizedString("GUI_LAND_PATTERN_PAD_SUMMARY_KEY", new Object[]
                                                                    {landPatternPad.getName()}));
    return description;
  }
  
  /*
   * @author Kee Chin Seong
   */
  private java.util.List<com.axi.guiUtil.Renderer> getRenderers(java.util.List<LandPatternPad> landPatternPads)
  {
    Assert.expect(landPatternPads != null);

    java.util.List<com.axi.guiUtil.Renderer> renderers = new ArrayList<com.axi.guiUtil.Renderer>();
    for (LandPatternPad pad : landPatternPads)
    {
      com.axi.guiUtil.Renderer renderer = (com.axi.guiUtil.Renderer)_landPatternPadToLandPatternPadRendererMap.get(pad);
      if (renderer == null)
            System.out.println("null");
      Assert.expect(renderer != null);
      renderers.add(renderer);
    }

    return renderers;
  }

  /**
   * @author Chin-Seong, Kee
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        // will decrement the reference count of the inspection event.
         handleUpdate(observable, argument);
      }
    });
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void refreshGraphicsEngine()
  {
    //setColors();
    _graphicsEngine.setAxisInterpretation(true, false);
    _graphicsEngine.center();
    _graphicsEngine.repaint();
    _graphicsEngine.fitGraphicsToScreen();
  }

  /**
   * @author Chin-Seong, Kee
    _toolBar.setMeasureToggleButtonEnabled(true);
   */
  private void createLayers()
  {
    int layerNumber = JLayeredPane.DEFAULT_LAYER.intValue();

    _diagnosticLayers.clear();

    // the image itself is not a MeasurementRegion, so must be handled separately
    

    // messages for passing and failing components
    _croppedImageRendererLayer = layerNumber++;
    _croppedImageOutlierLayer = layerNumber++;
    _textLayer = layerNumber++;
    _draggedLandPatternLayer = layerNumber++;
    _landPatternRendererLayer = layerNumber++;

    Collection<Integer> layersToUseWhenFittingToScreen = new ArrayList<Integer>(2);
    layersToUseWhenFittingToScreen.add(_landPatternRendererLayer);
    layersToUseWhenFittingToScreen.add(_draggedLandPatternLayer);
    _graphicsEngine.setLayersToUseWhenFittingToScreen(layersToUseWhenFittingToScreen);
    
    _allLayers.add(_croppedImageRendererLayer);
    _allLayers.add(_textLayer);
    _allLayers.add(_croppedImageOutlierLayer);
    _allLayers.add(_draggedLandPatternLayer);
    _allLayers.add(_landPatternRendererLayer);
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void setColors()
  {
    _graphicsEngine.setForeground(_landPatternRendererLayer, LayerColorEnum.SURFACEMOUNT_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(_croppedImageRendererLayer, Color.lightGray);
    _graphicsEngine.setForeground(_croppedImageOutlierLayer, Color.green);
    _graphicsEngine.setForeground(_textLayer, Color.BLACK); //.PROFILE_TEXT.getColor());
    _graphicsEngine.setForeground(_draggedLandPatternLayer, Color.GREEN);
    
    for (MeasurementRegionEnum measurementRegionEnum : MeasurementRegionEnum.getSetOfAllEnums())
    {
      Integer graphicsLayerForMeasurementRegion = _CroppedImageToGraphicsLayerNumberMap.get(measurementRegionEnum);
      if (graphicsLayerForMeasurementRegion != null)
      {
        Color measurementRegionColor = XRayLayerColorEnum.getColorForMeasurementRegionEnum(measurementRegionEnum);
        _graphicsEngine.setForeground(graphicsLayerForMeasurementRegion.intValue(), measurementRegionColor);
      }
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void populateLayersWithRenderers()
  {
    Assert.expect(_currentLandPattern != null);
    _graphicsEngine.reset();
    _landPatternPadToLandPatternPadRendererMap.clear();
    for (LandPatternPad landPatternPad: _currentLandPattern.getLandPatternPads())
    {
      LandPatternPadRenderer landPatternPadRenderer = new LandPatternPadRenderer(landPatternPad);
      _graphicsEngine.addRenderer(_landPatternRendererLayer, landPatternPadRenderer);
      _landPatternPadToLandPatternPadRendererMap.put(landPatternPad, landPatternPadRenderer);
    }
    refreshGraphicsEngine();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void displayLandPattern(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);
    _currentLandPattern = landPattern;

    createLayers();
    setVisibility();
    setColors();
    
    populateLayersWithRenderers();
    
    _graphicsEngine.setAxisInterpretation(true, false);

    MathUtilEnum displayUnits = _currentLandPattern.getPanel().getProject().getDisplayUnits();
    double measureFactor = 1.0/MathUtil.convertUnits(1.0, displayUnits, MathUtilEnum.NANOMETERS);
    _graphicsEngine.setMeasurementInfo(measureFactor, MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits),
       MeasurementUnits.getMeasurementUnitString(displayUnits));
  }
  
  //public
  
  /*
   * @author Kee Chin Seong
   */
  public LandPatternAreaRenderer getLandPatternRenderer()
  {
     return _currentDraggedAreaRenderer;
  }
  
  /*
   * @author Chin-Seong, Kee
   */
  public Shape getRendererShape()
  {
    Assert.expect(_currentDraggedAreaRenderer != null);
    
    return _currentDraggedAreaRenderer.getAreaShape();
  }
  
  /*
   * @author Chin-Seong, Kee
   */
  public LandPattern getCurrentLandPattern()
  {
    Assert.expect(_currentLandPattern != null);
    
    return _currentLandPattern;
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void zoomIn()
  {
    _graphicsEngine.zoom(_ZOOM_FACTOR);
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void zoomOut()
  {
    _graphicsEngine.zoom(1.0 / _ZOOM_FACTOR);
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void setZoomFactor(double zoomFactor)
  {
      
    _graphicsEngine.resetZoom();
    _graphicsEngine.setZoom(zoomFactor);
    _graphicsEngine.center();
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void setZoomRectangleMode(boolean zoomRectangleMode)
  {
    if(zoomRectangleMode)
    {
      _graphicsEngine.setMeasurementMode(false);
      _graphicsEngine.setDragGraphicsMode(false);
      _graphicsEngine.setZoomRectangleMode(true);
    }
    else
    {
      _graphicsEngine.setZoomRectangleMode(false);
      _graphicsEngine.setMouseEventsEnabled(true);
    }
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void setDragGraphicsMode(boolean dragGraphicsMode)
  {
    if(dragGraphicsMode)
    {
      _graphicsEngine.setMeasurementMode(false);
      _graphicsEngine.setZoomRectangleMode(false);
      _graphicsEngine.setDragGraphicsMode(true);
    }
    else
    {
      _graphicsEngine.setDragGraphicsMode(false);
      _graphicsEngine.setMouseEventsEnabled(true);
    }
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void setMeasurmentMode(boolean measurementMode)
  {
    if(measurementMode)
    {
      _graphicsEngine.setZoomRectangleMode(false);
      _graphicsEngine.setDragGraphicsMode(false);
      _graphicsEngine.setMeasurementMode(true);
    }
    else
    {
      _graphicsEngine.setMeasurementMode(false);
      _graphicsEngine.setMouseEventsEnabled(true);
    }
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void setImageFitToScreen(boolean isImageFitToScreen)
  {
    _isGraphicsShouldBeFitToScreen = isImageFitToScreen;
    if(_inspectionImageRenderer != null)
        _inspectionImageRenderer.setBicubicInterpolation(_isGraphicsShouldBeFitToScreen);
 }
  
  public boolean getImageFitToScreen()
  {
    return _isGraphicsShouldBeFitToScreen;
  }
  /**
   * @author Chin-Seong, Kee
   */
  public void showTopSide()
  {
    Assert.expect(false);
  }
  
  /**
   * @author Chin-Seong, Kee
   */
  public void setImageStayPersistent(boolean isImageStayPersistent)
  {
    _isImageStayPersistent = isImageStayPersistent;
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void showBottomSide()
  {
    Assert.expect(false);
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void clearGraphics()
  {
    _graphicsEngine.setVisible(_landPatternRendererLayer, false);
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void showGraphics()
  {
    _graphicsEngine.setVisible(_landPatternRendererLayer, true);
  }

  /**
   * @author Scott Richardson
   */
  public void translateUpInZAxis()
  {
    Assert.expect(false, "translateUpInZAxis is not implemented here");
  }

  /**
   * @author Scott Richardson
   */
  public void translateDownInZAxis()
  {
    Assert.expect(false, "translateUpInZAxis is not implemented here");
  }
  
  /**
   * @author Kee Chin Seong
   * @param isSelectRegionMode 
   */
  public void setSelectRegionMode(boolean isSelectRegionMode)
  {
      
  }
  
  public void setGroupSelectMode(boolean isGroupSelectMode)
  {
        
  }
  
  public void setSelectRegionMode(boolean isSelectRegionMode, double maxImageRegionWidth, double maxImageRegionHeight)
  {
  }
  
  public void resetGraphics()
  {
    
  }
}
