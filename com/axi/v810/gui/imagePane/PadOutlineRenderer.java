package com.axi.v810.gui.imagePane;

import java.awt.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.drawCad.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

/**
 * This class draws a Pad outline.
 *
 * @author Kay Lannen
 */
public class PadOutlineRenderer extends ShapeRenderer
{
  private Pad _pad;

  /**
   * @author Kay Lannen
   */
  PadOutlineRenderer(Pad pad)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(pad != null);
    _pad = pad;

    refreshData();
  }

  /**
   * @author Kay Lannen
   */
  public Pad getPad()
  {
    return _pad;
  }

  /**
   * Return the name of the panel
   * @author Kay Lannen
   */
  public String toString()
  {
    return _pad.getComponent().toString() + "-" + _pad.getName();
  }

  /**
   * @author Kay Lannen
   */
  protected void refreshData()
  {
    Assert.expect(_pad != null);

    initializeShape(_pad.getShapeRelativeToPanelInNanoMeters());
  }

  protected void paintComponent(Graphics graphics)
  {
    super.paintComponent(graphics);
//    System.out.println("pc: Pad outline renderer bounds are: " + this.getBounds().toString() +
//                       " for " + this.toString());

  }

/*

  protected void setBounds()
  {
    super.setBounds();
    System.out.println("sb: Pad outline renderer bounds are: " + this.getBounds().toString() +
                       " for " + this.toString());
  }

  public void setBounds(Rectangle r)
  {
    super.setBounds(r);
    System.out.println("sbr: Pad outline renderer bounds are: " + this.getBounds().toString() +
                       " for " + this.toString());

  }

  public void setBounds(int x, int y, int width, int height)
  {
    super.setBounds(x, y, width, height);
    System.out.println("sbxywh: Pad outline renderer bounds are: " + this.getBounds().toString() +
                       " for " + this.toString());
  }
*/

}
