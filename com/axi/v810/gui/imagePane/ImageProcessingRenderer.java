package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.util.List;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * This class draws an inspection image
 * @author George A. David
 * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
 *    Copy and modify from both InspectionImageRenderer & ReconstructedImageRenderer for code 
 *    centralization.
 */
public class ImageProcessingRenderer extends BufferedImageRenderer
{
  protected BufferedImage _sourceBufferedImage = null;
  protected BufferedImage _filterBufferedImage = null; 
  
  //Lim, Lay Ngor - XCR1652 - Image Post Processing
  protected BufferedImage _postProcessedBufferedImage = null;
  BrightnessAndContrastSetting _brightnessAndContrastSetting = null;  
  
  private RescaleOp _rescale;
  
  /**
   * @author Lim, Lay Ngor
   */
  public ImageProcessingRenderer(BufferedImage bufferedImage)
  {
    super();
    refreshData(bufferedImage, 0, 0); 
  }

  /**
   * @author Lim, Lay Ngor
   */
  public ImageProcessingRenderer(BufferedImage bufferedImage, int x, int y)
  {
    super();
    refreshData(bufferedImage, x, y); 
  }
  
  /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor
   */
  protected void refreshData(BufferedImage bufferedImage, int x, int y)
  {
    Assert.expect(bufferedImage != null);

    initializeImage(bufferedImage, x, y, bufferedImage.getWidth(), bufferedImage.getHeight());
    _sourceBufferedImage = bufferedImage;
    _filterBufferedImage = _sourceBufferedImage;
    _postProcessedBufferedImage = _sourceBufferedImage;
    
    _brightnessAndContrastSetting = new BrightnessAndContrastSetting(); //with default value
  }  
  
/**
 * @author George A. David
 */
  public void paintComponent(Graphics graphics)
  {
    super.paintComponent(graphics);
  }

  /**
   * @author Chong, Wei Chin
   */
  public void brighten()
  {
//    if (_offset < 255)
//    {
      _brightnessAndContrastSetting._offset = _brightnessAndContrastSetting._offset + 5.0f;

      //Lim, Lay Ngor - XCR1652 - Image Post Processing
      _brightnessAndContrastSetting._scaleImage = true;
      normalizeAndRescaleImage();      
//    }
  }

  /**
   * @author Chong, Wei Chin
   */
  public void darken()
  {
//    if (_offset > 0)
//    {
      _brightnessAndContrastSetting._offset = _brightnessAndContrastSetting._offset - 5.0f;

      //Lim, Lay Ngor - XCR1652 - Image Post Processing
      _brightnessAndContrastSetting._scaleImage = true;
      normalizeAndRescaleImage();           
//    }
  }

  /**
   * @author Chong, Wei Chin
   */
  public void increaseContrast()
  {
//    if (_scaleFactor < 2)
//    {
      _brightnessAndContrastSetting._scaleFactor = _brightnessAndContrastSetting._scaleFactor + 0.05f;
       
      //Lim, Lay Ngor - XCR1652 - Image Post Processing
      _brightnessAndContrastSetting._scaleImage = true;
      normalizeAndRescaleImage();  
//    }
  }

  /**
   * @author Chong, Wei Chin
   */
  public void decreaseContrast()
  {
//    if (_scaleFactor > 0)
//    {
      _brightnessAndContrastSetting._scaleFactor = _brightnessAndContrastSetting._scaleFactor - 0.05f;

      //Lim, Lay Ngor - XCR1652 - Image Post Processing
      _brightnessAndContrastSetting._scaleImage = true;
      normalizeAndRescaleImage();     
//    }
  }

  /**
   * @author Chong, Wei Chin
   */
  protected void rescale(BufferedImage sourceBufferedImage)
  {
    _rescale = new RescaleOp(_brightnessAndContrastSetting._scaleFactor, _brightnessAndContrastSetting._offset, null);
    
    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    _bufferedImage = _rescale.filter(sourceBufferedImage, null);

    revalidate();
  }

  /**
   * @author Chong, Wei Chin
   */
  public void equalizeGrayScale()
  {
    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    //Reset so don't need to update scale
    _filterBufferedImage = BufferedImageUtil.equalizeGrayScale(_postProcessedBufferedImage);//beware inside new    
    _bufferedImage = _filterBufferedImage;

    revalidate();
    _brightnessAndContrastSetting.resetToDefaultValue();
    _brightnessAndContrastSetting._normalizeImage = true;
  }

  /**
   * @author Chong, Wei Chin
   */
  public void revertToOriginalBufferedImage()
  {
    if(_bufferedImage.equals(_sourceBufferedImage) == false)
    {
      _bufferedImage = _sourceBufferedImage;

     //Lim, Lay Ngor - XCR1652 - Image Post Processing
      _postProcessedBufferedImage = _sourceBufferedImage;

      revalidate();
    }

    _brightnessAndContrastSetting.resetToDefaultValue();
  }

  /**
   * @param filename
   *
   * @author Chong, Wei Chin
   */
  public void saveBufferedImage(String filename)
  {
    Assert.expect(filename != null);
    try
    {
      BufferedImageUtil.writeImageToJpegFile(_bufferedImage, filename);
    }
    catch(final FileNotFoundException fnfe)
    {
      MessageDialog.showErrorDialog(this, fnfe.getLocalizedMessage(), StringLocalizer.keyToString("DCGUI_SAVE_IMAGE_KEY"));
    }
    catch(final IOException ioe)
    {
      MessageDialog.showErrorDialog(this, ioe.getLocalizedMessage(), StringLocalizer.keyToString("DCGUI_SAVE_IMAGE_KEY"));
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  public void increaseDesiredMaxValue()
  {
    _brightnessAndContrastSetting._normalizeImage = true;
    _brightnessAndContrastSetting._shiftValue += 1;
    
    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    normalizeAndRescaleImage();
  }

  /**
   * @author Chong, Wei Chin
   */
  public void decreaseDesiredMaxValue()
  {
    _brightnessAndContrastSetting._normalizeImage = true;
    _brightnessAndContrastSetting._shiftValue -= 1;
    
    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    normalizeAndRescaleImage();
  }
  
  /**
   * 
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */
  public void normalizeAndRescaleImage()
  {
    if(_brightnessAndContrastSetting._normalizeImage)
      _filterBufferedImage = BufferedImageUtil.shiftNormalizeGrayScale(_postProcessedBufferedImage, _brightnessAndContrastSetting._shiftValue);
    else
      _filterBufferedImage = _postProcessedBufferedImage;
      
    if(_brightnessAndContrastSetting._scaleImage)
      rescale(_filterBufferedImage); 
    else
      _bufferedImage = _filterBufferedImage;
     
    revalidate();
  }
  
  /**
   * Use original buffer to perform image enhancement
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */
  public void imagePostProcessing(List<ImageEnhancerBase> enhancerList)
  {
    //Reset and do once for Post processing
    _postProcessedBufferedImage = _sourceBufferedImage;
    if (enhancerList != null)
    {
      for (ImageEnhancerBase enhancer : enhancerList)
      {
        com.axi.util.image.Image currentImage = com.axi.util.image.Image.createFloatImageFromBufferedImage(_postProcessedBufferedImage);
        if (enhancer.getType() == ImageEnhancerBase.ImageEnhancerType.Resize)
        {
          ResizeImage resize = (ResizeImage) enhancer;
          int width = (int) (currentImage.getWidth() * resize._scaleX);
          int height = (int) (currentImage.getHeight() * resize._scaleY);

          com.axi.util.image.Image resizedImage = new com.axi.util.image.Image(width, height);
          enhancer.runEnhancement(resizedImage, currentImage);
          _postProcessedBufferedImage = resizedImage.getBufferedImage();
          resizedImage.decrementReferenceCount();//seems cannot delete, if not other function failed to get this buffer      
        }
        else
        {
          com.axi.util.image.Image filterImage = new com.axi.util.image.Image(currentImage.getWidth(), currentImage.getHeight());
          enhancer.runEnhancement(filterImage, currentImage);
          _postProcessedBufferedImage = filterImage.getBufferedImage();
          filterImage.decrementReferenceCount();//seems cannot delete, if not other function failed to get this buffer      
        }

        //delete the buffer after create it.
        currentImage.decrementReferenceCount();
      }
    }
    //enhancerList == null when user disable all enhancer button.
    normalizeAndRescaleImage();
  }      
  
  /**
   * Return the BrightnessAndContrastSetting of this image for next image use.
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */
  public BrightnessAndContrastSetting getBrightnessAndContrastSetting()
  {
    return _brightnessAndContrastSetting;
  }
  
  /**
   * Set the BrightnessAndContrastSetting from previous image to current image.
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */  
  public void setBrightnessAndContrastSetting(BrightnessAndContrastSetting setting)
  {
    if (setting != null)
    {
      _brightnessAndContrastSetting._scaleFactor = setting._scaleFactor;
      _brightnessAndContrastSetting._offset = setting._offset;
      _brightnessAndContrastSetting._shiftValue = setting._shiftValue;
      _brightnessAndContrastSetting._normalizeImage = setting._normalizeImage;
      _brightnessAndContrastSetting._scaleImage = setting._scaleImage;
    }
    else
    {
      _brightnessAndContrastSetting = new BrightnessAndContrastSetting(
        setting._scaleFactor,
        setting._offset,
        setting._shiftValue,
        setting._normalizeImage,
        setting._scaleImage
        );      
    }
  }
}
