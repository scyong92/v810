package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author George A. David
 */
public class ImagePaneRendererCreator
{
  private Project _project;
  private Map<ReconstructionRegion, VerificationImageRenderer> _reconstructionRegionToVerificationImageRendererMap = new HashMap<ReconstructionRegion, VerificationImageRenderer>();
  private Map<Pad, RegionOfInterestRenderer> _padToPadRendererMap = new HashMap<Pad, RegionOfInterestRenderer>();
  private Map<Pad, PadNameRenderer> _padToPadNameRendererMap = new HashMap<Pad, PadNameRenderer>();
  private Map<Pad, JointTypeRenderer> _padToPadJointTypeRendererMap = new HashMap<Pad, JointTypeRenderer>();
  private Map<Pad, OrientationArrowRenderer> _padToOrientationArrowRendererMap = new HashMap<Pad, OrientationArrowRenderer>();
  private Map<com.axi.v810.business.panelDesc.Component, ReferenceDesignatorRenderer> _componentToRefDesRendererMap = new HashMap<com.axi.v810.business.panelDesc.Component, ReferenceDesignatorRenderer>();
  private Map<com.axi.v810.business.panelDesc.Component, JointTypeRenderer> _componentToJointTypeRendererMap = new HashMap<com.axi.v810.business.panelDesc.Component, JointTypeRenderer>();
  private Map<com.axi.v810.business.panelDesc.Component, Set<Renderer>> _componentToRenderersMap = new HashMap<com.axi.v810.business.panelDesc.Component, Set<Renderer>>();

  private Map<Fiducial, FiducialReferenceDesignatorRenderer> _fiducialToRefDesRendererMap = new HashMap<Fiducial, FiducialReferenceDesignatorRenderer>();
  private Map<Fiducial, RegionOfInterestRenderer> _fiducialToFiducialRendererMap = new HashMap<Fiducial, RegionOfInterestRenderer>();
  private java.util.List<Pad> _selectedPads = new LinkedList<Pad>();
  private java.util.List<Fiducial> _selectedFiducials = new LinkedList<Fiducial>();

  private SurroundingReconstructionRegionUtil<ReconstructionRegion> _regionUtil = new SurroundingReconstructionRegionUtil<ReconstructionRegion>();
  private SurroundingComponentUtil _componentUtil = new SurroundingComponentUtil();
  private AffineTransform _nanoMetersToPixelTransform = AffineTransform.getScaleInstance(1.0 / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel(),
                                                                                        1.0 / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
  
  protected Map<Integer, Integer> _subProgramIdToImageLayer = null;
  private Map<Integer, Integer> _subProgramIdToPadLayer = null;
  private Map<Integer, Integer> _subProgramIdToPadNameLayer = null;
  private Map<Integer, Integer> _subProgramIdToOrientationArrowLayer = null;
  private Map<Integer, Integer> _subProgramIdToRefDesLayer = null;
  
  private boolean _drawRefDes = false;
  private boolean _drawPadNames = false;
  private boolean _drawOrientationArrows = false;
  private Stroke _selectedStroke = new BasicStroke(3);
  private Color _selectedColor = Color.white;
  private boolean _showCadGraphics = true;
  private boolean _showSelectedPadsOfFiducials = true;

  private MagnificationEnum _magnification = MagnificationEnum.NOMINAL;
  private PanelLocationInSystemEnum _panelLocationInSystem = null;
  
  private boolean _isCheckLatestProgram = true;

  private int _currentTestSubProgramId = -1;
  private int _layer_Number;
  
  /**
   * @author George A. David
   */
  ImagePaneRendererCreator(Project project , boolean checkLatestestProgram)
  {
    Assert.expect(project != null);

    _project = project;
    //verification image - sham
    if(checkLatestestProgram)
    {
    	MainMenuGui.getInstance().generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_VERIFICATION_IMAGES_NEEDS_TEST_PROGRAM_KEY"),
                                                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                                    _project);
    }
    _regionUtil.setUseBoundsForDeterminingVicinity(true);
    _regionUtil.setReconstructionRegions(_project.getTestProgram().getVerificationRegions());
    _componentUtil.setComponents(_project.getPanel().getComponents());
  }

  /**
   * @author Chong, Wei Chin
   */
  ImagePaneRendererCreator(Project project, MagnificationEnum magnification ,  boolean checkLatestestProgram)
  {
    Assert.expect(project != null);

    _project = project;
    _magnification = magnification;
    _nanoMetersToPixelTransform = AffineTransform.getScaleInstance(1.0 / (double)_magnification.getNanoMetersPerPixel(),
                                                                                        1.0 / (double)_magnification.getNanoMetersPerPixel());

    _isCheckLatestProgram = checkLatestestProgram;
    if(checkLatestestProgram)
    {
      MainMenuGui.getInstance().generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_VERIFICATION_IMAGES_NEEDS_TEST_PROGRAM_KEY"),
                                                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                                      _project);
    }

    _regionUtil.setUseBoundsForDeterminingVicinity(true);
    _regionUtil.setReconstructionRegions(_project.getTestProgram().getVerificationRegions());
    _componentUtil.setComponents(_project.getPanel().getComponents());
  }
  
  /**
   * @author Goh Bee Hoon
   */
  ImagePaneRendererCreator(Project project, MagnificationEnum magnification , boolean checkLatestestProgram, PanelLocationInSystemEnum panelLocationInSystem)
  {
    Assert.expect(project != null);

    _project = project;
    _magnification = magnification;
    _nanoMetersToPixelTransform = AffineTransform.getScaleInstance(1.0 / (double)_magnification.getNanoMetersPerPixel(),
                                                                                        1.0 / (double)_magnification.getNanoMetersPerPixel());

    _isCheckLatestProgram = checkLatestestProgram;
    if(checkLatestestProgram)
    {
      MainMenuGui.getInstance().generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_VERIFICATION_IMAGES_NEEDS_TEST_PROGRAM_KEY"),
                                                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                                      _project);
    }
    
    _regionUtil.setUseBoundsForDeterminingVicinity(true);
    _panelLocationInSystem = panelLocationInSystem;
    if (panelLocationInSystem.equals(PanelLocationInSystemEnum.RIGHT))
    {
      _regionUtil.setReconstructionRegions(_project.getTestProgram().getRightVerificationRegions());
    } 
    else if (panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT))
    {
      _regionUtil.setReconstructionRegions(_project.getTestProgram().getLeftVerificationRegions());
    }
    _componentUtil.setComponents(_project.getPanel().getComponents());
  }

  /**
   * @author George A. David
   */
  public void setSelectedColor(Color selectedColor)
  {
    Assert.expect(selectedColor != null);

    _selectedColor = selectedColor;
  }

  /**
   * @author Chong, Wei Chin
   */
  public void setImageLayer(Map subProgramIdToImageLayer)
  {
    Assert.expect(subProgramIdToImageLayer != null);
    Assert.expect(subProgramIdToImageLayer.size() > 0);
    
    _subProgramIdToImageLayer = subProgramIdToImageLayer;
  }

  /**
   * @author Chong, Wei Chin
   */
  public void setRefDesLayer(Map subProgramIdToRefDesLayer)
  {
    Assert.expect(subProgramIdToRefDesLayer != null);
    Assert.expect(subProgramIdToRefDesLayer.size() > 0);

    _subProgramIdToRefDesLayer = subProgramIdToRefDesLayer;
    _drawRefDes = true;
  }

  /**
   * @author Chong, Wei Chin
   */
  public void setPadLayer(Map subProgramIdToPadLayer)
  {
    Assert.expect(subProgramIdToPadLayer != null);
    Assert.expect(subProgramIdToPadLayer.size() > 0);

    _subProgramIdToPadLayer = subProgramIdToPadLayer;
  }

  /**
   * @author Chong, Wei Chin
   */
  public void setPadNameLayer(Map subProgramIdToPadNameLayer)
  {
    Assert.expect(subProgramIdToPadNameLayer != null);
    Assert.expect(subProgramIdToPadNameLayer.size() > 0);

    _subProgramIdToPadNameLayer = subProgramIdToPadNameLayer;
    _drawPadNames = true;
  }

  /**
   * @author  Chong, Wei Chin
   */
  public void setOrientationArrowLayer(Map subProgramIdToArrowLayer)
  {
    Assert.expect(subProgramIdToArrowLayer != null);
    Assert.expect(subProgramIdToArrowLayer.size() > 0);
    
    _subProgramIdToOrientationArrowLayer = subProgramIdToArrowLayer;
    _drawOrientationArrows = true;
  }

  /**
   * @author George A. David
   */
  public void setSelectedPads(Collection<Pad> selectedPads)
  {
    Assert.expect(selectedPads != null);

    _selectedPads.clear();
    _selectedPads.addAll(selectedPads);
  }

  /**
   * @author George A. David
   */
  public void setSelectedFiducials(Collection<Fiducial> selectedFiducials)
  {
    Assert.expect(selectedFiducials != null);

    _selectedFiducials.clear();
    _selectedFiducials.addAll(selectedFiducials);
  }

  /**
   * @author George A. David
   */
  public void createVerificationImageRenderers(GraphicsEngine graphicsEngine, boolean showTopSide)
  {
    Assert.expect(graphicsEngine != null);

    PanelRectangle visibleRect = new PanelRectangle(graphicsEngine.getVisibleRectangleInRendererCoordinates());
    visibleRect.setRect(visibleRect.getMinX() * _magnification.getNanoMetersPerPixel(),
                        visibleRect.getMinY() * _magnification.getNanoMetersPerPixel(),
                        visibleRect.getWidth() * _magnification.getNanoMetersPerPixel(),
                        visibleRect.getHeight() * _magnification.getNanoMetersPerPixel());

    Set<ReconstructionRegion> regionsNeedingRenderers = _regionUtil.getIntersectingReconstructionRegionsInRectangle(visibleRect, showTopSide);
    Set<ReconstructionRegion> newlyInvisibleReconstructionRegions = new HashSet<ReconstructionRegion>(
        _reconstructionRegionToVerificationImageRendererMap.keySet());
    newlyInvisibleReconstructionRegions.removeAll(regionsNeedingRenderers);
    regionsNeedingRenderers.removeAll(_reconstructionRegionToVerificationImageRendererMap.keySet());

    for (ReconstructionRegion region : regionsNeedingRenderers)
    {
      if(region.getTestSubProgram().isLowMagnification() && _magnification.equals(MagnificationEnum.H_NOMINAL))
        continue;
      if(region.getTestSubProgram().isHighMagnification() && _magnification.equals(MagnificationEnum.NOMINAL))
        continue;
      
      VerificationImageRenderer renderer = new VerificationImageRenderer(_project.getName(), region);
      graphicsEngine.addRenderer(_subProgramIdToImageLayer.get(region.getTestSubProgram().getId()), renderer);
      _reconstructionRegionToVerificationImageRendererMap.put(region, renderer);
    }

    for (ReconstructionRegion region : newlyInvisibleReconstructionRegions)
    {
      graphicsEngine.removeRenderer(_reconstructionRegionToVerificationImageRendererMap.remove(region));
    }

    graphicsEngine.repaint();
  }

  /**
   * @author George A. David
   */
  public void clearCadRenderers(GraphicsEngine graphicsEngine)
  {
    Assert.expect(graphicsEngine != null);

    if (_subProgramIdToRefDesLayer == null || graphicsEngine.isZoomLevelBelowCreationThreshold(_subProgramIdToRefDesLayer.values().iterator().next()) == false)
    {
      for(Renderer renderer : _componentToRefDesRendererMap.values())
      {
        if( graphicsEngine.getLayerNumberForRenderer(renderer) != -1)
          graphicsEngine.removeRenderer(renderer);
      }
      
      for(Renderer renderer: _componentToJointTypeRendererMap.values())
      {
        if( graphicsEngine.getLayerNumberForRenderer(renderer) != -1)
          graphicsEngine.removeRenderer(renderer);
      }
    }
    _componentToRefDesRendererMap.clear();
    _componentToJointTypeRendererMap.clear();

//    if(graphicsEngine.isLayerVisible(_rightPadNameLayer))
    {
      for (Renderer renderer : _padToPadNameRendererMap.values())
      {
        if( graphicsEngine.getLayerNumberForRenderer(renderer) != -1)
          graphicsEngine.removeRenderer(renderer);
      }
      for (Renderer renderer : _padToPadJointTypeRendererMap.values())
      {
        if( graphicsEngine.getLayerNumberForRenderer(renderer) != -1)
          graphicsEngine.removeRenderer(renderer);
      }
    }
    _padToPadNameRendererMap.clear();
    _padToPadJointTypeRendererMap.clear();

//    if(graphicsEngine.isLayerVisible(_rightOrientationArrowLayer))
    {
      for (Renderer renderer : _padToOrientationArrowRendererMap.values())
      {
        if( graphicsEngine.getLayerNumberForRenderer(renderer) != -1)
          graphicsEngine.removeRenderer(renderer);
      }
    }
    _padToOrientationArrowRendererMap.clear();

    if(_subProgramIdToPadLayer == null || graphicsEngine.isLayerVisible(_subProgramIdToPadLayer.values().iterator().next()))
    {
      for (Renderer renderer : _padToPadRendererMap.values())
      {
        if( graphicsEngine.getLayerNumberForRenderer(renderer) != -1)
          graphicsEngine.removeRenderer(renderer);
      }
    }
    _padToPadRendererMap.clear();

    if (_subProgramIdToRefDesLayer == null || graphicsEngine.isZoomLevelBelowCreationThreshold(_subProgramIdToRefDesLayer.values().iterator().next()) == false)
    {
      for(Renderer renderer : _fiducialToRefDesRendererMap.values())
      {
        if( graphicsEngine.getLayerNumberForRenderer(renderer) != -1)
          graphicsEngine.removeRenderer(renderer);
      }
    }
    _fiducialToRefDesRendererMap.clear();

    for(Renderer renderer : _fiducialToFiducialRendererMap.values())
    {
      if( graphicsEngine.getLayerNumberForRenderer(renderer) != -1)
        graphicsEngine.removeRenderer(renderer);
    }
    _fiducialToFiducialRendererMap.clear();

    _componentToRenderersMap.clear();
  }

  /**
   * @author George A. David
   */
  public void clearRenderers(GraphicsEngine graphicsEngine)
  {
    clearCadRenderers(graphicsEngine);

    for(Renderer renderer : _reconstructionRegionToVerificationImageRendererMap.values())
      graphicsEngine.removeRenderer(renderer);

    _reconstructionRegionToVerificationImageRendererMap.clear();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void clearSurroundingReconstructionRegions()
  {
    _regionUtil.clearReconstructionRegions();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void reset()
  {
    _project = null;

    _reconstructionRegionToVerificationImageRendererMap.clear();
    _padToPadRendererMap.clear();
    _padToPadNameRendererMap.clear();
    _padToPadJointTypeRendererMap.clear();
    _padToOrientationArrowRendererMap.clear();
    _componentToRefDesRendererMap.clear();
    _componentToJointTypeRendererMap.clear();
    _componentToRenderersMap.clear();
    _fiducialToRefDesRendererMap.clear();
    _fiducialToFiducialRendererMap.clear();
    _selectedPads.clear();
    _selectedFiducials.clear();
    
    _regionUtil = null;
    _componentUtil = null;
  }

  /**
   * @author George A. David
   */
  public void clearComponentRenderers(com.axi.v810.business.panelDesc.Component component, GraphicsEngine graphicsEngine)
  {
    Assert.expect(component != null);
    Assert.expect(graphicsEngine != null);

    if (_componentToRefDesRendererMap.containsKey(component))
    {
      if (_subProgramIdToRefDesLayer == null || graphicsEngine.isZoomLevelBelowCreationThreshold(_subProgramIdToRefDesLayer.values().iterator().next()) == false)
        graphicsEngine.removeRenderer(_componentToRefDesRendererMap.remove(component));
      else
        _componentToRefDesRendererMap.remove(component);
    }
    if (_componentToJointTypeRendererMap.containsKey(component))
    {
      if (graphicsEngine.isZoomLevelBelowCreationThreshold(_subProgramIdToRefDesLayer.values().iterator().next()) == false)
        graphicsEngine.removeRenderer(_componentToJointTypeRendererMap.remove(component));
      else
        _componentToJointTypeRendererMap.remove(component);
    }


    for(Pad pad : component.getPads())
    {
      if (_padToPadRendererMap.containsKey(pad))
        graphicsEngine.removeRenderer(_padToPadRendererMap.remove(pad));

      if (_padToPadNameRendererMap.containsKey(pad))
        graphicsEngine.removeRenderer(_padToPadNameRendererMap.remove(pad));

      if (_padToPadJointTypeRendererMap.containsKey(pad))
        graphicsEngine.removeRenderer(_padToPadJointTypeRendererMap.remove(pad));

      if (_padToOrientationArrowRendererMap.containsKey(pad))
        graphicsEngine.removeRenderer(_padToOrientationArrowRendererMap.remove(pad));
    }

    _componentToRenderersMap.remove(component);
  }

  /**
   * @author George A. David
   */
  void setShowCadGraphics(boolean showCadGraphics)
  {
    _showCadGraphics = showCadGraphics;
  }

  /**
   * @author George A. David
   */
  void setShowSelectedPadsOrFiducials(boolean showSelectedPads)
  {
    _showSelectedPadsOfFiducials = showSelectedPads;
  }

  /**
   * @author George A. David
   */
  private void addRendererForComponent(GraphicsEngine graphicsEngine, com.axi.v810.business.panelDesc.Component component, Renderer renderer)
  {
    Assert.expect(component != null);
    Assert.expect(renderer != null);

    //XCR2240: Khaw Chek Hau - Assert when dragging around at Adjust CAD
    _layer_Number = graphicsEngine.getLayerNumberForRenderer(renderer);
    
    //Prevent creating renderer for those region outside the panel
    if(_layer_Number != -1 )  
    {    
      if(_componentToRenderersMap.containsKey(component))
        _componentToRenderersMap.get(component).add(renderer);
      else
      {
        Set<Renderer> renderers = new HashSet<Renderer>();
        renderers.add(renderer);
        _componentToRenderersMap.put(component, renderers);
      }
    }
  }

  /**
   * @author George A. David
   */
  private void removeRendererForComponent(com.axi.v810.business.panelDesc.Component component, Renderer renderer)
  {
    Assert.expect(component != null);
    Assert.expect(renderer != null);

    if(_componentToRenderersMap.containsKey(component))
    {
      Set<Renderer> renderers = _componentToRenderersMap.get(component);
      renderers.remove(renderer);
      if(renderers.isEmpty())
        _componentToRenderersMap.remove(component);
    }
  }

  /**
   * @author George A. David
   */
  private void updateRefDesRenderer(com.axi.v810.business.panelDesc.Component component, boolean drawRefDes, GraphicsEngine graphicsEngine)
  {
    Assert.expect(component != null);
    Assert.expect(graphicsEngine != null);

    if(drawRefDes)
    {
      if (_componentToRefDesRendererMap.containsKey(component) == false)
      {
        ReferenceDesignatorRenderer renderer = new ReferenceDesignatorRenderer(component, false);
        JointTypeRenderer jointTypeRenderer = new JointTypeRenderer(component, false);
        
        _project.setCheckLatestTestProgram(_isCheckLatestProgram);
        for(TestSubProgram testSubProgram : _project.getTestProgram().getAllTestSubPrograms())
        {
          if(testSubProgram.isLowMagnification() && _magnification.equals(MagnificationEnum.H_NOMINAL))
              continue;
          if(testSubProgram.isHighMagnification() && _magnification.equals(MagnificationEnum.NOMINAL))
              continue;
          if (_currentTestSubProgramId != -1)
            if (testSubProgram.getId() != _currentTestSubProgramId)
              continue;
            
          if(testSubProgram.getRealVerificationRegionsBoundsInNanoMeters().intersects(component.getShapeRelativeToPanelInNanoMeters().getBounds2D()))
          {
            graphicsEngine.addRenderer(_subProgramIdToRefDesLayer.get(testSubProgram.getId()), renderer);
            graphicsEngine.addRenderer(_subProgramIdToRefDesLayer.get(testSubProgram.getId()), jointTypeRenderer);
            break;
          }
        }
        _componentToRefDesRendererMap.put(component, renderer);
        _componentToJointTypeRendererMap.put(component, jointTypeRenderer);
        addRendererForComponent(graphicsEngine, component, renderer);
        addRendererForComponent(graphicsEngine, component, jointTypeRenderer);
      }
    }
    else
    {
      if (_componentToRefDesRendererMap.containsKey(component))
      {
        Renderer renderer = null;
        Renderer jointTypeRenderer = null;
        if (graphicsEngine.isZoomLevelBelowCreationThreshold(_subProgramIdToRefDesLayer.values().iterator().next()) == false)
        {
         renderer = _componentToRefDesRendererMap.remove(component);
         graphicsEngine.removeRenderer(renderer);
         jointTypeRenderer = _componentToJointTypeRendererMap.remove(component);
         graphicsEngine.removeRenderer(jointTypeRenderer);
        }
        else
        {
          renderer = _componentToRefDesRendererMap.remove(component);
          jointTypeRenderer = _componentToJointTypeRendererMap.remove(component);
        }
        removeRendererForComponent(component, renderer);
        removeRendererForComponent(component, jointTypeRenderer);
      }
    }

  }

  /**
   * @author George A. David
   */
  private void updatePadRenderers(com.axi.v810.business.panelDesc.Component component,
                                  boolean isPadLayerVisible,
                                  boolean drawPadNames,
                                  boolean drawOrientationArrows,
                                  GraphicsEngine graphicsEngine,
                                  Rectangle2D visibleRect)
  {
    Assert.expect(component != null);
    Assert.expect(graphicsEngine != null);
    Assert.expect(visibleRect != null);

    if (isPadLayerVisible || drawPadNames || drawOrientationArrows)
    {
      for (Pad pad : component.getPads())
      {
        java.awt.Shape padShape = pad.getShapeRelativeToPanelInNanoMeters();
        if(padShape.intersects(visibleRect))
        {
          int padLayer = -1;
          int padNameLayer = -1;
          int orientationArrowLayer = -1;

          for(TestSubProgram testSubProgram : _project.getTestProgram().getAllTestSubPrograms())
          {
            if(testSubProgram.isLowMagnification() && _magnification.equals(MagnificationEnum.H_NOMINAL))
              continue;
            if(testSubProgram.isHighMagnification() && _magnification.equals(MagnificationEnum.NOMINAL))
              continue;
            if (_currentTestSubProgramId != -1) // indicate selected testSubProgram ID only
              if (testSubProgram.getId() != _currentTestSubProgramId) // bypass any that does not belongs to selected testSubProgram due to possible overlap
                continue;

            if (testSubProgram.getRealVerificationRegionsBoundsInNanoMeters().intersects(padShape.getBounds2D()))
            {
              //Swee Yee Wong - XCR-2680 Wrong transform is used for overlap board
              PanelSettings panelSettings = component.getBoard().getPanel().getPanelSettings();
              if (panelSettings.isPanelBasedAlignment() == false && testSubProgram.getBoard().equals(component.getBoard()) == false)
              {
                continue;
              }
              if(_panelLocationInSystem != null && _panelLocationInSystem.equals(testSubProgram.getPanelLocationInSystem()) == false)
              {
                continue;
              }
              
              padLayer = _subProgramIdToPadLayer.get(testSubProgram.getId());
              if(drawPadNames)
                padNameLayer = _subProgramIdToPadNameLayer.get(testSubProgram.getId());
              if(drawOrientationArrows)
                orientationArrowLayer = _subProgramIdToOrientationArrowLayer.get(testSubProgram.getId());
              break;
            }
          }
          if(padLayer == -1)
            return;
          
          Assert.expect(padLayer != -1);
          Assert.expect(drawPadNames == (padNameLayer != -1));
          Assert.expect(drawOrientationArrows == (orientationArrowLayer != -1));

          if (isPadLayerVisible)
          {
            if (_padToPadRendererMap.containsKey(pad) == false)
            {
              Assert.expect(padShape != null);
              padShape = _nanoMetersToPixelTransform.createTransformedShape(padShape);
              PadRegionOfInterestRenderer regionRenderer = new PadRegionOfInterestRenderer(padShape, pad);
              graphicsEngine.addRenderer(padLayer, regionRenderer);
              _padToPadRendererMap.put(pad, regionRenderer);
              addRendererForComponent(graphicsEngine, component, regionRenderer);
            }
          }

          if (drawPadNames)
          {
            if (_padToPadNameRendererMap.containsKey(pad) == false)
            {
              PadNameRenderer padNameRenderer = new PadNameRenderer(pad, false);
              graphicsEngine.addRenderer(padNameLayer, padNameRenderer);
              _padToPadNameRendererMap.put(pad, padNameRenderer);
              addRendererForComponent(graphicsEngine, component, padNameRenderer);
            }
            if (_padToPadJointTypeRendererMap.containsKey(pad) == false)
            {
              JointTypeRenderer jointTypeRenderer = new JointTypeRenderer(pad, false);
              graphicsEngine.addRenderer(padNameLayer, jointTypeRenderer);
              _padToPadJointTypeRendererMap.put(pad, jointTypeRenderer);
              addRendererForComponent(graphicsEngine, component, jointTypeRenderer);
            }
          }

          if (drawOrientationArrows)
          {
            if (_padToOrientationArrowRendererMap.containsKey(pad) == false && pad.getPinOrientationEnum().equals(PinOrientationEnum.NOT_APPLICABLE) == false)
            {
              OrientationArrowRenderer orientationArrowRenderer = new OrientationArrowRenderer(pad);
              graphicsEngine.addRenderer(orientationArrowLayer, orientationArrowRenderer);
              _padToOrientationArrowRendererMap.put(pad, orientationArrowRenderer);
              addRendererForComponent(graphicsEngine, component, orientationArrowRenderer);
            }
          }
        }
        else
        {
          if (_padToPadRendererMap.containsKey(pad))
          {
            Renderer renderer = _padToPadRendererMap.remove(pad);
            graphicsEngine.removeRenderer(renderer);
            removeRendererForComponent(component, renderer);
          }
          if (_padToPadNameRendererMap.containsKey(pad))
          {
            Renderer renderer = _padToPadNameRendererMap.remove(pad);
            graphicsEngine.removeRenderer(renderer);
            removeRendererForComponent(component, renderer);
          }
          if (_padToPadJointTypeRendererMap.containsKey(pad))
          {
            Renderer renderer = _padToPadJointTypeRendererMap.remove(pad);
            graphicsEngine.removeRenderer(renderer);
            removeRendererForComponent(component, renderer);
          }

          if (_padToOrientationArrowRendererMap.containsKey(pad))
          {
            Renderer renderer = _padToOrientationArrowRendererMap.remove(pad);
            graphicsEngine.removeRenderer(renderer);
            removeRendererForComponent(component, renderer);
          }
        }
      }
    }
  }

  /**
   * @author Lim, Seng Yew
   */
  public void createCadRenderers(GraphicsEngine graphicsEngine, boolean showTopSide, int testSubProgramId)
  {
    _currentTestSubProgramId = testSubProgramId;
    createCadRenderers(graphicsEngine, showTopSide);
    _currentTestSubProgramId = -1; // Reset after use
  }

  /**
   * @author George A. David
   */
  public void createCadRenderers(GraphicsEngine graphicsEngine, boolean showTopSide)
  {
    Assert.expect(graphicsEngine != null);

    if (_showCadGraphics == false)
      return;

    PanelRectangle visibleRect = null;
    for(int padLayer : _subProgramIdToPadLayer.values())
    {
      if(visibleRect == null)
        visibleRect = new PanelRectangle(graphicsEngine.getVisibleRectangleInRendererCoordinates(padLayer));
      else
        visibleRect.add(graphicsEngine.getVisibleRectangleInRendererCoordinates(padLayer));
    }

    visibleRect.setRect(visibleRect.getMinX() * _magnification.getNanoMetersPerPixel(),
                        visibleRect.getMinY() * _magnification.getNanoMetersPerPixel(),
                        visibleRect.getWidth() * _magnification.getNanoMetersPerPixel(),
                        visibleRect.getHeight() * _magnification.getNanoMetersPerPixel());
    createCadRenderers(graphicsEngine, visibleRect, showTopSide);
  }

  /**
   * @author George A. David
   */
  public void createCadRenderers(GraphicsEngine graphicsEngine, PanelRectangle visibleRect, boolean showTopSide, int testSubProgramId)
  {
    _currentTestSubProgramId = testSubProgramId;
    createCadRenderers(graphicsEngine, visibleRect, showTopSide);
    _currentTestSubProgramId = -1; // Reset after use
  }

  /**
   * @author George A. David
   */
  public void createCadRenderers(GraphicsEngine graphicsEngine, PanelRectangle visibleRect, boolean showTopSide)
  {
    Assert.expect(graphicsEngine != null);
    Assert.expect(visibleRect != null);

    if(_showCadGraphics == false)
      return;

    Rectangle2D visibleRect2d = visibleRect.getRectangle2D();

    boolean isPadLayerVisible = false;
    for(int padLayer : _subProgramIdToPadLayer.values())
    {
      isPadLayerVisible = isPadLayerVisible || graphicsEngine.isLayerVisible(padLayer);
    }

    boolean drawPadNames = false;
    if(_subProgramIdToPadNameLayer != null)
    {
      for(int padNameLayer : _subProgramIdToPadNameLayer.values())
      {
        drawPadNames = drawPadNames || graphicsEngine.isLayerVisible(padNameLayer);
      }
    }
    drawPadNames = _drawPadNames && drawPadNames;

    boolean drawOrientationArrows = false;
    if(_subProgramIdToOrientationArrowLayer != null)
    {
      for(int orientationArrowsLayer : _subProgramIdToOrientationArrowLayer.values())
      {
        drawOrientationArrows = drawOrientationArrows || graphicsEngine.isLayerVisible(orientationArrowsLayer);
      }
    }
    drawOrientationArrows = _drawOrientationArrows && drawOrientationArrows;


//    boolean drawRefDes = _drawRefDes && graphicsEngine.isZoomLevelBelowCreationThreshold(_rightRefDesLayer) == false;
    
    boolean drawRefDes = false;
    if(_subProgramIdToRefDesLayer != null)
    {
      for(int refDesLayer : _subProgramIdToRefDesLayer.values())
      {
        drawRefDes = drawRefDes || graphicsEngine.isZoomLevelBelowCreationThreshold(refDesLayer) == false;
      }
    }
    drawRefDes = _drawRefDes && drawRefDes;

    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    Set<com.axi.v810.business.panelDesc.Component> componentsNeedingRenderers;
    
    componentsNeedingRenderers = _componentUtil.getIntersectingComponentsInRectangle(visibleRect, showTopSide);

    Set<com.axi.v810.business.panelDesc.Component> newlyInvisibleComponents = new HashSet<com.axi.v810.business.panelDesc.Component>(_componentToRenderersMap.keySet());
    newlyInvisibleComponents.removeAll(componentsNeedingRenderers);
//    componentsNeedingRenderers.removeAll(_componentToRefDesRendererMap.keySet());

    for(com.axi.v810.business.panelDesc.Component component : componentsNeedingRenderers)
    {
      updateRefDesRenderer(component, drawRefDes, graphicsEngine);
      updatePadRenderers(component, isPadLayerVisible, drawPadNames, drawOrientationArrows, graphicsEngine, visibleRect2d);
    }

    for (com.axi.v810.business.panelDesc.Component component : newlyInvisibleComponents)
      clearComponentRenderers(component, graphicsEngine);

    // If we are using Panel Alignment, we will add Panel Fiducials.
    // If we are using Board Alignment, we are only interested in Board Fiducials
    Collection<Fiducial> fiducials;
    if (_project.getPanel().getPanelSettings().isPanelBasedAlignment())
        fiducials = _project.getPanel().getFiducials();
    else
        fiducials = new ArrayList<Fiducial>();
    Assert.expect(fiducials != null);
    
    for(Board board : _project.getPanel().getBoards())
      fiducials.addAll(board.getFiducials());

    for (Fiducial fiducial : fiducials)
    {
      Rectangle2D bounds = fiducial.getShapeRelativeToPanelInNanoMeters().getBounds2D();

      if ((visibleRect.contains(bounds) || visibleRect.intersects(bounds)) && fiducial.isTopSide() == showTopSide)
      {
        int refDesLayer = -1;
        int padLayer = -1;

        for(TestSubProgram testSubProgram : _project.getTestProgram().getAllTestSubPrograms())
        {
          if(testSubProgram.getRealVerificationRegionsBoundsInNanoMeters().intersects(bounds))
          {
            padLayer = _subProgramIdToPadLayer.get(testSubProgram.getId());
            if (_subProgramIdToRefDesLayer != null)
              refDesLayer = _subProgramIdToRefDesLayer.get(testSubProgram.getId());
            break;
          }
        }

//        Assert.expect(refDesLayer != -1);
        Assert.expect(padLayer != -1);

        if (_drawRefDes && _fiducialToRefDesRendererMap.containsKey(fiducial) == false)
        {
          FiducialReferenceDesignatorRenderer renderer = new FiducialReferenceDesignatorRenderer(fiducial);
          renderer.setInitialScaling(1.0 / (double) _magnification.getNanoMetersPerPixel());
          if (refDesLayer != -1)
            graphicsEngine.addRenderer(refDesLayer, renderer);
          _fiducialToRefDesRendererMap.put(fiducial, renderer);
        }

        if (isPadLayerVisible && _fiducialToFiducialRendererMap.containsKey(fiducial) == false)
        {
          RegionOfInterestRenderer renderer = new RegionOfInterestRenderer(_nanoMetersToPixelTransform.createTransformedShape(fiducial.getShapeRelativeToPanelInNanoMeters()));
          graphicsEngine.addRenderer(padLayer, renderer);
          _fiducialToFiducialRendererMap.put(fiducial, renderer);
        }
      }
      else
      {
        if (_fiducialToRefDesRendererMap.containsKey(fiducial))
          graphicsEngine.removeRenderer(_fiducialToRefDesRendererMap.remove(fiducial));

        if (_fiducialToFiducialRendererMap.containsKey(fiducial))
          graphicsEngine.removeRenderer(_fiducialToFiducialRendererMap.remove(fiducial));
      }
    }

    updateSelectedPads(graphicsEngine);

    graphicsEngine.repaint();
  }

  /**
   * @author George A. David
   */
  private void updateSelectedPads(GraphicsEngine graphicsEngine)
  {
    Assert.expect(graphicsEngine != null);

    if (_showSelectedPadsOfFiducials)
    {
      // ok, now highlight the pads of the component
      java.util.List<Renderer> renderersToHighlight = new LinkedList<Renderer>();
      for(Pad pad : _selectedPads)
      {
        if (_padToPadRendererMap.containsKey(pad))
        {
          ShapeRenderer renderer = _padToPadRendererMap.get(pad);
          renderer.setStroke(_selectedStroke);
          renderersToHighlight.add(renderer);
        }
      }

      for(Fiducial selectedFiducial : _selectedFiducials)
      {
        if (_fiducialToFiducialRendererMap.containsKey(selectedFiducial))
        {
          ShapeRenderer renderer = _fiducialToFiducialRendererMap.get(selectedFiducial);
          renderer.setStroke(_selectedStroke);
          renderersToHighlight.add(renderer);
        }
      }

      graphicsEngine.clearHighlightedRenderers();
      graphicsEngine.setHighlightedRenderers(renderersToHighlight, _selectedColor);
    }
    else
    {
      // we also need to deal with the ref des, but there is only 1 ref des renderer per component, so
      // we'll get the component out of the _selectedPad and do that outside the pad loop
      if (_selectedPads.isEmpty() == false)
      {
        com.axi.v810.business.panelDesc.Component selectedComponent = _selectedPads.get(0).getComponent();
        if (_componentToRefDesRendererMap.containsKey(selectedComponent))
        {
          ReferenceDesignatorRenderer renderer = _componentToRefDesRendererMap.remove(selectedComponent);
          graphicsEngine.removeRenderer(renderer);
          removeRendererForComponent(selectedComponent, renderer);
          JointTypeRenderer jointTypeRenderer = _componentToJointTypeRendererMap.remove(selectedComponent);
          graphicsEngine.removeRenderer(jointTypeRenderer);
          removeRendererForComponent(selectedComponent, jointTypeRenderer);
        }
      }
      for(Pad pad : _selectedPads)
      {
        // a selected pad has 3 attributes that are special: pad name, pad outline and orientation arrow
        if (_padToPadRendererMap.containsKey(pad))
        {
          ShapeRenderer renderer = _padToPadRendererMap.remove(pad);
          graphicsEngine.removeRenderer(renderer);
          removeRendererForComponent(pad.getComponent(), renderer);
        }
        if (_padToOrientationArrowRendererMap.containsKey(pad))
        {
          ShapeRenderer renderer = _padToOrientationArrowRendererMap.remove(pad);
          graphicsEngine.removeRenderer(renderer);
          removeRendererForComponent(pad.getComponent(), renderer);
        }
        if (_padToPadNameRendererMap.containsKey(pad))
        {
          PadNameRenderer renderer = _padToPadNameRendererMap.remove(pad);
          graphicsEngine.removeRenderer(renderer);
          removeRendererForComponent(pad.getComponent(), renderer);
        }
        if (_padToPadJointTypeRendererMap.containsKey(pad))
        {
          Renderer renderer = _padToPadJointTypeRendererMap.remove(pad);
          graphicsEngine.removeRenderer(renderer);
          removeRendererForComponent(pad.getComponent(), renderer);
        }
      }

      for(Fiducial selectedFiducial : _selectedFiducials)
      {
        if (_fiducialToFiducialRendererMap.containsKey(selectedFiducial))
        {
          ShapeRenderer renderer = _fiducialToFiducialRendererMap.remove(selectedFiducial);
          graphicsEngine.removeRenderer(renderer);
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  public void swapLayers()
  {
//    int tempLayerNumber = _rightImageLayer;
//    _rightImageLayer = _leftImageLayer;
//    _leftImageLayer = tempLayerNumber;
//
//    tempLayerNumber = _rightRefDesLayer;
//    _rightRefDesLayer = _leftRefDesLayer;
//    _leftRefDesLayer = tempLayerNumber;
//
//    tempLayerNumber = _rightPadLayer;
//    _rightPadLayer = _leftPadLayer;
//    _leftPadLayer = tempLayerNumber;
//
//    tempLayerNumber = _rightPadNameLayer;
//    _rightPadNameLayer = _leftPadNameLayer;
//    _leftPadNameLayer = tempLayerNumber;
//    MathUtil.swap(_rightImageLayer, _leftImageLayer);
//    MathUtil.swap(_rightRefDesLayer, _leftRefDesLayer);
//    MathUtil.swap(_rightPadLayer, _leftPadLayer);
//    MathUtil.swap(_rightPadNameLayer, _leftPadNameLayer);
  }

  /**
   * @author George A. David
   */
  public void updateComponentTypeData(GraphicsEngine graphicsEngine, ComponentType componentType, boolean isTopSide)
  {
    Assert.expect(componentType != null);

    PanelRectangle visibleRect = null;
    for(int padLayer : _subProgramIdToPadLayer.values())
    {
      if(visibleRect == null)
        visibleRect = new PanelRectangle(graphicsEngine.getVisibleRectangleInRendererCoordinates(padLayer));
      else
        visibleRect.add(graphicsEngine.getVisibleRectangleInRendererCoordinates(padLayer));
    }

    int widthInNanoMeters = visibleRect.getWidth() * _magnification.getNanoMetersPerPixel();
    int lengthInNanoMeters = visibleRect.getHeight() * _magnification.getNanoMetersPerPixel();
    visibleRect.setRect(visibleRect.getMinX() * _magnification.getNanoMetersPerPixel(),
                        visibleRect.getMinY() * _magnification.getNanoMetersPerPixel(),
                        widthInNanoMeters,
                        lengthInNanoMeters);

    for(com.axi.v810.business.panelDesc.Component component : componentType.getComponents())
    {
      _componentUtil.updateComponent(component);
      if(component.isTopSide() == isTopSide)
      {
        java.awt.Shape shape = component.getShapeSurroundingPadsRelativeToPanelInNanoMeters();
        if (shape.intersects(visibleRect.getRectangle2D()))
          updateComponentData(component, graphicsEngine);
      }
    }

    graphicsEngine.repaint();
  }

  /**
   * Update all visible renderers - useful when changing pin level orientation or joint types
   * @author Andy Mechtenberg
   * @author Chong, Wei Chin
   */
  public void updateVisibleData(GraphicsEngine graphicsEngine, boolean isTopSide)
  {
//    PanelRectangle visibleRect = new PanelRectangle(graphicsEngine.getVisibleRectangleInRendererCoordinates(_rightPadLayer));
//    visibleRect.add(graphicsEngine.getVisibleRectangleInRendererCoordinates(_leftPadLayer));
    PanelRectangle visibleRect = null;
    for(int padLayer : _subProgramIdToPadLayer.values())
    {
      if(visibleRect == null)
        visibleRect = new PanelRectangle(graphicsEngine.getVisibleRectangleInRendererCoordinates(padLayer));
      else
        visibleRect.add(graphicsEngine.getVisibleRectangleInRendererCoordinates(padLayer));
    }
    int widthInNanoMeters = visibleRect.getWidth() * _magnification.getNanoMetersPerPixel();
    int lengthInNanoMeters = visibleRect.getHeight() * _magnification.getNanoMetersPerPixel();
    visibleRect.setRect(visibleRect.getMinX() * _magnification.getNanoMetersPerPixel(),
                        visibleRect.getMinY() * _magnification.getNanoMetersPerPixel(),
                        widthInNanoMeters,
                        lengthInNanoMeters);

    for (com.axi.v810.business.panelDesc.Component component : _componentUtil.getIntersectingComponentsInRectangle(visibleRect, isTopSide))
    {
      if(component.isTopSide() == isTopSide)
      {
        java.awt.Shape shape = component.getShapeSurroundingPadsRelativeToPanelInNanoMeters();
        if (shape.intersects(visibleRect.getRectangle2D()))
          updateComponentData(component, graphicsEngine);
      }
    }
    graphicsEngine.repaint();
  }

  /**
   * @author Andy Mechtenberg
   */
  public Renderer getRendererForPad(Pad pad)
  {
    Assert.expect(pad != null);
    return _padToPadRendererMap.get(pad);
  }

  /**
   * @author George A. David
   */
  public void updateComponentData(com.axi.v810.business.panelDesc.Component component, GraphicsEngine graphicsEngine)
  {
    Assert.expect(component != null);
    Assert.expect(graphicsEngine != null);

    if (_componentToRefDesRendererMap.containsKey(component))
    {
      _componentToRefDesRendererMap.get(component).validateData();
      _componentToJointTypeRendererMap.get(component).validateData();
    }
    for(Pad pad : component.getPads())
    {
      java.awt.Shape padShape = pad.getShapeRelativeToPanelInNanoMeters();
      if (_padToPadRendererMap.containsKey(pad))
      {
        int padLayer = -1;
        for (TestSubProgram testSubProgram : _project.getTestProgram().getAllTestSubPrograms())
        {
          if(testSubProgram.isLowMagnification() && _magnification.equals(MagnificationEnum.H_NOMINAL))
              continue;
          if(testSubProgram.isHighMagnification() && _magnification.equals(MagnificationEnum.NOMINAL))
              continue;
          
          if (testSubProgram.getRealVerificationRegionsBoundsInNanoMeters().intersects(padShape.getBounds2D()))
          {
            padLayer = _subProgramIdToPadLayer.get(testSubProgram.getId());
            break;
          }
        }
        if (padLayer == -1)
          continue;
        
        //Ngie Xing, PCR-18, CAD not refresh after change orientation 
        //use transformed shape here and not before comparing with verification regions bounds
        padShape = _nanoMetersToPixelTransform.createTransformedShape(padShape);
        PadRegionOfInterestRenderer regionRenderer = new PadRegionOfInterestRenderer(padShape, pad);
        
        graphicsEngine.addRenderer(padLayer, regionRenderer);
        RegionOfInterestRenderer previousRenderer = _padToPadRendererMap.put(pad, regionRenderer);
        graphicsEngine.removeRenderer(previousRenderer);
        removeRendererForComponent(component, previousRenderer);
        addRendererForComponent(graphicsEngine, component, regionRenderer);
      }

      if (_padToPadNameRendererMap.containsKey(pad))
        _padToPadNameRendererMap.get(pad).validateData();

      if (_padToPadJointTypeRendererMap.containsKey(pad))
        _padToPadJointTypeRendererMap.get(pad).validateData();

      if (_padToOrientationArrowRendererMap.containsKey(pad))
        _padToOrientationArrowRendererMap.get(pad).validateData();
    }

    updateSelectedPads(graphicsEngine);
  }

  /**
   * @author George A. David
   */
  public void updateComponentData()
  {
    _componentUtil.setComponents(_project.getPanel().getComponents());
  }

  /**
   * @author George A. David
   */
  public Collection<com.axi.v810.business.panelDesc.Component> getSurroundingComponents(Point2D point, boolean isTopSide)
  {
    return _componentUtil.getSurroundingComponents((int)point.getX(), (int)point.getY(), isTopSide);
  }
}
