package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * <p>Title: GenerateImageSetWizardMainOptionsPanel/p>
 *
 * <p>Description: This panel is the main panel of the generate image set wizard.  This panel will present the user with all
 * the general image set options: Location to store image set , type of image set (panel, board, component, pin, joint type,
 * subtype), and if the panel is populated or not.</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: AXI</p>
 *
 * @author Erica Wheatcroft
 */
public class GenerateImageSetWizardMainOptionsPanel extends WizardPanel
{
  private JPanel _imageSetTypeSpecificationPanel;

  private Border border1 = BorderFactory.createEtchedBorder();
  private Border border2 = new TitledBorder(border1, StringLocalizer.keyToString("GISWK_TYPE_KEY"));

  private GridLayout _imageSetTypeSpecificationPanelGridLayout = new GridLayout();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout(5, 5);

  private JCheckBox _saveOnlyTestedDevicesCheckBox;
  private JCheckBox _saveLightestImagesCheckBox;
  private JCheckBox _saveFocusConfirmationImagesCheckBox;
  private ButtonGroup _imageSetTypeRadioButtonGroup;

  private JRadioButton _subTypeRadioButton;
  private JRadioButton _jointTypeRadioButton;
  private JRadioButton _pinRadioButton;
  private JRadioButton _componentRadioButton;
  private JRadioButton _boardRadioButton;
  private JRadioButton _panelRadioButton;

  private ButtonGroup _populatedRadioButtonGroup;
  private JRadioButton _populatedRadioButton;
  private JRadioButton _unpopulatedRadioButton;

  private ImageSetTypeEnum _selectedImageSetType = null;
  private boolean _populatedBoard = true;
  private ImageSetData _imageSetData = null;
  private GenerateImageSetWizardDialog _dialog = null;

  /**
   * @author Erica Wheatcroft
   */
  public GenerateImageSetWizardMainOptionsPanel(Project currentLoadedProject, ImageSetData imageSetData, WizardDialog dialog)
  {
    super(dialog);
    Assert.expect(currentLoadedProject != null, "project is null");
    Assert.expect(imageSetData != null, "image set data is null");
    Assert.expect(dialog != null, "Dialog is not null");
    _imageSetData = imageSetData;
    _dialog = (GenerateImageSetWizardDialog)dialog;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _imageSetData = null;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void removeObservers()
  {
    // do nothing
  }


  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    removeAll();
    setLayout(_mainPanelBorderLayout);
    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    _subTypeRadioButton = new JRadioButton();
    _jointTypeRadioButton = new JRadioButton();
    _pinRadioButton = new JRadioButton();
    _componentRadioButton = new JRadioButton();
    _boardRadioButton = new JRadioButton();
    _panelRadioButton = new JRadioButton();

    JPanel checkBoxPanel;
    if (Config.isDeveloperDebugModeOn())
      checkBoxPanel = new JPanel(new GridLayout(4, 1, 0, 5));
    else
      checkBoxPanel = new JPanel(new GridLayout(2, 1, 0, 5));
    checkBoxPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,0));
    _saveOnlyTestedDevicesCheckBox = new JCheckBox();
    _saveOnlyTestedDevicesCheckBox.setText(StringLocalizer.keyToString("GISK_SAVE_ONLY_TESTED_DEVICES_KEY"));
    _saveOnlyTestedDevicesCheckBox.setSelected(_imageSetData.generateImagesForNoTestAndNoLoadDevices());
    _saveLightestImagesCheckBox = new JCheckBox();
    _saveLightestImagesCheckBox.setText(StringLocalizer.keyToString("GISWK_GEN_ADDITIONAL_IMAGES_FOR_SHORT_KEY"));
    _saveLightestImagesCheckBox.setSelected(_imageSetData.collectLightestImages());

    _saveFocusConfirmationImagesCheckBox = new JCheckBox();
    _saveFocusConfirmationImagesCheckBox.setText(StringLocalizer.keyToString("GISWK_GEN_ADDITIONAL_IMAGES_FOR_FOCUS_CONFIRMATION_KEY"));
    _saveFocusConfirmationImagesCheckBox.setSelected(_imageSetData.collectFocusConfirmationImages());

    _populatedRadioButton = new JRadioButton();
    _unpopulatedRadioButton = new JRadioButton();
    _populatedRadioButton.setText(StringLocalizer.keyToString("GISWK_POPULATED_PANEL_KEY"));
    _unpopulatedRadioButton.setText(StringLocalizer.keyToString("GISWK_UNPOPULATED_PANEL_KEY"));
    JPanel populatedPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0,0));
    populatedPanel.add(_populatedRadioButton);
    populatedPanel.add(_unpopulatedRadioButton);
    _populatedRadioButtonGroup = new ButtonGroup();
    _populatedRadioButtonGroup.add(_populatedRadioButton);
    _populatedRadioButtonGroup.add(_unpopulatedRadioButton);

    if(_imageSetData.isBoardPopulated())
      _populatedRadioButton.setSelected(true);
    else
      _unpopulatedRadioButton.setSelected(true);

    checkBoxPanel.add(_saveOnlyTestedDevicesCheckBox);
    if (Config.isDeveloperDebugModeOn())
    {
      checkBoxPanel.add(_saveLightestImagesCheckBox);
      checkBoxPanel.add(_saveFocusConfirmationImagesCheckBox);
    }
    checkBoxPanel.add(populatedPanel);
    _imageSetTypeSpecificationPanel = new JPanel();
    _imageSetTypeSpecificationPanel.setBorder(BorderFactory.createCompoundBorder(border2, BorderFactory.createEmptyBorder(5, 25, 5, 5)));
    _imageSetTypeSpecificationPanel.setLayout(_imageSetTypeSpecificationPanelGridLayout);
    _imageSetTypeSpecificationPanelGridLayout.setColumns(2);
    _imageSetTypeSpecificationPanelGridLayout.setHgap(25);
    _imageSetTypeSpecificationPanelGridLayout.setRows(3);
    _imageSetTypeSpecificationPanelGridLayout.setVgap(15);
    _subTypeRadioButton.setText(StringLocalizer.keyToString("GISWK_SUBTYPE_TYPE_KEY"));
    _jointTypeRadioButton.setText(StringLocalizer.keyToString("GISWK_JOINTTYPE_TYPE_KEY"));
    _pinRadioButton.setText(StringLocalizer.keyToString("GISWK_PIN_TYPE_KEY"));
    _componentRadioButton.setText(StringLocalizer.keyToString("GISWK_COMPONENT_TYPE_KEY"));
    _boardRadioButton.setText(StringLocalizer.keyToString("GISWK_BOARD_KEY"));
    _panelRadioButton.setText(StringLocalizer.keyToString("GISWK_PANEL_TYPE_KEY"));
    if(_imageSetData != null && _imageSetData.hasImageSetTypeEnum())
    {
      _selectedImageSetType = _imageSetData.getImageSetTypeEnum();
      if(_selectedImageSetType.equals(ImageSetTypeEnum.PANEL))
      {
        _panelRadioButton.setSelected(true);
      }
      else if(_selectedImageSetType.equals(ImageSetTypeEnum.BOARD))
      {
        _boardRadioButton.setSelected(true);
      }
      else if(_selectedImageSetType.equals(ImageSetTypeEnum.COMPONENT))
      {
        _componentRadioButton.setSelected(true);
      }
      else if(_selectedImageSetType.equals(ImageSetTypeEnum.JOINT_TYPE))
      {
        _jointTypeRadioButton.setSelected(true);
      }
      else if(_selectedImageSetType.equals(ImageSetTypeEnum.PIN))
      {
        _pinRadioButton.setSelected(true);
      }
      else if(_selectedImageSetType.equals(ImageSetTypeEnum.SUBTYPE))
      {
        _subTypeRadioButton.setSelected(true);
      }
      else
        Assert.expect(false);
    }
    else
    {
      _panelRadioButton.setSelected(true);
      _selectedImageSetType = ImageSetTypeEnum.PANEL;
      _imageSetData.setImageSetTypeEnum(_selectedImageSetType);
    }

    JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
    panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    JLabel instructionLabel = new JLabel(StringLocalizer.keyToString("GISK_INSTRUCTION_LABEL_KEY"));
    instructionLabel.setFont(FontUtil.getBoldFont(instructionLabel.getFont(),Localization.getLocale()));
    panel.add(instructionLabel);

    add(panel, BorderLayout.NORTH);
    add(checkBoxPanel, BorderLayout.SOUTH);
    add(_imageSetTypeSpecificationPanel, BorderLayout.CENTER);

    // add the radiobuttons
    _imageSetTypeSpecificationPanel.add(_panelRadioButton);
    _imageSetTypeSpecificationPanel.add(_boardRadioButton);
    _imageSetTypeSpecificationPanel.add(_componentRadioButton);
    _imageSetTypeSpecificationPanel.add(_pinRadioButton);
    _imageSetTypeSpecificationPanel.add(_jointTypeRadioButton);
    _imageSetTypeSpecificationPanel.add(_subTypeRadioButton);

    // add the buttons to the group
    _imageSetTypeRadioButtonGroup = new ButtonGroup();
    _imageSetTypeRadioButtonGroup.add(_panelRadioButton);
    _imageSetTypeRadioButtonGroup.add(_boardRadioButton);
    _imageSetTypeRadioButtonGroup.add(_componentRadioButton);
    _imageSetTypeRadioButtonGroup.add(_pinRadioButton);
    _imageSetTypeRadioButtonGroup.add(_jointTypeRadioButton);
    _imageSetTypeRadioButtonGroup.add(_subTypeRadioButton);

    // listeners
    _panelRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedImageSetType = ImageSetTypeEnum.PANEL;
        validatePanel();
      }
    });
    _boardRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedImageSetType = ImageSetTypeEnum.BOARD;
        validatePanel();
      }
    });
    _componentRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedImageSetType = ImageSetTypeEnum.COMPONENT;
        validatePanel();
      }
    });
    _pinRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedImageSetType = ImageSetTypeEnum.PIN;
        validatePanel();
      }
    });
    _jointTypeRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedImageSetType = ImageSetTypeEnum.JOINT_TYPE;
        validatePanel();
      }
    });
    _subTypeRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedImageSetType = ImageSetTypeEnum.SUBTYPE;
        validatePanel();
      }
    });
    _populatedRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _populatedBoard = true;
        validatePanel();
      }
    });
    _unpopulatedRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _populatedBoard = false;
        validatePanel();
      }
    });
    _saveOnlyTestedDevicesCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _imageSetData.setGenerateImagesForNoTestAndNoLoadDevices(_saveOnlyTestedDevicesCheckBox.isSelected());
        validatePanel();
      }
    });
    _saveLightestImagesCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _imageSetData.setCollectLightestImages(_saveLightestImagesCheckBox.isSelected());
        validatePanel();
      }
    });

    _saveFocusConfirmationImagesCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _imageSetData.setCollectFocusConfirmationImages(_saveFocusConfirmationImagesCheckBox.isSelected());
        validatePanel();
      }
    });

  }


  /**
   * @author Erica Wheatcroft
   */
  public void populatePanelWithData()
  {
    _populatedBoard = _imageSetData.isBoardPopulated();

    createPanel();

    // set the title
    _dialog.changeTitle(StringLocalizer.keyToString("GISWK_SCREEN_2_TITLE_KEY"));

    // now call validate to see if the panel has all the info needed to go to the next step.
    validatePanel();

  }

  /**
   * This method will check to make sure all the data is specified and if so then will fire an update event to the
   * dialog to update the buttons correctly. This method will also specify the next and previous pointers
   *
   * @author Erica Wheatcroft
   */
  public void validatePanel()
  {
    // type must be set inorder for the panel to be valid.

    if(_selectedImageSetType != null)
    {
      _imageSetData.setBoardPopulated( _populatedBoard);
      _imageSetData.setImageSetTypeEnum(_selectedImageSetType);
      
      // XCR-3881 System Crash When Collect Fine Tuning Image on Multi-Long Board
      if(_selectedImageSetType.equals(ImageSetTypeEnum.PANEL))
      {
        _imageSetData.setCollectImagesForPanel(true);
        _imageSetData.setCollectImagesOnAllBoardInstances(true);
      }

      // next can be set to enabled, cancel can be enabled, back is disabled, and generate is disabled.
      WizardButtonState buttonState = new WizardButtonState(true, true, false, true);
      _observable.updateButtonState(buttonState);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null, "Name of panel is null");
    Assert.expect(name.length() > 0, "Name of panel is an empty string");
    _name = name;
  }

  /**
   * @author Erica Wheatcroft
   */
  public String getWizardName()
  {
    Assert.expect(_name != null, "Name of panel is not set");
    return _name;
  }

}
