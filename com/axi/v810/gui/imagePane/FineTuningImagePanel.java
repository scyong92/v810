package com.axi.v810.gui.imagePane;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.algoTuner.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

//Lim, Lay Ngor - XCR1652 - Image Post Processing
import com.axi.v810.business.panelSettings.*;
import com.axi.util.image.*;
import com.axi.v810.datastore.config.*;

/**
 * This class is the main panel for the image pane display during fine tuning.
 * @author George A. David
 */
public class FineTuningImagePanel extends JPanel
{
  private FineTuningImageGraphicsEngineSetup _fineTuningImageGraphicsEngineSetup = null;
  //Anthony01005
  private FineTuningPanel _fineTuningPanel = null;
  private GraphicsEngine _graphicsEngine = null;
  
  //Khaw Chek Hau - XCR2366: Hide/Show Sharpness Profile based on config setting
  private static Config _config = Config.getInstance();

  private JToolBar _imageAdjustmentToolBar = new JToolBar();
  private JButton _saveImageButton = new JButton(); 
  private JButton _saveImageInComponentAndPadNameButton = new JButton();
  private JButton _saveImageInAllSliceButton = new JButton();
  private JButton _saveDiagnosticImageButton = new JButton(); 
  //Ying-Huan.Chu
  private JToggleButton _toggle2DImageButton = new JToggleButton();
  private JButton _previousImageButton = new JButton();
  private JButton _nextImageButton = new JButton();

  private final int _SAVE_IN_NORMAL_MODE = 1;
  private final int _SAVE_IN_COMPONENT_PAD_NAME_MODE = 2;
  private final int _SAVE_IN_OVERALL_SLICE_MODE = 3;
  
  private JFileChooser _fileChooser = null;
  private String _fileFullPath = Directory.getLogDir();
  private AlgoTunerGui _algoTunerGui = null;
  //Ying-Huan.Chu
  private JButton _viewSharpnessProfileButton = new JButton();
  
  //Lim, Lay Ngor - XCR1652 - Image Post Processing - START      
  private JPanel _northPanel = new JPanel();  
  private ImageProcessingToolBar _imageProcessingToolBar;
  private JToggleButton _imageProcessingToggleButton = new JToggleButton();
  //Lim, Lay Ngor - XCR1652 - Image Post Processing - END  

  // bee-hoon.goh
  private JToggleButton _toggleViewComponentImageButton = new JToggleButton();
  //Siew Yeng - XCR-2761
  private JToggleButton _toggleViewEnhancedComponentImageButton = new JToggleButton();
  private ImageManagerToolBar _toolBar;

  /**
   * @author George A. David
   */
  public FineTuningImagePanel(ImageManagerToolBar toolBar)
  {
    _fineTuningImageGraphicsEngineSetup = new FineTuningImageGraphicsEngineSetup(toolBar);
    _graphicsEngine = _fineTuningImageGraphicsEngineSetup.getGraphicsEngine();
    _toolBar = toolBar;
    jbInit();
  }

  public void saveAlgoTunerInstance(AlgoTunerGui algoTunerGui)
  {
    _algoTunerGui = algoTunerGui;
  }

  /**
   * @author George A. David
   */
  private void jbInit()
  {
    //Lim, Lay Ngor - XCR1652 - Image Post Processing - START
    Assert.expect(_fineTuningImageGraphicsEngineSetup != null);
    //Fine tuning image panel must use renderer panel to get previuos post processing setting instead of image layer.
    _imageProcessingToolBar = new ImageProcessingToolBar(_graphicsEngine, 0);
    //Lim, Lay Ngor - XCR1652 - Image Post Processing - END
    
    createImageAdjustmentToolBar();
    setLayout(new BorderLayout());
    add(_northPanel, BorderLayout.NORTH);//Lim, Lay Ngor - XCR1652 - Image Post Processing
    add(_graphicsEngine, BorderLayout.CENTER);
    
    //Lim, Lay Ngor - XCR1652 - Image Post Processing - START       
    _northPanel.setBackground(Color.BLACK);
    _northPanel.setLayout(new BorderLayout());
    _northPanel.add(_imageAdjustmentToolBar, BorderLayout.NORTH);
    _northPanel.add(_imageProcessingToolBar, BorderLayout.SOUTH);
    //Lim, Lay Ngor - XCR1652 - Image Post Processing - END  
    
    //Anthony01005
    _fineTuningImageGraphicsEngineSetup.setFineTuningImagePanel(this);
  }

  /**
   * @author Chong, Wei Chin
   */
  private void createImageAdjustmentToolBar()
  {
    _saveImageButton.setIcon(Image5DX.getImageIcon(Image5DX.SAVE_FILE));
    _saveImageButton.setToolTipText(StringLocalizer.keyToString("DCGUI_SAVE_IMAGE_KEY"));
    _saveImageButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        saveImageButton_actionPerformed();
      }
    });
    
    _saveImageInComponentAndPadNameButton.setIcon(Image5DX.getImageIcon(Image5DX.SAVE_SCREEN));//Lim, Lay Ngor - XCR1652 - Image Post Processing
    _saveImageInComponentAndPadNameButton.setToolTipText(StringLocalizer.keyToString("DCGUI_SAVE_COMPONENT_AND_PAD_NAME_IMAGE_KEY"));
    _saveImageInComponentAndPadNameButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        saveImageInComponentAndPadNameButton_actionPerformed();
      }
    });
    
    _saveImageInAllSliceButton.setIcon(Image5DX.getImageIcon(Image5DX.SAVE_ALL_FILE));//Lim, Lay Ngor - XCR1652 - Image Post Processing
    _saveImageInAllSliceButton.setToolTipText(StringLocalizer.keyToString("DCGUI_SAVE_ALL_SLICE_IMAGE_KEY"));
    _saveImageInAllSliceButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        saveImageInAllSlicesButton_actionPerformed();
      }
    });
    
    _saveDiagnosticImageButton.setIcon(Image5DX.getImageIcon(Image5DX.SAVE_DIAGNOSTIC_IMAGE));
    _saveDiagnosticImageButton.setToolTipText(StringLocalizer.keyToString("DCGUI_SAVE_DIAGNOSTIC_IMAGE_KEY"));
    _saveDiagnosticImageButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        saveDiagnosticImageButton_actionPerformed();
      }
    });
    
    //YHChu
    _toggle2DImageButton.setToolTipText(StringLocalizer.keyToString("DCGUI_TOGGLE_2D_IMAGE_KEY"));
    _toggle2DImageButton.setIcon(Image5DX.getImageIcon(Image5DX.TOGGLE_2D_IMAGE));
    _toggle2DImageButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
          toggle2DImageButton_actionPerformed();
      }
    });
    
    _previousImageButton.setToolTipText(StringLocalizer.keyToString("DCGUI_PREVIOUS_2D_IMAGE_KEY"));
    _previousImageButton.setIcon(Image5DX.getImageIcon(Image5DX.PREVIOUS_IMAGE));
    _previousImageButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        previousImageButton_actionPerformed();
      }
    });
    
    _nextImageButton.setToolTipText(StringLocalizer.keyToString("DCGUI_NEXT_2D_IMAGE_KEY"));
    _nextImageButton.setIcon(Image5DX.getImageIcon(Image5DX.NEXT_IMAGE));
    _nextImageButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        nextImageButton_actionPerformed();
      }
    });

    //Ying-Huan.Chu
    _viewSharpnessProfileButton.setToolTipText(StringLocalizer.keyToString("SHARPNESS_PROFILE_TOOLTIP_KEY"));
    _viewSharpnessProfileButton.setIcon(Image5DX.getImageIcon(Image5DX.SHARPNESS_PROFILE));
    _viewSharpnessProfileButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        calculateSharpnessProfileButton_actionPerformed();
      }
    });
    
	// bee-hoon.goh 
    _toggleViewComponentImageButton.setToolTipText(StringLocalizer.keyToString("DCGUI_STITCH_COMPONENT_IMAGES_TOOLTIP_KEY"));
    _toggleViewComponentImageButton.setIcon(Image5DX.getImageIcon(Image5DX.STITCH_COMPONENT_IMAGES));
    _toggleViewComponentImageButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        toggleViewComponentImageButton_actionPerformed();
      }
    });
    
    // Siew Yeng - XCR-2761
    _toggleViewEnhancedComponentImageButton.setToolTipText(StringLocalizer.keyToString("DCGUI_STITCH_ENHANCED_COMPONENT_IMAGES_TOOLTIP_KEY"));
    _toggleViewEnhancedComponentImageButton.setIcon(Image5DX.getImageIcon(Image5DX.STITCH_ENHANCED_COMPONENT_IMAGES));
    _toggleViewEnhancedComponentImageButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        toggleViewEnhancedComponentImageButton_actionPerformed();
      }
    });
	
    //Lim, Lay Ngor - XCR1652 - Image Post Processing - START
    _imageProcessingToggleButton.setIcon(Image5DX.getImageIcon(Image5DX.TOGGLE_POST_PROCESSING_SMALL));
    _imageProcessingToggleButton.setToolTipText(StringLocalizer.keyToString("IMTB_IMAGE_PROCESSING_KEY"));
    _imageProcessingToggleButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        imageProcessingToggleButton_actionPerformed();
      }
    });
   //Lim, Lay Ngor - XCR1652 - Image Post Processing - END    
    
    _imageAdjustmentToolBar.add(_saveImageButton);
    _imageAdjustmentToolBar.add(_saveImageInComponentAndPadNameButton);
    _imageAdjustmentToolBar.add(_saveImageInAllSliceButton);
    _imageAdjustmentToolBar.add(_saveDiagnosticImageButton);
    //YHChu
    _imageAdjustmentToolBar.addSeparator();
    _imageAdjustmentToolBar.add(_toggle2DImageButton);
    _imageAdjustmentToolBar.add(_previousImageButton);
    _imageAdjustmentToolBar.add(_nextImageButton);
    _imageAdjustmentToolBar.addSeparator();
    
    //Khaw Chek Hau - XCR2366: Hide/Show Sharpness Profile based on config setting
    if (_config.getBooleanValue(SoftwareConfigEnum.GENERATE_IMAGE_WITH_SHARPNESS_INFO))
      _imageAdjustmentToolBar.add(_viewSharpnessProfileButton);
    
	//bee-hoon.goh
    _imageAdjustmentToolBar.addSeparator();
    _imageAdjustmentToolBar.add(_toggleViewComponentImageButton);
    _imageAdjustmentToolBar.add(_toggleViewEnhancedComponentImageButton);// Siew Yeng - XCR-2761
	
    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    _imageAdjustmentToolBar.add(_imageProcessingToggleButton);
    //Change from Vertical to Horizontal
    _imageAdjustmentToolBar.setOrientation(JToolBar.HORIZONTAL);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setAllowToggle2DImageButton(boolean enabled)
  {
    _toggle2DImageButton.setEnabled(enabled);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void setEnablePrevious2DImageButton(boolean enabled)
  {
    _previousImageButton.setEnabled(enabled);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setEnableNext2DImageButton(boolean enabled)
  {
    _nextImageButton.setEnabled(enabled);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setEnableSharpnessProfileButton(boolean enabled)
  {
    _viewSharpnessProfileButton.setEnabled(enabled);
  }
  /*
   * Kee, chin Seong - This save all slice button and component and pad name button
   *                   only enable when NOT in run test tab.
   */
  public void setSaveAllSliceAndComponentPadButtonEnabled(boolean enabled)
  {
    _saveImageInComponentAndPadNameButton.setEnabled(enabled);
    _saveImageInAllSliceButton.setEnabled(enabled);
  }

  /**
   * @author George A. David
   */
  public FineTuningImageGraphicsEngineSetup getFineTuningImageGraphicsEngineSetup()
  {
    Assert.expect(_fineTuningImageGraphicsEngineSetup != null);
    return _fineTuningImageGraphicsEngineSetup;
  }

  /**
   * @author Anthony Fong //Anthony01005
   */
  public void setFineTuningPanel(FineTuningPanel fineTuningPanel)
  {
    Assert.expect(fineTuningPanel != null);
    _fineTuningPanel = fineTuningPanel;
  }
  /**
   * @author Anthony Fong //Anthony01005
   */
  public FineTuningPanel getFineTuningPanel()
  {
    Assert.expect(_fineTuningPanel != null);
    return _fineTuningPanel;
  }

  /**
   * @author George A. David
   */
  void display()
  {
    Assert.expect(_fineTuningImageGraphicsEngineSetup != null);
    _fineTuningImageGraphicsEngineSetup.display();
  }

  /**
   * @author Andy Mechtenberg
   */
  void unpopulate()
  {
    Assert.expect(_fineTuningImageGraphicsEngineSetup != null);
    _fineTuningImageGraphicsEngineSetup.unpopulate();
  }

  /**
   * @author George A. David
   */
  void finish()
  {
    Assert.expect(_fineTuningImageGraphicsEngineSetup != null);
    _fineTuningImageGraphicsEngineSetup.finish();
  }

    /**
   * @author Chong, Wei Chin
   */
  void saveImageButton_actionPerformed()
  {
    Assert.expect(_fineTuningImageGraphicsEngineSetup != null);
    if(_fineTuningImageGraphicsEngineSetup.isImageDisplayed())
    {
      saveImage(_SAVE_IN_NORMAL_MODE);
    }
  }
  
   /**
   * @author Chin Seong, Kee
   */
  void saveImageInComponentAndPadNameButton_actionPerformed()
  {
    Assert.expect(_fineTuningImageGraphicsEngineSetup != null);
    if(_fineTuningImageGraphicsEngineSetup.isImageDisplayed())
    {
      saveImage(_SAVE_IN_COMPONENT_PAD_NAME_MODE);
    }
  }
  
  void saveImageInAllSlicesButton_actionPerformed()
  {
    Assert.expect(_fineTuningImageGraphicsEngineSetup != null);
    if(_fineTuningImageGraphicsEngineSetup.isImageDisplayed())
    {
      saveImage(_SAVE_IN_OVERALL_SLICE_MODE);
    }
  }
  
   /**
   * @author Jack Hwee
   */
  void saveDiagnosticImageButton_actionPerformed()
  {
    Assert.expect(_fineTuningImageGraphicsEngineSetup != null);
    if(_fineTuningImageGraphicsEngineSetup.isImageDisplayed())
    {
      saveDiagnosticImage();
    }
  }
  
  /**
  * @author Ying-Huan.Chu
  */
  void toggle2DImageButton_actionPerformed()
  {
    Assert.expect(_fineTuningImageGraphicsEngineSetup != null);
    if (_toggle2DImageButton.isSelected())
    {
      // bee-hoon.goh - Disable Whole Component Image button
      setEnableToggleViewComponentImageButton(false);     
      _fineTuningImageGraphicsEngineSetup.display2DImage();
    }
    else
    {
      // bee-hoon.goh
      setEnableToggleViewComponentImageButton(true);  
      _fineTuningImageGraphicsEngineSetup.displayOriginalImage();     
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  void setToggle2DImageButtonSelected(boolean isSelected)
  {
    _toggle2DImageButton.setSelected(isSelected);
  }
  
  /**
  * @author Ying-Huan.Chu
  */
  void previousImageButton_actionPerformed()
  {
    Assert.expect(_fineTuningImageGraphicsEngineSetup != null);
    _fineTuningImageGraphicsEngineSetup.displayNextOrPrevious2DImage(false);
  }
  
  /**
  * @author Ying-Huan.Chu
  */
  void nextImageButton_actionPerformed()
  {
    Assert.expect(_fineTuningImageGraphicsEngineSetup != null);
    _fineTuningImageGraphicsEngineSetup.displayNextOrPrevious2DImage(true);
  }
  
  /**
  * @author Ying-Huan.Chu
  */
  void calculateSharpnessProfileButton_actionPerformed()
  {
    Assert.expect(_fineTuningImageGraphicsEngineSetup != null);
    _fineTuningImageGraphicsEngineSetup.displaySharpnessProfile();
  }
  
  /**
  * @author bee-hoon.goh
  */
  void toggleViewComponentImageButton_actionPerformed()
  {
    Assert.expect(_fineTuningImageGraphicsEngineSetup != null);
    if (_toggleViewComponentImageButton.isSelected())
    {
      //Siew Yeng - XCR-2761 - deselect enhanced component image if whole component button is selected
      setToggleViewEnhancedComponentImageButtonSelected(false); 
      _fineTuningImageGraphicsEngineSetup.displayComponentImage(false); 
      // disable 2D button if contain 2D images
      if(_fineTuningImageGraphicsEngineSetup.has2DImages())
        setAllowToggle2DImageButton(false);
      setToolBarEnabled(false);
      _graphicsEngine.setMouseEventsEnabled(false);        
    }
    else
    {
      _fineTuningImageGraphicsEngineSetup.displayOriginalImagePadSelectionImage();
       // Enable 2D button if contain 2D images
      if(_fineTuningImageGraphicsEngineSetup.has2DImages())
        setAllowToggle2DImageButton(true);
      setToolBarEnabled(true);
      _graphicsEngine.setMouseEventsEnabled(true);
    }  
  }
  
  /**
  * @author Siew Yeng
  */
  void toggleViewEnhancedComponentImageButton_actionPerformed()
  {
    Assert.expect(_fineTuningImageGraphicsEngineSetup != null);
    if (_toggleViewEnhancedComponentImageButton.isSelected())
    {
      //deselect whole component button when whole component enhanced image is selected
      setToggleViewComponentImageButtonSelected(false);
      _fineTuningImageGraphicsEngineSetup.displayComponentImage(true); 
      // disable 2D button if contain 2D images
      if(_fineTuningImageGraphicsEngineSetup.has2DImages())
        setAllowToggle2DImageButton(false);
      setToolBarEnabled(false);
      _graphicsEngine.setMouseEventsEnabled(false);        
    }
    else
    {
      _fineTuningImageGraphicsEngineSetup.displayOriginalImagePadSelectionImage();
       // Enable 2D button if contain 2D images
      if(_fineTuningImageGraphicsEngineSetup.has2DImages())
        setAllowToggle2DImageButton(true);
      setToolBarEnabled(true);
      _graphicsEngine.setMouseEventsEnabled(true);
    }  
  }
  
  /**
   * @author bee-hoon.goh
   */
  public void setEnableToggleViewComponentImageButton(boolean enabled)
  {
    _toggleViewComponentImageButton.setEnabled(enabled);
    _toggleViewEnhancedComponentImageButton.setEnabled(enabled);//Siew Yeng - XCR-2761
  }
  
  /**
   * @author bee-hoon.goh
   */
  void setToggleViewComponentImageButtonSelected(boolean isSelected)
  {
    _toggleViewComponentImageButton.setSelected(isSelected);
  }
  
  /**
   * @author bee-hoon.goh
   */
  boolean isToggleViewComponentImageButtonSelected()
  {
    return _toggleViewComponentImageButton.isSelected();
  }
  
  /**
   * @author Siew Yeng
   */
  void setToggleViewEnhancedComponentImageButtonSelected(boolean isSelected)
  {
    _toggleViewEnhancedComponentImageButton.setSelected(isSelected);
  }
  
  /**
   * @author Siew Yeng
   */
  boolean isToggleViewEnhancedComponentImageButtonSelected()
  {
    return _toggleViewEnhancedComponentImageButton.isSelected();
  }
  
  /**
   * This method displays a file chooser and return the directory selected by the user.
   * @return String
   *
   * @author Wei Chin, Chong
   */
  private void saveImage(final int saveMode)
  {
    File outputDirectory = null;
    
    if(saveMode == _SAVE_IN_NORMAL_MODE)
    _fileFullPath = _fileFullPath + File.separator + _fineTuningImageGraphicsEngineSetup.getCurrentImageRegion().getName() + ".jpg";
    else if(saveMode == _SAVE_IN_COMPONENT_PAD_NAME_MODE)
      _fileFullPath = _fileFullPath + File.separator + _fineTuningImageGraphicsEngineSetup.getCurrentJointName() + "_" +
                      _fineTuningImageGraphicsEngineSetup.getCurrentSliceNameEnum() + ".jpg";
    else if(saveMode == _SAVE_IN_OVERALL_SLICE_MODE)
      _fileFullPath = _fileFullPath + File.separator + _fineTuningImageGraphicsEngineSetup.getCurrentJointName() + "_";
    
    outputDirectory = new File(_fileFullPath);

    if ( _fileChooser == null )
    {
       _fileChooser = new JFileChooser();

       // initialize the JFIle Dialog.
       FileFilterUtil fileFilter = new FileFilterUtil("jpg", "Jpeg only");
       _fileChooser.setAcceptAllFileFilterUsed(false);
       _fileChooser.setFileFilter(fileFilter);

       _fileChooser.setFileHidingEnabled(false);
       _fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
       _fileChooser.setSelectedFile(outputDirectory);
       _fileChooser.setCurrentDirectory(outputDirectory);
    }
    else
    {
      _fileChooser.setSelectedFile(outputDirectory);
      _fileChooser.setCurrentDirectory(outputDirectory);
    }

    if ( _fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION )
    {
      File selectedFile = _fileChooser.getSelectedFile();

      if(saveMode == _SAVE_IN_NORMAL_MODE || saveMode == _SAVE_IN_COMPONENT_PAD_NAME_MODE)
      {
      _fileFullPath = selectedFile.getPath();
      int index = _fileFullPath.lastIndexOf(".");
      String extension = _fileFullPath.substring(index, _fileFullPath.length());

      if( extension.equalsIgnoreCase(".jpg") == false)
      {
        _fileFullPath += ".jpg";
      }
      
      _fineTuningImageGraphicsEngineSetup.saveBufferedImageForImagePanel(_fileFullPath);
      }
      else if(saveMode == _SAVE_IN_OVERALL_SLICE_MODE)
         _fineTuningImageGraphicsEngineSetup.saveAllSliceBufferedImageForCurrentSelectedPad(selectedFile.getPath());
    }
  }
  
   /**
   * This method displays a file chooser and return the directory selected by the user.
   * @return String
   *
   * @author Jack Hwee
   */
  private void saveDiagnosticImage()
  {
    File outputDirectory = null;
    
    _fileFullPath = _fileFullPath + File.separator + _fineTuningImageGraphicsEngineSetup.getCurrentImageRegion().getName() + ".jpg";
    outputDirectory = new File(_fileFullPath);

    if ( _fileChooser == null )
    {
       _fileChooser = new JFileChooser();

       // initialize the JFIle Dialog.
       FileFilterUtil fileFilter = new FileFilterUtil("jpg", "Jpeg only");
       _fileChooser.setAcceptAllFileFilterUsed(false);
       _fileChooser.setFileFilter(fileFilter);

       _fileChooser.setFileHidingEnabled(false);
       _fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
       _fileChooser.setSelectedFile(outputDirectory);
       _fileChooser.setCurrentDirectory(outputDirectory);
    }
    else
    {
      _fileChooser.setSelectedFile(outputDirectory);
      _fileChooser.setCurrentDirectory(outputDirectory);
    }

    if ( _fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION )
    {
      File selectedFile = _fileChooser.getSelectedFile();

      _fileFullPath = selectedFile.getPath();
      int index = _fileFullPath.lastIndexOf(".");
      String extension = _fileFullPath.substring(index, _fileFullPath.length());

      if( extension.equalsIgnoreCase(".jpg") == false)
      {
        _fileFullPath += ".jpg";
      }
      
      _fineTuningImageGraphicsEngineSetup.saveDiagnosticBufferedImageForImagePanel(_fileFullPath);
    }
  }
  
  /**
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */
 public void setEnableImageEnhancerButton(boolean enabled)
  {
    _imageProcessingToggleButton.setEnabled(enabled);
  }
    
  /**
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */   
  void imageProcessingToggleButton_actionPerformed()
  {   
    //To let ImagePaneToolbar control the visibility of this toolBar.
    _imageProcessingToolBar.setVisible(_imageProcessingToggleButton.isSelected());
  }
  
  /**
   * Update the current subtype and previous image renderer for post processing use.
   * @param subtype - Use for retrieving the component size for image enhancer block radius use.
   * @param previousImageRenderer - Use for retrieving the previous image renderer pointer to get the previous 
   *                                                         image BrightnessAndContrast Setting so that we can apply the same setting
   *                                                         to current image.
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */  
 public void updateCurrentSubtypeAndRenderer(Subtype subtype, InspectionImageRenderer previousImageRenderer)
  {
    _imageProcessingToolBar.setCurrentSubtypeAndRenderer(subtype, previousImageRenderer);  
  }  
 
 /*
  * bee-hoon.goh
  */
 private void setToolBarEnabled(boolean enabled)
  {
    _toolBar.setAllowDragGraphics(enabled);
    _toolBar.setAllowResetGraphics(enabled);
    _toolBar.setAllowZoomIn(enabled);
    _toolBar.setAllowZoomOut(enabled);
    _toolBar.setAllowZoomRectangle(enabled);
  }
 
 /**
  * To get and return the current Gui selected image enhancer list.
  * @return Current GUI selected image enhancer list.
  * @author Lim, Lay Ngor - XCR2126 - Save with Post Processing
  */
  public java.util.List<ImageEnhancerBase> getCurrentGuiImageEnhancerList()
  {
    return _imageProcessingToolBar.getImageEnhancerList();
  } 
}
