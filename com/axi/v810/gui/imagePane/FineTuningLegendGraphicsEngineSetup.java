package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.geom.*;
import java.lang.reflect.*;
import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * The FineTuningLegendGraphicsEngineSetup class sets up the graphics engine
 * for displaying profile legends from the algorithms.
 * @author George Booth
 */
public class FineTuningLegendGraphicsEngineSetup implements Observer
{
  private static final int _BORDER_SIZE_IN_PIXELS = 2;

  private final int _INITIAL_INSERTION_HEIGHT = 5;
  private int _maxWidthOfLegend = 0;
  private int _insertionHeight = _INITIAL_INSERTION_HEIGHT;

  private GraphicsEngine _graphicsEngine;
  private InspectionEventObservable _inspectionObservable = null;

  // Graphic engine layers
  private int _backgroundLayer;
  private int _backgroundPixelsLayer;
  private int _solderAreaLayer;
  private int _expectedImageLayer;
  private int _voidingLayer;
  private int _largestVoidingLayer;
  private int _wettingCoverageLayer;
  private int _textLayer;
  private java.util.List<Integer> _allGraphicLayers = new LinkedList<Integer>();

  Map<MeasurementRegionEnum, Integer> _measurementRegionEnumToGraphicsLayerNumberMap = new HashMap<MeasurementRegionEnum, Integer>();

  // current list of measurement legends being displayed
  java.util.List<Pair <String, Integer>> _measurementLegends = new ArrayList<Pair <String, Integer>>();

  /**
   * @author George Booth
   */
  public FineTuningLegendGraphicsEngineSetup()
  {
    _graphicsEngine = new GraphicsEngine(_BORDER_SIZE_IN_PIXELS);
    _graphicsEngine.setBackground(Color.lightGray);
    _graphicsEngine.setShouldGraphicsBeFitToScreen(false);
    _graphicsEngine.setAxisInterpretation(true, true);
    _graphicsEngine.setZoomEnabled(false);
    _inspectionObservable = InspectionEventObservable.getInstance();
    createLayers();
    setColors();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void display()
  {
    _inspectionObservable.getInstance().addObserver(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void finish()
  {
    _inspectionObservable.getInstance().deleteObserver(this);
  }

  /**
   * @author Andy Mechtenberg
   * @author Patrick Lacz
   */
  public void unpopulate()
  {
    _graphicsEngine.removeRenderers(_allGraphicLayers);
    setColors();
    _measurementLegends.clear();
    _maxWidthOfLegend = 0;
    _insertionHeight = _INITIAL_INSERTION_HEIGHT;
  }

  /**
   * @author George Booth
   */
  public GraphicsEngine getGraphicsEngine()
  {
    Assert.expect(_graphicsEngine != null);

    return _graphicsEngine;
  }

  /**
   * @author George Booth
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    // This is outside of the run to keep track of the fact that this method may use
    // any images on the Inspection Event
    if (argument instanceof InspectionEvent)
    {
      InspectionEvent event = (InspectionEvent)argument;
      event.incrementReferenceCount();
    }

    try
    {
      if (SwingUtilities.isEventDispatchThread())
      {
        handleUpdate(observable, argument);
      }
      else
      {
        // PE:  I am using invokeAndWait() here because we have to handle the algorithm
        // events as they occur.  Otherwise, the algorithms get out of sync with
        // the GUI and can build up a big backlog of events, which the swing thread
        // then processes in chunks at a time.  This makes the user experience very
        // herky jerky and also makes it difficult to debug behavior in the algorithms.
        SwingUtils.invokeAndWait(new Runnable()
        {
          public void run()
          {
            handleUpdate(observable, argument);
          }
        });
      }
    }
    catch (InterruptedException iex)
    {
      Assert.logException(iex);
    }
    catch (InvocationTargetException itex)
    {
      Assert.logException(itex);
    }
  }

  /**
   * @author George Booth
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  private void handleUpdate(final Observable observable, final Object argument)
  {
    try
    {
      if (observable instanceof InspectionEventObservable)
      {
        if (argument instanceof AlgorithmDiagnosticInspectionEvent)
        {
          AlgorithmDiagnosticInspectionEvent algorithmDiagEvent = (AlgorithmDiagnosticInspectionEvent)argument;
          if (algorithmDiagEvent.hasAlgorithmDiagnosticMessage())
          {
            displayLegend(algorithmDiagEvent.getAlgorithmDiagnosticMessage());
          }
        }
        else if (argument instanceof InspectionEvent)
        {
          InspectionEvent inspectionEvent = (InspectionEvent)argument;
          if (inspectionEvent.getInspectionEventEnum().equals(InspectionEventEnum.INSPECTION_STARTED))
          {
            _graphicsEngine.removeRenderers(_allGraphicLayers);
            setColors();
            _graphicsEngine.repaint();
          }
        }
      }
      else
        Assert.expect(false);
    }
    finally
    {
      if (argument instanceof InspectionEvent)
      {
        InspectionEvent event = (InspectionEvent)argument;
        event.decrementReferenceCount();
      }
    }
  }

  /**
   * @author George Booth
   * @author Patrick Lacz
   * @author Matt Wharton
   */
  public void displayLegend(AlgorithmDiagnosticMessage algorithmDiagMessage)
  {
    Assert.expect(algorithmDiagMessage != null);
    java.util.List<Pair <String, Integer>> newLegendEntries = new ArrayList<Pair <String, Integer>>();

    // See if we got a "diagnostic infos reset" flag.  This is an indication to clear the legend.
    if (algorithmDiagMessage.areDiagnosticInfosReset())
    {
      unpopulate();
    }

    if (algorithmDiagMessage.hasAnyGraphicalDiagnosticInfo())
    {
      if (algorithmDiagMessage.arePreviousDiagnosticsInvalidated())
      {
        unpopulate();
      }

      for (DiagnosticInfo diagnosticInfo : algorithmDiagMessage.getDiagnosticInfoList())
      {
        if (diagnosticInfo instanceof MeasurementRegionDiagnosticInfo)
        {
          MeasurementRegionDiagnosticInfo measurementDiagInfo = (MeasurementRegionDiagnosticInfo)diagnosticInfo;
          MeasurementRegionEnum measurementRegion = measurementDiagInfo.getMeasurementRegionEnum();
          Pair<String, Integer> legend = new Pair<String, Integer>();
          legend.setFirst(StringLocalizer.keyToString(measurementRegion.getRegionDescription()));
          legend.setSecond(_measurementRegionEnumToGraphicsLayerNumberMap.get(measurementRegion));
          if (_measurementLegends.contains(legend) == false && newLegendEntries.contains(legend) == false)
          {
            newLegendEntries.add(legend);
          }
        }
        else if (diagnosticInfo instanceof OverlayImageDiagnosticInfo)
        {
          OverlayImageDiagnosticInfo overlayImageDiagInfo = (OverlayImageDiagnosticInfo) diagnosticInfo;
          // voiding diagnostic
          Pair<String, Integer> legend = new Pair<String, Integer>();
          //Siew Yeng - get correct legend from diagnostic info
          //legend.setFirst(StringLocalizer.keyToString("VOIDING_DIAGNOSTIC_KEY"));
          legend.setFirst(overlayImageDiagInfo.getLabel());
          legend.setSecond(new Integer(_voidingLayer));
          if (_measurementLegends.contains(legend) == false && newLegendEntries.contains(legend) == false)
          {
            newLegendEntries.add(legend);
          }
        }
        else if (diagnosticInfo instanceof OverlayLargestVoidImageDiagnosticInfo)
        {
          // largest voiding diagnostic
          Pair<String, Integer> legend = new Pair<String, Integer>();
          legend.setFirst(StringLocalizer.keyToString("LARGEST_VOIDING_DIAGNOSTIC_KEY"));
          legend.setSecond(new Integer(_largestVoidingLayer));
          if (_measurementLegends.contains(legend) == false && newLegendEntries.contains(legend) == false)
          {
            newLegendEntries.add(legend);
          }
        }        
        else if (diagnosticInfo instanceof OverlayBackgroundImageDiagnosticInfo)
        {
          // background pixels diagnostic
          Pair<String, Integer> legend = new Pair<String, Integer>();
          legend.setFirst(StringLocalizer.keyToString("BACKGROUND_PIXELS_DIAGNOSTIC_KEY"));
          legend.setSecond(new Integer(_backgroundPixelsLayer));
          if (_measurementLegends.contains(legend) == false && newLegendEntries.contains(legend) == false)
          {
            newLegendEntries.add(legend);
          }
        }
        else if (diagnosticInfo instanceof OverlaySolderImageDiagnosticInfo)
        {
          // solder area diagnostic
          Pair<String, Integer> legend = new Pair<String, Integer>();
          legend.setFirst(StringLocalizer.keyToString("SOLDER_AREA_DIAGNOSTIC_KEY"));
          legend.setSecond(new Integer(_solderAreaLayer));
          if (_measurementLegends.contains(legend) == false && newLegendEntries.contains(legend) == false)
          {
            newLegendEntries.add(legend);
          }
        }
        else if (diagnosticInfo instanceof OverlayExpectedImageDiagnosticInfo)
        {
          // expected image diagnostic
          Pair<String, Integer> legend = new Pair<String, Integer>();
          legend.setFirst(StringLocalizer.keyToString("EXPECTED_IMAGE_DIAGNOSTIC_KEY"));
          legend.setSecond(new Integer(_expectedImageLayer));
          if (_measurementLegends.contains(legend) == false && newLegendEntries.contains(legend) == false)
          {
            newLegendEntries.add(legend);
          }
        }
        else if (diagnosticInfo instanceof OverlayWettingCoverageImageDiagnosticInfo)
        {
          // largest voiding diagnostic
          Pair<String, Integer> legend = new Pair<String, Integer>();
          legend.setFirst(StringLocalizer.keyToString("WETTING_COVERAGE_DIAGNOSTIC_KEY"));
          legend.setSecond(new Integer(_wettingCoverageLayer));
          if (_measurementLegends.contains(legend) == false && newLegendEntries.contains(legend) == false)
          {
            newLegendEntries.add(legend);
          }
        }       
      }
    }

    if (algorithmDiagMessage.hasProfileDiagnosticInfo())
    {
      ProfileDiagnosticInfo profileInfo = algorithmDiagMessage.getProfileDiagnosticInfo();

      if (profileInfo instanceof SingleDimensionProfileDiagnosticInfo)
      {
        SingleDimensionProfileDiagnosticInfo singleDimensionProfileInfo = (SingleDimensionProfileDiagnosticInfo)
            profileInfo;

        Assert.expect(singleDimensionProfileInfo.getNumberOfDimensions() == 1);

        Set<Integer> tagsFromProfileAddedSoFar = new HashSet<Integer>();
        // determine what layers are used
        for (Pair<MeasurementRegionEnum, Float> pair : singleDimensionProfileInfo.getData())
        {
          MeasurementRegionEnum measurementRegion = pair.getFirst();
          if (tagsFromProfileAddedSoFar.contains(measurementRegion.getId()))
            continue;

          Pair<String, Integer> legend = new Pair<String, Integer>();
          legend.setFirst(StringLocalizer.keyToString(measurementRegion.getRegionDescription()));
          legend.setSecond(this._measurementRegionEnumToGraphicsLayerNumberMap.get(measurementRegion));
          if (_measurementLegends.contains(legend) == false && newLegendEntries.contains(legend) == false)
          {
            newLegendEntries.add(legend);
            tagsFromProfileAddedSoFar.add(measurementRegion.getId());
          }
        }
      }
      else if (profileInfo instanceof MultiDimensionProfileDiagnosticInfo)
        Assert.expect(false, "Multi dimension profiles are not presently supported!");
      else
        Assert.expect(false, "Unknown profile diagnostic info type!");
    }

    // add color box and legend text for each unique measurement region

    if (newLegendEntries.size() > 0)
    {
      for (Pair<String, Integer> legend : newLegendEntries)
      {
        // better have a label and a layer
        Assert.expect(legend.getFirst() != null);
        Assert.expect(legend.getSecond() != null);

        ProfileTextRenderer legendTextRenderer = new ProfileTextRenderer(legend.getFirst(), 35, _insertionHeight - 5);
        _graphicsEngine.addRenderer(_textLayer, legendTextRenderer);
        _maxWidthOfLegend = Math.max(_maxWidthOfLegend, legendTextRenderer.getWidth() + 35 + 2);

        ProfileGraphicRenderer legendBoxRenderer = new ProfileGraphicRenderer(new Rectangle2D.Double(
            5, _insertionHeight, 25, 15));
        legendBoxRenderer.setFillShape(true);
        _graphicsEngine.addRenderer(legend.getSecond(), legendBoxRenderer);

        _insertionHeight = _insertionHeight + 20;
        _measurementLegends.add(legend);
      }

      _graphicsEngine.setPreferredSize(new Dimension((int)_maxWidthOfLegend, (int)_insertionHeight));
      _graphicsEngine.repaint();
    }
  }

  /**
   * @author George Booth
   */
  private void createLayers()
  {
    int layerNumber  = JLayeredPane.DEFAULT_LAYER.intValue();
    _backgroundLayer = layerNumber++;
    _textLayer = layerNumber++;
    _solderAreaLayer = layerNumber++;
    _expectedImageLayer = layerNumber++;
    _voidingLayer = layerNumber++;
    _largestVoidingLayer = layerNumber++;
    _wettingCoverageLayer = layerNumber++;
    _backgroundPixelsLayer = layerNumber++;
    _expectedImageLayer = layerNumber++;

    for (MeasurementRegionEnum measurementRegionEnum : MeasurementRegionEnum.getSetOfAllEnums())
    {
      int thisLayerNumber = layerNumber++;
      _measurementRegionEnumToGraphicsLayerNumberMap.put(measurementRegionEnum, thisLayerNumber);
    }

    _allGraphicLayers.add(_backgroundLayer);
    _allGraphicLayers.add(_textLayer);
    _allGraphicLayers.add(_solderAreaLayer);
    _allGraphicLayers.add(_expectedImageLayer);
    _allGraphicLayers.add(_voidingLayer);
    _allGraphicLayers.add(_largestVoidingLayer);
    _allGraphicLayers.add(_wettingCoverageLayer);
    _allGraphicLayers.add(_backgroundPixelsLayer);
    _allGraphicLayers.add(_expectedImageLayer);
    _allGraphicLayers.addAll(_measurementRegionEnumToGraphicsLayerNumberMap.values());
  }

  /**
   * @author George Booth
   */
  private void setColors()
  {
    _graphicsEngine.setForeground(_backgroundLayer, LayerColorEnum.PROFILE_BACKGROUND.getColor());
    _graphicsEngine.setForeground(_solderAreaLayer, LayerColorEnum.SOLDER_AREA_PIXELS.getColor());
    _graphicsEngine.setForeground(_expectedImageLayer, LayerColorEnum.EXPECTED_IMAGE_PIXELS.getColor());
    _graphicsEngine.setForeground(_voidingLayer, LayerColorEnum.VOIDING_PIXELS.getColor());
    _graphicsEngine.setForeground(_largestVoidingLayer, LayerColorEnum.LARGEST_VOIDING_PIXELS.getColor());
    _graphicsEngine.setForeground(_wettingCoverageLayer, LayerColorEnum.VOIDING_PIXELS.getColor());
    _graphicsEngine.setForeground(_backgroundPixelsLayer, LayerColorEnum.BACKGROUND_PIXELS.getColor());
    _graphicsEngine.setForeground(_expectedImageLayer, LayerColorEnum.EXPECTED_IMAGE_PIXELS.getColor());
    _graphicsEngine.setForeground(_textLayer, LayerColorEnum.PROFILE_TEXT.getColor());

    for (MeasurementRegionEnum measurementRegionEnum : MeasurementRegionEnum.getSetOfAllEnums())
    {
      Integer graphicsLayerForMeasurementRegion = _measurementRegionEnumToGraphicsLayerNumberMap.get(
          measurementRegionEnum);
      if (graphicsLayerForMeasurementRegion != null)
      {
        Color measurementRegionColor = XRayLayerColorEnum.getColorForMeasurementRegionEnum(measurementRegionEnum);
        _graphicsEngine.setForeground(graphicsLayerForMeasurementRegion.intValue(), measurementRegionColor);
      }
    }
  }
}
