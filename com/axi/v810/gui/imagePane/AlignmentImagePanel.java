package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.geom.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 * This class is the main panel for the image pane display during manual alignment.
 *
 * @author Kay Lannen
 */
public class AlignmentImagePanel extends JPanel
{
  private AlignmentImageGraphicsEngineSetup _alignmentImageGraphicsEngineSetup = null;
  private GraphicsEngine _graphicsEngine = null;
  private BorderLayout _borderLayout1 = new BorderLayout();
  
  /**
   * @author George A. David
   */
  public AlignmentImagePanel(ImageManagerToolBar toolBar)
  {
    super();
    _alignmentImageGraphicsEngineSetup = new AlignmentImageGraphicsEngineSetup(toolBar);
    _graphicsEngine = _alignmentImageGraphicsEngineSetup.getGraphicsEngine();
    jbInit();
  }
  
  /**
   * @author Chong, Wei Chin
   */
  public AlignmentImagePanel(ImageManagerToolBar toolBar, MagnificationEnum magnification)
  {
    super();
    _alignmentImageGraphicsEngineSetup = new AlignmentImageGraphicsEngineSetup(toolBar, magnification);
    _graphicsEngine = _alignmentImageGraphicsEngineSetup.getGraphicsEngine();
    jbInit();
  }

  /**
   * @author George A. David
   */
  public void display()
  {
    _alignmentImageGraphicsEngineSetup.display();
  }

  /**
   * @author George A. David
   */
  public void beginAlignment()
  {
    _alignmentImageGraphicsEngineSetup.beginAlignment();
  }

  /**
   * @author George A. David
   */
  public void endAlignment()
  {
    _alignmentImageGraphicsEngineSetup.endAlignment();
  }

  /**
   * @author George A. David
   */
  private void jbInit()
  {
    setLayout(_borderLayout1);
    add(_graphicsEngine, BorderLayout.CENTER);
  }

  /**
   * @author George A. David
   */
  public Point2D getOffsetRelativeToPanelInNanoMeters()
  {
    Assert.expect(_alignmentImageGraphicsEngineSetup != null);
    return _alignmentImageGraphicsEngineSetup.getOffsetRelativeToPanelInNanoMetersFromExpectedLocation();
  }

  /**
   * @author George A. David
   */
  public AlignmentImageGraphicsEngineSetup getAlignmentImageGraphicsEngineSetup()
  {
    Assert.expect(_alignmentImageGraphicsEngineSetup != null);
    return _alignmentImageGraphicsEngineSetup;
  }
  
  /**
   *
   * XCR-3589 Combo STD and XXL software GUI
   *
   * @author weng-jian.eoh
   */
  public void finish()
  {
    Assert.expect(_alignmentImageGraphicsEngineSetup != null);
    _alignmentImageGraphicsEngineSetup.finish();
  }
}
