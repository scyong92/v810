 package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * <p>Title: GenerateImageSetWizardSummaryPanel</p>
 *
 * <p>Description: This is the last panel of the image set generation wizard. This panel will summarize the details of
 * the image set that will be generated.
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: AXI</p>
 *
 * @author Erica Wheatcroft
 */
public class GenerateImageSetWizardSummaryPanel extends WizardPanel
{
  private MainMenuGui _mainUI;
  private JPanel _summaryPanel;
  private JPanel _panelHandlingStatusPanel;
  private JPanel _buttonPanel;
  private JPanel _loadButtonPanel;
  private JPanel _unloadButtonPanel;

  private FlowLayout _loadButtonPanelFlowLayout = new FlowLayout();
  private FlowLayout _unloadButtonPanelFlowLayout = new FlowLayout();
  private GridLayout _buttonPanelGridLayout = new GridLayout();

  private JTextField _summaryTextField;
  private JTextField _descriptionTextField;
  private JLabel _panelHandlingStatusLabel = new JLabel();
  private JLabel _numberOfImagesLabel = new JLabel();
  private JLabel _estimatedDiskSpaceLabel = new JLabel();
  private JButton _loadButton;
  private JButton _unloadButton;
  private ActionListener _loadButtonActionListener;
  private ActionListener _unloadButtonActionListener;

  private Border _etchedBorder = BorderFactory.createEtchedBorder();
  private Border _mainBorder = BorderFactory.createEmptyBorder(5, 10, 5, 10);
  private Border _panelStatusBorder = null;
  private Border _summaryPanelBorder = null;

  private Project _project = null;
  private ImageManager _imageManager;
  private ImageSetData _imageSetData = null;

  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  private PanelHandler _panelHandler = null;

  private GenerateImageSetWizardDialog _dialog = null;
  private boolean _saveOnlyTestedDevices = false;

  /**
   * @author Erica Wheatcroft
   */
  public GenerateImageSetWizardSummaryPanel(Project project, ImageSetData imageSetData, WizardDialog dialog)
  {
    super(dialog);
    Assert.expect(project != null, "project is null");
    Assert.expect(imageSetData != null, "Image set data is null");
    _mainUI = MainMenuGui.getInstance();
    _imageSetData = imageSetData;
    _project = project;
    _imageManager = ImageManager.getInstance();
    _dialog = (GenerateImageSetWizardDialog)dialog;

    _panelHandler = PanelHandler.getInstance();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _project = null;
    _imageSetData = null;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void removeObservers()
  {
    // do nothing
  }


  /**
   * @author Erica Wheatcroft
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null, "Name of panel is null");
    Assert.expect(name.length() > 0, "Name of panel is an empty string");
    _name = name;
  }

  /**
   * @author Erica Wheatcroft
   */
  public String getWizardName()
  {
    Assert.expect(_name != null, "Name of panel is not set");
    return _name;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    removeAll();

    _panelStatusBorder = BorderFactory.createCompoundBorder(new TitledBorder(_etchedBorder, StringLocalizer.keyToString("GISWK_MACHINE_STATUS_LABEL_KEY")),
        BorderFactory.createEmptyBorder(0, 0, 0, 0));
    _summaryPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(_etchedBorder, StringLocalizer.keyToString("GISWK_IMAGE_SET_DETAILS_KEY")),
        BorderFactory.createEmptyBorder(10, 10, 10, 10));
    setLayout(new GridLayout(1,1));
    setBorder(_mainBorder);

    _summaryPanel = new JPanel(new FlowLayout());
    _summaryPanel.setBorder(_summaryPanelBorder);
    PairLayout layout2 = new PairLayout();

    JPanel descriptionPanel = new JPanel(layout2);
    descriptionPanel.add(new JLabel(StringLocalizer.keyToString("GISWK_SUMMARY_KEY")));
    _summaryTextField = new JTextField();
    _summaryTextField.setColumns(30);
    descriptionPanel.add(_summaryTextField);
    _summaryTextField.setEditable(false);
    descriptionPanel.add(new JLabel(StringLocalizer.keyToString("GISWK_DESCRIPTION_KEY")));
    _descriptionTextField = new JTextField();
    descriptionPanel.add(_descriptionTextField);
    _descriptionTextField.setColumns(30);

    //JPanel panel = new JPanel(new VerticalFlowLayout());
    JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 0));
    panel.add(_numberOfImagesLabel);
    panel.add(_estimatedDiskSpaceLabel);

    Box summaryPanelBox = new Box(BoxLayout.Y_AXIS);
    summaryPanelBox.add(descriptionPanel);
    summaryPanelBox.add(Box.createVerticalStrut(15));
    summaryPanelBox.add(panel);
    _summaryPanel.add(summaryPanelBox, BorderLayout.CENTER);

    _panelHandlingStatusPanel = new JPanel();
    _panelHandlingStatusPanel.setBorder(_panelStatusBorder);
    JPanel tempPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    tempPanel.add(_panelHandlingStatusLabel);
    _panelHandlingStatusLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _panelHandlingStatusLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _buttonPanelGridLayout.setHgap(0);
    _buttonPanelGridLayout.setVgap(0);
    _buttonPanel = new JPanel();
    _buttonPanel.setLayout(_buttonPanelGridLayout);
    _loadButtonPanel = new JPanel();
    _unloadButtonPanel = new JPanel();
    _loadButtonPanel.setLayout(_loadButtonPanelFlowLayout);
    _unloadButtonPanel.setLayout(_unloadButtonPanelFlowLayout);
    _loadButton = new JButton();
    _unloadButton = new JButton();
    _loadButton.setText(StringLocalizer.keyToString("GISWK_LOAD_PANEL_KEY"));
    _unloadButton.setText(StringLocalizer.keyToString("GISWK_UNLOAD_PANEL_KEY"));
    _loadButtonPanel.add(_loadButton);
    _unloadButtonPanel.add(_unloadButton);

    _buttonPanel.add(_loadButtonPanel);
    _buttonPanel.add(_unloadButtonPanel);

    Box panelHandlingBox = new Box(BoxLayout.Y_AXIS);
    panelHandlingBox.add(tempPanel);
    panelHandlingBox.add(Box.createVerticalGlue());
    panelHandlingBox.add(_buttonPanel);
    _panelHandlingStatusPanel.add(panelHandlingBox);

    Box mainBox = new Box(BoxLayout.Y_AXIS);
    mainBox.add(_summaryPanel);
    mainBox.add(Box.createVerticalGlue());
    mainBox.add(_panelHandlingStatusPanel);
    add(mainBox, java.awt.BorderLayout.CENTER);

    // listeners
    _loadButtonActionListener = (new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        // the user wishes to load the panel
        loadPanel(true);
      }
    });
    _unloadButtonActionListener = (new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        // the user wishes to load the panel
        loadPanel(false);
      }
    });

    _loadButton.addActionListener(_loadButtonActionListener);
    _unloadButton.addActionListener(_unloadButtonActionListener);

    _descriptionTextField.addFocusListener(new FocusListener()
    {
      public void focusGained(FocusEvent evt)
      {
        // select the text
        _descriptionTextField.selectAll();
      }
      public void focusLost(FocusEvent evt)
      {
        String userDescription = _descriptionTextField.getText();
        if(userDescription.equalsIgnoreCase(StringLocalizer.keyToString("GISWK_USER_TEXT_KEY")))
          _imageSetData.setUserDescription("");
        else
          _imageSetData.setUserDescription(userDescription);
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void loadPanel(final boolean loadingPanel)
  {
    // we have hardware so lets load a panel
    // first lets check to make sure we have done a startup on the hardware
    try
    {
      if (loadingPanel)
        _mainUI.loadPanel();
      else
        _mainUI.unloadPanel();
    }
    catch (XrayTesterException xte)
    {
      _mainUI.handleXrayTesterException(xte);
    }

//    validatePanel();

    populatePanelHandlingButtons();
//    setInitialButtonState();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void populatePanelWithData()
  {
    _saveOnlyTestedDevices = _dialog.saveOnlyTestedDevices();

    // set the title
    _dialog.changeTitle(StringLocalizer.keyToString("GISWK_SCREEN_FINAL_TITLE_KEY"));

    // set up the button state
    setInitialButtonState();

    // create the panel
    createPanel();

    // this method will set up the test program with the correct filters
    createFilters();

    // populate the summary and the desciption
    populateImageSetSummary();
    populatePanelHandlingButtons();

    // validate the panel see if the user can generate the image set yet
    validatePanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createFilters()
  {
    /** @todo fill in reason */
    TestProgram testProgram = _mainUI.getTestProgramWithReason(StringLocalizer.keyToString("MMGUI_IMAGE_SET_GEN_NEEDS_TEST_PROGRAM_KEY"),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        _project);

    // set the image set data for the test program
    testProgram.clearFilters();
    testProgram.addFilter( ! _imageSetData.generateImagesForNoTestAndNoLoadDevices());
    testProgram.setUseOnlyInspectedRegionsInScanPath( ! _imageSetData.generateImagesForNoTestAndNoLoadDevices());

    // lets setup the filters that will tell the image acquisition system what to do
    ImageSetTypeEnum imageRunTypeEnum = _imageSetData.getImageSetTypeEnum();
    if (imageRunTypeEnum.equals(ImageSetTypeEnum.PANEL))
    {
      // do nothing.. we have already cleared the filters so we are ready to test.
    }
    else if (imageRunTypeEnum.equals(ImageSetTypeEnum.BOARD))
    {
      // set the board instance the user has selected
      testProgram.addFilter(_imageSetData.getBoardToAcquireImagesFor());
    }
    else if (imageRunTypeEnum.equals(ImageSetTypeEnum.COMPONENT))
    {
      testProgram.addFilter(_imageSetData.getComponentTypeToAcquireImagesFor());
      if (_imageSetData.collectImagesOnAllBoardInstances())
      {
        testProgram.addFilter(_imageSetData.getBoardTypeToAcquireImagesAgainst());
      }
      else
      {
        testProgram.addFilter(_imageSetData.getBoardInstanceToAcquireImagesAgainst());
      }

    }
    else if (imageRunTypeEnum.equals(ImageSetTypeEnum.PIN))
    {
      testProgram.addFilter(_imageSetData.getPadTypeToAcquireImagesFor());
      if (_imageSetData.collectImagesOnAllBoardInstances())
      {
        testProgram.addFilter(_imageSetData.getBoardTypeToAcquireImagesAgainst());
      }
      else
      {
        testProgram.addFilter(_imageSetData.getBoardInstanceToAcquireImagesAgainst());
      }
    }
    else if (imageRunTypeEnum.equals(ImageSetTypeEnum.SUBTYPE))
    {
      testProgram.addFilter(_imageSetData.getSubtypeToAcquireImagesFor());
      if(_imageSetData.collectImagesForPanel() == false)
      {
        if (_imageSetData.collectImagesOnAllBoardInstances())
          testProgram.addFilter(_imageSetData.getBoardTypeToAcquireImagesAgainst());
        else
          testProgram.addFilter(_imageSetData.getBoardInstanceToAcquireImagesAgainst());
      }
    }
    else if (imageRunTypeEnum.equals(ImageSetTypeEnum.JOINT_TYPE))
    {
      testProgram.addFilter(_imageSetData.getJointTypeEnumToAcquireImagesFor());
      if(_imageSetData.collectImagesForPanel() == false)
      {
        if (_imageSetData.collectImagesOnAllBoardInstances())
          testProgram.addFilter(_imageSetData.getBoardTypeToAcquireImagesAgainst());
        else
          testProgram.addFilter(_imageSetData.getBoardInstanceToAcquireImagesAgainst());
      }
    }
    else
      Assert.expect(false, "invalid image set type ");

    // if this is a regular image set (i.e. not a precision image set), we want to exclude from
    // imaging subtypes for which predictive slice height feature is turned on.
    if (_imageSetData.getImageSetCollectionTypeEnum().equals(ImageSetCollectionTypeEnum.FINE_TUNING))
    {
      Set<Subtype> subtypesToExclude = new HashSet<Subtype>();
      for (Subtype subtype: _project.getPanel().getSubtypes())
      {
        if (subtype.usePredictiveSliceHeight())
          subtypesToExclude.add(subtype);
      }

      testProgram.addExcludeFilter(subtypesToExclude);
    }
    else if (_imageSetData.getImageSetCollectionTypeEnum().equals(ImageSetCollectionTypeEnum.PRECISION))
    {
      testProgram.addFilter(true);
      testProgram.setUseOnlyInspectedRegionsInScanPath(true);
    }

  }

  /**
   * @author Erica Wheatcroft
   */
  private void populatePanelHandlingButtons()
  {
    // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
    try
    {
       final boolean panelLoaded = _panelHandler.isPanelLoaded();

       SwingUtils.invokeLater(new Runnable()
       {
         public void run()
         {
           if (panelLoaded)
           {
             // a panel is loaded. - disable the load button and update the label
             _loadButton.setEnabled(false);
             _unloadButton.setEnabled(true);
             String projectName = null;

 //          if(XrayTester.getInstance().isSimulationModeOn() ||XrayTester.getInstance().isHardwareAvailable() == false)
 //            projectName = _project.getName();
 //          else
             projectName = _panelHandler.getLoadedPanelProjectName();

             _panelHandlingStatusLabel.setText(StringLocalizer.keyToString(
                 new LocalizedString("GISWK_PANEL_IN_MACHINE_STATUS_DESCRIPTION_KEY",
                                     new Object[]
                                     {projectName})));
           }
           else
           {
             // a panel is NOT loaded. - disable the unload button and update the label
             _loadButton.setEnabled(true);
             _unloadButton.setEnabled(false);
             _panelHandlingStatusLabel.setText(StringLocalizer.keyToString("GISWK_PANEL_NOT_IN_MACHINE_STATUS_DESCRIPTION_KEY"));
           }
         }
       });
     }
     catch (XrayTesterException xte)
     {
       _mainUI.handleXrayTesterException(xte);
     }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateImageSetSummary()
  {
    boolean collectImagesOnAllBoardInstances = _imageSetData.collectImagesOnAllBoardInstances();

    String text = null;
    // if this image set is a precision one, it is automatically for the whole panel and populated
    if (_imageSetData.getImageSetCollectionTypeEnum().equals(ImageSetCollectionTypeEnum.PRECISION))
      text = StringLocalizer.keyToString("Precision-Panel: " + _project.getName() + ", Populated");
    // otherwise, for fine tunning image sets, proceed with the user choices
    else
    {
    // first lets get what type of image set summary we are doing
    ImageSetTypeEnum type = _imageSetData.getImageSetTypeEnum();
    if(type.equals(ImageSetTypeEnum.PANEL))
    {
      // since the user wants to do a panel test we will write out the following
      // Panel : <panelName>
      text = StringLocalizer.keyToString("ATGUI_PANEL_KEY") + ": " + _project.getName() + ", ";
    }
    else if(type.equals(ImageSetTypeEnum.BOARD))
    {
      // since the user wants to do a board test we will write out the following
      // Board : <boardInstanceName> [BoardType]
      // boardTYpe will only be written if there exists more than one board type

      int numberOfBoardTypes = _imageSetData.getBoardToAcquireImagesFor().getPanel().getBoardTypes().size();
      if( numberOfBoardTypes == 1)
        text = StringLocalizer.keyToString("ATGUI_BOARD_KEY") +": " + _imageSetData.getBoardToAcquireImagesFor().getName() + ", ";
      else if(numberOfBoardTypes > 1)
        text = StringLocalizer.keyToString("ATGUI_BOARD_KEY") +": " + _imageSetData.getBoardToAcquireImagesFor().getName() + ", " +
               StringLocalizer.keyToString("ATGUI_BOARD_TYPE_KEY") +": " + _imageSetData.getBoardToAcquireImagesFor().getBoardType().getName() + ", ";
    }
    else if(type.equals(ImageSetTypeEnum.COMPONENT))
    {
      // since the user wants to do a component test we will write out the following info
      // Component: <componentInstanceName>, Board : <boardInstanceName>, BoardType: <BoardType>
      // board instance and board type will only be written if there exists more than one board instance and more than
      // one board type, if the user wants to collect images for all board instances the key word ALL will be written out for
      // board instances

      ComponentType compType = _imageSetData.getComponentTypeToAcquireImagesFor();
      Board board = _imageSetData.getBoardInstanceToAcquireImagesAgainst();

      text = StringLocalizer.keyToString("ATGUI_COMPONENT_KEY")+": " + compType.getReferenceDesignator() + ", ";

      if(collectImagesOnAllBoardInstances)
        text += StringLocalizer.keyToString("ATGUI_BOARD_KEY")+": "+ StringLocalizer.keyToString("ATGUI_ALL_KEY") + ", ";
      else
      {
        int numberOfBoardInstances = board.getPanel().getBoards().size();
        if( numberOfBoardInstances > 1)
          text += StringLocalizer.keyToString("ATGUI_BOARD_KEY")+": " + board.getName() + ", ";
      }

      int numberOfBoardTypes = board.getPanel().getBoardTypes().size();
      if( numberOfBoardTypes > 1)
        text += StringLocalizer.keyToString("ATGUI_BOARD_TYPE_KEY")+": " + board.getBoardType() + ", ";

    }
    else if(type.equals(ImageSetTypeEnum.PIN))
    {
      // since the user wants to do a pin test we will write out the following
      // Component: <componentInstanceName>, Pin: <pinName>, Board : <boardInstanceName>, BoardType: <BoardType>
      // board instance and board type will only be written if there exists more than one board instance and more than
      // one board type, if the user wants to collect images for all board instances the key word ALL will be written out for
      // board instances

      PadType padType = _imageSetData.getPadTypeToAcquireImagesFor();

      text = StringLocalizer.keyToString("ATGUI_COMPONENT_KEY") + ": " + padType.getComponentType().getReferenceDesignator() + ", " +
             StringLocalizer.keyToString("ATGUI_PIN_KEY") + ": " + padType.getName() + ", ";

      if(collectImagesOnAllBoardInstances)
      {
        text += StringLocalizer.keyToString("ATGUI_BOARD_KEY") + ": " + StringLocalizer.keyToString("ATGUI_ALL_KEY") + ", ";
      }
      else
      {
        Board board = _imageSetData.getBoardInstanceToAcquireImagesAgainst();
        int numberOfBoardInstances = board.getPanel().getBoards().size();
        if(numberOfBoardInstances > 1)
          text += StringLocalizer.keyToString("ATGUI_BOARD_KEY")+": " + board.getName() + ", ";

        int numberOfBoardTypes = board.getPanel().getBoardTypes().size();
        if(numberOfBoardTypes > 1)
          text += StringLocalizer.keyToString("ATGUI_BOARD_TYPE_KEY")+": " + board.getBoardType() + ", ";
      }
    }
    else if(type.equals(ImageSetTypeEnum.JOINT_TYPE))
    {
      JointTypeEnum jointTypeEnum = _imageSetData.getJointTypeEnumToAcquireImagesFor();

      text = StringLocalizer.keyToString("ATGUI_FAMILY_KEY") + ": " + jointTypeEnum + ", ";

      if(_imageSetData.collectImagesForPanel())
      {
        // the user wants to collect images for the entire panel
        // since the user wants to do a joint type test we will write out the following
        // Panel : <panelName> JointType: <jointType>
        text += StringLocalizer.keyToString("ATGUI_PANEL_KEY") + ": " +  _project.getName() + ", ";
      }
      else
      {
        // the user wants to collect images for a joint type at the board level.
        // Joint Type: <jointType> Board : <boardInstanceName>, BoardType: <BoardType>
        // board instance and board type will only be written if there exists more than one board instance and more than
        // one board type, if the user wants to collect images for all board instances the key word ALL will be written out for
        // board instances
        if(collectImagesOnAllBoardInstances)
          text += StringLocalizer.keyToString("ATGUI_BOARD_KEY") + ": " + StringLocalizer.keyToString("ATGUI_ALL_KEY") + ", ";
        else
        {
          int numberOfBoardInstances = _imageSetData.getBoardTypeToAcquireImagesAgainst().getPanel().getBoards().size();
          if(numberOfBoardInstances > 1)
            text += StringLocalizer.keyToString("ATGUI_BOARD_KEY") + ": " + _imageSetData.getBoardInstanceToAcquireImagesAgainst().getName() + ", ";
        }

//        int numberOfBoardTypes = _imageSetData.getBoardTypeToAcquireImagesAgainst().getPanel().getBoardTypes().size();
//        if(numberOfBoardTypes > 1)
//          text += StringLocalizer.keyToString("ATGUI_BOARD_KEY") + ": " + _imageSetData.getBoardInstanceToAcquireImagesAgainst().getName() + ", ";
      }
    }
    else if(type.equals(ImageSetTypeEnum.SUBTYPE))
    {

      Subtype subtype = _imageSetData.getSubtypeToAcquireImagesFor();

      text = StringLocalizer.keyToString("ATGUI_FAMILY_KEY") + ": " + subtype.getJointTypeEnum() + ", " +
             StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY") + ": " + subtype.getShortName() + ", ";

      if(_imageSetData.collectImagesForPanel())
      {
        // the user wants to collect images for the entire panel
        // since the user wants to do a subtype type test we will write out the following
        // Joint Type: <jointType> Subtype: <subtype> Panel: <panel>
        text += StringLocalizer.keyToString("ATGUI_PANEL_KEY") + ": " +  _project.getName() + ", ";
      }
      else
      {
        // the user wants to collect images for a subtype at the board level.
        // Joint Type: <jointType>, Subtype: <subtype>, Board : <boardInstanceName>, BoardType: <BoardType>
        // board instance and board type will only be written if there exists more than one board instance and more than
        // one board type, if the user wants to collect images for all board instances the key word ALL will be written out for
        // board instances
        if(collectImagesOnAllBoardInstances)
          text += StringLocalizer.keyToString("ATGUI_BOARD_KEY") + ": " + StringLocalizer.keyToString("ATGUI_ALL_KEY") + ", ";
        else
        {
          int numberOfBoardInstances = _imageSetData.getBoardInstanceToAcquireImagesAgainst().getPanel().getBoards().size();
          if(numberOfBoardInstances > 1)
            text += StringLocalizer.keyToString("ATGUI_BOARD_KEY") + ": " + _imageSetData.getBoardInstanceToAcquireImagesAgainst().getName() + ", ";
        }
        int numberOfBoardTypes = _imageSetData.getBoardTypeToAcquireImagesAgainst().getPanel().getBoardTypes().size();
        if(numberOfBoardTypes > 1)
          text += StringLocalizer.keyToString("ATGUI_BOARD_TYPE_KEY") + ": " + _imageSetData.getBoardTypeToAcquireImagesAgainst().getName() + ", ";      }
    }
    else
      Assert.expect(false, "unkown image set type");

    // now lets add on if the image set is populated or not to the system description.
    if(_imageSetData.isBoardPopulated())
      text += StringLocalizer.keyToString("GISWK_POPULATED_KEY");
    else
      text += StringLocalizer.keyToString("GISWK_UNPOPULATED_KEY");

    // now lets add on if the user wants to save only inspectable devices
    if(_saveOnlyTestedDevices)
      text += ", " + StringLocalizer.keyToString("GISK_TESTED_DEVICES_SAVED_KEY");
    }
    
    if (_imageSetData.getVariationName().equals(VariationSettingManager.ALL_LOADED) == false)
    {
      text += "_" + _imageSetData.getVariationName();
    }
    
    // set the description
    _descriptionTextField.setText(StringLocalizer.keyToString("GISWK_USER_TEXT_KEY"));

    // set the summary
    _summaryTextField.setText(text);

    // set the number of images to be created
    TestProgram testProgram = _mainUI.getTestProgramWithReason(StringLocalizer.keyToString("MMGUI_IMAGE_SET_GEN_NEEDS_TEST_PROGRAM_KEY"),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        _project);
    int numberOfImages = _imageManager.getNumberOfInspectionImagesToSave(_imageSetData, testProgram);
    _numberOfImagesLabel.setText(numberOfImages + " " + StringLocalizer.keyToString("GISWK_NUMBER_OF_IMAGES_KEY"));

    // set the estimated space on disk
    _estimatedDiskSpaceLabel.setText(StringLocalizer.keyToString("GISWK_SPACE_ESTIMATION_KEY") +
                                     " " + MathUtil.roundToPlaces(numberOfImages *.010, 2) + " " +
                                     StringLocalizer.keyToString("GISWK_SPACE_ESTIMATION_UNITS_KEY"));
  }

  /**
   * In order for the generate button to become enabled the user must have a panel loaded and manual alignment must have
   * been performed.
   *
   * @author Erica Wheatcroft
   */
  public void validatePanel()
  {
    // get the number of images that will be created
    TestProgram testProgram = _mainUI.getTestProgramWithReason(StringLocalizer.keyToString("MMGUI_IMAGE_SET_GEN_NEEDS_TEST_PROGRAM_KEY"),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        _project);
    _imageSetData.setNumberOfImagesSaved(_imageManager.getNumberOfInspectionImagesToSave(_imageSetData, testProgram));

    // set the machine serial number that this image set was created on.
    _imageSetData.setMachineSerialNumber(XrayTester.getInstance().getSerialNumber());
 
    // get the user description..
    String userDescription = _descriptionTextField.getText();
    if (userDescription.equals(StringLocalizer.keyToString("GISWK_USER_TEXT_KEY")))
      _imageSetData.setUserDescription("");
    else
      _imageSetData.setUserDescription(userDescription);

    // get the system description..
    _imageSetData.setSystemDescription(_summaryTextField.getText());
    populatePanelHandlingButtons();
    setInitialButtonState();
  }

  /**
   * This method will set up the button state for this screen of the wizzard
   *
   * @author Erica Wheatcroft
   */
  private void setInitialButtonState()
  {
//    boolean isPanelLoaded = _panelHandler.isPanelLoaded();
    WizardButtonState buttonState = new WizardButtonState(false, true, true, true);
    _observable.updateButtonState(buttonState);
  }
}
