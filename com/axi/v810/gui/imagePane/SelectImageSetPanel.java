package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.mainMenu.MainMenuGui;
import com.axi.v810.util.*;

/**
 * <p>Title: SelectImageSetPanel</p>
 *
 * <p>Description:  This panel will allow the user to select one or more compatible image set for the loaded
 *                  test program. </p>
 *
 * @author Erica Wheatcroft
 */
public class SelectImageSetPanel extends JPanel implements Observer
{
  private JPanel _selectedImageSetPanel = new JPanel();
  private BorderLayout _mainBorderLayout = new BorderLayout();
  private GridLayout _selectedImageSetPanelGridLayout = new GridLayout();
  private Border _selectedImageSetPanelBorder = null;
  private TitledBorder _titledBorder = new TitledBorder(BorderFactory.createEtchedBorder(), StringLocalizer.keyToString("SELECTED_IMAGE_SETS_PANEL_TITLE_KEY"));
  
  private JScrollPane _selectedImageSetsScrollPane = new JScrollPane();
  private JSortTable _selectedImageSetsTable = new JSortTable();
  private AvailableAndSelectedImageSetsTableModel _selectedImageSetsTableModel = null;
  private JButton _clearButton = new JButton(StringLocalizer.keyToString("GUI_SELECT_CLEAR_ALL_KEY"));
  private JButton _selectAllButton = new JButton(StringLocalizer.keyToString("GUI_SELECT_SELECT_ALL_KEY"));
  private JPopupMenu _popupmenu = new JPopupMenu();
  private JMenuItem _detailsMenuItem = new JMenuItem();
  private JLabel _noImageSetsExistLabel = new JLabel(StringLocalizer.keyToString("SELECTED_NO_IMAGE_SETS_EXIST_KEY"), JLabel.CENTER);

  private boolean _createTable = false;

  private ProjectPersistance _persistance = null;

  private java.util.List<ImageSetData> _selectedImageSets = new LinkedList<ImageSetData>();

  private ImageManager _imageManager = ImageManager.getInstance();
  
  protected static WorkerThread _imageSetUpdateThread = new WorkerThread("ImageSetUpdateThread");
  
  private boolean _continueToUpdateImageSet = true;
  private int _totalImageSetCount = 0;
  /**
   * @author Erica Wheatcroft
   */
  public SelectImageSetPanel()
  {
    GuiObservable.getInstance().addObserver(this);
    _selectedImageSets.clear();
  }

  /**
   * @author Andy Mechtenberg
   */
  void clear()
  {
    GuiObservable.getInstance().deleteObserver(this);
    _selectedImageSets.clear();
    _continueToUpdateImageSet = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel(boolean createTable)
  {
    _createTable = createTable;

    //Lim, Lay Ngor - set etched border to optimise the ImageSet Border Size on display.
    //Adjust border to smaller edge.
    Border etchedBorder = BorderFactory.createEtchedBorder();    
    _selectedImageSetPanelBorder = BorderFactory.createCompoundBorder(_titledBorder,
                                                                      BorderFactory.createEmptyBorder(2, 2, 2, 2));//(5, 5, 5, 5));
    setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));//(10, 5, 10, 5));
    setLayout(_mainBorderLayout);
    _selectedImageSetPanel.setLayout(_selectedImageSetPanelGridLayout);
    _selectedImageSetPanel.setBorder(_selectedImageSetPanelBorder);
    _selectedImageSetPanel.add(_selectedImageSetsScrollPane);
    _selectedImageSetsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _selectedImageSetsTable.setRowSelectionAllowed(true);
    _selectedImageSetsTable.setColumnSelectionAllowed(false);

    if(_createTable)
      _selectedImageSetsScrollPane.getViewport().add(_selectedImageSetsTable);
    else
      _selectedImageSetsScrollPane.getViewport().add(_noImageSetsExistLabel);

    Box box = new Box(BoxLayout.X_AXIS);
    box.add(_selectedImageSetPanel);
    add(box, java.awt.BorderLayout.CENTER);

    // now lets create the button panel.
    JPanel buttonPanel = new JPanel(new FlowLayout());
    buttonPanel.add(_clearButton);
    buttonPanel.add(_selectAllButton);

    if(_createTable == false)
    {
      _clearButton.setEnabled(false);
      _selectAllButton.setEnabled(false);
    }

    _selectedImageSetsTable.setDragEnabled(false);

    add(buttonPanel, BorderLayout.SOUTH);
    _popupmenu.add(_detailsMenuItem);
    _detailsMenuItem.setText(StringLocalizer.keyToString("MMGUI_DETAILS_KEY"));

    _detailsMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        JPopupMenu popupmenu =(JPopupMenu)((JMenuItem) e.getSource()).getParent();
        JSortTable table = (JSortTable)popupmenu.getInvoker();

        ImageSetData selectedImageSet = ((AvailableAndSelectedImageSetsTableModel)table.getModel()).getSelectedImageSet(table.getSelectedRow());
        Assert.expect(selectedImageSet != null, "image set is null");

        // the user wants to see more info on this particular image set.
        ImageManagerDialog dialog = new ImageManagerDialog(MainMenuGui.getInstance(), true);
        dialog.populateDialogWithImageSet(selectedImageSet);
        dialog.pack();
        SwingUtils.centerOnComponent(dialog, MainMenuGui.getInstance());
        dialog.setVisible(true);
        dialog.dispose();
      }
    });

    _selectedImageSetsTable.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent evt){}
      public void mouseReleased(MouseEvent e)
      {
        int selectedRowFromClick = _selectedImageSetsTable.rowAtPoint(e.getPoint());
        if(selectedRowFromClick > -1)
        {
          if (e.isPopupTrigger())
          {
            // show the popup menu
            if (selectedRowFromClick >= 0)
            {
              _selectedImageSetsTable.setRowSelectionInterval(selectedRowFromClick, selectedRowFromClick);
              _popupmenu.show(e.getComponent(), e.getX(), e.getY());
            }
          }
          else
          {
            int selectedRow = _selectedImageSetsTable.getSelectedRow();
            if (selectedRow != selectedRowFromClick)
            {
              // first we need to stop the cell editing
              TableCellEditor cellEditor = _selectedImageSetsTable.getColumnModel().getColumn(0).getCellEditor();
              if (cellEditor != null)
                cellEditor.cancelCellEditing();
              // clear the selection
              _selectedImageSetsTable.clearSelection();
              // then select the row
              _selectedImageSetsTable.setRowSelectionInterval(selectedRowFromClick, selectedRowFromClick);
            }
          }
        }
      }
    });

    _selectAllButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        // the user wants to select all the image sets listed.
        _selectedImageSetsTableModel.selectAllImageSets();
      }
    });

    _clearButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedImageSetsTableModel.clearSelectedImageSets();
      }
    });

  }

  /**
   * @author Andy Mechtenberg
   */
  int getTableRowHeight()
  {
    int currentRows = _selectedImageSetsTableModel.getRowCount();
    if (currentRows < 3)
      return _selectedImageSetsTable.getRowHeight();
    else if (_selectedImageSetsScrollPane.getVerticalScrollBar().isVisible() || currentRows > 10)
      return 0;
    else
      return _selectedImageSetsTable.getRowHeight();
  }

  /**
   * This method will populate the gui with the current compatible image Set data.
   *
   * @author Erica Wheatcroft
   */
  public void populateWithProject(final Project project, boolean needToCollectImageSetData) throws DatastoreException
  {
    Assert.expect(project != null, "project is null");
    _persistance = MainMenuGui.getInstance().getTestDev().getPersistance().getProjectPersistance();
    MainMenuGui.getInstance().generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_INSPECTION_IMAGES_NEEDS_TEST_PROGRAM_KEY"),
                                                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                                    project);
    _selectedImageSetsTableModel = new AvailableAndSelectedImageSetsTableModel(false);
    _selectedImageSetsTable.setModel(_selectedImageSetsTableModel);
    _totalImageSetCount = 0;
    // first lets populate with the available list of image Sets

    // get the available set of image Sets from the image manager
    // lets clear the filters so the compatible call works correctly - G$
    project.getTestProgram().clearFilters();
    final java.util.List<ImageSetData> compatibleImageSets = new ArrayList<ImageSetData>(_imageManager.getImageSetData(project, needToCollectImageSetData));
    
    if(compatibleImageSets.isEmpty())
      createPanel(false);
    else
      createPanel(true);

    // now lets check to see if we have any image sets already persisted..
   java.util.List<String> savedImageSetNames = _persistance.getSelectedImageSetNames();
    if (savedImageSetNames.size() > 0)
    {
      for (String imageSetName : savedImageSetNames)
      {
        for (ImageSetData imageSetData : compatibleImageSets)
        {
          if (imageSetData.getImageSetName().equalsIgnoreCase(imageSetName))
          {
            _selectedImageSets.add(imageSetData);
            break;
          }
        }
      }
    }
    
    //prepare all preselected image set once recipe tab ready
    if(needToCollectImageSetData)
       _selectedImageSetsTableModel.populateWithImageSetData(compatibleImageSets, _selectedImageSets);
    else
    {
      _totalImageSetCount = compatibleImageSets.size();
    
      _imageSetUpdateThread.invokeLater(new Runnable()
      {
        public void run()
        {
          for (ImageSetData imageSetData : compatibleImageSets)
          {
            if(_continueToUpdateImageSet == false)
              break;
            try
            {
              imageSetData.setPercentOfCompatibleImages(_imageManager.calculateImageDataCompatibility(imageSetData, project.getName()));
              imageSetData.setImageSetDataLoadStatus(true);
            
              if(imageSetData.getPercentOfCompatibleImages() != 0)
              {
                _selectedImageSetsTableModel.insertRow(imageSetData,_selectedImageSets.contains(imageSetData));
              }
            
              _titledBorder.setTitle(StringLocalizer.keyToString("SELECTED_IMAGE_SETS_PANEL_TITLE_KEY")+"  (Loaded Image Set :"+ _selectedImageSetsTableModel.getRowCount()+"/"+_totalImageSetCount+") ");
              _selectedImageSetPanel.setBorder(_selectedImageSetPanelBorder);
              revalidate();
              repaint();
            }
            catch (final DatastoreException de)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                    de.getLocalizedMessage(),
                    StringLocalizer.keyToString("IMD_TITLE_KEY"),
                    true);
                }
              });
            }
          }
      
          if (_selectedImageSetsTableModel.isImageSetEmpty())
            _selectedImageSetsScrollPane.getViewport().add(_noImageSetsExistLabel);
          else
            _selectedImageSetsScrollPane.getViewport().add(_selectedImageSetsTable);
        
          _titledBorder.setTitle(StringLocalizer.keyToString("SELECTED_IMAGE_SETS_PANEL_TITLE_KEY"));
          _selectedImageSetPanel.setBorder(_selectedImageSetPanelBorder);
          revalidate();
          repaint();
        }
      });
    }
    
    setupTable();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void setupTable()
  {
    TableColumn test = _selectedImageSetsTable.getColumnModel().getColumn(0);
    CheckCellEditor cellEditor = new CheckCellEditor(_selectedImageSetsTable.getBackground(), _selectedImageSetsTable.getForeground());
    test.setCellEditor( cellEditor );
    test.setCellRenderer(new CheckCellRenderer());

    TableColumn selectedImageSetsNameCol = _selectedImageSetsTable.getColumnModel().getColumn(1);
    ImageSetRenderer imageSetRenderForSelectedImageSets = new ImageSetRenderer();
    selectedImageSetsNameCol.setCellRenderer(imageSetRenderForSelectedImageSets);

    TableColumn selectedImageSetsDateCol = _selectedImageSetsTable.getColumnModel().getColumn(2);
    DefaultTableCellRenderer selectedImageSetsDateColumnCenterRenderer = new DefaultTableCellRenderer();
    selectedImageSetsDateColumnCenterRenderer.setHorizontalAlignment( JLabel.CENTER );
    selectedImageSetsDateCol.setCellRenderer(selectedImageSetsDateColumnCenterRenderer);

    TableColumn selectedImageSetsPopulatedCol = _selectedImageSetsTable.getColumnModel().getColumn(3);
    DefaultTableCellRenderer selectedImageSetsPopulatedColumnCenterRenderer = new DefaultTableCellRenderer();
    selectedImageSetsPopulatedColumnCenterRenderer.setHorizontalAlignment( JLabel.CENTER );
    selectedImageSetsPopulatedCol.setCellRenderer(selectedImageSetsPopulatedColumnCenterRenderer);

    TableColumn selectedImageSetsVersionCol = _selectedImageSetsTable.getColumnModel().getColumn(4);
    DefaultTableCellRenderer selectedImageSetsVersionCenterRenderer = new DefaultTableCellRenderer();
    selectedImageSetsVersionCenterRenderer.setHorizontalAlignment( JLabel.CENTER );
    selectedImageSetsVersionCol.setCellRenderer(selectedImageSetsVersionCenterRenderer);

    TableColumn selectedImageSetsPercentCompatibleCol = _selectedImageSetsTable.getColumnModel().getColumn(5);
    DefaultTableCellRenderer selectedImageSetsPercentCompatibleCenterRenderer = new DefaultTableCellRenderer();
    selectedImageSetsPercentCompatibleCenterRenderer.setHorizontalAlignment( JLabel.CENTER );
    selectedImageSetsPercentCompatibleCol.setCellRenderer(selectedImageSetsPercentCompatibleCenterRenderer);

    // set up column widths
    ColumnResizer.adjustColumnPreferredWidths(_selectedImageSetsTable, true);

    int padding = 175;
    int defaultHeight = 125;
    int origTableWidth = _selectedImageSetsTable.getMinimumSize().width;
    Dimension prefScrollSize = new Dimension(origTableWidth + padding, defaultHeight);
    _selectedImageSetsTable.setPreferredScrollableViewportSize(prefScrollSize);

    // now lets set up the % of relestate for each column.
    // set up column widths
    int iTotalColumnWidth = _selectedImageSetsTable.getColumnModel().getTotalColumnWidth();
    _selectedImageSetsTable.getColumnModel().getColumn(0).setPreferredWidth((int)(iTotalColumnWidth * .10));
    _selectedImageSetsTable.getColumnModel().getColumn(1).setPreferredWidth((int)(iTotalColumnWidth * .80));
    _selectedImageSetsTable.getColumnModel().getColumn(2).setPreferredWidth((int)(iTotalColumnWidth * .20));
    _selectedImageSetsTable.getColumnModel().getColumn(3).setPreferredWidth((int)(iTotalColumnWidth * .10));
    _selectedImageSetsTable.getColumnModel().getColumn(4).setPreferredWidth((int)(iTotalColumnWidth * .15));
    _selectedImageSetsTable.getColumnModel().getColumn(5).setPreferredWidth((int)(iTotalColumnWidth * .15));
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(Observable observable, Object object)
  {
    Assert.expect(observable != null, " observable is null");

    // this should be an update from something already on the swing thread. if not then assert.
    Assert.expect(SwingUtilities.isEventDispatchThread(), "update not from a swing thread");

    if (observable instanceof GuiObservable)
    {
      GuiEventEnum imageSetEnum = ((GuiEvent)object).getGuiEventEnum();
      if (imageSetEnum instanceof ImageSetEventEnum)
      {
        if(imageSetEnum.equals(ImageSetEventEnum.IMAGE_SET_CREATED))
        {
          // the user has created a new image set - lets add it to the available image sets table
          Assert.expect(object != null, "Object is null");
          Assert.expect(object instanceof GuiEvent, "Object is not of type Gui Event");
          Assert.expect(((GuiEvent)object).getSource() instanceof ImageSetData, ((GuiEvent)object).getSource().getClass().toString());
          final ImageSetData newImageSetData = (ImageSetData)((GuiEvent)object).getSource();

          // if an image set generation was aborted, we still get this event.  So, we should check to see if the image set
          // we got as the parameter actually exists on disk before adding to the list of available image sets
          String name = newImageSetData.getImageSetName();
          if (FileUtil.exists(FileName.getImageSetInfoFullPath(newImageSetData.getProjectName(), name)) == false)
            return;  // nope -- didn't exist.  Bail.

          // add it to the available image Set table
          _selectedImageSetsTableModel.insertRow(newImageSetData, false);

          if (_createTable == false)
          {
            _createTable = true;
            // this is the first image set created so lets add the table back in
            _selectedImageSetsScrollPane.getViewport().removeAll();
            _selectedImageSetsScrollPane.getViewport().add(_selectedImageSetsTable);
            _selectedImageSetPanel.remove(_selectedImageSetsScrollPane);
            _selectedImageSetPanel.add(_selectedImageSetsScrollPane);

            _clearButton.setEnabled(true);
            _selectAllButton.setEnabled(true);
            repaint();
            validate();

          }
          setupTable();
        }
        else if(imageSetEnum.equals(ImageSetEventEnum.IMAGE_SET_DELETED))
        {
          Assert.expect(object != null, "Object is null");
          Assert.expect(object instanceof GuiEvent, "Object is not of type Gui Event");
          Assert.expect(((GuiEvent)object).getSource() instanceof java.util.List, "not of type List<ImageSetData>");
          final java.util.List<ImageSetData> deletedImageSets = (java.util.List<ImageSetData>) ((GuiEvent)object).getSource();

          // the user deleted one or more image sets lets find them and remove them
          for (ImageSetData imageSet : deletedImageSets)
          {
            if (_selectedImageSetsTableModel.contains(imageSet))
            {
              // the selected image sets table contains the deleted image set.. lets delete it and refresh it.
              _selectedImageSetsTableModel.removeImageSet(imageSet);
              _titledBorder.setTitle(StringLocalizer.keyToString("SELECTED_IMAGE_SETS_PANEL_TITLE_KEY")+"  (Loaded Image Set :"+ _selectedImageSetsTableModel.getRowCount()+"/"+ --_totalImageSetCount+") ");
              _selectedImageSetPanel.setBorder(_selectedImageSetPanelBorder);
              revalidate();
              repaint();
            }
          }
          if (_selectedImageSetsTableModel.getImageSets().size() == 0)
          {
            // now if this is the last image set to be delete lets notify the user that they do not have any
            // image sets on disk for this project
            _persistance.clearSelectedImageSets();
            GuiObservable.getInstance().stateChanged(null, ImageSetEventEnum.IMAGE_SET_NOT_SELECTED);
            _createTable = false;
            _selectedImageSetsScrollPane.getViewport().remove(_selectedImageSetsTable);
            _selectedImageSetsScrollPane.getViewport().add(_noImageSetsExistLabel);
            _clearButton.setEnabled(false);
            _selectAllButton.setEnabled(false);
            repaint();
            validate();
          }
          else
            setupTable();
        }
        else if(imageSetEnum.equals(ImageSetEventEnum.IMAGE_SET_CLEAR_ALL))
        {
          _selectedImageSetsTableModel.clearSelectedImageSets();
        }
      }
    }
    else
      Assert.expect(false, "observable is not recongized");

  }
  
  /**
   * @author Swee Yee Wong
   * get selected image sets
   */
  public java.util.List<ImageSetData> getSelectedImageSets()
  {
    Assert.expect(_selectedImageSets!=null);
    return _selectedImageSets;
  }
}
