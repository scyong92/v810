package com.axi.v810.gui.imagePane;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.algoTuner.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import java.awt.Shape;
import java.awt.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.lang.reflect.*;
import java.util.*;
import javax.swing.*;

/**
 * The FineTuningImageGraphicsEngineSetup class sets up the graphics engine
 * for image display during fine tuning.
 * @author George A. David
 */
public class FineTuningImageGraphicsEngineSetup implements Observer, ImagePanelGraphicsEngineSetupInt
{
  private static final int _BORDER_SIZE_IN_PIXELS = 0;
  private static final double _ZOOM_FACTOR = 1.2;

  private GraphicsEngine _graphicsEngine;
  private ImageManagerToolBar _toolBar;
  private Stroke _stroke = new BasicStroke(2);
  // width of outline for component level text message
  private int _outlineWidth = 5;

  // Graphic engine layers
  private int _imageLayer;
  private int _textBackgroundLayer;
  private int _passingOutlineLayer;
  private int _failingOutlineLayer;
  private int _textLayer;
  private int _diagnosticMeasurementLayer;
  private int _solderAreaGraphicsLayer;
  private int _expectedImageGraphicsLayer;
  private int _voidingGraphicsLayer;
  private int _largestVoidingGraphicsLayer;
  private int _wettingCoverageGraphicsLayer;
  private int _backgroundPixelsGraphicsLayer;
  private java.util.List<Integer> _diagnosticLayers = new LinkedList<Integer>();
  private java.util.List<Integer> _allLayers = new LinkedList<Integer>();

  private SortedMap<MeasurementRegionEnum, Integer> _measurementRegionEnumToGraphicsLayerNumberMap = new TreeMap<MeasurementRegionEnum, Integer>();
  private InspectionEventObservable _inspectionObservable = null;
  private GuiObservable _guiObservable = null;

  private boolean _isGraphicsEngineReady = false;

  private ReconstructionRegion _currentReconstructionRegion = null;
  private SliceNameEnum _currentSliceNameEnum = null;
  private String _currentJointName = "";
  private Map<SliceNameEnum, BufferedImage>_allSliceBufferedImage = null;

  private boolean _isImageStayPersistent = false;
  //Anthony01005
  private boolean _isGraphicsShouldBeFitToScreen = false;
  private FineTuningImagePanel _fineTuningImagePanel = null;
  private InspectionImageRenderer _inspectionImageRenderer = null;
  
  //Ying-Huan.Chu;
  private int _currentCameraSliceNameEnumId;
 
  private String _sharpnessProfileChartTitle = StringLocalizer.keyToString("SHARPNESS_PROFILE_CHART_TITLE_KEY");
  private String _xLabel = "";
  private String _yLabel = StringLocalizer.keyToString("SHARPNESS_PROFILE_CHART_Y_LABEL_KEY");
  private java.util.List<Float>_zHeightInNanometerList = null;
  private java.util.List<Float>_sharpnessList = null;
  private int _zHeightInNanos = 0;
  
  // bee-hoon.goh
  private com.axi.v810.business.panelDesc.Component _component = null;
  private SliceNameEnum _sliceNameEnum = null;
  private ImageManager _imageManager;
  private ImageSetData _imageSetData = null;
  private BufferedImage _lastBufferedImage = null;
  private MeasurementRegionDiagnosticInfo _lastMeasurementDiagInfo;
  private MeasurementRegionDiagnosticInfo _componentMeasurementDiagInfo;
  private boolean _has2DImages = false;
  
  //Lim, Lay Ngor - XCR1652 - Image Post Processing
  InspectionImageRenderer _previousInspectionImageRenderer;
 
  /**
   * @author George A. David
   */
  public FineTuningImageGraphicsEngineSetup(ImageManagerToolBar toolBar)
  {
    Assert.expect(toolBar != null);

    _toolBar = toolBar;
    _graphicsEngine = new GraphicsEngine(_BORDER_SIZE_IN_PIXELS);
    _graphicsEngine.setShouldGraphicsBeFitToScreen(false);
    _graphicsEngine.setShouldGraphicsBeCentered(true);
    _inspectionObservable = InspectionEventObservable.getInstance();
    _guiObservable = GuiObservable.getInstance();
    _imageManager = ImageManager.getInstance();
    createLayers();
    setColors();
  }

  /**
   * @author George A. David
   */
  private void setUpToolBar()
  {
    _toolBar.setAllowDragGraphics(true);
    _toolBar.setAllowMeasurements(_isGraphicsEngineReady);
    _toolBar.setAllowResetGraphics(_isGraphicsEngineReady);
    _toolBar.setAllowToggleGraphics(_isGraphicsEngineReady);
    _toolBar.setAllowToggleImageStay(_isGraphicsEngineReady);
    //Anthony01005
    _toolBar.setAllowToggleImageFitToScreen(_isGraphicsEngineReady);

    _toolBar.setAllowViewBottomSide(false);
    _toolBar.setAllowViewTopSide(false);

    _toolBar.setAllowZoomIn(_isGraphicsEngineReady);
    _toolBar.setAllowZoomOut(_isGraphicsEngineReady);
    _toolBar.setAllowZoomRectangle(_isGraphicsEngineReady);
  }

  /**
   * @author George A. David
   */
  private void setToolBarEnabled(boolean enabled)
  {
    _toolBar.setAllowDragGraphics(enabled);
    _toolBar.setAllowMeasurements(enabled);
    _toolBar.setAllowToggleGraphics(enabled);
    _toolBar.setAllowToggleImageStay(enabled);
    //Anthony01005
    _toolBar.setAllowToggleImageFitToScreen(enabled);
    _toolBar.setAllowResetGraphics(enabled);

    _toolBar.setAllowZoomIn(enabled);
    _toolBar.setAllowZoomOut(enabled);
    _toolBar.setAllowZoomRectangle(enabled);
  }

  /**
   * @author George A. David
   */
  private void clearRenderers()
  {
    _graphicsEngine.removeRenderers(_allLayers);
    setColors();
  }

  /**
   * @author George A. David
   */
  public void display()
  {
    setUpToolBar();
    MathUtilEnum displayUnits = Project.getCurrentlyLoadedProject().getDisplayUnits();
    if(displayUnits.equals(MathUtilEnum.NANOMETERS) == false)
      _graphicsEngine.setMeasurementInfo(MathUtil.convertUnits(MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel(), MathUtilEnum.NANOMETERS, displayUnits),
                                         MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits), MeasurementUnits.getMeasurementUnitString(displayUnits));
    _graphicsEngine.setAxisInterpretation(true, true);
    _graphicsEngine.setMouseEventsEnabled(true);
    _inspectionObservable.addObserver(this);
    _guiObservable.addObserver(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _graphicsEngine.removeRenderers(_allLayers);
    setColors();
    finish();
  }

  /**
   * @author George A. David
   */
  public void finish()
  {
    
    setToolBarEnabled(false);
    _inspectionObservable.deleteObserver(this);
    _guiObservable.deleteObserver(this);
  }

  /**
   * @author Anthony Fong //Anthony01005
   */
  public void setFineTuningImagePanel(FineTuningImagePanel fineTuningImagePanel)
  {
    Assert.expect(fineTuningImagePanel != null);

    _fineTuningImagePanel = fineTuningImagePanel;
  }
  public FineTuningImagePanel getFineTuningImagePanel()
  {
    Assert.expect(_fineTuningImagePanel != null);

    return _fineTuningImagePanel;
  }

  /**
   * @author George A. David
   */
  public GraphicsEngine getGraphicsEngine()
  {
    Assert.expect(_graphicsEngine != null);

    return _graphicsEngine;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isImageDisplayed()
  {
    if (_currentReconstructionRegion == null)
      return false;
    else
      return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public ReconstructionRegion getCurrentImageRegion()
  {
    Assert.expect(_currentReconstructionRegion != null);
    return _currentReconstructionRegion;
  }

  /**
   * @author Andy Mechtenberg
   */
  public SliceNameEnum getCurrentSliceNameEnum()
  {
    Assert.expect(_currentSliceNameEnum != null);
    return _currentSliceNameEnum;
  }

  /*
   * @author Kee Chin Seong
   */
  public String getCurrentJointName()
  {
    Assert.expect(_currentJointName.equals("") == false);
    return _currentJointName;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleUpdate(final Observable observable, final Object argument)
  {
    try
    {
      _fineTuningImagePanel.setToggle2DImageButtonSelected(false);
      _fineTuningImagePanel.setEnablePrevious2DImageButton(false);
      _fineTuningImagePanel.setEnableNext2DImageButton(false);
      if(_fineTuningImagePanel.isToggleViewComponentImageButtonSelected() ||
         _fineTuningImagePanel.isToggleViewEnhancedComponentImageButtonSelected()) //Siew Yeng - XCR-2761
        displayOriginalImagePadSelectionImage();
      _fineTuningImagePanel.setToggleViewComponentImageButtonSelected(false);
      _fineTuningImagePanel.setToggleViewEnhancedComponentImageButtonSelected(false); //Siew Yeng - XCR-2761
      //Siew Yeng
      //_graphicsEngine.resetMinimumZoomFactorForNormalImageView();
      _graphicsEngine.setMouseEventsEnabled(true);
      setToolBarEnabled(true);
	  if(_toolBar.isToggleImageFitToScreenButtonSelected() == false)
        setImageFitToScreen(false);
      
      if (observable instanceof InspectionEventObservable)
      {
        if (argument instanceof AlgorithmDiagnosticInspectionEvent)
        {
          AlgorithmDiagnosticInspectionEvent algorithmDiagEvent = (AlgorithmDiagnosticInspectionEvent)argument;
          if (algorithmDiagEvent.hasAlgorithmDiagnosticMessage())
          {
            displayImageAndGraphics(algorithmDiagEvent.getAlgorithmDiagnosticMessage());
          }
        }
        else if (argument instanceof InspectingImageInspectionEvent)
        {
          InspectingImageInspectionEvent imageInspectionEvent = (InspectingImageInspectionEvent)argument;
          if (imageInspectionEvent.getInspectionEventEnum().equals(InspectionEventEnum.BEGIN_INSPECTING_IMAGE))
          {
            MagnificationTypeEnum magnificationTypeEnum = imageInspectionEvent.getInspectionRegion().getSubtypes().iterator().next().getSubtypeAdvanceSettings().getMagnificationType();
            
            MathUtilEnum displayUnits = Project.getCurrentlyLoadedProject().getDisplayUnits();
            if (displayUnits.equals(MathUtilEnum.NANOMETERS) == false)
            {
              if(magnificationTypeEnum.equals(MagnificationTypeEnum.HIGH))
                _graphicsEngine.setMeasurementInfo(MathUtil.convertUnits(MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel(), MathUtilEnum.NANOMETERS, displayUnits),
                  MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits), MeasurementUnits.getMeasurementUnitString(displayUnits));
              else
                _graphicsEngine.setMeasurementInfo(MathUtil.convertUnits(MagnificationEnum.NOMINAL.getNanoMetersPerPixel(), MathUtilEnum.NANOMETERS, displayUnits),
                  MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits), MeasurementUnits.getMeasurementUnitString(displayUnits));
              
            }

            Subtype subtype = imageInspectionEvent.getInspectionRegion().getSubtypes().iterator().next();
            com.axi.util.image.Image imageToDisplay = imageInspectionEvent.getImageToDisplay();
            
            //Siew Yeng - XCR-2218 - Increase inspectable pad size
            // XCR-3050 Resize 2,3 and 4 is not expected result
            resetMinimumZoomFactorIfNecessary(imageToDisplay.getBufferedImage(),magnificationTypeEnum, subtype);
            
            displayImage(imageToDisplay);
            //Siew Yeng
            _lastBufferedImage = imageToDisplay.getBufferedImage();
            
            imageToDisplay.decrementReferenceCount();
            _currentReconstructionRegion = imageInspectionEvent.getInspectionRegion();
            _currentSliceNameEnum = imageInspectionEvent.getSliceNameEnum();
          }
        }
      }
      else if (observable instanceof GuiObservable)
      {
        GuiEvent guiEvent = (GuiEvent)argument;
        GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
        if (guiEventEnum instanceof SelectionEventEnum)
        {
          SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
          if (selectionEventEnum.equals(SelectionEventEnum.INSPECTION_IMAGE_CLEAR_SELECTION))
          {
            clearRenderers();
            clearSharpnessProfile(); // Ying-Huan.Chu
			clearComponentImageState();
          }
          else if (selectionEventEnum.equals(SelectionEventEnum.INSPECTION_IMAGE_PAD_SELECTION))
          {
            //                _graphicsEngine.reset();
            ImagePadSelection imagePadSelection = (ImagePadSelection)guiEvent.getSource();
            String jointName = imagePadSelection.getJointName();
            
            //Siew Yeng - XCR-2218 - Increase inspectable pad size
            MagnificationTypeEnum magnificationType = imagePadSelection.getSelectedComponent().getComponentType().getMagnificationType();
            // XCR-3050 Resize 2,3 and 4 is not expected result
            Subtype subtype = imagePadSelection.getReconstructionRegion().getSubtypes().iterator().next();
            resetMinimumZoomFactorIfNecessary(imagePadSelection.getBufferedImage(), magnificationType, subtype);
            
            displayImage(imagePadSelection.getBufferedImage());
            
            // Ying-Huan.Chu
            if (imagePadSelection.getZHeightInNanometerList().isEmpty() == false &&
                imagePadSelection.getSharpnessList().isEmpty() == false)
            {
              _zHeightInNanometerList = imagePadSelection.getZHeightInNanometerList();
              _sharpnessList = imagePadSelection.getSharpnessList();
              _zHeightInNanos = imagePadSelection.getZHeightInNanos();
            }
            _fineTuningImagePanel.setEnableSharpnessProfileButton(true);
            
            _currentReconstructionRegion = imagePadSelection.getReconstructionRegion();
            _currentSliceNameEnum = imagePadSelection.getSliceNameEnum();
            _currentJointName = imagePadSelection.getJointName();
            _allSliceBufferedImage = imagePadSelection.getAllSliceBufferedImage();
            has2DImages(_allSliceBufferedImage);
            
            // bee-hoon.goh
            _lastBufferedImage = imagePadSelection.getBufferedImage();
            _component = imagePadSelection.getSelectedComponent();
            _sliceNameEnum = imagePadSelection.getSliceNameEnum();
            _imageSetData = imagePadSelection.getImageSetData();
            if (_component.isInspected())
              _componentMeasurementDiagInfo = imagePadSelection.getComponentMeasurementDiagInfo();
            if (_component.getReconstructionRegionNameList().size() > 1)
              _fineTuningImagePanel.setEnableToggleViewComponentImageButton(true);
            else 
              _fineTuningImagePanel.setEnableToggleViewComponentImageButton(false);
            
            MeasurementRegionDiagnosticInfo measurementDiagInfo = imagePadSelection.getMeasurementDiagInfo();
            displayPassingFailingPadGraphics(measurementDiagInfo);
			_lastMeasurementDiagInfo = measurementDiagInfo;
          }
          else if (selectionEventEnum.equals(SelectionEventEnum.INSPECTION_IMAGE_COMPONENT_SELECTION))
          {
            //                _graphicsEngine.reset();
            ImagePadSelection imagePadSelection = (ImagePadSelection)guiEvent.getSource();
            String jointName = imagePadSelection.getJointName();
            
            //Siew Yeng - XCR-2218 - Increase inspectable pad size
            MagnificationTypeEnum magnificationType = imagePadSelection.getSelectedComponent().getComponentType().getMagnificationType();
            // XCR-3050 Resize 2,3 and 4 is not expected result
            Subtype subtype = imagePadSelection.getReconstructionRegion().getSubtypes().iterator().next();
            resetMinimumZoomFactorIfNecessary(imagePadSelection.getBufferedImage(), magnificationType, subtype);
            
            displayImage(imagePadSelection.getBufferedImage());

            // Ying-Huan.Chu
            if (imagePadSelection.getZHeightInNanometerList().isEmpty() == false &&
                imagePadSelection.getSharpnessList().isEmpty() == false)
            {
              _zHeightInNanometerList = imagePadSelection.getZHeightInNanometerList();
              _sharpnessList = imagePadSelection.getSharpnessList();
              _zHeightInNanos = imagePadSelection.getZHeightInNanos();
            }
            _fineTuningImagePanel.setEnableSharpnessProfileButton(true);
            _currentReconstructionRegion = imagePadSelection.getReconstructionRegion();
            _currentSliceNameEnum = imagePadSelection.getSliceNameEnum();
            _allSliceBufferedImage = imagePadSelection.getAllSliceBufferedImage();
            has2DImages(_allSliceBufferedImage);

            // bee-hoon.goh
            _lastBufferedImage = imagePadSelection.getBufferedImage();
            _component = imagePadSelection.getSelectedComponent();
            _sliceNameEnum = imagePadSelection.getSliceNameEnum();
            _imageSetData = imagePadSelection.getImageSetData();
            if (_component.isInspected())
              _componentMeasurementDiagInfo = imagePadSelection.getComponentMeasurementDiagInfo();
            if (_component.getReconstructionRegionNameList().size() > 1)
              _fineTuningImagePanel.setEnableToggleViewComponentImageButton(true);
            else 
              _fineTuningImagePanel.setEnableToggleViewComponentImageButton(false);
           
            MeasurementRegionDiagnosticInfo measurementDiagInfo = imagePadSelection.getMeasurementDiagInfo();
            displayPassingFailingComponentGraphics(measurementDiagInfo);
			_lastMeasurementDiagInfo = measurementDiagInfo;
          }
          else if (selectionEventEnum.equals(SelectionEventEnum.INSPECTION_IMAGE_NO_IMAGE_SELECTION))
          {
            try
            {
              com.axi.util.image.Image image =
                  XrayImageIoUtil.loadJpegImage(FileName.getNoInspectionImageFullPath());
              displayImage(image);
              image.decrementReferenceCount();
            }
            catch (DatastoreException de)
            {
              MessageDialog.reportIOError(MainMenuGui.getInstance(), de.getLocalizedMessage());
            }
          }
        }
      }
      else
        Assert.expect(false);

      //Lim, Lay Ngor - XCR1652 - Image Post Processing - START
      _fineTuningImagePanel.setEnableImageEnhancerButton(true);
      //Update the current subtype and previous image renderer for post processing use.
      if (_previousInspectionImageRenderer != null)
      {
        if (_currentReconstructionRegion != null)
          _fineTuningImagePanel.updateCurrentSubtypeAndRenderer(_currentReconstructionRegion.getSubtypes().iterator().next(), _previousInspectionImageRenderer);
        else
          _fineTuningImagePanel.updateCurrentSubtypeAndRenderer(null, _previousInspectionImageRenderer);
      }
       //Lim, Lay Ngor - XCR1652 - Image Post Processing- END      
    }
    finally
    {
      if (argument instanceof InspectionEvent)
        ((InspectionEvent)argument).decrementReferenceCount();
    }
  }

  /**
   * @author George A. David
   */
  public synchronized void update(final Observable observable, final Object argument)
  {

    if (argument instanceof InspectionEvent)
    {
      ((InspectionEvent)argument).incrementReferenceCount();
    }

    try
    {
      // PE:  I am using invokeAndWait() here because we have to handle the algorithm
      // events as they occur.  Otherwise, the algorithms get out of sync with
      // the GUI and can build up a big backlog of events, which the swing thread
      // then processes in chunks at a time.  This makes the user experience very
      // herky jerky and also makes it difficult to debug behavior in the algorithms.
      if (SwingUtilities.isEventDispatchThread())
      {
        // will decrement the reference count of the inspection event.
        handleUpdate(observable, argument);
      }
      else
      {
        SwingUtils.invokeAndWait(new Runnable()
        {
          public void run()
          {
            // will decrement the reference count of the inspection event.
            handleUpdate(observable, argument);
          }
        });
      }
    }
    catch (InterruptedException iex)
    {
      Assert.expect(false);
    }
    catch (InvocationTargetException itex)
    {
      Assert.expect(false);
    }
  }

  /**
   * @author Matt Wharton
   */
  private void refreshGraphicsEngine()
  {
    setColors();
    _graphicsEngine.setAxisInterpretation(true, true);
    _graphicsEngine.center();
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  private void displayImageAndGraphics(AlgorithmDiagnosticMessage algorithmDiagMessage)
  {
    if (algorithmDiagMessage.hasAnyGraphicalDiagnosticInfo())
    {
      if (algorithmDiagMessage.arePreviousDiagnosticsInvalidated())
      {
        _graphicsEngine.removeRenderers(_diagnosticLayers);
        setColors();
      }
      displayGraphics(algorithmDiagMessage);
    }
  }

  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  private void displayImage(com.axi.util.image.Image image)
  {
    Assert.expect(image != null);

    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    _previousInspectionImageRenderer = _inspectionImageRenderer;
  
    clearRenderers();
    clearSharpnessProfile(); // Ying-Huan.Chu
    //Anthony01005
    _inspectionImageRenderer = new InspectionImageRenderer(image);
    _inspectionImageRenderer.setBicubicInterpolation(_isGraphicsShouldBeFitToScreen);
    _graphicsEngine.addRenderer(_imageLayer, _inspectionImageRenderer);
    //_graphicsEngine.addRenderer(_imageLayer, new InspectionImageRenderer(image));
    if (_isImageStayPersistent==false)
      if (_isGraphicsShouldBeFitToScreen==false)  //Anthony01005
        resetGraphics();
    
    if (_isGraphicsShouldBeFitToScreen==true)
    {
      //Anthony01005
      double widthZoomFactor = (double)_graphicsEngine.getWidth()/(double)image.getWidth();
      double heightZoomFactor = (double)_graphicsEngine.getHeight()/(double)image.getHeight();
      if(widthZoomFactor>heightZoomFactor)
      {
        setZoomFactor(heightZoomFactor);
      }
      else
      {
        setZoomFactor(widthZoomFactor);
      }
      _graphicsEngine.center();
    }
    _isGraphicsEngineReady = true;
    setToolBarEnabled(true);
  }


  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  private void displayImage(BufferedImage bufferedImage)
  {
    Assert.expect(bufferedImage != null);

    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    _previousInspectionImageRenderer = _inspectionImageRenderer;    

    clearRenderers();
    clearSharpnessProfile(); // Ying-Huan.Chu
    //Anthony01005
    _inspectionImageRenderer = new InspectionImageRenderer(bufferedImage);
    _inspectionImageRenderer.setBicubicInterpolation(_isGraphicsShouldBeFitToScreen);
    _graphicsEngine.addRenderer(_imageLayer, _inspectionImageRenderer);

    //_graphicsEngine.addRenderer(_imageLayer, new InspectionImageRenderer(bufferedImage));
    if (_isImageStayPersistent==false)
      if (_isGraphicsShouldBeFitToScreen==false)  //Anthony01005
        resetGraphics();

    if (_isGraphicsShouldBeFitToScreen==true)
    {
      //Anthony01005
      double widthZoomFactor = (double)_graphicsEngine.getWidth()/(double)bufferedImage.getWidth();
      double heightZoomFactor = (double)_graphicsEngine.getHeight()/(double)bufferedImage.getHeight();
      if(widthZoomFactor>heightZoomFactor)
      {
        setZoomFactor(heightZoomFactor);
      }
      else
      {
        setZoomFactor(widthZoomFactor);
      }
      _graphicsEngine.center();
    }
    _isGraphicsEngineReady = true;
    setToolBarEnabled(true);
  }


  /**
   * @author George A. David
   * @author Patrick Lacz
   */
  private void displayGraphics(AlgorithmDiagnosticMessage algorithmDiagMessage)
  {
    boolean haveModifiedGraphics = false;
    boolean repaintAll = false;
    Rectangle repaintRect = null;
    for (DiagnosticInfo diagnosticInfo : algorithmDiagMessage.getDiagnosticInfoList())
    {
      if (diagnosticInfo instanceof MeasurementRegionDiagnosticInfo)
      {
        MeasurementRegionDiagnosticInfo measurementDiagInfo = (MeasurementRegionDiagnosticInfo)diagnosticInfo;
        Shape shapeToRender = measurementDiagInfo.getDiagnosticRegion();
        MeasurementRegionEnum measurementRegionEnum = measurementDiagInfo.getMeasurementRegionEnum();
        RegionOfInterestRenderer renderer = new RegionOfInterestRenderer(shapeToRender);
        renderer.setStroke(_stroke);

        Integer graphicsLayer = _measurementRegionEnumToGraphicsLayerNumberMap.get(measurementRegionEnum);
        Assert.expect(graphicsLayer != null);
        _graphicsEngine.addRenderer(graphicsLayer, renderer);
        if(repaintRect == null)
          repaintRect = renderer.getBounds();
        else
          repaintRect.add(renderer.getBounds());

        if (measurementDiagInfo.hasLabel())
        {
          Rectangle2D shapeBounds = shapeToRender.getBounds2D();
          TextRenderer textRenderer = new TextRenderer(false);
          textRenderer.initializeText(measurementDiagInfo.getLabel(), shapeBounds.getCenterX(), shapeBounds.getCenterY(), true);
          _graphicsEngine.addRenderer(graphicsLayer, textRenderer);
          repaintRect.add(textRenderer.getBounds());
        }
        haveModifiedGraphics = true;
      }
      else if (diagnosticInfo instanceof InspectionImageDiagnosticInfo)
      {
        InspectionImageDiagnosticInfo imageDiagInfo = (InspectionImageDiagnosticInfo)diagnosticInfo;
        
        //Siew Yeng - XCR-2218 - Increase inspectable pad size
        MagnificationTypeEnum magnificationType = _currentReconstructionRegion.getComponent().getComponentType().getMagnificationType();
        // XCR-3050 Resize 2,3 and 4 is not expected result
        Subtype subtype = _currentReconstructionRegion.getSubtypes().iterator().next();
        resetMinimumZoomFactorIfNecessary(imageDiagInfo.getImage().getBufferedImage(), magnificationType, subtype);
        
        displayImage(imageDiagInfo.getImage());
        repaintAll = true;
        haveModifiedGraphics = true;
      }
      else if (diagnosticInfo instanceof OverlaySolderImageDiagnosticInfo)
      {
        OverlaySolderImageDiagnosticInfo overlaySolderDiagInfo = (OverlaySolderImageDiagnosticInfo)diagnosticInfo;
        OverlayImageRenderer renderer = new OverlayImageRenderer(overlaySolderDiagInfo.getOverlayImage(),
            overlaySolderDiagInfo.getRegionOfInterestInInspectionImage(), LayerColorEnum.SOLDER_AREA_PIXELS.getColor());
        renderer.setStroke(_stroke);
        _graphicsEngine.addRenderer(_solderAreaGraphicsLayer, renderer);

        if(repaintRect == null)
          repaintRect = renderer.getBounds();
        else
          repaintRect.add(renderer.getBounds());
        haveModifiedGraphics = true;
      }
      else if (diagnosticInfo instanceof OverlayExpectedImageDiagnosticInfo)
      {
        OverlayExpectedImageDiagnosticInfo overlayExpectedDiagInfo = (OverlayExpectedImageDiagnosticInfo)diagnosticInfo;
        OverlayImageRenderer renderer = new OverlayImageRenderer(overlayExpectedDiagInfo.getOverlayImage(),
            overlayExpectedDiagInfo.getRegionOfInterestInInspectionImage(), LayerColorEnum.EXPECTED_IMAGE_PIXELS.getColor());
        renderer.setStroke(_stroke);
        _graphicsEngine.addRenderer(_expectedImageGraphicsLayer, renderer);

        if(repaintRect == null)
          repaintRect = renderer.getBounds();
        else
          repaintRect.add(renderer.getBounds());
        haveModifiedGraphics = true;
      }
      else if (diagnosticInfo instanceof OverlayImageDiagnosticInfo)
      {
        OverlayImageDiagnosticInfo overlayDiagInfo = (OverlayImageDiagnosticInfo)diagnosticInfo;
        OverlayImageRenderer renderer = new OverlayImageRenderer(overlayDiagInfo.getOverlayImage(),
            overlayDiagInfo.getRegionOfInterestInInspectionImage(), LayerColorEnum.VOIDING_PIXELS.getColor());
        renderer.setStroke(_stroke);
        _graphicsEngine.addRenderer(_voidingGraphicsLayer, renderer);

        if(repaintRect == null)
          repaintRect = renderer.getBounds();
        else
          repaintRect.add(renderer.getBounds());
        
        //Siew Yeng - XCR-2566 - Display Voiding Percentage Value on Diagnostic Image
        if (overlayDiagInfo.hasMeasurementLabel())
        {
          Rectangle2D shapeBounds = overlayDiagInfo.getRegionOfInterestInInspectionImage().getShape().getBounds2D();
          
          TextRenderer textRenderer = new TextRenderer(false, FontUtil.getRendererFont(16));
          textRenderer.initializeText(overlayDiagInfo.getMeasurementLabel(), 
                                      shapeBounds.getCenterX(), 
                                      shapeBounds.getMinY() - 10, 
                                      true);
          _graphicsEngine.addRenderer(_diagnosticMeasurementLayer, textRenderer);
          repaintRect.add(textRenderer.getBounds());
        }
        haveModifiedGraphics = true;
      }
      else if (diagnosticInfo instanceof OverlayLargestVoidImageDiagnosticInfo)
      {
        OverlayLargestVoidImageDiagnosticInfo overlayDiagInfo = (OverlayLargestVoidImageDiagnosticInfo)diagnosticInfo;
        OverlayImageRenderer renderer = new OverlayImageRenderer(overlayDiagInfo.getOverlayImage(),
            overlayDiagInfo.getRegionOfInterestInInspectionImage(), LayerColorEnum.LARGEST_VOIDING_PIXELS.getColor());
        renderer.setStroke(_stroke);
        _graphicsEngine.addRenderer(_largestVoidingGraphicsLayer, renderer);
    
        if(repaintRect == null)
          repaintRect = renderer.getBounds();
        else
          repaintRect.add(renderer.getBounds());
        haveModifiedGraphics = true;
      }
      else if (diagnosticInfo instanceof OverlayBackgroundImageDiagnosticInfo)
      {
        OverlayBackgroundImageDiagnosticInfo overlayBackgroundDiagInfo = (OverlayBackgroundImageDiagnosticInfo)diagnosticInfo;
        OverlayImageRenderer renderer = new OverlayImageRenderer(overlayBackgroundDiagInfo.getOverlayImage(),
            overlayBackgroundDiagInfo.getRegionOfInterestInInspectionImage(), LayerColorEnum.BACKGROUND_PIXELS.getColor());
        renderer.setStroke(_stroke);
        _graphicsEngine.addRenderer(_backgroundPixelsGraphicsLayer, renderer);

        if(repaintRect == null)
          repaintRect = renderer.getBounds();
        else
          repaintRect.add(renderer.getBounds());
        haveModifiedGraphics = true;
      }
      else if (diagnosticInfo instanceof OverlaySolderImageDiagnosticInfo)
      {
        OverlaySolderImageDiagnosticInfo overlaySolderDiagInfo = (OverlaySolderImageDiagnosticInfo)diagnosticInfo;
        OverlayImageRenderer renderer = new OverlayImageRenderer(overlaySolderDiagInfo.getOverlayImage(),
            overlaySolderDiagInfo.getRegionOfInterestInInspectionImage(), LayerColorEnum.SOLDER_AREA_PIXELS.getColor());
        renderer.setStroke(_stroke);
        _graphicsEngine.addRenderer(_solderAreaGraphicsLayer, renderer);

        if(repaintRect == null)
          repaintRect = renderer.getBounds();
        else
          repaintRect.add(renderer.getBounds());
        haveModifiedGraphics = true;
      }
      else if (diagnosticInfo instanceof OverlayExpectedImageDiagnosticInfo)
      {
        OverlayExpectedImageDiagnosticInfo overlayExpectedDiagInfo = (OverlayExpectedImageDiagnosticInfo)diagnosticInfo;
        OverlayImageRenderer renderer = new OverlayImageRenderer(overlayExpectedDiagInfo.getOverlayImage(),
            overlayExpectedDiagInfo.getRegionOfInterestInInspectionImage(), LayerColorEnum.EXPECTED_IMAGE_PIXELS.getColor());
        renderer.setStroke(_stroke);
        _graphicsEngine.addRenderer(_expectedImageGraphicsLayer, renderer);

        if(repaintRect == null)
          repaintRect = renderer.getBounds();
        else
          repaintRect.add(renderer.getBounds());
        haveModifiedGraphics = true;
      }
      else if (diagnosticInfo instanceof OverlayWettingCoverageImageDiagnosticInfo)
      {
        OverlayWettingCoverageImageDiagnosticInfo overlayDiagInfo = (OverlayWettingCoverageImageDiagnosticInfo)diagnosticInfo;
        OverlayImageRenderer renderer = new OverlayImageRenderer(overlayDiagInfo.getOverlayImage(),
            overlayDiagInfo.getRegionOfInterestInInspectionImage(), LayerColorEnum.VOIDING_PIXELS.getColor());
        renderer.setStroke(_stroke);
        _graphicsEngine.addRenderer(_wettingCoverageGraphicsLayer, renderer);
    
        if(repaintRect == null)
          repaintRect = renderer.getBounds();
        else
          repaintRect.add(renderer.getBounds());
        haveModifiedGraphics = true;
      }
    }

    if (haveModifiedGraphics)
    {
      if(repaintAll)
        _graphicsEngine.repaint();
      else if(repaintRect != null)
        _graphicsEngine.repaint(repaintRect);
//      _graphicsEngine.center();
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void displayOriginalImage()
  {
    Assert.expect(_allSliceBufferedImage != null);
    if (_allSliceBufferedImage.get(_currentSliceNameEnum) != null)
    {
      BufferedImage bufferedImage = _allSliceBufferedImage.get(_currentSliceNameEnum);
      
      //Siew Yeng - XCR-2218 - Increase inspectable pad size
      MagnificationTypeEnum magnificationType = _currentReconstructionRegion.getComponent().getComponentType().getMagnificationType();
      // XCR-3050 Resize 2,3 and 4 is not expected result
      Subtype subtype = _currentReconstructionRegion.getSubtypes().iterator().next();
      resetMinimumZoomFactorIfNecessary(bufferedImage, magnificationType, subtype);
      
      displayImage(bufferedImage);
      _graphicsEngine.repaint();
      
      //Lim, Lay Ngor - XCR1652 - Image Post Processing - START
      //Update the current subtype and previous image renderer for post processing use.
      if (_previousInspectionImageRenderer != null)
      {
        if (_currentReconstructionRegion != null)
          _fineTuningImagePanel.updateCurrentSubtypeAndRenderer(_currentReconstructionRegion.getSubtypes().iterator().next(), _previousInspectionImageRenderer);
        else
          _fineTuningImagePanel.updateCurrentSubtypeAndRenderer(null, _previousInspectionImageRenderer);
      }
      //Lim, Lay Ngor - XCR1652 - Image Post Processing- END   
      
      _fineTuningImagePanel.setEnablePrevious2DImageButton(false);
      _fineTuningImagePanel.setEnableNext2DImageButton(false);
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void display2DImage()
  {
    Assert.expect(_allSliceBufferedImage != null);
    for (int cameraID = SliceNameEnum.CAMERA_0.getId(); cameraID < SliceNameEnum.CAMERA_13.getId(); ++cameraID)
    {
      //Only display the first available 2D image and return.
      if (_allSliceBufferedImage.get(SliceNameEnum.getEnumFromIndex(cameraID)) != null)
      {
        BufferedImage bufferedImage = _allSliceBufferedImage.get(SliceNameEnum.getEnumFromIndex(cameraID));
        
        //Siew Yeng - XCR-2218 - Increase inspectable pad size
        MagnificationTypeEnum magnificationType = _currentReconstructionRegion.getComponent().getComponentType().getMagnificationType();
        // XCR-3050 Resize 2,3 and 4 is not expected result
        Subtype subtype = _currentReconstructionRegion.getSubtypes().iterator().next();
        resetMinimumZoomFactorIfNecessary(bufferedImage, magnificationType, subtype);
        
        displayImage(bufferedImage);
        //Lim, Lay Ngor - XCR1652 - Image Post Processing - START
        //Update the current subtype and previous image renderer for post processing use.
        if (_previousInspectionImageRenderer != null)
        {
          if (_currentReconstructionRegion != null)
            _fineTuningImagePanel.updateCurrentSubtypeAndRenderer(_currentReconstructionRegion.getSubtypes().iterator().next(), _previousInspectionImageRenderer);
          else
            _fineTuningImagePanel.updateCurrentSubtypeAndRenderer(null, _previousInspectionImageRenderer);
        }
        //Lim, Lay Ngor - XCR1652 - Image Post Processing- END        
        _graphicsEngine.repaint();
        _fineTuningImagePanel.setEnablePrevious2DImageButton(false);
        _fineTuningImagePanel.setEnableNext2DImageButton(true);
        
        _currentCameraSliceNameEnumId = cameraID;
        return;
      }
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void displayNextOrPrevious2DImage(boolean isNext)
  {
    Assert.expect(_allSliceBufferedImage != null);
    if (isNext)
      _currentCameraSliceNameEnumId++;
    else
      _currentCameraSliceNameEnumId--;

    if (_currentCameraSliceNameEnumId <= SliceNameEnum.CAMERA_0.getId()) // first camera
    {
      _fineTuningImagePanel.setEnablePrevious2DImageButton(false);
      _fineTuningImagePanel.setEnableNext2DImageButton(true);
    }
    else if (_currentCameraSliceNameEnumId >= SliceNameEnum.CAMERA_13.getId()) // last camera
    {
      _fineTuningImagePanel.setEnablePrevious2DImageButton(true);
      _fineTuningImagePanel.setEnableNext2DImageButton(false);
    }
    else
    {
      _fineTuningImagePanel.setEnablePrevious2DImageButton(true);
      _fineTuningImagePanel.setEnableNext2DImageButton(true);
    }
    if (_allSliceBufferedImage.get(SliceNameEnum.getEnumFromIndex(_currentCameraSliceNameEnumId)) != null)
    {
      BufferedImage bufferedImage = _allSliceBufferedImage.get(SliceNameEnum.getEnumFromIndex(_currentCameraSliceNameEnumId));
      
      //Siew Yeng - XCR-2218 - Increase inspectable pad size
      MagnificationTypeEnum magnificationType = _currentReconstructionRegion.getComponent().getComponentType().getMagnificationType();
      // XCR-3050 Resize 2,3 and 4 is not expected result
      Subtype subtype = _currentReconstructionRegion.getSubtypes().iterator().next();
      resetMinimumZoomFactorIfNecessary(bufferedImage, magnificationType, subtype);
      
      displayImage(bufferedImage);
      //Lim, Lay Ngor - XCR1652 - Image Post Processing - START
      //Update the current subtype and previous image renderer for post processing use.
      if (_previousInspectionImageRenderer != null)
      {
        if (_currentReconstructionRegion != null)
          _fineTuningImagePanel.updateCurrentSubtypeAndRenderer(_currentReconstructionRegion.getSubtypes().iterator().next(), _previousInspectionImageRenderer);
        else
          _fineTuningImagePanel.updateCurrentSubtypeAndRenderer(null, _previousInspectionImageRenderer);
      }
      //Lim, Lay Ngor - XCR1652 - Image Post Processing- END      
      
      _graphicsEngine.repaint();
    }
    else // if the next / previous camera image is not available, jumps to the next / previous one.
    {
      if ((_currentCameraSliceNameEnumId > SliceNameEnum.CAMERA_0.getId()) &&
      (_currentCameraSliceNameEnumId < SliceNameEnum.CAMERA_13.getId())) 
      {
        if (isNext)
          displayNextOrPrevious2DImage(true);
        else
          displayNextOrPrevious2DImage(false);
      }
    }
  }
  
  /**
   * @author George A. David
   * @author Patrick Lacz
   */
  private void displayPassingFailingPadGraphics(MeasurementRegionDiagnosticInfo measurementDiagInfo)
  {
    Shape shapeToRender = measurementDiagInfo.getDiagnosticRegion();
    MeasurementRegionEnum measurementRegion = measurementDiagInfo.getMeasurementRegionEnum();
    RegionOfInterestRenderer renderer =  new RegionOfInterestRenderer(shapeToRender);
    renderer.setStroke(_stroke);

    Integer graphicsLayer = _measurementRegionEnumToGraphicsLayerNumberMap.get(measurementRegion);
    Assert.expect(graphicsLayer != null);
    _graphicsEngine.addRenderer(graphicsLayer.intValue(), renderer);

    _graphicsEngine.setAxisInterpretation(true, true);
    _graphicsEngine.center();
    _graphicsEngine.repaint();
  }

  /**
   * @author George Booth
   */
  private void displayPassingFailingComponentGraphics(MeasurementRegionDiagnosticInfo measurementDiagInfo)
  {
    // display text
    boolean passingTest = true;
    String messageText = null;
    MeasurementRegionEnum measurementRegion = measurementDiagInfo.getMeasurementRegionEnum();
    if (measurementRegion == MeasurementRegionEnum.PASSING_JOINT_REGION)
    {
      messageText = StringLocalizer.keyToString("RDGUI_PASSING_COMPONENT_TEST_KEY");
    }
    else if (measurementRegion == MeasurementRegionEnum.FAILING_JOINT_REGION)
    {
      messageText = StringLocalizer.keyToString("RDGUI_FAILING_COMPONENT_TEST_KEY");
      passingTest = false;
    }
    else
    {
      return;
    }
    double borderLocationX = 0.0;  // aligned with left edge of x-ray image
    double textLocationY = -40.0;  // 40 pixels above top of x-ray image
    double textMarginX = 10;       // 10 pixels left and right of text
    double textMarginY = 5;        //  5 pixels above and below text
    ProfileTextRenderer textRenderer = new ProfileTextRenderer(messageText,
        borderLocationX + _outlineWidth + textMarginX, textLocationY);
    _graphicsEngine.addRenderer(_textLayer, textRenderer);

    // get size of text
    Rectangle2D textRect = textRenderer.getTextBounds();

    // draw background box
    ProfileGraphicRenderer textBoxRenderer = new ProfileGraphicRenderer(new Rectangle2D.Double(
        borderLocationX + _outlineWidth, textLocationY - textMarginY,
        textRect.getWidth() + (textMarginX * 2.0),
        textRect.getHeight() + (textMarginY * 2.0)));
    textBoxRenderer.setFillShape(true);
    _graphicsEngine.addRenderer(_textBackgroundLayer, textBoxRenderer);

    // draw passing/failing border around box (has a cool 1 pixel gap between outline and box)
    ProfileGraphicRenderer outlineRenderer = new ProfileGraphicRenderer(new Rectangle2D.Double(
        borderLocationX, textLocationY - textMarginY - _outlineWidth,
        textRect.getWidth() + (textMarginX * 2.0) + (_outlineWidth * 2.0) - 1.0,
        textRect.getHeight() + (textMarginY * 2.0) + (_outlineWidth * 2.0) - 1.0));
    outlineRenderer.setStroke(new BasicStroke(_outlineWidth));
    if (passingTest)
    {
      _graphicsEngine.addRenderer(_passingOutlineLayer, outlineRenderer);
    }
    else
    {
      _graphicsEngine.addRenderer(_failingOutlineLayer, outlineRenderer);
    }
    
    // display failing component level region
    Shape shapeToRender = measurementDiagInfo.getDiagnosticRegion();
    RegionOfInterestRenderer renderer =  new RegionOfInterestRenderer(shapeToRender);
    renderer.setStroke(_stroke);

    Integer graphicsLayer = _measurementRegionEnumToGraphicsLayerNumberMap.get(measurementRegion);
    Assert.expect(graphicsLayer != null);
    _graphicsEngine.addRenderer(graphicsLayer.intValue(), renderer);

    _graphicsEngine.setAxisInterpretation(true, true);
    _graphicsEngine.center();
    _graphicsEngine.repaint();
  }

  /**
   * @author Patrick Lacz
    _toolBar.setMeasureToggleButtonEnabled(true);
   */
  private void createLayers()
  {
    int layerNumber = JLayeredPane.DEFAULT_LAYER.intValue();

    _diagnosticLayers.clear();

    // the image itself is not a MeasurementRegion, so must be handled separately
    _imageLayer = layerNumber++;

    // messages for passing and failing components
    _textBackgroundLayer = layerNumber++;
    _passingOutlineLayer = layerNumber++;
    _failingOutlineLayer = layerNumber++;
    _textLayer = layerNumber++;
    _diagnosticMeasurementLayer = layerNumber++;

    for (MeasurementRegionEnum measurementRegionEnum : MeasurementRegionEnum.getSetOfAllEnums())
    {
      int thisLayerNumber = layerNumber++;
      _measurementRegionEnumToGraphicsLayerNumberMap.put(measurementRegionEnum, thisLayerNumber);
      _diagnosticLayers.add(thisLayerNumber);
    }

    // voiding is not a MeasurementRegion, so must be handled separately
    _solderAreaGraphicsLayer = layerNumber++;
    _expectedImageGraphicsLayer = layerNumber++;
    _voidingGraphicsLayer = layerNumber++;
    _largestVoidingGraphicsLayer = layerNumber++;
    _wettingCoverageGraphicsLayer = layerNumber++;
    _backgroundPixelsGraphicsLayer = layerNumber++;
    _solderAreaGraphicsLayer = layerNumber++;
    _expectedImageGraphicsLayer = layerNumber++;

    _diagnosticLayers.add(_solderAreaGraphicsLayer);
    _diagnosticLayers.add(_expectedImageGraphicsLayer);
    _diagnosticLayers.add(_voidingGraphicsLayer);
    _diagnosticLayers.add(_largestVoidingGraphicsLayer);
    _diagnosticLayers.add(_wettingCoverageGraphicsLayer);
    _diagnosticLayers.add(_backgroundPixelsGraphicsLayer);
    _diagnosticLayers.add(_solderAreaGraphicsLayer);
    _diagnosticLayers.add(_expectedImageGraphicsLayer);
    _diagnosticLayers.add(_diagnosticMeasurementLayer); //Siew Yeng - XCR-2566

    Collection<Integer> layersToUseWhenFittingToScreen = new ArrayList<Integer>(1);
    layersToUseWhenFittingToScreen.add(_imageLayer);
    _graphicsEngine.setLayersToUseWhenFittingToScreen(layersToUseWhenFittingToScreen);

    _allLayers.add(_imageLayer);
    _allLayers.add(_textBackgroundLayer);
    _allLayers.add(_passingOutlineLayer);
    _allLayers.add(_failingOutlineLayer);
    _allLayers.add(_textLayer);
    _allLayers.add(_solderAreaGraphicsLayer);
    _allLayers.add(_expectedImageGraphicsLayer);
    _allLayers.add(_voidingGraphicsLayer);
    _allLayers.add(_largestVoidingGraphicsLayer);
    _allLayers.add(_wettingCoverageGraphicsLayer);
    _allLayers.add(_backgroundPixelsGraphicsLayer);
    _allLayers.add(_solderAreaGraphicsLayer);
    _allLayers.add(_expectedImageGraphicsLayer);
    _allLayers.addAll(_diagnosticLayers);
  }

  /**
   * @author George A. David
   */
  private void setColors()
  {
    _graphicsEngine.setForeground(_imageLayer, Color.MAGENTA);
    _graphicsEngine.setForeground(_textBackgroundLayer, Color.lightGray);
    _graphicsEngine.setForeground(_passingOutlineLayer, Color.GREEN);
    _graphicsEngine.setForeground(_failingOutlineLayer, Color.RED);
    _graphicsEngine.setForeground(_textLayer, Color.BLACK); //.PROFILE_TEXT.getColor());
    _graphicsEngine.setForeground(_diagnosticMeasurementLayer, Color.BLUE); //Siew Yeng - XCR-2566 - display measurement value on diagnostic image
    for (MeasurementRegionEnum measurementRegionEnum : MeasurementRegionEnum.getSetOfAllEnums())
    {
      Integer graphicsLayerForMeasurementRegion = _measurementRegionEnumToGraphicsLayerNumberMap.get(measurementRegionEnum);
      if (graphicsLayerForMeasurementRegion != null)
      {
        Color measurementRegionColor = XRayLayerColorEnum.getColorForMeasurementRegionEnum(measurementRegionEnum);
        _graphicsEngine.setForeground(graphicsLayerForMeasurementRegion.intValue(), measurementRegionColor);
      }
    }
    _graphicsEngine.setForeground(_solderAreaGraphicsLayer, LayerColorEnum.SOLDER_AREA_PIXELS.getColor());
    _graphicsEngine.setForeground(_expectedImageGraphicsLayer, LayerColorEnum.EXPECTED_IMAGE_PIXELS.getColor());
    _graphicsEngine.setForeground(_voidingGraphicsLayer, LayerColorEnum.VOIDING_PIXELS.getColor());
    _graphicsEngine.setForeground(_largestVoidingGraphicsLayer, LayerColorEnum.LARGEST_VOIDING_PIXELS.getColor());
    _graphicsEngine.setForeground(_wettingCoverageGraphicsLayer, LayerColorEnum.VOIDING_PIXELS.getColor());
    _graphicsEngine.setForeground(_backgroundPixelsGraphicsLayer, LayerColorEnum.BACKGROUND_PIXELS.getColor());
    _graphicsEngine.setForeground(_expectedImageGraphicsLayer, LayerColorEnum.EXPECTED_IMAGE_PIXELS.getColor());
  }

  /**
   * @author George A. David
   */
  public void zoomIn()
  {
    _graphicsEngine.zoom(_ZOOM_FACTOR);
  }

  /**
   * @author George A. David
   */
  public void zoomOut()
  {
    _graphicsEngine.zoom(1.0 / _ZOOM_FACTOR);
  }

  /**
   * @author Anthony Fong //Anthony01005
   */
  public void setZoomFactor(double zoomFactor)
  {
      
    _graphicsEngine.resetZoom();
    _graphicsEngine.setZoom(zoomFactor);
    _graphicsEngine.center();
  }

  /**
   * @author George A. David
   */
  public void setZoomRectangleMode(boolean zoomRectangleMode)
  {
    if(zoomRectangleMode)
    {
      _graphicsEngine.setMeasurementMode(false);
      _graphicsEngine.setDragGraphicsMode(false);
      _graphicsEngine.setZoomRectangleMode(true);
    }
    else
    {
      _graphicsEngine.setZoomRectangleMode(false);
      _graphicsEngine.setMouseEventsEnabled(true);
    }
  }

  /**
   * @author George A. David
   */
  public void setDragGraphicsMode(boolean dragGraphicsMode)
  {
    if(dragGraphicsMode)
    {
      _graphicsEngine.setMeasurementMode(false);
      _graphicsEngine.setZoomRectangleMode(false);
      _graphicsEngine.setDragGraphicsMode(true);
    }
    else
    {
      _graphicsEngine.setDragGraphicsMode(false);
      _graphicsEngine.setMouseEventsEnabled(true);
    }
  }

  /**
   * @author George A. David
   */
  public void setMeasurmentMode(boolean measurementMode)
  {
    if(measurementMode)
    {
      _graphicsEngine.setZoomRectangleMode(false);
      _graphicsEngine.setDragGraphicsMode(false);
      _graphicsEngine.setMeasurementMode(true);
    }
    else
    {
      _graphicsEngine.setMeasurementMode(false);
      _graphicsEngine.setMouseEventsEnabled(true);
    }
  }

  /**
   * @author Seng-Yew Lim
   */
  public void setImageStayPersistent(boolean isImageStayPersistent)
  {
    _isImageStayPersistent = isImageStayPersistent;
  }

  /**
   * @author Anthony Fong //Anthony01005
   */
  public void setImageFitToScreen(boolean isImageFitToScreen)
  {
    _isGraphicsShouldBeFitToScreen = isImageFitToScreen;
    if(_inspectionImageRenderer != null)
        _inspectionImageRenderer.setBicubicInterpolation(_isGraphicsShouldBeFitToScreen);
    if(_fineTuningImagePanel != null)
        _fineTuningImagePanel.getFineTuningPanel().getFineTuningProfileGraphicPanel().getFineTuningProfileGraphicsEngineSetup().setEnableProfileZoom(isImageFitToScreen);
  }
  public boolean getImageFitToScreen()
  {
    return _isGraphicsShouldBeFitToScreen;
  }
  /**
   * @author George A. David
   */
  public void showTopSide()
  {
    Assert.expect(false);
  }

  /**
   * @author George A. David
   */
  public void showBottomSide()
  {
    Assert.expect(false);
  }

  /**
   * @author George A. David
   */
  public void resetGraphics()
  {
    //Siew Yeng
    if(_graphicsEngine.getMinimumZoomFactor() < 1.0 && _lastBufferedImage != null)
    {
      double widthZoomFactor = (double)_graphicsEngine.getWidth()/(double)_lastBufferedImage.getWidth();
      double heightZoomFactor = (double)_graphicsEngine.getHeight()/(double)_lastBufferedImage.getHeight();
      if(widthZoomFactor > heightZoomFactor)
      {
        setZoomFactor(heightZoomFactor);
      }
      else
      {
        setZoomFactor(widthZoomFactor);
      }
    }
    else
      _graphicsEngine.resetZoom();
    
    _graphicsEngine.center();
  }

  /**
   * @author George A. David
   */
  public void clearGraphics()
  {
    _graphicsEngine.setVisible(_diagnosticLayers, false);
  }

  /**
   * @author George A. David
   */
  public void showGraphics()
  {
    _graphicsEngine.setVisible(_diagnosticLayers, true);
  }

  /**
   * @author Scott Richardson
   */
  public void translateUpInZAxis()
  {
    Assert.expect(false, "translateUpInZAxis is not implemented here");
  }

  /**
   * @author Scott Richardson
   */
  public void translateDownInZAxis()
  {
    Assert.expect(false, "translateUpInZAxis is not implemented here");
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setGroupSelectMode(boolean isGroupSelectMode)
  {
    Assert.expect(false, "translateUpInZAxis is not implemented here");
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setSelectRegionMode(boolean isSelectRegionMode)
  {
    Assert.expect(false, "translateUpInZAxis is not implemented here");
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setSelectRegionMode(boolean isSelectRegionMode, double maxImageRegionWidth, double maxImageRegionHeight)
  {
    Assert.expect(false, "translateUpInZAxis is not implemented here");
  }

  /**
   * @author Chong, Wei Chin
   */
  public void increaseBrightnessForImagePanel()
  {
    _graphicsEngine.increaseBrightnessForRendererPanel(_imageLayer);
  }

  /**
   * @author Chong, Wei Chin
   */
  public void decreaseBrightnessForImagePanel()
  {
    _graphicsEngine.decreaseBrightnessForRendererPanel(_imageLayer);
  }

  /**
   * @author Chong, Wei Chin
   */
  public void increaseContrastForImagePanel()
  {
    _graphicsEngine.increaseContrastForRendererPanel(_imageLayer);
  }

  /**
   * @author Chong, Wei Chin
   */
  public void decreaseContrastForImagePanel()
  {
    _graphicsEngine.decreaseContrastForRendererPanel(_imageLayer);
  }

  /**
   * @author Chong, Wei Chin
   */
  public void increaseNormalizeForImagePanel()
  {
    _graphicsEngine.increaseNormalizeForRendererPanel(_imageLayer);
  }

  /**
   * @author Chong, Wei Chin
   */
  public void decreaseNormalizeForImagePanel()
  {
    _graphicsEngine.decreaseNormalizeForRendererPanel(_imageLayer);
  }


  /**
   * @author Chong, Wei Chin
   */
  public void equalizeGrayScaleForImagePanel()
  {
    _graphicsEngine.equalizeGrayScaleForRendererPanel(_imageLayer);
  }

  /**
   * @author Chong, Wei Chin
   */
  public void revertToOriginalBufferedImageForImagePanel()
  {
    _graphicsEngine.revertToOriginalBufferedImageForRendererPanel(_imageLayer);
  }

  /**
   * @author Chong, Wei Chin
   */
  public void saveBufferedImageForImagePanel(String filename)
  {
    Assert.expect(filename != null);
    
    _graphicsEngine.saveBufferedImageForRendererPanel(_imageLayer, filename);
  }
  
  /**
   * @author Jack Hwee
   */
  public void saveDiagnosticBufferedImageForImagePanel(String filename)
  {
    Assert.expect(filename != null);
 
    try 
    {       
       Rectangle rect = new Rectangle((int)_inspectionImageRenderer.getLocationOnScreen().getX(),(int)_inspectionImageRenderer.getLocationOnScreen().getY(),(int)_inspectionImageRenderer.getWidth(),(int)_inspectionImageRenderer.getHeight());
       Robot robot = new Robot();
       Thread.sleep(1000);
       java.io.File f = new java.io.File(filename);
       BufferedImage img = robot.createScreenCapture(rect);
       //Lim, Lay Ngor: Don't need to apply post processing here as the image 
       //is a print screen colour image.
       //Besides, post processing did not support colour image.
       javax.imageio.ImageIO.write(img,"jpeg",f);
    } 
    catch(Exception e)
    {
       e.printStackTrace();
    }   
  }
  
  public void saveAllSliceBufferedImageForCurrentSelectedPad(String filename)
  {
    Assert.expect(filename != null);
    
    for (Slice slice : _currentReconstructionRegion.getSlices())
    {
      if (_allSliceBufferedImage.get(slice.getSliceName()) != null)
      {
        String fileNameFullPath = filename + slice.getSliceName() + ".jpg";
        //Lim, Lay Ngor - XCR2126 - Save with Post Processing
        // Apply post processing setting to buffered image from _allSliceBufferedImage then save it to file.
        saveImageWithPostProcessing(_allSliceBufferedImage.get(slice.getSliceName()), fileNameFullPath);
      }
    }
  }
  
  /**
   * Display sharpness profile
   * @author Ying-Huan.Chu
   */
  public void displaySharpnessProfile()
  {
    if (_zHeightInNanometerList == null || _sharpnessList == null)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(), 
                                          StringLocalizer.keyToString("XYPLOTDIALOG_NO_SHARPNESS_AND_ZHEIGHT_ARRAY_ERROR_MESSAGE_KEY"), 
                                          StringLocalizer.keyToString("SHARPNESS_PROFILE_ERROR_TITLE_KEY"));
    }
    else
    {
      XYPlotDialog sharpnessProfileXYPlotDialog;
      _xLabel = StringLocalizer.keyToString("SHARPNESS_PROFILE_CHART_X_LABEL_KEY") + " ("+ 
                Project.getCurrentlyLoadedProject().getDisplayUnits().toString() + ")";
      try
      {
        if (Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric())
        {
          sharpnessProfileXYPlotDialog = new XYPlotDialog(_sharpnessProfileChartTitle, _xLabel, _yLabel, _zHeightInNanometerList, _sharpnessList);
          sharpnessProfileXYPlotDialog.plotData();
          sharpnessProfileXYPlotDialog.markSpecificPoint(_zHeightInNanos);
        }
        else // use mils instead of nanometer for z-height
        {
          java.util.List<Integer> zHeightList = getZHeightInMilsList();
          sharpnessProfileXYPlotDialog = new XYPlotDialog(_sharpnessProfileChartTitle, _xLabel, _yLabel, zHeightList, _sharpnessList);
          sharpnessProfileXYPlotDialog.plotData();
          sharpnessProfileXYPlotDialog.markSpecificPoint(MathUtil.convertNanoMetersToMils(_zHeightInNanos));
        }
        SwingUtils.centerOnComponent(sharpnessProfileXYPlotDialog, MainMenuGui.getInstance());
        sharpnessProfileXYPlotDialog.setVisible(true);
      }
      catch (final BadFormatException | EmptyListException | XYListSizeDifferentException ex)
      {
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(), 
                                          StringLocalizer.keyToString(new LocalizedString("SHARPNESS_PROFILE_IMAGE_CORRUPTED_ERROR_KEY",
                                                                      new Object[]{StringLocalizer.keyToString(ex.getLocalizedString().getMessageKey())})), 
                                          StringLocalizer.keyToString("SHARPNESS_PROFILE_ERROR_TITLE_KEY"));
          }
        });
      }
    }
  }
  
  /**
   * Get the Z-height list in mils instead of nanometer
   * @author Ying-Huan.Chu
   */
  public java.util.List<Integer> getZHeightInMilsList()
  {
    java.util.List<Integer> zHeightList = new ArrayList<>();
    if (_zHeightInNanometerList != null)
    {
      for (int i = 0; i < _zHeightInNanometerList.size(); ++i)
      {
        zHeightList.add((int) com.axi.util.MathUtil.convertNanoMetersToMils(_zHeightInNanometerList.get(i)));
      }
    }
    return zHeightList;
  }
  
  /**
   * Clear Sharpness Profile related data
   * @author Ying-Huan.Chu
   */
  public void clearSharpnessProfile()
  {
    if (_zHeightInNanometerList != null)
    {
      _zHeightInNanometerList.clear();
    }
    if (_sharpnessList != null)
    {
      _sharpnessList.clear();
    }
  }
  
  /**
   * editted by sheng chuan
   */
  public void clearComponentImageState()
  {
    // XCR-3050 Resize 2,3 and 4 is not expected result.
    _currentReconstructionRegion = null;
    _component = null;
    _sliceNameEnum = null;
    _fineTuningImagePanel.setEnableToggleViewComponentImageButton(false);
  }
  
  /**
   * Display enhanced component image
   * @author Siew Yeng
   */
  public void displayComponentImage(boolean enhanceImage)
  {
    java.util.List<ReconstructionRegion> reconstructionRegionsList = new ArrayList();
    TestProgram testProgram = Project.getCurrentlyLoadedProject().getTestProgram();
    reconstructionRegionsList =  _component.getReconstructionRegionList(testProgram);
    
    //XCR-3787, Original and whole component image is different
    //remove it and re-add it to make it the last region to draw so that will not overlapped by other region image
    if (reconstructionRegionsList.contains(_currentReconstructionRegion))
    {
      reconstructionRegionsList.remove(_currentReconstructionRegion);
      reconstructionRegionsList.add(_currentReconstructionRegion);
    }
    
    BufferedImage componentBufferedImage = null;
    
    if (reconstructionRegionsList.size() > 1)
    {
      componentBufferedImage = _imageManager.stitchOfflineComponentImage(_component, _sliceNameEnum, reconstructionRegionsList, _imageSetData, enhanceImage);
      _graphicsEngine.setMinimumZoomFactorForComponentImageView();
      setImageFitToScreen(true);
      displayImage(componentBufferedImage);
      displayPassingFailingPadGraphics(_componentMeasurementDiagInfo);
	  
      //Lim, Lay Ngor - XCR2124 - Component image did not apply post processing after return from original state - START
      //Update the current subtype and previous image renderer for post processing use.
      if (_previousInspectionImageRenderer != null)
      {
        if (_currentReconstructionRegion != null)
          _fineTuningImagePanel.updateCurrentSubtypeAndRenderer(_currentReconstructionRegion.getSubtypes().iterator().next(), _previousInspectionImageRenderer);
        else
          _fineTuningImagePanel.updateCurrentSubtypeAndRenderer(null, _previousInspectionImageRenderer);
      }
      //Lim, Lay Ngor - XCR2124 - Component image did not apply post processing after return from original state - END 
    }
  }
  
  /**
   * Display original inspection image
   * @author bee-hoon.goh
   */
  public void displayOriginalImagePadSelectionImage()
  {
    if(_toolBar.isToggleImageFitToScreenButtonSelected() == false)
      setImageFitToScreen(false);
    
    //Siew Yeng - XCR-2218 - Increase inspectable pad size
    MagnificationTypeEnum magnificationType = _component.getComponentType().getMagnificationType();
    // XCR-3050 Resize 2,3 and 4 is not expected result
    Subtype subtype = _currentReconstructionRegion.getSubtypes().iterator().next();
    resetMinimumZoomFactorIfNecessary(_lastBufferedImage, magnificationType, subtype);
    
    displayImage(_lastBufferedImage);
    displayPassingFailingPadGraphics(_lastMeasurementDiagInfo);
    
    //Lim, Lay Ngor - XCR2124 - Component image did not apply post processing after return from original state - START
    //Update the current subtype and previous image renderer for post processing use.
    if (_previousInspectionImageRenderer != null)
    {
      if (_currentReconstructionRegion != null)
        _fineTuningImagePanel.updateCurrentSubtypeAndRenderer(_currentReconstructionRegion.getSubtypes().iterator().next(), _previousInspectionImageRenderer);
      else
        _fineTuningImagePanel.updateCurrentSubtypeAndRenderer(null, _previousInspectionImageRenderer);
    }
    //Lim, Lay Ngor - XCR2124 - Component image did not apply post processing after return from original state - END        
  }
  
  /**
   * @author bee-hoon.goh
   */
  public void setHas2DImages(boolean has2DImages)
  {
    _has2DImages = has2DImages;
  }
  
  /**
   * @author bee-hoon.goh
   */
  public boolean has2DImages()
  {
    return _has2DImages;
  }
  
  /*
   * @author Ying-Huan.Chu
   * @edited by bee-hoon.goh
   * check if the _allSliceBufferedImage contains 2.5D image(s).
   */
  private void has2DImages(Map<SliceNameEnum, BufferedImage> allSliceBufferedImage)
  {
    if (allSliceBufferedImage != null)
    {
      for(int cameraID = SliceNameEnum.CAMERA_0.getId(); cameraID < SliceNameEnum.CAMERA_13.getId(); ++cameraID)
      {
        if (allSliceBufferedImage.containsKey(SliceNameEnum.getEnumFromIndex(cameraID)))
        {
          setHas2DImages(true);
          _fineTuningImagePanel.setAllowToggle2DImageButton(has2DImages());
          break;
        }
        else
        {
          setHas2DImages(false);
          _fineTuningImagePanel.setAllowToggle2DImageButton(has2DImages());
          _fineTuningImagePanel.setEnablePrevious2DImageButton(has2DImages());
          _fineTuningImagePanel.setEnableNext2DImageButton(has2DImages());
        }
      }
    }
    else
    {
      setHas2DImages(false);
      _fineTuningImagePanel.setAllowToggle2DImageButton(has2DImages());
      _fineTuningImagePanel.setEnablePrevious2DImageButton(has2DImages());
      _fineTuningImagePanel.setEnableNext2DImageButton(has2DImages());
    }
  }

  
  /*
   * XCR2126: Save the input buffered image by applying the post processing base on user selected setting.
   * @author Lim, Lay Ngor
   */  
  private void saveImageWithPostProcessing(BufferedImage bufferedImage, String fileNameFullPath)
  {
    Assert.expect(fileNameFullPath != null);
    Assert.expect(bufferedImage != null);

    java.io.File bufferedImageWriter = new java.io.File(fileNameFullPath);      
    try 
    {
      //Create a temporary renderer which will link to the post processing implementation.
      ImageProcessingRenderer tempRenderer = new ImageProcessingRenderer(bufferedImage);
      
      //Set the current user selected BrightnessAndContrastSetting to the renderer.
      if (_inspectionImageRenderer != null)
        tempRenderer.setBrightnessAndContrastSetting(_inspectionImageRenderer._brightnessAndContrastSetting);
      else//set to default value if _inspectionImageRenderer is null
        tempRenderer.setBrightnessAndContrastSetting(new BrightnessAndContrastSetting());
      
      //Set the current user selected Enhancer Objects to the renderer.
      if(_fineTuningImagePanel!= null)
        tempRenderer.imagePostProcessing(_fineTuningImagePanel.getCurrentGuiImageEnhancerList());
      else//skip Enhancer process if _fineTuningImagePanel is null
        tempRenderer.imagePostProcessing(null);
      
      //Save the processed image to file.
      javax.imageio.ImageIO.write(tempRenderer.getBufferedImageFromRenderer(), "jpeg", bufferedImageWriter);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    finally
    {      
      bufferedImageWriter = null;
    }
  }
  
  /**
   * Set Graphic Engine to minimum zoom factor of Component Image View if the image is enlarged.
   * @param image
   * @param magnificationType 
   * @author Siew Yeng
   */
  private void resetMinimumZoomFactorIfNecessary(BufferedImage image, MagnificationTypeEnum magnificationType, Subtype subtype)
  {
    int defaultMaxInspectionRegionWidth = ReconstructionRegion.getDefaultMaxInspectionRegionWidthInNanoMeters();
    int defaultMaxInspectionRegionLength = ReconstructionRegion.getDefaultMaxInspectionRegionLengthInNanoMeters();
    int nanometersPerPixel = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
    if(magnificationType.equals(MagnificationTypeEnum.HIGH))
    {
      defaultMaxInspectionRegionWidth = ReconstructionRegion.getDefaultMaxHighMagInspectionRegionWidthInNanoMeters();
      defaultMaxInspectionRegionLength = ReconstructionRegion.getDefaultMaxHighMagInspectionRegionLengthInNanoMeters();
      nanometersPerPixel = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
    }
    // XCR-3050 Resize 2,3 and 4 is not expected result
    if((image.getWidth() > MathUtil.convertNanoMetersToPixelsUsingRound(defaultMaxInspectionRegionWidth, nanometersPerPixel) ||
      image.getHeight()> MathUtil.convertNanoMetersToPixelsUsingRound(defaultMaxInspectionRegionLength, nanometersPerPixel)) &&
      ImageProcessingAlgorithm.useResize(subtype) == false)
    {
      _graphicsEngine.setMinimumZoomFactorForComponentImageView();
      setImageFitToScreen(true);
    }
    else
    {
      _graphicsEngine.resetMinimumZoomFactorForNormalImageView();
    }
  }    
}
