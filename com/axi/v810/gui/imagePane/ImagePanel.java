package com.axi.v810.gui.imagePane;

import java.awt.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.gui.algoTuner.AlgoTunerGui;
import com.axi.v810.hardware.MagnificationEnum;

/**
 * This class manages the image graphics displays in a card layout.
 *
 * @author Kay Lannen
 */
public class ImagePanel extends JPanel
{
  private AlignmentImagePanel _alignmentImagePanel;
  private FineTuningPanel _fineTuningPanel;
  private VerificationImagePanel _verificationImagePanel;
  private JPanel _blankPanel;
  private CardLayout _layout;
  private final static String _ALIGNMENT = "Alignment";
  private final static String _FINE_TUNING = "Fine Tuning";
  private final static String _VERIFICATION = "Verification";
  private final static String _VIRTUAL_LIVE = "VirtualLive";  
  private final static String _BLANK = "Blank";
  // high mag
  private final static String _ALIGNMENT_FOR_HIGH_MAG = "Alignment For High Mag";
  private final static String _VERIFICATION_FOR_HIGH_MAG = "Verification For High Mag";
  private AlignmentImagePanel _alignmentImagePanelForHighMag;
  private VerificationImagePanel _verificationImagePanelForHighMag;

  /**
   * @author Scott Richardson
   */
  public ImagePanel()
  {
    // Do nothing
  }

  public void saveAlgoTunerInstance(AlgoTunerGui algoTunerGui)
  {
      _fineTuningPanel.saveAlgoTunerInstance(algoTunerGui);
  }
  /**
   * @author George A. David
   */
  ImagePanel(ImageManagerToolBar toolBar)
  {
    _alignmentImagePanel = new AlignmentImagePanel(toolBar);
    _fineTuningPanel = new FineTuningPanel(toolBar);
    _verificationImagePanel = new VerificationImagePanel(toolBar);
    _alignmentImagePanelForHighMag = new AlignmentImagePanel(toolBar,MagnificationEnum.H_NOMINAL);
    _verificationImagePanelForHighMag = new VerificationImagePanel(toolBar,MagnificationEnum.H_NOMINAL);
    _blankPanel = new JPanel();
    _blankPanel.setBackground(Color.black);

    _layout = new CardLayout();
    setLayout(_layout);
    add(_alignmentImagePanel, _ALIGNMENT);    
    add(_fineTuningPanel, _FINE_TUNING);
    add(_verificationImagePanel, _VERIFICATION);
    add(_alignmentImagePanelForHighMag, _ALIGNMENT_FOR_HIGH_MAG);
    add(_verificationImagePanelForHighMag, _VERIFICATION_FOR_HIGH_MAG);
    add(_blankPanel, _BLANK);
  }

  /**
   * @author Andy Mechtenberg
   */
  void unpopulate()
  {
    _fineTuningPanel.unpopulate();
    _verificationImagePanel.unpopulate();
    _verificationImagePanelForHighMag.unpopulate();
  }

  /**
   * @author George A. David
   */
  void displayAlignmentImagePanel()
  {
    _layout.show(this, _ALIGNMENT);
    _alignmentImagePanel.display();
  }

  /**
   * @author George A. David
   */
  void displayFineTuningImagePanel()
  {
    _layout.show(this, _FINE_TUNING);
    _fineTuningPanel.display();
  }

  /**
   * @author George A. David
   */
  void finishFineTuningImagePanel()
  {
    _layout.show(this, _BLANK);
    _fineTuningPanel.finish();
  }

  /**
   * @author George A. David
   */
  void displayVerificationImagePanel(boolean checkLatestestProgram)
  {
    _layout.show(this, _VERIFICATION);
    _verificationImagePanel.display(checkLatestestProgram);
  }

  /**
   * @author George A. David
   */
  void finishVerificationImagePanel()
  {
    _layout.show(this, _BLANK);
    _verificationImagePanel.finish();
  }

  /**
   * XCR-3589 Combo STD and XXL software GUI
   *
   * @author weng-jian.
   *
   */
  void finishAlignmentImagePanel()
  {
    _layout.show(this, _BLANK);
    _alignmentImagePanel.finish();
  }

  /**
   * @author George A. David
   */
  FineTuningImagePanel getFineTuningImagePanel()
  {
    Assert.expect(_fineTuningPanel != null);

    return _fineTuningPanel.getFineTuningImagePanel();
  }

  /**
   * @author George A. David
   */
  AlignmentImagePanel getAlignmentImagePanel()
  {
    Assert.expect(_alignmentImagePanel != null);

    return _alignmentImagePanel;
  }

  /**
   * @author George A. David
   */
  VerificationImagePanel getVerificationImagePanel()
  {
    Assert.expect(_verificationImagePanel != null);

    return _verificationImagePanel;
  }
  
  /**
   * @author Chong Wei Chin
   */
  void displayAlignmentImagePanelForHighMag()
  {
    _layout.show(this, _ALIGNMENT_FOR_HIGH_MAG);
    _alignmentImagePanelForHighMag.display();
  }

  /**
   * @author Wei Chin
   */
//  void displayFineTuningImagePanel()
//  {
//    _layout.show(this, _FINE_TUNING);
//    _fineTuningPanel.display();
//  }

  /**
   * @author Wei Chin
   */
//  void finishFineTuningImagePanel()
//  {
//    _layout.show(this, _BLANK);
//    _fineTuningPanel.finish();
//  }

  /**
   * @author Chong, Wei Chin
   */
  void displayVerificationImagePanelForHighMag(boolean checkLatestestProgram)
  {
    _layout.show(this, _VERIFICATION_FOR_HIGH_MAG);
    _verificationImagePanelForHighMag.display(checkLatestestProgram);
  }

  /**
   * @author Chong, Wei Chin
   */
  void finishVerificationImagePanelForHighMag()
  {
    _layout.show(this, _BLANK);
    _verificationImagePanelForHighMag.finish();
  }

  /**
   * @author Scott Richardson
   */
//  void displayVirtualLiveImagePanel()
//  {
//    _layout.show(this, _VIRTUAL_LIVE);
//    _virtualLiveImagePanel.display();
//  }

  /**
   * @author George A. David
   */
//  FineTuningImagePanel getFineTuningImagePanel()
//  {
//    Assert.expect(_fineTuningPanel != null);
//
//    return _fineTuningPanel.getFineTuningImagePanel();
//  }

  /**
   * @author Chong, Wei Chin
   */
  AlignmentImagePanel getAlignmentImagePanelForHighMag()
  {
    Assert.expect(_alignmentImagePanelForHighMag != null);

    return _alignmentImagePanelForHighMag;
  }

  /**
   * @author Chong, Wei Chin
   */
  VerificationImagePanel getVerificationImagePanelForHighMag()
  {
    Assert.expect(_verificationImagePanelForHighMag != null);

    return _verificationImagePanelForHighMag;
  }

  /**
   * @author Scott Richardson
   */
//  VirtualLiveImagePanel getVirtualLiveImagePanel()
//  {
//    Assert.expect(_virtualLiveImagePanel != null);
//
//    return _virtualLiveImagePanel;
//  }

  /**
   * @author Scott Richardson
   */
//  void finishVirtualLiveImagePanel()
//  {
//    _layout.show(this, _BLANK);
//    _virtualLiveImagePanel.finish();
//  }
}
