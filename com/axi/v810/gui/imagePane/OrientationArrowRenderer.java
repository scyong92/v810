package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.hardware.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.MagnificationTypeEnum;


/**
 * This class is responsible for drawing the refernece designator
 *
 * @author George A. David
 */
class OrientationArrowRenderer extends ShapeRenderer
{
  private Pad _pad;
  private Shape _arrowShape;
  //Kee Chin Seong - Always using nominal magnification to create affineTrasnform for the Arrow
  private AffineTransform _nanoMetersToPixelTransform;// = AffineTransform.getScaleInstance(1.0 / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel(),
                                                       //                                  1.0 / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel());

  /**
   * @author George A. David
   */
  OrientationArrowRenderer(Pad pad)
  {
    super(false);
    Assert.expect(pad != null);
    _pad = pad;
    setFillShape(true);

    refreshData();
  }

  /**
   * @author George A. David
   */
  protected void refreshData()
  {
    _arrowShape = createOrientationArrow();
    initializeShape(_arrowShape);
  }

  /**
   * @author George A. David
   */
  private java.awt.Shape createOrientationArrow()
  {
    Assert.expect(_pad != null);

    LandPatternPad _landPatternPad = _pad.getLandPatternPad();
    java.awt.geom.Rectangle2D shapeRect = _landPatternPad.getShapeInNanoMeters().getBounds2D();
    double centerX = shapeRect.getCenterX();
    double centerY = shapeRect.getCenterY();

    double arrowHeadSize = Math.min(_landPatternPad.getWidthInNanoMeters(), _landPatternPad.getLengthInNanoMeters()) / 2.7;
    double arrowSizeFactor = 2.3; // A factor of 2 is half the size of the pad.  2.3 is slightly less.

    // first let' s make a right arrow.
    PinOrientationEnum pinEnum = _pad.getPackagePin().getPinOrientationEnum();
    double startingX = centerX - arrowHeadSize;

    if(pinEnum.equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN) ||
       pinEnum.equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN))
      startingX += _landPatternPad.getLengthInNanoMeters() / arrowSizeFactor;
    else if(pinEnum.equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN) ||
       pinEnum.equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN))
      startingX += _landPatternPad.getWidthInNanoMeters() / arrowSizeFactor;
    else if(pinEnum.equals(PinOrientationEnum.NOT_APPLICABLE))
    {
      // in this case, we generally don't create a renderer, so
      // the only way to get here is if a use change the cad on
      // the fly from say a gullwing to a cga. so let's just return an empty shape
      return new Rectangle2D.Double(centerX, centerY, 0, 0);
    }
    else
      Assert.expect(false);

    double topLeftY = centerY + arrowHeadSize / 2.0;
    double bottomLeftY = centerY - arrowHeadSize / 2.0;
    double topLeftX = startingX;
    double bottomLeftX = startingX;
    double pointEndY = centerY;
    double pointEndX = startingX + arrowHeadSize;

    java.awt.geom.GeneralPath gp = new java.awt.geom.GeneralPath();
    gp.moveTo((float)pointEndX, (float)pointEndY);
    gp.lineTo((float)topLeftX, (float)topLeftY);
    gp.lineTo((float)bottomLeftX, (float)bottomLeftY);

    gp.lineTo((float)pointEndX, (float)pointEndY);
    gp.closePath();

    java.awt.geom.AffineTransform transform = _pad.getOrientationArrowTransform();

//    // ok, now rotate it by its pin orientation enum and by its land pattern rotation case.
//    java.awt.geom.AffineTransform transform = java.awt.geom.AffineTransform.getRotateInstance(Math.toRadians(_pad.getPackagePin().getPinOrientationEnum().getDegrees() + _landPatternPad.getDegreesRotation()), centerX, centerY);
//
//    // apply the transform of the component.
//    _pad.getComponent().preConcatenateShapeTransform(transform);
    if(_pad.getComponent().getBoard().getPanel().getProject().getTestProgram().isUsingOneMagnification() == false)
    {
         _nanoMetersToPixelTransform = AffineTransform.getScaleInstance(1.0 / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel(),
                                                                                         1.0 / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
    }
    else
    {
        if(_pad.getSubtype().getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
             _nanoMetersToPixelTransform = AffineTransform.getScaleInstance(1.0 / (double)MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel(),
                                                                                             1.0 / (double)MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel());
        else
             _nanoMetersToPixelTransform = AffineTransform.getScaleInstance(1.0 / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel(),
                                                                                             1.0 / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
    }
    // now apply the nano meters to pixel transform.
    transform.preConcatenate(_nanoMetersToPixelTransform);

    return transform.createTransformedShape(gp);
  }
}
