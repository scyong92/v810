package com.axi.v810.gui.imagePane;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.image.RenderedImage;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.util.image.*;
import java.awt.geom.AffineTransform;

/**
 * @author Patrick Lacz
 */
public class OverlayImageRenderer extends BufferedImageRenderer
{

  /**
   * @author Patrick Lacz
   */
  public OverlayImageRenderer(Image image, RegionOfInterest whereToDrawImage, Color renderingColor)
  {
    Assert.expect(image != null);
    Assert.expect(whereToDrawImage != null);

    initializeImage(image.getAlphaBufferedImage(renderingColor), whereToDrawImage.getMinX(), whereToDrawImage.getMinY(),
                    whereToDrawImage.getWidth(), whereToDrawImage.getHeight());
  }

  /**
   * This method needs to overridden to get the most recent data required to
   * draw the appropriate Shape.
   *
   * @author Patrick Lacz
   */
  protected void refreshData()
  {
  }
}
