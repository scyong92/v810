package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author kok-chun.tan
 */
public class GenerateImageSetWizardVariationOptionPanel extends WizardPanel
{
  private JLabel _instructionsLabel = new JLabel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private Border _mainPanelBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
  private JScrollPane _availableItemsScroller;
  private JComboBox _availableBoardTypesComboBox;

  private JList _availableItemsList = null; // this will hold components, subtypes, or joint types, pins etc
  private DefaultListModel _availableItemsListModel = null;

  private Object _selectedItem = null; // this is an object as it can be a board, component, subtype, joint type, or pad.
  private String _selectedVariation = null;
  private Project _project = null; // currently loaded project

  private boolean _boardScope = true;
  private ImageSetData _imageSetData = null;

  private GenerateImageSetWizardDialog _dialog = null;
  
  private ListSelectionListener _availableItemsListActionListener;
  private ActionListener _availableBoardTypesComboBoxActionListener;

  /**
   * @author Erica Wheatcroft
   */
  public GenerateImageSetWizardVariationOptionPanel(Project project, ImageSetData imageSetData, WizardDialog dialog)
  {
    super(dialog);
    Assert.expect(imageSetData != null, "Image set data is null");
    Assert.expect(project != null, "Current Loaded project is null");
    _imageSetData = imageSetData;
    _project = project;

    _dialog = (GenerateImageSetWizardDialog)dialog;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    //remove listener first
    if (_availableBoardTypesComboBox != null)
      _availableBoardTypesComboBox.removeActionListener(_availableBoardTypesComboBoxActionListener);
    if (_availableItemsList != null)
      _availableItemsList.removeListSelectionListener(_availableItemsListActionListener);
    
    if (_availableBoardTypesComboBox != null)
      _availableBoardTypesComboBox.removeAllItems();

    if (_availableItemsListModel != null)
      _availableItemsListModel.clear();
    _selectedItem = null;
    _project = null;
    _imageSetData = null;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void removeObservers()
  {
    // do nothing
  }


  /**
   * @author Erica Wheatcroft
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null, "Name of panel is null");
    Assert.expect(name.length() > 0, "Name of panel is an empty string");
    _name = name;
  }

  /**
   * @author Erica Wheatcroft
   */
  public String getWizardName()
  {
    Assert.expect(_name != null, "Name of panel is not set");
    return _name;
  }


  /**
   * @author Erica Wheatcroft
   */
  private void setInstructionLabel()
  {
    _instructionsLabel.setText(StringLocalizer.keyToString("GISWK_VARIATION_SELECTION_INSTRUCTION_KEY"));
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel() throws DatastoreException
  {
    _mainPanelBorderLayout.setVgap(5);
    removeAll();
    setLayout(_mainPanelBorderLayout);
    setBorder(_mainPanelBorder);

    // create the instructions panel..
    JPanel instructionsPanel = createInstructionsPanel();
    add(instructionsPanel, java.awt.BorderLayout.NORTH);

    //create the board type combobox
    GridLayout gridLayout = (GridLayout) instructionsPanel.getLayout();
    gridLayout.setRows(2);
    gridLayout.setVgap(10);
    instructionsPanel.add(createBoardTypePanel());
    add(createBoardTypeOptionsPanel(), java.awt.BorderLayout.CENTER);
    populateBoardTypeOptionsPanel(null);

    // now create the listeners
    createComponentListeners();
  }

  /**
   * @author Erica Wheatcroft
   */
  private JPanel createInstructionsPanel()
  {
    JPanel instructionsPanel = new JPanel();
    // figure out the instruction, set the label and add the panel
    setInstructionLabel();
    GridLayout instructionsPanelLayout = new GridLayout();
    instructionsPanelLayout.setColumns(0);
    instructionsPanel.setLayout(instructionsPanelLayout);
    // adding the label
    instructionsPanel.add(_instructionsLabel);
    _instructionsLabel.setFont(FontUtil.getBoldFont(_instructionsLabel.getFont(),Localization.getLocale()));
    return instructionsPanel;
  }

  /**
   * This method will create a board type panel.. This panel will have a combo box with all available board types. If only
   * one board type exist then the combo box will be replaced with a label.
   * @author Erica Wheatcroft
   */
  private JPanel createBoardTypePanel()
  {
    JPanel boardTypePanel = new JPanel();
    PairLayout boardTypePanelLayout = new PairLayout();
    boardTypePanel.setLayout(boardTypePanelLayout);
    JLabel boardTypeLabel = new JLabel(StringLocalizer.keyToString("GISKW_SELECTED_BOARD_TYPE_LABEL_KEY"));
    boardTypePanel.add(boardTypeLabel);
    _availableBoardTypesComboBox = new JComboBox();
    boardTypePanel.add(_availableBoardTypesComboBox);
    return boardTypePanel;
  }

  /**
   * This will set up all listeners for the necessary gui components.
   * @author Erica Wheatcroft
   */
  private void createComponentListeners()
  {
    if (_availableBoardTypesComboBox != null)
    {
      availableBoardTypesComboBox_ActionListener();
    }

    if(_availableItemsList != null)
    {
      availableItemsList_ActionListener();
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void availableBoardTypesComboBox_ActionListener()
  {
    _availableBoardTypesComboBoxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        BoardType boardType = (BoardType) _availableBoardTypesComboBox.getSelectedItem();
        try
        {
          populateBoardTypeOptionsPanel(boardType);
        }
        catch (DatastoreException ex)
        {
          //nothing
        }
      }
    };
    _availableBoardTypesComboBox.addActionListener(_availableBoardTypesComboBoxActionListener);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void availableItemsList_ActionListener()
  {
    _availableItemsListActionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent evt)
      {
        _selectedItem = _availableItemsList.getSelectedValue();
        _selectedVariation = (String) _selectedItem;
        _imageSetData.setVariationName(_selectedVariation);
        validatePanel();
      }
    };
    _availableItemsList.addListSelectionListener(_availableItemsListActionListener);
  }

  /**
   * this will create the item selection panel which is just a JList and a label.
   * @author Erica Wheatcroft
   */
  private JPanel createItemSelectionPanel()
  {
     // now lets create the list panel - this panel will contain the list of elements the user can select.
    _availableItemsList = new JList();
    _availableItemsListModel = new DefaultListModel();
    _availableItemsList.setModel(_availableItemsListModel);
    JLabel availableItemsLabel = new JLabel();
    // set the label text.
    availableItemsLabel.setText(StringLocalizer.keyToString("GISWK_VARIATION_SELECTION_INSTRUCTION_KEY"));

    _availableItemsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _availableItemsList.setVisibleRowCount(8); // visible row count

    JPanel itemListPanel = new JPanel();
    BorderLayout itemListPanelLayout = new BorderLayout();
    itemListPanel.setLayout(itemListPanelLayout);
    itemListPanel.add(availableItemsLabel,BorderLayout.NORTH);
    _availableItemsScroller = new JScrollPane();
    _availableItemsScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
    _availableItemsScroller.getViewport().add(_availableItemsList);
    itemListPanel.add(_availableItemsScroller, BorderLayout.CENTER);

    return itemListPanel;
  }

  /**
   * This will create the board type image set options panel. This will create the components only and not populate them
   *
   * @author Erica Wheatcroft
   */
  private JPanel createBoardTypeOptionsPanel()
  {
    return createItemSelectionPanel();
  }

  /**
   * This method will populate the list of available board instances for the currently loaded program.
   * @author Erica Wheatcroft
   */
  private void populateBoardTypeOptionsPanel(BoardType selectedBoardType) throws DatastoreException
  {
    Assert.expect(_project != null, "Project is null");
    Assert.expect(_availableItemsList != null, "list is null");

    if (selectedBoardType == null)
    {
      // first populate the available board types combo box.
      java.util.List<BoardType> boardTypes = _project.getPanel().getBoardTypes();
      _availableBoardTypesComboBox.removeAllItems();
      for (BoardType type : boardTypes)
      {
        _availableBoardTypesComboBox.addItem(type);
      }

      // now select the first one board type
      _availableBoardTypesComboBox.setSelectedIndex(0);
      selectedBoardType = (BoardType) _availableBoardTypesComboBox.getSelectedItem();
    }

    _availableItemsListModel.clear();
    
    java.util.List<String> variations = new ArrayList<String> ();
    variations.add(VariationSettingManager.ALL_LOADED);
    variations.addAll(VariationSettingManager.getInstance().getProjectNameToVariationNamesMap(_project.getName()));

    for(String variation : variations)
      _availableItemsListModel.addElement(variation);
  }

  /**
   * @author Erica Wheatcroft
   */
  public void populatePanelWithData()
  {
    setInitialButtonState();

    // set the title
    _dialog.changeTitle(StringLocalizer.keyToString("GISWK_SCREEN_2_TITLE_KEY"));

    try
    {
      createPanel();
    }
    catch (DatastoreException ex)
    {
      //nothing
    }
    populateWithCurrentSelections();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateWithCurrentSelections()
  {
    if (_selectedItem != null)
    {
      _selectedVariation = (String)_selectedItem;
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public void validatePanel()
  {
    //in order for this secondary screen to be complete the select item must not be null.
    if(_selectedItem != null)
    {
      WizardButtonState buttonState = new WizardButtonState(true, true, false, true);
      _observable.updateButtonState(buttonState);
    }
    else
    {
      // do nothing we are not ready to go to the next screen
    }
  }

  /**
   * This method will set up the button state for this screen of the wizzard
   *
   * @author Erica Wheatcroft
   */
  private void setInitialButtonState()
  {
    WizardButtonState buttonState = new WizardButtonState(false, true, false, true);
    _observable.updateButtonState(buttonState);
  }
}
