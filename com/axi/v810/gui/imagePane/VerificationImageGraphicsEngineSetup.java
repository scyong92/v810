package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * The VerificationImageGraphicsEngineSetup class sets up the graphics engine
 * for image display during fine tuning.  *
 * @author George A. David
 */
public class VerificationImageGraphicsEngineSetup implements Observer, RendererCreator, ImagePanelGraphicsEngineSetupInt
{
  private static final int _BORDER_SIZE_IN_PIXELS = 0;
  private static final double _ZOOM_FACTOR = 1.2;
  private static final int _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(3000);  
  private static final int _BORDER_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(100);

  // convert to non-static variable due to variable magnification
  private int _REF_DES_VISIBILITY_THRESHOLD = MathUtil.convertMilsToNanoMetersInteger(30) / MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
  private int _PAD_VISIBILITY_THRESHOLD = MathUtil.convertMilsToNanoMetersInteger(20) / MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
  private int _PAD_NAME_VISIBILITY_THRESHOLD = MathUtil.convertMilsToNanoMetersInteger(5) / MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
  private int _ORIENTATION_ARROW_VISIBILITY_THRESHOLD = MathUtil.convertMilsToNanoMetersInteger(20) / MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
  
  private GraphicsEngine _graphicsEngine;
  // Graphic engine layers

  // Wei Chin : fix the maximum layer board to handle the graphic update properly
  // edited by : Kee, Chin Seong - make it dynamic so that the UI dragging wont lag
  private int _MAXNUMBER_OF_LAYER_FOR_MULTIBOARD = 60;

  protected Map<Integer, Integer> _subProgramIdToImageLayer = new TreeMap<Integer,Integer>();
  private Map<Integer, Integer> _subProgramIdToPadLayer = new TreeMap<Integer,Integer>();
  private Map<Integer, Integer> _subProgramIdToPadNameLayer = new TreeMap<Integer,Integer>();
  private Map<Integer, Integer> _subProgramIdToOrientationArrowLayer = new TreeMap<Integer,Integer>();
  private Map<Integer, Integer> _subProgramIdToSelectedPadLayer = new TreeMap<Integer,Integer>();
  private Map<Integer, Integer> _subProgramIdToSelectedPadNameLayer = new TreeMap<Integer,Integer>();
  private Map<Integer, Integer> _subProgramIdToSelectedOrientationArrowLayer = new TreeMap<Integer,Integer>();
  private Map<Integer, Integer> _subProgramIdToSelectedRefDesLayer = new TreeMap<Integer,Integer>();
  private Map<Integer, Integer> _subProgramIdToRefDesLayer = new TreeMap<Integer,Integer>();

  private AffineTransform _nanoMetersToPixelTransform = AffineTransform.getScaleInstance(1.0 / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel(),
                                                                                         1.0 / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
  private AffineTransform _rightAlignmentTransform;
  private AffineTransform _leftAlignmentTransform;
  private boolean _showingTopSide = false;
  private boolean _isAdjustingComponent = false;
  private boolean _isAdjustingBoard = false;
  protected Project _project;
  private com.axi.v810.business.panelDesc.Component _selectedComponent;
  private java.util.List<com.axi.guiUtil.Renderer> _selectedPadRenderersForAdjustment = new LinkedList<com.axi.guiUtil.Renderer>();
  private Collection<com.axi.guiUtil.Renderer> _selectedPadRenderersForRightClick = new LinkedList<com.axi.guiUtil.Renderer>();
  private ComponentType _selectedComponentType;
  private Board _selectedBoard;
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  private MouseClickObservable _mouseClickObservable;
  private SelectedRendererObservable _selectedRendererObservable;

  private Collection<Integer> _layersToUseForDragging = new LinkedList<Integer>();
  private Collection<Integer> _layersToDragWhenAdjustingBoard = new LinkedList<Integer>();
//  private Collection<Integer> _layersToApplyLeftAlignmentTransform = new LinkedList<Integer>();
//  private Collection<Integer> _layersToApplyRightAlignmentTransform = new LinkedList<Integer>();
  private double _xOffsetInNanoMeters = 0;
  private double _yOffsetInNanoMeters = 0;

  protected ImageManagerToolBar _toolBar;
  protected ImagePaneRendererCreator _rendererCreator;
//  private Rectangle2D _rightSectionOfPanel;
  private boolean _areVerificationImagesValid = false;
  private boolean _isDisplayed = false;

  // command manager for doing undos
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  
  private MagnificationEnum _magnification = MagnificationEnum.NOMINAL;

  /**
   * @author George A. David
   */
  public VerificationImageGraphicsEngineSetup(ImageManagerToolBar toolBar)
  {
    Assert.expect(toolBar != null);

    _toolBar = toolBar;
    
    _graphicsEngine = new GraphicsEngine(_BORDER_SIZE_IN_PIXELS);
    _graphicsEngine.setShouldGraphicsBeFitToScreen(false);
    _graphicsEngine.setMouseEventsEnabled(true);
    _graphicsEngine.setEnableSelectionOnMouseRightClick(false);
    _mouseClickObservable = _graphicsEngine.getMouseClickObservable();
    _selectedRendererObservable = _graphicsEngine.getSelectedRendererObservable();
//    createLayers();
  }

  /**
   * @author Chong, Wei Chin
   */
  public VerificationImageGraphicsEngineSetup(ImageManagerToolBar toolBar, MagnificationEnum magnification)
  {
    Assert.expect(toolBar != null);
    Assert.expect(magnification != null);

    _toolBar = toolBar;    
    _magnification = magnification;
    _nanoMetersToPixelTransform = AffineTransform.getScaleInstance(1.0 / (double)_magnification.getNanoMetersPerPixel(),
                                                                                         1.0 / (double)_magnification.getNanoMetersPerPixel());
    
    _graphicsEngine = new GraphicsEngine(_BORDER_SIZE_IN_PIXELS);
    _graphicsEngine.setShouldGraphicsBeFitToScreen(false);
    _graphicsEngine.setMouseEventsEnabled(true);
    _graphicsEngine.setEnableSelectionOnMouseRightClick(false);
    _mouseClickObservable = _graphicsEngine.getMouseClickObservable();
    _selectedRendererObservable = _graphicsEngine.getSelectedRendererObservable();
    
    _REF_DES_VISIBILITY_THRESHOLD = MathUtil.convertMilsToNanoMetersInteger(30) / _magnification.getNanoMetersPerPixel();
    _PAD_VISIBILITY_THRESHOLD = MathUtil.convertMilsToNanoMetersInteger(20) / _magnification.getNanoMetersPerPixel();
    _PAD_NAME_VISIBILITY_THRESHOLD = MathUtil.convertMilsToNanoMetersInteger(5) / _magnification.getNanoMetersPerPixel();
    _ORIENTATION_ARROW_VISIBILITY_THRESHOLD = MathUtil.convertMilsToNanoMetersInteger(20) / _magnification.getNanoMetersPerPixel();
    
//    createLayers();
  }

  /**
   * @author George A. David
   */
  public GraphicsEngine getGraphicsEngine()
  {
    Assert.expect(_graphicsEngine != null);

    return _graphicsEngine;
  }

  /**
   * @author George A. David
   */
  private void createLayers()
  {
    int layerNumber = JLayeredPane.DEFAULT_LAYER.intValue() - 1;

    //XCR-3437, Cad offset when view verification on long panel recipe
    java.util.List<Integer> subProgramIds = new ArrayList<Integer>();
    subProgramIds.addAll(_project.getTestProgram().getAllUsedTestSubProgramID());
    Collections.sort(subProgramIds, Collections.reverseOrder());
    
    for (Integer testSubProgramID : subProgramIds)
    {
      _subProgramIdToImageLayer.put(testSubProgramID, ++layerNumber);
    }
    for (Integer testSubProgramID :subProgramIds)
    {
      _subProgramIdToPadLayer.put(testSubProgramID, ++layerNumber);
    }
    for (Integer testSubProgramID : subProgramIds)
    {
      _subProgramIdToRefDesLayer.put(testSubProgramID, ++layerNumber);
    }
    for (Integer testSubProgramID : subProgramIds)
    {
      _subProgramIdToOrientationArrowLayer.put(testSubProgramID, ++layerNumber);
    }
    for (Integer testSubProgramID : subProgramIds)
    {
      _subProgramIdToPadNameLayer.put(testSubProgramID, ++layerNumber);
    }
    for (Integer testSubProgramID : subProgramIds)
    {
      _subProgramIdToSelectedPadLayer.put(testSubProgramID, ++layerNumber);
      _subProgramIdToSelectedPadNameLayer.put(testSubProgramID, ++layerNumber);
      _subProgramIdToSelectedOrientationArrowLayer.put(testSubProgramID, ++layerNumber);
      _subProgramIdToSelectedRefDesLayer.put(testSubProgramID, ++layerNumber);
    }
      
    _layersToUseForDragging.clear();
    _layersToDragWhenAdjustingBoard.clear();
    _layersToUseForDragging.addAll(_subProgramIdToImageLayer.values());
    _layersToUseForDragging.addAll(_subProgramIdToRefDesLayer.values());
    _layersToUseForDragging.addAll(_subProgramIdToPadLayer.values());
    _layersToUseForDragging.addAll(_subProgramIdToPadNameLayer.values());
    _layersToUseForDragging.addAll(_subProgramIdToOrientationArrowLayer.values());

    _layersToDragWhenAdjustingBoard.addAll(_subProgramIdToImageLayer.values());
  }

  /**
   * @author George A. David
   * @author Chong, Wei Chin
   */
  private void setColorsAndThresholds()
  {
    _graphicsEngine.setForeground(_subProgramIdToImageLayer.values(), LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(_subProgramIdToPadLayer.values(), LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(_subProgramIdToSelectedPadLayer.values(), LayerColorEnum.VERIFY_CAD_SELECTED_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(_subProgramIdToSelectedOrientationArrowLayer.values(), LayerColorEnum.VERIFY_CAD_ORIENTATION_ARROW_COLOR.getColor());
    _graphicsEngine.setForeground(_subProgramIdToSelectedRefDesLayer.values(), LayerColorEnum.REFERENCE_DESIGNATOR_COLOR.getColor());
    _graphicsEngine.setForeground(_subProgramIdToSelectedPadNameLayer.values(), LayerColorEnum.PAD_NAME_COLOR.getColor());
    _graphicsEngine.setForeground(_subProgramIdToRefDesLayer.values(), LayerColorEnum.REFERENCE_DESIGNATOR_COLOR.getColor());
    _graphicsEngine.setForeground(_subProgramIdToPadNameLayer.values(), LayerColorEnum.PAD_NAME_COLOR.getColor());
    _graphicsEngine.setForeground(_subProgramIdToOrientationArrowLayer.values(), LayerColorEnum.VERIFY_CAD_ORIENTATION_ARROW_COLOR.getColor());
    
    _graphicsEngine.setCreationThreshold(_subProgramIdToImageLayer.values(), 1000, this);
    _graphicsEngine.setCreationThreshold(_subProgramIdToRefDesLayer.values(), _REF_DES_VISIBILITY_THRESHOLD, this);

    _graphicsEngine.setVisibilityThreshold(_subProgramIdToPadLayer.values(), _PAD_VISIBILITY_THRESHOLD);
    _graphicsEngine.setVisibilityThreshold(_subProgramIdToOrientationArrowLayer.values(), _ORIENTATION_ARROW_VISIBILITY_THRESHOLD);
    _graphicsEngine.setVisibilityThreshold(_subProgramIdToPadNameLayer.values(), _PAD_NAME_VISIBILITY_THRESHOLD);
  }

  /**
   * @author George A. David
   */
  private void reset()
  {
    _graphicsEngine.reset();    
  }

  /**
   * @author George A. David
   */
  protected void setUpToolBar()
  {
    _toolBar.setAllowDragGraphics(true);
    _toolBar.setAllowMeasurements(true);
    _toolBar.setAllowResetGraphics(false);
    _toolBar.setAllowToggleGraphics(true);
    _toolBar.setAllowToggleImageStay(false);

    _toolBar.setAllowViewBottomSide(true);
    _toolBar.setAllowViewTopSide(true);

    _toolBar.setAllowZoomIn(true);
    _toolBar.setAllowZoomOut(true);
    _toolBar.setAllowZoomRectangle(true);

    setToolBarEnabled(true);
//    _toolBar.setDragToggleButtonSelected(true);
  }

  /**
   * @author George A. David
   */
  public void setToolBarEnabled(boolean enabled)
  {
    _toolBar.setAllowZoomIn(enabled);
    _toolBar.setAllowZoomOut(enabled);
    _toolBar.setAllowZoomRectangle(enabled);
    _toolBar.setAllowDragGraphics(enabled);
    _toolBar.setAllowMeasurements(enabled);
    _toolBar.setAllowViewBottomSide(enabled);
    _toolBar.setAllowViewTopSide(enabled);
    _toolBar.setAllowToggleGraphics(enabled);
  }

  /**
   * @author George A. David
   * @author Edited by : Chin Seong, Kee
   *                     Related CR : XCR1479 
   *                                  Adjust CAD slow, so to prevent this happened we will only created 
   *                                  necessary size of Test SubProgram, so that the UI wont lag
   */
  public void display(boolean checkLatestestProgram)
  {
    _isDisplayed = true;
    reset();
    _guiObservable.addObserver(this);
    _projectObservable.addObserver(this);
    _selectedRendererObservable.addObserver(this);
    _mouseClickObservable.addObserver(this);

    _project = Project.getCurrentlyLoadedProject();
    
    boolean isCheckLatestProgram = _project.isCheckLatestTestProgram();
    _project.setCheckLatestTestProgram(checkLatestestProgram);
    
//    if(_MAXNUMBER_OF_LAYER_FOR_MULTIBOARD < _project.getTestProgram().getSizeOfAllTestSubProgramsForGraphicEngineLayersSetup() + 1)
//    {
      //always clear layers first before add more layers
//      _MAXNUMBER_OF_LAYER_FOR_MULTIBOARD = _project.getTestProgram().getSizeOfAllTestSubProgramsForGraphicEngineLayersSetup() + 1;
    clearAllLayers();
    createLayers();
//    }
      
    int firstTestSubProgramId = -1;
    _areVerificationImagesValid = _project.areVerificationImagesValid(false);
//    _areVerificationImagesValid = true;
    MathUtilEnum displayUnits = _project.getDisplayUnits();

    if(displayUnits.equals(MathUtilEnum.NANOMETERS) == false)
    {
      _graphicsEngine.setMeasurementInfo(MathUtil.convertUnits(_magnification.getNanoMetersPerPixel(), MathUtilEnum.NANOMETERS, displayUnits),
                                       MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits),
                                       MeasurementUnits.getMeasurementUnitString(displayUnits));
    }

    enableToolBarIfAppropriate();

    createImagePaneRendererCreator(checkLatestestProgram);

    AffineTransform pixelToNanoMetersTransform = null;
    try
    {
      pixelToNanoMetersTransform = _nanoMetersToPixelTransform.createInverse();
    }
    catch (NoninvertibleTransformException ex)
    {
      Assert.expect(false);
    }

    _rightAlignmentTransform = null;
    _leftAlignmentTransform = null;

    // ok, now drag to layers to take into account alignment.
    for(TestSubProgram subProgram : _project.getTestProgram().getAllTestSubPrograms())
    {
      if(_magnification.equals(MagnificationEnum.NOMINAL) && subProgram.isHighMagnification())
        continue;
      
      if(_magnification.equals(MagnificationEnum.H_NOMINAL) && subProgram.isLowMagnification())
        continue;
      
      if(firstTestSubProgramId == -1)
        firstTestSubProgramId = subProgram.getId();
      
//      Collection<Integer> layersToApplyAlignmentTransform = null;
      AffineTransform alignmentTransform = subProgram.getLastAlignedWithVerificationAlignmentTransform();
      if(alignmentTransform == null)
        continue;
      if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
      {
//        layersToApplyAlignmentTransform = _layersToApplyRightAlignmentTransform;
        _rightAlignmentTransform = alignmentTransform;
      }
      else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
      {
//        layersToApplyAlignmentTransform = _layersToApplyLeftAlignmentTransform;
        _leftAlignmentTransform = alignmentTransform;
      }
      else
        Assert.expect(false);
      AffineTransform transform = new AffineTransform(pixelToNanoMetersTransform);
      transform.preConcatenate(alignmentTransform);
      transform.preConcatenate(_nanoMetersToPixelTransform);
//      for(int layerNumber : layersToApplyAlignmentTransform)
//        _graphicsEngine.setInitialTransformForLayer(layerNumber, transform);
      // wei chin added for individual board aligment
      _graphicsEngine.setInitialTransformForLayer(_subProgramIdToPadLayer.get(subProgram.getId()), transform);
      _graphicsEngine.setInitialTransformForLayer(_subProgramIdToPadNameLayer.get(subProgram.getId()), transform);
      _graphicsEngine.setInitialTransformForLayer(_subProgramIdToRefDesLayer.get(subProgram.getId()), transform);
      _graphicsEngine.setInitialTransformForLayer(_subProgramIdToOrientationArrowLayer.get(subProgram.getId()), transform);
      _graphicsEngine.setInitialTransformForLayer(_subProgramIdToSelectedPadLayer.get(subProgram.getId()), transform);
      _graphicsEngine.setInitialTransformForLayer(_subProgramIdToSelectedPadNameLayer.get(subProgram.getId()), transform);
      _graphicsEngine.setInitialTransformForLayer(_subProgramIdToSelectedRefDesLayer.get(subProgram.getId()), transform);
      _graphicsEngine.setInitialTransformForLayer(_subProgramIdToSelectedOrientationArrowLayer.get(subProgram.getId()), transform);
    }

    Rectangle2D maxRegionRect = _nanoMetersToPixelTransform.createTransformedShape(new Rectangle2D.Double(0, 0, _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS, _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS)).getBounds2D();
    RegionOfInterestRenderer renderer = new RegionOfInterestRenderer(maxRegionRect);

//    for( int verificationImageLayer : _subProgramIdToImageLayer.values())
//    {
//      _graphicsEngine.addRenderer(verificationImageLayer, renderer);
//    }
    // Wei Chin - fixed for crash error when fit Graphic to Screen
    if(firstTestSubProgramId == -1)
        firstTestSubProgramId = 0;
    _graphicsEngine.addRenderer((int)_subProgramIdToImageLayer.get(firstTestSubProgramId), renderer);
    _graphicsEngine.setAxisInterpretation(true, false);
    _graphicsEngine.fitGraphicsToScreen();
    _graphicsEngine.removeRenderer(renderer);

    setColorsAndThresholds();

//    _graphicsEngine.setDragGraphicsMode(true);
    if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment() == false)
      _graphicsEngine.setGroupSelectMode(true);

    _graphicsEngine.setLayersToUseWhenDraggingGraphics(_layersToUseForDragging);

    _graphicsEngine.repaint();
    
    _project.setCheckLatestTestProgram(isCheckLatestProgram);
  }

  /**
   * @author Scott Richardson
   */
  protected void enableToolBarIfAppropriate()
  {
    if (_areVerificationImagesValid)
    {
      setUpToolBar();
    }
    else
    {
      setToolBarEnabled(false);
      return;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void clearImagePaneRendererCreator()
  {
    if (_rendererCreator != null)
    {
      _rendererCreator.clearSurroundingReconstructionRegions();
      _rendererCreator.reset();
      _rendererCreator = null;
    }
  }

  /**
   * @author Scott Richardson
   */
  protected void createImagePaneRendererCreator(boolean checkLatestestProgram)
  {
    _rendererCreator = new ImagePaneRendererCreator(_project, _magnification, checkLatestestProgram);
    _rendererCreator.setSelectedColor(LayerColorEnum.VERIFY_CAD_SELECTED_PAD_COLOR.getColor());
    _rendererCreator.setImageLayer(_subProgramIdToImageLayer);
    _rendererCreator.setPadLayer(_subProgramIdToPadLayer);
    setPadNameLayerVisibility(true);
    setRefDesLayerVisibility(true);
    setOrientationLayerVisibility(true);
  }
  
  /*
   * @author Chin Seong, Kee
   */
  private void setOrientationLayerVisibility(boolean visible)
  {
    if(visible)
    {
      _rendererCreator.setOrientationArrowLayer(_subProgramIdToOrientationArrowLayer);
    }
    else
    {
      for(int orientationLayer : _subProgramIdToOrientationArrowLayer.values())
        _graphicsEngine.removeRenderers(orientationLayer);
    }
  }
  
  /*
   * @author Chin Seong, Kee
   */
  private void setRefDesLayerVisibility(boolean visible)
  {
    if(visible)
    {
      _rendererCreator.setRefDesLayer(_subProgramIdToRefDesLayer);
    }
    else
    {
      for(int refDesLayes : _subProgramIdToRefDesLayer.values())
        _graphicsEngine.removeRenderers(refDesLayes);
    }
  }
  
  /*
   * @author Chin Seong, Kee
   */
  private void setPadNameLayerVisibility(boolean visible)
  {
    if(visible)
    {
      _rendererCreator.setPadNameLayer(_subProgramIdToPadNameLayer);
    }
    else
    {
      for(int padNameLayers : _subProgramIdToPadNameLayer.values())
        _graphicsEngine.removeRenderers(padNameLayers);
    }
  }

  /**
   * @author George A. David
   */
  private void adjustBoard()
  {
    // during board adjustment, we don't allow the graphics to be re-drawn.
    // so, let's set it up so that the maximum region we allow will have
    // all the graphics drawn. That way if they zoom out, they will see
    // more graphics.
    Rectangle2D visibleRect = _graphicsEngine.getVisibleRectangleInRendererCoordinates();
    PanelRectangle regionToDrawGraphics = new PanelRectangle(visibleRect);
    regionToDrawGraphics.setRect((regionToDrawGraphics.getCenterX() * _magnification.getNanoMetersPerPixel()) - (_MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS / 2.0),
                                 (regionToDrawGraphics.getCenterY() * _magnification.getNanoMetersPerPixel()) - (_MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS / 2.0),
                                 _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS,
                                 _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS);

    _isAdjustingBoard = true;
    _graphicsEngine.fitRectangleInRendererCoordinatesToScreen(visibleRect);
    _rendererCreator.createCadRenderers(_graphicsEngine, regionToDrawGraphics, _showingTopSide);
    _graphicsEngine.setLayersToUseWhenDraggingGraphics(_layersToDragWhenAdjustingBoard);
    _graphicsEngine.setDragGraphicsMode(true);
    _graphicsEngine.mark();
    _toolBar.setAllowViewBottomSide(false);
    _toolBar.setAllowViewTopSide(false);
  }

  /**
   * @author George A. David
   */
  private void finishAdjustingBoard(boolean wasAdjustmentCancelled)
  {
    if (_rendererCreator != null)  // if this is null, we've left Verify CAD (this is cleaned up when we leave)
    {
      _rendererCreator.clearCadRenderers(_graphicsEngine);
      calculateOffsetInNanoMeters();
      _isAdjustingBoard = false;
      if (wasAdjustmentCancelled)      
        _graphicsEngine.resetToMark();
      setDragGraphicsMode(false);
    }
    _isAdjustingBoard = false;
    _graphicsEngine.setLayersToUseWhenDraggingGraphics(_layersToUseForDragging);
    _toolBar.setAllowViewBottomSide(true);
    _toolBar.setAllowViewTopSide(true);
  }

  /**
   * @author George A. David
   */
  private void makeSelectedComponentVisible()
  {
    Rectangle componentBounds = getAlignedBoundsOfSelectedComponentInPixels();
    if (_graphicsEngine.isRectangleInRendererCoordinatesVisibleOnScreen(componentBounds) == false)
      _graphicsEngine.centerRectInRendererCoordinates(componentBounds);
  }

  /**
   * @author George A. David
   */
  private void adjustSelectedComponent()
  {
    _selectedPadRenderersForAdjustment.clear();
    for(Pad pad : _selectedComponent.getPads())
    {
      PadRegionOfInterestRenderer padRenderer = new PadRegionOfInterestRenderer(
          _nanoMetersToPixelTransform.createTransformedShape(pad.getShapeRelativeToPanelInNanoMeters()), 3, pad);
      ReferenceDesignatorRenderer refDesRenderer = new ReferenceDesignatorRenderer(_selectedComponent, false);
      PadNameRenderer padNameRenderer = new PadNameRenderer(pad, false);
      OrientationArrowRenderer orientationArrowRenderer = new OrientationArrowRenderer(pad);

      // wei chin modify for individual board alignment
      for(TestSubProgram testSubProgram : _project.getTestProgram().getAllTestSubPrograms())
      {
        if(_magnification.equals(MagnificationEnum.NOMINAL) && testSubProgram.isHighMagnification())
          continue;
      
        if(_magnification.equals(MagnificationEnum.H_NOMINAL) && testSubProgram.isLowMagnification())
          continue;
      
        if( testSubProgram.getVerificationRegionsBoundsInNanoMeters().intersects(pad.getShapeRelativeToPanelInNanoMeters().getBounds2D()) )
        {
          _graphicsEngine.addRenderer(_subProgramIdToSelectedPadLayer.get(testSubProgram.getId()), padRenderer);
          _graphicsEngine.addRenderer(_subProgramIdToSelectedRefDesLayer.get(testSubProgram.getId()), refDesRenderer);
          _graphicsEngine.addRenderer(_subProgramIdToSelectedPadNameLayer.get(testSubProgram.getId()), padNameRenderer);
          _graphicsEngine.addRenderer(_subProgramIdToSelectedOrientationArrowLayer.get(testSubProgram.getId()), orientationArrowRenderer);
          break;
        }
      }
      
      _selectedPadRenderersForAdjustment.add(padRenderer);
      _selectedPadRenderersForAdjustment.add(refDesRenderer);
      _selectedPadRenderersForAdjustment.add(padNameRenderer);
      _selectedPadRenderersForAdjustment.add(orientationArrowRenderer);

    }

    _isAdjustingComponent = true;
    _showingTopSide = _selectedComponent.isTopSide();
    setDragGraphicsMode(true);
    // ok, now make sure the component is visible.
    makeSelectedComponentVisible();
    _graphicsEngine.mark();
    _rendererCreator.setShowSelectedPadsOrFiducials(false);
    calculateOffsetInNanoMeters();
    updateRenderers(_graphicsEngine, _subProgramIdToImageLayer.values().iterator().next());
    updateRenderers(_graphicsEngine, _subProgramIdToRefDesLayer.values().iterator().next());
    _toolBar.setAllowViewBottomSide(false);
    _toolBar.setAllowViewTopSide(false);
  }

  /**
   * @author George A. David
   */
  private void finishAdjustingSelectedComponent(boolean wasAdjustmentCancelled)
  {
    if (_rendererCreator != null)
    {
      if (wasAdjustmentCancelled)
        _rendererCreator.clearCadRenderers(_graphicsEngine);
      _graphicsEngine.removeRenderers(_selectedPadRenderersForAdjustment);

      _rendererCreator.setShowSelectedPadsOrFiducials(true);
      calculateOffsetInNanoMeters();
      _isAdjustingComponent = false;

      if (wasAdjustmentCancelled)
        _graphicsEngine.resetToMark();
      setDragGraphicsMode(false);
    }
    _isAdjustingComponent = false;
    _toolBar.setAllowViewBottomSide(true);
    _toolBar.setAllowViewTopSide(true);    
  }

  /**
   * @author George A. David
   * @author Phang Siew Yeng - add handling for long panel with short program if is board based alignment
   */
  private Rectangle getAlignedBoundsOfSelectedComponentInPixels()
  {
    Assert.expect(_selectedComponent != null);

    java.awt.Shape shape = _selectedComponent.getShapeRelativeToPanelInNanoMeters();
    if( _project.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      if(_leftAlignmentTransform == null || _project.getPanel().getPanelSettings().getRightSectionOfLongPanel().intersectsShape(shape))
        shape = _rightAlignmentTransform.createTransformedShape(shape);
      else
        shape = _leftAlignmentTransform.createTransformedShape(shape);
    }
    else
    {
      MagnificationTypeEnum magnificationType = null;
      if(_magnification.equals(MagnificationEnum.NOMINAL))
        magnificationType = MagnificationTypeEnum.LOW;
      else if(_magnification.equals(MagnificationEnum.H_NOMINAL))
        magnificationType = MagnificationTypeEnum.HIGH;
        
      if(_selectedComponent.getBoard().isLongBoard())
      {
        TestSubProgram subProgram = null;
        if( _selectedComponent.getBoard().getRightSectionOfLongBoard().intersectsShape(shape))
          subProgram = _project.getTestProgram().getTestSubProgramHasAlignmentRegion(_selectedComponent.getBoard(), PanelLocationInSystemEnum.RIGHT, magnificationType);
        else
          subProgram = _project.getTestProgram().getTestSubProgramHasAlignmentRegion(_selectedComponent.getBoard(), PanelLocationInSystemEnum.LEFT, magnificationType);
        shape = subProgram.getCoreRuntimeAlignmentTransform().createTransformedShape(shape);
      }
      else
      {
        if (_project.getPanel().getPanelSettings().isLongPanel() && 
            _selectedComponent.getBoard().getRightSectionOfLongBoard().getMaxY() <= XrayTester.getMaxImageableAreaLengthInNanoMeters())
        {
          TestSubProgram subProgram = _project.getTestProgram().getTestSubProgramHasAlignmentRegion(_selectedComponent.getBoard(), PanelLocationInSystemEnum.LEFT, magnificationType);
          shape = subProgram.getCoreRuntimeAlignmentTransform().createTransformedShape(shape);
        }
        else
        {
          TestSubProgram subProgram = _project.getTestProgram().getTestSubProgramHasAlignmentRegion(_selectedComponent.getBoard(), PanelLocationInSystemEnum.RIGHT, magnificationType);
          shape = subProgram.getCoreRuntimeAlignmentTransform().createTransformedShape(shape);
        }
      }
    }

    Rectangle bounds = shape.getBounds();
    bounds.setRect(bounds.getX() / _magnification.getNanoMetersPerPixel(),
                   bounds.getY() / _magnification.getNanoMetersPerPixel(),
                   bounds.getWidth() / _magnification.getNanoMetersPerPixel(),
                   bounds.getHeight() / _magnification.getNanoMetersPerPixel());

    return bounds;
  }

  /**
   * @author George A. David
   * @author Phang Siew Yeng - add handling for long panel with short program if is board based alignment
   */
  private Rectangle getMaxVisibleAlignedBoundsOfSelectedComponentInPixels()
  {
    Assert.expect(_selectedComponent != null);

    java.awt.Shape shape = _selectedComponent.getShapeRelativeToPanelInNanoMeters();

    if( _project.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      if(_rightAlignmentTransform != null || _leftAlignmentTransform != null)
      {
        if(_leftAlignmentTransform == null || _project.getPanel().getPanelSettings().getRightSectionOfLongPanel().intersectsShape(shape))
          shape = _rightAlignmentTransform.createTransformedShape(shape);
        else
          shape = _leftAlignmentTransform.createTransformedShape(shape);
      }
    }
    else
    {
      MagnificationTypeEnum magnificationType = null;
      if(_magnification.equals(MagnificationEnum.NOMINAL))
        magnificationType = MagnificationTypeEnum.LOW;
      else if(_magnification.equals(MagnificationEnum.H_NOMINAL))
        magnificationType = MagnificationTypeEnum.HIGH;
      
      if(_selectedComponent.getBoard().isLongBoard())
      {
        TestSubProgram subProgram = null;
        if( _selectedComponent.getBoard().getRightSectionOfLongBoard().intersectsShape(shape))
          subProgram = _project.getTestProgram().getTestSubProgramHasAlignmentRegion(_selectedComponent.getBoard(), PanelLocationInSystemEnum.RIGHT, magnificationType);
        else
          subProgram = _project.getTestProgram().getTestSubProgramHasAlignmentRegion(_selectedComponent.getBoard(), PanelLocationInSystemEnum.LEFT, magnificationType);
        shape = subProgram.getCoreRuntimeAlignmentTransform().createTransformedShape(shape);
      }
      else
      {
        if (_project.getPanel().getPanelSettings().isLongPanel() && 
            _selectedComponent.getBoard().getRightSectionOfLongBoard().getMaxY() <= XrayTester.getMaxImageableAreaLengthInNanoMeters())
        {
          TestSubProgram subProgram = _project.getTestProgram().getTestSubProgramHasAlignmentRegion(_selectedComponent.getBoard(), PanelLocationInSystemEnum.LEFT, magnificationType);
          shape = subProgram.getCoreRuntimeAlignmentTransform().createTransformedShape(shape);
        }
        else
        {
          TestSubProgram subProgram = _project.getTestProgram().getTestSubProgramHasAlignmentRegion(_selectedComponent.getBoard(), PanelLocationInSystemEnum.RIGHT, magnificationType);
          shape = subProgram.getCoreRuntimeAlignmentTransform().createTransformedShape(shape);
        }
      }
    }
    
    Rectangle bounds = shape.getBounds();
    double x = bounds.getX() - _BORDER_IN_NANOMETERS;
    double y = bounds.getY() - _BORDER_IN_NANOMETERS;
    double width = bounds.getWidth() + (_BORDER_IN_NANOMETERS * 2);
    double length = bounds.getHeight() + (_BORDER_IN_NANOMETERS * 2);
    if (bounds.getWidth() > _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS)
    {
      x = bounds.getCenterX() - (_MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS / 2.0);
      width = _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS;
    }

    if (bounds.getHeight() > _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS)
    {
      y = bounds.getCenterY() - (_MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS / 2.0);
      length = _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS;
    }

    bounds.setRect(x / _magnification.getNanoMetersPerPixel(),
                   y / _magnification.getNanoMetersPerPixel(),
                   width / _magnification.getNanoMetersPerPixel(),
                   length / _magnification.getNanoMetersPerPixel());

    return bounds;
  }

  /**
   * @author George A. David
   */
  private void displayComponent()
  {
    Assert.expect(_selectedComponentType != null);
    Assert.expect(_project != null);

    _selectedComponent = null;

    if (_selectedBoard != null)
    {
      for (com.axi.v810.business.panelDesc.Component component : _selectedComponentType.getComponents())
      {
        if (component.getBoard() == _selectedBoard)
          _selectedComponent = component;
      }
    }

    if (_selectedComponent == null)
      _selectedComponent = _selectedComponentType.getComponents().get(0);

    Assert.expect(_selectedComponent != null);
    _showingTopSide = _selectedComponent.isTopSide();

    for(int selectedPadLayer : _subProgramIdToSelectedPadLayer.values())
      _graphicsEngine.removeRenderers(selectedPadLayer);
    
    _rendererCreator.clearCadRenderers(_graphicsEngine);

    _rendererCreator.setSelectedPads(_selectedComponent.getPads());
     Rectangle bounds = getMaxVisibleAlignedBoundsOfSelectedComponentInPixels();
    _graphicsEngine.fitRectangleInRendererCoordinatesToScreen(bounds.getX(),
                                                              bounds.getY(),
                                                              bounds.getWidth(),
                                                              bounds.getHeight());
    _toolBar.setSideViewButtons(_showingTopSide);

    // when we show a new component, we should clear out older selected ones
    _selectedPadRenderersForRightClick.clear();
    _graphicsEngine.clearSelectedRenderers();
  }

  /**
   * @author George A. David
   * @author Edited by - Chin Seong, Kee. to smooth out the dragging image while in adjust CAD.
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if(_isDisplayed == false)
          return;

        if (observable instanceof ProjectObservable)
        {
          if (argument instanceof ProjectChangeEvent)
          {
            ProjectChangeEventEnum eventEnum = ((ProjectChangeEvent)argument).getProjectChangeEventEnum();
            if (eventEnum instanceof ProjectEventEnum)
            {
              ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)argument;
              ProjectEventEnum projectEventEnum = (ProjectEventEnum)projectChangeEvent.getProjectChangeEventEnum();
              if (projectEventEnum.equals(ProjectEventEnum.TEST_PROGRAM_GENERATED))
              {
                // if the test program has been generated -- we need to update the verification image display to use the new data
                // the display() call will do that.  It also clears old TestProgram references so we don't have a memory leak.
                boolean checkLatestTestProgram=true;
                display(checkLatestTestProgram);
                if (_selectedComponent != null && _selectedComponentType != null)
                  displayComponent();
              }
              else if (projectEventEnum.equals(ProjectEventEnum.TEST_PROGRAM_CLEARING))
              {
                clearImagePaneRendererCreator();
              }
            }
          }
        }

        //Kee Chin Seong, - RendereCreator Change to here because when apply adjustment the 
        //                  creator will be NULL so the user will thought UI no responsive anymore once apply adjustment
        if (_areVerificationImagesValid == false || _rendererCreator == null)
          return;

        if (observable instanceof GuiObservable)
        {
          GuiEvent guiEvent = (GuiEvent)argument;
          GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
          if (guiEventEnum instanceof VerifyCadAdjustmentEventEnum)
          {
            VerifyCadAdjustmentEventEnum adjustmentEventEnum = (VerifyCadAdjustmentEventEnum)guiEventEnum;
            if (adjustmentEventEnum.equals(VerifyCadAdjustmentEventEnum.ADJUST_COMPONENT))
            {
              // Don't use assert anymore ! just cancel the component adjustment
              //By theory, when user will straight check this button only 
              //if in Off Mode / Adjust component mode, they didnt off properly the button
              //Assert.expect(_isAdjustingBoard == false);
              if (_isAdjustingBoard)
                finishAdjustingBoard(true);
              // @author Chin Seong , Kee - don't allow graphics toggle while adjusting
              // In order smoothen the dragging graphic action , we must clear the renderer and the previous graphic first.
              clearGraphics();
              _toolBar.setAllowToggleGraphics(false);
              adjustSelectedComponent();
            }
            else if (adjustmentEventEnum.equals(VerifyCadAdjustmentEventEnum.CANCEL_ADJUSTMENT) ||
                     adjustmentEventEnum.equals(VerifyCadAdjustmentEventEnum.SAVE_ADJUSTMENT))
            {
              boolean wasAdjustmentCancelled = true;
              if(adjustmentEventEnum.equals(VerifyCadAdjustmentEventEnum.SAVE_ADJUSTMENT))
                wasAdjustmentCancelled = false;

              if (_isAdjustingComponent)
                finishAdjustingSelectedComponent(wasAdjustmentCancelled);
              else if (_isAdjustingBoard)
                finishAdjustingBoard(wasAdjustmentCancelled);
              else
                Assert.expect(false);
              // re-enable graphics toggle after adjusting
              // Chin Seong, Kee - re-enable graphics toggle after adjusting
              showGraphics();
              _toolBar.setAllowToggleGraphics(true);
            }
            else if (adjustmentEventEnum.equals(VerifyCadAdjustmentEventEnum.ADJUST_BOARD_ORIGIN) ||
                    adjustmentEventEnum.equals(VerifyCadAdjustmentEventEnum.ADJUST_BOARD_SIDE))
            {
             // Assert.expect(_isAdjustingComponent == false); // Don't use assert anymore ! just cancel the component adjustment
              //By theory, when user will straight check this button only 
                //if in Off Mode / Adjust component mode, they didnt off properly the button
               if (_isAdjustingComponent)
                  finishAdjustingSelectedComponent(true); 
               
              // don't allow graphics toggle while adjusting
              _toolBar.setAllowToggleGraphics(false);
              adjustBoard();
            }
          }
          else if (guiEventEnum instanceof SelectionEventEnum)
          {
            SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
            if (selectionEventEnum.equals(SelectionEventEnum.COMPONENT_TYPE))
            {
              /** Warning "unchecked cast" approved for cast from Object type when sent from SelectedRendererObservable notifyAll.  */
              java.util.List<ComponentType> componentTypes = (java.util.List<ComponentType>)guiEvent.getSource();
              if (componentTypes.isEmpty() == false)
                _selectedComponentType = componentTypes.get(0);
              if(_selectedComponentType != null)
                displayComponent();
              
              //XCR-2270 ,to reset the CAD image in "adjust cad" tab because CAD graphics will act abnormal after apply adjustment, 
              //some image will not be displayed after select the component from the tab and sometime CAD will disappear.
              //so calling zoom will not cause any zoom effect since zoom(1) will not zoom in or out.
              _graphicsEngine.zoom(1);        
            }
            else if (selectionEventEnum.equals(SelectionEventEnum.BOARD_INSTANCE))
            {
              /** Warning "unchecked cast" approved for cast from Object type when sent from SelectedRendererObservable notifyAll.  */
              java.util.List<Board> boards = (java.util.List<Board>)guiEvent.getSource();
              if (boards.isEmpty() == false)
                _selectedBoard = boards.get(0);
            }
          }
        }
        else if (observable instanceof ProjectObservable)
        {
          if (argument instanceof ProjectChangeEvent)
          {
            ProjectChangeEventEnum eventEnum = ((ProjectChangeEvent)argument).getProjectChangeEventEnum();
            if ((eventEnum instanceof ComponentTypeEventEnum))
            {
              ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)argument;
              ComponentTypeEventEnum componentTypeEventEnum = (ComponentTypeEventEnum)eventEnum;
              // Modified by Seng Yew on 11-Jan-2013
              // XCR1559 - Able to delete multiple components at once
              // Bypass update selected if ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST
              if (componentTypeEventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST))
              {
                // If selected component is the one removed, make selection null.
                if (((ProjectChangeEvent)argument).isRemoved())
                {
                  java.util.List<ComponentType> componentsToRemoveList = (java.util.List<ComponentType>)((ProjectChangeEvent)argument).getOldValue();
                  //Ngie Xing, XCR-2174, Assert when delete multiple components of a recipe that contains verification images at once
                  if (_selectedComponent != null)
                  {
                    if (componentsToRemoveList.contains(_selectedComponent.getComponentType()))
                      _selectedComponent = null;
                  }
                }
              }
              else
              {
                //Siew Yeng - XCR-2123 & XCR-3240
                //add this checking to make sure we start update graphic engine after component update completed. 
                //
                _commandManager.waitUntilCommandEnd();
                
                if(_selectedComponent != null)
                {
                  _rendererCreator.updateComponentTypeData(_graphicsEngine, (ComponentType)projectChangeEvent.getSource(), _selectedComponent.isTopSide());

                  // since it's not null, let' s force it to update the selectedComponent
                  if(_selectedComponent.getComponentType() == (ComponentType)projectChangeEvent.getSource())
                  {
                    _rendererCreator.updateComponentData(_selectedComponent, _graphicsEngine);
                    makeSelectedComponentVisible();
                  }
                }
                else
                  _rendererCreator.updateComponentTypeData(_graphicsEngine, (ComponentType)projectChangeEvent.getSource(), true);
              }
            }
            else if ((eventEnum instanceof PackagePinEventEnum) || (eventEnum instanceof CompPackageEventEnum))
            {
              // sometimes these events are generated because of a program gen -- we might not have actually rendered anything by then
              if (_rendererCreator == null)
                return;
              _rendererCreator.updateVisibleData(_graphicsEngine, _showingTopSide);

              // the updateVisibleData actually created new renderers for all those pads, so our current list is not out-of-date
              // we'll go through our list, get each pad, find the new renderer for that pad, and then re-create our list
              // all this inorder to maintain the list of selected (white) renderers
              _graphicsEngine.clearSelectedRenderers();
              Collection<com.axi.guiUtil.Renderer> newSelectedPadRenderers = new LinkedList<com.axi.guiUtil.Renderer>();
              for (com.axi.guiUtil.Renderer renderer : _selectedPadRenderersForRightClick)
              {
                if (renderer instanceof PadRegionOfInterestRenderer)
                {
                  PadRegionOfInterestRenderer padRenderer = (PadRegionOfInterestRenderer)renderer;
                  Pad pad = padRenderer.getPad();
                  com.axi.guiUtil.Renderer newRenderer = _rendererCreator.getRendererForPad(pad); // key call here - get's the new renderer
                  newSelectedPadRenderers.add(newRenderer);
                }
              }
              if (newSelectedPadRenderers.isEmpty() == false)
              {
                _graphicsEngine.setSelectedRenderers(newSelectedPadRenderers);
                _selectedPadRenderersForRightClick.clear();
                // now, re-create the list so a subsequent right-click doesn't require the user to re-select things.
                for (com.axi.guiUtil.Renderer renderer : newSelectedPadRenderers)
                {
                  _selectedPadRenderersForRightClick.add(renderer);
                }
              }
            }
          }
        }
        else if(observable instanceof MouseClickObservable)
        {
          MouseEvent mouseEvent = (MouseEvent)argument;
          if (mouseEvent.isPopupTrigger())
          {
            handleRightClick(mouseEvent);
          }
          else
          {
            // left click -- finds the component the click was on (if any) and centers that components
//            Point2D point = _graphicsEngine.convertFromMousePixelToRendererPixel(mouseEvent.getX(), mouseEvent.getY());
//            point = _graphicsEngine.convertFromPixelToRendererCoordinates((int)mouseEvent.getX(), (int)mouseEvent.getY());
//            point.setLocation(point.getX() * MagnificationEnum.NOMINAL.getNanoMetersPerPixel(),
//                              point.getY() * MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
//            Collection<com.axi.v810.business.panelDesc.Component> components = _rendererCreator.getSurroundingComponents(point, _showingTopSide);
//            if (components.isEmpty() == false)
//            {
//              _selectedComponentType = components.iterator().next().getComponentType();
//              displayComponent();
//            }
          }
        }
        else if (observable instanceof SelectedRendererObservable)
        {
//          System.out.println("servicing a selectedRenderer event");
          /** Warning "unchecked cast" approved for cast from Object type when sent from SelectedRendererObservable notifyAll.  */
          _selectedPadRenderersForRightClick = (Collection<com.axi.guiUtil.Renderer>)argument;
          _graphicsEngine.clearSelectedRenderers();
          _graphicsEngine.setSelectedRenderers(_selectedPadRenderersForRightClick);
        }
        else
          Assert.expect(false);
      }
    });
  }

  /**
   * @author George A. David
   */
  public synchronized void updateRenderers(GraphicsEngine graphicsEngine, int layerNumber)
  {
    if(_isDisplayed == false)
      return;

    if (_areVerificationImagesValid == false)
      return;

    if (_subProgramIdToImageLayer.values().contains(layerNumber))
    {
      _rendererCreator.createVerificationImageRenderers(graphicsEngine, _showingTopSide);
    }
    else if (_subProgramIdToRefDesLayer.values().contains(layerNumber))
    {
      if (_selectedComponent == null)
        return;

      // don't redraw when adjusting the board location
      if (_isAdjustingBoard)
        return;

      _rendererCreator.createCadRenderers(graphicsEngine, _showingTopSide);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    java.util.List<Integer> layers = new ArrayList<Integer>(_layersToUseForDragging);
    _graphicsEngine.removeRenderers(layers);
    layers = new ArrayList<Integer>(_layersToUseForDragging);
    _graphicsEngine.removeRenderers(layers);

    finish();
    _selectedBoard = null;
    _selectedComponent = null;
    _selectedComponentType = null;
    _project = null;
    if (_rendererCreator != null)
      _rendererCreator.reset();
    _isDisplayed = false;
  }

  /**
   * @author George A. David 
   * @author Kee chin Seong - Clean up the adjustment board / component before leave the page
   */
  public void finish()
  {
    if (_isAdjustingComponent)
       finishAdjustingBoard(true);
    if (_isAdjustingBoard)
       finishAdjustingBoard(true);
    
    setToolBarEnabled(false);
    _guiObservable.deleteObserver(this);
    _projectObservable.deleteObserver(this);
    _mouseClickObservable.deleteObserver(this);
    
    clearImagePaneRendererCreator();
//    clearAllLayers();
    _graphicsEngine.reset();
  }

  /**
   * @author George A. David
   */
  public synchronized void calculateOffsetInNanoMeters()
  {
    Point2D offset = _graphicsEngine.getOffsetFromMarkInPixels();

    offset = _graphicsEngine.getOffsetFromMarkInRendererCoordinates();
    _xOffsetInNanoMeters = -offset.getX() * _magnification.getNanoMetersPerPixel();
    _yOffsetInNanoMeters = -offset.getY() * _magnification.getNanoMetersPerPixel();
  }

  /**
   * @author George A. David
   */
  public Point2D getOffsetRelativeToPanelInNanoMeters()
  {
    if (_isAdjustingComponent || _isAdjustingBoard)
      calculateOffsetInNanoMeters();
    return new Point2D.Double(_xOffsetInNanoMeters, _yOffsetInNanoMeters);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleRightClick(MouseEvent mouseEvent)
  {
    // handle right click operation here
    // ok, create the menu, and display at mouse coordinate

    final Collection<Pad> selectedPads = new ArrayList<Pad>();
    Set<ComponentType> componentTypes = new HashSet<ComponentType>();
    Set<JointTypeEnum> selectedJointTypes = new HashSet<JointTypeEnum>();

    JPopupMenu popupMenu = new JPopupMenu();

    // need to get what's currently selected to provide the user with good choices about joint type assignment and
    // joint orientation
    boolean somePadsHaveOrientation = false;
    for(com.axi.guiUtil.Renderer renderer : _selectedPadRenderersForRightClick)
    {
      if (renderer instanceof PadRegionOfInterestRenderer)
      {
        PadRegionOfInterestRenderer padRenderer = (PadRegionOfInterestRenderer)renderer;
        Pad pad = padRenderer.getPad();
        selectedPads.add(pad);
        componentTypes.add(pad.getComponent().getComponentType());
        selectedJointTypes.add(pad.getJointTypeEnum());
        if (pad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.NOT_APPLICABLE) == false)
          somePadsHaveOrientation = true;
      }
    }

    // if we have no components, then nothing was selected, so just return
    if (componentTypes.isEmpty())
      return;

    // make sure all pads belong to same component, or we do nothing
    if (componentTypes.size() != 1)
    {
      JMenuItem errorItem = new JMenuItem(StringLocalizer.keyToString("VERIFICATION_IMAGE_MUST_HAVE_ONLY_COMPONENT_KEY"));
      errorItem.setEnabled(false);
      popupMenu.add(errorItem);
      popupMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
      return;
    }

    // if the component selected is of joint type CAP/RES/PCAP, an orientation change is NOT allowed
    Pad aPad = (Pad)selectedPads.toArray()[0];
    JointTypeEnum jointTypeEnum = aPad.getPackagePin().getJointTypeEnum();
    boolean allowOrientationChange = true;
    if (ImageAnalysis.isSpecialTwoPinComponent(jointTypeEnum))
    {
      allowOrientationChange = false;
      LocalizedString errMsg = new LocalizedString("VERIFICATION_IMAGE_CANNOT_CHANGE_ORIENTATION_KEY", new Object[]{jointTypeEnum});
      JMenuItem errorItem = new JMenuItem(StringLocalizer.keyToString(errMsg));
      errorItem.setEnabled(false);
      popupMenu.add(errorItem);
//      popupMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
//      return;
    }

    // we need to know if all the pads are orthoganol or not to build our menus correctly.
    // if they are all orthoganol, then N/S/E/W makes sense
    // if they are all non-orthoganol, then use the NE/NW/SE/SW stuff
    // if it's a mixture, then only allow the rotational changes
    // mixture defined as both variable are true
    boolean atLeastOneOrthogonal = false ;
    boolean atLeastOneNonOrthogonal = false;

    // first pass, see if any orthogonal
    for(Pad pad : selectedPads)
    {
      PinOrientationAfterRotationEnum currentPadOrientation = pad.getPinOrientationAfterRotationEnum();
      if (currentPadOrientation.isOrthogonal())
      {
        atLeastOneOrthogonal = true;
        break;
      }
    }
    for(Pad pad : selectedPads)
    {
      PinOrientationAfterRotationEnum currentPadOrientation = pad.getPinOrientationAfterRotationEnum();
      if (currentPadOrientation.isOrthogonal() == false)
      {
        atLeastOneNonOrthogonal = true;
        break;
      }
    }

    boolean atLeastOnePadTested = false;
    boolean atLeastOnePadUntested = false;
    for(Pad pad : selectedPads)
    {
      if (pad.getPadType().isInspected())
      {
        atLeastOnePadTested = true;
        break;
      }
    }
    for(Pad pad : selectedPads)
    {
      if (pad.getPadType().isInspected() == false)
      {
        atLeastOnePadUntested = true;
        break;
      }
    }

    // state now:
    // all orthogonal pads:      atLeastOneOrthogonal=true  and atLeastOneNonOrthogonal=false
    // all non-orthogonal pads:  atLeastOneOrthogonal=false and atLeastOneNonOrthogonal=true
    // mixture:                  atLeastOneOrthogonal=true  and atLeastOneNonOrthogonal=true

    // now, let's build up our context menu -- we'll deal with orientations first, then joint type

    // orientation -- this can be tricky -- we have package pins, but we don't really have a good visual representation
    // of N/S/E/W because the user is seeing a component, which is rotated, on a board, which is also rotated, etc.
    // so, how about just allowing the user to rotation the orientation by 90, 180, 270 degrees.
//    VERIFICATION_IMAGE_ROTATE_ORIENTATION_KEY
    LocalizedString rotate90Msg = new LocalizedString("VERIFICATION_IMAGE_ROTATE_ORIENTATION_KEY", new Object[]{new Integer(90)});
    JMenuItem rotate90Item = new JMenuItem(StringLocalizer.keyToString(rotate90Msg));
    rotate90Item.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        rotateByDegrees(selectedPads, 90);
      }
    });

    LocalizedString rotate180Msg = new LocalizedString("VERIFICATION_IMAGE_ROTATE_ORIENTATION_KEY", new Object[]{new Integer(180)});
    JMenuItem rotate180Item = new JMenuItem(StringLocalizer.keyToString(rotate180Msg));
    rotate180Item.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        rotateByDegrees(selectedPads, 180);
      }
    });

    LocalizedString rotate270Msg = new LocalizedString("VERIFICATION_IMAGE_ROTATE_ORIENTATION_KEY", new Object[]{new Integer(270)});
    JMenuItem rotate270Item = new JMenuItem(StringLocalizer.keyToString(rotate270Msg));
    rotate270Item.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        rotateByDegrees(selectedPads, 270);
      }
    });

    JMenuItem infoItem = new JMenuItem(StringLocalizer.keyToString("PrintInfo"));
    infoItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        displayInfo(selectedPads);
      }
    });

    if (somePadsHaveOrientation && allowOrientationChange)
    {
      popupMenu.add(rotate90Item);
      popupMenu.add(rotate180Item);
      popupMenu.add(rotate270Item);
    }

//    popupMenu.addSeparator();
//    popupMenu.add(infoItem);
//    popupMenu.addSeparator();


    if ((atLeastOneOrthogonal) && (atLeastOneNonOrthogonal == false))
    {
      LocalizedString northMsg = new LocalizedString("VERIFICATION_IMAGE_CHANGE_ORIENTATION_KEY",
                                                     new Object[]{PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN});
      JMenuItem setNorthItem = new JMenuItem(StringLocalizer.keyToString(northMsg));
      setNorthItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setPadsToAbsoluteOrientation(selectedPads, PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN);
        }
      });

      LocalizedString southMsg = new LocalizedString("VERIFICATION_IMAGE_CHANGE_ORIENTATION_KEY",
                                                     new Object[]{PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN});
      JMenuItem setSouthItem = new JMenuItem(StringLocalizer.keyToString(southMsg));
      setSouthItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setPadsToAbsoluteOrientation(selectedPads, PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN);
        }
      });

      LocalizedString eastMsg = new LocalizedString("VERIFICATION_IMAGE_CHANGE_ORIENTATION_KEY",
                                                     new Object[]{PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN});
      JMenuItem setEastItem = new JMenuItem(StringLocalizer.keyToString(eastMsg));
      setEastItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setPadsToAbsoluteOrientation(selectedPads, PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN);
        }
      });

      LocalizedString westMsg = new LocalizedString("VERIFICATION_IMAGE_CHANGE_ORIENTATION_KEY",
                                                     new Object[]{PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN});
      JMenuItem setWestItem = new JMenuItem(StringLocalizer.keyToString(westMsg));
      setWestItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setPadsToAbsoluteOrientation(selectedPads, PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN);
        }
      });
      if (somePadsHaveOrientation && allowOrientationChange)
      {
        popupMenu.addSeparator();
        popupMenu.add(setNorthItem);
        popupMenu.add(setSouthItem);
        popupMenu.add(setEastItem);
        popupMenu.add(setWestItem);
      }
    }

    // now, create the diagonal compass directions items (if needed)
    // these are NE/SE/SW/NW
    if ((atLeastOneOrthogonal == false) && (atLeastOneNonOrthogonal))
    {
      LocalizedString neMsg = new LocalizedString("VERIFICATION_IMAGE_CHANGE_ORIENTATION_KEY",
                                                     new Object[]{PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_EAST_OF_PIN});
      JMenuItem setNorthEastItem = new JMenuItem(StringLocalizer.keyToString(neMsg));
      setNorthEastItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setPadsToAbsoluteOrientation(selectedPads, PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_EAST_OF_PIN);
        }
      });

      LocalizedString seMsg = new LocalizedString("VERIFICATION_IMAGE_CHANGE_ORIENTATION_KEY",
                                                     new Object[]{PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_EAST_OF_PIN});
      JMenuItem setSouthEastItem = new JMenuItem(StringLocalizer.keyToString(seMsg));
      setSouthEastItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setPadsToAbsoluteOrientation(selectedPads, PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_EAST_OF_PIN);
        }
      });

      LocalizedString swMsg = new LocalizedString("VERIFICATION_IMAGE_CHANGE_ORIENTATION_KEY",
                                                     new Object[]{PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_WEST_OF_PIN});
      JMenuItem setSouthWestItem = new JMenuItem(StringLocalizer.keyToString(swMsg));
      setSouthWestItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setPadsToAbsoluteOrientation(selectedPads, PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_WEST_OF_PIN);
        }
      });

      LocalizedString nwMsg = new LocalizedString("VERIFICATION_IMAGE_CHANGE_ORIENTATION_KEY",
                                                     new Object[]{PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_WEST_OF_PIN});
      JMenuItem setNorthWestItem = new JMenuItem(StringLocalizer.keyToString(nwMsg));
      setNorthWestItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setPadsToAbsoluteOrientation(selectedPads, PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_WEST_OF_PIN);
        }
      });
      if (somePadsHaveOrientation)
      {
        popupMenu.addSeparator();
        popupMenu.add(setNorthEastItem);
        popupMenu.add(setSouthEastItem);
        popupMenu.add(setSouthWestItem);
        popupMenu.add(setNorthWestItem);
      }
    }

    // add in the joint type choices
    // rules:  pad level changes -- assume that cap/pcap/res are not allowed -- can't change from or to these joint types

    // do the single selection first - easier if we get this out of the way
    java.util.List<JointTypeEnum> supportedJointTypes = JointTypeEnum.getAllJointTypeEnums();
    //    Collections.sort(supportedJointTypes, new JointTypeEnumComparator(true));

    // if there is at least one non-throughhole joint in the list, then don't allow the throughhole joint types
    // to be selected
    boolean atLeastOneSurfaceMount = false;    
    for(Pad  pad : selectedPads)
    {
      if (pad.getLandPatternPad().isThroughHolePad() == false)
      {
        atLeastOneSurfaceMount = true;
        break;
      }
    }

    JMenu jointTypesSubMenu = new JMenu(StringLocalizer.keyToString("ATGUI_FAMILY_KEY"));
    for (final JointTypeEnum jte : supportedJointTypes)
    {
      // skip all special two pin components Cap/Res/PCap
//      if (ImageAnalysis.isSpecialTwoPinComponent(jte))
//        continue;

      JCheckBoxMenuItem jointTypeMenuItem = new JCheckBoxMenuItem(jte.getName());
      if (selectedJointTypes.contains(jte))
        jointTypeMenuItem.setSelected(true);

      // don't allow throughhole joint types if there are surface mount pads in list
      if (atLeastOneSurfaceMount && ImageAnalysis.requiresThroughHoleLandPatternPad(jte))
        jointTypeMenuItem.setEnabled(false);

      // special case -- if the joint type is a special two pin device, and BOTH pins have been selected, allow the joint type change
      if (ImageAnalysis.isSpecialTwoPinComponent(jte))
      {
        if (selectedPads.size() != 2)
          jointTypeMenuItem.setEnabled(false);  
        //XCR2370 - Khaw Chek Hau: Disable the option of changing to Special Two Pin Component joint type, If the component total pad size not equal to 2
        else
        {
          for(Pad pad : selectedPads)
          {
            if (pad.getComponent().getPads().size() != 2)
              jointTypeMenuItem.setEnabled(false);  
          }
        }
      }

      jointTypeMenuItem.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          setPadsToJointType(selectedPads, jte);
        }
      });
      jointTypesSubMenu.add(jointTypeMenuItem);

    }
    JCheckBoxMenuItem noTestMenuItem = new JCheckBoxMenuItem(StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOTEST_KEY"));
    noTestMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setPadsToTestState(selectedPads, false);
      }
    });
    JCheckBoxMenuItem testMenuItem = new JCheckBoxMenuItem(StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_RESTORE_KEY"));
    testMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setPadsToTestState(selectedPads, true);
      }
    });

    jointTypesSubMenu.addSeparator();
    jointTypesSubMenu.add(noTestMenuItem);
    jointTypesSubMenu.add(testMenuItem);
    if ((allowOrientationChange == false) && (selectedPads.size() != 2))  // if both are selected, allow the test/notest
    {
      noTestMenuItem.setEnabled(false);
      testMenuItem.setEnabled(false);
    }
    if (atLeastOnePadUntested)
      noTestMenuItem.setSelected(true);
    if (atLeastOnePadTested)
      testMenuItem.setSelected(true);
    
    // final sanity check -- do not allow a single pin for a cap/res/pcap to have it's joint type changed
    if (selectedPads.size() == 1)
    {
      Pad pad = (Pad)selectedPads.toArray()[0];
      JointTypeEnum jte = pad.getJointTypeEnum();
      if (ImageAnalysis.isSpecialTwoPinComponent(jte))
      {
        LocalizedString errMsg = new LocalizedString("VERIFICATION_IMAGE_CANNOT_CHANGE_JOINT_TYPE_KEY", new Object[]{jte});
        //VERIFICATION_IMAGE_CANNOT_CHANGE_JOINT_TYPE_KEY
        jointTypesSubMenu.setText(StringLocalizer.keyToString(errMsg));
        jointTypesSubMenu.setEnabled(false);
      }
    }

    if (popupMenu.getComponentCount() != 0)
      popupMenu.addSeparator();
    popupMenu.add(jointTypesSubMenu);

    popupMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
  }

  /**
   * @author Andy Mechtenberg
   */
  private void rotateByDegrees(Collection<Pad> pads, int degrees)
  {
    _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PACKAGE_PIN_SET_ORIENTATION_KEY"));
    // order of the pins doesn't matter -- change each from it's current to it's opposite.
    for(Pad pad : pads)
    {
      PackagePin packagePin = pad.getPackagePin();
      PinOrientationAfterRotationEnum currentPackagePinOrientation = packagePin.getPinOrientationAfterRotationEnum();

      // if orientation is N/A, then skip this pad
      if (currentPackagePinOrientation.equals(PinOrientationAfterRotationEnum.NOT_APPLICABLE))
        continue;

      PinOrientationAfterRotationEnum newPackagePinOrientation = null;
      // on the bottom side of the board, we need to rotate in the other direction

      //Khaw Chek Hau - XCR2909 : Pad orientation changed to wrong direction in Adjust CAD
      if (pad.isTopSide())
        degrees = -degrees;
      int packagePinDegrees = currentPackagePinOrientation.getDegrees();
      packagePinDegrees = packagePinDegrees + degrees;
      newPackagePinOrientation = PinOrientationAfterRotationEnum.getPinOrientationAfterRotationEnumFromDegrees(packagePinDegrees);
      try
      {
        _commandManager.execute(new PackagePinSetOrientationCommand(packagePin, newPackagePinOrientation));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
      }
    }
    _commandManager.endCommandBlock();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void displayInfo(Collection<Pad> pads)
  {
    for(Pad pad : pads)
    {
      PackagePin packagePin = pad.getPackagePin();
      PinOrientationAfterRotationEnum padOrientation = pad.getPinOrientationAfterRotationEnum();
      PinOrientationAfterRotationEnum packagePinOrientation = packagePin.getPinOrientationAfterRotationEnum();

      System.out.println("padOrientation: " + padOrientation);
      System.out.println("packagePinOrientation: " + packagePinOrientation);
      System.out.println("Joint Type: " + pad.getJointTypeEnum());
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setPadsToAbsoluteOrientation(Collection<Pad> pads, PinOrientationAfterRotationEnum targetOrientation)
  {
    _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PACKAGE_PIN_SET_ORIENTATION_KEY"));
    // order of the pins doesn't matter -- change each from it's current to it's opposite.
    for(Pad pad : pads)
    {
      PackagePin packagePin = pad.getPackagePin();
      PinOrientationAfterRotationEnum padOrientation = pad.getPinOrientationAfterRotationEnum();
      PinOrientationAfterRotationEnum packagePinOrientation = packagePin.getPinOrientationAfterRotationEnum();

      // if orientation is N/A, then skip this pad
      if (packagePinOrientation.equals(PinOrientationAfterRotationEnum.NOT_APPLICABLE))
        continue;
//
//      System.out.println("padOrientation(before): " + padOrientation);
//      System.out.println("packagePinOrientation(before): " + packagePinOrientation);
      int degreesOffFromTarget = 0;
      PinOrientationAfterRotationEnum newOrientation = null;
      if (targetOrientation.isOrthogonal())
      {
        newOrientation = PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN;

        //  Take the visual (pad) orientation, and rotate it by whatever is needed to make it the target orientation
        // then rotate the package pin orientation by the same amount
        // this is tricky because the orientation is stored as an enum, not a rotation number

        // do initial calculation based off of NORTH, then we'll adjust later to the real target orientation
        if (padOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN))
          degreesOffFromTarget = 0; // we're already at north, so no change
        else if (padOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN))
          degreesOffFromTarget = 180; // south to north is 180 off
        else if (padOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN))
          degreesOffFromTarget = 90; // east to north, clockwise is 90
        else if (padOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN))
          degreesOffFromTarget = 270; // west to north, clockwise is 270

        if (targetOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN))
          degreesOffFromTarget += 0;
        else if (targetOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN))
          degreesOffFromTarget += 180; // south to north is 180 off
        else if (targetOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN))
          degreesOffFromTarget += 270; // east to north, clockwise is 90
        else if (targetOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN))
          degreesOffFromTarget += 90; // west to north, clockwise is 270
      }
      else
      {
        newOrientation = PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_EAST_OF_PIN;
        //  Take the visual (pad) orientation, and rotate it by whatever is needed to make it the target orientation
        // then rotate the package pin orientation by the same amount
        // this is tricky because the orientation is stored as an enum, not a rotation number

        // do initial calculation based off of NORTH EAST, then we'll adjust later to the real target orientation
        if (padOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_EAST_OF_PIN))
          degreesOffFromTarget = 0;
        else if (padOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_EAST_OF_PIN))
          degreesOffFromTarget = 90;
        else if (padOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_WEST_OF_PIN))
          degreesOffFromTarget = 180;
        else if (padOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_WEST_OF_PIN))
          degreesOffFromTarget = 270;

        if (targetOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_EAST_OF_PIN))
          degreesOffFromTarget += 0;
        else if (targetOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_EAST_OF_PIN))
          degreesOffFromTarget += 270;
        else if (targetOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_WEST_OF_PIN))
          degreesOffFromTarget += 180;
        else if (targetOrientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_WEST_OF_PIN))
          degreesOffFromTarget += 90;
      }

      //Khaw Chek Hau - XCR2909 : Pad orientation changed to wrong direction in Adjust CAD
      if (pad.isBottomSide())
      {
        degreesOffFromTarget = -degreesOffFromTarget;
      }

      newOrientation = packagePinOrientation.getPinOrientationAfterRotationEnum(degreesOffFromTarget);
      try
      {
        _commandManager.execute(new PackagePinSetOrientationCommand(packagePin, newOrientation));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
      }
    }
    _commandManager.endCommandBlock();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setPadsToJointType(Collection<Pad> pads, JointTypeEnum jointTypeEnum)
  {

    if (ImageAnalysis.isSpecialTwoPinComponent(jointTypeEnum))
    {
      // in the special case of Cap/Res/PCap, don't set each pad -- just change the package's joint type
      try
      {
        Pad pad = (Pad)pads.toArray()[0];  // this is safe, as we wouldn't get this far unless we had only 2 entries in list
        _commandManager.execute(new CompPackageSetJointTypeCommand(pad.getPackagePin().getCompPackage(), jointTypeEnum, true));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
      }
      return;
    }
    
    // if the collection of pads is complete, that is, all pads for the component are being set at the same time,
    // let's take shortcut and just set the package instead of iterating over all the pads
    // this is pretty necessary for setting some res/cap/pcap to some non-2-pin joint type (like gullwing)
    // the above case won't handle it, as thats for setting *to* res/cap/pcap, instead of setting *from* res/cap/pcap
    Pad firstPad = (Pad)pads.toArray()[0];
    CompPackage compPackage = firstPad.getPackagePin().getCompPackage();
    com.axi.v810.business.panelDesc.Component component = firstPad.getComponent();
    boolean allPadsPresent = true;
    for(Pad compPad : component.getPads())
    {
      if (pads.contains(compPad) == false)
        allPadsPresent = false;      
    }
    if (allPadsPresent && compPackage.usesOneJointTypeEnum())
    {
      try
      {
        Pad pad = (Pad) pads.toArray()[0];  // this is safe, as we wouldn't get this far unless we had only 2 entries in list        
        _commandManager.execute(new CompPackageSetJointTypeCommand(compPackage, jointTypeEnum, true));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
      }
      return;
    }
    else
    {
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PACKAGE_PIN_SET_JOINT_TYPE_KEY"));
      // order of the pins doesn't matter -- change each from it's current to it's opposite.
      for (Pad pad : pads)
      {
        PackagePin packagePin = pad.getPackagePin();

        try
        {
          _commandManager.execute(new PackagePinSetJointTypeCommand(packagePin, jointTypeEnum));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  ex.getLocalizedMessage(),
                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                  true);
        }
      }
      _commandManager.endCommandBlock();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private PadType getPadType(ComponentType componentType, PadType basePadType)
  {
    String padName = basePadType.getName();
    for (PadType componentPadType : componentType.getPadTypes())
    {
      if (componentPadType.getName().equalsIgnoreCase(padName))
        return componentPadType;
    }
    Assert.expect(false, "Unable to find pad type for : " + componentType + " " + basePadType);
    return null;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setPadsToTestState(Collection<Pad> pads, boolean toTest)
  {
    _commandManager.beginCommandBlock(StringLocalizer.keyToString("MMGUI_NO_TEST_GROUP_UNDO_KEY"));
    // order of the pins doesn't matter -- change each from it's current to it's opposite.
    for(Pad pad : pads)
    {
      PadType basePadType = pad.getPadType();
      PackagePin packagePin = pad.getPackagePin();
      CompPackage compPackage = packagePin.getCompPackage();
      java.util.List<ComponentType> componentTypes = compPackage.getComponentTypes();
      for (ComponentType componentType : componentTypes)
      {
        PadType padType = getPadType(componentType, basePadType);
        try
        {
          _commandManager.execute(new PadTypeSetTestedCommand(padType, toTest));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
        }

      }
    }
    _commandManager.endCommandBlock();

  }


  /**
   * @author George A. David
   */
  public void zoomIn()
  {
    _graphicsEngine.zoom(_ZOOM_FACTOR);
  }

  /**
   * @author George A. David
   */
  public void zoomOut()
  {
    _graphicsEngine.zoom(1.0 / _ZOOM_FACTOR);
  }

  /**
   * @author George A. David
   */
  public void setZoomRectangleMode(boolean zoomRectangleMode)
  {
    if (zoomRectangleMode)
    {
      _graphicsEngine.setDragGraphicsMode(false);
      _graphicsEngine.setMeasurementMode(false);
      _graphicsEngine.setZoomRectangleMode(true);
    }
    else
    {
      _graphicsEngine.setZoomRectangleMode(false);
      _graphicsEngine.setMouseEventsEnabled(true);
    }
  }

  /**
   * @author George A. David
   */
  public void setDragGraphicsMode(boolean dragGraphicsMode)
  {
    if(dragGraphicsMode)
    {
      _graphicsEngine.setMeasurementMode(false);
      _graphicsEngine.setZoomRectangleMode(false);
      _graphicsEngine.setDragGraphicsMode(true);
      _graphicsEngine.clearSelectedRenderers();
      _toolBar.setDragToggleButtonSelected(true);
    }
    else
    {
      _graphicsEngine.setDragGraphicsMode(false);
      _graphicsEngine.setMouseEventsEnabled(true);
      if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment() == false)
        // set group select mode has to be set AFTER enabling the mouse events.
        _graphicsEngine.setGroupSelectMode(true);
      _toolBar.setDragToggleButtonSelected(false);
    }
  }

  /**
   * @author George A. David
   */
  public void setMeasurmentMode(boolean measurementMode)
  {
    if (measurementMode)
    {
      _graphicsEngine.setDragGraphicsMode(false);
      _graphicsEngine.setZoomRectangleMode(false);
      _graphicsEngine.setMeasurementMode(true);
    }
    else
    {
      _graphicsEngine.setMeasurementMode(false);
      _graphicsEngine.setMouseEventsEnabled(true);
    }
  }

  /**
   * @author Seng-Yew Lim
   */
  public void setImageStayPersistent(boolean isImageStayPersistent)
  {
  }

  /**
   * @author Anthony Fong //Anthony01005
   */
  public void setImageFitToScreen(boolean isImageFitToScreen)
  {
    // do nothing
  }
  /**
   * @author Anthony Fong //Anthony01005
   */
  public void setZoomFactor(double zoomFactor)
  {
    // do nothing
  }
  
  /**
   * @author George A. David
   */
  public void showTopSide()
  {
    _showingTopSide = true;
    updateRenderers(_graphicsEngine, _subProgramIdToImageLayer.values().iterator().next());
    updateRenderers(_graphicsEngine, _subProgramIdToRefDesLayer.values().iterator().next());
  }

  /**
   * @author George A. David
   */
  public void showBottomSide()
  {
    _showingTopSide = false;
    updateRenderers(_graphicsEngine, _subProgramIdToImageLayer.values().iterator().next());
    updateRenderers(_graphicsEngine, _subProgramIdToRefDesLayer.values().iterator().next());
  }

  /**
   * @author George A. David
   */
  public void resetGraphics()
  {
    Assert.expect(false);
  }

  /**
   * @author George A. David
   */
  public void clearGraphics()
  {
    _rendererCreator.setShowCadGraphics(false);
    _rendererCreator.clearCadRenderers(_graphicsEngine);
  }

  /**
   * @author George A. David
   */
  public void showGraphics()
  {
    _rendererCreator.setShowCadGraphics(true);
    _rendererCreator.createCadRenderers(_graphicsEngine, _showingTopSide);
  }

  /**
   * @author Scott Richardson
   */
  public void translateUpInZAxis()
  {
    Assert.expect(false, "translateUpInZAxis is not implemented here");
  }

  /**
   * @author Scott Richardson
   */
  public void translateDownInZAxis()
  {
    Assert.expect(false, "translateUpInZAxis is not implemented here");
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setGroupSelectMode(boolean isGroupSelectMode)
  {
    Assert.expect(false, "translateUpInZAxis is not implemented here");
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setSelectRegionMode(boolean isSelectRegionMode)
  {
    Assert.expect(false, "translateUpInZAxis is not implemented here");
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setSelectRegionMode(boolean isSelectRegionMode, double maxImageRegionWidth, double maxImageRegionHeight)
  {
    Assert.expect(false, "translateUpInZAxis is not implemented here");
  }

  /**
   * @author Chong, Wei Chin
   */
  private void clearAllLayers()
  {
    java.util.List<Integer> allLayers = getAllLayers();
    for(Integer layerNumber : allLayers)
    {
      _graphicsEngine.removeLayer(layerNumber.intValue());
//      _graphicsEngine.validate();
    }
    _subProgramIdToImageLayer.clear();
    _subProgramIdToPadLayer.clear();
    _subProgramIdToRefDesLayer.clear();
    _subProgramIdToPadNameLayer.clear();
    _subProgramIdToOrientationArrowLayer.clear();
    _subProgramIdToSelectedPadLayer.clear();
    _subProgramIdToSelectedRefDesLayer.clear();
    _subProgramIdToSelectedPadNameLayer.clear();
    _subProgramIdToSelectedOrientationArrowLayer.clear();
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  private java.util.List<Integer> getAllLayers()
  {
    java.util.List <Integer> allLayers = new ArrayList<Integer>();
    allLayers.addAll(_subProgramIdToImageLayer.values());
    allLayers.addAll(_subProgramIdToPadLayer.values());
    allLayers.addAll(_subProgramIdToRefDesLayer.values());
    allLayers.addAll(_subProgramIdToPadNameLayer.values());
    allLayers.addAll(_subProgramIdToOrientationArrowLayer.values());
    allLayers.addAll(_subProgramIdToSelectedPadLayer.values());
    allLayers.addAll(_subProgramIdToSelectedRefDesLayer.values());
    allLayers.addAll(_subProgramIdToSelectedPadNameLayer.values());
    allLayers.addAll(_subProgramIdToSelectedOrientationArrowLayer.values());
    return allLayers;
  }
}
