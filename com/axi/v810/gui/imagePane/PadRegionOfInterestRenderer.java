package com.axi.v810.gui.imagePane;

import java.awt.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This class draws a Pad Region of Interest
 *
 * @author Laura Cormos
 */
public class PadRegionOfInterestRenderer extends RegionOfInterestRenderer
{
  private Pad _pad;

  /**
   * @author Laura Cormos
   */
  public PadRegionOfInterestRenderer(Shape shape, Pad pad)
  {
    super(shape);
    Assert.expect(shape != null);
    Assert.expect(pad != null);

    _pad = pad;
    refreshData();
  }

  /**
   * @author Laura Cormos
   */
  public PadRegionOfInterestRenderer(Shape shape, int strokeSize, Pad pad)
  {
    super(shape, strokeSize);
    Assert.expect(shape != null);
    Assert.expect(strokeSize > 0);
    Assert.expect(pad != null);

    _pad = pad;
    refreshData();
  }

  /**
   * @author Laura Cormos
   */
  public Pad getPad()
  {
    Assert.expect(_pad != null);
    return _pad;
  }

  /**
   * @author Laura Cormos
   */
  public String toString()
  {
    return "Region of Interest renderer for pad: " + _pad.getBoardAndComponentAndPadName();
  }
}
