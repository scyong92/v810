package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.geom.*;
import java.awt.image.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.MagnificationTypeEnum;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * This class draws an verification image
 * @author George A. David
 */
public class CadCreatorVerificationImageRenderer extends BufferedImageRenderer
{
  private ImageManager _imageManager = ImageManager.getInstance();
  private ReconstructionRegion _verificationRegion;
  private Point2D _pixelCoordinates;

  /**
   * @author George A. David
   */
  public CadCreatorVerificationImageRenderer(BufferedImage bufferedImage, com.axi.v810.business.panelDesc.Panel panel)
  {
  
    _bufferedImage = bufferedImage;
    _pixelCoordinates = new Point2D.Double(panel.getCadOriginInNanometers().getX() / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel(),
                                          panel.getCadOriginInNanometers().getY() / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
    //refreshData();
  }

  /**
   * @author George A. David
   */
  protected void displayAlternateImage()
  {
    // ok, we couldn't load the image
    // create an image with an error
    ImageRectangle rect = _verificationRegion.getRegionRectangleInPixels();
    _bufferedImage = new BufferedImage(rect.getWidth(), rect.getHeight(), BufferedImage.TYPE_INT_RGB);
    Graphics2D graphics = _bufferedImage.createGraphics();
    graphics.setColor(Color.black);
    graphics.fillRect(0, 0, rect.getWidth(), rect.getHeight());
    graphics.setColor(Color.white);
    graphics.setFont(FontUtil.getRendererFont(20));
    float border = rect.getWidth() * 0.10f;
    graphics.drawString(StringLocalizer.keyToString("VISGUI_ERROR_LOADING_IMAGE_KEY"), border, rect.getHeight() / 2.0f);
  }
  
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;
    Color origColor = graphics2D.getColor();
    graphics2D.setColor(getForeground());
    Stroke origStroke = graphics2D.getStroke();
    if(hasStroke())
      graphics2D.setStroke(new BasicStroke(2));
    graphics2D.setColor(origColor);
    graphics2D.setStroke(origStroke);
    
    // bee-hoon.goh
    BufferedImage resizePanelImage = null;
    Graphics2D resizePanelGraphic = null;
    if(_bufferedImage != null)
    {
      double scaleWidth = _bufferedImage.getWidth() / _widthInPixels;
      double scaleHeight = _bufferedImage.getHeight() / _heightInPixels;
      int resizeImageWidthInPixels = (int) Math.floor(_bufferedImage.getWidth() / scaleWidth);
      int resizeImageHeightInPixels = (int) Math.floor(_bufferedImage.getHeight() / scaleHeight);
      resizePanelImage = new BufferedImage(resizeImageWidthInPixels, resizeImageHeightInPixels, BufferedImage.TYPE_BYTE_GRAY);
      resizePanelGraphic = resizePanelImage.createGraphics();
      resizePanelGraphic.drawImage(_bufferedImage, 0, 0, resizeImageWidthInPixels, resizeImageHeightInPixels, null);
      resizePanelGraphic.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
      resizePanelGraphic.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);    
      graphics2D.drawImage(resizePanelImage, null, 0, 0);
    }
  }

  /**
   * @author George A. David
   */
  protected void refreshData()
  {
    initializeImageWithTranslation(_bufferedImage, _pixelCoordinates.getX(), _pixelCoordinates.getY());
  }
}
