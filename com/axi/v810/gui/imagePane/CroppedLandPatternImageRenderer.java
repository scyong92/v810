package com.axi.v810.gui.imagePane;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.hardware.MagnificationEnum;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.awt.image.*;

/**
 * This class is responsible for drawing the cropped land pattern
 *
 * @author Chin Seong, Kee
 */
public class CroppedLandPatternImageRenderer extends BufferedImageRenderer
{
  private Point2D _pixelCoordinates;

  /**
   * @author Chin Seong, Kee
   */
  public CroppedLandPatternImageRenderer(Renderer renderer, BufferedImage img)
  {
    Assert.expect(renderer != null);
    Assert.expect(img != null);

    _bufferedImage = img;
    _pixelCoordinates = new Point2D.Double(renderer.getBounds().getMinX() / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel(),
                                           renderer.getBounds().getMinY() / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
    
    refreshData();
  }

  /**
   * @author Chin Seong, Kee
   */
  protected void refreshData()
  {
    initializeImageWithTranslation(_bufferedImage, _pixelCoordinates.getX(), _pixelCoordinates.getY());
  }
}
