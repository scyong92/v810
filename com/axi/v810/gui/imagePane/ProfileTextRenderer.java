package com.axi.v810.gui.imagePane;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * This class draws a Region of Interest
 *
 * @author George A. David
 */
public class ProfileTextRenderer extends TextRenderer
{
  private String _text;
  private double _xCoordinate;
  private double _yCoordinate;

  /**
   * @author George A. David
   */
  ProfileTextRenderer(String text, double xCoord, double yCoord)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(text != null);

    _text = text;
    _xCoordinate = xCoord;
    _yCoordinate = yCoord;

    setFont(FontUtil.getBoldFont(getFont(),Localization.getLocale()));
    refreshData();
  }

  /**
   * @author George A. David
   */
  public void adjustXcoordinate(double xAdjustment)
  {
    _xCoordinate += xAdjustment;
    refreshData();
  }

  /**
   * @author George A. David
   */
  public String toString()
  {
    return "profile text";
  }

  /**
   * @author George A. David
   */
  protected void refreshData()
  {
    Assert.expect(_text != null);

    initializeText(_text, _xCoordinate, _yCoordinate, false);
  }
}
