package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * @author Erica Wheatcroft
 */
public class ImageManagerToolBar extends JToolBar implements Observer
{
  private JToggleButton _showSelectImageSetPanelButton = new JToggleButton();
  //private JButton _showSelectImageSetPanelButton = new JButton();
  private JButton _showGenerateImageSetWizardButton = new JButton();

  private boolean _tuningScreensVisible = false;
  // algo tuner may be looping on an inspection; to prevent button flashing,
  // don't enable buttons on INSPECTION_COMPLETED until algo tuner says it's OK
  private boolean _algoTunerRunMode = false;

  private ImageManagerPanel _parent = null;
  private Project _project = null;

  private static double _STANDARD_ZOOM = 2.0;

  protected ImagePanelGraphicsEngineSetupInt _graphicsEngineSetup;
  private com.axi.v810.business.panelDesc.Panel _panel;

  private ImageIcon _viewTopSideImage;
  private ImageIcon _viewBottomSideImage;
  private ImageIcon _zoomInImage;
  private ImageIcon _zoomOutImage;
  private ImageIcon _zoomRectangleImage;
  private ImageIcon _zoomResetImage;
  private ImageIcon _dragImage;

  protected JButton _zoomInButton = new JButton();
  protected JButton _zoomOutButton = new JButton();
  protected JToggleButton _zoomRectangleToggleButton = new JToggleButton();
  protected JToggleButton _dragToggleButton = new JToggleButton();
  protected JToggleButton _measureToggleButton = new JToggleButton();
  protected JButton _resetGraphicsButton = new JButton();
  protected JToggleButton _toggleGraphicsToggleButton = new JToggleButton();
  private JToggleButton _topSideToggleButton = new JToggleButton();
  private JToggleButton _bottomSideToggleButton = new JToggleButton();
  protected JToggleButton _toggleImageStayToggleButton = new JToggleButton();
  //Anthony01005
  protected JToggleButton _toggleImageFitToScreenButton = new JToggleButton();
  protected JCheckBox _ignoreImageCompatibleCheckButton = new JCheckBox();

  private ButtonGroup _viewSidesButtonGroup = new ButtonGroup();
  private boolean _prevCheckLatestTestProgram;
  //Khaw Chek Hau - XCR2285: 2D Alignment on v810
  private JToggleButton _selectRegionToggleButton = new JToggleButton();
  private AlignmentImageGraphicsEngineSetup _alignmentImageGraphicsEngineSetup = null;
  private GraphicsEngine _graphicsEngine = null;
  private transient double _maximumImagePanelSelectRegionSizeInPixel = 350;

  /**
   * @author Erica Wheatcroft
   */
  public ImageManagerToolBar(ImageManagerPanel parent)
  {
    Assert.expect(parent != null, "parent is not null");

    _parent = parent;
    jbInit();
    GuiObservable.getInstance().addObserver(this);
    ProjectObservable.getInstance().addObserver(this);
    InspectionEventObservable.getInstance().addObserver(this);
  }

  /**
   * @author George A. David
   */
  public void setImagePanelGraphicsEngineSetup(ImagePanelGraphicsEngineSetupInt graphicsEngineSetup)
  {
    Assert.expect(graphicsEngineSetup != null);
    _graphicsEngineSetup = graphicsEngineSetup;
  }

  /**
   * @author George A. David
   */
  public void setAllowZoomIn(boolean allowZoomIn)
  {
    _zoomInButton.setEnabled(allowZoomIn);
  }

  /**
   * @author George A. David
   */
  public void setAllowZoomOut(boolean allowZoomOut)
  {
    _zoomOutButton.setEnabled(allowZoomOut);
  }

  /**
   * @author George A. David
   */
  public void setAllowZoomRectangle(boolean allowZoomRectangle)
  {
    _zoomRectangleToggleButton.setEnabled(allowZoomRectangle);
    if (allowZoomRectangle == false)
      _zoomRectangleToggleButton.setSelected(false);
  }

  /**
   * @author George A. David
   */
  public void setAllowDragGraphics(boolean allowDragGraphics)
  {
    _dragToggleButton.setEnabled(allowDragGraphics);
    if (allowDragGraphics == false)
      _dragToggleButton.setSelected(false);
  }

  /**
   * @author George A. David
   */
  public void setAllowMeasurements(boolean allowMeasurements)
  {
    _measureToggleButton.setEnabled(allowMeasurements);
  }

  /**
   * @author George A. David
   */
  public void setAllowResetGraphics(boolean allowResetGraphics)
  {
    _resetGraphicsButton.setEnabled(allowResetGraphics);
    if (allowResetGraphics == false)
      _resetGraphicsButton.setSelected(false);
  }

  /**
   * @author George A. David
   */
  public void setAllowViewTopSide(boolean allowViewTopSide)
  {
    _topSideToggleButton.setEnabled(allowViewTopSide);
  }

  /**
   * @author George A. David
   */
  public void setAllowViewBottomSide(boolean allowViewBottomSide)
  {
    _bottomSideToggleButton.setEnabled(allowViewBottomSide);
  }

  /**
   * @author George A. David
   */
  public void setAllowToggleGraphics(boolean allowToggleGraphics)
  {
    _toggleGraphicsToggleButton.setEnabled(allowToggleGraphics);
  }

  /**
   * @author Seng-Yew Lim
   */
  public void setAllowToggleImageStay(boolean allowToggleImageStay)
  {
    _toggleImageStayToggleButton.setEnabled(allowToggleImageStay);
  }

  /**
   * @author Anthony Fong //Anthony01005
   */
  public void setAllowToggleImageFitToScreen(boolean allowToggleImageFitToScreen)
  {
    _toggleImageFitToScreenButton.setEnabled(allowToggleImageFitToScreen);
  }

  /**
   * @author Scott Richardson
   * enableButtons() provides the default logic to enable/disable this button
   */
  void setAllowSelectImageSetPanel(boolean allowSelectImageSetPanel)
  {
    _showSelectImageSetPanelButton.setEnabled(allowSelectImageSetPanel);
  }

  /**
   * @author Scott Richardson
   * enableButtons() provides the default logic to enable/disable this button
   */
  public void setAllowGenerateImageSetWizard(boolean allowGenerateImageSetWizard)
  {
    _showGenerateImageSetWizardButton.setEnabled(allowGenerateImageSetWizard);
  }

  /**
   * @author George A. David
   */
  protected void createToolBar()
  {
    removeAll();

    Dimension separatorSpacing = new Dimension(10, 25); // 10 is width, 25 is height

    add(_zoomInButton);
    add(_zoomOutButton);
    add(_zoomRectangleToggleButton);
    add(_dragToggleButton);
    add(_resetGraphicsButton);
    addSeparator(separatorSpacing);

    add(_topSideToggleButton);
    add(_bottomSideToggleButton);
    addSeparator(separatorSpacing);

    add(_measureToggleButton);

    add(_toggleGraphicsToggleButton);
    add(_toggleImageStayToggleButton);
    //Anthony01005
    add(_toggleImageFitToScreenButton);
    add(_ignoreImageCompatibleCheckButton);

    add(_showSelectImageSetPanelButton);
    add(_showGenerateImageSetWizardButton);

    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    add(_selectRegionToggleButton);
  }

  /**
   * @author George A. David
   * @edited by Kee Chin Seong - Updating Surface Map Data
   */
  private void jbInit()
  {
    _viewTopSideImage = Image5DX.getImageIcon(Image5DX.DC_VIEW_TOPSIDE);
    _viewBottomSideImage = Image5DX.getImageIcon(Image5DX.DC_VIEW_BOTTOMSIDE);
    _zoomInImage = Image5DX.getImageIcon(Image5DX.DC_ZOOM_IN);
    _zoomOutImage = Image5DX.getImageIcon(Image5DX.DC_ZOOM_OUT);
    _zoomRectangleImage = Image5DX.getImageIcon(Image5DX.DC_ZOOM_RECTANGLE);
    _zoomResetImage = Image5DX.getImageIcon(Image5DX.DC_ZOOM_RESET);
    _dragImage = Image5DX.getImageIcon(Image5DX.DC_MOVE);

    _zoomOutButton.setRolloverEnabled(true);
    _toggleGraphicsToggleButton.setActionCommand("Toggle Graphics");
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _selectRegionToggleButton.setName("selectRegionToggleButton");
    _toggleImageStayToggleButton.setActionCommand("Toggle Image to Stay");
    //Anthony01005
    _toggleImageFitToScreenButton.setActionCommand("Toggle Image fit to Screen");

    _viewSidesButtonGroup.add(_topSideToggleButton);
    _viewSidesButtonGroup.add(_bottomSideToggleButton);

    enableButtons();
    _zoomInButton.setFocusable(false);
    _zoomInButton.setEnabled(false);
    _zoomOutButton.setFocusable(false);
    _zoomOutButton.setEnabled(false);
    _zoomRectangleToggleButton.setFocusable(false);
    _zoomRectangleToggleButton.setEnabled(false);
    _dragToggleButton.setFocusable(false);
    _dragToggleButton.setEnabled(false);
    _measureToggleButton.setFocusable(false);
    _measureToggleButton.setEnabled(false);
    _toggleGraphicsToggleButton.setFocusable(false);
    _toggleGraphicsToggleButton.setEnabled(false);
    _toggleImageStayToggleButton.setFocusable(false);
    _toggleImageStayToggleButton.setEnabled(false);
    //Anthony01005
    _toggleImageFitToScreenButton.setFocusable(false);
    _toggleImageFitToScreenButton.setEnabled(false);
    _resetGraphicsButton.setFocusable(false);
    _resetGraphicsButton.setEnabled(false);
    _topSideToggleButton.setFocusable(false);
    _topSideToggleButton.setEnabled(false);
    _bottomSideToggleButton.setFocusable(false);
    _bottomSideToggleButton.setEnabled(false);
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _selectRegionToggleButton.setFocusable(false);
    _selectRegionToggleButton.setEnabled(false);

    _showSelectImageSetPanelButton.setToolTipText(StringLocalizer.keyToString("IMTB_SELECT_IMAGE_SET_KEY"));
    _showSelectImageSetPanelButton.setIcon(Image5DX.getImageIcon(Image5DX.SELECT_IMAGE_SET));
    _showSelectImageSetPanelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_showSelectImageSetPanelButton.isSelected())
        {
          // lets first see if the test program is valid. If not then we will want to create it and notify that user
          // that we are getting the necessary data ready to display the image set panel. was added since for some
          // projects the test program generation is a lengthy call.
          if (_project.isTestProgramValid() == false)
          {
            /** @todo Please add in a user visible reason for generating the test program */
            _prevCheckLatestTestProgram = _project.isCheckLatestTestProgram();
            _project.setCheckLatestTestProgram(true);
            MainMenuGui.getInstance().generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_INSPECTION_IMAGES_NEEDS_TEST_PROGRAM_KEY"),
                StringLocalizer.keyToString("SELECT_IMAGE_SET_PANEL_NAME_KEY"),
                _project);
            _project.setCheckLatestTestProgram(_prevCheckLatestTestProgram);
          }
          try
          {
            _parent.displaySelectImageSetPanel(true);
          }
          catch (XrayTesterException ex)
          {
            // and exception occured while trying to populate the gui display the error and then return out of method.
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(), ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("IMD_TITLE_KEY"), true);

            _showSelectImageSetPanelButton.setSelected(false);
            try
            {
              _parent.displaySelectImageSetPanel(false);
            }
            catch (XrayTesterException ext)
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(), ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("IMD_TITLE_KEY"), true);
            }

          }

        }
        else
        {
          try
          {
            _parent.displaySelectImageSetPanel(false);
          }
          catch (XrayTesterException ex)
          {
            // and exception occured while trying to populate the gui display the error and then return out of method.
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(), ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("IMD_TITLE_KEY"), true);
            return;
          }

        }
      }
    });

    _showGenerateImageSetWizardButton.setToolTipText(StringLocalizer.keyToString("IMTB_GENERATE_IMAGE_SET_KEY"));
    _showGenerateImageSetWizardButton.setIcon(Image5DX.getImageIcon(Image5DX.GENERATE_IMAGE_SET));
    _showGenerateImageSetWizardButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        // lets first see if the test program is valid. If not then we will want to create it and notify that user
        // that we are getting the necessary data ready to display the image set panel. was added since for some
        // projects the test program generation is a lengthy call.
        MainMenuGui mainUI = MainMenuGui.getInstance();
        if (mainUI.isPanelInLegalState() == false)
          return;

        if (_project.isTestProgramValid() == false)
        {
          boolean prevCheckLatestTestProgram = _project.isCheckLatestTestProgram();
          _project.setCheckLatestTestProgram(true);
          mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_IMAGE_SET_GEN_NEEDS_TEST_PROGRAM_KEY"),
              StringLocalizer.keyToString("IMTB_GENERATE_IMAGE_SET_KEY"),
              _project);
          _project.setCheckLatestTestProgram(prevCheckLatestTestProgram);
        }
        else
        {
          mainUI.modifyTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_IMAGE_SET_GEN_NEEDS_TEST_PROGRAM_KEY"),
              StringLocalizer.keyToString("IMTB_GENERATE_IMAGE_SET_KEY"),
              _project);
        }
        
        final BusyCancelDialog busyDialog = new BusyCancelDialog(MainMenuGui.getInstance(),
                                                                 StringLocalizer.keyToString("PSP_UPDATE_DATA_KEY"),
                                                                 StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                                 true);
        busyDialog.pack();
        SwingUtils.centerOnComponent(busyDialog, MainMenuGui.getInstance());
        SwingWorkerThread.getInstance().invokeLater(new Runnable()
        {
           public void run()
           {
             try
             {
                _project.setupSurfaceMapIfNeeded();
             }
             finally
             {
               busyDialog.dispose();
             }
           }
        });
        busyDialog.setVisible(true);

            if (_project.getPanel().getPanelSettings().isManualAlignmentCompleted())
        {
          MainMenuGui.getInstance().generateImageSet();
//          // now lets start the wizard
//          GenerateImageSetWizardDialog dialog = new GenerateImageSetWizardDialog(mainUI, true, _project);
//          SwingUtils.centerOnComponent(dialog, mainUI);
//          dialog.pack();
//          dialog.setVisible(true);
//          dialog.dispose();
        }
        else
        {
          // they have not done manual alignment. so tell them they can no start the wizard.
          String message = "An alignment must be performed before an image set can be generated. \n" +
                           "Go to Alignment Setup to perform the alignment";

          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                        message,
                                        StringLocalizer.keyToString("GISWK_TITLE_KEY"),
                                        JOptionPane.ERROR_MESSAGE);

          return;
        }
      }
    });

    _zoomInButton.setToolTipText(StringLocalizer.keyToString("DCGUI_ZOOMIN_TOOLTIP_KEY"));
    _zoomInButton.setIcon(_zoomInImage);
    _zoomInButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        zoomInButton_actionPerformed(e);
      }
    });

    _zoomOutButton.setToolTipText(StringLocalizer.keyToString("DCGUI_ZOOMOUT_TOOLTIP_KEY"));
    _zoomOutButton.setIcon(_zoomOutImage);
    _zoomOutButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        zoomOutButton_actionPerformed(e);
      }
    });

    _bottomSideToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_VIEWBOTTOM_TOOLTIP_KEY"));
    _bottomSideToggleButton.setIcon(_viewBottomSideImage);
    _bottomSideToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        bottomSideToggleButton_actionPerformed(e);
      }
    });

    _topSideToggleButton.setSelected(true);
    _topSideToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_VIEWTOP_TOOLTIP_KEY"));
    _topSideToggleButton.setIcon(_viewTopSideImage);
    _topSideToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        topSideToggleButton_actionPerformed(e);
      }
    });

    _dragToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_DRAG_TOOLTIP_KEY"));
    _dragToggleButton.setIcon(_dragImage);
    _dragToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dragToggleButton_actionPerformed(e);
      }
    });

    _measureToggleButton.setToolTipText(StringLocalizer.keyToString("GUI_MEASURE_TOOLTIP_KEY"));
    _measureToggleButton.setIcon(Image5DX.getImageIcon(Image5DX.MEASURE));
//    _measureToggleButton.setIcon(_dragImage);
    _measureToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        measureToggleButton_actionPerformed(e);
      }
    });

    _toggleGraphicsToggleButton.setToolTipText(StringLocalizer.keyToString("GUI_TOGGLE_GRAPHICS_TOOLTIP_KEY"));
    _toggleGraphicsToggleButton.setIcon(Image5DX.getImageIcon(Image5DX.TOGGLE_GRAPHICS));
//    _toggleGraphicsToggleButton.setIcon(_dragImage);
    _toggleGraphicsToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        toggleGraphicsToggleButton_actionPerformed(e);
      }
    });

    _toggleImageStayToggleButton.setToolTipText(StringLocalizer.keyToString("GUI_TOGGLE_IMAGE_STAY_TOOLTIP_KEY"));
    _toggleImageStayToggleButton.setIcon(Image5DX.getImageIcon(Image5DX.TOGGLE_IMAGE_STAY));
    _toggleImageStayToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        toggleImageStayToggleButton_actionPerformed(e);
      }
    });

    //Anthony01005
    _toggleImageFitToScreenButton.setToolTipText(StringLocalizer.keyToString("GUI_TOGGLE_IMAGE_FIT_TO_SCREEN"));
    _toggleImageFitToScreenButton.setIcon(Image5DX.getImageIcon(Image5DX.TOGGLE_IMAGE_FIT_TO_SCREEN));
    _toggleImageFitToScreenButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        toggleImageFitToScreenButton_actionPerformed(e);
      }
    });
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _selectRegionToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_SELECT_REGION_KEY"));
    _selectRegionToggleButton.setIcon(Image5DX.getImageIcon(Image5DX.DC_SELECT_REGION));
    _selectRegionToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectRegionToggleButton_actionPerformed(e);
      }
    });
    
    _zoomRectangleToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_ZOOMWINDOW_TOOLTIP_KEY"));
    _zoomRectangleToggleButton.setIcon(_zoomRectangleImage);
    _zoomRectangleToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        zoomRectangleToggleButton_actionPerformed(e);
      }
    });

    _resetGraphicsButton.setToolTipText(StringLocalizer.keyToString("DCGUI_ZOOMRESTORE_TOOLTIP_KEY"));
    _resetGraphicsButton.setIcon(_zoomResetImage);
    _resetGraphicsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        resetGraphicsButton_actionPerformed(e);
      }
    });

    _ignoreImageCompatibleCheckButton.setToolTipText(StringLocalizer.keyToString("DCGUI_IGNORE_IMAGE_COMPATIBLE_TOOLTIP_KEY"));
    _ignoreImageCompatibleCheckButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ignoreImageCompatibleCheckButton_actionPerformed(e);
      }
    });

    setFloatable(false);
    setRollover(true);

    Insets margins = new Insets(0, 2, 2, 0);
    _zoomInButton.setMargin(margins);
    _zoomOutButton.setMargin(margins);
    _zoomRectangleToggleButton.setMargin(margins);
    _dragToggleButton.setMargin(margins);
    _measureToggleButton.setMargin(margins);
    _resetGraphicsButton.setMargin(margins);
    _toggleGraphicsToggleButton.setMargin(margins);
    _toggleImageStayToggleButton.setMargin(margins);
    _topSideToggleButton.setMargin(margins);
    _bottomSideToggleButton.setMargin(margins);
    _showSelectImageSetPanelButton.setMargin(margins);
    _showGenerateImageSetWizardButton.setMargin(margins);
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _selectRegionToggleButton.setMargin(margins);

    _zoomInButton.setName(".zoomInButton");
    _zoomOutButton.setName(".zoomOutButton");
    _zoomRectangleToggleButton.setName(".zoomRectangleToggleButton");
    _dragToggleButton.setName(".dragToggleButton");
    _measureToggleButton.setName(".measureToggleButton");
    _toggleGraphicsToggleButton.setName(".toggleGraphicsToggleButton");
    _toggleImageStayToggleButton.setName(".toggleImageStayToggleButton");
    _resetGraphicsButton.setName(".resetGraphicsButton");
    _topSideToggleButton.setName(".topSideToggleButton");
    _bottomSideToggleButton.setName(".bottomSideToggleButton");
    _showSelectImageSetPanelButton.setName(".showSelectImageSetPanelButton");
    _showGenerateImageSetWizardButton.setName(".showGenerateImageSetWizardButton");
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _selectRegionToggleButton.setName("selectRegionToggleButton");
    
    createToolBar();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void enableButtons()
  {
    if (SwingUtilities.isEventDispatchThread() == false)
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          enableButtons();
        }
      });
    else
    {
      // add logic that will turn on and off the buttons.
      // right now just turn them on if the project is loaded.
      if (_project != null)
      {
        if (_tuningScreensVisible)
        {
          _showSelectImageSetPanelButton.setEnabled(true);
        }
        else
        {
          if (_showSelectImageSetPanelButton.isSelected())
            _showSelectImageSetPanelButton.doClick();
          _showSelectImageSetPanelButton.setEnabled(false);
        }
        
        try
        {
          if (LicenseManager.isOfflineProgramming() == false)
          {
            if (XrayTester.getInstance().isSimulationModeOn()
                || (XrayTester.getInstance().isHardwareAvailable()
                    && _project.isSystemTypeSettingConfigCompatibleWithSystem()))
              _showGenerateImageSetWizardButton.setEnabled(true);
            else
              _showGenerateImageSetWizardButton.setEnabled(false);
          }
          else
          {
            _showGenerateImageSetWizardButton.setEnabled(false);
          }
        }
        catch (final BusinessException e)
        {
          
        }

      }
      else
      {
        if (_showSelectImageSetPanelButton.isSelected())
          _showSelectImageSetPanelButton.doClick();
        _showSelectImageSetPanelButton.setEnabled(false);
        _showGenerateImageSetWizardButton.setEnabled(false);
      }
    }
  }

  /**
   * @param project Project
   * @author Erica Wheatcroft
   */
  void populateWithProjectData(Project project)
  {
    Assert.expect(project != null, "project is null");
    _project = project;
    // enable the buttons
    enableButtons();
    // make sure that button is not pressed as we are loading a new project
    _showSelectImageSetPanelButton.setSelected(false);
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    Assert.expect(observable != null, " observable is null");

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof GuiObservable)
        {
          if (object instanceof GuiEvent)
          {
            GuiEventEnum guiEventEnum = ((GuiEvent)object).getGuiEventEnum();
            if (guiEventEnum instanceof TaskPanelScreenEnum)
            {
              if (guiEventEnum.equals(TaskPanelScreenEnum.ALGORITHM_TUNER) ||
                  guiEventEnum.equals(TaskPanelScreenEnum.INITIAL_TUNE))
              {
                _tuningScreensVisible = true;
                enableButtons(); // no need for swing thread.. method does its work on the swing thread.
              }
              else
              {
                // lets check to see if the toggle button is pressed if so then we want to set the flag and
                // make the panel not visible.
                _tuningScreensVisible = false;
                enableButtons();
              }
            }
            else if (guiEventEnum instanceof GuiUpdateEventEnum)
            {
              GuiUpdateEventEnum guiUpdateEventEnum = (GuiUpdateEventEnum)guiEventEnum;
              if (guiUpdateEventEnum.equals(GuiUpdateEventEnum.TUNER_INSPECTION_START))
              {
                // algo tuner can loop continuously; make sure buttons aren't enabled between loops
                _algoTunerRunMode = true;
              }
              else if (guiUpdateEventEnum.equals(GuiUpdateEventEnum.TUNER_INSPECTION_FINISH))
              {
                _algoTunerRunMode = false;
                enableButtons();
              }
            }
          }
        }
        else if (observable instanceof ProjectObservable)
        {
          // the user has updated the project check to see if we should update our buttons.
          if (object instanceof ProjectChangeEvent)
          {
            ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)object;
            ProjectChangeEventEnum projectChangeEventEnum = projectChangeEvent.getProjectChangeEventEnum();

            if (projectChangeEventEnum instanceof ProjectStateEventEnum)
            {
              ProjectStateEventEnum projectStateEventEnum = (ProjectStateEventEnum)projectChangeEventEnum;
              if (projectStateEventEnum.equals(ProjectStateEventEnum.ALIGNMENT_TRANSFORM_INVALID))
              {
                enableButtons();
              }
              else if (projectStateEventEnum.equals(ProjectStateEventEnum.ALIGNMENT_TRANSFORM_VALID))
              {
                enableButtons();
              }
              if(projectStateEventEnum.equals(ProjectStateEventEnum.TEST_PROGRAM_INVALID))
              {
                // the test program is invalid. If the Select Image Set Panel is up lets close it so that
                // when the user brings it back up the test program will be regenerated.
                if (_parent.isSelectImageSetPanelVisible())
                  _showSelectImageSetPanelButton.doClick();
              }
            }
            else if (projectChangeEventEnum instanceof ProjectEventEnum)
            {
              ProjectEventEnum projectEventEnum = (ProjectEventEnum)projectChangeEventEnum;

              if (projectEventEnum.equals(ProjectEventEnum.SLOW_LOAD) ||
                  projectEventEnum.equals(ProjectEventEnum.FAST_LOAD) ||
                  projectEventEnum.equals(ProjectEventEnum.IMPORT_PROJECT_FROM_NDFS))
              {
                try
                {
                  if (_parent.isSelectImageSetPanelVisible())
                    _parent.displaySelectImageSetPanel(false);
                }
                catch (XrayTesterException ex)
                {
                  // and exception occured while trying to populate the gui display the error and then return out of method.
                  MessageDialog.showErrorDialog(MainMenuGui.getInstance(), ex.getLocalizedMessage(),
                                                StringLocalizer.keyToString("IMD_TITLE_KEY"), true);
                  return;

                }
              }
            }
          }
        }
        else if (observable instanceof InspectionEventObservable)
        {
          Assert.expect(object instanceof InspectionEvent, "not an instance of InspectionEventEnum");
          InspectionEventEnum event = ((InspectionEvent)object).getInspectionEventEnum();
          // check to see if the user is starting a test or ending one. then update
          // buttons as needed.
          if (event.equals(InspectionEventEnum.INSPECTION_STARTED) ||
              event.equals(InspectionEventEnum.SUBTYPE_LEARNING_STARTED))
          {
            // if the user has started a test and the select image set is visible .. lets hide it.
            if (_showSelectImageSetPanelButton.isSelected())
            {
              _showSelectImageSetPanelButton.doClick();
            }

            _showSelectImageSetPanelButton.setEnabled(false);
            _showGenerateImageSetWizardButton.setEnabled(false);
          }
          else if (event.equals(InspectionEventEnum.INSPECTION_COMPLETED) ||
                   event.equals(InspectionEventEnum.SUBTYPE_LEARNING_COMPLETED) )
          {
            // don't enable buttons until algo tuner is done
            if (_algoTunerRunMode == false)
            {
              enableButtons();
            }
          }
        }
        else
          Assert.expect(false, "observable is not recongized");

      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  public void resetForNewProject()
  {
    _showSelectImageSetPanelButton.setSelected(false);
    _zoomRectangleToggleButton.setSelected(false);
    _dragToggleButton.setSelected(false);
    _measureToggleButton.setSelected(false);
    _toggleGraphicsToggleButton.setSelected(false);
    _toggleImageStayToggleButton.setSelected(false);
    _topSideToggleButton.setSelected(false);
    _bottomSideToggleButton.setSelected(false);

    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _selectRegionToggleButton.setSelected(false);

    _project = null;
  }
  /**
   * will disable all the buttons and other controls for tool bar
   * Used when there are no graphics to display because no project is loaded yet
   * @author Andy Mechteberg
   */
  public void disableAllControls()
  {
    Component components[] = getComponents();
    for (int i = 0; i < components.length; i++)
    {
      Component comp = components[i];
      // if is a JPanel, then we can't just disable the panel, we have to disable all the controls in the panel
      // I suppose should be recursive, but hopefully only one level of nesting will occur.
      if (comp instanceof JPanel)
      {
        Component nestedComponents[] = ((JPanel)comp).getComponents();
        for (int j = 0; j < nestedComponents.length; j++)
        {
          Component nestedComp = nestedComponents[j];
          nestedComp.setEnabled(false);
        }
      }
      else
        comp.setEnabled(false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void topSideToggleButton_actionPerformed(ActionEvent e)
  {
    showTopSide();
  }

  /**
   * @author Andy Mechtenberg
   */
  void showTopSide()
  {
    _graphicsEngineSetup.showTopSide();
    setSideViewButtons(true);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void bottomSideToggleButton_actionPerformed(ActionEvent e)
  {
    showBottomSide();
  }

  /**
   * @author Andy Mechtenberg
   */
  void showBottomSide()
  {
    _graphicsEngineSetup.showBottomSide();
    setSideViewButtons(false);
  }

  /**
   * @author George A. David
   */
  void setSideViewButtons(boolean isTopSideVisible)
  {
    if (isTopSideVisible)
    {
      _topSideToggleButton.setSelected(true);
      _bottomSideToggleButton.setSelected(false);
    }
    else
    {
      _topSideToggleButton.setSelected(false);
      _bottomSideToggleButton.setSelected(true);
    }
  }

  /**
   * @author George A. David
   */
  void setMeasureToggleButtonSelected(boolean selected)
  {
    _measureToggleButton.setSelected(selected);
    if (selected)
    {
      _dragToggleButton.setSelected(false);
      _zoomRectangleToggleButton.setSelected(false);
    }
  }

  /**
   * @author George A. David
   */
  void setZoomRectangleToggleButtonSelected(boolean selected)
  {
    _zoomRectangleToggleButton.setSelected(selected);
    if (selected)
    {
      _measureToggleButton.setSelected(false);
      _dragToggleButton.setSelected(false);
    }
  }

  /**
   * @author George A. David
   */
  public void setDragToggleButtonSelected(boolean selected)
  {
    _dragToggleButton.setSelected(selected);
    if (selected)
    {
      _measureToggleButton.setSelected(false);
      _zoomRectangleToggleButton.setSelected(false);
    }
  }

  /**
   * @author George A. David
   */
  void setToggleGraphicsToggleButtonSelected(boolean selected)
  {
    _toggleGraphicsToggleButton.setSelected(selected);
  }

  /**
   * @author Seng-Yew Lim
   */
  void setToggleImageStayToggleButtonSelected(boolean selected)
  {
    _toggleImageStayToggleButton.setSelected(selected);
  }

  /**
   * Recieves the current selected thing's description
   * @author Andy Mechtenberg
   */
  private void setDescription(String description)
  {
//    _parent.setDescription(description);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void zoomInButton_actionPerformed(ActionEvent e)
  {
    _graphicsEngineSetup.zoomIn();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void zoomOutButton_actionPerformed(ActionEvent e)
  {
    _graphicsEngineSetup.zoomOut();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void zoomRectangleToggleButton_actionPerformed(ActionEvent e)
  {
    if (_zoomRectangleToggleButton.isSelected())
    {
      _dragToggleButton.setSelected(false);
      _measureToggleButton.setSelected(false);
      _graphicsEngineSetup.setZoomRectangleMode(true);
    }
    else
    {
      _graphicsEngineSetup.setZoomRectangleMode(false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void dragToggleButton_actionPerformed(ActionEvent e)
  {
    if (_dragToggleButton.isSelected())
    {
      _zoomRectangleToggleButton.setSelected(false);
      _measureToggleButton.setSelected(false);
      _graphicsEngineSetup.setDragGraphicsMode(true);
    }
    else
    {
      _graphicsEngineSetup.setDragGraphicsMode(false);
    }
  }

  /**
   * @author George A. David
   */
  private void toggleGraphicsToggleButton_actionPerformed(ActionEvent e)
  {
    if (_toggleGraphicsToggleButton.isSelected())
      _graphicsEngineSetup.clearGraphics();
    else
      _graphicsEngineSetup.showGraphics();
  }

  /**
   * @author Seng-Yew Lim
   */
  private void toggleImageStayToggleButton_actionPerformed(ActionEvent e)
  {
    if (_toggleImageStayToggleButton.isSelected())
      _graphicsEngineSetup.setImageStayPersistent(true);
    else
      _graphicsEngineSetup.setImageStayPersistent(false);
  }
  /**
   * @author Anthoyn Fong //Anthony01005
   */
  private void toggleImageFitToScreenButton_actionPerformed(ActionEvent e)
  {
    if (_toggleImageFitToScreenButton.isSelected())
    {
      _graphicsEngineSetup.setImageFitToScreen(true);
    }
    else
    {
      _graphicsEngineSetup.setImageFitToScreen(false);
    }
  }

  /**
   * @author George A. David
   */
  private void measureToggleButton_actionPerformed(ActionEvent e)
  {
    if (_measureToggleButton.isSelected())
    {
      _zoomRectangleToggleButton.setSelected(false);
      _dragToggleButton.setSelected(false);
      _graphicsEngineSetup.setMeasurmentMode(true);
    }
    else
    {
      _graphicsEngineSetup.setMeasurmentMode(false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void resetGraphicsButton_actionPerformed(ActionEvent e)
  {
    _graphicsEngineSetup.resetGraphics();
  }

  /**
   * @author Chong, Wei Chin
   */
  private void ignoreImageCompatibleCheckButton_actionPerformed(ActionEvent e)
  {
    ImageManager.setIgnoreCompatibilityCheck(_ignoreImageCompatibleCheckButton.isSelected());
    if (_showSelectImageSetPanelButton.isSelected())
      _showSelectImageSetPanelButton.doClick();    
  }  

  /**
   * @author George A. David
   */
  private void showCurrentSides()
  {
    if (_topSideToggleButton.isSelected())
      showTopSide();

    if (_bottomSideToggleButton.isSelected())
      showBottomSide();
  }
  
  /*
   * @author bee-hoon.goh
   */
  public boolean isToggleImageFitToScreenButtonSelected()
  {
    return _toggleImageFitToScreenButton.isSelected();
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  private void selectRegionToggleButton_actionPerformed(ActionEvent e)
  {
    double maximumImagePanelRegionSizeInPixel = _maximumImagePanelSelectRegionSizeInPixel;
    
    if (_selectRegionToggleButton.isSelected())
    {
      _zoomRectangleToggleButton.setSelected(false);
      _dragToggleButton.setSelected(false);
      
      _graphicsEngineSetup.setGroupSelectMode(false);
      _graphicsEngineSetup.setSelectRegionMode(true, maximumImagePanelRegionSizeInPixel, maximumImagePanelRegionSizeInPixel);
    }
    else
    {
      _graphicsEngineSetup.setGroupSelectMode(false);
      _graphicsEngineSetup.setSelectRegionMode(false, maximumImagePanelRegionSizeInPixel, maximumImagePanelRegionSizeInPixel);
    }
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setEnableSelectRegionToggleButon(boolean enable)
  {
    double maximumImagePanelRegionSizeInPixel = _maximumImagePanelSelectRegionSizeInPixel;

    if (_selectRegionToggleButton.isSelected())
    {
      if(enable == true)
      {
        // Reset to previous mode when switch back to virtualLive
        _selectRegionToggleButton.setSelected(true);
        _zoomRectangleToggleButton.setSelected(false);
        _dragToggleButton.setSelected(false);
        _graphicsEngineSetup.setGroupSelectMode(false);
        _graphicsEngineSetup.setSelectRegionMode(true, maximumImagePanelRegionSizeInPixel, maximumImagePanelRegionSizeInPixel);
      }
    }
    _selectRegionToggleButton.setEnabled(enable);
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setSelectRegionToggleButtonSelected(boolean selected)
  {
    _selectRegionToggleButton.setSelected(selected);
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setSelectRegionMaximumSizeInPixel(double maximumImagePanelSelectRegionSizeInPixel)
  {
    _maximumImagePanelSelectRegionSizeInPixel = maximumImagePanelSelectRegionSizeInPixel;
  }
}
