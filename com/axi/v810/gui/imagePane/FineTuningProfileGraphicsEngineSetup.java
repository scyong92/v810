package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.lang.reflect.*;
import java.text.*;
import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * The FineTuningProfileGraphicsEngineSetup class sets up the graphics engine
 * for displaying profile graphics from the algorithms.
 * @author George A. David
 */
public class FineTuningProfileGraphicsEngineSetup implements Observer
{
  private static final int _BORDER_SIZE_IN_PIXELS = 0;

  private GraphicsEngine _graphicsEngine;
  private GraphicsEngine _graphicsEngineForImageCreation;
  private InspectionEventObservable _inspectionObservable = null;

  // Graphic engine layers
  private int _backgroundLayer;
  // hidden layer used to add line in graphics engine background color to get image size correct
  private int _hiddenLayer;
  private int _axisLayer;
  private int _textLayer;
  private java.util.List<Integer> _allGraphicLayers = new LinkedList<Integer>();

  Map<MeasurementRegionEnum, Integer> _measurementRegionEnumToGraphicsLayerNumberMap = new HashMap<MeasurementRegionEnum, Integer>();
  private int _imageLayer;

  private Stroke _outlineStroke = new BasicStroke(2);  // was 5
  private int _xOffsetToAvoidPrevProfile = 0;
  private int _totalWidth = 0;
  private JScrollBar _horizontalScrollBar;
  //Anthony01005 -->
  private boolean _enableProfileZoom = false;
  private float _profileZoomFactor = 1.0f;
  private JScrollPane _graphicsEngineParent = null;
  public void setGraphicsEngineParent(JScrollPane Parent)
  {
    Assert.expect(Parent != null);

      _graphicsEngineParent = Parent;
  }
  //Anthony01005 <--
  /**
   * @author George A. David
   */
  public FineTuningProfileGraphicsEngineSetup()
  {
    _graphicsEngine = new GraphicsEngine(_BORDER_SIZE_IN_PIXELS);
    _graphicsEngine.setBackground(Color.lightGray);
    _graphicsEngine.setShouldGraphicsBeFitToScreen(false);
    _graphicsEngine.setAxisInterpretation(true, true);
//    _graphicsEngine.setAllowDrayInY(false);
    _graphicsEngine.setZoomEnabled(false);

    _graphicsEngineForImageCreation = new GraphicsEngine(0);
    _graphicsEngineForImageCreation.setBackground(Color.lightGray);
    _graphicsEngineForImageCreation.setAxisInterpretation(true, true);
    _inspectionObservable = InspectionEventObservable.getInstance();
    createLayers();
    setColors();
  }

  /**
   * @author Anthony Fong //Anthony01005
   */
  public void setEnableProfileZoom(boolean enableProfileZoom )
  {
    _enableProfileZoom = enableProfileZoom;
  }
  
  /**
   * @author Andy Mechtenberg
   */
  public void display()
  {
    _inspectionObservable.getInstance().addObserver(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void finish()
  {
    _inspectionObservable.getInstance().deleteObserver(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _graphicsEngine.removeRenderers(_imageLayer);
    _graphicsEngine.setPreferredSize(new Dimension(0, 50));
  }

  /**
   * @author George A. David
   */
  public GraphicsEngine getGraphicsEngine()
  {
    Assert.expect(_graphicsEngine != null);

    return _graphicsEngine;
  }

  /**
   * @author George Booth
   */
  void setHorizontalScrollBar(JScrollBar horizontalScrollBar)
  {
    Assert.expect(horizontalScrollBar != null);
    _horizontalScrollBar = horizontalScrollBar;
  }

  /**
   * @author George A. David
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    try
    {
      if (SwingUtilities.isEventDispatchThread())
      {
        handleUpdate(observable, argument);
      }
      else
      {
        // PE:  I am using invokeAndWait() here because we have to handle the algorithm
        // events as they occur.  Otherwise, the algorithms get out of sync with
        // the GUI and can build up a big backlog of events, which the swing thread
        // then processes in chunks at a time.  This makes the user experience very
        // herky jerky and also makes it difficult to debug behavior in the algorithms.
        SwingUtils.invokeAndWait(new Runnable()
        {
          public void run()
          {
            handleUpdate(observable, argument);
          }
        });
      }
    }
    catch (InterruptedException iex)
    {
      Assert.logException(iex);
    }
    catch (InvocationTargetException itex)
    {
      Assert.logException(itex);
    }
  }

  /**
   * @author Matt Wharton
   */
  private void handleUpdate(Observable observable, Object argument)
  {
    if (observable instanceof InspectionEventObservable)
    {
      if (argument instanceof AlgorithmDiagnosticInspectionEvent)
      {
        AlgorithmDiagnosticInspectionEvent algorithmDiagEvent = (AlgorithmDiagnosticInspectionEvent)argument;
        if (algorithmDiagEvent.hasAlgorithmDiagnosticMessage())
        {
          displayProfile(algorithmDiagEvent.getAlgorithmDiagnosticMessage());
        }
      }
      else if(argument instanceof InspectionEvent)
      {
        InspectionEvent inspectionEvent = (InspectionEvent)argument;
        if(inspectionEvent.getInspectionEventEnum().equals(InspectionEventEnum.INSPECTION_STARTED))
        {
          _graphicsEngine.removeRenderers(_imageLayer);
          setColors();
          _xOffsetToAvoidPrevProfile = 0;
          _graphicsEngine.repaint();
        }
      }
    }
    else
      Assert.expect(false);
  }

  /**
   * @author George A. David
   */
  public void displayProfile(AlgorithmDiagnosticMessage algorithmDiagMessage)
  {
    Assert.expect(algorithmDiagMessage != null);

    if (algorithmDiagMessage.arePreviousDiagnosticsInvalidated())
    {
      _graphicsEngine.removeRenderers(_imageLayer);
      setColors();
      _totalWidth = 0;
      _graphicsEngine.setPreferredSize(new Dimension(0, 50));
    }

    if (algorithmDiagMessage.hasProfileDiagnosticInfo())
    {
      ProfileDiagnosticInfo profileInfo = algorithmDiagMessage.getProfileDiagnosticInfo();

      if (profileInfo instanceof SingleDimensionProfileDiagnosticInfo)
      {
        SingleDimensionProfileDiagnosticInfo singleDimensionProfileInfo = (SingleDimensionProfileDiagnosticInfo)
            profileInfo;

        Assert.expect(singleDimensionProfileInfo.getNumberOfDimensions() == 1);

        // If needed, convert to the user specified display units.
        Project project = Project.getCurrentlyLoadedProject();
        MeasurementUnitsEnum profileMeasurementUnitsEnum = singleDimensionProfileInfo.getProfileUnits();
        MathUtilEnum displayUnits = project.getDisplayUnits();
        MathUtilEnum profileUnits = null;
        if (profileMeasurementUnitsEnum.equals(MeasurementUnitsEnum.MILS))
        {
          profileUnits = MathUtilEnum.MILS;
        }
        else if (profileMeasurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS))
        {
          profileUnits = MathUtilEnum.MILLIMETERS;
        }
        else
        {
          // Native profile unit is not one that we need to convert.  We'll just set the source unit
          // to match the user display unit--that way, no conversion will take place.
          profileUnits = displayUnits;
        }

        java.util.List<Pair<Float, Float>> axisExtents = singleDimensionProfileInfo.getAxisExtents();
        Pair<Float, Float> axisExtentsPair = axisExtents.get(0);
        float maxYvalue = MathUtil.convertUnits(axisExtentsPair.getSecond(), profileUnits, displayUnits);
        float minYvalue = MathUtil.convertUnits(axisExtentsPair.getFirst(), profileUnits, displayUnits);
//        System.out.println("glb - maxY = " + maxYvalue + ", minY = " + minYvalue);
        float yAxisSize = maxYvalue - minYvalue;
            if(yAxisSize == 0)
              yAxisSize = 100;
        
        // Anthony01005  -->      
        if(_enableProfileZoom)
        {                
            if(yAxisSize == 0)
            {
                if(_horizontalScrollBar.isShowing())
                {
                    //yAxisSize = _graphicsEngineParent.getHeight()-80-40;
                    yAxisSize = _graphicsEngineParent.getHeight()-80;
                }else
                {
                    yAxisSize = _graphicsEngineParent.getHeight()-80;
                }
            }
        }
        // Anthony01005  <--      

 
        // we only want to scale the y-axis. we want the graphics to always
        // be 100 pixels tall.
        // Anthony01005 -->
        float yAxisHeight = 100.0f;
        if(_enableProfileZoom)
        {
            yAxisHeight = _graphicsEngineParent.getHeight()-80;
            if(_horizontalScrollBar.isShowing())
            {
                //Height = _graphicsEngineParent.getHeight()-80-40;
                yAxisHeight = _graphicsEngineParent.getHeight()-80;
            }
            else
            {
                yAxisHeight = _graphicsEngineParent.getHeight()-80;
            }
            _profileZoomFactor = yAxisHeight/100.0f;
            if(_profileZoomFactor<1)
            {
                _profileZoomFactor=1;
            }
            else
            {
                _profileZoomFactor = (int)Math.ceil(_profileZoomFactor);
            }
        }

        float actualToZoomRatio = yAxisHeight / yAxisSize;
        // Anthony01005  <--
        float yZoomAxisSize = yAxisSize * actualToZoomRatio;
        float xAxisSize = singleDimensionProfileInfo.getData().size();
        float yZoomOffset = 10;

//        System.out.println("glb - xAxisSize = " + xAxisSize);
//        System.out.println("glb - yZoomAxisSize = " + yZoomAxisSize);

        float maxYzoomValue = maxYvalue * actualToZoomRatio;
        float minYzoomValue = minYvalue * actualToZoomRatio;

        NumberFormat nf = new DecimalFormat();
        /** PWL : This is actually inaccurate - if there is a non-distance unit being used (eg. grayscale),
         * this will inheirit the decimal places of whatever distance unit has been chosen, eg. mils with 1. */
        nf.setMaximumFractionDigits(MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits));

        float xTextOffset = 10;

        // graphics artifact
        // draw a hidden crosshair at 0,0 so that graphics renders the text offset
        _graphicsEngineForImageCreation.addRenderer(_hiddenLayer, new ProfileGraphicRenderer(
            new Line2D.Float(0, 0, xAxisSize, 0)));
        _graphicsEngineForImageCreation.addRenderer(_hiddenLayer, new ProfileGraphicRenderer(
            new Line2D.Float(0, 0, 0, yZoomAxisSize)));

        ProfileTextRenderer maxYvalueRenderer = new ProfileTextRenderer(
            nf.format(maxYvalue), xTextOffset, 0);
        ProfileTextRenderer minYvalueRenderer = new ProfileTextRenderer(
            nf.format(minYvalue), xTextOffset, (double)yZoomAxisSize);
        _graphicsEngineForImageCreation.addRenderer(_textLayer, maxYvalueRenderer);
        _graphicsEngineForImageCreation.addRenderer(_textLayer, minYvalueRenderer);
//        System.out.println("glb - maxYvalueRenderer size = " + maxYvalueRenderer.getWidth() + ", " + maxYvalueRenderer.getHeight());
//        System.out.println("glb - minYvalueRenderer size = " + minYvalueRenderer.getWidth() + ", " + minYvalueRenderer.getHeight());

        float xOffsetToAvoidText = Math.max(maxYvalueRenderer.getWidth(), minYvalueRenderer.getWidth()) + 12 + xTextOffset;

        // create a border
        // Anthony01005 -->
        ProfileGraphicRenderer outlineRenderer;
        if(_enableProfileZoom)
        {
            outlineRenderer = new ProfileGraphicRenderer(
            new Rectangle2D.Float(xOffsetToAvoidText - 1, yZoomOffset - 1, _profileZoomFactor*(xAxisSize) + 2, yZoomAxisSize + 2));
        }
        else
        {
            outlineRenderer = new ProfileGraphicRenderer(
                new Rectangle2D.Float(xOffsetToAvoidText - 1, yZoomOffset - 1, xAxisSize + 2, yZoomAxisSize + 2));
        }
        // Anthony01005 <--
        outlineRenderer.setStroke(_outlineStroke);
        _graphicsEngineForImageCreation.addRenderer(_axisLayer, outlineRenderer);
//        System.out.println("glb - outlineRenderer size = " + outlineRenderer.getWidth() + ", " + outlineRenderer.getHeight());

        // graphics artifact
        // draw a hidden line outside border so that graphics renders the whole outline
        float xOutsideBorder = xOffsetToAvoidText + xAxisSize + 2;
        
        // Anthony01005 -->
        if(_enableProfileZoom)
        {
            _graphicsEngineForImageCreation.addRenderer(_hiddenLayer, new ProfileGraphicRenderer(
                new Line2D.Float(_profileZoomFactor*xOutsideBorder, 0, _profileZoomFactor*xOutsideBorder, yZoomAxisSize)));
        }
        else
        {
            _graphicsEngineForImageCreation.addRenderer(_hiddenLayer, new ProfileGraphicRenderer(
                new Line2D.Float(xOutsideBorder, 0, xOutsideBorder, yZoomAxisSize)));
        }
        // Anthony01005 <--

        // fill in the background
        float xOffsetToAvoidBorder = xOffsetToAvoidText + 3;
        float yOffsetToAvoidBorder = yZoomOffset + 3;

        // Anthony01005 -->
        ProfileGraphicRenderer filledRenderer;
        
        if(_enableProfileZoom)
        {
            filledRenderer = new ProfileGraphicRenderer(
                new Rectangle2D.Double(xOffsetToAvoidText, yZoomOffset, _profileZoomFactor*xAxisSize, yZoomAxisSize));
        }
        else
        {
            filledRenderer = new ProfileGraphicRenderer(
                new Rectangle2D.Double(xOffsetToAvoidText, yZoomOffset, xAxisSize, yZoomAxisSize));
        }
        filledRenderer.setFillShape(true);
        _graphicsEngineForImageCreation.addRenderer(_backgroundLayer, filledRenderer);
//        System.out.println("glb - filledRenderer size = " + filledRenderer.getWidth() + ", " + filledRenderer.getHeight());

        // draw the graph / profile
        int index = 0;
        for (Pair<MeasurementRegionEnum, Float> pair : singleDimensionProfileInfo.getData())
        {
          MeasurementRegionEnum measurementRegion = pair.getFirst();

          Integer layer = _measurementRegionEnumToGraphicsLayerNumberMap.get(measurementRegion);
          Assert.expect(layer != null);

          float dataPoint = MathUtil.convertUnits(pair.getSecond(), profileUnits, displayUnits);
          // data should be clipped at the stated max/min
          if (dataPoint > maxYvalue)
          {
            dataPoint = maxYvalue;
          }
          if (dataPoint < minYvalue)
          {
            dataPoint = minYvalue;
          }
          // create a line
          float beginX = index + xOffsetToAvoidText;
          // Anthony01005 -->
          if(_enableProfileZoom)
          {
              beginX = _profileZoomFactor*index + xOffsetToAvoidText;
          }
          // Anthony01005 <--
          float beginY = yZoomAxisSize + yZoomOffset;
          float endX = beginX;
          float endY = yZoomAxisSize + yZoomOffset - (dataPoint * actualToZoomRatio) + minYzoomValue;
          if (endY == beginY)
          {
            // graphics artifact - seems to draw 1 pixel below desired location for this case
            endY = endY - 1;
          }
//          System.out.print("glb - x = " + index + " y = " + dataPoint + ", scaledY = " + endY);
//          System.out.println(" -- measurementRegion = " + StringLocalizer.keyToString(measurementRegion.getRegionDescription()));
            _graphicsEngineForImageCreation.addRenderer(layer, new ProfileGraphicRenderer(
              new Line2D.Float(beginX, beginY, endX, endY)));
            ++index;
        }

        int maxTextWidth = 0;
        double yTextOffset = yZoomOffset + yZoomAxisSize + 5;
        for (LocalizedString localString : singleDimensionProfileInfo.getAxisDescriptions())
        {
          ProfileTextRenderer renderer = new ProfileTextRenderer(
              StringLocalizer.keyToString(localString), xOffsetToAvoidText, yTextOffset);
          maxTextWidth = Math.max(maxTextWidth, renderer.getWidth());
          _graphicsEngineForImageCreation.addRenderer(_textLayer, renderer);
          yTextOffset += renderer.getHeight();
        }

        ProfileTextRenderer padInfo = padInfo = new ProfileTextRenderer(
              singleDimensionProfileInfo.getNameOfJointOrComponent(), xOffsetToAvoidText, yTextOffset);
        maxTextWidth = Math.max(maxTextWidth, padInfo.getWidth());
        _graphicsEngineForImageCreation.addRenderer(_textLayer, padInfo);

        BufferedImage image = _graphicsEngineForImageCreation.createImage(0); // was 1
        _graphicsEngineForImageCreation.removeRenderers(_allGraphicLayers);
        setColors();

        _xOffsetToAvoidPrevProfile -= image.getWidth() + 15;
        _totalWidth += image.getWidth() + 15;
//        System.out.print("glb - image size = " + image.getWidth() + ", " + image.getHeight());

        //Anthony01005
        BufferedImageRenderer bufferedImageRenderer = new BufferedImageRenderer(image, _xOffsetToAvoidPrevProfile, 0);
        //bufferedImageRenderer.setBicubicInterpolation(_enableProfileZoom);
   
        _graphicsEngine.addRenderer(_imageLayer, bufferedImageRenderer);
        //_graphicsEngine.addRenderer(_imageLayer, new BufferedImageRenderer(image, _xOffsetToAvoidPrevProfile, 0));
//        System.out.print("glb - graphicsEngine size = " + _graphicsEngine.getWidth() + ", " + _graphicsEngine.getHeight());

        int visibleX = _xOffsetToAvoidPrevProfile;
        int visibleY = 0;
        int visibleWidth = _graphicsEngine.getWidth();
        int visibleHeight = image.getHeight();
        // Anthony01005 -->
        if(_enableProfileZoom)
        {
            visibleHeight =  _graphicsEngineParent.getHeight()-80;
            if(_horizontalScrollBar.isShowing())
            {
                //visibleHeight = _graphicsEngineParent.getHeight()-80-40;
                visibleHeight = _graphicsEngineParent.getHeight()-80;

            }else
            {
                visibleHeight = _graphicsEngineParent.getHeight()-80;
            }
        }
        // Anthony01005 <--
        _graphicsEngine.scrollRectInRendererCoordinatesToVisible(
            new Rectangle(visibleX, visibleY, visibleWidth, visibleHeight));
//        _graphicsEngine.setDragGraphicsMode(true);

        _graphicsEngine.setPreferredSize(new Dimension(_totalWidth, visibleHeight));
        _graphicsEngine.repaint();

        _horizontalScrollBar.setValue(0);
      }
      else if (profileInfo instanceof MultiDimensionProfileDiagnosticInfo)
        Assert.expect(false, "Multi dimension profiles are not presently supported!");
      else
        Assert.expect(false, "Unknown profile diagnostic info type!");
    }
  }

  /**
   * @author George A. David
   */
  private void createLayers()
  {
    int layerNumber  = JLayeredPane.DEFAULT_LAYER.intValue();
    _backgroundLayer = layerNumber++;
    _hiddenLayer = layerNumber++;
    _textLayer = layerNumber++;

    for (MeasurementRegionEnum measurementRegionEnum : MeasurementRegionEnum.getSetOfAllEnums())
    {
      int thisLayerNumber = layerNumber++;
      _measurementRegionEnumToGraphicsLayerNumberMap.put(measurementRegionEnum, thisLayerNumber);
    }
    _imageLayer = ++layerNumber;

    // the axis layer should be the last layer
    // so the graphics do not draw on top of this layer
    _axisLayer = ++layerNumber;

    _allGraphicLayers.add(_backgroundLayer);
    _allGraphicLayers.add(_hiddenLayer);
    _allGraphicLayers.add(_textLayer);
    _allGraphicLayers.add(_axisLayer);
    _allGraphicLayers.addAll(_measurementRegionEnumToGraphicsLayerNumberMap.values());
  }

  /**
   * @author George A. David
   */
  public void setColors()
  {
    _graphicsEngineForImageCreation.setForeground(_backgroundLayer, LayerColorEnum.PROFILE_BACKGROUND.getColor());
    _graphicsEngineForImageCreation.setForeground(_hiddenLayer, Color.lightGray);
    _graphicsEngineForImageCreation.setForeground(_axisLayer, LayerColorEnum.PROFILE_AXIS.getColor());
    _graphicsEngineForImageCreation.setForeground(_textLayer, LayerColorEnum.PROFILE_TEXT.getColor());

    for (MeasurementRegionEnum measurementRegionEnum : MeasurementRegionEnum.getSetOfAllEnums())
    {
      Integer graphicsLayerForMeasurementRegion = _measurementRegionEnumToGraphicsLayerNumberMap.get(
          measurementRegionEnum);
      if (graphicsLayerForMeasurementRegion != null)
      {
        Color measurementRegionColor = XRayLayerColorEnum.getColorForMeasurementRegionEnum(measurementRegionEnum);
        _graphicsEngineForImageCreation.setForeground(graphicsLayerForMeasurementRegion.intValue(), measurementRegionColor);
      }
    }
  }
}
