package com.axi.v810.gui.imagePane;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.algoTuner.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.plaf.basic.*;

/**
 * This class is the main panel for the image pane display during fine tuning.
 * @author George A. David
 */
public class FineTuningPanel extends JPanel implements Observer
{
  FineTuningImagePanel _fineTuningImagePanel;
//  JPanel _southPanel;
  JSplitPane _southSplitPane;
  //Anthony01005
  JSplitPane _fineTuningProfileGraphicSplitPane;
  FineTuningProfileGraphicPanel _fineTuningProfileGraphicPanel;
  FineTuningLegendGraphicPanel _fineTuningLegendGraphicPanel;
  private GuiObservable _guiObservable = GuiObservable.getInstance();

  private AlgoTunerGui _algoTuner = null;
  private boolean _isSplitPaneDragged = false;
  private final int UNCERTAINTY_SPLIT_PANE_HEIGHT = 12;
  /**
   * @author George A. David
   */
  public FineTuningPanel(ImageManagerToolBar toolBar)
  {
    _fineTuningImagePanel = new FineTuningImagePanel(toolBar);
    _fineTuningProfileGraphicPanel = new FineTuningProfileGraphicPanel();
    _fineTuningLegendGraphicPanel = new FineTuningLegendGraphicPanel();
    jbInit();
  }

  public void saveAlgoTunerInstance(AlgoTunerGui algoTunerGui)
  {
    _algoTuner = algoTunerGui;
  }

  /**
   * @author George A. David
   */
  private void jbInit()
  {
    setLayout(new BorderLayout());
    /** @todo erica, put your panel in here north */
    add(new JPanel(), BorderLayout.NORTH);
    /*
    add(_fineTuningImagePanel, BorderLayout.CENTER);

    _southSplitPane = new JSplitPane(
      JSplitPane.HORIZONTAL_SPLIT,
      _fineTuningLegendGraphicPanel,
      _fineTuningProfileGraphicPanel);
    _southSplitPane.setPreferredSize(new Dimension(100, 190));
    _southSplitPane.setDividerLocation(200);
    add(_southSplitPane, BorderLayout.SOUTH);

     */

    //Anthony01005-->
    _southSplitPane = new JSplitPane(
      JSplitPane.HORIZONTAL_SPLIT,
      _fineTuningLegendGraphicPanel,
      _fineTuningProfileGraphicPanel);
    _southSplitPane.setPreferredSize(new Dimension(100, 190));
    _southSplitPane.setDividerLocation(200);
    _fineTuningLegendGraphicPanel.setMinimumSize(new Dimension(200,180));
    _fineTuningProfileGraphicPanel.setMinimumSize(new Dimension(200,180));


      _fineTuningProfileGraphicSplitPane  = new JSplitPane(
      JSplitPane.VERTICAL_SPLIT,
      _fineTuningImagePanel,
      _southSplitPane);
    _fineTuningProfileGraphicSplitPane.setPreferredSize(new Dimension(300, 300));
    _fineTuningProfileGraphicSplitPane.setDividerLocation(800);

    add(_fineTuningProfileGraphicSplitPane, BorderLayout.CENTER);

    //Anthony01005
    _fineTuningImagePanel.setFineTuningPanel(this);
    //Anthony01005<--


    BasicSplitPaneUI paneUi = (BasicSplitPaneUI)_fineTuningProfileGraphicSplitPane.getUI();

    paneUi.getDivider().addMouseListener(new MouseAdapter() {

    public void mousePressed(MouseEvent e) {
         _isSplitPaneDragged = true;
    }

    });


    // diagnostics must be explicitly enabled by instantiating panel
    _southSplitPane.setVisible(false);
    _guiObservable.addObserver(this);
  }

  /**
   * @author George A. David
   */
  public FineTuningImagePanel getFineTuningImagePanel()
  {
    Assert.expect(_fineTuningImagePanel != null);

    return _fineTuningImagePanel;
  }
  /**
   * @author Anthony Fong //Anthony01005
   */
  public FineTuningProfileGraphicPanel getFineTuningProfileGraphicPanel()
  {
    Assert.expect(_fineTuningProfileGraphicPanel != null);

    return _fineTuningProfileGraphicPanel;
  }
  /**
   * @author Andy Mechtenberg
   */
  void unpopulate()
  {
    _fineTuningImagePanel.unpopulate();
    _fineTuningLegendGraphicPanel.unpopulate();
    _fineTuningProfileGraphicPanel.unpopulate();
  }

  /**
   * @author George Booth
   */
  void enableDiagnostics(boolean enabled)
  {
    if(enabled)
    {
      // XCR-2722 Assert in Fine Tuning When Run on Diagnostic - Cheah Lee Herng 14 May 2015
      // This is to make sure both _fineTuningProfileGraphicSplitPane and _algoTuner does not null
      if (_fineTuningProfileGraphicSplitPane != null && _algoTuner != null)
      {
        _fineTuningProfileGraphicSplitPane.setSize(new Dimension(_algoTuner.getFineTuningSpliterHeight(), 
                                                                 _algoTuner.getFineTuningSpliterLocation() + UNCERTAINTY_SPLIT_PANE_HEIGHT));
        _fineTuningProfileGraphicSplitPane.doLayout();
      }
      else
      {
        if (_fineTuningProfileGraphicSplitPane == null)
          System.out.println("FineTuningPanel : _fineTuningProfileGraphicSplitPane == null");
        
        if (_algoTuner == null)
          System.out.println("FineTuningPanel : _algoTuner == null");
      }
    }
    _southSplitPane.setVisible(enabled);
    validate();  // make sure it repaints
  }

  /**
   * @author George A. David
   */
  void display()
  {
    Assert.expect(_fineTuningImagePanel != null);

    _isSplitPaneDragged = false;

    //Anthony01005
    _fineTuningProfileGraphicPanel.getFineTuningProfileGraphicsEngineSetup().setEnableProfileZoom(_fineTuningImagePanel.getFineTuningImageGraphicsEngineSetup().getImageFitToScreen());
    _fineTuningImagePanel.display();
    _fineTuningLegendGraphicPanel.display();
    _fineTuningProfileGraphicPanel.display();
  }

  /**
   * @author George A. David
   */
  void finish()
  {
    Assert.expect(_fineTuningImagePanel != null);

    _fineTuningImagePanel.finish();
    _fineTuningLegendGraphicPanel.finish();
    _fineTuningProfileGraphicPanel.finish();

  }

  /**
   * @author George Booth
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    Assert.expect(observable != null);

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof GuiObservable)
        {
          final GuiEvent guiEvent = (GuiEvent)object;
          final GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
          if (guiEventEnum instanceof SelectionEventEnum)
          {
            SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
            if (selectionEventEnum.equals(SelectionEventEnum.INSPECTION_DIAGNOSTICS_SELECTION))
            {
              Boolean enabled = (Boolean)guiEvent.getSource();
              if(enabled == false && _isSplitPaneDragged == true)
              {
                  _algoTuner.saveFineTuningPanel(_fineTuningProfileGraphicSplitPane.getSize().height,
                                                 _fineTuningProfileGraphicSplitPane.getSize().width,
                                                 _fineTuningProfileGraphicSplitPane.getTopComponent().getSize().height);
                  _isSplitPaneDragged = false;
              }
              enableDiagnostics(enabled);
              _fineTuningImagePanel.setSaveAllSliceAndComponentPadButtonEnabled(false);
              //Ying-Huan.Chu
              _fineTuningImagePanel.setEnableSharpnessProfileButton(false);
              _fineTuningImagePanel.setAllowToggle2DImageButton(false);
              _fineTuningImagePanel.setEnablePrevious2DImageButton(false);
              _fineTuningImagePanel.setEnableNext2DImageButton(false);
           
			  // bee-hoon.goh
              _fineTuningImagePanel.setEnableToggleViewComponentImageButton(false);

              //Lim, Lay Ngor - XCR1652 - Image Post Processing
              //Always allow image enhancer button to be toggle even while run inspection.
//              _fineTuningImagePanel.setEnableImageEnhancerButton(false);    
            }
            if(selectionEventEnum.equals(SelectionEventEnum.INSPECTION_IMAGE_PAD_SELECTION))
            {
              _fineTuningImagePanel.setSaveAllSliceAndComponentPadButtonEnabled(true);
   
            }
          }
        }
      }
    });
  }
}
