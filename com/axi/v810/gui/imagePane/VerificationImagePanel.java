package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;

import com.axi.v810.hardware.*;
import com.axi.guiUtil.*;
import com.axi.util.*;

/**
 * This class is the main panel for the image pane display during fine tuning.
 * @author George A. David
 */
public class VerificationImagePanel extends JPanel
{
  private VerificationImageGraphicsEngineSetup _verificationImageGraphicsEngineSetup = null;
  private GraphicsEngine _graphicsEngine = null;
  
  /**
   * @author George A. David
   */
  VerificationImagePanel(ImageManagerToolBar toolBar)
  {
    _verificationImageGraphicsEngineSetup = new VerificationImageGraphicsEngineSetup(toolBar);
    _graphicsEngine = _verificationImageGraphicsEngineSetup.getGraphicsEngine();
    jbInit();
  }
  
  /**
   * @author Chong Wei Chin
   */
  VerificationImagePanel(ImageManagerToolBar toolBar, MagnificationEnum magnification)
  {
    Assert.expect(magnification != null);
    _verificationImageGraphicsEngineSetup = new VerificationImageGraphicsEngineSetup(toolBar, magnification);
    _graphicsEngine = _verificationImageGraphicsEngineSetup.getGraphicsEngine();
    jbInit();
  }

  /**
   * @author George A. David
   */
  private void jbInit()
  {
    setLayout(new BorderLayout());
    add(_graphicsEngine, BorderLayout.CENTER);
  }

  /**
   * @author Andy Mechtenberg
   */
  void unpopulate()
  {
    Assert.expect(_verificationImageGraphicsEngineSetup != null);
    _verificationImageGraphicsEngineSetup.unpopulate();
  }


  /**
   * @author George A. David
   */
  void display(boolean checkLatestestProgram)
  {
    Assert.expect(_verificationImageGraphicsEngineSetup != null);
    _verificationImageGraphicsEngineSetup.display(checkLatestestProgram);
  }

  /**
   * @author George A. David
   */
  public Point2D getOffsetRelativeToPanelInNanoMeters()
  {
    Assert.expect(_verificationImageGraphicsEngineSetup != null);
    return _verificationImageGraphicsEngineSetup.getOffsetRelativeToPanelInNanoMeters();
  }

  /**
   * @author George A. David
   */
  void finish()
  {
    Assert.expect(_verificationImageGraphicsEngineSetup != null);
    _verificationImageGraphicsEngineSetup.finish();
  }

  /**
   * @author George A. David
   */
  public VerificationImageGraphicsEngineSetup getVerificationImageGraphicsEngineSetup()
  {
    Assert.expect(_verificationImageGraphicsEngineSetup != null);

    return _verificationImageGraphicsEngineSetup;
  }
}
