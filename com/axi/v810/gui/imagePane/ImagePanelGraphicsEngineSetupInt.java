package com.axi.v810.gui.imagePane;

/**
 * This is the apis used by the ImageManagerToolBar. Any class that sets
 * up a graphics engine with the intent on using the ImageManagerToolBar
 * must implement this interface.
 *
 * @author George A. David
 */
public interface ImagePanelGraphicsEngineSetupInt
{
  /**
   * @author George A. David
   */
  public void zoomIn();

  /**
   * @author George A. David
   */
  public void zoomOut();

  /**
   * @author Anthony Fong //Anthony01005
   */
  public void setZoomFactor(double zoomFactor);

  /**
   * @author George A. David
   */
  public void setZoomRectangleMode(boolean zoomRectangleMode);

  /**
   * @author George A. David
   */
  public void setDragGraphicsMode(boolean dragGraphicsMode);

  /**
   * @author George A. David
   */
  public void setMeasurmentMode(boolean measurementMode);

  /**
   * @author Seng-Yew Lim
   */
  public void setImageStayPersistent(boolean isImageStayPersistent);

  /**
   * @author Anthony Fong //Anthony01005
   */
  public void setImageFitToScreen(boolean isImageFitToScreen);

  /**
   * @author George A. David
   */
  public void showTopSide();

  /**
   * @author George A. David
   */
  public void showBottomSide();

  /**
   * @author George A. David
   */
  public void resetGraphics();

  /**
   * @author George A. David
   */
  public void clearGraphics();

  /**
   * @author George A. David
   */
  public void showGraphics();

  /**
   * @author Scott Richardson
   */
  public void translateUpInZAxis();

  /**
   * @author Scott Richardson
   */
  public void translateDownInZAxis();
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setGroupSelectMode(boolean isGroupSelectMode);
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setSelectRegionMode(boolean isSelectRegionMode);
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setSelectRegionMode(boolean isSelectRegionMode, double maxImageRegionWidth, double maxImageRegionHeight);  
}
