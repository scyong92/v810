package com.axi.v810.gui.imagePane;

import java.awt.image.*;

/**
 * This class draws an inspection image
 * @author George A. David
 * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
 *    Clear code for centralization to ImageProcessingRenderer.
 */
public class InspectionImageRenderer extends ImageProcessingRenderer
{
  /**
   * @author George A. David
   * @author Patrick Lacz
   */
  public InspectionImageRenderer(com.axi.util.image.Image image)
  {
    super(image.getBufferedImage());
  }

  /**
   * @author Patrick Lacz
   */
  public InspectionImageRenderer(BufferedImage bufferedImage)
  {
   super(bufferedImage);   
  }

  /**
   * @author George A. David
   */
  protected void refreshData()
  {
    super.refreshData();
    //do nothing
  }
}
