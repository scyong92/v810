package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.util.*;

/**
 * <p>Title: GenerateImageSetWizardPrecisionSecondaryPanel/p>
 *
 * <p>Description: This panel is the pre main panel of the generate image set wizard.  This panel analyzes the learned
 * data to make sure users are not trying to generate a precision tuning image set too early, that is before the panel
 * has been fully learned and tuned.</p>
 *
 * <p>Copyright: Copyright (c) 2008</p>
 *
 * <p>Company: AXI</p>
 *
 * @author Laura Cormos
 */
public class GenerateImageSetWizardPrecisionSecondaryPanel extends WizardPanel
{
  private JPanel _mainPanel;
  private Project _project;

  private java.util.List<Subtype> _unlearnedShorts = new ArrayList<Subtype>(); // list of Subtypes that have no short learned data
  private java.util.List<Subtype> _unlearnedExpectedImages = new ArrayList<Subtype>(); // list of Subtypes that have no expected image learned data
  private java.util.List<Subtype> _unlearnedSubtypes = new ArrayList<Subtype>(); // list of Subtypes that have no subtype learned data

  private Border _border1 = BorderFactory.createEtchedBorder();
  private Border _titledBorder = new TitledBorder(_border1, StringLocalizer.keyToString("GISWK_STATUS_KEY"));

  private BorderLayout _mainPanelBorderLayout = new BorderLayout(5, 5);
  private JLabel _shortsLearningStatusLabel = new JLabel();
  private JLabel _subtypeLearningStatusLabel = new JLabel();
//  private JTextArea _subtypeLearningStatusTexArea = new JTextArea();


  private JLabel _expectedImageLearningStatusLabel = new JLabel();
  private JButton _subtypeLearningDetailsButton = new JButton();
  private JButton _shortsLearningDetailsButton = new JButton();
  private JButton _expectedImageLearningDetailsButton = new JButton();

  private ActionListener _subtypeLearnDetailButtonActionListener;
  private ActionListener _shortsLearnDetailButtonActionListener;
  private ActionListener _expectedImageLearnDetailButtonActionListener;

  private GenerateImageSetWizardDialog _dialog = null;
  private MainMenuGui _mainUI;

  /**
   * @author Laura Cormos
   */
  public GenerateImageSetWizardPrecisionSecondaryPanel(Project currentLoadedProject, ImageSetData imageSetData, WizardDialog dialog)
  {
    super(dialog);
    Assert.expect(currentLoadedProject != null, "project is null");
    Assert.expect(imageSetData != null, "image set data is null");
    Assert.expect(dialog != null, "Dialog is not null");
    _project = currentLoadedProject;
    _dialog = (GenerateImageSetWizardDialog)dialog;
    _mainUI = MainMenuGui.getInstance();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _unlearnedSubtypes.clear();
    _unlearnedShorts.clear();
    _project = null;
    _mainPanel = null;
  }

  /**
   * @author Laura Cormos
   */
  public void removeObservers()
  {
    // do nothing
  }


  /**
   * @author Laura Cormos
   */
  private void createPanel()
  {
    removeAll();
    setLayout(_mainPanelBorderLayout);
    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    _mainPanel = new JPanel();
    _mainPanel.setBorder(BorderFactory.createCompoundBorder(_titledBorder, BorderFactory.createEmptyBorder(5, 25, 5, 5)));
    _mainPanel.setLayout(new BorderLayout());
    add(_mainPanel, BorderLayout.CENTER);

    JPanel warningPanel = new JPanel(new BorderLayout());
    warningPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    JLabel warningLabel = new JLabel(StringLocalizer.keyToString("GISWK_WARNING_KEY"));
    warningLabel.setFont(FontUtil.getBoldFont(warningLabel.getFont(),Localization.getLocale()));
    warningPanel.add(warningLabel, BorderLayout.NORTH);

    JTextArea explanationText = new JTextArea();
    explanationText.setText(StringLocalizer.keyToString("GISWK_WARNING_TEXT_KEY"));
    explanationText.setEditable(false);
    explanationText.setLineWrap(true);
    explanationText.setWrapStyleWord(true);
    explanationText.setBackground((Color)UIManager.get("Label.background"));
    explanationText.setForeground((Color)UIManager.get("Label.foreground"));
    explanationText.setFont((Font)UIManager.get("Label.font"));
    warningPanel.add(explanationText, BorderLayout.CENTER);

    _mainPanel.add(warningPanel, BorderLayout.NORTH);

    JPanel learnStatusPanel = new JPanel(new BorderLayout());
    learnStatusPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    JLabel learnStatusLabel = new JLabel(StringLocalizer.keyToString("GISWK_LEARN_STATUS_KEY"));
    learnStatusLabel.setFont(FontUtil.getBoldFont(learnStatusLabel.getFont(),Localization.getLocale()));
    learnStatusPanel.add(learnStatusLabel, BorderLayout.NORTH);

    JPanel allLearningStatusPanel = new JPanel();
    VerticalFlowLayout subtypeLearnStatusPanelVerticalFlowLayout = new VerticalFlowLayout();
    allLearningStatusPanel.setLayout(subtypeLearnStatusPanelVerticalFlowLayout);

    JPanel subtypeLearningOuterPanel = new JPanel(new BorderLayout());
    JPanel shortLearningOuterPanel = new JPanel(new BorderLayout());
    JPanel expectedImageLearningOuterPanel = new JPanel(new BorderLayout());

    allLearningStatusPanel.add(subtypeLearningOuterPanel);
    allLearningStatusPanel.add(shortLearningOuterPanel);
    if (usesExpectedImages())
      allLearningStatusPanel.add(expectedImageLearningOuterPanel);

    subtypeLearningOuterPanel.add(new JLabel(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_ALGORITHM_SETTINGS_LEARNING_KEY") + ":"),
                                  BorderLayout.NORTH);
    shortLearningOuterPanel.add(new JLabel(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_SHORT_BACKGROUND_REGIONS_LEARNING_KEY") + ":"),
                                BorderLayout.NORTH);
    expectedImageLearningOuterPanel.add(new JLabel(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_EXPECTED_IMAGES_FOR_VOIDING_LEARNING_KEY") + ":"),
                                        BorderLayout.NORTH);

    JPanel subtypeLearnStatusAndDetailsPanel = new JPanel(new BorderLayout(40, 5));
    subtypeLearnStatusAndDetailsPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 5));
    JPanel shortsLearnStatusAndDetailsPanel = new JPanel(new BorderLayout(40, 5));
    shortsLearnStatusAndDetailsPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 5));
    JPanel expectedImageLearnStatusAndDetailsPanel = new JPanel(new BorderLayout(40, 5));
    expectedImageLearnStatusAndDetailsPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 5));

    subtypeLearningOuterPanel.add(subtypeLearnStatusAndDetailsPanel, BorderLayout.CENTER);
    shortLearningOuterPanel.add(shortsLearnStatusAndDetailsPanel, BorderLayout.CENTER);
    expectedImageLearningOuterPanel.add(expectedImageLearnStatusAndDetailsPanel, BorderLayout.CENTER);

    subtypeLearnStatusAndDetailsPanel.add(_subtypeLearningStatusLabel, BorderLayout.WEST);
    shortsLearnStatusAndDetailsPanel.add(_shortsLearningStatusLabel, BorderLayout.WEST);
    expectedImageLearnStatusAndDetailsPanel.add(_expectedImageLearningStatusLabel, BorderLayout.WEST);

    removeAllActionListeners();

    _subtypeLearningDetailsButton.setText(StringLocalizer.keyToString("MMGUI_DETAILS_KEY"));
    _subtypeLearnDetailButtonActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        subtypeLearningDetailsButton_actionPerformed();
      }
    };
    _shortsLearningDetailsButton.setText(StringLocalizer.keyToString("MMGUI_DETAILS_KEY"));
    _shortsLearnDetailButtonActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        shortLearningDetailsButton_actionPerformed();
      }
    };
    _expectedImageLearningDetailsButton.setText(StringLocalizer.keyToString("MMGUI_DETAILS_KEY"));
    _expectedImageLearnDetailButtonActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        expectedImageLearningDetailsButton_actionPerformed();
      }
    };

    subtypeLearnStatusAndDetailsPanel.add(_subtypeLearningDetailsButton, BorderLayout.CENTER);
    shortsLearnStatusAndDetailsPanel.add(_shortsLearningDetailsButton, BorderLayout.CENTER);
    expectedImageLearnStatusAndDetailsPanel.add(_expectedImageLearningDetailsButton, BorderLayout.CENTER);

    learnStatusPanel.add(allLearningStatusPanel, BorderLayout.CENTER);

    _mainPanel.add(learnStatusPanel, BorderLayout.CENTER);

    // add the radiobuttons to the panel for display

    // add the buttons to the radiobuttongroup

    // listeners
    addAllActionListeners();
  }


  /**
   * @author Laura Cormos
   */
  public void populatePanelWithData()
  {
    updateLearningStatusFields();
    createPanel();

    // set the title
    _dialog.changeTitle(StringLocalizer.keyToString("GISWK_SCREEN_2_TITLE_KEY"));

    // now call validate to see if the panel has all the info needed to go to the next step.
    validatePanel();
  }

  /**
   * This method will check to make sure all the data is specified and if so then will fire an update event to the
   * dialog to update the buttons correctly. This method will also specify the next and previous pointers
   *
   * @author Laura Cormos
   */
  public void validatePanel()
  {
    WizardButtonState buttonState = new WizardButtonState(true, true, false, true);
    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Laura Cormos
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null, "Name of panel is null");
    Assert.expect(name.length() > 0, "Name of panel is an empty string");
    _name = name;
  }

  /**
   * @author Laura Cormos
   */
  public String getWizardName()
  {
    Assert.expect(_name != null, "Name of panel is not set");
    return _name;
  }

  /**
    * @author Andy Mechtenberg
    * @author Laura Cormos
    */
   private void updateLearningStatusFields()
   {
     com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
     int totalSubtypes = panel.getNumberOfInspectedSubtypes();
     int shortLearnedSubtypes = panel.getNumberOfInspectedSubtypesWithShortLearned();
     int subtypeLearnedSubtypes = panel.getNumberOfInspectedSubtypesWithAllAlgorithmsExceptShortLearned();
     int totalSubtypesWithExpectedImages = panel.getNumberOfInspectedSubtypesWithExpectedImageLearning();
     int expectedImageLearnedSubtypes = panel.getNumberOfInspectedSubtypesWithExpectedImageLearned();


     LocalizedString shortLearnState = new LocalizedString("MMGUI_INITIAL_TUNING_LEARNING_STATE_KEY",
                                                           new Object[]{new Integer(shortLearnedSubtypes),
                                                           new Integer(totalSubtypes)});
     LocalizedString subtypeLearnState = new LocalizedString("MMGUI_INITIAL_TUNING_LEARNING_STATE_KEY",
                                                             new Object[]{new Integer(subtypeLearnedSubtypes),
                                                             new Integer(totalSubtypes)});
     LocalizedString expectedImageLearnState = new LocalizedString("MMGUI_INITIAL_TUNING_LEARNING_STATE_KEY",
                                                             new Object[]{new Integer(expectedImageLearnedSubtypes),
                                                             new Integer(totalSubtypesWithExpectedImages)});
     _subtypeLearningStatusLabel.setText(StringLocalizer.keyToString(subtypeLearnState));
     if (panel.areAlgorithmUnlearnedSubtypesNotUsingPredictiveSliceHeight())
       _subtypeLearningStatusLabel.setForeground(Color.RED);

     _shortsLearningStatusLabel.setText(StringLocalizer.keyToString(shortLearnState));
     if (panel.areShortsUnlearnedSubtypesNotUsingPredictiveSliceHeight())
       _shortsLearningStatusLabel.setForeground(Color.RED);

//     _subtypeLearningStatusTexArea.setText(StringLocalizer.keyToString(subtypeLearnState));
//     _subtypeLearningStatusTexArea.setEditable(false);
//     _subtypeLearningStatusTexArea.setLineWrap(true);
//     _subtypeLearningStatusTexArea.setWrapStyleWord(true);
//     _subtypeLearningStatusTexArea.setBackground((Color)UIManager.get("Label.background"));
//     _subtypeLearningStatusTexArea.setForeground((Color)UIManager.get("Label.foreground"));
//     _subtypeLearningStatusTexArea.setFont((Font)UIManager.get("Label.font"));

     _expectedImageLearningStatusLabel.setText(StringLocalizer.keyToString(expectedImageLearnState));
     if (panel.areExpectedImageUnlearnedSubtypesNotUsingPredictiveSliceHeight())
       _expectedImageLearningStatusLabel.setForeground(Color.RED);

     _unlearnedShorts = panel.getInspectedSubtypesWithoutShortLearned();
     _unlearnedSubtypes = panel.getInspectedSubtypesWithoutAllAlgorithmsExceptShortLearned();
     _unlearnedExpectedImages = panel.getInspectedSubtypesWithoutExpectedImagesLearned();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void shortLearningDetailsButton_actionPerformed()
  {
    // bring up dialog with list of all untuned subtypes
    ProjectDetailsDialog dlg = new ProjectDetailsDialog(_mainUI,
                                                        StringLocalizer.keyToString("MMGUI_UNLEARNED_SHORT_BACKGROUND_REGIONS_KEY"),
                                                        _unlearnedShorts);
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    dlg.dispose();
  }

  /**
   * @author George Booth
   */
  private void expectedImageLearningDetailsButton_actionPerformed()
  {
    // bring up dialog with list of all untuned subtypes
    ProjectDetailsDialog dlg = new ProjectDetailsDialog(_mainUI,
                                                        StringLocalizer.keyToString("MMGUI_UNLEARNED_EXPECTED_IMAGES_KEY"),
                                                        _unlearnedExpectedImages);
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    dlg.dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void subtypeLearningDetailsButton_actionPerformed()
  {
    // bring up dialog with list of all untuned subtypes
    ProjectDetailsDialog dlg = new ProjectDetailsDialog(_mainUI,
                                                        StringLocalizer.keyToString("MMGUI_UNLEARNED_ALGORITHM_SETTINGS_KEY"),
                                                        _unlearnedSubtypes);
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    dlg.dispose();
  }

  /**
   * @author Laura Cormos
   */
  private boolean usesExpectedImages()
  {
    // only show expected image controls if an ExposedPad joint type is active
    java.util.List<JointTypeEnum> jointTypes = _project.getPanel().getInspectedJointTypeEnums();
    for (JointTypeEnum jte : jointTypes)
    {
      if (jte.getName().equals(JointTypeEnum.EXPOSED_PAD.getName()))
        return true;
    }
    return false;
  }

  /**
   * @author Laura Cormos
   */
  private void addAllActionListeners()
  {
    _subtypeLearningDetailsButton.addActionListener(_subtypeLearnDetailButtonActionListener);
    _shortsLearningDetailsButton.addActionListener(_shortsLearnDetailButtonActionListener);
    _expectedImageLearningDetailsButton.addActionListener(_expectedImageLearnDetailButtonActionListener);
  }

  /**
   * @author Laura Cormos
   */
  private void removeAllActionListeners()
  {
    _subtypeLearningDetailsButton.removeActionListener(_subtypeLearnDetailButtonActionListener);
    _shortsLearningDetailsButton.removeActionListener(_shortsLearnDetailButtonActionListener);
    _expectedImageLearningDetailsButton.removeActionListener(_expectedImageLearnDetailButtonActionListener);
  }
}
