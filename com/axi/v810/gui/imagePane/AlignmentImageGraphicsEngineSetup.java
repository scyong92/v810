package com.axi.v810.gui.imagePane;

import java.awt.geom.*;
import java.awt.Shape;
import java.util.regex.*;
import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import static com.axi.v810.gui.mainMenu.MainMenuGui.finishLicenseMonitor;

/**
 * The AlignmentImageGraphicsEngineSetup class sets up the graphics engine
 * for image display during manual alignment.  This instance of the
 * graphics image displays pad outlines over the image and supports dragging
 * the pad outlines to align them with the image.
 *
 * @author Kay Lannen
 * @author George A. David
 */
public class AlignmentImageGraphicsEngineSetup extends Observable implements Observer, ImagePanelGraphicsEngineSetupInt, RendererCreator
{
  private static final int _BORDER_SIZE_IN_PIXELS = 50;
  private static final double _ZOOM_FACTOR = 1.2;
//  private static final int _PAD_VISIBILITY_THRESHOLD = MathUtil.convertMilsToNanoMetersInteger(30) / MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
  private static final int _PAD_VISIBILITY_THRESHOLD = 1000;
  private static final int _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(3000);

  private GraphicsEngine _graphicsEngine;
  private boolean _alignmentInProcess = false;

  // Graphic engine layers
//  private int _rightVerificationImageLayer;
//  private int _leftVerificationImageLayer;
//  private int _rightManualAlignmentPadLayer;
//  private int _leftManualAlignmentPadLayer;
  // Wei Chin : fix the maximum layer board to handle the graphic update properly
  // edited by : Kee, Chin Seong - make it dynamic so that the UI dragging wont lag
  private int _MAXNUMBER_OF_LAYER_FOR_MULTIBOARD = 300; 

  protected Map<Integer, Integer> _subProgramIdToVerificationImageLayer = new HashMap<Integer,Integer>();
  private Map<Integer, Integer> _subProgramIdToManualAlignmentPadLayer = new HashMap<Integer,Integer>();

//  private java.util.List <Integer> _subProgramIdToVerificationImageLayer;
//  private java.util.List <Integer> _subProgramIdToManualAlignmentPadLayer;
  private int _reconstructedAlignmentImageLayer;
  private int _locatedAlignmentPadLayer;
  private int _locatedAlignmentContextPadLayer;

  private InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
  private ImagePaneRendererCreator _rendererCreator;
  private Project _project;

  private AffineTransform _nanoMetersToPixelTransform = AffineTransform.getScaleInstance(1.0 / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel(),
                                                                                         1.0 / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
  
  private boolean _showingTopSide = true;
  Collection<Integer> _layersToUseForDragging = new LinkedList<Integer>();
  public boolean _isInitialized = false;
  private ImageManagerToolBar _toolBar;
  private Alignment _alignment = Alignment.getInstance();
  private double _prevAlignmentXoffsetInNanoMeters = 0;
  private double _prevAlignmentYoffsetInNanoMeters = 0;
  private double _prevAlignmentXoffsetInNanoMeters2 = 0;
  private double _prevAlignmentYoffsetInNanoMeters2 = 0;

  private boolean _verifyingManualAlignment = false;
  
  private MagnificationEnum _magnification = MagnificationEnum.NOMINAL;
  private List<Integer> currentTestSubProgramIds = new ArrayList();
  
  //Khaw Chek Hau - XCR2285: 2D Alignment on v810
  private int _selectRegionAreaLayer;
  private Shape _selectedFocusRegion;
  private com.axi.guiUtil.Renderer _draggedRenderer;
  private AlignmentGroup _currentAlignmentGroup = null;
  private static final Pattern _alignment2DImageFileNamePattern = Pattern.compile(FileName.getAlignment2DImageFileNamePattern());
  private static final double _M11_ZOOM_FACTOR = 8.95;
  private static final double _M13_ZOOM_FACTOR = 7.50;
  private static final double _M19_ZOOM_FACTOR = 5.15;
  private static final double _M23_ZOOM_FACTOR = 4.20;
  private static final String _Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED_FOR_M13 = "6157895000";
  
  /**
   * This constructor creates a new graphics engine.
   * @author Kay Lannen
   * @author George A. David
   */
  public AlignmentImageGraphicsEngineSetup(ImageManagerToolBar toolBar)
  {
    Assert.expect(toolBar != null);

    _toolBar = toolBar;
    _graphicsEngine = new GraphicsEngine(_BORDER_SIZE_IN_PIXELS);
    _graphicsEngine.setShouldGraphicsBeFitToScreen(false);
    _inspectionEventObservable.addObserver(this);
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _graphicsEngine.getSelectedAreaObservable().addObserver(this);
//    createLayers();
  }
  
  /**
   * This constructor creates a new graphics engine.
   * @author Chong, Wei Chin
   */
  public AlignmentImageGraphicsEngineSetup(ImageManagerToolBar toolBar, MagnificationEnum magnification)
  {
    Assert.expect(toolBar != null);
    Assert.expect(magnification != null);

    _toolBar = toolBar;
    _magnification = magnification;
    
    _nanoMetersToPixelTransform = AffineTransform.getScaleInstance(1.0 / (double)_magnification.getNanoMetersPerPixel(),
                                                                                         1.0 / (double)_magnification.getNanoMetersPerPixel());
    _graphicsEngine = new GraphicsEngine(_BORDER_SIZE_IN_PIXELS);
    _graphicsEngine.setShouldGraphicsBeFitToScreen(false);
    _inspectionEventObservable.addObserver(this);
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _graphicsEngine.getSelectedAreaObservable().addObserver(this);
//    createLayers();
  }

  /**
   * @author George A. David
   */
  private void setUpToolBar()
  {
    _toolBar.setAllowDragGraphics(true);
    _toolBar.setAllowMeasurements(true);
    _toolBar.setAllowResetGraphics(false);
    _toolBar.setAllowToggleGraphics(true);
    _toolBar.setAllowToggleImageStay(false);
    _toolBar.setAllowToggleImageFitToScreen(true);

    _toolBar.setAllowViewBottomSide(false);
    _toolBar.setAllowViewTopSide(false);

    _toolBar.setAllowZoomIn(false);
    _toolBar.setAllowZoomOut(false);
    _toolBar.setAllowZoomRectangle(false);
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _toolBar.setEnableSelectRegionToggleButon(true);
  }

  /**
   * @author George A. David
   */
  private void setToolBarEnabled(boolean enabled)
  {
    _toolBar.setAllowDragGraphics(enabled);
    _toolBar.setAllowMeasurements(enabled);
    _toolBar.setAllowToggleGraphics(enabled);
    _toolBar.setAllowZoomIn(enabled);
    _toolBar.setAllowZoomOut(enabled);
    _toolBar.setAllowZoomRectangle(enabled);
    _toolBar.setAllowMeasurements(enabled);
    _toolBar.setAllowToggleImageFitToScreen(enabled);
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _toolBar.setEnableSelectRegionToggleButon(enabled);
  }

  /**
   * @author George A. David
   */
  public void display()
  {
    setUpToolBar();
    setToolBarEnabled(false);
    _isInitialized = false;
  }
  
  /*
   * @Kee Chin Seong - Fix for Crash during layer setup
   */
  private java.util.List<Integer> getAllLayers()
  {
    java.util.List <Integer> allLayers = new ArrayList<Integer>();
    allLayers.addAll(_subProgramIdToVerificationImageLayer.values());
    allLayers.addAll(_subProgramIdToManualAlignmentPadLayer.values());
    return allLayers;
  }
  
  /*
   * @Kee Chin Seong - Fix for Crash during layer setup
   */
  private void clearAllLayers()
  {
    java.util.List<Integer> allLayers = getAllLayers();
    for(Integer layerNumber : allLayers)
    {
      _graphicsEngine.removeLayer(layerNumber.intValue());
    }
    _subProgramIdToVerificationImageLayer.clear();
    _subProgramIdToManualAlignmentPadLayer.clear();
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _graphicsEngine.removeRenderers(_selectRegionAreaLayer);
  }

  /**
   * @author George A. David
   */
  private void initialize(PanelLocationInSystemEnum panelLocation)
  {
    if(_isInitialized)
      return;
    
    currentTestSubProgramIds.clear();
    _project = Project.getCurrentlyLoadedProject();
    _graphicsEngine.reset();
    
    
    /*
    * @Kee Chin Seong - Fix for Crash during layer setup
    */
//    if(_MAXNUMBER_OF_LAYER_FOR_MULTIBOARD < _project.getTestProgram().getSizeOfAllTestSubProgramsForGraphicEngineLayersSetup() + 1)
//    {
      //always clear layers first before add more layers
//      _MAXNUMBER_OF_LAYER_FOR_MULTIBOARD = _project.getTestProgram().getSizeOfAllTestSubProgramsForGraphicEngineLayersSetup() + 1;
      clearAllLayers();
      createLayers();
//    }
      
    MathUtilEnum displayUnits = _project.getDisplayUnits();
    if(displayUnits.equals(MathUtilEnum.NANOMETERS) == false)
    _graphicsEngine.setMeasurementInfo(MathUtil.convertUnits(_magnification.getNanoMetersPerPixel(), MathUtilEnum.NANOMETERS, displayUnits),
                                       MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits),
                                       MeasurementUnits.getMeasurementUnitString(displayUnits));

    boolean checkLatestestProgram = true;
    _rendererCreator = new ImagePaneRendererCreator(_project, _magnification, checkLatestestProgram, panelLocation);
    _rendererCreator.setSelectedColor(LayerColorEnum.VERIFY_CAD_SELECTED_PAD_COLOR.getColor());
    _rendererCreator.setImageLayer(_subProgramIdToVerificationImageLayer);
    _rendererCreator.setPadLayer(_subProgramIdToManualAlignmentPadLayer);

    // ok, now drag to layers to take into account alignment.
    for(TestSubProgram subProgram : _project.getTestProgram().getAllTestSubPrograms())
    {
      if(_magnification.equals(MagnificationEnum.NOMINAL) && subProgram.getMagnificationType().equals(MagnificationTypeEnum.HIGH))
      {
        continue;
      }
      
      if(_magnification.equals(MagnificationEnum.H_NOMINAL) && subProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
      {
        continue;
      }
        
      AffineTransform pixelToNanoMetersTransform = null;
      try
      {
        pixelToNanoMetersTransform = _nanoMetersToPixelTransform.createInverse();
      }
      catch (NoninvertibleTransformException ex)
      {
        Assert.expect(false);
      }
      AffineTransform reverseYtransform = AffineTransform.getScaleInstance(-1, -1);
    
      int padLayer = 0;
      AffineTransform alignmentTransform = subProgram.getCoreRuntimeAlignmentTransform();
      padLayer = _subProgramIdToManualAlignmentPadLayer.get(subProgram.getId());

      AffineTransform transform = new AffineTransform(pixelToNanoMetersTransform);
      transform.preConcatenate(alignmentTransform);
      transform.preConcatenate(_nanoMetersToPixelTransform);
      _graphicsEngine.setInitialTransformForLayer(padLayer, transform);
        
      if (subProgram.getPanelLocationInSystem().equals(panelLocation))
      {
        currentTestSubProgramIds.add(subProgram.getId());
      }
    }

    Rectangle2D maxRegionRect = _nanoMetersToPixelTransform.createTransformedShape(new Rectangle2D.Double(0, 0, _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS, _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS)).getBounds2D();
    RegionOfInterestRenderer renderer = new RegionOfInterestRenderer(maxRegionRect);
//    _graphicsEngine.addRenderer(_rightManualAlignmentPadLayer, renderer);
    
    for( int verificationImageLayer : _subProgramIdToVerificationImageLayer.values())
    {
      _graphicsEngine.addRenderer(verificationImageLayer, renderer);
    }
      _graphicsEngine.setAxisInterpretation(true, false);
      _graphicsEngine.fitGraphicsToScreen();
      _graphicsEngine.removeRenderer(renderer);
    
    setColorsAndThreshold();

    _graphicsEngine.setDragGraphicsMode(true);

    _graphicsEngine.setLayersToUseWhenDraggingGraphics(_layersToUseForDragging);
    _graphicsEngine.repaint();
    _isInitialized = true;

    _toolBar.setAllowMeasurements(true);
//    _toolBar.setMeasureToggleButtonSelected(false);
    _toolBar.setDragToggleButtonSelected(true);
    _toolBar.setAllowToggleGraphics(true);
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _toolBar.setEnableSelectRegionToggleButon(true);
  }

  /**
   * Returns the graphics engine.
   * @author Kay Lannen
   * @author George A. David
   */
  public GraphicsEngine getGraphicsEngine()
  {
    Assert.expect(_graphicsEngine != null);

    return _graphicsEngine;
  }

  /**
   * @author George A. David
   */
  public void beginAlignment()
  {
    _inspectionEventObservable.addObserver(this);
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _graphicsEngine.getSelectedAreaObservable().addObserver(this);
  }

  /**
   * @author George A. David
   */
  public void endAlignment()
  {
    _inspectionEventObservable.deleteObserver(this);
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _graphicsEngine.getSelectedAreaObservable().deleteObserver(this);
  }

  /**
   * @author George A. David
   */
  public void display(AlignmentInspectionEvent alignmentInspectionEvent)
  {
    Assert.expect(alignmentInspectionEvent != null);

    _alignmentInProcess = true;

    AlignmentGroup group = alignmentInspectionEvent.getAlignmentGroup();
    _showingTopSide = group.isTopSide();
    _rendererCreator.clearCadRenderers(_graphicsEngine);
    // ClearCadRenderers does not guarantee clear all CAD renderers, need to specifically remove all renderers in that layer.
    // MUST BE SOMEWHERE ADD RENDERER WITHOUT USING AddRenderer METHOD !!!!!
    TestSubProgram subProgram = _project.getTestProgram().getTestSubProgram(group, alignmentInspectionEvent.getMagnificationType());
    
    _graphicsEngine.removeRenderers(_subProgramIdToManualAlignmentPadLayer.get(subProgram.getId()));
    _rendererCreator.setSelectedPads(group.getPads());
    _rendererCreator.setSelectedFiducials(group.getFiducials());
    Rectangle2D expectedBounds = null;
    for(Pad pad : group.getPads())
    {
      if(expectedBounds == null)
      {
        expectedBounds = pad.getShapeRelativeToPanelInNanoMeters().getBounds2D();
      }
      else
      {
        expectedBounds.add(pad.getShapeRelativeToPanelInNanoMeters().getBounds2D());
      }
    }

    for(Fiducial fiducial : group.getFiducials())
    {
      if(expectedBounds == null)
      {
        expectedBounds = fiducial.getShapeRelativeToPanelInNanoMeters().getBounds2D();
      }
      else
      {
        expectedBounds.add(fiducial.getShapeRelativeToPanelInNanoMeters().getBounds2D());
      }
    }

    MainMenuGui.getInstance().generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_ALIGNMENT_NEEDS_TEST_PROGRAM_KEY"),
                                                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                                    _project);

    _rendererCreator.clearRenderers(_graphicsEngine);
    _rendererCreator.setPadLayer(_subProgramIdToManualAlignmentPadLayer);

    Rectangle2D alignedBounds = _alignment.getCorrectedPositionInPanelCoordinates(expectedBounds, subProgram).getBounds2D();
    _prevAlignmentXoffsetInNanoMeters = expectedBounds.getCenterX() - alignedBounds.getCenterX();
    _prevAlignmentYoffsetInNanoMeters = expectedBounds.getCenterY() - alignedBounds.getCenterY();

    PanelRectangle regionToDrawPads = new PanelRectangle(alignedBounds);
    regionToDrawPads.setRect(alignedBounds.getCenterX() - (_MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS / 2.0),
                             alignedBounds.getCenterY() - (_MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS / 2.0),
                             _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS,
                             _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS);


    alignedBounds = _nanoMetersToPixelTransform.createTransformedShape(alignedBounds).getBounds2D();

    alignedBounds.setRect(alignedBounds.getMinX() - 50,
                          alignedBounds.getMinY() - 50,
                          alignedBounds.getWidth() + 100,
                          alignedBounds.getHeight() + 100);

//    for(int _manualAlignmentPadLayer : _subProgramIdToManualAlignmentPadLayer.values())
//      _graphicsEngine.setVisible(_manualAlignmentPadLayer, true);
    clearGraphics();
    _graphicsEngine.setVisible(_subProgramIdToManualAlignmentPadLayer.get(subProgram.getId()), true);
    _graphicsEngine.setVisible(_subProgramIdToVerificationImageLayer.get(subProgram.getId()), true);
   
    _toolBar.setAllowToggleGraphics(true);
    _graphicsEngine.fitRectangleInRendererCoordinatesToScreen(alignedBounds);
    _rendererCreator.createCadRenderers(_graphicsEngine, regionToDrawPads, _showingTopSide, subProgram.getId());
    _graphicsEngine.mark();
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void display2D(AlignmentInspectionEvent alignmentInspectionEvent)
  {
    Assert.expect(alignmentInspectionEvent != null);

    _alignmentInProcess = true;

    AlignmentGroup group = alignmentInspectionEvent.getAlignmentGroup();
    _showingTopSide = group.isTopSide();
    _rendererCreator.clearCadRenderers(_graphicsEngine);
    // ClearCadRenderers does not guarantee clear all CAD renderers, need to specifically remove all renderers in that layer.
    // MUST BE SOMEWHERE ADD RENDERER WITHOUT USING AddRenderer METHOD !!!!!
    TestSubProgram subProgram = _project.getTestProgram().getTestSubProgram(group, alignmentInspectionEvent.getMagnificationType());
    
    _graphicsEngine.removeRenderers(_subProgramIdToManualAlignmentPadLayer.get(subProgram.getId()));
    _graphicsEngine.removeRenderers(_selectRegionAreaLayer);
    
    //Check for any 2D Alignment learned
    String alignment2DPath = Directory.getAlignment2DImagesDir(_project.getName());
    String alignmentFileName = null;

    if (FileUtilAxi.exists(alignment2DPath))
    {
      String magnification = null;

      if (subProgram.getMagnificationType() == MagnificationTypeEnum.LOW)
      {
        if (Config.getSystemCurrentLowMagnification().equals("M19"))
          magnification = "M19";   
        else if (Config.getSystemCurrentLowMagnification().equals("M23"))
          magnification = "M23"; 
      }
      else
      {
        magnification = "M11";  
      }

      String fileInDirectory = null;
      java.io.File folderToScan = new java.io.File(alignment2DPath); 
      java.io.File[] listOfFiles = folderToScan.listFiles();

      for (java.io.File file : listOfFiles) 
      {
        if (file.isFile()) 
        { 
          fileInDirectory = file.getName();
          Matcher matcher = _alignment2DImageFileNamePattern.matcher(fileInDirectory);
          matcher.reset(fileInDirectory); 
          
          if (matcher.find()) 
          {
            if (fileInDirectory.contains(magnification + "_G" + group.getName()))
            {
              alignmentFileName = fileInDirectory;
              break;
            }
          }
        }
      } 

      if (alignmentFileName != null)
      {
        String[] alignmentImageInfoArray = alignmentFileName.split("G" + group.getName());
        String alignmentImageInfo = alignmentImageInfoArray[1]; 

        String[] strArray = null;      
        strArray = ((String)alignmentImageInfo).split("_");

        Assert.expect(strArray[1].matches("^[+-]?\\d+$") && strArray[2].matches("^[+-]?\\d+$") 
                      && strArray[3].matches("^[+-]?\\d+$") && strArray[4].matches("^[+-]?\\d+$"),
                      "The variable is not integer, the file name is: " + (String)alignmentFileName);

        int draggedImageExpectedMinX = Integer.parseInt(strArray[1]);
        int draggedImageExpectedMinY = Integer.parseInt(strArray[2]);
        int draggedImageWidth = Integer.parseInt(strArray[3]);
        int draggedImageHeight = Integer.parseInt(strArray[4]);
        int draggedImageActualMinX = Integer.parseInt(strArray[5]);
        int draggedImageActualMinY = Integer.parseInt(strArray[6]);
        
        java.awt.Shape shape = (java.awt.Shape) new Rectangle2D.Double(draggedImageActualMinX,draggedImageActualMinY,draggedImageWidth,draggedImageHeight);
        shape = _nanoMetersToPixelTransform.createTransformedShape(shape);
        AreaRenderer alignmentRegionRenderer = new AreaRenderer(new Area(shape),false);
        alignmentRegionRenderer.setFillShape(false);
        
        _graphicsEngine.addRenderer(_selectRegionAreaLayer, alignmentRegionRenderer);
        _selectedFocusRegion = shape;
        _draggedRenderer = alignmentRegionRenderer;
        group.setHasDraggedRegion(true);
        
        group.setDraggedExpectedMinX(draggedImageExpectedMinX);
        group.setDraggedExpectedMinY(draggedImageExpectedMinY);
        group.setDraggedWidth(draggedImageWidth);
        group.setDraggedHeight(draggedImageHeight);
        group.setDraggedActualMinX(draggedImageActualMinX);
        group.setDraggedActualMinY(draggedImageActualMinY);
      }
    }
    
    _rendererCreator.setSelectedPads(group.getPads());
    _rendererCreator.setSelectedFiducials(group.getFiducials());
    Rectangle2D expectedBounds = null;
    Rectangle2D expected2DRegionBounds = null;
    Rectangle2D alignedBounds = null;
          
    for(Pad pad : group.getPads())
    {
      if(expectedBounds == null)
      {
        expectedBounds = pad.getShapeRelativeToPanelInNanoMeters().getBounds2D();
      }
      else
      {
        expectedBounds.add(pad.getShapeRelativeToPanelInNanoMeters().getBounds2D());
      }
    }

    for(Fiducial fiducial : group.getFiducials())
    {
      if(expectedBounds == null)
      {
        expectedBounds = fiducial.getShapeRelativeToPanelInNanoMeters().getBounds2D();
      }
      else
      {
        expectedBounds.add(fiducial.getShapeRelativeToPanelInNanoMeters().getBounds2D());
      }
    }
    
    if (alignmentFileName != null)
    {
      java.awt.Shape shape = (java.awt.Shape) new Rectangle2D.Double(group.getDraggedExpectedMinX(),group.getDraggedExpectedMinY(),group.getDraggedWidth(),group.getDraggedHeight());  
      expected2DRegionBounds = shape.getBounds2D();
    }
    else
      expected2DRegionBounds = expectedBounds;

    MainMenuGui.getInstance().generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_ALIGNMENT_NEEDS_TEST_PROGRAM_KEY"),
                                                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                                    _project);

    _rendererCreator.clearRenderers(_graphicsEngine);
    _rendererCreator.setPadLayer(_subProgramIdToManualAlignmentPadLayer);

    alignedBounds = _alignment.getCorrectedPositionInPanelCoordinates(expected2DRegionBounds, subProgram).getBounds2D();
    _prevAlignmentXoffsetInNanoMeters = expected2DRegionBounds.getCenterX() - alignedBounds.getCenterX();
    _prevAlignmentYoffsetInNanoMeters = expected2DRegionBounds.getCenterY() - alignedBounds.getCenterY();

    alignedBounds = _alignment.getCorrectedPositionInPanelCoordinates(expectedBounds, subProgram).getBounds2D();
    
    PanelRectangle regionToDrawPads = new PanelRectangle(alignedBounds);
    regionToDrawPads.setRect(alignedBounds.getCenterX() - (_MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS / 2.0),
                             alignedBounds.getCenterY() - (_MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS / 2.0),
                             _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS,
                             _MAX_VISIBLE_RECTANGLE_SIZE_IN_NANOMETERS);


    alignedBounds = _nanoMetersToPixelTransform.createTransformedShape(alignedBounds).getBounds2D();
    
    alignedBounds.setRect(alignedBounds.getMinX() - 50,
                          alignedBounds.getMinY() - 50,
                          alignedBounds.getWidth() + 100,
                          alignedBounds.getHeight() + 100);

    clearGraphics();
    _graphicsEngine.setVisible(_subProgramIdToManualAlignmentPadLayer.get(subProgram.getId()), true);
    _graphicsEngine.setVisible(_subProgramIdToVerificationImageLayer.get(subProgram.getId()), true);
   
    _toolBar.setAllowToggleGraphics(true);
    _graphicsEngine.fitRectangleInRendererCoordinatesToScreen(alignedBounds);
//    _graphicsEngine.resetZoom();
    _rendererCreator.createCadRenderers(_graphicsEngine, regionToDrawPads, _showingTopSide, subProgram.getId());   
    _graphicsEngine.mark();
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  private void handleSelectedRegionArea(final Shape selectedRegion)
  {
    Assert.expect(selectedRegion != null);
    
    _graphicsEngine.clearCrossHairs();
    _graphicsEngine.removeRenderers(_selectRegionAreaLayer);
    java.awt.Shape shape = (java.awt.Shape) selectedRegion;

    if(shape.getBounds2D().getWidth() != 0 && shape.getBounds().getHeight() != 0)
    {
      AreaRenderer selectedFocusRegionRenderer = new AreaRenderer(new Area(shape),false);
      selectedFocusRegionRenderer.setFillShape(false);
      _graphicsEngine.addRenderer(_selectRegionAreaLayer, selectedFocusRegionRenderer);
      _draggedRenderer = selectedFocusRegionRenderer;
      _selectedFocusRegion = shape;
      _currentAlignmentGroup.setHasDraggedRegion(true);
    }
  }
   
  /**
   * @author Kay Lannen
   * @author George A. David
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    // This is outside of the run to keep track of the fact that this method may use
    // any images on the Inspection Event
    if (argument instanceof InspectionEvent)
    {
      InspectionEvent event = (InspectionEvent)argument;
      event.incrementReferenceCount();
    }
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        //Khaw Chek Hau - XCR2285: 2D Alignment on v810
        if (observable instanceof SelectedAreaObservable)
        {
          if(argument instanceof Shape)
          {
            handleSelectedRegionArea((Shape)argument);
          }
        }
        else if (observable instanceof InspectionEventObservable)
        {
          if (argument instanceof AlignmentInspectionEvent)
          {
            AlignmentInspectionEvent alignmentInspectionEvent = (AlignmentInspectionEvent)argument;
            InspectionEventEnum eventEnum = alignmentInspectionEvent.getInspectionEventEnum();
            if (_alignmentInProcess)
            {
              if (eventEnum.equals(InspectionEventEnum.MANUAL_ALIGNMENT_COMPLETED))
              {
                if( (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.LOW) && 
                   _magnification.equals(MagnificationEnum.H_NOMINAL) ) ||
                    (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.HIGH) && 
                   _magnification.equals(MagnificationEnum.NOMINAL) )   )
                  return;
                
                _alignmentInProcess = false;
                _rendererCreator.clearRenderers(_graphicsEngine);
                _graphicsEngine.setDragGraphicsMode(false);
                setToolBarEnabled(false);
                _toolBar.setDragToggleButtonSelected(true);
                try 
                {
                  if (LicenseManager.isOfflineProgramming() == false)
                  {
                    _toolBar.setAllowGenerateImageSetWizard(true);
                  }
                  else
                  {
                    _toolBar.setAllowGenerateImageSetWizard(false);
                  }
                }
                catch (final BusinessException e)
                {
                  finishLicenseMonitor();
                  SwingUtils.invokeLater(new Runnable()
                  {
                    public void run()
                    {
                      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                        e.getMessage(),
                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                        true);
                      MainMenuGui.getInstance().exitV810(false);
                    }
                  });
                }
                _toolBar.setSelectRegionToggleButtonSelected(false);
                _toolBar.setAllowZoomRectangle(true);
                _isInitialized = false;
                _graphicsEngine.reset();
//                _rendererCreator.reset();
                _project = null;
              }
              //Khaw Chek Hau - XCR2285: 2D Alignment on v810
              else if (eventEnum.equals(InspectionEventEnum.MANUAL_2D_ALIGNMENT_COMPLETED))
              {
                if( (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.LOW) && 
                   _magnification.equals(MagnificationEnum.H_NOMINAL) ) ||
                    (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.HIGH) && 
                   _magnification.equals(MagnificationEnum.NOMINAL) )   )
                  return;
                
                _alignmentInProcess = false;
                _currentAlignmentGroup.setHasDraggedRegion(false);
                _rendererCreator.clearRenderers(_graphicsEngine);
                _graphicsEngine.setDragGraphicsMode(false);
                setToolBarEnabled(false);
                _toolBar.setDragToggleButtonSelected(true);
                try 
                {
                  if (LicenseManager.isOfflineProgramming() == false)
                  {
                    _toolBar.setAllowGenerateImageSetWizard(true);
                  }
                  else
                  {
                    _toolBar.setAllowGenerateImageSetWizard(false);
                  }
                }
                catch (final BusinessException e)
                {
                  finishLicenseMonitor();
                  SwingUtils.invokeLater(new Runnable()
                  {
                    public void run()
                    {
                      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                        e.getMessage(),
                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                        true);
                      MainMenuGui.getInstance().exitV810(false);
                    }
                  });
                }
                _toolBar.setSelectRegionToggleButtonSelected(false);
                _toolBar.setAllowZoomRectangle(true);
                _isInitialized = false;
                _graphicsEngine.reset();
//                _rendererCreator.reset();
                _project = null;
                _selectedFocusRegion = null;
              }
              else if (eventEnum.equals(InspectionEventEnum.WAITING_FOR_USER_TO_ALIGN_IMAGE))
              {
                if( (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.LOW) && 
                   _magnification.equals(MagnificationEnum.H_NOMINAL) ) ||
                    (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.HIGH) && 
                   _magnification.equals(MagnificationEnum.NOMINAL) )   )
                  return;

                _currentAlignmentGroup = alignmentInspectionEvent.getAlignmentGroup();
                display(alignmentInspectionEvent);
              }
              else if (eventEnum.equals(InspectionEventEnum.DETERMINE_CROPPED_ALIGNMENT_REGION))
              {
                if( (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.LOW) && 
                   _magnification.equals(MagnificationEnum.H_NOMINAL) ) ||
                    (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.HIGH) && 
                   _magnification.equals(MagnificationEnum.NOMINAL) )   )
                  return;
                
                AffineTransform pixelToNanoMetersTransform = null;
                try
                {
                  _nanoMetersToPixelTransform = AffineTransform.getScaleInstance(1.0 / (double)_magnification.getNanoMetersPerPixel(),
                                                                                 1.0 / (double)_magnification.getNanoMetersPerPixel());
                  pixelToNanoMetersTransform = _nanoMetersToPixelTransform.createInverse();
                }
                catch (NoninvertibleTransformException ex)
                {
                  Assert.expect(false);
                }

                Assert.expect(_selectedFocusRegion != null);
                
                double draggedActualMinXInDouble = pixelToNanoMetersTransform.createTransformedShape(_selectedFocusRegion).getBounds2D().getMinX();
                double draggedActualMinYInDouble = pixelToNanoMetersTransform.createTransformedShape(_selectedFocusRegion).getBounds2D().getMinY();
                double draggedWidthInDouble = pixelToNanoMetersTransform.createTransformedShape(_selectedFocusRegion).getBounds2D().getWidth();
                double draggedHeightInDouble = pixelToNanoMetersTransform.createTransformedShape(_selectedFocusRegion).getBounds2D().getHeight();

                //Get expected minX and minY
                double draggedExpectedMinXInDouble = draggedActualMinXInDouble - _currentAlignmentGroup.getCurrentManualAlignmentXOffsetRelativeToPanelInNanoMeters();
                double draggedExpectedMinYInDouble = draggedActualMinYInDouble - _currentAlignmentGroup.getCurrentManualAlignmentYOffsetRelativeToPanelInNanoMeters();
                
                int draggedActualMinX = (int) Math.round(draggedActualMinXInDouble);
                int draggedActualMinY = (int) Math.round(draggedActualMinYInDouble);
                int draggedWidth = (int) Math.round(draggedWidthInDouble);
                int draggedHeight = (int) Math.round(draggedHeightInDouble);
                int draggedExpectedMinX = (int) Math.round(draggedExpectedMinXInDouble);
                int draggedExpectedMinY = (int) Math.round(draggedExpectedMinYInDouble);

                _currentAlignmentGroup.setDraggedActualMinX(draggedActualMinX);
                _currentAlignmentGroup.setDraggedActualMinY(draggedActualMinY);
                _currentAlignmentGroup.setDraggedExpectedMinX(draggedExpectedMinX);
                _currentAlignmentGroup.setDraggedExpectedMinY(draggedExpectedMinY);
                _currentAlignmentGroup.setDraggedWidth(draggedWidth);
                _currentAlignmentGroup.setDraggedHeight(draggedHeight);
                
                String magnification = null;
                if (alignmentInspectionEvent.getMagnificationType() == MagnificationTypeEnum.LOW)
                {
                  if (Config.getSystemCurrentLowMagnification().equals("M19"))
                    magnification = "M19";   
                  else if (Config.getSystemCurrentLowMagnification().equals("M23"))
                    magnification = "M23"; 
                }
                else
                {
                  magnification = "M11";  
                }
                
                String draggedImageFileName = FileName.get2DAlignmentImageFileName(magnification, 
                                                                                   _currentAlignmentGroup.getName(), 
                                                                                   draggedExpectedMinX, 
                                                                                   draggedExpectedMinY, 
                                                                                   draggedWidth, 
                                                                                   draggedHeight, 
                                                                                   draggedActualMinX, 
                                                                                   draggedActualMinY);

                if (alignmentInspectionEvent.getMagnificationType() == MagnificationTypeEnum.LOW)
                  _currentAlignmentGroup.setDraggedImageFileName(draggedImageFileName); 
                else
                  _currentAlignmentGroup.setDraggedImageFileNameForHighMag(draggedImageFileName); 
              }
              //Khaw Chek Hau - XCR2285: 2D Alignment on v810
              else if (eventEnum.equals(InspectionEventEnum.RESET_ZOOM_FACTOR))
              {
                if( (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.LOW) && 
                   _magnification.equals(MagnificationEnum.H_NOMINAL) ) ||
                    (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.HIGH) && 
                   _magnification.equals(MagnificationEnum.NOMINAL) )   )
                  return;

                Rectangle2D alignedBounds = new Rectangle2D.Double(_currentAlignmentGroup.getDraggedActualMinX(), 
                                                                   _currentAlignmentGroup.getDraggedActualMinY(), 
                                                                   _currentAlignmentGroup.getDraggedWidth(), 
                                                                   _currentAlignmentGroup.getDraggedHeight());
                
                String magnification = null;
                if (alignmentInspectionEvent.getMagnificationType() == MagnificationTypeEnum.LOW)
                {
                  if (Config.getSystemCurrentLowMagnification().equals("M19"))
                    magnification = "M19";   
                  else if (Config.getSystemCurrentLowMagnification().equals("M23"))
                    magnification = "M23"; 
                }
                else
                {
                  if (Config.getInstance().getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED).equals(_Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED_FOR_M13))
                    magnification = "M13";
                  else
                    magnification = "M11";
                }
                
                //Disable components renderer color before screen image captured
                for(int manualAlignmentPadLayer : _subProgramIdToManualAlignmentPadLayer.values())
                  _graphicsEngine.setVisible(manualAlignmentPadLayer, false);
                
                _graphicsEngine.repaint();
                
                alignedBounds = _nanoMetersToPixelTransform.createTransformedShape(alignedBounds).getBounds2D();
                _graphicsEngine.fitRectangleInRendererCoordinatesToScreen(alignedBounds.getBounds2D());
                
                resetZoomForAlignment2DRegion(magnification);         
                
                _currentAlignmentGroup.setDraggedRegionRecWidth((int)_draggedRenderer.getWidth());
                _currentAlignmentGroup.setDraggedRegionRecHeight((int)_draggedRenderer.getHeight());
                
                _currentAlignmentGroup.setHasDraggedRegion(false);
                _graphicsEngine.setVisible(_selectRegionAreaLayer, false);              
              }
              //Khaw Chek Hau - XCR2285: 2D Alignment on v810
              else if (eventEnum.equals(InspectionEventEnum.WAITING_FOR_USER_TO_CROP_IMAGE))
              {
                if( (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.LOW) && 
                   _magnification.equals(MagnificationEnum.H_NOMINAL) ) ||
                    (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.HIGH) && 
                   _magnification.equals(MagnificationEnum.NOMINAL) )   )
                  return;
                
                _graphicsEngine.setVisible(_selectRegionAreaLayer, true);
                _toolBar.setSelectRegionToggleButtonSelected(false);
                _toolBar.setDragToggleButtonSelected(true);
                _graphicsEngine.setDragGraphicsMode(true);
                
                int maximumAlignmentRegionLengthInNanoMeters = ReconstructionRegion.getMaxVerificationRegionLengthInNanoMeters();
                
                if (alignmentInspectionEvent.getMagnificationType() == MagnificationTypeEnum.HIGH)
                {
                  maximumAlignmentRegionLengthInNanoMeters = ReconstructionRegion.getMaxHighMagVerificationRegionLengthInNanoMeters();
                }    
                
                Point2D maximumAlignmentDraggedLengthInPixel = new Point2D.Double(0, maximumAlignmentRegionLengthInNanoMeters);
                _nanoMetersToPixelTransform.transform(maximumAlignmentDraggedLengthInPixel, maximumAlignmentDraggedLengthInPixel);
                
                double maximum2DRegionDragSizeInPixel = maximumAlignmentDraggedLengthInPixel.getY();
                
                _toolBar.setSelectRegionMaximumSizeInPixel(maximum2DRegionDragSizeInPixel - 10);
                display2D(alignmentInspectionEvent);
              }
              //Khaw Chek Hau - XCR2285: 2D Alignment on v810
              else if (eventEnum.equals(InspectionEventEnum.DISABLE_NEXT_BUTTON_BEFORE_IMAGE_CROPPED))
              {
                if( (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.LOW) && 
                   _magnification.equals(MagnificationEnum.H_NOMINAL) ) ||
                    (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.HIGH) && 
                   _magnification.equals(MagnificationEnum.NOMINAL) )   )
                  return;
                
                _currentAlignmentGroup = alignmentInspectionEvent.getAlignmentGroup();
              }
              else if (eventEnum.equals(InspectionEventEnum.ALIGNED_ON_IMAGE))
              {
                if( (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.LOW) && 
                   _magnification.equals(MagnificationEnum.H_NOMINAL) ) ||
                    (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.HIGH) && 
                   _magnification.equals(MagnificationEnum.NOMINAL) )   )
                  return;
                
                _toolBar.setSelectRegionToggleButtonSelected(false);
                _verifyingManualAlignment = true;
                _rendererCreator.clearRenderers(_graphicsEngine);
                _graphicsEngine.reset();

                Image alignmentImage = alignmentInspectionEvent.getImage();
                displayReconstructedAlignmentImage(alignmentImage);
                MeasurementRegionDiagnosticInfo locatedAlignmentPadRegionsDiagnosticInfo =
                    alignmentInspectionEvent.getLocatedAlignmentFeaturesDiagnosticInfo();
                java.awt.Shape locatedAlignmentPadRegionsShape = locatedAlignmentPadRegionsDiagnosticInfo.getDiagnosticRegion();
                displayLocatedAlignmentPadGraphics(locatedAlignmentPadRegionsShape);
                _graphicsEngine.setForeground(_locatedAlignmentPadLayer, LayerColorEnum.LOCATED_ALIGNMENT_PAD_COLOR.getColor());
                // MDW - uncomment the code below to also see the 'context' pads used during alignment.
//                MeasurementRegionDiagnosticInfo locatedAlignmentContextPadRegionsDiagnosticInfo =
//                    alignmentInspectionEvent.getLocatedAlignmentContextFeaturesDiagnosticInfo();
//                java.awt.Shape locatedAlignmentContextPadRegionsShape = locatedAlignmentContextPadRegionsDiagnosticInfo.getDiagnosticRegion();
//                displayLocatedAlignmentContextPadGraphics(locatedAlignmentContextPadRegionsShape);
//                _graphicsEngine.setForeground(_locatedAlignmentContextPadLayer, LayerColorEnum.LOCATED_ALIGNMENT_CONTEXT_PAD_COLOR.getColor());
              }
              //Khaw Chek Hau - XCR2285: 2D Alignment on v810
              else if (eventEnum.equals(InspectionEventEnum.ALIGNED_ON_2D_IMAGE))
              {
                if( (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.LOW) && 
                   _magnification.equals(MagnificationEnum.H_NOMINAL) ) ||
                    (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.HIGH) && 
                   _magnification.equals(MagnificationEnum.NOMINAL) )   )
                  return;
                
                _toolBar.setSelectRegionToggleButtonSelected(false);
                _verifyingManualAlignment = true;
                _rendererCreator.clearRenderers(_graphicsEngine);
                _graphicsEngine.reset();

                Image alignmentImage = alignmentInspectionEvent.getImage();
                displayReconstructedAlignmentImage(alignmentImage);
                _graphicsEngine.setForeground(_locatedAlignmentPadLayer, LayerColorEnum.LOCATED_ALIGNMENT_PAD_COLOR.getColor());
              }
            }
            else if (eventEnum.equals(InspectionEventEnum.MANUAL_ALIGNMENT_STARTED))
            {
              if( (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.LOW) && 
                   _magnification.equals(MagnificationEnum.H_NOMINAL) ) ||
                    (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.HIGH) && 
                   _magnification.equals(MagnificationEnum.NOMINAL) )   )
                  return;
              
              _verifyingManualAlignment = false;
              _alignmentInProcess = true;
              initialize(alignmentInspectionEvent.getPanelLocationInSystem());              
              _graphicsEngine.setDragGraphicsMode(true);
              setToolBarEnabled(true);
              _toolBar.setToggleGraphicsToggleButtonSelected(false);
              _toolBar.setAllowGenerateImageSetWizard(false);
              _toolBar.setAllowZoomRectangle(false);
              showGraphics(alignmentInspectionEvent.getTestSubProgramId());
            }
            //Khaw Chek Hau - XCR2285: 2D Alignment on v810
            else if (eventEnum.equals(InspectionEventEnum.MANUAL_2D_ALIGNMENT_STARTED))
            { 
              if( (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.LOW) && 
                   _magnification.equals(MagnificationEnum.H_NOMINAL) ) ||
                    (alignmentInspectionEvent.getMagnificationType().equals(MagnificationTypeEnum.HIGH) && 
                   _magnification.equals(MagnificationEnum.NOMINAL) )   )
                  return;
              
              _verifyingManualAlignment = false;
              _alignmentInProcess = true;
              initialize(alignmentInspectionEvent.getPanelLocationInSystem());     
              setToolBarEnabled(true);
              _toolBar.setToggleGraphicsToggleButtonSelected(false);
              _toolBar.setAllowGenerateImageSetWizard(false);
              _toolBar.setAllowZoomRectangle(false);
              showGraphics(alignmentInspectionEvent.getTestSubProgramId());
            }
          }
        }
        else
          Assert.expect(false);
        if (argument instanceof InspectionEvent)
        {
          InspectionEvent event = (InspectionEvent)argument;
          event.decrementReferenceCount();
        }
      }
    });
  }

  /**
   * @author Matt Wharton
   */
  private void displayReconstructedAlignmentImage(com.axi.util.image.Image image)
  {
    Assert.expect(image != null);

    _graphicsEngine.addRenderer(_reconstructedAlignmentImageLayer, new InspectionImageRenderer(image));
    _graphicsEngine.setAxisInterpretation(true, true);
    _graphicsEngine.center();
    _graphicsEngine.repaint();
  }

  /**
   * @author Matt Wharton
   */
  private void displayLocatedAlignmentPadGraphics(java.awt.Shape locatedAlignmentPadRegionsShape)
  {
    Assert.expect(locatedAlignmentPadRegionsShape != null);

    _graphicsEngine.addRenderer(_locatedAlignmentPadLayer, new RegionOfInterestRenderer(locatedAlignmentPadRegionsShape));
    _graphicsEngine.setAxisInterpretation(true, true);
    _graphicsEngine.center();
    _graphicsEngine.repaint();
  }

  /**
   * @author Matt Wharton
   */
  private void displayLocatedAlignmentContextPadGraphics(java.awt.Shape locatedAlignmentContextPadRegionsShape)
  {
    Assert.expect(locatedAlignmentContextPadRegionsShape != null);

    _graphicsEngine.addRenderer(_locatedAlignmentContextPadLayer, new RegionOfInterestRenderer(locatedAlignmentContextPadRegionsShape));
    _graphicsEngine.setAxisInterpretation(true, true);
    _graphicsEngine.center();
    _graphicsEngine.repaint();
  }

  /**
   * @author George A. David
   */
  private void createLayers()
  {
    int layerNumber = JLayeredPane.DEFAULT_LAYER.intValue() - 1;
    // this order is important! left must come before right.
    // otherwise the wrong images and wrong alignment transform will be used
    //XCR-3437, Cad offset when view verification on long panel recipe
    List<Integer> subProgramIds = new ArrayList<Integer>();
    subProgramIds.addAll(_project.getTestProgram().getAllUsedTestSubProgramID());
    Collections.sort(subProgramIds, Collections.reverseOrder());
    
    for (Integer testSubProgramID : subProgramIds)
    {
      _subProgramIdToVerificationImageLayer.put(testSubProgramID, ++layerNumber);
    }
    
    for (Integer testSubProgramID : subProgramIds)
    {
      _subProgramIdToManualAlignmentPadLayer.put(testSubProgramID, ++layerNumber);
    }
    
    _layersToUseForDragging.clear();
    _layersToUseForDragging.addAll(_subProgramIdToVerificationImageLayer.values());

    _reconstructedAlignmentImageLayer = ++layerNumber;
    _locatedAlignmentPadLayer = ++layerNumber;
    _locatedAlignmentContextPadLayer = ++layerNumber;
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _selectRegionAreaLayer = ++layerNumber;
    _layersToUseForDragging.add(_selectRegionAreaLayer);
  }

  /**
   * @author George A. David
   */
  private void setColorsAndThreshold()
  {
    Assert.expect(_graphicsEngine != null);

    for(int padLayer : _subProgramIdToManualAlignmentPadLayer.values())
    {
      _graphicsEngine.setForeground(padLayer, LayerColorEnum.ALIGNMENT_PAD_COLOR.getColor());
      _graphicsEngine.setCreationThreshold(padLayer, _PAD_VISIBILITY_THRESHOLD, this);
    }

    for(int imageLayer : _subProgramIdToVerificationImageLayer.values())
    {
      _graphicsEngine.setCreationThreshold(imageLayer, 1000, this);
    }
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _graphicsEngine.setForeground(_selectRegionAreaLayer,LayerColorEnum.EXPECTED_IMAGE_PIXELS.getColor()); 
  }

  /**
   * @author George A. David
   */
  public void updateRenderers(GraphicsEngine graphicsEngine, int layerNumber)
  {
    if (_verifyingManualAlignment)
    {
      return;
    }

    if(_subProgramIdToVerificationImageLayer.values().contains(layerNumber))
    {
      _rendererCreator.createVerificationImageRenderers(graphicsEngine, _showingTopSide);
    }
    else if(_subProgramIdToManualAlignmentPadLayer.containsValue(layerNumber))
    {
      // don't redraw when adjusting the board location
      if(_alignmentInProcess)
        return;

      _rendererCreator.createCadRenderers(graphicsEngine, _showingTopSide);
    }
    graphicsEngine.repaint();
  }

  /**
   * @author George A. David
   */
  public Point2D getOffsetRelativeToPanelInNanoMeters()
  {
    Point2D point = _graphicsEngine.getOffsetFromMarkInRendererCoordinates();
    point.setLocation(-point.getX() * _magnification.getNanoMetersPerPixel(),
                      -point.getY() * _magnification.getNanoMetersPerPixel());

    return point;
  }

  /**
   * This calculates the offset from the expected coordinate, not the exact
   * offset the user applied. This is because we are drawing the cad based
   * on the correction from the last manual alignment. If no previous manual
   * alignment exists, then this offest will match what the user offset.
   *
   * @author George A. David
   */
  public Point2D getOffsetRelativeToPanelInNanoMetersFromExpectedLocation()
  {
    Point2D point = getOffsetRelativeToPanelInNanoMeters();
    point.setLocation(point.getX() - _prevAlignmentXoffsetInNanoMeters,
                      point.getY() - _prevAlignmentYoffsetInNanoMeters);

//    System.out.println("Alignment Offset  X: " + point.getX() + " Y: " + point.getY());
    return point;
  }

  /**
   * @author George A. David
   */
  public void zoomIn()
  {
    _graphicsEngine.zoom(_ZOOM_FACTOR);
  }

  /**
   * @author George A. David
   */
  public void zoomOut()
  {
    _graphicsEngine.zoom(1.0 / _ZOOM_FACTOR);
  }

  /**
   * @author George A. David
   */
  public void setZoomRectangleMode(boolean zoomRectangleMode)
  {
    if(zoomRectangleMode)
    {
      _graphicsEngine.setDragGraphicsMode(false);
      _graphicsEngine.setZoomRectangleMode(true);
    }
    else
    {
      _graphicsEngine.setZoomRectangleMode(false);
      _graphicsEngine.setDragGraphicsMode(true);
    }
  }

  /**
   * @author George A. David
   */
  public void setDragGraphicsMode(boolean dragGraphicsMode)
  {
    _graphicsEngine.setDragGraphicsMode(dragGraphicsMode);
//    Assert.expect(false);
  }

  /**
   * @author George A. David
   */
  public void setMeasurmentMode(boolean measurementMode)
  {
    if(measurementMode)
    {
      _graphicsEngine.setDragGraphicsMode(false);
      _graphicsEngine.setMeasurementMode(true);
    }
    else
    {
      _graphicsEngine.setMeasurementMode(false);
      _graphicsEngine.setDragGraphicsMode(true);
    }
  }

  /**
   * @author Seng-Yew Lim
   */
  public void setImageStayPersistent(boolean isImageStayPersistent)
  {
    // do nothing
  }

  /**
   * @author Anthony Fong //Anthony01005
   */
  public void setImageFitToScreen(boolean isImageFitToScreen)
  {
    // do nothing
  }
  /**
   * @author Anthony Fong //Anthony01005
   */
  public void setZoomFactor(double zoomFactor)
  {
    // do nothing
  }

  /**
   * @author George A. David
   */
  public void showTopSide()
  {
    Assert.expect(false);
  }

  /**
   * @author George A. David
   */
  public void showBottomSide()
  {
    Assert.expect(false);
  }

  /**
   * @author George A. David
   */
  public void resetGraphics()
  {
    Assert.expect(false);
  }

  /**
   * @author George A. David
   */
  public void clearGraphics()
  {
    for(int manualAlignmentPadLayer : _subProgramIdToManualAlignmentPadLayer.values())
      _graphicsEngine.setVisible(manualAlignmentPadLayer, false);
    for(int verificationAlignmentPadLayer : _subProgramIdToVerificationImageLayer.values())
      _graphicsEngine.setVisible(verificationAlignmentPadLayer, false);
    _graphicsEngine.repaint();
  }

  /**
   * @author George A. David
   */
  public void showGraphics()
  {
    for(int testSubProgramId : currentTestSubProgramIds)
    {
      _graphicsEngine.setVisible(_subProgramIdToManualAlignmentPadLayer.get(testSubProgramId), true); 
      _graphicsEngine.setVisible(_subProgramIdToVerificationImageLayer.get(testSubProgramId), true); 
    }
    _graphicsEngine.repaint();
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void resetZoomForAlignment2DRegion(String magnification)
  {    
    _graphicsEngine.resetZoom();
    if (magnification.equals("M19"))
      _graphicsEngine.setZoom(_M19_ZOOM_FACTOR); 
    else if ((magnification.equals("M11")))
      _graphicsEngine.setZoom(_M11_ZOOM_FACTOR);
    else if ((magnification.equals("M13")))
      _graphicsEngine.setZoom(_M13_ZOOM_FACTOR);
    else if ((magnification.equals("M23")))
      _graphicsEngine.setZoom(_M23_ZOOM_FACTOR);
    else
      Assert.expect(false, "Failed to handle the magnification " + magnification);
  }
  
  /**
   * @author George A. David
   */
  public void showGraphics(int currentTestSubProgramId)
  {
    _rendererCreator.clearCadRenderers(_graphicsEngine);
    for(int testSubProgramId : currentTestSubProgramIds)
    {
      if (testSubProgramId == currentTestSubProgramId)
      {
        _rendererCreator.createCadRenderers(_graphicsEngine, _showingTopSide,_subProgramIdToManualAlignmentPadLayer.get(testSubProgramId));
        _graphicsEngine.setVisible(_subProgramIdToManualAlignmentPadLayer.get(testSubProgramId), true);
        _graphicsEngine.setVisible(_subProgramIdToVerificationImageLayer.get(testSubProgramId), true);
      }
      else
      {
        _graphicsEngine.setVisible(_subProgramIdToManualAlignmentPadLayer.get(testSubProgramId), false);
        _graphicsEngine.setVisible(_subProgramIdToVerificationImageLayer.get(testSubProgramId), false);
      }
    }
    _graphicsEngine.repaint();
  }

  /**
   * @author Scott Richardson
   */
  public void translateUpInZAxis()
  {
    Assert.expect(false, "translateUpInZAxis is not implemented here");
  }

  /**
   * @author Scott Richardson
   */
  public void translateDownInZAxis()
  {
    Assert.expect(false, "translateUpInZAxis is not implemented here");
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setGroupSelectMode(boolean isGroupSelectMode)
  {
    _graphicsEngine.setGroupSelectMode(isGroupSelectMode);
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setSelectRegionMode(boolean isSelectRegionMode)
  {
    _graphicsEngine.setSelectRegionMode(isSelectRegionMode); 
  }

  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setSelectRegionMode(boolean isSelectRegionMode, double maxImageRegionWidth, double maxImageRegionHeight)
  {
    _graphicsEngine.setSelectRegionMode(isSelectRegionMode, maxImageRegionWidth, maxImageRegionHeight);  
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   *
   * @author weng-jian.eoh
   */
  public void finish()
  {
    _inspectionEventObservable.deleteObserver(this);
  }
}
