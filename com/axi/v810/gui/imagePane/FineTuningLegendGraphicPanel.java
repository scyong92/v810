package com.axi.v810.gui.imagePane;

import java.awt.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.Assert;

/**
 * This class is the legend panel for the image pane display during fine tuning.
 * @author George Booth
 */
public class FineTuningLegendGraphicPanel extends JPanel
{
  private FineTuningLegendGraphicsEngineSetup _fineTuningLegendGraphicsEngineSetup = null;
  private JScrollPane _fineTuningLegendScrollPane;
  private GraphicsEngine _graphicsEngine = null;

  /**
   * @author George Booth
   */
  public FineTuningLegendGraphicPanel()
  {
    _fineTuningLegendGraphicsEngineSetup = new FineTuningLegendGraphicsEngineSetup();
    _graphicsEngine = _fineTuningLegendGraphicsEngineSetup.getGraphicsEngine();
    jbInit();
  }

  /**
   * @author George Booth
   */
  private void jbInit()
  {
    // panel preferred size set in FineTuningPanel
    setLayout(new BorderLayout());
    setBackground(Color.lightGray);

    // width will be determined by panel width
    _graphicsEngine.setPreferredSize(new Dimension(0, 50));

    _fineTuningLegendScrollPane = new JScrollPane();
    _fineTuningLegendScrollPane.getViewport().add(_graphicsEngine);
    add(_fineTuningLegendScrollPane, BorderLayout.CENTER);
  }

  /**
   * @author Andy Mechtenberg
   */
  void display()
  {
    Assert.expect(_fineTuningLegendGraphicsEngineSetup != null);
    _fineTuningLegendGraphicsEngineSetup.display();
  }

  /**
   * @author Andy Mechtenberg
   */
  void finish()
  {
    Assert.expect(_fineTuningLegendGraphicsEngineSetup != null);
    _fineTuningLegendGraphicsEngineSetup.finish();
  }

  /**
   * @author Andy Mechtenberg
   */
  void unpopulate()
  {
    _fineTuningLegendGraphicsEngineSetup.unpopulate();
  }
}
