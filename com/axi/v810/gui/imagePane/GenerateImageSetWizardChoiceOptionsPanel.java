package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * <p>Title: GenerateImageSetWizardChoiceOptionsPanel/p>
 *
 * <p>Description: This panel is the pre main panel of the generate image set wizard.  This panel will present the user with
 * a choice on whether they want to generate a 'normal' fine tuning image set or a 'precision' image set. Precision
 * image sets use surface model to more accurately determine the focus of components.</p>
 *
 * <p>Copyright: Copyright (c) 2008</p>
 *
 * <p>Company: AXI</p>
 *
 * @author Laura Cormos
 */
public class GenerateImageSetWizardChoiceOptionsPanel extends WizardPanel
{
  private JPanel _imageSetTypeSpecificationPanel;

  private Border _border1 = BorderFactory.createEtchedBorder();
  private Border _titledBorder = new TitledBorder(_border1, StringLocalizer.keyToString("GISWK_PURPOSE_KEY"));

  private GridLayout _imageSetTypeSpecificationPanelGridLayout = new GridLayout();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout(5, 5);

  private ButtonGroup _imageSetTypeRadioButtonGroup;

  private JRadioButton _precisionImageSetTypeRadioButton;
  private JRadioButton _fineTuningImageSetTypeRadioButton;
  private JCheckBox _saveGoodImagesCheckBox;
  private ImageSetData _imageSetData = null;
  private GenerateImageSetWizardDialog _dialog = null;

  /**
   * @author Laura Cormos
   */
  public GenerateImageSetWizardChoiceOptionsPanel(Project currentLoadedProject, ImageSetData imageSetData, WizardDialog dialog)
  {
    super(dialog);
    Assert.expect(currentLoadedProject != null, "project is null");
    Assert.expect(imageSetData != null, "image set data is null");
    Assert.expect(dialog != null, "Dialog is not null");
    _imageSetData = imageSetData;
    _imageSetData.setGenerateImagesForNoTestAndNoLoadDevices(false);
    _dialog = (GenerateImageSetWizardDialog) dialog;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _imageSetData = null;
  }

  /**
   * @author Laura Cormos
   */
  public void removeObservers()
  {
    // do nothing
  }

  /**
   * @author Laura Cormos
   * @author khang-shian.sham
   */
  private void createPanel()
  {
    removeAll();
    setLayout(_mainPanelBorderLayout);
    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    _precisionImageSetTypeRadioButton = new JRadioButton();
    _fineTuningImageSetTypeRadioButton = new JRadioButton();
    _saveGoodImagesCheckBox = new JCheckBox();

    _imageSetTypeSpecificationPanel = new JPanel();
    _imageSetTypeSpecificationPanel.setBorder(BorderFactory.createCompoundBorder(_titledBorder, BorderFactory.createEmptyBorder(5, 25, 5, 5)));
    _imageSetTypeSpecificationPanel.setLayout(new VerticalFlowLayout(FlowLayout.LEFT, FlowLayout.LEFT, 0, 15));
    _precisionImageSetTypeRadioButton.setText(StringLocalizer.keyToString("GISWK_PRECISION_TUNING_IMAGE_SET_KEY"));
    _fineTuningImageSetTypeRadioButton.setText(StringLocalizer.keyToString("GISWK_FINE_TUNING_IMAGE_SET_KEY"));
    _saveGoodImagesCheckBox.setText(StringLocalizer.keyToString("GISWK_SAVE_GOOD_IMAGES__KEY"));

    JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
    panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    JLabel instructionLabel = new JLabel(StringLocalizer.keyToString("GISWK_SELECT_IMAGE_SET_PURPOSE_KEY"));
    instructionLabel.setFont(FontUtil.getBoldFont(instructionLabel.getFont(), Localization.getLocale()));
    panel.add(instructionLabel);

    add(panel, BorderLayout.NORTH);
    add(_imageSetTypeSpecificationPanel, BorderLayout.CENTER);

    // add the radiobuttons to the panel for display
    _imageSetTypeSpecificationPanel.add(_fineTuningImageSetTypeRadioButton);
    _imageSetTypeSpecificationPanel.add(_precisionImageSetTypeRadioButton);
    _imageSetTypeSpecificationPanel.add(_saveGoodImagesCheckBox);

    // add the buttons to the radiobuttongroup
    _imageSetTypeRadioButtonGroup = new ButtonGroup();
    _imageSetTypeRadioButtonGroup.add(_fineTuningImageSetTypeRadioButton);
    _imageSetTypeRadioButtonGroup.add(_precisionImageSetTypeRadioButton);
    
    // listeners
    _fineTuningImageSetTypeRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _imageSetData.setImageSetCollectionTypeEnum(ImageSetCollectionTypeEnum.FINE_TUNING);
      }
    });
    _precisionImageSetTypeRadioButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        // defaults settings for a precision tuning images set
        _imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.PANEL);
        _imageSetData.setBoardPopulated(true);
        _imageSetData.setGenerateImagesForNoTestAndNoLoadDevices(false);
        _imageSetData.setImageSetCollectionTypeEnum(ImageSetCollectionTypeEnum.PRECISION);

//        validatePanel();
      }
    });
    _fineTuningImageSetTypeRadioButton.setSelected(true);
    _saveGoodImagesCheckBox.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        if (_saveGoodImagesCheckBox.isSelected())
        {
          _imageSetData.setSaveGoodImages(true);
        }
        else
        {
          _imageSetData.setSaveGoodImages(false);
        }
      }
    });
  }

  /**
   * @author Laura Cormos
   */
  public void populatePanelWithData()
  {
    createPanel();

    // set the title
    _dialog.changeTitle(StringLocalizer.keyToString("GISWK_SCREEN_1_TITLE_KEY"));

    // now call validate to see if the panel has all the info needed to go to the next step.
    validatePanel();

  }

  /**
   * This method will check to make sure all the data is specified and if so then will fire an update event to the
   * dialog to update the buttons correctly. This method will also specify the next and previous pointers
   *
   * @author Laura Cormos
   */
  public void validatePanel()
  {
    // set the image set colletion type accordingly
    if (_imageSetData.isCollectionTypeEnumSet())
    {
      if (_imageSetData.getImageSetCollectionTypeEnum().equals(ImageSetCollectionTypeEnum.FINE_TUNING))
      {
        _fineTuningImageSetTypeRadioButton.setSelected(true);
      }
      else if (_imageSetData.getImageSetCollectionTypeEnum().equals(ImageSetCollectionTypeEnum.PRECISION))
      {
        _precisionImageSetTypeRadioButton.setSelected(true);
      }
      else
      {
        Assert.expect(false, "image set collection type is not specified correctly");
      }
    }
    // next, cancel, finish, previous button states
    WizardButtonState buttonState = new WizardButtonState(true, true, false, false);
    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Laura Cormos
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null, "Name of panel is null");
    Assert.expect(name.length() > 0, "Name of panel is an empty string");
    _name = name;
  }

  /**
   * @author Laura Cormos
   */
  public String getWizardName()
  {
    Assert.expect(_name != null, "Name of panel is not set");
    return _name;
  }
}
