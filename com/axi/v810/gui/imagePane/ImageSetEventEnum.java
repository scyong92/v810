package com.axi.v810.gui.imagePane;

import com.axi.v810.gui.*;

/**
 * <p>Title: ImageSetEventEnum</p>
 *
 * <p>Description: Gui Event enumeration used to indicate if the user has selected an image set or not.</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: AXI</p>
 *
 * @author Erica Wheatcroft
 */
public class ImageSetEventEnum extends GuiEventEnum
{
  private static int _index = -1;

  public static final ImageSetEventEnum IMAGE_SET_SELECTED = new ImageSetEventEnum(++_index);
  public static final ImageSetEventEnum IMAGE_SET_NOT_SELECTED = new ImageSetEventEnum(++_index);
  public static final ImageSetEventEnum IMAGE_SET_CREATED = new ImageSetEventEnum(++_index);
  public static final ImageSetEventEnum IMAGE_SET_DELETED = new ImageSetEventEnum(++_index);
  public static final ImageSetEventEnum IMAGE_SET_SELECTED_FOR_ARCHIVE = new ImageSetEventEnum(++_index);
  public static final ImageSetEventEnum IMAGE_SET_NOT_SELECTED_FOR_ARCHIVE = new ImageSetEventEnum(++_index);
  public static final ImageSetEventEnum IMAGE_SET_CLEAR_ALL = new ImageSetEventEnum(++_index);
  public static final ImageSetEventEnum VIRTUAL_LIVE_IMAGE_SET_SELECTED = new ImageSetEventEnum(++_index); //Siew Yeng - XCR-3284

  /**
   * @author Erica Wheatcroft
   */
  protected ImageSetEventEnum(int id)
  {
    super(id);
  }
}
