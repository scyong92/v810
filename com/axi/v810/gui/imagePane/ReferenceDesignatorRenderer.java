package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.v810.hardware.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.MagnificationTypeEnum;


/**
 * This class is responsible for drawing the refernece designator
 *
 * @author George A. David
 */
class ReferenceDesignatorRenderer extends TextRenderer
{
  private com.axi.v810.business.panelDesc.Component _component;
  private boolean _useAlignedShape = false;

  /**
   * @author George A. David
   */
  ReferenceDesignatorRenderer(com.axi.v810.business.panelDesc.Component component, boolean useAlignedShape)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(component != null);
    _component = component;
    _useAlignedShape = useAlignedShape;

    // set a default font
    setFont(FontUtil.getRendererFont(16));

    refreshData();
  }

  /**
   * @author George A. David
   * @Edited By - Kee Chin Seong
   */
  protected void refreshData()
  {
    String referenceDesignator = _component.getReferenceDesignator();

    Shape shape = null;
    if(_useAlignedShape)
      shape = _component.getAlignedShapeRelativeToPanelInNanoMeters();
    else
      shape = _component.getShapeRelativeToPanelInNanoMeters();

    Rectangle2D bounds = shape.getBounds2D();


    //Kee Chin Seong - Get the High Mag Or Low Mag AffineTransform in order Get the correct Scale
    if(_component.isComponentUseSingleMagnification() == false ||
       _component.getBoard().getPanel().getProject().getTestProgram().isUsingOneMagnification() == false)
       initializeText(referenceDesignator, bounds.getCenterX() / MagnificationEnum.NOMINAL.getNanoMetersPerPixel(), bounds.getCenterY() / MagnificationEnum.NOMINAL.getNanoMetersPerPixel(), true);
    else
    {
       if(_component.getPadOne().getSubtype().getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
          initializeText(referenceDesignator, bounds.getCenterX() / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel(), bounds.getCenterY() / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel(), true);  
       else
          initializeText(referenceDesignator, bounds.getCenterX() / MagnificationEnum.NOMINAL.getNanoMetersPerPixel(), bounds.getCenterY() / MagnificationEnum.NOMINAL.getNanoMetersPerPixel(), true);  
    }
    //initializeText(referenceDesignator, bounds.getCenterX() / MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel(), bounds.getCenterY() / MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel(), true);
  }

  /**
   * @author George A. David
   */
  public com.axi.v810.business.panelDesc.Component getComponent()
  {
    Assert.expect(_component != null);
    return _component;
  }
  }
