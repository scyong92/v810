package com.axi.v810.gui.imagePane;

import java.awt.*;
import java.awt.event.*;
import java.util.List;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;


/**
 * <p>Title: GenerateImageSetWizardSecondaryOptionsPanel</p>
 *
 * <p>Description: This is the second panel of the generate image set wizard. This panel will allow the user to select
 * which object they would like to perform the image set generation on. This panel will change depending on what
 * type the user has selected in the main screen of the wizard.</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: AXI</p>
 *
 * @author Erica Wheatcroft
 */
public class GenerateImageSetWizardSecondaryOptionsPanel extends WizardPanel
{
  private JLabel _instructionsLabel = new JLabel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private Border _mainPanelBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
  private JScrollPane _availableItemsScroller;
  private JComboBox _availableBoardTypesComboBox;
  private JComboBox _availableBoardsComboBox;
  private JComboBox _availableComponentsComboBox;
  private JComboBox _availableJointTypesComboBox;
  private JRadioButton _panelRadioButton = null;
  private JRadioButton _boardInstanceRadioButton = null;

  private JList _availableItemsList = null; // this will hold components, subtypes, or joint types, pins etc
  private DefaultListModel _availableItemsListModel = null;

  private ImageSetTypeEnum _currentImageSetType = null;
  private JointTypeEnum _currentSelectedJointType = null;
  private Object _selectedItem = null; // this is an object as it can be a board, component, subtype, joint type, or pad.
  private Project _project = null; // currently loaded project

  private boolean _loadingSwingComponentsWithData = false; // this flag will be used when loading swing components.
  private boolean _boardScope = true;
  private ImageSetData _imageSetData = null;

  private boolean _collectImagesOnAllBoardInstances = false;
  private boolean _userSwitchedBoardInstance = false;
  private boolean _userSwitchedBoardType = false;

  private GenerateImageSetWizardDialog _dialog = null;
  private boolean _saveOnlyTestedDevices = false;

  /**
   * @author Erica Wheatcroft
   */
  public GenerateImageSetWizardSecondaryOptionsPanel(Project project, ImageSetData imageSetData, WizardDialog dialog)
  {
    super(dialog);
    Assert.expect(imageSetData != null, "Image set data is null");
    Assert.expect(project != null, "Current Loaded project is null");
    _imageSetData = imageSetData;
    _project = project;

    _dialog = (GenerateImageSetWizardDialog)dialog;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _loadingSwingComponentsWithData = true;
    if (_availableBoardTypesComboBox != null)
      _availableBoardTypesComboBox.removeAllItems();

    if (_availableBoardsComboBox != null)
      _availableBoardsComboBox.removeAllItems();

    if (_availableComponentsComboBox != null)
      _availableComponentsComboBox.removeAllItems();

    if (_availableJointTypesComboBox != null)
      _availableJointTypesComboBox.removeAllItems();

    if (_availableItemsListModel != null)
      _availableItemsListModel.clear();
    _selectedItem = null;
    _project = null;
    _imageSetData = null;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void removeObservers()
  {
    // do nothing
  }


  /**
   * @author Erica Wheatcroft
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null, "Name of panel is null");
    Assert.expect(name.length() > 0, "Name of panel is an empty string");
    _name = name;
  }

  /**
   * @author Erica Wheatcroft
   */
  public String getWizardName()
  {
    Assert.expect(_name != null, "Name of panel is not set");
    return _name;
  }


  /**
   * @author Erica Wheatcroft
   */
  private void setInstructionLabel()
  {
    if(_currentImageSetType.equals(ImageSetTypeEnum.BOARD))
       _instructionsLabel.setText(StringLocalizer.keyToString("GISWK_BOARD_TYPE_LABEL_INSTRUCTION_KEY"));
    else if(_currentImageSetType.equals(ImageSetTypeEnum.COMPONENT))
       _instructionsLabel.setText(StringLocalizer.keyToString("GISWK_COMPONENT_TYPE_LABEL_INSTRUCTION_KEY"));
    else if(_currentImageSetType.equals(ImageSetTypeEnum.PIN))
       _instructionsLabel.setText(StringLocalizer.keyToString("GISWK_PIN_TYPE_LABEL_INSTRUCTION_KEY"));
    else if(_currentImageSetType.equals(ImageSetTypeEnum.JOINT_TYPE))
       _instructionsLabel.setText(StringLocalizer.keyToString("GISWK_JOINTTYPE_TYPE_LABEL_INSTRUCTION_KEY"));
    else if(_currentImageSetType.equals(ImageSetTypeEnum.SUBTYPE))
       _instructionsLabel.setText(StringLocalizer.keyToString("GISWK_SUBTYPE_TYPE_LABEL_INSTRUCTION_KEY"));
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    if(_currentImageSetType.equals(ImageSetTypeEnum.PANEL) == false)
    {
      _mainPanelBorderLayout.setVgap(5);
      removeAll();
      setLayout(_mainPanelBorderLayout);
      setBorder(_mainPanelBorder);

      // create the instructions panel..
      JPanel instructionsPanel = createInstructionsPanel();
      add(instructionsPanel, java.awt.BorderLayout.NORTH);

      // now lets create the center panel - the panel will change depending on the type of image set the user wants to
      // perform
      if(_currentImageSetType.equals(ImageSetTypeEnum.BOARD))
      {
        // we need to add the board type panel to the instruction panel..
        // so get the layout and add a row to the layout along with some
        // space between the rows.
        GridLayout gridLayout = (GridLayout) instructionsPanel.getLayout();
        gridLayout.setRows(2);
        gridLayout.setVgap(10);
        instructionsPanel.add(createBoardTypePanel());
        add(createBoardTypeOptionsPanel(), java.awt.BorderLayout.CENTER);
        populateBoardTypeOptionsPanel(null);
      }
      else if(_currentImageSetType.equals(ImageSetTypeEnum.COMPONENT))
      {
        add(createComponentTypeOptionsPanel(), java.awt.BorderLayout.CENTER);
        populateComponentOptionsPanel();
      }
      else if(_currentImageSetType == ImageSetTypeEnum.JOINT_TYPE ||
              _currentImageSetType == ImageSetTypeEnum.SUBTYPE)
      {
        add(createJointtypeOrSubtypeOptionsPanel(), java.awt.BorderLayout.CENTER);
        populateJointtypeOrSubtypeOptionsPanel();
      }
      else if(_currentImageSetType.equals(ImageSetTypeEnum.PIN))
      {
        add(createPinTypeOptionsPanel(), java.awt.BorderLayout.CENTER);
        populatePinTypeOptionsPanel();
      }

      // now create the listeners
      createComponentListeners();
    }
    else
      Assert.expect(false, "we should not be on this screen!! Type is PANEL");
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateComponentOptionsPanel()
  {
    _loadingSwingComponentsWithData = true;

    BoardType boardType = (BoardType)_availableBoardTypesComboBox.getSelectedItem();
    if((_userSwitchedBoardType == true && _userSwitchedBoardInstance == false) ||
      (_userSwitchedBoardType == false && _userSwitchedBoardInstance == false))
    {
      if (boardType != null)
      {
        // second populate the available board instances combo box
        List<Board> boards = boardType.getBoards();
        _availableBoardsComboBox.removeAllItems();

        for (Board board : boards)
          _availableBoardsComboBox.addItem(board);
      }
      else
      {
        // first populate the available board types combo box.
        List<BoardType> boardTypes = _project.getPanel().getBoardTypes();
        _availableBoardTypesComboBox.removeAllItems();
        for (BoardType type : boardTypes)
          _availableBoardTypesComboBox.addItem(type);

        // now select the first one board type
        _availableBoardTypesComboBox.setSelectedIndex(0);
        boardType = (BoardType)_availableBoardTypesComboBox.getSelectedItem();

        // second populate the available board instances combo box
        List<Board> boards = boardType.getBoards();
        _availableBoardsComboBox.removeAllItems();

        for (Board board : boards)
          _availableBoardsComboBox.addItem(board);
      }
      // now add the key word all - allows the user to test a component on all board instances for a given board type
      _availableBoardsComboBox.addItem(StringLocalizer.keyToString("GISKW_ALL_KEYWORD_KEY"));
      _availableBoardsComboBox.setSelectedIndex(0);
    }
    _userSwitchedBoardType = false;
    _userSwitchedBoardInstance = false;
    // populate the items list with available components
    populateAvailableItemsList();
    _loadingSwingComponentsWithData = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  private JPanel createInstructionsPanel()
  {
    JPanel instructionsPanel = new JPanel();
    // figure out the instruction, set the label and add the panel
    setInstructionLabel();
    GridLayout instructionsPanelLayout = new GridLayout();
    instructionsPanelLayout.setColumns(0);
    instructionsPanel.setLayout(instructionsPanelLayout);
    // adding the label
    instructionsPanel.add(_instructionsLabel);
    _instructionsLabel.setFont(FontUtil.getBoldFont(_instructionsLabel.getFont(),Localization.getLocale()));
    return instructionsPanel;
  }

  /**
   * This method will create a board type panel.. This panel will have a combo box with all available board types. If only
   * one board type exist then the combo box will be replaced with a label.
   * @author Erica Wheatcroft
   */
  private JPanel createBoardTypePanel()
  {
    JPanel boardTypePanel = new JPanel();
    PairLayout boardTypePanelLayout = new PairLayout();
    boardTypePanel.setLayout(boardTypePanelLayout);
    JLabel boardTypeLabel = new JLabel(StringLocalizer.keyToString("GISKW_SELECTED_BOARD_TYPE_LABEL_KEY"));
    boardTypePanel.add(boardTypeLabel);
    _availableBoardTypesComboBox = new JComboBox();
    boardTypePanel.add(_availableBoardTypesComboBox);
    return boardTypePanel;
  }

  /**
   * This will set up all listeners for the necessary gui components.
   * @author Erica Wheatcroft
   */
  private void createComponentListeners()
  {
    if (_availableBoardTypesComboBox != null)
      _availableBoardTypesComboBox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          if (_loadingSwingComponentsWithData == false)
          {
            _userSwitchedBoardType = true;
            _userSwitchedBoardInstance = false;
            // first get the selected board type.
            Assert.expect(_availableBoardTypesComboBox.getSelectedItem() instanceof BoardType, "Not an instance of a board type");
            BoardType selectedBoardType = (BoardType)_availableBoardTypesComboBox.getSelectedItem();

            // now depending on what type if image set we are doing we need to repopulate the necessary components.
            if (_currentImageSetType.equals(ImageSetTypeEnum.BOARD))
            {
              populateBoardTypeOptionsPanel(selectedBoardType);
            }
            else if (_currentImageSetType.equals(ImageSetTypeEnum.COMPONENT))
            {
              populateComponentOptionsPanel();
            }
            else if (_currentImageSetType.equals(ImageSetTypeEnum.PIN))
            {
              populatePinTypeOptionsPanel();
            }
            else if (_currentImageSetType.equals(ImageSetTypeEnum.JOINT_TYPE) || _currentImageSetType.equals(ImageSetTypeEnum.SUBTYPE))
            {
              populateJointtypeOrSubtypeOptionsPanel();
            }
            else
              Assert.expect(false, "not a valid image set type");
            _selectedItem = null;
            setInitialButtonState();
            validatePanel();
          }
        }
      });

    if (_availableBoardsComboBox != null)
      _availableBoardsComboBox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          if (_loadingSwingComponentsWithData == false)
          {
            _userSwitchedBoardType = false;
            _userSwitchedBoardInstance = true;
            // first get the selected board type.
            if (_availableBoardsComboBox.getSelectedItem() instanceof Board)
            {
              _collectImagesOnAllBoardInstances = false;
            }
            else if (_availableBoardsComboBox.getSelectedItem() instanceof String)
            {
              _collectImagesOnAllBoardInstances = true;
            }
            else
              Assert.expect(false, "selection must be of type board or of type string");

            // now depending on what type if image set we are doing we need to repopulate the necessary components.
            if (_currentImageSetType.equals(ImageSetTypeEnum.BOARD))
            {
              // this better not happen as this combo box is not available when we are doing a board image set
              Assert.expect(false, "Combo box is not available");
            }
            else if (_currentImageSetType.equals(ImageSetTypeEnum.COMPONENT))
            {
              populateComponentOptionsPanel();
            }
            else if (_currentImageSetType.equals(ImageSetTypeEnum.PIN))
            {
              populatePinTypeOptionsPanel();
            }
            else if (_currentImageSetType.equals(ImageSetTypeEnum.JOINT_TYPE) ||
                     _currentImageSetType.equals(ImageSetTypeEnum.SUBTYPE))
            {
              populateJointtypeOrSubtypeOptionsPanel();
            }
            else
              Assert.expect(false, "not a valid image set type");
            _selectedItem = null;
            setInitialButtonState();
            validatePanel();
          }
        }
      });

    if(_availableComponentsComboBox != null)
      _availableComponentsComboBox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          if (_loadingSwingComponentsWithData == false)
          {
            _loadingSwingComponentsWithData = true;

            if (_availableComponentsComboBox.getSelectedItem() instanceof com.axi.v810.business.panelDesc.Component)
            {
              com.axi.v810.business.panelDesc.Component selectedComponent =
                  (com.axi.v810.business.panelDesc.Component)_availableComponentsComboBox.getSelectedItem();

              List<PadType> padTypes = null;

              if (_saveOnlyTestedDevices == false)
                padTypes = selectedComponent.getComponentType().getTestablePadTypes();
              else
                padTypes = selectedComponent.getComponentType().getInspectedPadTypes();

              Assert.expect(padTypes != null, "padtypes are null");

              _availableItemsListModel.clear();
              for (PadType padType : padTypes)
                _availableItemsListModel.addElement(padType);
              _selectedItem = null;

              setInitialButtonState();
              validatePanel();
            }
            else if (_availableComponentsComboBox.getSelectedItem() instanceof ComponentType)
            {
              ComponentType selectedComponentType = (ComponentType)_availableComponentsComboBox.getSelectedItem();

              List<PadType> padTypes = null;

              if (_saveOnlyTestedDevices == false)
                padTypes = selectedComponentType.getTestablePadTypes();
              else
                padTypes = selectedComponentType.getInspectedPadTypes();

              Assert.expect(padTypes != null, "pad types are null");

              _availableItemsListModel.clear();
              for (PadType padType : padTypes)
                _availableItemsListModel.addElement(padType);
              _selectedItem = null;

              setInitialButtonState();
              validatePanel();
            }
            else
              Assert.expect(false, "Not of type Component or ComponentType");

            _loadingSwingComponentsWithData = false;
          }
        }
      });

    if (_availableJointTypesComboBox != null)
      _availableJointTypesComboBox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          if (_loadingSwingComponentsWithData == false)
          {
            Object selectedItem = _availableJointTypesComboBox.getSelectedItem();
            Assert.expect(selectedItem instanceof JointTypeEnum);
            _currentSelectedJointType = (JointTypeEnum)selectedItem;
            populateAvailableItemsList();
            _selectedItem = null;
            setInitialButtonState();
            validatePanel();
          }
        }
      });

    if(_availableItemsList != null)
    {
      _availableItemsList.addListSelectionListener(new ListSelectionListener()
      {
        public void valueChanged(ListSelectionEvent evt)
        {
          if (_loadingSwingComponentsWithData == false)
          {
            _selectedItem = _availableItemsList.getSelectedValue();
            validatePanel();
          }
        }
      });
    }

    if (_panelRadioButton != null)
    {
      _panelRadioButton.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _boardScope = false;
          createPanel();
          repaint();
          validate();
          populateJointtypeOrSubtypeOptionsPanel();
//          validatePanel();
        }
      });
    }
    if (_boardInstanceRadioButton != null)
    {
      _boardInstanceRadioButton.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          // the user has selected to do a joint type or subtype test by board scope
          // so create the filter panel and drop it in
          _boardScope = true;
          createPanel();
          repaint();
          validate();
          populateJointtypeOrSubtypeOptionsPanel();
//          validatePanel();
        }
      });
    }
  }

  /**
   * This will create the pin type options panel
   * @author Erica Wheatcroft
   */
  private JPanel createPinTypeOptionsPanel()
  {
    _loadingSwingComponentsWithData = true;
    BorderLayout pinTypeOptionsPanelLayout = new BorderLayout();
    pinTypeOptionsPanelLayout.setVgap(10);
    JPanel pinTypeOptionsPanel = new JPanel(pinTypeOptionsPanelLayout);

    // lets create the filter panel this will have to combo boxes.
    JLabel availableBoardsLabel = new JLabel(StringLocalizer.keyToString("GISKW_AVAILABLE_BOARDS_LABEL_KEY"));
    JLabel availableComponentsLabel = new JLabel(StringLocalizer.keyToString("GISKW_AVAILABLE_COMPONENTS_LABEL_KEY"));
    JPanel filterItemsPanel = new JPanel();
    PairLayout pairLayout = new PairLayout();
    filterItemsPanel.setLayout(pairLayout);

    // now lets create the board type components
    JLabel boardTypeLabel = new JLabel(StringLocalizer.keyToString("GISKW_SELECTED_BOARD_TYPE_LABEL_KEY"));
    filterItemsPanel.add(boardTypeLabel);
    _availableBoardTypesComboBox = new JComboBox();
    _availableBoardTypesComboBox.removeAllItems();
    List<BoardType> boardTypes = _project.getPanel().getBoardTypes();
    for (BoardType type : boardTypes)
      _availableBoardTypesComboBox.addItem(type);

    _availableBoardsComboBox = new JComboBox();
    filterItemsPanel.add(_availableBoardTypesComboBox);
    filterItemsPanel.add(availableBoardsLabel);

    filterItemsPanel.add(_availableBoardsComboBox);
    filterItemsPanel.add(availableComponentsLabel);
    _availableComponentsComboBox = new JComboBox();
    filterItemsPanel.add(_availableComponentsComboBox);
    pinTypeOptionsPanel.add(filterItemsPanel, BorderLayout.NORTH);
    pinTypeOptionsPanel.add(createItemSelectionPanel(), BorderLayout.CENTER);

    _loadingSwingComponentsWithData = false;
    return pinTypeOptionsPanel;
  }

  /**
   * this will create the item selection panel which is just a JList and a label.
   * @author Erica Wheatcroft
   */
  private JPanel createItemSelectionPanel()
  {
     // now lets create the list panel - this panel will contain the list of elements the user can select.
    _availableItemsList = new JList();
    _availableItemsListModel = new DefaultListModel();
    _availableItemsList.setModel(_availableItemsListModel);
    JLabel availableItemsLabel = new JLabel();
    // set the label text.
    if(_currentImageSetType.equals(ImageSetTypeEnum.COMPONENT))
      availableItemsLabel.setText(StringLocalizer.keyToString("GISWK_COMPONENT_TYPE_SELECTION_INSTRUCTION_KEY"));
    else if(_currentImageSetType.equals(ImageSetTypeEnum.JOINT_TYPE))
      availableItemsLabel.setText(StringLocalizer.keyToString("GISWK_JOINTTYPE_TYPE_SELECTION_INSTRUCTION_KEY"));
    else if(_currentImageSetType.equals(ImageSetTypeEnum.SUBTYPE))
      availableItemsLabel.setText(StringLocalizer.keyToString("GISWK_SUBTYPE_TYPE_SELECTION_INSTRUCTION_KEY"));
    else if(_currentImageSetType.equals(ImageSetTypeEnum.PIN))
      availableItemsLabel.setText(StringLocalizer.keyToString("GISWK_PIN_TYPE_SELECTION_INSTRUCTION_KEY"));
    else if(_currentImageSetType.equals(ImageSetTypeEnum.BOARD))
      availableItemsLabel.setText(StringLocalizer.keyToString("GISWK_BOARD_TYPE_SELECTION_INSTRUCTION_KEY"));
    else
      Assert.expect(false, "not a valid image set type");

    _availableItemsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _availableItemsList.setVisibleRowCount(8); // visible row count

    JPanel itemListPanel = new JPanel();
    BorderLayout itemListPanelLayout = new BorderLayout();
    itemListPanel.setLayout(itemListPanelLayout);
    itemListPanel.add(availableItemsLabel,BorderLayout.NORTH);
    _availableItemsScroller = new JScrollPane();
    _availableItemsScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
    _availableItemsScroller.getViewport().add(_availableItemsList);

//    int origWidth = _availableItemsScroller.getPreferredSize().width;
//    int scrollHeight = _availableItemsScroller.getPreferredSize().height;
//    int listHeight =  _availableItemsList.getPreferredSize().height;
//    int prefHeight = scrollHeight;
//    if (listHeight < scrollHeight)
//    {
//      prefHeight = listHeight;
//      Dimension prefScrollSize = new Dimension(origWidth, prefHeight);
//      _availableItemsList.setMaximumSize(prefScrollSize);
//    }
    itemListPanel.add(_availableItemsScroller, BorderLayout.CENTER);

    return itemListPanel;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populatePinTypeOptionsPanel()
  {
    _loadingSwingComponentsWithData = true;

    BoardType boardType = (BoardType)_availableBoardTypesComboBox.getSelectedItem();
    if((_userSwitchedBoardType == true && _userSwitchedBoardInstance == false) ||
      (_userSwitchedBoardType == false && _userSwitchedBoardInstance == false))
    {
      if (boardType == null)
      {
        // first populate the available board types combo box.
        List<BoardType> boardTypes = _project.getPanel().getBoardTypes();
        _availableBoardTypesComboBox.removeAllItems();
        for (BoardType type : boardTypes)
          _availableBoardTypesComboBox.addItem(type);

        // now select the first one board type
        _availableBoardTypesComboBox.setSelectedIndex(0);
        boardType = (BoardType)_availableBoardTypesComboBox.getSelectedItem();

        // second populate the available board instances combo box
        List<Board> boards = boardType.getBoards();
        _availableBoardsComboBox.removeAllItems();

        for (Board board : boards)
          _availableBoardsComboBox.addItem(board);
      }
      else
      {
        // second populate the available board instances combo box
        List<Board> boards = boardType.getBoards();
        _availableBoardsComboBox.removeAllItems();

        for (Board board : boards)
          _availableBoardsComboBox.addItem(board);

      }

      // now add the key word all - allows the user to test a component on all board instances for a given board type
      _availableBoardsComboBox.addItem(StringLocalizer.keyToString("GISKW_ALL_KEYWORD_KEY"));
      _availableBoardsComboBox.setSelectedIndex(0);
    }
    _userSwitchedBoardType = false;
    _userSwitchedBoardInstance = false;
    java.util.List<ComponentType> componentTypes = null;

    if(_saveOnlyTestedDevices == false)
      componentTypes = boardType.getTestableComponentTypes();
    else
      componentTypes = boardType.getInspectedComponentTypes();

    Assert.expect(componentTypes != null, "component types is null");

    // remove all the items.
    _availableComponentsComboBox.removeAllItems();

    // populate the combo box
    for (ComponentType componentType : componentTypes)
      _availableComponentsComboBox.addItem(componentType);

    // get and first board instance and select it in the combo box.
    _availableComponentsComboBox.setSelectedIndex(0);
    ComponentType selectedComponentType = (ComponentType)_availableComponentsComboBox.getSelectedItem();

    // finally populate the list of available pins list
    _availableItemsListModel.clear();

    java.util.List<PadType> padTypes = null;

    if(_saveOnlyTestedDevices == false)
      padTypes = selectedComponentType.getTestablePadTypes();
    else
      padTypes = selectedComponentType.getInspectedPadTypes();

    Assert.expect(padTypes != null, "padTypes are null");

    for (PadType padType : padTypes)
      _availableItemsListModel.addElement(padType);

    _loadingSwingComponentsWithData = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  private JPanel createJointtypeOrSubtypeOptionsPanel()
  {
    JPanel jointTypeOrSubtypeOptionsPanel = new JPanel();
    BorderLayout borderLayout = new BorderLayout();
    borderLayout.setVgap(2);
    jointTypeOrSubtypeOptionsPanel.setLayout(borderLayout);

    // now create the filter panel which will allow the user to sort their joint types and / or subtypes by
    // board instance or by panel
    JPanel instructionsPanel = new JPanel();
    instructionsPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder()), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    BorderLayout instructionsPanelLayout = new BorderLayout();
    instructionsPanelLayout.setVgap(3);
    instructionsPanel.setLayout(instructionsPanelLayout);
    instructionsPanel.add(_instructionsLabel, BorderLayout.NORTH);

    JPanel radioButtonPanel = new JPanel();
    GridLayout radioButtonPanelLayout = new GridLayout();
    radioButtonPanelLayout.setColumns(2);
    radioButtonPanelLayout.setHgap(0);
    radioButtonPanel.setLayout(radioButtonPanelLayout);
    ButtonGroup radioButtonGroup = new ButtonGroup();
    _panelRadioButton = new JRadioButton(StringLocalizer.keyToString("GISWK_ENTIRE_PANEL_KEY"), !_boardScope);
    _boardInstanceRadioButton = new JRadioButton(StringLocalizer.keyToString("GISWK_BOARD_KEY"), _boardScope);
    radioButtonGroup.add(_panelRadioButton);
    radioButtonGroup.add(_boardInstanceRadioButton);
    radioButtonPanel.add(_panelRadioButton);
    radioButtonPanel.add(_boardInstanceRadioButton);
    instructionsPanel.add(radioButtonPanel, BorderLayout.CENTER);

    // add the instructions panel
    jointTypeOrSubtypeOptionsPanel.add(instructionsPanel, BorderLayout.NORTH);
    JPanel centerPanel = createCenterPanel();
    jointTypeOrSubtypeOptionsPanel.add(centerPanel, BorderLayout.CENTER);

    return jointTypeOrSubtypeOptionsPanel;
  }


  /**
   * @author Erica Wheatcroft
   */
  private JPanel createCenterPanel()
  {
    JPanel centerPanel = new JPanel();
    BorderLayout centerPanelLayout = new BorderLayout();
    centerPanel.setLayout(centerPanelLayout);
    centerPanelLayout.setVgap(5);
    centerPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder()),
                                                             BorderFactory.createEmptyBorder(5, 5, 5, 5)));

    if(_currentImageSetType.equals(ImageSetTypeEnum.SUBTYPE) ||
       _currentImageSetType.equals(ImageSetTypeEnum.JOINT_TYPE))
    {
      JPanel filterItemsPanel = createFilterPanelForJointtypeOrSubtypeOptionsPanel();
      centerPanel.add(filterItemsPanel, BorderLayout.NORTH);
    }

    // create the item selection panel
    JPanel itemSelectionPanel = createItemSelectionPanel();
    centerPanel.add(itemSelectionPanel, BorderLayout.CENTER);

    return centerPanel;
  }

  /**
   * @author Erica Wheatcroft
   */
  private JPanel createFilterPanelForJointtypeOrSubtypeOptionsPanel()
  {
    // now create the filter panel which will allow the user to sort their components, joint types, and subtypes by
    // board instance.
    JLabel availableBoardsLabel = new JLabel(StringLocalizer.keyToString("GISWK_BOARDS_KEY"));
    JPanel panel = new JPanel();
    PairLayout pairLayout = new PairLayout();
    panel.setLayout(pairLayout);

    // now lets create the board type components
    JLabel boardTypeLabel = new JLabel(StringLocalizer.keyToString("GISWK_BOARD_TYPE_KEY"));
    if(_boardScope)
    {
      panel.add(boardTypeLabel);
      _availableBoardsComboBox = new JComboBox();
      _availableBoardTypesComboBox = new JComboBox();
      panel.add(_availableBoardTypesComboBox);
      panel.add(availableBoardsLabel);
      panel.add(_availableBoardsComboBox);
    }

    if(_currentImageSetType.equals(ImageSetTypeEnum.SUBTYPE))
    {
      panel.add(new JLabel(StringLocalizer.keyToString("GISWK_JOINT_TYPE_KEY")));
      _availableJointTypesComboBox = new JComboBox();
      panel.add(_availableJointTypesComboBox);
    }
    return panel;
  }

  /**
   * @author Erica Wheatcroft
   */
  private JPanel createComponentTypeOptionsPanel()
  {
    JPanel componentTypeOptionsPanel = new JPanel();
    BorderLayout borderLayout = new BorderLayout();
    borderLayout.setVgap(10);
    componentTypeOptionsPanel.setLayout(borderLayout);

    // now create the filter panel which will allow the user to sort their components, joint types, and subtypes by
    // board instance.
    JLabel availableBoardsLabel = new JLabel(StringLocalizer.keyToString("GISKW_AVAILABLE_BOARDS_LABEL_KEY"));
    JPanel filterItemsPanel = new JPanel();
    PairLayout pairLayout = new PairLayout();
    filterItemsPanel.setLayout(pairLayout);

    // now lets create the board type components
    JLabel boardTypeLabel = new JLabel(StringLocalizer.keyToString("GISKW_SELECTED_BOARD_TYPE_LABEL_KEY"));
    filterItemsPanel.add(boardTypeLabel);
    _availableBoardTypesComboBox = new JComboBox();
    filterItemsPanel.add(_availableBoardTypesComboBox);
    filterItemsPanel.add(availableBoardsLabel);
    _availableBoardsComboBox = new JComboBox();
    filterItemsPanel.add(_availableBoardsComboBox);
    componentTypeOptionsPanel.add(filterItemsPanel, BorderLayout.NORTH);
    componentTypeOptionsPanel.add(createItemSelectionPanel(), BorderLayout.CENTER);

    return componentTypeOptionsPanel;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateJointtypeOrSubtypeOptionsPanel()
  {
    Assert.expect(_project != null, "Project is null");
    Assert.expect(_availableItemsList != null, "available items list is null");

    _loadingSwingComponentsWithData = true;

    if(_boardScope == true)
    {
      BoardType boardType = (BoardType)_availableBoardTypesComboBox.getSelectedItem();
      if((_userSwitchedBoardType == true && _userSwitchedBoardInstance == false) ||
      (_userSwitchedBoardType == false && _userSwitchedBoardInstance == false))
      {
        if (boardType == null)
        {
          // first populate the available board types combo box.
          List<BoardType> boardTypes = _project.getPanel().getBoardTypes();
          _availableBoardTypesComboBox.removeAllItems();
          for (BoardType type : boardTypes)
            _availableBoardTypesComboBox.addItem(type);

          // now select the first one board type
          _availableBoardTypesComboBox.setSelectedIndex(0);
          boardType = (BoardType)_availableBoardTypesComboBox.getSelectedItem();

          // second populate the available board instances combo box
          List<Board> boards = boardType.getBoards();
          _availableBoardsComboBox.removeAllItems();

          for (Board board : boards)
            _availableBoardsComboBox.addItem(board);
        }
        else
        {
          // second populate the available board instances combo box
          List<Board> boards = boardType.getBoards();
          _availableBoardsComboBox.removeAllItems();

          for (Board board : boards)
            _availableBoardsComboBox.addItem(board);

        }
        // now add the key word all - allows the user to test a component on all board instances for a given board type
        _availableBoardsComboBox.addItem(StringLocalizer.keyToString("GISKW_ALL_KEYWORD_KEY"));
        _availableBoardsComboBox.setSelectedIndex(0);

      }
      _userSwitchedBoardType = false;
      _userSwitchedBoardInstance = false;
      if (_currentImageSetType.equals(ImageSetTypeEnum.SUBTYPE))
      {
        List<JointTypeEnum> jointTypeEnums = null;

        if(_saveOnlyTestedDevices == false)
          jointTypeEnums = boardType.getTestableJointTypeEnums();
        else
          jointTypeEnums = boardType.getInspectedJointTypeEnums();

        Assert.expect(jointTypeEnums != null, "joint type enums is null");

        _availableJointTypesComboBox.removeAllItems();
        for (JointTypeEnum jointTypeEnum : jointTypeEnums)
          _availableJointTypesComboBox.addItem(jointTypeEnum);
        _availableJointTypesComboBox.setSelectedIndex(0);
        _currentSelectedJointType = (JointTypeEnum)_availableJointTypesComboBox.getSelectedItem();
      }
    }

    if (_currentImageSetType.equals(ImageSetTypeEnum.SUBTYPE))
    {
      List<JointTypeEnum> jointTypeEnums = null;

      if(_saveOnlyTestedDevices == false)
        jointTypeEnums = _project.getPanel().getTestableJointTypeEnums();
      else
        jointTypeEnums = _project.getPanel().getInspectedJointTypeEnums();

      Assert.expect(jointTypeEnums != null, "joint types are null");

      _availableJointTypesComboBox.removeAllItems();
      for (JointTypeEnum jointTypeEnum : jointTypeEnums)
        _availableJointTypesComboBox.addItem(jointTypeEnum);
      _availableJointTypesComboBox.setSelectedIndex(0);
      _currentSelectedJointType = (JointTypeEnum)_availableJointTypesComboBox.getSelectedItem();
    }

    populateAvailableItemsList();

    _loadingSwingComponentsWithData = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateAvailableItemsList()
  {
    if(_currentImageSetType.equals(ImageSetTypeEnum.COMPONENT))
    {
      BoardType boardType = (BoardType)_availableBoardTypesComboBox.getSelectedItem();
      Assert.expect(boardType != null, "boardType is null");
      // populate the list with component types for the selected board type.
      java.util.List<ComponentType> componentTypes = null;

      if(_saveOnlyTestedDevices == false)
        componentTypes = boardType.getTestableComponentTypes();
      else
        componentTypes = boardType.getInspectedComponentTypes();

      Assert.expect(componentTypes != null, "Component Types are null");

      _availableItemsListModel.clear();
      for (ComponentType componentType : componentTypes)
        _availableItemsListModel.addElement(componentType);

    }
    else if(_currentImageSetType.equals(ImageSetTypeEnum.JOINT_TYPE))
    {
      if(_boardScope == true)
      {
        BoardType boardType = (BoardType)_availableBoardTypesComboBox.getSelectedItem();
        Assert.expect(boardType != null, "boardType is null");

        java.util.List<JointTypeEnum> jointTypeEnums = null;

        if(_saveOnlyTestedDevices == false)
          jointTypeEnums = boardType.getTestableJointTypeEnums();
        else
          jointTypeEnums = boardType.getInspectedJointTypeEnums();

        Assert.expect(jointTypeEnums != null, "joint type enums are null");

        _availableItemsListModel.clear();
        for (JointTypeEnum jointTypeEnum : jointTypeEnums)
          _availableItemsListModel.addElement(jointTypeEnum);
      }
      else
      {
        Assert.expect(_project != null, "Project is null!");

        java.util.List<JointTypeEnum> jointTypes = null;

        if(_saveOnlyTestedDevices == false)
          jointTypes = _project.getPanel().getTestableJointTypeEnums();
        else
          jointTypes = _project.getPanel().getInspectedJointTypeEnums();

        Assert.expect(jointTypes != null, "joint type enums are null");

        _availableItemsListModel.clear();
        for (JointTypeEnum jointType : jointTypes)
          _availableItemsListModel.addElement(jointType);
      }
    }
    else if(_currentImageSetType.equals(ImageSetTypeEnum.SUBTYPE))
    {
      Assert.expect(_currentSelectedJointType != null, " no joint type has been selected");
      if(_boardScope == true)
      {
        BoardType boardType = (BoardType)_availableBoardTypesComboBox.getSelectedItem();
        Assert.expect(boardType != null, "boardType is null");

        java.util.List<Subtype> subtypes = null;

        if(_saveOnlyTestedDevices == false)
          subtypes = boardType.getTestableSubtypes(_currentSelectedJointType);
        else
          subtypes = boardType.getInspectedSubtypes(_currentSelectedJointType);

        Assert.expect(subtypes != null, "subtypes is null");

        _availableItemsListModel.clear();
        for (Subtype subtype : subtypes)
          _availableItemsListModel.addElement(subtype);
      }
      else
      {
        Assert.expect(_project != null, "Project is null!");
        java.util.List<Subtype> subtypes = null;

        if(_saveOnlyTestedDevices == false)
          subtypes = _project.getPanel().getTestableSubtypes(_currentSelectedJointType);
        else
          subtypes = _project.getPanel().getInspectedSubtypes(_currentSelectedJointType);

        Assert.expect(subtypes != null, "subtypes is null");

        _availableItemsListModel.clear();
        for (Subtype subtype : subtypes)
          _availableItemsListModel.addElement(subtype);
      }
    }
    else
      Assert.expect(false, "Image Set Type is not component, joint type, or subtype");

    _availableItemsScroller.setPreferredSize(_availableItemsList.getMaximumSize());
    setInitialButtonState();
    // now select the first one in the list.
//    if(_availableItemsListModel.isEmpty() == false)
//    {
//      _availableItemsList.setSelectedIndex(0);
//      if (_selectedItem == null)
//        _selectedItem = _availableItemsList.getSelectedValue();
//      validatePanel();
//    }
  }

  /**
   * This will create the board type image set options panel. This will create the components only and not populate them
   *
   * @author Erica Wheatcroft
   */
  private JPanel createBoardTypeOptionsPanel()
  {
    return createItemSelectionPanel();
  }

  /**
   * This method will populate the list of available board instances for the currently loaded program.
   * @author Erica Wheatcroft
   */
  private void populateBoardTypeOptionsPanel(BoardType selectedBoardType)
  {
    Assert.expect(_project != null, "Project is null");
    Assert.expect(_availableItemsList != null, "list is null");

    _loadingSwingComponentsWithData = true;

    if(selectedBoardType == null)
    {
    // first populate the available board types combo box.
    List<BoardType> boardTypes = _project.getPanel().getBoardTypes();
    _availableBoardTypesComboBox.removeAllItems();
    for (BoardType type : boardTypes)
      _availableBoardTypesComboBox.addItem(type);

    // now select the first one board type
    _availableBoardTypesComboBox.setSelectedIndex(0);
    selectedBoardType = (BoardType)_availableBoardTypesComboBox.getSelectedItem();
    }

    _availableItemsListModel.clear();
    java.util.List<Board> boards = selectedBoardType.getBoards();

    for(Board board : boards)
      _availableItemsListModel.addElement(board);

    _loadingSwingComponentsWithData = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void populatePanelWithData()
  {
    _saveOnlyTestedDevices = _dialog.saveOnlyTestedDevices();

    setInitialButtonState();

    // set the title

    _dialog.changeTitle(StringLocalizer.keyToString("GISWK_SCREEN_3_TITLE_KEY"));

    // set the current type as this could have change in the main screen of the wizzard
    _currentImageSetType = _imageSetData.getImageSetTypeEnum();

    // since this method is called whenever the wizard is switching to this panel we want to make sure our selected
    // item is null
//    _selectedItem = null;

    // create the center panel;
    createPanel();

    _loadingSwingComponentsWithData = true;
//    validatePanel();
    _loadingSwingComponentsWithData = false;
    populateWithCurrentSelections();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateWithCurrentSelections()
  {
    if (_selectedItem != null)
    {
      if (_currentImageSetType.equals(ImageSetTypeEnum.BOARD))
      {
        if ((_selectedItem instanceof Board) == false)
          return;
        Board currentBoard = (Board)_selectedItem;
        _availableBoardTypesComboBox.setSelectedItem(currentBoard.getBoardType());
        _availableItemsList.setSelectedValue(currentBoard, true);
      }
      else if (_currentImageSetType.equals(ImageSetTypeEnum.COMPONENT))
      {
        if ((_selectedItem instanceof ComponentType) == false)
          return;
        boolean collectImagesOnAllBoardInstances = _imageSetData.collectImagesOnAllBoardInstances();
        Board board = _imageSetData.getBoardInstanceToAcquireImagesAgainst();
        ComponentType currentComponentType = (ComponentType)_selectedItem;
        _availableBoardTypesComboBox.setSelectedItem(board.getBoardType());
        if (collectImagesOnAllBoardInstances)
          _availableBoardsComboBox.setSelectedItem(StringLocalizer.keyToString("GISKW_ALL_KEYWORD_KEY"));
        else
          _availableBoardsComboBox.setSelectedItem(board);
        _availableItemsList.setSelectedValue(currentComponentType, true);
      }
      else if (_currentImageSetType.equals(ImageSetTypeEnum.PIN))
      {
        if ((_selectedItem instanceof PadType) == false)
          return;
        boolean collectImagesOnAllBoardInstances = _imageSetData.collectImagesOnAllBoardInstances();
        Board board = _imageSetData.getBoardInstanceToAcquireImagesAgainst();

        PadType selectedPadType = (PadType)_selectedItem;
        _availableBoardTypesComboBox.setSelectedItem(board.getBoardType());
        if (collectImagesOnAllBoardInstances)
          _availableBoardsComboBox.setSelectedItem(StringLocalizer.keyToString("GISKW_ALL_KEYWORD_KEY"));
        else
          _availableBoardsComboBox.setSelectedItem(board);

        _availableComponentsComboBox.setSelectedItem(selectedPadType.getComponentType());
        _availableItemsList.setSelectedValue(selectedPadType, true);
      }
      else if (_currentImageSetType.equals(ImageSetTypeEnum.JOINT_TYPE))
      {
        if ((_selectedItem instanceof JointTypeEnum) == false)
          return;
        JointTypeEnum jointTypeEnum = (JointTypeEnum)_selectedItem;

        _boardInstanceRadioButton.setSelected(_boardScope);

        Board board = _imageSetData.getBoardInstanceToAcquireImagesAgainst();
        _availableBoardTypesComboBox.setSelectedItem(board.getBoardType());
        if (_boardScope)
        {
          boolean collectImagesOnAllBoardInstances = _imageSetData.collectImagesOnAllBoardInstances();
          if (collectImagesOnAllBoardInstances)
            _availableBoardsComboBox.setSelectedItem(StringLocalizer.keyToString("GISKW_ALL_KEYWORD_KEY"));
          else
            _availableBoardsComboBox.setSelectedItem(board);
        }

        _availableItemsList.setSelectedValue(jointTypeEnum, true);
      }
      else if (_currentImageSetType.equals(ImageSetTypeEnum.SUBTYPE))
      {
        if ((_selectedItem instanceof Subtype) == false)
          return;
        Subtype subtype = (Subtype)_selectedItem;

        _boardInstanceRadioButton.setSelected(_boardScope);

        Board board = _imageSetData.getBoardInstanceToAcquireImagesAgainst();
        _availableBoardTypesComboBox.setSelectedItem(board.getBoardType());
        if (_boardScope)
        {
          boolean collectImagesOnAllBoardInstances = _imageSetData.collectImagesOnAllBoardInstances();
          if (collectImagesOnAllBoardInstances)
            _availableBoardsComboBox.setSelectedItem(StringLocalizer.keyToString("GISKW_ALL_KEYWORD_KEY"));
          else
            _availableBoardsComboBox.setSelectedItem(board);
        }

        _availableJointTypesComboBox.setSelectedItem(subtype.getJointTypeEnum());
        _availableItemsList.setSelectedValue(subtype, true);
      }
      else
        Assert.expect(false, "unknown image set type");
    }

  }

  /**
   * @author Erica Wheatcroft
   */
  public void validatePanel()
  {
    //in order for this secondary screen to be complete the select item must not be null.
    if(_selectedItem != null)
    {
      // set if we are testing using a board type or instance
      _imageSetData.setCollectImagesOnAllBoardInstances(_collectImagesOnAllBoardInstances);

      // set if we are testing using a panel scope
      _imageSetData.setCollectImagesForPanel( ! _boardScope);

      // available boards combo box is not available for subtype an jointype when dealing with panel scope.
      if((BoardType) _availableBoardTypesComboBox.getSelectedItem() != null)
        _imageSetData.setBoardTypeToAcquireImagesAgainst( (BoardType) _availableBoardTypesComboBox.getSelectedItem() );

//      // available boards combo box is not available for board image set.
//      if (_availableBoardsComboBox != null)
//      {
//        Object selectedBoard = _availableBoardsComboBox.getSelectedItem();
//        if (selectedBoard != null && (selectedBoard instanceof String) == false)
//          if (_loadingSwingComponentsWithData == false)
//            _imageSetData.setBoardInstanceToAcquireImagesAgainst((Board)selectedBoard);
//      }
      _imageSetData.setCollectImagesForPanel(! _boardScope);

      if (_currentImageSetType.equals(ImageSetTypeEnum.BOARD))
      {
        if (_selectedItem instanceof Board)
          _imageSetData.setBoardToAcquireImagesFor((Board) _selectedItem);
        else
          _selectedItem = null;
      }
      else if (_currentImageSetType.equals(ImageSetTypeEnum.COMPONENT))
      {
        if (_selectedItem instanceof ComponentType)
        {
          _imageSetData.setComponentTypeToAcquireImagesFor((ComponentType)_selectedItem);
          BoardType boardType = (BoardType)_availableBoardTypesComboBox.getSelectedItem();
          _imageSetData.setBoardTypeToAcquireImagesAgainst(boardType);
          Object board = _availableBoardsComboBox.getSelectedItem();
          if (board instanceof String)// must the "All"
          {
            _imageSetData.setCollectImagesOnAllBoardInstances(true);
            _imageSetData.setBoardInstanceToAcquireImagesAgainst(boardType.getBoards().get(0));
          }
          else
          {
            _imageSetData.setBoardInstanceToAcquireImagesAgainst((Board)board);
          }
        }
        else
          _selectedItem = null;
      }
      else if (_currentImageSetType.equals(ImageSetTypeEnum.PIN))
      {
        if (_selectedItem instanceof PadType)
        {
          PadType selectedPadType = (PadType)_selectedItem;
          _imageSetData.setPadTypeToAcquireImagesFor(selectedPadType);

          BoardType boardType = (BoardType)_availableBoardTypesComboBox.getSelectedItem();
          _imageSetData.setBoardTypeToAcquireImagesAgainst(boardType);
          Object board = _availableBoardsComboBox.getSelectedItem();
          if (board instanceof String) // must the "All"
          {
            _imageSetData.setCollectImagesOnAllBoardInstances(true);
            _imageSetData.setBoardInstanceToAcquireImagesAgainst(boardType.getBoards().get(0));
          }
          else
          {
            _imageSetData.setBoardInstanceToAcquireImagesAgainst((Board)board);
          }

        }
        else
          _selectedItem = null;
      }
      else if (_currentImageSetType.equals(ImageSetTypeEnum.JOINT_TYPE))
      {
        if (_selectedItem instanceof JointTypeEnum)
        {
          _imageSetData.setJointTypeEnumToAcquireImagesFor((JointTypeEnum) _selectedItem);
          BoardType boardType = (BoardType)_availableBoardTypesComboBox.getSelectedItem();
          _imageSetData.setBoardTypeToAcquireImagesAgainst(boardType);
          Object board = _availableBoardsComboBox.getSelectedItem();
          if (board instanceof String)// must the "All"
          {
            _imageSetData.setCollectImagesOnAllBoardInstances(true);
            _imageSetData.setBoardInstanceToAcquireImagesAgainst(boardType.getBoards().get(0));
          }
          else
          {
            _imageSetData.setCollectImagesOnAllBoardInstances(false);
            _imageSetData.setBoardInstanceToAcquireImagesAgainst((Board)board);
          }
        }
        else
          _selectedItem = null;
      }
      else if (_currentImageSetType.equals(ImageSetTypeEnum.SUBTYPE))
      {
        if (_selectedItem instanceof Subtype)
        {
          _imageSetData.setSubtypeToAcquireImagesFor((Subtype)_selectedItem);
          BoardType boardType = (BoardType)_availableBoardTypesComboBox.getSelectedItem();
          _imageSetData.setBoardTypeToAcquireImagesAgainst(boardType);
          Object board = _availableBoardsComboBox.getSelectedItem();
          if (board instanceof String) // must the "All"
          {
            _imageSetData.setCollectImagesOnAllBoardInstances(true);
            _imageSetData.setBoardInstanceToAcquireImagesAgainst(boardType.getBoards().get(0));
          }
          else
          {
            _imageSetData.setCollectImagesOnAllBoardInstances(false);
            _imageSetData.setBoardInstanceToAcquireImagesAgainst((Board)board);
          }
        }
        else
          _selectedItem = null;
      }
      else
        Assert.expect(false,"unknown image set type");

      WizardButtonState buttonState = new WizardButtonState(true, true, false, true);
      _observable.updateButtonState(buttonState);
    }
    else
    {
      // do nothing we are not ready to go to the next screen
    }

  }

  /**
   * This method will set up the button state for this screen of the wizzard
   *
   * @author Erica Wheatcroft
   */
  private void setInitialButtonState()
  {
    WizardButtonState buttonState = new WizardButtonState(false, true, false, true);
    _observable.updateButtonState(buttonState);
  }
}
