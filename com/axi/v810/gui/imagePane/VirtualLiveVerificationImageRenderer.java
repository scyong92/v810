package com.axi.v810.gui.imagePane;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.util.image.ImageIoUtil;
import com.axi.v810.business.panelDesc.*;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.image.*;

/**
 * This class is responsible for drawing the Panel outline.
 *
 * @author bee-hoon.goh
 */
public class VirtualLiveVerificationImageRenderer extends ShapeRenderer
{
  private Panel _panel;
  private BufferedImage _panelImage = null;

  /**
   * @author bee-hoon.goh
   */
  public VirtualLiveVerificationImageRenderer(BufferedImage img, Panel panel)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(panel != null);
    
    _panel = panel;
    //_bufferedImage = img;
    
    refreshData();
  }

  /**
   * Set Virtual Live Verification Image
   * @author bee-hoon.goh
   */
  public void setPanelImage(BufferedImage panelImage)
  {
    _panelImage = panelImage;
  }
  
  /**
   * @author bee-hoon.goh
   */
  public String toString()
  {
    return _panel.getProject().getName();
  }

  /**
   * @author bee-hoon.goh
   */
  protected void refreshData()
  {
    Assert.expect(_panel != null);

    initializeShape(_panel.getShapeInNanoMeters());
  }

  /**
   * @author bee-hoon.goh
   */
  public Panel getPanel()
  {
    Assert.expect(_panel != null);

    return _panel;
  }
  
  /**
   * @author bee-hoon.goh
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;
    Color origColor = graphics2D.getColor();
    graphics2D.setColor(getForeground());
    Stroke origStroke = graphics2D.getStroke();
    if(hasStroke())
      graphics2D.setStroke(new BasicStroke(2));
    graphics2D.setColor(origColor);
    graphics2D.setStroke(origStroke);
    
    // bee-hoon.goh
    BufferedImage resizePanelImage = null;
    Graphics2D resizePanelGraphic = null;
    if(_panelImage != null)
    {
      double scaleWidth = _panelImage.getWidth() / _widthInPixels;
      double scaleHeight = _panelImage.getHeight() / _heightInPixels;
      int resizeImageWidthInPixels = (int) Math.floor(_panelImage.getWidth() / scaleWidth);
      int resizeImageHeightInPixels = (int) Math.floor(_panelImage.getHeight() / scaleHeight);
      resizePanelImage = new BufferedImage(resizeImageWidthInPixels, resizeImageHeightInPixels, BufferedImage.TYPE_BYTE_GRAY);
      resizePanelGraphic = resizePanelImage.createGraphics();
      resizePanelGraphic.drawImage(_panelImage, 0, 0, resizeImageWidthInPixels, resizeImageHeightInPixels, null);
      resizePanelGraphic.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
      resizePanelGraphic.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);    
      graphics2D.drawImage(resizePanelImage, null, 0, 0);
    }
  }
}
