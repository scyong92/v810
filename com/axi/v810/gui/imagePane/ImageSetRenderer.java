package com.axi.v810.gui.imagePane;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * <p>Title: ImageSetRenderer</p>
 *
 * <p>Description: This cell renderer will be used in tables that display image sets. This was created in order to
 * diplay the user defined description for the image set.</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: AXI</p>
 *
 * @author Erica Wheatcroft
 */
public class ImageSetRenderer extends JLabel implements TableCellRenderer
{

  /**
   * @author Erica Wheatcroft
   */
  public ImageSetRenderer()
  {
    // do nothing.
  }

  /**
   * @author Erica Wheatcroft
   */
  public Component getTableCellRendererComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 boolean hasFocus,
                                                 int row,
                                                 int column)
  {
    Assert.expect(table != null, "table is null");
    Assert.expect(column >= 0, "column index is negative");
    Assert.expect(row >= 0, "Row index is negative");

    Color alternativeColor = (Color) UIManager.get ("Table.alternateRowColor");
    Color backgroundColor = Color.WHITE;
    
    setOpaque(true);
    if (isSelected)
    {
      setForeground(table.getSelectionForeground());
      setBackground(table.getSelectionBackground());
    }
    else
    {
      setForeground(table.getForeground());
      if(row%2 == 0)
        setBackground(backgroundColor);
      else
        setBackground(alternativeColor);
    }

    if(table.isRowSelected(row))
      setBackground(table.getSelectionBackground());
    else
    {
      if (hasFocus)
        setBackground(table.getSelectionBackground());
      else
      {
        if (row % 2 == 0)
        {
          setBackground(backgroundColor);
        }
        else
        {
          setBackground(alternativeColor);
        }
      }
    }

    ImageSetData selectedImageSet = ((AvailableAndSelectedImageSetsTableModel)table.getModel()).getSelectedImageSet(row);
    setText(selectedImageSet.getSystemDescription());
    setToolTipText(selectedImageSet.getUserDescription());

    return this;
  }

}
