package com.axi.v810.gui.imagePane;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.List;

/**
 * This class is the post processing dialog for expert user.
 * Extra post processing setting is allow to adjust here.
 * 
 * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
 */
public class PostProcessingDialog extends JDialog
{
  ImageProcessingToolBar _imageProcessingToolBar = null;
     
  private JPanel _northPanel = new JPanel();
  private JPanel _centerPanel = new JPanel();
  private JPanel _southPanel = new JPanel();

  private BorderLayout _mainBorderLayout = new BorderLayout(5, 5);
  private FlowLayout _centerFlowLayout = new FlowLayout(FlowLayout.CENTER, 2, 2);  
  
  private PostProcessingTableModel _postProcessingTableModel = new PostProcessingTableModel();
  private JTable _parameterTable = new JTable(_postProcessingTableModel)
  {     
    JFormattedTextField floatTextField = new JFormattedTextField(java.text.NumberFormat.FRACTION_FIELD);
    JFormattedTextField integerTextField = new JFormattedTextField(java.text.NumberFormat.INTEGER_FIELD);
       
    private TableCellEditor resizeCellEditor = new DefaultCellEditor(PostProcessingTableModel.resizeMethodComboBox);
    private TableCellEditor CLAHECellEditor = new DefaultCellEditor(PostProcessingTableModel.CLAHEMethodComboBox);
    private TableCellEditor backgroundFilterCellEditor = new DefaultCellEditor(PostProcessingTableModel.backgroundFilterMethodComboBox);
    private TableCellEditor floatCellEditor = new DefaultCellEditor(floatTextField);
    private TableCellEditor integerCellEditor = new DefaultCellEditor(integerTextField);

    public TableCellEditor getCellEditor(int row, int column)
    {
      int modelColumn = convertColumnIndexToModel(column);
      int modelRow = convertRowIndexToModel(row);
      if (modelColumn == PostProcessingTableModel.ColumnType.Method && row == 0)
        return resizeCellEditor;
      else if (modelColumn == PostProcessingTableModel.ColumnType.Method && row == 1)
        return CLAHECellEditor;
      else if (modelColumn == PostProcessingTableModel.ColumnType.Method && row == 2)
        return backgroundFilterCellEditor;
      else if (modelColumn == PostProcessingTableModel.ColumnType.Value && row == 0)
        return floatCellEditor;
      else if (modelColumn == PostProcessingTableModel.ColumnType.Value && row == 2)
        return integerCellEditor;
      else
        return super.getCellEditor(row, column);
    }
  };
  
  private JScrollPane _tableScrollPane = new JScrollPane(_parameterTable);//so that table header will show
  
  private JButton  _applyButton = new JButton();
  private JButton  _copyToPreProcessingButton = new JButton();
  private JButton  _resetButton = new JButton();
  private JButton  _closeButton = new JButton();
  private JButton  _factoryDefaultSettingButton = new JButton();
  private JCheckBox _previewCheckBox = new JCheckBox();
  
  Object[][] _EnhancerCurrentData2 = _postProcessingTableModel.DEFAULT_DATA;
 
  public PostProcessingDialog(Frame frame,
    String title,
    boolean modal,
    ImageProcessingToolBar imageProcessingToolBar)
  {
    super(frame, title, modal);
    _imageProcessingToolBar = imageProcessingToolBar;
    jbInit();    
    pack();
    setPreferredSize(new Dimension(550,350));    
  }
  
  /**
   * @author Lim, Lay Ngor
   */
  private void jbInit()
  {
    setLayout(_mainBorderLayout);
    add(_northPanel, BorderLayout.NORTH);
    add(_centerPanel, BorderLayout.CENTER);
    add(_southPanel, BorderLayout.SOUTH);
    
    _northPanel.setLayout(_centerFlowLayout);
    _centerPanel.setLayout(_centerFlowLayout);
    _southPanel.setLayout(_centerFlowLayout);        
    
    postProcessingTableAndButton();             
    
    _northPanel.add(_tableScrollPane);
    _centerPanel.add(_previewCheckBox);
    _centerPanel.add(_applyButton);    
//    _centerPanel.add(_copyToPreProcessingButton);//temporary not support the preprocessing as no actual implementation yet.
    _southPanel.add(_resetButton);    
    _southPanel.add(_factoryDefaultSettingButton);        
    _southPanel.add(_closeButton);     
  }
   
  void postProcessingTableAndButton()
  {
    _parameterTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);//will auto adjust table
    _parameterTable.setPreferredScrollableViewportSize(_parameterTable.getPreferredSize());
    _parameterTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);        
    _parameterTable.setRowSelectionAllowed(true);

    //Will enter this action when an cell is "enter" after edit.
    Action action = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        if(_previewCheckBox.isSelected())
        {
          postProcessingImplementation();//Call this if want real time respond.
        }
      }
    };
            
    _previewCheckBox.setToolTipText("Preview And Apply Post Processing");
    _previewCheckBox.setText(StringLocalizer.keyToString("Preview And Apply"));
    _previewCheckBox.setSelected(false);
    _previewCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        previewCheckBox_actionPerformed();
      }
    });
    
    _applyButton.setToolTipText("Apply Post Processing");
    _applyButton.setText(StringLocalizer.keyToString("GUI_APPLY_KEY"));
    _applyButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        applyButton_actionPerformed();
      }
    });
    
    //Not yet apply the real implementation
    _copyToPreProcessingButton.setToolTipText("Copy Pre-Processing");
    _copyToPreProcessingButton.setText(StringLocalizer.keyToString("GUI_APPLY_KEY"));
    _copyToPreProcessingButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        copyToPreProcessingButton_actionPerformed();
      }
    });      
      
    _resetButton.setToolTipText("Reset image to original");
    _resetButton.setText(StringLocalizer.keyToString("DCGUI_RESET_TO_ORIGINAL_KEY"));
    _resetButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        resetButton_actionPerformed(e);
      }
    });  
        
    _factoryDefaultSettingButton.setToolTipText("Reset to Factory Default Setting for Post Processing");
    _factoryDefaultSettingButton.setText("Factory Default");
    _factoryDefaultSettingButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        factoryDefaultSettingButton_actionPerformed(e);
      }
    });
    
    _closeButton.setToolTipText("Exit Post Processing Dialog");
    _closeButton.setText(StringLocalizer.keyToString("GUI_EXIT_BUTTON_KEY"));
    _closeButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        closeButton_actionPerformed(e);
      }
    });     
    
    this.addWindowListener(new WindowAdapter()
    {
      public void windowClosing(WindowEvent e)
      {
        closeDialogAction();
        super.windowClosing(e);
      }
    });    
  }
  
  /**
   * Post Processing actual Implementation.
   * @author Lim, Lay Ngor
   */
  void postProcessingImplementation()
  {
    Assert.expect(_imageProcessingToolBar != null);            
    List<ImageEnhancerBase> enhancerList = getPostProcessingEnhancerList();    
    _imageProcessingToolBar.imagePostProcessingAction(enhancerList, false); 
  }  
  
  /**
   * Get Post Processing Enhancer List - not support FFT & colour yet
   * @author Lim, Lay Ngor
   */
  List<ImageEnhancerBase> getPostProcessingEnhancerList()
  {
    Assert.expect(_imageProcessingToolBar != null);    
    
    int blockSize = (int)_imageProcessingToolBar.getBlockSize();   
    if(blockSize == -1)
      blockSize = 30;//use default block size!
    
    List<ImageEnhancerBase> enhancerList = new ArrayList<ImageEnhancerBase>();
    if(_postProcessingTableModel.getValue(0, PostProcessingTableModel.ColumnType.PostProcess) == true)
    {
        float scale = Float.parseFloat((String) _EnhancerCurrentData2[0][PostProcessingTableModel.ColumnType.Value]);
        
        InterpolationModeEnum resizeMethod = InterpolationModeEnum.Linear; //fix
        if(_EnhancerCurrentData2[0][PostProcessingTableModel.ColumnType.Method] 
          == PostProcessingTableModel.ResizeMethod[1])
          resizeMethod = InterpolationModeEnum.Cubic;
            
        ImageEnhancerBase enhancer = new ResizeImage(scale, scale, resizeMethod);
        enhancerList.add(enhancer);
    }
    
    if(_postProcessingTableModel.getValue(1, PostProcessingTableModel.ColumnType.PostProcess) == true)
    {
        int blockRadius = blockSize;
        int bin = 256;//fix to default value
        int slope = Integer.parseInt((String) _EnhancerCurrentData2[1][PostProcessingTableModel.ColumnType.Value]); 
        ImageEnhancerBase enhancer = new CLAHE(blockRadius, bin, slope, true);           
        enhancerList.add(enhancer);
    }

    if(_postProcessingTableModel.getValue(2, PostProcessingTableModel.ColumnType.PostProcess) == true)
    {          
        int maskSize = blockSize;
        int iteration = Integer.parseInt((String) _EnhancerCurrentData2[2][PostProcessingTableModel.ColumnType.Value]);        
        ImageEnhancerBase enhancer = new RemoveArtifactByBoxFilter(maskSize, maskSize, iteration);                       
        enhancerList.add(enhancer);
    } 
    
    return enhancerList;
  }    
  
  /**
   * Pre-Processing actual Implementation - no actual implementation yet.
   * @author Lim, Lay Ngor
   */
  void preProcessingImplementation()
  {
    Assert.expect(_imageProcessingToolBar != null);            
    //Update Pre-Processing Enhancer Setting
    if (_postProcessingTableModel.getValue(0, PostProcessingTableModel.ColumnType.PreProcess) == true)
    {
      //Update scale and method at Pre-Processing side.
      float scale = Float.parseFloat((String) _EnhancerCurrentData2[0][PostProcessingTableModel.ColumnType.Value]);

      InterpolationModeEnum resizeMethod = InterpolationModeEnum.Linear;//fix
      if (_EnhancerCurrentData2[0][PostProcessingTableModel.ColumnType.Method]
        == PostProcessingTableModel.ResizeMethod[1])
      {
        resizeMethod = InterpolationModeEnum.Cubic;
      }

      //Actual implementation shall update to subtype file!
//        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_SCALE = scale
    }

    if (_postProcessingTableModel.getValue(1, PostProcessingTableModel.ColumnType.PreProcess) == true)
    {
      int slope = Integer.parseInt((String) _EnhancerCurrentData2[1][PostProcessingTableModel.ColumnType.Value]);
      //Actual implementation shall update slope at Pre-Processing side to subtype file!       
    }

    if (_postProcessingTableModel.getValue(2, PostProcessingTableModel.ColumnType.PreProcess) == true)
    {      
      int iteration = Integer.parseInt((String) _EnhancerCurrentData2[2][PostProcessingTableModel.ColumnType.Value]);
      //Actual implementation shall update iteration at Pre-Processing side to subtype file!       
//        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_ITERATOR = iteration;
    }    
  }  
  
  /**
   * @author Lim, Lay Ngor
   */
  void applyButton_actionPerformed()
  {
    postProcessingImplementation();
  }
  
  /**
   * @author Lim, Lay Ngor
   */  
  void copyToPreProcessingButton_actionPerformed()
  {
    preProcessingImplementation();
  }

  /**
   * @author Lim, Lay Ngor
   */
  void closeButton_actionPerformed(ActionEvent e)
  {
    closeDialogAction();
    dispose();
  }

  /**
   * @author Lim, Lay Ngor
   */  
  void closeDialogAction()
  {
    Assert.expect(_imageProcessingToolBar != null);    
    _imageProcessingToolBar.setpostProcessingDialogToggleButtonSelected(false);
  }

  /**
   * @author Lim, Lay Ngor
   */  
  void factoryDefaultSettingButton_actionPerformed(ActionEvent e)
  {
    _postProcessingTableModel.resetDataColumnToDefaultValue(PostProcessingTableModel.ColumnType.Value);
  }

  /**
   * Revert the image to original non-processed image
   * @author Lim, Lay Ngor
   */
  void revertButton_actionPerformed()
  {
    Assert.expect(_imageProcessingToolBar != null);
    _imageProcessingToolBar.revertButton_actionPerformed();              
  }
  
  /**
   * Reset the Post Processing Dialog status to un-check and revert the image to original non--processed image.
   * @author Lim, Lay Ngor
   */
  void resetButton_actionPerformed(ActionEvent e)
  {
    revertButton_actionPerformed();
    _postProcessingTableModel.setValueAt(false, 0, PostProcessingTableModel.ColumnType.PostProcess);
    _postProcessingTableModel.setValueAt(false, 1, PostProcessingTableModel.ColumnType.PostProcess);
    _postProcessingTableModel.setValueAt(false, 2, PostProcessingTableModel.ColumnType.PostProcess);    
  }
  
  /**
   *  @author Lim, Lay Ngor
   */
  void previewCheckBox_actionPerformed()
  {
    if(_previewCheckBox.isSelected() == true)
      postProcessingImplementation();     
    else
      revertButton_actionPerformed();
  }
}
