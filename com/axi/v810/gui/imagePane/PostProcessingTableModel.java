package com.axi.v810.gui.imagePane;

import javax.swing.*;
import javax.swing.table.*;

/**
 * Temporary not support pre-processing data update.
 * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
 */
public class PostProcessingTableModel extends AbstractTableModel
{
  private String[] columnNames = {"Enhancer Type",
                                                     "Parameter",
                                                      "Value",
                                                      "Method",
                                                      "Post-Process"
//                                                    , "Pre-Process"    
                                                    };
  
  public class ColumnType
  {
    public static final int EnhancerType = 0;
    public static final int Parameter = 1;
    public static final int Value = 2;
    public static final int Method = 3;
    public static final int PostProcess = 4;    
    public static final int PreProcess = 5;
  }

  public static String[] ResizeMethod = new String[]
  {"Linear", "Bi-Cubic"};
  
  public static String[] CLAHEMethod = new String[]
  {"Normal"};//"Shifted"
  
  public static String[] BackgroundFilterMethod = new String[]
  {"Box"};
   
  public static final JComboBox resizeMethodComboBox = new JComboBox(ResizeMethod);
  public static final JComboBox CLAHEMethodComboBox = new JComboBox(CLAHEMethod);
  public static final JComboBox backgroundFilterMethodComboBox = new JComboBox(BackgroundFilterMethod); 
  
  //Consider use String instead of Object to ease implementation in future
  public Object[][] DEFAULT_DATA =
  {
    {"Resize", "Scale", "2.0", "Linear", false, false},
    {"CLAHE", "Slope", "3", "Normal", false, false},
    {"Background Filter", "Iteration", "1", "Box", false, false},
  };
  
  private Object[][] FACTORY_DEFAULT_DATA =
  {
    {"Resize", "Scale", "2.0", "Linear", false, false},
    {"CLAHE", "Slope", "3", "Normal", false, false},
    {"Background Filter", "Iteration", "1", "Box", false, false},
  };
   
  Object[][] _data;
  
/**
 * @author Lim, Lay Ngor
 */
   public PostProcessingTableModel()
   {
     _data = DEFAULT_DATA; 
   }
   
/**
 * @author Lim, Lay Ngor
 */   
  public int getColumnCount()
  {
    return columnNames.length;
  }
  
/**
 * @author Lim, Lay Ngor
 */  
  public int getRowCount()
  {
    return _data.length;
  }

/**
 * @author Lim, Lay Ngor
 */  
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    return _data[rowIndex][columnIndex];
  }
  
/**
 * To return the boolean value instead of Object value for the column PostProcessing & PreProcessing.
 * @author Lim, Lay Ngor
 */  
  public boolean getValue(int rowIndex, int columnIndex)
  {
    boolean value = false;
    if(columnIndex == ColumnType.PostProcess || columnIndex == ColumnType.PreProcess)
    {
      value = (boolean)_data[rowIndex][columnIndex];
    }
    
    return value;
  }
  
/**
 * @author Lim, Lay Ngor
 */  
  public String getColumnName(int col)
  {
    return columnNames[col];
  }
  
/**
 * @author Lim, Lay Ngor
 */  
  public boolean isCellEditable(int row, int col)
  {
//    return ((col == ColumnType.Method || col == ColumnType.Value || col == ColumnType.PreProcess || col == ColumnType.PostProcess));
    return ((col == ColumnType.Method || col == ColumnType.Value || col == ColumnType.PostProcess));     
  }
  
/**
 * @author Lim, Lay Ngor
 */  
  public void setValueAt(Object value, int row, int col)
  {  
    //check for input limit.
    if(col == ColumnType.Value)
    {
      if(row == 0) //Resize Scale
      {
        float scale = Float.parseFloat((String)value);
        if(scale <= 0f || scale > 15.f)
        {
          JOptionPane.showMessageDialog(null, "Input value shall set between 0.001 to 15!");
          return;
        }
      }
      if(row == 1) //CLAHE Slope
      {
        float slope = Integer.parseInt((String) value);
        if (slope < 1 || slope > 50)
        {
          JOptionPane.showMessageDialog(null, "Input value shall set between 1 to 50!");
          return;
        }
      }
      if (row == 2) //Remove Artifact Iteration
      {
        float iteration = Integer.parseInt((String) value);
        if (iteration < 1 || iteration > 15)
        {
          JOptionPane.showMessageDialog(null, "Input value shall set between 1 to 15!");
          return;
        }
      }
    }
      
    _data[row][col] = value;
    this.fireTableCellUpdated(row, col);
  }
  
/**
 * @author Lim, Lay Ngor
 */  
  public void updateDataObject(Object[][] newDataObject)
  {
    _data = newDataObject;
    this.fireTableDataChanged();
  }
  
/**
 * @author Lim, Lay Ngor
 */  
  public Class getColumnClass(int column) 
  {
    return (getValueAt(0, column).getClass());
  }
  
/**
 * @author Lim, Lay Ngor
 */  
  public void resetDataObjectToDefaultValue()
  {
    for(int i=0; i<getRowCount(); ++i)
    {
      for(int j=0; j<getColumnCount(); ++j)
      {
        _data[i][j] = FACTORY_DEFAULT_DATA[i][j];
      }
    }
    
    this.fireTableDataChanged();
  }
  
/**
 * @author Lim, Lay Ngor
 */  
  public void resetDataColumnToDefaultValue(int column)
  {
    for (int i = 0; i < getRowCount(); ++i)
    {
       _data[i][column] = FACTORY_DEFAULT_DATA[i][column];
    }

    this.fireTableDataChanged();
  }
}
