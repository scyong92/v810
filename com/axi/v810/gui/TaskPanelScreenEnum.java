package com.axi.v810.gui;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * The base class for all gui screen (Task Panel) events
 * @author Andy Mechtenberg
 */
public class TaskPanelScreenEnum extends GuiEventEnum
{
  private static int _index = -1;

  // axi home screen
  public static final TaskPanelScreenEnum AXI_HOME = new TaskPanelScreenEnum(++_index);

  // test dev screens
  public static final TaskPanelScreenEnum NULL_SCREEN = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum PROJECT_HOME = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum PANEL_SETUP = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum MANUAL_ALIGNMENT = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum PSP = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum SCAN_PATH_REVIEW = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum VERIFY_CAD = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum ADJUST_CAD = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum MODIFY_SUBTYPES = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum INITIAL_TUNE = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum ALGORITHM_TUNER = new TaskPanelScreenEnum(++_index);

  // test exec screen
  public static final TaskPanelScreenEnum OPERATOR_PANEL = new TaskPanelScreenEnum(++_index);

  // config screens
  public static final TaskPanelScreenEnum CONFIG_SOFTWARE = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum CONFIG_PRODUCTION = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum CONFIG_RESULTS = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum CONFIG_CUSTOMER = new TaskPanelScreenEnum(++_index);

  // service screens
  public static final TaskPanelScreenEnum SERVICE_HOME = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum SERVICE_CDA = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum SERVICE_SSC = new TaskPanelScreenEnum(++_index);
  public static final TaskPanelScreenEnum SERVICE_XST = new TaskPanelScreenEnum(++_index);

  // virtual live screens
  public static final TaskPanelScreenEnum VIRTUAL_LIVE = new TaskPanelScreenEnum(++_index);

  private static Map<TaskPanelScreenEnum, String> _screenEnumToScreenNameMap = new HashMap<TaskPanelScreenEnum, String>();

  static
  {
    init();
  }

  /**
   * @author George A. David
   */
  private static void init()
  {
    initScreenNames();
  }

  /**
   * @author George A. David
   */
  private static void initScreenNames()
  {
    // AXI Home UI
    String envTitle = StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY") + " - ";
    _screenEnumToScreenNameMap.put(AXI_HOME, envTitle + StringLocalizer.keyToString("HP_HOME_PANEL_TITLE_KEY"));

    // Test Dev UI
    envTitle = StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY") + " - ";
    _screenEnumToScreenNameMap.put(PROJECT_HOME, envTitle + StringLocalizer.keyToString("MM_GUI_TDT_TITLE_NULL_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(PROJECT_HOME, envTitle + StringLocalizer.keyToString("MM_GUI_TDT_TITLE_HOME_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(PANEL_SETUP, envTitle + StringLocalizer.keyToString("MM_GUI_TDT_TITLE_PANEL_SETUP_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(MANUAL_ALIGNMENT, envTitle + StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(PSP, envTitle + StringLocalizer.keyToString("MM_GUI_TDT_TITLE_PSP_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(SCAN_PATH_REVIEW, envTitle + StringLocalizer.keyToString("MM_GUI_TDT_TITLE_SCAN_PATH_REVIEW_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(VERIFY_CAD, envTitle + StringLocalizer.keyToString("MM_GUI_TDT_TITLE_VERIFY_CAD_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(ADJUST_CAD, envTitle + StringLocalizer.keyToString("MM_GUI_TDT_TITLE_EDIT_CAD_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(MODIFY_SUBTYPES, envTitle + StringLocalizer.keyToString("MM_GUI_TDT_TITLE_GROUPINGS_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(INITIAL_TUNE, envTitle + StringLocalizer.keyToString("MM_GUI_TDT_TITLE_INITIAL_TUNE_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(ALGORITHM_TUNER, envTitle + StringLocalizer.keyToString("MM_GUI_TDT_TITLE_TUNER_KEY") + " - " + Version.getVersion());

    // Test Execution UI
    envTitle = StringLocalizer.keyToString("TEGUI_ENV_NAME_KEY") + " - ";
    _screenEnumToScreenNameMap.put(OPERATOR_PANEL, envTitle + StringLocalizer.keyToString("TEGUI_OPERATOR_PANEL_TITLE_KEY"));

    // System Configuration UI
    envTitle = StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY") + " - ";
    _screenEnumToScreenNameMap.put(CONFIG_SOFTWARE, envTitle + StringLocalizer.keyToString("CFGUI_SOFTWARE_PANEL_TITLE_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(CONFIG_PRODUCTION, envTitle + StringLocalizer.keyToString("CFGUI_PRODUCTION_PANEL_TITLE_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(CONFIG_RESULTS, envTitle + StringLocalizer.keyToString("CFGUI_RESULTS_PANEL_TITLE_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(CONFIG_CUSTOMER, envTitle + StringLocalizer.keyToString("CFGUI_CUSTOMER_PANEL_TITLE_KEY") + " - " + Version.getVersion());

    // Service UI
    envTitle = StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY") + " - ";
    _screenEnumToScreenNameMap.put(SERVICE_HOME, envTitle + StringLocalizer.keyToString("CDGUI_HOME_PANEL_TITLE_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(SERVICE_CDA, envTitle + StringLocalizer.keyToString("CDGUI_CDA_PANEL_TITLE_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(SERVICE_SSC, envTitle + StringLocalizer.keyToString("CDGUI_SSC_PANEL_TITLE_KEY") + " - " + Version.getVersion());
    _screenEnumToScreenNameMap.put(SERVICE_XST, envTitle + StringLocalizer.keyToString("CDGUI_XST_PANEL_TITLE_KEY") + " - " + Version.getVersion());

    // Virtual Live UI
    _screenEnumToScreenNameMap.put(VIRTUAL_LIVE, "VirtualLive Environment - " + Version.getVersion());
  }

  /**
   * @param id the unique id for this enum
   * @author George A. David
   */
  private TaskPanelScreenEnum(int id)
  {
    super(id);
  }

  /**
   * get the name of this screen
   * @author George A. David
   */
  public String getName()
  {
    Assert.expect(_screenEnumToScreenNameMap.containsKey(this));
    String name = (String)_screenEnumToScreenNameMap.get(this);

    Assert.expect(name != null);
    return name;
  }

}
