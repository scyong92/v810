package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.mainMenu.*;
import static com.axi.v810.gui.mainMenu.MainMenuGui.finishLicenseMonitor;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * A dialog to display the alignment failure and provide access to the images
 *
 * @author Andy Mechtenberg
 */
public class SaveProjectAsDialog extends EscapeDialog
{

  private String _initialProjectName;
  private JTextField _inputTextField;
  private JCheckBox _saveImageSetsCheckBox;

  private String _newProjectName = null;
  private boolean _shouldCopyImageSets = true;
  private String _selectedVersion = java.lang.String.valueOf(Version.getVersionNumber());
  private SystemTypeEnum _selectedSystemTypeEnum = null;
  private String _selectedSystemLowMagForS2EX = null;
  private JComboBox _versionCombobox;
  private JComboBox _systemTypeCombobox;
  private JComboBox _systemLowMagComboBox;
  
  private final String _M23 = "M23";
  private final String _M19 = "M19";

  /**
   * @author Andy Mechtenberg
   */
  public SaveProjectAsDialog(JFrame parent, String title, String initialProjectName)
  {
    super(parent, title, true);
    Assert.expect(parent != null);
    Assert.expect(title != null);
    Assert.expect(initialProjectName != null);

    _initialProjectName = initialProjectName;

    jbInit();
    pack();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    setResizable(false);  // do this so no frame icon is displayed
    JButton okButton = new JButton(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    JButton cancelButton = new JButton(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    okButton.setMargin(new Insets(2, 8, 2, 8));
    cancelButton.setMargin(new Insets(2, 8, 2, 8));
    okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed();
      }
    });
    cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });

    JPanel mainPanel = new JPanel(new BorderLayout(0, 5));
    JPanel OptionPanel = new JPanel(new BorderLayout());
    JPanel versionPanel = new JPanel(new GridLayout(1, 2, 10, 0));
    JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    JPanel innerButtonPanel = new JPanel(new GridLayout(1, 2, 10, 0));
    innerButtonPanel.add(okButton);
    innerButtonPanel.add(cancelButton);
    buttonPanel.add(innerButtonPanel);

    Icon infoIcon = UIManager.getIcon("OptionPane.questionIcon");
    JLabel iconLabel = new JLabel(infoIcon);
    JLabel versionLabel = new JLabel(StringLocalizer.keyToString("MM_GUI_SAVE_AS_VERSION_KEY"));
    JPanel centerPanel = new JPanel(new BorderLayout(0, 5));
    centerPanel.add(new JLabel(StringLocalizer.keyToString("GUI_SAVE_AS_PROMPT_KEY")), BorderLayout.NORTH);
    _inputTextField = new JTextField(_initialProjectName, 23);
    _inputTextField.selectAll();
    centerPanel.add(_inputTextField, BorderLayout.CENTER);
    _saveImageSetsCheckBox = new JCheckBox(StringLocalizer.keyToString("MM_GUI_COPY_IMAGE_SETS_KEY"));
    _saveImageSetsCheckBox.setSelected(true);
    _versionCombobox = new JComboBox();
    for (String softwareRevision : SoftwareRevisionEnum.getFullStringList())
    {
      _versionCombobox.addItem(softwareRevision);
    }
    versionPanel.add(versionLabel);
    versionPanel.add(_versionCombobox);
    
    // only add save as other system type combo box when license is olp
    try
    {
      if (LicenseManager.isOfflineProgramming())
      {
       // JLabel warningLabel = new JLabel("Only available for S2EX System Type");
        JPanel systemPanel = new JPanel(new BorderLayout());
        JPanel systemTypePanel = new JPanel(new GridLayout(1, 2, 10, 0));
        JLabel systemTypeLabel = new JLabel(StringLocalizer.keyToString("MM_GUI_SAVE_AS_SYSTEMTYPE_KEY"));
        JPanel systemMagmPanel = new JPanel (new GridLayout(1, 2, 10, 0));
        final JLabel systemMagLabel= new JLabel(StringLocalizer.keyToString("MMGUI_SAVE_AS_LOW_MAG_KEY"));
        _systemTypeCombobox = new JComboBox();
        _systemLowMagComboBox = new JComboBox();
        _systemLowMagComboBox.addItem(_M19);
        _systemLowMagComboBox.addItem(_M23);
               
        systemMagmPanel.add(systemMagLabel);
        systemMagmPanel.add(_systemLowMagComboBox);
        
        java.util.List<String> systemTypeList = null;
        try
        {
          systemTypeList =  LicenseManager.getInstance().getSupportedSystemType();
        }
        catch (final BusinessException e)
        {
          finishLicenseMonitor();
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                      e.getMessage(),
                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                      true);
              MainMenuGui.getInstance().exitV810(false);
            }
          });
        }

        for (String systemType : systemTypeList)
        {
          _systemTypeCombobox.addItem(systemType);
        }
        
        systemTypePanel.add(systemTypeLabel);
        systemTypePanel.add(_systemTypeCombobox);
        
        if (XrayTester.getSystemType().equals(SystemTypeEnum.THROUGHPUT))
        {
          _systemTypeCombobox.setSelectedItem(SystemTypeEnum.STANDARD.getName().toUpperCase());
        }
        else
        {
          _systemTypeCombobox.setSelectedItem(XrayTester.getSystemType().getName());
        }
        
        if (XrayTester.getSystemType().equals(SystemTypeEnum.S2EX))
        {
          _systemLowMagComboBox.setSelectedItem(Config.getSystemCurrentLowMagnification());
        }
        else
        {
          systemMagLabel.setEnabled(false);
          _systemLowMagComboBox.setEnabled(false);
        }
       // systemPanel.add(warningLabel);
        systemPanel.add(systemTypePanel,BorderLayout.NORTH);
        if (systemTypeList.contains(SystemTypeEnum.S2EX.getName()))
        {
          systemPanel.add(systemMagmPanel,BorderLayout.CENTER);
        }
        
        
        // disable copyimage selection if current project is S2EX and low mag is M23.
        // This is because when we save as other system type , system will load the new system type setting in software.
        // Only S2EX support M23, so the image will have wrong view.
//        if (XrayTester.getSystemType().equals(SystemTypeEnum.S2EX) && Config.getSystemCurrentLowMagnification().equals(_M23))
//        {
//          _saveImageSetsCheckBox.setEnabled(false);
//        }
        
        // disable copyimage selection if current project is S2EX and low mag is M23.
        // This is because when we save as other system type , system will load the new system type setting in software.
        // XXL support long panel which is other system type dont support. So the image is become uncompatibiity.
//        if (XrayTester.getSystemType().equals(SystemTypeEnum.XXL) && Project.getCurrentlyLoadedProject().getPanel().getPanelSettings().isLongPanel())
//        {
//          _saveImageSetsCheckBox.setEnabled(false);
//        }
        
        _systemTypeCombobox.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            String systemTypeName = (String)_systemTypeCombobox.getSelectedItem();
            SystemTypeEnum systemType = SystemTypeEnum.getSystemType(systemTypeName);
            if (XrayTester.getSystemType().equals(SystemTypeEnum.S2EX))
            {
              if (systemType.equals(SystemTypeEnum.S2EX))
              {
                systemMagLabel.setEnabled(true);
                _systemLowMagComboBox.setEnabled(true);
                if (Config.getSystemCurrentLowMagnification().equals(_systemLowMagComboBox.getSelectedItem()))
                {
                  _saveImageSetsCheckBox.setEnabled(true);
                }
                else
                {
                  _saveImageSetsCheckBox.setEnabled(false);
                }
              }
              else
              {
                if (Config.getSystemCurrentLowMagnification().equals(_M23))
                {
                  _saveImageSetsCheckBox.setEnabled(false);
                }
                else
                {
                  _saveImageSetsCheckBox.setEnabled(true);
                }
                systemMagLabel.setEnabled(false);
                _systemLowMagComboBox.setEnabled(false);
                _systemLowMagComboBox.setSelectedItem(Config.getSystemCurrentLowMagnification());
              }
            }
            else if (XrayTester.getSystemType().equals(SystemTypeEnum.XXL) && Project.getCurrentlyLoadedProject().getPanel().getPanelSettings().isLongPanel())
            {
              if (systemType.equals(SystemTypeEnum.XXL))
              {
                _saveImageSetsCheckBox.setEnabled(true);
              }
              else
              {
                _saveImageSetsCheckBox.setEnabled(false);
              }
            }
            
            else
            {
              if (systemType.equals(SystemTypeEnum.XXL))
              {
                systemMagLabel.setEnabled(false);
                _systemLowMagComboBox.setEnabled(false);
                if (Project.getCurrentlyLoadedProject().getPanel().getPanelSettings().isLongPanel())
                {
                  _saveImageSetsCheckBox.setEnabled(false);
                }
                else
                {
                  _saveImageSetsCheckBox.setEnabled(true);
                }
              }
              else if (systemType.equals(SystemTypeEnum.S2EX))
              {
                systemMagLabel.setEnabled(true);
                _systemLowMagComboBox.setEnabled(true);
                if (_systemLowMagComboBox.getSelectedItem().equals(_M23))
                {
                  _saveImageSetsCheckBox.setEnabled(false);
                }
                else
                {
                  _saveImageSetsCheckBox.setEnabled(true);
                }
              }
              else
              {
                systemMagLabel.setEnabled(false);
                _systemLowMagComboBox.setEnabled(false);
                if (Config.getSystemCurrentLowMagnification().equals(_M19))
                {
                  _saveImageSetsCheckBox.setEnabled(true);
                }
              }
            }
          }
        });
        
        _systemLowMagComboBox.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            if (_systemLowMagComboBox.isEnabled())
            {
              if (Config.getSystemCurrentLowMagnification().equals(_systemLowMagComboBox.getSelectedItem()))
              {
                _saveImageSetsCheckBox.setEnabled(true);
              }
              else
              {
                _saveImageSetsCheckBox.setEnabled(false);
              }
            }
          }
        });
        OptionPanel.add(systemPanel,BorderLayout.SOUTH);
      }
    }
    catch (final BusinessException e)
    {
      finishLicenseMonitor();
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  e.getMessage(),
                  StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                  true);
          MainMenuGui.getInstance().exitV810(false);
        }
      });
    }
    OptionPanel.add(versionPanel, BorderLayout.CENTER);
    OptionPanel.add(_saveImageSetsCheckBox, BorderLayout.NORTH);
    centerPanel.add(OptionPanel, BorderLayout.SOUTH);

    mainPanel.add(buttonPanel, BorderLayout.SOUTH);
    mainPanel.add(iconLabel, BorderLayout.WEST);
    mainPanel.add(centerPanel, BorderLayout.CENTER);
    mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 5, 10));
    iconLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
    this.getContentPane().add(mainPanel, BorderLayout.CENTER);
    this.getRootPane().setDefaultButton(okButton);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void okButton_actionPerformed()
  {
    if (_saveImageSetsCheckBox.isEnabled())
    {
      _shouldCopyImageSets = _saveImageSetsCheckBox.isSelected();
    }
    else
    {
      _shouldCopyImageSets = false;
    }
    _newProjectName = _inputTextField.getText();
    _selectedVersion = (String) _versionCombobox.getSelectedItem();
    if (_systemTypeCombobox != null)
    {
      String systemTypeName = (String) _systemTypeCombobox.getSelectedItem();
      if (systemTypeName.equalsIgnoreCase(SystemTypeEnum.STANDARD.getName().toUpperCase()))
      {
        systemTypeName = SystemTypeEnum.THROUGHPUT.getName();
      }
      _selectedSystemTypeEnum = SystemTypeEnum.getSystemType(systemTypeName);
    }
    if (_systemLowMagComboBox != null)
    {
      _selectedSystemLowMagForS2EX = (String) _systemLowMagComboBox.getSelectedItem();
    }
        
    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean getShouldCopyImageSets()
  {
    return _shouldCopyImageSets;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getNewProjectName()
  {
    return _newProjectName;
  }

  /**
   * @author Eoh Weng Jian
   */
  public String getSelectedVersion()
  {
    return _selectedVersion;
  }

  /**
   * @author Phang Siew Yeng
   */
  public void setEnabledSaveImageSetsCheckBox(boolean enable)
  {
    _saveImageSetsCheckBox.setEnabled(enable);
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   *
   * @author weng-jian.eoh
   *
   * this function should only called when license is OLP
   */
  public SystemTypeEnum getSelectedSystemType()
  {
    if (_systemTypeCombobox != null)
    {
      return _selectedSystemTypeEnum;
    }
    
    return null;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public String geSelectedSystemMagnification()
  {
    if (_systemLowMagComboBox != null && _systemLowMagComboBox.isEnabled())
    {
      return _selectedSystemLowMagForS2EX;
    }

    return null;
  }
}