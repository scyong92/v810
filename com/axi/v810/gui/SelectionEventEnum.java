package com.axi.v810.gui;

/**
 * This class enumerates all events resulting
 * from a selction. For example selcting a package or
 * a land pattern from a table or combo box.
 *
 * @author George A. David
 */
public class SelectionEventEnum extends GuiEventEnum
{
  private static int _index = -1;

  public static SelectionEventEnum BOARD_INSTANCE = new SelectionEventEnum(++_index);
  public static SelectionEventEnum BOARD_TYPE = new SelectionEventEnum(++_index);
  public static SelectionEventEnum PAD_TYPE = new SelectionEventEnum(++_index);
  public static SelectionEventEnum PACKAGE = new SelectionEventEnum(++_index);
  public static SelectionEventEnum COMPONENT_TYPE = new SelectionEventEnum(++_index);
  public static SelectionEventEnum FIDUCIAL_TYPE = new SelectionEventEnum(++_index);
  public static SelectionEventEnum DISPLAY_UNITS = new SelectionEventEnum(++_index);
  public static SelectionEventEnum FIND_COMPONENT = new SelectionEventEnum(++_index);
  public static SelectionEventEnum ALIGN_POINTS = new SelectionEventEnum(++_index);
  public static SelectionEventEnum LANDPATTERN = new SelectionEventEnum(++_index);
  public static SelectionEventEnum PACKAGE_PIN = new SelectionEventEnum(++_index);
  public static SelectionEventEnum LANDPATTERN_PAD = new SelectionEventEnum(++_index);
  public static SelectionEventEnum PAD_OR_FIDUCIAL = new SelectionEventEnum(++_index);
  public static SelectionEventEnum CLEAR_SELECTIONS = new SelectionEventEnum(++_index);
  public static SelectionEventEnum TUNER_TEST_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum TUNER_BOARD_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum MEASUREMENT_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum SUBTYPE_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum SLICE_MEASUREMENT_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum INSPECTION_IMAGE_CLEAR_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum INSPECTION_IMAGE_PAD_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum INSPECTION_IMAGE_COMPONENT_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum INSPECTION_IMAGE_NO_IMAGE_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum INSPECTION_DIAGNOSTICS_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum INSPECTION_RUN_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum INSPECTION_RUN_SELECTION_PROCESSING_COMPLETE = new SelectionEventEnum(++_index);
  public static SelectionEventEnum EXPORT_LIBRARY_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum EXPORT_SUBTYPE_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum IMPORT_PACKAGES_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum IMPORT_REFINE_PACKAGES_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum IMPORT_REFINE_PACKAGES_SELECTION_CHANGE = new SelectionEventEnum(++_index);
  public static SelectionEventEnum DEFECT_DETAILS_SELECTION = new SelectionEventEnum(++_index); //Ngie Xing, April 2014
  public static SelectionEventEnum SORT_COMPONENT = new SelectionEventEnum(++_index); //Sheng Chuan
  public static SelectionEventEnum IMPORT_RECIPES_SELECTION = new SelectionEventEnum(++_index);
  public static SelectionEventEnum PSH_COMPONENT_SELECTION = new SelectionEventEnum(++ _index); //Siew Yeng - XCR-3781
  public static SelectionEventEnum PSH_NEIGHBOR_COMPONENT_SELECTION = new SelectionEventEnum(++ _index); //Siew Yeng - XCR-3781

  /**
   * @author George A. David
   */
  private SelectionEventEnum(int id)
  {
    super(id);
  }
}
