package com.axi.v810.gui.packageLibrary;

import java.util.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.home.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: ImportWizardRecipeSelectionPanel</p>
 *
 * <p>Description: This panel is to display recipes available in the system and allow users to choose which to import.</p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: Vitrox Technogies</p>
 *
 * @author Cheah Lee Herng
 * @version 1.0
 */
public class ImportWizardRecipeSelectionPanel extends WizardPanel implements Observer
{
  // this is not a user visible string so therefore it does not have to be localized.
  protected String _name = null;
  
  private JButton _selectAllButton;
  private JButton _unselectAllButton;
  private JPanel _recipeSelectionDescPanel;
  private JLabel _recipeSelectionDescLabel;
  private JScrollPane _scrollPane;
  private JPanel _buttonPanel;
  private JPanel _mainPanel;
  
  private List<AvailableProjectData> _currentlySelectedProjectData;
  private ImportRecipeSelectionLayoutTable _recipeSelectionTable;
  private ImportRecipeSelectionLayoutTableModel _recipeSelectionTableModel;
  private GuiObservable _guiObservable;
  
  /**
   * @author Cheah Lee Herng
   */
  public ImportWizardRecipeSelectionPanel(WizardDialog dialog, List<AvailableProjectData> currentlySelectedProjectData)
  {
    super(dialog);
    
    _currentlySelectedProjectData = currentlySelectedProjectData;
    
    _guiObservable = GuiObservable.getInstance();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void init()
  {
    _guiObservable.addObserver(this);
    
    setLayout(new java.awt.BorderLayout(5, 5));

    _mainPanel = new JPanel(new java.awt.BorderLayout(5, 5));
    _mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
    
    _buttonPanel = new JPanel(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));
    
    _recipeSelectionTableModel = new ImportRecipeSelectionLayoutTableModel(_currentlySelectedProjectData);
    _recipeSelectionTable = new ImportRecipeSelectionLayoutTable();
    
    _selectAllButton = new JButton(StringLocalizer.keyToString("GUI_SELECT_SELECT_ALL_KEY"));
    _unselectAllButton = new JButton(StringLocalizer.keyToString("PLWK_UNSELECT_ALL_KEY"));
    
    _recipeSelectionDescPanel = new JPanel(new java.awt.BorderLayout(5, 5));
    _recipeSelectionDescPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    _recipeSelectionDescLabel = new JLabel(StringLocalizer.keyToString("PLWK_IMPORT_SELECT_RECIPES_DESC_KEY"));
    
    _scrollPane = new JScrollPane();    
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void createPanel()
  {
    removeAll();
    init();
    
    _scrollPane.getViewport().add(_recipeSelectionTable, null);
    
    _recipeSelectionDescPanel.setBackground(java.awt.Color.WHITE);
    _recipeSelectionDescPanel.add(_recipeSelectionDescLabel, java.awt.BorderLayout.CENTER);   
    
    _recipeSelectionTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    
    _buttonPanel.add(_selectAllButton);
    _buttonPanel.add(_unselectAllButton);
    
    _mainPanel.add(_recipeSelectionDescPanel, java.awt.BorderLayout.NORTH);
    _mainPanel.add(_scrollPane, java.awt.BorderLayout.CENTER);
    _mainPanel.add(_buttonPanel, java.awt.BorderLayout.SOUTH);
    
    add(_mainPanel, java.awt.BorderLayout.CENTER);
    
    _selectAllButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent ae)
      {
        _recipeSelectionTableModel.selectAllProjectData();
        validatePanel();
      }
    });

    _unselectAllButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent ae)
      {
        _recipeSelectionTableModel.unselectAllProjectData();
        validatePanel();
      }
    });
    
    SwingUtils.setTableAndScrollPanelToPreferredSize(_scrollPane, _recipeSelectionTable);
  }
        
  /**
   * @author Cheah Lee Herng
   */
  public void validatePanel()
  {
    WizardButtonState buttonState;

    if (_recipeSelectionTableModel.getSelectedProjectData().isEmpty() == false)
    {
      buttonState = new WizardButtonState(false, true, true, true);
    }
    else
    {
      buttonState = new WizardButtonState(false, true, false, true);
    }

    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void populatePanelWithData()
  {
    createPanel();   

    _dialog.setTitle(StringLocalizer.keyToString("PLWK_IMPORT_SELECT_RECIPE_KEY"));
    
    populatePanelWithFilteringData();
    
    validatePanel();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void populatePanelWithFilteringData()
  {
    _recipeSelectionTableModel.populateWithPanelData();
    
    _recipeSelectionTable.setModel(_recipeSelectionTableModel);
    _recipeSelectionTable.setEditorsAndRenderers();
    _recipeSelectionTable.setPreferredColumnWidths();
    validate();
    repaint();
  }

  /**
   * @author Cheah Lee Herng
   */
  public void unpopulate()
  {
    _recipeSelectionTableModel = null;
    _recipeSelectionTable = null;
    
    removeAll();
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Cheah Lee Herng
   */
  public String getWizardName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void removeObservers()
  {
    _guiObservable.deleteObserver(this);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof GuiObservable)
        {
          GuiEvent guiEvent = (GuiEvent)object;
          GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
          if (guiEventEnum instanceof SelectionEventEnum)
          {
            SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
            if (selectionEventEnum.equals(SelectionEventEnum.IMPORT_RECIPES_SELECTION))
            {
              validatePanel();
            }
          }
        }
      }
    });
  }
}
