package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.io.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class CompareThresholdTableCellRenderer extends DefaultTableCellRenderer
{
  private Algorithm _algorithm;
  private CompareThresholdTable _compareThresholdTable;
  private CompareThresholdTableModel _compareThresholdTableModel;
  private Font _regularFont;
  private Font _boldFont;
  private Subtype _subtype;

  private final Color _VALUE_IS_DEFAULT_BACKGROUND_COLOR = Color.white;
  private final Color _VALUE_IS_MODIFIED_BACKGROUND_COLOR = new Color(255, 255, 153); // light yellow
  private final Color _VALUE_IS_IMPORTED_BACKGROUND_COLOR = new Color(255, 153, 0); // light orange
  private final Color _VALUE_IS_EMPTY_BACKGROUND_COLOR = new Color(200, 200, 200); // light GRAY

  /**
   * Creates a Custom JLabel Cell Renderer
   *
   * @param compareThresholdTable CompareThresholdTable
   * @param compareThresholdTableModel CompareThresholdTableModel
   * @param algorithm Algorithm
   * @param subtype Subtype
   *
   * @author Wei Chin, Chong
   */
  public CompareThresholdTableCellRenderer(CompareThresholdTable compareThresholdTable,
                                           CompareThresholdTableModel compareThresholdTableModel,
                                           Algorithm algorithm,
                                           Subtype subtype)
  {
    Assert.expect(compareThresholdTable != null);
    Assert.expect(compareThresholdTableModel != null);
    Assert.expect(algorithm != null);
    Assert.expect(subtype != null);

    _algorithm = algorithm;

    _subtype = subtype;
    _compareThresholdTable = compareThresholdTable;
    _compareThresholdTableModel = compareThresholdTableModel;
    _regularFont = this.getFont();
    _boldFont = FontUtil.getBoldFont(this.getFont(), Localization.getLocale());
  }

  /**
   * Returns the component used for drawing the cell.  This method is
   * used to configure the renderer appropriately before drawing.
   * see TableCellRenderer.getTableCellRendererComponent(...); for more comments on the method
   *
   * @param table JTable
   * @param value Object
   * @param isSelected boolean
   * @param hasFocus boolean
   * @param row int
   * @param column int
   * @return Component
   *
   * @author Wei Chin, Chong
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    label.setOpaque(true);
    label.setFont(_regularFont);
    if (row == _compareThresholdTableModel.getStandardRow() ||
        row == _compareThresholdTableModel.getAdditionalRow())
    {

      label.setFont(_boldFont);
      label.setBackground(Color.GRAY);
      label.setForeground(Color.WHITE);
    }
    else if (row == _compareThresholdTableModel.getAlgorithmEnabledRow())
    {
      label.setFont(_boldFont);
      label.setBackground(Color.WHITE);
      label.setForeground(Color.BLACK);
    }
    else
    {
      if(isSelected)
      {
        label.setBackground(_compareThresholdTable.getSelectionBackground());
        label.setForeground(_compareThresholdTable.getSelectionForeground());
      }
      else
      {
        label.setBackground(_compareThresholdTable.getBackground());
        label.setForeground(_compareThresholdTable.getForeground());
      }

      if (value != null)
    {
      Object cellObject = table.getModel().getValueAt(row, CompareThresholdTableModel.NAME_COLUMN);
      MeasurementUnitsEnum unitsEnum = null;
      AlgorithmSetting algorithmSetting = null;
      if (cellObject instanceof AlgorithmSetting)
      {
        algorithmSetting = (AlgorithmSetting)cellObject;
        unitsEnum = algorithmSetting.getUnits(_algorithm.getVersion());
        MathUtilEnum displayUnits;

        displayUnits = Project.getCurrentlyLoadedProject().getDisplayUnits();

        String valueText = MeasurementUnitsEnum.formatNumberIfNecessary(unitsEnum, value);
        String valueWithUnitsLabelText = valueText;

        if (column > CompareThresholdTableModel.NAME_COLUMN)  //don't put the units on the name column
          valueWithUnitsLabelText = AlgorithmSettingUtil.getAlgorithmSettingDisplayText(value, unitsEnum, displayUnits);

        setText(valueWithUnitsLabelText);

        String unitsName = unitsEnum.getName();
        if (unitsName.length() > 0)
        {
          setToolTipText(valueText + " [" + unitsName + "]");
        }
        else
        {
          setToolTipText(valueText);
        }

        if (column == CompareThresholdTableModel.PROJECT_VALUE_COLUMN)
        {
          setForeground(Color.black);
          Object defaultValue = table.getValueAt(row, CompareThresholdTableModel.DEFAULT_COLUMN);
          Object setValue = table.getValueAt(row, CompareThresholdTableModel.PROJECT_VALUE_COLUMN);

          if (setValue.toString().length() <= 0)
          {
            setBackground(_VALUE_IS_EMPTY_BACKGROUND_COLOR);
          }
          else if (defaultValue.toString().equals(setValue.toString()))
          {
            setBackground(_VALUE_IS_DEFAULT_BACKGROUND_COLOR);
//            if (_subtype != null)
//            {
//              if (_subtype.isImportedPackageLibrary())
//              {
//                if (_subtype.isImportedValue(algorithmSetting.getAlgorithmSettingEnum(), getSerializedValue( setValue , algorithmSetting)))
//                  setBackground(_VALUE_IS_IMPORTED_BACKGROUND_COLOR);
//              }
//            }
          }
          else
          {
            setBackground(_VALUE_IS_MODIFIED_BACKGROUND_COLOR);

            if (_subtype != null)
            {
              if (_subtype.isImportedPackageLibrary())
              {
                if (_subtype.isImportedValue(algorithmSetting.getAlgorithmSettingEnum(), getSerializedValue( setValue , algorithmSetting)))
                  setBackground(_VALUE_IS_IMPORTED_BACKGROUND_COLOR);
              }
            }
          }
        }
        else if(column == CompareThresholdTableModel.LIBRARY_VALUE_COLUMN)
        {
          setForeground(Color.black);
          Object setValue = table.getValueAt(row, CompareThresholdTableModel.LIBRARY_VALUE_COLUMN);
          Object defaultValue = table.getValueAt(row, CompareThresholdTableModel.DEFAULT_COLUMN);
          if (setValue.toString().equals(defaultValue.toString()))
            setBackground(_VALUE_IS_DEFAULT_BACKGROUND_COLOR);
          else
            setBackground(_VALUE_IS_IMPORTED_BACKGROUND_COLOR);
        }
        else if(column == CompareThresholdTableModel.DEFAULT_COLUMN)
        {
          setForeground(Color.black);
          setBackground(_VALUE_IS_DEFAULT_BACKGROUND_COLOR);
        }
      }
      else
      {
        setText((String)value);
        this.setFont(_boldFont);
        setBackground(Color.gray);
      }
    }

    }
    // Since the renderer is a component, return itself
    return label;
  }

  /**
   * @param tunedValue Object
   * @param algorithmSetting AlgorithmSetting
   * @return Serializable
   *
   * @author Wei Chin, Chong
   */
  private Serializable getSerializedValue(Object tunedValue, AlgorithmSetting algorithmSetting)
  {
    Assert.expect(tunedValue != null);
    Serializable value = null;

    if (tunedValue instanceof Integer)
      value = (Integer)tunedValue;

    else if (tunedValue instanceof String)
      value = (String)tunedValue;

    else if (tunedValue instanceof Float)
    {
      Float tunedValueFloat = (Float)tunedValue;

      MeasurementUnitsEnum measurementUnitsEnum = _subtype.getAlgorithmSettingUnitsEnum(algorithmSetting.getAlgorithmSettingEnum());
      if (measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS) &&
          Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
      {
        // convert from mils (mils is what the user has input)
        tunedValueFloat = MathUtil.convertMilsToMillimeters(tunedValueFloat.floatValue());
      }
      else if (measurementUnitsEnum.equals(MeasurementUnitsEnum.SQUARE_MILLIMETERS) &&
               Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
      {
        // convert from sq mils (sq mils is what the user has input)
        tunedValueFloat = MathUtil.convertSquareMilsToSquareMillimeters(tunedValueFloat.floatValue());
      }
      else if (measurementUnitsEnum.equals(MeasurementUnitsEnum.SQUARE_MILLIMETERS) &&
               Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
      {
        // convert from cu mils (cu mils is what the user has input)
        tunedValueFloat = MathUtil.convertCubicMilsToCubicMillimeters(tunedValueFloat.floatValue());
      }
      tunedValueFloat = MathUtil.roundToPlaces(tunedValueFloat, 4);
      value = tunedValueFloat;
    }
    else
      Assert.expect(false, "unexpect serialize object : " + tunedValue);

    return value;
  }

}
