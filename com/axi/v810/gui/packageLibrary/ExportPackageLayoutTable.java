package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.util.*;

/**
 *
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class ExportPackageLayoutTable extends JSortTable
{
  public static final int _SELECT_INDEX = 0;
  public static final int _PACKAGE_INDEX = 1;
  public static final int _JOINT_TYPE_INDEX = 2;
  public static final int _NEW_MODIFIED_IMPORTED_INDEX = 3;
  public static final int _PACKAGE_HAS_MODIFIED_INDEX = 4;
  public static final int _SUBTYPE_HAS_MODIFIED_INDEX = 5;
  public static final int _OVERWRITE_INDEX = 6;
  public static final int _SOURCE_LIBRARY_INDEX = 7;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _SELECT_COLUMN_WIDTH_PERCENTAGE = .06;
  private static final double _PACKAGE_COLUMN_WIDTH_PERCENTAGE = .14;
  private static final double _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _NEW_MODIFIED_IMPORTED_COLUMN_WIDTH_PERCENTAGE = .09;
  private static final double _PACKAGE_HAS_MODIFIED_COLUMN_WIDTH_PERCENTAGE = .16;
  private static final double _SUBTYPE_HAS_MODIFIED_COLUMN_WIDTH_PERCENTAGE = .16;
  private static final double _OVERWRITE_COLUMN_WIDTH_PERCENTAGE = .09;
  private static final double _SOURCE_LIBRARY_COLUMN_WIDTH_PERCENTAGE = .2;

  private CheckCellEditor _checkEditor = new CheckCellEditor(getBackground(), getForeground());;
  private LibraryCellEditor _libraryCellEditor = null;
  private ExportPackageLayoutTableModel _exportPackageLayoutTableModel = null;
  private LibraryManager _libraryManager = null;

  /**
   * @param exportPackageLayoutTableModel ExportPackageLayoutTableModel
   * @param libraryManager LibraryManager
   *
   * @author Wei Chin, Chong
   */
  public ExportPackageLayoutTable(ExportPackageLayoutTableModel exportPackageLayoutTableModel,
                                  LibraryManager libraryManager)
  {
    Assert.expect(exportPackageLayoutTableModel != null);
    Assert.expect(libraryManager != null);

    _exportPackageLayoutTableModel = exportPackageLayoutTableModel;
    _libraryManager = libraryManager;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_SELECT_INDEX).setPreferredWidth((int)(totalColumnWidth * _SELECT_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PACKAGE_INDEX).setPreferredWidth((int)(totalColumnWidth * _PACKAGE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_JOINT_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_NEW_MODIFIED_IMPORTED_INDEX).setPreferredWidth((int)(totalColumnWidth * _NEW_MODIFIED_IMPORTED_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_SUBTYPE_HAS_MODIFIED_INDEX).setPreferredWidth((int)(totalColumnWidth * _SUBTYPE_HAS_MODIFIED_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_OVERWRITE_INDEX).setPreferredWidth((int)(totalColumnWidth * _OVERWRITE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_SOURCE_LIBRARY_INDEX).setPreferredWidth((int)(totalColumnWidth * _SOURCE_LIBRARY_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PACKAGE_HAS_MODIFIED_INDEX).setPreferredWidth((int)(totalColumnWidth * _PACKAGE_HAS_MODIFIED_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * @author Wei Chin,Chong
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    _libraryCellEditor = new LibraryCellEditor(new ExtendedComboBox());
    _libraryCellEditor.setEditorChoices(StringLocalizer.keyToString("PLWK_EXPORT_NONE_SOURCE_LIBRARY_TABLE_FIELD_KEY"),
                                        _libraryManager);
    columnModel.getColumn(_SELECT_INDEX).setCellEditor(_checkEditor);
    columnModel.getColumn(_SELECT_INDEX).setCellRenderer(new CheckCellRenderer());
    columnModel.getColumn(_SOURCE_LIBRARY_INDEX).setCellEditor(_libraryCellEditor);
    columnModel.getColumn(_SOURCE_LIBRARY_INDEX).setCellRenderer(new LibraryCellRenderer());

    JComboBox overwriteComboBox = new JComboBox();
    overwriteComboBox.addItem(StringLocalizer.keyToString("PLWK_YES_STRING_KEY"));
    overwriteComboBox.addItem(StringLocalizer.keyToString("PLWK_NO_STRING_KEY"));
    columnModel.getColumn(_OVERWRITE_INDEX).setCellEditor(new DefaultCellEditor(overwriteComboBox));
  }

  /**
   * Overriding this method allows us to have one cell's value change apply
   * to every selected row in the table.  Multi-cell editing requires this.
   *
   * @param value Object
   * @param row int
   * @param column int
   *
   * @author Wei Chin, Chong
   */
  public void setValueAt(Object value, int row, int column)
  {
    int col = convertColumnIndexToModel(column);
    if (column == _SOURCE_LIBRARY_INDEX || column == _SELECT_INDEX)
    {
      int rows[] = getSelectedRows();
      for (int i = 0; i < rows.length; i++)
      {
        _exportPackageLayoutTableModel.setValueAt(value, rows[i], col);
      }
    }
    else
      getModel().setValueAt(value, row, column);
  }

  /**
   * @param e
   * @return String
   * @author Wei Chin, Chong
   */
  @Override
  public String getToolTipText(MouseEvent e)
  {
    Point p = e.getPoint();
    int row = rowAtPoint(p);
    int col = columnAtPoint(p);

    if(row >=0 && col == _SOURCE_LIBRARY_INDEX)
    {
      return _exportPackageLayoutTableModel.getCompPackagesAt(row).getLibraryPath();
    }
    else if (row >= 0 && col >= 0)
    {
      return getValueAt(row, col).toString();
    }
    else
    {
      return null;
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  public void stopCellsEditing()
  {
    TableColumnModel tableColumnModel = getColumnModel();
    tableColumnModel.getColumn(_SELECT_INDEX).getCellEditor().stopCellEditing();
    tableColumnModel.getColumn(_OVERWRITE_INDEX).getCellEditor().stopCellEditing();
    tableColumnModel.getColumn(_SOURCE_LIBRARY_INDEX).getCellEditor().stopCellEditing();
  }
}
