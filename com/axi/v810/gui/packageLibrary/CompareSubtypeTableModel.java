package com.axi.v810.gui.packageLibrary;

import java.util.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class CompareSubtypeTableModel extends AbstractTableModel
{  
  private List<List<Object>> _displayData = null; // a list of lists
  private final String[] _subtypeAdvaceSettingColumns = {StringLocalizer.keyToString("PLWK_CT_SUBTYPE_ADVANCE_NAME_TABLE_HEADER_KEY"),
                                                         StringLocalizer.keyToString("PLWK_CT_PROJECT_VALUE_TABLE_HEADER_KEY"),
                                                         StringLocalizer.keyToString("PLWK_CT_LIBRARY_VALUE_TABLE_HEADER_KEY")};

  public static final int NAME_COLUMN = 0;
  public static final int PROJECT_VALUE_COLUMN = 1;
  public static final int LIBRARY_VALUE_COLUMN = 2;
  
  /**
   * @author Cheah Lee Herng
   */
  public CompareSubtypeTableModel()
  {
    _displayData = new LinkedList<List<Object>>();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void populateDisplayData(Subtype projectSubtype, LibrarySubtype librarySubtype)
  {
    Assert.expect(projectSubtype != null);
    Assert.expect(librarySubtype != null);
            
    SubtypeAdvanceSettings projectSubtypeAdvanceSettings = projectSubtype.getSubtypeAdvanceSettings();
    
    _displayData.clear();
    
    // User Gain  
    List<Object> userGainList = new LinkedList<Object>();
    userGainList.add(StringLocalizer.keyToString("PLWK_CT_SUBTYPE_ADVANCE_NAME_USER_GAIN_KEY"));
    userGainList.add(projectSubtypeAdvanceSettings.getUserGain());
    userGainList.add(librarySubtype.isLegacyUserGainEnum() ? "" : librarySubtype.getUserGainEnum());    
    _displayData.add(userGainList);
    
    // Integration Level
    List<Object> integrationLevelList = new LinkedList<Object>();
    integrationLevelList.add(StringLocalizer.keyToString("PLWK_CT_SUBTYPE_ADVANCE_NAME_INTEGRATION_LEVEL_KEY"));
    integrationLevelList.add(projectSubtypeAdvanceSettings.getSignalCompensation());
    integrationLevelList.add(librarySubtype.isLegacySignalCompensationEnum() ? "" : librarySubtype.getSignalCompensationEnum());
    _displayData.add(integrationLevelList);
    
    // Artifact-Compensation Level
    List<Object> artifactCompensationLevelList = new LinkedList<Object>();
    artifactCompensationLevelList.add(StringLocalizer.keyToString("PLWK_CT_SUBTYPE_ADVANCE_NAME_ARTIFACT_COMPENSATION_LEVEL_KEY"));
    artifactCompensationLevelList.add(projectSubtypeAdvanceSettings.getArtifactCompensationState());
    artifactCompensationLevelList.add(librarySubtype.isLegacyArtifactCompensationEnum() ? "" : librarySubtype.getArtifactCompensationStateEnum());
    _displayData.add(artifactCompensationLevelList);
    
    // Magnification
    List<Object> magnificationList = new LinkedList<Object>();
    magnificationList.add(StringLocalizer.keyToString("PLWK_CT_SUBTYPE_ADVANCE_NAME_MAGNIFICATION_LEVEL_KEY"));
    magnificationList.add(projectSubtypeAdvanceSettings.getMagnificationType());
    magnificationList.add(librarySubtype.isLegacyMagnificationTypeEnum() ? "" : librarySubtype.getMagnificationTypeEnum());
    _displayData.add(magnificationList);
    
    // DRO
    List<Object> droList = new LinkedList<Object>();
    droList.add(StringLocalizer.keyToString("PLWK_CT_SUBTYPE_ADVANCE_NAME_DRO_LEVEL_KEY"));
    droList.add(projectSubtypeAdvanceSettings.getDynamicRangeOptimizationLevel());
    droList.add(librarySubtype.isLegacyDynamicRangeOptimizationLevelEnum() ? "" : librarySubtype.getDynamicRangeOptimizationLevelEnum());
    _displayData.add(droList);
    
    // Focus Method
    List<Object> focusMethodList = new LinkedList<Object>();
    focusMethodList.add(StringLocalizer.keyToString("PLWK_CT_SUBTYPE_ADVANCE_NAME_FOCUS_METHOD_KEY"));
    
    List<ComponentType> componentTypes = projectSubtype.getComponentTypes();
    if (componentTypes.isEmpty() == false)
      focusMethodList.add(componentTypes.get(0).getPadTypes().get(0).getPadTypeSettings().getGlobalSurfaceModel());
    else
      focusMethodList.add("");
    
    if (librarySubtype.hasLibraryComponentTypes())
      focusMethodList.add(librarySubtype.getLibraryComponentTypes().get(0).isLegacyGlobalSurfaceModelEnum() ? "" : librarySubtype.getLibraryComponentTypes().get(0).getGlobalSurfaceModelEnum());    
    else
      focusMethodList.add("");    
    _displayData.add(focusMethodList);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getRowCount()
  {
    if (_displayData == null)
      return 0;
    else
      return _displayData.size();
  }

  /**
   * @author Cheah Lee Herng 
   */
  public int getColumnCount()
  {
    return _subtypeAdvaceSettingColumns.length;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(_subtypeAdvaceSettingColumns != null);
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());

    return _subtypeAdvaceSettingColumns[columnIndex];
  }

  /**
   * @author Cheah Lee Herng 
   */
  public Object getValueAt(int row, int col)
  {
    List<Object> rowData = _displayData.get(row);
    Object data = null;
    data = rowData.get(col);
    
    // return original value if the data is empty
    if(data instanceof String)
    {
      String emptyString = (String)data;
      if(emptyString.length() <= 0)
        return data;
    }    
    return data;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isCellEditable(int row, int col)
  {
      return false;
  }
}
