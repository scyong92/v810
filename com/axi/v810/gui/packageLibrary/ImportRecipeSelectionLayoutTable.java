package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;

/**
 *
 * <p>Title: ImportRecipeSelectionLayoutTable</p>
 *
 * <p>Description: This table display the list of recipes to be imported.</p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: Vitrox Technogies</p>
 *
 * @author Cheah Lee Herng
 * @version 1.0
 */
public class ImportRecipeSelectionLayoutTable extends JSortTable
{
  public static final int _SELECT_INDEX = 0;
  public static final int _RECIPE_INDEX = 1;
  
  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _SELECT_COLUMN_WIDTH_PERCENTAGE = .05;
  private static final double _RECIPE_COLUMN_WIDTH_PERCENTAGE = .95;
  
  private CheckCellEditor _checkEditor = new CheckCellEditor(getBackground(), getForeground());
  
  /**
   * @author Cheah Lee Herng
   */
  public ImportRecipeSelectionLayoutTable()
  {
    // Do nothing
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_SELECT_INDEX).setPreferredWidth((int)(totalColumnWidth * _SELECT_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_RECIPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _RECIPE_COLUMN_WIDTH_PERCENTAGE));    
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    columnModel.getColumn(_SELECT_INDEX).setCellEditor(_checkEditor);
    columnModel.getColumn(_SELECT_INDEX).setCellRenderer(new CheckCellRenderer());
  }
  
  /**
   *
   * @param e
   * @return String
   *
   * @author Cheah Lee Herng
   */
  public String getToolTipText(MouseEvent e)
  {
    Point p = e.getPoint();
    int row = rowAtPoint(p);
    int col = columnAtPoint(p);

    if (row >= 0 && col >= 0)
    {
      return getValueAt(row, col).toString();
    }
    else
    {
      return null;
    }
  }
}
