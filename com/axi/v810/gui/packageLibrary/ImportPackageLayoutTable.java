package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;

import javax.swing.table.*;

import com.axi.guiUtil.*;

/**
 *
 * <p>Title: ImportPackageLayoutTable</p>
 *
 * <p>Description: This table construct the current project subtype scheme data.</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Tan Hock Zoon
 * @version 1.0
 */
public class ImportPackageLayoutTable extends JSortTable
{
  public static final int _SELECT_INDEX = 0;
  public static final int _PACKAGE_INDEX = 1;
  public static final int _JOINT_TYPE_INDEX = 2;
  public static final int _SUBTYPE_INDEX = 3;
  public static final int _IMPORTED_INDEX = 4;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _SELECT_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _PACKAGE_COLUMN_WIDTH_PERCENTAGE = .2;
  private static final double _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE = .3;
  private static final double _SUBTYPE_COLUMN_WIDTH_PERCENTAGE = .2;
  private static final double _IMPORTED_COLUMN_WIDTH_PERCENTAGE = .2;

  private CheckCellEditor _checkEditor = new CheckCellEditor(getBackground(), getForeground());;

  /**
   * @author Tan Hock Zoon
   */
  public ImportPackageLayoutTable()
  {
    /** @todo APM Code Review do nothing comment */
  }

  /**
   * @author Tan Hock Zoon
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_SELECT_INDEX).setPreferredWidth((int)(totalColumnWidth * _SELECT_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PACKAGE_INDEX).setPreferredWidth((int)(totalColumnWidth * _PACKAGE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_JOINT_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_SUBTYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _SUBTYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_IMPORTED_INDEX).setPreferredWidth((int)(totalColumnWidth * _IMPORTED_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * @author Tan Hock Zoon
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    columnModel.getColumn(_SELECT_INDEX).setCellEditor(_checkEditor);
    columnModel.getColumn(_SELECT_INDEX).setCellRenderer(new CheckCellRenderer());
  }

  /**
   *
   * @param e
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String getToolTipText(MouseEvent e)
  {
    Point p = e.getPoint();
    int row = rowAtPoint(p);
    int col = columnAtPoint(p);

    if (row >= 0 && col >= 0)
    {
      return getValueAt(row, col).toString();
    }
    else
    {
      return null;
    }
  }
}
