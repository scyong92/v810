package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;

/**
 * <p>Company: Agilent Technogies</p>
 *
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class ExportSubtypeLayoutTable extends JSortTable
{
  public static final int _SELECT_INDEX = 0;
  public static final int _PACKAGE_INDEX = 1;
  public static final int _JOINT_TYPE_INDEX = 2;
  public static final int _SUBTYPE_TYPE_INDEX = 3;
  public static final int _ADD_UPDATE_INDEX = 4;
  public static final int _NEW_MODIFIED_INDEX = 5;
  public static final int _SOURCE_LIBRARY_INDEX = 6;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _SELECT_COLUMN_WIDTH_PERCENTAGE = .05;
  private static final double _PACKAGE_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double _SUBTYPE_COLUMN_WIDTH_PERCENTAGE = .16;
  private static final double _ADD_UPDATE_COLUMN_WIDTH_PERCENTAGE = .14;
  private static final double _NEW_MODIFIED_COLUMN_WIDTH_PERCENTAGE= .15;
  private static final double _SOURCE_LIBRARY_COLUMN_WIDTH_PERCENTAGE = .2;

  private CheckCellEditor _checkEditor = new CheckCellEditor(getBackground(), getForeground());;

  private Frame _parentFrame = null;

  /**
   * @param frame Frame
   * @author Wei Chin,Chong
   */
  ExportSubtypeLayoutTable(Frame frame)
  {
    super();
    _parentFrame = frame;
  }

  /**
   * @author Wei Chin,Chong
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_SELECT_INDEX).setPreferredWidth((int)(totalColumnWidth * _SELECT_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PACKAGE_INDEX).setPreferredWidth((int)(totalColumnWidth * _PACKAGE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_SUBTYPE_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _SUBTYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_JOINT_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_ADD_UPDATE_INDEX).setPreferredWidth((int)(totalColumnWidth * _ADD_UPDATE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_NEW_MODIFIED_INDEX).setPreferredWidth((int)(totalColumnWidth * _NEW_MODIFIED_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_SOURCE_LIBRARY_INDEX).setPreferredWidth((int)(totalColumnWidth * _SOURCE_LIBRARY_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * @author Wei Chin,Chong
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel tableColumnModel = getColumnModel();
    tableColumnModel.getColumn(_SELECT_INDEX).setCellEditor(_checkEditor);
    tableColumnModel.getColumn(_SELECT_INDEX).setCellRenderer(new CheckCellRenderer());
    
    // XCR-3102 Assert when import library with empty subtype name
    // Disable subtype name editing feature in Export Wizard. This could cause unexpected crash during import.
    //tableColumnModel.getColumn(_SUBTYPE_TYPE_INDEX).setCellEditor(new SubtypeSchemeCellEditor(_parentFrame));
    //tableColumnModel.getColumn(_SUBTYPE_TYPE_INDEX).setCellRenderer(new SubtypeSchemeCellRenderer());
    
    tableColumnModel.getColumn(_SOURCE_LIBRARY_INDEX).setCellRenderer(new LibraryCellRenderer());
  }

  public Frame getParentFrame()
  {
    return _parentFrame;
  }

  /**
   * Overriding this method allows us to have one cell's value change apply
   * to every selected row in the table.  Multi-cell editing requires this.
   *
   * @param value Object
   * @param row int
   * @param column int
   *
   * @author Wei Chin, Chong
   */
  @Override
  public void setValueAt(Object value, int row, int column)
  {
    int rows[] = getSelectedRows();
    int col = convertColumnIndexToModel(column);
    if (column == _SELECT_INDEX)
    {
      for (int i = 0; i < rows.length; i++)
      {
        getModel().setValueAt(value, rows[i], col);
      }
    }
    else
      getModel().setValueAt(value, row, col);
  }

  /**
   * @param e
   * @return String
   * @author Wei Chin, Chong
   */
  @Override
  public String getToolTipText(MouseEvent e)
  {
    Point p = e.getPoint();
    int row = rowAtPoint(p);
    int col = columnAtPoint(p);

    if(row >= 0 && col == _SOURCE_LIBRARY_INDEX)
    {
      ExportSubtypeLayoutTableModel exportSubtypeLayoutTableModel = (ExportSubtypeLayoutTableModel)getModel();
      return exportSubtypeLayoutTableModel.getSubtypeSchemeAt(row).getLibraryPath();
    }
    else if (row >= 0 && col >= 0)
    {
      return getValueAt(row, col).toString();
    }
    else
    {
      return null;
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  public void stopCellsEditing()
  {
    TableColumnModel tableColumnModel = getColumnModel();
    tableColumnModel.getColumn(_SELECT_INDEX).getCellEditor().stopCellEditing();
    
    // XCR-3102 Assert when import library with empty subtype name
    // Disable subtype name editing feature in Export Wizard. This could cause unexpected crash during import.
    //tableColumnModel.getColumn(_SUBTYPE_TYPE_INDEX).getCellEditor().stopCellEditing();    
  }
}
