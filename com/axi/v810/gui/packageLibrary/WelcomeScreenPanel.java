package com.axi.v810.gui.packageLibrary;

import java.awt.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: WelcomeScreenPanel</p>
 *
 * <p>Description: This panel the welcome screen for Import and Export feature.</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Tan Hock Zoon
 * @version 1.0
 */
public class WelcomeScreenPanel extends WizardPanel
{
  private boolean _importPackage;

  /**
   *
   * @param wizardDialog
   * @param importPackage
   *
   * @author Tan Hock Zoon
   */
  public WelcomeScreenPanel(WizardDialog wizardDialog, boolean importPackage)
  {
    super(wizardDialog);

    _importPackage = importPackage;
  }

  /**
   * @author Tan Hock Zoon
   */
  private void createPanel()
  {
    removeAll();

    JPanel welcomePanel = new JPanel();
    welcomePanel.setLayout(new BoxLayout(welcomePanel, BoxLayout.Y_AXIS));

    String welcomeText = "";
    String welcomeDescText = "";

    if (_importPackage)
    {
      welcomeText = "PLWK_IMPORT_WELCOME_KEY";
      welcomeDescText = "PLWK_IMPORT_WELCOME_DESC_KEY";
    }
    else
    {
      welcomeText = "PLWK_EXPORT_WELCOME_KEY";
      welcomeDescText = "PLWK_EXPORT_WELCOME_DESC_KEY";
    }

    JLabel welcomeTextLabel = new JLabel(StringLocalizer.keyToString(welcomeText));
    welcomeTextLabel.setFont(FontUtil.getBoldFont(welcomeTextLabel.getFont(), Localization.getLocale()));

    JLabel welcomeDescTextLabel = new JLabel(StringLocalizer.keyToString(welcomeDescText));

    welcomePanel.add(welcomeTextLabel);
    welcomePanel.add(welcomeDescTextLabel);

    add(welcomePanel, BorderLayout.CENTER);
  }

  /**
   * @author Tan Hock Zoon
   */
  public void validatePanel()
  {
    //do nothing
  }

  /**
   * @author Tan Hock Zoon
   */
  public void populatePanelWithData()
  {
    createPanel();

    if (_importPackage)
    {
      _dialog.setTitle(StringLocalizer.keyToString("PLWK_IMPORT_TITLE_KEY"));
    }
    else
    {
      _dialog.setTitle(StringLocalizer.keyToString("PLWK_EXPORT_TITLE_KEY"));
    }

    WizardButtonState buttonState = new WizardButtonState(true, true, false, false);
    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Tan Hock Zoon
   */
  public void unpopulate()
  {
    //do nothing
  }

  /**
   *
   * @param name
   *
   * @author Tan Hock Zoon
   */
  public void setWizardName(String name)
  {
    _name = name;
  }

  /**
   *
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String getWizardName()
  {
    return _name;
  }

  /**
   * @author Tan Hock Zoon
   */
  public void removeObservers()
  {
    //do nothing
  }
}
