package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: ImportWizardPackagesSelectionPanel</p>
 *
 * <p>Description: This panel display a list of current project subtype schemes</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Tan Hock Zoon
 * @version 1.0
 */
public class ImportWizardPackagesSelectionPanel extends WizardPanel implements Observer
{
  private JButton _selectAllButton;
  private JButton _unselectAllButton;
  private JRadioButton _showAllRadioButton = new JRadioButton(StringLocalizer.keyToString("PLWK_SHOW_ALL_KEY"));
  private JRadioButton _showImportedRadioButton = new JRadioButton(StringLocalizer.keyToString("PLWK_SHOW_IMPORTED_ONLY_KEY"));
  private JRadioButton _showUnimportedRadioButton = new JRadioButton(StringLocalizer.keyToString("PLWK_SHOW_UNIMPORTED_ONLY_KEY"));
  private ButtonGroup _filterRadioButtonGroup = new ButtonGroup();
  private JLabel _packageSelectionDescLabel;
  private JLabel _packagesLabel;
  private JScrollPane _scrollPane;
  private JPanel _mainPanel;
  private JPanel _packagesPanel;
  private JPanel _filteringPanel;
  private JPanel _buttonPanel;
  private JPanel _packageSelectionDescPanel;
  private JPanel _drawCadPanel;
  private JCheckBox _toggleCadDisplayCheckBox = new JCheckBox(StringLocalizer.keyToString("PLWK_TOGGLE_CAD_DISPLAY_KEY"));

  private List<SubtypeScheme> _currentlySelectedSchemes;
  private DrawCadPanel _drawLandPatternPanel;
  private ImportPackageLayoutTable _packageTable;
  private ImportPackageLayoutTableModel _packageTableModel;
  private Project _project;
  private GuiObservable _guiObservable;
  private TitledBorder _drawCadTitleBorder;
    
  private LibraryWizardGeneralPersistance _libraryWizardGeneralPersistance;

  /**
   *
   * @param project Project
   * @param dialog WizardDialog
   * @param currentlySelectedSchemes List
   * @param libraryWizardGeneralPersistance
   *
   * @author Tan Hock Zoon
   */
  public ImportWizardPackagesSelectionPanel(Project project, 
                                            WizardDialog dialog, 
                                            List<SubtypeScheme> currentlySelectedSchemes,
                                            LibraryWizardGeneralPersistance libraryWizardGeneralPersistance)
  {
    super(dialog);

    Assert.expect(project != null);
    Assert.expect(currentlySelectedSchemes != null);
    Assert.expect(libraryWizardGeneralPersistance != null);

    _project = project;
    _currentlySelectedSchemes = currentlySelectedSchemes;

    //set the default radion button
    _showUnimportedRadioButton.setSelected(true);
    _guiObservable = GuiObservable.getInstance();
    
    _libraryWizardGeneralPersistance = libraryWizardGeneralPersistance;
  }

  /**
   * @author Tan Hock Zoon
   */
  private void init()
  {
    _guiObservable.addObserver(this);

    setLayout(new BorderLayout(5, 5));

    _mainPanel = new JPanel(new BorderLayout(5, 5));
    _mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
    _packagesPanel = new JPanel(new BorderLayout(5, 5));
    _filteringPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _packageSelectionDescPanel = new JPanel(new BorderLayout(5, 5));
    _packageSelectionDescPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

    _packageTableModel = new ImportPackageLayoutTableModel(_currentlySelectedSchemes);
    _packageTable = new ImportPackageLayoutTable();

    _selectAllButton = new JButton(StringLocalizer.keyToString("GUI_SELECT_SELECT_ALL_KEY"));
    _unselectAllButton = new JButton(StringLocalizer.keyToString("PLWK_UNSELECT_ALL_KEY"));
    _packagesLabel = new JLabel(StringLocalizer.keyToString("PLWK_PACKAGES_KEY"));
    _packageSelectionDescLabel = new JLabel(StringLocalizer.keyToString("PLWK_IMPORT_SELECT_PACKAGES_DESC_KEY"));
    _scrollPane = new JScrollPane();
    _drawLandPatternPanel = new DrawCadPanel(350, 400);

    _drawCadTitleBorder = BorderFactory.createTitledBorder(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY"));
    _drawCadPanel = new JPanel(new BorderLayout());
    _drawCadPanel.setBorder(_drawCadTitleBorder);
  }

  /**
   * @author Tan Hock Zoon
   */
  private void createPanel()
  {
    removeAll();
    init();

    _filterRadioButtonGroup.add(_showAllRadioButton);
    _filterRadioButtonGroup.add(_showImportedRadioButton);
    _filterRadioButtonGroup.add(_showUnimportedRadioButton);

    _filteringPanel.add(_packagesLabel);
    _filteringPanel.add(_showAllRadioButton);
    _filteringPanel.add(_showImportedRadioButton);
    _filteringPanel.add(_showUnimportedRadioButton);

    _buttonPanel.add(_selectAllButton);
    _buttonPanel.add(_unselectAllButton);

    _drawCadPanel.add(_drawLandPatternPanel, BorderLayout.CENTER);
    _drawCadPanel.add(_toggleCadDisplayCheckBox, BorderLayout.SOUTH);

    _packagesPanel.add(_filteringPanel, BorderLayout.NORTH);
    _packagesPanel.add(_drawCadPanel, BorderLayout.EAST);
    _packagesPanel.add(_scrollPane, BorderLayout.CENTER);

    _packageTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    ListSelectionModel rowSM = _packageTable.getSelectionModel();
    rowSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();

        if (lsm.getValueIsAdjusting() == false)
        {
          updateCadDisplay();
        }
      }
    });

    _scrollPane.getViewport().add(_packageTable, null);

    _packageSelectionDescPanel.setBackground(Color.WHITE);
    _packageSelectionDescPanel.add(_packageSelectionDescLabel, BorderLayout.CENTER);

    _mainPanel.add(_packageSelectionDescPanel, BorderLayout.NORTH);
    _mainPanel.add(_packagesPanel, BorderLayout.CENTER);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);

    add(_mainPanel, BorderLayout.CENTER);

    _selectAllButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        _packageTableModel.selectAllPackages();
        validatePanel();
      }
    });

    _unselectAllButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        _packageTableModel.unselectAllPackages();
        validatePanel();
      }
    });

    _showAllRadioButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent ie)
      {
        populatePanelWithFilteringData();
        validatePanel();
      }
    });

    _showImportedRadioButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent ie)
      {
        populatePanelWithFilteringData();
        validatePanel();
      }
    });

    _showUnimportedRadioButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent ie)
      {
        populatePanelWithFilteringData();
        validatePanel();
      }
    });
    
    _toggleCadDisplayCheckBox.setSelected(_libraryWizardGeneralPersistance.getImportPackageSelectionCadDisplay());
    _toggleCadDisplayCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if(_toggleCadDisplayCheckBox.isSelected())
        {
          _libraryWizardGeneralPersistance.setImportPackageSelectionCadDisplay(true);
          updateCadDisplay();
        }
        else
        {
          _libraryWizardGeneralPersistance.setImportPackageSelectionCadDisplay(false);
          updateCadDisplay();
        }
      }
    });

    SwingUtils.setTableAndScrollPanelToPreferredSize(_scrollPane, _packageTable);
  }

  /**
   * @author Tan Hock Zoon
   */
  public void validatePanel()
  {
    WizardButtonState buttonState;

    if (_packageTableModel.getSelectedSchemes().size() != 0)
    {
      buttonState = new WizardButtonState(true, true, false, true);
    }
    else
    {
      buttonState = new WizardButtonState(false, true, false, true);
    }

    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Tan Hock Zoon
   */
  public void populatePanelWithData()
  {
    createPanel();

    _dialog.setTitle(StringLocalizer.keyToString("PLWK_IMPORT_SELECT_PACKAGE_DIALOG_TITLE_KEY"));

    populatePanelWithFilteringData();

    validatePanel();
  }

  /**
   * @author Tan Hock Zoon
   */
  private void populatePanelWithFilteringData()
  {
    if (_showAllRadioButton.isSelected())
    {
      _packageTableModel.populateWithPanelData(_project.getPanel(), _packageTableModel.FILTER_BY_ALL);
    }
    else if (_showImportedRadioButton.isSelected())
    {
      _packageTableModel.populateWithPanelData(_project.getPanel(), _packageTableModel.FILTER_BY_IMPORTED);
    }
    else if (_showUnimportedRadioButton.isSelected())
    {
      _packageTableModel.populateWithPanelData(_project.getPanel(), _packageTableModel.FILTER_BY_UNIMPORTED);
    }

    _packageTable.setModel(_packageTableModel);
    _packageTable.setEditorsAndRenderers();
    _packageTable.setPreferredColumnWidths();
    
    _packageTableModel.fireTableDataChanged();
    
    validate();
    repaint();
  }

  /**
   * @author Tan Hock Zoon
   */
  public void unpopulate()
  {
    if (_drawLandPatternPanel != null)
    {
      _drawLandPatternPanel.resetPackageGraphics();
      _drawLandPatternPanel.dispose();
    }

    if (_packageTableModel != null)
      _packageTableModel.clear();

    if (_packageTable != null)
      _packageTable.removeAll();

    if (_currentlySelectedSchemes != null)
      _currentlySelectedSchemes.clear();

    _packageTableModel = null;
    _packageTable = null;
    _currentlySelectedSchemes = null;
    _drawLandPatternPanel = null;

    removeAll();
  }

  /**
   * @author Tan Hock Zoon
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null);
    _name = name;
  }

  /**
   * @author Tan Hock Zoon
   */
  public String getWizardName()
  {
    Assert.expect(_name != null);
    return _name;
  }

  /**
   * @author Tan Hock Zoon
   */
  public void removeObservers()
  {
    _guiObservable.deleteObserver(this);
  }

  /**
   * @param observable Observable
   * @param object Object
   * @author Tan Hock Zoon
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof GuiObservable)
        {
          GuiEvent guiEvent = (GuiEvent)object;
          GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
          if (guiEventEnum instanceof SelectionEventEnum)
          {
            SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
            if (selectionEventEnum.equals(SelectionEventEnum.IMPORT_PACKAGES_SELECTION))
            {
              validatePanel();
            }
          }
        }
      }
    });
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void updateCadDisplay()
  {
    if (_drawLandPatternPanel != null)
    {      
      // we want to select the pad(s) in the graphics window corresponding to the newly selected table row(s)
      int selectedRows = _packageTable.getSelectedRow();
      if (selectedRows >= 0)
      {
        if (_libraryWizardGeneralPersistance.getImportPackageSelectionCadDisplay() == true)
        {
          _drawLandPatternPanel.resetPackageGraphics();
          CompPackage compPackage = _packageTableModel.getSubtypeSchemeAt(selectedRows).getCompPackage();
          _drawLandPatternPanel.drawPackage(compPackage);
          _drawLandPatternPanel.scaleDrawing();
          _drawCadTitleBorder.setTitle(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY") + compPackage.getShortName());
        }
        else
        {
          _drawLandPatternPanel.resetPackageGraphics();
          _drawCadTitleBorder.setTitle(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY"));
        }
      }
      else
      {
        _drawLandPatternPanel.resetPackageGraphics();
        _drawCadTitleBorder.setTitle(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY"));
      }
      
      revalidate();
      repaint();
    }
  }
}
