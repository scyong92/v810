package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class CompareSubtypeTable extends JTable
{
  public static final int _SUBTYPE_ADVANCE_SETTING_INDEX = 0;
  public static final int _PROJECT_SUBTYPE_VALUE_INDEX = 1;
  public static final int _LIBRARY_SUBTYPE_VALUE_INDEX = 2;
  
  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _SUBTYPE_ADVANCE_SETTING_COLUMN_WIDTH_PERCENTAGE = .55;
  private static final double _PROJECT_SUBTYPE_VALUE_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double _LIBRARY_SUBTYPE_VALUE_COLUMN_WIDTH_PERCENTAGE = .15;
  
  /**
   * @author Cheah Lee Herng
   */
  public CompareSubtypeTable(TableModel tableModel)
  {
    super(tableModel);
    getTableHeader().setReorderingAllowed(false);
    //renderColumns(new CompareThresholdTableCellRenderer(this,(CompareThresholdTableModel)tableModel, algorithm,subtype));
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    setPreferredColumnWidths();
  }
  
  /**
   * Render all columns with the specified cell renderer
   * @param cellRender TableCellRenderer
   *
   * @author Cheah Lee Herng
   */
  public void renderColumns(TableCellRenderer cellRender)
  {
    for (int i = 0; i < this.getModel().getColumnCount(); i++)
    {
      renderColumn(this.getColumnModel().getColumn(i), cellRender);
    }
  }
  
  /**
   * @author Wei Chin,Chong
   */
  public void renderColumn(TableColumn col, TableCellRenderer cellRender)
  {
    try
    {
      col.setCellRenderer(cellRender);
    }
    catch(Exception e)
    {
      Assert.expect(false,e.getMessage());
    }
  }
  
  /**
   * @author Wei Chin,Chong
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_SUBTYPE_ADVANCE_SETTING_INDEX).setPreferredWidth((int)(totalColumnWidth * _SUBTYPE_ADVANCE_SETTING_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PROJECT_SUBTYPE_VALUE_INDEX).setPreferredWidth((int)(totalColumnWidth * _PROJECT_SUBTYPE_VALUE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_LIBRARY_SUBTYPE_VALUE_INDEX).setPreferredWidth((int)(totalColumnWidth * _LIBRARY_SUBTYPE_VALUE_COLUMN_WIDTH_PERCENTAGE));    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public String getToolTipText(MouseEvent e)
  {
    Point p = e.getPoint();
    int row = rowAtPoint(p);
    int col = columnAtPoint(p);

    if (row >= 0 && col >= 0)
    {
      return getValueAt(row, col).toString();
    }
    else
    {
      return null;
    }
  }
}
