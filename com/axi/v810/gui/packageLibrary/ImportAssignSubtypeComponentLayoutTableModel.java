package com.axi.v810.gui.packageLibrary;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.util.*;

/**
 * @author Tan Hock Zoon
 */
public class ImportAssignSubtypeComponentLayoutTableModel extends DefaultSortTableModel
{
  private SubtypeScheme _subtypeScheme;
  private List<ComponentType> _componentTypes = new ArrayList<ComponentType>();
  private Collection<ComponentType> _currentlySelectedComponents = new ArrayList<ComponentType>();

  private static final String _COMPONENT_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_GROUPINGS_COMPONENT_KEY");
  private static final String _PACKAGE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_PACKAGE_TABLE_HEADER_KEY");
  private static final String _JOINT_TYPE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_JOINT_TYPE_TABLE_HEADER_KEY");
  private static final String _SUBTYPE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_SUBTYPE_TABLE_HEADER_KEY");
  private static final String _LIBRARY_JOINT_TYPE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_IMPORT_LIBRARY_JOINT_TYPE_KEY");
  private static final String _LIBRARY_PROJECT_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_IMPORT_LIBRARY_PROJECT_KEY");
  private static final String _LIBRARY_SUBTYPE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_IMPORT_LIBRARY_SUBTYPE_KEY");

  private final String[] _tableColumns =
      {_COMPONENT_COLUMN_HEADING,
      _PACKAGE_COLUMN_HEADING,
      _JOINT_TYPE_COLUMN_HEADING,
      _SUBTYPE_COLUMN_HEADING,
      _LIBRARY_JOINT_TYPE_COLUMN_HEADING,
      _LIBRARY_PROJECT_COLUMN_HEADING,
      _LIBRARY_SUBTYPE_COLUMN_HEADING};

  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;

  /**
   * @author Wei Chin, Chong
   */
  public ImportAssignSubtypeComponentLayoutTableModel()
  {
    // do nothing
  }

  /**
   * @author Tan Hock Zoon
   */
  void clear()
  {
    _componentTypes.clear();
    _currentlySelectedComponents.clear();
    _currentSortColumn = 0;
    _currentSortAscending = true;
  }

  /**
   *
   * @param column int
   * @param ascending boolean
   *
   * @author Tan Hock Zoon
   */
  public void sortColumn(int column, boolean ascending)
  {
    if (column == 0)  // sort on ref.des.
      Collections.sort(_componentTypes, new ComponentTypeComparator(ascending, ComponentTypeComparatorEnum.REFERENCE_DESIGNATOR));
    else if (column == 1)  // sort on package
      Collections.sort(_componentTypes, new ComponentTypeComparator(ascending, ComponentTypeComparatorEnum.PACKAGE));
    else if (column == 2)  // sort on joint type
      Collections.sort(_componentTypes, new ComponentTypeComparator(ascending, ComponentTypeComparatorEnum.JOINT_TYPE));
    else if (column == 3)  // sort on subtype
      Collections.sort(_componentTypes, new ComponentTypeComparator(ascending, ComponentTypeComparatorEnum.SUBTYPE));

    _currentSortColumn = column;
    _currentSortAscending = ascending;
  }

  /**
   * @author Tan Hock Zoon
   */
  public int getColumnCount()
  {
    Assert.expect(_tableColumns != null);
    return _tableColumns.length;
  }

  /**
   *
   * @param columnIndex int
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(_tableColumns != null);
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_tableColumns[columnIndex];
  }

  /**
   * @author Tan Hock Zoon
   */
  public int getRowCount()
  {
    if (_componentTypes == null)
      return 0;
    else
      return _componentTypes.size();
  }

  /**
   *
   * @param rowIndex int
   * @param columnIndex int
   * @return boolean
   *
   * @author Tan Hock Zoon
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if (_componentTypes == null || _componentTypes.size() == 0)
    {
      return false;
    }

    switch (columnIndex)
    {
      case ImportAssignSubtypeComponentLayoutTable._LIBRARY_SUBTYPE_INDEX:
        return true;
    }
    return false;
  }

  /**
   *
   * @param object Object
   * @param rowIndex int
   * @param columnIndex int
   *
   * @author Tan Hock Zoon
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    ComponentType compType = _componentTypes.get(rowIndex);

    switch (columnIndex)
    {
      case ImportAssignSubtypeComponentLayoutTable._LIBRARY_SUBTYPE_INDEX:
      {
        LibrarySubtypeScheme librarySubtypeScheme = (LibrarySubtypeScheme)object;
        compType.setAssignLibrarySubtypeScheme(librarySubtypeScheme);
        break;
      }
    }
  }

  /**
   *
   * @param rowIndex int
   * @param columnIndex int
   * @return Object
   *
   * @author Tan Hock Zoon
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_componentTypes != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    if (rowIndex >= getRowCount())
    {
      return value;
    }

    ComponentType compType = _componentTypes.get(rowIndex);
    CompPackage compPackage = compType.getCompPackage();

    //get the appropriate data item that corresponds to the column
    switch (columnIndex)
    {
      case ImportAssignSubtypeComponentLayoutTable._COMPONENT_INDEX:
      {
        value = compType;
        break;
      }
      case ImportAssignSubtypeComponentLayoutTable._PACKAGE_INDEX:
      {
        value = compPackage;
        break;
      }
      case ImportAssignSubtypeComponentLayoutTable._JOINT_TYPE_INDEX:
      {
        value = TestDev.getComponentDisplayJointType(compType);
        break;
      }

      case ImportAssignSubtypeComponentLayoutTable._SUBTYPE_INDEX:
      {
        value = TestDev.getComponentDisplaySubtype(compType);
        break;
      }
      case ImportAssignSubtypeComponentLayoutTable._LIBRARY_JOINT_TYPE_INDEX:
      {
        if(compType.getAssignLibrarySubtypeScheme().useOneJointType())
          value = compType.getAssignLibrarySubtypeScheme().getLibrarySubtypes().get(0).getJointTypeName();
        else
          value = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
        break;
      }
      case ImportAssignSubtypeComponentLayoutTable._LIBRARY_PROJECT_INDEX:
      {
        value = compType.getAssignLibrarySubtypeScheme().getLibraryProjectName();
        break;
      }
      case ImportAssignSubtypeComponentLayoutTable._LIBRARY_SUBTYPE_INDEX:
      {
        value = compType.getAssignLibrarySubtypeScheme().toString();
        break;
      }

      default:
      {
        Assert.expect(false);
      }
    }
    return value;
  }

  /**
   * @author Tan Hock Zoon
   */
  public Collection getSelectedPackages()
  {
    return _currentlySelectedComponents;
  }

  /**
   *
   * @param row int
   * @return ComponentType
   *
   * @author Tan Hock Zoon
   */
  ComponentType getComponentTypeAt(int row)
  {
    return _componentTypes.get(row);
  }

  /**
   *
   * @param subtypeScheme SubtypeScheme
   *
   * @author Tan Hock Zoon
   */
  void populateWithPanelData(SubtypeScheme subtypeScheme)
  {
    Assert.expect(subtypeScheme != null);

    _subtypeScheme = subtypeScheme;

    _componentTypes = subtypeScheme.getComponentTypes();

    sortColumn(_currentSortColumn, _currentSortAscending);

    // components may have been deleted, so if they were previously selected, we need to remove them.
    Iterator<ComponentType> it = _currentlySelectedComponents.iterator();
    while(it.hasNext())
    {
      if (_componentTypes.contains(it.next()) == false)
        it.remove();
    }
  }

  /**
   * @param componentTypes Collection
   *
   * @author Wei Chin, Chong
   */
  void setSelectedComponentTypes(Collection<ComponentType> componentTypes)
  {
    Assert.expect(componentTypes != null);
    _currentlySelectedComponents = componentTypes;
  }

  /**
   * @return Collection
   *
   * @author Wei Chin, Chong
   */
  Collection<ComponentType> getSelectedComponentTypes()
  {
    Assert.expect(_currentlySelectedComponents != null);
    return _currentlySelectedComponents;
  }

  /**
   * @return boolean
   *
   * @author Wei Chin, Chong
   */
  boolean isCommonSelectedComponentType()
  {
    Assert.expect(_currentlySelectedComponents != null);
    if (_currentlySelectedComponents.isEmpty())
      return false;
    CompPackage compPackage = null;
    for (ComponentType componentType : _currentlySelectedComponents)
    {
      if (compPackage == null)
        compPackage = componentType.getCompPackage();
      else
      {
        if (compPackage.equals(componentType.getCompPackage()) == false)
          return false;
      }
    }
    return true;
  }

  /**
   * @param subtypeScheme SubtypeScheme
   * @author Wei Chin, Chong
   */
  public void setSubtypeScheme(SubtypeScheme subtypeScheme)
  {
    Assert.expect(subtypeScheme != null);
    _subtypeScheme = subtypeScheme;
  }

  /**
   * @return SubtypeScheme
   * @author Wei Chin, Chong
   */
  public SubtypeScheme getSubtypeScheme()
  {
    Assert.expect(_subtypeScheme != null);
    return _subtypeScheme;
  }
}
