package com.axi.v810.gui.packageLibrary;

import java.io.*;

import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class LibraryWizardGeneralPersistance implements Serializable
{
  private boolean _exportPackageSelectionCadDisplay = true;
  private boolean _importPackageSelectionCadDisplay = true;
  private boolean _importRefinePackageCurrentProjectCadDisplay = true;
  private boolean _importRefinePackageLibraryProjectCadDisplay = true;
  
  /**
   * @author Cheah Lee Herng
   */
  public LibraryWizardGeneralPersistance()
  {
    // Do nothing
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public LibraryWizardGeneralPersistance readSettings()
  {
    LibraryWizardGeneralPersistance persistance = null;
    try
    {
      if (FileUtilAxi.exists(FileName.getPackageLibraryGeneralPersistFullPath()))
        persistance = (LibraryWizardGeneralPersistance) FileUtilAxi.loadObjectFromSerializedFile(FileName.getPackageLibraryGeneralPersistFullPath());
      else
        persistance = new LibraryWizardGeneralPersistance();
    }
    catch(DatastoreException de)
    {
      persistance = new LibraryWizardGeneralPersistance();
    }

    return persistance;
  }
  
  /**
   *
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void writeSettings() throws DatastoreException
  {
    FileUtilAxi.saveObjectToSerializedFile(this, FileName.getPackageLibraryGeneralPersistFullPath());
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setExportPackageSelectionCadDisplay(boolean exportPackageSelectionCadDisplay)
  {
    _exportPackageSelectionCadDisplay = exportPackageSelectionCadDisplay;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean getExportPackageSelectionCadDisplay()
  {
    return _exportPackageSelectionCadDisplay;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setImportPackageSelectionCadDisplay(boolean importPackageSelectionCadDisplay)
  {
    _importPackageSelectionCadDisplay = importPackageSelectionCadDisplay;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean getImportPackageSelectionCadDisplay()
  {
    return _importPackageSelectionCadDisplay;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setImportRefinePackageCurrentProjectCadDisplay(boolean importRefinePackageCurrentProjectCadDisplay)
  {
    _importRefinePackageCurrentProjectCadDisplay = importRefinePackageCurrentProjectCadDisplay;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean getImportRefinePackageCurrentProjectCadDisplay()
  {
    return _importRefinePackageCurrentProjectCadDisplay;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setImportRefinePackageLibraryProjectCadDisplay(boolean importRefinePackageLibraryProjectCadDisplay)
  {
    _importRefinePackageLibraryProjectCadDisplay = importRefinePackageLibraryProjectCadDisplay;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean getImportRefinePackageLibraryProjectCadDisplay()
  {
    return _importRefinePackageLibraryProjectCadDisplay;
  }
}
