package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.v810.business.packageLibrary.*;

/**
 *
 * <p>Title: ImportRefinePackageLayoutTable</p>
 *
 * <p>Description: This table construct both current project subtype scheme and library subtype scheme.</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Tan Hock Zoon
 * @version 1.0
 */
public class ImportRefinePackageLayoutTable extends JSortTable
{
  public static final int _SELECT_INDEX = 0;
  public static final int _PACKAGE_INDEX = 1;
  public static final int _JOINT_TYPE_INDEX = 2;
  public static final int _SUBTYPE_INDEX = 3;
  public static final int _LIBRARY_PATH_INDEX = 4;  
  public static final int _LIBRARY_PACKAGE_INDEX = 5;
  public static final int _LIBRARY_JOINT_TYPE_INDEX = 6;
  public static final int _LIBRARY_PROJECT_INDEX = 7;
  public static final int _LIBRARY_SUBTYPE_INDEX = 8;
  public static final int _LIBRARY_SUBTYPE_DATE_INDEX = 9;
  public static final int _MATCHES_INDEX = 10;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _SELECT_COLUMN_WIDTH_PERCENTAGE = .05;
  private static final double _PACKAGE_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _SUBTYPE_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _LIBRARY_PATH_COLUMN_WIDTH_PERCENTAGE = .1;  
  private static final double _LIBRARY_PACKAGE_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _LIBRARY_JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _LIBRARY_PROJECT_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _LIBRARY_SUBTYPE_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _LIBRARY_SUBTYPE_DATE_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _MATCHES_COLUMN_WIDTH_PERCENTAGE = .05;

  private CheckCellEditor _checkEditor = new CheckCellEditor(getBackground(), getForeground());;
  private CheckCellRenderer _checkCellRenderer = new CheckCellRenderer();
  private LibraryPathCellEditor _libraryPathCellEditor = new LibraryPathCellEditor(new ExtendedComboBox());
  private LibraryPathCellRenderer _libraryPathCellRenderer = new LibraryPathCellRenderer();  
  private LibraryPackageCellEditor _libraryPackageCellEditor = new LibraryPackageCellEditor(new JComboBox());
  private LibraryPackageCellRenderer _libraryPackageCellRenderer = new LibraryPackageCellRenderer();
  private LibraryJointTypeCellEditor _libraryJointTypeCellEditor = new LibraryJointTypeCellEditor(new JComboBox());
  private LibraryJointTypeCellRenderer _libraryJointTypeCellRenderer = new LibraryJointTypeCellRenderer();  
  private LibraryProjectCellRenderer _libraryProjectCellRenderer = new LibraryProjectCellRenderer();
  private LibrarySubtypeCellEditor _librarySubtypeCellEditor = new LibrarySubtypeCellEditor(new ScrollableComboBox());
  private LibrarySubtypeCellRenderer _librarySubtypeCellRenderer = new LibrarySubtypeCellRenderer();
  private LibraryMatchCellRenderer _libraryMatchCellRenderer = new LibraryMatchCellRenderer();

  /**
   * @author Tan Hock Zoon
   */
  public ImportRefinePackageLayoutTable()
  {
    //do nothing
  }

  /**
   * @author Tan Hock Zoon
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_SELECT_INDEX).setPreferredWidth((int)(totalColumnWidth * _SELECT_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PACKAGE_INDEX).setPreferredWidth((int)(totalColumnWidth * _PACKAGE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_JOINT_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_SUBTYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _SUBTYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_LIBRARY_PATH_INDEX).setPreferredWidth((int)(totalColumnWidth * _LIBRARY_PATH_COLUMN_WIDTH_PERCENTAGE));    
    columnModel.getColumn(_LIBRARY_PACKAGE_INDEX).setPreferredWidth((int)(totalColumnWidth * _LIBRARY_PACKAGE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_LIBRARY_JOINT_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _LIBRARY_JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_LIBRARY_PROJECT_INDEX).setPreferredWidth((int)(totalColumnWidth * _LIBRARY_PROJECT_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_LIBRARY_SUBTYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _LIBRARY_SUBTYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_LIBRARY_SUBTYPE_DATE_INDEX).setPreferredWidth((int)(totalColumnWidth * _LIBRARY_SUBTYPE_DATE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_MATCHES_INDEX).setPreferredWidth((int)(totalColumnWidth * _MATCHES_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * @author Tan Hock Zoon
   */
  public void clear()
  {
    _checkEditor = null;
    _checkCellRenderer = null;
    _libraryPathCellEditor= null;
    _libraryPathCellRenderer = null;    
    _libraryProjectCellRenderer = null;
    _libraryPackageCellEditor = null;
    _libraryPackageCellRenderer = null;
    _libraryJointTypeCellEditor = null;
    _libraryJointTypeCellRenderer = null;
    _librarySubtypeCellEditor = null;
    _librarySubtypeCellRenderer = null;
    _libraryMatchCellRenderer = null;
  }

  /**
   * @author Tan Hock Zoon
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    columnModel.getColumn(_SELECT_INDEX).setCellEditor(_checkEditor);
    columnModel.getColumn(_SELECT_INDEX).setCellRenderer(_checkCellRenderer);

    columnModel.getColumn(_LIBRARY_PATH_INDEX).setCellEditor(_libraryPathCellEditor);
    columnModel.getColumn(_LIBRARY_PATH_INDEX).setCellRenderer(_libraryPathCellRenderer);        

    columnModel.getColumn(_LIBRARY_PACKAGE_INDEX).setCellEditor(_libraryPackageCellEditor);
    columnModel.getColumn(_LIBRARY_PACKAGE_INDEX).setCellRenderer(_libraryPackageCellRenderer);

    columnModel.getColumn(_LIBRARY_JOINT_TYPE_INDEX).setCellEditor(_libraryJointTypeCellEditor);
    columnModel.getColumn(_LIBRARY_JOINT_TYPE_INDEX).setCellRenderer(_libraryJointTypeCellRenderer);
        
    columnModel.getColumn(_LIBRARY_PROJECT_INDEX).setCellRenderer(_libraryProjectCellRenderer);

    columnModel.getColumn(_LIBRARY_SUBTYPE_INDEX).setCellEditor(_librarySubtypeCellEditor);
    columnModel.getColumn(_LIBRARY_SUBTYPE_INDEX).setCellRenderer(_librarySubtypeCellRenderer);
    columnModel.getColumn(_MATCHES_INDEX).setCellRenderer(_libraryMatchCellRenderer);
  }

  /**
   *
   * @param e
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String getToolTipText(MouseEvent e)
  {
    Point p = e.getPoint();
    int row = rowAtPoint(p);
    int col = columnAtPoint(p);

    if (row >= 0 && col >= 0)
    {
      if (col == _LIBRARY_PATH_INDEX)
      {
        SubtypeScheme subtypeScheme = ((ImportRefinePackageLayoutTableModel) getModel()).getSubtypeSchemeAt(row);

        if (subtypeScheme.getLibraryPathList().size() > 0)
          return subtypeScheme.getSelectedLibraryPath();
      }

      return getValueAt(row, col).toString();
    }
    else
    {
      return null;
    }
  }
}
