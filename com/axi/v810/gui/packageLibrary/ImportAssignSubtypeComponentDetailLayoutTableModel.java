package com.axi.v810.gui.packageLibrary;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class ImportAssignSubtypeComponentDetailLayoutTableModel extends DefaultSortTableModel
{
  private static final String _JOINT_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_IMPORT_PAD_NAME_HEADER_KEY");
  private static final String _JOINT_TYPE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_JOINT_TYPE_TABLE_HEADER_KEY");
  private static final String _SUBTYPE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_SUBTYPE_TABLE_HEADER_KEY");
  private static final String _LIBRARY_JOINT_TYPE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_IMPORT_LIBRARY_JOINT_TYPE_KEY");
  private static final String _LIBRARY_SUBTYPE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_IMPORT_LIBRARY_SUBTYPE_KEY");

  private List<ComponentType> _componentTypes = new ArrayList<ComponentType>();
  private List<PadType> _componentPads = new ArrayList<PadType>();  // list of PadType objects
  private List<PadType> _currentlySelectedPadTypes = new ArrayList<PadType>();

  private final String _NOTEST = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOTEST_KEY");
  private final String _NOLOAD = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOLOAD_KEY");
  private final String _UNTESTABLE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_UNTESTABLE_KEY");

  private final String[] _componentTableColumns =
      {_JOINT_COLUMN_HEADING,
      _JOINT_TYPE_COLUMN_HEADING,
      _SUBTYPE_COLUMN_HEADING,
      _LIBRARY_JOINT_TYPE_COLUMN_HEADING,
      _LIBRARY_SUBTYPE_COLUMN_HEADING};

  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;

  /**
   * @author Wei Chin, Chong
   */
  public ImportAssignSubtypeComponentDetailLayoutTableModel()
  {
    _componentPads = new ArrayList<PadType>();
  }

  /**
   * @author Wei Chin, Chong
   */
  void clear()
  {
    if (_componentPads != null)
      _componentPads.clear();
    if (_currentlySelectedPadTypes != null)
      _currentlySelectedPadTypes.clear();
    if (_componentTypes != null)
      _componentTypes.clear();
  }

  /**
   * @author Wei Chin, Chong
   */
  public void populateWithComponentData(Collection<ComponentType> componentTypes)
  {
    Assert.expect(componentTypes != null);
    Assert.expect(componentTypes.isEmpty() == false);

    _componentTypes = new ArrayList<ComponentType>(componentTypes);
    _componentPads = _componentTypes.get(0).getPadTypes();
    sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableDataChanged();
  }

  /**
   * @author Wei Chin, Chong
   */
  public void sortColumn(int columnIndex, boolean ascending)
  {
    switch (columnIndex)
    {
      case ImportAssignSubtypeComponentDetailLayoutTable._PAD_NAME:
        Collections.sort(_componentPads, new AlphaNumericComparator(ascending));
        break;

      case ImportAssignSubtypeComponentDetailLayoutTable._JOINT_TYPE_INDEX:
        Collections.sort(_componentPads, new PadJointTypeOrSubtypeComparator(ascending, true, false, false, false, false));
        break;

      case ImportAssignSubtypeComponentDetailLayoutTable._SUBTYPE_INDEX:
        Collections.sort(_componentPads, new PadJointTypeOrSubtypeComparator(ascending, false, false, false, false, false));
        break;

      case ImportAssignSubtypeComponentDetailLayoutTable._LIBRARY_JOINT_TYPE_INDEX:
        break;

      case ImportAssignSubtypeComponentDetailLayoutTable._LIBRARY_SUBTYPE_INDEX:
        break;

      default:
        Assert.expect(false, "Column index out of range: " + columnIndex);
    }
    _currentSortColumn = columnIndex;
    _currentSortAscending = ascending;
  }

  /**
   * @author Wei Chin, Chong
   */
  public int getColumnCount()
  {
    return _componentTableColumns.length;
  }

  /**
   * @author Wei Chin, Chong
   */
  public int getRowCount()
  {
    if (_componentPads == null)
      return 0;
    else
      return _componentPads.size();
  }

  /**
   * @author Wei Chin, Chong
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_componentTableColumns[columnIndex];
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    if(rowIndex < 0)
      return false;

    switch (columnIndex)
    {
      case ImportAssignSubtypeComponentDetailLayoutTable._PAD_NAME:
        return false;

      case ImportAssignSubtypeComponentDetailLayoutTable._JOINT_TYPE_INDEX:
        return false;

      case ImportAssignSubtypeComponentDetailLayoutTable._SUBTYPE_INDEX:
        return false;

      case ImportAssignSubtypeComponentDetailLayoutTable._LIBRARY_JOINT_TYPE_INDEX:
        return false;

      case ImportAssignSubtypeComponentDetailLayoutTable._LIBRARY_SUBTYPE_INDEX:
        return false;

      default:
        Assert.expect(false, "Column index out of range: " + columnIndex);
    }
    return false;
  }

  /**
   * @param rowIndex int
   * @param columnIndex int
   * @return Object
   *
   * @author Wei Chin, Chong
   */
  public synchronized Object getValueAt(int rowIndex, int columnIndex)
  {
    PadType padType = _componentPads.get(rowIndex);

    String libraryJointTypeName = "";
    String librarySubtypename = "";

    if(_componentTypes.get(0).getAssignLibrarySubtypeScheme().useOneJointType())
      libraryJointTypeName = _componentTypes.get(0).getAssignLibrarySubtypeScheme().getLibraryPackagePins().get(0).getJointTypeName();
    else
      libraryJointTypeName = _componentTypes.get(0).getAssignLibrarySubtypeScheme().getLibraryPackagePins().get(rowIndex).getJointTypeName();

    if(_componentTypes.get(0).getAssignLibrarySubtypeScheme().useOneSubtype())
      librarySubtypename = _componentTypes.get(0).getAssignLibrarySubtypeScheme().getLibraryPackagePins().get(0).getLibrarySubtype().getSubtypeName();
    else
      librarySubtypename = _componentTypes.get(0).getAssignLibrarySubtypeScheme().getLibraryPackagePins().get(rowIndex).getLibrarySubtype().getSubtypeName();

    Object value = null;
    switch(columnIndex)
    {
      case ImportAssignSubtypeComponentDetailLayoutTable._PAD_NAME:
        value = padType;
        break;

      case ImportAssignSubtypeComponentDetailLayoutTable._JOINT_TYPE_INDEX:
        value = padType.getPackagePin().getJointTypeEnum();
        break;

      case ImportAssignSubtypeComponentDetailLayoutTable._SUBTYPE_INDEX:
        if (padType.getPadTypeSettings().isTestable() == false)
          value = _UNTESTABLE;
        else if (padType.getComponentType().isLoaded() == false)
          value = _NOLOAD;
        else if (padType.isInspected() == false)
          value = _NOTEST;
        else
          value = padType.getSubtype().getShortName();
        break;

      case ImportAssignSubtypeComponentDetailLayoutTable._LIBRARY_JOINT_TYPE_INDEX:
        value = libraryJointTypeName;
        break;

      case ImportAssignSubtypeComponentDetailLayoutTable._LIBRARY_SUBTYPE_INDEX:
        value = librarySubtypename;
        break;

      default:
        Assert.expect(false, "Column index out of range: " + columnIndex);
    }
    return value;
  }

  /**
   * @author Wei Chin, Chong
   */
  public synchronized void setValueAt(Object value, int rowIndex, int columnIndex)
  {
    // we do not support set functions
    switch(columnIndex)
    {
      case ImportAssignSubtypeComponentDetailLayoutTable._PAD_NAME:
        Assert.expect(false);
        break;

      case ImportAssignSubtypeComponentDetailLayoutTable._JOINT_TYPE_INDEX:
        Assert.expect(false);
        break;

      case ImportAssignSubtypeComponentDetailLayoutTable._SUBTYPE_INDEX:
        Assert.expect(false);
        break;

      case ImportAssignSubtypeComponentDetailLayoutTable._LIBRARY_JOINT_TYPE_INDEX:
        Assert.expect(false);
        break;

      case ImportAssignSubtypeComponentDetailLayoutTable._LIBRARY_SUBTYPE_INDEX:
        Assert.expect(false);
        break;

      default:
        Assert.expect(false, "Column index out of range: " + columnIndex);
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  public int getRowForDataStartingWith(String key)
  {
    key = key.toLowerCase();
    int index = -1;
    for (PadType padType : _componentPads)
    {
      index++;
      String padName = padType.getName().toLowerCase();
      if (padName.startsWith(key))
      {
        return index;
      }
    }
    return -1;
  }
}
