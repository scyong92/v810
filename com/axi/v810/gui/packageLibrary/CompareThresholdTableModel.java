package com.axi.v810.gui.packageLibrary;

import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.util.*;

/**
 * The model for all threshold tables
 * @author Wei Chin, Chong
 */
public class CompareThresholdTableModel extends AbstractTableModel
{
  private int _algorithmEnabledRow = 0;
  private int _standardRow = 1;
  private int _additionalRow = -1;
  private boolean _displaySliceThresholdsOnly = false;
  private Subtype _projectSubtype;
  private LibrarySubtype _librarySubtype;
  private InspectionFamilyEnum _inspectionFamilyEnum = null;
  private AlgorithmEnum _algorithmEnum = null;
  private List<List<Object>> _displayData = null; // a list of lists
  private final String[] _thresholdColumns = {StringLocalizer.keyToString("PLWK_CT_NAME_TABLE_HEADER_KEY"),
                                              StringLocalizer.keyToString("PLWK_CT_PROJECT_VALUE_TABLE_HEADER_KEY"),
                                              StringLocalizer.keyToString("PLWK_CT_LIBRARY_VALUE_TABLE_HEADER_KEY"),
                                              StringLocalizer.keyToString("PLWK_CT_DEFAULT_VALUE_TABLE_HEADER_KEY")};

  public static final int NAME_COLUMN = 0;
  public static final int PROJECT_VALUE_COLUMN = 1;
  public static final int LIBRARY_VALUE_COLUMN = 2;
  public static final int DEFAULT_COLUMN = 3;

  /**
   * @param projectSubtype Subtype
   * @param librarySubtype LibrarySubtype
   * @param algorithmEnum AlgorithmEnum
   * @param displaySliceThresholdsOnly boolean
   *
   * @author Wei Chin, Chong
   */
  public CompareThresholdTableModel(Subtype projectSubtype,
                                    LibrarySubtype librarySubtype,
                                    AlgorithmEnum algorithmEnum,
                                    boolean displaySliceThresholdsOnly)
  {
    Assert.expect(projectSubtype != null);
    Assert.expect(librarySubtype != null);

    _projectSubtype = projectSubtype;
    _librarySubtype = librarySubtype;
//    _inspectionFamilyEnum = _projectSubtype.getInspectionFamilyEnum();
    _inspectionFamilyEnum = InspectionFamily.getInspectionFamily(
        JointTypeEnum.getJointTypeEnum(
          _librarySubtype.getJointTypeName())).getInspectionFamilyEnum();
    _algorithmEnum = algorithmEnum;
    _displayData = new LinkedList<List<Object>>();
    _displaySliceThresholdsOnly = displaySliceThresholdsOnly;

    if (displaySliceThresholdsOnly)  // look only for SLICE_HEIGHT and SLICE_HEIGHT_ADDITIONAL
    {
      List<Object> standardThreshList = new LinkedList<Object>();
      standardThreshList.add(StringLocalizer.keyToString("PLWK_CT_STANDARD_TABLE_HEADER_KEY"));
      standardThreshList.add("");
      standardThreshList.add("");
      standardThreshList.add("");
      _displayData.add(standardThreshList);

      List<AlgorithmSetting> standardAlgorithmSettings = getSliceSetupThresholds(_librarySubtype,
                                                                                 _inspectionFamilyEnum,
                                                                                 true);
      populateDisplayData(standardAlgorithmSettings);

      _additionalRow = _displayData.size();

      List<Object> additionalThreshList = new LinkedList<Object>();
      additionalThreshList.add(StringLocalizer.keyToString("PLWK_CT_ADDITIONAL_TABLE_HEADER_KEY"));
      additionalThreshList.add("");
      additionalThreshList.add("");
      additionalThreshList.add("");
      _displayData.add(additionalThreshList);

      List<AlgorithmSetting> additionalAlgorithmSettings = getSliceSetupThresholds(_librarySubtype,
                                                                                   _inspectionFamilyEnum,
                                                                                   false);
      populateDisplayData(additionalAlgorithmSettings);
      
      updateTableModelRow();
    }
    else  // look only for STANDARD and ADDITIONAL thresholds
    {      
      // Display Algorithm Enable/Disable
      if (_algorithmEnum.equals(AlgorithmEnum.MEASUREMENT) == false)
      {
        String librarySubtypeEnableString = "False";
        if (librarySubtype.isLegacyUserGainEnum() || 
            librarySubtype.isLegacySignalCompensationEnum() || 
            librarySubtype.isLegacyArtifactCompensationEnum() ||
            librarySubtype.isLegacyDynamicRangeOptimizationLevelEnum() ||
            librarySubtype.isLegacyMagnificationTypeEnum())
        {
          librarySubtypeEnableString = "N/A";
        }
        else if (librarySubtype.isAlgorithmEnabled(algorithmEnum))
          librarySubtypeEnableString = "True";
        else
          librarySubtypeEnableString = "False";
        
        List<Object> algorithmEnableThresList = new LinkedList<Object>();
        algorithmEnableThresList.add(StringLocalizer.keyToString("PLWK_CT_ALGORITHM_ENABLED_KEY"));
        
        // XCR-3105 Assert when compare threshold during import library after change joint type
        if (projectSubtype.getAlgorithmEnums().contains(algorithmEnum))
          algorithmEnableThresList.add(projectSubtype.isAlgorithmEnabled(algorithmEnum) ? "True" : "False");
        else
          algorithmEnableThresList.add("");
        
        algorithmEnableThresList.add(librarySubtypeEnableString);
        algorithmEnableThresList.add("");
        _displayData.add(algorithmEnableThresList);
      }

      List<Object> standardThreshList = new LinkedList<Object>();
      standardThreshList.add(StringLocalizer.keyToString("PLWK_CT_STANDARD_TABLE_HEADER_KEY"));
      standardThreshList.add("");
      standardThreshList.add("");
      standardThreshList.add("");
      _displayData.add(standardThreshList);

      List<AlgorithmSetting> standardAlgorithmSettings = getThresholds(_librarySubtype,
                                                                       _inspectionFamilyEnum,
                                                                       _algorithmEnum,
                                                                       true);
      populateDisplayData(standardAlgorithmSettings);

      _additionalRow = _displayData.size();

      List<Object> additionalThreshList = new LinkedList<Object>();
      additionalThreshList.add(StringLocalizer.keyToString("PLWK_CT_ADDITIONAL_TABLE_HEADER_KEY"));
      additionalThreshList.add("");
      additionalThreshList.add("");
      additionalThreshList.add("");
      _displayData.add(additionalThreshList);

      List<AlgorithmSetting> additionalAlgorithmSettings = getThresholds(_librarySubtype,
                                                                         _inspectionFamilyEnum,
                                                                         _algorithmEnum,
                                                                         false);
      populateDisplayData(additionalAlgorithmSettings);
      
      updateTableModelRow();
    }
  }

  /**
   * @param algorithmSettings List
   *
   * @author Wei Chin, Chong
   */
  private void populateDisplayData(List<AlgorithmSetting> algorithmSettings)
  {
    Assert.expect(algorithmSettings != null);

    for (AlgorithmSetting algorithmSetting : algorithmSettings)
    {
      List<Object> threshList = new LinkedList<Object>();
      Serializable projectValue = null;

      if(_projectSubtype.getJointTypeEnum().getName().equals(_librarySubtype.getJointTypeName()) == false)
        projectValue = "";
      else
        projectValue = _projectSubtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum());

      if (projectValue instanceof Boolean)
      {
        Boolean bool = (Boolean)projectValue;
        if (bool.booleanValue())
          projectValue = new String("Yes");
        else
          projectValue = new String("No");
      }

      Serializable libraryValue = _librarySubtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum());
      if (libraryValue instanceof Boolean)
      {
        Boolean bool = (Boolean)libraryValue;
        if (bool.booleanValue())
          libraryValue = new String("Yes");
        else
          libraryValue = new String("No");
      }

      Object defaultValue = _librarySubtype.getAlgorithmSettingDefaultValue(algorithmSetting.getAlgorithmSettingEnum());
      if (defaultValue instanceof Boolean)
      {
        Boolean bool = (Boolean)defaultValue;
        if (bool.booleanValue())
          defaultValue = new String("Yes");
        else
          defaultValue = new String("No");
      }

      threshList.add(algorithmSetting);
      threshList.add(projectValue);
      threshList.add(libraryValue);
      threshList.add(defaultValue);
      _displayData.add(threshList);
    }
  }

  /**
   * Get the current column count -- from AbstractTableModel
   *
   * @return int ( The integer specifying the column count.)
   *
   * @author Wei Chin, Chong
   */
  public int getColumnCount()
  {
    return _thresholdColumns.length;
  }

  /**
   * Get the current column name -- from AbstractTableModel
   *
   * @param col int
   * @return String
   *
   * @author Wei Chin, Chong
   */
  public String getColumnName(int col)
  {
    Assert.expect(col >= 0 && col < getColumnCount());
    return (String)_thresholdColumns[col];
  }

 /**
  * Get the value at row/column -- from AbstractTableModel
  *
  * @param row int
  * @param col int
  * @return Object
  *
  * @author Wei Chin, Chong
  */
 public Object getValueAt(int row, int col)
 {
    List<Object> rowData = _displayData.get(row);
    Object data = null;
    data = rowData.get(col);

    // return original value if the data is empty
    if(data instanceof String)
    {
      String emptyString = (String)data;
      if(emptyString.length() <= 0)
        return data;
    }

    if (col > NAME_COLUMN &&
        row != getAdditionalRow() &&
        row != getStandardRow() &&
        row != getAlgorithmEnabledRow())
    {
      // check to see if we should be returning metric or imperial units.
      // we will convert to Mils if the units are millimeters
      AlgorithmSetting algorithmSetting = (AlgorithmSetting) rowData.get(NAME_COLUMN);
      MeasurementUnitsEnum algorithmSettingUnitsEnum = _librarySubtype.getAlgorithmSettingUnitsEnum(algorithmSetting.getAlgorithmSettingEnum());

      Pair<MeasurementUnitsEnum, Object> convertedValue = MeasurementUnitsEnum.convertToProjectUnitsIfNecessary(algorithmSettingUnitsEnum, data);
      algorithmSettingUnitsEnum = convertedValue.getFirst();
      data = convertedValue.getSecond();

      if (algorithmSettingUnitsEnum.equals(MeasurementUnitsEnum.MILS) && data instanceof Number)
      {
        data = MathUtil.roundToPlaces(((Number)data).floatValue(), MeasurementUnitsEnum.MILS.getNumberOfDecimalPlaces());
      }
      else if (algorithmSettingUnitsEnum.equals(MeasurementUnitsEnum.SQUARE_MILS) && data instanceof Number)
      {
        data = MathUtil.roundToPlaces(((Number)data).floatValue(), MeasurementUnitsEnum.SQUARE_MILS.getNumberOfDecimalPlaces());
      }
    }
    return data;
  }

  /**
   * @param value Object
   * @param row int
   * @param col int
   *
   * @author Wei Chin, Chong
   */
  public void setValueAt(Object value, int row, int col)
  {
    // do nothing
  }

  /**
   * @param row int
   * @param col int
   * @return boolean
   *
   * @author Wei Chin, Chong
   */
  public boolean isCellEditable(int row, int col)
  {
      return false;
  }

  /**
   * @return int
   *
   * @author Wei Chin, Chong
   */
  public int getRowCount()
  {
    if (_displayData == null)
      return 0;
    else
      return _displayData.size();
  }

  /**
   * get all Thresholds form the subtype except SliceSetup
   *
   * @param librarySubtype LibrarySubtype
   * @param inspectionFamilyEnum InspectionFamilyEnum
   * @param algoEnum AlgorithmEnum
   * @param standard boolean
   * @return List
   *
   * @author Wei Chin, Chong
   */
  private List<AlgorithmSetting> getThresholds(LibrarySubtype librarySubtype,
                                               InspectionFamilyEnum inspectionFamilyEnum,
                                               AlgorithmEnum algoEnum,
                                               boolean standard)
  {
    Assert.expect(librarySubtype != null);
    Assert.expect(inspectionFamilyEnum != null);
    Assert.expect(algoEnum != null);

    AlgorithmSettingTypeEnum typeEnumToDisplay;
    if (standard)
      typeEnumToDisplay = AlgorithmSettingTypeEnum.STANDARD;
    else
      typeEnumToDisplay = AlgorithmSettingTypeEnum.ADDITIONAL;

    Algorithm algorithm = InspectionFamily.getAlgorithm(inspectionFamilyEnum, algoEnum);

    List<AlgorithmSetting> settings = algorithm.getAlgorithmSettings(JointTypeEnum.getJointTypeEnum(librarySubtype.getJointTypeName()));

    // now, remove settings that are HIDDEN or don't match the incoming "standard" parameter
    Iterator<AlgorithmSetting> it = settings.iterator();
    while (it.hasNext())
    {
      AlgorithmSetting algoSetting = it.next();
      AlgorithmSettingTypeEnum typeEnum = librarySubtype.getAlgorithmSettingTypeEnum(algoSetting.getAlgorithmSettingEnum());
      if (typeEnum.equals(AlgorithmSettingTypeEnum.HIDDEN))
        it.remove();
      else if (!typeEnum.equals(typeEnumToDisplay))
        it.remove();
    }
    Collections.sort(settings, new AlgorithmSettingDisplayOrderComparator());
    return settings;
  }

  /**
   * get the SliceSetup Thresholds Only
   *
   * @param librarySubtype LibrarySubtype
   * @param inspectionFamilyEnum InspectionFamilyEnum
   * @param standard boolean
   * @return List
   *
   * @author Wei Chin, Chong
   */
  private List<AlgorithmSetting> getSliceSetupThresholds(LibrarySubtype librarySubtype,
                                                         InspectionFamilyEnum inspectionFamilyEnum,
                                                         boolean standard)
  {
    Assert.expect(librarySubtype != null);
    Assert.expect(inspectionFamilyEnum != null);

    List<Algorithm> algorithms = _librarySubtype.getAlgorithms();
    List<AlgorithmSetting> sliceRelatedSettings = new ArrayList<AlgorithmSetting>();

    for (Algorithm algorithm : algorithms)
    {
      List<AlgorithmSetting> settings = algorithm.getAlgorithmSettings(JointTypeEnum.getJointTypeEnum(librarySubtype.getJointTypeName()));

      for (AlgorithmSetting as : settings)
      {
        if (standard) // want standard settings
        {
          if (as.getAlgorithmSettingTypeEnum(algorithm.getVersion()).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT))
            sliceRelatedSettings.add(as);
        }
        else // want additional settings
        {
          if (as.getAlgorithmSettingTypeEnum(algorithm.getVersion()).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL))
            sliceRelatedSettings.add(as);
        }
      }
    }
    Collections.sort(sliceRelatedSettings, new AlgorithmSettingDisplayOrderComparator());
    return sliceRelatedSettings;
  }

  /**
   * return a List of AlgorithmEnums
   *
   * @param currentSubtype Subtype
   * @return List
   *
   * @author Wei Chin, Chong
   */
  List<Algorithm> getAlgorithms(Subtype currentSubtype)
  {
    Assert.expect(currentSubtype != null);

    return currentSubtype.getAlgorithms();
  }

  /**
   * return Additional Header Row Index
   *
   * @return int
   * @author Wei Chin, Chong
   */
  public int getAdditionalRow()
  {
    Assert.expect(_additionalRow >= 0);
    return _additionalRow;
  }

  /**
   * return Standard Header Row Index
   *
   * @return int
   *
   * @author Wei Chin, Chong
   */
  public int getStandardRow()
  {
    return _standardRow;
  }
  
  /**
   * return Algorithm-Enabled Row index
   * 
   * @return int
   * 
   * @author Cheah Lee Herng
   */
  public int getAlgorithmEnabledRow()
  {
    return _algorithmEnabledRow;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void updateTableModelRow()
  {
    if (_algorithmEnum.equals(AlgorithmEnum.MEASUREMENT) || _displaySliceThresholdsOnly)
    {
      _standardRow = 0;
      _algorithmEnabledRow = -1;      
    }
    else
    {
      _algorithmEnabledRow = 0;
      _standardRow = 1;      
    }
  }
}
