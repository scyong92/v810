package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellEditor;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class RenameSubtypeSchemeDialog extends JDialog
{
  private BorderLayout _mainBorderLayout;

  private JButton _doneButton = null;
  private JTable _subtypeTable = null;
  private JScrollPane _tablePanelLayoutScroll = null;

  private RenameSubtypeSchemeTableModel _renameSubtypeSchemeTableModel = null;

  private SubtypeScheme _subtypeScheme = null;

  /**
   * @param parentFrame Frame
   * @param modal boolean
   * @author Wei Chin, Chong
   */
  RenameSubtypeSchemeDialog(Frame parentFrame, boolean modal, SubtypeScheme subtypeScheme)
  {
    super(parentFrame, modal);
    _subtypeScheme = subtypeScheme;
    createPanel();
  }

  /**
   * @author Wei Chin, Chong
   */
  private void init()
  {
    _mainBorderLayout = new BorderLayout(5,10);

    setTitle(StringLocalizer.keyToString("PLWK_RENAME_SUBTYPE_KEY"));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

    _tablePanelLayoutScroll = new JScrollPane();
    _doneButton = new JButton(StringLocalizer.keyToString("PLWK_DONE_BUTTON_KEY"));
    _subtypeTable = new JTable();
    _renameSubtypeSchemeTableModel = new RenameSubtypeSchemeTableModel();
  }

  /**
   * @author Wei Chin, Chong
   */
  public void createPanel()
  {
    init();
    setLayout(_mainBorderLayout);

    _subtypeTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    _tablePanelLayoutScroll.getViewport().add(_subtypeTable, null);
    _tablePanelLayoutScroll.getViewport().setOpaque(false);
    _tablePanelLayoutScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

    populateSubtypeScheme(_subtypeScheme);
    _subtypeTable.setModel(_renameSubtypeSchemeTableModel);
    _tablePanelLayoutScroll.setPreferredSize(new Dimension(200,100));

    _subtypeTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        TableCellEditor tableCellEditor = _subtypeTable.getCellEditor();

        if (tableCellEditor != null)
          tableCellEditor.cancelCellEditing();
      }
    });

    _doneButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });

    add(_tablePanelLayoutScroll , BorderLayout.CENTER);
    add(_doneButton, BorderLayout.SOUTH);
    pack();
  }

  /**
   * @param subtypeScheme SubtypeScheme
   *
   * @author Wei Chin, Chong
   */
  public void populateSubtypeScheme(SubtypeScheme subtypeScheme)
  {
    Assert.expect(subtypeScheme != null);
    _subtypeScheme = subtypeScheme;
    _renameSubtypeSchemeTableModel.setSubtypeScheme(_subtypeScheme);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void unpopulate()
  {
    _subtypeScheme = null;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void dispose()
  {
    GuiObservable.getInstance().stateChanged(this, ScreenChangeEventEnum.PACKAGE_LIBRARY );
    unpopulate();
    super.dispose();
  }
}
