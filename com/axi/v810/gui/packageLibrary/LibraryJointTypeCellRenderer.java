package com.axi.v810.gui.packageLibrary;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.v810.business.packageLibrary.*;

public class LibraryJointTypeCellRenderer extends DefaultTableCellRenderer
{
  /**
   * @author Tan Hock Zoon
   */
  public LibraryJointTypeCellRenderer()
  {
    super();
  }

  /**
   * Returns the component used for drawing the cell.  This method is
   * used to configure the renderer appropriately before drawing.
   * see TableCellRenderer.getTableCellRendererComponent(...); for more comments on the method
   *
   * @author Tan Hock Zoon
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    ImportRefinePackageLayoutTable refinePackageTable = (ImportRefinePackageLayoutTable) table;
    ImportRefinePackageLayoutTableModel refinePackageTableModel = (ImportRefinePackageLayoutTableModel) refinePackageTable.getModel();

    SubtypeScheme subtypeScheme = refinePackageTableModel.getSubtypeSchemeAt(row);

    if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false
        && subtypeScheme.getLibraryPackageList().size() > 0
        && subtypeScheme.getSelectedLibraryPackage().getJointTypeList().size() > 1)
    {
      JComboBox jComboBox = new JComboBox();

      jComboBox.setBorder(null);
      jComboBox.removeAllItems();
      jComboBox.addItem(value);

      return jComboBox;
    }
    else
    {
      return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }
  }

}
