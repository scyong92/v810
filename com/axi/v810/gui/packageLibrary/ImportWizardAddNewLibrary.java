package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.database.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: ImportWizardAddNewLibrary</p>
 *
 * <p>Description: This dialog allow users to add new Library path to current wizard.</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Tan Hock Zoon
 * @version 1.0
 */
public class ImportWizardAddNewLibrary extends EscapeDialog
{
  private JTextField _libraryPathTextField;
  private JButton _browseButton;
  private JButton _okButton;
  private JButton _cancelButton;
  private JFileChooser _libraryPathChooser;

  private JPanel _libraryPathPanel;
  private JLabel _libraryPathLabel;
  private JPanel _buttonPanel;
  private DefaultListModel _libraryListData;
  private JPanel _addNewLibraryDescPanel;
  private JLabel _addNewLibraryDescLabel;
  private LibraryManager _libraryManager = LibraryManager.getInstance();
  private DatabaseManager _databaseManager = DatabaseManager.getInstance();
  private java.util.List<String> _libraryFullPathList;

  /**
   *
   * @param frame Frame
   * @param libraryListData DefaultListModel
   * @param libraryFullPathList List
   *
   * @author Tan Hock Zoon
   */
  public ImportWizardAddNewLibrary(Frame frame, DefaultListModel libraryListData, java.util.List<String> libraryFullPathList)
  {
    super(frame, true);

    Assert.expect(libraryListData != null);
    Assert.expect(libraryFullPathList != null);

    _libraryListData = libraryListData;
    _libraryFullPathList = libraryFullPathList;
    init();
    createPanel();
  }

  /**
   * @author Tan Hock Zoon
   */
  private void init()
  {
    _libraryPathTextField = new JTextField(30)
    {
      protected void processKeyEvent(KeyEvent e)
      {
        super.processKeyEvent(e);
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
          unpopulate();
        }
      }

    };

    _browseButton = new JButton(StringLocalizer.keyToString("PLWK_BROWSE_BUTTON_KEY"))
    {
      protected void processKeyEvent(KeyEvent e)
      {
        super.processKeyEvent(e);
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
          unpopulate();
        }
      }

    };

    _okButton = new JButton(StringLocalizer.keyToString("PLWK_OK_BUTTON_KEY"))
    {
      protected void processKeyEvent(KeyEvent e)
      {
        super.processKeyEvent(e);
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
          unpopulate();
        }
      }

    };

    _cancelButton = new JButton(StringLocalizer.keyToString("PLWK_CANCEL_BUTTON_KEY"))
    {
      protected void processKeyEvent(KeyEvent e)
      {
        super.processKeyEvent(e);
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
          unpopulate();
        }
      }

    };

    _libraryPathChooser = new JFileChooser();
    _libraryPathPanel = new JPanel();
    _libraryPathLabel = new JLabel(StringLocalizer.keyToString("PLWK_LIBRARY_PATH_KEY"));
    _buttonPanel = new JPanel();
    _addNewLibraryDescPanel = new JPanel();
    _addNewLibraryDescLabel = new JLabel(StringLocalizer.keyToString("PLWK_IMPORT_ADD_NEW_LIBRARY_DESC_KEY"));
  }

  /**
   * @author Tan Hock Zoon
   */
  private void createPanel()
  {
    setTitle(StringLocalizer.keyToString("PLWK_ADD_NEW_LIBRARY_KEY"));
    setLayout(new BorderLayout(5, 5));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

    _libraryPathPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

    _libraryPathPanel.add(_libraryPathLabel);
    _libraryPathPanel.add(_libraryPathTextField);
    _libraryPathPanel.add(_browseButton);

    _buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

    _buttonPanel.add(_okButton);
    _buttonPanel.add(_cancelButton);

    _addNewLibraryDescPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    _addNewLibraryDescPanel.setBackground(Color.WHITE);
    _addNewLibraryDescPanel.add(_addNewLibraryDescLabel);

    add(_addNewLibraryDescPanel, BorderLayout.NORTH);
    add(_libraryPathPanel, BorderLayout.CENTER);
    add(_buttonPanel, BorderLayout.SOUTH);

    _browseButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        browseButton_actionPerformed();
      }
    });

    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        String libraryPath = _libraryPathTextField.getText();

        if (libraryPath.equals(""))
        {
          MessageDialog.showErrorDialog(null,
                                        StringLocalizer.keyToString("PLWK_LIBRARY_PATH_EMPTY_KEY"),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                        true);
        }
        else
        {
          libraryPath = FileUtil.getAbsolutePath(libraryPath);

          try
          {
            //check if the library path provide exist and accessible
            if (_databaseManager.doesDatabaseExists(libraryPath.trim()))
            {

              if (_libraryFullPathList.contains(libraryPath) == false)
              {
                _libraryManager.add(libraryPath);
                _libraryListData.addElement(libraryPath.substring(libraryPath.lastIndexOf(File.separator) + 1, libraryPath.length()));
                _libraryFullPathList.add(libraryPath);
                unpopulate();
              }
              else
              {
                MessageDialog.showErrorDialog(null,
                                              StringLocalizer.keyToString("PLWK_LIBRARY_EXISTS_KEY"),
                                              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                              true);
              }
            }
            else
            {
              MessageDialog.showErrorDialog(null,
                                            StringLocalizer.keyToString("PLWK_EXPORT_INVALID_LIBRARY_PATH_WARNING_KEY"),
                                            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                            true);
            }
          }
          catch (DatastoreException ex)
          {
            // wei chin: it might be an database shutdown exception when calling
            // the function _databaseManager.doesDatabaseExists(libraryPath.trim()
            // this error messages is specific for writing library to the file exception
            /** @todo write a proper message to handle this exception */
            MessageDialog.showErrorDialog(null,
                                          StringLocalizer.keyToString("PLWK_ERROR_WRITING_PATH_TO_FILE_KEY"),
                                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                          true);
          }

        }
      }
    });

    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        unpopulate();
      }
    });
  }

  /**
   * @author Tan Hock Zoon
   */
  private void browseButton_actionPerformed()
  {
    String dirName = null;

    File currentDir = null;

    javax.swing.filechooser.FileFilter fileFilter = _libraryPathChooser.getAcceptAllFileFilter();
    _libraryPathChooser.removeChoosableFileFilter(fileFilter);

    _libraryPathChooser.setFileHidingEnabled(false);
    _libraryPathChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

    if (_libraryPathTextField.getText().equals("") == false)
    {
      currentDir = new File(_libraryPathTextField.getText());
    }
    else
    {
      currentDir = new File(Directory.getPackageLibraryDir());
    }

    _libraryPathChooser.setCurrentDirectory(currentDir);

    if ( _libraryPathChooser.showDialog(this,StringLocalizer.keyToString("GUI_SELECT_FOLDER_KEY")) == JFileChooser.APPROVE_OPTION )
    {
      File selectedFile = _libraryPathChooser.getSelectedFile();

      if (selectedFile.exists() == true)
      {
        if (selectedFile.isDirectory() == true)
        {
          dirName = selectedFile.getPath();
        }
      }

      _libraryPathTextField.setText(dirName);
    }
  }

  /**
   * @author Tan Hock Zoon
   */
  public void unpopulate()
  {
    _databaseManager = null;
    _libraryListData = null;
    _libraryManager = null;
    _libraryFullPathList = null;
    super.dispose();
  }
}
