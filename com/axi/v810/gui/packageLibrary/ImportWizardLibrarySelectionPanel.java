package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.database.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: ImportWizardLibrarySelectionPanel</p>
 *
 * <p>Description: This panel is to display a list of Libraries and the selection made.</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Tan Hock Zoon
 * @version 1.0
 */
public class ImportWizardLibrarySelectionPanel extends WizardPanel
{
  private JButton _moveLibraryDownButton;
  private JButton _moveLibraryUpButton;
  private JButton _unselectSingleLibraryButton;
  private JButton _unselectAllLibraryButton;
  private JButton _selectSingleLibraryButton;
  private JButton _selectAllLibraryButton;
  private JButton _deleteLibraryButton;
  private JButton _addLibraryButton;
  private JList _libraryToUseList;
  private JList _libraryList;
  private DefaultListModel _libraryListData = new DefaultListModel();
  private DefaultListModel _libraryToUseListData = new DefaultListModel();
  private JScrollPane _librariesListScroll;
  private JScrollPane _librariesToUseListScroll;
  private JLabel _allLibrariesLabel;
  private JLabel _librariesToUseLabel;
  private JLabel _librarySelectionDescLabel;
  private JPanel _mainPanel;
  private JPanel _contentPanel;
  private JPanel _allLibrariesPanel;
  private JPanel _addRemoveButtonPanel;
  private JPanel _librariesToUsePanel;
  private JPanel _selectionPanel;
  private JPanel _moveUpDownPanel;
  private JPanel _librarySelectionDescPanel;

  private ImportWizardAddNewLibrary _addNewLibraryDialog = null;
  private LibraryManager _libraryManager = LibraryManager.getInstance();
  private DatabaseManager _databaseManager = DatabaseManager.getInstance();
  private java.util.List<String> _librariesFullPathList = new ArrayList<String>();
  private java.util.List<String> _selectedfLibrariesFullPathList;

  /**
   *
   * @param dialog WizardDialog
   * @param selectedfLibrariesFullPathList List
   *
   * @author Tan Hock Zoon
   */
  public ImportWizardLibrarySelectionPanel(WizardDialog dialog, java.util.List<String> selectedfLibrariesFullPathList)
  {
    super(dialog);

    Assert.expect(selectedfLibrariesFullPathList != null);

    _selectedfLibrariesFullPathList = selectedfLibrariesFullPathList;
    populateLibraryListData();
  }  

  /**
   * @author Tan Hock Zoon
   */
  private void init()
  {
    setLayout(new BorderLayout(5, 5));

    _addLibraryButton = new JButton(StringLocalizer.keyToString("ATGUI_ADD_KEY"));
    _deleteLibraryButton = new JButton(StringLocalizer.keyToString("ATGUI_REMOVE_KEY"));
    _moveLibraryDownButton = new JButton(StringLocalizer.keyToString("PLWK_MOVE_DOWN_KEY"));
    _moveLibraryUpButton = new JButton(StringLocalizer.keyToString("PLWK_MOVE_UP_KEY"));
    _unselectAllLibraryButton = new JButton(StringLocalizer.keyToString("PLWK_REMOVE_ALL_KEY"));
    _unselectSingleLibraryButton = new JButton(StringLocalizer.keyToString("PLWK_REMOVE_SINGLE_SELECTION_KEY"));
    _selectAllLibraryButton = new JButton(StringLocalizer.keyToString("PLWK_ADD_ALL_KEY"));
    _selectSingleLibraryButton = new JButton(StringLocalizer.keyToString("PLWK_ADD_SINGLE_SELECTION_KEY"));

    _libraryList = new JList(_libraryListData)
    {
      public String getToolTipText(MouseEvent e)
      {
        int row = locationToIndex(e.getPoint());

        if (row >= 0)
        {
          return _librariesFullPathList.get(row);
        }
        else
        {
          return null;
        }
      }
    };

    _libraryToUseList = new JList(_libraryToUseListData)
    {
      public String getToolTipText(MouseEvent e)
      {
        int row = locationToIndex(e.getPoint());

        if (row >= 0)
        {
          return _selectedfLibrariesFullPathList.get(row);
        }
        else
        {
          return null;
        }
      }
    };

    _librariesListScroll = new JScrollPane(_libraryList);
    _librariesToUseListScroll = new JScrollPane(_libraryToUseList);

    _allLibrariesLabel = new JLabel(StringLocalizer.keyToString("PLWK_ALL_LIBRARIES_KEY"));
    _librariesToUseLabel = new JLabel(StringLocalizer.keyToString("PLWK_LIBRARIES_TO_USE_KEY"));
    _librarySelectionDescLabel = new JLabel(StringLocalizer.keyToString("PLWK_IMPORT_SELECT_LIBRARY_DESC_KEY"));

    _mainPanel = new JPanel(new BorderLayout(5, 5));
    _mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
    _contentPanel = new JPanel(new GridLayout(1, 2, 5, 5));

    _librarySelectionDescPanel = new JPanel(new BorderLayout(5, 5));
    _librarySelectionDescPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

    _allLibrariesPanel = new JPanel(new BorderLayout(5, 5));
    _librariesToUsePanel = new JPanel(new BorderLayout(5, 5));

    _addRemoveButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

    _selectionPanel = new JPanel();
    _moveUpDownPanel = new JPanel();

    JPanel[] panels = new JPanel[]{_allLibrariesPanel, _librariesToUsePanel};
    SwingUtils.makeAllSameLength(panels);

    JButton[] buttons = new JButton[]{_addLibraryButton, _deleteLibraryButton};
    SwingUtils.makeAllSameLength(buttons);
  }

  /**
   * @author Tan Hock Zoon
   */
  private void createPanel()
  {
    removeAll();
    init();

    _libraryList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _libraryList.setLayoutOrientation(JList.VERTICAL);

    _libraryToUseList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _libraryToUseList.setLayoutOrientation(JList.VERTICAL);

    _libraryList.setVisibleRowCount(20);
    _libraryToUseList.setVisibleRowCount(20);

    _addRemoveButtonPanel.add(_addLibraryButton);
    _addRemoveButtonPanel.add(_deleteLibraryButton);

    //set all button equal size
    _selectSingleLibraryButton.setMaximumSize(_selectAllLibraryButton.getMaximumSize());
    _unselectAllLibraryButton.setMaximumSize(_selectAllLibraryButton.getMaximumSize());
    _unselectSingleLibraryButton.setMaximumSize(_selectAllLibraryButton.getMaximumSize());
    _moveLibraryUpButton.setMaximumSize(_moveLibraryDownButton.getMaximumSize());

    _selectionPanel.setLayout(new BoxLayout(_selectionPanel, BoxLayout.PAGE_AXIS));
    _selectionPanel.add(Box.createVerticalGlue());
    _selectionPanel.add(_selectAllLibraryButton);
    _selectionPanel.add(_selectSingleLibraryButton);
    _selectionPanel.add(_unselectAllLibraryButton);
    _selectionPanel.add(_unselectSingleLibraryButton);
    _selectionPanel.add(Box.createVerticalGlue());

    _moveUpDownPanel.setLayout(new BoxLayout(_moveUpDownPanel, BoxLayout.Y_AXIS));
    _moveUpDownPanel.add(Box.createVerticalGlue());
    _moveUpDownPanel.add(_moveLibraryUpButton);
    _moveUpDownPanel.add(_moveLibraryDownButton);
    _moveUpDownPanel.add(Box.createVerticalGlue());

    _allLibrariesPanel.add(_allLibrariesLabel, BorderLayout.NORTH);
    _allLibrariesPanel.add(_librariesListScroll, BorderLayout.CENTER);
    _allLibrariesPanel.add(_selectionPanel, BorderLayout.EAST);

    _librariesToUsePanel.add(_librariesToUseLabel, BorderLayout.NORTH);
    _librariesToUsePanel.add(_librariesToUseListScroll, BorderLayout.CENTER);
    _librariesToUsePanel.add(_moveUpDownPanel, BorderLayout.EAST);

    _librarySelectionDescPanel.setBackground(Color.WHITE);
    _librarySelectionDescPanel.add(_librarySelectionDescLabel, BorderLayout.CENTER);

    _contentPanel.add(_allLibrariesPanel);
    _contentPanel.add(_librariesToUsePanel);

    _mainPanel.add(_librarySelectionDescPanel, BorderLayout.NORTH);
    _mainPanel.add(_contentPanel, BorderLayout.CENTER);
    _mainPanel.add(_addRemoveButtonPanel, BorderLayout.SOUTH);

    add(_mainPanel, BorderLayout.CENTER);

    _addLibraryButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        addLibraryButton_actionPerformed();
      }
    });

    _deleteLibraryButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        deleteLibraryButton_actionPerformed();
      }
    });

    _moveLibraryDownButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        if (_libraryToUseList.getSelectedIndex() != -1)
        {
          moveLibraryButton_actionPerformed(false);
        }
      }

    });

    _moveLibraryUpButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        if (_libraryToUseList.getSelectedIndex() != -1)
        {
          moveLibraryButton_actionPerformed(true);
        }
      }

    });

    _unselectAllLibraryButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        unselectAllLibraryButton_actionPerformed();
        validatePanel();
      }
    });

    _unselectSingleLibraryButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        unselectSingleLibraryButton_actionPerformed();
        validatePanel();
      }
    });

    _selectAllLibraryButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        selectAllLibraryButton_actionPerformed();
        validatePanel();
      }
    });

    _selectSingleLibraryButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        selectSingleLibraryButton_actionPerformed();
        validatePanel();
      }
    });
  }

  /**
   * @author Tan Hock Zoon
   */
  public void validatePanel()
  {
    WizardButtonState buttonState = null;

    if (_libraryToUseListData.isEmpty())
    {
      buttonState = new WizardButtonState(false, true, false, true);
    }
    else
    {
      buttonState = new WizardButtonState(true, true, false, true);
    }

    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Tan Hock Zoon
   */
  public void populatePanelWithData()
  {
    createPanel();

    _dialog.setTitle(StringLocalizer.keyToString("PLWK_LIBRARIES_SELECTION_KEY"));

    validatePanel();
  }

  /**
   * @author Tan Hock Zoon
   */
  public void unpopulate()
  {
    if (_libraryListData != null)
    {
      _libraryListData.clear();
    }

    if (_libraryToUseListData != null)
    {
      _libraryToUseListData.clear();
    }

    if (_librariesFullPathList != null)
    {
      _librariesFullPathList.clear();
    }

    if (_selectedfLibrariesFullPathList != null)
    {
      _selectedfLibrariesFullPathList.clear();
    }

    _databaseManager = null;
    _libraryListData = null;
    _libraryToUseListData = null;
    _librariesFullPathList = null;
    _selectedfLibrariesFullPathList = null;
    _addNewLibraryDialog = null;
    _libraryManager = null;

    removeAll();
  }

  /**
   *
   * @param name String
   *
   * @author Tan Hock Zoon
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Tan Hock Zoon
   */
  public String getWizardName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author Tan Hock Zoon
   */
  public void removeObservers()
  {
    /** @todo APM code review Why is there here if it's empty? */
    //do nothing since this is carry forward from WizardPanel class
  }

  /**
   * @author Tan Hock Zoon
   */
  private void populateLibraryListData()
  {
    _librariesFullPathList.clear();
    _selectedfLibrariesFullPathList.clear();
    _libraryListData.removeAllElements();
    _libraryToUseListData.removeAllElements();

    //this will populate all libraries current used and is in alphabetical order
    for (String libraryPath : _libraryManager.getLibraries())
    {
      if (FileUtil.existsDirectory(libraryPath))
      {
        _librariesFullPathList.add(libraryPath);
        _libraryListData.addElement(libraryPath.substring(libraryPath.lastIndexOf(File.separator) + 1, libraryPath.length()));
      }
    }

    //this will populate selected libraries and is according to the user selection without alphabetical order
    for (String libraryPath : _libraryManager.getSelectedLibraries())
    {
      if (FileUtil.existsDirectory(libraryPath))
      {
        _selectedfLibrariesFullPathList.add(libraryPath);
        _libraryToUseListData.addElement(libraryPath.substring(libraryPath.lastIndexOf(File.separator) + 1, libraryPath.length()));
      }
    }
  }

  /**
   *
   * @author Tan Hock Zoon
   */
  private void addLibraryButton_actionPerformed()
  {
    _addNewLibraryDialog = new ImportWizardAddNewLibrary((Frame)_dialog.getParent(), _libraryListData, _librariesFullPathList);
    _addNewLibraryDialog.pack();
    SwingUtils.centerOnComponent(_addNewLibraryDialog, this);
    _addNewLibraryDialog.setVisible(true);
    _addNewLibraryDialog.dispose();
  }

  /**
   *
   * @author Tan Hock Zoon
   */
  private void deleteLibraryButton_actionPerformed()
  {
    int[] indices = _libraryList.getSelectedIndices();

    String libraryPath = null;

    for (int i = (indices.length - 1); i >= 0; i--)
    {
      try
      {
        libraryPath = _librariesFullPathList.get(indices[i]);
        _libraryManager.remove(libraryPath);
        _libraryListData.removeElementAt(indices[i]);
        _librariesFullPathList.remove(indices[i]);

        if (_selectedfLibrariesFullPathList.contains(libraryPath))
        {
          _libraryToUseListData.remove(_selectedfLibrariesFullPathList.indexOf(libraryPath));
          _selectedfLibrariesFullPathList.remove(libraryPath);
        }
      }
      catch (DatastoreException de)
      {
        MessageDialog.showErrorDialog(null,
                                      StringLocalizer.keyToString("PLWK_ERROR_REMOVING_PATH_TO_FILE_KEY"),
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);

      }
    }
  }

  /**
   *
   * @author Tan Hock Zoon
   */
  private void selectAllLibraryButton_actionPerformed()
  {
    String invalidLibraryPath = "";

    for (String libraryPath : _librariesFullPathList)
    {
      if (_selectedfLibrariesFullPathList.contains(libraryPath) == false)
      {
        try
        {
          //check if database exist and is accessible
          if (_databaseManager.doesDatabaseExists(libraryPath))
          {
            _selectedfLibrariesFullPathList.add(libraryPath);
            _libraryToUseListData.addElement(libraryPath.substring(libraryPath.lastIndexOf("\\") + 1, libraryPath.length()));
          }
          else
          {
            invalidLibraryPath = invalidLibraryPath + libraryPath + "\n";
          }
        }
        catch(DatastoreException dex)
        {
          // failed to shutdown the connection
          /** @todo write a proper message to handle this exception */
        }
      }
    }

    if (invalidLibraryPath.length() > 0)
    {
      MessageDialog.showInformationDialog(null,
                                          StringLocalizer.keyToString(new LocalizedString("PLWK_INVALID_LIBRARY_PATH_ACCESS_KEY",
                                                                       new Object[]{invalidLibraryPath})),
                                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                          true);
    }
    else
    {
      invalidLibraryPath = null;
    }
  }

  /**
   *
   * @author Tan Hock Zoon
   */
  private void selectSingleLibraryButton_actionPerformed()
  {
    String invalidLibraryPath = "";
    String libraryPath = "";

    for (int index : _libraryList.getSelectedIndices())
    {
      libraryPath = _librariesFullPathList.get(index);

      if (_selectedfLibrariesFullPathList.contains(libraryPath) == false)
      {
        try
        {
          //check if database exist and is accessible
          if (_databaseManager.doesDatabaseExists(libraryPath))
          {
            _selectedfLibrariesFullPathList.add(libraryPath);
            _libraryToUseListData.addElement(libraryPath.substring(libraryPath.lastIndexOf("\\") + 1, libraryPath.length()));
          }
          else
          {
            invalidLibraryPath = invalidLibraryPath + libraryPath + "\n";
          }
        }
        catch(DatastoreException dex)
        {
          // failed to shutdown connection
          /** @todo write a proper message to handle this exception */
        }
      }
    }

    if (invalidLibraryPath.length() > 0)
    {
      MessageDialog.showInformationDialog(null,
                                          StringLocalizer.keyToString(new LocalizedString("PLWK_INVALID_LIBRARY_PATH_ACCESS_KEY",
                                                                       new Object[]{invalidLibraryPath})),
                                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                          true);
    }
    else
    {
      invalidLibraryPath = null;
      libraryPath = null;
    }
  }

  /**
   *
   * @author Tan Hock Zoon
   */
  private void unselectAllLibraryButton_actionPerformed()
  {
    _libraryToUseListData.removeAllElements();
    _selectedfLibrariesFullPathList.clear();
  }

  /**
   *
   * @author Tan Hock Zoon
   */
  private void unselectSingleLibraryButton_actionPerformed()
  {
    for (int index : _libraryToUseList.getSelectedIndices())
    {
      _libraryToUseListData.removeElementAt(index);
      _selectedfLibrariesFullPathList.remove(index);
    }
  }

  /**
   *
   * @param moveUp boolean
   *
   * @author Tan Hock Zoon
   */
  private void moveLibraryButton_actionPerformed(boolean moveUp)
  {
    int[] indices = _libraryToUseList.getSelectedIndices();
    int[] newIndices = indices;
    int count = 0;

    if (moveUp)
    {
      if (indices[0] != 0)
      {
        for (int index : indices)
        {
          swap(index, index - 1);
          newIndices[count] = index - 1;
          count++;
        }
      }
    }
    else
    {
      if (indices[(indices.length - 1)] != (_libraryToUseListData.size() - 1))
      {
        count = indices.length - 1;

        for (int i = indices.length; i > 0; i--)
        {
          int index = indices[i - 1];

          swap(index, index + 1);
          newIndices[count] = index + 1;
          count--;
        }
      }
    }

    _libraryToUseList.setSelectedIndices(newIndices);
  }

  /**
   *
   * @param currentIndex int
   * @param nextIndex int
   *
   * @author Tan Hock Zoon
   */
  private void swap(int currentIndex, int nextIndex)
  {
    Assert.expect(currentIndex >= 0);
    Assert.expect(nextIndex >= 0);

    Object currentObj = _libraryToUseListData.getElementAt(currentIndex);
    Object nextObj = _libraryToUseListData.getElementAt(nextIndex);

    String currentString = _selectedfLibrariesFullPathList.get(currentIndex);
    String nextString = _selectedfLibrariesFullPathList.get(nextIndex);

    _libraryToUseListData.setElementAt(nextObj, currentIndex);
    _libraryToUseListData.setElementAt(currentObj, nextIndex);

    _selectedfLibrariesFullPathList.set(currentIndex, nextString);
    _selectedfLibrariesFullPathList.set(nextIndex, currentString);
  }
}
