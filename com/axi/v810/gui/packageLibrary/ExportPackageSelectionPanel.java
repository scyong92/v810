package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.v810.gui.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.util.*;
import com.axi.util.*;

/** @todo APM Code Review I noticed when doing a sort on the table in this panel, that the Row Selection was lost after
  * the sort -- I don't remember where the code to do that goes, so I'm putting my code review tag here before I forget. */

/**
 * <p>Company: Agilent Technogies</p>
 *
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class ExportPackageSelectionPanel extends WizardPanel implements Observer
{
  private BorderLayout _mainPanelBorderLayout;

  private ButtonGroup _packagesButtonGroup;
  private JButton _selectAllButton;
  private JButton _unselectAllButton;
  private JButton _createNewLibraryButton;
  private JButton _assignToAllEmptyLibraryButton;
  private JButton _assignToAllLibraryButton;

  private JRadioButton _allPackagesButton;
  private JRadioButton _newModifiedPackagesButton;
  private JRadioButton _importedPackageButton;

  private ExportWizardNewLibraryDialog _newLibraryDialog = null;

  private JLabel _descLabel;
  private JLabel _packagesLabel;

  private DrawCadPanel _drawLandPatternPanel;
  private JCheckBox _toggleCadDisplayCheckBox = new JCheckBox(StringLocalizer.keyToString("PLWK_TOGGLE_CAD_DISPLAY_KEY"));
  private JPanel _northPanel;
  private JPanel _centerPanel;
  private JPanel _centerNorthPanel;
  private JPanel _centerSouthPanel;
  private JScrollPane _tablePanelLayoutScroll = null;
  private JSplitPane _splitPane = null;
  private JPanel _drawCadPanel;
  private TitledBorder _drawCadTitleBorder;

  private ExportPackageLayoutTable _packageLayoutTable = null;
  private ExportPackageLayoutTableModel _packageLayoutTableModel = null;

  protected String _name;

  private Project _project = null;
  private static ProjectObservable _projectObservable;
  private static GuiObservable _guiObservable;
  private Set<CompPackage> _selectedPackages = null;
  private LibraryManager _libraryManager;

  private boolean _isFirstTime = true;

  private boolean _allPackagesSelected = false;
  private boolean _newModifiedPackagesSelected = true;
  private boolean _importedPackageSelected = false;
    
  private LibraryWizardGeneralPersistance _libraryWizardGeneralPersistance;

  /**
   * @param dialog WizardDialog
   * @param project Project
   * @param libraryWizardGeneralPersistance
   *
   * @author Wei Chin, Chong
   */
  public ExportPackageSelectionPanel(WizardDialog dialog, Project project, LibraryWizardGeneralPersistance libraryWizardGeneralPersistance)
  {
    super(dialog);

    Assert.expect(project != null);
    Assert.expect(libraryWizardGeneralPersistance != null);

    _project = project;
    _projectObservable = ProjectObservable.getInstance();
    _guiObservable = GuiObservable.getInstance();
    _isFirstTime = true;
    _allPackagesSelected = false;
    _newModifiedPackagesSelected = true;
    _importedPackageSelected = false;
    
    _libraryWizardGeneralPersistance = libraryWizardGeneralPersistance;
  }

  /**
   * @param dialog WizardDialog
   * @param project Project
   * @param selectedPackages Set
   *
   * @author Wei Chin, Chong
   */
  public ExportPackageSelectionPanel(WizardDialog dialog, Project project, Set<CompPackage> selectedPackages, LibraryWizardGeneralPersistance libraryWizardGeneralPersistance)
  {
     this(dialog, project, libraryWizardGeneralPersistance);

     Assert.expect(selectedPackages != null);

    _selectedPackages = selectedPackages;
    _isFirstTime = true;
  }

  /**
   * @author Wei Chin, Chong
   */
  private void init()
  {
    _projectObservable.addObserver(this);
    _guiObservable.addObserver(this);

     _mainPanelBorderLayout = new BorderLayout(5, 5);

    _descLabel = new JLabel(StringLocalizer.keyToString("PLWK_EXPORT_SELECT_PACKAGE_DESCRIPTION_KEY"));
    _packagesLabel = new JLabel(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY"));

    _packagesButtonGroup = new ButtonGroup();
    _selectAllButton = new JButton(StringLocalizer.keyToString("PLWK_EXPORT_SELECT_ALL_BUTTON_KEY"));
    _unselectAllButton = new JButton(StringLocalizer.keyToString("PLWK_UNSELECT_ALL_KEY"));
    _createNewLibraryButton = new JButton(StringLocalizer.keyToString("PLWK_EXPORT_CREATE_NEW_LIBRARY_BUTTON_KEY"));
    _assignToAllEmptyLibraryButton = new JButton(StringLocalizer.keyToString("PLWK_EXPORT_ASSIGN_EMPTY_LIBRARY_BUTTON_KEY"));
    _assignToAllLibraryButton = new JButton(StringLocalizer.keyToString("PLWK_EXPORT_ASSIGN_ALL_LIBRARY_BUTTON_KEY"));

    _allPackagesButton = new JRadioButton(StringLocalizer.keyToString("PLWK_EXPORT_ALL_RADIO_BUTTON_KEY"));
    _newModifiedPackagesButton = new JRadioButton(StringLocalizer.keyToString("PLWK_EXPORT_NEW_MODIFIED_RADIO_BUTTON_KEY"));
    _importedPackageButton = new JRadioButton(StringLocalizer.keyToString("PLWK_EXPORT_IMPORTED_RADIO_BUTTON_KEY"));

    _drawLandPatternPanel = new DrawCadPanel(300, 400);
    _northPanel = new JPanel(new BorderLayout(5, 5));
    _centerPanel = new JPanel(new BorderLayout(5, 5));
    _centerNorthPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
    _centerSouthPanel = new JPanel();
    _tablePanelLayoutScroll = new JScrollPane();

    if(_selectedPackages == null)
      _selectedPackages = new HashSet<CompPackage>();

    _libraryManager = LibraryManager.getInstance();
    _packageLayoutTableModel = new ExportPackageLayoutTableModel(_selectedPackages);
    _packageLayoutTable = new ExportPackageLayoutTable(_packageLayoutTableModel,_libraryManager);

    _drawCadTitleBorder = BorderFactory.createTitledBorder(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY"));
    _drawCadPanel = new JPanel(new BorderLayout());
    _drawCadPanel.setBorder(_drawCadTitleBorder);

    _splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, _centerPanel, _drawCadPanel);

    _splitPane.setContinuousLayout(true);
    _splitPane.setResizeWeight(1);
  }

  /**
   * @author Wei Chin, Chong
   */
  private void createPanel()
  {
    removeAll();
    init();

    _unselectAllButton.setEnabled(false);
    _selectAllButton.setEnabled(false);

    setLayout(_mainPanelBorderLayout);
    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    _northPanel.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));
    _northPanel.setBackground(Color.WHITE);
    _northPanel.add(_descLabel, BorderLayout.CENTER);

    assignRadioButtonState();

    _packagesButtonGroup.add(_allPackagesButton);
    _packagesButtonGroup.add(_newModifiedPackagesButton);
    _packagesButtonGroup.add(_importedPackageButton);

    _centerNorthPanel.add(_packagesLabel);
    _centerNorthPanel.add(_allPackagesButton);
    _centerNorthPanel.add(_newModifiedPackagesButton);
    _centerNorthPanel.add(_importedPackageButton);

    _centerSouthPanel.setLayout(new BoxLayout(_centerSouthPanel, BoxLayout.LINE_AXIS));
    _centerSouthPanel.add(_selectAllButton);
    _centerSouthPanel.add(Box.createRigidArea(new Dimension(5, 0)));
    _centerSouthPanel.add(_unselectAllButton);
    _centerSouthPanel.add(Box.createHorizontalGlue());
    _centerSouthPanel.add(_assignToAllEmptyLibraryButton);
    _centerSouthPanel.add(Box.createRigidArea(new Dimension(5, 0)));
    _centerSouthPanel.add(_assignToAllLibraryButton);
    _centerSouthPanel.add(Box.createRigidArea(new Dimension(5, 0)));
    _centerSouthPanel.add(_createNewLibraryButton);

    _packageLayoutTable.setModel(_packageLayoutTableModel);
    _packageLayoutTable.setEditorsAndRenderers();
    _packageLayoutTable.setPreferredColumnWidths();
    _packageLayoutTableModel.setExportPackageLayoutTable(_packageLayoutTable);
    populateTableWithFilterData();

    if(_isFirstTime)
    {
      _packageLayoutTableModel.selectAllPackages();
      _isFirstTime = false;
    }
    _tablePanelLayoutScroll.getViewport().add(_packageLayoutTable, null);
    _tablePanelLayoutScroll.getViewport().setOpaque(false);

    _centerPanel.add(_centerNorthPanel, BorderLayout.NORTH);
    _centerPanel.add(_centerSouthPanel, BorderLayout.SOUTH);
    _centerPanel.add(_tablePanelLayoutScroll, BorderLayout.CENTER);
    _drawCadPanel.add(_drawLandPatternPanel, BorderLayout.CENTER);
    _drawCadPanel.add(_toggleCadDisplayCheckBox, BorderLayout.SOUTH);
    add(_northPanel, BorderLayout.NORTH);
    add(_splitPane, BorderLayout.CENTER);

    ListSelectionModel rowSM = _packageLayoutTable.getSelectionModel();
    rowSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        TableColumnModel tableColumnModel = _packageLayoutTable.getColumnModel();

        tableColumnModel.getColumn(ExportPackageLayoutTable._SOURCE_LIBRARY_INDEX).getCellEditor().cancelCellEditing();

        ListSelectionModel lsm = (ListSelectionModel)e.getSource();

        if (lsm.getValueIsAdjusting() == false)
        {
          updateCadDisplay();
        }
      }
    });

    _packageLayoutTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        multiEditTableMouseReleased(e, _packageLayoutTable);
      }
    });

    _selectAllButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectAllButton_actionPerformed();
      }
    });

    _unselectAllButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        unselectAllButton_actionPerformed();
      }
    });

    _assignToAllEmptyLibraryButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        assignToAllEmptyLibraryButton_actionPerformed(e);
      }
    });

    _assignToAllLibraryButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        assignToAllLibraryButton_actionPerformed();
      }
    });

    _createNewLibraryButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        createNewLibraryButton_actionPerformed();
      }
    });

    _allPackagesButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        packagesButtonGroup_itemStateChanged(e);
      }
    });

    _newModifiedPackagesButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        packagesButtonGroup_itemStateChanged(e);
      }
    });

    _importedPackageButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        packagesButtonGroup_itemStateChanged(e);
      }
    });
    
    _toggleCadDisplayCheckBox.setSelected(_libraryWizardGeneralPersistance.getExportPackageSelectionCadDisplay());
    _toggleCadDisplayCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if(_toggleCadDisplayCheckBox.isSelected())
        {
          _libraryWizardGeneralPersistance.setExportPackageSelectionCadDisplay(true);
          updateCadDisplay();
        }
        else
        {
          _libraryWizardGeneralPersistance.setExportPackageSelectionCadDisplay(false);
          updateCadDisplay();
        }
      }
    });

    _unselectAllButton.setEnabled(true);
    _selectAllButton.setEnabled(true);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void validatePanel()
  {
    boolean canProceedToNextButton = true;

    Assert.expect(_packageLayoutTableModel != null );

    if(_packageLayoutTableModel.getSelectedPackages().size() == 0)
    {
      canProceedToNextButton = false;
    }

    for (CompPackage compPackage : _packageLayoutTableModel.getSelectedPackages())
    {
      if (compPackage.hasLibraryPath() == false)
      {
        canProceedToNextButton = false;
        break;
      }
      else if (LibraryManager.doesLibraryExist(compPackage.getLibraryPath()) == false)
      {
        canProceedToNextButton = false;
        break;
      }
    }

    // next = enabled , cancel = enabled , finish = disable , back = enabled
    WizardButtonState buttonState = new WizardButtonState(canProceedToNextButton, true, false, true);
    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void populatePanelWithData()
  {
    createPanel();

    // set the title
    _dialog.changeTitle(StringLocalizer.keyToString("PLWK_EXPORT_SELECT_PACKAGE_DIALOG_TITLE_KEY"));

    validatePanel();
  }

  /**
   * @author Wei Chin, Chong
   */
  public void unpopulate()
  {
    if (_drawLandPatternPanel != null)
    {
      _drawLandPatternPanel.resetPackageGraphics();
      _drawLandPatternPanel.dispose();
    }

    if(_packageLayoutTableModel != null)
      _packageLayoutTableModel.clear();

    _packageLayoutTableModel = null;

    if(_packageLayoutTable != null)
      _packageLayoutTable.removeAll();

    _mainPanelBorderLayout = null;

    _packagesButtonGroup = null;
    _selectAllButton = null;
    _unselectAllButton = null;
    _createNewLibraryButton = null;
    _assignToAllEmptyLibraryButton = null;
    _assignToAllLibraryButton = null;

    _allPackagesButton = null;
    _newModifiedPackagesButton = null;
    _importedPackageButton = null;

    _newLibraryDialog = null;

    _descLabel = null;
    _packagesLabel = null;

    _drawLandPatternPanel = null;
    _northPanel = null;
    _centerPanel = null;
    _centerNorthPanel = null;
    _centerSouthPanel = null;
    _tablePanelLayoutScroll = null;

    _packageLayoutTable = null;
    _packageLayoutTableModel = null;

    _name = null;
    _project = null;
    _selectedPackages = null;
    _libraryManager = null;
  }

  /**
   * Set the Panel Name
   * @param name String
   *
   * @author Wei Chin, Chong
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null, "Name of panel is null");
    Assert.expect(name.length() > 0, "Name of panel is an empty string");
    _name = name;
  }

  /**
   * get The Panel Name
   * @return String
   *
   * @author Wei Chin, Chong
   */
  public String getWizardName()
  {
    Assert.expect(_name != null, "Name of panel is not set");
    return _name;
  }

  /**
   * Get a list of selected (checked) Packages
   * @return List
   *
   * @author Wei Chin, Chong
   */
  public java.util.List<CompPackage> getSelectedPackages()
  {
    Assert.expect(_packageLayoutTableModel != null);

    return _packageLayoutTableModel.getSelectedPackages();
  }

  /**
   * @author Wei Chin, Chong
   */
  public void removeObservers()
  {
    _projectObservable.deleteObserver(this);
    _guiObservable.deleteObserver(this);
  }

  /**
   * @author Wei Chin, Chong
   */
  void selectAllButton_actionPerformed()
  {
    Assert.expect(_packageLayoutTableModel != null);

    _packageLayoutTableModel.selectAllPackages();
  }

  /**
   * @author Wei Chin, Chong
   */
  void unselectAllButton_actionPerformed()
  {
    Assert.expect(_packageLayoutTableModel != null);

    _packageLayoutTableModel.unselectAllPackages();
  }

  /**
   * @author Wei Chin, Chong
   */
  private void createNewLibraryButton_actionPerformed()
  {
    Assert.expect(_packageLayoutTable != null);
    Assert.expect(_packageLayoutTableModel != null);

    java.util.List <CompPackage> selectedCompPackages = new ArrayList<CompPackage>();
    int [] selectedRows = _packageLayoutTable.getSelectedRows();
    for(int selectedRow : selectedRows)
    {
      if (selectedRows.length >= 0)
        selectedCompPackages.add(_packageLayoutTableModel.getCompPackagesAt(selectedRow));
    }
    _newLibraryDialog = new ExportWizardNewLibraryDialog((Frame)_dialog.getParent(), true, selectedCompPackages, _project.getPanel().getCompPackages(), this);
    _newLibraryDialog.pack();
    _newLibraryDialog.populateDialogWithData();
    SwingUtils.centerOnComponent(_newLibraryDialog, _dialog);
    _newLibraryDialog.setVisible(true);
//    _newLibraryDialog.dispose();
    _newLibraryDialog = null;

    selectedRows = null;
    if(selectedCompPackages != null)
    {
      selectedCompPackages.clear();
      selectedCompPackages = null;
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  private void packagesButtonGroup_itemStateChanged(ItemEvent e)
  {
    saveRadioButtonState((JRadioButton)e.getSource());
    populateTableWithFilterData();
  }

  /**
   * @author Wei Chin, Chong
   */
  public void populateTableWithFilterData()
  {
    Assert.expect(_allPackagesButton != null);
    Assert.expect(_newModifiedPackagesButton != null);
    Assert.expect(_importedPackageButton != null);
    Assert.expect(_packageLayoutTable != null);
    Assert.expect(_packageLayoutTableModel != null);

    if(_allPackagesButton.isSelected())
      _packageLayoutTableModel.populateWithPackages(_project,_packageLayoutTableModel.FILTER_BY_ALL);
    else if(_newModifiedPackagesButton.isSelected())
      _packageLayoutTableModel.populateWithPackages(_project,_packageLayoutTableModel.FILTER_BY_NEW_OR_MOPDIFIED);
    else if(_importedPackageButton.isSelected())
      _packageLayoutTableModel.populateWithPackages(_project, _packageLayoutTableModel.FILTER_BY_IMPORTED);

    if(_packageLayoutTable.getSortedColumnIndex() > 0)
      _packageLayoutTableModel.sortColumn(_packageLayoutTable.getSortedColumnIndex(), _packageLayoutTable.isSortedColumnAscending());
    else
      _packageLayoutTableModel.sortColumn(ExportPackageLayoutTable._PACKAGE_INDEX, true);
  }

  /**
   * This method implements the right click edit when multiple lines of a table are selected
   *
   * @param e MouseEvent
   * @param table JTable
   *
   * @author Wei Chin, Chong
   */
  private void multiEditTableMouseReleased(MouseEvent e, JTable table)
  {
    Assert.expect(e != null);
    Assert.expect(table != null);

    // ignore any other mouse events than a popup trigger -- we don't want to break the normal L&F
    if (e.isPopupTrigger())
    {
      // get the table cell that the mouse was right-clicked in
      Point p = e.getPoint();
      int row = table.rowAtPoint(p);
      int col = table.columnAtPoint(p);

      // if this cell is not currently selected, exit.  It's confusing to edit a cell that not selected.
      // we could "select" this cell too, but for now, we'll just not.
      if (table.isCellSelected(row, col) == false)
        return;

      // start the editing process for this cell
      table.editCellAt(row, col);
      // if this is a combo box cell editor, the editCellAt doesn't show the combo box popup, so let's show that
      // only if this is a combo box editor
      Object obj = table.getEditorComponent();
      if (obj instanceof JComboBox)
      {
        JComboBox comboBox = (JComboBox)obj;
        comboBox.showPopup();
      }
      else if(obj instanceof JCheckBox)
      {
        JCheckBox checkBox = (JCheckBox)obj;
        checkBox.doClick();
      }
    }
  }

  /**
   * @param actionEvent ActionEvent
   * @author Wei Chin, Chong
   */
  public void assignToAllEmptyLibraryButton_actionPerformed(ActionEvent actionEvent)
  {
    Assert.expect(_packageLayoutTable != null);
    Assert.expect(_packageLayoutTableModel != null);

    int selectedRow = _packageLayoutTable.getSelectedRow();
    if(selectedRow >= 0)
    {
      _packageLayoutTableModel.assignEmptyLibraries(
          _packageLayoutTableModel.getCompPackagesAt(selectedRow).getLibraryPath());
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  public void assignToAllLibraryButton_actionPerformed()
  {
    Assert.expect(_packageLayoutTable != null);
    Assert.expect(_packageLayoutTableModel != null);

    int selectedRow = _packageLayoutTable.getSelectedRow();
    if (selectedRow >= 0)
    {
      _packageLayoutTableModel.assignAllLibraries(
          _packageLayoutTableModel.getCompPackagesAt(selectedRow).getLibraryPath());
    }
  }

  /**
   * @param radioButton JRadioButton
   * @author Wei Chin, Chong
   */
  private void saveRadioButtonState(JRadioButton radioButton)
  {
    if(radioButton == _allPackagesButton)
    {
      _allPackagesSelected = true;
      _newModifiedPackagesSelected = false;
      _importedPackageSelected = false;
    }
    else if(radioButton == _newModifiedPackagesButton)
    {
      _allPackagesSelected = false;
      _newModifiedPackagesSelected = true;
      _importedPackageSelected = false;
    }
    else if(radioButton == _importedPackageButton)
    {
      _allPackagesSelected = false;
      _newModifiedPackagesSelected = false;
      _importedPackageSelected = true;
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  private void assignRadioButtonState()
  {
    validateRadioButtonState();
    _allPackagesButton.setSelected(_allPackagesSelected);
    _newModifiedPackagesButton.setSelected(_newModifiedPackagesSelected);
    _importedPackageButton.setSelected(_importedPackageSelected);
  }

  /**
   * @author Wei Chin, Chong
   */
  private void validateRadioButtonState()
  {
    if(_allPackagesSelected)
    {
      Assert.expect(_newModifiedPackagesSelected == false &&
                    _importedPackageSelected == false);
    }
    else if(_newModifiedPackagesSelected)
    {
      Assert.expect(_allPackagesSelected == false &&
                    _importedPackageSelected == false);
    }
    else if(_importedPackageSelected)
    {
      Assert.expect(_newModifiedPackagesSelected == false &&
                    _allPackagesSelected == false);
    }
  }

  /**
   * @param observable Observable
   * @param object Object
   * @author Wei Chin, Chong
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof GuiObservable)
        {
          GuiEvent guiEvent = (GuiEvent)object;
          GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
          if (guiEventEnum instanceof SelectionEventEnum)
          {
            SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
            if (selectionEventEnum.equals(SelectionEventEnum.EXPORT_LIBRARY_SELECTION))
            {
              validatePanel();
            }
          }
        }
      }
    });
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void updateCadDisplay()
  {
    if (_drawLandPatternPanel != null)
    {
      int selectedRows = _packageLayoutTable.getSelectedRow();
      if (selectedRows >= 0)
      {
        if (_libraryWizardGeneralPersistance.getExportPackageSelectionCadDisplay() == true)
        {
          _drawLandPatternPanel.resetPackageGraphics();
          CompPackage compPackage = _packageLayoutTableModel.getCompPackagesAt(selectedRows);
          _drawLandPatternPanel.drawPackage(compPackage);
          _drawLandPatternPanel.scaleDrawing();
          _drawCadTitleBorder.setTitle(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY") + compPackage.getShortName());
        }
        else
        {
          _drawLandPatternPanel.resetPackageGraphics();
          _drawCadTitleBorder.setTitle(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY"));
        }
      }
      else
      {
        _drawLandPatternPanel.resetPackageGraphics();
        _drawCadTitleBorder.setTitle(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY"));
      }
      
      revalidate();
      repaint();
    }
  }
}
