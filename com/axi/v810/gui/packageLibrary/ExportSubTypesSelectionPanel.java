package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.v810.gui.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class ExportSubTypesSelectionPanel extends WizardPanel implements Observer
{
  private BorderLayout _mainPanelBorderLayout;

  private ButtonGroup _subtypesButtonGroup;
  private JButton _selectAllButton;
  private JButton _unselectAllButton;
  private JButton _compareThresholdsButton;
  private JRadioButton _allSubtypesButton;
  private JRadioButton _newOrModifiedSubtypesButton;
  private JRadioButton _importedSubtypesButton;

  private ExportSubtypeLayoutTable _subtypeLayoutTable;
  private ExportSubtypeLayoutTableModel _subtypeLayoutTableModel;

  private JLabel _descLabel;
  private JLabel _subtypeLabel;

  private JScrollPane _tablePanelLayoutScroll;

  private JPanel _northPanel;
  private JPanel _centerPanel;
  private JPanel _centerNorthPanel;
  private JPanel _centerSouthPanel;

  private List<CompPackage> _packages = null;
  private List<SubtypeScheme> _subtypeSchemes = null;
  private List<SubtypeScheme> _selectedSubtypeSchemes = null;

  private ExportPackageSelectionPanel _exportPackageSelectionPanel = null;

  protected String _name;

  private static SubtypeSchemeObservable _subtypeSchemeObservable = null;
  private static GuiObservable _guiObservable = null;

  private boolean _isFirstTime = true;
  private boolean _allSubtypeSchemesSelected = true;
  private boolean _newModifiedSubtypeSchemesSelected = false;
  private boolean _importedSubtypeSchemesSelected = false;

  /**
   * @param dialog WizardDialog
   * @param exportPackageSelectionPanel ExportPackageSelectionPanel
   * @author Wei Chin, Chong
   */
  ExportSubTypesSelectionPanel(WizardDialog dialog, ExportPackageSelectionPanel exportPackageSelectionPanel)
  {
    super(dialog);

    Assert.expect(exportPackageSelectionPanel != null);

    _exportPackageSelectionPanel = exportPackageSelectionPanel;
    _subtypeSchemeObservable = SubtypeSchemeObservable.getInstance();
    _guiObservable = GuiObservable.getInstance();
    _isFirstTime = true;
  }

  /**
   * @author Wei Chin, Chong
   */
  private void init()
  {
    _subtypeSchemeObservable.addObserver(this);
    _guiObservable.addObserver(this);

    _mainPanelBorderLayout = new BorderLayout(5, 5);

    _descLabel = new JLabel(StringLocalizer.keyToString("PLWK_EXPORT_SELECT_SUBTYPES_DESCRIPTION_KEY"));
    _subtypeLabel = new JLabel(StringLocalizer.keyToString("PLWK_EXPORT_SUBTYPES_KEY"));
    _subtypesButtonGroup = new ButtonGroup();
    _selectAllButton = new JButton(StringLocalizer.keyToString("PLWK_EXPORT_SELECT_ALL_BUTTON_KEY"));
    _unselectAllButton = new JButton(StringLocalizer.keyToString("PLWK_UNSELECT_ALL_KEY"));
    _compareThresholdsButton = new JButton(StringLocalizer.keyToString("PLWK_COMPARE_THRESHOLD_BUTTON_KEY"));
    _allSubtypesButton = new JRadioButton(StringLocalizer.keyToString("PLWK_EXPORT_ALL_RADIO_BUTTON_KEY"));
    _newOrModifiedSubtypesButton = new JRadioButton(StringLocalizer.keyToString("PLWK_EXPORT_NEW_MODIFIED_RADIO_BUTTON_KEY"));
    _importedSubtypesButton = new JRadioButton(StringLocalizer.keyToString("PLWK_EXPORT_IMPORTED_TABLE_FIELD_KEY"));

    _northPanel = new JPanel(new BorderLayout(5, 5));
    _centerPanel = new JPanel(new BorderLayout(5, 5));
    _centerNorthPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
    _centerSouthPanel = new JPanel();
    _tablePanelLayoutScroll = new JScrollPane();

    _packages = _exportPackageSelectionPanel.getSelectedPackages();
    if(_packages == null)
      _packages = new ArrayList<CompPackage>();

    if(_selectedSubtypeSchemes == null)
      _selectedSubtypeSchemes = new ArrayList<SubtypeScheme>();

    _subtypeSchemes = new ArrayList<SubtypeScheme>();
    _subtypeLayoutTable = new ExportSubtypeLayoutTable((Frame)_dialog.getOwner());
    _subtypeLayoutTableModel = new ExportSubtypeLayoutTableModel(_selectedSubtypeSchemes);
  }

  /**
   * @author Wei Chin, Chong
   */
  private void createPanel()
  {
    removeAll();

    init();

    populateSubtypeScheme();

    setLayout(_mainPanelBorderLayout);
    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    _northPanel.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));
    _northPanel.setBackground(Color.WHITE);
    _northPanel.add(_descLabel, BorderLayout.CENTER);

    assignRadioButtonState();

    _subtypeLayoutTable.setModel(_subtypeLayoutTableModel);
    _subtypeLayoutTable.setEditorsAndRenderers();
    _subtypeLayoutTable.setPreferredColumnWidths();
    _subtypeLayoutTableModel.setExportSubtypeLayoutTable(_subtypeLayoutTable);
    populateTableWithFilterData();

    // set default value for the first time
    if(_isFirstTime)
    {
      _subtypeLayoutTableModel.selectAllPackages();
      _isFirstTime = false;
    }

    _tablePanelLayoutScroll.getViewport().add(_subtypeLayoutTable, null);
    _tablePanelLayoutScroll.getViewport().setOpaque(false);

    _subtypesButtonGroup.add(_allSubtypesButton);
    _subtypesButtonGroup.add(_newOrModifiedSubtypesButton);
    _subtypesButtonGroup.add(_importedSubtypesButton);

    _centerNorthPanel.add(_subtypeLabel);
    _centerNorthPanel.add(_allSubtypesButton);
    _centerNorthPanel.add(_newOrModifiedSubtypesButton);
    _centerNorthPanel.add(_importedSubtypesButton);

    _centerSouthPanel.setLayout(new BoxLayout(_centerSouthPanel, BoxLayout.LINE_AXIS));
    _centerSouthPanel.add(_selectAllButton);
    _centerSouthPanel.add(Box.createRigidArea(new Dimension(5, 0)));
    _centerSouthPanel.add(_unselectAllButton);
    _centerSouthPanel.add(Box.createHorizontalGlue());
    _centerSouthPanel.add(_compareThresholdsButton);

    _centerPanel.add(_centerNorthPanel, BorderLayout.NORTH);
    _centerPanel.add(_tablePanelLayoutScroll, BorderLayout.CENTER);
    _centerPanel.add(_centerSouthPanel, BorderLayout.SOUTH);

    add(_northPanel,BorderLayout.NORTH);
    add(_centerPanel, BorderLayout.CENTER);

    _compareThresholdsButton.setEnabled(false);

    _subtypeLayoutTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        multiEditTableMouseReleased(e, _subtypeLayoutTable);
      }
    });

    _selectAllButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectAllButton_actionPerformed();
      }
    });

    _unselectAllButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        unselectAllButton_actionPerformed();
      }
    });

    _compareThresholdsButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        compareThresholdsButton_actionPerformed();
      }
    });

    // XCR-3102 Assert when import library with empty subtype name
    // Disable subtype name editing feature in Export Wizard. This could cause unexpected crash during import.
    //_subtypeLayoutTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    //{
    //  public void valueChanged(ListSelectionEvent e)
    //  {
    //    TableColumnModel tableColumnModel = _subtypeLayoutTable.getColumnModel();
    //
    //    tableColumnModel.getColumn(ExportSubtypeLayoutTable._SUBTYPE_TYPE_INDEX).getCellEditor().cancelCellEditing();
    //
    //    subtypeLayoutTable_valueChanged(e);
    //  }
    //});

    _allSubtypesButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        subtypesButtonGroup_itemStateChanged(e);
      }
    });

    _newOrModifiedSubtypesButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        subtypesButtonGroup_itemStateChanged(e);
      }
    });

    _importedSubtypesButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        subtypesButtonGroup_itemStateChanged(e);
      }
    });
  }

  /**
   * @author Wei Chin, Chong
   */
  public void validatePanel()
  {
    boolean canProceedToFinishButton = true;

    Assert.expect(_subtypeLayoutTableModel != null);

    if (_subtypeLayoutTableModel.getSelectedSubtypeSchmes().size() == 0)
    {
      canProceedToFinishButton = false;
    }

  // next = enabled , cancel = enabled , finish = disable , back = enabled
    WizardButtonState buttonState = new WizardButtonState(false, true, canProceedToFinishButton, true);
    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void populatePanelWithData()
  {
    createPanel();

    // set the title
    _dialog.changeTitle(StringLocalizer.keyToString("PLWK_EXPORT_SELECT_SUBTYPE_DIALOG_TITLE_KEY"));

    validatePanel();
  }

  /**
   * @author Wei Chin, Chong
   */
  public void unpopulate()
  {
    if (_subtypeLayoutTableModel != null)
      _subtypeLayoutTableModel.clear();

    if (_packages != null)
      _packages.clear();

    if (_selectedSubtypeSchemes != null)
      _selectedSubtypeSchemes.clear();

    if (_subtypeSchemes != null)
      _subtypeSchemes.clear();

    _mainPanelBorderLayout = null;
    _subtypesButtonGroup = null;
    _selectAllButton = null;
    _unselectAllButton = null;
    _compareThresholdsButton = null;
    _allSubtypesButton = null;
    _newOrModifiedSubtypesButton = null;
    _importedSubtypesButton = null;

    _subtypeLayoutTable = null;
    _subtypeLayoutTableModel = null;

    _descLabel = null;
    _subtypeLabel = null;

    _tablePanelLayoutScroll = null;

    _northPanel = null;
    _centerPanel = null;
    _centerNorthPanel = null;
    _centerSouthPanel = null;

    _packages = null;
    _subtypeSchemes = null;
    _selectedSubtypeSchemes = null;

    _exportPackageSelectionPanel = null;

    _name = null;
  }

  /**
   * @param name String
   * @author Wei Chin, Chong
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null, "Name of panel is null");
    Assert.expect(name.length() > 0, "Name of panel is an empty string");
    _name = name;
  }

  /**
   * @return String
   * @author Wei Chin, Chong
   */
  public String getWizardName()
  {
    Assert.expect(_name != null, "Name of panel is not set");
    return _name;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void removeObservers()
  {
    Assert.expect(_subtypeSchemeObservable != null);
    Assert.expect(_guiObservable != null);

    _subtypeSchemeObservable.deleteObserver(this);
    _guiObservable.deleteObserver(this);
  }

  /**
   * @param packages List
   * @author Wei Chin, Chong
   */
  public void setPackages(java.util.List<CompPackage> packages)
  {
    Assert.expect(packages != null);

    _packages = packages;
  }

  /**
   * @param selectedSubtypeSchmes List
   * @author Wei Chin, Chong
   */
  public void setSelectedSubtypeSchemes(java.util.List<SubtypeScheme> selectedSubtypeSchmes)
  {
    Assert.expect(selectedSubtypeSchmes != null);

    _selectedSubtypeSchemes = selectedSubtypeSchmes;
  }

  /**
   * @author Wei Chin, Chong
   */
  private void selectAllButton_actionPerformed()
  {
    Assert.expect(_subtypeLayoutTableModel != null);

    _subtypeLayoutTableModel.selectAllPackages();
  }

  /**
   * @author Wei Chin, Chong
   */
  private void unselectAllButton_actionPerformed()
  {
    Assert.expect(_subtypeLayoutTableModel != null);

    _subtypeLayoutTableModel.unselectAllPackages();
  }

  /**
   * @author Wei Chin, Chong
   */
  private void compareThresholdsButton_actionPerformed()
  {
    Assert.expect(_subtypeLayoutTable != null);
    Assert.expect(_subtypeLayoutTableModel != null);

    CompareThresholdDialog compareThresholdDialog = new CompareThresholdDialog((Frame)_dialog.getParent(), true);

    compareThresholdDialog.populateDialogWithData(_subtypeLayoutTableModel.getSubtypeSchemeAt(_subtypeLayoutTable.getSelectedRow()),
                                                  _subtypeLayoutTableModel.getSubtypeSchemeAt(_subtypeLayoutTable.getSelectedRow()).getLibraryPath());

    compareThresholdDialog.pack();
    SwingUtils.centerOnComponent(compareThresholdDialog, this);
    compareThresholdDialog.setVisible(true);
    compareThresholdDialog.dispose();
  }

  /**
   * @author Wei Chin, Chong
   */
  private void subtypesButtonGroup_itemStateChanged(ItemEvent e)
  {
    saveRadioButtonState((JRadioButton)e.getSource());
    populateTableWithFilterData();
  }

  /**
   * @param e ListSelectionEvent
   * @author Wei Chin, Chong
   */
  private void subtypeLayoutTable_valueChanged(ListSelectionEvent e)
  {
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    if (lsm.isSelectionEmpty())
      _compareThresholdsButton.setEnabled(false);
    else if (lsm.getValueIsAdjusting() == false)
    {
      int selectedRows = _subtypeLayoutTable.getSelectedRow();
      if (selectedRows >= 0)
      {
        SubtypeScheme subtypeScheme = _subtypeLayoutTableModel.getSubtypeSchemeAt(selectedRows);
        if (subtypeScheme.isUpdatable())
          _compareThresholdsButton.setEnabled(true);
        else
          _compareThresholdsButton.setEnabled(false);
      }
    }
  }

  /**
   * @return List
   * @author Wei Chin, Chong
   */
  public List<SubtypeScheme> getSelectedSubTypeScheme()
  {
    Assert.expect(_subtypeLayoutTableModel != null);

    return _subtypeLayoutTableModel.getSelectedSubtypeSchmes();
  }

  /**
   * must use after init()
   * @author Wei Chin, Chong
   */
  private void populateSubtypeScheme()
  {
    Assert.expect(_subtypeSchemes != null);

    _subtypeSchemes.clear();

    for (CompPackage compPackage : _packages)
    {
      _subtypeSchemes.addAll(compPackage.getSubtypeSchemes());
    }

    checkSubtypeSchemeUpdatable();
  }

  /**
   * @author Wei Chin, Chong
   */
  private void populateTableWithFilterData()
  {
    Assert.expect(_subtypeLayoutTable != null);
    Assert.expect(_subtypeLayoutTableModel != null);
    Assert.expect(_allSubtypesButton != null);
    Assert.expect(_newOrModifiedSubtypesButton != null);
    Assert.expect(_importedSubtypesButton != null);

    if (_allSubtypesButton.isSelected())
      _subtypeLayoutTableModel.populateWithSubtypeSchemes(_subtypeSchemes, _subtypeLayoutTableModel.FILTER_BY_ALL);
    else if (_newOrModifiedSubtypesButton.isSelected())
      _subtypeLayoutTableModel.populateWithSubtypeSchemes(_subtypeSchemes, _subtypeLayoutTableModel.FILTER_BY_NEW_OR_MOPDIFIED);
    else if (_importedSubtypesButton.isSelected())
      _subtypeLayoutTableModel.populateWithSubtypeSchemes(_subtypeSchemes, _subtypeLayoutTableModel.FILTER_BY_IMPORTED);

    _subtypeLayoutTableModel.sortColumn(_subtypeLayoutTable.getSortedColumnIndex(), _subtypeLayoutTable.isSortedColumnAscending());
  }

  /**
   * @param radioButton JRadioButton
   * @author Wei Chin, Chong
   */
  private void saveRadioButtonState(JRadioButton radioButton)
  {
    if (radioButton == _allSubtypesButton)
    {
      _allSubtypeSchemesSelected = true;
      _newModifiedSubtypeSchemesSelected = false;
      _importedSubtypeSchemesSelected = false;
    }
    else if (radioButton == _newOrModifiedSubtypesButton)
    {
      _allSubtypeSchemesSelected = false;
      _newModifiedSubtypeSchemesSelected = true;
      _importedSubtypeSchemesSelected = false;
    }
    else if (radioButton == _importedSubtypesButton)
    {
      _allSubtypeSchemesSelected = false;
      _newModifiedSubtypeSchemesSelected = false;
      _importedSubtypeSchemesSelected = true;
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  private void assignRadioButtonState()
  {
    validateRadioButtonState();
    _allSubtypesButton.setSelected(_allSubtypeSchemesSelected);
    _newOrModifiedSubtypesButton.setSelected(_newModifiedSubtypeSchemesSelected);
    _importedSubtypesButton.setSelected(_importedSubtypeSchemesSelected);
  }

  /**
   * @author Wei Chin, Chong
   */
  private void validateRadioButtonState()
  {
    if (_allSubtypeSchemesSelected)
    {
      Assert.expect(_newModifiedSubtypeSchemesSelected == false &&
                    _importedSubtypeSchemesSelected == false);
    }
    else if (_newModifiedSubtypeSchemesSelected)
    {
      Assert.expect(_allSubtypeSchemesSelected == false &&
                    _importedSubtypeSchemesSelected == false);
    }
    else if (_importedSubtypeSchemesSelected)
    {
      Assert.expect(_newModifiedSubtypeSchemesSelected == false &&
                    _allSubtypeSchemesSelected == false);
    }
  }

  /**
   * @throws DatastoreException
   * @author Wei Chin, Chong
   */
  private void checkSubtypeSchemeUpdatable()
  {
    final BusyCancelDialog busyDialog = new BusyCancelDialog(
        (Frame)_dialog.getOwner(),
        StringLocalizer.keyToString("PLWK_EXPORT_CHECK_SUBTYPE_SCHEME_UPDATABLE_WAITING_KEY"),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    SwingUtils.centerOnComponent(busyDialog, (Frame)_dialog.getOwner());
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          Map subtypeSchemeToExistability = LibraryUtil.getInstance().isMatchedLibrarySubtypeSchemeExist(_subtypeSchemes);

          for (SubtypeScheme subtypeScheme : _subtypeSchemes)
          {
            Boolean exist = (Boolean)subtypeSchemeToExistability.get(subtypeScheme);
            subtypeScheme.setUpdatable(exist.booleanValue());
//            subtypeScheme.setLibraryPath(subtypeScheme.getCompPackage().getLibraryPath());
          }
        }
        catch (final DatastoreException dex)
        {
          dex.printStackTrace();
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              MessageDialog.showErrorDialog((Frame)_dialog.getOwner(),
                                            StringLocalizer.keyToString(new LocalizedString("PLWK_CHECK_SUBTYPE_SCHEME_UPDATABLE_FAILED_MESSAGE_OF_GUI_KEY",
                                                                                            new Object[]
                                                                                            {StringLocalizer.keyToString("GUI_COMPANY_CONTACT_INFORMATION_KEY")})),
                                            StringLocalizer.keyToString("PLWK_EXPORT_ERROR_MESSAGE_OF_TITLE_KEY"));
            }
          });
        }
        finally
        {
          // wait until visible
          while (busyDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          try
          {
            SwingUtils.invokeAndWait(new Runnable()
            {
              public void run()
              {
                busyDialog.dispose();
              }
            });
          }
          catch (InterruptedException ie)
          {
            // do nothing
          }
          catch (java.lang.reflect.InvocationTargetException ite)
          {
            // do nothing
          }
        }
      }
    });
    busyDialog.setVisible(true);
  }

  /**
   * This method implements the right click edit when multiple lines of a table are selected
   *
   * @param e MouseEvent
   * @param table JTable
   *
   * @author Wei Chin, Chong
   */
  private void multiEditTableMouseReleased(MouseEvent e, JTable table)
  {
    Assert.expect(e != null);
    Assert.expect(table != null);

    // ignore any other mouse events than a popup trigger -- we don't want to break the normal L&F
    if (e.isPopupTrigger())
    {
      // get the table cell that the mouse was right-clicked in
      Point p = e.getPoint();
      int row = table.rowAtPoint(p);
      int col = table.columnAtPoint(p);

      // if this cell is not currently selected, exit.  It's confusing to edit a cell that not selected.
      // we could "select" this cell too, but for now, we'll just not.
      if (table.isCellSelected(row, col) == false)
        return;

      // start the editing process for this cell
      table.editCellAt(row, col);

      Object obj = table.getEditorComponent();
      if(obj instanceof JCheckBox)
      {
        JCheckBox checkBox = (JCheckBox)obj;
        checkBox.doClick();
      }
    }
  }
  /**
   * @param observable Observable
   * @param object Object
   * @author Wei Chin, Chong
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof SubtypeSchemeObservable)
        {
          if (object instanceof ProjectChangeEvent)
          {
            ProjectChangeEvent event = (ProjectChangeEvent)object;
            ProjectChangeEventEnum eventEnum = event.getProjectChangeEventEnum();
            if (eventEnum instanceof SubtypeSchemeEventEnum )
              if (eventEnum.equals(SubtypeSchemeEventEnum.SUBTYPE_NAME))
              {
                checkSubtypeSchemeUpdatable();
                populateTableWithFilterData();
              }
          }
        }
        else if (observable instanceof GuiObservable)
        {
          GuiEvent guiEvent = (GuiEvent)object;
          GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
          if (guiEventEnum instanceof ScreenChangeEventEnum)
          {
            ScreenChangeEventEnum selectionEventEnum = (ScreenChangeEventEnum)guiEventEnum;
            if (selectionEventEnum.equals(ScreenChangeEventEnum.PACKAGE_LIBRARY))
            {
              populateTableWithFilterData();
            }
          }
          else if (guiEventEnum instanceof SelectionEventEnum)
          {
            SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
            if (selectionEventEnum.equals(SelectionEventEnum.EXPORT_SUBTYPE_SELECTION))
            {
              validatePanel();
            }
          }
        }
      }
    });
  }
}
