package com.axi.v810.gui.packageLibrary;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

public class ImportWizardSummaryPanel extends WizardPanel
{
  private JPanel _mainPanel;
  private JPanel _contentPanel;
  private TitledBorder _titleBorder;
  private JLabel _librariesUsedLabel;
  private JLabel _packagesToImportLabel;
  private JLabel _packagesLabel;
  private JLabel _automaticMatchCriteriaLabel;
  private JLabel _landPatternGeometryLabel;
  private JLabel _jointTypeLabel;
  private JLabel _landPatternNameLabel;
  private JPanel _summaryDescPanel;
  private JLabel _summaryDescLabel;

  /**
   * @param project Project
   * @param dialog WizardDialog
   *
   * @author Tan Hock Zoon
   */
  public ImportWizardSummaryPanel(Project project, WizardDialog dialog)
  {
    super(dialog);
    init();
  }

  private void init()
  {
    _mainPanel = new JPanel();
    _contentPanel = new JPanel();
    _titleBorder = BorderFactory.createTitledBorder(StringLocalizer.keyToString("PLWK_LIBRABRY_IMPORT_KEY"));
    _librariesUsedLabel = new JLabel(StringLocalizer.keyToString("PLWK_LIBRARIES_USED_KEY"));
    _packagesToImportLabel = new JLabel(StringLocalizer.keyToString("PLWK_PACKAGES_TO_IMPORT_KEY"));
    _packagesLabel = new JLabel(StringLocalizer.keyToString(new LocalizedString("PLWK_PACKAGES_IMPORT_NUMBER_KEY", new Object[]{"10", "34"})));
    _automaticMatchCriteriaLabel = new JLabel(StringLocalizer.keyToString("PLWK_MATCH_CRITERIA_KEY"));
    _landPatternGeometryLabel = new JLabel(StringLocalizer.keyToString(new LocalizedString("PLWK_LAND_PATTERN_GEOMETRY_PERCENTAGE_KEY", new Object[]{"95"})));
    _jointTypeLabel = new JLabel(StringLocalizer.keyToString("ATGUI_FAMILY_KEY"));
    _landPatternNameLabel = new JLabel(StringLocalizer.keyToString(new LocalizedString("PLWK_LAND_PATTERN_NAME_CRITERIA_KEY", new Object[]{"Start with", "\"Proj\""})));
    _summaryDescPanel = new JPanel();
    _summaryDescLabel = new JLabel(StringLocalizer.keyToString("PLWK_IMPORT_SUMMARY_DESC_KEY"));
  }
  /**
   * @author Tan Hock Zoon
   */
  private void createPanel()
  {
    removeAll();

    _mainPanel.setLayout(new BorderLayout(5,5));

    _contentPanel.setLayout(new BoxLayout(_contentPanel, BoxLayout.Y_AXIS));
    _contentPanel.add(_librariesUsedLabel);
    _contentPanel.add(_packagesToImportLabel);
    _contentPanel.add(_packagesLabel);
    _contentPanel.add(_automaticMatchCriteriaLabel);
    _contentPanel.add(_landPatternGeometryLabel);
    _contentPanel.add(_jointTypeLabel);
    _contentPanel.add(_landPatternNameLabel);

    _contentPanel.setBorder(_titleBorder);

    _summaryDescPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    _summaryDescPanel.setBackground(Color.WHITE);
    _summaryDescPanel.add(_summaryDescLabel);

    _mainPanel.add(_summaryDescPanel, BorderLayout.NORTH);
    _mainPanel.add(_contentPanel, BorderLayout.CENTER);
    //setBorder(titleBorder);

    add(_mainPanel, BorderLayout.CENTER);

    _dialog.setPreferredSize(new Dimension(575, 400));
  }

  /**
   * @author Tan Hock Zoon
   */
  public void validatePanel()
  {
    WizardButtonState buttonState = new WizardButtonState(true, true, false, true);
    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Tan Hock Zoon
   */
  public void populatePanelWithData()
  {
    createPanel();
    validatePanel();
  }

  /**
   * @author Tan Hock Zoon
   */
  public void unpopulate()
  {
  }

  /**
   * @author Tan Hock Zoon
   */
  public void setWizardName(String name)
  {
    _name = name;
  }

  /**
   * @author Tan Hock Zoon
   */
  public String getWizardName()
  {
    return _name;
  }

  /**
   * @author Tan Hock Zoon
   */
  public void removeObservers()
  {
  }
}
