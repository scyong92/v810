package com.axi.v810.gui.packageLibrary;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * <p>Company: Agilent Technogies</p>
 *
 * @author Wei Chin, Chong
 * @version 1.0
 */

/** @todo APM Code Review I noticed when doing a sort on the table in this panel, that the Row Selection was lost after
  * the sort -- I don't remember where the code to do that goes, so I'm putting my code review tag here before I forget. */
public class ExportSubtypeLayoutTableModel extends DefaultSortTableModel
{
  private ExportSubtypeLayoutTable _exportSubtypeLayoutTable = null;
// command manager for undo functionality
//  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private List<SubtypeScheme> _subtypeSchemes = new ArrayList<SubtypeScheme>();

  private static final String _SELECT_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_SELECT_TABLE_HEADER_KEY");
  private static final String _PACKAGE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_PACKAGE_TABLE_HEADER_KEY");
  private static final String _JOINT_TYPE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_JOINT_TYPE_TABLE_HEADER_KEY");
  private static final String _SUBTYPE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_SUBTYPE_TABLE_HEADER_KEY");
  private static final String _ADD_UPDATE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_ADD_UPDATE_TABLE_HEADER_KEY");
  private static final String _NEW_MODIFIED_COLUMN_HEADING = StringLocalizer.keyToString(new LocalizedString("PLWK_EXPORT_NEW_MODIFIED_TABLE_HEADER_KEY",
                                                                                         new Object[]{StringLocalizer.keyToString("PLWK_EXPORT_SUBTYPE_TABLE_HEADER_KEY")}));
  private static final String _SOURCE_LIBRARY_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_SOURCE_LIBRARY_TABLE_HEADER_KEY");

  public static final int FILTER_BY_ALL = 0;
  public static final int FILTER_BY_NEW_OR_MOPDIFIED = 1;
  public static final int FILTER_BY_IMPORTED = 2;

  private final String[] _tableColumns =
      {_SELECT_COLUMN_HEADING,
      _PACKAGE_COLUMN_HEADING,
      _JOINT_TYPE_COLUMN_HEADING,
      _SUBTYPE_COLUMN_HEADING,
      _ADD_UPDATE_COLUMN_HEADING,
      _NEW_MODIFIED_COLUMN_HEADING,
      _SOURCE_LIBRARY_COLUMN_HEADING};

  private List<SubtypeScheme> _selectedSubtypeSchemes = null;

  /**
   * @param selectedSubtypeSchmes List
   * @author Wei Chin, Chong
   */
  public ExportSubtypeLayoutTableModel(List<SubtypeScheme> selectedSubtypeSchmes)
  {
    Assert.expect(selectedSubtypeSchmes != null);

    _selectedSubtypeSchemes = selectedSubtypeSchmes;
  }

  /**
   * @param exportSubtypeLayoutTable
   * @author Wei Chin, Chong
   */
  public void setExportSubtypeLayoutTable(ExportSubtypeLayoutTable exportSubtypeLayoutTable)
  {
    Assert.expect(exportSubtypeLayoutTable != null);
    _exportSubtypeLayoutTable = exportSubtypeLayoutTable;
  }

  /**
   * @param subtypeSchemes List
   *
   * @author Wei Chin, Chong
   */
  public void populateWithSubtypeSchemes(List<SubtypeScheme> subtypeSchemes)
  {
    populateWithSubtypeSchemes(subtypeSchemes, FILTER_BY_NEW_OR_MOPDIFIED);
    removeInvalidSelectedSubtypeSchemes(subtypeSchemes);
  }

  /**
   * @param subtypeSchemes List
   * @param filterType int
   *
   * @author Wei Chin, Chong
   */
  public void populateWithSubtypeSchemes(List<SubtypeScheme> subtypeSchemes, int filterType)
  {
    Assert.expect(subtypeSchemes != null);
    _subtypeSchemes.clear();

    if (filterType == FILTER_BY_ALL)
    {
      _subtypeSchemes.addAll(subtypeSchemes);
    }
    else if (filterType == FILTER_BY_NEW_OR_MOPDIFIED)
    {
      for (SubtypeScheme subtypeScheme : subtypeSchemes)
      {
        if (subtypeScheme.isImportedSubtypeScheme() == false ||
            subtypeScheme.isModifiedAfterImported() )
          _subtypeSchemes.add(subtypeScheme);
      }
    }
    else if (filterType == FILTER_BY_IMPORTED)
    {
      for (SubtypeScheme subtypeScheme : subtypeSchemes)
      {
        if (subtypeScheme.isImportedSubtypeScheme() &&
            subtypeScheme.isModifiedAfterImported() == false)
          _subtypeSchemes.add(subtypeScheme);
      }
    }
    else
    {
      Assert.expect(false);
    }
    removeInvalidSelectedSubtypeSchemes(subtypeSchemes);
    fireTableDataChanged();
  }

  /**
   * @author Wei Chin, Chong
   */
  public void removeInvalidSelectedSubtypeSchemes(List<SubtypeScheme> subtypeSchemes)
  {
    List<SubtypeScheme> subtypeSchemeToRemove = null;

    if (_selectedSubtypeSchemes.isEmpty() == false)
    {
      subtypeSchemeToRemove = new ArrayList<SubtypeScheme>();
      for (SubtypeScheme subtypeScheme : _selectedSubtypeSchemes)
      {
        if (subtypeSchemes.contains(subtypeScheme) == false)
        {
          subtypeSchemeToRemove.add(subtypeScheme);
        }
      }
      _selectedSubtypeSchemes.removeAll(subtypeSchemeToRemove);
      subtypeSchemeToRemove.clear();
    }
    subtypeSchemeToRemove = null;
  }

  /**
   * @param row int
   * @return SubtypeScheme
   * @author Wei Chin, Chong
   */
  public SubtypeScheme getSubtypeSchemeAt(int row)
  {
    Assert.expect(row >= 0 && row < _subtypeSchemes.size());
    Assert.expect(_subtypeSchemes != null);
    return (SubtypeScheme)_subtypeSchemes.get(row);
  }

  /**
   * @param subtypeSchemes SubtypeScheme
   * @return int
   *
   * @author Wei Chin, Chong
   */
  public int getRowForSubtypeScheme(SubtypeScheme subtypeSchemes)
  {
    Assert.expect(_subtypeSchemes != null);
    Assert.expect(subtypeSchemes != null);

    return _subtypeSchemes.indexOf(subtypeSchemes);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void clear()
  {
    _subtypeSchemes.clear();
    _selectedSubtypeSchemes.clear();
    _subtypeSchemes = null;
    _selectedSubtypeSchemes = null;
    _exportSubtypeLayoutTable = null;
    fireTableDataChanged();
  }

  /**
   * @param column int
   * @param ascending boolean
   * @author Wei Chin, Chong
   */
  public void sortColumn(int column, final boolean ascending)
  {
    if(_exportSubtypeLayoutTable != null)
      _exportSubtypeLayoutTable.stopCellsEditing();

    switch (column)
    {
      case ExportSubtypeLayoutTable._SELECT_INDEX: // selected or not
        Collections.sort(_subtypeSchemes, new Comparator<SubtypeScheme>()
        {
          public int compare(SubtypeScheme lhSubtypeScheme, SubtypeScheme rhSubtypeScheme)
          {
            if(ascending == false)
            {
              SubtypeScheme tempSubtypeScheme = lhSubtypeScheme;
              lhSubtypeScheme = rhSubtypeScheme;
              rhSubtypeScheme = tempSubtypeScheme;
            }
            if (_selectedSubtypeSchemes.contains(lhSubtypeScheme) &&
                     _selectedSubtypeSchemes.contains(rhSubtypeScheme) == false)
              return 1;
            else if (_selectedSubtypeSchemes.contains(lhSubtypeScheme) == false &&
                     _selectedSubtypeSchemes.contains(rhSubtypeScheme))
              return -1;
            else
              return lhSubtypeScheme.getPackageShortName().compareToIgnoreCase(rhSubtypeScheme.getPackageShortName());
          }
        });
        break;
      case ExportSubtypeLayoutTable._PACKAGE_INDEX: // package name
        Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.PACKAGE_NAME));
        break;
      case ExportSubtypeLayoutTable._JOINT_TYPE_INDEX: // joint type
        Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.JOINT_TYPE));
        break;
      case ExportSubtypeLayoutTable._SUBTYPE_TYPE_INDEX: // subtype
        Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.SUBTYPE_NAME));
        break;
      case ExportSubtypeLayoutTable._ADD_UPDATE_INDEX: // add | update
        Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.ADD_UPDATE));
        break;
      case ExportSubtypeLayoutTable._NEW_MODIFIED_INDEX: // new | modified
        Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.NEW_MODIFIED));
        break;
      case ExportSubtypeLayoutTable._SOURCE_LIBRARY_INDEX: // library path
        Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.EXPORT_LIBRARY_PATH));
        break;
      default:
        // do nothing
        break;
    }
  }

  /**
   * @return int
   * @author Wei Chin, Chong
   */
  public int getColumnCount()
  {
    Assert.expect(_tableColumns != null);
    return _tableColumns.length;
  }

  /**
   * @param columnIndex int
   * @return String
   * @author Wei Chin, Chong
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(_tableColumns != null);
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_tableColumns[columnIndex];
  }

  /**
   * @return int
   * @author Wei Chin, Chong
   */
  public int getRowCount()
  {
    if (_subtypeSchemes == null)
    {
      return 0;
    }
    else
    {
      return _subtypeSchemes.size();
    }
  }

  /**
   * @param rowIndex int
   * @param columnIndex int
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if (_subtypeSchemes == null || _subtypeSchemes.size() == 0)
    {
      return false;
    }

    switch (columnIndex)
    {
      case ExportSubtypeLayoutTable._SELECT_INDEX:
      {
        return true;
      }
      case ExportSubtypeLayoutTable._SUBTYPE_TYPE_INDEX:
      {
        // XCR-3102 Assert when import library with empty subtype name
        // Disable subtype name editing feature in Export Wizard. This could cause unexpected crash during import.
        return false;
      }
    }
    return false;
  }

  /**
   * @param object Object
   * @param rowIndex int
   * @param columnIndex int
   *
   * @author Wei Chin, Chong
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    SubtypeScheme subtypeScheme = _subtypeSchemes.get(rowIndex);

    switch (columnIndex)
    {
      case ExportSubtypeLayoutTable._SELECT_INDEX:
      {
        boolean selected = (Boolean)object;
        if (selected)
        {
          if (_selectedSubtypeSchemes.contains(subtypeScheme) == false)
          {
            _selectedSubtypeSchemes.add(subtypeScheme);
          }
        }
        else
        {
          if (_selectedSubtypeSchemes.contains(subtypeScheme) == true)
          {
            _selectedSubtypeSchemes.remove(subtypeScheme);
          }
        }
        fireTableCellUpdated(rowIndex, columnIndex);
        GuiObservable.getInstance().stateChanged(this, SelectionEventEnum.EXPORT_SUBTYPE_SELECTION);
        break;
      }
      case ExportSubtypeLayoutTable._SUBTYPE_TYPE_INDEX:
      {
        String newSubtypeName = (String)object;
        if (subtypeScheme.usesOneSubtype())
        {
          String oldSubtypename = subtypeScheme.hasNewSubTypeName()?subtypeScheme.getNewSubtypeName() : subtypeScheme.getSubtype().getShortName();
          if(oldSubtypename.equalsIgnoreCase(newSubtypeName) == false)
          {
            /**
             * after rename the subtype, the check sum of the subtype will changes,
             * if the subtype is selected before, we should remove it,
             * if not the old subtype will keep save and hide in the selected list which is not true
             */
            boolean isSelected = false;
            if (_selectedSubtypeSchemes.contains(subtypeScheme) == true)
            {
              _selectedSubtypeSchemes.remove(subtypeScheme);
              isSelected = true;
            }
            subtypeScheme.setNewSubtypeName(newSubtypeName);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                MessageDialog.showInformationDialog(null,
                                                    StringLocalizer.keyToString("PLWK_SUBTYPE_NAME_CHANGED_MESSAGE_OF_GUI_KEY"),
                                                    StringLocalizer.keyToString("MMGUI_PLWK_NAME_KEY"),
                                                    true);
              }
            });
            if(isSelected)
            {
              _selectedSubtypeSchemes.add(subtypeScheme);
              // make sure the selected subtype schemes is not greater than the total subtype schemes
              Assert.expect(_selectedSubtypeSchemes.size() <= _subtypeSchemes.size());
            }
            fireTableCellUpdated(rowIndex,columnIndex);
          }
        }
        break;
      }
      default:
        Assert.expect(false,"Edit this column is not implemented.");
    }
  }

  /**
   * @param rowIndex int
   * @param columnIndex int
   * @return Object
   *
   * @author Wei Chin, Chong
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_subtypeSchemes != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    SubtypeScheme subtypeScheme = _subtypeSchemes.get(rowIndex);
    //get the appropriate data item that corresponds to the column
    switch (columnIndex)
    {
      case ExportSubtypeLayoutTable._SELECT_INDEX:
      {
        value = new Boolean(_selectedSubtypeSchemes.contains(subtypeScheme));
        break;
      }
      case ExportSubtypeLayoutTable._PACKAGE_INDEX:
      {
        value = subtypeScheme.getPackageShortName();
        break;
      }
      case ExportSubtypeLayoutTable._SUBTYPE_TYPE_INDEX:
      {
        value = "";
        if (subtypeScheme.usesOneSubtype())
        {
          if (subtypeScheme.hasNewSubTypeName() == false)
          {
            value = subtypeScheme.getSubtype().getShortName();
          }
          else
          {
            value = subtypeScheme.getNewSubtypeName();
          }
        }
        else
        {
          value = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
        }
        break;
      }
      case ExportSubtypeLayoutTable._JOINT_TYPE_INDEX:
      {
        if (subtypeScheme.usesOneJointTypeEnum())
        {
          value = subtypeScheme.getJointTypeEnum().getName();
        }
        else
        {
          value = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
        }
        break;
      }
      case ExportSubtypeLayoutTable._NEW_MODIFIED_INDEX:
      {
        if(subtypeScheme.isImportedSubtypeScheme() == false)
        {
          value = StringLocalizer.keyToString("PLWK_EXPORT_NEW_TABLE_FIELD_KEY");
        }
        else if (subtypeScheme.isModifiedAfterImported())
        {
          value = StringLocalizer.keyToString("PLWK_EXPORT_MODIFIED_TABLE_FIELD_KEY");
        }
        else
        {
          value = StringLocalizer.keyToString("PLWK_EXPORT_IMPORTED_TABLE_FIELD_KEY");
        }
        break;
      }

      case ExportSubtypeLayoutTable._ADD_UPDATE_INDEX:
      {
        if (subtypeScheme.isUpdatable())
        {
          value = StringLocalizer.keyToString("PLWK_EXPORT_UPDATE_TABLE_FIELD_KEY");
        }
        else
        {
          value = StringLocalizer.keyToString("PLWK_EXPORT_ADD_TABLE_FIELD_KEY");
        }
        break;
      }
      case ExportSubtypeLayoutTable._SOURCE_LIBRARY_INDEX:
      {
        int lastIndex = subtypeScheme.getLibraryPath().lastIndexOf("\\");
        int length = subtypeScheme.getLibraryPath().length();

        if (lastIndex > 0 && (lastIndex + 1) != length)
          value = subtypeScheme.getLibraryPath().substring(lastIndex + 1);
        else
          value = subtypeScheme.getLibraryPath();
        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }
    return value;
  }

  /**
   * @return List
   *
   * @author Wei Chin, Chong
   */
  public List<SubtypeScheme> getSelectedSubtypeSchmes()
  {
    Assert.expect(_selectedSubtypeSchemes != null);

    return _selectedSubtypeSchemes;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void selectAllPackages()
  {
    for (int i = 0; i < getRowCount(); i++)
    {
      setValueAt(new Boolean(true), i, ExportSubtypeLayoutTable._SELECT_INDEX);
      fireTableCellUpdated(i, ExportPackageLayoutTable._SELECT_INDEX);
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  public void unselectAllPackages()
  {
    for (int i = 0; i < getRowCount(); i++)
    {
      setValueAt(new Boolean(false), i, ExportSubtypeLayoutTable._SELECT_INDEX);
      fireTableCellUpdated(i, ExportPackageLayoutTable._SELECT_INDEX);
    }
  }
}
