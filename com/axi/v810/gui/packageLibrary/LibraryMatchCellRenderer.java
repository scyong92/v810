package com.axi.v810.gui.packageLibrary;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.v810.business.packageLibrary.*;

public class LibraryMatchCellRenderer extends DefaultTableCellRenderer
{
  /**
   * @author Tan Hock Zoon
   */
  public LibraryMatchCellRenderer()
  {
    super();
  }

  /**
   * Returns the component used for drawing the cell.  This method is
   * used to configure the renderer appropriately before drawing.
   * see TableCellRenderer.getTableCellRendererComponent(...); for more comments on the method
   *
   * @author Tan Hock Zoon
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    ImportRefinePackageLayoutTable refinePackageTable = (ImportRefinePackageLayoutTable) table;
    ImportRefinePackageLayoutTableModel refinePackageTableModel = (ImportRefinePackageLayoutTableModel) refinePackageTable.getModel();

    SubtypeScheme subtypeScheme = refinePackageTableModel.getSubtypeSchemeAt(row);

    JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    label.setOpaque(true);

    if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false)
    {
      double percentage = subtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getMatchPercentage();

      if (percentage > 80)
      {
        label.setBackground(Color.GREEN);
      }
      else if (percentage > 60)
      {
        label.setBackground(Color.YELLOW);
      }
      else if (percentage > 40)
      {
        label.setBackground(Color.ORANGE);
      }
      else
      {
        label.setBackground(Color.RED);
      }
    }
    else
    {
      label.setBackground(Color.RED);
    }

    label.setForeground(Color.BLACK);

    return label;
  }

}
