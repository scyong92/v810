package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;

import javax.swing.table.*;

import com.axi.guiUtil.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class ImportAssignSubtypeComponentDetailLayoutTable extends JSortTable
{
  public static final int _PAD_NAME = 0;
  public static final int _JOINT_TYPE_INDEX = 1;
  public static final int _SUBTYPE_INDEX = 2;
  public static final int _LIBRARY_JOINT_TYPE_INDEX = 3;
  public static final int _LIBRARY_SUBTYPE_INDEX = 4;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _PAD_NAME_COLUMN_WIDTH_PERCENTAGE = .2;
  private static final double _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE = .2;
  private static final double _SUBTYPE_COLUMN_WIDTH_PERCENTAGE = .2;
  private static final double _LIBRARY_JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE = .2;
  private static final double _LIBRARY_SUBTYPE_COLUMN_WIDTH_PERCENTAGE = .2;

  /**
   * @author Wei Chin, Chong
   */
  public ImportAssignSubtypeComponentDetailLayoutTable()
  {
    //do nothing
  }

  /**
   * @author Wei Chin, Chong
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_PAD_NAME).setPreferredWidth((int)(totalColumnWidth * _PAD_NAME_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_JOINT_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_SUBTYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _SUBTYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_LIBRARY_JOINT_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _LIBRARY_JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_LIBRARY_SUBTYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _LIBRARY_SUBTYPE_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   *
   * @param e MouseEvent
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String getToolTipText(MouseEvent e)
  {
    Point p = e.getPoint();
    int row = rowAtPoint(p);
    int col = columnAtPoint(p);

    if (row >= 0 && col >= 0)
    {
      return getValueAt(row, col).toString();
    }
    else
    {
      return null;
    }
  }

}
