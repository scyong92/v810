package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.v810.business.packageLibrary.*;
import com.axi.util.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class LibraryCellRenderer extends DefaultTableCellRenderer
{
  /**
   * Creates a Custom JLabel Cell Renderer
   *
   * @author Wei Chin, Chong
   */
  public LibraryCellRenderer()
  {
    super();
  }

  /**
   * Returns the component used for drawing the cell.  This method is
   * used to configure the renderer appropriately before drawing.
   * see TableCellRenderer.getTableCellRendererComponent(...); for more comments on the method
   *
   * @author Wei Chin, Chong
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    String libraryPathValue = (String)value;

    JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    label.setOpaque(true);
    label.setToolTipText(libraryPathValue);

    String libraryFullPath = null;

    if ( table instanceof ExportPackageLayoutTable)
    {
      ExportPackageLayoutTable exportPackageLayoutTable = (ExportPackageLayoutTable)table;
      ExportPackageLayoutTableModel exportPackageLayoutTableModel = (ExportPackageLayoutTableModel)exportPackageLayoutTable.getModel();
      libraryFullPath = exportPackageLayoutTableModel.getCompPackagesAt(row).getLibraryPath();
    }
    else if(table instanceof ExportSubtypeLayoutTable)
    {
      ExportSubtypeLayoutTable exportSubtypeLayoutTable = (ExportSubtypeLayoutTable)table;
      ExportSubtypeLayoutTableModel exportSubtypeLayoutTableModel = (ExportSubtypeLayoutTableModel)exportSubtypeLayoutTable.getModel();
      libraryFullPath = exportSubtypeLayoutTableModel.getSubtypeSchemeAt(row).getLibraryPath();
    }

    Assert.expect(libraryFullPath != null);

    if( LibraryManager.doesLibraryExist( libraryFullPath ) == false)
    {
      if (isSelected)
      {
        label.setBackground(table.getSelectionBackground());
        label.setForeground(Color.WHITE);
      }
      else
      {
        label.setBackground(Color.RED);
        label.setForeground(Color.WHITE);
      }
    }
    else
    {
      if (isSelected)
      {
        label.setBackground(table.getSelectionBackground());
        label.setForeground(table.getSelectionForeground());
      }
      else
      {
        label.setBackground(table.getBackground());
        label.setForeground(table.getForeground());
      }
    }
    return label;
  }
}
