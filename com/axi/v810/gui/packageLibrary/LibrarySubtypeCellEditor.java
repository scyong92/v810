package com.axi.v810.gui.packageLibrary;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.util.*;

public class LibrarySubtypeCellEditor extends DefaultCellEditor
{

  public LibrarySubtypeCellEditor(JComboBox comboBox)
  {
    super (comboBox);
  }

  public java.awt.Component getTableCellEditorComponent(JTable table, Object value, boolean isSelectable, int row, int column)
  {
    java.awt.Component editor = super.getTableCellEditorComponent(table, value, isSelectable, row, column);
    JComboBox comboBox = (JComboBox)editor;
    comboBox.removeAllItems();
    comboBox.setRenderer(new LibrarySubtypeListCellRenderer());

    SubtypeScheme subtypeScheme = null;

    if( table instanceof ImportRefinePackageLayoutTable)
    {
      ImportRefinePackageLayoutTableModel refinePackageTableModel =(ImportRefinePackageLayoutTableModel)table.getModel();
      subtypeScheme = refinePackageTableModel.getSubtypeSchemeAt(row);
    }
    else if( table instanceof ImportAssignSubtypeComponentLayoutTable)
    {
      ImportAssignSubtypeComponentLayoutTableModel assignSubtypeComponentLayoutTableModel = (ImportAssignSubtypeComponentLayoutTableModel) table.getModel();
      subtypeScheme = assignSubtypeComponentLayoutTableModel.getSubtypeScheme();
    }

    Assert.expect(subtypeScheme != null);

    boolean hasMixedLibrarySubtypeScheme = subtypeScheme.getSelectedLibraryPackage().hasMixedLibrarySubtypeScheme(subtypeScheme.getComponentTypes());
    if (hasMixedLibrarySubtypeScheme)
      comboBox.addItem(StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY"));

    for (LibrarySubtypeScheme librarySubtypeScheme : subtypeScheme.getSelectedLibraryPackage().getLibrarySubtypeSchemesBySelectedJointType())
    {
      comboBox.addItem(librarySubtypeScheme);
    }
    
    comboBox.setSelectedItem(value);
    comboBox.setMaximumRowCount(30);
    return comboBox;
  }

}
