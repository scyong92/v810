package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;
import java.util.List;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.text.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: ImportWizardRefineImportPanel</p>
 *
 * <p>Description: This panel is to display the search criteria and record matches from the define criteria.</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Tan Hock Zoon
 * @version 1.0
 */
public class ImportWizardRefineImportPanel extends WizardPanel implements Observer
{
  private JPanel _mainPanel;
  private JPanel _contentPanel;
  private JPanel _packagesSelectionPanel;
  private JPanel _buttonPanel;
  private JPanel _refineImportDescPanel;
  private JPanel _drawCADPanel;
  private JPanel _landPatternPanel;
  private JPanel _jointTypePanel;
  private JPanel _landPatternNamePanel;
  private JPanel _matchCriteriaPanel;
  private JPanel _selectUnselectButtonPanel;
  private JPanel _mainButtonPanel;
  private JPanel _matchPanel;
  private JPanel _projectDrawCADPanel;
  private JPanel _libraryDrawCADPanel;

  private JButton _assignSubtypesToIndividualButton;
  private JButton _compareThreasholdButton;
  private JButton _importSettingButton;
  private JButton _selectAllButton;
  private JButton _unselectAllButton;
  private JButton _searchButton;

  private JLabel _refineImportDescLabel;
  private JLabel _percentMatchLabel;

  private DrawCadPanel _currentProjectCADPanel;
  private DrawCadPanel _libraryCADPanel;

  private ImportRefinePackageLayoutTable _refinePackageTable;
  private ImportRefinePackageLayoutTableModel _refinePackageTableModel;
  private JScrollPane _scrollPane;
  private TableColumn _matchPercentageTableColumn = null;
  private List<SubtypeScheme> _subtypeSchemes = null;
  private List<SubtypeScheme> _currentlySelectedSchemes = null;
  private List<String> _libraryPathList = null;

  private ImportWizardAssignSubtypeToIndividualComponent _assignSubtypeDialog;
  private CompareThresholdDialog _compareThresholdDialog;

  private JCheckBox _landPatternCheckBox;
  private JCheckBox _jointTypeCheckBox;
  private JCheckBox _landPatternNameCheckBox;
  
  private JCheckBox _currentProjectToggleCadDisplayCheckBox = new JCheckBox(StringLocalizer.keyToString("PLWK_TOGGLE_CAD_DISPLAY_KEY"));
  private JCheckBox _libraryProjectToggleCadDisplayCheckBox = new JCheckBox(StringLocalizer.keyToString("PLWK_TOGGLE_CAD_DISPLAY_KEY"));

  private NumericTextField _landPatternMatchPercentTextField;
  private JTextField _landPatternNameTextField;

  private JComboBox _landPatternNameFilterComboBox;
  private DefaultComboBoxModel _filterComboBoxModel = new DefaultComboBoxModel();

  private TitledBorder _titleBorder;
  private TitledBorder _projectTitleBorder;
  private TitledBorder _libraryTitleBorder;

  private SearchFilterPersistance _searchFilterPersistance;
  private ImportLibrarySettingPersistance _importLibrarySettingPersistance;
  private JSplitPane _splitPane;
  private GuiObservable _guiObservable;
  
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getPackageLibraryInstance();
    
  private LibraryWizardGeneralPersistance _libraryWizardGeneralPersistance;

  /**
   *
   * @param dialog
   * @param subtypeSchemes
   * @param currentlySelectedSchemes
   * @param libraryPathList
   * @param searchFilterPersistance
   * @param importLibrarySettingPersistance
   * @param libraryWizardGeneralPersistance
   *
   * @author Cheah Lee Herng
   * @author Tan Hock Zoon   
   */
  public ImportWizardRefineImportPanel(WizardDialog dialog, 
                                       List<SubtypeScheme> subtypeSchemes, 
                                       List<SubtypeScheme> currentlySelectedSchemes, 
                                       List<String> libraryPathList, 
                                       SearchFilterPersistance searchFilterPersistance,
                                       ImportLibrarySettingPersistance importLibrarySettingPersistance,
                                       LibraryWizardGeneralPersistance libraryWizardGeneralPersistance)
  {
    super(dialog);

    Assert.expect(subtypeSchemes != null);
    Assert.expect(currentlySelectedSchemes != null);
    Assert.expect(libraryPathList != null);
    Assert.expect(searchFilterPersistance != null);
    Assert.expect(importLibrarySettingPersistance != null);

    populateComboBoxModelData();
    _subtypeSchemes = subtypeSchemes;
    _currentlySelectedSchemes = currentlySelectedSchemes;
    _libraryPathList = libraryPathList;
    _guiObservable = GuiObservable.getInstance();
    _searchFilterPersistance = searchFilterPersistance;
    _importLibrarySettingPersistance = importLibrarySettingPersistance;
    
    _libraryWizardGeneralPersistance = libraryWizardGeneralPersistance;
  }

  /**
   * @author Tan Hock Zoon
   */
  private void init()
  {
    _guiObservable.addObserver(this);

    setLayout(new BorderLayout(5, 5));

    _currentlySelectedSchemes.clear();

    _mainPanel = new JPanel(new BorderLayout(5, 5));
    _contentPanel = new JPanel(new BorderLayout(5, 5));
    _packagesSelectionPanel = new JPanel(new BorderLayout(5, 5));
    _buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    _refineImportDescPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _drawCADPanel = new JPanel(new GridLayout(1, 2, 5, 5));
    _landPatternPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _jointTypePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _landPatternNamePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _matchCriteriaPanel = new JPanel(new BorderLayout(5, 5));
    _selectUnselectButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _mainButtonPanel = new JPanel(new GridLayout(1, 2, 5, 5));
    _matchPanel = new JPanel(new GridLayout(3, 1, 5, 5));
    _projectDrawCADPanel = new JPanel(new BorderLayout(5, 5));
    _libraryDrawCADPanel = new JPanel(new BorderLayout(5, 5));

    _assignSubtypesToIndividualButton = new JButton(StringLocalizer.keyToString("PLWK_IMPORT_ASSIGN_SUBTYPES_TO_INDIVIDUAL_KEY"));
    _compareThreasholdButton = new JButton(StringLocalizer.keyToString("PLWK_COMPARE_THRESHOLDS_KEY"));
    _importSettingButton = new JButton(StringLocalizer.keyToString("PLWK_IMPORT_SETTINGS_KEY"));
    _searchButton = new JButton(StringLocalizer.keyToString("PLWK_SEARCH_BUTTON_KEY"));
    _selectAllButton = new JButton(StringLocalizer.keyToString("GUI_SELECT_SELECT_ALL_KEY"));
    _unselectAllButton = new JButton(StringLocalizer.keyToString("PLWK_UNSELECT_ALL_KEY"));

    _refineImportDescLabel = new JLabel(StringLocalizer.keyToString("PLWK_IMPORT_REFINE_PACKAGES_IMPORT_DESC_KEY"));
    _percentMatchLabel = new JLabel(StringLocalizer.keyToString("PLWK_LAND_PATTERN_MATCH_PERCENT_KEY"));

    _currentProjectCADPanel = new DrawCadPanel(300, 150);
    _libraryCADPanel = new DrawCadPanel(300, 150);

    _refinePackageTableModel = new ImportRefinePackageLayoutTableModel(_currentlySelectedSchemes);
    _refinePackageTable = new ImportRefinePackageLayoutTable();

    _scrollPane = new JScrollPane();

    _landPatternCheckBox = new JCheckBox(StringLocalizer.keyToString("PLWK_LAND_PATTERN_GEOMETRY_KEY"));
    _jointTypeCheckBox = new JCheckBox(StringLocalizer.keyToString("ATGUI_FAMILY_KEY"));
    _landPatternNameCheckBox = new JCheckBox(StringLocalizer.keyToString("PLWK_LAND_PATTERN_NAME_KEY"));
    _landPatternMatchPercentTextField = new NumericTextField("", 3, new DecimalFormat("###"));

    NumericRangePlainDocument doc = new NumericRangePlainDocument();
    doc.setRange(new DoubleRange(1, 100));
    _landPatternMatchPercentTextField.setDocument(doc);

    _landPatternNameTextField = new JTextField(30);
    _landPatternNameTextField.setDocument(new PlainDocument()
    {
      private static final String ALPHA_NUMERIC = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";

      public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException
      {
        for (int i=0; i < str.length(); i++)
        {
          if (ALPHA_NUMERIC.indexOf(str.valueOf(str.charAt(i))) == -1)
            return;
        }

        super.insertString(offset, str, attr);
      }
    });

    _landPatternNameFilterComboBox = new JComboBox(_filterComboBoxModel);

    _titleBorder = BorderFactory.createTitledBorder(StringLocalizer.keyToString("PLWK_MATCH_CRITERIA_KEY"));
    _projectTitleBorder = BorderFactory.createTitledBorder(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY"));
    _libraryTitleBorder = BorderFactory.createTitledBorder(StringLocalizer.keyToString("PLWK_LIBRARY_PACKAGE_KEY"));

    _splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, _packagesSelectionPanel, _drawCADPanel);

    _splitPane.setResizeWeight(0.5);
  }

  /**
   * @author Tan Hock Zoon
   */
  private void createPanel()
  {
    removeAll();
    init();

    _matchCriteriaPanel.setBorder(_titleBorder);

    _landPatternCheckBox.setSelected(_searchFilterPersistance.isLandPatternGeometryMatchPercentageFilterOn());
    _landPatternMatchPercentTextField.setText(_searchFilterPersistance.isLandPatternGeometryMatchPercentageFilterOn() ? _searchFilterPersistance.getLandPatternGeometryMatchPercentage() + "" : "");

    _landPatternPanel.add(_landPatternCheckBox);
    _landPatternPanel.add(_landPatternMatchPercentTextField);
    _landPatternPanel.add(_percentMatchLabel);

    _jointTypeCheckBox.setSelected(_searchFilterPersistance.isJointTypeFilterOn());
    _jointTypePanel.add(_jointTypeCheckBox);

    _landPatternNameCheckBox.setSelected(_searchFilterPersistance.isLandPatternNameFilterOn());
    _landPatternNameFilterComboBox.setSelectedItem(_searchFilterPersistance.isLandPatternNameFilterOn() ? _searchFilterPersistance.getLandPatternNameSearchBy() : SearchOptionEnum.CONTAINS);
    _landPatternNameTextField.setText(_searchFilterPersistance.isLandPatternNameFilterOn() ? _searchFilterPersistance.getLandPatternName() : "");

    _landPatternNamePanel.add(_landPatternNameCheckBox);
    _landPatternNamePanel.add(_landPatternNameFilterComboBox);
    _landPatternNamePanel.add(_landPatternNameTextField);

    _matchPanel.add(_landPatternPanel);
    _matchPanel.add(_jointTypePanel);
    _matchPanel.add(_landPatternNamePanel);

    _matchCriteriaPanel.add(_matchPanel, BorderLayout.CENTER);
    _matchCriteriaPanel.add(_searchButton, BorderLayout.EAST);

    _buttonPanel.add(_assignSubtypesToIndividualButton);
    _buttonPanel.add(_compareThreasholdButton);
    _buttonPanel.add(_importSettingButton);

    _assignSubtypesToIndividualButton.setEnabled(false);
    _compareThreasholdButton.setEnabled(false);
    _importSettingButton.setEnabled(false);

    _refinePackageTable.setModel(_refinePackageTableModel);
    _matchPercentageTableColumn = _refinePackageTable.getColumn(_refinePackageTable.getColumnName(ImportRefinePackageLayoutTable._MATCHES_INDEX));
    _scrollPane.getViewport().add(_refinePackageTable, null);
    _scrollPane.getViewport().setOpaque(false);

    _selectUnselectButtonPanel.add(_selectAllButton);
    _selectUnselectButtonPanel.add(_unselectAllButton);

    _selectAllButton.setEnabled(false);
    _unselectAllButton.setEnabled(false);

    _mainButtonPanel.add(_selectUnselectButtonPanel);
    _mainButtonPanel.add(_buttonPanel);

    _packagesSelectionPanel.add(_scrollPane, BorderLayout.CENTER);
    _packagesSelectionPanel.add(_mainButtonPanel, BorderLayout.SOUTH);

    _projectDrawCADPanel.setBorder(_projectTitleBorder);
    _projectDrawCADPanel.add(_currentProjectCADPanel, BorderLayout.CENTER);
    _projectDrawCADPanel.add(_currentProjectToggleCadDisplayCheckBox, BorderLayout.SOUTH);

    _libraryDrawCADPanel.setBorder(_libraryTitleBorder);
    _libraryDrawCADPanel.add(_libraryCADPanel, BorderLayout.CENTER);
    _libraryDrawCADPanel.add(_libraryProjectToggleCadDisplayCheckBox, BorderLayout.SOUTH);

    _drawCADPanel.add(_projectDrawCADPanel);
    _drawCADPanel.add(_libraryDrawCADPanel);

    _refineImportDescPanel.setBackground(Color.WHITE);
    _refineImportDescPanel.add(_refineImportDescLabel);

    _contentPanel.add(_matchCriteriaPanel, BorderLayout.NORTH);
    _contentPanel.add(_splitPane, BorderLayout.CENTER);

    _mainPanel.add(_refineImportDescPanel, BorderLayout.NORTH);
    _mainPanel.add(_contentPanel, BorderLayout.CENTER);

    add(_mainPanel, BorderLayout.CENTER);

    _assignSubtypesToIndividualButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        assignSubtypesToIndividualButton_actionPerformed();
      }
    });

    _refinePackageTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    ListSelectionModel rowSM = _refinePackageTable.getSelectionModel();
    rowSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        TableColumnModel tableColumnModel = _refinePackageTable.getColumnModel();

        tableColumnModel.getColumn(ImportRefinePackageLayoutTable._LIBRARY_PATH_INDEX).getCellEditor().cancelCellEditing();
        tableColumnModel.getColumn(ImportRefinePackageLayoutTable._LIBRARY_PACKAGE_INDEX).getCellEditor().cancelCellEditing();
        tableColumnModel.getColumn(ImportRefinePackageLayoutTable._LIBRARY_JOINT_TYPE_INDEX).getCellEditor().cancelCellEditing();
        tableColumnModel.getColumn(ImportRefinePackageLayoutTable._LIBRARY_SUBTYPE_INDEX).getCellEditor().cancelCellEditing();

        ListSelectionModel lsm = (ListSelectionModel)e.getSource();

        if (lsm.getValueIsAdjusting() == false)
        {
          // we want to select the pad(s) in the graphics window corresponding to the newly selected table row(s)
          updateCurrentProjectCadDisplay();
          updateLibraryProjectCadDisplay();

          int rowSelected = _refinePackageTableModel.getRowForSubtypeScheme(_refinePackageTableModel.getSelectedSubtypeScheme());
          _refinePackageTable.setRowSelectionInterval(rowSelected, rowSelected);
        }

        revalidate();
        repaint();
      }
    });

    _landPatternCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        validateSearchPanel();
      }
    });

    _jointTypeCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        validateSearchPanel();
      }
    });

    _landPatternNameCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        validateSearchPanel();
      }
    });

    _searchButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        searchButton_actionPerformed();
      }
    });

    _selectAllButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        _refinePackageTableModel.selectAllPackages();
        validatePanel();
      }
    });

    _unselectAllButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        _refinePackageTableModel.unselectAllPackages();
        validatePanel();
      }
    });

    _compareThreasholdButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        compareThresholdButton_actionPerformed();
      }
    });
    
    _importSettingButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        importSettingsButton_actionPerformed();
      }
    });
    
    _currentProjectToggleCadDisplayCheckBox.setSelected(_libraryWizardGeneralPersistance.getImportRefinePackageCurrentProjectCadDisplay());
    _currentProjectToggleCadDisplayCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if(_currentProjectToggleCadDisplayCheckBox.isSelected())
        {
          _libraryWizardGeneralPersistance.setImportRefinePackageCurrentProjectCadDisplay(true);
          updateCurrentProjectCadDisplay();
        }
        else
        {
          _libraryWizardGeneralPersistance.setImportRefinePackageCurrentProjectCadDisplay(false);
          updateCurrentProjectCadDisplay();
        }
      }
    });
    
    _libraryProjectToggleCadDisplayCheckBox.setSelected(_libraryWizardGeneralPersistance.getImportRefinePackageLibraryProjectCadDisplay());
    _libraryProjectToggleCadDisplayCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if(_libraryProjectToggleCadDisplayCheckBox.isSelected())
        {
          _libraryWizardGeneralPersistance.setImportRefinePackageLibraryProjectCadDisplay(true);
          updateLibraryProjectCadDisplay();
        }
        else
        {
          _libraryWizardGeneralPersistance.setImportRefinePackageLibraryProjectCadDisplay(false);
          updateLibraryProjectCadDisplay();
        }
      }
    });

    SwingUtils.setTableAndScrollPanelToPreferredSize(_scrollPane, _refinePackageTable);
  }

  /**
   * @author Tan Hock Zoon
   */
  public void validatePanel()
  {
    WizardButtonState buttonState;

    if (_refinePackageTableModel.getSelectedPackages().size() != 0)
    {
      buttonState = new WizardButtonState(true, true, false, true);
    }
    else
    {
      buttonState = new WizardButtonState(false, true, false, true);
    }

    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Tan Hock Zoon
   */
  public void validateSearchPanel()
  {

    _landPatternMatchPercentTextField.setEditable(_landPatternCheckBox.isSelected());
    _landPatternNameFilterComboBox.setEnabled(_landPatternNameCheckBox.isSelected());
    _landPatternNameTextField.setEditable(_landPatternNameCheckBox.isSelected());

    if (_landPatternCheckBox.isSelected() || _jointTypeCheckBox.isSelected() || _landPatternNameCheckBox.isSelected())
    {
      _searchButton.setEnabled(true);
      _searchButton.setToolTipText("");
      
      _importSettingButton.setEnabled(true);
    }
    else
    {
      _searchButton.setEnabled(false);
      _searchButton.setToolTipText(StringLocalizer.keyToString("PLWK_AT_LEAST_ONE_OPTION_SELECTED_KEY"));
      
      _importSettingButton.setEnabled(false);
    }
  }

  /**
   * @author Tan Hock Zoon
   */
  public void populatePanelWithData()
  {
    createPanel();

    _dialog.setTitle(StringLocalizer.keyToString("PLWK_IMPORT_REFINE_SEARCH_LIBRARY_KEY"));

    validatePanel();
    validateSearchPanel();
  }

  /**
   * @author Tan Hock Zoon
   */
  public void unpopulate()
  {
    if (_currentProjectCADPanel != null)
    {
      _currentProjectCADPanel.resetPackageGraphics();
      _currentProjectCADPanel.dispose();
    }

    if (_libraryCADPanel != null)
    {
      _libraryCADPanel.resetLibraryPackageGraphics();
      _libraryCADPanel.dispose();
    }

    if (_refinePackageTableModel != null)
      _refinePackageTableModel.clear();

    if (_refinePackageTable != null)
      _refinePackageTable.removeAll();

    if (_searchFilterPersistance != null)
      _searchFilterPersistance.clearFilter();

    _refinePackageTableModel = null;
    _refinePackageTable = null;
    _libraryPathList = null;
    _searchFilterPersistance = null;
    _subtypeSchemes = null;
    _currentlySelectedSchemes = null;
    _assignSubtypeDialog = null;
    _compareThresholdDialog = null;
    _currentProjectCADPanel = null;
    _libraryCADPanel = null;

    removeAll();
  }

  /**
   * @author Tan Hock Zoon
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Tan Hock Zoon
   */
  public String getWizardName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author Tan Hock Zoon
   */
  public void removeObservers()
  {
    _guiObservable.deleteObserver(this);
  }

  /**
   * @author Tan Hock Zoon
   */
  private void assignSubtypesToIndividualButton_actionPerformed()
  {
    _assignSubtypeDialog = new ImportWizardAssignSubtypeToIndividualComponent((Frame)_dialog.getParent(), true);
    _assignSubtypeDialog.populatePanelWithData(_refinePackageTableModel.getSubtypeSchemeAt(_refinePackageTable.getSelectedRow()));
    _assignSubtypeDialog.pack();
    SwingUtils.centerOnComponent(_assignSubtypeDialog, this);
    _assignSubtypeDialog.setVisible(true);
    _assignSubtypeDialog.dispose();
  }

  /**
   * @author Tan Hock Zoon
   */
  private void populateComboBoxModelData()
  {
    _filterComboBoxModel.addElement(SearchOptionEnum.CONTAINS);
    _filterComboBoxModel.addElement(SearchOptionEnum.START_WITH);
    _filterComboBoxModel.addElement(SearchOptionEnum.END_WITH);
  }

  /**
   *
   * @author Tan Hock Zoon
   */
  private void searchButton_actionPerformed()
  {
    if (validateSearchOption())
    {
      _refinePackageTableModel.clear();

      for (SubtypeScheme subtypeScheme : _subtypeSchemes)
      {
        subtypeScheme.clear();
      }

      LibraryUtil.getInstance().setCancelDatabaseOperation(false);

      searchSubtypeSchemeBySearchCriteria();

      if (LibraryUtil.getInstance().cancelDatabaseOperation() == false)
      {
        for (SubtypeScheme subtypeScheme : _subtypeSchemes)
        {
          if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false)
          {
            subtypeScheme.sortLibraryPathListAndSet();

            if (subtypeScheme.getLibraryPackageList().size() > 0)
            {
              subtypeScheme.sortLibraryPackageAndSet();
              subtypeScheme.getSelectedLibraryPackage().assignComponentType(subtypeScheme.getComponentTypes());
            }
          }
        }

        populatePanelWithFilteringData();
        validatePanel();

        _currentProjectCADPanel.resetPackageGraphics();
        _libraryCADPanel.resetLibraryPackageGraphics();
        
        if (_refinePackageTable.getModel().getRowCount() > 0)
          _refinePackageTable.setRowSelectionInterval(0, 0);

        try
        {
          _searchFilterPersistance.writeSettings();
          _importLibrarySettingPersistance.writeSettings();
        }
        catch (DatastoreException de)
        {
          MessageDialog.showErrorDialog(null,
                                        StringLocalizer.keyToString("PLWK_ERROR_WRITING_PATH_TO_FILE_KEY"),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                        true);
        }
      }
    }

    validate();
    repaint();
  }

  /**
   * @author Tan Hock Zoon
   */
  private void populatePanelWithFilteringData()
  {
    _refinePackageTableModel.populateWithPanelData(_subtypeSchemes, _searchFilterPersistance);
    _refinePackageTableModel.fireTableDataChanged();

    if (_refinePackageTableModel.getRowCount() > 0)
    {
      _selectAllButton.setEnabled(true);
      _unselectAllButton.setEnabled(true);
    }

    _refinePackageTable.setModel(_refinePackageTableModel);

    _refinePackageTable.setEditorsAndRenderers();
    _refinePackageTable.setPreferredColumnWidths();
  }

  /**
   * @author Tan Hock Zoon
   */
  private void searchSubtypeSchemeBySearchCriteria()
  {
    /** @todo APM Code Review Shouldn't this operation be cancelable?  I don't know how difficult it would be to put a cancel operation in, but we should think about it. */
    final BusyCancelDialog busyDialog = new BusyCancelDialog(
        (Frame)_dialog.getParent(),
        StringLocalizer.keyToString("PLWK_IMPORT_ACCESS_DATABASE_WAITING_KEY"),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        StringLocalizer.keyToString("PLWK_CANCEL_BUTTON_KEY"),
        true);
    busyDialog.pack();
    busyDialog.addCancelActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        LibraryUtil.getInstance().setCancelDatabaseOperation(true);
      }
    });

    SwingUtils.centerOnComponent(busyDialog, (Frame)_dialog.getParent());
    SwingWorkerThread.getInstance3().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          LibraryUtil.getInstance().getRefineMatchLibrarySubtypeSchemeBySubtypeSchemes(_subtypeSchemes, _libraryPathList, _searchFilterPersistance, _importLibrarySettingPersistance);

          try
          {
            // now lets sleep for 2 seconds so that we can make sure the dialog doesn't just flash.
            Thread.sleep(1000);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }
        }
        finally
        {
          // wait until visible
          while (busyDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          try
          {
            SwingUtils.invokeAndWait(new Runnable()
            {
              public void run()
              {
                busyDialog.dispose();
              }
            });
          }
          catch (InterruptedException ie)
          {
            // do nothing
          }
          catch (java.lang.reflect.InvocationTargetException ite)
          {
            // do nothing
          }
        }
      }
    });
    busyDialog.setVisible(true);
  }

  /**
   *
   * @return boolean
   */
  private boolean validateSearchOption()
  {
    boolean proceed = true;
    String errorMessage = "";

    _searchFilterPersistance.clearFilter();

    if (_landPatternCheckBox.isSelected())
    {
      if (_landPatternMatchPercentTextField.getText().trim().equals(""))
      {
        errorMessage = StringLocalizer.keyToString(new LocalizedString("PLWK_TEXT_FIELD_EMPTY_KEY",
                                                                       new Object[]{StringLocalizer.keyToString("PLWK_LAND_PATTERN_GEOMETRY_KEY")}));

        errorMessage += "\n";
        proceed = false;
      }
      else
      {
        _searchFilterPersistance.setLandPatternGeometryMatchPercentage(Integer.parseInt(_landPatternMatchPercentTextField.getText().trim()));
      }
    }
    else
    {
      //zero is use to indicate this fields is not part of the search criteria
      _searchFilterPersistance.setLandPatternGeometryMatchPercentage(0);
    }

    if (_jointTypeCheckBox.isSelected())
    {
      _searchFilterPersistance.setJointTypeFilter(true);
    }
    else
    {
      _searchFilterPersistance.setJointTypeFilter(false);
    }

    if (_landPatternNameCheckBox.isSelected())
    {
      _searchFilterPersistance.setLandPatternNameFilter(_landPatternNameTextField.getText(), (SearchOptionEnum) _landPatternNameFilterComboBox.getSelectedItem());
      _searchFilterPersistance.setLandPatternNameFilter(true);
    }
    else
    {
      _searchFilterPersistance.setLandPatternNameFilter("%", (SearchOptionEnum) _landPatternNameFilterComboBox.getItemAt(0));
      _searchFilterPersistance.setLandPatternNameFilter(false);
    }

    if (proceed == false)
    {
      JOptionPane.showMessageDialog(this,
                                    errorMessage,
                                    "Warning",
                                    JOptionPane.WARNING_MESSAGE);

    }

    return proceed;
  }

  /**
   *
   * @author Tan Hock Zoon
   */
  private void compareThresholdButton_actionPerformed()
  {
    _compareThresholdDialog = new CompareThresholdDialog((Frame)_dialog.getParent(), true);
    _compareThresholdDialog.populateDialogWithData(_refinePackageTableModel.getSubtypeSchemeAt(_refinePackageTable.getSelectedRow()));
    _compareThresholdDialog.pack();
    SwingUtils.centerOnComponent(_compareThresholdDialog, this);
    _compareThresholdDialog.setVisible(true);
    _compareThresholdDialog.dispose();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void importSettingsButton_actionPerformed()
  {
    Map<String, Boolean> checkBoxOptionToIntegerChoiceMap = new TreeMap<String, Boolean>();
    java.util.List<ImportLibrarySettingEnum> importLibrarySettingEnumList = ImportLibrarySettingEnum.getAllImportLibrarySettingList();
    for(ImportLibrarySettingEnum importLibrarySettingEnum : importLibrarySettingEnumList)
    {
      Boolean previousEntry = checkBoxOptionToIntegerChoiceMap.put(importLibrarySettingEnum.toString(), 
                                                                   _importLibrarySettingPersistance.getImportLibrarySettingStatus(importLibrarySettingEnum));
      Assert.expect(previousEntry == null);
    }
    
    ChoiceCheckBoxDialog importLibrarySettingChoiceCheckBoxDialog = new ChoiceCheckBoxDialog(MainMenuGui.getInstance(),
                                                                                             StringLocalizer.keyToString("PLWK_IMPORT_LIBRARY_SETTING_TITLE_KEY"),
                                                                                             StringLocalizer.keyToString("PLWK_IMPORT_LIBRARY_SETTING_OPTION_TITLE_KEY"),
                                                                                             StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                                                                             StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                                                                             checkBoxOptionToIntegerChoiceMap);
    
    Map<String, Boolean> _checkBoxOptionToIntegerChoiceSettingsMap = new HashMap<String, Boolean>();
    int retVal = importLibrarySettingChoiceCheckBoxDialog.showDialog();
    if (retVal == JOptionPane.OK_OPTION)
    {
      _checkBoxOptionToIntegerChoiceSettingsMap = importLibrarySettingChoiceCheckBoxDialog.getSelectedValue();
      _commandManager.beginCommandBlock(StringLocalizer.keyToString(new LocalizedString("PLWK_CHANGE_IMPORT_LIBRARY_SETTING_OPTION_KEY", null)));
      try
      {
        _commandManager.execute(new SetImportLibrarySettingCommand(_importLibrarySettingPersistance, 
                                                                   ImportLibrarySettingEnum.IMPORT_THRESHOLD, 
                                                                   _checkBoxOptionToIntegerChoiceSettingsMap.get(ImportLibrarySettingEnum.IMPORT_THRESHOLD.toString())));
        _commandManager.execute(new SetImportLibrarySettingCommand(_importLibrarySettingPersistance, 
                                                                   ImportLibrarySettingEnum.IMPORT_NOMINAL, 
                                                                   _checkBoxOptionToIntegerChoiceSettingsMap.get(ImportLibrarySettingEnum.IMPORT_NOMINAL.toString())));
        _commandManager.execute(new SetImportLibrarySettingCommand(_importLibrarySettingPersistance, 
                                                                   ImportLibrarySettingEnum.IMPORT_LANDPATTERN, 
                                                                   _checkBoxOptionToIntegerChoiceSettingsMap.get(ImportLibrarySettingEnum.IMPORT_LANDPATTERN.toString())));
        _commandManager.execute(new SetImportLibrarySettingCommand(_importLibrarySettingPersistance, 
                                                                   ImportLibrarySettingEnum.IMPORT_LIBRARY_SUBTYPE_NAME, 
                                                                   _checkBoxOptionToIntegerChoiceSettingsMap.get(ImportLibrarySettingEnum.IMPORT_LIBRARY_SUBTYPE_NAME.toString())));
        
        // Save the preference
        _importLibrarySettingPersistance.writeSettings();
      }
      catch (XrayTesterException ex) 
      {
        System.out.println("Import library setting error.");
      }   
      finally
      {
        _commandManager.endCommandBlock();
      }
    }
    checkBoxOptionToIntegerChoiceMap.clear();
  }

  /**
   * @param observable Observable
   * @param object Object
   * @author Tan Hock Zoon
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof GuiObservable)
        {
          GuiEvent guiEvent = (GuiEvent)object;
          GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
          if (guiEventEnum instanceof SelectionEventEnum)
          {
            SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
            if (selectionEventEnum.equals(SelectionEventEnum.IMPORT_REFINE_PACKAGES_SELECTION))
            {
              validatePanel();
            }
            else if (selectionEventEnum.equals(SelectionEventEnum.IMPORT_REFINE_PACKAGES_SELECTION_CHANGE))
            {
              updateLibraryProjectCadDisplay();
            }
          }
        }
      }
    });
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void updateCurrentProjectCadDisplay()
  {
    if (_currentProjectCADPanel != null)
    {
      // we want to select the pad(s) in the graphics window corresponding to the newly selected table row(s)
      int selectedRows = _refinePackageTable.getSelectedRow();
      if (selectedRows >= 0)
      {
        if (_libraryWizardGeneralPersistance.getImportRefinePackageCurrentProjectCadDisplay() == true)
        {
          _currentProjectCADPanel.resetPackageGraphics();
          CompPackage compPackage = _refinePackageTableModel.getSubtypeSchemeAt(selectedRows).getCompPackage();
          _currentProjectCADPanel.drawPackage(compPackage);
          _currentProjectCADPanel.scaleDrawing();
          _projectTitleBorder.setTitle(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY") + compPackage.getShortName());
        }
        else
        {
           _currentProjectCADPanel.resetPackageGraphics();
          _projectTitleBorder.setTitle(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY"));
        }
      }
      else
      {
        _currentProjectCADPanel.resetPackageGraphics();
        _projectTitleBorder.setTitle(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY"));
      }
      
      revalidate();
      repaint();
    }    
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void updateLibraryProjectCadDisplay()
  {
    if (_libraryCADPanel != null)
    {     
      // we want to select the pad(s) in the graphics window corresponding to the newly selected table row(s)
      int selectedRows = _refinePackageTable.getSelectedRow();
      if (selectedRows >= 0)
      {
        SubtypeScheme subtypeScheme = _refinePackageTableModel.getSubtypeSchemeAt(selectedRows);
        _refinePackageTableModel.setSelectedSubtypeScheme(subtypeScheme);

        if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false)
        {
          if (_libraryWizardGeneralPersistance.getImportRefinePackageLibraryProjectCadDisplay() == true)
          {
            _libraryCADPanel.resetLibraryPackageGraphics();
            LibraryUtil.getInstance().getLibraryPackagePinDetails(subtypeScheme.getSelectedLibraryPath(), subtypeScheme.getSelectedLibraryPackage());
            _libraryCADPanel.drawLibraryPackage(subtypeScheme.getSelectedLibraryPackage());
            _libraryCADPanel.scaleDrawing();
            _libraryTitleBorder.setTitle(StringLocalizer.keyToString("PLWK_LIBRARY_PACKAGE_KEY") + subtypeScheme.getSelectedLibraryPackage().getPackageName());
          }
          else
          {
            _libraryCADPanel.resetLibraryPackageGraphics();
            _libraryTitleBorder.setTitle(StringLocalizer.keyToString("PLWK_LIBRARY_PACKAGE_KEY"));
          }

          updateButtonStatus(true);
        }
        else
        {
          _libraryCADPanel.resetLibraryPackageGraphics();
          _libraryTitleBorder.setTitle(StringLocalizer.keyToString("PLWK_LIBRARY_PACKAGE_KEY"));
          updateButtonStatus(false);
        }
      }
      else
      {
        _libraryCADPanel.resetLibraryPackageGraphics();
        _libraryTitleBorder.setTitle(StringLocalizer.keyToString("PLWK_LIBRARY_PACKAGE_KEY"));
        updateButtonStatus(false);
      }
      
      revalidate();
      repaint();
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void updateButtonStatus(boolean enable)
  {
    _assignSubtypesToIndividualButton.setEnabled(enable);
    _compareThreasholdButton.setEnabled(enable);
    _importSettingButton.setEnabled(enable);
  }
}
