package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class CompareThresholdDialog extends EscapeDialog
{
  private BorderLayout _mainBorderLayout;
  private PairLayout _pairLayout;

  private JLabel _projectJointTypeLabel;
  private JLabel _subtypeLabel;
  private JLabel _librarySubtypeLabel;
  private JLabel _currentLibraryLabel;
  private JLabel _libraryJointTypeLabel;
  private JLabel _projectSubtypeCommentLabel;
  private JLabel _librarySubtypeCommentLabel;
  private JLabel _projectThresholdCommentLabel;
  private JLabel _libraryThresholdCommentLabel;

  private JTextField _librarySubtypeCommentTextField;
  private JTextField _libraryThresholdCommentTextField;
  private JTextField _projectSubtypeCommentTextField;
  private JTextField _projectThresholdCommentTextField;
  private JTextField _currentLibraryTextField;
  private JTextField _jointTypeTextField;
  private JTextField _subtypeTextField;
  private JTextField _librarySubtypeTextField;
  private JTextField _libraryJointTypeTextField;

  private JTabbedPane _thresholdsTabbedPane;

  private JPanel _northPanel;
  private JPanel _northDetailedPanel;
  private JPanel _selectionPanel;
  private JPanel _subtypeAdvanceSettingPanel;
//  private JPanel _thresholdsOptionPanel;
  private JPanel _subtypeCommentPanel;
//  private JPanel _currentLibraryPanel;
  private JPanel _thresholdCommentPanel;
  private JPanel _centerPanel;
  private JPanel _centerTabbedPanel;
  private JPanel _southPanel;
  private JPanel _mainPanel;

  private VariableWidthComboBox _jointTypeComboBox;
  private VariableWidthComboBox _subtypeComboBox;
  private VariableWidthComboBox _librarySubtypeComboBox;
  
  private JScrollPane _compareSubtypeScrollPane = new JScrollPane();
  private CompareSubtypeTable _compareSubtypeTable;
  private CompareSubtypeTableModel _compareSubtypeTableModel;

  private ButtonGroup _thresholdTypeButtonGroup;
  private JRadioButton _allThresholdsRadioButton;
  private JRadioButton _onlyDifferentThresholdsRadioButton;
  private JButton _doneButton;

  private int _currentSelectedAlgorithmTab;  // holds the current tab (used when changing tabs to keep track of the old one)

  private java.util.List <SubtypeScheme> _subtypeSchemes;
  private Map<Subtype, LibrarySubtype> _projectSubtypeToLibrarySubtype = new HashMap<Subtype,LibrarySubtype>();
  private String _currentlibraryPath = null;

  private boolean _importThreshold = false;

  /**
   * @param parentFrame Frame
   * @param modal boolean
   * @author Wei Chin, Chong
   */
  CompareThresholdDialog(Frame parentFrame, boolean modal)
  {
    super(parentFrame, modal);
    createPanel();
  }

  /**
   * @author Wei Chin, Chong
   */
  private void init()
  {
    _mainPanel = new JPanel();
    _mainBorderLayout = new BorderLayout(5, 10);
    _mainPanel.setLayout(_mainBorderLayout);

    _pairLayout = new PairLayout(5,10,false);
    setTitle(StringLocalizer.keyToString("PLWK_COMPARE_THRESHOLDS_KEY"));
    _projectJointTypeLabel = new JLabel(" " + StringLocalizer.keyToString("PLWK_EXPORT_JOINT_TYPE_KEY"));
    _subtypeLabel = new JLabel(" " + StringLocalizer.keyToString("PLWK_EXPORT_SUBTYPES_KEY"));

    _libraryJointTypeLabel = new JLabel(" " + StringLocalizer.keyToString("PLWK_EXPORT_JOINT_TYPE_KEY"));
    _librarySubtypeLabel = new JLabel(" " + StringLocalizer.keyToString("PLWK_LIBRARY_SUBTYPES_KEY"));

    _currentLibraryLabel = new JLabel(" " + StringLocalizer.keyToString("PLWK_CT_CURRENT_LIBRARY_KEY"));
    _projectSubtypeCommentLabel = new JLabel(" " + StringLocalizer.keyToString("PLWK_CT_PROJECT_SUBTYPE_COMMENT_KEY"));
    _librarySubtypeCommentLabel = new JLabel(" " + StringLocalizer.keyToString("PLWK_CT_LIBRARY_SUBTYPE_COMMENT_KEY"));
    _projectThresholdCommentLabel = new JLabel(" " + StringLocalizer.keyToString("PLWK_CT_PROJECT_THRESHOLD_COMMENT_KEY"));
    _libraryThresholdCommentLabel = new JLabel(" " + StringLocalizer.keyToString("PLWK_CT_LIBRARY_THRESHOLD_COMMENT_KEY"));

    _librarySubtypeCommentTextField = new JTextField(40);
    _libraryThresholdCommentTextField = new JTextField(40);
    _projectSubtypeCommentTextField = new JTextField(40);
    _projectThresholdCommentTextField = new JTextField(40);
    _currentLibraryTextField = new JTextField(40);
    _jointTypeTextField = new JTextField(20);
    _subtypeTextField = new JTextField(20);
    _librarySubtypeTextField = new JTextField(20);
    _libraryJointTypeTextField = new JTextField(20);

    _jointTypeComboBox = new VariableWidthComboBox();
    _subtypeComboBox = new VariableWidthComboBox();
    _librarySubtypeComboBox = new VariableWidthComboBox();

    _thresholdTypeButtonGroup = new ButtonGroup();
    _allThresholdsRadioButton = new JRadioButton(StringLocalizer.keyToString("PLWK_CT_ALL_THRESHOLDS_KEY"));
    _onlyDifferentThresholdsRadioButton = new JRadioButton(StringLocalizer.keyToString("PLWK_CT_ONLY_DIFFERENT_THRESHOLDS_KEY"));
    _doneButton = new JButton(StringLocalizer.keyToString("PLWK_OK_BUTTON_KEY"));

    _thresholdsTabbedPane = new JTabbedPane();
    _thresholdsTabbedPane.setPreferredSize(new Dimension(150, 400));
    _northPanel = new JPanel(new BorderLayout(10, 5));
    _northDetailedPanel = new JPanel(new BorderLayout(10, 5));
    _selectionPanel = new JPanel(_pairLayout);
    _subtypeAdvanceSettingPanel = new JPanel(new BorderLayout(10, 5));
    _subtypeAdvanceSettingPanel.setPreferredSize(new Dimension(150, 150));
//    _thresholdsOptionPanel = new JPanel( new FlowLayout(FlowLayout.LEFT,5,5));
//    _currentLibraryPanel = new JPanel( new FlowLayout(FlowLayout.LEFT,5,5));

    _compareSubtypeTableModel = new CompareSubtypeTableModel();
    _compareSubtypeTable = new CompareSubtypeTable(_compareSubtypeTableModel);    
    
    _centerPanel = new JPanel(new BorderLayout(5, 5));
    _centerTabbedPanel = new JPanel(new BorderLayout(5, 5));
    _southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    _subtypeCommentPanel = new JPanel(_pairLayout);
    _thresholdCommentPanel = new JPanel(_pairLayout);
    SwingUtils.makeAllSameLength(new JLabel[]{_projectJointTypeLabel,_subtypeLabel,_librarySubtypeLabel, _libraryJointTypeLabel,
    _currentLibraryLabel, _projectSubtypeCommentLabel, _librarySubtypeCommentLabel, _projectThresholdCommentLabel, _libraryThresholdCommentLabel});
  }

  /**
   * @author Wei Chin, Chong
   */
  private void createPanel()
  {
    init();
//    setLayout(_mainBorderLayout);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

//    _selectionPanel.add(_jointTypeLabel);
//    _selectionPanel.add(_jointTypeComboBox);
//    _selectionPanel.add(_subtypeLabel);
//    _selectionPanel.add(_subtypeComboBox);
        
    _compareSubtypeScrollPane.getViewport().add(_compareSubtypeTable, null);        
    _subtypeAdvanceSettingPanel.add(_compareSubtypeScrollPane);

    _subtypeCommentPanel.add(_currentLibraryLabel);
    _subtypeCommentPanel.add(_currentLibraryTextField);

    _subtypeCommentPanel.add(_projectSubtypeCommentLabel);
    _subtypeCommentPanel.add(_projectSubtypeCommentTextField);
    _subtypeCommentPanel.add(_librarySubtypeCommentLabel);
    _subtypeCommentPanel.add(_librarySubtypeCommentTextField);
    
    _northDetailedPanel.add(_subtypeCommentPanel, BorderLayout.CENTER);
    _northDetailedPanel.add(_subtypeAdvanceSettingPanel, BorderLayout.SOUTH);

    _thresholdCommentPanel.add(_projectThresholdCommentLabel);
    _thresholdCommentPanel.add(_projectThresholdCommentTextField);
    _thresholdCommentPanel.add(_libraryThresholdCommentLabel);
    _thresholdCommentPanel.add(_libraryThresholdCommentTextField);

    _northPanel.setBorder(BorderFactory.createEtchedBorder());
//    _northPanel.add(_selectionPanel, BorderLayout.WEST);
    _northPanel.add(_selectionPanel, BorderLayout.CENTER);
    _northPanel.add(_northDetailedPanel, BorderLayout.SOUTH);

    _thresholdTypeButtonGroup.add(_allThresholdsRadioButton);
    _thresholdTypeButtonGroup.add(_onlyDifferentThresholdsRadioButton);
//    _thresholdsOptionPanel.add(_allThresholdsRadioButton);
//    _thresholdsOptionPanel.add(_onlyDifferentThresholdsRadioButton);
//    _centerPanel.add(_thresholdsOptionPanel,BorderLayout.NORTH);
    _centerPanel.add(_thresholdsTabbedPane, BorderLayout.CENTER);
    _centerPanel.add(_thresholdCommentPanel,BorderLayout.SOUTH);
    _thresholdsTabbedPane.add(_centerTabbedPanel);
    _southPanel.add(_doneButton);

    _mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    _mainPanel.add(_northPanel, BorderLayout.NORTH);
    _mainPanel.add(_centerPanel, BorderLayout.CENTER);
    _mainPanel.add(_southPanel, BorderLayout.SOUTH);

    _projectSubtypeCommentTextField.setEditable(false);
    _librarySubtypeCommentTextField.setEditable(false);
    _projectThresholdCommentTextField.setEditable(false);
    _libraryThresholdCommentTextField.setEditable(false);
    _currentLibraryTextField.setEditable(false);
    _jointTypeTextField.setEditable(false);
    _subtypeTextField.setEditable(false);
    _librarySubtypeTextField.setEditable(false);
    _libraryJointTypeTextField.setEditable(false);
    _projectSubtypeCommentTextField.setBackground(Color.WHITE);
    _librarySubtypeCommentTextField.setBackground(Color.WHITE);
    _projectThresholdCommentTextField.setBackground(Color.WHITE);
    _libraryThresholdCommentTextField.setBackground(Color.WHITE);

    _allThresholdsRadioButton.setSelected(true);

    _doneButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });

    add(_mainPanel, BorderLayout.CENTER);
    pack();
  }

  /**
   * for export use.
   * @author Tan Hock Zoon
   */
  public void populateDialogWithData(SubtypeScheme subtypeScheme)
  {
    Assert.expect(subtypeScheme != null);

    _importThreshold = true;
    _currentlibraryPath = subtypeScheme.getSelectedLibraryPath();
    _subtypeSchemes = new ArrayList<SubtypeScheme>();
    _subtypeSchemes.add(subtypeScheme);
    populateLibrarySubtypeComboBox();
    populateJointTypeComboBox();
    populateSubtypeComboBox();
    populateThresholdTables();
    populateSliceHeightTable();
    _currentLibraryTextField.setText(_currentlibraryPath);

    createSelectionPanel();
    _selectionPanel.add(_librarySubtypeLabel);

    if (_librarySubtypeComboBox.getItemCount() > 1)
      _selectionPanel.add(_librarySubtypeComboBox);
    else
    {
      _librarySubtypeTextField.setText(_librarySubtypeComboBox.getItemAt(0).toString());
      _selectionPanel.add(_librarySubtypeTextField);
    }
    _selectionPanel.add(_libraryJointTypeLabel);
    updateLibraryJointTypeField();
    _selectionPanel.add(_libraryJointTypeTextField);
    addAllItemListener();
  }

  /**
   * for export use.
   * @author Wei Chin, Chong
   */
  public void populateDialogWithData(SubtypeScheme subtypeScheme, String libraryPath)
  {
    Assert.expect(subtypeScheme != null);
    Assert.expect(libraryPath != null);

    _currentlibraryPath = libraryPath;
    _subtypeSchemes = new ArrayList<SubtypeScheme>();
    _subtypeSchemes.add(subtypeScheme);
    mapProjectSubtypeToLibrarySubtype();
    populateJointTypeComboBox();
    populateSubtypeComboBox();
    populateThresholdTables();
    populateSliceHeightTable();
    _currentLibraryTextField.setText(_currentlibraryPath);
    createSelectionPanel();
    addAllItemListener();
  }

  /**
   * Takes the algos specified by the jointType and adds tabs/tables to the correct panel
   * @author Wei Chin, Chong
   */
  private void populateThresholdTables()
  {
    // clear out all the tabs and the tables in them in preparation for creating new ones
    _thresholdsTabbedPane.removeAll();

    Subtype currentSubtypeChoice = (Subtype)_subtypeComboBox.getSelectedItem();

    LibrarySubtype librarySubtype = null;

    if (_importThreshold == false)
    {
      librarySubtype = _projectSubtypeToLibrarySubtype.get(currentSubtypeChoice);
    }
    else
    {
      librarySubtype = (LibrarySubtype) _librarySubtypeComboBox.getSelectedItem();
      assignSubtypeToLibrarySubtype();
    }
    Assert.expect(librarySubtype != null);

    librarySubtype.setInspectionFamilyEnum(
      InspectionFamily.getInspectionFamily(
        JointTypeEnum.getJointTypeEnum(
          librarySubtype.getJointTypeName())).getInspectionFamilyEnum());
    
    // Update CompareSubtypeTableModel
    _compareSubtypeTableModel.populateDisplayData(currentSubtypeChoice, librarySubtype);

    // populate the threshold tabs and tables with them with Datastore data
    java.util.List<AlgorithmEnum> algorithms = librarySubtype.getAlgorithmEnums();
    if (algorithms.isEmpty())
    {
      return;
    }

    InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(JointTypeEnum.getJointTypeEnum(librarySubtype.getJointTypeName()));

    for (AlgorithmEnum algoEnum : algorithms)
    {
      CompareThresholdTableModel threshTableModel = new CompareThresholdTableModel(currentSubtypeChoice,
                                                                                   librarySubtype,
                                                                                   algoEnum,
                                                                                   false);
      // all slice controlling thresholds are in the Measurement algorithm
      Algorithm algorithm = InspectionFamily.getAlgorithm(inspectionFamily.getInspectionFamilyEnum(), algoEnum);

      // don't add the table if there are no thresholds
      if (threshTableModel.getRowCount() > 0)
      {
        JPanel tablePanel = new JPanel();
        BorderLayout tablePanelBorderLayout = new BorderLayout();
        tablePanel.setLayout(tablePanelBorderLayout);
        JScrollPane jsp = new JScrollPane();
        CompareThresholdTable compareThresholdTable = new CompareThresholdTable(threshTableModel,algorithm, currentSubtypeChoice);

        jsp.getViewport().add(compareThresholdTable);
        tablePanel.add(jsp, BorderLayout.CENTER);
        _thresholdsTabbedPane.add(tablePanel, algoEnum.toString());
        compareThresholdTable.changeSelection(0, 1, false, false);
        compareThresholdTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
        {
          public void valueChanged(ListSelectionEvent e)
          {
            compareThresholdTable_valueChanged(e);
          }
        });
      }
      //This is the NO THRESHOLDS case. For Standard, add the tab with a "No Thresholds" message. For Additional, do not add the tab
      else
      {
        JLabel label = new JLabel(StringLocalizer.keyToString("ATGUI_NO_THRESHOLDS_KEY"));
        JPanel panel = new JPanel();
        BorderLayout panelBorderLayout = new BorderLayout();
        panel.setLayout(panelBorderLayout);
        JPanel innerPanel = new JPanel();
        innerPanel.setBorder(new EtchedBorder());
        innerPanel.add(label);
        panel.add(innerPanel, BorderLayout.CENTER);
        _thresholdsTabbedPane.add(panel, algoEnum.toString());
      }
    }

    _thresholdsTabbedPane.requestFocus();
    _thresholdsTabbedPane.setSelectedIndex(0);  // reset to the first tab
    // reset the description to the selected TAB, this was reset by the last table as it
    // was created because I do a tt.changeSelection above.

    _currentSelectedAlgorithmTab = _thresholdsTabbedPane.getSelectedIndex();
    if (_currentSelectedAlgorithmTab < 0) // no tabs
      return;

    JTable table = findCurrentTable();
    updateThresholdDescription(currentSubtypeChoice, table, table.getSelectedRow());
  }

  /**
   * @author Wei Chin, Chong
   */
  private void populateSliceHeightTable()
  {
    Subtype currentSubtypeChoice = (Subtype)_subtypeComboBox.getSelectedItem();
    // this extracts all the slice height thresholds out, and creates a new tab to put them all in
    // add the last tab for slice height settings
    JPanel panel = new JPanel();
    BorderLayout panelBorderLayout = new BorderLayout();
    panel.setLayout(panelBorderLayout);

    LibrarySubtype librarySubtype = null;

    if (_importThreshold == false)
    {
      librarySubtype = _projectSubtypeToLibrarySubtype.get(currentSubtypeChoice);
    }
    else
    {
      librarySubtype = (LibrarySubtype) _librarySubtypeComboBox.getSelectedItem();
    }

    Assert.expect(librarySubtype != null);

    InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(currentSubtypeChoice.getJointTypeEnum());
    // all slice controlling thresholds are in the Measurement algorithm
    Algorithm algorithm = InspectionFamily.getAlgorithm(inspectionFamily.getInspectionFamilyEnum(), AlgorithmEnum.MEASUREMENT);

    CompareThresholdTableModel threshTableModel = new CompareThresholdTableModel(currentSubtypeChoice,
                                                                                 librarySubtype,
                                                                                 algorithm.getAlgorithmEnum(),
                                                                                 true); // slice settings
    //table model cannot be empty, all joint types have at least one slice height algorithm setting
    Assert.expect(threshTableModel.getRowCount() > 0);
    // don't add the table if there are no thresholds
    if (threshTableModel.getRowCount() > 0)
    {
      JPanel tablePanel = new JPanel();
      BorderLayout tablePanelBorderLayout = new BorderLayout();
      tablePanel.setLayout(tablePanelBorderLayout);
      JScrollPane jsp = new JScrollPane();
      CompareThresholdTable compareThresholdTable = new CompareThresholdTable(threshTableModel, algorithm, currentSubtypeChoice);

      jsp.getViewport().add(compareThresholdTable);
      tablePanel.add(jsp, BorderLayout.CENTER);
      _thresholdsTabbedPane.add(tablePanel, StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY"));
      compareThresholdTable.changeSelection(0, 1, false, false);
      compareThresholdTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
      {
        public void valueChanged(ListSelectionEvent e)
        {
          compareThresholdTable_valueChanged(e);
        }
      });
    }
    //This is the NO THRESHOLDS case. For Standard, add the tab with a "No Thresholds" message. For Additional, do not add the tab
    else
    {
      JLabel label = new JLabel(StringLocalizer.keyToString("ATGUI_NO_THRESHOLDS_KEY"));
      panel.setLayout(panelBorderLayout);
      JPanel innerPanel = new JPanel();
      innerPanel.setBorder(new EtchedBorder());
      innerPanel.add(label);
      panel.add(innerPanel, BorderLayout.CENTER);
      _thresholdsTabbedPane.add(panel, StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY"));
    }
  }

  /**
   * Updates the project and library threshold description, and user comment (if any)
   * @param subtype Subtype
   * @param tt JTable
   * @param selectedRow int
   * @author Wei Chin, Chong
   */
  private void updateThresholdDescription(Subtype subtype, JTable tt, int selectedRow)
  {
    Assert.expect(subtype != null);
    Assert.expect(tt != null);

    _projectSubtypeCommentTextField.setText(subtype.getUserComment());
    if (_importThreshold == false)
    {
      _librarySubtypeCommentTextField.setText(_projectSubtypeToLibrarySubtype.get(subtype).getSubtypeComment());
    }
    else
    {
      _librarySubtypeCommentTextField.setText(((LibrarySubtype)_librarySubtypeComboBox.getSelectedItem()).getSubtypeComment());
    }

    CompareThresholdTableModel tableModel = (CompareThresholdTableModel)tt.getModel();
    if (selectedRow < 0 ||
        selectedRow == tableModel.getStandardRow() ||
        selectedRow == tableModel.getAdditionalRow() ||
        selectedRow == tableModel.getAlgorithmEnabledRow()) // no selection, so put the empty threshold message
    {
      _projectThresholdCommentTextField.setText("");
      _projectThresholdCommentTextField.setEnabled(false); // disable the control, can't set a threshold comment if no comment
      _libraryThresholdCommentTextField.setText("");
      _libraryThresholdCommentTextField.setEnabled(false); // disable the control, can't set a threshold comment if no comment
      return;
    }

    AlgorithmSetting algorithmSetting = (AlgorithmSetting)tt.getModel().getValueAt(selectedRow, 0); // 0 column is the AlgorithmSetting object

    // lastly, do the user threshold comment
    _projectThresholdCommentTextField.setEnabled(true);
    _projectThresholdCommentTextField.setText(subtype.getAlgorithmSettingUserComment(algorithmSetting.getAlgorithmSettingEnum()));

    _libraryThresholdCommentTextField.setEnabled(true); // disable the control, can't set a threshold comment if no comment

    if (_importThreshold == false)
    {
      _libraryThresholdCommentTextField.setText(_projectSubtypeToLibrarySubtype.get(subtype).getThresholdComment(algorithmSetting.getAlgorithmSettingEnum()));
    }
    else
    {
      _libraryThresholdCommentTextField.setText(((LibrarySubtype) _librarySubtypeComboBox.getSelectedItem()).getThresholdComment(algorithmSetting.getAlgorithmSettingEnum()));
    }
  }

  /**
   * @return JTable
   * @author Wei Chin, Chong
   */
  private JTable findCurrentTable()
  {
    JTable table = null;
  // now, we need to select the ROW that this threshold is on

    java.awt.Component comp = _thresholdsTabbedPane.getSelectedComponent();
    Assert.expect(comp instanceof JPanel);
    JPanel compPanel = (JPanel)comp;
    java.awt.Component[] componentArray = compPanel.getComponents();
    for (int compIndex = 0; compIndex < componentArray.length; compIndex++)
    {
      java.awt.Component potentialComp = componentArray[compIndex];
      if (potentialComp instanceof JScrollPane)
      {
        JScrollPane jsp = (JScrollPane)potentialComp;
        java.awt.Component comp1 = (JTable)jsp.getViewport().getView();
        if (comp1 == null)
          table = null;
        else
        {
          table = (JTable)comp1;
        }
      }
    }
    return table;
  }

  /**
   * @author Wei Chin, Chong
   */
  private void populateSubtypeComboBox()
  {
    Assert.expect(_subtypeSchemes != null);
    Assert.expect(_subtypeSchemes.isEmpty() == false);

    _subtypeComboBox.removeAllItems();
    JointTypeEnum currentJointTypeChoice = (JointTypeEnum)_jointTypeComboBox.getSelectedItem();
    for( SubtypeScheme subtypeScheme :_subtypeSchemes)
    {
      for (Subtype subtype : subtypeScheme.getSubtypes())
      {
        if (currentJointTypeChoice.getName().equals(subtype.getJointTypeEnum().getName()))
          _subtypeComboBox.addItem(subtype);
      }
    }
    _subtypeComboBox.setSelectedIndex(0);
  }

  /**
   * @author Wei Chin, Chong
   */
  private void populateJointTypeComboBox()
  {
    Assert.expect(_subtypeSchemes != null);
    Assert.expect(_subtypeSchemes.isEmpty() == false);

    for( SubtypeScheme subtypeScheme :_subtypeSchemes)
      _jointTypeComboBox.addAll(subtypeScheme.getJointTypeEnums());

    _jointTypeComboBox.setSelectedIndex(0);
  }

  /**
   * This function useful for export library wizard only.
   *
   * @author Wei Chin, Chong
   */
  private void mapProjectSubtypeToLibrarySubtype()
  {
    Assert.expect(_subtypeSchemes != null);

    final Frame parent = (Frame)super.getOwner();

    final BusyCancelDialog busyDialog = new BusyCancelDialog(
        (Frame)getOwner(),
        StringLocalizer.keyToString("PLWK_COMPARE_THRESHOLDS_WAITING_KEY"),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, (Frame)getOwner());
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          for (SubtypeScheme subtypeScheme : _subtypeSchemes)
          {
            for (Subtype subtype : subtypeScheme.getSubtypes())
            {
              LibrarySubtype librarySubtype = LibraryUtil.getInstance().getMatchLibrarySubtype(subtype, subtypeScheme.getLibraryPath());
              _projectSubtypeToLibrarySubtype.put(subtype, librarySubtype);
            }
          }
        }
        catch (DatastoreException de)
        {
          JOptionPane.showMessageDialog(parent,
                                        StringLocalizer.keyToString("PLWK_ERROR_RETRIEVING_LIBRARY_SUBTYPE_KEY"),
                                        StringLocalizer.keyToString("PLWK_WARNING_TITLE_KEY"),
                                        JOptionPane.WARNING_MESSAGE);
        }
        finally
        {
          busyDialog.dispose();
        }
      }
    });
    busyDialog.setVisible(true);
  }

  /**
   * @param e ItemEvent
   * @author Wei Chin, Chong
   */
  private void jointTypeComboBox_itemStateChanged(ItemEvent e)
  {
    if( e.getStateChange() == e.SELECTED )
      populateSubtypeComboBox();
  }

  /**
   * @param e ItemEvent
   * @author Wei Chin, Chong
   */
  private void subtypeComboBox_itemStateChanged(ItemEvent e)
  {
    if( e.getStateChange() == e.SELECTED )
    {
      populateThresholdTables();
      populateSliceHeightTable();
    }
  }

  /**
   * @param e ListSelectionEvent
   * @author Wei Chin, Chong
   */
  private void compareThresholdTable_valueChanged(ListSelectionEvent e)
  {
    Subtype currentSubtypeChoice = (Subtype)_subtypeComboBox.getSelectedItem();
    JTable table = findCurrentTable();
    updateThresholdDescription(currentSubtypeChoice, table, table.getSelectedRow());
  }

  /**
   * @author Wei Chin, Chong
   */
  public void unpopulate()
  {
    if (_subtypeSchemes != null)
      _subtypeSchemes.clear();

    if (_projectSubtypeToLibrarySubtype != null)
      _projectSubtypeToLibrarySubtype.clear();

    _subtypeSchemes = null;
    _projectSubtypeToLibrarySubtype = null;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void dispose()
  {
    unpopulate();
    super.dispose();
  }

  /**
   * @author Tan Hock Zoon
   */
  private void populateLibrarySubtypeComboBox()
  {
    _librarySubtypeComboBox.removeAllItems();

    java.util.List<Integer> temporaryArray = new ArrayList<Integer>();

    for(SubtypeScheme subtypeScheme : _subtypeSchemes)
    {
      try
      {
        LibraryUtil.getInstance().populateAlgorithmSettingByLibrarySubtypeScheme(subtypeScheme.getSelectedLibraryPath(), 
                                                                                 subtypeScheme.getSelectedLibraryPackage());
      }
      catch (DatastoreException de)
      {
        JOptionPane.showMessageDialog(this,
                                    StringLocalizer.keyToString("PLWK_ERROR_RETRIEVING_LIBRARY_SUBTYPE_KEY"),
                                    StringLocalizer.keyToString("PLWK_WARNING_TITLE_KEY"),
                                    JOptionPane.WARNING_MESSAGE);
      }
      
      for(LibrarySubtypeScheme librarySubtypeScheme : subtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeSchemes())
      {
        for (LibrarySubtype librarySubtype : librarySubtypeScheme.getLibrarySubtypes())
        {
          if (temporaryArray.contains(librarySubtype.getSubtypeId()) == false)
          {
            _librarySubtypeComboBox.addItem(librarySubtype);
            temporaryArray.add(librarySubtype.getSubtypeId());
          }
        }
      }
    }

    temporaryArray.clear();
    temporaryArray = null;

    _librarySubtypeComboBox.setSelectedIndex(0);
  }

  /**
   * @param e ItemEvent
   * @author Tan Hock Zoon
   */
  private void librarySubtypeComboBox_itemStateChanged(ItemEvent e)
  {
    if( e.getStateChange() == e.SELECTED )
    {
      populateThresholdTables();
      populateSliceHeightTable();
      updateLibraryJointTypeField();
    }
  }

  /**
   * @author Tan Hock Zoon
   */
  private void addAllItemListener()
  {
    _librarySubtypeComboBox.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        librarySubtypeComboBox_itemStateChanged(e);
      }
    });

    _jointTypeComboBox.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        jointTypeComboBox_itemStateChanged(e);
      }
    });

    _subtypeComboBox.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        subtypeComboBox_itemStateChanged(e);
      }
    });
  }

  /**
   * @author Tan Hock Zoon
   */
  private void assignSubtypeToLibrarySubtype()
  {
    Subtype currentSubtypeChoice = (Subtype)_subtypeComboBox.getSelectedItem();
    LibrarySubtype librarySubtype = (LibrarySubtype) _librarySubtypeComboBox.getSelectedItem();

    librarySubtype.setProjectSubtype(currentSubtypeChoice);
  }

  /**
   * @author Tan Hock Zoon
   */
  private void createSelectionPanel()
  {
    _selectionPanel.add(_projectJointTypeLabel);

    if (_jointTypeComboBox.getItemCount() > 1)
      _selectionPanel.add(_jointTypeComboBox);
    else
    {
      _jointTypeTextField.setText(_jointTypeComboBox.getItemAt(0).toString());
      _selectionPanel.add(_jointTypeTextField);
    }

    _selectionPanel.add(_subtypeLabel);

    if (_subtypeComboBox.getItemCount() > 1)
      _selectionPanel.add(_subtypeComboBox);
    else
    {
      _subtypeTextField.setText(_subtypeComboBox.getItemAt(0).toString());
      _selectionPanel.add(_subtypeTextField);
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  private void updateLibraryJointTypeField()
  {
    LibrarySubtype librarySubtype = null;
    Subtype currentSubtypeChoice = (Subtype)_subtypeComboBox.getSelectedItem();

    if (_importThreshold == false)
    {
      librarySubtype = _projectSubtypeToLibrarySubtype.get(currentSubtypeChoice);
    }
    else
    {
      librarySubtype = (LibrarySubtype) _librarySubtypeComboBox.getSelectedItem();
    }

    _libraryJointTypeTextField.setText(librarySubtype.getJointTypeName());
  }
}
