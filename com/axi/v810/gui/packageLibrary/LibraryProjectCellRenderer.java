package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.v810.business.packageLibrary.*;

/**
 * @author Cheah Lee Herng
 */
public class LibraryProjectCellRenderer extends DefaultTableCellRenderer
{
  /**
   * @author Tan Hock Zoon
   */
  public LibraryProjectCellRenderer()
  {
    super();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    ImportRefinePackageLayoutTable refinePackageTable = (ImportRefinePackageLayoutTable) table;
    ImportRefinePackageLayoutTableModel refinePackageTableModel = (ImportRefinePackageLayoutTableModel) refinePackageTable.getModel();

    SubtypeScheme subtypeScheme = refinePackageTableModel.getSubtypeSchemeAt(row);

    JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    label.setOpaque(true);
    
//    if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false)
//    {
//      label.setForeground(Color.BLACK);
//      label.setBackground(Color.WHITE);
//    }
//    else
//    {
//      label.setForeground(Color.BLACK);
//      label.setBackground(Color.WHITE);
//    }
    
    label.setForeground(Color.BLACK);
    label.setBackground(Color.WHITE);
    
    return label;
  }
}
