package com.axi.v810.gui.packageLibrary;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: ImportPackageLayoutTableModel</p>
 *
 * <p>Description: This model populate the current project SubtypeSchemes</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Tan Hock Zoon
 * @version 1.0
 */
public class ImportPackageLayoutTableModel extends DefaultSortTableModel
{
  private List<SubtypeScheme> _subtypeSchemes = new ArrayList<SubtypeScheme>();
  private List<SubtypeScheme> _currentlySelectedSchemes = new ArrayList<SubtypeScheme>();

  private static final String _SELECT_COLUMN_HEADING = StringLocalizer.keyToString("ZIP_PROEJECT_WIZARD_SELECT_BUTTON_KEY");
  private static final String _PACKAGE_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_GROUPINGS_PACKAGE_KEY");
  private static final String _JOINT_TYPE_COLUMN_HEADING = StringLocalizer.keyToString("ATGUI_FAMILY_KEY");
  private static final String _SUBTYPE_COLUMN_HEADING = StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY");
  private static final String _IMPORTED_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_IMPORTED_QUESTION_KEY");

  private final String[] _tableColumns =
      {_SELECT_COLUMN_HEADING,
      _PACKAGE_COLUMN_HEADING,
      _JOINT_TYPE_COLUMN_HEADING,
      _SUBTYPE_COLUMN_HEADING,
      _IMPORTED_COLUMN_HEADING};

  private int _currentSortColumn = 1;
  private boolean _currentSortAscending = true;
  private final static String _MIXED = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");

  /** @todo APM Code Review Not overly fond of public things like this, but can't think of a better way. */
  public static final int FILTER_BY_ALL = 0;
  public static final int FILTER_BY_IMPORTED = 1;
  public static final int FILTER_BY_UNIMPORTED = 2;

  /**
   *
   * @param currentlySelectedSchemes List
   *
   * @author Tan Hock Zoon
   */
  public ImportPackageLayoutTableModel(List<SubtypeScheme> currentlySelectedSchemes)
  {
    super();

    Assert.expect(currentlySelectedSchemes != null);

    _currentlySelectedSchemes = currentlySelectedSchemes;
  }

  /**
   * @author Tan Hock Zoon
   */
  void clear()
  {
    if (_subtypeSchemes != null)
      _subtypeSchemes.clear();

    if (_currentlySelectedSchemes != null)
      _currentlySelectedSchemes.clear();

    _currentSortColumn = 0;
    _currentSortAscending = true;
  }

  /**
   *
   * @param column int
   * @param ascending boolean
   *
   * @author Tan Hock Zoon
   */
  public void sortColumn(int column, final boolean ascending)
  {
    if(column == ImportPackageLayoutTable._SELECT_INDEX)  // Select column
      Collections.sort(_subtypeSchemes, new Comparator<SubtypeScheme>()
      {
        public int compare(SubtypeScheme lhPackage, SubtypeScheme rhPackage)
        {
          if( _currentlySelectedSchemes.contains(lhPackage) &&
              _currentlySelectedSchemes.contains(rhPackage) )
            return 0;
          else if( ascending &&
                   _currentlySelectedSchemes.contains(lhPackage) &&
                   _currentlySelectedSchemes.contains(rhPackage) == false)
            return 1;
          else if( ascending == false &&
                   _currentlySelectedSchemes.contains(lhPackage) &&
                   _currentlySelectedSchemes.contains(rhPackage) == false)
            return -1;
          else if( ascending &&
                   _currentlySelectedSchemes.contains(lhPackage) == false &&
                   _currentlySelectedSchemes.contains(rhPackage))
            return -1;
          else if( ascending == false &&
                   _currentlySelectedSchemes.contains(lhPackage) == false &&
                   _currentlySelectedSchemes.contains(rhPackage))
            return 1;
          return 0;
        }
      });
    else if(column == ImportPackageLayoutTable._PACKAGE_INDEX) // package name
      Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.PACKAGE_NAME));
    else if (column == ImportPackageLayoutTable._JOINT_TYPE_INDEX) // joint type
      Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.JOINT_TYPE));
    else if (column == ImportPackageLayoutTable._SUBTYPE_INDEX) // subtype
      Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.SUBTYPE_NAME));
    else if (column == ImportPackageLayoutTable._IMPORTED_INDEX) // imported
      Collections.sort(_subtypeSchemes, new Comparator<SubtypeScheme>()
      {
        public int compare(SubtypeScheme lhPackage, SubtypeScheme rhPackage)
        {
          Boolean lhBoolean = new Boolean(lhPackage.getCompPackage().isImported());
          Boolean rhBoolean = new Boolean(rhPackage.getCompPackage().isImported());

          if (ascending)
            return lhBoolean.compareTo(rhBoolean);
          else
            return rhBoolean.compareTo(lhBoolean);
        }
      });

    _currentSortColumn = column;
    _currentSortAscending = ascending;
  }

  /**
   * @author Tan Hock Zoon
   */
  public int getColumnCount()
  {
    Assert.expect(_tableColumns != null);
    return _tableColumns.length;
  }

  /**
   *
   * @param columnIndex int
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(_tableColumns != null);
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_tableColumns[columnIndex];
  }

  /**
   * @author Tan Hock Zoon
   */
  public int getRowCount()
  {
    if (_subtypeSchemes == null)
      return 0;
    else
      return _subtypeSchemes.size();
  }

  /**
   *
   * @param rowIndex int
   * @param columnIndex int
   * @return boolean
   *
   * @author Tan Hock Zoon
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if (_subtypeSchemes == null || _subtypeSchemes.size() == 0)
    {
      return false;
    }

    if (columnIndex == ImportPackageLayoutTable._SELECT_INDEX)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   *
   * @param object Object
   * @param rowIndex int
   * @param columnIndex int
   *
   * @author Tan Hock Zoon
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    SubtypeScheme subtypeScheme = _subtypeSchemes.get(rowIndex);

    if (columnIndex == ImportPackageLayoutTable._SELECT_INDEX)
    {
      boolean selected = (Boolean)object;
      if (selected)
      {
        if (_currentlySelectedSchemes.contains(subtypeScheme) == false)
        {
          _currentlySelectedSchemes.add(subtypeScheme);
        }
      }
      else
      {
        if (_currentlySelectedSchemes.contains(subtypeScheme) == true)
        {
          _currentlySelectedSchemes.remove(subtypeScheme);
        }
      }

      fireTableCellUpdated(rowIndex, columnIndex);
      GuiObservable.getInstance().stateChanged(this, SelectionEventEnum.IMPORT_PACKAGES_SELECTION);
    }
  }

  /**
   *
   * @param rowIndex int
   * @param columnIndex int
   * @return Object
   *
   * @author Tan Hock Zoon
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_subtypeSchemes != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    SubtypeScheme subtypeScheme = _subtypeSchemes.get(rowIndex);

    //get the appropriate data item that corresponds to the column
    switch (columnIndex)
    {
      case ImportPackageLayoutTable._SELECT_INDEX:
      {
        value = new Boolean(_currentlySelectedSchemes.contains(subtypeScheme));
        break;
      }
      case ImportPackageLayoutTable._PACKAGE_INDEX:
      {
        value = subtypeScheme.getPackageShortName();
        break;
      }
      case ImportPackageLayoutTable._JOINT_TYPE_INDEX:
      {
        if (subtypeScheme.usesOneJointTypeEnum())
        {
          value = subtypeScheme.getJointTypeEnum().getName();
        }
        else
        {
          value = _MIXED;
        }

        break;
      }
      case ImportPackageLayoutTable._SUBTYPE_INDEX:
      {
        if (subtypeScheme.usesOneSubtype())
        {
          value = subtypeScheme.getSubtype().getShortName();
        }
        else
        {
          value = _MIXED;
        }

        break;
      }
      case ImportPackageLayoutTable._IMPORTED_INDEX:
      {
        if (subtypeScheme.getCompPackage().isImported() == false)
        {
          value = StringLocalizer.keyToString("PLWK_NO_STRING_KEY");
        }
        else
        {
          value = StringLocalizer.keyToString("PLWK_YES_STRING_KEY");
        }

        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }
    return value;
  }

  /**
   * @author Tan Hock Zoon
   */
  public List<SubtypeScheme> getSelectedSchemes()
  {
    Assert.expect(_currentlySelectedSchemes != null);
    return _currentlySelectedSchemes;
  }

  /**
   * @author Tan Hock Zoon
   */
  public void selectAllPackages()
  {
    for (int i = 0; i < getRowCount(); i++)
    {
      setValueAt(new Boolean(true), i, ImportPackageLayoutTable._SELECT_INDEX);
      fireTableCellUpdated(i, ImportPackageLayoutTable._SELECT_INDEX);
    }

  }

  /**
   * @author Tan Hock Zoon
   */
  public void unselectAllPackages()
  {
    for (int i = 0; i < getRowCount(); i++)
    {
      setValueAt(new Boolean(false), i, ImportPackageLayoutTable._SELECT_INDEX);
      fireTableCellUpdated(i, ImportPackageLayoutTable._SELECT_INDEX);
    }
  }

  /**
   *
   * @param row int
   * @return SubtypeScheme
   *
   * @author Tan Hock Zoon
   */
  SubtypeScheme getSubtypeSchemeAt(int row)
  {
    Assert.expect(row >= 0);
    Assert.expect(_subtypeSchemes != null);
    return _subtypeSchemes.get(row);
  }

  /**
   *
   * @param panel Panel
   * @param filteringCriteria int
   *
   * @author Tan Hock Zoon
   */
  void populateWithPanelData(Panel panel, int filteringCriteria)
  {
    _subtypeSchemes.clear();
    _currentlySelectedSchemes.clear();

    for (CompPackage compPackage : panel.getCompPackages())
    {
      _subtypeSchemes.addAll(compPackage.getSubtypeSchemes());
    }

    List<SubtypeScheme> subtypeSchemeToRemove = new ArrayList<SubtypeScheme>();

    if (filteringCriteria == FILTER_BY_IMPORTED)
    {
      for (SubtypeScheme subtypeScheme : _subtypeSchemes)
      {
        if (subtypeScheme.getCompPackage().isImported() == false)
        {
          subtypeSchemeToRemove.add(subtypeScheme);
        }
      }

      _subtypeSchemes.removeAll(subtypeSchemeToRemove);
      subtypeSchemeToRemove.clear();
    }
    else if (filteringCriteria == FILTER_BY_UNIMPORTED)
    {
      for (SubtypeScheme subtypeScheme : _subtypeSchemes)
      {
        if (subtypeScheme.getCompPackage().isImported() == true)
        {
          subtypeSchemeToRemove.add(subtypeScheme);
        }
      }

      _subtypeSchemes.removeAll(subtypeSchemeToRemove);
      subtypeSchemeToRemove.clear();

      if (_currentlySelectedSchemes.size() <= 0)
        selectAllPackages();
    }
    
    // XCR-3130 Select imported packages only does not take effect if packages under show all is selected all
    _currentlySelectedSchemes.removeAll(_subtypeSchemes);
    _currentlySelectedSchemes.addAll(_subtypeSchemes);

    sortColumn(_currentSortColumn, _currentSortAscending);
  }
}
