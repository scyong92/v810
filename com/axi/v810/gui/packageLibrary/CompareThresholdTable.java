package com.axi.v810.gui.packageLibrary;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import java.awt.Point;
import java.awt.event.MouseEvent;
/**
 *
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class CompareThresholdTable extends JTable
{
  public static final int _ALGORITHM_SETTING_INDEX = 0;
  public static final int _PROJECT_VALUE_INDEX = 1;
  public static final int _LIBRARY_VALUE_INDEX = 2;
  public static final int _DEFAULT_VALUE_INDEX = 3;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _ALGORITHMS_SETTING_COLUMN_WIDTH_PERCENTAGE = .55;
  private static final double _PROJECT_VALUE_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double _LIBRARY_VALUE_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double _DEFAULT_VALUE_COLUMN_WIDTH_PERCENTAGE = .15;

  /**
   * @author Wei Chin,Chong
   */
  CompareThresholdTable(TableModel tableModel, Algorithm algorithm, Subtype subtype)
  {
    super(tableModel);
    getTableHeader().setReorderingAllowed(false);
    renderColumns(new CompareThresholdTableCellRenderer(this,(CompareThresholdTableModel)tableModel, algorithm,subtype));
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    setPreferredColumnWidths();
  }

  /**
   * Render all columns with the specified cell renderer
   * @param cellRender TableCellRenderer
   *
   * @author Wei Chin, Chong
   */
  public void renderColumns(TableCellRenderer cellRender)
  {
    for (int i = 0; i < this.getModel().getColumnCount(); i++)
    {
      renderColumn(this.getColumnModel().getColumn(i), cellRender);
    }
  }

  /**
   * @author Wei Chin,Chong
   */
  public void renderColumn(TableColumn col, TableCellRenderer cellRender)
  {
    try
    {
      col.setCellRenderer(cellRender);
    }
    catch(Exception e)
    {
      Assert.expect(false,e.getMessage());
    }
  }

  /**
   * @author Wei Chin,Chong
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_ALGORITHM_SETTING_INDEX).setPreferredWidth((int)(totalColumnWidth * _ALGORITHMS_SETTING_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PROJECT_VALUE_INDEX).setPreferredWidth((int)(totalColumnWidth * _PROJECT_VALUE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_LIBRARY_VALUE_INDEX).setPreferredWidth((int)(totalColumnWidth * _LIBRARY_VALUE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_DEFAULT_VALUE_INDEX).setPreferredWidth((int)(totalColumnWidth * _DEFAULT_VALUE_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * @param e MouseEvent
   * @return String
   */
  public String getToolTipText(MouseEvent e)
  {
    Point p = e.getPoint();
    int row = rowAtPoint(p);
    int col = columnAtPoint(p);

    if (row >= 0 && col >= 0)
    {
      return getValueAt(row, col).toString();
    }
    else
    {
      return null;
    }
  }
}
