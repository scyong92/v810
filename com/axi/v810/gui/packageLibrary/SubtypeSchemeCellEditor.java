package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.v810.business.packageLibrary.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class SubtypeSchemeCellEditor extends AbstractCellEditor implements TableCellEditor
{
  private JTextField _textField = null;
  private MultiSelectPanel multiSelectPanel = null;

  private Frame _parentFrame = null;

  private boolean useTextMultiSelection = false;

  /**
   * @param frame Frame
   *
   * @author Wei Chin, Chong
   */
  public SubtypeSchemeCellEditor(Frame frame)
  {
    super();
    _parentFrame = frame;
  }

  /**
   * @return Object
   *
   * @author Wei Chin, Chong
   */
  public Object getCellEditorValue()
  {
    if(useTextMultiSelection)
      return multiSelectPanel.getText();

    else
      return _textField.getText();
  }

  /**
   * @param table JTable
   * @param value Object
   * @param isSelected boolean
   * @param row int
   * @param column int
   * @return Component
   *
   * @author Wei Chin, Chong
   */
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, final int row, int column)
  {
    _textField = new JTextField((String)value);

    if (table instanceof ExportSubtypeLayoutTable)
    {
      ExportSubtypeLayoutTable exportSubtypeLayoutTable = (ExportSubtypeLayoutTable)table;
      final ExportSubtypeLayoutTableModel exportSubtypeLayoutTableModel = (ExportSubtypeLayoutTableModel)exportSubtypeLayoutTable.getModel();

      if (exportSubtypeLayoutTableModel.getSubtypeSchemeAt(row).usesOneSubtype() == false)
      {
        multiSelectPanel = new MultiSelectPanel();
        // since the MultiSelectDlg class is really what is being rendered, I'm calling functions in
        // that class to set foreground and background colors
        if (isSelected)
        {
          multiSelectPanel.setMultiSelectBackgroundColor(table.getSelectionBackground());
          multiSelectPanel.setMultiSelectForegroundColor(table.getSelectionForeground());
        }
        else
        {
          multiSelectPanel.setMultiSelectBackgroundColor(table.getBackground());
          multiSelectPanel.setMultiSelectForegroundColor(table.getForeground());
        }
        multiSelectPanel.setText((String)value);
        multiSelectPanel.addButtonActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            buttonActionPerformed(e, exportSubtypeLayoutTableModel.getSubtypeSchemeAt(row));
          }
        });
        multiSelectPanel.addButton();
        return multiSelectPanel;
      }
    }
    return _textField;
  }

  /**
   * @param e ActionEvent
   * @param subtypeScheme SubtypeScheme
   *
   * @author Wei Chin, Chong
   */
  public void buttonActionPerformed(ActionEvent e, final SubtypeScheme subtypeScheme)
  {
    final SubtypeSchemeCellEditor subtypeSchemeCellEditor = this;
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        RenameSubtypeSchemeDialog _renameSubtypeSchemeDialog = new RenameSubtypeSchemeDialog(_parentFrame, true, subtypeScheme);
        _renameSubtypeSchemeDialog.populateSubtypeScheme(subtypeScheme);
        SwingUtils.centerOnComponent(_renameSubtypeSchemeDialog, _parentFrame);
        _renameSubtypeSchemeDialog.setVisible(true);
        _renameSubtypeSchemeDialog.dispose();
        subtypeSchemeCellEditor.stopCellEditing();
      }
    });
  }
}
