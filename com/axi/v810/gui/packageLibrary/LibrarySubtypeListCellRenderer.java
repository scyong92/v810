package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

import com.axi.v810.business.packageLibrary.*;

/**
 * This class handles the background highlighting of ListItem for LibrarySubtype combo-box.
 * For library subtype that is from Legacy Project, we should highlight it with different color
 * for easier identification.
 * 
 * @author Cheah Lee Herng
 */
public class LibrarySubtypeListCellRenderer extends JLabel implements ListCellRenderer<Object>
{
  /**
   * Default Constructor
   * @author Cheah Lee Herng
   */
  public LibrarySubtypeListCellRenderer()
  {
    setOpaque(true);
  }
  
  /**   
   * @author Cheah Lee Herng
   */
  public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus)
  {
    // Prepare List UI
    setBorder(new EmptyBorder(2,2,2,2));
    setText(value.toString());
    
    // Initialization
    Color background = null;
    Color foreground = null;        
    
    if (value instanceof LibrarySubtypeScheme)
    {
      LibrarySubtypeScheme librarySubtypeScheme = (LibrarySubtypeScheme)value;                

      if (isSelected)
      {
        background = list.getSelectionBackground();
        foreground = list.getSelectionForeground();
      }
      else
      {
        if (librarySubtypeScheme.isLegacy())
        {
          background = Color.ORANGE;
          foreground = Color.BLACK;
        }
        else
        {
          background = Color.WHITE;
          foreground = Color.BLACK;
        }                       
      }      
    }
    else if (value instanceof String) // Mixed
    {      
      if (isSelected)
      {
        background = list.getSelectionBackground();
        foreground = list.getSelectionForeground();
      }
      else
      {
        background = Color.WHITE;
        foreground = Color.BLACK;
      }            
    }
    
    // Set Foreground and Background color
    setBackground(background);
    setForeground(foreground);
    
    return this;
  }
}
