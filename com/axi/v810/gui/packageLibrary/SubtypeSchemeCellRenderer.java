package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.v810.business.packageLibrary.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class SubtypeSchemeCellRenderer extends MultiSelectPanel implements TableCellRenderer
{
  /**
   * @author Wei Chin, Chong
   */
  public SubtypeSchemeCellRenderer()
  {
    setOpaque(true);
  }

  /**
   * @author Wei Chin, Chong
   */
  public Component getTableCellRendererComponent(JTable table, Object value,
                                                 boolean isSelected, boolean hasFocus, final int row, int column)
  {
    setOpaque(true);

    if (value instanceof String)
      setText((String)value);

    // since the MultiSelectDlg class is really what is being rendered, I'm calling functions in
    // that class to set foreground and background colors
    if (isSelected)
    {
      setMultiSelectBackgroundColor(table.getSelectionBackground());
      setMultiSelectForegroundColor(table.getSelectionForeground());
    }
    else
    {
      setMultiSelectBackgroundColor(table.getBackground());
      setMultiSelectForegroundColor(table.getForeground());
    }

    if (table instanceof ExportSubtypeLayoutTable)
    {
      ExportSubtypeLayoutTable exportSubtypeLayoutTable = (ExportSubtypeLayoutTable)table;
      final ExportSubtypeLayoutTableModel exportSubtypeLayoutTableModel = (ExportSubtypeLayoutTableModel)exportSubtypeLayoutTable.getModel();

      if( exportSubtypeLayoutTableModel.getSubtypeSchemeAt(row).usesOneSubtype() == false)
      {
        addButton();
      }
      else
      {
        removeButton();
      }
    }
    return this;
  }
}
