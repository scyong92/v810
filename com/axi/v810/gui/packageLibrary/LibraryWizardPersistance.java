package com.axi.v810.gui.packageLibrary;

import java.io.*;
import com.axi.v810.util.FileUtilAxi;
import com.axi.v810.datastore.FileName;
import com.axi.v810.datastore.DatastoreException;
import java.awt.Dimension;
import com.axi.util.Assert;

/**
 *
 * <p>Title: LibraryWizardPersistance</p>
 *
 * <p>Description: This class is used to record each Package Library Wizard panel sizes.</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Tan Hock Zoon
 * @version 1.0
 */
public class LibraryWizardPersistance implements Serializable
{
  private Dimension _importWelcomeSize = new Dimension(575, 400);
  private Dimension _importLibrarySelectionSize = new Dimension(750, 500);
  private Dimension _importPackageSelectionSize = new Dimension(1080, 520);
  private Dimension _importRefineSize = new Dimension(1200, 790);
  private Dimension _importRecipeSelectionSize = new Dimension(630, 520);
  private Dimension _exportWelcomeSize = new Dimension(575, 400);
  private Dimension _exportPackageSelectionSize = new Dimension(1200, 500);
  private Dimension _exportSubtypeSelectionSize = new Dimension(1000, 500);

  /**
   * @author Tan Hock Zoon
   */
  public LibraryWizardPersistance()
  {
    //do nothing
  }

  /**
   *
   * @return LibraryWizardPersistance
   *
   * @author Tan Hock Zoon
   */
  public LibraryWizardPersistance readSettings()
  {
    LibraryWizardPersistance persistance = null;
    try
    {
      if (FileUtilAxi.exists(FileName.getPackageLibraryPanelSizePersistFullPath()))
        persistance = (LibraryWizardPersistance) FileUtilAxi.loadObjectFromSerializedFile(FileName.getPackageLibraryPanelSizePersistFullPath());
      else
        persistance = new LibraryWizardPersistance();
    }
    catch(DatastoreException de)
    {
      persistance = new LibraryWizardPersistance();
    }

    return persistance;
  }

  /**
   *
   * @throws DatastoreException
   *
   * @author Tan Hock Zoon
   */
  public void writeSettings() throws DatastoreException
  {
    FileUtilAxi.saveObjectToSerializedFile(this, FileName.getPackageLibraryPanelSizePersistFullPath());
  }

  /**
   *
   * @param size Dimension
   *
   * @author Tan Hock Zoon
   */
  public void setImportWelcomeScreenSize(Dimension size)
  {
    Assert.expect(size != null);
    _importWelcomeSize = size;
  }

  /**
   *
   * @param size Dimension
   *
   * @author Tan Hock Zoon
   */
  public void setImportLibrarySelectionScreenSize(Dimension size)
  {
    Assert.expect(size != null);
    _importLibrarySelectionSize = size;
  }

  /**
   *
   * @param size Dimension
   *
   * @author Tan Hock Zoon
   */
  public void setImportPackageSelectionScreenSize(Dimension size)
  {
    Assert.expect(size != null);
    _importPackageSelectionSize = size;
  }

  /**
   *
   * @param size Dimension
   *
   * @author Tan Hock Zoon
   */
  public void setImportRefineScreenSize(Dimension size)
  {
    Assert.expect(size != null);
    _importRefineSize = size;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setImportRecipeSelectionScreenSize(Dimension size)
  {
    Assert.expect(size != null);
    _importRecipeSelectionSize = size;
  }

  /**
   *
   * @param size Dimension
   *
   * @author Tan Hock Zoon
   */
  public void setExportWelcomeScreenSize(Dimension size)
  {
    Assert.expect(size != null);
    _exportWelcomeSize = size;
  }

  /**
   *
   * @param size Dimension
   *
   * @author Tan Hock Zoon
   */
  public void setExportPackageSelectionScreenSize(Dimension size)
  {
    Assert.expect(size != null);
    _exportPackageSelectionSize = size;
  }

  /**
   *
   * @param size Dimension
   *
   * @author Tan Hock Zoon
   */
  public void setExportSubtypeSelectionScreenSize(Dimension size)
  {
    Assert.expect(size != null);
    _exportSubtypeSelectionSize = size;
  }

  /**
   *
   * @return Dimension
   *
   * @author Tan Hock Zoon
   */
  public Dimension getImportWelcomeScreenSize()
  {
    return _importWelcomeSize;
  }

  /**
   *
   * @return Dimension
   *
   * @author Tan Hock Zoon
   */
  public Dimension getImportLibrarySelectionScreenSize()
  {
    return _importLibrarySelectionSize;
  }

  /**
   *
   * @return Dimension
   *
   * @author Tan Hock Zoon
   */
  public Dimension getImportPackageSelectionScreenSize()
  {
    return _importPackageSelectionSize;
  }

  /**
   *
   * @return Dimension
   *
   * @author Tan Hock Zoon
   */
  public Dimension getImportRefineScreenSize()
  {
    return _importRefineSize;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public Dimension getImportRecipeSelectionScreenSize()
  {
    return _importRecipeSelectionSize;
  }

  /**
   *
   * @return Dimension
   *
   * @author Tan Hock Zoon
   */
  public Dimension getExportWelcomeScreenSize()
  {
    return _exportWelcomeSize;
  }

  /**
   *
   * @return Dimension
   *
   * @author Tan Hock Zoon
   */
  public Dimension getExportPackageSelectionScreenSize()
  {
    return _exportPackageSelectionSize;
  }

  /**
   *
   * @return Dimension
   *
   * @author Tan Hock Zoon
   */
  public Dimension getExportSubtypeSelectionScreenSize()
  {
    return _exportSubtypeSelectionSize;
  }

}
