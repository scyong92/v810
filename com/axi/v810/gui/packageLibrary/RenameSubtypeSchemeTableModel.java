package com.axi.v810.gui.packageLibrary;

import javax.swing.table.*;

import com.axi.util.*;
import com.axi.guiUtil.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class RenameSubtypeSchemeTableModel extends AbstractTableModel
{
  private final int _JOINT_TYPE_COLUMN = 0;
  private final int _SUBTYPE_COLUMN = 1;

  protected final String[] _renameSubtypeSchemeColumns =
      {StringLocalizer.keyToString("PLWK_EXPORT_JOINT_TYPE_TABLE_HEADER_KEY"),
       StringLocalizer.keyToString("PLWK_EXPORT_SUBTYPE_TABLE_HEADER_KEY")};

  private SubtypeScheme _subtypeScheme;

  /**
   * @author Wei Chin
   */
  public RenameSubtypeSchemeTableModel()
  {
    // do nothing
  }

  /**
   *  @param  rowIndex  the row being queried
   *  @param  columnIndex the column being queried
   *  @return false
   *
   * @author Wei Chin, Chong
   */
  public boolean isCellEditable(int rowIndex, int columnIndex) {

    switch(columnIndex)
    {
      case _SUBTYPE_COLUMN:
        return true;
    }
    return false;
  }

  /**
  * Get the current column name -- from AbstractTableModel
  *
  * @return The string specifying the column header name
  * @param col Column number
  *
  * @author Wei Chin, Chong
  */
  public String getColumnName(int col)
  {
    Assert.expect(col >= 0 && col < getColumnCount());

    return (String)_renameSubtypeSchemeColumns[col];
  }

  /**
   * @return int
   *
   * @author Wei Chin
   */
  public int getRowCount()
  {
    return _subtypeScheme.getSubtypes().size();
  }

  /**
   * @return int
   *
   * @author Wei Chin, Chong
   */
  public int getColumnCount()
  {
    return _renameSubtypeSchemeColumns.length;
  }

  /**
   * @param rowIndex int
   * @param columnIndex int
   * @return Object
   *
   * @author Wei Chin, Chong
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Subtype subtype = _subtypeScheme.getSubtypes().get(rowIndex);

    switch(columnIndex)
    {
      case _JOINT_TYPE_COLUMN :
        return subtype.getJointTypeEnum();

      case _SUBTYPE_COLUMN :

        subtype = _subtypeScheme.getSubtypes().get(rowIndex);
        if(_subtypeScheme.hasNewSubTypeName(subtype))
          return _subtypeScheme.getNewSubtypeName(subtype);
        return subtype;

      default :
        Assert.expect(false, "unexpect column index for Subtype Table.");
    }

    return null;
  }

  /**
   * @param value Object
   * @param rowIndex int
   * @param columnIndex int
   *
   * @author Wei Chin, Chong
   */
  public void setValueAt(Object value, int rowIndex, int columnIndex)
  {
    if(columnIndex == _SUBTYPE_COLUMN)
    {
      String newSubtypeName = (String)value;
      Subtype subtype = _subtypeScheme.getSubtypes().get(rowIndex);

      String oldSubtypeName = subtype.getShortName();

      if(_subtypeScheme.hasNewSubTypeName(subtype))
      {
        oldSubtypeName = _subtypeScheme.getNewSubtypeName(subtype);
      }

      if(oldSubtypeName.equals(newSubtypeName) == false)
      {
        _subtypeScheme.setNewSubtypeName(newSubtypeName, subtype);
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            MessageDialog.showInformationDialog(null,
                                                StringLocalizer.keyToString("PLWK_SUBTYPE_NAME_CHANGED_MESSAGE_OF_GUI_KEY"),
                                                StringLocalizer.keyToString("MMGUI_PLWK_NAME_KEY"),
                                                true);
          }
        });
      }
    }
    else
      Assert.expect(false, "unexpect column index for Subtype Table.");
  }

  /**
   * @param subtypeScheme SubtypeScheme
   *
   * @author Wei Chin, Chong
   */
  public void setSubtypeScheme(SubtypeScheme subtypeScheme)
  {
    Assert.expect(subtypeScheme != null);

    _subtypeScheme = subtypeScheme;
  }
}
