package com.axi.v810.gui.packageLibrary;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class ExportPackageLayoutTableModel extends DefaultSortTableModel
{
  private ExportPackageLayoutTable _exportPackageLayoutTable = null;
  private List<CompPackage> _packages = new ArrayList<CompPackage>();

  private static final String _SELECT_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_SELECT_TABLE_HEADER_KEY");
  private static final String _PACKAGE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_PACKAGE_TABLE_HEADER_KEY");
  private static final String _JOINT_TYPE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_JOINT_TYPE_TABLE_HEADER_KEY");
  private static final String _NEW_MODIFIED_IMPORTED_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_IMPORTED_TABLE_FIELD_KEY");
  private static final String _PACKAGFE_HAS_MODIFIED_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_PACKAGE_HAS_MODIFIED_TABLE_HEADER_KEY");
  private static final String _SUBTYPE_HAS_MODIFIED_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_SUBTYPE_HAS_MODIFIED_TABLE_HEADER_KEY");
  private static final String _OVERWRITE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_OVERWRITE_TABLE_HEADER_KEY");
  private static final String _SOURCE_LIBRARY_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_SOURCE_LIBRARY_TABLE_HEADER_KEY");

  public static final int FILTER_BY_ALL = 0;
  public static final int FILTER_BY_NEW_OR_MOPDIFIED = 1;
  public static final int FILTER_BY_IMPORTED = 2;

  private final String[] _tableColumns =
      {_SELECT_COLUMN_HEADING,
      _PACKAGE_COLUMN_HEADING,
      _JOINT_TYPE_COLUMN_HEADING,
      _NEW_MODIFIED_IMPORTED_COLUMN_HEADING,
      _PACKAGFE_HAS_MODIFIED_COLUMN_HEADING,
      _SUBTYPE_HAS_MODIFIED_COLUMN_HEADING,
      _OVERWRITE_COLUMN_HEADING,
      _SOURCE_LIBRARY_COLUMN_HEADING};

  private Set<CompPackage> _selectedPackages = null;

  /**
   * @param selectedPackages Set
   * @author Wei Chin, Chong
   */
  ExportPackageLayoutTableModel(Set<CompPackage> selectedPackages)
  {
    Assert.expect(selectedPackages != null);
    _selectedPackages = selectedPackages;
  }

  /**
   * @param exportPackageLayoutTable
   * @author Wei Chin, Chong
   */
  public void setExportPackageLayoutTable (ExportPackageLayoutTable exportPackageLayoutTable)
  {
    Assert.expect(exportPackageLayoutTable != null);
    _exportPackageLayoutTable = exportPackageLayoutTable;
  }

  /**
   * @param project Project
   * @author Wei Chin, Chong
   */
  void populateWithPackages(Project project)
  {
    populateWithPackages(project, FILTER_BY_NEW_OR_MOPDIFIED);
  }

  /**
   * @param project Project
   * @param filterType int
   * @author Wei Chin, Chong
   */
  void populateWithPackages(Project project, int filterType)
  {
     Assert.expect(project != null);

    _packages.clear();

    if (filterType == FILTER_BY_ALL)
    {
      for (CompPackage compPackage : project.getPanel().getCompPackages())
      {
        compPackage.resetCheckSum(); //make sure the checksum is re-generated

        _packages.add(compPackage);
      }
    }
    else if (filterType == FILTER_BY_NEW_OR_MOPDIFIED)
    {
      for (CompPackage compPackage : project.getPanel().getCompPackages())
      {
        compPackage.resetCheckSum(); //make sure the checksum is re-generated

        if (compPackage.hasSourceLibrary() == false ||
            compPackage.hasLibraryCheckSum() == false ||
            compPackage.isModifiedAfterImported() ||
            compPackage.hasSubtypeModified() )
          _packages.add(compPackage);
      }
    }
    else if (filterType == FILTER_BY_IMPORTED)
    {
      for (CompPackage compPackage : project.getPanel().getCompPackages())
      {
        compPackage.resetCheckSum(); //make sure the checksum is re-generated

        if (compPackage.isImported())
          _packages.add(compPackage);
      }
    }
    else
      Assert.expect(false);

    fireTableDataChanged();
  }

  /**
   * @param row int
   * @return CompPackage
   * @author Wei Chin, Chong
   */
  CompPackage getCompPackagesAt(int row)
  {
    Assert.expect(row >= 0 && row < _packages.size());
    Assert.expect(_packages != null);

    return (CompPackage)_packages.get(row);
  }

  /**
   * @param compPackage CompPackage
   * @return int
   * @author Wei Chin, Chong
   */
  int getRowForCompPackages(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);
    Assert.expect(_packages != null);

    return _packages.indexOf(compPackage);
  }

  /**
   * @author Wei Chin, Chong
   */
  void clear()
  {
    _packages.clear();
    _selectedPackages.clear();
    fireTableDataChanged();
    _exportPackageLayoutTable = null;
  }

  /**
   * @param column int
   * @param ascending boolean
   * @author Wei Chin, Chong
   */
  @Override
  public void sortColumn(int column, final boolean ascending)
  {
    if(_exportPackageLayoutTable != null)
      _exportPackageLayoutTable.stopCellsEditing();

    switch (column)
    {
      case ExportPackageLayoutTable._SELECT_INDEX: // selected or not
        Collections.sort(_packages, new Comparator<CompPackage>()
        {
          public int compare(CompPackage lhPackage, CompPackage rhPackage)
          {
            if( _selectedPackages.contains(lhPackage) &&
                _selectedPackages.contains(rhPackage) )
              return 0;
            else if( ascending &&
                     _selectedPackages.contains(lhPackage) &&
                     _selectedPackages.contains(rhPackage) == false)
              return 1;
            else if( ascending == false &&
                     _selectedPackages.contains(lhPackage) &&
                     _selectedPackages.contains(rhPackage) == false)
              return -1;
            else if( ascending &&
                     _selectedPackages.contains(lhPackage) == false &&
                     _selectedPackages.contains(rhPackage))
              return -1;
            else if( ascending == false &&
                     _selectedPackages.contains(lhPackage) == false &&
                     _selectedPackages.contains(rhPackage))
              return 1;
            return 0;
          }
        });
        break;
      case ExportPackageLayoutTable._PACKAGE_INDEX: // package name
        Collections.sort(_packages, new CompPackageComparator(ascending, CompPackageComparatorEnum.LONG_NAME));
        break;
      case ExportPackageLayoutTable._JOINT_TYPE_INDEX: // joint type
        Collections.sort(_packages, new CompPackageComparator(ascending, CompPackageComparatorEnum.JOINT_TYPE));
        break;
      case ExportPackageLayoutTable._NEW_MODIFIED_IMPORTED_INDEX: // New / Modified / Imported
        Collections.sort(_packages, new CompPackageComparator(ascending, CompPackageComparatorEnum.IS_IMPORTED));
        break;
      case ExportPackageLayoutTable._PACKAGE_HAS_MODIFIED_INDEX: // New / Modified / Imported
        Collections.sort(_packages, new CompPackageComparator(ascending, CompPackageComparatorEnum.IS_MODIFIED));
        break;
      case ExportPackageLayoutTable._SUBTYPE_HAS_MODIFIED_INDEX: // New / Modified / Imported
        Collections.sort(_packages, new CompPackageComparator(ascending, CompPackageComparatorEnum.HAS_MODIFIED_SUBTYPE));
        break;
      case ExportPackageLayoutTable._OVERWRITE_INDEX: // New / Modified / Imported
        Collections.sort(_packages, new CompPackageComparator(ascending, CompPackageComparatorEnum.OVERWRITE));
        break;
      case ExportPackageLayoutTable._SOURCE_LIBRARY_INDEX: // Library
        Collections.sort(_packages, new CompPackageComparator(ascending, CompPackageComparatorEnum.LIBRARY_PATH));
        break;
      default:
        Assert.expect(false,"Sorting function not implements! Column: " + column);
        break;
    }
  }

  /**
   * @return int
   * @author Wei Chin, Chong
   */
  public int getColumnCount()
  {
    Assert.expect(_tableColumns != null);
    return _tableColumns.length;
  }

  /**
   * @param columnIndex int
   * @return String
   * @author Wei Chin, Chong
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(_tableColumns != null);
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_tableColumns[columnIndex];
  }

  /**
   * @return int
   * @author Wei Chin, Chong
   */
  public int getRowCount()
  {
    if (_packages == null)
    {
      return 0;
    }
    else
    {
      return _packages.size();
    }
  }

  /**
   * @param rowIndex int
   * @param columnIndex int
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if (_packages == null || _packages.size() == 0)
    {
      return false;
    }

    switch (columnIndex)
    {
      case ExportPackageLayoutTable._SELECT_INDEX:
      {
        return true;
      }
      case ExportPackageLayoutTable._SOURCE_LIBRARY_INDEX:
      {
        return true;
      }
      case ExportPackageLayoutTable._OVERWRITE_INDEX:
      {
        if(_packages.get(rowIndex).hasLibraryCheckSum() && _packages.get(rowIndex).isModifiedAfterImported())
          return true;
      }
    }
    return false;
  }

  /**
   * @param object Object
   * @param rowIndex int
   * @param columnIndex int
   * @author Wei Chin, Chong
   */
  @Override
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    CompPackage compPackage = _packages.get(rowIndex);

    switch (columnIndex)
    {
      case ExportPackageLayoutTable._SELECT_INDEX:
      {
        boolean selected = (Boolean)object;
        if (selected)
        {
          if (_selectedPackages.contains(compPackage) == false)
          {
            _selectedPackages.add(compPackage);
          }
        }
        else
        {
          if (_selectedPackages.contains(compPackage) == true)
          {
            _selectedPackages.remove(compPackage);
          }
        }
        fireTableCellUpdated(rowIndex, columnIndex);
        GuiObservable.getInstance().stateChanged(this, SelectionEventEnum.EXPORT_LIBRARY_SELECTION);
        break;
      }
      case ExportPackageLayoutTable._SOURCE_LIBRARY_INDEX:
      {
        String newLibraryPath = (String)object;
        if(newLibraryPath.equals(StringLocalizer.keyToString("PLWK_EXPORT_NONE_SOURCE_LIBRARY_TABLE_FIELD_KEY")))
           newLibraryPath = "";
        compPackage.setLibraryPath(newLibraryPath);
        GuiObservable.getInstance().stateChanged(this, SelectionEventEnum.EXPORT_LIBRARY_SELECTION);
        fireTableCellUpdated(rowIndex, columnIndex);
        break;
      }
      case ExportPackageLayoutTable._OVERWRITE_INDEX:
      {
        String overwrite = (String)object;
        if(overwrite.equalsIgnoreCase(StringLocalizer.keyToString("PLWK_YES_STRING_KEY")))
          compPackage.setOverwriteToLibrary(true);
        else if (overwrite.equalsIgnoreCase(StringLocalizer.keyToString("PLWK_NO_STRING_KEY")))
          compPackage.setOverwriteToLibrary(false);
        else
          Assert.expect(false,"Failed to Set value");
        break;
      }
      default:
        Assert.expect(false,"Edit this column is not implemented");
    }
  }

  /**
   * @param rowIndex int
   * @param columnIndex int
   * @return Object
   * @author Wei Chin, Chong
   */
  @Override
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_packages != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    CompPackage compPackage = _packages.get(rowIndex);
    //get the appropriate data item that corresponds to the column
    switch (columnIndex)
    {
      case ExportPackageLayoutTable._SELECT_INDEX:
      {
        value = new Boolean(_selectedPackages.contains(compPackage));
        break;
      }
      case ExportPackageLayoutTable._PACKAGE_INDEX:
      {
        value = compPackage.getShortName();
        break;
      }
      case ExportPackageLayoutTable._JOINT_TYPE_INDEX:
      {
        if (compPackage.usesOneJointTypeEnum())
          value = compPackage.getJointTypeEnum().getName();
        else
          value = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
        break;
      }
      case ExportPackageLayoutTable._NEW_MODIFIED_IMPORTED_INDEX:
      {
        if (compPackage.isImported())
          value = StringLocalizer.keyToString("PLWK_YES_STRING_KEY");
        else
          value = StringLocalizer.keyToString("PLWK_NO_STRING_KEY");
        break;
      }
      case ExportPackageLayoutTable._PACKAGE_HAS_MODIFIED_INDEX:
      {
        if(compPackage.hasLibraryCheckSum() == false)
          value = StringLocalizer.keyToString("PLWK_NA_STRING_KEY");
        else if(compPackage.isModifiedAfterImported())
          value = StringLocalizer.keyToString("PLWK_YES_STRING_KEY");
        else
          value = StringLocalizer.keyToString("PLWK_NO_STRING_KEY");
        break;
      }
      case ExportPackageLayoutTable._SUBTYPE_HAS_MODIFIED_INDEX:
      {
        if(compPackage.hasSourceLibrary() == false)
          value = StringLocalizer.keyToString("PLWK_NA_STRING_KEY");
        else if(compPackage.hasSubtypeModified())
          value = StringLocalizer.keyToString("PLWK_YES_STRING_KEY");
        else
          value = StringLocalizer.keyToString("PLWK_NO_STRING_KEY");
        break;
      }
      case ExportPackageLayoutTable._OVERWRITE_INDEX:
      {
        if( compPackage.hasLibraryCheckSum() == false ||
            compPackage.isModifiedAfterImported() == false )
          value = StringLocalizer.keyToString("PLWK_NA_STRING_KEY");
        else if(compPackage.isOverwriteToLibrary())
          value = StringLocalizer.keyToString("PLWK_YES_STRING_KEY");
        else
          value = StringLocalizer.keyToString("PLWK_NO_STRING_KEY");
        break;
      }
      case ExportPackageLayoutTable._SOURCE_LIBRARY_INDEX:
      {
        if(compPackage.hasLibraryPath())
        {
          int lastIndex = compPackage.getLibraryPath().lastIndexOf("\\");
          int length = compPackage.getLibraryPath().length();

          if (lastIndex > 0 && (lastIndex + 1) != length)
            value = compPackage.getLibraryPath().substring(lastIndex+1);
          else
            value = compPackage.getLibraryPath();
        }
        else
          value = StringLocalizer.keyToString("PLWK_EXPORT_NONE_SOURCE_LIBRARY_TABLE_FIELD_KEY");
        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }
    return value;
  }

  /**
   * @return List
   * @author Wei Chin, Chong
   */
  public List <CompPackage> getSelectedPackages()
  {
    Assert.expect(_selectedPackages != null);
    return new ArrayList<CompPackage>(_selectedPackages);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void selectAllPackages()
  {
    for (int i = 0; i < getRowCount(); i++)
    {
      setValueAt(new Boolean(true), i, ExportPackageLayoutTable._SELECT_INDEX);
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  public void unselectAllPackages()
  {
    for (int i = 0; i < getRowCount(); i++)
    {
      setValueAt(new Boolean(false), i, ExportPackageLayoutTable._SELECT_INDEX);
    }
  }

  /**
   * @param libraryPath String
   * @author Wei Chin, Chong
   */
  public void assignEmptyLibraries(String libraryPath)
  {
    Assert.expect(libraryPath != null);

    for(CompPackage compPackage : _packages)
    {
      if(compPackage.hasLibraryPath() == false)
      {
        setValueAt(libraryPath,getRowForCompPackages(compPackage),ExportPackageLayoutTable._SOURCE_LIBRARY_INDEX);
      }
    }
  }

  /**
   * @param libraryPath String
   * @author Wei Chin, Chong
   */
  public void assignAllLibraries(String libraryPath)
  {
    Assert.expect(libraryPath != null);

    for (int i = 0; i < getRowCount(); i++)
    {
        setValueAt(libraryPath, i, ExportPackageLayoutTable._SOURCE_LIBRARY_INDEX);
    }
  }
}
