package com.axi.v810.gui.packageLibrary;

import javax.swing.*;

import com.axi.v810.business.packageLibrary.*;

public class LibraryJointTypeCellEditor extends DefaultCellEditor
{
  public LibraryJointTypeCellEditor(JComboBox comboBox)
  {
    super (comboBox);
  }

  public java.awt.Component getTableCellEditorComponent(JTable table, Object value, boolean isSelectable, int row, int column)
  {
    java.awt.Component editor = super.getTableCellEditorComponent(table, value, isSelectable, row, column);
    JComboBox comboBox = (JComboBox)editor;
    comboBox.removeAllItems();

    ImportRefinePackageLayoutTable refinePackageTable = (ImportRefinePackageLayoutTable) table;
    ImportRefinePackageLayoutTableModel refinePackageTableModel = (ImportRefinePackageLayoutTableModel) refinePackageTable.getModel();

    SubtypeScheme subtypeScheme = refinePackageTableModel.getSubtypeSchemeAt(row);

    for (String jointType : subtypeScheme.getSelectedLibraryPackage().getJointTypeList())
    {
      comboBox.addItem(jointType);
    }

    comboBox.setSelectedItem(value);
    comboBox.setMaximumRowCount(30);
    return comboBox;
  }

}
