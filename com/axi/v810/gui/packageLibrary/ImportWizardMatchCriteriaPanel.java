package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.axi.guiUtil.wizard.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

public class ImportWizardMatchCriteriaPanel extends WizardPanel
{
  private Project _project = null;

  private JCheckBox _landPatternCheckBox;
  private JCheckBox _jointTypeCheckBox;
  private JCheckBox _landPatternNameCheckBox;

  private JTextField _landPatternMatchPercentTextField;
  private JTextField _landPatternNameTextField;

  private JComboBox _landPatternNameFilterComboBox;
  private DefaultComboBoxModel _filterComboBoxModel = new DefaultComboBoxModel();

  private JPanel _mainPanel;
  private JPanel _contentPanel;
  private JPanel _landPatternPanel;
  private JLabel _percentMatchLabel;
  private JPanel _jointTypePanel;
  private JPanel _landPatternNamePanel;
  private JPanel _matchCriteriaDescPanel;
  private JLabel _matchCriteriaDescLabel;

  /**
   * @param project Project
   * @param dialog WizardDialog
   *
   * @author Tan Hock Zoon
   */
  public ImportWizardMatchCriteriaPanel(Project project, WizardDialog dialog)
  {
    super(dialog);
    _project = project;
    populateComboBoxModelData();
    init();
  }

  /**
   * @author Tan Hock Zoon
   */
  private void init()
  {
    _landPatternCheckBox = new JCheckBox(StringLocalizer.keyToString("PLWK_LAND_PATTERN_GEOMETRY_KEY"));
    _jointTypeCheckBox = new JCheckBox(StringLocalizer.keyToString("ATGUI_FAMILY_KEY"));
    _landPatternNameCheckBox = new JCheckBox(StringLocalizer.keyToString("PLWK_LAND_PATTERN_NAME_KEY"));
    _landPatternMatchPercentTextField = new JTextField(3);
    _landPatternNameTextField = new JTextField(30);

    _landPatternNameFilterComboBox = new JComboBox(_filterComboBoxModel);

    _mainPanel = new JPanel();
    _contentPanel = new JPanel();
    _landPatternPanel = new JPanel();
    _percentMatchLabel = new JLabel(StringLocalizer.keyToString("PLWK_LAND_PATTERN_MATCH_PERCENT_KEY"));
    _jointTypePanel = new JPanel();
    _landPatternNamePanel = new JPanel();

    _matchCriteriaDescPanel = new JPanel();
    _matchCriteriaDescLabel = new JLabel(StringLocalizer.keyToString("PLWK_IMPORT_SELECT_MATCH_CRITERIA_DESC_KEY"));
  }

  /**
   * @author Tan Hock Zoon
   */
  private void createPanel()
  {
    removeAll();

    _mainPanel.setLayout(new BorderLayout(5, 5));
    _contentPanel.setLayout(new GridLayout(3, 1));

    _landPatternPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

    _landPatternPanel.add(_landPatternCheckBox);
    _landPatternPanel.add(_landPatternMatchPercentTextField);
    _landPatternPanel.add(_percentMatchLabel);

    _jointTypePanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    _jointTypePanel.add(_jointTypeCheckBox);

    _landPatternNamePanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    _landPatternNamePanel.add(_landPatternNameCheckBox);
    _landPatternNamePanel.add(_landPatternNameFilterComboBox);
    _landPatternNamePanel.add(_landPatternNameTextField);

    _contentPanel.add(_landPatternPanel);
    _contentPanel.add(_jointTypePanel);
    _contentPanel.add(_landPatternNamePanel);

    _matchCriteriaDescPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    _matchCriteriaDescPanel.setBackground(Color.WHITE);
    _matchCriteriaDescPanel.add(_matchCriteriaDescLabel);

    _mainPanel.add(_matchCriteriaDescPanel, BorderLayout.NORTH);
    _mainPanel.add(_contentPanel, BorderLayout.CENTER);

    add(_mainPanel, BorderLayout.CENTER);

    _landPatternCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        validatePanel();
      }
    });

    _jointTypeCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        validatePanel();
      }
    });

    _landPatternNameCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        validatePanel();
      }
    });

    _dialog.setPreferredSize(new Dimension(675, 400));
  }

  /**
   * @author Tan Hock Zoon
   */
  public void validatePanel()
  {
    WizardButtonState buttonState = null;

    if (_landPatternCheckBox.isSelected())
    {
      _landPatternMatchPercentTextField.setEditable(true);
    }
    else
    {
      _landPatternMatchPercentTextField.setEditable(false);
    }

    if (_landPatternNameCheckBox.isSelected())
    {
      _landPatternNameTextField.setEditable(true);
    }
    else
    {
      _landPatternNameTextField.setEditable(false);
    }

    if (_landPatternCheckBox.isSelected() || _jointTypeCheckBox.isSelected() || _landPatternNameCheckBox.isSelected())
    {
      buttonState = new WizardButtonState(true, true, false, true);
    }
    else
    {
      buttonState = new WizardButtonState(false, true, false, true);
    }

    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Tan Hock Zoon
   */
  public void populatePanelWithData()
  {
    createPanel();

    validatePanel();
  }

  /**
   * @author Tan Hock Zoon
   */
  public void unpopulate()
  {
  }

  /**
   * @author Tan Hock Zoon
   */
  public void setWizardName(String name)
  {
    _name = name;
  }

  /**
   * @author Tan Hock Zoon
   */
  public String getWizardName()
  {
    return _name;
  }

  /**
   * @author Tan Hock Zoon
   */
  public void removeObservers()
  {
  }

  /**
   * @author Tan Hock Zoon
   */
  private void populateComboBoxModelData()
  {
    _filterComboBoxModel.addElement(StringLocalizer.keyToString("PLWK_START_WITH_KEY"));
    _filterComboBoxModel.addElement(StringLocalizer.keyToString("PLWK_END_WITH_KEY"));
    _filterComboBoxModel.addElement(StringLocalizer.keyToString("PLWK_CONTAINS_KEY"));
  }
}
