package com.axi.v810.gui.packageLibrary;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.home.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: ImportRecipeSelectionLayoutTableModel</p>
 *
 * <p>Description: This model populate available recipes in the system.</p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: Vitrox Technogies</p>
 *
 * @author Cheah Lee Herng
 * @version 1.0
 */
public class ImportRecipeSelectionLayoutTableModel extends DefaultSortTableModel
{
  private List<AvailableProjectData> _projectData = new LinkedList<AvailableProjectData>();
  private List<AvailableProjectData> _currentlySelectedProjectData = new LinkedList<AvailableProjectData>();
  
  private static final String _SELECT_COLUMN_HEADING = StringLocalizer.keyToString("ZIP_PROEJECT_WIZARD_SELECT_BUTTON_KEY");
  private static final String _RECIPE_COLUMN_HEADING = StringLocalizer.keyToString("HP_PROJECTS_TABLE_PROJECT_NAME_COLUMN_KEY");
  
  protected ProjectSummaryReader _projectSummaryReader = null;
  
  private final String[] _tableColumns =
                                      {_SELECT_COLUMN_HEADING,
                                      _RECIPE_COLUMN_HEADING};

  private int _currentSortColumn = 1;
  private boolean _currentSortAscending = true;
  
  /**
   * @author Cheah Lee Herng
   */
  public ImportRecipeSelectionLayoutTableModel(List<AvailableProjectData> currentlySelectedProjectData)
  {
    super();

    Assert.expect(currentlySelectedProjectData != null);

    _currentlySelectedProjectData = currentlySelectedProjectData;    
    _projectSummaryReader = ProjectSummaryReader.getInstance();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  void clear()
  {
    if (_projectData != null)
      _projectData.clear();

    if (_currentlySelectedProjectData != null)
      _currentlySelectedProjectData.clear();

    _currentSortColumn = 0;
    _currentSortAscending = true;
  }
  
  /**
   *
   * @param column int
   * @param ascending boolean
   *
   * @author Cheah Lee Herng
   */
  public void sortColumn(int column, final boolean ascending)
  {
    _currentSortColumn = column;
    _currentSortAscending = ascending;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getColumnCount()
  {
    Assert.expect(_tableColumns != null);
    return _tableColumns.length;
  }
  
  /**
   *
   * @param columnIndex int
   * @return String
   *
   * @author Cheah Lee Herng
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(_tableColumns != null);
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_tableColumns[columnIndex];
  }

  /**
   * @author Cheah Lee Herng
   */
  public int getRowCount()
  {
    if (_projectData == null)
      return 0;
    else
      return _projectData.size();
  }
  
  /**
   *
   * @param rowIndex int
   * @param columnIndex int
   * @return boolean
   *
   * @author Cheah Lee Herng
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if (_projectData == null || _projectData.isEmpty())
    {
      return false;
    }

    if (columnIndex == ImportRecipeSelectionLayoutTable._SELECT_INDEX)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  /**
   *
   * @param object Object
   * @param rowIndex int
   * @param columnIndex int
   *
   * @author Cheah Lee Herng
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    AvailableProjectData projectData = _projectData.get(rowIndex);

    if (columnIndex == ImportPackageLayoutTable._SELECT_INDEX)
    {
      boolean selected = (Boolean)object;
      if (selected)
      {
        if (_currentlySelectedProjectData.contains(projectData) == false)
        {
          _currentlySelectedProjectData.add(projectData);
        }
      }
      else
      {
        if (_currentlySelectedProjectData.contains(projectData) == true)
        {
          _currentlySelectedProjectData.remove(projectData);
        }
      }

      fireTableCellUpdated(rowIndex, columnIndex);
      GuiObservable.getInstance().stateChanged(this, SelectionEventEnum.IMPORT_RECIPES_SELECTION);
    }
  }
  
  /**
   *
   * @param rowIndex int
   * @param columnIndex int
   * @return Object
   *
   * @author Tan Hock Zoon
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_projectData != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    AvailableProjectData projectData = _projectData.get(rowIndex);
    
    //get the appropriate data item that corresponds to the column
    switch (columnIndex)
    {
      case ImportRecipeSelectionLayoutTable._SELECT_INDEX:
      {
        value = new Boolean(_currentlySelectedProjectData.contains(projectData));
        break;
      }
      case ImportRecipeSelectionLayoutTable._RECIPE_INDEX:
      {
        value = projectData.getProjectName();
        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }
    return value;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public List<AvailableProjectData> getSelectedProjectData()
  {
    Assert.expect(_currentlySelectedProjectData != null);
    return _currentlySelectedProjectData;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void selectAllProjectData()
  {
    for (int i = 0; i < getRowCount(); i++)
    {
      setValueAt(new Boolean(true), i, ImportRecipeSelectionLayoutTable._SELECT_INDEX);
      fireTableCellUpdated(i, ImportRecipeSelectionLayoutTable._SELECT_INDEX);
    }

  }

  /**
   * @author Cheah Lee Herng
   */
  public void unselectAllProjectData()
  {
    for (int i = 0; i < getRowCount(); i++)
    {
      setValueAt(new Boolean(false), i, ImportRecipeSelectionLayoutTable._SELECT_INDEX);
      fireTableCellUpdated(i, ImportRecipeSelectionLayoutTable._SELECT_INDEX);
    }
  }
  
  /**
   *
   * @param row int
   * @return SubtypeScheme
   *
   * @author Cheah Lee Herng
   */
  AvailableProjectData getProjectAt(int row)
  {
    Assert.expect(row >= 0);
    Assert.expect(_projectData != null);
    return _projectData.get(row);
  }
  
  /**
   *
   * @param panel Panel
   * @param filteringCriteria int
   *
   * @author Cheah Lee Herng
   */
  void populateWithPanelData()
  {
    _projectData.clear();
    
    // Get available recipes in the system
    List<String> availableProjectNames = FileName.getProjectNames();
    Object[] projectNames = availableProjectNames.toArray();
    
    // get summary data for each project
    for (int i = 0; i < projectNames.length; i++)
    {
      String projectName = (String)projectNames[i];
      // don't add projects with invalid names
      if (Project.isProjectNameValid(projectName))
      {                
        try
        {
          SystemTypeEnum systemTypeEnum = ProjectReader.getInstance().readProjectSystemType(projectName);
          if (systemTypeEnum.equals(XrayTester.getSystemType()))
          {
            ProjectSummary projectSummary = _projectSummaryReader.read(projectName);
            AvailableProjectData projectData = new AvailableProjectData(projectName, projectSummary);
            _projectData.add(projectData);
          }                    
        }
        catch (DatastoreException de)
        {
          // Do nothing
        }
      }
    }
  }
}
