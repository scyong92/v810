package com.axi.v810.gui.packageLibrary;

import java.io.*;
import java.text.*;
import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: ImportRefinePackageLayoutTableModel</p>
 *
 * <p>Description: This model is populate the search result from library</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Tan Hock Zoon
 * @version 1.0
 */
public class ImportRefinePackageLayoutTableModel extends DefaultSortTableModel
{
  private List<SubtypeScheme> _subtypeSchemes = new ArrayList<SubtypeScheme>();
  private List<SubtypeScheme> _currentlySelectedSchemes = new ArrayList<SubtypeScheme>();

  private static final String _SELECT_COLUMN_HEADING = StringLocalizer.keyToString("ZIP_PROEJECT_WIZARD_SELECT_BUTTON_KEY");
  private static final String _PACKAGE_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_GROUPINGS_PACKAGE_KEY");
  private static final String _JOINT_TYPE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_JOINT_TYPE_TABLE_HEADER_KEY");
  private static final String _SUBTYPE_COLUMN_HEADING = StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY");
  private static final String _LIBRARY_PATH_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_IMPORT_LIBRARY_PATH_KEY");  
  private static final String _LIBRARY_PACKAGE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_IMPORT_LIBRARY_PACKAGE_KEY");
  private static final String _LIBRARY_JOINT_TYPE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_IMPORT_LIBRARY_JOINT_TYPE_KEY");
  private static final String _LIBRARY_PROJECT_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_IMPORT_LIBRARY_PROJECT_KEY");
  private static final String _LIBRARY_SUBTYPE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_IMPORT_LIBRARY_SUBTYPE_KEY");
  private static final String _LIBRARY_SUBTYPE_DATE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_LIBRARY_SUBTYPE_DATE_KEY");
  private static final String _MATCHES_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_IMPORT_LIBRARY_MATCHES_KEY");

  private final String[] _tableColumns =
      {_SELECT_COLUMN_HEADING,
      _PACKAGE_COLUMN_HEADING,
      _JOINT_TYPE_COLUMN_HEADING,
      _SUBTYPE_COLUMN_HEADING,
      _LIBRARY_PATH_COLUMN_HEADING,
      _LIBRARY_PACKAGE_COLUMN_HEADING,
      _LIBRARY_JOINT_TYPE_COLUMN_HEADING,
      _LIBRARY_PROJECT_COLUMN_HEADING,
      _LIBRARY_SUBTYPE_COLUMN_HEADING,
      _LIBRARY_SUBTYPE_DATE_COLUMN_HEADING,
      _MATCHES_COLUMN_HEADING};

  private int _currentSortColumn = 1;
  private boolean _currentSortAscending = true;
  private final static String _MIXED = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
  private final static String _NONE = StringLocalizer.keyToString("PLWK_EXPORT_NONE_SOURCE_LIBRARY_TABLE_FIELD_KEY");

  /** @todo APM Code Review Should this be hardcoded, or is it related to a user-entry? */
  /** @todo APM Code Review If this is hardcoded, then do we need a legend in the GUI to explain it? */
  /** @todo APM Code Review Oh, and add the _ to this variable -- it's private after all */
  private static final int _MATCH_PERCENTAGE_IN_GREEN = 80;
  private SubtypeScheme _selectedSubtypeScheme;
  private boolean _setSubtypeScheme = true;
  private SimpleDateFormat _simpleDateFormat = new SimpleDateFormat("MMM-dd-yyyy");

  /**
   *
   * @param currentlySelectedSchemes List
   *
   * @author Tan Hock Zoon
   */
  public ImportRefinePackageLayoutTableModel(List<SubtypeScheme> currentlySelectedSchemes)
  {
    Assert.expect(currentlySelectedSchemes != null);
    _currentlySelectedSchemes = currentlySelectedSchemes;
  }

  /**
   * @author Tan Hock Zoon
   */
  void clear()
  {
    if (_subtypeSchemes != null)
    {
      for (SubtypeScheme subtypeScheme : _subtypeSchemes)
      {
        for (LibraryPackage libraryPackage : subtypeScheme.getCompleteLibraryPackageList())
        {
          for (LibrarySubtypeScheme librarySubtypeScheme : libraryPackage.getCompleteLibrarySubtypeSchemeList())
          {
            librarySubtypeScheme.clear();
          }

          libraryPackage.clear();
        }

        subtypeScheme.clear();
      }

      _subtypeSchemes.clear();
    }

    if (_currentlySelectedSchemes != null)
    {
      for (SubtypeScheme subtypeScheme : _currentlySelectedSchemes)
      {
        for (LibraryPackage libraryPackage : subtypeScheme.getCompleteLibraryPackageList())
        {
          for (LibrarySubtypeScheme librarySubtypeScheme : libraryPackage.getCompleteLibrarySubtypeSchemeList())
          {
            librarySubtypeScheme.clear();
          }

          libraryPackage.clear();
        }

        subtypeScheme.clear();
      }

      _currentlySelectedSchemes.clear();
    }

    _currentSortColumn = 0;
    _currentSortAscending = true;
  }

  /**
   * @author Tan Hock Zoon
   */
  public void sortColumn(int column, final boolean ascending)
  {
    if(column == ImportRefinePackageLayoutTable._SELECT_INDEX)  // selected or not
      Collections.sort(_subtypeSchemes, new Comparator<SubtypeScheme>()
      {
        public int compare(SubtypeScheme lhPackage, SubtypeScheme rhPackage)
        {
          if( _currentlySelectedSchemes.contains(lhPackage) &&
              _currentlySelectedSchemes.contains(rhPackage) )
            return 0;
          else if( ascending &&
                   _currentlySelectedSchemes.contains(lhPackage) &&
                   _currentlySelectedSchemes.contains(rhPackage) == false)
            return -1;
          else if( ascending == false &&
                   _currentlySelectedSchemes.contains(lhPackage) &&
                   _currentlySelectedSchemes.contains(rhPackage) == false)
            return 1;
          else if( ascending &&
                   _currentlySelectedSchemes.contains(lhPackage) == false &&
                   _currentlySelectedSchemes.contains(rhPackage))
            return 1;
          else if( ascending == false &&
                   _currentlySelectedSchemes.contains(lhPackage) == false &&
                   _currentlySelectedSchemes.contains(rhPackage))
            return -1;
          return 0;
        }
      });
    else if(column == ImportRefinePackageLayoutTable._PACKAGE_INDEX) // package name
      Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.PACKAGE_NAME));
    else if (column == ImportRefinePackageLayoutTable._JOINT_TYPE_INDEX) // joint type
      Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.JOINT_TYPE));
    else if (column == ImportPackageLayoutTable._SUBTYPE_INDEX) // subtype
      Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.SUBTYPE_NAME));
    else if (column == ImportRefinePackageLayoutTable._LIBRARY_PATH_INDEX) //library path
      Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.IMPORT_LIBRARY_PATH));
    else if (column == ImportRefinePackageLayoutTable._LIBRARY_PACKAGE_INDEX) //library package
      Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.IMPORT_LIBRARY_PACKAGE));
    else if (column == ImportRefinePackageLayoutTable._LIBRARY_JOINT_TYPE_INDEX) //library joint type
      Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.IMPORT_LIBRARY_JOINT_TYPE));
    else if (column == ImportRefinePackageLayoutTable._LIBRARY_PROJECT_INDEX) //library project
      Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.IMPORT_LIBRARY_PROJECT));
    else if (column == ImportRefinePackageLayoutTable._LIBRARY_SUBTYPE_INDEX) //library subtype scheme
      Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.IMPORT_LIBRARY_SUBTYPE));
    else if (column == ImportRefinePackageLayoutTable._LIBRARY_SUBTYPE_DATE_INDEX) //library subtype scheme
      Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.IMPORT_LIBRARY_SUBTYPE_DATE));
    else if (column == ImportRefinePackageLayoutTable._MATCHES_INDEX) //match percentage
      Collections.sort(_subtypeSchemes, new SubtypeSchemeComparator(ascending, SubtypeSchemeComparatorEnum.IMPORT_LIBRARY_MATCH_PERCENTAGE));

    _currentSortColumn = column;
    _currentSortAscending = ascending;
  }

  /**
   * @author Tan Hock Zoon
   */
  public int getColumnCount()
  {
    Assert.expect(_tableColumns != null);
    return _tableColumns.length;
  }

  /**
   * @author Tan Hock Zoon
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(_tableColumns != null);
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());

    return _tableColumns[columnIndex];
  }

  /**
   * @author Tan Hock Zoon
   */
  public int getRowCount()
  {
    if (_subtypeSchemes == null)
      return 0;
    else
      return _subtypeSchemes.size();
  }

  /**
   * @author Tan Hock Zoon
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if (_subtypeSchemes == null || _subtypeSchemes.size() == 0)
    {
      return false;
    }

    SubtypeScheme subtypeScheme = _subtypeSchemes.get(rowIndex);

    if (columnIndex == ImportRefinePackageLayoutTable._SELECT_INDEX)
    {
      if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else if (columnIndex == ImportRefinePackageLayoutTable._LIBRARY_PATH_INDEX)
    {
      if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false
          && subtypeScheme.getLibraryPathList().size() > 1)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else if (columnIndex == ImportRefinePackageLayoutTable._LIBRARY_PACKAGE_INDEX)
    {
      if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false
          && subtypeScheme.getLibraryPackageList().size() > 1)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else if (columnIndex == ImportRefinePackageLayoutTable._LIBRARY_JOINT_TYPE_INDEX)
    {
      if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false
          && subtypeScheme.getLibraryPackageList().size() > 0)
      {
        if (subtypeScheme.getSelectedLibraryPackage().getJointTypeList().size() > 1)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }
    }
    else if (columnIndex == ImportRefinePackageLayoutTable._LIBRARY_PROJECT_INDEX)
    {
      return false;
    }
    else if (columnIndex == ImportRefinePackageLayoutTable._LIBRARY_SUBTYPE_INDEX)
    {
      if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false
          && subtypeScheme.getLibraryPackageList().size() > 0)
      {
        if (subtypeScheme.getSelectedLibraryPackage().getLibrarySubtypeSchemesBySelectedJointType().size() > 1)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }
    }
    else
    {
      return false;
    }
  }

  /**
   * @author Tan Hock Zoon
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    SubtypeScheme subtypeScheme = _subtypeSchemes.get(rowIndex);

    switch (columnIndex)
    {
      case ImportRefinePackageLayoutTable._SELECT_INDEX:
      {
        if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false)
        {
          boolean selected = (Boolean)object;
          if (selected)
          {
            if (_currentlySelectedSchemes.contains(subtypeScheme) == false)
            {
              _currentlySelectedSchemes.add(subtypeScheme);
            }
          }
          else
          {
            if (_currentlySelectedSchemes.contains(subtypeScheme) == true)
            {
              _currentlySelectedSchemes.remove(subtypeScheme);
            }
          }

          if(_setSubtypeScheme == true)
            setSelectedSubtypeScheme(subtypeScheme);
        }
        fireTableDataChanged();
        GuiObservable.getInstance().stateChanged(this, SelectionEventEnum.IMPORT_REFINE_PACKAGES_SELECTION);
        break;
      }
      case ImportRefinePackageLayoutTable._LIBRARY_PATH_INDEX:
      {
        subtypeScheme.setSelectedLibraryPath((String) object);

        if (subtypeScheme.getLibraryPackageList().size() > 0)
        {
          subtypeScheme.sortLibraryPackageAndSet();
          //subtypeScheme.getSelectedLibraryPackage().assignComponentTypeToLibrarySubtypeScheme(subtypeScheme.getComponentTypes());
        }

        fireTableDataChanged();
        GuiObservable.getInstance().stateChanged(this, SelectionEventEnum.IMPORT_REFINE_PACKAGES_SELECTION_CHANGE);
        break;
      }
      case ImportRefinePackageLayoutTable._LIBRARY_PACKAGE_INDEX:
      {
        if (subtypeScheme.getLibraryPackageList().size() > 0)
        {
          subtypeScheme.setSelectedLibraryPackage((LibraryPackage) object);
          subtypeScheme.getSelectedLibraryPackage().setSelectedJointType(subtypeScheme.getSelectedLibraryPackage().getJointTypeList().get(0));
          subtypeScheme.getSelectedLibraryPackage().setupLibraryComponentTypeToLibrarySubtypeSchemeMapping();
          //subtypeScheme.getSelectedLibraryPackage().assignComponentTypeToLibrarySubtypeScheme(subtypeScheme.getComponentTypes());
        }

        fireTableDataChanged();
        GuiObservable.getInstance().stateChanged(this, SelectionEventEnum.IMPORT_REFINE_PACKAGES_SELECTION_CHANGE);
        break;
      }
      case ImportRefinePackageLayoutTable._LIBRARY_JOINT_TYPE_INDEX:
      {
        if (subtypeScheme.getLibraryPackageList().size() > 0)
        {
          subtypeScheme.getSelectedLibraryPackage().setSelectedJointType((String) object);
          subtypeScheme.getSelectedLibraryPackage().setupLibraryComponentTypeToLibrarySubtypeSchemeMapping();
          //subtypeScheme.getSelectedLibraryPackage().assignComponentTypeToLibrarySubtypeScheme(subtypeScheme.getComponentTypes());
        }

        fireTableDataChanged();
        GuiObservable.getInstance().stateChanged(this, SelectionEventEnum.IMPORT_REFINE_PACKAGES_SELECTION_CHANGE);
        break;
      }
      case ImportRefinePackageLayoutTable._LIBRARY_PROJECT_INDEX:
      {
        // Do nothing
        break;
      }
      case ImportRefinePackageLayoutTable._LIBRARY_SUBTYPE_INDEX:
      {
        if (subtypeScheme.getLibraryPackageList().size() > 0)
        {
          if (object instanceof LibrarySubtypeScheme)
          {
            subtypeScheme.getSelectedLibraryPackage().clearSelectedLibrarySubtypeSchemes();
            subtypeScheme.getSelectedLibraryPackage().addSelectedLibrarySubtypeScheme((LibrarySubtypeScheme) object);
            subtypeScheme.getSelectedLibraryPackage().assignComponentTypeToSingleLibrarySubtypeScheme(subtypeScheme.getComponentTypes());
          }
          else if (object instanceof String)  // In this case, it is MIXED type
          {
            subtypeScheme.getSelectedLibraryPackage().clearSelectedLibrarySubtypeSchemes();
            subtypeScheme.getSelectedLibraryPackage().assignComponentTypeToLibrarySubtypeScheme(subtypeScheme.getComponentTypes());
            for(ComponentType componentType : subtypeScheme.getComponentTypes())
            {
              subtypeScheme.getSelectedLibraryPackage().addSelectedLibrarySubtypeScheme(componentType.getAssignLibrarySubtypeScheme());
            }
          }
        }

        fireTableDataChanged();
        break;
      }
      default:
        Assert.expect(false);
    }
  }

  /**
   * @author Tan Hock Zoon
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_subtypeSchemes != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    SubtypeScheme subtypeScheme = _subtypeSchemes.get(rowIndex);

    //get the appropriate data item that corresponds to the column
    switch (columnIndex)
    {
      case ImportRefinePackageLayoutTable._SELECT_INDEX:
      {
        value = new Boolean(_currentlySelectedSchemes.contains(subtypeScheme));
        break;
      }
      case ImportRefinePackageLayoutTable._PACKAGE_INDEX:
      {
        value = subtypeScheme.getCompPackage().getShortName();
        break;
      }
      case ImportRefinePackageLayoutTable._JOINT_TYPE_INDEX:
      {
        if (subtypeScheme.usesOneJointTypeEnum())
        {
          value = subtypeScheme.getJointTypeEnum().getName();
        }
        else
        {
          value = _MIXED;
        }

        break;
      }
      case ImportPackageLayoutTable._SUBTYPE_INDEX:
      {
        if (subtypeScheme.usesOneSubtype())
        {
          value = subtypeScheme.getSubtype().getShortName();
        }
        else
        {
          value = _MIXED;
        }

        break;
      }
      case ImportRefinePackageLayoutTable._LIBRARY_PATH_INDEX:
      {
        String libraryPath = null;

        if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false)
        {
          libraryPath = subtypeScheme.getSelectedLibraryPath();
          value = libraryPath.substring(libraryPath.lastIndexOf(File.separator) + 1, libraryPath.length());
        }
        else
        {
          value = _NONE;
        }

        libraryPath = null;
        break;
      }
      case ImportRefinePackageLayoutTable._LIBRARY_PACKAGE_INDEX:
      {
        if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false
            && subtypeScheme.getLibraryPackageList().size() > 0)
        {
          value = subtypeScheme.getSelectedLibraryPackage();
        }
        else
        {
          value = _NONE;
        }

        break;
      }
      case ImportRefinePackageLayoutTable._LIBRARY_JOINT_TYPE_INDEX:
      {
        if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false
            && subtypeScheme.getLibraryPackageList().size() > 0)
        {
          value = subtypeScheme.getSelectedLibraryPackage().getSelectedJointType();
        }
        else
        {
          value = _NONE;
        }

        break;
      }
      case ImportRefinePackageLayoutTable._LIBRARY_PROJECT_INDEX:
      {
        if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false)
        {
          if (subtypeScheme.getSelectedLibraryPackage().hasSelectedMixedLibrarySubtypeScheme())
          {
            if (subtypeScheme.getSelectedLibraryPackage().isSelectedMixedLibraryProjectName())
              value = _MIXED;
            else
              value = subtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getLibraryProjectName();
          }
          else
          {
            value = subtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getLibraryProjectName();
          }
        }
        else
        {
          value = _NONE;
        }
        
        break;
      }
      case ImportRefinePackageLayoutTable._LIBRARY_SUBTYPE_INDEX:
      {
        if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false
            && subtypeScheme.getLibraryPackageList().size() > 0)
        {
          if (subtypeScheme.getSelectedLibraryPackage().hasSelectedMixedLibrarySubtypeScheme())
            value = _MIXED;
          else
            value = subtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme();
        }
        else
        {
          value = _NONE;
        }

        break;
      }
      case ImportRefinePackageLayoutTable._LIBRARY_SUBTYPE_DATE_INDEX:
      {
        if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false
            && subtypeScheme.getLibraryPackageList().size() > 0)
        {
          if (subtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().hasDateUpdated())
            value = _simpleDateFormat.format(subtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getDateUpdated());
          else
            value = _simpleDateFormat.format(subtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getDateCreated());
        }
        else
        {
          value = _NONE;
        }

        break;
      }
      case ImportRefinePackageLayoutTable._MATCHES_INDEX:
      {
        if (subtypeScheme.isLibraryPathToLibraryPackageMapEmpty() == false
            && subtypeScheme.getLibraryPackageList().size() > 0)
        {
          value = subtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getMatchPercentage() + "%";
        }
        else
        {
          value = "0.0%";
        }

        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }
    return value;
  }

  /**
   * @author Tan Hock Zoon
   */
  public List getSelectedPackages()
  {
    Assert.expect(_currentlySelectedSchemes != null);
    return _currentlySelectedSchemes;
  }

  /**
   * @author Tan Hock Zoon
   */
  public void selectAllPackages()
  {
    _setSubtypeScheme = false;
    for (int i = 0; i < getRowCount(); i++)
    {
      setValueAt(new Boolean(true), i, ImportRefinePackageLayoutTable._SELECT_INDEX);
      fireTableCellUpdated(i, ImportRefinePackageLayoutTable._SELECT_INDEX);
    }
    _setSubtypeScheme = true;
  }

  /**
   * @author Tan Hock Zoon
   */
  public void unselectAllPackages()
  {
    _setSubtypeScheme = false;
    for (int i = 0; i < getRowCount(); i++)
    {
      setValueAt(new Boolean(false), i, ImportRefinePackageLayoutTable._SELECT_INDEX);
      fireTableCellUpdated(i, ImportRefinePackageLayoutTable._SELECT_INDEX);
    }
    _setSubtypeScheme = true;
  }

  /**
   * @author Tan Hock Zoon
   */
  SubtypeScheme getSubtypeSchemeAt(int row)
  {
    Assert.expect(_subtypeSchemes != null);
    Assert.expect(row >= 0);

    return _subtypeSchemes.get(row);
  }

  /**
   * @author Tan Hock Zoon
   */
  void populateWithPanelData(List<SubtypeScheme> subtypeSchemes, SearchFilterPersistance searchFilter)
  {
    _subtypeSchemes.clear();

    //define the percentage use for record selection
    int percentageToSelect = _MATCH_PERCENTAGE_IN_GREEN;

    if (searchFilter.isLandPatternGeometryMatchPercentageFilterOn())
    {
      percentageToSelect = searchFilter.getLandPatternGeometryMatchPercentage();
    }

    for (SubtypeScheme subtypeScheme : subtypeSchemes)
    {      
      if (subtypeScheme.getLibraryPathList().size() > 0)
      {
        if (searchFilter.isLandPatternGeometryMatchPercentageFilterOn())
        {
          if (subtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getMatchPercentage() >= percentageToSelect)
          {
            _currentlySelectedSchemes.add(subtypeScheme);
            _subtypeSchemes.add(subtypeScheme);
          }
        }
        else
        {
          _currentlySelectedSchemes.add(subtypeScheme);
          _subtypeSchemes.add(subtypeScheme);
        }
      }
    }

    sortColumn(_currentSortColumn, _currentSortAscending);
  }

  /**
   *
   * @param subtypeScheme
   *
   * @author Tan Hock Zoon
   */
  public void setSelectedSubtypeScheme(SubtypeScheme subtypeScheme)
  {
    Assert.expect(subtypeScheme != null);
    _selectedSubtypeScheme = subtypeScheme;
  }

  /**
   *
   * @return SubtypeScheme
   *
   * @author Tan Hock Zoon
   */
  public SubtypeScheme getSelectedSubtypeScheme()
  {
    Assert.expect(_selectedSubtypeScheme != null);
    return _selectedSubtypeScheme;
  }

  /**
   *
   * @param subtypeScheme
   * @return int
   *
   * @author Tan Hock Zoon
   */
  public int getRowForSubtypeScheme(SubtypeScheme subtypeScheme)
  {
    Assert.expect(subtypeScheme != null);
    if (_subtypeSchemes.contains(subtypeScheme))
      return _subtypeSchemes.indexOf(subtypeScheme);
    else
      return -1;
  }
}
