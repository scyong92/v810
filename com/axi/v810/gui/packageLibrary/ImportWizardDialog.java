package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.home.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * <p>Title: ImportWizardDialog</p>
 *
 * <p>Description: This wizard will allow the user to import the package library to the current project.</p>
 *
 * <p>Company: AXI</p>
 *
 * @author Tan Hock Zoon
 */
public class ImportWizardDialog extends WizardDialog
{
  private final static String _WELCOME_PANEL = "welcomePanel";
  private final static String _LIBRARY_SELECTION_PANEL = "librarySelectionPanel";
  private final static String _PACKAGES_SELECTION_PANEL = "packagesSelectionPanel";
  private final static String _REFINE_IMPORT_PANEL = "refineImportPanel";
  private final static String _RECIPE_SELECTION_PANEL = "recipeSelectionPanel";

  private WizardPanel _currentVisiblePanel = null;
  private Project _project = null;

  private Frame _parent = null;
  private LibraryManager _libraryManager = LibraryManager.getInstance();

  private List<SubtypeScheme> _subtypeSchemes = new ArrayList<SubtypeScheme>();
  private List<SubtypeScheme> _currentlySelectedSchemes = new ArrayList<SubtypeScheme>();
  private List<AvailableProjectData> _projectData = new LinkedList<AvailableProjectData>();
  private List<AvailableProjectData> _currentlySelectedProjectData = new LinkedList<AvailableProjectData>();
  private List<String> _libraryToUseListData = new ArrayList<String>();

  private ProgressDialog _importProgressDialog = null;
  private DatabaseProgressObservable _databaseProgressObservable = DatabaseProgressObservable.getInstance();

  private Map<String, Dimension> _panelDimension = new HashMap<String, Dimension>();
  private SearchFilterPersistance _searchFilterPersistance;
  private ImportLibrarySettingPersistance _importLibrarySettingPersistance;
  private LibraryWizardPersistance _libraryWizardPersistance;
  private LibraryWizardGeneralPersistance _libraryWizardGeneralPersistance;
  
  private ProjectReader _projectReader;

  /**
   * @param parent Frame
   * @param modal boolean
   * @param project Project
   *
   * @author Tan Hock Zoon
   */
  public ImportWizardDialog (Frame parent, boolean modal, Project project)
  {
    super(parent,
          modal,
          StringLocalizer.keyToString("WIZARD_NEXT_KEY"),
          StringLocalizer.keyToString("WIZARD_CANCEL_KEY"),
          StringLocalizer.keyToString("WIZARD_FINISH_KEY"),
          StringLocalizer.keyToString("WIZARD_BACK_KEY") );

    Assert.expect(project != null, "Project is null");
    _parent = parent;
    _project = project;
    _searchFilterPersistance = new SearchFilterPersistance();
    _searchFilterPersistance = _searchFilterPersistance.readSettings();

    _importLibrarySettingPersistance = new ImportLibrarySettingPersistance();
    _importLibrarySettingPersistance = _importLibrarySettingPersistance.readSettings();
    
    _libraryWizardPersistance = new LibraryWizardPersistance();
    _libraryWizardPersistance = _libraryWizardPersistance.readSettings();
    
    _libraryWizardGeneralPersistance = new LibraryWizardGeneralPersistance();
    _libraryWizardGeneralPersistance = _libraryWizardGeneralPersistance.readSettings();
    
    _projectReader = ProjectReader.getInstance();

    populatePanelDimension();
    loadLibraries();
    initWizard();
    LibraryUtil.getInstance().setCancelDatabaseOperation(false);
  }

  /**
   * @author Tan Hock Zoon
   */
  protected void handleNextScreenButtonEvent()
  {
    Assert.expect(_currentVisiblePanel != null, "current visible panel is not set");
    String nextPanelName = null;
    String currentPanelName = _currentVisiblePanel.getWizardName();

    if (currentPanelName.equals(_WELCOME_PANEL))
    {
      nextPanelName = _LIBRARY_SELECTION_PANEL;
    }
    else if (currentPanelName.equals(_LIBRARY_SELECTION_PANEL))
    {
      nextPanelName = _PACKAGES_SELECTION_PANEL;
      saveLibrariesSelectionChanges();
      //Siew Yeng - XCR-3318 - Oval PTH
      try 
      {
        LibraryUtil.getInstance().updateLibraryIfNecessary(_libraryToUseListData);
      } 
      catch (DatastoreException ex) 
      {
        MessageDialog.showErrorDialog(null,
                                    ex.getMessage(),
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
      }
    }
    else if (currentPanelName.equals(_PACKAGES_SELECTION_PANEL))
    {
      nextPanelName = _REFINE_IMPORT_PANEL;
    }
    else if (currentPanelName.equals(_REFINE_IMPORT_PANEL))
    {
      nextPanelName = _RECIPE_SELECTION_PANEL;
    }
    else
    {
      // they better not be on the refine import and press next!
      Assert.expect(false, " on Refine Import screen next button should not be enabled");
    }

    //set the size of the panel
    setCurrentPanelSize();

    CardLayout layout = (CardLayout)_contentPanel.getLayout();
    layout.show(_contentPanel, nextPanelName);
    setCurrentVisiblePanel(nextPanelName);
    _currentVisiblePanel.populatePanelWithData();

    setPreferredSize(_panelDimension.get(_currentVisiblePanel.getWizardName()));
    pack();

    SwingUtils.centerOnComponent(this, _parent);
  }

  /**
   * @author Tan Hock Zoon
   */
  protected void handlePreviousScreenButtonEvent()
  {
    Assert.expect(_currentVisiblePanel != null, "current visible panel is not set");
    String nextPanelName = null;
    String currentPanelName = _currentVisiblePanel.getWizardName();

    if (currentPanelName.equals(_LIBRARY_SELECTION_PANEL))
    {
      nextPanelName = _WELCOME_PANEL;
    }
    else if (currentPanelName.equals(_PACKAGES_SELECTION_PANEL))
    {
      nextPanelName = _LIBRARY_SELECTION_PANEL;
    }
    else if (currentPanelName.equals(_REFINE_IMPORT_PANEL))
    {
      nextPanelName = _PACKAGES_SELECTION_PANEL;
    }
    else if (currentPanelName.equals(_RECIPE_SELECTION_PANEL))
    {
      nextPanelName = _REFINE_IMPORT_PANEL;
    }
    else
    {
      // user is on Welcome Screen and press Back!
      Assert.expect(false, " on Welcome screen back button should not be enabled");
    }

    //set the size of the panel
    setCurrentPanelSize();

    CardLayout layout = (CardLayout)_contentPanel.getLayout();
    layout.show(_contentPanel, nextPanelName);
    setCurrentVisiblePanel(nextPanelName);
    _currentVisiblePanel.populatePanelWithData();

    setPreferredSize(_panelDimension.get(_currentVisiblePanel.getWizardName()));
    pack();

    SwingUtils.centerOnComponent(this, _parent);
  }

  /**
   * @author Tan Hock Zoon
   */
  protected void handleCompleteProcessButtonEvent()
  {
    if (confirmImportOfDialog())
    {
      LibraryUtil.getInstance().setCancelDatabaseOperation(false);

      handleImportEvent();

      if (LibraryUtil.getInstance().cancelDatabaseOperation() == false)
      {
        //remove the undo stack on Test Development
        com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().clear();
        removeUnusedSubtypeFromProject();
        dispose();
      }
    }
  }

  /**
   * @author Tan Hock Zoon
   */
  protected void confirmExitOfDialog()
  {
    // confirm with the user if they want to cancel the dialog

    int response = ChoiceInputDialog.showConfirmDialog(MainMenuGui.getInstance(),
                                                       StringLocalizer.keyToString("GISWK_CONFIRM_EXIT_OF_GUI_KEY"),
                                                        StringLocalizer.keyToString("GISWK_CONFIRM_EXIT_OF_GUI_DIALOG_TITLE_KEY"));


    if (response == JOptionPane.NO_OPTION || response == JOptionPane.CLOSED_OPTION)
      return;

    //set the size of the panel
    setCurrentPanelSize();

    //save the setting since user agreed to cancel the process.
    try
    {
      _libraryWizardPersistance.writeSettings();
      _libraryWizardGeneralPersistance.writeSettings();
    }
    catch (DatastoreException de)
    {
      MessageDialog.showErrorDialog(null,
                                    de.getMessage(),
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }

    // the user wants to cancel.
    dispose();
  }

  /**
   * @author Tan Hock Zoon
   */
  private void initWizard()
  {
    java.util.List<WizardPanel> panels = new ArrayList<WizardPanel>();

    WelcomeScreenPanel welcomePanel = new WelcomeScreenPanel(this, true);
    welcomePanel.setWizardName(_WELCOME_PANEL);
    panels.add(welcomePanel);
    welcomePanel.populatePanelWithData();

    ImportWizardLibrarySelectionPanel libraryPanel = new ImportWizardLibrarySelectionPanel(this, _libraryToUseListData);
    libraryPanel.setWizardName(_LIBRARY_SELECTION_PANEL);
    panels.add(libraryPanel);

    ImportWizardPackagesSelectionPanel packagesPanel = new ImportWizardPackagesSelectionPanel(_project, this, _subtypeSchemes, _libraryWizardGeneralPersistance);
    packagesPanel.setWizardName(_PACKAGES_SELECTION_PANEL);
    panels.add(packagesPanel);

    ImportWizardRefineImportPanel refineImportPanel = new ImportWizardRefineImportPanel(this, 
                                                                                       _subtypeSchemes, 
                                                                                       _currentlySelectedSchemes, 
                                                                                       _libraryToUseListData, 
                                                                                       _searchFilterPersistance,
                                                                                       _importLibrarySettingPersistance,
                                                                                       _libraryWizardGeneralPersistance);
    refineImportPanel.setWizardName(_REFINE_IMPORT_PANEL);
    panels.add(refineImportPanel);
    
    ImportWizardRecipeSelectionPanel recipePanel = new ImportWizardRecipeSelectionPanel(this, _currentlySelectedProjectData);
    recipePanel.setWizardName(_RECIPE_SELECTION_PANEL);
    panels.add(recipePanel);

    // set the panels
    setContentPanels(panels);
    setCurrentVisiblePanel(_WELCOME_PANEL);

    setPreferredSize(_panelDimension.get(_currentVisiblePanel.getWizardName()));
    pack();
  }

  /**
   * @author Tan Hock Zoon
   */
  private void setCurrentVisiblePanel(String currentVisiblePanelName)
  {
    Assert.expect(currentVisiblePanelName != null, "current visible panel name is null");
    Assert.expect(_contentPanels != null, "content panels are null");

    _currentVisiblePanel = null;

    // iterate through the list and find the panel with the name sent in
    for(WizardPanel panel : _contentPanels)
    {
      if(panel.getWizardName().equals(currentVisiblePanelName))
      {
        _currentVisiblePanel = panel;
        break;
      }
    }

    Assert.expect(_currentVisiblePanel != null, "Current visible panel is null and was not found");
  }

  /**
   * @author Wei Chin, Chong
   */
  public void dispose()
  {
    super.dispose();
    unpopulate();

    if (_databaseProgressObservable != null)
    {
      _databaseProgressObservable.deleteObserver(this);
    }

    if (_currentlySelectedSchemes != null)
    {
      for (SubtypeScheme subtypeScheme : _currentlySelectedSchemes)
      {
        for (LibraryPackage libraryPackage : subtypeScheme.getCompleteLibraryPackageList())
        {
          for (LibrarySubtypeScheme librarySubtypeScheme : libraryPackage.getCompleteLibrarySubtypeSchemeList())
          {
            librarySubtypeScheme.clear();
          }

          libraryPackage.clear();
        }

        subtypeScheme.clear();
      }

      _currentlySelectedSchemes.clear();
    }

    if (_subtypeSchemes != null)
    {
      for (SubtypeScheme subtypeScheme : _subtypeSchemes)
      {
        for (LibraryPackage libraryPackage : subtypeScheme.getCompleteLibraryPackageList())
        {
          for (LibrarySubtypeScheme librarySubtypeScheme : libraryPackage.getCompleteLibrarySubtypeSchemeList())
          {
            librarySubtypeScheme.clear();
          }

          libraryPackage.clear();
        }

        subtypeScheme.clear();
      }

      _subtypeSchemes.clear();
    }

    if (_libraryToUseListData != null)
    {
      _libraryToUseListData.clear();
    }

    if (_panelDimension != null)
    {
      _panelDimension.clear();
    }

    if (_searchFilterPersistance != null)
        _searchFilterPersistance.clearFilter();
    
    if (_importLibrarySettingPersistance != null)
      _importLibrarySettingPersistance.clearFilter();

    _subtypeSchemes = null;
    _currentlySelectedSchemes = null;
    _libraryToUseListData = null;
    _panelDimension = null;
    _databaseProgressObservable = null;
    _importProgressDialog = null;
    _libraryManager = null;
    _project = null;
    _parent = null;
    _searchFilterPersistance = null;
    _importLibrarySettingPersistance = null;
    _libraryWizardPersistance = null;
    _libraryWizardGeneralPersistance = null;
  }

  /**
   * @author Wei Chin, Chong
   */
  private void handleImportEvent()
  {
    Assert.expect(_databaseProgressObservable != null);
    Assert.expect(_project != null);
    Assert.expect(_currentlySelectedSchemes != null);

    _databaseProgressObservable.addObserver(this);
    createImportProgressDialog();

    SwingWorkerThread.getInstance2().invokeLater(new Runnable()
    {
      public void run()
      {
        // Initialize Map
        int padCount = 0;
        double stepSize = 0.0;
        Map<Project, List<SubtypeScheme>> projectToSubtypeSchemeMap = new HashMap<Project, List<SubtypeScheme>>();
        
        // Reset everything
        _databaseProgressObservable.reset();
        if (_databaseProgressObservable.isComplete() == false)
        {
          _importProgressDialog.updateProgressBarPrecent((int)_databaseProgressObservable.getPercentDone(),
                                                 StringLocalizer.keyToString("PLWK_IMPORT_UPDATE_THRESHOLD_WAITING_KEY"));
        }
        _databaseProgressObservable.reportProgress(0);
        
        // Looop through selected recipes
        ProjectObservable.getInstance().setEnabled(false);
        try
        {
          for(AvailableProjectData availableProjectData : _currentlySelectedProjectData)
          {
            List<SubtypeScheme> currentlySelectedSubtypeSchemes = new ArrayList<SubtypeScheme>(_currentlySelectedSchemes);
            
            // Start by loading the selected recipe name
            Project project = null;
            if (_project.getName().equalsIgnoreCase(availableProjectData.getProjectName()))
              project = _project;
            else
              project = _projectReader.load(availableProjectData.getProjectName());
            
            // Get panel object
            com.axi.v810.business.panelDesc.Panel panel = project.getPanel();
            
            List<SubtypeScheme> recipeSubtypeSchemes = new ArrayList<SubtypeScheme>();
            for(CompPackage compPackage : panel.getCompPackages())
            {
              recipeSubtypeSchemes.addAll(compPackage.getSubtypeSchemes());
            }
            
            recipeSubtypeSchemes.retainAll(_currentlySelectedSchemes);
            currentlySelectedSubtypeSchemes.retainAll(recipeSubtypeSchemes);
            
            if (projectToSubtypeSchemeMap.containsKey(project) == false)
              projectToSubtypeSchemeMap.put(project, recipeSubtypeSchemes);
            else
              projectToSubtypeSchemeMap.get(project).addAll(recipeSubtypeSchemes);
            
            for(SubtypeScheme subtypeScheme : recipeSubtypeSchemes)
            {
              for(SubtypeScheme currentSelectedSubtypeScheme : currentlySelectedSubtypeSchemes)
              {
                if (currentSelectedSubtypeScheme.equals(subtypeScheme))
                {
                  // Re-assign from currently selected SubtypeScheme                  
                  subtypeScheme.setSelectedLibraryPath(currentSelectedSubtypeScheme.getSelectedLibraryPath());
                  subtypeScheme.setSelectedLibraryPackage(currentSelectedSubtypeScheme.getSelectedLibraryPackage());
                  
                  subtypeScheme.addLibraryPathToLibraryPackageMap(subtypeScheme.getSelectedLibraryPath(), subtypeScheme.getSelectedLibraryPackage());
                  subtypeScheme.getSelectedLibraryPackage().assignComponentType(subtypeScheme.getComponentTypes());
                }
              }                            
            }                        
          }
          
          // Calculate total pad count for each recipe
          for(Map.Entry<Project, List<SubtypeScheme>> entry : projectToSubtypeSchemeMap.entrySet())
          {
            for (SubtypeScheme subtypeScheme : entry.getValue())
            {
              for (ComponentType componentType : subtypeScheme.getComponentTypes())
              {
                padCount += componentType.getPadTypes().size();
              }
            }
          }
          
          if (padCount == 0)
          {
            _databaseProgressObservable.reportProgress(100);
          }
          else
          {
            // Calculate progress bar step size
            stepSize = 1.0 / (padCount * 9) * 100;

            // Initialize step size
            _databaseProgressObservable.setStepSize(stepSize);

            // Start performing the real library import work
            for(Map.Entry<Project, List<SubtypeScheme>> entry : projectToSubtypeSchemeMap.entrySet())
            {
              Project project = entry.getKey();
              List<SubtypeScheme> subtypeSchemes = entry.getValue();

              // Start perform Import process
              if (subtypeSchemes.isEmpty() == false)
              {
                // Start import SubtypeScheme
                importSubtypeScheme(project, subtypeSchemes);

                // Save project
                ProjectWriter.getInstance().save(project, true);
              }

              if (_databaseProgressObservable.isComplete() == false)
                _importProgressDialog.updateProgressBarPrecent((int)_databaseProgressObservable.getPercentDone(),
                                                               StringLocalizer.keyToString("PLWK_IMPORT_UPDATE_THRESHOLD_WAITING_KEY"));
            }
          }
        }
        catch(final FileNotFoundDatastoreException ex) // XCR1529, New Scan Route, khang-wah.chnee
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              LocalizedString savingStr = new LocalizedString("MM_GUI_SCAN_ROUTE_FILE_NOT_FOUND_KEY", 
                                                              new Object[]{ex.getFileName(), FileName.getScanRouteConfigFullPath()});
                
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(), 
                                            StringLocalizer.keyToString(savingStr), 
                                            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                            true);
            }
          });
        }
        catch(final DatastoreException ex)
        {          
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              MainMenuGui.getInstance().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
              MessageDialog.showErrorDialog(
                  MainMenuGui.getInstance(),
                  ex.getLocalizedMessage(),
                  StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                  true);
            }
          });
        }
        finally
        {
          if (projectToSubtypeSchemeMap != null)
            projectToSubtypeSchemeMap.clear();
          
          ProjectObservable.getInstance().setEnabled(true);
        }
      }
    });

    _importProgressDialog.setVisible(true);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void importSubtypeScheme(Project project, List<SubtypeScheme> subtypeSchemes)
  {
    Assert.expect(project != null);
    Assert.expect(subtypeSchemes != null);            

    try
    {
      LibraryUtil.getInstance().importSubtypeScheme(project, 
                                                    subtypeSchemes, 
                                                    _searchFilterPersistance.isLandPatternGeometryMatchPercentageFilterOn(),
                                                    _importLibrarySettingPersistance);
    }
    catch (final DatastoreException ex)
    {
      ex.printStackTrace();
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showErrorDialog(_parent,
                                        StringLocalizer.keyToString(new LocalizedString("PLWK_IMPORT_DATABASE_FAILED_MESSAGE_OF_GUI_KEY",
                                                                                        new Object[]
                                                                                        {StringLocalizer.keyToString("GUI_COMPANY_CONTACT_INFORMATION_KEY")})),
                                        StringLocalizer.keyToString("PLWK_IMPORT_ERROR_MESSAGE_OF_TITLE_KEY"));

        }
      });

      rollbackWhenError(project, subtypeSchemes);
    }
    finally
    {
      if (LibraryUtil.getInstance().cancelDatabaseOperation())
      {
        _importProgressDialog.setVisible(false);
        rollbackWhenError(project, subtypeSchemes);
      }
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  public void unpopulate()
  {
    // shutdown database
    final BusyCancelDialog busyDialog = new BusyCancelDialog(
        (Frame)getOwner(),
        StringLocalizer.keyToString("PLWK_EXPORT_SHUTDOWN_LIBRARY_WAITING_KEY"),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, (Frame)getOwner());
    SwingWorkerThread.getInstance2().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (_project != null)
          {
            for (CompPackage compPackage : _project.getPanel().getCompPackages())
            {
              compPackage.clearUnusedTransientObjects();
            }
          }

          try
          {
            // now lets sleep for 2 seconds so that we can make sure the dialog doesn't just flash.
            Thread.sleep(1000);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }
        }
        finally
        {
          // wait until visible
          while (busyDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          try
          {
            SwingUtils.invokeAndWait(new Runnable()
            {
              public void run()
              {
                busyDialog.dispose();
              }
            });
          }
          catch (InterruptedException ie)
          {
            // do nothing
          }
          catch (java.lang.reflect.InvocationTargetException ite)
          {
            // do nothing
          }
        }
      }
    });
    busyDialog.setVisible(true);
  }

  /**
   * @author Wei Chin, Chong
   */
  private void loadLibraries()
  {
    Assert.expect(_libraryManager != null);

    final BusyCancelDialog busyDialog = new BusyCancelDialog(
        (Frame)getOwner(),
        StringLocalizer.keyToString("PLWK_EXPORT_CHECK_LIBRARY_WAITING_KEY"),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, (Frame)getOwner());
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _libraryManager.load();

          try
          {
            // now lets sleep for 2 seconds so that we can make sure the dialog doesn't just flash.
            Thread.sleep(1000);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }
        }
        catch (final DatastoreException ex)
        {
          ex.printStackTrace();
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              MessageDialog.showErrorDialog(_parent,
                                            StringLocalizer.keyToString(new LocalizedString("PLWK_START_OR_VALIDATE_DATABASE_FAILED_MESSAGE_OF_GUI_KEY",
                                                                                            new Object[]
                                                                                            {StringLocalizer.keyToString("GUI_COMPANY_CONTACT_INFORMATION_KEY")})),
                                            StringLocalizer.keyToString("PLWK_IMPORT_ERROR_MESSAGE_OF_TITLE_KEY"));

            }
          });
        }
        finally
        {
          // wait until visible
          while (busyDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          try
          {
            SwingUtils.invokeAndWait(new Runnable()
            {
              public void run()
              {
                busyDialog.dispose();
              }
            });
          }
          catch (InterruptedException ie)
          {
            // do nothing
          }
          catch (java.lang.reflect.InvocationTargetException ite)
          {
            // do nothing
          }
        }
      }
    });
    busyDialog.setVisible(true);
  }

  /**
   * @author Wei Chin, Chong
   */
  protected boolean confirmImportOfDialog()
  {
    // confirm with the user if they want to export the dialog

    int response = ChoiceInputDialog.showConfirmDialog(MainMenuGui.getInstance(),
                                                       StringLocalizer.keyToString("PLWK_CONFIRM_IMPORT_OF_GUI_KEY"),
                                                       StringLocalizer.keyToString("PLWK_CONFIRM_IMPORT_OF_GUI_DIALOG_TITLE_KEY"));

    if (response == JOptionPane.NO_OPTION || response == JOptionPane.CLOSED_OPTION)
      return false;

    //set the size of the panel
    setCurrentPanelSize();

    //save the setting since user agreed to cancel the process.
    try
    {
      _libraryWizardPersistance.writeSettings();
      _libraryWizardGeneralPersistance.writeSettings();
    }
    catch (DatastoreException de)
    {
      MessageDialog.showErrorDialog(null,
                                    de.getMessage(),
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }

    return true;
  }

  /**
   * @author Wei Chin, Chong
   */
  private void createImportProgressDialog()
  {
    _importProgressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                         StringLocalizer.keyToString("MMGUI_PLWK_NAME_KEY") + " - " + StringLocalizer.keyToString("PLWK_IMPORT_TITLE_KEY"),
                                         StringLocalizer.keyToString("PLWK_IMPORT_THRESHOLD_WAITING_KEY"),
                                         StringLocalizer.keyToString("PLWK_IMPORT_THRESHOLD_COMPLETE_KEY"),
                                         StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                         StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                         0,
                                         100,
                                         true);
    _importProgressDialog.pack();
    _importProgressDialog.addCancelActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        LibraryUtil.getInstance().setCancelDatabaseOperation(true);
      }
    });
    SwingUtils.centerOnComponent(_importProgressDialog, _parent);
  }

  /**
   * this method will handle update calls from the WizardPanelObservable.
   *
   * @author Wei Chin, Chong
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    super.update(observable,object);
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof DatabaseProgressObservable)
        {
          DatabaseProgressObservable databaseProgressObservable = (DatabaseProgressObservable)observable;
          if(databaseProgressObservable.isComplete() == false)
            _importProgressDialog.updateProgressBarPrecent((int)databaseProgressObservable.getPercentDone());
          else
            _importProgressDialog.updateProgressBarPrecent(100);

          if(databaseProgressObservable.getPercentDone() + databaseProgressObservable.getStepSize() >= 99)
            _importProgressDialog.setCancelButtonEnabled(false);
        }
      }
    });
  }

  /**
   * @author Tan Hock Zoon
   */
  private void rollbackWhenError(final Project project, final List<SubtypeScheme> recipeSubtypeSchemes)
  {
    Assert.expect(project != null);
    Assert.expect(recipeSubtypeSchemes != null);

    final BusyCancelDialog busyDialog = new BusyCancelDialog(
        (Frame)getOwner(),
        StringLocalizer.keyToString("PLWK_IMPORT_ROLLBACK_KEY"),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, (Frame)getOwner());
    SwingWorkerThread.getInstance3().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          LibraryUtil.getInstance().rollbackImportSubtypeScheme(project, recipeSubtypeSchemes);

          try
          {
            // now lets sleep for 2 seconds so that we can make sure the dialog doesn't just flash.
            Thread.sleep(1000);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }
        }
        finally
        {
          // wait until visible
          while (busyDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          try
          {
            SwingUtils.invokeAndWait(new Runnable()
            {
              public void run()
              {
                busyDialog.dispose();
              }
            });
          }
          catch (InterruptedException ie)
          {
            // do nothing
          }
          catch (java.lang.reflect.InvocationTargetException ite)
          {
            // do nothing
          }
        }
      }
    });
    busyDialog.setVisible(true);
  }

  /**
   * @author Tan Hock Zoon
   */
  private void removeUnusedSubtypeFromProject()
  {
    Assert.expect(_project != null);

    final BusyCancelDialog busyDialog = new BusyCancelDialog(
        (Frame)getOwner(),
        StringLocalizer.keyToString("PLWK_UPDATING_PROJECT_DATA_KEY"),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, (Frame)getOwner());
    SwingWorkerThread.getInstance2().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          LibraryUtil.getInstance().removeUnusedSubtype(_project.getPanel());

          try
          {
            // now lets sleep for 2 seconds so that we can make sure the dialog doesn't just flash.
            Thread.sleep(1000);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }
        }
        finally
        {
          // wait until visible
          while (busyDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          try
          {
            SwingUtils.invokeAndWait(new Runnable()
            {
              public void run()
              {
                busyDialog.dispose();
              }
            });
          }
          catch (InterruptedException ie)
          {
            // do nothing
          }
          catch (java.lang.reflect.InvocationTargetException ite)
          {
            // do nothing
          }
        }
      }
    });
    busyDialog.setVisible(true);
  }

  /**
   * @author Tan Hock Zoon
   */
  private void saveLibrariesSelectionChanges()
  {
    Assert.expect(_libraryManager != null);
    Assert.expect(_libraryToUseListData != null);

    _libraryManager.removeAllSelectedLibraries();

    for (String libraryPath : _libraryToUseListData)
    {
      try
      {
        _libraryManager.addSelectedLibrary(libraryPath, false);
      }
      catch (DatastoreException de)
      {
        MessageDialog.showErrorDialog(null,
                                      de.getMessage(),
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }

    try
    {
      _libraryManager.save();
    }
    catch (DatastoreException de)
    {
      MessageDialog.showErrorDialog(null,
                                    de.getMessage(),
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Tan Hock Zoon
   */
  private void populatePanelDimension()
  {
    _panelDimension.put(_WELCOME_PANEL, _libraryWizardPersistance.getImportWelcomeScreenSize());
    _panelDimension.put(_LIBRARY_SELECTION_PANEL, _libraryWizardPersistance.getImportLibrarySelectionScreenSize());
    _panelDimension.put(_PACKAGES_SELECTION_PANEL, _libraryWizardPersistance.getImportPackageSelectionScreenSize());
    _panelDimension.put(_REFINE_IMPORT_PANEL, _libraryWizardPersistance.getImportRefineScreenSize());
    _panelDimension.put(_RECIPE_SELECTION_PANEL, _libraryWizardPersistance.getImportRecipeSelectionScreenSize());
  }

  /**
   * @author Tan Hock Zoon
   */
  private void setCurrentPanelSize()
  {
    String currentPanelName = _currentVisiblePanel.getWizardName();

    if (currentPanelName.equals(_WELCOME_PANEL))
    {
      _libraryWizardPersistance.setImportWelcomeScreenSize(getSize());
    }
    else if (currentPanelName.equals(_LIBRARY_SELECTION_PANEL))
    {
      _libraryWizardPersistance.setImportLibrarySelectionScreenSize(getSize());
    }
    else if (currentPanelName.equals(_PACKAGES_SELECTION_PANEL))
    {
      _libraryWizardPersistance.setImportPackageSelectionScreenSize(getSize());
    }
    else if (currentPanelName.equals(_REFINE_IMPORT_PANEL))
    {
      _libraryWizardPersistance.setImportRefineScreenSize(getSize());
    }
    else if (currentPanelName.equals(_RECIPE_SELECTION_PANEL))
    {
      _libraryWizardPersistance.setImportRecipeSelectionScreenSize(getSize());
    }

    populatePanelDimension();
  }
}
