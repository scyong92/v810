package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;
import com.axi.util.*;
import com.axi.v810.datastore.database.*;

/**
 * <p>Company: Agilent Technogies</p>
 *
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class ExportLibraryWizardDialog extends WizardDialog
{
  private final static String _WELCOME_PANEL = "welcomePanel";
  private final static String _EXPORT_PACKAGE_SELECTION_PANEL = "exportPackageSelectionPanel";
  private final static String _EXPORT_SUBTYPE_SELECTION_PANEL = "exportSubtypeSelectionPanel";

  private Frame _parent = null;
  private Project _project = null;
  private LibraryManager _libraryManager = LibraryManager.getInstance();

  private WizardPanel _currentVisiblePanel = null;
  private ExportPackageSelectionPanel _exportPackageSelectionPanel = null;
  private ExportSubTypesSelectionPanel _exportSubTypesSelectionPanel = null;

  private ProgressDialog _exportProgressDialog = null;
  private DatabaseProgressObservable _databaseProgressObservable = DatabaseProgressObservable.getInstance();

  private boolean _timingDebug = false;
  private Map<String, Dimension> _panelDimension = new HashMap<String, Dimension>();
  private LibraryWizardPersistance _libraryWizardPersistance;
  private LibraryWizardGeneralPersistance _libraryWizardGeneralPersistance;

  /**
   * @param parent Frame
   * @param modal boolean
   * @param project Project
   *
   * @author Wei Chin, Chong
   */
  public ExportLibraryWizardDialog(Frame parent, boolean modal, Project project)
  {
    super(parent,
          modal,
          StringLocalizer.keyToString("WIZARD_NEXT_KEY"),
          StringLocalizer.keyToString("WIZARD_CANCEL_KEY"),
          StringLocalizer.keyToString("WIZARD_FINISH_KEY"),
          StringLocalizer.keyToString("WIZARD_BACK_KEY") );

          // note - NOT adding asserts as super class checks for valid parameters.
    Assert.expect(project != null, "Project is null");
    _project = project;
    _parent = parent;

    _libraryWizardPersistance = new LibraryWizardPersistance();
    _libraryWizardPersistance = _libraryWizardPersistance.readSettings();
    
    _libraryWizardGeneralPersistance = new LibraryWizardGeneralPersistance();
    _libraryWizardGeneralPersistance = _libraryWizardGeneralPersistance.readSettings();

    populatePanelDimension();
    loadLibraries();
    populatePackagesLibrary();
    initWizard();
  }

  /**
   * @author Wei Chin, Chong
   */
  private void initWizard()
  {
    // change the title
    changeTitle(StringLocalizer.keyToString("PLWK_EXPORT_TITLE_KEY"));

    java.util.List<WizardPanel> panels = new ArrayList<WizardPanel>();

    // create the 4 panels
    WelcomeScreenPanel welcomeScreenPanel = new WelcomeScreenPanel(this,false);
    welcomeScreenPanel.setWizardName(_WELCOME_PANEL); // not a user defined string no localization needed
    panels.add(welcomeScreenPanel);
    // now lets populate the welcomePanel
    welcomeScreenPanel.populatePanelWithData();

    _exportPackageSelectionPanel = new ExportPackageSelectionPanel( this, _project, _libraryWizardGeneralPersistance);
    _exportPackageSelectionPanel.setWizardName(_EXPORT_PACKAGE_SELECTION_PANEL);// not a user defined string no localization needed
    panels.add(_exportPackageSelectionPanel);

    _exportSubTypesSelectionPanel = new ExportSubTypesSelectionPanel( this, _exportPackageSelectionPanel);
    _exportSubTypesSelectionPanel.setWizardName(_EXPORT_SUBTYPE_SELECTION_PANEL);// not a user defined string no localization needed
    panels.add(_exportSubTypesSelectionPanel);

    // set the panels
    setContentPanels(panels);
    setCurrentVisiblePanel(_WELCOME_PANEL);
    _currentVisiblePanel.revalidate();

    setPreferredSize(_panelDimension.get(_currentVisiblePanel.getWizardName()));
    pack();
  }

  /**
   * @author Wei Chin, Chong
   */
  protected void handleCompleteProcessButtonEvent()
  {
    boolean continueExport = confirmExportOfDialog();

    if(continueExport)
    {
      try
      {
        // the user wants to export.
        handleExportEvent();
        _libraryManager.save();
      }
      catch (DatastoreException ex)
      {
        MessageDialog.showErrorDialog(null,
                              ex.getMessage(),
                              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                              true);
      }
      if (DatabaseManager.getInstance().cancelDatabaseOperation() == false)
        dispose();
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  protected void handleNextScreenButtonEvent()
  {
    Assert.expect(_currentVisiblePanel != null, "current visible panel is not set");

    String nextPanelName = null;
    String currentPanelName = _currentVisiblePanel.getWizardName();

    if(currentPanelName.equals(_WELCOME_PANEL))
    {
      nextPanelName = _EXPORT_PACKAGE_SELECTION_PANEL;
    }
    else if(currentPanelName.equals(_EXPORT_PACKAGE_SELECTION_PANEL))
    {
      nextPanelName = _EXPORT_SUBTYPE_SELECTION_PANEL;
    }
    else
    {
      // they better not be on the summary and press next!
      Assert.expect(false, " on summary screen next button should not be enabled");
    }

    //set the size of the panel
    setCurrentPanelSize();

    CardLayout layout = (CardLayout)_contentPanel.getLayout();
    layout.show(_contentPanel, nextPanelName);

    setCurrentVisiblePanel(nextPanelName);
    _currentVisiblePanel.populatePanelWithData();
    _currentVisiblePanel.revalidate();

    setPreferredSize(_panelDimension.get(_currentVisiblePanel.getWizardName()));
    pack();

    SwingUtils.centerOnComponent(this, _parent);
  }

  /**
   * @author Wei Chin, Chong
   */
  protected void handlePreviousScreenButtonEvent()
  {
    Assert.expect(_currentVisiblePanel != null, "current visible panel is not set");

    String previousPanelName = null;
    String currentPanelName = _currentVisiblePanel.getWizardName();

    if(currentPanelName.equals(_EXPORT_PACKAGE_SELECTION_PANEL))
    {
      previousPanelName = _WELCOME_PANEL;
    }
    else if(currentPanelName.equals(_EXPORT_SUBTYPE_SELECTION_PANEL))
    {
      previousPanelName = _EXPORT_PACKAGE_SELECTION_PANEL;
    }
    else
    {
      Assert.expect(false);
    }

    //set the size of the panel
    setCurrentPanelSize();

    CardLayout layout = (CardLayout)_contentPanel.getLayout();
    layout.show(_contentPanel, previousPanelName);
    setCurrentVisiblePanel(previousPanelName);
    _currentVisiblePanel.populatePanelWithData();
    _currentVisiblePanel.revalidate();

    setPreferredSize(_panelDimension.get(_currentVisiblePanel.getWizardName()));
    pack();

    SwingUtils.centerOnComponent(this, _parent);
  }

  /**
   * @author Wei Chin, Chong
   */
   protected void confirmExitOfDialog()
   {
     // confirm with the user if they want to cancel the dialog

     int response = ChoiceInputDialog.showConfirmDialog(MainMenuGui.getInstance(),
                                                        StringLocalizer.keyToString("GISWK_CONFIRM_EXIT_OF_GUI_KEY"),
                                                        StringLocalizer.keyToString("GISWK_CONFIRM_EXIT_OF_GUI_DIALOG_TITLE_KEY"));

     if (response == JOptionPane.NO_OPTION || response == JOptionPane.CLOSED_OPTION)
       return;

     //set the size of the panel
     setCurrentPanelSize();

     //save the setting since user agreed to cancel the process.
     try
     {
       _libraryWizardPersistance.writeSettings();
       _libraryWizardGeneralPersistance.writeSettings();
     }
     catch (DatastoreException de)
     {
       MessageDialog.showErrorDialog(null,
                                     de.getMessage(),
                                     StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                     true);
     }

     // the user wants to cancel.
     dispose();
   }

  /**
   * @author Wei Chin, Chong
   */
  public void dispose()
  {
    unpopulate();
    super.dispose();
    if (_project != null)
    {
      for (CompPackage compPackage : _project.getPanel().getCompPackages())
      {
        compPackage.clearUnusedTransientObjects();
      }
    }
    _project = null;
    _libraryManager = null;

    if(_panelDimension != null)
    {
      _panelDimension.clear();
      _panelDimension = null;
    }

    _parent = null;
    _currentVisiblePanel = null;
    _exportPackageSelectionPanel = null;
    _exportSubTypesSelectionPanel = null;
    _exportProgressDialog = null;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void unpopulate()
  {
    // shutdown database
    final BusyCancelDialog busyDialog = new BusyCancelDialog(
        (Frame)getOwner(),
        StringLocalizer.keyToString("PLWK_EXPORT_SHUTDOWN_LIBRARY_WAITING_KEY"),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, (Frame)getOwner());
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          AbstractDatabase.getInstance().shutdownDatabase();
        }
        catch(final DatastoreException ex)
        {
          ex.printStackTrace();
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              MessageDialog.showErrorDialog(_parent,
                                            StringLocalizer.keyToString(new LocalizedString("PLWK_SHUTDOWN_DATABASE_FAILED_MESSAGE_OF_GUI_KEY",
                                                                                            new Object[]
                                                                                            {StringLocalizer.keyToString("GUI_COMPANY_CONTACT_INFORMATION_KEY")})),
                                            StringLocalizer.keyToString("PLWK_EXPORT_ERROR_MESSAGE_OF_TITLE_KEY"));
            }
          });
        }
        finally
        {
          busyDialog.setVisible(false);
        }
      }
    });
    busyDialog.setVisible(true);
  }

  /**
   * @param currentVisiblePanelName String
   *
   * @author Wei Chin, Chong
   */
  private void setCurrentVisiblePanel(String currentVisiblePanelName)
  {
    Assert.expect(currentVisiblePanelName != null, "current visible panel name is null");
    Assert.expect(_contentPanels != null, "content panels are null");

    _currentVisiblePanel = null;

    // iterate through the list and find the panel with the name sent in
    for(WizardPanel panel : _contentPanels)
      if(panel.getWizardName().equals(currentVisiblePanelName))
      {
        _currentVisiblePanel = panel;
        break;
      }

    Assert.expect(_currentVisiblePanel != null, "Current visible panel is null and was not found");
  }

  /**
   * @author Wei Chin, Chong
   */
  private void handleExportEvent()
  {
    final ExportLibraryWizardDialog exportLibraryWizardDialog = this;

    createExportProgressDialog();

    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        _databaseProgressObservable.addObserver(exportLibraryWizardDialog);

        _databaseProgressObservable.reset();

        _exportProgressDialog.updateProgressBarPrecent((int) _databaseProgressObservable.getPercentDone(),
                StringLocalizer.keyToString("PLWK_EXPORT_THRESHOLD_WAITING_KEY"));

        DatabaseManager.getInstance().setCancelDatabaseOperation(false);
        java.util.List<SubtypeScheme> subtypeSchemes = _exportSubTypesSelectionPanel.getSelectedSubTypeScheme();

        java.util.List<SubtypeScheme> exportSubtypeSchemes = null;
        java.util.List<SubtypeScheme> updateSubtypeSchemes = null;

        if (subtypeSchemes != null && subtypeSchemes.size() > 0)
        {
          for (SubtypeScheme subtypeScheme : subtypeSchemes)
          {
            if (subtypeScheme.isUpdatable())
            {
              if (updateSubtypeSchemes == null)
                updateSubtypeSchemes = new ArrayList<SubtypeScheme>();
              updateSubtypeSchemes.add(subtypeScheme);
            }
            else
            {
              if (exportSubtypeSchemes == null)
                exportSubtypeSchemes = new ArrayList<SubtypeScheme>();
              exportSubtypeSchemes.add(subtypeScheme);
            }
          }

          try
          {
            _databaseProgressObservable.reportProgress(1);

            int updateSize;
            int exportSize;
            if (updateSubtypeSchemes == null)
              updateSize = 0;
            else
              updateSize = updateSubtypeSchemes.size();

            if (exportSubtypeSchemes == null)
              exportSize = 0;
            else
              exportSize = exportSubtypeSchemes.size();

            double stepSize = 1.0 / (updateSize + exportSize) * 100;

            _databaseProgressObservable.setStepSize(stepSize);

            TimerUtil times = new TimerUtil();

            if (_timingDebug)
              times.start();

            if (exportSubtypeSchemes != null && exportSubtypeSchemes.size() > 0)
            {
              try
              {
                LibraryUtil.getInstance().exportToLibrary(exportSubtypeSchemes);
              }
              catch (final DatastoreException ex)
              {
                ex.printStackTrace();
                SwingUtils.invokeLater(new Runnable()
                {

                  public void run()
                  {
                    MessageDialog.showErrorDialog(_parent,
                            StringLocalizer.keyToString(new LocalizedString("PLWK_EXPORT_DATABASE_FAILED_MESSAGE_OF_GUI_KEY",
                            new Object[]
                            {
                              StringLocalizer.keyToString("GUI_COMPANY_CONTACT_INFORMATION_KEY")
                            })),
                            StringLocalizer.keyToString("PLWK_EXPORT_ERROR_MESSAGE_OF_TITLE_KEY"));

                  }
                });
                _databaseProgressObservable.reset();
                _exportProgressDialog.dispose();
                return;
              }
            }

            if (_timingDebug)
              System.out.println("ExportToLibrary Times taken : " + times.getElapsedTimeInMillis());

            if (updateSubtypeSchemes != null && updateSubtypeSchemes.size() > 0)
            {
              _exportProgressDialog.updateProgressBarPrecent((int) _databaseProgressObservable.getPercentDone(),
                      StringLocalizer.keyToString("PLWK_EXPORT_UPDATE_THRESHOLD_WAITING_KEY"));
              try
              {
                LibraryUtil.getInstance().updateToLibrary(updateSubtypeSchemes);
              }
              catch (final DatastoreException ex)
              {
                ex.printStackTrace();
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    MessageDialog.showErrorDialog(_parent,
                            StringLocalizer.keyToString(new LocalizedString("PLWK_UPDATE_DATABASE_FAILED_MESSAGE_OF_GUI_KEY",
                            new Object[]
                            {
                              StringLocalizer.keyToString("GUI_COMPANY_CONTACT_INFORMATION_KEY")
                            })),
                            StringLocalizer.keyToString("PLWK_EXPORT_ERROR_MESSAGE_OF_TITLE_KEY"));

                  }
                });
                _databaseProgressObservable.reset();
                _exportProgressDialog.dispose();
                return;
              }
            }

            if (_timingDebug)
              System.out.println("ExportToLibrary + UpdateToLibrary Times taken : " + times.getElapsedTimeInMillis());

          }
          finally
          {
            if (DatabaseManager.getInstance().cancelDatabaseOperation() == false)
            {
              if(exportSubtypeSchemes != null)
              {
                for (SubtypeScheme subtypeScheme : exportSubtypeSchemes)
                  subtypeScheme.getCompPackage().saveExportState();
              }

              if(updateSubtypeSchemes != null)
              {
                for (SubtypeScheme subtypeScheme : updateSubtypeSchemes)
                  subtypeScheme.getCompPackage().saveExportState();
              }
            }

            if(exportSubtypeSchemes != null)
              exportSubtypeSchemes.clear();

            if(updateSubtypeSchemes != null)
              updateSubtypeSchemes.clear();

            exportSubtypeSchemes = null;
            updateSubtypeSchemes = null;
            _databaseProgressObservable.deleteObserver(exportLibraryWizardDialog);
          }
        }
        else
        {
          _databaseProgressObservable.reportProgress(100);
        }
      }
    });
    _exportProgressDialog.setVisible(true);
  }

  /**
   * @author Wei Chin, Chong
   */
  private void loadLibraries()
  {
    final BusyCancelDialog busyDialog = new BusyCancelDialog(
        (Frame)getOwner(),
        StringLocalizer.keyToString("PLWK_EXPORT_CHECK_LIBRARY_WAITING_KEY"),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, (Frame)getOwner());
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _libraryManager.load();
        }
        catch (final DatastoreException ex)
        {
          // do nothing because failed to load library is not an issue to continue the export
//          SwingUtils.invokeLater(new Runnable()
//          {
//            public void run()
//            {
//              MessageDialog.showErrorDialog(_parent,
//                                            StringLocalizer.keyToString(new LocalizedString("PLWK_START_OR_VALIDATE_DATABASE_FAILED_MESSAGE_OF_GUI_KEY",
//                                                                                            new Object[]
//                                                                                            {ex.getLocalizedMessage()})),
//                                            StringLocalizer.keyToString("PLWK_EXPORT_ERROR_MESSAGE_OF_TITLE_KEY"));
//
//            }
//          });
        }
        finally
        {
          // wait until visible
          while (busyDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          try
          {
            SwingUtils.invokeAndWait(new Runnable()
            {
              public void run()
              {
                busyDialog.dispose();
              }
            });
          }
          catch (InterruptedException ie)
          {
            // do nothing
          }
          catch (java.lang.reflect.InvocationTargetException ite)
          {
            // do nothing
          }
        }
      }
    });
    busyDialog.setVisible(true);
  }

  /**
   * must call after load library
   * @author Wei Chin, Chong
   */
  private void populatePackagesLibrary()
  {
    Assert.expect(_project != null);

    for (CompPackage compPackage : _project.getPanel().getCompPackages())
    {
      if (compPackage.hasSourceLibrary())
      {
        compPackage.setLibraryPath(compPackage.getSourceLibrary());
        try
        {
          _libraryManager.add(compPackage.getSourceLibrary());
        }
        catch (final DatastoreException ex)
        {
          ex.printStackTrace();
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              MessageDialog.showErrorDialog(_parent,
                                            StringLocalizer.keyToString(new LocalizedString("PLWK_START_OR_VALIDATE_DATABASE_FAILED_MESSAGE_OF_GUI_KEY",
                                                                                            new Object[]
                                                                                            {StringLocalizer.keyToString("GUI_COMPANY_CONTACT_INFORMATION_KEY")})),
                                            StringLocalizer.keyToString("PLWK_EXPORT_ERROR_MESSAGE_OF_TITLE_KEY"));

            }
          });
        }
      }
      else
        compPackage.setLibraryPath("");
    }
  }

  /**
   * @return boolean
   *
   * @author Wei Chin, Chong
   */
  protected boolean confirmExportOfDialog()
  {
    // confirm with the user if they want to export the dialog

    int response = ChoiceInputDialog.showConfirmDialog(MainMenuGui.getInstance(),
                                                       StringLocalizer.keyToString("PLWK_CONFIRM_EXPORT_OF_GUI_KEY"),
                                                       StringLocalizer.keyToString("PLWK_CONFIRM_EXPORT_OF_GUI_DIALOG_TITLE_KEY"));

    if (response == JOptionPane.NO_OPTION || response == JOptionPane.CLOSED_OPTION)
      return false;

    //set the size of the panel
    setCurrentPanelSize();

    //save the setting since user agreed to cancel the process.
    try
    {
      _libraryWizardPersistance.writeSettings();
      _libraryWizardGeneralPersistance.writeSettings();
    }
    catch (DatastoreException de)
    {
      MessageDialog.showErrorDialog(null,
                                    de.getMessage(),
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }

    return true;
  }

  /**
   * @author Wei Chin, Chong
   */
  private void createExportProgressDialog()
  {
    _exportProgressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                         StringLocalizer.keyToString("MMGUI_PLWK_NAME_KEY") + " - " + StringLocalizer.keyToString("PLWK_EXPORT_TITLE_KEY"),
                                         StringLocalizer.keyToString("PLWK_EXPORT_THRESHOLD_WAITING_KEY"),
                                         StringLocalizer.keyToString("PLWK_EXPORT_THRESHOLD_COMPLETE_KEY"),
                                         StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                         StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                         StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                         0,
                                         100,
                                         true);
    _exportProgressDialog.pack();
    _exportProgressDialog.addCancelActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exportProgressCancelActionPerformed(e);
      }
    });
    SwingUtils.centerOnComponent(_exportProgressDialog, _parent);
  }

  /**
   * @param e ActionEvent
   * @author Wei Chin, Chong
   */
  public void exportProgressCancelActionPerformed(ActionEvent e)
  {
    DatabaseManager.getInstance().setCancelDatabaseOperation(true);
    _databaseProgressObservable.reset();
    _exportProgressDialog.dispose();
  }

  /**
   * @author Wei Chin, Chong
   */
  private void populatePanelDimension()
  {
    _panelDimension.put(_WELCOME_PANEL, _libraryWizardPersistance.getExportWelcomeScreenSize());
    _panelDimension.put(_EXPORT_PACKAGE_SELECTION_PANEL, _libraryWizardPersistance.getExportPackageSelectionScreenSize());
    _panelDimension.put(_EXPORT_SUBTYPE_SELECTION_PANEL, _libraryWizardPersistance.getExportSubtypeSelectionScreenSize());
  }

  /*
   * this method will handle update calls from the WizardPanelObservable.
   *
   * @param observable Observable
   * @param object Object
   *
   * @author Wei Chin, Chong
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    super.update(observable,object);
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof DatabaseProgressObservable)
        {
          DatabaseProgressObservable databaseProgressObservable = (DatabaseProgressObservable)observable;

          if(databaseProgressObservable.isComplete() == false)
            _exportProgressDialog.updateProgressBarPrecent((int)databaseProgressObservable.getPercentDone());
          else
            _exportProgressDialog.updateProgressBarPrecent(100);

          if(databaseProgressObservable.getPercentDone() + databaseProgressObservable.getStepSize() >= 99)
            _exportProgressDialog.setCancelButtonEnabled(false);
        }
      }
    });
  }

  /**
   * @author Tan Hock Zoon
   */
  private void setCurrentPanelSize()
  {
    String currentPanelName = _currentVisiblePanel.getWizardName();

    if (currentPanelName.equals(_WELCOME_PANEL))
    {
      _libraryWizardPersistance.setExportWelcomeScreenSize(getSize());
    }
    else if (currentPanelName.equals(_EXPORT_PACKAGE_SELECTION_PANEL))
    {
      _libraryWizardPersistance.setExportPackageSelectionScreenSize(getSize());
    }
    else if (currentPanelName.equals(_EXPORT_SUBTYPE_SELECTION_PANEL))
    {
      _libraryWizardPersistance.setExportSubtypeSelectionScreenSize(getSize());
    }

    populatePanelDimension();
  }

}
