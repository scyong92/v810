package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.filechooser.FileFilter;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class ExportWizardNewLibraryDialog extends EscapeDialog
{
  private BorderLayout _mainBorderLayout;
  private Border _buttonTitleBorder;

  private JLabel _emptyLabel;
  private JLabel _libraryNameLabel;
  private JLabel _libraryPathLabel;

  private JTextField _libraryNameTextField;
  private JTextField _libraryPathTextField;
  private JFileChooser _fileChooser = null;

  private JPanel _centerPanel;
  private JPanel _centerTopPanel;
  private JPanel _centerMiddlePanel;
  private JPanel _centerBottomPanel;
  private JPanel _centerBottomSelectionPanel;
  private JPanel _southPanel;
//  private JScrollPane _scrollingArea;

  private ButtonGroup _updateTypeButtonGroup;
  private JRadioButton _updateSelectedRowRadioButton;
  private JRadioButton _updateAllNoneRowRadioButton;
  private JRadioButton _updateAllRowRadioButton;
  private JButton _okButton;
  private JButton _cancelButton;
  private JButton _browseButton;

  private String _outputDirectory = null;
  private java.util.List<CompPackage> _selectedCompPackages = null;
  private LibraryManager _libraryManager = null;
  private java.util.List<CompPackage> _compPackages = null;
  private ExportPackageSelectionPanel _exportPackageSelectionPanel = null;

  private LibraryDirectoryPersistance _libraryDirectoryPersistance = null;

  /**
   * @param parentFrame Frame
   * @param modal boolean
   * @param selectedCompPackages List
   * @param compPackages List
   * @param exportPackageSelectionPanel ExportPackageSelectionPanel
   *
   * @author Wei Chin, Chong
   */
  ExportWizardNewLibraryDialog(Frame parentFrame,
                               boolean modal,
                               java.util.List<CompPackage> selectedCompPackages,
                               java.util.List<CompPackage> compPackages,
                               ExportPackageSelectionPanel exportPackageSelectionPanel)
  {
    super(parentFrame, modal);

    Assert.expect(selectedCompPackages != null);
    Assert.expect(compPackages != null);
    Assert.expect(exportPackageSelectionPanel != null);

    _selectedCompPackages = selectedCompPackages;
    _compPackages = compPackages;
    _exportPackageSelectionPanel = exportPackageSelectionPanel;
    _libraryManager = LibraryManager.getInstance();
    _libraryDirectoryPersistance = new LibraryDirectoryPersistance();
    _libraryDirectoryPersistance = _libraryDirectoryPersistance.readSettings();

    createPanel();
  }

  /**
   * @author Wei Chin, Chong
   */
  private void init()
  {
    _mainBorderLayout = new BorderLayout(5,5);
    _buttonTitleBorder = new TitledBorder(BorderFactory.createEtchedBorder(
            Color.white, new Color(165, 163, 151)),
              StringLocalizer.keyToString("PLWK_APPLY_NEW_LIBRARY_OPTIONS_TITLE_KEY"));
    setTitle(StringLocalizer.keyToString("PLWK_EXPORT_CREATE_NEW_LIBRARY_TITLE_KEY"));
    _libraryNameLabel = new JLabel(StringLocalizer.keyToString("PLWK_EXPORT_LIBRARY_NAME_LABEL_KEY"));
    _libraryPathLabel = new JLabel(StringLocalizer.keyToString("PLWK_EXPORT_LIBRARY_PATH_LABEL_KEY"));
    _emptyLabel = new JLabel();

    _libraryNameTextField = new JTextField(25);
    _libraryPathTextField  = new JTextField(25);

    _updateTypeButtonGroup = new ButtonGroup();
    _updateSelectedRowRadioButton = new JRadioButton(StringLocalizer.keyToString("PLWK_EXPORT_LIBRARY_UPDATE_SELECTED_RADIOBUTTON_KEY"));
    _updateAllNoneRowRadioButton = new JRadioButton(StringLocalizer.keyToString("PLWK_EXPORT_LIBRARY_UPDATE_ALL_NONE_ROW_RADIOBUTTON_KEY"));
    _updateAllRowRadioButton = new JRadioButton(StringLocalizer.keyToString("PLWK_EXPORT_LIBRARY_UPDATE_ALL_ROW_RADIOBUTTON_KEY"));
    _okButton = new JButton(StringLocalizer.keyToString("PLWK_OK_BUTTON_KEY"));
    _cancelButton = new JButton(StringLocalizer.keyToString("PLWK_CANCEL_BUTTON_KEY"));
    _browseButton = new JButton(StringLocalizer.keyToString("GUI_BROWSE_BUTTON_KEY"));

    _centerPanel = new JPanel(new BorderLayout());
    _centerTopPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
    _centerMiddlePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
    _centerBottomPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _centerBottomSelectionPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
    _southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 5, 5));
  }

  /**
   * @author Wei Chin, Chong
   */
  private void createPanel()
  {
    init();
    setLayout(_mainBorderLayout);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

    _centerBottomSelectionPanel.setBorder(_buttonTitleBorder);
    if( _selectedCompPackages.size() == 0)
      _updateSelectedRowRadioButton.setEnabled(false);

    _updateTypeButtonGroup.add(_updateAllNoneRowRadioButton);
    _updateTypeButtonGroup.add(_updateSelectedRowRadioButton);
    _updateTypeButtonGroup.add(_updateAllRowRadioButton);

    SwingUtils.makeAllSameLength(new JLabel[]{_libraryNameLabel, _libraryPathLabel});
    SwingUtils.makeAllSameLength(new JTextField[]{_libraryNameTextField, _libraryPathTextField});
    SwingUtils.makeAllSameLength(new JComponent[]{_okButton, _cancelButton, _browseButton, _emptyLabel});

    _centerTopPanel.add(_libraryNameLabel);
    _centerTopPanel.add(_libraryNameTextField);

    _centerMiddlePanel.add(_libraryPathLabel);
    _centerMiddlePanel.add(_libraryPathTextField);
    _centerMiddlePanel.add(_browseButton);

    _centerBottomSelectionPanel.add(_updateAllNoneRowRadioButton);
    _centerBottomSelectionPanel.add(_updateSelectedRowRadioButton);
    _centerBottomSelectionPanel.add(_updateAllRowRadioButton);

    _centerBottomPanel.add(_centerBottomSelectionPanel);
    _centerBottomPanel.add(_emptyLabel);

    _centerPanel.add(_centerTopPanel,BorderLayout.NORTH);
    _centerPanel.add(_centerMiddlePanel,BorderLayout.CENTER);
    _centerPanel.add(_centerBottomPanel,BorderLayout.SOUTH);
    _southPanel.add(_okButton);
    _southPanel.add(_cancelButton);
    add(_centerPanel,BorderLayout.CENTER);
    add(_southPanel,BorderLayout.SOUTH);

    _updateAllNoneRowRadioButton.setSelected(true);

    _libraryNameTextField.addKeyListener(new KeyListener()
    {
      public void keyTyped(KeyEvent e)
      {
        //do nothing
      }

      public void keyPressed(KeyEvent e)
      {
        //do nothing
      }

      public void keyReleased(KeyEvent e)
      {
        validateInput();
        if(e.getKeyCode() == KeyEvent.VK_ENTER)
          ok_actionPerformed();
      }
    });

    _libraryPathTextField.addKeyListener(new KeyListener()
    {
      public void keyTyped(KeyEvent e)
      {
        //do nothing
      }

      public void keyPressed(KeyEvent e)
      {
        //do nothing
      }

      public void keyReleased(KeyEvent e)
      {
        validateInput();
        if(e.getKeyCode() == KeyEvent.VK_ENTER)
          ok_actionPerformed();
      }
    });


    _browseButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        browse_actionPerformed();
      }
    });

    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ok_actionPerformed();
      }
    });

    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });

    _okButton.setEnabled(false);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void populateDialogWithData()
  {
    _outputDirectory = _libraryDirectoryPersistance.getPackageLibraryDirectory();
    _libraryPathTextField.setText(_outputDirectory);
  }

  /**
   * @author Wei Chin, Chong
   */
  private void browse_actionPerformed()
  {
    String directoryResult = selectDirectory();
    if (directoryResult.length() > 0) // if the user cancels, don't lose his previous directory information
    {
      _outputDirectory = directoryResult;
      _libraryPathTextField.setText(_outputDirectory);
      validateInput();
    }
  }

  /**
   * This method displays a file chooser and return the directory selected by the user.
   * @return String
   *
   * @author Wei Chin, Chong
   */
  private String selectDirectory()
  {
    String dirName = null;
    File outputDirectory = null;

    if (FileUtil.exists(_libraryPathTextField.getText()))
    {
      _outputDirectory = _libraryPathTextField.getText();
    }

    outputDirectory = new File(_outputDirectory);

    if ( _fileChooser == null )
    {
       _fileChooser = new JFileChooser();

       // initialize the JFIle Dialog.
       FileFilter fileFilter = _fileChooser.getAcceptAllFileFilter();
       _fileChooser.removeChoosableFileFilter(fileFilter);

       _fileChooser.setFileHidingEnabled(false);
       _fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
       _fileChooser.setSelectedFile(outputDirectory);
       _fileChooser.setCurrentDirectory(outputDirectory);
    }
    else
    {
      _fileChooser.setSelectedFile(outputDirectory);
      _fileChooser.setCurrentDirectory(outputDirectory);
    }

    if ( _fileChooser.showDialog(this,StringLocalizer.keyToString("GUI_SELECT_FOLDER_KEY")) != JFileChooser.APPROVE_OPTION )
      return "";

    File selectedFile = _fileChooser.getSelectedFile();

    if (selectedFile.exists() == true)
    {
      if (selectedFile.isDirectory() == true)
      {
        dirName = selectedFile.getPath();
      }
    }

    return dirName;
  }

  /**
   * @author Wei Chin, Chong
   */
  private void validateInput()
  {
    if(_libraryNameTextField.getText() != null &&
       _libraryNameTextField.getText().length() > 0 &&
       _libraryPathTextField.getText() != null &&
       _libraryPathTextField.getText().length() > 0 )
    {
      _okButton.setEnabled(true);
    }
    else
      _okButton.setEnabled(false);
  }

  /**
   * @param libraryFullPath String
   * @throws DatastoreException
   *
   * @author Wei Chin, Chong
   */
  private void updateLibraryPath(String libraryFullPath) throws DatastoreException
  {
    Assert.expect(libraryFullPath != null);

    if(_libraryManager.getLibraries().contains(libraryFullPath))
      _libraryManager.remove(libraryFullPath);

    _libraryManager.add(libraryFullPath);

    if (_updateSelectedRowRadioButton.isSelected())
    {
      for (CompPackage selectedCompPackage : _selectedCompPackages)
      {
        selectedCompPackage.setLibraryPath(libraryFullPath);
      }
    }
    else if (_updateAllNoneRowRadioButton.isSelected())
    {
      for (CompPackage compPackage : _compPackages)
      {
        if (compPackage.hasLibraryPath() == false)
        {
          compPackage.setLibraryPath(libraryFullPath);
        }
      }
    }
    else if (_updateAllRowRadioButton.isSelected())
    {
      for (CompPackage compPackage : _compPackages)
      {
        compPackage.setLibraryPath(libraryFullPath);
      }
    }
    else
    {
      Assert.expect(false);
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  private void ok_actionPerformed()
  {
    String libraryName = _libraryNameTextField.getText();

    if( FileName.hasIllegalChars(libraryName) )
    {
      invalidLibraryNameMessage(libraryName, true);
      return;
    }

    if (FileUtil.exists(_libraryPathTextField.getText()) == false)
    {
      _libraryPathTextField.selectAll();

      MessageDialog.showWarningDialog(this,
                                StringLocalizer.keyToString("PLWK_EXPORT_INVALID_LIBRARY_PATH_WARNING_KEY"),
                                StringLocalizer.keyToString("PLWK_WARNING_TITLE_KEY"),
                                true);
    }
    else
    {
      _outputDirectory = _libraryPathTextField.getText();

      final String libraryFullPath =  FileUtil.getAbsolutePath(FileUtil.concatFileNameToPath(_outputDirectory,libraryName));

      try
      {
        if (LibraryManager.doesLibraryExist(libraryFullPath) ||
            LibraryUtil.getInstance().doeslibraryExist(libraryFullPath))
        {
          MessageDialog.showWarningDialog(this,
                                          StringLocalizer.keyToString("PLWK_EXPORT_LIBRARY_PATH_ALREADY_EXIST_WARNING_KEY"),
                                          StringLocalizer.keyToString("PLWK_WARNING_TITLE_KEY"),
                                          true);
          return;
        }
      }
      catch (XrayTesterException dex)
      {
        // fail to shutdown database exception
        MessageDialog.showErrorDialog(this,
                                       dex,
                                       StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                       true);
        return;
      }
      final BusyCancelDialog busyDialog = new BusyCancelDialog(
          this,
          StringLocalizer.keyToString("PLWK_EXPORT_CREATE_NEW_LIBRARY_WAITING_KEY"),
          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
          true);
      SwingUtils.centerOnComponent(busyDialog, this);
      SwingWorkerThread.getInstance().invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            LibraryUtil.getInstance().createNewLibrary(libraryFullPath);
            updateLibraryPath(libraryFullPath);
          }
          catch(final DatastoreException ex)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                MessageDialog.showErrorDialog(ExportWizardNewLibraryDialog.this,
                        StringLocalizer.keyToString(new LocalizedString("PLWK_CREATE_DATABASE_FAILED_MESSAGE_OF_GUI_KEY",
                        new Object[]{StringLocalizer.keyToString("GUI_COMPANY_CONTACT_INFORMATION_KEY")})),
                        StringLocalizer.keyToString("PLWK_EXPORT_ERROR_MESSAGE_OF_TITLE_KEY"),
                        true);
              }
            });
          }
          finally
          {
            _libraryDirectoryPersistance.setPackageLibraryDirectory(_outputDirectory);
            try
            {
              _libraryDirectoryPersistance.writeSettings();
            }
            catch (DatastoreException dex)
            {
              MessageDialog.showErrorDialog(ExportWizardNewLibraryDialog.this,
                      StringLocalizer.keyToString("PLWK_ERROR_WRITING_PATH_TO_FILE_KEY"),
                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                      true);
            }
            _exportPackageSelectionPanel.populateTableWithFilterData();
            _exportPackageSelectionPanel.validatePanel();
            // wait until visible
            while (busyDialog.isVisible() == false)
            {
              try
              {
                Thread.sleep(200);
              }
              catch (InterruptedException ex)
              {
                // do nothing
              }
            }
            try
            {
              SwingUtils.invokeAndWait(new Runnable()
              {
                public void run()
                {
                  busyDialog.dispose();
                }
              });
            }
            catch (InterruptedException ie)
            {
              // do nothing
            }
            catch (java.lang.reflect.InvocationTargetException ite)
            {
              // do nothing
            }
            busyDialog.dispose();
          }
        }
      });
      busyDialog.setVisible(true);
      dispose();
    }
  }

  /**
   * @param libraryName String
   * @param insertFixMessage boolean
   *
   * @author Wei Chin, Chong
   */
  private void invalidLibraryNameMessage(String libraryName, boolean insertFixMessage)
  {
    LocalizedString localizedMessage = new LocalizedString("PLWK_INVALID_LIBRARY_NAME_KEY",
        new Object[]{libraryName, FileName.getIllegalChars()});

    String finalMessage = StringLocalizer.keyToString(localizedMessage);
    if (insertFixMessage)
    {
      finalMessage += "\n\n" + StringLocalizer.keyToString("PLWK_FIX_LIBRARY_NAME_KEY");
    }
    MessageDialog.showWarningDialog(this,
                                    finalMessage,
                                    StringLocalizer.keyToString("PLWK_WARNING_TITLE_KEY"),
                                    true);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void dispose()
  {
    _outputDirectory = null;

    if(_selectedCompPackages != null)
    {
      _selectedCompPackages.clear();
      _selectedCompPackages = null;
    }
    if(_compPackages != null)
    {
      _compPackages.clear();
      _compPackages = null;
    }
    if(_libraryDirectoryPersistance != null)
    {
      _libraryDirectoryPersistance.clearFilter();
      _libraryDirectoryPersistance = null;
    }
    super.dispose();
  }
}
