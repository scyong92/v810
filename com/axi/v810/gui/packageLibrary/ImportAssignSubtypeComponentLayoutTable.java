package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;

public class ImportAssignSubtypeComponentLayoutTable extends JSortTable
{
  public static final int _COMPONENT_INDEX = 0;
  public static final int _PACKAGE_INDEX = 1;
  public static final int _JOINT_TYPE_INDEX = 2;
  public static final int _SUBTYPE_INDEX = 3;
  public static final int _LIBRARY_JOINT_TYPE_INDEX = 4;
  public static final int _LIBRARY_PROJECT_INDEX = 5;
  public static final int _LIBRARY_SUBTYPE_INDEX = 6;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _COMPONENT_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _PACKAGE_COLUMN_WIDTH_PERCENTAGE = .2;
  private static final double _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _SUBTYPE_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _LIBRARY_JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _LIBRARY_PROJECT_COLUMN_WIDTH_PERCENTAGE = .2;
  private static final double _LIBRARY_SUBTYPE_COLUMN_WIDTH_PERCENTAGE = .2;

  private LibrarySubtypeCellEditor _librarySubtypeCellEditor;

  /**
   * @author Tan Hock Zoon
   */
  public ImportAssignSubtypeComponentLayoutTable()
  {
    // do nothing
  }

  /**
   * @author Tan Hock Zoon
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_COMPONENT_INDEX).setPreferredWidth((int)(totalColumnWidth * _COMPONENT_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PACKAGE_INDEX).setPreferredWidth((int)(totalColumnWidth * _PACKAGE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_JOINT_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_SUBTYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _SUBTYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_LIBRARY_JOINT_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _LIBRARY_JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_LIBRARY_PROJECT_INDEX).setPreferredWidth((int)(totalColumnWidth * _LIBRARY_PROJECT_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_LIBRARY_SUBTYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _LIBRARY_SUBTYPE_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * @author Wei Chin, Chong
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    _librarySubtypeCellEditor = new LibrarySubtypeCellEditor(new JComboBox());
    columnModel.getColumn(_LIBRARY_SUBTYPE_INDEX).setCellEditor(_librarySubtypeCellEditor);
  }

  /**
   *
   * @param e MouseEvent
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String getToolTipText(MouseEvent e)
  {
    Point p = e.getPoint();
    int row = rowAtPoint(p);
    int col = columnAtPoint(p);

    if (row >= 0 && col >= 0)
    {
      return getValueAt(row, col).toString();
    }
    else
    {
      return null;
    }
  }

}
