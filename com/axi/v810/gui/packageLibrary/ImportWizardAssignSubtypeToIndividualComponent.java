package com.axi.v810.gui.packageLibrary;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.database.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.util.*;
import javax.swing.table.TableCellEditor;

public class ImportWizardAssignSubtypeToIndividualComponent extends EscapeDialog implements Observer
{
  private JPanel _componentViewPanel;
  private JPanel _componentDetailsPanel;
  private JPanel _mainPanel;
  private JPanel _buttonPanel;
  private TitledBorder _componentViewTitleBorder;
  private TitledBorder _componentDetailsTitleBorder;
  private JButton _okButton;
  private JButton _cancelButton;

  private ImportAssignSubtypeComponentLayoutTableModel _assignSubtypeComponentTableModel;
  private ImportAssignSubtypeComponentLayoutTable _assignSubtypeComponentTable;
  private ImportAssignSubtypeComponentDetailLayoutTable _assignSubtypeComponentDetailTable;
  private ImportAssignSubtypeComponentDetailLayoutTableModel _assignSubtypeComponentDetailTableModel;

  private JScrollPane _componentScrollPane;
  private JScrollPane _componentDetailsScrollPane;

  // listener for row table selection
  private ListSelectionListener _assignSubtypeComponentTableListSelectionListener;

  private SubtypeScheme _subtypeScheme = null;
  private SubtypeSchemeObservable _subtypeSchemeObservable = SubtypeSchemeObservable.getInstance();
  private DatabaseManager _databaseManager = DatabaseManager.getInstance();

  /**
   *
   * @param frame Frame
   * @param modal boolean
   *
   * @author Tan Hock Zoon
   */
  public ImportWizardAssignSubtypeToIndividualComponent(Frame frame, boolean modal)
  {
    super (frame, modal);
    init();
    _subtypeSchemeObservable.addObserver(this);
  }

  /**
   * @author Tan Hock Zoon
   */
  private void init()
  {
    _mainPanel = new JPanel();
    _buttonPanel = new JPanel();
    _componentViewPanel = new JPanel();
    _componentDetailsPanel = new JPanel();

    _componentViewTitleBorder = BorderFactory.createTitledBorder(StringLocalizer.keyToString("PLWK_IMPORT_COMPONENT_VIEW_KEY"));
    _componentDetailsTitleBorder = BorderFactory.createTitledBorder(StringLocalizer.keyToString("PLWK_IMPORT_COMPONENT_DETAILS_KEY"));

    _okButton = new JButton(StringLocalizer.keyToString("PLWK_OK_BUTTON_KEY"));
    _cancelButton = new JButton(StringLocalizer.keyToString("PLWK_CANCEL_BUTTON_KEY"));

    _assignSubtypeComponentTable = new ImportAssignSubtypeComponentLayoutTable();
    _assignSubtypeComponentTableModel = new ImportAssignSubtypeComponentLayoutTableModel();
    _assignSubtypeComponentDetailTable = new ImportAssignSubtypeComponentDetailLayoutTable();
    _assignSubtypeComponentDetailTableModel = new ImportAssignSubtypeComponentDetailLayoutTableModel();

    _componentScrollPane = new JScrollPane();
    _componentDetailsScrollPane = new JScrollPane();
  }

  /**
   * @author Tan Hock Zoon
   */
  private void createPanel()
  {
    setTitle(StringLocalizer.keyToString("PLWK_IMPORT_ASSIGN_SUBTYPES_TO_INDIVIDUAL_KEY"));
    setLayout(new BorderLayout(5, 5));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

    _mainPanel.setLayout(new BoxLayout(_mainPanel, BoxLayout.Y_AXIS));

    _componentScrollPane.getViewport().add(_assignSubtypeComponentTable, null);
    _componentScrollPane.getViewport().setOpaque(false);

    _componentDetailsScrollPane.getViewport().add(_assignSubtypeComponentDetailTable, null);
    _componentDetailsScrollPane.getViewport().setOpaque(false);

    _componentViewPanel.setLayout(new BorderLayout(5, 5));
    _componentViewPanel.setBorder(_componentViewTitleBorder);
    _componentViewPanel.add(_componentScrollPane);
    _componentViewPanel.setPreferredSize(new Dimension(750, 200));

    _componentDetailsPanel.setLayout(new BorderLayout(5, 5));
    _componentDetailsPanel.setBorder(_componentDetailsTitleBorder);
    _componentDetailsPanel.add(_componentDetailsScrollPane);
    _componentDetailsPanel.setPreferredSize(new Dimension(750, 200));

    _mainPanel.add(_componentViewPanel);
    _mainPanel.add(_componentDetailsPanel);

    _buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    _buttonPanel.add(_okButton);
    _buttonPanel.add(_cancelButton);

    add(_mainPanel, BorderLayout.CENTER);
    add(_buttonPanel, BorderLayout.SOUTH);

    _assignSubtypeComponentTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        TableCellEditor tableCellEditor = _assignSubtypeComponentTable.getCellEditor();

        if (tableCellEditor != null)
          tableCellEditor.cancelCellEditing();

        assignSubtypeComponentTableListSelectionChanged(e);
      }
    };

    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        dispose();
      }
    });

    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        // roll back the changes        
        _subtypeScheme.getSelectedLibraryPackage().assignComponentType(_subtypeScheme.getComponentTypes());
        dispose();
      }
    });

    JButton[] buttons = new JButton[]{_okButton, _cancelButton};
    SwingUtils.makeAllSameLength(buttons);
  }

  /**
   * @param subtypeScheme SubtypeScheme
   *
   * @author Wei Chin, Chong
   */
  public void populatePanelWithData(SubtypeScheme subtypeScheme)
  {
    Assert.expect(subtypeScheme != null);

    _subtypeScheme = subtypeScheme;

    createPanel();

    populateLibraryPackagPinForEachLibrarySubtypeScheme(subtypeScheme);

    _assignSubtypeComponentTableModel.populateWithPanelData(subtypeScheme);
    _assignSubtypeComponentTable.setModel(_assignSubtypeComponentTableModel);
    _assignSubtypeComponentTable.setPreferredColumnWidths();
    _assignSubtypeComponentTable.setEditorsAndRenderers();

    ListSelectionModel rowSM = _assignSubtypeComponentTable.getSelectionModel();
    rowSM.addListSelectionListener(_assignSubtypeComponentTableListSelectionListener); // add the listener for a details table row selection

    validate();
    repaint();
  }

  /**
   * @param e ListSelectionEvent
   *
   * @author Wei Chin, Chong
   */
  public void assignSubtypeComponentTableListSelectionChanged(ListSelectionEvent e)
  {
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();

    if (lsm.isSelectionEmpty() || lsm.getValueIsAdjusting())
    {
      // do nothing
    }
    else
    {
      populateAssignSubtypeComponentDetailTable();
    }
  }

  /**
   * @author Wei Chin, CHong
   */
  public void populateAssignSubtypeComponentDetailTable()
  {
    _assignSubtypeComponentTable.processSelections(false);

    int[] selectedComponentRows = _assignSubtypeComponentTable.getSelectedRows();
    java.util.List<ComponentType> selectedComponents = new ArrayList<ComponentType>();

    for (int i = 0; i < selectedComponentRows.length; i++)
    {
      selectedComponents.add(_assignSubtypeComponentTableModel.getComponentTypeAt(selectedComponentRows[i]));
    }

    _assignSubtypeComponentTableModel.setSelectedComponentTypes(selectedComponents);
    if (_assignSubtypeComponentTableModel.isCommonSelectedComponentType())
    {
      _assignSubtypeComponentDetailTableModel.populateWithComponentData(selectedComponents);
      _assignSubtypeComponentDetailTable.setModel(_assignSubtypeComponentDetailTableModel);
      _assignSubtypeComponentDetailTable.setPreferredColumnWidths();
    }

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        _assignSubtypeComponentTable.processSelections(true);
      }
    });
  }

  /**
   * @param subtypeScheme SubtypeScheme
   * @author Wei Chin, Chong
   */
  public void populateLibraryPackagPinForEachLibrarySubtypeScheme(SubtypeScheme subtypeScheme)
  {
    Assert.expect(subtypeScheme != null);

    for (LibrarySubtypeScheme librarySubtypeScheme : subtypeScheme.getSelectedLibraryPackage().getLibrarySubtypeSchemesBySelectedJointType())
    {
      try
      {
        Set<LibraryPackagePin> libaryPackagePins =
            _databaseManager.getLibraryPackagePinsByLibrarySubtypeSchemeId(subtypeScheme.getSelectedLibraryPath(), librarySubtypeScheme.getLibrarySubtypeSchemeId());

        librarySubtypeScheme.setLibraryPackagePins(libaryPackagePins);
      }
      catch (DatastoreException dex)
      {
        dex.printStackTrace();
      }
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  public void dispose()
  {
    _databaseManager = null;
    _subtypeSchemeObservable.deleteObserver(this);
    super.dispose();
  }

  /**
   * @param observable Observable
   * @param object Object
   * @author Wei Chin, Chong
   */
  public synchronized void update(final Observable observable,final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof SubtypeSchemeObservable)
        {
          if (object instanceof ProjectChangeEvent)
          {
            ProjectChangeEvent event = (ProjectChangeEvent)object;
            ProjectChangeEventEnum eventEnum = event.getProjectChangeEventEnum();
            if (eventEnum instanceof SubtypeSchemeEventEnum)
              if (eventEnum.equals(SubtypeSchemeEventEnum.ASSIGNED_LIBRARY_SUBTYPE_SCHEME))
              {
                populateAssignSubtypeComponentDetailTable();
              }
          }
        }
      }
    });
  }
}
