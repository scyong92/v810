package com.axi.v810.gui;

/**
 * <p>Title: JointResultsEntryComparatorEnum</p>
 * <p>Description: Enumeration of fields to be sorted by the JointResultsEntryComparator.</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Agilent Technogies</p>
 * @author George Booth
 * @author Rex Shang
 */
public class JointResultsEntryComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static JointResultsEntryComparatorEnum DEFAULT = new JointResultsEntryComparatorEnum(++_index);
  public static JointResultsEntryComparatorEnum RUN_ID = new JointResultsEntryComparatorEnum(++_index);
  public static JointResultsEntryComparatorEnum BOARD_NAME = new JointResultsEntryComparatorEnum(++_index);
  public static JointResultsEntryComparatorEnum COMPONENT_NAME = new JointResultsEntryComparatorEnum(++_index);
  public static JointResultsEntryComparatorEnum PIN_NAME = new JointResultsEntryComparatorEnum(++_index);
  public static JointResultsEntryComparatorEnum COMP_PIN_NAME = new JointResultsEntryComparatorEnum(++_index);
  public static JointResultsEntryComparatorEnum JOINT_TYPE = new JointResultsEntryComparatorEnum(++_index);
  public static JointResultsEntryComparatorEnum SUBTYPE = new JointResultsEntryComparatorEnum(++_index);
  public static JointResultsEntryComparatorEnum PASS_FAIL = new JointResultsEntryComparatorEnum(++_index);
  public static JointResultsEntryComparatorEnum SIDE = new JointResultsEntryComparatorEnum(++_index);
  public static JointResultsEntryComparatorEnum NUM_DEFECTS = new JointResultsEntryComparatorEnum(++_index);
  public static JointResultsEntryComparatorEnum MEASUREMENT_VALUE = new JointResultsEntryComparatorEnum(++_index);

  /**
   * @author George Booth
   */
  private JointResultsEntryComparatorEnum(int id)
  {
    super(id);
  }
}
