package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * A dialog to display the three alignment verification images
 * @author Andy Mechtenberg
 */
public class AlignmentImageDialog extends EscapeDialog
{
  private Project _project;
  /**
   * @author Andy Mechtenberg
   */
  public AlignmentImageDialog(JFrame parent, String title, Project project)
  {
    super(parent, title, true);
    Assert.expect(parent != null);
    Assert.expect(title != null);
    Assert.expect(project != null);
    _project = project;
    jbInit();
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    pack();
  }

  /**
   * Override so we can clear out the _project reference
   * @author Andy Mechtenberg
   */
  public void setVisible(boolean visible)
  {
    super.setVisible(visible);
    _project = null;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    JButton okButton = new JButton(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));

    okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });

    JPanel mainPanel = new JPanel(new BorderLayout());
    JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    buttonPanel.add(okButton);
    mainPanel.add(buttonPanel, BorderLayout.SOUTH);
    JTabbedPane imageTabPane = new JTabbedPane();

    boolean isLongPanel = _project.getTestProgram().isLongProgram();
    java.util.List<AlignmentGroup> alignmentGroups = new java.util.ArrayList<AlignmentGroup>();

    // wei chin change for individual alignment
    if(_project.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      if (isLongPanel)
      {
        alignmentGroups.addAll(_project.getPanel().getPanelSettings().getRightAlignmentGroupsForLongPanel());
        // XCR-3885 System Crash When Click on View Alignment Button for Multi-Long Board
        alignmentGroups.addAll(_project.getPanel().getPanelSettings().getLeftAlignmentGroupsForLongPanel());
      }
      else
        alignmentGroups = _project.getPanel().getPanelSettings().getAlignmentGroupsForShortPanel();
    }
    else
    {
      for(Board board: _project.getPanel().getBoards())
      {
        if (board.getBoardSettings().isLongBoard())
        {
          alignmentGroups.addAll(board.getBoardSettings().getRightAlignmentGroupsForLongPanel());
          alignmentGroups.addAll(board.getBoardSettings().getLeftAlignmentGroupsForLongPanel());
        }
        else
          alignmentGroups.addAll(board.getBoardSettings().getAlignmentGroupsForShortPanel());
      }
    }

    for (AlignmentGroup alignmentGroup : alignmentGroups)
    {
      ImageDisplayPanel aligmentImagePanel = null;
      JPanel detailPanel = new JPanel(new BorderLayout());
      // Ying-Huan.Chu [XCR-2554] - Alignment-failed images are not displayed.
      boolean isSecondPortionOfLongPanel = false; // default value for short panel = false.
      if (_project.getTestProgram().hasAlignmentRegionInInspectableTestSubProgram())
        isSecondPortionOfLongPanel = _project.getTestProgram().getAlignmentRegion(alignmentGroup).getTestSubProgram().getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT);
      String alignmentImageFullPath = FileName.getAlignmentVerificationImageFullPath(_project.getName(), alignmentGroup.getName(), isSecondPortionOfLongPanel);

      boolean imageExists = FileUtilAxi.exists(alignmentImageFullPath);
      if (imageExists)
      {
        try
        {
          aligmentImagePanel = new ImageDisplayPanel(XrayImageIoUtil.loadPngBufferedImage(alignmentImageFullPath));
        }
        catch (DatastoreException ex)
        {
          imageExists = false;
        }
      }

      double matchQuality = 0.0f;
      if (alignmentGroup.hasMatchQuality())
      {
        matchQuality = MathUtil.roundToPlaces(alignmentGroup.getMatchQuality() * 100, 2);
      }
      LocalizedString matchQualityStringKey = new LocalizedString("MMGUI_ALIGNMENT_MATCH_QUALITY_KEY", new Object[]{new Double(matchQuality)});
      detailPanel.add(new JLabel(StringLocalizer.keyToString(matchQualityStringKey)),BorderLayout.NORTH);
      
      LocalizedString groupName = new LocalizedString("MMGUI_ALIGN_GROUP_NAME_KEY", new Object[]{alignmentGroup.getName()});
      if (isSecondPortionOfLongPanel)
        groupName = new LocalizedString("MMGUI_ALIGN_SECOND_REGION_GROUP_NAME_KEY", new Object[]{alignmentGroup.getName()});
      
      if (imageExists)
      {
        detailPanel.add(aligmentImagePanel,BorderLayout.CENTER);
      }
      else
      {
        JPanel noImagePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JLabel noImageLabel = new JLabel(StringLocalizer.keyToString("MMGUI_ALIGNMENT_NO_IMAGE_KEY"));
        noImagePanel.add(noImageLabel, null);
        detailPanel.add(noImagePanel,BorderLayout.CENTER);
      }
      imageTabPane.add(detailPanel, StringLocalizer.keyToString(groupName));
    }

    mainPanel.add(imageTabPane, BorderLayout.CENTER);

    if(_project.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      if (isLongPanel)
        mainPanel.setPreferredSize(new Dimension(600, 400));
      else
        mainPanel.setPreferredSize(new Dimension(500, 400));
    }
    else
    {
      mainPanel.setPreferredSize(new Dimension(650, 400));
    }

    this.getContentPane().add(mainPanel, BorderLayout.CENTER);
    this.getRootPane().setDefaultButton(okButton);
  }
}
