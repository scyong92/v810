package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This class draws a PackagePin outline.
 *
 * @author Bill Darbie
 */
public class PackagePinPitchRenderer extends ShapeRenderer
{
  private PackagePin _packagePin;

  /**
   * @author Andy Mechtenberg
   */
  PackagePinPitchRenderer(PackagePin packagePin)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(packagePin != null);
    _packagePin = packagePin;

    refreshData();
  }

  /**
   * @author Bill Darbie
   */
  public PackagePin getPackagePin()
  {
    return _packagePin;
  }

  /**
   * @author Bill Darbie
   */
  protected void refreshData()
  {
    Assert.expect(_packagePin != null);

    Shape pitchRectangle = _packagePin.getShapeInNanoMeters();

    int pitchInNanoMeters = _packagePin.getPitchInNanoMeters();

    Rectangle2D rect = pitchRectangle.getBounds2D();
    double centerX = rect.getCenterX();
    double centerY = rect.getCenterY();
    double lowerLeftX = centerX - pitchInNanoMeters/2.0;
    double lowerLeftY = centerY - pitchInNanoMeters/2.0;

    rect.setRect(new Rectangle2D.Double(lowerLeftX, lowerLeftY, pitchInNanoMeters, pitchInNanoMeters));

    initializeShape(rect);
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    Color origColor = graphics2D.getColor();

    graphics2D.setColor(LayerColorEnum.PITCH_COLOR.getColor());

    super.paintComponent(graphics);

    graphics2D.setColor(origColor);
  }

  /**
   * Return the name of the panel
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    return _packagePin.getName();
  }
}
