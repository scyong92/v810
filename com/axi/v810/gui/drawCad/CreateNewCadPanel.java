package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import com.axi.v810.util.*;
import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.cadCreator.LandPatternAreaRenderer;
import com.axi.v810.gui.imagePane.LandPatternEditorGraphicsEngineSetup;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.images.*;
import com.axi.v810.hardware.*;
import java.awt.image.BufferedImage;

/**
 * This is a standalone panel that will fit into the MainMenu frame.
 * @author Kee Chin Seong
 */
public class CreateNewCadPanel extends JPanel
{
  private int _defaultWidth = 800;
  private int _defaultHeight = 800;
  
  private BoardGraphicsEngineSetup _boardGraphicsEngineSetup;
  private CompPackageGraphicsEngineSetup _compPackageGraphicsEngineSetup;
  private CreateCadLandPatternGraphicsEngineSetup _landPatternGraphicsEngineSetup;
  private LibraryPackageGraphicsEngineSetup _libraryPackageGraphicsEngineSetup;
  
  private LandPatternEditorGraphicsEngineSetup _landPatternEditorrGraphicsEngineSetup;
  private CreatePanelGraphicsEngineSetup _createPanelGraphicsEngineSetup;
  
  private GraphicsEngine _graphicsEngine;
  private CadCreatorControlToolBar _controlToolBar;
  private JPanel _statusPanel = new JPanel();
  private JLabel _selectedDescriptionLabel = new JLabel();
  private FlowLayout _statusPanelFlowLayout = new FlowLayout();
  private JPanel _exitInfoPanel = new JPanel();
  private JLabel _exitInfoLabel = new JLabel();
  private FlowLayout _exitInfoPanelFlowLayout = new FlowLayout();
  private JPanel _eastArrowPanel = new JPanel();
  private JPanel _westArrowPanel = new JPanel();
  private boolean _isCadDrawed = false;
  
  private com.axi.guiUtil.Renderer _landPatternRenderer = null;
  //private PanelLayoutTable _panelLayoutTable = new PanelLayoutTable();
  //private PanelLayoutTableModel _panelLayoutTableModel;
  private Project _project;
  /**
   * 
   * @author Kee Chin Seong
   */
  private Project loadProject()
  {
    Project project = null;
    try
    {
      java.util.List<LocalizedString> warnings = new ArrayList<LocalizedString>();

      String projectName = "";

      projectName = "digby_orig";

      long startTime = System.currentTimeMillis();
      project = Project.importProjectFromNdfs(projectName, warnings);
      long stopTime = System.currentTimeMillis();
    }
    catch(Exception e)
    {
      Assert.logException(e);
    }

    Assert.expect(project != null);
    return project;
  }

  /**
   * @author Kee Chin Seong
   * for Land Pattern
   */
  public CreateNewCadPanel()
  {
    _graphicsEngine = new GraphicsEngine(0);
    if (_controlToolBar == null)
      _controlToolBar = new CadCreatorControlToolBar(this, _graphicsEngine);
    _controlToolBar.disableAllControls();

    jbInit(false);
  }

  /**
   * @author Kee Chin Seong
   * for Land Pattern Dialog
   */
  public CreateNewCadPanel(com.axi.guiUtil.Renderer landPatternRenderer)
  {
    //_cadCreatorGraphicsEngineSetup = new LandPatternEditorGraphicsEngineSetup(); 
    //_graphicsEngine = _landPatternEditorrGraphicsEngineSetup.getGraphicsEngine();
    _graphicsEngine = new GraphicsEngine(0);
    
    if (_controlToolBar == null)
      _controlToolBar = new CadCreatorControlToolBar(this, _graphicsEngine);
    _controlToolBar.disableAllControls();
   
    if(_landPatternEditorrGraphicsEngineSetup == null)
    {
      _landPatternEditorrGraphicsEngineSetup = new LandPatternEditorGraphicsEngineSetup(this, _graphicsEngine);
    }
    else
      _graphicsEngine.reset();
    
    if(landPatternRenderer != null)
    {
        _landPatternRenderer = landPatternRenderer;
        
        Rectangle rect = new Rectangle((int)_landPatternRenderer.getLocationOnScreen().getX(), (int)_landPatternRenderer.getLocationOnScreen().getY(),(int)_landPatternRenderer.getWidth(),(int)_landPatternRenderer.getHeight());
        BufferedImage img = null;
        try
        {
           Robot robot = new Robot();
           Thread.sleep(1000);
           //java.io.File f = new java.io.File("C:\\abc.jpg");
           img = robot.createScreenCapture(rect);
        } 
        catch(Exception e)
        {
           e.printStackTrace();
        }
        displayCroppedArea(img);
        
        _landPatternEditorrGraphicsEngineSetup.addObservers();
        //_graphicsEngine.setAxisInterpretation(true, false);
        //_graphicsEngine.center();
    }

    _controlToolBar.resetForCroppedLandPattern(_graphicsEngine);
    
    add(_graphicsEngine, BorderLayout.CENTER);
    setPreferredSize(new Dimension(200, 200));
    setLayout(new BorderLayout());

    jbInit(false);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public LandPatternAreaRenderer getLandPatternRenderer()
  {
     return _landPatternEditorrGraphicsEngineSetup.getLandPatternRenderer();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public Shape getCurrentDragShape()
  {
    Assert.expect(_landPatternEditorrGraphicsEngineSetup != null);
    
    return _landPatternEditorrGraphicsEngineSetup.getRendererShape();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public LandPattern getCurrentCreatedLandPattern()
  {
    Assert.expect(_landPatternEditorrGraphicsEngineSetup != null);
    
    return _landPatternEditorrGraphicsEngineSetup.getCurrentLandPattern();
  }
  
  /**
   * @author Kee Chin Seong
   */
  public CreateNewCadPanel(int width, int height)
  {
    Assert.expect(width > 0, "Width was too small: " + width);
    Assert.expect(height > 0, "Height was too small: " + height);
    _defaultWidth = width;
    _defaultHeight = height;

    _graphicsEngine = new GraphicsEngine(0);
    if (_controlToolBar == null)
      _controlToolBar = new CadCreatorControlToolBar(this, _graphicsEngine);
    _controlToolBar.disableAllControls();

    jbInit(false);
  }

  /*
   * @author Kee Chin Seong
   */
  public CreateNewCadPanel(Project project, int width, int height, boolean flipped)
  {
    Assert.expect(project != null, "Project is NULL");
    Assert.expect(width > 0, "Width was too small: " + width);
    Assert.expect(height > 0, "Height was too small: " + height);
    _defaultWidth = width;
    _defaultHeight = height;

    _graphicsEngine = new GraphicsEngine(0);
    
    _createPanelGraphicsEngineSetup = new CreatePanelGraphicsEngineSetup(this, _graphicsEngine);
    
    setPreferredSize(new Dimension(_defaultWidth, _defaultHeight));
    setLayout(new BorderLayout());
    add(_graphicsEngine, BorderLayout.CENTER);

    _createPanelGraphicsEngineSetup.displayPanelForStaticImageGeneration(project.getPanel(), flipped);
  }
  
  /**
   * Sets up this DrawCadPanel to display the normal Panel graphics
   * @author Kee Chin Seong
   */
  public void drawPanel(Project project)
  {
    Assert.expect(project != null);

    // we only need to do this if the panel grapics engine setup class
    // has not been initialized otherwise, it will be listening to datastore
    // events and will repopulate itself as appropriate.
    if(_createPanelGraphicsEngineSetup == null)
    {
      _createPanelGraphicsEngineSetup = new CreatePanelGraphicsEngineSetup(this, _graphicsEngine);
    }
    else
      _graphicsEngine.reset();

    _createPanelGraphicsEngineSetup.displayPanel(project.getPanel());
    _createPanelGraphicsEngineSetup.addObservers();

    Assert.expect(_controlToolBar != null);
    _controlToolBar.resetForNewPanel(_createPanelGraphicsEngineSetup, project.getPanel());
    VirtualLiveImageGenerationSettings.getInstance().setPanelShape(project.getPanel().getShapeInNanoMeters());

    jbInit(true);
    invalidate();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void drawCadPanel(Project project)
  {
    Assert.expect(project != null);

    _project = project;
    // we only need to do this if the panel grapics engine setup class
    // has not been initialized otherwise, it will be listening to datastore
    // events and will repopulate itself as appropriate.
    if(_createPanelGraphicsEngineSetup == null)
    {
      _createPanelGraphicsEngineSetup = new CreatePanelGraphicsEngineSetup(this, _graphicsEngine);
    }
    else
      _graphicsEngine.reset();

    _createPanelGraphicsEngineSetup.displayPanel(project.getPanel());
    _createPanelGraphicsEngineSetup.addObservers();

    Assert.expect(_controlToolBar != null);
    _controlToolBar.resetForNewPanel(_createPanelGraphicsEngineSetup, project.getPanel());
    VirtualLiveImageGenerationSettings.getInstance().setPanelShape(project.getPanel().getShapeInNanoMeters());

    jbInit(true);
    invalidate();
  }

  /**
   * Sets up this DrawCadPanel to display the normal Panel graphics
   * @author Kee Chin Seong
   */
  public void drawBoard(Board board)
  {
    Assert.expect(board != null);

    // we only need to do this if the panel grapics engine setup class
    // has not been initialized otherwise, it will be listening to datastore
    // events and will repopulate itself as appropriate.
    if(_boardGraphicsEngineSetup == null)
    {
      _boardGraphicsEngineSetup = new BoardGraphicsEngineSetup(this, _graphicsEngine);
    }
    else
      _graphicsEngine.reset();

    _boardGraphicsEngineSetup.displayBoard(board);
    _boardGraphicsEngineSetup.addObservers();

    Assert.expect(_controlToolBar != null);
    _controlToolBar.resetForNewBoard(_boardGraphicsEngineSetup, board);

    jbInit(true);
    invalidate();
  }

  /**
   * Sets up this DrawCadPanel to display the normal Panel graphics
   * @author Kee Chin Seong
   */
  public void drawPackage(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);

    if (_compPackageGraphicsEngineSetup == null)
      _compPackageGraphicsEngineSetup = new CompPackageGraphicsEngineSetup(this, _graphicsEngine);

    _compPackageGraphicsEngineSetup.displayPackage(compPackage);

    Assert.expect(_controlToolBar != null);
    _controlToolBar.resetForNewPackageOrLandPattern(_graphicsEngine);
    _controlToolBar.enableAllControls();

    jbInit(false);
  }

  /**
   * Sets up this DrawCadPanel to display the normal Panel graphics
   * @author Kee Chin Seong
   */
  public void drawLandPattern(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);

    if (_landPatternGraphicsEngineSetup == null)
      _landPatternGraphicsEngineSetup = new CreateCadLandPatternGraphicsEngineSetup(this, _graphicsEngine);

    _landPatternGraphicsEngineSetup.displayLandPattern(landPattern);

    Assert.expect(_controlToolBar != null);
    _controlToolBar.resetForNewPackageOrLandPattern(_graphicsEngine);
    
    java.util.List<String> excludeList = new java.util.LinkedList<String>();
    excludeList.add(_controlToolBar.getDisplayPadNameToggleButtonName());
    _controlToolBar.enableAllControlsWithExcludeList(excludeList);

    jbInit(false);
  }

  /**
   * Sets up this DrawCadPanel to display the normal Panel graphics
   * @author Kee Chin Seong
   */
  public void drawLibraryPackage(LibraryPackage libraryPackage)
  {
    Assert.expect(libraryPackage != null);

    if (_libraryPackageGraphicsEngineSetup == null)
      _libraryPackageGraphicsEngineSetup = new LibraryPackageGraphicsEngineSetup(this, _graphicsEngine);

    _libraryPackageGraphicsEngineSetup.displayLibraryPackage(libraryPackage);

    Assert.expect(_controlToolBar != null);
    _controlToolBar.resetForNewPackageOrLandPattern(_graphicsEngine);
    _controlToolBar.enableAllControls();

    jbInit(false);
  }

  /**
   * @author Kee Chin Seong
   */
  public void clearGraphics()
  {
    _graphicsEngine = new GraphicsEngine(0);
    Assert.expect(_controlToolBar != null);
    _controlToolBar.disableAllControls();

    jbInit(false);
  }

  /**
   * @author Kee Chin Seong
   */
  private void resetAll()
  {
    _isCadDrawed = false;
    _graphicsEngine.reset();
    _controlToolBar.disableAllControls();
    _controlToolBar.unpopulate();
    jbInit(false);
  }

  /**
   * @author Kee Chin Seong
   */
  public void resetPanelGraphics()
  {
    resetAll();
    if (_createPanelGraphicsEngineSetup != null)
    {
      _createPanelGraphicsEngineSetup.removeObservers();
      _createPanelGraphicsEngineSetup.unpopulate();
    }
  }

  /**
   * @author Kee Chin Seong
   */
  public void resetBoardGraphics()
  {
    resetAll();
    if (_boardGraphicsEngineSetup != null)
    {
      _boardGraphicsEngineSetup.removeObservers();
      _boardGraphicsEngineSetup.unpopulate();
    }
  }

  /**
   * @author Kee Chin Seong
   */
  public void resetPackageGraphics()
  {
    resetAll();
    if (_compPackageGraphicsEngineSetup != null)
      _compPackageGraphicsEngineSetup.reset();
  }

  /**
   * @author Kee Chin Seong
   */
  public void resetLandPatternGraphics()
  {
    resetAll();
    if(_landPatternGraphicsEngineSetup != null)
      _landPatternGraphicsEngineSetup.reset();
  }

  /**
   * @author Kee Chin Seong
   */
  public void resetLibraryPackageGraphics()
  {
    resetAll();
    if(_libraryPackageGraphicsEngineSetup != null)
      _libraryPackageGraphicsEngineSetup.reset();
  }

  /*
   * @author Kee Chin Seong
   */
  public CreatePanelGraphicsEngineSetup getCreatePanelGraphicsEngineSetup()
  {
    Assert.expect(_createPanelGraphicsEngineSetup != null);

    return _createPanelGraphicsEngineSetup;
  }
  
  /*
   * @author Kee Chin Seong - To activate the graphic engine drag mode
   */
  public void setGroupSelectMode(boolean groupSelectMode)
  {
    Assert.expect(_controlToolBar != null);
    
    _controlToolBar.setGroupSelectMode(groupSelectMode);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean hasBoardGraphicsEngineSetup()
  {
    if (_boardGraphicsEngineSetup != null)
      return true;
    return false;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public LandPatternEditorGraphicsEngineSetup getLandPatternEditorGraphicsEngineSetup()
  {
    Assert.expect(_landPatternEditorrGraphicsEngineSetup != null);
    
    return _landPatternEditorrGraphicsEngineSetup;
  }

  /**
   * @author Kee Chin Seong
   */
  public BoardGraphicsEngineSetup getBoardGraphicsEngineSetup()
  {
    Assert.expect(_boardGraphicsEngineSetup != null);

    return _boardGraphicsEngineSetup;
  }

  /**
   * @author Kee Chin Seong
   */
  public CompPackageGraphicsEngineSetup getCompPackageGraphicsEngineSetup()
  {
    Assert.expect(_compPackageGraphicsEngineSetup != null);

    return _compPackageGraphicsEngineSetup;
  }

  /**
   * @author Kee Chin Seong
   */
  public CreateCadLandPatternGraphicsEngineSetup getLandPatternGraphicsEngineSetup()
  {
    Assert.expect(_landPatternGraphicsEngineSetup != null);

    return _landPatternGraphicsEngineSetup;
  }

  /**
   * @author Kee Chin Seong
   */
  public LibraryPackageGraphicsEngineSetup getLibraryPackageGraphicsEngineSetup()
  {
    Assert.expect(_libraryPackageGraphicsEngineSetup != null);

    return _libraryPackageGraphicsEngineSetup;
  }

  /**
   * @author Kee Chin Seong
   */
  public void initBoardGraphicsEngineSetup()
  {
     if (_boardGraphicsEngineSetup == null)
      _boardGraphicsEngineSetup = new BoardGraphicsEngineSetup(this, _graphicsEngine);
  }

  /**
   * @author Kee Chin Seong
   */
  public void initLandPatternGraphicsEngineSetup()
  {
    if (_landPatternGraphicsEngineSetup == null)
      _landPatternGraphicsEngineSetup = new CreateCadLandPatternGraphicsEngineSetup(this, _graphicsEngine);
  }

  /**
   * @author Kee Chin Seong
   */
  public void initLibraryPackageGraphicsEngineSetup()
  {
    if (_libraryPackageGraphicsEngineSetup == null)
      _libraryPackageGraphicsEngineSetup = new LibraryPackageGraphicsEngineSetup(this, _graphicsEngine);
  }

  /**
   * @author Kee Chin Seong
   */
  public void initPackageGraphicsEngineSetup()
  {
    if (_compPackageGraphicsEngineSetup == null)
      _compPackageGraphicsEngineSetup = new CompPackageGraphicsEngineSetup(this, _graphicsEngine);
  }

  /**
   * Create a line-drawing panel.jpg using gui/drawCad technology
   * @author Kee Chin Seong
   */
  public static void createPanelImage(final Project project)
  {
    Assert.expect(project != null);

    createPanelImage(project, true);
  }
  
  /**
   * Create a line-drawing panel.jpg using gui/drawCad technology 
   * @author Kee Chin Seong
   * use this for serial number mapping dialog
   */
  public static void createPanelImage(final Project project, final boolean saveProject)
  {
    Assert.expect(project != null);

    final String panelImageName;
    final String panelFlippedImageName;
    
    if(saveProject)
    {
      //create panel image when save project - Siew Yeng
      panelImageName = FileName.getPanelImageFullPath(project.getName());
      panelFlippedImageName = FileName.getPanelFlippedImageFullPath(project.getName());
    }
    else
    {
      //create panel image for serial number mapping - Siew Yeng
      panelImageName = FileName.getTempPanelImageFullPath(project.getName());
      panelFlippedImageName = FileName.getTempPanelFlippedImageFullPath(project.getName());
    }
    
    if (FileUtilAxi.exists(panelImageName))
      return;

    try
    {
      SwingUtils.invokeAndWait(new Runnable()
      {
        public void run()
        {
          long startTime = System.currentTimeMillis();

          CadFrame frame = new CadFrame();
          CreateNewCadPanel cadPanel = new CreateNewCadPanel(project, 200, 200, false); // 200, 200 is small, but the jpg will scale UP better than DOWN
          frame.getContentPane().add(cadPanel);
          frame.pack();
          try
          {
            cadPanel.writeToFile(panelImageName);
            ProjectObservable.getInstance().stateChanged(project, null, null, ProjectEventEnum.PANEL_IMAGE_CREATION);
//            frame.setVisible(true);
          }
          catch (FileNotFoundException fnfe)
          {
            // do nothing.  If we fail to create, it's not a showstopper
          }
          catch (IOException ioe)
          {
            // do nothing.  If we fail to create, it's not a showstopper
          }
          finally
          {
            cadPanel.dispose();
            frame.dispose();
            cadPanel = null;
            frame = null;
          }
          frame = new CadFrame();
          cadPanel = new CreateNewCadPanel(project, 200, 200, true); // 200, 200 is small, but the jpg will scale UP better than DOWN
          frame.getContentPane().add(cadPanel);
          frame.pack();
//          frame.setVisible(true);
          try
          {
            cadPanel.writeToFile(panelFlippedImageName);
            ProjectObservable.getInstance().stateChanged(project, null, null, ProjectEventEnum.PANEL_IMAGE_CREATION);
//            frame.setVisible(true);
          }
          catch (FileNotFoundException fnfe)
          {
            // do nothing.  If we fail to create, it's not a showstopper
          }
          catch (IOException ioe)
          {
            // do nothing.  If we fail to create, it's not a showstopper
          }
          finally
          {
            cadPanel.dispose();
            frame.dispose();
            cadPanel = null;
            frame = null;
          }
          
          /**
          * @To generate image set for different board 
          * @author Lim Boon Hong 
          */
          for (Board selectedBoard:  project.getPanel().getBoards())
          {
            cadPanel = new CreateNewCadPanel(project, 200, 200, false); // 200, 200 is small, but the jpg will scale UP better than DOWN

            frame = new CadFrame();
            frame.getContentPane().add(cadPanel);
            frame.pack();
  //          frame.setVisible(true);

            String boardFilePath;
            if(saveProject)
            {
              boardFilePath = FileName.getPanelBoardImageFullPath(project.getName(), selectedBoard.getName());
            }
            else
            {
              boardFilePath = FileName.getTempPanelBoardImageFullPath(project.getName(), selectedBoard.getName());
            }
            
            try
            {
              cadPanel.getCreatePanelGraphicsEngineSetup().setHighlightBoard(selectedBoard);
              cadPanel.writeToFile(boardFilePath);
              ProjectObservable.getInstance().stateChanged(project, null, null, ProjectEventEnum.PANEL_IMAGE_CREATION);
  //            frame.setVisible(true);
            }
            catch (FileNotFoundException fnfe)
            {
              // do nothing.  If we fail to create, it's not a showstopper
            }
            catch (IOException ioe)
            {
              // do nothing.  If we fail to create, it's not a showstopper
            }
            finally
            {
              cadPanel.dispose();
              frame.dispose();
              cadPanel = null;
              frame = null;
            }
          }
          
          //Generate image set for flip image. By Lim Boon Hong
          for (Board selectedBoard:  project.getPanel().getBoards())
          {
            cadPanel = new CreateNewCadPanel(project, 200, 200, true); // 200, 200 is small, but the jpg will scale UP better than DOWN

            frame = new CadFrame();
            frame.getContentPane().add(cadPanel);
            frame.pack();
  //          frame.setVisible(true);

            String boardFilePath;
            if(saveProject)
            {
              boardFilePath = FileName.getPanelBoardFlippedImageFullPath(project.getName(), selectedBoard.getName());
            }
            else
            {
              boardFilePath = FileName.getTempPanelBoardFlippedImageFullPath(project.getName(), selectedBoard.getName());
            }
         
            try
            {
              cadPanel.getCreatePanelGraphicsEngineSetup().setHighlightBoard(selectedBoard);
              cadPanel.writeToFile(boardFilePath);
              ProjectObservable.getInstance().stateChanged(project, null, null, ProjectEventEnum.PANEL_IMAGE_CREATION);
  //            frame.setVisible(true);
            }
            catch (FileNotFoundException fnfe)
            {
              // do nothing.  If we fail to create, it's not a showstopper
            }
            catch (IOException ioe)
            {
              // do nothing.  If we fail to create, it's not a showstopper
            }
            finally
            {
              cadPanel.dispose();
              frame.dispose();
              cadPanel = null;
              frame = null;
            }
          }          
          long endTime = System.currentTimeMillis();
        }
      });
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }


  /**
   * @author Kee Chin Seong
   */
  private void jbInit(boolean useLoadArrows)
  {
    JPanel centerPanel = new JPanel(new BorderLayout());
    removeAll();
    setPreferredSize(new Dimension(_defaultWidth, _defaultHeight));
    setBackground(Color.black);
    setLayout(new BorderLayout());
    _statusPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _statusPanelFlowLayout.setVgap(0);
    _statusPanel.setLayout(_statusPanelFlowLayout);
    _exitInfoPanel.setLayout(_exitInfoPanelFlowLayout);
    _exitInfoPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _exitInfoPanelFlowLayout.setHgap(0);
    _exitInfoPanelFlowLayout.setVgap(0);
    _statusPanel.setBorder(BorderFactory.createLoweredBevelBorder());
    centerPanel.add(_controlToolBar, BorderLayout.NORTH);
    centerPanel.add(_graphicsEngine, BorderLayout.CENTER);
    centerPanel.add(_statusPanel,  BorderLayout.SOUTH);
    _statusPanel.add(_selectedDescriptionLabel, null);
    _statusPanel.add(_exitInfoPanel, null);
    _exitInfoPanel.add(_exitInfoLabel, null);
    _selectedDescriptionLabel.setText(" ");
    add(centerPanel, BorderLayout.CENTER);
    if (useLoadArrows)
    {
      _eastArrowPanel.setLayout(new BorderLayout());
      _eastArrowPanel.setBorder(BorderFactory.createEmptyBorder(0,0,0,5));
      _eastArrowPanel.setBackground(Color.black);
      _westArrowPanel.setLayout(new BorderLayout());
      _westArrowPanel.setBorder(BorderFactory.createEmptyBorder(0,5,0,0));
      _westArrowPanel.setBackground(Color.black);
      setPanelLoadArrows();
      centerPanel.add(_eastArrowPanel, BorderLayout.EAST);
      centerPanel.add(_westArrowPanel, BorderLayout.WEST);
    }
  }

  /**
   * @author Kee Chin Seong
   */
  public void setPanelLoadArrows()
  {
    _eastArrowPanel.removeAll();
    _westArrowPanel.removeAll();
    JLabel eastArrowLabel;
    JLabel westArrowLabel;
    PanelHandlerPanelFlowDirectionEnum flowDirection = PanelHandler.getInstance().getPanelFlowDirection();
    if (flowDirection == PanelHandlerPanelFlowDirectionEnum.LEFT_TO_RIGHT)
    {
      // use up arrows if handler is left-to-right ("normal")
      eastArrowLabel = new JLabel(Image5DX.getImageIcon(Image5DX.UP_ARROW));
      westArrowLabel = new JLabel(Image5DX.getImageIcon(Image5DX.UP_ARROW));
    }
    else
    {
      // use down arrows if handler is right-to-left
      eastArrowLabel = new JLabel(Image5DX.getImageIcon(Image5DX.DOWN_ARROW));
      westArrowLabel = new JLabel(Image5DX.getImageIcon(Image5DX.DOWN_ARROW));
    }
    _eastArrowPanel.add(eastArrowLabel, BorderLayout.CENTER);
    _westArrowPanel.add(westArrowLabel, BorderLayout.CENTER);
    repaint();
  }

  /**
   * @author Kee Chin Seong
   * @param description - string to be displayed on the status bar
   */
  public void setDescription(String description)
  {
    _selectedDescriptionLabel.setText(description);
  }

/**
 * Converts all shapes from nano meter based to pixel based
 * This must be called on the Swing thread!
 * @author Kee Chin Seong
 */
  public void scaleDrawing()
  {
    _graphicsEngine.fitGraphicsToScreen();
    _graphicsEngine.mark();
    if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment() == false)
      _graphicsEngine.setGroupSelectMode(true);
    _controlToolBar.selectText();
    _isCadDrawed = true;   
  }

  /**
   * @author Kee Chin Seong
   */
  private void writeToFile(String fileName) throws FileNotFoundException, IOException
  {
    Assert.expect(fileName != null);
    _graphicsEngine.fitGraphicsToScreen();
    _graphicsEngine.setWidthAndHeight();
    _graphicsEngine.writeToFile(fileName, 5);
  }

  /**
   * Removes all the renderers to free up the memory
   * This must be called on the Swing thread!
   * @author Kee Chin Seong
   */
  public void dispose()
  {
    _graphicsEngine.dispose();
  }
  
  /**
   * @author sham
   */
  public boolean isCadDrawed()
  {
    return _isCadDrawed;
  }

  /**
   * @author Kee Chin Seong
   */
  public void setEnableSelectRegionToggleButton(boolean enable)
  {
    Assert.expect(_controlToolBar != null);	

    _controlToolBar.setEnableSelectRegionToggleButon(enable);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void enabledOvalDragShape()
  {
    _graphicsEngine.enabledOvalDragShape();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void enabledRectangleDragShape()
  {
    _graphicsEngine.enabledRectangleDragShape();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void switchToCadCreatorModeDisplay()
  {
    Assert.expect(_statusPanel != null);

    if(isCadDrawed())
    {
      Assert.expect(_createPanelGraphicsEngineSetup != null);
      Assert.expect(_graphicsEngine != null);
      
      _createPanelGraphicsEngineSetup.drawVirtualLiveAreaDisplay();
      _graphicsEngine.fitGraphicsToScreen();
    }
    _statusPanel.setVisible(false);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void switchToTestDevModeDisplay()
  {
    Assert.expect(_statusPanel != null);

    if(isCadDrawed())
    {
      Assert.expect(_createPanelGraphicsEngineSetup != null);
      _createPanelGraphicsEngineSetup.clearVirtualLiveAreaDisplay();
    }
    _statusPanel.setVisible(true);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void removePanelGraphicObserver()
  {
    if (_createPanelGraphicsEngineSetup != null)
      _createPanelGraphicsEngineSetup.removeObservers();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void addPanelGraphicObserver()
  {
    if (_createPanelGraphicsEngineSetup != null)
      _createPanelGraphicsEngineSetup.addObservers();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void removeBoardGraphicObserver()
  {
    if (_boardGraphicsEngineSetup != null)
      _boardGraphicsEngineSetup.removeObservers();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void addBoardGraphicObserver()
  {
    if (_boardGraphicsEngineSetup != null)
    {
      // XCR-2135 - Add observers only if the current displayed board is not null. - Ying-Huan.Chu
      if (_boardGraphicsEngineSetup.getCurrentDisplayBoard() != null)
      {
        _boardGraphicsEngineSetup.addObservers();
      }
    }
  }
  
  /*
   * @author Kee chin Seong
   */
  public void removeLandPatternGraphicsObserver()
  {
    if(_landPatternEditorrGraphicsEngineSetup != null)
    {
       _landPatternEditorrGraphicsEngineSetup.removeObservers();
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void addLandPatternGraphicObersver()
  {
    if(_landPatternEditorrGraphicsEngineSetup != null)
    {
        _landPatternEditorrGraphicsEngineSetup.addObservers();
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void displayCroppedArea(BufferedImage img)
  {
     _landPatternEditorrGraphicsEngineSetup.displayCroppedArea(_landPatternRenderer, img);
  }
  
  
  /*
   * @author Kee Chin Seong
   */
  public void displayLandPattern(LandPattern landPattern)
  {
    if(_landPatternEditorrGraphicsEngineSetup == null)
    {
      _landPatternEditorrGraphicsEngineSetup = new LandPatternEditorGraphicsEngineSetup(this, _graphicsEngine);
    }
    else
      _graphicsEngine.reset();
    
    _landPatternEditorrGraphicsEngineSetup.displayLandPattern(landPattern);
  }
}

