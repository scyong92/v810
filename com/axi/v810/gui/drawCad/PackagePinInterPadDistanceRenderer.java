package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This class draws a PackagePin outline.
 *
 * @author Andy Mechtenberg
 */
public class PackagePinInterPadDistanceRenderer extends ShapeRenderer
{
  private PackagePin _packagePin;

  /**
   * @author Andy Mechtenberg
   */
  PackagePinInterPadDistanceRenderer(PackagePin packagePin)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(packagePin != null);
    _packagePin = packagePin;

    refreshData();
  }

 /**
  * @author Bill Darbie
  */
 public PackagePin getPackagePin()
 {
   return _packagePin;
 }

 /**
  * @author Bill Darbie
  */
 protected void refreshData()
 {
   Assert.expect(_packagePin != null);

   Shape pitchRectangle = _packagePin.getShapeInNanoMeters();

   int ipdInNanoMeters = _packagePin.getInterPadDistanceInNanoMeters();

   Rectangle2D rect = pitchRectangle.getBounds2D();
   double centerX = rect.getCenterX();
   double centerY = rect.getCenterY();
   double lowerLeftX = centerX - ipdInNanoMeters/2.0;
   double lowerLeftY = centerY - ipdInNanoMeters/2.0;

   rect.setRect(new Rectangle2D.Double((rect.getX() - ipdInNanoMeters), (rect.getY() - ipdInNanoMeters),
                                        (rect.getWidth() + 2*ipdInNanoMeters), (rect.getHeight() + 2*ipdInNanoMeters)));

   initializeShape(rect);
 }

 /**
  * @author Andy Mechtenberg
  */
 protected void paintComponent(Graphics graphics)
 {
   Graphics2D graphics2D = (Graphics2D)graphics;

   Color origColor = graphics2D.getColor();

   graphics2D.setColor(LayerColorEnum.IPD_COLOR.getColor());

   super.paintComponent(graphics);

   graphics2D.setColor(origColor);
 }

 /**
  * Return the name of the panel
  * @author Andy Mechtenberg
  */
 public String toString()
 {
   return _packagePin.getName();
 }


}
