package com.axi.v810.gui.drawCad;

import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.business.panelSettings.*;

/**
 * This class sets up the GraphicsEngine for a particular LibraryLandPattern so the
 * GraphicsEngine can draw the LibraryLandPattern's CAD graphics.
 *
 * This is class did not reuse the LandPatternGrapthicEngineSetup because
 * a) LibraryLandPattern doesn't need Observer and
 * b) To have a clear seperation of the Project Datastore and the Package Library object
 *
 * @author Poh Kheng
 */
public class LibraryLandPatternGraphicsEngineSetup
{
  private static final int _BORDER_SIZE_IN_PIXELS = 50;
  private static final int _libraryLandPatternPadLayer = JLayeredPane.DEFAULT_LAYER.intValue();

  private GraphicsEngine _graphicsEngine;
  private LibraryLandPattern _libraryLandPattern;
  private Map<LibraryLandPatternPad, LibraryLandPatternPadRenderer> _libraryLandPatternPadToLibraryLandPatternPadRendererMap
      = new HashMap<LibraryLandPatternPad, LibraryLandPatternPadRenderer>();

  /**
   * @author Poh Kheng
   */
  LibraryLandPatternGraphicsEngineSetup(DrawCadPanel drawCadPanel, GraphicsEngine graphicsEngine)
  {
    Assert.expect(drawCadPanel != null);
    Assert.expect(graphicsEngine != null);
    _graphicsEngine = graphicsEngine;

    _graphicsEngine.setPixelBorderSize(_BORDER_SIZE_IN_PIXELS);
  }

  /**
   * @author Poh Kheng
   */
  void displayLibraryLandPattern(LibraryLandPattern libraryLandPattern)
  {
    Assert.expect(libraryLandPattern != null);
    _libraryLandPattern = libraryLandPattern;

    populateLayersWithRenderers();
    setColors();
    setVisibility();
    setLayersToUseWhenFittingToScreen();

    _graphicsEngine.setAxisInterpretation(true, false);

    MathUtilEnum displayUnits = null;

    displayUnits = Project.getCurrentlyLoadedProject().getDisplayUnits();

    double measureFactor = 1.0/MathUtil.convertUnits(1.0, displayUnits, MathUtilEnum.NANOMETERS);
    _graphicsEngine.setMeasurementInfo(measureFactor, MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits),
       MeasurementUnits.getMeasurementUnitString(displayUnits));

  }

  /**
   * @author Poh Kheng
   */
  void reset()
  {
    _graphicsEngine.reset();
    _libraryLandPatternPadToLibraryLandPatternPadRendererMap.clear();
    _libraryLandPattern = null;
  }

  /**
   * Set up the layeredPanel for drawing packages
   * @author Poh Kheng
   */
  public GraphicsEngine getGraphicsEngine()
  {
    Assert.expect(_graphicsEngine != null);

    return _graphicsEngine;
  }

  /**
   * @author Poh Kheng
   */
  private void populateLayersWithRenderers()
  {
    Assert.expect(_libraryLandPattern != null);
    _graphicsEngine.reset();
    _libraryLandPatternPadToLibraryLandPatternPadRendererMap.clear();
    for (LibraryLandPatternPad libraryLandPatternPad: _libraryLandPattern.getLibraryLandPatternPads())
    {
      LibraryLandPatternPadRenderer libraryLandPatternPadRenderer = new LibraryLandPatternPadRenderer(libraryLandPatternPad);
      _graphicsEngine.addRenderer(_libraryLandPatternPadLayer, libraryLandPatternPadRenderer);
      _libraryLandPatternPadToLibraryLandPatternPadRendererMap.put(libraryLandPatternPad, libraryLandPatternPadRenderer);
    }
  }

  /**
   * Set up the proper colors for the layers
   * Poh Kheng
   */
  private void setVisibility()
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.setVisible(_libraryLandPatternPadLayer, true);
  }

  /**
   * Set up the proper colors for the layers
   * Poh Kheng
   */
  private void setColors()
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.setForeground(_libraryLandPatternPadLayer, LayerColorEnum.SURFACEMOUNT_PAD_COLOR.getColor());
  }

  /**
   * Set up which layers to use when figuring out how big the whole deal is
   * Poh Kheng
   */
  private void setLayersToUseWhenFittingToScreen()
  {
    Collection<Integer> layers = new ArrayList<Integer>(1);
    layers.add(new Integer(_libraryLandPatternPadLayer));
    _graphicsEngine.setLayersToUseWhenFittingToScreen(layers);
  }

  /**
   * @author Poh Kheng
   */
  public Integer getLandPatternPadLayer()
  {
    return new Integer(_libraryLandPatternPadLayer);
  }


}
