package com.axi.v810.gui.drawCad;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.geom.*;
import java.awt.Rectangle;
import java.awt.Shape;
import java.util.*;
import java.util.concurrent.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class sets up the GraphicsEngine for a particular Board so the
 * GraphicsEngine can draw the Board's CAD graphics
 *
 * @author Wei Chin
 */
public class BoardGraphicsEngineSetup extends Observable implements RendererCreator, Observer
{
  private static final int _BORDER_SIZE_IN_PIXELS = 15;//50;

  private static GuiObservable _guiObservable;

  private GraphicsEngine _graphicsEngine;
  private DrawCadPanel _drawCadPanel;
  private CreateNewCadPanel _createNewCadPanel;
  
  private Board _board;
  private ControlToolBar _controlToolBar;

  private int _layerNumber = JLayeredPane.DEFAULT_LAYER.intValue() -1;

  private final int _topBoardLayer = ++_layerNumber;
  private final int _topPadLayer = ++_layerNumber;
  private final int _topComponentLayer = ++_layerNumber;
  private final int _topRefDesLayer = ++_layerNumber;
  private final int _topFiducialLayer = ++_layerNumber;
  private final int _originLayer = ++_layerNumber;
  private final int _boardNameLayer = ++_layerNumber;
  private final int _bottomBoardLayer = ++_layerNumber;
  private final int _bottomPadLayer = ++_layerNumber;
  private final int _bottomComponentLayer = ++_layerNumber;
  private final int _bottomRefDesLayer = ++_layerNumber;
  private final int _bottomFiducialLayer = ++_layerNumber;

  private final int _alignmentRegionLayer = ++_layerNumber;

  // inspection regions
  private final int _topInspectionRegionRectangleLayer = ++_layerNumber;
  private final int _bottomInspectionRegionRectangleLayer = ++_layerNumber;
  private final int _topInspectionRegionPadBoundsLayer = ++_layerNumber;
  private final int _bottomInspectionRegionPadBoundsLayer = ++_layerNumber;
  private final int _topInspectionFocusRegionLayer = ++_layerNumber;
  private final int _bottomInspectionFocusRegionLayer = ++_layerNumber;

  // verification regions
  private final int _topVerificationRegionRectangleLayer = ++_layerNumber;
  private final int _bottomVerificationRegionRectangleLayer = ++_layerNumber;
  private final int _topVerificationFocusRegionLayer = ++_layerNumber;
  private final int _bottomVerificationFocusRegionLayer = ++_layerNumber;

    // alignment regions
  private final int _topAlignmentRegionRectangleLayer = ++_layerNumber;
  private final int _bottomAlignmentRegionRectangleLayer = ++_layerNumber;
  private final int _topAlignmentRegionPadBoundsLayer = ++_layerNumber;
  private final int _bottomAlignmentRegionPadBoundsLayer = ++_layerNumber;
  private final int _topAlignmentFocusRegionLayer = ++_layerNumber;
  private final int _bottomAlignmentFocusRegionLayer = ++_layerNumber;

  // irp boundaries
  private final int _inspectionIrpBoundariesLayer = ++_layerNumber;
  private final int _verificationIrpBoundariesLayer = ++_layerNumber;
  
  private int _dragOpticalRegionLayer = ++_layerNumber;
  private int _dragOpticalRegionUnSelectedLayer = ++_layerNumber;
  private int _highlightOpticalRegionLayer = ++_layerNumber;
  private int _opticalRegionSelectedLayer = ++_layerNumber;
  private int _opticalRegionUnSelectedLayer = ++_layerNumber;
  private int _dragAlignmentOpticalRegionLayer = ++_layerNumber;
  private int _meshTriangleLayer = ++_layerNumber;
  
  //@author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
  private int _topNoLoadedComponentLayer = ++_layerNumber;
  private int _bottomNoLoadedComponentLayer = ++_layerNumber;
  private int _topNoLoadPadLayer = ++_layerNumber;
  private int _bottomNoLoadPadLayer = ++_layerNumber;
  private int _panelWidthRendererLayer = ++_layerNumber;
  private int _topNoLoadPadNameLayer = ++_layerNumber;
  private int _bottomNoLoadPadNameLayer = ++_layerNumber;
  
  //@author By Kee Chin Seong - Renderer zoom in for RefDesingator must be updated when no load component is pressed.
  private int _topNoLoadRefDesignatorLayer = ++_layerNumber;
  private int _bottomNoLoadRefDesignatorLayer = ++_layerNumber;
   
  
  private Map<BoardType, List<BoardRenderer>> _boardTypeToBoardRenderersMap = new HashMap<BoardType, List<BoardRenderer>>();
  private Map<ComponentType, List<ComponentRenderer>> _componentTypeToComponentRenderersMap = new HashMap<ComponentType, List<ComponentRenderer>>();
  private Map<ComponentType, List<ReferenceDesignatorRenderer>> _componentTypeToReferenceDesignatorRenderersMap =
      new HashMap<ComponentType,  List<ReferenceDesignatorRenderer>>();
  private Map<Pad, PadRenderer> _padToPadRendererMap = new HashMap<Pad, PadRenderer>();
  private Map<ComponentRenderer, List<PadRenderer>> _componentRendererToPadRenderersMap = new HashMap<ComponentRenderer, List<PadRenderer>>();

  private List<HighlightAlignmentItems> _highlightPads = new ArrayList<HighlightAlignmentItems>();
  private java.util.List<String> _tableOpticalRegions = new ArrayList<String>();
  private int _index = -1;
  
  private static java.util.List<OpticalCameraRectangle> _opticalCameraRectangleList = new ArrayList<>();
  private java.util.List<OpticalCameraRectangle> _unselectedOpticalCameraRectangleList = new ArrayList<>();
  private java.util.List<OpticalCameraRectangle> _selectedOpticalCameraRectangleList = new ArrayList<>();
  private java.util.List<OpticalCameraRectangle> _highlightedOpticalCameraRectangleList = new ArrayList<>();
  private java.util.List<OpticalCameraRectangle> _ignoredOpticalCameraRectangleList = new ArrayList<>();
  
  
  //@author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
  private Map<ComponentType, List<ComponentRenderer>> _noLoadComponentTypeToComponentRenderersMap = new HashMap<ComponentType, List<ComponentRenderer>>();
  private Map<ComponentType, List<ReferenceDesignatorRenderer>> _noLoadComponentTypeToReferenceDesignatorRenderersMap =
      new HashMap<ComponentType,  List<ReferenceDesignatorRenderer>>();
  private Map<Pad, PadRenderer> _noLoadPadToPadRendererMap = new HashMap<Pad, PadRenderer>();
  private Map<ComponentRenderer, List<PadRenderer>> _noLoadComponentRendererToPadRenderersMap = new HashMap<ComponentRenderer, List<PadRenderer>>();
  //private Map<ComponentRenderer, List<PadNameRenderer>> _noLoadComponentRendererToPadNameRendererMap = new HashMap<ComponentRenderer, List<PadNameRenderer>>();
  
  private double _currentZoomFactor = 0;
  private double _currentCanvasWidthToHeightRatio = 0;
  private boolean _drawDebugGraphics = false;

  // reference to a single board that was selected
  private String _selectedBoardName = null;
  
  private DragOpticalRectangleRenderer _dragOpticalRectangleRenderer;
  private HighlightOpticalRectangleRenderer _highlightRectangleRenderer;
  private SelectedOpticalRectangleRenderer _selectedRectangleRenderer;
  private MeshTriangleRenderer _meshTriangleRenderer;
  private static Map<Rectangle, String> _alignmentOpticalRegionList = new ConcurrentHashMap<Rectangle, String>();
 
  //Kee Chin Seong - XXL Speed Development - UI Graphic Engine Controler
  private boolean _showTopComponentLayer = true;
  private boolean _showTopComponentRefDesLayer = true;
  private boolean _showTopPadLayer = true;
  private boolean _showTopPanelLayer = true;
  
  private boolean _showBottomComponentLayer = false;
  private boolean _showBottomComponentRefDesLayer = false;
  private boolean _showBottomPadLayer = true;
  private boolean _showBottomPanelLayer = true;
  
  private boolean _showNoLoadComponent = false;
 
  //Kee Chin Seong
  private Shape _selectedFocusRegion;
  private boolean _isFocusRegionEditorOn = false;
  
  /**
   * @author Wei Chin
   */
  static
  {
    _guiObservable = GuiObservable.getInstance();
  }


  /**
   * @author Wei Chin
   */
  BoardGraphicsEngineSetup(DrawCadPanel drawCadPanel, GraphicsEngine graphicsEngine)
  {
    Assert.expect(drawCadPanel != null);
    Assert.expect(graphicsEngine != null);
    _drawCadPanel = drawCadPanel;
    _graphicsEngine = graphicsEngine;

    _graphicsEngine.setPixelBorderSize(_BORDER_SIZE_IN_PIXELS);

    try
    {
      _drawDebugGraphics = LicenseManager.isDeveloperSystemEnabled() && TestDev.isInDeveloperDebugMode();
    }
    catch (BusinessException bex)
    {
      _drawDebugGraphics = false;
      MainMenuGui.getInstance().handleXrayTesterException(bex);
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  BoardGraphicsEngineSetup(CreateNewCadPanel createNewCadPanel, GraphicsEngine graphicsEngine)
  {
    Assert.expect(createNewCadPanel != null);
    Assert.expect(graphicsEngine != null);
    _createNewCadPanel = createNewCadPanel;
    _graphicsEngine = graphicsEngine;

    _graphicsEngine.setPixelBorderSize(_BORDER_SIZE_IN_PIXELS);

    try
    {
      _drawDebugGraphics = LicenseManager.isDeveloperSystemEnabled() && TestDev.isInDeveloperDebugMode();
    }
    catch (BusinessException bex)
    {
      _drawDebugGraphics = false;
      MainMenuGui.getInstance().handleXrayTesterException(bex);
    }
  }

  /**
   * @author Wei Chin
   */
  void addObservers()
  {
    // add this as an observer of the project
    ProjectObservable.getInstance().addObserver(this);
    // add this as an observer of the gui
    _guiObservable.addObserver(this);
    // add this as an observer of the engine
    _graphicsEngine.getSelectedRendererObservable().addObserver(this);    
    // add this as an observer of the psp drag region
    _graphicsEngine.getDragOpticalRegionObservable().addObserver(this);
    _graphicsEngine.getShowNoLoadComponentObservable().addObserver(this);
 
  }

  /**
    * @author Wei Chin
    */
   void removeObservers()
   {
     // remove this as an observer of the project data
     ProjectObservable.getInstance().deleteObserver(this);
     // remove this as an observer of the gui
     _guiObservable.deleteObserver(this);
     // remove this as an observer of the engine
     _graphicsEngine.getSelectedRendererObservable().deleteObserver(this);
     // remove this as a observer of the psp drag region
     _graphicsEngine.getDragOpticalRegionObservable().deleteObserver(this);
     _graphicsEngine.getShowNoLoadComponentObservable().deleteObserver(this);
  }

   /**
   * @author Wei Chin
   */
  void unpopulate()
  {
    _board = null;
    clearAllInternalEngineData();
    
    //Ngie Xing
    _showTopComponentLayer = true;
    _showTopPadLayer = true;
    _showTopComponentRefDesLayer = true;
    _showTopPanelLayer = true;

    _showBottomComponentLayer = false;
    _showBottomPadLayer = false;
    _showBottomComponentRefDesLayer = false;
    _showBottomPanelLayer = false;
  }
  
   /**
   * @author Kee Chin Seong
   */
  private void populateReferenceDesignatorLayerWithRenderers(Board board, Collection<Component> components, boolean isTopside)
  {
    for (Component component : components)
    {
      if(component.isLoaded() == true)
      {
         ReferenceDesignatorRenderer referenceDesignatorRenderer = new ReferenceDesignatorRenderer(component);
         
         if(isTopside == true)
          _graphicsEngine.addRenderer(_topRefDesLayer, referenceDesignatorRenderer);
         else
          _graphicsEngine.addRenderer(_bottomRefDesLayer, referenceDesignatorRenderer);
         
         List<ReferenceDesignatorRenderer> compRefDesRenderers = null;
         if (_componentTypeToReferenceDesignatorRenderersMap.containsKey(component.getComponentType()))
           compRefDesRenderers = _componentTypeToReferenceDesignatorRenderersMap.get(component.getComponentType());
         else
         {
           compRefDesRenderers = new ArrayList<ReferenceDesignatorRenderer>();
           Object previous = _componentTypeToReferenceDesignatorRenderersMap.put(component.getComponentType(), compRefDesRenderers);
           Assert.expect(previous == null);
         }
         compRefDesRenderers.add(referenceDesignatorRenderer);
      }
      else
      {
         ReferenceDesignatorRenderer referenceDesignatorRenderer = new ReferenceDesignatorRenderer(component);

         Integer layerInt = (isTopside == true) ? (Integer)_topNoLoadRefDesignatorLayer :
                                                     (Integer)_bottomNoLoadRefDesignatorLayer;
         
         _graphicsEngine.addRenderer(layerInt.intValue(), referenceDesignatorRenderer);

         List<ReferenceDesignatorRenderer> compRefDesRenderers = null;
         if (_noLoadComponentTypeToReferenceDesignatorRenderersMap.containsKey(component.getComponentType()))
           compRefDesRenderers = _noLoadComponentTypeToReferenceDesignatorRenderersMap.get(component.getComponentType());
         else
         {
           compRefDesRenderers = new ArrayList<ReferenceDesignatorRenderer>();
           Object previous = _noLoadComponentTypeToReferenceDesignatorRenderersMap.put(component.getComponentType(), compRefDesRenderers);
           Assert.expect(previous == null);
         }
         compRefDesRenderers.add(referenceDesignatorRenderer);
       }
    }
  }
  
  private void unPopulateComponentLayerWithRenderers(Collection<Component> components)
  {
    for(Component component  : components)
    {
      _componentTypeToComponentRenderersMap.remove(component.getComponentType());
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void populateComponentLayersWithRenderers(Board board, Collection<Component> components, boolean isTopSide, boolean isNeedRepopulate)
  {
     for (Component component : components)
     {
        if(component.isLoaded() == true)
        {
          ComponentRenderer componentRenderer = null;
          
          if(!isNeedRepopulate)
             componentRenderer = new ComponentRenderer(this, component);
          
          ComponentType componentType = component.getComponentType();
          List<ComponentRenderer> componentRenderers = null;
          //ComponentRenderer componentRenderer = null;
          List<ReferenceDesignatorRenderer> compRefDesRenderers = null;
          if (_componentTypeToComponentRenderersMap.containsKey(componentType))
          {
            componentRenderers = _componentTypeToComponentRenderersMap.get(componentType);
          }
          else
          {
            componentRenderers = new ArrayList<ComponentRenderer>();
            Object previous = _componentTypeToComponentRenderersMap.put(componentType, componentRenderers);
            Assert.expect(previous == null);
          }
          
          if(isNeedRepopulate)
          {
            if(componentRenderers.size() == 0 )
            {
              componentRenderer = new ComponentRenderer(this, component);
              componentRenderers.add(componentRenderer);
            }
          }
          else
            componentRenderers.add(componentRenderer);
          
          Integer layerInt = (isTopSide == true) ? (Integer)_topComponentLayer :
                                                   (Integer)_bottomComponentLayer;
          
          //for(ComponentRenderer compRenderer : componentRenderers)
            _graphicsEngine.addRenderer(layerInt.intValue(), componentRenderer); 
           
        }
        else
        {
          ComponentRenderer componentRenderer = null;
          
          if(!isNeedRepopulate)
             componentRenderer = new ComponentRenderer(this, component);
          
          ComponentType componentType = component.getComponentType();
          List<ComponentRenderer> componentRenderers = null;
          //ComponentRenderer componentRenderer = null;
          List<ReferenceDesignatorRenderer> compRefDesRenderers = null;
          if (_noLoadComponentTypeToComponentRenderersMap.containsKey(componentType))
          {
            componentRenderers = _noLoadComponentTypeToComponentRenderersMap.get(componentType);
          }
          else
          {
            componentRenderers = new ArrayList<ComponentRenderer>();
            Object previous = _noLoadComponentTypeToComponentRenderersMap.put(componentType, componentRenderers);
            Assert.expect(previous == null);
          }
          
          if(isNeedRepopulate)
          {
            if(componentRenderers.size() == 0 )
            {
              componentRenderer = new ComponentRenderer(this, component);
              componentRenderers.add(componentRenderer);
            }
          }
          else
            componentRenderers.add(componentRenderer);
          
          Integer layerInt = (isTopSide == true) ? (Integer)_topNoLoadedComponentLayer :
                                                   (Integer)_bottomNoLoadedComponentLayer;
          
          _graphicsEngine.addRenderer(layerInt.intValue(), componentRenderer); 
             
        }
      
        // pads are only displayed once the user zooms in
//          Collection pads = component.getPads();
//          numPads += pads.size();
//          Iterator pit = pads.iterator();
//          while (pit.hasNext())
//          {
//            Pad pad = (Pad)pit.next();
//            PadRenderer padRenderer = new PadRenderer(pad, componentRenderer);
//            layerInt = (Integer)_boardToTopPadLayerMap.get(board);
//            _graphicsEngine.addRenderer(layerInt.intValue(), padRenderer);
//
//            if (pad.isThroughHolePad())
//            {
//              // through hole pads show up on both sides of the panel
//              padRenderer = new PadRenderer(pad, componentRenderer);
//              layerInt = (Integer)_boardToBottomPadLayerMap.get(board);
//              _graphicsEngine.addRenderer(layerInt.intValue(), padRenderer);
//            }
//          }
     }
  }
  
  private void populateFiducialWithLayers(Board board, Collection<Fiducial> boardFiducials, boolean isTopSide)
  {
     for (Fiducial fiducial : boardFiducials)
     {
        FiducialRenderer fiducialRenderer = new FiducialRenderer(fiducial);
        
        Integer layerInt = (isTopSide == true) ? (Integer)_topComponentLayer :
                                                 (Integer)_bottomComponentLayer;
        
        _graphicsEngine.addRenderer(layerInt.intValue(), fiducialRenderer);
        FiducialReferenceDesignatorRenderer fiducialReferenceDesignatorRenderer = new FiducialReferenceDesignatorRenderer(fiducial);
        
        layerInt = (isTopSide == true) ? (Integer)_topComponentLayer :
                                         (Integer)_bottomComponentLayer;
        
        _graphicsEngine.addRenderer(layerInt.intValue(), fiducialReferenceDesignatorRenderer);
     }
  }


  /**
   * @author Wei Chin
   */
  private void clearAllInternalEngineData()
  {
    _boardTypeToBoardRenderersMap.clear();
    _componentTypeToComponentRenderersMap.clear();
    _componentTypeToReferenceDesignatorRenderersMap.clear();
    _componentRendererToPadRenderersMap.clear();
    _padToPadRendererMap.clear();
    _highlightPads.clear();
    _graphicsEngine.clearAllFlashingRenderersAndLayers();
    
    //@author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
    _noLoadComponentTypeToComponentRenderersMap.clear();
    _noLoadComponentTypeToReferenceDesignatorRenderersMap.clear();
    _noLoadComponentRendererToPadRenderersMap.clear();
    //_noLoadComponentRendererToPadNameRendererMap.clear();
  }
  
  /**
   * @author Wei Chin
   */
  void setControlToolBar(ControlToolBar controlToolBar)
  {
    Assert.expect(controlToolBar != null);
    _controlToolBar = controlToolBar;
  }

  /**
   * @author Wei Chin
   */
  void displayBoard(Board board)
  {
    Assert.expect(board != null);
    _board = board;
    clearAllInternalEngineData();

    populateLayersWithRenderers();
    setColors();
    setVisibility();
    setThresholds();
    setLayersToUseWhenFittingToScreen();

    if(_showNoLoadComponent)
      _graphicsEngine.setVisible(_topNoLoadPadNameLayer, true); 

 
    _graphicsEngine.setAxisInterpretation(true, false);

    MathUtilEnum displayUnits = _board.getPanel().getProject().getDisplayUnits();
    double measureFactor = 1.0/MathUtil.convertUnits(1.0, displayUnits, MathUtilEnum.NANOMETERS);
    _graphicsEngine.setMeasurementInfo(measureFactor, MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits),
       MeasurementUnits.getMeasurementUnitString(displayUnits));

  }

  /**
   * Set up the layeredPanel for drawing board
   * @author Wei Chin
   */
  public GraphicsEngine getGraphicsEngine()
  {
    Assert.expect(_graphicsEngine != null);

    return _graphicsEngine;
  }

  /**
   * @author Bill Darbie
   */
  private void populateLayersWithRenderers()
  {
    _graphicsEngine.reset();

    populateLayersWithDebugGraphics();
    populateBoardLayers(_board);
  }

  /**
   * @author Andy Mechtenberg
   * @author Kee Chin Seong
   */
  private void populateBoardLayers(Board board)
  {
    BoardType boardType = board.getBoardType();
    List<BoardRenderer> boardRenderers = null;

    if (_boardTypeToBoardRenderersMap.containsKey(boardType))
    {
      boardRenderers = _boardTypeToBoardRenderersMap.get(boardType);
    }
    else
    {
      boardRenderers = new ArrayList<BoardRenderer>();
      Object previous = _boardTypeToBoardRenderersMap.put(boardType, boardRenderers);
      Assert.expect(previous == null);
    }
    BoardRenderer topBoardRenderer = new BoardRenderer(board);
    boardRenderers.add(topBoardRenderer);
    _graphicsEngine.addRenderer(_topBoardLayer, topBoardRenderer);
    BoardRenderer bottomBoardRenderer = new BoardRenderer(board);
    boardRenderers.add(bottomBoardRenderer);
    _graphicsEngine.addRenderer(_bottomBoardLayer, bottomBoardRenderer);

    if (board.topSideBoardExists())
    {
      SideBoard topSideBoard = board.getTopSideBoard();

      // add the board origin and axis renderers
      BoardCadOriginRenderer boardOriginRenderer = new BoardCadOriginRenderer(board);
      _graphicsEngine.addRenderer(_originLayer, boardOriginRenderer);
      BoardCadOriginAxisLetterRenderer xAxisRenderer = new BoardCadOriginAxisLetterRenderer(board, true);
      _graphicsEngine.addRenderer(_originLayer, xAxisRenderer);
      BoardCadOriginAxisLetterRenderer yAxisRenderer = new BoardCadOriginAxisLetterRenderer(board, false);
      _graphicsEngine.addRenderer(_originLayer, yAxisRenderer);

      //add the board name renderer
      BoardNameRenderer boardNameRenderer = new BoardNameRenderer(board);
      _graphicsEngine.addRenderer(_boardNameLayer, boardNameRenderer);

      // Populate top board side components
      Collection<Component> components = topSideBoard.getComponents();
      populateComponentLayersWithRenderers(board, components, true, false);
      populateReferenceDesignatorLayerWithRenderers(board, components, true);
      
      // Populate top board side fiducials
      populateFiducialWithLayers(board, topSideBoard.getFiducials(), true);
    }

    // moving on to the bottom board
    if (board.bottomSideBoardExists())
    {
      SideBoard bottomSideBoard = board.getBottomSideBoard();

      // add the board origin and axis renderers
      BoardCadOriginRenderer boardOriginRenderer = new BoardCadOriginRenderer(board);
      _graphicsEngine.addRenderer(_originLayer, boardOriginRenderer);
      BoardCadOriginAxisLetterRenderer xAxisRenderer = new BoardCadOriginAxisLetterRenderer(board, true);
      _graphicsEngine.addRenderer(_originLayer, xAxisRenderer);
      BoardCadOriginAxisLetterRenderer yAxisRenderer = new BoardCadOriginAxisLetterRenderer(board, false);
      _graphicsEngine.addRenderer(_originLayer, yAxisRenderer);

      //add the board name renderer only if the top side didn't exist
      if ((board.topSideBoardExists() == false))// && (board.getPanel().getNumBoards() > 1))
      {
        BoardNameRenderer boardNameRenderer = new BoardNameRenderer(board);
        _graphicsEngine.addRenderer(_boardNameLayer, boardNameRenderer);
      }
      Collection<Component> components = bottomSideBoard.getComponents();
            populateComponentLayersWithRenderers(board, components, false, false);
      populateReferenceDesignatorLayerWithRenderers(board, components, false);
      
      // Populate bottom board side fiducials
      populateFiducialWithLayers(board, bottomSideBoard.getFiducials(), false);
    }
  }
  
   /*
   * @author Kee Chin Seong
   */
  public void setTopGraphicLayersAccordingToCustomizeSettings()
  {
    _graphicsEngine.setVisible(_topBoardLayer, _showTopPanelLayer);
    _graphicsEngine.setVisible(_topFiducialLayer, true);
    _graphicsEngine.setVisible(_topRefDesLayer, true);

    _graphicsEngine.setVisible(getTopBoardLayer(), true);
    _graphicsEngine.setVisible(getTopPadLayer(), _showTopPadLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopComponentLayer(), _showTopComponentLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopReferenceDesignatorLayer(), _showTopComponentRefDesLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopBoardFiducialLayer(), true);
    _graphicsEngine.setVisible(getBoardNameLayer(), false);

    _graphicsEngine.setVisible(_topInspectionRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_topInspectionRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_topInspectionFocusRegionLayer, true);
    _graphicsEngine.setVisible(_topAlignmentRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_topAlignmentRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_topAlignmentFocusRegionLayer, true);
    _graphicsEngine.setVisible(_topVerificationRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_topVerificationFocusRegionLayer, true);
    
    _graphicsEngine.setVisible(getBottomLayers(), false);
    
    _graphicsEngine.setCrossHairsSelectionVisibility(false);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setBottomGraphicLayersAccordingToCustomizeSettings()
  {
    _graphicsEngine.setVisible(_bottomBoardLayer, _showBottomPanelLayer);
    _graphicsEngine.setVisible(_bottomFiducialLayer, true);
    _graphicsEngine.setVisible(_bottomRefDesLayer, true);

    _graphicsEngine.setVisible(getBottomBoardLayer(), true);
    _graphicsEngine.setVisible(getBottomPadLayer(), _showBottomPadLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomComponentLayer(), _showBottomComponentLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomReferenceDesignatorLayer(), _showBottomComponentRefDesLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomBoardFiducialLayer(), true);
    _graphicsEngine.setVisible(getBoardNameLayer(), false);

    _graphicsEngine.setVisible(_bottomInspectionRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_bottomInspectionRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_bottomInspectionFocusRegionLayer, true);
    _graphicsEngine.setVisible(_bottomAlignmentRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_bottomAlignmentRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_bottomAlignmentFocusRegionLayer, true);
    _graphicsEngine.setVisible(_bottomVerificationRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_bottomVerificationFocusRegionLayer, true);
    
    _graphicsEngine.setVisible(getTopLayers(), false);
    
    _graphicsEngine.setCrossHairsSelectionVisibility(false);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setBothSidesGraphicLayersAccordingToCustomizeSettings()
  {
    //set top side
    _graphicsEngine.setVisible(_topBoardLayer, _showTopPanelLayer);
    _graphicsEngine.setVisible(_topFiducialLayer, true);
    _graphicsEngine.setVisible(_topRefDesLayer, true);

    _graphicsEngine.setVisible(getTopBoardLayer(), true);
    _graphicsEngine.setVisible(getTopPadLayer(), _showTopPadLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopComponentLayer(), _showTopComponentLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopReferenceDesignatorLayer(), _showTopComponentRefDesLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopBoardFiducialLayer(), true);
    _graphicsEngine.setVisible(getBoardNameLayer(), false);

    _graphicsEngine.setVisible(_topInspectionRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_topInspectionRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_topInspectionFocusRegionLayer, true);
    _graphicsEngine.setVisible(_topAlignmentRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_topAlignmentRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_topAlignmentFocusRegionLayer, true);
    _graphicsEngine.setVisible(_topVerificationRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_topVerificationFocusRegionLayer, true);

    //Ok, now set bottom side
    _graphicsEngine.setVisible(_bottomBoardLayer, _showBottomPanelLayer);
    _graphicsEngine.setVisible(_bottomFiducialLayer, true);
    _graphicsEngine.setVisible(_bottomRefDesLayer, true);

    _graphicsEngine.setVisible(getBottomBoardLayer(), true);
    _graphicsEngine.setVisible(getBottomPadLayer(), _showBottomPadLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomComponentLayer(), _showBottomComponentLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomReferenceDesignatorLayer(), _showBottomComponentRefDesLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomBoardFiducialLayer(), true);
    _graphicsEngine.setVisible(getBoardNameLayer(), false);

    _graphicsEngine.setVisible(_bottomInspectionRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_bottomInspectionRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_bottomInspectionFocusRegionLayer, true);
    _graphicsEngine.setVisible(_bottomAlignmentRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_bottomAlignmentRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_bottomAlignmentFocusRegionLayer, true);
    _graphicsEngine.setVisible(_bottomVerificationRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_bottomVerificationFocusRegionLayer, true);
    
    _graphicsEngine.setCrossHairsSelectionVisibility(false);
  }
  

  /**
   * @author George A. David
   */
  private void repopulateLayersWithDebugAlignmentGraphics()
  {
    if (_drawDebugGraphics)
    {
      _graphicsEngine.removeRenderers(_topAlignmentFocusRegionLayer);
      _graphicsEngine.removeRenderers(_topAlignmentRegionPadBoundsLayer);
      _graphicsEngine.removeRenderers(_topAlignmentRegionRectangleLayer);
      _graphicsEngine.removeRenderers(_bottomAlignmentFocusRegionLayer);
      _graphicsEngine.removeRenderers(_bottomAlignmentRegionPadBoundsLayer);
      _graphicsEngine.removeRenderers(_bottomAlignmentRegionRectangleLayer);

      populateLayersWithDebugAlignmentGraphics();
    }
  }

  /**
   * @author George A. David
   */
  private void populateLayersWithDebugAlignmentGraphics()
  {
    if (_drawDebugGraphics)
    {
      TestProgram testProgram = MainMenuGui.getInstance().getTestProgramWithReason("Calculating data for graphics engine. . .",
          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
          _board.getPanel().getProject());
      TestSubProgram currentSubProgram = null;

      for(TestSubProgram subProgram :testProgram.getAllTestSubPrograms())
      {
        if(subProgram.getBoard().equals(_board))
          currentSubProgram = subProgram;
      }

      Assert.expect(currentSubProgram != null);

      // alignmentRegions regions
      for (ReconstructionRegion alignmentRegion : currentSubProgram.getAlignmentRegions())
      {
        InspectionRegionRenderer regionRenderer = new InspectionRegionRenderer(alignmentRegion);
        InspectionRegionRenderer boundsRenderer = new InspectionRegionRenderer(alignmentRegion);
        boundsRenderer.setUsePadBounds(true);
        if (alignmentRegion.isTopSide())
        {
          _graphicsEngine.addRenderer(_topAlignmentRegionRectangleLayer, regionRenderer);
          _graphicsEngine.addRenderer(_topAlignmentRegionPadBoundsLayer, boundsRenderer);
        }
        else
        {
          _graphicsEngine.addRenderer(_bottomAlignmentRegionRectangleLayer, regionRenderer);
          _graphicsEngine.addRenderer(_bottomAlignmentRegionPadBoundsLayer, boundsRenderer);
        }

        int index = 0;
        for (FocusGroup focusGroup : alignmentRegion.getFocusGroups())
        {
          if (index == 0)
          {
            FocusRegionRenderer focusRenderer = new FocusRegionRenderer(focusGroup.getFocusSearchParameters().
                getFocusRegion());
            if (alignmentRegion.isTopSide())
              _graphicsEngine.addRenderer(_topAlignmentFocusRegionLayer, focusRenderer);
            else
              _graphicsEngine.addRenderer(_bottomAlignmentFocusRegionLayer, focusRenderer);
            break;
          }
          ++index;
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  private void repopulateLayersWithDebugGraphics()
  {
    if (_drawDebugGraphics)
    {
      _graphicsEngine.removeRenderers(_inspectionIrpBoundariesLayer);
      _graphicsEngine.removeRenderers(_verificationIrpBoundariesLayer);

      _graphicsEngine.removeRenderers(_topVerificationFocusRegionLayer);
      _graphicsEngine.removeRenderers(_topVerificationRegionRectangleLayer);
      _graphicsEngine.removeRenderers(_bottomVerificationFocusRegionLayer);
      _graphicsEngine.removeRenderers(_bottomVerificationRegionRectangleLayer);

      _graphicsEngine.removeRenderers(_topInspectionFocusRegionLayer);
      _graphicsEngine.removeRenderers(_topInspectionRegionPadBoundsLayer);
      _graphicsEngine.removeRenderers(_topInspectionRegionRectangleLayer);
      _graphicsEngine.removeRenderers(_bottomInspectionFocusRegionLayer);
      _graphicsEngine.removeRenderers(_bottomInspectionRegionPadBoundsLayer);
      _graphicsEngine.removeRenderers(_bottomInspectionRegionRectangleLayer);

      _graphicsEngine.removeRenderers(_topAlignmentFocusRegionLayer);
      _graphicsEngine.removeRenderers(_topAlignmentRegionPadBoundsLayer);
      _graphicsEngine.removeRenderers(_topAlignmentRegionRectangleLayer);
      _graphicsEngine.removeRenderers(_bottomAlignmentFocusRegionLayer);
      _graphicsEngine.removeRenderers(_bottomAlignmentRegionPadBoundsLayer);
      _graphicsEngine.removeRenderers(_bottomAlignmentRegionRectangleLayer);

      populateLayersWithDebugGraphics();
    }
  }

/**
   * This method is synchronized because it's call from both the initial populate and the response to a program
   * generation.  But, the initial populate needs to create a test program, so this may be called prior to it being
   * initially populated
   * @author George A. David
   */
  private synchronized void populateLayersWithDebugGraphics()
  {
    // populate the region layer only if it's turned on (performance reasons)
    Project project = _board.getPanel().getProject();
    
    //XCR1745 - fix crash when all components are set to no test
    if(project.getTestProgram().getAllInspectableTestSubPrograms().isEmpty() == true)
      return;
    
    TestProgram testProgram = MainMenuGui.getInstance().getTestProgramWithReason("Calculating data for graphics engine. . .",
          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
          project);
    
    TestSubProgram currentSubProgram = null;
    
    for(TestSubProgram subProgram :testProgram.getAllTestSubPrograms())
    {
      //XCR-3204 Assert when click "Alignment Setting" after cancel alignment and go fine tuning
      //if(subProgram.isTestSubProgramUseCustomizedAlignmentSetting())
      //  continue;
      
      if(subProgram.getBoard().equals(_board))
          currentSubProgram = subProgram;
    }
    Assert.expect(currentSubProgram != null);
    
    if (_drawDebugGraphics)
    {
      // irp boundaries
      if (currentSubProgram.hasInspectionRegions())
      {
        for (ProcessorStrip processorStrip : currentSubProgram.getProcessorStripsForInspectionRegions())
        {
          RectangleRenderer renderer = new RectangleRenderer(processorStrip.getRegion().getRectangle2D(), true);
          renderer.setFillShape(false);
          _graphicsEngine.addRenderer(_inspectionIrpBoundariesLayer, renderer);
        }

        for (ProcessorStrip processorStrip : currentSubProgram.getProcessorStripsForVerificationRegions())
        {
          RectangleRenderer renderer = new RectangleRenderer(processorStrip.getRegion().getRectangle2D(), true);
          renderer.setFillShape(false);
          _graphicsEngine.addRenderer(_verificationIrpBoundariesLayer, renderer);
        }
      }


      // verification regions
      for (ReconstructionRegion verificationRegion : currentSubProgram.getVerificationRegions())
      {
        VerificationRegionRenderer regionRenderer = new VerificationRegionRenderer(verificationRegion);
        if (verificationRegion.isTopSide())
          _graphicsEngine.addRenderer(_topVerificationRegionRectangleLayer, regionRenderer);
        else
          _graphicsEngine.addRenderer(_bottomVerificationRegionRectangleLayer, regionRenderer);

        for (FocusGroup focusGroup : verificationRegion.getFocusGroups())
        {
          FocusRegionRenderer focusRenderer = new FocusRegionRenderer(focusGroup.getFocusSearchParameters().
              getFocusRegion());
          if (verificationRegion.isTopSide())
            _graphicsEngine.addRenderer(_topVerificationFocusRegionLayer, focusRenderer);
          else
            _graphicsEngine.addRenderer(_bottomVerificationFocusRegionLayer, focusRenderer);
        }
      }

      // inspection regions
      for (ReconstructionRegion inspectionRegion : currentSubProgram.getAllInspectionRegions())
      {
        InspectionRegionRenderer boundsRenderer = new InspectionRegionRenderer(inspectionRegion);
        boundsRenderer.setUsePadBounds(true);
        if (inspectionRegion.isTopSide())
        {
          _graphicsEngine.addRenderer(_topInspectionRegionPadBoundsLayer, boundsRenderer);
        }
        else
        {
          _graphicsEngine.addRenderer(_bottomInspectionRegionPadBoundsLayer, boundsRenderer);
        }
      }
      populateLayersWithDebugAlignmentGraphics();
    }

    for (ReconstructionRegion inspectionRegion : currentSubProgram.getAllInspectionRegions())
    {
      InspectionRegionRenderer regionRenderer = new InspectionRegionRenderer(inspectionRegion);
      if (inspectionRegion.isTopSide())
      {
        _graphicsEngine.addRenderer(_topInspectionRegionRectangleLayer, regionRenderer);
      }
      else
      {
        _graphicsEngine.addRenderer(_bottomInspectionRegionRectangleLayer, regionRenderer);
      }

      int index = 0;
      for (FocusGroup focusGroup : inspectionRegion.getFocusGroups())
      {
        if (index == 0)
        {
          FocusRegionRenderer focusRenderer = new FocusRegionRenderer(focusGroup.getFocusSearchParameters().
                    getFocusRegion());
          if (inspectionRegion.isTopSide())
          {
            _graphicsEngine.addRenderer(_topInspectionFocusRegionLayer, focusRenderer);
          }
          else
          {
            _graphicsEngine.addRenderer(_bottomInspectionFocusRegionLayer, focusRenderer);
          }
          break;
        }
        ++index;
      }
    }
  }

  /**
   * @return a List of Integers of all the top side layers
   * @author Wei Chin
   */
  List<Integer> getTopLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(_topBoardLayer);
    layers.add(_topPadLayer);
    layers.add(_topComponentLayer);
    layers.add(_topRefDesLayer);
    layers.add(_topFiducialLayer);
    //layers.add(_boardNameLayer);
    if(_isFocusRegionEditorOn == true)
    {
      layers.add(_topInspectionRegionRectangleLayer);
      layers.add(_topInspectionFocusRegionLayer);
    }
    layers.add(_topInspectionRegionPadBoundsLayer);
    layers.add(_topAlignmentRegionRectangleLayer);
    layers.add(_topAlignmentRegionPadBoundsLayer);
    layers.add(_topAlignmentFocusRegionLayer);
    layers.add(_topVerificationRegionRectangleLayer);
    layers.add(_topVerificationFocusRegionLayer);

    return layers;
  }

  /**
   * @return a List of Integers of all the bottom side layers
   * @author Wei Chin
   */
  List<Integer> getBottomLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(_bottomBoardLayer);
    layers.add(_bottomPadLayer);
    layers.add(_bottomComponentLayer);
    layers.add(_bottomRefDesLayer);
    layers.add(_bottomFiducialLayer);
    if(_isFocusRegionEditorOn == true)
    {
      layers.add(_bottomInspectionRegionRectangleLayer);
      layers.add(_bottomInspectionFocusRegionLayer);
    }
    layers.add(_bottomInspectionRegionPadBoundsLayer);
    layers.add(_bottomAlignmentRegionRectangleLayer);
    layers.add(_bottomAlignmentRegionPadBoundsLayer);
    layers.add(_bottomAlignmentFocusRegionLayer);
    layers.add(_bottomVerificationRegionRectangleLayer);
    layers.add(_bottomVerificationFocusRegionLayer);

    return layers;
  }

  /**
   * @author Wei Chin
   */
  int getBoardOriginLayers()
  {
    return _originLayer;
  }

  /**
   * @author Wei Chin
   */
  int getBoardNameLayer()
  {
    return _boardNameLayer;
  }

  /**
   * @return a List of Integers of all the layers on the entire panel
   * for both sides
   * @author Wei Chin
   */
  public List<Integer> getAllLayers()
  {
    List<Integer> layers = getTopLayers();
    layers.addAll(getBottomLayers());
    layers.add(getBoardOriginLayers());

    return layers;
  }

/**
   * @return the top layer used by the Board passed in
   * @author Wei Chin
   */
  int getTopBoardLayer()
  {
    return _topBoardLayer;
  }

  /**
   * @return the bottom layer used by the Board passed in
   * @author Wei Chin
   */
  private int getBottomBoardLayer()
  {
    return _bottomBoardLayer;
  }

  /**
   * @return a List of Integers of all Board layers
   * @author Wei Chin
   */
  List<Integer> getBoardLayer()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.add(getTopBoardLayer());
    layers.add(getBottomBoardLayer());

    return layers;
  }

/**
   * @return the top Component layer used by the board passed in
   * @author Wei Chin
   */
  int getTopComponentLayer()
  {
    return _topComponentLayer;
  }

  /**
   * @return the bottom Component layer used by the board passed in
   * @author Wei Chin
   */
  int getBottomComponentLayer()
  {
    return _bottomComponentLayer;
  }

  /**
   * @return a List of Integers of all Component layers
   * @author Wei Chin
   */
  List<Integer> getComponentLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.add(getTopComponentLayer());
    layers.add(getBottomComponentLayer());

    return layers;
  }

  /**
   * @return the top Pad layer used by the board passed in
   * @author Wei Chin
   */
  int getTopPadLayer()
  {
    return _topPadLayer;
  }
/**
   * @return the bottom Pad layer used by the board passed in
   * @author Wei Chin
   */
  int getBottomPadLayer()
  {
    return _bottomPadLayer;
  }

  /**
   * @return a List of Integers of all Pad layers
   * @author Wei Chin
   */
  List<Integer> getPadLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.add(getTopPadLayer());
    layers.add(getBottomPadLayer());

    return layers;
  }

/**
   * @return the top layer used by the Reference Designators for the board passed in
   * @author Wei Chin
   */
  int getTopReferenceDesignatorLayer()
  {
    return _topRefDesLayer;
  }

  /**
   * @return the bottom layer used by the Reference Designators for the board passed in
   * @author Wei Chin
   */
  int getBottomReferenceDesignatorLayer()
  {
    return _bottomRefDesLayer;
  }

  /**
   * @return a List of Integers of all Reference Designator layers
   * @author Wei Chin
   */
  List<Integer> getReferenceDesignatorLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.add(getTopReferenceDesignatorLayer());
    layers.add(getBottomReferenceDesignatorLayer());

    return layers;
  }

  /**
   * @return the top Fiducial layer used by the board passed in
   * @author Wei Chin
   */
  int getTopBoardFiducialLayer()
  {
    return _topFiducialLayer;
  }

  /**
   * @return the bottom Fiducial layer used by the board passed in
   * @author Wei Chin
   */
  int getBottomBoardFiducialLayer()
  {
    return _bottomFiducialLayer;
  }

  /**
   * @return a List of Integers of all Fiducial layers
   * @author Wei Chin
   */
  List<Integer> getFiducialLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.add(getTopBoardFiducialLayer());
    layers.add(getBottomBoardFiducialLayer());

    return layers;
  }

  /**
   * @return the layers used to display the inspection region rectangle
   * @author Wei Chin
   */
  List<Integer> getInspectionRegionRectangleLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(new Integer(_topInspectionRegionRectangleLayer));
    layers.add(new Integer(_bottomInspectionRegionRectangleLayer));

    return layers;
  }

  /**
   * @return the layers used to display the inspection region pad bounds
   * @author Wei Chin
   */
  List<Integer> getInspectionRegionPadBoundsLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(new Integer(_topInspectionRegionPadBoundsLayer));
    layers.add(new Integer(_bottomInspectionRegionPadBoundsLayer));

    return layers;
  }

  /**
   * @return the layers used to display the inspection focus regions
   * @author Wei Chin
   */
  List<Integer> getInspectionFocusRegionLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(new Integer(_topInspectionFocusRegionLayer));
    layers.add(new Integer(_bottomInspectionFocusRegionLayer));

    return layers;
  }

  /**
   * @return the layers used to display the alignment region rectangles
   * @author Wei Chin
   */
  List<Integer> getAlignmentRegionRectangleLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(new Integer(_topAlignmentRegionRectangleLayer));
    layers.add(new Integer(_bottomAlignmentRegionRectangleLayer));

    layers.add(new Integer(_dragOpticalRegionLayer));
    layers.add(new Integer(_highlightOpticalRegionLayer));
    layers.add(new Integer(_opticalRegionSelectedLayer));
     
    return layers;
  }

  /**
   * @return the layers used to display the alignment region pad bounds
   * @author Wei Chin
   */
  List<Integer> getAlignmentRegionPadBoundsLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(new Integer(_topAlignmentRegionPadBoundsLayer));
    layers.add(new Integer(_bottomAlignmentRegionPadBoundsLayer));

    return layers;
  }

  /**
   * @return the layers used to display the alignment focus regions
   * @author Wei Chin
   */
  List<Integer> getAlignmentFocusRegionLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(new Integer(_topAlignmentFocusRegionLayer));
    layers.add(new Integer(_bottomAlignmentFocusRegionLayer));

    return layers;
  }

  /**
   * @return the layers used to display the verification region rectangles
   * @author Wei Chin
   */
  List<Integer> getVerificationRegionRectangleLayers()
  {
    List<Integer> layers = new LinkedList<Integer>();

    layers.add(_topVerificationRegionRectangleLayer);
    layers.add(_bottomVerificationRegionRectangleLayer);

    return layers;
  }

  /**
   * @return the layers used to display the verification focus regions
   * @author Wei Chin
   */
  List<Integer> getVerificationFocusRegionLayers()
  {
    List<Integer> layers = new LinkedList<Integer>();

    layers.add(_topVerificationFocusRegionLayer);
    layers.add(_bottomVerificationFocusRegionLayer);

    return layers;
  }

  /**
   * @author Wei Chin
   * @Edited By Kee Chin Seong
   */
  public void showTopSide()
  {
    // board name and origin renderers may or may not be visible.  Preserve that state here
    boolean boardNamesVisible = false;
    if (_graphicsEngine.isLayerVisible(_boardNameLayer))
      boardNamesVisible = true;
    
    boolean boardOriginsVisible = false;
    if (_graphicsEngine.isLayerVisible(_originLayer))
      boardOriginsVisible = true;

    _graphicsEngine.setVisible(getTopLayers(), true);
    _graphicsEngine.setVisible(getBottomLayers(), false);

    _graphicsEngine.setVisible(_boardNameLayer, boardNamesVisible);
    _graphicsEngine.setVisible(_originLayer, boardOriginsVisible);
    
    _showTopPadLayer = true;
    _showTopComponentLayer = true;
    _showTopComponentRefDesLayer = true;
    _showTopPanelLayer = true;
    
    //Ngie Xing
    _showBottomPadLayer = false;
    _showBottomComponentLayer = false;
    _showBottomComponentRefDesLayer = false;
    _showBottomPanelLayer = false;
    
    //@author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
    if(_showNoLoadComponent)
    {
       _graphicsEngine.setVisible(_topNoLoadedComponentLayer, true);
       _graphicsEngine.setVisible(_bottomNoLoadedComponentLayer, false);
       _graphicsEngine.setVisible(_topNoLoadPadLayer, true);
       _graphicsEngine.setVisible(_bottomNoLoadPadLayer, false);
    }
  }

  /**
   * @author Wei Chin
   * @Edited by Kee Chin Seong
   */
  public void showBottomSide()
  {
    // board name and origin renderers may or may not be visible.  Preserve that state here
    boolean boardNamesVisible = false;
    if (_graphicsEngine.isLayerVisible(_boardNameLayer))
      boardNamesVisible = true;

    boolean boardOriginsVisible = false;
    if (_graphicsEngine.isLayerVisible(_originLayer))
      boardOriginsVisible = true;

    _graphicsEngine.setVisible(getTopLayers(), false);
    _graphicsEngine.setVisible(getBottomLayers(), true);

    _graphicsEngine.setVisible(_boardNameLayer, boardNamesVisible);
    _graphicsEngine.setVisible(_originLayer, boardOriginsVisible);
          
    _showBottomPadLayer = true;
    _showBottomComponentLayer = true;
    _showBottomComponentRefDesLayer = true;
    _showBottomPanelLayer = true;
    
    //Ngie Xing
    _showTopPadLayer = false;
    _showTopComponentLayer = false;
    _showTopComponentRefDesLayer = false;
    _showTopPanelLayer = false;
    
    //@author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
    if(_showNoLoadComponent)
    {
        _graphicsEngine.setVisible(_topNoLoadPadLayer, false);
        _graphicsEngine.setVisible(_topNoLoadedComponentLayer, false);
        _graphicsEngine.setVisible(_bottomNoLoadPadLayer, true);
        _graphicsEngine.setVisible(_bottomNoLoadedComponentLayer, true);
    }
  }

  /**
   * @author Wei Chin
   */
  public void showBothSides()
  {
    // board name and origin renderers may or may not be visible.  Preserve that state here
    boolean boardNamesVisible = false;
    if (_graphicsEngine.isLayerVisible(_boardNameLayer))
      boardNamesVisible = true;
    
    boolean boardOriginsVisible = false;
    if (_graphicsEngine.isLayerVisible(_originLayer))
      boardOriginsVisible = true;

    _graphicsEngine.setVisible(getTopLayers(), true);
    _graphicsEngine.setVisible(getBottomLayers(), true);

    _graphicsEngine.setVisible(_boardNameLayer, boardNamesVisible);
    _graphicsEngine.setVisible(_originLayer, boardOriginsVisible);
    
    //Ngie Xing
    _showTopComponentLayer = true;
    _showTopPadLayer = true;
    _showTopComponentRefDesLayer = true;
    _showTopPanelLayer = true;

    _showBottomComponentLayer = true;
    _showBottomPadLayer = true;
    _showBottomComponentRefDesLayer = true;
    _showBottomPanelLayer = true;
    
    //@author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
    if(_showNoLoadComponent)
    {
      _graphicsEngine.setVisible(_topNoLoadPadLayer, true);
      _graphicsEngine.setVisible(_bottomNoLoadPadLayer, true);
      _graphicsEngine.setVisible(_topNoLoadedComponentLayer, true);
      _graphicsEngine.setVisible(_bottomNoLoadedComponentLayer, true);
    }
  }


  /**
   * Return true if any top layer is visisble, else false
   * @author Wei Chin
   */
  public boolean isTopSideVisible()
  {
    List<Integer> topLayers = getTopLayers();
    for (Integer layerNum : topLayers)
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true;
    }
    return false;
  }

  /**
   * @author Wei Chin
   */
  public boolean isBottomSideVisible()
  {
    for (Integer layerNum: getBottomLayers())
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true;
    }
    return false;
  }

  /**
   * Set the top layers to be visible by default
   * @author Wei Chin
   */
  private void setVisibility()
  {
    Assert.expect(_graphicsEngine != null);
    _graphicsEngine.setVisible(getBottomLayers(), false);
    _graphicsEngine.setVisible(getTopLayers(), true);
    _graphicsEngine.setVisible(_alignmentRegionLayer, false);
    _graphicsEngine.setVisible(getBoardOriginLayers(), false);
    _graphicsEngine.setVisible(getInspectionRegionRectangleLayers(), false);
    _graphicsEngine.setVisible(getInspectionRegionPadBoundsLayers(), false);
    _graphicsEngine.setVisible(getInspectionFocusRegionLayers(), false);
    _graphicsEngine.setVisible(getAlignmentRegionRectangleLayers(), false);
    _graphicsEngine.setVisible(getAlignmentRegionPadBoundsLayers(), false);
    _graphicsEngine.setVisible(getAlignmentFocusRegionLayers(), false);
    _graphicsEngine.setVisible(getVerificationRegionRectangleLayers(), false);
    _graphicsEngine.setVisible(getVerificationFocusRegionLayers(), false);
    _graphicsEngine.setVisible(_verificationIrpBoundariesLayer, false);
    _graphicsEngine.setVisible(_inspectionIrpBoundariesLayer, false);
  }

  /**
   * Set up the proper colors for the layers
   * @author Wei Chin
   */
  private void setColors()
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.setForeground(getPadLayers(), LayerColorEnum.SURFACEMOUNT_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(getReferenceDesignatorLayers(), LayerColorEnum.REFERENCE_DESIGNATOR_COLOR.getColor());

    _graphicsEngine.setForeground(getTopComponentLayer(), LayerColorEnum.TOP_COMPONENT_COLOR.getColor());
    _graphicsEngine.setForeground(getBottomComponentLayer(), LayerColorEnum.BOTTOM_COMPONENT_COLOR.getColor());

    _graphicsEngine.setForeground(getBoardLayer(), LayerColorEnum.BOARD_COLOR.getColor());
    _graphicsEngine.setForeground(getFiducialLayers(), LayerColorEnum.FIDUCIAL_COLOR.getColor());

    _graphicsEngine.setForeground(getBoardOriginLayers(), LayerColorEnum.BOARD_ORIGIN_COLOR.getColor());

    _graphicsEngine.setForeground(getInspectionRegionRectangleLayers(), LayerColorEnum.RECONSTRUCTION_REGION_RECTANGLE_COLOR.getColor());
    _graphicsEngine.setForeground(getVerificationRegionRectangleLayers(), LayerColorEnum.RECONSTRUCTION_REGION_RECTANGLE_COLOR.getColor());
    _graphicsEngine.setForeground(getAlignmentRegionRectangleLayers(), LayerColorEnum.RECONSTRUCTION_REGION_RECTANGLE_COLOR.getColor());

    _graphicsEngine.setForeground(getInspectionRegionPadBoundsLayers(), LayerColorEnum.REGION_PAD_BOUNDS_COLOR.getColor());
    _graphicsEngine.setForeground(getAlignmentRegionPadBoundsLayers(), LayerColorEnum.REGION_PAD_BOUNDS_COLOR.getColor());

    _graphicsEngine.setForeground(getInspectionFocusRegionLayers(), LayerColorEnum.FOCUS_REGION_COLOR.getColor());
    _graphicsEngine.setForeground(getAlignmentFocusRegionLayers(), LayerColorEnum.FOCUS_REGION_COLOR.getColor());
    _graphicsEngine.setForeground(getVerificationFocusRegionLayers(), LayerColorEnum.FOCUS_REGION_COLOR.getColor());

    _graphicsEngine.setForeground(_inspectionIrpBoundariesLayer, LayerColorEnum.IRP_BOUNDARY_COLOR.getColor());
    _graphicsEngine.setForeground(_verificationIrpBoundariesLayer, LayerColorEnum.IRP_BOUNDARY_COLOR.getColor());
  
    //@author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
    _graphicsEngine.setForeground(_topNoLoadedComponentLayer, LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(_bottomNoLoadedComponentLayer, LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(_topNoLoadPadLayer, LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(_bottomNoLoadPadLayer, LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(_panelWidthRendererLayer, LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    
    _graphicsEngine.setForeground(_topNoLoadRefDesignatorLayer, LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(_bottomNoLoadRefDesignatorLayer, LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    
    _graphicsEngine.setForeground(_topNoLoadPadNameLayer, LayerColorEnum.PAD_NAME_COLOR.getColor());
    _graphicsEngine.setForeground(_bottomNoLoadPadNameLayer, LayerColorEnum.PAD_NAME_COLOR.getColor());
  }
  
    /**
  * @author KEe Chin Seong - you dont have to check the layernumber as the Normal UpdateRendere, because this is a "special"
  *                          Handle for no load PAD and PadName
  */
  public void updateNoLoadRenderers(GraphicsEngine graphicsEngine, int layerNumber)
  {
    if(_showNoLoadComponent == false)
      return;
    
    Assert.expect(graphicsEngine != null);
    Assert.expect(graphicsEngine == _graphicsEngine);
    
    Collection<com.axi.guiUtil.Renderer> padRenderers = null;
    if(isTopSideVisible())
       padRenderers = graphicsEngine.getRenderers(_topNoLoadPadLayer);
    else
       padRenderers = graphicsEngine.getRenderers(_bottomNoLoadPadLayer);
    
    // get the current list of component renderers
    Collection<com.axi.guiUtil.Renderer> componentRenderers = null;
    if (isTopSideVisible())
      componentRenderers = graphicsEngine.getRenderers(_topNoLoadedComponentLayer);
    else
      componentRenderers = graphicsEngine.getRenderers(_bottomNoLoadedComponentLayer);
    
    Set<ComponentRenderer> componentRenderersShowingPadsSet = new HashSet<ComponentRenderer>();
    
    if (graphicsEngine.isZoomLevelBelowCreationThreshold(layerNumber))
    {
      // remove pad name layer
      graphicsEngine.removeRenderers(_topNoLoadPadLayer);
      graphicsEngine.removeRenderers(_bottomNoLoadPadLayer);
    }
    else // this is a 'regular' renderer update call whenever the zoom level is above pad renderer creation threshold
    {
      // iterate over each component to see if it is visible or not
      for (com.axi.guiUtil.Renderer renderer : componentRenderers)
      {
        if(renderer instanceof ComponentRenderer)
        {
            ComponentRenderer componentRenderer = (ComponentRenderer)renderer;
            boolean isVisible = graphicsEngine.wouldRendererBeVisibleOnScreenIfLayerWereVisible(componentRenderer);
            if (isVisible)
            {
              // the component is visible, create its PadRenderers in memory if they do not already
              // exist and add them to the GraphicsEngine
              if (componentRenderersShowingPadsSet.contains(componentRenderer) == false)
              {
                componentRenderersShowingPadsSet.add(componentRenderer);
                Component component = componentRenderer.getComponent();
                boolean componentIsTopSide = component.getSideBoard().isTopSide();

                // first do top side pads for the component
                Collection<Pad> pads = component.getPads();
                java.util.List<PadRenderer> padRenderersList = new ArrayList<PadRenderer>();
                for (Pad pad : pads)
                {
                  PadRenderer padRenderer = new PadRenderer(pad, componentRenderer);
                  //PadNameRenderer padNameRenderer = new PadNameRenderer(pad, false, false);
                  padRenderersList.add(padRenderer);

                  if(isTopSideVisible())
                     graphicsEngine.addRenderer(_topNoLoadPadLayer, padRenderer);
                  else
                     graphicsEngine.addRenderer(_bottomNoLoadPadLayer, padRenderer); 
                  
                 /*if(isTopSideVisible())
                    graphicsEngine.addRenderer(_topNoLoadPadNameLayer, padNameRenderer);
                  else
                    graphicsEngine.addRenderer(_bottomNoLoadPadNameLayer, padNameRenderer);*/
                  
                  _noLoadPadToPadRendererMap.put(pad, padRenderer);

                  if (pad.isThroughHolePad())
                  {
                    // the pad is through hole, so its pads should show up on both sides
                    padRenderer = new PadRenderer(pad, componentRenderer);
                    padRenderersList.add(padRenderer);

                    if (componentIsTopSide)
                      graphicsEngine.addRenderer(_bottomNoLoadPadLayer, padRenderer);
                    else
                      graphicsEngine.addRenderer(_topNoLoadPadLayer, padRenderer);
                  }
                }

                noLoadComponentRendererToPadRenderersMapPut(componentRenderer, padRenderersList);
              }
            }
            else
            {
              if (componentRenderersShowingPadsSet.contains(componentRenderer))
              {
                componentRenderersShowingPadsSet.remove(componentRenderer);
                java.util.List<PadRenderer> padRenderersList = _noLoadComponentRendererToPadRenderersMap.get(componentRenderer);
                if (padRenderersList != null)
                {
                  noLoadComponentRendererToPadRenderersMapRemove(componentRenderer);

                  for (PadRenderer padRenderer : padRenderersList)
                  {
                    graphicsEngine.removeRenderer(padRenderer);
                    _noLoadPadToPadRendererMap.remove(padRenderer.getPad());
                  }
                }
              }
            }
          }
      }
    }
    graphicsEngine.validateData(_topNoLoadRefDesignatorLayer);
    graphicsEngine.validateData(_bottomNoLoadRefDesignatorLayer);
  }

    /*
   * @author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
   */
  private void updateNoLoadedComponentData(ComponentType componentType, ComponentTypeSettings componentSettings)
  {
    List<ComponentRenderer> compRenderers = new ArrayList<ComponentRenderer>();
    //List<PadNameRenderer> padNameRenderers = new ArrayList<PadNameRenderer>();
    List<ReferenceDesignatorRenderer> refDesRenderers = new ArrayList<ReferenceDesignatorRenderer>();
    List<PadRenderer> padRenderers = new ArrayList<PadRenderer>();
    
    if(componentSettings.isLoaded() == true)
    {
      compRenderers = _noLoadComponentTypeToComponentRenderersMap.get(componentType);
      if(compRenderers == null)
          return;
      for (ComponentRenderer compRend : compRenderers)
      {
        padRenderers = _noLoadComponentRendererToPadRenderersMap.get(compRend);
        //padNameRenderers = _noLoadComponentRendererToPadNameRendererMap.get(compRend);
        
        if (padRenderers != null) // could be null if pad creation threshold is not met (not zoomed in enough)
        {
          _graphicsEngine.removeRenderers(padRenderers);
          //_graphicsEngine.removeRenderers(padNameRenderers);
        }
      }
      _noLoadComponentTypeToComponentRenderersMap.remove(componentType);
      _graphicsEngine.removeRenderers(compRenderers);
      List<ReferenceDesignatorRenderer> compRefDesRenderers = _noLoadComponentTypeToReferenceDesignatorRenderersMap.get(componentType);

      if(compRefDesRenderers != null)
       _graphicsEngine.removeRenderers(compRefDesRenderers);
      
      _noLoadComponentTypeToReferenceDesignatorRenderersMap.remove(componentType);
      _noLoadComponentTypeToComponentRenderersMap.remove(compRenderers);
      _noLoadComponentRendererToPadRenderersMap.remove(compRenderers);
    }
    else
    {
       for (com.axi.v810.business.panelDesc.Component component : componentType.getComponents())
       {   
         if(component.getReferenceDesignator().equals(componentType.getReferenceDesignator()))
         {    
            ComponentRenderer compRenderer = new ComponentRenderer(this, component);
            ReferenceDesignatorRenderer referenceDesignatorRenderer = new ReferenceDesignatorRenderer(component);

            compRenderers.add(compRenderer);
            refDesRenderers.add(referenceDesignatorRenderer);
            
            _noLoadComponentTypeToComponentRenderersMap.put(componentType, compRenderers);
            _noLoadComponentTypeToReferenceDesignatorRenderersMap.put(componentType, refDesRenderers);

            SideBoard sideBoard = component.getSideBoard();

            List<Pad> pads = component.getPads();

            for (Pad pad : pads)
            {
              PadRenderer padRenderer = new PadRenderer(pad, compRenderer);
              //PadNameRenderer padNameRenderer = new PadNameRenderer(pad, false, false);

              padRenderers.add(padRenderer);
              //padNameRenderers.add(padNameRenderer);
              
              if (sideBoard.isTopSide())
              {
                _graphicsEngine.addRenderer(_topNoLoadPadLayer, padRenderer);
                //_graphicsEngine.addRenderer(_topNoLoadedComponentLayer, padNameRenderer);
              }
              else
              {
                _graphicsEngine.addRenderer(_bottomNoLoadPadLayer, padRenderer);  
                //_graphicsEngine.addRenderer(_bottomNoLoadedComponentLayer, padNameRenderer);
              }
              _noLoadComponentRendererToPadRenderersMap.put(compRenderer, padRenderers);
              //_noLoadComponentRendererToPadNameRendererMap.put(compRenderer, padNameRenderers);                      
            }

            if (sideBoard.isTopSide())
            {
              _graphicsEngine.addRenderer(_topNoLoadedComponentLayer, compRenderer);
              _graphicsEngine.addRenderer(_topNoLoadRefDesignatorLayer, referenceDesignatorRenderer);
              if(_showNoLoadComponent == true && _graphicsEngine.getCurrentZoomFactor() >= 2.0)
                  _graphicsEngine.setVisible(_topNoLoadRefDesignatorLayer, true);
              else
                  _graphicsEngine.setVisible(_topNoLoadRefDesignatorLayer, false);
            }
            else if (sideBoard.isBottomSide())
            {
              _graphicsEngine.addRenderer(_bottomNoLoadedComponentLayer, compRenderer);
              _graphicsEngine.addRenderer(_bottomNoLoadRefDesignatorLayer, referenceDesignatorRenderer);
              if(_showNoLoadComponent == true && _graphicsEngine.getCurrentZoomFactor() >= 2.0 )
                 _graphicsEngine.setVisible(_bottomNoLoadRefDesignatorLayer, true);
              else
                _graphicsEngine.setVisible(_bottomNoLoadRefDesignatorLayer, false);
            }
            else
              Assert.expect(false);
          }
       } 
     }
  }
  
  /**
   * Set up the threshold settings for each layer
   * @author Wei Chin
   * * @edited By Kee Chin Seong - Set the threshold for the visibility of the Refdes and PAdNAme layer
   */
  private void setThresholds()
  {
    Assert.expect(_graphicsEngine != null);

    // since there are a lot of pads set a threshold for when they get created
    // only show reference designators at a certain threshold 25400 nm = 1 mil
    _graphicsEngine.setCreationThreshold(getPadLayers(), 25400 * 20, this);
    _graphicsEngine.setVisibilityThreshold(getReferenceDesignatorLayers(), 25400 * 20);
    
    _graphicsEngine.setVisibilityThreshold(_topNoLoadRefDesignatorLayer, 25400 * 30);
    _graphicsEngine.setVisibilityThreshold(_bottomNoLoadRefDesignatorLayer, 25400 * 30);
    _graphicsEngine.setVisibilityThreshold(_topNoLoadPadNameLayer, 25400 * 40);
    _graphicsEngine.setVisibilityThreshold(_bottomNoLoadPadNameLayer, 25400 * 40);
  }


  /**
   * Set up which layers to use when figuring out how big the whole deal is
   * @author Wei Chin
   */
  private void setLayersToUseWhenFittingToScreen()
  {
    Collection<Integer> layers = new ArrayList<Integer>(1);
    layers.addAll(getBoardLayer());
    _graphicsEngine.setLayersToUseWhenFittingToScreen(layers);
  }

/**
   * This call will create PadRenderers for any Components that are currently visible
   * on the screen and add them to the GraphicsEngine.
   * @author Wei Chin
   * @Edited By Kee Chin Seong
   */
  public void updateRenderers(GraphicsEngine graphicsEngine, int layerNumber)
  {
    if(getTopPadLayer() == layerNumber && _showTopPadLayer == false)
      return;
    else 
      if(getBottomPadLayer() == layerNumber && _showBottomPadLayer == false)
        return;
    
    if(_graphicsEngine.getCurrentZoomFactor() >= 2.0 )
    {
      if(isTopSideVisible() && _showNoLoadComponent == true)
         _graphicsEngine.setVisible(_topNoLoadRefDesignatorLayer, true);
      else
         _graphicsEngine.setVisible(_bottomNoLoadRefDesignatorLayer, true);
       
    }
    else
    {
       _graphicsEngine.setVisible(_topNoLoadRefDesignatorLayer, false);
       _graphicsEngine.setVisible(_bottomNoLoadRefDesignatorLayer, false);  
    }
    
    Assert.expect(graphicsEngine != null);
    Assert.expect(graphicsEngine == _graphicsEngine);

    List<Integer> padLayers = getPadLayers();
    if (padLayers.contains(layerNumber))
    {
//      System.out.println("Updating Renderers, graphics engine contents:");
//      printRenderers();
      boolean topLayer = false;
      if (getTopPadLayer() == layerNumber)
        topLayer = true;

      Set<ComponentRenderer> componentRenderersShowingPadsSet = new HashSet<ComponentRenderer>();

      // figure out the current list of Components that already have PadRenderers
      Collection<com.axi.guiUtil.Renderer> padRenderers = null;
      if (topLayer)
        padRenderers = graphicsEngine.getRenderers(getTopPadLayer());
      else
        padRenderers = graphicsEngine.getRenderers(getBottomPadLayer());

      for (com.axi.guiUtil.Renderer padRenderer : padRenderers)
      {
        ComponentRenderer componentRenderer = ((PadRenderer)padRenderer).getComponentRenderer();
        componentRenderersShowingPadsSet.add(componentRenderer);
      }

      // get the current list of component renderers
      Collection<com.axi.guiUtil.Renderer> componentRenderers = null;
      if (topLayer)
        componentRenderers = graphicsEngine.getRenderers(getTopComponentLayer());
      else
        componentRenderers = graphicsEngine.getRenderers(getBottomComponentLayer());

      if (graphicsEngine.isZoomLevelBelowCreationThreshold(layerNumber))
      {
        // if we crossed the pad creation threshold, then remove all entries from _componentRendererToPadRenderersMap
        // for all components who currently have pads on the passed in layer. The graphics engine whould have taken care
        // of removing the actual pad renderers from its layer
        for (ComponentRenderer componentRendererWithPads : componentRenderersShowingPadsSet)
        {
          componentRendererToPadRenderersMapRemove(componentRendererWithPads);
        }
      }
      else // this is a 'regular' renderer update call whenever the zoom level is above pad renderer creation threshold
      {
        // iterate over each component to see if it is visible or not
        for (com.axi.guiUtil.Renderer renderer : componentRenderers)
        {
          if(renderer instanceof ComponentRenderer)
          {
            ComponentRenderer componentRenderer = (ComponentRenderer)renderer;
            //System.out.println("UR:Processing component renderer for: " + componentRenderer.getComponent().getReferenceDesignator());
            boolean isVisible = graphicsEngine.wouldRendererBeVisibleOnScreenIfLayerWereVisible(componentRenderer);
            if (isVisible)
            {
              //System.out.println("UR:The Component is visible");
              // the component is visible, create its PadRenderers in memory if they do not already
              // exist and add them to the GraphicsEngine
              if (componentRenderersShowingPadsSet.contains(componentRenderer) == false)
              {
                //System.out.println("UR:The Component is NOT part of set of compRenderers showing pads - create its pads");
                componentRenderersShowingPadsSet.add(componentRenderer);
                Component component = componentRenderer.getComponent();
                boolean componentIsTopSide = component.getSideBoard().isTopSide();

                // first do top side pads for the component
                Collection<Pad> pads = component.getPads();
                java.util.List<PadRenderer> padRenderersList = new ArrayList<PadRenderer>();
                for (Pad pad : pads)
                {
                  // we want to make sure we put the pad on proper board's layer.  For multi-board panels,
                  // each board has it's own top and bottom pad layer.  Without this check, we'd put all the pads
                  // for each board on one layer.
                  if (layerNumber == _topPadLayer || layerNumber == _bottomPadLayer)
                  {
                    PadRenderer padRenderer = new PadRenderer(pad, componentRenderer);
                    padRenderersList.add(padRenderer);

                    graphicsEngine.addRenderer(layerNumber, padRenderer);
                    _padToPadRendererMap.put(pad, padRenderer);

                    if (pad.isThroughHolePad())
                    {
                      // the pad is through hole, so its pads should show up on both sides
                      padRenderer = new PadRenderer(pad, componentRenderer);
                      padRenderersList.add(padRenderer);

                      // put the pad on the correct layer - opposite of what layer the
                      // component is on since it is a bottom side Pad
                      if (componentIsTopSide)
                        graphicsEngine.addRenderer(getBottomPadLayer(), padRenderer);
                      else
                        graphicsEngine.addRenderer(getTopPadLayer(), padRenderer);
                    }
                  }
                }

                // System.out.println("wpd padRenderersList: " + padRenderersList);
                componentRendererToPadRenderersMapPut(componentRenderer, padRenderersList);
              }
            }
            else
            {
              //System.out.println("UR:The Component is NOT visible");
              // component is not visible.  If its PadRenderers are currently in the GraphicsEngine
              // remove them.
              if (componentRenderersShowingPadsSet.contains(componentRenderer))
              {
                //System.out.println("UR:The Component is part of set of compRenderers showing pads - remove pads if they exist");
                componentRenderersShowingPadsSet.remove(componentRenderer);
                java.util.List<PadRenderer> padRenderersList = _componentRendererToPadRenderersMap.get(componentRenderer);
                if (padRenderersList != null)
                {
                  //System.out.println("UR:The Component has pads - remove them");
                  componentRendererToPadRenderersMapRemove(componentRenderer);

                  for (PadRenderer padRenderer : padRenderersList)
                  {
                    graphicsEngine.removeRenderer(padRenderer);
                    _padToPadRendererMap.remove(padRenderer.getPad());
                  }
                }
              }
            }
          }
        }
      }
    }
    else
    {
      // the layer passed in is not valid
      Assert.expect(false, "Pad Layer " + layerNumber + " does not exist");
    }

    if(_showNoLoadComponent == true)
       updateNoLoadRenderers(graphicsEngine, layerNumber);
//    System.out.println("Finished Updating Renderers, graphics engine contents:");
//    printRenderers();

    highlightPads(_highlightPads);
  }
  
  /*
   *     @author Kee Chin Seong
   * *
   */
  private void updateComponentTypeLoadedData(ProjectChangeEvent event, final Object argument)
  {
    ProjectChangeEventEnum eventEnum = ((ProjectChangeEvent)argument).getProjectChangeEventEnum();
    ComponentTypeSettingsEventEnum compTypeSettingsEventEnum = (ComponentTypeSettingsEventEnum) eventEnum;
     
      if(compTypeSettingsEventEnum.equals(ComponentTypeSettingsEventEnum.LOADED))
      {
        ComponentTypeSettings componentSettings = (ComponentTypeSettings) event.getSource();
        updateComponentTypeLoadedData(componentSettings);
      }
      else if (compTypeSettingsEventEnum.equals(ComponentTypeSettingsEventEnum.LOADED_LIST))
      {
        List<ComponentTypeSettings> componentSettingsList = (List<ComponentTypeSettings>) event.getSource();
        for (ComponentTypeSettings componentSettings : componentSettingsList)
        {
          updateComponentTypeLoadedData(componentSettings);
        }
      }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void updateComponentTypeLoadedData(ComponentTypeSettings componentSettings)
  {
    Assert.expect(componentSettings != null);
    ComponentType componentType = componentSettings.getComponentType();

    if (componentSettings.isLoaded() == false)
    {
      List<ComponentRenderer> compRenderers = _componentTypeToComponentRenderersMap.get(componentType);
      for (ComponentRenderer compRend : compRenderers)
      {
        List<PadRenderer> padRenderers = _componentRendererToPadRenderersMap.get(compRend);
        if (padRenderers != null) // could be null if pad creation threshold is not met (not zoomed in enough)
        {
          _graphicsEngine.removeRenderers(padRenderers);
          //componentRendererToPadRenderersMapRemove(compRend);
        }
      }
      _componentTypeToComponentRenderersMap.remove(componentType);
      _graphicsEngine.removeRenderers(compRenderers);
      List<ReferenceDesignatorRenderer> compRefDesRenderers = _componentTypeToReferenceDesignatorRenderersMap.get(componentType);
      _graphicsEngine.removeRenderers(compRefDesRenderers);
      _componentTypeToReferenceDesignatorRenderersMap.remove(componentType);
      _componentRendererToPadRenderersMap.remove(compRenderers);
    }
    else
    {
      List<ComponentRenderer> compRenderers = new ArrayList<ComponentRenderer>();
      for (com.axi.v810.business.panelDesc.Component component : componentType.getComponents())
      {
        if (component.getReferenceDesignator().equals(componentType.getReferenceDesignator()))
        {
          List<ReferenceDesignatorRenderer> compRefDesRenderers = new ArrayList<ReferenceDesignatorRenderer>();
          List<PadRenderer> padRenderers = new ArrayList<PadRenderer>();

          ComponentRenderer compRenderer = new ComponentRenderer(this, component);
          ReferenceDesignatorRenderer referenceDesignatorRenderer = new ReferenceDesignatorRenderer(component);
          compRenderers.add(compRenderer);
          compRefDesRenderers.add(referenceDesignatorRenderer);
          SideBoard sideBoard = component.getSideBoard();
                 // do not create pad renderers unless the pad layers are currently visible on screen

          if (arePadsVisible(sideBoard.isTopSide()))
          {
            List<Pad> pads = component.getPads();

            for (Pad pad : pads)
            {
              PadRenderer padRenderer = new PadRenderer(pad, compRenderer);
              padRenderers.add(padRenderer);
              _graphicsEngine.addRenderer(_topPadLayer, padRenderer);
              //_graphicsEngine.add(padRenderer);
            }
            //componentRendererToPadRenderersMapPut(compRenderer, padRenderers);
          }
          Integer layerInt = null;
          if (sideBoard.isTopSide())
          {
            _graphicsEngine.addRenderer(_topComponentLayer, compRenderer);
            _graphicsEngine.addRenderer(_topRefDesLayer, referenceDesignatorRenderer);
            //_graphicsEngine.validateData(getTopPadLayers());
          }
          else if (sideBoard.isBottomSide())
          {
            _graphicsEngine.addRenderer(_bottomComponentLayer, compRenderer);
            _graphicsEngine.addRenderer(_bottomRefDesLayer, referenceDesignatorRenderer);
            //_graphicsEngine.validateData(getBottomPadLayers());
          }
          else
          {
            Assert.expect(false);
          }

          _componentTypeToComponentRenderersMap.put(componentType, compRenderers);
          _componentTypeToReferenceDesignatorRenderersMap.put(componentType, compRefDesRenderers);
          _componentRendererToPadRenderersMap.put(compRenderer, padRenderers);

        }
               //_graphicsEngine.validateData(getComponentLayers());
        //_graphicsEngine.validateData(getReferenceDesignatorLayers());     
        _graphicsEngine.validateData(getPadLayers());

      }

    }
    updateNoLoadedComponentData(componentType, componentSettings);
  }


  /**
   * Checks to see if the pad layers (for either top side or bottom side) are currently visible.
   * This depends on the creation threshold and whether the user has selected to view top or bottom side.
   * @author Andy Mechtenberg
   */
  boolean arePadsVisible(boolean topSide)
  {
    int padLayer;

    if (topSide)
     padLayer = getTopPadLayer();
    else
     padLayer = getBottomPadLayer();

    boolean padsVisible = _graphicsEngine.isLayerVisible(padLayer);

    if (padsVisible)
      return true;
    
    return false;
  }

  /**
   * This class is an observer of the datastore layer. Any changes made
   * to the datastore will trigger an update call. We will look at the
   * change event and determine if anything needs to be updated.
   * @author George A. David
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if(observable instanceof ProjectObservable)
        {
          if (argument instanceof ProjectChangeEvent)
          {
            ProjectChangeEvent event = (ProjectChangeEvent)argument;
            ProjectChangeEventEnum eventEnum = ((ProjectChangeEvent)argument).getProjectChangeEventEnum();

            if (eventEnum instanceof AlignmentGroupEventEnum)
            {
              updateAlignmentRegionData();
            }
            else if (eventEnum instanceof BoardEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof BoardTypeEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof ComponentEventEnum)
            {
//              updateComponentData(event);
            }
            else if (eventEnum instanceof ComponentTypeEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof CompPackageEventEnum)
            {
              // do nothing -- panel graphics show land patterns, not packages
            }
            else if (eventEnum instanceof FiducialEventEnum)
            {
               // do nothing
            }
            else if (eventEnum instanceof FiducialTypeEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof LandPatternPadEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof LandPatternEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof PackagePinEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof PadEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof PanelEventEnum)
            {
//               updatePanelData(event);
            }
            else if (eventEnum instanceof PanelSettingsEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof ProjectEventEnum)
            {
              // do nothing
              updateProjectData(event);
            }
            else if (eventEnum instanceof SideBoardEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof SideBoardTypeEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof SideBoardTypeSettingsEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof ThroughHoleLandPatternPadEventEnum)
            {
              // do nothing
            }
            // commmented out.  This was used to implement graphical undo, but we decided
            // not to support it.  It was causing a deadlock with other updates in some situations
            // _guiObservable.stateChanged(this, GuiUpdateEventEnum.CAD_GRAPHICS);
          }
        }
        else if (observable instanceof SelectedRendererObservable)
        {
          handleSelectedRenderers(argument);
        }
        else if (observable instanceof GuiObservable)
        {
          handleGuiEvent((GuiEvent)argument);
        }
        else if (observable instanceof DragOpticalRegionObservable)
        {
          handleDragOpticalRegion();
        }
        else if (observable instanceof ShowNoLoadComponentObservable)
        {
          if (argument instanceof Boolean)
          {
            Boolean setNoLoadComponentVisible = (Boolean)argument;
            if (setNoLoadComponentVisible.booleanValue())
            {
              _showNoLoadComponent = true;
              if(isTopSideVisible())
              {
                _graphicsEngine.setVisible(_topNoLoadedComponentLayer, true);
                _graphicsEngine.setVisible(_topNoLoadPadLayer, true);
              }
              
              if(isBottomSideVisible())
              {
                _graphicsEngine.setVisible(_bottomNoLoadedComponentLayer, true);
                _graphicsEngine.setVisible(_bottomNoLoadPadLayer, true);
              }
              
            } 
            else
            {
              _showNoLoadComponent = false;
              _graphicsEngine.setVisible(_topNoLoadedComponentLayer, false);
              _graphicsEngine.setVisible(_bottomNoLoadedComponentLayer, false);
              _graphicsEngine.setVisible(_topNoLoadPadLayer, false);
              _graphicsEngine.setVisible(_bottomNoLoadPadLayer, false);
              //always force the renderer close 
              _graphicsEngine.setVisible(_bottomNoLoadRefDesignatorLayer, false);
              _graphicsEngine.setVisible(_bottomNoLoadPadNameLayer, false);
              _graphicsEngine.setVisible(_topNoLoadRefDesignatorLayer, false);
              _graphicsEngine.setVisible(_topNoLoadPadNameLayer, false);
            }
          }
        }
        else
        {
          Assert.expect(false, "BoardGraphicsEngineSetup: No update code for observable: " + observable.getClass().getName());
        }
      }
    });
  }

/**
   * This method will take a collection of Renderers that have been selected in the GraphicsEngine and
   * determine which to highlight in the graphics.
   * @author Andy Mechtenberg
   */
  private void handleSelectedRenderers(final Object argument)
  {
    // notify all the PanelGraphicsEngineSetup observers that a mouse click through the Board Graphics generated
    // a selectedRenderers change.
    notifyObservers(argument);

    _graphicsEngine.clearCrossHairs();
    _graphicsEngine.clearSelectedRenderers();
    _graphicsEngine.clearAllFlashingRenderersAndLayers();

    // there are two modes of selection:
    // 1.  The user clicked on a cad feature
    // When using this method, a complex algorithm to figure out what to select is used (see below)
    // 2.  The user used the group select mode to draw a rectangle around a bunch of stuff
    // When using the second method, EVERYTHING is selected

    // Let's handle the 2nd method first (because it's easier)
    // if this wasn't a group select, it still could be a CTRL-Left Click which ADDS to the selections.  Treat this as a group select
    if (_graphicsEngine.wasSelectionPartOfAGroupSelection())
    {
      /** Warning "unchecked cast" approved for cast from Object types.*/
      for (com.axi.guiUtil.Renderer renderer : (Collection<com.axi.guiUtil.Renderer>)argument)
      {
        _graphicsEngine.setSelectedRenderer(renderer);
      }
      _drawCadPanel.setDescription(" "); // clear out the descriptive text because lots of things were selected
    }
    else
    {
      int i = 0;
      com.axi.guiUtil.Renderer rendererToUse = null;
      // here, I want to hightlight ONE renderer, even though I may have a lot in the Collection.  The
      // collection is not order dependent, so I'll need to iterate through and find the one I want to use.
      // The ONE will be, in order:
      //  1.  A Fiducial renderer.  If more than one fiducial, choose one from the TOP side
      //  2.  A Pad renderer.  If more than one pad renderer, choose one from the TOP side
      //  3.  A component renderer.  If more than one, choose one from the TOP side
      //  4.  A Board Renderer
      //  5.  A panel renderer

      /** Warning "unchecked cast" approved for cast from Object types.*/
      for (com.axi.guiUtil.Renderer renderer : (Collection<com.axi.guiUtil.Renderer>)argument)
      {
        ++i;
        // if it's a fiducial, use it
        if (renderer instanceof FiducialRenderer)
        {
          FiducialRenderer fiducialRenderer = (FiducialRenderer)renderer;
          if (rendererToUse == null) // haven't found anything yet
            rendererToUse = renderer;
          else // we have a fiducial renderer to consider, so compare to rendererToUse
          {
            if (rendererToUse instanceof FiducialRenderer)
            {
              if (fiducialRenderer.getFiducial().isTopSide())
                rendererToUse = renderer;
            }
            else // the previously chosen one is not a fiducial, so make it this fiducial
              rendererToUse = renderer;
          }
        }
        // if it's a pad, use it
        else if (renderer instanceof PadRenderer)
        {
          PadRenderer padRenderer = (PadRenderer)renderer;
          if (rendererToUse == null) // haven't found anything yet
            rendererToUse = renderer;
          else // we have a pad renderer to consider, so compare to rendererToUse
          {
            if (rendererToUse instanceof PadRenderer)
            {
              if (padRenderer.getPad().isTopSide())
                rendererToUse = renderer;
            }
            else // the previously chosen one is not a pad, so make it this pad
              rendererToUse = renderer;
          }
        }
        else if (renderer instanceof ComponentRenderer)
        {
          ComponentRenderer compRenderer = (ComponentRenderer)renderer;
          if (rendererToUse == null) // haven't found any yet, so use this one
            rendererToUse = renderer;
          else
          {
            if ((rendererToUse instanceof PadRenderer) == false) // already using a pad, so don't change to component
            {
              // our renderer is not a pad, so we'll use this current one
              if (compRenderer.getComponent().isTopSide())
                rendererToUse = renderer;
              else if (rendererToUse instanceof ComponentRenderer)
              {
                // our state here:  rendererToUse is a component and the one under consideration is a component
                // so only switch if rendererToUse is on the bottom and the renderer is on the top
                ComponentRenderer compRendererToUse = (ComponentRenderer)rendererToUse;
                if ((compRendererToUse.getComponent().isTopSide() == false) &&
                    (compRenderer.getComponent().isTopSide()))
                  rendererToUse = renderer;
              }
              else
                rendererToUse = renderer;
            }
          }
        }
        else if ((renderer instanceof InspectionRegionRenderer) ||
                 (renderer instanceof VerificationRegionRenderer)||
                 (renderer instanceof RectangleRenderer))
        {
          if (rendererToUse == null) // haven't found any yet, so use this one
            rendererToUse = renderer;
          else
          {
            if (((rendererToUse instanceof PadRenderer) == false) &&
                ((rendererToUse instanceof ComponentRenderer) == false))// already using a pad, so don't change to component
              rendererToUse = renderer;
          }
        }
        else if (renderer instanceof BoardRenderer)
        {
          // only use if nothing or set to panel renderer
          if (rendererToUse == null)
            rendererToUse = renderer;
          else
          {
            if (rendererToUse instanceof PanelRenderer)
              rendererToUse = renderer;
          }
        }
        else // last resort
        {
          if (rendererToUse == null)
            rendererToUse = renderer;
        }
        // when i==1, it's the first on list.  Use that unless it's already been set by the pad
        if ((i == 1) && (rendererToUse == null))
          rendererToUse = renderer;
      }
      if (rendererToUse != null) // could be null if they clicked outside the boundary of any renderer
      {
        // now that we have the renderer of interest, we need to highlight it!
        _graphicsEngine.setSelectedRenderer(rendererToUse);
        setStatusBarDescription(rendererToUse);
      }
    }
  }

/**
   * Updates the status bar description based on the renderer passed in
   * @author Andy Mechtenberg
   */
  private void setStatusBarDescription(com.axi.guiUtil.Renderer renderer)
  {
    Assert.expect(renderer != null);
    String description = " ";
    // handle all the normal types of selections: panel board component pad
    if (renderer instanceof BoardRenderer)
    {
      BoardRenderer boardRenderer = (BoardRenderer)renderer;

      String projName = boardRenderer.getBoard().getPanel().getProject().getName();
      LocalizedString panelString = new LocalizedString("GUI_PANEL_SUMMARY_KEY", new Object[]{projName});
      description = StringLocalizer.keyToString(panelString);
      LocalizedString boardString = new LocalizedString("GUI_BOARD_SUMMARY_KEY", new Object[]{renderer.toString()});
      description = StringLocalizer.keyToString(panelString) + " " + StringLocalizer.keyToString(boardString);

    }
    else if (renderer instanceof ComponentRenderer)
    {
      Component component = ((ComponentRenderer)renderer).getComponent();
      description = getComponentDescriptionString(component);
    }
    else if (renderer instanceof PadRenderer)
    {
      Pad pad = ((PadRenderer)renderer).getPad();
      description = getPadDescriptionString(pad);
    }
    else if (renderer instanceof FiducialRenderer)
    {
      Fiducial fiducial = ((FiducialRenderer)renderer).getFiducial();
      LocalizedString fiducialString = null;
     if (fiducial.isOnBoard())
       fiducialString = new LocalizedString("GUI_BOARD_FIDUCIAL_SUMMARY_KEY", new Object[]{fiducial.getName()});
     else if (fiducial.isOnPanel())
       fiducialString = new LocalizedString("GUI_PANEL_FIDUCIAL_SUMMARY_KEY", new Object[]{fiducial.getName()});
     else
       Assert.expect(false, "Found fiducial that is neither on the board nor on the panel");

     description = StringLocalizer.keyToString(fiducialString);
    }
    else// if (renderer instanceof InspectionRegionRenderer) || (renderer instanceof VerificationRegionRenderer))
    {
      description = renderer.toString();
    }
    _drawCadPanel.setDescription(description);
  }

/**
   * @author Andy Mechtenberg
   */
  private String getComponentDescriptionString(Component component)
  {
    Assert.expect(component != null);
    // component is quite complex.  We want to display the package, land pattern and possibly joint type
    // and subtype
    String jointType = TestDev.getComponentDisplayJointType(component.getComponentType());

    String subType = TestDev.getComponentDisplaySubtype(component.getComponentType());

    LocalizedString localizedDescription = new LocalizedString("GUI_COMPONENT_SUMMARY_KEY",
        new Object[]
        {component.getReferenceDesignator(),
        component.getCompPackage().toString(),
        component.getLandPattern().getName(),
        jointType,
        subType});
    return StringLocalizer.keyToString(localizedDescription);
  }

  /**
   * @author Andy Mechtenberg
   */
  private String getPadDescriptionString(Pad pad)
  {
    Assert.expect(pad != null);
    Component component = pad.getComponent();
    PadType padType = pad.getPadType();
    // pad, like component, is quite complex.  We want to display the package, land pattern and possibly joint type
    // and subtype.  But pad joint type and subtype cannot be "mixed", so that's a little simpler
    String jointType = pad.getJointTypeEnum().getName();

    String subType = "";
    if (pad.isTestable() == false)
      subType = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_UNTESTABLE_KEY");
    else if (padType.getComponentType().isLoaded() == false)
      subType = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOLOAD_KEY");
    else if (padType.isInspected() == false)
      subType = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOTEST_KEY");
    else
      subType = padType.getSubtype().getShortName();

    LocalizedString padDescription = new LocalizedString("GUI_PIN_SUMMARY_KEY", new Object[]{pad.getName()});

    LocalizedString componentDescription = new LocalizedString("GUI_COMPONENT_SUMMARY_KEY",
        new Object[]
        {component.getReferenceDesignator(),
        component.getCompPackage().toString(),
        component.getLandPattern().getName(),
        jointType,
        subType});
    return StringLocalizer.keyToString(padDescription) + " " + StringLocalizer.keyToString(componentDescription);
  }
  
    /*
  * Author Kee Chin Seong
  */
  private java.util.List<ComponentRenderer> findNoLoadComponentRenderers(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    java.util.List<ComponentRenderer> componentRenderers = new ArrayList<ComponentRenderer>();

    java.util.List<Component> components = componentType.getComponents();
    for (Component component : components)
    {
      boolean onTop = component.isTopSide();

      ComponentRenderer componentRenderer = null;


      Collection<com.axi.guiUtil.Renderer> renderers = null;
      if (onTop)
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(_topNoLoadedComponentLayer));
      else
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(_bottomNoLoadedComponentLayer));

      Collection<ComponentRenderer> compRenderers = new ArrayList<ComponentRenderer>();
      for (com.axi.guiUtil.Renderer renderer : renderers)
      {
        if(renderer instanceof ComponentRenderer)
           compRenderers.add((ComponentRenderer)renderer);
      }

      for (ComponentRenderer compRenderer : compRenderers)
      {
        com.axi.v810.business.panelDesc.Component comp = compRenderer.getComponent();
        if (comp == component)
        {
          componentRenderer = compRenderer;
          break;
        }
      }
//      Assert.expect(componentRenderer != null);
      if(componentRenderer != null)
        componentRenderers.add(componentRenderer);
    }
    return componentRenderers;
  }

/**
   * @author Andy Mechtenberg
   */
  private void highlightComponentType(ComponentType compType)
  {
    java.util.List<ComponentRenderer> renderersToHighlight = null;
    if(compType.isLoaded() == false)
      renderersToHighlight = findNoLoadComponentRenderers(compType);
    else
      renderersToHighlight = findComponentRenderers(compType);
    
    if(renderersToHighlight.size() > 0)
    {
      // first, figure out which side(s) of the panel we need to make visible in order to get all the
      // the component renderers visible.  Because of flipped panels, both sides may need to be made visible
      boolean topComponentsVisible = isTopSideVisible();
      boolean bottomComponentsVisible = isBottomSideVisible();
      boolean componentOnTop = false;
      boolean componentOnBottom = false;

      for (ComponentRenderer componentRenderer : renderersToHighlight)
      {
        if (componentRenderer.getComponent().isTopSide())
          componentOnTop = true;
        else
          componentOnBottom = true;
      }
      if (componentOnTop && componentOnBottom) // flipped board case, must show both sides at once!
      {
        _controlToolBar.showBothSides();
      }
      else // the user has only one side to show, so if they've only got one side visible, respect that only make what needs to be visible
      {
        if (componentOnTop && (topComponentsVisible == false))
        {
          _controlToolBar.showTopSide();
        }
        if ((componentOnBottom) && (bottomComponentsVisible == false))
        {
          _controlToolBar.showBottomSide();
        }
      }
      // use flashing to highlight all but the first renderer -- the first one will be highlighted with crosshairs
      // if a single board was selected, select the renderer for that board
      _graphicsEngine.clearAllFlashingRenderersAndLayers();
      ComponentRenderer firstComponent = null;
      if (_selectedBoardName == null)
      {
        firstComponent = (ComponentRenderer)renderersToHighlight.remove(0);
      }
      else
      {
        int index = 0;
        for (ComponentRenderer componentRenderer : renderersToHighlight)
        {
          if (componentRenderer.getComponent().getBoard().getName().equals(_selectedBoardName))
          {
            firstComponent = (ComponentRenderer)renderersToHighlight.remove(index);
            break;
          }
          index++;
        }
      }
      if (firstComponent == null)
      {
        firstComponent = (ComponentRenderer)renderersToHighlight.remove(0);
      }
      if (renderersToHighlight.isEmpty() == false)
      {
        _graphicsEngine.setFlashRenderers(renderersToHighlight, java.awt.Color.YELLOW);
        
        // XCR1533 - The flashing cause memory leak in V810 
        _graphicsEngine.setFlashing(false);
      }
      // use crosshairs to highlight ONE of the components -- the first one in the list, which we just removed
      _graphicsEngine.selectWithCrossHairs(firstComponent);

      // move to center if zoom is not at 1:1
      if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0) == false)
      {
        // we're at some zoom level, let's center the graphics to show the new alignment region
        DoubleCoordinate coordinate = firstComponent.getCenterCoordinate();
        _graphicsEngine.center(coordinate.getX(), coordinate.getY(), 0, 0);
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void highlightFiducialType(FiducialType fidType)
  {
    java.util.List<FiducialRenderer> renderersToHighlight = getFiducialRenderers(fidType);
    // first, figure out which side(s) of the panel we need to make visible in order to get all the
    // the component renderers visible.  Because of flipped panels, both sides may need to be made visible
    boolean topFiducialsVisible = isTopSideVisible();
    boolean bottomFiducialsVisible = isBottomSideVisible();
    boolean fiducialOnTop = false;
    boolean fiducialOnBottom = false;

    for (FiducialRenderer fiducialRenderer : renderersToHighlight)
    {
      if (fiducialRenderer.getFiducial().isTopSide())
        fiducialOnTop = true;
      else
        fiducialOnBottom = true;
    }
    if (fiducialOnTop && fiducialOnBottom) // flipped board case, must show both sides at once!
    {
      _controlToolBar.showBothSides();
    }
    else // the user has only one side to show, so if they've only got one side visible, respect that only make what needs to be visible
    {
      if (fiducialOnTop && (topFiducialsVisible == false))
      {
        _controlToolBar.showTopSide();
      }
      if ((fiducialOnBottom) && (bottomFiducialsVisible == false))
      {
        _controlToolBar.showBottomSide();
      }
    }
    // use flashing to highlight all but the first renderer -- the first one will be highlighted with crosshairs
    // if a single board was selected, select the renderer for that board
    _graphicsEngine.clearAllFlashingRenderersAndLayers();
    FiducialRenderer firstFiducial = null;
    if (fidType.getFiducials().get(0).isOnBoard())
    {
      if (_selectedBoardName == null)
      {
        firstFiducial = (FiducialRenderer)renderersToHighlight.remove(0);
      }
      else
      {
        int index = 0;
        for (FiducialRenderer fiducialRenderer : renderersToHighlight)
        {
          if (fiducialRenderer.getFiducial().getSideBoard().getBoard().getName().equals(_selectedBoardName))
          {
            firstFiducial = (FiducialRenderer)renderersToHighlight.remove(index);
            break;
          }
          index++;
        }
      }
    }
    else
      firstFiducial = (FiducialRenderer)renderersToHighlight.remove(0);

    if (firstFiducial == null)
    {
      firstFiducial = (FiducialRenderer)renderersToHighlight.remove(0);
    }
    if (renderersToHighlight.isEmpty() == false)
    {
      _graphicsEngine.setFlashRenderers(renderersToHighlight, java.awt.Color.YELLOW);
      
      // XCR1533 - The flashing cause memory leak in V810 
      _graphicsEngine.setFlashing(false);
    }
    // use crosshairs to highlight ONE of the components -- the first one in the list, which we just removed
    _graphicsEngine.selectWithCrossHairs(firstFiducial);

    // move to center if zoom is not at 1:1
    if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0) == false)
    {
      // we're at some zoom level, let's center the graphics to show the new alignment region
      DoubleCoordinate coordinate = firstFiducial.getCenterCoordinate();
      _graphicsEngine.center(coordinate.getX(), coordinate.getY(), 0, 0);
    }
  }

  /**
   * @param components A collection of ComponentType objects
   * @author Andy Mechtenberg
   */
  private void highlightMultipleComponents(Collection<ComponentType> components)
  {
    Assert.expect(components != null);
    Assert.expect(components.size() > 1);

    for (ComponentType componentType : components)
    {
      for (ComponentRenderer componentRenderer : findComponentRenderers(componentType))
      {
        _graphicsEngine.setSelectedRenderer(componentRenderer);
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void highlightMultipleFiducials(Collection<FiducialType> fiducials)
  {
    Assert.expect(fiducials != null);
    Assert.expect(fiducials.size() > 1);

    for (FiducialType fiducialType : fiducials)
    {
      for (FiducialRenderer fiducialRenderer : getFiducialRenderers(fiducialType))
      {
        _graphicsEngine.setSelectedRenderer(fiducialRenderer);
      }
    }
  }

  /**
   * @return a List of ComponentRenderers that match the componentType passed in
   * @author Andy Mechtenberg
   */
  private java.util.List<ComponentRenderer> findComponentRenderers(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    java.util.List<ComponentRenderer> componentRenderers = new ArrayList<ComponentRenderer>();

    java.util.List<Component> components = componentType.getComponents();
    for (Component component : components)
    {
      boolean onTop = component.isTopSide();

      ComponentRenderer componentRenderer = null;


      Collection<com.axi.guiUtil.Renderer> renderers = null;
      if (onTop)
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopComponentLayer()));
      else
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomComponentLayer()));

      Collection<ComponentRenderer> compRenderers = new ArrayList<ComponentRenderer>();
      for (com.axi.guiUtil.Renderer renderer : renderers)
      {
        // XCR-3650 Filtering component is not working after delete component
        if (renderer instanceof ComponentRenderer)
          compRenderers.add((ComponentRenderer)renderer);
      }

      for (ComponentRenderer compRenderer : compRenderers)
      {
        com.axi.v810.business.panelDesc.Component comp = compRenderer.getComponent();
        if (comp == component)
        {
          componentRenderer = compRenderer;
          break;
        }
      }
//      Assert.expect(componentRenderer != null);
      if(componentRenderer != null)
        componentRenderers.add(componentRenderer);
    }
    return componentRenderers;
  }

  /**
   * @return a List of FiducialRenderers that match the fiducialType passed in
   * @author Laura Cormos
   */
  private java.util.List<FiducialRenderer> findFiducialRenderers(FiducialType fiducialType)
  {
    Assert.expect(fiducialType != null);
    java.util.List<FiducialRenderer> fiducialRenderers = new ArrayList<FiducialRenderer>();

    java.util.List<Fiducial> fiducials = fiducialType.getFiducials();
    for (Fiducial fiducial : fiducials)
    {
      boolean onTop = fiducial.isTopSide();

      FiducialRenderer fiducialRenderer = null;

      Collection<com.axi.guiUtil.Renderer> renderers = null;
      if (fiducial.isOnBoard())
      {
        if (onTop)
          renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopBoardFiducialLayer()));
        else
          renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomBoardFiducialLayer()));
      }
      else
        Assert.expect(false);

      Collection<FiducialRenderer> fidRenderers = new ArrayList<FiducialRenderer>();
      for (com.axi.guiUtil.Renderer renderer : renderers)
      {
        fidRenderers.add((FiducialRenderer)renderer);
      }

      for (FiducialRenderer fidRenderer : fidRenderers)
      {
        Fiducial fid = fidRenderer.getFiducial();
        if (fid == fiducial)
        {
          fiducialRenderer = fidRenderer;
          break;
        }
      }
      Assert.expect(fiducialRenderer != null);
      fiducialRenderers.add(fiducialRenderer);
    }
    return fiducialRenderers;
  }

/**
   * @author Andy Mechtenberg
   */
  public void selectPadTypes(Collection<PadType> padTypes)
  {
    Assert.expect(padTypes != null);

    java.util.List<PadRenderer> padRenderers = findPadRenderersForPadTypes(padTypes);
    selectPadRenderers(padRenderers);

    // if all pads belong to the same component, then crosshair that component

    ComponentType componentTypeToSelect = null;
    for(PadType padType : padTypes)
    {
      ComponentType compType = padType.getComponentType();
      if (componentTypeToSelect == null)
        componentTypeToSelect = compType;
      else
      {
        if (compType != componentTypeToSelect)
        {
          componentTypeToSelect = null;
          break;
        }
      }
    }
    if (componentTypeToSelect != null)
    {
      java.util.List<ComponentRenderer> renderersToHighlight = findComponentRenderers(componentTypeToSelect);
      ComponentRenderer firstComponent = (ComponentRenderer)renderersToHighlight.remove(0);
      _graphicsEngine.selectWithCrossHairs(firstComponent);
//      highlightComponentType(componentTypeToSelect);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void selectPadsAndFiducials(Collection<Object> padsAndFiducials)
  {
    java.util.List<Pad> pads = new ArrayList<Pad>();
    java.util.List<Fiducial> fiducials = new ArrayList<Fiducial>();
    for(Object obj : padsAndFiducials)
    {
      if (obj instanceof Pad)
      {
        Pad pad = (Pad)obj;
        pads.add(pad);
      }
      else if (obj instanceof Fiducial)
      {
        Fiducial fiducial = (Fiducial)obj;
        fiducials.add(fiducial);
      }
      else
        Assert.expect(false, "Unexpected object type: " + obj);
    }
    java.util.List<PadRenderer> padRenderers = findPadRenderersForPads(pads);
    selectPadRenderers(padRenderers);

    java.util.List<FiducialRenderer> fiducialRenderers = findFiducialRenderersForFiducials(fiducials);
    selectFiducialRenderers(fiducialRenderers);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void selectPadRenderers(java.util.List<PadRenderer> padRenderers)
  {
    Assert.expect(padRenderers != null);

    // With pads, we don't worry about top/bottom side.  If the pad is visible, it'll be selected.
    // This is because there is a zoom threshold which must be met before pads are drawn anyway, so
    // finding it is pretty easy.
//    _graphicsEngine.clearSelectedRenderers();
//    _graphicsEngine.clearCrossHairs();
//    _graphicsEngine.clearAllFlashingRenderersAndLayers();

    for (PadRenderer padRenderer : padRenderers)
    {
//      setDescription(padRenderer.toString());
      _graphicsEngine.setSelectedRenderer(padRenderer);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void selectFiducialRenderers(java.util.List<FiducialRenderer> fiducialRenderers)
  {
    Assert.expect(fiducialRenderers != null);
    for (FiducialRenderer fiducialRenderer : fiducialRenderers)
      _graphicsEngine.setSelectedRenderer(fiducialRenderer);
  }
  
   /*
  * @author KEe Chin Seong
  */
  private java.util.List<PadRenderer> findNoLoadPadRenderersForPads(Collection<Pad> pads)
  {
    Assert.expect(pads != null);
    java.util.List<PadRenderer> padRenderers = new ArrayList<PadRenderer>();
    for (Pad pad : pads)
    {
      boolean onTop = pad.isTopSide();
      boolean throughHolePad = pad.isThroughHolePad();
      PadRenderer padRenderer = null;
      java.util.List<com.axi.guiUtil.Renderer> renderers = null;

      if (throughHolePad)
      {
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(_topNoLoadPadLayer));
        renderers.addAll(new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(_bottomNoLoadPadLayer)));
      }
      else if (onTop)
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(_topNoLoadPadLayer));
      else
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(_bottomNoLoadPadLayer));

      for (com.axi.guiUtil.Renderer tempPadRenderer : renderers)
      {
        com.axi.v810.business.panelDesc.Pad tempPad = ((PadRenderer)tempPadRenderer).getPad();
        if (tempPad == pad)
        {
          padRenderer = (PadRenderer)tempPadRenderer;
          if (throughHolePad)
          {
            padRenderers.add(padRenderer);
          }
          else
            break;  // if surface mount, it's on top or bottom, but only one place, so we can stop as soon as find one
        }
      }
      // since pads are not always shown, this renderer may not be created.  In other words, it's ok for this to be null
      if ((throughHolePad == false) && (padRenderer != null))
      {
        padRenderers.add(padRenderer);
      }
    }
    return padRenderers;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void highlightPads(Collection<HighlightAlignmentItems> highlightPads)
  {
    Assert.expect(highlightPads != null);

    for(HighlightAlignmentItems highlightPad : highlightPads)
    {
      List<Pad> pads = highlightPad.getPads();
      Color highlightColor = highlightPad.getHighlightColor();
      java.util.List<PadRenderer> padRenderers = findPadRenderersForPads(pads);
      
      //Kee Chin Seong - im expecting there is highlight always here, but if is Empty, Ok might inside the noload Component there
      //                 look for it! BUT provided with the show no load component is turn on
      if(padRenderers.isEmpty() && _showNoLoadComponent == true)
        padRenderers = findNoLoadPadRenderersForPads(pads);
      
      
      highlightPadRenderers(padRenderers, highlightColor);

      List<Fiducial> fiducials = highlightPad.getFiducials();
      java.util.List<FiducialRenderer> fiducialRenderers = findFiducialRenderersForFiducials(fiducials);
      highlightFiducialRenderers(fiducialRenderers, highlightColor);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void highlightPadRenderers(java.util.List<PadRenderer> padRenderers, Color highlightColor)
  {
    Assert.expect(padRenderers != null);

    // With pads, we don't worry about top/bottom side.  If the pad is visible, it'll be highlighted.
    // This is because there is a zoom threshold which must be met before pads are drawn anyway, so
    // finding it is pretty easy.
    for (PadRenderer padRenderer : padRenderers)
    {
//      setDescription(padRenderer.toString());
      _graphicsEngine.setHighightedRenderer(padRenderer, highlightColor);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void highlightFiducialRenderers(java.util.List<FiducialRenderer> fiducialRenderers, Color highlightColor)
  {
    Assert.expect(fiducialRenderers != null);

    // With pads, we don't worry about top/bottom side.  If the pad is visible, it'll be highlighted.
    // This is because there is a zoom threshold which must be met before pads are drawn anyway, so
    // finding it is pretty easy.
    for (FiducialRenderer fiducialRenderer : fiducialRenderers)
    {
//      setDescription(padRenderer.toString());
      _graphicsEngine.setHighightedRenderer(fiducialRenderer, highlightColor);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private java.util.List<PadRenderer> findPadRenderersForPadTypes(Collection<PadType> padTypes)
  {
    Assert.expect(padTypes != null);
    java.util.List<PadRenderer> padRenderers = new ArrayList<PadRenderer>();
    for (PadType padType : padTypes)
    {
      java.util.List<Pad> pads = padType.getPads();
      for (Pad pad : pads)
      {
        boolean onTop = pad.isTopSide();
        PadRenderer padRenderer = null;
        java.util.List<com.axi.guiUtil.Renderer> renderers = null;

        if (onTop)
          renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopPadLayer()));
        else
          renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomPadLayer()));

        for (com.axi.guiUtil.Renderer tempPadRenderer : renderers)
        {
          com.axi.v810.business.panelDesc.Pad tempPad = ((PadRenderer)tempPadRenderer).getPad();
          if (tempPad == pad)
          {
            padRenderer = (PadRenderer)tempPadRenderer;
            break;
          }
        }
        // since pads are not always shown, this renderer may not be created.  In other words, it's ok for this to be null
        if (padRenderer != null)
          padRenderers.add(padRenderer);
      }
    }
    return padRenderers;
  }

  /**
   * @author Andy Mechtenberg
   */
  private java.util.List<PadRenderer> findPadRenderersForPads(Collection<Pad> pads)
  {
    Assert.expect(pads != null);
    java.util.List<PadRenderer> padRenderers = new ArrayList<PadRenderer>();
    for (Pad pad : pads)
    {
      boolean onTop = pad.isTopSide();
      boolean throughHolePad = pad.isThroughHolePad();
      PadRenderer padRenderer = null;
      java.util.List<com.axi.guiUtil.Renderer> renderers = null;

      if (throughHolePad)
      {
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopPadLayer()));
        renderers.addAll(new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomPadLayer())));
      }
      else if (onTop)
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopPadLayer()));
      else
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomPadLayer()));

      for (com.axi.guiUtil.Renderer tempPadRenderer : renderers)
      {
        com.axi.v810.business.panelDesc.Pad tempPad = ((PadRenderer)tempPadRenderer).getPad();
        if (tempPad.getComponentAndPadName().equals(pad.getComponentAndPadName()))
        {
          padRenderer = (PadRenderer)tempPadRenderer;
          if (throughHolePad)
          {
            padRenderers.add(padRenderer);
          }
          else
            break;  // if surface mount, it's on top or bottom, but only one place, so we can stop as soon as find one
        }
      }
      // since pads are not always shown, this renderer may not be created.  In other words, it's ok for this to be null
      if ((throughHolePad == false) && (padRenderer != null))
      {
        padRenderers.add(padRenderer);
      }
    }
    return padRenderers;
  }

  /**
   * @author Andy Mechtenberg
   */
  private java.util.List<FiducialRenderer> findFiducialRenderersForFiducials(Collection<Fiducial> fiducials)
  {
    Assert.expect(fiducials != null);
    java.util.List<FiducialRenderer> fiducialRenderers = new ArrayList<FiducialRenderer>();
    for (Fiducial fiducial : fiducials)
    {
      boolean onTop = fiducial.isTopSide();
      FiducialRenderer fiducialRenderer = null;
      java.util.List<com.axi.guiUtil.Renderer> renderers = null;

      if (onTop)
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopBoardFiducialLayer()));
      else
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomBoardFiducialLayer()));

      for (com.axi.guiUtil.Renderer tempFiducialRenderer : renderers)
      {
        com.axi.v810.business.panelDesc.Fiducial tempFiducial = ((FiducialRenderer)tempFiducialRenderer).getFiducial();
        if (tempFiducial == fiducial)
        {
          fiducialRenderer = (FiducialRenderer)tempFiducialRenderer;
          break;
        }
      }

      if (fiducialRenderer != null)
      {
        fiducialRenderers.add(fiducialRenderer);
      }
    }
    return fiducialRenderers;
  }

/**
   * @author Andy Mechtenberg
   */
  private void updateAlignmentRegionData()
  {
    if(_graphicsEngine.isLayerVisible(_alignmentRegionLayer))
       _graphicsEngine.validateData(_alignmentRegionLayer);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateAlignmentRegionData(boolean topRegion)
  {
//    _graphicsEngine.validateData(_alignmentRegionLayer);
    Collection<com.axi.guiUtil.Renderer> alignmentRenderers = _graphicsEngine.getRenderers(_alignmentRegionLayer);
    for(com.axi.guiUtil.Renderer renderer : alignmentRenderers)
    {
      if (renderer instanceof AlignmentRegionRenderer)
      {
        AlignmentRegionRenderer alignmentRegionRenderer = (AlignmentRegionRenderer)renderer;
        alignmentRegionRenderer.setTopRegion(topRegion);
        alignmentRegionRenderer.refreshData();
      }
      else if (renderer instanceof InactiveAlignmentRegionRenderer)
      {
        InactiveAlignmentRegionRenderer inactiveAlignmentRegionRenderer = (InactiveAlignmentRegionRenderer)renderer;
        inactiveAlignmentRegionRenderer.setTopRegion(topRegion);
        inactiveAlignmentRegionRenderer.refreshData();
      }
      else if (renderer instanceof AlignmentThroughputRenderer)
      {
        AlignmentThroughputRenderer alignmentThroughputRenderer = (AlignmentThroughputRenderer)renderer;
        alignmentThroughputRenderer.setTopRegion(topRegion);
        alignmentThroughputRenderer.refreshData();
      }

    }
    _graphicsEngine.validateData(_alignmentRegionLayer);
  }
  
   /**
   * @author Kee Chin Seong
   */
  public void setTopPadLayerVisibility(boolean visible)
  {
    _showTopPadLayer = visible;
    if(visible == false)
    {
      _graphicsEngine.removeRenderers(getTopPadLayer());//, visible);
      //_componentRendererToPadRenderersMap.clear();
    }
    else
    {
      updateRenderers(_graphicsEngine, getTopPadLayer());
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void repopulateReconstructionRegionGraphics()
  {
     repopulateLayersWithDebugGraphics();
  }
  
  /**
   * @author Kee Chin Seong
   */
  private void setReconstructionRegionsConfigurationsVisible(boolean visible)
  {
    setVerificationIrpBoundariesLayerVisible(false);
    setVerificationFocusRegionLayersVisible(false);
    setVerificationRegionRectangleLayersVisible(false);
    
    setInspectionRegionPadBoundsLayersVisible(false);
    setInspectionRegionRectangleLayersVisible(false);
    setInspectionIrpBoundariesLayerVisible(false);
    setInspectionFocusRegionLayersVisible(false);
    
    setAlignmentFocusRegionLayersVisible(false);
    setAlignmentRegionRectangleLayersVisible(false);
    setAlignmentRegionPadBoundsLayersVisible(false);
    
    _graphicsEngine.setVisible(getBoardNameLayer(), false);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTopLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getTopLayers(), visible);
    
    //if(!_drawDebugGraphics)
      setReconstructionRegionsConfigurationsVisible(false);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getBottomLayers(), visible);
    
    //if(!_drawDebugGraphics)
      setReconstructionRegionsConfigurationsVisible(false);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTopComponentLayersVisible(boolean visible, Panel panel)
  {
    _showTopComponentLayer = visible;
    
    if(visible == false)
    {
        _graphicsEngine.removeRenderers(getTopComponentLayer());
        setTopPadLayerVisibility(false);
        setTopReferenceDesignatorLayerVisibility(false, panel);
        
        for(Board board : panel.getBoards())
        {
            //Kee Chin Seong - need to check top or bottom side exist
            if(board.topSideBoardExists())
              unPopulateComponentLayerWithRenderers(board.getTopSideBoard().getComponents());
        }
    }
    else
    {
        for(Board board : panel.getBoards())
        {
         //Kee Chin Seong - need to check top or bottom side exist
         if(board.topSideBoardExists())
           populateComponentLayersWithRenderers(board, board.getTopSideBoard().getComponents(), true, false);   
        }
        setTopPadLayerVisibility(true);
        setTopReferenceDesignatorLayerVisibility(true, panel);
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean isTopComponentLayersVisible()
  {
    if (_graphicsEngine.isLayerVisible(getTopComponentLayer()))
      return true & _showTopComponentLayer;
    
    return false & _showTopComponentLayer;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean isBottomComponentLayersVisible()
  {
    if (_graphicsEngine.isLayerVisible(getBottomComponentLayer()))
      return true & _showBottomComponentLayer;
    
    return false & _showBottomComponentLayer;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomComponentLayersVisible(boolean visible, Panel panel)
  {
    _showBottomComponentLayer = visible;
    
    if(visible == false)
    {
       _graphicsEngine.removeRenderers(getBottomComponentLayer());
       setBottomPadLayerVisibility(false);
       setBottomReferenceDesignatorLayerVisibility(false, panel);
       
       for(Board board : panel.getBoards())
       {
         //Kee Chin Seong - need to check top or bottom side exist
         if(board.bottomSideBoardExists())
             unPopulateComponentLayerWithRenderers(board.getBottomSideBoard().getComponents());
         }
       }
    else
    {
       for(Board board : panel.getBoards())
       {
         //Kee Chin Seong - need to check top or bottom side exist
         if(board.bottomSideBoardExists())
             populateComponentLayersWithRenderers(board, board.getBottomSideBoard().getComponents(), false, false);
         }
       
       setBottomPadLayerVisibility(false);
       setBottomReferenceDesignatorLayerVisibility(true, panel);
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTopReferenceDesignatorLayerVisibility(boolean visible, Panel panel)
  {
    _showTopComponentRefDesLayer = visible;
    if(visible == false)
      _graphicsEngine.removeRenderers(getTopReferenceDesignatorLayer());//, visible);
    else
    {
      for(Board board : panel.getBoards())
        populateReferenceDesignatorLayerWithRenderers(board, board.getTopSideBoard().getComponents(), true);
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomReferenceDesignatorLayerVisibility(boolean visible, Panel panel)
  {
    _showBottomComponentRefDesLayer = visible;
    if(visible == false)
      _graphicsEngine.removeRenderers(getBottomReferenceDesignatorLayer());//, visible);
    else
    {
      for(Board board : panel.getBoards())
        populateReferenceDesignatorLayerWithRenderers(board, board.getBottomSideBoard().getComponents(), false);
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomPadLayerVisibility(boolean visible)
  {
    _showBottomPadLayer = visible;
    if(visible == false)
      _graphicsEngine.removeRenderers(getBottomPadLayer());//, visible);
    else
    {
      updateRenderers(_graphicsEngine, getBottomPadLayer());
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isTopComponentRefDesLayersVisible()
  {
    if (_graphicsEngine.isLayerVisible(getTopReferenceDesignatorLayer()))
        return true & _showTopComponentRefDesLayer;

    return false & _showTopComponentRefDesLayer;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isBottomComponentRefDesLayersVisible()
  {
   if (_graphicsEngine.isLayerVisible(getBottomReferenceDesignatorLayer()))
      return true & _showBottomComponentRefDesLayer;
   
    return false & _showBottomComponentRefDesLayer;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean isTopPadLayersVisible()
  {
    if (_graphicsEngine.isLayerVisible(getTopPadLayer()))
        return true & _showTopPadLayer;
    
    return false  & _showTopPadLayer;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean isBottomPadLayersVisible()
  {
    if (_graphicsEngine.isLayerVisible(getBottomPadLayer()))
      return true & _showBottomPadLayer;
    
    return false & _showBottomPadLayer;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTopBoardLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getTopBoardLayer(), visible);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomBoardLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getBottomBoardLayer(), visible);
  }
  
    /**
   * @author Kee Chin Seong
   * 
   */
  List<ComponentRenderer> getNoLoadComponentRenderers(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    List<ComponentRenderer> renderers = _noLoadComponentTypeToComponentRenderersMap.get(componentType);
    return renderers;
  }

    /**
   * @author Chin Seong Kee
   */
  private void noLoadComponentRendererToPadRenderersMapPut(ComponentRenderer componentRenderer,
                                                     List<PadRenderer> padRenderers)
  {
    _noLoadComponentRendererToPadRenderersMap.put(componentRenderer, padRenderers);
  }
  
  /**
   * @author Chin Seong Kee
   */
  private void noLoadComponentRendererToPadRenderersMapRemove(ComponentRenderer componentRenderer)
  {
    _noLoadComponentRendererToPadRenderersMap.remove(componentRenderer);
  }


  /**
   * @author George A. David
//   * @author Kee Chin Seong
   */
  private void handleGuiEvent(GuiEvent guiEvent)
  {
    Assert.expect(guiEvent != null);

    GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();

    if (guiEventEnum instanceof SelectionEventEnum)
    {
      //XCR-2207: Yellow crosshair that mark active components are not always ON
      _graphicsEngine.clearSelectedRenderersWithoutCrossHair();
      SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
      if (selectionEventEnum.equals(SelectionEventEnum.CLEAR_SELECTIONS))
      {
        //XCR-2207: Yellow crosshair that mark active components are not always ON
        _graphicsEngine.clearSelectedRenderersWithoutCrossHair();
      }
      else if (selectionEventEnum.equals(SelectionEventEnum.COMPONENT_TYPE) ||
               selectionEventEnum.equals(SelectionEventEnum.PSH_NEIGHBOR_COMPONENT_SELECTION)) //Siew Yeng - XCR-3781
      {
        /** Warning "unchecked cast" approved for cast from Object types.*/
                /** Warning "unchecked cast" approved for cast from Object types.*/
        // modify subtype.
        List<ComponentType> components = (List<ComponentType>)guiEvent.getSource();
        if (components.isEmpty())
          return;
        else if(components.get(0).isLoaded() == false&& _showNoLoadComponent == false)
         return;
        
        if(isTopSideVisible() && _showTopComponentLayer == false)
          return;
        else if(isBottomSideVisible() && _showBottomComponentLayer == false)
          return;
                
        else if (components.size() == 1)
        {
          ComponentType componentType = (ComponentType)components.get(0);
          if(componentType != null)
          {
            highlightComponentType(componentType); // highlight a single component different from multiple components
            
            //Siew Yeng - XCR-3781
            if(selectionEventEnum.equals(SelectionEventEnum.PSH_NEIGHBOR_COMPONENT_SELECTION))
            {
              for (ComponentRenderer componentRenderer : findComponentRenderers(componentType))
              {
                _graphicsEngine.setSelectedRenderer(componentRenderer);
              }
            }
            
            if(getComponentRenderers(componentType) != null)
              setStatusBarDescription(getComponentRenderers(componentType).get(0));
          }
        }
        else
          highlightMultipleComponents(components);
      }
      else if (selectionEventEnum.equals(SelectionEventEnum.FIDUCIAL_TYPE))
      {
        /** Warning "unchecked cast" approved for cast from Object types.*/
        List<FiducialType> fiducials = (List<FiducialType>)guiEvent.getSource();
        if (fiducials.isEmpty())
          return;
        else if (fiducials.size() == 1)
        {
          FiducialType fiducialType = (FiducialType)fiducials.get(0);
          highlightFiducialType(fiducialType); // highlight a single fiducial different from multiple fiducials
          FiducialRenderer fiducialRenderer = getFiducialRenderers(fiducialType).get(0);
          setStatusBarDescription(fiducialRenderer);
        }
        else
          highlightMultipleFiducials(fiducials);
      }
      else if (selectionEventEnum.equals(SelectionEventEnum.PAD_TYPE))
      {
        /** Warning "unchecked cast" approved for cast from Object types.*/
        List<PadType> padTypes = (List<PadType>)guiEvent.getSource();
        if (padTypes.isEmpty())
          return;
        selectPadTypes(padTypes);
        List<Pad> pads = padTypes.get(0).getPads();
        if (pads.isEmpty())
          return;
        Pad pad = pads.get(0);
        PadRenderer padRenderer = getPadRenderer(pad);
        if (padRenderer != null)
          setStatusBarDescription(padRenderer);
      }
      else if (selectionEventEnum.equals(SelectionEventEnum.PAD_OR_FIDUCIAL))
      {
        /** Warning "unchecked cast" approved for cast from Object types.*/
        List<Object> padOrFiducials = (List<Object>)guiEvent.getSource();
        selectPadsAndFiducials(padOrFiducials);
      }
      else if (selectionEventEnum.equals(SelectionEventEnum.PSH_COMPONENT_SELECTION))
      {
        //Siew Yeng - XCR-3781
        /** Warning "unchecked cast" approved for cast from Object types.*/
        // modify subtype.
        List<ComponentType> components = (List<ComponentType>)guiEvent.getSource();
        if (components.isEmpty())
          return;
        else if(components.get(0).isLoaded() == false && _showNoLoadComponent == false)
         return;
        
        if(isTopSideVisible() && _showTopComponentLayer == false)
          return;
        else if(isBottomSideVisible() && _showBottomComponentLayer == false)
          return; 
        else if (components.size() == 1)
        {
          _graphicsEngine.clearHighlightedRenderers();
          ComponentType componentType = (ComponentType)components.get(0);
          if(componentType != null)
          {
            highlightComponentType(componentType); // highlight a single component different from multiple components
            for (ComponentRenderer componentRenderer : findComponentRenderers(componentType))
            {
              _graphicsEngine.setHighightedRenderer(componentRenderer, Color.BLUE);
            }
            
            if(getComponentRenderers(componentType) != null)
              setStatusBarDescription(getComponentRenderers(componentType).get(0));
          }
        }
        else
        {
          for(ComponentType componentType : components)
          {
            for (ComponentRenderer componentRenderer : findComponentRenderers(componentType))
            {
              _graphicsEngine.setHighightedRenderer(componentRenderer, Color.BLUE);
            }
          }
        }
      }
    }
    if (guiEventEnum instanceof HighlightEventEnum)
    {
      HighlightEventEnum highlightEventEnum = (HighlightEventEnum)guiEventEnum;
      if (highlightEventEnum.equals(HighlightEventEnum.CLEAR_HIGHLIGHTS))
      {
        _graphicsEngine.clearHighlightedRenderers();
        _highlightPads.clear();
      }
      else if (highlightEventEnum.equals(HighlightEventEnum.HIGHLIGHT_PAD_INSTANCES))
      {
        //XCR-2207: Yellow crosshair that mark active components are not always ON
        _graphicsEngine.clearHighlightedRenderersWithoutCrossHair();
//        _graphicsEngine.clearSelectedRenderers();
        /** Warning "unchecked cast" approved for cast from Object types.*/
        List<HighlightAlignmentItems> highlightPads = (List<HighlightAlignmentItems>)guiEvent.getSource();
        _highlightPads = highlightPads;
        highlightPads(highlightPads);
      }
      else if (highlightEventEnum.equals(HighlightEventEnum.CENTER_FEATURE))
      {
        Object source = guiEvent.getSource();
        if (source instanceof AlignmentGroup)
        {
          AlignmentGroup alignmentGroup = (AlignmentGroup)source;
          Collection<com.axi.guiUtil.Renderer>
              alignmentRegionRenderers = _graphicsEngine.getRenderers(_alignmentRegionLayer);
          for (com.axi.guiUtil.Renderer alignmentLayerRenderer : alignmentRegionRenderers)
          {
            if (alignmentLayerRenderer instanceof AlignmentRegionRenderer)
            {
              AlignmentRegionRenderer alignmentRegionRenderer = (AlignmentRegionRenderer)alignmentLayerRenderer;
              AlignmentGroup renderedAlignmentGroup = alignmentRegionRenderer.getAlignmentGroup();
              if (renderedAlignmentGroup == alignmentGroup)
              {
                if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0) == false)
                {
                  if (alignmentGroup.isEmpty())
                  {
                    _graphicsEngine.fitGraphicsToScreen();
                    return;
                  }
                  // we're at some zoom level, let's center the graphics to show the new alignment region
                  DoubleCoordinate coordinate = alignmentRegionRenderer.getCenterCoordinate();
                  _graphicsEngine.center(coordinate.getX(), coordinate.getY(), 0, 0);

                  if (alignmentGroup.isTopSide())
                    _controlToolBar.showTopSide();
                  else
                    _controlToolBar.showBottomSide();
                  return;
                }
              }
            }
          }
        }
      }
    }
    if (guiEventEnum instanceof GraphicsControlEnum)
    {
      GraphicsControlEnum graphicsControlEnum = (GraphicsControlEnum)guiEventEnum;
      if (graphicsControlEnum.equals(GraphicsControlEnum.ALIGNMENT_VISIBLE))
      {
        Boolean isTopSide = (Boolean)guiEvent.getSource();
        populateAlignmentRenderers(isTopSide.booleanValue());
        _graphicsEngine.setVisible(_alignmentRegionLayer, true);
        updateAlignmentRegionData(isTopSide);
      }
      else if (graphicsControlEnum.equals(GraphicsControlEnum.ALIGNMENT_INVISIBLE))
      {
        removeAlignmentRenderers();
        _graphicsEngine.clearHighlightedRenderers();
        _highlightPads.clear();
        _graphicsEngine.setVisible(_alignmentRegionLayer, false);
      }
      else if (graphicsControlEnum.equals(GraphicsControlEnum.ORIGIN_VISIBLE))
      {
        _graphicsEngine.setVisible(getBoardOriginLayers(), true);
      }
      else if (graphicsControlEnum.equals(GraphicsControlEnum.ORIGIN_INVISIBLE))
      {
        _graphicsEngine.setVisible(getBoardOriginLayers(), false);
      }
      else if (graphicsControlEnum.equals(GraphicsControlEnum.BOARD_NAME_VISIBLE))
      {
        _graphicsEngine.setVisible(getBoardNameLayer(), true);
      }
      else if (graphicsControlEnum.equals(GraphicsControlEnum.BOARD_NAME_INVISIBLE))
      {
        _graphicsEngine.setVisible(getBoardNameLayer(), false);
      }

    }
    if (guiEventEnum instanceof LongPanelAlignmentRegionEnum)
    {
      LongPanelAlignmentRegionEnum longPanelAlignmentRegionEnum = (LongPanelAlignmentRegionEnum)guiEventEnum;
      if (longPanelAlignmentRegionEnum.equals(LongPanelAlignmentRegionEnum.TOP))
        updateAlignmentRegionData(true);
      if (longPanelAlignmentRegionEnum.equals(LongPanelAlignmentRegionEnum.BOTTOM))
        updateAlignmentRegionData(false);
    }
  }


  /**
   * @author George A. David
   */
  private void removeAlignmentRenderers()
  {
    if (_graphicsEngine.layerExists(_alignmentRegionLayer))
      _graphicsEngine.removeRenderers(_alignmentRegionLayer);
  }

  /**
   * @author Wei Chin
   */
  private void populateAlignmentRenderers(boolean isTopSide)
  {
    Assert.expect(_board != null);
    Project project = _board.getPanel().getProject();
    // populate alignment region layers
    BoardSettings boardSettings = _board.getBoardSettings();
    Assert.expect(project.isTestProgramValidIgnoringAlignmentRegions());
    // in some corner cases, we may not have any test sub programs. this can happen
    // on an extremely wide panel where all the joints lie on the left or right edge.
    // this is becuase our system cannot physically inspect anything wider than 17.5 inches
    TestProgram testProgram = project.getTestProgram();
    if(testProgram.getAllTestSubPrograms().isEmpty())
      return;

    if (testProgram.isLongProgram())
    {
      java.util.List<AlignmentGroup> topAlignmentGroups = boardSettings.getRightAlignmentGroupsForLongPanel();
      java.util.List<AlignmentGroup> bottomAlignmentGroups = boardSettings.getLeftAlignmentGroupsForLongPanel();
      InactiveAlignmentRegionRenderer inactiveAlignmentRegionRenderer = new InactiveAlignmentRegionRenderer(_board, isTopSide);
      _graphicsEngine.addRenderer(_alignmentRegionLayer, inactiveAlignmentRegionRenderer);
      if ((topAlignmentGroups.size() == 3) && (bottomAlignmentGroups.size() == 3))
      {
        AlignmentRegionRenderer region1Renderer = new AlignmentRegionRenderer(project, topAlignmentGroups.get(0),
            bottomAlignmentGroups.get(0),
            LayerColorEnum.ALIGNMENT_GROUP_1_COLOR.getColor());
        AlignmentRegionRenderer region2Renderer = new AlignmentRegionRenderer(project, topAlignmentGroups.get(1),
            bottomAlignmentGroups.get(1),
            LayerColorEnum.ALIGNMENT_GROUP_2_COLOR.getColor());
        AlignmentRegionRenderer region3Renderer = new AlignmentRegionRenderer(project, topAlignmentGroups.get(2),
            bottomAlignmentGroups.get(2),
            LayerColorEnum.ALIGNMENT_GROUP_3_COLOR.getColor());
        _graphicsEngine.addRenderer(_alignmentRegionLayer, region1Renderer);
        _graphicsEngine.addRenderer(_alignmentRegionLayer, region2Renderer);
        _graphicsEngine.addRenderer(_alignmentRegionLayer, region3Renderer);

        // now slap in the performance ones
        AlignmentThroughputRenderer performance1Renderer = new AlignmentThroughputRenderer(_board, true);
        _graphicsEngine.addRenderer(_alignmentRegionLayer, performance1Renderer);
        AlignmentThroughputRenderer performance2Renderer = new AlignmentThroughputRenderer(_board, false);
        _graphicsEngine.addRenderer(_alignmentRegionLayer, performance2Renderer);
      }
    }
    else
    {
      java.util.List<AlignmentGroup> alignmentGroups = boardSettings.getAlignmentGroupsForShortPanel();
      if (alignmentGroups.size() == 3)
      {
        AlignmentRegionRenderer region1Renderer = new AlignmentRegionRenderer(project, alignmentGroups.get(0),
            LayerColorEnum.ALIGNMENT_GROUP_1_COLOR.getColor());
        AlignmentRegionRenderer region2Renderer = new AlignmentRegionRenderer(project, alignmentGroups.get(1),
            LayerColorEnum.ALIGNMENT_GROUP_2_COLOR.getColor());
        AlignmentRegionRenderer region3Renderer = new AlignmentRegionRenderer(project, alignmentGroups.get(2),
            LayerColorEnum.ALIGNMENT_GROUP_3_COLOR.getColor());
        _graphicsEngine.addRenderer(_alignmentRegionLayer, region1Renderer);
        _graphicsEngine.addRenderer(_alignmentRegionLayer, region2Renderer);
        _graphicsEngine.addRenderer(_alignmentRegionLayer, region3Renderer);

        // now slap in the performance ones
        AlignmentThroughputRenderer performance1Renderer = new AlignmentThroughputRenderer(_board, true);
        _graphicsEngine.addRenderer(_alignmentRegionLayer, performance1Renderer);
        AlignmentThroughputRenderer performance2Renderer = new AlignmentThroughputRenderer(_board, false);
        _graphicsEngine.addRenderer(_alignmentRegionLayer, performance2Renderer);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  PadRenderer getPadRenderer(Pad pad)
  {
    Assert.expect(pad != null);
    PadRenderer padRenderer = _padToPadRendererMap.get(pad);
    if (padRenderer == null)
    {
      java.util.List<ComponentRenderer> componentRenderers = _componentTypeToComponentRenderersMap.get(pad.getComponent().getComponentType());
      Assert.expect(componentRenderers.isEmpty() == false);
      ComponentRenderer componentRenderer = componentRenderers.get(0);
      padRenderer = new PadRenderer(pad, componentRenderer);
    }
    return padRenderer;
  }

  /**
   * @author Andy Mechtenberg
   */
  List<ComponentRenderer> getComponentRenderers(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    List<ComponentRenderer> renderers = _componentTypeToComponentRenderersMap.get(componentType);
    //Assert.expect(renderers != null);
    //Assert.expect(renderers.isEmpty() == false);
    return renderers;
  }

  /**
   * @author Laura Cormos
   */
  List<FiducialRenderer> getFiducialRenderers(FiducialType fiducialType)
  {
    Assert.expect(fiducialType != null);
    List<FiducialRenderer> renderers = new ArrayList<FiducialRenderer>();

    for (com.axi.guiUtil.Renderer renderer : _graphicsEngine.getRenderers(getTopBoardFiducialLayer()))
    {
      Assert.expect(renderer instanceof FiducialRenderer);
      if (((FiducialRenderer) renderer).getFiducial().getFiducialType() == fiducialType)
        renderers.add((FiducialRenderer) renderer);
    }
    for (com.axi.guiUtil.Renderer renderer : _graphicsEngine.getRenderers(getBottomBoardFiducialLayer()))
    {
      Assert.expect(renderer instanceof FiducialRenderer);
      if (((FiducialRenderer) renderer).getFiducial().getFiducialType() == fiducialType)
        renderers.add((FiducialRenderer) renderer);
    }
    return renderers;
  }

  /**
   * @author Laura Cormos
   */
  private void componentRendererToPadRenderersMapPut(ComponentRenderer componentRenderer,
                                                     List<PadRenderer> padRenderers)
  {
    _componentRendererToPadRenderersMap.put(componentRenderer, padRenderers);
  }

  /**
   * @author Laura Cormos
   */
  private void componentRendererToPadRenderersMapRemove(ComponentRenderer componentRenderer)
  {
    _componentRendererToPadRenderersMap.remove(componentRenderer);
  }

/**
   * @author George A. David
   */
  public void setInspectionRegionRectangleLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getInspectionRegionRectangleLayers(), visible);
  }

  /**
   * @author George A. David
   */
  public void setInspectionRegionPadBoundsLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getInspectionRegionPadBoundsLayers(), visible);
  }
  
   /*
   * @author Kee Chin Seong
   */
  public void setFocusRegionDebugGraphicVisible(boolean visible)
  {
    if(isTopSideVisible() == true)
    {
      setInspectionFocusRegionTopLayersVisible(visible);
      setInspectionRegionTopLayersVisible(visible);
    }
   
    if(isBottomSideVisible() == true)
    {
      setInspectionFocusRegionBottomLayersVisible(visible);
      setInspectionRegionBottomLayersVisible(visible);
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setInspectionRegionTopLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_topInspectionRegionRectangleLayer, visible);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setInspectionRegionBottomLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_bottomInspectionRegionRectangleLayer, visible);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setInspectionFocusRegionTopLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_topInspectionFocusRegionLayer, visible);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setInspectionFocusRegionBottomLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_bottomInspectionFocusRegionLayer, visible);
  }

  /**
   * @author George A. David
   */
  public void setInspectionFocusRegionLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getInspectionFocusRegionLayers(), visible);
  }

  /**
   * @author George A. David
   */
  public void setVerificationRegionRectangleLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getVerificationRegionRectangleLayers(), visible);
  }

  /**
   * @author George A. David
   */
  public void setVerificationFocusRegionLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getVerificationFocusRegionLayers(), visible);
  }

  /**
   * @author George A. David
   */
  public void setVerificationIrpBoundariesLayerVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_verificationIrpBoundariesLayer, visible);
  }

  /**
   * @author George A. David
   */
  public void setInspectionIrpBoundariesLayerVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_inspectionIrpBoundariesLayer, visible);
  }

  /**
   * @author George A. David
   */
  public void setAlignmentRegionRectangleLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getAlignmentRegionRectangleLayers(), visible);
  }

  /**
   * @author George A. David
   */
  public void setAlignmentRegionPadBoundsLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getAlignmentRegionPadBoundsLayers(), visible);
  }

  /**
   * @author George A. David
   */
  public void setAlignmentFocusRegionLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getAlignmentFocusRegionLayers(), visible);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateProjectData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getProjectChangeEventEnum() instanceof ProjectEventEnum);

    ProjectEventEnum projectEventEnum = (ProjectEventEnum)event.getProjectChangeEventEnum();
    if (projectEventEnum.equals(projectEventEnum.DISPLAY_UNITS))
    {
      MathUtilEnum displayUnits = _board.getPanel().getProject().getDisplayUnits();
      double measureFactor = 1.0/MathUtil.convertUnits(1.0, displayUnits, MathUtilEnum.NANOMETERS);
      _graphicsEngine.setMeasurementInfo(measureFactor, MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits),
                                         MeasurementUnits.getMeasurementUnitString(displayUnits));
    }
    else if(projectEventEnum.equals(ProjectEventEnum.TEST_PROGRAM_GENERATED))
      repopulateLayersWithDebugGraphics();
    else if(projectEventEnum.equals(ProjectEventEnum.TEST_PROGRAM_UPDATED_WITH_ALIGNMENT_REGIONS))
      repopulateLayersWithDebugAlignmentGraphics();
  }

// debugging functions below.
  /**
   * @author Laura Cormos
   */
  private void printRenderers()
  {
    Collection<com.axi.guiUtil.Renderer> allRenderers = _graphicsEngine.getRenderers(getAllLayers());
    System.out.println("Printing all renderers currently residing in the graphics engine");
    for (com.axi.guiUtil.Renderer renderer : allRenderers)
    {
      if (renderer instanceof ComponentRenderer)
      {
        ComponentRenderer rend = (ComponentRenderer)renderer;
        System.out.println("\tComponent Renderer for comp: " + rend.getComponent().getReferenceDesignator() +
                           " on layer: " + _graphicsEngine.getLayerNumberForRenderer(rend));
      }
      else if (renderer instanceof FiducialRenderer)
      {
        FiducialRenderer rend = (FiducialRenderer)renderer;
        System.out.println("\tFiducial Renderer for fid: " + rend.getFiducial().getName() +
                           " on layer: " + _graphicsEngine.getLayerNumberForRenderer(rend));
      }
      else if (renderer instanceof PadRenderer)
      {
        PadRenderer rend = (PadRenderer)renderer;
        if (rend.getPad().isThroughHolePad())
          System.out.println("\tTH pad - should have 2 renderers");
        System.out.println("\tPad Renderer for pad: " + rend.getPad().getComponentAndPadName() +
                           " on layer: " + _graphicsEngine.getLayerNumberForRenderer(rend));
      }
    }
  }
  
    /**
   * @author Jack Hwee
   * Draw a red region to indicates the optical region selected by user in CAD
   */
  public void handleDragOpticalRegionInAlignment()
  {
    _graphicsEngine.removeLayer(_dragOpticalRegionUnSelectedLayer);
    _graphicsEngine.removeLayer(_dragAlignmentOpticalRegionLayer);

    for (java.util.Map.Entry<Rectangle, String> map : getAlignmentDividedOpticalRegionList().entrySet())
    {
      Rectangle rectangle = map.getKey();
         
      Rectangle rectInPixels = rectangle;

      Rectangle rectInNano = _graphicsEngine.calculateSelectedComponentRegionInNanoMeter(rectInPixels);
      _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
      //_dragOpticalRectangleRenderer = new DragOpticalRectangleRenderer();
      //_dragOpticalRectangleRenderer.setRegion(rectInNano.getBounds2D());
      _graphicsEngine.addRenderer(_dragAlignmentOpticalRegionLayer, _dragOpticalRectangleRenderer);
      _graphicsEngine.setVisible(_dragAlignmentOpticalRegionLayer, true);

      _graphicsEngine.repaint();
    }
  }
  
   /**
   * @author Jack Hwee
   */
  public void setDragOpticalRegionLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_dragOpticalRegionLayer, visible);
    _graphicsEngine.setVisible(_dragOpticalRegionUnSelectedLayer, visible);
  }
  
  /**
   * @author Jack Hwee
   */
  public void setDragAlignmentOpticalRegionLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_dragAlignmentOpticalRegionLayer, visible);
  }
  
    /**
   * @author Jack Hwee
   * The list which keep the list of optical region which will be displayed in live view
   */
  public void setAlignmentDividedOpticalRegionList(Map<Rectangle, String> dividedOpticalRegionList)
  {
    _alignmentOpticalRegionList = dividedOpticalRegionList;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public Map<Rectangle, String> getAlignmentDividedOpticalRegionList()
  {
    return _alignmentOpticalRegionList;
  }
  
     /**
   * @author Jack Hwee
   * Draw a yellow region to indicates the optical region drawn in alignment region
   */
  public void handleHighlightOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {  
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_highlightOpticalRegionLayer);

    if (opticalCameraRectangle != null)
    {
      _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
      _highlightRectangleRenderer = new HighlightOpticalRectangleRenderer(opticalCameraRectangle);
      _graphicsEngine.addRenderer(_highlightOpticalRegionLayer, _highlightRectangleRenderer);
      _graphicsEngine.setVisible(_highlightOpticalRegionLayer, true);
      _graphicsEngine.repaint();
    }
  }
  
   /**
   * @author Jack Hwee
   * Draw a blue region to indicates the optical region drawn in alignment region
   */
  public void handleSelectedAlignmentOpticalRegion(java.util.List<Rectangle> rectangleList)
  {  
    _graphicsEngine.removeLayer(_opticalRegionSelectedLayer);
    _graphicsEngine.removeLayer(_dragAlignmentOpticalRegionLayer);
    _graphicsEngine.removeLayer(_opticalRegionSelectedLayer);
  
//    for(Rectangle rectInNano: rectangleList)
//    {
//      rectInNano.setRect(rectInNano.getMinX(), rectInNano.getMinY(), rectInNano.getWidth(), rectInNano.getHeight());
//      _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
//      _selectedRectangleRenderer = new SelectedOpticalRectangleRenderer(opticalCameraRectangle);
//      _graphicsEngine.addRenderer(_opticalRegionSelectedLayer, _selectedRectangleRenderer);
//      _graphicsEngine.setVisible(_opticalRegionSelectedLayer, true);
//
//      // _graphicsEngine.drawRegionCrossHairs();
//      _graphicsEngine.repaint();   
//    }  
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   * Draw multiple yellow region to indicates the optical region selected in combo box
   */
  public void handleMultipleHighlightOpticalCameraRectangles(java.util.List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_highlightOpticalRegionLayer);
    
    if (opticalCameraRectangles != null)
    {
      for(OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangles)
      {
        if (opticalCameraRectangle.getBoard().equals(_board))
        {
          _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
          _highlightRectangleRenderer = new HighlightOpticalRectangleRenderer(opticalCameraRectangle);
          _graphicsEngine.addRenderer(_highlightOpticalRegionLayer, _highlightRectangleRenderer);
          _graphicsEngine.setVisible(_highlightOpticalRegionLayer, true);
          _graphicsEngine.repaint();
        }
      }
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void handleMultipleHighlightOpticalCameraRectanglesForCurrentBoard(java.util.List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_highlightOpticalRegionLayer);
    
    if (opticalCameraRectangles != null)
    {
      for(OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangles)
      {
        if (opticalCameraRectangle.getBoard().equals(_board))
        {
          _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
          _highlightRectangleRenderer = new HighlightOpticalRectangleRenderer(opticalCameraRectangle);
          _graphicsEngine.addRenderer(_highlightOpticalRegionLayer, _highlightRectangleRenderer);
          _graphicsEngine.setVisible(_highlightOpticalRegionLayer, true);
          _graphicsEngine.repaint();  
        }
      }
    }
  }

  /*
   * @author Kee Chin Seong
   */
  public void setEditFocusRegionMode(boolean isEnabled)
  {
    _isFocusRegionEditorOn = isEnabled;
  }
  
  /**
   * @author Jack Hwee
   */
  public void setOpticalRegionSelectedLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_opticalRegionSelectedLayer, visible);
    _graphicsEngine.setVisible(_opticalRegionUnSelectedLayer, visible);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setMeshTriangleLayerVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_meshTriangleLayer, visible);
  }
  
  /**
   * @author Jack Hwee
   * The list which keep the list of optical region which will be displayed in live view
   */
  public void setDividedOpticalRegionList(java.util.List<OpticalCameraRectangle> dividedOpticalRegionList)
  {
    _opticalCameraRectangleList = dividedOpticalRegionList;
    handleDragOpticalRegion();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void addDividedOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {
    if (hasDividedOpticalCameraRectangleAlreadyAdded(opticalCameraRectangle) == false)
      _opticalCameraRectangleList.add(opticalCameraRectangle);
    
    //handleDragOpticalRegion();
  }
  
  private boolean hasDividedOpticalCameraRectangleAlreadyAdded(OpticalCameraRectangle opticalCameraRectangle)
  {
    for (OpticalCameraRectangle ocr : _opticalCameraRectangleList)
    {
      if (ocr.getRegion().equals(opticalCameraRectangle.getRegion()))
      {
        if (ocr.getBoard().equals(opticalCameraRectangle.getBoard()))
        {
          return true;
        }
      }
    }
    return false;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removeDividedOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {
    if (_opticalCameraRectangleList.contains(opticalCameraRectangle))
      _opticalCameraRectangleList.remove(opticalCameraRectangle);
    
    clearHighlightedOpticalCameraRectangleList();
    //handleDragOpticalRegion();
  }
  
  /**
   * @author Jack Hwee
   * Clear the list
   */
  public void clearDividedOpticalRegionList()
  {
    if (_opticalCameraRectangleList != null)
      _opticalCameraRectangleList.clear();
    
    handleDragOpticalRegion();
  }
  
  /**
   * Clear the list for certain board.
   * @author Ying-Huan.Chu
   */
  public void clearDividedOpticalRegionList(Board board)
  {
    Assert.expect(board != null);
    
    java.util.List<OpticalCameraRectangle> opticalCameraRectangleList = new ArrayList<>();
    for (OpticalCameraRectangle opticalCameraRectangle : _opticalCameraRectangleList)
    {
      if (opticalCameraRectangle.getBoard().equals(board))
      {
        opticalCameraRectangleList.add(opticalCameraRectangle);
      }
    }
    _opticalCameraRectangleList.removeAll(opticalCameraRectangleList);
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public java.util.List<OpticalCameraRectangle> getDividedOpticalRegionListForAllBoards()
  {
    if (_opticalCameraRectangleList == null)
      _opticalCameraRectangleList = new ArrayList<OpticalCameraRectangle>();
    return _opticalCameraRectangleList;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public java.util.List<OpticalCameraRectangle> getDividedOpticalRegionList(Board board)
  {
    Assert.expect(board != null);
    
    if (_opticalCameraRectangleList == null)
    {
      _opticalCameraRectangleList = new ArrayList<OpticalCameraRectangle>();
      return _opticalCameraRectangleList;
    }
    else
    {
      java.util.List<OpticalCameraRectangle> opticalCameraRectangleList = new ArrayList<>();
      for (OpticalCameraRectangle opticalCameraRectangle : _opticalCameraRectangleList)
      {
        if (opticalCameraRectangle.getBoard().equals(board))
        {
          opticalCameraRectangleList.add(opticalCameraRectangle);
        }
      }
      return opticalCameraRectangleList;
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public Map<Board, java.util.List<OpticalCameraRectangle>> getBoardToOpticalRegionMap()
  {
    Map<Board, java.util.List<OpticalCameraRectangle>> boardToOpticalRegionMap = new LinkedHashMap<>();
    if (_opticalCameraRectangleList != null)
    {
      for (Board board : _board.getPanel().getBoards())
      {
        if (getDividedOpticalRegionList(board).isEmpty() == false)
        {
          boardToOpticalRegionMap.put(board, getDividedOpticalRegionList(board));
        }
      }
    }
    return boardToOpticalRegionMap;
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   * Draw a red region to indicates the optical region selected by user in CAD
   */
  public void handleDragOpticalRegion()
  { 
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_dragOpticalRegionUnSelectedLayer);
    
    for (OpticalCameraRectangle ocr : getDividedOpticalRegionList(_board))
    {
      _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
      _dragOpticalRectangleRenderer = new DragOpticalRectangleRenderer(ocr);
      _graphicsEngine.addRenderer(_dragOpticalRegionUnSelectedLayer, _dragOpticalRectangleRenderer);
      _graphicsEngine.setVisible(_dragOpticalRegionUnSelectedLayer, true);
      _graphicsEngine.repaint();
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getAllInspectedOpticalRegions()
  {
    Assert.expect(_opticalCameraRectangleList != null);

    int inspectedOpticalRegionsCount = 0;
    for(OpticalCameraRectangle ocr : _opticalCameraRectangleList)
    {
      boolean bool = ocr.getInspectableBool();
      if (bool == true)
        ++inspectedOpticalRegionsCount;
    }
    return inspectedOpticalRegionsCount;
  }
  
  /**
   * @author Jack Hwee
   * Draw a yellow region to indicates the optical region selected in combo box
   */
  public void handleHighlightOpticalRegionWhenLiveView(OpticalCameraRectangle opticalCameraRectangle)
  {
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_highlightOpticalRegionLayer);

    if (opticalCameraRectangle != null)
    {
      _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
      _highlightRectangleRenderer = new HighlightOpticalRectangleRenderer(opticalCameraRectangle);
      _graphicsEngine.addRenderer(_highlightOpticalRegionLayer, _highlightRectangleRenderer);
      _graphicsEngine.setVisible(_highlightOpticalRegionLayer, true);
      _graphicsEngine.repaint();
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  public void addHighlightedOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {
    Assert.expect(opticalCameraRectangle != null);
      
    if (_highlightedOpticalCameraRectangleList.contains(opticalCameraRectangle) == false)
      _highlightedOpticalCameraRectangleList.add(opticalCameraRectangle);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setHighlightedOpticalCameraRectangleList(java.util.List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    Assert.expect(opticalCameraRectangles != null);
    
    _highlightedOpticalCameraRectangleList = opticalCameraRectangles;
    handleMultipleHighlightOpticalCameraRectangles(_highlightedOpticalCameraRectangleList);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private OpticalRegion getSelectedOpticalRegion(String opticalRegionName)
  {
    Assert.expect(opticalRegionName != null);
    
    Project project = _board.getPanel().getProject();
    boolean isCheckLatestTestProgram = project.isCheckLatestTestProgram();
    project.setCheckLatestTestProgram(false);
    TestProgram testProgram = _board.getPanel().getProject().getTestProgram();
    project.setCheckLatestTestProgram(isCheckLatestTestProgram);
    
    OpticalRegion opticalRegion = null;
    if (_board.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      opticalRegion = testProgram.getOpticalRegion(opticalRegionName);
    }
    else
    {
      String name[] = opticalRegionName.toString().split("_", 6);
      String newOpticalRegionName = name[0] + "_" + name[1] + "_" + name[2] + "_" + name[3];
      opticalRegion = testProgram.getOpticalRegion(_board, newOpticalRegionName);
    }
    return opticalRegion; 
  }
  
  /**
   * @author Jack Hwee
   */
  public void setOpticalRegionUnSelectedLayerVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_opticalRegionUnSelectedLayer, visible);
  }
  
  /**
   * @author Jack Hwee
   */
  public java.util.List<OpticalCameraRectangle> getSelectedOpticalCameraRectangleList()
  {
    return _selectedOpticalCameraRectangleList;
  }
  
  /**
   * 
   * @author Jack Hwee
   * The list which keep the list of optical region which is going to be displayed live view
   */
  public void addSelectedOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {     
    if (_selectedOpticalCameraRectangleList.contains(opticalCameraRectangle) == false)
       _selectedOpticalCameraRectangleList.add(opticalCameraRectangle);
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public void removeSelectedOpticalCameraRectangle(OpticalCameraRectangle ocr)
  {
    if (_selectedOpticalCameraRectangleList.contains(ocr))
      _selectedOpticalCameraRectangleList.remove(ocr);
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public void clearSelectedOpticalCameraRectangleList()
  {
    if (_selectedOpticalCameraRectangleList != null)
      _selectedOpticalCameraRectangleList.clear();
    
    handleMultipleSelectedOpticalCameraRectangles(_selectedOpticalCameraRectangleList);
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public void clearUnselectedOpticalCameraRectangleList()
  {
    if (_unselectedOpticalCameraRectangleList != null)
      _unselectedOpticalCameraRectangleList.clear();
  }
  
  /**
   * @author Jack Hwee
   */
  public boolean isHighlightOpticalRegionLayerVisible()
  {   
    return _graphicsEngine.isLayerVisible(_highlightOpticalRegionLayer); 
  }
  
  /**
   * @author Jack Hwee
   */
  public boolean isDragOpticalRegionLayerVisible()
  {   
    return _graphicsEngine.isLayerVisible(_dragOpticalRegionLayer); 
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean isDragOpticalRegionUnselectedLayerVisible()
  {
    return _graphicsEngine.isLayerVisible(_dragOpticalRegionUnSelectedLayer);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean isSelectedOpticalRegionLayerVisible()
  {
    return _graphicsEngine.isLayerVisible(_opticalRegionSelectedLayer);
  }
  
  /**
   * @author Jack Hwee
   */
  public java.util.List<OpticalCameraRectangle> getHighlightedOpticalCameraRectangleList()
  {
    return _highlightedOpticalCameraRectangleList;
  }
  
  /**
   * @author Jack Hwee
   * Draw a blue region to indicates the optical region selected in combo box
   */
  public void handleSelectedOpticalCameraRectangleWithGivenRegion(String regionName)
  {
    Assert.expect(regionName != null);
    
    _graphicsEngine.removeLayer(_opticalRegionUnSelectedLayer);
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_opticalRegionSelectedLayer);
       
    OpticalRegion opticalRegion = getSelectedOpticalRegion(regionName);    
    if (opticalRegion != null)
    {  
      for(OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getOpticalCameraRectangles(_board))
      {
        addSelectedOpticalCameraRectangle(opticalCameraRectangle);
        _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
        _selectedRectangleRenderer = new SelectedOpticalRectangleRenderer(opticalCameraRectangle, opticalCameraRectangle.getInspectableBool());
        _graphicsEngine.addRenderer(_opticalRegionSelectedLayer, _selectedRectangleRenderer);
        _graphicsEngine.setVisible(_opticalRegionSelectedLayer, true);
        _graphicsEngine.repaint();
      }
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   * Draw a blue region to indicates the optical region selected in combo box
   */
  public void handleSelectedOpticalCameraRectangleWithGivenBoard(Board board)
  {
    Assert.expect(board != null);
    
    _graphicsEngine.removeLayer(_opticalRegionUnSelectedLayer);
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_opticalRegionSelectedLayer);
    
    if (board.hasBoardSurfaceMapSettings())
    {
      BoardSurfaceMapSettings boardSurfaceMapSettings = board.getBoardSurfaceMapSettings();
      if (boardSurfaceMapSettings.hasOpticalRegion())
      {
        for (OpticalRegion opticalRegion : boardSurfaceMapSettings.getOpticalRegions())
        {
          for(OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getOpticalCameraRectangles(board))
          {
            addSelectedOpticalCameraRectangle(opticalCameraRectangle);
            _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
            _selectedRectangleRenderer = new SelectedOpticalRectangleRenderer(opticalCameraRectangle, opticalCameraRectangle.getInspectableBool());
            _graphicsEngine.addRenderer(_opticalRegionSelectedLayer, _selectedRectangleRenderer);
            _graphicsEngine.setVisible(_opticalRegionSelectedLayer, true);
            _graphicsEngine.repaint();
          }
        }
      }
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   * Draw a blue region to indicates the optical region selected in combo box
   */
  public void handleMultipleSelectedOpticalCameraRectangles(java.util.List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    _graphicsEngine.removeLayer(_opticalRegionUnSelectedLayer);
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_opticalRegionSelectedLayer);
    _graphicsEngine.removeLayer(_highlightOpticalRegionLayer);
    
    _selectedOpticalCameraRectangleList = opticalCameraRectangles;
    
    if (opticalCameraRectangles != null)
    {
      for(OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangles)
      {
        if (opticalCameraRectangle.getBoard().equals(_board))
        {
          _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
          _selectedRectangleRenderer = new SelectedOpticalRectangleRenderer(opticalCameraRectangle, opticalCameraRectangle.getInspectableBool());
          _graphicsEngine.addRenderer(_opticalRegionSelectedLayer, _selectedRectangleRenderer);
          _graphicsEngine.setVisible(_opticalRegionSelectedLayer, true);
          _graphicsEngine.repaint();
        }
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  /*private OpticalRegion getSelectedOpticalRegion()
  {
    Assert.expect(_tableOpticalRegions != null);
    
    TestProgram testProgram = _board.getPanel().getProject().getTestProgram();
    OpticalRegion opticalRegion = null;
    
    if (_tableOpticalRegions.isEmpty() == false)
    {
      String opticalRegionName =  _tableOpticalRegions.get(_index);
      if (_board.getPanel().getPanelSettings().isPanelBasedAlignment())
      {
        opticalRegion = testProgram.getOpticalRegion(opticalRegionName);
      }
      else
      {
        String name[] = opticalRegionName.toString().split("_", 6);
        String newOpticalRegionName = name[2] + "_" + name[3] + "_" + name[4] + "_" + name[5];
        opticalRegion = testProgram.getOpticalRegion(newOpticalRegionName);
      }
    }
    return opticalRegion;
  }*/
  
  /**
   * @author Jack Hwee
   */
  public void setHighlightOpticalRegionLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_highlightOpticalRegionLayer, visible);
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public void clearHighlightedOpticalCameraRectangleList()
  {
    if (_highlightedOpticalCameraRectangleList != null)
      _highlightedOpticalCameraRectangleList.clear();
    
    handleMultipleHighlightOpticalCameraRectangles(_highlightedOpticalCameraRectangleList);
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public void addSelectedRegionInTable(String regionName)
  {
    Assert.expect(regionName != null);

    if (_tableOpticalRegions.contains(regionName) == false)
      _tableOpticalRegions.add(regionName);
    setSelectedRegionInTableAvailable();       
  }
  /**
   * 
   * @author Jack Hwee
   */
  public void addSelectedRegionInComboboxUsingIndex(String regionName, int index)
  {
    Assert.expect(regionName != null);

    if (_tableOpticalRegions.contains(regionName) == false)
      _tableOpticalRegions.add(index, regionName);
    setSelectedRegionInTableAvailable();       
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public void removeSelectedRegionInTable(String regionName)
  {    
    Assert.expect(regionName != null);
    _tableOpticalRegions.remove(regionName);
    setSelectedRegionInTableAvailable();
  }
  
  /**
   * Clear Selected OpticalCameraRectangles of a Board's OpticalRegion
   * @author Ying-Huan.Chu
   */
  public void clearSelectedOpticalCameraRectangleList(Board board)
  {
    Assert.expect(board != null);
    java.util.List<OpticalCameraRectangle> boardSelectedOpticalCameraRectangles = new ArrayList<>();
    for (OpticalCameraRectangle opticalCameraRectangle : _selectedOpticalCameraRectangleList)
    {
      if (opticalCameraRectangle.getBoard().equals(board))
      {
        boardSelectedOpticalCameraRectangles.add(opticalCameraRectangle);
      }
    }
    _selectedOpticalCameraRectangleList.removeAll(boardSelectedOpticalCameraRectangles);
  }

  /**
   * 
   * @author Jack Hwee
   */
  public Rectangle getSelectedRegionInCombobox()
  {
    if (_tableOpticalRegions.isEmpty() == false)
    {
      int x = 0; 
      int y = 0; 
      int width = 0; 
      int height = 0; 
      String regionName =  _tableOpticalRegions.get(_index);

      if (_board.getPanel().getPanelSettings().isPanelBasedAlignment())
      {
        String name[] = regionName.toString().split("_", 4);
        x = Integer.parseInt(name[0], 10); 
        y = Integer.parseInt(name[1], 10); 
        width = Integer.parseInt(name[2], 10); 
        height = Integer.parseInt(name[3], 10); 
      }
      else
      {
        String name[] = regionName.toString().split("_", 6);
        x = Integer.parseInt(name[2], 10); 
        y = Integer.parseInt(name[3], 10); 
        width = Integer.parseInt(name[4], 10); 
        height = Integer.parseInt(name[5], 10); 
      }
      Rectangle rect = new Rectangle(x , y , width, height);
      return rect;
    }
    else
      return new Rectangle(0,0,0,0);
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public void setSelectedRegionIndexInTable(int index)
  {
    _index = index;
  }
 
  /** 
   * @author Jack Hwee
   */
  public void setSelectedRegionInTableAvailable()
  {
    if (_tableOpticalRegions.isEmpty() )
      _graphicsEngine.setSelectedRegionInComboboxAvailable(false);
    else
      _graphicsEngine.setSelectedRegionInComboboxAvailable(true);
  }
  
  /**
   * 
   * @author Jack Hwee
   * Remove all the Blue rectangles 
   */
  public void removeSelectedOpticalRectangleLayer()
  {
    _graphicsEngine.removeLayer(_opticalRegionSelectedLayer);
    _graphicsEngine.removeLayer(_highlightOpticalRegionLayer);
    _graphicsEngine.removeLayer(_meshTriangleLayer);
    _graphicsEngine.removeLayer(_opticalRegionUnSelectedLayer);
    _graphicsEngine.repaint();
  }
  
  /**
   * @author Jack Hwee
   */
  public void removeMeshTriangleLayer()
  {
    _graphicsEngine.removeLayer(_meshTriangleLayer);
    _graphicsEngine.repaint();
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public void clearIgnoredOpticalCameraRectangleList()
  {
    if (_ignoredOpticalCameraRectangleList != null)
       _ignoredOpticalCameraRectangleList.clear();
  }
  
  /**
   * 
   * @author Jack Hwee
   * The list which keep the list of optical region which not going to be displayed live view
   */
  public void removeIgnoreOpticalCameraRectangleList(OpticalCameraRectangle opticalCameraRectangle)
  {
    if (_ignoredOpticalCameraRectangleList.contains(opticalCameraRectangle))
       _ignoredOpticalCameraRectangleList.remove(opticalCameraRectangle);
  }
  
  /**
   *
   * @author Jack Hwee
   */
  public DragOpticalRectangleRenderer getDragOpticalRectangleRenderer()
  {
    return _dragOpticalRectangleRenderer;
  }
  
   /**
   * @author Andy Mechtenberg
   */
  public List<ComponentRenderer> getSurfaceMapSelectedComponentRenderers(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    List<ComponentRenderer> renderers = _componentTypeToComponentRenderersMap.get(componentType);
    Assert.expect(renderers != null);
    Assert.expect(renderers.isEmpty() == false);
    return renderers;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public Board getCurrentDisplayBoard()
  {
    return _board;
  }
  
   /**
   * @author Jack Hwee
   * Clear the list
   */
  public void clearAlignmentDividedOpticalRegionList()
  {
    _alignmentOpticalRegionList.clear();
  }
  
   /**
   * @author Jack Hwee
   */
  public int getAllInspectedAlignmentOpticalRegions()
  {
    Assert.expect(_alignmentOpticalRegionList != null);

    return _alignmentOpticalRegionList.size();
  }
  
  /**
   * @author Ying-Huan.Chu
   * @author Jack Hwee
   */
  public Rectangle calculateRectangleInNanometerRelativeToBoard(Board board, Rectangle rectInNano, double distanceXInNano, double distanceYInNano)
  {
    Point2D boardCadOrigin = board.getLowerLeftCoordinateRelativeToPanelInNanoMeters().getPoint2D();
    Rectangle newRectInNano = new Rectangle();
    newRectInNano.setRect((boardCadOrigin.getX() + distanceXInNano), 
                          (boardCadOrigin.getY() + distanceYInNano), 
                          rectInNano.width, 
                          rectInNano.height);
    return newRectInNano;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public OpticalCameraRectangle getOpticalCameraRectangleRelativeToBoard(Board boardToAddOpticalCameraRectangle, OpticalCameraRectangle opticalCameraRectangle)
  {
    if (boardToAddOpticalCameraRectangle.equals(opticalCameraRectangle.getBoard()))
    {
      return opticalCameraRectangle;
    }
    Board board = boardToAddOpticalCameraRectangle.getPanel().getBoard(opticalCameraRectangle.getBoard().getName());
    Rectangle boardShape = board.getShapeRelativeToPanelInNanoMeters().getBounds();
    double boardOriginMinXToOpticalCameraRectangleMinXDistanceInNano = opticalCameraRectangle.getRegion().getMinX() - boardShape.getMinX();
    double boardOriginMinYToOpticalCameraRectangleMinYDistanceInNano = opticalCameraRectangle.getRegion().getMinY() - boardShape.getMinY();
    
    Rectangle rectInNano = opticalCameraRectangle.getRegion().getRectangle2D().getBounds();
    Rectangle newRectInNano = calculateRectangleInNanometerRelativeToBoard(boardToAddOpticalCameraRectangle, 
                                                                            rectInNano, 
                                                                            boardOriginMinXToOpticalCameraRectangleMinXDistanceInNano,
                                                                            boardOriginMinYToOpticalCameraRectangleMinYDistanceInNano);
    OpticalCameraRectangle newOpticalCameraRectangle = new OpticalCameraRectangle();
    newOpticalCameraRectangle.setRegion(newRectInNano.x, newRectInNano.y, newRectInNano.width, newRectInNano.height);
    newOpticalCameraRectangle.setBoard(boardToAddOpticalCameraRectangle);
    newOpticalCameraRectangle.setInspectableBool(true);
    newOpticalCameraRectangle.setZHeightInNanometer(0);
    return newOpticalCameraRectangle;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public PanelCoordinate calculatePointInPanelCoordinateRelativeToBoard(Board board, double distanceXInNano, double distanceYInNano)
  {
    Point2D boardCadOrigin = board.getLowerLeftCoordinateRelativeToPanelInNanoMeters().getPoint2D();
    PanelCoordinate newPoint = new PanelCoordinate();
    newPoint.setLocation((int)(boardCadOrigin.getX() + distanceXInNano), (int)(boardCadOrigin.getY() + distanceYInNano));
    return newPoint;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public PanelCoordinate getSurfaceMapPointInPanelCoordinateRelativeToBoard(Board boardToCopyFrom, Board boardToCopyTo, PanelCoordinate panelCoordinatePoint)
  {
    Rectangle boardShape = boardToCopyFrom.getShapeRelativeToPanelInNanoMeters().getBounds();
    double boardOriginMinXToPanelCoordinatePointXDistanceInNano = panelCoordinatePoint.getX() - boardShape.getMinX();
    double boardOriginMinYToPanelCoordinatePointYDistanceInNano = panelCoordinatePoint.getY() - boardShape.getMinY();
    PanelCoordinate newPanelCoordinate = calculatePointInPanelCoordinateRelativeToBoard(boardToCopyTo, 
                                                                                      boardOriginMinXToPanelCoordinatePointXDistanceInNano, 
                                                                                      boardOriginMinYToPanelCoordinatePointYDistanceInNano);
    return newPanelCoordinate;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public PanelCoordinate calculatePointInPixelRelativeToBoard(Board board, double distanceXInNano, double distanceYInNano)
  {
    Point2D boardCadOrigin = board.getLowerLeftCoordinateRelativeToPanelInNanoMeters().getPoint2D();
    PanelCoordinate newPoint = new PanelCoordinate();
    newPoint.setLocation((int)(boardCadOrigin.getX() + distanceXInNano), (int)(boardCadOrigin.getY() + distanceYInNano));
    return newPoint;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public PanelCoordinate getSurfaceMapPointPositionInPixelRelativeToBoard(Board boardToCopyFrom, Board boardToCopyTo, PanelCoordinate panelCoordinatePoint)
  {
    Rectangle boardShape = boardToCopyFrom.getShapeRelativeToPanelInNanoMeters().getBounds();
    double boardOriginMinXToPanelCoordinatePointXDistanceInNano = panelCoordinatePoint.getX() - boardShape.getMinX();
    double boardOriginMinYToPanelCoordinatePointYDistanceInNano = panelCoordinatePoint.getY() - boardShape.getMinY();
    PanelCoordinate newPanelCoordinate = calculatePointInPanelCoordinateRelativeToBoard(boardToCopyTo, 
                                                                                      boardOriginMinXToPanelCoordinatePointXDistanceInNano, 
                                                                                      boardOriginMinYToPanelCoordinatePointYDistanceInNano);
    return newPanelCoordinate;
  }
  
  /**
   * @author Jack Hwee
   * Draw a blue lines to indicates the mesh triangle
   */
  public void handleMeshTriangle(java.util.List<MeshTriangle> meshTriangleList)
  {
    //_graphicsEngine.removeLayer(_opticalRegionUnSelectedLayer);
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_meshTriangleLayer);
    //_graphicsEngine.removeLayer(_opticalRegionSelectedLayer);
    //_graphicsEngine.removeLayer(_highlightOpticalRegionLayer);

    for (MeshTriangle meshTriangle: meshTriangleList)
    {
      int line1BeginX = meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getCenterX();
      int line1EndX = meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getCenterX();
      int line1BeginY = meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getCenterY();
      int line1EndY = meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getCenterY();
      
      int line2BeginX = meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getCenterX();
      int line2EndX = meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getCenterX();
      int line2BeginY = meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getCenterY();
      int line2EndY = meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getCenterY();
      
      int line3BeginX = meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getCenterX();
      int line3EndX = meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getCenterX();
      int line3BeginY = meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getCenterY();
      int line3EndY = meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getCenterY();

      _meshTriangleRenderer = new MeshTriangleRenderer();
      _meshTriangleRenderer.setLine(line1BeginX, line1EndX, line1BeginY, line1EndY, line2BeginX, line2EndX, line2BeginY, line2EndY,
                                    line3BeginX, line3EndX, line3BeginY, line3EndY);
      _graphicsEngine.addRenderer(_meshTriangleLayer, _meshTriangleRenderer);
      _graphicsEngine.setVisible(_meshTriangleLayer, true);      
      _graphicsEngine.repaint();       
    }
  }
}
