package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This class draws a PackagePin outline.
 *
 * @author Bill Darbie
 */
public class PackagePinRenderer extends ShapeRenderer
{
  private PackagePin _packagePin;

  /**
   * @author Tan Hock Zoon
   */
  PackagePinRenderer()
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
  }

  /**
   * @author Andy Mechtenberg
   */
  PackagePinRenderer(PackagePin packagePin)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(packagePin != null);
    _packagePin = packagePin;

    refreshData();
  }

  /**
   * @author Bill Darbie
   */
  public PackagePin getPackagePin()
  {
    return _packagePin;
  }

  /**
   * @author Bill Darbie
   */
  protected void refreshData()
  {
    Assert.expect(_packagePin != null);

    LandPatternPad landPatternPad = _packagePin.getLandPatternPad();
    if (landPatternPad.isThroughHolePad())
    {
      Assert.expect(landPatternPad instanceof ThroughHoleLandPatternPad);

      ThroughHoleLandPatternPad throughholeLandPatternPad = (ThroughHoleLandPatternPad)landPatternPad;

      initializeShape(landPatternPad.getShapeInNanoMeters(), throughholeLandPatternPad.getHoleShapeInNanoMeters());
    }
    else
    {
      initializeShape(landPatternPad.getShapeInNanoMeters());
    }

  }

  /**
   * Pad 1 needs to be hilighted (colored in RED), so we'll override the default paintComponent
   * @author Andy Mechtenberg
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    Color origColor = graphics2D.getColor();
    
    boolean isThroughHole = _packagePin.getLandPatternPad().isThroughHolePad();

    if (getForeground() != getSelectedColor()) // if selected, use the selected color
    {      
      if (_packagePin.isPadOne())
      {
        if (isThroughHole)
          graphics2D.setColor(LayerColorEnum.FIRST_THROUGHHOLE_PAD_COLOR.getColor());
        else
          graphics2D.setColor(LayerColorEnum.FIRST_SURFACEMOUNT_PAD_COLOR.getColor());
      }
      else if (isThroughHole)
        graphics2D.setColor(LayerColorEnum.THROUGHHOLE_PAD_COLOR.getColor());
    }

    graphics2D.fill(getShape());

    if (isThroughHole)
    {
      Assert.expect(getNumberOfShapes() == 2);
      // darken the color slightly
      graphics2D.setColor(graphics2D.getColor().darker());
      graphics2D.fill(getShape(1));
    }

    // now do orientation arrow
    drawOrientationArrow(graphics2D, _packagePin.getPinOrientationAfterRotationEnum());

    graphics2D.setColor(origColor);
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void drawOrientationArrow(Graphics2D graphics2D, PinOrientationAfterRotationEnum orientation)
  {
    Assert.expect(graphics2D != null);
    Assert.expect(orientation != null);

    int strokeSize = 4;
    double arrowSizeFactor = 2.3;  // A factor of 2 is half the size of the pad.  2.3 is slightly less.

    Shape padShape = getShape();
    Rectangle rect = padShape.getBounds();
    int centerX = (int)rect.getCenterX();
    int centerY = (int)rect.getCenterY();
    int arrowHeadSize = (int)Math.max(rect.getWidth(), rect.getHeight()) / 8;

    int shaftEndX = 0, shaftEndY = 0;
    int pointEndX = 0, pointEndY = 0;
    int startLeftX = 0, startLeftY = 0;
    int startRightX = 0, startRightY = 0;

    if (orientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN))
    {
      // arrow starts from North and Points to South (DOWN ARROW)
      // draw three lines:  First is arrow's shaft:  Center at pad center, end point at pad edge

      // pointEnd is the tip of the point.  The arrow starts at Center and ends at Point
      pointEndX = centerX;
      pointEndY = centerY + (int)(rect.getHeight() / arrowSizeFactor);

      // the shaft doesn't go all the way to the point because with larger stroke sizes, it extends past the point
      // and looks bad.  So we end the shaft right where the point triangle is to begin
      shaftEndX = centerX;
      shaftEndY = pointEndY - arrowHeadSize;

      // now the diagonal lines which define the tip of the arrow
      // left line "\"
      startLeftX = pointEndX - arrowHeadSize;
      startLeftY = pointEndY - arrowHeadSize;

      // right line "/"
      startRightX = pointEndX + arrowHeadSize;
      startRightY = pointEndY - arrowHeadSize;
    }
    else if (orientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN))
    {
      // arrow starts from South and Points to North (UP ARROW)
      // draw three lines:  First is arrow's shaft:  Center at pad center, end point at pad edge

      // pointEnd is the tip of the point.  The arrow starts at Center and ends at Point
      pointEndX = centerX;
      pointEndY = centerY - (int)(rect.getHeight() / arrowSizeFactor);

      // the shaft doesn't go all the way to the point because with larger stroke sizes, it extends past the point
      // and looks bad.  So we end the shaft right where the point triangle is to begin
      shaftEndX = centerX;
      shaftEndY = pointEndY + arrowHeadSize;

      // now the diagonal lines which define the tip of the arrow
      // left line "\"
      startLeftX = pointEndX + arrowHeadSize;
      startLeftY = pointEndY + arrowHeadSize;

      // right line "/"
      startRightX = pointEndX - arrowHeadSize;
      startRightY = pointEndY + arrowHeadSize;
    }
    else if (orientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN))
    {
      // arrow starts from East and Points to West (LEFT ARROW)
      // draw three lines:  First is arrow's shaft:  Center at pad center, end point at pad edge
      pointEndX = centerX - (int)(rect.getWidth() / arrowSizeFactor);
      pointEndY = centerY;

      // the shaft doesn't go all the way to the point because with larger stroke sizes, it extends past the point
      // and looks bad.  So we end the shaft right where the point triangle is to begin
      shaftEndX = pointEndX + arrowHeadSize;
      shaftEndY = centerY;

      // now the diagonal lines which define the tip of the arrow
      // uppper line "/"
      startLeftX = pointEndX + arrowHeadSize;
      startLeftY = pointEndY - arrowHeadSize;

      // lower line "\"
      startRightX = pointEndX + arrowHeadSize;
      startRightY = pointEndY + arrowHeadSize;
    }
    else if (orientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN))
    {
      // arrow starts from West and Points to East (RIGHT ARROW)
      // draw three lines:  First is arrow's shaft:  Center at pad center, end point at pad edge
      pointEndX = centerX + (int)(rect.getWidth() / arrowSizeFactor);
      pointEndY = centerY;

      // the shaft doesn't go all the way to the point because with larger stroke sizes, it extends past the point
      // and looks bad.  So we end the shaft right where the point triangle is to begin
      shaftEndX = pointEndX - arrowHeadSize;
      shaftEndY = centerY;

      // now the diagonal lines which define the tip of the arrow
      // uppper line "\"
      startLeftX = pointEndX - arrowHeadSize;
      startLeftY = pointEndY - arrowHeadSize;

      // lower line "/"
      startRightX = pointEndX - arrowHeadSize;
      startRightY = pointEndY + arrowHeadSize;
    }
    else if (orientation.equals(PinOrientationAfterRotationEnum.NOT_APPLICABLE))
      return;  // no arrow to draw here!
    // here is where we'll handle the NON ORTHOGONAL arrows!
    else if (orientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_EAST_OF_PIN))
    {
      double degreesRotation = _packagePin.getLandPatternPad().getDegreesRotation();
      // this is first quadrant, angles between 0 and 90
      double newRotation = degreesRotation % 90;
      double radians = Math.toRadians(-newRotation);

      Shape rotatedArrow = createRightArrow();
      // at this point the generalPath is a right arrow :  -->
      // we now need to ROTATE this arrow by the pad theta
      // build up the correct transform for this Pad
      AffineTransform trans = AffineTransform.getRotateInstance(radians, centerX, centerY);

      // apply the transform
      rotatedArrow = trans.createTransformedShape(rotatedArrow);

      Stroke origStroke = graphics2D.getStroke();
      Color origColor = graphics2D.getColor();
      graphics2D.setColor(LayerColorEnum.ORIENTATION_ARROW_COLOR.getColor()); // pastel green
      graphics2D.setStroke(new BasicStroke(strokeSize, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
      graphics2D.draw(rotatedArrow);
      graphics2D.fill(rotatedArrow);  // the fill goes nice and square, the draw edges round.  No biggie.  Fix later.
      graphics2D.setStroke(origStroke);
      graphics2D.setColor(origColor);
      return;
    }
    else if (orientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_WEST_OF_PIN))
    {
      double degreesRotation = _packagePin.getLandPatternPad().getDegreesRotation();
      // this is second quadrant, angles between 90 and 180
      double newRotation = (degreesRotation % 90) + 90.0;
      double radians = Math.toRadians(-newRotation);
      Shape rotatedArrow = createRightArrow();
      // at this point the generalPath is a right arrow :  -->
      // we now need to ROTATE this arrow by the pad theta
      // build up the correct transform for this Pad
      AffineTransform trans = AffineTransform.getRotateInstance(radians, centerX, centerY);

      // apply the transform
      rotatedArrow = trans.createTransformedShape(rotatedArrow);

      Stroke origStroke = graphics2D.getStroke();
      Color origColor = graphics2D.getColor();
      graphics2D.setColor(LayerColorEnum.ORIENTATION_ARROW_COLOR.getColor()); // pastel green
      graphics2D.setStroke(new BasicStroke(strokeSize, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
      graphics2D.draw(rotatedArrow);
      graphics2D.fill(rotatedArrow);  // the fill goes nice and square, the draw edges round.  No biggie.  Fix later.
      graphics2D.setStroke(origStroke);
      graphics2D.setColor(origColor);
      return;

    }
    else if (orientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_WEST_OF_PIN))
    {
      double degreesRotation = _packagePin.getLandPatternPad().getDegreesRotation();
      // this is third quadrant, angles between 180 and 270
      double newRotation = (degreesRotation % 90) + 180.0;
      double radians = Math.toRadians(-newRotation);

      Shape rotatedArrow = createRightArrow();
      // at this point the generalPath is a right arrow :  -->
      // we now need to ROTATE this arrow by the pad theta
      // build up the correct transform for this Pad
      AffineTransform trans = AffineTransform.getRotateInstance(radians, centerX, centerY);

      // apply the transform
      rotatedArrow = trans.createTransformedShape(rotatedArrow);

      Stroke origStroke = graphics2D.getStroke();
      Color origColor = graphics2D.getColor();
      graphics2D.setColor(LayerColorEnum.ORIENTATION_ARROW_COLOR.getColor()); // pastel green
      graphics2D.setStroke(new BasicStroke(strokeSize, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
      graphics2D.draw(rotatedArrow);
      graphics2D.fill(rotatedArrow);  // the fill goes nice and square, the draw edges round.  No biggie.  Fix later.
      graphics2D.setStroke(origStroke);
      graphics2D.setColor(origColor);
      return;

    }
    else if (orientation.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_EAST_OF_PIN))
    {
      double degreesRotation = _packagePin.getLandPatternPad().getDegreesRotation();
      // this is forth quadrant, angles between 270 and 360
      double newRotation = (degreesRotation % 90) + 270.0;
      double radians = Math.toRadians(-newRotation);

      Shape rotatedArrow = createRightArrow();
      // at this point the generalPath is a right arrow :  -->
      // we now need to ROTATE this arrow by the pad theta
      // build up the correct transform for this Pad
      AffineTransform trans = AffineTransform.getRotateInstance(radians, centerX, centerY);

      // apply the transform
      rotatedArrow = trans.createTransformedShape(rotatedArrow);

      Stroke origStroke = graphics2D.getStroke();
      Color origColor = graphics2D.getColor();
      graphics2D.setColor(LayerColorEnum.ORIENTATION_ARROW_COLOR.getColor()); // pastel green
      graphics2D.setStroke(new BasicStroke(strokeSize, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
      graphics2D.draw(rotatedArrow);
      graphics2D.fill(rotatedArrow);  // the fill goes nice and square, the draw edges round.  No biggie.  Fix later.
      graphics2D.setStroke(origStroke);
      graphics2D.setColor(origColor);
      return;
    }
    else
      Assert.expect(false, "Expect a N, S, E, W or NA arrow");

    // if the pads are being drawn, make the component outline a wider
    Stroke origStroke = graphics2D.getStroke();
    Color origColor = graphics2D.getColor();

    //new Color(128,128,255));  // steel blue  nice color, save for later
    graphics2D.setColor(LayerColorEnum.ORIENTATION_ARROW_COLOR.getColor()); // pastel green
    graphics2D.setStroke(new BasicStroke(strokeSize, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));

    graphics2D.drawLine(centerX, centerY, shaftEndX, shaftEndY);  // shaft

    GeneralPath gp = new GeneralPath();
    gp.moveTo(pointEndX, pointEndY);
    gp.lineTo(startLeftX, startLeftY);
    gp.lineTo(startRightX, startRightY);
    gp.closePath();

    graphics2D.fill(gp);

//    graphics2D.drawLine(startLeftX, startLeftY, endX, endY);
//    graphics2D.drawLine(startRightX, startRightY, endX, endY);

    graphics2D.setStroke(origStroke);
    graphics2D.setColor(origColor);

  }

  /**
   * Creates an arrow pointing to the right.  The length of the arrow will be the longest
   * it can be, either using the width or height of the pad, whichever is longer.
   * This arrow is rotated outside this method to line up with a non-orthogonal pad, so a longer
   * arrow is best.
   * @author Andy Mechtenberg
   */
  private Shape createRightArrow()
  {
    double arrowSizeFactor = 2.3;  // A factor of 2 is half the size of the pad.  2.3 is slightly less.
    Shape padShape = getShape();
    Rectangle rect = padShape.getBounds();
    int centerX = (int)rect.getCenterX();
    int centerY = (int)rect.getCenterY();
    int width = (int)rect.getWidth();
    int height = (int)rect.getHeight();
    int arrowHeadSize = (int)Math.max(rect.getWidth(), rect.getHeight()) / 8;

    int pointEndX = 0, pointEndY = 0;
    int startLeftX = 0, startLeftY = 0;
    int startRightX = 0, startRightY = 0;

    if (width > height)
    {
      pointEndX = centerX + (int)(width / arrowSizeFactor);
    }
    else
      pointEndX = centerX + (int)(height / arrowSizeFactor);

    pointEndY = centerY;

    // now the diagonal lines which define the tip of the arrow
    // uppper line "\"
    startLeftX = pointEndX - arrowHeadSize;
    startLeftY = pointEndY - arrowHeadSize;

    // lower line "/"
    startRightX = pointEndX - arrowHeadSize;
    startRightY = pointEndY + arrowHeadSize;

    // draw the arrow
    GeneralPath gp = new GeneralPath();
    gp.moveTo(centerX, centerY);
    gp.lineTo(pointEndX, pointEndY);
    gp.lineTo(startLeftX, startLeftY);
    gp.lineTo(startRightX, startRightY);
    gp.lineTo(pointEndX, pointEndY);

    return gp;
  }

  /**
   * Return the name of the panel
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    String packagePinDesc = _packagePin.getName() + " " + _packagePin.getJointTypeEnum().toString();

    return packagePinDesc;
  }
}
