package com.axi.v810.gui.drawCad;

import java.awt.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testProgram.*;

/**
 * This class is responsible for drawing the FocusRegionRenderer outline.
 *
 * @author Andy Mechtenberg
 */
public class FocusRegionRenderer extends ShapeRenderer
{
  private FocusRegion _focusRegion;

  /**
   * @author Andy Mechtenberg
   */
  FocusRegionRenderer(FocusRegion focusRegion)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(focusRegion != null);

    _focusRegion = focusRegion;

    refreshData();
  }

  /**
   * Return the name of the board
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    return _focusRegion.toString();
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void refreshData()
  {
    Assert.expect(_focusRegion != null);

    Shape region =_focusRegion.getRectangleRelativeToPanelInNanometers().getRectangle2D();

    initializeShape(region);
  }

  /**
   * @return FocusRegion object associated with renderer
   * @author Andy Mechtenberg
   */
  public FocusRegion getFocusRegion()
  {
    Assert.expect(_focusRegion != null);

    return _focusRegion;
  }
}
