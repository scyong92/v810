package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.util.List;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import java.util.*;

/**
 * This class contains the information to highlight pads.
 * It contains a list of Pad instance object and the highlight color object
 * @author Andy Mechtenberg
 */
public class HighlightAlignmentItems
{
  private List<Pad> _pads;
  private List<Fiducial> _fiducials;
  private Color _highlightColor;

  /**
   * @author Andy Mechtenberg
   */
  public HighlightAlignmentItems(List<Pad> pads, List<Fiducial> fiducials, Color highlightColor)
  {
    Assert.expect(pads != null);
    Assert.expect(highlightColor != null);

    _pads = pads;
    _fiducials = fiducials;
    _highlightColor = highlightColor;
  }

  /**
   * @author Andy Mechtenberg
   */
  public List<Pad> getPads()
  {
    return _pads;
  }

  /**
   * @author Andy Mechtenberg
   */
  public List<Fiducial> getFiducials()
  {
    return _fiducials;
  }

  /**
   * @author Andy Mechtenberg
   */
  public Color getHighlightColor()
  {
    return _highlightColor;
  }
}
