package com.axi.v810.gui.drawCad;

import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;


/**
 * This class sets up the GraphicsEngine for a particular LandPattern so the
 * GraphicsEngine can draw the LandPattern's CAD graphics
 *
 * @author Bill Darbie
 */
public class CreateCadLandPatternGraphicsEngineSetup implements Observer
{
  private static final int _BORDER_SIZE_IN_PIXELS = 50;
  private static final int _landPatternPadLayer = JLayeredPane.DEFAULT_LAYER.intValue();

  private GraphicsEngine _graphicsEngine;
  private LandPattern _landPattern;
  
  private CreateNewCadPanel _drawCadPanel;
  
  private Map<LandPatternPad, LandPatternPadRenderer> _landPatternPadToLandPatternPadRendererMap = new HashMap<LandPatternPad, LandPatternPadRenderer>();
  private boolean _graphicsAreActive = true;

  /**
   * @author Kee Chin Seong
   */
  CreateCadLandPatternGraphicsEngineSetup(CreateNewCadPanel drawCadPanel, GraphicsEngine graphicsEngine)
  {
    Assert.expect(drawCadPanel != null);
    Assert.expect(graphicsEngine != null);
    _drawCadPanel = drawCadPanel;
    _graphicsEngine = graphicsEngine;

    _graphicsEngine.setPixelBorderSize(_BORDER_SIZE_IN_PIXELS);
  }
  
 /**
   * @author Kee Chin Seong
   */
  void displayLandPattern(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);
    _landPattern = landPattern;

    populateLayersWithRenderers();
    setColors();
    setVisibility();
    setLayersToUseWhenFittingToScreen();

    _graphicsEngine.setAxisInterpretation(true, false);

    MathUtilEnum displayUnits = _landPattern.getPanel().getProject().getDisplayUnits();
    double measureFactor = 1.0/MathUtil.convertUnits(1.0, displayUnits, MathUtilEnum.NANOMETERS);
    _graphicsEngine.setMeasurementInfo(measureFactor, MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits),
       MeasurementUnits.getMeasurementUnitString(displayUnits));

    // add this as an observer of the datastore now that the engine is populated with renderers
    ProjectObservable.getInstance().addObserver(this);
    // add this as an aboserver of the gui
    GuiObservable.getInstance().addObserver(this);
    // add this as an observer of the engine
    _graphicsEngine.getSelectedRendererObservable().addObserver(this);

  }

  /**
   * @author Kee Chin Seong
   */
  void reset()
  {
    _graphicsEngine.reset();
    _landPatternPadToLandPatternPadRendererMap.clear();
    _landPattern = null;
    ProjectObservable.getInstance().deleteObserver(this);
    GuiObservable.getInstance().deleteObserver(this);
    _graphicsEngine.getSelectedRendererObservable().deleteObserver(this);

    _graphicsAreActive = false;
  }

  /**
   * Set up the layeredPanel for drawing packages
   * @author Kee Chin Seong
   */
  public GraphicsEngine getGraphicsEngine()
  {
    Assert.expect(_graphicsEngine != null);

    return _graphicsEngine;
  }

  /**
   * @author Kee Chin Seong
   */
  private void populateLayersWithRenderers()
  {
    Assert.expect(_landPattern != null);
    _graphicsEngine.reset();
    _landPatternPadToLandPatternPadRendererMap.clear();
    for (LandPatternPad landPatternPad: _landPattern.getLandPatternPads())
    {
      LandPatternPadRenderer landPatternPadRenderer = new LandPatternPadRenderer(landPatternPad);
      _graphicsEngine.addRenderer(_landPatternPadLayer, landPatternPadRenderer);
      _landPatternPadToLandPatternPadRendererMap.put(landPatternPad, landPatternPadRenderer);
    }
    _graphicsAreActive = true;
  }

  /**
   * Set up the proper colors for the layers
   * Bill Kee Chin Seong
   */
  private void setVisibility()
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.setVisible(_landPatternPadLayer, true);
  }

  /**
   * Set up the proper colors for the layers
   * Kee Chin Seong
   */
  private void setColors()
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.setForeground(_landPatternPadLayer, LayerColorEnum.SURFACEMOUNT_PAD_COLOR.getColor());
  }

  /**
   * Set up which layers to use when figuring out how big the whole deal is
   * Kee Chin Seong
   */
  private void setLayersToUseWhenFittingToScreen()
  {
    Collection<Integer> layers = new ArrayList<Integer>(1);
    layers.add(new Integer(_landPatternPadLayer));
    _graphicsEngine.setLayersToUseWhenFittingToScreen(layers);
  }

  /**
   * @author Kee Chin Seong
   */
  public Integer getLandPatternPadLayer()
  {
    return new Integer(_landPatternPadLayer);
  }

  /**
   * @author Kee Chin Seong
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if(observable instanceof ProjectObservable)
        {
          if(argument instanceof ProjectChangeEvent)
          {
            // no point in servicing any project events if the graphics are not active at this time.
            if (_graphicsAreActive)
            {
              ProjectChangeEvent event = (ProjectChangeEvent)argument;
              ProjectChangeEventEnum eventEnum = ((ProjectChangeEvent)argument).getProjectChangeEventEnum();

              if (eventEnum instanceof LandPatternPadEventEnum || eventEnum instanceof ThroughHoleLandPatternPadEventEnum)
              {
                if (eventEnum.equals(LandPatternPadEventEnum.CREATE_OR_DESTROY) || eventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE))
                {
                  Object oldValue = event.getOldValue();
                  Object newValue = event.getNewValue();
                  if (newValue != null) // create case
                  {
                    Assert.expect(oldValue == null);
                    Assert.expect(newValue instanceof LandPatternPad);
                    LandPatternPad landPatternPad = (LandPatternPad)newValue;
                    LandPatternPadRenderer landPatternPadRend = new LandPatternPadRenderer(landPatternPad);
                    LandPatternPadRenderer prev = _landPatternPadToLandPatternPadRendererMap.put(landPatternPad, landPatternPadRend);
                    if (prev != null)
                      _graphicsEngine.removeRenderer(prev);
                    _graphicsEngine.addRenderer(_landPatternPadLayer, landPatternPadRend);
                    _graphicsEngine.validateData(_landPatternPadLayer);
                    _graphicsEngine.fitGraphicsToScreen();
                  }
                  else if (oldValue != null) // destroy case
                  {
                    Assert.expect(newValue == null);
                    Assert.expect(oldValue instanceof LandPatternPad);
                    LandPatternPad landPatternPad = (LandPatternPad)oldValue;
                    LandPatternPadRenderer prev = _landPatternPadToLandPatternPadRendererMap.remove(landPatternPad);
                    if (prev != null)
                    {
                      com.axi.guiUtil.Renderer renderer = getRendererForLandPatternPad(landPatternPad);
                      Assert.expect(renderer == prev);
                      _graphicsEngine.removeRenderer(renderer); // this also repaints the deleted renderer (or lack of)
                      _graphicsEngine.fitGraphicsToScreen();
                    }
                  }
                  else
                  {
                    Assert.expect(false, "Both oldValue and newValue are null for LandPatternPadEvenEnum.CREATE_OR_DESTROY event");
                    return;
                  }
                }
                else if (eventEnum.equals(LandPatternPadEventEnum.PAD_ONE) == false &&
                         eventEnum.equals(LandPatternPadEventEnum.PITCH) == false &&
                         eventEnum.equals(LandPatternPadEventEnum.INTERPAD_DISTANCE) == false &&
                         eventEnum.equals(LandPatternPadEventEnum.PAD_ORIENTATION) == false && 
                         eventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE_LIST) == false)
                {
                  LandPatternPad landPatternPad = (LandPatternPad)event.getSource();

                  com.axi.guiUtil.Renderer landPatternPadRenderer = getRendererForLandPatternPad(landPatternPad);
                  landPatternPadRenderer.validateData();
                  if (eventEnum.equals(LandPatternPadEventEnum.NAME))
                  {
                    _drawCadPanel.setDescription(getDescription((LandPatternPadRenderer)landPatternPadRenderer));
                  }
                }
                else if (eventEnum.equals(LandPatternPadEventEnum.PAD_ONE))
                {
                  LandPatternPad landPatternPad = (LandPatternPad)event.getSource();
                  for (LandPatternPad lp : landPatternPad.getLandPattern().getLandPatternPads())
                  {
                    com.axi.guiUtil.Renderer landPatternPadRenderer = getRendererForLandPatternPad(lp);
                    landPatternPadRenderer.validateData();
                  }
                }
                else if(eventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE_LIST))
                {
                  Object oldValue = event.getOldValue();
                  Object newValue = event.getNewValue();
                  
                  if (newValue != null) // create case
                  {
                    Assert.expect(oldValue == null);
                    Assert.expect(newValue instanceof ArrayList );
                    java.util.List <LandPatternPad> landPatternPads = (java.util.ArrayList <LandPatternPad>)newValue;
                    
                    for (LandPatternPad landPatternPad : landPatternPads)
                    {
                      LandPatternPadRenderer landPatternPadRend = new LandPatternPadRenderer(landPatternPad);
                      LandPatternPadRenderer prev = _landPatternPadToLandPatternPadRendererMap.put(landPatternPad, landPatternPadRend);
                      if (prev != null)
                        _graphicsEngine.removeRenderer(prev);
                      _graphicsEngine.addRenderer(_landPatternPadLayer, landPatternPadRend);
                    }
                    _graphicsEngine.validateData(_landPatternPadLayer);
                    _graphicsEngine.fitGraphicsToScreen();
                  }
                  else if (oldValue != null) // destroy case
                  {
                    Assert.expect(newValue == null);
                    Assert.expect(oldValue instanceof ArrayList );
                    
                    java.util.List <LandPatternPad> landPatternPads = (java.util.ArrayList <LandPatternPad>)oldValue;

                    for (LandPatternPad landPatternPad : landPatternPads)
                    {
                      LandPatternPadRenderer prev = _landPatternPadToLandPatternPadRendererMap.remove(landPatternPad);
                      if (prev != null)
                      {
                        com.axi.guiUtil.Renderer renderer = getRendererForLandPatternPad(landPatternPad);
                        Assert.expect(renderer == prev);
                        _graphicsEngine.removeRenderer(renderer); // this also repaints the deleted renderer (or lack of)
                      }
                    }
                    _graphicsEngine.fitGraphicsToScreen();
                  }
                }
              }
            }
          }
        }
        else if (observable instanceof SelectedRendererObservable)
        {
          // this comes from the GraphicsEngine whenever a group of "things" has been selected by the user
          // this code will highlight (turn white) the selected items.  Since this graphics engine setup only
          // cares about LandPatternPads, that's all this observer will pay attention to.
          boolean descriptionIsSet = false;
          _graphicsEngine.clearSelectedRenderers();
          /** Warning "unchecked cast" approved for cast from Object type when sent from SelectedRendererObservable notifyAll.  */
          for (com.axi.guiUtil.Renderer renderer :
               ((Collection<com.axi.guiUtil.Renderer>)argument)) {
            // if it's a PackagePin, highlight it
            if (renderer instanceof LandPatternPadRenderer)
            {
              _graphicsEngine.setSelectedRenderer(renderer);

              // if more than one land pattern pad was selected, we set the status bar descriptive text to nothing
              // so, we'll set it the first time, and if we get another, clear it out.
              if (descriptionIsSet)
              {
                _drawCadPanel.setDescription(" ");  // if we use null, the entire status bar goes away, so we'll use an empty space instead
              }
              else
              {
                _drawCadPanel.setDescription(getDescription((LandPatternPadRenderer)renderer));
                descriptionIsSet = true;
              }
            }
          }
        }
        else if(observable instanceof GuiObservable)
        {
          handleGuiEvent((GuiEvent)argument);
        }
        else
        {
          Assert.expect(false, "LandPatternGraphicsEngineSetup: No update code for observable: " + observable.getClass().getName());
        }
      }
    });
  }

  /**
   * @author Kee Chin Seong
   */
  private void handleGuiEvent(GuiEvent guiEvent)
  {
    Assert.expect(guiEvent != null);

    GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
    if(guiEventEnum instanceof SelectionEventEnum)
    {
      SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
      if(selectionEventEnum.equals(SelectionEventEnum.LANDPATTERN_PAD))
      {
        _graphicsEngine.clearSelectedRenderers();
        /** Warning "unchecked cast" approved for cast from Object types */
        List<LandPatternPad> pads = (List<LandPatternPad>)guiEvent.getSource();
        List<com.axi.guiUtil.Renderer> renderers = getRenderers(pads);
        _graphicsEngine.setSelectedRenderers(renderers);
        if (renderers.size() == 1)
        {
          com.axi.guiUtil.Renderer renderer = renderers.get(0);
          Assert.expect(renderer instanceof LandPatternPadRenderer);
          _drawCadPanel.setDescription(getDescription((LandPatternPadRenderer)renderer));
        }
        else // clear the status line for multiple selected pads
        {
          _drawCadPanel.setDescription(" ");
        }
      }   
    }
  }

  /**
   * @author Kee Chin Seong
   */
  private String getDescription(LandPatternPadRenderer renderer)
  {
    Assert.expect(renderer != null);

    LandPatternPad landPatternPad = ((LandPatternPadRenderer)renderer).getLandPatternPad();
    String description;
    if (landPatternPad instanceof ThroughHoleLandPatternPad)
    {
      ThroughHoleLandPatternPad thLandPatternPad = (ThroughHoleLandPatternPad)landPatternPad;
      MathUtilEnum displayUnits = thLandPatternPad.getLandPattern().getPanel().getProject().getDisplayUnits();
      description = StringLocalizer.keyToString(new LocalizedString("GUI_TH_LAND_PATTERN_PAD_SUMMARY_KEY",
                                                                    new Object[]
                                                                    {landPatternPad.getName(),
                                                                    MathUtil.convertUnits(thLandPatternPad.getHoleCoordinateInNanoMeters().getX(),
                                                                                          MathUtilEnum.NANOMETERS,
                                                                                          displayUnits),
                                                                    MathUtil.convertUnits(thLandPatternPad.getHoleCoordinateInNanoMeters().getY(),
                                                                                          MathUtilEnum.NANOMETERS,
                                                                                          displayUnits)}));
    }
    else
      description = StringLocalizer.keyToString(new LocalizedString("GUI_LAND_PATTERN_PAD_SUMMARY_KEY", new Object[]
                                                                    {landPatternPad.getName()}));
    return description;
  }
  /**
   * Given a list of land pattern pads, return their corresponding renderers.
   * @param landPatternPads a list of LandPatternPads
   * @author Kee Chin Seong
   */
  private List<com.axi.guiUtil.Renderer> getRenderers(List<LandPatternPad> landPatternPads)
  {
    Assert.expect(landPatternPads != null);

    List<com.axi.guiUtil.Renderer> renderers = new ArrayList<com.axi.guiUtil.Renderer>();
    for (LandPatternPad pad : landPatternPads)
    {
      com.axi.guiUtil.Renderer renderer = (com.axi.guiUtil.Renderer)_landPatternPadToLandPatternPadRendererMap.get(pad);
      Assert.expect(renderer != null);
      renderers.add(renderer);
    }

    return renderers;
  }

  /**
   * @author Kee Chin Seongg
   */
  private com.axi.guiUtil.Renderer getRendererForLandPatternPad(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);
    Collection<com.axi.guiUtil.Renderer> renderers =
      _graphicsEngine.getRenderers(getLandPatternPadLayer().intValue());
    for (com.axi.guiUtil.Renderer renderer : renderers)
    {
      if (renderer instanceof LandPatternPadRenderer)
      {
        LandPatternPadRenderer tempPadRenderer = (LandPatternPadRenderer)renderer;
        if (landPatternPad == tempPadRenderer.getLandPatternPad())
        {
          return tempPadRenderer;
        }
      }
      else
        Assert.expect(false, "Unexpected renderer: " + renderer);
    }
    Assert.expect(false, "Could not find renderer for land pattern pad " + landPatternPad);
    return null;

  }

  /**
   * @author Kee Chin Seong 
   * For Debug Purpose
   */
  private void debugLandPatternPadEvent(ProjectChangeEvent event)
  {
    Assert.expect(event != null);

    LandPatternPad landPatternPad = (LandPatternPad)event.getSource();
    ProjectChangeEventEnum eventEnum = ((ProjectChangeEvent)event).getProjectChangeEventEnum();

    System.out.print("Received LPP event ");
    switch (eventEnum.getId())
    {
      case 0:
        System.out.print("CREATE_OR_DESTROY");
        break;
      case 1:
        System.out.print("ADD_OR_REMOVE");
        break;
      case 2:
        System.out.print("LAND_PATTERN");
        break;
      case 3:
        System.out.print("NAME");
        break;
      case 4:
        System.out.print("COORDINATE");
        break;
      case 5:
        System.out.print("WIDTH");
        break;
      case 6:
        System.out.print("LENGTH");
        break;
      case 7:
        System.out.print("SHAPE_ENUM");
        break;
      case 8:
        System.out.print("DEGREES_ROTATION");
        break;
      case 9:
        System.out.print("PITCH");
        break;
      case 10:
        System.out.print("INTERPAD_DISTANCE");
        break;
      case 11:
        System.out.print("PAD_ORIENTATION");
        break;
      case 12:
        System.out.print("PAD_ONE");
        break;
    }

    System.out.println(" for LPP: " + landPatternPad.getName() + " of LP: " + landPatternPad.getLandPattern().getName());
  }
}
