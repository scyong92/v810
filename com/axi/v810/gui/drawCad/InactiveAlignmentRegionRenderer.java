package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;

/**
 * This class is responsible for drawing the VerificationRegions.
 *
 * @author Andy Mechtenberg
 */
public class InactiveAlignmentRegionRenderer extends ShapeRenderer
{
  private com.axi.v810.business.panelDesc.Panel _panel = null;
  private com.axi.v810.business.panelDesc.Board _board = null;
  private boolean _topRegion;
  private Color _renderColor; // translucent gray

  /**
   * @author Andy Mechtenberg
   */
  InactiveAlignmentRegionRenderer(com.axi.v810.business.panelDesc.Panel panel, boolean topSide)  // InactiveAlignmentRegionRenderer
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(panel != null);

    _panel = panel;
    _board = null;
    _topRegion = topSide;
    Color gray = Color.gray;
    _renderColor = new Color(gray.getRed(), gray.getGreen(), gray.getBlue(), 128);  // translucent gray
    refreshData();
  }

  /**
   * @author Andy Mechtenberg
   */
  InactiveAlignmentRegionRenderer(com.axi.v810.business.panelDesc.Board board, boolean topSide)  // InactiveAlignmentRegionRenderer
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(board != null);

    _board = board;
    _panel = null;
    _topRegion = topSide;
    Color gray = Color.gray;
    _renderColor = new Color(gray.getRed(), gray.getGreen(), gray.getBlue(), 128);  // translucent gray
    refreshData();
  }

  /**
   * @author Andy Mechtenberg
   */
  void setTopRegion(boolean topRegion)
  {
    _topRegion = topRegion;
  }

  /**
   * Color the region it's proper color
   * @author Andy Mechtenberg
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    Color origColor = graphics2D.getColor();

    graphics2D.setColor(_renderColor);

    graphics2D.fill(getShape());

    graphics2D.setColor(origColor);
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void refreshData()
  {
    int splitLocationInNanoMeters = 0;
    Shape panelShape = null;
    if(_panel != null)
    {
      splitLocationInNanoMeters = _panel.getPanelSettings().getAlignmentMaxImageableAreaLengthInNanoMeters();
      panelShape = _panel.getShapeInNanoMeters();
    }
    else if(_board != null)
    {
      splitLocationInNanoMeters = _board.getAlignmentMaxImageableAreaLengthInNanoMeters();
      panelShape = _board.getShapeRelativeToPanelInNanoMeters();
    }
    else
      Assert.expect(false, "no Panel or Board assigned to this renderer!");
    
    Rectangle2D panelBounds = panelShape.getBounds2D();
    double boundsX = panelBounds.getX();
    double boundsWidth = panelBounds.getWidth();
    double boundsHeight = panelBounds.getHeight();

    double yCenter;
    if (_topRegion)
      yCenter = panelBounds.getHeight() - splitLocationInNanoMeters;
    else
      yCenter = splitLocationInNanoMeters;

//    Shape horizontalLine = new Line2D.Double(boundsX, yCenter, boundsX + boundsWidth, yCenter);
    Shape grayRectangle;
    if (_topRegion)
      grayRectangle = new Rectangle2D.Double(panelBounds.getMinX(), panelBounds.getMinY(), boundsWidth, yCenter);
    else
      grayRectangle = new Rectangle2D.Double(panelBounds.getMinX(), panelBounds.getMinY() + yCenter, boundsWidth, boundsHeight - splitLocationInNanoMeters);


//    GeneralPath generalPath = new GeneralPath();
//    generalPath.append(horizontalLine, true);
//    generalPath.append(grayRectangle,true);

    initializeShape(grayRectangle);
  }
}
