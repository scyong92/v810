package com.axi.v810.gui.drawCad;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * @author huai-en.janan-ezekie
 */
public class UntestableAreaRenderer extends ShapeRenderer
{

  private Panel _panel;
  private boolean _isStaticImage = false;

  /**
   * @author huai-en.janan-ezekie
   */
  public UntestableAreaRenderer(Panel panel)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(panel != null);
    _panel = panel;
    this.setFillShape(true);
    refreshData();
  }

  /**
   * @author huai-en.janan-ezekie
   */
  public String toString()
  {
    return _panel.getProject().getName();
  }

  /**
   * @author huai-en.janan-ezekie
   */
  public Panel getPanel()
  {
    Assert.expect(_panel != null);

    return _panel;
  }

  /**
   * @author huai-en.janan-ezekie
   */
  protected void refreshData()
  {
    Assert.expect(_panel != null);

    initializeShape(_panel.getRightUntestableArea(), _panel.getLeftUntestableArea());
  }

}
