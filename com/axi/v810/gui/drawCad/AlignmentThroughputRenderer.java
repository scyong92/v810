package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * This class is responsible for drawing the optimal region to place alignment points
 * It's based on an alignment group, the topmost or bottom most, and draws a panel width
 * rectangle of certain height to indicate the best region for a second group based on performance only
 *
 * @author Andy Mechtenberg
 */
public class AlignmentThroughputRenderer extends ShapeRenderer
{
  private com.axi.v810.business.panelDesc.Panel _panel = null;
  private com.axi.v810.business.panelDesc.Board _board = null;

  private boolean _topRegion = true;
  private boolean _topShape;
  private Shape _shape;
  private Color _renderColor; // translucent blue

  private int _performanceWidthInNanoMeters = 1000 * 24500; // one inch, hardcoded

  /**
   * @author Andy Mechtenberg
   */
  AlignmentThroughputRenderer(com.axi.v810.business.panelDesc.Panel panel, boolean topShape)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(panel != null);

    _panel = panel;
    _board = null;
    _topShape = topShape;

    Color happyColor = Color.white;//new Color(0, 0, 255);
    _renderColor =  happyColor;//new Color(happyColor.getRed(), happyColor.getGreen(), happyColor.getBlue(), 100);
    refreshData();
  }

  /**
   * @author Andy Mechtenberg
   */
  AlignmentThroughputRenderer(com.axi.v810.business.panelDesc.Board board, boolean topShape)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(board != null);

    _board = board;
    _panel = null;
    _topShape = topShape;

    Color happyColor = Color.white;//new Color(0, 0, 255);
    _renderColor =  happyColor;//new Color(happyColor.getRed(), happyColor.getGreen(), happyColor.getBlue(), 100);
    refreshData();
  }

  /**
   * @author Andy Mechtenberg
   */
  void setTopRegion(boolean topRegion)
  {
    _topRegion = topRegion;
  }

  /**
   * Color the region it's proper color
   * @author Andy Mechtenberg
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    Color origColor = graphics2D.getColor();
    Stroke origStroke = graphics2D.getStroke();

    graphics2D.setStroke(new BasicStroke(5));
    graphics2D.setColor(_renderColor);

//    graphics2D.fill(getShape());
    graphics2D.draw(getShape());

    graphics2D.setColor(origColor);
    graphics2D.setStroke(origStroke);
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void refreshData()
  {
//    _topShape = getUpperShape();
    
//    // Added by Jack Hwee - to avoid crash when AlignmentThroughputRenderer calls refreshData(),
//    //                      if testSubProgram is empty. 
//    if(_panel != null)
//    {
//      if (_panel.getProject().getTestProgram().getFilteredTestSubPrograms().isEmpty() == false) 
//      {
//        initializeShape(new Rectangle2D.Double(0, 0, 0, 0));
//        return;
//      }
//    }
//    else if(_board != null)
//    {
//      if (_board.getPanel().getProject().getTestProgram().getFilteredTestSubPrograms().isEmpty() == false) 
//      {
//        initializeShape(new Rectangle2D.Double(0, 0, 0, 0));
//        return;
//      }
//    }
      
    if (_topShape)
      _shape = getUpperShape();
    else
      _shape = getLowerShape();

    initializeShape(_shape);
//    // if no pads or fiducials at all, then no rect -- render nothing
//    if (_alignmentGroup.isEmpty())
//    {
//      initializeShape(new Rectangle2D.Double(0, 0, 0, 0));
//      return;
//    }
//
//    Shape region = _programGeneration.getAllowableAlignmentRegion(_panel.getProject(), _alignmentGroup).getRectangle2D();
//    Rectangle2D bounds = region.getBounds2D();
//    double yLocation = bounds.getCenterY();
//    Shape panelShape = _panel.getShapeInNanoMeters();
//    Rectangle2D panelBounds = panelShape.getBounds2D();
//
//    double boundsWidth = panelBounds.getWidth();
//    double maxPerformanceHeightInNanoMeters = 1000 * 25400;  // one INCH (1000 mils * 25400 miles per nanometer)
//
//    Shape performanceRectangle = new Rectangle2D.Double(0, yLocation - (maxPerformanceHeightInNanoMeters/2), boundsWidth, maxPerformanceHeightInNanoMeters);
//
//    initializeShape(performanceRectangle);
  }

  /**
   * @author Andy Mechtenberg
   */
  private Shape createShapeFromAlignmentGroup(AlignmentGroup alignmentGroup)
  {
    Shape region = null;
    Shape panelShape = null;

    if(_panel != null)
    {
      region = _panel.getProject().getTestProgram().getAlignmentRegion(alignmentGroup).getAllowableAlignmentRegionInNanoMeters().getRectangle2D();
      panelShape = _panel.getShapeInNanoMeters();
    }
    else if(_board != null)
    {
      region = _board.getPanel().getProject().getTestProgram().getAlignmentRegion(alignmentGroup).getAllowableAlignmentRegionInNanoMeters().getRectangle2D();
      panelShape = _board.getShapeRelativeToPanelInNanoMeters();
    }
    else
      Assert.expect(false, "No Panel or Board Assigned to Renderer");

    Rectangle2D bounds = region.getBounds2D();
    double yLocation = bounds.getCenterY();

    Rectangle2D panelBounds = panelShape.getBounds2D();

    double boundsWidth = panelBounds.getWidth();

    return new Rectangle2D.Double(panelBounds.getMinX(), yLocation - (_performanceWidthInNanoMeters / 2), boundsWidth, _performanceWidthInNanoMeters);
  }

  /**
   * @author Andy Mechtenberg
   */
  private Shape getUpperShape()
  {
    // find the top most alignment group
    AlignmentGroup topGroup = null;
    java.util.List<AlignmentGroup> alignmentGroups = null;

    Project project = null;

    if(_panel != null)
    {
      project = _panel.getProject();
    }
    else if(_board != null)
    {
      project = _board.getPanel().getProject();
    }
    else
      Assert.expect(false, "no Panel or Board assigned to this renderer!");

    Assert.expect(project.isTestProgramValidIgnoringAlignmentRegions());

    if (project.getTestProgram().isLongProgram())
    {
      if (_topRegion)
      {
        if(_panel != null)
          alignmentGroups = _panel.getPanelSettings().getRightAlignmentGroupsForLongPanel();
        else
          alignmentGroups = _board.getBoardSettings().getRightAlignmentGroupsForLongPanel();
      }
      else
      {
        if(_panel != null)
          alignmentGroups = _panel.getPanelSettings().getLeftAlignmentGroupsForLongPanel();
        else
          alignmentGroups = _board.getBoardSettings().getLeftAlignmentGroupsForLongPanel();
      }
    }
    else
    {
      if(_panel != null)
        alignmentGroups = _panel.getPanelSettings().getAlignmentGroupsForShortPanel();
      else
        alignmentGroups = _board.getBoardSettings().getAlignmentGroupsForShortPanel();
    }

    for(AlignmentGroup alignmentGroup : alignmentGroups)
    {
      if (alignmentGroup.isEmpty() || project.getTestProgram().isAlignmentGroupInvalidated(alignmentGroup))
        continue;
      if (topGroup == null)
        topGroup = alignmentGroup;
      else
      {
        Shape topShape = project.getTestProgram().getAlignmentRegion(alignmentGroup).getAllowableAlignmentRegionInNanoMeters().getRectangle2D();
        Shape otherShape = project.getTestProgram().getAlignmentRegion(alignmentGroup).getAllowableAlignmentRegionInNanoMeters().getRectangle2D();
        if (otherShape.getBounds2D().getCenterY() > topShape.getBounds2D().getCenterY())
          topGroup = alignmentGroup;
      }
    }
    if (topGroup == null)
      return new Rectangle2D.Double(0, 0, 0, 0);
    else
    {
      return createShapeFromAlignmentGroup(topGroup);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private Shape getLowerShape()
  {
    // find the top most alignment group
    AlignmentGroup bottomGroup = null;
    java.util.List<AlignmentGroup> alignmentGroups;

    Project project = null;

    if(_panel != null)
    {
      project = _panel.getProject();
    }
    else if(_board != null)
    {
      project = _board.getPanel().getProject();
    }
    else
      Assert.expect(false, "no Panel or Board assigned to this renderer!");

    Assert.expect(project.isTestProgramValidIgnoringAlignmentRegions());
    
    if (project.getTestProgram().isLongProgram())
    {
      if (_topRegion)
      {
        if(_panel != null)
          alignmentGroups = _panel.getPanelSettings().getRightAlignmentGroupsForLongPanel();
        else
          alignmentGroups = _board.getBoardSettings().getRightAlignmentGroupsForLongPanel();
      }
      else
      {
        if(_panel != null)
          alignmentGroups = _panel.getPanelSettings().getLeftAlignmentGroupsForLongPanel();
        else
          alignmentGroups = _board.getBoardSettings().getLeftAlignmentGroupsForLongPanel();
      }
    }
    else
    {
      if(_panel != null)
        alignmentGroups = _panel.getPanelSettings().getAlignmentGroupsForShortPanel();
      else
        alignmentGroups = _board.getBoardSettings().getAlignmentGroupsForShortPanel();
    }

    for (AlignmentGroup alignmentGroup : alignmentGroups)
    {
      if (alignmentGroup.isEmpty() || project.getTestProgram().isAlignmentGroupInvalidated(alignmentGroup))
        continue;
      if (bottomGroup == null)
        bottomGroup = alignmentGroup;
      else
      {
        Shape topShape = project.getTestProgram().getAlignmentRegion(bottomGroup).getAllowableAlignmentRegionInNanoMeters().getRectangle2D();
        Shape otherShape = project.getTestProgram().getAlignmentRegion(alignmentGroup).getAllowableAlignmentRegionInNanoMeters().getRectangle2D();
        if (otherShape.getBounds2D().getCenterY() < topShape.getBounds2D().getCenterY())
          bottomGroup = alignmentGroup;
      }
    }
    if (bottomGroup == null)
      return new Rectangle2D.Double(0, 0, 0, 0);
    else
    {
      return createShapeFromAlignmentGroup(bottomGroup);
    }
  }

}
