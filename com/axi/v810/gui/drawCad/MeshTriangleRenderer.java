/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.drawCad;

import java.awt.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

/**
 *
 * @author Jack Hwee
 */
public class MeshTriangleRenderer extends ShapeRenderer
{
  private int _line1BeginX;
  private int _line1EndX;
  private int _line1BeginY;
  private int _line1EndY;
  private int _line2BeginX;
  private int _line2EndX;
  private int _line2BeginY;
  private int _line2EndY;
  private int _line3BeginX;
  private int _line3EndX;
  private int _line3BeginY;
  private int _line3EndY;

  /**
   * @author Jack Hwee
   * 
   */
  protected MeshTriangleRenderer()
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);

    setLine(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
  }

  /**
   * @author Jack Hwee
   */
  void setLine(int line1BeginX, int line1EndX, int line1BeginY, int line1EndY,
                int line2BeginX, int line2EndX, int line2BeginY, int line2EndY,
                int line3BeginX, int line3EndX, int line3BeginY, int line3EndY)
  {
    Assert.expect(line1BeginX >= 0);
    Assert.expect(line1EndX >= 0);
    Assert.expect(line1BeginY >= 0);
    Assert.expect(line1EndY >= 0);
    Assert.expect(line2BeginX >= 0);
    Assert.expect(line2EndX >= 0);
    Assert.expect(line2BeginY >= 0);
    Assert.expect(line2EndY >= 0);
    Assert.expect(line3BeginX >= 0);
    Assert.expect(line3EndX >= 0);
    Assert.expect(line3BeginY >= 0);
    Assert.expect(line3EndY >= 0);

    _line1BeginX = line1BeginX;
    _line1EndX = line1EndX;
    _line1BeginY = line1BeginY;
    _line1EndY = line1EndY;
    
    _line2BeginX = line2BeginX;
    _line2EndX = line2EndX;
    _line2BeginY = line2BeginY;
    _line2EndY = line2EndY;
    
    _line3BeginX = line3BeginX;
    _line3EndX = line3EndX;
    _line3BeginY = line3BeginY;
    _line3EndY = line3EndY;

    refreshData();
  }

  /**
   * @author Jack Hwee
   */
  protected void refreshData()
  {   
    Shape meshLine1 = new Line2D.Double(_line1BeginX, _line1BeginY, _line1EndX, _line1EndY);
    Shape meshLine2 = new Line2D.Double(_line2BeginX, _line2BeginY, _line2EndX, _line2EndY);
    Shape meshLine3 = new Line2D.Double(_line3BeginX, _line3BeginY, _line3EndX, _line3EndY);
    GeneralPath generalPath = new GeneralPath();
    generalPath.append(meshLine1, false);
    generalPath.append(meshLine2, false);
    generalPath.append(meshLine3, false);

    initializeShape(generalPath);
  }

  /**
   * @author Jack Hwee
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    Color origColor = graphics2D.getColor();
    Stroke origStroke = graphics2D.getStroke();

    graphics2D.setColor(Color.YELLOW);
    float dash1[] = {10.0f};
    BasicStroke dashedStroke = new BasicStroke(2.0f,
                                               BasicStroke.CAP_BUTT,
                                               BasicStroke.JOIN_MITER,
                                               10.0f, dash1, 0.0f);
    graphics2D.setStroke(dashedStroke);

    graphics2D.draw(getShape());

    graphics2D.setColor(origColor);
    graphics2D.setStroke(origStroke);
  }
}
