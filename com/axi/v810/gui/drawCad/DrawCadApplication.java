package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.axi.guiUtil.*;

/**
 * @author Bill Darbie
 */
public class DrawCadApplication
{
  private boolean _packFrame = false;

  /**
   * @author Bill Darbie
   */
  public DrawCadApplication()
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        CadFrame frame = new CadFrame();
        DrawCadPanel cadPanel = new DrawCadPanel();
        frame.getContentPane().add(cadPanel);
        frame.setTitle("CAD Graphics");
        frame.setSize(new Dimension(800, 600));
        frame.addKeyListener(new KeyAdapter()
        {
          public void keyPressed(KeyEvent e)
          {
            int altXCode = KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.VK_ALT).getKeyCode();
            if (e.getKeyCode() == altXCode)
            {
              exitApp();
            }
          }
        });
        //Validate frames that have preset sizes
        //Pack frames that have useful preferred size info, e.g. from their layout
        if (_packFrame)
        {
          frame.pack();
        }
        else
        {
          frame.validate();
        }
        //Center the window
        SwingUtils.centerOnScreen(frame);
        frame.pack();
        cadPanel.scaleDrawing();
        frame.setVisible(true);
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  public void exitApp()
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        System.exit(0);
      }
    });
  }


  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception e)
        {
          e.printStackTrace();
        }
        new DrawCadApplication();
      }
    });
  }
}
