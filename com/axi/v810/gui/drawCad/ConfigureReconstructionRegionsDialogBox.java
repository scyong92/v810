package com.axi.v810.gui.drawCad;

import javax.swing.*;

import com.axi.util.*;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import java.awt.Color;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.border.BevelBorder;
import java.awt.FlowLayout;

/**
 * This is for internal use only. It aids the software developer in creating
 * Reconstruciton Regions, Focus Regions, slices, etc..
 * @author George A. David
 */
public class ConfigureReconstructionRegionsDialogBox extends JDialog
{
  private ControlToolBar _controlToolBar;
  private JPanel _selectReconstructionRegionTypesPanel = new JPanel();
  private JPanel _selectRegionsPanel = new JPanel();
  private Border border1 = BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140));
  private Border border2 = new TitledBorder(border1, "Select Reconstruction Regions");
  private Border border3 = BorderFactory.createEtchedBorder(EtchedBorder.RAISED, Color.white, new Color(148, 145, 140));
  private Border border4 = new TitledBorder(border3, "Select Regions");
  private GridLayout _selectReconstructionRegionsGridLayout = new GridLayout();
  private JCheckBox _verificationRegionsCheckBox = new JCheckBox();
  private JCheckBox _inspectionRegionsCheckBox = new JCheckBox();
  private JCheckBox _alignmentRegionsCheckBox = new JCheckBox();
  private GridLayout gridLayout1 = new GridLayout();
  private JCheckBox _reconstructionRectangleCheckBox = new JCheckBox();
  private JCheckBox _reconstructionRegionPadBoundsCheckBox = new JCheckBox();
  private JCheckBox _focusRegionCheckBox = new JCheckBox();
  private JPanel _reconstructionRegionPanel = new JPanel();
  private GridLayout gridLayout2 = new GridLayout();
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel _buttonPanel = new JPanel();
  private JButton _okButton = new JButton();
  private JPanel _mainPanel = new JPanel();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JPanel _irpBoundaryPanel = new JPanel();
  private ButtonGroup buttonGroup1 = new ButtonGroup();
  private JRadioButton _verificationIrpBoundariesRadioButton = new JRadioButton();
  private JRadioButton _inspectionIrpBoundariesRadioButton = new JRadioButton();
  private Border border5 = BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.white, Color.white,
      new Color(103, 101, 98), new Color(148, 145, 140));
  private Border border6 = new TitledBorder(border5, "Select IRP Boundaries");
  private JRadioButton _noneRadioButton = new JRadioButton();
  private FlowLayout flowLayout1 = new FlowLayout();

  /**
   * this constructor is for the designer only!
   * @author George A. David
   */
  public ConfigureReconstructionRegionsDialogBox()
  {
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }


  /**
   * @author George A. David
   */
  public ConfigureReconstructionRegionsDialogBox(ControlToolBar controlToolBar)
  {
    Assert.expect(controlToolBar != null);

    _controlToolBar = controlToolBar;
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    pack();
  }

  /**
   * @author George A. David
   */
  private void jbInit() throws Exception
  {
    border6 = new TitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED, Color.white,
        new Color(148, 145, 140)), "Select IRP Boundaries");
    this.getContentPane().setLayout(borderLayout1);
    _selectReconstructionRegionTypesPanel.setBorder(border2);
    _selectReconstructionRegionTypesPanel.setLayout(_selectReconstructionRegionsGridLayout);
    _selectReconstructionRegionsGridLayout.setColumns(1);
    _selectReconstructionRegionsGridLayout.setRows(3);
    _verificationRegionsCheckBox.setText("Verification Regions");
    _verificationRegionsCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        verificationRegionsCheckBox_actionPerformed(e);
      }
    });
    _inspectionRegionsCheckBox.setText("Inspection Regions");
    _inspectionRegionsCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        inspectionRegionsCheckBox_actionPerformed(e);
      }
    });
    _alignmentRegionsCheckBox.setText("Alignment Regions");
    _alignmentRegionsCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        alignmentRegionsCheckBox_actionPerformed(e);
      }
    });
    _selectRegionsPanel.setLayout(gridLayout1);
    gridLayout1.setColumns(1);
    gridLayout1.setRows(3);
    _reconstructionRectangleCheckBox.setText("Reconstruction Region Rectangle");
    _reconstructionRectangleCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        reconstructionRectangleCheckBox_actionPerformed(e);
      }
    });
    _reconstructionRegionPadBoundsCheckBox.setText("Reconstruction Region Pad Bounds");
    _reconstructionRegionPadBoundsCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        reconstructionRegionPadBoundsCheckBox_actionPerformed(e);
      }
    });
    _focusRegionCheckBox.setText("Focus Regions");
    _focusRegionCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        focusRegionCheckBox_actionPerformed(e);
      }
    });
    _selectRegionsPanel.setBorder(border4);
    _reconstructionRegionPanel.setLayout(gridLayout2);
    gridLayout2.setColumns(2);
    _okButton.setText("OK");
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    _mainPanel.setLayout(borderLayout2);
    _verificationIrpBoundariesRadioButton.setText("Verification IRP Boundaries");
    _verificationIrpBoundariesRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        verificationIrpBoundariesRadioButton_actionPerformed(e);
      }
    });
    _inspectionIrpBoundariesRadioButton.setText("Inspection IRP Boundaries");
    _inspectionIrpBoundariesRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        inspectionIrpBoundariesRadioButton_actionPerformed(e);
      }
    });
    _irpBoundaryPanel.setLayout(flowLayout1);
    _irpBoundaryPanel.setBorder(border6);
    _noneRadioButton.setText("None");
    _noneRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        noneRadioButton_actionPerformed(e);
      }
    });
    _selectReconstructionRegionTypesPanel.add(_verificationRegionsCheckBox);
    _selectReconstructionRegionTypesPanel.add(_inspectionRegionsCheckBox);
    _selectReconstructionRegionTypesPanel.add(_alignmentRegionsCheckBox);
    _buttonPanel.add(_okButton);
    _selectRegionsPanel.add(_reconstructionRectangleCheckBox);
    _selectRegionsPanel.add(_reconstructionRegionPadBoundsCheckBox);
    _selectRegionsPanel.add(_focusRegionCheckBox);
    _reconstructionRegionPanel.add(_selectReconstructionRegionTypesPanel);
    _reconstructionRegionPanel.add(_selectRegionsPanel);
    this.getContentPane().add(_buttonPanel, java.awt.BorderLayout.SOUTH);
    _irpBoundaryPanel.add(_noneRadioButton);
    _mainPanel.add(_reconstructionRegionPanel, java.awt.BorderLayout.CENTER);
    _mainPanel.add(_irpBoundaryPanel, java.awt.BorderLayout.SOUTH);
    _irpBoundaryPanel.add(_inspectionIrpBoundariesRadioButton);
    _irpBoundaryPanel.add(_verificationIrpBoundariesRadioButton);
    this.getContentPane().add(_mainPanel, java.awt.BorderLayout.CENTER);
    this.getContentPane().add(_buttonPanel, java.awt.BorderLayout.SOUTH);
    setTitle("Configure Reconstruction Regions Display");

    buttonGroup1.add(_noneRadioButton);
    buttonGroup1.add(_inspectionIrpBoundariesRadioButton);
    buttonGroup1.add(_verificationIrpBoundariesRadioButton);
  }

  /**
   * @author George A. David
   */
  public void okButton_actionPerformed(ActionEvent e)
  {
    setVisible(false);
    dispose();
  }

  /**
   * @author George A. David
   */
  public void verificationRegionsCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateVerificationRegionsVisibility();
  }

  /**
   * @author George A. David
   */
  public void inspectionRegionsCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateInspectionRegionsVisibility();
  }

  /**
   * @author George A. David
   */
  public void alignmentRegionsCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateAlignmentRegionsVisibility();
  }

  /**
   * @author George A. David
   */
  public void reconstructionRectangleCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateReconstructionRegionRectangleVisibility();
  }

  /**
   * @author George A. David
   */
  public void reconstructionRegionPadBoundsCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateReconstructionRegionPadBoundsVisibility();
  }

  /**
   * @author George A. David
   */
  public void focusRegionCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateReconstructionFocusRegionsVisibility();
  }

  /**
   * @author George A. David
   */
  public boolean areVerificationRegionRectanglesVisible()
  {
    return _reconstructionRectangleCheckBox.isSelected() && _verificationRegionsCheckBox.isSelected();
  }

  /**
   * @author George A. David
   */
  public boolean areVerificationRegionPadBoundsVisible()
  {
    return _reconstructionRegionPadBoundsCheckBox.isSelected() && _verificationRegionsCheckBox.isSelected();
  }

  /**
   * @author George A. David
   */
  public boolean areVerificationFocusRegionsVisible()
  {
    return _focusRegionCheckBox.isSelected() && _verificationRegionsCheckBox.isSelected();
  }

  /**
   * @author George A. David
   */
  public boolean areInspectionRegionRectanglesVisible()
  {
    return _reconstructionRectangleCheckBox.isSelected() && _inspectionRegionsCheckBox.isSelected();
  }

  /**
   * @author George A. David
   */
  public boolean areInspectionRegionPadBoundsVisible()
  {
    return _reconstructionRegionPadBoundsCheckBox.isSelected() && _inspectionRegionsCheckBox.isSelected();
  }

  /**
   * @author George A. David
   */
  public boolean areInspectionFocusRegionsVisible()
  {
    return _focusRegionCheckBox.isSelected() && _inspectionRegionsCheckBox.isSelected();
  }

  /**
   * @author George A. David
   */
  public boolean areAlignmentRegionRectanglesVisible()
  {
    return _reconstructionRectangleCheckBox.isSelected() && _alignmentRegionsCheckBox.isSelected();
  }

  /**
   * @author George A. David
   */
  public boolean areAlignmentRegionPadBoundsVisible()
  {
    return _reconstructionRegionPadBoundsCheckBox.isSelected() && _alignmentRegionsCheckBox.isSelected();
  }

  /**
   * @author George A. David
   */
  public boolean areAlignmentFocusRegionsVisible()
  {
    return _focusRegionCheckBox.isSelected() && _alignmentRegionsCheckBox.isSelected();
  }

  /**
   * @author George A. David
   */
  public boolean areVerificationIrpBoundariesVisible()
  {
    return _verificationIrpBoundariesRadioButton.isSelected();
  }

  /**
   * @author George A. David
   */
  public boolean areInspectionIrpBoundariesVisible()
  {
    return _inspectionIrpBoundariesRadioButton.isSelected();
  }

  /**
   * @author George A. David
   */
  public void noneRadioButton_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateIrpBoundariesVisibility();
  }

  /**
   * @author George A. David
   */
  public void inspectionIrpBoundariesRadioButton_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateIrpBoundariesVisibility();
  }

  /**
   * @author George A. David
   */
  public void verificationIrpBoundariesRadioButton_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateIrpBoundariesVisibility();
  }
}

