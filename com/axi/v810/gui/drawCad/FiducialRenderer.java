package com.axi.v810.gui.drawCad;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Graphics;

/**
 * This class draws Fiducial outlines.
 *
 * @author Laura Cormos
 */
public class FiducialRenderer extends ShapeRenderer
{
  private Fiducial _fiducial = null;

  /**
   * @author Laura Cormos
   */
  FiducialRenderer(Fiducial fiducial)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(fiducial != null);
    _fiducial = fiducial;

    refreshData();
  }

  /**
   * @author Laura Cormos
   */
  public Fiducial getFiducial()
  {
    return _fiducial;
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    Color origColor = graphics2D.getColor();

//    if ((getForeground() != getSelectedColor()) && (getForeground() != getHighlightedColor())) // if selected or highlighted, use the selected color
//    {
//    graphics2D.setColor(LayerColorEnum.FIDUCIAL_COLOR.getColor());
    graphics2D.setColor(getForeground());
      graphics2D.draw(getShape());
//    }
//    else
//      graphics2D.fill(getShape());

    graphics2D.setColor(origColor);
  }

  /**
   * Return the name of the fiducial being renderered
   * @author Laura Cormos
   */
  public String toString()
  {
    return _fiducial.getName();
  }

  /**
   * @author Laura Cormos
   */
  protected void refreshData()
  {
    Assert.expect(_fiducial != null);

    initializeShape(_fiducial.getShapeRelativeToPanelInNanoMeters());
  }
}
