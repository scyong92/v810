package com.axi.v810.gui.drawCad;


import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelDesc.Panel;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.Component;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * @author Andy Mectenberg
 */
class ControlToolBar extends JToolBar implements Observer
{
  private static double _STANDARD_ZOOM = 2.0;

  private DrawCadPanel _parent;
  private GraphicsEngine _graphicsEngine;
  private PanelGraphicsEngineSetup _panelGraphicsEngineSetup = null;
  private BoardGraphicsEngineSetup _boardGraphicsEngineSetup = null;
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private com.axi.v810.business.panelDesc.Panel _panel = null;
  private com.axi.v810.business.panelDesc.Board _board = null;

  private ShapeRenderer _rendererWithCrosshairs = null;
  private String _componentNotFound = "";

  private ActionListener _findComponentActionListener;
  PopupMenuListener _findComponentPopupMenuListener;

  private ImageIcon _viewTopSideImage;
  private ImageIcon _viewBottomSideImage;
  private ImageIcon _viewBothSidesImage;
  private ImageIcon _viewFromTopImage;
  private ImageIcon _viewFromBottomImage;
  private ImageIcon _zoomInImage;
  private ImageIcon _zoomOutImage;
  private ImageIcon _zoomRectangleImage;
  private ImageIcon _zoomResetImage;
  private ImageIcon _dragImage;
  private ImageIcon _groupSelectImage;
  private ImageIcon _selectRegionImage;
  private ImageIcon _graphicEngineController;
  private ImageIcon _previousBoardImage;
  private ImageIcon _nextBoardImage;
  private ImageIcon _untestableAreaImage;

  private JButton _zoomInButton = new JButton();
  private JButton _zoomOutButton = new JButton();
  private JToggleButton _zoomRectangleToggleButton = new JToggleButton();
  private JToggleButton _dragToggleButton = new JToggleButton();
  private JButton _resetGraphicsButton = new JButton();

  private JToggleButton _groupSelectButton = new JToggleButton();

  private JToggleButton _topSideToggleButton = new JToggleButton();
  private JToggleButton _bottomSideToggleButton = new JToggleButton();
  private JToggleButton _bothSidesToggleButton = new JToggleButton();
  private JToggleButton _selectRegionToggleButton = new JToggleButton();

  private JToggleButton _viewFromTopToggleButton = new JToggleButton();
  private JToggleButton _viewFromBottomToggleButton = new JToggleButton();
  private JToggleButton _graphicEngineControllerButton = new JToggleButton();

//  private JToggleButton _viewSurfaceMapToggleButton = new JToggleButton();
  private JComboBox _findComponentComboBox = new JComboBox();
  private JButton _findComponentButton = new JButton();

  private ButtonGroup _viewFromButtonGroup = new ButtonGroup();
  private ButtonGroup _viewSidesButtonGroup = new ButtonGroup();

  private JToggleButton _measureToggleButton = new JToggleButton(); 
  private JToggleButton _surfaceMappingDragToggleButton = new JToggleButton();
  private JToggleButton _displayPadNameToggleButton = new JToggleButton();
  
  //Kee Chin Seong 
  private JToggleButton _displayNoLoadComponentToggleButton = new JToggleButton();
  
  //Janan XCR-3837 - Display untestable Area on virtual live CAD
  private JToggleButton _displayUntestableAreaToggleButton = new JToggleButton();
          
  private JButton _configureReconstructionRegionsButton = new JButton();

  private JPanel _toolBarPanel = new JPanel();

  private ConfigureReconstructionRegionsDialogBox _configureReconstructionRegionsDialogBox = null;
  //Kee Chin Seong
  private ConfigureCadGraphicEngineDialogBox _configureCadGraphicEngineDiaglogBox = null;
  
  private boolean _allowDebugGraphicsConfiguration = false;
  private boolean _shouldGraphicsBeFitToScreen = true;
  
  // Ying-Huan.Chu
  private JButton _previousBoardButton = new JButton();
  private JButton _nextBoardButton = new JButton();

  /**
   * The tool bar can be created in one of two ways.  The full tool bar is puts all controls in and is
   * intended for Panel graphics.
   * Zoom In
   * Zoom Out
   * Zoom Rectangle
   * Pan
   * Reset (1:1)
   * View Top Side
   * View Bottom Side
   * View Both Sides
   * View From Top
   * View From Bottom
   * Component Combobox
   * Find Component button
   *
   * The not full toolbar is intended for displaying packages or land patterns.  Controls having to do
   * with panel display are not included.
   * Zoom In
   * Zoom Out
   * Zoom Rectangle
   * Pan
   * Reset (1:1)
   * @author Andy Mechtenberg
   */
  public ControlToolBar(DrawCadPanel parent,
                        PanelGraphicsEngineSetup panelGraphicsEngineSetup,
                        Panel panel)
  {
    this(parent, panelGraphicsEngineSetup.getGraphicsEngine());

    Assert.expect(panel != null);
    Assert.expect(panelGraphicsEngineSetup != null);

    _panel = panel;
    _panelGraphicsEngineSetup = panelGraphicsEngineSetup;
    _panelGraphicsEngineSetup.setControlToolBar(this);
    if (_allowDebugGraphicsConfiguration)
      _configureReconstructionRegionsDialogBox = new ConfigureReconstructionRegionsDialogBox(this);
    
    _configureCadGraphicEngineDiaglogBox = new ConfigureCadGraphicEngineDialogBox(this);
    
    createFullToolBar();

    // add this as an observer of the graphics engine for left clicks
    _panelGraphicsEngineSetup.getGraphicsEngine().getGraphicsEngineObservable().addObserver(this);
    ProjectObservable.getInstance().addObserver(this);
  }

  
  /*
   * @author Kee Chin Seong
   */
  public ControlToolBar(DrawCadPanel parent,
                        BoardGraphicsEngineSetup boardGraphicsEngineSetup,
                        Board board)
  {
    this(parent, boardGraphicsEngineSetup.getGraphicsEngine());

    Assert.expect(board != null);
    Assert.expect(boardGraphicsEngineSetup != null);

    _board = board;
    _boardGraphicsEngineSetup = boardGraphicsEngineSetup;
    _boardGraphicsEngineSetup.setControlToolBar(this);
    if (_allowDebugGraphicsConfiguration)
      _configureReconstructionRegionsDialogBox = new ConfigureReconstructionRegionsDialogBox(this);
    
    _configureCadGraphicEngineDiaglogBox = new ConfigureCadGraphicEngineDialogBox(this);
    
    createBoardToolBar();

    // add this as an observer of the graphics engine for left clicks
    _boardGraphicsEngineSetup.getGraphicsEngine().getGraphicsEngineObservable().addObserver(this);
    ProjectObservable.getInstance().addObserver(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  void resetForNewPanel(PanelGraphicsEngineSetup panelGraphicsEngineSetup, Panel panel)
  {
    Assert.expect(panel != null);
    Assert.expect(panelGraphicsEngineSetup != null);

    _panel = panel;
    _board = null;
    _panelGraphicsEngineSetup = panelGraphicsEngineSetup;
    _boardGraphicsEngineSetup = null;
    _panelGraphicsEngineSetup.setControlToolBar(this);
    _graphicsEngine = _panelGraphicsEngineSetup.getGraphicsEngine();
    _rendererWithCrosshairs = null;

    _panelGraphicsEngineSetup.getGraphicsEngine().getGraphicsEngineObservable().addObserver(this);
    ProjectObservable.getInstance().addObserver(this);

    if (_allowDebugGraphicsConfiguration)
      _configureReconstructionRegionsDialogBox = new ConfigureReconstructionRegionsDialogBox(this);
    
    _configureCadGraphicEngineDiaglogBox = new ConfigureCadGraphicEngineDialogBox(this);
    
    createFullToolBar();
    
    //XCR-2180 - disable the control CAD renderer button function
    //this.add(_graphicEngineControllerButton, null);
    // Only add DisplayPadName toogle button
    this.add(_displayPadNameToggleButton, null);
    this.add(_displayNoLoadComponentToggleButton, null);
    
    //Janan XCR-3837 - Display untestable Area on virtual live CAD
    this.add(_displayUntestableAreaToggleButton,null);
    if(panel.hasUntestableArea())
    {
      _displayUntestableAreaToggleButton.setVisible(true);
    }
    else
    {
      _displayUntestableAreaToggleButton.setVisible(false);
    }
    
    if (_displayPadNameToggleButton.isSelected())
        _graphicsEngine.displayPadName(true);
    else
        _graphicsEngine.displayPadName(false);
    
    if (_displayNoLoadComponentToggleButton.isSelected())
        _graphicsEngine.displayNoLoadComponent(true);
    else
        _graphicsEngine.displayNoLoadComponent(false);
    
    
    java.util.List<String> excludeList = new java.util.LinkedList<String>();
    //XCR-3792,Select region button at Modify Subtype and Adjust CAD should be disabled
    excludeList.add(_selectRegionToggleButton.getName());
    enableAllControlsWithExcludeList(excludeList);
  }

 /**
   * @author Andy Mechtenberg
   * @Edited By Kee Chin Seong
   */
  void resetForNewBoard(BoardGraphicsEngineSetup boardGraphicsEngineSetup, Board board)
  {
    Assert.expect(board != null);
    Assert.expect(boardGraphicsEngineSetup != null);
    
    _board = board;
    _panel = null;
    _boardGraphicsEngineSetup = boardGraphicsEngineSetup;
    _panelGraphicsEngineSetup = null;
    _boardGraphicsEngineSetup.setControlToolBar(this);
    _graphicsEngine = _boardGraphicsEngineSetup.getGraphicsEngine();
    _rendererWithCrosshairs = null;

    _boardGraphicsEngineSetup.getGraphicsEngine().getGraphicsEngineObservable().addObserver(this);
    ProjectObservable.getInstance().addObserver(this);

    if (_allowDebugGraphicsConfiguration)
      _configureReconstructionRegionsDialogBox = new ConfigureReconstructionRegionsDialogBox(this);
    
    _configureCadGraphicEngineDiaglogBox = new ConfigureCadGraphicEngineDialogBox(this);
    
    createBoardToolBar();
    enableAllControls();
    
    this.add(_displayNoLoadComponentToggleButton, null);
    _displayNoLoadComponentToggleButton.setEnabled(true);
    
    if (_displayNoLoadComponentToggleButton.isSelected())
        _graphicsEngine.displayNoLoadComponent(true);
    else
        _graphicsEngine.displayNoLoadComponent(false);
    
    showTopSide();
  }

  /**
   * @author Andy Mechtenberg
   */
  void resetForNewPackageOrLandPattern(GraphicsEngine graphicsEngine)
  {
    Assert.expect(graphicsEngine != null);
    _graphicsEngine = graphicsEngine;
    _measureToggleButton.setSelected(false);
    
    // Remove SurfaceMap dragging toggle button in Package/LandPattern view
    this.remove(_surfaceMappingDragToggleButton);
    this.remove(_groupSelectButton);
    
    // Only add DisplayPadName toogle button in Package view
    this.add(_displayPadNameToggleButton, null);
    this.add(_displayNoLoadComponentToggleButton, null);
    
    if (_displayNoLoadComponentToggleButton.isSelected())
        _graphicsEngine.displayNoLoadComponent(true);
    else
        _graphicsEngine.displayNoLoadComponent(false);
    
    if (_displayPadNameToggleButton.isSelected())
        _graphicsEngine.displayPadName(true);
    else
        _graphicsEngine.displayPadName(false);
  }

  /**
   * @author Andy Mechtenberg
   */
  public ControlToolBar(DrawCadPanel parent, GraphicsEngine graphicsEngine)
  {
    Assert.expect(parent != null);
    Assert.expect(graphicsEngine != null);

    _parent = parent;
    _graphicsEngine = graphicsEngine;

    try
    {
      _allowDebugGraphicsConfiguration = LicenseManager.isDeveloperSystemEnabled() && TestDev.isInDeveloperDebugMode();
    }
    catch(BusinessException bex)
    {
      _allowDebugGraphicsConfiguration = false;
      MainMenuGui.getInstance().handleXrayTesterException(bex);
    }

    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
    _graphicsEngine.getGraphicsEngineObservable().addObserver(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof GraphicsEngineObservable)
          handleGraphicsEngineEvent(object);
        else if (observable instanceof ProjectObservable)
          handleProjectChangeEvent(object);
        else
          Assert.expect(false);
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleGraphicsEngineEvent(final Object object)
  {
    GraphicsEngineEvent graphicsEngineEvent = (GraphicsEngineEvent)object;
    if (graphicsEngineEvent.getGraphicsEngineEventEnum() == GraphicsEngineEventEnum.ZOOM_RECTANGLE_EVENT_FINISHED)
    {
      _zoomRectangleToggleButton.setSelected(false);
      _graphicsEngine.setZoomRectangleMode(false);
      //Jack Hwee
      if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment() == false)
        _graphicsEngine.setGroupSelectMode(true);
      else
        _graphicsEngine.setMouseEventsEnabled(true);
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setGroupSelectMode(boolean selectGroupMode)
  {
    Assert.expect(_graphicsEngine != null);
    
    if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment() == false)
      _graphicsEngine.setGroupSelectMode(selectGroupMode);
    else
      _graphicsEngine.setGroupSelectMode(false);
  }
  
  /**
   * @author Andy Mechtenberg
   */
  private void handleProjectChangeEvent(final Object object)
  {
    // ignore the update if our project object hasn't been initialized
    if (object instanceof ProjectChangeEvent)
    {
      ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)object;
      ProjectChangeEventEnum eventEnum = projectChangeEvent.getProjectChangeEventEnum();
      //            System.out.println("Groupings: Datastore Event " + eventEnum);
      if (eventEnum instanceof ComponentTypeEventEnum) // pay attention to add/delete/rename
      {
        if (eventEnum.equals(ComponentTypeEventEnum.REFERENCE_DESIGNATOR))
        {
          //Modified by Seng Yew on 11-Jan-2013
          // This casting is only applicable to ComponentTypeEventEnum.REFERENCE_DESIGNATOR,
          // so put this line inside here.
          ComponentType compType = (ComponentType) projectChangeEvent.getSource();

          Object text = _findComponentComboBox.getEditor().getItem();
          populateFindComponentComboBox(_panel, false);
          // rename -- only care if the edit box has it
          String oldComponentName = projectChangeEvent.getOldValue().toString();
          if (text.toString().equalsIgnoreCase(oldComponentName))
          {
            _findComponentComboBox.getEditor().setItem(compType.getReferenceDesignator());
            _findComponentComboBox.setSelectedItem(compType.getReferenceDesignator());
          }
        }
        else if (eventEnum.equals(ComponentTypeEventEnum.CREATE_OR_DESTROY) || eventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE))
        {
          if (projectChangeEvent.isDestroyed() || projectChangeEvent.isRemoved())
          {
            // destroy case -- remove the object from the combo box
            ComponentType destroyedComponent = (ComponentType)projectChangeEvent.getOldValue();
            Object text = _findComponentComboBox.getEditor().getItem();
            _findComponentComboBox.removeActionListener(_findComponentActionListener);
            _findComponentComboBox.removeItem(destroyedComponent);
            _findComponentComboBox.addActionListener(_findComponentActionListener);
            if (text.toString().equalsIgnoreCase(destroyedComponent.getReferenceDesignator()))
            {
              // the component being deleted is same as the currently selected on in the combo box,
              // so since it won't exist anymore, we should repopulate the box fully.
              String instructionString = StringLocalizer.keyToString("DCGUI_FINDCOMPONENT_SELECTION_KEY");
              _findComponentComboBox.setSelectedItem(instructionString);
              _findComponentComboBox.getEditor().selectAll();
            }
          }
          else
          {
            // a component was added -- we want to add it to the combo box in the correct alphabetical order
            // for speed reasons we'll add the new component to the end of the combo box
  //                ComponentType addedComponent = (ComponentType)projectChangeEvent.getNewValue();
  //                _findComponentComboBox.addItem(addedComponent);
            // it's not so slow after all -- we just populate in alphabetical order
            populateFindComponentComboBox(_panel, false);
          }
        }
        // Modified by Seng Yew on 11-Jan-2013
        // XCR1559 - Able to delete multiple components at once
        // This is to remove a list of ComponentType
        else if (eventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST))
        {
          if (projectChangeEvent.isRemoved())
          {
            // destroy case -- remove the object from the combo box
            java.util.List<ComponentType> destroyedComponent = (java.util.List<ComponentType>)projectChangeEvent.getOldValue();
            _findComponentComboBox.removeActionListener(_findComponentActionListener);
            Iterator destroyedComponentIt = destroyedComponent.iterator();
            while (destroyedComponentIt.hasNext())
            {
              _findComponentComboBox.removeItem(destroyedComponentIt.next());
            }
            _findComponentComboBox.addActionListener(_findComponentActionListener);
            populateFindComponentComboBox(_panel, true);
            
          }
          else
            populateFindComponentComboBox(_panel, false);
        }
      }
      else if (eventEnum instanceof FiducialTypeEventEnum) // pay attention to add/delete/rename
      {
        FiducialType fiducialType = (FiducialType)projectChangeEvent.getSource();
        if (eventEnum.equals(FiducialTypeEventEnum.NAME))
        {
          Object text = _findComponentComboBox.getEditor().getItem();
          populateFindComponentComboBox(_panel, false);
          // rename -- only care if the edit box has it
          String oldFiducialName = projectChangeEvent.getOldValue().toString();
          if (text.toString().equalsIgnoreCase(oldFiducialName))
          {
            _findComponentComboBox.getEditor().setItem(fiducialType.getName());
            _findComponentComboBox.setSelectedItem(fiducialType.getName());
          }
        }
        else if (eventEnum.equals(FiducialTypeEventEnum.CREATE_OR_DESTROY) || eventEnum.equals(FiducialTypeEventEnum.ADD_OR_REMOVE) ||
            eventEnum.equals(FiducialTypeEventEnum.ADD_OR_REMOVE_FIDUCIAL))
        {
          if (projectChangeEvent.isDestroyed() || projectChangeEvent.isRemoved())
          {
            // destroy case -- remove the object from the combo box
            FiducialType destroyedFiducial = (FiducialType)projectChangeEvent.getOldValue();
            Object text = _findComponentComboBox.getEditor().getItem();
            _findComponentComboBox.removeActionListener(_findComponentActionListener);
            _findComponentComboBox.removeItem(destroyedFiducial);
            _findComponentComboBox.addActionListener(_findComponentActionListener);
            if (text.toString().equalsIgnoreCase(destroyedFiducial.getName()))
            {
              // the component being deleted is same as the currently selected on in the combo box,
              // so since it won't exist anymore, we should repopulate the box fully.
              String instructionString = StringLocalizer.keyToString("DCGUI_FINDCOMPONENT_SELECTION_KEY");
              _findComponentComboBox.setSelectedItem(instructionString);
              _findComponentComboBox.getEditor().selectAll();
            }
          }
          else
          {
            // a component was added -- we want to add it to the combo box in the correct alphabetical order
            // for speed reasons we'll add the new component to the end of the combo box
  //                ComponentType addedComponent = (ComponentType)projectChangeEvent.getNewValue();
  //                _findComponentComboBox.addItem(addedComponent);
            // it's not so slow after all -- we just populate in alphabetical order
            populateFindComponentComboBox(_panel, false);
          }
        }
      }
      else if (eventEnum instanceof FiducialEventEnum) // pay attention to add/delete/rename for panel fiducials
      {
        Fiducial fiducial = (Fiducial)projectChangeEvent.getSource();
        FiducialType fiducialType = fiducial.getFiducialType();
        if (eventEnum.equals(FiducialEventEnum.CREATE_OR_DESTROY) || eventEnum.equals(FiducialEventEnum.ADD_OR_REMOVE))
        {
          if (projectChangeEvent.isDestroyed() || projectChangeEvent.isRemoved())
          {
            // destroy case -- remove the object from the combo box
            Fiducial destroyedFiducial = (Fiducial)projectChangeEvent.getOldValue();
            Object text = _findComponentComboBox.getEditor().getItem();
            _findComponentComboBox.removeActionListener(_findComponentActionListener);
            _findComponentComboBox.removeItem(destroyedFiducial.getFiducialType());
            _findComponentComboBox.addActionListener(_findComponentActionListener);
            if (text.toString().equalsIgnoreCase(destroyedFiducial.getName()))
            {
              // the component being deleted is same as the currently selected on in the combo box,
              // so since it won't exist anymore, we should repopulate the box fully.
              String instructionString = StringLocalizer.keyToString("DCGUI_FINDCOMPONENT_SELECTION_KEY");
              _findComponentComboBox.setSelectedItem(instructionString);
              _findComponentComboBox.getEditor().selectAll();
            }
          }
          else
          {
            // a component was added -- we want to add it to the combo box in the correct alphabetical order
            // for speed reasons we'll add the new component to the end of the combo box
  //                ComponentType addedComponent = (ComponentType)projectChangeEvent.getNewValue();
  //                _findComponentComboBox.addItem(addedComponent);
            // it's not so slow after all -- we just populate in alphabetical order
            populateFindComponentComboBox(_panel, false);
          }
        }
      }

      else if (eventEnum instanceof BoardEventEnum) // pay attention to add/delete
      {
        BoardEventEnum boardEventEnum = (BoardEventEnum)eventEnum;
        if (boardEventEnum.equals(BoardEventEnum.CREATE_OR_DESTROY) || boardEventEnum.equals(BoardEventEnum.ADD_OR_REMOVE))
        {  
          // we need to repopulate if all the boards for a boardtype
          populateFindComponentComboBox(_panel, false);
        }
      }
      else if (eventEnum instanceof ComponentTypeSettingsEventEnum)
      {
        //XCR 2273 - Handle remove renderer problem, remove/add the component to find component combo box
        ProjectChangeEvent event = (ProjectChangeEvent) object;
        java.util.List<ComponentTypeSettings> componentSettingsList = null;
        if (event.getSource() instanceof ArrayList)
        {
          componentSettingsList = (java.util.List<ComponentTypeSettings>) event.getSource();
        }
        else
        {
          componentSettingsList = new ArrayList<ComponentTypeSettings> ();
          componentSettingsList.add((ComponentTypeSettings)event.getSource());
        }

        for (ComponentTypeSettings componentSettings : componentSettingsList)
        {
          ComponentType componentType = componentSettings.getComponentType();

          ProjectChangeEventEnum projectEventEnum = ((ProjectChangeEvent) object).getProjectChangeEventEnum();
          ComponentTypeSettingsEventEnum compTypeSettingsEventEnum = (ComponentTypeSettingsEventEnum) projectEventEnum;
          if (compTypeSettingsEventEnum.equals(ComponentTypeSettingsEventEnum.LOADED))
          {
            _findComponentComboBox.removeActionListener(_findComponentActionListener);

            if (componentSettings.isLoaded() || (componentSettings.isLoaded() == false && _displayNoLoadComponentToggleButton.isSelected() == true))
              _findComponentComboBox.addItem(componentType);
            else
              _findComponentComboBox.removeItem(componentType);

          //reset the combobox text to "Find Component" to prevent user press enter at the combo box
            //when the component's renderers is removed after set to no load
            String instructionString = StringLocalizer.keyToString("DCGUI_FINDCOMPONENT_SELECTION_KEY");
            _findComponentComboBox.setSelectedItem(instructionString);
            _findComponentComboBox.addActionListener(_findComponentActionListener);
          }
        }
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   * @Edited By Kee Chin Seong
   */
  private void jbInit() throws Exception
  {
    _viewTopSideImage = Image5DX.getImageIcon(Image5DX.DC_VIEW_TOPSIDE);
    _viewBottomSideImage = Image5DX.getImageIcon(Image5DX.DC_VIEW_BOTTOMSIDE);
    _viewBothSidesImage = Image5DX.getImageIcon(Image5DX.DC_VIEW_BOTHSIDES);
    _viewFromTopImage = Image5DX.getImageIcon(Image5DX.DC_VIEW_FROMTOP);
    _viewFromBottomImage = Image5DX.getImageIcon(Image5DX.DC_VIEW_FROMBOTTOM);
    _zoomInImage = Image5DX.getImageIcon(Image5DX.DC_ZOOM_IN);
    _zoomOutImage = Image5DX.getImageIcon(Image5DX.DC_ZOOM_OUT);
    _zoomRectangleImage = Image5DX.getImageIcon(Image5DX.DC_ZOOM_RECTANGLE);
    _zoomResetImage = Image5DX.getImageIcon(Image5DX.DC_ZOOM_RESET);
    _dragImage = Image5DX.getImageIcon(Image5DX.DC_MOVE);
    _groupSelectImage = Image5DX.getImageIcon(Image5DX.DC_GROUPSELECT);
    _selectRegionImage = Image5DX.getImageIcon(Image5DX.DC_SELECT_REGION);
    _previousBoardImage = Image5DX.getImageIcon(Image5DX.PREVIOUS_IMAGE);
    _nextBoardImage = Image5DX.getImageIcon(Image5DX.NEXT_IMAGE);
    _untestableAreaImage = Image5DX.getImageIcon(Image5DX.DC_UNTESTABLE_AREA);

    _zoomInButton.setToolTipText(StringLocalizer.keyToString("DCGUI_ZOOMIN_TOOLTIP_KEY"));
    _zoomInButton.setIcon(_zoomInImage);

    _zoomInButton.setFocusable(false);
    _zoomOutButton.setFocusable(false);
    _zoomRectangleToggleButton.setFocusable(false);
    _dragToggleButton.setFocusable(false);
    _resetGraphicsButton.setFocusable(false);
    _groupSelectButton.setFocusable(false);
    _topSideToggleButton.setFocusable(false);
    _bottomSideToggleButton.setFocusable(false);
    _bothSidesToggleButton.setFocusable(false);
    _viewFromTopToggleButton.setFocusable(false);
    _viewFromBottomToggleButton.setFocusable(false);
    _measureToggleButton.setFocusable(false);
//    _viewSurfaceMapToggleButton.setFocusable(false);
    _findComponentButton.setFocusable(false);
    _surfaceMappingDragToggleButton.setFocusable(false);
    _displayPadNameToggleButton.setFocusable(false);
    _displayNoLoadComponentToggleButton.setFocusable(false);
    _displayUntestableAreaToggleButton.setFocusable(false);
 
    _previousBoardButton.setFocusable(false);
    _nextBoardButton.setFocusable(false);
    
    _graphicEngineControllerButton.setFocusable(false);

    _configureReconstructionRegionsButton.setText("Configure Reconstruction Regions");
    _configureReconstructionRegionsButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        configureReconstructionRegionsButton_actionPerformed(e);
      }
    });

    _zoomInButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        zoomInButton_actionPerformed(e);
      }
    });

    _zoomOutButton.setToolTipText(StringLocalizer.keyToString("DCGUI_ZOOMOUT_TOOLTIP_KEY"));
    _zoomOutButton.setIcon(_zoomOutImage);
    _zoomOutButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        zoomOutButton_actionPerformed(e);
      }
    });
    _bottomSideToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_VIEWBOTTOM_TOOLTIP_KEY"));
    _bottomSideToggleButton.setIcon(_viewBottomSideImage);
    _bottomSideToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        bottomSideToggleButton_actionPerformed(e);
      }
    });
    _viewFromBottomToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_VIEWFROMBOTTOM_TOOLTIP_KEY"));
    _viewFromBottomToggleButton.setIcon(_viewFromBottomImage);
    _viewFromBottomToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        viewFromBottomToggleButton_actionPerformed(e);
      }
    });
    _topSideToggleButton.setSelected(true);
    _topSideToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_VIEWTOP_TOOLTIP_KEY"));
    _topSideToggleButton.setIcon(_viewTopSideImage);
    _topSideToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        topSideToggleButton_actionPerformed(e);
      }
    });
    _dragToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_DRAG_TOOLTIP_KEY"));
    _dragToggleButton.setIcon(_dragImage);
    _dragToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dragToggleButton_actionPerformed(e);
      }
    });

    _selectRegionToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_SELECT_REGION_KEY"));
    _selectRegionToggleButton.setIcon(_selectRegionImage);
    _selectRegionToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectRegionToggleButton_actionPerformed(e);
      }
    });

    _viewFromTopToggleButton.setSelected(true);
    _viewFromTopToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_VIEWFROMTOP_TOOLTIP_KEY"));
    _viewFromTopToggleButton.setIcon(_viewFromTopImage);
    _viewFromTopToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        viewFromTopToggleButton_actionPerformed(e);
      }
    });
    _bothSidesToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_VIEWBOTH_TOOLTIP_KEY"));
    _bothSidesToggleButton.setIcon(_viewBothSidesImage);
    _bothSidesToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        bothSidesToggleButton_actionPerformed(e);
      }
    });
    _zoomRectangleToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_ZOOMWINDOW_TOOLTIP_KEY"));
    _zoomRectangleToggleButton.setIcon(_zoomRectangleImage);
    _zoomRectangleToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        zoomRectangleToggleButton_actionPerformed(e);
      }
    });
    _resetGraphicsButton.setToolTipText(StringLocalizer.keyToString("DCGUI_ZOOMRESTORE_TOOLTIP_KEY"));
    _resetGraphicsButton.setIcon(_zoomResetImage);
    _resetGraphicsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        resetGraphicsButton_actionPerformed(e);
      }
    });

    _groupSelectButton.setToolTipText(StringLocalizer.keyToString("DCGUI_GROUPSELECT_TOOLTIP_KEY"));
    _groupSelectButton.setIcon(_groupSelectImage);
    _groupSelectButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        groupSelectButton_actionPerformed(e);
      }
    });

    _measureToggleButton.setToolTipText(StringLocalizer.keyToString("GUI_MEASURE_TOOLTIP_KEY"));
    _measureToggleButton.setIcon(Image5DX.getImageIcon(Image5DX.MEASURE));
  //    _measureToggleButton.setIcon(_dragImage);
    _measureToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        measureToggleButton_actionPerformed(e);
      }
    });

    _findComponentButton.setToolTipText(StringLocalizer.keyToString("DCGUI_FINDCOMPONENT_TOOLTIP_KEY"));
    _findComponentButton.setText(StringLocalizer.keyToString("DCGUI_FINDCOMPONENT_KEY"));
    _findComponentActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        //Xcr- 2273, to sort the component everytime user click on the combobox, to make sure the component in correct order
        findComponent();
      }
    };
    
    _findComponentPopupMenuListener = new PopupMenuListener()
    {
      public void popupMenuWillBecomeVisible(PopupMenuEvent e)
      {
        populateFindComponentComboBox(_panel,false);
      }

      public void popupMenuWillBecomeInvisible(PopupMenuEvent e)
      {
        //do nothing
      }

      public void popupMenuCanceled(PopupMenuEvent e)
      {
        //do nothing
      }
    };
    
    _surfaceMappingDragToggleButton.setToolTipText(StringLocalizer.keyToString("GUI_ADD_RECTANGLE_TOOLTIP_KEY"));
    _surfaceMappingDragToggleButton.setIcon(Image5DX.getImageIcon(Image5DX.DRAG_OPTICAL_REGION));
    _surfaceMappingDragToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        surfaceMappingToggleButton_actionPerformed(e);
      }
    });
    
    _displayPadNameToggleButton.setToolTipText(StringLocalizer.keyToString("GUI_DISPLAY_PAD_NAME_TOOLTIP_KEY"));
    _displayPadNameToggleButton.setIcon(Image5DX.getImageIcon(Image5DX.DISPLAY_PAD_NAME));
    _displayPadNameToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        displayPadNameToggleButton_actionPerformed(e);
      }
    });
    
    /*
    * Kee Chin Seong
    */
    _displayNoLoadComponentToggleButton.setToolTipText(StringLocalizer.keyToString("GUI_DISPLAY_NO_LOAD_COMPONENT_TOOLTIP_KEY"));
    _displayNoLoadComponentToggleButton.setIcon(Image5DX.getImageIcon(Image5DX.DISPLAY_NO_LOAD_COMPONENT));
    _displayNoLoadComponentToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        displayNoLoadToggleButton_actionPerformed(e);
      }
    });
    
    //Janan XCR-3837 - Display untestable Area on virtual live CAD
    _displayUntestableAreaToggleButton.setToolTipText(StringLocalizer.keyToString("GUI_DISPLAY_UNTESTABLE_AREA_TOOLTIP_KEY"));
    _displayUntestableAreaToggleButton.setIcon(_untestableAreaImage);
    _displayUntestableAreaToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        displayUntestableAreaToggleButton_actionPerformed(e);
      }
    });
    
    //XXL - Add in controller button
    _graphicEngineControllerButton.setToolTipText(StringLocalizer.keyToString("GUI_REMOTE_CONTROOLER_NAME_TOOLTIP_KEY"));
    _graphicEngineControllerButton.setIcon(Image5DX.getImageIcon(Image5DX.GRAPHIC_CONTROLLER_NAME));
    _graphicEngineControllerButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        updateGraphicEnginesRendererSettings(e);
      }
    });
    
    _previousBoardButton.setToolTipText(StringLocalizer.keyToString("DCGUI_PREVIOUS_BOARD_IMAGE_KEY"));
    _previousBoardButton.setIcon(_previousBoardImage);
    _previousBoardButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        previousBoardButton_actionPerformed(e);
      }
    });
    
    _nextBoardButton.setToolTipText(StringLocalizer.keyToString("DCGUI_NEXT_BOARD_IMAGE_KEY"));
    _nextBoardButton.setIcon(_nextBoardImage);
    _nextBoardButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        nextBoardButton_actionPerformed(e);
      }
    });

    Insets margins = new Insets(0,2,2,0);
    _zoomInButton.setMargin(margins);
    _zoomOutButton.setMargin(margins);
    _zoomRectangleToggleButton.setMargin(margins);
    _dragToggleButton.setMargin(margins);
    _resetGraphicsButton.setMargin(margins);
    _topSideToggleButton.setMargin(margins);
    _bottomSideToggleButton.setMargin(margins);
    _bothSidesToggleButton.setMargin(margins);
    _viewFromTopToggleButton.setMargin(margins);
    _viewFromBottomToggleButton.setMargin(margins);
    _measureToggleButton.setMargin(margins);
    _displayPadNameToggleButton.setMargin(margins);
    _displayNoLoadComponentToggleButton.setMargin(margins);
    _graphicEngineControllerButton.setMargin(margins);
    _surfaceMappingDragToggleButton.setMargin(new Insets(0,1,0,0));
    _groupSelectButton.setMargin(margins);
    _previousBoardButton.setMargin(margins);
    _nextBoardButton.setMargin(margins);
    _displayUntestableAreaToggleButton.setMargin(margins);

    this.setFloatable(false);
    this.setRollover(true);

    _toolBarPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

//    this.add(_toolBarPanel, null);

    this.add(_zoomInButton, null);
    this.add(_zoomOutButton, null);
    this.add(_zoomRectangleToggleButton, null);
    this.add(_dragToggleButton, null);
    // removed this button because our default mode is not group selection
//    this.add(_groupSelectButton, null);
    this.add(_resetGraphicsButton, null);
    this.add(_measureToggleButton, null);
    //XCR-2180 - disable the control CAD renderer button function
    //this.add(_graphicEngineControllerButton, null);
    this.add(_displayPadNameToggleButton, null);
    this.add(_displayNoLoadComponentToggleButton, null);

    _zoomInButton.setName(".zoomInButton");
    _zoomOutButton.setName(".zoomOutButton");
    _zoomRectangleToggleButton.setName(".zoomRectangleToggleButton");
    _dragToggleButton.setName(".dragToggleButton.setName");
    _resetGraphicsButton.setName(".resetGraphicsButton");
    _groupSelectButton.setName(".groupSelectButton");
    _topSideToggleButton.setName(".topSideToggleButton");
    _bottomSideToggleButton.setName(".bottomSideToggleButton");
    _bothSidesToggleButton.setName(".bothSidesToggleButton");
    _viewFromTopToggleButton.setName(".viewFromTopToggleButton");
    _viewFromBottomToggleButton.setName(".viewFromBottomToggleButton");
    _measureToggleButton.setName(".measureToggleButton");
    _displayPadNameToggleButton.setName(".displayPadNameToggleButton");
    _graphicEngineControllerButton.setName(".graphicEngineController");
    _findComponentComboBox.setName(".findComponentComboBox");
    _findComponentButton.setName(".findComponentButton");
    _configureReconstructionRegionsButton.setName(".configureReconstructionRegionsButton");
    _selectRegionToggleButton.setName("selectRegionToggleButton");
    _previousBoardButton.setName(".previousBoardButton");
    _nextBoardButton.setName(".nextBoardButton");
    _displayNoLoadComponentToggleButton.setName(".displayNoLoadComponent");
    _displayUntestableAreaToggleButton.setName(".displayUntestableArea");

    if (TestDev.isInSurfaceMappingMode())
    {
      this.add(_surfaceMappingDragToggleButton, null); 
      _surfaceMappingDragToggleButton.setVisible(false);
      this.add(_groupSelectButton, null); 
      _groupSelectButton.setVisible(false);
      _surfaceMappingDragToggleButton.setName(".surfaceMappingDragToggleButton");
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void createFullToolBar()
  {
    _zoomRectangleToggleButton.setSelected(false);
    _dragToggleButton.setSelected(false);
    _groupSelectButton.setSelected(false);
    _topSideToggleButton.setSelected(true);
    _bottomSideToggleButton.setSelected(false);
    _bothSidesToggleButton.setSelected(false);
    _viewFromTopToggleButton.setSelected(true);
    _viewFromBottomToggleButton.setSelected(false);
    _measureToggleButton.setSelected(false);
    _surfaceMappingDragToggleButton.setSelected(false);
    _selectRegionToggleButton.setSelected(false);
    
    //Janan XCR-3837 - Display untestable Area on virtual live CAD
    if (MainMenuGui.getInstance().getTestDev().hasGuiPersistanceSettings() ||
        MainMenuGui.getInstance().getTestDev().getPersistance().displayUntestableArea())
    {
      _displayUntestableAreaToggleButton.setSelected(true);
    }
    else
    {
      _displayUntestableAreaToggleButton.setSelected(false);
    }
    
    this.removeAll();

    Dimension separatorSpacing = new Dimension(10, 25);  // 10 is width, 25 is height

    this.add(_zoomInButton, null);
    this.add(_zoomOutButton, null);
    this.add(_zoomRectangleToggleButton, null);
    this.add(_dragToggleButton, null);
    this.add(_selectRegionToggleButton, null);
    // removed this button because our default mode is not group selection
//    this.add(_groupSelectButton, null);
    this.add(_resetGraphicsButton, null);


    this.addSeparator(separatorSpacing);

    this.add(_topSideToggleButton, null);
    this.add(_bottomSideToggleButton, null);
    this.add(_bothSidesToggleButton, null);

    this.addSeparator(separatorSpacing);

    this.add(_viewFromTopToggleButton, null);
    this.add(_viewFromBottomToggleButton, null);
    this.add(_measureToggleButton, null);
    
    _findComponentComboBox.setEditable(true);

    _toolBarPanel.add(_findComponentComboBox, null);
//      _toolBarPanel.add(_findComponentButton, null);


    if (_allowDebugGraphicsConfiguration)
      _toolBarPanel.add(_configureReconstructionRegionsButton, null);

    add(_toolBarPanel, null);
    
    if (TestDev.isInSurfaceMappingMode())
    {
      //Kok Chun, Tan - no more using this button. Now direct right click add rectangle.
      //_toolBarPanel.add(_surfaceMappingDragToggleButton, null);
      if (TestDev.isInSurfaceMapManualAlignmentMode() == false)
        _toolBarPanel.add(_groupSelectButton, null); 
      else
        _toolBarPanel.remove(_groupSelectButton); 
      // _surfaceMappingDragToggleButton.hide();
    }

    _viewFromButtonGroup.add(_viewFromTopToggleButton);
    _viewFromButtonGroup.add(_viewFromBottomToggleButton);

    _viewSidesButtonGroup.add(_topSideToggleButton);
    _viewSidesButtonGroup.add(_bottomSideToggleButton);
    _viewSidesButtonGroup.add(_bothSidesToggleButton);

    populateFindComponentComboBox(_panel, true);
    
    FontMetrics fm = _findComponentComboBox.getFontMetrics(_findComponentComboBox.getFont());
    int prefWidth = fm.stringWidth(StringLocalizer.keyToString("DCGUI_FINDCOMPONENT_SELECTION_KEY"));
    _findComponentComboBox.setPreferredSize(new Dimension(prefWidth + 45, CommonUtil._COMBO_BOX_HEIGHT));  // the +45 is for the drop down control
  }

  /**
   * @author Wei Chin
   */
  private void createBoardToolBar()
  {
    _zoomRectangleToggleButton.setSelected(false);
    _dragToggleButton.setSelected(false);
    _groupSelectButton.setSelected(false);
    _topSideToggleButton.setSelected(true);
    _bottomSideToggleButton.setSelected(false);
    _bothSidesToggleButton.setSelected(false);
    _viewFromTopToggleButton.setSelected(true);
    _viewFromBottomToggleButton.setSelected(false);
    _measureToggleButton.setSelected(false);
    if (TestDev.isInSurfaceMappingMode())
      _surfaceMappingDragToggleButton.setSelected(false);

    this.removeAll();

    Dimension separatorSpacing = new Dimension(10, 25);  // 10 is width, 25 is height

    this.add(_zoomInButton, null);
    this.add(_zoomOutButton, null);
    this.add(_zoomRectangleToggleButton, null);
    this.add(_dragToggleButton, null);
    // removed this button because our default mode is not group selection
//    this.add(_groupSelectButton, null);
    this.add(_resetGraphicsButton, null);


    this.addSeparator(separatorSpacing);

    this.add(_topSideToggleButton, null);
    this.add(_bottomSideToggleButton, null);
    this.add(_bothSidesToggleButton, null);

    this.addSeparator(separatorSpacing);

    this.add(_viewFromTopToggleButton, null);
    this.add(_viewFromBottomToggleButton, null);
    this.add(_measureToggleButton, null);

    _findComponentComboBox.setEditable(true);
    this.add(_findComponentComboBox, null);
    
//      _toolBarPanel.add(_findComponentButton, null);


    if (_allowDebugGraphicsConfiguration)
      _toolBarPanel.add(_configureReconstructionRegionsButton, null);

    add(_toolBarPanel, null);

    _previousBoardButton.setVisible(false);
    _nextBoardButton.setVisible(false);
    
    // Jack Hwee - Show button if in surface map mode
    if (TestDev.isInSurfaceMappingMode())
    {
      if (TestDev.isInAutoPopulateMode())
      {
        _toolBarPanel.add(_previousBoardButton);
        _toolBarPanel.add(_nextBoardButton);
        _previousBoardButton.setVisible(true);
        _nextBoardButton.setVisible(true);
        // Kok Chun, Tan - no more using this button. Now direct right click add rectangle.
        //_toolBarPanel.add(_surfaceMappingDragToggleButton); 
        _toolBarPanel.add(_groupSelectButton);
      }
      // Commented by Ying-Huan.Chu as this is only used in Surface Map alignment.
      // Surface Map alignment will only be implemented in PSP Phase 3.
//      else
//      {
//        _toolBarPanel.add(_surfaceMappingDragToggleButton); 
//      }
    }

    _viewFromButtonGroup.add(_viewFromTopToggleButton);
    _viewFromButtonGroup.add(_viewFromBottomToggleButton);

    _viewSidesButtonGroup.add(_topSideToggleButton);
    _viewSidesButtonGroup.add(_bottomSideToggleButton);
    _viewSidesButtonGroup.add(_bothSidesToggleButton);
    
    populateBoardFindComponentComboBox(_board, true);
    
    FontMetrics fm = _findComponentComboBox.getFontMetrics(_findComponentComboBox.getFont());
    int prefWidth = fm.stringWidth(StringLocalizer.keyToString("DCGUI_FINDCOMPONENT_SELECTION_KEY"));
    _findComponentComboBox.setPreferredSize(new Dimension(prefWidth + 45, CommonUtil._COMBO_BOX_HEIGHT));  // the +45 is for the drop down control
  }

  /**
   * This will enable all the buttons and other controls for this tool bar
   * Used when we re-initialize to display something
   * @author Andy Mechteberg
   */
  public void enableAllControls()
  {
    Component components[] = this.getComponents();
    for (int i = 0; i < components.length; i++)
    {
      Component comp = components[i];
      // if this is a JPanel, then we can't just disable the panel, we have to disable all the controls in the panel
      // I suppose this should be recursive, but hopefully only one level of nesting will occur.
      if (comp instanceof JPanel)
      {
        Component nestedComponents[] = ((JPanel)comp).getComponents();
        for (int j = 0; j < nestedComponents.length; j++)
        {
          Component nestedComp = nestedComponents[j];
          nestedComp.setEnabled(true);
        }
      }
      else
        comp.setEnabled(true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _panel = null;
    _findComponentComboBox.removePopupMenuListener(_findComponentPopupMenuListener);
    _findComponentComboBox.removeActionListener(_findComponentActionListener);
    _findComponentComboBox.removeAllItems();
    // put the "Select Component" field back in so it looks the same
    String instructionString = StringLocalizer.keyToString("DCGUI_FINDCOMPONENT_SELECTION_KEY");
    _findComponentComboBox.setSelectedItem(instructionString);
    _findComponentComboBox.getEditor().selectAll();  // this doesn't work, but I think it should!  I want the default text to be selected.
    _findComponentComboBox.addActionListener(_findComponentActionListener);
    _findComponentComboBox.addPopupMenuListener(_findComponentPopupMenuListener);
    
    //Due to there is another PanelGraphicsEngine cereated for Cad Creator, There will be confusion in Oberver
    //To make sure of the clear cut, Everytime unpopulate need to remove observer
    if(_panelGraphicsEngineSetup != null)
      _panelGraphicsEngineSetup.getGraphicsEngine().getGraphicsEngineObservable().deleteObserver(this);
    
    ProjectObservable.getInstance().deleteObserver(this);
  }

  /**
   * This will disable all the buttons and other controls for this tool bar
   * Used when there are no graphics to display because no project is loaded yet
   * @author Andy Mechteberg
   */
  public void disableAllControls()
  {
    Component components[] = this.getComponents();
    for (int i = 0; i < components.length; i++)
    {
      Component comp = components[i];
      // if this is a JPanel, then we can't just disable the panel, we have to disable all the controls in the panel
      // I suppose this should be recursive, but hopefully only one level of nesting will occur.
      if (comp instanceof JPanel)
      {
        Component nestedComponents[] = ((JPanel)comp).getComponents();
        for (int j = 0; j < nestedComponents.length; j++)
        {
          Component nestedComp = nestedComponents[j];
          nestedComp.setEnabled(false);
        }
      }
      else
        comp.setEnabled(false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void highlightRenderer(ShapeRenderer renderer)
  {
    Assert.expect(renderer != null);
    java.util.List<com.axi.guiUtil.Renderer> renderers = new ArrayList<com.axi.guiUtil.Renderer>();

    renderers.add(renderer);
    if (renderer instanceof ComponentRenderer)
    {
      java.util.List<ComponentType> componentTypes = new ArrayList<ComponentType>();
      ComponentRenderer componentRenderer = (ComponentRenderer)renderer;
      componentTypes.add(componentRenderer.getComponent().getComponentType());
      _graphicsEngine.getSelectedRendererObservable().setSelectedRenderer(renderers);
      _guiObservable.stateChanged(componentTypes, SelectionEventEnum.COMPONENT_TYPE);
    }
    else if (renderer instanceof FiducialRenderer)
    {
      java.util.List<FiducialType> fiducialTypes = new ArrayList<FiducialType>();
      FiducialRenderer fiducialRenderer = (FiducialRenderer)renderer;
      fiducialTypes.add(fiducialRenderer.getFiducial().getFiducialType());
      _graphicsEngine.getSelectedRendererObservable().setSelectedRenderer(renderers);
      _guiObservable.stateChanged(fiducialTypes, SelectionEventEnum.FIDUCIAL_TYPE);
    }


    // this code commented because the call above should do it instead
//    boolean topComponentsVisible = _panelGraphicsEngineSetup.isTopSideVisible();
//    boolean bottomComponentsVisible = _panelGraphicsEngineSetup.isBottomSideVisible();
//    boolean componentOnTop = componentRenderer.getComponent().isTopSide();
//
//    if (componentOnTop && (topComponentsVisible == false))
//    {
//      showTopSide();
//    }
//    if ((componentOnTop == false) && (bottomComponentsVisible == false))
//    {
//      showBottomSide();
//    }
//    // center the graphics on the new component renderer if the zoom factor is not 1.0 (user has zoomed)
//    if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0) == false)
//    {
//      // we're at some zoom level, let's center the graphics to show the new alignment region
//      DoubleCoordinate coordinate = componentRenderer.getCenterCoordinate();
//      _graphicsEngine.center(coordinate.getX(), coordinate.getY(), 0, 0);
//    }
//
//    if (componentRenderer != null)
//    {
//      setDescription(componentRenderer.toString());
//      _graphicsEngine.clearSelectedRenderers();
//      _graphicsEngine.clearAllFlashingRenderersAndLayers();
//      _graphicsEngine.selectWithCrossHairs(componentRenderer);
//      _graphicsEngine.setSelectedRenderer(componentRenderer);
//    }
  }

  /**
   * @author Andy Mechtenberg
   */
  void highlightPadRenderer(PadRenderer padRenderer)
  {
    if (padRenderer != null)
    {
      setDescription(padRenderer.toString());
      _graphicsEngine.clearSelectedRenderers();
      _graphicsEngine.clearCrossHairs();
      _graphicsEngine.setSelectedRenderer(padRenderer);
    }
  }

  /**
   * Looks to see if the selected with crosshair component is visible, and removes the crosshairs
   * if it is not
   * @author Andy Mechtenberg
   * @author Wei Chin
   */
  private void clearCrossHairsIfNecessary()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      java.util.List<com.axi.guiUtil.Renderer> componentRenderers = new ArrayList<com.axi.guiUtil.Renderer>
                                                                        (_graphicsEngine.getVisibleRenderers(_panelGraphicsEngineSetup.getTopComponentLayers()));

      componentRenderers.addAll(_graphicsEngine.getVisibleRenderers(_panelGraphicsEngineSetup.getBottomComponentLayers()));
      if(_rendererWithCrosshairs != null)
      {
        if(componentRenderers.contains(_rendererWithCrosshairs) == false)
          _graphicsEngine.clearCrossHairs();
      }
    }
    else if(_boardGraphicsEngineSetup != null)
    {
      java.util.List<com.axi.guiUtil.Renderer> componentRenderers = new ArrayList<com.axi.guiUtil.Renderer>
                                                                        (_graphicsEngine.getVisibleRenderers(_boardGraphicsEngineSetup.getTopComponentLayer()));

      componentRenderers.addAll(_graphicsEngine.getVisibleRenderers(_boardGraphicsEngineSetup.getBottomComponentLayer()));
      if(_rendererWithCrosshairs != null)
      {
        if(componentRenderers.contains(_rendererWithCrosshairs) == false)
          _graphicsEngine.clearCrossHairs();
      }
    }
  }

  /**
   * Fills in the find component combobox with all componentTypes for all board types on the panel
   * @author Andy Mechtenberg
   */
  private void populateFindComponentComboBox(Panel panel, boolean fromScratch)
  {
    Object currentItem = null;
    if (fromScratch == false)
      currentItem = _findComponentComboBox.getSelectedItem();
    _findComponentComboBox.removePopupMenuListener(_findComponentPopupMenuListener);
    _findComponentComboBox.removeActionListener(_findComponentActionListener);
    _findComponentComboBox.removeAllItems();

//    java.util.List<com.axi.guiUtil.Renderer> componentRenderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(_panelGraphicsEngineSetup.getTopComponentLayers()));
//    componentRenderers.addAll(_graphicsEngine.getRenderers(_panelGraphicsEngineSetup.getBottomComponentLayers()));
//
//    // too many!  We only want 1 renderer in the list per board TYPE, not per board instance
//    java.util.List<BoardType> boardTypes = panel.getBoardTypes();
//    for (BoardType boardType : boardTypes)
//    {
//      Board boardInstance = (boardType.getBoards().get(0));
//      Iterator<com.axi.guiUtil.Renderer> it = componentRenderers.iterator();
//      while(it.hasNext())
//      {
//        ComponentRenderer compRenderer = (ComponentRenderer)it.next();
//        if (boardType == compRenderer.getComponent().getSideBoard().getBoard().getBoardType())
//        {
//          if ((compRenderer.getComponent().getSideBoard().getBoard() == boardInstance) == false)  // if it's not the BoardInstance
//            it.remove();
//        }
//      }
//    }
//
//    Collections.sort(componentRenderers, new AlphaNumericComparator());
//    Iterator<com.axi.guiUtil.Renderer> it = componentRenderers.iterator();
//    while (it.hasNext())
//    {
//      ComponentRenderer componentRenderer = (ComponentRenderer)it.next();
//      _findComponentComboBox.addItem(componentRenderer);
//    }

//    long beforeTime = System.currentTimeMillis();

    // _panel will be null if in board base alignment, check if it is null, get _panel from _board
    if (panel == null)
       panel = _board.getPanel();
                 
    java.util.List allComponentsAndFiducials = new ArrayList();
    java.util.List<BoardType> usedBoardTypes = panel.getUsedBoardTypes();

    // to be efficient, detect whether there any fiducials or not
    // before trying to fold them into the component list

    allComponentsAndFiducials.addAll(panel.getFiducialTypes());
//    java.util.List<Board> boards = panel.getBoards();
//    for (Board board : boards)
    for(BoardType boardType : usedBoardTypes)
    {
      allComponentsAndFiducials.addAll(boardType.getFiducialTypes());
    }

    if (allComponentsAndFiducials.isEmpty() == false)
    {
      java.util.List<ComponentType> componentTypes = panel.getComponentTypes();
      for (ComponentType componentType : componentTypes)
      {
        if ((usedBoardTypes.contains(componentType.getSideBoardType().getBoardType())  && componentType.getComponentTypeSettings().isLoaded()) ||
            (componentType.getComponentTypeSettings().isLoaded() == false && _displayNoLoadComponentToggleButton.isSelected() == true))
          allComponentsAndFiducials.add(componentType);
    //        _findComponentComboBox.addItem(componentType);
      }
      Collections.sort(allComponentsAndFiducials, new AlphaNumericComparator());
      for (Object object : allComponentsAndFiducials)
      {
        _findComponentComboBox.addItem(object);
      }
    }
    else
    {
      // Just Components - easy
      java.util.List<ComponentType> componentTypes = panel.getComponentTypes();
      for (ComponentType componentType : componentTypes)
      {
        //sheng-chuan.yong XCR-2273, add another and condition to add only non-noload type component to prevent error.
        if (usedBoardTypes.contains(componentType.getSideBoardType().getBoardType()) && componentType.getComponentTypeSettings().isLoaded() ||
            (usedBoardTypes.contains(componentType.getSideBoardType().getBoardType()) && componentType.getComponentTypeSettings().isLoaded() == false &&
             _displayNoLoadComponentToggleButton.isSelected() == true))
          _findComponentComboBox.addItem(componentType);
      }
    }

    if (fromScratch == false)
    {
      _findComponentComboBox.setSelectedItem(currentItem);
    }
    else
    {
      String instructionString = StringLocalizer.keyToString("DCGUI_FINDCOMPONENT_SELECTION_KEY");
      _findComponentComboBox.setSelectedItem(instructionString);
      _findComponentComboBox.getEditor().selectAll();  // this doesn't work, but I think it should!  I want the default text to be selected.
    }

    _findComponentComboBox.addActionListener(_findComponentActionListener);
    _findComponentComboBox.addPopupMenuListener(_findComponentPopupMenuListener);
    _findComponentComboBox.setMaximumRowCount(20);
    
   

//    long afterTime = System.currentTimeMillis();
//    System.out.println("Total time to load Find Components: " + (afterTime - beforeTime));
  }
  
  /**
   * Fills in the find component combobox with all componentTypes on the board.
   * @author Andy Mechtenberg
   */
  private void populateBoardFindComponentComboBox(Board board, boolean fromScratch)
  {
    Object currentItem = null;
    if (fromScratch == false)
      currentItem = _findComponentComboBox.getSelectedItem();
    _findComponentComboBox.removePopupMenuListener(_findComponentPopupMenuListener);
    _findComponentComboBox.removeActionListener(_findComponentActionListener);
    _findComponentComboBox.removeAllItems();

    // _panel will be null if in board base alignment, check if it is null, get _panel from _board
 
    java.util.List allComponentsAndFiducials = new ArrayList();
    java.util.List<BoardType> usedBoardTypes = _board.getPanel().getUsedBoardTypes();
    
    // to be efficient, detect whether there any fiducials or not
    // before trying to fold them into the component list

    allComponentsAndFiducials.addAll(board.getBoardType().getFiducialTypes());
    
    
    if (allComponentsAndFiducials.isEmpty() == false)
    {
      java.util.List<ComponentType> componentTypes = _board.getBoardType().getComponentTypes();
      for (ComponentType componentType : componentTypes)
      {
        if (usedBoardTypes.contains(componentType.getSideBoardType().getBoardType())  && componentType.getComponentTypeSettings().isLoaded())
          allComponentsAndFiducials.add(componentType);
    //        _findComponentComboBox.addItem(componentType);
      }
      Collections.sort(allComponentsAndFiducials, new AlphaNumericComparator());
      for (Object object : allComponentsAndFiducials)
      {
        _findComponentComboBox.addItem(object);
      }
    }
    else
    {
      // Just Components - easy
      java.util.List<ComponentType> componentTypes = _board.getPanel().getComponentTypes();
      for (ComponentType componentType : componentTypes)
      {
        if (usedBoardTypes.contains(componentType.getSideBoardType().getBoardType()) && componentType.getComponentTypeSettings().isLoaded())
          _findComponentComboBox.addItem(componentType);
      }
    }

    Collections.sort(allComponentsAndFiducials, new AlphaNumericComparator());
    if (fromScratch == false)
    {
      _findComponentComboBox.setSelectedItem(currentItem);
    }
    else
    {
      String instructionString = StringLocalizer.keyToString("DCGUI_FINDCOMPONENT_SELECTION_KEY");
      _findComponentComboBox.setSelectedItem(instructionString);
      _findComponentComboBox.getEditor().selectAll();  // this doesn't work, but I think it should!  I want the default text to be selected.
    }
    _findComponentComboBox.addActionListener(_findComponentActionListener);
    _findComponentComboBox.addPopupMenuListener(_findComponentPopupMenuListener);
    _findComponentComboBox.setMaximumRowCount(20);
  }

  /**
   * After made visible, I want the "find component" text in this box selected.  So, I have to have focus for that
   * to happen.  This is called after the scale drawing during a draw cad project load.
   * @author Andy Mechtenberg
   */
  void selectText()
  {
    _findComponentComboBox.requestFocus();
    _findComponentComboBox.getEditor().selectAll();  // this doesn't work, but I think it should!  I want the default text to be selected.
  }

  /**
   * @author Andy Mechtenberg
   */
  private void topSideToggleButton_actionPerformed(ActionEvent e)
  {
    showTopSide();
  }

  /**
   * @author Andy Mechtenberg
   * @Edited By Kee Chin Seong
   */
  void showTopSide()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      if(_configureCadGraphicEngineDiaglogBox.isSettingsAppliedByUser())
        _panelGraphicsEngineSetup.showTopSide();
      else
        _panelGraphicsEngineSetup.showTopSide();
    }
    else if(_boardGraphicsEngineSetup != null)
      _boardGraphicsEngineSetup.showTopSide();
    else
      Assert.expect(false);

    if (_allowDebugGraphicsConfiguration)
      hideReconstructionRegionsIfNecessary();
    clearCrossHairsIfNecessary();
    _topSideToggleButton.setSelected(true);
    _bottomSideToggleButton.setSelected(false);
    _bothSidesToggleButton.setSelected(false);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void bottomSideToggleButton_actionPerformed(ActionEvent e)
  {
    showBottomSide();
  }

  /**
   * @author Andy Mechtenberg
   * @Edited by Kee Chin Seong
   */
  void showBottomSide()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      if(_configureCadGraphicEngineDiaglogBox.isSettingsAppliedByUser())
        _panelGraphicsEngineSetup.showBottomSide();
      else
        _panelGraphicsEngineSetup.showBottomSide();
    }
    else if(_boardGraphicsEngineSetup != null)
      _boardGraphicsEngineSetup.showBottomSide();
    else
      Assert.expect(false);

    if (_allowDebugGraphicsConfiguration)
      hideReconstructionRegionsIfNecessary();
    clearCrossHairsIfNecessary();
    _topSideToggleButton.setSelected(false);
    _bottomSideToggleButton.setSelected(true);
    _bothSidesToggleButton.setSelected(false);
  }

  /**
   * @author Andy Mechtenberg
   * Edited by : Kee Chin Seong
   */
  void showBothSides()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      if(_configureCadGraphicEngineDiaglogBox.isSettingsAppliedByUser())
        _panelGraphicsEngineSetup.showBothSides();
      else
        _panelGraphicsEngineSetup.showBothSides();
    }
    else if(_boardGraphicsEngineSetup != null)
      _boardGraphicsEngineSetup.showBothSides();
    else
      Assert.expect(false);

    if (_allowDebugGraphicsConfiguration)
      hideReconstructionRegionsIfNecessary();
    clearCrossHairsIfNecessary();
    _topSideToggleButton.setSelected(false);
    _bottomSideToggleButton.setSelected(false);
    _bothSidesToggleButton.setSelected(true);
  }

  /**
   * @author George A. David
   */
  private void hideReconstructionRegionsIfNecessary()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      if(_configureReconstructionRegionsDialogBox.areVerificationRegionRectanglesVisible() == false)
        _panelGraphicsEngineSetup.setVerificationRegionRectangleLayersVisible(false);
      if(_configureReconstructionRegionsDialogBox.areVerificationFocusRegionsVisible() == false)
        _panelGraphicsEngineSetup.setVerificationFocusRegionLayersVisible(false);

      if(_configureReconstructionRegionsDialogBox.areInspectionRegionRectanglesVisible() == false)

        _panelGraphicsEngineSetup.setInspectionRegionRectangleLayersVisible(false);
      if(_configureReconstructionRegionsDialogBox.areInspectionRegionPadBoundsVisible() == false)
        _panelGraphicsEngineSetup.setInspectionRegionPadBoundsLayersVisible(false);
      if(_configureReconstructionRegionsDialogBox.areInspectionFocusRegionsVisible() == false)
        _panelGraphicsEngineSetup.setInspectionFocusRegionLayersVisible(false);

      if(_configureReconstructionRegionsDialogBox.areAlignmentRegionRectanglesVisible() == false)
        _panelGraphicsEngineSetup.setAlignmentRegionRectangleLayersVisible(false);
      if(_configureReconstructionRegionsDialogBox.areAlignmentRegionPadBoundsVisible() == false)
        _panelGraphicsEngineSetup.setAlignmentRegionPadBoundsLayersVisible(false);
      if(_configureReconstructionRegionsDialogBox.areAlignmentFocusRegionsVisible() == false)
        _panelGraphicsEngineSetup.setAlignmentFocusRegionLayersVisible(false);
    }
    else if(_boardGraphicsEngineSetup != null)
    {
      if(_configureReconstructionRegionsDialogBox.areVerificationRegionRectanglesVisible() == false)
        _boardGraphicsEngineSetup.setVerificationRegionRectangleLayersVisible(false);
      if(_configureReconstructionRegionsDialogBox.areVerificationFocusRegionsVisible() == false)
        _boardGraphicsEngineSetup.setVerificationFocusRegionLayersVisible(false);

      if(_configureReconstructionRegionsDialogBox.areInspectionRegionRectanglesVisible() == false)

        _boardGraphicsEngineSetup.setInspectionRegionRectangleLayersVisible(false);
      if(_configureReconstructionRegionsDialogBox.areInspectionRegionPadBoundsVisible() == false)
        _boardGraphicsEngineSetup.setInspectionRegionPadBoundsLayersVisible(false);
      if(_configureReconstructionRegionsDialogBox.areInspectionFocusRegionsVisible() == false)
        _boardGraphicsEngineSetup.setInspectionFocusRegionLayersVisible(false);

      if(_configureReconstructionRegionsDialogBox.areAlignmentRegionRectanglesVisible() == false)
        _boardGraphicsEngineSetup.setAlignmentRegionRectangleLayersVisible(false);
      if(_configureReconstructionRegionsDialogBox.areAlignmentRegionPadBoundsVisible() == false)
        _boardGraphicsEngineSetup.setAlignmentRegionPadBoundsLayersVisible(false);
      if(_configureReconstructionRegionsDialogBox.areAlignmentFocusRegionsVisible() == false)
        _boardGraphicsEngineSetup.setAlignmentFocusRegionLayersVisible(false);
    }
    else
      Assert.expect(false);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void bothSidesToggleButton_actionPerformed(ActionEvent e)
  {
    showBothSides();
  }

  /**
   * Recieves the current selected thing's description
   * @author Andy Mechtenberg
   */
  private void setDescription(String description)
  {
    _parent.setDescription(description);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void zoomInButton_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.zoom(_STANDARD_ZOOM);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void zoomOutButton_actionPerformed(ActionEvent e)
  {
    double zoomFactor = _graphicsEngine.getCurrentZoomFactor();

    // if we are at 1.0 (full zomm), do nothing
    if (MathUtil.fuzzyEquals(zoomFactor, 1.0))
      return;

    double proposedNewZoom = zoomFactor * (1.0 / _STANDARD_ZOOM);
    if (proposedNewZoom < 1.0)  // if we were to zoom and be at less than 1.0, go directly to 1.0 (full screen)
    {
      _graphicsEngine.fitGraphicsToScreen();
    }
    else
    {
      _graphicsEngine.zoom(1.0/_STANDARD_ZOOM);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void zoomRectangleToggleButton_actionPerformed(ActionEvent e)
  {
    if (_zoomRectangleToggleButton.isSelected())
    {
      _dragToggleButton.setSelected(false);
      _measureToggleButton.setSelected(false);
      _surfaceMappingDragToggleButton.setSelected(false);
//      _graphicsEngine.setGroupSelectMode(false);
      _graphicsEngine.setZoomRectangleMode(true);
    }
    else
    {
      _graphicsEngine.setZoomRectangleMode(false);
      if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment() == false)
        _graphicsEngine.setGroupSelectMode(true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void dragToggleButton_actionPerformed(ActionEvent e)
  {
    if (_dragToggleButton.isSelected())
    {
      _zoomRectangleToggleButton.setSelected(false);
      _measureToggleButton.setSelected(false);    
      //_graphicsEngine.setGroupSelectMode(false);
      _surfaceMappingDragToggleButton.setSelected(false); 
      _graphicsEngine.setDragGraphicsMode(true);   
    }
    else
    {
      _graphicsEngine.setDragGraphicsMode(false);
      if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment() == false)
        _graphicsEngine.setGroupSelectMode(true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void resetGraphicsButton_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.fitGraphicsToScreen();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void groupSelectButton_actionPerformed(ActionEvent e)
  {
    if (_groupSelectButton.isSelected())
    {
      _dragToggleButton.setSelected(false);
      _zoomRectangleToggleButton.setSelected(false);
      _graphicsEngine.setGroupSelectMode(true);
    }
    else
    {
      _graphicsEngine.setGroupSelectMode(false);
      if (TestDev.isInAutoPopulateMode())
        _graphicsEngine.setDragSelectedRegionMode(true);   
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void viewFromTopToggleButton_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.viewFromTop();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void viewFromBottomToggleButton_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.viewFromBottom();
  }

  /**
   * @author George A. David
   */
  private void measureToggleButton_actionPerformed(ActionEvent e)
  {
    if (_measureToggleButton.isSelected())
    {
      _zoomRectangleToggleButton.setSelected(false);
      _dragToggleButton.setSelected(false);
      _surfaceMappingDragToggleButton.setSelected(false);
      _graphicsEngine.setDragGraphicsMode(false);
      _graphicsEngine.setGroupSelectMode(false);
      _graphicsEngine.setMeasurementMode(true);
    }
    else
    {
      _graphicsEngine.setMeasurementMode(false);
      _graphicsEngine.setDragGraphicsMode(false);
      if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment() == false)
        _graphicsEngine.setGroupSelectMode(true);
    }
  }
  
   /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void surfaceMappingToggleButton_actionPerformed(ActionEvent e)
  {
    if (_surfaceMappingDragToggleButton.isSelected() && TestDev.isInAutoPopulateMode())
    {
      _graphicsEngine.clearMouseEventsInSurfaceMappingScreen();
      _zoomRectangleToggleButton.setSelected(false);
      _measureToggleButton.setSelected(false);
      _dragToggleButton.setSelected(false);
      _groupSelectButton.setSelected(false);
      _graphicsEngine.setDragOpticalRegionButtonIsToggle(true);
      _graphicsEngine.setDragGraphicsMode(true);
      _graphicsEngine.setDragRegionMode(true);
      _graphicsEngine.setGroupSelectMode(true);
      _graphicsEngine.setDragSelectedRegionMode(true);
      //Jack Hwee - remove Blue Rectangles when this button is clicked
      if (_panelGraphicsEngineSetup != null)
      {
        _panelGraphicsEngineSetup.clearHighlightedOpticalCameraRectangleList();
        _panelGraphicsEngineSetup.setOpticalRegionSelectedLayersVisible(false);
        _panelGraphicsEngineSetup.setMeshTriangleLayerVisible(false);
        _panelGraphicsEngineSetup.setDragOpticalRegionLayersVisible(true);
        
        _panelGraphicsEngineSetup.handleDragOpticalRegion();
      }
      if (_boardGraphicsEngineSetup != null)
      {
        _boardGraphicsEngineSetup.clearHighlightedOpticalCameraRectangleList();
        _boardGraphicsEngineSetup.setOpticalRegionSelectedLayersVisible(false);
        _boardGraphicsEngineSetup.setMeshTriangleLayerVisible(false);
        _boardGraphicsEngineSetup.setDragOpticalRegionLayersVisible(true);
        
        _boardGraphicsEngineSetup.handleDragOpticalRegion();
      }
      PspAutoPopulatePanel.getInstance().setAddRectangleMode(true);
    }
    else if (_surfaceMappingDragToggleButton.isSelected() && TestDev.isInSurfaceMapManualAlignmentMode())
    {
      clearDragAlignmentOpticalRegionOnCad();
      _graphicsEngine.clearMouseEventsInSurfaceMappingScreen();
      _zoomRectangleToggleButton.setSelected(false);
      _measureToggleButton.setSelected(false);
      _dragToggleButton.setSelected(false);
      _graphicsEngine.setDragOpticalRegionButtonIsToggle(true);
      _graphicsEngine.setDragGraphicsMode(false);
      _graphicsEngine.setGroupSelectMode(false);
      _graphicsEngine.setDragRegionMode(true);
      
      if (_boardGraphicsEngineSetup != null)
      {
        _boardGraphicsEngineSetup.setDragOpticalRegionLayersVisible(true);     
      }
      else
      {
        _panelGraphicsEngineSetup.setDragAlignmentOpticalRegionLayersVisible(true);
      }
    }
    else
    {
      clearDragAlignmentOpticalRegionOnCad();
      _graphicsEngine.clearMouseEventsInSurfaceMappingScreen();  
      _graphicsEngine.setDragOpticalRegionButtonIsToggle(false);     
      _graphicsEngine.setDragGraphicsMode(false);
      _graphicsEngine.setGroupSelectMode(false);
      _graphicsEngine.setDragRegionMode(false); 
      _graphicsEngine.setDragSelectedRegionMode(true);
      if (TestDev.isInSurfaceMapManualAlignmentMode() == false)
      {
        PspAutoPopulatePanel.getInstance().setAddRectangleMode(false);
      }
      
      if (_panelGraphicsEngineSetup != null)
      {
        _panelGraphicsEngineSetup.clearHighlightedOpticalCameraRectangleList();
        _panelGraphicsEngineSetup.setDragOpticalRegionLayersVisible(false);
        _panelGraphicsEngineSetup.setOpticalRegionSelectedLayersVisible(true);
        _panelGraphicsEngineSetup.setMeshTriangleLayerVisible(true);
      }
      else if (_boardGraphicsEngineSetup != null)
      {
        _boardGraphicsEngineSetup.clearHighlightedOpticalCameraRectangleList();
        _boardGraphicsEngineSetup.setDragOpticalRegionLayersVisible(false);
        _boardGraphicsEngineSetup.setOpticalRegionSelectedLayersVisible(true);
        _boardGraphicsEngineSetup.setMeshTriangleLayerVisible(true);
      }
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void updateGraphicEnginesRendererSettings(ActionEvent e)
  {
    if (_configureCadGraphicEngineDiaglogBox != null)
    {
      _configureCadGraphicEngineDiaglogBox.updateGraphicControllerStatus();
      _configureCadGraphicEngineDiaglogBox.setVisible(true);
      SwingUtils.centerOnComponent(_configureCadGraphicEngineDiaglogBox, this);
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void displayPadNameToggleButton_actionPerformed(ActionEvent e)
  {
    if (_displayPadNameToggleButton.isSelected())
    {
      _graphicsEngine.displayPadName(true);
    }
    else
    {
      _graphicsEngine.displayPadName(false);
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  private void displayNoLoadToggleButton_actionPerformed(ActionEvent e)
  {
    if (_displayNoLoadComponentToggleButton.isSelected())
    {
      _graphicsEngine.displayNoLoadComponent(true);
    }
    else
    {
      _graphicsEngine.displayNoLoadComponent(false);
    }
    populateFindComponentComboBox(_panel, false);
  }
  
  /**
   * XCR-3837 - Display untestable Area on virtual live CAD
   * @author Janan Wong
   */
  private void displayUntestableAreaToggleButton_actionPerformed(ActionEvent e)
  {
    if (_panel.hasUntestableArea())
    {
      if (MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment())
      {
        _graphicsEngine.displayUntestableArea(true);
      }
      else if (_displayUntestableAreaToggleButton.isSelected())
      {
        _graphicsEngine.displayUntestableArea(true);
      }
      else
      {
        _graphicsEngine.displayUntestableArea(false);
      }
      MainMenuGui.getInstance().getTestDev().getPersistance().setDisplayUntestableArea(_displayUntestableAreaToggleButton.isSelected());
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void previousBoardButton_actionPerformed(ActionEvent e)
  {
    PspAutoPopulatePanel.getInstance().displayPreviousBoard();
    if (_panelGraphicsEngineSetup != null)
      _panelGraphicsEngineSetup.showTopSide();
    if (_boardGraphicsEngineSetup != null)
      _boardGraphicsEngineSetup.showTopSide();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void nextBoardButton_actionPerformed(ActionEvent e)
  {
    PspAutoPopulatePanel.getInstance().displayNextBoard();
    if (_panelGraphicsEngineSetup != null)
      _panelGraphicsEngineSetup.showTopSide();
    if (_boardGraphicsEngineSetup != null)
      _boardGraphicsEngineSetup.showTopSide();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void findComponent()
  {
    _graphicsEngine.setDragRegionModeAvailable(false);
    
    _graphicsEngine.clearMouseEventsInSurfaceMappingScreen();
   
    // since this combo box is editable, this call will get the currently selected item, whether chosen or typed in
    Object selectedObject = _findComponentComboBox.getEditor().getItem();
    if (selectedObject instanceof String) // user typed in something, so now we need to find the renderer, if it exists
    {
      if (selectedObject.equals(StringLocalizer.keyToString("DCGUI_FINDCOMPONENT_SELECTION_KEY")))
        return;
      _rendererWithCrosshairs = null;  // if we don't find one with the string, we'll clear any selected component and pop up an error
      String selectedName = (String)selectedObject;

      int itemCount = _findComponentComboBox.getItemCount();
      for(int i = 0; i < itemCount; i++)
      {
        Object comboBoxEntry = _findComponentComboBox.getItemAt(i);
        if (comboBoxEntry instanceof ComponentRenderer)
        {
          ComponentRenderer compRenderer = (ComponentRenderer)comboBoxEntry;
          if (compRenderer.getComponent().getReferenceDesignator().equalsIgnoreCase(selectedName))
          {
            _rendererWithCrosshairs = compRenderer;
            _findComponentComboBox.setSelectedIndex(i);
            break;
          }
        }
        else if (comboBoxEntry instanceof ComponentType)
        {
          ComponentType componentType = (ComponentType)comboBoxEntry;
          if (componentType.getReferenceDesignator().equalsIgnoreCase(selectedName))
          {
            if(_panelGraphicsEngineSetup != null)
            {
              if(componentType.isLoaded() == false && _displayNoLoadComponentToggleButton.isSelected() == true)
                 _rendererWithCrosshairs = _panelGraphicsEngineSetup.getNoLoadComponentRenderers(componentType).get(0);
              else
                 _rendererWithCrosshairs = _panelGraphicsEngineSetup.getComponentRenderers(componentType).get(0);
            }
            else if(_boardGraphicsEngineSetup != null)
            {
              if(componentType.isLoaded() == false && _displayNoLoadComponentToggleButton.isSelected() == true)
                 _rendererWithCrosshairs = _boardGraphicsEngineSetup.getNoLoadComponentRenderers(componentType).get(0);
              else
                 _rendererWithCrosshairs = _boardGraphicsEngineSetup.getComponentRenderers(componentType).get(0);
            }
            else
              Assert.expect(false);
            
            _findComponentComboBox.setSelectedIndex(i);
            // at this point, the action listener will kick in and handle the acual work of the selection.  If we
            // break here, then we'll get called twice which can be a problem.
            return;
//            break;
          }
        }
        else if (comboBoxEntry instanceof FiducialType)
        {
          // do something good
          FiducialType fiducialType = (FiducialType)comboBoxEntry;
          if (fiducialType.getName().equalsIgnoreCase(selectedName))
          {
            if(_panelGraphicsEngineSetup != null)
              _rendererWithCrosshairs = _panelGraphicsEngineSetup.getFiducialRenderers(fiducialType).get(0);
            else if(_boardGraphicsEngineSetup != null)
              _rendererWithCrosshairs = _boardGraphicsEngineSetup.getFiducialRenderers(fiducialType).get(0);

            _findComponentComboBox.setSelectedIndex(i);
            // at this point, the action listener will kick in and handle the acual work of the selection.  If we
            // break here, then we'll get called twice which can be a problem.
            return;
          }
        }
      }
    }
    else if (selectedObject instanceof ComponentRenderer)
    {         
      Assert.expect(selectedObject instanceof ComponentRenderer);
      _rendererWithCrosshairs = (ComponentRenderer)selectedObject;
    }
    else if (selectedObject instanceof ComponentType)
    {
      ComponentType componentType = (ComponentType)selectedObject;
      if(_panelGraphicsEngineSetup != null)
      {
        if(componentType.isLoaded() == false && _displayNoLoadComponentToggleButton.isSelected() == true)
          _rendererWithCrosshairs = _panelGraphicsEngineSetup.getNoLoadComponentRenderers(componentType).get(0);
        else
          _rendererWithCrosshairs = _panelGraphicsEngineSetup.getComponentRenderers(componentType).get(0);
      }
      else if(_boardGraphicsEngineSetup != null)
      {
        if(componentType.isLoaded() == false && _displayNoLoadComponentToggleButton.isSelected() == true)
          _rendererWithCrosshairs = _boardGraphicsEngineSetup.getNoLoadComponentRenderers(componentType).get(0);
        else
          _rendererWithCrosshairs = _boardGraphicsEngineSetup.getComponentRenderers(componentType).get(0);
      }
      else
        Assert.expect(false);
    }
    else if (selectedObject instanceof FiducialType)
    {
      FiducialType fiducialType = (FiducialType)selectedObject;
      if(_panelGraphicsEngineSetup != null)
      {
        java.util.List<FiducialRenderer> renderers = _panelGraphicsEngineSetup.getFiducialRenderers(fiducialType);
        _rendererWithCrosshairs = renderers.get(0);
      }
      else if(_boardGraphicsEngineSetup != null)
      {
        java.util.List<FiducialRenderer> renderers = _boardGraphicsEngineSetup.getFiducialRenderers(fiducialType);
        _rendererWithCrosshairs = renderers.get(0);
      }
      else
        Assert.expect(false);
    }


    // this will be null if the user typed something that didn't exist in the combo box
    if (_rendererWithCrosshairs == null)
    {
      String currentComponent = (String)selectedObject;
      if (_componentNotFound.equalsIgnoreCase(currentComponent))
      {
        String instructionString = StringLocalizer.keyToString("DCGUI_FINDCOMPONENT_SELECTION_KEY");
        _findComponentComboBox.setSelectedItem(instructionString);
        _findComponentComboBox.getEditor().selectAll();
        return;
      }
      LocalizedString message = new LocalizedString("DCGUI_COMPONENT_NOT_FOUND_KEY", new Object[]{currentComponent});
      JOptionPane.showMessageDialog(_parent,
                                    StringLocalizer.keyToString(message),
                                    StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      _componentNotFound = currentComponent;
    }
    else
    {
      _componentNotFound = "";
      highlightRenderer(_rendererWithCrosshairs);
    }
  }

  /**
   * @author George A. David
   */
  public void configureReconstructionRegionsButton_actionPerformed(ActionEvent e)
  {
    _configureReconstructionRegionsDialogBox.setVisible(true);
    SwingUtils.centerOnComponent(_configureReconstructionRegionsDialogBox, this);
  }

  /**
   * @author George A. David
   */
  void updateVerificationRegionsVisibility()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setVerificationRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areVerificationRegionRectanglesVisible());
      _panelGraphicsEngineSetup.setVerificationRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areVerificationFocusRegionsVisible());
    }
    else if(_boardGraphicsEngineSetup != null)
    {
      _boardGraphicsEngineSetup.setVerificationRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areVerificationRegionRectanglesVisible());
      _boardGraphicsEngineSetup.setVerificationRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areVerificationFocusRegionsVisible());
    }
    else
      Assert.expect(false);
    showCurrentSides();
  }

  /**
   * @author George A. David
   */
  void updateInspectionRegionsVisibility()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setInspectionRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areInspectionRegionRectanglesVisible());
      _panelGraphicsEngineSetup.setInspectionRegionPadBoundsLayersVisible(_configureReconstructionRegionsDialogBox.areInspectionRegionPadBoundsVisible());
      _panelGraphicsEngineSetup.setInspectionRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areInspectionFocusRegionsVisible());
    }
    else if(_boardGraphicsEngineSetup != null)
    {
      _boardGraphicsEngineSetup.setInspectionRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areInspectionRegionRectanglesVisible());
      _boardGraphicsEngineSetup.setInspectionRegionPadBoundsLayersVisible(_configureReconstructionRegionsDialogBox.areInspectionRegionPadBoundsVisible());
      _boardGraphicsEngineSetup.setInspectionRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areInspectionFocusRegionsVisible());
    }
    else
      Assert.expect(false);

    showCurrentSides();
  }

  /**
   * @author George A. David
   */
  void updateAlignmentRegionsVisibility()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setAlignmentRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areAlignmentRegionRectanglesVisible());
      _panelGraphicsEngineSetup.setAlignmentRegionPadBoundsLayersVisible(_configureReconstructionRegionsDialogBox.areAlignmentRegionPadBoundsVisible());
      _panelGraphicsEngineSetup.setAlignmentRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areAlignmentFocusRegionsVisible());
    }
    else if(_boardGraphicsEngineSetup != null)
    {
      _boardGraphicsEngineSetup.setAlignmentRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areAlignmentRegionRectanglesVisible());
      _boardGraphicsEngineSetup.setAlignmentRegionPadBoundsLayersVisible(_configureReconstructionRegionsDialogBox.areAlignmentRegionPadBoundsVisible());
      _boardGraphicsEngineSetup.setAlignmentRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areAlignmentFocusRegionsVisible());
    }
    else
      Assert.expect(false);
    showCurrentSides();
  }

  /**
   * @author George A. David
   */
  void updateIrpBoundariesVisibility()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setVerificationIrpBoundariesLayerVisible(_configureReconstructionRegionsDialogBox.areVerificationIrpBoundariesVisible());
      _panelGraphicsEngineSetup.setInspectionIrpBoundariesLayerVisible(_configureReconstructionRegionsDialogBox.areInspectionIrpBoundariesVisible());
    }
    else if(_boardGraphicsEngineSetup != null)
    {
      _boardGraphicsEngineSetup.setVerificationIrpBoundariesLayerVisible(_configureReconstructionRegionsDialogBox.areVerificationIrpBoundariesVisible());
      _boardGraphicsEngineSetup.setInspectionIrpBoundariesLayerVisible(_configureReconstructionRegionsDialogBox.areInspectionIrpBoundariesVisible());

    }
    else
      Assert.expect(false);

    //showCurrentSides();
  }

  /**
   * @author George A. David
   */
  void updateReconstructionRegionRectangleVisibility()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setAlignmentRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areAlignmentRegionRectanglesVisible());
      _panelGraphicsEngineSetup.setInspectionRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areInspectionRegionRectanglesVisible());
      _panelGraphicsEngineSetup.setVerificationRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areVerificationRegionRectanglesVisible());
    }
    else if(_boardGraphicsEngineSetup != null)
    {
      _boardGraphicsEngineSetup.setAlignmentRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areAlignmentRegionRectanglesVisible());
      _boardGraphicsEngineSetup.setInspectionRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areInspectionRegionRectanglesVisible());
      _boardGraphicsEngineSetup.setVerificationRegionRectangleLayersVisible(_configureReconstructionRegionsDialogBox.areVerificationRegionRectanglesVisible());
    }
    showCurrentSides();
  }

  /**
   * @author George A. David
   */
  void updateReconstructionRegionPadBoundsVisibility()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setAlignmentRegionPadBoundsLayersVisible(_configureReconstructionRegionsDialogBox.areAlignmentRegionPadBoundsVisible());
      _panelGraphicsEngineSetup.setInspectionRegionPadBoundsLayersVisible(_configureReconstructionRegionsDialogBox.areInspectionRegionPadBoundsVisible());
    }
    else if(_boardGraphicsEngineSetup != null)
    {
      _boardGraphicsEngineSetup.setAlignmentRegionPadBoundsLayersVisible(_configureReconstructionRegionsDialogBox.areAlignmentRegionPadBoundsVisible());
      _boardGraphicsEngineSetup.setInspectionRegionPadBoundsLayersVisible(_configureReconstructionRegionsDialogBox.areInspectionRegionPadBoundsVisible());
    }
    else
      Assert.expect(false);
    showCurrentSides();
  }

  /**
   * @author George A. David
   */
  void updateReconstructionFocusRegionsVisibility()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setAlignmentFocusRegionLayersVisible(_configureReconstructionRegionsDialogBox.areAlignmentFocusRegionsVisible());
      _panelGraphicsEngineSetup.setInspectionFocusRegionLayersVisible(_configureReconstructionRegionsDialogBox.areInspectionFocusRegionsVisible());
      _panelGraphicsEngineSetup.setVerificationFocusRegionLayersVisible(_configureReconstructionRegionsDialogBox.areVerificationFocusRegionsVisible());
    }
    else if(_boardGraphicsEngineSetup != null)
    {
      _boardGraphicsEngineSetup.setAlignmentFocusRegionLayersVisible(_configureReconstructionRegionsDialogBox.areAlignmentFocusRegionsVisible());
      _boardGraphicsEngineSetup.setInspectionFocusRegionLayersVisible(_configureReconstructionRegionsDialogBox.areInspectionFocusRegionsVisible());
      _boardGraphicsEngineSetup.setVerificationFocusRegionLayersVisible(_configureReconstructionRegionsDialogBox.areVerificationFocusRegionsVisible());
    }
    else
      Assert.expect(false);
    showCurrentSides();
  }
  
  /*
   * @author : Kee Chin Seong
   */
  void updateTopLayerGraphicVisiblity()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setTopLayersVisible(_configureCadGraphicEngineDiaglogBox.areTopLayerGraphicVisible());
    }
    else if(_boardGraphicsEngineSetup != null)
    {
      //_boardGraphicsEngineSetup.setAlignmentFocusRegionLayersVisible(_configureReconstructionRegionsDialogBox.areAlignmentFocusRegionsVisible());
      //_boardGraphicsEngineSetup.setInspectionFocusRegionLayersVisible(_configureReconstructionRegionsDialogBox.areInspectionFocusRegionsVisible());
      //_boardGraphicsEngineSetup.setVerificationFocusRegionLayersVisible(_configureReconstructionRegionsDialogBox.areVerificationFocusRegionsVisible());
    }
    else
      Assert.expect(false);
    
    updateBoardViewButton();
  }
  
  /*
   * @author : Kee Chin Seong
   */
  void updateTopPanelGraphicLayerVisibility()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setTopPanelLayersVisible(_configureCadGraphicEngineDiaglogBox.areTopPanelGraphicLayerVisible());
    }
    else
      Assert.expect(false);
    
    updateBoardViewButton();
  }
  
  /*
   * @author : Kee Chin Seong
   */
  void updateTopComponentLayerVisibility()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setTopComponentLayersVisible(_configureCadGraphicEngineDiaglogBox.areTopComponentGraphicLayerVisible(), _panel);
    }
    else
      Assert.expect(false);
    
    updateBoardViewButton();
  }
  
  /*
   * @author : Kee Chin Seong
   */
  void updateTopRefDesLayerVisibility()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setTopReferenceDesignatorLayerVisibility(_configureCadGraphicEngineDiaglogBox.areTopCompRefDesGraphicLayerVisible(), _panel);
    }
    else
      Assert.expect(false);
    
    updateBoardViewButton();
  }
  
  /*
   * @author : Kee Chin Seong
   */
  void updateBottomRefDesLayerVisibility()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setBottomReferenceDesignatorLayerVisibility(_configureCadGraphicEngineDiaglogBox.areBottomCompRefDesGraphicLayerVisible(), _panel);
    }
    else
      Assert.expect(false);
    
    updateBoardViewButton();
  }
  
  /*
   * @author : Kee Chin Seong
   */
  void updateTopPadGraphicLayerVisiblity()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setTopPadLayerVisibility(_configureCadGraphicEngineDiaglogBox.areTopPadGraphicLayerVisible());
    }
    //else if(_boardGraphicsEngineSetup != null)
    //{
    //  _boardGraphicsEngineSetup.setTopPadLayerVisibility(_configureCadGraphicEngineDiaglogBox.areTopPadGraphicLayerVisible());
    //}
    else
      Assert.expect(false);
    
    updateBoardViewButton();
  }
  
   /*
   * @author : Kee Chin Seong
   */
  void updateBottomLayerGraphicVisiblity()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setBottomLayersVisible(_configureCadGraphicEngineDiaglogBox.areBottomLayerGraphicVisible());
    }
    //else if(_boardGraphicsEngineSetup != null)
    //{
    //  _boardGraphicsEngineSetup.setBottomLayersVisible(_configureCadGraphicEngineDiaglogBox.areBottomLayerGraphicVisible());
    //}
    else
      Assert.expect(false);
    
    updateBoardViewButton();
  }
  
  /*
   * @author : Kee Chin Seong
   */
  void updateBottomPanelGraphicLayerVisibility()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setBottomPanelLayersVisible(_configureCadGraphicEngineDiaglogBox.areBottomPanelGraphicLayerVisible());
    }
    else
      Assert.expect(false);
    
    updateBoardViewButton();
  }
  
  /*
   * @author : Kee Chin Seong
   */
  void updateBottomComponentLayerVisibility()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setBottomComponentLayersVisible(_configureCadGraphicEngineDiaglogBox.areBottomComponentGraphicLayerVisible(), _panel);
    }
    
    //if(_boardGraphicsEngineSetup != null)
    //{
    //  _boardGraphicsEngineSetup.setBottomComponentLayersVisible(_configureCadGraphicEngineDiaglogBox.areBottomComponentGraphicLayerVisible(), _panel);
    //}       
    else
      Assert.expect(false);
    
    updateBoardViewButton();
  }
  
  /*
   * @author : Kee Chin Seong
   */
  void updateBottomPadGraphicLayerVisiblity()
  {
    if(_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.setBottomPadLayerVisibility(_configureCadGraphicEngineDiaglogBox.areBottomPadGraphicLayerVisible());
    }
    else
      Assert.expect(false);
    
    updateBoardViewButton();
  }
  
  /*
   * @author Kee Chin Seong
   */
  boolean isTopLayerGraphicVisible()
  {
    return _panelGraphicsEngineSetup.isTopSideVisible();
  }
  
  /*
   * @author Kee Chin Seong
   */
  boolean isBottomLayerGraphicVisible()
  {
    return _panelGraphicsEngineSetup.isBottomSideVisible();
  }
  
  /*
   * @author Kee Chin Seong
   */
  boolean isTopLayerPanelGraphicVisible()
  {
    return _panelGraphicsEngineSetup.isTopPanelLayersVisible();
  }
  
  /*
   * @author Kee Chin Seong
   */
  boolean isBottomLayerPanelGraphicVisible()
  {
    return _panelGraphicsEngineSetup.isBottomPanelLayersVisible();
  }
  
  /*
   * @author Kee Chin Seong
   */
  boolean isTopPadGraphicLayersVisible()
  {
    return _panelGraphicsEngineSetup.isTopPadLayersVisible();
  }
  
  /*
   * @author Kee Chin Seong
   */
  boolean isTopCompRefDesGraphicLayersVisible()
  {
    return _panelGraphicsEngineSetup.isTopComponentRefDesLayersVisible();
  }
  
  /*
   * @author Kee Chin Seong
   */
  boolean isBottomCompRefDesGraphicLayersVisible()
  {
    return _panelGraphicsEngineSetup.isBottomComponentRefDesLayersVisible();
  }
    
  /*
   * @author Kee Chin Seong
   */
  boolean isTopComponentLayerGraphicVisible()
  {
    return _panelGraphicsEngineSetup.isTopComponentLayersVisible();
  }
  
  /*
   * @author Kee Chin Seong
   */
  boolean isBottomPadGraphicLayersVisible()
  {
    return _panelGraphicsEngineSetup.isBottomPadLayersVisible();
  }
  
    
  /*
   * @author Kee Chin Seong
   */
  boolean isBottomComponentLayerGraphicVisible()
  {
    return _panelGraphicsEngineSetup.isBottomComponentLayersVisible();
  }

   /*
   * @author Kee Chin Seong
   */
  void updateBoardViewButton()
  {
    if(_panelGraphicsEngineSetup.isTopSideVisible() && _panelGraphicsEngineSetup.isBottomSideVisible())
      _bothSidesToggleButton.setSelected(true);
    else if(_panelGraphicsEngineSetup.isTopSideVisible())
      _topSideToggleButton.setSelected(true);
    else 
      _bottomSideToggleButton.setSelected(true);
  }
  
  /**
   * @author George A. David
   */
  private void showCurrentSides()
  {
    if(_topSideToggleButton.isSelected())
      showTopSide();

    if(_bottomSideToggleButton.isSelected())
      showBottomSide();
  }
 
   /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  public void changeToSurfaceMappingToolBar(Boolean inSurfaceMap)
  {
    if (inSurfaceMap && TestDev.isInAutoPopulateMode())
    {
      _surfaceMappingDragToggleButton.setVisible(true);
      _groupSelectButton.setVisible(true);
      if (_surfaceMappingDragToggleButton.isSelected()) 
      {
        if (_panel.getPanelSettings().isPanelBasedAlignment())
        {
          _panelGraphicsEngineSetup.setDragOpticalRegionLayersVisible(true);
          _panelGraphicsEngineSetup.setOpticalRegionUnSelectedLayerVisible(true);
        }
        else
        {
          _boardGraphicsEngineSetup.setDragOpticalRegionLayersVisible(true);
          _boardGraphicsEngineSetup.setOpticalRegionUnSelectedLayerVisible(true);
        }
        PspAutoPopulatePanel.getInstance().setAddRectangleMode(true);
      } 
      else 
      {
        _graphicsEngine.setDragRegionMode(false);
        if (_panelGraphicsEngineSetup != null)
          _panelGraphicsEngineSetup.setDragOpticalRegionLayersVisible(false);
        if (_boardGraphicsEngineSetup != null)
          _boardGraphicsEngineSetup.setDragOpticalRegionLayersVisible(false);
        PspAutoPopulatePanel.getInstance().setAddRectangleMode(false);
      }
      if (Project.getCurrentlyLoadedProject().getPanel().getPanelSettings().isPanelBasedAlignment())
      {
        _previousBoardButton.setVisible(false);
        _nextBoardButton.setVisible(false);
      }
      else
      {
        _previousBoardButton.setVisible(true);
        _nextBoardButton.setVisible(true);
      }
    }
    else if (inSurfaceMap && TestDev.isInSurfaceMapManualAlignmentMode())
    {
      _graphicsEngine.clearMouseEventsInSurfaceMappingScreen();
      _surfaceMappingDragToggleButton.setVisible(true);
      _previousBoardButton.setVisible(false);
      _nextBoardButton.setVisible(false);
      _groupSelectButton.setVisible(true);
    
      if (_surfaceMappingDragToggleButton.isSelected())
      {
        _graphicsEngine.clearMouseEventsInSurfaceMappingScreen();
        _zoomRectangleToggleButton.setSelected(false);
        _measureToggleButton.setSelected(false);
        _dragToggleButton.setSelected(false);
        _graphicsEngine.setDragGraphicsMode(false);
        _graphicsEngine.setGroupSelectMode(false);
        _graphicsEngine.setDragRegionMode(true);

        if (_boardGraphicsEngineSetup != null)
          _boardGraphicsEngineSetup.setDragAlignmentOpticalRegionLayersVisible(true);
        else
          _panelGraphicsEngineSetup.setDragAlignmentOpticalRegionLayersVisible(true);
      }
      else
      {
        _graphicsEngine.clearMouseEventsInSurfaceMappingScreen();
        _graphicsEngine.setDragRegionMode(false);
        if (_panelGraphicsEngineSetup != null)
          _panelGraphicsEngineSetup.setDragAlignmentOpticalRegionLayersVisible(false);
        if (_boardGraphicsEngineSetup != null)
          _boardGraphicsEngineSetup.setDragAlignmentOpticalRegionLayersVisible(false);
      }
    }
    else 
    {  
      _surfaceMappingDragToggleButton.setSelected(false);
      _surfaceMappingDragToggleButton.setVisible(false);
      _groupSelectButton.setSelected(false);
      _groupSelectButton.setVisible(false);
    }
    updateUI();
    this.repaint();
  }
  
    /**
   * @author Kee Chin Seong
   */
  public void changeToScanPathToolBar(Boolean isScanPathReviewEnable)
  {
    if (isScanPathReviewEnable)
    {
      _graphicsEngine.clearMouseEventsInSurfaceMappingScreen();
      _surfaceMappingDragToggleButton.setVisible(true);
      _groupSelectButton.setVisible(true);
      if (_surfaceMappingDragToggleButton.isSelected())
      {
        _graphicsEngine.clearMouseEventsInSurfaceMappingScreen();
        _zoomRectangleToggleButton.setSelected(false);
        _measureToggleButton.setSelected(false);
        _dragToggleButton.setSelected(false);
        _graphicsEngine.setDragGraphicsMode(false);
        _graphicsEngine.setGroupSelectMode(false);
        _graphicsEngine.setDragRegionMode(false);
        _panelGraphicsEngineSetup.setDragOpticalRegionLayersVisible(false);
        _panelGraphicsEngineSetup.setOpticalRegionUnSelectedLayerVisible(false);
      }
      else
      {
        _graphicsEngine.clearMouseEventsInSurfaceMappingScreen();
        _graphicsEngine.setDragRegionMode(false);
        _graphicsEngine.setDragGraphicsMode(false);
        _graphicsEngine.setGroupSelectMode(false);
        _panelGraphicsEngineSetup.setDragOpticalRegionLayersVisible(false);
      }
    }
    else
    {
      _graphicsEngine.clearMouseEventsInSurfaceMappingScreen();
      _graphicsEngine.setDragRegionMode(false);


      _surfaceMappingDragToggleButton.setSelected(false);
      _surfaceMappingDragToggleButton.setVisible(false);
      _groupSelectButton.setSelected(false);
      _groupSelectButton.setVisible(false);
    }
    updateUI();
    this.repaint();
  }
  
    /**
   * @author Jack Hwee
   */
  private void clearDragAlignmentOpticalRegionOnCad()
  {   
    if (_board != null)
    {
      _boardGraphicsEngineSetup.setAlignmentDividedOpticalRegionList(new LinkedHashMap<Rectangle, String>());
      _boardGraphicsEngineSetup.handleDragOpticalRegionInAlignment();
      _graphicsEngine.resetSelectedRegionCoordinate();
      // _testDev.getBoardGraphicsEngineSetup().setDragAlignmentOpticalRegionLayersVisible(false);
      _graphicsEngine.setRectangleInPixels(0, 0, 0, 0);
      _graphicsEngine.repaint();
    }
    else
    {
      _panelGraphicsEngineSetup.setAlignmentDividedOpticalRegionList(new LinkedHashMap<Rectangle, String>());
      //_panelGraphicsEngineSetup.setDividedOpticalRegionList(new LinkedHashMap<Rectangle, BooleanRef>());
      _panelGraphicsEngineSetup.handleDragOpticalRegionInAlignment();
      //_panelGraphicsEngineSetup.handleDragOpticalRegion();
      _graphicsEngine.resetSelectedRegionCoordinate();
      _panelGraphicsEngineSetup.setDragOpticalRegionLayersVisible(false);
      _panelGraphicsEngineSetup.setDragAlignmentOpticalRegionLayersVisible(false);
      _graphicsEngine.setRectangleInPixels(0, 0, 0, 0);
      _graphicsEngine.repaint();
    }
  }
  
  /**
   * @author:sham
   */
  private void selectRegionToggleButton_actionPerformed(ActionEvent e)
  {
    //swee yee - 13 June 2014
    //return the optimal maximum select region size from virtual live panel
    double maximumImageRegionWidthInNanometers = VirtualLiveImageGenerationSettings.getInstance().getMaxImageRegionWidth();
    double maximumImageRegionHeightInNanometers = VirtualLiveImageGenerationSettings.getInstance().getMaxImageRegionHeight();
   
    if (_selectRegionToggleButton.isSelected())
    {
      _zoomRectangleToggleButton.setSelected(false);
      _dragToggleButton.setSelected(false);
      _graphicsEngine.setGroupSelectMode(false);
      _graphicsEngine.setSelectRegionMode(true, maximumImageRegionWidthInNanometers, maximumImageRegionHeightInNanometers);
    }
    else
    {
      _graphicsEngine.setGroupSelectMode(false);
      _graphicsEngine.setSelectRegionMode(false, maximumImageRegionWidthInNanometers, maximumImageRegionHeightInNanometers);
    }
  }

/**
 * @author sham
 */
  public void setEnableSelectRegionToggleButon(boolean enable)
  {
    //swee yee - 16 June 2014
    if (_selectRegionToggleButton.isSelected())
    {
      if(enable == true)
      {
        // Reset to previous mode when switch back to virtualLive
        _selectRegionToggleButton.setSelected(true);
        _zoomRectangleToggleButton.setSelected(false);
        _dragToggleButton.setSelected(false);
        _graphicsEngine.setGroupSelectMode(false);
        _graphicsEngine.setSelectRegionMode(true, VirtualLiveImageGenerationSettings.getInstance().getMaxImageRegionWidth(), VirtualLiveImageGenerationSettings.getInstance().getMaxImageRegionHeight());
      }
    }
    _selectRegionToggleButton.setEnabled(enable);
  }

  /**
   * @author sham
   */
  public void setShouldGraphicsBeFitToScreen(boolean shouldGraphicsBeFitToScreen)
  {
    _shouldGraphicsBeFitToScreen = shouldGraphicsBeFitToScreen;
    _graphicsEngine.setShouldGraphicsBeFitToScreen(shouldGraphicsBeFitToScreen);
  }
  
  /**
   * This will enable majority of the buttons and other controls for this tool bar
   * Used when we re-initialize to display something
   * @author Andy Mechteberg
   * @author sham
   */
  public void enableAllControlsWithExcludeList(java.util.List<String> excludeList)
  {
    Component components[] = this.getComponents();
    for (int i = 0; i < components.length; i++)
    {
      Component comp = components[i];
      // if this is a JPanel, then we can't just disable the panel, we have to disable all the controls in the panel
      // I suppose this should be recursive, but hopefully only one level of nesting will occur.
      if (comp instanceof JPanel)
      {
        Component nestedComponents[] = ((JPanel)comp).getComponents();
        for (int j = 0; j < nestedComponents.length; j++)
        {
          Component nestedComp = nestedComponents[j];
          if(excludeList.contains(nestedComp.getName())==false)
            nestedComp.setEnabled(true);
        }
      }
      else
      {
        if(excludeList.contains(comp.getName())==false)
          comp.setEnabled(true);
      }
    }
  }
  
   /**
   * @author Jack Hwee
   */
  public void updateGroupSelectButton(Boolean isGroupSelectMode)
  {
    _groupSelectButton.setSelected(isGroupSelectMode);
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  public String getDisplayPadNameToggleButtonName()
  {
    return _displayPadNameToggleButton.getName();
  }
  
  /**
   * @author Kee Chin Seong
   * @return 
   */
  public String getDisplayNoLoadComponentToggleButton()
  {
    return _displayNoLoadComponentToggleButton.getName();
  }

  /**
   * @author Janan Wong
   * XCR-3837: Display untestable Area on virtual live CAD
   */
  public void setDisplayUntestableAreaToggleButton(boolean enable)
  {
    _displayUntestableAreaToggleButton.setSelected(enable);
  }
  
  /**
   * @author Janan Wong
   * XCR-3837: Display untestable Area on virtual live CAD
   */
  public void setEnableDisplayUntestableAreaToggleButton(boolean enable)
  {
    _displayUntestableAreaToggleButton.setEnabled(enable);
  }
}
