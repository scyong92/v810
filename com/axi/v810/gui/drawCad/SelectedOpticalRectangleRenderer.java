/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.drawCad;

import java.awt.*;

import com.axi.guiUtil.*;
import com.axi.util.*;

/**
 *
 * @author Jack Hwee
 */
public class SelectedOpticalRectangleRenderer extends ShapeRenderer
{
  com.axi.v810.business.panelSettings.OpticalCameraRectangle _opticalCameraRectangle = null;
  private boolean _isEnabled = false;

  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  protected SelectedOpticalRectangleRenderer(com.axi.v810.business.panelSettings.OpticalCameraRectangle opticalCameraRectangle, boolean isEnabled)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);

    Assert.expect(opticalCameraRectangle != null);
    _opticalCameraRectangle = opticalCameraRectangle;
    _isEnabled = isEnabled;
    
    refreshData();
  }

  /**
   * @author Jack Hwee
   */
  protected void refreshData()
  {
    Assert.expect(_opticalCameraRectangle != null);

    initializeShape(_opticalCameraRectangle.getRegion().getRectangle2D());
  }

  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    if (_isEnabled == false)
    {
      // fill in translucent gray colour.
      Color gray = Color.gray;
      Color renderColor = new Color(gray.getRed(), gray.getGreen(), gray.getBlue(), 200);  // translucent gray
      graphics2D.setColor(renderColor);
      graphics2D.fill(getShape());
    }
    graphics2D.setColor(Color.BLUE);
    graphics2D.setStroke(new BasicStroke(5));
    graphics2D.draw(getShape());
  }
}
