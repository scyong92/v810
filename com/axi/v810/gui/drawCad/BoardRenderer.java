package com.axi.v810.gui.drawCad;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This class is responsible for drawing the Board outline.
 *
 * @author Bill Darbie
 */
public class BoardRenderer extends ShapeRenderer
{
  private Board _board;

  /**
   * @author Andy Mechtenberg
   */
  BoardRenderer(Board board)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(board != null);

    _board = board;

    refreshData();
  }

  /**
   * Return the name of the board
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    return _board.getName();
  }

  /**
   * @author Bill Darbie
   */
  protected void refreshData()
  {
    Assert.expect(_board != null);

    initializeShape(_board.getShapeRelativeToPanelInNanoMeters());
  }

  /**
   * @return Board object associated with renderer
   * @author Carli Connally
   */
  public Board getBoard()
  {
    Assert.expect(_board != null);

    return _board;
  }
}
