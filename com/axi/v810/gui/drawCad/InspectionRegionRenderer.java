package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.business.testProgram.*;

/**
 * This class is responsible for drawing the InspectionRegions.
 *
 * @author Andy Mechtenberg
 */
public class InspectionRegionRenderer extends ShapeRenderer
{
  private ReconstructionRegion _inspectionRegion;
  private boolean _usePadBounds = false;

  /**
   * @author Andy Mechtenberg
   */
  InspectionRegionRenderer(ReconstructionRegion inspectionRegion)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(inspectionRegion != null);

    _inspectionRegion = inspectionRegion;

    refreshData();
  }

  /**
   * Return the name of the inspection region
   * @author Andy Mechtenberg
   */
  public String toString()
  {
   Integer viewNum = new Integer(_inspectionRegion.getRegionId());
   return "Inspection Region ID: " + viewNum.toString() +  " (mils X: " +
         (int)MathUtil.convertNanoMetersToMils(_inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinX()) + " Y: " +
         (int)MathUtil.convertNanoMetersToMils(_inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinY()) +
         " DX: " + (int)MathUtil.convertNanoMetersToMils(_inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters().getWidth()) +
         " DY: " + (int)MathUtil.convertNanoMetersToMils(_inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters().getHeight()) + "Focus Priority: " + _inspectionRegion.getFocusPriority().getId() + ")";
  }

  /**
   * Pad 1 needs to be hilighted (colored in RED), so we'll override the default paintComponent
   * @author Andy Mechtenberg
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    Color origColor = graphics2D.getColor();
    graphics2D.draw(getShape());

    // text rendering
    Font origGraphicsFont = graphics.getFont();
    Font rendererFont = getFont();
    FontMetrics fontMetrics = getFontMetrics(rendererFont);

    // the Height is the max height needed to display any text in this font (ascent + descent + 1)
    // basically, the height of the bounding rectangle.
    // DrawString, however, needs the X, Y of the leftmost BASELINE point, so we need to subtract the max descent
    // from this Y otherwise any character below the baseline will be cut off.
    // we subtract another one, to better center it, and the height stuff added one as well. (Which is doesn't need to
    // do for text, but is needed for other shapes)
    String text = String.valueOf(_inspectionRegion.getRegionId());
    graphics.setFont(rendererFont);
    graphics.drawString(text, 2, fontMetrics.getHeight() - fontMetrics.getMaxDescent());
    graphics.setFont(origGraphicsFont);
    graphics.setColor(origColor);
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void refreshData()
  {
    Assert.expect(_inspectionRegion != null);

    PanelRectangle panelRectangleInNanoMeters = null;
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    if(_usePadBounds  && _inspectionRegion.getTestSubProgram().getTestProgram().getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod() == false)
      panelRectangleInNanoMeters = _inspectionRegion.getBoundingRegionOfPadsInNanoMeters();
    else
      panelRectangleInNanoMeters = _inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    Assert.expect(panelRectangleInNanoMeters != null);

    Shape region = new Rectangle2D.Double(panelRectangleInNanoMeters.getMinX(),
                                          panelRectangleInNanoMeters.getMinY(),
                                          panelRectangleInNanoMeters.getWidth(),
                                          panelRectangleInNanoMeters.getHeight());

    initializeShape(region);
  }

  /**
   * @return InspectionRegion object associated with renderer
   * @author Andy Mechtenberg
   */
  public ReconstructionRegion getInspectionRegion()
  {
    Assert.expect(_inspectionRegion != null);

    return _inspectionRegion;
  }

  /**
   * @author George A. David
   */
  public void setUsePadBounds(boolean usePadBounds)
  {
    _usePadBounds = usePadBounds;
    refreshData();
  }
}
