package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;

/**
 * @author Cheah Lee Herng
 */
public class LibraryPackagePinNameRenderer extends TextRenderer 
{
  private LibraryPackagePin _libraryPackagePin;
  private int _totalPins;
  
  /**
   * @author Cheah Lee Herng 
   */
  public LibraryPackagePinNameRenderer(LibraryPackagePin libraryPackagePin, int totalPins)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(libraryPackagePin != null);
    _libraryPackagePin = libraryPackagePin;
    _totalPins = totalPins;

    refreshData();
  }
  
  /**
    * @author George A. David
   */
  protected void refreshData()
  {
    Assert.expect(_libraryPackagePin != null);

    String packagePinName = _libraryPackagePin.getName();

    Shape shape = _libraryPackagePin.getShapeInNanoMeters();
    Rectangle2D bounds = shape.getBounds2D();

    // set a default font    
    if(_totalPins < 10)
      setFont(FontUtil.getRendererFont(48));
    else if(_totalPins < 100)
      setFont(FontUtil.getRendererFont(22));
    else if(_totalPins < 300)
      setFont(FontUtil.getRendererFont(18));
    else if(_totalPins < 1000)
      setFont(FontUtil.getRendererFont(11));
    else
      setFont(FontUtil.getRendererFont(6));

    initializeText(packagePinName, bounds.getCenterX(), bounds.getCenterY(), true);
  }
}
