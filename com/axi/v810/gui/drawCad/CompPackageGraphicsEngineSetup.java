package com.axi.v810.gui.drawCad;

import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.testDev.*;

/**
 * This class sets up the GraphicsEngine for a particular CompPackage so the
 * GraphicsEngine can draw the CompPackage's CAD graphics
 *
 * @author Bill Darbie
 */
public class CompPackageGraphicsEngineSetup implements Observer
{
  private static final int _BORDER_SIZE_IN_PIXELS = 50;
  private static final int _packagePinLayer = JLayeredPane.DEFAULT_LAYER.intValue();
  private static final int _packagePinNameLayer = _packagePinLayer + 1;
  private static final int _developerDebugLayer = _packagePinNameLayer + 1;

  private GraphicsEngine _graphicsEngine;
  private CompPackage _compPackage;
  
  private DrawCadPanel _drawCadPanel;
  private CreateNewCadPanel _createNewCadPanel;

  private Map<PackagePin, PackagePinRenderer> _packgePinToPackagePinRenderMap = new HashMap<PackagePin, PackagePinRenderer>();

  /**
   * @author Andy Mechtenberg
   */
  CompPackageGraphicsEngineSetup(DrawCadPanel drawCadPanel, GraphicsEngine graphicsEngine)
  {
    Assert.expect(drawCadPanel != null);
    Assert.expect(graphicsEngine != null);
    _drawCadPanel = drawCadPanel;
    _graphicsEngine = graphicsEngine;

    _graphicsEngine.setPixelBorderSize(_BORDER_SIZE_IN_PIXELS);
  }
  
  /*
   * @author Kee chin Seong
   */
  CompPackageGraphicsEngineSetup(CreateNewCadPanel createNewCadPanel, GraphicsEngine graphicsEngine)
  {
    Assert.expect(createNewCadPanel != null);
    Assert.expect(graphicsEngine != null);
    
    _createNewCadPanel = createNewCadPanel;
    _graphicsEngine = graphicsEngine;

    _graphicsEngine.setPixelBorderSize(_BORDER_SIZE_IN_PIXELS);
  }

  /**
   * @author Andy Mechtenberg
   */
  void displayPackage(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);
    _compPackage = compPackage;

    populateLayersWithRenderers();
    setColors();
    setVisibility();
    setLayersToUseWhenFittingToScreen();

    _graphicsEngine.setAxisInterpretation(true, false);

    MathUtilEnum displayUnits = _compPackage.getLandPattern().getPanel().getProject().getDisplayUnits();
    double measureFactor = 1.0/MathUtil.convertUnits(1.0, displayUnits, MathUtilEnum.NANOMETERS);
    _graphicsEngine.setMeasurementInfo(measureFactor, MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits),
       MeasurementUnits.getMeasurementUnitString(displayUnits));

    // add this as an observer of the datastore
    ProjectObservable.getInstance().addObserver(this);
    // add this as an observer of gui events
    GuiObservable.getInstance().addObserver(this);
    // add this as an observer of the engine
    _graphicsEngine.getSelectedRendererObservable().addObserver(this);
    _graphicsEngine.getDisplayPadNameObservable().addObserver(this);
  }

  /**
   * Set up the layeredPanel for drawing packages
   * @author Andy Mechtenberg
   */
  public GraphicsEngine getGraphicsEngine()
  {
    Assert.expect(_graphicsEngine != null);

    return _graphicsEngine;
  }

  /**
   * @author Bill Darbie
   */
  private void populateLayersWithRenderers()
  {
    _graphicsEngine.reset();
    for  (PackagePin packagePin: _compPackage.getPackagePinsUnsorted())
    {
      PackagePinRenderer packagePinRenderer = new PackagePinRenderer(packagePin);
      PackagePinNameRenderer packagePinNameRenderer = new PackagePinNameRenderer(packagePin);
      _graphicsEngine.addRenderer(_packagePinLayer, packagePinRenderer);
      _graphicsEngine.addRenderer(_packagePinNameLayer, packagePinNameRenderer);
      _packgePinToPackagePinRenderMap.put(packagePin, packagePinRenderer);

      if (TestDev.isInDeveloperDebugMode())
      {
        PackagePinPitchRenderer packagePinPitchRenderer = new PackagePinPitchRenderer(packagePin);
        _graphicsEngine.addRenderer(_developerDebugLayer, packagePinPitchRenderer);

        PackagePinInterPadDistanceRenderer packagePinIpdRenderer = new PackagePinInterPadDistanceRenderer(packagePin);
        _graphicsEngine.addRenderer(_developerDebugLayer, packagePinIpdRenderer);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  void reset()
  {
    _graphicsEngine.reset();
    _compPackage = null;
    _packgePinToPackagePinRenderMap.clear();

    // add this as an observer of the datastore
    ProjectObservable.getInstance().deleteObserver(this);
    // add this as an observer of gui events
    GuiObservable.getInstance().deleteObserver(this);
    // add this as an observer of the engine
    _graphicsEngine.getSelectedRendererObservable().deleteObserver(this);
    _graphicsEngine.getDisplayPadNameObservable().deleteObserver(this);
  }

  /**
   * Set up the proper colors for the layers
   * Bill Darbie
   */
  private void setVisibility()
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.setVisible(_packagePinLayer, true);
    _graphicsEngine.setVisible(_packagePinNameLayer, false);
    _graphicsEngine.setVisible(_developerDebugLayer, true);
  }

  /**
   * Set up the proper colors for the layers
   * Bill Darbie
   */
  private void setColors()
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.setForeground(_packagePinLayer, LayerColorEnum.SURFACEMOUNT_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(_developerDebugLayer, LayerColorEnum.SURFACEMOUNT_PAD_COLOR.getColor());
  }

  /**
   * Set up which layers to use when figuring out how big the whole deal is
   * Bill Darbie
   */
  private void setLayersToUseWhenFittingToScreen()
  {
    Collection<Integer> layers = new ArrayList<Integer>(1);
    layers.add(new Integer(_packagePinLayer));
    _graphicsEngine.setLayersToUseWhenFittingToScreen(layers);
  }

  /**
   * @author Andy Mechtenberg
   */
  public Integer getPackagePinLayer()
  {
    return new Integer(_packagePinLayer);
  }

  /**
   * This class is an observer of the datastore layer. Any changes made
   * to the datastore will trigger an update call. We will look at the
   * change event and determine if anything needs to be updated.
   * @author George A. David
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if(observable instanceof ProjectObservable)
        {
          if(argument instanceof ProjectChangeEvent)
          {
            ProjectChangeEvent event = (ProjectChangeEvent)argument;
            ProjectChangeEventEnum eventEnum = ((ProjectChangeEvent)argument).getProjectChangeEventEnum();

            if(eventEnum instanceof PackagePinEventEnum)
            {
              PackagePin packagePin = (PackagePin)event.getSource();
              if (isPackagePinRendered(packagePin))
              {
                com.axi.guiUtil.Renderer packagePinRenderer = getRendererForPackagePin(packagePin);
                packagePinRenderer.validateData();
              }
            }
          }
        }
        else if (observable instanceof SelectedRendererObservable)
        {
          // this comes from the GraphicsEngine whenever a group of "things" has been selected by the user
          // this code will highlight (turn white) the selected items.  Since this graphics engine setup only
          // cares about PackagePins, that's all this observer will pay attention to.
          boolean descriptionIsSet = false;
          _graphicsEngine.clearSelectedRenderers();
          /** Warning "unchecked cast" approved for cast from Object type
           *  when sent from SelectedRendererObservable notifyAll.  */
          for (com.axi.guiUtil.Renderer renderer :
               ((Collection<com.axi.guiUtil.Renderer>)argument)) {
            // if it's a PackagePin, highlight it
            if (renderer instanceof PackagePinRenderer)
            {
              _graphicsEngine.setSelectedRenderer(renderer);

              // if more than one package pin was selected, we set the status bar descriptive text to nothing
              // so, we'll set it the first time, and if we get another, clear it out.
              if (descriptionIsSet)
              {
                _drawCadPanel.setDescription(" ");  // if we use null, the entire status bar goes away, so we'll use an empty space instead
                
                if(_createNewCadPanel != null)
                   _createNewCadPanel.setDescription(" ");
              }
              else
              {
                _drawCadPanel.setDescription(renderer.toString());
                
                if(_createNewCadPanel != null)
                   _createNewCadPanel.setDescription(renderer.toString());
                
                descriptionIsSet = true;
              }
            }
          }
        }
        else if (observable instanceof GuiObservable)
        {
          handleGuiEvent((GuiEvent)argument);
        }
        else if (observable instanceof DisplayPadNameObservable)
        {
          if (argument instanceof Boolean)
          {
              Boolean setPadNameVisible = (Boolean)argument;
              if (setPadNameVisible.booleanValue())
                  _graphicsEngine.setVisible(_packagePinNameLayer, true); 
              else
                  _graphicsEngine.setVisible(_packagePinNameLayer, false); 
          }             
        }
        else
        {
          Assert.expect(false, "CompPackageGraphicsEngineSetup: No update code for observable: " + observable.getClass().getName());
        }
      }
    });
  }

  /**
   * @author George A. David
   */
  private void handleGuiEvent(GuiEvent guiEvent)
  {
    Assert.expect(guiEvent != null);

    GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
    if(guiEventEnum instanceof SelectionEventEnum)
    {
      SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
      if(selectionEventEnum.equals(SelectionEventEnum.PACKAGE_PIN))
      {
        _graphicsEngine.clearSelectedRenderers();
        /** Warning "unchecked cast" approved for cast from Object types */
        List<PackagePin> packagePins = (List<PackagePin>)guiEvent.getSource();
        _graphicsEngine.setSelectedRenderers(getRenderers(packagePins));
      }
    }
  }

  /**
   * Returns true if the package pin passed in is actually being displayed, false otherwise
   * @author Andy Mechtenberg
   */
  private boolean isPackagePinRendered(PackagePin packagePin)
  {
    Assert.expect(packagePin != null);
    Collection<com.axi.guiUtil.Renderer> renderers =
      _graphicsEngine.getRenderers(getPackagePinLayer().intValue());
    for (com.axi.guiUtil.Renderer renderer : renderers)
    {
      // if matching PackagePin then return true.
      if (renderer instanceof PackagePinRenderer)
      {
        PackagePinRenderer tempPinRenderer = (PackagePinRenderer) renderer;
        if(packagePin == tempPinRenderer.getPackagePin())
        {
          return true;
        }
     }
     else
       Assert.expect(false, "Unexpected renderer: " + renderer);
    }
    return false;
  }

  /**
   * @author George A. David
   */
  private List<com.axi.guiUtil.Renderer> getRenderers(List<PackagePin> packagePins)
  {
    Assert.expect(packagePins != null);

    List<com.axi.guiUtil.Renderer> renderers = new ArrayList<com.axi.guiUtil.Renderer>();
    for (PackagePin pp : packagePins)
    {
      renderers.add(_packgePinToPackagePinRenderMap.get(pp));
    }

    return renderers;
  }

  /**
   * @author Andy Mechtenberg
   */
  private com.axi.guiUtil.Renderer getRendererForPackagePin(PackagePin packagePin)
  {
    Assert.expect(packagePin != null);
    Collection<com.axi.guiUtil.Renderer> renderers = _graphicsEngine.getRenderers(getPackagePinLayer().intValue());
    Iterator<com.axi.guiUtil.Renderer> it = renderers.iterator();
    while(it.hasNext())
    {
      PackagePinRenderer tempPinRenderer = (PackagePinRenderer)it.next();
      if(packagePin == tempPinRenderer.getPackagePin())
      {
        return tempPinRenderer;
      }
    }
    Assert.expect(false, "Could not find renderer for " + packagePin);
    return null;
  }
}
