package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This class draws a Pad outline.
 *
 * @author Bill Darbie
 */
public class PadRenderer extends ShapeRenderer
{
  private Pad _pad;
  private ComponentRenderer _componentRenderer;

  /**
   * @author Andy Mechtenberg
   */
  PadRenderer(Pad pad, ComponentRenderer componentRenderer)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(pad != null);
    Assert.expect(componentRenderer != null);
    _pad = pad;
    _componentRenderer = componentRenderer;

    refreshData();
  }

  /**
   * @author Bill Darbie
   */
  public Pad getPad()
  {
    return _pad;
  }

  /**
   * @author Bill Darbie
   */
  ComponentRenderer getComponentRenderer()
  {
    return _componentRenderer;
  }

  /**
   * Pad 1 needs to be hilighted (colored in RED), so we'll override the default paintComponent
   * @author Andy Mechtenberg
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    Color origColor = graphics2D.getColor();

    boolean useSelectedColor = (getForeground() == getSelectedColor()) || (getForeground() == getHighlightedColor());

    if (!useSelectedColor) // if selected or highlighted, use the selected color
    {
      boolean isThroughHole = _pad.getLandPatternPad().isThroughHolePad();
      if (_pad.isPadOne())
      {
        if (isThroughHole)
          graphics2D.setColor(LayerColorEnum.FIRST_THROUGHHOLE_PAD_COLOR.getColor());
        else
          graphics2D.setColor(LayerColorEnum.FIRST_SURFACEMOUNT_PAD_COLOR.getColor());
      }
      else if (isThroughHole)
        graphics2D.setColor(LayerColorEnum.THROUGHHOLE_PAD_COLOR.getColor());
    }

    graphics2D.fill(getShape(0));

    if (_pad.getLandPatternPad().isThroughHolePad())
    {
      // darken the color slightly
      graphics2D.setColor(graphics2D.getColor().darker());
      graphics2D.fill(getShape(1));
    }

    graphics2D.setColor(origColor);
  }

  /**
   * Return the name of the panel
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    return _componentRenderer.toString() + "-" + _pad.getName();
  }

  /**
   * @author Bill Darbie
   */
  protected void refreshData()
  {
    Assert.expect(_pad != null);

    if (_pad.getLandPatternPad().isThroughHolePad())
    {
      Assert.expect(_pad.getLandPatternPad() instanceof ThroughHoleLandPatternPad);
      ThroughHoleLandPatternPad throughholeLandPatternPad = (ThroughHoleLandPatternPad)_pad.getLandPatternPad();

      Shape holeShape = throughholeLandPatternPad.getHoleShapeInNanoMeters();

      // transform the hole to the right place
      AffineTransform transform = new AffineTransform();
      _pad.getComponent().preConcatenateShapeTransform(transform);
      holeShape = transform.createTransformedShape(holeShape);

      initializeShape(_pad.getShapeRelativeToPanelInNanoMeters(), holeShape);
    }
    else
    {
      initializeShape(_pad.getShapeRelativeToPanelInNanoMeters());
    }
  }
}
