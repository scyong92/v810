package com.axi.v810.gui.drawCad;

import java.awt.*;

import com.axi.guiUtil.*;
import com.axi.util.*;

/**
 * This class draws Component outlines.
 *
 * @author Bill Darbie
 */
public class ComponentRenderer extends ShapeRenderer
{
  private com.axi.v810.business.panelDesc.Component _component = null;
  private PanelGraphicsEngineSetup _panelGraphicsEngineSetup = null;
  private BoardGraphicsEngineSetup _boardGraphicsEngineSetup = null;
  
  private CreatePanelGraphicsEngineSetup _createPanelGraphicsEngineSetup = null;

  /**
   * @author Bill Darbie
   */
  ComponentRenderer(PanelGraphicsEngineSetup panelGraphicsEngineSetup,
                    com.axi.v810.business.panelDesc.Component component)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(panelGraphicsEngineSetup != null);
    Assert.expect(component != null);
    _panelGraphicsEngineSetup = panelGraphicsEngineSetup;
    _component = component;

    refreshData();
  }
  
  ComponentRenderer(CreatePanelGraphicsEngineSetup createPanelGraphicsEngineSetup,
                    com.axi.v810.business.panelDesc.Component component)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(createPanelGraphicsEngineSetup != null);
    Assert.expect(component != null);
    _createPanelGraphicsEngineSetup = createPanelGraphicsEngineSetup;
    _component = component;

    refreshData();
  }

  /**
   * @author Bill Darbie
   */
  ComponentRenderer(BoardGraphicsEngineSetup boardGraphicsEngineSetup,
                    com.axi.v810.business.panelDesc.Component component)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(boardGraphicsEngineSetup != null);
    Assert.expect(component != null);
    _boardGraphicsEngineSetup = boardGraphicsEngineSetup;
    _component = component;

    refreshData();
  }

  /**
   * @author Bill Darbie
   */
  public com.axi.v810.business.panelDesc.Component getComponent()
  {
    return _component;
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    // if the pads are being drawn, make the component outline a wider
    Stroke origStroke = graphics2D.getStroke();

    boolean padVisible = false;

    if(_createPanelGraphicsEngineSetup != null)
    {
        if(_createPanelGraphicsEngineSetup != null)
            padVisible = _createPanelGraphicsEngineSetup.arePadsVisible(_component.isTopSide());
        else if(_boardGraphicsEngineSetup != null)
          padVisible = _boardGraphicsEngineSetup.arePadsVisible(_component.isTopSide());
        else
          Assert.expect(false, "Graphic Engine is not available!");
    }
    else
    { 
        if(_panelGraphicsEngineSetup != null)
          padVisible = _panelGraphicsEngineSetup.arePadsVisible(_component.isTopSide());
        else if(_boardGraphicsEngineSetup != null)
          padVisible = _boardGraphicsEngineSetup.arePadsVisible(_component.isTopSide());
        else
          Assert.expect(false, "Graphic Engine is not available!");
    }
    
    if (padVisible)
      graphics2D.setStroke(new BasicStroke(5));

    super.paintComponent(graphics);

    if (padVisible)
      graphics2D.setStroke(origStroke);

  }

  /**
   * Return the reference designator for the component being rendered.
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    return _component.getReferenceDesignator();
  }

  /**
   * @author Bill Darbie
   */
  protected void refreshData()
  {
    Assert.expect(_component != null);

    initializeShape(_component.getShapeRelativeToPanelInNanoMeters());
  }
}
