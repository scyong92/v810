package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelDesc.Component;
import com.axi.v810.business.panelDesc.Panel;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.virtualLive.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class sets up the GraphicsEngine for a particular Panel so the
 * GraphicsEngine can draw the panels CAD graphics
 *
 * @author Bill Darbie
 */
public class PanelGraphicsEngineSetup extends Observable implements RendererCreator, Observer
{
  private static final int _BORDER_SIZE_IN_PIXELS = 15;//50;

  private static GuiObservable _guiObservable;

  private GraphicsEngine _graphicsEngine;
  private DrawCadPanel _drawCadPanel;
  private ControlToolBar _controlToolBar;
  private Panel _panel;

  private int _layerNumber = -1;
  private int _topPanelLayer;
  private int _topPanelFiducialLayer;
  private int _topPanelFiducialRefDesLayer;
  private int _addOnEmptyLayer;
  private Map<Board, Integer> _boardToTopBoardLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToTopPadLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToTopComponentLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToTopRefDesLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToTopFiducialLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToOriginLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToBoardNameLayerMap = new HashMap<Board, Integer>();
  private int _bottomPanelLayer;
  private int _bottomPanelFiducialLayer;
  private int _bottomPanelFiducialRefDesLayer;
  private Map<Board, Integer> _boardToBottomBoardLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToBottomPadLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToBottomComponentLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToBottomRefDesLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToBottomFiducialLayerMap = new HashMap<Board, Integer>();

  private int _alignmentRegionLayer;

  // inspection regions
  private int _topInspectionRegionRectangleLayer;
  private int _bottomInspectionRegionRectangleLayer;
  private int _topInspectionRegionPadBoundsLayer;
  private int _bottomInspectionRegionPadBoundsLayer;
  private int _topInspectionFocusRegionLayer;
  private int _bottomInspectionFocusRegionLayer;
  
  private int _topEditFocusRegionInspectionFocusRegionLayer;
  private int _bottomEditFocusRegionInspectionFocusRegionLayer;
  private int _topEditFocusRegionInspectionRegionRectangleLayer;
  private int _bottomEditFocusRegionInspectionRegionRectangleLayer;

  // verification regions
  private int _topVerificationRegionRectangleLayer;
  private int _bottomVerificationRegionRectangleLayer;
  private int _topVerificationFocusRegionLayer;
  private int _bottomVerificationFocusRegionLayer;

  // alignment regions
  private int _topAlignmentRegionRectangleLayer;
  private int _bottomAlignmentRegionRectangleLayer;
  private int _topAlignmentRegionPadBoundsLayer;
  private int _bottomAlignmentRegionPadBoundsLayer;
  private int _topAlignmentFocusRegionLayer;
  private int _bottomAlignmentFocusRegionLayer;

  // irp boundaries
  private int _inspectionIrpBoundariesLayer;
  private int _verificationIrpBoundariesLayer;
  
  // optical region
  private int _opticalRegionSelectedLayer;
  private int _dragOpticalRegionLayer;
  private int _dragOpticalRegionUnSelectedLayer;
  private int _opticalRegionUnSelectedLayer;
  private int _highlightOpticalRegionLayer;
  private int _dragAlignmentOpticalRegionLayer;
  private int _meshTriangleLayer;
  
  //@author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
  private int _topNoLoadedComponentLayer;
  private int _bottomNoLoadedComponentLayer;
  private int _topNoLoadPadLayer;
  private int _bottomNoLoadPadLayer;
  private int _panelWidthRendererLayer;
  private int _topNoLoadPadNameLayer;
  private int _bottomNoLoadPadNameLayer;

  //@author By Kee Chin Seong - Renderer zoom in for RefDesingator must be updated when no load component is pressed.
  private int _topNoLoadRefDesignatorLayer; 
  private int _bottomNoLoadRefDesignatorLayer;
  
  private int _topPadNameLayer;
  private int _bottomPadNameLayer;
  
  private int _untestableAreaLayer;
  
  private Map<BoardType, List<BoardRenderer>> _boardTypeToBoardRenderersMap = new HashMap<BoardType, List<BoardRenderer>>();
  private Map<ComponentType, List<ComponentRenderer>> _componentTypeToComponentRenderersMap = new HashMap<ComponentType, List<ComponentRenderer>>();
  private Map<ComponentType, List<ReferenceDesignatorRenderer>> _componentTypeToReferenceDesignatorRenderersMap =
      new HashMap<ComponentType,  List<ReferenceDesignatorRenderer>>();
  private Map<Pad, PadRenderer> _padToPadRendererMap = new HashMap<Pad, PadRenderer>();
  private Map<ComponentRenderer, List<PadRenderer>> _componentRendererToPadRenderersMap = new HashMap<ComponentRenderer, List<PadRenderer>>();

  //@author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
  private Map<ComponentType, List<ComponentRenderer>> _noLoadComponentTypeToComponentRenderersMap = new HashMap<ComponentType, List<ComponentRenderer>>();
  private Map<ComponentType, List<ReferenceDesignatorRenderer>> _noLoadComponentTypeToReferenceDesignatorRenderersMap =
      new HashMap<ComponentType,  List<ReferenceDesignatorRenderer>>();
  private Map<Pad, PadRenderer> _noLoadPadToPadRendererMap = new HashMap<Pad, PadRenderer>();
  private Map<ComponentRenderer, List<PadRenderer>> _noLoadComponentRendererToPadRenderersMap = new HashMap<ComponentRenderer, List<PadRenderer>>();
  private Map<ComponentRenderer, List<PadNameRenderer>> _noLoadComponentRendererToPadNameRendererMap = new HashMap<ComponentRenderer, List<PadNameRenderer>>();
  
  private List<HighlightAlignmentItems> _highlightPads = new ArrayList<HighlightAlignmentItems>();

  private boolean _drawDebugGraphics = false;

  // reference to a single board that was selected
  private String _selectedBoardName = null;
  
  private SelectedOpticalRectangleRenderer _selectedRectangleRenderer;
  private HighlightOpticalRectangleRenderer _highlightRectangleRenderer;
  private DragOpticalRectangleRenderer _dragOpticalRectangleRenderer;
  private MeshTriangleRenderer _meshTriangleRenderer;
  private java.util.List<String> _tableOpticalRegions = new ArrayList<String>();
  private int _index = -1;
  private static Map<Rectangle, String> _alignmentOpticalRegionList = new LinkedHashMap<Rectangle, String>();
 
  private boolean _showPadName = false;
  
  private boolean _showNoLoadComponent = false;
  
 // Kee Chin Seong - XXL GUI Development 
  private boolean _showTopComponentLayer = true;
  private boolean _showTopComponentRefDesLayer = true;
  private boolean _showTopPadLayer = true;
  private boolean _showTopPanelLayer = true;
  
  private boolean _showBottomComponentLayer = false;
  private boolean _showBottomComponentRefDesLayer = false;
  private boolean _showBottomPadLayer = true;
  private boolean _showBottomPanelLayer = true;
  
  private int _virtualLiveAreaLayer;
  private com.axi.guiUtil.Renderer _rendererToUse;
  
  private static java.util.List<OpticalCameraRectangle> _opticalCameraRectangleList = new ArrayList<>();
  private java.util.List<OpticalCameraRectangle> _unselectedOpticalCameraRectangleList = new ArrayList<>();
  private java.util.List<OpticalCameraRectangle> _selectedOpticalCameraRectangleList = new ArrayList<>();
  private java.util.List<OpticalCameraRectangle> _highlightedOpticalCameraRectangleList = new ArrayList<>();
  private java.util.List<OpticalCameraRectangle> _ignoredOpticalCameraRectangleList = new ArrayList<>();
  private double _currentZoomFactor = 0;
  private double _currentCanvasWidthToHeightRatio = 0;

  //Kee Chin Seong
  private Shape _selectedFocusRegion;
  private boolean _isFocusRegionEditorOn = false;
  
  //bee-hoon.goh - Virtual Live Verification Image
  private int _topVirtualLiveVerificationImageLayer;  
  private int _bottomVirtualLiveVerificationImageLayer;
  private int _bothVirtualLiveVerificationImageLayer;

  /**
   * @author Bill Darbie
   */
  static
  {
    _guiObservable = GuiObservable.getInstance();
  }

  /**
   * @author Andy Mechtenberg
   */
  PanelGraphicsEngineSetup(DrawCadPanel drawCadPanel, GraphicsEngine graphicsEngine)
  {
    Assert.expect(drawCadPanel != null);
    Assert.expect(graphicsEngine != null);

    _drawCadPanel = drawCadPanel;
    _graphicsEngine = graphicsEngine;
    _graphicsEngine.setPixelBorderSize(_BORDER_SIZE_IN_PIXELS);
    
    try
    {
      _drawDebugGraphics = LicenseManager.isDeveloperSystemEnabled() && TestDev.isInDeveloperDebugMode();
    }
    catch (BusinessException bex)
    {
      _drawDebugGraphics = false;
      MainMenuGui.getInstance().handleXrayTesterException(bex);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  void addObservers()
  {
    // add this as an observer of the project
    ProjectObservable.getInstance().addObserver(this);
    // add this as an observer of the gui
    _guiObservable.addObserver(this);
    // add this as an observer of the engine
    _graphicsEngine.getSelectedRendererObservable().addObserver(this);
    // add this as an observer of the psp selected region
    _graphicsEngine.getSelectedOpticalRegionRendererObservable().addObserver(this);
    // add this as an observer of the psp drag region
    _graphicsEngine.getDragOpticalRegionObservable().addObserver(this);
    
    _graphicsEngine.getDisplayPadNameObservable().addObserver(this);
    _graphicsEngine.getSelectedAreaObservable().addObserver(this);
    _graphicsEngine.getShowNoLoadComponentObservable().addObserver(this);
    
    VirtualLiveImageGenerationSettings.getInstance().addObserver(this);
    _graphicsEngine.getShowUntestableAreaObservable().addObserver(this);
  }

  /**
    * @author Andy Mechtenberg
    */
   void removeObservers()
   {
     // remove this as an observer of the project data
     ProjectObservable.getInstance().deleteObserver(this);
     // remove this as an observer of the gui
     _guiObservable.deleteObserver(this);
     // remove this as an observer of the engine
     _graphicsEngine.getSelectedRendererObservable().deleteObserver(this);
     // remove this as a observer of the psp selected region
     _graphicsEngine.getSelectedOpticalRegionRendererObservable().deleteObserver(this);
     // remove this as a observer of the psp drag region
     _graphicsEngine.getDragOpticalRegionObservable().deleteObserver(this);
     
     _graphicsEngine.getDisplayPadNameObservable().deleteObserver(this);    
     _graphicsEngine.getSelectedAreaObservable().deleteObserver(this);
     _graphicsEngine.getShowNoLoadComponentObservable().deleteObserver(this);
     
     VirtualLiveImageGenerationSettings.getInstance().deleteObserver(this);
     _graphicsEngine.getShowUntestableAreaObservable().deleteObserver(this);
  }
   
  /**
   * @author Andy Mechtenberg
   */
  void unpopulate()
  {
    _panel = null;
    clearAllInternalEngineData();
    _showTopPadLayer = true;
    _showTopComponentLayer = true;
    _showTopComponentRefDesLayer = true;
    _showTopPanelLayer = true;
    
    //Ngie Xing
    _showBottomPadLayer = false;
    _showBottomComponentLayer = false;
    _showBottomComponentRefDesLayer = false;
    _showBottomPanelLayer = false;
  }

  /**
   * @author George A. David
   */
  public void displayPanel(Panel panel)
  {
    Assert.expect(panel != null);

    _panel = panel;
    clearAllInternalEngineData();    

    createLayers();
    populateLayersWithRenderers();
    setColors();
    setVisibility();
    setThresholds();
    setLayersToUseWhenFittingToScreen();

    _graphicsEngine.setAxisInterpretation(true, false);

    MathUtilEnum displayUnits = _panel.getProject().getDisplayUnits();
    double measureFactor = 1.0/MathUtil.convertUnits(1.0, displayUnits, MathUtilEnum.NANOMETERS);
    _graphicsEngine.setMeasurementInfo(measureFactor, MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits),
       MeasurementUnits.getMeasurementUnitString(displayUnits));
  }
  
  /**
   * @author Andy Mechtenberg
   * @Edited by Kee Chin Seong
   */
  public void displayPanelForStaticImageGeneration(Panel panel, boolean flipped)
  {
    Assert.expect(panel != null);
    _panel = panel;
    clearAllInternalEngineData();    

    // here we'll only populate the TOP side, and skip things like pad/ref des, etc that don't make sense for
    // a JPG generation

    createTopLayers();
    populateTopLayersWithRenderers();
    // colors
    _graphicsEngine.setForeground(getTopComponentLayers(), LayerColorEnum.TOP_COMPONENT_COLOR.getColor());
    _graphicsEngine.setForeground(getTopBoardLayers(), LayerColorEnum.BOARD_COLOR.getColor());     
    _graphicsEngine.setForeground(_topPanelLayer, LayerColorEnum.PANEL_COLOR.getColor()); 
    _graphicsEngine.setForeground(_topPanelFiducialLayer, LayerColorEnum.FIDUCIAL_COLOR.getColor());
    _graphicsEngine.setForeground(_boardToTopFiducialLayerMap.values(), LayerColorEnum.FIDUCIAL_COLOR.getColor());
    _graphicsEngine.setForeground(_boardToBoardNameLayerMap.values(), LayerColorEnum.BOARD_NAME_COLOR.getColor());
    _graphicsEngine.setForeground(_topNoLoadedComponentLayer, LayerColorEnum.SAVE_NO_LOAD_COMPONENT_COLOR.getColor()); 
        //@author Kee Chin Seong - Panel Width Renderer
    _graphicsEngine.setForeground(_panelWidthRendererLayer, LayerColorEnum.BOARD_NAME_COLOR.getColor());
    //_addOnEmptyLayer is used to set the no load layer back to red and remain the penel width layer as white
    _graphicsEngine.setForeground(_addOnEmptyLayer, LayerColorEnum.SAVE_NO_LOAD_COMPONENT_COLOR.getColor());

    List<Integer> boardNameLayers = getBoardNameLayers();
    //XCR-3149: Panel image and board width is upside down if panel is loaded from right to left
    Collection<com.axi.guiUtil.Renderer> boardNameRenderers = _graphicsEngine.getRenderers(boardNameLayers);
    for(com.axi.guiUtil.Renderer renderer : boardNameRenderers)
    {
      if (renderer instanceof BoardNameRenderer)
      {
        BoardNameRenderer boardNameRenderer = (BoardNameRenderer)renderer;
        boardNameRenderer.setTranslucent(true);
        boardNameRenderer.setFlipped(flipped);
        boardNameRenderer.setIsStaticImage(true);
      }    
    }
    
    Collection<com.axi.guiUtil.Renderer> panelWidthRenderers = _graphicsEngine.getRenderers(_panelWidthRendererLayer);
    for(com.axi.guiUtil.Renderer renderer : panelWidthRenderers)
    {
      if (renderer instanceof PanelWidthRenderer)
      {
        PanelWidthRenderer panelWidthRenderer = (PanelWidthRenderer)renderer;
        panelWidthRenderer.setFlipped(flipped);
      }
    }

    // set visibility 
    _graphicsEngine.setVisible(_topNoLoadedComponentLayer,true);   
    _graphicsEngine.setVisible(getTopLayers(), true);
  

    if(_showPadName)
    {
      _graphicsEngine.setVisible(_topPadNameLayer, true);
      
      if(_showNoLoadComponent)
        _graphicsEngine.setVisible(_topNoLoadPadNameLayer, true); 
    }

    if (_panel.getNumBoards() == 1)
    {    
      _graphicsEngine.setVisible(getBoardNameLayers(), false);
      // to show the no load component as red colour.
      _graphicsEngine.setVisible(_addOnEmptyLayer,true); 
    }  
   // set layer to use when fitting to screen - panel/board layers
    Collection<Integer> layers = new ArrayList<Integer>(2);
    layers.addAll(getPanelLayers());
    layers.addAll(getBoardLayers());
    _graphicsEngine.setLayersToUseWhenFittingToScreen(layers);

    _graphicsEngine.setAxisInterpretation(true, false);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void clearAllInternalEngineData()
  {
    _boardTypeToBoardRenderersMap.clear();
    _componentTypeToComponentRenderersMap.clear();
    _componentTypeToReferenceDesignatorRenderersMap.clear();
    _boardToTopBoardLayerMap.clear();
    _boardToTopPadLayerMap.clear();
    _boardToTopComponentLayerMap.clear();
    _boardToOriginLayerMap.clear();
    _boardToBoardNameLayerMap.clear();
    _boardToTopRefDesLayerMap.clear();
    _boardToTopFiducialLayerMap.clear();
    _boardToBottomBoardLayerMap.clear();
    _boardToBottomPadLayerMap.clear();
    _boardToBottomComponentLayerMap.clear();
    _boardToBottomRefDesLayerMap.clear();
    _boardToBottomFiducialLayerMap.clear();
    _componentRendererToPadRenderersMap.clear();
    _padToPadRendererMap.clear();
    _highlightPads.clear();
    _graphicsEngine.clearAllFlashingRenderersAndLayers();
    
    //@author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
    _noLoadComponentTypeToComponentRenderersMap.clear();
    _noLoadComponentTypeToReferenceDesignatorRenderersMap.clear();
    _noLoadComponentRendererToPadRenderersMap.clear();
    _noLoadComponentRendererToPadNameRendererMap.clear();
  }
  
  /**
   * @author Andy Mechtenberg
   */
  void setControlToolBar(ControlToolBar controlToolBar)
  {
    Assert.expect(controlToolBar != null);
    _controlToolBar = controlToolBar;
  }

  /**
   * Set up the layeredPanel
   * @author Bill Darbie
   */
  public GraphicsEngine getGraphicsEngine()
  {
    Assert.expect(_graphicsEngine != null);

    return _graphicsEngine;
  }

  /**
   * @author Bill Darbie
   */
  private void createLayers()
  {
    Assert.expect(_panel != null);

    // set things up like this:
    // - 1 panel layer
    // - 1 board layer per board
    // - 2 componenent layers per board (one for each side)
    // - 2 reference designator layers per board (one for each side)
    // - 2 pad layers per board (one for each side)
    // - 2 fiducial layers for panel fiducials (one for each side)
    // - 2 fiducial layers per board for board fiducials (one for each side)

    // the higher the layer number, the  deeper down the layer is
    // put largest components at the smallest layer number so mouse clicks
    // will be able to select small components before larger ones
    _layerNumber = JLayeredPane.DEFAULT_LAYER.intValue() - 1;
	
	//Janan XCR-3837: Display untestable Area on virtual live CAD
    _untestableAreaLayer = ++_layerNumber;

    _topVirtualLiveVerificationImageLayer = ++_layerNumber;
    _bottomVirtualLiveVerificationImageLayer = ++_layerNumber;
    _bothVirtualLiveVerificationImageLayer = ++_layerNumber;
    _topPanelLayer = ++_layerNumber;
    _bottomPanelLayer = ++_layerNumber;

    _topPanelFiducialLayer = ++_layerNumber;
    _bottomPanelFiducialLayer = ++_layerNumber;
    _addOnEmptyLayer = ++_layerNumber;
    for (Board board: _panel.getBoards())
    {
      _boardToTopBoardLayerMap.put(board, new Integer(++_layerNumber));
      _boardToBottomBoardLayerMap.put(board, new Integer(++_layerNumber));

      _boardToTopPadLayerMap.put(board, new Integer(++_layerNumber));
      _boardToBottomPadLayerMap.put(board, new Integer(++_layerNumber));

      _boardToTopComponentLayerMap.put(board, new Integer(++_layerNumber));
      _boardToBottomComponentLayerMap.put(board, new Integer(++_layerNumber));

      _boardToTopFiducialLayerMap.put(board, new Integer(++_layerNumber));
      _boardToBottomFiducialLayerMap.put(board, new Integer(++_layerNumber));

      _boardToTopRefDesLayerMap.put(board, new Integer(++_layerNumber));
      _boardToBottomRefDesLayerMap.put(board, new Integer(++_layerNumber));

      _boardToOriginLayerMap.put(board, new Integer(++_layerNumber));
      _boardToBoardNameLayerMap.put(board, new Integer(++_layerNumber));
    }
    _topPanelFiducialRefDesLayer = ++_layerNumber;
    _bottomPanelFiducialRefDesLayer = ++_layerNumber;
    _alignmentRegionLayer = ++_layerNumber;

    _topInspectionRegionRectangleLayer = ++_layerNumber;
    _bottomInspectionRegionRectangleLayer = ++_layerNumber;
    _topInspectionRegionPadBoundsLayer = ++_layerNumber;
    _bottomInspectionRegionPadBoundsLayer = ++_layerNumber;
    _topInspectionFocusRegionLayer = ++_layerNumber;
    _bottomInspectionFocusRegionLayer = ++_layerNumber;
    
    
    //Kee Chin Seong - New layer for the edit focus region
    _topEditFocusRegionInspectionFocusRegionLayer = ++_layerNumber;
    _bottomEditFocusRegionInspectionFocusRegionLayer = ++_layerNumber;
    _topEditFocusRegionInspectionRegionRectangleLayer = ++_layerNumber;
    _bottomEditFocusRegionInspectionRegionRectangleLayer = ++_layerNumber;

    _topVerificationRegionRectangleLayer = ++_layerNumber;
    _bottomVerificationRegionRectangleLayer = ++_layerNumber;
    _topVerificationFocusRegionLayer = ++_layerNumber;
    _bottomVerificationFocusRegionLayer = ++_layerNumber;

    _topAlignmentRegionRectangleLayer = ++_layerNumber;
    _bottomAlignmentRegionRectangleLayer = ++_layerNumber;
    _topAlignmentRegionPadBoundsLayer = ++_layerNumber;
    _bottomAlignmentRegionPadBoundsLayer = ++_layerNumber;
    _topAlignmentFocusRegionLayer = ++_layerNumber;
    _bottomAlignmentFocusRegionLayer = ++_layerNumber;

    _inspectionIrpBoundariesLayer = ++_layerNumber;
    _verificationIrpBoundariesLayer = ++_layerNumber;
    _opticalRegionSelectedLayer = ++_layerNumber;
    _dragOpticalRegionLayer = ++_layerNumber;
    _dragOpticalRegionUnSelectedLayer = ++_layerNumber;
    _opticalRegionUnSelectedLayer = ++_layerNumber;
    _highlightOpticalRegionLayer = ++_layerNumber;
    _dragAlignmentOpticalRegionLayer = ++_layerNumber;
    _meshTriangleLayer = ++_layerNumber;
    
    _topPadNameLayer = ++_layerNumber;
    _bottomPadNameLayer = ++_layerNumber;
    _virtualLiveAreaLayer = ++_layerNumber;
    
       //@author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
    _bottomNoLoadedComponentLayer = ++_layerNumber;
    _topNoLoadedComponentLayer = ++_layerNumber;
    
    _topNoLoadPadLayer = ++_layerNumber;
    _bottomNoLoadPadLayer = ++_layerNumber;
    _bottomNoLoadPadNameLayer = ++_layerNumber;
    _topNoLoadPadNameLayer = ++_layerNumber;
    _topNoLoadRefDesignatorLayer = ++_layerNumber;
    _bottomNoLoadRefDesignatorLayer = ++_layerNumber;
    _addOnEmptyLayer = ++_layerNumber;  
    _panelWidthRendererLayer = ++_layerNumber; 
  }

  /**
   * @author Bill Darbie
   */
  private void createTopLayers()
  {
    Assert.expect(_panel != null);

    // set things up like this:
    // - 1 panel layer
    // - 1 board layer per board
    // - Top componenent layer per board
    // - Top fiducial layer for panel fiducials
    // - Top fiducial layer per board for board fiducials

    // the higher the layer number, the  deeper down the layer is
    // put largest components at the smallest layer number so mouse clicks
    // will be able to select small components before larger ones
    _layerNumber = JLayeredPane.DEFAULT_LAYER.intValue() - 1;
    _topPanelLayer = ++_layerNumber;

    _topPanelFiducialLayer = ++_layerNumber;

    for (Board board: _panel.getBoards())
    {
      _boardToTopBoardLayerMap.put(board, new Integer(++_layerNumber));

      _boardToTopComponentLayerMap.put(board, new Integer(++_layerNumber));

      _boardToTopFiducialLayerMap.put(board, new Integer(++_layerNumber));

      _boardToBoardNameLayerMap.put(board, new Integer(++_layerNumber));
    }
    _topPanelFiducialRefDesLayer = ++_layerNumber;
    _addOnEmptyLayer = ++_layerNumber;  
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void repopulateReconstructionRegionGraphics()
  {
     repopulateLayersWithDebugGraphics();
  }

  /**
   * @author George A. David
   */
  private void repopulateLayersWithDebugAlignmentGraphics()
  {
    if (_drawDebugGraphics)
    {
      _graphicsEngine.removeRenderers(_topAlignmentFocusRegionLayer);
      _graphicsEngine.removeRenderers(_topAlignmentRegionPadBoundsLayer);
      _graphicsEngine.removeRenderers(_topAlignmentRegionRectangleLayer);
      _graphicsEngine.removeRenderers(_bottomAlignmentFocusRegionLayer);
      _graphicsEngine.removeRenderers(_bottomAlignmentRegionPadBoundsLayer);
      _graphicsEngine.removeRenderers(_bottomAlignmentRegionRectangleLayer);

      populateLayersWithDebugAlignmentGraphics();
    }
  }

  /**
   * @author George A. David
   */
  private void populateLayersWithDebugAlignmentGraphics()
  {
    if (_drawDebugGraphics)
    {
      TestProgram testProgram = MainMenuGui.getInstance().getTestProgramWithReason("Calculating data for graphics engine. . .",
          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
          _panel.getProject());
      // alignmentRegions regions
      for (ReconstructionRegion alignmentRegion : testProgram.getAlignmentRegions())
      {
        InspectionRegionRenderer regionRenderer = new InspectionRegionRenderer(alignmentRegion);
        InspectionRegionRenderer boundsRenderer = new InspectionRegionRenderer(alignmentRegion);
        boundsRenderer.setUsePadBounds(true);
        if (alignmentRegion.isTopSide())
        {
          _graphicsEngine.addRenderer(_topAlignmentRegionRectangleLayer, regionRenderer);
          _graphicsEngine.addRenderer(_topAlignmentRegionPadBoundsLayer, boundsRenderer);
        }
        else
        {
          _graphicsEngine.addRenderer(_bottomAlignmentRegionRectangleLayer, regionRenderer);
          _graphicsEngine.addRenderer(_bottomAlignmentRegionPadBoundsLayer, boundsRenderer);
        }

        int index = 0;
        for (FocusGroup focusGroup : alignmentRegion.getFocusGroups())
        {
          if (index == 0)
          {
            FocusRegionRenderer focusRenderer = new FocusRegionRenderer(focusGroup.getFocusSearchParameters().
                getFocusRegion());
            if (alignmentRegion.isTopSide())
              _graphicsEngine.addRenderer(_topAlignmentFocusRegionLayer, focusRenderer);
            else
              _graphicsEngine.addRenderer(_bottomAlignmentFocusRegionLayer, focusRenderer);
            break;
          }
          ++index;
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  private void repopulateLayersWithDebugGraphics()
  {
    if (_drawDebugGraphics)
    {
      _graphicsEngine.removeRenderers(_inspectionIrpBoundariesLayer);
      _graphicsEngine.removeRenderers(_verificationIrpBoundariesLayer);
   //   _graphicsEngine.removeRenderers(_opticalRegionSelectedLayer);

      _graphicsEngine.removeRenderers(_topVerificationFocusRegionLayer);
      _graphicsEngine.removeRenderers(_topVerificationRegionRectangleLayer);
      _graphicsEngine.removeRenderers(_bottomVerificationFocusRegionLayer);
      _graphicsEngine.removeRenderers(_bottomVerificationRegionRectangleLayer);

      _graphicsEngine.removeRenderers(_topAlignmentFocusRegionLayer);
      _graphicsEngine.removeRenderers(_topAlignmentRegionPadBoundsLayer);
      _graphicsEngine.removeRenderers(_topAlignmentRegionRectangleLayer);
      _graphicsEngine.removeRenderers(_bottomAlignmentFocusRegionLayer);
      _graphicsEngine.removeRenderers(_bottomAlignmentRegionPadBoundsLayer);
      _graphicsEngine.removeRenderers(_bottomAlignmentRegionRectangleLayer);
      
      populateLayersWithDebugGraphics();
    }
    
    //Kee Chin Seong - Remove the renderers
    _graphicsEngine.removeRenderers(_topEditFocusRegionInspectionFocusRegionLayer);
    _graphicsEngine.removeRenderers(_bottomEditFocusRegionInspectionFocusRegionLayer);
    _graphicsEngine.removeRenderers(_bottomEditFocusRegionInspectionRegionRectangleLayer);
    _graphicsEngine.removeRenderers(_topEditFocusRegionInspectionRegionRectangleLayer);
    
    
  }

  /**
   * This method is synchronized because it's call from both the initial populate and the response to a program
   * generation.  But, the initial populate needs to create a test program, so this may be called prior to it being
   * initially populated
   * @author George A. David
   */
  private synchronized void populateLayersWithDebugGraphics()
  {
    Project project = _panel.getProject();

    TestProgram testProgram = MainMenuGui.getInstance().getTestProgramWithReason("Calculating data for graphics engine. . .",
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        project);
    
    // populate the region layer only if it's turned on (performance reasons)
    if (_drawDebugGraphics)
    {
      // irp boundaries
      for (TestSubProgram subProgram : testProgram.getAllTestSubPrograms())
      {
        if(subProgram.hasInspectionRegions() == false)
          continue;

        for (ProcessorStrip processorStrip : subProgram.getProcessorStripsForInspectionRegions())
        {
          RectangleRenderer renderer = new RectangleRenderer(processorStrip.getRegion().getRectangle2D(), true);
          renderer.setFillShape(false);
          _graphicsEngine.addRenderer(_inspectionIrpBoundariesLayer, renderer);
        }

        for (ProcessorStrip processorStrip : subProgram.getProcessorStripsForVerificationRegions())
        {
          RectangleRenderer renderer = new RectangleRenderer(processorStrip.getRegion().getRectangle2D(), true);
          renderer.setFillShape(false);
          _graphicsEngine.addRenderer(_verificationIrpBoundariesLayer, renderer);
        }
        
      /*   for (OpticalRegion opticalRegion : subProgram.getOpticalRegions())
        {
          RectangleRenderer renderer = new RectangleRenderer(_graphicsEngine.getSelectedComponentRegion().getBounds2D(), true);
          renderer.setFillShape(true);
          _graphicsEngine.addRenderer(_opticalRegionSelectedLayer, renderer);
        }  */
        
      }

      // verification regions
      for (ReconstructionRegion verificationRegion : testProgram.getVerificationRegions())
      {
        VerificationRegionRenderer regionRenderer = new VerificationRegionRenderer(verificationRegion);
        if (verificationRegion.isTopSide())
          _graphicsEngine.addRenderer(_topVerificationRegionRectangleLayer, regionRenderer);
        else
          _graphicsEngine.addRenderer(_bottomVerificationRegionRectangleLayer, regionRenderer);

        for (FocusGroup focusGroup : verificationRegion.getFocusGroups())
        {
          FocusRegionRenderer focusRenderer = new FocusRegionRenderer(focusGroup.getFocusSearchParameters().
              getFocusRegion());
          if (verificationRegion.isTopSide())
            _graphicsEngine.addRenderer(_topVerificationFocusRegionLayer, focusRenderer);
          else
            _graphicsEngine.addRenderer(_bottomVerificationFocusRegionLayer, focusRenderer);
        }
      }
      
      // inspection regions pad bounds
      for (ReconstructionRegion inspectionRegion : testProgram.getAllInspectionRegions())
      {
        InspectionRegionRenderer boundsRenderer = new InspectionRegionRenderer(inspectionRegion);
        
        boundsRenderer.setUsePadBounds(true);
        if (inspectionRegion.isTopSide())
        {
          _graphicsEngine.addRenderer(_topInspectionRegionPadBoundsLayer, boundsRenderer);
        }
        else
        {
          _graphicsEngine.addRenderer(_bottomInspectionRegionPadBoundsLayer, boundsRenderer);
        }
      }
      
       for (TestSubProgram subProgram : testProgram.getAllTestSubPrograms())
       {
          if(subProgram.hasInspectionRegions() == false)
             continue;
         // inspection regions
         for (ReconstructionRegion inspectionRegion : subProgram.getAllInspectionRegions())
         {
           InspectionRegionRenderer regionRenderer = new InspectionRegionRenderer(inspectionRegion);
           if (inspectionRegion.isTopSide())
           {
             _graphicsEngine.addRenderer(_topInspectionRegionRectangleLayer, regionRenderer);
           }
           else
           {
             _graphicsEngine.addRenderer(_bottomInspectionRegionRectangleLayer, regionRenderer);
           }

    //        if(inspectionRegion.hasSurfaceModelingSlice())
    //        {
    //          FocusRegionRenderer focusRenderer = new FocusRegionRenderer(inspectionRegion.getSurfaceModelingFocusRegion());
    //          focusRenderer.setForeground(Color.white);
    //          if (inspectionRegion.isTopSide())
    //            _graphicsEngine.addRenderer(_topInspectionFocusRegionLayer, focusRenderer);
    //          else
    //            _graphicsEngine.addRenderer(_bottomInspectionFocusRegionLayer, focusRenderer);
    //        }
    //        else
           {
             int index = 0;
             for (FocusGroup focusGroup : inspectionRegion.getFocusGroups())
             {
               if (index == 0)
               {
                 FocusRegionRenderer focusRenderer = new FocusRegionRenderer(focusGroup.getFocusSearchParameters().
                     getFocusRegion());
                 if (inspectionRegion.isTopSide())
                   _graphicsEngine.addRenderer(_topInspectionFocusRegionLayer, focusRenderer);
                 else
                   _graphicsEngine.addRenderer(_bottomInspectionFocusRegionLayer, focusRenderer);
                 break;
               }
               ++index;
             }
           }
         }
         populateLayersWithDebugAlignmentGraphics();
        }
    }
  }

  /*
   * @author Kee Chin Seong
   */
  private void populatePanelLayersWithRenderers()
  {
    // now add all renderers to the layers
    PanelRenderer topPanelRenderer = new PanelRenderer(_panel);
    _graphicsEngine.addRenderer(_topPanelLayer, topPanelRenderer);
    PanelRenderer bottomPanelRenderer = new PanelRenderer(_panel);
    _graphicsEngine.addRenderer(_bottomPanelLayer, bottomPanelRenderer);

    // add panel fiducials and their reference designators for top and bottom panel sides
    Collection<Fiducial> panelFiducials = _panel.getFiducials();
    for (Fiducial fiducial : panelFiducials)
    {
      FiducialRenderer fiducialRenderer = new FiducialRenderer(fiducial);
      FiducialReferenceDesignatorRenderer fiducialReferenceDesignatorRenderer = new FiducialReferenceDesignatorRenderer(
          fiducial);
      if (fiducial.isTopSide())
      {
        _graphicsEngine.addRenderer(_topPanelFiducialLayer, fiducialRenderer);
        _graphicsEngine.addRenderer(_topPanelFiducialRefDesLayer, fiducialReferenceDesignatorRenderer);
      }
      else
      {
        _graphicsEngine.addRenderer(_bottomPanelFiducialLayer, fiducialRenderer);
        _graphicsEngine.addRenderer(_bottomPanelFiducialRefDesLayer, fiducialReferenceDesignatorRenderer);
      }
    }

	//Janan XCR-3837: Display untestable Area on virtual live CAD
    if(_panel.hasUntestableArea())
    {
      _graphicsEngine.addRenderer(_untestableAreaLayer, new UntestableAreaRenderer(_panel));
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void populateReferenceDesignatorLayerWithRenderers(Board board, Collection<Component> components, boolean isTopside)
  {
    for (Component component : components)
    {
      if(component.isLoaded() == true)
      {
         ReferenceDesignatorRenderer referenceDesignatorRenderer = new ReferenceDesignatorRenderer(component);

         Integer layerInt = (isTopside == true) ? (Integer)_boardToTopRefDesLayerMap.get(board) :
                                                  (Integer)_boardToBottomRefDesLayerMap.get(board);
         
         _graphicsEngine.addRenderer(layerInt.intValue(), referenceDesignatorRenderer);
         
         List<ReferenceDesignatorRenderer> compRefDesRenderers = null;
         if (_componentTypeToReferenceDesignatorRenderersMap.containsKey(component.getComponentType()))
           compRefDesRenderers = _componentTypeToReferenceDesignatorRenderersMap.get(component.getComponentType());
         else
         {
           compRefDesRenderers = new ArrayList<ReferenceDesignatorRenderer>();
           Object previous = _componentTypeToReferenceDesignatorRenderersMap.put(component.getComponentType(), compRefDesRenderers);
           Assert.expect(previous == null);
         }
         compRefDesRenderers.add(referenceDesignatorRenderer);
      }
      else
      {
         ReferenceDesignatorRenderer referenceDesignatorRenderer = new ReferenceDesignatorRenderer(component);

         Integer layerInt = (isTopside == true) ? (Integer)_topNoLoadRefDesignatorLayer :
                                                  (Integer)_bottomNoLoadRefDesignatorLayer;
         
         _graphicsEngine.addRenderer(layerInt.intValue(), referenceDesignatorRenderer);
         
         List<ReferenceDesignatorRenderer> compRefDesRenderers = null;
         if (_noLoadComponentTypeToReferenceDesignatorRenderersMap.containsKey(component.getComponentType()))
           compRefDesRenderers = _noLoadComponentTypeToReferenceDesignatorRenderersMap.get(component.getComponentType());
         else
         {
           compRefDesRenderers = new ArrayList<ReferenceDesignatorRenderer>();
           Object previous = _noLoadComponentTypeToReferenceDesignatorRenderersMap.put(component.getComponentType(), compRefDesRenderers);
           Assert.expect(previous == null);
         }
         compRefDesRenderers.add(referenceDesignatorRenderer);
      }
    }
  }
  
   /*
   * @author Kee Chin Seong
   */
  private void unpopulateComponentLayerWithRenderers(Collection<Component> components)
  {
    for(Component component  : components)
    {
      _componentTypeToComponentRenderersMap.remove(component.getComponentType());
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void populateComponentLayersWithRenderers(Board board, Collection<Component> components, boolean isTopSide, boolean isNeedRepopulate)
  {
     for (Component component : components)
     {
        if(component.isLoaded() == true)
        {
          ComponentRenderer componentRenderer = null;
          
          if(!isNeedRepopulate)
             componentRenderer = new ComponentRenderer(this, component);
          
          ComponentType componentType = component.getComponentType();
          List<ComponentRenderer> componentRenderers = null;
          //ComponentRenderer componentRenderer = null;
          List<ReferenceDesignatorRenderer> compRefDesRenderers = null;
          if (_componentTypeToComponentRenderersMap.containsKey(componentType))
          {
            componentRenderers = _componentTypeToComponentRenderersMap.get(componentType);
          }
          else
          {
            componentRenderers = new ArrayList<ComponentRenderer>();
            Object previous = _componentTypeToComponentRenderersMap.put(componentType, componentRenderers);
            Assert.expect(previous == null);
          }
          
          if(isNeedRepopulate)
          {
            if(componentRenderers.size() == 0 )
            {
              componentRenderer = new ComponentRenderer(this, component);
              componentRenderers.add(componentRenderer);
            }
          }
          else
            componentRenderers.add(componentRenderer);
          
          Integer layerInt = (isTopSide == true) ? (Integer)_boardToTopComponentLayerMap.get(board) :
                                                   (Integer)_boardToBottomComponentLayerMap.get(board);
          
          //for(ComponentRenderer compRenderer : componentRenderers)
            _graphicsEngine.addRenderer(layerInt.intValue(), componentRenderer); 
           
        }
        else
        {
          ComponentRenderer componentRenderer = null;
          
          if(!isNeedRepopulate)
             componentRenderer = new ComponentRenderer(this, component);
          
          ComponentType componentType = component.getComponentType();
          List<ComponentRenderer> componentRenderers = null;
          //ComponentRenderer componentRenderer = null;
          List<ReferenceDesignatorRenderer> compRefDesRenderers = null;
          if (_noLoadComponentTypeToComponentRenderersMap.containsKey(componentType))
          {
            componentRenderers = _noLoadComponentTypeToComponentRenderersMap.get(componentType);
          }
          else
          {
            componentRenderers = new ArrayList<ComponentRenderer>();
            Object previous = _noLoadComponentTypeToComponentRenderersMap.put(componentType, componentRenderers);
            Assert.expect(previous == null);
          }
          
          if(isNeedRepopulate)
          {
            if(componentRenderers.size() == 0 )
            {
              componentRenderer = new ComponentRenderer(this, component);
              componentRenderers.add(componentRenderer);
            }
          }
          else
            componentRenderers.add(componentRenderer);
          
          Integer layerInt = (isTopSide == true) ? (Integer)_topNoLoadedComponentLayer :
                                                   (Integer)_bottomNoLoadedComponentLayer;
          
          _graphicsEngine.addRenderer(layerInt.intValue(), componentRenderer); 
            
        }
      
        // pads are only displayed once the user zooms in
//          Collection pads = component.getPads();
//          numPads += pads.size();
//          Iterator pit = pads.iterator();
//          while (pit.hasNext())
//          {
//            Pad pad = (Pad)pit.next();
//            PadRenderer padRenderer = new PadRenderer(pad, componentRenderer);
//            layerInt = (Integer)_boardToTopPadLayerMap.get(board);
//            _graphicsEngine.addRenderer(layerInt.intValue(), padRenderer);
//
//            if (pad.isThroughHolePad())
//            {
//              // through hole pads show up on both sides of the panel
//              padRenderer = new PadRenderer(pad, componentRenderer);
//              layerInt = (Integer)_boardToBottomPadLayerMap.get(board);
//              _graphicsEngine.addRenderer(layerInt.intValue(), padRenderer);
//            }
//          }
     }
  }
  
  /*
   * @author Kee Chin seong
   */
  private void populateFiducialWithLayers(Board board, Collection<Fiducial> boardFiducials, boolean isTopSide)
  {
     for (Fiducial fiducial : boardFiducials)
     {
        FiducialRenderer fiducialRenderer = new FiducialRenderer(fiducial);
        
        Integer layerInt = (isTopSide == true) ? (Integer)_boardToTopFiducialLayerMap.get(board) :
                                                 (Integer)_boardToBottomFiducialLayerMap.get(board);
        
        _graphicsEngine.addRenderer(layerInt.intValue(), fiducialRenderer);
        FiducialReferenceDesignatorRenderer fiducialReferenceDesignatorRenderer = new FiducialReferenceDesignatorRenderer(fiducial);
        
        layerInt = (isTopSide == true) ? (Integer)_boardToTopRefDesLayerMap.get(board) :
                                         (Integer)_boardToBottomRefDesLayerMap.get(board);
        
        _graphicsEngine.addRenderer(layerInt.intValue(), fiducialReferenceDesignatorRenderer);
     }
  }
  
  /**
   * @author Bill Darbie
   * @Edited By Kee Chin Seong
   */
  private void populateLayersWithRenderers()
  {
//    long startTime = System.currentTimeMillis();

    Assert.expect(_panel != null);
    
    populatePanelLayersWithRenderers();
    populateLayersWithDebugGraphics();

    Collection<Board> boards = _panel.getBoards();
    for (Board board : boards)
    {
      populateBoardLayers(board);
    }
   
//wpd - used to test ImageBufferRenderer
//    System.out.println("panel width: " + _panel.getWidthInNanoMeters());
//    System.out.println("panel height: " + _panel.getLengthInNanoMeters());
//    TestImageRenderer testImageRenderer = new TestImageRenderer();
//    graphicsEngine.addRenderer(-100, testImageRenderer);
//    graphicsEngine.setVisible(-100, true);
//
//    Iterator it = _panel.getBoards().iterator();
//    while (it.hasNext())
//    {
//      Board board = (Board)it.next();
//      System.out.println("wpd board: " + board + " joints: " + board.getNumJoints());
//    }

//    long stopTime = System.currentTimeMillis();
//    if (UnitTest.unitTesting() == false)
//      System.out.println("Generate Renderers in millis: " + (stopTime - startTime));
  }
  
  /**
   * Just creates enough renderers to show the top side panel, board and components.  Used typically for
   * JPG generation, where we don't need all renderers because we'll just create the JPG then throw them all away.
   * @author Andy Mechtenberg
   * @Edited By Kee Chin Seong
   */
  private void populateTopLayersWithRenderers()
  {
//    long startTime = System.currentTimeMillis();

    Assert.expect(_panel != null);
    // now add all renderers to the layers
    PanelRenderer topPanelRenderer = new PanelRenderer(_panel, true);
    _graphicsEngine.addRenderer(_topPanelLayer, topPanelRenderer);
   
   // add panel fiducials and their reference designators for top and bottom panel sides
    Collection<Fiducial> panelFiducials = _panel.getFiducials();
    for (Fiducial fiducial : panelFiducials)
    {
      FiducialRenderer fiducialRenderer = new FiducialRenderer(fiducial);
      if (fiducial.isTopSide())
      {
        _graphicsEngine.addRenderer(_topPanelFiducialLayer, fiducialRenderer);
      }
    }

    Collection<Board> boards = _panel.getBoards();
    Integer panelWidthLayerInt = 0;
    for (Board board : boards)
    {
      BoardType boardType = board.getBoardType();
      List<BoardRenderer> boardRenderers = null;

      if (_boardTypeToBoardRenderersMap.containsKey(boardType))
      {
        boardRenderers = _boardTypeToBoardRenderersMap.get(boardType);
      }
      else
      {
        boardRenderers = new ArrayList<BoardRenderer>();
        Object previous = _boardTypeToBoardRenderersMap.put(boardType, boardRenderers);
        Assert.expect(previous == null);
      }
      BoardRenderer topBoardRenderer = new BoardRenderer(board);
      boardRenderers.add(topBoardRenderer);
      Integer layerInt = _boardToTopBoardLayerMap.get(board);
      _graphicsEngine.addRenderer(layerInt.intValue(), topBoardRenderer);
      //add the board name renderer

//      if (_panel.getNumBoards() > 1)
//      {
        layerInt = (Integer)_boardToBoardNameLayerMap.get(board);
        BoardNameRenderer boardNameRenderer = new BoardNameRenderer(board);
        _graphicsEngine.addRenderer(layerInt, boardNameRenderer);
//      }

      //@author Kee Chin Seong - Panel Width Renderer
      panelWidthLayerInt = (Integer)_boardToBoardNameLayerMap.get(_panel.getBoards().get(0));
      PanelWidthRenderer panelWidthRenderer = new PanelWidthRenderer(_panel);
      panelWidthRenderer.scalingFontToFitInPanel(false);
      _graphicsEngine.addRenderer((Integer)panelWidthLayerInt, panelWidthRenderer);
      _graphicsEngine.setVisible((Integer)panelWidthLayerInt, false);
     
      if (board.topSideBoardExists())
      {
        SideBoard topSideBoard = board.getTopSideBoard();

    // Populate top board side components
        Collection<Component> components = topSideBoard.getComponents();
        for (Component component : components)
        {
          //XCR-3618-Show no load components in Panel image frame.
          if (component.isLoaded() == true)
          {    
          ComponentRenderer componentRenderer = new ComponentRenderer(this, component);
          ComponentType componentType = component.getComponentType();
          List<ComponentRenderer> componentRenderers = null;
          if (_componentTypeToComponentRenderersMap.containsKey(componentType))
          {
            componentRenderers = _componentTypeToComponentRenderersMap.get(componentType);
          }
          else
          {
            componentRenderers = new ArrayList<ComponentRenderer>();
            Object previous = _componentTypeToComponentRenderersMap.put(componentType, componentRenderers);
            Assert.expect(previous == null);
          }
          componentRenderers.add(componentRenderer);
          layerInt = (Integer)_boardToTopComponentLayerMap.get(board);
          _graphicsEngine.addRenderer(layerInt.intValue(), componentRenderer);
        }
        else   
        {   // Populate top board side no loaded components

          ComponentRenderer componentRenderer = new ComponentRenderer(this, component);
          ComponentType componentType = component.getComponentType();  
          List<ComponentRenderer> componentRenderers = null;
  
          if (_noLoadComponentTypeToComponentRenderersMap.containsKey(componentType))
          {
            componentRenderers = _noLoadComponentTypeToComponentRenderersMap.get(componentType);
          }
          else
          {
            componentRenderers = new ArrayList<ComponentRenderer>();
            Object previous = _noLoadComponentTypeToComponentRenderersMap.put(componentType, componentRenderers);
            Assert.expect(previous == null);
          }
          componentRenderers.add(componentRenderer);
          layerInt = (Integer)_topNoLoadedComponentLayer;
          _graphicsEngine.addRenderer(layerInt.intValue(),componentRenderer);
          // add on layer to keep the no load component layer remain as red instead of white for panel board.
          _graphicsEngine.addRenderer(_addOnEmptyLayer, componentRenderer);
        } 
        } 
     
        // Populate top board side fiducials
        Collection<Fiducial> boardFiducials = topSideBoard.getFiducials();
        for (Fiducial fiducial : boardFiducials)
        {
          FiducialRenderer fiducialRenderer = new FiducialRenderer(fiducial);
          layerInt = (Integer)_boardToTopFiducialLayerMap.get(board);
          _graphicsEngine.addRenderer(layerInt.intValue(), fiducialRenderer);
        }
      }
    }

    //@author Kee Chin Seong - Panel Width Renderer
      PanelWidthRenderer panelWidthRenderer = new PanelWidthRenderer(_panel);
      panelWidthRenderer.scalingFontToFitInPanel(false);
      _graphicsEngine.addRenderer((Integer)_panelWidthRendererLayer, panelWidthRenderer);
      _graphicsEngine.setVisible((Integer)_panelWidthRendererLayer, false);
//    long stopTime = System.currentTimeMillis();
//    if (UnitTest.unitTesting() == false)
//      System.out.println("Generate Renderers FOR JPG in millis: " + (stopTime - startTime));
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateBoardLayers(Board board)
  {
    BoardType boardType = board.getBoardType();
    List<BoardRenderer> boardRenderers = null;

    if (_boardTypeToBoardRenderersMap.containsKey(boardType))
    {
      boardRenderers = _boardTypeToBoardRenderersMap.get(boardType);
    }
    else
    {
      boardRenderers = new ArrayList<BoardRenderer>();
      Object previous = _boardTypeToBoardRenderersMap.put(boardType, boardRenderers);
      Assert.expect(previous == null);
    }
    BoardRenderer topBoardRenderer = new BoardRenderer(board);
    boardRenderers.add(topBoardRenderer);
    Integer layerInt = _boardToTopBoardLayerMap.get(board);
    _graphicsEngine.addRenderer(layerInt.intValue(), topBoardRenderer);
    BoardRenderer bottomBoardRenderer = new BoardRenderer(board);
    boardRenderers.add(bottomBoardRenderer);
    layerInt = _boardToBottomBoardLayerMap.get(board);
    _graphicsEngine.addRenderer(layerInt.intValue(), bottomBoardRenderer);
    
    //@author Kee Chin Seong - Panel Width Renderer - Removed this so that will not show ing 
    //                         panel
    //PanelWidthRenderer panelWidthRenderer = new PanelWidthRenderer(board.getPanel());
    //panelWidthRenderer.scalingFontToFitInPanel(true);
    //_graphicsEngine.addRenderer(layerInt.intValue(), panelWidthRenderer);
    //_graphicsEngine.setVisible(layerInt.intValue(), true);

    if (board.topSideBoardExists())
    {
      SideBoard topSideBoard = board.getTopSideBoard();

      // add the board origin and axis renderers
      layerInt = (Integer)_boardToOriginLayerMap.get(board);
      BoardCadOriginRenderer boardOriginRenderer = new BoardCadOriginRenderer(board);
      _graphicsEngine.addRenderer(layerInt, boardOriginRenderer);
      BoardCadOriginAxisLetterRenderer xAxisRenderer = new BoardCadOriginAxisLetterRenderer(board, true);
      _graphicsEngine.addRenderer(layerInt, xAxisRenderer);
      BoardCadOriginAxisLetterRenderer yAxisRenderer = new BoardCadOriginAxisLetterRenderer(board, false);
      _graphicsEngine.addRenderer(layerInt, yAxisRenderer);

      //add the board name renderer
      layerInt = (Integer)_boardToBoardNameLayerMap.get(board);
      BoardNameRenderer boardNameRenderer = new BoardNameRenderer(board);
      _graphicsEngine.addRenderer(layerInt, boardNameRenderer);
    
      // Populate top board side components
      Collection<Component> components = topSideBoard.getComponents();
      populateComponentLayersWithRenderers(board, components, true, false);
      populateReferenceDesignatorLayerWithRenderers(board, components, true);
      
      // Populate top board side fiducials
      populateFiducialWithLayers(board, topSideBoard.getFiducials(), true);
    }

    // moving on to the bottom board
    if (board.bottomSideBoardExists())
    {
      SideBoard bottomSideBoard = board.getBottomSideBoard();

      // add the board origin and axis renderers
      layerInt = (Integer)_boardToOriginLayerMap.get(board);
      BoardCadOriginRenderer boardOriginRenderer = new BoardCadOriginRenderer(board);
      _graphicsEngine.addRenderer(layerInt, boardOriginRenderer);
      BoardCadOriginAxisLetterRenderer xAxisRenderer = new BoardCadOriginAxisLetterRenderer(board, true);
      _graphicsEngine.addRenderer(layerInt, xAxisRenderer);
      BoardCadOriginAxisLetterRenderer yAxisRenderer = new BoardCadOriginAxisLetterRenderer(board, false);
      _graphicsEngine.addRenderer(layerInt, yAxisRenderer);

      //add the board name renderer only if the top side didn't exist
      if ((board.topSideBoardExists() == false))// && (board.getPanel().getNumBoards() > 1))
      {
        layerInt = (Integer)_boardToBoardNameLayerMap.get(board);
        BoardNameRenderer boardNameRenderer = new BoardNameRenderer(board);
        _graphicsEngine.addRenderer(layerInt, boardNameRenderer);
      }
      Collection<Component> components = bottomSideBoard.getComponents();
      populateComponentLayersWithRenderers(board, components, false, false);
      populateReferenceDesignatorLayerWithRenderers(board, components, false);
      
      // Populate bottom board side fiducials
      populateFiducialWithLayers(board, bottomSideBoard.getFiducials(), false);
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setEditFocusRegionMode(boolean isEnabled)
  {
    _isFocusRegionEditorOn = isEnabled;
  }

  /**
   * @return a List of Integers of all the top side layers
   * @author Bill Darbie
   */
  List<Integer> getTopLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(_topPanelLayer);
    layers.add(_topPanelFiducialLayer);
    layers.add(_topPanelFiducialRefDesLayer);
    layers.add(_addOnEmptyLayer);
    layers.addAll(_boardToTopBoardLayerMap.values());
    layers.addAll(_boardToTopPadLayerMap.values());
    layers.addAll(_boardToTopComponentLayerMap.values());
    layers.addAll(_boardToTopRefDesLayerMap.values());
    layers.addAll(_boardToTopFiducialLayerMap.values());
    layers.addAll(_boardToBoardNameLayerMap.values());
    layers.add(_topInspectionRegionRectangleLayer);
    layers.add(_topInspectionFocusRegionLayer);

    if(_isFocusRegionEditorOn == true)
    {
      layers.add(_topEditFocusRegionInspectionFocusRegionLayer);
      layers.add(_topEditFocusRegionInspectionRegionRectangleLayer);
    }
    layers.add(_topInspectionRegionPadBoundsLayer);
    layers.add(_topAlignmentRegionRectangleLayer);
    layers.add(_topAlignmentRegionPadBoundsLayer);
    layers.add(_topAlignmentFocusRegionLayer);
    layers.add(_topVerificationRegionRectangleLayer);
    layers.add(_topVerificationFocusRegionLayer);

//    layers.add(_inspectionIrpBoundariesLayer);

    return layers;
  }

  /*
   * @author Kee Chin Seong
   */
  public void setTopGraphicLayersAccordingToCustomizeSettings()
  {
    _graphicsEngine.setVisible(_topPanelLayer, _showTopPanelLayer);
    _graphicsEngine.setVisible(_topPanelFiducialLayer, true);
    _graphicsEngine.setVisible(_topPanelFiducialRefDesLayer, true);

    _graphicsEngine.setVisible(getTopBoardLayers(), true);
    _graphicsEngine.setVisible(getTopPadLayers(), _showTopPadLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopComponentLayers(), _showTopComponentLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopReferenceDesignatorLayers(), _showTopComponentRefDesLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopBoardFiducialLayers(), true);
    _graphicsEngine.setVisible(getBoardNameLayers(), false);

    _graphicsEngine.setVisible(_topInspectionRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_topInspectionRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_topInspectionFocusRegionLayer, true);
    _graphicsEngine.setVisible(_topAlignmentRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_topAlignmentRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_topAlignmentFocusRegionLayer, true);
    _graphicsEngine.setVisible(_topVerificationRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_topVerificationFocusRegionLayer, true);
    
    _graphicsEngine.setVisible(_topEditFocusRegionInspectionFocusRegionLayer, false);
    _graphicsEngine.setVisible(_topEditFocusRegionInspectionRegionRectangleLayer, false);
    
    _graphicsEngine.setVisible(getBottomLayers(), false);
    
    _graphicsEngine.setCrossHairsSelectionVisibility(false);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setBottomGraphicLayersAccordingToCustomizeSettings()
  {
    _graphicsEngine.setVisible(_bottomPanelLayer, _showBottomPanelLayer);
    _graphicsEngine.setVisible(_bottomPanelFiducialLayer, true);
    _graphicsEngine.setVisible(_bottomPanelFiducialRefDesLayer, true);

    _graphicsEngine.setVisible(getBottomBoardLayers(), true);
    _graphicsEngine.setVisible(getBottomPadLayers(), _showBottomPadLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomComponentLayers(), _showBottomComponentLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomReferenceDesignatorLayers(), _showBottomComponentRefDesLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomBoardFiducialLayers(), true);
    _graphicsEngine.setVisible(getBoardNameLayers(), false);

    _graphicsEngine.setVisible(_bottomInspectionRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_bottomInspectionRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_bottomInspectionFocusRegionLayer, true);
    _graphicsEngine.setVisible(_bottomAlignmentRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_bottomAlignmentRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_bottomAlignmentFocusRegionLayer, true);
    _graphicsEngine.setVisible(_bottomVerificationRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_bottomVerificationFocusRegionLayer, true);
    
    //Kee Chin Seong - Visiblity for Edit focus REgion layer
    _graphicsEngine.setVisible(_bottomEditFocusRegionInspectionRegionRectangleLayer, true);  
    _graphicsEngine.setVisible(_bottomEditFocusRegionInspectionFocusRegionLayer, true);
    
    _graphicsEngine.setVisible(getTopLayers(), false);
    
    _graphicsEngine.setCrossHairsSelectionVisibility(false);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setBothSidesGraphicLayersAccordingToCustomizeSettings()
  {
    //set top side
    _graphicsEngine.setVisible(_topPanelLayer, _showTopPanelLayer);
    _graphicsEngine.setVisible(_topPanelFiducialLayer, true);
    _graphicsEngine.setVisible(_topPanelFiducialRefDesLayer, true);

    _graphicsEngine.setVisible(getTopBoardLayers(), true);
    _graphicsEngine.setVisible(getTopPadLayers(), _showTopPadLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopComponentLayers(), _showTopComponentLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopReferenceDesignatorLayers(), _showTopComponentRefDesLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopBoardFiducialLayers(), true);
    _graphicsEngine.setVisible(getBoardNameLayers(), false);

    _graphicsEngine.setVisible(_topInspectionRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_topInspectionRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_topInspectionFocusRegionLayer, true);
    _graphicsEngine.setVisible(_topAlignmentRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_topAlignmentRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_topAlignmentFocusRegionLayer, true);
    _graphicsEngine.setVisible(_topVerificationRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_topVerificationFocusRegionLayer, true);

    //Ok, now set bottom side
    _graphicsEngine.setVisible(_bottomPanelLayer, _showBottomPanelLayer);
    _graphicsEngine.setVisible(_bottomPanelFiducialLayer, true);
    _graphicsEngine.setVisible(_bottomPanelFiducialRefDesLayer, true);

    _graphicsEngine.setVisible(getBottomBoardLayers(), true);
    _graphicsEngine.setVisible(getBottomPadLayers(), _showBottomPadLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomComponentLayers(), _showBottomComponentLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomReferenceDesignatorLayers(), _showBottomComponentRefDesLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomBoardFiducialLayers(), true);
    _graphicsEngine.setVisible(getBoardNameLayers(), false);

    _graphicsEngine.setVisible(_bottomInspectionRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_bottomInspectionRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_bottomInspectionFocusRegionLayer, true);
    _graphicsEngine.setVisible(_bottomAlignmentRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_bottomAlignmentRegionPadBoundsLayer, true);
    _graphicsEngine.setVisible(_bottomAlignmentFocusRegionLayer, true);
    _graphicsEngine.setVisible(_bottomVerificationRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_bottomVerificationFocusRegionLayer, true);
    
    //Kee Chin Seong - Visiblity for Edit Focus Region Layer
    _graphicsEngine.setVisible(_bottomEditFocusRegionInspectionRegionRectangleLayer, true);
    _graphicsEngine.setVisible(_bottomEditFocusRegionInspectionFocusRegionLayer, true);
    _graphicsEngine.setVisible(_topEditFocusRegionInspectionFocusRegionLayer, true);
    _graphicsEngine.setVisible(_topEditFocusRegionInspectionRegionRectangleLayer, true);
    
    _graphicsEngine.setCrossHairsSelectionVisibility(false);
  }
  
  /**
   * @return a List of Integers of all the bottom side layers
   * @author Bill Darbie
   */
  List<Integer> getBottomLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(_bottomPanelLayer);
    layers.add(_bottomPanelFiducialLayer);
    layers.add(_bottomPanelFiducialRefDesLayer);
    layers.addAll(_boardToBottomBoardLayerMap.values());
    layers.addAll(_boardToBottomPadLayerMap.values());
    layers.addAll(_boardToBottomComponentLayerMap.values());
    layers.addAll(_boardToBottomRefDesLayerMap.values());
    layers.addAll(_boardToBottomFiducialLayerMap.values());
    
    layers.add(_bottomInspectionRegionRectangleLayer);
    layers.add(_bottomInspectionFocusRegionLayer);
    
    if(_isFocusRegionEditorOn == true)
    {
      layers.add(_bottomEditFocusRegionInspectionFocusRegionLayer);
      layers.add(_bottomEditFocusRegionInspectionRegionRectangleLayer);
    }
    layers.add(_bottomInspectionRegionPadBoundsLayer);
    layers.add(_bottomAlignmentRegionRectangleLayer);
    layers.add(_bottomAlignmentRegionPadBoundsLayer);
    layers.add(_bottomAlignmentFocusRegionLayer);
    layers.add(_bottomVerificationRegionRectangleLayer);
    layers.add(_bottomVerificationFocusRegionLayer);
//    layers.add(_inspectionIrpBoundariesLayer);

    return layers;
  }

  /**
   * @author Andy Mechtenberg
   */
  List<Integer> getBoardOriginLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(_boardToOriginLayerMap.values());
    return layers;
  }

  /**
   * @author Andy Mechtenberg
   */
  List<Integer> getBoardOriginLayers(Board board)
  {
    List<Integer> layers = new ArrayList<Integer>();
    Integer integer = (Integer)_boardToOriginLayerMap.get(board);
    layers.add(integer);
    return layers;
  }

  /**
   * @author Andy Mechtenberg
   */
  List<Integer> getBoardNameLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(_boardToBoardNameLayerMap.values());
    return layers;
  }

  /**
   * @author Andy Mechtenberg
   */
  List<Integer> getBoardNameLayers(Board board)
  {
    List<Integer> layers = new ArrayList<Integer>();
    Integer integer = (Integer)_boardToBoardNameLayerMap.get(board);
    layers.add(integer);
    return layers;
  }

  /**
   * @return a List of Integers of all the layers on the entire panel
   * for both sides
   * @author Bill Darbie
   */
  public List<Integer> getAllLayers()
  {
    List<Integer> layers = getTopLayers();
    layers.addAll(getBottomLayers());
    layers.addAll(getBoardOriginLayers());

    return layers;
  }

  /**
   * @return  List of Integers of all the layers used by all the Boards on this Panel.
   * @author Bill Darbie
   */
  List<Integer> getAllLayers(Board board)
  {
    Assert.expect(board != null);

    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(getBoardLayers(board));
    layers.addAll(getBoardFiducialLayers(board));
    layers.addAll(getComponentLayers(board));
    layers.addAll(getReferenceDesignatorLayers(board));
    layers.addAll(getPadLayers(board));
    layers.addAll(getBoardOriginLayers(board));
    layers.addAll(getBoardNameLayers(board));

    return layers;
  }

  /**
   * @return the top Panel layer
   * @author Bill Darbie
   */
  int getTopPanelLayer()
  {
    return _topPanelLayer;
  }

  /**
   * @return the top Panel layer
   * @author Bill Darbie
   */
  int getBottomPanelLayer()
  {
    return _bottomPanelLayer;
  }

  /**
   * @return the List of Integers of all Panel layers.
   * @author Bill Darbie
   */
  List<Integer> getPanelLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(2);

    layers.add(new Integer(_topPanelLayer));
    layers.add(new Integer(_bottomPanelLayer));

    return layers;
  }

  /**
   * @return the top layer used by the Board passed in
   * @author Bill Darbie
   */
  int getTopBoardLayer(Board board)
  {
    Integer integer = (Integer)_boardToTopBoardLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return has bottom layer used by the Board passed in
   * @author Lim Boon Hong
   */
  private boolean hasBottomBoardLayer(Board board)
  {
    return _boardToBottomBoardLayerMap.containsKey(board);
  }
  
  /**
   * @return the bottom layer used by the Board passed in
   * @author Bill Darbie
   */
  private int getBottomBoardLayer(Board board)
  {
    Integer integer = (Integer)_boardToBottomBoardLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return a List of Integers of all the top Board layers.
   * @author Bill Darbie
   */
  List<Integer> getTopBoardLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToTopBoardLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all the bottom Board layers.
   * @author Bill Darbie
   */
  List<Integer> getBottomBoardLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToBottomBoardLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all Board layers
   * @author Bill Darbie
   */
  List<Integer> getBoardLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(getTopBoardLayers());
    layers.addAll(getBottomBoardLayers());

    return layers;
  }

  /**
   * @return the List of Integers for layers that the passed in board uses.
   * @return Bill Darbie
   * @check hasBottomBoardLayer by Lim Boon Hong
   */
  List<Integer> getBoardLayers(Board board)
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.add(new Integer(getTopBoardLayer(board)));
    if(hasBottomBoardLayer(board))
      layers.add(new Integer(getBottomBoardLayer(board)));

    return layers;
  }

  /**
   * @return the top Component layer used by the board passed in
   * @author Bill Darbie
   */
  int getTopComponentLayer(Board board)
  {
    Integer integer = (Integer)_boardToTopComponentLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return has bottom component layer used by the Board passed in
   * @author Lim Boon Hong
   */
  private boolean hasBottomBoardComponentLayer(Board board)
  {
    return _boardToBottomComponentLayerMap.containsKey(board);
  }
  
  /**
   * @return the bottom Component layer used by the board passed in
   * @author Bill Darbie
   */
  int getBottomComponentLayer(Board board)
  {
    Integer integer = (Integer)_boardToBottomComponentLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return a List of Integers of all the top Component layers.
   * @author Bill Darbie
   */
  List<Integer> getTopComponentLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToTopComponentLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all the bottom Component layers.
   * @author Bill Darbie
   */
  List<Integer> getBottomComponentLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToBottomComponentLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all Component layers
   * @author Bill Darbie
   */
  List<Integer> getComponentLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(getTopComponentLayers());
    layers.addAll(getBottomComponentLayers());

    return layers;
  }

  /**
   * @return the List of Integers for Component layers that the passed in board uses.
   * @return Bill Darbie
   * @check hasBottomBoardComponentLayer by Lim Boon Hong 
   */
  List<Integer> getComponentLayers(Board board)
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.add(new Integer(getTopComponentLayer(board)));
    if(hasBottomBoardComponentLayer(board))
      layers.add(new Integer(getBottomComponentLayer(board)));

    return layers;
  }

  /**
   * @return has top Pad layer used by the Board passed in
   * @author BH
   */
  private boolean hasTopBoardPadLayer(Board board)
  {
    return _boardToTopPadLayerMap.containsKey(board);
  }
  
  /**
   * @return the top Pad layer used by the board passed in
   * @author Bill Darbie
   */
  int getTopPadLayer(Board board)
  {
    Integer integer = (Integer)_boardToTopPadLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return has bottom Pad layer used by the Board passed in
   * @author Lim Boon Hong
   */
  private boolean hasBottomBoardPadLayer(Board board)
  {
    return _boardToBottomPadLayerMap.containsKey(board);
  }
  
  /**
   * @return the bottom Pad layer used by the board passed in
   * @author Bill Darbie
   */
  int getBottomPadLayer(Board board)
  {
    Integer integer = (Integer)_boardToBottomPadLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return a List of Integers of all the top Pad layers.
   * @author Bill Darbie
   */
  List<Integer> getTopPadLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToTopPadLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all the bottom Pad layers.
   * @author Bill Darbie
   */
  List<Integer> getBottomPadLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToBottomPadLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all Pad layers
   * @author Bill Darbie
   */
  List<Integer> getPadLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(getTopPadLayers());
    layers.addAll(getBottomPadLayers());

    return layers;
  }

  /**
   * @return the List of Integers for all Pad layers that the passed in board uses.
   * @return Bill Darbie
   * @check hasTopBoardPadLayer and hasBottomBoardPadLayer by Lim Boon Hong
   */
  List<Integer> getPadLayers(Board board)
  {
    List<Integer> layers = new ArrayList<Integer>();
    if (hasTopBoardPadLayer(board))
      layers.add(new Integer(getTopPadLayer(board)));
    if (hasBottomBoardPadLayer(board))
      layers.add(new Integer(getBottomPadLayer(board)));

    return layers;
  }

  /**
   * @return has top Ref Des layer used by the Board passed in
   * @author Lim Boon Hong
   */
  private boolean hasTopBoardRefDesLayer(Board board)
  {
    return _boardToTopRefDesLayerMap.containsKey(board);
  }
  
  /**
   * @return the top layer used by the Reference Designators for the board passed in
   * @author Bill Darbie
   */
  int getTopReferenceDesignatorLayer(Board board)
  {
    Integer integer = (Integer)_boardToTopRefDesLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }
  
  /**
   * @return has bottom Ref Des layer used by the Board passed in
   * @author Lim Boon Hong
   */
  private boolean hasBottomBoardRefDesLayer(Board board)
  {
    return _boardToBottomRefDesLayerMap.containsKey(board);
  }

  /**
   * @return the bottom layer used by the Reference Designators for the board passed in
   * @author Bill Darbie
   */
  int getBottomReferenceDesignatorLayer(Board board)
  {
    Integer integer = (Integer)_boardToBottomRefDesLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return a List of Integers of all the top Reference Designator layers.
   * @author Bill Darbie
   */
  List<Integer> getTopReferenceDesignatorLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToTopRefDesLayerMap.values());
    layers.add(new Integer(_topPanelFiducialRefDesLayer));
    return layers;
  }

  /**
   * @return a List of Integers of all the bottom Reference Designator layers.
   * @author Bill Darbie
   */
  List<Integer> getBottomReferenceDesignatorLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToBottomRefDesLayerMap.values());
    layers.add(new Integer(_bottomPanelFiducialRefDesLayer));
    return layers;
  }

  /**
   * @return a List of Integers of all Reference Designator layers
   * @author Bill Darbie
   */
  List<Integer> getReferenceDesignatorLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(getTopReferenceDesignatorLayers());
    layers.addAll(getBottomReferenceDesignatorLayers());

    return layers;
  }

  /**
   * @return the List of Integers for Reference Designator layers that the passed in board uses.
   * @return Bill Darbie
   * @check hasTopBoardRefDesLayer and hasBottomBoardRefDesLayer by Lim Boon Hong  
   */
  List<Integer> getReferenceDesignatorLayers(Board board)
  {
    List<Integer> layers = new ArrayList<Integer>();
    if(hasTopBoardRefDesLayer(board))
      layers.add(new Integer(getTopReferenceDesignatorLayer(board)));
    if(hasBottomBoardRefDesLayer(board))
      layers.add(new Integer(getBottomReferenceDesignatorLayer(board)));

    return layers;
  }

  /**
   * @return the top Fiducial layer used by the board passed in
   * @author Bill Darbie
   */
  int getTopBoardFiducialLayer(Board board)
  {
    Integer integer = (Integer)_boardToTopFiducialLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

   /**
   * @return has bottom fiducial layer used by the Board passed in
   * @author Lim Boon Hong
   */
  private boolean hasBottomBoardFiducialLayer(Board board)
  {
    return _boardToBottomFiducialLayerMap.containsKey(board);
  }
  
  /**
   * @return the bottom Fiducial layer used by the board passed in
   * @author Bill Darbie
   */
  int getBottomBoardFiducialLayer(Board board)
  {
    Integer integer = (Integer)_boardToBottomFiducialLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return a List of Integers of all the top Fiducial layers.
   * @author Bill Darbie
   */
  List<Integer> getTopBoardFiducialLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToTopFiducialLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all the bottom Fiducial layers.
   * @author Bill Darbie
   */
  List<Integer> getBottomBoardFiducialLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToBottomFiducialLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all Fiducial layers
   * @author Bill Darbie
   */
  List<Integer> getBoardFiducialLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(getTopBoardFiducialLayers());
    layers.addAll(getBottomBoardFiducialLayers());

    return layers;
  }

  /**
   * @return the List of Integers for all Fiducial layers that the passed in board uses.
   * @return Bill Darbie
   * @check hasBottomBoardFiducialLayer by Lim Boon Hong
   */
  List<Integer> getBoardFiducialLayers(Board board)
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.add(new Integer(getTopBoardFiducialLayer(board)));
    if(hasBottomBoardFiducialLayer(board))
    layers.add(new Integer(getBottomBoardFiducialLayer(board)));

    return layers;
  }

  /**
   * @return a List of Integers of Layers for Panel fiducials
   * @author Bill Darbie
   */
  List<Integer> getPanelFiducialLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(2);
    layers.add(new Integer(_topPanelFiducialLayer));
    layers.add(new Integer(_bottomPanelFiducialLayer));

    return layers;
  }

  /**
   * @return a List of Integers of Layers for Panel fiducials reference designators
   * @author Laura Cormos
   */
  List<Integer> getPanelFiducialRefDesLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(2);
    layers.add(new Integer(_topPanelFiducialRefDesLayer));
    layers.add(new Integer(_bottomPanelFiducialRefDesLayer));

    return layers;
  }

  /**
   * @return a List of Integers of both Panel and Board fiducial layers.
   * @author Bill Darbie
   */
  List<Integer> getFiducialLayers()
  {
    List<Integer> layers = getBoardFiducialLayers();
    layers.addAll(getPanelFiducialLayers());

    return layers;
  }
  
  /*
   * @return the layers used to display the inspection region rectangle
   * @author Kee Chin Seong
   */
  List<Integer> getEditFocusRegionInspectionRegionRectangleLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(new Integer(_bottomEditFocusRegionInspectionRegionRectangleLayer));
    layers.add(new Integer(_topEditFocusRegionInspectionRegionRectangleLayer));
 
    return layers;
  }

  /**
   * @return the layers used to display the inspection region rectangle
   * @author Andy Mechtenberg
   * @author George A. David
   */
  List<Integer> getInspectionRegionRectangleLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(new Integer(_topInspectionRegionRectangleLayer));
    layers.add(new Integer(_bottomInspectionRegionRectangleLayer));   

    return layers;
  }

  /**
   * @return the layers used to display the inspection region pad bounds
   * @author George A. David
   */
  List<Integer> getInspectionRegionPadBoundsLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(new Integer(_topInspectionRegionPadBoundsLayer));
    layers.add(new Integer(_bottomInspectionRegionPadBoundsLayer));

    return layers;
  }
  
    /**
   * @return the layers used to display the inspection focus regions
   * @author Kee Chin Seong
   */
  List<Integer> getEditFocusRegionInspectionFocusRegionLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(new Integer(_topEditFocusRegionInspectionFocusRegionLayer));
    layers.add(new Integer(_bottomEditFocusRegionInspectionFocusRegionLayer));
    
    return layers;
  }

  /**
   * @return the layers used to display the inspection focus regions
   * @author George A. David
   */
  List<Integer> getInspectionFocusRegionLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(new Integer(_topInspectionFocusRegionLayer));
    layers.add(new Integer(_bottomInspectionFocusRegionLayer));
    
    if(_isFocusRegionEditorOn == true)
    {
        layers.add(new Integer(_topEditFocusRegionInspectionFocusRegionLayer));
        layers.add(new Integer(_bottomEditFocusRegionInspectionFocusRegionLayer));
    }
    return layers;
  }

  /**
   * @return the layers used to display the alignment region rectangles
   * @author George A. David
   */
  List<Integer> getAlignmentRegionRectangleLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(new Integer(_topAlignmentRegionRectangleLayer));
    layers.add(new Integer(_bottomAlignmentRegionRectangleLayer));

    return layers;
  }

  /**
   * @return the layers used to display the alignment region pad bounds
   * @author George A. David
   */
  List<Integer> getAlignmentRegionPadBoundsLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(new Integer(_topAlignmentRegionPadBoundsLayer));
    layers.add(new Integer(_bottomAlignmentRegionPadBoundsLayer));

    return layers;
  }

  /**
   * @return the layers used to display the alignment focus regions
   * @author George A. David
   */
  List<Integer> getAlignmentFocusRegionLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(new Integer(_topAlignmentFocusRegionLayer));
    layers.add(new Integer(_bottomAlignmentFocusRegionLayer));

    return layers;
  }
  
   /**
   * @return the layers used to display the alignment focus regions
   * @author George A. David
   
  List<Integer> getOpticalRegionSelectedLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(new Integer(_opticalRegionSelectedLayer));
   
    return layers;
  } */
  
  /**
   * @return the layers used to display the verification region rectangles
   * @author George A. David
   */
  List<Integer> getVerificationRegionRectangleLayers()
  {
    List<Integer> layers = new LinkedList<Integer>();

    layers.add(_topVerificationRegionRectangleLayer);
    layers.add(_bottomVerificationRegionRectangleLayer);

    return layers;
  }

  /**
   * @return the layers used to display the verification focus regions
   * @author George A. David
   */
  List<Integer> getVerificationFocusRegionLayers()
  {
    List<Integer> layers = new LinkedList<Integer>();

    layers.add(_topVerificationFocusRegionLayer);
    layers.add(_bottomVerificationFocusRegionLayer);

    return layers;
  }

  /**
   * @author Andy Mechtenberg
   * @Edited By Kee Chin Seong
   */
  public void showTopSide()
  {
    // board name and origin renderers may or may not be visible.  Preserve that state here
    Collection<Integer> boardNameLayers = _boardToBoardNameLayerMap.values();
    boolean boardNamesVisible = false;
    for(Integer boardNameLayer : boardNameLayers)
    {
      if (_graphicsEngine.isLayerVisible(boardNameLayer))
        boardNamesVisible = true;
    }

    Collection<Integer> boardOriginLayers = _boardToOriginLayerMap.values();
    boolean boardOriginsVisible = false;
    for(Integer boardOriginLayer : boardOriginLayers)
    {
      if (_graphicsEngine.isLayerVisible(boardOriginLayer))
        boardOriginsVisible = true;
    }
    _graphicsEngine.setVisible(getTopLayers(), true);
    _graphicsEngine.setVisible(getBottomLayers(), false);

    if(_showPadName)
    {
      _graphicsEngine.setVisible(_topPadNameLayer, true);
      
      if(_showNoLoadComponent)
         _graphicsEngine.setVisible(_topNoLoadPadNameLayer, true);
    }
    
    //@author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
    if(_showNoLoadComponent)
    {
       _graphicsEngine.setVisible(_topNoLoadedComponentLayer, true);
       _graphicsEngine.setVisible(_bottomNoLoadedComponentLayer, false);
       _graphicsEngine.setVisible(_topNoLoadPadLayer, true);
       _graphicsEngine.setVisible(_bottomNoLoadPadLayer, false);
    }
    
    _graphicsEngine.setVisible(_bottomPadNameLayer, false);
    
    _graphicsEngine.setVisible(boardNameLayers, boardNamesVisible);
    _graphicsEngine.setVisible(boardOriginLayers, boardOriginsVisible);
    
    _showTopPadLayer = true;
    _showTopComponentLayer = true;
    _showTopComponentRefDesLayer = true;
    _showTopPanelLayer = true;
    
    //Ngie Xing
    _showBottomPadLayer = false;
    _showBottomComponentLayer = false;
    _showBottomComponentRefDesLayer = false;
    _showBottomPanelLayer = false;
    
    _graphicsEngine.setVisible(_topVirtualLiveVerificationImageLayer, true);
    _graphicsEngine.setVisible(_bottomVirtualLiveVerificationImageLayer, false);
    _graphicsEngine.setVisible(_bothVirtualLiveVerificationImageLayer, false);
  }

  /**
   * @author Andy Mechtenberg
   * @Edited By Kee Chin Seong
   */
  public void showBottomSide()
  {
    // board name and origin renderers may or may not be visible.  Preserve that state here
    Collection<Integer> boardNameLayers = _boardToBoardNameLayerMap.values();
    boolean boardNamesVisible = false;
    for(Integer boardNameLayer : boardNameLayers)
    {
      if (_graphicsEngine.isLayerVisible(boardNameLayer))
        boardNamesVisible = true;
    }

    Collection<Integer> boardOriginLayers = _boardToOriginLayerMap.values();
    boolean boardOriginsVisible = false;
    for(Integer boardOriginLayer : boardOriginLayers)
    {
      if (_graphicsEngine.isLayerVisible(boardOriginLayer))
        boardOriginsVisible = true;
    }
    _graphicsEngine.setVisible(getTopLayers(), false);
    _graphicsEngine.setVisible(getBottomLayers(), true);
    
    if(_showPadName)
    {
      _graphicsEngine.setVisible(_bottomPadNameLayer, true);
      if(_showNoLoadComponent)
         _graphicsEngine.setVisible(_bottomNoLoadPadNameLayer, true);
    }

    //@author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
    if(_showNoLoadComponent)
    {
        _graphicsEngine.setVisible(_topNoLoadPadLayer, false);
        _graphicsEngine.setVisible(_topNoLoadedComponentLayer, false);
        _graphicsEngine.setVisible(_bottomNoLoadPadLayer, true);
        _graphicsEngine.setVisible(_bottomNoLoadedComponentLayer, true);
    }
    
    _graphicsEngine.setVisible(_topPadNameLayer, false);
    
    _graphicsEngine.setVisible(boardNameLayers, boardNamesVisible);
    _graphicsEngine.setVisible(boardOriginLayers, boardOriginsVisible);
    
    _showBottomPadLayer = true;
    _showBottomComponentLayer = true;
    _showBottomComponentRefDesLayer = true;
    _showBottomPanelLayer = true;
    
    //Ngie Xing
    _showTopPadLayer = false;
    _showTopComponentLayer = false;
    _showTopComponentRefDesLayer = false;
    _showTopPanelLayer = false;
    
    _graphicsEngine.setVisible(_topVirtualLiveVerificationImageLayer, false);
    _graphicsEngine.setVisible(_bottomVirtualLiveVerificationImageLayer, true);
    _graphicsEngine.setVisible(_bothVirtualLiveVerificationImageLayer, false);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void showBothSides()
  {
    // board name and origin renderers may or may not be visible.  Preserve that state here
    Collection<Integer> boardNameLayers = _boardToBoardNameLayerMap.values();
    boolean boardNamesVisible = false;
    for(Integer boardNameLayer : boardNameLayers)
    {
      if (_graphicsEngine.isLayerVisible(boardNameLayer))
        boardNamesVisible = true;
    }

    Collection<Integer> boardOriginLayers = _boardToOriginLayerMap.values();
    boolean boardOriginsVisible = false;
    for(Integer boardOriginLayer : boardOriginLayers)
    {
      if (_graphicsEngine.isLayerVisible(boardOriginLayer))
        boardOriginsVisible = true;
    }
    _graphicsEngine.setVisible(getTopLayers(), true);
    _graphicsEngine.setVisible(getBottomLayers(), true);
    
    if(_showPadName)
    {
      _graphicsEngine.setVisible(_topPadNameLayer, true);
      _graphicsEngine.setVisible(_bottomPadNameLayer, true);
      _graphicsEngine.setVisible(_topNoLoadPadNameLayer, false);
      _graphicsEngine.setVisible(_bottomNoLoadPadNameLayer, false);
    }
    //@author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
    if(_showNoLoadComponent)
    {
      _graphicsEngine.setVisible(_topNoLoadPadLayer, true);
      _graphicsEngine.setVisible(_bottomNoLoadPadLayer, true);
      _graphicsEngine.setVisible(_topNoLoadedComponentLayer, true);
      _graphicsEngine.setVisible(_bottomNoLoadedComponentLayer, true);
    }

    _graphicsEngine.setVisible(boardNameLayers, boardNamesVisible);
    _graphicsEngine.setVisible(boardOriginLayers, boardOriginsVisible);
    
    _graphicsEngine.setVisible(_topVirtualLiveVerificationImageLayer, false);
    _graphicsEngine.setVisible(_bottomVirtualLiveVerificationImageLayer, false);
    _graphicsEngine.setVisible(_bothVirtualLiveVerificationImageLayer, true);
    
    //Ngie Xing
    _showTopComponentLayer = true;
    _showTopPadLayer = true;
    _showTopComponentRefDesLayer = true;
    _showTopPanelLayer = true;

    _showBottomComponentLayer = true;
    _showBottomPadLayer = true;
    _showBottomComponentRefDesLayer = true;
    _showBottomPanelLayer = true;
  }


  /**
   * Return true if any top layer is visisble, else false
   * @author Andy Mechtenberg
   */
  public boolean isTopSideVisible()
  {
    List<Integer> topLayers = getTopLayers();
    for (Integer layerNum : topLayers)
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true;
    }
    return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isBottomSideVisible()
  {
    for (Integer layerNum: getBottomLayers())
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true;
    }
    return false;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean isSelectedOpticalRegionLayerVisible()
  {
    if (_graphicsEngine.isLayerVisible(_opticalRegionSelectedLayer))
      return true;
    return false;
  }

  /**
   * Set the top layers to be visible by default
   * Bill Darbie
   */
  private void setVisibility()
  {
    Assert.expect(_graphicsEngine != null);
    _graphicsEngine.setVisible(getBottomLayers(), false);
    _graphicsEngine.setVisible(getTopLayers(), true);
    _graphicsEngine.setVisible(_alignmentRegionLayer, false);
    _graphicsEngine.setVisible(getBoardOriginLayers(), false);
    _graphicsEngine.setVisible(getBoardNameLayers(), false);
    _graphicsEngine.setVisible(getInspectionRegionRectangleLayers(), false);
    _graphicsEngine.setVisible(getInspectionRegionPadBoundsLayers(), false);
    _graphicsEngine.setVisible(getInspectionFocusRegionLayers(), false);
    _graphicsEngine.setVisible(getAlignmentRegionRectangleLayers(), false);
    _graphicsEngine.setVisible(getAlignmentRegionPadBoundsLayers(), false);
    _graphicsEngine.setVisible(getAlignmentFocusRegionLayers(), false);
    _graphicsEngine.setVisible(getVerificationRegionRectangleLayers(), false);
    _graphicsEngine.setVisible(getVerificationFocusRegionLayers(), false);
    _graphicsEngine.setVisible(_verificationIrpBoundariesLayer, false);
    _graphicsEngine.setVisible(_inspectionIrpBoundariesLayer, false);
    _graphicsEngine.setVisible(_topPadNameLayer, false);
    _graphicsEngine.setVisible(_bottomPadNameLayer, false);
    _graphicsEngine.setVisible(_virtualLiveAreaLayer, true);
    _graphicsEngine.setVisible(_topVirtualLiveVerificationImageLayer, true);
    _graphicsEngine.setVisible(_bottomVirtualLiveVerificationImageLayer, false);
    _graphicsEngine.setVisible(_bothVirtualLiveVerificationImageLayer, false);
    _graphicsEngine.setVisible(_topNoLoadPadNameLayer, false);
    _graphicsEngine.setVisible(_bottomNoLoadPadNameLayer, false);
    
    // Kok Chun, Tan
    // XCR-3383 - Region ID in CAD doesn't not refresh after change Region ID in Edit Focus Region
    _graphicsEngine.setVisible(getEditFocusRegionInspectionFocusRegionLayers(), false);
    _graphicsEngine.setVisible(getEditFocusRegionInspectionRegionRectangleLayers(), false);

	// Janan - XCR-3837: Display untestable Area on virtual live CAD
    _graphicsEngine.setVisible(_untestableAreaLayer, false);

  }

  /**
   * Set up the proper colors for the layers
   * Bill Darbie
   */
  private void setColors()
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.setForeground(getPadLayers(), LayerColorEnum.SURFACEMOUNT_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(getReferenceDesignatorLayers(), LayerColorEnum.REFERENCE_DESIGNATOR_COLOR.getColor());

    _graphicsEngine.setForeground(getTopComponentLayers(), LayerColorEnum.TOP_COMPONENT_COLOR.getColor());
    _graphicsEngine.setForeground(getBottomComponentLayers(), LayerColorEnum.BOTTOM_COMPONENT_COLOR.getColor());

    _graphicsEngine.setForeground(getBoardLayers(), LayerColorEnum.BOARD_COLOR.getColor());
    _graphicsEngine.setForeground(getPanelLayers(), LayerColorEnum.PANEL_COLOR.getColor());
    _graphicsEngine.setForeground(getFiducialLayers(), LayerColorEnum.FIDUCIAL_COLOR.getColor());

    _graphicsEngine.setForeground(getBoardOriginLayers(), LayerColorEnum.BOARD_ORIGIN_COLOR.getColor());
    _graphicsEngine.setForeground(getBoardNameLayers(), LayerColorEnum.BOARD_NAME_COLOR.getColor());

    _graphicsEngine.setForeground(getInspectionRegionRectangleLayers(), LayerColorEnum.RECONSTRUCTION_REGION_RECTANGLE_COLOR.getColor());
    _graphicsEngine.setForeground(getVerificationRegionRectangleLayers(), LayerColorEnum.RECONSTRUCTION_REGION_RECTANGLE_COLOR.getColor());
    _graphicsEngine.setForeground(getAlignmentRegionRectangleLayers(), LayerColorEnum.RECONSTRUCTION_REGION_RECTANGLE_COLOR.getColor());
    
    /*
     * @author Kee Chin Seong 
     */
    _graphicsEngine.setForeground(getEditFocusRegionInspectionRegionRectangleLayers(), LayerColorEnum.RECONSTRUCTION_REGION_RECTANGLE_COLOR.getColor());
    _graphicsEngine.setForeground(getEditFocusRegionInspectionFocusRegionLayers(), LayerColorEnum.FOCUS_REGION_COLOR.getColor());

    _graphicsEngine.setForeground(getInspectionRegionPadBoundsLayers(), LayerColorEnum.REGION_PAD_BOUNDS_COLOR.getColor());
    _graphicsEngine.setForeground(getAlignmentRegionPadBoundsLayers(), LayerColorEnum.REGION_PAD_BOUNDS_COLOR.getColor());

    _graphicsEngine.setForeground(getInspectionFocusRegionLayers(), LayerColorEnum.FOCUS_REGION_COLOR.getColor());
    _graphicsEngine.setForeground(getAlignmentFocusRegionLayers(), LayerColorEnum.FOCUS_REGION_COLOR.getColor());
    _graphicsEngine.setForeground(getVerificationFocusRegionLayers(), LayerColorEnum.FOCUS_REGION_COLOR.getColor());

    _graphicsEngine.setForeground(_inspectionIrpBoundariesLayer, LayerColorEnum.IRP_BOUNDARY_COLOR.getColor());
    _graphicsEngine.setForeground(_verificationIrpBoundariesLayer, LayerColorEnum.IRP_BOUNDARY_COLOR.getColor());

    _graphicsEngine.setForeground(_topPadNameLayer, LayerColorEnum.PAD_NAME_COLOR.getColor());
    _graphicsEngine.setForeground(_bottomPadNameLayer, LayerColorEnum.PAD_NAME_COLOR.getColor());
    _graphicsEngine.setForeground(_virtualLiveAreaLayer,LayerColorEnum.VIRTUALLIVE_AREA_BORDER_COLOR.getColor()); 
    _graphicsEngine.setForeground(_topVirtualLiveVerificationImageLayer, LayerColorEnum.PANEL_COLOR.getColor());
    _graphicsEngine.setForeground(_bottomVirtualLiveVerificationImageLayer, LayerColorEnum.PANEL_COLOR.getColor());
    _graphicsEngine.setForeground(_bothVirtualLiveVerificationImageLayer, LayerColorEnum.PANEL_COLOR.getColor());
    //@author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
    _graphicsEngine.setForeground(_topNoLoadedComponentLayer, LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(_bottomNoLoadedComponentLayer, LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(_topNoLoadPadLayer, LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(_bottomNoLoadPadLayer, LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(_panelWidthRendererLayer, LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    
    _graphicsEngine.setForeground(_topNoLoadPadNameLayer, LayerColorEnum.PAD_NAME_COLOR.getColor());
    _graphicsEngine.setForeground(_bottomNoLoadPadNameLayer, LayerColorEnum.PAD_NAME_COLOR.getColor());
    
    _graphicsEngine.setForeground(_topNoLoadRefDesignatorLayer, LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(_bottomNoLoadRefDesignatorLayer, LayerColorEnum.VERIFY_CAD_PAD_COLOR.getColor());
    
	//Janan XCR-3837: Display untestable Area on virtual live CAD
	_graphicsEngine.setForeground(_untestableAreaLayer, LayerColorEnum.UNTESTABLE_AREA_COLOR.getColor());

  }

  /**
   * Set up the threshold settings for each layer
   * Bill Darbie
   * @author By Kee Chin Seong - Renderer zoom in for RefDesingator must be updated when no load component is pressed.
   */
  private void setThresholds()
  {
    Assert.expect(_graphicsEngine != null);

    // since there are a lot of pads set a threshold for when they get created
    // only show reference designators at a certain threshold 25400 nm = 1 mil
    _graphicsEngine.setCreationThreshold(getPadLayers(), 25400 * 20, this);
    _graphicsEngine.setVisibilityThreshold(getReferenceDesignatorLayers(), 25400 * 30);

    _graphicsEngine.setVisibilityThreshold(_topNoLoadRefDesignatorLayer, 25400 * 30);
    _graphicsEngine.setVisibilityThreshold(_bottomNoLoadRefDesignatorLayer, 25400 * 30);
    _graphicsEngine.setVisibilityThreshold(_topNoLoadPadNameLayer, 25400 * 20);
    _graphicsEngine.setVisibilityThreshold(_bottomNoLoadPadNameLayer, 25400 * 20);
  }

  /**
   * Set up which layers to use when figuring out how big the whole deal is
   * Bill Darbie
   */
  private void setLayersToUseWhenFittingToScreen()
  {
    Collection<Integer> layers = new ArrayList<Integer>(2);
    layers.addAll(getPanelLayers());
    layers.addAll(getBoardLayers());

//    layers.addAll(getRegionLayers());

    Assert.expect(_graphicsEngine != null);
    _graphicsEngine.setLayersToUseWhenFittingToScreen(layers);
  }

  /**
   * Flip the panel so all top and bottom layers are swapped.
   * @author Bill Darbie
   */
  private void flipPanel()
  {
    Assert.expect(_panel != null);
    for (Board board: _panel.getBoards())
    {
      flipBoard(board);
    }

    // swap all renderers between top and bottom layers
    _graphicsEngine.swapLayers(_topPanelLayer, _bottomPanelLayer);
    _graphicsEngine.swapLayers(_topPanelFiducialLayer, _bottomPanelFiducialLayer);
    _graphicsEngine.swapLayers(_topPanelFiducialRefDesLayer, _bottomPanelFiducialRefDesLayer);

    // now call validateData()
    _graphicsEngine.validateData(getPanelLayers());
    _graphicsEngine.validateData(getPanelFiducialLayers());
  }

  /**
   * Flip a board so that top and bottom layers are swapped.
   * @author Bill Darbie
   */
  private void flipBoard(Board board)
  {
    Assert.expect(board != null);
    // now tell the engine to swap the layers
    _graphicsEngine.swapLayers(getTopBoardLayer(board), getBottomBoardLayer(board));
    _graphicsEngine.swapLayers(getTopPadLayer(board), getBottomPadLayer(board));
    _graphicsEngine.swapLayers(getTopComponentLayer(board), getBottomComponentLayer(board));
    _graphicsEngine.swapLayers(getTopReferenceDesignatorLayer(board), getBottomReferenceDesignatorLayer(board));
    _graphicsEngine.swapLayers(getTopBoardFiducialLayer(board), getBottomBoardFiducialLayer(board));

    // call validateData() on all layers affected
    _graphicsEngine.validateData(getAllLayers(board));
  }

  /**
   * @author Andy Mechtenberg
   */
  private void createBoard(Board board)
  {
    Assert.expect(board != null);

    // first, create the layers needed by the board
    _boardToTopBoardLayerMap.put(board, new Integer(++_layerNumber));
    _boardToBottomBoardLayerMap.put(board, new Integer(++_layerNumber));

    _boardToTopPadLayerMap.put(board, new Integer(++_layerNumber));
    _boardToBottomPadLayerMap.put(board, new Integer(++_layerNumber));

    _boardToTopComponentLayerMap.put(board, new Integer(++_layerNumber));
    _boardToBottomComponentLayerMap.put(board, new Integer(++_layerNumber));

    _boardToTopFiducialLayerMap.put(board, new Integer(++_layerNumber));
    _boardToBottomFiducialLayerMap.put(board, new Integer(++_layerNumber));

    _boardToTopRefDesLayerMap.put(board, new Integer(++_layerNumber));
    _boardToBottomRefDesLayerMap.put(board, new Integer(++_layerNumber));

    _boardToOriginLayerMap.put(board, new Integer(++_layerNumber));
    _boardToBoardNameLayerMap.put(board, new Integer(++_layerNumber));

    // now that the board layers are created (and put in the correct maps), we need to populate them
    populateBoardLayers(board);
    setColors();
    setVisibility();
    setThresholds();
    setLayersToUseWhenFittingToScreen();

    // we may be zoomed in enough to show pads or other conditional renderers, so this call will make that happen
    _graphicsEngine.validateData(getPadLayers());
  }

  /**
   * Removes all renderers associated with the passed in board instance object
   * @author Andy Mechtenberg
   * @author Kee Chin Seong 
   * Remove the board renderer handling
   */
  private void removeBoard(Board board)
  {
    Assert.expect(board != null);
    List<Integer> allLayers = getAllLayers(board);
    for(Integer layerNumber : allLayers)
    {
      _graphicsEngine.removeLayer(layerNumber.intValue());
    }

    _boardToTopBoardLayerMap.remove(board);
    _boardToBottomBoardLayerMap.remove(board);

    _boardToTopPadLayerMap.remove(board);
    _boardToBottomPadLayerMap.remove(board);

    _boardToTopComponentLayerMap.remove(board);
    _boardToBottomComponentLayerMap.remove(board);

    _boardToTopFiducialLayerMap.remove(board);
    _boardToBottomFiducialLayerMap.remove(board);

    _boardToTopRefDesLayerMap.remove(board);
    _boardToBottomRefDesLayerMap.remove(board);

    _boardToOriginLayerMap.remove(board);
    _boardToBoardNameLayerMap.remove(board);

    // remove all component renderers associated with the deleted/removed board
    for (ComponentType componentType : board.getBoardType().getComponentTypes())
    {
      List<ComponentRenderer> componentRenderers = _componentTypeToComponentRenderersMap.get(componentType);
      List<ComponentRenderer> componentRenderersToDelete = new ArrayList<ComponentRenderer>();
      if(componentRenderers != null)
      {
        for (ComponentRenderer componentRenderer : componentRenderers)
        {
          // build the list of component renderers to remove. Choose only those whose components belong to the
          // board being removed
          if (componentRenderer.getComponent().getBoard() == board)
            componentRenderersToDelete.add(componentRenderer);
        }
        componentRenderers.removeAll(componentRenderersToDelete);
        
        // replace the list of component renderers with the reduced list
        _componentTypeToComponentRenderersMap.put(componentType, componentRenderers);
      }
    }

    // remove all component reference designators renderers associated with the deleted/removed board
    for (ComponentType componentType : board.getBoardType().getComponentTypes())
    {
      List<ReferenceDesignatorRenderer> refDesRenderers = _componentTypeToReferenceDesignatorRenderersMap.get(componentType);
      List<ReferenceDesignatorRenderer> refDesRenderersToDelete = new ArrayList<ReferenceDesignatorRenderer>();
      
      if(refDesRenderers != null)
      {
        for (ReferenceDesignatorRenderer refDesRenderer : refDesRenderers)
        {
          // build the list of refDes renderers to remove. Choose only those whose components belong to the
          // board being removed
          if (refDesRenderer.getComponent().getBoard() == board)
            refDesRenderersToDelete.add(refDesRenderer);
        }
        refDesRenderers.removeAll(refDesRenderersToDelete);
        
        // replace the list of refDes renderers with the reduced list
        _componentTypeToReferenceDesignatorRenderersMap.put(componentType, refDesRenderers);
      } 
    }
  }

  /**
  * @author KEe Chin Seong - you dont have to check the layernumber as the Normal UpdateRendere, because this is a "special"
  *                          Handle for no load PAD and PadName
  */
  public void updateNoLoadRenderers(GraphicsEngine graphicsEngine, int layerNumber)
  {
    if(_showNoLoadComponent == false)
      return;
    
    Assert.expect(graphicsEngine != null);
    Assert.expect(graphicsEngine == _graphicsEngine);
    
    Collection<com.axi.guiUtil.Renderer> padRenderers = null;
    if(isTopSideVisible())
       padRenderers = graphicsEngine.getRenderers(_topNoLoadPadLayer);
    else
       padRenderers = graphicsEngine.getRenderers(_bottomNoLoadPadLayer);
    
    // get the current list of component renderers
    Collection<com.axi.guiUtil.Renderer> componentRenderers = null;
    if (isTopSideVisible())
      componentRenderers = graphicsEngine.getRenderers(_topNoLoadedComponentLayer);
    else
      componentRenderers = graphicsEngine.getRenderers(_bottomNoLoadedComponentLayer);
    
    Set<ComponentRenderer> componentRenderersShowingPadsSet = new HashSet<ComponentRenderer>();
    
    if (graphicsEngine.isZoomLevelBelowCreationThreshold(layerNumber))
    {
      // remove pad name layer
      graphicsEngine.removeRenderers(_topNoLoadPadLayer);
      graphicsEngine.removeRenderers(_bottomNoLoadPadLayer);
    }
    else // this is a 'regular' renderer update call whenever the zoom level is above pad renderer creation threshold
    {
      // iterate over each component to see if it is visible or not
      for (com.axi.guiUtil.Renderer renderer : componentRenderers)
      {
        if(renderer instanceof ComponentRenderer)
        {
            ComponentRenderer componentRenderer = (ComponentRenderer)renderer;
            boolean isVisible = graphicsEngine.wouldRendererBeVisibleOnScreenIfLayerWereVisible(componentRenderer);
            if (isVisible)
            {
              // the component is visible, create its PadRenderers in memory if they do not already
              // exist and add them to the GraphicsEngine
              if (componentRenderersShowingPadsSet.contains(componentRenderer) == false)
              {
                componentRenderersShowingPadsSet.add(componentRenderer);
                Component component = componentRenderer.getComponent();
                boolean componentIsTopSide = component.getSideBoard().isTopSide();

                // first do top side pads for the component
                Collection<Pad> pads = component.getPads();
                java.util.List<PadRenderer> padRenderersList = new ArrayList<PadRenderer>();
                java.util.List<PadNameRenderer> padNameRenderersList = new ArrayList<PadNameRenderer>();
                for (Pad pad : pads)
                {
                  PadRenderer padRenderer = new PadRenderer(pad, componentRenderer);
                  PadNameRenderer padNameRenderer = new PadNameRenderer(pad, false, false);
                  padRenderersList.add(padRenderer);
                  padNameRenderersList.add(padNameRenderer);
                  
                  if(isTopSideVisible())
                     graphicsEngine.addRenderer(_topNoLoadPadLayer, padRenderer);
                  else
                     graphicsEngine.addRenderer(_bottomNoLoadPadLayer, padRenderer); 
                  
                  if(isTopSideVisible())
                    graphicsEngine.addRenderer(_topNoLoadPadNameLayer, padNameRenderer);
                  else
                    graphicsEngine.addRenderer(_bottomNoLoadPadNameLayer, padNameRenderer);
                  
                  _noLoadPadToPadRendererMap.put(pad, padRenderer);

                  if (pad.isThroughHolePad())
                  {
                    // the pad is through hole, so its pads should show up on both sides
                    padRenderer = new PadRenderer(pad, componentRenderer);
                    padNameRenderer = new PadNameRenderer(pad, false, false);
                    
                    padRenderersList.add(padRenderer);
                    padNameRenderersList.add(padNameRenderer);
                    
                    if (componentIsTopSide)
                      graphicsEngine.addRenderer(_topNoLoadPadLayer, padRenderer);
                    else
                      graphicsEngine.addRenderer(_bottomNoLoadPadLayer, padRenderer);
                  }
                }

                noLoadComponentRendererToPadRenderersMapPut(componentRenderer, padRenderersList);
                noLoadComponentRendererToPadRendererNamesMapPut(componentRenderer, padNameRenderersList);
              }
            }
            else
            {
              if (componentRenderersShowingPadsSet.contains(componentRenderer))
              {
                componentRenderersShowingPadsSet.remove(componentRenderer);
                java.util.List<PadRenderer> padRenderersList = _noLoadComponentRendererToPadRenderersMap.get(componentRenderer);
                if (padRenderersList != null)
                {
                  noLoadComponentRendererToPadRenderersMapRemove(componentRenderer);
                  noLoadComponentRendererToPadRendererNamesMapRemove(componentRenderer);

                  for (PadRenderer padRenderer : padRenderersList)
                  {
                    graphicsEngine.removeRenderer(padRenderer);
                    _noLoadPadToPadRendererMap.remove(padRenderer.getPad());
                  }
                }
              }
            }
          }
      }
    }
    
    graphicsEngine.validateData(_topNoLoadRefDesignatorLayer);
    graphicsEngine.validateData(_bottomNoLoadRefDesignatorLayer);
  }
  /**
   * This call will create PadRenderers for any Components that are currently visible
   * on the screen and add them to the GraphicsEngine.
   * Do not let user see the pad layers when been disabled, XXL version
   * @author Bill Darbie
   * @author Kee Chin Seong
   */
  public void updateRenderers(GraphicsEngine graphicsEngine, int layerNumber)
  {
    if(getTopPadLayers().contains(new Integer(layerNumber)) && _showTopPadLayer == false)
      return;
    else 
      if(getBottomPadLayers().contains(new Integer(layerNumber)) && _showBottomPadLayer == false)
        return;
    
    if(_graphicsEngine.getCurrentZoomFactor() >= 2.0 )
    {
      if(_showNoLoadComponent == true)
      {
         if(isTopSideVisible())
            _graphicsEngine.setVisible(_topNoLoadRefDesignatorLayer, true);
         else
            _graphicsEngine.setVisible(_bottomNoLoadRefDesignatorLayer, true);
      }
       
    }
    else
    {
       _graphicsEngine.setVisible(_topNoLoadRefDesignatorLayer, false);
       _graphicsEngine.setVisible(_bottomNoLoadRefDesignatorLayer, false);  
    }
    
    Assert.expect(graphicsEngine != null);
    Assert.expect(graphicsEngine == _graphicsEngine);

    Integer layerNumberInteger = new Integer(layerNumber);
    List<Integer> padLayers = getPadLayers();
    if (padLayers.contains(layerNumberInteger))
    {
//      System.out.println("Updating Renderers, graphics engine contents:");
//      printRenderers();
      boolean topLayer = false;
      List<Integer> topPadLayers = getTopPadLayers();
      if (topPadLayers.contains(layerNumberInteger))
        topLayer = true;

      Set<ComponentRenderer> componentRenderersShowingPadsSet = new HashSet<ComponentRenderer>();

      // figure out the current list of Components that already have PadRenderers
      Collection<com.axi.guiUtil.Renderer> padRenderers = null;
      if (topLayer)
        padRenderers = graphicsEngine.getRenderers(getTopPadLayers());
      else
        padRenderers = graphicsEngine.getRenderers(getBottomPadLayers());

      for (com.axi.guiUtil.Renderer padRenderer : padRenderers)
      {
        ComponentRenderer componentRenderer = ((PadRenderer)padRenderer).getComponentRenderer();
        componentRenderersShowingPadsSet.add(componentRenderer);
      }

      // get the current list of component renderers
      Collection<com.axi.guiUtil.Renderer> componentRenderers = null;
      if (topLayer)
        componentRenderers = graphicsEngine.getRenderers(getTopComponentLayers());
      else
        componentRenderers = graphicsEngine.getRenderers(getBottomComponentLayers());

      if (graphicsEngine.isZoomLevelBelowCreationThreshold(layerNumber))
      {
        // if we crossed the pad creation threshold, then remove all entries from _componentRendererToPadRenderersMap
        // for all components who currently have pads on the passed in layer. The graphics engine whould have taken care
        // of removing the actual pad renderers from its layer
        for (ComponentRenderer componentRendererWithPads : componentRenderersShowingPadsSet)
        {
          componentRendererToPadRenderersMapRemove(componentRendererWithPads);
        }
        // remove pad name layer
        graphicsEngine.removeRenderers(_topPadNameLayer);
        graphicsEngine.removeRenderers(_bottomPadNameLayer);
      }
      else // this is a 'regular' renderer update call whenever the zoom level is above pad renderer creation threshold
      {
        // iterate over each component to see if it is visible or not
        for (com.axi.guiUtil.Renderer renderer : componentRenderers)
        {
          ComponentRenderer componentRenderer = (ComponentRenderer)renderer;
          //System.out.println("UR:Processing component renderer for: " + componentRenderer.getComponent().getReferenceDesignator());
          boolean isVisible = graphicsEngine.wouldRendererBeVisibleOnScreenIfLayerWereVisible(componentRenderer);
          if (isVisible)
          {
            //System.out.println("UR:The Component is visible");
            // the component is visible, create its PadRenderers in memory if they do not already
            // exist and add them to the GraphicsEngine
            if (componentRenderersShowingPadsSet.contains(componentRenderer) == false)
            {
              //System.out.println("UR:The Component is NOT part of set of compRenderers showing pads - create its pads");
              componentRenderersShowingPadsSet.add(componentRenderer);
              Component component = componentRenderer.getComponent();
              boolean componentIsTopSide = component.getSideBoard().isTopSide();

              // first do top side pads for the component
              Collection<Pad> pads = component.getPads();
              java.util.List<PadRenderer> padRenderersList = new ArrayList<PadRenderer>();
              for (Pad pad : pads)
              {
                Integer topPadLayer = _boardToTopPadLayerMap.get(pad.getComponent().getBoard());
                Integer bottomPadLayer = _boardToBottomPadLayerMap.get(pad.getComponent().getBoard());
                // we want to make sure we put the pad on proper board's layer.  For multi-board panels,
                // each board has it's own top and bottom pad layer.  Without this check, we'd put all the pads
                // for each board on one layer.
                if ((layerNumber == topPadLayer.intValue()) || layerNumber == bottomPadLayer.intValue())
                {
                  PadRenderer padRenderer = new PadRenderer(pad, componentRenderer);
                  PadNameRenderer padNameRenderer = new PadNameRenderer(pad, false, false);
                  padRenderersList.add(padRenderer);

                  //if(layerNumber == topPadLayer.intValue())
                 //   graphicsEngine.addRenderer(topPadLayer.intValue(), padRenderer);
                 //else
                  //  graphicsEngine.addRenderer(bottomPadLayer.intValue(), padRenderer);
                  
                  graphicsEngine.addRenderer(layerNumber, padRenderer);
                  if(layerNumber == topPadLayer.intValue())
                    graphicsEngine.addRenderer(_topPadNameLayer, padNameRenderer);
                  else
                    graphicsEngine.addRenderer(_bottomPadNameLayer, padNameRenderer);
                  _padToPadRendererMap.put(pad, padRenderer);

                  if (pad.isThroughHolePad())
                  {
                    // the pad is through hole, so its pads should show up on both sides
                    padRenderer = new PadRenderer(pad, componentRenderer);
                    padRenderersList.add(padRenderer);

                    // put the pad on the correct layer - opposite of what layer the
                    // component is on since it is a bottom side Pad
                    Board board = component.getSideBoard().getBoard();
                    if (componentIsTopSide)
                      graphicsEngine.addRenderer(getBottomPadLayer(board), padRenderer);
                    else
                      graphicsEngine.addRenderer(getTopPadLayer(board), padRenderer);
                  }
                }
              }

              // System.out.println("wpd padRenderersList: " + padRenderersList);
              componentRendererToPadRenderersMapPut(componentRenderer, padRenderersList);
            }
          }
          else
          {
            //System.out.println("UR:The Component is NOT visible");
            // component is not visible.  If its PadRenderers are currently in the GraphicsEngine
            // remove them.
            if (componentRenderersShowingPadsSet.contains(componentRenderer))
            {
              //System.out.println("UR:The Component is part of set of compRenderers showing pads - remove pads if they exist");
              componentRenderersShowingPadsSet.remove(componentRenderer);
              java.util.List<PadRenderer> padRenderersList = _componentRendererToPadRenderersMap.get(componentRenderer);
              if (padRenderersList != null)
              {
                //System.out.println("UR:The Component has pads - remove them");
                componentRendererToPadRenderersMapRemove(componentRenderer);

                for (PadRenderer padRenderer : padRenderersList)
                {
                  graphicsEngine.removeRenderer(padRenderer);
                  _padToPadRendererMap.remove(padRenderer.getPad());
                }
              }
            }
          }
        }
      }
    }
    else
    {
      // the layer passed in is not valid
      Assert.expect(false, "Pad Layer " + layerNumberInteger + " does not exist");
    }

//    System.out.println("Finished Updating Renderers, graphics engine contents:");
//    printRenderers();
    if(_showNoLoadComponent == true)
       updateNoLoadRenderers(graphicsEngine, layerNumber);
    
    highlightPads(_highlightPads);
  }

  /**
   * Checks to see if the pad layers (for either top side or bottom side) are currently visible.
   * This depends on the creation threshold and whether the user has selected to view top or bottom side.
   * @author Andy Mechtenberg
   */
  boolean arePadsVisible(boolean topSide)
  {
    List<Integer> padLayers;

    if (topSide)
     padLayers = getTopPadLayers();
    else
      padLayers = getBottomPadLayers();

    for (Integer layerNum : padLayers)
    {
      boolean padsVisible = _graphicsEngine.isLayerVisible(layerNum.intValue());
      if (padsVisible)
        return true;
    }

    return false;
  }

  /**
   * This class is an observer of the datastore layer. Any changes made
   * to the datastore will trigger an update call. We will look at the
   * change event and determine if anything needs to be updated.
   * It's also an observer of the GUI screens and renderers selected in the graphics engine
   * @author George A. David
   * @edited By Kee Chin Seong - Renderer zoom in for RefDesingator must be updated when no load component is pressed.
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProjectObservable)
        {
          if (argument instanceof ProjectChangeEvent)
          {
            final ProjectChangeEvent event = (ProjectChangeEvent)argument;
            ProjectChangeEventEnum eventEnum = ((ProjectChangeEvent)argument).getProjectChangeEventEnum();

            if (eventEnum instanceof AlignmentGroupEventEnum)
            {
              updateAlignmentRegionData();
            }
            else if (eventEnum instanceof BoardEventEnum)
            {
              updateBoardData(event);
            }
            else if (eventEnum instanceof BoardTypeEventEnum)
            {
              updateBoardTypeData(event);
            }
            else if (eventEnum instanceof ComponentEventEnum)
            {
//              updateComponentData(event);
            }
            else if (eventEnum instanceof ComponentTypeEventEnum)
            {
              ComponentTypeEventEnum compTypeEventEnum = (ComponentTypeEventEnum)eventEnum;
              if (compTypeEventEnum.equals(ComponentTypeEventEnum.COORDINATE))
                MainMenuGui.getInstance().getTestDev().updateProgressDialog();
              updateComponentTypeData(event);
            }
            else if(eventEnum instanceof ComponentTypeSettingsEventEnum)
            {
              updateComponentTypeLoadedData(event, argument);
            }
            else if (eventEnum instanceof CompPackageEventEnum)
            {
              // do nothing -- panel graphics show land patterns, not packages
            }
            else if (eventEnum instanceof FiducialEventEnum)
            {
               updateFiducialData(event);
            }
            else if (eventEnum instanceof FiducialTypeEventEnum)
            {
              updateFiducialTypeData(event);
            }
            else if (eventEnum instanceof LandPatternPadEventEnum)
            {
              updateLandPatternPadData(event);
            }
            else if (eventEnum instanceof LandPatternEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof PackagePinEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof PadEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof PanelEventEnum)
            {
              // bee-hoon.goh - Prevent crash when create dummy board from TestDev environment
              if(Project.isUsingVirtualDummyProject() == false) 
                updatePanelData(event);
            }
            else if (eventEnum instanceof PanelSettingsEventEnum)
            {
              // bee-hoon.goh - Prevent crash when create dummy board from TestDev environment
              if(Project.isUsingVirtualDummyProject() == false)
                updatePanelSettingsData(event);
            }
            else if (eventEnum instanceof ProjectEventEnum)
            {
              updateProjectData(event);
            }
            else if (eventEnum instanceof SideBoardEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof SideBoardTypeEventEnum)
            {
              updateSideBoardTypeData(event);
            }
            else if (eventEnum instanceof SideBoardTypeSettingsEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof ThroughHoleLandPatternPadEventEnum)
            {
              updateThroughHoleLandPatternPadData(event);
            }
            // commmented out.  This was used to implement graphical undo, but we decided
            // not to support it.  It was causing a deadlock with other updates in some situations
            // _guiObservable.stateChanged(this, GuiUpdateEventEnum.CAD_GRAPHICS);
          }
        }
        else if (observable instanceof SelectedRendererObservable)
        {
          handleSelectedRenderers(argument);
        }
        else if (observable instanceof GuiObservable)
        {
          handleGuiEvent((GuiEvent)argument);
        }
        else if (observable instanceof SelectedOpticalRegionObservable)
        {
          //handleSelectedOpticalCameraRectangle();
        }
        else if (observable instanceof UnselectedOpticalRegionObservable)
        {
          //handleUnselectedOpticalRegion();
        }        
        else if (observable instanceof UnselectedDragOpticalRegionObservable)
        {
          //handleUnselectedOpticalRegion();
        }
        else if (observable instanceof DragOpticalRegionObservable)
        {
          handleDragOpticalRegion();
        }
        else if (observable instanceof DisplayPadNameObservable)
        {
          if (argument instanceof Boolean)
          {
            Boolean setPadNameVisible = (Boolean)argument;
            if (setPadNameVisible.booleanValue())
            {
              _showPadName = true;
              if(isTopSideVisible())
              {
                _graphicsEngine.setVisible(_topPadNameLayer, true);
                if(_showNoLoadComponent)
                  _graphicsEngine.setVisible(_topNoLoadPadNameLayer, true);
                else
                  _graphicsEngine.setVisible(_topNoLoadPadNameLayer, false);
              }
              if(isBottomSideVisible())
              {
                _graphicsEngine.setVisible(_bottomPadNameLayer, true);
                if(_showNoLoadComponent)
                  _graphicsEngine.setVisible(_bottomNoLoadPadNameLayer, true);
                else
                  _graphicsEngine.setVisible(_bottomNoLoadPadNameLayer, false);
              }
            } 
            else
            {
              _showPadName = false;
              _graphicsEngine.setVisible(_topNoLoadPadNameLayer, false);
              _graphicsEngine.setVisible(_bottomNoLoadPadNameLayer, false);
              _graphicsEngine.setVisible(_topPadNameLayer, false);
              _graphicsEngine.setVisible(_bottomPadNameLayer, false);
            }
          }
        }
        else if (observable instanceof ShowNoLoadComponentObservable)
        {
          if (argument instanceof Boolean)
          {
            Boolean setNoLoadComponentVisible = (Boolean)argument;
            if (setNoLoadComponentVisible.booleanValue())
            {
              _showNoLoadComponent = true;
              if(isTopSideVisible())
              {
                _graphicsEngine.setVisible(_topNoLoadedComponentLayer, true);
                _graphicsEngine.setVisible(_topNoLoadPadLayer, true);
                _graphicsEngine.setVisible(_topNoLoadRefDesignatorLayer, true);
              }
              
              if(isBottomSideVisible())
              {
                _graphicsEngine.setVisible(_bottomNoLoadedComponentLayer, true);
                _graphicsEngine.setVisible(_bottomNoLoadPadLayer, true);
                _graphicsEngine.setVisible(_bottomNoLoadRefDesignatorLayer, true);
              }
              
            } 
            else
            {
              _showNoLoadComponent = false;
              _graphicsEngine.setVisible(_topNoLoadedComponentLayer, false);
              _graphicsEngine.setVisible(_bottomNoLoadedComponentLayer, false);
              _graphicsEngine.setVisible(_topNoLoadPadLayer, false);
              _graphicsEngine.setVisible(_bottomNoLoadPadLayer, false);
              //always force the renderer close 
              _graphicsEngine.setVisible(_bottomNoLoadRefDesignatorLayer, false);
              _graphicsEngine.setVisible(_bottomNoLoadPadNameLayer, false);
              _graphicsEngine.setVisible(_topNoLoadRefDesignatorLayer, false);
              _graphicsEngine.setVisible(_topNoLoadPadNameLayer, false);
            }
          }
        }
        else if (observable instanceof VirtualLiveImageGenerationSettings)
        {
          handleVirtualLiveImageGenerationDataObservable(argument);
        }
        else if (observable instanceof SelectedAreaObservable)
        {
          if(argument instanceof Shape)
          {
            if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment())
            {
              handleSelectedAreaObservable((Shape)argument);
            }
            else
            {
              handleFocusRegionSelectedArea((Shape)argument);
            }
          }
        }
        else if (observable instanceof ShowUntestableAreaObservable)
        {
          if (argument instanceof Boolean)
          {
            Boolean setUntestableAreaVisible = (Boolean)argument;
            _graphicsEngine.setVisible(_untestableAreaLayer, setUntestableAreaVisible.booleanValue());
          }
        }
        else
        {
          Assert.expect(false, "PanelGraphicsEngineSetup: No update code for observable: " + observable.getClass().getName());
        }
      }
    });
  }
  
   /**
   * @author Jack Hwee
   * Draw a blue region to indicates the optical region selected in combo box
   */
  public void handleSelectedOpticalCameraRectangleWithGivenRegion(String regionName)
  {
    _graphicsEngine.removeLayer(_opticalRegionUnSelectedLayer);
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_opticalRegionSelectedLayer);
    
    OpticalRegion opticalRegion = getSelectedOpticalRegion(regionName);
    
    if (opticalRegion != null)
    {
      for (OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getAllOpticalCameraRectangles())
      {
        addSelectedOpticalCameraRectangle(opticalCameraRectangle);
        _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
        _selectedRectangleRenderer = new SelectedOpticalRectangleRenderer(opticalCameraRectangle, opticalCameraRectangle.getInspectableBool());
        _graphicsEngine.addRenderer(_opticalRegionSelectedLayer, _selectedRectangleRenderer);
        _graphicsEngine.setVisible(_opticalRegionSelectedLayer, true);
        _graphicsEngine.repaint();
      }
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   * Draw a blue region to indicates the optical region selected in combo box
   */
  public void handleMultipleSelectedOpticalCameraRectangles(java.util.List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    _graphicsEngine.removeLayer(_opticalRegionUnSelectedLayer);
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_opticalRegionSelectedLayer);
    _graphicsEngine.removeLayer(_highlightOpticalRegionLayer);
    
    _selectedOpticalCameraRectangleList = opticalCameraRectangles;
    
    if (opticalCameraRectangles != null)
    {
      for(OpticalCameraRectangle opticalCameraRectangle : _selectedOpticalCameraRectangleList)
      {
        _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
        _selectedRectangleRenderer = new SelectedOpticalRectangleRenderer(opticalCameraRectangle, opticalCameraRectangle.getInspectableBool());
        _graphicsEngine.addRenderer(_opticalRegionSelectedLayer, _selectedRectangleRenderer);
        _graphicsEngine.setVisible(_opticalRegionSelectedLayer, true);
        _graphicsEngine.repaint();
      }
    }
  }
  
  /**
   * @author Jack Hwee
   * Draw a yellow region to indicates the optical region selected in combo box
   */
  public void handleHighlightOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_highlightOpticalRegionLayer);

    if (opticalCameraRectangle != null)
    {
      _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
      _highlightRectangleRenderer = new HighlightOpticalRectangleRenderer(opticalCameraRectangle);
      _graphicsEngine.addRenderer(_highlightOpticalRegionLayer, _highlightRectangleRenderer);
      _graphicsEngine.setVisible(_highlightOpticalRegionLayer, true);
      _graphicsEngine.repaint();
    }
  }
  
   /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   * Draw a yellow region to indicates the optical region selected in combo box
   */
  public void handleHighlightOpticalRegionWhenLiveView(OpticalCameraRectangle opticalCameraRectangle)
  {
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_highlightOpticalRegionLayer);

    if (opticalCameraRectangle != null)
    {
      _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
      _highlightRectangleRenderer = new HighlightOpticalRectangleRenderer(opticalCameraRectangle);
      _graphicsEngine.addRenderer(_highlightOpticalRegionLayer, _highlightRectangleRenderer);
      _graphicsEngine.setVisible(_highlightOpticalRegionLayer, true);
      _graphicsEngine.repaint();
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   * Draw multiple yellow region to indicates the optical region selected in combo box
   */
  public void handleMultipleHighlightOpticalCameraRectangles(java.util.List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_highlightOpticalRegionLayer);
    
    if (opticalCameraRectangles != null)
    {
      for(OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangles)
      {
        _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
        _highlightRectangleRenderer = new HighlightOpticalRectangleRenderer(opticalCameraRectangle);
        _graphicsEngine.addRenderer(_highlightOpticalRegionLayer, _highlightRectangleRenderer);
        _graphicsEngine.setVisible(_highlightOpticalRegionLayer, true);
        _graphicsEngine.repaint();                
      }
    }
  }
  
   /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   * Draw a red region to indicates the optical region selected by user in CAD
   */
  public void handleDragOpticalRegion()
  { 
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_dragOpticalRegionUnSelectedLayer);
    
    for (OpticalCameraRectangle ocr : getDividedOpticalRegionList())
    {
      if (ocr != null && ocr.getInspectableBool() == false) // fill grey rectangle
      {
        _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
        _dragOpticalRectangleRenderer = new DragOpticalRectangleRenderer(ocr);
        _graphicsEngine.addRenderer(_dragOpticalRegionUnSelectedLayer, _dragOpticalRectangleRenderer);
        _graphicsEngine.setVisible(_dragOpticalRegionUnSelectedLayer, true);
        _graphicsEngine.repaint();
      }
      else if (ocr != null && ocr.getInspectableBool() == true) // draw red rectangle
      {
        _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
        _dragOpticalRectangleRenderer = new DragOpticalRectangleRenderer(ocr);
        _graphicsEngine.addRenderer(_dragOpticalRegionUnSelectedLayer, _dragOpticalRectangleRenderer);
        _graphicsEngine.setVisible(_dragOpticalRegionUnSelectedLayer, true);
        _graphicsEngine.repaint();
      }
    }
  }
  
   /**
   * @author Jack Hwee
   * Draw a red region to indicates the optical region selected by user in CAD
   */
  public void handleDragOpticalRegionInAlignment()
  {
//    _graphicsEngine.removeLayer(_dragAlignmentOpticalRegionLayer);
//   
//    for (java.util.Map.Entry<Rectangle, String> map : getAlignmentDividedOpticalRegionList().entrySet())
//    {
//      Rectangle rectangle = map.getKey();
//        
//      Rectangle rectInPixels = rectangle;
//
//      Rectangle rectInNano = _graphicsEngine.calculateSelectedComponentRegionInNanoMeter(rectInPixels);
//
//      _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
//      _dragOpticalRectangleRenderer = new DragOpticalRectangleRenderer();
//      _dragOpticalRectangleRenderer.setRegion(rectInNano.getBounds2D());
//      _graphicsEngine.addRenderer(_dragAlignmentOpticalRegionLayer, _dragOpticalRectangleRenderer);
//      _graphicsEngine.setVisible(_dragAlignmentOpticalRegionLayer, true);
//
//      _graphicsEngine.repaint();
//    }
  }
  
  
  /**
   * This method will take a collection of Renderers that have been selected in the GraphicsEngine and
   * determine which to highlight in the graphics.
   * @author Andy Mechtenberg
   */
  private void handleSelectedRenderers(final Object argument)
  {
    // notify all the PanelGraphicsEngineSetup observers that a mouse click through the Panel Graphics generated
    // a selectedRenderers change.
    notifyObservers(argument);

    _graphicsEngine.clearCrossHairs();
    _graphicsEngine.clearSelectedRenderers();
    _graphicsEngine.clearAllFlashingRenderersAndLayers();

    // there are two modes of selection:
    // 1.  The user clicked on a cad feature
    // When using this method, a complex algorithm to figure out what to select is used (see below)
    // 2.  The user used the group select mode to draw a rectangle around a bunch of stuff
    // When using the second method, EVERYTHING is selected

    // Let's handle the 2nd method first (because it's easier)
    // if this wasn't a group select, it still could be a CTRL-Left Click which ADDS to the selections.  Treat this as a group select
    if (_graphicsEngine.wasSelectionPartOfAGroupSelection())
    {
      /** Warning "unchecked cast" approved for cast from Object types.*/
      for (com.axi.guiUtil.Renderer renderer : (Collection<com.axi.guiUtil.Renderer>)argument)
      {
        if (MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment())
        {
          if (renderer != null) // could be null if they clicked outside the boundary of any renderer
          {
            if (renderer instanceof PadRenderer)
            {
              continue;
            }
          }
        }
        _graphicsEngine.setSelectedRenderer(renderer);
        if(renderer != null) // could be null if they clicked outside the boundary of any renderer
        {
          if(renderer instanceof ComponentRenderer)
          {
            if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment())
            {
              ComponentRenderer compRenderer = (ComponentRenderer) renderer;              
              //Jack Hwee
              VirtualLiveImageGenerationSettings.getInstance().setBoardName(compRenderer.getComponent().getBoard().getName()); 
              VirtualLiveImageGenerationSettings.getInstance().addComponentRefDes(compRenderer.getComponent().getReferenceDesignator(), compRenderer.getComponent().getBoard().getPanel().isMultiBoardPanel());
           
              VirtualLiveImageGenerationSettings.getInstance().addVirtualLiveImageGenerationData(compRenderer.getComponent().getReferenceDesignator(),
                                                                                                 compRenderer.getComponent().getShapeRelativeToPanelInNanoMeters());
              
              VirtualLiveImageGenerationSettings.getInstance().setObservable(compRenderer.getComponent().getShapeRelativeToPanelInNanoMeters());        
            }
          }        
        }
      }      
      
       //Jack Hwee - only allow user to select 3 components
      if (MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment() && VirtualLiveImageGenerationSettings.getInstance().getComponentList().size() > 3)
      {
        String componentToBeRemoved = VirtualLiveImageGenerationSettings.getInstance().getComponentList().get(0);
        String boardName = "1";
        if (Project.getCurrentlyLoadedProject().getPanel().isMultiBoardPanel())
        {
          String[] refDesBoardName = componentToBeRemoved.split("#");
          componentToBeRemoved = refDesBoardName[0];
          boardName = refDesBoardName[1];
        }        
        VirtualLiveImageGenerationSettings.getInstance().getComponentList().remove(0);
        _graphicsEngine.clearSelectedRenderers();
        for (com.axi.guiUtil.Renderer renderer : (Collection<com.axi.guiUtil.Renderer>) argument)
        {
          if (renderer instanceof ComponentRenderer)
          {
            ComponentRenderer compRenderer = (ComponentRenderer) renderer;
            if (compRenderer.getComponent().getBoard().getPanel().isMultiBoardPanel() == false)
            {
              if (compRenderer.getComponent().getReferenceDesignator().equals(componentToBeRemoved) == false)
              {
                _graphicsEngine.setSelectedRenderer(renderer);
                 VirtualLiveImageGenerationSettings.getInstance().setObservable(compRenderer.getComponent().getShapeRelativeToPanelInNanoMeters());
              }
            }
            else
            {   
              if (compRenderer.getComponent().getReferenceDesignator().equals(componentToBeRemoved) == false || compRenderer.getComponent().getBoard().getName().equals(boardName) == false)
              {
                _graphicsEngine.setSelectedRenderer(renderer);
                VirtualLiveImageGenerationSettings.getInstance().setObservable(compRenderer.getComponent().getShapeRelativeToPanelInNanoMeters());
              }
            }       
          }
        }
      }
      _drawCadPanel.setDescription(" "); // clear out the descriptive text because lots of things were selected
    }
    else
    {
      int i = 0;
      com.axi.guiUtil.Renderer rendererToUse = null;
      // here, I want to hightlight ONE renderer, even though I may have a lot in the Collection.  The
      // collection is not order dependent, so I'll need to iterate through and find the one I want to use.
      // The ONE will be, in order:
      //  1.  A Fiducial renderer.  If more than one fiducial, choose one from the TOP side
      //  2.  A Pad renderer.  If more than one pad renderer, choose one from the TOP side
      //  3.  A component renderer.  If more than one, choose one from the TOP side
      //  4.  A Board Renderer
      //  5.  A panel renderer

      /** Warning "unchecked cast" approved for cast from Object types.*/
      for (com.axi.guiUtil.Renderer renderer : (Collection<com.axi.guiUtil.Renderer>)argument)
      {
        ++i;
        // if it's a fiducial, use it
        if (renderer instanceof FiducialRenderer)
        {
          FiducialRenderer fiducialRenderer = (FiducialRenderer)renderer;
          if (rendererToUse == null) // haven't found anything yet
            rendererToUse = renderer;
          else // we have a fiducial renderer to consider, so compare to rendererToUse
          {
            if (rendererToUse instanceof FiducialRenderer)
            {
              if (fiducialRenderer.getFiducial().isTopSide())
                rendererToUse = renderer;
            }
            else // the previously chosen one is not a fiducial, so make it this fiducial
              rendererToUse = renderer;
          }
        }
        // if it's a pad, use it
        else if (renderer instanceof PadRenderer)
        {
          PadRenderer padRenderer = (PadRenderer)renderer;
          if (rendererToUse == null) // haven't found anything yet
            rendererToUse = renderer;
          else // we have a pad renderer to consider, so compare to rendererToUse
          {
            if (rendererToUse instanceof PadRenderer)
            {
              if (padRenderer.getPad().isTopSide())
                rendererToUse = renderer;
            }
            else // the previously chosen one is not a pad, so make it this pad
              rendererToUse = renderer;
          }
        }
        else if (renderer instanceof ComponentRenderer)
        {
          ComponentRenderer compRenderer = (ComponentRenderer)renderer;
          if (rendererToUse == null) // haven't found any yet, so use this one
            rendererToUse = renderer;
          else
          {
            if ((rendererToUse instanceof PadRenderer) == false) // already using a pad, so don't change to component
            {
              // our renderer is not a pad, so we'll use this current one
              if (compRenderer.getComponent().isTopSide())
                rendererToUse = renderer;
              else if (rendererToUse instanceof ComponentRenderer)
              {
                // our state here:  rendererToUse is a component and the one under consideration is a component
                // so only switch if rendererToUse is on the bottom and the renderer is on the top
                ComponentRenderer compRendererToUse = (ComponentRenderer)rendererToUse;
                if ((compRendererToUse.getComponent().isTopSide() == false) &&
                    (compRenderer.getComponent().isTopSide()))
                  rendererToUse = renderer;
              }
              else
                rendererToUse = renderer;
            }
          }
        }
        else if ((renderer instanceof InspectionRegionRenderer) ||
                 (renderer instanceof VerificationRegionRenderer)||
                 (renderer instanceof RectangleRenderer))
        {
          if (rendererToUse == null) // haven't found any yet, so use this one
            rendererToUse = renderer;
          else
          {
            if (((rendererToUse instanceof PadRenderer) == false) &&
                ((rendererToUse instanceof ComponentRenderer) == false))// already using a pad, so don't change to component
              rendererToUse = renderer;
          }
        }
        else if (renderer instanceof BoardRenderer)
        {
          // only use if nothing or set to panel renderer
          if (rendererToUse == null)
            rendererToUse = renderer;
          else
          {
            if (rendererToUse instanceof PanelRenderer)
              rendererToUse = renderer;
          }
        }
        else // last resort
        {
          if (rendererToUse == null)
            rendererToUse = renderer;
        }
        // when i==1, it's the first on list.  Use that unless it's already been set by the pad
        if ((i == 1) && (rendererToUse == null))
          rendererToUse = renderer;
      }
      if(rendererToUse != null) // could be null if they clicked outside the boundary of any renderer
      {
        if(rendererToUse instanceof ComponentRenderer)
        {
          if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment())
          {
            ComponentRenderer compRenderer = (ComponentRenderer) rendererToUse;
            //Jack Hwee         
            VirtualLiveImageGenerationSettings.getInstance().clearComponentList();
            VirtualLiveImageGenerationSettings.getInstance().setBoardName(compRenderer.getComponent().getBoard().getName());
            VirtualLiveImageGenerationSettings.getInstance().addComponentRefDes(compRenderer.getComponent().getReferenceDesignator(), compRenderer.getComponent().getBoard().getPanel().isMultiBoardPanel());
         
            VirtualLiveImageGenerationSettings.getInstance().setComponentRefDes(compRenderer.getComponent().getReferenceDesignator());
            
            VirtualLiveImageGenerationSettings.getInstance().setVirtualLiveImageGenerationData(compRenderer.getComponent().getShapeRelativeToPanelInNanoMeters());
              //XCR-2612 prompt out message to ask user re-select another component    
            if (VirtualLiveImageGenerationSettings.getInstance().isSelectedVirtualLiveRoiInvalid()) 
            {
              String message = StringLocalizer.keyToString("VIRTUAL_LIVE_IMAGE_ROI_INVALID_KEY");
              // cannot generate virtual live image set for the selected region.
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                message,
                StringLocalizer.keyToString("GISWK_INVALID_ROI_KEY"),
                JOptionPane.ERROR_MESSAGE);
              return;
            }
            drawVirtualLiveAreaDisplay();
            _graphicsEngine.setSelectedRenderer(compRenderer);
          }
          else
          {
            _rendererToUse = rendererToUse;
            // now that we have the renderer of interest, we need to highlight it!
            _graphicsEngine.setSelectedRenderer(rendererToUse);
            setStatusBarDescription(rendererToUse);
          }
        }
        else if(rendererToUse instanceof PadRenderer)
        {
          if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment())
          {
            PadRenderer padRenderer = (PadRenderer) rendererToUse;
            ComponentRenderer compRenderer =  padRenderer.getComponentRenderer();        
            //Jack Hwee         
            VirtualLiveImageGenerationSettings.getInstance().clearComponentList();
            VirtualLiveImageGenerationSettings.getInstance().setBoardName(compRenderer.getComponent().getBoard().getName());
            VirtualLiveImageGenerationSettings.getInstance().addComponentRefDes(compRenderer.getComponent().getReferenceDesignator(), compRenderer.getComponent().getBoard().getPanel().isMultiBoardPanel());
         
            VirtualLiveImageGenerationSettings.getInstance().setComponentRefDes(compRenderer.getComponent().getReferenceDesignator());
           
            VirtualLiveImageGenerationSettings.getInstance().setVirtualLiveImageGenerationData(compRenderer.getComponent().getShapeRelativeToPanelInNanoMeters());
            drawVirtualLiveAreaDisplay();
            _graphicsEngine.setSelectedRenderer(compRenderer);
          }
        }
        else
        { 
          //Jack Hwee      
          if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment())
          {
            //Jack Hwee
            VirtualLivePanel.getInstance().resetAllVirtualLiveSettings();
            drawVirtualLiveAreaDisplay();
            //_graphicsEngine.clearRegionCrossHairs();
            clearVirtualLiveAreaDisplay();     
            _graphicsEngine.invalidate();
          }
        }
      }
    }
  }

  /**
   * Updates the status bar description based on the renderer passed in
   * @author Andy Mechtenberg
   */
  private void setStatusBarDescription(com.axi.guiUtil.Renderer renderer)
  {
    Assert.expect(renderer != null);
    String description = " ";
    // handle all the normal types of selections: panel board component pad
    if (renderer instanceof PanelRenderer)
    {
      LocalizedString panelString = new LocalizedString("GUI_PANEL_SUMMARY_KEY", new Object[]{renderer.toString()});
      description = StringLocalizer.keyToString(panelString);
    }
    else if (renderer instanceof BoardRenderer)
    {
      BoardRenderer boardRenderer = (BoardRenderer)renderer;

      String projName = boardRenderer.getBoard().getPanel().getProject().getName();
      LocalizedString panelString = new LocalizedString("GUI_PANEL_SUMMARY_KEY", new Object[]{projName});
      description = StringLocalizer.keyToString(panelString);
      LocalizedString boardString = new LocalizedString("GUI_BOARD_SUMMARY_KEY", new Object[]{renderer.toString()});
      description = StringLocalizer.keyToString(panelString) + " " + StringLocalizer.keyToString(boardString);

    }
    else if (renderer instanceof ComponentRenderer)
    {
      Component component = ((ComponentRenderer)renderer).getComponent();
      description = getComponentDescriptionString(component);
    }
    else if (renderer instanceof PadRenderer)
    {
      Pad pad = ((PadRenderer)renderer).getPad();
      description = getPadDescriptionString(pad);
    }
    else if (renderer instanceof FiducialRenderer)
    {
      Fiducial fiducial = ((FiducialRenderer)renderer).getFiducial();
      LocalizedString fiducialString = null;
     if (fiducial.isOnBoard())
       fiducialString = new LocalizedString("GUI_BOARD_FIDUCIAL_SUMMARY_KEY", new Object[]{fiducial.getName()});
     else if (fiducial.isOnPanel())
       fiducialString = new LocalizedString("GUI_PANEL_FIDUCIAL_SUMMARY_KEY", new Object[]{fiducial.getName()});
     else
       Assert.expect(false, "Found fiducial that is neither on the board nor on the panel");

     description = StringLocalizer.keyToString(fiducialString);
    }
    else// if (renderer instanceof InspectionRegionRenderer) || (renderer instanceof VerificationRegionRenderer))
    {
      description = renderer.toString();
    }
    _drawCadPanel.setDescription(description);
  }

  /**
   * @author Andy Mechtenberg
   */
  private String getComponentDescriptionString(Component component)
  {
    Assert.expect(component != null);
    // component is quite complex.  We want to display the package, land pattern and possibly joint type
    // and subtype
    String jointType = TestDev.getComponentDisplayJointType(component.getComponentType());

    String subType = TestDev.getComponentDisplaySubtype(component.getComponentType());

    LocalizedString localizedDescription = new LocalizedString("GUI_COMPONENT_SUMMARY_KEY",
        new Object[]
        {component.getReferenceDesignator(),
        component.getCompPackage().toString(),
        component.getLandPattern().getName(),
        jointType,
        subType});
    return StringLocalizer.keyToString(localizedDescription);
  }

  /**
   * @author Andy Mechtenberg
   */
  private String getPadDescriptionString(Pad pad)
  {
    Assert.expect(pad != null);
    Component component = pad.getComponent();
    PadType padType = pad.getPadType();
    // pad, like component, is quite complex.  We want to display the package, land pattern and possibly joint type
    // and subtype.  But pad joint type and subtype cannot be "mixed", so that's a little simpler
    String jointType = pad.getJointTypeEnum().getName();

    String subType = "";
    if (pad.isTestable() == false)
      subType = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_UNTESTABLE_KEY");
    else if (padType.getComponentType().isLoaded() == false)
      subType = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOLOAD_KEY");
    else if (padType.isInspected() == false)
      subType = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOTEST_KEY");
    else
      subType = padType.getSubtype().getShortName();

    LocalizedString padDescription = new LocalizedString("GUI_PIN_SUMMARY_KEY", new Object[]{pad.getName()});

    LocalizedString componentDescription = new LocalizedString("GUI_COMPONENT_SUMMARY_KEY",
        new Object[]
        {component.getReferenceDesignator(),
        component.getCompPackage().toString(),
        component.getLandPattern().getName(),
        jointType,
        subType});
    return StringLocalizer.keyToString(padDescription) + " " + StringLocalizer.keyToString(componentDescription);
  }


  /**
   * @author Andy Mechtenberg
   */
  private void highlightComponentType(ComponentType compType)
  {      
    java.util.List<ComponentRenderer> renderersToHighlight = null;
    if(compType.isLoaded() == false)
      renderersToHighlight = findNoLoadComponentRenderers(compType);
    else
      renderersToHighlight = findComponentRenderers(compType);
    // first, figure out which side(s) of the panel we need to make visible in order to get all the
    // the component renderers visible.  Because of flipped panels, both sides may need to be made visible
    boolean topComponentsVisible = isTopSideVisible();
    boolean bottomComponentsVisible = isBottomSideVisible();
    boolean componentOnTop = false;
    boolean componentOnBottom = false;

    //There is no use to continue the code if the renderers size is 0
    if(renderersToHighlight.size() == 0)
      return; 
    
    for (ComponentRenderer componentRenderer : renderersToHighlight)
    {
      if (componentRenderer.getComponent().isTopSide())
        componentOnTop = true;
      else
        componentOnBottom = true;
    }
    if (componentOnTop && componentOnBottom) // flipped board case, must show both sides at once!
    {
      _controlToolBar.showBothSides();
    }
    else // the user has only one side to show, so if they've only got one side visible, respect that only make what needs to be visible
    {
      if (componentOnTop && (topComponentsVisible == false))
      {
        _controlToolBar.showTopSide();
      }
      if ((componentOnBottom) && (bottomComponentsVisible == false))
      {
        _controlToolBar.showBottomSide();
      }
    }
    // use flashing to highlight all but the first renderer -- the first one will be highlighted with crosshairs
    // if a single board was selected, select the renderer for that board
    _graphicsEngine.clearAllFlashingRenderersAndLayers();
    ComponentRenderer firstComponent = null;
    if (_selectedBoardName == null)
    {
      firstComponent = (ComponentRenderer)renderersToHighlight.remove(0);
    }
    else
    {
      int index = 0;
      for (ComponentRenderer componentRenderer : renderersToHighlight)
      {
        if (componentRenderer.getComponent().getBoard().getName().equals(_selectedBoardName))
        {
          firstComponent = (ComponentRenderer)renderersToHighlight.remove(index);
          break;
        }
        index++;
      }
    }
    if (firstComponent == null)
    {
      firstComponent = (ComponentRenderer)renderersToHighlight.remove(0);
    }
    if (renderersToHighlight.isEmpty() == false)
    {
      _graphicsEngine.setFlashRenderers(renderersToHighlight, java.awt.Color.YELLOW);
      
      // XCR1533 - The flashing cause memory leak in V810 
      _graphicsEngine.setFlashing(false);
    }
    // use crosshairs to highlight ONE of the components -- the first one in the list, which we just removed
    _graphicsEngine.selectWithCrossHairs(firstComponent);

    // move to center if zoom is not at 1:1
    if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0) == false)
    {
      // we're at some zoom level, let's center the graphics to show the new alignment region
      DoubleCoordinate coordinate = firstComponent.getCenterCoordinate();
      _graphicsEngine.center(coordinate.getX(), coordinate.getY(), 0, 0);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void highlightFiducialType(FiducialType fidType)
  {
    java.util.List<FiducialRenderer> renderersToHighlight = getFiducialRenderers(fidType);
    // first, figure out which side(s) of the panel we need to make visible in order to get all the
    // the component renderers visible.  Because of flipped panels, both sides may need to be made visible
    boolean topFiducialsVisible = isTopSideVisible();
    boolean bottomFiducialsVisible = isBottomSideVisible();
    boolean fiducialOnTop = false;
    boolean fiducialOnBottom = false;

    for (FiducialRenderer fiducialRenderer : renderersToHighlight)
    {
      if (fiducialRenderer.getFiducial().isTopSide())
        fiducialOnTop = true;
      else
        fiducialOnBottom = true;
    }
    if (fiducialOnTop && fiducialOnBottom) // flipped board case, must show both sides at once!
    {
      _controlToolBar.showBothSides();
    }
    else // the user has only one side to show, so if they've only got one side visible, respect that only make what needs to be visible
    {
      if (fiducialOnTop && (topFiducialsVisible == false))
      {
        _controlToolBar.showTopSide();
      }
      if ((fiducialOnBottom) && (bottomFiducialsVisible == false))
      {
        _controlToolBar.showBottomSide();
      }
    }
    // use flashing to highlight all but the first renderer -- the first one will be highlighted with crosshairs
    // if a single board was selected, select the renderer for that board
    _graphicsEngine.clearAllFlashingRenderersAndLayers();
    FiducialRenderer firstFiducial = null;
    if (fidType.getFiducials().get(0).isOnBoard())
    {
      if (_selectedBoardName == null)
      {
        firstFiducial = (FiducialRenderer)renderersToHighlight.remove(0);
      }
      else
      {
        int index = 0;
        for (FiducialRenderer fiducialRenderer : renderersToHighlight)
        {
          if (fiducialRenderer.getFiducial().getSideBoard().getBoard().getName().equals(_selectedBoardName))
          {
            firstFiducial = (FiducialRenderer)renderersToHighlight.remove(index);
            break;
          }
          index++;
        }
      }
    }
    else
      firstFiducial = (FiducialRenderer)renderersToHighlight.remove(0);

    if (firstFiducial == null)
    {
      firstFiducial = (FiducialRenderer)renderersToHighlight.remove(0);
    }
    if (renderersToHighlight.isEmpty() == false)
    {
      _graphicsEngine.setFlashRenderers(renderersToHighlight, java.awt.Color.YELLOW);
      
      // XCR1533 - The flashing cause memory leak in V810 
      _graphicsEngine.setFlashing(false);
    }
    // use crosshairs to highlight ONE of the components -- the first one in the list, which we just removed
    _graphicsEngine.selectWithCrossHairs(firstFiducial);

    // move to center if zoom is not at 1:1
    if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0) == false)
    {
      // we're at some zoom level, let's center the graphics to show the new alignment region
      DoubleCoordinate coordinate = firstFiducial.getCenterCoordinate();
      _graphicsEngine.center(coordinate.getX(), coordinate.getY(), 0, 0);
    }
  }

  /**
   * @param components A collection of ComponentType objects
   * @author Andy Mechtenberg
   */
  private void highlightMultipleComponents(Collection<ComponentType> components)
  {
    Assert.expect(components != null);
    Assert.expect(components.size() > 1);

    for (ComponentType componentType : components)
    {
      for (ComponentRenderer componentRenderer : findComponentRenderers(componentType))
      {
        _graphicsEngine.setSelectedRenderer(componentRenderer);
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void highlightMultipleFiducials(Collection<FiducialType> fiducials)
  {
    Assert.expect(fiducials != null);
    Assert.expect(fiducials.size() > 1);

    for (FiducialType fiducialType : fiducials)
    {
      for (FiducialRenderer fiducialRenderer : getFiducialRenderers(fiducialType))
      {
        _graphicsEngine.setSelectedRenderer(fiducialRenderer);
      }
    }
  }
  
  /*
  * Author Kee Chin Seong
  */
  private java.util.List<ComponentRenderer> findNoLoadComponentRenderers(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    java.util.List<ComponentRenderer> componentRenderers = new ArrayList<ComponentRenderer>();

    java.util.List<Component> components = componentType.getComponents();
    for (Component component : components)
    {
      boolean onTop = component.isTopSide();

      ComponentRenderer componentRenderer = null;


      Collection<com.axi.guiUtil.Renderer> renderers = null;
      if (onTop)
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(_topNoLoadedComponentLayer));
      else
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(_bottomNoLoadedComponentLayer));

      Collection<ComponentRenderer> compRenderers = new ArrayList<ComponentRenderer>();
      for (com.axi.guiUtil.Renderer renderer : renderers)
      {
        if(renderer instanceof ComponentRenderer)
           compRenderers.add((ComponentRenderer)renderer);
      }

      for (ComponentRenderer compRenderer : compRenderers)
      {
        com.axi.v810.business.panelDesc.Component comp = compRenderer.getComponent();
        if (comp == component)
        {
          componentRenderer = compRenderer;
          break;
        }
      }
//      Assert.expect(componentRenderer != null);
      if(componentRenderer != null)
        componentRenderers.add(componentRenderer);
    }
    return componentRenderers;
  }

  /**
   * @return a List of ComponentRenderers that match the componentType passed in
   * @author Andy Mechtenberg
   */
  private java.util.List<ComponentRenderer> findComponentRenderers(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    java.util.List<ComponentRenderer> componentRenderers = new ArrayList<ComponentRenderer>();

    java.util.List<Component> components = componentType.getComponents();
    for (Component component : components)
    {
      boolean onTop = component.isTopSide();

      ComponentRenderer componentRenderer = null;


      Collection<com.axi.guiUtil.Renderer> renderers = null;
      if (onTop)
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopComponentLayers()));
      else
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomComponentLayers()));

      Collection<ComponentRenderer> compRenderers = new ArrayList<ComponentRenderer>();
      for (com.axi.guiUtil.Renderer renderer : renderers)
      {
        // XCR-3650 Filtering component is not working after delete component
        if (renderer instanceof ComponentRenderer)
          compRenderers.add((ComponentRenderer)renderer);
      }

      for (ComponentRenderer compRenderer : compRenderers)
      {
        com.axi.v810.business.panelDesc.Component comp = compRenderer.getComponent();
        if (comp == component)
        {
          componentRenderer = compRenderer;
          break;
        }
      }
//      Assert.expect(componentRenderer != null);
      if(componentRenderer != null)
        componentRenderers.add(componentRenderer);
    }
    return componentRenderers;
  }

  /**
   * @return a List of FiducialRenderers that match the fiducialType passed in
   * @author Laura Cormos
   */
  private java.util.List<FiducialRenderer> findFiducialRenderers(FiducialType fiducialType)
  {
    Assert.expect(fiducialType != null);
    java.util.List<FiducialRenderer> fiducialRenderers = new ArrayList<FiducialRenderer>();

    java.util.List<Fiducial> fiducials = fiducialType.getFiducials();
    for (Fiducial fiducial : fiducials)
    {
      boolean onTop = fiducial.isTopSide();

      FiducialRenderer fiducialRenderer = null;

      Collection<com.axi.guiUtil.Renderer> renderers = null;
      if (fiducial.isOnBoard())
      {
        if (onTop)
          renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopBoardFiducialLayers()));
        else
          renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomBoardFiducialLayers()));
      }
      else if (fiducial.isOnPanel())
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getPanelFiducialLayers()));
      else
        Assert.expect(false);

      Collection<FiducialRenderer> fidRenderers = new ArrayList<FiducialRenderer>();
      for (com.axi.guiUtil.Renderer renderer : renderers)
      {
        fidRenderers.add((FiducialRenderer)renderer);
      }

      for (FiducialRenderer fidRenderer : fidRenderers)
      {
        Fiducial fid = fidRenderer.getFiducial();
        if (fid == fiducial)
        {
          fiducialRenderer = fidRenderer;
          break;
        }
      }
      Assert.expect(fiducialRenderer != null);
      fiducialRenderers.add(fiducialRenderer);
    }
    return fiducialRenderers;
  }

  /**
   * @author Andy Mechtenberg
   * @EditedBy : Kee Chin Seong
   */
  public void selectPadTypes(Collection<PadType> padTypes)
  {
    Assert.expect(padTypes != null);

    java.util.List<PadRenderer> padRenderers = findPadRenderersForPadTypes(padTypes);
    
    //Kee Chin Seong - ok user might select the pad types, make sure padtype is not empty.
    if(padRenderers.size() <= 0)
      return;
    
    selectPadRenderers(padRenderers);

    // if all pads belong to the same component, then crosshair that component

    ComponentType componentTypeToSelect = null;
    for(PadType padType : padTypes)
    {
      ComponentType compType = padType.getComponentType();
      if (componentTypeToSelect == null)
        componentTypeToSelect = compType;
      else
      {
        if (compType != componentTypeToSelect)
        {
          componentTypeToSelect = null;
          break;
        }
      }
    }
    //Kee chin seong :: Make sure the component is not loaded.
    if (componentTypeToSelect != null && componentTypeToSelect.isLoaded() == true)
    {
      java.util.List<ComponentRenderer> renderersToHighlight = findComponentRenderers(componentTypeToSelect);
      ComponentRenderer firstComponent = (ComponentRenderer)renderersToHighlight.remove(0);
      _graphicsEngine.selectWithCrossHairs(firstComponent);
//      highlightComponentType(componentTypeToSelect);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void selectPadsAndFiducials(Collection<Object> padsAndFiducials)
  {
    java.util.List<Pad> pads = new ArrayList<Pad>();
    java.util.List<Fiducial> fiducials = new ArrayList<Fiducial>();
    for(Object obj : padsAndFiducials)
    {
      if (obj instanceof Pad)
      {
        Pad pad = (Pad)obj;
        pads.add(pad);
      }
      else if (obj instanceof Fiducial)
      {
        Fiducial fiducial = (Fiducial)obj;
        fiducials.add(fiducial);
      }
      else
        Assert.expect(false, "Unexpected object type: " + obj);
    }
    java.util.List<PadRenderer> padRenderers = findPadRenderersForPads(pads);
    selectPadRenderers(padRenderers);

    java.util.List<FiducialRenderer> fiducialRenderers = findFiducialRenderersForFiducials(fiducials);
    selectFiducialRenderers(fiducialRenderers);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void selectPadRenderers(java.util.List<PadRenderer> padRenderers)
  {
    Assert.expect(padRenderers != null);

    // With pads, we don't worry about top/bottom side.  If the pad is visible, it'll be selected.
    // This is because there is a zoom threshold which must be met before pads are drawn anyway, so
    // finding it is pretty easy.
//    _graphicsEngine.clearSelectedRenderers();
//    _graphicsEngine.clearCrossHairs();
//    _graphicsEngine.clearAllFlashingRenderersAndLayers();

    for (PadRenderer padRenderer : padRenderers)
    {
//      setDescription(padRenderer.toString());
      _graphicsEngine.setSelectedRenderer(padRenderer);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void selectFiducialRenderers(java.util.List<FiducialRenderer> fiducialRenderers)
  {
    Assert.expect(fiducialRenderers != null);
    for (FiducialRenderer fiducialRenderer : fiducialRenderers)
      _graphicsEngine.setSelectedRenderer(fiducialRenderer);
  }
  
  /**
   * @author Andy Mechtenberg
   * @author Kee Chin Seong
   */
  public void highlightPads(Collection<HighlightAlignmentItems> highlightPads)
  {
    Assert.expect(highlightPads != null);

    for(HighlightAlignmentItems highlightPad : highlightPads)
    {
      List<Pad> pads = highlightPad.getPads();
      Color highlightColor = highlightPad.getHighlightColor();
      java.util.List<PadRenderer> padRenderers = findPadRenderersForPads(pads);
      //Kee Chin Seong - im expecting there is highlight always here, but if is Empty, Ok might inside the noload Component there
      //                 look for it! BUT provided with the show no load component is turn on
      if(padRenderers.isEmpty() && _showNoLoadComponent == true)
        padRenderers = findNoLoadPadRenderersForPads(pads);
      
      highlightPadRenderers(padRenderers, highlightColor);

      List<Fiducial> fiducials = highlightPad.getFiducials();
      java.util.List<FiducialRenderer> fiducialRenderers = findFiducialRenderersForFiducials(fiducials);
      highlightFiducialRenderers(fiducialRenderers, highlightColor);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void highlightPadRenderers(java.util.List<PadRenderer> padRenderers, Color highlightColor)
  {
    Assert.expect(padRenderers != null);

    // With pads, we don't worry about top/bottom side.  If the pad is visible, it'll be highlighted.
    // This is because there is a zoom threshold which must be met before pads are drawn anyway, so
    // finding it is pretty easy.
    for (PadRenderer padRenderer : padRenderers)
    {
//      setDescription(padRenderer.toString());
      _graphicsEngine.setHighightedRenderer(padRenderer, highlightColor);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void highlightFiducialRenderers(java.util.List<FiducialRenderer> fiducialRenderers, Color highlightColor)
  {
    Assert.expect(fiducialRenderers != null);

    // With pads, we don't worry about top/bottom side.  If the pad is visible, it'll be highlighted.
    // This is because there is a zoom threshold which must be met before pads are drawn anyway, so
    // finding it is pretty easy.
    for (FiducialRenderer fiducialRenderer : fiducialRenderers)
    {
//      setDescription(padRenderer.toString());
      _graphicsEngine.setHighightedRenderer(fiducialRenderer, highlightColor);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private java.util.List<PadRenderer> findPadRenderersForPadTypes(Collection<PadType> padTypes)
  {
    Assert.expect(padTypes != null);
    java.util.List<PadRenderer> padRenderers = new ArrayList<PadRenderer>();
    for (PadType padType : padTypes)
    {
      java.util.List<Pad> pads = padType.getPads();
      for (Pad pad : pads)
      {
        boolean onTop = pad.isTopSide();
        PadRenderer padRenderer = null;
        java.util.List<com.axi.guiUtil.Renderer> renderers = null;

        if (onTop)
          renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopPadLayers()));
        else
          renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomPadLayers()));

        for (com.axi.guiUtil.Renderer tempPadRenderer : renderers)
        {
          com.axi.v810.business.panelDesc.Pad tempPad = ((PadRenderer)tempPadRenderer).getPad();
          if (tempPad == pad)
          {
            padRenderer = (PadRenderer)tempPadRenderer;
            break;
          }
        }
        // since pads are not always shown, this renderer may not be created.  In other words, it's ok for this to be null
        if (padRenderer != null)
          padRenderers.add(padRenderer);
      }
    }
    return padRenderers;
  }

  /**
   * @author Andy Mechtenberg
   */
  private java.util.List<PadRenderer> findPadRenderersForPads(Collection<Pad> pads)
  {
    Assert.expect(pads != null);
    java.util.List<PadRenderer> padRenderers = new ArrayList<PadRenderer>();
    for (Pad pad : pads)
    {
      boolean onTop = pad.isTopSide();
      boolean throughHolePad = pad.isThroughHolePad();
      PadRenderer padRenderer = null;
      java.util.List<com.axi.guiUtil.Renderer> renderers = null;

      if (throughHolePad)
      {
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopPadLayers()));
        renderers.addAll(new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomPadLayers())));
      }
      else if (onTop)
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopPadLayers()));
      else
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomPadLayers()));

      for (com.axi.guiUtil.Renderer tempPadRenderer : renderers)
      {
        com.axi.v810.business.panelDesc.Pad tempPad = ((PadRenderer)tempPadRenderer).getPad();
        if (tempPad == pad)
        {
          padRenderer = (PadRenderer)tempPadRenderer;
          if (throughHolePad)
          {
            padRenderers.add(padRenderer);
          }
          else
            break;  // if surface mount, it's on top or bottom, but only one place, so we can stop as soon as find one
        }
      }
      // since pads are not always shown, this renderer may not be created.  In other words, it's ok for this to be null
      if ((throughHolePad == false) && (padRenderer != null))
      {
        padRenderers.add(padRenderer);
      }
    }
    return padRenderers;
  }
  
  /*
  * @author KEe Chin Seong
  */
  private java.util.List<PadRenderer> findNoLoadPadRenderersForPads(Collection<Pad> pads)
  {
    Assert.expect(pads != null);
    java.util.List<PadRenderer> padRenderers = new ArrayList<PadRenderer>();
    for (Pad pad : pads)
    {
      boolean onTop = pad.isTopSide();
      boolean throughHolePad = pad.isThroughHolePad();
      PadRenderer padRenderer = null;
      java.util.List<com.axi.guiUtil.Renderer> renderers = null;

      if (throughHolePad)
      {
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(_topNoLoadPadLayer));
        renderers.addAll(new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(_bottomNoLoadPadLayer)));
      }
      else if (onTop)
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(_topNoLoadPadLayer));
      else
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(_bottomNoLoadPadLayer));

      for (com.axi.guiUtil.Renderer tempPadRenderer : renderers)
      {
        com.axi.v810.business.panelDesc.Pad tempPad = ((PadRenderer)tempPadRenderer).getPad();
        if (tempPad == pad)
        {
          padRenderer = (PadRenderer)tempPadRenderer;
          if (throughHolePad)
          {
            padRenderers.add(padRenderer);
          }
          else
            break;  // if surface mount, it's on top or bottom, but only one place, so we can stop as soon as find one
        }
      }
      // since pads are not always shown, this renderer may not be created.  In other words, it's ok for this to be null
      if ((throughHolePad == false) && (padRenderer != null))
      {
        padRenderers.add(padRenderer);
      }
    }
    return padRenderers;
  }

  /**
   * @author Andy Mechtenberg
   */
  private java.util.List<FiducialRenderer> findFiducialRenderersForFiducials(Collection<Fiducial> fiducials)
  {
    Assert.expect(fiducials != null);
    java.util.List<FiducialRenderer> fiducialRenderers = new ArrayList<FiducialRenderer>();
    for (Fiducial fiducial : fiducials)
    {
      boolean onTop = fiducial.isTopSide();
      FiducialRenderer fiducialRenderer = null;
      java.util.List<com.axi.guiUtil.Renderer> renderers = null;

      if (onTop)
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopBoardFiducialLayers()));
      else
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomBoardFiducialLayers()));

      for (com.axi.guiUtil.Renderer tempFiducialRenderer : renderers)
      {
        com.axi.v810.business.panelDesc.Fiducial tempFiducial = ((FiducialRenderer)tempFiducialRenderer).getFiducial();
        if (tempFiducial == fiducial)
        {
          fiducialRenderer = (FiducialRenderer)tempFiducialRenderer;
          break;
        }
      }

      if (fiducialRenderer != null)
      {
        fiducialRenderers.add(fiducialRenderer);
      }
    }
    return fiducialRenderers;
  }

  /**
   * update the board data from the datastore
   * @author George A. David
   */
  private void updateBoardData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getSource() instanceof Board);
    Assert.expect(event.getProjectChangeEventEnum() instanceof BoardEventEnum);

    BoardEventEnum eventEnum = (BoardEventEnum)event.getProjectChangeEventEnum();
    Board board = (Board)event.getSource();

    if (eventEnum.equals(eventEnum.COORDINATE) ||
        eventEnum.equals(eventEnum.DEGREES_ROTATION))
    {
      _graphicsEngine.validateData(getAllLayers(board));
    }
    else if (eventEnum.equals(eventEnum.LEFT_TO_RIGHT_FLIP) ||
             eventEnum.equals(eventEnum.TOP_TO_BOTTOM_FLIP))
    {
      // the selections must be clear before swapping layers or it loses track of
      // which layer needs to be on top
      _graphicsEngine.clearSelectedRenderers();
      flipBoard(board);
    }
    else if (eventEnum.equals(eventEnum.NAME))
    {
      // do nothing, as we don't render the name
    }
    else if (eventEnum.equals(eventEnum.CREATE_OR_DESTROY) || eventEnum.equals(BoardEventEnum.ADD_OR_REMOVE))
    {
      // have to add or remove a whole board from the engine
      if (event.isDestroyed() || event.isRemoved())
      {
        removeBoard((Board)event.getOldValue());
      }
      else
      {
        createBoard(board);
      }
    }
    if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0))
    {
      _graphicsEngine.fitGraphicsToScreen();
    }

  }
  
  /*
   * @author Kee Chin Seong
   */
  private void updateComponentTypeLoadedData(ProjectChangeEvent event, final Object argument)
  {
     ProjectChangeEventEnum eventEnum = ((ProjectChangeEvent)argument).getProjectChangeEventEnum();
     ComponentTypeSettingsEventEnum compTypeSettingsEventEnum = (ComponentTypeSettingsEventEnum) eventEnum;
     
      if(compTypeSettingsEventEnum.equals(ComponentTypeSettingsEventEnum.LOADED))
      {
        ComponentTypeSettings componentSettings = (ComponentTypeSettings) event.getSource();
        updateComponentTypeData(componentSettings);
      }
      else if (compTypeSettingsEventEnum.equals(ComponentTypeSettingsEventEnum.LOADED_LIST))
      {
        List<ComponentTypeSettings> componentSettingsList = (List<ComponentTypeSettings>) event.getSource();
        for (ComponentTypeSettings componentSettings : componentSettingsList)
        {
          updateComponentTypeData(componentSettings);
        }
      }
  }
  
  /*
   * @author Kee Chin Seong - Switch Those Unloaded Component To Another Layer
   */
  private void updateNoLoadedComponentData(ComponentType componentType, ComponentTypeSettings componentSettings)
  {
    List<ComponentRenderer> compRenderers = new ArrayList<ComponentRenderer>();
    List<PadNameRenderer> padNameRenderers = new ArrayList<PadNameRenderer>();
    List<ReferenceDesignatorRenderer> refDesRenderers = new ArrayList<ReferenceDesignatorRenderer>();
    List<PadRenderer> padRenderers = new ArrayList<PadRenderer>();
    
    if(componentSettings.isLoaded() == true)
    {
      compRenderers = _noLoadComponentTypeToComponentRenderersMap.get(componentType);
      if(compRenderers == null)
          return;
      for (ComponentRenderer compRend : compRenderers)
      {
        padRenderers = _noLoadComponentRendererToPadRenderersMap.get(compRend);
        padNameRenderers = _noLoadComponentRendererToPadNameRendererMap.get(compRend);
        
        if (padRenderers != null) // could be null if pad creation threshold is not met (not zoomed in enough)
        {
          _graphicsEngine.removeRenderers(padRenderers);
          _graphicsEngine.removeRenderers(padNameRenderers);
        }
      }
      _noLoadComponentTypeToComponentRenderersMap.remove(componentType);
      _graphicsEngine.removeRenderers(compRenderers);
      List<ReferenceDesignatorRenderer> compRefDesRenderers = _noLoadComponentTypeToReferenceDesignatorRenderersMap.get(componentType);

      if(compRefDesRenderers != null)
       _graphicsEngine.removeRenderers(compRefDesRenderers);
      
      _noLoadComponentTypeToReferenceDesignatorRenderersMap.remove(componentType);
      _noLoadComponentTypeToComponentRenderersMap.remove(compRenderers);
      _noLoadComponentRendererToPadRenderersMap.remove(compRenderers);
    }
    else
    {
       for (com.axi.v810.business.panelDesc.Component component : componentType.getComponents())
       {   
         if(component.getReferenceDesignator().equals(componentType.getReferenceDesignator()))
         {    
            ComponentRenderer compRenderer = new ComponentRenderer(this, component);
            ReferenceDesignatorRenderer referenceDesignatorRenderer = new ReferenceDesignatorRenderer(component);

            compRenderers.add(compRenderer);
            refDesRenderers.add(referenceDesignatorRenderer);
            
            _noLoadComponentTypeToComponentRenderersMap.put(componentType, compRenderers);
            _noLoadComponentTypeToReferenceDesignatorRenderersMap.put(componentType, refDesRenderers);

            SideBoard sideBoard = component.getSideBoard();

            List<Pad> pads = component.getPads();

            for (Pad pad : pads)
            {
              PadRenderer padRenderer = new PadRenderer(pad, compRenderer);
              PadNameRenderer padNameRenderer = new PadNameRenderer(pad, false, false);

              padRenderers.add(padRenderer);
              padNameRenderers.add(padNameRenderer);
              
              if (sideBoard.isTopSide())
              {
                _graphicsEngine.addRenderer(_topNoLoadPadLayer, padRenderer);
              }
              else
              {
                _graphicsEngine.addRenderer(_bottomNoLoadPadLayer, padRenderer);  
              }
              _noLoadComponentRendererToPadRenderersMap.put(compRenderer, padRenderers);
              _noLoadComponentRendererToPadNameRendererMap.put(compRenderer, padNameRenderers);                      
            }

            if (sideBoard.isTopSide())
            {
              _graphicsEngine.addRenderer(_topNoLoadedComponentLayer, compRenderer);
              _graphicsEngine.addRenderer(_topNoLoadRefDesignatorLayer, referenceDesignatorRenderer);
              if(_showNoLoadComponent == true && _graphicsEngine.getCurrentZoomFactor() >= 2.0)
                  _graphicsEngine.setVisible(_topNoLoadRefDesignatorLayer, true);
              else
                  _graphicsEngine.setVisible(_topNoLoadRefDesignatorLayer, false);
            }
            else if (sideBoard.isBottomSide())
            {
              _graphicsEngine.addRenderer(_bottomNoLoadedComponentLayer, compRenderer);
              _graphicsEngine.addRenderer(_bottomNoLoadRefDesignatorLayer, referenceDesignatorRenderer);
              if(_showNoLoadComponent == true && _graphicsEngine.getCurrentZoomFactor() >= 2.0 )
                 _graphicsEngine.setVisible(_bottomNoLoadRefDesignatorLayer, true);
              else
                _graphicsEngine.setVisible(_bottomNoLoadRefDesignatorLayer, false);
            }
            else
              Assert.expect(false);
          }
       } 
     }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void updateComponentTypeData(ComponentTypeSettings componentSettings)
  {
    Assert.expect(componentSettings != null);
    
    ComponentType componentType = componentSettings.getComponentType();

    if (componentSettings.isLoaded() == false)
    {
           // XCR-3173 Software crashed when upload a new variation with a new recipe saved from a recipe with variation
      // When no loaded component is already under the no load map DO not let it go through this process!
      if (_noLoadComponentTypeToComponentRenderersMap.get(componentType) == null && _componentTypeToComponentRenderersMap.get(componentType) != null)
      {
        List<ComponentRenderer> compRenderers = _componentTypeToComponentRenderersMap.get(componentType);

        for (ComponentRenderer compRend : compRenderers)
        {
          List<PadRenderer> padRenderers = _componentRendererToPadRenderersMap.get(compRend);
          if (padRenderers != null) // could be null if pad creation threshold is not met (not zoomed in enough)
          {
            _graphicsEngine.removeRenderers(padRenderers);
            //componentRendererToPadRenderersMapRemove(compRend);
          }
        }
        _componentTypeToComponentRenderersMap.remove(componentType);
        _graphicsEngine.removeRenderers(compRenderers);
        List<ReferenceDesignatorRenderer> compRefDesRenderers = _componentTypeToReferenceDesignatorRenderersMap.get(componentType);

        if (compRefDesRenderers != null)
        {
          _graphicsEngine.removeRenderers(compRefDesRenderers);
        }
        _componentTypeToReferenceDesignatorRenderersMap.remove(componentType);
        _componentRendererToPadRenderersMap.remove(compRenderers);
      }
    }
    else
    {
      List<ComponentRenderer> compRenderers = new ArrayList<ComponentRenderer>();
      for (com.axi.v810.business.panelDesc.Component component : componentType.getComponents())
      {
        if (component.getReferenceDesignator().equals(componentType.getReferenceDesignator()))
        {
          List<ReferenceDesignatorRenderer> compRefDesRenderers = new ArrayList<ReferenceDesignatorRenderer>();
          List<PadRenderer> padRenderers = new ArrayList<PadRenderer>();

          ComponentRenderer compRenderer = new ComponentRenderer(this, component);
          ReferenceDesignatorRenderer referenceDesignatorRenderer = new ReferenceDesignatorRenderer(component);
          compRenderers.add(compRenderer);
          compRefDesRenderers.add(referenceDesignatorRenderer);
          SideBoard sideBoard = component.getSideBoard();
                 // do not create pad renderers unless the pad layers are currently visible on screen

          if (arePadsVisible(sideBoard.isTopSide()))
          {
            List<Pad> pads = component.getPads();

            for (Pad pad : pads)
            {
              PadRenderer padRenderer = new PadRenderer(pad, compRenderer);
              padRenderers.add(padRenderer);
              Integer layerInt = (Integer) _boardToTopPadLayerMap.get(component.getBoard());
              _graphicsEngine.addRenderer(layerInt.intValue(), padRenderer);
              //_graphicsEngine.add(padRenderer);
            }
            //componentRendererToPadRenderersMapPut(compRenderer, padRenderers);
          }
          Integer layerInt = null;
          if (sideBoard.isTopSide())
          {
            layerInt = (Integer) _boardToTopComponentLayerMap.get(sideBoard.getBoard());
            _graphicsEngine.addRenderer(layerInt.intValue(), compRenderer);
            layerInt = (Integer) _boardToTopRefDesLayerMap.get(sideBoard.getBoard());
            _graphicsEngine.addRenderer(layerInt.intValue(), referenceDesignatorRenderer);
            //_graphicsEngine.validateData(getTopPadLayers());
          }
          else if (sideBoard.isBottomSide())
          {
            layerInt = (Integer) _boardToBottomComponentLayerMap.get(sideBoard.getBoard());
            _graphicsEngine.addRenderer(layerInt.intValue(), compRenderer);
            layerInt = (Integer) _boardToBottomRefDesLayerMap.get(sideBoard.getBoard());
            _graphicsEngine.addRenderer(layerInt.intValue(), referenceDesignatorRenderer);
            //_graphicsEngine.validateData(getBottomPadLayers());
          }
          else
          {
            Assert.expect(false);
          }

          _componentTypeToComponentRenderersMap.put(componentType, compRenderers);
          _componentTypeToReferenceDesignatorRenderersMap.put(componentType, compRefDesRenderers);
          _componentRendererToPadRenderersMap.put(compRenderer, padRenderers);

        }
               //_graphicsEngine.validateData(getComponentLayers());
        //_graphicsEngine.validateData(getReferenceDesignatorLayers());     
        _graphicsEngine.validateData(getPadLayers());
      }
    }
    updateNoLoadedComponentData(componentType, componentSettings);
  }

  /**
   * update the board type data from the datastore
   * @author George A. David
   */
  private void updateBoardTypeData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getSource() instanceof BoardType);
    Assert.expect(event.getProjectChangeEventEnum() instanceof BoardTypeEventEnum);

    BoardTypeEventEnum eventEnum = (BoardTypeEventEnum)event.getProjectChangeEventEnum();
    BoardType boardType = (BoardType)event.getSource();
    Assert.expect(_boardTypeToBoardRenderersMap.containsKey(boardType), "board type " + boardType.getName() + " not in map");
    List<BoardRenderer> boardTypes = _boardTypeToBoardRenderersMap.get(boardType);
    for (BoardRenderer renderer : boardTypes)
    {
      renderer.validateData();
    }
//    boardType.ge

//    if (eventEnum.equals(eventEnum.COORDINATE)||
//       eventEnum.equals(eventEnum.DEGREES_ROTATION))
//    {
//      _graphicsEngine.validateData(getAllLayers(board));
//    }
//    else if (eventEnum.equals(eventEnum.FLIP))
//    {
//      flipBoard(board);
//    }
//    else if (eventEnum.equals(eventEnum.NAME))
//    {
//    }
    if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0))
    {
      _graphicsEngine.fitGraphicsToScreen();
    }


  }

  /**
   * @author George A. David
   * @author Laura Cormos
   */
  private void updateComponentData(ProjectChangeEvent event)
  {
//    Assert.expect(event != null);
//    Assert.expect(event.getSource() instanceof Component);
//    Assert.expect(event.getProjectChangeEventEnum() instanceof ComponentEventEnum);
//
//    ComponentEventEnum componentEventEnum = (ComponentEventEnum)event.getProjectChangeEventEnum();
//
//    if(UnitTest.unitTesting() == false)
//    {
//      switch (componentEventEnum.getId())
//      {
//        case 0:
//          System.out.println("Received component event CREATE_OR_DESTROY. Doing nothing about it.");
//          break;
//        case 1:
//          System.out.println("Received component event COMPONENT_TYPE. Doing nothing about it.");
//          break;
//        case 2:
//          System.out.println("Received component event SIDE_BOARD. Doing nothing about it.");
//          break;
//        case 3:
//          System.out.println("Received component event ADD_OR_REMOVE_PAD. Doing nothing about it.");
//          break;
//        default:
//          break;
//      }
//    }
  }

  /**
   * @author George A. David
   * @author Laura Cormos
   * @edited By Kee Chin Seong
   */
  private void updateComponentTypeData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    // Modified by Seng Yew on 11-Jan-2013 (Final on 27-Oct-2013)
    // XCR1559 - Able to delete multiple components at once
    // Source can be ComponentType or EditCadPanel
//    Assert.expect(event.getSource() instanceof ComponentType);
    Assert.expect(event.getProjectChangeEventEnum() instanceof ComponentTypeEventEnum);

    ComponentTypeEventEnum compTypeEventEnum = (ComponentTypeEventEnum)event.getProjectChangeEventEnum();
    Object newValue = event.getNewValue();
    Object oldValue = event.getOldValue();

    if (compTypeEventEnum.equals(ComponentTypeEventEnum.CREATE_OR_DESTROY) 
            || compTypeEventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE)
            || compTypeEventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST))
    {
      if (event.isAdded()|| event.isCreated())
      {
        //Ngie Xing, combine functions for adding single component and multiple components 
        java.util.List<ComponentType> componentTypeList = new ArrayList<ComponentType>();
        if (compTypeEventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST) == false)
        {
          Assert.expect(newValue instanceof ComponentType);
          componentTypeList.add((ComponentType) newValue);
        }
        else
        {
          componentTypeList.addAll((java.util.List<ComponentType>) newValue);
        }

        List<com.axi.v810.business.panelDesc.Component> components = new ArrayList<com.axi.v810.business.panelDesc.Component>();

        for (ComponentType componentType : componentTypeList)
        {
          components.addAll(componentType.getComponents());

          List<ComponentRenderer> compRenderers = new ArrayList<ComponentRenderer>();
          List<ReferenceDesignatorRenderer> compRefDesRenderers = new ArrayList<ReferenceDesignatorRenderer>();
          for (com.axi.v810.business.panelDesc.Component component : components)
          {
            ComponentRenderer compRenderer = new ComponentRenderer(this, component);
            ReferenceDesignatorRenderer referenceDesignatorRenderer = new ReferenceDesignatorRenderer(component);
            compRenderers.add(compRenderer);
            compRefDesRenderers.add(referenceDesignatorRenderer);
            SideBoard sideBoard = component.getSideBoard();
            
            //Ngie Xing, changed decision whether to add pad layers according to zoom level,
            // do not create pad renderers unless the pad layers are currently visible on screen
            Integer padLayerInt = null;
            if (sideBoard.isTopSide())
              padLayerInt = (Integer) _boardToTopPadLayerMap.get(component.getBoard());
            else if (sideBoard.isBottomSide())
              padLayerInt = (Integer) _boardToBottomPadLayerMap.get(component.getBoard());
            else
              Assert.expect(false);

            if (_graphicsEngine.isZoomLevelBelowCreationThreshold(padLayerInt) == false);// && arePadsVisible(sideBoard.isTopSide()))
            {
              List<Pad> pads = component.getPads();
              List<PadRenderer> padRenderers = new ArrayList<PadRenderer>();
              for (Pad pad : pads)
              {
                PadRenderer padRenderer = new PadRenderer(pad, compRenderer);
                padRenderers.add(padRenderer);
                _graphicsEngine.addRenderer(padLayerInt.intValue(), padRenderer);
              }
              componentRendererToPadRenderersMapPut(compRenderer, padRenderers);
            }

            Integer layerInt = null;
            if (sideBoard.isTopSide())
            {
              layerInt = (Integer) _boardToTopComponentLayerMap.get(sideBoard.getBoard());
              _graphicsEngine.addRenderer(layerInt.intValue(), compRenderer);
              layerInt = (Integer) _boardToTopRefDesLayerMap.get(sideBoard.getBoard());
              _graphicsEngine.addRenderer(layerInt.intValue(), referenceDesignatorRenderer);
            }
            else if (sideBoard.isBottomSide())
            {
              layerInt = (Integer) _boardToBottomComponentLayerMap.get(sideBoard.getBoard());
              _graphicsEngine.addRenderer(layerInt.intValue(), compRenderer);
              layerInt = (Integer) _boardToBottomRefDesLayerMap.get(sideBoard.getBoard());
              _graphicsEngine.addRenderer(layerInt.intValue(), referenceDesignatorRenderer);
            }
            else
            {
              Assert.expect(false);
            }
          }
          components.clear();
          _componentTypeToComponentRenderersMap.put(componentType, compRenderers);
          _componentTypeToReferenceDesignatorRenderersMap.put(componentType, compRefDesRenderers);
        }
      }
      else if (event.isDestroyed() || event.isRemoved())
      {
        //Ngie Xing, combine functions for deleting single component and multiple components 
        java.util.List<ComponentType> componentTypeList = new ArrayList<ComponentType>();
        if (compTypeEventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST) == false)
        {
          Assert.expect(oldValue instanceof ComponentType);
          componentTypeList.add((ComponentType) oldValue);
        }
        else
        {
          componentTypeList.addAll((java.util.List<ComponentType>) oldValue);
        }

        for (ComponentType componentTypeTemp : componentTypeList)
        {
          List<ComponentRenderer> compRendererList = _componentTypeToComponentRenderersMap.get(componentTypeTemp);
          List<ReferenceDesignatorRenderer> compRefDesRenderers = _componentTypeToReferenceDesignatorRenderersMap.get(componentTypeTemp);

          if (compRendererList != null)
          {
            for (ComponentRenderer compRend : compRendererList)
            {
              List<PadRenderer> padRenderers = _componentRendererToPadRenderersMap.get(compRend);
              if (padRenderers != null) // could be null if pad creation threshold is not met (not zoomed in enough)
              {
                for (PadRenderer padRenderer : padRenderers)
                {
                  if (_graphicsEngine.getLayerNumberForRenderer(padRenderer) != -1)
                    _graphicsEngine.removeRenderer(padRenderer);
                }
                padRenderers.clear();

                componentRendererToPadRenderersMapRemove(compRend);
              }
              if (_graphicsEngine.getLayerNumberForRenderer(compRend) != -1)
              {
                _graphicsEngine.removeRenderer(compRend);
                _graphicsEngine.removeRendererFromCurrentlySelectedRendererSet(compRend); //Ngie Xing
              }
            }
          }
          
          if (compRefDesRenderers != null)
          {
            for (ReferenceDesignatorRenderer compRefDes : compRefDesRenderers)
            {
              if (_graphicsEngine.getLayerNumberForRenderer(compRefDes) != -1)
                _graphicsEngine.removeRenderer(compRefDes);
            }
          }
          _componentTypeToComponentRenderersMap.remove(componentTypeTemp);
          _componentTypeToReferenceDesignatorRenderersMap.remove(componentTypeTemp);
        }
      }
      else
      {
        Assert.expect(false, "Both oldValue and newValue for ComponentTypeEventEnum.CREATE_OR_DESTROY are null!!");
        return;
      }
      _graphicsEngine.validateData(getComponentLayers());
      _graphicsEngine.validateData(getReferenceDesignatorLayers());
      _graphicsEngine.validateData(getPadLayers());
    }
    else if (compTypeEventEnum.equals(ComponentTypeEventEnum.CHANGE_SIDE))
    {
      ComponentType componentType = (ComponentType)event.getSource();
      
      // XCR-3138 Kee Chin Seong - Assert when change side for component 
      if(componentType.isLoaded() == false)
        return;
      
      List<ComponentRenderer> compRenderers = _componentTypeToComponentRenderersMap.get(componentType);
      Assert.expect(compRenderers != null && compRenderers.size() > 0);
      List<ComponentRenderer> newCompRenderers = new ArrayList<ComponentRenderer>();
      // flip all component renderers from whatever layer they're on to the opposite layer (top/bottom)
      for (ComponentRenderer compRenderer : compRenderers)
      {
        Integer layerNumber = null;
        Component component = compRenderer.getComponent();

        List<PadRenderer> padRenderers = _componentRendererToPadRenderersMap.get(compRenderer);
        // the component's side board is where it was moved to, therefore we'll take the renderers from the bottom
        // layers and put them in the top layers
        if (component.getSideBoard().isTopSide())
        {
          _graphicsEngine.removeRenderer(compRenderer);
          layerNumber = _boardToTopComponentLayerMap.get(component.getBoard());
          Assert.expect(layerNumber != null);
          ComponentRenderer newCompRenderer = new ComponentRenderer(this, component);
          newCompRenderers.add(newCompRenderer);
          _graphicsEngine.addRenderer(layerNumber.intValue(), newCompRenderer);
          // change the pad Renderers layer as well, if visible on screen
          if (padRenderers != null)
          {
            List<PadRenderer> newPadRenderers = new ArrayList<PadRenderer>();
            _graphicsEngine.removeRenderers(padRenderers);
            layerNumber = _boardToTopPadLayerMap.get(component.getBoard());
            Assert.expect(layerNumber != null);
            for (PadRenderer padRenderer : padRenderers)
            {
              PadRenderer newPadRenderer = new PadRenderer(padRenderer.getPad(), newCompRenderer);
              newPadRenderers.add(newPadRenderer);
              _graphicsEngine.addRenderer(layerNumber.intValue(), newPadRenderer);
            }
            componentRendererToPadRenderersMapRemove(compRenderer);
            componentRendererToPadRenderersMapPut(newCompRenderer, newPadRenderers);
          }
        }
        else // the component's new side is not facing x-rays (is on the bottom) so we'll move the component's renderers
         // from the top layers and put it in the bottom layers
        {
          _graphicsEngine.removeRenderer(compRenderer);
          layerNumber = _boardToBottomComponentLayerMap.get(component.getBoard());
          Assert.expect(layerNumber != null);
          ComponentRenderer newCompRenderer = new ComponentRenderer(this, component);
          newCompRenderers.add(newCompRenderer);
          _graphicsEngine.addRenderer(layerNumber.intValue(), newCompRenderer);
          // change the pad Renderers layer as well, if visible on screen
          if (padRenderers != null)
          {
            List<PadRenderer> newPadRenderers = new ArrayList<PadRenderer>();
            _graphicsEngine.removeRenderers(padRenderers);
            layerNumber = _boardToBottomPadLayerMap.get(component.getBoard());
            Assert.expect(layerNumber != null);
            for (PadRenderer padRenderer : padRenderers)
            {
              PadRenderer newPadRenderer = new PadRenderer(padRenderer.getPad(), newCompRenderer);
              newPadRenderers.add(newPadRenderer);
              _graphicsEngine.addRenderer(layerNumber.intValue(), newPadRenderer);
            }
            componentRendererToPadRenderersMapRemove(compRenderer);
            componentRendererToPadRenderersMapPut(newCompRenderer, newPadRenderers);
          }
        }
      }
      _componentTypeToComponentRenderersMap.put(componentType, newCompRenderers);
      // flip all component reference designator renderers from whatever layer they're on to the opposite layer (top/bottom)
      List<ReferenceDesignatorRenderer> refDesRenderers = _componentTypeToReferenceDesignatorRenderersMap.get(componentType);
      List<ReferenceDesignatorRenderer> newRefDesRenderers = new ArrayList<ReferenceDesignatorRenderer>();
      for (ReferenceDesignatorRenderer refDesRenderer : refDesRenderers)
      {
        Integer layerNumber = null;
        Component component = refDesRenderer.getComponent();
        // the component's side board is where it was moved to, therefore we'll take the renderers from the bottom
        // layers and put it in the top layers
        if (component.getSideBoard().isTopSide())
        {
          _graphicsEngine.removeRenderer(refDesRenderer);
          layerNumber = _boardToTopRefDesLayerMap.get(component.getBoard());
          Assert.expect(layerNumber != null);
          ReferenceDesignatorRenderer newRefDesRend = new ReferenceDesignatorRenderer(component);
          newRefDesRenderers.add(newRefDesRend);
          _graphicsEngine.addRenderer(layerNumber.intValue(), newRefDesRend);
        }
        // the component's new side is not facing x-rays (is on the bottom) so we'll move the component's renderers
        // from the top layers and put it in the bottom layers
        else
        {
          _graphicsEngine.removeRenderer(refDesRenderer);
          layerNumber = _boardToBottomRefDesLayerMap.get(component.getBoard());
          Assert.expect(layerNumber != null);
          ReferenceDesignatorRenderer newRefDesRend = new ReferenceDesignatorRenderer(component);
          newRefDesRenderers.add(newRefDesRend);
          _graphicsEngine.addRenderer(layerNumber.intValue(), newRefDesRend);
        }
      }
      _componentTypeToReferenceDesignatorRenderersMap.put(componentType, newRefDesRenderers);

      _graphicsEngine.validateData(getComponentLayers());
      _graphicsEngine.validateData(getReferenceDesignatorLayers());
      _graphicsEngine.validateData(getPadLayers());
      if (newCompRenderers.isEmpty() == false)
      {
        com.axi.guiUtil.Renderer firstComponent = newCompRenderers.get(0);
        _graphicsEngine.selectWithCrossHairs(firstComponent);
      }
      _controlToolBar.showBothSides();
    }
    else // for any other component type event, just repaint the component, its refDes and its pads, if visible
    {
      ComponentType componentType = (ComponentType)event.getSource();
      
      if (componentType != null && componentType.isLoaded() == true)
      {
        Assert.expect(_componentTypeToComponentRenderersMap.containsKey(componentType));
        Assert.expect(_componentTypeToReferenceDesignatorRenderersMap.containsKey(componentType));

        List<ComponentRenderer> compRenderers = _componentTypeToComponentRenderersMap.get(componentType);
        for (ComponentRenderer compRenderer : compRenderers)
        {
          List<PadRenderer> padRenderers = _componentRendererToPadRenderersMap.get(compRenderer);
          // re-paint the component's pads only if created at this time
          if (padRenderers != null)
            for (PadRenderer padRenderer : padRenderers)
            {
              padRenderer.validateData();
            }
          for (ReferenceDesignatorRenderer refDesRend : _componentTypeToReferenceDesignatorRenderersMap.get(componentType))
          {
            if (refDesRend.getComponent().equals(compRenderer.getComponent()))
            {
              // re-paint the reference designator
              refDesRend.validateData();
              break; // move on once the refDesRend for current component was found
            }
          }
          // re-paint the component
          compRenderer.validateData();
        }
        if (compTypeEventEnum.equals(ComponentTypeEventEnum.REFERENCE_DESIGNATOR))
          setStatusBarDescription(compRenderers.get(0));
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void updateFiducialData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getSource() instanceof Fiducial);
    Assert.expect(event.getProjectChangeEventEnum() instanceof FiducialEventEnum);

    FiducialEventEnum fiducialEventEnum = (FiducialEventEnum)event.getProjectChangeEventEnum();
    Object newValue = event.getNewValue();
    Object oldValue = event.getOldValue();

    if (fiducialEventEnum.equals(FiducialEventEnum.CREATE_OR_DESTROY) || fiducialEventEnum.equals(FiducialEventEnum.ADD_OR_REMOVE))
    {
      if (event.isAdded()|| event.isCreated())
      {
        Assert.expect(newValue instanceof Fiducial);
//        if (UnitTest.unitTesting() == false)
//          System.out.println("Creating renderer for new panel fiducial");
        Fiducial fiducial = (Fiducial)newValue;
        Assert.expect(fiducial.isOnPanel());
        // add the fiducial renderer on the panel side where it was created
        if (fiducial.isTopSide())
        {
          _graphicsEngine.addRenderer(_topPanelFiducialLayer, new FiducialRenderer(fiducial));
          _graphicsEngine.addRenderer(_topPanelFiducialRefDesLayer, new FiducialReferenceDesignatorRenderer(fiducial));
        }
        else
        {
          _graphicsEngine.addRenderer(_bottomPanelFiducialLayer, new FiducialRenderer(fiducial));
          _graphicsEngine.addRenderer(_bottomPanelFiducialRefDesLayer,
                                      new FiducialReferenceDesignatorRenderer(fiducial));
        }
        _graphicsEngine.validateData(getFiducialLayers());
        _graphicsEngine.validateData(getReferenceDesignatorLayers());
      }
      else if (event.isDestroyed() || event.isRemoved())
      {
        Assert.expect(oldValue instanceof Fiducial);
//        if (UnitTest.unitTesting() == false)
//          System.out.println("Removing renderer for destroyed panel fiducial");
        Fiducial fiducial = (Fiducial)oldValue;
        Assert.expect(fiducial.isOnPanel());
        if (isTopSideVisible())
        {
          for (com.axi.guiUtil.Renderer fidRenderer : _graphicsEngine.getRenderers(_topPanelFiducialLayer))
          {
            if (((FiducialRenderer)fidRenderer).getFiducial() == fiducial)
              _graphicsEngine.removeRenderer(fidRenderer);
          }
          for (com.axi.guiUtil.Renderer fidRefDesRenderer :
               _graphicsEngine.getRenderers(_topPanelFiducialRefDesLayer))
          {
            if (((FiducialReferenceDesignatorRenderer)fidRefDesRenderer).getFiducial() == fiducial)
              _graphicsEngine.removeRenderer(fidRefDesRenderer);
          }
        }
        else
        {
          for (com.axi.guiUtil.Renderer fidRenderer : _graphicsEngine.getRenderers(_bottomPanelFiducialLayer))
          {
            if (((FiducialRenderer)fidRenderer).getFiducial() == fiducial)
              _graphicsEngine.removeRenderer(fidRenderer);
          }
          for (com.axi.guiUtil.Renderer fidRefDesRenderer :
               _graphicsEngine.getRenderers(_bottomPanelFiducialRefDesLayer))
          {
            if (((FiducialReferenceDesignatorRenderer)fidRefDesRenderer).getFiducial() == fiducial)
              _graphicsEngine.removeRenderer(fidRefDesRenderer);
          }
        }
        _drawCadPanel.setDescription("");
      }
      else
      {
        Assert.expect(false, "Both newValue and oldValue are null for FiducialEventEnum.CREATE_OR_DESTROY event.");
        return;
      }
    }
    else
    {
      _graphicsEngine.validateData(getPanelFiducialLayers());
      _graphicsEngine.validateData(getPanelFiducialRefDesLayers());
    }
  }

  /**
   * @author Laura Cormos
   */
  private void updateFiducialTypeData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getSource() instanceof FiducialType);
    Assert.expect(event.getProjectChangeEventEnum() instanceof FiducialTypeEventEnum);

    FiducialTypeEventEnum fiducialTypeEventEnum = (FiducialTypeEventEnum)event.getProjectChangeEventEnum();
    Object newValue = event.getNewValue();
    Object oldValue = event.getOldValue();

    if (fiducialTypeEventEnum.equals(FiducialTypeEventEnum.CREATE_OR_DESTROY) || fiducialTypeEventEnum.equals(FiducialTypeEventEnum.ADD_OR_REMOVE))
    {
      if (event.isAdded()|| event.isCreated())
      {
        Assert.expect(newValue instanceof FiducialType);
//        if (UnitTest.unitTesting() == false)
//          System.out.println("Creating renderers for new fiducial type");
        FiducialType fiducialType = (FiducialType)newValue;
        List<Fiducial> fiducials = fiducialType.getFiducials();
        for (Fiducial fiducial : fiducials)
        {
          // this event can only be generated by a board fiducial
          Assert.expect(fiducial.isOnBoard());
          SideBoard sideBoard = fiducial.getSideBoard();
          Integer layerInt = null;
          if (sideBoard.isTopSide())
          {
            layerInt = (Integer)_boardToTopFiducialLayerMap.get(sideBoard.getBoard());
            _graphicsEngine.addRenderer(layerInt.intValue(), new FiducialRenderer(fiducial));
            layerInt = (Integer)_boardToTopRefDesLayerMap.get(sideBoard.getBoard());
            _graphicsEngine.addRenderer(layerInt.intValue(), new FiducialReferenceDesignatorRenderer(fiducial));
          }
          else if (sideBoard.isBottomSide())
          {
            layerInt = (Integer)_boardToBottomFiducialLayerMap.get(sideBoard.getBoard());
            _graphicsEngine.addRenderer(layerInt.intValue(), new FiducialRenderer(fiducial));
            layerInt = (Integer)_boardToBottomRefDesLayerMap.get(sideBoard.getBoard());
            _graphicsEngine.addRenderer(layerInt.intValue(), new FiducialReferenceDesignatorRenderer(fiducial));
          }
        }
      }
      else if (event.isDestroyed() || event.isRemoved())
      {
        Assert.expect(oldValue instanceof FiducialType);
//        if (UnitTest.unitTesting() == false)
//          System.out.println("Removing renderers for destroyed fiducial type");
        FiducialType fiducialType = (FiducialType)oldValue;
        List<Fiducial> fiducials = fiducialType.getFiducials();
        for (Fiducial fiducial : fiducials)
        {
          // this event can only be generate by a board fiducial
          Assert.expect(fiducial.isOnBoard());
          SideBoard sideBoard = fiducial.getSideBoard();
          Integer layerInt = null;
          if (sideBoard.isTopSide())
          {
            layerInt = (Integer)_boardToTopFiducialLayerMap.get(sideBoard.getBoard());
            for (com.axi.guiUtil.Renderer fidRenderer : _graphicsEngine.getRenderers(layerInt))
            {
              if (((FiducialRenderer)fidRenderer).getFiducial() == fiducial)
                _graphicsEngine.removeRenderer(fidRenderer);
            }
            layerInt = (Integer)_boardToTopRefDesLayerMap.get(sideBoard.getBoard());
            for (com.axi.guiUtil.Renderer fidRefDesRenderer : _graphicsEngine.getRenderers(layerInt))
            {
              if (fidRefDesRenderer instanceof FiducialReferenceDesignatorRenderer &&
                  ((FiducialReferenceDesignatorRenderer)fidRefDesRenderer).getFiducial() == fiducial)
                _graphicsEngine.removeRenderer(fidRefDesRenderer);
            }
          }
          else if (sideBoard.isBottomSide())
          {
            layerInt = (Integer)_boardToBottomFiducialLayerMap.get(sideBoard.getBoard());
            for (com.axi.guiUtil.Renderer fidRenderer : _graphicsEngine.getRenderers(layerInt))
            {
              if (((FiducialRenderer)fidRenderer).getFiducial() == fiducial)
                _graphicsEngine.removeRenderer(fidRenderer);
            }
            layerInt = (Integer)_boardToBottomRefDesLayerMap.get(sideBoard.getBoard());
            for (com.axi.guiUtil.Renderer fidRefDesRenderer : _graphicsEngine.getRenderers(layerInt))
            {
              if (fidRefDesRenderer instanceof FiducialReferenceDesignatorRenderer &&
                  ((FiducialReferenceDesignatorRenderer)fidRefDesRenderer).getFiducial() == fiducial)
                _graphicsEngine.removeRenderer(fidRefDesRenderer);
            }
          }
        }
      }
      else
      {
        Assert.expect(false,
                      "Both oldValue and newValue for FiducialTypeEventEnum.CREATE_OR_DESTROY are null!!");
        return;
      }
    }
    _graphicsEngine.validateData(getFiducialLayers());
    _graphicsEngine.validateData(getReferenceDesignatorLayers());
    if (fiducialTypeEventEnum.equals(FiducialTypeEventEnum.NAME))
    {
      FiducialType fiducialType = (FiducialType)event.getSource();
      setStatusBarDescription(findFiducialRenderers(fiducialType).get(0));
    }
  }

  /**
   * @author Laura Cormos
   */
  private void updateLandPatternPadData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    if(event.getSource() instanceof LandPatternPad)
    {
      Assert.expect(event.getProjectChangeEventEnum() instanceof LandPatternPadEventEnum);

      Object oldValue = event.getOldValue();
      Object newValue = event.getNewValue();

      LandPatternPadEventEnum landPaternPadEventEnum = (LandPatternPadEventEnum)event.getProjectChangeEventEnum();
      LandPatternPad landPatternPad = null;
  //    System.out.println("Servicing a LPP event, GE contents:");
  //    printRenderers();
  //    System.out.println("Servicing a LPP event, variables contents:");
  //    printCTtoCRMap();
  //    printCRtoPRMap();
      // revalidate the component's shape if any land pattern pad is being created or destroyed
      if (landPaternPadEventEnum.equals(LandPatternPadEventEnum.CREATE_OR_DESTROY) ||
          landPaternPadEventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE))
      {
        if (event.isDestroyed() || event.isRemoved())
        {
          Assert.expect(oldValue instanceof LandPatternPad);
          landPatternPad = (LandPatternPad)oldValue;
  //        System.out.println("Event is LPP destroyed/removed");
        }
        else if (event.isAdded()|| event.isCreated())
        {
          Assert.expect(newValue instanceof LandPatternPad);
          landPatternPad = (LandPatternPad)newValue;
  //        System.out.println("Event is LPP created/added");
        }
        updateSelectedLandPatternPadData(landPatternPad, event);
      }
      // for any other changes to a land pattern pad just refresh the graphics
      else
      {
  //      System.out.println("Event is " + landPaternPadEventEnum.getId());
        landPatternPad = (LandPatternPad)event.getSource();
        java.util.List<ComponentType> compTypes = landPatternPad.getLandPattern().getComponentTypesUnsorted();
        for (ComponentType compType : compTypes)
        {
          List<ComponentRenderer> compRenderers = _componentTypeToComponentRenderersMap.get(compType);
          if(compRenderers != null && compRenderers.isEmpty() == false)
          {
            for (ComponentRenderer compRend : compRenderers)
            {
              compRend.validateData();
              List<PadRenderer> padRenderers = _componentRendererToPadRenderersMap.get(compRend);
              if (padRenderers != null)
              {
                for (PadRenderer padRend : padRenderers)
                {
                  if (padRend.getPad().getLandPatternPad().equals(landPatternPad))
                    padRend.validateData();
                }
              }
            }
          }
        }
      }
    }
    else if(event.getSource() instanceof ArrayList )
    {
      Assert.expect(event.getProjectChangeEventEnum() instanceof LandPatternPadEventEnum);

      LandPatternPadEventEnum landPaternPadEventEnum = (LandPatternPadEventEnum)event.getProjectChangeEventEnum();

      if(landPaternPadEventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE_LIST))
      {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        java.util.List<LandPatternPad> landPatternPads = null;

        if (event.isDestroyed() || event.isRemoved())
        {
          Assert.expect(oldValue instanceof ArrayList);
          landPatternPads = (java.util.ArrayList<LandPatternPad>)oldValue;
  //        System.out.println("Event is LPP destroyed/removed");
        }
        else if (event.isAdded()|| event.isCreated())
        {
          Assert.expect(newValue instanceof ArrayList);
          landPatternPads = (java.util.ArrayList<LandPatternPad>) newValue;
  //        System.out.println("Event is LPP created/added");
        }
        else
        {
          return;
        }
        for(LandPatternPad landPatternPad : landPatternPads)
        {
          updateSelectedLandPatternPadData(landPatternPad, event);
        }         
      }
    }
//    System.out.println("Finished Servicing a LPP event, GE contents:");
//    printRenderers();
//    System.out.println("Finished Servicing a LPP event, variables contents:");
//    printCTtoCRMap();
//    printCRtoPRMap();
  }

  private void updateSelectedLandPatternPadData(LandPatternPad landPatternPad, ProjectChangeEvent event)
  {
    java.util.List<ComponentType> compTypes = landPatternPad.getLandPattern().getComponentTypesUnsorted();
    for (ComponentType compType : compTypes)
    {
      List<ComponentRenderer> compRenderers = _componentTypeToComponentRenderersMap.get(compType);
      if (compRenderers != null)
      {
        for (ComponentRenderer compRend : compRenderers)
        {
          if (event.isCreated() || event.isAdded())
          {
            PadRenderer padRend = null;
            Component component = compRend.getComponent();
            List<Pad> pads = component.getPads();
            Board board = component.getBoard();
            Pad thePad = null;
            for (Pad pad : pads)
            {
              if (pad.getLandPatternPad() == landPatternPad)
              {
                thePad = pad;
                break;
              }
            }
            Assert.expect(thePad != null);
            padRend = new PadRenderer(thePad, compRend);
            boolean padRendExists = false;
            List<PadRenderer> newPadRenderersList = _componentRendererToPadRenderersMap.get(compRend);
            // add the new pad renderer only if is not already present (don't want duplicates)
            if (newPadRenderersList != null)
            {
              for (PadRenderer padRenderer : newPadRenderersList)
              {
                if (padRenderer.getPad() == thePad)
                {
                  padRendExists = true;
                  break;
                }
              }
              if (padRendExists == false)
              {
                newPadRenderersList.add(padRend);
              }
              if (component.getSideBoard().isTopSide())
              {
                //                System.out.println("Adding pad renderer for pad: " + padRend.getPad().getName() +
                //                                   " of LP: " + padRend.getPad().getLandPatternPad().getLandPattern().getName());
                _graphicsEngine.addRenderer(getTopPadLayer(board), padRend);
              }
              else
              {
                //                System.out.println("Adding pad renderer for pad: " + padRend.getPad().getName() +
                //                                   " of LP: " + padRend.getPad().getLandPatternPad().getLandPattern().getName());
                _graphicsEngine.addRenderer(getBottomPadLayer(board), padRend);
              }
              if (landPatternPad instanceof ThroughHoleLandPatternPad)
              {
                PadRenderer theOtherPadRend = new PadRenderer(thePad, compRend);
                if (padRendExists == false)
                {
                  newPadRenderersList.add(theOtherPadRend);
                }
                if (component.getSideBoard().isTopSide())
                {
                  //                  System.out.println("Adding pad renderer for pad: " + theOtherPadRend.getPad().getName() +
                  //                                     " of LP: " + theOtherPadRend.getPad().getLandPatternPad().getLandPattern().getName());
                  _graphicsEngine.addRenderer(getBottomPadLayer(board), theOtherPadRend);
                }
                else
                {
                  //                  System.out.println("Adding pad renderer for pad: " + theOtherPadRend.getPad().getName() +
                  //                                     " of LP: " + theOtherPadRend.getPad().getLandPatternPad().getLandPattern().getName());
                  _graphicsEngine.addRenderer(getTopPadLayer(board), theOtherPadRend);
                }
                _graphicsEngine.validateData(getBottomPadLayer(board));
                _graphicsEngine.validateData(getTopPadLayer(board));
              }
            }
            componentRendererToPadRenderersMapPut(compRend, newPadRenderersList);
          }
          else if (event.isDestroyed() || event.isRemoved())
          {
            List<PadRenderer> padRenderers = _componentRendererToPadRenderersMap.get(compRend);
            List<PadRenderer> removedPadRenderers = new ArrayList<PadRenderer>();
            if (padRenderers != null)
            {
              for (PadRenderer padRend : padRenderers)
              {
                if (padRend.getPad().getLandPatternPad().equals(landPatternPad))
                {
                  //                  System.out.println("Removing pad renderer for pad: " + padRend.getPad().getName() +
                  //                                     " of LP: " + padRend.getPad().getLandPatternPad().getLandPattern().getName());
                  _graphicsEngine.removeRenderer(padRend);
                  removedPadRenderers.add(padRend);
                }
              }
              padRenderers.removeAll(removedPadRenderers);
              // replace the list of pad renderers for this component renderer after removing the deleted ones.
              _componentRendererToPadRenderersMap.put(compRend, padRenderers);
            }
          }
          else
          {
            Assert.expect(false);
          }
          compRend.validateData();
        }
        List<ReferenceDesignatorRenderer> compRefDesRenderers = _componentTypeToReferenceDesignatorRenderersMap.get(compType);
        for (ReferenceDesignatorRenderer compRefDesRend : compRefDesRenderers)
        {
          compRefDesRend.validateData();
        }
      }
    }
  }
  
  /**
   * @author Laura Cormos
   * @Edited by Kee Chin Seong
   */
  private void updateThroughHoleLandPatternPadData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getSource() instanceof ThroughHoleLandPatternPad);
    Assert.expect(event.getProjectChangeEventEnum() instanceof ThroughHoleLandPatternPadEventEnum);

    ThroughHoleLandPatternPadEventEnum throughHoleLandPaternPadEventEnum = (ThroughHoleLandPatternPadEventEnum)event.getProjectChangeEventEnum();
    // refresh the hole graphics
    //Siew Yeng - XCR-3318 - Oval PTH
    if (throughHoleLandPaternPadEventEnum.equals(ThroughHoleLandPatternPadEventEnum.HOLE_LENGTH) || 
        throughHoleLandPaternPadEventEnum.equals(ThroughHoleLandPatternPadEventEnum.HOLE_WIDTH))
    {
      ThroughHoleLandPatternPad throughHoleLandPatternPad = (ThroughHoleLandPatternPad)event.getSource();
      java.util.List<ComponentType> compTypes = throughHoleLandPatternPad.getLandPattern().getComponentTypesUnsorted();
      for (ComponentType compType : compTypes)
      {
        List<ComponentRenderer> compRenderers = _componentTypeToComponentRenderersMap.get(compType);
        if(compRenderers != null)
        {   
            for (ComponentRenderer compRend : compRenderers)
            {
              List<PadRenderer> padRenderers = _componentRendererToPadRenderersMap.get(compRend);
              if (padRenderers != null)
              {
                for (PadRenderer padRend : padRenderers)
                {
                  if (padRend.getPad().getLandPatternPad().equals(throughHoleLandPatternPad))
                    padRend.validateData();
                }
              }
            }
        }
      }
    }
  }

  /**
   * @author George A. David
   * @author Laura Cormos
   */
  private void updatePanelData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getProjectChangeEventEnum() instanceof PanelEventEnum);
    PanelEventEnum eventEnum = (PanelEventEnum)event.getProjectChangeEventEnum();

    _graphicsEngine.validateData(getAllLayers());
    if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0))
    {
      _graphicsEngine.fitGraphicsToScreen();
    }

  }

  /**
   * @author George A. David
   */
  private void updatePanelSettingsData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getProjectChangeEventEnum() instanceof PanelSettingsEventEnum);

    PanelSettingsEventEnum panelSettingsEventEnum = (PanelSettingsEventEnum)event.getProjectChangeEventEnum();
    if ((panelSettingsEventEnum.equals(PanelSettingsEventEnum.LEFT_TO_RIGHT_FLIP)) ||
        (panelSettingsEventEnum.equals(PanelSettingsEventEnum.TOP_TO_BOTTOM_FLIP)))
    {
      // the selections must be clear before swapping layers or it loses track of
      // which layer needs to be on top
      _graphicsEngine.clearSelectedRenderers();
      flipPanel();
      if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0))
      {
        _graphicsEngine.fitGraphicsToScreen();
      }
    }
    else if (panelSettingsEventEnum.equals(PanelSettingsEventEnum.DEGREES_ROTATION))
    {
      _graphicsEngine.validateData(getAllLayers());
      if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0))
      {
        _graphicsEngine.fitGraphicsToScreen();
      }
    }
    else if(panelSettingsEventEnum.equals(PanelSettingsEventEnum.ALIGNMENT_METHOD))
    {
      repopulateLayersWithDebugAlignmentGraphics();
    }
    else if(panelSettingsEventEnum.equals(PanelSettingsEventEnum.SPLIT_SETTING))
    {
      repopulateSplitRenderer();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateProjectData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getProjectChangeEventEnum() instanceof ProjectEventEnum);

    ProjectEventEnum projectEventEnum = (ProjectEventEnum)event.getProjectChangeEventEnum();
    if (projectEventEnum.equals(projectEventEnum.DISPLAY_UNITS))
    {
      MathUtilEnum displayUnits = _panel.getProject().getDisplayUnits();
      double measureFactor = 1.0/MathUtil.convertUnits(1.0, displayUnits, MathUtilEnum.NANOMETERS);
      _graphicsEngine.setMeasurementInfo(measureFactor, MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits),
                                         MeasurementUnits.getMeasurementUnitString(displayUnits));
    }
    else if(projectEventEnum.equals(ProjectEventEnum.TEST_PROGRAM_GENERATED))
      repopulateLayersWithDebugGraphics();
    else if(projectEventEnum.equals(ProjectEventEnum.TEST_PROGRAM_UPDATED_WITH_ALIGNMENT_REGIONS))
      repopulateLayersWithDebugAlignmentGraphics();
  }

  /**
   * @author Laura Cormos
   */
  private void updateSideBoardTypeData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getProjectChangeEventEnum() instanceof SideBoardTypeEventEnum);

    SideBoardTypeEventEnum sideBoardTypeEventEnum = (SideBoardTypeEventEnum)event.getProjectChangeEventEnum();
    // either newValue or oldValue can be null, depending on the type of event generated
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateAlignmentRegionData()
  {
    if(_graphicsEngine.isLayerVisible(_alignmentRegionLayer))
       _graphicsEngine.validateData(_alignmentRegionLayer);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateAlignmentRegionData(boolean topRegion)
  {
//    _graphicsEngine.validateData(_alignmentRegionLayer);
    Collection<com.axi.guiUtil.Renderer> alignmentRenderers = _graphicsEngine.getRenderers(_alignmentRegionLayer);
    for(com.axi.guiUtil.Renderer renderer : alignmentRenderers)
    {
      if (renderer instanceof AlignmentRegionRenderer)
      {
        AlignmentRegionRenderer alignmentRegionRenderer = (AlignmentRegionRenderer)renderer;
        alignmentRegionRenderer.setTopRegion(topRegion);
        alignmentRegionRenderer.refreshData();
      }
      else if (renderer instanceof InactiveAlignmentRegionRenderer)
      {
        InactiveAlignmentRegionRenderer inactiveAlignmentRegionRenderer = (InactiveAlignmentRegionRenderer)renderer;
        inactiveAlignmentRegionRenderer.setTopRegion(topRegion);
        inactiveAlignmentRegionRenderer.refreshData();
      }
      else if (renderer instanceof AlignmentThroughputRenderer)
      {
        AlignmentThroughputRenderer alignmentThroughputRenderer = (AlignmentThroughputRenderer)renderer;
        alignmentThroughputRenderer.setTopRegion(topRegion);
        alignmentThroughputRenderer.refreshData();
      }

    }
    _graphicsEngine.validateData(_alignmentRegionLayer);
  }
  
  /**
   * @author George A. David
   * @Edited by Kee Chin Seong
   */
  private void handleGuiEvent(GuiEvent guiEvent)
  {
    Assert.expect(guiEvent != null);

    GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();

    if (guiEventEnum instanceof SelectionEventEnum)
    {
      //XCR-2207: Yellow crosshair that mark active components are not always ON
      _graphicsEngine.clearSelectedRenderersWithoutCrossHair();
      SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
      if (selectionEventEnum.equals(SelectionEventEnum.CLEAR_SELECTIONS))
      {
        //XCR-2207: Yellow crosshair that mark active components are not always ON
        _graphicsEngine.clearSelectedRenderersWithoutCrossHair();
      }
      else if (selectionEventEnum.equals(SelectionEventEnum.BOARD_INSTANCE))
      {
        /** Warning "unchecked cast" approved for cast from Object types.*/
        //Panel Settings
        List<Board> selectedBoards = (List<Board>)guiEvent.getSource();
        // if a single board is selected, remember it
        if (selectedBoards.size() == 1)
        {
          _selectedBoardName = selectedBoards.get(0).getName();
//          System.out.println("glb - _selectedBoardName = " + _selectedBoardName);
        }
        else
        {
          _selectedBoardName = null;
//          System.out.println("glb - _selectedBoardName = null");
        }
        List<Integer> boardLayers = new ArrayList<Integer>();
        for (Board board : selectedBoards)
        {
          boardLayers.addAll(getAllLayers(board));
        }

        Collection<com.axi.guiUtil.Renderer> renderers = _graphicsEngine.getRenderers(boardLayers);
        _graphicsEngine.setSelectedRenderers(renderers);
      }
      else if (selectionEventEnum.equals(SelectionEventEnum.COMPONENT_TYPE) ||
               selectionEventEnum.equals(SelectionEventEnum.PSH_NEIGHBOR_COMPONENT_SELECTION))//Siew Yeng - XCR-3781
      {
        /** Warning "unchecked cast" approved for cast from Object types.*/
        // modify subtype.
        List<ComponentType> components = (List<ComponentType>)guiEvent.getSource();
        if (components.isEmpty())
          return;
        else if(components.get(0).isLoaded() == false && _showNoLoadComponent == false)
         return;
        
        if(isTopSideVisible() && _showTopComponentLayer == false)
          return;
        else if(isBottomSideVisible() && _showBottomComponentLayer == false)
          return;
                
        else if (components.size() == 1)
        {
          ComponentType componentType = (ComponentType)components.get(0);
          if(componentType != null)
          {
            highlightComponentType(componentType); // highlight a single component different from multiple components
            
            //Siew Yeng - XCR-3781
            if(selectionEventEnum.equals(SelectionEventEnum.PSH_NEIGHBOR_COMPONENT_SELECTION))
            {
              for (ComponentRenderer componentRenderer : findComponentRenderers(componentType))
              {
                _graphicsEngine.setSelectedRenderer(componentRenderer);
              }
            }
            if(getComponentRenderers(componentType) != null)
              setStatusBarDescription(getComponentRenderers(componentType).get(0));
          }
        }
        else
          highlightMultipleComponents(components);

      }
      else if (selectionEventEnum.equals(SelectionEventEnum.FIDUCIAL_TYPE))
      {
        /** Warning "unchecked cast" approved for cast from Object types.*/
        List<FiducialType> fiducials = (List<FiducialType>)guiEvent.getSource();
        if (fiducials.isEmpty())
          return;
        else if (fiducials.size() == 1)
        {
          FiducialType fiducialType = (FiducialType)fiducials.get(0);
          highlightFiducialType(fiducialType); // highlight a single fiducial different from multiple fiducials
          FiducialRenderer fiducialRenderer = getFiducialRenderers(fiducialType).get(0);
          setStatusBarDescription(fiducialRenderer);
        }
        else
          highlightMultipleFiducials(fiducials);

      }
      else if (selectionEventEnum.equals(SelectionEventEnum.PAD_TYPE))
      {
        /** Warning "unchecked cast" approved for cast from Object types.*/
        /**Changes :: No load : pad Type must be loaded !*/
        List<PadType> padTypes = (List<PadType>)guiEvent.getSource();
        if (padTypes.isEmpty())
          return;
        
        if(findPadRenderersForPadTypes(padTypes).size() <= 0)
          return;
        
        if(padTypes.get(0).isInspected() == false)
          return;
        selectPadTypes(padTypes);
        List<Pad> pads = padTypes.get(0).getPads();
        if (pads.isEmpty())
          return;
        Pad pad = pads.get(0);
        PadRenderer padRenderer = getPadRenderer(pad);
        if (padRenderer != null)
          setStatusBarDescription(padRenderer);
      }
      else if (selectionEventEnum.equals(SelectionEventEnum.PAD_OR_FIDUCIAL))
      {
        /** Warning "unchecked cast" approved for cast from Object types.*/
        List<Object> padOrFiducials = (List<Object>)guiEvent.getSource();
        selectPadsAndFiducials(padOrFiducials);
      }
      else if (selectionEventEnum.equals(SelectionEventEnum.PSH_COMPONENT_SELECTION))
      {
        //Siew Yeng - XCR-3781
        /** Warning "unchecked cast" approved for cast from Object types.*/
        // modify subtype.
        List<ComponentType> components = (List<ComponentType>)guiEvent.getSource();
        if (components.isEmpty())
          return;
        else if(components.get(0).isLoaded() == false && _showNoLoadComponent == false)
         return;
        
        if(isTopSideVisible() && _showTopComponentLayer == false)
          return;
        else if(isBottomSideVisible() && _showBottomComponentLayer == false)
          return;
                
        else if (components.size() == 1)
        {
          _graphicsEngine.clearHighlightedRenderers();
          ComponentType componentType = (ComponentType)components.get(0);
          if(componentType != null)
          {
            highlightComponentType(componentType); // highlight a single component different from multiple components
            for (ComponentRenderer componentRenderer : findComponentRenderers(componentType))
            {
              _graphicsEngine.setHighightedRenderer(componentRenderer, Color.BLUE);
            }
            
            if(getComponentRenderers(componentType) != null)
              setStatusBarDescription(getComponentRenderers(componentType).get(0));
          }
        }
        else
        {
          for(ComponentType componentType : components)
          {
            for (ComponentRenderer componentRenderer : findComponentRenderers(componentType))
            {
              _graphicsEngine.setHighightedRenderer(componentRenderer, Color.BLUE);
            }
          }
        }
      }
    }
    if (guiEventEnum instanceof HighlightEventEnum)
    {
      //alignment tab
      HighlightEventEnum highlightEventEnum = (HighlightEventEnum)guiEventEnum;
      if (highlightEventEnum.equals(HighlightEventEnum.CLEAR_HIGHLIGHTS))
      {
        _graphicsEngine.clearHighlightedRenderers();
        _highlightPads.clear();
      }
      else if (highlightEventEnum.equals(HighlightEventEnum.HIGHLIGHT_PAD_INSTANCES))
      {
        //XCR-2207: Yellow crosshair that mark active components are not always ON
        _graphicsEngine.clearHighlightedRenderersWithoutCrossHair();
//        _graphicsEngine.clearSelectedRenderers();
        /** Warning "unchecked cast" approved for cast from Object types.*/
        List<HighlightAlignmentItems> highlightPads = (List<HighlightAlignmentItems>)guiEvent.getSource();
        _highlightPads = highlightPads;
        highlightPads(highlightPads);
      }
      else if (highlightEventEnum.equals(HighlightEventEnum.CENTER_FEATURE))
      {
        Object source = guiEvent.getSource();
        if (source instanceof AlignmentGroup)
        {
          AlignmentGroup alignmentGroup = (AlignmentGroup)source;
          Collection<com.axi.guiUtil.Renderer>
              alignmentRegionRenderers = _graphicsEngine.getRenderers(_alignmentRegionLayer);
          for (com.axi.guiUtil.Renderer alignmentLayerRenderer : alignmentRegionRenderers)
          {
            if (alignmentLayerRenderer instanceof AlignmentRegionRenderer)
            {
              AlignmentRegionRenderer alignmentRegionRenderer = (AlignmentRegionRenderer)alignmentLayerRenderer;
              AlignmentGroup renderedAlignmentGroup = alignmentRegionRenderer.getAlignmentGroup();
              if (renderedAlignmentGroup == alignmentGroup)
              {
                if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0) == false)
                {
                  if (alignmentGroup.isEmpty())
                  {
                    _graphicsEngine.fitGraphicsToScreen();
                    return;
                  }
                  // we're at some zoom level, let's center the graphics to show the new alignment region
                  DoubleCoordinate coordinate = alignmentRegionRenderer.getCenterCoordinate();
                  _graphicsEngine.center(coordinate.getX(), coordinate.getY(), 0, 0);

                  if (alignmentGroup.isTopSide())
                    _controlToolBar.showTopSide();
                  else
                    _controlToolBar.showBottomSide();
                  return;
                }
              }
            }
          }
        }
      }
    }
    if (guiEventEnum instanceof GraphicsControlEnum)
    {
      GraphicsControlEnum graphicsControlEnum = (GraphicsControlEnum)guiEventEnum;
      if (graphicsControlEnum.equals(GraphicsControlEnum.ALIGNMENT_VISIBLE))
      {
        Boolean isTopSide = (Boolean)guiEvent.getSource();
        populateAlignmentRenderers(isTopSide.booleanValue());
        _graphicsEngine.setVisible(_alignmentRegionLayer, true);
        updateAlignmentRegionData(isTopSide);
      }
      else if (graphicsControlEnum.equals(GraphicsControlEnum.ALIGNMENT_INVISIBLE))
      {
        removeAlignmentRenderers();
        _graphicsEngine.clearHighlightedRenderers();
        _highlightPads.clear();
        _graphicsEngine.setVisible(_alignmentRegionLayer, false);
      }
      else if (graphicsControlEnum.equals(GraphicsControlEnum.ORIGIN_VISIBLE))
      {
        _graphicsEngine.setVisible(getBoardOriginLayers(), true);
      }
      else if (graphicsControlEnum.equals(GraphicsControlEnum.ORIGIN_INVISIBLE))
      {
        _graphicsEngine.setVisible(getBoardOriginLayers(), false);
      }
      else if (graphicsControlEnum.equals(GraphicsControlEnum.BOARD_NAME_VISIBLE))
      {
        _graphicsEngine.setVisible(getBoardNameLayers(), true);
      }
      else if (graphicsControlEnum.equals(GraphicsControlEnum.BOARD_NAME_INVISIBLE))
      {
        _graphicsEngine.setVisible(getBoardNameLayers(), false);
      }

    }
    if (guiEventEnum instanceof LongPanelAlignmentRegionEnum)
    {
      LongPanelAlignmentRegionEnum longPanelAlignmentRegionEnum = (LongPanelAlignmentRegionEnum)guiEventEnum;
      if (longPanelAlignmentRegionEnum.equals(LongPanelAlignmentRegionEnum.TOP))
        updateAlignmentRegionData(true);
      if (longPanelAlignmentRegionEnum.equals(LongPanelAlignmentRegionEnum.BOTTOM))
        updateAlignmentRegionData(false);
    }

//    if (guiEventEnum instanceof TaskPanelScreenEnum)
//    {
//      TaskPanelScreenEnum screenEnum = (TaskPanelScreenEnum)guiEventEnum;
//      if(screenEnum.equals(TaskPanelScreenEnum.MANUAL_ALIGNMENT))
//      {
//        populateAlignmentRenderers();
//      }
//      else
//      {
//        removeAlignmentRenderers();
//      }
//    }
	//janan - XCR-3837: Display untestable Area on virtual live CAD
    if (guiEventEnum instanceof TaskPanelScreenEnum)
    {
      TaskPanelScreenEnum taskPanelScreenEnum = (TaskPanelScreenEnum) guiEventEnum;
      if (taskPanelScreenEnum.equals(TaskPanelScreenEnum.VIRTUAL_LIVE))
      {
        _graphicsEngine.setVisible(_untestableAreaLayer, true);
        _controlToolBar.setEnableDisplayUntestableAreaToggleButton(false);
        _controlToolBar.setDisplayUntestableAreaToggleButton(true);
      }
      else if (MainMenuGui.getInstance().getTestDev().getPersistance().displayUntestableArea())
      {
        _graphicsEngine.setVisible(_untestableAreaLayer, true);
        _controlToolBar.setDisplayUntestableAreaToggleButton(true);
      }
      else
      {
        _graphicsEngine.setVisible(_untestableAreaLayer, false);
        _controlToolBar.setDisplayUntestableAreaToggleButton(false);
      }
    }
  }

  /**
   * @author George A. David
   */
  private void removeAlignmentRenderers()
  {
    if (_graphicsEngine.layerExists(_alignmentRegionLayer))
      _graphicsEngine.removeRenderers(_alignmentRegionLayer);
  }

  /**
   * @author George A. David
   */
  private void populateAlignmentRenderers(boolean isTopSide)
  {
    // populate alignment region layers
    Project project = _panel.getProject();
    PanelSettings panelSettings = _panel.getPanelSettings();
    
    // XCR1519 - Need to re-generate TestProgram if we found there is invalid TestProgram
    if (project.isTestProgramValidIgnoringAlignmentRegions() == false)
    {
      MainMenuGui.getInstance().generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_ALIGNMENT_NEEDS_TEST_PROGRAM_KEY"),
                                                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                                      project);
    }
    
    // in some corner cases, we may not have any test sub programs. this can happen
    // on an extremely wide panel where all the joints lie on the left or right edge.
    // this is becuase our system cannot physically inspect anything wider than 17.5 inches
    TestProgram testProgram = project.getTestProgram();
    if(testProgram.getAllTestSubPrograms().size() == 0)
      return;

    if(panelSettings.isPanelBasedAlignment())
    {
      if (testProgram.isLongProgram())
      {
        java.util.List<AlignmentGroup> topAlignmentGroups = panelSettings.getRightAlignmentGroupsForLongPanel();
        java.util.List<AlignmentGroup> bottomAlignmentGroups = panelSettings.getLeftAlignmentGroupsForLongPanel();
        InactiveAlignmentRegionRenderer inactiveAlignmentRegionRenderer = new InactiveAlignmentRegionRenderer(_panel, isTopSide);
        _graphicsEngine.addRenderer(_alignmentRegionLayer, inactiveAlignmentRegionRenderer);
        if ((topAlignmentGroups.size() == 3) && (bottomAlignmentGroups.size() == 3))
        {
          AlignmentRegionRenderer region1Renderer = new AlignmentRegionRenderer(project, topAlignmentGroups.get(0),
              bottomAlignmentGroups.get(0),
              LayerColorEnum.ALIGNMENT_GROUP_1_COLOR.getColor());
          AlignmentRegionRenderer region2Renderer = new AlignmentRegionRenderer(project, topAlignmentGroups.get(1),
              bottomAlignmentGroups.get(1),
              LayerColorEnum.ALIGNMENT_GROUP_2_COLOR.getColor());
          AlignmentRegionRenderer region3Renderer = new AlignmentRegionRenderer(project, topAlignmentGroups.get(2),
              bottomAlignmentGroups.get(2),
              LayerColorEnum.ALIGNMENT_GROUP_3_COLOR.getColor());
          _graphicsEngine.addRenderer(_alignmentRegionLayer, region1Renderer);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, region2Renderer);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, region3Renderer);

          // now slap in the performance ones
          AlignmentThroughputRenderer performance1Renderer = new AlignmentThroughputRenderer(_panel, true);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, performance1Renderer);
          AlignmentThroughputRenderer performance2Renderer = new AlignmentThroughputRenderer(_panel, false);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, performance2Renderer);

        }
      }
      else
      {
        java.util.List<AlignmentGroup> alignmentGroups = panelSettings.getAlignmentGroupsForShortPanel();
        if (alignmentGroups.size() == 3)
        {
          AlignmentRegionRenderer region1Renderer = new AlignmentRegionRenderer(project, alignmentGroups.get(0),
              LayerColorEnum.ALIGNMENT_GROUP_1_COLOR.getColor());
          AlignmentRegionRenderer region2Renderer = new AlignmentRegionRenderer(project, alignmentGroups.get(1),
              LayerColorEnum.ALIGNMENT_GROUP_2_COLOR.getColor());
          AlignmentRegionRenderer region3Renderer = new AlignmentRegionRenderer(project, alignmentGroups.get(2),
              LayerColorEnum.ALIGNMENT_GROUP_3_COLOR.getColor());
          _graphicsEngine.addRenderer(_alignmentRegionLayer, region1Renderer);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, region2Renderer);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, region3Renderer);

          // now slap in the performance ones
          AlignmentThroughputRenderer performance1Renderer = new AlignmentThroughputRenderer(_panel, true);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, performance1Renderer);
          AlignmentThroughputRenderer performance2Renderer = new AlignmentThroughputRenderer(_panel, false);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, performance2Renderer);
        }
      }
    }
    else
    {
      // wei chin added for individual alignment
      if (testProgram.isLongProgram())
      {
        java.util.List<AlignmentGroup> topAlignmentGroups = _panel.getBoards().get(0).getBoardSettings().getRightAlignmentGroupsForLongPanel();
        java.util.List<AlignmentGroup> bottomAlignmentGroups = _panel.getBoards().get(0).getBoardSettings().getLeftAlignmentGroupsForLongPanel();
        InactiveAlignmentRegionRenderer inactiveAlignmentRegionRenderer = new InactiveAlignmentRegionRenderer(_panel.getBoards().get(0), isTopSide);
        _graphicsEngine.addRenderer(_alignmentRegionLayer, inactiveAlignmentRegionRenderer);
        if ((topAlignmentGroups.size() == 3) && (bottomAlignmentGroups.size() == 3))
        {
          AlignmentRegionRenderer region1Renderer = new AlignmentRegionRenderer(project, topAlignmentGroups.get(0),
              bottomAlignmentGroups.get(0),
              LayerColorEnum.ALIGNMENT_GROUP_1_COLOR.getColor());
          AlignmentRegionRenderer region2Renderer = new AlignmentRegionRenderer(project, topAlignmentGroups.get(1),
              bottomAlignmentGroups.get(1),
              LayerColorEnum.ALIGNMENT_GROUP_2_COLOR.getColor());
          AlignmentRegionRenderer region3Renderer = new AlignmentRegionRenderer(project, topAlignmentGroups.get(2),
              bottomAlignmentGroups.get(2),
              LayerColorEnum.ALIGNMENT_GROUP_3_COLOR.getColor());
          _graphicsEngine.addRenderer(_alignmentRegionLayer, region1Renderer);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, region2Renderer);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, region3Renderer);

          // now slap in the performance ones
          AlignmentThroughputRenderer performance1Renderer = new AlignmentThroughputRenderer(_panel.getBoards().get(0), true);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, performance1Renderer);
          AlignmentThroughputRenderer performance2Renderer = new AlignmentThroughputRenderer(_panel.getBoards().get(0), false);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, performance2Renderer);

        }
      }
      else
      {
        InactiveAlignmentRegionRenderer inactiveAlignmentRegionRenderer = new InactiveAlignmentRegionRenderer(_panel.getBoards().get(0), isTopSide);
        _graphicsEngine.addRenderer(_alignmentRegionLayer, inactiveAlignmentRegionRenderer);
        java.util.List<AlignmentGroup> alignmentGroups = _panel.getBoards().get(0).getBoardSettings().getAlignmentGroupsForShortPanel();
        if (alignmentGroups.size() == 3)
        {
          AlignmentRegionRenderer region1Renderer = new AlignmentRegionRenderer(project, alignmentGroups.get(0),
              LayerColorEnum.ALIGNMENT_GROUP_1_COLOR.getColor());
          AlignmentRegionRenderer region2Renderer = new AlignmentRegionRenderer(project, alignmentGroups.get(1),
              LayerColorEnum.ALIGNMENT_GROUP_2_COLOR.getColor());
          AlignmentRegionRenderer region3Renderer = new AlignmentRegionRenderer(project, alignmentGroups.get(2),
              LayerColorEnum.ALIGNMENT_GROUP_3_COLOR.getColor());
          _graphicsEngine.addRenderer(_alignmentRegionLayer, region1Renderer);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, region2Renderer);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, region3Renderer);

          // now slap in the performance ones
          AlignmentThroughputRenderer performance1Renderer = new AlignmentThroughputRenderer(_panel.getBoards().get(0), true);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, performance1Renderer);
          AlignmentThroughputRenderer performance2Renderer = new AlignmentThroughputRenderer(_panel.getBoards().get(0), false);
          _graphicsEngine.addRenderer(_alignmentRegionLayer, performance2Renderer);
        }
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  PadRenderer getPadRenderer(Pad pad)
  {
    Assert.expect(pad != null);
    PadRenderer padRenderer = _padToPadRendererMap.get(pad);
    if (padRenderer == null)
    {
      java.util.List<ComponentRenderer> componentRenderers = _componentTypeToComponentRenderersMap.get(pad.getComponent().getComponentType());
      Assert.expect(componentRenderers.isEmpty() == false);
      ComponentRenderer componentRenderer = componentRenderers.get(0);
      padRenderer = new PadRenderer(pad, componentRenderer);
    }
    return padRenderer;
  }

  /**
   * @author Andy Mechtenberg
   * @Edited by Kee Chin Seong
   */
  List<ComponentRenderer> getComponentRenderers(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    List<ComponentRenderer> renderers = _componentTypeToComponentRenderersMap.get(componentType);
    //Assert.expect(renderers != null);
    //Assert.expect(renderers.isEmpty() == false);
    return renderers;
  }
  
  /**
   * @author Kee Chin Seong
   * 
   */
  List<ComponentRenderer> getNoLoadComponentRenderers(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    List<ComponentRenderer> renderers = _noLoadComponentTypeToComponentRenderersMap.get(componentType);
    return renderers;
  }


  /**
   * @author Laura Cormos
   */
  List<FiducialRenderer> getFiducialRenderers(FiducialType fiducialType)
  {
    Assert.expect(fiducialType != null);
    List<FiducialRenderer> renderers = new ArrayList<FiducialRenderer>();

    if (fiducialType.getFiducials().get(0).isOnPanel())
    {
      for (com.axi.guiUtil.Renderer renderer : _graphicsEngine.getRenderers(_topPanelFiducialLayer))
      {
        Assert.expect(renderer instanceof FiducialRenderer);
        if (((FiducialRenderer)renderer).getFiducial().getFiducialType() == fiducialType)
          renderers.add((FiducialRenderer)renderer);
      }
      for (com.axi.guiUtil.Renderer renderer : _graphicsEngine.getRenderers(_bottomPanelFiducialLayer))
      {
        Assert.expect(renderer instanceof FiducialRenderer);
        if (((FiducialRenderer)renderer).getFiducial().getFiducialType() == fiducialType)
          renderers.add((FiducialRenderer)renderer);
      }
    }
    else
    {
      for (com.axi.guiUtil.Renderer renderer : _graphicsEngine.getRenderers(getTopBoardFiducialLayers()))
      {
        Assert.expect(renderer instanceof FiducialRenderer);
        if (((FiducialRenderer)renderer).getFiducial().getFiducialType() == fiducialType)
          renderers.add((FiducialRenderer)renderer);
      }
      for (com.axi.guiUtil.Renderer renderer : _graphicsEngine.getRenderers(getBottomBoardFiducialLayers()))
      {
        Assert.expect(renderer instanceof FiducialRenderer);
        if (((FiducialRenderer)renderer).getFiducial().getFiducialType() == fiducialType)
          renderers.add((FiducialRenderer)renderer);
      }
    }
    Assert.expect(renderers != null);
    Assert.expect(renderers.isEmpty() == false);
    return renderers;
  }

  /**
   * @author Laura Cormos
   */
  private void componentRendererToPadRenderersMapPut(ComponentRenderer componentRenderer,
                                                     List<PadRenderer> padRenderers)
  {
    _componentRendererToPadRenderersMap.put(componentRenderer, padRenderers);
  }
  
  /**
   * @author Kee Chin Seong
  */
  private void noLoadComponentRendererToPadRendererNamesMapPut(ComponentRenderer componentRenderer,
                                                     List<PadNameRenderer> padRendererNames)
  {
    _noLoadComponentRendererToPadNameRendererMap.put(componentRenderer, padRendererNames);
  }
  
  /**
   * @author Chin Seong Kee
   */
  private void noLoadComponentRendererToPadRendererNamesMapRemove(ComponentRenderer componentRenderer)
  {
    _noLoadComponentRendererToPadNameRendererMap.remove(componentRenderer);
  }
  
  /**
   * @author Chin Seong Kee
   */
  private void noLoadComponentRendererToPadRenderersMapPut(ComponentRenderer componentRenderer,
                                                     List<PadRenderer> padRenderers)
  {
    _noLoadComponentRendererToPadRenderersMap.put(componentRenderer, padRenderers);
  }
  
  /**
   * @author Chin Seong Kee
   */
  private void noLoadComponentRendererToPadRenderersMapRemove(ComponentRenderer componentRenderer)
  {
    _noLoadComponentRendererToPadRenderersMap.remove(componentRenderer);
  }

  /**
   * @author Laura Cormos
   */
  private void componentRendererToPadRenderersMapRemove(ComponentRenderer componentRenderer)
  {
    _componentRendererToPadRenderersMap.remove(componentRenderer);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTopPadLayerVisibility(boolean visible)
  {
    _showTopPadLayer = visible;
    if(visible == false)
    {
      _graphicsEngine.removeRenderers(getTopPadLayers());//, visible);
      //_componentRendererToPadRenderersMap.clear();
    }
    else
    {
      for(Integer layerInt : getTopPadLayers())
        updateRenderers(_graphicsEngine, layerInt.intValue());
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  private void setReconstructionRegionsConfigurationsVisible(boolean visible)
  {
    setVerificationIrpBoundariesLayerVisible(false);
    setVerificationFocusRegionLayersVisible(false);
    setVerificationRegionRectangleLayersVisible(false);
    
    setInspectionRegionPadBoundsLayersVisible(false);
    setInspectionRegionRectangleLayersVisible(false);
    setInspectionIrpBoundariesLayerVisible(false);
    setInspectionFocusRegionLayersVisible(false);
    
    setAlignmentFocusRegionLayersVisible(false);
    setAlignmentRegionRectangleLayersVisible(false);
    setAlignmentRegionPadBoundsLayersVisible(false);
    
    _graphicsEngine.setVisible(getBoardNameLayers(), false);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTopLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getTopLayers(), visible);
    
    //if(!_drawDebugGraphics)
      setReconstructionRegionsConfigurationsVisible(false);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getBottomLayers(), visible);
    
    //if(!_drawDebugGraphics)
      setReconstructionRegionsConfigurationsVisible(false);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomPanelLayersVisible(boolean visible)
  {
    _showBottomPanelLayer = visible;
    _graphicsEngine.setVisible(getBottomPanelLayer(), visible);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTopPanelLayersVisible(boolean visible)
  {
    _showTopPanelLayer = visible;
    _graphicsEngine.setVisible(getTopPanelLayer(), visible);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isTopPanelLayersVisible()
  {
    return (_graphicsEngine.isLayerVisible(getTopPanelLayer())) ? (true & _showTopPanelLayer) : (false & _showTopPanelLayer);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isBottomPanelLayersVisible()
  {
    return (_graphicsEngine.isLayerVisible(getBottomPanelLayer())) ? (true & _showBottomPanelLayer) : (false & _showBottomPanelLayer);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTopComponentLayersVisible(boolean visible, Panel panel)
  {
    _showTopComponentLayer = visible;
    
    if(visible == false)
    {
        _graphicsEngine.removeRenderers(getTopComponentLayers());
        setTopPadLayerVisibility(false);
        setTopReferenceDesignatorLayerVisibility(false, panel);
        
        for(Board board : panel.getBoards())
          unpopulateComponentLayerWithRenderers(board.getTopSideBoard().getComponents());
    }
    else
    {
        for(Board board : panel.getBoards())
          populateComponentLayersWithRenderers(board, board.getTopSideBoard().getComponents(), true, false);    
        setTopPadLayerVisibility(true);
        setTopReferenceDesignatorLayerVisibility(true, panel);
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean isTopComponentLayersVisible()
  {
    List<Integer> topComponentLayers = getTopComponentLayers();
    for (Integer layerNum : topComponentLayers)
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true & _showTopComponentLayer;
    }
    return false & _showTopComponentLayer;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean isBottomComponentLayersVisible()
  {
    List<Integer> bottomComponentLayers = getBottomComponentLayers();
    for (Integer layerNum : bottomComponentLayers)
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true & _showBottomComponentLayer;
    }
    return false & _showBottomComponentLayer;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomComponentLayersVisible(boolean visible, Panel panel)
  {
    _showBottomComponentLayer = visible;
    
    if(visible == false)
    {
       _graphicsEngine.removeRenderers(getBottomComponentLayers());
       setBottomPadLayerVisibility(false);
       setBottomReferenceDesignatorLayerVisibility(false, panel);
       
       for(Board board : panel.getBoards())
          unpopulateComponentLayerWithRenderers(board.getBottomSideBoard().getComponents());
    }
    else
    {
       for(Board board : panel.getBoards())
          populateComponentLayersWithRenderers(board, board.getBottomSideBoard().getComponents(), false, false);
       
       setBottomPadLayerVisibility(false);
       setBottomReferenceDesignatorLayerVisibility(true, panel);
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTopReferenceDesignatorLayerVisibility(boolean visible, Panel panel)
  {
    _showTopComponentRefDesLayer = visible;
    if(visible == false)
      _graphicsEngine.removeRenderers(getTopReferenceDesignatorLayers());//, visible);
    else
    {
      for(Board board : panel.getBoards())
      {
        //Kee Chin Seong - need to check top or bottom side exist
        if(board.topSideBoardExists())
            populateReferenceDesignatorLayerWithRenderers(board, board.getTopSideBoard().getComponents(), true);
      }
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomReferenceDesignatorLayerVisibility(boolean visible, Panel panel)
  {
    _showBottomComponentRefDesLayer = visible;
    if(visible == false)
      _graphicsEngine.removeRenderers(getBottomReferenceDesignatorLayers());//, visible);
    else
    {
      for(Board board : panel.getBoards())
      {
        //Kee Chin Seong - need to check top or bottom side exist
        if(board.bottomSideBoardExists())
           populateReferenceDesignatorLayerWithRenderers(board, board.getBottomSideBoard().getComponents(), false);
      }
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomPadLayerVisibility(boolean visible)
  {
    _showBottomPadLayer = visible;
    if(visible == false)
      _graphicsEngine.removeRenderers(getBottomPadLayers());//, visible);
    else
    {
      for(Integer layerInt : getBottomPadLayers())
        updateRenderers(_graphicsEngine, layerInt.intValue());
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isTopComponentRefDesLayersVisible()
  {
    List<Integer> topCompRefDesLayers = getTopReferenceDesignatorLayers();
    for (Integer layerNum : topCompRefDesLayers)
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true & _showTopComponentRefDesLayer;
    }
    return false & _showTopComponentRefDesLayer;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isBottomComponentRefDesLayersVisible()
  {
    List<Integer> bottomCompRefDesLayers = getBottomReferenceDesignatorLayers();
    for (Integer layerNum : bottomCompRefDesLayers)
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true & _showBottomComponentRefDesLayer;
    }
    return false & _showBottomComponentRefDesLayer;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean isTopPadLayersVisible()
  {
    List<Integer> topPadLayers = getTopPadLayers();
    for (Integer layerNum : topPadLayers)
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true & _showTopPadLayer;
    }
    return false  & _showTopPadLayer;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean isBottomPadLayersVisible()
  {
    List<Integer> bottomPadLayers = getBottomPadLayers();
    for (Integer layerNum : bottomPadLayers)
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true & _showBottomPadLayer;
    }
    return false & _showBottomPadLayer;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTopBoardLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getTopBoardLayers(), visible);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomBoardLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getBottomBoardLayers(), visible);
  }

  /**
   * @author George A. David
   */
  public void setInspectionRegionRectangleLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getInspectionRegionRectangleLayers(), visible);
  }

  /**
   * @author George A. David
   */
  public void setInspectionRegionPadBoundsLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getInspectionRegionPadBoundsLayers(), visible);
  }

  /**
   * @author George A. David
   */
  public void setInspectionFocusRegionLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getInspectionFocusRegionLayers(), visible);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setFocusRegionDebugGraphicVisible(String componentName, int regionId, boolean visible)
  {
     if(componentName != null)
       setInspectionRegionBottomLayersVisible(componentName, regionId, visible);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setInspectionRegionTopLayersVisible(String componentName, int regionId, boolean visible)
  {
    //_graphicsEngine.setVisible(_topInspectionRegionRectangleLayer, visible);
    setSpecificRenderersVisible(componentName, regionId, visible);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setInspectionRegionBottomLayersVisible(String componentName, int regionId, boolean visible)
  {
    setSpecificRenderersVisible(componentName, regionId, visible);
  }
  
/*
 * @author Kee Chin Seong - this will find a specific component to draw the inspection region And the focus 
 */
  private void setSpecificRenderersVisible(String componentName, int regionId, boolean visible)
  {
   //Remove the renderers make sure is empty first!
    _graphicsEngine.removeRenderers(_topEditFocusRegionInspectionRegionRectangleLayer);
    _graphicsEngine.removeRenderers(_bottomEditFocusRegionInspectionRegionRectangleLayer);
    _graphicsEngine.removeRenderers(_topEditFocusRegionInspectionFocusRegionLayer);
    _graphicsEngine.removeRenderers(_bottomEditFocusRegionInspectionFocusRegionLayer);
    
    for (TestSubProgram subProgram : _panel.getProject().getTestProgram().getAllTestSubPrograms())
    {
       if(subProgram.hasInspectionRegions() == false)
          continue;
      // inspection regions
      for (ReconstructionRegion inspectionRegion : subProgram.getAllInspectionRegions())
      {
        if(inspectionRegion.getComponent().getReferenceDesignator().equals(componentName) &&
           inspectionRegion.getRegionId() == regionId)
        {
            InspectionRegionRenderer regionRenderer = new InspectionRegionRenderer(inspectionRegion);
            if (inspectionRegion.isTopSide())
            {
              _graphicsEngine.addRenderer(_topEditFocusRegionInspectionRegionRectangleLayer, regionRenderer);
            }
            else
            {
              _graphicsEngine.addRenderer(_bottomEditFocusRegionInspectionRegionRectangleLayer, regionRenderer);
            }
            {
              int index = 0;
              for (FocusGroup focusGroup : inspectionRegion.getFocusGroups())
              {
                if (index == 0)
                {
                  FocusRegionRenderer focusRenderer = new FocusRegionRenderer(focusGroup.getFocusSearchParameters().
                      getFocusRegion());
                  if (inspectionRegion.isTopSide())
                    _graphicsEngine.addRenderer(_topEditFocusRegionInspectionFocusRegionLayer, focusRenderer);
                  else
                    _graphicsEngine.addRenderer(_bottomEditFocusRegionInspectionFocusRegionLayer, focusRenderer);
                  break;
                }
                ++index;
              }
            }
        }
      }
    }
    // Kok Chun, Tan - XCR3811 - index out of bound when the selected component is no load
    setEditFocusRegionInspectionFocusRegionLayerVisibility(isTopSideVisible(), isTopSideVisible() == false);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setEditFocusRegionInspectionFocusRegionLayerVisibility(boolean topLayeVisible, boolean bottomLayerVisible)
  {
    _graphicsEngine.setVisible(_topEditFocusRegionInspectionFocusRegionLayer, topLayeVisible);
    _graphicsEngine.setVisible(_topEditFocusRegionInspectionRegionRectangleLayer, topLayeVisible);
    _graphicsEngine.setVisible(_bottomEditFocusRegionInspectionRegionRectangleLayer, bottomLayerVisible);
    _graphicsEngine.setVisible(_bottomEditFocusRegionInspectionFocusRegionLayer, bottomLayerVisible);
  }
  
  /**
   * @author George A. David
   */
  public void setVerificationRegionRectangleLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getVerificationRegionRectangleLayers(), visible);
  }

  /**
   * @author George A. David
   */
  public void setVerificationFocusRegionLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getVerificationFocusRegionLayers(), visible);
  }

  /**
   * @author George A. David
   */
  public void setVerificationIrpBoundariesLayerVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_verificationIrpBoundariesLayer, visible);
  }

  /**
   * @author George A. David
   */
  public void setInspectionIrpBoundariesLayerVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_inspectionIrpBoundariesLayer, visible);
  }

  /**
   * @author George A. David
   */
  public void setAlignmentRegionRectangleLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getAlignmentRegionRectangleLayers(), visible);
  }
  
   /**
   * @author Jack Hwee
   */
  public void setOpticalRegionSelectedLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_opticalRegionSelectedLayer, visible);
    _graphicsEngine.setVisible(_opticalRegionUnSelectedLayer, visible);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setMeshTriangleLayerVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_meshTriangleLayer, visible);
  }
  
    /**
   * @author Jack Hwee
   */
  public void setDragOpticalRegionLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_dragOpticalRegionLayer, visible);
  }
  
    /**
   * @author Jack Hwee
   */
  public void setDragAlignmentOpticalRegionLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_dragAlignmentOpticalRegionLayer, visible);
  }
  
    /**
   * @author Jack Hwee
   */
  public void setOpticalRegionUnSelectedLayerVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_opticalRegionUnSelectedLayer, visible);
  }

  /**
   * @author George A. David
   */
  public void setAlignmentRegionPadBoundsLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getAlignmentRegionPadBoundsLayers(), visible);
  }

  /**
   * @author George A. David
   */
  public void setAlignmentFocusRegionLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getAlignmentFocusRegionLayers(), visible);
  }
  
   /**
   * @author George A. David
  
  public void setOpticalRegionSelectedLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getOpticalRegionSelectedLayers(), visible);
  }  */

  // debugging functions below.
  /**
   * @author Laura Cormos
   */
  private void printRenderers()
  {
    Collection<com.axi.guiUtil.Renderer> allRenderers = _graphicsEngine.getRenderers(getAllLayers());
    System.out.println("Printing all renderers currently residing in the graphics engine");
    for (com.axi.guiUtil.Renderer renderer : allRenderers)
    {
      if (renderer instanceof ComponentRenderer)
      {
        ComponentRenderer rend = (ComponentRenderer)renderer;
        System.out.println("\tComponent Renderer for comp: " + rend.getComponent().getReferenceDesignator() +
                           " on layer: " + _graphicsEngine.getLayerNumberForRenderer(rend));
      }
      else if (renderer instanceof FiducialRenderer)
      {
        FiducialRenderer rend = (FiducialRenderer)renderer;
        System.out.println("\tFiducial Renderer for fid: " + rend.getFiducial().getName() +
                           " on layer: " + _graphicsEngine.getLayerNumberForRenderer(rend));
      }
      else if (renderer instanceof PadRenderer)
      {
        PadRenderer rend = (PadRenderer)renderer;
        if (rend.getPad().isThroughHolePad())
          System.out.println("\tTH pad - should have 2 renderers");
        System.out.println("\tPad Renderer for pad: " + rend.getPad().getComponentAndPadName() +
                           " on layer: " + _graphicsEngine.getLayerNumberForRenderer(rend));
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void printCTtoCRMap()
  {
    System.out.println("ComponentType To ComponentRenderer Map");
    for(ComponentType ct : _componentTypeToComponentRenderersMap.keySet())
    {
      System.out.println("ComponentType: " + ct.getReferenceDesignator());
      List<ComponentRenderer> crList = _componentTypeToComponentRenderersMap.get(ct);
      for (ComponentRenderer cr : crList)
      {
        System.out.println("\tComponentRenderer: " + cr.getComponent().getReferenceDesignator());
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void printCRtoPRMap()
  {
    System.out.println("ComponentRenderer To PadRenderer Map");
    for (ComponentRenderer cr : _componentRendererToPadRenderersMap.keySet())
    {
      System.out.println("ComponentRenderer: " + cr.getComponent().getReferenceDesignator());
      List<PadRenderer> crList = _componentRendererToPadRenderersMap.get(cr);
      for (PadRenderer pr : crList)
      {
        System.out.println("\tPadRenderer " + pr.getPad().getName());
      }
    }
  }
  
   /**
   * @author Jack Hwee
   * The list which keep the list of optical region which will be displayed in live view
   */
  public void setDividedOpticalRegionList(java.util.List<OpticalCameraRectangle> dividedOpticalRegionList)
  {
    _opticalCameraRectangleList = dividedOpticalRegionList;
    handleDragOpticalRegion();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void addDividedOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {
    if (_opticalCameraRectangleList != null)
      _opticalCameraRectangleList.add(opticalCameraRectangle);
    
    //handleDragOpticalRegion();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removeDividedOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {
    if (_opticalCameraRectangleList.contains(opticalCameraRectangle))
      _opticalCameraRectangleList.remove(opticalCameraRectangle);
    
    handleDragOpticalRegion();
  }
  
  /**
   * @author Jack Hwee
   * Clear the list
   */
  public void clearDividedOpticalRegionList()
  {
    if (_opticalCameraRectangleList != null)
      _opticalCameraRectangleList.clear();
    
    handleDragOpticalRegion();
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public java.util.List<OpticalCameraRectangle> getDividedOpticalRegionList()
  {
    if (_opticalCameraRectangleList == null)
      _opticalCameraRectangleList = new ArrayList<OpticalCameraRectangle>();
    return _opticalCameraRectangleList;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getAllInspectedOpticalRegions()
  {
    Assert.expect(_opticalCameraRectangleList != null);

    int inspectedOpticalRegionsCount = 0;
    for (OpticalCameraRectangle ocr : _opticalCameraRectangleList)
    {
      boolean bool = ocr.getInspectableBool();
      if (bool == true)
      {
        ++inspectedOpticalRegionsCount;
      }
    }
    return inspectedOpticalRegionsCount;
  }
  
  /**
   * @author Jack Hwee
   */
  public void clearUnselectedOpticalCameraRectangleList()
  {
    if (_unselectedOpticalCameraRectangleList != null)
      _unselectedOpticalCameraRectangleList.clear();
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public void addSelectedRegionInTable(String regionName)
  {
        Assert.expect(regionName != null);
        
         if (_tableOpticalRegions.contains(regionName) == false)
          _tableOpticalRegions.add(regionName);
        setSelectedRegionInTableAvailable();       
  }
  
  
     /**
   * 
   * @author Jack Hwee
   */
  public void addSelectedRegionInComboboxUsingIndex(String regionName, int index)
  {
        Assert.expect(regionName != null);
      
        if (_tableOpticalRegions.contains(regionName) == false)
          _tableOpticalRegions.add(index, regionName);
        setSelectedRegionInTableAvailable();       
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void removeSelectedRegionInTable(String regionName)
  {    
      Assert.expect(regionName != null);
      _tableOpticalRegions.remove(regionName);
      setSelectedRegionInTableAvailable();
  }
  
     /**
   * 
   * @author Jack Hwee
   */
  public Rectangle getSelectedRegionInCombobox()
  {
    if (_tableOpticalRegions.isEmpty() == false)
    {
      int x = 0;
      int y = 0;
      int width = 0;
      int height = 0;
      String regionName = _tableOpticalRegions.get(_index);

      if (_panel.getPanelSettings().isPanelBasedAlignment())
      {
        String name[] = regionName.toString().split("_", 4);
        x = Integer.parseInt(name[0], 10);
        y = Integer.parseInt(name[1], 10);
        width = Integer.parseInt(name[2], 10);
        height = Integer.parseInt(name[3], 10);
      }
      else
      {
        String name[] = regionName.toString().split("_", 6);
        x = Integer.parseInt(name[2], 10);
        y = Integer.parseInt(name[3], 10);
        width = Integer.parseInt(name[4], 10);
        height = Integer.parseInt(name[5], 10);
      }
      Rectangle rect = new Rectangle(x, y, width, height);
      return rect;
    }
    else
      return new Rectangle(0, 0, 0, 0);
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void setSelectedRegionIndexInTable(int index)
  {
      _index = index;
  }
 
  /** 
   * @author Jack Hwee
   */
  public void setSelectedRegionInTableAvailable()
  {
      if (_tableOpticalRegions.isEmpty() )
          _graphicsEngine.setSelectedRegionInComboboxAvailable(false);
      else
          _graphicsEngine.setSelectedRegionInComboboxAvailable(true);
  }
  
  /**
   * @author Andy Mechtenberg
   */
  public List<ComponentRenderer> getSurfaceMapSelectedComponentRenderers(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    List<ComponentRenderer> renderers = _componentTypeToComponentRenderersMap.get(componentType);
    Assert.expect(renderers != null);
    Assert.expect(renderers.isEmpty() == false);
    return renderers;
  }
  
    /**
   * @author Jack Hwee
   * The list which keep the list of optical region which will be displayed in live view
   */
  public void setAlignmentDividedOpticalRegionList(Map<Rectangle, String> dividedOpticalRegionList)
  {
    if (dividedOpticalRegionList != null)
       _alignmentOpticalRegionList = dividedOpticalRegionList;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public Map<Rectangle, String> getAlignmentDividedOpticalRegionList()
  {
    return _alignmentOpticalRegionList;
  }
  
   /**
   * @author Jack Hwee
   * Draw a blue region to indicates the optical region drawn in alignment region
   */
  public void handleSelectedAlignmentOpticalRegion(java.util.List<Rectangle> rectangleList)
  {
    _graphicsEngine.removeLayer(_opticalRegionSelectedLayer);
    _graphicsEngine.removeLayer(_dragAlignmentOpticalRegionLayer);
    _graphicsEngine.removeLayer(_highlightOpticalRegionLayer);

//    for (Rectangle rect: rectangleList)
//    {
//      //Rectangle rect = rectangle;
//      rect.setRect(rect.getMinX(), rect.getMinY(), rect.getWidth(), rect.getHeight());
//      _graphicsEngine.setShouldGraphicsBeFitToScreen(true);
//      _selectedRectangleRenderer = new SelectedOpticalRectangleRenderer(opticalCameraRectangle);
//      _selectedRectangleRenderer.setRegion(rect.getBounds2D());
//      _graphicsEngine.addRenderer(_opticalRegionSelectedLayer, _selectedRectangleRenderer);
//      _graphicsEngine.setVisible(_opticalRegionSelectedLayer, true);
//
//      _graphicsEngine.repaint();
//    }       
  }
  
  /**
   * Use to draw shape that actual used for virtualLive generation
   *
   * @author khang-shian.sham
   */
  private void handleVirtualLiveImageGenerationDataObservable(final Object argument)
  {
    Assert.expect(argument != null);

    _graphicsEngine.clearCrossHairs();
    _graphicsEngine.removeRenderers(_virtualLiveAreaLayer);
    java.awt.Shape shape = (java.awt.Shape) argument;
    if(shape != null)
    {
      if(shape.getBounds2D().getWidth() != 0 && shape.getBounds().getHeight() != 0)
      {
		//modified Janan - XCR-3837: Display untestable Area on virtual live CAD
        VirtualLiveAreaRenderer virtualLiveAreaRenderer = new VirtualLiveAreaRenderer(getAutoRefitSelectedArea(shape),0);

        _graphicsEngine.addRenderer(_virtualLiveAreaLayer,virtualLiveAreaRenderer);
        _graphicsEngine.selectWithCrossHairs(virtualLiveAreaRenderer);
      }
    }
  }

  /**
   * @author sham
   */
  public void drawVirtualLiveAreaDisplay()
  {
    Assert.expect(_graphicsEngine != null);
    Assert.expect(_panel != null);

    //Khaw Chek Hau - XCR2481: Previous dragged region remained after change the board orientation cause OOF
    VirtualLiveImageGenerationSettings.getInstance().setPanelShape(_panel.getShapeInNanoMeters()); 
    VirtualLiveImageGenerationSettings.getInstance().setObservable();
    _graphicsEngine.clearSelectedRenderers();
  }

  /**
   * @author sham
   */
  public void clearVirtualLiveAreaDisplay()
  {
    Assert.expect(_graphicsEngine != null);

	_graphicsEngine.clearCrossHairs();
    _graphicsEngine.removeRenderers(_virtualLiveAreaLayer);
    if(_rendererToUse != null)
      _graphicsEngine.setSelectedRenderer(_rendererToUse);
  }
  
    /**
   * Use to draw shape that actual used for virtualLive generation
   *
   * @author Kee Chin Seong
   */
  private void handleFocusRegionSelectedArea(final Object argument)
  {
    Assert.expect(argument != null);

    _graphicsEngine.clearCrossHairs();
    _graphicsEngine.removeRenderers(_virtualLiveAreaLayer);
    java.awt.Shape shape = (java.awt.Shape) argument;
    if(shape != null)
    {
      if(shape.getBounds2D().getWidth() != 0 && shape.getBounds().getHeight() != 0)
      {
        AreaRenderer selectedFocusRegionRenderer = new AreaRenderer(new Area(shape),false);
        selectedFocusRegionRenderer.setFillShape(false);

        _graphicsEngine.addRenderer(_virtualLiveAreaLayer, selectedFocusRegionRenderer);
        _graphicsEngine.selectWithCrossHairs(selectedFocusRegionRenderer);
      }
    }
    _selectedFocusRegion = shape;
  }
  
 /*
  * @author Kee Chin Seong
  */
  public void clearFocusRegion()
  {
    _graphicsEngine.removeRenderers(_virtualLiveAreaLayer);
    _graphicsEngine.clearCrossHairs();
  }
  
  /*
   * @author Kee Chin Seong
   */
 public Shape getSelectedRegion()
 {
   return _selectedFocusRegion;
 }
  /**
   * @author sham
   */
  private void handleSelectedAreaObservable(Shape virtualLiveSelectedArea)
  {
    //Jack Hwee - if selected area out of bound, igrone the area.
    VirtualLiveImageGenerationSettings.getInstance().setMultipleSelectComponent(false);
    VirtualLiveImageGenerationSettings.getInstance().clearComponentList();
    Rectangle rect = virtualLiveSelectedArea.getBounds();
    if (rect.getMinX() < 0 || rect.getMinY() < 0)
      return;
   
	//Modified Janan - XCR-3837: Display untestable Area on virtual live CAD
    VirtualLiveImageGenerationSettings.getInstance().setVirtualLiveImageGenerationData(getAutoRefitSelectedArea(rect));
      //XCR-2612 prompt out message to ask user re-select another ROI
    if (VirtualLiveImageGenerationSettings.getInstance().isSelectedVirtualLiveRoiInvalid())          
    {
      String message = StringLocalizer.keyToString("VIRTUAL_LIVE_IMAGE_ROI_INVALID_KEY");
      // cannot generate virtual live image set for the selected region.
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
        message,
        StringLocalizer.keyToString("GISWK_INVALID_ROI_KEY"),
        JOptionPane.ERROR_MESSAGE);
      return;
    }
    drawVirtualLiveAreaDisplay();
  }
  
  /**
   * @author Wei Chin
   */
  public void repopulateSplitRenderer()
  {
//    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(MainMenuGui.getInstance(),
//            "Refreshing Panel Information...",
//            "Updating",
//            true);
//
//    busyCancelDialog.setFont(FontUtil.getMediumFont());    
//    busyCancelDialog.pack();
//    SwingUtils.centerOnComponent(busyCancelDialog, MainMenuGui.getInstance());
    
    _graphicsEngine.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              Collection<com.axi.guiUtil.Renderer> alignmentRenderers = _graphicsEngine.getRenderers(_alignmentRegionLayer);
              for(com.axi.guiUtil.Renderer renderer : alignmentRenderers)
              {
                if (renderer instanceof AlignmentRegionRenderer)
                {
                  AlignmentRegionRenderer alignmentRegionRenderer = (AlignmentRegionRenderer)renderer;
                  alignmentRegionRenderer.refreshData();
                }
                else if (renderer instanceof InactiveAlignmentRegionRenderer)
                {
                  InactiveAlignmentRegionRenderer inactiveAlignmentRegionRenderer = (InactiveAlignmentRegionRenderer)renderer;
                  inactiveAlignmentRegionRenderer.refreshData();
                }
                else if (renderer instanceof AlignmentThroughputRenderer)
                {
                  AlignmentThroughputRenderer alignmentThroughputRenderer = (AlignmentThroughputRenderer)renderer;
                  alignmentThroughputRenderer.refreshData();
                }
              }
              _graphicsEngine.validateData(_alignmentRegionLayer);
            }
          });
        }
        finally
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              _graphicsEngine.setCursor(Cursor.getDefaultCursor());
            }
          });
        }
      }
    });
  }

 /**
  * @set the board to be highlight
  * @author Lim Boon Hong
  */
  public void setHighlightBoard(Board board)
  {
    _selectedBoardName = board.getName();
//    System.out.println("glb - _selectedBoardName = " + _selectedBoardName);

    List<Integer> boardLayers = new ArrayList<Integer>();
    boardLayers.addAll(getAllLayers(board));

    //Filter null boardLayers
    int size = boardLayers.size();
    for (int i = 0; i < size; ++i)
    {
      if(boardLayers.get(i) == null)
      {  
        boardLayers.remove(i);
        size = size - 1;
      }
    }

    Collection<com.axi.guiUtil.Renderer> renderers = _graphicsEngine.getRenderers(boardLayers);
    _graphicsEngine.setSelectedRenderers(renderers);
  }
  
  /**
   * 
   * @author Jack Hwee
   * The list which keep the list of optical region which not going to be displayed live view
   */
  public void addSelectedOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {     
    if (_selectedOpticalCameraRectangleList.contains(opticalCameraRectangle) == false)
       _selectedOpticalCameraRectangleList.add(opticalCameraRectangle);
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void clearSelectedOpticalCameraRectangleList()
  {
    if (_selectedOpticalCameraRectangleList != null)
       _selectedOpticalCameraRectangleList.clear();
    
    handleMultipleSelectedOpticalCameraRectangles(_selectedOpticalCameraRectangleList);
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public void removeSelectedOpticalCameraRectangle(OpticalCameraRectangle ocr)
  {
    if (_selectedOpticalCameraRectangleList.contains(ocr))
      _selectedOpticalCameraRectangleList.remove(ocr);
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public java.util.List<OpticalCameraRectangle> getSelectedOpticalCameraRectangleList()
  {
    return _selectedOpticalCameraRectangleList;
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  public void addHighlightedOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {
     Assert.expect(opticalCameraRectangle != null);
     
     if (_highlightedOpticalCameraRectangleList.contains(opticalCameraRectangle) == false)
       _highlightedOpticalCameraRectangleList.add(opticalCameraRectangle);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setHighlightedOpticalCameraRectangleList(java.util.List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    Assert.expect(opticalCameraRectangles != null);
    
    _highlightedOpticalCameraRectangleList = opticalCameraRectangles;
    handleMultipleHighlightOpticalCameraRectangles(_highlightedOpticalCameraRectangleList);
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public void clearHighlightedOpticalCameraRectangleList()
  {
    if (_highlightedOpticalCameraRectangleList != null)
      _highlightedOpticalCameraRectangleList.clear();
    
    handleMultipleHighlightOpticalCameraRectangles(_highlightedOpticalCameraRectangleList);
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public java.util.List<OpticalCameraRectangle> getHighlightedOpticalCameraRectangleList()
  {
    return _highlightedOpticalCameraRectangleList;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private OpticalRegion getSelectedOpticalRegion()
  {
    Assert.expect(_tableOpticalRegions != null);
    
    TestProgram testProgram = _panel.getProject().getTestProgram();
    OpticalRegion opticalRegion = null;
    
    if (_tableOpticalRegions.isEmpty() == false)
    {
      String opticalRegionName =  _tableOpticalRegions.get(_index);
      if (_panel.getPanelSettings().isPanelBasedAlignment())
      {
        opticalRegion = testProgram.getOpticalRegion(opticalRegionName);
      }
      else
      {
        String name[] = opticalRegionName.toString().split("_", 6);
        String newOpticalRegionName = name[2] + "_" + name[3] + "_" + name[4] + "_" + name[5];
        opticalRegion = testProgram.getOpticalRegion(newOpticalRegionName);
      }
    }
    return opticalRegion;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private OpticalRegion getSelectedOpticalRegion(String opticalRegionName)
  {
    Assert.expect(opticalRegionName != null);
    
    Project project = _panel.getProject();
    boolean isCheckLatestTestProgram = project.isCheckLatestTestProgram();
    project.setCheckLatestTestProgram(false);
    TestProgram testProgram = _panel.getProject().getTestProgram();
    project.setCheckLatestTestProgram(isCheckLatestTestProgram);
    
    OpticalRegion opticalRegion = null;
    if (_panel.getPanelSettings().isPanelBasedAlignment())
    {
      opticalRegion = testProgram.getOpticalRegion(opticalRegionName);
    }
    else
    {
      String name[] = opticalRegionName.toString().split("_", 6);
      String newOpticalRegionName = name[0] + "_" + name[1] + "_" + name[2] + "_" + name[3];
      opticalRegion = testProgram.getOpticalRegion(newOpticalRegionName);
    }
    return opticalRegion; 
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void setSelectedRegionIndexInCombobox(int index)
  {
    _index = index;
  }
  
  /**
   * @author Jack Hwee
   */
  public void setHighlightOpticalRegionLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(_highlightOpticalRegionLayer, visible);
  }
  
  /**
   * @author Jack Hwee
   */
  public boolean isHighlightOpticalRegionLayerVisible()
  {   
    return _graphicsEngine.isLayerVisible(_highlightOpticalRegionLayer); 
  }
  
  /**
   * @author Jack Hwee
   */
  public boolean isDragOpticalRegionLayerVisible()
  {   
    return _graphicsEngine.isLayerVisible(_dragOpticalRegionLayer); 
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void clearIgnoredOpticalCameraRectangleList()
  {
    if (_ignoredOpticalCameraRectangleList != null)
       _ignoredOpticalCameraRectangleList.clear();
  }
  
   /**
   * 
   * @author Jack Hwee
   * The list which keep the list of optical region which not going to be displayed live view
   */
  public void removeIgnoreOpticalRegionList(OpticalCameraRectangle opticalCameraRectangle)
  {
    if (_ignoredOpticalCameraRectangleList.contains(opticalCameraRectangle))
       _ignoredOpticalCameraRectangleList.remove(opticalCameraRectangle);
  }
  
  /**
   * Returns a list of rectangles in nanometer by passing in a list of rectangles in pixel.
   * @author Ying-Huan.Chu
   */
  public java.util.List<Rectangle> convertRectanglesFromPixelToNanometer(java.util.List<Rectangle> rectanglesInPixel)
  {
    java.util.List<Rectangle> rectanglesInNanometer = new ArrayList<>();
    for (Rectangle rect : rectanglesInPixel)
    {
      Rectangle rectInNano = _graphicsEngine.calculateSelectedComponentRegionInNanoMeter(rect);
      rectanglesInNanometer.add(rectInNano);
    }
    return rectanglesInNanometer;
  }
  
   /**
   * 
   * @author Jack Hwee
   * Remove all the Blue rectangles 
   */
  public void removeSelectedOpticalRectangleLayer()
  {
    _graphicsEngine.removeLayer(_opticalRegionSelectedLayer);
    _graphicsEngine.removeLayer(_meshTriangleLayer);
    _graphicsEngine.removeLayer(_highlightOpticalRegionLayer);
    _graphicsEngine.removeLayer(_opticalRegionUnSelectedLayer);
    _graphicsEngine.repaint();
  }
  
  /**
   * @author Jack Hwee
   */
  public void removeMeshTriangleLayer()
  {
    _graphicsEngine.removeLayer(_meshTriangleLayer);
    _graphicsEngine.repaint();
  }
  
   /**
   *
   * @author Jack Hwee
   */
  public DragOpticalRectangleRenderer getDragOpticalRectangleRenderer()
  {
    return _dragOpticalRectangleRenderer;
  }
  
  /**
   * @author Jack Hwee
   * Clear the list
   */
  public void clearAlignmentDividedOpticalRegionList()
  {
    _alignmentOpticalRegionList.clear();
  }
  
    /**
   * @author Jack Hwee
   * Draw a blue lines to indicates the mesh triangle
   */
  public void handleMeshTriangle(java.util.List<MeshTriangle> meshTriangleList)
  {
    //_graphicsEngine.removeLayer(_opticalRegionUnSelectedLayer);
    _graphicsEngine.removeLayer(_dragOpticalRegionLayer);
    _graphicsEngine.removeLayer(_meshTriangleLayer);
    //_graphicsEngine.removeLayer(_opticalRegionSelectedLayer);
    //_graphicsEngine.removeLayer(_highlightOpticalRegionLayer);

    for (MeshTriangle meshTriangle: meshTriangleList)
    {
      int line1BeginX = meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getCenterX();
      int line1EndX = meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getCenterX();
      int line1BeginY = meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getCenterY();
      int line1EndY = meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getCenterY();
      
      int line2BeginX = meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getCenterX();
      int line2EndX = meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getCenterX();
      int line2BeginY = meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getCenterY();
      int line2EndY = meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getCenterY();
      
      int line3BeginX = meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getCenterX();
      int line3EndX = meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getCenterX();
      int line3BeginY = meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getCenterY();
      int line3EndY = meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getCenterY();

      _meshTriangleRenderer = new MeshTriangleRenderer();
      _meshTriangleRenderer.setLine(line1BeginX, line1EndX, line1BeginY, line1EndY, line2BeginX, line2EndX, line2BeginY, line2EndY, 
                                    line3BeginX, line3EndX, line3BeginY, line3EndY);
      _graphicsEngine.addRenderer(_meshTriangleLayer, _meshTriangleRenderer);
      _graphicsEngine.setVisible(_meshTriangleLayer, true);
      _graphicsEngine.repaint();
    }
  }
  
  /*
   * @author bee-hoon.goh
   */
  public void loadVirtualLiveVerificationImage(String projectName)
  {
    String virtualLiveVerificationImagesDir = Directory.getVirtualLiveVerificationImagesDir(projectName);
    File virtualLiveVerificationImagesFolder = new File(virtualLiveVerificationImagesDir);
    BufferedImage img = null;
    try 
    {
      img = ImageIO.read(new File(virtualLiveVerificationImagesFolder + File.separator + "verificationImage_top.jpg"));
    } 
    catch (IOException ex) 
    {
      System.out.println("Unable to read verificationImage_top.jpg....");
    }
 
    // XCR-3886 Verification image set not appear after generation at Virtual Live with Dummy board
    VirtualLiveVerificationImageRenderer virtualLiveVerificationImageRenderer = new VirtualLiveVerificationImageRenderer(img, _panel);
    virtualLiveVerificationImageRenderer.setPanelImage(img);
    _graphicsEngine.addRenderer(_topVirtualLiveVerificationImageLayer, virtualLiveVerificationImageRenderer);
    
    // bee-hoon.goh - Populate Bottom Verification Image
    BufferedImage bottomVirtualLiveVerificationImg = null;
    try 
    {
      bottomVirtualLiveVerificationImg = ImageIO.read(new File(virtualLiveVerificationImagesFolder + File.separator + "verificationImage_bottom.jpg"));
    } 
    catch (IOException ex) 
    {
      System.out.println("Unable to read verificationImage_bottom.jpg....");
    }
    VirtualLiveVerificationImageRenderer bottomVirtualLiveVerificationImageRenderer = new VirtualLiveVerificationImageRenderer(bottomVirtualLiveVerificationImg, _panel);
    bottomVirtualLiveVerificationImageRenderer.setPanelImage(bottomVirtualLiveVerificationImg);
    _graphicsEngine.addRenderer(_bottomVirtualLiveVerificationImageLayer, bottomVirtualLiveVerificationImageRenderer);
    
    // bee-hoon.goh - Populate blended Verification Image
    BufferedImage blendedVirtualLiveVerificationImg = null;
    try 
    {
      blendedVirtualLiveVerificationImg = ImageIO.read(new File(virtualLiveVerificationImagesFolder + File.separator + "verificationImage_both.jpg"));
    } 
    catch (IOException ex) 
    {
      System.out.println("Unable to read verificationImage_bottom.jpg....");
    }
    VirtualLiveVerificationImageRenderer blendedVirtualLiveVerificationImageRenderer = new VirtualLiveVerificationImageRenderer(blendedVirtualLiveVerificationImg, _panel);
    blendedVirtualLiveVerificationImageRenderer.setPanelImage(blendedVirtualLiveVerificationImg);
    _graphicsEngine.addRenderer(_bothVirtualLiveVerificationImageLayer, blendedVirtualLiveVerificationImageRenderer);
  }
  
  /**
   * @author bee-hoon.goh
   */
  public void clearVirtualLiveVerificationImageDisplay()
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.removeRenderers(_topVirtualLiveVerificationImageLayer);
    _graphicsEngine.removeRenderers(_bottomVirtualLiveVerificationImageLayer);
    _graphicsEngine.removeRenderers(_bothVirtualLiveVerificationImageLayer);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public Rectangle getPanelRectangleInPixel()
  {
    java.util.List<com.axi.guiUtil.Renderer> renderers = new ArrayList<>(_graphicsEngine.getRenderers(_topPanelLayer));
    
    Assert.expect(renderers.size() > 0);
    return renderers.get(0).getBounds();
  }

  /**
   * 
   * @author Janan Wong
   * XCR-3837: Display untestable Area on virtual live CAD
   */
  public java.awt.Shape getAutoRefitSelectedArea(java.awt.Shape selectedArea)
  {
    if (_panel.hasUntestableArea())
    {
      if (selectedArea.getBounds2D().getX() >= _panel.getRightUntestableArea().getBounds2D().getX() || 
          selectedArea.getBounds2D().getMaxX() >= _panel.getRightUntestableArea().getBounds2D().getX())
      {
        java.awt.Shape newShape = new Rectangle2D.Double(selectedArea.getBounds2D().getX(),
                                                         selectedArea.getBounds2D().getY(),
                                                         selectedArea.getBounds2D().getWidth() - (selectedArea.getBounds2D().getMaxX() - _panel.getRightUntestableArea().getBounds2D().getX()),
                                                         selectedArea.getBounds2D().getHeight());
        return newShape;
      }
      else if (selectedArea.getBounds2D().getX() <= _panel.getLeftUntestableArea().getBounds2D().getMaxX() || 
               selectedArea.getBounds2D().getMaxX() <= _panel.getLeftUntestableArea().getBounds2D().getMaxX())
      {
        java.awt.Shape newShape = new Rectangle2D.Double(_panel.getLeftUntestableArea().getBounds2D().getMaxX(),
                                                         selectedArea.getBounds2D().getY(),
                                                         selectedArea.getBounds2D().getWidth() - (_panel.getLeftUntestableArea().getBounds2D().getMaxX() - selectedArea.getBounds2D().getX()),
                                                         selectedArea.getBounds2D().getHeight());
        return newShape;
      }
      else
      {
        return selectedArea;
      }
    }
    else
    {
      return selectedArea;
    }
  }
}
