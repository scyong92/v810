package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * This class draws the Board and/or Panel origin.
 *
 * @author Laura Cormos
 */
public class BoardCadOriginRenderer extends ShapeRenderer
{
  private Board _board = null;

  /**
   * @author Laura Cormos
   */
  BoardCadOriginRenderer(Board board)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(board != null);

    _board = board;

    refreshData();
  }

  /**
   * @author Laura Cormos
   */
  public Board getSideBoard()
  {
    return _board;
  }

  /**
   * @author Laura Cormos
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    int strokeSize = 2;
    graphics2D.setStroke(new BasicStroke(strokeSize, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
    graphics2D.draw(getShape());
    graphics2D.fill(getShape());
  }

  /**
   * Return the name of the panel or sideboard being renderered
   * @author Laura Cormos
   */
  public String toString()
  {
    Assert.expect(_board != null);
    return _board.getName();
  }

  /**
   * @author Laura Cormos
   */
  protected void refreshData()
  {
    Assert.expect(_board != null);

    // everything is started with the board origin coordinate which includes all rotations and flips
    PanelCoordinate boardOriginCoord = _board.getCadOriginRelativeToPanelInNanometers();
    int boardOriginX = boardOriginCoord.getX();
    int boardOriginY = boardOriginCoord.getY();

    // get the direction of the x axis
    Shape xAxisLineShape = _board.getXaxisLineAfterAllRotationsInNanometers();
    Rectangle2D xBounds = xAxisLineShape.getBounds2D();
    int minX = MathUtil.roundDoubleToInt(xBounds.getMinX());
    int minY = MathUtil.roundDoubleToInt(xBounds.getMinY());
    int maxX = MathUtil.roundDoubleToInt(xBounds.getMaxX());
    int maxY = MathUtil.roundDoubleToInt(xBounds.getMaxY());

    boolean xAxisIsHorizontal = false;
    int xAxisSign = 0;

    if (minY == maxY) // x axis is horizontal
    {
      if (MathUtil.fuzzyLessThan(boardOriginX, maxX))
      {
        // left to right x axis arrow
        xAxisIsHorizontal = true;
        xAxisSign = 1;
      }
      else
      {
        // right to left x axis arrow
        xAxisIsHorizontal = true;
        xAxisSign = -1;
      }
    }
    else if (minX  == maxX) // x axis is vertical
    {
      if (MathUtil.fuzzyLessThan(boardOriginY, maxY))
        {
          // bottom to top x axis arrow
          xAxisSign = 1;
        }
      else
        {
          // top to bottom x axis arrow
          xAxisSign = -1;
        }
    }
    else
      Assert.expect(false);

    // get the direction of the y axis
    // x and y axis are not always in the same relationship to each other, depending on the flips
    Shape yAxisLineShape = _board.getYaxisLineAfterAllRotationsInNanometers();
    Rectangle2D yBounds = yAxisLineShape.getBounds2D();
    minX = MathUtil.roundDoubleToInt(yBounds.getMinX());
    minY = MathUtil.roundDoubleToInt(yBounds.getMinY());
    maxX = MathUtil.roundDoubleToInt(yBounds.getMaxX());
    maxY = MathUtil.roundDoubleToInt(yBounds.getMaxY());

    int yAxisSign = 0;

    if (xAxisIsHorizontal) // y axis has to be vertical, determine which way, up or down
    {
      if (MathUtil.fuzzyLessThan(boardOriginY, maxY))
      {
        // bottom to top y axis arrow
        yAxisSign = 1;
      }
      else
      {
        // top to bottom y axis arrow
        yAxisSign = -1;
      }
    }
    else // y axis is horizontal, determine which way, left or right
    {
      if (MathUtil.fuzzyLessThan(boardOriginX, maxX))
      {
        // left to right y axis arrow
        yAxisSign = 1;
      }
      else
      {
        // right to left y axis arrow
        yAxisSign = -1;
      }
    }

    // create the board origin (x and y axes) shape
    // make arrows' size relative to the board size
    int boardXdimension = _board.getWidthInNanoMeters();
    int arrowBodyLength = boardXdimension / 10; // long dimension of the arrow body (without the head)
    int arrowHeadLength = arrowBodyLength / 8; // long dimension of the arrow head only (along)
    int arrowHeadWidth = arrowHeadLength * 2 / 3; // short dimension of the arrow head only (across)
    int originShapeDimension = boardXdimension / 80;

    GeneralPath gp = new GeneralPath();
    gp.moveTo(boardOriginX, boardOriginY); // move to board cad origin
    // construct x axis arrow body
    if (xAxisIsHorizontal)
      gp.lineTo(boardOriginX + (xAxisSign * arrowBodyLength), boardOriginY); // go right/left on X the length of the arrow body
    else
      gp.lineTo(boardOriginX, boardOriginY + (xAxisSign * arrowBodyLength)); // go up/down on Y the length of the arrow body
    // construct x axis arrow head 1st half
    if (xAxisIsHorizontal)
      gp.lineTo(boardOriginX + (xAxisSign * arrowBodyLength), boardOriginY - (xAxisSign * arrowHeadWidth / 2)); // go down/up on X 1/2 the arrow head width
    else
      gp.lineTo(boardOriginX + (xAxisSign * arrowHeadWidth / 2), boardOriginY + (xAxisSign * arrowBodyLength)); // go right/left on X 1/2 the arrow head width
    // construct the x axis arrow head point
    if (xAxisIsHorizontal)
      gp.lineTo(boardOriginX + (xAxisSign * arrowBodyLength) + (xAxisSign * arrowHeadLength), boardOriginY); // go to the point of the arrow head
    else
      gp.lineTo(boardOriginX, boardOriginY + (xAxisSign * arrowBodyLength) + (xAxisSign * arrowHeadLength)); // go to the point of the arrow head
    // construct the x axis arrow head 2nd half
    if (xAxisIsHorizontal)
      gp.lineTo(boardOriginX + (xAxisSign * arrowBodyLength), boardOriginY + (xAxisSign * arrowHeadWidth / 2));
    else
      gp.lineTo(boardOriginX - (xAxisSign * arrowHeadWidth / 2), boardOriginY + (xAxisSign * arrowBodyLength));
    if (xAxisIsHorizontal)
      gp.lineTo(boardOriginX + (xAxisSign * arrowBodyLength), boardOriginY); // back to arrow body
    else
      gp.lineTo(boardOriginX, boardOriginY + (xAxisSign * arrowBodyLength)); // back to arrow body
    // back to board origin
    gp.lineTo(boardOriginX, boardOriginY); // move to board cad origin
    // construct the y axis arrow body
    if (xAxisIsHorizontal) // y axis is vertical then
      gp.lineTo(boardOriginX, boardOriginY + (yAxisSign * arrowBodyLength)); // go up/down on Y the length of the arrow body
    else
      gp.lineTo(boardOriginX + (yAxisSign * arrowBodyLength), boardOriginY); // go right/left on X the length of the arrow body
    // construct the y axis arrow head 1st half
    if (xAxisIsHorizontal)
      gp.lineTo(boardOriginX + (yAxisSign * arrowHeadWidth / 2), boardOriginY + (yAxisSign * arrowBodyLength));
    else
      gp.lineTo(boardOriginX + (yAxisSign * arrowBodyLength), boardOriginY - (yAxisSign * arrowHeadWidth / 2));
    // construct the y axis arrow head point
    if (xAxisIsHorizontal)
      gp.lineTo(boardOriginX, boardOriginY + (yAxisSign * arrowBodyLength) + (yAxisSign * arrowHeadLength));
    else
      gp.lineTo(boardOriginX + (yAxisSign * arrowBodyLength) + (yAxisSign * arrowHeadLength), boardOriginY);
    // construct the y axis arrow head 2nd half
    if (xAxisIsHorizontal)
      gp.lineTo(boardOriginX - (yAxisSign * arrowHeadWidth / 2), boardOriginY + (yAxisSign * arrowBodyLength));
    else
      gp.lineTo(boardOriginX + (yAxisSign * arrowBodyLength), boardOriginY + (yAxisSign * arrowHeadWidth / 2));
    if (xAxisIsHorizontal)
      gp.lineTo(boardOriginX, boardOriginY + (yAxisSign * arrowBodyLength));
    else
      gp.lineTo(boardOriginX + (yAxisSign * arrowBodyLength), boardOriginY);
    // back to board origin
    gp.lineTo(boardOriginX, boardOriginY); // move to board cad origin
    // append the circle around the board origin
    gp.append(new Ellipse2D.Double(boardOriginX - originShapeDimension / 2, boardOriginY - originShapeDimension / 2,
                                   originShapeDimension, originShapeDimension),
              false);

    initializeShape(gp);
  }
}
