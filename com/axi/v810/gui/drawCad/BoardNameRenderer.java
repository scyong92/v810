package com.axi.v810.gui.drawCad;

import java.awt.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This class is responsible for drawing the reference designator.
 *
 * @author Bill Darbie
 */
class BoardNameRenderer extends ShapeRenderer
{
  private Board _board;
  private boolean _translucent = true;
  private boolean _flipped = false;
  
  //@Kee Chin Seong
  private boolean _isStaticImage = false;

  /**
   * @author Andy Mechtenberg
   */
  BoardNameRenderer(Board board)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(board != null);
    _board = board;

    refreshData();
  }

  /**
   * @author Bill Darbie
   */
  protected void refreshData()
  {
    initializeShape(_board.getShapeRelativeToPanelInNanoMeters());
  }

  void setTranslucent(boolean translucent)
  {
    _translucent = translucent;
  }

  /**
   * @author Andy Mechtenberg
   */
  void setFlipped(boolean flipped)
  {
    _flipped = flipped;
  }
  
  /*
   * @author Kee Chin Seong
  */
  void setIsStaticImage(boolean isStaticImage)
  {
    _isStaticImage = isStaticImage;
  }

  /**
   * @author Andy Mechtenberg
   * @author Edited by Kee Chin Seong
   */
  protected void paintComponent(Graphics graphics)
  {
    int maxWidth = getWidth();
    int maxHeight = getHeight();
    int fontSize = maxHeight;  // initial guess
    Font rendererFont = FontUtil.getRendererFont(fontSize);

    rendererFont = FontUtil.getRendererFont(fontSize);
    
    String boardName = _board.getName();
    FontMetrics fontMetrics = getFontMetrics(rendererFont);
    int maxAscent = fontMetrics.getMaxAscent();
    int maxDescent = fontMetrics.getMaxDescent();

    int stringHeight = maxAscent - maxDescent;//(int)stringBounds.getHeight() - (2*fontMetrics.getMaxDescent());
    int stringWidth = fontMetrics.stringWidth(boardName);
    
    // Wei Chin : very slow here... need to refine the code...
    if((stringHeight > maxHeight) || (stringWidth > maxWidth))
    {
      fontSize = maxHeight - 2;  // make it smaller
      if(maxHeight > maxWidth)
        fontSize = maxWidth - 2;
      if(_isStaticImage)
      {
         rendererFont = FontUtil.getRendererFont(maxWidth / boardName.length());
         _isStaticImage = false;
      }
      //rendererFont = FontUtil.getRendererFont(fontSize);
      fontMetrics = getFontMetrics(rendererFont);
      maxAscent = fontMetrics.getMaxAscent();
      maxDescent = fontMetrics.getMaxDescent();
      stringHeight = maxAscent - maxDescent;//(int)stringBounds.getHeight() - (2*fontMetrics.getMaxDescent());
      stringWidth = fontMetrics.stringWidth(boardName);
    }

    int xpos = (maxWidth / 2) - (stringWidth / 2);
    int ypos = (maxHeight / 2) + (stringHeight / 2);

    Font origGraphicsFont = graphics.getFont();
    Color origColor = graphics.getColor();
    graphics.setFont(rendererFont);
    Color nameColor = getForeground();
    if (_translucent)
      graphics.setColor(new Color(nameColor.getRed(), nameColor.getGreen(), nameColor.getBlue(), 128));
    else
      graphics.setColor(nameColor);
    if (_flipped)
    {
//      AffineTransform tx = new AffineTransform();
//      tx.rotate(Math.toRadians(180.0), xpos, ypos);
      int xCenter = (maxWidth / 2);
      int yCenter = (maxHeight / 2);
      Graphics2D g2d = (Graphics2D)graphics;
      g2d.rotate(Math.toRadians(180.0), xCenter, yCenter);
      g2d.drawString(boardName, xpos, ypos);
    }
    else
    {
      graphics.drawString(boardName, xpos, ypos);
      graphics.setFont(origGraphicsFont);
      graphics.setColor(origColor);
    }
  }
}
