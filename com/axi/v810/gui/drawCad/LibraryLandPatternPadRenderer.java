package com.axi.v810.gui.drawCad;

import java.awt.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.LibraryLandPatternPad;

/**
 * This class draws a Library LandPattern Pad outline.
 *
 * @author Poh Kheng
 */
public class LibraryLandPatternPadRenderer extends ShapeRenderer
{
  private LibraryLandPatternPad _libraryLandPatternPad;

  /**
   * @author Poh Kheng
   */
  LibraryLandPatternPadRenderer(LibraryLandPatternPad libraryLandPatternPad)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(libraryLandPatternPad != null);
    _libraryLandPatternPad = libraryLandPatternPad;

    refreshData();
  }

  /**
   * @author Poh Kheng
   */
  public LibraryLandPatternPad getLibraryLandPatternPad()
  {
    Assert.expect(_libraryLandPatternPad != null);
    return _libraryLandPatternPad;
  }

  /**
   * Pad 1 needs to be hilighted (colored in RED), so we'll override the default paintComponent
   * @author Poh Kheng
   */
  protected void paintComponent(Graphics graphics)
  {
    Assert.expect(graphics != null);
    Assert.expect(_libraryLandPatternPad != null);

    Graphics2D graphics2D = (Graphics2D)graphics;

    Color origColor = graphics2D.getColor();

    if (getForeground() != getSelectedColor()) // if selected, use the selected color
    {
      if (_libraryLandPatternPad.isPadOne())
      {
        if (_libraryLandPatternPad.isThroughHolePad())
          graphics2D.setColor(LayerColorEnum.FIRST_THROUGHHOLE_PAD_COLOR.getColor());
        else
          graphics2D.setColor(LayerColorEnum.FIRST_SURFACEMOUNT_PAD_COLOR.getColor());
      }
      else if (_libraryLandPatternPad.isThroughHolePad())
        graphics2D.setColor(LayerColorEnum.THROUGHHOLE_PAD_COLOR.getColor());
    }

    graphics2D.fill(getShape(0));

    if (_libraryLandPatternPad.isThroughHolePad())
    {
      Assert.expect(getNumberOfShapes() == 2);
      // darken the color slightly
      graphics2D.setColor(graphics2D.getColor().darker());
      graphics2D.fill(getShape(1));
    }

    graphics2D.setColor(origColor);
  }


  /**
   * @author Poh Kheng
   */
  protected void refreshData()
  {
    Assert.expect(_libraryLandPatternPad != null);

    if (_libraryLandPatternPad.isThroughHolePad())
    {
      LibraryLandPatternPad libraryLandPatternPad = (LibraryLandPatternPad)_libraryLandPatternPad;
      initializeShape(_libraryLandPatternPad.getShapeInNanoMeters(), libraryLandPatternPad.getHoleShapeInNanoMeters());
    }
    else
    {
      initializeShape(_libraryLandPatternPad.getShapeInNanoMeters());
    }
  }
}
