package com.axi.v810.gui.drawCad;

import java.awt.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.packageLibrary.LibraryLandPatternPad;

/**
 * This class draws a Pad outline.
 *
 * @author Bill Darbie
 */
public class LandPatternPadRenderer extends ShapeRenderer
{
  private LandPatternPad _landPatternPad;

  /**
   * @author Andy Mechtenberg
   */
  public LandPatternPadRenderer(LandPatternPad landPatternPad)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(landPatternPad != null);
    _landPatternPad = landPatternPad;

    refreshData();
  }

  /**
   * @author Bill Darbie
   */
  public LandPatternPad getLandPatternPad()
  {
    Assert.expect(_landPatternPad != null);
    return _landPatternPad;
  }

  /**
   * Pad 1 needs to be hilighted (colored in RED), so we'll override the default paintComponent
   * @author Andy Mechtenberg
   */
  protected void paintComponent(Graphics graphics)
  {
    Assert.expect(graphics != null);
    Assert.expect(_landPatternPad != null);

    Graphics2D graphics2D = (Graphics2D)graphics;

    Color origColor = graphics2D.getColor();

    if (getForeground() != getSelectedColor()) // if selected, use the selected color
    {
      if (_landPatternPad.isPadOne())
      {
        if (_landPatternPad.isThroughHolePad())
          graphics2D.setColor(LayerColorEnum.FIRST_THROUGHHOLE_PAD_COLOR.getColor());
        else
          graphics2D.setColor(LayerColorEnum.FIRST_SURFACEMOUNT_PAD_COLOR.getColor());
      }
      else if (_landPatternPad.isThroughHolePad())
        graphics2D.setColor(LayerColorEnum.THROUGHHOLE_PAD_COLOR.getColor());
    }

    graphics2D.fill(getShape(0));

    if (_landPatternPad.isThroughHolePad())
    {
      Assert.expect(getNumberOfShapes() == 2);
      // darken the color slightly
      graphics2D.setColor(graphics2D.getColor().darker());
      graphics2D.fill(getShape(1));
    }

    graphics2D.setColor(origColor);
  }


  /**
   * Return the name of the panel
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    Assert.expect(_landPatternPad != null);
    return _landPatternPad.getName();
  }

  /**
   * @author Bill Darbie
   */
  protected void refreshData()
  {
    Assert.expect(_landPatternPad != null);

    if (_landPatternPad.isThroughHolePad())
    {
      Assert.expect(_landPatternPad instanceof ThroughHoleLandPatternPad);

      if (_landPatternPad instanceof ThroughHoleLandPatternPad)
      {
        ThroughHoleLandPatternPad throughholeLandPatternPad = (ThroughHoleLandPatternPad)_landPatternPad;

        initializeShape(_landPatternPad.getShapeInNanoMeters(), throughholeLandPatternPad.getHoleShapeInNanoMeters());
      }
      else
      {
        Assert.expect(false);
      }

    }
    else
    {
      initializeShape(_landPatternPad.getShapeInNanoMeters());
    }
  }
}
