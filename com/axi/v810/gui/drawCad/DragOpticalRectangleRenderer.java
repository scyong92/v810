/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.drawCad;

import java.awt.*;

import com.axi.guiUtil.*;
import com.axi.util.*;

/**
 *
 * @author Jack Hwee
 */
public class DragOpticalRectangleRenderer extends ShapeRenderer
{
  com.axi.v810.business.panelSettings.OpticalCameraRectangle _opticalCameraRectangle = null;
 
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  protected DragOpticalRectangleRenderer(com.axi.v810.business.panelSettings.OpticalCameraRectangle opticalCameraRectangle)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);

    Assert.expect(opticalCameraRectangle != null);
    _opticalCameraRectangle = opticalCameraRectangle;
    
    refreshData();
  }
  
  /**
   * @author Jack Hwee
   */
  protected void refreshData()
  {
    Assert.expect(_opticalCameraRectangle != null);

    initializeShape(_opticalCameraRectangle.getRegion().getRectangle2D());
  }

  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;
    
    // draw a red bounding box.
    graphics2D.setColor(Color.RED);
    graphics2D.setStroke(new BasicStroke(5));
    graphics2D.draw(getShape());
  }
}
