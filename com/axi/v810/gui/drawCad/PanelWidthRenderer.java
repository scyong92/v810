package com.axi.v810.gui.drawCad;

import java.awt.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.Panel;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

/**
 * This class is responsible for drawing the Panel Width
 *
 * @author Kee Chin Seong
 */
class PanelWidthRenderer extends TextRenderer
{
  private double _initialScaling = 1.0;
  private double _fontScaling = 1.0;
  
  private Panel _panel = null;
  //XCR-3149: Panel image and board width is upside down if panel is loaded from right to left
  private boolean _flipped = false;
  private static final double PANEL_WIDTH_TEXT_POSITION_FACTOR = 1000 * 25400;
  
  /**
   * @author Kee Chin Seong
   */
  public PanelWidthRenderer(Panel panel)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(panel != null);
    _panel = panel;

    refreshData();
  }

  /**
   * @author Kee Chin Seong
   */
  protected void refreshData()
  {
    int fontSize = (int)((MathUtil.convertUnits(_panel.getWidthInNanoMeters(), MathUtilEnum.NANOMETERS, MathUtilEnum.INCHES)) * _fontScaling);
    
    // set a default font
    setFont(FontUtil.getRendererFont(12));
    
    String panelWidth = Float.toString(MathUtil.roundToPlaces(MathUtil.convertUnits(_panel.getWidthAfterAllRotationsInNanoMeters(), MathUtilEnum.NANOMETERS, _panel.getProject().getDisplayUnits()), 2));
    
    if(_panel.getProject().getDisplayUnits().equals(MathUtilEnum.INCHES))
       panelWidth = panelWidth + " in";
    else if(_panel.getProject().getDisplayUnits().equals(MathUtilEnum.MILLIMETERS))
       panelWidth = panelWidth + " mm";
    else if(_panel.getProject().getDisplayUnits().equals(MathUtilEnum.MILS))
       panelWidth = panelWidth + " mils";

    Shape shape = _panel.getShapeInNanoMeters();
    Rectangle2D bounds = shape.getBounds2D();
    //XCR-3149: Panel image and board width is upside down if panel is loaded from right to left
    if (_flipped)
      initializeText(panelWidth, (bounds.getCenterX()) * _initialScaling, -(PANEL_WIDTH_TEXT_POSITION_FACTOR * _initialScaling), true);
    else
      initializeText(panelWidth, (bounds.getCenterX()) * _initialScaling, (bounds.getMaxY() + PANEL_WIDTH_TEXT_POSITION_FACTOR) * _initialScaling, true);
  }

  /*
   * @author Kee Chin Seong
   */
  public void scalingFontToFitInPanel(boolean isNeedFitInPanelSetup)
  {
    if(isNeedFitInPanelSetup)
       _fontScaling = 10;
    else
    {
       String panelWidth = Float.toString(MathUtil.roundToPlaces(MathUtil.convertUnits(_panel.getWidthAfterAllRotationsInNanoMeters(), MathUtilEnum.NANOMETERS, _panel.getProject().getDisplayUnits()), 2));
       _fontScaling = getWidth() / (panelWidth.length() * 6);
    }
       
    refreshData();
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setInitialScaling(double initialScaling)
  {
    Assert.expect(initialScaling > 0);

    _initialScaling = initialScaling;
    refreshData();
  }

  /**
   * @author Kee Chin Seong
   */
  public Panel getPanel()
  {
    Assert.expect(_panel != null);
    return _panel;
  }
  
  /**
   * @author hee-jihn.chuah - XCR-3149: Panel image and board width is upside down if panel is loaded from right to left
   */
  public void setFlipped(boolean flipped)
  {
    _flipped = flipped;
    refreshData();
    super.setFlipped(flipped);
  }
}