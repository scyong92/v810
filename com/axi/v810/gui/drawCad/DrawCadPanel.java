package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import com.axi.v810.util.*;
import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.images.*;
import com.axi.v810.hardware.*;

/**
 * This is a standalone panel that will fit into the MainMenu frame.
 * @author Andy Mechtenberg
 */
public class DrawCadPanel extends JPanel
{
  private int _defaultWidth = 800;
  private int _defaultHeight = 600;
  private PanelGraphicsEngineSetup _panelGraphicsEngineSetup;
  private BoardGraphicsEngineSetup _boardGraphicsEngineSetup;
  private CompPackageGraphicsEngineSetup _compPackageGraphicsEngineSetup;
  private LandPatternGraphicsEngineSetup _landPatternGraphicsEngineSetup;
  private LibraryPackageGraphicsEngineSetup _libraryPackageGraphicsEngineSetup;
  private GraphicsEngine _graphicsEngine;
  private ControlToolBar _controlToolBar;
  private JPanel _statusPanel = new JPanel();
  private JLabel _selectedDescriptionLabel = new JLabel();
  private FlowLayout _statusPanelFlowLayout = new FlowLayout();
  private JPanel _exitInfoPanel = new JPanel();
  private JLabel _exitInfoLabel = new JLabel();
  private FlowLayout _exitInfoPanelFlowLayout = new FlowLayout();
  private JPanel _eastArrowPanel = new JPanel();
  private JPanel _westArrowPanel = new JPanel();
  private boolean _drawed = false;


  /**
   * This is a function used only when the "main" in drawCad is run
   * Customers will not use this method -- DrawCAD will use the CAD loaded
   * via the normal 5dx user interface
   * @author George A. David
   */
  private Project loadProject()
  {
    Project project = null;
    try
    {
      java.util.List<LocalizedString> warnings = new ArrayList<LocalizedString>();

      String projectName = "";
//       projectName = "CISCO_SWITCH";
//       projectName = "PUERTO_RICO";
//       projectName = "NEPCON_WIDE";
//       projectName = "GEORGE_TEST";
//       projectName = "ROTATED_SLEDGE";
//       projectName = "5064_8726_A3_B";
//       projectName = "DRAWCADTEST";
//       projectName = "HP5DX_512";
//       projectName = "digby_650_single";
       projectName = "digby_orig";
//       projectName = "FAMILIES_ALL_RLV";
//       projectName = "SLEDGEHAMMER";
//       projectName = "0027Parts";
//       projectName = "41k_joints_from_NDFs";
//       projectName = "MOTO_SPEED";
//       projectName = "NEPCON_WIDE_ANDY";
//       projectName = "NEPCON0";
//       projectName = "NEPCON90";
//       projectName = "NEPCON180";
//       projectName = "NEPCON270";

//      projectName = "xraymap"; // single sided, bottom side only
//      projectName = "PALISADES_PARTIAL"; // single sided, top side only
//      projectName = "nepcon_onesided";  // a single side board, in a 2-up panel, one is flipped
//      projectName = "nepcon_onesidebot";  // a single side board, in a 2-up panel, one is flipped
//      projectName = "CYGNUS_ASAP_81";  // tests a top side reference point application
//      projectName = "PT_552_0007_200_R05_XL"; // tests a surface map point defect -- fuzzy compare was not fuzzy enough in SurfaceMapRtfReader, updateNdfList
//      projectName = "E1418-66502"; // tests another case where load is failing on surface map points -- single sided, populated side down, panel rotated 90 degrees

      long startTime = System.currentTimeMillis();
      project = Project.importProjectFromNdfs(projectName, warnings);
      long stopTime = System.currentTimeMillis();
//      System.out.println("Load: " + (stopTime - startTime));
    }
    catch(Exception e)
    {
      Assert.logException(e);
    }

    Assert.expect(project != null);
    return project;
  }

  /**
   * @author Andy Mechtenberg
   */
  public DrawCadPanel()
  {
    _graphicsEngine = new GraphicsEngine(0);
    if (_controlToolBar == null)
      _controlToolBar = new ControlToolBar(this, _graphicsEngine);
    _controlToolBar.disableAllControls();

    jbInit(false);
  }

  /**
   * @author Andy Mechtenberg
   */
  public DrawCadPanel(int width, int height)
  {
    Assert.expect(width > 0, "Width was too small: " + width);
    Assert.expect(height > 0, "Height was too small: " + height);
    _defaultWidth = width;
    _defaultHeight = height;

    _graphicsEngine = new GraphicsEngine(0);
    if (_controlToolBar == null)
      _controlToolBar = new ControlToolBar(this, _graphicsEngine);
    _controlToolBar.disableAllControls();

    jbInit(false);
  }

  /**
   * The constructor called when creating a drawCAD of custom size.  Used when trying to
   * create a JPG of the panel image.
   * @param project Project data to use
   * @param width size in pixels
   * @param height size in pixels
   * @author Andy Mechtenberg
   */
  public DrawCadPanel(Project project, int width, int height, boolean flipped)
  {
    Assert.expect(project != null, "Project is NULL");
    Assert.expect(width > 0, "Width was too small: " + width);
    Assert.expect(height > 0, "Height was too small: " + height);
    _defaultWidth = width;
    _defaultHeight = height;

    _graphicsEngine = new GraphicsEngine(0);
    _panelGraphicsEngineSetup = new PanelGraphicsEngineSetup(this, _graphicsEngine);
//    _graphicsEngine = _panelGraphicsEngineSetup.getGraphicsEngine();

    setPreferredSize(new Dimension(_defaultWidth, _defaultHeight));
    setLayout(new BorderLayout());
    add(_graphicsEngine, BorderLayout.CENTER);

    _panelGraphicsEngineSetup.displayPanelForStaticImageGeneration(project.getPanel(), flipped);
  }

  /**
   * Sets up this DrawCadPanel to display the normal Panel graphics
   * @author Andy Mechtenberg
   */
  public void drawPanel(Project project)
  {
    Assert.expect(project != null);

    // we only need to do this if the panel grapics engine setup class
    // has not been initialized otherwise, it will be listening to datastore
    // events and will repopulate itself as appropriate.
    if(_panelGraphicsEngineSetup == null)
    {
      _panelGraphicsEngineSetup = new PanelGraphicsEngineSetup(this, _graphicsEngine);
//      _graphicsEngine = _panelGraphicsEngineSetup.getGraphicsEngine();
    }
    else
      _graphicsEngine.reset();

    _panelGraphicsEngineSetup.displayPanel(project.getPanel());
    _panelGraphicsEngineSetup.addObservers();

    Assert.expect(_controlToolBar != null);
    _controlToolBar.resetForNewPanel(_panelGraphicsEngineSetup, project.getPanel());
    VirtualLiveImageGenerationSettings.getInstance().setPanelShape(project.getPanel().getShapeInNanoMeters());

    jbInit(true);
    invalidate();
  }

  /**
   * Sets up this DrawCadPanel to display the normal Panel graphics
   * @author Andy Mechtenberg
   */
  public void drawBoard(Board board)
  {
    Assert.expect(board != null);

    // we only need to do this if the panel grapics engine setup class
    // has not been initialized otherwise, it will be listening to datastore
    // events and will repopulate itself as appropriate.
    if(_boardGraphicsEngineSetup == null)
    {
      _boardGraphicsEngineSetup = new BoardGraphicsEngineSetup(this, _graphicsEngine);
//      _graphicsEngine = _panelGraphicsEngineSetup.getGraphicsEngine();
    }
    else
      _graphicsEngine.reset();

    _boardGraphicsEngineSetup.displayBoard(board);
    _boardGraphicsEngineSetup.addObservers();

    Assert.expect(_controlToolBar != null);
    _controlToolBar.resetForNewBoard(_boardGraphicsEngineSetup, board);

    jbInit(true);
    invalidate();
  }

  /**
   * Sets up this DrawCadPanel to display the normal Panel graphics
   * @author Andy Mechtenberg
   */
  public void drawPackage(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);

    if (_compPackageGraphicsEngineSetup == null)
      _compPackageGraphicsEngineSetup = new CompPackageGraphicsEngineSetup(this, _graphicsEngine);

    _compPackageGraphicsEngineSetup.displayPackage(compPackage);

//    _graphicsEngine = _compPackageGraphicsEngineSetup.getGraphicsEngine();
    Assert.expect(_controlToolBar != null);
    _controlToolBar.resetForNewPackageOrLandPattern(_graphicsEngine);
    _controlToolBar.enableAllControls();


    jbInit(false);
  }

  /**
   * Sets up this DrawCadPanel to display the normal Panel graphics
   * @author Andy Mechtenberg
   */
  public void drawLandPattern(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);

    if (_landPatternGraphicsEngineSetup == null)
      _landPatternGraphicsEngineSetup = new LandPatternGraphicsEngineSetup(this, _graphicsEngine);

    _landPatternGraphicsEngineSetup.displayLandPattern(landPattern);
//    _graphicsEngine = _landPatternGraphicsEngineSetup.getGraphicsEngine();

    Assert.expect(_controlToolBar != null);
    _controlToolBar.resetForNewPackageOrLandPattern(_graphicsEngine);
    
    //Swee-Yee.Wong
    //Disable DisplayPadNameButton when called in EditLandPatternTab
    java.util.List<String> excludeList = new java.util.LinkedList<String>();
    excludeList.add(_controlToolBar.getDisplayPadNameToggleButtonName());
    _controlToolBar.enableAllControlsWithExcludeList(excludeList);

    jbInit(false);
  }

  /**
   * Sets up this DrawCadPanel to display the normal Panel graphics
   * @author Poh Kheng
   */
  public void drawLibraryPackage(LibraryPackage libraryPackage)
  {
    Assert.expect(libraryPackage != null);

    if (_libraryPackageGraphicsEngineSetup == null)
      _libraryPackageGraphicsEngineSetup = new LibraryPackageGraphicsEngineSetup(this, _graphicsEngine);

    _libraryPackageGraphicsEngineSetup.displayLibraryPackage(libraryPackage);

    Assert.expect(_controlToolBar != null);
    _controlToolBar.resetForNewPackageOrLandPattern(_graphicsEngine);
    _controlToolBar.enableAllControls();

    jbInit(false);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void clearGraphics()
  {
    _graphicsEngine = new GraphicsEngine(0);
    Assert.expect(_controlToolBar != null);
    _controlToolBar.disableAllControls();

    jbInit(false);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void resetAll()
  {
    _drawed = false;
    _graphicsEngine.reset();
    _controlToolBar.disableAllControls();
    _controlToolBar.unpopulate();
    jbInit(false);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void resetPanelGraphics()
  {
    resetAll();
    if (_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.removeObservers();
      _panelGraphicsEngineSetup.unpopulate();
    }
  }

  /**
   * @author Wei Chin
   */
  public void resetBoardGraphics()
  {
    resetAll();
    if (_boardGraphicsEngineSetup != null)
    {
      _boardGraphicsEngineSetup.removeObservers();
      _boardGraphicsEngineSetup.unpopulate();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void resetPackageGraphics()
  {
    resetAll();
    if (_compPackageGraphicsEngineSetup != null)
      _compPackageGraphicsEngineSetup.reset();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void resetLandPatternGraphics()
  {
    resetAll();
    if(_landPatternGraphicsEngineSetup != null)
      _landPatternGraphicsEngineSetup.reset();
  }

  /**
   * @author Poh Kheng
   */
  public void resetLibraryPackageGraphics()
  {
    resetAll();
    if(_libraryPackageGraphicsEngineSetup != null)
      _libraryPackageGraphicsEngineSetup.reset();
  }

  /**
   * @author Andy Mechtenberg
   */
  public PanelGraphicsEngineSetup getPanelGraphicsEngineSetup()
  {
    Assert.expect(_panelGraphicsEngineSetup != null);

    return _panelGraphicsEngineSetup;
  }
  
  /*
   * @author Kee Chin Seong - To activate the graphic engine drag mode
   */
  public void setGroupSelectMode(boolean groupSelectMode)
  {
    Assert.expect(_controlToolBar != null);
    
    _controlToolBar.setGroupSelectMode(groupSelectMode);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean hasBoardGraphicsEngineSetup()
  {
    if (_boardGraphicsEngineSetup != null)
      return true;
    return false;
  }

  /**
   * @author Wei Chin
   */
  public BoardGraphicsEngineSetup getBoardGraphicsEngineSetup()
  {
    Assert.expect(_boardGraphicsEngineSetup != null);

    return _boardGraphicsEngineSetup;
  }

  /**
   * @author Andy Mechtenberg
   */
  public CompPackageGraphicsEngineSetup getCompPackageGraphicsEngineSetup()
  {
    Assert.expect(_compPackageGraphicsEngineSetup != null);

    return _compPackageGraphicsEngineSetup;
  }

  /**
   * @author Andy Mechtenberg
   */
  public LandPatternGraphicsEngineSetup getLandPatternGraphicsEngineSetup()
  {
    Assert.expect(_landPatternGraphicsEngineSetup != null);

    return _landPatternGraphicsEngineSetup;
  }

  /**
   * @author Poh Kheng
   */
  public LibraryPackageGraphicsEngineSetup getLibraryPackageGraphicsEngineSetup()
  {
    Assert.expect(_libraryPackageGraphicsEngineSetup != null);

    return _libraryPackageGraphicsEngineSetup;
  }

  /**
   * @author Wei Chin
   */
  public void initBoardGraphicsEngineSetup()
  {
     if (_boardGraphicsEngineSetup == null)
      _boardGraphicsEngineSetup = new BoardGraphicsEngineSetup(this, _graphicsEngine);
  }

  /**
   * @author Laura Cormos
   */
  public void initLandPatternGraphicsEngineSetup()
  {
    if (_landPatternGraphicsEngineSetup == null)
      _landPatternGraphicsEngineSetup = new LandPatternGraphicsEngineSetup(this, _graphicsEngine);
  }

  /**
   * @author Poh Kheng
   */
  public void initLibraryPackageGraphicsEngineSetup()
  {
    if (_libraryPackageGraphicsEngineSetup == null)
      _libraryPackageGraphicsEngineSetup = new LibraryPackageGraphicsEngineSetup(this, _graphicsEngine);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void initPackageGraphicsEngineSetup()
  {
    if (_compPackageGraphicsEngineSetup == null)
      _compPackageGraphicsEngineSetup = new CompPackageGraphicsEngineSetup(this, _graphicsEngine);
  }

  /**
   * Create a line-drawing panel.jpg using gui/drawCad technology
   * @author Andy Mechtenberg
   * @author Lim Boon Hong 
   */
  public static void createPanelImage(final Project project)
  {
    Assert.expect(project != null);

    createPanelImage(project, true);
  }
  
  /**
   * Create a line-drawing panel.jpg using gui/drawCad technology
   * @author Andy Mechtenberg
   * @author Lim Boon Hong 
   * @author Siew Yeng 
   * @edited Kee chin Seong - Existing image will not be replace if there is already have 1 
   * use this for serial number mapping dialog
   */
  public static void createPanelImage(final Project project, final boolean saveProject)
  {
    Assert.expect(project != null);

    final String panelImageName;
    final String panelFlippedImageName;
    
    if(saveProject)
    {
      //create panel image when save project - Siew Yeng
      panelImageName = FileName.getPanelImageFullPath(project.getName());
      panelFlippedImageName = FileName.getPanelFlippedImageFullPath(project.getName());
    }
    else
    {
      //create panel image for serial number mapping - Siew Yeng
      panelImageName = FileName.getTempPanelImageFullPath(project.getName());
      panelFlippedImageName = FileName.getTempPanelFlippedImageFullPath(project.getName());
    }
    
    // don't create it if it already exists  - on the x6000, we'll overwrite it, so this is commented out
    // Kee Chin Seong, If the image is there dont replace it.
    // Cheah Lee Herng - 11 April 2016 - Temporary comment out the following 2 lines as S&S think that system
    //                                   need to update panel.png whenever they perform panel rotation / flip
    //if (FileUtilAxi.exists(panelImageName))
    //    return;

    try
    {
      SwingUtils.invokeAndWait(new Runnable()
      {
        public void run()
        {
          long startTime = System.currentTimeMillis();

          CadFrame frame = new CadFrame();
          DrawCadPanel cadPanel = new DrawCadPanel(project, 200, 200, false); // 200, 200 is small, but the jpg will scale UP better than DOWN
          frame.getContentPane().add(cadPanel);
          frame.pack();
//          frame.setVisible(true);
          try
          {
            cadPanel.writeToFile(panelImageName);
            ProjectObservable.getInstance().stateChanged(project, null, null, ProjectEventEnum.PANEL_IMAGE_CREATION);
//            frame.setVisible(true);
          }
          catch (FileNotFoundException fnfe)
          {
            // do nothing.  If we fail to create, it's not a showstopper
          }
          catch (IOException ioe)
          {
            // do nothing.  If we fail to create, it's not a showstopper
          }
          finally
          {
            cadPanel.dispose();
            frame.dispose();
            cadPanel = null;
            frame = null;
          }
          frame = new CadFrame();
          cadPanel = new DrawCadPanel(project, 200, 200, true); // 200, 200 is small, but the jpg will scale UP better than DOWN
          frame.getContentPane().add(cadPanel);
          frame.pack();
//          frame.setVisible(true);
          try
          {
            cadPanel.writeToFile(panelFlippedImageName);
            ProjectObservable.getInstance().stateChanged(project, null, null, ProjectEventEnum.PANEL_IMAGE_CREATION);
//            frame.setVisible(true);
          }
          catch (FileNotFoundException fnfe)
          {
            // do nothing.  If we fail to create, it's not a showstopper
          }
          catch (IOException ioe)
          {
            // do nothing.  If we fail to create, it's not a showstopper
          }
          finally
          {
            cadPanel.dispose();
            frame.dispose();
            cadPanel = null;
            frame = null;
          }
          
          /**
          * @To generate image set for different board 
          * @author Lim Boon Hong 
          */
          for (Board selectedBoard:  project.getPanel().getBoards())
          {
            cadPanel = new DrawCadPanel(project, 200, 200, false); // 200, 200 is small, but the jpg will scale UP better than DOWN

            frame = new CadFrame();
            frame.getContentPane().add(cadPanel);
            frame.pack();
  //          frame.setVisible(true);

            String boardFilePath;
            if(saveProject)
            {
              boardFilePath = FileName.getPanelBoardImageFullPath(project.getName(), selectedBoard.getName());
            }
            else
            {
              boardFilePath = FileName.getTempPanelBoardImageFullPath(project.getName(), selectedBoard.getName());
            }
            
            try
            {
              cadPanel.getPanelGraphicsEngineSetup().setHighlightBoard(selectedBoard);
              cadPanel.writeToFile(boardFilePath);
              ProjectObservable.getInstance().stateChanged(project, null, null, ProjectEventEnum.PANEL_IMAGE_CREATION);
  //            frame.setVisible(true);
            }
            catch (FileNotFoundException fnfe)
            {
              // do nothing.  If we fail to create, it's not a showstopper
            }
            catch (IOException ioe)
            {
              // do nothing.  If we fail to create, it's not a showstopper
            }
            finally
            {
              cadPanel.dispose();
              frame.dispose();
              cadPanel = null;
              frame = null;
            }
          }
          
          //Generate image set for flip image. By Lim Boon Hong
          for (Board selectedBoard:  project.getPanel().getBoards())
          {
            cadPanel = new DrawCadPanel(project, 200, 200, true); // 200, 200 is small, but the jpg will scale UP better than DOWN

            frame = new CadFrame();
            frame.getContentPane().add(cadPanel);
            frame.pack();
  //          frame.setVisible(true);

            String boardFilePath;
            if(saveProject)
            {
              boardFilePath = FileName.getPanelBoardFlippedImageFullPath(project.getName(), selectedBoard.getName());
            }
            else
            {
              boardFilePath = FileName.getTempPanelBoardFlippedImageFullPath(project.getName(), selectedBoard.getName());
            }
         
            try
            {
              cadPanel.getPanelGraphicsEngineSetup().setHighlightBoard(selectedBoard);
              cadPanel.writeToFile(boardFilePath);
              ProjectObservable.getInstance().stateChanged(project, null, null, ProjectEventEnum.PANEL_IMAGE_CREATION);
  //            frame.setVisible(true);
            }
            catch (FileNotFoundException fnfe)
            {
              // do nothing.  If we fail to create, it's not a showstopper
            }
            catch (IOException ioe)
            {
              // do nothing.  If we fail to create, it's not a showstopper
            }
            finally
            {
              cadPanel.dispose();
              frame.dispose();
              cadPanel = null;
              frame = null;
            }
          }
           
          long endTime = System.currentTimeMillis();
//          if (UnitTest.unitTesting() == false)
//            System.out.println("Panel Image Creation Time:  " + (endTime - startTime));
        }
      });
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }


  /**
   * @author Bill Darbie
   */
  private void jbInit(boolean useLoadArrows)
  {
    JPanel centerPanel = new JPanel(new BorderLayout());
    removeAll();
    setPreferredSize(new Dimension(_defaultWidth, _defaultHeight));
    setBackground(Color.black);
    setLayout(new BorderLayout());
    _statusPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _statusPanelFlowLayout.setVgap(0);
    _statusPanel.setLayout(_statusPanelFlowLayout);
//    _statusPanelGridLayout.setColumns(2);
//    _exitInfoLabel.setText(StringLocalizer.keyToString("DCGUI_EXITINFORMATION_KEY"));
    _exitInfoPanel.setLayout(_exitInfoPanelFlowLayout);
    _exitInfoPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _exitInfoPanelFlowLayout.setHgap(0);
    _exitInfoPanelFlowLayout.setVgap(0);
    _statusPanel.setBorder(BorderFactory.createLoweredBevelBorder());
    centerPanel.add(_controlToolBar, BorderLayout.NORTH);
    centerPanel.add(_graphicsEngine, BorderLayout.CENTER);
    centerPanel.add(_statusPanel,  BorderLayout.SOUTH);
    _statusPanel.add(_selectedDescriptionLabel, null);
    _statusPanel.add(_exitInfoPanel, null);
    _exitInfoPanel.add(_exitInfoLabel, null);
    _selectedDescriptionLabel.setText(" ");
    add(centerPanel, BorderLayout.CENTER);
    if (useLoadArrows)
    {
//      JPanel eastPanel = new JPanel(new BorderLayout());
//      JPanel westPanel = new JPanel(new BorderLayout());
//      eastPanel.add(_eastUpArrowLabel, BorderLayout.EAST);
//      westPanel.add(_westUpArrowLabel, BorderLayout.WEST);
//      add(eastPanel, BorderLayout.EAST);
//      add(westPanel, BorderLayout.WEST);

      _eastArrowPanel.setLayout(new BorderLayout());
      _eastArrowPanel.setBorder(BorderFactory.createEmptyBorder(0,0,0,5));
      _eastArrowPanel.setBackground(Color.black);
      _westArrowPanel.setLayout(new BorderLayout());
      _westArrowPanel.setBorder(BorderFactory.createEmptyBorder(0,5,0,0));
      _westArrowPanel.setBackground(Color.black);
      setPanelLoadArrows();
      // on the black
//      centerPanel.setBackground(Color.black);
      centerPanel.add(_eastArrowPanel, BorderLayout.EAST);
      centerPanel.add(_westArrowPanel, BorderLayout.WEST);
    }
  }

  /**
   * @author George Booth
   */
  public void setPanelLoadArrows()
  {
    _eastArrowPanel.removeAll();
    _westArrowPanel.removeAll();
    JLabel eastArrowLabel;
    JLabel westArrowLabel;
    PanelHandlerPanelFlowDirectionEnum flowDirection = PanelHandler.getInstance().getPanelFlowDirection();
    if (flowDirection == PanelHandlerPanelFlowDirectionEnum.LEFT_TO_RIGHT)
    {
      // use up arrows if handler is left-to-right ("normal")
      eastArrowLabel = new JLabel(Image5DX.getImageIcon(Image5DX.UP_ARROW));
      westArrowLabel = new JLabel(Image5DX.getImageIcon(Image5DX.UP_ARROW));
    }
    else
    {
      // use down arrows if handler is right-to-left
      eastArrowLabel = new JLabel(Image5DX.getImageIcon(Image5DX.DOWN_ARROW));
      westArrowLabel = new JLabel(Image5DX.getImageIcon(Image5DX.DOWN_ARROW));
    }
    _eastArrowPanel.add(eastArrowLabel, BorderLayout.CENTER);
    _westArrowPanel.add(westArrowLabel, BorderLayout.CENTER);
    repaint();
  }

  /**
   * @author Andy Mechtenberg
   * @param description - string to be displayed on the status bar
   */
  public void setDescription(String description)
  {
    _selectedDescriptionLabel.setText(description);
  }

/**
 * Converts all shapes from nano meter based to pixel based
 * This must be called on the Swing thread!
 * @author Andy Mechtenberg
 */
  public void scaleDrawing()
  {
    _graphicsEngine.fitGraphicsToScreen();
    _graphicsEngine.mark();
    if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment() == false)
      _graphicsEngine.setGroupSelectMode(true);
    //XCR-3358, arrow down keyboard on Optical rectangles/Components table for board based recipe is not functioning
    //_controlToolBar.selectText();
    _drawed=true;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void writeToFile(String fileName) throws FileNotFoundException, IOException
  {
    Assert.expect(fileName != null);
    _graphicsEngine.fitGraphicsToScreen();
    _graphicsEngine.setWidthAndHeight();
    _graphicsEngine.writeToFile(fileName, 5);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setPanelInvisible()
  {
    _graphicsEngine.setVisible(_panelGraphicsEngineSetup.getTopLayers(), false);
    _graphicsEngine.setVisible(_panelGraphicsEngineSetup.getBottomLayers(), false);
  }

  /**
   * Removes all the renderers to free up the memory
   * This must be called on the Swing thread!
   * @author Andy Mechtenberg
   */
  public void dispose()
  {
    _graphicsEngine.dispose();
  }
  
   /**
   * @author Jack Hwee
   */
  public void changeToSurfaceMappingToolBar(Boolean inSurfaceMap)
  {
    _controlToolBar.changeToSurfaceMappingToolBar(inSurfaceMap);   
  }
  
  /**
   * @author sham
   */
  public boolean isCadDrawed()
  {
    return _drawed;
  }

  /**
   * @author sham
   */
  public void setEnableSelectRegionToggleButton(boolean enable)
  {
	Assert.expect(_controlToolBar != null);	

    _controlToolBar.setEnableSelectRegionToggleButon(enable);
  }

  /**
   * @author sham
   */
  public void switchToVirtualLiveModeDisplay()
  {
    Assert.expect(_statusPanel != null);

    if(isCadDrawed())
    {
      Assert.expect(_panelGraphicsEngineSetup != null);
      Assert.expect(_graphicsEngine != null);
      _panelGraphicsEngineSetup.drawVirtualLiveAreaDisplay();
      _graphicsEngine.fitGraphicsToScreen();
    }
    _statusPanel.setVisible(false);
  }

  /*
   * @author Kee Chin Seong
   */
  public void switchToCadCreatorModeDisplay()
  {
    Assert.expect(_statusPanel != null);

    if(isCadDrawed())
    {
      Assert.expect(_panelGraphicsEngineSetup != null);
      Assert.expect(_graphicsEngine != null);
      
      _graphicsEngine.fitGraphicsToScreen();
    }
    _statusPanel.setVisible(false);
  }
  
  /**
   * @author sham
   */
  public void switchToTestDevModeDisplay()
  {
    Assert.expect(_statusPanel != null);

    if(isCadDrawed())
    {
      Assert.expect(_panelGraphicsEngineSetup != null);
      _panelGraphicsEngineSetup.clearVirtualLiveAreaDisplay();
    }
    _statusPanel.setVisible(true);
  }

  /*
   * Kee, Chin Seong
   */
  public void changeToScanPathToolBar(Boolean isScanPathReviewEnabled)
  {
    _controlToolBar.changeToScanPathToolBar(isScanPathReviewEnabled);
  }
  
  /*
   * @author Bee Hoon
   */
  public void removePanelGraphicObserver()
  {
    if (_panelGraphicsEngineSetup != null)
      _panelGraphicsEngineSetup.removeObservers();
  }
  
  /*
   * @author Bee Hoon
   */
  public void addPanelGraphicObserver()
  {
    if (_panelGraphicsEngineSetup != null)
      _panelGraphicsEngineSetup.addObservers();
  }
  
  /*
   * @author Bee Hoon
   */
  public void removeBoardGraphicObserver()
  {
    if (_boardGraphicsEngineSetup != null)
      _boardGraphicsEngineSetup.removeObservers();
  }
  
  /*
   * @author Bee Hoon
   */
  public void addBoardGraphicObserver()
  {
    if (_boardGraphicsEngineSetup != null)
    {
      // XCR-2135 - Add observers only if the current displayed board is not null. - Ying-Huan.Chu
      if (_boardGraphicsEngineSetup.getCurrentDisplayBoard() != null)
      {
        _boardGraphicsEngineSetup.addObservers();
      }
    }
  }
  
   /**
   * @author Jack Hwee
   */
  public void updateGroupSelectButton(Boolean isGroupSelectMode)
  {
    Assert.expect(_controlToolBar != null);
    _controlToolBar.updateGroupSelectButton(isGroupSelectMode);
  }
  
  /**
   * Sets up this DrawCadPanel to display the normal Panel graphics
   *
   * @author Andy Mechtenberg
   */
  public void resetSurfaceMapPanel(Project project)
  {
    Assert.expect(_controlToolBar != null);
    Assert.expect(project != null);
    _controlToolBar.resetForNewPanel(_panelGraphicsEngineSetup, project.getPanel());
  }
  
  /**
   * @author Janan Wong
   * XCR-3837: Display untestable Area on virtual live CAD
   */
  public void setEnableDisplayUntestableAreaToggleButton(boolean enable)
  {
    Assert.expect(_controlToolBar != null);	
    _controlToolBar.setEnableDisplayUntestableAreaToggleButton(enable);
  }
}

