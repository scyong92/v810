package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PackagePinNameRenderer extends TextRenderer 
{
    private PackagePin _packagePin;
    
    /**
     * @author Cheah Lee Herng
     */
    public PackagePinNameRenderer(PackagePin packagePin)
    {
        super(Renderer._COMPONENT_IS_SELECTABLE);
        Assert.expect(packagePin != null);
        _packagePin = packagePin;
        
        refreshData();
    }
    
    /**
     * @author George A. David
    */
    protected void refreshData()
    {
        Assert.expect(_packagePin != null);
        
        String packagePinName = _packagePin.getName();
        
        Shape shape = _packagePin.getShapeInNanoMeters();
        Rectangle2D bounds = shape.getBounds2D();
       
//        System.out.println("_packagePin number of pins" + _packagePin.getCompPackage().getPackagePins().size());
        // set a default font
        int totalPins = _packagePin.getCompPackage().getPackagePins().size();
        if(totalPins < 10)
          setFont(FontUtil.getRendererFont(48));
        else if(totalPins < 100)
          setFont(FontUtil.getRendererFont(22));
        else if(totalPins < 300)
          setFont(FontUtil.getRendererFont(18));
        else if(totalPins < 1000)
          setFont(FontUtil.getRendererFont(11));
        else
          setFont(FontUtil.getRendererFont(6));
            
        initializeText(packagePinName, bounds.getCenterX(), bounds.getCenterY(), true);
    }
}