package com.axi.v810.gui.drawCad;

import java.awt.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;

/**
 *
 * <p>Title: LibraryPackagePinRenderer</p>
 *
 * <p>Description: This class render the Library Package Pin</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LibraryPackagePinRenderer extends PackagePinRenderer
{
  private LibraryPackagePin _libraryPackagePin;

  LibraryPackagePinRenderer(LibraryPackagePin libraryPackagePin)
  {
    _libraryPackagePin = libraryPackagePin;

    refreshData();
  }

  /**
   * @author Tan Hock Zoon
   */
  protected void refreshData()
  {
    Assert.expect(_libraryPackagePin != null);

    initializeShape(_libraryPackagePin.getLibraryLandPatternPad().getShapeInNanoMeters());

    Assert.expect(_libraryPackagePin != null);

    LibraryLandPatternPad landPatternPad = _libraryPackagePin.getLibraryLandPatternPad();
    if (landPatternPad.isThroughHolePad())
    {
      initializeShape(landPatternPad.getShapeInNanoMeters(), landPatternPad.getHoleShapeInNanoMeters());
    }
    else
    {
      initializeShape(landPatternPad.getShapeInNanoMeters());
    }
  }

  /**
   *
   * @param graphics Graphics
   *
   * @author Tan Hock Zoon
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    Color origColor = graphics2D.getColor();

    boolean isThroughHole = _libraryPackagePin.getLibraryLandPatternPad().isThroughHolePad();

    if (getForeground() != getSelectedColor()) // if selected, use the selected color
    {      
      if (_libraryPackagePin.getLibraryLandPatternPad().isPadOne())
      {
        if (isThroughHole)
          graphics2D.setColor(LayerColorEnum.FIRST_THROUGHHOLE_PAD_COLOR.getColor());
        else
          graphics2D.setColor(LayerColorEnum.FIRST_SURFACEMOUNT_PAD_COLOR.getColor());
      }
      else if (isThroughHole)
        graphics2D.setColor(LayerColorEnum.THROUGHHOLE_PAD_COLOR.getColor());
    }

    graphics2D.fill(getShape());

    if (isThroughHole)
    {
      Assert.expect(getNumberOfShapes() == 2);
      // darken the color slightly
      graphics2D.setColor(graphics2D.getColor().darker());
      graphics2D.fill(getShape(1));
    }

    // now do orientation arrow
    super.drawOrientationArrow(graphics2D, _libraryPackagePin.getPinOrientationAfterRotationEnum());

    graphics2D.setColor(origColor);
  }

  /**
   *
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String toString()
  {
    String packagePinDesc = _libraryPackagePin.getName() + " " + _libraryPackagePin.getJointTypeName();

    return packagePinDesc;
  }
}
