package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.cadcreator.CadCreatorSelectedAreaSettings;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelDesc.Component;
import com.axi.v810.business.panelDesc.Panel;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.virtualLive.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class sets up the GraphicsEngine for a particular Panel so the
 * GraphicsEngine can draw the panels CAD graphics
 *
 * @author Chin-Seong, Kee
 */
public class CreatePanelGraphicsEngineSetup extends Observable implements RendererCreator, Observer
{
  private static final int _BORDER_SIZE_IN_PIXELS = 15;//50;

  private static GuiObservable _guiObservable;

  private GraphicsEngine _graphicsEngine;
  private CreateNewCadPanel _createNewCadPanel;
  private CadCreatorControlToolBar _controlToolBar;
  private Panel _panel;

  private int _layerNumber = -1;
  private int _topPanelLayer;
  private int _topPanelFiducialLayer;
  private int _topPanelFiducialRefDesLayer;
  
  private Map<Board, Integer> _boardToTopBoardLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToTopPadLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToTopComponentLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToTopRefDesLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToTopFiducialLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToOriginLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToBoardNameLayerMap = new HashMap<Board, Integer>();
  private int _bottomPanelLayer;
  private int _bottomPanelFiducialLayer;
  private int _bottomPanelFiducialRefDesLayer;
  private Map<Board, Integer> _boardToBottomBoardLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToBottomPadLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToBottomComponentLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToBottomRefDesLayerMap = new HashMap<Board, Integer>();
  private Map<Board, Integer> _boardToBottomFiducialLayerMap = new HashMap<Board, Integer>();

  private int _topPadNameLayer;
  private int _bottomPadNameLayer;
  
  private Map<BoardType, List<BoardRenderer>> _boardTypeToBoardRenderersMap = new HashMap<BoardType, List<BoardRenderer>>();
  private Map<ComponentType, List<ComponentRenderer>> _componentTypeToComponentRenderersMap = new HashMap<ComponentType, List<ComponentRenderer>>();
  private Map<ComponentType, List<ReferenceDesignatorRenderer>> _componentTypeToReferenceDesignatorRenderersMap =
      new HashMap<ComponentType,  List<ReferenceDesignatorRenderer>>();
  private Map<Pad, PadRenderer> _padToPadRendererMap = new HashMap<Pad, PadRenderer>();
  private Map<ComponentRenderer, List<PadRenderer>> _componentRendererToPadRenderersMap = new HashMap<ComponentRenderer, List<PadRenderer>>();

  // reference to a single board that was selected
  private String _selectedBoardName = null;

  private int _index = -1;

 
  private boolean _showPadName = false;
  
 // Kee Chin Seong - XXL GUI Development 
  private boolean _showTopComponentLayer = true;
  private boolean _showTopComponentRefDesLayer = true;
  private boolean _showTopPadLayer = true;
  private boolean _showTopPanelLayer = true;
  
  private boolean _showBottomComponentLayer = false;
  private boolean _showBottomComponentRefDesLayer = false;
  private boolean _showBottomPadLayer = true;
  private boolean _showBottomPanelLayer = true;
  
  private int _virtualLiveAreaLayer;
  private com.axi.guiUtil.Renderer _rendererToUse;
  private com.axi.guiUtil.Renderer _draggedRenderer;

  //chin-seong.kee - Virtual Live Verification Image
  private int _topVirtualLiveVerificationImageLayer;  
  private int _bottomVirtualLiveVerificationImageLayer;
  private int _bothVirtualLiveVerificationImageLayer;

  protected ImagePaneRendererCreator _rendererCreator;
  
  private MagnificationEnum _magnification = MagnificationEnum.NOMINAL;
  private Project _project = null;
  
  /**
   * @author Kee Chin Seong
   */
  static
  {
    _guiObservable = GuiObservable.getInstance();
  }

  /**
   * @author Kee Chin Seong
   * 
   */
  CreatePanelGraphicsEngineSetup(CreateNewCadPanel createNewCadPanel, GraphicsEngine graphicsEngine)
  {
    Assert.expect(createNewCadPanel != null);
    Assert.expect(graphicsEngine != null);

    _createNewCadPanel = createNewCadPanel;
    _graphicsEngine = graphicsEngine;
    _graphicsEngine.setPixelBorderSize(_BORDER_SIZE_IN_PIXELS);
    _project = Project.getCurrentlyLoadedProject();
  }
  
  /**
   * @author Andy Mechtenberg
   */
  void addObservers()
  {
    // add this as an observer of the project
    ProjectObservable.getInstance().addObserver(this);
    // add this as an observer of the gui
    _guiObservable.addObserver(this);
    // add this as an observer of the engine
    _graphicsEngine.getSelectedRendererObservable().addObserver(this);
    // add this as an observer of the psp selected region
    _graphicsEngine.getSelectedOpticalRegionRendererObservable().addObserver(this);
    // add this as an observer of the psp drag region
    _graphicsEngine.getDragOpticalRegionObservable().addObserver(this);
    // add this as an observer of the psp selected region
    _graphicsEngine.getUnselectedOpticalRegionRendererObservable().addObserver(this);
    
    _graphicsEngine.getUnselectedDragOpticalRegionRendererObservable().addObserver(this);
    
    _graphicsEngine.getDisplayPadNameObservable().addObserver(this);
    _graphicsEngine.getSelectedAreaObservable().addObserver(this);
    VirtualLiveImageGenerationSettings.getInstance().addObserver(this);
    CadCreatorSelectedAreaSettings.getInstance().addObserver(this);
  }

  /**
    * @author Andy Mechtenberg
    */
   void removeObservers()
   {
     // remove this as an observer of the project data
     ProjectObservable.getInstance().deleteObserver(this);
     // remove this as an observer of the gui
     _guiObservable.deleteObserver(this);
     // remove this as an observer of the engine
     _graphicsEngine.getSelectedRendererObservable().deleteObserver(this);
     // remove this as a observer of the psp selected region
     _graphicsEngine.getSelectedOpticalRegionRendererObservable().deleteObserver(this);
     // remove this as a observer of the psp drag region
     _graphicsEngine.getDragOpticalRegionObservable().deleteObserver(this);
     // remove this as a observer of the psp selected region
     _graphicsEngine.getUnselectedOpticalRegionRendererObservable().deleteObserver(this);
     
     _graphicsEngine.getUnselectedDragOpticalRegionRendererObservable().deleteObserver(this);
     
     _graphicsEngine.getDisplayPadNameObservable().deleteObserver(this);    
     _graphicsEngine.getSelectedAreaObservable().deleteObserver(this);
     VirtualLiveImageGenerationSettings.getInstance().deleteObserver(this);
     CadCreatorSelectedAreaSettings.getInstance().addObserver(this);
  }
   
  /**
   * @author Andy Mechtenberg
   */
  void unpopulate()
  {
    _panel = null;
    clearAllInternalEngineData();
    _showTopPadLayer = true;
    _showTopComponentLayer = true;
    _showTopComponentRefDesLayer = true;
    _showTopPanelLayer = true;

    _showBottomPadLayer = false;
    _showBottomComponentLayer = false;
    _showBottomComponentRefDesLayer = false;
    _showBottomPanelLayer = false;
  }

  /**
   * @author chin-seong.kee
   */
  public void displayPanel(Panel panel)
  {
    Assert.expect(panel != null);

    _panel = panel;
    clearAllInternalEngineData();    

    createLayers();
    populateLayersWithRenderers();
    setColors();
    setVisibility();
    setThresholds();
    setLayersToUseWhenFittingToScreen();

    _graphicsEngine.setAxisInterpretation(true, false);

    MathUtilEnum displayUnits = _panel.getProject().getDisplayUnits();
    double measureFactor = 1.0/MathUtil.convertUnits(1.0, displayUnits, MathUtilEnum.NANOMETERS);
    _graphicsEngine.setMeasurementInfo(measureFactor, MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits),
       MeasurementUnits.getMeasurementUnitString(displayUnits));
  }
  
  /**
   * @author Andy Mechtenberg
   * @Edited by Kee Chin Seong
   */
  public void displayPanelForStaticImageGeneration(Panel panel, boolean flipped)
  {
    Assert.expect(panel != null);
    _panel = panel;
    clearAllInternalEngineData();    

    // here we'll only populate the TOP side, and skip things like pad/ref des, etc that don't make sense for
    // a JPG generation

    createTopLayers();
    populateTopLayersWithRenderers();
    // colors
    _graphicsEngine.setForeground(getTopComponentLayers(), LayerColorEnum.TOP_COMPONENT_COLOR.getColor());
    _graphicsEngine.setForeground(getTopBoardLayers(), LayerColorEnum.NEW_CAD_BOARD_COLOR.getColor());
    _graphicsEngine.setForeground(_topPanelLayer, LayerColorEnum.PANEL_COLOR.getColor());
    _graphicsEngine.setForeground(_topPanelFiducialLayer, LayerColorEnum.FIDUCIAL_COLOR.getColor());
    _graphicsEngine.setForeground(_boardToTopFiducialLayerMap.values(), LayerColorEnum.FIDUCIAL_COLOR.getColor());
    _graphicsEngine.setForeground(_boardToBoardNameLayerMap.values(), LayerColorEnum.BOARD_NAME_COLOR.getColor());

    List<Integer> boardNameLayers = getBoardNameLayers();
    Collection<com.axi.guiUtil.Renderer> boardNameRenderers = _graphicsEngine.getRenderers(boardNameLayers);
    for(com.axi.guiUtil.Renderer renderer : boardNameRenderers)
    {
      Assert.expect(renderer instanceof BoardNameRenderer);
      BoardNameRenderer boardNameRenderer = (BoardNameRenderer)renderer;
      boardNameRenderer.setTranslucent(false);
      boardNameRenderer.setFlipped(flipped);
      boardNameRenderer.setIsStaticImage(true);
    }

    // set visibility
    _graphicsEngine.setVisible(getTopLayers(), true);
    
    if(_showPadName)
      _graphicsEngine.setVisible(_topPadNameLayer, true);

    if (_panel.getNumBoards() == 1)
      _graphicsEngine.setVisible(getBoardNameLayers(), false);

    // set layer to use when fitting to screen - panel/board layers
    Collection<Integer> layers = new ArrayList<Integer>(2);
    layers.addAll(getPanelLayers());
    layers.addAll(getBoardLayers());
    _graphicsEngine.setLayersToUseWhenFittingToScreen(layers);

    _graphicsEngine.setAxisInterpretation(true, false);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void clearAllInternalEngineData()
  {
    _boardTypeToBoardRenderersMap.clear();
    _componentTypeToComponentRenderersMap.clear();
    _componentTypeToReferenceDesignatorRenderersMap.clear();
    _boardToTopBoardLayerMap.clear();
    _boardToTopPadLayerMap.clear();
    _boardToTopComponentLayerMap.clear();
    _boardToOriginLayerMap.clear();
    _boardToBoardNameLayerMap.clear();
    _boardToTopRefDesLayerMap.clear();
    _boardToTopFiducialLayerMap.clear();
    _boardToBottomBoardLayerMap.clear();
    _boardToBottomPadLayerMap.clear();
    _boardToBottomComponentLayerMap.clear();
    _boardToBottomRefDesLayerMap.clear();
    _boardToBottomFiducialLayerMap.clear();
    _componentRendererToPadRenderersMap.clear();
    _padToPadRendererMap.clear();
    _graphicsEngine.clearAllFlashingRenderersAndLayers();
  }
  
  /**
   * @author Andy Mechtenberg
   */
  void setControlToolBar(CadCreatorControlToolBar controlToolBar)
  {
    Assert.expect(controlToolBar != null);
    _controlToolBar = controlToolBar;
  }

  /**
   * Set up the layeredPanel
   * @author Bill Darbie
   */
  public GraphicsEngine getGraphicsEngine()
  {
    Assert.expect(_graphicsEngine != null);

    return _graphicsEngine;
  }

  /**
   * @author Bill Darbie
   */
  private void createLayers()
  {
    Assert.expect(_panel != null);

    // set things up like this:
    // - 1 panel layer
    // - 1 board layer per board
    // - 2 componenent layers per board (one for each side)
    // - 2 reference designator layers per board (one for each side)
    // - 2 pad layers per board (one for each side)
    // - 2 fiducial layers for panel fiducials (one for each side)
    // - 2 fiducial layers per board for board fiducials (one for each side)

    // the higher the layer number, the  deeper down the layer is
    // put largest components at the smallest layer number so mouse clicks
    // will be able to select small components before larger ones
    _layerNumber = JLayeredPane.DEFAULT_LAYER.intValue() - 1;
    _topVirtualLiveVerificationImageLayer = ++_layerNumber;
    _bottomVirtualLiveVerificationImageLayer = ++_layerNumber;
    _bothVirtualLiveVerificationImageLayer = ++_layerNumber;
    _topPanelLayer = ++_layerNumber;
    _bottomPanelLayer = ++_layerNumber;

    _topPanelFiducialLayer = ++_layerNumber;
    _bottomPanelFiducialLayer = ++_layerNumber;

    for (Board board: _panel.getBoards())
    {
      _boardToTopBoardLayerMap.put(board, new Integer(++_layerNumber));
      _boardToBottomBoardLayerMap.put(board, new Integer(++_layerNumber));

      _boardToTopPadLayerMap.put(board, new Integer(++_layerNumber));
      _boardToBottomPadLayerMap.put(board, new Integer(++_layerNumber));

      _boardToTopComponentLayerMap.put(board, new Integer(++_layerNumber));
      _boardToBottomComponentLayerMap.put(board, new Integer(++_layerNumber));

      _boardToTopFiducialLayerMap.put(board, new Integer(++_layerNumber));
      _boardToBottomFiducialLayerMap.put(board, new Integer(++_layerNumber));

      _boardToTopRefDesLayerMap.put(board, new Integer(++_layerNumber));
      _boardToBottomRefDesLayerMap.put(board, new Integer(++_layerNumber));

      _boardToOriginLayerMap.put(board, new Integer(++_layerNumber));
      _boardToBoardNameLayerMap.put(board, new Integer(++_layerNumber));
    }
    _topPanelFiducialRefDesLayer = ++_layerNumber;
    _bottomPanelFiducialRefDesLayer = ++_layerNumber;
  
    _topPadNameLayer = ++_layerNumber;
    _bottomPadNameLayer = ++_layerNumber;
    _virtualLiveAreaLayer = ++_layerNumber;
  }

  /**
   * @author Bill Darbie
   */
  private void createTopLayers()
  {
    Assert.expect(_panel != null);

    // set things up like this:
    // - 1 panel layer
    // - 1 board layer per board
    // - Top componenent layer per board
    // - Top fiducial layer for panel fiducials
    // - Top fiducial layer per board for board fiducials

    // the higher the layer number, the  deeper down the layer is
    // put largest components at the smallest layer number so mouse clicks
    // will be able to select small components before larger ones
    _layerNumber = JLayeredPane.DEFAULT_LAYER.intValue() - 1;
    _topPanelLayer = ++_layerNumber;

    _topPanelFiducialLayer = ++_layerNumber;

    for (Board board: _panel.getBoards())
    {
      _boardToTopBoardLayerMap.put(board, new Integer(++_layerNumber));

      _boardToTopComponentLayerMap.put(board, new Integer(++_layerNumber));

      _boardToTopFiducialLayerMap.put(board, new Integer(++_layerNumber));

      _boardToBoardNameLayerMap.put(board, new Integer(++_layerNumber));
    }
    _topPanelFiducialRefDesLayer = ++_layerNumber;
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void populatePanelLayersWithRenderers()
  {
    // now add all renderers to the layers
    PanelRenderer topPanelRenderer = new PanelRenderer(_panel);
    _graphicsEngine.addRenderer(_topPanelLayer, topPanelRenderer);
    PanelRenderer bottomPanelRenderer = new PanelRenderer(_panel);
    _graphicsEngine.addRenderer(_bottomPanelLayer, bottomPanelRenderer);

    // add panel fiducials and their reference designators for top and bottom panel sides
    Collection<Fiducial> panelFiducials = _panel.getFiducials();
    for (Fiducial fiducial : panelFiducials)
    {
      FiducialRenderer fiducialRenderer = new FiducialRenderer(fiducial);
      FiducialReferenceDesignatorRenderer fiducialReferenceDesignatorRenderer = new FiducialReferenceDesignatorRenderer(
          fiducial);
      if (fiducial.isTopSide())
      {
        _graphicsEngine.addRenderer(_topPanelFiducialLayer, fiducialRenderer);
        _graphicsEngine.addRenderer(_topPanelFiducialRefDesLayer, fiducialReferenceDesignatorRenderer);
      }
      else
      {
        _graphicsEngine.addRenderer(_bottomPanelFiducialLayer, fiducialRenderer);
        _graphicsEngine.addRenderer(_bottomPanelFiducialRefDesLayer, fiducialReferenceDesignatorRenderer);
      }
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void populateReferenceDesignatorLayerWithRenderers(Board board, Collection<Component> components, boolean isTopside)
  {
    for (Component component : components)
    {
      if(component.isLoaded() == true)
      {
         ReferenceDesignatorRenderer referenceDesignatorRenderer = new ReferenceDesignatorRenderer(component);

         Integer layerInt = (isTopside == true) ? (Integer)_boardToTopRefDesLayerMap.get(board) :
                                                  (Integer)_boardToBottomRefDesLayerMap.get(board);
         
         _graphicsEngine.addRenderer(layerInt.intValue(), referenceDesignatorRenderer);
         
         List<ReferenceDesignatorRenderer> compRefDesRenderers = null;
         if (_componentTypeToReferenceDesignatorRenderersMap.containsKey(component.getComponentType()))
           compRefDesRenderers = _componentTypeToReferenceDesignatorRenderersMap.get(component.getComponentType());
         else
         {
           compRefDesRenderers = new ArrayList<ReferenceDesignatorRenderer>();
           Object previous = _componentTypeToReferenceDesignatorRenderersMap.put(component.getComponentType(), compRefDesRenderers);
           Assert.expect(previous == null);
         }
         compRefDesRenderers.add(referenceDesignatorRenderer);
      }
    }
  }
  
   /*
   * @author Kee Chin Seong
   */
  private void unpopulateComponentLayerWithRenderers(Collection<Component> components)
  {
    for(Component component  : components)
    {
      _componentTypeToComponentRenderersMap.remove(component.getComponentType());
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void populateComponentLayersWithRenderers(Board board, Collection<Component> components, boolean isTopSide, boolean isNeedRepopulate)
  {
     for (Component component : components)
     {
        if(component.isLoaded() == true)
        {
          ComponentRenderer componentRenderer = null;
          
          if(!isNeedRepopulate)
             componentRenderer = new ComponentRenderer(this, component);
          
          ComponentType componentType = component.getComponentType();
          List<ComponentRenderer> componentRenderers = null;
          //ComponentRenderer componentRenderer = null;
          List<ReferenceDesignatorRenderer> compRefDesRenderers = null;
          if (_componentTypeToComponentRenderersMap.containsKey(componentType))
          {
            componentRenderers = _componentTypeToComponentRenderersMap.get(componentType);
          }
          else
          {
            componentRenderers = new ArrayList<ComponentRenderer>();
            Object previous = _componentTypeToComponentRenderersMap.put(componentType, componentRenderers);
            Assert.expect(previous == null);
          }
          
          if(isNeedRepopulate)
          {
            if(componentRenderers.size() == 0 )
            {
              componentRenderer = new ComponentRenderer(this, component);
              componentRenderers.add(componentRenderer);
            }
          }
          else
            componentRenderers.add(componentRenderer);
          
          Integer layerInt = (isTopSide == true) ? (Integer)_boardToTopComponentLayerMap.get(board) :
                                                   (Integer)_boardToBottomComponentLayerMap.get(board);
          
          //for(ComponentRenderer compRenderer : componentRenderers)
            _graphicsEngine.addRenderer(layerInt.intValue(), componentRenderer); 
           
        }
      
        // pads are only displayed once the user zooms in
//          Collection pads = component.getPads();
//          numPads += pads.size();
//          Iterator pit = pads.iterator();
//          while (pit.hasNext())
//          {
//            Pad pad = (Pad)pit.next();
//            PadRenderer padRenderer = new PadRenderer(pad, componentRenderer);
//            layerInt = (Integer)_boardToTopPadLayerMap.get(board);
//            _graphicsEngine.addRenderer(layerInt.intValue(), padRenderer);
//
//            if (pad.isThroughHolePad())
//            {
//              // through hole pads show up on both sides of the panel
//              padRenderer = new PadRenderer(pad, componentRenderer);
//              layerInt = (Integer)_boardToBottomPadLayerMap.get(board);
//              _graphicsEngine.addRenderer(layerInt.intValue(), padRenderer);
//            }
//          }
     }
  }
  
  /*
   * @author Kee Chin seong
   */
  private void populateFiducialWithLayers(Board board, Collection<Fiducial> boardFiducials, boolean isTopSide)
  {
     for (Fiducial fiducial : boardFiducials)
     {
        FiducialRenderer fiducialRenderer = new FiducialRenderer(fiducial);
        
        Integer layerInt = (isTopSide == true) ? (Integer)_boardToTopFiducialLayerMap.get(board) :
                                                 (Integer)_boardToBottomFiducialLayerMap.get(board);
        
        _graphicsEngine.addRenderer(layerInt.intValue(), fiducialRenderer);
        FiducialReferenceDesignatorRenderer fiducialReferenceDesignatorRenderer = new FiducialReferenceDesignatorRenderer(fiducial);
        
        layerInt = (isTopSide == true) ? (Integer)_boardToTopRefDesLayerMap.get(board) :
                                         (Integer)_boardToBottomRefDesLayerMap.get(board);
        
        _graphicsEngine.addRenderer(layerInt.intValue(), fiducialReferenceDesignatorRenderer);
     }
  }
  
  /**
   * @author Bill Darbie
   * @Edited By Kee Chin Seong
   */
  private void populateLayersWithRenderers()
  {
    Assert.expect(_panel != null);
    
    populatePanelLayersWithRenderers();

    Collection<Board> boards = _panel.getBoards();
    for (Board board : boards)
    {
      populateBoardLayers(board);
    }
  }
  
  /**
   * Just creates enough renderers to show the top side panel, board and components.  Used typically for
   * JPG generation, where we don't need all renderers because we'll just create the JPG then throw them all away.
   * @author Andy Mechtenberg
   * @Edited By Kee Chin Seong
   */
  private void populateTopLayersWithRenderers()
  {
//    long startTime = System.currentTimeMillis();

    Assert.expect(_panel != null);
    // now add all renderers to the layers
    PanelRenderer topPanelRenderer = new PanelRenderer(_panel);
    _graphicsEngine.addRenderer(_topPanelLayer, topPanelRenderer);

    // add panel fiducials and their reference designators for top and bottom panel sides
    Collection<Fiducial> panelFiducials = _panel.getFiducials();
    for (Fiducial fiducial : panelFiducials)
    {
      FiducialRenderer fiducialRenderer = new FiducialRenderer(fiducial);
      if (fiducial.isTopSide())
      {
        _graphicsEngine.addRenderer(_topPanelFiducialLayer, fiducialRenderer);
      }
    }

    Collection<Board> boards = _panel.getBoards();
    for (Board board : boards)
    {
      BoardType boardType = board.getBoardType();
      List<BoardRenderer> boardRenderers = null;

      if (_boardTypeToBoardRenderersMap.containsKey(boardType))
      {
        boardRenderers = _boardTypeToBoardRenderersMap.get(boardType);
      }
      else
      {
        boardRenderers = new ArrayList<BoardRenderer>();
        Object previous = _boardTypeToBoardRenderersMap.put(boardType, boardRenderers);
        Assert.expect(previous == null);
      }
      BoardRenderer topBoardRenderer = new BoardRenderer(board);
      boardRenderers.add(topBoardRenderer);
      Integer layerInt = _boardToTopBoardLayerMap.get(board);
      _graphicsEngine.addRenderer(layerInt.intValue(), topBoardRenderer);
      //add the board name renderer

//      if (_panel.getNumBoards() > 1)
//      {
        layerInt = (Integer)_boardToBoardNameLayerMap.get(board);
        BoardNameRenderer boardNameRenderer = new BoardNameRenderer(board);
        _graphicsEngine.addRenderer(layerInt, boardNameRenderer);
//      }

      if (board.topSideBoardExists())
      {
        SideBoard topSideBoard = board.getTopSideBoard();

        // Populate top board side components
        Collection<Component> components = topSideBoard.getComponents();
        for (Component component : components)
        {
          ComponentRenderer componentRenderer = new ComponentRenderer(this, component);
          ComponentType componentType = component.getComponentType();
          List<ComponentRenderer> componentRenderers = null;
          if (_componentTypeToComponentRenderersMap.containsKey(componentType))
          {
            componentRenderers = _componentTypeToComponentRenderersMap.get(componentType);
          }
          else
          {
            componentRenderers = new ArrayList<ComponentRenderer>();
            Object previous = _componentTypeToComponentRenderersMap.put(componentType, componentRenderers);
            Assert.expect(previous == null);
          }
          componentRenderers.add(componentRenderer);
          layerInt = (Integer)_boardToTopComponentLayerMap.get(board);
          _graphicsEngine.addRenderer(layerInt.intValue(), componentRenderer);
        }
        // Populate top board side fiducials
        Collection<Fiducial> boardFiducials = topSideBoard.getFiducials();
        for (Fiducial fiducial : boardFiducials)
        {
          FiducialRenderer fiducialRenderer = new FiducialRenderer(fiducial);
          layerInt = (Integer)_boardToTopFiducialLayerMap.get(board);
          _graphicsEngine.addRenderer(layerInt.intValue(), fiducialRenderer);
        }
      }
    }

//    long stopTime = System.currentTimeMillis();
//    if (UnitTest.unitTesting() == false)
//      System.out.println("Generate Renderers FOR JPG in millis: " + (stopTime - startTime));
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateBoardLayers(Board board)
  {
    BoardType boardType = board.getBoardType();
    List<BoardRenderer> boardRenderers = null;

    if (_boardTypeToBoardRenderersMap.containsKey(boardType))
    {
      boardRenderers = _boardTypeToBoardRenderersMap.get(boardType);
    }
    else
    {
      boardRenderers = new ArrayList<BoardRenderer>();
      Object previous = _boardTypeToBoardRenderersMap.put(boardType, boardRenderers);
      Assert.expect(previous == null);
    }
    BoardRenderer topBoardRenderer = new BoardRenderer(board);
    boardRenderers.add(topBoardRenderer);
    Integer layerInt = _boardToTopBoardLayerMap.get(board);
    _graphicsEngine.addRenderer(layerInt.intValue(), topBoardRenderer);
    BoardRenderer bottomBoardRenderer = new BoardRenderer(board);
    boardRenderers.add(bottomBoardRenderer);
    layerInt = _boardToBottomBoardLayerMap.get(board);
    _graphicsEngine.addRenderer(layerInt.intValue(), bottomBoardRenderer);

    if (board.topSideBoardExists())
    {
      SideBoard topSideBoard = board.getTopSideBoard();

      // add the board origin and axis renderers
      layerInt = (Integer)_boardToOriginLayerMap.get(board);
      BoardCadOriginRenderer boardOriginRenderer = new BoardCadOriginRenderer(board);
      _graphicsEngine.addRenderer(layerInt, boardOriginRenderer);
      BoardCadOriginAxisLetterRenderer xAxisRenderer = new BoardCadOriginAxisLetterRenderer(board, true);
      _graphicsEngine.addRenderer(layerInt, xAxisRenderer);
      BoardCadOriginAxisLetterRenderer yAxisRenderer = new BoardCadOriginAxisLetterRenderer(board, false);
      _graphicsEngine.addRenderer(layerInt, yAxisRenderer);

      //add the board name renderer
      layerInt = (Integer)_boardToBoardNameLayerMap.get(board);
      BoardNameRenderer boardNameRenderer = new BoardNameRenderer(board);
      _graphicsEngine.addRenderer(layerInt, boardNameRenderer);

      // Populate top board side components
      Collection<Component> components = topSideBoard.getComponents();
      populateComponentLayersWithRenderers(board, components, true, false);
      populateReferenceDesignatorLayerWithRenderers(board, components, true);
      
      // Populate top board side fiducials
      populateFiducialWithLayers(board, topSideBoard.getFiducials(), true);
    }

    // moving on to the bottom board
    if (board.bottomSideBoardExists())
    {
      SideBoard bottomSideBoard = board.getBottomSideBoard();

      // add the board origin and axis renderers
      layerInt = (Integer)_boardToOriginLayerMap.get(board);
      BoardCadOriginRenderer boardOriginRenderer = new BoardCadOriginRenderer(board);
      _graphicsEngine.addRenderer(layerInt, boardOriginRenderer);
      BoardCadOriginAxisLetterRenderer xAxisRenderer = new BoardCadOriginAxisLetterRenderer(board, true);
      _graphicsEngine.addRenderer(layerInt, xAxisRenderer);
      BoardCadOriginAxisLetterRenderer yAxisRenderer = new BoardCadOriginAxisLetterRenderer(board, false);
      _graphicsEngine.addRenderer(layerInt, yAxisRenderer);

      //add the board name renderer only if the top side didn't exist
      if ((board.topSideBoardExists() == false))// && (board.getPanel().getNumBoards() > 1))
      {
        layerInt = (Integer)_boardToBoardNameLayerMap.get(board);
        BoardNameRenderer boardNameRenderer = new BoardNameRenderer(board);
        _graphicsEngine.addRenderer(layerInt, boardNameRenderer);
      }
      Collection<Component> components = bottomSideBoard.getComponents();
      populateComponentLayersWithRenderers(board, components, false, false);
      populateReferenceDesignatorLayerWithRenderers(board, components, false);
      
      // Populate bottom board side fiducials
      populateFiducialWithLayers(board, bottomSideBoard.getFiducials(), false);
    }
  }

  /**
   * @return a List of Integers of all the top side layers
   * @author Bill Darbie
   */
  List<Integer> getTopLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(_topPanelLayer);
    layers.add(_topPanelFiducialLayer);
    layers.add(_topPanelFiducialRefDesLayer);
    layers.addAll(_boardToTopBoardLayerMap.values());
    layers.addAll(_boardToTopPadLayerMap.values());
    layers.addAll(_boardToTopComponentLayerMap.values());
    layers.addAll(_boardToTopRefDesLayerMap.values());
    layers.addAll(_boardToTopFiducialLayerMap.values());
    layers.addAll(_boardToBoardNameLayerMap.values());

    return layers;
  }

  /*
   * @author Kee Chin Seong
   */
  public void setTopGraphicLayersAccordingToCustomizeSettings()
  {
    _graphicsEngine.setVisible(_topPanelLayer, _showTopPanelLayer);
    _graphicsEngine.setVisible(_topPanelFiducialLayer, true);
    _graphicsEngine.setVisible(_topPanelFiducialRefDesLayer, true);

    _graphicsEngine.setVisible(getTopBoardLayers(), true);
    _graphicsEngine.setVisible(getTopPadLayers(), _showTopPadLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopComponentLayers(), _showTopComponentLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopReferenceDesignatorLayers(), _showTopComponentRefDesLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopBoardFiducialLayers(), true);
    _graphicsEngine.setVisible(getBoardNameLayers(), false);
    
    _graphicsEngine.setVisible(getBottomLayers(), false);
    
    _graphicsEngine.setCrossHairsSelectionVisibility(false);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setBottomGraphicLayersAccordingToCustomizeSettings()
  {
    _graphicsEngine.setVisible(_bottomPanelLayer, _showBottomPanelLayer);
    _graphicsEngine.setVisible(_bottomPanelFiducialLayer, true);
    _graphicsEngine.setVisible(_bottomPanelFiducialRefDesLayer, true);

    _graphicsEngine.setVisible(getBottomBoardLayers(), true);
    _graphicsEngine.setVisible(getBottomPadLayers(), _showBottomPadLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomComponentLayers(), _showBottomComponentLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomReferenceDesignatorLayers(), _showBottomComponentRefDesLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomBoardFiducialLayers(), true);
    _graphicsEngine.setVisible(getBoardNameLayers(), false);
    
    _graphicsEngine.setVisible(getTopLayers(), false);
    
    _graphicsEngine.setCrossHairsSelectionVisibility(false);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setBothSidesGraphicLayersAccordingToCustomizeSettings()
  {
    //set top side
    _graphicsEngine.setVisible(_topPanelLayer, _showTopPanelLayer);
    _graphicsEngine.setVisible(_topPanelFiducialLayer, true);
    _graphicsEngine.setVisible(_topPanelFiducialRefDesLayer, true);

    _graphicsEngine.setVisible(getTopBoardLayers(), true);
    _graphicsEngine.setVisible(getTopPadLayers(), _showTopPadLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopComponentLayers(), _showTopComponentLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopReferenceDesignatorLayers(), _showTopComponentRefDesLayer); /// according UI Settings
    _graphicsEngine.setVisible(getTopBoardFiducialLayers(), true);
    _graphicsEngine.setVisible(getBoardNameLayers(), false);
    
    //Ok, now set bottom side
    _graphicsEngine.setVisible(_bottomPanelLayer, _showBottomPanelLayer);
    _graphicsEngine.setVisible(_bottomPanelFiducialLayer, true);
    _graphicsEngine.setVisible(_bottomPanelFiducialRefDesLayer, true);

    _graphicsEngine.setVisible(getBottomBoardLayers(), true);
    _graphicsEngine.setVisible(getBottomPadLayers(), _showBottomPadLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomComponentLayers(), _showBottomComponentLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomReferenceDesignatorLayers(), _showBottomComponentRefDesLayer); /// according UI Settings
    _graphicsEngine.setVisible(getBottomBoardFiducialLayers(), true);
    _graphicsEngine.setVisible(getBoardNameLayers(), false);
    
    _graphicsEngine.setCrossHairsSelectionVisibility(false);
  }
  
  /**
   * @return a List of Integers of all the bottom side layers
   * @author Bill Darbie
   */
  List<Integer> getBottomLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();

    layers.add(_bottomPanelLayer);
    layers.add(_bottomPanelFiducialLayer);
    layers.add(_bottomPanelFiducialRefDesLayer);
    layers.addAll(_boardToBottomBoardLayerMap.values());
    layers.addAll(_boardToBottomPadLayerMap.values());
    layers.addAll(_boardToBottomComponentLayerMap.values());
    layers.addAll(_boardToBottomRefDesLayerMap.values());
    layers.addAll(_boardToBottomFiducialLayerMap.values());
    
    return layers;
  }

  /**
   * @author Andy Mechtenberg
   */
  List<Integer> getBoardOriginLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(_boardToOriginLayerMap.values());
    return layers;
  }

  /**
   * @author Andy Mechtenberg
   */
  List<Integer> getBoardOriginLayers(Board board)
  {
    List<Integer> layers = new ArrayList<Integer>();
    Integer integer = (Integer)_boardToOriginLayerMap.get(board);
    layers.add(integer);
    return layers;
  }

  /**
   * @author Andy Mechtenberg
   */
  List<Integer> getBoardNameLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(_boardToBoardNameLayerMap.values());
    return layers;
  }

  /**
   * @author Andy Mechtenberg
   */
  List<Integer> getBoardNameLayers(Board board)
  {
    List<Integer> layers = new ArrayList<Integer>();
    Integer integer = (Integer)_boardToBoardNameLayerMap.get(board);
    layers.add(integer);
    return layers;
  }

  /**
   * @return a List of Integers of all the layers on the entire panel
   * for both sides
   * @author Bill Darbie
   */
  public List<Integer> getAllLayers()
  {
    List<Integer> layers = getTopLayers();
    layers.addAll(getBottomLayers());
    layers.addAll(getBoardOriginLayers());

    return layers;
  }

  /**
   * @return  List of Integers of all the layers used by all the Boards on this Panel.
   * @author Bill Darbie
   */
  List<Integer> getAllLayers(Board board)
  {
    Assert.expect(board != null);

    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(getBoardLayers(board));
    layers.addAll(getBoardFiducialLayers(board));
    layers.addAll(getComponentLayers(board));
    layers.addAll(getReferenceDesignatorLayers(board));
    layers.addAll(getPadLayers(board));
    layers.addAll(getBoardOriginLayers(board));
    layers.addAll(getBoardNameLayers(board));

    return layers;
  }

  /**
   * @return the top Panel layer
   * @author Bill Darbie
   */
  int getTopPanelLayer()
  {
    return _topPanelLayer;
  }

  /**
   * @return the top Panel layer
   * @author Bill Darbie
   */
  int getBottomPanelLayer()
  {
    return _bottomPanelLayer;
  }

  /**
   * @return the List of Integers of all Panel layers.
   * @author Bill Darbie
   */
  List<Integer> getPanelLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(2);

    layers.add(new Integer(_topPanelLayer));
    layers.add(new Integer(_bottomPanelLayer));

    return layers;
  }

  /**
   * @return the top layer used by the Board passed in
   * @author Bill Darbie
   */
  int getTopBoardLayer(Board board)
  {
    Integer integer = (Integer)_boardToTopBoardLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return has bottom layer used by the Board passed in
   * @author Lim Boon Hong
   */
  private boolean hasBottomBoardLayer(Board board)
  {
    return _boardToBottomBoardLayerMap.containsKey(board);
  }
  
  /**
   * @return the bottom layer used by the Board passed in
   * @author Bill Darbie
   */
  private int getBottomBoardLayer(Board board)
  {
    Integer integer = (Integer)_boardToBottomBoardLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return a List of Integers of all the top Board layers.
   * @author Bill Darbie
   */
  List<Integer> getTopBoardLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToTopBoardLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all the bottom Board layers.
   * @author Bill Darbie
   */
  List<Integer> getBottomBoardLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToBottomBoardLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all Board layers
   * @author Bill Darbie
   */
  List<Integer> getBoardLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(getTopBoardLayers());
    layers.addAll(getBottomBoardLayers());

    return layers;
  }

  /**
   * @return the List of Integers for layers that the passed in board uses.
   * @return Bill Darbie
   * @check hasBottomBoardLayer by Lim Boon Hong
   */
  List<Integer> getBoardLayers(Board board)
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.add(new Integer(getTopBoardLayer(board)));
    if(hasBottomBoardLayer(board))
      layers.add(new Integer(getBottomBoardLayer(board)));

    return layers;
  }

  /**
   * @return the top Component layer used by the board passed in
   * @author Bill Darbie
   */
  int getTopComponentLayer(Board board)
  {
    Integer integer = (Integer)_boardToTopComponentLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return has bottom component layer used by the Board passed in
   * @author Lim Boon Hong
   */
  private boolean hasBottomBoardComponentLayer(Board board)
  {
    return _boardToBottomComponentLayerMap.containsKey(board);
  }
  
  /**
   * @return the bottom Component layer used by the board passed in
   * @author Bill Darbie
   */
  int getBottomComponentLayer(Board board)
  {
    Integer integer = (Integer)_boardToBottomComponentLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return a List of Integers of all the top Component layers.
   * @author Bill Darbie
   */
  List<Integer> getTopComponentLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToTopComponentLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all the bottom Component layers.
   * @author Bill Darbie
   */
  List<Integer> getBottomComponentLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToBottomComponentLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all Component layers
   * @author Bill Darbie
   */
  List<Integer> getComponentLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(getTopComponentLayers());
    layers.addAll(getBottomComponentLayers());

    return layers;
  }

  /**
   * @return the List of Integers for Component layers that the passed in board uses.
   * @return Bill Darbie
   * @check hasBottomBoardComponentLayer by Lim Boon Hong 
   */
  List<Integer> getComponentLayers(Board board)
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.add(new Integer(getTopComponentLayer(board)));
    if(hasBottomBoardComponentLayer(board))
      layers.add(new Integer(getBottomComponentLayer(board)));

    return layers;
  }

  /**
   * @return has top Pad layer used by the Board passed in
   * @author BH
   */
  private boolean hasTopBoardPadLayer(Board board)
  {
    return _boardToTopPadLayerMap.containsKey(board);
  }
  
  /**
   * @return the top Pad layer used by the board passed in
   * @author Bill Darbie
   */
  int getTopPadLayer(Board board)
  {
    Integer integer = (Integer)_boardToTopPadLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return has bottom Pad layer used by the Board passed in
   * @author Lim Boon Hong
   */
  private boolean hasBottomBoardPadLayer(Board board)
  {
    return _boardToBottomPadLayerMap.containsKey(board);
  }
  
  /**
   * @return the bottom Pad layer used by the board passed in
   * @author Bill Darbie
   */
  int getBottomPadLayer(Board board)
  {
    Integer integer = (Integer)_boardToBottomPadLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return a List of Integers of all the top Pad layers.
   * @author Bill Darbie
   */
  List<Integer> getTopPadLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToTopPadLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all the bottom Pad layers.
   * @author Bill Darbie
   */
  List<Integer> getBottomPadLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToBottomPadLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all Pad layers
   * @author Bill Darbie
   */
  List<Integer> getPadLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(getTopPadLayers());
    layers.addAll(getBottomPadLayers());

    return layers;
  }

  /**
   * @return the List of Integers for all Pad layers that the passed in board uses.
   * @return Bill Darbie
   * @check hasTopBoardPadLayer and hasBottomBoardPadLayer by Lim Boon Hong
   */
  List<Integer> getPadLayers(Board board)
  {
    List<Integer> layers = new ArrayList<Integer>();
    if (hasTopBoardPadLayer(board))
      layers.add(new Integer(getTopPadLayer(board)));
    if (hasBottomBoardPadLayer(board))
      layers.add(new Integer(getBottomPadLayer(board)));

    return layers;
  }

  /**
   * @return has top Ref Des layer used by the Board passed in
   * @author Lim Boon Hong
   */
  private boolean hasTopBoardRefDesLayer(Board board)
  {
    return _boardToTopRefDesLayerMap.containsKey(board);
  }
  
  /**
   * @return the top layer used by the Reference Designators for the board passed in
   * @author Bill Darbie
   */
  int getTopReferenceDesignatorLayer(Board board)
  {
    Integer integer = (Integer)_boardToTopRefDesLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }
  
  /**
   * @return has bottom Ref Des layer used by the Board passed in
   * @author Lim Boon Hong
   */
  private boolean hasBottomBoardRefDesLayer(Board board)
  {
    return _boardToBottomRefDesLayerMap.containsKey(board);
  }

  /**
   * @return the bottom layer used by the Reference Designators for the board passed in
   * @author Bill Darbie
   */
  int getBottomReferenceDesignatorLayer(Board board)
  {
    Integer integer = (Integer)_boardToBottomRefDesLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return a List of Integers of all the top Reference Designator layers.
   * @author Bill Darbie
   */
  List<Integer> getTopReferenceDesignatorLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToTopRefDesLayerMap.values());
    layers.add(new Integer(_topPanelFiducialRefDesLayer));
    return layers;
  }

  /**
   * @return a List of Integers of all the bottom Reference Designator layers.
   * @author Bill Darbie
   */
  List<Integer> getBottomReferenceDesignatorLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToBottomRefDesLayerMap.values());
    layers.add(new Integer(_bottomPanelFiducialRefDesLayer));
    return layers;
  }

  /**
   * @return a List of Integers of all Reference Designator layers
   * @author Bill Darbie
   */
  List<Integer> getReferenceDesignatorLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(getTopReferenceDesignatorLayers());
    layers.addAll(getBottomReferenceDesignatorLayers());

    return layers;
  }

  /**
   * @return the List of Integers for Reference Designator layers that the passed in board uses.
   * @return Bill Darbie
   * @check hasTopBoardRefDesLayer and hasBottomBoardRefDesLayer by Lim Boon Hong  
   */
  List<Integer> getReferenceDesignatorLayers(Board board)
  {
    List<Integer> layers = new ArrayList<Integer>();
    if(hasTopBoardRefDesLayer(board))
      layers.add(new Integer(getTopReferenceDesignatorLayer(board)));
    if(hasBottomBoardRefDesLayer(board))
      layers.add(new Integer(getBottomReferenceDesignatorLayer(board)));

    return layers;
  }

  /**
   * @return the top Fiducial layer used by the board passed in
   * @author Bill Darbie
   */
  int getTopBoardFiducialLayer(Board board)
  {
    Integer integer = (Integer)_boardToTopFiducialLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

   /**
   * @return has bottom fiducial layer used by the Board passed in
   * @author Lim Boon Hong
   */
  private boolean hasBottomBoardFiducialLayer(Board board)
  {
    return _boardToBottomFiducialLayerMap.containsKey(board);
  }
  
  /**
   * @return the bottom Fiducial layer used by the board passed in
   * @author Bill Darbie
   */
  int getBottomBoardFiducialLayer(Board board)
  {
    Integer integer = (Integer)_boardToBottomFiducialLayerMap.get(board);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @return a List of Integers of all the top Fiducial layers.
   * @author Bill Darbie
   */
  List<Integer> getTopBoardFiducialLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToTopFiducialLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all the bottom Fiducial layers.
   * @author Bill Darbie
   */
  List<Integer> getBottomBoardFiducialLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(_boardToBottomFiducialLayerMap.values());
    return layers;
  }

  /**
   * @return a List of Integers of all Fiducial layers
   * @author Bill Darbie
   */
  List<Integer> getBoardFiducialLayers()
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(getTopBoardFiducialLayers());
    layers.addAll(getBottomBoardFiducialLayers());

    return layers;
  }

  /**
   * @return the List of Integers for all Fiducial layers that the passed in board uses.
   * @return Bill Darbie
   * @check hasBottomBoardFiducialLayer by Lim Boon Hong
   */
  List<Integer> getBoardFiducialLayers(Board board)
  {
    List<Integer> layers = new ArrayList<Integer>();
    layers.add(new Integer(getTopBoardFiducialLayer(board)));
    if(hasBottomBoardFiducialLayer(board))
    layers.add(new Integer(getBottomBoardFiducialLayer(board)));

    return layers;
  }

  /**
   * @return a List of Integers of Layers for Panel fiducials
   * @author Bill Darbie
   */
  List<Integer> getPanelFiducialLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(2);
    layers.add(new Integer(_topPanelFiducialLayer));
    layers.add(new Integer(_bottomPanelFiducialLayer));

    return layers;
  }

  /**
   * @return a List of Integers of Layers for Panel fiducials reference designators
   * @author Laura Cormos
   */
  List<Integer> getPanelFiducialRefDesLayers()
  {
    List<Integer> layers = new ArrayList<Integer>(2);
    layers.add(new Integer(_topPanelFiducialRefDesLayer));
    layers.add(new Integer(_bottomPanelFiducialRefDesLayer));

    return layers;
  }

  /**
   * @return a List of Integers of both Panel and Board fiducial layers.
   * @author Bill Darbie
   */
  List<Integer> getFiducialLayers()
  {
    List<Integer> layers = getBoardFiducialLayers();
    layers.addAll(getPanelFiducialLayers());

    return layers;
  }

  /**
   * @author Andy Mechtenberg
   * @Edited By Kee Chin Seong
   */
  public void showTopSide()
  {
    // board name and origin renderers may or may not be visible.  Preserve that state here
    Collection<Integer> boardNameLayers = _boardToBoardNameLayerMap.values();
    boolean boardNamesVisible = false;
    for(Integer boardNameLayer : boardNameLayers)
    {
      if (_graphicsEngine.isLayerVisible(boardNameLayer))
        boardNamesVisible = true;
    }

    Collection<Integer> boardOriginLayers = _boardToOriginLayerMap.values();
    boolean boardOriginsVisible = false;
    for(Integer boardOriginLayer : boardOriginLayers)
    {
      if (_graphicsEngine.isLayerVisible(boardOriginLayer))
        boardOriginsVisible = true;
    }
    _graphicsEngine.setVisible(getTopLayers(), true);
    _graphicsEngine.setVisible(getBottomLayers(), false);

    if(_showPadName)
      _graphicsEngine.setVisible(_topPadNameLayer, true);
    
    _graphicsEngine.setVisible(_bottomPadNameLayer, false);
    
    _graphicsEngine.setVisible(boardNameLayers, boardNamesVisible);
    _graphicsEngine.setVisible(boardOriginLayers, boardOriginsVisible);
    
    _showTopPadLayer = true;
    _showTopComponentLayer = true;
    _showTopComponentRefDesLayer = true;
    _showTopPanelLayer = true;
    
    _showBottomPadLayer = false;
    _showBottomComponentLayer = false;
    _showBottomComponentRefDesLayer = false;
    _showBottomPanelLayer = false;
    
    _graphicsEngine.setVisible(_topVirtualLiveVerificationImageLayer, true);
    _graphicsEngine.setVisible(_bottomVirtualLiveVerificationImageLayer, false);
    _graphicsEngine.setVisible(_bothVirtualLiveVerificationImageLayer, false);
  }

  /**
   * @author Andy Mechtenberg
   * @Edited By Kee Chin Seong
   */
  public void showBottomSide()
  {
    // board name and origin renderers may or may not be visible.  Preserve that state here
    Collection<Integer> boardNameLayers = _boardToBoardNameLayerMap.values();
    boolean boardNamesVisible = false;
    for(Integer boardNameLayer : boardNameLayers)
    {
      if (_graphicsEngine.isLayerVisible(boardNameLayer))
        boardNamesVisible = true;
    }

    Collection<Integer> boardOriginLayers = _boardToOriginLayerMap.values();
    boolean boardOriginsVisible = false;
    for(Integer boardOriginLayer : boardOriginLayers)
    {
      if (_graphicsEngine.isLayerVisible(boardOriginLayer))
        boardOriginsVisible = true;
    }
    _graphicsEngine.setVisible(getTopLayers(), false);
    _graphicsEngine.setVisible(getBottomLayers(), true);
    
    if(_showPadName)
      _graphicsEngine.setVisible(_bottomPadNameLayer, true);

    _graphicsEngine.setVisible(_topPadNameLayer, false);
    
    _graphicsEngine.setVisible(boardNameLayers, boardNamesVisible);
    _graphicsEngine.setVisible(boardOriginLayers, boardOriginsVisible);
    
    _showBottomPadLayer = true;
    _showBottomComponentLayer = true;
    _showBottomComponentRefDesLayer = true;
    _showBottomPanelLayer = true;

    _showTopPadLayer = false;
    _showTopComponentLayer = false;
    _showTopComponentRefDesLayer = false;
    _showTopPanelLayer = false;
    
    _graphicsEngine.setVisible(_topVirtualLiveVerificationImageLayer, false);
    _graphicsEngine.setVisible(_bottomVirtualLiveVerificationImageLayer, true);
    _graphicsEngine.setVisible(_bothVirtualLiveVerificationImageLayer, false);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void showBothSides()
  {
    // board name and origin renderers may or may not be visible.  Preserve that state here
    Collection<Integer> boardNameLayers = _boardToBoardNameLayerMap.values();
    boolean boardNamesVisible = false;
    for(Integer boardNameLayer : boardNameLayers)
    {
      if (_graphicsEngine.isLayerVisible(boardNameLayer))
        boardNamesVisible = true;
    }

    Collection<Integer> boardOriginLayers = _boardToOriginLayerMap.values();
    boolean boardOriginsVisible = false;
    for(Integer boardOriginLayer : boardOriginLayers)
    {
      if (_graphicsEngine.isLayerVisible(boardOriginLayer))
        boardOriginsVisible = true;
    }
    _graphicsEngine.setVisible(getTopLayers(), true);
    _graphicsEngine.setVisible(getBottomLayers(), true);
    
    if(_showPadName)
    {
      _graphicsEngine.setVisible(_topPadNameLayer, true);
      _graphicsEngine.setVisible(_bottomPadNameLayer, true);
    }

    _graphicsEngine.setVisible(boardNameLayers, boardNamesVisible);
    _graphicsEngine.setVisible(boardOriginLayers, boardOriginsVisible);
    
    _graphicsEngine.setVisible(_topVirtualLiveVerificationImageLayer, false);
    _graphicsEngine.setVisible(_bottomVirtualLiveVerificationImageLayer, false);
    _graphicsEngine.setVisible(_bothVirtualLiveVerificationImageLayer, true);
    
    _showTopComponentLayer = true;
    _showTopPadLayer = true;
    _showTopComponentRefDesLayer = true;
    _showTopPanelLayer = true;

    _showBottomComponentLayer = true;
    _showBottomPadLayer = true;
    _showBottomComponentRefDesLayer = true;
    _showBottomPanelLayer = true;
  }


  /**
   * Return true if any top layer is visisble, else false
   * @author Andy Mechtenberg
   */
  public boolean isTopSideVisible()
  {
    List<Integer> topLayers = getTopLayers();
    for (Integer layerNum : topLayers)
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true;
    }
    return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isBottomSideVisible()
  {
    for (Integer layerNum: getBottomLayers())
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true;
    }
    return false;
  }

  /**
   * Set the top layers to be visible by default
   * Bill Darbie
   */
  private void setVisibility()
  {
    Assert.expect(_graphicsEngine != null);
    _graphicsEngine.setVisible(getBottomLayers(), false);
    _graphicsEngine.setVisible(getTopLayers(), true);
    _graphicsEngine.setVisible(getBoardOriginLayers(), true);
    _graphicsEngine.setVisible(getBoardNameLayers(), true);
    _graphicsEngine.setVisible(_topPadNameLayer, false);
    _graphicsEngine.setVisible(_bottomPadNameLayer, false);
    _graphicsEngine.setVisible(_virtualLiveAreaLayer, true);
    _graphicsEngine.setVisible(_topVirtualLiveVerificationImageLayer, true);
    _graphicsEngine.setVisible(_bottomVirtualLiveVerificationImageLayer, false);
    _graphicsEngine.setVisible(_bothVirtualLiveVerificationImageLayer, false);
  }

  /**
   * Set up the proper colors for the layers
   * Bill Darbie
   */
  private void setColors()
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.setForeground(getPadLayers(), LayerColorEnum.SURFACEMOUNT_PAD_COLOR.getColor());
    _graphicsEngine.setForeground(getReferenceDesignatorLayers(), LayerColorEnum.REFERENCE_DESIGNATOR_COLOR.getColor());

    _graphicsEngine.setForeground(getTopComponentLayers(), LayerColorEnum.TOP_COMPONENT_COLOR.getColor());
    _graphicsEngine.setForeground(getBottomComponentLayers(), LayerColorEnum.BOTTOM_COMPONENT_COLOR.getColor());

    _graphicsEngine.setForeground(getBoardLayers(), LayerColorEnum.NEW_CAD_BOARD_COLOR.getColor());
    _graphicsEngine.setForeground(getPanelLayers(), LayerColorEnum.PANEL_COLOR.getColor());
    _graphicsEngine.setForeground(getFiducialLayers(), LayerColorEnum.FIDUCIAL_COLOR.getColor());

    _graphicsEngine.setForeground(getBoardOriginLayers(), LayerColorEnum.BOARD_ORIGIN_COLOR.getColor());
    _graphicsEngine.setForeground(getBoardNameLayers(), LayerColorEnum.BOARD_NAME_COLOR.getColor());

    _graphicsEngine.setForeground(_topPadNameLayer, LayerColorEnum.PAD_NAME_COLOR.getColor());
    _graphicsEngine.setForeground(_bottomPadNameLayer, LayerColorEnum.PAD_NAME_COLOR.getColor());
    _graphicsEngine.setForeground(_virtualLiveAreaLayer,LayerColorEnum.VIRTUALLIVE_AREA_BORDER_COLOR.getColor()); 
    _graphicsEngine.setForeground(_topVirtualLiveVerificationImageLayer, LayerColorEnum.PANEL_COLOR.getColor());
    _graphicsEngine.setForeground(_bottomVirtualLiveVerificationImageLayer, LayerColorEnum.PANEL_COLOR.getColor());
    _graphicsEngine.setForeground(_bothVirtualLiveVerificationImageLayer, LayerColorEnum.PANEL_COLOR.getColor());
  }

  /**
   * Set up the threshold settings for each layer
   * Bill Darbie
   */
  private void setThresholds()
  {
    Assert.expect(_graphicsEngine != null);

    // since there are a lot of pads set a threshold for when they get created
    // only show reference designators at a certain threshold 25400 nm = 1 mil
    _graphicsEngine.setCreationThreshold(getPadLayers(), 25400 * 20, this);
    _graphicsEngine.setVisibilityThreshold(getReferenceDesignatorLayers(), 25400 * 30);
  }

  /**
   * Set up which layers to use when figuring out how big the whole deal is
   * Bill Darbie
   */
  private void setLayersToUseWhenFittingToScreen()
  {
    Collection<Integer> layers = new ArrayList<Integer>(2);
    layers.addAll(getPanelLayers());
    layers.addAll(getBoardLayers());

//    layers.addAll(getRegionLayers());

    Assert.expect(_graphicsEngine != null);
    _graphicsEngine.setLayersToUseWhenFittingToScreen(layers);
  }

  /**
   * Flip the panel so all top and bottom layers are swapped.
   * @author Bill Darbie
   */
  private void flipPanel()
  {
    Assert.expect(_panel != null);
    for (Board board: _panel.getBoards())
    {
      flipBoard(board);
    }

    // swap all renderers between top and bottom layers
    _graphicsEngine.swapLayers(_topPanelLayer, _bottomPanelLayer);
    _graphicsEngine.swapLayers(_topPanelFiducialLayer, _bottomPanelFiducialLayer);
    _graphicsEngine.swapLayers(_topPanelFiducialRefDesLayer, _bottomPanelFiducialRefDesLayer);

    // now call validateData()
    _graphicsEngine.validateData(getPanelLayers());
    _graphicsEngine.validateData(getPanelFiducialLayers());
  }

  /**
   * Flip a board so that top and bottom layers are swapped.
   * @author Bill Darbie
   */
  private void flipBoard(Board board)
  {
    Assert.expect(board != null);
    // now tell the engine to swap the layers
    _graphicsEngine.swapLayers(getTopBoardLayer(board), getBottomBoardLayer(board));
    _graphicsEngine.swapLayers(getTopPadLayer(board), getBottomPadLayer(board));
    _graphicsEngine.swapLayers(getTopComponentLayer(board), getBottomComponentLayer(board));
    _graphicsEngine.swapLayers(getTopReferenceDesignatorLayer(board), getBottomReferenceDesignatorLayer(board));
    _graphicsEngine.swapLayers(getTopBoardFiducialLayer(board), getBottomBoardFiducialLayer(board));

    // call validateData() on all layers affected
    _graphicsEngine.validateData(getAllLayers(board));
  }

  /**
   * @author Andy Mechtenberg
   */
  private void createBoard(Board board)
  {
    Assert.expect(board != null);

    // first, create the layers needed by the board
    _boardToTopBoardLayerMap.put(board, new Integer(++_layerNumber));
    _boardToBottomBoardLayerMap.put(board, new Integer(++_layerNumber));

    _boardToTopPadLayerMap.put(board, new Integer(++_layerNumber));
    _boardToBottomPadLayerMap.put(board, new Integer(++_layerNumber));

    _boardToTopComponentLayerMap.put(board, new Integer(++_layerNumber));
    _boardToBottomComponentLayerMap.put(board, new Integer(++_layerNumber));

    _boardToTopFiducialLayerMap.put(board, new Integer(++_layerNumber));
    _boardToBottomFiducialLayerMap.put(board, new Integer(++_layerNumber));

    _boardToTopRefDesLayerMap.put(board, new Integer(++_layerNumber));
    _boardToBottomRefDesLayerMap.put(board, new Integer(++_layerNumber));

    _boardToOriginLayerMap.put(board, new Integer(++_layerNumber));
    _boardToBoardNameLayerMap.put(board, new Integer(++_layerNumber));

    // now that the board layers are created (and put in the correct maps), we need to populate them
    populateBoardLayers(board);
    setColors();
    setVisibility();
    setThresholds();
    setLayersToUseWhenFittingToScreen();

    // we may be zoomed in enough to show pads or other conditional renderers, so this call will make that happen
    _graphicsEngine.validateData(getPadLayers());
  }

  /**
   * Removes all renderers associated with the passed in board instance object
   * @author Andy Mechtenberg
   * @author Kee Chin Seong 
   * Remove the board renderer handling
   */
  private void removeBoard(Board board)
  {
    Assert.expect(board != null);
    List<Integer> allLayers = getAllLayers(board);
    for(Integer layerNumber : allLayers)
    {
      _graphicsEngine.removeLayer(layerNumber.intValue());
    }

    _boardToTopBoardLayerMap.remove(board);
    _boardToBottomBoardLayerMap.remove(board);

    _boardToTopPadLayerMap.remove(board);
    _boardToBottomPadLayerMap.remove(board);

    _boardToTopComponentLayerMap.remove(board);
    _boardToBottomComponentLayerMap.remove(board);

    _boardToTopFiducialLayerMap.remove(board);
    _boardToBottomFiducialLayerMap.remove(board);

    _boardToTopRefDesLayerMap.remove(board);
    _boardToBottomRefDesLayerMap.remove(board);

    _boardToOriginLayerMap.remove(board);
    _boardToBoardNameLayerMap.remove(board);

    // remove all component renderers associated with the deleted/removed board
    for (ComponentType componentType : board.getBoardType().getComponentTypes())
    {
      List<ComponentRenderer> componentRenderers = _componentTypeToComponentRenderersMap.get(componentType);
      List<ComponentRenderer> componentRenderersToDelete = new ArrayList<ComponentRenderer>();
      if(componentRenderers != null)
      {
        for (ComponentRenderer componentRenderer : componentRenderers)
        {
          // build the list of component renderers to remove. Choose only those whose components belong to the
          // board being removed
          if (componentRenderer.getComponent().getBoard() == board)
            componentRenderersToDelete.add(componentRenderer);
        }
        componentRenderers.removeAll(componentRenderersToDelete);
        
        // replace the list of component renderers with the reduced list
        _componentTypeToComponentRenderersMap.put(componentType, componentRenderers);
      }
    }

    // remove all component reference designators renderers associated with the deleted/removed board
    for (ComponentType componentType : board.getBoardType().getComponentTypes())
    {
      List<ReferenceDesignatorRenderer> refDesRenderers = _componentTypeToReferenceDesignatorRenderersMap.get(componentType);
      List<ReferenceDesignatorRenderer> refDesRenderersToDelete = new ArrayList<ReferenceDesignatorRenderer>();
      
      if(refDesRenderers != null)
      {
        for (ReferenceDesignatorRenderer refDesRenderer : refDesRenderers)
        {
          // build the list of refDes renderers to remove. Choose only those whose components belong to the
          // board being removed
          if (refDesRenderer.getComponent().getBoard() == board)
            refDesRenderersToDelete.add(refDesRenderer);
        }
        refDesRenderers.removeAll(refDesRenderersToDelete);
        
        // replace the list of refDes renderers with the reduced list
        _componentTypeToReferenceDesignatorRenderersMap.put(componentType, refDesRenderers);
      } 
    }
  }

  /**
   * This call will create PadRenderers for any Components that are currently visible
   * on the screen and add them to the GraphicsEngine.
   * Do not let user see the pad layers when been disabled, XXL version
   * @author Bill Darbie
   * @author Kee Chin Seong
   */
  public void updateRenderers(GraphicsEngine graphicsEngine, int layerNumber)
  {
    if(getTopPadLayers().contains(new Integer(layerNumber)) && _showTopPadLayer == false)
      return;
    else 
      if(getBottomPadLayers().contains(new Integer(layerNumber)) && _showBottomPadLayer == false)
        return;
    
    Assert.expect(graphicsEngine != null);
    Assert.expect(graphicsEngine == _graphicsEngine);

    Integer layerNumberInteger = new Integer(layerNumber);
    List<Integer> padLayers = getPadLayers();
    if (padLayers.contains(layerNumberInteger))
    {
//      System.out.println("Updating Renderers, graphics engine contents:");
//      printRenderers();
      boolean topLayer = false;
      List<Integer> topPadLayers = getTopPadLayers();
      if (topPadLayers.contains(layerNumberInteger))
        topLayer = true;

      Set<ComponentRenderer> componentRenderersShowingPadsSet = new HashSet<ComponentRenderer>();

      // figure out the current list of Components that already have PadRenderers
      Collection<com.axi.guiUtil.Renderer> padRenderers = null;
      if (topLayer)
        padRenderers = graphicsEngine.getRenderers(getTopPadLayers());
      else
        padRenderers = graphicsEngine.getRenderers(getBottomPadLayers());

      for (com.axi.guiUtil.Renderer padRenderer : padRenderers)
      {
        ComponentRenderer componentRenderer = ((PadRenderer)padRenderer).getComponentRenderer();
        componentRenderersShowingPadsSet.add(componentRenderer);
      }

      // get the current list of component renderers
      Collection<com.axi.guiUtil.Renderer> componentRenderers = null;
      if (topLayer)
        componentRenderers = graphicsEngine.getRenderers(getTopComponentLayers());
      else
        componentRenderers = graphicsEngine.getRenderers(getBottomComponentLayers());

      if (graphicsEngine.isZoomLevelBelowCreationThreshold(layerNumber))
      {
        // if we crossed the pad creation threshold, then remove all entries from _componentRendererToPadRenderersMap
        // for all components who currently have pads on the passed in layer. The graphics engine whould have taken care
        // of removing the actual pad renderers from its layer
        for (ComponentRenderer componentRendererWithPads : componentRenderersShowingPadsSet)
        {
          componentRendererToPadRenderersMapRemove(componentRendererWithPads);
        }
        // remove pad name layer
        graphicsEngine.removeRenderers(_topPadNameLayer);
        graphicsEngine.removeRenderers(_bottomPadNameLayer);
      }
      else // this is a 'regular' renderer update call whenever the zoom level is above pad renderer creation threshold
      {
        // iterate over each component to see if it is visible or not
        for (com.axi.guiUtil.Renderer renderer : componentRenderers)
        {
          ComponentRenderer componentRenderer = (ComponentRenderer)renderer;
          //System.out.println("UR:Processing component renderer for: " + componentRenderer.getComponent().getReferenceDesignator());
          boolean isVisible = graphicsEngine.wouldRendererBeVisibleOnScreenIfLayerWereVisible(componentRenderer);
          if (isVisible)
          {
            //System.out.println("UR:The Component is visible");
            // the component is visible, create its PadRenderers in memory if they do not already
            // exist and add them to the GraphicsEngine
            if (componentRenderersShowingPadsSet.contains(componentRenderer) == false)
            {
              //System.out.println("UR:The Component is NOT part of set of compRenderers showing pads - create its pads");
              componentRenderersShowingPadsSet.add(componentRenderer);
              Component component = componentRenderer.getComponent();
              boolean componentIsTopSide = component.getSideBoard().isTopSide();

              // first do top side pads for the component
              Collection<Pad> pads = component.getPads();
              java.util.List<PadRenderer> padRenderersList = new ArrayList<PadRenderer>();
              for (Pad pad : pads)
              {
                Integer topPadLayer = _boardToTopPadLayerMap.get(pad.getComponent().getBoard());
                Integer bottomPadLayer = _boardToBottomPadLayerMap.get(pad.getComponent().getBoard());
                // we want to make sure we put the pad on proper board's layer.  For multi-board panels,
                // each board has it's own top and bottom pad layer.  Without this check, we'd put all the pads
                // for each board on one layer.
                if ((layerNumber == topPadLayer.intValue()) || layerNumber == bottomPadLayer.intValue())
                {
                  PadRenderer padRenderer = new PadRenderer(pad, componentRenderer);
                  PadNameRenderer padNameRenderer = new PadNameRenderer(pad, false, false);
                  padRenderersList.add(padRenderer);

                  //if(layerNumber == topPadLayer.intValue())
                 //   graphicsEngine.addRenderer(topPadLayer.intValue(), padRenderer);
                 //else
                  //  graphicsEngine.addRenderer(bottomPadLayer.intValue(), padRenderer);
                  
                  graphicsEngine.addRenderer(layerNumber, padRenderer);
                  if(layerNumber == topPadLayer.intValue())
                    graphicsEngine.addRenderer(_topPadNameLayer, padNameRenderer);
                  else
                    graphicsEngine.addRenderer(_bottomPadNameLayer, padNameRenderer);
                  _padToPadRendererMap.put(pad, padRenderer);

                  if (pad.isThroughHolePad())
                  {
                    // the pad is through hole, so its pads should show up on both sides
                    padRenderer = new PadRenderer(pad, componentRenderer);
                    padRenderersList.add(padRenderer);

                    // put the pad on the correct layer - opposite of what layer the
                    // component is on since it is a bottom side Pad
                    Board board = component.getSideBoard().getBoard();
                    if (componentIsTopSide)
                      graphicsEngine.addRenderer(getBottomPadLayer(board), padRenderer);
                    else
                      graphicsEngine.addRenderer(getTopPadLayer(board), padRenderer);
                  }
                }
              }

              // System.out.println("wpd padRenderersList: " + padRenderersList);
              componentRendererToPadRenderersMapPut(componentRenderer, padRenderersList);
            }
          }
          else
          {
            //System.out.println("UR:The Component is NOT visible");
            // component is not visible.  If its PadRenderers are currently in the GraphicsEngine
            // remove them.
            if (componentRenderersShowingPadsSet.contains(componentRenderer))
            {
              //System.out.println("UR:The Component is part of set of compRenderers showing pads - remove pads if they exist");
              componentRenderersShowingPadsSet.remove(componentRenderer);
              java.util.List<PadRenderer> padRenderersList = _componentRendererToPadRenderersMap.get(componentRenderer);
              if (padRenderersList != null)
              {
                //System.out.println("UR:The Component has pads - remove them");
                componentRendererToPadRenderersMapRemove(componentRenderer);

                for (PadRenderer padRenderer : padRenderersList)
                {
                  graphicsEngine.removeRenderer(padRenderer);
                  _padToPadRendererMap.remove(padRenderer.getPad());
                }
              }
            }
          }
        }
      }
    }
    else
    {
      // the layer passed in is not valid
      Assert.expect(false, "Pad Layer " + layerNumberInteger + " does not exist");
    }
  }

  /**
   * Checks to see if the pad layers (for either top side or bottom side) are currently visible.
   * This depends on the creation threshold and whether the user has selected to view top or bottom side.
   * @author Andy Mechtenberg
   */
  boolean arePadsVisible(boolean topSide)
  {
    List<Integer> padLayers;

    if (topSide)
     padLayers = getTopPadLayers();
    else
      padLayers = getBottomPadLayers();

    for (Integer layerNum : padLayers)
    {
      boolean padsVisible = _graphicsEngine.isLayerVisible(layerNum.intValue());
      if (padsVisible)
        return true;
    }

    return false;
  }

  /**
   * This class is an observer of the datastore layer. Any changes made
   * to the datastore will trigger an update call. We will look at the
   * change event and determine if anything needs to be updated.
   * It's also an observer of the GUI screens and renderers selected in the graphics engine
   * @author chin-seong.kee
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProjectObservable)
        {
          if (argument instanceof ProjectChangeEvent)
          {
            final ProjectChangeEvent event = (ProjectChangeEvent)argument;
            ProjectChangeEventEnum eventEnum = ((ProjectChangeEvent)argument).getProjectChangeEventEnum();

            if (eventEnum instanceof BoardEventEnum)
            {
              updateBoardData(event);
            }
            else if (eventEnum instanceof BoardTypeEventEnum)
            {
              updateBoardTypeData(event);
            }
            else if (eventEnum instanceof ComponentEventEnum)
            {
//              updateComponentData(event);
            }
            else if (eventEnum instanceof ComponentTypeEventEnum)
            {
              ComponentTypeEventEnum compTypeEventEnum = (ComponentTypeEventEnum)eventEnum;
              if (compTypeEventEnum.equals(ComponentTypeEventEnum.COORDINATE))
                MainMenuGui.getInstance().getTestDev().updateProgressDialog();
              updateComponentTypeData(event);
            }
            else if(eventEnum instanceof ComponentTypeSettingsEventEnum)
            {
              updateComponentTypeLoadedData(event, argument);
            }
            else if (eventEnum instanceof CompPackageEventEnum)
            {
              // do nothing -- panel graphics show land patterns, not packages
            }
            else if (eventEnum instanceof FiducialEventEnum)
            {
               updateFiducialData(event);
            }
            else if (eventEnum instanceof FiducialTypeEventEnum)
            {
              updateFiducialTypeData(event);
            }
            else if (eventEnum instanceof LandPatternPadEventEnum)
            {
              updateLandPatternPadData(event);
            }
            else if (eventEnum instanceof LandPatternEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof PackagePinEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof PadEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof PanelEventEnum)
            {
              // chin-seong.kee - Prevent crash when create dummy board from TestDev environment
              if(Project.isUsingVirtualDummyProject() == false && MainMenuGui.getInstance().isCadCreatorCurrentEnvironment() == false)  
                updatePanelData(event);
            }
            else if (eventEnum instanceof PanelSettingsEventEnum)
            {
              // chin-seong.kee - Prevent crash when create dummy board from TestDev environment
              if(Project.isUsingVirtualDummyProject() == false  && MainMenuGui.getInstance().isCadCreatorCurrentEnvironment() == false)
                updatePanelSettingsData(event);
            }
            else if (eventEnum instanceof ProjectEventEnum)
            {
              updateProjectData(event);
            }
            else if (eventEnum instanceof SideBoardEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof SideBoardTypeEventEnum)
            {
              updateSideBoardTypeData(event);
            }
            else if (eventEnum instanceof SideBoardTypeSettingsEventEnum)
            {
              // do nothing
            }
            else if (eventEnum instanceof ThroughHoleLandPatternPadEventEnum)
            {
              updateThroughHoleLandPatternPadData(event);
            }
            // commmented out.  This was used to implement graphical undo, but we decided
            // not to support it.  It was causing a deadlock with other updates in some situations
            // _guiObservable.stateChanged(this, GuiUpdateEventEnum.CAD_GRAPHICS);
          }
        }
        else if (observable instanceof SelectedRendererObservable)
        {
          handleSelectedRenderers(argument);
        }
        else if (observable instanceof GuiObservable)
        {
          handleGuiEvent((GuiEvent)argument);
        }
        else if (observable instanceof DisplayPadNameObservable)
        {
          if (argument instanceof Boolean)
          {
            Boolean setPadNameVisible = (Boolean)argument;
            if (setPadNameVisible.booleanValue())
            {
              _showPadName = true;
              if(isTopSideVisible())
                _graphicsEngine.setVisible(_topPadNameLayer, true);
              if(isBottomSideVisible())
                _graphicsEngine.setVisible(_bottomPadNameLayer, true);
            } 
            else
            {
              _showPadName = false;
              _graphicsEngine.setVisible(_topPadNameLayer, false);
              _graphicsEngine.setVisible(_bottomPadNameLayer, false);
            }
          }
        }
        else if (observable instanceof VirtualLiveImageGenerationSettings ||
                 observable instanceof CadCreatorSelectedAreaSettings)
        {
          handleVirtualLiveImageGenerationDataObservable(argument);
        }
        else if (observable instanceof SelectedAreaObservable)
        {
          if(argument instanceof Shape)
          {
            if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment())
            {
              handleSelectedAreaObservable((Shape)argument);
            }
            else if(MainMenuGui.getInstance().isCadCreatorCurrentEnvironment())
            {
              handleSelectedAreaOnEmptyCadObservable((Shape)argument);
            }
          }
        }
        else
        {
          Assert.expect(false, "PanelGraphicsEngineSetup: No update code for observable: " + observable.getClass().getName());
        }
      }
    });
  }
  
  /**
   * This method will take a collection of Renderers that have been selected in the GraphicsEngine and
   * determine which to highlight in the graphics.
   * @author Andy Mechtenberg
   */
  private void handleSelectedRenderers(final Object argument)
  {
    // notify all the PanelGraphicsEngineSetup observers that a mouse click through the Panel Graphics generated
    // a selectedRenderers change.
    notifyObservers(argument);

    _graphicsEngine.clearCrossHairs();
    _graphicsEngine.clearSelectedRenderers();
    _graphicsEngine.clearAllFlashingRenderersAndLayers();

    // there are two modes of selection:
    // 1.  The user clicked on a cad feature
    // When using this method, a complex algorithm to figure out what to select is used (see below)
    // 2.  The user used the group select mode to draw a rectangle around a bunch of stuff
    // When using the second method, EVERYTHING is selected

    // Let's handle the 2nd method first (because it's easier)
    // if this wasn't a group select, it still could be a CTRL-Left Click which ADDS to the selections.  Treat this as a group select
    if (_graphicsEngine.wasSelectionPartOfAGroupSelection())
    {
      /** Warning "unchecked cast" approved for cast from Object types.*/
      for (com.axi.guiUtil.Renderer renderer : (Collection<com.axi.guiUtil.Renderer>)argument)
      {
        if (MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment())
        {
          if (renderer != null) // could be null if they clicked outside the boundary of any renderer
          {
            if (renderer instanceof PadRenderer)
            {
              continue;
            }
          }
        }
        _graphicsEngine.setSelectedRenderer(renderer);
        if(renderer != null) // could be null if they clicked outside the boundary of any renderer
        {
          if(renderer instanceof ComponentRenderer)
          {
            if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment())
            {
              ComponentRenderer compRenderer = (ComponentRenderer) renderer;              
              //Jack Hwee
              VirtualLiveImageGenerationSettings.getInstance().setBoardName(compRenderer.getComponent().getBoard().getName()); 
              VirtualLiveImageGenerationSettings.getInstance().addComponentRefDes(compRenderer.getComponent().getReferenceDesignator(), compRenderer.getComponent().getBoard().getPanel().isMultiBoardPanel());
           
              VirtualLiveImageGenerationSettings.getInstance().addVirtualLiveImageGenerationData(compRenderer.getComponent().getReferenceDesignator(),
                                                                                                 compRenderer.getComponent().getShapeRelativeToPanelInNanoMeters());
              
              VirtualLiveImageGenerationSettings.getInstance().setObservable(compRenderer.getComponent().getShapeRelativeToPanelInNanoMeters());        
            }
          }        
        }
      }      
      
       //Jack Hwee - only allow user to select 3 components
      if (MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment() && VirtualLiveImageGenerationSettings.getInstance().getComponentList().size() > 3)
      {
        String componentToBeRemoved = VirtualLiveImageGenerationSettings.getInstance().getComponentList().get(0);
        String boardName = "1";
        if (Project.getCurrentlyLoadedProject().getPanel().isMultiBoardPanel())
        {
          String[] refDesBoardName = componentToBeRemoved.split("#");
          componentToBeRemoved = refDesBoardName[0];
          boardName = refDesBoardName[1];
        }        
        VirtualLiveImageGenerationSettings.getInstance().getComponentList().remove(0);
        _graphicsEngine.clearSelectedRenderers();
        for (com.axi.guiUtil.Renderer renderer : (Collection<com.axi.guiUtil.Renderer>) argument)
        {
          if (renderer instanceof ComponentRenderer)
          {
            ComponentRenderer compRenderer = (ComponentRenderer) renderer;
            if (compRenderer.getComponent().getBoard().getPanel().isMultiBoardPanel() == false)
            {
              if (compRenderer.getComponent().getReferenceDesignator().equals(componentToBeRemoved) == false)
              {
                _graphicsEngine.setSelectedRenderer(renderer);
                 VirtualLiveImageGenerationSettings.getInstance().setObservable(compRenderer.getComponent().getShapeRelativeToPanelInNanoMeters());
              }
            }
            else
            {   
              if (compRenderer.getComponent().getReferenceDesignator().equals(componentToBeRemoved) == false || compRenderer.getComponent().getBoard().getName().equals(boardName) == false)
              {
                _graphicsEngine.setSelectedRenderer(renderer);
                VirtualLiveImageGenerationSettings.getInstance().setObservable(compRenderer.getComponent().getShapeRelativeToPanelInNanoMeters());
              }
            }       
          }
        }
      }
      _createNewCadPanel.setDescription(" "); // clear out the descriptive text because lots of things were selected
    }
    else
    {
      int i = 0;
      com.axi.guiUtil.Renderer rendererToUse = null;
      // here, I want to hightlight ONE renderer, even though I may have a lot in the Collection.  The
      // collection is not order dependent, so I'll need to iterate through and find the one I want to use.
      // The ONE will be, in order:
      //  1.  A Fiducial renderer.  If more than one fiducial, choose one from the TOP side
      //  2.  A Pad renderer.  If more than one pad renderer, choose one from the TOP side
      //  3.  A component renderer.  If more than one, choose one from the TOP side
      //  4.  A Board Renderer
      //  5.  A panel renderer

      /** Warning "unchecked cast" approved for cast from Object types.*/
      for (com.axi.guiUtil.Renderer renderer : (Collection<com.axi.guiUtil.Renderer>)argument)
      {
        ++i;
        // if it's a fiducial, use it
        if (renderer instanceof FiducialRenderer)
        {
          FiducialRenderer fiducialRenderer = (FiducialRenderer)renderer;
          if (rendererToUse == null) // haven't found anything yet
            rendererToUse = renderer;
          else // we have a fiducial renderer to consider, so compare to rendererToUse
          {
            if (rendererToUse instanceof FiducialRenderer)
            {
              if (fiducialRenderer.getFiducial().isTopSide())
                rendererToUse = renderer;
            }
            else // the previously chosen one is not a fiducial, so make it this fiducial
              rendererToUse = renderer;
          }
        }
        // if it's a pad, use it
        else if (renderer instanceof PadRenderer)
        {
          PadRenderer padRenderer = (PadRenderer)renderer;
          if (rendererToUse == null) // haven't found anything yet
            rendererToUse = renderer;
          else // we have a pad renderer to consider, so compare to rendererToUse
          {
            if (rendererToUse instanceof PadRenderer)
            {
              if (padRenderer.getPad().isTopSide())
                rendererToUse = renderer;
            }
            else // the previously chosen one is not a pad, so make it this pad
              rendererToUse = renderer;
          }
        }
        else if (renderer instanceof ComponentRenderer)
        {
          ComponentRenderer compRenderer = (ComponentRenderer)renderer;
          if (rendererToUse == null) // haven't found any yet, so use this one
            rendererToUse = renderer;
          else
          {
            if ((rendererToUse instanceof PadRenderer) == false) // already using a pad, so don't change to component
            {
              // our renderer is not a pad, so we'll use this current one
              if (compRenderer.getComponent().isTopSide())
                rendererToUse = renderer;
              else if (rendererToUse instanceof ComponentRenderer)
              {
                // our state here:  rendererToUse is a component and the one under consideration is a component
                // so only switch if rendererToUse is on the bottom and the renderer is on the top
                ComponentRenderer compRendererToUse = (ComponentRenderer)rendererToUse;
                if ((compRendererToUse.getComponent().isTopSide() == false) &&
                    (compRenderer.getComponent().isTopSide()))
                  rendererToUse = renderer;
              }
              else
                rendererToUse = renderer;
            }
          }
        }
        else if ((renderer instanceof InspectionRegionRenderer) ||
                 (renderer instanceof VerificationRegionRenderer)||
                 (renderer instanceof RectangleRenderer))
        {
          if (rendererToUse == null) // haven't found any yet, so use this one
            rendererToUse = renderer;
          else
          {
            if (((rendererToUse instanceof PadRenderer) == false) &&
                ((rendererToUse instanceof ComponentRenderer) == false))// already using a pad, so don't change to component
              rendererToUse = renderer;
          }
        }
        else if (renderer instanceof BoardRenderer)
        {
          // only use if nothing or set to panel renderer
          if (rendererToUse == null)
            rendererToUse = renderer;
          else
          {
            if (rendererToUse instanceof PanelRenderer)
              rendererToUse = renderer;
          }
        }
        else // last resort
        {
          if (rendererToUse == null)
            rendererToUse = renderer;
        }
        // when i==1, it's the first on list.  Use that unless it's already been set by the pad
        if ((i == 1) && (rendererToUse == null))
          rendererToUse = renderer;
      }
      if(rendererToUse != null) // could be null if they clicked outside the boundary of any renderer
      {
        if(rendererToUse instanceof ComponentRenderer)
        {
          if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment())
          {
            ComponentRenderer compRenderer = (ComponentRenderer) rendererToUse;
            //Jack Hwee         
            VirtualLiveImageGenerationSettings.getInstance().clearComponentList();
            VirtualLiveImageGenerationSettings.getInstance().setBoardName(compRenderer.getComponent().getBoard().getName());
            VirtualLiveImageGenerationSettings.getInstance().addComponentRefDes(compRenderer.getComponent().getReferenceDesignator(), compRenderer.getComponent().getBoard().getPanel().isMultiBoardPanel());
         
            VirtualLiveImageGenerationSettings.getInstance().setComponentRefDes(compRenderer.getComponent().getReferenceDesignator());
            
            VirtualLiveImageGenerationSettings.getInstance().setVirtualLiveImageGenerationData(compRenderer.getComponent().getShapeRelativeToPanelInNanoMeters());
            drawVirtualLiveAreaDisplay();
            _graphicsEngine.setSelectedRenderer(compRenderer);
          }
          else
          {
            _rendererToUse = rendererToUse;
            // now that we have the renderer of interest, we need to highlight it!
            _graphicsEngine.setSelectedRenderer(rendererToUse);
            setStatusBarDescription(rendererToUse);
          }
        }
        else if(rendererToUse instanceof PadRenderer)
        {
          if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment())
          {
            PadRenderer padRenderer = (PadRenderer) rendererToUse;
            ComponentRenderer compRenderer =  padRenderer.getComponentRenderer();        
            //Jack Hwee         
            VirtualLiveImageGenerationSettings.getInstance().clearComponentList();
            VirtualLiveImageGenerationSettings.getInstance().setBoardName(compRenderer.getComponent().getBoard().getName());
            VirtualLiveImageGenerationSettings.getInstance().addComponentRefDes(compRenderer.getComponent().getReferenceDesignator(), compRenderer.getComponent().getBoard().getPanel().isMultiBoardPanel());
         
            VirtualLiveImageGenerationSettings.getInstance().setComponentRefDes(compRenderer.getComponent().getReferenceDesignator());
           
            VirtualLiveImageGenerationSettings.getInstance().setVirtualLiveImageGenerationData(compRenderer.getComponent().getShapeRelativeToPanelInNanoMeters());
            drawVirtualLiveAreaDisplay();
            _graphicsEngine.setSelectedRenderer(compRenderer);
          }
        }
        else
        { 
          //Jack Hwee      
          if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment())
          {
            //Jack Hwee
            VirtualLivePanel.getInstance().resetAllVirtualLiveSettings();
            drawVirtualLiveAreaDisplay();
            //_graphicsEngine.clearRegionCrossHairs();
            clearVirtualLiveAreaDisplay();     
            _graphicsEngine.invalidate();
          }
        }
      }
    }
  }

  /**
   * Updates the status bar description based on the renderer passed in
   * @author Andy Mechtenberg
   */
  private void setStatusBarDescription(com.axi.guiUtil.Renderer renderer)
  {
    Assert.expect(renderer != null);
    String description = " ";
    // handle all the normal types of selections: panel board component pad
    if (renderer instanceof PanelRenderer)
    {
      LocalizedString panelString = new LocalizedString("GUI_PANEL_SUMMARY_KEY", new Object[]{renderer.toString()});
      description = StringLocalizer.keyToString(panelString);
    }
    else if (renderer instanceof BoardRenderer)
    {
      BoardRenderer boardRenderer = (BoardRenderer)renderer;

      String projName = boardRenderer.getBoard().getPanel().getProject().getName();
      LocalizedString panelString = new LocalizedString("GUI_PANEL_SUMMARY_KEY", new Object[]{projName});
      description = StringLocalizer.keyToString(panelString);
      LocalizedString boardString = new LocalizedString("GUI_BOARD_SUMMARY_KEY", new Object[]{renderer.toString()});
      description = StringLocalizer.keyToString(panelString) + " " + StringLocalizer.keyToString(boardString);

    }
    else if (renderer instanceof ComponentRenderer)
    {
      Component component = ((ComponentRenderer)renderer).getComponent();
      description = getComponentDescriptionString(component);
    }
    else if (renderer instanceof PadRenderer)
    {
      Pad pad = ((PadRenderer)renderer).getPad();
      description = getPadDescriptionString(pad);
    }
    else if (renderer instanceof FiducialRenderer)
    {
      Fiducial fiducial = ((FiducialRenderer)renderer).getFiducial();
      LocalizedString fiducialString = null;
     if (fiducial.isOnBoard())
       fiducialString = new LocalizedString("GUI_BOARD_FIDUCIAL_SUMMARY_KEY", new Object[]{fiducial.getName()});
     else if (fiducial.isOnPanel())
       fiducialString = new LocalizedString("GUI_PANEL_FIDUCIAL_SUMMARY_KEY", new Object[]{fiducial.getName()});
     else
       Assert.expect(false, "Found fiducial that is neither on the board nor on the panel");

     description = StringLocalizer.keyToString(fiducialString);
    }
    else// if (renderer instanceof InspectionRegionRenderer) || (renderer instanceof VerificationRegionRenderer))
    {
      description = renderer.toString();
    }
    _createNewCadPanel.setDescription(description);
  }

  /**
   * @author Andy Mechtenberg
   */
  private String getComponentDescriptionString(Component component)
  {
    Assert.expect(component != null);
    // component is quite complex.  We want to display the package, land pattern and possibly joint type
    // and subtype
    String jointType = TestDev.getComponentDisplayJointType(component.getComponentType());

    String subType = TestDev.getComponentDisplaySubtype(component.getComponentType());

    LocalizedString localizedDescription = new LocalizedString("GUI_COMPONENT_SUMMARY_KEY",
        new Object[]
        {component.getReferenceDesignator(),
        component.getCompPackage().toString(),
        component.getLandPattern().getName(),
        jointType,
        subType});
    return StringLocalizer.keyToString(localizedDescription);
  }

  /**
   * @author Andy Mechtenberg
   */
  private String getPadDescriptionString(Pad pad)
  {
    Assert.expect(pad != null);
    Component component = pad.getComponent();
    PadType padType = pad.getPadType();
    // pad, like component, is quite complex.  We want to display the package, land pattern and possibly joint type
    // and subtype.  But pad joint type and subtype cannot be "mixed", so that's a little simpler
    String jointType = pad.getJointTypeEnum().getName();

    String subType = "";
    if (pad.isTestable() == false)
      subType = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_UNTESTABLE_KEY");
    else if (padType.getComponentType().isLoaded() == false)
      subType = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOLOAD_KEY");
    else if (padType.isInspected() == false)
      subType = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOTEST_KEY");
    else
      subType = padType.getSubtype().getShortName();

    LocalizedString padDescription = new LocalizedString("GUI_PIN_SUMMARY_KEY", new Object[]{pad.getName()});

    LocalizedString componentDescription = new LocalizedString("GUI_COMPONENT_SUMMARY_KEY",
        new Object[]
        {component.getReferenceDesignator(),
        component.getCompPackage().toString(),
        component.getLandPattern().getName(),
        jointType,
        subType});
    return StringLocalizer.keyToString(padDescription) + " " + StringLocalizer.keyToString(componentDescription);
  }


  /**
   * @author Andy Mechtenberg
   */
  private void highlightComponentType(ComponentType compType)
  {      
    java.util.List<ComponentRenderer> renderersToHighlight = findComponentRenderers(compType);
    // first, figure out which side(s) of the panel we need to make visible in order to get all the
    // the component renderers visible.  Because of flipped panels, both sides may need to be made visible
    boolean topComponentsVisible = isTopSideVisible();
    boolean bottomComponentsVisible = isBottomSideVisible();
    boolean componentOnTop = false;
    boolean componentOnBottom = false;

    //There is no use to continue the code if the renderers size is 0
    if(renderersToHighlight.size() == 0)
      return; 
    
    for (ComponentRenderer componentRenderer : renderersToHighlight)
    {
      if (componentRenderer.getComponent().isTopSide())
        componentOnTop = true;
      else
        componentOnBottom = true;
    }
    if (componentOnTop && componentOnBottom) // flipped board case, must show both sides at once!
    {
      _controlToolBar.showBothSides();
    }
    else // the user has only one side to show, so if they've only got one side visible, respect that only make what needs to be visible
    {
      if (componentOnTop && (topComponentsVisible == false))
      {
        _controlToolBar.showTopSide();
      }
      if ((componentOnBottom) && (bottomComponentsVisible == false))
      {
        _controlToolBar.showBottomSide();
      }
    }
    // use flashing to highlight all but the first renderer -- the first one will be highlighted with crosshairs
    // if a single board was selected, select the renderer for that board
    _graphicsEngine.clearAllFlashingRenderersAndLayers();
    ComponentRenderer firstComponent = null;
    if (_selectedBoardName == null)
    {
      firstComponent = (ComponentRenderer)renderersToHighlight.remove(0);
    }
    else
    {
      int index = 0;
      for (ComponentRenderer componentRenderer : renderersToHighlight)
      {
        if (componentRenderer.getComponent().getBoard().getName().equals(_selectedBoardName))
        {
          firstComponent = (ComponentRenderer)renderersToHighlight.remove(index);
          break;
        }
        index++;
      }
    }
    if (firstComponent == null)
    {
      firstComponent = (ComponentRenderer)renderersToHighlight.remove(0);
    }
    if (renderersToHighlight.isEmpty() == false)
    {
      _graphicsEngine.setFlashRenderers(renderersToHighlight, java.awt.Color.YELLOW);
      _graphicsEngine.setFlashing(false);
    }
    // use crosshairs to highlight ONE of the components -- the first one in the list, which we just removed
    _graphicsEngine.selectWithCrossHairs(firstComponent);

    // move to center if zoom is not at 1:1
    if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0) == false)
    {
      // we're at some zoom level, let's center the graphics to show the new alignment region
      DoubleCoordinate coordinate = firstComponent.getCenterCoordinate();
      _graphicsEngine.center(coordinate.getX(), coordinate.getY(), 0, 0);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void highlightFiducialType(FiducialType fidType)
  {
    java.util.List<FiducialRenderer> renderersToHighlight = getFiducialRenderers(fidType);
    // first, figure out which side(s) of the panel we need to make visible in order to get all the
    // the component renderers visible.  Because of flipped panels, both sides may need to be made visible
    boolean topFiducialsVisible = isTopSideVisible();
    boolean bottomFiducialsVisible = isBottomSideVisible();
    boolean fiducialOnTop = false;
    boolean fiducialOnBottom = false;

    for (FiducialRenderer fiducialRenderer : renderersToHighlight)
    {
      if (fiducialRenderer.getFiducial().isTopSide())
        fiducialOnTop = true;
      else
        fiducialOnBottom = true;
    }
    if (fiducialOnTop && fiducialOnBottom) // flipped board case, must show both sides at once!
    {
      _controlToolBar.showBothSides();
    }
    else // the user has only one side to show, so if they've only got one side visible, respect that only make what needs to be visible
    {
      if (fiducialOnTop && (topFiducialsVisible == false))
      {
        _controlToolBar.showTopSide();
      }
      if ((fiducialOnBottom) && (bottomFiducialsVisible == false))
      {
        _controlToolBar.showBottomSide();
      }
    }
    // use flashing to highlight all but the first renderer -- the first one will be highlighted with crosshairs
    // if a single board was selected, select the renderer for that board
    _graphicsEngine.clearAllFlashingRenderersAndLayers();
    FiducialRenderer firstFiducial = null;
    if (fidType.getFiducials().get(0).isOnBoard())
    {
      if (_selectedBoardName == null)
      {
        firstFiducial = (FiducialRenderer)renderersToHighlight.remove(0);
      }
      else
      {
        int index = 0;
        for (FiducialRenderer fiducialRenderer : renderersToHighlight)
        {
          if (fiducialRenderer.getFiducial().getSideBoard().getBoard().getName().equals(_selectedBoardName))
          {
            firstFiducial = (FiducialRenderer)renderersToHighlight.remove(index);
            break;
          }
          index++;
        }
      }
    }
    else
      firstFiducial = (FiducialRenderer)renderersToHighlight.remove(0);

    if (firstFiducial == null)
    {
      firstFiducial = (FiducialRenderer)renderersToHighlight.remove(0);
    }
    if (renderersToHighlight.isEmpty() == false)
    {
      _graphicsEngine.setFlashRenderers(renderersToHighlight, java.awt.Color.YELLOW);
      _graphicsEngine.setFlashing(false);
    }
    // use crosshairs to highlight ONE of the components -- the first one in the list, which we just removed
    _graphicsEngine.selectWithCrossHairs(firstFiducial);

    // move to center if zoom is not at 1:1
    if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0) == false)
    {
      // we're at some zoom level, let's center the graphics to show the new alignment region
      DoubleCoordinate coordinate = firstFiducial.getCenterCoordinate();
      _graphicsEngine.center(coordinate.getX(), coordinate.getY(), 0, 0);
    }
  }

  /**
   * @param components A collection of ComponentType objects
   * @author Andy Mechtenberg
   */
  private void highlightMultipleComponents(Collection<ComponentType> components)
  {
    Assert.expect(components != null);
    Assert.expect(components.size() > 1);

    for (ComponentType componentType : components)
    {
      for (ComponentRenderer componentRenderer : findComponentRenderers(componentType))
      {
        _graphicsEngine.setSelectedRenderer(componentRenderer);
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void highlightMultipleFiducials(Collection<FiducialType> fiducials)
  {
    Assert.expect(fiducials != null);
    Assert.expect(fiducials.size() > 1);

    for (FiducialType fiducialType : fiducials)
    {
      for (FiducialRenderer fiducialRenderer : getFiducialRenderers(fiducialType))
      {
        _graphicsEngine.setSelectedRenderer(fiducialRenderer);
      }
    }
  }

  /**
   * @return a List of ComponentRenderers that match the componentType passed in
   * @author Andy Mechtenberg
   */
  private java.util.List<ComponentRenderer> findComponentRenderers(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    java.util.List<ComponentRenderer> componentRenderers = new ArrayList<ComponentRenderer>();

    java.util.List<Component> components = componentType.getComponents();
    for (Component component : components)
    {
      boolean onTop = component.isTopSide();

      ComponentRenderer componentRenderer = null;


      Collection<com.axi.guiUtil.Renderer> renderers = null;
      if (onTop)
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopComponentLayers()));
      else
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomComponentLayers()));

      Collection<ComponentRenderer> compRenderers = new ArrayList<ComponentRenderer>();
      for (com.axi.guiUtil.Renderer renderer : renderers)
      {
        compRenderers.add((ComponentRenderer)renderer);
      }

      for (ComponentRenderer compRenderer : compRenderers)
      {
        com.axi.v810.business.panelDesc.Component comp = compRenderer.getComponent();
        if (comp == component)
        {
          componentRenderer = compRenderer;
          break;
        }
      }
//      Assert.expect(componentRenderer != null);
      if(componentRenderer != null)
        componentRenderers.add(componentRenderer);
    }
    return componentRenderers;
  }

  /**
   * @return a List of FiducialRenderers that match the fiducialType passed in
   * @author Laura Cormos
   */
  private java.util.List<FiducialRenderer> findFiducialRenderers(FiducialType fiducialType)
  {
    Assert.expect(fiducialType != null);
    java.util.List<FiducialRenderer> fiducialRenderers = new ArrayList<FiducialRenderer>();

    java.util.List<Fiducial> fiducials = fiducialType.getFiducials();
    for (Fiducial fiducial : fiducials)
    {
      boolean onTop = fiducial.isTopSide();

      FiducialRenderer fiducialRenderer = null;

      Collection<com.axi.guiUtil.Renderer> renderers = null;
      if (fiducial.isOnBoard())
      {
        if (onTop)
          renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopBoardFiducialLayers()));
        else
          renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomBoardFiducialLayers()));
      }
      else if (fiducial.isOnPanel())
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getPanelFiducialLayers()));
      else
        Assert.expect(false);

      Collection<FiducialRenderer> fidRenderers = new ArrayList<FiducialRenderer>();
      for (com.axi.guiUtil.Renderer renderer : renderers)
      {
        fidRenderers.add((FiducialRenderer)renderer);
      }

      for (FiducialRenderer fidRenderer : fidRenderers)
      {
        Fiducial fid = fidRenderer.getFiducial();
        if (fid == fiducial)
        {
          fiducialRenderer = fidRenderer;
          break;
        }
      }
      Assert.expect(fiducialRenderer != null);
      fiducialRenderers.add(fiducialRenderer);
    }
    return fiducialRenderers;
  }

  /**
   * @author Andy Mechtenberg
   * @EditedBy : Kee Chin Seong
   */
  public void selectPadTypes(Collection<PadType> padTypes)
  {
    Assert.expect(padTypes != null);

    java.util.List<PadRenderer> padRenderers = findPadRenderersForPadTypes(padTypes);
    
    //Kee Chin Seong - ok user might select the pad types, make sure padtype is not empty.
    if(padRenderers.size() <= 0)
      return;
    
    selectPadRenderers(padRenderers);

    // if all pads belong to the same component, then crosshair that component

    ComponentType componentTypeToSelect = null;
    for(PadType padType : padTypes)
    {
      ComponentType compType = padType.getComponentType();
      if (componentTypeToSelect == null)
        componentTypeToSelect = compType;
      else
      {
        if (compType != componentTypeToSelect)
        {
          componentTypeToSelect = null;
          break;
        }
      }
    }
    //Kee chin seong :: Make sure the component is not loaded.
    if (componentTypeToSelect != null && componentTypeToSelect.isLoaded() == true)
    {
      java.util.List<ComponentRenderer> renderersToHighlight = findComponentRenderers(componentTypeToSelect);
      ComponentRenderer firstComponent = (ComponentRenderer)renderersToHighlight.remove(0);
      _graphicsEngine.selectWithCrossHairs(firstComponent);
//      highlightComponentType(componentTypeToSelect);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void selectPadsAndFiducials(Collection<Object> padsAndFiducials)
  {
    java.util.List<Pad> pads = new ArrayList<Pad>();
    java.util.List<Fiducial> fiducials = new ArrayList<Fiducial>();
    for(Object obj : padsAndFiducials)
    {
      if (obj instanceof Pad)
      {
        Pad pad = (Pad)obj;
        pads.add(pad);
      }
      else if (obj instanceof Fiducial)
      {
        Fiducial fiducial = (Fiducial)obj;
        fiducials.add(fiducial);
      }
      else
        Assert.expect(false, "Unexpected object type: " + obj);
    }
    java.util.List<PadRenderer> padRenderers = findPadRenderersForPads(pads);
    selectPadRenderers(padRenderers);

    java.util.List<FiducialRenderer> fiducialRenderers = findFiducialRenderersForFiducials(fiducials);
    selectFiducialRenderers(fiducialRenderers);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void selectPadRenderers(java.util.List<PadRenderer> padRenderers)
  {
    Assert.expect(padRenderers != null);

    // With pads, we don't worry about top/bottom side.  If the pad is visible, it'll be selected.
    // This is because there is a zoom threshold which must be met before pads are drawn anyway, so
    // finding it is pretty easy.
//    _graphicsEngine.clearSelectedRenderers();
//    _graphicsEngine.clearCrossHairs();
//    _graphicsEngine.clearAllFlashingRenderersAndLayers();

    for (PadRenderer padRenderer : padRenderers)
    {
//      setDescription(padRenderer.toString());
      _graphicsEngine.setSelectedRenderer(padRenderer);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void selectFiducialRenderers(java.util.List<FiducialRenderer> fiducialRenderers)
  {
    Assert.expect(fiducialRenderers != null);
    for (FiducialRenderer fiducialRenderer : fiducialRenderers)
      _graphicsEngine.setSelectedRenderer(fiducialRenderer);
  }
  
  /**
   * @author Andy Mechtenberg
   */
  public void highlightPads(Collection<HighlightAlignmentItems> highlightPads)
  {
    Assert.expect(highlightPads != null);

    for(HighlightAlignmentItems highlightPad : highlightPads)
    {
      List<Pad> pads = highlightPad.getPads();
      Color highlightColor = highlightPad.getHighlightColor();
      java.util.List<PadRenderer> padRenderers = findPadRenderersForPads(pads);
      highlightPadRenderers(padRenderers, highlightColor);

      List<Fiducial> fiducials = highlightPad.getFiducials();
      java.util.List<FiducialRenderer> fiducialRenderers = findFiducialRenderersForFiducials(fiducials);
      highlightFiducialRenderers(fiducialRenderers, highlightColor);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void highlightPadRenderers(java.util.List<PadRenderer> padRenderers, Color highlightColor)
  {
    Assert.expect(padRenderers != null);

    // With pads, we don't worry about top/bottom side.  If the pad is visible, it'll be highlighted.
    // This is because there is a zoom threshold which must be met before pads are drawn anyway, so
    // finding it is pretty easy.
    for (PadRenderer padRenderer : padRenderers)
    {
//      setDescription(padRenderer.toString());
      _graphicsEngine.setHighightedRenderer(padRenderer, highlightColor);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void highlightFiducialRenderers(java.util.List<FiducialRenderer> fiducialRenderers, Color highlightColor)
  {
    Assert.expect(fiducialRenderers != null);

    // With pads, we don't worry about top/bottom side.  If the pad is visible, it'll be highlighted.
    // This is because there is a zoom threshold which must be met before pads are drawn anyway, so
    // finding it is pretty easy.
    for (FiducialRenderer fiducialRenderer : fiducialRenderers)
    {
//      setDescription(padRenderer.toString());
      _graphicsEngine.setHighightedRenderer(fiducialRenderer, highlightColor);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private java.util.List<PadRenderer> findPadRenderersForPadTypes(Collection<PadType> padTypes)
  {
    Assert.expect(padTypes != null);
    java.util.List<PadRenderer> padRenderers = new ArrayList<PadRenderer>();
    for (PadType padType : padTypes)
    {
      java.util.List<Pad> pads = padType.getPads();
      for (Pad pad : pads)
      {
        boolean onTop = pad.isTopSide();
        PadRenderer padRenderer = null;
        java.util.List<com.axi.guiUtil.Renderer> renderers = null;

        if (onTop)
          renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopPadLayers()));
        else
          renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomPadLayers()));

        for (com.axi.guiUtil.Renderer tempPadRenderer : renderers)
        {
          com.axi.v810.business.panelDesc.Pad tempPad = ((PadRenderer)tempPadRenderer).getPad();
          if (tempPad == pad)
          {
            padRenderer = (PadRenderer)tempPadRenderer;
            break;
          }
        }
        // since pads are not always shown, this renderer may not be created.  In other words, it's ok for this to be null
        if (padRenderer != null)
          padRenderers.add(padRenderer);
      }
    }
    return padRenderers;
  }

  /**
   * @author Andy Mechtenberg
   */
  private java.util.List<PadRenderer> findPadRenderersForPads(Collection<Pad> pads)
  {
    Assert.expect(pads != null);
    java.util.List<PadRenderer> padRenderers = new ArrayList<PadRenderer>();
    for (Pad pad : pads)
    {
      boolean onTop = pad.isTopSide();
      boolean throughHolePad = pad.isThroughHolePad();
      PadRenderer padRenderer = null;
      java.util.List<com.axi.guiUtil.Renderer> renderers = null;

      if (throughHolePad)
      {
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopPadLayers()));
        renderers.addAll(new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomPadLayers())));
      }
      else if (onTop)
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopPadLayers()));
      else
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomPadLayers()));

      for (com.axi.guiUtil.Renderer tempPadRenderer : renderers)
      {
        com.axi.v810.business.panelDesc.Pad tempPad = ((PadRenderer)tempPadRenderer).getPad();
        if (tempPad == pad)
        {
          padRenderer = (PadRenderer)tempPadRenderer;
          if (throughHolePad)
          {
            padRenderers.add(padRenderer);
          }
          else
            break;  // if surface mount, it's on top or bottom, but only one place, so we can stop as soon as find one
        }
      }
      // since pads are not always shown, this renderer may not be created.  In other words, it's ok for this to be null
      if ((throughHolePad == false) && (padRenderer != null))
      {
        padRenderers.add(padRenderer);
      }
    }
    return padRenderers;
  }

  /**
   * @author Andy Mechtenberg
   */
  private java.util.List<FiducialRenderer> findFiducialRenderersForFiducials(Collection<Fiducial> fiducials)
  {
    Assert.expect(fiducials != null);
    java.util.List<FiducialRenderer> fiducialRenderers = new ArrayList<FiducialRenderer>();
    for (Fiducial fiducial : fiducials)
    {
      boolean onTop = fiducial.isTopSide();
      FiducialRenderer fiducialRenderer = null;
      java.util.List<com.axi.guiUtil.Renderer> renderers = null;

      if (onTop)
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getTopBoardFiducialLayers()));
      else
        renderers = new ArrayList<com.axi.guiUtil.Renderer>(_graphicsEngine.getRenderers(getBottomBoardFiducialLayers()));

      for (com.axi.guiUtil.Renderer tempFiducialRenderer : renderers)
      {
        com.axi.v810.business.panelDesc.Fiducial tempFiducial = ((FiducialRenderer)tempFiducialRenderer).getFiducial();
        if (tempFiducial == fiducial)
        {
          fiducialRenderer = (FiducialRenderer)tempFiducialRenderer;
          break;
        }
      }

      if (fiducialRenderer != null)
      {
        fiducialRenderers.add(fiducialRenderer);
      }
    }
    return fiducialRenderers;
  }

  /**
   * update the board data from the datastore
   * @author chin-seong.kee
   */
  private void updateBoardData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getSource() instanceof Board);
    Assert.expect(event.getProjectChangeEventEnum() instanceof BoardEventEnum);

    BoardEventEnum eventEnum = (BoardEventEnum)event.getProjectChangeEventEnum();
    Board board = (Board)event.getSource();

    if (eventEnum.equals(eventEnum.COORDINATE) ||
        eventEnum.equals(eventEnum.DEGREES_ROTATION))
    {
      _graphicsEngine.validateData(getAllLayers(board));
    }
    else if (eventEnum.equals(eventEnum.LEFT_TO_RIGHT_FLIP) ||
             eventEnum.equals(eventEnum.TOP_TO_BOTTOM_FLIP))
    {
      // the selections must be clear before swapping layers or it loses track of
      // which layer needs to be on top
      _graphicsEngine.clearSelectedRenderers();
      flipBoard(board);
    }
    else if (eventEnum.equals(eventEnum.NAME))
    {
      // do nothing, as we don't render the name
    }
    else if (eventEnum.equals(eventEnum.CREATE_OR_DESTROY) || eventEnum.equals(BoardEventEnum.ADD_OR_REMOVE))
    {
      // have to add or remove a whole board from the engine
      if (event.isDestroyed() || event.isRemoved())
      {
        removeBoard((Board)event.getOldValue());
      }
      else
      {
        createBoard(board);
      }
    }
    if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0))
    {
      _graphicsEngine.fitGraphicsToScreen();
    }

  }
  
  /*
   * @author Kee Chin Seong
   */
  private void updateComponentTypeLoadedData(ProjectChangeEvent event, final Object argument)
  {
     ProjectChangeEventEnum eventEnum = ((ProjectChangeEvent)argument).getProjectChangeEventEnum();
     ComponentTypeSettingsEventEnum compTypeSettingsEventEnum = (ComponentTypeSettingsEventEnum) eventEnum;
     
     if(compTypeSettingsEventEnum.equals(ComponentTypeSettingsEventEnum.LOADED))
     {
         ComponentTypeSettings componentSettings = (ComponentTypeSettings)event.getSource();
         ComponentType componentType = componentSettings.getComponentType();
                
         if(componentSettings.isLoaded() == false)
         {
           List<ComponentRenderer> compRenderers = _componentTypeToComponentRenderersMap.get(componentType);
           for (ComponentRenderer compRend : compRenderers)
           {
             List<PadRenderer> padRenderers = _componentRendererToPadRenderersMap.get(compRend);
             if (padRenderers != null) // could be null if pad creation threshold is not met (not zoomed in enough)
             {
               _graphicsEngine.removeRenderers(padRenderers);
               //componentRendererToPadRenderersMapRemove(compRend);
             }
           }
           _componentTypeToComponentRenderersMap.remove(componentType);
           _graphicsEngine.removeRenderers(compRenderers);
           List<ReferenceDesignatorRenderer> compRefDesRenderers = _componentTypeToReferenceDesignatorRenderersMap.get(componentType);
           
           if(compRefDesRenderers != null)
            _graphicsEngine.removeRenderers(compRefDesRenderers);
           _componentTypeToReferenceDesignatorRenderersMap.remove(componentType);
            _componentRendererToPadRenderersMap.remove(compRenderers);
         }
         else
         {
            List<ComponentRenderer> compRenderers = new ArrayList<ComponentRenderer>();
            for (com.axi.v810.business.panelDesc.Component component : componentType.getComponents())
            {   
               if(component.getReferenceDesignator().equals(componentType.getReferenceDesignator()))
               {    
                 List<ReferenceDesignatorRenderer> compRefDesRenderers = new ArrayList<ReferenceDesignatorRenderer>();
                 List<PadRenderer> padRenderers = new ArrayList<PadRenderer>();
                 
                 ComponentRenderer compRenderer = new ComponentRenderer(this, component);
                 ReferenceDesignatorRenderer referenceDesignatorRenderer = new ReferenceDesignatorRenderer(component);
                 compRenderers.add(compRenderer);
                 compRefDesRenderers.add(referenceDesignatorRenderer);
                 SideBoard sideBoard = component.getSideBoard();
                 // do not create pad renderers unless the pad layers are currently visible on screen
                 
                 if (arePadsVisible(sideBoard.isTopSide()))
                 {
                    List<Pad> pads = component.getPads();
               
                    for (Pad pad : pads)
                    {
                      PadRenderer padRenderer = new PadRenderer(pad, compRenderer);
                      padRenderers.add(padRenderer);
                      Integer layerInt = (Integer)_boardToTopPadLayerMap.get(component.getBoard());
                      _graphicsEngine.addRenderer(layerInt.intValue(), padRenderer);
                      //_graphicsEngine.add(padRenderer);
                    }
                    //componentRendererToPadRenderersMapPut(compRenderer, padRenderers);
                 }
                 Integer layerInt = null;
                 if (sideBoard.isTopSide())
                 {
                   layerInt = (Integer)_boardToTopComponentLayerMap.get(sideBoard.getBoard());
                   _graphicsEngine.addRenderer(layerInt.intValue(), compRenderer);
                   layerInt = (Integer)_boardToTopRefDesLayerMap.get(sideBoard.getBoard());
                   _graphicsEngine.addRenderer(layerInt.intValue(), referenceDesignatorRenderer);
                   //_graphicsEngine.validateData(getTopPadLayers());
                 }
                 else if (sideBoard.isBottomSide())
                 {
                   layerInt = (Integer)_boardToBottomComponentLayerMap.get(sideBoard.getBoard());
                   _graphicsEngine.addRenderer(layerInt.intValue(), compRenderer);
                   layerInt = (Integer)_boardToBottomRefDesLayerMap.get(sideBoard.getBoard());
                   _graphicsEngine.addRenderer(layerInt.intValue(), referenceDesignatorRenderer);
                   //_graphicsEngine.validateData(getBottomPadLayers());
                 }
                 else
                   Assert.expect(false);
                 
                 _componentTypeToComponentRenderersMap.put(componentType, compRenderers);
                 _componentTypeToReferenceDesignatorRenderersMap.put(componentType, compRefDesRenderers);  
                 _componentRendererToPadRenderersMap.put(compRenderer, padRenderers);
                 
               }    
               //_graphicsEngine.validateData(getComponentLayers());
               //_graphicsEngine.validateData(getReferenceDesignatorLayers());     
               _graphicsEngine.validateData(getPadLayers());
               
            }
            
         }
      }
  }

  /**
   * update the board type data from the datastore
   * @author chin-seong.kee
   */
  private void updateBoardTypeData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getSource() instanceof BoardType);
    Assert.expect(event.getProjectChangeEventEnum() instanceof BoardTypeEventEnum);

    BoardTypeEventEnum eventEnum = (BoardTypeEventEnum)event.getProjectChangeEventEnum();
    BoardType boardType = (BoardType)event.getSource();
    Assert.expect(_boardTypeToBoardRenderersMap.containsKey(boardType), "board type " + boardType.getName() + " not in map");
    List<BoardRenderer> boardTypes = _boardTypeToBoardRenderersMap.get(boardType);
    for (BoardRenderer renderer : boardTypes)
    {
      renderer.validateData();
    }
//    boardType.ge

//    if (eventEnum.equals(eventEnum.COORDINATE)||
//       eventEnum.equals(eventEnum.DEGREES_ROTATION))
//    {
//      _graphicsEngine.validateData(getAllLayers(board));
//    }
//    else if (eventEnum.equals(eventEnum.FLIP))
//    {
//      flipBoard(board);
//    }
//    else if (eventEnum.equals(eventEnum.NAME))
//    {
//    }
    if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0))
    {
      _graphicsEngine.fitGraphicsToScreen();
    }


  }

  /**
   * @author chin-seong.kee
   * @author Laura Cormos
   */
  private void updateComponentData(ProjectChangeEvent event)
  {
  }

  /**
   * @author chin-seong.kee
   */
  private void updateComponentTypeData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getProjectChangeEventEnum() instanceof ComponentTypeEventEnum);

    ComponentTypeEventEnum compTypeEventEnum = (ComponentTypeEventEnum)event.getProjectChangeEventEnum();
    Object newValue = event.getNewValue();
    Object oldValue = event.getOldValue();

    if (compTypeEventEnum.equals(ComponentTypeEventEnum.CREATE_OR_DESTROY) 
            || compTypeEventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE)
            || compTypeEventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST))
    {
      if (event.isAdded()|| event.isCreated())
      {
        java.util.List<ComponentType> componentTypeList = new ArrayList<ComponentType>();
        if (compTypeEventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST) == false)
        {
          Assert.expect(newValue instanceof ComponentType);
          componentTypeList.add((ComponentType) newValue);
        }
        else
        {
          componentTypeList.addAll((java.util.List<ComponentType>) newValue);
        }

        List<com.axi.v810.business.panelDesc.Component> components = new ArrayList<com.axi.v810.business.panelDesc.Component>();

        for (ComponentType componentType : componentTypeList)
        {
          components.addAll(componentType.getComponents());

          List<ComponentRenderer> compRenderers = new ArrayList<ComponentRenderer>();
          List<ReferenceDesignatorRenderer> compRefDesRenderers = new ArrayList<ReferenceDesignatorRenderer>();
          for (com.axi.v810.business.panelDesc.Component component : components)
          {
            ComponentRenderer compRenderer = new ComponentRenderer(this, component);
            ReferenceDesignatorRenderer referenceDesignatorRenderer = new ReferenceDesignatorRenderer(component);
            compRenderers.add(compRenderer);
            compRefDesRenderers.add(referenceDesignatorRenderer);
            SideBoard sideBoard = component.getSideBoard();
                
            Integer padLayerInt = null;
            if (sideBoard.isTopSide())
              padLayerInt = (Integer) _boardToTopPadLayerMap.get(component.getBoard());
            else if (sideBoard.isBottomSide())
              padLayerInt = (Integer) _boardToBottomPadLayerMap.get(component.getBoard());
            else
              Assert.expect(false);

            if (_graphicsEngine.isZoomLevelBelowCreationThreshold(padLayerInt) == false);// && arePadsVisible(sideBoard.isTopSide()))
            {
              List<Pad> pads = component.getPads();
              List<PadRenderer> padRenderers = new ArrayList<PadRenderer>();
              for (Pad pad : pads)
              {
                PadRenderer padRenderer = new PadRenderer(pad, compRenderer);
                padRenderers.add(padRenderer);
                _graphicsEngine.addRenderer(padLayerInt.intValue(), padRenderer);
              }
              componentRendererToPadRenderersMapPut(compRenderer, padRenderers);
            }

            Integer layerInt = null;
            if (sideBoard.isTopSide())
            {
              layerInt = (Integer) _boardToTopComponentLayerMap.get(sideBoard.getBoard());
              _graphicsEngine.addRenderer(layerInt.intValue(), compRenderer);
              layerInt = (Integer) _boardToTopRefDesLayerMap.get(sideBoard.getBoard());
              _graphicsEngine.addRenderer(layerInt.intValue(), referenceDesignatorRenderer);
            }
            else if (sideBoard.isBottomSide())
            {
              layerInt = (Integer) _boardToBottomComponentLayerMap.get(sideBoard.getBoard());
              _graphicsEngine.addRenderer(layerInt.intValue(), compRenderer);
              layerInt = (Integer) _boardToBottomRefDesLayerMap.get(sideBoard.getBoard());
              _graphicsEngine.addRenderer(layerInt.intValue(), referenceDesignatorRenderer);
            }
            else
            {
              Assert.expect(false);
            }
          }
          components.clear();
          _componentTypeToComponentRenderersMap.put(componentType, compRenderers);
          _componentTypeToReferenceDesignatorRenderersMap.put(componentType, compRefDesRenderers);
        }
      }
      else if (event.isDestroyed() || event.isRemoved())
      {
        java.util.List<ComponentType> componentTypeList = new ArrayList<ComponentType>();
        if (compTypeEventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST) == false)
        {
          Assert.expect(oldValue instanceof ComponentType);
          componentTypeList.add((ComponentType) oldValue);
        }
        else
        {
          componentTypeList.addAll((java.util.List<ComponentType>) oldValue);
        }

        for (ComponentType componentTypeTemp : componentTypeList)
        {
          List<ComponentRenderer> compRendererList = _componentTypeToComponentRenderersMap.get(componentTypeTemp);
          List<ReferenceDesignatorRenderer> compRefDesRenderers = _componentTypeToReferenceDesignatorRenderersMap.get(componentTypeTemp);

          if (compRendererList != null)
          {
            for (ComponentRenderer compRend : compRendererList)
            {
              List<PadRenderer> padRenderers = _componentRendererToPadRenderersMap.get(compRend);
              if (padRenderers != null) // could be null if pad creation threshold is not met (not zoomed in enough)
              {
                for (PadRenderer padRenderer : padRenderers)
                {
                  if (_graphicsEngine.getLayerNumberForRenderer(padRenderer) != -1)
                    _graphicsEngine.removeRenderer(padRenderer);
                }
                padRenderers.clear();

                componentRendererToPadRenderersMapRemove(compRend);
              }
              if (_graphicsEngine.getLayerNumberForRenderer(compRend) != -1)
              {
                _graphicsEngine.removeRenderer(compRend);
                _graphicsEngine.removeRendererFromCurrentlySelectedRendererSet(compRend); 
              }
            }
          }
          
          if (compRefDesRenderers != null)
          {
            for (ReferenceDesignatorRenderer compRefDes : compRefDesRenderers)
            {
              if (_graphicsEngine.getLayerNumberForRenderer(compRefDes) != -1)
                _graphicsEngine.removeRenderer(compRefDes);
            }
          }
          _componentTypeToComponentRenderersMap.remove(componentTypeTemp);
          _componentTypeToReferenceDesignatorRenderersMap.remove(componentTypeTemp);
        }
      }
      else
      {
        Assert.expect(false, "Both oldValue and newValue for ComponentTypeEventEnum.CREATE_OR_DESTROY are null!!");
        return;
      }
      _graphicsEngine.validateData(getComponentLayers());
      _graphicsEngine.validateData(getReferenceDesignatorLayers());
      _graphicsEngine.validateData(getPadLayers());
    }
    else if (compTypeEventEnum.equals(ComponentTypeEventEnum.CHANGE_SIDE))
    {
      ComponentType componentType = (ComponentType)event.getSource();
      List<ComponentRenderer> compRenderers = _componentTypeToComponentRenderersMap.get(componentType);
      Assert.expect(compRenderers != null && compRenderers.size() > 0);
      List<ComponentRenderer> newCompRenderers = new ArrayList<ComponentRenderer>();
      // flip all component renderers from whatever layer they're on to the opposite layer (top/bottom)
      for (ComponentRenderer compRenderer : compRenderers)
      {
        Integer layerNumber = null;
        Component component = compRenderer.getComponent();

        List<PadRenderer> padRenderers = _componentRendererToPadRenderersMap.get(compRenderer);
        // the component's side board is where it was moved to, therefore we'll take the renderers from the bottom
        // layers and put them in the top layers
        if (component.getSideBoard().isTopSide())
        {
          _graphicsEngine.removeRenderer(compRenderer);
          layerNumber = _boardToTopComponentLayerMap.get(component.getBoard());
          Assert.expect(layerNumber != null);
          ComponentRenderer newCompRenderer = new ComponentRenderer(this, component);
          newCompRenderers.add(newCompRenderer);
          _graphicsEngine.addRenderer(layerNumber.intValue(), newCompRenderer);
          // change the pad Renderers layer as well, if visible on screen
          if (padRenderers != null)
          {
            List<PadRenderer> newPadRenderers = new ArrayList<PadRenderer>();
            _graphicsEngine.removeRenderers(padRenderers);
            layerNumber = _boardToTopPadLayerMap.get(component.getBoard());
            Assert.expect(layerNumber != null);
            for (PadRenderer padRenderer : padRenderers)
            {
              PadRenderer newPadRenderer = new PadRenderer(padRenderer.getPad(), newCompRenderer);
              newPadRenderers.add(newPadRenderer);
              _graphicsEngine.addRenderer(layerNumber.intValue(), newPadRenderer);
            }
            componentRendererToPadRenderersMapRemove(compRenderer);
            componentRendererToPadRenderersMapPut(newCompRenderer, newPadRenderers);
          }
        }
        else // the component's new side is not facing x-rays (is on the bottom) so we'll move the component's renderers
         // from the top layers and put it in the bottom layers
        {
          _graphicsEngine.removeRenderer(compRenderer);
          layerNumber = _boardToBottomComponentLayerMap.get(component.getBoard());
          Assert.expect(layerNumber != null);
          ComponentRenderer newCompRenderer = new ComponentRenderer(this, component);
          newCompRenderers.add(newCompRenderer);
          _graphicsEngine.addRenderer(layerNumber.intValue(), newCompRenderer);
          // change the pad Renderers layer as well, if visible on screen
          if (padRenderers != null)
          {
            List<PadRenderer> newPadRenderers = new ArrayList<PadRenderer>();
            _graphicsEngine.removeRenderers(padRenderers);
            layerNumber = _boardToBottomPadLayerMap.get(component.getBoard());
            Assert.expect(layerNumber != null);
            for (PadRenderer padRenderer : padRenderers)
            {
              PadRenderer newPadRenderer = new PadRenderer(padRenderer.getPad(), newCompRenderer);
              newPadRenderers.add(newPadRenderer);
              _graphicsEngine.addRenderer(layerNumber.intValue(), newPadRenderer);
            }
            componentRendererToPadRenderersMapRemove(compRenderer);
            componentRendererToPadRenderersMapPut(newCompRenderer, newPadRenderers);
          }
        }
      }
      _componentTypeToComponentRenderersMap.put(componentType, newCompRenderers);
      // flip all component reference designator renderers from whatever layer they're on to the opposite layer (top/bottom)
      List<ReferenceDesignatorRenderer> refDesRenderers = _componentTypeToReferenceDesignatorRenderersMap.get(componentType);
      List<ReferenceDesignatorRenderer> newRefDesRenderers = new ArrayList<ReferenceDesignatorRenderer>();
      for (ReferenceDesignatorRenderer refDesRenderer : refDesRenderers)
      {
        Integer layerNumber = null;
        Component component = refDesRenderer.getComponent();
        // the component's side board is where it was moved to, therefore we'll take the renderers from the bottom
        // layers and put it in the top layers
        if (component.getSideBoard().isTopSide())
        {
          _graphicsEngine.removeRenderer(refDesRenderer);
          layerNumber = _boardToTopRefDesLayerMap.get(component.getBoard());
          Assert.expect(layerNumber != null);
          ReferenceDesignatorRenderer newRefDesRend = new ReferenceDesignatorRenderer(component);
          newRefDesRenderers.add(newRefDesRend);
          _graphicsEngine.addRenderer(layerNumber.intValue(), newRefDesRend);
        }
        // the component's new side is not facing x-rays (is on the bottom) so we'll move the component's renderers
        // from the top layers and put it in the bottom layers
        else
        {
          _graphicsEngine.removeRenderer(refDesRenderer);
          layerNumber = _boardToBottomRefDesLayerMap.get(component.getBoard());
          Assert.expect(layerNumber != null);
          ReferenceDesignatorRenderer newRefDesRend = new ReferenceDesignatorRenderer(component);
          newRefDesRenderers.add(newRefDesRend);
          _graphicsEngine.addRenderer(layerNumber.intValue(), newRefDesRend);
        }
      }
      _componentTypeToReferenceDesignatorRenderersMap.put(componentType, newRefDesRenderers);

      _graphicsEngine.validateData(getComponentLayers());
      _graphicsEngine.validateData(getReferenceDesignatorLayers());
      _graphicsEngine.validateData(getPadLayers());
      if (newCompRenderers.isEmpty() == false)
      {
        com.axi.guiUtil.Renderer firstComponent = newCompRenderers.get(0);
        _graphicsEngine.selectWithCrossHairs(firstComponent);
      }
      _controlToolBar.showBothSides();
    }
    else // for any other component type event, just repaint the component, its refDes and its pads, if visible
    {
      ComponentType componentType = (ComponentType)event.getSource();
      
      if (componentType != null && componentType.isLoaded() == true)
      {
        Assert.expect(_componentTypeToComponentRenderersMap.containsKey(componentType));
        Assert.expect(_componentTypeToReferenceDesignatorRenderersMap.containsKey(componentType));

        List<ComponentRenderer> compRenderers = _componentTypeToComponentRenderersMap.get(componentType);
        for (ComponentRenderer compRenderer : compRenderers)
        {
          List<PadRenderer> padRenderers = _componentRendererToPadRenderersMap.get(compRenderer);
          // re-paint the component's pads only if created at this time
          if (padRenderers != null)
            for (PadRenderer padRenderer : padRenderers)
            {
              padRenderer.validateData();
            }
          for (ReferenceDesignatorRenderer refDesRend : _componentTypeToReferenceDesignatorRenderersMap.get(componentType))
          {
            if (refDesRend.getComponent().equals(compRenderer.getComponent()))
            {
              // re-paint the reference designator
              refDesRend.validateData();
              break; // move on once the refDesRend for current component was found
            }
          }
          // re-paint the component
          compRenderer.validateData();
        }
        if (compTypeEventEnum.equals(ComponentTypeEventEnum.REFERENCE_DESIGNATOR))
          setStatusBarDescription(compRenderers.get(0));
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void updateFiducialData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getSource() instanceof Fiducial);
    Assert.expect(event.getProjectChangeEventEnum() instanceof FiducialEventEnum);

    FiducialEventEnum fiducialEventEnum = (FiducialEventEnum)event.getProjectChangeEventEnum();
    Object newValue = event.getNewValue();
    Object oldValue = event.getOldValue();

    if (fiducialEventEnum.equals(FiducialEventEnum.CREATE_OR_DESTROY) || fiducialEventEnum.equals(FiducialEventEnum.ADD_OR_REMOVE))
    {
      if (event.isAdded()|| event.isCreated())
      {
        Assert.expect(newValue instanceof Fiducial);
//        if (UnitTest.unitTesting() == false)
//          System.out.println("Creating renderer for new panel fiducial");
        Fiducial fiducial = (Fiducial)newValue;
        Assert.expect(fiducial.isOnPanel());
        // add the fiducial renderer on the panel side where it was created
        if (fiducial.isTopSide())
        {
          _graphicsEngine.addRenderer(_topPanelFiducialLayer, new FiducialRenderer(fiducial));
          _graphicsEngine.addRenderer(_topPanelFiducialRefDesLayer, new FiducialReferenceDesignatorRenderer(fiducial));
        }
        else
        {
          _graphicsEngine.addRenderer(_bottomPanelFiducialLayer, new FiducialRenderer(fiducial));
          _graphicsEngine.addRenderer(_bottomPanelFiducialRefDesLayer,
                                      new FiducialReferenceDesignatorRenderer(fiducial));
        }
        _graphicsEngine.validateData(getFiducialLayers());
        _graphicsEngine.validateData(getReferenceDesignatorLayers());
      }
      else if (event.isDestroyed() || event.isRemoved())
      {
        Assert.expect(oldValue instanceof Fiducial);
//        if (UnitTest.unitTesting() == false)
//          System.out.println("Removing renderer for destroyed panel fiducial");
        Fiducial fiducial = (Fiducial)oldValue;
        Assert.expect(fiducial.isOnPanel());
        if (isTopSideVisible())
        {
          for (com.axi.guiUtil.Renderer fidRenderer : _graphicsEngine.getRenderers(_topPanelFiducialLayer))
          {
            if (((FiducialRenderer)fidRenderer).getFiducial() == fiducial)
              _graphicsEngine.removeRenderer(fidRenderer);
          }
          for (com.axi.guiUtil.Renderer fidRefDesRenderer :
               _graphicsEngine.getRenderers(_topPanelFiducialRefDesLayer))
          {
            if (((FiducialReferenceDesignatorRenderer)fidRefDesRenderer).getFiducial() == fiducial)
              _graphicsEngine.removeRenderer(fidRefDesRenderer);
          }
        }
        else
        {
          for (com.axi.guiUtil.Renderer fidRenderer : _graphicsEngine.getRenderers(_bottomPanelFiducialLayer))
          {
            if (((FiducialRenderer)fidRenderer).getFiducial() == fiducial)
              _graphicsEngine.removeRenderer(fidRenderer);
          }
          for (com.axi.guiUtil.Renderer fidRefDesRenderer :
               _graphicsEngine.getRenderers(_bottomPanelFiducialRefDesLayer))
          {
            if (((FiducialReferenceDesignatorRenderer)fidRefDesRenderer).getFiducial() == fiducial)
              _graphicsEngine.removeRenderer(fidRefDesRenderer);
          }
        }
        _createNewCadPanel.setDescription("");
      }
      else
      {
        Assert.expect(false, "Both newValue and oldValue are null for FiducialEventEnum.CREATE_OR_DESTROY event.");
        return;
      }
    }
    else
    {
      _graphicsEngine.validateData(getPanelFiducialLayers());
      _graphicsEngine.validateData(getPanelFiducialRefDesLayers());
    }
  }

  /**
   * @author Laura Cormos
   */
  private void updateFiducialTypeData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getSource() instanceof FiducialType);
    Assert.expect(event.getProjectChangeEventEnum() instanceof FiducialTypeEventEnum);

    FiducialTypeEventEnum fiducialTypeEventEnum = (FiducialTypeEventEnum)event.getProjectChangeEventEnum();
    Object newValue = event.getNewValue();
    Object oldValue = event.getOldValue();

    if (fiducialTypeEventEnum.equals(FiducialTypeEventEnum.CREATE_OR_DESTROY) || fiducialTypeEventEnum.equals(FiducialTypeEventEnum.ADD_OR_REMOVE))
    {
      if (event.isAdded()|| event.isCreated())
      {
        Assert.expect(newValue instanceof FiducialType);
//        if (UnitTest.unitTesting() == false)
//          System.out.println("Creating renderers for new fiducial type");
        FiducialType fiducialType = (FiducialType)newValue;
        List<Fiducial> fiducials = fiducialType.getFiducials();
        for (Fiducial fiducial : fiducials)
        {
          // this event can only be generated by a board fiducial
          Assert.expect(fiducial.isOnBoard());
          SideBoard sideBoard = fiducial.getSideBoard();
          Integer layerInt = null;
          if (sideBoard.isTopSide())
          {
            layerInt = (Integer)_boardToTopFiducialLayerMap.get(sideBoard.getBoard());
            _graphicsEngine.addRenderer(layerInt.intValue(), new FiducialRenderer(fiducial));
            layerInt = (Integer)_boardToTopRefDesLayerMap.get(sideBoard.getBoard());
            _graphicsEngine.addRenderer(layerInt.intValue(), new FiducialReferenceDesignatorRenderer(fiducial));
          }
          else if (sideBoard.isBottomSide())
          {
            layerInt = (Integer)_boardToBottomFiducialLayerMap.get(sideBoard.getBoard());
            _graphicsEngine.addRenderer(layerInt.intValue(), new FiducialRenderer(fiducial));
            layerInt = (Integer)_boardToBottomRefDesLayerMap.get(sideBoard.getBoard());
            _graphicsEngine.addRenderer(layerInt.intValue(), new FiducialReferenceDesignatorRenderer(fiducial));
          }
        }
      }
      else if (event.isDestroyed() || event.isRemoved())
      {
        Assert.expect(oldValue instanceof FiducialType);
//        if (UnitTest.unitTesting() == false)
//          System.out.println("Removing renderers for destroyed fiducial type");
        FiducialType fiducialType = (FiducialType)oldValue;
        List<Fiducial> fiducials = fiducialType.getFiducials();
        for (Fiducial fiducial : fiducials)
        {
          // this event can only be generate by a board fiducial
          Assert.expect(fiducial.isOnBoard());
          SideBoard sideBoard = fiducial.getSideBoard();
          Integer layerInt = null;
          if (sideBoard.isTopSide())
          {
            layerInt = (Integer)_boardToTopFiducialLayerMap.get(sideBoard.getBoard());
            for (com.axi.guiUtil.Renderer fidRenderer : _graphicsEngine.getRenderers(layerInt))
            {
              if (((FiducialRenderer)fidRenderer).getFiducial() == fiducial)
                _graphicsEngine.removeRenderer(fidRenderer);
            }
            layerInt = (Integer)_boardToTopRefDesLayerMap.get(sideBoard.getBoard());
            for (com.axi.guiUtil.Renderer fidRefDesRenderer : _graphicsEngine.getRenderers(layerInt))
            {
              if (fidRefDesRenderer instanceof FiducialReferenceDesignatorRenderer &&
                  ((FiducialReferenceDesignatorRenderer)fidRefDesRenderer).getFiducial() == fiducial)
                _graphicsEngine.removeRenderer(fidRefDesRenderer);
            }
          }
          else if (sideBoard.isBottomSide())
          {
            layerInt = (Integer)_boardToBottomFiducialLayerMap.get(sideBoard.getBoard());
            for (com.axi.guiUtil.Renderer fidRenderer : _graphicsEngine.getRenderers(layerInt))
            {
              if (((FiducialRenderer)fidRenderer).getFiducial() == fiducial)
                _graphicsEngine.removeRenderer(fidRenderer);
            }
            layerInt = (Integer)_boardToBottomRefDesLayerMap.get(sideBoard.getBoard());
            for (com.axi.guiUtil.Renderer fidRefDesRenderer : _graphicsEngine.getRenderers(layerInt))
            {
              if (fidRefDesRenderer instanceof FiducialReferenceDesignatorRenderer &&
                  ((FiducialReferenceDesignatorRenderer)fidRefDesRenderer).getFiducial() == fiducial)
                _graphicsEngine.removeRenderer(fidRefDesRenderer);
            }
          }
        }
      }
      else
      {
        Assert.expect(false,
                      "Both oldValue and newValue for FiducialTypeEventEnum.CREATE_OR_DESTROY are null!!");
        return;
      }
    }
    _graphicsEngine.validateData(getFiducialLayers());
    _graphicsEngine.validateData(getReferenceDesignatorLayers());
    if (fiducialTypeEventEnum.equals(FiducialTypeEventEnum.NAME))
    {
      FiducialType fiducialType = (FiducialType)event.getSource();
      setStatusBarDescription(findFiducialRenderers(fiducialType).get(0));
    }
  }

  /**
   * @author Laura Cormos
   */
  private void updateLandPatternPadData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    if(event.getSource() instanceof LandPatternPad)
    {
      Assert.expect(event.getProjectChangeEventEnum() instanceof LandPatternPadEventEnum);

      Object oldValue = event.getOldValue();
      Object newValue = event.getNewValue();

      LandPatternPadEventEnum landPaternPadEventEnum = (LandPatternPadEventEnum)event.getProjectChangeEventEnum();
      LandPatternPad landPatternPad = null;
  //    System.out.println("Servicing a LPP event, GE contents:");
  //    printRenderers();
  //    System.out.println("Servicing a LPP event, variables contents:");
  //    printCTtoCRMap();
  //    printCRtoPRMap();
      // revalidate the component's shape if any land pattern pad is being created or destroyed
      if (landPaternPadEventEnum.equals(LandPatternPadEventEnum.CREATE_OR_DESTROY) ||
          landPaternPadEventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE))
      {
        if (event.isDestroyed() || event.isRemoved())
        {
          Assert.expect(oldValue instanceof LandPatternPad);
          landPatternPad = (LandPatternPad)oldValue;
  //        System.out.println("Event is LPP destroyed/removed");
        }
        else if (event.isAdded()|| event.isCreated())
        {
          Assert.expect(newValue instanceof LandPatternPad);
          landPatternPad = (LandPatternPad)newValue;
  //        System.out.println("Event is LPP created/added");
        }
        updateSelectedLandPatternPadData(landPatternPad, event);
      }
      // for any other changes to a land pattern pad just refresh the graphics
      else
      {
  //      System.out.println("Event is " + landPaternPadEventEnum.getId());
        landPatternPad = (LandPatternPad)event.getSource();
        java.util.List<ComponentType> compTypes = landPatternPad.getLandPattern().getComponentTypesUnsorted();
        for (ComponentType compType : compTypes)
        {
          List<ComponentRenderer> compRenderers = _componentTypeToComponentRenderersMap.get(compType);
          if(compRenderers != null && compRenderers.isEmpty() == false)
          {
            for (ComponentRenderer compRend : compRenderers)
            {
              compRend.validateData();
              List<PadRenderer> padRenderers = _componentRendererToPadRenderersMap.get(compRend);
              if (padRenderers != null)
              {
                for (PadRenderer padRend : padRenderers)
                {
                  if (padRend.getPad().getLandPatternPad().equals(landPatternPad))
                    padRend.validateData();
                }
              }
            }
          }
        }
      }
    }
    else if(event.getSource() instanceof ArrayList )
    {
      Assert.expect(event.getProjectChangeEventEnum() instanceof LandPatternPadEventEnum);

      LandPatternPadEventEnum landPaternPadEventEnum = (LandPatternPadEventEnum)event.getProjectChangeEventEnum();

      if(landPaternPadEventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE_LIST))
      {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        java.util.List<LandPatternPad> landPatternPads = null;

        if (event.isDestroyed() || event.isRemoved())
        {
          Assert.expect(oldValue instanceof ArrayList);
          landPatternPads = (java.util.ArrayList<LandPatternPad>)oldValue;
  //        System.out.println("Event is LPP destroyed/removed");
        }
        else if (event.isAdded()|| event.isCreated())
        {
          Assert.expect(newValue instanceof ArrayList);
          landPatternPads = (java.util.ArrayList<LandPatternPad>) newValue;
  //        System.out.println("Event is LPP created/added");
        }
        else
        {
          return;
        }
        for(LandPatternPad landPatternPad : landPatternPads)
        {
          updateSelectedLandPatternPadData(landPatternPad, event);
        }         
      }
    }
//    System.out.println("Finished Servicing a LPP event, GE contents:");
//    printRenderers();
//    System.out.println("Finished Servicing a LPP event, variables contents:");
//    printCTtoCRMap();
//    printCRtoPRMap();
  }

  private void updateSelectedLandPatternPadData(LandPatternPad landPatternPad, ProjectChangeEvent event)
  {
    java.util.List<ComponentType> compTypes = landPatternPad.getLandPattern().getComponentTypesUnsorted();
    for (ComponentType compType : compTypes)
    {
      List<ComponentRenderer> compRenderers = _componentTypeToComponentRenderersMap.get(compType);
      if (compRenderers != null)
      {
        for (ComponentRenderer compRend : compRenderers)
        {
          if (event.isCreated() || event.isAdded())
          {
            PadRenderer padRend = null;
            Component component = compRend.getComponent();
            List<Pad> pads = component.getPads();
            Board board = component.getBoard();
            Pad thePad = null;
            for (Pad pad : pads)
            {
              if (pad.getLandPatternPad() == landPatternPad)
              {
                thePad = pad;
                break;
              }
            }
            Assert.expect(thePad != null);
            padRend = new PadRenderer(thePad, compRend);
            boolean padRendExists = false;
            List<PadRenderer> newPadRenderersList = _componentRendererToPadRenderersMap.get(compRend);
            // add the new pad renderer only if is not already present (don't want duplicates)
            if (newPadRenderersList != null)
            {
              for (PadRenderer padRenderer : newPadRenderersList)
              {
                if (padRenderer.getPad() == thePad)
                {
                  padRendExists = true;
                  break;
                }
              }
              if (padRendExists == false)
              {
                newPadRenderersList.add(padRend);
              }
              if (component.getSideBoard().isTopSide())
              {
                //                System.out.println("Adding pad renderer for pad: " + padRend.getPad().getName() +
                //                                   " of LP: " + padRend.getPad().getLandPatternPad().getLandPattern().getName());
                _graphicsEngine.addRenderer(getTopPadLayer(board), padRend);
              }
              else
              {
                //                System.out.println("Adding pad renderer for pad: " + padRend.getPad().getName() +
                //                                   " of LP: " + padRend.getPad().getLandPatternPad().getLandPattern().getName());
                _graphicsEngine.addRenderer(getBottomPadLayer(board), padRend);
              }
              if (landPatternPad instanceof ThroughHoleLandPatternPad)
              {
                PadRenderer theOtherPadRend = new PadRenderer(thePad, compRend);
                if (padRendExists == false)
                {
                  newPadRenderersList.add(theOtherPadRend);
                }
                if (component.getSideBoard().isTopSide())
                {
                  //                  System.out.println("Adding pad renderer for pad: " + theOtherPadRend.getPad().getName() +
                  //                                     " of LP: " + theOtherPadRend.getPad().getLandPatternPad().getLandPattern().getName());
                  _graphicsEngine.addRenderer(getBottomPadLayer(board), theOtherPadRend);
                }
                else
                {
                  //                  System.out.println("Adding pad renderer for pad: " + theOtherPadRend.getPad().getName() +
                  //                                     " of LP: " + theOtherPadRend.getPad().getLandPatternPad().getLandPattern().getName());
                  _graphicsEngine.addRenderer(getTopPadLayer(board), theOtherPadRend);
                }
                _graphicsEngine.validateData(getBottomPadLayer(board));
                _graphicsEngine.validateData(getTopPadLayer(board));
              }
            }
            componentRendererToPadRenderersMapPut(compRend, newPadRenderersList);
          }
          else if (event.isDestroyed() || event.isRemoved())
          {
            List<PadRenderer> padRenderers = _componentRendererToPadRenderersMap.get(compRend);
            List<PadRenderer> removedPadRenderers = new ArrayList<PadRenderer>();
            if (padRenderers != null)
            {
              for (PadRenderer padRend : padRenderers)
              {
                if (padRend.getPad().getLandPatternPad().equals(landPatternPad))
                {
                  //                  System.out.println("Removing pad renderer for pad: " + padRend.getPad().getName() +
                  //                                     " of LP: " + padRend.getPad().getLandPatternPad().getLandPattern().getName());
                  _graphicsEngine.removeRenderer(padRend);
                  removedPadRenderers.add(padRend);
                }
              }
              padRenderers.removeAll(removedPadRenderers);
              // replace the list of pad renderers for this component renderer after removing the deleted ones.
              _componentRendererToPadRenderersMap.put(compRend, padRenderers);
            }
          }
          else
          {
            Assert.expect(false);
          }
          compRend.validateData();
        }
        List<ReferenceDesignatorRenderer> compRefDesRenderers = _componentTypeToReferenceDesignatorRenderersMap.get(compType);
        for (ReferenceDesignatorRenderer compRefDesRend : compRefDesRenderers)
        {
          compRefDesRend.validateData();
        }
      }
    }
  }
  
  /**
   * @author Laura Cormos
   * @Edited by Kee Chin Seong
   */
  private void updateThroughHoleLandPatternPadData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getSource() instanceof ThroughHoleLandPatternPad);
    Assert.expect(event.getProjectChangeEventEnum() instanceof ThroughHoleLandPatternPadEventEnum);

    ThroughHoleLandPatternPadEventEnum throughHoleLandPaternPadEventEnum = (ThroughHoleLandPatternPadEventEnum)event.getProjectChangeEventEnum();
    // refresh the hole graphics
    if (throughHoleLandPaternPadEventEnum.equals(ThroughHoleLandPatternPadEventEnum.HOLE_DIAMETER))
    {
      ThroughHoleLandPatternPad throughHoleLandPatternPad = (ThroughHoleLandPatternPad)event.getSource();
      java.util.List<ComponentType> compTypes = throughHoleLandPatternPad.getLandPattern().getComponentTypesUnsorted();
      for (ComponentType compType : compTypes)
      {
        List<ComponentRenderer> compRenderers = _componentTypeToComponentRenderersMap.get(compType);
        if(compRenderers != null)
        {   
            for (ComponentRenderer compRend : compRenderers)
            {
              List<PadRenderer> padRenderers = _componentRendererToPadRenderersMap.get(compRend);
              if (padRenderers != null)
              {
                for (PadRenderer padRend : padRenderers)
                {
                  if (padRend.getPad().getLandPatternPad().equals(throughHoleLandPatternPad))
                    padRend.validateData();
                }
              }
            }
        }
      }
    }
  }

  /**
   * @author chin-seong.kee
   * @author Laura Cormos
   */
  private void updatePanelData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getProjectChangeEventEnum() instanceof PanelEventEnum);
    PanelEventEnum eventEnum = (PanelEventEnum)event.getProjectChangeEventEnum();

    _graphicsEngine.validateData(getAllLayers());
    if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0))
    {
      _graphicsEngine.fitGraphicsToScreen();
    }

  }

  /**
   * @author chin-seong.kee
   */
  private void updatePanelSettingsData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getProjectChangeEventEnum() instanceof PanelSettingsEventEnum);

    PanelSettingsEventEnum panelSettingsEventEnum = (PanelSettingsEventEnum)event.getProjectChangeEventEnum();
    if ((panelSettingsEventEnum.equals(PanelSettingsEventEnum.LEFT_TO_RIGHT_FLIP)) ||
        (panelSettingsEventEnum.equals(PanelSettingsEventEnum.TOP_TO_BOTTOM_FLIP)))
    {
      // the selections must be clear before swapping layers or it loses track of
      // which layer needs to be on top
      _graphicsEngine.clearSelectedRenderers();
      flipPanel();
      if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0))
      {
        _graphicsEngine.fitGraphicsToScreen();
      }
    }
    else if (panelSettingsEventEnum.equals(PanelSettingsEventEnum.DEGREES_ROTATION))
    {
      _graphicsEngine.validateData(getAllLayers());
      if (MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0))
      {
        _graphicsEngine.fitGraphicsToScreen();
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateProjectData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getProjectChangeEventEnum() instanceof ProjectEventEnum);

    ProjectEventEnum projectEventEnum = (ProjectEventEnum)event.getProjectChangeEventEnum();
    if (projectEventEnum.equals(projectEventEnum.DISPLAY_UNITS))
    {
      MathUtilEnum displayUnits = _panel.getProject().getDisplayUnits();
      double measureFactor = 1.0/MathUtil.convertUnits(1.0, displayUnits, MathUtilEnum.NANOMETERS);
      _graphicsEngine.setMeasurementInfo(measureFactor, MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits),
                                         MeasurementUnits.getMeasurementUnitString(displayUnits));
    }
  }

  /**
   * @author Laura Cormos
   */
  private void updateSideBoardTypeData(ProjectChangeEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(event.getProjectChangeEventEnum() instanceof SideBoardTypeEventEnum);

    SideBoardTypeEventEnum sideBoardTypeEventEnum = (SideBoardTypeEventEnum)event.getProjectChangeEventEnum();
    // either newValue or oldValue can be null, depending on the type of event generated
  }

  /**
   * @author chin-seong.kee
   * @Edited by Kee Chin Seong
   */
  private void handleGuiEvent(GuiEvent guiEvent)
  {
    Assert.expect(guiEvent != null);

    GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();

    if (guiEventEnum instanceof SelectionEventEnum)
    {
      _graphicsEngine.clearSelectedRenderers();
      SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
      if (selectionEventEnum.equals(SelectionEventEnum.CLEAR_SELECTIONS))
      {
        _graphicsEngine.clearSelectedRenderers();
      }
      else if (selectionEventEnum.equals(SelectionEventEnum.BOARD_INSTANCE))
      {
        /** Warning "unchecked cast" approved for cast from Object types.*/
        //Panel Settings
        List<Board> selectedBoards = (List<Board>)guiEvent.getSource();
        // if a single board is selected, remember it
        if (selectedBoards.size() == 1)
        {
          _selectedBoardName = selectedBoards.get(0).getName();
        }
        else
        {
          _selectedBoardName = null;
        }
        List<Integer> boardLayers = new ArrayList<Integer>();
        for (Board board : selectedBoards)
        {
          boardLayers.addAll(getAllLayers(board));
        }

        Collection<com.axi.guiUtil.Renderer> renderers = _graphicsEngine.getRenderers(boardLayers);
        _graphicsEngine.setSelectedRenderers(renderers);
      }
      else if (selectionEventEnum.equals(SelectionEventEnum.COMPONENT_TYPE))
      {
        /** Warning "unchecked cast" approved for cast from Object types.*/
        // modify subtype.
        List<ComponentType> components = (List<ComponentType>)guiEvent.getSource();
        if (components.isEmpty())
          return;
        else if(components.get(0).isLoaded() == false)
          return;
        
        if(isTopSideVisible() && _showTopComponentLayer == false)
          showTopSide();
        else if(isBottomSideVisible() && _showBottomComponentLayer == false)
          showBottomSide();
                
        if (components.size() == 1)
        {
          ComponentType componentType = (ComponentType)components.get(0);
          if(componentType != null)
          {
            highlightComponentType(componentType); // highlight a single component different from multiple components
            
            if(getComponentRenderers(componentType) != null)
              setStatusBarDescription(getComponentRenderers(componentType).get(0));
          }
        }
        else
          highlightMultipleComponents(components);

      }
      else if (selectionEventEnum.equals(SelectionEventEnum.FIDUCIAL_TYPE))
      {
        /** Warning "unchecked cast" approved for cast from Object types.*/
        List<FiducialType> fiducials = (List<FiducialType>)guiEvent.getSource();
        if (fiducials.isEmpty())
          return;
        else if (fiducials.size() == 1)
        {
          FiducialType fiducialType = (FiducialType)fiducials.get(0);
          highlightFiducialType(fiducialType); // highlight a single fiducial different from multiple fiducials
          FiducialRenderer fiducialRenderer = getFiducialRenderers(fiducialType).get(0);
          setStatusBarDescription(fiducialRenderer);
        }
        else
          highlightMultipleFiducials(fiducials);

      }
      else if (selectionEventEnum.equals(SelectionEventEnum.PAD_TYPE))
      {
        /** Warning "unchecked cast" approved for cast from Object types.*/
        /**Changes :: No load : pad Type must be loaded !*/
        List<PadType> padTypes = (List<PadType>)guiEvent.getSource();
        if (padTypes.isEmpty())
          return;
        
        if(findPadRenderersForPadTypes(padTypes).size() <= 0)
          return;
        
        if(padTypes.get(0).isInspected() == false)
          return;
        selectPadTypes(padTypes);
        List<Pad> pads = padTypes.get(0).getPads();
        if (pads.isEmpty())
          return;
        Pad pad = pads.get(0);
        PadRenderer padRenderer = getPadRenderer(pad);
        if (padRenderer != null)
          setStatusBarDescription(padRenderer);
      }
      else if (selectionEventEnum.equals(SelectionEventEnum.PAD_OR_FIDUCIAL))
      {
        /** Warning "unchecked cast" approved for cast from Object types.*/
        List<Object> padOrFiducials = (List<Object>)guiEvent.getSource();
        selectPadsAndFiducials(padOrFiducials);
      }

    }
    if (guiEventEnum instanceof HighlightEventEnum)
    {
 
    }
    if (guiEventEnum instanceof GraphicsControlEnum)
    {
      GraphicsControlEnum graphicsControlEnum = (GraphicsControlEnum)guiEventEnum;
      if (graphicsControlEnum.equals(GraphicsControlEnum.ORIGIN_VISIBLE))
      {
        _graphicsEngine.setVisible(getBoardOriginLayers(), true);
      }
      else if (graphicsControlEnum.equals(GraphicsControlEnum.ORIGIN_INVISIBLE))
      {
        _graphicsEngine.setVisible(getBoardOriginLayers(), false);
      }
      else if (graphicsControlEnum.equals(GraphicsControlEnum.BOARD_NAME_VISIBLE))
      {
        _graphicsEngine.setVisible(getBoardNameLayers(), true);
      }
      else if (graphicsControlEnum.equals(GraphicsControlEnum.BOARD_NAME_INVISIBLE))
      {
        _graphicsEngine.setVisible(getBoardNameLayers(), false);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  PadRenderer getPadRenderer(Pad pad)
  {
    Assert.expect(pad != null);
    PadRenderer padRenderer = _padToPadRendererMap.get(pad);
    if (padRenderer == null)
    {
      java.util.List<ComponentRenderer> componentRenderers = _componentTypeToComponentRenderersMap.get(pad.getComponent().getComponentType());
      Assert.expect(componentRenderers.isEmpty() == false);
      ComponentRenderer componentRenderer = componentRenderers.get(0);
      padRenderer = new PadRenderer(pad, componentRenderer);
    }
    return padRenderer;
  }

  /**
   * @author Andy Mechtenberg
   * @Edited by Kee Chin Seong
   */
  List<ComponentRenderer> getComponentRenderers(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    List<ComponentRenderer> renderers = _componentTypeToComponentRenderersMap.get(componentType);
    //Assert.expect(renderers != null);
    //Assert.expect(renderers.isEmpty() == false);
    return renderers;
  }

  /**
   * @author Laura Cormos
   */
  List<FiducialRenderer> getFiducialRenderers(FiducialType fiducialType)
  {
    Assert.expect(fiducialType != null);
    List<FiducialRenderer> renderers = new ArrayList<FiducialRenderer>();

    if (fiducialType.getFiducials().get(0).isOnPanel())
    {
      for (com.axi.guiUtil.Renderer renderer : _graphicsEngine.getRenderers(_topPanelFiducialLayer))
      {
        Assert.expect(renderer instanceof FiducialRenderer);
        if (((FiducialRenderer)renderer).getFiducial().getFiducialType() == fiducialType)
          renderers.add((FiducialRenderer)renderer);
      }
      for (com.axi.guiUtil.Renderer renderer : _graphicsEngine.getRenderers(_bottomPanelFiducialLayer))
      {
        Assert.expect(renderer instanceof FiducialRenderer);
        if (((FiducialRenderer)renderer).getFiducial().getFiducialType() == fiducialType)
          renderers.add((FiducialRenderer)renderer);
      }
    }
    else
    {
      for (com.axi.guiUtil.Renderer renderer : _graphicsEngine.getRenderers(getTopBoardFiducialLayers()))
      {
        Assert.expect(renderer instanceof FiducialRenderer);
        if (((FiducialRenderer)renderer).getFiducial().getFiducialType() == fiducialType)
          renderers.add((FiducialRenderer)renderer);
      }
      for (com.axi.guiUtil.Renderer renderer : _graphicsEngine.getRenderers(getBottomBoardFiducialLayers()))
      {
        Assert.expect(renderer instanceof FiducialRenderer);
        if (((FiducialRenderer)renderer).getFiducial().getFiducialType() == fiducialType)
          renderers.add((FiducialRenderer)renderer);
      }
    }
    Assert.expect(renderers != null);
    Assert.expect(renderers.isEmpty() == false);
    return renderers;
  }

  /**
   * @author Laura Cormos
   */
  private void componentRendererToPadRenderersMapPut(ComponentRenderer componentRenderer,
                                                     List<PadRenderer> padRenderers)
  {
    _componentRendererToPadRenderersMap.put(componentRenderer, padRenderers);
  }

  /**
   * @author Laura Cormos
   */
  private void componentRendererToPadRenderersMapRemove(ComponentRenderer componentRenderer)
  {
    _componentRendererToPadRenderersMap.remove(componentRenderer);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTopPadLayerVisibility(boolean visible)
  {
    _showTopPadLayer = visible;
    if(visible == false)
    {
      _graphicsEngine.removeRenderers(getTopPadLayers());//, visible);
      //_componentRendererToPadRenderersMap.clear();
    }
    else
    {
      for(Integer layerInt : getTopPadLayers())
        updateRenderers(_graphicsEngine, layerInt.intValue());
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomPanelLayersVisible(boolean visible)
  {
    _showBottomPanelLayer = visible;
    _graphicsEngine.setVisible(getBottomPanelLayer(), visible);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTopPanelLayersVisible(boolean visible)
  {
    _showTopPanelLayer = visible;
    _graphicsEngine.setVisible(getTopPanelLayer(), visible);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isTopPanelLayersVisible()
  {
    return (_graphicsEngine.isLayerVisible(getTopPanelLayer())) ? (true & _showTopPanelLayer) : (false & _showTopPanelLayer);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isBottomPanelLayersVisible()
  {
    return (_graphicsEngine.isLayerVisible(getBottomPanelLayer())) ? (true & _showBottomPanelLayer) : (false & _showBottomPanelLayer);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTopComponentLayersVisible(boolean visible, Panel panel)
  {
    _showTopComponentLayer = visible;
    
    if(visible == false)
    {
        _graphicsEngine.removeRenderers(getTopComponentLayers());
        setTopPadLayerVisibility(false);
        setTopReferenceDesignatorLayerVisibility(false, panel);
        
        for(Board board : panel.getBoards())
          unpopulateComponentLayerWithRenderers(board.getTopSideBoard().getComponents());
    }
    else
    {
        for(Board board : panel.getBoards())
          populateComponentLayersWithRenderers(board, board.getTopSideBoard().getComponents(), true, false);    
        setTopPadLayerVisibility(true);
        setTopReferenceDesignatorLayerVisibility(true, panel);
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean isTopComponentLayersVisible()
  {
    List<Integer> topComponentLayers = getTopComponentLayers();
    for (Integer layerNum : topComponentLayers)
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true & _showTopComponentLayer;
    }
    return false & _showTopComponentLayer;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean isBottomComponentLayersVisible()
  {
    List<Integer> bottomComponentLayers = getBottomComponentLayers();
    for (Integer layerNum : bottomComponentLayers)
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true & _showBottomComponentLayer;
    }
    return false & _showBottomComponentLayer;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomComponentLayersVisible(boolean visible, Panel panel)
  {
    _showBottomComponentLayer = visible;
    
    if(visible == false)
    {
       _graphicsEngine.removeRenderers(getBottomComponentLayers());
       setBottomPadLayerVisibility(false);
       setBottomReferenceDesignatorLayerVisibility(false, panel);
       
       for(Board board : panel.getBoards())
          unpopulateComponentLayerWithRenderers(board.getBottomSideBoard().getComponents());
    }
    else
    {
       for(Board board : panel.getBoards())
          populateComponentLayersWithRenderers(board, board.getBottomSideBoard().getComponents(), false, false);
       
       setBottomPadLayerVisibility(false);
       setBottomReferenceDesignatorLayerVisibility(true, panel);
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTopReferenceDesignatorLayerVisibility(boolean visible, Panel panel)
  {
    _showTopComponentRefDesLayer = visible;
    if(visible == false)
      _graphicsEngine.removeRenderers(getTopReferenceDesignatorLayers());//, visible);
    else
    {
      for(Board board : panel.getBoards())
      {
        //Kee Chin Seong - need to check top or bottom side exist
        if(board.topSideBoardExists())
            populateReferenceDesignatorLayerWithRenderers(board, board.getTopSideBoard().getComponents(), true);
      }
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomReferenceDesignatorLayerVisibility(boolean visible, Panel panel)
  {
    _showBottomComponentRefDesLayer = visible;
    if(visible == false)
      _graphicsEngine.removeRenderers(getBottomReferenceDesignatorLayers());//, visible);
    else
    {
      for(Board board : panel.getBoards())
      {
        //Kee Chin Seong - need to check top or bottom side exist
        if(board.bottomSideBoardExists())
           populateReferenceDesignatorLayerWithRenderers(board, board.getBottomSideBoard().getComponents(), false);
      }
    }
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomPadLayerVisibility(boolean visible)
  {
    _showBottomPadLayer = visible;
    if(visible == false)
      _graphicsEngine.removeRenderers(getBottomPadLayers());//, visible);
    else
    {
      for(Integer layerInt : getBottomPadLayers())
        updateRenderers(_graphicsEngine, layerInt.intValue());
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isTopComponentRefDesLayersVisible()
  {
    List<Integer> topCompRefDesLayers = getTopReferenceDesignatorLayers();
    for (Integer layerNum : topCompRefDesLayers)
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true & _showTopComponentRefDesLayer;
    }
    return false & _showTopComponentRefDesLayer;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isBottomComponentRefDesLayersVisible()
  {
    List<Integer> bottomCompRefDesLayers = getBottomReferenceDesignatorLayers();
    for (Integer layerNum : bottomCompRefDesLayers)
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true & _showBottomComponentRefDesLayer;
    }
    return false & _showBottomComponentRefDesLayer;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean isTopPadLayersVisible()
  {
    List<Integer> topPadLayers = getTopPadLayers();
    for (Integer layerNum : topPadLayers)
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true & _showTopPadLayer;
    }
    return false  & _showTopPadLayer;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean isBottomPadLayersVisible()
  {
    List<Integer> bottomPadLayers = getBottomPadLayers();
    for (Integer layerNum : bottomPadLayers)
    {
      if (_graphicsEngine.isLayerVisible(layerNum.intValue()))
        return true & _showBottomPadLayer;
    }
    return false & _showBottomPadLayer;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTopBoardLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getTopBoardLayers(), visible);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setBottomBoardLayersVisible(boolean visible)
  {
    _graphicsEngine.setVisible(getBottomBoardLayers(), visible);
  }

  /**
   * @author chin-seong.kee
   */
  public List<ComponentRenderer> getSurfaceMapSelectedComponentRenderers(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    List<ComponentRenderer> renderers = _componentTypeToComponentRenderersMap.get(componentType);
    Assert.expect(renderers != null);
    Assert.expect(renderers.isEmpty() == false);
    return renderers;
  }
  
  /**
   * Use to draw shape that actual used for virtualLive generation
   *
   * @author chin-seong.kee
   */
  private void handleVirtualLiveImageGenerationDataObservable(final Object argument)
  {
    Assert.expect(argument != null);

    _graphicsEngine.clearCrossHairs();
    _graphicsEngine.removeRenderers(_virtualLiveAreaLayer);
    java.awt.Shape shape = (java.awt.Shape) argument;
    if(shape != null)
    {
      if(shape.getBounds2D().getWidth() != 0 && shape.getBounds2D().getHeight() != 0)
      {
        VirtualLiveAreaRenderer virtualLiveAreaRenderer = new VirtualLiveAreaRenderer(shape,0);

        _graphicsEngine.addRenderer(_virtualLiveAreaLayer, virtualLiveAreaRenderer);
        //_graphicsEngine.selectWithCrossHairs(virtualLiveAreaRenderer);
        _draggedRenderer = virtualLiveAreaRenderer;
      }
    }
  }

  /*
   * @author Kee Chin Seong
   */
  public void saveDraggedArea()
  {
    try 
    {
       Rectangle rect = new Rectangle((int)_draggedRenderer.getLocationOnScreen().getX(), (int)_draggedRenderer.getLocationOnScreen().getY(),(int)_draggedRenderer.getWidth(),(int)_draggedRenderer.getHeight());
       Robot robot = new Robot();
       Thread.sleep(1000);
       /*java.io.File f = new java.io.File("C:\\abc.jpg");
       BufferedImage img = robot.createScreenCapture(rect);

       javax.imageio.ImageIO.write(img,"jpeg",f);*/
    } 
    catch(Exception e)
    {
       e.printStackTrace();
    }   
  }
  
  public com.axi.guiUtil.Renderer getScreenShot()
  {
    return _draggedRenderer;
  }
  
  /**
   * @author sham
   */
  public void drawVirtualLiveAreaDisplay()
  {
    Assert.expect(_graphicsEngine != null);

    VirtualLiveImageGenerationSettings.getInstance().setObservable();
    _graphicsEngine.clearSelectedRenderers();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void drawSelectedROIOnEmptyCadDisplay()
  {
    Assert.expect(_graphicsEngine != null);

    CadCreatorSelectedAreaSettings.getInstance().setObservable();
    _graphicsEngine.clearSelectedRenderers();
  }

  /**
   * @author sham
   */
  public void clearVirtualLiveAreaDisplay()
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.clearCrossHairs();
    _graphicsEngine.removeRenderers(_virtualLiveAreaLayer);
    if(_rendererToUse != null)
      _graphicsEngine.setSelectedRenderer(_rendererToUse);
  }
  
  /**
   * @author sham
   */
  private void handleSelectedAreaObservable(Shape virtualLiveSelectedArea)
  {
    //Jack Hwee - if selected area out of bound, igrone the area.
    VirtualLiveImageGenerationSettings.getInstance().setMultipleSelectComponent(false);
    VirtualLiveImageGenerationSettings.getInstance().clearComponentList();
    Rectangle rect = virtualLiveSelectedArea.getBounds();
    if (rect.getMinX() < 0 || rect.getMinY() < 0)
      return;
   
    VirtualLiveImageGenerationSettings.getInstance().setVirtualLiveImageGenerationData(virtualLiveSelectedArea);
    drawVirtualLiveAreaDisplay();
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void handleSelectedAreaOnEmptyCadObservable(Shape virtualLiveSelectedArea)
  {
    Rectangle rect = virtualLiveSelectedArea.getBounds();
    if (rect.getMinX() < 0 || rect.getMinY() < 0)
      return;
   
    CadCreatorSelectedAreaSettings.getInstance().setCadCreatorSelectedAreaData(virtualLiveSelectedArea);
    drawSelectedROIOnEmptyCadDisplay();
  }
  
 /**
  * @set the board to be highlight
  * @author chin-seong.kee
  */
  public void setHighlightBoard(Board board)
  {
    _selectedBoardName = board.getName();

    List<Integer> boardLayers = new ArrayList<Integer>();
    boardLayers.addAll(getAllLayers(board));

    //Filter null boardLayers
    int size = boardLayers.size();
    for (int i = 0; i < size; ++i)
    {
      if(boardLayers.get(i) == null)
      {  
        boardLayers.remove(i);
        size = size - 1;
      }
    }

    Collection<com.axi.guiUtil.Renderer> renderers = _graphicsEngine.getRenderers(boardLayers);
    _graphicsEngine.setSelectedRenderers(renderers);
  }
  
  /*
   * @author chin-seong.kee
   */
  public void loadVirtualLiveVerificationImage(String projectName, boolean isCadCreatorEnv)
  {
    String virtualLiveVerificationImagesDir = "";//Directory.getVirtualLiveVerificationImagesDir(projectName);
    if(isCadCreatorEnv == false)
      virtualLiveVerificationImagesDir = Directory.getVirtualLiveVerificationImagesDir(projectName);
    else
      virtualLiveVerificationImagesDir = Directory.getXrayVerificationImagesDir(projectName);;
    
    File virtualLiveVerificationImagesFolder = new File(virtualLiveVerificationImagesDir);
    BufferedImage img = null;
    try 
    {
      img = ImageIO.read(new File(virtualLiveVerificationImagesFolder + File.separator + "verificationImage_top.jpg"));
    } 
    catch (IOException ex) 
    {
      System.out.println("Unable to read verificationImage_top.jpg....");
    }
 
    VirtualLiveVerificationImageRenderer virtualLiveVerificationImageRenderer = new VirtualLiveVerificationImageRenderer(img, _panel);
    virtualLiveVerificationImageRenderer.setPanelImage(img);
    _graphicsEngine.addRenderer(_topVirtualLiveVerificationImageLayer, virtualLiveVerificationImageRenderer);
    
    // chin-seong.kee - Populate Bottom Verification Image
    BufferedImage bottomVirtualLiveVerificationImg = null;
    try 
    {
      bottomVirtualLiveVerificationImg = ImageIO.read(new File(virtualLiveVerificationImagesFolder + File.separator + "verificationImage_bottom.jpg"));
    } 
    catch (IOException ex) 
    {
      System.out.println("Unable to read verificationImage_bottom.jpg....");
    }
    VirtualLiveVerificationImageRenderer bottomVirtualLiveVerificationImageRenderer = new VirtualLiveVerificationImageRenderer(bottomVirtualLiveVerificationImg, _panel);
    bottomVirtualLiveVerificationImageRenderer.setPanelImage(bottomVirtualLiveVerificationImg);
    _graphicsEngine.addRenderer(_bottomVirtualLiveVerificationImageLayer, bottomVirtualLiveVerificationImageRenderer);
    
    // chin-seong.kee - Populate blended Verification Image
    BufferedImage blendedVirtualLiveVerificationImg = null;
    try 
    {
      blendedVirtualLiveVerificationImg = ImageIO.read(new File(virtualLiveVerificationImagesFolder + File.separator + "verificationImage_both.jpg"));
    } 
    catch (IOException ex) 
    {
      System.out.println("Unable to read verificationImage_bottom.jpg....");
    }
    VirtualLiveVerificationImageRenderer blendedVirtualLiveVerificationImageRenderer = new VirtualLiveVerificationImageRenderer(blendedVirtualLiveVerificationImg, _panel);
    blendedVirtualLiveVerificationImageRenderer.setPanelImage(blendedVirtualLiveVerificationImg);
    _graphicsEngine.addRenderer(_bothVirtualLiveVerificationImageLayer, blendedVirtualLiveVerificationImageRenderer); 
  }
  
  /*
   * @author chin-seong.kee
   */
  public void clearVirtualLiveVerificationImageDisplay()
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.removeRenderers(_topVirtualLiveVerificationImageLayer);
    _graphicsEngine.removeRenderers(_bottomVirtualLiveVerificationImageLayer);
    _graphicsEngine.removeRenderers(_bothVirtualLiveVerificationImageLayer);
  }
  
  /*
   * @author chin-seong.kee
   */
  public Rectangle getPanelRectangleInPixel()
  {
    java.util.List<com.axi.guiUtil.Renderer> renderers = new ArrayList<>(_graphicsEngine.getRenderers(_topPanelLayer));
    
    Assert.expect(renderers.size() > 0);
    return renderers.get(0).getBounds();
  }
}
