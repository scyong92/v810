package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;


/**
 * This class is responsible for drawing the reference designator.
 *
 * @author Bill Darbie
 */
class ReferenceDesignatorRenderer extends TextRenderer
{
  private com.axi.v810.business.panelDesc.Component _component;

  /**
   * @author Andy Mechtenberg
   */
  ReferenceDesignatorRenderer(com.axi.v810.business.panelDesc.Component component)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(component != null);
    _component = component;

    // set a default font
    setFont(FontUtil.getRendererFont(16));

    refreshData();
  }

  /**
   * @author Bill Darbie
   */
  protected void refreshData()
  {
    String referenceDesignator = _component.getReferenceDesignator();

    Shape shape = _component.getShapeRelativeToPanelInNanoMeters();
    Rectangle2D bounds = shape.getBounds2D();

    initializeText(referenceDesignator, bounds.getCenterX(), bounds.getCenterY(), true);
  }

  /**
   * @author Laura Cormos
   */
  public com.axi.v810.business.panelDesc.Component getComponent()
  {
    Assert.expect(_component != null);
    return _component;
  }
  }
