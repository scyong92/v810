package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.rmi.*;




/**
 * This is the main Frame for the drawCad GUI
 *
 * @author Bill Darbie
 */
public class CadFrame extends JFrame
{


  /**
   * @author Bill Darbie
   */
  public CadFrame()
  {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
  }

  /**
   * @author Bill Darbie
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    int eventId = e.getID();
    if (eventId == WindowEvent.WINDOW_CLOSING)
    {
      System.exit(0);
    }
  }
}
