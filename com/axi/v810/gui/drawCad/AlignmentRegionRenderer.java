package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

/**
 * This class is responsible for drawing the VerificationRegions.
 *
 * @author Andy Mechtenberg
 */
public class AlignmentRegionRenderer extends ShapeRenderer
{
  private ProgramGeneration _programGeneration = ProgramGeneration.getInstance();
  private Project _project;
  private AlignmentGroup _alignmentGroup;
  private AlignmentGroup _topAlignmentGroup;
  private AlignmentGroup _bottomAlignmentGroup;
  private Color _regionColor;


  /**
   * @author Andy Mechtenberg
   */
  AlignmentRegionRenderer(Project project, AlignmentGroup alignmentGroup, Color regionColor)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(project != null);
    Assert.expect(alignmentGroup != null);
    Assert.expect(regionColor != null);

    _project = project;
    _alignmentGroup = alignmentGroup;
    _topAlignmentGroup = _alignmentGroup;
    _regionColor = regionColor;
    refreshData();
  }

  /**
   * @author Andy Mechtenberg
   */
  AlignmentRegionRenderer(Project project, AlignmentGroup topAlignmentGroup, AlignmentGroup bottomAlignmentGroup, Color regionColor)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(project != null);
    Assert.expect(topAlignmentGroup != null);
    Assert.expect(bottomAlignmentGroup != null);
    Assert.expect(regionColor != null);

    _project = project;
    _topAlignmentGroup = topAlignmentGroup;
    _bottomAlignmentGroup = bottomAlignmentGroup;
    _alignmentGroup = _topAlignmentGroup;
    _regionColor = regionColor;
    refreshData();
  }

  /**
   * @author Andy Mechtenberg
   */
  void setTopRegion(boolean topRegion)
  {
    if (topRegion)
      _alignmentGroup = _topAlignmentGroup;
    else
      _alignmentGroup = _bottomAlignmentGroup;
  }

  /**
   * @author Andy Mechtenberg
   */
  public AlignmentGroup getAlignmentGroup()
  {
    return _alignmentGroup;
  }

  /**
   * Color the region it's proper color
   * @author Andy Mechtenberg
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    Color origColor = graphics2D.getColor();
    Stroke origStroke = graphics2D.getStroke();

    if (_project.getTestProgram().isAlignmentGroupInvalidated(_alignmentGroup))
      graphics2D.setColor(LayerColorEnum.ALIGNMENT_GROUP_INVALID_COLOR.getColor());
    else
      graphics2D.setColor(_regionColor);
    graphics2D.setStroke(new BasicStroke(5));

    graphics2D.draw(getShape());

    graphics2D.setColor(origColor);
    graphics2D.setStroke(origStroke);
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void refreshData()
  {
    Assert.expect(_project != null);
    Assert.expect(_alignmentGroup != null);
    
    // XCR1549 (By Lee Herng 22-Dec-2012) - Always get TestProgram here because it will always
    // re-generate TestProgram when calling getTestProgram(). This will prevent
    // assert when AlignmentThroughputRenderer calls refreshData().
    TestProgram testProgram = _project.getTestProgram();

    // if no pads or fiducials at all, then no rect -- render nothing
    if (_alignmentGroup.isEmpty())
    {
      initializeShape(new Rectangle2D.Double(0, 0, 0, 0));
      return;
    }
    else if (testProgram.isAlignmentGroupInvalidated(_alignmentGroup))
    {
      // if invalid, calc bounds of pads, display differently
      PanelRectangle boundsRect = _alignmentGroup.getBoundsRelativeToPanelInNanoMeters();
      Shape groupBounds = new Rectangle(boundsRect.getMinX(), boundsRect.getMinY(), boundsRect.getWidth(), boundsRect.getHeight());
      initializeShape(groupBounds);
      return;
    }
//    // Added by Jack Hwee - to avoid crash when AlignmentThroughputRenderer calls refreshData(),
//    //                      if testSubProgram is empty. 
//    else if (testProgram.getFilteredTestSubPrograms().isEmpty() == false)  
//    {
//      initializeShape(new Rectangle2D.Double(0, 0, 0, 0));
//      return;
//    }
    
    Shape region = testProgram.getAlignmentRegion(_alignmentGroup).getAllowableAlignmentRegionInNanoMeters().getRectangle2D();

    initializeShape(region);
  }
}
