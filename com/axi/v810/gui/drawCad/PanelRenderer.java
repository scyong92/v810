package com.axi.v810.gui.drawCad;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This class is responsible for drawing the Panel outline.
 *
 * @author Bill Darbie
 */
public class PanelRenderer extends ShapeRenderer
{
  private Panel _panel;
  private boolean _isStaticImage = false;

  /**
   * @author Andy Mechtenberg
   */
  public PanelRenderer(Panel panel)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(panel != null);
    _panel = panel;

    refreshData();
  }
  
  public PanelRenderer(Panel panel, boolean isStaticImage)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(panel != null);
    _panel = panel;

    _isStaticImage = isStaticImage;
    
    refreshData();
    
    _isStaticImage = false;
  }
  
  /**
   * Return the name of the panel
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    return _panel.getProject().getName();
  }

  /**
   * @author Bill Darbie
   */
  protected void refreshData()
  {
    Assert.expect(_panel != null);

    if(_isStaticImage)
       initializeShape(_panel.getStaticImagePanelShapeInNanoMeters());
    else
       initializeShape(_panel.getShapeInNanoMeters());
  }

  /**
   * @author Laura Cormos
   */
  public Panel getPanel()
  {
    Assert.expect(_panel != null);

    return _panel;
  }
}
