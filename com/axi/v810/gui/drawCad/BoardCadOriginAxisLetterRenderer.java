package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * This class is responsible for drawing the reference designator.
 *
 * @author Laura Cormos
 */
class BoardCadOriginAxisLetterRenderer extends TextRenderer
{
  private Board _board;
  private boolean _xAxis;
  private PanelCoordinate _letterBoxCenterCoordinate;

  /**
   * @author Laura Cormos
   */
  BoardCadOriginAxisLetterRenderer(Board board, boolean xAxis)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(board != null);
    _board = board;
    _xAxis = xAxis;

    // set a default font
    setFont(FontUtil.getRendererFont(14));

    refreshData();
  }

  /**
   * @author Laura Cormos
   */
  protected void refreshData()
  {
    Shape shape;
    Rectangle2D bounds;

    // everything is started with the board origin coordinate which includes all rotations and flips
    PanelCoordinate boardOriginCoord = _board.getCadOriginRelativeToPanelInNanometers();
    int boardOriginX = boardOriginCoord.getX();
    int boardOriginY = boardOriginCoord.getY();

    // get the direction of the x axis
    Shape xAxisLineShape = _board.getXaxisLineAfterAllRotationsInNanometers();
    Rectangle2D xBounds = xAxisLineShape.getBounds2D();
    int minX = MathUtil.roundDoubleToInt(xBounds.getMinX());
    int minY = MathUtil.roundDoubleToInt(xBounds.getMinY());
    int maxX = MathUtil.roundDoubleToInt(xBounds.getMaxX());
    int maxY = MathUtil.roundDoubleToInt(xBounds.getMaxY());

    boolean xAxisIsHorizontal = false;
    int xAxisSign = 0;

    if (minY == maxY) // x axis is horizontal
    {
      if (MathUtil.fuzzyLessThan(boardOriginX, maxX))
      {
        // left to right x axis arrow
        xAxisIsHorizontal = true;
        xAxisSign = 1;
      }
      else
      {
        // right to left x axis arrow
        xAxisIsHorizontal = true;
        xAxisSign = -1;
      }
    }
    else if (minX  == maxX) // x axis is vertical
    {
      if (MathUtil.fuzzyLessThan(boardOriginY, maxY))
        {
          // bottom to top x axis arrow
          xAxisSign = 1;
        }
      else
        {
          // top to bottom x axis arrow
          xAxisSign = -1;
        }
    }
    else
      Assert.expect(false);

    // get the direction of the y axis
    // x and y axis are not always in the same relationship to each other, depending on the flips
    Shape yAxisLineShape = _board.getYaxisLineAfterAllRotationsInNanometers();
    Rectangle2D yBounds = yAxisLineShape.getBounds2D();
    minX = MathUtil.roundDoubleToInt(yBounds.getMinX());
    minY = MathUtil.roundDoubleToInt(yBounds.getMinY());
    maxX = MathUtil.roundDoubleToInt(yBounds.getMaxX());
    maxY = MathUtil.roundDoubleToInt(yBounds.getMaxY());

    int yAxisSign = 0;

    if (xAxisIsHorizontal) // y axis has to be vertical, determine which way, up or down
    {
      if (MathUtil.fuzzyLessThan(boardOriginY, maxY))
      {
        // bottom to top y axis arrow
        yAxisSign = 1;
      }
      else
      {
        // top to bottom y axis arrow
        yAxisSign = -1;
      }
    }
    else // y axis is horizontal, determine which way, left or right
    {
      if (MathUtil.fuzzyLessThan(boardOriginX, maxX))
      {
        // left to right y axis arrow
        yAxisSign = 1;
      }
      else
      {
        // right to left y axis arrow
        yAxisSign = -1;
      }
    }

    // make arrows and letter position relative to the board size
    int boardXdimension = _board.getWidthInNanoMeters();
    int arrowBodyLength = boardXdimension/10;
    int arrowHeadLength = arrowBodyLength/8;
    int letterBoxDimension = arrowHeadLength*3;

    // set the center coordinate for the letter's bounding box for the X axis
    if (_xAxis)
    {
      // the letter should always show on the outside of the board ouline, away from the board origin
      if (xAxisIsHorizontal)
        _letterBoxCenterCoordinate = new PanelCoordinate(boardOriginX + xAxisSign * (arrowBodyLength + arrowHeadLength + letterBoxDimension / 2),
                                                         boardOriginY - (yAxisSign * letterBoxDimension / 2));
      else
        _letterBoxCenterCoordinate = new PanelCoordinate(boardOriginX - (yAxisSign * letterBoxDimension / 2),
                                                         boardOriginY + xAxisSign * (arrowBodyLength + arrowHeadLength + letterBoxDimension / 2));
    }
    // set the center coordinate for the letter's bounding box for the Y axis
    else
    {
      // the letter should always show on the outside of the board ouline, away from the board origin
      if (xAxisIsHorizontal)
        _letterBoxCenterCoordinate = new PanelCoordinate(boardOriginX - (xAxisSign * letterBoxDimension / 2),
                                                         boardOriginY + yAxisSign * (arrowBodyLength + arrowHeadLength + letterBoxDimension / 2));
      else
        _letterBoxCenterCoordinate = new PanelCoordinate(boardOriginX + yAxisSign * (arrowBodyLength + arrowHeadLength + letterBoxDimension / 2),
                                                         boardOriginY - (xAxisSign * letterBoxDimension / 2));
    }

    // set up the bounding box shape
    shape = new Rectangle2D.Double(_letterBoxCenterCoordinate.getX() - letterBoxDimension / 2,
                                   _letterBoxCenterCoordinate.getY() - letterBoxDimension / 2,
                                   letterBoxDimension,
                                   letterBoxDimension);
    if (_xAxis)
    {
      bounds = shape.getBounds2D();
      initializeText("X", bounds.getCenterX(), bounds.getCenterY(), false);
    }
    else
    {
      bounds = shape.getBounds2D();
      initializeText("Y", bounds.getCenterX(), bounds.getCenterY(), true);
    }
  }
}
