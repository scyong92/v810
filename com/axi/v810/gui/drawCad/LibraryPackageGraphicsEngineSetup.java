package com.axi.v810.gui.drawCad;

import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;

/**
 *
 * <p>Title: LibraryPackageGraphicsEngineSetup</p>
 *
 * <p>Description: This engine is to draw the Library Land Pattern with the orientation arrow</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LibraryPackageGraphicsEngineSetup implements Observer
{
  private static final int _BORDER_SIZE_IN_PIXELS = 50;
  private static final int _packagePinLayer = JLayeredPane.DEFAULT_LAYER.intValue();
  private static final int _packagePinNameLayer = _packagePinLayer + 1;

  private DrawCadPanel _drawCadPanel;
  private GraphicsEngine _graphicsEngine;
  private LibraryPackage _libraryPackage;
  private Map<LibraryPackagePin, LibraryPackagePinRenderer> _packgePinToPackagePinRenderMap = new HashMap<LibraryPackagePin, LibraryPackagePinRenderer>();

  private CreateNewCadPanel _createNewCadPanel;
  /**
   *
   * @param drawCadPanel DrawCadPanel
   * @param graphicsEngine GraphicsEngine
   *
   * @author Tan Hock Zoon
   */
  LibraryPackageGraphicsEngineSetup(DrawCadPanel drawCadPanel, GraphicsEngine graphicsEngine)
  {
    Assert.expect(drawCadPanel != null);
    Assert.expect(graphicsEngine != null);
    _drawCadPanel = drawCadPanel;
    _graphicsEngine = graphicsEngine;

    _graphicsEngine.setPixelBorderSize(_BORDER_SIZE_IN_PIXELS);
  }
  
  /*
   * @author Kee Chin Seong
   */
  LibraryPackageGraphicsEngineSetup(CreateNewCadPanel createNewCadPanel, GraphicsEngine graphicsEngine)
  {
    Assert.expect(createNewCadPanel != null);
    Assert.expect(graphicsEngine != null);
    _createNewCadPanel = createNewCadPanel;
    _graphicsEngine = graphicsEngine;

    _graphicsEngine.setPixelBorderSize(_BORDER_SIZE_IN_PIXELS);
  }

  /**
   *
   * @param libraryPackage LibraryPackage
   *
   * @author Tan Hock Zoon
   */
  void displayLibraryPackage(LibraryPackage libraryPackage)
  {
    Assert.expect(libraryPackage != null);
    _libraryPackage = libraryPackage;

    populateLayersWithRenderers();
    setColors();
    setVisibility();
    setLayersToUseWhenFittingToScreen();

    _graphicsEngine.setAxisInterpretation(true, false);

    MathUtilEnum displayUnits = null;

    displayUnits = Project.getCurrentlyLoadedProject().getDisplayUnits();

    double measureFactor = 1.0/MathUtil.convertUnits(1.0, displayUnits, MathUtilEnum.NANOMETERS);
    _graphicsEngine.setMeasurementInfo(measureFactor, MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits),
       MeasurementUnits.getMeasurementUnitString(displayUnits));

    _graphicsEngine.getSelectedRendererObservable().addObserver(this);
    _graphicsEngine.getDisplayPadNameObservable().addObserver(this);
  }

  /**
   *
   * @return GraphicsEngine
   *
   * @author Tan Hock Zoon
   */
  public GraphicsEngine getGraphicsEngine()
  {
    Assert.expect(_graphicsEngine != null);

    return _graphicsEngine;
  }

  /**
   * @author Tan Hock Zoon
   */
  private void populateLayersWithRenderers()
  {
    _graphicsEngine.reset();
    _packgePinToPackagePinRenderMap.clear();

    for  (LibraryPackagePin libraryPackagePin: _libraryPackage.getLibraryPackagePins())
    {
      LibraryPackagePinRenderer packagePinRenderer = new LibraryPackagePinRenderer(libraryPackagePin);
      LibraryPackagePinNameRenderer libraryPackagePinNameRenderer = new LibraryPackagePinNameRenderer(libraryPackagePin, _libraryPackage.getLibraryPackagePins().size());
      
      _graphicsEngine.addRenderer(_packagePinLayer, packagePinRenderer);
      _graphicsEngine.addRenderer(_packagePinNameLayer, libraryPackagePinNameRenderer);
      _packgePinToPackagePinRenderMap.put(libraryPackagePin, packagePinRenderer);
    }
  }

  /**
   * @author Tan Hock Zoon
   */
  private void setVisibility()
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.setVisible(_packagePinLayer, true);
    _graphicsEngine.setVisible(_packagePinNameLayer, false);
  }

  /**
   * @author Tan Hock Zoon
   */
  private void setColors()
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.setForeground(_packagePinLayer, LayerColorEnum.SURFACEMOUNT_PAD_COLOR.getColor());
  }

  /**
   * @author Tan Hock Zoon
   */
  private void setLayersToUseWhenFittingToScreen()
  {
    Collection<Integer> layers = new ArrayList<Integer>(1);
    layers.add(new Integer(_packagePinLayer));
    _graphicsEngine.setLayersToUseWhenFittingToScreen(layers);
  }

  /**
   * @author Tan Hock Zoon
   */
  void reset()
  {
    _graphicsEngine.reset();
    _packgePinToPackagePinRenderMap.clear();
    _libraryPackage = null;

    _graphicsEngine.getSelectedRendererObservable().deleteObserver(this);
    _graphicsEngine.getDisplayPadNameObservable().deleteObserver(this);
  }

  /**
   *
   * @param observable Observable
   * @param argument Object
   *
   * @author Tan Hock Zoon
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof SelectedRendererObservable)
        {
          // this comes from the GraphicsEngine whenever a group of "things" has been selected by the user
          // this code will highlight (turn white) the selected items.  Since this graphics engine setup only
          // cares about PackagePins, that's all this observer will pay attention to.
          boolean descriptionIsSet = false;
          _graphicsEngine.clearSelectedRenderers();
          /** Warning "unchecked cast" approved for cast from Object type
           *  when sent from SelectedRendererObservable notifyAll.  */
          for (com.axi.guiUtil.Renderer renderer :
               ((Collection<com.axi.guiUtil.Renderer>)argument)) {
            // if it's a PackagePin, highlight it
            if (renderer instanceof PackagePinRenderer)
            {
              _graphicsEngine.setSelectedRenderer(renderer);

              // if more than one package pin was selected, we set the status bar descriptive text to nothing
              // so, we'll set it the first time, and if we get another, clear it out.
              if (descriptionIsSet)
                _drawCadPanel.setDescription(" ");  // if we use null, the entire status bar goes away, so we'll use an empty space instead
              else
              {
                _drawCadPanel.setDescription(renderer.toString());
                descriptionIsSet = true;
              }
            }
          }
        }
        else if (observable instanceof DisplayPadNameObservable)
        {
          if (argument instanceof Boolean)
          {
            Boolean setPadNameVisible = (Boolean)argument;
            if (setPadNameVisible.booleanValue())
                _graphicsEngine.setVisible(_packagePinNameLayer, true); 
            else
                _graphicsEngine.setVisible(_packagePinNameLayer, false); 
          }
        }
      }
    });
  }
}
