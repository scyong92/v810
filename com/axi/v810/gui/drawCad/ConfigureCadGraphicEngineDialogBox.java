package com.axi.v810.gui.drawCad;

import javax.swing.*;

import com.axi.util.*;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import java.awt.Color;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.border.BevelBorder;
import java.awt.FlowLayout;
/**
 *
 * @author chin-seong.kee
 */
public class ConfigureCadGraphicEngineDialogBox extends JDialog
{
  private ControlToolBar _controlToolBar;
  private CadCreatorControlToolBar _cadCreatorControlToolBar;
  
  private JPanel _topGraphicLayerPanel = new JPanel();
  private JPanel _bottomGraphicLayerPanel = new JPanel();
  
  private Border border1 = BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140));
  private Border border2 = new TitledBorder(border1, "Top Graphic Layer");
  private Border border3 = BorderFactory.createEtchedBorder(EtchedBorder.RAISED, Color.white, new Color(148, 145, 140));
  private Border border4 = new TitledBorder(border3, "Bottom Graphic Layer");
  
  private GridLayout _selectReconstructionRegionsGridLayout = new GridLayout();
  private JCheckBox _topGraphicLayerCheckBox = new JCheckBox();
  private JCheckBox _topPanelGraphicLayerCheckBox = new JCheckBox();
  private JCheckBox _topComponentGraphicLayerCheckBox = new JCheckBox();
  private JCheckBox _topPadGraphicLayerCheckBox = new JCheckBox();
  private JCheckBox _topCompRefDesGraphicLayerCheckBox = new JCheckBox();
  private GridLayout gridLayout1 = new GridLayout();
  
  private JCheckBox _bottomGraphicLayerCheckBox = new JCheckBox();
  private JCheckBox _bottomPanelGraphicLayerCheckBox = new JCheckBox();
  private JCheckBox _bottomComponentGraphicLayerCheckBox = new JCheckBox();
  private JCheckBox _bottomPadGraphicLayerCheckBox = new JCheckBox();
  private JCheckBox _bottomCompRefDesGraphicLayerCheckBox = new JCheckBox();
  
  private JPanel _topGraphicLayers = new JPanel();
  private GridLayout gridLayout2 = new GridLayout();
  private BorderLayout borderLayout1 = new BorderLayout();
  
  private JPanel _buttonPanel = new JPanel();
  private JButton _okButton = new JButton();
  
  private JPanel _mainPanel = new JPanel();
  private BorderLayout borderLayout2 = new BorderLayout();
  
  private JPanel _irpBoundaryPanel = new JPanel();
  
  private ButtonGroup buttonGroup1 = new ButtonGroup();
  private JRadioButton _verificationIrpBoundariesRadioButton = new JRadioButton();
  private JRadioButton _inspectionIrpBoundariesRadioButton = new JRadioButton();
  private Border border5 = BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.white, Color.white,
      new Color(103, 101, 98), new Color(148, 145, 140));
  private Border border6 = new TitledBorder(border5, "Select IRP Boundaries");
  private JRadioButton _noneRadioButton = new JRadioButton();
  private FlowLayout flowLayout1 = new FlowLayout();
  
  private boolean _applySettings = false;
  
  public ConfigureCadGraphicEngineDialogBox()
  {
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author George A. David
   */
  public ConfigureCadGraphicEngineDialogBox(ControlToolBar controlToolBar)
  {
    Assert.expect(controlToolBar != null);

    //Ngie Xing, XCR-2093, Two Control CAD renderer windows shown when switch from Modify Subtypes tab to Alignment Setup tab
    setModal(true);

    _controlToolBar = controlToolBar;
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    pack();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public ConfigureCadGraphicEngineDialogBox(CadCreatorControlToolBar controlToolBar)
  {
    Assert.expect(controlToolBar != null);

    //Ngie Xing, XCR-2093, Two Control CAD renderer windows shown when switch from Modify Subtypes tab to Alignment Setup tab
    setModal(true);

    _cadCreatorControlToolBar = controlToolBar;
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    pack();
  }
  
  /**
   * @author George A. David
   */
  private void jbInit() throws Exception
  {
    border6 = new TitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED, Color.white,
        new Color(148, 145, 140)), "Select IRP Boundaries");
    this.getContentPane().setLayout(borderLayout1);
    _topGraphicLayerPanel.setBorder(border2);
    _topGraphicLayerPanel.setLayout(_selectReconstructionRegionsGridLayout);
    _selectReconstructionRegionsGridLayout.setColumns(1);
    _selectReconstructionRegionsGridLayout.setRows(5);
    
    _topGraphicLayerCheckBox.setText("Top Graphic Layer");
    _topGraphicLayerCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        topGraphicLayerCheckBox_actionPerformed(e);
      }
    });
    
    _topPanelGraphicLayerCheckBox.setText("Panel Graphic Layer");
    _topPanelGraphicLayerCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        topPanelGraphicLayerCheckBox_actionPerformed(e);
      }
    });
    
    _topComponentGraphicLayerCheckBox.setText("Component Layer");
    _topComponentGraphicLayerCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        topComponentLayerCheckBox_actionPerformed(e);
      }
    });
    
    _topPadGraphicLayerCheckBox.setText("Pad Layer");
    _topPadGraphicLayerCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        topPadLayerCheckBox_actionPerformed(e);
      }
    });
    
    _topCompRefDesGraphicLayerCheckBox.setText("Ref Des Layer");
    _topCompRefDesGraphicLayerCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        topRefDesLayerCheckBox_actionPerformed(e);
      }
    });
    
    _bottomGraphicLayerCheckBox.setText("Bottom Graphic Layer");
    _bottomGraphicLayerCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        bottomGraphicLayerCheckBox_actionPerformed(e);
      }
    });
    
    _bottomPanelGraphicLayerCheckBox.setText("Panel Graphic Layer");
    _bottomPanelGraphicLayerCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        bottomPanelGraphicLayerCheckBox_actionPerformed(e);
      }
    });
    
    _bottomComponentGraphicLayerCheckBox.setText("Component Layer");
    _bottomComponentGraphicLayerCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        bottomComponentLayerCheckBox_actionPerformed(e);
      }
    });
    
    _bottomCompRefDesGraphicLayerCheckBox.setText("Ref Des Layer");
    _bottomCompRefDesGraphicLayerCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        bottomRefDesLayerCheckBox_actionPerformed(e);
      }
    });
    
    _bottomPadGraphicLayerCheckBox.setText("Pad Layer");
    _bottomPadGraphicLayerCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        bottomPadLayerCheckBox_actionPerformed(e);
      }
    });

    _bottomGraphicLayerPanel.setLayout(gridLayout1);
    gridLayout1.setColumns(1);
    gridLayout1.setRows(5);
   
    _bottomGraphicLayerPanel.setBorder(border4);
    _topGraphicLayers.setLayout(gridLayout2);
    gridLayout2.setColumns(2);
    _okButton.setText("OK");//Apply Settings");
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    _mainPanel.setLayout(borderLayout2);
    _verificationIrpBoundariesRadioButton.setText("Verification IRP Boundaries");
    _verificationIrpBoundariesRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        //verificationIrpBoundariesRadioButton_actionPerformed(e);
      }
    });
    _inspectionIrpBoundariesRadioButton.setText("Inspection IRP Boundaries");
    _inspectionIrpBoundariesRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        //inspectionIrpBoundariesRadioButton_actionPerformed(e);
      }
    });
    _irpBoundaryPanel.setLayout(flowLayout1);
    _irpBoundaryPanel.setBorder(border6);
    _noneRadioButton.setText("None");
    _noneRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        //noneRadioButton_actionPerformed(e);
      }
    });
    _topGraphicLayerPanel.add(_topGraphicLayerCheckBox);
    _topGraphicLayerPanel.add(_topPanelGraphicLayerCheckBox);
    _topGraphicLayerPanel.add(_topComponentGraphicLayerCheckBox);
    _topGraphicLayerPanel.add(_topCompRefDesGraphicLayerCheckBox);
    _topGraphicLayerPanel.add(_topPadGraphicLayerCheckBox);
    
    _buttonPanel.add(_okButton);
    
    _bottomGraphicLayerPanel.add(_bottomGraphicLayerCheckBox);
    _bottomGraphicLayerPanel.add(_bottomPanelGraphicLayerCheckBox);
    _bottomGraphicLayerPanel.add(_bottomComponentGraphicLayerCheckBox);
    _bottomGraphicLayerPanel.add(_bottomCompRefDesGraphicLayerCheckBox);
    _bottomGraphicLayerPanel.add(_bottomPadGraphicLayerCheckBox);
    
    _topGraphicLayers.add(_topGraphicLayerPanel);
    _topGraphicLayers.add(_bottomGraphicLayerPanel);
    this.getContentPane().add(_buttonPanel, java.awt.BorderLayout.SOUTH);
    _irpBoundaryPanel.add(_noneRadioButton);
    _mainPanel.add(_topGraphicLayers, java.awt.BorderLayout.CENTER);
    
    //_mainPanel.add(_irpBoundaryPanel, java.awt.BorderLayout.SOUTH);
    
    _irpBoundaryPanel.add(_inspectionIrpBoundariesRadioButton);
    _irpBoundaryPanel.add(_verificationIrpBoundariesRadioButton);
    this.getContentPane().add(_mainPanel, java.awt.BorderLayout.CENTER);
    this.getContentPane().add(_buttonPanel, java.awt.BorderLayout.SOUTH);
    setTitle("Configure Reconstruction Regions Display");

    buttonGroup1.add(_noneRadioButton);
    buttonGroup1.add(_inspectionIrpBoundariesRadioButton);
    buttonGroup1.add(_verificationIrpBoundariesRadioButton);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void updateGraphicControllerStatus()
  {
    _topGraphicLayerCheckBox.setSelected(_controlToolBar.isTopLayerGraphicVisible());
    _topPanelGraphicLayerCheckBox.setSelected(_controlToolBar.isTopLayerPanelGraphicVisible());
    _topComponentGraphicLayerCheckBox.setSelected(_controlToolBar.isTopComponentLayerGraphicVisible());
    _topPadGraphicLayerCheckBox.setSelected(_controlToolBar.isTopPadGraphicLayersVisible());
    _topCompRefDesGraphicLayerCheckBox.setSelected(_controlToolBar.isTopCompRefDesGraphicLayersVisible());
    
    _bottomGraphicLayerCheckBox.setSelected(_controlToolBar.isBottomLayerGraphicVisible());
    _bottomPanelGraphicLayerCheckBox.setSelected(_controlToolBar.isBottomLayerPanelGraphicVisible());
    _bottomComponentGraphicLayerCheckBox.setSelected(_controlToolBar.isBottomComponentLayerGraphicVisible());
    _bottomPadGraphicLayerCheckBox.setSelected(_controlToolBar.isBottomPadGraphicLayersVisible());
    _bottomCompRefDesGraphicLayerCheckBox.setSelected(_controlToolBar.isBottomCompRefDesGraphicLayersVisible());
  }

  /**
   * @author Kee Chin Seong
   */
  public void okButton_actionPerformed(ActionEvent e)
  {
    _applySettings = true;
    
    setVisible(false);
    dispose();
  }
  
  public boolean isSettingsAppliedByUser()
  {
    return _applySettings;
  }

  /**
   * @author Kee Chin Seong
   */
  public void topGraphicLayerCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateTopLayerGraphicVisiblity();
  }

  /**
   * @author Kee Chin Seong
   */
  public void topPanelGraphicLayerCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateTopPanelGraphicLayerVisibility();
  }

  /**
   * @author Kee Chin Seong
   */
  public void topComponentLayerCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateTopComponentLayerVisibility();
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void topPadLayerCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateTopPadGraphicLayerVisiblity();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void topRefDesLayerCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateTopRefDesLayerVisibility();
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void bottomGraphicLayerCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateBottomLayerGraphicVisiblity();
  }

  /**
   * @author Kee Chin Seong
   */
  public void bottomPanelGraphicLayerCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateBottomPanelGraphicLayerVisibility();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void bottomRefDesLayerCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateBottomRefDesLayerVisibility();
  }

  /**
   * @author Kee Chin Seong
   */
  public void bottomComponentLayerCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateBottomComponentLayerVisibility();
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void bottomPadLayerCheckBox_actionPerformed(ActionEvent e)
  {
    _controlToolBar.updateBottomPadGraphicLayerVisiblity();
  }

  /**
   * @author Kee Chin Seong
   */
  public boolean areTopLayerGraphicVisible()
  {
    return _topGraphicLayerCheckBox.isSelected();
  }

  /**
   * @author Kee Chin Seong
   */
  public boolean areTopComponentGraphicLayerVisible()
  {
    return  _topComponentGraphicLayerCheckBox.isSelected();
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean areTopCompRefDesGraphicLayerVisible()
  {
    return  _topCompRefDesGraphicLayerCheckBox.isSelected();
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean areBottomCompRefDesGraphicLayerVisible()
  {
    return  _bottomCompRefDesGraphicLayerCheckBox.isSelected();
  }

  /**
   * @author Kee Chin Seong
   */
  public boolean areTopPadGraphicLayerVisible()
  {
    return _topPadGraphicLayerCheckBox.isSelected();
  }

  /**
   *@author Kee Chin Seong
   */
  public boolean areTopPanelGraphicLayerVisible()
  {
    return _topPanelGraphicLayerCheckBox.isSelected();
  }

  /**
   * @author Kee Chin Seong
   */
  public boolean areBottomLayerGraphicVisible()
  {
    return _bottomGraphicLayerCheckBox.isSelected();
  }

  /**
   * @author Kee Chin Seong
   */
  public boolean areBottomComponentGraphicLayerVisible()
  {
    return  _bottomComponentGraphicLayerCheckBox.isSelected();
  }

  /**
   * @author Kee Chin Seong
   */
  public boolean areBottomPadGraphicLayerVisible()
  {
    return _bottomPadGraphicLayerCheckBox.isSelected();
  }

  /**
   *@author Kee Chin Seong
   */
  public boolean areBottomPanelGraphicLayerVisible()
  {
    return _bottomPanelGraphicLayerCheckBox.isSelected();
  }

}
