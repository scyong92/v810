package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.MagnificationTypeEnum;
import com.axi.v810.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.MagnificationEnum;

/**
 * This class is responsible for drawing the VerificationRegions.
 *
 * @author George A. David
 */
public class VerificationRegionRenderer extends ShapeRenderer
{
  private ReconstructionRegion _verificationRegion;

  /**
   * @author George A. David
   */
  public VerificationRegionRenderer(ReconstructionRegion verificationRegion)
  {
    super(Renderer._COMPONENT_IS_SELECTABLE);
    Assert.expect(verificationRegion != null);

    _verificationRegion = verificationRegion;
    refreshData();
  }

  /**
   * @author George A. David
   */
  public String toString()
  {
   return "Verification Region: " + _verificationRegion.getRegionId() + " (X mils: " +
         (int)MathUtil.convertNanoMetersToMils(_verificationRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinX()) + " Y mils: " +
         (int)MathUtil.convertNanoMetersToMils(_verificationRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinY()) + ")";
  }

  /**
   * Pad 1 needs to be hilighted (colored in RED), so we'll override the default paintComponent
   * @author Andy Mechtenberg
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    Color origColor = graphics2D.getColor();
    graphics2D.draw(getShape());

    // text rendering
    Font origGraphicsFont = graphics.getFont();
    Font rendererFont = getFont();
    FontMetrics fontMetrics = getFontMetrics(rendererFont);

    // the Height is the max height needed to display any text in this font (ascent + descent + 1)
    // basically, the height of the bounding rectangle.
    // DrawString, however, needs the X, Y of the leftmost BASELINE point, so we need to subtract the max descent
    // from this Y otherwise any character below the baseline will be cut off.
    // we subtract another one, to better center it, and the height stuff added one as well. (Which is doesn't need to
    // do for text, but is needed for other shapes)
    String text = String.valueOf(_verificationRegion.getRegionId());
    graphics.setFont(rendererFont);
    graphics.drawString(text, 2, fontMetrics.getHeight() - fontMetrics.getMaxDescent());
    graphics.setFont(origGraphicsFont);
    graphics.setColor(origColor);
  }

  /**
   * @author George A. David
   */
  protected void refreshData()
  {
    Assert.expect(_verificationRegion != null);

    PanelRectangle regionBoundsInNanoMeters = _verificationRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    Assert.expect(regionBoundsInNanoMeters != null);

    if(_verificationRegion.getTestSubProgram().getMagnificationType().equals(MagnificationTypeEnum.LOW))
    {      
      Shape region = new Rectangle2D.Double(regionBoundsInNanoMeters.getMinX() / (double)MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel(),
                                            regionBoundsInNanoMeters.getMinY() / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel(),
                                            regionBoundsInNanoMeters.getWidth() / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel(),
                                            regionBoundsInNanoMeters.getHeight() / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
      region = regionBoundsInNanoMeters.getRectangle2D();

      initializeShape(region);
    }
    else
    {
      Shape region = new Rectangle2D.Double(regionBoundsInNanoMeters.getMinX() / (double)MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel(),
                                            regionBoundsInNanoMeters.getMinY() / (double)MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel(),
                                            regionBoundsInNanoMeters.getWidth() / (double)MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel(),
                                            regionBoundsInNanoMeters.getHeight() / (double)MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel());
      region = regionBoundsInNanoMeters.getRectangle2D();

      initializeShape(region);
    }
  }

  /**
   * @return VerificationRegion object associated with renderer
   * @author George A. David
   */
  public ReconstructionRegion getVerificationRegion()
  {
    Assert.expect(_verificationRegion != null);

    return _verificationRegion;
  }
}

