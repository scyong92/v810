/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.drawCad;

import java.awt.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author Jack Hwee
 */
public class HighlightOpticalRectangleRenderer extends ShapeRenderer
{
  com.axi.v810.business.panelSettings.OpticalCameraRectangle _opticalCameraRectangle = null;
  private Color _renderColor;

  /**
   * @author Jack Hwee
   */
  protected HighlightOpticalRectangleRenderer(com.axi.v810.business.panelSettings.OpticalCameraRectangle opticalCameraRectangle)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);

    Assert.expect(opticalCameraRectangle != null);
    _opticalCameraRectangle = opticalCameraRectangle;
    
    Color yellow = Color.YELLOW;
    _renderColor = new Color(yellow.getRed(), yellow.getGreen(), yellow.getBlue(), 128);  // translucent yellow
    
    refreshData();
  }

  /**
   * @author Jack Hwee
   */
  protected void refreshData()
  {
    Assert.expect(_opticalCameraRectangle != null);

    initializeShape(_opticalCameraRectangle.getRegion().getRectangle2D());
  }

  /**
   * @author Jack Hwee
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    graphics2D.setColor(_renderColor);
    graphics2D.setStroke(new BasicStroke(5));
    graphics2D.fill(getShape());
  }
}
