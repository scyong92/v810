package com.axi.v810.gui.drawCad;

import java.awt.*;
import java.awt.geom.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;


/**
 * This class is responsible for drawing the reference designator.
 *
 * @author Laura Cormos
 */
public class FiducialReferenceDesignatorRenderer extends TextRenderer
{
  private Fiducial _fiducial;
  private double _initialScaling = 1.0;

  /**
   * @author Laura Cormos
   */
  public FiducialReferenceDesignatorRenderer(Fiducial fiducial)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(fiducial != null);
    _fiducial = fiducial;

    // set a default font
    setFont(FontUtil.getRendererFont(16));

    refreshData();
  }

  /**
   * @author Laura Cormos
   */
  protected void refreshData()
  {
    String referenceDesignator = _fiducial.getName();

    Shape shape = _fiducial.getShapeRelativeToPanelInNanoMeters();
    Rectangle2D bounds = shape.getBounds2D();

    initializeText(referenceDesignator, bounds.getCenterX() * _initialScaling, bounds.getCenterY() * _initialScaling, true);
  }

  /**
   * @author George A. David
   */
  public void setInitialScaling(double initialScaling)
  {
    Assert.expect(initialScaling > 0);

    _initialScaling = initialScaling;
    refreshData();
  }

  /**
   * @author Laura Cormos
   */
  public Fiducial getFiducial()
  {
    Assert.expect(_fiducial != null);
    return _fiducial;
  }
  }
