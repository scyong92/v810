package com.axi.v810.gui;

/**
 * This class provides the ability to communicate graphics control information to the
 * graphics engine.
 *
 * @author Andy Mechtenberg
 */
public class GraphicsControlEnum extends GuiEventEnum
{
  private static int _index = -1;

  public static GraphicsControlEnum ALIGNMENT_VISIBLE = new GraphicsControlEnum(++_index);
  public static GraphicsControlEnum ALIGNMENT_INVISIBLE = new GraphicsControlEnum(++_index);
  public static GraphicsControlEnum ORIGIN_VISIBLE = new GraphicsControlEnum(++_index);
  public static GraphicsControlEnum ORIGIN_INVISIBLE = new GraphicsControlEnum(++_index);
  public static GraphicsControlEnum BOARD_NAME_VISIBLE = new GraphicsControlEnum(++_index);
  public static GraphicsControlEnum BOARD_NAME_INVISIBLE = new GraphicsControlEnum(++_index);


  /**
   * @author Andy Mechtenberg
   */
  private GraphicsControlEnum(int id)
  {
    super(id);
  }
}

