package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.util.*;

/**
 * This panel will allow the user to select if they would like to include learned data and which image sets to include
 * in their zip file. This panel is the second panel in the collect diagnostic package wizard.
 *
 * @author Erica Wheatcroft
 */
public class CollectDiagnosticPackageWizardMainPanel extends WizardPanel implements Observer
{
  private JCheckBox _addLearnedDataCheckBox = new JCheckBox();
  private JCheckBox _filterImageSetsCheckbox = new JCheckBox();
  private JComboBox _pinToFilterImageSetsOnComboBox = new JComboBox();
  private JComboBox _componentToFilterImagesOnComboBox = new JComboBox();
  private JTable _availableImageSets = new JTable();
  private JLabel _destinationLabel = new JLabel();
  private JButton _findDestinationButton = new JButton();
  private JTextField _destinationTextField = new JTextField();
  private JFileChooser _fileChooser = null;
  private JPanel _imageSetsPanel = null;
  private JPanel _imageSetsTableAndButtonPanels = null;
  private JPanel _filterImageSetsPanel = null;
  private JPanel _learnedDataPanel = null;
  private AvailableAndSelectedImageSetsTableModel _tableModel = new AvailableAndSelectedImageSetsTableModel(true);

  private ProjectArchiver _archiver = null;

  private Project _project = null;

  // default destination directory. Users can change the directory but not the zip file name.
  private static String _defaultDestinationDirectoryText = Directory.getProjectsDir();

  // boolean used to indicating that we are populating a swing component and to ignore events until done.
  private boolean _loadingComponent = false;

  private WizardDialog _dialog = null;

  /**
   * @author Erica Wheatcroft
   */
  public CollectDiagnosticPackageWizardMainPanel(WizardDialog dialog, ProjectArchiver archiver)
  {
    super(dialog);

    Assert.expect(archiver != null);
    Assert.expect(dialog != null);

    _archiver = archiver;
    _dialog = dialog;

    createPanel();

    GuiObservable.getInstance().addObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null, "Name of panel is null");
    Assert.expect(name.length() > 0, "Name of panel is an empty string");
    _name = name;
  }

  /**
   * @author Erica Wheatcroft
   */
  public String getWizardName()
  {
    Assert.expect(_name != null, "Name of panel is not set");
    return _name;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void populatePanelWithData()
  {
    Assert.expect(_archiver != null);

    // set the title
    _dialog.changeTitle(StringLocalizer.keyToString("COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_PANEL1_TITLE_KEY"));

    _project = Project.getCurrentlyLoadedProject();

    // set the name of the project to zip
    _archiver.setProjectName(_project.getName());

    // now populate the destination text field
    String zipName = _defaultDestinationDirectoryText + File.separator + _archiver.getProjectName() + FileName.getZipFileExtension();
    _destinationTextField.setText(zipName);
    _archiver.setDestinationFilename(zipName);

    populateComponentPins();

    // populate the learned data panel
    populateLearnedDataPanel();

    // populate the image sets panel
    populateAvailableImageSetsPanel();

    syncScreenDataWithArchiver();

    validatePanel();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void syncScreenDataWithArchiver()
  {
    if (_archiver.hasJointPad())
    {
      _loadingComponent = true;

      Pad archivePad = _archiver.getJointPad();
      com.axi.v810.business.panelDesc.Component archiveComponent = archivePad.getComponent();
      _componentToFilterImagesOnComboBox.setSelectedItem(archiveComponent);
      // now repopulate the pins combo box with the new selected combobox
      java.util.List<Pad> pads = archiveComponent.getPads();
      _pinToFilterImageSetsOnComboBox.removeAllItems();
      for (Pad pad : pads)
        _pinToFilterImageSetsOnComboBox.addItem(pad);
      _pinToFilterImageSetsOnComboBox.setSelectedItem(archivePad);

      _loadingComponent = false;
    }

  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateLearnedDataPanel()
  {
    Assert.expect(_archiver.hasProjectName());

    // remove anything that was previously displayed.
    _learnedDataPanel.removeAll();

    if (_project.areShortAndSubtypeLearningFilesValid())
    {
      _addLearnedDataCheckBox.setText(StringLocalizer.keyToString("COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_ADD_LEARNED_DATA_INSTRUCTION_KEY"));
      _learnedDataPanel.add(_addLearnedDataCheckBox);
    }
    else
    {
      JLabel noImagesExistLabel = new JLabel(StringLocalizer.keyToString("COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_EARNED_DATA_DOES_NOT_EXIST_KEY"));
      _learnedDataPanel.add(noImagesExistLabel);
    }

    repaint();
    revalidate();

  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateAvailableImageSetsPanel()
  {
    Assert.expect(_archiver.hasProjectName());
    Project project = Project.getCurrentlyLoadedProject();
    Assert.expect(_archiver.getProjectName().equalsIgnoreCase(project.getName()));

    // remove anything that was previously displayed.
    _imageSetsPanel.removeAll();

    // first check to see if for the project the user has selected if there exists verification images.
    boolean exceptionOccurred = false;
    java.util.List<ImageSetData> imageSets = null;
    try
    {
      project.getTestProgram().clearFilters();
      imageSets = ImageManager.getInstance().getCompatibleImageSetData(project);
    }
    catch (DatastoreException de)
    {
      exceptionOccurred = true;
    }

    if (imageSets == null || exceptionOccurred || imageSets.isEmpty())
    {
      JLabel noImagesExistLabel = new JLabel(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_NO_IMAGES_SETS_EXIST_KEY"));
      noImagesExistLabel.setHorizontalAlignment(JLabel.CENTER);
      _imageSetsPanel.add(noImagesExistLabel, BorderLayout.CENTER);

    }
    else if (imageSets.isEmpty() == false && exceptionOccurred == false)
    {
      _imageSetsPanel.add(_filterImageSetsPanel, BorderLayout.NORTH);
      _imageSetsPanel.add(_imageSetsTableAndButtonPanels, BorderLayout.CENTER);
      _tableModel.populateWithImageSetData(imageSets, _archiver.getImageSetsToArchive());
      setupTable();
    }

    repaint();
    revalidate();

  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateComponentPins()
  {
    _loadingComponent = true;
    java.util.List < com.axi.v810.business.panelDesc.Component > components = _project.getPanel().getComponents();
    _componentToFilterImagesOnComboBox.removeAllItems();
    for (com.axi.v810.business.panelDesc.Component component : components)
      _componentToFilterImagesOnComboBox.addItem(component);
    // now select the first component
    _componentToFilterImagesOnComboBox.setSelectedIndex(0);

    Assert.expect(_componentToFilterImagesOnComboBox.getSelectedItem() instanceof com.axi.v810.business.panelDesc.Component);
    com.axi.v810.business.panelDesc.Component selectedComponent =
      (com.axi.v810.business.panelDesc.Component)_componentToFilterImagesOnComboBox.getSelectedItem();

    java.util.List<Pad> pads = selectedComponent.getPads();
    _pinToFilterImageSetsOnComboBox.removeAllItems();
    for (Pad pad : pads)
      _pinToFilterImageSetsOnComboBox.addItem(pad);
    _pinToFilterImageSetsOnComboBox.setSelectedIndex(0);
    _loadingComponent = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void validatePanel()
  {
    if (_archiver.hasProjectName() && _archiver.hasDestinationFilename() && _archiver.getNumberOfImageSetsToArchive() > 0)
    {
      // if we have gotten this far then we know the user has specified all the necessary information. so we can fire
      // the button state event next can be set to enabled, cancel can be enabled, back is enabled, and generate is enabled.
      WizardButtonState buttonState = new WizardButtonState(true, true, false, false);
      _observable.updateButtonState(buttonState);
    }
    else
    {
      WizardButtonState buttonState = new WizardButtonState(false, true, false, false);
      _observable.updateButtonState(buttonState);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    // do nothing
  }

  /**
   * @author Erica Wheatcroft
   */
  public void removeObservers()
  {
    GuiObservable.getInstance().deleteObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void setupTable()
  {
    TableColumn test = _availableImageSets.getColumnModel().getColumn(0);
    CheckCellEditor cellEditor = new CheckCellEditor(_availableImageSets.getBackground(), _availableImageSets.getForeground());
    test.setCellEditor(cellEditor);

    TableColumn selectedImageSetsNameCol = _availableImageSets.getColumnModel().getColumn(1);
    ImageSetRenderer imageSetRenderForSelectedImageSets = new ImageSetRenderer();
    selectedImageSetsNameCol.setCellRenderer(imageSetRenderForSelectedImageSets);

    TableColumn selectedImageSetsDateCol = _availableImageSets.getColumnModel().getColumn(2);
    DefaultTableCellRenderer selectedImageSetsDateColumnCenterRenderer = new DefaultTableCellRenderer();
    selectedImageSetsDateColumnCenterRenderer.setHorizontalAlignment(JLabel.CENTER);
    selectedImageSetsDateCol.setCellRenderer(selectedImageSetsDateColumnCenterRenderer);

    TableColumn selectedImageSetsPopulatedCol = _availableImageSets.getColumnModel().getColumn(3);
    DefaultTableCellRenderer selectedImageSetsPopulatedColumnCenterRenderer = new DefaultTableCellRenderer();
    selectedImageSetsPopulatedColumnCenterRenderer.setHorizontalAlignment(JLabel.CENTER);
    selectedImageSetsPopulatedCol.setCellRenderer(selectedImageSetsPopulatedColumnCenterRenderer);

    TableColumn selectedImageSetsPercentCompatibleCol = _availableImageSets.getColumnModel().getColumn(4);
    NumericRenderer selectedImageSetsPercentCompatibleColCenterRenderer = new NumericRenderer(0, JLabel.CENTER);
    selectedImageSetsPercentCompatibleCol.setCellRenderer(selectedImageSetsPercentCompatibleColCenterRenderer);


    // set up column widths
    ColumnResizer.adjustColumnPreferredWidths(_availableImageSets, true);

    int padding = 175;
    int origTableWidth = _availableImageSets.getMinimumSize().width;
    int scrollHeight = _availableImageSets.getPreferredSize().height;
    int tableHeight = _availableImageSets.getPreferredSize().height;
    int prefHeight = scrollHeight;
    if (tableHeight < scrollHeight)
    {
      prefHeight = tableHeight;
      Dimension prefScrollSize = new Dimension(origTableWidth + padding, prefHeight);
      _availableImageSets.setPreferredScrollableViewportSize(prefScrollSize);
    }

    // now lets set up the % of relestate for each column.
    // set up column widths
    int iTotalColumnWidth = _availableImageSets.getColumnModel().getTotalColumnWidth();
    _availableImageSets.getColumnModel().getColumn(0).setPreferredWidth((int)(iTotalColumnWidth * .15));
    _availableImageSets.getColumnModel().getColumn(1).setPreferredWidth((int)(iTotalColumnWidth * .40));
    _availableImageSets.getColumnModel().getColumn(2).setPreferredWidth((int)(iTotalColumnWidth * .20));
    _availableImageSets.getColumnModel().getColumn(3).setPreferredWidth((int)(iTotalColumnWidth * .10));
    _availableImageSets.getColumnModel().getColumn(4).setPreferredWidth((int)(iTotalColumnWidth * .15));

  }


  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setLayout(new BorderLayout(5, 5));
    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    // lets create the learned data panel
    JPanel northPanel = new JPanel(new BorderLayout(5,5));
    JPanel instructionPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0,0));
    JLabel instructionLabel = new JLabel(StringLocalizer.keyToString("COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_INSTRUCTION_KEY"));
    instructionLabel.setFont(FontUtil.getBoldFont(instructionLabel.getFont(),Localization.getLocale()));
    instructionPanel.add(instructionLabel);
    northPanel.add(instructionPanel, BorderLayout.NORTH);

    _learnedDataPanel = new JPanel(new GridLayout(1,1,5,5));
    _learnedDataPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
                       StringLocalizer.keyToString("COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_ADD_LEARNED_DATA_PANEL_INSTRUCTION_KEY")),
      BorderFactory.createEmptyBorder(5,5,5,5)));
    _learnedDataPanel.add(_addLearnedDataCheckBox);
    northPanel.add(_learnedDataPanel, BorderLayout.SOUTH);
    add(northPanel, BorderLayout.NORTH);

    // lets create the image sets panel
    _imageSetsPanel = new JPanel(new BorderLayout(5,5));
    _imageSetsPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
                       StringLocalizer.keyToString("COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_ADD_IMAGE_SETS_INSTRUCTION_KEY")),
                       BorderFactory.createEmptyBorder(0,5,0,5)));

    _imageSetsTableAndButtonPanels = new JPanel(new BorderLayout(5,5));

    JScrollPane availableImageSetsScrollBar = new JScrollPane();
    _availableImageSets.getTableHeader().setReorderingAllowed(false);
    availableImageSetsScrollBar.getViewport().add(_availableImageSets);
    _availableImageSets.setModel(_tableModel);
    _imageSetsTableAndButtonPanels.add(availableImageSetsScrollBar, BorderLayout.CENTER);
    _imageSetsPanel.add(_imageSetsTableAndButtonPanels, BorderLayout.CENTER);

    _filterImageSetsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    JPanel enableFilteringCheckBoxPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _filterImageSetsCheckbox.setText(StringLocalizer.keyToString("COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_FILTER_IMAGE_SETS_KEY"));
    enableFilteringCheckBoxPanel.add(_filterImageSetsCheckbox);
    _filterImageSetsPanel.add(enableFilteringCheckBoxPanel);

    JPanel filterPanel = new JPanel(new GridLayout(1,2,5,5));
    JPanel innerFilterPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    innerFilterPanel.setBorder(BorderFactory.createEmptyBorder(0, 25, 0, 0));
    JLabel componentLabel = new JLabel();
    componentLabel.setText(StringLocalizer.keyToString("COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_FILTER_IMAGE_COMPONENT_KEY"));
    innerFilterPanel.add(componentLabel);
    _componentToFilterImagesOnComboBox.setEnabled(false);
    innerFilterPanel.add(_componentToFilterImagesOnComboBox);
    JLabel pinLabel = new JLabel();
    pinLabel.setText(StringLocalizer.keyToString("COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_FILTER_IMAGE_PIN_KEY"));
    innerFilterPanel.add(pinLabel);
    _pinToFilterImageSetsOnComboBox.setEnabled(false);
    innerFilterPanel.add(_pinToFilterImageSetsOnComboBox);
    filterPanel.add(innerFilterPanel);
    _filterImageSetsPanel.add(filterPanel);
    _imageSetsPanel.add(_filterImageSetsPanel, BorderLayout.NORTH);
    add(_imageSetsPanel, BorderLayout.CENTER);

    // create the destination panel
    JPanel destinationPanel = new JPanel(new FlowLayout());
    destinationPanel.setBorder(BorderFactory.createEmptyBorder(0,10,0,10));
    _destinationLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_DESTINATION_LABEL_KEY"));
    _findDestinationButton.setText("...");
    _destinationTextField.setColumns(25);
    _destinationTextField.setEditable(false);
    destinationPanel.add(_destinationLabel);
    destinationPanel.add(_destinationTextField);
    destinationPanel.add(_findDestinationButton);
    add(destinationPanel, BorderLayout.SOUTH);

    _findDestinationButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_fileChooser == null)
        {
          _fileChooser = new JFileChooser()
          {
            public void approveSelection()
            {
              File selectedFile = getSelectedFile();
              if (selectedFile.exists())
                super.approveSelection();
              else
              {
                String message = StringLocalizer.keyToString(new LocalizedString("WIZARD_DIR_DOES_NOT_EXIST_KEY", new Object[]{selectedFile}));
                JOptionPane.showMessageDialog(this, message);
              }
            }
          };
        }

        _fileChooser.setApproveButtonText(StringLocalizer.keyToString("ZIP_PROEJECT_WIZARD_SELECT_BUTTON_KEY"));
        _fileChooser.setCurrentDirectory(new File(Directory.getProjectsDir()));
        _fileChooser.setDialogTitle(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_SELECT_DESTINATION_LABEL_KEY"));
        _fileChooser.setDialogType(JFileChooser.CUSTOM_DIALOG);
        _fileChooser.setMultiSelectionEnabled(false);
        _fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int returnValue = _fileChooser.showDialog(_dialog, StringLocalizer.keyToString("ZIP_PROEJECT_WIZARD_SELECT_BUTTON_KEY"));
        if (returnValue == JFileChooser.APPROVE_OPTION)
        {
          // the user has selected a new directory.
          File selectedDirectory = _fileChooser.getSelectedFile();
          Assert.expect(selectedDirectory.isDirectory());

          String zipDestination = selectedDirectory.getAbsolutePath() + File.separator + _archiver.getProjectName() + FileName.getZipFileExtension();

          _destinationTextField.setText(zipDestination);
          _archiver.setDestinationFilename(zipDestination);

          validatePanel();
        }
      }
    });
    _addLearnedDataCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if(_addLearnedDataCheckBox.isSelected())
        {
          // tell the archiver to include verification images
          _archiver.setIncludeAlgorithmLearning(true);
        }
        else
        {
          // tell the archiver to NOT include verification images
          _archiver.setIncludeAlgorithmLearning(false);
        }
      }
    });
    _filterImageSetsCheckbox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_filterImageSetsCheckbox.isSelected() == false)
        {
          _componentToFilterImagesOnComboBox.setEnabled(false);
          _pinToFilterImageSetsOnComboBox.setEnabled(false);
          _archiver.clearComponent();
          _archiver.clearJointPad();
          populateAvailableImageSetsPanel();
        }
        else
        {
//          _loadingComponent = true;

          _componentToFilterImagesOnComboBox.setEnabled(true);
          _pinToFilterImageSetsOnComboBox.setEnabled(true);
//          java.util.List < com.axi.v810.business.panelDesc.Component > components = _project.getPanel().getComponents();
//          _componentToFilterImagesOnComboBox.removeAllItems();
//          for (com.axi.v810.business.panelDesc.Component component : components)
//            _componentToFilterImagesOnComboBox.addItem(component);
          // now select the first component
//          _pinToFilterImageSetsOnComboBox.removeAllItems();
//          _componentToFilterImagesOnComboBox.setSelectedIndex(0);
//          _pinToFilterImageSetsOnComboBox.setSelectedIndex(0);
//          _loadingComponent = false;
          populateAvailableImageSetsPanel();
          pinComboBox_actionPerformed();
        }
      }
    });

    _componentToFilterImagesOnComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        componentComboBox_actionPerformed();
      }
    });

    _pinToFilterImageSetsOnComboBox.addActionListener(new ActionListener()
   {
     public void actionPerformed(ActionEvent e)
     {
       pinComboBox_actionPerformed();
     }
   });
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(Observable observable, Object object)
  {
    Assert.expect(observable != null, " observable is null");

    // this should be an update from something already on the swing thread. if not then assert.
    Assert.expect(SwingUtilities.isEventDispatchThread(), "update not from a swing thread");

    if (observable instanceof GuiObservable)
    {
      GuiEventEnum imageSetEnum = ((GuiEvent)object).getGuiEventEnum();
      if (imageSetEnum instanceof ImageSetEventEnum)
      {
        if (imageSetEnum.equals(ImageSetEventEnum.IMAGE_SET_SELECTED_FOR_ARCHIVE))
        {
          Assert.expect(object != null, "Object is null");
          Assert.expect(object instanceof GuiEvent, "Object is not of type Gui Event");
          Assert.expect(((GuiEvent)object).getSource() instanceof java.util.List, "not of type image set data");
          java.util.List<ImageSetData> imageSets = (java.util.List)((GuiEvent)object).getSource();
          _archiver.clearInspectionImageSets();
          for (ImageSetData imageSet : imageSets)
            _archiver.addInspectionImageSet(imageSet);
          validatePanel();
        }
        else if (imageSetEnum.equals(ImageSetEventEnum.IMAGE_SET_NOT_SELECTED_FOR_ARCHIVE))
        {
          Assert.expect(object != null, "Object is null");
          Assert.expect(object instanceof GuiEvent, "Object is not of type Gui Event");
          Assert.expect(((GuiEvent)object).getSource() instanceof java.util.List, "not of type image set data");
          java.util.List<ImageSetData> imageSets = (java.util.List)((GuiEvent)object).getSource();
          _archiver.clearInspectionImageSets();
          for (ImageSetData imageSet : imageSets)
            _archiver.addInspectionImageSet(imageSet);
          validatePanel();
        }
//        else
//          Assert.expect(false); // no other events should be sent as we have limited notifications
      }
    }
    else
      Assert.expect(false);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void componentComboBox_actionPerformed()
  {
    if(_loadingComponent == false)
    {
      Assert.expect(_componentToFilterImagesOnComboBox.getSelectedItem() instanceof com.axi.v810.business.panelDesc.Component);
      com.axi.v810.business.panelDesc.Component selectedComponent =
          (com.axi.v810.business.panelDesc.Component)_componentToFilterImagesOnComboBox.getSelectedItem();

      _archiver.clearComponent();
      _archiver.clearJointPad();
      // now we need to check if the archiver has a pin / pad already set if so then clear it and add this component.
      if(_archiver.isComponentAllowed() == false)
        _archiver.clearJointPad();
      else
      {
        if (_archiver.hasComponent())
        {
          if (_archiver.getComponent().equals(selectedComponent))
          {
            // the component selected is the same as the one the already stored in the archiver.
            return;
          }
        }
      }

      // now repopulate the pins combo box with the new selected combobox
      _loadingComponent = true;
      java.util.List<Pad> pads = selectedComponent.getPads();
      _pinToFilterImageSetsOnComboBox.removeAllItems();
      for (Pad pad : pads)
        _pinToFilterImageSetsOnComboBox.addItem(pad);
      _pinToFilterImageSetsOnComboBox.setSelectedIndex(0);
      _loadingComponent = false;

      // so they are selecting another component then the one already stored so lets clear out the image sets
      _archiver.clearInspectionImageSets();
      _tableModel.clearSelectedImageSets();
      _archiver.setComponent(selectedComponent);
      try
      {
        java.util.List<ImageSetData> filteredImageSets = _archiver.getAllowableInspectionImageSets();
        _tableModel.populateWithImageSetData(filteredImageSets, new ArrayList<ImageSetData>());
      }
      catch (DatastoreException de)
      {
        _archiver.clearInspectionImageSets();
        _tableModel.clearSelectedImageSets();
        JOptionPane.showMessageDialog(_dialog,
                                      de.getMessage(),
                                      StringLocalizer.keyToString("COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_PANEL1_TITLE_KEY"),
                                      JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void pinComboBox_actionPerformed()
  {
    if (_loadingComponent == false)
    {
      Assert.expect(_pinToFilterImageSetsOnComboBox.getSelectedItem() instanceof Pad);
      Pad selectedPad = (Pad)_pinToFilterImageSetsOnComboBox.getSelectedItem();

      // now we need to check if the archiver has a pin / pad already set if so then clear it and add this component.
      if (_archiver.isJointPadAllowed() == false)
        _archiver.clearComponent();
      else
      {
        if (_archiver.hasJointPad())
        {
          if (_archiver.getJointPad().equals(selectedPad))
          {
            // the joint selected is the same as the one the already stored in the archiver.
            return;
          }
        }
      }
      // so they are selecting another pad then the one already stored so lets clear out the image sets
      _archiver.clearInspectionImageSets();
      _tableModel.clearSelectedImageSets();
      _archiver.setJointPad(selectedPad);
      try
      {
        java.util.List<ImageSetData> filteredImageSets = _archiver.getAllowableInspectionImageSets();
        _tableModel.populateWithImageSetData(filteredImageSets, new ArrayList<ImageSetData>());
      }
      catch (DatastoreException de)
      {
        _archiver.clearInspectionImageSets();
        _tableModel.clearSelectedImageSets();
        JOptionPane.showMessageDialog(_dialog,
                                      de.getMessage(),
                                      StringLocalizer.keyToString("COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_TITLE_KEY"),
                                      JOptionPane.ERROR_MESSAGE);
      }
    }
  }
}
