package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.util.image.Image;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

public class InteractiveLearningExposedPadPanel extends JPanel
{
  InitialTuningPanel _invoker;

  private JPanel _mainPanel;
  private JPanel _labelPanel;
  private JPanel _jointPanel;
  private JLabel _jointLabel;
  private JLabel _jointNameLabel;
  private JPanel _imageSetPanel;
  private JLabel _imageSetLabel;
  private JLabel _imageSetNameLabel;
  private JPanel _subtypePanel;
  private JLabel _subtypeLabel;
  private JLabel _subtypeNameLabel;
  private JPanel _imagePanel;
  private JLabel _currentImageLabel;
  private JPanel _currentImagePanel;
  private ExposedPadImageRenderer _currentImageRenderer;
  private JLabel _expectedImageLabel;
  private JPanel _expectedImagePanel;
  private ExposedPadImageRenderer _expectedImageRenderer;
  private JPanel _controlPanel;
  private JPanel _mainButtonsPanel;
  private JButton _acceptButton;
  private JButton _rejectButton;
  private JPanel _otherButtonsPanel;
  private JButton _cancelButton;

  private boolean _useBoldFont = false;

  private final String SUBTYPE_NAME_PREFIX = StringLocalizer.keyToString("MMGUI_INTERACTIVE_LEARNING_SUBTYPE_PREFIX_KEY");
  private final String IMAGE_SET_PREFIX = StringLocalizer.keyToString("MMGUI_INTERACTIVE_LEARNING_IMAGEE_SET_PREFIX_KEY");
  private final String JOINT_NAME_PREFIX = StringLocalizer.keyToString("MMGUI_INTERACTIVE_LEARNING_JOINT_NAME_PREFIX_KEY");
  private final String CURRENT_IMAGE_LABEL = StringLocalizer.keyToString("MMGUI_INTERACTIVE_LEARNING_CURRENT_IMAGE_KEY");

  private Algorithm _algorithm;
  private Image _inspectedImage;
  private java.awt.image.BufferedImage _bufferedInspectedImage;
  private Image _expectedImage;
  private java.awt.image.BufferedImage _bufferedExpectedImage;
  private String _subtypeName;
  private String _imageSetName;
  private String _boardName;
  private String _jointName;

  private int _previousImageWidth = 0;
  private int _previousImageHeight = 0;

  public InteractiveLearningExposedPadPanel(InitialTuningPanel invoker)
  {
    _invoker = invoker;
    jbInit();
  }

  private void jbInit()
  {
    // set flag to use bold font highlight only in English
    String lang = Config.getInstance().getStringValue(SoftwareConfigEnum.LANGUAGE);
    _useBoldFont = lang.equalsIgnoreCase("en");

    _mainPanel = new JPanel(new BorderLayout(20, 20));
    this.add(_mainPanel);

    _labelPanel = new JPanel(new GridLayout(3, 1, 10, 0));
    _mainPanel.add(_labelPanel, BorderLayout.NORTH);

    _jointPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    _labelPanel.add(_jointPanel);

    _jointLabel = new JLabel(JOINT_NAME_PREFIX);
    _jointPanel.add(_jointLabel);

    _jointNameLabel = new JLabel("");
    _jointPanel.add(_jointNameLabel);

    _imageSetPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    _labelPanel.add(_imageSetPanel);

    _imageSetLabel = new JLabel(IMAGE_SET_PREFIX);
    _imageSetPanel.add(_imageSetLabel);

    _imageSetNameLabel = new JLabel("");
    _imageSetPanel.add(_imageSetNameLabel);

    _subtypePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    _labelPanel.add(_subtypePanel);

    _subtypeLabel = new JLabel(SUBTYPE_NAME_PREFIX);
    _subtypePanel.add(_subtypeLabel);

    _subtypeNameLabel = new JLabel("");
    _subtypePanel.add(_subtypeNameLabel);

    if (_useBoldFont)
    {
      _jointNameLabel.setFont(FontUtil.getBoldFont(_jointNameLabel.getFont(),Localization.getLocale()));
      _imageSetNameLabel.setFont(FontUtil.getBoldFont(_imageSetNameLabel.getFont(),Localization.getLocale()));
      _subtypeNameLabel.setFont(FontUtil.getBoldFont(_subtypeNameLabel.getFont(),Localization.getLocale()));
    }

    _imagePanel = new JPanel(new BorderLayout(10, 10));
    _mainPanel.add(_imagePanel, BorderLayout.CENTER);

    _currentImagePanel = new JPanel(new BorderLayout());
    _imagePanel.add(_currentImagePanel, BorderLayout.WEST);

    _currentImageLabel = new JLabel(CURRENT_IMAGE_LABEL);
    if (_useBoldFont)
    {
      _currentImageLabel.setFont(FontUtil.getBoldFont(_currentImageLabel.getFont(),Localization.getLocale()));
    }
    _currentImagePanel.add(_currentImageLabel, BorderLayout.NORTH);

    _currentImageRenderer = new ExposedPadImageRenderer();
    _currentImagePanel.add(_currentImageRenderer, BorderLayout.CENTER);

    _expectedImagePanel = new JPanel(new BorderLayout());
    _imagePanel.add(_expectedImagePanel, BorderLayout.EAST);

    _expectedImageLabel = new JLabel(StringLocalizer.keyToString("MMGUI_INTERACTIVE_LEARNING_EXPECTED_IMAGE_KEY"));
    if (_useBoldFont)
    {
      _expectedImageLabel.setFont(FontUtil.getBoldFont(_expectedImageLabel.getFont(),Localization.getLocale()));
    }
    _expectedImagePanel.add(_expectedImageLabel, BorderLayout.NORTH);

    _expectedImageRenderer = new ExposedPadImageRenderer();
    _currentImagePanel.add(_expectedImageRenderer, BorderLayout.CENTER);

    _controlPanel = new JPanel(new BorderLayout());
    _mainPanel.add(_controlPanel, BorderLayout.SOUTH);

    _mainButtonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
    _controlPanel.add(_mainButtonsPanel, BorderLayout.WEST);

    _acceptButton = new JButton(StringLocalizer.keyToString("MMGUI_INTERACTIVE_LEARNING_ACCEPT_BUTTON_KEY"));
    _acceptButton.setToolTipText(StringLocalizer.keyToString("MMGUI_INTERACTIVE_LEARNING_ACCEPT_BUTTON_TOOLTIP_KEY"));
    _acceptButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        acceptButton_actionPerformed();
      }
    });
    _mainButtonsPanel.add(_acceptButton);

    _rejectButton = new JButton(StringLocalizer.keyToString("MMGUI_INTERACTIVE_LEARNING_REJECT_BUTTON_KEY"));
    _rejectButton.setToolTipText(StringLocalizer.keyToString("MMGUI_INTERACTIVE_LEARNING_REJECT_BUTTON_TOOLTIP_KEY"));
    _rejectButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        rejectButton_actionPerformed();
      }
    });
    _mainButtonsPanel.add(_rejectButton);

    _otherButtonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
    _controlPanel.add(_otherButtonsPanel, BorderLayout.EAST);

    _cancelButton = new JButton(StringLocalizer.keyToString("MMGUI_INTERACTIVE_LEARNING_CANCEL_BUTTON_KEY"));
    _rejectButton.setToolTipText(StringLocalizer.keyToString("MMGUI_INTERACTIVE_LEARNING_CANCEL_BUTTON_TOOLTIP_KEY"));
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed();
      }
    });
    _otherButtonsPanel.add(_cancelButton);

    _mainPanel.validate();
  }

  /**
   * @author George Booth
   */
  public void setButtonState(boolean enabled)
  {
    _acceptButton.setEnabled(enabled);
    _rejectButton.setEnabled(enabled);
    _cancelButton.setEnabled(enabled);
  }

  /**
   * @author George Booth
   */
  private void setDefaultState()
  {
    setButtonState(false);
    _subtypeNameLabel.setText("");
    _imageSetNameLabel.setText("");
    _jointNameLabel.setText("");
    _currentImagePanel.removeAll();
    _expectedImagePanel.removeAll();
    repaint();
  }

  /**
   * @author George Booth
   */
  public void setUserInputState(InteractiveLearningDialog parent, InspectionEvent inspectionEvent)
  {
    InspectionEventEnum inspectionEventEnum = inspectionEvent.getInspectionEventEnum();
    if (inspectionEventEnum.equals(InspectionEventEnum.INTERACTIVE_LEARNING_EXPOSED_PAD_USER_INFO))
    {
      InteractiveLearningExposedPadInspectionEvent learningEvent = (InteractiveLearningExposedPadInspectionEvent)inspectionEvent;

      _algorithm = learningEvent.getAlgorithm();
      Assert.expect(learningEvent.hasImageList());
      java.util.List<Image> imageList = learningEvent.getImageList();
      Assert.expect(imageList.size() == 2);
      _inspectedImage = imageList.get(0);
      _expectedImage = imageList.get(1);

      Assert.expect(learningEvent.hasStringList());
      java.util.List<String> stringList = learningEvent.getStringList();
      Assert.expect(stringList.size() == 4);
      _subtypeName = stringList.get(0);
      _imageSetName = stringList.get(1);
      _boardName = stringList.get(2);
      _jointName = stringList.get(3);

      learningEvent.decrementReferenceCount();

      _jointNameLabel.setText(_jointName);
      _imageSetNameLabel.setText(_imageSetName);
      _subtypeNameLabel.setText(_subtypeName);

      String currentImageLabelText = CURRENT_IMAGE_LABEL + ": " + _jointName + ", Board \"" + _boardName + "\"";
      _currentImageLabel.setText(currentImageLabelText);

      Dimension parentWithImages = parent.getSize();
      int parentWidthMinusImages = (int)parentWithImages.getWidth() - (2 * _previousImageWidth);
      int parentHeightMinusImages = (int)parentWithImages.getHeight() - _previousImageHeight;

      int imageWidth = _inspectedImage.getWidth();
      int imageHeight = _inspectedImage.getHeight();

      _previousImageWidth = imageWidth;
      _previousImageHeight = imageHeight;

      int windowWidth = Math.max((int)parentWidthMinusImages, (2 * imageWidth + 50));
      windowWidth = Math.max(windowWidth, 600);
      int windowHeight = parentHeightMinusImages + imageHeight;

      Dimension windowSize = new Dimension(windowWidth, windowHeight);
      parent.setSize(windowSize);
      parent.validate();

      _currentImagePanel.removeAll();
      _bufferedInspectedImage = _inspectedImage.getBufferedImage();
      _currentImageRenderer.setPreferredSize(new Dimension(imageWidth, imageHeight));
      _currentImageRenderer.updateImage(_bufferedInspectedImage);
      _currentImagePanel.add(_currentImageLabel, BorderLayout.NORTH);
      _currentImagePanel.add(_currentImageRenderer, BorderLayout.CENTER);

      _expectedImagePanel.removeAll();
      _bufferedExpectedImage = _expectedImage.getBufferedImage();
      _expectedImageRenderer.setPreferredSize(new Dimension(imageWidth, imageHeight));
      _expectedImageRenderer.updateImage(_bufferedExpectedImage);
      _expectedImagePanel.add(_expectedImageLabel, BorderLayout.NORTH);
      _expectedImagePanel.add(_expectedImageRenderer, BorderLayout.CENTER);

      setButtonState(true);
    }
    else
    {
      Assert.expect(false, "Wrong inspectionEventEnum with InteractiveLearningExposedPadInspectionEvent");
    }
  }

  /**
   * @author George Booth
   */
  private void acceptButton_actionPerformed()
  {
    Assert.expect(_algorithm != null);
    setButtonState(false);

    _algorithm.setPostiveUserInput();
    // only one response per inspection event
    _algorithm = null;
    // clean up
    _inspectedImage.decrementReferenceCount();
    _expectedImage.decrementReferenceCount();
  }

  /**
   * @author George Booth
   */
  private void rejectButton_actionPerformed()
  {
    Assert.expect(_algorithm != null);
    setButtonState(false);

    _algorithm.setNegativeUserInput();
    // only one response per inspection event
    _algorithm = null;
    // clean up
    _inspectedImage.decrementReferenceCount();
    _expectedImage.decrementReferenceCount();
  }

  /**
   * @author George Booth
   */
  public void cancelButton_actionPerformed()
  {
    setDefaultState();
    Assert.expect(_invoker != null);
    _invoker.interactiveLearningCancelled();
    // cancel interactive learning in algorithm
    Assert.expect(_algorithm != null);
    _algorithm.enableInteractiveLearning(false);
    // send negative response
    _algorithm.setNegativeUserInput();
    // only one response per inspection event
    _algorithm = null;
    // clean up
    _inspectedImage.decrementReferenceCount();
    _expectedImage.decrementReferenceCount();
  }
}


class ExposedPadImageRenderer extends JPanel
{
  private BufferedImage _image;

  public ExposedPadImageRenderer()
  {
    // do nothing
  }

  public void updateImage(BufferedImage image)
  {
    _image = image;
  }

  public void paintComponent(Graphics g)
  {
    super.paintComponent(g);
    if (_image == null)
      return;
    Dimension d = this.getSize();

    int imageWidth = _image.getWidth(this);
    int imageHeight = _image.getHeight(this);

    if ((imageWidth <= d.width) && (imageHeight <= d.height))
    {
      g.drawImage(_image, 0, 0, imageWidth, imageHeight, this);
    }
    else
    { // set width and height as needed to show whole image
      // scale width to d.width, check heigth
      double widthRatio = (double)imageWidth / (double)d.width;
      int width = (int)(imageWidth / widthRatio);
      int height = (int)(imageHeight / widthRatio);
      if (height > d.height)
      {
        // scale heigth to d.height
        double heightRatio = (double)imageHeight / (double)d.height;
        width = (int)(imageWidth / heightRatio);
        height = (int)(imageHeight / heightRatio);
      }

      g.drawImage(_image, 0, 0, width, height, this);
    }
  }
}
