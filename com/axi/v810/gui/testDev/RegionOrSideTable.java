package com.axi.v810.gui.testDev;

import java.awt.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.StringLocalizer;

/**
 * This class displays the information for all the regions shown in Verify CAD
 *
 * Regions are not supported in version 1
 *
 * @author George Booth
 */

class RegionOrSideTable extends JSortTable
{
  public static final int _REGION_NAME_INDEX = 0;
  public static final int _REGION_SIDE_INDEX = 1;

  // side choices
  private final String _TOP_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_TOP_LABEL_KEY");
  private final String _BOTTOM_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_BOTTOM_LABEL_KEY");

  private boolean _modelSet = false;

  /**
   * @author George Booth
   */
  public RegionOrSideTable(RegionOrSideTableModel model)
  {
    Assert.expect(model != null);
    _modelSet = true;
    super.setModel(model);

    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  }

  /**
   * @author George Booth
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };

    // column 0 is the package name
    columnModel.getColumn(_REGION_NAME_INDEX).setCellEditor(stringEditor);

    // column 1 is the joint type
    JComboBox sideComboBox = new JComboBox();
    sideComboBox.addItem(_TOP_SIDE);
    sideComboBox.addItem(_BOTTOM_SIDE);
    columnModel.getColumn(_REGION_SIDE_INDEX).setCellEditor(new DefaultCellEditor(sideComboBox));
  }

  /**
   * @author George Booth
   */
  public void clearSelection()
  {
    TableModel model = getModel();
    // this function gets called before the table model is set, during intialization. At that time,
    // the model will be a defaultTableModel so the assert is sure to fail
    if (_modelSet)
    {
      Assert.expect(model instanceof RegionOrSideTableModel);
      ((RegionOrSideTableModel)model).clearSelectedComponents();
    }
    super.clearSelection();
  }

}
