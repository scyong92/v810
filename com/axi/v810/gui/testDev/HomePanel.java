package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;
import com.axi.v810.hardware.SystemTypeEnum;

/**
 * This is the Home page panel for the Test Development Tool
 * This panel is mostly non-interactive.  It shows the user summary information about their
 * project, allows them to view the un-tuned subtypes, and enter free-form descriptive text
 * about their project.
 * @author Andy Mechtenberg
 */
public class HomePanel extends AbstractTaskPanel implements Observer
{
  private TestDev _testDev = null;
  private Project _project = null;
  private boolean _updateData = false;
//  private java.util.List<Subtype> _untunedSubtypes;
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  private BorderLayout _homeBorderLayout = new BorderLayout();
  private BorderLayout _projectTopBorderLayout = new BorderLayout(0, 5);
  private JPanel _projectTopPanel = new JPanel();
  private JPanel _projectCustomerInfoPanel = new JPanel();
  private JPanel _projectSummaryPanel = new JPanel();
  private BorderLayout _projectCustomerInfoBorderLayout = new BorderLayout();
  private JLabel _projectNotesLabel = new JLabel();
  private JTextArea _projectNotesTextArea = new JTextArea();
  private JScrollPane _projectNotesScrollPane = new JScrollPane();
  private JPanel _projectInfoPanel = new JPanel();
  private JPanel _projectGuidancePanel = new JPanel();
  private BorderLayout _projectInfoBorderLayout = new BorderLayout();
  private JLabel _projectGuidanceLabel = new JLabel();
  private JLabel _projectSummaryLabel = new JLabel();
  private JPanel _projectInfoDetailsPanel = new JPanel();
  private GridLayout _projectInfoDetailsGridLayout = new GridLayout();
  private JLabel _panelNameLabel = new JLabel();
  private JLabel _panelNameDataLabel = new JLabel();
  private JLabel _projectVersionLabel = new JLabel();
  private JLabel _projectVersionDataLabel = new JLabel();
  private JLabel _testTimeDataLabel = new JLabel();
  private JLabel _testTimeLabel = new JLabel();
  private JLabel _testCoverageDataLabel = new JLabel();
  private JLabel _testCoverageLabel = new JLabel();
  private JLabel _numUnTestableJointsDataLabel = new JLabel(); //XCR1374, KhangWah, 2011-09-20
  private JLabel _numUnTestableJointsLabel = new JLabel(); //XCR1374, KhangWah, 2011-09-20
  private JLabel _numTestedJointsDataLabel = new JLabel();
  private JLabel _numTestedJointsLabel = new JLabel();
  private JLabel _numJointsDataLabel = new JLabel();
  private JLabel _numJointsLabel = new JLabel();
  private JLabel _numBoardsDataLabel = new JLabel();
  private JLabel _numComponentsDataLabel = new JLabel();
  private JLabel _numComponentsLabel = new JLabel();
  private JLabel _numBoardsLabel = new JLabel();
  private JLabel _fixFocusSentance = new JLabel();

  private JPanel _initialThresholdsPanel = new JPanel();
  private BorderLayout _initialThresholdsBorderLayout = new BorderLayout();
  private JLabel _initialThresholdsLabel = new JLabel();
  private JComboBox _initialThresholdsComboBox = new JComboBox();
  private ActionListener _initialThresholdsComboBoxActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      initialThresholdsActionPerformed();
    }
  };
  
  private JPanel _initialRecipeSettingPanel = new JPanel();
  private BorderLayout _initialRecipeSettingBorderLayout = new BorderLayout();
  private JLabel _initialRecipeSettingLabel = new JLabel();
  private JComboBox _initialRecipeSettingComboBox = new JComboBox();
  private ActionListener _initialRecipeSettingComboBoxActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      initialRecipeSettingActionPerformed();
    }
  };

  private JPanel _systemTypePanel = new JPanel();
  private BorderLayout _systemTypeBorderLayout = new BorderLayout();
  private JLabel _systemTypeLabel = new JLabel();
  private JComboBox _systemTypeComboBox = new JComboBox();
  private ActionListener _systemTypeComboBoxActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      systemTypeActionPerformed();
    }
  };
  
  private BorderLayout _projectInitialSettingBorderLayout = new BorderLayout();
  private JPanel _projectInitialSettingPanel = new JPanel();
  private BorderLayout _projectTextBorderLayout = new BorderLayout(10, 5);
  private JPanel _projectTextPanel = new JPanel();
  private JLabel _customerNameLabel = new JLabel();
  private JTextField _customerNameTextField = new JTextField();
  private JLabel _programmerNameLabel = new JLabel();
  private JTextField _programmerNameTextField = new JTextField();

  private GridLayout _projectSummaryGridLayout = new GridLayout();
  private FlowLayout _projectGuidanceFlowLayout = new FlowLayout();
  private Border _homePanelBorder;
  private Border _lightGrayLineBorder;
  private Border _etchedLeftBorder;
  private JPanel _projectNotesPanel = new JPanel();
  private BorderLayout _projectNotesBorderLayout = new BorderLayout();
  private JPanel _customerNamePanel = new JPanel();
  private JPanel _programmerNamePanel = new JPanel();
  private BorderLayout _customerNameBorderLayout = new BorderLayout();
  private BorderLayout _programmerNameBorderLayout = new BorderLayout();

  /**
   * Private constructor used for JBuilder designer only
   * @author Andy Mechtenberg
   */
  private HomePanel()
  {
    jbInit();
  }

  /**
   * @author Andy Mechtenberg
   */
  public HomePanel(TestDev testDev)
  {
    super(testDev);
    Assert.expect(testDev != null);
    _testDev = testDev;
    jbInit();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    _projectTopPanel.setLayout(_projectTopBorderLayout);

    _homePanelBorder = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(115, 114, 105),new Color(165, 163, 151)),BorderFactory.createEmptyBorder(5,5,5,5));
    _lightGrayLineBorder = BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.lightGray,1),BorderFactory.createEmptyBorder(0,2,0,0));
    _etchedLeftBorder = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new Color(142, 142, 142)),BorderFactory.createEmptyBorder(0,2,0,0));
    this.setLayout(_homeBorderLayout);
    _projectCustomerInfoPanel.setLayout(_projectCustomerInfoBorderLayout);
    _projectNotesLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_PROJECT_NOTES_KEY"));
    _projectSummaryPanel.setLayout(_projectSummaryGridLayout);
    _projectGuidancePanel.setBackground(SystemColor.info);
    _projectGuidancePanel.setBorder(_homePanelBorder);
    _projectGuidancePanel.setLayout(_projectGuidanceFlowLayout);
    _projectInfoPanel.setLayout(_projectInfoBorderLayout);
    _projectGuidanceLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_NO_PROJECT_LOADED_KEY"));
    _projectSummaryLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_PROJECT_SUMMARY_KEY"));
    _projectInfoDetailsPanel.setLayout(_projectInfoDetailsGridLayout);
    _projectInfoDetailsGridLayout.setColumns(2);
    _projectInfoDetailsGridLayout.setHgap(-1);
    _projectInfoDetailsGridLayout.setRows(8); //UnTestableJoints, change to 8 to include number of unTestableJoints 
    _projectInfoDetailsGridLayout.setVgap(-2);
    _panelNameLabel.setBorder(_etchedLeftBorder);
    _panelNameLabel.setOpaque(true);
    _panelNameLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_PANEL_NAME_KEY"));
    _projectVersionLabel.setBorder(_etchedLeftBorder);
    _projectVersionLabel.setOpaque(true);
    _projectVersionLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_PROJECT_VERSION_KEY"));

    _testTimeDataLabel.setBackground(Color.white);
    _testTimeDataLabel.setBorder(_lightGrayLineBorder);
    _testTimeDataLabel.setOpaque(true);
    _testTimeDataLabel.setText("");
    _testTimeLabel.setBorder(_etchedLeftBorder);
    _testTimeLabel.setOpaque(true);
    _testTimeLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_TEST_TIME_KEY"));
    _testCoverageDataLabel.setBackground(Color.white);
    _testCoverageDataLabel.setBorder(_lightGrayLineBorder);
    _testCoverageDataLabel.setOpaque(true);
    _testCoverageDataLabel.setText("");
    _testCoverageLabel.setBorder(_etchedLeftBorder);
    _testCoverageLabel.setOpaque(true);
    _testCoverageLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_TEST_COVERAGE_KEY"));
    _numUnTestableJointsDataLabel.setBackground(Color.white); //XCR1374, KhangWah, 2011-09-20
    _numUnTestableJointsDataLabel.setBorder(_lightGrayLineBorder); //XCR1374, KhangWah, 2011-09-20
    _numUnTestableJointsDataLabel.setOpaque(true); //XCR1374, KhangWah, 2011-09-20
    _numUnTestableJointsDataLabel.setText(""); //XCR1374, KhangWah, 2011-09-20
    _numUnTestableJointsLabel.setBorder(_etchedLeftBorder); //XCR1374, KhangWah, 2011-09-20
    _numUnTestableJointsLabel.setOpaque(true); //XCR1374, KhangWah, 2011-09-20
    _numUnTestableJointsLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_NUM_UNTESTABLE_JOINTS_KEY")); //XCR1374, KhangWah, 2011-09-20
    _numTestedJointsDataLabel.setBackground(Color.white);
    _numTestedJointsDataLabel.setBorder(_lightGrayLineBorder);
    _numTestedJointsDataLabel.setOpaque(true);
    _numTestedJointsDataLabel.setText("");
    _numTestedJointsLabel.setBorder(_etchedLeftBorder);
    _numTestedJointsLabel.setOpaque(true);
    _numTestedJointsLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_NUM_TESTED_JOINTS_KEY"));
    _numJointsDataLabel.setBackground(Color.white);
    _numJointsDataLabel.setBorder(_lightGrayLineBorder);
    _numJointsDataLabel.setOpaque(true);
    _numJointsDataLabel.setText("");

    _numJointsLabel.setBorder(_etchedLeftBorder);
    _numJointsLabel.setOpaque(true);
    _numJointsLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_NUM_JOINTS_KEY"));
    _numBoardsDataLabel.setBackground(Color.white);
    _numBoardsDataLabel.setBorder(_lightGrayLineBorder);
    _numBoardsDataLabel.setOpaque(true);
    _numBoardsDataLabel.setText("");
    _numBoardsLabel.setBorder(_etchedLeftBorder);
    _numBoardsLabel.setOpaque(true);
    _numBoardsLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_NUM_BOARDS_KEY"));

    _numComponentsDataLabel.setBackground(Color.white);
    _numComponentsDataLabel.setBorder(_lightGrayLineBorder);
    _numComponentsDataLabel.setOpaque(true);
    _numComponentsDataLabel.setText("");
    _numComponentsLabel.setBorder(_etchedLeftBorder);
    _numComponentsLabel.setOpaque(true);
    _numComponentsLabel.setText(StringLocalizer.keyToString("HP_COMPONENTS_LABEL_KEY"));

    _panelNameDataLabel.setBackground(Color.white);
    _panelNameDataLabel.setBorder(_lightGrayLineBorder);
    _panelNameDataLabel.setOpaque(true);
    _panelNameDataLabel.setText("");
    _projectVersionDataLabel.setBackground(Color.white);
    _projectVersionDataLabel.setBorder(_lightGrayLineBorder);
    _projectVersionDataLabel.setOpaque(true);
    _projectVersionDataLabel.setText("");

    _projectSummaryGridLayout.setColumns(1);
    _projectSummaryGridLayout.setRows(2);
    _projectSummaryGridLayout.setVgap(10);
    _projectGuidanceFlowLayout.setVgap(20);
    this.setBorder(_homePanelBorder);
    _projectNotesTextArea.setBorder(_homePanelBorder);
    _projectNotesTextArea.setEditable(false);
    _projectNotesTextArea.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        projectNotesTextArea_focusLost();
      }
    });
    _projectCustomerInfoBorderLayout.setVgap(5);
    _homeBorderLayout.setVgap(10);
    _projectInfoBorderLayout.setVgap(5);
    _projectInfoDetailsPanel.setBackground(Color.white);
    _projectInfoDetailsPanel.setBorder(BorderFactory.createLoweredBevelBorder());
    _projectNotesPanel.setLayout(_projectNotesBorderLayout);
    _customerNamePanel.setLayout(_customerNameBorderLayout);
    _programmerNamePanel.setLayout(_programmerNameBorderLayout);
    _projectTextBorderLayout.setHgap(0);
    _initialThresholdsComboBox.setEnabled(false);
    _initialRecipeSettingComboBox.setEnabled(false);
    _customerNameTextField.setEditable(false);
    _programmerNameTextField.setEditable(false);
    this.add(_projectCustomerInfoPanel, BorderLayout.CENTER);
//    this.add(_projectSummaryPanel, BorderLayout.NORTH);
    this.add(_projectTopPanel, BorderLayout.NORTH);
    _projectTopPanel.add(_projectSummaryPanel, BorderLayout.CENTER);
    _projectNotesScrollPane.getViewport().add(_projectNotesTextArea, null);
    _projectSummaryPanel.add(_projectInfoPanel, null);
    _projectInfoPanel.add(_projectSummaryLabel,  BorderLayout.NORTH);
    _projectInfoPanel.add(_projectInfoDetailsPanel, BorderLayout.CENTER);
    _projectSummaryPanel.add(_projectGuidancePanel, null);
    _projectGuidancePanel.add(_projectGuidanceLabel, null);
    _projectInfoDetailsPanel.add(_panelNameLabel, null);
    _projectInfoDetailsPanel.add(_panelNameDataLabel, null);
    _projectInfoDetailsPanel.add(_projectVersionLabel, null);
    _projectInfoDetailsPanel.add(_projectVersionDataLabel, null);
    _projectInfoDetailsPanel.add(_numBoardsLabel, null);
    _projectInfoDetailsPanel.add(_numBoardsDataLabel, null);
    _projectInfoDetailsPanel.add(_numComponentsLabel, null);
    _projectInfoDetailsPanel.add(_numComponentsDataLabel, null);
    _projectInfoDetailsPanel.add(_numJointsLabel, null);
    _projectInfoDetailsPanel.add(_numJointsDataLabel, null);
    _projectInfoDetailsPanel.add(_numTestedJointsLabel, null);
    _projectInfoDetailsPanel.add(_numTestedJointsDataLabel, null);
    _projectInfoDetailsPanel.add(_numUnTestableJointsLabel, null); //XCR1374, KhangWah, 2011-09-20
    _projectInfoDetailsPanel.add(_numUnTestableJointsDataLabel, null); //XCR1374, KhangWah, 2011-09-20
    _projectInfoDetailsPanel.add(_testCoverageLabel, null);
    _projectInfoDetailsPanel.add(_testCoverageDataLabel, null);
//    _projectInfoDetailsPanel.add(_testTimeLabel, null);
//    _projectInfoDetailsPanel.add(_testTimeDataLabel, null);

//    _systemTypePanel.setLayout(_systemTypeBorderLayout);
//    _systemTypeLabel.setText(StringLocalizer.keyToString("HP_SYSTEM_TYPE_LABEL_KEY"));

//    _systemTypePanel.add(_systemTypeLabel, BorderLayout.NORTH);
//    _systemTypePanel.add(_systemTypeComboBox, BorderLayout.SOUTH);
//    _projectTopPanel.add(_systemTypePanel, BorderLayout.SOUTH);

    _initialThresholdsPanel.setLayout(_initialThresholdsBorderLayout);
    _initialThresholdsLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_PROJECT_INITIAL_THRESHOLDS_KEY"));

    _initialThresholdsPanel.add(_initialThresholdsLabel, BorderLayout.NORTH);
    _initialThresholdsPanel.add(_initialThresholdsComboBox, BorderLayout.CENTER);
   
    _initialRecipeSettingPanel.setLayout(_initialRecipeSettingBorderLayout);
    _initialRecipeSettingLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_PROJECT_INITIAL_RECIPE_SETTING_KEY"));

    _initialRecipeSettingPanel.add(_initialRecipeSettingLabel, BorderLayout.NORTH);
    _initialRecipeSettingPanel.add(_initialRecipeSettingComboBox, BorderLayout.CENTER);
    
    _projectInitialSettingPanel.setLayout(_projectInitialSettingBorderLayout);
    _projectInitialSettingPanel.add(_initialThresholdsPanel, BorderLayout.NORTH);
    _projectInitialSettingPanel.add(_initialRecipeSettingPanel, BorderLayout.CENTER);
    
    _projectTextPanel.setLayout(_projectTextBorderLayout);
    _customerNameLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_PROJECT_CUSTOMER_NAME_KEY"));
    _programmerNameLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_PROGRAMMER_NAME_KEY"));
    _customerNameTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        customerNameTextField_focusLost();
      }
    });
    _customerNameTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        customerNameTextField_focusLost();
      }
    });

    _programmerNameTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        programmerNameTextField_focusLost();
      }
    });
    _programmerNameTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        programmerNameTextField_focusLost();
      }
    });
    _projectNotesPanel.add(_projectNotesScrollPane, java.awt.BorderLayout.CENTER);
    _projectCustomerInfoPanel.add(_projectTextPanel, java.awt.BorderLayout.NORTH);
    _projectNotesPanel.add(_projectNotesLabel, java.awt.BorderLayout.NORTH);
    _projectCustomerInfoPanel.add(_projectNotesPanel, java.awt.BorderLayout.CENTER);
    _customerNamePanel.add(_customerNameTextField, java.awt.BorderLayout.CENTER);
    _customerNamePanel.add(_customerNameLabel, java.awt.BorderLayout.NORTH);
    _programmerNamePanel.add(_programmerNameTextField, java.awt.BorderLayout.CENTER);
    _programmerNamePanel.add(_programmerNameLabel, java.awt.BorderLayout.NORTH);
    _projectTextPanel.add(_projectInitialSettingPanel, java.awt.BorderLayout.NORTH);
    _projectTextPanel.add(_customerNamePanel, java.awt.BorderLayout.CENTER);
    _projectTextPanel.add(_programmerNamePanel, java.awt.BorderLayout.SOUTH);

    _projectNotesTextArea.setName(".projectNotesTextArea");
    _customerNameTextField.setName(".customerNameTextField");
    _programmerNameTextField.setName(".programmerNameTextField");
    _testTimeDataLabel.setName(".testTimeDataLabel");
    _projectVersionDataLabel.setName(".projectVersionDataLabel");
    _panelNameDataLabel.setName(".panelNameDataLabel");
    _numBoardsDataLabel.setName(".numberBoardsDataLabel");
    _numJointsDataLabel.setName(".totalJointsDataLabel");
    _numTestedJointsDataLabel.setName(".testedJointsDataLabel");
    _testCoverageDataLabel.setName(".testCoverageDataLabel");
    _numUnTestableJointsDataLabel.setName(".numUnTestableJointsDataLabel"); //XCR1374, KhangWah, 2011-09-20

    // make sure font matches test fields
    _projectNotesTextArea.setFont(_customerNameTextField.getFont());
  }

  /**
   * Called when this screen is made visible.  Some of the information displayed may have changed since
   * this screen was last visible, so this will update it.
   * @author Andy Mechtenberg
   */
  private void updateDataOnScreen()
  {
    // this screen is visible before a project is loaded, so this is a valid case.
    if (_project == null || _project.doesPanelExist() == false)
      return;
    com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
    _panelNameDataLabel.setText(_project.getName());
    updateProjectVersion();
    _numBoardsDataLabel.setText(String.valueOf(panel.getNumBoards()));
    _numComponentsDataLabel.setText(String.valueOf(panel.getNumComponents()));
    _numJointsDataLabel.setText(String.valueOf(panel.getNumJoints()));
    
    //XCR1374, KhangWah, 2011-09-12
    _project.getPanel().gatherJointsCountDetail(); //XCR1374, gather information of inspected joint and untestable joint.
//    int numTestedJoints = _project.getPanel().getNumInspectedJoints();
    int numTestedJoints = _project.getPanel().getNumInspectedJointsFast(); //XCR1374, KhangWah, 2011-09-12
    _numTestedJointsDataLabel.setText(String.valueOf(numTestedJoints));
//    int numUnTestableJoints = _project.getPanel().getNumUnTestableJoints();
    int numUnTestableJoints = _project.getPanel().getNumUnTestableJointsFast(); //XCR1374, KhangWah, 2011-09-12
     _numUnTestableJointsDataLabel.setText(String.valueOf(numUnTestableJoints));
    
     int totalJoints = panel.getNumJoints();
    double testCoveragePct = 0.0;
    if (totalJoints > 0)
      testCoveragePct = ((double)numTestedJoints / (double)totalJoints) * 100.0;
    DecimalFormat df = (DecimalFormat)DecimalFormat.getInstance();
    df.applyPattern(".##");
    _testCoverageDataLabel.setText(df.format(testCoveragePct));

//    _testTimeDataLabel.setText(testTimeInMilliSeconds / 1000 + " " + StringLocalizer.keyToString("MMGUI_SECONDS_KEY"));
    _testTimeDataLabel.setText(StringLocalizer.keyToString("HP_NO_ESTIMATED_TEST_TIME_KEY"));
    _projectGuidancePanel.removeAll();
    VerticalFlowLayout newProjectGuidanceLayout = new VerticalFlowLayout();
    newProjectGuidanceLayout.setVgap(0);
    _projectGuidancePanel.setLayout(newProjectGuidanceLayout);
//    JLabel coverageSentance1 = new JLabel(StringLocalizer.keyToString("MMGUI_COVERAGE_GUIDANCE_STATE_KEY"));//"Coverage Options have not been set up for this project.");
//    coverageSentance1.setFont(FontUtil.getBoldFont(coverageSentance1.getFont());
//    JLabel coverageSentance2 = new JLabel(StringLocalizer.keyToString("MMGUI_COVERAGE_GUIDANCE_ACTION_KEY"));//"Click on \"Coverage Options\" to change coverage.");
//    coverageSentance2.setFont(FontUtil.getBoldFont(coverageSentance1.getFont());
    int totalSubtypes = panel.getNumberOfInspectedSubtypes();
    int shortLearnedSubtypes = panel.getNumberOfInspectedSubtypesWithShortLearned();
    int subtypeLearnedSubtypes = panel.getNumberOfInspectedSubtypesWithAllAlgorithmsExceptShortLearned();

    LocalizedString shortLearnCount = new LocalizedString("MMGUI_HOME_UNLEARNED_SHORT_BACKGROUND_REGIONS_STATE_KEY",
                                                        new Object[]{new Integer(shortLearnedSubtypes),
                                                                     new Integer(totalSubtypes)});
    JLabel shortLearnSentance = new JLabel(StringLocalizer.keyToString(shortLearnCount));

    LocalizedString subtypeLearnCount = new LocalizedString("MMGUI_HOME_UNLEARNED_ALGORITHM_SETTINGS_STATE_KEY",
                                                        new Object[]{new Integer(subtypeLearnedSubtypes),
                                                                     new Integer(totalSubtypes)});
    JLabel subtypeLearnSentance = new JLabel(StringLocalizer.keyToString(subtypeLearnCount));

    updateFocusStatus();

//    if (_testDev.hasCoverageOptionsBeenVisited() == false)
//    {
//      _projectGuidancePanel.add(coverageSentance1);
//      _projectGuidancePanel.add(coverageSentance2);
//      _projectGuidancePanel.add(new JLabel("  "));
//    }
    _projectGuidancePanel.add(subtypeLearnSentance);
    _projectGuidancePanel.add(new JLabel("  "));
    _projectGuidancePanel.add(shortLearnSentance);
    _projectGuidancePanel.add(new JLabel("  "));
    _projectGuidancePanel.add(_fixFocusSentance);

    _projectGuidancePanel.repaint();
//    JButton detailsButton = new JButton(StringLocalizer.keyToString("MMGUI_HOME_DETAILS_KEY"));
//    detailsButton.addActionListener(new java.awt.event.ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        detailsButton_actionPerformed(e);
//      }
//    });
//
//    _projectGuidancePanel.add(detailsButton);

    populateInitialThresholds();
//    populateSystemType();
    
    populateInitialRecipeSetting();

    _projectNotesTextArea.setEditable(true);
    _initialThresholdsComboBox.setEnabled(true);
    _initialRecipeSettingComboBox.setEnabled(true);
    _systemTypeComboBox.setEnabled(true);
    _customerNameTextField.setEditable(true);
    _programmerNameTextField.setEditable(true);

    _projectNotesTextArea.setText(_project.getNotes());
    _customerNameTextField.setText(_project.getTargetCustomerName());
    _programmerNameTextField.setText(_project.getProgrammerName());
  }

  /**
   * @author Andy Mechtenberg
   */
   private void populateInitialThresholds()
  {
    _initialThresholdsComboBox.removeActionListener(_initialThresholdsComboBoxActionListener);
    _initialThresholdsComboBox.removeAllItems();
    for (String setName : InitialThresholds.getInstance().getAllSetNames())
    {
      _initialThresholdsComboBox.addItem(setName);
    }
    _initialThresholdsComboBox.setSelectedItem(_project.getInitialThresholdsSetName());
    _initialThresholdsComboBox.addActionListener(_initialThresholdsComboBoxActionListener);
  }
   
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  private void populateInitialRecipeSetting()
  {
    _initialRecipeSettingComboBox.removeActionListener(_initialRecipeSettingComboBoxActionListener);
    _initialRecipeSettingComboBox.removeAllItems();
    for (String setName : InitialRecipeSetting.getInstance().getAllSetNames())
    {
      _initialRecipeSettingComboBox.addItem(setName);
    }
    _initialRecipeSettingComboBox.setSelectedItem(_project.getInitialRecipeSettingName());
    _initialRecipeSettingComboBox.addActionListener(_initialRecipeSettingComboBoxActionListener);
  }

  /**
   * @author Poh Kheng
   */
   private void populateSystemType()
  {
    _systemTypeComboBox.removeActionListener(_systemTypeComboBoxActionListener);
    _systemTypeComboBox.removeAllItems();
    for (SystemTypeEnum systemTypeEnum : SystemTypeEnum.getAllSystemTypes())
    {
      _systemTypeComboBox.addItem(systemTypeEnum);
    }
    _systemTypeComboBox.setSelectedItem(_project.getSystemType());
    _systemTypeComboBox.addActionListener(_systemTypeComboBoxActionListener);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateProjectVersion()
  {
    _projectVersionDataLabel.setText(new Double(_project.getVersion()).toString());
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateFocusStatus()
  {
    com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
    int numberOfImagesToBeFixedForFocus = panel.getPanelSettings().getUnfocusedRegionSlicesToBeFixed().size();
    LocalizedString fixFocusCount = new LocalizedString("MMGUI_HOME_REGIONS_MARKED_FIX_FOCUS_STATE_KEY",
                                                        new Object[]{new Integer(numberOfImagesToBeFixedForFocus)});
    if(numberOfImagesToBeFixedForFocus > 0)
      _fixFocusSentance.setText(StringLocalizer.keyToString(fixFocusCount));
    else
      _fixFocusSentance.setText(null);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    // nothing to persist, so skip to clearing out data

    _project = null;

    _panelNameDataLabel.setText("");
    _projectVersionDataLabel.setText("");
    _numBoardsDataLabel.setText("");
    _numComponentsDataLabel.setText("");
    _numJointsDataLabel.setText("");
    _numTestedJointsDataLabel.setText("");
    _testCoverageDataLabel.setText("");
    _numUnTestableJointsDataLabel.setText(""); //XCR1374, KhangWah, 2011-09-20

    _testTimeDataLabel.setText("");
    _projectNotesTextArea.setEditable(false);

    _initialThresholdsComboBox.removeActionListener(_initialThresholdsComboBoxActionListener);
    _initialThresholdsComboBox.removeAllItems();
    _initialThresholdsComboBox.setEnabled(false);
    
    _initialRecipeSettingComboBox.removeActionListener(_initialRecipeSettingComboBoxActionListener);
    _initialRecipeSettingComboBox.removeAllItems();
    _initialRecipeSettingComboBox.setEnabled(false);

    _systemTypeComboBox.removeActionListener(_systemTypeComboBoxActionListener);
    _systemTypeComboBox.removeAllItems();
    _systemTypeComboBox.setEnabled(false);

    _customerNameTextField.setEditable(false);
    _programmerNameTextField.setEditable(false);

    _projectNotesTextArea.setText("");
    _customerNameTextField.setText("");
    _programmerNameTextField.setText("");

    _projectGuidancePanel.removeAll();

    _projectGuidancePanel.setLayout(_projectGuidanceFlowLayout);
    _projectGuidancePanel.add(_projectGuidanceLabel, null);

    ProjectObservable.getInstance().deleteObserver(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void populateWithProjectData(Project project)
  {
    Assert.expect(project != null);
    _project = project;
    ProjectObservable.getInstance().addObserver(this);
    if (_active)
    {
      updateDataOnScreen();
      _updateData = false;
    }
    else
    {
      _updateData = true;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void update(Observable observable, final Object argument)
  {
    if (observable instanceof ProjectObservable)
      handleProjectEvent(argument);
    else
      Assert.expect(false);
  }

  /**
   * Handles information regarding a change in datastore.  Typically generated by someone doing an undo for
   * data displayed in this panel
   * @author Andy Mechtenberg
   */
  private void handleProjectEvent(final Object object)
  {
    if (_active == false)
      _updateData = true;
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          Assert.expect(object instanceof ProjectChangeEvent,
                        "Got a non-ProjectChangeEvent in the ProjectObservable: " + object);
          ProjectChangeEvent projectEvent = (ProjectChangeEvent)object;
          ProjectChangeEventEnum eventEnum = projectEvent.getProjectChangeEventEnum();
          if (eventEnum.equals(ProjectEventEnum.UNLOAD))
            return;
          // only 4 things are editable here:
          //   ProjectEventEnum.NOTES
          //   ProjectEventEnum.INITIAL_THRESHOLDS
          //   ProjectEventEnum.TARGET_CUSTOMER_NAME
          //   ProjectEventEnum.PROGRAMMER_NAME
          updateProjectVersion();
          updateFocusStatus();
          if (eventEnum instanceof ProjectEventEnum)
          {
            ProjectEventEnum projectEventEnum = (ProjectEventEnum)eventEnum;
            if (projectEventEnum.equals(ProjectEventEnum.NOTES))
            {
              _projectNotesTextArea.setText(_project.getNotes());
            }
            else if (projectEventEnum.equals(ProjectEventEnum.INITIAL_THESHOLDS))
            {
              _initialThresholdsComboBox.removeActionListener(_initialThresholdsComboBoxActionListener);
              _initialThresholdsComboBox.removeAllItems();
              for (String setName : InitialThresholds.getInstance().getAllSetNames())
              {
                _initialThresholdsComboBox.addItem(setName);
              }
              _initialThresholdsComboBox.setSelectedItem(_project.getInitialThresholdsSetName());
              _initialThresholdsComboBox.addActionListener(_initialThresholdsComboBoxActionListener);
            }
            else if (projectEventEnum.equals(ProjectEventEnum.INITIAL_RECIPE_SETTING))
            {
              _initialRecipeSettingComboBox.removeActionListener(_initialRecipeSettingComboBoxActionListener);
              _initialRecipeSettingComboBox.removeAllItems();
              for (String setName : InitialRecipeSetting.getInstance().getAllSetNames())
              {
                _initialRecipeSettingComboBox.addItem(setName);
              }
              _initialRecipeSettingComboBox.setSelectedItem(_project.getInitialRecipeSettingName());
              _initialRecipeSettingComboBox.addActionListener(_initialRecipeSettingComboBoxActionListener);
            }
            else if (projectEventEnum.equals(ProjectEventEnum.SYSTEM_TYPE))
            {
              _systemTypeComboBox.removeActionListener(_systemTypeComboBoxActionListener);
              _systemTypeComboBox.removeAllItems();
              for (SystemTypeEnum systemTypeEnum : SystemTypeEnum.getAllSystemTypes())
              {
                _systemTypeComboBox.addItem(systemTypeEnum);
              }
              _systemTypeComboBox.setSelectedItem(_project.getSystemType());
              _systemTypeComboBox.addActionListener(_systemTypeComboBoxActionListener);
            }
            else if (projectEventEnum.equals(ProjectEventEnum.TARGET_CUSTOMER_NAME))
            {
              _customerNameTextField.setText(_project.getTargetCustomerName());
            }
            else if (projectEventEnum.equals(ProjectEventEnum.PROGRAMMER_NAME))
            {
              _programmerNameTextField.setText(_project.getProgrammerName());
            }
            else if (projectEventEnum.equals(ProjectEventEnum.NAME))
            {
              _panelNameDataLabel.setText(_project.getName());
            }
          }
        }
      });
    }
  }

  /**
   * Brings up a modal dialog to display the untuned subtypes to the user.  The user is not
   * able to edit or change anything -- it's purely a read-only view into some project data.
   * @author Andy Mechtenberg
   */
//  private void detailsButton_actionPerformed(ActionEvent e)
//  {
//    // bring up dialog with list of all untuned subtypes
//    ProjectDetailsDialog dlg = new ProjectDetailsDialog(_testDev.getMainMenuGui(),
//                                                        StringLocalizer.keyToString("MMGUI_HOME_PROJECT_DETAILS_KEY"),
//                                                        _untunedSubtypes);
//    SwingUtils.centerOnComponent(dlg, _testDev.getMainMenuGui());
//    dlg.setVisible(true);
//    dlg.dispose();
//  }

  /**
   * @author Andy Mechtenberg
   */
  String getProjectNotes()
  {
    return _projectNotesTextArea.getText();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void projectNotesTextArea_focusLost()
  {
    if (_project != null)
    {
//      System.out.println("Setting NOTES");
      try
      {
        ProjectSetNotesCommand setNotesCommand = new ProjectSetNotesCommand(_project, getProjectNotes());
        _commandManager.execute(setNotesCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void initialThresholdsActionPerformed()
  {
    // new intial threhsolds selection:
    String newSetName = (String)_initialThresholdsComboBox.getSelectedItem();
    try
    {
      ProjectSetInitialThresholdsNameCommand setInitialThresholdsCommand = new ProjectSetInitialThresholdsNameCommand(_project, newSetName);
      _commandManager.execute(setInitialThresholdsCommand);

      MessageDialog.showInformationDialog(MainMenuGui.getInstance(),
                                          StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_EXPLAINATION_KEY"),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
              ex,
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
              true);
    }
  }

  /**
   * @author Poh Kheng
   */
  private void systemTypeActionPerformed()
  {
    // new system type selection:
    SystemTypeEnum newSystemType = (SystemTypeEnum)_systemTypeComboBox.getSelectedItem();
    try
    {
      ProjectSetSystemTypeCommand setSystemTypeCommand = new ProjectSetSystemTypeCommand(_project, newSystemType);
      _commandManager.execute(setSystemTypeCommand);

      MessageDialog.showInformationDialog(MainMenuGui.getInstance(),
                                          StringLocalizer.keyToString("MMGUI_SYSTEM_TYPES_DIFFER_WARNING_KEY"),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
              ex,
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
              true);
    }
  }

  /**
   * @author George Booth
   */
  String getCustomerName()
  {
    return _customerNameTextField.getText();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void customerNameTextField_focusLost()
  {
    if (_project != null)
    {
//      System.out.println("Setting CustomerName");
      try
      {
        ProjectSetCustomerNameCommand setCustomerNameCommand = new ProjectSetCustomerNameCommand(_project, getCustomerName());
        _commandManager.execute(setCustomerNameCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
      }
    }
  }

  /**
   * @author George Booth
   */
  String getProgrammerName()
  {
    return _programmerNameTextField.getText();
  }

  /**
   * @author George Booth
   */
  private void programmerNameTextField_focusLost()
  {
    if (_project != null)
    {
//      System.out.println("Setting ProgrammerName");
      try
      {
        ProjectSetProgrammerNameCommand setProgrammerNameCommand = new ProjectSetProgrammerNameCommand(_project, getProgrammerName());
        _commandManager.execute(setProgrammerNameCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
      }
    }
  }

  /**
   * Task is ready to start
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("TestDev:HomePanel().start()");

    com.axi.util.TimerUtil screenTimer = new com.axi.util.TimerUtil();
    screenTimer.start();

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();

    _active = true;

    // add custom menus

    // add custom toolbar components

    _testDev.switchToPanelGraphics();
    _testDev.showCadWindow();

    // update the choices in the Initial Thresholds list -- this could have changed in Config UI
    if (_project != null)
      populateInitialThresholds();
    
    if (_project != null)
      populateInitialRecipeSetting();
    
    if (_updateData)
      updateDataOnScreen();
    _updateData = false;

    screenTimer.stop();
//    System.out.println("  Total Loading for Project Home: (in mils): "+ screenTimer.getElapsedTimeInMillis());
  }

  /**
   * Can the environment be swapped out
   * @return true if environment can be swapped
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
//    System.out.println("TestDev:HomePanel().isReadyToFinish()");
//    if (_customerNameTextField.getText().equals(_project.getTargetCustomerName()) &&
//        _programmerNameTextField.getText().equals(_project.getProgrammerName()) &&
//        _projectNotesTextArea.getText().equals(_project.getNotes()))
//      return true;
//    else
//      return false;
    return true;
  }

  /**
   * The environment is about to be swapped out and should perform cleanup
   * @author George Booth
   */
  public void finish()
  {
//    System.out.println("TestDev:HomePanel().finish()");

    // set all the textual information here incase it wasn't set already.
//    customerNameTextField_focusLost();
//    programmerNameTextField_focusLost();
//    projectNotesTextArea_focusLost();
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();

    _active = false;
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  private void initialRecipeSettingActionPerformed()
  {
    // new intial threhsolds selection:
    String newSetName = (String)_initialRecipeSettingComboBox.getSelectedItem();
    try
    {
      ProjectSetInitialRecipeSettingNameCommand setInitialRecipeSettingCommand = new ProjectSetInitialRecipeSettingNameCommand(_project, newSetName);
      _commandManager.execute(setInitialRecipeSettingCommand);

      MessageDialog.showInformationDialog(MainMenuGui.getInstance(),
                                          StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_EXPLAINATION_KEY"),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
              ex,
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
              true);
    }
  }

}
