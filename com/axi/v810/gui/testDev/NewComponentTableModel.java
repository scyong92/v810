package com.axi.v810.gui.testDev;

import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * Holds the data model for any new components added to the panel
 * @author Laura Cormos
 */
public class NewComponentTableModel extends DefaultSortTableModel
{
  // command manager for undo functionality
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private List<Component> _components = new ArrayList<Component>();
  private static final String _REFDES_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_REF_DES_KEY");
  private static final String _BOARD_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_BOARD_KEY");
  private static final String _XLOC_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_X_LOC_KEY");
  private static final String _YLOC_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_Y_LOC_KEY");
  private static final String _ROTATION_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_ROTATION_KEY");
  private static final String _SIDE_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_SIDE_KEY");
  private final String[] _tableColumns = {_REFDES_COLUMN_HEADING,
                                         _BOARD_COLUMN_HEADING,
                                         _XLOC_COLUMN_HEADING,
                                         _YLOC_COLUMN_HEADING,
                                         _ROTATION_COLUMN_HEADING,
                                         _SIDE_COLUMN_HEADING};
  private MainMenuGui _mainUI;
  private TestDev _testDev = null;
  private EditCadPanel _parent = null;
  private Project _project = null;
  private Set<Component> _selectedComponents = new HashSet<Component>();
  private ComponentComparatorEnum _currentSortOrder = ComponentComparatorEnum.REFERENCE_DESIGNATOR;
  private boolean _sortAscending = true;

  /**
   * author Laura Cormos
   */
  public NewComponentTableModel(TestDev testDev, EditCadPanel parent)
  {
    Assert.expect(testDev != null);
    Assert.expect(parent != null);

    _testDev = testDev;
    _mainUI = _testDev.getMainUI();
    _parent = parent;
  }

  /**
   * @author Laura Cormos
   */
  void addComponent(Component component)
  {
    Assert.expect(component != null);
    Assert.expect(_components != null);

    _components.add(component);
    fireTableDataChanged();
  }

  /**
   * @author Laura Cormos
   * @param component The component to be removed from this table model
   * @return true if the component was found in the list, false otherwise
   */
  boolean deleteComponent(Component component)
  {
    Assert.expect(component != null);
    Assert.expect(_components != null);

    if (_components.remove(component))
    {
      fireTableDataChanged();
      return true;
    }
    return false;
  }

//  /**
//   * @author Laura Cormos
//   */
//  public boolean isSortable(int col)
//  {
//    return false;
//  }

  /**
   * @author Laura Cormos
   */
  public void sortColumn(int column, boolean ascending)
  {
    _sortAscending = ascending;
    switch(column)
    {
    case 0: // name
      _currentSortOrder = ComponentComparatorEnum.REFERENCE_DESIGNATOR;
      Collections.sort(_components, new ComponentComparator(ascending, ComponentComparatorEnum.REFERENCE_DESIGNATOR));
      break;
    case 1: // board name
      _currentSortOrder = ComponentComparatorEnum.BOARD_NAME;
      Collections.sort(_components, new ComponentComparator(ascending, ComponentComparatorEnum.BOARD_NAME));
      break;
    case 2: // x location
      _currentSortOrder = ComponentComparatorEnum.X_COORDINATE;
      Collections.sort(_components, new ComponentComparator(ascending, ComponentComparatorEnum.X_COORDINATE));
      break;
    case 3: // y location
      _currentSortOrder = ComponentComparatorEnum.Y_COORDINATE;
      Collections.sort(_components, new ComponentComparator(ascending, ComponentComparatorEnum.Y_COORDINATE));
      break;
    case 4: // rotation
      _currentSortOrder = ComponentComparatorEnum.DEGREES_ROTATION;
      Collections.sort(_components, new ComponentComparator(ascending, ComponentComparatorEnum.DEGREES_ROTATION));
      break;
    case 5: // board side
      _currentSortOrder = ComponentComparatorEnum.SIDE_BOARD;
      Collections.sort(_components, new ComponentComparator(ascending, ComponentComparatorEnum.SIDE_BOARD));
      break;
    default:
      Assert.expect(false, "Column does not exist in table model");
      break;
    }
  }

  /**
   * @author Laura Cormos
   */
  public int getColumnCount()
  {
    return _tableColumns.length;
  }

  /**
   * @author Laura Cormos
   */
  public int getRowCount()
  {
    if (_components == null)
      return 0;
    else
      return _components.size();
  }

  /**
   * @author Laura Cormos
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return(String)_tableColumns[columnIndex];
  }

  /**
   * @author Laura Cormos
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if (_selectedComponents.contains(_components.get(rowIndex)) == false)
      return false;

    if (columnIndex == NewComponentTable._BOARD_NAME_INDEX) // the board name column is for information purposes only
      return false;
    else
      return true;
  }

  /**
   * @author Laura Cormos
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    Assert.expect(_components != null);

    Component component = _components.get(rowIndex);
    switch(columnIndex)
    {
      case 0: // reference designator
        return component.getReferenceDesignator();
      case 1: // board name
        return component.getBoard().getName();
      case 2: // X location relative to board origin
        int x = component.getCoordinateInNanoMeters().getX();
        double mils = MathUtil.convertUnits(x, MathUtilEnum.NANOMETERS, _project.getDisplayUnits());
        return mils;
      case 3: // Y location relative to board origin
        int y = component.getCoordinateInNanoMeters().getY();
        mils = MathUtil.convertUnits(y, MathUtilEnum.NANOMETERS, _project.getDisplayUnits());
        return mils;
      case 4: // rotation or orientation
        double theta = component.getDegreesRotationRelativeToBoard();
        return new Double(theta);
      case 5: // board side
        if (component.getSideBoard().isSideBoard1())
          return StringLocalizer.keyToString("CAD_SIDE1_KEY");
        else
          return StringLocalizer.keyToString("CAD_SIDE2_KEY");
      default:
        Assert.expect(false);
        return null;
    }

  }

  /**
   * @author Laura Cormos
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    Assert.expect(aValue != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    Assert.expect(_components != null);

    if (rowIndex >= _components.size())
      return;
    Assert.expect(_project != null, "Project not set for new component table model");

    Component component = _components.get(rowIndex);
    // we are modifying component type here so that the changed value is propagated to all components of this type
    ComponentType componentType = component.getComponentType();
    BoardCoordinate coordinateInNanometers = null;
    String valueStr;
    Double value;
    switch(columnIndex)
    {
      case 0: // reference designator
        Assert.expect(aValue instanceof String);
        valueStr = (String)aValue;
        if (valueStr.equals(componentType.getReferenceDesignator()) == false)
        {
          SideBoardType sideBoardType = componentType.getSideBoardType();
          BoardType boardType = sideBoardType.getBoardType();
          if (boardType.isComponentTypeReferenceDesignatorValid(valueStr) == false)
          {
            // pop dialog about invalid name
            String illegalChars = boardType.getComponentTypeReferenceDesignatorIllegalChars();
            LocalizedString message = null;
            if (illegalChars.equals(" "))
              message = new LocalizedString("CAD_INVALID_COMPONENT_NAME_SPACES_KEY", new Object[]{valueStr});
            else
              message = new LocalizedString("CAD_INVALID_COMPONENT_NAME_OTHER_CHARS_KEY", new Object[]{valueStr, illegalChars});
            JOptionPane.showMessageDialog(_mainUI,
                                          StringLocalizer.keyToString(message),
                                          StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                          JOptionPane.INFORMATION_MESSAGE);
            return;
          }
          else if (boardType.isComponentTypeReferenceDesignatorDuplicate(valueStr))
          {
            // pop dialog about duplicate name
            JOptionPane.showMessageDialog(_mainUI,
                                          StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
                                                                                          new Object[]{valueStr})),
                                          StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                          JOptionPane.INFORMATION_MESSAGE);
            return;
          }
          try
          {
            _commandManager.trackState(_parent.getCurrentUndoState());
            _commandManager.execute(new ComponentTypeSetRefDesCommand(componentType, valueStr));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
        }
        break;
      case 1: // board instance name cannot be changed
        break;
      case 2: // X location relative to board origin
        Assert.expect(aValue instanceof Number);
        value = ((Number)aValue).doubleValue();
        double convertedValue = MathUtil.convertUnits(value, _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);

        coordinateInNanometers = component.getCoordinateInNanoMeters();
        coordinateInNanometers.setX((int)convertedValue);
        if (component.getComponentType().willComponentTypeBeInsideBoardTypeOutlineAfterCoordinate(coordinateInNanometers) == false)
//        if (convertedValue < component.getWidthInNanoMeters()/2 ||
//            convertedValue > (component.getBoard().getWidthInNanoMeters() - component.getWidthInNanoMeters()/2))
        {
          int result = JOptionPane.showOptionDialog(_mainUI,
              StringLocalizer.keyToString(new LocalizedString("CAD_X_COORD_OUT_OF_BOUNDS_KEY",
                                                              new Object[]{StringLocalizer.keyToString("CAD_COMPONENT_KEY"),
                                                                           component.getReferenceDesignator(),
                                                                           StringLocalizer.keyToString("CAD_BOARD_KEY")})),
              StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
              JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
              null,
              new Object[]{StringLocalizer.keyToString("GUI_ACCEPT_BUTTON_KEY"), StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")},
              null);

          if (result == JOptionPane.CLOSED_OPTION || result == 1 /** this means user chose the Cancel button */)
              return;
        }

        try
        {
          _commandManager.trackState(_parent.getCurrentUndoState());
          _commandManager.execute(new ComponentTypeSetCoordinateInNanoMetersCommand(componentType, coordinateInNanometers));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      case 3: // Y location relative to board origin
        Assert.expect(aValue instanceof Number);
        value = ((Number)aValue).doubleValue();
        convertedValue = MathUtil.convertUnits(value, _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
        coordinateInNanometers = component.getCoordinateInNanoMeters();
        coordinateInNanometers.setY((int)convertedValue);
        if (component.getComponentType().willComponentTypeBeInsideBoardTypeOutlineAfterCoordinate(coordinateInNanometers) == false)
//        if (convertedValue < component.getLengthInNanoMeters()/2 ||
//            convertedValue > (component.getBoard().getLengthInNanoMeters() - component.getLengthInNanoMeters()/2))
        {
          int result = JOptionPane.showOptionDialog(_mainUI,
              StringLocalizer.keyToString(new LocalizedString("CAD_Y_COORD_OUT_OF_BOUNDS_KEY",
                                                              new Object[]{StringLocalizer.keyToString("CAD_COMPONENT_KEY"),
                                                                           component.getReferenceDesignator(),
                                                                           StringLocalizer.keyToString("CAD_BOARD_KEY")})),
              StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
              JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
              null,
              new Object[]{StringLocalizer.keyToString("GUI_ACCEPT_BUTTON_KEY"), StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")},
              null);

          if (result == JOptionPane.CLOSED_OPTION || result == 1 /** this means user chose the Cancel button */)
              return;
        }

        try
        {
          _commandManager.trackState(_parent.getCurrentUndoState());
          _commandManager.execute(new ComponentTypeSetCoordinateInNanoMetersCommand(componentType, coordinateInNanometers));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      case 4: // rotation
        Assert.expect(aValue instanceof String);
        valueStr = (String)aValue;
        Double doubleValue = new Double(valueStr);
        try
        {
          _commandManager.trackState(_parent.getCurrentUndoState());
          _commandManager.execute(new ComponentTypeSetDegreesRotationCommand(componentType, doubleValue.doubleValue()));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      case 5: // board side
        Assert.expect(aValue instanceof String);
        valueStr = (String)aValue;
        boolean componentIsSOnSide1 = false;
        boolean changeSideOk = false;
        SideBoard componentSideBoard = component.getSideBoard();

        if (componentSideBoard.getBoard().isBoardSingleSided())
        {
          componentSideBoard.getBoard().createSecondSideBoard();
        }

        if (component.getSideBoard().isSideBoard1())
          componentIsSOnSide1 = true;

        if (valueStr.equals(StringLocalizer.keyToString("CAD_SIDE1_KEY")))
        {
          if (componentIsSOnSide1)
            return; // do nothing, component is already on the side the user asked to changed to
          else
          {
            SideBoard otherSideBoard = componentSideBoard.getBoard().getSideBoard1();
            if (otherSideBoard.hasComponent(component.getReferenceDesignator()) == false)
              changeSideOk = true;
          }
        }
        else // user wants to change to side 2
        {
          if (componentIsSOnSide1 == false)
            return; // do nothing, component is already on the side the user asked to changed to
          else
          {
            SideBoard otherSideBoard = componentSideBoard.getBoard().getSideBoard2();
            if (otherSideBoard.hasComponent(component.getReferenceDesignator()) == false)
              changeSideOk = true;
          }
        }
        if (changeSideOk)
        {
          try
          {
            _commandManager.trackState(_parent.getCurrentUndoState());
            _commandManager.execute(new ComponentTypeChangeSideCommand(componentType));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
        }
        else
        {
          // pop dialog about duplicate name
          JOptionPane.showMessageDialog(_mainUI, StringLocalizer.keyToString(new LocalizedString("CAD_COMPONENT_CHANGE_SIDE_DUPLICATE_NAME_KEY",
                                                                                                 new Object[]{component.getReferenceDesignator()})),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
          fireTableCellUpdated(rowIndex, columnIndex);
          return;
        }
        break;
      default:
        Assert.expect(false);
    }
  }

  /**
   * author Laura Cormos
   */
  void clearData()
  {
    _components.clear();
    _selectedComponents.clear();
    fireTableDataChanged();
    _project = null;
  }

  /**
   * author Laura Cormos
   */
  Component getComponentAt(int rowIndex)
  {
    Assert.expect(_components != null);
    Assert.expect(rowIndex >= 0 && rowIndex < _components.size());
    return _components.get(rowIndex);
  }

  /**
   * @author Laura Cormos
   */
  int getRowForComponent(Component component)
  {
    Assert.expect(component != null);
    Assert.expect(_components != null);
    return _components.indexOf(component);
  }

  /**
   * @author Laura Cormos
   */
  void setProject (Project project)
  {
    Assert.expect(project != null);
    _project = project;
  }

  /**
   * @author Laura Cormos
   */
  void setSelectedComponents(Set<Component> selectedComponents)
  {
    Assert.expect(selectedComponents != null);
    _selectedComponents = selectedComponents;
  }

  /**
   * @author Laura Cormos
   */
  public void clearSelectedComponents()
  {
    _selectedComponents.clear();
  }

  /**
   * @author Laura Cormos
   */
  void addComponents(List<Component> componentsToAdd)
  {
    Assert.expect(componentsToAdd != null);
    Assert.expect(_components != null);

    _components.addAll(componentsToAdd);
    Collections.sort(_components, new ComponentComparator(_sortAscending, _currentSortOrder));
    fireTableDataChanged();
  }

  /**
   * @author Ngie Xing Wong
   * Modified on Apr 2014
   * @author Seng Yew Lim
   * Added on 16-Jan-2013
   */
  void addComponentTypes(List<ComponentType> componentTypeList)
  {
    Assert.expect(componentTypeList != null);
    Assert.expect(_components != null);

    List<Component> componentsToAddList = new ArrayList<Component>();

    for(ComponentType componentType : componentTypeList)
    {
      for(Component component : componentType.getComponents())
      {
        if (_components.contains(component) == false)
          componentsToAddList.add(component);
      }
    }
    _components.addAll(componentsToAddList);
    Collections.sort(_components, new ComponentComparator(_sortAscending, _currentSortOrder));
    fireTableDataChanged();
  }
  
  /**
    * @author Laura Cormos
    */
   void deleteComponents(List<Component> componentsToRemove)
   {
     Assert.expect(componentsToRemove != null);
     Assert.expect(_components != null);

     _components.removeAll(componentsToRemove);
     fireTableDataChanged();
  }

  /**
   * @author Seng Yew Lim
   * Added on 16-Jan-2013
   */
  void deleteComponentsForComponentType(List<ComponentType> componentTypeList)
  {
    Assert.expect(componentTypeList != null);

    List<Component> componentsToRemove = new ArrayList<Component>();
    //int currentValue = 0;
    for (Component component : _components)
    {
      ComponentType componentType = component.getComponentType();
      if (componentTypeList.contains(componentType))
        componentsToRemove.add(component);
    }
    _components.removeAll(componentsToRemove);
    fireTableDataChanged();
  }

  /**
   * @author Laura Cormos
   */
  void deleteComponentsForComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);

    List<Component> componentsToRemove = new ArrayList<Component>();
    for (Component component : _components)
    {
      if (component.getComponentType() == componentType)
        componentsToRemove.add(component);
    }
    _components.removeAll(componentsToRemove);
    fireTableDataChanged();
  }

  /**
   * @author Laura Cormos
   */
  void populateWithComponents(List<Component> components)
  {
    Assert.expect(components != null);
    Assert.expect(_components != null);

    _components.addAll(components);
    Collections.sort(_components, new ComponentComparator(_sortAscending, _currentSortOrder));
    fireTableDataChanged();
  }
}
