package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.images.Image5DX;
import com.axi.v810.util.*;


/**
 * @author Khaw Chek Hau
 * POP Development
 */
public class MergeComponentsIntoPOPDialog extends EscapeDialog implements Observer
{
  private JScrollPane _componentScrollPane = new JScrollPane();
  private POPComponentLocationTable _popComponentLocationTable;
  private POPComponentLocationTableModel _popComponentLocationTableModel;
  private JButton _okButton = new JButton();
  private JButton _upButton = new JButton();
  private JButton _downButton = new JButton();
  private MainMenuGui _parentComponent;
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private JPanel _mainPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _tablePanel = new JPanel();
  private JPanel _learnButtonInnerPanel = new JPanel();
  private GridLayout _learnButtonInnerPanelGridLayout = new GridLayout();
  private JPanel _statusPanel;
  private JLabel _statusTextValue;
  private ListSelectionListener _mergeComponentTableListSelectionListener;
  private java.util.List<ComponentType> _componentTypes = null;
  private java.util.List<ComponentType> _selectedComponentTypes = new java.util.ArrayList<ComponentType>();
  private Project _project;
  private static final String _POP_NAME_EXTENSION = "_POP";

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public MergeComponentsIntoPOPDialog(java.util.List<ComponentType> componentType, Project project, Frame parent, String title)
  {
    super(parent, title, true);

    _componentTypes = componentType;
    _project = project;
    _parentComponent = (MainMenuGui)parent;
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    
    jbInit();
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void jbInit()
  {
    _mergeComponentTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        componentTypeTableListSelectionChanged(e);
      }
    };

    _popComponentLocationTableModel = new POPComponentLocationTableModel();     
    _popComponentLocationTableModel.populateComponentTypesWithPanelData(_componentTypes);
     
    String popPackageName = null;
    ComponentType componentType = null;
    CompPackageOnPackage compPackageOnPackage = new CompPackageOnPackage();
    
    for (int row = 0; row < _componentTypes.size(); row++)
    {
      componentType = _componentTypes.get(row);

      if (row == 0)
      {
        popPackageName = componentType.getReferenceDesignator() + _POP_NAME_EXTENSION;
        compPackageOnPackage.setPanel(_project.getPanel());
        compPackageOnPackage.setName(popPackageName);
      }

      POPLayerIdEnum popLayerIdEnum = POPLayerIdEnum.getLayerIdToEnumMapValue(row);
      _popComponentLocationTableModel.setValueAt(popLayerIdEnum,row,1);
      _popComponentLocationTableModel.setValueAt(compPackageOnPackage,row,2);
      compPackageOnPackage.addComponentType(componentType);
    }

    ProjectObservable.getInstance().addObserver(this);
     
    _popComponentLocationTable = new POPComponentLocationTable(_popComponentLocationTableModel)
    {
      public void mouseReleased(MouseEvent event)
      {          
        int selectedRows[] = _popComponentLocationTable.getSelectedRows();
        java.util.List<Object> selectedData = new ArrayList<Object>();
        for(int i = 0; i < selectedRows.length; i++)
        {
          selectedData.add(_popComponentLocationTableModel.getComponentTypeAt(selectedRows[i]));
        }
        ListSelectionModel rowSM = _popComponentLocationTable.getSelectionModel();
        rowSM.removeListSelectionListener(_mergeComponentTableListSelectionListener); // add the listener for a details table row selection

        super.mouseReleased(event);

        _popComponentLocationTable.clearSelection();

        Iterator it = selectedData.iterator();
        while (it.hasNext())
        {
          int row;
          Object obj = it.next();
          row = _popComponentLocationTableModel.getRowForComponentType((ComponentType)obj);
          _popComponentLocationTable.addRowSelectionInterval(row, row);
        }
        SwingUtils.scrollTableToShowSelection(_popComponentLocationTable);
        rowSM.addListSelectionListener(_mergeComponentTableListSelectionListener); // add the listener for a details table row selection
      }
    };
          
    _popComponentLocationTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);     
    _popComponentLocationTable.setSurrendersFocusOnKeystroke(false);
    _popComponentLocationTable.setPreferredColumnWidths();
    _popComponentLocationTable.setupCellEditorsAndRenderers();
    _popComponentLocationTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        multiEditTableMouseReleased(e);
      }
    });

    ListSelectionModel rowSM = _popComponentLocationTable.getSelectionModel();
    rowSM.addListSelectionListener(_mergeComponentTableListSelectionListener);

    _tablePanel.setLayout(new BorderLayout());
    _tablePanel.add(_componentScrollPane, BorderLayout.CENTER);

    _learnButtonInnerPanel.setLayout(_learnButtonInnerPanelGridLayout);
    _learnButtonInnerPanelGridLayout.setColumns(1);
    _learnButtonInnerPanelGridLayout.setRows(10);
    _learnButtonInnerPanelGridLayout.setVgap(2);

    _upButton =  new JButton(StringLocalizer.keyToString("PLWK_MOVE_UP_KEY"), Image5DX.getImageIcon(Image5DX.VC_UP_ARROW));
    _downButton =  new JButton(StringLocalizer.keyToString("PLWK_MOVE_DOWN_KEY"), Image5DX.getImageIcon(Image5DX.VC_DOWN_ARROW));

    _learnButtonInnerPanel.add(_upButton);
    _learnButtonInnerPanel.add(_downButton);

    _tablePanel.add(_learnButtonInnerPanel, BorderLayout.EAST);

    _upButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        upButton_actionPerformed(e);
      }
    });
     
    _downButton.addActionListener(new java.awt.event.ActionListener()
    {
     public void actionPerformed(ActionEvent e)
     {
       downButton_actionPerformed(e);
     }
    });

    _statusPanel = new JPanel();
    _statusPanel.setLayout(new BorderLayout());

    _buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    _buttonPanel.add(_okButton);
    _okButton.setText(StringLocalizer.keyToString("MMGUI_SELECT_SUBTYPE_BUTTON_KEY"));

    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
     
    JButton close = new JButton(StringLocalizer.keyToString("MMGUI_START_UP_CLOSE_MESSAGE_KEY"));  

    close.addActionListener(new ActionListener() 
    {
      public void actionPerformed(ActionEvent e) 
      {
        closeWindow();
      }
    });
     
    setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    add(close);

    addWindowListener(new WindowAdapter() 
    {
      public void windowClosing(WindowEvent e) 
      {
        closeWindow();
      }
    });

    setupMenuItem();

    String workingUnit = StringLocalizer.keyToString(new LocalizedString("MMGUI_SETUP_UNITS_STATUS_KEY", new Object[]{_project.getDisplayUnits().toString()})) + "   ";
    _statusTextValue = new JLabel();
    _statusTextValue.setText(workingUnit);
    _statusTextValue.setFont(FontUtil.getBoldFont(_statusTextValue.getFont(), Localization.getLocale()));
    _statusPanel.add(_buttonPanel, BorderLayout.WEST);
    _statusPanel.add(_statusTextValue, BorderLayout.EAST);     
    _mainPanel.setLayout(new BorderLayout());
    _mainPanel.add(_tablePanel, BorderLayout.CENTER);
    _mainPanel.add(_statusPanel, BorderLayout.SOUTH);
    this.getContentPane().add(_mainPanel);
    _popComponentLocationTable.setModel(_popComponentLocationTableModel);
    _componentScrollPane.getViewport().add(_popComponentLocationTable, null);

    pack();
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void closeWindow() 
  {
    _componentTypes.get(0).getCompPackageOnPackage().destroy();
    unpopulate();
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void componentTypeTableListSelectionChanged(ListSelectionEvent e)
  {
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();

    if (lsm.isSelectionEmpty() || lsm.getValueIsAdjusting())
    {
      // do nothing
      // Make sure cell editor is not activated on first click.
      if(_popComponentLocationTable.getCellEditor() != null)
        _popComponentLocationTable.getCellEditor().cancelCellEditing();
    }
    else
    {
      // turn off event processing until graphics had a chance to update
      _popComponentLocationTable.processSelections(false);
      int[] selectedComponentTypeRows = _popComponentLocationTable.getSelectedRows();     
      _selectedComponentTypes.clear();
      for(int i = 0; i < selectedComponentTypeRows.length; i++)
      {
        _selectedComponentTypes.add(_popComponentLocationTableModel.getComponentTypeAt(selectedComponentTypeRows[i]));
      }
              
      _popComponentLocationTableModel.setSelectedComponentTypes(_selectedComponentTypes);

     // wait for graphics
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          _popComponentLocationTable.processSelections(true);
        }
      });
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void multiEditTableMouseReleased(MouseEvent e)
  {
    Assert.expect(e != null);

    // ignore any other mouse events than a popup trigger -- we don't want to break the normal L&F
    if (e.isPopupTrigger())
    {
      // get the table cell that the mouse was right-clicked in
      Point p = e.getPoint();
      int row = _popComponentLocationTable.rowAtPoint(p);
      int col = _popComponentLocationTable.columnAtPoint(p);

      // if this cell is not currently selected, exit.  It's confusing to edit a cell that not selected.
      // we could "select" this cell too, but for now, we'll just not.
      if (_popComponentLocationTable.isCellSelected(row, col) == false)
        return;

      // start the editing process for this cell
      _popComponentLocationTable.editCellAt(row, col);
      // if this is a combo box cell editor, the editCellAt doesn't show the combo box popup, so let's show that
      // only if this is a combo box editor
      Object obj = _popComponentLocationTable.getEditorComponent();
      if (obj instanceof JComboBox)
      {
        JComboBox comboBox = (JComboBox)obj;
        comboBox.showPopup();
      }
    }
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void setupMenuItem()
  {
    JPanel menuPanel = new JPanel(new BorderLayout());
    menuPanel.setBorder(BorderFactory.createLineBorder(Color.black));
    JMenuBar menuBar = new JMenuBar();
    JMenu menu = new JMenu(StringLocalizer.keyToString("GUI_EDIT_MENU_KEY"));
    menuBar.add(menu);
    menuPanel.add(menuBar, BorderLayout.WEST);
    _mainPanel.add(menuPanel, BorderLayout.NORTH);
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void upButton_actionPerformed(ActionEvent e)
  {
    Collection<ComponentType> componentTypes = _popComponentLocationTableModel.getSelectedComponentTypes();

    if (componentTypes.size() != 1)
    {
      MessageDialog.showErrorDialog(this,
                                    StringLocalizer.keyToString("CAD_MUST_SELECT_ONLY_ONE_COMPONENT_FOR_MOVING_UP_DOWN_KEY"),  
                                    StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                    true);
      return;
    }

    int[] selectedComponentTypeRows = _popComponentLocationTable.getSelectedRows();         
    int currentRow = selectedComponentTypeRows[0];
    int layerIdColumnIndex = 1;
    int compPackageOnPackageColumnIndex = 2;
    
    if (currentRow == 0)
      return;
    
    Object c = _popComponentLocationTableModel.getValueAt(currentRow, layerIdColumnIndex);
    Object p = _popComponentLocationTableModel.getValueAt(currentRow-1, layerIdColumnIndex);

    _popComponentLocationTableModel.setValueAt(p, currentRow, layerIdColumnIndex);
    _popComponentLocationTableModel.setValueAt(c, currentRow-1, layerIdColumnIndex);
    _popComponentLocationTableModel.sortUpward();
    
    CompPackageOnPackage compPackageOnPackage = _componentTypes.get(0).getCompPackageOnPackage();
    compPackageOnPackage.setName(_componentTypes.get(0).getReferenceDesignator());
            
    for (ComponentType componentType: _componentTypes)
    {
      componentType.setCompPackageOnPackage(compPackageOnPackage);
      _popComponentLocationTableModel.setValueAt(compPackageOnPackage, _popComponentLocationTableModel.getRowForComponentType(componentType), compPackageOnPackageColumnIndex);
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void downButton_actionPerformed(ActionEvent e)
  {
    Collection<ComponentType> componentTypes = _popComponentLocationTableModel.getSelectedComponentTypes();

    if (componentTypes.size() != 1)
    {
      MessageDialog.showErrorDialog(this,
                                    StringLocalizer.keyToString("CAD_MUST_SELECT_ONLY_ONE_COMPONENT_FOR_MOVING_UP_DOWN_KEY"),  
                                    StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                    true);
      return;
    }
    
    int[] selectedComponentTypeRows = _popComponentLocationTable.getSelectedRows();         
    int currentRow = selectedComponentTypeRows[0];
    int layerIdColumnIndex = 1;
    int compPackageOnPackageColumnIndex = 2;
    
    if (currentRow == _componentTypes.size()-1)
      return;
    
    Object c = _popComponentLocationTableModel.getValueAt(currentRow, layerIdColumnIndex);
    Object p = _popComponentLocationTableModel.getValueAt(currentRow+1, layerIdColumnIndex);

    _popComponentLocationTableModel.setValueAt(p, currentRow, layerIdColumnIndex);
    _popComponentLocationTableModel.setValueAt(c, currentRow+1, layerIdColumnIndex);    
    _popComponentLocationTableModel.sortUpward();
    
    CompPackageOnPackage compPackageOnPackage = _componentTypes.get(0).getCompPackageOnPackage();
    compPackageOnPackage.setName(_componentTypes.get(0).getReferenceDesignator());
                
    for (ComponentType componentType: _componentTypes)
    {
      componentType.setCompPackageOnPackage(compPackageOnPackage);
      _popComponentLocationTableModel.setValueAt(compPackageOnPackage, 
                                                 _popComponentLocationTableModel.getRowForComponentType(componentType), 
                                                 compPackageOnPackageColumnIndex);
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    if (_popComponentLocationTable.isEditing())
      _popComponentLocationTable.getCellEditor().stopCellEditing();
    
    int rowIndex = 0;
    int HighestZHeight = 0;
    for (rowIndex = 0; rowIndex < _componentTypes.size(); rowIndex++)
    {
      ComponentType componentType = _popComponentLocationTableModel.getComponentTypeAt(rowIndex); 
      if (rowIndex == 0)
        HighestZHeight = componentType.getPOPZHeightInNanometers();  
      else if (componentType.getPOPZHeightInNanometers() > HighestZHeight)
        HighestZHeight = componentType.getPOPZHeightInNanometers();
      else
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      StringLocalizer.keyToString(new LocalizedString("CAD_INVALID_POP_ZHEIGHT_ERROR_MESSAGE_KEY", 
                                                                  new Object[]{componentType.getReferenceDesignator(), 
                                                                  _popComponentLocationTableModel.getComponentTypeAt(rowIndex-1).getReferenceDesignator()})),
                                      StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
                                      true);
        return;
      }
    }
    
    java.util.List<Subtype> subtypes = new ArrayList<Subtype>();
    boolean isComponentTypeInLowestIL = false;

    for (ComponentType compType: _componentTypes)
    {        
      SignalCompensationEnum signalComp;

      for(Subtype subtype : compType.getSubtypes())
      {
        signalComp = subtype.getSubtypeAdvanceSettings().getSignalCompensation();

        if (signalComp == SignalCompensationEnum.DEFAULT_LOW)  
        {
          if (subtypes.contains(subtype) == false)
            subtypes.add(subtype);
          
          isComponentTypeInLowestIL = true;
        }
      }
    }

    if (isComponentTypeInLowestIL)
    {
      int response = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                                                    StringLocalizer.keyToString(new LocalizedString("CAD_POP_SUBTYPE_RECOMMENDED_IL2_AND_ABOVE_MESSAGE_KEY", 
                                                                                new Object[]{subtypes.toString()})),
                                                    StringLocalizer.keyToString("CAD_POP_SUBTYPE_MUST_IL2_AND_ABOVE_TITLE_KEY"),
                                                    JOptionPane.YES_NO_OPTION);
      if (response == JOptionPane.YES_OPTION)
      {
        for (Subtype subtype: subtypes)
        {     
          subtype.getSubtypeAdvanceSettings().setSignalCompensation(SignalCompensationEnum.MEDIUM);
        }
      }
    }
 
    String popName = _componentTypes.get(0).getCompPackageOnPackage().getPOPName();
    
    HashMap<ComponentType, POPLayerIdEnum> componentTypeToPOPLayerIdEnumMap = new HashMap<ComponentType, POPLayerIdEnum>();
    HashMap<ComponentType, Integer> componentTypeToPOPZHeightInNanometersMap = new HashMap<ComponentType, Integer>();
    java.util.List<ComponentType> compPackageOnPackageTotalComponentTypes = new ArrayList<ComponentType>();
    compPackageOnPackageTotalComponentTypes.addAll(_componentTypes);
    
    for (ComponentType componentType : compPackageOnPackageTotalComponentTypes)
    {
      componentTypeToPOPLayerIdEnumMap.put(componentType, componentType.getPOPLayerId());
      componentTypeToPOPZHeightInNanometersMap.put(componentType, componentType.getPOPZHeightInNanometers());
    }
    
    _componentTypes.get(0).getCompPackageOnPackage().destroy();
    
    CompPackageOnPackage compPackageOnPackage = new CompPackageOnPackage();
    compPackageOnPackage.setPanel(_project.getPanel());
    compPackageOnPackage.setName(popName);

    try
    {
      _commandManager.execute(new CreateCompPackageOnPackageCommand(compPackageOnPackageTotalComponentTypes.get(0).getSideBoardType(),
                                                                    compPackageOnPackageTotalComponentTypes.get(0).getReferenceDesignator(),
                                                                    compPackageOnPackage,
                                                                    compPackageOnPackageTotalComponentTypes,
                                                                    componentTypeToPOPLayerIdEnumMap,
                                                                    componentTypeToPOPZHeightInNanometersMap));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(this, ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
    }
    
    unpopulate();
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void unpopulate()
  {
    _popComponentLocationTableModel.clear();

    ProjectObservable.getInstance().deleteObserver(this);
    dispose();
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  void setProject (Project project)
  {
    Assert.expect(project != null);
    _project = project;
    _popComponentLocationTableModel.setProject(project);
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProjectObservable)
        {
          ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent) object;
          ProjectChangeEventEnum eventEnum = projectChangeEvent.getProjectChangeEventEnum();

          if (eventEnum instanceof ComponentTypeEventEnum)
          {
            _popComponentLocationTableModel.populateComponentTypesWithPanelData(_componentTypes);
            _popComponentLocationTableModel.fireTableDataChanged();

            if (_selectedComponentTypes.size() > 0)
            {
              //now reselect the previouse selected row
              for (ComponentType componentType : _selectedComponentTypes)
              {
                int rowPosition = _popComponentLocationTableModel.getRowForComponentType(componentType);
                if (rowPosition != -1) // -1 measn it does not exist in the table.  This can happen with more than one board type
                {
                  _popComponentLocationTable.addRowSelectionInterval(rowPosition, rowPosition);
                }
              }
            }
          }
        }
      }
    });
  }
}
