package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * @author Khaw Chek Hau
 * POP Development
 */
public class POPComponentLocationTableModel extends DefaultSortTableModel
{
  private List<ComponentType> _componentTypes = new ArrayList<ComponentType>();
  private Collection<ComponentType> _currentlySelectedComponentTypes = new ArrayList<ComponentType>();
  private static ComponentTypeComparator _componentTypeComparator;
  
  // side choices
  private final String _TOP_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_TOP_LABEL_KEY");
  private final String _BOTTOM_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_BOTTOM_LABEL_KEY");
  
  static
  {
    _componentTypeComparator = new ComponentTypeComparator(true, ComponentTypeComparatorEnum.REFERENCE_DESIGNATOR);
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private final String[] _mergeComponentTableColumns =
  {
    StringLocalizer.keyToString("MMGUI_SETUP_REF_DES_KEY"),
    StringLocalizer.keyToString("MMGUI_SETUP_LAYER_NUMBER_KEY"),
    StringLocalizer.keyToString("MMGUI_SETUP_POP_NAME_KEY"),
    StringLocalizer.keyToString("MMGUI_SETUP_POP_ZHEIGHT_KEY")  
  };
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private Project _project;
  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;
  private MathUtilEnum _mathSelectedUnits = MathUtilEnum.MILLIMETERS;

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public POPComponentLocationTableModel()
  {
    // do nothing
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  void clear()
  {
    _componentTypes.clear();
    _currentlySelectedComponentTypes.clear();
    _currentSortColumn = 0;
    _currentSortAscending = true;
    fireTableDataChanged();    
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  void populateComponentTypesWithPanelData(List<ComponentType> componentTypes)
  {
    if (componentTypes == null)
      return;
    
    _componentTypes = componentTypes;

    // components may have been deleted, so if they were previously selected, we need to remove them.
    Iterator<ComponentType> it = _currentlySelectedComponentTypes.iterator();
    while(it.hasNext())
    {
      if (_componentTypes.contains(it.next()) == false)
        it.remove();
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void populateWithPanelData(Board board, BoardType boardType, String side)
  {
    Assert.expect(board != null);
    Assert.expect(boardType != null);
    Assert.expect(side != null);

    _componentTypes = getBoardSideComponentTypes(board, boardType, side);

    sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableDataChanged();
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void populateWithPOPData(Board board, BoardType boardType, String side, CompPackageOnPackage selectedPOP)
  {
    Assert.expect(board != null);
    Assert.expect(boardType != null);
    Assert.expect(side != null);
    Assert.expect(selectedPOP != null);
    
    // get component types for specified board, boardType and side
    List<ComponentType> selectedComponentTypes = getBoardSideComponentTypes(board, boardType, side);
    _componentTypes = new ArrayList<ComponentType>();

    // get those matching selectedPackage
    for (ComponentType componentType : selectedComponentTypes)
    {
      if (componentType.hasCompPackageOnPackage())
      {
        if (componentType.getCompPackageOnPackage().equals(selectedPOP))
          _componentTypes.add(componentType);
      }
    }

    Collections.sort(_componentTypes, new ComponentTypeComparator(true, ComponentTypeComparatorEnum.LAYER_ID));

    sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableStructureChanged();
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public List<ComponentType> getBoardSideComponentTypes(Board board, BoardType boardType, String side)
  {
    Assert.expect(board != null);
    Assert.expect(boardType != null);
    Assert.expect(side != null);

    List<ComponentType> componentTypes = null;
    if (side.equalsIgnoreCase(_TOP_SIDE))
    {
      if (board.topSideBoardExists())
      {
        SideBoardType sideBoard = board.getTopSideBoard().getSideBoardType();
        componentTypes = sideBoard.getComponentTypes();
      }
    }
    else if (side.equalsIgnoreCase(_BOTTOM_SIDE))
    {
      if (board.bottomSideBoardExists())
      {
        SideBoardType sideBoard = board.getBottomSideBoard().getSideBoardType();
        componentTypes = sideBoard.getComponentTypes();
      }
    }
    else
    {
       componentTypes =  boardType.getComponentTypes();
    }

    if (componentTypes != null)
    {
      return componentTypes;
    }
    else // return empty list
    {
      return new ArrayList<ComponentType>();
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  void updateSpecificComponentType(ComponentType componentType)
  {
    int componentTypeRow = _componentTypes.indexOf(componentType);
    fireTableRowsUpdated(componentTypeRow, componentTypeRow);
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  int getRowForComponentType(ComponentType componentType)
  {
    return _componentTypes.lastIndexOf(componentType);
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  void setSelectedUnits(MathUtilEnum mathSelectedUnits)
  {
    _mathSelectedUnits = mathSelectedUnits;
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void sortColumn(int column, boolean ascending)
  {
    //Don't sort column as it is sort based on layerNumber
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void sortUpward()
  {
    _componentTypeComparator.setAscending(true);
    _componentTypeComparator.setComponentTypeComparatorEnum(ComponentTypeComparatorEnum.LAYER_ID);
    Collections.sort(_componentTypes, _componentTypeComparator);
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public int getColumnCount()
  {    
    return _mergeComponentTableColumns.length;
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public int getRowCount()
  {
    if(_componentTypes == null)
        return 0;
    else
      return _componentTypes.size();
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public Class getColumnClass(int c)
  {
    return getValueAt(0, c).getClass();
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());

    return (String)_mergeComponentTableColumns[columnIndex];
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    // column 0 is component type - always not editable
    // column 1 is layerId - always not editable
    // column 2 is POP Id - always not editable
    // column 3 is ZHeight POP - always editable

    if (columnIndex == POPComponentLocationTable._COMPONENT_TYPE_INDEX)
      return false;
    else if (columnIndex == POPComponentLocationTable._LAYER_ID_INDEX)
      return false;
    else if (columnIndex == POPComponentLocationTable._POP_ID_INDEX)
      return false;
    else if (columnIndex == POPComponentLocationTable._ZHEIGHT_POP_INDEX)
      return true;
    
    return false;
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    ComponentType componentType = getComponentTypeAt(rowIndex);

    if(columnIndex == 0) // component type
    {
      return componentType.getReferenceDesignator();
    }
    else if (columnIndex == 1) // layer id
    {
      return componentType.getPOPLayerId();
    }
    else if (columnIndex == 2) // POP id
    {        
      return componentType.getCompPackageOnPackage();
    }
    else if (columnIndex == 3) // Zheight POP
    {
      int zHeightInNanoMeters = componentType.getPOPZHeightInNanometers();

      double ZHeightPOPInDisplayUnit = (double)MathUtil.convertUnits(zHeightInNanoMeters, MathUtilEnum.NANOMETERS, _project.getDisplayUnits());   
      
      return ZHeightPOPInDisplayUnit;
    }

    return null;
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    if (columnIndex == 0)
    {
      //do nothing
    }
    else if (columnIndex == 1)
    {
      Assert.expect(aValue instanceof POPLayerIdEnum);
      ComponentType componentType = getComponentTypeAt(rowIndex);
      componentType.setPOPLayerId((POPLayerIdEnum) (aValue));
    }
    else if (columnIndex == 2)
    {
      Assert.expect(aValue instanceof CompPackageOnPackage);
      ComponentType componentType = getComponentTypeAt(rowIndex);
      componentType.setCompPackageOnPackage((CompPackageOnPackage) (aValue));
    }
    else if (columnIndex == 3)
    {
      Assert.expect(aValue instanceof Number);
      Assert.expect(_project != null);
      
      Double value = ((Number)aValue).doubleValue();
      // convert value in display units to nanometers
      int newZHeightPOP = (int)MathUtil.convertUnits(value, _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);      

      ComponentType currentComponentType = getComponentTypeAt(rowIndex);
      
      for (ComponentType componentType : _componentTypes)
      {
        if (componentType == currentComponentType)
          continue;
      
        if (componentType.getPOPZHeightInNanometers() != 0)
        {
          if (rowIndex < getRowForComponentType(componentType) && newZHeightPOP >= componentType.getPOPZHeightInNanometers())
          {            
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          StringLocalizer.keyToString(new LocalizedString("CAD_INVALID_POP_ZHEIGHT_ERROR_MESSAGE_KEY", 
                                                                      new Object[]{componentType.getReferenceDesignator(), 
                                                                      currentComponentType.getReferenceDesignator()})),
                                          StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
                                          true);

            return;
          }
          else if (rowIndex > getRowForComponentType(componentType) && newZHeightPOP <= componentType.getPOPZHeightInNanometers())
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          StringLocalizer.keyToString(new LocalizedString("CAD_INVALID_POP_ZHEIGHT_ERROR_MESSAGE_KEY", 
                                                                      new Object[]{currentComponentType.getReferenceDesignator(), 
                                                                      componentType.getReferenceDesignator()})),
                                          StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
                                          true);
            
            return;
          }
        }
      }
      
      try
      {
        _commandManager.execute(new ComponentTypeSetPOPZHeightInNanometersCommand(currentComponentType, newZHeightPOP));
      }
      catch (XrayTesterException xte)
      {
        displayXrayTesterException(xte);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    else
      Assert.expect(false);
  }
    
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  void setSelectedComponentTypes(Collection<ComponentType> componentTypes)
  {
    Assert.expect(componentTypes != null);
    _currentlySelectedComponentTypes = componentTypes;
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  Collection<ComponentType> getSelectedComponentTypes()
  {
    Assert.expect(_currentlySelectedComponentTypes != null);
    return _currentlySelectedComponentTypes;
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  ComponentType getComponentTypeAt(int row)
  {
    return _componentTypes.get(row);
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  List<ComponentType> getComponentTypes()
  {
    return _componentTypes;
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  POPLayerIdEnum getLayerIdEnumAt(int row)
  {
    return _componentTypes.get(row).getPOPLayerId();
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  int getPOPZHeightInNanometersAt(int row)
  {
    return _componentTypes.get(row).getPOPZHeightInNanometers();
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  void setProject (Project project)
  {
    Assert.expect(project != null);
    _project = project;
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void clearSelectedComponent()
  {
    _currentlySelectedComponentTypes.clear();
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  void displayXrayTesterException(XrayTesterException xte)
  {
    MessageDialog.showErrorDialog(
        MainMenuGui.getInstance(),
        xte.getLocalizedMessage(),
        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
        true);
  }
}
