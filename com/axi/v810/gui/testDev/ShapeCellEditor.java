package com.axi.v810.gui.testDev;

import javax.swing.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: ShapeCellEditor</p>
 * <p>Description: Table cell editor for pad shape values (Circle, Rect)</p>
 * <p>Company:  Agilent Technogies</p>
 * @author Laura Cormos
 * @version 1.0
 */
class ShapeCellEditor extends DefaultCellEditor
{
  //Shape strings
  private final String _CIRCLE = StringLocalizer.keyToString("CAD_CIRCLE_SHAPE_KEY");
  private final String _RECTANGLE = StringLocalizer.keyToString("CAD_RECT_SHAPE_KEY");

  JComboBox _comboBox = null;

  /**
   * @author Laura Cormos
   * Constructor.
   * Initializes the drop down box with the shape values
   */
  public ShapeCellEditor()
  {
    super(new JComboBox());
    _comboBox = (JComboBox)getComponent();

    _comboBox.addItem(_CIRCLE);
    _comboBox.addItem(_RECTANGLE);
  }
}
