package com.axi.v810.gui.testDev;

import java.util.*;
import java.util.logging.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 *
 * @author Kok Chun, Tan
 * @edited By Kee Chin Seong
 */
public class VariationSettingTableModel extends AbstractTableModel
{
  private MainMenuGui _mainUI;
  //String labels retrieved from the localization properties file
  private static final String VARIATION_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_VARIATION_KEY");
  private static final String ENABLE_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_ENABLE_KEY");
  private static final String VIEWABLE_IN_PRODUCTION_LABEL = StringLocalizer.keyToString("MMGUI_ENABLE_CHOICE_IN_PRODUCTION_KEY");

  private String[] _columnLabels = { VARIATION_LABEL,
                                     ENABLE_LABEL,
                                     VIEWABLE_IN_PRODUCTION_LABEL };

  private Project _project = null;
  private static com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  
  private List<VariationSetting> _variationList = new ArrayList<VariationSetting> ();
  private List<VariationSetting> _selectedVariations = null;
  private List<VariationSetting> _viewableVariations = new ArrayList<VariationSetting> ();
  
  private SwingWorkerThread _busyDialogWorkerThread = SwingWorkerThread.getInstance();

  /**
   * Constructor.
   * @author Kok Chun, Tan
   */
  public VariationSettingTableModel()
  {
    _mainUI = MainMenuGui.getInstance();
    // do nothing
  }

  /**
   * @author Kok Chun, Tan
   */
  void clearProjectData()
  {
    _project = null;
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Kok Chun, Tan
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @param column Index of the column
   * @return Column label for the column specified
   * @author Kok Chun, Tan
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @param columnIndex Index of the column
   * @return Class data type for the column specified
   * @author Kok Chun, Tan
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * Overridden method.  Gets the values from the project object.
   * @param rowIndex Index of the row
   * @param columnIndex Index of the column
   * @return Object containing the value
   * @author Kok Chun, Tan
   * @edited By Kee Chin Seong
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_project != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    //get the appropriate data item that corresponds to the column
    switch(columnIndex)
    {
      case VariationSettingTable._VARIATION_INDEX:
      {
        value = _variationList.get(rowIndex).getName();
        break;
      }
      case VariationSettingTable._ENABLE_INDEX:
      {
        value = _variationList.get(rowIndex).isEnable();
        break;
      }
      case VariationSettingTable._VIEWABLE_IN_PRODUCTION_INDEX:
      {
        value = _variationList.get(rowIndex).isViewableInProduction();
        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Kok Chun, Tan
   */
  public int getRowCount()
  {
    if (_variationList != null)
    {
      return _variationList.size();
    }
    return 0;
  }

  /**
   * Overridden method.
   * @param rowIndex  Index of the row
   * @param columnIndex  Index of the column
   * @return boolean - whether the cell is editable
   * @author Kok Chun, Tan
   * @edited By Kee Chin Seong
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    boolean isEditable = false;
    
    if ((_selectedVariations == null) || (_selectedVariations.contains(_variationList.get(rowIndex)) == false))
      return false;

    switch(columnIndex)
    {
      case VariationSettingTable._VARIATION_INDEX:
      {
        isEditable = true;
        break;
      }
      case VariationSettingTable._ENABLE_INDEX:
      {
        isEditable = true;
        break;
      }
      case VariationSettingTable._VIEWABLE_IN_PRODUCTION_INDEX:
      {
        isEditable = true;
        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }

    return isEditable;
  }

  /**
   * Initialize the table with data from a new project
   * @param project Project data
   * @author Kok Chun, Tan
   */
  public void setData(Project project)
  {
    Assert.expect(project != null);
    
    _project = project;
    _variationList = _project.getPanel().getPanelSettings().getAllVariationSettings();
    fireTableDataChanged();
  }

  /**
   * Overridden method.  Saves the data entered by the user in the table
   * to the project object.
   * @param object Object containing the data to be saved to the model
   * @param rowIndex Index of the row
   * @param columnIndex Index of the column
   * @author Kok Chun, Tan
   */
  public void setValueAt(final Object object,final int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    switch(columnIndex)
    {
      case VariationSettingTable._VARIATION_INDEX:
      {
        String name = object.toString();

        // same name as previous, return
        if (_variationList.get(rowIndex).getName().equals(name))
        {
          return;
        }
        
        if (name.isEmpty())
        {
          JOptionPane.showMessageDialog(_mainUI, StringLocalizer.keyToString("AVD_NAME_NOT_SPECIFIED_KEY"));
          return;
        }
        
        boolean isVariationNameValid = false;
        if (name.trim().length() > 0)
        {
          isVariationNameValid = name.matches("\\w*");
        }
        
        // check whether have same name or not
        List<String> variationNames = VariationSettingManager.getInstance().getAllVariationNames();
        if (variationNames.contains(name.trim()))
        {
          isVariationNameValid = false;
        }

        if (isVariationNameValid == false)
        {
          LocalizedString message = new LocalizedString("AVD_NAME_NOT_VALID_KEY", new Object[]
          {
          });
          JOptionPane.showMessageDialog(_mainUI, StringUtil.format(StringLocalizer.keyToString(message), 50));
          return;
        }
        
        try
        {
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SN_RENAME_VARIATION_NAME_KEY"));
          _commandManager.execute(new ConfigSetVariationNameCommand(_variationList.get(rowIndex),
                                                                    name));
          _commandManager.endCommandBlock();
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
        }
        break;
      }
      case VariationSettingTable._ENABLE_INDEX:
      {    
        final BusyCancelDialog busyDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("MMGUI_SETUP_VARIATION_SETTING_KEY"),
          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
          true);

        busyDialog.pack();
        SwingUtils.centerOnComponent(busyDialog, _mainUI);

        _busyDialogWorkerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            boolean newEnableState = (boolean) object;
            boolean oldEnableState = false;
            VariationSetting oldVariationSetting = null;
            VariationSetting newVariationSetting = _variationList.get(rowIndex);
            
            for (int i = 0; i < getRowCount(); i++)
            {
              if (_variationList.get(i).isEnable())
              {
                oldVariationSetting = _variationList.get(i);
                oldEnableState = true;
                break;
              }
            }
            try
            {
              _commandManager.execute(new ConfigSetEnableVariationCommand(_project,
                                                                          newEnableState,
                                                                          newVariationSetting,
                                                                          oldEnableState,
                                                                          oldVariationSetting));
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
            }
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                busyDialog.setVisible(false);
              }
            });
          }
        });
        busyDialog.setVisible(true);
        break;
      }
      case VariationSettingTable._VIEWABLE_IN_PRODUCTION_INDEX:
      {
        if(_variationList.get(rowIndex).isViewableInProduction() == true)
            _variationList.get(rowIndex).setIsViewableInProduction(false);
        else
            _variationList.get(rowIndex).setIsViewableInProduction(true);
        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }
    fireTableDataChanged();
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setSelectedVariations(List<VariationSetting> variations)
  {
    Assert.expect(variations != null);
    _selectedVariations = variations;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public VariationSetting getVariation(int rowIndex)
  {
    Assert.expect(rowIndex >= 0);
    
    return _variationList.get(rowIndex);
  }
}
