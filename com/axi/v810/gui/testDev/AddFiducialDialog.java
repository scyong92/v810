package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.text.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
public class AddFiducialDialog extends EscapeDialog
{
  private boolean _ok = false;
  private Project _project;
  private BoardCoordinate _coordinate;
  private boolean _side1;
  private boolean _sideIsSet;
  private BoardType _boardType;
  private boolean _panelFid;
  private boolean _fidTypeIsSet;

  private double _MIN_FIDUCIAL_DIMENSION_IN_NANOS = 127000.0; // 5 mils

  private String _PANEL_FID = StringLocalizer.keyToString("AFD_PANEL_TYPE_KEY");
  private String _BOARD_FID = StringLocalizer.keyToString("AFD_BOARD_TYPE_KEY");

  private DecimalFormat _xLocDataFormat = new DecimalFormat();
  private NumericRangePlainDocument _xLocDataDoc = new NumericRangePlainDocument();

  private DecimalFormat _yLocDataFormat = new DecimalFormat();
  private NumericRangePlainDocument _yLocDataDoc = new NumericRangePlainDocument();

  private DecimalFormat _xDimDataFormat = new DecimalFormat();
  private NumericRangePlainDocument _xDimDataDoc = new NumericRangePlainDocument();

  private DecimalFormat _yDimDataFormat = new DecimalFormat();
  private NumericRangePlainDocument _yDimDataDoc = new NumericRangePlainDocument();

  private BorderLayout _mainBorderLayout = new BorderLayout();
  private JPanel _selectPanel = new JPanel();
  private JPanel _selectFiducialTypePanel = new JPanel();
  private JPanel _okCancelPanel = new JPanel();
  private JPanel _okCancelInnerPanel = new JPanel();
  private GridLayout _okCancelInnerGridLayout = new GridLayout();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private FlowLayout _okCanelFowLayout = new FlowLayout();
  private PairLayout _selectPanelPairLayout = new PairLayout(10, 10, false);
  private JLabel _typeLabel = new JLabel();
  private JComboBox _typeComboBox = new JComboBox();
  private JLabel _nameLabel = new JLabel();
  private JTextField _nameTextField = new JTextField();
  private JLabel _xLocLabel = new JLabel();
  private NumericTextField _xLocTextField = new NumericTextField();
  private JLabel _yLocLabel = new JLabel();
  private NumericTextField _yLocTextField = new NumericTextField();
  private JLabel _xDimLabel = new JLabel();
  private NumericTextField _xDimTextField = new NumericTextField();
  private JLabel _yDimLabel = new JLabel();
  private NumericTextField _yDimTextField = new NumericTextField();
  private JLabel _boardSideLabel = new JLabel();
  private JComboBox _boardSideComboBox = new JComboBox();
  private JLabel _boardTypeLabel = new JLabel();
  private JComboBox _boardTypeComboBox = new JComboBox();

  /**
   * @author Laura Cormos
   */
  public AddFiducialDialog(Frame frame, String title, boolean modal, BoardType boardType, Project project,
                           BoardCoordinate coordinate, boolean side1, boolean isPanelFiducial)
  {
    super(frame, title, modal);

    Assert.expect(boardType != null);
    Assert.expect(project != null);
    Assert.expect(coordinate != null);

    _boardType = boardType;
    _project = project;
    _coordinate = coordinate;
    _side1 = side1;
    _sideIsSet = true;
    _panelFid = isPanelFiducial;
    _fidTypeIsSet = true;

    jbInit();

    pack();
  }

  /**
   * @author Laura Cormos
   */
  public AddFiducialDialog(Frame frame, String title, boolean modal,Project project,
                           BoardCoordinate coordinate, boolean side1, boolean isPanelFiducial)
  {
    super(frame, title, modal);

    Assert.expect(project != null);
    Assert.expect(coordinate != null);

    _project = project;
    _coordinate = coordinate;
    _side1 = side1;
    _sideIsSet = true;
    _panelFid = isPanelFiducial;
    _fidTypeIsSet = true;

    jbInit();

    pack();
  }

  /**
   * @author Laura Cormos
   */
  public AddFiducialDialog(Frame frame, String title, boolean modal, Project project)
  {
    super(frame, title, modal);

    Assert.expect(project != null);

    _project = project;

    jbInit();

    pack();
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit()
  {
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    getContentPane().setLayout(_mainBorderLayout);
    getContentPane().add(_okCancelPanel,  BorderLayout.SOUTH);
    _okCancelPanel.setLayout(_okCanelFowLayout);
    _okCancelPanel.setBorder(BorderFactory.createEmptyBorder(0,0,5,5));
    _okCanelFowLayout.setAlignment(FlowLayout.RIGHT);
    _okCancelPanel.add(_okCancelInnerPanel, null);
    _okCancelInnerPanel.setLayout(_okCancelInnerGridLayout);
    _okCancelInnerGridLayout.setHgap(20);
    _okCancelInnerPanel.add(_okButton, null);
    _okCancelInnerPanel.add(_cancelButton, null);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });

    _typeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _typeLabel.setText(StringLocalizer.keyToString("AFD_SELECT_FIDUCIAL_TYPE_KEY"));
    _nameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _nameLabel.setText(StringLocalizer.keyToString("CAD_ENTER_ITEM_NAME_KEY"));
    _xLocLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _xLocLabel.setText(StringLocalizer.keyToString("ACD_X_LOCATION_LABEL_KEY"));
    _yLocLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _yLocLabel.setText(StringLocalizer.keyToString("ACD_Y_LOCATION_LABEL_KEY"));
    _xDimLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _xDimLabel.setText(StringLocalizer.keyToString("AFD_X_DIMENSION_LABEL_KEY"));
    _yDimLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _yDimLabel.setText(StringLocalizer.keyToString("AFD_Y_DIMENSION_LABEL_KEY"));
    _boardSideLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _boardSideLabel.setText(StringLocalizer.keyToString("AFD_SIDE_LABEL_KEY"));
    _boardTypeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _boardTypeLabel.setText(StringLocalizer.keyToString("ACD_BOARD_TYPE_LABEL_KEY"));

    // only if this dialog is started from the Add button, allow type selection, otherwise
    // the type is predetemined when the right click on graphics happened.
    if (_fidTypeIsSet == false)
    {
      getContentPane().add(_selectFiducialTypePanel, BorderLayout.NORTH);
      _selectFiducialTypePanel.setBorder(BorderFactory.createCompoundBorder(
          new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
                                                            new Color(142, 142, 142)), StringLocalizer.keyToString("AFD_FIDUCIAL_TYPE_KEY")),
          BorderFactory.createEmptyBorder(0, 5, 10, 5)));
      _selectFiducialTypePanel.add(_typeLabel);
      _selectFiducialTypePanel.add(_typeComboBox);
    }
    getContentPane().add(_selectPanel, BorderLayout.CENTER);
    _selectPanel.setLayout(_selectPanelPairLayout);
    _selectPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(142, 142, 142)), StringLocalizer.keyToString("AFD_FIDUCIAL_DATA_KEY")),
      BorderFactory.createEmptyBorder(0, 5, 10, 5)));
    _selectPanel.add(_nameLabel);
    _selectPanel.add(_nameTextField);
    if (_panelFid == false)
    {
      _selectPanel.add(_boardTypeLabel);
      _selectPanel.add(_boardTypeComboBox);
    }
    _selectPanel.add(_boardSideLabel);
    _selectPanel.add(_boardSideComboBox);
    _selectPanel.add(_xLocLabel);
    _selectPanel.add(_xLocTextField);
    _selectPanel.add(_yLocLabel);
    _selectPanel.add(_yLocTextField);
    _selectPanel.add(_xDimLabel);
    _selectPanel.add(_xDimTextField);
    _selectPanel.add(_yDimLabel);
    _selectPanel.add(_yDimTextField);

    _nameTextField.setColumns(10);

    FocusAdapter textFieldFocusAdapter = new FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        ((NumericTextField)e.getSource()).selectAll();
      }
    };
    _xLocTextField.addFocusListener(textFieldFocusAdapter);
    _yLocTextField.addFocusListener(textFieldFocusAdapter);
    _xDimTextField.addFocusListener(textFieldFocusAdapter);
    _yDimTextField.addFocusListener(textFieldFocusAdapter);

    getRootPane().setDefaultButton(_okButton);

    // set up the fiducial type combo box
    com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
    // add the panel fiducial choice only if the panel is larger than the board surface or there are multiple boards
    // Punit, XCR-2338: Add Fiducial Dialog box is disabled when try to add Fiducal on board carrier
    // Use shape relative to panel to compare to take care of rotated Panel and Board
    if (panel.isMultiBoardPanel() || isPanelSizeGreaterThanBoardSize(panel.getShapeInNanoMeters(),
                                                                     panel.getBoards().get(0).getShapeRelativeToPanelInNanoMeters()))
      _typeComboBox.addItem(_PANEL_FID);
    _typeComboBox.addItem(_BOARD_FID);
    _typeComboBox.setSelectedIndex(-1); // force user to make a choice here
    _typeComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        typeComboBox_actionPerformed();
      }
    });

    setEnabledSelectPanel(false);

    // set up the type combo box, if this dialog is following a mouse click
    if (_fidTypeIsSet)
    {
      if (_panelFid)
        _typeComboBox.setSelectedItem(_PANEL_FID);
      else
        _typeComboBox.setSelectedItem(_BOARD_FID);
    }

    // if this is a multiple board type project, set up the board type combo box, otherwise, cut it
    java.util.List<BoardType> boardTypes = _project.getPanel().getBoardTypes();
    if (boardTypes.size() > 1)
    {
      for (BoardType boardType : boardTypes)
        _boardTypeComboBox.addItem(boardType);
      if (_boardType != null)
        _boardTypeComboBox.setSelectedItem(_boardType);
      else
        _boardType = (BoardType)_boardTypeComboBox.getSelectedItem();
    }
    else
    {
      _selectPanel.remove(_boardTypeLabel);
      _selectPanel.remove(_boardTypeComboBox);
      _boardType = _project.getPanel().getBoardTypes().get(0);
    }
    _boardTypeComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        boardTypeComboBox_actionPerformed();
      }
    });

    _xLocTextField.setColumns(8);
    _yLocTextField.setColumns(8);
    _xDimTextField.setColumns(8);
    _yDimTextField.setColumns(8);

    MathUtilEnum displayUnits = _project.getDisplayUnits();
    Double minFiducialDimensionInDisplayUnits = MathUtil.convertUnits(_MIN_FIDUCIAL_DIMENSION_IN_NANOS, MathUtilEnum.NANOMETERS, displayUnits);

    // set up the data document for the coordinate and dimensions text fields
    _xLocDataDoc.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _xLocTextField.setDocument(_xLocDataDoc);
    _yLocDataDoc.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _yLocTextField.setDocument(_yLocDataDoc);
    _xDimDataDoc.setRange(new DoubleRange(minFiducialDimensionInDisplayUnits, Double.MAX_VALUE));
    _xDimTextField.setDocument(_xDimDataDoc);
    _yDimDataDoc.setRange(new DoubleRange(minFiducialDimensionInDisplayUnits, Double.MAX_VALUE));
    _yDimTextField.setDocument(_yDimDataDoc);

    // set up the proper number of decimal digits based on the selected units
    StringBuffer decimalFormat = new StringBuffer("########0.");

    int decimalPlaces = MeasurementUnits.getMeasurementUnitDecimalPlaces(_project.getDisplayUnits());
    for(int i = 0; i < decimalPlaces; i++)
      decimalFormat.append("0");  // use a "0" if you want to show trailing zeros, use a "#" if you don't

    _xLocDataFormat.applyPattern(decimalFormat.toString());
    _xLocTextField.setFormat(_xLocDataFormat);
    _yLocDataFormat.applyPattern(decimalFormat.toString());
    _yLocTextField.setFormat(_yLocDataFormat);
    _xDimDataFormat.applyPattern(decimalFormat.toString());
    _xDimTextField.setFormat(_xDimDataFormat);
    _yDimDataFormat.applyPattern(decimalFormat.toString());
    _yDimTextField.setFormat(_yDimDataFormat);

    if (_coordinate != null)
    {
      double xLocInDisplayUnits = MathUtil.convertUnits(_coordinate.getX(), MathUtilEnum.NANOMETERS, _project.getDisplayUnits());
      double yLocInDisplayUnits = MathUtil.convertUnits(_coordinate.getY(), MathUtilEnum.NANOMETERS, _project.getDisplayUnits());
      _xLocTextField.setValue(xLocInDisplayUnits);
      _yLocTextField.setValue(yLocInDisplayUnits);
    }

    // set up default size for fiducial (100 mils round)
    double xDimInDisplayUnits = MathUtil.convertUnits(2540000, MathUtilEnum.NANOMETERS, _project.getDisplayUnits());
    double yDimInDisplayUnits = MathUtil.convertUnits(2540000, MathUtilEnum.NANOMETERS, _project.getDisplayUnits());
    _xDimTextField.setValue(xDimInDisplayUnits);
    _yDimTextField.setValue(yDimInDisplayUnits);

    // set up the board side combo box, if known
    _boardSideComboBox.addItem(StringLocalizer.keyToString("CAD_SIDE1_KEY"));
    _boardSideComboBox.addItem(StringLocalizer.keyToString("CAD_SIDE2_KEY"));
    if (_sideIsSet)
    {
      if (_side1)
        _boardSideComboBox.setSelectedItem(StringLocalizer.keyToString("CAD_SIDE1_KEY"));
      else
        _boardSideComboBox.setSelectedItem(StringLocalizer.keyToString("CAD_SIDE2_KEY"));
    }
    _boardSideComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        boardSideComboBox_actionPerformed();
      }
    });
  }

  /**
   * @author Laura Cormos
   */
  boolean userClickedOk()
  {
    return _ok;
  }

  /**
   * @author Laura Cormos
   */
  String getFiducialName()
  {
    return _nameTextField.getText();
  }

  /**
   * @author Laura Cormos
   */
  String getFiducialType()
  {
    return (String)_typeComboBox.getSelectedItem();
  }

  /**
   * @author Laura Cormos
   */
  String getBoardSide()
  {
    return (String)_boardSideComboBox.getSelectedItem();
  }

  /**
   * @author Laura Cormos
   */
  BoardType getBoardType()
  {
    if (_boardType != null)
      return _boardType;
    else
      return (BoardType)_boardTypeComboBox.getSelectedItem();
  }

  /**
   * @author Laura Cormos
   */
  BoardCoordinate getBoardCoordinate()
  {
    Assert.expect(_coordinate != null);

    return _coordinate;
  }

  /**
   * @author Laura Cormos
   */
  int getXDimensionInNanos()
  {
    try
    {
      return (int)MathUtil.convertUnits(_xDimTextField.getDoubleValue(), _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
    }
    catch (ParseException ex)
    {
      MessageDialog.showErrorDialog(this,
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
      return -1;
    }
  }

  /**
   * @author Laura Cormos
   */
  int getYDimensionInNanos()
  {
    try
    {
      return (int)MathUtil.convertUnits(_yDimTextField.getDoubleValue(), _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
    }
    catch (ParseException ex)
    {
      MessageDialog.showErrorDialog(this,
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
      return -1;
    }
  }

  /**
   * @author Laura Cormos
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    _ok = false;
    _project = null;
    dispose();
  }

  /**
   * @author Laura Cormos
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    double xLocInNanos = 0.0;
    double yLocInNanos = 0.0;
    double xDimInNanos = 0.0;
    double yDimInNanos = 0.0;

    String errorMessage = new String();

    // check name validity
    String name = _nameTextField.getText();
    if (name.length() > 0)
    {
      if (getFiducialType().equals(_BOARD_FID))
      {
        Assert.expect(_boardType != null);
        if (_boardType.isFiducialTypeNameValid(name) == false)
        {
          String illegalChars = _boardType.getFiducialTypeNameIllegalChars();
          LocalizedString message = null;
          if (illegalChars.equals(" "))
            message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_SPACES_KEY",
                                          new Object[]{name});
          else
            message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_OTHER_CHARS_KEY",
                                          new Object[]{name, illegalChars});

          errorMessage += StringLocalizer.keyToString(message) + "\n";
        }
        else if (_boardType.isFiducialTypeNameDuplicate(name))
        {
          errorMessage += StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
                                                                          new Object[]{name})) + "\n";
        }
      }
      else
      {
        if (_project.getPanel().isFiducialNameValid(name) == false)
        {
          String illegalChars = _project.getPanel().getFiducialNameIllegalChars();
          LocalizedString message = null;
          if (illegalChars.equals(" "))
            message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_SPACES_KEY", new Object[]{name});
          else
            message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_OTHER_CHARS_KEY",
                                          new Object[]{name, illegalChars});

          errorMessage += StringLocalizer.keyToString(message) + "\n";
        }
        else if (_project.getPanel().isFiducialNameDuplicate(name))
        {
          errorMessage += StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
                                                                          new Object[]{name})) + "\n";
        }
      }
    }
    else
      errorMessage += StringLocalizer.keyToString("AFD_MISSING_NAME_KEY") + "\n";
    // check coordinate validity
    try
    {
      xLocInNanos = MathUtil.convertUnits(_xLocTextField.getDoubleValue(), _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
    }
    catch (ParseException ex)
    {
      errorMessage += StringLocalizer.keyToString("ACD_X_LOC_VALUE_KEY") + "\n";
    }
    try
    {
      yLocInNanos = MathUtil.convertUnits(_yLocTextField.getDoubleValue(), _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
    }
    catch (ParseException ex)
    {
      errorMessage += StringLocalizer.keyToString("ACD_Y_LOC_VALUE_KEY") + "\n";
    }
    // check dimensions validity
    try
    {
      xDimInNanos = MathUtil.convertUnits(_xDimTextField.getDoubleValue(), _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
      if (xDimInNanos < _MIN_FIDUCIAL_DIMENSION_IN_NANOS)
        errorMessage += StringLocalizer.keyToString("AFD_FID_X_DIM_TOO_SMALL_KEY") + "\n";
    }
    catch (ParseException ex)
    {
      errorMessage += StringLocalizer.keyToString("AFD_X_DIM_VALUE_KEY") + "\n";
    }
    try
    {
      yDimInNanos = MathUtil.convertUnits(_yDimTextField.getDoubleValue(), _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
      if (yDimInNanos < _MIN_FIDUCIAL_DIMENSION_IN_NANOS)
        errorMessage += StringLocalizer.keyToString("AFD_FID_Y_DIM_TOO_SMALL_KEY") + "\n";
    }
    catch (ParseException ex)
    {
      errorMessage += StringLocalizer.keyToString("AFD_Y_DIM_VALUE_KEY") + "\n";
    }

    if (errorMessage.length() > 0)
    {
      MessageDialog.showErrorDialog(this,
                                    errorMessage,
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
      return;
    }

    // check if fiducial is within the panel or the board boundary
    errorMessage = new String();

    if (getFiducialType().equals(_PANEL_FID))
    {
      int panelWidth = 0;
      int panelLength = 0;
      int rotation = _project.getPanel().getPanelSettings().getDegreesRotationAfterAllRotations();
      if (rotation == 0 || rotation == 180)
      {
        panelWidth = _project.getPanel().getWidthAfterAllRotationsInNanoMeters();
        panelLength = _project.getPanel().getLengthAfterAllRotationsInNanoMeters();
      }
      else
      {
        panelWidth = _project.getPanel().getLengthAfterAllRotationsInNanoMeters();
        panelLength = _project.getPanel().getWidthAfterAllRotationsInNanoMeters();
      }
      if ( xLocInNanos - (xDimInNanos / 2) < 0 || xLocInNanos + (xDimInNanos / 2) > panelWidth)
      {
        errorMessage += StringLocalizer.keyToString("AFD_X_LOC_OFF_PANEL_KEY") + "\n";
      }
      if ( yLocInNanos - (yDimInNanos / 2) < 0 || yLocInNanos + (yDimInNanos / 2) > panelLength)
      {
        errorMessage += StringLocalizer.keyToString("AFD_Y_LOC_OFF_PANEL_KEY") + "\n";
      }
      // check if the panel fiducial is within any board boundary
      errorMessage += isPanelFiducialWithinBoardBoundary(xLocInNanos, yLocInNanos, xDimInNanos, yDimInNanos);
    }
    else
    {
      double boardWidth = _boardType.getWidthInNanoMeters();
      double boardLength = _boardType.getLengthInNanoMeters();
      if ( xLocInNanos - (xDimInNanos / 2) < 0 || xLocInNanos + (xDimInNanos / 2) > boardWidth)
      {
        errorMessage += StringLocalizer.keyToString("AFD_X_LOC_OFF_BOARD_KEY") + "\n";
      }
      if ( yLocInNanos - (yDimInNanos / 2) < 0 || yLocInNanos + (yDimInNanos / 2) > boardLength)
      {
        errorMessage += StringLocalizer.keyToString("AFD_Y_LOC_OFF_BOARD_KEY") + "\n";
      }
    }

    if (errorMessage.length() > 0)
    {
      MessageDialog.showErrorDialog(this,
                                    errorMessage,
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
      return;
    }

    _coordinate = new BoardCoordinate((int)xLocInNanos, (int)yLocInNanos);
    _ok = true;
    dispose();
  }

  /**
   * @author George Booth
   */
  public static String isPanelFiducialWithinBoardBoundary(double xLocInNanos,
                                                          double yLocInNanos,
                                                          double xDimInNanos,
                                                          double yDimInNanos)
  {
    String errorMessage = new String();

    // This function does nothing at first release. The possible checks use the board's
    // bounding rectangle rather than the true outline. There may be cutouts in the board
    // that would be a viable location for a panel fiducial.  This situation would fail
    // this check.
    if (true)
    {
      return errorMessage;
    }

    // fiducial location is relative to panel

    double fiducialMinX = xLocInNanos - (xDimInNanos / 2);
    double fiducialMinY = yLocInNanos - (yDimInNanos / 2);
    Rectangle2D fiducial = new Rectangle2D.Double(fiducialMinX, fiducialMinY, xDimInNanos, yDimInNanos);
//    System.out.println("\nfiducial   : x = " + (int)fiducial.getBounds2D().getX() +
//                                    ", y = " + (int)fiducial.getBounds2D().getY() +
//                                ", width = " + (int)fiducial.getBounds2D().getWidth() +
//                               ", length = " + (int)fiducial.getBounds2D().getHeight() );

//    System.out.println("fiducial   : minX = " + (int)fiducial.getBounds2D().getMinX() +
//                                  ", minY = " + (int)fiducial.getBounds2D().getMinY() +
//                                  ", maxX = " + (int)fiducial.getBounds2D().getMaxX() +
//                                  ", maxY = " + (int)fiducial.getBounds2D().getMaxY() );

    java.util.List<Board> boardList = Project.getCurrentlyLoadedProject().getPanel().getBoards();
    for (Board board : boardList)
    {
      java.awt.Shape boardShape = board.getShapeRelativeToPanelInNanoMeters();

//      System.out.println("board shape: x = " + (int)boardShape.getBounds2D().getX() +
//                                    ", y = " + (int)boardShape.getBounds2D().getY() +
//                                ", width = " + (int)boardShape.getBounds2D().getWidth() +
//                               ", length = " + (int)boardShape.getBounds2D().getHeight() );

//      System.out.println("board      : minX = " + (int)boardShape.getBounds2D().getMinX() +
//                                    ", minY = " + (int)boardShape.getBounds2D().getMinY() +
//                                    ", maxX = " + (int)boardShape.getBounds2D().getMaxX() +
//                                    ", maxY = " + (int)boardShape.getBounds2D().getMaxY() );

      if (boardShape.intersects(fiducial))
      {
        errorMessage += StringLocalizer.keyToString(new LocalizedString("AFD_PANEL_FID_WITHIN_BOARD_KEY",
                                                                        new Object[]{board.getName()})) + "\n";
        // fiducial can only be in one board
        break;
      }
    }
    return errorMessage;
  }

  /**
   * @author Laura Cormos
   */
  private void boardTypeComboBox_actionPerformed()
  {
    BoardType boardType = (BoardType)_boardTypeComboBox.getSelectedItem();
    _boardType = boardType;
  }

  /**
   * @author Laura Cormos
   */
  private void typeComboBox_actionPerformed()
  {
    String type = (String)_typeComboBox.getSelectedItem();
    setEnabledSelectPanel(true);
    if (type.equals(_PANEL_FID))
    {
      _boardTypeLabel.setEnabled(false);
      _boardTypeComboBox.setEnabled(false);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void setEnabledSelectPanel(boolean enabled)
  {
    _selectPanel.setEnabled(enabled);
    _nameLabel.setEnabled(enabled);
    _nameTextField.setEnabled(enabled);
    _xLocLabel.setEnabled(enabled);
    _xLocTextField.setEnabled(enabled);
    _xDimLabel.setEnabled(enabled);
    _xDimTextField.setEnabled(enabled);
    _yLocLabel.setEnabled(enabled);
    _yLocTextField.setEnabled(enabled);
    _yDimLabel.setEnabled(enabled);
    _yDimTextField.setEnabled(enabled);
    _boardSideLabel.setEnabled(enabled);
    _boardSideComboBox.setEnabled(enabled);
    _boardTypeLabel.setEnabled(enabled);
    _boardTypeComboBox.setEnabled(enabled);
  }

  /**
   * @author Laura Cormos
   */
  public void clear()
  {
    _boardSideComboBox.removeAllItems();
    _boardTypeComboBox.removeAllItems();
    _project = null;
    _coordinate = null;
    _boardType = null;
  }

  /**
   * @author Laura Cormos
   */
  private void boardSideComboBox_actionPerformed()
  {
    String boardSide = getBoardSide();
    if (boardSide != null)
    {
      if(getBoardSide().equals(StringLocalizer.keyToString("CAD_SIDE1_KEY")))
        _side1 = true;
      else
        _side1 = false;
      _sideIsSet = true;
    }
  }

  /**
   * @author Laura Cormos
   */
  private boolean isPanelSizeGreaterThanBoardSize(Shape panelShapeInNanos, Shape boardShapeInNanos)
  {
    Assert.expect(panelShapeInNanos != null);
    Assert.expect(boardShapeInNanos != null);

    if (MathUtil.fuzzyContains(panelShapeInNanos, boardShapeInNanos, 500) &&
        MathUtil.fuzzyContains(boardShapeInNanos, panelShapeInNanos, 500) == false)
      return true;
    return false;
  }
}
