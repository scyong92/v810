package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * <p>Title: InteractiveLearningDialog</p>
 *
 * <p>Description: This is a dialog used as the controlling panel for Interactive Learning in the Initial Tuning
 * task.  It's visibility is controlled by a series of InspectionEvents.  The individual algorithms will send
 * a specific inspection event and data to be displayed to the user.  A panel tuned to the needs of the algorithm
 * will be shown to the user and the user will respond with the appropriate selection. </p>
 *
 * <p>This is set up right now for the Exposed Pad algorithm. Hints on how to extend it for other algorithms are
 * commented, e.g., Capacitor. <p>
 *
 * <p>Refer to the ExpectedImageVoidingAlgorithm for an example of how the interactive feature is implemented. </p>
 *
 * <p>Copyright: Copyright (c) 2008</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 * @version 1.0
 */
public class InteractiveLearningDialog extends JDialog implements Observer
{
  private Frame _frame;
  private InitialTuningPanel _invoker;

  private JPanel _mainPanel;
  private CardLayout _mainCardLayout;
  private InteractiveLearningStartedPanel _startPanel;
  private InteractiveLearningExposedPadPanel _exposedPadPanel;
//  private InteractiveLearningCapacitorPanel _capacitorPanel;

  private InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();

  // internal names for panels, not user visible
  private final String START_PANEL = "Start Panel";
  private final String EXPOSED_PAD_PANEL = "Exposd Pad Panel";
//  private final String CAPACITOR_PANEL = "Capacitor Panel";

  private String _activePanel = START_PANEL;


  /**
   * @author George Booth
   */
  public InteractiveLearningDialog(Frame frame, InitialTuningPanel invoker, boolean modal)
  {
    super(frame, modal);
    _frame = frame;
    _invoker = invoker;
    jbInit();
  }

  /**
   * @author George Booth
   */
  private void jbInit()
  {
    setTitle(StringLocalizer.keyToString("MMGUI_INTERACTIVE_LEARNING_TITLE_KEY"));
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

    _mainPanel = new JPanel(new BorderLayout());
    getContentPane().add(_mainPanel);
    _mainCardLayout = new CardLayout();
    _mainPanel.setLayout(_mainCardLayout);
    this.add(_mainPanel);

    _startPanel = new InteractiveLearningStartedPanel();
    _mainPanel.add(_startPanel, START_PANEL);
    _mainCardLayout.first(_mainPanel);

    _exposedPadPanel = new InteractiveLearningExposedPadPanel(_invoker);
    _mainPanel.add(_exposedPadPanel, EXPOSED_PAD_PANEL);

//    _capacitorPanel = new InteractiveLearningCapacitorPanel(_invoker);
//    _mainPanel.add(_capacitorPanel, CAPACITOR_PANEL);

    _inspectionEventObservable.addObserver(this);

    pack();

    _mainCardLayout.show(_mainPanel, START_PANEL);
  }

  /**
   * @author George Booth
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      if (_activePanel.equals(START_PANEL))
      {
        // do nothing - with all the invokeLater() calls, it hard to cancel before the first image is shown to the user
      }
      else if (_activePanel.equals(EXPOSED_PAD_PANEL))
      {
        _exposedPadPanel.cancelButton_actionPerformed();
      }
//      else if (_activePanel.equals(CAPACITOR_PANEL))
//      {
//        _capacitorPanel.cancelButton_actionPerformed();
//      }
      else
      {
        Assert.expect(false, "Bogus activePanel in Interactive Learning = " + _activePanel);
      }
    }
  }

  /**
   * The InteractiveLearningDialog is a listener to Inspection Events
   * @author George Booth
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof InspectionEventObservable)
          handleInspectionEvent((InspectionEvent)object);
        else
          Assert.expect(false);
      }
    });
  }

  /**
   * @author George Booth
   */
  private void handleInspectionEvent(final InspectionEvent inspectionEvent)
  {
    if (inspectionEvent instanceof InteractiveLearningStartedInspectionEvent)
    {
      this.setVisible(false);
      _mainCardLayout.show(_mainPanel, START_PANEL);
      SwingUtils.centerOnComponent(this, _frame);
      _startPanel.setUserInputState(inspectionEvent);
      _activePanel = START_PANEL;
      this.setVisible(true);
    }

    else if (inspectionEvent instanceof InteractiveLearningExposedPadInspectionEvent)
    {
      this.setVisible(false);
      _exposedPadPanel.setUserInputState(this, inspectionEvent);
      _mainCardLayout.show(_mainPanel, EXPOSED_PAD_PANEL);
      _activePanel = EXPOSED_PAD_PANEL;
      SwingUtils.centerOnComponent(this, _frame);
      this.setVisible(true);
    }

//    else if (inspectionEvent instanceof InteractiveLearningCapacitorInspectionEvent)
//    {
//      this.setVisible(false);
//      _mainCardLayout.show(_mainPanel, CAPACITOR_PANEL);
//      _activePanel = CAPACITOR_PANEL;
//      _capacitorPanel.setUserInputState(inspectionEvent);
//      this.setVisible(true);
//    }

    else if (inspectionEvent instanceof InteractiveLearningCompletedInspectionEvent)
    {
      this.setVisible(false);
      _mainCardLayout.show(_mainPanel, START_PANEL);
      _activePanel = START_PANEL;
      _startPanel.setDefaultState();
    }
  }

}
