package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.util.image.Image;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 *
 * Dialog used to add multiple instances of a board type
 * to a panel.  This is a modal dialog.
 * @author Andy Mechtenberg
 */
public class FixFocusDlg extends JDialog
{
  private Project _project = null;
  private java.util.List<FocusReconstructionRegionWrapper> _regions;
  private FocusReconstructionRegionWrapper _currentReconstructionRegion;
  private Slice _currentSlice;

  // this ReconstructedSlice does not increase the reference count of the images since it's lifespan is strictly bound by
  // the _slices list.
  private ReconstructedSlice _defaultReconstructedSlice;
  private int _currentImageIndex = -1;
  private int _indexOfDefault;
  private int _indexOfCustom = -1;
  private java.util.List<ReconstructedSlice> _slices;

  private final int _maxImageSize = 400;

  private Border _statusOuterBorder = BorderFactory.createBevelBorder(
       BevelBorder.LOWERED,Color.white,Color.white,
       new Color(99, 99, 99),new Color(142, 142, 142));
  private Border _statusInnerBorder = BorderFactory.createEmptyBorder(0, 5, 0, 0);
  private Border _statusBorder = BorderFactory.createCompoundBorder(_statusOuterBorder, _statusInnerBorder);
  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private JPanel _statusPanel = new JPanel();
  private FlowLayout _statusPanelFlowLayout = new FlowLayout(FlowLayout.LEFT, 0, 0);
  private JLabel _statusLabel = new JLabel(" ");
  private BorderLayout _contentPaneBorderLayout = new BorderLayout();
  private JPanel _buttonPanel = new JPanel();
  private FlowLayout _buttonPanelFlowLayout = new FlowLayout();
  private JButton _okButton = new JButton();
  private JPanel _imageControlPanel = new JPanel();
  private JPanel _adjustPanel = new JPanel();
  private JPanel _sliceChoicePanel = new JPanel();
  private BorderLayout _imageControlPanelBorderLayout = new BorderLayout();
  private JLabel _sliceLabel = new JLabel();
  private JRadioButton _slice3RadioButton = new JRadioButton();
  private JRadioButton _slice2RadioButton = new JRadioButton();
  private JRadioButton _slice1RadioButton = new JRadioButton();
  private FlowLayout _sliceChoicePanelFlowLayout = new FlowLayout();
  private JPanel _imageDisplayPanel = new JPanel();
  private JPanel _bestFocusButtonPanel = new JPanel();
  private JButton _bestFocusButton = new JButton();
  private JButton _defaultFocusButton = new JButton();
  private FlowLayout _bestFocusButtonPanelFlowLayout = new FlowLayout();
  private BorderLayout _adjustPanelBorderLayout = new BorderLayout();
  private JSlider _imageSlider = new JSlider();
  private JPanel _needingAdjustmentPanel = new JPanel();
  private JPanel _currentAdjustmentPanel = new JPanel();
  private BorderLayout _currentAdjustmentPanelBorderLayout = new BorderLayout();
  private JPanel _summaryNoTestPanel = new JPanel();
  private JButton _summaryButton = new JButton();
  private JCheckBox _noTestCheckBox = new JCheckBox();
  private Border _currentAdjustInnerBorder = BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166));
  private Border _currentAdjustBorder = new TitledBorder(_currentAdjustInnerBorder, StringLocalizer.keyToString("GUI_FOCUS_CURRENTLY_ADJUSTING_KEY"));
  private JScrollPane _needAdjustmentScrollPane = new JScrollPane();
  private JList _needAdjustmentList = new JList();
  private DefaultListModel _regionListModel;
  private Border _needAdjustInnerBorder = BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166));
  private Border _needAdjustBorder = new TitledBorder(_needAdjustInnerBorder, StringLocalizer.keyToString("GUI_FOCUS_NEEDING_ADJUSTMENT_KEY"));
  private BorderLayout _summaryNoTestPanelBorderLayout = new BorderLayout();
  private BorderLayout _imageDisplayPanelBorderLayout = new BorderLayout();
  private ImageDisplayPanel _imagePanel = new ImageDisplayPanel();
  private Border _sliderCompensationBorder = BorderFactory.createEmptyBorder(0, 0, 0, 0);
  private BorderLayout _needingAdjustmentPanelBorderLayout = new BorderLayout();
  private JPanel _infoPanel = new JPanel();
  private JLabel _componentLabel = new JLabel();
  private JLabel _jointTypeDataLabel = new JLabel();
  private JLabel _jointTypeLabel = new JLabel();
  private JLabel _jointDataLabel = new JLabel();
  private JLabel _jointLabel = new JLabel();
  private JLabel _componentDataLabel = new JLabel();

  private ImageIcon _completedIcon = Image5DX.getImageIcon(Image5DX.CD_PASSED_LEAF_TEST);
  private ImageIcon _currentIcon = Image5DX.getImageIcon(Image5DX.CT_RIGHT_ARROW);
  private ImageIcon _uncompletedIcon = Image5DX.getImageIcon(Image5DX.CD_DISABLED_LEAF_TEST);
  private ImageIcon _noTestIcon = Image5DX.getImageIcon(Image5DX.AT_DELETE_LEARNED_DATA);
  private JPanel _sliderPanel = new JPanel();
  private GridLayout _sliderPanelGridLayout = new GridLayout();

  /**
   * @author Andy Mechtenberg
   */
  private FixFocusDlg()
  {
    jbInit();
//    pack();
  }

  /**
   * @author Andy Mechtenberg
   */
  public FixFocusDlg(Project project)
  {
    super(MainMenuGui.getInstance(), StringLocalizer.keyToString("GUI_FOCUS_FIX_TITLE_KEY"), true);
    Assert.expect(project != null);

    _project = project;
    jbInit();
    populate();
    pack();
    updateSliderLayout();
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void populate()
  {
    java.util.List<ReconstructionRegion> regions = _project.getTestProgram().getUnfocusedReconstructionRegionsToBeFixed();

    _regions = new ArrayList<FocusReconstructionRegionWrapper>();
    for(ReconstructionRegion rr : regions)
      _regions.add(new FocusReconstructionRegionWrapper(rr));

    Collections.sort(_regions, new ReconstructionRegionComparator(true, ReconstructionRegionComparatorEnum.DESCRIPTION));
    for(FocusReconstructionRegionWrapper focusReconstructionRegionWrapper : _regions)
      _regionListModel.addElement(focusReconstructionRegionWrapper);

    // by default, select the first one in the list
    _needAdjustmentList.setSelectedIndex(0);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _project = null;
    _currentReconstructionRegion = null;
    _currentSlice = null;
    _regionListModel.clear();
    _regions.clear();

    if (_defaultReconstructedSlice != null)
      _defaultReconstructedSlice = null;
    if (_slices != null)
    {
      for (ReconstructedSlice slice : _slices)
        slice.decrementReferenceCount();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      okButton_actionPerformed();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    Container contentPane = this.getContentPane();
    contentPane.setLayout(_contentPaneBorderLayout);

    _regionListModel = new DefaultListModel();
    _needAdjustmentList.setModel(_regionListModel);
    _needAdjustmentList.setCellRenderer(new DefaultListCellRenderer()
    {
      public java.awt.Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus)
      {
        JLabel label = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, hasFocus);
        label.setIcon(_uncompletedIcon);
//        if (isSelected)
//          label.setIcon(_currentIcon);
        FocusReconstructionRegionWrapper rr = _regions.get(index);
        if (rr.isUsingCustomFocus())
          label.setIcon(_completedIcon);
        if (rr.isTestableForUnfocusedRegions() == false)
          label.setIcon(_noTestIcon);

        return label;
      }
    });
    _needAdjustmentList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _needAdjustmentList.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyPressed(KeyEvent e)
      {
//        diagnosticsList_keyPressed(e);
      }
    });
    _needAdjustmentList.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent lse)
      {
        regionListSelectionChanged(lse);
      }
    });


    _statusPanel.setLayout(_statusPanelFlowLayout);
    _imageControlPanel.setLayout(_imageControlPanelBorderLayout);
    _sliceLabel.setText(StringLocalizer.keyToString("GUI_FOCUS_SLICE_LABEL_KEY"));
    _slice1RadioButton.setText("Pad");
    _slice2RadioButton.setEnabled(false);
    _slice2RadioButton.setText(StringLocalizer.keyToString("GUI_FOCUS_SLICE_NONE_KEY"));
    _slice3RadioButton.setEnabled(false);
    _slice3RadioButton.setText(StringLocalizer.keyToString("GUI_FOCUS_SLICE_NONE_KEY"));

    _slice1RadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        sliceChanged_actionPerformed(_slice1RadioButton);
      }
    });

    _slice2RadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        sliceChanged_actionPerformed(_slice2RadioButton);
      }
    });

    _slice3RadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        sliceChanged_actionPerformed(_slice3RadioButton);
      }
    });

    _sliceChoicePanel.setLayout(_sliceChoicePanelFlowLayout);
    _sliceChoicePanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _sliceChoicePanelFlowLayout.setVgap(0);
    _bestFocusButton.setText(StringLocalizer.keyToString("GUI_FOCUS_BEST_CHOICE_KEY"));
    _bestFocusButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        bestFocusButton_actionPerformed();
      }
    });
    _defaultFocusButton.setText(StringLocalizer.keyToString("GUI_FOCUS_DEFAULT_CHOICE_KEY"));
    _defaultFocusButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        defaultFocusButton_actionPerformed();
      }
    });


    _bestFocusButtonPanel.setLayout(_bestFocusButtonPanelFlowLayout);
    _bestFocusButtonPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _imageDisplayPanel.setBorder(null);
    _imageDisplayPanel.setLayout(_imageDisplayPanelBorderLayout);
    _adjustPanel.setLayout(_adjustPanelBorderLayout);
    _imageSlider.setOrientation(JSlider.VERTICAL);
    _imageSlider.setPaintTicks(true);
    _imageSlider.setPaintLabels(true);
    _imageSlider.setPaintTrack(true);
    _imageSlider.setSnapToTicks(false);

    Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
    labelTable.put(new Integer(20), new JLabel(" " + StringLocalizer.keyToString("GUI_FOCUS_DEFAULT_FOCUS_KEY")));
    _imageSlider.setLabelTable(labelTable);

    _imageSlider.addChangeListener(new ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        handleSliderChangeEvent(e);
      }
    });

    _currentAdjustmentPanel.setLayout(_currentAdjustmentPanelBorderLayout);
    _summaryButton.setText(StringLocalizer.keyToString("GUI_FOCUS_SUMMARY_KEY"));
    _summaryButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        summaryButton_actionPerformed(e);
      }
    });

    _noTestCheckBox.setText(StringLocalizer.keyToString("GUI_FOCUS_FIX_WITH_NO_TEST_KEY"));
    _noTestCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        noTestCheckBox_actionPerformed();
      }
    });

    _infoPanel.setBorder(_currentAdjustBorder);
    _needingAdjustmentPanel.setBorder(_needAdjustBorder);
    _needingAdjustmentPanel.setLayout(_needingAdjustmentPanelBorderLayout);
    _summaryNoTestPanel.setLayout(_summaryNoTestPanelBorderLayout);
    _imagePanel.setBackground(Color.black);
    _imagePanel.setLimitImageSizeToMaximum(true);
    _imagePanel.setBorder(BorderFactory.createLoweredBevelBorder());
    _imagePanel.setPreferredSize(new Dimension(_maxImageSize, _maxImageSize));
    _bestFocusButtonPanel.setBorder(_sliderCompensationBorder);
    _sliceChoicePanel.setBorder(_sliderCompensationBorder);
    _currentAdjustmentPanelBorderLayout.setVgap(5);
    _sliderPanel.setLayout(_sliderPanelGridLayout);
    _imageDisplayPanelBorderLayout.setHgap(10);
    _mainPanelBorderLayout.setHgap(10);
    _statusPanel.add(_statusLabel);
    _statusPanel.setBorder(_statusBorder);

    _mainPanel.setLayout(_mainPanelBorderLayout);
    _mainPanel.setBorder(BorderFactory.createEmptyBorder(10,10,0,10));

    _buttonPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _buttonPanelFlowLayout.setHgap(0);
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed();
      }
    });

    _componentLabel.setText(StringLocalizer.keyToString("GUI_FOCUS_COMPONENT_KEY"));
    _jointLabel.setText(StringLocalizer.keyToString("GUI_FOCUS_JOINTS_KEY"));
    _jointTypeLabel.setText(StringLocalizer.keyToString("GUI_FOCUS_JOINT_TYPE_KEY"));

    _infoPanel.setLayout(new PairLayout(5,0));
    _infoPanel.add(_componentLabel);
    _infoPanel.add(_componentDataLabel);
    _infoPanel.add(_jointLabel);
    _infoPanel.add(_jointDataLabel);
    _infoPanel.add(_jointTypeLabel);
    _infoPanel.add(_jointTypeDataLabel);


    _buttonPanel.add(_okButton);
    this.getRootPane().setDefaultButton(_okButton);

    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    contentPane.add(_mainPanel, BorderLayout.CENTER);
    contentPane.add(_statusPanel, BorderLayout.SOUTH);
    _mainPanel.add(_imageControlPanel, java.awt.BorderLayout.CENTER);
    _mainPanel.add(_adjustPanel, java.awt.BorderLayout.WEST);
    _sliceChoicePanel.add(_sliceLabel);
    _sliceChoicePanel.add(_slice1RadioButton);
    _sliceChoicePanel.add(_slice2RadioButton);
    _sliceChoicePanel.add(_slice3RadioButton);
    _imageControlPanel.add(_imageDisplayPanel, java.awt.BorderLayout.CENTER);
    _imageControlPanel.add(_bestFocusButtonPanel, java.awt.BorderLayout.SOUTH);
    _bestFocusButtonPanel.add(_bestFocusButton);
    _bestFocusButtonPanel.add(_defaultFocusButton);
    _imageControlPanel.add(_sliceChoicePanel, java.awt.BorderLayout.NORTH);

    _adjustPanel.add(_needingAdjustmentPanel, java.awt.BorderLayout.CENTER);
    _needAdjustmentScrollPane.getViewport().add(_needAdjustmentList);
    _adjustPanel.add(_currentAdjustmentPanel, java.awt.BorderLayout.SOUTH);
    _currentAdjustmentPanel.add(_infoPanel, java.awt.BorderLayout.CENTER);
    _currentAdjustmentPanel.add(_summaryNoTestPanel, java.awt.BorderLayout.SOUTH);
    _summaryNoTestPanel.add(_summaryButton, java.awt.BorderLayout.WEST);
    _summaryNoTestPanel.add(_noTestCheckBox, java.awt.BorderLayout.EAST);
    _needingAdjustmentPanel.add(_needAdjustmentScrollPane, java.awt.BorderLayout.CENTER);
    _imageDisplayPanel.add(_imagePanel, java.awt.BorderLayout.CENTER);
    _sliderPanel.add(_imageSlider);

    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(_slice1RadioButton);
    buttonGroup.add(_slice2RadioButton);
    buttonGroup.add(_slice3RadioButton);
    _imageDisplayPanel.add(_sliderPanel, java.awt.BorderLayout.WEST);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateSliderLayout()
  {
    int sliderWidth = _sliderPanel.getWidth();
    _sliderCompensationBorder = BorderFactory.createEmptyBorder(0, sliderWidth + 10, 0, 0);  // the +10 is a hgap setting

    _bestFocusButtonPanel.setBorder(_sliderCompensationBorder);
    _sliceChoicePanel.setBorder(_sliderCompensationBorder);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void noTestCheckBox_actionPerformed()
  {
    // don't go through the wrapper here, as CANCEL is not supported in this dialog
    ReconstructionRegion currentRR = _currentReconstructionRegion.getReconstructionRegion();
    if (_noTestCheckBox.isSelected())
    {
      _currentReconstructionRegion.setTestestableForUnfocusedRegions(false);
      if (_currentReconstructionRegion.isMarkedForFixFocus())
        _currentReconstructionRegion.clearFixFocusLater();
    }
    else
    {
      _currentReconstructionRegion.setTestestableForUnfocusedRegions(true);
      _currentReconstructionRegion.setFixFocusLater(_currentSlice, true);
    }
    _needAdjustmentList.repaint();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void summaryButton_actionPerformed(ActionEvent e)
  {
    FocusSummaryDlg dlg = new FocusSummaryDlg(_project, _currentReconstructionRegion);
    SwingUtils.centerOnComponent(dlg, this);
    dlg.setVisible(true);
    dlg.unpopulate();
    dlg.dispose();
    _needAdjustmentList.repaint();
    if (_currentReconstructionRegion.isTestableForUnfocusedRegions())
      _noTestCheckBox.setSelected(false);
    else
      _noTestCheckBox.setSelected(true);
  }
  /**
   * @author Andy Mechtenberg
   */
  private void okButton_actionPerformed()
  {
    for(FocusReconstructionRegionWrapper rr : _regions)
    {
      if (rr.userAdjustedFocus())
      {
        if (rr.isMarkedForFixFocus())
          rr.clearFixFocusLater();
      }
      rr.commitChanges();
    }

    // if there are no more regions marked for fix focus, ask the user if they want to remove the focus
    // image set
    try
    {
      removeFocusSetIfNecessary();
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(this, ex, StringLocalizer.keyToString("GUI_FOCUS_FIX_TITLE_KEY"), true);
    }
    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void removeFocusSetIfNecessary() throws XrayTesterException
  {
    boolean askForRemoval = true;
    for(FocusReconstructionRegionWrapper rr : _regions)
    {
      if (rr.isMarkedForFixFocus())
        askForRemoval = false;
    }

    if (askForRemoval)
    {
      int answer = JOptionPane.showConfirmDialog(this,
                                                 StringLocalizer.keyToString("GUI_FOCUS_REMOVE_FOCUS_SET_KEY"),
                                                 StringLocalizer.keyToString("GUI_FOCUS_FIX_TITLE_KEY"),
                                                 JOptionPane.YES_NO_OPTION);
      if (answer == JOptionPane.YES_OPTION)
      {
        FileUtilAxi.delete(Directory.getAdjustFocusImagesDir(_project.getName()));
      }
    }
  }


  /**
   * @author Andy Mechtenberg
   */
  private void regionListSelectionChanged(ListSelectionEvent lse)
  {
    if (lse.getValueIsAdjusting())
      return;

    int index = _needAdjustmentList.getSelectedIndex();
    if (index < 0)
      return;
    _currentReconstructionRegion = _regions.get(index);

    if (_currentReconstructionRegion.isTestableForUnfocusedRegions())
      _noTestCheckBox.setSelected(false);
    else
      _noTestCheckBox.setSelected(true);

    setUIEnabled(true);
    updateRegionDescription();
    updateSliceChoices();
    updateSlider();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateSlider()
  {
    ReconstructedImages reconstructedImages = null;

    _defaultReconstructedSlice = null;

    if (_slices != null)
    {
      for (ReconstructedSlice slice : _slices)
        slice.decrementReferenceCount();
      _slices.clear();
    }

    try
    {
      reconstructedImages = ImageManager.getInstance().loadAdjustFocusImages(_currentReconstructionRegion.getReconstructionRegion());
    }
    catch (XrayTesterException ex)
    {
      String regionDesc = _currentReconstructionRegion.toString();
      LocalizedString errorMessage = new LocalizedString("GUI_FOCUS_ERROR_LOADING_IMAGES_KEY", new Object[]{regionDesc});
      if (this.isVisible())
        MessageDialog.showErrorDialog(this, StringLocalizer.keyToString(errorMessage), StringLocalizer.keyToString("GUI_FOCUS_FIX_TITLE_KEY"), true);
      else
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(), StringLocalizer.keyToString(errorMessage), StringLocalizer.keyToString("GUI_FOCUS_FIX_TITLE_KEY"), true);
      // disable most of the UI -- no images
      setUIEnabled(false);
      return;
    }

    _defaultReconstructedSlice = reconstructedImages.getReconstructedSlice(_currentSlice.getSliceName());
    _defaultReconstructedSlice.incrementReferenceCount();
    // we're not incrementing the slice's reference count here because it will be kept within the lifetime of _slices.

    _slices = new ArrayList<ReconstructedSlice>(reconstructedImages.getAdjustFocusReconstructedSlices());
//    // now add the original default slice
    _slices.add(_defaultReconstructedSlice);
    reconstructedImages.decrementReferenceCount();

    // sort them by slice height
    Collections.sort(_slices, new Comparator<ReconstructedSlice>()
    {
      public int compare(ReconstructedSlice lhs, ReconstructedSlice rhs)
      {
        Integer lhsZ = new Integer(lhs.getHeightInNanometers());
        Integer rhsZ = new Integer(rhs.getHeightInNanometers());
        return lhsZ.compareTo(rhsZ);
      }
    });

    _indexOfDefault =  _slices.indexOf(_defaultReconstructedSlice);

    int numberOfSlices = _slices.size();
    _currentImageIndex = -1;
    _imageSlider.setMinimum(0);
    _imageSlider.setMaximum(numberOfSlices - 1);
    _imageSlider.setMajorTickSpacing(10);

    _currentImageIndex = _indexOfDefault;
    // pre-load the image -- helps when the list selection changes but the slider didn't
    ReconstructedSlice slice = _slices.get(_currentImageIndex);
    displayImage(slice);
//    _imagePanel.setImage(slice.getImage().getBufferedImage());
    _imageSlider.setValue(_currentImageIndex);

//    _imageSlider.setMinorTickSpacing(1);

    Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
    labelTable.put(new Integer(_indexOfDefault), new JLabel(" " + StringLocalizer.keyToString("GUI_FOCUS_DEFAULT_FOCUS_KEY")));
//    System.out.println("New default z: " + _currentReconstructedSlice.getHeightInNanometers());

    _indexOfCustom = -1;
    if(_currentReconstructionRegion.isUsingCustomFocus(_currentSlice))
    {
      // find the index of the custom focus
      int deltaZ = Integer.MAX_VALUE;

      // to find the right slice offset
      // take the default slice height
      // minus it's focus instruction offset
      // and then add back in the custom slice offset
      int zHeight = _defaultReconstructedSlice.getHeightInNanometers() -
                    _currentSlice.getDefaultFocusInstructionOffsetInNanoMeters() +
                    _currentReconstructionRegion.getCustomFocusZheightOffsetInNanoMeters(_currentSlice);
      for(ReconstructedSlice reconstructedSlice : _slices)
      {
        int slizeZ = reconstructedSlice.getHeightInNanometers();
        int currentDelta = Math.abs(zHeight - slizeZ);
        if (currentDelta < deltaZ)
        {
          deltaZ = currentDelta;
          _indexOfCustom = _slices.indexOf(reconstructedSlice);
        }
//        System.out.println("z: " + reconstructedSlice.getHeightInNanometers());
//        if (reconstructedSlice.getHeightInNanometers() == zHeight)
//        {
//          _indexOfCustom = _slices.indexOf(reconstructedSlice);
//          labelTable.put(new Integer(_indexOfCustom), new JLabel(" " + StringLocalizer.keyToString("GUI_FOCUS_CUSTOM_FOCUS_KEY")));
//          _imageSlider.setValue(_indexOfCustom);
//          break;
//        }
      }
      labelTable.put(new Integer(_indexOfCustom), new JLabel(" " + StringLocalizer.keyToString("GUI_FOCUS_CUSTOM_FOCUS_KEY")));
      _imageSlider.setValue(_indexOfCustom);
    }
    _imageSlider.setLabelTable(labelTable);
  }

  private void setUIEnabled(boolean enabled)
  {
    _imageSlider.setEnabled(enabled);
    _slice1RadioButton.setEnabled(enabled);
    _slice2RadioButton.setEnabled(enabled);
    _slice3RadioButton.setEnabled(enabled);
    _bestFocusButton.setEnabled(enabled);
    _defaultFocusButton.setEnabled(enabled);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateRegionDescription()
  {
    JointTypeEnum jointTypeEnum = _currentReconstructionRegion.getJointTypeEnum();
    _componentDataLabel.setText(_currentReconstructionRegion.getReconstructionRegion().getComponent().getReferenceDesignator());
    _jointDataLabel.setText(_currentReconstructionRegion.getReconstructionRegion().getPadDescription());
    _jointTypeDataLabel.setText(jointTypeEnum.getName());
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateSliceChoices()
  {
    java.util.List<Slice> slices = new ArrayList<Slice>(_currentReconstructionRegion.getReconstructionRegion().getInspectedSlices());
    Assert.expect(slices.isEmpty() == false);

    // for PTH joint types, only choose the bottom slice
    JointTypeEnum jointTypeEnum = _currentReconstructionRegion.getJointTypeEnum();
    if (jointTypeEnum.equals(JointTypeEnum.THROUGH_HOLE) || 
        jointTypeEnum.equals(JointTypeEnum.OVAL_THROUGH_HOLE)) //Siew Yeng - XCR-3318 - Oval PTH
    {
      for (Slice slice : slices)
      {
        if (slice.getSliceName().equals(SliceNameEnum.THROUGHHOLE_PIN_SIDE))
        {
          _slice1RadioButton.setText(slice.getSliceName().getName());
          _slice1RadioButton.setSelected(true);
          _slice1RadioButton.setEnabled(true);
          _slice2RadioButton.setText(StringLocalizer.keyToString("GUI_FOCUS_SLICE_NONE_KEY"));
          _slice2RadioButton.setEnabled(false);
          _slice3RadioButton.setText(StringLocalizer.keyToString("GUI_FOCUS_SLICE_NONE_KEY"));
          _slice3RadioButton.setEnabled(false);
          _currentSlice = slice;
          break;
        }
      }
      return;
    }

    int numSlices = slices.size();
    _slice1RadioButton.setText(slices.get(0).getSliceName().getName());
    _slice1RadioButton.setSelected(true);
    _slice1RadioButton.setEnabled(true);
    _currentSlice = slices.get(0);
    if (numSlices > 1)
    {
      _slice2RadioButton.setText(slices.get(1).getSliceName().getName());
      _slice2RadioButton.setEnabled(true);
    }
    else
    {
      _slice2RadioButton.setText(StringLocalizer.keyToString("GUI_FOCUS_SLICE_NONE_KEY"));
      _slice2RadioButton.setEnabled(false);
    }
    if (numSlices > 2)
    {
      _slice3RadioButton.setText(slices.get(2).getSliceName().getName());
      _slice3RadioButton.setEnabled(true);
    }
    else
    {
      _slice3RadioButton.setText(StringLocalizer.keyToString("GUI_FOCUS_SLICE_NONE_KEY"));
      _slice3RadioButton.setEnabled(false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleSliderChangeEvent(ChangeEvent e)
  {
    int currentValue = _imageSlider.getValue();
    if (currentValue != _currentImageIndex)
    {
      _currentImageIndex = currentValue;
      ReconstructedSlice slice = _slices.get(_currentImageIndex);
      displayImage(slice);
      // apply the contrast enhacement to this image
//      Image image = slice.getImage();
//      Image imageCopy = new Image(image);
//      ImageEnhancement.autoEnhanceContrastInPlace(imageCopy);
//      BufferedImage bufferedImage = imageCopy.getBufferedImage();
//
//      _imagePanel.setImage(bufferedImage);
//      System.out.println("Z: " + slice.getHeightInNanometers() / 25400.0);
    }
//    System.out.println("CurrentValue : " + currentValue);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void displayImage(ReconstructedSlice slice)
  {
    // apply the contrast enhacement to this image
    Image image = slice.getImage();
    Image imageCopy = new Image(image);
    ImageEnhancement.autoEnhanceContrastInPlace(imageCopy);
    BufferedImage bufferedImage = imageCopy.getBufferedImage();
    imageCopy.decrementReferenceCount();
    _imagePanel.setImage(bufferedImage);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void bestFocusButton_actionPerformed()
  {
    Assert.expect(_defaultReconstructedSlice != null);
    Assert.expect(_slices != null);

//    _currentReconstructionRegion.clearFixFocusLater();
    ReconstructedSlice slice = _slices.get(_currentImageIndex);
    _indexOfCustom = _currentImageIndex;

    Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
    labelTable.put(new Integer(_indexOfDefault), new JLabel(" " + StringLocalizer.keyToString("GUI_FOCUS_DEFAULT_FOCUS_KEY")));

//    int a = slice.getHeightInNanometers();
//    int b = _defaultReconstructedSlice.getHeightInNanometers();
//    int bb = _currentSlice.getDefaultFocusInstructionOffsetInNanoMeters();
//    int c = _currentSlice.getFocusInstruction().getZOffsetInNanoMeters();

    if (_currentImageIndex == _indexOfDefault)
    {
      if (_currentSlice.isUsingCustomFocus())
        _currentReconstructionRegion.clearCustomFocus(_currentSlice);

      _indexOfCustom = -1;
    }
    else
    {
      labelTable.put(new Integer(_indexOfCustom), new JLabel(" " + StringLocalizer.keyToString("GUI_FOCUS_CUSTOM_FOCUS_KEY")));
      int zOffsetInNanos = 0;
      if (_currentSlice.isUsingCustomFocus())
        zOffsetInNanos = slice.getHeightInNanometers() - (_defaultReconstructedSlice.getHeightInNanometers() - _currentSlice.getDefaultFocusInstructionOffsetInNanoMeters());
      else
        zOffsetInNanos = slice.getHeightInNanometers() - (_defaultReconstructedSlice.getHeightInNanometers() - _currentSlice.getFocusInstruction().getZOffsetInNanoMeters());

      _currentReconstructionRegion.setCustomFocus(_currentSlice, zOffsetInNanos);
    }
     _imageSlider.setLabelTable(labelTable);
    _needAdjustmentList.repaint();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void defaultFocusButton_actionPerformed()
  {
    _imageSlider.setValue(_indexOfDefault);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void sliceChanged_actionPerformed(JRadioButton sliceRadioButton)
  {
     Collection<Slice> slices = _currentReconstructionRegion.getReconstructionRegion().getInspectedSlices();
     String newSliceName = sliceRadioButton.getText();
     // now, find the new slice object
     for(Slice slice : slices)
     {
       if (slice.getSliceName().getName().equals(newSliceName))
       {
         _currentSlice = slice;
         updateSlider();
         break;
       }
     }
  }
}
