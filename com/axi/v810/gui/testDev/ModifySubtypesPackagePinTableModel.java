package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * This class is the data model for the package table for the Groupings panel and it also
 * converts to the component table model in the groupings panel
 * @author Andy Mechtenberg
 */
public class ModifySubtypesPackagePinTableModel extends DefaultSortTableModel
{
  private List<PackagePin> _packagePins;  // list of PackagePin object
  private List<PackagePin> _currentlySelectedPackagePins = new ArrayList<PackagePin>();  // list of PackagePin objects that are currently selected

  private final String[] _packageTableColumns = {StringLocalizer.keyToString("ATGUI_JOINT_KEY"),
                                                StringLocalizer.keyToString("ATGUI_FAMILY_KEY"),
                                                StringLocalizer.keyToString("MMGUI_GROUPINGS_JOINT_HEIGHT_KEY"),
                                                StringLocalizer.keyToString("MMGUI_ORIENTATION_KEY")};

  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;
  private Project _project = null;

  /**
   * @author Andy Mechtenberg
   */
  public ModifySubtypesPackagePinTableModel()
  {
    // do nothing
    _packagePins = new ArrayList<PackagePin>();
  }

  void clear()
  {
    _project = null;
    if (_packagePins != null)
      _packagePins.clear();
    if (_currentlySelectedPackagePins != null)
      _currentlySelectedPackagePins.clear();
    _currentSortColumn = 0;
    _currentSortAscending = true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void populateWithPackageData(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);

    _project = Project.getCurrentlyLoadedProject();
    _packagePins = compPackage.getPackagePins();
    sortColumn(_currentSortColumn, _currentSortAscending);
    _currentlySelectedPackagePins = new ArrayList<PackagePin>();
    fireTableDataChanged();
  }

  /**
   * @author Andy Mechtenberg
   */
  void updateSpecificPackagePin(PackagePin packagePin)
  {
    Assert.expect(packagePin != null);
    int packagePinRow = _packagePins.indexOf(packagePin);
    fireTableRowsUpdated(packagePinRow, packagePinRow);
  }

  /**
   * @author Andy Mechtenberg
   */
  int getRowForPackagePin(PackagePin packagePin)
  {
    Assert.expect(packagePin != null);
    Assert.expect(_packagePins != null);
    return _packagePins.lastIndexOf(packagePin);
  }

  /**
   * @author Andy Mechtenberg
   */
  PackagePin getPackagePinAt(int row)
  {
    Assert.expect(_packagePins != null);
    return (PackagePin)_packagePins.get(row);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void sortColumn(int column, boolean ascending)
  {
    if (column == 0)
      Collections.sort(_packagePins, new AlphaNumericComparator(ascending));
    else if (column == 1)
      Collections.sort(_packagePins, new PackagePinComparator(ascending, PackagePinComparatorEnum.JOINT_TYPE)); // sort on joint type
    else if (column == 2)
    {
      Collections.sort(_packagePins, new PackagePinComparator(ascending, PackagePinComparatorEnum.JOINT_HEIGHT)); // sort on joint height
    }
    else if (column == 3)
      Collections.sort(_packagePins, new PackagePinComparator(ascending, PackagePinComparatorEnum.ORIENTATION)); // sort on orientation
    else
      Assert.expect(false, "too many columns for package pin details table");

    _currentSortColumn = column;
    _currentSortAscending = ascending;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getColumnCount()
  {
    return _packageTableColumns.length;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getRowCount()
  {
    if(_packagePins == null)
      return 0;
    else
      return _packagePins.size();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_packageTableColumns[columnIndex];
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    if (_currentlySelectedPackagePins == null)
      return false;

    if (columnIndex == 0) // joint name
      return false;
    else if (columnIndex == 1) // joint type for package Pin
    {
      JointTypeEnum jointType = getPackagePinAt(rowIndex).getJointTypeEnum();
      if (ImageAnalysis.isSpecialTwoPinComponent(jointType))
        return false;
      if (_currentlySelectedPackagePins.contains(_packagePins.get(rowIndex)))
        return true;
      else
        return false;
    }
    else if (columnIndex == 2) // joint height column
    {
      JointTypeEnum jointType = getPackagePinAt(rowIndex).getJointTypeEnum();
      if (ImageAnalysis.isSpecialTwoPinComponent(jointType))
        return false;
      if (_currentlySelectedPackagePins.contains(_packagePins.get(rowIndex)))
        return true;
      else
        return false;
    }
    else if (columnIndex == 3) // orientation column
    {
      PackagePin packagePin = _packagePins.get(rowIndex);
      if (_currentlySelectedPackagePins.contains(packagePin) == false)
        return false;
     
      //Added by Jack Hwee - If it is two pin compPackage, not allowed to change its rotation as it must be in tally orientation
      if (ProgramVerificationUtil.is2PinDeviceLandPattern(packagePin.getLandPatternPad().getLandPattern()))
        return false;
      // ok, the row is selected.  Now let this logic decide
      // if the joint type is of cap/res/pcap, then it's not editable
      //JointTypeEnum jointTypeEnum = packagePin.getJointTypeEnum();
      //***Chin-seong.Kee, i comment this as customer request us to enable it
      //if(ImageAnalysis.isSpecialTwoPinComponent(jointTypeEnum))
      //  return false;
      // if the pad does not have an orientation, then it's not editable
      PinOrientationEnum padOrientation = packagePin.getPinOrientationEnum();
      if (padOrientation.equals(PinOrientationEnum.NOT_APPLICABLE))
        return false;
      else
        return true;
    }
    else
      Assert.expect(false, "Column out of range: " + columnIndex);
    return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    PackagePin packagePin = _packagePins.get(rowIndex);
    if (columnIndex == 0)
    {
      return packagePin.getName();
    }
    else if (columnIndex == 1)
    {
      return packagePin.getJointTypeEnum();
    }
    else if (columnIndex == 2)
    {
      return new Double(MathUtil.convertUnits(packagePin.getJointHeightInNanoMeters(),
                                              MathUtilEnum.NANOMETERS,
                                              _project.getDisplayUnits()));
    }
    else
    {
      PinOrientationAfterRotationEnum padOrientation = packagePin.getPinOrientationAfterRotationEnum();
      return padOrientation;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    if (columnIndex == 0) // joint name -- not editable
      Assert.expect(false);
    else if (columnIndex == 1) // Joint Type assigned to the package pin
    {
      // change the packagePin's joint type assignment

      JointTypeEnum jointTypeEnum = (JointTypeEnum)aValue;
      PackagePin packagePin = _packagePins.get(rowIndex);
      // only allow this change if the throughhole/smt land pattern is compatible with the new joint type
      LandPatternPad landPatternPad = packagePin.getLandPatternPad();
      if (landPatternPad.isThroughHolePad() == false) // not through hole
      {
        if (ImageAnalysis.requiresThroughHoleLandPatternPad(jointTypeEnum))
        {
          // give error, and do not do the operation
          MessageDialog.showErrorDialog(
              MainMenuGui.getInstance(),
              StringLocalizer.keyToString("MMGUI_GROUPINGS_JOINT_TYPE_MUST_BE_THROUGHHOLE_KEY"),
              StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
              true);
          return;
        }
      }

      try
      {
        _commandManager.execute(new PackagePinSetJointTypeCommand(packagePin, jointTypeEnum));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    else if (columnIndex == 2)
    {
      Assert.expect(aValue instanceof Number);
      Double newJointHeight = ((Number)aValue).doubleValue();

      double maximumAllowedJointHeightInDisplayUnits = _project.getMaxAllowedJointHeightInDisplayUnits(_project.getDisplayUnits());
      if (newJointHeight <= 0 || newJointHeight > maximumAllowedJointHeightInDisplayUnits)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      StringLocalizer.keyToString(new LocalizedString("MMGUI_GROUPINGS_JOINT_HEIGHT_OUT_OF_ALLOWED_RANGE_ERROR_KEY",
                                                                                      new Object[]{maximumAllowedJointHeightInDisplayUnits,
                                                                                                   _project.getDisplayUnits().toString()})),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        return;
      }
      PackagePin packagePin = _packagePins.get(rowIndex);
      try
      {
        _commandManager.execute(new PackagePinSetJointHeightCommand(
            packagePin,
            MathUtil.convertDoubleToInt(MathUtil.convertUnits(newJointHeight, _project.getDisplayUnits(), MathUtilEnum.NANOMETERS))));
      }
      catch (ValueOutOfRangeException ex1)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex1.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);

        fireTableCellUpdated(rowIndex, columnIndex);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    else if (columnIndex == 3) // joint orientation
    {    
      PackagePin packagePin = _packagePins.get(rowIndex);
      PinOrientationAfterRotationEnum padOrientation = (PinOrientationAfterRotationEnum)aValue;
      try
      {
        _commandManager.execute(new PackagePinSetOrientationCommand(packagePin, padOrientation));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    else
      Assert.expect(false, "Column index out of range: " + columnIndex);
  }

  /**
   * @author Andy Mechtenberg
   */
  List<PackagePin> getSelectedPackagePins()
  {
    return _currentlySelectedPackagePins;
  }

  /**
   * @author Andy Mechtenberg
   */
  void setSelectedPackagePins(List<PackagePin> selectedPackagePins)
  {
    Assert.expect(selectedPackagePins != null);
    _currentlySelectedPackagePins = selectedPackagePins;
  }

  /**
   * @author George Booth
   */
  public int getRowForDataStartingWith(String key)
  {
    key = key.toLowerCase();
    int index = -1;
    for (PackagePin packagePin : _packagePins)
    {
      index++;
      String pinName = packagePin.getName().toLowerCase();
      if (pinName.startsWith(key))
      {
        return index;
      }
    }
    return -1;
  }
}
