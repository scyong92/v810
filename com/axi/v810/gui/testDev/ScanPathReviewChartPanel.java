package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.v810.hardware.StagePosition;

import org.jfree.chart.*;


/**
 *
 * @author chin-seong.kee
 */
class ScanPathReviewChartPanel extends JPanel
{
  private ScanPathPointEntryXPlot _plot;
  private JFreeChart _chart;
  private ChartPanel _chartContentPanel;
  
  private final boolean _SHOW_CHART_LEGEND = false;

  private List<StagePosition> _stagePositionEntries;
  
  ScanPathReviewChartPanel()
  {
    _stagePositionEntries = null;

    jbInit();
  }
  
  private void jbInit()
  { 
     _chartContentPanel = new ChartPanel(null);
     _chartContentPanel.setBorder(new EtchedBorder());
     
     // Call drawChart() to draw the chart, then set the chart in _chartContentPanel.
     // If there is no chart there, setMouseZoomable will throw exception.
     drawChart();
     
     _chartContentPanel.setMouseZoomable(true, false);
     _chartContentPanel.setDisplayToolTips(true);
     //_scanPathChartPanel.add(_chartContentPanel);  
     
    this.setLayout(new BorderLayout());
    this.add(_chartContentPanel, BorderLayout.CENTER);
  }
  
  void clearDataSet()
  {
    _stagePositionEntries = null;

    drawChart();
  }
  
  void populateDataSet(List<StagePosition> stagePositionEntries)
  {
    _stagePositionEntries = stagePositionEntries;
    
    drawChart();
  }
  
  void populateDataSet(List<StagePosition> stagePositionEntries, int seqNumber)
  {
    _stagePositionEntries = stagePositionEntries;
    
    drawChart(seqNumber);
  }
  
  private void drawChart()
  {
    _plot = new ScanPathPointEntryXPlot(_stagePositionEntries);
    _chart = new JFreeChart(_plot.getTitle(), JFreeChart.DEFAULT_TITLE_FONT, _plot, _SHOW_CHART_LEGEND);
    _chartContentPanel.setChart(_chart);
  }
  
  private void drawChart(int seqNumber)
  {
    _plot = new ScanPathPointEntryXPlot(_stagePositionEntries, seqNumber);
    _chart = new JFreeChart(_plot.getTitle(), JFreeChart.DEFAULT_TITLE_FONT, _plot, _SHOW_CHART_LEGEND);
    _chartContentPanel.setChart(_chart);
  }
  
  public int getScanPathsSize()
  {
    return _plot.getScanPathsSize();
  }
}
