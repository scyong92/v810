package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.text.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;


/**
 * This is the Coverage Options panel for the Test Development Tool
 * @author Andy Mechtenberg
 */
public class CoverageOptionsPanel extends JPanel
{
  private TestDev _testDev = null;
  private Project _project = null;

  private BorderLayout _overallBorderLayout = new BorderLayout();
  private JLabel _magDetailsLabel = new JLabel();
  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private JPanel _magTablePanel = new JPanel();
  private JPanel _shadingPanel = new JPanel();
  private BorderLayout _magTableBorderLayout = new BorderLayout();
  private JScrollPane _magTableScrollPane = new JScrollPane();
  private JSortTable _magTable;
  private ShadingTableModel _shadingTableModel;
  private JCheckBox _highMagCheckBox = new JCheckBox();
  private BorderLayout _shadingPanelBorderLayout = new BorderLayout();
  private JPanel _analyzeShadingButtonPanel = new JPanel();
  private JPanel _shadingTablePanel = new JPanel();
  private JButton _analyzeShadingButton = new JButton();
  private FlowLayout _analyzeShadingButtonFlowLayout = new FlowLayout();
  private BorderLayout _shadingTablePanelBorderLayout = new BorderLayout();
  private JLabel _shadingDetailsLabel = new JLabel();
  private JScrollPane _shadingTableScrollPane = new JScrollPane();
  private JTable _shadingTable;
  private JPanel _testTimePanel = new JPanel();
  private BorderLayout _testTimePanelBorderLayout = new BorderLayout();
  private JLabel _estimatedTestTimeLabel = new JLabel();
  private JLabel _coveragePercentLabel = new JLabel();
  private JPanel _recalcTestTimePanel = new JPanel();
  private JButton _recalcTestTimeButton = new JButton();
  private FlowLayout _recalcTestTimePanelFlowLayout = new FlowLayout();
  private Border _etchedBorder;
  private TitledBorder _testTimeAnalysisTitledBorder;
  private Border _testTimeBorder;
  /**
   * @author Andy Mechtenberg
   */
  public CoverageOptionsPanel(TestDev testDev)
  {
    Assert.expect(testDev != null);
    _testDev = testDev;
    jbInit();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    _etchedBorder = BorderFactory.createEtchedBorder(Color.white,new Color(142, 142, 142));
    _testTimeAnalysisTitledBorder = new TitledBorder(_etchedBorder,StringLocalizer.keyToString("MMGUI_COVERAGE_TEST_TIME_ANALYSIS_KEY"));
    _testTimeBorder = BorderFactory.createCompoundBorder(_testTimeAnalysisTitledBorder,BorderFactory.createEmptyBorder(5,5,5,5));
    _magDetailsLabel.setText(StringLocalizer.keyToString("MMGUI_COVERAGE_MAG_EXPLANATION_KEY"));
    setLayout(_overallBorderLayout);
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _magTablePanel.setLayout(_magTableBorderLayout);
    _highMagCheckBox.setText(StringLocalizer.keyToString("MMGUI_COVERAGE_PANEL_AT_HIGH_MAG_KEY"));
    _shadingPanel.setLayout(_shadingPanelBorderLayout);
    _analyzeShadingButton.setText(StringLocalizer.keyToString("MMGUI_COVERAGE_ANALYZE_SHADING_KEY"));
    _analyzeShadingButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        analyzeShadingButton_actionPerformed(e);
      }
    });

    _analyzeShadingButtonPanel.setLayout(_analyzeShadingButtonFlowLayout);
    _analyzeShadingButtonFlowLayout.setAlignment(FlowLayout.LEFT);
    _analyzeShadingButtonFlowLayout.setHgap(0);
    _analyzeShadingButtonFlowLayout.setVgap(0);
    _shadingTablePanel.setLayout(_shadingTablePanelBorderLayout);
    _shadingDetailsLabel.setBorder(BorderFactory.createEmptyBorder(0,0,5,0));
    _shadingDetailsLabel.setText(StringLocalizer.keyToString("MMGUI_COVERAGE_SHADING_EXPLANATION_KEY"));
    _testTimePanel.setLayout(_testTimePanelBorderLayout);
    _estimatedTestTimeLabel.setText(StringLocalizer.keyToString("MMGUI_COVERAGE_ESTIMATED_TEST_TIME_KEY"));
    _coveragePercentLabel.setText(StringLocalizer.keyToString("MMGUI_COVERAGE_COVERAGE_PERCENT_KEY"));
    _recalcTestTimeButton.setText(StringLocalizer.keyToString("MMGUI_COVERAGE_RECALCUATE_TEST_TIME_KEY"));
    _recalcTestTimeButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        recalcTestTimeButton_actionPerformed(e);
      }
    });
    _recalcTestTimePanel.setLayout(_recalcTestTimePanelFlowLayout);
    _recalcTestTimePanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _recalcTestTimePanelFlowLayout.setHgap(0);
    _recalcTestTimePanelFlowLayout.setVgap(0);
    _testTimePanel.setBorder(_testTimeBorder);
    _testTimePanelBorderLayout.setHgap(0);
    _testTimePanelBorderLayout.setVgap(10);
    _mainPanelBorderLayout.setVgap(10);
    _mainPanel.setBorder(BorderFactory.createEmptyBorder(5,0,30,0));
    _shadingTablePanel.setBorder(BorderFactory.createEmptyBorder(0,0,30,0));
    _shadingTablePanelBorderLayout.setHgap(0);
    _shadingTablePanelBorderLayout.setVgap(0);
    _magTablePanel.setBorder(null);
    setBorder(BorderFactory.createEmptyBorder(10,5,0,5));
    _shadingPanelBorderLayout.setVgap(10);
    add(_magDetailsLabel,  BorderLayout.NORTH);
    add(_mainPanel, BorderLayout.CENTER);
    _mainPanel.add(_magTablePanel, BorderLayout.NORTH);
    _mainPanel.add(_shadingPanel, BorderLayout.CENTER);
    _magTablePanel.add(_magTableScrollPane,  BorderLayout.CENTER);
    _magTable = new JSortTable();
    _magTableScrollPane.getViewport().add(_magTable, null);
    _magTablePanel.add(_highMagCheckBox,  BorderLayout.SOUTH);
    _shadingPanel.add(_analyzeShadingButtonPanel,  BorderLayout.NORTH);
    _shadingPanel.add(_shadingTablePanel, BorderLayout.CENTER);
    _analyzeShadingButtonPanel.add(_analyzeShadingButton, null);
    _shadingTableModel = new ShadingTableModel();
    _shadingTable = new JSortTable(_shadingTableModel);
    _shadingTablePanel.add(_shadingDetailsLabel,  BorderLayout.NORTH);
    _shadingTablePanel.add(_shadingTableScrollPane, BorderLayout.CENTER);
    _shadingTableScrollPane.getViewport().add(_shadingTable, null);
    _mainPanel.add(_testTimePanel,  BorderLayout.SOUTH);
    _testTimePanel.add(_estimatedTestTimeLabel, BorderLayout.NORTH);
    _testTimePanel.add(_coveragePercentLabel, BorderLayout.CENTER);
    _testTimePanel.add(_recalcTestTimePanel,  BorderLayout.SOUTH);
    /** @todo Put this back in if the recalulate routine takes over a second */
//    _recalcTestTimePanel.add(_recalcTestTimeButton, null);
  }

  /**
   * @author Andy Mechtenberg
   */
  void populateWithProjectData(Project project)
  {
    Assert.expect(project != null);
    _project = project;

    com.axi.v810.business.panelDesc.Panel panel = project.getPanel();
    int numTested = panel.getNumInspectedJoints();
    double testCoveragePct = ((double)numTested / (double)panel.getNumJoints()) * 100.0;
    DecimalFormat df = (DecimalFormat)DecimalFormat.getInstance();
    df.applyPattern(".#");
    _coveragePercentLabel.setText(StringLocalizer.keyToString("MMGUI_COVERAGE_COVERAGE_PERCENT_KEY") + " " + df.format(testCoveragePct));

    int testTimeInMilliSeconds = getEstimatedTestTimeInMillis();

    _estimatedTestTimeLabel.setText(StringLocalizer.keyToString("MMGUI_COVERAGE_ESTIMATED_TEST_TIME_KEY") + " " + testTimeInMilliSeconds / 1000 + " seconds");

// this outputs the time in the format HH:MM:SS which appears too accurate for an estimate
//    String testTimeStr = StringUtil.convertMilliSecondsToElapsedTime(testTimeInMilliSeconds, true);
//    _estimatedTestTimeLabel.setText(StringLocalizer.keyToString("MMGUI_COVERAGE_ESTIMATED_TEST_TIME_KEY") + " " + testTimeStr);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void analyzeShadingButton_actionPerformed(ActionEvent e)
  {
    _shadingTableModel.populateWithProjectData(_project);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void recalcTestTimeButton_actionPerformed(ActionEvent e)
  {
    int testTimeInMilliSeconds = getEstimatedTestTimeInMillis();
    _estimatedTestTimeLabel.setText(StringLocalizer.keyToString("MMGUI_COVERAGE_ESTIMATED_TEST_TIME_KEY") + " " + testTimeInMilliSeconds / 1000 + " seconds");
  }

  private void reCalculateTimeAndCoverage()
  {
    int testTimeInMilliSeconds = getEstimatedTestTimeInMillis();
    _estimatedTestTimeLabel.setText(StringLocalizer.keyToString("MMGUI_COVERAGE_ESTIMATED_TEST_TIME_KEY") + " " +
                                    testTimeInMilliSeconds / 1000 + " seconds");

    int numTested = _project.getPanel().getNumInspectedJoints();
    double testCoveragePct = ((double)numTested / (double)_project.getPanel().getNumJoints()) * 100.0;
    DecimalFormat df = (DecimalFormat)DecimalFormat.getInstance();
    df.applyPattern(".#");
    _coveragePercentLabel.setText(StringLocalizer.keyToString("MMGUI_COVERAGE_COVERAGE_PERCENT_KEY") + " " +
                                  df.format(testCoveragePct));

  }

  /**
   * @author Andy Mechtenberg
   */
  private int getEstimatedTestTimeInMillis()
  {
    /** @todo uncomment when test generation is less flaky */
//    TestExecutionEngine tee = TestExecutionEngine.getInstance();
//    return tee.getEstimatedTestTimeInMillis(_project);
    return 35000;
  }
}
