package com.axi.v810.gui.testDev;

import java.awt.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.JointTypeEnum;
import com.axi.v810.business.imageAnalysis.ImageAnalysis;

/**
 * This class displays the information for all the packages shown in Verify CAD
 * @author George Booth
 */

class PackageJointTypeTable extends JSortTable
{
  public static final int _PACKAGE_NAME_INDEX = 0;
  public static final int _PACKAGE_JOINT_TYPE_INDEX = 1;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _PACKAGE_NAME_COLUMN_WIDTH_PERCENTAGE = .50;
  private static final double _PACKAGE_JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE = .50;

  private ModifySubtypesPackageTableModel _tableModel;
  private boolean _modelSet = false;

  /**
   * @author George Booth
   */
  public PackageJointTypeTable(PackageJointTypeTableModel model)
  {
    Assert.expect(model != null);
    _tableModel = model;
    _modelSet = true;
    super.setModel(model);

//    setRowSelectionAllowed(true);
//    setColumnSelectionAllowed(true);
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author George Booth
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_PACKAGE_NAME_INDEX).setPreferredWidth((int)(totalColumnWidth * _PACKAGE_NAME_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PACKAGE_JOINT_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _PACKAGE_JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * @author George Booth
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };

    // column 0 is the package name
    columnModel.getColumn(_PACKAGE_NAME_INDEX).setCellEditor(stringEditor);

    // column 1 is the joint type
    JComboBox jointTypeComboBox = new JComboBox();
    JointTypeEnumCellEditor jointTypeEnumCellEditor = new JointTypeEnumCellEditor(jointTypeComboBox, _tableModel);
    columnModel.getColumn(_PACKAGE_JOINT_TYPE_INDEX).setCellEditor(jointTypeEnumCellEditor);
  }

  /**
   * @author George Booth
   */
  public void clearSelection()
  {
    TableModel model = getModel();
    // this function gets called before the table model is set, during intialization. At that time,
    // the model will be a defaultTableModel so the assert is sure to fail
    if (_modelSet)
    {
      Assert.expect(model instanceof PackageJointTypeTableModel);
      ((PackageJointTypeTableModel)model).clearSelectedPackage();
    }
    super.clearSelection();
  }

  /**
   * @author George Booth
   */
  void cancelCellEditing()
  {
    TableCellEditor cellEditor = getColumnModel().getColumn(_PACKAGE_JOINT_TYPE_INDEX).getCellEditor();
    if (cellEditor != null)
    {
      cellEditor.cancelCellEditing();
    }
  }

}
