/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.testDev;


import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.TestSubProgram;
import com.axi.v810.util.*;

/**
 *
 * @author chin-seong.kee
 */
public class ScanPathReviewTable extends JSortTable
{
  public static final int _SCAN_PATH_REVIEW_NUMBER_OF_LOCATION_INDEX = 0;  
  public static final int _SCAN_PATH_REVIEW_X_LOCATION_INDEX = 1;
  public static final int _SCAN_PATH_REVIEW_Y_LOCATION_INDEX = 2;
 
  private static final int _COMPONENT_UNDO_INDEX = 0;

  private ScanPathReviewTableModel _scanPathReviewTableModel;
  private TestSubProgram _currentTestSubProgram;

  private NumericRenderer _numericRenderer = new NumericRenderer(2, JLabel.RIGHT);
  private NumericEditor _xNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.RIGHT, 0, 0, Double.MAX_VALUE);
  private NumericEditor _yNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.RIGHT, 0, 0, Double.MAX_VALUE);
  //private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  /**
   * @param model
   */
  public ScanPathReviewTable(ScanPathReviewTableModel model)
  {
    Assert.expect(model != null);

    _scanPathReviewTableModel = model;
    super.setModel(model);
  }

  /**
   * @return
   */
  public void setModel(ScanPathReviewTableModel componentTableModel)
  {
    Assert.expect(componentTableModel != null);

    _scanPathReviewTableModel = componentTableModel;
    super.setModel(_scanPathReviewTableModel);
  }

  /**
   * @return
   */
  public ScanPathReviewTableModel getTableModel()
  {
    Assert.expect(_scanPathReviewTableModel != null);
    return _scanPathReviewTableModel;
  }

  /**
   *
   */
  public void setupCellEditorsAndRenderers()
  {
   TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };
    columnModel.getColumn(_SCAN_PATH_REVIEW_NUMBER_OF_LOCATION_INDEX).setCellEditor(stringEditor);

    columnModel.getColumn(_SCAN_PATH_REVIEW_X_LOCATION_INDEX).setCellEditor(_xNumericEditor);
    columnModel.getColumn(_SCAN_PATH_REVIEW_X_LOCATION_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(_SCAN_PATH_REVIEW_Y_LOCATION_INDEX).setCellEditor(_yNumericEditor);
    columnModel.getColumn(_SCAN_PATH_REVIEW_Y_LOCATION_INDEX).setCellRenderer(_numericRenderer);   
  }

  /**
   * Overriding this method allows us to have one cell's value change apply
   * to every selected row in the table.  Multi-cell editing requires this.
   * @author Kee chin Seong
   */
  public void setValueAt(Object value, int row, int column)
  {
    // if there is no value, exit out here -- there is no work to do
    if (value == null)
      return;
    componentTableSetValues(value, row, column);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void componentTableSetValues(Object value, int row, int column)
  {
    // Temporary do nothing
  }

  /**
   * @return the _currentBoardType
   */
  public TestSubProgram getCurrentTestSubProgram()
  {
    return _currentTestSubProgram;
  }

  /**
   * @param currentBoardType the _currentBoardType to set
   */
  public void setCurrentTestSubProgram(TestSubProgram currentTestSubProgram)
  {
    this._currentTestSubProgram = currentTestSubProgram;
  }

  /**
   * @author Erica Wheatcroft
   */
  private UndoState getCurrentUndoState()
  {
    // Temporary do nothing
    return null;
  }  
}
