/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.testDev;

/**
 * @author Ying-Huan.Chu
 */
public class OpticalCameraRectangleComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  
  public static OpticalCameraRectangleComparatorEnum BOARD_NAME = new OpticalCameraRectangleComparatorEnum(++_index);
  public static OpticalCameraRectangleComparatorEnum NAME = new OpticalCameraRectangleComparatorEnum(++_index);
  public static OpticalCameraRectangleComparatorEnum Z_HEIGHT = new OpticalCameraRectangleComparatorEnum(++_index);
  public static OpticalCameraRectangleComparatorEnum USABILITY_STATUS = new OpticalCameraRectangleComparatorEnum(++_index);
  public static OpticalCameraRectangleComparatorEnum COORDINATE = new OpticalCameraRectangleComparatorEnum(++_index);

  /**
   * @author Laura Cormos
   */
  private OpticalCameraRectangleComparatorEnum(int id)
  {
    super(id);
  }
}
