package com.axi.v810.gui.testDev;

import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import java.util.*;

/**
 *
 * @author siew-yeng.phang
 */
public class SerialNumberMappingTable extends JSortTable
{
  public static final int _BOARD_NAME_INDEX = 0;
  public static final int _BARCODE_READER_ID_INDEX = 1;
  public static final int _SERIAL_NUMBER_SCAN_SEQUENCE_INDEX = 2;
  
  private SerialNumberMappingTableModel _serialNumberMappingTableModel;
  private ListSelectionCellEditor _boardNameEditor;
  private ListSelectionCellEditor _barcodeReaderIdEditor;
  private ListSelectionCellEditor _serialNumberScanSequenceEditor;
  
  private List<Object> _boardNameList = null;
  private List<Object> _barcodeReaderIdList = null;
  private List<Object> _serialNumberScanSequenceList = new ArrayList<Object>();
  private int _numberOfScanner = 0;
  
  /**
   * @author Phang Siew Yeng
   */
  public SerialNumberMappingTable(SerialNumberMappingTableModel model)
  {
    Assert.expect(model != null);

    _serialNumberMappingTableModel = model;
    
    super.setModel(model);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public void setModel(SerialNumberMappingTableModel serialNumberMappingTableModel)
  {
    Assert.expect(_serialNumberMappingTableModel != null);
    
    _serialNumberMappingTableModel = serialNumberMappingTableModel;
    super.setModel(_serialNumberMappingTableModel);
  }

  /**
   * @author Phang Siew Yeng
   */
  public SerialNumberMappingTableModel getTableModel()
  {
    Assert.expect(_serialNumberMappingTableModel != null);
    return _serialNumberMappingTableModel;
  }

  /**
   * @author Phang Siew Yeng
   */
  public void setBoardNames(List<String> boardNames)
  {
    _boardNameList = new ArrayList<Object>(boardNames);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public void setNumberOfScanner(int numberOfScanner)
  {
    _numberOfScanner = numberOfScanner;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  private void updateList()
  {
    Assert.expect(_numberOfScanner != 0);
    
    _serialNumberScanSequenceList = new ArrayList<Object>();
    _barcodeReaderIdList = new ArrayList<Object>();
    
    for(int n = 0; n < _boardNameList.size(); n++)
    {
      _serialNumberScanSequenceList.add(n + 1);
    }
    
    for(int i = 0; i < _numberOfScanner; i++)
    {
      _barcodeReaderIdList.add(i + 1);
    }
    
//    for(SerialNumberMappingData data: _serialNumberMappingTableModel.getData())
//    {
//      if(_barcodeReaderIdList.contains(data.getBarcodeReaderId()) ==false)
//        _barcodeReaderIdList.add(data.getBarcodeReaderId());
      
      //_boardNameList.add(data.getBoardName());
      
//      if(_serialNumberSequenceList.contains(data.getSerialNumberSequence()) ==false)
//        _serialNumberSequenceList.add(data.getSerialNumberSequence());
//    }
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public void setupCellEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();
    
    updateList();
    
    _boardNameEditor = new ListSelectionCellEditor(_boardNameList);
    _barcodeReaderIdEditor = new ListSelectionCellEditor(_barcodeReaderIdList);
    _serialNumberScanSequenceEditor = new ListSelectionCellEditor(_serialNumberScanSequenceList);
    
    columnModel.getColumn(_BARCODE_READER_ID_INDEX).setCellEditor(_barcodeReaderIdEditor);
    columnModel.getColumn(_SERIAL_NUMBER_SCAN_SEQUENCE_INDEX).setCellEditor(_serialNumberScanSequenceEditor);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public boolean editCellAt(int row, int column, EventObject e)
  {    
    if(_barcodeReaderIdList.isEmpty() ||
      _boardNameList.isEmpty() ||
      _serialNumberScanSequenceList.isEmpty())
      updateList();
    
    if (column == _BOARD_NAME_INDEX)
    {
      // want to update the list of existing projects every time the project name cell gets selected
      // to keep the projects list up to date with what's on disk
      _boardNameEditor.setData(_boardNameList);
    }
    else if (column == _BARCODE_READER_ID_INDEX)
    {
      // want to update the list of existing projects every time the project name cell gets selected
      // to keep the projects list up to date with what's on disk
      _barcodeReaderIdEditor.setData(_barcodeReaderIdList);
    }
    else if (column == _SERIAL_NUMBER_SCAN_SEQUENCE_INDEX)
    {
      // want to update the list of existing projects every time the project name cell gets selected
      // to keep the projects list up to date with what's on disk
      _serialNumberScanSequenceEditor.setData(_serialNumberScanSequenceList);
    }
    return super.editCellAt(row, column, e);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public void clear()
  {
    _boardNameList.clear();
    _barcodeReaderIdList.clear();
    _serialNumberScanSequenceList.clear();
  }
}
