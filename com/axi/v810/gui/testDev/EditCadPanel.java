package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;
import java.text.*;

/**
 * This is the Edit CAD panel for the Test Development Tool
 * @author Laura Cormos
 */
public class EditCadPanel extends AbstractTaskPanel implements Observer
{
  private Color _labelColor = new Color(0,70,213); //trying to match the blue of the panel titles

  // menu items
  private JMenuItem _editHoleLocationMenuItem = null;
  private JMenuItem _makePadOneMenuItem = null;

  private TestDev _testDev = null;
  private ImageManagerPanel _imagePanel;
  private MainMenuGui _mainUI;
  private boolean _shouldInteractWithGraphics = false;
  private java.util.List<LandPattern> _landPatterns;
  private Project _project = null;
  private boolean _updateData = false;
  private boolean _populateProjectData = true;
  private TestDevPersistance _uiPersistSettings;
  private BoardType _currentBoardType = null;
  private boolean _landPatternTabIsVisible = false;
  private boolean _verifyCadTabIsVisible = true;
  private Board _boardToAddTo = null;
  private java.util.List<com.axi.v810.business.panelDesc.Component> _componentsFoundOnRightClick =
      new ArrayList<com.axi.v810.business.panelDesc.Component>();
  private java.util.List<Fiducial> _fiducialsFoundOnRightClick = new ArrayList<Fiducial>();
  private com.axi.v810.business.panelDesc.Panel _panelFoundOnRightClick = null;
  private LandPatternPad _landPatternPadFoundOnRightClick = null;
  private MathUtilEnum _enumSelectedUnits = null;

  // every time a Renderer is selected the Observable will update any Observers to tell them it was selected
  private GuiObservable _guiObservable = GuiObservable.getInstance();

  private SelectedRendererObservable _panelGraphicsSelectedRendererObservable;
  private MouseClickObservable _panelGraphicsMouseClickObservable;
  private SelectedRendererObservable _landPatternGraphicsSelectedRendererObservable;
  private MouseClickObservable _landPatternGraphicsMouseClickObservable;

  // command manager for undo functionality
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  // actionlisteners
  private ActionListener _selectLandPatternActionListener;

  // GUI elements
  private BorderLayout _editCadBorderLayout = new BorderLayout();
  private JPanel _editLandPatternPanel = new JPanel();
  private JPanel _editLandPatternDetailsPanel = new JPanel();
  private BorderLayout _editLandPatternDetailsPanelBorderLayout = new BorderLayout();
  private JPanel _editLandPatternInfoPanel = new JPanel();
  private BorderLayout _editLandPatternInfoPanelBorderLayout = new BorderLayout();
  private JPanel _newComponentPanel = new JPanel();
  private BorderLayout _newComponentPanelBorderLayout = new BorderLayout();
  private BorderLayout _editLandPatternPanelBorderLayout = new BorderLayout();
  private JScrollPane _newComponentScrollPane = new JScrollPane();
  private NewComponentTableModel _newComponentTableModel;
  private NewFiducialTableModel _newFiducialTableModel;
  private NewComponentTable _newComponentDetailsTable;
  private JPanel _newFiducialPanel = new JPanel();
  private BorderLayout _newFiducialPanelBorderLayout = new BorderLayout();
  private JScrollPane _newFiducialScrollPane = new JScrollPane();
  private NewFiducialTable _newFiducialDetailsTable;
  private JPanel _landPatternSelectPanel = new JPanel();
  private JPanel _landPatternDetailsPanel = new JPanel();
  private JPanel _landPatternActionsPanel = new JPanel();
  private FlowLayout _landPatternSelectFlowLayout = new FlowLayout();
  private BorderLayout _landPatternDetailsPanelBorderLayout = new BorderLayout();
  private GridLayout _landPatternActionsPanelGridLayout = new GridLayout();
  private JButton _addPadButton = new JButton();
  private JButton _copyToButton = new JButton();
  private JButton _addPadArrayButton = new JButton();
  private JScrollPane _landPatternDetailsScrollPane = new JScrollPane();
  private LandPatternDetailsTableModel _landPatternDetailsTableModel;
  private LandPatternDetailsTable _landPatternDetailsTable;
  private JComboBox _selectLandPatternComboBox = new JComboBox();
  private JLabel _selectLandPatternLabel = new JLabel();
  private JPanel _addComponentPanel = new JPanel();
  private FlowLayout _addComponentPanelFlowLayout = new FlowLayout();
  private JPanel _newComponentAndFiducialPanel = new JPanel();
  private BorderLayout _newComponentAndFiducialPanelBorderLayout = new BorderLayout();
  private JPanel _newComponentDetailsPanel = new JPanel();
  private BorderLayout _newComponentDetailsPanelBorderLayout = new BorderLayout();
  private JPanel _addFiducialPanel = new JPanel();
  private FlowLayout _addFiducialPanelFlowLayout = new FlowLayout();
  private JPanel _newFiducialDetailsPanel = new JPanel();
  private BorderLayout _newFiducialDetailsPanelBorderLayout = new BorderLayout();
  private JPanel _boardAndLandPatternSelectPanel = new JPanel();
  private BorderLayout _boardAndLandPatternSelectPanelBorderLayout = new BorderLayout();
  private JPanel _newComponentActionsPanel = new JPanel();
  private GridLayout _newComponentActionsPanelGridLayout = new GridLayout();
  private JButton _deleteComponentButton = new JButton();
  private JButton _addComponentButton = new JButton();
  private JSplitPane _compFiducialSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true);
  private JPanel _newComponentActionsWrapperPanel = new JPanel();
  private FlowLayout _newComponentActionsWrapperPanelFlowLayout = new FlowLayout();
  private JPanel _landPatternActionsWrapperPanel = new JPanel();
  private FlowLayout _landPatternActionsWrapperPanelFlowLayout = new FlowLayout();
  private JButton _deletePadButton = new JButton();
  private JButton _deleteUnusedLandPatternButton = new JButton();
  private JTabbedPane _editCadTabbedPane = new JTabbedPane();
  private JPanel _newFiducialActionsWrapperPanel = new JPanel();
  private JPanel _newFiducialActionsPanel = new JPanel();
  private FlowLayout _newFiducialActionsWrapperPanelFlowLayout = new FlowLayout();
  private GridLayout _newFiducialActionsPanelGridLayout = new GridLayout();
  private JButton _deleteFiducialButton = new JButton();
  private JButton _addFiducialButton = new JButton();
  private JButton _changePadNameButton = new JButton();
  private JLabel _howToAddComponentLabel = new JLabel();
  private JLabel _howToAddFiducialLabel = new JLabel();
  private JLabel _howToEditLandPatternPad = new JLabel();
  private VerifyCadPanel _verifyCadPanel = null;
  
  //Ying-Huan.Chu
  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  private LandPattern _currentLandPattern;
  private int _firstRow = -1, _lastRow = -1;

  // Bee Hoon - To prevent crash when undo from other environment tabs
  private boolean isUndoLandPattern = false;
  
  private EditFocusRegionPanel _editFocusRegionPanel = null;
  private boolean _editFocusRegionVisible = false;
  
  //Siew Yeng - XCR-3781
  private EditPshSettingsPanel _editPshSettingsPanel = null;
  private boolean _editPshSettingsVisible = false;
  
  //Ngie Xing
  private final int _VERIFY_CAD = 0;
  private final int _EDIT_LAND_PATTERNS = 1;
  private final int _EDIT_COMPONENTS_FIDUCIALS = 2;
  private final int _EDIT_FOCUS_REGION = 3;
  private final int _EDIT_PSH_SETTINGS = 4;//Siew Yeng - XCR-3781
  
  java.util.List<LandPatternPad> _newHighlightedlandPatternPads = new ArrayList<LandPatternPad>();
  
  //Khaw Chek Hau - XCR3471: System crash when switch tab after create new land pattern
  private boolean _inLandPatternUndoAction = false;

  //Khaw Chek Hau - XCR3554: Package on package (PoP) development
  private com.axi.v810.business.panelDesc.Component _popBaseComponent;
  
  /**
   * @author Laura Cormos
   */
  public EditCadPanel(TestDev testDev, ImageManagerPanel imagePanel)
  {
    super(testDev);
    Assert.expect(testDev != null);
    Assert.expect(imagePanel != null);

    _testDev = testDev;
    _mainUI = _testDev.getMainUI();
    _imagePanel = imagePanel;

    jbInit();
  }

  /**
    * Determine if the given land pattern is used by a 2 pin device (cap, res or pcap)
    * @author Laura Cormos
    */
   public static boolean is2PinDeviceLandPattern(LandPattern landPattern)
   {
     Assert.expect(landPattern != null);

     if (landPattern.getLandPatternPads().size() == 2)
     {
       java.util.List<ComponentType> compTypesForLP = landPattern.getComponentTypes();
       for (ComponentType ct : compTypesForLP)
       {
         java.util.List<JointTypeEnum> jointTypeEnumsForCompType = ct.getJointTypeEnums();
         for (JointTypeEnum jte : jointTypeEnumsForCompType)
         {
           if (ImageAnalysis.isSpecialTwoPinComponent(jte))
           {
             return true;
           }
         }
       }
     }
     return false;
   }

  /**
   * @author Laura Cormos
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof SelectedRendererObservable)
          handleSelectedRendererEvent(object);
        else if (observable instanceof ProjectObservable)
          handleProjectDataEvent(object);
        else if (observable instanceof MouseClickObservable)
          handleMouseClickEvent(object);
        else if (observable instanceof GuiObservable)
          handleGuiChangesEvent(object);
        else if (observable instanceof com.axi.v810.gui.undo.CommandManager)
          handleUndoState(object);
        else
          Assert.expect(false);
      }
    });
  }

  /**
   * @author Laura Cormos
   */
  private void handleUndoState(final Object stateObject)
  {
    if (_currentLandPattern != null)
    {
      _currentLandPattern.assignAll();
    }
    _newHighlightedlandPatternPads.clear();
    UndoState undoState = (UndoState)stateObject;
    int selectedIndex = undoState.getAdjustCadTabbedPaneIndex();
    _editCadTabbedPane.setSelectedIndex(selectedIndex);
    if (selectedIndex == 1) // land pattern tab
    {
      //Khaw Chek Hau - XCR3471: System crash when switch tab after create new land pattern
      _inLandPatternUndoAction = true;
      cancelEditingIfNecessary(_landPatternDetailsTable);
      int landPatternIndex = undoState.getLandPatternComboBoxIndex();
      if (landPatternIndex == 0) // this is the New... option
        landPatternIndex++; // set the combo box to the next LP or else it looks like we want to create a new LP
      // only set the combo box index if one exists beyond the "New..." option
      if (landPatternIndex < _selectLandPatternComboBox.getItemCount())
      {
        // set the landpattern to the one from the Undo state object if one exists
        if (undoState.isLandPatternComboBoxNameSet())
        {
          _currentLandPattern = _project.getPanel().getLandPattern(undoState.getLandPatternComboBoxName());
          _selectLandPatternComboBox.setSelectedItem(_project.getPanel().getLandPattern(undoState.getLandPatternComboBoxName()));
        }
        // if not, set it to the next land pattern in the list
        else
        {
          _currentLandPattern = (LandPattern) _selectLandPatternComboBox.getItemAt(landPatternIndex);
          _selectLandPatternComboBox.setSelectedIndex(landPatternIndex);
        }
        
        setDeletePadButtonState((LandPattern)_selectLandPatternComboBox.getSelectedItem());
      }
    }
    else if (selectedIndex == 2) // components and fiducials tab
    {
      cancelEditingIfNecessary(_newComponentDetailsTable);
      cancelEditingIfNecessary(_newFiducialDetailsTable);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void handleSelectedRendererEvent(Object object)
  {
    // ok.  We got notice that something was selected.  If verify CAD is visible, send
    // the event to it, otherwise handle it locally. If it was a LandPatternPadRenderer,
    // we want to select in the _landPatternDetailsTable the row that corresponds to that pad
    if (_verifyCadTabIsVisible)
    {
      _verifyCadPanel.handleSelectedRendererEvent(object);
    }
    else if (_landPatternTabIsVisible)
    {
      _landPatternDetailsTable.clearSelection();
      _landPatternPadFoundOnRightClick = null;
    }
    else if (_editFocusRegionVisible)
    {
      _editFocusRegionPanel.handleSelectedRendererEvent(object);
    }
    else
    {
      _newComponentDetailsTable.clearSelection();
      _newFiducialDetailsTable.clearSelection();
      _componentsFoundOnRightClick.clear();
      _fiducialsFoundOnRightClick.clear();
      _boardToAddTo = null;
      _panelFoundOnRightClick = null;
    }
    // turn off auto-updating of the graphics which happens when table selections are changed.
    // Since this action happened because of a graphical selection, it should override
    _shouldInteractWithGraphics = false;

    // We receive from the update, a collection of selected objects.
    /** Warning "unchecked cast" approved for cast from Object type
     *  when sent from SelectedRendererObservable notifyAll.  */
    for (com.axi.guiUtil.Renderer renderer : ((Collection < com.axi.guiUtil.Renderer > )object))
    {
//          System.out.println("Found renderer:" + renderer);
      if (_landPatternTabIsVisible) // look only for LandPatternPadRenderers
      {
        if (renderer instanceof LandPatternPadRenderer)
        {
          LandPatternPadRenderer padRenderer = (LandPatternPadRenderer)renderer;
          LandPatternPad landPatternPad = padRenderer.getLandPatternPad();
          int rowPosition = _landPatternDetailsTableModel.getRowForLandPatternPad(landPatternPad);
          _landPatternDetailsTable.addRowSelectionInterval(rowPosition, rowPosition);

          // remember the pad that was clicked on
          _landPatternPadFoundOnRightClick = landPatternPad;
        }
      }
      else // we are in component and fiducial view, so we look for panel, board, component and fiducial renderers
      {
        if (renderer instanceof BoardRenderer)
        {
          _boardToAddTo = ((BoardRenderer)renderer).getBoard();
        }
        else if (renderer instanceof ComponentRenderer)
        {
          com.axi.v810.business.panelDesc.Component comp = ((ComponentRenderer)renderer).getComponent();
          _componentsFoundOnRightClick.add(comp);
//              System.out.println("Adding component: " + comp.getReferenceDesignator() +
//                                 " to list of comps found on rightClick");
          int rowPosition = _newComponentTableModel.getRowForComponent(comp);
          if (rowPosition >= 0)
          {
            _newComponentDetailsTable.addRowSelectionInterval(rowPosition, rowPosition);
            _newComponentDetailsTable.requestFocus();
          }
        }
        else if (renderer instanceof FiducialRenderer)
        {
          FiducialRenderer fiducialRenderer = (FiducialRenderer)renderer;
          Fiducial fiducial = fiducialRenderer.getFiducial();
          _fiducialsFoundOnRightClick.add(fiducial);
          int rowPosition = _newFiducialTableModel.getRowForFiducial(fiducial);
          if (rowPosition >= 0)
          {
            _newFiducialDetailsTable.addRowSelectionInterval(rowPosition, rowPosition);
            _newFiducialDetailsTable.requestFocus();
          }
        }
        else if (renderer instanceof PanelRenderer)
        {
          _panelFoundOnRightClick = ((PanelRenderer)renderer).getPanel();
        }
      }
    }
    if (_landPatternTabIsVisible)
    {
      // after everything is selected, scroll to make the first selection visible.
      SwingUtils.scrollTableToShowSelection(_landPatternDetailsTable);
    }
    else
    {
      // after everything is selected, scroll to make the first selection visible.
      SwingUtils.scrollTableToShowSelection(_newFiducialDetailsTable);
      SwingUtils.scrollTableToShowSelection(_newComponentDetailsTable);
    }
    _shouldInteractWithGraphics = true;
  }

  /**
   * @author Laura Cormos
   */
  private void handleProjectDataEvent(Object object)
  {
    Assert.expect(object != null);

    if (_active == false)
      _updateData = true;
    else
    {
      if (object instanceof ProjectChangeEvent)
      {
        ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)object;
        ProjectChangeEventEnum projectChangeEventEnum = projectChangeEvent.getProjectChangeEventEnum();
        if ((projectChangeEventEnum instanceof LandPatternEventEnum) ||
            (projectChangeEventEnum instanceof LandPatternPadEventEnum) ||
            (projectChangeEventEnum instanceof ThroughHoleLandPatternPadEventEnum) ||
            (projectChangeEventEnum instanceof ComponentEventEnum) ||
            (projectChangeEventEnum instanceof ComponentTypeEventEnum) ||
            (projectChangeEventEnum instanceof FiducialEventEnum) ||
            (projectChangeEventEnum instanceof FiducialTypeEventEnum) ||
            (projectChangeEventEnum instanceof ProjectEventEnum) ||
            (projectChangeEventEnum instanceof ProjectStateEventEnum))
        {
          // anything here should cause an update
          updateData(projectChangeEvent);
        }
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void handleMouseClickEvent(final Object object)
  {
    Assert.expect(object != null);

    if (_verifyCadTabIsVisible)
    {
      return;
    }
    MouseEvent mouseEvent = (MouseEvent)object;
    final int pixelX = mouseEvent.getX();
    final int pixelY = mouseEvent.getY();
    if (SwingUtilities.isLeftMouseButton(mouseEvent))
    {
      // do nothing (there is no special handling of left mouse clicks)
    }
    else if (SwingUtilities.isRightMouseButton(mouseEvent))
    {
      // handle right click with a popup menu
      if (_shouldInteractWithGraphics)
      {
        JPopupMenu popup = new JPopupMenu("Actions"); // this name is never seen by the users
        // convert screen pixel coordinates into nanometers
        Point point = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().
                      convertFromMousePixelToRendererPixel(pixelX, pixelY);
        final Point2D graphicCoordInNanoMeters = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().
                                                 convertFromPixelToRendererCoordinates((int)point.getX(), (int)point.getY());

        //Ngie Xing, allowed only on EditComponents/Fiducials tab
        if (_editCadTabbedPane.getSelectedIndex() == _EDIT_COMPONENTS_FIDUCIALS)  //_landPatternTabIsVisible == false)
        {
          // handle right clicks in the component and fiducial tab, panel graphics engine
          boolean foundValidComponentsOrFiducials = false;
          //Ngie Xing, delete multiple fiducials
          if (_fiducialsFoundOnRightClick != null && _fiducialsFoundOnRightClick.isEmpty() == false)
          {
            // context menu for existing fiducial modifications
            int i = 0;
            for (Fiducial fid : _fiducialsFoundOnRightClick)
            {
              boolean fiducialExists = false;
              if (i > 0)
                popup.addSeparator();
              if (fid.isOnPanel())
              {
                if (fid.getPanel().hasFiducial(fid.getName()))
                  fiducialExists = true;
              }
              else
              {
                if (fid.getSideBoard().hasFiducial(fid.getName()))
                  fiducialExists = true;
              }
              if (fiducialExists)
              {
                foundValidComponentsOrFiducials = true;
                JMenuItem deleteFidMenuItem = createDeleteFiducialMenuItem(fid);
                JMenuItem renameFidMenuItem = createRenameFducialMenuItem(fid);
                popup.add(deleteFidMenuItem);
                popup.add(renameFidMenuItem);
                i++;
              }
            }
            if (_componentsFoundOnRightClick.isEmpty() == false)
            {
              popup.addSeparator();
              popup.addSeparator();
            }
          }
          //Ngie Xing, June2014, removed 'else' to let user to delete fiducials and components at the same time
          if (_componentsFoundOnRightClick.isEmpty() == false)
          {
            foundValidComponentsOrFiducials = true;

            //Ngie Xing XCR1559 - Able to delete multiple components at once
            int componentsFound = _componentsFoundOnRightClick.size();
            //if more than 3, only show the menu item for multiple delete
            /*if (componentsFound > 3)
            {
              JMenuItem deleteCompMenuItem = createDeleteCompMenuItem(null);
              JScrollPane pane = new JScrollPane();
              pane.getViewport().add(deleteCompMenuItem, null);
              popup.add(pane);
            }
            //if less than 3, add an additional menu item for multiple delete
            else
            {*/
              // context menu for existing component modifications
              int i = 0;
              for (com.axi.v810.business.panelDesc.Component comp : _componentsFoundOnRightClick)
              {
                JScrollPane pane = new JScrollPane();
                if (i > 0)
                  popup.addSeparator();
                if (comp.getSideBoard().getSideBoardType().hasComponentType(comp.getComponentType()))
                {
                  JMenuItem deleteCompMenuItem = createDeleteCompMenuItem(comp);
                  JMenuItem renameCompMenuItem = createRenameCompMenuItem(comp);
                  JMenuItem changeLandPatternForCompMenuItem = changeLandPatternForCompMenuItem(comp);
                  //Khaw Chek Hau - XCR3554: Package on package (PoP) development
                  JMenuItem addNewPOPForCompMenuItem = addNewPOPForCompMenuItem(comp);
                  
                  
                  pane.getViewport().add(deleteCompMenuItem, null);
                  pane.getViewport().add(renameCompMenuItem, null);
                  pane.getViewport().add(changeLandPatternForCompMenuItem, null);
                  pane.getViewport().add(addNewPOPForCompMenuItem, null);
                  
                  popup.add(pane);
                  //popup.add(renameCompMenuItem);
                  //popup.add(changeLandPatternForCompMenuItem);
                  //popup.add(addNewPOPForCompMenuItem);
                  i++;
                }
              }
              //Ngie Xing, add a menuItem for deleting multiple components
              if (componentsFound > 1)
              {
                popup.addSeparator();
                popup.addSeparator();
                JMenuItem deleteCompMenuItem = createDeleteCompMenuItem(null);
                popup.add(deleteCompMenuItem);
              }
            }
         // }
          else if (_panelFoundOnRightClick != null && _boardToAddTo == null)
          { // clicked on a panel, outside of a board outline, therefore we can only add a panel fiducial
            boolean isPanelTopSide = true;
            // determine which side of the panel the user clicked on by asking the graphics engine which side is visible
            if (_testDev.getPanelGraphicsEngineSetup().isTopSideVisible() == false)
            {
              isPanelTopSide = false;
            }
            final boolean topSideFiducial = isPanelTopSide;

            PanelCoordinate rendererCoordinate = new PanelCoordinate(
                (int)graphicCoordInNanoMeters.getX(), (int)graphicCoordInNanoMeters.getY());
            PanelCoordinate newCoordInNanoMeters = _project.getPanel().getPanelCoordinate(rendererCoordinate);

            JMenuItem addFidMenuItem = createAddPanelFidMenuItem(topSideFiducial, newCoordInNanoMeters);
            popup.add(addFidMenuItem);
          }
          if (_boardToAddTo != null && foundValidComponentsOrFiducials == false && _editFocusRegionPanel.isStarted() == false)
          { // clicked on a board but not a comp or fid, therefore we can only add one or the other
            SideBoard sideBoard = null;
            // determine which side of the board the user clicked on by asking the graphics engine which side is visible
            // if both sides are visible we are going to default to adding the item to the top side, however, the user
            // will get one more chance to change that choice when they enter the item's name
            if (_testDev.getPanelGraphicsEngineSetup().isTopSideVisible())
            {
              if (_boardToAddTo.topSideBoardExists() == false)
                _boardToAddTo.createSecondSideBoard();

              sideBoard = _boardToAddTo.getTopSideBoard();
            }
            else
            {
              if (_boardToAddTo.bottomSideBoardExists() == false)
                _boardToAddTo.createSecondSideBoard();

              sideBoard = _boardToAddTo.getBottomSideBoard();
            }
            // determine where on the board the user clicked on by defining the new coordinates in board space
            final SideBoardType sideBoardType = sideBoard.getSideBoardType();

            // now calculate the real coordinate relative to the board's CAD origin
            // based on all the exising board and panel rotations and/or flips
            BoardCoordinate rendererCoordinate = new BoardCoordinate(
                (int)graphicCoordInNanoMeters.getX(), (int)graphicCoordInNanoMeters.getY());
            final BoardCoordinate newCoordinate = sideBoard.getBoardCoordinate(rendererCoordinate);

            JMenuItem addFidMenuItem = createAddBoardFidMenuItem(sideBoardType, newCoordinate);
            JMenuItem addCompMenuItem = createAddCompMenuItem(sideBoardType, newCoordinate);
            popup.add(addCompMenuItem);
            popup.add(addFidMenuItem);
          }
          popup.show(mouseEvent.getComponent(), pixelX, pixelY);
        }
        else
        {
          // handle any right mouse clicks for the LandPattern graphics engine
          // we'll give an option to set Pad One and also, for TH landpatterns, to change the x,y coords of the hole
          if (_landPatternPadFoundOnRightClick != null)
          {
            JMenuItem makePadOneCompMenuItem = createMakePadOneMenuItem(_landPatternPadFoundOnRightClick);
            popup.add(makePadOneCompMenuItem);
            if (_landPatternPadFoundOnRightClick instanceof ThroughHoleLandPatternPad)
            {
              JMenuItem editHoleLocationCompMenuItem = createEditHoleLocationMenuItem((ThroughHoleLandPatternPad)_landPatternPadFoundOnRightClick);
              popup.add(editHoleLocationCompMenuItem);
            }
            popup.show(mouseEvent.getComponent(), pixelX, pixelY);
          }
        }
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void handleGuiChangesEvent(final Object object)
  {
    Assert.expect(object != null);

    GuiEventEnum guiEventEnum = ((GuiEvent)object).getGuiEventEnum();
    if (guiEventEnum instanceof SelectionEventEnum)
    {
      SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
      if (selectionEventEnum.equals(SelectionEventEnum.BOARD_TYPE))
      {
        _currentBoardType = _testDev.getCurrentBoardType();
        boardTypeChanged();
      }
      else if(selectionEventEnum.equals(SelectionEventEnum.LANDPATTERN_PAD))
      {
        //only highlight table rows if no rows selected, without the front if condition, a infinite loop will occur
        if(_landPatternDetailsTable.getSelectedRows().length <= 0 && _newHighlightedlandPatternPads.size() != _landPatternDetailsTable.getSelectedRows().length)
        {
          _landPatternDetailsTable.clearSelection();
          for(LandPatternPad landPatternPad : _newHighlightedlandPatternPads)
            _landPatternDetailsTable.addRowSelectionInterval(_landPatternDetailsTableModel.getRowForLandPatternPad(landPatternPad), _landPatternDetailsTableModel.getRowForLandPatternPad(landPatternPad));
        }
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateProjectData()
  {
    _landPatternDetailsTableModel.fireTableDataChanged();
    _newComponentTableModel.fireTableDataChanged();
    _newFiducialTableModel.fireTableDataChanged();
    updateNumericDecimalPlaces();
    updateTableEditorsWithDisplayUnits();
  }

  /**
   * @author Laura Cormos
   */
  private void updateData(ProjectChangeEvent projectChangeEvent)
  {
    Assert.expect(projectChangeEvent != null);

    ProjectChangeEventEnum projectChangeEventEnum = projectChangeEvent.getProjectChangeEventEnum();

    if (projectChangeEventEnum instanceof LandPatternEventEnum)
    {
      LandPatternEventEnum landPatternEventEnum = (LandPatternEventEnum)projectChangeEventEnum;
      if (landPatternEventEnum.equals(LandPatternEventEnum.CREATE_OR_DESTROY) || landPatternEventEnum.equals(LandPatternEventEnum.ADD_OR_REMOVE))
      {
        Object oldValue = projectChangeEvent.getOldValue();
        Object newValue = projectChangeEvent.getNewValue();
        if (newValue != null) // this is a create event
        {
          Assert.expect(oldValue == null);
          Assert.expect(newValue instanceof LandPattern);
          LandPattern newLandPattern = (LandPattern)newValue;

          java.util.List<LandPattern> landPatternList = new ArrayList<LandPattern>(_landPatterns);
          for(LandPattern lp : landPatternList)
          {
             if(lp.getName().equals(newLandPattern.getName()) == true)
             {
                _landPatterns.remove(lp);
             }
          }
          _landPatterns.add(newLandPattern);
          Collections.sort(_landPatterns, new LandPatternAlphaNumericComparator(true));
          populateLandPatternComboBox();
          _selectLandPatternComboBox.setSelectedItem(newLandPattern);

          // XCR-3076 Software Crash when try to delete old land pattern after import land pattern (in library)
          _currentLandPattern = newLandPattern;
          
          _deleteUnusedLandPatternButton.setEnabled(true);
          _copyToButton.setEnabled(true);
//          System.out.println("Servicing create LP event in EditCad for LP: " + newLandPattern.getName());
        }
        if (oldValue != null) // this is a destroy event
        {
          Assert.expect(newValue == null);
          Assert.expect(oldValue instanceof LandPattern);
          LandPattern oldLandPattern = (LandPattern)oldValue;

          _landPatterns.remove(oldLandPattern);
          // if the deleted land pattern is the second one, removing the item will cause the combo box to automatically
          // select the previous entry by default. That will make the "New..." entry become selected and act as if
          // the user wanted to create a new land pattern which, of course, is not what they wanted.
          int index = _selectLandPatternComboBox.getSelectedIndex();
          boolean graphicsInteractionState = _shouldInteractWithGraphics;
          // disable action on the New... selection
          if (index == 1)
            _shouldInteractWithGraphics = false;
          _selectLandPatternComboBox.removeItem(oldLandPattern);
          
          // XCR-3076 Software Crash when try to delete old land pattern after import land pattern (in library)
          int numLandPatterns = _selectLandPatternComboBox.getItemCount();
          if (numLandPatterns <= 1)
          {
            // there are no land patterns for this project so clear everything out, only the New... string, at most
            _selectLandPatternComboBox.setSelectedItem(null);
            _landPatternDetailsTableModel.clear();
          }
          else if (numLandPatterns >= 2)
          {
            if (index == 1)
              _selectLandPatternComboBox.setSelectedIndex(1);
          }
          _shouldInteractWithGraphics = graphicsInteractionState;
//          System.out.println("Servicing destroy LP event in EditCad for LP: " + oldLandPattern.getName() + " (Remove LP from combo box)");
        }
      }
      else
      {
//        System.out.println("Received a LP event in EditCad for LP: " + ((LandPattern)projectChangeEvent.getSource()).getName() +
//                           "\n Event is: " + projectChangeEventEnum.getId());
      }
    }
    else if (projectChangeEventEnum instanceof LandPatternPadEventEnum)
    {
      LandPatternPadEventEnum landPatternPadEventEnum = (LandPatternPadEventEnum)projectChangeEventEnum;
      Object oldValue = projectChangeEvent.getOldValue();
      Object newValue = projectChangeEvent.getNewValue();

      if (landPatternPadEventEnum.equals(LandPatternPadEventEnum.CREATE_OR_DESTROY) || landPatternPadEventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE))
      {
        if (newValue != null) // this is a create event
        {
          Assert.expect(oldValue == null);
          Assert.expect(newValue instanceof LandPatternPad);
          LandPatternPad newLandPatternPad = (LandPatternPad)newValue;
          _landPatternDetailsTableModel.addLandPatternPad(newLandPatternPad);
          _newHighlightedlandPatternPads.add(newLandPatternPad);
//          System.out.println("Service create LPP event in EditCad for " + newLandPatternPad.getLandPattern().getName() + ":" + newLandPatternPad.getName());
        }
        if (oldValue != null) // this is a destroy event
        {
          Assert.expect(newValue == null);
          Assert.expect(oldValue instanceof LandPatternPad);
          LandPatternPad oldLandPatternPad = (LandPatternPad)oldValue;
          _landPatternDetailsTableModel.deleteLandPatternPad(oldLandPatternPad);
          //Khaw Chek Hau - XCR3429: Software crashed when choose selected land pattern after delete few of it's pad
          _newHighlightedlandPatternPads.remove(oldLandPatternPad);
//          System.out.println("Service destroy LPP event in EditCad for " + oldLandPatternPad.getLandPattern().getName() + ":" + oldLandPatternPad.getName());
        }
      }
      else if (landPatternPadEventEnum.equals(LandPatternPadEventEnum.ADD_OR_REMOVE_LIST))
      {
        if (newValue != null) // this is a create event
        {
          Assert.expect(oldValue == null);
          Assert.expect(newValue instanceof ArrayList);
          java.util.List<LandPatternPad> landPatternPads = (java.util.ArrayList<LandPatternPad>) newValue;

          for (LandPatternPad landPatternPad : landPatternPads)
          {
            _landPatternDetailsTableModel.addLandPatternPad(landPatternPad);
            _newHighlightedlandPatternPads.add(landPatternPad);
          }
//          System.out.println("Service create LPP event in EditCad for " + newLandPatternPad.getLandPattern().getName() + ":" + newLandPatternPad.getName());
        }
        if (oldValue != null) // this is a destroy event
        {
          Assert.expect(newValue == null);
          Assert.expect(oldValue instanceof ArrayList);
          java.util.List<LandPatternPad> landPatternPads = (java.util.ArrayList<LandPatternPad>) oldValue;

          for (LandPatternPad landPatternPad : landPatternPads)
          {
            _landPatternDetailsTableModel.deleteLandPatternPad(landPatternPad);
            //Khaw Chek Hau - XCR3429: Software crashed when choose selected land pattern after delete few of it's pad
            _newHighlightedlandPatternPads.remove(landPatternPad);
          }
          
//          System.out.println("Service destroy LPP event in EditCad for " + oldLandPatternPad.getLandPattern().getName() + ":" + oldLandPatternPad.getName());
        }
      }
      else
      {
        Object source = projectChangeEvent.getSource();
        if(source instanceof LandPatternPad)
        {
          Assert.expect(source != null);
          //LandPatternPad landPatternPad = (LandPatternPad)source;

          //int row = _landPatternDetailsTableModel.getRowForLandPatternPad(landPatternPad);
          if ((_firstRow >= 0) && (_lastRow >= 0)) // java will wipe out the cell editors if a table is asked to update a negative row range
            _landPatternDetailsTableModel.fireTableRowsUpdated(_firstRow, _lastRow);
          if (_landPatternTabIsVisible)
            _testDev.landPatternScaleDrawing();

  //        System.out.println("Service LPP event in EditCad for " + landPatternPad.getLandPattern().getName() + ":" +
  //                           landPatternPad.getName() + "\n Event is: " + projectChangeEventEnum.getId());
        }
      }
    }
    else if (projectChangeEventEnum instanceof ThroughHoleLandPatternPadEventEnum)
    {
      ThroughHoleLandPatternPadEventEnum throughHoleLandPatternPadEventEnum = (ThroughHoleLandPatternPadEventEnum)projectChangeEventEnum;
      if (throughHoleLandPatternPadEventEnum.equals(ThroughHoleLandPatternPadEventEnum.HOLE_WIDTH) || //Siew Yeng - XCR-3318 - Oval PTH
          throughHoleLandPatternPadEventEnum.equals(ThroughHoleLandPatternPadEventEnum.HOLE_LENGTH) ||
//          throughHoleLandPatternPadEventEnum.equals(ThroughHoleLandPatternPadEventEnum.HOLE_DIAMETER) ||
          throughHoleLandPatternPadEventEnum.equals(ThroughHoleLandPatternPadEventEnum.HOLE_COORDINATE))
      {
        Object source = projectChangeEvent.getSource();
        Assert.expect(source instanceof ThroughHoleLandPatternPad);
        Assert.expect(source != null);
        ThroughHoleLandPatternPad landPatternPad = (ThroughHoleLandPatternPad)source;

        int row = _landPatternDetailsTableModel.getRowForLandPatternPad(landPatternPad);
        if (row >= 0) // java will wipe out the cell editors if a table is asked to update a negative row range
          _landPatternDetailsTableModel.fireTableRowsUpdated(row, row);
        if (_landPatternTabIsVisible)
          _testDev.landPatternScaleDrawing();
      }
    }
    else if (projectChangeEventEnum instanceof ComponentTypeEventEnum)
    {
      ComponentTypeEventEnum componentTypeEventEnum = (ComponentTypeEventEnum)projectChangeEventEnum;
      if (componentTypeEventEnum.equals(ComponentTypeEventEnum.CREATE_OR_DESTROY) || componentTypeEventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE))
      {
        Object oldValue = projectChangeEvent.getOldValue();
        Object newValue = projectChangeEvent.getNewValue();
        if (newValue != null) // this is a create event, make sure the components table model gets updated
        {
          Assert.expect(oldValue == null);
          Assert.expect(newValue instanceof ComponentType);
          _newComponentTableModel.addComponents(((ComponentType)newValue).getComponents());
        }
        if (oldValue != null) // this is a destroy event, make sure the components table model gets updated
        {
          Assert.expect(newValue == null);
          Assert.expect(oldValue instanceof ComponentType);
          ComponentType componentType = (ComponentType)oldValue;
          _newComponentTableModel.deleteComponentsForComponentType(componentType);
        }
      }
      // Modified by Seng Yew on 11-Jan-2013
      // XCR1559 - Able to delete multiple components at once
      else if (componentTypeEventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST))
      {
        Object oldValue = projectChangeEvent.getOldValue();
        Object newValue = projectChangeEvent.getNewValue();
        // this is a create event, make sure the components table model gets updated
        if (oldValue == null && newValue != null)
        {
          java.util.List<ComponentType> componentTypeList = (java.util.List<ComponentType>) newValue;
          _newComponentTableModel.addComponentTypes(componentTypeList);
        }
        // this is a destroy event, make sure the components table model gets updated
        if (oldValue != null && newValue == null)
        {
          java.util.List<ComponentType> componentTypeList = (java.util.List<ComponentType>) oldValue;
          _newComponentTableModel.deleteComponentsForComponentType(componentTypeList);
        }
      }
      else // for any other event, need to update the other rows in the table affected by the change (for multiple board panels)
      {
        Object source = projectChangeEvent.getSource();
        Assert.expect(source instanceof ComponentType);
        Assert.expect(source != null);
        ComponentType componentType = (ComponentType)source;
        for (com.axi.v810.business.panelDesc.Component component : componentType.getComponents())
        {
          int row = _newComponentTableModel.getRowForComponent(component);
          if (row >= 0) // java will wipe out the cell editors if a table is asked to update a negative row range
            _newComponentTableModel.fireTableRowsUpdated(row, row);
        }
      }
    }
    else if (projectChangeEventEnum instanceof ComponentEventEnum)
    {
      ComponentEventEnum componentEventEnum = (ComponentEventEnum)projectChangeEventEnum;
      if (componentEventEnum.equals(ComponentEventEnum.CREATE_OR_DESTROY))
      {
        Object oldValue = projectChangeEvent.getOldValue();
        Object newValue = projectChangeEvent.getNewValue();
        if (oldValue != null) // this is a destroy event, component creation is handled with a component type event
        {
          Assert.expect(newValue == null);
          Assert.expect(oldValue instanceof com.axi.v810.business.panelDesc.Component);
          _newComponentTableModel.deleteComponent((com.axi.v810.business.panelDesc.Component)oldValue);
        }
      }
//      _newComponentTableModel.fireTableDataChanged();
    }
    else if (projectChangeEventEnum instanceof FiducialEventEnum)
    {
      FiducialEventEnum fiducialEventEnum = (FiducialEventEnum)projectChangeEventEnum;
      if (fiducialEventEnum.equals(FiducialEventEnum.CREATE_OR_DESTROY) || fiducialEventEnum.equals(FiducialEventEnum.ADD_OR_REMOVE))
      {
        Object oldValue = projectChangeEvent.getOldValue();
        Object newValue = projectChangeEvent.getNewValue();
        if (newValue != null) // this is a create event
        {
          Assert.expect(oldValue == null);
          Assert.expect(newValue instanceof Fiducial);
          _newFiducialTableModel.addFiducial((Fiducial)newValue);
        }
        if (oldValue != null) // this is a destroy event
        {
          Assert.expect(newValue == null);
          Assert.expect(oldValue instanceof Fiducial);
          _newFiducialTableModel.deleteFiducial((Fiducial)oldValue);
        }
      }
      else
      {
        Object source = projectChangeEvent.getSource();
        Assert.expect(source instanceof Fiducial);
        Assert.expect(source != null);
        Fiducial fiducial = (Fiducial)source;

        int row = _newFiducialTableModel.getRowForFiducial(fiducial);
        if (row >= 0) // java will wipe out the cell editors if a table is asked to update a negative row range
          _newFiducialTableModel.fireTableRowsUpdated(row, row);
      }
    }
    else if (projectChangeEventEnum instanceof FiducialTypeEventEnum)
    {
      FiducialTypeEventEnum fiducialTypeEventEnum = (FiducialTypeEventEnum)projectChangeEventEnum;
      if (fiducialTypeEventEnum.equals(FiducialTypeEventEnum.CREATE_OR_DESTROY) || fiducialTypeEventEnum.equals(FiducialTypeEventEnum.ADD_OR_REMOVE))
      {
        Object oldValue = projectChangeEvent.getOldValue();
        Object newValue = projectChangeEvent.getNewValue();
        if (newValue != null) // this is a create event
        {
          Assert.expect(oldValue == null);
          Assert.expect(newValue instanceof FiducialType);
          _newFiducialTableModel.addFiducials(((FiducialType)newValue).getFiducials());
        }
        if (oldValue != null) // this is a destroy event
        {
          Assert.expect(newValue == null);
          Assert.expect(oldValue instanceof FiducialType);
          _newFiducialTableModel.deleteFiducials(((FiducialType)oldValue).getFiducials());
        }
      }
      else // for any other event, need to update the other rows in the table affected by the change (for multiple board panels)
      {
        Object source = projectChangeEvent.getSource();
        Assert.expect(source instanceof FiducialType);
        Assert.expect(source != null);
        FiducialType fiducialType = (FiducialType)source;

        for (Fiducial fiducial : fiducialType.getFiducials())
        {
          int row = _newFiducialTableModel.getRowForFiducial(fiducial);
          if (row >= 0) // java will wipe out the cell editors if a table is asked to update a negative row range
          {
            cancelEditingIfNecessary(_newFiducialDetailsTable);
            _newFiducialTableModel.fireTableRowsUpdated(row, row);
          }
        }
      }
    }
    else if (projectChangeEventEnum instanceof ProjectEventEnum)
    {
      if(projectChangeEventEnum.equals(ProjectEventEnum.DISPLAY_UNITS))
      {
        updateNumericDecimalPlaces();
        updateTableEditorsWithDisplayUnits();
      }
      else if (projectChangeEventEnum.equals(ProjectEventEnum.LIBRARY_IMPORT_COMPLETE) || projectChangeEventEnum.equals(ProjectEventEnum.IMPORT_LAND_PATTERN_COMPLETE))
      {
        populateWithProjectData();
      }
    }
  }


  /**
   * @author Laura Cormos
   */
  private JMenuItem createDeleteFiducialMenuItem(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);

    JMenuItem deleteFidMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("CAD_DELETE_FIDUCIAL_KEY",
                                                                                                new Object[]{fiducial.getName()})));
    deleteFidMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        JMenuItem mi = (JMenuItem)e.getSource();
        String menuItemText = mi.getText();
        for (Fiducial selectedFiducial : _fiducialsFoundOnRightClick)
        {
          Map<Fiducial, AlignmentGroup> fiducialToAlignmentGroupMap = createFiducialsAlignmentMapIfNecessary(selectedFiducial);
          boolean fiducialIsUsedForAlignment = false;

          if (fiducialToAlignmentGroupMap.isEmpty() == false)
            fiducialIsUsedForAlignment = true;
          if (fiducialIsUsedForAlignment)
          {
            // pop dialog about fiducial being part of an alignment group
            String alignmentGroupNames = new String();
            Set<AlignmentGroup> uniqueGroups = new HashSet<AlignmentGroup>();
            uniqueGroups.addAll(fiducialToAlignmentGroupMap.values());
            for (AlignmentGroup alignmentGroup : uniqueGroups)
              alignmentGroupNames += alignmentGroup.getName() + ", ";
            // remove the last comma and space
            alignmentGroupNames = alignmentGroupNames.substring(0, alignmentGroupNames.length() - 2);

            JOptionPane.showMessageDialog(_mainUI,
                                          StringLocalizer.keyToString(new LocalizedString("CAD_DELETED_FIDUCIAL_IS_IN_ALIGNMENT_GROUP_KEY",
                                                                                          new Object[]{selectedFiducial.getName(), alignmentGroupNames})),
                                          StringLocalizer.keyToString("CAD_CONFIRM_ALIGNMENT_FIDUCIAL_DELETE_KEY"),
                                          JOptionPane.OK_OPTION);
            continue; // skip the rest of delete for this fiducial, go on to the next
          }

          if (menuItemText.contains(selectedFiducial.getName()))
          {
            if (selectedFiducial.isOnBoard())
            {
              int response = 0; // OK_OPTION
              if (_project.getPanel().isMultiBoardPanel())
              {
                // ask the user to confirm deleting all instances of this fiducial
                response = JOptionPane.showConfirmDialog(_mainUI,
                    StringLocalizer.keyToString(new LocalizedString("CAD_DELETE_BOARD_FIDUCIAL_CONFIRM_KEY",
                                                                    new Object[]{selectedFiducial.getName()})),
                    StringLocalizer.keyToString("PDD_CONFIRM_DELETE_TITLE_KEY"),
                    JOptionPane.OK_CANCEL_OPTION);
              }
              if (response != JOptionPane.OK_OPTION)
                return;
              try
              {
                _commandManager.trackState(getCurrentUndoState());
                _commandManager.execute(new RemoveFiducialTypeCommand(selectedFiducial.getFiducialType()));
              }
              catch (XrayTesterException ex)
              {
                MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
              }
            }
            else if (selectedFiducial.isOnPanel())
            {
              try
              {
                _commandManager.trackState(getCurrentUndoState());
                _commandManager.execute(new RemoveFiducialCommand(selectedFiducial));
              }
              catch (XrayTesterException ex)
              {
                MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
              }
            }
          }
        }
      }
    });
    return deleteFidMenuItem;
  }

  /**
   * @author Laura Cormos
   */
  private JMenuItem createRenameFducialMenuItem(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);

    JMenuItem renameFidMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("CAD_RENAME_FIDUCIAL_KEY",
                                                                                                new Object[]{fiducial.getName()})));
    renameFidMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        JMenuItem mi = (JMenuItem)e.getSource();
        String menuItemText = mi.getText();
        for (Fiducial fid : _fiducialsFoundOnRightClick)
        {
          if (menuItemText.contains(fid.getName()))
          {
            String fidName = JOptionPane.showInputDialog(_mainUI,
                StringLocalizer.keyToString("CAD_ENTER_NEW_FIDUCIAL_NAME_KEY"),
                StringLocalizer.keyToString("CAD_RENAME_DIALOG_TITLE_KEY"),
                JOptionPane.INFORMATION_MESSAGE);
            if (fidName != null) // null means the dialog was canceled
            {
              if (fid.isOnBoard())
              {
                BoardType boardType = fid.getSideBoard().getBoard().getBoardType();
                if (boardType.isFiducialTypeNameValid(fidName) == false)
                {
                  String illegalChars = boardType.getFiducialTypeNameIllegalChars();
                  LocalizedString message = null;
                  if (illegalChars.equals(" "))
                    message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_SPACES_KEY", new Object[]{fidName});
                  else
                    message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_OTHER_CHARS_KEY", new Object[]{fidName, illegalChars});

                  JOptionPane.showMessageDialog(_mainUI,
                                                StringLocalizer.keyToString(message),
                                                StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                JOptionPane.INFORMATION_MESSAGE);
                  return;
                }
                else if (boardType.isFiducialTypeNameDuplicate(fidName))
                {
                  JOptionPane.showMessageDialog(_mainUI,
                                                StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
                                                                                                new Object[]{fidName})),
                                                StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                JOptionPane.INFORMATION_MESSAGE);
                  return;
                }
                else
                {
                  FiducialType fiducialType = fid.getFiducialType();
                  try
                  {
                    cancelEditingIfNecessary(_newFiducialDetailsTable);
                    _commandManager.trackState(getCurrentUndoState());
                    _commandManager.execute(new FiducialTypeSetNameCommand(fiducialType, fidName));
                  }
                  catch (XrayTesterException ex)
                  {
                    MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
                  }
                }
              }
              else if (fid.isOnPanel())
              {
                com.axi.v810.business.panelDesc.Panel panel = fid.getPanel();
                if (panel.isFiducialNameValid(fidName) == false)
                {
                  String illegalChars = panel.getFiducialNameIllegalChars();
                  LocalizedString message = null;
                  if (illegalChars.equals(" "))
                    message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_SPACES_KEY", new Object[]{fidName});
                  else
                    message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_OTHER_CHARS_KEY", new Object[]{fidName, illegalChars});

                  JOptionPane.showMessageDialog(_mainUI,
                                                StringLocalizer.keyToString(message),
                                                StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                JOptionPane.INFORMATION_MESSAGE);
                  return;
                }
                else if (panel.isFiducialNameDuplicate(fidName))
                {
                  JOptionPane.showMessageDialog(_mainUI,
                                                StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
                                                                                                new Object[]{fidName})),
                                                StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                JOptionPane.INFORMATION_MESSAGE);
                  return;
                }
                else
                {
                  FiducialType fiducialType = fid.getFiducialType();
                  try
                  {
                    _commandManager.trackState(getCurrentUndoState());
                    _commandManager.execute(new FiducialTypeSetNameCommand(fiducialType, fidName));
                  }
                  catch (XrayTesterException ex)
                  {
                    MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
                  }
                  _newFiducialTableModel.fireTableDataChanged();
                }
              }
            }
          }
        }
      }
    });
    return renameFidMenuItem;
  }

  /**
   * @author Laura Cormos
   */
  private JMenuItem createDeleteCompMenuItem(com.axi.v810.business.panelDesc.Component component)
  {
//    Assert.expect(component != null);

    // Modified by Seng Yew on 21-Jan-2013
    // XCR1559 - Able to delete multiple components at once
    final com.axi.v810.business.panelDesc.Component selectedComponent = component;
    JMenuItem deleteCompMenuItem = null;
    if (component != null)
    {
      deleteCompMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("CAD_DELETE_COMPONENT_KEY",
        new Object[]{component.getReferenceDesignator()})));
    }
    else
    {
      deleteCompMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("CAD_DELETE_SELECTED_COMPONENTS_KEY",
        new Object[]{_componentsFoundOnRightClick.size()})));
    }

    deleteCompMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        JMenuItem mi = (JMenuItem)e.getSource();
        String menuItemText = mi.getText();
        boolean yesToAllChoice = false;
        java.util.List<String> listOfPOPToBeRemoved = new ArrayList<String>();
        // Modified by Seng Yew on 11-Jan-2013
        // XCR1558
        java.util.Set<ComponentType> listOfComponentTypeSelected = new HashSet<ComponentType>(); //for all the component types selected from panel graphics
        java.util.List<ComponentType> listOfComponentType = new ArrayList<ComponentType>(); //for component types chosen by the user
        for (com.axi.v810.business.panelDesc.Component comp : _componentsFoundOnRightClick)
        {
          //Ngie Xing added on Apr 2014
          //avoid delete error when components of the same ComponentType are selected ACROSS multiboard
          if (listOfComponentTypeSelected.add(comp.getComponentType()) == false)
            continue;

          if (comp.getComponentType().hasCompPackageOnPackage())  
          {
            listOfPOPToBeRemoved.add(comp.getComponentType().getCompPackageOnPackage().getPOPName());
          }
          
          boolean bShouldDelete = false;
          // Modified by Seng Yew on 11-Jan-2013
          // XCR1558 - This is to fix ambiguity issue when do matching.
          String selectedComponentString = "\"" + comp.getReferenceDesignator() + "\"";
          if (selectedComponent != null)
          {
            bShouldDelete = menuItemText.contains(selectedComponentString);
          }
          else
          {
            bShouldDelete = true;
          }
          
          if (bShouldDelete)	//
          {
            // do not allow the last component to be deleted or else a whole slew of problems will show up
            if (comp.getBoard().getComponents().size() == 1)
            {
              MessageDialog.showErrorDialog(_mainUI, StringLocalizer.keyToString("CAD_CANNOT_DELETE_LAST_COMPONENT_KEY"),
                                            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
              return;
            }
            
            //Ngie Xing
            //modified to add "Yes to all" button for deleting multiple components on multi-board
            if (_project.getPanel().isMultiBoardPanel() && yesToAllChoice == false)
            {
              int response = 0; // OK_OPTION
              Object[] options =
              {
                StringLocalizer.keyToString("CAD_DELETE_COMPONENT_YES_BUTTON_KEY"),
                StringLocalizer.keyToString("CAD_DELETE_COMPONENT_NO_BUTTON_KEY"),
                StringLocalizer.keyToString("CAD_DELETE_COMPONENT_YES_TO_ALL_BUTTON_KEY")
              };

              response = JOptionPane.showOptionDialog(_mainUI,
                StringLocalizer.keyToString(new LocalizedString("CAD_DELETE_COMPONENT_CONFIRM_KEY",
                new Object[]{comp.getReferenceDesignator()})),
                StringLocalizer.keyToString("PDD_CONFIRM_DELETE_TITLE_KEY"),
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null, options, options[2]);

              if (response == JOptionPane.CLOSED_OPTION)
              {
                return;
              }
              else if (response == 1) //"NO" option
              {
                continue;
              }
              else if (response == 2)
              {
                yesToAllChoice = true;
              }
            }

            Map<Pad, AlignmentGroup> padToAlignmentGroupMap = createPadAlignmentMapIfNecessary(comp.getComponentType());
            boolean componentPadIsUsedForAlignment = false;

            if (padToAlignmentGroupMap.isEmpty() == false)
              componentPadIsUsedForAlignment = true;
            if (componentPadIsUsedForAlignment)
            {
              String message = new String();
              for (Pad alignmentPad : padToAlignmentGroupMap.keySet())
              {
                // pop dialog about pads being part of an alignment group
                String alignmentGroupNames = new String();
                Set<AlignmentGroup> uniqueGroups = new HashSet<AlignmentGroup>();
                uniqueGroups.addAll(padToAlignmentGroupMap.values());
                for (AlignmentGroup alignmentGroup : uniqueGroups)
                {
                  if (alignmentGroup == padToAlignmentGroupMap.get(alignmentPad))
                    alignmentGroupNames += alignmentGroup.getName() + ", ";
                }
                // remove the last comma and space
                alignmentGroupNames = alignmentGroupNames.substring(0, alignmentGroupNames.length() - 2);
                message += StringLocalizer.keyToString("CAD_PAD_KEY")  + " " + alignmentPad.getName() + " " +
                    StringLocalizer.keyToString("CAD_IS_PART_OF_ALIGNMENT_GROUP_KEY") + " " + alignmentGroupNames + ".\n";
              }

              JOptionPane.showMessageDialog(_mainUI,
                                            StringLocalizer.keyToString(new LocalizedString(
                                                "CAD_DELETED_COMPONENT_PAD_IS_IN_ALIGNMENT_GROUP_KEY",
                                                new Object[]{message})),
                                            StringLocalizer.keyToString("CAD_CONFIRM_ALIGNMENT_COMPONENT_PAD_DELETE_KEY"),
                                            JOptionPane.OK_OPTION);
              return;
            }

            // the line below should not be necessary if the component type Destroy event would contain
            // all of the destroyed component type's components, but it doesn't
//            _newComponentTableModel.deleteComponents(comp.getComponentType().getComponents());

            // Modified by Seng Yew on 11-Jan-2013
            // XCR1559 - Able to delete multiple components at once
            // Previously execute command to remove one ComponentType at a time here.
            // Changed to store a list and remove all in one command later, otherwise might have too many undo command.
            // For example, if we multiple select 1000 components and delete.
            listOfComponentType.add(comp.getComponentType());
            if (selectedComponent != null)
            {
              break; // found the one they clicked on; stop looping
            }
          }
        }
        
        //Khaw Chek Hau - XCR3554: Package on package (PoP) development
        if (listOfPOPToBeRemoved.isEmpty() == false)
        {
          int status = JOptionPane.CANCEL_OPTION;
          status = JOptionPane.showConfirmDialog(_mainUI, 
                                              StringLocalizer.keyToString(new LocalizedString("MMGUI_DELETE_COMPONENTS_WOULD_DELETE_POP_KEY",
                                                                                               new Object[]{listOfComponentType, listOfPOPToBeRemoved})),
                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), 
                                              JOptionPane.YES_NO_CANCEL_OPTION, 
                                              JOptionPane.QUESTION_MESSAGE);

          if(status != JOptionPane.YES_OPTION)
          {
            return;
          }   
        }
        
        int listOfComponentTypeSize = listOfComponentType.size();
        
        //Ngie Xing, PREVENT all components on a board from being deleted
        //use componentType instead of components to avoid mismatch when selecting across multi-boards
        if (listOfComponentTypeSize == _componentsFoundOnRightClick.get(0).getBoard().getComponents().size())
        {
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString("CAD_CANNOT_DELETE_ALL_COMPONENT_KEY"),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        JOptionPane.ERROR_MESSAGE);
          return;
        }
        
        //If only one ComponentType, refresh UI the same as previously implementation.
        //If update more than one ComponentType, need to handle differently to speed up the update process.
        //Ngie Xing, added to cancel the delete of multiple components
        if (listOfComponentTypeSize > 100)
        {
          // Show warning dialog if selection more than 100 components
          //Ngie Xing, switch to showOptionDialog instead of using MessageDialog so that user can choose to quit action
          int response = JOptionPane.YES_OPTION;
          Object[] options =
          {
            StringLocalizer.keyToString("CAD_DELETE_COMPONENT_YES_BUTTON_KEY"),
            StringLocalizer.keyToString("CAD_DELETE_COMPONENT_NO_BUTTON_KEY"),
          };

          response = JOptionPane.showOptionDialog(
            null,
            StringLocalizer.keyToString(new LocalizedString("CAD_DELETE_SELECTED_COMPONENTS_WARNING_MESSAGE_KEY",
              new Object[]{100})),
            StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null,
            options,
            options[0]);

          //Ngie Xing, if user does not wish to proceed, return
          if (response == JOptionPane.CLOSED_OPTION || response == JOptionPane.NO_OPTION)
            return;
        }

        if (listOfComponentTypeSize > 1)
        {
          ProjectObservable.getInstance().setEnabled(false);
        }
        try
        {
          _commandManager.trackState(getCurrentUndoState());
          _commandManager.execute(new RemoveComponentTypeCommand(listOfComponentType));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
        }
        finally
        {
          // It will be very slow if let RemoveComponentTypeCommand to update each ComponentType.
          // Need to specially handle here so that update process is faster.
          if (listOfComponentTypeSize > 1)
          {
            ProjectObservable.getInstance().setEnabled(true);
          }
        }
        if (listOfComponentTypeSize > 1)
        {
          ProjectObservable.getInstance().stateChanged(this, (Object) listOfComponentType, null, ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST);
        }
      }
    });
    return deleteCompMenuItem;
  }

  /**
   * @author Laura Cormos
   */
  private JMenuItem createRenameCompMenuItem(final com.axi.v810.business.panelDesc.Component component)
  {
    Assert.expect(component != null);

    JMenuItem renameCompMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("CAD_RENAME_COMPONENT_KEY",
                                                                                                  new Object[]{component.getReferenceDesignator()})));
    renameCompMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        JMenuItem mi = (JMenuItem)e.getSource();
        String menuItemText = mi.getText();
        for (com.axi.v810.business.panelDesc.Component comp : _componentsFoundOnRightClick)
        {
          if (menuItemText.contains(comp.getReferenceDesignator()))
          {
            Object userInput = JOptionPane.showInputDialog(_mainUI,
                                                          StringLocalizer.keyToString("CAD_ENTER_NEW_COMPONENT_NAME_KEY"),
                                                          StringLocalizer.keyToString("CAD_RENAME_DIALOG_TITLE_KEY"),
                                                          JOptionPane.INFORMATION_MESSAGE,
                                                          null /*no icon*/,
                                                          null /*no other selection values*/,
                                                          component.getReferenceDesignator() /*original component name*/);
            if (userInput != null) // null means the dialog was canceled
            {
              Assert.expect(userInput instanceof String);
              String compName = (String)userInput;
              BoardType boardType = comp.getBoard().getBoardType();
              SideBoardType sideBoardType = comp.getSideBoard().getSideBoardType();
              if (boardType.isComponentTypeReferenceDesignatorValid(compName) == false)
              {
                // pop dialog about invalid name
                String illegalChars = boardType.getComponentTypeReferenceDesignatorIllegalChars();
                LocalizedString message = null;
                if (illegalChars.equals(" "))
                  message = new LocalizedString("CAD_INVALID_COMPONENT_NAME_SPACES_KEY", new Object[]{compName});
                else
                  message = new LocalizedString("CAD_INVALID_COMPONENT_NAME_OTHER_CHARS_KEY", new Object[]{compName, illegalChars});
                JOptionPane.showMessageDialog(_mainUI,
                                              StringLocalizer.keyToString(message),
                                              StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                              JOptionPane.INFORMATION_MESSAGE);
                return;
              }
              else if (boardType.isComponentTypeReferenceDesignatorDuplicate(compName))
              {
                // pop dialog about duplicate name
                JOptionPane.showMessageDialog(_mainUI,
                                              StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
                                                                                              new Object[]{compName})),
                                              StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                              JOptionPane.INFORMATION_MESSAGE);
                return;
              }
              else
              {
                ComponentType componentType = comp.getComponentType();
                try
                {
                  cancelEditingIfNecessary(_newComponentDetailsTable);
                  _commandManager.trackState(getCurrentUndoState());
                  _commandManager.execute(new ComponentTypeSetRefDesCommand(componentType, compName));
                }
                catch (XrayTesterException ex)
                {
                  MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
                }
                _newComponentTableModel.fireTableDataChanged();
              }
            }
          }
        }
      }
    });
    return renameCompMenuItem;
  }

  /**
   * @author Laura Cormos
   */
  private JMenuItem changeLandPatternForCompMenuItem(final com.axi.v810.business.panelDesc.Component component)
  {
    Assert.expect(component != null);

    JMenuItem changeLandPatternForCompMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("CAD_CHANGE_LP_FOR_COMPONENT_KEY",
                                                                                                               new Object[]{component.getReferenceDesignator()})));
    changeLandPatternForCompMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        java.util.List<LandPattern> allLandPatterns = _project.getPanel().getAllLandPatterns();
        Object [] possibleValues = new Object[allLandPatterns.size()];
        int i = 0;
        for (LandPattern lp : allLandPatterns)
          possibleValues[i++] = lp;

        Object choice = JOptionPane.showInputDialog(_mainUI,
                                                    StringLocalizer.keyToString("CAD_SELECT_NEW_LP_FOR_COMPONENT_KEY"),
                                                    StringLocalizer.keyToString(new LocalizedString("MM_GUI_TDT_NAME_KEY", null)),
                                                    JOptionPane.QUESTION_MESSAGE,
                                                    null,
                                                    possibleValues,
                                                    possibleValues[0]);
        if (choice == null) // user cancelled the dialog
          return;

        Assert.expect(choice instanceof LandPattern);
        LandPattern chosenLandPattern = (LandPattern)choice;

        try
        {
          ComponentType componentType = component.getComponentType();

          //Khaw Chek Hau - XCR3554: Package on package (PoP) development
          if (component.getComponentType().hasCompPackageOnPackage())
          {
            java.util.List<ComponentType> componentTypesInSamePOP = component.getComponentType().getCompPackageOnPackage().getComponentTypes();
            CompPackageOnPackage compPackageOnPackage = component.getComponentType().getCompPackageOnPackage();
            HashMap<ComponentType, POPLayerIdEnum> componentTypeToPOPLayerIdEnumMap = new HashMap<ComponentType, POPLayerIdEnum>();
            HashMap<ComponentType, Integer> componentTypeToPOPZHeightInNanometersMap = new HashMap<ComponentType, Integer>();
            
            java.util.List<ComponentType> componentTypes = compPackageOnPackage.getComponentTypes();

            for (ComponentType compType : componentTypes)
            {
              componentTypeToPOPLayerIdEnumMap.put(compType, compType.getPOPLayerId());
              componentTypeToPOPZHeightInNanometersMap.put(compType, compType.getPOPZHeightInNanometers());
            }
            
            _commandManager.trackState(getCurrentUndoState());
            _commandManager.beginCommandBlock(StringLocalizer.keyToString(new LocalizedString("CAD_CHANGE_LP_FOR_COMPONENT_KEY",
                                                                                              new Object[]{component.getReferenceDesignator()})));
            _commandManager.execute(new RemoveCompPackageOnPackageCommand(compPackageOnPackage));
            _commandManager.execute(new RemoveComponentTypeCommand(component.getComponentType()));
            _commandManager.execute(new CreateComponentTypeCommand(componentType.getSideBoardType(),
                                                                   componentType.getReferenceDesignator(),
                                                                   chosenLandPattern,
                                                                   componentType.getCoordinateInNanoMeters(),
                                                                   componentType.getDegreesRotationRelativeToBoard()));

            _commandManager.execute(new CreateCompPackageOnPackageCommand(componentType.getSideBoardType(),
                                                                          componentType.getReferenceDesignator(),
                                                                          compPackageOnPackage,
                                                                          componentTypesInSamePOP,
                                                                          componentTypeToPOPLayerIdEnumMap,
                                                                          componentTypeToPOPZHeightInNanometersMap));
            _commandManager.endCommandBlock();
          }
          else
          {
            _commandManager.trackState(getCurrentUndoState());
            _commandManager.beginCommandBlock(StringLocalizer.keyToString(new LocalizedString("CAD_CHANGE_LP_FOR_COMPONENT_KEY",
                                                                                              new Object[]{component.getReferenceDesignator()})));
            _commandManager.execute(new RemoveComponentTypeCommand(component.getComponentType()));
            _commandManager.execute(new CreateComponentTypeCommand(componentType.getSideBoardType(),
                                                                   componentType.getReferenceDesignator(),
                                                                   chosenLandPattern,
                                                                   componentType.getCoordinateInNanoMeters(),
                                                                   componentType.getDegreesRotationRelativeToBoard()));
            _commandManager.endCommandBlock();
          }
        }
        catch (Exception ex)
        {

        }
      }
    });
    return changeLandPatternForCompMenuItem;
  }

  /**
   * @author Laura Cormos
   */
  private JMenuItem createAddBoardFidMenuItem(final SideBoardType sideBoardType, final BoardCoordinate newCoordinate)
  {
    Assert.expect(sideBoardType != null);
    Assert.expect(newCoordinate != null);

    final BoardType boardType = sideBoardType.getBoardType();
    JMenuItem addFidMenuItem = new JMenuItem(StringLocalizer.keyToString("CAD_ADD_BOARD_FIDUCIAL_KEY"));
    addFidMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        AddFiducialDialog addFiducialDialog = new AddFiducialDialog(_mainUI,
                                                                     StringLocalizer.keyToString("CAD_ADD_FIDUCIAL_DIALOG_TITLE_KEY"),
                                                                     true,
                                                                     boardType,
                                                                     _project,
                                                                     newCoordinate,
                                                                     sideBoardType.getSideBoards().get(0).isSideBoard1(),
                                                                     false);
        SwingUtils.centerOnComponent(addFiducialDialog, _mainUI);
        addFiducialDialog.setVisible(true);
        showAndAddBoardFiducial(addFiducialDialog);
      }
    });
    return addFidMenuItem;
  }

  /**
   * @author Laura Cormos
   */
  private JMenuItem createAddCompMenuItem(final SideBoardType sideBoardType, final BoardCoordinate newCoordinate)
  {
    Assert.expect(sideBoardType != null);
    Assert.expect(newCoordinate != null);

    JMenuItem addCompMenuItem = new JMenuItem(StringLocalizer.keyToString("CAD_ADD_COMPONENT_KEY"));
    addCompMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        AddComponentDialog addComponentDialog = new AddComponentDialog(_mainUI,
                                                                       StringLocalizer.keyToString("CAD_ADD_COMPONENT_DIALOG_TITLE_KEY"),
                                                                       true,
                                                                       _project,
                                                                       sideBoardType.getBoardType(),
                                                                       _landPatterns,
                                                                       newCoordinate,
                                                                       sideBoardType.getSideBoards().get(0).isSideBoard1());
        SwingUtils.centerOnComponent(addComponentDialog, _mainUI);
        addComponentDialog.setVisible(true);
        showAndAddComponent(addComponentDialog);
      }
    });
    return addCompMenuItem;
  }

  /**
   * @author Laura Cormos
   */
  private JMenuItem createAddPanelFidMenuItem(final boolean topSideFiducial, final PanelCoordinate newCoordInNanoMeters)
  {
    Assert.expect(newCoordInNanoMeters != null);

    JMenuItem addFidMenuItem = new JMenuItem(StringLocalizer.keyToString("CAD_ADD_PANEL_FIDUCIAL_KEY"));
    addFidMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        AddFiducialDialog addFiducialDialog = new AddFiducialDialog(_mainUI,
                                                                     StringLocalizer.keyToString("CAD_ADD_FIDUCIAL_DIALOG_TITLE_KEY"),
                                                                     true,
                                                                     _project,
                                                                     new BoardCoordinate(newCoordInNanoMeters),
                                                                     topSideFiducial,
                                                                     true);

        SwingUtils.centerOnComponent(addFiducialDialog, _mainUI);
        addFiducialDialog.setVisible(true);
        showAndAddPanelFiducial(addFiducialDialog);
      }
    });
    return addFidMenuItem;
  }

  /**
   * @author Laura Cormos
   */
  private JMenuItem createMakePadOneMenuItem(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    JMenuItem makePadOneMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("CAD_ASSIGN_PAD_ONE_MENU_KEY",
                                                                                                 new Object[]{landPatternPad.getName()})));
    makePadOneMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setLandPatternPadOne(_landPatternPadFoundOnRightClick);
      }
    });
    return makePadOneMenuItem;
  }

  /**
   * @author Laura Cormos
   */
  private void setLandPatternPadOne(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    try
    {
      _commandManager.trackState(getCurrentUndoState());
      _commandManager.execute(new LandPatternPadSetPadOneCommand(landPatternPad));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private JMenuItem createEditHoleLocationMenuItem(final ThroughHoleLandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    JMenuItem editHoleLocationMenuItem = new JMenuItem(StringLocalizer.keyToString("CAD_EDIT_HOLE_LOCATIION_MENU_KEY"));
    editHoleLocationMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showEditHoleLocationDialog(landPatternPad);
      }
    });
    return editHoleLocationMenuItem;
  }

  /**
   * @author Laura Cormos
   */
  private void showEditHoleLocationDialog(ThroughHoleLandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    //show the edit location dialog
    EditHoleLocationDialog dlg = new EditHoleLocationDialog(_mainUI, _project, landPatternPad);
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    dlg.clear();
    dlg.dispose();
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit()
  {
    setLayout(_editCadBorderLayout);
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    add(_editCadTabbedPane, BorderLayout.CENTER);
    _editCadTabbedPane.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        editCadTabbedPane_stateChanged(e);
      }
    });

    _verifyCadPanel = new VerifyCadPanel();
    _editCadTabbedPane.add(_verifyCadPanel, StringLocalizer.keyToString("CAD_VERIFY_CAD_KEY"));
    _verifyCadTabIsVisible = true;
    _editCadTabbedPane.add(_editLandPatternPanel, StringLocalizer.keyToString("CAD_EDIT_LP_KEY"));
    _editCadTabbedPane.add(_newComponentAndFiducialPanel, StringLocalizer.keyToString("CAD_ENTER_COMPONENTS_FIDUCIALS_KEY"));
    
    //Kee Chin Seong - Edit Focus Region
    _editFocusRegionPanel = new EditFocusRegionPanel(_mainUI);

    _editCadTabbedPane.add(_editFocusRegionPanel, StringLocalizer.keyToString("CAD_EDIT_FOCUS_REGION_KEY"));
    
    //Siew Yeng - XCR-3781
    _editPshSettingsPanel = new EditPshSettingsPanel(_mainUI);
    _editCadTabbedPane.add(_editPshSettingsPanel, StringLocalizer.keyToString("CAD_EDIT_PSH_SETTINGS_KEY"));

    _editLandPatternPanel.setLayout(_editLandPatternPanelBorderLayout);
    _editLandPatternPanelBorderLayout.setVgap(10);
    _editLandPatternPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
                                                                                                                         new Color(142, 142, 142)),
                                                                                        StringLocalizer.keyToString("CAD_EDIT_OR_CREATE_LP_PANEL_BORDER_KEY")),
                                                                       BorderFactory.createEmptyBorder(0, 5, 10, 5)));
    _editLandPatternPanel.add(_editLandPatternDetailsPanel, BorderLayout.CENTER);
    _editLandPatternPanel.add(_editLandPatternInfoPanel, BorderLayout.SOUTH);

    _editLandPatternDetailsPanel.setLayout(_editLandPatternDetailsPanelBorderLayout);
    _editLandPatternDetailsPanel.add(_boardAndLandPatternSelectPanel, BorderLayout.NORTH);
    _editLandPatternDetailsPanel.add(_landPatternDetailsPanel, BorderLayout.CENTER);
    _editLandPatternDetailsPanel.add(_landPatternActionsWrapperPanel, BorderLayout.SOUTH);

    _boardAndLandPatternSelectPanel.setLayout(_boardAndLandPatternSelectPanelBorderLayout);
    _boardAndLandPatternSelectPanel.add(_landPatternSelectPanel, BorderLayout.CENTER);

    _landPatternSelectPanel.setLayout(new BorderLayout());
    _landPatternSelectPanel.add(_selectLandPatternLabel, BorderLayout.WEST);
    _selectLandPatternLabel.setText(StringLocalizer.keyToString("CAD_SELECT_LP_LABEL_KEY"));

    JPanel holderPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    holderPanel.add(_selectLandPatternComboBox);
    _selectLandPatternComboBox.setMaximumRowCount(30);
    _landPatternSelectPanel.add(holderPanel, BorderLayout.CENTER);

    _selectLandPatternActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        landPatternSelectComboBox_actionPerformed(e);
      }
    };

    _landPatternDetailsPanel.setLayout(_landPatternDetailsPanelBorderLayout);
    _landPatternDetailsPanel.add(_landPatternDetailsScrollPane, BorderLayout.CENTER);
    _landPatternDetailsTableModel = new LandPatternDetailsTableModel(_testDev);
    _landPatternDetailsTable = new LandPatternDetailsTable(this)
    {
      // this overload allows multiple rows to be edited at once, for any one column
      public void setValueAt(Object value, int row, int column)
      {
        _currentLandPattern = _landPatterns.get(_selectLandPatternComboBox.getSelectedIndex()-1);
        final int rows[] = getSelectedRows();
        if (rows.length > 0)
        {
          _firstRow = rows[0];
          _lastRow = rows[rows.length-1];
        }
        _newHighlightedlandPatternPads.clear();
        final int col = convertColumnIndexToModel(column);
        _commandManager.trackState(getCurrentUndoState());
        if (column == 0)
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_NAME_KEY"));
        else if (column == 1 || column == 2)
        {
          DecimalFormat twoDecimalPlaces = new DecimalFormat("#######.####");

          if(value instanceof Long)
          {
             value = Double.parseDouble(value.toString());
          }

          if((Double)value == Double.parseDouble(twoDecimalPlaces.format((Double)this.getValueAt(row, column))))
              return;

          Double longValue = (Double)value;

          Object[] options = {StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_ACTUAL_LOCATION_OPTION_KEY"), 
                              StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_OFFSET_FROM_ACTUAL_LOCATION_OPTION_KEY")}; 
          //chin-seong, kee - this is for Offset X, Y use, when need handle multiple pad in the sametime
          //                  for e.g. BGA can mutliple select whole region which user desired and make the offset
          //if (valStr.equalsIgnoreCase(StringLocalizer.keyToString("CAD_LAND_PATTERN_SET_LOCATION_KEY")))
          //{
          String prompt = StringLocalizer.keyToString(new LocalizedString("MMGUI_PAD_LAND_PATTERN_LOCATION_OR_OFFSET_KEY",
                                                  new Object[]{longValue}));
          String title = (column == 1) ? StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_LOCATION_OR_OFFSET_X_TITLE_KEY") :
                                          StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_LOCATION_OR_OFFSET_Y_TITLE_KEY");
          /*int retVal = JOptionPane.showOptionDialog(MainMenuGui.getInstance(),
                                                          prompt,
                                                          title,
                                                          JOptionPane.YES_NO_CANCEL_OPTION,
                                                          JOptionPane.QUESTION_MESSAGE,
                                                          null, options, options[1]);*/

          ChoiceRadioButtonDialog loadDialog = new ChoiceRadioButtonDialog(MainMenuGui.getInstance(),
                                                        title,
                                                        prompt,
                                                        StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                                        StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                                        options,
                                                        null);

          int retVal = loadDialog.showDialog();

          if (retVal == JOptionPane.OK_OPTION) 
          {
            if(loadDialog.getSelectedValue().equals(StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_ACTUAL_LOCATION_OPTION_KEY")) == true)
             this.setIsOffsetBeenSet(false);
            else if(loadDialog.getSelectedValue().equals(StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_OFFSET_FROM_ACTUAL_LOCATION_OPTION_KEY")) == true)
             this.setIsOffsetBeenSet(true);
          }
          else // the use hit cancel or entered an empty string -- change nothing
             return;


          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_COORD_NM_KEY"));
        }
        else if (column == 3)
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_WIDTH_NM_KEY"));
        else if (column == 4)
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_LENGTH_NM_KEY"));
        else if (column == 5)
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_ROTATION_KEY"));
        else if (column == 6)
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_SHAPE_KEY"));
        else if (column == 7)
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SWAP_TYPE_KEY"));
        else if (column == 8)
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_HOLE_WIDTH_KEY")); //Siew Yeng - XCR-3318 - Oval PTH
        else if (column == 9)
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_HOLE_LENGTH_KEY"));
        
        final java.util.List<Integer> selectedLandPatternPadIndexs = new ArrayList<Integer>();
        // Ying-Huan.Chu
        // if user is setting multiple rows of column other than swapping TH/SM type
        if (col != 7 && _currentLandPattern.getLandPatternPads().size() <= 200)
        {
          for (int i = 0; i < rows.length; ++i)
          {
            if (col == 8)
            {
              // do not set value for SM pads
              if (_landPatternDetailsTableModel.getLandPatternPadAt(rows[i]).isSurfaceMountPad())
                continue;
            }
            //chin-seong, kee - this is for Offset X, Y use, when need handle multiple pad in the sametime
            //                  for e.g. BGA can mutliple select whole region which user desired and make the offset
            if (this.isOffsetBeenSet() == true)
            {
              _landPatternDetailsTableModel.setValueAt((Double)_landPatternDetailsTableModel.getValueAt(rows[i], col) - (Double)value, rows[i], col);
            }
            else
            {
              _landPatternDetailsTableModel.setValueAt(value, rows[i], col);
            }
            if (col!= 0 && col != 5 && col != 6 && col != 7) // these columns are DropDownList - Kee Chin Seong, column 0 is not allowed too !!
            {
              if (Float.parseFloat(_landPatternDetailsTableModel.getValueAt(rows[i], col).toString()) != Float.parseFloat(value.toString()))
                continue; // break from the loop as the newly assigned value isn't successfully successfully. - Ying-Huan.Chu
            }
          }
          _commandManager.endCommandBlockIfNecessary();
        }
        else 
        {
          _projectObservable.setEnabled(false);
          final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
                                      StringLocalizer.keyToString("ECP_BUSYCANCELDIALOG_SETTING_TABLE_VALUES_MESSAGE_KEY"),
                                      StringLocalizer.keyToString("ECP_BUSYCANCELDIALOG_SETTING_TABLE_VALUES_TITLE_KEY"),
                                      true);
          busyCancelDialog.pack();
          SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
          busyCancelDialog.setCursor(new Cursor(Cursor.WAIT_CURSOR));
          final Object finalValue = value;
          SwingWorkerThread.getInstance().invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                for (int i = 0; i < rows.length; i++)
                {
                  if (col == 8)
                  {
                    // do not set value for SM pads
                    if (_landPatternDetailsTableModel.getLandPatternPadAt(rows[i]).isSurfaceMountPad())
                      continue;
                  }
                  //chin-seong, kee - this is for Offset X, Y use, when need handle multiple pad in the sametime
                  //                  for e.g. BGA can mutliple select whole region which user desired and make the offset
                  if(_landPatternDetailsTable.isOffsetBeenSet() == true)
                  {
                    _landPatternDetailsTableModel.setValueAt((Double)_landPatternDetailsTableModel.getValueAt(rows[i], col) - (Double)finalValue, rows[i], col);
                  }
                  else
                  {
                    //XCR-2807 to keep track the new change row to highlight it after all.
                    if(col == 7)
                      selectedLandPatternPadIndexs.add(rows[i]);
                    
                    _landPatternDetailsTableModel.setValueAt(finalValue, rows[i], col);
                  }
                  if (col != 0 && col != 5 && col != 6 && col != 7) // these columns are DropDownList
                  {
                    // Kee Chin Seong - need to check is NOT COLUMN 0 also as this column is allow string input
                    if (Float.parseFloat(_landPatternDetailsTableModel.getValueAt(rows[i], col).toString()) != Float.parseFloat(finalValue.toString()))
                      continue; // break from the loop as the newly assigned value isn't successfully set. - Ying-Huan.Chu
                  }
                }
              }
              finally
              {
                // wait until visible
                _commandManager.endCommandBlockIfNecessary();
                while (busyCancelDialog.isVisible() == false)
                {
                  try
                  {
                    Thread.sleep(200);
                  }
                  catch (InterruptedException ex)
                  {
                    // do nothing
                  }
                }
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    busyCancelDialog.dispose();
                  }
                });
              }
            }
          });
          busyCancelDialog.setVisible(true);
          // delete current land pattern
          _currentLandPattern.assignAll();
          _projectObservable.setEnabled(true);
          _projectObservable.stateChanged(_currentLandPattern, _currentLandPattern, null, LandPatternEventEnum.CREATE_OR_DESTROY);
          // create new land pattern
          _projectObservable.stateChanged(_currentLandPattern, null, _currentLandPattern, LandPatternEventEnum.CREATE_OR_DESTROY);
          // reselect land pattern in combo box
          _selectLandPatternComboBox.setSelectedItem(_currentLandPattern);
          //keep track the row of changed data
          for(int i = 0 ; i < selectedLandPatternPadIndexs.size() ; i++)
            _newHighlightedlandPatternPads.add(_landPatternDetailsTableModel.getLandPatternPadAt(selectedLandPatternPadIndexs.get(i)));

          //Khaw Chek Hau - XCR3471: System crash when switch tab after create new land pattern
          if (_inLandPatternUndoAction == false)
            _guiObservable.stateChanged(_newHighlightedlandPatternPads, SelectionEventEnum.LANDPATTERN_PAD); 
        }
        this.setIsOffsetBeenSet(false);
      }
      /**
       * We override this to make sure the selected rows are still the proper selected
       * rows after a sort.
       * @author Andy Mechtenberg
       * @author Laura Cormos
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = getSelectedRows();
        java.util.List<Object> selectedData = new ArrayList<Object>();
        for (int i = 0; i < selectedRows.length; i++)
        {
          selectedData.add(_landPatternDetailsTableModel.getLandPatternPadAt(selectedRows[i]));
        }

        super.mouseReleased(event);

        // now, reselect everything
        _landPatternDetailsTable.clearSelection();
        Iterator it = selectedData.iterator();
        while (it.hasNext())
        {
          int row;
          Object obj = it.next();
          row = _landPatternDetailsTableModel.getRowForLandPatternPad((LandPatternPad)obj);
          addRowSelectionInterval(row, row);
        }
      }

    };
    _landPatternDetailsTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    //chin-seong, kee - this is for Offset X, Y use, when need handle multiple pad in the sametime
    //                  for e.g. BGA can mutliple select whole region which user desired and make the offset
    _landPatternDetailsTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        multiEditTableMouseReleased(e, _landPatternDetailsTable);
      }
    });
    _landPatternDetailsTable.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        pad_keyTyped(e);
      }
    });
    _landPatternDetailsTable.setModel(_landPatternDetailsTableModel);
    _landPatternDetailsScrollPane.getViewport().add(_landPatternDetailsTable, null);
    // add the listener for a table row selection
    ListSelectionModel rowSM = _landPatternDetailsTable.getSelectionModel();
    rowSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();

        if (lsm.isSelectionEmpty() || lsm.getValueIsAdjusting())
        {
          // disable edit menu item when no pads are selected
          editHoleLocationMenuItemSetEnabled(false);
          makePadOneMenuItemSetEnabled(false);
        }
        else
        {
          // we want to select the pad(s) in the graphics window corresponding to the newly selected table row(s)
          int selectedRows[] = _landPatternDetailsTable.getSelectedRows();
          java.util.List<LandPatternPad> landPatternPads = new ArrayList<LandPatternPad>();
          for (int i = 0; i < selectedRows.length; i++)
          {
            LandPatternPad landPatternPad = _landPatternDetailsTableModel.getLandPatternPadAt(selectedRows[i]);
            landPatternPads.add(landPatternPad);
          }
          _landPatternDetailsTableModel.setSelectedPads(landPatternPads);

          // enable edit menu item when a pad is selected
          if (landPatternPads.size() == 1)
          {
            makePadOneMenuItemSetEnabled(true);
            if (landPatternPads.get(0) instanceof ThroughHoleLandPatternPad)
              editHoleLocationMenuItemSetEnabled(true);
          }
          else
          {
            editHoleLocationMenuItemSetEnabled(false);
            makePadOneMenuItemSetEnabled(false);
          }

          if (_shouldInteractWithGraphics)
            _guiObservable.stateChanged(landPatternPads, SelectionEventEnum.LANDPATTERN_PAD);
        }
      }
    });

    _landPatternActionsWrapperPanel.add(_landPatternActionsPanel);
    _landPatternActionsWrapperPanel.setLayout(_landPatternActionsWrapperPanelFlowLayout);
    _landPatternActionsWrapperPanelFlowLayout.setAlignment(FlowLayout.LEFT);

    _landPatternActionsPanel.setLayout(_landPatternActionsPanelGridLayout);
    _landPatternActionsPanelGridLayout.setColumns(2);
    _landPatternActionsPanelGridLayout.setHgap(10);
    _landPatternActionsPanelGridLayout.setRows(0);
    _landPatternActionsPanelGridLayout.setVgap(10);
//    _landPatternActionsPanel.add(_deleteComponentButton, null);
    _landPatternActionsPanel.add(_addPadButton, null);
    _addPadButton.setText(StringLocalizer.keyToString("CAD_ADD_PAD_KEY"));
    _addPadButton.setToolTipText(StringLocalizer.keyToString("CAD_CANNOT_DELETE_PAD_2_PIN_DEVICE_KEY"));
    _addPadButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addPadButton_actionPerformed(e);
      }
    });
    _landPatternActionsPanel.add(_addPadArrayButton, null);
    _addPadArrayButton.setText(StringLocalizer.keyToString("CAD_ADD_PAD_ARRAY_KEY"));
    _addPadArrayButton.setToolTipText(StringLocalizer.keyToString("CAD_CANNOT_DELETE_PAD_2_PIN_DEVICE_KEY"));
    _addPadArrayButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addPadArrayButton_actionPerformed(e);
      }
    });
    _landPatternActionsPanel.add(_deletePadButton, null);
    _deletePadButton.setText(StringLocalizer.keyToString("CAD_DELETE_PADS_KEY"));
    _deletePadButton.setToolTipText(StringLocalizer.keyToString("CAD_CANNOT_DELETE_PAD_2_PIN_DEVICE_KEY"));
    _deletePadButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deletePadButton_actionPerformed();
      }
    });
    _landPatternActionsPanel.add(_changePadNameButton, null);
    _changePadNameButton.setText(StringLocalizer.keyToString("CAD_MODICY_PADS_NAME_KEY"));
    _changePadNameButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        changePadNameButton_actionPerformed(e);
      }
    });
    _landPatternActionsPanel.add(_copyToButton, null);
    _copyToButton.setText(StringLocalizer.keyToString("CAD_COPY_LP_TO_KEY"));
    _copyToButton.setEnabled(false);
    _copyToButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        copyToButton_actionPerformed(e);
      }
    });
    _landPatternActionsPanel.add(_deleteUnusedLandPatternButton, null);
    _deleteUnusedLandPatternButton.setText(StringLocalizer.keyToString("CAD_DELETE_LP_KEY"));
    _deleteUnusedLandPatternButton.setEnabled(false);
    _deleteUnusedLandPatternButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteUnusedLandPatternButton_actionPerformed(e);
      }
    });

    _editLandPatternInfoPanel.setLayout(_editLandPatternInfoPanelBorderLayout);
    _editLandPatternInfoPanel.add(_howToEditLandPatternPad);
    _howToEditLandPatternPad.setForeground(_labelColor);
    _howToEditLandPatternPad.setText(StringLocalizer.keyToString("CAD_EDIT_LPP_INSTRUCTIONS_LABEL_KEY"));

    _newComponentAndFiducialPanel.setLayout(_newComponentAndFiducialPanelBorderLayout);
    _newComponentAndFiducialPanel.add(_compFiducialSplitPane, BorderLayout.CENTER);
    _compFiducialSplitPane.add(_newComponentPanel, JSplitPane.TOP);
    _compFiducialSplitPane.add(_newFiducialPanel, JSplitPane.BOTTOM);
    _newComponentPanel.setLayout(_newComponentPanelBorderLayout);
    _newComponentPanelBorderLayout.setVgap(5);
    _newComponentPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(
        Color.white, new Color(142, 142, 142)), StringLocalizer.keyToString("CAD_NEW_COMPONENTS_KEY")), BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _newComponentPanel.add(_addComponentPanel, BorderLayout.SOUTH);
    _newComponentPanel.add(_newComponentDetailsPanel, BorderLayout.CENTER);
//    _newComponentPanel.add(_newComponentActionsPanel, BorderLayout.SOUTH);

    _addComponentPanel.setLayout(_addComponentPanelFlowLayout);
    _addComponentPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _addComponentPanelFlowLayout.setHgap(0);
    _addComponentPanelFlowLayout.setVgap(0);
    _addComponentPanel.add(_howToAddComponentLabel);
    _howToAddComponentLabel.setForeground(_labelColor);
    _howToAddComponentLabel.setText(StringLocalizer.keyToString(new LocalizedString("CAD_ADD_ITEM_INSTRUCTIONS_LABEL_KEY",
                                                                                    new Object[]{
                                                                                    StringLocalizer.keyToString("CAD_COMPONENT_KEY")})));

    _newComponentDetailsPanel.setLayout(_newComponentDetailsPanelBorderLayout);
    _newComponentDetailsPanel.add(_newComponentScrollPane, BorderLayout.CENTER);
    _newComponentDetailsPanel.add(_newComponentActionsWrapperPanel, BorderLayout.SOUTH);
    _newComponentTableModel = new NewComponentTableModel(_testDev, this);
    _newComponentDetailsTable = new NewComponentTable(this);
    _newComponentDetailsTable.setModel(_newComponentTableModel);
    _newComponentDetailsTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    _newComponentDetailsTable.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        component_keyTyped(e);
      }
    });
    _newComponentScrollPane.getViewport().add(_newComponentDetailsTable, null);
    final ListSelectionModel newComponentRowSelectionModel = _newComponentDetailsTable.getSelectionModel();
    newComponentRowSelectionModel.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel listSelectionModel = (ListSelectionModel)e.getSource();
//        if (listSelectionModel.isSelectionEmpty() == false)
        if (listSelectionModel.getValueIsAdjusting() == false)
        {
          newComponentRowSelectionModel.removeListSelectionListener(this);
          // we want to select all the other components with the same component type as the selected
          int selectedRows[] = _newComponentDetailsTable.getSelectedRows();
          com.axi.v810.business.panelDesc.Component selectedComponent = null;
          java.util.List<ComponentType> componentTypes = new ArrayList<ComponentType>();
          Set<com.axi.v810.business.panelDesc.Component> selectedComponents =
              new HashSet<com.axi.v810.business.panelDesc.Component>();

          for (int i = 0; i < selectedRows.length; i++)
          {
            selectedComponent = _newComponentTableModel.getComponentAt(selectedRows[i]);
            ComponentType selectedComponentType = selectedComponent.getComponentType();
            java.util.List < com.axi.v810.business.panelDesc.Component >
                compForSelCompType = selectedComponentType.getComponents();
            // add the current component type to the list to highlighted in the graphics only if not already there
            if (componentTypes.contains(selectedComponentType) == false)
            {
              componentTypes.add(selectedComponentType);
              for (com.axi.v810.business.panelDesc.Component component : compForSelCompType)
              {
                int index = _newComponentTableModel.getRowForComponent(component);
                _newComponentDetailsTable.addRowSelectionInterval(index, index);
                selectedComponents.add(component);
              }
            }
            else
              continue;
          }
          // tell graphics to highlight all the components of the component type from the list passed in
          if (_shouldInteractWithGraphics)
          {
            if (componentTypes.size() != 0)
              _guiObservable.stateChanged(componentTypes, SelectionEventEnum.COMPONENT_TYPE);
          }
          newComponentRowSelectionModel.addListSelectionListener(this);
          _newComponentTableModel.setSelectedComponents(selectedComponents);
        }
      }
    });
    _newComponentActionsWrapperPanel.add(_newComponentActionsPanel);
    _newComponentActionsWrapperPanel.setLayout(_newComponentActionsWrapperPanelFlowLayout);
    _newComponentActionsWrapperPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _newComponentActionsPanel.setLayout(_newComponentActionsPanelGridLayout);
    _newComponentActionsPanelGridLayout.setHgap(10);
    _newComponentActionsPanel.add(_addComponentButton, null);
    _addComponentButton.setText(StringLocalizer.keyToString("CAD_ADD_BUTTON_KEY"));
    _addComponentButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addComponentButton_actionPerformed(e);
      }
    });
    _newComponentActionsPanel.add(_deleteComponentButton, null);
    _deleteComponentButton.setText(StringLocalizer.keyToString("CAD_DELETE_BUTTON_KEY"));
    _deleteComponentButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteComponentButton_actionPerformed();
      }
    });

    _newFiducialPanel.setLayout(_newFiducialPanelBorderLayout);
    _newFiducialPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(
        Color.white, new Color(142, 142, 142)), StringLocalizer.keyToString("CAD_FIDUCIALS_KEY")),
        BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _newFiducialPanel.add(_addFiducialPanel, BorderLayout.SOUTH);
    _newFiducialPanel.add(_newFiducialDetailsPanel, BorderLayout.CENTER);

    _addFiducialPanel.setLayout(_addFiducialPanelFlowLayout);
    _addFiducialPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _addFiducialPanelFlowLayout.setHgap(0);
    _addFiducialPanelFlowLayout.setVgap(0);
    _addFiducialPanel.add(_howToAddFiducialLabel);
    _howToAddFiducialLabel.setForeground(_labelColor);
    _howToAddFiducialLabel.setText(StringLocalizer.keyToString(new LocalizedString("CAD_ADD_ITEM_INSTRUCTIONS_LABEL_KEY",
                                                                               new Object[]{StringLocalizer.keyToString("CAD_FIDUCIAL_KEY")})));

    _newFiducialDetailsPanel.setLayout(_newFiducialDetailsPanelBorderLayout);
    _newFiducialDetailsPanel.add(_newFiducialScrollPane, BorderLayout.CENTER);
    _newFiducialDetailsPanel.add(_newFiducialActionsWrapperPanel, BorderLayout.SOUTH);
    _newFiducialTableModel = new NewFiducialTableModel(_testDev, this);
    _newFiducialDetailsTable = new NewFiducialTable();
    _newFiducialDetailsTable.setModel(_newFiducialTableModel);
    _newFiducialDetailsTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    _newFiducialDetailsTable.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        fiducial_keyTyped(e);
      }
    });
    _newFiducialScrollPane.getViewport().add(_newFiducialDetailsTable, null);
    final ListSelectionModel newFiducialRowSelectionModel = _newFiducialDetailsTable.getSelectionModel();
    newFiducialRowSelectionModel.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel listSelectionModel = (ListSelectionModel)e.getSource();
//        if (lsm.isSelectionEmpty() == false)
        if (listSelectionModel.getValueIsAdjusting() == false)
        {
          newFiducialRowSelectionModel.removeListSelectionListener(this);
          // we want to select all the other fiducials with the same fiducial type as the selected
          int selectedRows[] = _newFiducialDetailsTable.getSelectedRows();
          Fiducial selectedFiducial = null;
          java.util.List<FiducialType> fiducialTypes = new ArrayList<FiducialType>();
          Set<Fiducial> selectedFiducials = new HashSet<Fiducial>();

          for (int i = 0; i < selectedRows.length; i++)
          {
            selectedFiducial = _newFiducialTableModel.getFiducialAt(selectedRows[i]);
            FiducialType selectedFiducialType = selectedFiducial.getFiducialType();
            java.util.List <Fiducial> fidsForSelectedFiducialType = selectedFiducialType.getFiducials();
            // add the current fiducial type to the list to highlighted in the graphics only if not already there
            if (fiducialTypes.contains(selectedFiducialType) == false)
            {
              fiducialTypes.add(selectedFiducialType);
              for (Fiducial fiducial : fidsForSelectedFiducialType)
              {
                int index = _newFiducialTableModel.getRowForFiducial(fiducial);
                _newFiducialDetailsTable.addRowSelectionInterval(index, index);
                selectedFiducials.add(fiducial);
              }
            }
            else
              continue;
          }
          // tell graphics to highlight all the fiducials if only one fiducial type was selected,
          // clear selection otherwise
          if (_shouldInteractWithGraphics)
            _guiObservable.stateChanged(fiducialTypes, SelectionEventEnum.FIDUCIAL_TYPE);

          newFiducialRowSelectionModel.addListSelectionListener(this);
          _newFiducialTableModel.setSelectedFiducials(selectedFiducials);
        }
      }
    });

    _newFiducialActionsWrapperPanel.add(_newFiducialActionsPanel);
    _newFiducialActionsWrapperPanel.setLayout(_newFiducialActionsWrapperPanelFlowLayout);
    _newFiducialActionsWrapperPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _newFiducialActionsPanel.setLayout(_newFiducialActionsPanelGridLayout);
    _newFiducialActionsPanelGridLayout.setHgap(10);
    _newFiducialActionsPanel.add(_addFiducialButton, null);
    _newFiducialActionsPanel.add(_deleteFiducialButton, null);

    _addFiducialButton.setText(StringLocalizer.keyToString("CAD_ADD_BUTTON_KEY"));
    _addFiducialButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addFiducialButton_actionPerformed();
      }
    });

    _deleteFiducialButton.setText(StringLocalizer.keyToString("CAD_DELETE_BUTTON_KEY"));
    _deleteFiducialButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteFiducialButton_actionPerformed();
      }
    });

    _selectLandPatternComboBox.setName(".selectLandPatternComboBox");
    _addPadButton.setName(".addPadButton");
    _addPadArrayButton.setName(".addPadArrayButton");
    _deletePadButton.setName(".deletePadButton");
    _changePadNameButton.setName(".changePadNameButton");
    _copyToButton.setName(".copyToButton");
    _deleteUnusedLandPatternButton.setName(".deleteLandPatternButton");

    _editHoleLocationMenuItem = new JMenuItem();
    _editHoleLocationMenuItem.setText(StringLocalizer.keyToString("CAD_EDIT_HOLE_LOCATION_TITLE_KEY"));
    editHoleLocationMenuItemSetEnabled(false);
    _editHoleLocationMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        LandPatternPad landPatternPad = _landPatternDetailsTableModel.getLandPatternPadAt(_landPatternDetailsTable.getSelectedRow());
        Assert.expect(landPatternPad instanceof ThroughHoleLandPatternPad);
        showEditHoleLocationDialog((ThroughHoleLandPatternPad)landPatternPad);
      }
    });

    _makePadOneMenuItem = new JMenuItem();
    _makePadOneMenuItem.setText(StringLocalizer.keyToString("CAD_ASSIGN_PAD_ONE_MENU_ITEM_KEY"));
    makePadOneMenuItemSetEnabled(false);
    _makePadOneMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        LandPatternPad landPatternPad = _landPatternDetailsTableModel.getLandPatternPadAt(_landPatternDetailsTable.getSelectedRow());
        setLandPatternPadOne(landPatternPad);
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    savePersistSettings();
    _project = null;
    _currentBoardType = null;
    _panelFoundOnRightClick = null;
    _componentsFoundOnRightClick.clear();
    _boardToAddTo = null;
    _selectLandPatternComboBox.removeAllItems();
    if (_landPatterns != null)
      _landPatterns.clear();
    _selectLandPatternComboBox.removeAllItems();
    _landPatternDetailsTable.resetEditorsAndRenderers();
    _landPatternDetailsTableModel.clear();
    _landPatternDetailsTable.resetSortHeader();
    _newComponentDetailsTable.resetEditorsAndRenderers();
    _newComponentTableModel.clearData();
    _newFiducialTableModel.clearData();
    _newComponentDetailsTable.resetSortHeader();
    _newFiducialDetailsTable.resetSortHeader();
    _verifyCadPanel.unpopulate();
    _editPshSettingsPanel.unpopulate();//Siew Yeng - XCR-3781
    _editCadTabbedPane.setSelectedIndex(0);
    _landPatternTabIsVisible = false;
    _verifyCadTabIsVisible = true;
    ProjectObservable.getInstance().deleteObserver(this);
    _guiObservable.deleteObserver(this);
  }

  /**
   * @author Laura Cormos
   */
  void populateWithProjectData()
  {
    _uiPersistSettings = _testDev.getPersistance();  // non project specific persist class

    // populate land patterns data
    _shouldInteractWithGraphics = false;
    _selectLandPatternComboBox.removeAllItems();

    java.util.List<LandPattern> landPatternList = new ArrayList<LandPattern>();

    _currentBoardType = _testDev.getCurrentBoardType();
    landPatternList = _currentBoardType.getPanel().getLandPatterns();

    Assert.expect(landPatternList != null);
    _landPatterns = new ArrayList<LandPattern>(landPatternList);
    Collections.sort(_landPatterns, new LandPatternAlphaNumericComparator(true));
    populateLandPatternComboBox();
    _landPatternDetailsTableModel.setProject(_project);

    int numLandPatterns = _selectLandPatternComboBox.getItemCount();
    if (numLandPatterns <= 1)
    {
      // there are no land patterns for this project so clear everything out, only the New... string, at most
      _selectLandPatternComboBox.setSelectedItem(null);
      _landPatternDetailsTableModel.clear();
    }
    else if (numLandPatterns >= 2)
    {
      // there are some land patterns
      Object selectedLp = _selectLandPatternComboBox.getSelectedItem();
      LandPattern lp;
      // if one is selected, load it up
      if (selectedLp != null && selectedLp instanceof LandPattern)
      {
        lp = (LandPattern)selectedLp;
      }
      else // go to first on the list
      {
        selectedLp = _selectLandPatternComboBox.getItemAt(1);
        Assert.expect(selectedLp instanceof LandPattern);
        lp = (LandPattern)selectedLp;
      }

      Assert.expect(lp != null);
      setupAndDisplayLandPattern(lp);
    }

    // populate components data
//    updateComponentsTableModel(); this line is for components table to show all components on the panel
    _newComponentTableModel.clearData();
    _newComponentTableModel.setProject(_project);

    // populate fiducials data
    // collect all fiducials from the panel and each board into a list to be sent to the fiducial table model
    updateFiducialsTableModel();

    setupTableEditors();
    _shouldInteractWithGraphics = true;
  }

  /**
   * @author George Booth
   */
  void populateWithProjectData(Project project)
  {
    // assumption: this method is to be called only when a new project is loaded
    // and not when switching between tasks
    Assert.expect(project != null);
    _project = project;

    updateTableEditorsWithDisplayUnits();

    ProjectObservable.getInstance().addObserver(this);
    _guiObservable.addObserver(this);

    if (_active)
    {
      populateWithProjectData();
      _populateProjectData = false;
    }
    else
      _populateProjectData = true;

    _verifyCadPanel.populateWithProjectData(_project);
  }

  /**
   * collect all fiducials from the panel and each board of the currently selected board type
   * @author Laura Cormos
   */
  private void updateFiducialsTableModel()
  {
    java.util.List<Fiducial> fiducials = new ArrayList<Fiducial>();
    fiducials.addAll(_project.getPanel().getFiducials());
    java.util.List<Board> boards = _project.getPanel().getBoards();
    for (Board board : boards)
    {
      if (board.getBoardType() == _currentBoardType)
        fiducials.addAll(board.getFiducials());
    }
    _newFiducialTableModel.clearData();
    _newFiducialTableModel.setProject(_project);
    _newFiducialTableModel.populateWithFiducials(fiducials);
  }

  /**
   * collect all components from each board of the currently selected board type
   * @author Laura Cormos
   */
  private void updateComponentsTableModel()
  {
    java.util.List<com.axi.v810.business.panelDesc.Component> components = new ArrayList<com.axi.v810.business.panelDesc.Component>();
    java.util.List<Board> boards = _project.getPanel().getBoards();
    for (Board board : boards)
    {
      if (board.getBoardType() == _currentBoardType)
        components.addAll(board.getComponents());
    }
    _newComponentTableModel.clearData();
    _newComponentTableModel.setProject(_project);
    _newComponentTableModel.populateWithComponents(components);
  }

  /**
   * @author Laura Cormos
   */
  private void populateLandPatternComboBox()
  {
    ActionListener [] actListenerList = _selectLandPatternComboBox.getActionListeners();
    boolean found = false;
    for (int i = 0; i < actListenerList.length; i++)
      if (actListenerList[i] == _selectLandPatternActionListener)
        found = true;
    if (found)
      _selectLandPatternComboBox.removeActionListener(_selectLandPatternActionListener);
    _selectLandPatternComboBox.removeAllItems();
    boolean flag = _shouldInteractWithGraphics;
    _shouldInteractWithGraphics = false;
    _selectLandPatternComboBox.addItem(StringLocalizer.keyToString("CAD_NEW_ITEM_KEY"));
    _shouldInteractWithGraphics = flag;
    for (LandPattern lp : _landPatterns)
    {
      _selectLandPatternComboBox.addItem(lp);
    }
    if (found)
      _selectLandPatternComboBox.addActionListener(_selectLandPatternActionListener);
  }

  /**
   * Set up how each column in each table can be edited. Some are text editor, other are numeric editors, etc...
   * This also sets up the available options for columns with combo boxes.
   * @author Laura Cormos
   */
  private void setupTableEditors()
  {
    _newComponentDetailsTable.setEditorsAndRenderers();

    Set<Double> customRotationValues = new HashSet<Double>();
    for (LandPattern landPattern : _landPatterns)
    {
      for (LandPatternPad lpPad : landPattern.getLandPatternPads())
      {
        customRotationValues.add(lpPad.getDegreesRotation());
      }
    }
    _landPatternDetailsTable.setEditorsAndRenderers(customRotationValues);

    _newFiducialDetailsTable.setEditorsAndRenderers();

    //set up table row height based on the size of the font
    FontMetrics fm = _landPatternDetailsTable.getFontMetrics(getFont());
    int prefHeight = fm.getHeight() + MainMenuGui.TABLE_ROW_HEIGHT_EXTRA;
    
    // set up table row height based on the size of the font - this is like setting the number of rows
    // on a JList.  The only way to do this with Tables is with setPreferredSize
    // it's okay to do set size here.. bill just doesn't like it on the whole panel.
    Dimension prefScrollSize = new Dimension(200, prefHeight * 20); // 10 rows for tables
    _newComponentScrollPane.setPreferredSize(prefScrollSize);
    _newFiducialScrollPane.setPreferredSize(prefScrollSize);

    // set up column widths
    _newComponentDetailsTable.setPreferredColumnWidths();
    _newFiducialDetailsTable.setPreferredColumnWidths();
    _landPatternDetailsTable.setPreferredColumnWidths();

    // make sure all tables display numbers with the appropriate number of decimal digits as dictated by
    // the project display units
    updateNumericDecimalPlaces();
    updateTableEditorsWithDisplayUnits();
  }

  /**
   * @author Laura Cormos
   */
  void updateCadGraphics()
  {
    _testDev.initGraphicsToLandPattern();
    _shouldInteractWithGraphics = true;
    if (_landPatternTabIsVisible)
      _testDev.switchToLandPatternGraphics();
    else if(_editPshSettingsVisible && _project.getPanel().getPanelSettings().isPanelBasedAlignment() ==  false)//Siew Yeng - XCR-3844
    {
      //Siew Yeng - XCR-3781
      if(_testDev.isBoardGraphicsAlreadyDrawnInAlignment() == false)
      {
        _testDev.initGraphicsToBoard();
        _testDev.changeGraphicsToBoard(_project.getPanel().getBoards().get(0));
        _testDev.switchToBoardGraphics();
      }
    }
    else
      _testDev.switchToPanelGraphics();
  }

  /**
   * @author Laura Cormos
   */
  private void landPatternSelectComboBox_actionPerformed(ActionEvent e)
  {
    cancelEditingIfNecessary(_landPatternDetailsTable);
    Object selectedLp = _selectLandPatternComboBox.getSelectedItem();
    if ((selectedLp != null) && ((selectedLp instanceof String) == false)) // this is NOT the "new . . ." case
    {
      _deleteUnusedLandPatternButton.setEnabled(true);
      _copyToButton.setEnabled(true);
      _changePadNameButton.setEnabled(true);
      LandPattern lp = (LandPattern)_selectLandPatternComboBox.getSelectedItem();
      _landPatternDetailsTableModel.populateWithLandPattern(lp);
//      if (lp.getLandPatternPads().size() > 0)
      _testDev.changeGraphicsToLandPattern(lp);
//      else
//        _testDev.clearLandPatternGraphics();

      // check if the selected land pattern belongs to a 2 pin device, such as a res, cap or pcap.
      // If so, these devices can only have 2 pads, no more, no less so we'll disabled add/delete buttons
      // Also these devices cannot have pads rotated at anything other than 0 or 180 degrees
      setGraphicsControlsFor2PinDevice(lp);
      
      // set delete pad button appropriately
      setDeletePadButtonState(lp);
      
      //the new landpatternpad list might be null at this point but it will be contain value when executing gui event handling.
      //perform a statechange after value of combo box changed to highlight the changed component
      
      //Khaw Chek Hau - XCR3471: System crash when switch tab after create new land pattern
      if ( _currentLandPattern == lp && _shouldInteractWithGraphics && _inLandPatternUndoAction)
      {
        _guiObservable.stateChanged(_newHighlightedlandPatternPads, SelectionEventEnum.LANDPATTERN_PAD);
      }
      
      _inLandPatternUndoAction = false;
      _currentLandPattern = lp;
    }
    else if (selectedLp != null) // creating a new land pattern
    {
      if (selectedLp instanceof String)
      {
        // new land pattern.
        if (_shouldInteractWithGraphics)
        {
          String newLandPatternName = JOptionPane.showInputDialog(this, StringLocalizer.keyToString("CAD_ENTER_ITEM_NAME_KEY"),
              StringLocalizer.keyToString("CAD_NEW_LP_TITLE_KEY"), JOptionPane.INFORMATION_MESSAGE);
          if (newLandPatternName != null) // null means the dialog was canceled
          {
            com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
            _deleteUnusedLandPatternButton.setEnabled(false);
            _copyToButton.setEnabled(false);
            while (newLandPatternName != null && (panel.isLandPatternNameValid(newLandPatternName) == false || panel.isLandPatternNameDuplicate(newLandPatternName)))
            {
              if (panel.isLandPatternNameValid(newLandPatternName) == false)
              {
                String illegalChars = panel.getLandPatternNameIllegalChars();
                LocalizedString message = null;
                if (illegalChars.equals(" "))
                  message = new LocalizedString("CAD_INVALID_LAND_PATTERN_NAME_SPACES_KEY", new Object[]
                                                {newLandPatternName});
                else
                  message = new LocalizedString("CAD_INVALID_LAND_PATTERN_NAME_OTHER_CHARS_KEY", new Object[]
                                                {newLandPatternName, illegalChars});

                newLandPatternName = JOptionPane.showInputDialog(this,
                                                                 StringLocalizer.keyToString(message),
                                                                 StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                                 JOptionPane.INFORMATION_MESSAGE);
              }
              else if (panel.isLandPatternNameDuplicate(newLandPatternName))
              {
                newLandPatternName = JOptionPane.showInputDialog(this,
                                                        StringLocalizer.keyToString("CAD_LAND_PATTERN_NAME_DUPLICATE_KEY"),
                                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                        JOptionPane.INFORMATION_MESSAGE);
              }
            }
            if (newLandPatternName == null)
              return; // dialog was canceled without a name entered

            // XCR-3698 Unable to add new land pattern
            try
            {
              _landPatternDetailsTableModel.clear();

              _commandManager.trackState(getCurrentUndoState());
              _commandManager.execute(new CreateLandPatternCommand(panel, newLandPatternName, true));

              _testDev.changeGraphicsToLandPattern(panel.getLandPattern(newLandPatternName));
              _landPatternDetailsTableModel.setProject(_project);
              _deleteUnusedLandPatternButton.setEnabled(true);
              _copyToButton.setEnabled(true);
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            }
          }
          else // dialog for new land pattern name was cancelled.
          {
            setLandpatternScreenStateForNullLandPattern();
          }
        }
        // the "New..." string was selected in the combo box but we're not supposed to interact with graphics - clear up the graphics
        setLandpatternScreenStateForNullLandPattern();
        
        //Khaw Chek Hau - XCR3471: System crash when switch tab after create new land pattern
        _inLandPatternUndoAction = false;
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void setLandpatternScreenStateForNullLandPattern()
  {
    _selectLandPatternComboBox.setSelectedItem(null);
    _landPatternDetailsTableModel.clear();
    _testDev.clearLandPatternGraphics();
    _deleteUnusedLandPatternButton.setEnabled(false);
    _copyToButton.setEnabled(false);
    _addPadButton.setEnabled(false);
    _addPadArrayButton.setEnabled(false);
    _changePadNameButton.setEnabled(false);
  }

  /**
   * @author Laura Cormos
   */
  private void addPadButton_actionPerformed(ActionEvent e)
  {
    Object obj = _selectLandPatternComboBox.getSelectedItem();
    if (obj instanceof LandPattern)
    {
      LandPattern landPattern = (LandPattern)obj;
      int size = MathUtil.convertMilsToNanoMetersInteger(30);
      java.util.List<ComponentCoordinate> selectedPads = new ArrayList<ComponentCoordinate>();
      selectedPads.add(new ComponentCoordinate(0, 0));
      try
      {
        _commandManager.trackState(getCurrentUndoState());
        _commandManager.execute(new CreateSurfaceMountLandPatternPadCommand(landPattern, selectedPads,
                                                                            0.0, size, size, ShapeEnum.RECTANGLE));
        setDeletePadButtonState(landPattern);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void addPadArrayButton_actionPerformed(ActionEvent e)
  {
    Object obj = _selectLandPatternComboBox.getSelectedItem();
    if (obj instanceof LandPattern)
    {
      LandPattern landPattern = (LandPattern)obj;
      AddPadArrayDialog dlg = new AddPadArrayDialog(_mainUI, landPattern, _project);
      SwingUtils.centerOnComponent(dlg, _mainUI);
      dlg.setVisible(true);
      dlg.dispose();

      _testDev.landPatternScaleDrawing();
      setDeletePadButtonState(landPattern);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void deletePadButton_actionPerformed()
  {
    //XCR-3464, System crash when switch tab after delete pad
    if (is2PinDeviceLandPattern(_currentLandPattern))
    {
      MessageDialog.showErrorDialog(_mainUI, 
                                    StringLocalizer.keyToString("CAD_CANNOT_DELETE_2_PIN_PAD_COMPONENT_KEY"),
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
      return;
    }
    //XCR-2082
    //Kok Chun, Tan - add busy cancel dialog because delete pads is slow when the number of pads is large.
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
      StringLocalizer.keyToString("CAD_BUSYCANCELDIALOG_DELETE_PADS_MESSAGE_KEY"),
      StringLocalizer.keyToString("CAD_BUSYCANCELDIALOG_DELETE_PADS_TITLE_KEY"),
      true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
    busyCancelDialog.setCursor(new Cursor(Cursor.WAIT_CURSOR));
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        java.util.List<LandPatternPad> selectedPads = new ArrayList<LandPatternPad>();
        try
        {
          ProjectObservable.getInstance().setEnabled(false);
          if (_landPatternDetailsTable.getSelectedRow() != -1)
          {
            _shouldInteractWithGraphics = false;
            int selectedRows[] = _landPatternDetailsTable.getSelectedRows();
            //java.util.List<LandPatternPad> selectedPads = new ArrayList<LandPatternPad>();
            // first gather all the selected pads since the table model will be modified as deletion happens.
            LandPattern currentLandPattern = _landPatternDetailsTableModel.getLandPatternPadAt(selectedRows[0]).getLandPattern();
            int maxPadsToDelete = selectedRows.length;
            int landPatternPadNum = currentLandPattern.getLandPatternPads().size();
            Assert.expect(maxPadsToDelete <= landPatternPadNum);
            // don't allow the last pad to be deleted, basically don't leave an empty landpattern
            if (maxPadsToDelete == landPatternPadNum)
            {
              maxPadsToDelete--;
            }

            for (int i = 0; i < maxPadsToDelete; i++)
            {
              selectedPads.add(_landPatternDetailsTableModel.getLandPatternPadAt(selectedRows[i]));
            }
            cancelEditingIfNecessary(_landPatternDetailsTable);
            _commandManager.trackState(getCurrentUndoState());
            _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_DESTROY_LANDPATTERN_PAD_KEY"));

            try
            {
              _commandManager.execute(new RemoveLandPatternPadCommand(selectedPads));
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            }

            _commandManager.endCommandBlock();
            setDeletePadButtonState(currentLandPattern);
            _shouldInteractWithGraphics = true;
          }
        }
        finally
        {
          ProjectObservable.getInstance().setEnabled(true);          
          ProjectObservable.getInstance().stateChanged(this, selectedPads, null, LandPatternPadEventEnum.ADD_OR_REMOVE_LIST);
          
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              busyCancelDialog.dispose();
            }
          });
        }
      }
    });
    busyCancelDialog.setVisible(true);
  }

  /**
   * @author Laura Cormos
   */
  private void changePadNameButton_actionPerformed(ActionEvent e)
  {
    if (_landPatternDetailsTable.getSelectedRow() != -1)
    {
      ChangePadNameDialog changePadNameDialog = new ChangePadNameDialog(_mainUI,
                                                                        StringLocalizer.keyToString("CAD_MODIFY_LPP_NAME_KEY"),
                                                                        true);
      SwingUtils.centerOnComponent(changePadNameDialog, this);
      changePadNameDialog.setVisible(true);
      if (changePadNameDialog.userClickedOk())
      {
        String textToAdd = changePadNameDialog.getTextToAdd();
        boolean isSuffix = changePadNameDialog.isSuffix();
        _shouldInteractWithGraphics = false;
        int selectedRows[] = _landPatternDetailsTable.getSelectedRows();

        _commandManager.trackState(getCurrentUndoState());
        _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_LAND_PATTERN_PAD_SET_NAME_KEY"));

        for (int i = 0; i < selectedRows.length; i++)
        {
          LandPatternPad landPatternPad = _landPatternDetailsTableModel.getLandPatternPadAt(selectedRows[i]);
          String padName = landPatternPad.getName();
          if (isSuffix)
          {
            if (landPatternPad.getLandPattern().isLandPatternPadNameValid(padName + textToAdd) == false)
            {
              String illegalChars = landPatternPad.getLandPattern().getLandPatternPadNameIllegalChars();
              LocalizedString message = null;
              if (illegalChars.equals(" "))
                message = new LocalizedString("CAD_INVALID_LAND_PATTERN_PAD_NAME_SPACES_KEY", new Object[]
                                              {padName + textToAdd});
              else
                message = new LocalizedString("CAD_INVALID_LAND_PATTERN_PAD_NAME_OTHER_CHARS_KEY", new Object[]
                                              {padName + textToAdd, illegalChars});

              JOptionPane.showMessageDialog(this,
                                            StringLocalizer.keyToString(message),
                                            StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                            JOptionPane.INFORMATION_MESSAGE);
              break;
            }
            else if (landPatternPad.getLandPattern().isLandPatternPadNameDuplicate(padName + textToAdd))
            {
              JOptionPane.showMessageDialog(this,
                                            StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_LPP_SUFFIX_NAME_KEY",
                                                                                            new Object[]{padName + textToAdd})),
                                            StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                            JOptionPane.INFORMATION_MESSAGE);
              break;
            }
            else
            {
              try
              {
                _commandManager.trackState(getCurrentUndoState());
                _commandManager.execute(new LandPatternPadSetNameCommand(landPatternPad, padName + textToAdd));
              }
              catch (XrayTesterException ex)
              {
                MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
                _landPatternDetailsTableModel.fireTableRowsUpdated(selectedRows[i], selectedRows[i]);
              }
            }
          }
          else // is a prefix
          {
            if (landPatternPad.getLandPattern().isLandPatternPadNameValid(textToAdd + padName) == false)
            {
              String illegalChars = landPatternPad.getLandPattern().getLandPatternPadNameIllegalChars();
              LocalizedString message = null;
              if (illegalChars.equals(" "))
                message = new LocalizedString("CAD_INVALID_LAND_PATTERN_PAD_NAME_SPACES_KEY", new Object[]
                                              {textToAdd + padName});
              else
                message = new LocalizedString("CAD_INVALID_LAND_PATTERN_PAD_NAME_OTHER_CHARS_KEY", new Object[]
                                              {textToAdd + padName, illegalChars});
              JOptionPane.showMessageDialog(this,
                                            StringLocalizer.keyToString(message),
                                            StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                            JOptionPane.INFORMATION_MESSAGE);
              break;
            }
            else if (landPatternPad.getLandPattern().isLandPatternPadNameDuplicate(textToAdd + padName))
            {
              JOptionPane.showMessageDialog(this,
                                            StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_LPP_SUFFIX_NAME_KEY",
                                                                                            new Object[]{textToAdd + padName})),
                                            StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                            JOptionPane.INFORMATION_MESSAGE);
              break;
            }
            else
            {
              try
              {
                _commandManager.trackState(getCurrentUndoState());
                _commandManager.execute(new LandPatternPadSetNameCommand(landPatternPad, textToAdd + padName));
              }
              catch (XrayTesterException ex)
              {
                MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
                _landPatternDetailsTableModel.fireTableRowsUpdated(selectedRows[i], selectedRows[i]);
              }
            }
          }
        }
        _commandManager.endCommandBlock();
        _shouldInteractWithGraphics = true;
      }
    }
  }


  /**
   * @author Laura Cormos
   */
  private void deleteUnusedLandPatternButton_actionPerformed(ActionEvent e)
  {
    Object entry = _selectLandPatternComboBox.getSelectedItem();
    if (entry instanceof LandPattern == false)
      return;
    LandPattern landPattern = (LandPattern)_selectLandPatternComboBox.getSelectedItem();
    if (landPattern != null)
    {
      if (landPattern.isInUse())
      {
        JOptionPane.showMessageDialog(this,
                                      StringLocalizer.keyToString(new LocalizedString("CAD_CANT_DELETED_USED_LP_KEY",
                                                                                      new Object[]{landPattern.getName()})),
                                      StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                      JOptionPane.INFORMATION_MESSAGE);
        return;
      }
      //Khaw Chek Hau - XCR3472: Assert when switch tab after delete all the land pattern
      else if (_selectLandPatternComboBox.getItemCount() <= 2)
      {
        JOptionPane.showMessageDialog(this,
                                      StringLocalizer.keyToString("CAD_CANNOT_DELETE_LAST_LAND_PATTERN_KEY"),   
                                      StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                      JOptionPane.INFORMATION_MESSAGE);
        return;
      }
      else if (landPattern.getLandPatternPads().isEmpty() == false)
      {
        _commandManager.trackState(getCurrentUndoState());
        _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_BLOCK_DELETE_NON_EMPTY_LAND_PATTERN_KEY"));
        try
        {
          _commandManager.execute(new RemoveLandPatternCommand(landPattern));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
        }
        _commandManager.endCommandBlock();
      }
      else
      {
        try
        {
          _commandManager.trackState(getCurrentUndoState());
          _commandManager.execute(new RemoveLandPatternCommand(landPattern));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
        }
      }
    }
  }
  /**
   * @author Laura Cormos
   */
  private void copyToButton_actionPerformed(ActionEvent e)
  {
    Object obj = _selectLandPatternComboBox.getSelectedItem();
    if (obj instanceof LandPattern)
    {
      LandPattern originalLandPattern = (LandPattern)obj;
      String landPatName = JOptionPane.showInputDialog(this,
                                                       StringLocalizer.keyToString("CAD_ENTER_ITEM_NAME_KEY"),
                                                       StringLocalizer.keyToString("CAD_COPY_DIALOG_TITLE_KEY"),
                                                       JOptionPane.INFORMATION_MESSAGE);
      if (landPatName != null) // null means the dialog was canceled
      {
        com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
        if (panel.isLandPatternNameValid(landPatName) == false)
        {
          String illegalChars = panel.getLandPatternNameIllegalChars();
          LocalizedString message = null;
          if (illegalChars.equals(" "))
            message = new LocalizedString("CAD_INVALID_LAND_PATTERN_NAME_SPACES_KEY", new Object[]
                                          {landPatName});
          else
            message = new LocalizedString("CAD_INVALID_LAND_PATTERN_NAME_OTHER_CHARS_KEY", new Object[]
                                          {landPatName, illegalChars});

          JOptionPane.showMessageDialog(this,
                                        StringLocalizer.keyToString(message),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
          return;
        }
        else if (panel.isLandPatternNameDuplicate(landPatName))
        {
          JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("CAD_LAND_PATTERN_NAME_DUPLICATE_KEY"),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
          return;
        }
        else
        {
          // this call generates a "new LP created" event whose servicing will update the lp combo box
          LandPatternCopyCommand landPatternCopyCommand = new LandPatternCopyCommand(originalLandPattern, landPatName);
          try
          {
            _commandManager.trackState(getCurrentUndoState());
            _commandManager.execute(landPatternCopyCommand);
            LandPattern newLandPattern = landPatternCopyCommand.getNewLandPattern();
            _landPatternDetailsTableModel.populateWithLandPattern(newLandPattern);
            if (newLandPattern.getLandPatternPads().size() > 0)
            {
              _testDev.changeGraphicsToLandPattern(newLandPattern);
            }
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          }
        }
      }
    }
  }

  /**
   * @author Laura Cormos
   * @author George Booth
   */
  private void editCadTabbedPane_stateChanged(ChangeEvent e)
  {
    if (!_active)
      return;

    cancelEditingIfNecessary(_landPatternDetailsTable);
    cancelEditingIfNecessary(_newComponentDetailsTable);
    cancelEditingIfNecessary(_newFiducialDetailsTable);
    
    // make sure Verify CAD is not in the middle of something
    // moving from verify cad tab
    if (_verifyCadTabIsVisible && _editCadTabbedPane.getSelectedIndex() != 0)
    {
      if (_verifyCadPanel.isReadyToFinish())
      {
        _verifyCadPanel.finish();
        //always clear off the graphics engine.
        _editFocusRegionPanel.finish();
        _verifyCadTabIsVisible = false;
      }
      else
      {
        _editCadTabbedPane.setSelectedIndex(0);  // stay on Verify CAD
        return;
      }
    }
    else if (_editFocusRegionVisible && _editCadTabbedPane.getSelectedIndex() != 3)
    {
        _editFocusRegionPanel.finish();
        _editFocusRegionVisible = false;
    }
    else if(_editPshSettingsVisible && _editCadTabbedPane.getSelectedIndex() != 4)//Siew Yeng - XCR-3781
    {
      _editPshSettingsPanel.finish();
      _editPshSettingsVisible = false;
    }

    if (_editCadTabbedPane.getSelectedIndex() == _VERIFY_CAD) // verify cad tab
    { 
      if(isReadyToFinish()) // Bee Hoon, XCR1672 - To prevent crash after switch away from edit component/fiduncial tab
      {
        _verifyCadPanel.start(_imagePanel);
        _verifyCadTabIsVisible = true;
        _testDev.switchToPanelGraphics();
        _landPatternTabIsVisible = false;
        _guiObservable.deleteObserver(this);
        editHoleLocationMenuItemSetEnabled(false);
        makePadOneMenuItemSetEnabled(false);
      }
      else
      {
        _editCadTabbedPane.setSelectedIndex(2);  // stay on Edit Component/Fiduncial tab
        return;
      }     
    }
    else if (_editCadTabbedPane.getSelectedIndex() == _EDIT_LAND_PATTERNS) // land pattern tab
    {
      if (isReadyToFinish()) // Bee Hoon, XCR1672 - To prevent crash after switch away from edit component/fiduncial tab
      {
        _testDev.switchToLandPatternGraphics();
        _testDev.initGraphicsToLandPattern();
        if (_verifyCadPanel.isCurrentComponentValid())
        {
          ComponentType currentComponent = _verifyCadPanel.getCurrentComponent();
          LandPattern lp = currentComponent.getLandPattern();
          _selectLandPatternComboBox.setSelectedItem(lp);
        }
        Object selectedItem = _selectLandPatternComboBox.getSelectedItem();
        if (selectedItem instanceof LandPattern)
          _testDev.changeGraphicsToLandPattern((LandPattern)selectedItem);

        _testDev.showCadWindow();
        _landPatternTabIsVisible = true;
        _guiObservable.addObserver(this);
      }
      else
      {
        _editCadTabbedPane.setSelectedIndex(2);  // stay on Edit Component/Fiduncial tab
        return;
      }
    }
    else if (_editCadTabbedPane.getSelectedIndex() == _EDIT_FOCUS_REGION) // Edit Focus Region tab
    { 
      if(isReadyToFinish()) // Bee Hoon, XCR1672 - To prevent crash after switch away from edit component/fiduncial tab
      {
        _editFocusRegionPanel.start();
        //_testDev.switchToPackageGraphics();
        _testDev.showCadWindow();
        _testDev.switchToPanelGraphics();
        _landPatternTabIsVisible = false;
        _guiObservable.deleteObserver(this);
        editHoleLocationMenuItemSetEnabled(false);
        makePadOneMenuItemSetEnabled(false);
        
        _editFocusRegionVisible = true;
      }
      else if (_editCadTabbedPane.getSelectedIndex() == _EDIT_COMPONENTS_FIDUCIALS)    //Ngie Xing
      {
        _editCadTabbedPane.setSelectedIndex(2);  // stay on Edit Component/Fiduncial tab
        return;
      }     
    }
    else if(_editCadTabbedPane.getSelectedIndex() == _EDIT_PSH_SETTINGS)//Siew Yeng - XCR-3781
    {
      if(isReadyToFinish())
      {
        _testDev.showCadWindow();
        if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false) //Siew Yeng - XCR-3844
        {
          if(_testDev.isBoardGraphicsAlreadyDrawnInAlignment() == false)
          {
            _testDev.initGraphicsToBoard();
            _testDev.changeGraphicsToBoard(_project.getPanel().getBoards().get(0));
            _testDev.switchToBoardGraphics();
          }
        }
        else
          _testDev.switchToPanelGraphics();
        
        _editPshSettingsPanel.start();
        
        _landPatternTabIsVisible = false;
        _guiObservable.deleteObserver(this);
        editHoleLocationMenuItemSetEnabled(false);
        makePadOneMenuItemSetEnabled(false);
        
        _editPshSettingsVisible = true;
      }
      else if (_editCadTabbedPane.getSelectedIndex() == _EDIT_COMPONENTS_FIDUCIALS)    //Ngie Xing
      {
        _editCadTabbedPane.setSelectedIndex(2);  // stay on Edit Component/Fiduncial tab
        return;
      }  
    }
    else
    {
      _testDev.switchToPanelGraphics();
      _testDev.showCadWindow();
      _testDev.clearLandPatternGraphics(); // removes the LPGES as an observer of Project events
      _landPatternTabIsVisible = false;
      _guiObservable.addObserver(this);
      editHoleLocationMenuItemSetEnabled(false);
      makePadOneMenuItemSetEnabled(false);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void addComponentButton_actionPerformed(ActionEvent e)
  {
    if (_newComponentDetailsTable.getSelectedRowCount() > 0)
      _newComponentDetailsTable.clearSelection();

    AddComponentDialog addComponentDialog = new AddComponentDialog(_mainUI,
                                                                   StringLocalizer.keyToString("CAD_ADD_COMPONENT_DIALOG_TITLE_KEY"),
                                                                   true,
                                                                   _landPatterns,
                                                                   _project);
    SwingUtils.centerOnComponent(addComponentDialog, _mainUI);
    addComponentDialog.setVisible(true);

    showAndAddComponent(addComponentDialog);
  }

  /**
   * @author Laura Cormos
   */
  private void deleteComponentButton_actionPerformed()
  {
    java.util.List<String> compPackageOnPackageToBeRemovedList = new ArrayList<String>();

    if (_newComponentDetailsTable.getSelectedRow() != -1)
    {
      _shouldInteractWithGraphics = false;
      int selectedRows[] = _newComponentDetailsTable.getSelectedRows();
      com.axi.v810.business.panelDesc.Component selectedComponent = null;
      final java.util.List<ComponentType> compTypes = new ArrayList<ComponentType>();
      // since on a multi board panel one selection makes all components of the same type selected,
      // first we build a list of only the component types that were selected
      for (int i = 0; i < selectedRows.length; i++)
      {
        cancelEditingIfNecessary(_newComponentDetailsTable);

        selectedComponent = _newComponentTableModel.getComponentAt(selectedRows[i]);
        ComponentType selectedComponentType = selectedComponent.getComponentType();

        if (compTypes.contains(selectedComponentType))
          continue; // we already handled all the components of this type

        Map<Pad, AlignmentGroup> padToAlignmentGroupMap = createPadAlignmentMapIfNecessary(selectedComponentType);
        boolean componentPadIsUsedForAlignment = false;

        if (padToAlignmentGroupMap.isEmpty() == false)
          componentPadIsUsedForAlignment = true;
        if (componentPadIsUsedForAlignment)
        {
          String message = new String();
          for (Pad alignmentPad : padToAlignmentGroupMap.keySet())
          {
            // pop dialog about pad being part of an alignment group
            String alignmentGroupNames = new String();
            Set<AlignmentGroup> uniqueGroups = new HashSet<AlignmentGroup>();
            uniqueGroups.addAll(padToAlignmentGroupMap.values());
            for (AlignmentGroup alignmentGroup : uniqueGroups)
              alignmentGroupNames += alignmentGroup.getName() + ", ";
            // remove the last comma and space
            alignmentGroupNames = alignmentGroupNames.substring(0, alignmentGroupNames.length() - 2);
            message += StringLocalizer.keyToString("CAD_PAD_KEY") + " " + alignmentPad.getName() + " " +
                StringLocalizer.keyToString("CAD_OF_COMPONENT_KEY") + " " + alignmentPad.getComponent().getReferenceDesignator() + " " +
                StringLocalizer.keyToString("CAD_IS_PART_OF_ALIGNMENT_GROUP_KEY") + alignmentGroupNames + ".\n";
          }

          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString(new LocalizedString(
                                            "CAD_DELETED_COMPONENT_PAD_IS_IN_ALIGNMENT_GROUP_KEY",
                                            new Object[]{message})),
                                        StringLocalizer.keyToString("CAD_CONFIRM_ALIGNMENT_COMPONENT_PAD_DELETE_KEY"),
                                        JOptionPane.OK_OPTION);
          continue;
        }

        if (compTypes.contains(selectedComponentType) == false)
        {
          compTypes.add(selectedComponentType);
          
          if (selectedComponentType.hasCompPackageOnPackage())
          {
            compPackageOnPackageToBeRemovedList.add(selectedComponentType.getCompPackageOnPackage().getPOPName());
          }
        }
      }

      //Khaw Chek Hau - XCR3554: Package on package (PoP) development
      if (compPackageOnPackageToBeRemovedList.isEmpty() == false)
      {
        int status = JOptionPane.CANCEL_OPTION;
        status = JOptionPane.showConfirmDialog(_mainUI, 
                                            StringLocalizer.keyToString(new LocalizedString("MMGUI_DELETE_COMPONENTS_WOULD_DELETE_POP_KEY",
                                                                                             new Object[]{selectedComponent.getReferenceDesignator(), 
                                                                                                          compPackageOnPackageToBeRemovedList})),
                                            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), 
                                            JOptionPane.YES_NO_CANCEL_OPTION, 
                                            JOptionPane.QUESTION_MESSAGE);

        if(status != JOptionPane.YES_OPTION)
        {
          return;
        }   
      }
      
      // then we proceed to delete each component for each component type ending with the component type itself
//      for (ComponentType componentType : compTypes)
//      {
        // The line below should not be necessary if the component type Destroy event would contain
        // all of the destroyed component type's components
//        _newComponentTableModel.deleteComponents(componentType.getComponents());

      //Ngie Xing, add busyCancelDialog because deleting process might be slow
      final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
        StringLocalizer.keyToString("ECP_BUSYCANCELDIALOG_DELETE_COMPONENT_MESSAGE_KEY"),
        StringLocalizer.keyToString("ECP_BUSYCANCELDIALOG_DELETE_COMPONENT_TITLE_KEY"),
        true);
      busyCancelDialog.pack();
      SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
      busyCancelDialog.setCursor(new Cursor(Cursor.WAIT_CURSOR));
      SwingWorkerThread.getInstance().invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _commandManager.trackState(getCurrentUndoState());
            _commandManager.execute(new RemoveComponentTypeCommand(compTypes));//componentType)); //Ngie Xing
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          }
          finally
          {
            // wait until visible
            while (busyCancelDialog.isVisible() == false)
            {
              try
              {
                Thread.sleep(200);
              }
              catch (InterruptedException ex)
              {
                // do nothing
              }
            }
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                busyCancelDialog.dispose();
              }
            });
          }
        }
      });
      busyCancelDialog.setVisible(true);
    }
    else
    {
      JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("CAD_ITEM_SELECT_TO_DELETE_REQUEST_KEY"),
                                    StringLocalizer.keyToString("CAD_NO_SELECTION_FOUND_TITLE_KEY"),
                                    JOptionPane.INFORMATION_MESSAGE);
    }
    _shouldInteractWithGraphics = true;
  }

  /**
    * @author Laura Cormos
    */
   private void deleteFiducialButton_actionPerformed()
   {
     if (_newFiducialDetailsTable.getSelectedRow() != -1)
     {
       _shouldInteractWithGraphics = false;
       int selectedRows[] = _newFiducialDetailsTable.getSelectedRows();
       java.util.List<FiducialType> fidTypes = new ArrayList<FiducialType>();
       java.util.List<Fiducial> selectedFiducials = new ArrayList<Fiducial>();
       // first gather all the selected fiducials since the table model will be modified as deletion happens.
       for (int i = 0; i < selectedRows.length; i++)
       {
         selectedFiducials.add(_newFiducialTableModel.getFiducialAt(selectedRows[i]));
       }

       for (Fiducial selectedFiducial : selectedFiducials)
       {
         cancelEditingIfNecessary(_newFiducialDetailsTable);

         FiducialType selectedFiducialType = selectedFiducial.getFiducialType();

         if (fidTypes.contains(selectedFiducialType))
           continue; // we already handled all the board fiducials of this type

         Map<Fiducial, AlignmentGroup>
             fiducialToAlignmentGroupMap = createFiducialsAlignmentMapIfNecessary(selectedFiducial);
         boolean fiducialIsUsedForAlignment = false;

         if (fiducialToAlignmentGroupMap.isEmpty() == false)
           fiducialIsUsedForAlignment = true;
         if (fiducialIsUsedForAlignment)
         {
           // pop dialog about fiducial being part of an alignment group
           String alignmentGroupNames = new String();
           Set<AlignmentGroup> uniqueGroups = new HashSet<AlignmentGroup>();
           uniqueGroups.addAll(fiducialToAlignmentGroupMap.values());
           for (AlignmentGroup alignmentGroup : uniqueGroups)
             alignmentGroupNames += alignmentGroup.getName() + ", ";
           // remove the last comma and space
           alignmentGroupNames = alignmentGroupNames.substring(0, alignmentGroupNames.length() - 2);

           JOptionPane.showMessageDialog(_mainUI,
               StringLocalizer.keyToString(new LocalizedString("CAD_DELETED_FIDUCIAL_IS_IN_ALIGNMENT_GROUP_KEY",
               new Object[]
               {selectedFiducial.getName(), alignmentGroupNames})),
               StringLocalizer.keyToString("CAD_CONFIRM_ALIGNMENT_FIDUCIAL_DELETE_KEY"),
               JOptionPane.OK_OPTION);

           // remember that we have handled this fiducial type, move on to the next fiducial
           fidTypes.add(selectedFiducialType);
           continue;
         }

         if (selectedFiducial.isOnPanel())
         {
           try
           {
             _commandManager.trackState(getCurrentUndoState());
             _commandManager.execute(new RemoveFiducialCommand(selectedFiducial));
           }
           catch (XrayTesterException ex)
           {
             MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                           StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
             _newFiducialTableModel.fireTableRowsUpdated(_newFiducialTableModel.getRowForFiducial(selectedFiducial),
                                                         _newFiducialTableModel.getRowForFiducial(selectedFiducial));
           }
         }
         else if (selectedFiducial.isOnBoard())
         {
           try
           {
             fidTypes.add(selectedFiducialType);
             _commandManager.trackState(getCurrentUndoState());
             _commandManager.execute(new RemoveFiducialTypeCommand(selectedFiducialType));
           }
           catch (XrayTesterException ex)
           {
             MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                           StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
           }
         }
         else
         {
           Assert.expect(false, "A fiducial must belong to either a board or a panel");
         }
       }
       // finally, the events generated by the fiducials' destruction will be acted upon in the updateData() method
       // by deleting the fiducial from the table model
     }
     else
     {
       JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("CAD_ITEM_SELECT_TO_DELETE_REQUEST_KEY"),
                                     StringLocalizer.keyToString("CAD_NO_SELECTION_FOUND_TITLE_KEY"),
                                     JOptionPane.INFORMATION_MESSAGE);
     }
     _shouldInteractWithGraphics = true;
  }

  /**
   * author Laura Cormos
   */
  private void boardTypeChanged()
  {
    // update land pattern combobox and pad table
    _selectLandPatternComboBox.removeActionListener(_selectLandPatternActionListener);
    java.util.List<LandPattern> landPatternList = new ArrayList<LandPattern>();
    landPatternList = _currentBoardType.getLandPatterns();
    Assert.expect(landPatternList != null);
    _landPatterns = new ArrayList<LandPattern>(landPatternList);
    Collections.sort(_landPatterns, new LandPatternAlphaNumericComparator(true));
    populateLandPatternComboBox();
    _landPatternDetailsTableModel.clear();
    _selectLandPatternComboBox.addActionListener(_selectLandPatternActionListener);

    // update fiducials table
    updateFiducialsTableModel();

    // update components table
//    updateComponentsTableModel(); - uncomment this to show all components on the panel when a board type changes
  }

  /**
   * Task is ready to start
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("EditCadPanel().start()");

    if (_editCadTabbedPane.getSelectedIndex() == _EDIT_FOCUS_REGION) // Edit Focus Region tab
    { 
        _editFocusRegionPanel.start();
    }
    
    com.axi.util.TimerUtil screenTimer = new com.axi.util.TimerUtil();
    screenTimer.start();
   
    // Bee Hoon - reset the selectLandPatternComboBox flag to false
    isUndoLandPattern = false;
    
    // generate test program if needed; notify user it is happening
    if (_project.isTestProgramValid() == false)
    {
      _mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_TESTABILITY_NEEDS_TEST_PROGRAM_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_TDT_TITLE_EDIT_CAD_KEY"),
                                                    _project);
      _populateProjectData = true;
    }
    
    if (_populateProjectData)
    {
      populateWithProjectData();
    }
    else
    {
      _newComponentTableModel.clearData();
      _newComponentTableModel.setProject(_project);
    }
    _populateProjectData = false;

    if (_landPatternTabIsVisible)
    {
      LandPattern lp = (LandPattern)_selectLandPatternComboBox.getSelectedItem();
      if (lp == null)
        lp = (LandPattern)_selectLandPatternComboBox.getItemAt(1);

      setupAndDisplayLandPattern(lp);
    }
    
    // set up for custom menus and tool bar
    super.startMenusAndToolBar();
    _testDev.addBoardSelectInToolbar(_envToolBarRequesterID);
    _active = true;

    // add custom menus
    JMenu editMenu = _menuBar.getEditMenu();
    _menuBar.addMenuItem(_menuRequesterID, editMenu, _editHoleLocationMenuItem);
    _menuBar.addMenuItem(_menuRequesterID, editMenu, _makePadOneMenuItem);

    // add custom toolbar components

    _testDev.showCadWindow();

    updateCadGraphics();

    // add EditCadPanel as an observer of the graphics engine for mouse clicks and selected renderers

    if (_panelGraphicsSelectedRendererObservable == null)
    {
      _panelGraphicsSelectedRendererObservable = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getSelectedRendererObservable();
    }
    if (_panelGraphicsMouseClickObservable == null)
    {
      _panelGraphicsMouseClickObservable = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getMouseClickObservable();
    }
    if (_landPatternGraphicsSelectedRendererObservable == null)
    {
      _landPatternGraphicsSelectedRendererObservable = _testDev.getLandPatternGraphicsEngineSetup().getGraphicsEngine().getSelectedRendererObservable();
    }
    if (_landPatternGraphicsMouseClickObservable == null)
    {
      _landPatternGraphicsMouseClickObservable = _testDev.getLandPatternGraphicsEngineSetup().getGraphicsEngine().getMouseClickObservable();
    }

    _panelGraphicsSelectedRendererObservable.addObserver(this);
    _panelGraphicsMouseClickObservable.addObserver(this);
    _landPatternGraphicsSelectedRendererObservable.addObserver(this);
    _landPatternGraphicsMouseClickObservable.addObserver(this);
    _commandManager.addObserver(this);    
    _guiObservable.addObserver(this);
    
//    _imagePanel.displayVerificationImagePanel();
    if (_updateData)
    {
      updateProjectData();
    }
    _updateData = false;
    _guiObservable.stateChanged(null, GraphicsControlEnum.ORIGIN_VISIBLE);

    _selectLandPatternComboBox.addActionListener(_selectLandPatternActionListener);
    if (_verifyCadTabIsVisible)
    {
      _verifyCadPanel.start(_imagePanel);
    }
    //Siew Yeng - XCR-3781
    if (_editPshSettingsVisible)
    {
      _editPshSettingsPanel.start();
    }
    restorePersistSettings();
    
//    // Bee Hoon - reset the selectLandPatternComboBox flag to false
//    isUndoLandPattern = false;
    screenTimer.stop();
//    System.out.println("  Total Loading for Adjust CAD: (in mils): "+ screenTimer.getElapsedTimeInMillis());
  }

  /**
   * Can the environment be swapped out
   * @return true if environment can be swapped
   * @author George Booth
   * @author Laura Cormos
   */
  public boolean isReadyToFinish()
  {
//    System.out.println("EditCadPanel().isReadyToFinish()");
    if (_verifyCadTabIsVisible)
    {
      return _verifyCadPanel.isReadyToFinish();
    }
    else if(_editPshSettingsVisible)//Siew Yeng - XCR-3781
    {
      return _editPshSettingsPanel.isReadyToFinish();
    }
    // for the edit component/fiducial tab, check that panel is in legal state before we're ready to move on.
    else if (_landPatternTabIsVisible == false)
    {
      if (_testDev.areClosingProject())
        return true;
      if (_mainUI.isPanelInLegalState() == false)
        return false;
      else
      {
        if (_landPatternDetailsTable.isEditing())
          _landPatternDetailsTable.getCellEditor().stopCellEditing();
        return true;
      }
    }
    else
    {
      if (_newComponentDetailsTable.isEditing())
        _newComponentDetailsTable.getCellEditor().stopCellEditing();
      if (_newFiducialDetailsTable.isEditing())
        _newFiducialDetailsTable.getCellEditor().stopCellEditing();
      return true;
    }
  }

  /**
   * The environment is about to be swapped out and should perform cleanup
   * @author George Booth
   */
  public void finish()
  {
    if (_verifyCadTabIsVisible)
    {
      _verifyCadPanel.finish();
    }
    if (_verifyCadPanel.isCurrentComponentValid())
    {
      ComponentType currentComponent = _verifyCadPanel.getCurrentComponent();
      _testDev.setActiveVerifyCadComponentType(currentComponent);
      _verifyCadPanel.resetCurrentComponent();
    }
    
    if(_editFocusRegionPanel.isStarted())
    {
       _editFocusRegionPanel.finish();
    }

	//Siew Yeng - XCR-3781
    if (_editPshSettingsVisible)
    {
      _editPshSettingsPanel.finish();
    }
    
    _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().clearHighlightedRenderers();
    
    cancelEditingIfNecessary(_landPatternDetailsTable);
    
    _imagePanel.finishVerificationImagePanel();
    _imagePanel.finishVerificationImagePanelForHighMag();
    _popBaseComponent = null;

//    System.out.println("EditCadPanel().finish()");
    savePersistSettings();

    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();

    _guiObservable.stateChanged(null, GraphicsControlEnum.ORIGIN_INVISIBLE);
    // user changed screens to another area of the TDE, therefore we are going to unregister our graphics observers
    _panelGraphicsSelectedRendererObservable.deleteObserver(this);
    _panelGraphicsMouseClickObservable.deleteObserver(this);
    _landPatternGraphicsSelectedRendererObservable.deleteObserver(this);
    _landPatternGraphicsMouseClickObservable.deleteObserver(this);
    _selectLandPatternComboBox.removeActionListener(_selectLandPatternActionListener);
    _guiObservable.deleteObserver(this);
    _commandManager.deleteObserver(this);
    // this call will delete the LPGES as an observer of project events which is what we want when we're moving
    // to another task in the development environment
    _testDev.clearLandPatternGraphics();
    _active = false;
  }

  /**
   * @author Laura Cormos
   */
  private void savePersistSettings()
  {
    if (_uiPersistSettings != null)
      _uiPersistSettings.setComponentsTabDividerLocation(_compFiducialSplitPane.getDividerLocation());
  }

  /**
   * @author Laura Cormos
   */
  private void restorePersistSettings()
  {
    int dividerLocation = _uiPersistSettings.getComponentsTabDividerLocation();
    if (dividerLocation > 0)
      _compFiducialSplitPane.setDividerLocation(dividerLocation);
  }


  /**
   * @author Laura Cormos
   */
  private void updateNumericDecimalPlaces()
  {
    _enumSelectedUnits = _project.getDisplayUnits();
    cancelEditingIfNecessary(_newFiducialDetailsTable);
    _newFiducialDetailsTable.setDecimalPlacesToDisplay(_enumSelectedUnits);
    _newFiducialTableModel.fireTableDataChanged();
    cancelEditingIfNecessary(_newComponentDetailsTable);
    _newComponentDetailsTable.setDecimalPlacesToDisplay(MeasurementUnits.getMeasurementUnitDecimalPlaces(_enumSelectedUnits));
    _newComponentTableModel.fireTableDataChanged();
    cancelEditingIfNecessary(_landPatternDetailsTable);
    _landPatternDetailsTable.setDecimalPlacesToDisplay(_enumSelectedUnits);
    _landPatternDetailsTableModel.fireTableDataChanged();
  }

  /**
   * @author Laura Cormos
   */
  public UndoState getCurrentUndoState()
  {
    UndoState undoState = new UndoState();
    if (_landPatternTabIsVisible)
    {
      undoState.setLandPatternComboBoxIndex(_selectLandPatternComboBox.getSelectedIndex());
      if (_selectLandPatternComboBox.getSelectedItem() instanceof LandPattern)
      {
        isUndoLandPattern = true;
        undoState.setLandPatternComboBoxName(((LandPattern)_selectLandPatternComboBox.getSelectedItem()).getName());
      }
      undoState.setAdjustCadTabbedPaneIndex(1);
    }
    else if (_verifyCadTabIsVisible)
      undoState.setAdjustCadTabbedPaneIndex(0);
    else
      undoState.setAdjustCadTabbedPaneIndex(2);

    return undoState;
  }

  /**
   * @author Laura Cormos
   */
  private Map<Fiducial, AlignmentGroup> createFiducialsAlignmentMapIfNecessary(Fiducial selectedFiducial)
  {
    Assert.expect(selectedFiducial != null);

    Map<Fiducial, AlignmentGroup> fiducialToAlignmentGroupMap =  new HashMap<Fiducial,AlignmentGroup>();
    PanelSettings panelSettings = _project.getPanel().getPanelSettings();
    java.util.List<AlignmentGroup> alignmentGroups = panelSettings.getAllAlignmentGroups();
    java.util.List<Fiducial> fiducials = selectedFiducial.getFiducialType().getFiducials();
    for (AlignmentGroup alignmentGroup : alignmentGroups)
    {
      for (Fiducial fiducial : fiducials)
      {
        if (alignmentGroup.getFiducials().contains(fiducial))
        {
          fiducialToAlignmentGroupMap.put(fiducial, alignmentGroup);
        }
      }
    }
    return fiducialToAlignmentGroupMap;
  }

  /**
   * @author Laura Cormos
   */
  private Map<Pad, AlignmentGroup> createPadAlignmentMapIfNecessary(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    Map<Pad, AlignmentGroup> padToAlignmentGroupMap =  new HashMap<Pad,AlignmentGroup>();
    PanelSettings panelSettings = _project.getPanel().getPanelSettings();
    if(panelSettings.isPanelBasedAlignment())
    {
      java.util.List<AlignmentGroup> alignmentGroups = panelSettings.getAllAlignmentGroups();
      java.util.List <com.axi.v810.business.panelDesc.Component> components = componentType.getComponents();
      for (AlignmentGroup alignmentGroup : alignmentGroups)
      {
        for (com.axi.v810.business.panelDesc.Component component : components)
        {
          for (Pad pad : component.getPads())
          {
            if (alignmentGroup.getPads().contains(pad))
            {
              padToAlignmentGroupMap.put(pad, alignmentGroup);
            }
          }
        }
      }
    }
    else
    {
      // Wei Chin added for individual alignment
      for(Board board :_project.getPanel().getBoards())
      {
        java.util.List<AlignmentGroup> alignmentGroups = board.getBoardSettings().getAllAlignmentGroups();
        java.util.List <com.axi.v810.business.panelDesc.Component> components = componentType.getComponents();
        for (AlignmentGroup alignmentGroup : alignmentGroups)
        {
          for (com.axi.v810.business.panelDesc.Component component : components)
          {
            for (Pad pad : component.getPads())
            {
              if (alignmentGroup.getPads().contains(pad))
              {
                padToAlignmentGroupMap.put(pad, alignmentGroup);
              }
            }
          }
        }
      }
    }
    return padToAlignmentGroupMap;

  }

  /**
   * @author Laura Cormos
   */
  private void cancelEditingIfNecessary(JTable table)
  {
    // cancel cell editing in case user deletes the row while editing is still going on.
    if (table.isEditing())
      table.getCellEditor().cancelCellEditing();
    _commandManager.endCommandBlockIfNecessary();
//    int column = table.getEditingColumn();
//    if (column > -1)
//    {
//      TableCellEditor cellEditor = table.getColumnModel().getColumn(column).getCellEditor();
//      Assert.expect(cellEditor != null);
//      cellEditor.cancelCellEditing();
//    }
  }

  /**
   * This method implements the right click edit when multiple lines of a table are selected
   * @author Andy Mechtenberg
   */
  private void multiEditTableMouseReleased(MouseEvent e, JTable table)
  {
    Assert.expect(e != null);
    Assert.expect(table != null);

    // ignore any other mouse events than a popup trigger -- we don't want to break the normal L&F
    if (e.isPopupTrigger())
    {
      // get the table cell that the mouse was right-clicked in
      Point p = e.getPoint();
      int row = table.rowAtPoint(p);
      int col = table.columnAtPoint(p);

      // if this cell is not currently selected, exit.  It's confusing to edit a cell that not selected.
      // we could "select" this cell too, but for now, we'll just not.
      if (table.isCellSelected(row, col) == false)
        return;

      // start the editing process for this cell
      table.editCellAt(row, col);
      // if this is a combo box cell editor, the editCellAt doesn't show the combo box popup, so let's show that
      // only if this is a combo box editor
      Object obj = table.getEditorComponent();
      if (obj instanceof JComboBox)
      {
        JComboBox comboBox = (JComboBox)obj;
        comboBox.showPopup();
      }
      else if (obj instanceof NumericTextField)
      {
        NumericTextField textField = (NumericTextField)obj;
        textField.requestFocus();
      }
    }
  }

  /**
   * Disable add/delete pad buttons for landpatterns used by 2-pin devices
   * @author Laura Cormos
   */
  void setGraphicsControlsFor2PinDevice(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);

    if (landPattern.getLandPatternPads().size() == 2)
    {
      java.util.List<ComponentType> compTypesForLP = landPattern.getComponentTypes();
      for (ComponentType ct : compTypesForLP)
      {
        java.util.List<JointTypeEnum> jointTypeEnumsForCompType = ct.getJointTypeEnums();
        for (JointTypeEnum jte : jointTypeEnumsForCompType)
        {
          if (ImageAnalysis.isSpecialTwoPinComponent(jte))
          {
            // first disable the add/delete pad buttons
            _addPadButton.setEnabled(false);
            _addPadArrayButton.setEnabled(false);
            _deletePadButton.setEnabled(false);
            // next remove the 90 and 270 degrees options from the rotation editor
            _landPatternDetailsTable.setRotationEditorFor2PinDevice();
            return;
          }
        }
      }
    }
    Set<Double> customRotationValues = new HashSet<Double>();
    for (LandPattern lp : _landPatterns)
    {
      for (LandPatternPad lpPad : lp.getLandPatternPads())
      {
        customRotationValues.add(lpPad.getDegreesRotation());
      }
    }
    _landPatternDetailsTable.setRotationEditorForNon2PinDevices(customRotationValues);
    _addPadButton.setEnabled(true);
    _addPadArrayButton.setEnabled(true);
    _deletePadButton.setEnabled(true);
  }

  /**
   * @author Laura Cormos
   */
  void setDeletePadButtonState(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);

    int numPads = landPattern.getLandPatternPads().size();
    if (numPads == 1)
      _deletePadButton.setEnabled(false);
    else
      _deletePadButton.setEnabled(true);
  }

  /**
   * @author Laura Cormos
   */
  private void showAndAddComponent(AddComponentDialog addComponentDialog)
  {
    Assert.expect(addComponentDialog != null);

    if (addComponentDialog.userClickedOk())
    {
      String refDes = addComponentDialog.getReferenceDesignator();
      if (refDes.length() > 0)
      {
        BoardType boardType = addComponentDialog.getBoardType();

        if (boardType.isComponentTypeReferenceDesignatorValid(refDes) == false)
        {
          // pop dialog about invalid name
          String illegalChars = boardType.getComponentTypeReferenceDesignatorIllegalChars();
          LocalizedString message = null;
          if (illegalChars.equals(" "))
            message = new LocalizedString("CAD_INVALID_COMPONENT_NAME_SPACES_KEY", new Object[]
                                          {refDes});
          else
            message = new LocalizedString("CAD_INVALID_COMPONENT_NAME_OTHER_CHARS_KEY", new Object[]
                                          {refDes, illegalChars});
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString(message),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
          return;
        }
        else if (boardType.isComponentTypeReferenceDesignatorDuplicate(refDes))
        {
          // pop dialog about duplicate name
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
              new Object[]
              {refDes})),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
          return;
        }
        else
        {
          LandPattern landPattern = (LandPattern)addComponentDialog.getLandPattern();
          BoardCoordinate newCoordinate = addComponentDialog.getBoardCoordinate();
          double rotation = addComponentDialog.getRotation();
          String boardSide = addComponentDialog.getBoardSide();
          SideBoardType sideBoardType;

          if (boardSide.equals(StringLocalizer.keyToString("CAD_SIDE1_KEY")))
          {
            if (boardType.sideBoardType1Exists() == false)
              boardType.getBoards().get(0).createSecondSideBoard();
            sideBoardType = boardType.getSideBoardType1();
          }
          else
          {
            if (boardType.sideBoardType2Exists() == false)
              boardType.getBoards().get(0).createSecondSideBoard();
            sideBoardType = boardType.getSideBoardType2();
          }

          // add new rotation to custom values, if needed
          _newComponentDetailsTable.addEditorCustomValue(new Double(rotation));

          try
          {
            _commandManager.trackState(getCurrentUndoState());
            _commandManager.execute(new CreateComponentTypeCommand(sideBoardType, refDes, landPattern, newCoordinate, rotation));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          }
        }
      }
    }
    addComponentDialog.clear();
  }

  /**
   * @author Laura Cormos
   */
  private void addFiducialButton_actionPerformed()
  {
    if (_newFiducialDetailsTable.getSelectedRowCount() > 0)
      _newFiducialDetailsTable.clearSelection();

    AddFiducialDialog addFiducialDialog = new AddFiducialDialog(_mainUI,
                                                                StringLocalizer.keyToString("CAD_ADD_FIDUCIAL_DIALOG_TITLE_KEY"),
                                                                true,
                                                                _project);
    SwingUtils.centerOnComponent(addFiducialDialog, _mainUI);
    addFiducialDialog.setVisible(true);

    if (addFiducialDialog.userClickedOk())
    {
      if (addFiducialDialog.getFiducialType().equals(StringLocalizer.keyToString("AFD_BOARD_TYPE_KEY")))
        showAndAddBoardFiducial(addFiducialDialog);
      else
        showAndAddPanelFiducial(addFiducialDialog);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void showAndAddBoardFiducial(AddFiducialDialog addFiducialDialog)
  {
    Assert.expect(addFiducialDialog != null);

    if (addFiducialDialog.userClickedOk())
    {
      String fidName = addFiducialDialog.getFiducialName();
      if (fidName.length() > 0)
      {
        BoardType boardType = addFiducialDialog.getBoardType();

        if (boardType.isFiducialTypeNameValid(fidName) == false)
        {
          String illegalChars = boardType.getFiducialTypeNameIllegalChars();
          LocalizedString message = null;
          if (illegalChars.equals(" "))
            message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_SPACES_KEY",
                                          new Object[]{fidName});
          else
            message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_OTHER_CHARS_KEY",
                                          new Object[]{fidName, illegalChars});

          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString(message),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
          return;
        }
        else if (boardType.isFiducialTypeNameDuplicate(fidName))
        {
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
                                                                                        new Object[]{fidName})),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
          return;
        }

        else
        {
          SideBoardType sideBoardType;
          if (addFiducialDialog.getBoardSide().equals(StringLocalizer.keyToString("CAD_SIDE1_KEY")))
            sideBoardType = boardType.getSideBoardType1();
          else
            sideBoardType = boardType.getSideBoardType2();

          int xDimInNanos = addFiducialDialog.getXDimensionInNanos();
          int yDimInNanos = addFiducialDialog.getYDimensionInNanos();
          Assert.expect(xDimInNanos > 0);
          Assert.expect(yDimInNanos > 0);
          BoardCoordinate coordinate = addFiducialDialog.getBoardCoordinate();
          try
          {
            CreateFiducialTypeCommand createFiducialTypeCommand = new CreateFiducialTypeCommand(sideBoardType, fidName, coordinate);
            _commandManager.trackState(getCurrentUndoState());
            _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_CREATE_FIDUCIAL_TYPE_KEY"));
            _commandManager.execute(createFiducialTypeCommand);
            FiducialType newFiducialType = createFiducialTypeCommand.getFiducialType();
            _commandManager.execute(new FiducialTypeSetWidthInNanoMetersCommand(newFiducialType, xDimInNanos));
            _commandManager.execute(new FiducialTypeSetLengthInNanoMetersCommand(newFiducialType, yDimInNanos));
            _commandManager.endCommandBlock();
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          }
        }
      }
    }
    addFiducialDialog.clear();
  }

  /**
   * @author Laura Cormos
   */
  private void showAndAddPanelFiducial(AddFiducialDialog addFiducialDialog)
  {
    if (addFiducialDialog.userClickedOk())
    {
      String fidName = addFiducialDialog.getFiducialName();
      if (fidName != null) // null means the dialog was canceled
      {
        if (_project.getPanel().isFiducialNameValid(fidName) == false)
        {
          String illegalChars = _project.getPanel().getFiducialNameIllegalChars();
          LocalizedString message = null;
          if (illegalChars.equals(" "))
            message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_SPACES_KEY", new Object[]{fidName});
          else
            message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_OTHER_CHARS_KEY", new Object[]{fidName, illegalChars});

          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString(message),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
          return;
        }
        else if (_project.getPanel().isFiducialNameDuplicate(fidName))
        {
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
                                                                                        new Object[]{fidName})),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.INFORMATION_MESSAGE);
          return;
        }
        else
        {
          int xDimInNanos = addFiducialDialog.getXDimensionInNanos();
          int yDimInNanos = addFiducialDialog.getYDimensionInNanos();
          Assert.expect(xDimInNanos > 0);
          Assert.expect(yDimInNanos > 0);
          BoardCoordinate coordinate = addFiducialDialog.getBoardCoordinate();
          boolean panelTop = false;

          if (addFiducialDialog.getBoardSide().equals(StringLocalizer.keyToString("CAD_SIDE1_KEY")))
            panelTop = true;

          // createFiducial method will set a default length, width and shape
          CreateFiducialCommand createFiducialCommand = new CreateFiducialCommand(_project.getPanel(), fidName,
              panelTop, new PanelCoordinate(coordinate));
          try
          {
            _commandManager.trackState(getCurrentUndoState());
            _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_CREATE_FIDUCIAL_KEY"));
            _commandManager.execute(createFiducialCommand);
            Fiducial newFiducial = createFiducialCommand.getFiducial();
            _commandManager.execute(new FiducialTypeSetWidthInNanoMetersCommand(newFiducial.getFiducialType(), xDimInNanos));
            _commandManager.execute(new FiducialTypeSetLengthInNanoMetersCommand(newFiducial.getFiducialType(), yDimInNanos));
            _commandManager.endCommandBlock();
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          }
        }
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void setupAndDisplayLandPattern(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);

    _selectLandPatternComboBox.setSelectedItem(landPattern);
    _landPatternDetailsTableModel.populateWithLandPattern(landPattern);
    _deleteUnusedLandPatternButton.setEnabled(true);
    _copyToButton.setEnabled(true);
    setGraphicsControlsFor2PinDevice(landPattern);
    setDeletePadButtonState(landPattern);

    if (_landPatternTabIsVisible && isUndoLandPattern == false)
      _testDev.changeGraphicsToLandPattern(landPattern);
  }

  /**
   * @author Laura Cormos
   */
  private void updateTableEditorsWithDisplayUnits()
  {
    _landPatternDetailsTable.updateEditorWithDisplayUnits(_project.getDisplayUnits());
  }

  /**
   * @author Laura Cormos
   */
  private void makePadOneMenuItemSetEnabled(boolean enabled)
  {
    _makePadOneMenuItem.setEnabled(enabled);
  }

  /**
  * @author Laura Cormos
  */
 private void editHoleLocationMenuItemSetEnabled(boolean enabled)
 {
   _editHoleLocationMenuItem.setEnabled(enabled);
 }

 /**
  * @author Laura Cormos
  */
 private void pad_keyTyped(KeyEvent e)
 {
   if (e.getKeyCode() == KeyEvent.VK_DELETE)
     deletePadButton_actionPerformed();
 }

 /**
  * @author Laura Cormos
  */
 private void component_keyTyped(KeyEvent e)
 {
   if (e.getKeyCode() == KeyEvent.VK_DELETE)
     deleteComponentButton_actionPerformed();
 }

 /**
  * @author Laura Cormos
  */
 private void fiducial_keyTyped(KeyEvent e)
 {
   if (e.getKeyCode() == KeyEvent.VK_DELETE)
     deleteFiducialButton_actionPerformed();
 }
 
  /**
   * @author weng-jian.eoh
   * 
   * XCR-3589 Combo STD and XXL software GUI
   * 
   * remove all observer to reinitialize whole panel to support different magnification
   */
 public void removeAllObservable()
 {
    // user changed screens to another area of the TDE, therefore we are going to unregister our graphics observers
    _guiObservable.deleteObserver(this);
    _commandManager.deleteObserver(this);
 }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  private void addNewPOPRightClick_actionPerformed(ActionEvent e)
  {
    java.util.List<ComponentType> compPackageOnPackageComponentTypes = new ArrayList<ComponentType>();
    
    if (_popBaseComponent.getComponentType().hasCompPackageOnPackage())
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATED_POP_ERROR_MESSAGE_KEY", 
                                                                 new Object[]{_popBaseComponent.getComponentType(), 
                                                                  _popBaseComponent.getComponentType().getCompPackageOnPackage().getPOPName()})),
                                    StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
                                    true);
      return;
    }
    
    AddPOPLayerNumberDialog dlg = new AddPOPLayerNumberDialog(_mainUI, _project);
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    dlg.clear();
    dlg.dispose();

    if (dlg.isOkButtonPerformed())
    {        
      if (_newComponentDetailsTable.getSelectedRowCount() > 0)
        _newComponentDetailsTable.clearSelection();

      try
      {
        HashMap<ComponentType, POPLayerIdEnum> componentTypeToPOPLayerIdEnumMap = new HashMap<ComponentType, POPLayerIdEnum>();
        HashMap<ComponentType, Integer> componentTypeToPOPZHeightInNanometersMap = new HashMap<ComponentType, Integer>();
        int previousLayerPOPZHeight = 0;
        
        _commandManager.trackState(getCurrentUndoState());
        _commandManager.beginCommandBlock(StringLocalizer.keyToString("CAD_ADD_POP_KEY"));

        ProjectObservable.getInstance().setEnabled(false);
        try
        {
          for (int popLayerNumber = 1; popLayerNumber <= dlg.getPOPLayerNumber(); popLayerNumber++)
          {        
            AddPackageOnPackageDialog addPackageOnPackageDialog = new AddPackageOnPackageDialog(_mainUI,
                                                                                                StringLocalizer.keyToString("CAD_ADD_COMPONENT_DIALOG_TITLE_KEY"),
                                                                                                true,
                                                                                                _landPatterns,
                                                                                                _project,
                                                                                                _popBaseComponent,
                                                                                                popLayerNumber,
                                                                                                previousLayerPOPZHeight);

            SwingUtils.centerOnComponent(addPackageOnPackageDialog, _mainUI);
            addPackageOnPackageDialog.setVisible(true);

            if (addPackageOnPackageDialog.userClickedOk())
            {
              String refDes = addPackageOnPackageDialog.getReferenceDesignator();
              if (refDes.length() > 0)
              {
                BoardType boardType = addPackageOnPackageDialog.getBoardType();

                if (boardType.isComponentTypeReferenceDesignatorValid(refDes) == false)
                {
                  // pop dialog about invalid name
                  String illegalChars = boardType.getComponentTypeReferenceDesignatorIllegalChars();
                  LocalizedString message = null;
                  if (illegalChars.equals(" "))
                    message = new LocalizedString("CAD_INVALID_COMPONENT_NAME_SPACES_KEY", new Object[]
                                                  {refDes});
                  else
                    message = new LocalizedString("CAD_INVALID_COMPONENT_NAME_OTHER_CHARS_KEY", new Object[]
                                                  {refDes, illegalChars});
                  JOptionPane.showMessageDialog(_mainUI,
                                                StringLocalizer.keyToString(message),
                                                StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                JOptionPane.INFORMATION_MESSAGE);
                  return;
                }
                else if (boardType.isComponentTypeReferenceDesignatorDuplicate(refDes))
                {
                  // pop dialog about duplicate name
                  JOptionPane.showMessageDialog(_mainUI,
                                                StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
                      new Object[]
                      {refDes})),
                                                StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                JOptionPane.INFORMATION_MESSAGE);
                  return;
                }
                else 
                {
                  LandPattern landPattern = (LandPattern)addPackageOnPackageDialog.getLandPattern();
                  BoardCoordinate newCoordinate = addPackageOnPackageDialog.getBoardCoordinate();
                  double rotation = addPackageOnPackageDialog.getRotation();
                  String boardSide = addPackageOnPackageDialog.getBoardSide();
                  SideBoardType sideBoardType;
                  int zHeightInNanometers = addPackageOnPackageDialog.getZHeight();
                  int layerNumber = addPackageOnPackageDialog.getLayerNumber();

                  if (boardSide.equals(StringLocalizer.keyToString("CAD_SIDE1_KEY")))
                  {
                    if (boardType.sideBoardType1Exists() == false)
                      boardType.getBoards().get(0).createSecondSideBoard();
                    sideBoardType = boardType.getSideBoardType1();
                  }
                  else
                  {
                    if (boardType.sideBoardType2Exists() == false)
                      boardType.getBoards().get(0).createSecondSideBoard();
                    sideBoardType = boardType.getSideBoardType2();
                  }

                  // add new rotation to custom values, if needed
                  _newComponentDetailsTable.addEditorCustomValue(new Double(rotation));

                  try
                  {
                    _commandManager.execute(new CreateComponentTypeCommand(sideBoardType, refDes, landPattern, newCoordinate, rotation));
                  }
                  catch (XrayTesterException ex)
                  {
                    MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
                  }

                  ComponentType componentType = sideBoardType.getComponentType(refDes);

                  componentTypeToPOPLayerIdEnumMap.put(componentType, POPLayerIdEnum.getLayerIdToEnumMapValue(layerNumber));
                  componentTypeToPOPZHeightInNanometersMap.put(componentType, zHeightInNanometers);
                  previousLayerPOPZHeight = zHeightInNanometers;
                  compPackageOnPackageComponentTypes.add(componentType);
                }
              }
            }      

            if (addPackageOnPackageDialog.userClickedOk() == false)
            {            
              _commandManager.execute(new RemoveComponentTypeCommand(compPackageOnPackageComponentTypes));  
              compPackageOnPackageComponentTypes.clear();
              break;
            }
            addPackageOnPackageDialog.clear();
            addPackageOnPackageDialog.dispose();
          }
        }
        finally
        {
          ProjectObservable.getInstance().setEnabled(true);
        }
        
        if (compPackageOnPackageComponentTypes.isEmpty() == false)
          ProjectObservable.getInstance().stateChanged(compPackageOnPackageComponentTypes.get(0), null, (Object) compPackageOnPackageComponentTypes, ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST);

        if (compPackageOnPackageComponentTypes.isEmpty() == false)
        {
          java.util.List<ComponentType> popTotalComponentTypes = new ArrayList<ComponentType>();
          popTotalComponentTypes.addAll(compPackageOnPackageComponentTypes);
          CompPackageOnPackage compPackageOnPackage = new CompPackageOnPackage();
          compPackageOnPackage.setPanel(_project.getPanel());
          compPackageOnPackage.setName(_popBaseComponent.getReferenceDesignator());
          componentTypeToPOPLayerIdEnumMap.put(_popBaseComponent.getComponentType(), POPLayerIdEnum.BASE);
          componentTypeToPOPZHeightInNanometersMap.put(_popBaseComponent.getComponentType(), 0);
          popTotalComponentTypes.add(_popBaseComponent.getComponentType());

          _commandManager.execute(new CreateCompPackageOnPackageCommand(_popBaseComponent.getComponentType().getSideBoardType(),
                                                                        _popBaseComponent.getComponentType().getReferenceDesignator(),
                                                                        compPackageOnPackage,
                                                                        popTotalComponentTypes,
                                                                        componentTypeToPOPLayerIdEnumMap,
                                                                        componentTypeToPOPZHeightInNanometersMap));

          java.util.List<Subtype> subtypesInLowestIL = new ArrayList<Subtype>(); 
          boolean isComponentTypeInLowestIL = false;

          for (ComponentType compType: popTotalComponentTypes)
          {        
            SignalCompensationEnum signalComp;

            for(Subtype subtype : compType.getSubtypes())
            {
              signalComp = subtype.getSubtypeAdvanceSettings().getSignalCompensation();

              if (signalComp == SignalCompensationEnum.DEFAULT_LOW)  
              {
                if (subtypesInLowestIL.contains(subtype) == false)
                  subtypesInLowestIL.add(subtype);

                isComponentTypeInLowestIL = true;
              }
            }
          }

          if (isComponentTypeInLowestIL)
          {
            int response = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                                                          StringLocalizer.keyToString(new LocalizedString("CAD_POP_SUBTYPE_RECOMMENDED_IL2_AND_ABOVE_MESSAGE_KEY", 
                                                                                      new Object[]{subtypesInLowestIL.toString()})),
                                                          StringLocalizer.keyToString("CAD_POP_SUBTYPE_MUST_IL2_AND_ABOVE_TITLE_KEY"),
                                                          JOptionPane.YES_NO_OPTION);
            if (response == JOptionPane.YES_OPTION)
            {
              for (Subtype subtype: subtypesInLowestIL)
              {     
                subtype.getSubtypeAdvanceSettings().setSignalCompensation(SignalCompensationEnum.MEDIUM);
              }
            }
          }

        }
        _commandManager.endCommandBlock();
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
      }
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  private JMenuItem addNewPOPForCompMenuItem(final com.axi.v810.business.panelDesc.Component component)
  {
    Assert.expect(component != null);

    JMenuItem addNewPOPForCompMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("CAD_ADD_POP_COMPONENT_KEY",
                                                                                    new Object[]{component.getReferenceDesignator()})));
    _popBaseComponent = component;
            
    addNewPOPForCompMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addNewPOPRightClick_actionPerformed(e);
      }
    });
    return addNewPOPForCompMenuItem;
  }
}
