package com.axi.v810.gui.testDev;

import java.util.List;
import java.util.ArrayList;

import org.jfree.chart.renderer.xy.*;

import com.axi.v810.hardware.StagePosition;
import com.axi.v810.util.StringLocalizer;

import org.jfree.chart.axis.NumberAxis;


/**
 * This class is used to generate a JFreeChart plot that will display a X-Plot
/**
 *
 * @author chin-seong.kee
 */
class ScanPathPointEntryXPlot extends ScanPathPointXYPlot
{
  private List<StagePosition> _stagePositionEntries;

  private XYItemRenderer _xPlotRenderer;
  
  private ScanPathPointEntryXPlotDataSet _dataSet; 
  
  // add by Khang Wah, 2013-12-13, to fix the scale of graph for every scan path
  private static int _maxX = 0;
  private static int _minX = 0;
  private static int _maxY = 0;
  private static int _minY = 0;

  /**
   * @author Kee Chin Seong
   */
  ScanPathPointEntryXPlot()
  {
    this(null);
  }

  /**
   * @author Kee Chin Seong
   */
  ScanPathPointEntryXPlot(List<StagePosition> stagePositionEntries)
  {
    _stagePositionEntries = stagePositionEntries;
    
    drawPlot();
  }
  
  ScanPathPointEntryXPlot(List<StagePosition> stagePositionEntries, int seqNumber)
  {
    _stagePositionEntries = stagePositionEntries;
    
    drawPlot(seqNumber);
  }
  
  /**
   * Returns our title so it can be displayed in JFreeChart.
   * @author Kee Chin Seong
   */
  String getTitle()
  {
    String title;
    String chartType = "X Plot";

    // Check to see if we have real data.  If not, display "Empty Chart" as title.
    if (_dataSet.getSeriesCount() > 0 && _dataSet.getItemCount(0) > 0)
      title = "Scan Path" + " " + chartType;
    else
      title = StringLocalizer.keyToString("RMGUI_CHART_EMPTY_CHART_TITLE_KEY") + " " + chartType;

    return title;
  }
  
   /**
   * Customize our plot.
   * @author Kee Chin Seong
   */
  private void drawPlot(int seqNumber)
  {
    _dataSet = new ScanPathPointEntryXPlotDataSet(_stagePositionEntries, seqNumber);
    
    //for(int dataSetCounter = 0 ; dataSetCounter < _dataSet.size() ; dataSetCounter ++)
    setDataset(_dataSet);
    
    _xPlotRenderer = new XYLineAndShapeRenderer(true, true);
    _xPlotRenderer.setToolTipGenerator(null);
    setRenderer(_xPlotRenderer);

    // Set up plot axis.
    NumberAxis xAxis = new NumberAxis(StringLocalizer.keyToString("RMGUI_CHART_X_PLOT_X_AXIS_LABEL_KEY"));
    xAxis.setAutoRangeIncludesZero(false);
    xAxis.setRange(_minX-10000000, _maxX+10000000); // add by Khang Wah, 2013-12-13, to fix the scale of graph for every scan path
    setDomainAxis(xAxis);

    String yAxisLabel = StringLocalizer.keyToString("RMGUI_CHART_X_PLOT_Y_AXIS_LABEL_KEY");
    NumberAxis yAxis = new NumberAxis(yAxisLabel);
    yAxis.setAutoRangeIncludesZero(false); 
    yAxis.setRange(_minY-10000000, _maxY+10000000); // add by Khang Wah, 2013-12-13, to fix the scale of graph for every scan path
    setRangeAxis(yAxis);
  }

  /**
   * Customize our plot.
   * @author Kee Chin Seong
   */
  private void drawPlot()
  {
    _dataSet = new ScanPathPointEntryXPlotDataSet(_stagePositionEntries);
    
    //for(int dataSetCounter = 0 ; dataSetCounter < _dataSet.size() ; dataSetCounter ++)
    setDataset(_dataSet);
    
    _xPlotRenderer = new XYLineAndShapeRenderer(true, true);
    _xPlotRenderer.setToolTipGenerator(null);
    setRenderer(_xPlotRenderer);

    // add by Khang Wah, 2013-12-13, to fix the scale of graph for every scan path
    // Extract Max and Min of x and y axis
    if(_stagePositionEntries!=null && _stagePositionEntries.isEmpty()==false)
    {
      _minX = _maxX = _stagePositionEntries.get(0).getXInNanometers();
      _minY = _maxY = _stagePositionEntries.get(0).getYInNanometers();
      
      for(StagePosition stagePos : _stagePositionEntries)
      {
        int curX = stagePos.getXInNanometers();
        int curY = stagePos.getYInNanometers();

        if(curX>_maxX) _maxX = curX;
        if(curX<_minX) _minX = curX;
        if(curY>_maxY) _maxY = curY;
        if(curY<_minY) _minY = curY;
      }
    }

    // Set up plot axis.
    NumberAxis xAxis = new NumberAxis(StringLocalizer.keyToString("RMGUI_CHART_X_PLOT_X_AXIS_LABEL_KEY"));
    xAxis.setAutoRangeIncludesZero(false);
    xAxis.setRange(_minX-10000000, _maxX+10000000); // add by Khang Wah, 2013-12-13, to fix the scale of graph for every scan path
    setDomainAxis(xAxis);

    String yAxisLabel = StringLocalizer.keyToString("RMGUI_CHART_X_PLOT_Y_AXIS_LABEL_KEY");
    NumberAxis yAxis = new NumberAxis(yAxisLabel);
    yAxis.setAutoRangeIncludesZero(false);
    yAxis.setRange(_minY-10000000, _maxY+10000000); // add by Khang Wah, 2013-12-13, to fix the scale of graph for every scan path
    setRangeAxis(yAxis);
  }

  /**
   * Draw a threshold line with its label.
   * @author Kee Chin Seong
   */
  void drawThreashold(String thresholdName, double thresholdValue)
  {
  }
  
  public int getScanPathsSize()
  {
    if(_dataSet != null)
      return _dataSet.getScanPathsSize();
    else
      return 0;
  }

  /**
   * Get the general measurement unit.
   * @author Kee Chin Seong
   */
  protected StagePosition getStartingPointXY()
  {
    return null;
  }

  /**
   * @author Kee Chin Seong
   */
  String getStatisticalInformationString()
  {
    return "";
  }

}
