package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 *
 * Dialog used to add multiple instances of a board type
 * to a panel.  This is a modal dialog.
 * @author Andy Mechtenberg
 */
class AddBoardArrayDialog extends EscapeDialog
{
  private Project _project = null;

  private BoardType _boardType;
  private Integer _rows;
  private Integer _columns;
  private Integer _rotation;
  private Boolean _flipped;
  private Double _anchorX;
  private Double _anchorY;
  private Double _offsetX;
  private Double _offsetY;

  private FocusAdapter _textFieldFocusAdapter;
  private ImageIcon _spacingSeparationImage = null;
  private ImageIcon _offsetSeparationImage = null;
  private JPanel _mainPanel = new JPanel();
  private JScrollPane _boardTableScrollPane = new JScrollPane();
  private BoardArrayTable _boardTable = new BoardArrayTable();
  private BoardArrayTableModel _boardArrayTableModel;
  private JPanel _selectionsPanel = new JPanel();
  private JPanel _buttonsPanel = new JPanel();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private JLabel _separationLabel = new JLabel();
  private JRadioButton _spacingRadioButton = new JRadioButton();
  private JRadioButton _offsetRadioButton = new JRadioButton();
  private JLabel _xValueLabel = new JLabel();
  private JLabel _yValueLabel = new JLabel();
  private JPanel _valuesPanel = new JPanel();
  private JPanel _anchorsPanel = new JPanel();
  private JPanel _xyPanelSelections = new JPanel();
  private JPanel _separationOptionsPanel = new JPanel();
  private FlowLayout _separationOptionsFlowLayout = new FlowLayout();
  private JPanel _imagePanel = new JPanel();
  private BorderLayout _selectionsPanelBorderLayout = new BorderLayout();
  private Border _border2;
  private JPanel _boardTabelPanel = new JPanel();
  private BorderLayout _boardTableBorderLayout = new BorderLayout();
  private Border _border3;
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private Border _border5;
  private Border _border6;
  private PairLayout _anchorsPanelPairLayout = new PairLayout(5, 10, false);
  private JLabel _yAnchroLabel = new JLabel();
  private JLabel _xAnchorLabel = new JLabel();
  private PairLayout _valuesPanelPairLayout = new PairLayout(5, 10, false);
  private BorderLayout _xyPanelSelectionsBorderLayout = new BorderLayout();
  private Border _border7;
  private Border _border8;
  private JPanel _innerButtonsPanel = new JPanel();
  private GridLayout _innerButtonsPanelGridLayout = new GridLayout();
  private FlowLayout _imagePanelFlowLayout = new FlowLayout();
  private JLabel _imageLabel = new JLabel();
  private ButtonGroup _separationButtonGroup = new ButtonGroup();
  private Border _border9;
  private Border _border10;

  private NumericTextField _xValueTextField = new NumericTextField();
  private NumericTextField _yValueTextField = new NumericTextField();
  private NumericTextField _xAnchorTextField = new NumericTextField();
  private NumericTextField _yAnchorTextField = new NumericTextField();
  private NumericRangePlainDocument _xValueDocument = new NumericRangePlainDocument();
  private NumericRangePlainDocument _yValueDocument = new NumericRangePlainDocument();
  private NumericRangePlainDocument _xAnchorDocument = new NumericRangePlainDocument();
  private NumericRangePlainDocument _yAnchorDocument = new NumericRangePlainDocument();
  private JPanel _outerButtonsPanel = new JPanel();
  private JPanel _removeBoardsPanel = new JPanel();
  private BorderLayout _buttonsPanelBorderLayout = new BorderLayout();
  private JCheckBox _removeBoardsCheckBox = new JCheckBox();
  private FlowLayout _removeBoardsFlowLayout = new FlowLayout();

  public AddBoardArrayDialog()
  {
    jbInit();
  }

  /**
   * Constructor.  Uses parent frame to center.
   * @author Carli Connally
   */
  public AddBoardArrayDialog(Project project)
  {
    super(MainMenuGui.getInstance(), StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_TITLE_KEY"), true);
    Assert.expect(project != null, "Project is undefined.");

    _project = project;
    jbInit();
    init();
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _project = null;
    _boardType = null;
    _boardTable.clear();
    _boardArrayTableModel.clear();
  }

  /**
   * UI initialization method.  Called and modified by the UI designer.
   * @author Carli Connally
   */
  private void jbInit()
  {
    _textFieldFocusAdapter = new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        JTextField sourceTextField = (JTextField)e.getSource();
        sourceTextField.selectAll();
      }

      public void focusLost(FocusEvent e)
      {
        NumericTextField sourceTextField = (NumericTextField)e.getSource();
        handleTextFieldData(sourceTextField);
      }
    };

    _border2 = BorderFactory.createEmptyBorder(7,7,7,7);
    _border3 = BorderFactory.createEmptyBorder(7,0,7,0);
    _border5 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(165, 163, 151)),
        StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_ARRAY_LAYOUT_KEY")),BorderFactory.createEmptyBorder(7,7,7,7));
    _border6 = BorderFactory.createEmptyBorder(7,7,7,7);
    _border7 = BorderFactory.createEmptyBorder(7,7,7,7);
    _border8 = BorderFactory.createEmptyBorder(7,7,7,7);
    _border9 = BorderFactory.createEmptyBorder(10,25,10,35);
    _border10 = BorderFactory.createEmptyBorder(0,10,0,10);
    _mainPanel.setBorder(_border6);
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _selectionsPanel.setLayout(_selectionsPanelBorderLayout);
    _buttonsPanel.setLayout(_buttonsPanelBorderLayout);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _selectionsPanel.setBorder(_border5);
    _separationLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_BOARD_SEPARATION_PROMPT_KEY") + ":  ");
    _spacingRadioButton.setSelected(true);
    _spacingRadioButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_SPACING_PROMPT_KEY"));
    _spacingRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        radioButtonSeparation_actionPerformed(e);
      }
    });
    _offsetRadioButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_OFFSET_PROMPT_KEY"));
    _offsetRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        radioButtonSeparation_actionPerformed(e);
      }
    });
    _xValueLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_X_VALUE_PROMPT_KEY") + ":  ");
    _yValueLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_Y_VALUE_PROMPT_KEY") + ":  ");
    _valuesPanel.setBorder(_border7);
    _valuesPanel.setLayout(_valuesPanelPairLayout);
    _anchorsPanel.setBorder(_border8);
    _anchorsPanel.setLayout(_anchorsPanelPairLayout);
    _xyPanelSelections.setBorder(_border2);
    _xyPanelSelections.setLayout(_xyPanelSelectionsBorderLayout);
    _separationOptionsPanel.setLayout(_separationOptionsFlowLayout);
    _separationOptionsFlowLayout.setAlignment(FlowLayout.LEFT);
    _separationOptionsFlowLayout.setHgap(7);
    _boardTabelPanel.setBorder(_border3);
    _boardTabelPanel.setLayout(_boardTableBorderLayout);
    _yAnchroLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_Y_ANCHOR_PROMPT_KEY") + ":  ");
    _xAnchorLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_X_ANCHOR_PROMPT_KEY") + ":  ");
    _innerButtonsPanel.setLayout(_innerButtonsPanelGridLayout);
    _innerButtonsPanelGridLayout.setColumns(2);
    _innerButtonsPanelGridLayout.setHgap(20);
    _imagePanel.setLayout(_imagePanelFlowLayout);
    _imagePanel.setBorder(_border9);
    _imageLabel.setBorder(_border10);
    _removeBoardsCheckBox.setSelected(true);
    _removeBoardsCheckBox.setFont(FontUtil.getBoldFont(_removeBoardsCheckBox.getFont(),Localization.getLocale()));
    _removeBoardsCheckBox.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_REMOVE_CURRENT_BOARDS_KEY"));
    _removeBoardsPanel.setLayout(_removeBoardsFlowLayout);
    _removeBoardsFlowLayout.setAlignment(FlowLayout.LEFT);
    this.getContentPane().add(_mainPanel, BorderLayout.CENTER);
    _mainPanel.add(_selectionsPanel, BorderLayout.CENTER);
    _mainPanel.add(_buttonsPanel, BorderLayout.SOUTH);
    _outerButtonsPanel.add(_innerButtonsPanel);
    _innerButtonsPanel.add(_okButton, null);
    _innerButtonsPanel.add(_cancelButton, null);
    _mainPanel.add(_boardTabelPanel, BorderLayout.NORTH);
    _boardTabelPanel.add(_boardTableScrollPane, BorderLayout.CENTER);
    _boardTableScrollPane.getViewport().add(_boardTable, null);
    _selectionsPanel.add(_xyPanelSelections, BorderLayout.SOUTH);
    _selectionsPanel.add(_separationOptionsPanel, BorderLayout.NORTH);
    _xyPanelSelections.add(_valuesPanel, BorderLayout.EAST);
    _valuesPanel.add(_xValueLabel);
    _valuesPanel.add(_xValueTextField);
    _valuesPanel.add(_yValueLabel);
    _valuesPanel.add(_yValueTextField);
    _xyPanelSelections.add(_anchorsPanel, BorderLayout.WEST);
    _separationOptionsPanel.add(_separationLabel, null);
    _separationOptionsPanel.add(_spacingRadioButton, null);
    _separationOptionsPanel.add(_offsetRadioButton, null);
    _selectionsPanel.add(_imagePanel, BorderLayout.CENTER);
    _imagePanel.add(_imageLabel, null);
    _anchorsPanel.add(_xAnchorLabel);
    _anchorsPanel.add(_xAnchorTextField);
    _anchorsPanel.add(_yAnchroLabel);
    _anchorsPanel.add(_yAnchorTextField);
    _separationButtonGroup.add(_offsetRadioButton);
    _separationButtonGroup.add(_spacingRadioButton);
    _buttonsPanel.add(_removeBoardsPanel, java.awt.BorderLayout.CENTER);
    _removeBoardsPanel.add(_removeBoardsCheckBox);
    _buttonsPanel.add(_outerButtonsPanel, java.awt.BorderLayout.EAST);
    _boardTable.setOpaque(false);
    _boardTable.getTableHeader().setReorderingAllowed(false);

    _xValueTextField.setDocument(_xValueDocument);
    _yValueTextField.setDocument(_yValueDocument);
    _xAnchorTextField.setDocument(_xAnchorDocument);
    _yAnchorTextField.setDocument(_yAnchorDocument);
    updateTextFieldDecimalFormats();
    _xValueTextField.setColumns(6);
    _yValueTextField.setColumns(6);
    _xAnchorTextField.setColumns(6);
    _yAnchorTextField.setColumns(6);

    _xValueDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _yValueDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _xAnchorDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _yAnchorDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));

    _xValueTextField.setValue(0.0);
    _yValueTextField.setValue(0.0);
    _xAnchorTextField.setValue(0.0);
    _yAnchorTextField.setValue(0.0);

    _xValueTextField.addFocusListener(_textFieldFocusAdapter);
    _yValueTextField.addFocusListener(_textFieldFocusAdapter);
    _xAnchorTextField.addFocusListener(_textFieldFocusAdapter);
    _yAnchorTextField.addFocusListener(_textFieldFocusAdapter);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateTextFieldDecimalFormats()
  {
    StringBuffer decimalFormat = new StringBuffer("########0.");
    MathUtilEnum unitsEnum = _project.getDisplayUnits();

    int decimalPlaces = MeasurementUnits.getMeasurementUnitDecimalPlaces(unitsEnum);
    for (int i = 0; i < decimalPlaces; i++)
      decimalFormat.append("0"); // use a "0" if you want to show trailing zeros, use a "#" if you don't

    _xValueTextField.setFormat(new DecimalFormat(decimalFormat.toString()));
    _yValueTextField.setFormat(new DecimalFormat(decimalFormat.toString()));
    _xAnchorTextField.setFormat(new DecimalFormat(decimalFormat.toString()));
    _yAnchorTextField.setFormat(new DecimalFormat(decimalFormat.toString()));
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleTextFieldData(NumericTextField numericTextField)
  {
    Assert.expect(numericTextField != null);

    Double pos;
    try
    {
      pos = numericTextField.getDoubleValue();
    }
    catch (ParseException ex)
    {
      // this typically happens when the user deletes all characters from the field -- same as zero
      pos = new Double(0.0);
    }
    numericTextField.setValue(pos);
  }

  /**
   * Extra initialization needed, but not called by the UI designer.
   * @author Carli Connally
   */
  private void init()
  {
    _boardTable.setBoardTypes(_project.getPanel().getBoardTypes());
    _boardArrayTableModel = new BoardArrayTableModel(_project.getPanel().getBoardTypes());
    _boardTable.setModel(_boardArrayTableModel);

    //set up appropriate table dimensions
    _boardTable.setEditorsAndRenderers();
    _boardTable.setPreferredColumnWidths();

    FontMetrics fm = getFontMetrics(getFont());
    int prefHeight = fm.getHeight() + MainMenuGui.TABLE_ROW_HEIGHT_EXTRA;

    int origWidth = _boardTableScrollPane.getPreferredSize().width;
    int scrollHeight = _boardTableScrollPane.getPreferredSize().height;

    int tableHeight =  _boardTable.getPreferredSize().height;

    prefHeight = scrollHeight;
    if (tableHeight < scrollHeight)
    {
      prefHeight = tableHeight;
      Dimension prefScrollSize = new Dimension(origWidth, prefHeight);
      _boardTable.setPreferredScrollableViewportSize(prefScrollSize);
    }

    //load the images and display the correct one
    _offsetSeparationImage = Image5DX.getImageIcon(Image5DX.PS_SEPARATION_BY_OFFSET);
    _spacingSeparationImage = Image5DX.getImageIcon(Image5DX.PS_SEPARATION_BY_SPACING);

    displaySeparationImage();

    pack();
  }

  /**
   * Board(s) specified will be added to the panel.  Dialog will close.
   * @param e ActionEvent Action Event fired
   * @author Carli Connally
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    if (_boardTable.isEditing())
      _boardTable.getCellEditor().stopCellEditing();

    _boardType = (BoardType)(_boardArrayTableModel.getValueAt(0, BoardArrayTable.BOARD_TYPE_COLUMN_INDEX));
    _rows = (Integer)(_boardArrayTableModel.getValueAt(0, BoardArrayTable.ROWS_COLUMN_INDEX));
    _columns = (Integer)(_boardArrayTableModel.getValueAt(0, BoardArrayTable.COLUMNS_COLUMN_INDEX));
    _rotation = (Integer)(_boardArrayTableModel.getValueAt(0, BoardArrayTable.ROTATION_COLUMN_INDEX));
    _flipped = (Boolean)(_boardArrayTableModel.getValueAt(0, BoardArrayTable.FLIPPED_COLUMN_INDEX));

    int totalNewBoards = _rows * _columns;
    // do these separately, so we can re-initialize the correct field back 0.0
    if (totalNewBoards == 0) // nothing to add
    {
      dispose();
      return;
    }
    if (totalNewBoards > 200)
    {
      // too many boards -- this would be slow.  Give an error, but don't exit.
      LocalizedString message = new LocalizedString("MMGUI_SETUP_ADD_BOARD_TOO_MANY_BOARDS_KEY", new Object[]{new Integer(totalNewBoards), new Integer(100)});
      MessageDialog.showErrorDialog(this, StringLocalizer.keyToString(message),
          StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_TITLE_KEY"),
          true);
      return;
    }

    try
    {
      _anchorX = _xAnchorTextField.getDoubleValue();
    }
    catch (ParseException ex)
    {
      _anchorX = new Double(0.0);
    }
    try
    {
      _anchorY = _yAnchorTextField.getDoubleValue();
    }
    catch (ParseException ex1)
    {
      _anchorY = new Double(0.0);
    }

    try
    {
      _offsetX = _xValueTextField.getDoubleValue();
    }
    catch (ParseException ex2)
    {
      _offsetX = new Double(0.0);
    }
    try
    {
      _offsetY = _yValueTextField.getDoubleValue();
    }
    catch (ParseException ex3)
    {
      _offsetY = new Double(0.0);
    }

    // ok, now all the data is gathered.  We need to convert a few things to nanometer units
    MathUtilEnum unitsEnum = _project.getDisplayUnits();
    _anchorX = MathUtil.convertUnits(_anchorX, unitsEnum, MathUtilEnum.NANOMETERS);
    _anchorY = MathUtil.convertUnits(_anchorY, unitsEnum, MathUtilEnum.NANOMETERS);
    _offsetX = MathUtil.convertUnits(_offsetX, unitsEnum, MathUtilEnum.NANOMETERS);
    _offsetY = MathUtil.convertUnits(_offsetY, unitsEnum, MathUtilEnum.NANOMETERS);

    java.util.List<PanelCoordinate> panelCoordinates = calculatePanelCoordinates();

    // we don't allow the deletion of any board that currently is used for alignment -- give an error if this is the case
    if (_removeBoardsCheckBox.isSelected())
    {
      java.util.List<Board> boardsToDelete = _project.getPanel().getBoards();
      Set<Board> boardsUnableToDelete = new HashSet<Board>();
      for (Board board : boardsToDelete)
      {
        for (AlignmentGroup alignmentGroup : board.getPanel().getPanelSettings().getAllAlignmentGroups())
        {
          java.util.List<Pad> pads = new ArrayList<Pad>(alignmentGroup.getPads());
          if (pads.isEmpty() == false)
          {
            for (Pad pad : pads)
            {
              if (pad.getComponent().getBoard() == board)
              {
                boardsUnableToDelete.add(board);
                break;
              }
            }
          }
        }
      }
      if (boardsUnableToDelete.isEmpty() == false)
      {
        java.util.List<Board> badBoards = new ArrayList<Board>(boardsUnableToDelete);
        Collections.sort(badBoards, new AlphaNumericComparator());
        String boardNames = "";
        for (Board board : badBoards)
        {
          boardNames = boardNames + board.getName() + ", ";
        }
        // remove the last comma
        boardNames = boardNames.substring(0, boardNames.lastIndexOf(","));
        LocalizedString message = new LocalizedString("MMGUI_SETUP_BOARD_USED_FOR_ALIGNMENT_KEY", new Object[]
                                                      {boardNames});
        MessageDialog.showErrorDialog(this,
                                      StringLocalizer.keyToString(message),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        return;
      }
    }
    //XCR1725 - Customizable serial number mapping
    //Siew Yeng - prompt to clear serial number mapping if barcode reader is enabled
    if(BarcodeReaderManager.getInstance().getBarcodeReaderAutomaticReaderEnabled() &&
      SerialNumberMappingManager.getInstance().isSerialNumberMappingFileExists())
    {
      int status = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(), 
                                                 StringLocalizer.keyToString("MMGUI_CLEAR_SERIAL_NUMBER_MAPPING_WARNING_MESSAGE_KEY"),
                                                 StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), 
                                                 JOptionPane.YES_NO_OPTION, 
                                                 JOptionPane.QUESTION_MESSAGE);  
      if(status == JOptionPane.NO_OPTION)
      {
        return;
      }
    }
    
    com.axi.v810.gui.undo.CommandManager commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
    commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_CREATE_BOARD_ARRAY_KEY"));
    if (_removeBoardsCheckBox.isSelected())
    {
      try
      {
        java.util.List<Board> boardsToDelete = _project.getPanel().getBoards();
        for(Board boardToDelete : boardsToDelete)
        {
          commandManager.execute(new RemoveBoardCommand(boardToDelete));
        }
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
      }
    }
    // now add the new ones in
    for(PanelCoordinate panelCoordinate : panelCoordinates)
    {
      try
      {
        CreateBoardCommand createBoardCommand = new CreateBoardCommand(_project.getPanel(),
            _boardType,
            panelCoordinate,
            _rotation.intValue(),
            _flipped.booleanValue());

        commandManager.execute(createBoardCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
      }

    }
    
    //XCR1725 - Customizable serial number mapping
    //Siew Yeng - reset serial number mapping
    if(BarcodeReaderManager.getInstance().getBarcodeReaderAutomaticReaderEnabled() &&
      SerialNumberMappingManager.getInstance().isSerialNumberMappingFileExists())
    {
      try
      {
        commandManager.execute(new EditSerialNumberMappingCommand(SerialNumberMappingManager.getInstance().getDefaultSerialNumberMappingDataList()));
        commandManager.execute(new EditSerialNumberMappingBarcodeConfigNameCommand(BarcodeReaderManager.getInstance().getBarcodeReaderConfigurationNameToUse()));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
      }
    }
    commandManager.endCommandBlock();
    // all done, ditch the dialog
    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  private java.util.List<PanelCoordinate> calculatePanelCoordinates()
  {
    int rotation = _rotation.intValue();
    java.util.List<PanelCoordinate> coordinates = new ArrayList<PanelCoordinate>();

    int boardWidth = _boardType.getWidthInNanoMeters();
    int boardLength = _boardType.getLengthInNanoMeters();

    int totalRotation = rotation + _project.getPanel().getDegreesRotationRelativeToCad();
    totalRotation = MathUtil.getDegreesWithin0To359(totalRotation);

    // swap width and length if rotated 90 or 270
    if ((totalRotation == 90) || (totalRotation == 270))
    {
      boardWidth = _boardType.getLengthInNanoMeters();
      boardLength = _boardType.getWidthInNanoMeters();
    }

//    if(rotation == 90)
//    {
//      // modify anchor position
//      _anchorX = _anchorX + boardWidth;
//    }
//    else if (rotation == 180)
//    {
//      // modify anchor position
//      _anchorX = _anchorX + boardWidth;
//      _anchorY = _anchorY + boardLength;
//    }
//    else if (rotation == 270)
//    {
//      _anchorY = _anchorY + boardLength;
//    }


    // first, decide on the spacing vs offset setting -- convert all to offset for consistancy
    if(_spacingRadioButton.isSelected())
    {
      _offsetX = _offsetX + boardWidth;
      _offsetY = _offsetY + boardLength;
    }

    // now everything is in offset mode.  check to make sure the boards don't overlap
    if ((_offsetX < boardWidth) || (_offsetY < boardLength))
    {
//      System.out.println("Too Close!");
    }
    // at this point, I don't care about board width or length -- we have folded that into the offsetx and offsety values

    // for each board to add, start at the anchor x and y, and start building up coordinates.
    // first coordinate is the anchor
    int totalBoards = _rows * _columns;
    if (totalBoards > 0)
    {
      // the rows and columns are 1-based (not zero-based).
      // we've alreday done row=1 col=1 (anchor).  We'll go throught row by row, doing each column
      for (int row = 1; row <= _rows; row++) // start at 1 instead of zero because we already did the anchor coordinate
      {
        for(int column = 1; column <= _columns; column++)
        {
          int newX = _anchorX.intValue() + (_offsetX.intValue() * (column-1));
          int newY = _anchorY.intValue() + (_offsetY.intValue() * (row-1));
          PanelCoordinate someCoordinate = new PanelCoordinate(newX, newY);
          coordinates.add(someCoordinate);
        }
      }
    }

    return coordinates;
  }

  /**
   * Cancels the operation.  Dialog closes. State of the panel remains the same
   * as before the dialog was opened.
   * @param e ActionEvent Action Event fired
   * @author Carli Connally
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  /**
   * Changes the image to match the selection.
   * @param e ActionEvent Action Event fired
   * @author Carli Connally
   */
  private void radioButtonSeparation_actionPerformed(ActionEvent e)
  {
    displaySeparationImage();
  }

  /**
   * Display the image example that matches the type of Separation selected
   * @author Carli Connally
   */
  private void displaySeparationImage()
  {
    if(_offsetRadioButton.isSelected())
    {
      _imageLabel.setIcon(_offsetSeparationImage);
    }
    else
    {
      _imageLabel.setIcon(_spacingSeparationImage);
    }
  }
}
