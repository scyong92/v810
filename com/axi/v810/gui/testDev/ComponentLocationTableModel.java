package com.axi.v810.gui.testDev;

import java.awt.geom.*;
import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: ComponentLocationTableModel</p>
 *
 * <p>Description:  This class is the table model for the component location table in Verify CAD.
 * This table model is responsible for getting the data as needed. There will be 6 columns:<br>
 * Component (Reference Designator)<br>
 * X Location<br>
 * Y Location<br>
 * Rotation<br>
 * Joint Type<br>
 * Subtype<br>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class ComponentLocationTableModel extends ModifySubtypesComponentTableModel
{
  private final String[] _componentLocationTableColumnNames = {
      StringLocalizer.keyToString("ATGUI_COMPONENT_KEY"),      // "Component"
      StringLocalizer.keyToString("MMGUI_SETUP_X_LOC_KEY"),    // "x"
      StringLocalizer.keyToString("MMGUI_SETUP_Y_LOC_KEY"),    // "y"
      StringLocalizer.keyToString("MMGUI_SETUP_ROTATION_KEY"), // "Rotation"
      StringLocalizer.keyToString("ATGUI_FAMILY_KEY"),         // "Joint Type"
      StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY"),        // "Subtype"
  };

  // side choices
  private final String _TOP_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_TOP_LABEL_KEY");
  private final String _BOTTOM_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_BOTTOM_LABEL_KEY");

  private List<ComponentType> _componentTypes;  // list of component Types
  private ComponentType _currentSelectedComponent;
  private int _currentSelectedComponentRow = -1;
  private MathUtilEnum _mathSelectedUnits = MathUtilEnum.MILLIMETERS;

  // default to sort by component, ascending
  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;

  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private Project _project;

  private boolean _isCellEditable = true;
  
  //Siew Yeng - XCR-2123
  private Collection<ComponentType> _currentlySelectedComponents = new ArrayList<ComponentType>();
  /**
   * @author George Booth
   */
  public ComponentLocationTableModel()
  {
    // do nothing
  }

  /**
   * @author George Booth
   */
  public void sortColumn(int column, boolean ascending)
  {
    _currentSortColumn = column;
    _currentSortAscending = ascending;

    switch (column)
    {
      case 0: // name
        Collections.sort(_componentTypes, new ComponentTypeComparator(ascending, ComponentTypeComparatorEnum.REFERENCE_DESIGNATOR));
        break;
      case 1: // x location
        Collections.sort(_componentTypes, new ComponentTypeComparator(ascending, ComponentTypeComparatorEnum.X_COORDINATE));
        break;
      case 2: // y location
        Collections.sort(_componentTypes, new ComponentTypeComparator(ascending, ComponentTypeComparatorEnum.Y_COORDINATE));
        break;
      case 3: // rotation
        Collections.sort(_componentTypes, new ComponentTypeComparator(ascending, ComponentTypeComparatorEnum.DEGREES_ROTATION));
        break;
      case 4: // joint type
        Collections.sort(_componentTypes, new ComponentTypeComparator(ascending, ComponentTypeComparatorEnum.JOINT_TYPE));
        break;
      case 5: // subtype
        Collections.sort(_componentTypes, new ComponentTypeComparator(ascending, ComponentTypeComparatorEnum.SUBTYPE));
        break;
      default:
        Assert.expect(false, "Column does not exist in table model");
        break;
    }
  }

  /**
   * @author George Booth
   */
  public boolean isSortable(int col)
  {
    return true;
  }

  /**
   * @author George Booth
   */
  void setSelectedUnits(MathUtilEnum mathSelectedUnits)
  {
    _mathSelectedUnits = mathSelectedUnits;
  }

  /**
   * @author George Booth
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    ComponentType selectedComponentType = _componentTypes.get(rowIndex);
    switch(columnIndex)
    {
      case 0: // component name
        return selectedComponentType;
      case 1: // x location
        int xCoord = selectedComponentType.getCoordinateInNanoMeters().getX();
        double xCoordInMils = 0.0;
        xCoordInMils = MathUtil.convertUnits(xCoord, MathUtilEnum.NANOMETERS, _mathSelectedUnits);
        xCoordInMils = MathUtil.roundToPlaces(xCoordInMils,2);
        return new Double(xCoordInMils);
      case 2: // y location
        int yCoord = selectedComponentType.getCoordinateInNanoMeters().getY();
        double yCoordInMils = 0.0;
        yCoordInMils = MathUtil.convertUnits(yCoord, MathUtilEnum.NANOMETERS, _mathSelectedUnits);
        yCoordInMils = MathUtil.roundToPlaces(yCoordInMils,2);
        return new Double(yCoordInMils);
      case 3: // rotation
        return new Double(selectedComponentType.getDegreesRotationRelativeToBoard());
      case 4: // joint type
        return TestDev.getComponentDisplayJointType(selectedComponentType);
      case 5: // subtype
        return TestDev.getComponentDisplaySubtype(selectedComponentType);
      default:
        // this should not happen if so then assert
        Assert.expect(false, "no case for column" + columnIndex);
        return null;
    }
  }

  /**
   * @author George Booth
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_componentLocationTableColumnNames[columnIndex];
  }

  /**
   * @author George Booth
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    Assert.expect(aValue != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    Assert.expect(_componentTypes != null);

    if (rowIndex >= _componentTypes.size())
      return;
    Assert.expect(_project != null, "Project not set for new component table model");

    // save current value in case an error occurs
//    Object currentValue = getValueAt(rowIndex, columnIndex);

    ComponentType componentType = _componentTypes.get(rowIndex);
    LandPattern landPattern = componentType.getLandPattern();
    Component component = null;
    String refDesig = componentType.getReferenceDesignator();
    List<Component> components = componentType.getComponents();
    for (Component comp : components)
    {
      if (comp.getReferenceDesignator().equals(refDesig))
      {
        component = comp;
      }
    }
    Assert.expect(component != null, "Component not found for ComponentType");

    BoardCoordinate boardCoordinate = componentType.getCoordinateInNanoMeters();
    String valueStr;
    Double value;

    switch (columnIndex)
    {
      case 0: // column 0 is package name -- not editable
        Assert.expect(false); break;
      case 1: // x location
        Assert.expect(aValue instanceof Number);
        value = ((Number)aValue).doubleValue();
        // convert value in display units to nanometers
        int xCoord = (int)MathUtil.convertUnits(value, _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
        boardCoordinate.setX(xCoord);

        //Siew Yeng - XCR-2123
        try
        {
          _commandManager.execute(new ComponentTypeSetCoordinateInNanoMetersCommand(componentType, boardCoordinate));
          //clear off the component which has done focus region edited
          if(component.isUseCustomFocusRegion() == true)
          {
              component.setUseCustomFocusRegion(false);
              _project.removeComponentFocusRegionInMap(component);
          }
        }
        catch (XrayTesterException xte)
        {
          displayXrayTesterException(xte);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      case 2: // y location
        Assert.expect(aValue instanceof Number);
        value = ((Number)aValue).doubleValue();
        int yCoord = (int)MathUtil.convertUnits(value, _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
        boardCoordinate.setY(yCoord);
        
        //Siew Yeng - XCR-2123
        try
        {
          _commandManager.execute(new ComponentTypeSetCoordinateInNanoMetersCommand(componentType, boardCoordinate));
        }
        catch (XrayTesterException xte)
        {
          displayXrayTesterException(xte);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      case 3: // rotation
        Assert.expect(aValue instanceof String);
        valueStr = (String)aValue;
        Double doubleValue = new Double(valueStr);
        
        //Siew Yeng - XCR-2123
        try
        {
          _commandManager.execute(new ComponentTypeSetDegreesRotationCommand(componentType, doubleValue.doubleValue()));
        }
        catch (XrayTesterException xte)
        {
          displayXrayTesterException(xte);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      case 4: // joint type
        JointTypeEnum newJointTypeEnum = (JointTypeEnum)aValue;

        try
        {
          _commandManager.execute(new ComponentTypeSetJointTypeCommand(componentType, newJointTypeEnum, true));
        }
        catch (XrayTesterException xte)
        {
          displayXrayTesterException(xte);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      case 5: // subtype
        String newSubtypeName = (String)aValue;
        JointTypeEnum jointType = componentType.getComponentTypeSettings().getSubtype().getJointTypeEnum();
        java.util.List<Subtype> usedSubtypes = componentType.getSideBoardType().getBoardType().getSubtypesIncludingUnused(jointType);
        for (Subtype subtype : usedSubtypes)
        {
          if (subtype.getShortName().equalsIgnoreCase(newSubtypeName))
          {
            try
            {
              _commandManager.execute(new ComponentTypeSetSubtypeCommand(componentType, subtype));
            }
            catch (XrayTesterException xte)
            {
              displayXrayTesterException(xte);
              fireTableCellUpdated(rowIndex, columnIndex);
            }
            return; // we're done as soon as it's set
          }
        }
        // if not set, then this is a new subtype and we need to create one
        Panel panel = componentType.getSideBoardType().getBoardType().getPanel();
        JointTypeEnum jointTypeEnum = componentType.getCompPackage().getJointTypeEnum();
        Subtype newSubtype = panel.createSubtype(newSubtypeName, landPattern, jointTypeEnum);
        try
        {
          _commandManager.execute(new ComponentTypeSetSubtypeCommand(componentType, newSubtype, true));
        }
        catch (XrayTesterException xte)
        {
          displayXrayTesterException(xte);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      default: // should not happen
        Assert.expect(false, "no case for column" + columnIndex);
    }
  }

  /**
   * @author George Booth
   * @Edited By Kee Chin Seong
   * @Edited by Siew Yeng - refer ModifySubtypesComponentTableModel.java
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    if(_isCellEditable == false)
      return false;
    
    // if nothing selected, nothing can be editable
    if (_currentlySelectedComponents.isEmpty())
      return false;

    // if the current component is NOT selected, then it's not editable.
    // need to make the check up front before all the more detailed checks
    if (_currentlySelectedComponents.contains(getValueAt(rowIndex, 0)) == false)
      return false;
    
    switch (columnIndex) {
      case 0: // component name - not editable
        return false;
      case 1: // x location
        if(_currentlySelectedComponents.size() > 1)
          return false;
        return true;
      case 2: // y location
        if(_currentlySelectedComponents.size() > 1)
          return false;
        return true;
      case 3: // rotation
        if(_currentlySelectedComponents.size() > 1)
          return false;
        return true;
      case 4: // joint type
      {
        //Siew Yeng - XCR-2123
        for (ComponentType compType : _currentlySelectedComponents)
        {
          if (compType.getCompPackage().usesOneJointTypeEnum() == false)  // mixed case not editable
            return false;
        }
        return true;
      }
      case 5: // subtype
      {
        //Siew Yeng - XCR-2123
        for (ComponentType compType : _currentlySelectedComponents)
        {
          if (compType.isTestable() == false)
            return false;
        }
        return true;
      }
      default: // should not happen
        Assert.expect(false, "no case for column" + columnIndex);
    }
    return false;
  }

  /**
   * @author George Booth
   */
  public int getRowCount()
  {
    if (_componentTypes == null)
      return 0;
    else
      return _componentTypes.size();
  }

  /**
   * @author George Booth
   */
  public int getColumnCount()
  {
    return _componentLocationTableColumnNames.length;
  }

  /**
   * @author George Booth
   */
  void clear()
  {
    if (_componentTypes != null)
    {
      _componentTypes.clear();
    }
    _currentSortColumn = 0;
    _currentSortAscending = true;
    _currentSelectedComponent = null;
    _currentlySelectedComponents.clear();
    _project = null;
    fireTableDataChanged();
  }

  /**
   * @author George Booth
   */
  public List<ComponentType> getBoardSideComponentTypes(Board board, BoardType boardType, String side)
  {
    Assert.expect(board != null);
    Assert.expect(boardType != null);
    Assert.expect(side != null);

    List<ComponentType> componentTypes = null;
    if (side.equalsIgnoreCase(_TOP_SIDE))
    {
      if (board.topSideBoardExists())
      {
        SideBoardType sideBoard = board.getTopSideBoard().getSideBoardType();
        componentTypes = sideBoard.getComponentTypes();
      }
    }
    else if (side.equalsIgnoreCase(_BOTTOM_SIDE))
    {
      if (board.bottomSideBoardExists())
      {
        SideBoardType sideBoard = board.getBottomSideBoard().getSideBoardType();
        componentTypes = sideBoard.getComponentTypes();
      }
    }
    else
    {
       componentTypes =  boardType.getComponentTypes();
    }

    if (componentTypes != null)
    {
      return componentTypes;
    }
    else // return empty list
    {
      return new ArrayList<ComponentType>();
    }
  }

  /**
   * @author George Booth
   */
  public int numberOfComponents(Board board, BoardType boardType, String side)
  {
    List<ComponentType> componentTypes = getBoardSideComponentTypes(board, boardType, side);
    if (componentTypes == null)
      return 0;
    else
      return componentTypes.size();
  }

  /**
   * @author George Booth
   */
  public void populateWithPackageData(Board board, BoardType boardType, String side, CompPackage selectedPackage)
  {
    Assert.expect(board != null);
    Assert.expect(boardType != null);
    Assert.expect(side != null);
    Assert.expect(selectedPackage != null);

    // get component types for specified board, boardType and side
    List<ComponentType> selectedComponentTypes = getBoardSideComponentTypes(board, boardType, side);

    _componentTypes = new ArrayList<ComponentType>();

    // get those matching selectedPackage
    for (ComponentType componentType : selectedComponentTypes)
    {
      if (componentType.getCompPackage().equals(selectedPackage))
      {
        _componentTypes.add(componentType);
      }
    }

    sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableStructureChanged();
  }
  
  /**
   * regions are not supported at version 1
   * @author George Booth
   */
  public void populateWithRegionData(String selectedRegion)
  {
    Assert.expect(selectedRegion != null);
  }

  /**
   * @author George Booth
   */
  public void populateWithPanelData(Board board, BoardType boardType, String side)
  {
    Assert.expect(board != null);
    Assert.expect(boardType != null);
    Assert.expect(side != null);

    _componentTypes = getBoardSideComponentTypes(board, boardType, side);

    sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableDataChanged();
  }

  /**
   * Get the row for a specified componentType. If the componentType is null or
   * the component is not found, default to select the first row by default.
   * @return row for componentType or 0 if not found
   * @author George Booth
   */
  int getRowForComponentType(ComponentType componentType)
  {
    Assert.expect(_componentTypes.size() > 0);
    // we can always select row 0
    if (componentType == null)
    {
      return 0;
    }
    // search by reference designator
    String refDesig = componentType.getReferenceDesignator();
    for (int row = 0; row < _componentTypes.size(); row ++)
    {
      ComponentType component = _componentTypes.get(row);
      if (component.getReferenceDesignator().equals(refDesig))
      {
        return row;
      }
    }
    // not found
    return 0;
  }

  /**
   * @author George Booth
   */
  ComponentType getComponentTypeAt(int row)
  {
    Assert.expect(row >= 0 && row < _componentTypes.size());
    return _componentTypes.get(row);
  }
  
  /**
   * @author George Booth
   */
  void showLocationErrorDialog(final String message)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        MessageDialog.showErrorDialog(
            MainMenuGui.getInstance(),
            message,
            StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
            true);
      }
    });
  }

  /**
   * @author George Booth
   */
  boolean adjustSelectedComponentLocation(Board board, Point2D offset)
  {
    Assert.expect(_currentlySelectedComponents != null);
    Assert.expect(_currentlySelectedComponents.size() == 1);
    Assert.expect(board != null);
    Assert.expect(offset != null);

    Component component = null;
    String boardName = board.getName();
    List<Component> components = _currentlySelectedComponents.iterator().next().getComponents();
    for (Component comp : components)
    {
      if (comp.getBoard().getName().equals(boardName))
      {
        component = comp;
        break;
      }
    }
    Assert.expect(component != null, "Component not found for ComponentType");

    if (component.willComponentBeInsideBoardOutlineAfterOffset((int)offset.getX(), (int)offset.getY()) == false)
    {
      showLocationErrorDialog(
        StringLocalizer.keyToString(new LocalizedString("CAD_COMPONENT_LOCATION_OUT_OF_BOUNDS_STILL_ACTIVE_KEY",
           new Object[] {component.getReferenceDesignator()})));
      // adjustment will still be active so they can readjust
      return false;
    }

    try
    {
      _commandManager.execute(new MoveComponentByOffsetInNanoMetersCommand(component, offset));
    }
    catch (XrayTesterException xte)
    {
      displayXrayTesterException(xte);
      // adjustment will be cancelled
      return true;
    }

    fireTableRowsUpdated(_currentSelectedComponentRow, _currentSelectedComponentRow);
    return true;
  }

  /**
   * @author George Booth
   */
  boolean adjustBoardSideLocation(Board board, String side, Point2D offset)
  {
    Assert.expect(board != null);
    Assert.expect(side != null);
    Assert.expect(offset != null);

    String sideMessage = "";
    SideBoard sideBoard = null;
    if (side.equalsIgnoreCase(_TOP_SIDE))
    {
      sideMessage = _TOP_SIDE;
      if (board.topSideBoardExists())
      {
        sideBoard = board.getTopSideBoard();
      }
      else
      {
        Assert.expect(false, "Invalid board side: Top does not exist");
      }
    }
    else if (side.equalsIgnoreCase(_BOTTOM_SIDE))
    {
      sideMessage = _BOTTOM_SIDE;
      if (board.bottomSideBoardExists())
      {
        sideBoard = board.getBottomSideBoard();
      }
      else
      {
        Assert.expect(false, "Invalid board side: Bottom does not exist");
      }
    }
    else
    {
      Assert.expect(false, "Invalid board side: \"" + side + "\"");
    }
    Assert.expect(sideBoard != null, "SideBoard not found for for side \"" + side + "\"");

    if (sideBoard.willComponentsBeInsideBoardOutlineAfterOffset((int)offset.getX(), (int)offset.getY()) == false)
    {
      showLocationErrorDialog(
      StringLocalizer.keyToString(new LocalizedString("CAD_BOARD_SIDE_LOCATION_OUT_OF_BOUNDS_STILL_ACTIVE_KEY",
          new Object[]{sideMessage})));
      // adjustment will still be active so they can readjust
      return false;
    }

    try
    {
      _commandManager.execute(new MoveBoardSideByOffsetInNanoMetersCommand(sideBoard, offset));
    }
    catch (XrayTesterException xte)
    {
      displayXrayTesterException(xte);
      // adjustment will be cancelled
      return true;
    }

    fireTableDataChanged();
    return true;
  }

  /**
   * @author George Booth
   */
  boolean adjustBoardOriginLocation(Board board, Point2D offset)
  {
    Assert.expect(board != null);
    Assert.expect(offset != null);
    
    if (board.willBoardBeInsidePanelOutlineAfterOffset((int)offset.getX(), (int)offset.getY()) == false)
    {
      showLocationErrorDialog(
      StringLocalizer.keyToString(
          new LocalizedString("CAD_BOARD_ORIGIN_LOCATION_OUT_OF_BOUNDS_STILL_ACTIVE_KEY",
              new Object[]{board.getName()})));
      // adjustment will still be active so they can readjust
      return false;
    }

    try
    {
      _commandManager.execute(new MoveBoardOriginByOffsetInNanoMetersCommand(board, offset));
    }
    catch (XrayTesterException xte)
    {
      displayXrayTesterException(xte);
      // adjustment will be cancelled
      return true;
    }
    
    return true;
  }

  /**
   * @author George Booth
   */
  public void setSelectedComponent(Board selectedBoard, ComponentType selectedComponent, int row)
  {
    Assert.expect(selectedBoard != null);
    Assert.expect(selectedComponent != null);
    _currentSelectedComponent = selectedComponent;
    _currentSelectedComponentRow = row;
  }
  
  /**
   * To handle multiple component selection for component table at Adjust CAD.
   * Create this new function so that it will not affect the existing behavior for package table.
   * @author Siew Yeng
   */
  public void setSelectedComponents(Board selectedBoard, Collection<ComponentType> selectedComponents)
  {
    Assert.expect(selectedBoard != null);
    Assert.expect(selectedComponents != null);
    _currentlySelectedComponents = selectedComponents;
  }

  /**
   * @author Siew Yeng
   */
  public Collection<ComponentType> getSelectedComponents()
  {
    Assert.expect(_currentlySelectedComponents != null);
    
    return _currentlySelectedComponents;
  }
  
  /**
   * @author George Booth
   */
  public void clearSelectedComponent()
  {
    _currentlySelectedComponents.clear();
    _currentSelectedComponent = null;
  }

  /**
   * @author George Booth
   */
  void setProject (Project project)
  {
    Assert.expect(project != null);
    _project = project;
  }

  /**
   * @author George Booth
   */
  void displayXrayTesterException(XrayTesterException xte)
  {
    MessageDialog.showErrorDialog(
        MainMenuGui.getInstance(),
        xte.getLocalizedMessage(),
        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
        true);
  }

  /**
   * @author George Booth
   */
  public int getRowForDataStartingWith(String key)
  {
    key = key.toLowerCase();
    int index = -1;
    for (ComponentType compType :_componentTypes)
    {
      index++;
      String compName = compType.getReferenceDesignator().toLowerCase();
      if (compName.startsWith(key))
      {
        return index;
      }
    }
    return -1;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setIsCellEditable(boolean isCellEditable)
  {
    _isCellEditable = isCellEditable;
  }

  /**
   * @author Siew Yeng
   */
  void updateSpecificComponent(ComponentType componentType)
  {
    int componentRow = _componentTypes.indexOf(componentType);
    fireTableRowsUpdated(componentRow, componentRow);
  }
}
