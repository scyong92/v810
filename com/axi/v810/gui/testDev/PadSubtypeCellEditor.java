package com.axi.v810.gui.testDev;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * This class implements a combo box cell editor tailored to changing the subtype
 * for a pad or list of selected pads, all with the same component
 * @author Andy Mechtenberg
 */
public class PadSubtypeCellEditor extends DefaultCellEditor
{
  private ModifySubtypesPadTypeTableModel _padTypeTableModel;
  // subtype change choices
  private String _newSubtype;
  private String _noTest;
  private String _renameSubtype;
  private String _restoreSubtype;

  /**
   * @author Andy Mechtenberg
   */
  public PadSubtypeCellEditor(JComboBox comboBox, ModifySubtypesPadTypeTableModel padTypeTableModel)
  {
    super(comboBox);

    Assert.expect(padTypeTableModel != null);
    _padTypeTableModel = padTypeTableModel;
  }

  /**
   * @author Andy Mechtenberg
   */
  void setEditorChoices(String newSubtype, String noTest, String renameSubtype, String restoreSubtype)
  {
    Assert.expect(newSubtype != null);
    Assert.expect(noTest != null);
    Assert.expect(renameSubtype != null);
    Assert.expect(restoreSubtype != null);

    _newSubtype = newSubtype;
    _noTest = noTest;
    _renameSubtype = renameSubtype;
    _restoreSubtype = restoreSubtype;
  }

  /**
   * @author Andy Mechtenberg
   */
  public java.awt.Component getTableCellEditorComponent(JTable table, Object value, boolean isSelectable, int row, int column)
  {
    // get the editor (should be a JComboBox as was specified in the constructor)
    // then modify the contents of the combo box to list only the valid choices for this row in the table
    int selectedRows[] = table.getSelectedRows();
    boolean allSameFamily = false;
    boolean allSameSubtype = false;
    boolean noTestsExist = false;
    boolean testsExist = false;
    boolean mixedSubtypes = false;
    JointTypeEnum singleJointTypeEnum = null;
    String singleSubtype = null;
    BoardType boardType = null;

    // assign all booleans above by looking at each component selected.
    for (int i = 0; i < selectedRows.length; i++)
    {
      PadType padType = _padTypeTableModel.getPadTypeAt(selectedRows[i]);
      boardType = padType.getComponentType().getSideBoardType().getBoardType();
      PadTypeSettings padTypeSettings = padType.getPadTypeSettings();

      if (padTypeSettings.isInspected() == false)
        noTestsExist = true;
      else
        testsExist = true;

      if (padTypeSettings.isInspected())
      {
        if(singleJointTypeEnum == null)
        {
          singleJointTypeEnum = padTypeSettings.getSubtype().getJointTypeEnum();
          allSameFamily = true;
        }
        else
        {
          if(singleJointTypeEnum.equals(padType.getPadTypeSettings().getSubtype().getJointTypeEnum()) == false)
            allSameFamily = false;
        }
        if(singleSubtype == null)
        {
          singleSubtype = padTypeSettings.getSubtype().getShortName();
          allSameSubtype = true;
        }
        else
        {
          if(singleSubtype.equalsIgnoreCase(padTypeSettings.getSubtype().getShortName()) == false)
            allSameSubtype = false;
        }
      }
    }

    java.awt.Component editor = super.getTableCellEditorComponent(table, value, isSelectable, row, column);
    JComboBox comboBox = (JComboBox)editor;
    comboBox.removeAllItems();
    // do the single selection first - easier if we get this out of the way
    if (selectedRows.length == 1)
    {
      if (noTestsExist)
      {
        comboBox.addItem(_noTest);
        comboBox.addItem(_restoreSubtype);
      }
      else if (mixedSubtypes)
      {
        comboBox.addItem(_noTest);
      }
      else
      {
        comboBox.addItem(_newSubtype);
        comboBox.addItem(_noTest);
        comboBox.addItem(_renameSubtype);
        addOtherSubtypesForFamily(comboBox, boardType, singleJointTypeEnum, singleSubtype);
        comboBox.setSelectedItem(singleSubtype);
      }
    }
    else
    {
      // now we know that more than one component was selected, so build up with different rules
      if(allSameSubtype && allSameFamily && (noTestsExist == false))
      {
        comboBox.addItem(_newSubtype);
        comboBox.addItem(_noTest);
        comboBox.addItem(_renameSubtype);
        addOtherSubtypesForFamily(comboBox, boardType, singleJointTypeEnum, singleSubtype);
        comboBox.setSelectedItem(singleSubtype);
        // add other subtypes for this family
      }
      else if(allSameFamily && (allSameSubtype == false) && (noTestsExist == false))
      {
        comboBox.addItem(_newSubtype);
        comboBox.addItem(_noTest);
        addOtherSubtypesForFamily(comboBox, boardType, singleJointTypeEnum, "");
        // add other subtypes for this family
      }
      else if(noTestsExist && (testsExist == false))
      {
        comboBox.addItem(_noTest);
        comboBox.addItem(_restoreSubtype);
      }
      else if (noTestsExist)
      {
        comboBox.addItem(_noTest);
        comboBox.addItem(_restoreSubtype);
      }
      else
      {
        comboBox.addItem(_noTest);
      }
    }

    comboBox.setMaximumRowCount(30);
    return comboBox;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void addOtherSubtypesForFamily(JComboBox comboBox, BoardType boardType, JointTypeEnum singleJointTypeEnum, String excludedSubtype)
  {
    Assert.expect(comboBox != null);
    Assert.expect(boardType != null);
    Assert.expect(singleJointTypeEnum != null);
    Assert.expect(excludedSubtype != null);

    java.util.List<Subtype> subtypes = boardType.getSubtypesIncludingUnused(singleJointTypeEnum);
    for(Subtype subtype : subtypes)
    {
//      if(excludedSubtype.equalsIgnoreCase(subtype.getName()) == false)
//      {
        String subtypeName = subtype.getShortName();
        comboBox.addItem(subtypeName);
//      }
    }
  }
}
