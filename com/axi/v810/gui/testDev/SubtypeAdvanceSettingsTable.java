package com.axi.v810.gui.testDev;

import javax.swing.*;
import javax.swing.table.*;
import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * @author chin-seong.kee
 */
public class SubtypeAdvanceSettingsTable extends JSortTable
{
  public static final int _SUBTYPE_INDEX = 0;
  public static final int _JOINT_TYPE_INDEX = 1;
  public static final int _USER_GAIN_INDEX = 2;
  public static final int _STAGE_SPEED_INDEX = 3;
  public static final int _INTEFERENCE_COMPENSATION_INDEX = 4;
  public static final int _INTERGRATION_LEVEL_INDEX = 5;
  public static int _CAMERA_ANGLE_ENABLED_INDEX = 6;
  public static int _MAGNIFICATION_TYPE_INDEX = 7;
  public static int _DRO_LEVEL_INDEX = 8;  // Siew Yeng - XCR-2140 - DRO
  
  private static final int _COMPONENT_UNDO_INDEX = 0;
  
  private final double _SUBTYPE_NAME_COLUMN_WIDTH_PERCENTAGE = 0.3;
  private final double _JOINT_NAME_COLUMN_WIDTH_PERCENTAGE = 0.2;
  private final double _USER_GAIN_COLUMN_WIDTH_PERCENTAGE = 0.1;
  private final double _STAGE_SPEED_COLUMN_WIDTH_PERCENTAGE = 0.1;
  private final double _INTEFERENCE_COMPENSATION_COLUMN_WIDTH_PERCENTAGE = 0.1;
  private final double _INTERGRATION_LEVEL_COLUMN_WIDTH_PERCENTAGE = 0.1;
  private final double _MAGNIFICATION_TYPE_COLUMN_WIDTH_PERCENTAGE = 0.1;
  private final double _DRO_LEVEL_COLUMN_WIDTH_PERCENTAGE = 0.1;
  private final double _CAMERA_ANGLE_ENABLED_COLUMN_WIDTH_PERCENTAGE = 0.1;
  
  // subtype change choices
  private final String _NEW_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NEW_KEY");
  private final String _NEW_SHADED_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NEW_SHADED_KEY");
  private final String _RENAME_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_RENAME_KEY");

  private SubtypeAdvanceSettingsTableModel _subtypeAdvancedSettingsTableModel;
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  
  private BoardType _currentBoardType = null;

  private boolean _isVariableMagnification = false;
  
  private int _maxDROLevel = 1; //Siew Yeng - XCR-2549
  private boolean _showCameraSelectionOption = false;
  private boolean _isAbleToSelectMultipleSpeed = false;
  /**
   * @param model
   * @author Chin Seong
   */
  public SubtypeAdvanceSettingsTable(SubtypeAdvanceSettingsTableModel model)
  {
    Assert.expect(model != null);

    super.setModel(model);
    
    _subtypeAdvancedSettingsTableModel = model;
   
    jbInit();
  }

  private void jbInit()
  {
    setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    setSurrendersFocusOnKeystroke(false);
    sizeColumnsToFit(-1);
    setEnabled(true);
  }
  
  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author George Booth
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();
 
    if(_isVariableMagnification)
    {
      columnModel.getColumn(_SUBTYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _SUBTYPE_NAME_COLUMN_WIDTH_PERCENTAGE));
      columnModel.getColumn(_JOINT_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _JOINT_NAME_COLUMN_WIDTH_PERCENTAGE));
      columnModel.getColumn(_USER_GAIN_INDEX).setPreferredWidth((int)(totalColumnWidth * _USER_GAIN_COLUMN_WIDTH_PERCENTAGE));
      columnModel.getColumn(_STAGE_SPEED_INDEX).setPreferredWidth((int)(totalColumnWidth * _STAGE_SPEED_COLUMN_WIDTH_PERCENTAGE));
      columnModel.getColumn(_INTEFERENCE_COMPENSATION_INDEX).setPreferredWidth((int)(totalColumnWidth * _INTEFERENCE_COMPENSATION_COLUMN_WIDTH_PERCENTAGE));
      columnModel.getColumn(_INTERGRATION_LEVEL_INDEX).setPreferredWidth((int)(totalColumnWidth * _INTERGRATION_LEVEL_COLUMN_WIDTH_PERCENTAGE));
      columnModel.getColumn(_MAGNIFICATION_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _MAGNIFICATION_TYPE_COLUMN_WIDTH_PERCENTAGE));
      if (_showCameraSelectionOption)
        columnModel.getColumn(_CAMERA_ANGLE_ENABLED_INDEX).setPreferredWidth((int)(totalColumnWidth * _CAMERA_ANGLE_ENABLED_COLUMN_WIDTH_PERCENTAGE));
      //Siew Yeng - XCR-2549
      if(_maxDROLevel > 1)
        columnModel.getColumn(_DRO_LEVEL_INDEX).setPreferredWidth((int)(totalColumnWidth * _DRO_LEVEL_COLUMN_WIDTH_PERCENTAGE));
    }
    else
    {
      columnModel.getColumn(_SUBTYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * (_SUBTYPE_NAME_COLUMN_WIDTH_PERCENTAGE+_MAGNIFICATION_TYPE_COLUMN_WIDTH_PERCENTAGE)));
      columnModel.getColumn(_JOINT_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _JOINT_NAME_COLUMN_WIDTH_PERCENTAGE));
      columnModel.getColumn(_USER_GAIN_INDEX).setPreferredWidth((int)(totalColumnWidth * _USER_GAIN_COLUMN_WIDTH_PERCENTAGE));
      columnModel.getColumn(_STAGE_SPEED_INDEX).setPreferredWidth((int)(totalColumnWidth * _STAGE_SPEED_COLUMN_WIDTH_PERCENTAGE));
      columnModel.getColumn(_INTEFERENCE_COMPENSATION_INDEX).setPreferredWidth((int)(totalColumnWidth * _INTEFERENCE_COMPENSATION_COLUMN_WIDTH_PERCENTAGE));
      columnModel.getColumn(_INTERGRATION_LEVEL_INDEX).setPreferredWidth((int)(totalColumnWidth * _INTERGRATION_LEVEL_COLUMN_WIDTH_PERCENTAGE));      
      
      //Siew Yeng - XCR-2549
      if(_maxDROLevel > 1)
        columnModel.getColumn((_DRO_LEVEL_INDEX)).setPreferredWidth((int)(totalColumnWidth * _DRO_LEVEL_COLUMN_WIDTH_PERCENTAGE));
      if (_showCameraSelectionOption)
        columnModel.getColumn(_CAMERA_ANGLE_ENABLED_INDEX).setPreferredWidth((int)(totalColumnWidth * _CAMERA_ANGLE_ENABLED_COLUMN_WIDTH_PERCENTAGE));
    }
  }
  /**
   * @return
   * @author Chin Seong
   */
  public void setModel(SubtypeAdvanceSettingsTableModel componentTableModel)
  {
    Assert.expect(componentTableModel != null);

    _subtypeAdvancedSettingsTableModel = componentTableModel;
    super.setModel(_subtypeAdvancedSettingsTableModel);
  }

  /**
   * @return
   * @author Chin Seong
   */
  public SubtypeAdvanceSettingsTableModel getTableModel()
  {
    Assert.expect(_subtypeAdvancedSettingsTableModel != null);
    return _subtypeAdvancedSettingsTableModel;
  }

  /**
   * @author Chin Seong
   */
  public void setupCellEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();   

    JComboBox componentSubtypeComboBox = new JComboBox();
    componentSubtypeComboBox.addItem(_NEW_SHADED_SUBTYPE);
    DefaultCellEditor componentSubtypeEditor = new DefaultCellEditor(componentSubtypeComboBox);
    columnModel.getColumn(_SUBTYPE_INDEX).setCellEditor(componentSubtypeEditor);
    
    JComboBox userGainComboBox = new JComboBox();
    userGainComboBox.addItem(UserGainEnum.ONE);
    userGainComboBox.addItem(UserGainEnum.TWO);
    userGainComboBox.addItem(UserGainEnum.THREE);
    userGainComboBox.addItem(UserGainEnum.FOUR);
    userGainComboBox.addItem(UserGainEnum.FIVE);
    userGainComboBox.addItem(UserGainEnum.SIX);
    userGainComboBox.addItem(UserGainEnum.SEVEN);
    userGainComboBox.addItem(UserGainEnum.EIGHT);
    userGainComboBox.addItem(UserGainEnum.NINE);
    DefaultCellEditor userGainCellEditor = new DefaultCellEditor(userGainComboBox);
    columnModel.getColumn(_USER_GAIN_INDEX).setCellEditor(userGainCellEditor);
    
    JComboBox stageSpeedComboBox = new JComboBox();
    stageSpeedComboBox.addItem(StageSpeedEnum.ONE);
    stageSpeedComboBox.addItem(StageSpeedEnum.TWO);
    stageSpeedComboBox.addItem(StageSpeedEnum.THREE);
    stageSpeedComboBox.addItem(StageSpeedEnum.FOUR);
    stageSpeedComboBox.addItem(StageSpeedEnum.FIVE);
    stageSpeedComboBox.addItem(StageSpeedEnum.SIX);
    stageSpeedComboBox.addItem(StageSpeedEnum.SEVEN);
    stageSpeedComboBox.addItem(StageSpeedEnum.EIGHT);
    stageSpeedComboBox.addItem(StageSpeedEnum.NINE);
    stageSpeedComboBox.addItem(StageSpeedEnum.TEN);
    stageSpeedComboBox.addItem(StageSpeedEnum.ELEVEN);
    stageSpeedComboBox.addItem(StageSpeedEnum.TWELVE);
    stageSpeedComboBox.addItem(StageSpeedEnum.THIRTEEN);
    stageSpeedComboBox.addItem(StageSpeedEnum.FOURTEEN);
    stageSpeedComboBox.addItem(StageSpeedEnum.FIFTEEN);
    stageSpeedComboBox.addItem(StageSpeedEnum.SIXTEEN);
    stageSpeedComboBox.addItem(StageSpeedEnum.SEVENTEEN);

    if (_isAbleToSelectMultipleSpeed == false)
    {
      DefaultCellEditor stageCellEditor = new DefaultCellEditor(stageSpeedComboBox);
      columnModel.getColumn(_STAGE_SPEED_INDEX).setCellEditor(stageCellEditor);
    }
    
    JComboBox artifactCompensationComboBox = new JComboBox();
    artifactCompensationComboBox.addItem(ArtifactCompensationStateEnum.COMPENSATED);
    artifactCompensationComboBox.addItem(ArtifactCompensationStateEnum.NOT_COMPENSATED);
    DefaultCellEditor artifactCompensationCellEditor = new DefaultCellEditor(artifactCompensationComboBox);
    
    columnModel.getColumn(_INTEFERENCE_COMPENSATION_INDEX).setCellEditor(artifactCompensationCellEditor);

    JComboBox integrationLevelComboBox = new JComboBox();
    SignalIntegrationLevelCellEditor integrationLevelColumnCellEditor = new SignalIntegrationLevelCellEditor(integrationLevelComboBox,
              _subtypeAdvancedSettingsTableModel);
    columnModel.getColumn(_INTERGRATION_LEVEL_INDEX).setCellEditor(integrationLevelColumnCellEditor);
    SignalIntegrationLevelCellRenderer integrationLevelColumnCellRenderer = new SignalIntegrationLevelCellRenderer();
    columnModel.getColumn(_INTERGRATION_LEVEL_INDEX).setCellRenderer(integrationLevelColumnCellRenderer);

    if(_isVariableMagnification)
    {
      JComboBox magnificationTypeComboBox = new JComboBox();
      MagnificationTypeCellEditor magnificationTypeCellEditor = new MagnificationTypeCellEditor(magnificationTypeComboBox, _subtypeAdvancedSettingsTableModel);
      columnModel.getColumn(_MAGNIFICATION_TYPE_INDEX).setCellEditor(magnificationTypeCellEditor);
      MagnificationTypeCellRenderer magnificationTypeCellRenderer = new MagnificationTypeCellRenderer();
      columnModel.getColumn(_MAGNIFICATION_TYPE_INDEX).setCellRenderer(magnificationTypeCellRenderer);      
    }
    
    //Siew Yeng - XCR-2549
    if(_maxDROLevel > 1)
    {
      //Siew Yeng - XCR-2140 - DRO
      JComboBox DROLevelComboBox = new JComboBox();
      for(int i = 1; i <= _maxDROLevel; i++)
      {
        DROLevelComboBox.addItem(DynamicRangeOptimizationLevelEnum.getDynamicRangeOptimizationLevelToEnumMapValue(i));
      }
      DefaultCellEditor DROLevelCellEditor = new DefaultCellEditor(DROLevelComboBox);
      DynamicRangeOptimizationLevelCellRenderer droLevelCellRenderer = new DynamicRangeOptimizationLevelCellRenderer();

      columnModel.getColumn(_DRO_LEVEL_INDEX).setCellEditor(DROLevelCellEditor);
      columnModel.getColumn(_DRO_LEVEL_INDEX).setCellRenderer(droLevelCellRenderer);    
    }
  }

  /**
   * Overriding this method allows us to have one cell's value change apply
   * to every selected row in the table.  Multi-cell editing requires this.
   * @author Chin Seong
   */
  public void setValueAt(Object value, int row, int column)
  {
    // if there is no value, exit out here -- there is no work to do
    if (value == null)
      return;
    subtypeAdvanceSettingsTableSetValues(value, row, column);
  }

  /**
   * @author Chin Seong
   */
  private void subtypeAdvanceSettingsTableSetValues(Object value, int row, int column)
  {
    int rows[] = getSelectedRows();
    int col = convertColumnIndexToModel(column);
    
    String newShadedSubtypeNames[] = new String[rows.length];
    boolean isSettingNewShadedSubtypeName = false;

    if (column == SubtypeAdvanceSettingsTable._SUBTYPE_INDEX)
    {
      if (value instanceof String)
      {
        String valStr = (String)value;
        if (valStr.equalsIgnoreCase(StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY"))) // mixed case
        {
          // the user is not allowed to set the subtype to mixed.  This result is because padtypes are set to different subtypes
          // and the resulting table entry is "mixed".  So, if this is the selection, it's because I show "mixed" in the subtype
          // combo box, and the user selected it.  I show "mixed" because it's current choice, and it's the default selection anyway.
          // So, if they try to set it to "mixed", it's best to ignore this, and treat it as a cancel edit.
          return;
        }
        else if (valStr.equalsIgnoreCase(_NEW_SHADED_SUBTYPE))
        {
          Assert.expect(newShadedSubtypeNames.length == rows.length);
            
          isSettingNewShadedSubtypeName = true;
            
          String prompt = StringLocalizer.keyToString("MMGUI_SUBTYPE_NEW_SHADED_PROMPT_KEY");
          String title = StringLocalizer.keyToString("MMGUI_SUBTYPE_NEW_SHADED_TITLE_KEY");
          String retVal = JOptionPane.showInputDialog(MainMenuGui.getInstance(),
                                                      prompt,
                                                      title,
                                                      JOptionPane.QUESTION_MESSAGE);

          if ((retVal == null) || (retVal.length() == 0)) // the use hit cancel or entered an empty string -- change nothing
            return;
            
          // Run through each row of subtype name and append "_SHADED" on the name
          for(int i = 0; i < rows.length; i++)
          {
            String subtypeName = (String)_subtypeAdvancedSettingsTableModel.getValueAt(rows[i], col);
            String newSubtypeName = subtypeName + "_" + retVal;

            // XCR1740 - Add in ?Append Name? option in Advance Subtype windows, as in ?Modify Subtypes? page
            // by Lim Seng Yew on 27-Dec-2013
            // Put checking if new subtype (with same joint type) already exist.
            com.axi.v810.business.panelDesc.Panel panel = getCurrentBoardType().getPanel();
            // Assuming existing subtype already exist
            JointTypeEnum jointTypeEnum = (JointTypeEnum)_subtypeAdvancedSettingsTableModel.getValueAt(rows[i], _JOINT_TYPE_INDEX);
            if (panel.doesSubtypeShortNameAndJointTypeExist(newSubtypeName, jointTypeEnum))
            {
              String warningMessage = StringLocalizer.keyToString(new LocalizedString("RMGUI_NEW_SUBTYPE_EXIST_ERROR_KEY", 
                                                                  new String[]{newSubtypeName,jointTypeEnum.getName()}));
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                                          warningMessage,
                                                          title,
                                                          JOptionPane.WARNING_MESSAGE);
              return;
            }
            if (panel.isSubtypeNameValid(newSubtypeName))
              newShadedSubtypeNames[i] = newSubtypeName;
            else
              newShadedSubtypeNames[i] = subtypeName;
          }
        }
        else
        {
          return;
        }
      }
    }
    
    //Khaw Chek Hau - XCR3063: Notification Message When Using DRO 
    if(TestExecution.getInstance().isPromptWarningMsgIfModifyDROLevel() && 
       col == SubtypeAdvanceSettingsTable._DRO_LEVEL_INDEX) //dro level
    {
      String choiceMessage = StringLocalizer.keyToString("MMGUI_CHANGE_DRO_LEVEL_WARNING_MESSAGE_KEY");
      int answer = ChoiceInputDialog.showConfirmDialog(MainMenuGui.getInstance(),
                                                       StringUtil.format(choiceMessage, 80),
                                                       StringLocalizer.keyToString("MMGUI_CHANGE_DRO_LEVEL_WARNING_TITLE_KEY"));
      if (answer != JOptionPane.OK_OPTION)
      {
        return;
      }
    }
    
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    List<Subtype> popSubtypesInIL1 = new ArrayList<Subtype>();
    
    if (column == SubtypeAdvanceSettingsTable._INTERGRATION_LEVEL_INDEX && (SignalCompensationEnum)(value) == SignalCompensationEnum.DEFAULT_LOW)
    {
      for(int i = 0; i < rows.length; i++)
      {
        Subtype popSubtype = null;
        
        JointTypeEnum jointType = (JointTypeEnum) _subtypeAdvancedSettingsTableModel.getValueAt(rows[i], 1);
        
        String subtypeName = (String) _subtypeAdvancedSettingsTableModel.getValueAt(rows[i], 0);

        List<Subtype> subtypes = getCurrentBoardType().getPanel().getSubtypes(jointType);
        
        for (Subtype subtype : subtypes)
        {
          if (subtype.getShortName().equals(subtypeName))
          {
            popSubtype = subtype;
            break;
          }
        }
        
        for (ComponentType componentType : popSubtype.getComponentTypes())
        {
          if (componentType.hasCompPackageOnPackage() && popSubtypesInIL1.contains(popSubtype) == false)
          {
            popSubtypesInIL1.add(popSubtype);
          }
        }
      }
    }
    
    if (popSubtypesInIL1.isEmpty() == false)
    {
      int response = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                                                    StringLocalizer.keyToString(new LocalizedString("CAD_POP_SUBTYPE_CHANGE_TO_IL1_WARNING_MESSAGE_KEY", 
                                                                                new Object[]{popSubtypesInIL1.toString()})),
                                                    StringLocalizer.keyToString("CAD_POP_SUBTYPE_MUST_IL2_AND_ABOVE_TITLE_KEY"),
                                                    JOptionPane.YES_NO_OPTION);
      if (response != JOptionPane.YES_OPTION)
      {
        return;
      }
    }
    
    _commandManager.trackState(getCurrentUndoState());
    if (column == SubtypeAdvanceSettingsTable._SUBTYPE_INDEX)
       _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_SUBTYPE_KEY"));
    else if (column == SubtypeAdvanceSettingsTable._USER_GAIN_INDEX)
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_ADVANCE_SETTINGS_SET_USER_GAIN_KEY"));
    else if (column == SubtypeAdvanceSettingsTable._STAGE_SPEED_INDEX)
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_ADVANCE_SETTINGS_SET_STAGE_SPEED_KEY"));
    else if (column == SubtypeAdvanceSettingsTable._INTEFERENCE_COMPENSATION_INDEX)
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_ADVANCE_SETTINGS_SET_ARTIFACT_COMPENSATION_KEY"));
    else if (column == SubtypeAdvanceSettingsTable._INTERGRATION_LEVEL_INDEX)
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_ADVANCE_SETTINGS_SET_INTEGRATION_LEVEL_KEY"));
    else if (column == SubtypeAdvanceSettingsTable._CAMERA_ANGLE_ENABLED_INDEX)
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_ADVANCE_SETTINGS_SET_CAMERA_ENABLED_LIST_KEY"));
    else if (column == SubtypeAdvanceSettingsTable._MAGNIFICATION_TYPE_INDEX)
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_ADVANCE_SETTINGS_SET_MAGNIFICATION_TYPE_KEY"));
    else if (column == SubtypeAdvanceSettingsTable._DRO_LEVEL_INDEX)
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_ADVANCE_SETTINGS_SET_DRO_LEVEL_KEY"));
    for(int i = 0; i < rows.length; i++)
    {
      if (isSettingNewShadedSubtypeName)
        _subtypeAdvancedSettingsTableModel.setValueAt(newShadedSubtypeNames[i], rows[i], col);
      else
        _subtypeAdvancedSettingsTableModel.setValueAt(value, rows[i], col);
    }
    _commandManager.endCommandBlock();
  }

  /**
   * @author Chin Seong
   */
  private UndoState getCurrentUndoState()
  {
    UndoState undoState = new UndoState();
    undoState.setModifySubtypesViewSelectIndex(_COMPONENT_UNDO_INDEX);
    return undoState;
  }
  
  /**
   * @return the _isVariableMagnification
   * @author Chong, Wei Chin
   */
  public boolean isVariableMagnification()
  {
    return _isVariableMagnification;
  }

  /**
   * @param isVariableMagnification the _isVariableMagnification to set
   * @author Chong, Wei Chin
   */
  public void enableVariableMagnification(boolean isVariableMagnification)
  {
    _isVariableMagnification = isVariableMagnification;
  }  
  
   /**
   * @author Chin Seong
   * @return the _currentBoardType
   */
  public BoardType getCurrentBoardType()
  {
    return _currentBoardType;
  }

  /**
   * @author Chin Seong
   * @param currentBoardType the _currentBoardType to set
   */
  public void setCurrentBoardType(BoardType currentBoardType)
  {
    this._currentBoardType = currentBoardType;
  }
  
  /**
   * @author Siew Yeng
   */
  public void setMaxDynamicRangeOptimizationLevel(int maxDroLevel)
  {
    _maxDROLevel = maxDroLevel;
  }  
  
  /**
   * @author Yong Sheng Chuan
   */
  public void enableCameraSelectionColumn(boolean cameraSelection)
  {
    _showCameraSelectionOption = cameraSelection;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setAbleToSelectMultipleSpeed(boolean ableToSelectMultipleSpeed)
  {
    _isAbleToSelectMultipleSpeed = ableToSelectMultipleSpeed;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void updateTableColumnIndex()
  {
    _MAGNIFICATION_TYPE_INDEX = 5;
    _CAMERA_ANGLE_ENABLED_INDEX = 5;
    _DRO_LEVEL_INDEX = 5;
    int hasDroColumn = (_maxDROLevel > 1) ? 1 : 0;
    int hasMagColumn = _isVariableMagnification ? 1 : 0;
    int hasCameraSelectionColumn = (_showCameraSelectionOption) ? 1 : 0;
      
    _CAMERA_ANGLE_ENABLED_INDEX = _INTERGRATION_LEVEL_INDEX + hasCameraSelectionColumn;
    _MAGNIFICATION_TYPE_INDEX = _CAMERA_ANGLE_ENABLED_INDEX + hasMagColumn;
    _DRO_LEVEL_INDEX = _MAGNIFICATION_TYPE_INDEX + hasDroColumn;
    
    _DRO_LEVEL_INDEX = _maxDROLevel > 1 ? _DRO_LEVEL_INDEX : -1;
    _MAGNIFICATION_TYPE_INDEX = _isVariableMagnification ? _MAGNIFICATION_TYPE_INDEX : -1;
    _CAMERA_ANGLE_ENABLED_INDEX = (_showCameraSelectionOption) ? _CAMERA_ANGLE_ENABLED_INDEX : -1;
  }
}
