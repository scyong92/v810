package com.axi.v810.gui.testDev;


import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.TestProgram;
import com.axi.v810.business.testProgram.TestSubProgram;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.ScanPass;
import com.axi.v810.hardware.StagePosition;
import com.axi.v810.util.*;

/**
 *
 * @author chin-seong.kee
 */

public class ScanPathReviewTableModel extends DefaultSortTableModel
{
  private List<StagePosition> _scanPathCoordinate = new ArrayList<StagePosition>();  // List of ScanPasses objects
 
  private Collection<StagePosition> _currentlySelectedPanelCoordinate = new ArrayList<StagePosition>();

  private final String[] _scanPathTableColumns = {StringLocalizer.keyToString("SCAN_PATH_REVIEW_SUMMARY_NUMBERING_KEY"),
                                                  StringLocalizer.keyToString("MMGUI_SETUP_X_LOC_KEY"),
                                                  StringLocalizer.keyToString("MMGUI_SETUP_Y_LOC_KEY"),
                                                  };

  //private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;
  private int _xPosition = 0;
  private int _yPosition = 0;
  private TestProgram _testProgram;
  private TestSubProgram _testSubProgram;
  
  public static final int _SCAN_PATH_NUMBER_OF_LOCATION_INDEX = 0;  
  public static final int _SCAN_PATH_X_LOCATION_INDEX = 1;
  public static final int _SCAN_PATH_Y_LOCATION_INDEX = 2;
 
  /**
   * @author Kee Chin Seong
   */
  public ScanPathReviewTableModel()
  {
    // do nothing
  }

  /**
   * @author Kee Chin Seong
   */
  void clear()
  {
    _scanPathCoordinate.clear();
    _currentlySelectedPanelCoordinate.clear();
    
    _currentSortColumn = 0;
    _currentSortAscending = true;
    
    _testProgram = null;
    _testSubProgram = null;
  }
  
  /**
   * @author Kee Chin Seong
   */
  int getRowForPanelCoordinate(StagePosition stagePosition)
  {
      return _scanPathCoordinate.lastIndexOf(stagePosition);
  }

  List<StagePosition> getStagePositionList()
  {
    if(_scanPathCoordinate.isEmpty() == true)
      return null;
    else
      return _scanPathCoordinate;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void sortColumn(int column, boolean ascending)
  {
    if (column == 0)  // sort on numbering
      Collections.sort( _scanPathCoordinate, new AlphaNumericComparator(ascending));
    else if (column == 1)  // sort on x
      Collections.sort(_scanPathCoordinate, new AlphaNumericComparator(ascending));
    else if (column == 2)  // sort on y
      Collections.sort(_scanPathCoordinate, new AlphaNumericComparator(ascending));
   
    _currentSortColumn = column;
    _currentSortAscending = ascending;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public int getColumnCount()
  {
    return _scanPathTableColumns.length;
  }

  /**
   * @author Kee Chin Seong
   */
  public int getRowCount()
  {
    if (_scanPathCoordinate == null)
      return 0;
    else
      return _scanPathCoordinate.size();
  }

  /**
   * @author Kee Chin Seong
   */
  public Class getColumnClass(int c)
  {
    Assert.expect(c >= 0);
    Object value = this.getValueAt(0,c);  
    return (value == null?Object.class:value.getClass());  
  }

  /**
   * @author Kee Chin Seong
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_scanPathTableColumns[columnIndex];
  }

  /**
   * @author Kee Chin Seong
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    if (columnIndex == 0)
      return false;
    if (columnIndex == 1)
      return false;
    if (columnIndex == 2)
    return false;
    
    // if nothing selected, nothing can be editable
    if (_scanPathCoordinate.isEmpty())
      return false;

    // if the current component is NOT selected, then it's not editable.
    // need to make the check up front before all the more detailed checks
    if (_scanPathCoordinate.contains(getValueAt(rowIndex, 0)) == false)
      return false;

    return true;
  }

  /**
   * @author Kee Chin Seong
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    if (_scanPathCoordinate.size() > 0)
    {
        if (_scanPathCoordinate.size() > rowIndex)
        {      
            StagePosition stagePosition = _scanPathCoordinate.get(rowIndex);

            if (columnIndex == 0) // number
              return rowIndex + 1;
            else if (columnIndex == 1) // x
            {
              return stagePosition.getXInNanometers();
            }
            else if (columnIndex == 2) // y
            {
               return  stagePosition.getYInNanometers();
            }
        }
    }
    return null;
  }

  /**
   * @author Kee Chin Seong
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    //do nothing for now
  }
  
   /**
   * @author Kee Chin Seong
   */
  void showLocationErrorDialog(String message)
  {
    MessageDialog.showErrorDialog(
        MainMenuGui.getInstance(),
        message,
        StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
        true);
  }
  
  /**
   * @author Chnee Khang Wah
   */
  void populateWithTestProgramScanPath(TestProgram testProgram)
  { 
    _testProgram = testProgram;
    
    //Always make sure the Scan Path Coordinate is empty only populate it
    if(_scanPathCoordinate.isEmpty() == false)
    {
      for(StagePosition sp : _scanPathCoordinate)
      {
        sp = null;
      }
      _scanPathCoordinate.clear();
    }
    
    if(testProgram != null)
    {
      for(TestSubProgram testSubProgram : testProgram.getFilteredScanPathTestSubPrograms())
      {
        for(ScanPass sp : testSubProgram.getScanPasses())
        {
           _scanPathCoordinate.add(sp.getStartPointInNanoMeters());
           _scanPathCoordinate.add(sp.getEndPointInNanoMeters());
        }
      }
    }
    //Collections.sort(_scanPathCoordinate, new AlphaNumericComparator());
    fireTableDataChanged();
  }
  
   /**
   * @author Kee Chin Seong
   */
  void populateWithTestSubProgramScanPath(TestSubProgram testSubProgram)
  { 
    _testSubProgram = testSubProgram;
    
    //Always make sure the Scan Path Coordinate is empty only populate it
    if(_scanPathCoordinate.isEmpty() == false)
    {
      for(StagePosition sp : _scanPathCoordinate)
      {
        sp = null;
      }
      _scanPathCoordinate.clear();
    }
    
    if(testSubProgram != null)
    {
      for(ScanPass sp : testSubProgram.getScanPasses())
      {
         _scanPathCoordinate.add(sp.getStartPointInNanoMeters());
         _scanPathCoordinate.add(sp.getEndPointInNanoMeters());
      }
    }
    //Collections.sort(_scanPathCoordinate, new AlphaNumericComparator());
    fireTableDataChanged();
  }
  
   /**
   * @author Kee Chin Seong
   */
  void setSelectedPanelCoordinate(Collection<StagePosition> StagePositionCoordinates)
  {
    Assert.expect(StagePositionCoordinates != null);
    _currentlySelectedPanelCoordinate = StagePositionCoordinates;

  }

  StagePosition getStageStartingPositionAt(int row)
  {
    return _scanPathCoordinate.get(row);  
  }

   /**
   * @author Kee Chin Seong
   */
  public int getXPosition()
  {
    return _xPosition;
  }

   /**
   * @author Kee Chin Seong
   */
  public int getYPosition()
  {
    return _yPosition;
  }

}
