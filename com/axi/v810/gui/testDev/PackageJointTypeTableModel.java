package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: PackageJointTypeTableModel</p>
 *
 * <p>Description:  This class is the table model for the package and joint type table in Verify CAD.
 * This table model is responsible for getting the data as needed. There will be 2 columns:<br>
 * Package<br>
 * Joint Type<br>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class PackageJointTypeTableModel extends ModifySubtypesPackageTableModel
{
  private final String[] _packageFamilyTableColumnNames =
  {
    StringLocalizer.keyToString("MMGUI_GROUPINGS_PACKAGE_KEY"), // "Package"
    StringLocalizer.keyToString("ATGUI_FAMILY_KEY"),            // "Joint Type"
  };

  // side choices
  private final String _TOP_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_TOP_LABEL_KEY");
  private final String _BOTTOM_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_BOTTOM_LABEL_KEY");

  private List<CompPackage> _packages = new ArrayList<CompPackage>();
  private CompPackage _currentSelectedPackage;
  private final String _MIXED = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");

  // default to sort by component, ascending
  private int _lastSortColumn = 0;
  private boolean _lastSortWasAscending = true;

  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  
  private boolean _isCellEditable = true;

  /**
   * @author George Booth
   */
  public PackageJointTypeTableModel()
  {
    // do nothing
  }

  void setDefaultSortOrder()
  {
    _lastSortColumn = 0;
    _lastSortWasAscending = true;
  }

  /**
   * @author George Booth
   */
  public void sortColumn(int column, boolean ascending)
  {
    _lastSortColumn = column;
    _lastSortWasAscending = ascending;

    if(column == 0) // package name
      Collections.sort(_packages, new CompPackageComparator(ascending, CompPackageComparatorEnum.LONG_NAME));
    else  // joint type
      Collections.sort(_packages, new CompPackageComparator(ascending, CompPackageComparatorEnum.JOINT_TYPE));
  }

  /**
   * @author George Booth
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    CompPackage compPackage = _packages.get(rowIndex);
    if(columnIndex == 0)
    {
      return compPackage.toString();
    }
    else if(columnIndex == 1)
    {
      // determine if the compPackage is an orphan.  This can happen
      // if the bus. layer removes a package, and this is called before we're
      // notified and remove it from our list
//      if (compPackage.getPackagePinsUnsorted().isEmpty())
//        return "";
//      if (compPackage.usesOneJointTypeEnum() == false)
//        return _MIXED;
//      else
//        return compPackage.getJointTypeEnum();
      return getJointTypeName(compPackage);
    }
    else
    {
      Assert.expect(false);
      return null;
    }
  }

  private String getJointTypeName(CompPackage compPackage)
  {
    // determine if the compPackage is an orphan.  This can happen
    // if the bus. layer removes a package, and this is called before we're
    // notified and remove it from our list
    if (compPackage.getPackagePinsUnsorted().isEmpty())
    {
      return "";
    }
    if (compPackage.usesOneJointTypeEnum() == false)
    {
      return _MIXED;
    }
    else
    {
      return compPackage.getJointTypeEnum().getName();
    }
  }

  /**
   * @author George Booth
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_packageFamilyTableColumnNames[columnIndex];
  }

  /**
   * @author George Booth
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    if (columnIndex == 0) // column 0 is package name -- not editable
      Assert.expect(false);
    else if (columnIndex == 1)
    {
      JointTypeEnum newJointTypeEnum = (JointTypeEnum)aValue;

      // only allow this change if the throughhole/smt land pattern is compatible with the new joint type
      CompPackage compPackage = _packages.get(rowIndex);
      LandPattern landPattern = compPackage.getLandPattern();
      if (landPattern.getPadTypeEnum().equals(PadTypeEnum.THROUGH_HOLE) == false) // not all through hole
      {
        if (ImageAnalysis.requiresThroughHoleLandPatternPad(newJointTypeEnum))
        {
          // give error, and do not do the operation
          MessageDialog.showErrorDialog(
              MainMenuGui.getInstance(),
              StringLocalizer.keyToString("MMGUI_GROUPINGS_JOINT_TYPE_MUST_BE_THROUGHHOLE_KEY"),
              StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
              true);
          return;
        }
      }

      try
      {
        _commandManager.execute(new CompPackageSetJointTypeCommand(_packages.get(rowIndex), newJointTypeEnum, true));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    else
      Assert.expect(false); // only column 1 is editable
  }

  /**
   * @author George Booth
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    if(_isCellEditable == false)
      return false;
    // column 0 is package name - not editable
    if (columnIndex == 0)
      return false;

    // fields are editable only if package is selected
    if (_currentSelectedPackage != null &&
        getPackageAt(rowIndex) == (_currentSelectedPackage))
    {
      // column 1 is joint type - editable only if not MIXED
      if (columnIndex == 1)
      {
        if (_currentSelectedPackage.usesOneJointTypeEnum())
          return true;
      }
    }
    return false;
  }

  /**
   * @author George Booth
   */
  public int getRowCount()
  {
    if (_packages == null)
      return 0;
    else
      return _packages.size();
  }

  /**
   * @author George Booth
   */
  public int getColumnCount()
  {
    return _packageFamilyTableColumnNames.length;
  }

  /**
   * @author George Booth
   */
  public CompPackage getPackageAt(int selectedPackageNumber)
  {
    Assert.expect(selectedPackageNumber < _packages.size());
    return _packages.get(selectedPackageNumber);
  }

  /**
   * @author George Booth
   */
  void clear()
  {
    _packages.clear();
    _currentSelectedPackage = null;
  }

  /**
   * @author George Booth
   */
  public void populateWithPanelData(Board board, BoardType boardType, String side)
  {
    Assert.expect(board != null);
    Assert.expect(boardType != null);
    Assert.expect(side != null);

    _packages.clear();

    if (side.equalsIgnoreCase(_TOP_SIDE))
    {
      if (board.topSideBoardExists())
      {
        SideBoardType sideBoard = board.getTopSideBoard().getSideBoardType();
        _packages.addAll(sideBoard.getCompPackages());
      }
    }
    else if (side.equalsIgnoreCase(_BOTTOM_SIDE))
    {
      if (board.bottomSideBoardExists())
      {
        SideBoardType sideBoard = board.getBottomSideBoard().getSideBoardType();
        _packages.addAll(sideBoard.getCompPackages());
      }
    }
    else
    {
      _packages.addAll(boardType.getCompPackages());
    }

    sortColumn(_lastSortColumn, _lastSortWasAscending);
    fireTableDataChanged();
  }

  /**
   * @author Andy Mechtenberg
   */
  void updateSpecificPackage(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);
    int packageRow = _packages.indexOf(compPackage);
    fireTableRowsUpdated(packageRow, packageRow);
  }

  /**
   * Get the row for a specified compPackage. If the compPackage is null or
   * the compPackage is not found, default to select the first row by default.
   * @return row for compPackage or 0 if not found
   * @author George Booth
   */
  int getRowForCompPackage(CompPackage compPackage)
  {
    Assert.expect(_packages.size() > 0);
    // we can always select row 0
    if (compPackage == null)
    {
      return 0;
    }
    // search by shortName and joinType
    // (longName includes jointType which might have changed)
    String packageName = compPackage.getShortName();
    String jointTypeName = getJointTypeName(compPackage);
    for (int row = 0; row < _packages.size(); row++)
    {
      CompPackage thisPackage = _packages.get(row);
      if (thisPackage.getShortName().equals(packageName) &&
          getJointTypeName(thisPackage).equals(jointTypeName))
      {
        return row;
      }
    }
    // not found
    return 0;
  }

  /**
   * @author George Booth
   */
  void setSelectedPackage(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);
    _currentSelectedPackage = compPackage;
  }

  /**
   * @author George Booth
   */
  CompPackage setSelectedPackageAt(int index)
  {
    Assert.expect(index >= 0 && index < _packages.size());
    _currentSelectedPackage = _packages.get(index);
    return _currentSelectedPackage;
  }

  /**
   * @author George Booth
   */
  CompPackage getSelectedPackage()
  {
    return _currentSelectedPackage;
  }

  /**
   * @author George Booth
   */
  public void clearSelectedPackage()
  {
    _currentSelectedPackage = null;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setIsCellEditable(boolean isCellEditable)
  {
     _isCellEditable = isCellEditable;
  }

  /**
   * @author George Booth
   */
  public int getRowForDataStartingWith(String key)
  {
    key = key.toLowerCase();
    int index = -1;
    for (CompPackage compPackage : _packages)
    {
      index++;
      String packageName = compPackage.toString().toLowerCase();
      if (packageName.startsWith(key))
      {
        return index;
      }
    }
    return -1;
  }
  
  /**
   * @author Khaw Chek Hau
   * Adjust CAD - Joint Type sorting restored to default when re-subtyping single pad / mix pad.
   */
  void updateAllPackages(BoardType selectedBoardType)
  {
    _packages = selectedBoardType.getCompPackages();
    sortColumn(_lastSortColumn, _lastSortWasAscending);
    fireTableDataChanged();
  }
}
