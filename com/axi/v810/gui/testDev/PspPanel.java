/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.gui.testDev;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;


/**
 * PSP (Surface Map) Abstract Task Panel.
 * @author Jack Hwee
 */
public class PspPanel extends AbstractTaskPanel implements Observer
{

  private MainMenuGui _mainUI = null;
  private TestDev _testDev = null;
  private ImageManagerPanel _imagePanel;
  private Project _project = null;
  private boolean _populateProjectData = true;
  private static boolean _isInAutoPopulateMode = false;
 
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private JTabbedPane _pspPanelTabbedPane = new JTabbedPane();
  private PspAutoPopulatePanel _pspAutoPopulatePanel = null;
  
  private BorderLayout _pspPanelBorderLayout = new BorderLayout();

  public PspPanel()
  {
    _pspAutoPopulatePanel = new PspAutoPopulatePanel();
    jbInit();
  }

  /**
   * @author Andy Mechtenberg
   */
  public PspPanel(TestDev testDev, ImageManagerPanel imagePanel)
  {
    super(testDev);
    Assert.expect(testDev != null);
    Assert.expect(imagePanel != null);
    _testDev = testDev;
    _imagePanel = imagePanel;
    _mainUI = _testDev.getMainUI();

    _pspAutoPopulatePanel = new PspAutoPopulatePanel();
    jbInit();
  }

  /**
   * @author Andy Mechtenberg
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof SelectedRendererObservable)
          handleSelectionEvent(object);
        else if (observable instanceof ProjectObservable)
          handleProjectEvent(object);
        else if (observable instanceof GuiObservable)
          handleGuiEvent((GuiEvent)object);
        else if (observable instanceof com.axi.v810.gui.undo.CommandManager)
          handleUndoState(object);
        else
          Assert.expect(false);
      }
    });
  }
  
  /**
   * @author Jack Hwee
   */
  private void handleUndoState(final Object object)
  {
    // do nothing 
  }
  
  /**
   * @author Andy Mechtenberg
   */
  private void handleSelectionEvent(Object object)
  {
    // do nothing
  }
  
  /**
   * @author Andy Mechtenberg
   */
  private void handleProjectEvent(Object object)
  {
    // do nothing
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleGuiEvent(GuiEvent guiEvent)
  {
    // do nothing
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    setLayout(_pspPanelBorderLayout);
    
    _pspPanelBorderLayout.setVgap(10);
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    add(_pspPanelTabbedPane, BorderLayout.CENTER);
    
    _pspPanelTabbedPane.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        pspPanelTabbedPane_stateChanged(e);
      }
    });
    _pspPanelTabbedPane.add(_pspAutoPopulatePanel, StringLocalizer.keyToString("PSP_AUTOPOPULATE_TAB_KEY"));
  }

  /**
   * @author Ying-Huan.Chu
   */
  void updateCadGraphics()
  {
    if (_project.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      _testDev.switchToPanelGraphics();
    }
    else
    {
      _testDev.initGraphicsToBoard();
      _testDev.changeGraphicsToBoard(_project.getPanel().getBoards().get(0));
      _testDev.switchToBoardGraphics();
    }
    _testDev.showCadWindow();
  }

  /**
   * @author Jack Hwee
   */
  public void unpopulate()
  {
    _pspAutoPopulatePanel.unpopulate();
    _project = null;
    ProjectObservable.getInstance().deleteObserver(this);
   _guiObservable.deleteObserver(this);
  }

  /**
   * @author Jack Hwee
   */
  private void pspPanelTabbedPane_stateChanged(ChangeEvent e)
  {
    if (!_active)
      return;

    if (_pspAutoPopulatePanel.isReadyToFinish())
    {
      _pspAutoPopulatePanel.finish();
    }
    if (_pspPanelTabbedPane.getSelectedIndex() == 0) // Auto-Populate tab
    {
      _isInAutoPopulateMode = true;
      _pspAutoPopulatePanel.start(_imagePanel);
      _guiObservable.deleteObserver(this);
    }
    else
    {
      _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().clearMouseEventsInSurfaceMappingScreen();  
      if (_project.getPanel().getPanelSettings().isPanelBasedAlignment())
      {
        _testDev.switchToPanelGraphics();
      }
      else
      {
        _testDev.initGraphicsToBoard();
        _testDev.changeGraphicsToBoard(_project.getPanel().getBoards().get(0));
        _testDev.switchToBoardGraphics();
      }
      _testDev.showCadWindow();
      _guiObservable.addObserver(this);
      _isInAutoPopulateMode = false;
    }
  }
   
   /**
    * @author George Booth
    */
   public void populateWithProjectData()
   {
     validate();
     repaint();
   }

  /**
   * @author Andy Mechtenberg
   */
  public void populateWithProjectData(Project project)
  {
    Assert.expect(project != null);

    _project = project;

    ProjectObservable.getInstance().addObserver(this);
    // add this as an observer of the gui
    _guiObservable.addObserver(this);

    if (_active)
    {
      populateWithProjectData();
      _populateProjectData = true;
    }
    else
      _populateProjectData = true;
    
    _pspAutoPopulatePanel.populateWithProjectData(project);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static boolean isInAutoPopulateMode()
  {
    return _isInAutoPopulateMode;
  }

  /**
   * Task is ready to start
   * @author George Booth
   * @author Ying-Huan.Chu
   */
  public void start()
  {
    com.axi.util.TimerUtil screenTimer = new com.axi.util.TimerUtil();
    screenTimer.start();
    
    // set up for custom menus and tool bar
    super.startMenusAndToolBar();
    
    if (_populateProjectData)
    {
      populateWithProjectData();
    }
    _populateProjectData = false;

    _testDev.switchToPanelGraphics();
    _testDev.showCadWindow();
    // set up for custom menus and tool bar
    _active = true;
    _testDev.showCadWindow();

    // generate test program if needed; notify user it is happening
    Project project = Project.getCurrentlyLoadedProject();
    if (project.isTestProgramValid() == false)
    {
      _mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_TESTABILITY_NEEDS_TEST_PROGRAM_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_TDT_TITLE_GROUPINGS_KEY"),
                                                    project);
    }
    
    _pspPanelTabbedPane.setSelectedIndex(0); 
    _isInAutoPopulateMode = true; // this is true in PspPanel's start() function as we have only Auto Populate tab in Surface Map for now.
    _testDev.changeToSurfaceMappingToolBar(true);
    _pspAutoPopulatePanel.start(_imagePanel);

    _guiObservable.deleteObserver(this);
    
    screenTimer.stop();
  }

  /**
   * Can the environment be swapped out
   * @return true if environment can be swapped
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * The environment is about to be swapped out and should perform cleanup
   * @author George Booth
   */
  public void finish()
  {
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
    _imagePanel.finishVerificationImagePanel();
    _testDev.clearPackageGraphics();
    _testDev.setActiveTunerComponentType();

    _active = false;
    _pspAutoPopulatePanel.finish();
    _testDev.changeToSurfaceMappingToolBar(false);
  }
}
