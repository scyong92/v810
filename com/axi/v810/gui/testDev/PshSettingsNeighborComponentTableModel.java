package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author siew-yeng.phang
 */
public class PshSettingsNeighborComponentTableModel extends DefaultSortTableModel
{
  //String labels retrieved from the localization properties file
  private static final String _NEIGHBOR_COMPONENT_NAME_LABEL = StringLocalizer.keyToString("CAD_PSH_NEIGHBOUR_COMPONENT_TABLE_HEADER_KEY");
  private static final String _JOINT_TYPE_LABEL = StringLocalizer.keyToString("CAD_PSH_JOINT_TYPE_TABLE_HEADER_KEY");
  private static final String _SUBTYPE_NAME_LABEL = StringLocalizer.keyToString("CAD_PSH_SUBTYPE_TABLE_HEADER_KEY");
  private static final String _PSH_COMPONENT_NAME_LABEL = StringLocalizer.keyToString("CAD_PSH_NEIGHBOUR_COMPONENT_PSH_COMPONENT_TABLE_HEADER_KEY");

  private String[] _columnLabels = {_NEIGHBOR_COMPONENT_NAME_LABEL,
                                    _JOINT_TYPE_LABEL,
                                    _SUBTYPE_NAME_LABEL,
                                    _PSH_COMPONENT_NAME_LABEL};

  private List<Pair<ComponentType, ComponentType>> _neighbourComponentTypeToPshComponentTypeList = new ArrayList<Pair<ComponentType, ComponentType>>();
  
  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;
  
  private static ComponentTypeComparator _componentTypeComparator = new ComponentTypeComparator(true, ComponentTypeComparatorEnum.REFERENCE_DESIGNATOR);
  
  /**
   * Constructor.
   * @author Siew Yeng
   */
  public PshSettingsNeighborComponentTableModel()
  {
    //do nothing
  }
  
  /**
   * @author Siew Yeng
   */
  void clearData()
  {
    _neighbourComponentTypeToPshComponentTypeList.clear();
    fireTableDataChanged();
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Siew Yeng
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @author Siew Yeng
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * @author Siew Yeng
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    Pair<ComponentType, ComponentType> neighbourPair = _neighbourComponentTypeToPshComponentTypeList.get(rowIndex);

    //get the appropriate data item that corresponds to the columnIndex
    switch(columnIndex)
    {
      case PshSettingsNeighborComponentTable._NEIGHBOUR_COMPONENT_NAME_INDEX:
        value = neighbourPair.getFirst();
        break;
      case PshSettingsNeighborComponentTable._JOINT_TYPE_INDEX:
        value = TestDev.getComponentDisplayJointType(neighbourPair.getFirst());
        break;
      case PshSettingsNeighborComponentTable._SUBTYPE_INDEX:
        value = TestDev.getComponentDisplaySubtype(neighbourPair.getFirst());
        break;
      case PshSettingsNeighborComponentTable._PSH_COMPONENT_INDEX:
        value = neighbourPair.getSecond();
        break;
      default:
      {
        Assert.expect(false);
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Siew Yeng
   */
  public int getRowCount()
  {
    if(_neighbourComponentTypeToPshComponentTypeList != null)
      return _neighbourComponentTypeToPshComponentTypeList.size();

    return 0;
  }

  /**
   * Overridden method.
   * @author Siew Yeng
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    
    return false;
  }
  
  /**
   * @author Siew Yeng
   */
  void setData(PshSettings pshSettings, List<ComponentType> pshComponentTypes)
  {
    Assert.expect(pshSettings != null);
    _neighbourComponentTypeToPshComponentTypeList.clear();
    
    for(ComponentType componentType : pshComponentTypes)
    {
      if( pshSettings.getPshComponentTypeToNeighbourComponentTypesMap().get(componentType) != null)
      {
        for(ComponentType neighbour : pshSettings.getPshComponentTypeToNeighbourComponentTypesMap().get(componentType))
        {
          _neighbourComponentTypeToPshComponentTypeList.add(new Pair(neighbour, componentType));
        }
      }
    }
    sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableDataChanged();
  }

  /**
   * @author Siew Yeng
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
  }
  
  /**
   * @author Siew Yeng
   */
  public void sortColumn(int column, boolean ascending)
  {
    _currentSortColumn = column;
    _currentSortAscending = ascending;

    Collections.sort(_neighbourComponentTypeToPshComponentTypeList, new Comparator<Pair<ComponentType, ComponentType>>() 
    {
      public int compare(Pair<ComponentType, ComponentType> lhs, Pair<ComponentType, ComponentType> rhs)
      {
        ComponentType lhsNeighborCompType = lhs.getFirst();
        ComponentType rhsNeighborCompType = rhs.getFirst();

        ComponentType lhsPshCompType = lhs.getSecond();
        ComponentType rhsPshCompType = rhs.getSecond();
        
        switch (_currentSortColumn) 
        {
          case 0:
            _componentTypeComparator.setAscending(_currentSortAscending);
            _componentTypeComparator.setComponentTypeComparatorEnum(ComponentTypeComparatorEnum.REFERENCE_DESIGNATOR);
            return _componentTypeComparator.compare(lhsNeighborCompType, rhsNeighborCompType);
          case 1:
            _componentTypeComparator.setAscending(_currentSortAscending);
            _componentTypeComparator.setComponentTypeComparatorEnum(ComponentTypeComparatorEnum.JOINT_TYPE);
            return _componentTypeComparator.compare(lhsNeighborCompType, rhsNeighborCompType);
          case 2:
            _componentTypeComparator.setAscending(_currentSortAscending);
            _componentTypeComparator.setComponentTypeComparatorEnum(ComponentTypeComparatorEnum.SUBTYPE);
            return _componentTypeComparator.compare(lhsNeighborCompType, rhsNeighborCompType);
          case 3:
            _componentTypeComparator.setAscending(_currentSortAscending);
            _componentTypeComparator.setComponentTypeComparatorEnum(ComponentTypeComparatorEnum.REFERENCE_DESIGNATOR);
            return _componentTypeComparator.compare(lhsPshCompType, rhsPshCompType);
          default:
            break;
        }
        
        return 0;
      }
    });
  } 
  
  /**
   * @author Siew Yeng
   */
  public ComponentType getComponentTypeAt(int rowIndex)
  {
    return getNeighbourAndPshComponentTypePairAt(rowIndex).getFirst();
  }
  
  /**
   * @author Siew Yeng
   */
  public Pair<ComponentType, ComponentType> getNeighbourAndPshComponentTypePairAt(int rowIndex)
  {
    Assert.expect(_neighbourComponentTypeToPshComponentTypeList != null);
    
    return _neighbourComponentTypeToPshComponentTypeList.get(rowIndex);
  }
  
  /**
   * @author Siew Yeng
   */
  public int getRowFoNeighbourAndPshComponentTypePair(Pair<ComponentType, ComponentType> data)
  {
    Assert.expect(data != null);
    Assert.expect(_neighbourComponentTypeToPshComponentTypeList != null);
    
    return _neighbourComponentTypeToPshComponentTypeList.indexOf(data);
  }
  
  /**
   * @author Siew Yeng
   */
  public int getRowForNeighbour(ComponentType neighbour)
  { 
    Assert.expect(neighbour != null);
    Assert.expect(_neighbourComponentTypeToPshComponentTypeList != null);
    
    for(Pair<ComponentType, ComponentType> pair : _neighbourComponentTypeToPshComponentTypeList)
    {
      if(pair.getFirst().equals(neighbour))
        return _neighbourComponentTypeToPshComponentTypeList.indexOf(pair);
    }
    return -1;
  }
  
  /**
   * @author Siew Yeng
   */
  public void addNeighbourComponentType(ComponentType neighbour, ComponentType pshComponent)
  {
    Assert.expect(neighbour != null);
    Assert.expect(pshComponent != null);
    Assert.expect(_neighbourComponentTypeToPshComponentTypeList != null);
    
    Pair<ComponentType, ComponentType> newNeighbor = new Pair(neighbour, pshComponent);
    
    if(_neighbourComponentTypeToPshComponentTypeList.contains(newNeighbor) == false)
      _neighbourComponentTypeToPshComponentTypeList.add(newNeighbor);
    
    fireTableDataChanged();
    sortColumn(_currentSortColumn, _currentSortAscending);
  }
  
  /**
   * @author Siew Yeng
   */
  public void removeNeighbourComponentTypes(int[] rows)
  {
    Assert.expect(_neighbourComponentTypeToPshComponentTypeList != null);
    
    List<Pair<ComponentType, ComponentType>> componentToRemove = new ArrayList();
    for(int row: rows)
    {
      componentToRemove.add(_neighbourComponentTypeToPshComponentTypeList.get(row));
    }
    _neighbourComponentTypeToPshComponentTypeList.removeAll(componentToRemove);
  }
}
