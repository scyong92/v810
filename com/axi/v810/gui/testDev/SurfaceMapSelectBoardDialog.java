package com.axi.v810.gui.testDev;

import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

/**
 * This dialog box allows user to select boards to apply OpticalCameraRectangles in Surface Map.
 * 
 * @author Ying-Huan.Chu
 */
public class SurfaceMapSelectBoardDialog extends JDialog
{
  private PspAutoPopulatePanel _pspAutoPopulatePanel;
  private BoardType _boardType;
  
  private JLabel _selectBoardLabel = new JLabel();
  
  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelLayout = new BorderLayout();
  
  private JPanel _checkBoxPanel = new JPanel();
  private BorderLayout _checkBoxPanelLayout = new BorderLayout();
  private JCheckBox _selectAllCheckBox = new JCheckBox();
  private JPanel _boardCheckBoxPanel = new JPanel();
  private GridLayout _boardCheckBoxPanelLayout = new GridLayout(0,2);
  private java.util.List<JCheckBox> _boardCheckBoxes = new ArrayList<>();
  
  private JPanel _buttonPanel = new JPanel();
  private FlowLayout _buttonPanelLayout = new FlowLayout();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  
  private boolean _isUserClickOk = false;
  
  /**
   * @author Ying-Huan.Chu
   */
  public SurfaceMapSelectBoardDialog(BoardType boardType, JFrame parent, String title)
  {
    super(parent, title, true);
    
    _pspAutoPopulatePanel = PspAutoPopulatePanel.getInstance();
    _boardType = boardType;
    
    jbInit();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void jbInit()
  {
    _selectBoardLabel.setText("<html><b>Please select the board(s) you wish <br> to apply Optical Camera Rectangles to: </b></html>");
    _boardCheckBoxPanel.setLayout(_boardCheckBoxPanelLayout);
    _boardCheckBoxPanel.setBorder(BorderFactory.createEmptyBorder(10,10,20,10));
    populateCheckBoxPanel();
    
    _selectAllCheckBox.setText(StringLocalizer.keyToString("GUI_SELECT_SELECT_ALL_KEY"));
    _selectAllCheckBox.setSelected(shouldCheckSelectAllCheckBox());
    _selectAllCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectAllCheckBox_actionPerformed();
      }  
    });
    
    _checkBoxPanel.setLayout(_checkBoxPanelLayout);
    _checkBoxPanel.setBorder(BorderFactory.createEmptyBorder(10,10,20,10));
    _checkBoxPanel.add(_selectAllCheckBox, BorderLayout.NORTH);
    _checkBoxPanel.add(_boardCheckBoxPanel, BorderLayout.CENTER);
    
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new ActionListener() 
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed();
      }
    });
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new ActionListener() 
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });
    _buttonPanel.setLayout(_buttonPanelLayout);
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,10,20,10));
    _buttonPanel.add(_okButton);
    _buttonPanel.add(_cancelButton);
    
    _mainPanel.setLayout(_mainPanelLayout);
    _mainPanel.setBorder(BorderFactory.createEmptyBorder(10,10,20,10));
    _mainPanel.add(_selectBoardLabel, BorderLayout.NORTH);
    _mainPanel.add(_checkBoxPanel, BorderLayout.CENTER);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    add(_mainPanel);
    pack();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void populateCheckBoxPanel()
  {
    for (Board board : _boardType.getBoards())
    {
      JCheckBox boardCheckBox = new JCheckBox();
      boardCheckBox.setText(board.getName());
      if (_pspAutoPopulatePanel.getBoardsToApplyOpticalCameraRectangles().contains(board))
      {
        boardCheckBox.setSelected(true);
      }
      _boardCheckBoxes.add(boardCheckBox);
    }
    for (JCheckBox checkBox : _boardCheckBoxes)
    {
      _boardCheckBoxPanel.add(checkBox);
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private boolean shouldCheckSelectAllCheckBox()
  {
    for (JCheckBox checkBox : _boardCheckBoxes)
    {
      if (checkBox.isSelected() == false)
      {
        return false;
      }
    }
    return true;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void okButton_actionPerformed()
  {
    java.util.List<Board> boards = new ArrayList<>();
    for (JCheckBox checkbox : _boardCheckBoxes)
    {
      if (checkbox.isSelected())
      {
        for (Board board : _boardType.getBoards())
        {
          if (board.getName().equals(checkbox.getText()))
          {
            boards.add(board);
          }
        }
      }
    }
    if (boards.isEmpty())
    {
      JOptionPane.showMessageDialog(_pspAutoPopulatePanel,
                  StringLocalizer.keyToString("PSP_BOARD_CANNOT_EMPTY_MESSAGE_KEY"),
                  StringLocalizer.keyToString("PSP_BOARD_CANNOT_EMPTY_TITLE_KEY"),
                  JOptionPane.ERROR_MESSAGE);
    }
    else
    {
      _pspAutoPopulatePanel.setBoardsToApplyOpticalCameraRectangle(boards);
      _isUserClickOk = true;
      dispose();
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void selectAllCheckBox_actionPerformed()
  {
    if (_selectAllCheckBox.isSelected()) // Select All
    {
      for (JCheckBox checkbox : _boardCheckBoxes)
      {
        if (checkbox.isEnabled())
        {
          checkbox.setSelected(true);
        }
      }
    }
    else // Unselect All 
    {
      for (JCheckBox checkbox : _boardCheckBoxes)
      {
        if (checkbox.isEnabled())
        {
          checkbox.setSelected(false);
        }
      }
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean isUserClickedOk()
  {
    return _isUserClickOk;
  }
}
