/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.testDev;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.MessageDialog;
import com.axi.v810.gui.mainMenu.MainMenuGui;
import com.axi.v810.gui.undo.SubtypeSetNameCommand;
import com.axi.v810.util.*;

/**
 * @author chin-seong.kee
 */
public class ModifySubtypesUnTestableReasonsTable extends JSortTable
{
  public static final int _JOINT_INDEX = 1;
  public static final int _JOINT_TYPE_INDEX = 2;
  public static final int _SUBTYPE_INDEX = 3;
  public static final int _UNTESTABLE_REASONS_INDEX = 4;
  public static final int _BOARD_NAME_INDEX = 0;

  private ModifySubtypesUnTestableReasonsTableModel _unTestableReasonsTableModel;

  private BoardType _currentBoardType;

  private static final int _COMPONENT_UNDO_INDEX = 0;

  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  /**
   * @param model
   */
  public ModifySubtypesUnTestableReasonsTable(ModifySubtypesUnTestableReasonsTableModel model)
  {
    Assert.expect(model != null);
    _unTestableReasonsTableModel = model;
    super.setModel(model);
  }

  /**
   * @return
   */
  public void setModel(ModifySubtypesUnTestableReasonsTableModel unTestableReasonsTableModel)
  {
    Assert.expect(unTestableReasonsTableModel != null);
    _unTestableReasonsTableModel = unTestableReasonsTableModel;
    super.setModel(unTestableReasonsTableModel);
  }

  /**
   * @return
   */
  public ModifySubtypesUnTestableReasonsTableModel getTableModel()
  {
    Assert.expect(_unTestableReasonsTableModel != null);
    return _unTestableReasonsTableModel;
  }
 
  /**
   * @return the _currentBoardType
   */
  public BoardType getCurrentBoardType()
  {
    return _currentBoardType;
  }

  /**
   * @param currentBoardType the _currentBoardType to set
   */
  public void setCurrentBoardType(BoardType currentBoardType)
  {
    this._currentBoardType = currentBoardType;
  }

  /**
   * @author Erica Wheatcroft
   */
  private UndoState getCurrentUndoState()
  {
    UndoState undoState = new UndoState();
    undoState.setModifySubtypesViewSelectIndex(_COMPONENT_UNDO_INDEX);
    return undoState;
  }
}
