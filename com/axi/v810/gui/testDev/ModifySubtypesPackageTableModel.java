package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.*;

/**
 * This class is the data model for the package table for the Groupings panel and it also
 * converts to the component table model in the groupings panel
 * @author Andy Mechtenberg
 * @author Erica Wheatcroft
 */
public class ModifySubtypesPackageTableModel extends DefaultSortTableModel
{
  private List<CompPackage> _packages = new ArrayList<CompPackage>(); // list of CompPackage objects

  // this is a list of all selected packages -- used to determine if a row is editable
  private List<CompPackage> _currentlySelectedPackages = new ArrayList<CompPackage>();  // list of CompPackage objects that are currently selected

  // this object is the "base" package -- it's data is shown in graphics
  private CompPackage _currentSelectedPackage;

  private int _sortedColumn = 0;
  private boolean _ascendingSort = true;

  private final String[] _packageTableColumns = {StringLocalizer.keyToString("MMGUI_GROUPINGS_PACKAGE_KEY"),
                                                 StringLocalizer.keyToString("ATGUI_FAMILY_KEY"),
                                                 StringLocalizer.keyToString("MMGUI_GROUPINGS_JOINT_HEIGHT_KEY")};

  private final String _MIXED = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");

  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  private Project _project;
  
  private static CompPackageComparator _compPackageComparator;
  
  static
  {
    _compPackageComparator = new CompPackageComparator(true, CompPackageComparatorEnum.LONG_NAME);
  }

  /**
   * @author Andy Mechtenberg
   */
  public ModifySubtypesPackageTableModel()
  {
    // do nothing
  }

  /**
   * @author Andy Mechtenberg
   */
  void clear()
  {
    _packages.clear();
    _currentlySelectedPackages.clear();
    _currentSelectedPackage = null;
    _sortedColumn = 0;
    _ascendingSort = true;
    _project = null;
  }

  /**
   * @author Andy Mechtenberg
   */
  void updateAllPackages(BoardType selectedBoardType)
  {
    _packages = selectedBoardType.getCompPackages();
    sortColumn(_sortedColumn, _ascendingSort);
    fireTableDataChanged();
  }

  /**
   * @author Andy Mechtenberg
   */
  void populateWithPanelData(BoardType selectedBoardType)
  {
    _project = Project.getCurrentlyLoadedProject();
    _packages = selectedBoardType.getCompPackages();
    _currentlySelectedPackages.clear();
    sortColumn(_sortedColumn, _ascendingSort);
//    fireTableStructureChanged();
  }

  /**
   * @author Andy Mechtenberg
   */
  void updateSpecificPackage(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);
    int packageRow = _packages.indexOf(compPackage);
    fireTableRowsUpdated(packageRow, packageRow);
  }

  /**
   * @author Andy Mechtenberg
   */
  int getRowForCompPackage(CompPackage compPackage)
  {
    // first just look for the object -- this is fast, and works most of the time.
    // It may fail, if the user modified the package data elsewhere and the system
    // had to create a new package.
    if (_packages.contains(compPackage))
      return _packages.indexOf(compPackage);

    // Ok, so we didn't find it by object.  Look now by long name, which includes the joint type
    // so we can distinguish between _CAP and _RES like things.
    String longName = compPackage.getLongName();
    for(CompPackage pkg : _packages)
    {
      if (pkg.getLongName().equalsIgnoreCase(longName))
          return _packages.indexOf(pkg);
    }

    // if we didn't find it, look for the short name -- as the joint type may have changed.
    String shortName = compPackage.getShortName();
    for(CompPackage pkg : _packages)
    {
      if (pkg.getShortName().equalsIgnoreCase(shortName))
          return _packages.indexOf(pkg);
    }

//    Assert.expect(false, "Package not found: " + longName);
    return -1;  // didn't find it
  }

  /**
   * @author Andy Mechtenberg
   */
  public void sortColumn(int column, boolean ascending)
  {
    if(column == 0)
    {
      _compPackageComparator.setAscending(ascending);
      _compPackageComparator.setCompPackageComparatorEnum(CompPackageComparatorEnum.LONG_NAME);
      Collections.sort(_packages, _compPackageComparator);
    }
    else if (column == 1)
    {
      _compPackageComparator.setAscending(ascending);
      _compPackageComparator.setCompPackageComparatorEnum(CompPackageComparatorEnum.JOINT_TYPE);
      Collections.sort(_packages, _compPackageComparator);
    }
    else if (column == 2)
    {
      _compPackageComparator.setAscending(ascending);
      _compPackageComparator.setCompPackageComparatorEnum(CompPackageComparatorEnum.JOINT_HEIGHT);
      Collections.sort(_packages, _compPackageComparator);
    }
    _sortedColumn = column;
    _ascendingSort = ascending;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getColumnCount()
  {
    return _packageTableColumns.length;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getRowCount()
  {
    if (_packages == null)
      return 0;
    else
      return _packages.size();
  }

  /**
   * @author Andy Mechtenberg
   */
  public Class getColumnClass(int c)
  {
    return getValueAt(0, c).getClass();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_packageTableColumns[columnIndex];
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    // column 0 is package name - not editable
    // column 1 if joint type - editable only if not MIXED
    // column 2 if joint height - editable only if not MIXED
    if (columnIndex > 0)
    {
      if (_currentlySelectedPackages.contains(_packages.get(rowIndex)) == false)
        return false;

      if (columnIndex == 1)
      {
        if (_packages.get(rowIndex).usesOneJointTypeEnum())
          return true;
      }
      else if (columnIndex == 2)
      {
        if (_packages.get(rowIndex).usesOneJointHeight())
          return true;
      }

      return false;
    }
    else
      return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    CompPackage compPackage = _packages.get(rowIndex);
    if (columnIndex == 0)
    {
      return compPackage.toString();
    }
    else if (columnIndex == 1)
    {
      if (compPackage.getPackagePinsUnsorted().isEmpty())
        return "";
      if (compPackage.usesOneJointTypeEnum() == false)
        return _MIXED;
      else
        return compPackage.getJointTypeEnum();
    }
    else if(columnIndex == 2)
    {
      if (compPackage.getPackagePinsUnsorted().isEmpty())
        return "";
      if (compPackage.usesOneJointHeight() == false)
        return _MIXED;
      else
        return new Double(MathUtil.convertUnits(compPackage.getJointHeightInNanoMeters(), MathUtilEnum.NANOMETERS, _project.getDisplayUnits()));
    }

    Assert.expect(false);
    return null;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    if (columnIndex == 0) // column 0 is package name -- not editable
      Assert.expect(false);
    else if (columnIndex == 1)
    {
      JointTypeEnum newJointTypeEnum = (JointTypeEnum)aValue;
      // not all package joint types are editable (they might be MIXED)
      // but if this is a group setting, we need to check for that
      if (_packages.get(rowIndex).usesOneJointTypeEnum() == false)
        return;

      // only allow this change if the throughhole/smt land pattern is compatible with the new joint type
      CompPackage compPackage = _packages.get(rowIndex);
      LandPattern landPattern = compPackage.getLandPattern();
      if (landPattern.getPadTypeEnum().equals(PadTypeEnum.THROUGH_HOLE) == false) // not all through hole
      {
        if (ImageAnalysis.requiresThroughHoleLandPatternPad(newJointTypeEnum))
        {
          // give error, and do not do the operation
          MessageDialog.showErrorDialog(
              MainMenuGui.getInstance(),
              StringLocalizer.keyToString("MMGUI_GROUPINGS_JOINT_TYPE_MUST_BE_THROUGHHOLE_KEY"),
              StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
              true);
          return;
        }
      }

      try
      {
        _commandManager.execute(new CompPackageSetJointTypeCommand(_packages.get(rowIndex), newJointTypeEnum, true));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }

    }
    else if (columnIndex == 2)
    {
      Assert.expect(aValue instanceof Number);
      Double newJointHeight = ((Number)aValue).doubleValue();
      // not all package joint types are editable (they might be MIXED)
      // but if this is a group setting, we need to check for that
      if (_packages.get(rowIndex).usesOneJointHeight() == false)
        return;

      double maximumAllowedJointHeightInDisplayUnits = _project.getMaxAllowedJointHeightInDisplayUnits(_project.getDisplayUnits());
      if (newJointHeight <= 0 || newJointHeight > maximumAllowedJointHeightInDisplayUnits)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      StringLocalizer.keyToString(new LocalizedString("MMGUI_GROUPINGS_JOINT_HEIGHT_OUT_OF_ALLOWED_RANGE_ERROR_KEY",
                                                                                      new Object[]{maximumAllowedJointHeightInDisplayUnits,
                                                                                                   _project.getDisplayUnits().toString()})),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        return;
      }
      CompPackage compPackage = _packages.get(rowIndex);
      try
      {
        _commandManager.execute(new CompPackageSetJointHeightCommand(
            compPackage,
            MathUtil.convertDoubleToInt(MathUtil.convertUnits(newJointHeight, _project.getDisplayUnits(), MathUtilEnum.NANOMETERS))));
      }
      catch (ValueOutOfRangeException ex1)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex1.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);

        fireTableCellUpdated(rowIndex, columnIndex);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    else
      Assert.expect(false); // only columns 1 and 2 are editable
  }

  /**
   * @author Andy Mechtenberg
   */
  void setSelectedPackages(List<CompPackage> selectedPackages)
  {
    Assert.expect(selectedPackages != null);
    _currentlySelectedPackages = selectedPackages;
  }

  /**
   * @author Andy Mechtenberg
   */
  void setSelectedPackage(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);
    _currentSelectedPackage = compPackage;
  }

  /**
   * @author Andy Mechtenberg
   */
  CompPackage getSelectedPackage()
  {
    Assert.expect(_currentSelectedPackage != null);
    return _currentSelectedPackage;
  }

  /**
   * @author Andy Mechtenberg
   */
  CompPackage getPackageAt(int row)
  {
    return _packages.get(row);
  }

  /**
   * @author George Booth
   */
  public int getRowForDataStartingWith(String key)
  {
    key = key.toLowerCase();
    int index = -1;
    for (CompPackage compPackage : _packages)
    {
      index++;
      String packageName = compPackage.toString().toLowerCase();
      if (packageName.startsWith(key))
      {
        return index;
      }
    }
    return -1;
  }

  /**
   *
   * @return boolean
   *
   * @author Tan Hock Zoon
   */
  public boolean hasSelectedPackage()
  {
    return (_currentSelectedPackage != null);
  }
}
