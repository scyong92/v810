package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * This class is the data model for the package table for the Groupings panel and it also
 * converts to the component table model in the groupings panel
 * @author Andy Mechtenberg
 */
public class ModifySubtypesComponentTableModel extends DefaultSortTableModel
{
  private List<ComponentType> _componentTypes = new ArrayList<ComponentType>();  // List of ComponentType objects

  private Collection<ComponentType> _currentlySelectedComponents = new ArrayList<ComponentType>();
  private final String[] _componentTableColumns =
  {
    StringLocalizer.keyToString("MMGUI_GROUPINGS_COMPONENT_KEY"),
    StringLocalizer.keyToString("MMGUI_GROUPINGS_PACKAGE_KEY"),
    StringLocalizer.keyToString("ATGUI_FAMILY_KEY"),
    StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY"),
    StringLocalizer.keyToString("GUI_FOCUS_FOCUS_METHOD_TABLE_HEADER_KEY"),
    //StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_GAIN_KEY"),
    //StringLocalizer.keyToString("MMGUI_GROUPINGS_INTERFACE_COMPENSATION_KEY"),
    //StringLocalizer.keyToString("MMGUI_GROUPINGS_INTEGRATION_LEVEL_KEY")
  };
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;  
  private Panel _panel = null;
  
  private static ComponentTypeComparator _componentTypeComparator;
  
  static
  {
    _componentTypeComparator = new ComponentTypeComparator(true, ComponentTypeComparatorEnum.REFERENCE_DESIGNATOR);
  }
  
  /**
   * @author Andy Mechtenberg
   */
  public ModifySubtypesComponentTableModel()
  {
    // do nothing
  }

  /**
   * @author Andy Mechtenberg
   */
  void clear()
  {
    _componentTypes.clear();
    _currentlySelectedComponents.clear();
    _currentSortColumn = 0;
    _currentSortAscending = true;
  }

  /**
   * @author Andy Mechtenberg
   */
  void populateWithPanelData(BoardType selectedBoardType)
  {
    _componentTypes = selectedBoardType.getComponentTypes();
    _panel = selectedBoardType.getPanel();
    sortColumn(_currentSortColumn, _currentSortAscending);

    // components may have been deleted, so if they were previously selected, we need to remove them.
    Iterator<ComponentType> it = _currentlySelectedComponents.iterator();
    while(it.hasNext())
    {
      if (_componentTypes.contains(it.next()) == false)
        it.remove();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  void updateSpecificComponent(ComponentType componentType)
  {
    int componentRow = _componentTypes.indexOf(componentType);
    fireTableRowsUpdated(componentRow, componentRow);
  }

  /**
   * @author Andy Mechtenberg
   */
  int getRowForComponentType(ComponentType componentType)
  {
    return _componentTypes.lastIndexOf(componentType);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void sortColumn(int column, boolean ascending)
  {
    if (column == 0)  // sort on ref.des.
    {
      _componentTypeComparator.setAscending(ascending);
      _componentTypeComparator.setComponentTypeComparatorEnum(ComponentTypeComparatorEnum.REFERENCE_DESIGNATOR);
      Collections.sort( _componentTypes, _componentTypeComparator);
    }
    else if (column == 1)  // sort on package
    {
      _componentTypeComparator.setAscending(ascending);
      _componentTypeComparator.setComponentTypeComparatorEnum(ComponentTypeComparatorEnum.PACKAGE);
      Collections.sort(_componentTypes, _componentTypeComparator);
    }
    else if (column == 2)  // sort on joint type
    {
      _componentTypeComparator.setAscending(ascending);
      _componentTypeComparator.setComponentTypeComparatorEnum(ComponentTypeComparatorEnum.JOINT_TYPE);
      Collections.sort(_componentTypes, _componentTypeComparator);
    }
    else if (column == 3) // sort on subtype
    {
      _componentTypeComparator.setAscending(ascending);
      _componentTypeComparator.setComponentTypeComparatorEnum(ComponentTypeComparatorEnum.SUBTYPE);
      Collections.sort(_componentTypes, _componentTypeComparator);
    }
    else if (column == 4) // sort on global surface model
    {
      _componentTypeComparator.setAscending(ascending);
      _componentTypeComparator.setComponentTypeComparatorEnum(ComponentTypeComparatorEnum.GSM_COMPENSATION);
      Collections.sort(_componentTypes, _componentTypeComparator);
    }
    //else if(column == 5) // user gain
    //  Collections.sort(_componentTypes, new ComponentTypeComparator(ascending, ComponentTypeComparatorEnum.USER_GAIN));
    //else if (column == 6 && _showShadingCompensation) // sort on artifact compensation
    //  Collections.sort(_componentTypes, new ComponentTypeComparator(ascending, ComponentTypeComparatorEnum.ARTIFACT_COMPENSATION));
    //else if (column == 7 && _showShadingCompensation) // sort on integration level
    //  Collections.sort(_componentTypes, new ComponentTypeComparator(ascending, ComponentTypeComparatorEnum.SIGNAL_COMPENSATION));

    _currentSortColumn = column;
    _currentSortAscending = ascending;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getColumnCount()
  {
    return _componentTableColumns.length;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getRowCount()
  {
    if (_componentTypes == null)
      return 0;
    else
      return _componentTypes.size();
  }

  /**
   * @author Andy Mechtenberg
   */
  public Class getColumnClass(int c)
  {
    return getValueAt(0, c).getClass();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_componentTableColumns[columnIndex];
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    // column 0 is component name - not editable
    // column 1 is package - not editable
    // column 2 is joint type -- only editable if not "MIXED"
    // column 3 is subtype - always editable
    // column 4 is auto focus - always editable
    // column 5 is artifact compensation - only editable if not "MIXED"
    // column 6 is signal compensation - only editable if not "MIXED"

    if (columnIndex == 0)
      return false;
    if (columnIndex == 1)
      return false;

    // if nothing selected, nothing can be editable
    if (_currentlySelectedComponents.isEmpty())
      return false;

    // if the current component is NOT selected, then it's not editable.
    // need to make the check up front before all the more detailed checks
    if (_currentlySelectedComponents.contains(getValueAt(rowIndex, 0)) == false)
      return false;

    if (columnIndex == 2)
    {
      for (ComponentType compType : _currentlySelectedComponents)
      {
        if (compType.getCompPackage().usesOneJointTypeEnum() == false)  // mixed case not editable
          return false;
      }
    }
    if (columnIndex == 3)
    {
      // if any selected components are not testable, then don't allow any editing
      // if the component is "partially testable" which means only some pins are untestable, then
      // allow editing
      for (ComponentType compType : _currentlySelectedComponents)
      {
        if (compType.getComponentTypeSettings().areAllPadTypesNotTestable())
          return false;
        else if (compType.isTestable() == false)
          return true;
      }
    }
    if (columnIndex == 4)
    {
        return true;
    }
    if (columnIndex == 5)
    {
      return true;
    }
    /*if(_showShadingCompensation)
    {
      if (columnIndex == 6)
      {
        if ((_currentlySelectedComponents.size() > 1) && (_currentlySelectedComponents.contains(getValueAt(rowIndex, 0))))
        {
          // if multiselected rows and they are all N/A then return NOT EDITABLE
          boolean allAreNotApplicable = true;
          for (ComponentType compType : _currentlySelectedComponents)
          {
            if (compType.usesOneArtifactCompensationSetting() == false)
              allAreNotApplicable = false;
            else
              if (compType.getArtifactCompensationSetting().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false)
                allAreNotApplicable = false;
          }
          if(allAreNotApplicable)
            return false;
        }

        if (_currentlySelectedComponents.size() == 1)
        {
          ComponentType compType = getComponentTypeAt(rowIndex);
          if (compType.usesOneArtifactCompensationSetting() && compType.getArtifactCompensationSetting().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE))
            return false;
        }

//        ComponentType compType = getComponentTypeAt(rowIndex);
//
//        if ((_currentlySelectedComponents.size() > 1) && (_currentlySelectedComponents.contains(getValueAt(rowIndex, 0))))
//          return true;
//
//        // mixed case is editable now per bensens request 10/9/2007
//        if (compType.usesOneArtifactCompensationSetting() == false)
//          return true;
//
//        // N/A not editable
//        if (compType.getArtifactCompensationSetting().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE))
//          return false;
      }*/

      /*if (columnIndex == 7)
      {
        ComponentType compType = getComponentTypeAt(rowIndex);
        if (compType.usesOneIntegrationLevelSetting() == false) // mixed case not editable
          return false;
        //CR fix by Khang Shian-not allow user to change Integration Level when select Interference Compensation to True or there is a mixed Compensation
        if(compType.usesOneArtifactCompensationSetting()==false)
          return false;
        if(compType.getArtifactCompensationSetting().equals(ArtifactCompensationStateEnum.COMPENSATED))
          return false;
      }*/
    //}

    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    ComponentType compType = _componentTypes.get(rowIndex);
    CompPackage compPackage = compType.getCompPackage();
    if (columnIndex == 0) // component name
      return compType;
    else if (columnIndex == 1) // component package
    {
      return compPackage;
    }
    else if (columnIndex == 2) // component joint type
    {
      return TestDev.getComponentDisplayJointType(compType);
    }
    else if(columnIndex == 3) // component subtype
    {
      return TestDev.getComponentDisplaySubtype(compType);
    }
    else if (columnIndex == 4) // global surface model
    {
      return compType.getGlobalSurfaceModel();
    }
    if (columnIndex == 5) // user gain
    {
      return compType.getUserGain();
    }
    /*if(_showShadingCompensation)
    {
      if (columnIndex == 6) // artifact compensation
      {
        if (compType.usesOneArtifactCompensationSetting() == false)
          return StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
        else
          return compType.getPadTypes().get(0).getPadTypeSettings().getEffectiveArtifactCompensationState();
      }
      else if (columnIndex == 7) // integration level
      {
        if (compType.usesOneIntegrationLevelSetting() == false)
          return StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
        else
          return compType.getSignalCompensationLevel();
      }
      else
        Assert.expect(false);
    }*/

    return null;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    if (columnIndex == 0) // column 0 is component name -- not editable
      Assert.expect(false);
    else if (columnIndex == 2) // component joint type
    {
      JointTypeEnum newJointTypeEnum = (JointTypeEnum)aValue;
      ComponentType componentType = _componentTypes.get(rowIndex);
      LandPattern landPattern = componentType.getLandPattern();
      Boolean isMixedSubtypes = false;
      Subtype oldSubtype = null;
      List<Subtype> oldSubtypes = null;
      
      if (componentType.getSubtypes().size()> 1)
      {
        isMixedSubtypes = true;
        oldSubtypes = componentType.getSubtypes();
      }
      else
      {
        oldSubtype = componentType.getComponentTypeSettings().getSubtype();
      }
   
      JointTypeEnum origJointType = componentType.getJointTypeEnums().get(0);  
      boolean isSubtypeExist = _panel.doesSubtypeExist(landPattern.getName() + "_" + newJointTypeEnum.getNameWithoutSpaces());
      // get it from config later on
      boolean isVariableMagnification = false;
      try
      {
        if(LicenseManager.isVariableMagnificationEnabled())
        {
          isVariableMagnification = true;
        }
      }
      catch(BusinessException be)
      {
        // do nothing
      }
      
      // only allow this change if the throughhole/smt land pattern is compatible with the new joint type
      if (landPattern.getPadTypeEnum().equals(PadTypeEnum.THROUGH_HOLE) == false) // not all through hole
      {
        if (ImageAnalysis.requiresThroughHoleLandPatternPad(newJointTypeEnum))
        {
          // give error, and do not do the operation
          MessageDialog.showErrorDialog(
              MainMenuGui.getInstance(),
              StringLocalizer.keyToString("MMGUI_GROUPINGS_JOINT_TYPE_MUST_BE_THROUGHHOLE_KEY"),
              StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
              true);
          return;
        }
      }
      try
      {  
        _commandManager.execute(new ComponentTypeSetJointTypeCommand(componentType, newJointTypeEnum, true));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
      
      if (isVariableMagnification && (newJointTypeEnum.equals(origJointType)) == false)
      {
        if (isMixedSubtypes)
        {
          for (Subtype subtype : oldSubtypes)
          {
            componentTypeSetJointTypeAndSetMagnification(componentType, newJointTypeEnum, subtype, isSubtypeExist);
          }
        }
        else
        {         
          componentTypeSetJointTypeAndSetMagnification(componentType, newJointTypeEnum, oldSubtype, isSubtypeExist);
        }
      }
      if (oldSubtypes != null)
        oldSubtypes.clear();
      return; // we're done as soon as it's set
    }
    else if (columnIndex == 3) // component subtype
    {
      String newSubtypeName = (String)aValue;
      ComponentType componentType = _componentTypes.get(rowIndex);
      JointTypeEnum jointType = componentType.getComponentTypeSettings().getSubtype().getJointTypeEnum();
      java.util.List<Subtype> usedSubtypes = componentType.getSideBoardType().getBoardType().getSubtypesIncludingUnused(jointType);
      for (Subtype subtype : usedSubtypes)
      {
        if (subtype.getShortName().equalsIgnoreCase(newSubtypeName))
        {
          try
          {
            _commandManager.execute(new ComponentTypeSetSubtypeCommand(componentType, subtype));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
          return; // we're done as soon as it's set
        }
      }
      // if not set, then this is a new subtype and we need to create one
      Panel panel = componentType.getSideBoardType().getBoardType().getPanel();
      LandPattern landPattern = componentType.getLandPattern();
      JointTypeEnum jointTypeEnum = componentType.getCompPackage().getJointTypeEnum();
      Subtype newSubtype = panel.createSubtype(newSubtypeName, landPattern, jointTypeEnum);
      try
      {
        _commandManager.execute(new ComponentTypeSetSubtypeCommand(componentType, newSubtype, true));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    else if (columnIndex == 4) // global surface model
    {
      Assert.expect(aValue instanceof GlobalSurfaceModelEnum);
      ComponentType componentType = _componentTypes.get(rowIndex);
      GlobalSurfaceModelEnum globalSurfaceModelEnum = (GlobalSurfaceModelEnum)(aValue);
      
      // Added by LeeHerng - Check if a component is marked to use PSP.
      // If yes, we will check if the selected component is set to use PSP 
      // and compared with the selected GlobalSurfaceModelEnum.
      // If PSP is selected and component is not marked to use PSP, we will
      // warn user about this.
      if (globalSurfaceModelEnum.equals(GlobalSurfaceModelEnum.PSP) &&
          componentType.isSetToUsePspResult() == false)
      {
          // Warn user about the selection
          MessageDialog.showErrorDialog(
              MainMenuGui.getInstance(),
              StringLocalizer.keyToString("MMGUI_GROUPINGS_NON_PSP_COMPONENT_SELECTED_KEY"),
              StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
              true);
          return;
      }
      
      try
      {
        _commandManager.execute(new ComponentTypeSetGlobalSurfaceModelCommand(componentType, (GlobalSurfaceModelEnum)(aValue)));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                  ex.getLocalizedMessage(),
                                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                  true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    /* else if (columnIndex == 5)//user gain
    {
      Assert.expect(aValue instanceof UserGainEnum);
      ComponentType componentType = _componentTypes.get(rowIndex);
      try
      {
        _commandManager.execute(new ComponentTypeSetUserGainCommand(componentType, (UserGainEnum) (aValue)));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
          ex.getLocalizedMessage(),
          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
          true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    else if (columnIndex == 6 && _showShadingCompensation) // artifact compensation
    {
      Assert.expect(aValue instanceof ArtifactCompensationStateEnum);
      ComponentType componentType = _componentTypes.get(rowIndex);
      try
      {
        // it is possible to try to set AC for mixed or N/A component when using the multi select feature
        // set a new AC at the component level only if this component uses one state (not mixed) and is not N/A
        if (componentType.usesOneArtifactCompensationSetting() &&
            componentType.getArtifactCompensationSetting().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false)
          _commandManager.execute(new ComponentTypeSetArtifactCompesationCommand(componentType, (ArtifactCompensationStateEnum)(aValue), SignalCompensationEnum.DEFAULT_LOW));
        else
        {
          for (PadType padType : componentType.getPadTypes())
            // set a new AC only if the pad type isn't already N/A
            if (!padType.getPadTypeSettings().getEffectiveArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE))
              _commandManager.execute(new PadTypeSetArtifactCompesationCommand(padType, (ArtifactCompensationStateEnum)(aValue), SignalCompensationEnum.DEFAULT_LOW));
        }
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    else if (columnIndex == 7 && _showShadingCompensation) // integration level
    {
      Assert.expect(aValue instanceof SignalCompensationEnum);
      ComponentType componentType = _componentTypes.get(rowIndex);
      try
      {
        _commandManager.execute(new ComponentTypeSetIntegrationLevelCommand(componentType, (SignalCompensationEnum)(aValue)));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }*/
    else
      Assert.expect(false);
  }

  /**
   * @author Andy Mechtenberg
   */
  void setSelectedComponentTypes(Collection<ComponentType> componentTypes)
  {
    Assert.expect(componentTypes != null);
    _currentlySelectedComponents = componentTypes;
  }

  /**
   * @author Andy Mechtenberg
   */
  Collection<ComponentType> getSelectedComponentTypes()
  {
    Assert.expect(_currentlySelectedComponents != null);
    return _currentlySelectedComponents;
  }

  /**
    * @author Andy Mechtenberg
   */
  boolean isCommonSelectedComponentType()
  {
    Assert.expect(_currentlySelectedComponents != null);
    if (_currentlySelectedComponents.isEmpty())
      return false;
    CompPackage compPackage = null;
    for(ComponentType componentType : _currentlySelectedComponents)
    {
      if (compPackage == null)
        compPackage = componentType.getCompPackage();
      else
      {
        if (compPackage.equals(componentType.getCompPackage()) == false)
          return false;
      }
    }
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  ComponentType getCommonComponentType()
  {
    Assert.expect(_currentlySelectedComponents != null);
    Assert.expect(isCommonSelectedComponentType());

    return _currentlySelectedComponents.iterator().next();
  }

  /**
   * @author Andy Mechtenberg
   */
  ComponentType getComponentTypeAt(int row)
  {
    return _componentTypes.get(row);
  }

  /**
   * @author George Booth
   */
  public int getRowForDataStartingWith(String key)
  {
    key = key.toLowerCase();
    int index = -1;
    for (ComponentType compType :_componentTypes)
    {
      index++;
      String compName = compType.getReferenceDesignator().toLowerCase();
      if (compName.startsWith(key))
      {
        return index;
      }
    }
    return -1;
  }

  /**
   * @author sham
   */
  public void clearCurrentlySelectedComponents()
  {
    _currentlySelectedComponents.clear();
    fireTableDataChanged();
  }

  /**
   * @author sham
   */
  public Collection<ComponentType> getCurrentlySelectedComponents()
  {
    Assert.expect(_currentlySelectedComponents != null);

    return _currentlySelectedComponents;
  }
  
     /**
   * Handle High Mag Joint Type Changed
   * @author Jack Hwee
   */
  private void componentTypeSetJointTypeAndSetMagnification(ComponentType componentType, JointTypeEnum newJointType, Subtype oldSubtype, boolean isSubtypeExist)
  {
    Assert.expect(componentType != null);
    Assert.expect(newJointType != null);
    Assert.expect(oldSubtype != null);
 
    for (Subtype newSubtype : componentType.getSubtypes())
    {
      // Subtype newSubtype = componentType.getComponentTypeSettings().getSubtype();

      SubtypeAdvanceSettings subtypeAdvanceSettings = newSubtype.getSubtypeAdvanceSettings();

      if (isSubtypeExist)
      {
        try
        {
          _commandManager.execute(new SubtypeAdvanceSettingsSetMagnificationTypeCommand(subtypeAdvanceSettings, subtypeAdvanceSettings.getMagnificationType()));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  ex.getLocalizedMessage(),
                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                  true);
        }
        break;
      }
      else
      {
        if (oldSubtype.getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
        {
          try
          {
            _commandManager.execute(new SubtypeAdvanceSettingsSetMagnificationTypeCommand(subtypeAdvanceSettings, (MagnificationTypeEnum.HIGH)));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                    ex.getLocalizedMessage(),
                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                    true);
          }
          break;
        }
      }
    }
  }
}
