package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ProjectDetailsDialog extends JDialog
{
  private java.util.List<Subtype> _subtypes; // list of Subtypes that are not tuned

  private BorderLayout _mainBorderLayout = new BorderLayout();
  private JSortTable _subtypeTable;
  private JointTypeSubtypeTableModel _subtypeTableModel;
  private JPanel _okCancelPanel = new JPanel();
  private FlowLayout _okCanelFowLayout = new FlowLayout();
  private JPanel _subyptesPanel = new JPanel();
  private BorderLayout _subtypesPanelBorderLayout = new BorderLayout();
  private JScrollPane _subtypesScrollPane = new JScrollPane();
  private JLabel _subtypesLabel = new JLabel();
  private JButton _okButton = new JButton();

  /**
   * @author Andy Mechtenberg
   */
  public ProjectDetailsDialog(Frame frame, String title, java.util.List<Subtype> untunedSubtypes)
  {
    super(frame, title, true);  // this dialog is always modal

    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(untunedSubtypes != null);

    _subtypes = untunedSubtypes;
    jbInit();
    pack();
    int headerHeight = _subtypeTable.getTableHeader().getHeight();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    this.getContentPane().setLayout(_mainBorderLayout);
    _subtypeTableModel = new JointTypeSubtypeTableModel(_subtypes);
    _subtypeTable = new JSortTable(_subtypeTableModel);
    _subyptesPanel.setLayout(_subtypesPanelBorderLayout);
    _subtypesLabel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
    _subtypesLabel.setText(StringLocalizer.keyToString("MMGUI_HOME_UNLEARNED_SUBTYPES_KEY"));
//    _untunedSubtypesScrollPane.setPreferredSize(new Dimension(453, 200));
    _okCanelFowLayout.setAlignment(FlowLayout.CENTER);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    _subyptesPanel.setBorder(BorderFactory.createEmptyBorder(0,5,0,5));
    this.getContentPane().add(_subyptesPanel, BorderLayout.CENTER);
    _subtypesScrollPane.getViewport().add(_subtypeTable, null);
    _okCancelPanel.setLayout(_okCanelFowLayout);
    _okCancelPanel.setBorder(BorderFactory.createEmptyBorder(0,0,5,5));
    this.getContentPane().add(_subtypesLabel, BorderLayout.NORTH);
    this.getContentPane().add(_okCancelPanel,  BorderLayout.SOUTH);
    _okCancelPanel.add(_okButton, null);
    _subyptesPanel.add(_subtypesScrollPane, BorderLayout.CENTER);
    getRootPane().setDefaultButton(_okButton);

    SwingUtils.setTableAndScrollPanelToPreferredSize(_subtypesScrollPane, _subtypeTable);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }
}
