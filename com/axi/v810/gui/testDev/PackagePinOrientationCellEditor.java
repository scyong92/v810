package com.axi.v810.gui.testDev;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import java.util.*;

/**
 * This class implements a combo box cell editor tailored to changing the pin orientation
 * for a package pin
 * @author Andy Mechtenberg
 */
public class PackagePinOrientationCellEditor extends DefaultCellEditor
{
  private ModifySubtypesPackagePinTableModel _packagePinTableModel;

  /**
   * @author Andy Mechtenberg
   */
  public PackagePinOrientationCellEditor(JComboBox comboBox, ModifySubtypesPackagePinTableModel packagePinTableModel)
  {
    super(comboBox);

    Assert.expect(packagePinTableModel != null);
    _packagePinTableModel = packagePinTableModel;
  }

  /**
   * @author Andy Mechtenberg
   */
  public java.awt.Component getTableCellEditorComponent(JTable table, Object value, boolean isSelectable, int row, int column)
  {
    // get the editor (should be a JComboBox as was specified in the constructor)
    // then modify the contents of the combo box to list only the valid choices for this row in the table
    int selectedRows[] = table.getSelectedRows();
    boolean useOrthogonal = false;
    boolean useAngled = false;

    // assign all booleans above by looking at each component selected.
    for (int i = 0; i < selectedRows.length; i++)
    {
      PackagePin packagePin = _packagePinTableModel.getPackagePinAt(selectedRows[i]);
      if (packagePin.getPinOrientationAfterRotationEnum().isOrthogonal())
      {
        useOrthogonal = true;
      }
      else
      {
        useAngled = true;
      }
    }
    java.awt.Component editor = super.getTableCellEditorComponent(table, value, isSelectable, row, column);
    JComboBox comboBox = (JComboBox)editor;
    comboBox.removeAllItems();
    // do the single selection first - easier if we get this out of the way
    if (useOrthogonal)
    {
      List<PinOrientationAfterRotationEnum>
          orthogonals = PinOrientationAfterRotationEnum.getAllOrthogonalPinOrientationEnums();
      for (PinOrientationAfterRotationEnum pinOrientationAfterRotationEnum : orthogonals)
        comboBox.addItem(pinOrientationAfterRotationEnum);
      
      // Clear memory - by Lee Herng 2 Dec 2012
      if (orthogonals != null)
        orthogonals.clear();
    }
    if (useAngled)
    {
      List<PinOrientationAfterRotationEnum>
          angles = PinOrientationAfterRotationEnum.getAllNonOrthogonalPinOrientationEnums();
      for (PinOrientationAfterRotationEnum pinOrientationAfterRotationEnum : angles)
        comboBox.addItem(pinOrientationAfterRotationEnum);
      
      // Clear memory - by Lee Herng 2 Dec 2012
      if (angles != null)
        angles.clear();
    }
    if (selectedRows.length == 1)
    {
      PackagePin packagePin = _packagePinTableModel.getPackagePinAt(selectedRows[0]);
      comboBox.setSelectedItem(packagePin.getPinOrientationAfterRotationEnum());
    }
    comboBox.setMaximumRowCount(8);
    return comboBox;
  }
}
