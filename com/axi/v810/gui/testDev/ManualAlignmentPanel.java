package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.gui.psp.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Erica Wheatcroft
 */
public class ManualAlignmentPanel extends AbstractTaskPanel implements Observer
{
  // these are not user visible strings, they are used to swap the card layout panels
  private final static String _PANEL_ALIGNMENT = "Panel Alignment";
  private final static String _LONG_PANEL_ALIGNMENT = "Long Panel Alignment";
  private BusyCancelDialog _busyCancelDialog = null;

  private XrayTester _xRayTester;
  private TestExecution _testExecution;
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private Alignment _alignment = null;
  private MainMenuGui _mainUI;
  private TestDev _testDev = null;
  private ImageManagerPanel _imagePanel;
  private Project _project;
  private boolean _updateData = false;
  private boolean _populateProjectData = false;
  private boolean _manualAlignmentInProgress = false;
  private int _maxPadsAndFiducials;

  private boolean _userAborted;
  private XrayTesterException _exception;

  private AlignmentGroup _alignmentGroup1;
  private AlignmentGroup _alignmentGroup2;
  private AlignmentGroup _alignmentGroup3;

  private AlignmentGroup _alignmentGroup1Top;
  private AlignmentGroup _alignmentGroup2Top;
  private AlignmentGroup _alignmentGroup3Top;
  private AlignmentGroup _alignmentGroup1Bottom;
  private AlignmentGroup _alignmentGroup2Bottom;
  private AlignmentGroup _alignmentGroup3Bottom;

  private java.util.List<AlignmentGroup> _longPanelAlignmentGroups = new ArrayList<AlignmentGroup>();
  private java.util.List<AlignmentGroup> _shortPanelAlignmentGroups = new ArrayList<AlignmentGroup>();

  private Map<AlignmentGroup, JRadioButton> _alignmentGroupToRadioButtonMap = new HashMap<AlignmentGroup, JRadioButton>();
  private Map<JRadioButton, JList> _radioButtonToListMap = new HashMap<JRadioButton, JList>();

  private boolean _topRegionActive = true; // remembers which region of a long panel is currently active
  private java.util.List<HighlightAlignmentItems> _allHighlights = new ArrayList<HighlightAlignmentItems>();
  private AlignmentImagePanel _alignmentImagePanel;

  // hold the currently selected stuff in these variables -- simplifies the logic lower down
  private AlignmentGroup _currentAlignmentGroup;
  private DefaultListModel _currentAlignmentGroupListModel;
  private JList _currentAlignmentGroupList;

  private boolean _isLongPanel = false;
  private int _runTimeCount = 0;

  private Color _group1Color = LayerColorEnum.ALIGNMENT_GROUP_1_COLOR.getColor(); // yellow
  private Color _group2Color = LayerColorEnum.ALIGNMENT_GROUP_2_COLOR.getColor(); // green
  private Color _group3Color = LayerColorEnum.ALIGNMENT_GROUP_3_COLOR.getColor(); // blue

  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  private SelectedRendererObservable _selectedRendererObservable;
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private HardwareObservable _hardwareObservable = HardwareObservable.getInstance();
  private DragOpticalRegionObservable _dragOpticalRegionRendererObservable;
  private ListSelectionListener _listSelectionListener;
  private ActionListener _alignmentGroup1RadioButtonListener;
  private ActionListener _alignmentGroup1TopRadioButtonListener;
  private JPanel _alignmentTargetPanel = new JPanel();
  private JLabel _alignmentTargetSelectionLabel = new JLabel();
  private JComboBox _alignmentTargetSelectionComboBox = new JComboBox();
  private VerticalFlowLayout _alignmentTargetVerticalFlowLayout = new VerticalFlowLayout(VerticalFlowLayout.TOP, VerticalFlowLayout.FILL, 0, 3);
  private VerticalFlowLayout _alignmentVerticalFlowLayout = new VerticalFlowLayout(VerticalFlowLayout.TOP, VerticalFlowLayout.FILL, 0, 0);
  private ButtonGroup _alignGroupsButtonGroup = new ButtonGroup();
  private ButtonGroup _longPanelAlignGroupsButtonGroup = new ButtonGroup();
//  private ButtonGroup _alignTargetButtonGroup = new ButtonGroup();
  private JPanel _alignmentGroupsPanel = new JPanel();
  private JList _alignmentGroup1List = new JList();
  private DefaultListModel _alignmentGroup1ListModel = new DefaultListModel();
  private DefaultListModel _alignmentGroup2ListModel = new DefaultListModel();
  private DefaultListModel _alignmentGroup3ListModel = new DefaultListModel();

  private DefaultListModel _alignmentGroup1TopListModel = new DefaultListModel();
  private DefaultListModel _alignmentGroup2TopListModel = new DefaultListModel();
  private DefaultListModel _alignmentGroup3TopListModel = new DefaultListModel();
  private DefaultListModel _alignmentGroup1BottomListModel = new DefaultListModel();
  private DefaultListModel _alignmentGroup2BottomListModel = new DefaultListModel();
  private DefaultListModel _alignmentGroup3BottomListModel = new DefaultListModel();

  private JPanel _alignmentGroup1Panel = new JPanel();
  private JRadioButton _alignmentGroup1RadioButton = new JRadioButton();
  private BorderLayout _alignmentGroup1BorderLayout = new BorderLayout();
  private JList _alignmentGroup3List = new JList();
  private JPanel _alignmentGroup3Panel = new JPanel();
  private BorderLayout _alignmentGroup3BorderLayout = new BorderLayout();
  private JRadioButton _alignmentGroup3RadioButton = new JRadioButton();
  private JList _alignmentGroup2List = new JList();
  private JPanel _alignmentGroup2Panel = new JPanel();
  private BorderLayout _alignmentGroup2BorderLayout = new BorderLayout();
  private JRadioButton _alignmentGroup2RadioButton = new JRadioButton();
  private JLabel _alignmentInstructionLabel = new JLabel();
  private JPanel _alignmentTypeChoicesPanel = new JPanel();
  private VerticalFlowLayout _alignmentOptionsVerticalFlowLayout = new VerticalFlowLayout(VerticalFlowLayout.TOP, VerticalFlowLayout.FILL, 0, 0);
  private JPanel _manualAlignmentNavigationPanel = new JPanel();
  private JPanel _manualAlignmentPanel = new JPanel();
  private FlowLayout _manualAlignmentNavigationPanelFlowLayout = new FlowLayout();
  private VerticalFlowLayout _manualAlignmentMatrixStatusPanelFlowLayout = new VerticalFlowLayout();
  private FlowLayout _manualAlignmentStatusPanelFlowLayout = new FlowLayout();
  private VerticalFlowLayout _manualAlignmentPanelVerticalFlowLayout = new VerticalFlowLayout();
  private JLabel _manualAlignmentMatrixStatusLabel = new JLabel();
  private JLabel _manualAlignmentMatrixInstructionLabel = new JLabel();
  private JPanel _manualAlignmentMatrixStatusPanel = new JPanel();
  private JLabel _manualAlignmentStatusLabel = new JLabel();
  private JPanel _manualAlignmentStatusPanel = new JPanel();
  private Border _runAlignmentPanelBorder;
  private JPanel _deleteAlignmentPointPanel = new JPanel();
  private JButton _deleteAlignmentPointButton = new JButton();
  private JPanel _alignmentButtonsPanel = new JPanel();
  private JButton _runAlignmentButton = new JButton();
  private JButton _runAlignmentWithDifferentSliceButton = new JButton();
  private JButton _clearAlignmentButton = new JButton();
  private JButton _viewAlignmentButton = new JButton();
  private JButton _autoAlignmentButton = new JButton();
  private JButton _alignmentGuideButton = new JButton();
  private JButton _autoSelectAlignmentGroupButton = new JButton();
  private FlowLayout _alignmentButtonsFlowLayout = new FlowLayout();
  private JPanel _innerButtonsPanel = new JPanel();
  private GridLayout _innerButtonsPanelGridLayout = new GridLayout();
  private GridLayout _innerAlignButtonGridLayout = new GridLayout(4,2);
  private JButton _cancelButton = new JButton();
  private JButton _nextButton = new JButton();

  private ImageIcon _imageIcon = com.axi.v810.images.Image5DX.getImageIcon(com.axi.v810.images.Image5DX.AXI_SPARK);
  private JScrollPane _alignmentGroup1ListScrollPane = new JScrollPane();
  private JScrollPane _alignmentGroup2ListScrollPane = new JScrollPane();
  private JScrollPane _alignmentGroup3ListScrollPane = new JScrollPane();
  private CardLayout _alignmentGroupsCardLayout = new CardLayout();
  private JPanel _panelBasedAlignmentPanel = new JPanel();
  private Dimension _panelBasedAlignmentPanelInitialSize = null;
  private Dimension _longPanelBasedAlignmentPanelInitialSize = null;
  private JPanel _longPanelAlignmentPanel = new JPanel();
  private GridLayout _panelBasedAlignmentPanelGridLayout = new GridLayout();
  private FlowLayout _deleteAlignmentPointPanelFlowLayout = new FlowLayout();

  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();

  private InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
  private GridLayout _longPanelAlignmentGridLayout = new GridLayout();
  private JPanel _alignmentGroup1TopPanel = new JPanel();
  private JRadioButton _alignmentGroup1TopRadioButton = new JRadioButton();

  private BorderLayout _alignmentGroup1TopBorderLayout = new BorderLayout();
  private JPanel _alignmentGroup2TopPanel = new JPanel();
  private JPanel _alignmentGroup3BottomPanel = new JPanel();
  private JPanel _alignmentGroup2BottomPanel = new JPanel();
  private JPanel _alignmentGroup1BottomPanel = new JPanel();
  private JPanel _alignmentGroup3TopPanel = new JPanel();
  private BorderLayout _alignmentGroup2TopBorderLayout = new BorderLayout();
  private BorderLayout _alignmentGroup3TopBorderLayout = new BorderLayout();
  private BorderLayout _alignmentGroup1BottomBorderLayout = new BorderLayout();
  private BorderLayout _alignmentGroup2BottomBorderLayout = new BorderLayout();
  private BorderLayout _alignmentGroup3BottomBorderLayout = new BorderLayout();
  private JRadioButton _alignmentGroup3TopRadioButton = new JRadioButton();
  private JRadioButton _alignmentGroup1BottomRadioButton = new JRadioButton();
  private JRadioButton _alignmentGroup2BottomRadioButton = new JRadioButton();
  private JRadioButton _alignmentGroup3BottomRadioButton = new JRadioButton();
  private JRadioButton _alignmentGroup2TopRadioButton = new JRadioButton();
  private JList _alignmentGroup1TopList = new JList();
  private JList _alignmentGroup2TopList = new JList();
  private JList _alignmentGroup3TopList = new JList();
  private JList _alignmentGroup1BottomList = new JList();
  private JList _alignmentGroup2BottomList = new JList();
  private JList _alignmentGroup3BottomList = new JList();
  private JScrollPane _alignmentGroup1TopScrollPane = new JScrollPane();
  private JScrollPane _alignmentGroup2TopScrollPane = new JScrollPane();
  private JScrollPane _alignmentGroup3TopScrollPane = new JScrollPane();
  private JScrollPane _alignmentGroup1BottomScrollPane = new JScrollPane();
  private JScrollPane _alignmentGroup2BottomScrollPane = new JScrollPane();
  private JScrollPane _alignmentGroup3BottomScrollPane = new JScrollPane();
  private JPanel _bottomRegionAlignmentPanel = new JPanel();
  private JPanel _topRegionAlignmentPanel = new JPanel();
  private BorderLayout _topRegionAlignmentBorderLayout = new BorderLayout();
  private JLabel _topRegionLabel = new JLabel();
  private JPanel _topGroupsPanel = new JPanel();
  private GridLayout _topGroupsGridLayout = new GridLayout();
  private BorderLayout _bottomRegionAlignmentBorderLayout = new BorderLayout();
  private JPanel _bottomGroupsPanel = new JPanel();
  private JLabel _bottomRegionLabel = new JLabel();
  private GridLayout _bottomGroupsGridLayout = new GridLayout();

  private int _zHeightOffsetTop = 0;
  private int _zHeightOffsetBottom = 0;
  private boolean _useRFP = false;
  private SignalCompensationEnum _currentSignalCompensation = SignalCompensationEnum.DEFAULT_LOW;
  private UserGainEnum _currentUserGain = UserGainEnum.ONE;
  private StageSpeedEnum _currentStageSpeed = StageSpeedEnum.ONE;
  private MagnificationTypeEnum _currentMagnification = MagnificationTypeEnum.LOW;

  private AlignmentCustomizationDialog _alignmentCustomizeDialog = null;
  
  private boolean _hasAlignmentGroupSetting = true;
  private JButton _alignmentSettingButton = new JButton();
  
  private AlignmentSettingDialog _alignmentSettingDialog = null;
  
  private static boolean _isSurfaceMapMode = false;
  // Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  // This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//  private JCheckBox _group1PspCheckBox = new JCheckBox();
//  private JCheckBox _group2PspCheckBox = new JCheckBox();
//  private JCheckBox _group3PspCheckBox = new JCheckBox();
//  private JCheckBox _topGroup1PspCheckBox = new JCheckBox();
//  private JCheckBox _topGroup2PspCheckBox = new JCheckBox();
//  private JCheckBox _topGroup3PspCheckBox = new JCheckBox();
//  private JCheckBox _bottomGroup1PspCheckBox = new JCheckBox();
//  private JCheckBox _bottomGroup2PspCheckBox = new JCheckBox();
//  private JCheckBox _bottomGroup3PspCheckBox = new JCheckBox();
  private Map<Rectangle, String> _opticalRegionMap = new LinkedHashMap<Rectangle, String>();
  private JButton _runSurfaceMapAlignmentButton = new JButton();
  private JButton _deleteSurfaceMapAlignmentButton = new JButton();
  private boolean _isNotEmptySpace = false;
  private static final int _CHARS_PER_LINE = 50;
  private SurfaceMapLiveVideoDialog _pspLiveVideoDialog;
  private SurfaceMapping _surfaceMapping;
  private boolean _simulationMode = XrayTester.getInstance().isSimulationModeOn();
  private boolean _isZoom = false;
  private String _currentAlignmentGroupForSurfaceMap = "0";
  private Map<String, String> _alignmentGroupNameToRegionPositionNameMap = new LinkedHashMap<String, String>();
  private static boolean _isPanelBasedAlignment = false;
  //Enlarge the allowable alignment region that can draw surface map rectangles on by 1.5 inch
  private final int PSP_RECTANGLE_OFFSET = 38100000;
  //Khaw Chek Hau - XCR2285: 2D Alignment on v810
  private JPanel _alignmentMethodPanel = new JPanel();
  private FlowLayout _alignmentMethodPanelFlowLayout = new FlowLayout();
  private GridLayout _alignmentMethodGridLayout = new GridLayout();
  private JRadioButton _alignment2DRadioButton = new JRadioButton();
  private JRadioButton _alignment3DRadioButton = new JRadioButton();
  private ActionListener _alignment2DRadioButtonActionListener;
  private ActionListener _alignment3DRadioButtonActionListener;
  private ButtonGroup _alignmentModeButtonGroup = new ButtonGroup();
  //XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
  private int _countTestSubprogram = 0;
  private int _customTestSubprogramSize = 0;
  private boolean _isManualAlignmentInitiallyComplete = false;
  /**
   * Useful ONLY for the JBuilder designer.  Not for use with normal code.
   * @author Andy Mechtenberg
   */
  private ManualAlignmentPanel()
  {
    Assert.expect(java.beans.Beans.isDesignTime());
    jbInit();
  }

  /**
   * @author Andy Mechtenberg
   */
  public ManualAlignmentPanel(TestDev testDev, ImageManagerPanel imagePanel)
  {
    super(testDev);
    Assert.expect(testDev != null);
    Assert.expect(imagePanel != null);
    _xRayTester = XrayTester.getInstance();
    _testExecution = TestExecution.getInstance();
    _testDev = testDev;
    _mainUI = _testDev.getMainUI();
    _imagePanel = imagePanel;

    jbInit();
    _maxPadsAndFiducials = AlignmentGroup.getMaximumPadsAndFiducials();

    _alignmentImagePanel = _imagePanel.getAlignmentImagePanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof SelectedRendererObservable)
          handleSelectionEvent(argument);
        else if (observable instanceof ProjectObservable)
          handleProjectEvent(argument);
        else if (observable instanceof InspectionEventObservable)
          handleInspectionEvent((InspectionEvent)argument);
        else if (observable instanceof HardwareObservable)
          handleHardwareEvent(argument);
        else if (observable instanceof DragOpticalRegionObservable)      
          handleDragOpticalRegionEvent(argument);
        else
          Assert.expect(false);
      }
    });
  }
  
  /**
   * @author Andy Mechtenberg
   */
  private void handleSelectionEvent(final Object object)
  {
    // if we are aligning, then no selections should be listened to
    if (_manualAlignmentInProgress)
      return;

    // ok.  We got notice that something was selected.

    // We receive from the update, a collection of selected objects.  We'll only pay attention to
    // the ones this screen is interested in
    Collection<com.axi.guiUtil.Renderer> selections = (Collection < com.axi.guiUtil.Renderer > )object;

    // sort the fiducial so they're displayed by name.  The pads can't be sorted by their name since
    // I want the component name to be part of the sorting
    java.util.Set<Fiducial> fiducials = new TreeSet<Fiducial>(new AlphaNumericComparator());
    java.util.List<Pad> pads = new ArrayList<Pad>();

    for (com.axi.guiUtil.Renderer renderer : selections)
    {
      if (renderer instanceof PadRenderer)
      {
        PadRenderer padRenderer = (PadRenderer)renderer;
        Pad pad = padRenderer.getPad();
        // Ying-Huan.Chu - [XCR-2037] subtask : handle panel-based recipe with overlapping boards.
        boolean shouldSkipThisComponent = false;
        java.util.List<Pad> padsInCurrentAlignmentGroup = new ArrayList<>();
        padsInCurrentAlignmentGroup.addAll(pads);
        padsInCurrentAlignmentGroup.addAll(_currentAlignmentGroup.getPads());
        for (Pad selectedPad : padsInCurrentAlignmentGroup)
        {
          if (pad.getComponent().getBoard().getName().equals(selectedPad.getComponent().getBoard().getName()) == false)
          {
            shouldSkipThisComponent = true;
            break;
          }
        }
        if (shouldSkipThisComponent)
        {
          continue;
        }
        pads.add(pad);
      }
      else if (renderer instanceof FiducialRenderer)
      {
        FiducialRenderer fiducialRenderer = (FiducialRenderer)renderer;
        Fiducial fiducial = fiducialRenderer.getFiducial();
        fiducials.add(fiducial);
      }
    }
    
    // if just the component was selected (not any specific pads), then do the following:
    for (com.axi.guiUtil.Renderer renderer : selections)
    {
      if (renderer instanceof ComponentRenderer)
      {
        ComponentRenderer componentRenderer = (ComponentRenderer)renderer;
        com.axi.v810.business.panelDesc.Component component = componentRenderer.getComponent();
        // Ying-Huan.Chu - [XCR-2037] subtask : handle panel-based recipe with overlapping boards.
        boolean shouldSkipThisComponent = false;
        java.util.List<Pad> padsInCurrentAlignmentGroup = new ArrayList<>();
        padsInCurrentAlignmentGroup.addAll(pads);
        padsInCurrentAlignmentGroup.addAll(_currentAlignmentGroup.getPads());
        for (Pad pad : padsInCurrentAlignmentGroup)
        {
          if (pad.getComponent().getBoard().getName().equals(component.getBoard().getName()) == false)
          {
            shouldSkipThisComponent = true;
            break;
          }
        }
        if (shouldSkipThisComponent)
        {
          continue;
        }
        if (component.getPads().size() < _maxPadsAndFiducials)
        {
          boolean foundPadForComponent = false;
          // first, make sure no pads for this guy are already in this list
          for (Pad pad : pads)
          {
            if (pad.getComponent() == component)
            {
              foundPadForComponent = true;
              break;
            }
          }
          if (foundPadForComponent == false)
            pads.addAll(component.getPads());
        }
      }
    }
    // if the selection has at least one pad or fiducial, some work will get done
    // so we'll clear the current selection in preparation for that work
    _currentAlignmentGroupList.clearSelection();

    // we'll do all fiducial first, then all pads.
    java.util.List<Fiducial> fiducialsToAdd = addFiducialsToCurrentAlignmentGroup(new ArrayList<Fiducial>(fiducials));

    // now, we have a list of pads that are candidates for adding to the current alignment groups.
    // If a pad fails for any reason, all other pads will NOT be tried.  This eliminates redundant error messages
    // about pads out range or too many pads, etc.
    // we also need to know about the fiducials that have passed checks, so we can use that data when checking pads (things
    // like space contraints, etc)
    java.util.List<Pad> padsToAdd = addPadsToCurrentAlignmentGroup(new ArrayList<Pad>(pads), fiducialsToAdd);
    
    //Khaw Chek Hau - XCR2181: Not able to run alignment from failed board
    //Whenever there are pads added to alignment group, reset the learned alignment flag
    _project.getTestProgram().clearRunAlignmentLearned();

    // some error was generated, so this will clear out any selections left over
//    if (fiducialsToAdd.isEmpty() && padsToAdd.isEmpty())
//      highlightAllAlignmentPoints();

    if(_project.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      try
      {
        _commandManager.execute(new AddFeaturesToAlignmentGroupCommand(_currentAlignmentGroup, padsToAdd, fiducialsToAdd));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
      }
    }
    else
    {
      try
      {
        _commandManager.execute(new AddFeaturesToBoardSettingsAlignmentGroupCommand(getCurrentAlignmentGroupIndex(), _project.getPanel(), padsToAdd, fiducialsToAdd));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                ex,
                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                true);
      }
    }       
  }

  /**
   * Handles information regarding a change in datastore.  Typically generated by someone doing a "set" when this
   * panel is not visible, but it should update itself anyway so when it becomes visible, data will be up to date.
   * @author Andy Mechtenberg
   */
  private void handleProjectEvent(final Object object)
  {
    if (_active == false)
      _updateData = true;
    else
    {
      Assert.expect(object instanceof ProjectChangeEvent,
                    "Got a non-ProjectChangeEvent in the ProjectObservable: " + object);
      ProjectChangeEvent datastoreEvent = (ProjectChangeEvent)object;
      ProjectChangeEventEnum eventEnum = datastoreEvent.getProjectChangeEventEnum();

      if (eventEnum instanceof ComponentTypeEventEnum)
      {
        ComponentTypeEventEnum componentTypeEventEnum = (ComponentTypeEventEnum)eventEnum;
        if (componentTypeEventEnum.equals(ComponentTypeEventEnum.CREATE_OR_DESTROY) || componentTypeEventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE))
        {
          if (datastoreEvent.isDestroyed() || datastoreEvent.isRemoved())
          {
            ComponentType destroyedComponentType = (ComponentType)datastoreEvent.getOldValue();
            deleteComponentTypeFromAlignmentGroups(destroyedComponentType);
          }
        }
        // Modified by Seng Yew on 11-Jan-2013
        // XCR1559 - Able to delete multiple components at once
        else if (componentTypeEventEnum.equals(ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST))
        {
          if (datastoreEvent.isRemoved())
          {
            java.util.List<ComponentType> destroyedComponentType = (java.util.List<ComponentType>)datastoreEvent.getOldValue();
            deleteComponentTypeFromAlignmentGroups(destroyedComponentType);
          }
        }
      }
      else if (eventEnum instanceof FiducialEventEnum)
      {
        FiducialEventEnum fiducialEventEnum = (FiducialEventEnum)eventEnum;
        if (fiducialEventEnum.equals(FiducialEventEnum.CREATE_OR_DESTROY) || fiducialEventEnum.equals(FiducialEventEnum.ADD_OR_REMOVE))
        {
          if (datastoreEvent.isDestroyed())
          {
            Fiducial destroyedFiducial = (Fiducial)datastoreEvent.getOldValue();
            deleteFiducialFromAlignmentGroup(destroyedFiducial);
          }
        }
      }
      else if (eventEnum instanceof FiducialTypeEventEnum)
      {
        FiducialTypeEventEnum fiducialTypeEventEnum = (FiducialTypeEventEnum)eventEnum;
        if (fiducialTypeEventEnum.equals(FiducialTypeEventEnum.CREATE_OR_DESTROY) || fiducialTypeEventEnum.equals(FiducialTypeEventEnum.ADD_OR_REMOVE))
        {
          if (datastoreEvent.isDestroyed())
          {
            FiducialType destroyedFiducialType = (FiducialType)datastoreEvent.getOldValue();
            java.util.List<Fiducial> destroyedFiducials = destroyedFiducialType.getFiducials();
            for (Fiducial destroyedFiducial : destroyedFiducials)
              deleteFiducialFromAlignmentGroup(destroyedFiducial);
          }
        }
      }
      else if (eventEnum instanceof AlignmentGroupEventEnum)
      {
        AlignmentGroup alignmentGroup = (AlignmentGroup)datastoreEvent.getSource();
        updateData(alignmentGroup);
      }
      else if (eventEnum instanceof ProjectStateEventEnum)
      {
        ProjectStateEventEnum projectStateEventEnum = (ProjectStateEventEnum)eventEnum;
        if (projectStateEventEnum.equals(ProjectStateEventEnum.ALIGNMENT_TRANSFORM_VALID))
        {
          if (_manualAlignmentInProgress == false)
            setRunAlignmentButtonState();
        }
        else if (projectStateEventEnum.equals(ProjectStateEventEnum.ALIGNMENT_TRANSFORM_INVALID))
        {
          if (_manualAlignmentInProgress == false)
            setRunAlignmentButtonState();
        }
      }
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      else if (eventEnum instanceof PanelSettingsEventEnum)
      {
        PanelSettingsEventEnum panelSettingsEventEnum = (PanelSettingsEventEnum)eventEnum;
        if (panelSettingsEventEnum.equals(PanelSettingsEventEnum.SET_ALIGNMENT_MODE))
        {
          update2DAlignmentState();
        }
      }
    }
  }

  /**
   * The only hardware events we care about is panel load and unload
   * @author Andy Mechtenberg
   */
  private void handleHardwareEvent(final Object arg)
  {
    Assert.expect(arg != null);
    if (arg instanceof HardwareEvent)
    {
      HardwareEvent event = (HardwareEvent)arg;
      HardwareEventEnum eventEnum = event.getEventEnum();
      // Xray Tester events
      if (eventEnum instanceof PanelHandlerEventEnum)
      {
        PanelHandlerEventEnum panelHandlerEventEnum = (PanelHandlerEventEnum)eventEnum;
        if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.LOAD_PANEL))
        {
          if (event.isStart() == false)
          {
            setRunAlignmentButtonState();
          }
        }
        else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.UNLOAD_PANEL))
        {
          if (event.isStart() == false)
          {
            setRunAlignmentButtonState();
          }
        }
      }
    }

  }

  /**
   * @author George A. David
   */
  private void startManualAlignment()
  {
    _manualAlignmentInProgress = true;
    _nextButton.setEnabled(true);
    _cancelButton.setEnabled(true);
    _manualAlignmentStatusLabel.setEnabled(true);
    _runAlignmentButton.setEnabled(false);
    _runAlignmentWithDifferentSliceButton.setEnabled(false);
    _clearAlignmentButton.setEnabled(false);
    _autoAlignmentButton.setEnabled(false);
    _alignmentGuideButton.setEnabled(false);
    //XCR-2290 disabled Auto Select Alignment Group and Alignment Settings button before adjustment to prevent uset
    //click on one of the button and produce unexpected result
    _autoSelectAlignmentGroupButton.setEnabled(false);
    _alignmentSettingButton.setEnabled(false);
    
    _runSurfaceMapAlignmentButton.setEnabled(false);
    _deleteSurfaceMapAlignmentButton.setEnabled(false);
    setSetupScreenEnabled(false);
    _mainUI.disableMenuAndToolBars();
    _testDev.disableTaskPanelNavigation();
    _runTimeCount = 1;
  }

  /**
   * @author George A. David
   */
  private void endManualAlignment()
  {
    _nextButton.setEnabled(false);
    _cancelButton.setEnabled(false);
    _manualAlignmentStatusLabel.setEnabled(false);
    setRunAlignmentButtonState();
    _manualAlignmentStatusLabel.setText(StringLocalizer.keyToString("MMGUI_NOT_ALIGNING_STATUS_KEY"));
    setSetupScreenEnabled(true);
    _manualAlignmentInProgress = false;
    _mainUI.closeAllDialogs();
    _mainUI.enableMenuAndToolBars();
    _testDev.enableTaskPanelNavigation();
    _testDev.showCadWindow();

    _viewAlignmentButton.setEnabled(true);
    _clearAlignmentButton.setEnabled(true);
    _autoAlignmentButton.setEnabled(true);
    _alignmentGuideButton.setEnabled(true);
    //XCR-2290 enable Auto Select Alignment Group and Alignment Settings button after adjustment
    _autoSelectAlignmentGroupButton.setEnabled(true);
    _alignmentSettingButton.setEnabled(true);
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    update2DAlignmentState();
            
    _mainUI.changeLightStackToStandby();

    // Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    if (TestDev.isInSurfaceMappingMode())
//      updateUsePspCheckBox();
  }

  /**
   * @author George A. David
   */
  private void handleInspectionEvent(final InspectionEvent inspectionEvent)
  {
    InspectionEventEnum eventEnum = inspectionEvent.getInspectionEventEnum();
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    if (eventEnum.equals(InspectionEventEnum.MANUAL_ALIGNMENT_STARTED) || eventEnum.equals(InspectionEventEnum.MANUAL_2D_ALIGNMENT_STARTED))
      startManualAlignment();
    else if (eventEnum.equals(InspectionEventEnum.MANUAL_ALIGNMENT_COMPLETED) || eventEnum.equals(InspectionEventEnum.MANUAL_2D_ALIGNMENT_COMPLETED))
      endManualAlignment();
    else if (eventEnum.equals(InspectionEventEnum.DISABLE_NEXT_BUTTON_BEFORE_IMAGE_CROPPED))
      _nextButton.setEnabled(false);
    else if (eventEnum.equals(InspectionEventEnum.ENABLE_NEXT_BUTTON_AFTER_IMAGE_CROPPED))
      _nextButton.setEnabled(true);
    else if (eventEnum.equals(InspectionEventEnum.WAITING_FOR_USER_TO_ALIGN_IMAGE) || eventEnum.equals(InspectionEventEnum.WAITING_FOR_USER_TO_CROP_IMAGE))
    {
      Assert.expect(inspectionEvent instanceof AlignmentInspectionEvent);
      AlignmentInspectionEvent alignmentInspectionEvent = (AlignmentInspectionEvent)inspectionEvent;
      String currentGroup = alignmentInspectionEvent.getAlignmentGroup().getName();
      if (_isLongPanel)
      {
        // for long panels, I need to put extra wording here to specify first or second region
        LocalizedString currentAlignmentStatus;
        if (_runTimeCount < 4) // first group
          currentAlignmentStatus = new LocalizedString("MMGUI_ALIGN_LONG_FIRST_STATUS_KEY", new Object[]
              {currentGroup});
        else
          currentAlignmentStatus = new LocalizedString("MMGUI_ALIGN_LONG_SECOND_STATUS_KEY", new Object[]
              {currentGroup});

        _manualAlignmentStatusLabel.setText(StringLocalizer.keyToString(currentAlignmentStatus));
        _runTimeCount++;
      }
      else
      {
        LocalizedString currentAlignmentStatus = new LocalizedString("MMGUI_ALIGN_STATUS_KEY", new Object[]
            {currentGroup});
        _manualAlignmentStatusLabel.setText(StringLocalizer.keyToString(currentAlignmentStatus));
      }
    }
    else if (eventEnum.equals(InspectionEventEnum.MANUAL_ALIGNMENT_VERIFICATION_STARTED))
    {
      // CR1050 fix by LeeHerng - I uncomment the existing code to show BusyDialogBox right
      // before showing Alignment Progress Dialog
      if(_busyCancelDialog == null)
      {
        _busyCancelDialog = new BusyCancelDialog(_mainUI,
                                               StringLocalizer.keyToString("MMGUI_ALIGN_VERIFICATION_KEY"),
                                               StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                               true);
        _busyCancelDialog.pack();
        SwingUtils.centerOnComponent(_busyCancelDialog, _mainUI);
      }
      _busyCancelDialog.setVisible(true);
    }
    else if (eventEnum.equals(InspectionEventEnum.MANUAL_ALIGNMENT_VERIFICATION_COMPLETED))
    {
      // CR1050 fix by LeeHerng - I uncomment the existing code to dispoose BusyDialogBox
      // after complete performing auto-alignment
      // wait until visible
      while (_busyCancelDialog == null || _busyCancelDialog.isVisible() == false)
      {
        try
        {
          Thread.sleep(200);
        }
        catch (InterruptedException ex)
        {
          // do nothing
        }
      }
      _busyCancelDialog.dispose();
    }
    else if (eventEnum.equals(InspectionEventEnum.ALIGNED_ON_IMAGE))
    {
      // do nothing
    }
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    else if (eventEnum.equals(InspectionEventEnum.ALIGNED_ON_2D_IMAGE))
    {
      // do nothing
    }
    else if (eventEnum.equals(InspectionEventEnum.PANEL_UNLOADED_FROM_MACHINE))
    {
      setRunAlignmentButtonState();
    }
    else if (eventEnum.equals(InspectionEventEnum.PANEL_LOADED_INTO_MACHINE))
    {
      setRunAlignmentButtonState();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void deleteComponentTypeFromAlignmentGroups(ComponentType componentType)
  {
    Assert.expect(componentType != null);

    String componentName = componentType.getReferenceDesignator();
    java.util.List<AlignmentGroup> alignmentGroups;
    if (_isLongPanel)
      alignmentGroups = _longPanelAlignmentGroups;
    else
      alignmentGroups = _shortPanelAlignmentGroups;

    for (AlignmentGroup alignmentGroup : alignmentGroups)
    {
      java.util.List<Pad> alignmentPads;

      alignmentPads = new ArrayList<Pad>(alignmentGroup.getPads());
      for (Pad pad : alignmentPads)
      {
        if (pad.getComponent().getReferenceDesignator().equalsIgnoreCase(componentName))
          alignmentGroup.removePad(pad);
      }
    }
  }

  /**
   * @author Seng Yew, XCR1559 - Able to delete multiple components at once
   * @author Andy Mechtenberg
   */
  private void deleteComponentTypeFromAlignmentGroups(java.util.List<ComponentType> componentTypeList)
  {
    Assert.expect(componentTypeList != null);

    java.util.List<String> componentNameList = new java.util.ArrayList<String>();
    Iterator componentTypeListIt = componentTypeList.iterator();
    while (componentTypeListIt.hasNext())
    {
      ComponentType componentType = (ComponentType)componentTypeListIt.next();
      componentNameList.add(componentType.getReferenceDesignator());
    }
    java.util.List<AlignmentGroup> alignmentGroups;
    if (_isLongPanel)
      alignmentGroups = _longPanelAlignmentGroups;
    else
      alignmentGroups = _shortPanelAlignmentGroups;

    for (AlignmentGroup alignmentGroup : alignmentGroups)
    {
      java.util.List<Pad> alignmentPads;

      alignmentPads = new ArrayList<Pad>(alignmentGroup.getPads());
      for (Pad pad : alignmentPads)
      {
        if (componentNameList.contains(pad.getComponent().getReferenceDesignator()))
          alignmentGroup.removePad(pad);
      }
      alignmentPads.clear();
    }
    componentNameList.clear();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void deleteFiducialFromAlignmentGroup(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);
    java.util.List<AlignmentGroup> alignmentGroups;
    if (_isLongPanel)
      alignmentGroups = _longPanelAlignmentGroups;
    else
      alignmentGroups = _shortPanelAlignmentGroups;

    for (AlignmentGroup alignmentGroup : alignmentGroups)
    {
      if (alignmentGroup.getFiducials().contains(fiducial))
        alignmentGroup.removeFiducial(fiducial);
    }
  }

  /**
   * Typically called when UNDO or REDO is invoked and this is the visible screen.
   * @author Andy Mechtenberg
   */
  private void updateData(AlignmentGroup alignmentGroup)
  {
    updateAlignmentGroup(alignmentGroup);
    _guiObservable.stateChanged(null, SelectionEventEnum.CLEAR_SELECTIONS);
    updateHighlightsAndPanelGui();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    // nothing to persist, so skip that step

//    if (_testDev != null && _project != null)
//    {
//      if (_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
//        _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().clearCurrentlySelectedComponentRendererSet();       
//      else
//        _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().clearCurrentlySelectedComponentRendererSet();
//    }
     
//    ProgramGeneration.resetCustomVerificationRegions(_project.getTestProgram());
  
    _project = null;
    _currentAlignmentGroup = null;
    _alignmentGroup1 = null;
    _alignmentGroup2 = null;
    _alignmentGroup3 = null;

    _alignmentGroup1Top = null;
    _alignmentGroup2Top = null;
    _alignmentGroup3Top = null;
    _alignmentGroup1Bottom = null;
    _alignmentGroup2Bottom = null;
    _alignmentGroup3Bottom = null;

    _longPanelAlignmentGroups.clear();
    _shortPanelAlignmentGroups.clear();
    _alignmentGroupToRadioButtonMap.clear();

    _allHighlights.clear();

    _alignmentGroupsCardLayout.show(_alignmentGroupsPanel, _PANEL_ALIGNMENT);
    _alignmentGroup1ListModel.clear();
    _alignmentGroup2ListModel.clear();
    _alignmentGroup3ListModel.clear();
    _alignmentGroup1TopListModel.clear();
    _alignmentGroup1BottomListModel.clear();
    _alignmentGroup2TopListModel.clear();
    _alignmentGroup2BottomListModel.clear();
    _alignmentGroup3TopListModel.clear();
    _alignmentGroup3BottomListModel.clear();
    _selectedRendererObservable = null;
    _dragOpticalRegionRendererObservable = null;
    _topRegionActive = true;
    _projectObservable.deleteObserver(this);
    _alignmentGroupNameToRegionPositionNameMap.clear();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void populateWithProjectData()
  {
    // generate the test program, because we'll need it
    _mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_ALIGNMENT_NEEDS_TEST_PROGRAM_KEY"),
                                                  StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                  _project);

    _isLongPanel = _project.getTestProgram().isLongProgram();
    if (_isLongPanel)
    {
      _alignmentGroupsCardLayout.show(_alignmentGroupsPanel, _LONG_PANEL_ALIGNMENT);
//      _alignmentGroupsPanel.setPreferredSize(_longPanelAlignmentPanel.getPreferredSize());
      // Take only once
      if (_longPanelBasedAlignmentPanelInitialSize == null)
      {
        _longPanelBasedAlignmentPanelInitialSize = new Dimension();
        _longPanelBasedAlignmentPanelInitialSize = _longPanelAlignmentPanel.getPreferredSize();
      }
      _alignmentGroupsPanel.setPreferredSize(_longPanelBasedAlignmentPanelInitialSize);

      _alignmentInstructionLabel.setText(StringLocalizer.keyToString("MMGUI_LONG_PANEL_ALIGN_INSTRUCTIONS_KEY"));
    }
    else
    {
      _alignmentGroupsCardLayout.show(_alignmentGroupsPanel, _PANEL_ALIGNMENT);
      // Take only once
      if (_panelBasedAlignmentPanelInitialSize == null)
      {
        _panelBasedAlignmentPanelInitialSize = new Dimension();
        _panelBasedAlignmentPanelInitialSize = _panelBasedAlignmentPanel.getPreferredSize();
      }
      _alignmentGroupsPanel.setPreferredSize(_panelBasedAlignmentPanelInitialSize);
      _alignmentInstructionLabel.setText(StringLocalizer.keyToString("MMGUI_PANEL_ALIGN_INSTRUCTIONS_KEY"));
    }
    populateAlignmentPointGroups();
    setRunAlignmentButtonState();
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    update2DAlignmentState();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateAlignmentStatusMessages()
  {
    // Ying-Huan.Chu [XCR-2599] - We have to reset the LowAndHighMagSettings so that the software will redo the hasManualAlignmentBeenPerformedForHighMag() checking.
    _project.getPanel().getPanelSettings().resetLowAndHighMagSettings();
    if ( _project.getPanel().getPanelSettings().isManualAlignmentCompleted())
    {
      _manualAlignmentMatrixStatusLabel.setText(StringLocalizer.keyToString("MMGUI_ALIGN_MANUAL_ALIGN_VALID_KEY"));
      _manualAlignmentMatrixInstructionLabel.setText(StringLocalizer.keyToString(new LocalizedString(
          "MMGUI_ALIGN_MANUAL_ALIGN_VALID_INSTRUCTION_KEY",
          new Object[]
          {StringLocalizer.keyToString("MMGUI_ALIGN_RUN_KEY")})));

    }
    else
    {
      _manualAlignmentMatrixStatusLabel.setText(StringLocalizer.keyToString("MMGUI_ALIGN_MANUAL_ALIGN_NOT_VALID_KEY"));
      _manualAlignmentMatrixInstructionLabel.setText(StringLocalizer.keyToString(new LocalizedString(
          "MMGUI_ALIGN_MANUAL_ALIGN_NOT_VALID_INSTRUCTION_KEY",
          new Object[]
          {StringLocalizer.keyToString("MMGUI_ALIGN_RUN_KEY")})));

    }
  }

  /**
   * @author George Booth
   */
  public void populateWithProjectData(Project project)
  {
    Assert.expect(project != null);
    _project = project;

    if (_active)
    {
      populateWithProjectData();
      _populateProjectData = false;
    }
    else
    {
      _populateProjectData = true;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    this.removeAll();
    _runAlignmentPanelBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
    setLayout(_alignmentVerticalFlowLayout);
    _alignmentGroupsPanel.setBorder(BorderFactory.createEmptyBorder(7, 5, 7, 5)); // apm was 20 on left
    _alignmentGroupsPanel.setOpaque(false);
    _alignmentGroupsPanel.setLayout(_alignmentGroupsCardLayout);
    setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(165, 163, 151)),
        StringLocalizer.keyToString("MMGUI_ALIGN_GROUPS_KEY")), BorderFactory.createEmptyBorder(0, 5, 0, 5)));
    _listSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        alignmentListSelectionChanged(e);
      }
    };

    _alignmentGroup1RadioButtonListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        alignmentGroup1RadioButton_actionPerformed(e);
      }
    };
    _alignmentGroup1TopRadioButtonListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        alignmentGroup1TopRadioButton_actionPerformed(e);
      }
    };

    // alignment group 1
    _alignmentGroup1Panel.setLayout(_alignmentGroup1BorderLayout);
    _alignmentGroup1RadioButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_GROUP_1_KEY"));
    _alignmentGroup1RadioButton.addActionListener(_alignmentGroup1RadioButtonListener);
    _alignmentGroup1List.setModel(_alignmentGroup1ListModel);
    _alignmentGroup1List.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _alignmentGroup1List.setEnabled(false);
    _alignmentGroup1List.addListSelectionListener(_listSelectionListener);
    // add the mouse listener so when the JList is clicked it will enable itself
    _alignmentGroup1List.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        alignmentGroupList_mouseReleased(_alignmentGroup1RadioButton);
      }
    });
    _alignmentGroup1List.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });
    _alignmentGroup1RadioButton.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    // alignment group 2
    _alignmentGroup2Panel.setLayout(_alignmentGroup2BorderLayout);
    _alignmentGroup2RadioButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_GROUP_2_KEY"));
    _alignmentGroup2RadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        alignmentGroup2RadioButton_actionPerformed(e);
      }
    });
    _alignmentGroup2RadioButton.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    _alignmentGroup2List.setModel(_alignmentGroup2ListModel);
    _alignmentGroup2List.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _alignmentGroup2List.setEnabled(false);
    _alignmentGroup2List.addListSelectionListener(_listSelectionListener);
    _alignmentGroup2List.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        alignmentGroupList_mouseReleased(_alignmentGroup2RadioButton);
      }
    });
    _alignmentGroup2List.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    // aligment group 3
    _alignmentGroup3Panel.setLayout(_alignmentGroup3BorderLayout);
    _alignmentGroup3RadioButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_GROUP_3_KEY"));
    _alignmentGroup3RadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        alignmentGroup3RadioButton_actionPerformed(e);
      }
    });
    _alignmentGroup3RadioButton.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    _alignmentGroup3List.setModel(_alignmentGroup3ListModel);
    _alignmentGroup3List.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _alignmentGroup3List.setEnabled(false);
    _alignmentGroup3List.addListSelectionListener(_listSelectionListener);
    _alignmentGroup3List.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        alignmentGroupList_mouseReleased(_alignmentGroup3RadioButton);
      }
    });
    _alignmentGroup3List.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    // now, we'll set up the 6 groups for long panel alignment
    // I've organized them into 3 top and 3 bottom groups
    // alignment top group 1
    _alignmentGroup1TopPanel.setLayout(_alignmentGroup1TopBorderLayout);
    _alignmentGroup1TopRadioButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_GROUP_1_KEY"));
    _alignmentGroup1TopRadioButton.addActionListener(_alignmentGroup1TopRadioButtonListener);
    _alignmentGroup1TopList.setModel(_alignmentGroup1TopListModel);
    _alignmentGroup1TopList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _alignmentGroup1TopList.setEnabled(false);
    _alignmentGroup1TopList.addListSelectionListener(_listSelectionListener);
    // add the mouse listener so when the JList is clicked it will enable itself
    _alignmentGroup1TopList.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        longAlignmentGroupList_mouseReleased(_alignmentGroup1TopRadioButton);
      }
    });
    _alignmentGroup1TopList.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });
    _alignmentGroup1TopRadioButton.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    // alignment top group 2
    _alignmentGroup2TopPanel.setLayout(_alignmentGroup2TopBorderLayout);
    _alignmentGroup2TopRadioButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_GROUP_2_KEY"));
    _alignmentGroup2TopRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        alignmentGroup2TopRadioButton_actionPerformed(e);
      }
    });
    _alignmentGroup2TopRadioButton.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    _alignmentGroup2TopList.setModel(_alignmentGroup2TopListModel);
    _alignmentGroup2TopList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _alignmentGroup2TopList.setEnabled(false);
    _alignmentGroup2TopList.addListSelectionListener(_listSelectionListener);
    // add the mouse listener so when the JList is clicked it will enable itself
    _alignmentGroup2TopList.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        longAlignmentGroupList_mouseReleased(_alignmentGroup2TopRadioButton);
      }
    });
    _alignmentGroup2TopList.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    // alignment top group 3
    _alignmentGroup3TopPanel.setLayout(_alignmentGroup3TopBorderLayout);
    _alignmentGroup3TopRadioButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_GROUP_3_KEY"));
    _alignmentGroup3TopRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        alignmentGroup3TopRadioButton_actionPerformed(e);
      }
    });
    _alignmentGroup3TopRadioButton.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    _alignmentGroup3TopList.setModel(_alignmentGroup3TopListModel);
    _alignmentGroup3TopList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _alignmentGroup3TopList.setEnabled(false);
    _alignmentGroup3TopList.addListSelectionListener(_listSelectionListener);
    // add the mouse listener so when the JList is clicked it will enable itself
    _alignmentGroup3TopList.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        longAlignmentGroupList_mouseReleased(_alignmentGroup3TopRadioButton);
      }
    });
    _alignmentGroup3TopList.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    // alignment bottom group 1
    _alignmentGroup1BottomPanel.setLayout(_alignmentGroup1BottomBorderLayout);
    _alignmentGroup1BottomRadioButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_GROUP_1_KEY"));
    _alignmentGroup1BottomRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        alignmentGroup1BottomRadioButton_actionPerformed(e);
      }
    });
    _alignmentGroup1BottomRadioButton.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    _alignmentGroup1BottomList.setModel(_alignmentGroup1BottomListModel);
    _alignmentGroup1BottomList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _alignmentGroup1BottomList.setEnabled(false);
    _alignmentGroup1BottomList.addListSelectionListener(_listSelectionListener);
    // add the mouse listener so when the JList is clicked it will enable itself
    _alignmentGroup1BottomList.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        longAlignmentGroupList_mouseReleased(_alignmentGroup1BottomRadioButton);
      }
    });
    _alignmentGroup1BottomList.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    // alignment bottom group 2
    _alignmentGroup2BottomPanel.setLayout(_alignmentGroup2BottomBorderLayout);
    _alignmentGroup2BottomRadioButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_GROUP_2_KEY"));
    _alignmentGroup2BottomRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        alignmentGroup2BottomRadioButton_actionPerformed(e);
      }
    });
    _alignmentGroup2BottomRadioButton.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    _alignmentGroup2BottomList.setModel(_alignmentGroup2BottomListModel);
    _alignmentGroup2BottomList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _alignmentGroup2BottomList.setEnabled(false);
    _alignmentGroup2BottomList.addListSelectionListener(_listSelectionListener);
    // add the mouse listener so when the JList is clicked it will enable itself
    _alignmentGroup2BottomList.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        longAlignmentGroupList_mouseReleased(_alignmentGroup2BottomRadioButton);
      }
    });
    _alignmentGroup2BottomList.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    // alignment bottom group 3
    _alignmentGroup3BottomPanel.setLayout(_alignmentGroup3BottomBorderLayout);
    _alignmentGroup3BottomRadioButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_GROUP_3_KEY"));
    _alignmentGroup3BottomRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        alignmentGroup3BottomRadioButton_actionPerformed(e);
      }
    });
    _alignmentGroup3BottomRadioButton.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    _alignmentGroup3BottomList.setModel(_alignmentGroup3BottomListModel);
    _alignmentGroup3BottomList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _alignmentGroup3BottomList.setEnabled(false);
    _alignmentGroup3BottomList.addListSelectionListener(_listSelectionListener);
    // add the mouse listener so when the JList is clicked it will enable itself
    _alignmentGroup3BottomList.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        longAlignmentGroupList_mouseReleased(_alignmentGroup3BottomRadioButton);
      }
    });
    _alignmentGroup3BottomList.addKeyListener(new KeyAdapter()
    {
      public void keyReleased(KeyEvent e)
      {
        alignmentGroupList_keyTyped(e);
      }
    });

    _alignmentTypeChoicesPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 7)); // apm was 20 on left
    _alignmentTypeChoicesPanel.setOpaque(false);
    _alignmentTypeChoicesPanel.setLayout(_alignmentOptionsVerticalFlowLayout);
    _alignmentTargetPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 5, 7));
    _alignmentTargetPanel.setOpaque(false);
    _alignmentTargetPanel.setLayout(_alignmentTargetVerticalFlowLayout);
    _alignmentTargetSelectionLabel.setText(StringLocalizer.keyToString("MMGUI_ALIGN_TARGET_LABEL_KEY") + ":  ");

    _manualAlignmentNavigationPanel.setLayout(_manualAlignmentNavigationPanelFlowLayout);
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _alignmentMethodGridLayout.setColumns(2);
    _alignmentMethodGridLayout.setHgap(-1);
    _alignmentMethodGridLayout.setRows(2);
    _alignmentMethodGridLayout.setVgap(-1);
    _alignmentMethodPanel.setLayout(_alignmentMethodGridLayout);
    _alignmentMethodPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _alignmentMethodPanelFlowLayout.setHgap(10);
    _alignmentMethodPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white, new Color(165, 163, 151)),
        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_ALIGNMENT_METHOD_KEY")));
    
    _manualAlignmentPanel.setLayout(_manualAlignmentPanelVerticalFlowLayout);
    _manualAlignmentPanelVerticalFlowLayout.setVgap(1);
    _manualAlignmentPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white, new Color(165, 163, 151)),
        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY")));
    _manualAlignmentPanel.setOpaque(false);
    _manualAlignmentStatusPanelFlowLayout.setAlignment(FlowLayout.LEFT);
//    _manualAlignmentPanelGridLayout.setColumns(1);
//    _manualAlignmentPanelGridLayout.setRows(4);
    _manualAlignmentMatrixStatusPanel.setLayout(_manualAlignmentMatrixStatusPanelFlowLayout);
    _manualAlignmentStatusPanel.setLayout(_manualAlignmentStatusPanelFlowLayout);
    _manualAlignmentNavigationPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _manualAlignmentStatusLabel.setEnabled(false);
    _manualAlignmentStatusLabel.setText(StringLocalizer.keyToString("MMGUI_NOT_ALIGNING_STATUS_KEY"));
    _alignmentButtonsPanel.setLayout(_alignmentButtonsFlowLayout);
    _alignmentButtonsPanel.setBorder(_runAlignmentPanelBorder);
    _alignmentButtonsPanel.setOpaque(false);
    _runAlignmentButton.setText(StringLocalizer.keyToString("MMGUI_ALIGN_RUN_KEY"));
    _runAlignmentButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        runAlignmentButton_actionPerformed(e);
      }
    });

    _runAlignmentWithDifferentSliceButton.setText(StringLocalizer.keyToString("MMGUI_ALIGN_WDS_RUN_KEY"));
    _runAlignmentWithDifferentSliceButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        runAlignmentWithDifferentSliceButton_actionPerformed(e);
      }
    });

    _clearAlignmentButton.setText(StringLocalizer.keyToString("MMGUI_ALIGN_WDS_CLEAR_KEY"));
    _clearAlignmentButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        clearAlignmentButton_actionPerformed(e);
      }
    });

    _viewAlignmentButton.setText(StringLocalizer.keyToString("MMGUI_ALIGN_VIEW_KEY"));
    _viewAlignmentButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        viewAlignmentButton_actionPerformed();
      }
    });

    _autoAlignmentButton.setText(StringLocalizer.keyToString("MMGUI_AUTO_ALIGN_KEY"));
    _autoAlignmentButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        autoAlignmentButton_actionPerformed(e);
      }
    });

    _alignmentGuideButton.setText(StringLocalizer.keyToString("MMGUI_ALIGN_INFO_KEY"));
    _alignmentGuideButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        guideForAlignmentButton_actionPerformed(e);
      }
    });
    
    _autoSelectAlignmentGroupButton.setText(StringLocalizer.keyToString("GUI_AUTO_SELECT_ALIGNMENT_GROUP_BUTTON_KEY"));
    _autoSelectAlignmentGroupButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
                                                                      StringLocalizer.keyToString("GUI_AUTO_SELECT_ALIGNMENT_GROUP_BUSYDIALOG_MESSAGE_KEY"),
                                                                      StringLocalizer.keyToString("GUI_AUTO_SELECT_ALIGNMENT_GROUP_BUSYDIALOG_TITLE_KEY"),
                                                                      true);
        busyCancelDialog.pack();
        SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
        SwingWorkerThread.getInstance().invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              autoSelectAlignmentGroupButton_actionPerformed();
            }
            finally
            {
              // wait until visible
              while (busyCancelDialog.isVisible() == false)
              {
                try
                {
                  Thread.sleep(200);
                }
                catch (InterruptedException ex)
                {
                  // do nothing
                }
              }
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  busyCancelDialog.dispose();
                }
              });
            }
          }
        });
        busyCancelDialog.setVisible(true);
      }
    });

    _alignmentButtonsFlowLayout.setAlignment(FlowLayout.LEFT);
    _alignmentButtonsFlowLayout.setHgap(0);
    _alignmentButtonsFlowLayout.setVgap(0);
    _innerButtonsPanel.setLayout(_innerButtonsPanelGridLayout);
    _cancelButton.setEnabled(false);
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        manualAlignmentCancelButton_actionPerformed(e);
      }
    });

    _nextButton.setEnabled(false);
    _nextButton.setText(StringLocalizer.keyToString("MMGUI_ALIGN_MANUAL_ALIGN_DONE_BUTTON_KEY"));
    _nextButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        manualAlignmentNextButton_actionPerformed(e);
      }
    });
    _innerButtonsPanelGridLayout.setHgap(15);
    _innerButtonsPanelGridLayout.setVgap(15);
    _innerAlignButtonGridLayout.setHgap(15);
    _innerButtonsPanel.setOpaque(false);
    _manualAlignmentNavigationPanel.setOpaque(false);
    _manualAlignmentStatusPanel.setOpaque(false);
    _alignmentGroup3Panel.setOpaque(false);
    _alignmentGroup2Panel.setOpaque(false);
    _alignmentGroup1Panel.setOpaque(false);
    _panelBasedAlignmentPanel.setLayout(_panelBasedAlignmentPanelGridLayout);
    _panelBasedAlignmentPanelGridLayout.setColumns(3);
    _panelBasedAlignmentPanelGridLayout.setHgap(5);
    _panelBasedAlignmentPanelGridLayout.setVgap(7);
    _deleteAlignmentPointPanel.setLayout(_deleteAlignmentPointPanelFlowLayout);
    _deleteAlignmentPointPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _deleteAlignmentPointButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteAlignmentPoint_actionPerformed();
      }
    });
    _alignmentGroup1ListScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
    _alignmentGroup2ListScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
    _alignmentGroup3ListScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
    _longPanelAlignmentPanel.setLayout(_longPanelAlignmentGridLayout);
    _alignmentGroup1TopPanel.setOpaque(false);

    _alignmentGroup1TopScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);

    _longPanelAlignmentGridLayout.setRows(2);
    _longPanelAlignmentGridLayout.setVgap(5);
    _topRegionAlignmentPanel.setLayout(_topRegionAlignmentBorderLayout);
    _topRegionLabel.setText(StringLocalizer.keyToString("MMGUI_LONG_PANEL_TOP_REGION_KEY"));
    _topGroupsPanel.setLayout(_topGroupsGridLayout);
    _topGroupsGridLayout.setHgap(5);
    _bottomGroupsGridLayout.setHgap(5);
    _bottomRegionAlignmentPanel.setLayout(_bottomRegionAlignmentBorderLayout);
    _bottomRegionLabel.setText(StringLocalizer.keyToString("MMGUI_LONG_PANEL_BOTTOM_REGION_KEY"));
    _bottomGroupsPanel.setLayout(_bottomGroupsGridLayout);
    _topGroupsPanel.add(_alignmentGroup1TopPanel);
    _topGroupsPanel.add(_alignmentGroup2TopPanel);
    _topGroupsPanel.add(_alignmentGroup3TopPanel);
    _bottomGroupsPanel.add(_alignmentGroup1BottomPanel);
    _bottomGroupsPanel.add(_alignmentGroup2BottomPanel);
    _bottomGroupsPanel.add(_alignmentGroup3BottomPanel);

    _alignmentInstructionLabel.setFont(FontUtil.getBoldFont(_alignmentInstructionLabel.getFont(),Localization.getLocale()));
    _alignmentTypeChoicesPanel.add(_alignmentInstructionLabel, null); //    _alignmentTypeChoicesPanel.add(_horizontalSeparator, null);
    add(_alignmentTypeChoicesPanel, null);
//    add(_alignmentTargetPanel, null);
    add(_alignmentGroupsPanel, null);
    _alignmentTargetPanel.add(_alignmentTargetSelectionLabel, null);
    _alignmentTargetPanel.add(_alignmentTargetSelectionComboBox, null);

    _alignGroupsButtonGroup.add(_alignmentGroup1RadioButton);
    _alignGroupsButtonGroup.add(_alignmentGroup2RadioButton);
    _alignGroupsButtonGroup.add(_alignmentGroup3RadioButton);

    // do the long panel alignment stuff (it's all handled sepately)
    _longPanelAlignGroupsButtonGroup.add(_alignmentGroup1TopRadioButton);
    _longPanelAlignGroupsButtonGroup.add(_alignmentGroup2TopRadioButton);
    _longPanelAlignGroupsButtonGroup.add(_alignmentGroup3TopRadioButton);
    _longPanelAlignGroupsButtonGroup.add(_alignmentGroup1BottomRadioButton);
    _longPanelAlignGroupsButtonGroup.add(_alignmentGroup2BottomRadioButton);
    _longPanelAlignGroupsButtonGroup.add(_alignmentGroup3BottomRadioButton);

//    _alignTargetButtonGroup.add(_panelAlignmentRadioButton);
//    _alignTargetButtonGroup.add(_boardAlignmentRadioButton);
    _deleteAlignmentPointPanel.add(_deleteAlignmentPointButton);
    _deleteAlignmentPointButton.setText(StringLocalizer.keyToString("MMGUI_ALIGN_DELETE_ROWS_KEY"));
    _deleteAlignmentPointButton.setEnabled(false);
    add(_deleteAlignmentPointPanel, null);

    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    add(_alignmentMethodPanel, null);
    _alignmentMethodPanel.add(_alignment3DRadioButton);
    _alignmentMethodPanel.add(_alignment2DRadioButton);
    _alignmentModeButtonGroup.add(_alignment3DRadioButton);
    _alignmentModeButtonGroup.add(_alignment2DRadioButton);
    add(_manualAlignmentPanel, null);
    _manualAlignmentNavigationPanel.add(_innerButtonsPanel, null);
    _innerButtonsPanel.add(_nextButton, null);
    _innerButtonsPanel.add(_cancelButton, null);
    // apm commented out refernence to board level run time manual alignment
    _manualAlignmentPanel.add(_manualAlignmentMatrixStatusPanel, null);
    _manualAlignmentMatrixStatusPanel.add(_manualAlignmentMatrixStatusLabel, null);
    _manualAlignmentMatrixStatusPanel.add(_manualAlignmentMatrixInstructionLabel, null);
    JPanel innerAlignmentButtonPanel = new JPanel(_innerAlignButtonGridLayout);
    innerAlignmentButtonPanel.add(_runAlignmentButton, 0);
    innerAlignmentButtonPanel.add(_viewAlignmentButton, 1);
    innerAlignmentButtonPanel.add(_runAlignmentWithDifferentSliceButton, 2);
    innerAlignmentButtonPanel.add(_alignmentGuideButton, 3);
    innerAlignmentButtonPanel.add(_clearAlignmentButton, 4);
    innerAlignmentButtonPanel.add(_autoSelectAlignmentGroupButton, 5);
//    innerAlignmentButtonPanel.add(_autoAlignmentButton, null);
    _alignmentButtonsPanel.add(innerAlignmentButtonPanel, null);

    _manualAlignmentPanel.add(_alignmentButtonsPanel, null);
//    _alignmentButtonsPanel.add(_runAlignmentButton, null);
//    _alignmentButtonsPanel.add(_viewAlignmentButton, null);
    _manualAlignmentPanel.add(_manualAlignmentStatusPanel, null);
    _manualAlignmentStatusPanel.add(_manualAlignmentStatusLabel, null);
    _manualAlignmentPanel.add(_manualAlignmentNavigationPanel, null);
    _alignmentGroup1Panel.add(_alignmentGroup1RadioButton, java.awt.BorderLayout.NORTH);
    _alignmentGroup1ListScrollPane.getViewport().add(_alignmentGroup1List);
    _alignmentGroup2Panel.add(_alignmentGroup2RadioButton, java.awt.BorderLayout.NORTH);
    _alignmentGroup2ListScrollPane.getViewport().add(_alignmentGroup2List);

    _alignmentGroupsPanel.add(_panelBasedAlignmentPanel, _PANEL_ALIGNMENT);
    _panelBasedAlignmentPanel.add(_alignmentGroup1Panel);
    _panelBasedAlignmentPanel.add(_alignmentGroup2Panel);
    _panelBasedAlignmentPanel.add(_alignmentGroup3Panel);

    _alignmentGroupsPanel.add(_longPanelAlignmentPanel, _LONG_PANEL_ALIGNMENT);
    _alignmentGroup1TopScrollPane.getViewport().add(_alignmentGroup1TopList);
    _alignmentGroup3Panel.add(_alignmentGroup3RadioButton, java.awt.BorderLayout.NORTH);
    _alignmentGroup3ListScrollPane.getViewport().add(_alignmentGroup3List);
    _alignmentGroup1Panel.add(_alignmentGroup1ListScrollPane, java.awt.BorderLayout.CENTER);
    _alignmentGroup2Panel.add(_alignmentGroup2ListScrollPane, java.awt.BorderLayout.CENTER);
    _alignmentGroup3Panel.add(_alignmentGroup3ListScrollPane, java.awt.BorderLayout.CENTER);
    _alignmentGroup1TopPanel.add(_alignmentGroup1TopScrollPane, java.awt.BorderLayout.CENTER);
    _alignmentGroup1TopPanel.add(_alignmentGroup1TopRadioButton, java.awt.BorderLayout.NORTH);
    _longPanelAlignmentPanel.add(_topRegionAlignmentPanel);
    _longPanelAlignmentPanel.add(_bottomRegionAlignmentPanel);
    _alignmentGroup3TopScrollPane.getViewport().add(_alignmentGroup3TopList);
    _alignmentGroup2TopScrollPane.getViewport().add(_alignmentGroup2TopList);
    _alignmentGroup3BottomScrollPane.getViewport().add(_alignmentGroup3BottomList);
    _alignmentGroup2BottomScrollPane.getViewport().add(_alignmentGroup2BottomList);
    _alignmentGroup1BottomScrollPane.getViewport().add(_alignmentGroup1BottomList);
    _alignmentGroup2TopPanel.add(_alignmentGroup2TopScrollPane, java.awt.BorderLayout.CENTER);
    _alignmentGroup2TopPanel.add(_alignmentGroup2TopRadioButton, java.awt.BorderLayout.NORTH);
    _alignmentGroup3TopPanel.add(_alignmentGroup3TopScrollPane, java.awt.BorderLayout.CENTER);
    _alignmentGroup3TopPanel.add(_alignmentGroup3TopRadioButton, java.awt.BorderLayout.NORTH);
    _alignmentGroup1BottomPanel.add(_alignmentGroup1BottomScrollPane, java.awt.BorderLayout.CENTER);
    _alignmentGroup1BottomPanel.add(_alignmentGroup1BottomRadioButton, java.awt.BorderLayout.NORTH);
    _alignmentGroup2BottomPanel.add(_alignmentGroup2BottomScrollPane, java.awt.BorderLayout.CENTER);
    _alignmentGroup2BottomPanel.add(_alignmentGroup2BottomRadioButton, java.awt.BorderLayout.NORTH);
    _alignmentGroup3BottomPanel.add(_alignmentGroup3BottomScrollPane, java.awt.BorderLayout.CENTER);
    _alignmentGroup3BottomPanel.add(_alignmentGroup3BottomRadioButton, java.awt.BorderLayout.NORTH);
    _topRegionAlignmentPanel.add(_topRegionLabel, java.awt.BorderLayout.NORTH);
    _topRegionAlignmentPanel.add(_topGroupsPanel, java.awt.BorderLayout.CENTER);
    _bottomRegionAlignmentPanel.add(_bottomGroupsPanel, java.awt.BorderLayout.CENTER);
    _bottomRegionAlignmentPanel.add(_bottomRegionLabel, java.awt.BorderLayout.NORTH);
    updateAlignmentSetColors(true);
    _radioButtonToListMap.put(_alignmentGroup1RadioButton, _alignmentGroup1List);
    _radioButtonToListMap.put(_alignmentGroup2RadioButton, _alignmentGroup2List);
    _radioButtonToListMap.put(_alignmentGroup3RadioButton, _alignmentGroup3List);

    _radioButtonToListMap.put(_alignmentGroup1TopRadioButton, _alignmentGroup1TopList);
    _radioButtonToListMap.put(_alignmentGroup2TopRadioButton, _alignmentGroup2TopList);
    _radioButtonToListMap.put(_alignmentGroup3TopRadioButton, _alignmentGroup3TopList);
    _radioButtonToListMap.put(_alignmentGroup1BottomRadioButton, _alignmentGroup1BottomList);
    _radioButtonToListMap.put(_alignmentGroup2BottomRadioButton, _alignmentGroup2BottomList);
    _radioButtonToListMap.put(_alignmentGroup3BottomRadioButton, _alignmentGroup3BottomList);

    _alignmentGroup1RadioButton.setOpaque(true);
    _alignmentGroup2RadioButton.setOpaque(true);
    _alignmentGroup3RadioButton.setOpaque(true);
    _alignmentGroup1TopRadioButton.setOpaque(true);
    _alignmentGroup2TopRadioButton.setOpaque(true);
    _alignmentGroup3TopRadioButton.setOpaque(true);
    _alignmentGroup1BottomRadioButton.setOpaque(true);
    _alignmentGroup2BottomRadioButton.setOpaque(true);
    _alignmentGroup3BottomRadioButton.setOpaque(true);
    
    _deleteAlignmentPointButton.setName(".deleteAlignmentPointButton");
    _runAlignmentButton.setName(".runAlignmentButton");
    _runAlignmentWithDifferentSliceButton.setName(".runAlignmentWithDifferentSliceButton");
    _cancelButton.setName(".cancelButton");
    _nextButton.setName(".nextButton");
    _autoSelectAlignmentGroupButton.setName("Auto Select Alignment Group");
    
    // Wei Chin added for Use Z-height Only at Alignment...
    if(_hasAlignmentGroupSetting)
    {
      _alignmentSettingButton.setText(StringLocalizer.keyToString("MMGUI_ALIGNMENT_SETTING_KEY"));
      _alignmentSettingButton.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          alignmentSettingButton_actionPerformed(e);
        }
      });
      innerAlignmentButtonPanel.add(_alignmentSettingButton, 5);
    }  
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _alignment3DRadioButton.setText(StringLocalizer.keyToString("MMGUI_USE_3D_ALIGNMENT_KEY"));
    _alignment3DRadioButton.setEnabled(true);
    _alignment3DRadioButton.addActionListener(_alignment3DRadioButtonActionListener);
   
    
    _alignment2DRadioButton.setText(StringLocalizer.keyToString("MMGUI_USE_2D_ALIGNMENT_KEY"));
    _alignment2DRadioButton.setEnabled(true);
    _alignment2DRadioButton.addActionListener(_alignment2DRadioButtonActionListener);
    
    _alignment3DRadioButtonActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        alignmentModeRadioButton_actionPerformed();
      }
    };
    _alignment3DRadioButton.addActionListener(_alignment3DRadioButtonActionListener);
    
    _alignment2DRadioButtonActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        alignmentModeRadioButton_actionPerformed();
      }
    };
    _alignment2DRadioButton.addActionListener(_alignment2DRadioButtonActionListener);
    
    // Commented by Lee Herng, 29 Aug 2014
    // This is because PSP alignment is still un-stable.
    /**
    if (TestDev.isInSurfaceMappingMode())
    {
      // Add radio button for alignment with PSP selection////////
      JPanel pspPanel = new JPanel(_innerAlignButtonGridLayout);

      pspPanel.setBorder(BorderFactory.createEmptyBorder(7, 15, 7, 15));

      _group1PspCheckBox.setText("Use Psp");
      _group2PspCheckBox.setText("Use Psp");
      _group3PspCheckBox.setText("Use Psp");

      _topGroup1PspCheckBox.setText("Use Psp");
      _topGroup2PspCheckBox.setText("Use Psp");
      _topGroup3PspCheckBox.setText("Use Psp");

      _bottomGroup1PspCheckBox.setText("Use Psp");
      _bottomGroup2PspCheckBox.setText("Use Psp");
      _bottomGroup3PspCheckBox.setText("Use Psp");

      _alignmentGroup1Panel.add(_group1PspCheckBox, java.awt.BorderLayout.SOUTH);
      _group1PspCheckBox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          pspCheckBox_actionPerformed(_group1PspCheckBox, _currentAlignmentGroup);
          updateCadGraphics();
          drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
        }
      });

      _alignmentGroup2Panel.add(_group2PspCheckBox, java.awt.BorderLayout.SOUTH);
      _group2PspCheckBox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          pspCheckBox_actionPerformed(_group2PspCheckBox, _currentAlignmentGroup);
          updateCadGraphics();
          drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
        }
      });

      _alignmentGroup3Panel.add(_group3PspCheckBox, java.awt.BorderLayout.SOUTH);
      _group3PspCheckBox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          pspCheckBox_actionPerformed(_group3PspCheckBox, _currentAlignmentGroup);
          updateCadGraphics();
          drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
        }
      });

      _alignmentGroup1TopPanel.add(_topGroup1PspCheckBox, java.awt.BorderLayout.SOUTH);
      _topGroup1PspCheckBox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          pspCheckBox_actionPerformed(_topGroup1PspCheckBox, _currentAlignmentGroup);
          updateCadGraphics();
          drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
        }
      });

      _alignmentGroup2TopPanel.add(_topGroup2PspCheckBox, java.awt.BorderLayout.SOUTH);
      _topGroup2PspCheckBox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          pspCheckBox_actionPerformed(_topGroup2PspCheckBox, _currentAlignmentGroup);
          updateCadGraphics();
          drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
        }
      });

      _alignmentGroup3TopPanel.add(_topGroup3PspCheckBox, java.awt.BorderLayout.SOUTH);
      _topGroup3PspCheckBox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          pspCheckBox_actionPerformed(_topGroup3PspCheckBox, _currentAlignmentGroup);
          updateCadGraphics();
          drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
        }
      });

      _alignmentGroup1BottomPanel.add(_bottomGroup1PspCheckBox, java.awt.BorderLayout.SOUTH);
      _bottomGroup1PspCheckBox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          pspCheckBox_actionPerformed(_bottomGroup1PspCheckBox, _currentAlignmentGroup);
          updateCadGraphics();
          drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
        }
      });

      _alignmentGroup2BottomPanel.add(_bottomGroup2PspCheckBox, java.awt.BorderLayout.SOUTH);
      _bottomGroup2PspCheckBox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          pspCheckBox_actionPerformed(_bottomGroup2PspCheckBox, _currentAlignmentGroup);
          updateCadGraphics();
          drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
        }
      });

      _alignmentGroup3BottomPanel.add(_bottomGroup3PspCheckBox, java.awt.BorderLayout.SOUTH);
      _bottomGroup3PspCheckBox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          pspCheckBox_actionPerformed(_bottomGroup3PspCheckBox, _currentAlignmentGroup);
          updateCadGraphics();
          drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
        }
      });

      if (_alignmentGroup1List.getVisibleRowCount() > 0)
      {
        _group1PspCheckBox.setEnabled(true);
      }
      else
      {
        _group1PspCheckBox.setEnabled(false);
      }

      _group2PspCheckBox.setEnabled(false);
      _group3PspCheckBox.setEnabled(false);

      pspPanel.add(_runSurfaceMapAlignmentButton, 0);
      _runSurfaceMapAlignmentButton.setText(StringLocalizer.keyToString("PSP_SURFACE_MAP_ALIGN_RUN_KEY"));
      _runSurfaceMapAlignmentButton.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          runSurfaceMapAlignmentButton_actionPerformed();
        }
      });

      pspPanel.add(_deleteSurfaceMapAlignmentButton, 1);
      _deleteSurfaceMapAlignmentButton.setText(StringLocalizer.keyToString("PSP_DELETE_SINGLE_OPTICAL_RECTANGLE_KEY"));
      _deleteSurfaceMapAlignmentButton.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          deleteRegionButton_actionPerformed(false);   
          _runSurfaceMapAlignmentButton.setEnabled(false);
          _deleteSurfaceMapAlignmentButton.setEnabled(false);
          updateUsePspCheckBox();
          if (_isPanelBasedAlignment)
          {
            _testDev.getPanelGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
            _testDev.getPanelGraphicsEngineSetup().removeSelectedOpticalRectangleLayer();
            _testDev.getPanelGraphicsEngineSetup().setDragOpticalRegionLayersVisible(false);
          }
          else
          {
            _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
            _testDev.getBoardGraphicsEngineSetup().removeSelectedOpticalRectangleLayer();
            _testDev.getBoardGraphicsEngineSetup().setDragOpticalRegionLayersVisible(false);
          }
        }
      });

      _runSurfaceMapAlignmentButton.setEnabled(false);
      _deleteSurfaceMapAlignmentButton.setEnabled(false);
      _alignmentButtonsPanel.add(pspPanel, null);
    }
    * */
  }

  /**
   * Populates the alignment point list set with the appropriate points.
   * This is dependent upon what alignment target (panel or board type) the user selected.
   * @author Carli Connally
   * @author Kee chin Seong
   */
  private void populateAlignmentPointGroups()
  {
    _alignmentGroup1RadioButton.removeActionListener(_alignmentGroup1RadioButtonListener);
    _alignmentGroup1TopRadioButton.removeActionListener(_alignmentGroup1TopRadioButtonListener);
    _alignmentGroup1RadioButton.setSelected(true);
    _alignmentGroupToRadioButtonMap.clear();

    PanelSettings panelSettings = _project.getPanel().getPanelSettings();
    LocalizedString groupName;

    if (_isLongPanel )
    {
      java.util.List<AlignmentGroup> topAlignmentGroups;
      java.util.List<AlignmentGroup> bottomAlignmentGroups;
      
      if(panelSettings.isPanelBasedAlignment() == false)
      {
        topAlignmentGroups = _project.getPanel().getBoards().get(0).getBoardSettings().getRightAlignmentGroupsForLongPanel();
        bottomAlignmentGroups = _project.getPanel().getBoards().get(0).getBoardSettings().getLeftAlignmentGroupsForLongPanel();
      }
      else
      {
        topAlignmentGroups = panelSettings.getRightAlignmentGroupsForLongPanel();
        bottomAlignmentGroups = panelSettings.getLeftAlignmentGroupsForLongPanel();
      }

      // these lists may or may not be populated.
      Assert.expect(topAlignmentGroups.size() == 3);
      Assert.expect(bottomAlignmentGroups.size() == 3);

      _alignmentGroup1TopListModel.clear();
      _alignmentGroup1Top = topAlignmentGroups.get(0);
      groupName = new LocalizedString("MMGUI_ALIGN_GROUP_NAME_KEY", new Object[]
                                      {_alignmentGroup1Top.getName()});
      _alignmentGroup1TopRadioButton.setText(StringLocalizer.keyToString(groupName));
//      _alignmentGroup1TopRadioButton.getUI().uninstallUI(_alignmentGroup1TopRadioButton);
//      _alignmentGroup1TopRadioButton.setUI((ButtonUI)MetalRadioButtonUI.createUI(_alignmentGroup1TopRadioButton));
      _alignmentGroupToRadioButtonMap.put(_alignmentGroup1Top, _alignmentGroup1TopRadioButton);
      updateAlignmentGroup(_alignmentGroup1Top);

      _alignmentGroup2TopListModel.clear();
      _alignmentGroup2Top = topAlignmentGroups.get(1);
      groupName = new LocalizedString("MMGUI_ALIGN_GROUP_NAME_KEY", new Object[]
                                      {_alignmentGroup2Top.getName()});
      _alignmentGroup2TopRadioButton.setText(StringLocalizer.keyToString(groupName));
//      _alignmentGroup2TopRadioButton.getUI().uninstallUI(_alignmentGroup2TopRadioButton);
//      _alignmentGroup2TopRadioButton.setUI((ButtonUI)MetalRadioButtonUI.createUI(_alignmentGroup2TopRadioButton));
      _alignmentGroupToRadioButtonMap.put(_alignmentGroup2Top, _alignmentGroup2TopRadioButton);

      _alignmentGroup3TopListModel.clear();
      _alignmentGroup3Top = topAlignmentGroups.get(2);
      groupName = new LocalizedString("MMGUI_ALIGN_GROUP_NAME_KEY", new Object[]
                                      {_alignmentGroup3Top.getName()});
      _alignmentGroup3TopRadioButton.setText(StringLocalizer.keyToString(groupName));
//      _alignmentGroup3TopRadioButton.getUI().uninstallUI(_alignmentGroup3TopRadioButton);
//      _alignmentGroup3TopRadioButton.setUI((ButtonUI)MetalRadioButtonUI.createUI(_alignmentGroup3TopRadioButton));
      _alignmentGroupToRadioButtonMap.put(_alignmentGroup3Top, _alignmentGroup3TopRadioButton);

      _alignmentGroup1BottomListModel.clear();
      _alignmentGroup1Bottom = bottomAlignmentGroups.get(0);
      groupName = new LocalizedString("MMGUI_ALIGN_GROUP_NAME_KEY", new Object[]
                                      {_alignmentGroup1Bottom.getName()});
      _alignmentGroup1BottomRadioButton.setText(StringLocalizer.keyToString(groupName));
//      _alignmentGroup1BottomRadioButton.getUI().uninstallUI(_alignmentGroup1BottomRadioButton);
//      _alignmentGroup1BottomRadioButton.setUI((ButtonUI)MetalRadioButtonUI.createUI(_alignmentGroup1BottomRadioButton));
      _alignmentGroupToRadioButtonMap.put(_alignmentGroup1Bottom, _alignmentGroup1BottomRadioButton);

      _alignmentGroup2BottomListModel.clear();
      _alignmentGroup2Bottom = bottomAlignmentGroups.get(1);
      groupName = new LocalizedString("MMGUI_ALIGN_GROUP_NAME_KEY", new Object[]
                                      {_alignmentGroup2Bottom.getName()});
      _alignmentGroup2BottomRadioButton.setText(StringLocalizer.keyToString(groupName));
//      _alignmentGroup2BottomRadioButton.getUI().uninstallUI(_alignmentGroup2BottomRadioButton);
//      _alignmentGroup2BottomRadioButton.setUI((ButtonUI)MetalRadioButtonUI.createUI(_alignmentGroup2BottomRadioButton));
      _alignmentGroupToRadioButtonMap.put(_alignmentGroup2Bottom, _alignmentGroup2BottomRadioButton);

      _alignmentGroup3BottomListModel.clear();
      _alignmentGroup3Bottom = bottomAlignmentGroups.get(2);
      groupName = new LocalizedString("MMGUI_ALIGN_GROUP_NAME_KEY", new Object[]
                                      {_alignmentGroup3Bottom.getName()});
      _alignmentGroup3BottomRadioButton.setText(StringLocalizer.keyToString(groupName));
//      _alignmentGroup3BottomRadioButton.getUI().uninstallUI(_alignmentGroup3BottomRadioButton);
//      _alignmentGroup3BottomRadioButton.setUI((ButtonUI)MetalRadioButtonUI.createUI(_alignmentGroup3BottomRadioButton));
      _alignmentGroupToRadioButtonMap.put(_alignmentGroup3Bottom, _alignmentGroup3BottomRadioButton);
      
      updateAlignmentGroup(_alignmentGroup1Top);
      updateAlignmentGroup(_alignmentGroup2Top);
      updateAlignmentGroup(_alignmentGroup3Top);
      updateAlignmentGroup(_alignmentGroup1Bottom);
      updateAlignmentGroup(_alignmentGroup2Bottom);
      updateAlignmentGroup(_alignmentGroup3Bottom);
      
      _longPanelAlignmentGroups.clear();
      _longPanelAlignmentGroups.add(_alignmentGroup1Top);
      _longPanelAlignmentGroups.add(_alignmentGroup2Top);
      _longPanelAlignmentGroups.add(_alignmentGroup3Top);
      _longPanelAlignmentGroups.add(_alignmentGroup1Bottom);
      _longPanelAlignmentGroups.add(_alignmentGroup2Bottom);
      _longPanelAlignmentGroups.add(_alignmentGroup3Bottom);
    }
    else
    {
      // wei chin added for individual board alignment
      java.util.List<AlignmentGroup> alignmentGroups;
      
      if(panelSettings.isPanelBasedAlignment() == false)
        alignmentGroups = _project.getPanel().getBoards().get(0).getBoardSettings().getAlignmentGroupsForShortPanel();
      else
        alignmentGroups = panelSettings.getAlignmentGroupsForShortPanel();
      
      // these lists may or may not be populated.
      Assert.expect(alignmentGroups.size() == 3);

      _alignmentGroup1ListModel.clear();
      _alignmentGroup1 = alignmentGroups.get(0);
      groupName = new LocalizedString("MMGUI_ALIGN_GROUP_NAME_KEY", new Object[]
                                      {_alignmentGroup1.getName()});
      _alignmentGroup1RadioButton.setText(StringLocalizer.keyToString(groupName));
//      _alignmentGroup1RadioButton.getUI().uninstallUI(_alignmentGroup1RadioButton);
//      _alignmentGroup1RadioButton.setUI((ButtonUI)MetalRadioButtonUI.createUI(_alignmentGroup1RadioButton));
      _alignmentGroupToRadioButtonMap.put(_alignmentGroup1, _alignmentGroup1RadioButton);

      _alignmentGroup2ListModel.clear();
      _alignmentGroup2 = alignmentGroups.get(1);
      groupName = new LocalizedString("MMGUI_ALIGN_GROUP_NAME_KEY", new Object[]
                                      {_alignmentGroup2.getName()});
      _alignmentGroup2RadioButton.setText(StringLocalizer.keyToString(groupName));
//      _alignmentGroup2RadioButton.getUI().uninstallUI(_alignmentGroup2RadioButton);
//      _alignmentGroup2RadioButton.setUI((ButtonUI)MetalRadioButtonUI.createUI(_alignmentGroup2RadioButton));
      _alignmentGroupToRadioButtonMap.put(_alignmentGroup2, _alignmentGroup2RadioButton);

      _alignmentGroup3ListModel.clear();
      _alignmentGroup3 = alignmentGroups.get(2);
      groupName = new LocalizedString("MMGUI_ALIGN_GROUP_NAME_KEY", new Object[]
                                      {_alignmentGroup3.getName()});

      _alignmentGroup3RadioButton.setText(StringLocalizer.keyToString(groupName));
      _alignmentGroupToRadioButtonMap.put(_alignmentGroup3, _alignmentGroup3RadioButton);
//      _alignmentGroup3RadioButton.getUI().uninstallUI(_alignmentGroup3RadioButton);
//      _alignmentGroup3RadioButton.setUI((ButtonUI)MetalRadioButtonUI.createUI(_alignmentGroup3RadioButton));

      updateAlignmentGroup(_alignmentGroup1);
      updateAlignmentGroup(_alignmentGroup2);
      updateAlignmentGroup(_alignmentGroup3);

      _shortPanelAlignmentGroups.clear();
      _shortPanelAlignmentGroups.add(_alignmentGroup1);
      _shortPanelAlignmentGroups.add(_alignmentGroup2);
      _shortPanelAlignmentGroups.add(_alignmentGroup3);
    }
    // at this point, group 1 should be selected.  So lets make that happen
    if (_isLongPanel)
    {
      _alignmentGroup1TopRadioButton.setSelected(true);

      _alignmentGroup1TopList.setEnabled(true);
      _alignmentGroup2TopList.setEnabled(false);
      _alignmentGroup3TopList.setEnabled(false);
      _alignmentGroup1BottomList.setEnabled(false);
      _alignmentGroup2BottomList.setEnabled(false);
      _alignmentGroup3BottomList.setEnabled(false);

      _deleteAlignmentPointButton.setEnabled(false);
      _topRegionActive = true;
      _currentAlignmentGroup = _alignmentGroup1Top;
      _currentAlignmentGroupListModel = _alignmentGroup1TopListModel;
      _currentAlignmentGroupList = _alignmentGroup1TopList;
    }
    else
    {
      _alignmentGroup1RadioButton.setSelected(true);
      _alignmentGroup1List.setEnabled(true);
      _alignmentGroup2List.setEnabled(false);
      _alignmentGroup3List.setEnabled(false);
      _topRegionActive = true;
      _deleteAlignmentPointButton.setEnabled(false);
      _currentAlignmentGroup = _alignmentGroup1;
      _currentAlignmentGroupListModel = _alignmentGroup1ListModel;
      _currentAlignmentGroupList = _alignmentGroup1List;
    }
    _alignmentGroup1RadioButton.addActionListener(_alignmentGroup1RadioButtonListener);
    _alignmentGroup1TopRadioButton.addActionListener(_alignmentGroup1TopRadioButtonListener);
  }

  /**
   * @author Andy Mechtenberg
   */
  private int findIndexOfPad(DefaultListModel alignmentGroupListModel, Pad pad)
  {
    Assert.expect(alignmentGroupListModel != null);
    Assert.expect(pad != null);

    Enumeration<ManualAlignmentGroupEntry> elements = (Enumeration<ManualAlignmentGroupEntry>)alignmentGroupListModel.elements();
    while(elements.hasMoreElements())
    {
      ManualAlignmentGroupEntry element = elements.nextElement();
      if (element.isPad() && (element.getPad() == pad))
        return alignmentGroupListModel.indexOf(element);
    }
//    Assert.expect(false, "Couldn't find pad" + pad);
    return -1;
  }

  /**
   * @author Andy Mechtenberg
   */
  private int findIndexOfFiducial(DefaultListModel alignmentGroupListModel, Fiducial fiducial)
  {
    Assert.expect(alignmentGroupListModel != null);
    Assert.expect(fiducial != null);

    Enumeration<ManualAlignmentGroupEntry> elements = (Enumeration<ManualAlignmentGroupEntry>)alignmentGroupListModel.elements();
    while(elements.hasMoreElements())
    {
      ManualAlignmentGroupEntry element = elements.nextElement();
      if ((element.isPad() == false) && (element.getFiducial() == fiducial))
        return alignmentGroupListModel.indexOf(element);
    }
//    Assert.expect(false, "Couldn't find fiducial" + fiducial);
    return -1;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateAlignmentGroup(AlignmentGroup alignmentGroup)
  {
    Assert.expect(alignmentGroup != null);
    JRadioButton radioButton = _alignmentGroupToRadioButtonMap.get(alignmentGroup);
    JList alignmentList = _radioButtonToListMap.get(radioButton);

    if(radioButton != null)
    {
      Assert.expect(radioButton != null);
      Assert.expect(alignmentList != null);
      
      // now, just update the current one, and select it too.
      if (radioButton.isSelected() == false)
        radioButton.doClick();
      DefaultListModel listModel = (DefaultListModel)alignmentList.getModel();
      listModel.clear();

      // create the entries, then sort them before adding to group
      java.util.List<ManualAlignmentGroupEntry> elementObjectList = new ArrayList<ManualAlignmentGroupEntry>();

      java.util.List<Pad> pads = alignmentGroup.getPads();
      for (Pad pad : pads)
      {
        elementObjectList.add(new ManualAlignmentGroupEntry(pad));
      }
      java.util.List<Fiducial> fiducials = alignmentGroup.getFiducials();
      for (Fiducial fiducial : fiducials)
      {
        elementObjectList.add(new ManualAlignmentGroupEntry(fiducial));
      }
      // sort now, so all like pads/fiducials are grouped together
      Collections.sort(elementObjectList, new Comparator<ManualAlignmentGroupEntry>()
      {
        public int compare(ManualAlignmentGroupEntry lhs, ManualAlignmentGroupEntry rhs)
        {
          String lhsString = lhs.getSortOrderString();
          String rhsString = rhs.getSortOrderString();
          return lhsString.compareTo(rhsString);
        }
      });

      for(ManualAlignmentGroupEntry entry : elementObjectList)
      {
        listModel.addElement(entry);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private java.util.List<Fiducial> addFiducialsToCurrentAlignmentGroup(java.util.List<Fiducial> fiducials)
  {
    Assert.expect(fiducials != null);

    Assert.expect(_currentAlignmentGroupListModel != null);
    Assert.expect(_currentAlignmentGroup != null);
    return addFiducials(fiducials, _currentAlignmentGroupListModel, _currentAlignmentGroup);

//    updateHighlightsAndPanelGui();
  }

  /**
   * @author Andy Mechtenberg
   */
  private java.util.List<Pad> addPadsToCurrentAlignmentGroup(java.util.List<Pad> pads, java.util.List<Fiducial> fiducialsAlreadyAbleToAdd)
  {
    Assert.expect(pads != null);
    Assert.expect(fiducialsAlreadyAbleToAdd != null);

    Assert.expect(_currentAlignmentGroupListModel != null);
    Assert.expect(_currentAlignmentGroup != null);
    return addPads(pads, fiducialsAlreadyAbleToAdd, _currentAlignmentGroupListModel, _currentAlignmentGroup);
  }

  /**
   * Called whenever a alignment group has something added or delete, it update the
   * highlighted alignment pads in the graphics, and other UI controls as needed.
   * @author Andy Mechtenberg
   */
  private void updateHighlightsAndPanelGui()
  {
    highlightAllAlignmentPoints();
    setRunAlignmentButtonState();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void highlightAllAlignmentPoints()
  {
    _allHighlights.clear();
    if (_isLongPanel)
    {
      if (_topRegionActive)
      {
        HighlightAlignmentItems group1Top = new HighlightAlignmentItems(_alignmentGroup1Top.getPads(),
            _alignmentGroup1Top.getFiducials(), _group1Color);
        HighlightAlignmentItems group2Top = new HighlightAlignmentItems(_alignmentGroup2Top.getPads(),
            _alignmentGroup2Top.getFiducials(), _group2Color);
        HighlightAlignmentItems group3Top = new HighlightAlignmentItems(_alignmentGroup3Top.getPads(),
            _alignmentGroup3Top.getFiducials(), _group3Color);
        _allHighlights.add(group1Top);
        _allHighlights.add(group2Top);
        _allHighlights.add(group3Top);
      }
      else
      {
        HighlightAlignmentItems group1Bottom = new HighlightAlignmentItems(_alignmentGroup1Bottom.getPads(),
            _alignmentGroup1Bottom.getFiducials(), _group1Color);
        HighlightAlignmentItems group2Bottom = new HighlightAlignmentItems(_alignmentGroup2Bottom.getPads(),
            _alignmentGroup2Bottom.getFiducials(), _group2Color);
        HighlightAlignmentItems group3Bottom = new HighlightAlignmentItems(_alignmentGroup3Bottom.getPads(),
            _alignmentGroup3Bottom.getFiducials(), _group3Color);
        _allHighlights.add(group1Bottom);
        _allHighlights.add(group2Bottom);
        _allHighlights.add(group3Bottom);
      }
    }
    else
    {
      HighlightAlignmentItems group1 = new HighlightAlignmentItems(_alignmentGroup1.getPads(),
          _alignmentGroup1.getFiducials(), _group1Color);
      HighlightAlignmentItems group2 = new HighlightAlignmentItems(_alignmentGroup2.getPads(),
          _alignmentGroup2.getFiducials(), _group2Color);
      HighlightAlignmentItems group3 = new HighlightAlignmentItems(_alignmentGroup3.getPads(),
          _alignmentGroup3.getFiducials(), _group3Color);

      _allHighlights.add(group1);
      _allHighlights.add(group2);
      _allHighlights.add(group3);
    }
    _guiObservable.stateChanged(_allHighlights, HighlightEventEnum.HIGHLIGHT_PAD_INSTANCES);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setRunAlignmentButtonState()
  {
    // this will give general status on wether the matrix is valid or not and what to do assuming the button is enabled.
    updateAlignmentStatusMessages();
    // if we're not in sim mode and we are offline, you cannot run manual alignment
    if (_xRayTester.isHardwareAvailable() == false)
    {
      _runAlignmentButton.setEnabled(false);
      _runAlignmentWithDifferentSliceButton.setEnabled(false);
      _runAlignmentButton.setToolTipText(StringLocalizer.keyToString("MMGUI_RUN_ALIGNMENT_DISABLED_OFFLINE_TOOLTIP_KEY"));
      _runAlignmentWithDifferentSliceButton.setToolTipText(StringLocalizer.keyToString("MMGUI_RUN_ALIGNMENT_DISABLED_OFFLINE_TOOLTIP_KEY"));
      _manualAlignmentMatrixInstructionLabel.setText(StringLocalizer.keyToString("MMGUI_RUN_ALIGNMENT_DISABLED_OFFLINE_TOOLTIP_KEY"));
      // Wei Chin
      _alignmentGuideButton.setEnabled(false);
      return;
    }

    // if there is not a panel in the machine, then no manual alignmnet is possible.
//    if (PanelHandler.getInstance().isPanelLoaded() == false)
//    {
//      _runAlignmentButton.setEnabled(false);
//      _runAlignmentButton.setToolTipText(StringLocalizer.keyToString("MMGUI_RUN_ALIGNMENT_DISABLED_NO_PANEL_TOOLTIP_KEY"));
//      _manualAlignmentMatrixInstructionLabel.setText(StringLocalizer.keyToString("MMGUI_RUN_ALIGNMENT_DISABLED_NO_PANEL_TOOLTIP_KEY"));
//      return;
//    }

    // disable the run alignment button if any one group has no pads or fiducials
    boolean ableToRunAlignment = true;
    boolean alignmentGroupMissing = false;
    boolean alignmentGroupInvalid = false;

    java.util.List<AlignmentGroup> alignmentGroups;
    if (_isLongPanel)
      alignmentGroups = _longPanelAlignmentGroups;
    else
      alignmentGroups = _shortPanelAlignmentGroups;
    for (AlignmentGroup alignmentGroup : alignmentGroups)
    {
      if (alignmentGroup.isEmpty())
      {
        alignmentGroupMissing = true;
        ableToRunAlignment = false;
        break;
      }
    }
    
    //XCR-2864 - Ee Jun Jiang - View Alignment with Missing Alignment Point Assertion Error
    _viewAlignmentButton.setEnabled(!alignmentGroupMissing);
    if(alignmentGroupMissing)
      _viewAlignmentButton.setToolTipText(StringLocalizer.keyToString("MMGUI_VIEW_ALIGNMENT_DISABLED_INVALID_KEY"));
    else
      _viewAlignmentButton.setToolTipText(null);

    // at this point, all the groups are populated with something if ableToRunAlignmnet is true.  We
    // now need to check to see that all groups are still VALID
    updateAlignmentSetColors(true);
    if (_project.getTestProgram().hasInvalidatedAlignmentGroups())
    {
      alignmentGroupInvalid = true;
      for (AlignmentGroup alignmentGroup : alignmentGroups)
      {
        if (_project.getTestProgram().isAlignmentGroupInvalidated(alignmentGroup))
        {
          // set it's header to red
          JRadioButton radioButton = _alignmentGroupToRadioButtonMap.get(alignmentGroup);
          radioButton.setBackground(LayerColorEnum.ALIGNMENT_GROUP_INVALID_COLOR.getColor());
        }
      }
//      JOptionPane.showMessageDialog(_mainUI,
//                                    StringLocalizer.keyToString("One or more alignment groups are invalid.  These are shown in red."),
//                                    StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"), JOptionPane.WARNING_MESSAGE);
      ableToRunAlignment = false;
    }
    else
    {
      for (AlignmentGroup alignmentGroup : alignmentGroups)
      {
        if (alignmentGroup.isAlignmentGroupJointTypeInconsistent())
        {
          alignmentGroupInvalid = true;
          // set it's header to red
          JRadioButton radioButton = _alignmentGroupToRadioButtonMap.get(alignmentGroup);
          radioButton.setBackground(LayerColorEnum.ALIGNMENT_GROUP_INVALID_COLOR.getColor());
          ableToRunAlignment = false;
        }
      }
    }

    if (ableToRunAlignment)
    {
      _runAlignmentButton.setEnabled(true);
      // Wei Chin
      _runAlignmentWithDifferentSliceButton.setEnabled(true);
      _alignmentSettingButton.setEnabled(true);
      _alignmentGuideButton.setEnabled(true);
      _viewAlignmentButton.setEnabled(true);
      _runAlignmentButton.setToolTipText(null);
      _runAlignmentWithDifferentSliceButton.setToolTipText(null);
    }
    else
    {
      _runAlignmentButton.setEnabled(false);
      _alignmentSettingButton.setEnabled(false);
      _runAlignmentWithDifferentSliceButton.setEnabled(false);
      _alignmentGuideButton.setEnabled(false);
      _viewAlignmentButton.setEnabled(false);
      if (alignmentGroupMissing)
      {
        _runAlignmentButton.setToolTipText(StringLocalizer.keyToString("MMGUI_RUN_ALIGNMENT_DISABLED_MISSING_KEY"));
        _runAlignmentWithDifferentSliceButton.setToolTipText(StringLocalizer.keyToString("MMGUI_RUN_ALIGNMENT_DISABLED_MISSING_KEY"));
        _manualAlignmentMatrixInstructionLabel.setText(StringLocalizer.keyToString("MMGUI_RUN_ALIGNMENT_DISABLED_MISSING_KEY"));
      }
      else if (alignmentGroupInvalid)
      {
        _runAlignmentButton.setToolTipText(StringLocalizer.keyToString("MMGUI_RUN_ALIGNMENT_DISABLED_INVALID_KEY"));
        _runAlignmentWithDifferentSliceButton.setToolTipText(StringLocalizer.keyToString("MMGUI_RUN_ALIGNMENT_DISABLED_INVALID_KEY"));
        _manualAlignmentMatrixInstructionLabel.setText(StringLocalizer.keyToString("MMGUI_RUN_ALIGNMENT_DISABLED_INVALID_KEY"));
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private java.util.List<Fiducial> addFiducials(java.util.List<Fiducial> fiducials, DefaultListModel listModel, AlignmentGroup alignmentGroup)
  {
    Assert.expect(fiducials != null);
    Assert.expect(listModel != null);
    Assert.expect(alignmentGroup != null);

    java.util.List<Fiducial> fiducialsOkToAdd = new ArrayList<Fiducial>();
    Iterator<Fiducial> fiducialIt = fiducials.iterator();

    while (fiducialIt.hasNext())
    {
      Fiducial fiducial = fiducialIt.next();

      // check to see if it's used by anyone else
      // check group 1 first
      LocalizedString errorMessage = null;
      if (_isLongPanel)
      {
        if (_topRegionActive)
        {
          if (_alignmentGroup1Top.getFiducials().contains(fiducial))
          {
            _alignmentGroup1TopRadioButton.doClick();
            int fiducialIndex = findIndexOfFiducial(_alignmentGroup1TopListModel, fiducial);
            if ((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup1TopListModel.size()))
            {
//              Assert.expect((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup1TopListModel.size()));
              _alignmentGroup1TopList.addSelectionInterval(fiducialIndex, fiducialIndex);
              _alignmentGroup1TopList.requestFocus();
            }
            continue;
          }
          else if (_alignmentGroup2Top.getFiducials().contains(fiducial))
          {
            _alignmentGroup2TopRadioButton.doClick();
            int fiducialIndex = findIndexOfFiducial(_alignmentGroup2TopListModel, fiducial);
            if ((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup2TopListModel.size()))
            {
//              Assert.expect((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup2TopListModel.size()));
              _alignmentGroup2TopList.addSelectionInterval(fiducialIndex, fiducialIndex);
              _alignmentGroup2TopList.requestFocus();
            }
            continue;
          }
          else if (_alignmentGroup3Top.getFiducials().contains(fiducial))
          {
            _alignmentGroup3TopRadioButton.doClick();
            int fiducialIndex = findIndexOfFiducial(_alignmentGroup3TopListModel, fiducial);
            if ((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup3TopListModel.size()))
            {
//              Assert.expect((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup3TopListModel.size()));
              _alignmentGroup3TopList.addSelectionInterval(fiducialIndex, fiducialIndex);
              _alignmentGroup3TopList.requestFocus();
            }
            continue;
          }
        }
        else
        {
          if (_alignmentGroup1Bottom.getFiducials().contains(fiducial))
          {
            _alignmentGroup1BottomRadioButton.doClick();
            int fiducialIndex = findIndexOfFiducial(_alignmentGroup1BottomListModel, fiducial);
            if ((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup1BottomListModel.size()))
            {
//              Assert.expect((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup1BottomListModel.size()));
              _alignmentGroup1BottomList.addSelectionInterval(fiducialIndex, fiducialIndex);
              _alignmentGroup1BottomList.requestFocus();
            }
            continue;
          }
          else if (_alignmentGroup2Bottom.getFiducials().contains(fiducial))
          {
            _alignmentGroup2BottomRadioButton.doClick();
            int fiducialIndex = findIndexOfFiducial(_alignmentGroup2BottomListModel, fiducial);
            if ((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup2BottomListModel.size()))
            {
//              Assert.expect((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup2BottomListModel.size()));
              _alignmentGroup2BottomList.addSelectionInterval(fiducialIndex, fiducialIndex);
              _alignmentGroup2BottomList.requestFocus();
            }
            continue;
          }
          else if (_alignmentGroup3Bottom.getFiducials().contains(fiducial))
          {
            _alignmentGroup3BottomRadioButton.doClick();
            int fiducialIndex = findIndexOfFiducial(_alignmentGroup3BottomListModel, fiducial);
            if ((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup3BottomListModel.size()))
            {
//              Assert.expect((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup3BottomListModel.size()));
              _alignmentGroup3BottomList.addSelectionInterval(fiducialIndex, fiducialIndex);
              _alignmentGroup3BottomList.requestFocus();
            }
            continue;
          }
        }
      }
      else
      {
        if (_alignmentGroup1.getFiducials().contains(fiducial))
        {
          _alignmentGroup1RadioButton.doClick();
          int fiducialIndex = findIndexOfFiducial(_alignmentGroup1ListModel, fiducial);
          if ((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup1ListModel.size()))
          {
            Assert.expect((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup1ListModel.size()));
            _alignmentGroup1List.addSelectionInterval(fiducialIndex, fiducialIndex);
            _alignmentGroup1List.requestFocus();
          }
          continue;
        }
        else if (_alignmentGroup2.getFiducials().contains(fiducial))
        {
          _alignmentGroup2RadioButton.doClick();
          int fiducialIndex = findIndexOfFiducial(_alignmentGroup2ListModel, fiducial);
          if ((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup2ListModel.size()))
          {
            Assert.expect((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup2ListModel.size()));
            _alignmentGroup2List.addSelectionInterval(fiducialIndex, fiducialIndex);
            _alignmentGroup2List.requestFocus();
          }
          continue;
        }
        else if (_alignmentGroup3.getFiducials().contains(fiducial))
        {
          _alignmentGroup3RadioButton.doClick();
          int fiducialIndex = findIndexOfFiducial(_alignmentGroup3ListModel, fiducial);
          if ((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup3ListModel.size()))
          {
            Assert.expect((fiducialIndex >= 0) && (fiducialIndex < _alignmentGroup3ListModel.size()));
            _alignmentGroup3List.addSelectionInterval(fiducialIndex, fiducialIndex);
            _alignmentGroup3List.requestFocus();
          }
          continue;
        }
      }
      // there are a bunch of reasons we can't add the fiducial
      //  - all pads and ficucials must be on the same side of the panel
      //  - There might not be room -- there is a max allowed fiducials per alignment group
      //  -  It may already be in the list
      //  -  It may be in some other list
      //  -  It might be too far away or cross a reconstruction line (business layer check)

      // if fiducialsOkToAdd has stuff, make sure it's on the same side as the new pads we're considering
      if (fiducialsOkToAdd.isEmpty() == false)
      {
        Fiducial sampleFiducial = fiducialsOkToAdd.get(0);
        if (sampleFiducial.isTopSide() != fiducial.isTopSide())
        {
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString("MMGUI_ALIGN_ALL_ON_SAME_SIDE_KEY"),
                                        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                        JOptionPane.ERROR_MESSAGE);

          break;
        }
      }


      if (alignmentGroup.isEmpty() == false)
      {
        if (alignmentGroup.isTopSide() != fiducial.isTopSide())
        {
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString("MMGUI_ALIGN_ALL_ON_SAME_SIDE_KEY"),
                                        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                        JOptionPane.ERROR_MESSAGE);
          break;
        }
      }

      int currentNumberOfElements = listModel.getSize();
      if (currentNumberOfElements + fiducialsOkToAdd.size() + 1 > _maxPadsAndFiducials)
      {
        errorMessage = new LocalizedString("MMGUI_ALIGN_TOO_MANY_PADS_KEY", new Object[]
                                           {new Integer(_maxPadsAndFiducials)});
        JOptionPane.showMessageDialog(_mainUI,
                                      StringLocalizer.keyToString(errorMessage),
                                      StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                      JOptionPane.ERROR_MESSAGE);
        break;
      }
      java.util.List<Fiducial> fiducialsToConsider = new ArrayList<Fiducial>(alignmentGroup.getFiducials());
      fiducialsToConsider.add(fiducial);
      fiducialsToConsider.addAll(fiducialsOkToAdd);

      // finally, we'll do the business layer check
      ProgramGeneration programGeneration = ProgramGeneration.getInstance();
      _mainUI.generateTestProgramForAlignmentIfNeededWithReason(StringLocalizer.keyToString("MMGUI_ALIGNMENT_NEEDS_TEST_PROGRAM_KEY"),
          StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
          _project);

      if ((_isLongPanel) && (_topRegionActive == false))
      {
        // need to call left
        AlignmentValidityEnum validityEnum = programGeneration.checkLeftAlignmentRegionValidity(_project, alignmentGroup.getPads(), fiducialsToConsider);

        if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
          validityEnum = AlignmentValidityEnum.NOT_SUPPORT_FIDUCIAL;

        if (validityEnum.equals(AlignmentValidityEnum.IS_VALID) == false)
        {
          if (_currentAlignmentGroup.isEmpty())
          {
            errorMessage = new LocalizedString("MMGUI_ALIGN_FIDUCIAL_DEFINITION_NOT_VALID_ERROR_KEY", new Object[]
                                               {fiducial.getName(), _currentAlignmentGroup.getName()});
          }
          else if (validityEnum.equals(AlignmentValidityEnum.INVALID_DOES_NOT_FIT))
          {
            errorMessage = new LocalizedString("MMGUI_ALIGN_FIDUCIAL_TOO_FAR_APART_ERROR_KEY", new Object[]
                                               {fiducial.getName(), _currentAlignmentGroup.getName()});
          }
          else if (validityEnum.equals(AlignmentValidityEnum.INVALID_FEATURES_NOT_ORTHOGONAL))
          {
            errorMessage = new LocalizedString("MMGUI_ALIGN_FIDUCIAL_NOT_ORTHOGONAL_ERROR_KEY", new Object[]{});
          }
          else if(validityEnum.equals(AlignmentValidityEnum.NOT_SUPPORT_FIDUCIAL))
          {
            errorMessage = new LocalizedString("MMGUI_ALIGN_FIDUCIAL_NOT_SUPPORT_ERROR_KEY", new Object[]{});
          }

          JOptionPane.showMessageDialog(_mainUI,
                                        StringUtil.format(StringLocalizer.keyToString(errorMessage), 60),
                                        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                        JOptionPane.ERROR_MESSAGE);
          break;
        }
      }
      else
      {
        AlignmentValidityEnum validityEnum = programGeneration.checkRightAlignmentRegionValidity(_project, alignmentGroup.getPads(), fiducialsToConsider);
        
        if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
          validityEnum = AlignmentValidityEnum.NOT_SUPPORT_FIDUCIAL;

        if (validityEnum.equals(AlignmentValidityEnum.IS_VALID) == false)
        {
          if (_currentAlignmentGroup.isEmpty())
          {
            errorMessage = new LocalizedString("MMGUI_ALIGN_FIDUCIAL_DEFINITION_NOT_VALID_ERROR_KEY", new Object[]
                                               {fiducial.getName(), _currentAlignmentGroup.getName()});
          }
          else if (validityEnum.equals(AlignmentValidityEnum.INVALID_DOES_NOT_FIT))
          {
            errorMessage = new LocalizedString("MMGUI_ALIGN_FIDUCIAL_TOO_FAR_APART_ERROR_KEY", new Object[]
                                               {fiducial.getName(), _currentAlignmentGroup.getName()});
          }
          else if (validityEnum.equals(AlignmentValidityEnum.INVALID_FEATURES_NOT_ORTHOGONAL))
          {
            errorMessage = new LocalizedString("MMGUI_ALIGN_FIDUCIAL_NOT_ORTHOGONAL_ERROR_KEY", new Object[]{});
          }
          else if(validityEnum.equals(AlignmentValidityEnum.NOT_SUPPORT_FIDUCIAL))
          {
            errorMessage = new LocalizedString("MMGUI_ALIGN_FIDUCIAL_NOT_SUPPORT_ERROR_KEY", new Object[]{});
          }

          JOptionPane.showMessageDialog(_mainUI,
                                        StringUtil.format(StringLocalizer.keyToString(errorMessage), 60),
                                        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                        JOptionPane.ERROR_MESSAGE);
          break;
        }
      }
      fiducialsOkToAdd.add(fiducial);
    }
    return fiducialsOkToAdd;
  }

  /**
   * @author Andy Mechtenberg
   */
  private java.util.List<Pad> addPads(java.util.List<Pad> pads,
                                      java.util.List<Fiducial> fiducialsAlreadyAbleToAdd,
                                      DefaultListModel listModel,
                                      AlignmentGroup alignmentGroup)
  {
    Assert.expect(pads != null);
    Assert.expect(fiducialsAlreadyAbleToAdd != null); // used only for checking validity of alignment group
    Assert.expect(listModel != null);
    Assert.expect(alignmentGroup != null);

    // first, go through the list of pads and if any are of cap, pcap or res, make sure only pad ONE
    // is in the list.  We'll add in pad 2 later.
    java.util.List<Pad> proposedPads = new ArrayList<Pad>();
    for (Pad pad : pads)
    {
      JointTypeEnum padJointType = pad.getPadType().getPackagePin().getJointTypeEnum();
      if (padJointType.equals(JointTypeEnum.CAPACITOR) ||
          padJointType.equals(JointTypeEnum.POLARIZED_CAP) ||
          padJointType.equals(JointTypeEnum.RESISTOR)
          // Wei Chin (Tall Cap)
//        ||  padJointType.equals(JointTypeEnum.TALL_CAPACITOR)
          )
      {
        // there can only be two here
        java.util.List<Pad> componentPads = pad.getComponent().getPads();
        pad = componentPads.get(0);
        Pad secondPad = componentPads.get(1);
        // only add one of these
        proposedPads.remove(pad);
        proposedPads.remove(secondPad);
        proposedPads.add(pad);
      }
      else
        proposedPads.add(pad);
    }

    // remove duplicates from this list, and sort
    Set<Pad> set = new HashSet<Pad>(proposedPads);
    proposedPads = new ArrayList<Pad>(set);
    Collections.sort(proposedPads, new AlphaNumericComparator());

    // this is the list of pads that pass the add criteria checks
    java.util.List<Pad> padsOkToAdd = new ArrayList<Pad>();
    for (Pad pad : proposedPads)
    {
      Pad secondPad = null;
      int numPadsToBeAdded = 1;

      // special case for Cap, Res and PCap
      // If one pad is selected, we automatically select the other
      JointTypeEnum padJointType = pad.getPadType().getPackagePin().getJointTypeEnum();
      if (padJointType.equals(JointTypeEnum.CAPACITOR) ||
          padJointType.equals(JointTypeEnum.POLARIZED_CAP) ||
          padJointType.equals(JointTypeEnum.RESISTOR) 
          // Wei Chin (Tall Cap)
//          || padJointType.equals(JointTypeEnum.TALL_CAPACITOR)
          )
      {
        // there can only be two here
        numPadsToBeAdded++;
        pad = pad.getComponent().getPads().get(0);
        secondPad = pad.getComponent().getPads().get(1);
      }

      LocalizedString errorMessage = null;
      if (_isLongPanel)
      {
        if (_topRegionActive)
        {
          if (_alignmentGroup1Top.getPads().contains(pad))
          {
            _alignmentGroup1TopRadioButton.doClick();
            int padIndex = findIndexOfPad(_alignmentGroup1TopListModel, pad);
            if ((padIndex >= 0) && (padIndex < _alignmentGroup1TopListModel.size()))
            {
//              Assert.expect((padIndex >= 0) && (padIndex < _alignmentGroup1TopListModel.size()));
              _alignmentGroup1TopList.addSelectionInterval(padIndex, padIndex);
              _alignmentGroup1TopList.requestFocus();
            }
            continue;
          }
          else if (_alignmentGroup2Top.getPads().contains(pad))
          {
            _alignmentGroup2TopRadioButton.doClick();
            int padIndex = findIndexOfPad(_alignmentGroup2TopListModel, pad);
            if ((padIndex >= 0) && (padIndex < _alignmentGroup2TopListModel.size()))
            {
//            Assert.expect((padIndex >= 0) && (padIndex < _alignmentGroup2TopListModel.size()));
              _alignmentGroup2TopList.addSelectionInterval(padIndex, padIndex);
              _alignmentGroup2TopList.requestFocus();
            }
            continue;
          }
          else if (_alignmentGroup3Top.getPads().contains(pad))
          {
            _alignmentGroup3TopRadioButton.doClick();
            int padIndex = findIndexOfPad(_alignmentGroup3TopListModel, pad);
            if ((padIndex >= 0) && (padIndex < _alignmentGroup3TopListModel.size()))
            {
//              Assert.expect((padIndex >= 0) && (padIndex < _alignmentGroup3TopListModel.size()));
              _alignmentGroup3TopList.addSelectionInterval(padIndex, padIndex);
              _alignmentGroup3TopList.requestFocus();
            }
            continue;
          }
        }
        else // bottom region is active
        {
          if (_alignmentGroup1Bottom.getPads().contains(pad))
          {
            _alignmentGroup1BottomRadioButton.doClick();
            int padIndex = findIndexOfPad(_alignmentGroup1BottomListModel, pad);
            if ((padIndex >= 0) && (padIndex < _alignmentGroup1BottomListModel.size()))
            {
//              Assert.expect((padIndex >= 0) && (padIndex < _alignmentGroup1BottomListModel.size()));
              _alignmentGroup1BottomList.addSelectionInterval(padIndex, padIndex);
              _alignmentGroup1BottomList.requestFocus();
            }
            continue;
          }
          else if (_alignmentGroup2Bottom.getPads().contains(pad))
          {
            _alignmentGroup2BottomRadioButton.doClick();
            int padIndex = findIndexOfPad(_alignmentGroup2BottomListModel, pad);
            if ((padIndex >= 0) && (padIndex < _alignmentGroup2BottomListModel.size()))
            {
//              Assert.expect((padIndex >= 0) && (padIndex < _alignmentGroup2BottomListModel.size()));
              _alignmentGroup2BottomList.addSelectionInterval(padIndex, padIndex);
              _alignmentGroup2BottomList.requestFocus();
            }
            continue;
          }
          else if (_alignmentGroup3Bottom.getPads().contains(pad))
          {
            _alignmentGroup3BottomRadioButton.doClick();
            int padIndex = findIndexOfPad(_alignmentGroup3BottomListModel, pad);
            if ((padIndex >= 0) && (padIndex < _alignmentGroup3BottomListModel.size()))
            {
//              Assert.expect((padIndex >= 0) && (padIndex < _alignmentGroup3BottomListModel.size()));
              _alignmentGroup3BottomList.addSelectionInterval(padIndex, padIndex);
              _alignmentGroup3BottomList.requestFocus();
            }
            continue;
          }
        }
      }
      else // is NOT long panel
      {
        if (_alignmentGroup1.getPads().contains(pad))
        {
          _alignmentGroup1RadioButton.doClick();
          int padIndex = findIndexOfPad(_alignmentGroup1ListModel, pad);
          if ((padIndex >= 0) && (padIndex < _alignmentGroup1ListModel.size()))
          {
//          Assert.expect((padIndex >= 0) && (padIndex < _alignmentGroup1ListModel.size()), "Index is : " + padIndex);
            _alignmentGroup1List.addSelectionInterval(padIndex, padIndex);
            _alignmentGroup1List.requestFocus();
          }
          continue;
        }
        else if (_alignmentGroup2.getPads().contains(pad))
        {
          _alignmentGroup2RadioButton.doClick();
          int padIndex = findIndexOfPad(_alignmentGroup2ListModel, pad);
          if ((padIndex >= 0) && (padIndex < _alignmentGroup2ListModel.size()))
          {
//          Assert.expect((padIndex >= 0) && (padIndex < _alignmentGroup2ListModel.size()));
            _alignmentGroup2List.addSelectionInterval(padIndex, padIndex);
            _alignmentGroup2List.requestFocus();
          }
          continue;
        }
        else if (_alignmentGroup3.getPads().contains(pad))
        {
          _alignmentGroup3RadioButton.doClick();
          int padIndex = findIndexOfPad(_alignmentGroup3ListModel, pad);
          if ((padIndex >= 0) && (padIndex < _alignmentGroup3ListModel.size()))
          {
//          Assert.expect((padIndex >= 0) && (padIndex < _alignmentGroup3ListModel.size()));
            _alignmentGroup3List.addSelectionInterval(padIndex, padIndex);
            _alignmentGroup3List.requestFocus();
          }
          continue;
        }
      }
      // there are a bunch of reasons we can't add the pad
      //  - It might not be on the same side as any existing pads
      //  - There might not be room -- there is a max allowed pads per alignment group
      //  -  It might be too far away or cross a reconstruction line (business layer check)

      // first, check for all pads on the SAME SIDE!!!
      // if padsOkToAdd has stuff, make sure it's on the same side as the new pads we're considering
      if (padsOkToAdd.isEmpty() == false)
      {
        Pad samplePad = padsOkToAdd.get(0);
        if (samplePad.isTopSide() != pad.isTopSide())
        {
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString("MMGUI_ALIGN_ALL_ON_SAME_SIDE_KEY"),
                                        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                        JOptionPane.ERROR_MESSAGE);
          break;
        }
      }

      if ((alignmentGroup.isEmpty() == false) || (fiducialsAlreadyAbleToAdd.isEmpty() == false))
      {
        if (isAlignmentGroupOnTopSide(alignmentGroup, fiducialsAlreadyAbleToAdd) != pad.isTopSide())
        {
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString("MMGUI_ALIGN_ALL_ON_SAME_SIDE_KEY"),
                                        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                        JOptionPane.ERROR_MESSAGE);
          break;
        }
      }

      // Check for too many pads in the group
      int currentNumberOfElements = listModel.getSize();
      // numPadsToBeAdded is 1 or 2 depending on the PCAP, CAP, RES nature of the pad object
      if ((currentNumberOfElements + fiducialsAlreadyAbleToAdd.size() + numPadsToBeAdded + padsOkToAdd.size()) > _maxPadsAndFiducials)
      {
        errorMessage = new LocalizedString("MMGUI_ALIGN_TOO_MANY_PADS_KEY", new Object[]
                                           {new Integer(_maxPadsAndFiducials)});
        JOptionPane.showMessageDialog(_mainUI,
                                      StringLocalizer.keyToString(errorMessage),
                                      StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                      JOptionPane.ERROR_MESSAGE);
        break;
      }

      // finally, we'll do the business layer check
      java.util.List<Pad> newPads = new ArrayList<Pad>();
      newPads.add(pad);
      if (secondPad != null)
        newPads.add(secondPad);
      newPads.addAll(padsOkToAdd);

      // check to see if the points are within the reconstruction size limitation
      java.util.List<Fiducial> fiducialsToConsider = new ArrayList<Fiducial>(alignmentGroup.getFiducials());
      fiducialsToConsider.addAll(fiducialsAlreadyAbleToAdd);

      java.util.List<Pad> padsToConsider = new ArrayList<Pad>(alignmentGroup.getPads());
      padsToConsider.addAll(newPads);
      
//      for(Pad padss : alignmentGroup.getPads())
//      {
//        System.out.println(" " + padss.getComponent().getBoard().getName());
//      }

      ProgramGeneration programGeneration = ProgramGeneration.getInstance();
      _mainUI.generateTestProgramForAlignmentIfNeededWithReason(StringLocalizer.keyToString("MMGUI_ALIGNMENT_NEEDS_TEST_PROGRAM_KEY"),
          StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
          _project);

      if ((_isLongPanel) && (_topRegionActive == false))
      {
        // need to call left
        AlignmentValidityEnum validityEnum = AlignmentValidityEnum.IS_VALID;

        if(_project.getPanel().getPanelSettings().isPanelBasedAlignment())
          validityEnum = programGeneration.checkLeftAlignmentRegionValidity(_project, padsToConsider, fiducialsToConsider);
        else
        {
          Board currentBoard = padsToConsider.get(0).getComponent().getBoard();
          validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgram(currentBoard, PanelLocationInSystemEnum.LEFT), padsToConsider, fiducialsToConsider);
        }

        if (validityEnum.equals(AlignmentValidityEnum.IS_VALID) == false)
        {
          if (_currentAlignmentGroup.isEmpty())
          {
            errorMessage = new LocalizedString("MMGUI_ALIGN_DEFINITION_NOT_VALID_ERROR_KEY", new Object[]
                                               {pad.getName(), pad.getComponent().getReferenceDesignator(),
                                               _currentAlignmentGroup.getName()});
          }
          else if (validityEnum.equals(AlignmentValidityEnum.INVALID_DOES_NOT_FIT))
          {
            errorMessage = new LocalizedString("MMGUI_ALIGN_TOO_FAR_APART_ERROR_KEY", new Object[]
                                               {pad.getName(), pad.getComponent().getReferenceDesignator(),
                                               _currentAlignmentGroup.getName()});
          }
          else if (validityEnum.equals(AlignmentValidityEnum.INVALID_FEATURES_NOT_ORTHOGONAL))
          {
            errorMessage = new LocalizedString("MMGUI_ALIGN_PAD_NOT_ORTHOGONAL_ERROR_KEY", new Object[]{});
          }
          JOptionPane.showMessageDialog(_mainUI,
                                        StringUtil.format(StringLocalizer.keyToString(errorMessage), 60),
                                        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                        JOptionPane.ERROR_MESSAGE);
          break;
        }
      }
      else
      {
        AlignmentValidityEnum validityEnum = null;
        if(_project.getPanel().getPanelSettings().isPanelBasedAlignment())
        {
          if(_project.getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.RIGHT))
          {
            if (_isLongPanel == false)
              Assert.expect(_project.getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.LEFT) == false);
            validityEnum = programGeneration.checkRightAlignmentRegionValidity(_project, padsToConsider, fiducialsToConsider);
          }
          else if(_project.getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.LEFT))
          {
            if (_isLongPanel == false)
              Assert.expect(_project.getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.RIGHT) == false);
            validityEnum = programGeneration.checkLeftAlignmentRegionValidity(_project, padsToConsider, fiducialsToConsider);
          }
          else
          {
            if (_mainUI.isPanelSizeWithinSpec())
              Assert.expect(false);
            break;
          }
        }
        else
        {
          Board currentBoard = null;

          if(padsToConsider.isEmpty() == false)
          {
            currentBoard = padsToConsider.get(0).getComponent().getBoard();
            if(currentBoard.isLongBoard() == false)
              validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgram(currentBoard), padsToConsider, fiducialsToConsider);
            else
              validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgram(currentBoard, PanelLocationInSystemEnum.RIGHT), padsToConsider, fiducialsToConsider);
          }
        }

        if (validityEnum.equals(AlignmentValidityEnum.IS_VALID) == false)
        {
          if (_mainUI.isPanelSizeWithinSpec() == false)
            break;
          if (_currentAlignmentGroup.isEmpty())
          {
            errorMessage = new LocalizedString("MMGUI_ALIGN_DEFINITION_NOT_VALID_ERROR_KEY", new Object[]
                                               {pad.getName(), pad.getComponent().getReferenceDesignator(),
                                               _currentAlignmentGroup.getName()});
          }
          else if (validityEnum.equals(AlignmentValidityEnum.INVALID_DOES_NOT_FIT))
          {
            errorMessage = new LocalizedString("MMGUI_ALIGN_TOO_FAR_APART_ERROR_KEY", new Object[]
                                               {pad.getName(), pad.getComponent().getReferenceDesignator(),
                                               _currentAlignmentGroup.getName()});
          }
          else if (validityEnum.equals(AlignmentValidityEnum.INVALID_FEATURES_NOT_ORTHOGONAL))
          {
            errorMessage = new LocalizedString("MMGUI_ALIGN_PAD_NOT_ORTHOGONAL_ERROR_KEY", new Object[]{});
          }
          else if(validityEnum.equals(AlignmentValidityEnum.NOT_SUPPORT_FIDUCIAL))
          {
            errorMessage = new LocalizedString("MMGUI_ALIGN_FIDUCIAL_NOT_SUPPORT_ERROR_KEY", new Object[]{});
          }

          JOptionPane.showMessageDialog(_mainUI,
                                        StringUtil.format(StringLocalizer.keyToString(errorMessage), 60),
                                        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                        JOptionPane.ERROR_MESSAGE);
          break;
        }
      }
      // ok -- all checks passed.  Now we update the list of pads that are ok to add
      padsOkToAdd.add(pad);
      if (secondPad != null)
        padsOkToAdd.add(secondPad);
    }
    // remove duplicates from this list
    set = new HashSet<Pad>(padsOkToAdd);
    padsOkToAdd = new ArrayList<Pad>(set);

    return padsOkToAdd;
  }

  /**
   * @author Andy Mechtenberg
   */
  private boolean isAlignmentGroupOnTopSide(AlignmentGroup alignmentGroup, java.util.List<Fiducial> fiducialsAlreadyAbleToAdd)
  {
    Assert.expect(alignmentGroup != null);
    Assert.expect(fiducialsAlreadyAbleToAdd != null);

    java.util.List<Pad> currentPads = alignmentGroup.getPads();
    java.util.List<Fiducial> currentFiducials = new ArrayList<Fiducial>(alignmentGroup.getFiducials());
    currentFiducials.addAll(fiducialsAlreadyAbleToAdd);

    if ((currentPads.size() == 0) && (currentFiducials.size() == 0))
      Assert.expect(false, "This must only be called with a populated alignment group");

    for (Pad pad : currentPads)
    {
      return pad.isTopSide();
    }
    for (Fiducial fiducial : currentFiducials)
    {
      return fiducial.isTopSide();
    }
    Assert.expect(false, "no pads or fiducials");
    return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void removeSelectedItems(JList alignmentGroupList, AlignmentGroup alignmentGroup)
  {
    Assert.expect(alignmentGroupList != null);
    Assert.expect(alignmentGroup != null);
    
    //Khaw Chek Hau - XCR2181: Not able to run alignment from failed board
    //Whenever there are pads removed alignment group, reset the learned alignment flag
    _project.getTestProgram().clearRunAlignmentLearned();

    // remove the list selection listener, so it doesn't interrupt the delete process.
    // we'll put it back when where done at the end of this method
    alignmentGroupList.removeListSelectionListener(_listSelectionListener);
    try
    {
      java.util.List<Pad> padsToRemove = new ArrayList<Pad>();
      java.util.List<Fiducial> fiducialsToRemove = new ArrayList<Fiducial>();
      DefaultListModel listModel = (DefaultListModel)alignmentGroupList.getModel();
      int[] selections = alignmentGroupList.getSelectedIndices();
      for (int i = selections.length; i > 0; i--)
      {
        // the format of the string for a pad is <comp> - <pad>
        // for a fiducial it's simply <fiducial> (there is no dash)
        ManualAlignmentGroupEntry entry = (ManualAlignmentGroupEntry)listModel.getElementAt(selections[i - 1]);
        if (entry.isPad())
        {
          Pad alignmentPad = entry.getPad();
          // now remove it from the project too
          for (Pad pad : alignmentGroup.getPads())
          {
            if (pad == alignmentPad)
            {
              padsToRemove.add(pad);
              break;
            }
          }
        }
        else // removing a fiducial
        {
          Fiducial alignmentFiducial = entry.getFiducial();
//          listModel.removeElementAt(selections[i - 1]);
          for (Fiducial fiducial : alignmentGroup.getFiducials())
          {
            if (fiducial == alignmentFiducial)
            {
              fiducialsToRemove.add(fiducial);
              break;
            }
          }
        }
      }
      // time to remove all selections
      if(_project.getPanel().getPanelSettings().isPanelBasedAlignment())
        _commandManager.execute(new RemoveFeaturesFromAlignmentGroupCommand(alignmentGroup, padsToRemove, fiducialsToRemove));
      else
      {
        int alignmentGroupIndex = getCurrentAlignmentGroupIndex(alignmentGroup);
        _commandManager.execute(new RemoveFeaturesFromBoardSettingsAlignmentGroupCommand(alignmentGroupIndex, _project.getPanel(), padsToRemove, fiducialsToRemove));
      }
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
    }
    finally
    {
      alignmentGroupList.addListSelectionListener(_listSelectionListener);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setSetupScreenEnabled(boolean enabled)
  {
    _alignmentGroup1RadioButton.setEnabled(enabled);
    _alignmentGroup2RadioButton.setEnabled(enabled);
    _alignmentGroup3RadioButton.setEnabled(enabled);

    // do the long panel radio buttons
    _alignmentGroup1TopRadioButton.setEnabled(enabled);
    _alignmentGroup2TopRadioButton.setEnabled(enabled);
    _alignmentGroup3TopRadioButton.setEnabled(enabled);
    _alignmentGroup1BottomRadioButton.setEnabled(enabled);
    _alignmentGroup2BottomRadioButton.setEnabled(enabled);
    _alignmentGroup3BottomRadioButton.setEnabled(enabled);
    updateAlignmentSetColors(enabled);

    //Khaw Chek Hau - XCR2285: 2D Alignment on v810    
    _alignment2DRadioButton.setEnabled(enabled);
    _alignment3DRadioButton.setEnabled(enabled);

    // now, if we are disabling, we want to disable all jlists
    // but, if re-enabling, then only the one with the selected jradiobutton
    if (enabled)
    {
      if (_alignmentGroup1RadioButton.isSelected())
        _alignmentGroup1List.setEnabled(true);
      if (_alignmentGroup2RadioButton.isSelected())
        _alignmentGroup2List.setEnabled(true);
      if (_alignmentGroup3RadioButton.isSelected())
        _alignmentGroup3List.setEnabled(true);

      if (_alignmentGroup1TopRadioButton.isSelected())
        _alignmentGroup1TopList.setEnabled(true);
      if (_alignmentGroup2TopRadioButton.isSelected())
        _alignmentGroup2TopList.setEnabled(true);
      if (_alignmentGroup3TopRadioButton.isSelected())
        _alignmentGroup3TopList.setEnabled(true);
      if (_alignmentGroup1BottomRadioButton.isSelected())
        _alignmentGroup1BottomList.setEnabled(true);
      if (_alignmentGroup2BottomRadioButton.isSelected())
        _alignmentGroup2BottomList.setEnabled(true);
      if (_alignmentGroup3BottomRadioButton.isSelected())
        _alignmentGroup3BottomList.setEnabled(true);
    }
    else
    {
      _alignmentGroup1List.setEnabled(false);
      _alignmentGroup2List.setEnabled(false);
      _alignmentGroup3List.setEnabled(false);

      _alignmentGroup1TopList.setEnabled(false);
      _alignmentGroup3TopList.setEnabled(false);
      _alignmentGroup1BottomList.setEnabled(false);
      _alignmentGroup2BottomList.setEnabled(false);
      _alignmentGroup3BottomList.setEnabled(false);
      _alignmentGroup2TopList.setEnabled(false);

      _deleteAlignmentPointButton.setEnabled(false);
      _viewAlignmentButton.setEnabled(false);

      Collection<JList> allGroupLists = _radioButtonToListMap.values();
      for(JList list : allGroupLists)
      {
        list.removeSelectionInterval(0, list.getMaxSelectionIndex());
      }
    }

  }

  /**
   * If the alignment point sets are enabled, then the radio buttons display
   * their correct colors.  Otherwise, they display a normal background.  This
   * cuts down on user noise when the user is not working on alignment.
   * @author Carli Connally
   *
   */
  private void updateAlignmentSetColors(boolean enabled)
  {
    if (enabled)
    {
      _alignmentGroup1RadioButton.setBackground(_group1Color);
      _alignmentGroup2RadioButton.setBackground(_group2Color);
      _alignmentGroup3RadioButton.setBackground(_group3Color);
      // do the long panel radio buttons
      _alignmentGroup1TopRadioButton.setBackground(_group1Color);
      _alignmentGroup2TopRadioButton.setBackground(_group2Color);
      _alignmentGroup3TopRadioButton.setBackground(_group3Color);
      _alignmentGroup1BottomRadioButton.setBackground(_group1Color);
      _alignmentGroup2BottomRadioButton.setBackground(_group2Color);
      _alignmentGroup3BottomRadioButton.setBackground(_group3Color);
    }
    else
    {
      _alignmentGroup1RadioButton.setBackground(SystemColor.control);
      _alignmentGroup2RadioButton.setBackground(SystemColor.control);
      _alignmentGroup3RadioButton.setBackground(SystemColor.control);
    }

  }

  /**
   * @author George A. David
   */
  public void paintComponent(Graphics graphics)
  {
    super.paintComponent(graphics);

    int panelWidth = getWidth();
    int panelHeight = getHeight();
    int iconWidth = _imageIcon.getIconWidth();
    int iconHeight = _imageIcon.getIconHeight();
    int xCoordinate = panelWidth - iconWidth;
    int yCoordinate = panelHeight - iconHeight;

    _imageIcon.paintIcon(this, graphics, xCoordinate, yCoordinate);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void runAlignmentButton_actionPerformed(ActionEvent e)
  {
    Assert.expect(_project != null);
    
    runAlignment(false, _project.getTestProgram().getFilteredTestSubPrograms());
  }

  /**
   * @author Wei Chin, Chong
   */
  private void runAlignmentWithDifferentSliceButton_actionPerformed(ActionEvent e)
  {
    boolean isUsing2DAlignmentMethod = _project.getPanel().getPanelSettings().isUsing2DAlignmentMethod();
    
    if (_alignmentCustomizeDialog == null)
    {
      _alignmentCustomizeDialog = new AlignmentCustomizationDialog(_mainUI,StringLocalizer.keyToString("MMGUI_ALIGN_WDS_RUN_KEY"), true);
      _alignmentCustomizeDialog.populateData(_project.getPanel(), 
                                             _useRFP, 
                                             _zHeightOffsetTop, 
                                             _zHeightOffsetBottom, 
                                             _currentSignalCompensation, 
                                             _currentUserGain,
                                             _currentStageSpeed,
                                             _currentMagnification);
    }

    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    if (isUsing2DAlignmentMethod)
      _alignmentCustomizeDialog.setEnableRFPFeature(false);
    else
      _alignmentCustomizeDialog.setEnableRFPFeature(true);
    
    if (_alignmentCustomizeDialog.showDialog() == true)
    {
      boolean isDifferent = false;

      if(_useRFP != _alignmentCustomizeDialog.useRFP())
      {
        _useRFP = _alignmentCustomizeDialog.useRFP();
        isDifferent = true;
      }

      if(_zHeightOffsetTop != _alignmentCustomizeDialog.getTopOffset())
      {
        _zHeightOffsetTop = _alignmentCustomizeDialog.getTopOffset();
        isDifferent = true;
      }
      if(_zHeightOffsetBottom != _alignmentCustomizeDialog.getBottomOffset())
      {
        _zHeightOffsetBottom = _alignmentCustomizeDialog.getBottomOffset();
        isDifferent = true;
      }

      if(_currentSignalCompensation.equals(_alignmentCustomizeDialog.getIntegrationLevel()) == false)
      {
        _currentSignalCompensation = _alignmentCustomizeDialog.getIntegrationLevel();
        isDifferent = true;
      }
      
      if(_currentUserGain.equals(_alignmentCustomizeDialog.getUserGain()) == false)
      {
        _currentUserGain = _alignmentCustomizeDialog.getUserGain();
        isDifferent = true;
      }
      
      if(_currentStageSpeed.equals(_alignmentCustomizeDialog.getStageSpeed()) == false)
      {
        _currentStageSpeed = _alignmentCustomizeDialog.getStageSpeed();
        isDifferent = true;
      }
      //XCR-3470, Run alignment Customize with Mix Mag not respond
      if(_alignmentCustomizeDialog.useAllMagnification() == false && _currentMagnification.equals(_alignmentCustomizeDialog.getMagnificationType()) == false)
      {
        _currentMagnification = _alignmentCustomizeDialog.getMagnificationType();
        isDifferent = true;
      }

      if(isDifferent == true)
      {
        try
        {
          ImageManager.getInstance().deleteOnlineCachedImages();
        }
        catch(DatastoreException de)
        {
          // do nothing
        }
      }

      TestProgram testProgram = _project.getTestProgram();
      java.util.List<TestSubProgram> testSubprograms = null;
      if (_project.getPanel().getPanelSettings().isPanelBasedAlignment())
      {
        if (_project.getPanel().getPanelSettings().isLongPanel())
        {
          if (_alignmentCustomizeDialog.useAllRegion() == false)
          {
            testSubprograms = new ArrayList();
            if(_alignmentCustomizeDialog.useAllMagnification() == false)
              testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(_alignmentCustomizeDialog.getSelectedRegion(), _currentMagnification));
            else
            {
              // XCR1722 by Cheah Lee Herng 24 Oct 2013
              if (testProgram.hasTestSubProgram(_alignmentCustomizeDialog.getSelectedRegion(), MagnificationTypeEnum.LOW))
                testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(_alignmentCustomizeDialog.getSelectedRegion(), MagnificationTypeEnum.LOW));
              
              if (testProgram.hasTestSubProgram(_alignmentCustomizeDialog.getSelectedRegion(), MagnificationTypeEnum.HIGH))
                testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(_alignmentCustomizeDialog.getSelectedRegion(), MagnificationTypeEnum.HIGH));
            }
            //Khaw Chek Hau - XCR2671: Skip Learned Message Box is Prompted when one region is selected only 
            if (_alignmentCustomizeDialog.useAllMagnification() == false)
              _project.getTestProgram().clearRunAlignmentLearned();
          }
          else if(_alignmentCustomizeDialog.useAllMagnification() == false)
          {
            testSubprograms = new ArrayList();
            
            // XCR1722 by Cheah Lee Herng 24 Oct 2013
            if (testProgram.hasTestSubProgram(PanelLocationInSystemEnum.RIGHT, _currentMagnification))
              testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(PanelLocationInSystemEnum.RIGHT, _currentMagnification));
            
            if (testProgram.hasTestSubProgram(PanelLocationInSystemEnum.LEFT, _currentMagnification))
              testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(PanelLocationInSystemEnum.LEFT, _currentMagnification));
          }
        }
        else if(_alignmentCustomizeDialog.useAllMagnification() == false)
        {
          testSubprograms = new ArrayList();
          
          // XCR1722 by Cheah Lee Herng 24 Oct 2013
          if (testProgram.hasTestSubProgram(PanelLocationInSystemEnum.RIGHT, _currentMagnification))
            testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(_currentMagnification));
          
          //Khaw Chek Hau - XCR2671: Skip Learned Message Box is Prompted when one region is selected only 
          _project.getTestProgram().clearRunAlignmentLearned();
        }
      }
      else
      {
        if (_alignmentCustomizeDialog.useAllBoard() == false)
        {
          Board selectedBoard = _alignmentCustomizeDialog.getSelectedBoard();
          if (selectedBoard.getBoardSettings().isLongBoard())
          {
            testSubprograms = new ArrayList();
            if (_alignmentCustomizeDialog.useAllRegion() == false)
            {
              if(_alignmentCustomizeDialog.useAllMagnification() == false)
                testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(selectedBoard, _alignmentCustomizeDialog.getSelectedRegion(), _currentMagnification));
              else
              {
                testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(selectedBoard, _alignmentCustomizeDialog.getSelectedRegion(), MagnificationTypeEnum.LOW));
                testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(selectedBoard, _alignmentCustomizeDialog.getSelectedRegion(), MagnificationTypeEnum.HIGH));
              }
            }
            else if(_alignmentCustomizeDialog.useAllMagnification() == false)
            {
              testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(selectedBoard, PanelLocationInSystemEnum.RIGHT,  _currentMagnification));
              testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(selectedBoard, PanelLocationInSystemEnum.LEFT, _currentMagnification));
            }
            else
            {
              testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(selectedBoard, PanelLocationInSystemEnum.RIGHT,  MagnificationTypeEnum.LOW));
              testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(selectedBoard, PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.LOW));
              testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(selectedBoard, PanelLocationInSystemEnum.RIGHT,  MagnificationTypeEnum.HIGH));
              testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(selectedBoard, PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.HIGH));
            }
          }
          else
          {
            testSubprograms = new ArrayList();
            if(_alignmentCustomizeDialog.useAllMagnification() == false)
            {
              testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(selectedBoard, _currentMagnification));
            }
            else
            {
              testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(selectedBoard, MagnificationTypeEnum.LOW));
              testSubprograms.add(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(selectedBoard, MagnificationTypeEnum.HIGH));
            }
            
            //Khaw Chek Hau - XCR2473: Skip learned alignment message box should not be shown when single board is selected
            if(_alignmentCustomizeDialog.useAllMagnification() == false)
              _project.getTestProgram().clearRunAlignmentLearned();
          }
        }
      }
      if (testSubprograms == null)
      {
        testSubprograms = _project.getTestProgram().getAllTestSubPrograms();
      }

      runAlignment(true, testSubprograms);
    }
  }

  /**
   * @author Seng-Yew Lim
   */
  private void clearAlignmentButton_actionPerformed(ActionEvent e)
  {
    //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
    _project.getTestProgram().clearRunAlignmentLearned();
      
    _project.getPanel().getPanelSettings().clearAllAlignmentTransforms();
    if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
    {
      for(Board board: _project.getPanel().getBoards())
      {
        board.getBoardSettings().clearAllAlignmentTransforms();
      }
    }

    //XCR1758- Once user clear alignment, usepsp checkbox need to be auto reset 
	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    if (TestDev.isInSurfaceMappingMode())
//    {
//      if (_isLongPanel)
//      {
//        _topGroup1PspCheckBox.setSelected(false);
//        _topGroup2PspCheckBox.setSelected(false);
//        _topGroup3PspCheckBox.setSelected(false);
//        _bottomGroup1PspCheckBox.setSelected(false);
//        _bottomGroup2PspCheckBox.setSelected(false);
//        _bottomGroup3PspCheckBox.setSelected(false);
//      }
//      else
//      {
//        _group1PspCheckBox.setSelected(false);
//        _group2PspCheckBox.setSelected(false);
//        _group3PspCheckBox.setSelected(false);
//      }
//      _isSurfaceMapMode = false;
//      _testDev.changeToSurfaceMappingToolBar(false);
//      _runSurfaceMapAlignmentButton.setEnabled(false);
//      _deleteSurfaceMapAlignmentButton.setEnabled(false);
//
//      updateCadGraphics();
//    }
  }

  /**
   * @author Seng-Yew Lim
   */
  private void autoAlignmentButton_actionPerformed(ActionEvent e)
  {
    // Assuming alignment has already been done
    _alignment = Alignment.getInstance();
    try
    {
      for (TestSubProgram subProgram : _project.getTestProgram().getAllTestSubPrograms())
      {
        // XCR 1168 : run auto alignment when user gain is one
        if ( subProgram.isSubProgramPerformAlignment() && subProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
        {
          _alignment.autoAlignForAlignmentConfirmation(subProgram, _testExecution.getTestExecutionTimer());
        }
      }
    }
    catch (XrayTesterException exception)
    {
      System.out.println("Automatic Alignment : Auto alignment button pressed but program not valid.");
    }

  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleAlignmentError(final XrayTesterException alignmentException)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        // should have 3 images
        // I'll have 1-3 images
        AlignmentFailedDialog alignmentFailedDialog = new AlignmentFailedDialog(_mainUI,
            StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
            _project,
            alignmentException);
        SwingUtils.centerOnComponent(alignmentFailedDialog, _mainUI);
        alignmentFailedDialog.setVisible(true);
        endManualAlignment();
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void viewAlignmentButton_actionPerformed()
  {
    AlignmentImageDialog alignmentImageDialog = new AlignmentImageDialog(_mainUI,
        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
        _project);
    SwingUtils.centerOnComponent(alignmentImageDialog, _mainUI);
    alignmentImageDialog.setVisible(true);

//    XrayTesterException alignmentException = new AlignmentAlgorithmFailedToLocatePadsBusinessException("Group 1");
//    XrayTesterException alignmentException = new AlignmentAlgorithmFailedToCalculateTransformBusinessException();
//    XrayTesterException alignmentException = new AlignmentAlgorithmResultOutOfBoundsBusinessException();
//    AlignmentFailedDialog alignmentFailedDialog = new AlignmentFailedDialog(_mainUI,
//        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
//        _project,
//        alignmentException);
//    SwingUtils.centerOnComponent(alignmentFailedDialog, _mainUI);
//    alignmentFailedDialog.setVisible(true);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void alignmentGroup1RadioButton_actionPerformed(ActionEvent e)
  {
    // first, turn off any selected list items in the other alignment groups
    _alignmentGroup2List.removeSelectionInterval(0, _alignmentGroup2List.getMaxSelectionIndex());
    _alignmentGroup3List.removeSelectionInterval(0, _alignmentGroup3List.getMaxSelectionIndex());
    _alignmentGroup1List.setEnabled(true);
    _alignmentGroup2List.setEnabled(false);
    _alignmentGroup3List.setEnabled(false);
    if (_alignmentGroup1List.isSelectionEmpty())
      _deleteAlignmentPointButton.setEnabled(false);
    else
      _deleteAlignmentPointButton.setEnabled(true);
    _currentAlignmentGroup = _alignmentGroup1;
    _currentAlignmentGroupListModel = _alignmentGroup1ListModel;
    _currentAlignmentGroupList = _alignmentGroup1List;
    updateHighlightsAndPanelGui();

	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    if (TestDev.isInSurfaceMappingMode())
//    {
//      _isNotEmptySpace = false;
//      //updateUsePspCheckBox();
//      if (_alignmentGroup1List.getVisibleRowCount() > 0 && (_alignmentGroup2RadioButton.isSelected() == false && _alignmentGroup3RadioButton.isSelected() == false))
//      {
//        _group1PspCheckBox.setEnabled(true);
//        _group2PspCheckBox.setEnabled(false);
//        _group3PspCheckBox.setEnabled(false);
//        _viewAlignmentButton.setEnabled(true);
//        _clearAlignmentButton.setEnabled(true);
//      
//        if (_group1PspCheckBox.isSelected())
//        {
//          _isSurfaceMapMode = true;      
//          _testDev.changeToSurfaceMappingToolBar(true);
//          _runSurfaceMapAlignmentButton.setEnabled(true);
//          _deleteSurfaceMapAlignmentButton.setEnabled(true);
//        }
//        else
//        {
//          _isSurfaceMapMode = false;    
//          _testDev.changeToSurfaceMappingToolBar(false);
//          _runSurfaceMapAlignmentButton.setEnabled(false);
//          _deleteSurfaceMapAlignmentButton.setEnabled(false);
//        }
//        updateCadGraphics();
//      }
//      else
//      {
//        _group1PspCheckBox.setEnabled(false);
//      }
//      drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
//    }
    _guiObservable.stateChanged(_currentAlignmentGroup, HighlightEventEnum.CENTER_FEATURE);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void alignmentGroup2RadioButton_actionPerformed(ActionEvent e)
  {
    // first, turn off any selected list items in the other alignment groups
    _alignmentGroup1List.removeSelectionInterval(0, _alignmentGroup1List.getMaxSelectionIndex());
    _alignmentGroup3List.removeSelectionInterval(0, _alignmentGroup3List.getMaxSelectionIndex());
    _alignmentGroup1List.setEnabled(false);
    _alignmentGroup2List.setEnabled(true);
    _alignmentGroup3List.setEnabled(false);
    if (_alignmentGroup2List.isSelectionEmpty())
      _deleteAlignmentPointButton.setEnabled(false);
    else
      _deleteAlignmentPointButton.setEnabled(true);
     
    _currentAlignmentGroup = _alignmentGroup2;
    _currentAlignmentGroupListModel = _alignmentGroup2ListModel;
    _currentAlignmentGroupList = _alignmentGroup2List;
    updateHighlightsAndPanelGui();

	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    if (TestDev.isInSurfaceMappingMode())
//    {
//      _isNotEmptySpace = false;
//      //updateUsePspCheckBox();
//      if (_alignmentGroup2List.getVisibleRowCount() > 0 && (_alignmentGroup1RadioButton.isSelected() == false && _alignmentGroup3RadioButton.isSelected() == false))
//      {
//        _group2PspCheckBox.setEnabled(true);
//        _group1PspCheckBox.setEnabled(false);
//        _group3PspCheckBox.setEnabled(false);
//        _viewAlignmentButton.setEnabled(true);
//        _clearAlignmentButton.setEnabled(true);
//
//        if (_group2PspCheckBox.isSelected())
//        {
//          _isSurfaceMapMode = true;    
//          _testDev.changeToSurfaceMappingToolBar(true);
//          _runSurfaceMapAlignmentButton.setEnabled(true);
//          _deleteSurfaceMapAlignmentButton.setEnabled(true);
//        }
//        else
//        {         
//          _isSurfaceMapMode = false;    
//          _testDev.changeToSurfaceMappingToolBar(false);
//          _runSurfaceMapAlignmentButton.setEnabled(false);
//          _deleteSurfaceMapAlignmentButton.setEnabled(false);
//        }
//        updateCadGraphics();
//      }
//      else
//      {
//        _group2PspCheckBox.setEnabled(false);
//      }
//      drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
//    }
    _guiObservable.stateChanged(_currentAlignmentGroup, HighlightEventEnum.CENTER_FEATURE);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void alignmentGroup3RadioButton_actionPerformed(ActionEvent e)
  {
    // first, turn off any selected list items in the other alignment groups
    _alignmentGroup1List.removeSelectionInterval(0, _alignmentGroup1List.getMaxSelectionIndex());
    _alignmentGroup2List.removeSelectionInterval(0, _alignmentGroup2List.getMaxSelectionIndex());
    _alignmentGroup1List.setEnabled(false);
    _alignmentGroup2List.setEnabled(false);
    _alignmentGroup3List.setEnabled(true);
    if (_alignmentGroup3List.isSelectionEmpty())
      _deleteAlignmentPointButton.setEnabled(false);
    else
      _deleteAlignmentPointButton.setEnabled(true);
  
    _currentAlignmentGroup = _alignmentGroup3;
    _currentAlignmentGroupListModel = _alignmentGroup3ListModel;
    _currentAlignmentGroupList = _alignmentGroup3List;
  
    updateHighlightsAndPanelGui();

	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    if (TestDev.isInSurfaceMappingMode())
//    {
//      _isNotEmptySpace = false;
//      //updateUsePspCheckBox();
//      if (_alignmentGroup3List.getVisibleRowCount() > 0 && (_alignmentGroup1RadioButton.isSelected() == false && _alignmentGroup2RadioButton.isSelected() == false))
//      {
//        _group1PspCheckBox.setEnabled(false);
//        _group2PspCheckBox.setEnabled(false);
//        _group3PspCheckBox.setEnabled(true);
//        _viewAlignmentButton.setEnabled(true);
//        _clearAlignmentButton.setEnabled(true);
//
//        if (_group3PspCheckBox.isSelected())
//        {
//          _isSurfaceMapMode = true;
//          _runSurfaceMapAlignmentButton.setEnabled(true);
//          _deleteSurfaceMapAlignmentButton.setEnabled(true);
//          _testDev.changeToSurfaceMappingToolBar(true);
//        }
//        else
//        {
//          _isSurfaceMapMode = false;
//          _runSurfaceMapAlignmentButton.setEnabled(false);
//          _deleteSurfaceMapAlignmentButton.setEnabled(false);
//          _testDev.changeToSurfaceMappingToolBar(false);
//        }
//        updateCadGraphics();
//      }
//      else
//      {
//        _group3PspCheckBox.setEnabled(false);
//      }
//      drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
//    }
    _guiObservable.stateChanged(_currentAlignmentGroup, HighlightEventEnum.CENTER_FEATURE);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void alignmentGroup1TopRadioButton_actionPerformed(ActionEvent e)
  {
    // first, turn off any selected list items in the other alignment groups
    _alignmentGroup2TopList.removeSelectionInterval(0, _alignmentGroup2TopList.getMaxSelectionIndex());
    _alignmentGroup3TopList.removeSelectionInterval(0, _alignmentGroup3TopList.getMaxSelectionIndex());
    _alignmentGroup1BottomList.removeSelectionInterval(0, _alignmentGroup1BottomList.getMaxSelectionIndex());
    _alignmentGroup2BottomList.removeSelectionInterval(0, _alignmentGroup2BottomList.getMaxSelectionIndex());
    _alignmentGroup3BottomList.removeSelectionInterval(0, _alignmentGroup3BottomList.getMaxSelectionIndex());

    _alignmentGroup1TopList.setEnabled(true);
    _alignmentGroup2TopList.setEnabled(false);
    _alignmentGroup3TopList.setEnabled(false);
    _alignmentGroup1BottomList.setEnabled(false);
    _alignmentGroup2BottomList.setEnabled(false);
    _alignmentGroup3BottomList.setEnabled(false);

    if (_alignmentGroup1TopList.isSelectionEmpty())
      _deleteAlignmentPointButton.setEnabled(false);
    else
      _deleteAlignmentPointButton.setEnabled(true);

    _currentAlignmentGroup = _alignmentGroup1Top;
    _currentAlignmentGroupListModel = _alignmentGroup1TopListModel;
    _currentAlignmentGroupList = _alignmentGroup1TopList;
    
    _topRegionActive = true;
    _guiObservable.stateChanged(null, LongPanelAlignmentRegionEnum.TOP);
    updateHighlightsAndPanelGui();
    
	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    if (TestDev.isInSurfaceMappingMode())
//    {
//      _isNotEmptySpace = false;
//      //updateUsePspCheckBox();
//      if (_alignmentGroup1TopList.getVisibleRowCount() > 0 && (_alignmentGroup2TopRadioButton.isSelected() == false && _alignmentGroup3TopRadioButton.isSelected() == false))
//      {
//        _topGroup1PspCheckBox.setEnabled(true);
//        _topGroup2PspCheckBox.setEnabled(false);
//        _topGroup3PspCheckBox.setEnabled(false);
//        _bottomGroup1PspCheckBox.setEnabled(false);
//        _bottomGroup2PspCheckBox.setEnabled(false);
//        _bottomGroup3PspCheckBox.setEnabled(false);
//        _viewAlignmentButton.setEnabled(true);
//        _clearAlignmentButton.setEnabled(true);
//
//        if (_topGroup1PspCheckBox.isSelected())
//        {
//          _isSurfaceMapMode = true;
//          _runSurfaceMapAlignmentButton.setEnabled(true);
//          _deleteSurfaceMapAlignmentButton.setEnabled(true);
//          _testDev.changeToSurfaceMappingToolBar(true);
//        }
//        else
//        {
//          _isSurfaceMapMode = false;
//          _runSurfaceMapAlignmentButton.setEnabled(false);
//          _deleteSurfaceMapAlignmentButton.setEnabled(false);
//          _testDev.changeToSurfaceMappingToolBar(false);
//        }
//        updateCadGraphics();
//      }
//      else
//      {
//        _topGroup1PspCheckBox.setEnabled(false);
//      }
//      drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
//    }
    _guiObservable.stateChanged(_currentAlignmentGroup, HighlightEventEnum.CENTER_FEATURE);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void alignmentGroup2TopRadioButton_actionPerformed(ActionEvent e)
  {
    // first, turn off any selected list items in the other alignment groups
    _alignmentGroup1TopList.removeSelectionInterval(0, _alignmentGroup1TopList.getMaxSelectionIndex());
    _alignmentGroup3TopList.removeSelectionInterval(0, _alignmentGroup3TopList.getMaxSelectionIndex());
    _alignmentGroup1BottomList.removeSelectionInterval(0, _alignmentGroup1BottomList.getMaxSelectionIndex());
    _alignmentGroup2BottomList.removeSelectionInterval(0, _alignmentGroup2BottomList.getMaxSelectionIndex());
    _alignmentGroup3BottomList.removeSelectionInterval(0, _alignmentGroup3BottomList.getMaxSelectionIndex());

    _alignmentGroup1TopList.setEnabled(false);
    _alignmentGroup2TopList.setEnabled(true);
    _alignmentGroup3TopList.setEnabled(false);
    _alignmentGroup1BottomList.setEnabled(false);
    _alignmentGroup2BottomList.setEnabled(false);
    _alignmentGroup3BottomList.setEnabled(false);

    if (_alignmentGroup2TopList.isSelectionEmpty())
      _deleteAlignmentPointButton.setEnabled(false);
    else
      _deleteAlignmentPointButton.setEnabled(true);

    _currentAlignmentGroup = _alignmentGroup2Top;
    _currentAlignmentGroupListModel = _alignmentGroup2TopListModel;
    _currentAlignmentGroupList = _alignmentGroup2TopList;
     
    _topRegionActive = true;
    _guiObservable.stateChanged(null, LongPanelAlignmentRegionEnum.TOP);
    updateHighlightsAndPanelGui();

	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    if (TestDev.isInSurfaceMappingMode())
//    {
//      _isNotEmptySpace = false;
//      //updateUsePspCheckBox();
//      if (_alignmentGroup2TopList.getVisibleRowCount() > 0 && (_alignmentGroup1TopRadioButton.isSelected() == false && _alignmentGroup3TopRadioButton.isSelected() == false))
//      {
//        _topGroup2PspCheckBox.setEnabled(true);
//        _topGroup1PspCheckBox.setEnabled(false);
//        _topGroup3PspCheckBox.setEnabled(false);
//        _bottomGroup1PspCheckBox.setEnabled(false);
//        _bottomGroup2PspCheckBox.setEnabled(false);
//        _bottomGroup3PspCheckBox.setEnabled(false);
//        _viewAlignmentButton.setEnabled(true);
//        _clearAlignmentButton.setEnabled(true);
//
//        if (_topGroup2PspCheckBox.isSelected())
//        {
//          _isSurfaceMapMode = true;
//          _runSurfaceMapAlignmentButton.setEnabled(true);
//          _deleteSurfaceMapAlignmentButton.setEnabled(true);
//          _testDev.changeToSurfaceMappingToolBar(true);
//        }
//        else
//        {
//          _isSurfaceMapMode = false;
//          _runSurfaceMapAlignmentButton.setEnabled(false);
//          _deleteSurfaceMapAlignmentButton.setEnabled(false);
//          _testDev.changeToSurfaceMappingToolBar(false);
//        }
//        updateCadGraphics();
//      }
//      else
//      {
//        _topGroup2PspCheckBox.setEnabled(false);
//      }
//      drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
//    }
    _guiObservable.stateChanged(_currentAlignmentGroup, HighlightEventEnum.CENTER_FEATURE);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void alignmentGroup3TopRadioButton_actionPerformed(ActionEvent e)
  {
    // first, turn off any selected list items in the other alignment groups
    _alignmentGroup1TopList.removeSelectionInterval(0, _alignmentGroup1TopList.getMaxSelectionIndex());
    _alignmentGroup2TopList.removeSelectionInterval(0, _alignmentGroup2TopList.getMaxSelectionIndex());
    _alignmentGroup1BottomList.removeSelectionInterval(0, _alignmentGroup1BottomList.getMaxSelectionIndex());
    _alignmentGroup2BottomList.removeSelectionInterval(0, _alignmentGroup2BottomList.getMaxSelectionIndex());
    _alignmentGroup3BottomList.removeSelectionInterval(0, _alignmentGroup3BottomList.getMaxSelectionIndex());

    _alignmentGroup1TopList.setEnabled(false);
    _alignmentGroup2TopList.setEnabled(false);
    _alignmentGroup3TopList.setEnabled(true);
    _alignmentGroup1BottomList.setEnabled(false);
    _alignmentGroup2BottomList.setEnabled(false);
    _alignmentGroup3BottomList.setEnabled(false);

    if (_alignmentGroup3TopList.isSelectionEmpty())
      _deleteAlignmentPointButton.setEnabled(false);
    else
      _deleteAlignmentPointButton.setEnabled(true);

    _currentAlignmentGroup = _alignmentGroup3Top;
    _currentAlignmentGroupListModel = _alignmentGroup3TopListModel;
    _currentAlignmentGroupList = _alignmentGroup3TopList;
     
    _topRegionActive = true;
    _guiObservable.stateChanged(null, LongPanelAlignmentRegionEnum.TOP);
    updateHighlightsAndPanelGui();
	
	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    if (TestDev.isInSurfaceMappingMode())
//    {
//      _isNotEmptySpace = false;
//      //updateUsePspCheckBox();
//      if (_alignmentGroup3TopList.getVisibleRowCount() > 0 && (_alignmentGroup1TopRadioButton.isSelected() == false && _alignmentGroup2TopRadioButton.isSelected() == false))
//      {
//        _topGroup3PspCheckBox.setEnabled(true);
//        _topGroup2PspCheckBox.setEnabled(false);
//        _topGroup1PspCheckBox.setEnabled(false);
//        _bottomGroup1PspCheckBox.setEnabled(false);
//        _bottomGroup2PspCheckBox.setEnabled(false);
//        _bottomGroup3PspCheckBox.setEnabled(false);
//        _viewAlignmentButton.setEnabled(true);
//        _clearAlignmentButton.setEnabled(true);
//
//        if (_topGroup3PspCheckBox.isSelected())
//        {
//          _isSurfaceMapMode = true;
//          _runSurfaceMapAlignmentButton.setEnabled(true);
//          _deleteSurfaceMapAlignmentButton.setEnabled(true);
//          _testDev.changeToSurfaceMappingToolBar(true);
//        }
//        else
//        {
//          _isSurfaceMapMode = false;
//          _runSurfaceMapAlignmentButton.setEnabled(false);
//          _deleteSurfaceMapAlignmentButton.setEnabled(false);
//          _testDev.changeToSurfaceMappingToolBar(false);
//        }
//        updateCadGraphics();
//      }
//      else
//      {
//        _topGroup3PspCheckBox.setEnabled(false);
//      }
//      drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
//    }
    _guiObservable.stateChanged(_currentAlignmentGroup, HighlightEventEnum.CENTER_FEATURE);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void alignmentGroup1BottomRadioButton_actionPerformed(ActionEvent e)
  {
    // first, turn off any selected list items in the other alignment groups
    _alignmentGroup1TopList.removeSelectionInterval(0, _alignmentGroup1TopList.getMaxSelectionIndex());
    _alignmentGroup2TopList.removeSelectionInterval(0, _alignmentGroup2TopList.getMaxSelectionIndex());
    _alignmentGroup3TopList.removeSelectionInterval(0, _alignmentGroup3TopList.getMaxSelectionIndex());
    _alignmentGroup2BottomList.removeSelectionInterval(0, _alignmentGroup2BottomList.getMaxSelectionIndex());
    _alignmentGroup3BottomList.removeSelectionInterval(0, _alignmentGroup3BottomList.getMaxSelectionIndex());

    _alignmentGroup1TopList.setEnabled(false);
    _alignmentGroup2TopList.setEnabled(false);
    _alignmentGroup3TopList.setEnabled(false);
    _alignmentGroup1BottomList.setEnabled(true);
    _alignmentGroup2BottomList.setEnabled(false);
    _alignmentGroup3BottomList.setEnabled(false);

    if (_alignmentGroup1BottomList.isSelectionEmpty())
      _deleteAlignmentPointButton.setEnabled(false);
    else
      _deleteAlignmentPointButton.setEnabled(true);

    _currentAlignmentGroup = _alignmentGroup1Bottom;
    _currentAlignmentGroupListModel = _alignmentGroup1BottomListModel;
    _currentAlignmentGroupList = _alignmentGroup1BottomList;
     
    _topRegionActive = false;
    _guiObservable.stateChanged(null, LongPanelAlignmentRegionEnum.BOTTOM);
    updateHighlightsAndPanelGui();

	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    if (TestDev.isInSurfaceMappingMode())
//    {
//      _isNotEmptySpace = false;
//      //updateUsePspCheckBox();
//      if (_alignmentGroup1BottomList.getVisibleRowCount() > 0 && (_alignmentGroup2BottomRadioButton.isSelected() == false && _alignmentGroup3BottomRadioButton.isSelected() == false))
//      {
//        _bottomGroup1PspCheckBox.setEnabled(true);
//        _bottomGroup2PspCheckBox.setEnabled(false);
//        _bottomGroup3PspCheckBox.setEnabled(false);
//        _topGroup1PspCheckBox.setEnabled(false);
//        _topGroup2PspCheckBox.setEnabled(false);
//        _topGroup3PspCheckBox.setEnabled(false);
//        _viewAlignmentButton.setEnabled(true);
//        _clearAlignmentButton.setEnabled(true);
//
//        if (_bottomGroup1PspCheckBox.isSelected())
//        {
//          _isSurfaceMapMode = true;
//          _runSurfaceMapAlignmentButton.setEnabled(true);
//          _deleteSurfaceMapAlignmentButton.setEnabled(true);
//          _testDev.changeToSurfaceMappingToolBar(true);
//        }
//        else
//        {
//          _isSurfaceMapMode = false;
//          _runSurfaceMapAlignmentButton.setEnabled(false);
//          _deleteSurfaceMapAlignmentButton.setEnabled(false);
//          _testDev.changeToSurfaceMappingToolBar(false);
//        }
//        updateCadGraphics();
//      }
//      else
//      {
//        _bottomGroup1PspCheckBox.setEnabled(false);
//      }
//      drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
//    }
    _guiObservable.stateChanged(_currentAlignmentGroup, HighlightEventEnum.CENTER_FEATURE);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void alignmentGroup2BottomRadioButton_actionPerformed(ActionEvent e)
  {
    // first, turn off any selected list items in the other alignment groups
    _alignmentGroup1TopList.removeSelectionInterval(0, _alignmentGroup1TopList.getMaxSelectionIndex());
    _alignmentGroup2TopList.removeSelectionInterval(0, _alignmentGroup2TopList.getMaxSelectionIndex());
    _alignmentGroup3TopList.removeSelectionInterval(0, _alignmentGroup3TopList.getMaxSelectionIndex());
    _alignmentGroup1BottomList.removeSelectionInterval(0, _alignmentGroup1BottomList.getMaxSelectionIndex());
    _alignmentGroup3BottomList.removeSelectionInterval(0, _alignmentGroup3BottomList.getMaxSelectionIndex());

    _alignmentGroup1TopList.setEnabled(false);
    _alignmentGroup2TopList.setEnabled(false);
    _alignmentGroup3TopList.setEnabled(false);
    _alignmentGroup1BottomList.setEnabled(false);
    _alignmentGroup2BottomList.setEnabled(true);
    _alignmentGroup3BottomList.setEnabled(false);

    if (_alignmentGroup2BottomList.isSelectionEmpty())
      _deleteAlignmentPointButton.setEnabled(false);
    else
      _deleteAlignmentPointButton.setEnabled(true);

    _currentAlignmentGroup = _alignmentGroup2Bottom;
    _currentAlignmentGroupListModel = _alignmentGroup2BottomListModel;
    _currentAlignmentGroupList = _alignmentGroup2BottomList;
    
    _topRegionActive = false;
    _guiObservable.stateChanged(null, LongPanelAlignmentRegionEnum.BOTTOM);
    updateHighlightsAndPanelGui();

	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    if (TestDev.isInSurfaceMappingMode())
//    {
//      _isNotEmptySpace = false;
//      //updateUsePspCheckBox();
//      if (_alignmentGroup2BottomList.getVisibleRowCount() > 0 && (_alignmentGroup1BottomRadioButton.isSelected() == false && _alignmentGroup3BottomRadioButton.isSelected() == false))
//      {
//        _bottomGroup2PspCheckBox.setEnabled(true);
//        _bottomGroup1PspCheckBox.setEnabled(false);
//        _bottomGroup3PspCheckBox.setEnabled(false);
//        _topGroup1PspCheckBox.setEnabled(false);
//        _topGroup2PspCheckBox.setEnabled(false);
//        _topGroup3PspCheckBox.setEnabled(false);
//        _viewAlignmentButton.setEnabled(true);
//        _clearAlignmentButton.setEnabled(true);
//
//        if (_bottomGroup2PspCheckBox.isSelected())
//        {
//          _isSurfaceMapMode = true;
//          _runSurfaceMapAlignmentButton.setEnabled(true);
//          _deleteSurfaceMapAlignmentButton.setEnabled(true);
//          _testDev.changeToSurfaceMappingToolBar(true);
//        }
//        else
//        {
//          _isSurfaceMapMode = false;
//          _runSurfaceMapAlignmentButton.setEnabled(false);
//          _deleteSurfaceMapAlignmentButton.setEnabled(false);
//          _testDev.changeToSurfaceMappingToolBar(false);
//        }
//        updateCadGraphics();
//      }
//      else
//      {
//        _bottomGroup2PspCheckBox.setEnabled(false);
//      }
//      drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
//    }
    _guiObservable.stateChanged(_currentAlignmentGroup, HighlightEventEnum.CENTER_FEATURE);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void alignmentGroup3BottomRadioButton_actionPerformed(ActionEvent e)
  {
    // first, turn off any selected list items in the other alignment groups
    _alignmentGroup1TopList.removeSelectionInterval(0, _alignmentGroup1TopList.getMaxSelectionIndex());
    _alignmentGroup2TopList.removeSelectionInterval(0, _alignmentGroup2TopList.getMaxSelectionIndex());
    _alignmentGroup3TopList.removeSelectionInterval(0, _alignmentGroup3TopList.getMaxSelectionIndex());
    _alignmentGroup1BottomList.removeSelectionInterval(0, _alignmentGroup1BottomList.getMaxSelectionIndex());
    _alignmentGroup2BottomList.removeSelectionInterval(0, _alignmentGroup2BottomList.getMaxSelectionIndex());

    _alignmentGroup1TopList.setEnabled(false);
    _alignmentGroup2TopList.setEnabled(false);
    _alignmentGroup3TopList.setEnabled(false);
    _alignmentGroup1BottomList.setEnabled(false);
    _alignmentGroup2BottomList.setEnabled(false);
    _alignmentGroup3BottomList.setEnabled(true);

    if (_alignmentGroup3BottomList.isSelectionEmpty())
      _deleteAlignmentPointButton.setEnabled(false);
    else
      _deleteAlignmentPointButton.setEnabled(true);

    _currentAlignmentGroup = _alignmentGroup3Bottom;
    _currentAlignmentGroupListModel = _alignmentGroup3BottomListModel;
    _currentAlignmentGroupList = _alignmentGroup3BottomList;
    
    _topRegionActive = false;
    _guiObservable.stateChanged(null, LongPanelAlignmentRegionEnum.BOTTOM);
    updateHighlightsAndPanelGui();

	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    if (TestDev.isInSurfaceMappingMode())
//    {
//      _isNotEmptySpace = false;
//      //updateUsePspCheckBox();
//      if (_alignmentGroup3BottomList.getVisibleRowCount() > 0 && (_alignmentGroup2BottomRadioButton.isSelected() == false && _alignmentGroup1BottomRadioButton.isSelected() == false))
//      {
//        _bottomGroup3PspCheckBox.setEnabled(true);
//        _bottomGroup2PspCheckBox.setEnabled(false);
//        _bottomGroup1PspCheckBox.setEnabled(false);
//        _topGroup1PspCheckBox.setEnabled(false);
//        _topGroup2PspCheckBox.setEnabled(false);
//        _topGroup3PspCheckBox.setEnabled(false);
//        _viewAlignmentButton.setEnabled(true);
//        _clearAlignmentButton.setEnabled(true);
//
//        if (_bottomGroup3PspCheckBox.isSelected())
//        {
//          _isSurfaceMapMode = true;
//          _runSurfaceMapAlignmentButton.setEnabled(true);
//          _deleteSurfaceMapAlignmentButton.setEnabled(true);
//          _testDev.changeToSurfaceMappingToolBar(true);
//        }
//        else
//        {
//          _isSurfaceMapMode = false;
//          _runSurfaceMapAlignmentButton.setEnabled(false);
//          _deleteSurfaceMapAlignmentButton.setEnabled(false);
//          _testDev.changeToSurfaceMappingToolBar(false);
//        }
//        updateCadGraphics();
//      }
//      else
//      {
//        _bottomGroup3PspCheckBox.setEnabled(false);
//      }
//      drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
//    }
    _guiObservable.stateChanged(_currentAlignmentGroup, HighlightEventEnum.CENTER_FEATURE);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void deleteAlignmentPoint_actionPerformed()
  {
    // only one alignment group is enabled, so find that one and delete all selected rows within it
    removeSelectedItems(_currentAlignmentGroupList, _currentAlignmentGroup);
    
    // since we deleted all selected rows, we should set this to false
    // it will be re-set to true once the user selects something else
    _deleteAlignmentPointButton.setEnabled(false); 
//    updateHighlightsAndPanelGui();
    // Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
    // This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
    if (/*TestDev.isInSurfaceMappingMode() &&*/ _currentAlignmentGroup.getPads().size() <= 0)
    {
      deleteRegionButton_actionPerformed(false);
      _runSurfaceMapAlignmentButton.setEnabled(false);
      _deleteSurfaceMapAlignmentButton.setEnabled(false);
      updateUsePspCheckBox();
      if (_isPanelBasedAlignment)
      {
        _testDev.getPanelGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
        _testDev.getPanelGraphicsEngineSetup().removeSelectedOpticalRectangleLayer();
        _testDev.getPanelGraphicsEngineSetup().setDragOpticalRegionLayersVisible(false);
      }
      else
      {
        _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
        _testDev.getBoardGraphicsEngineSetup().removeSelectedOpticalRectangleLayer();
        _testDev.getBoardGraphicsEngineSetup().setDragOpticalRegionLayersVisible(false);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void alignmentListSelectionChanged(ListSelectionEvent lse)
  {
    Assert.expect(lse != null);
    if (lse.getValueIsAdjusting())
      return;

    JList sourceList = (JList)lse.getSource();
    if (sourceList.getSelectedIndices().length == 0)
      _deleteAlignmentPointButton.setEnabled(false);
    else
      _deleteAlignmentPointButton.setEnabled(true);

    if (sourceList.getSelectedIndex() == -1) // no selection
      return;

    // if the user has selected one pin of a RES, CAP or PCAP, we MUST
    // automatically select the other

    // THERE is an exception -- if the pads failes the joint type consistancy check, then only select one
    // this alignment def is invalid, but we can't crash.  The only way to get into this state is for the
    // joint type to have changed to a RES, CAP or PCAP after it was initial selected

    // This one has to be right above or right below the selected one
    if (_currentAlignmentGroup.isAlignmentGroupJointTypeInconsistent() == false)
    {
      DefaultListModel listModel = (DefaultListModel)sourceList.getModel();
      int[] selectedIndices = sourceList.getSelectedIndices();
      for (int i = 0; i < selectedIndices.length; i++)
      {
        ManualAlignmentGroupEntry entry = (ManualAlignmentGroupEntry)listModel.getElementAt(selectedIndices[i]);
        if (entry.requiresOtherPadAsPair())
        {
          PadType padType = entry.getPadType();
          ComponentType componentType = padType.getComponentType();
          String componentName = componentType.getReferenceDesignator();
          String dataToFind1 = componentName + " - " + componentType.getPadTypes().get(0).getName();
          String dataToFind2 = componentName + " - " + componentType.getPadTypes().get(1).getName();
          if (entry.toString().equalsIgnoreCase(dataToFind1))
          {
            // Then we need to select the lower one
            // remove the listener
            sourceList.removeListSelectionListener(_listSelectionListener);
            sourceList.addSelectionInterval(selectedIndices[i], selectedIndices[i] + 1);
            sourceList.addListSelectionListener(_listSelectionListener);
          }
          else if (entry.toString().equalsIgnoreCase(dataToFind2))
          {
            // must select the one above
            sourceList.removeListSelectionListener(_listSelectionListener);
            sourceList.addSelectionInterval(selectedIndices[i] - 1, selectedIndices[i]);
            sourceList.addListSelectionListener(_listSelectionListener);
          }
        }
      }
    }

    // ok, now everything is selected.  Send that list over to the graphics so it can render them selected
    Object[] selectedObjects = sourceList.getSelectedValues();
    java.util.List<Object> padsAndFiducials = new ArrayList<Object>();
    for (int i = 0; i < selectedObjects.length; i++)
    {
      ManualAlignmentGroupEntry selection = (ManualAlignmentGroupEntry)selectedObjects[i];
//      System.out.println(selection);
      if (selection.isPad())
      {
        padsAndFiducials.add(selection.getPad());
      }
      else
      {
        padsAndFiducials.add(selection.getFiducial());
      }
    }
    _guiObservable.stateChanged(padsAndFiducials, SelectionEventEnum.PAD_OR_FIDUCIAL);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void alignmentGroupList_mouseReleased(JRadioButton radioButton)
  {
    Assert.expect(radioButton != null);
    if (radioButton.isSelected() == false)
      radioButton.doClick();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void longAlignmentGroupList_mouseReleased(JRadioButton radioButton)
  {
    if (radioButton.isSelected() == false)
      radioButton.doClick();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void manualAlignmentNextButton_actionPerformed(ActionEvent e)
  {
    Point2D point = _alignmentImagePanel.getOffsetRelativeToPanelInNanoMeters();
    _alignment.imageManuallyAligned(point.getX(), point.getY());
  }

  /**
   * @author Andy Mechtenberg
   */
  private void manualAlignmentCancelButton_actionPerformed(ActionEvent e)
  {
    _userAborted = true;
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    _project.getTestProgram().setManual2DAlignmentInProgress(false);
    _alignment.cancelManualAlignment();
  }

  /**
   * Task is ready to start
   * @author George Booth
   */
  public void start()
  {
    com.axi.util.TimerUtil screenTimer = new com.axi.util.TimerUtil();
    screenTimer.start();

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();
    
    _projectObservable.addObserver(this);
    
    _isPanelBasedAlignment = _project.getPanel().getPanelSettings().isPanelBasedAlignment();
//    System.out.println("ManualAlignmentPanel().start()");

    _active = true;
    
    // add custom menus

    // add custom toolbar components
   // updateCadGraphics();
    _testDev.showCadWindow();
    
    _inspectionEventObservable.addObserver(this);
    _hardwareObservable.addObserver(this);
    
    //Khaw Chek Hau - XCR2524: Skip alignment happened when alignment failed at Fine Tuning for mix mag recipe
    if (_project.isTestProgramValid())    
      _project.getTestProgram().clearRunAlignmentLearned();

    // generate the test program, because we'll need it
    _mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_ALIGNMENT_NEEDS_TEST_PROGRAM_KEY"),
                                                  StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                  _project);
    
    if(_project.getTestProgram().getAllInspectableTestSubPrograms().isEmpty() == true)
    {
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString("GUI_UNABLE_CREATE_ALIGNMENT_POINT_DUE_TO_NO_TEST_SUBPROGRAM_KEY"),
                                    StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          _testDev.getNavigationPanel().clickButton(StringLocalizer.keyToString("MM_GUI_GROUPINGS_KEY"));
        }
      });      
    }
    else if( _project.getTestProgram().isLongProgram() && 
            (_project.getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.LEFT) == false ||  
             _project.getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.RIGHT) == false))
    {
      if(_project.getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.LEFT) == false)
        JOptionPane.showMessageDialog( _mainUI,
                                      StringLocalizer.keyToString("GUI_UNABLE_CREATE_ALIGNMENT_POINT_DUE_TO_NO_TEST_LEFT_SUBPROGRAM_KEY"),
                                      StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                      JOptionPane.ERROR_MESSAGE);
      else
        JOptionPane.showMessageDialog( _mainUI,
                                      StringLocalizer.keyToString("GUI_UNABLE_CREATE_ALIGNMENT_POINT_DUE_TO_NO_TEST_RIGHT_SUBPROGRAM_KEY"),
                                      StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                      JOptionPane.ERROR_MESSAGE);      
      
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          _testDev.getNavigationPanel().clickButton(StringLocalizer.keyToString("MM_GUI_GROUPINGS_KEY"));
        }
      });      
    }
    else
    {
      final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
                                                                      StringLocalizer.keyToString("GUI_POPULLATING_ALIGNMENT_GROUP_INTO_INTERFACE_BUSYDIALOG_MESSAGE_KEY"),
                                                                      StringLocalizer.keyToString("GUI_POPULLATING_ALIGNMENT_GROUP_INTO_INTERFACE_BUSYDIALOG_TITLE_KEY"),
                                                                      true);
      busyCancelDialog.pack();
      SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
              updateCadGraphics();

             //Kee Chin Seong - do not let populate if the testSubProgram is empty!
             if ((_project.getTestProgram().getFilteredTestSubPrograms().isEmpty() == false) &&
                (_updateData || _populateProjectData))
             {
               populateWithProjectData(_project);      
             }
             _updateData = false;
             _populateProjectData = false;

              //Kee Chin Seong - do not let populate if the testSubProgram is empty!
             if (_project.getTestProgram().getFilteredTestSubPrograms().isEmpty() == false)
               updateHighlightsAndPanelGui();

                    // Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
                        // This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
               //     if (TestDev.isInSurfaceMappingMode() && _project.getTestProgram().getFilteredTestSubPrograms().isEmpty() == false)
               //     {
               //       updateUsePspCheckBoxEnableStatus();
               //       updateCadGraphics();
               //       drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
               //     }

             _imagePanel.displayAlignmentImagePanel();
             updateAlignmentGroupVisibility();
             if (_xRayTester.isHardwareAvailable())
               _viewAlignmentButton.setEnabled(true);
             else
               _viewAlignmentButton.setEnabled(false);

                //    if (_isLongPanel && _topRegionActive)
                //       _guiObservable.stateChanged(null, LongPanelAlignmentRegionEnum.TOP);
                //     else if (_isLongPanel && (_topRegionActive == false))
                //      _guiObservable.stateChanged(null, LongPanelAlignmentRegionEnum.BOTTOM);
           }
           finally
           {
             // wait until visible
             while (busyCancelDialog.isVisible() == false)
             {
               try
               {
                 Thread.sleep(200);
               }
               catch (InterruptedException ex)
               {
                 // do nothing
               }
             }
             SwingUtils.invokeLater(new Runnable()
             {
               public void run()
               {
                 busyCancelDialog.dispose();
               }
             });
           }
         }
      });
      busyCancelDialog.setVisible(true);
    }
    screenTimer.stop();
    // Ee Jun Jiang - XCR-3006 - View Alignment button enable after switch tab
    setRunAlignmentButtonState();
  }

  /**
   * Can the environment be swapped out
   * @return true if environment can be swapped
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
//    System.out.println("ManualAlignmentPanel().isReadyToFinish()");
    if (_manualAlignmentInProgress)
    {
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString("GUI_ALIGNMENT_RUNNING_ON_EXIT_KEY"),
                                    StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      return false;
    }
    else
      return true;
  }

  /**
   * The environment is about to be swapped out and should perform cleanup
   * @author George Booth
   */
  public void finish()
  {
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
    _guiObservable.stateChanged(null, GraphicsControlEnum.ALIGNMENT_INVISIBLE);
    removeAllObservable();

    
    
    // Ying-Huan.Chu - XCR-2037 subtask (XCR-2061)  
    //Siew Yeng - XCR1776 - fix switch tab crash
//    SwingUtils.invokeLater(new Runnable()
//    {
//      public void run()
//      {
        _testDev.clearBoardGraphics();
//      }
//    });
    
    if(_alignmentCustomizeDialog != null)
    {
      _alignmentCustomizeDialog.unPopulateData();
      _alignmentCustomizeDialog.removeAll();
      _alignmentCustomizeDialog = null;
    }
    
    if(_project != null && _project.isTestProgramValid())
    {
      ProgramGeneration.resetCustomVerificationRegions(_project.getTestProgram());
      
      //Khaw Chek Hau - XCR2181: Not able to run alignment from failed board
      //Whenever tab is swapped out from Alignment tab, reset alignment learned falg
      _project.getTestProgram().clearRunAlignmentLearned();
      _project.getTestProgram().setManual2DAlignmentInProgress(false);
    }
    
    _active = false;
    
	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    if (TestDev.isInSurfaceMappingMode())
//    {
//      _isSurfaceMapMode = false;
//      _group1PspCheckBox.setSelected(false);
//      _group2PspCheckBox.setSelected(false);
//      _group3PspCheckBox.setSelected(false);
//      //_runSurfaceMapAlignmentButton.setEnabled(false);
//      //_deleteSurfaceMapAlignmentButton.setEnabled(false);
//      _opticalRegionMap.clear();
//      _isNotEmptySpace = false;
//      _dragOpticalRegionRendererObservable.deleteObserver(this);
//
//      if (_testDev != null)
//      {
//        _testDev.setBoardGraphicsAlreadyDrawnInAlignment(false);
//        if (_isPanelBasedAlignment == false)
//        {
//          _testDev.getBoardGraphicsEngineSetup().clearAlignmentDividedOpticalRegionList();
//          _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
//          _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().resetSelectedRegionCoordinate();
//          _testDev.getBoardGraphicsEngineSetup().setOpticalRegionSelectedLayersVisible(false);
//          _testDev.getBoardGraphicsEngineSetup().setDragOpticalRegionLayersVisible(false);
//          //_testDev.getBoardGraphicsEngineSetup().setAlignmentDividedOpticalRegionList(new LinkedHashMap<Rectangle, String>());
//          _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().clearCurrentlySelectedComponentRendererSet();
//        }
//        else
//        {
//          _testDev.getPanelGraphicsEngineSetup().clearAlignmentDividedOpticalRegionList();
//          _testDev.getPanelGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
//          _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().resetSelectedRegionCoordinate();
//          _testDev.getPanelGraphicsEngineSetup().setOpticalRegionSelectedLayersVisible(false);
//          _testDev.getPanelGraphicsEngineSetup().setDragOpticalRegionLayersVisible(false);
//          //_testDev.getPanelGraphicsEngineSetup().setAlignmentDividedOpticalRegionList(new LinkedHashMap<Rectangle, String>());
//          _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().clearCurrentlySelectedComponentRendererSet();
//        }
//      }
//      _alignmentGroupNameToRegionPositionNameMap.clear();
//    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void alignmentGroupList_keyTyped(KeyEvent e)
  {
    if (e.getKeyCode() == KeyEvent.VK_DELETE)
      deleteAlignmentPoint_actionPerformed();
  }

  /**
   * @author Wei Chin, Chong
   */
  private void guideForAlignmentButton_actionPerformed(ActionEvent e)
  {
    Assert.expect(_project != null);

    String totalMessages = "";
    _alignment = Alignment.getInstance();
    
    if(_project.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      for (TestSubProgram testSubProgram : _project.getTestProgram().getAllTestSubPrograms())
      {
        try
        {
          if(testSubProgram.isSubProgramPerformAlignment())
            _alignment.qualifySelectedAlignmentPoints(testSubProgram);
        }
        catch (BusinessException ex)
        {
          totalMessages = totalMessages + ex.getMessage() + "\n\n";
        }
      }
    }
    else
    {
      for (TestSubProgram testSubProgram : _project.getTestProgram().getBoardsFilteredTestSubPrograms())
      {
        try
        {
          if(testSubProgram.isSubProgramPerformAlignment())
            _alignment.qualifySelectedAlignmentPoints(testSubProgram);
        }
        catch (BusinessException ex)
        {
          totalMessages = totalMessages + ex.getMessage() + "\n\n";
        }
      }
    }

    if(totalMessages.length() > 0)
      JOptionPane.showConfirmDialog(_mainUI,
                                  StringUtil.format(totalMessages, 50),
                                  StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                  JOptionPane.DEFAULT_OPTION,
                                  JOptionPane.INFORMATION_MESSAGE);
  }


  /**
   * @param topZHeight
   * @param bottomZHeight
   *
   * @author Wei Chin, Chong
   * @author Chin Seong, Kee 
   * - Test Sub Program must be "locked" before user is align the image on CAD.
   */
  private void runAlignment(final boolean useCustomAlignment, final java.util.List <TestSubProgram> customTestSubprograms)
  {
    Assert.expect(customTestSubprograms != null);
    Assert.expect(customTestSubprograms.size() > 0);
    // the user has initiated an alignment.  Now would be a good time to check if they're completed group
    // definition meets certain geometrical criteria
    _alignment = Alignment.getInstance();
    //XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
    _customTestSubprogramSize = customTestSubprograms.size();
    _runAlignmentButton.setEnabled(false);
    _runAlignmentWithDifferentSliceButton.setEnabled(false);
    if (_project.getPanel().getPanelSettings().isUsing2DAlignmentMethod())
    {
      String alignment2DPath = Directory.getAlignment2DImagesDir(_project.getName());
      
      if (FileUtilAxi.exists(alignment2DPath) == false)
      {
        //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
        _project.getTestProgram().clearRunAlignmentLearned();

        _project.getPanel().getPanelSettings().clearAllAlignmentTransforms();
        if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
        {
          for(Board board: _project.getPanel().getBoards())
          {
            board.getBoardSettings().clearAllAlignmentTransforms();
          }
        }
      }
      
      _project.getTestProgram().setManual2DAlignmentInProgress(true);
      
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      //Maximize V810 JFrame due to save 2D Alignment have to be done in maximized windows
      _mainUI.setVisible(true);
      _mainUI.setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    // re-acquire the test program -- sometimes it changes during a save or whatever.
    _mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_ALIGNMENT_NEEDS_TEST_PROGRAM_KEY"),
                                                  StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                  _project);
    
    try
    {
      boolean hasHighMagComponent = _testExecution.containHighMagInspectionInCurrentLoadedProject();
      if (_testExecution.isPanelLoaded(hasHighMagComponent, _project.getName()) == false)
      {
        if (_testExecution.isPromptWarningMsgIfRecipeContainHighMag() == true)
        {
          if (_project.getPanel().getPanelSettings().hasHighMagnificationComponent() == true)
          {
            String projectName = _project.getPanel().getProject().getName();

            LocalizedString message = new LocalizedString("BUS_SUBTYPE_CONTAINED_HIGH_MAG_COMPONENTS_RUN_ALIGNMENT_IN_TESTER_EXCEPTION_KEY", new Object[]
            {
              projectName
            });
            JOptionPane.showMessageDialog(_mainUI, StringLocalizer.keyToString(message),
              StringLocalizer.keyToString("GUI_ABOUT_BOX_MAIN_MENU_TITLE_KEY"),
              JOptionPane.INFORMATION_MESSAGE);
          }
        }
      }
    }
    catch (XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
      _runAlignmentButton.setEnabled(true);
      _runAlignmentWithDifferentSliceButton.setEnabled(true);
      _project.getTestProgram().setManual2DAlignmentInProgress(false);
      return;
    }

    if(useCustomAlignment)
    {
      int zHeightOffsetBottomInNanometer = MathUtil.convertMilsToNanoMetersInteger(_zHeightOffsetBottom);
      int zHeightOffsetTopInNanometer = MathUtil.convertMilsToNanoMetersInteger(_zHeightOffsetTop);
      
      ProgramGeneration.customizeVerificationRegions(_project.getTestProgram(), _useRFP, zHeightOffsetTopInNanometer, zHeightOffsetBottomInNanometer, _currentSignalCompensation, _currentUserGain,_currentStageSpeed);
    }
    else
      ProgramGeneration.resetCustomVerificationRegions(_project.getTestProgram());

    // warn the user about the importance of panel thickness ONE TIME ONLY
    if (_project.getPanel().getPanelSettings().wasUserWarnedOfThicknessImportance() == false)
    {
      MathUtilEnum units = _project.getDisplayUnits();
      int panelThicknessInNanoMeters = _project.getPanel().getThicknessInNanometers();
      
      //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on";
      if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
          panelThicknessInNanoMeters = panelThicknessInNanoMeters - XrayTester.getSystemPanelThicknessOffset();

      double actualThicknessInUnits = MathUtil.convertUnits(panelThicknessInNanoMeters, MathUtilEnum.NANOMETERS, units);
      String userOption = StringLocalizer.keyToString("MMGUI_ALIGN_CONTINUE_MANUAL_ALIGNMENT_KEY");
      LocalizedString warningMessage = new LocalizedString("MMGUI_ALIGN_PANEL_THICKNESS_IMPORTANT_KEY", new Object[]{actualThicknessInUnits, units});

      String totalMessage = StringLocalizer.keyToString(warningMessage) + "\n\n" + userOption;
      int answer = ChoiceInputDialog.showConfirmDialog(_mainUI, StringUtil.format(totalMessage, 50), StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"));
      _project.getPanel().getPanelSettings().setUserWasWarnedOfThicknessImportance();
      if (answer != JOptionPane.YES_OPTION)
      {
        _runAlignmentButton.setEnabled(true);
        _runAlignmentWithDifferentSliceButton.setEnabled(true);
        //Khaw Chek Hau - XCR2285: 2D Alignment on v810
        _project.getTestProgram().setManual2DAlignmentInProgress(false);
        return;
      }
    }
    
    //Khaw Chek Hau - XCR2472: Skip learned alignment message box still shown even aligning has been completed
    if (_project.getTestProgram().isUsingSkipAlignmentFeature()) 
    {
      int choice = JOptionPane.showConfirmDialog(_mainUI,
                                                 StringLocalizer.keyToString("RERUN_MANUAL_ALIGNMENT_MESSAGE_KEY"),
                                                 StringLocalizer.keyToString("RERUN_MANUAL_ALIGNMENT_TITLE_KEY"),
                                                 JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
      if (choice == JOptionPane.NO_OPTION) 
      {
        _project.getTestProgram().clearRunAlignmentLearned();
      } 
      else if (choice == JOptionPane.CLOSED_OPTION) 
      {
        _runAlignmentButton.setEnabled(true);
        _runAlignmentWithDifferentSliceButton.setEnabled(true);
        _project.getTestProgram().setManual2DAlignmentInProgress(false);
        return;
      }
    }
    else
      _project.getTestProgram().clearRunAlignmentLearned();

    try
    {
      _testExecution.clearExitAndAbortFlags();    
    }
    catch (XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
      _runAlignmentButton.setEnabled(true);
      _runAlignmentWithDifferentSliceButton.setEnabled(true);
      _project.getTestProgram().setManual2DAlignmentInProgress(false);
      return; 
    }
    SwingWorkerThread.getInstance3().invokeLater(new Runnable()
    {
      public void run()
      {
        final BooleanLock alignmentComfirmationLock = new BooleanLock(false);
        _userAborted = false;
        _exception = null;
       
        boolean generateVerificationImage = true;

        //XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
        _isManualAlignmentInitiallyComplete = _project.getPanel().getPanelSettings().isManualAlignmentCompleted();
        _alignment.setIsManualAlignmentInitiallyComplete(_isManualAlignmentInitiallyComplete);

        //Khaw Chek Hau - XCR3761 : Skip generating 2D verification image if the panel is not unloaded
        if (doesVerificationImageExist(customTestSubprograms) && (_project.getPanel().getPanelSettings().isUsing2DAlignmentMethod() == false || 
             (_project.getPanel().getPanelSettings().isUsing2DAlignmentMethod() && _project.getTestProgram().isPanelRemainsWithoutUnloadedFor2DVerificationImage())))
        {          
          int choice = JOptionPane.showConfirmDialog(_mainUI,
                                                     StringLocalizer.keyToString("GENERATE_NEW_VERIFICATION_IMAGES_MESSAGE_KEY"),
                                                     StringLocalizer.keyToString("GENERATE_NEW_VERIFICATION_IMAGES_TITLE_KEY"), 
                                                     JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
          if (choice == JOptionPane.YES_OPTION)
          {
            // regenerate verification images if user clicked 'Yes'.
            generateVerificationImage = true;
          }
          if (choice == JOptionPane.NO_OPTION)
          {
            // do not regenerate verification images if user clicked 'No'.
            generateVerificationImage = false;
          }
          else if (choice == JOptionPane.CLOSED_OPTION)
          {
            _runAlignmentButton.setEnabled(true);
            _runAlignmentWithDifferentSliceButton.setEnabled(true);
            //Khaw Chek Hau - XCR2285: 2D Alignment on v810
            _project.getTestProgram().setManual2DAlignmentInProgress(false);
            return; 
          }
        }
        
        if (FileUtilAxi.exists(Directory.getXrayVerificationImagesDir(_project.getName())) && _project.getPanel().getPanelSettings().isUsing2DAlignmentMethod())
        {
          //Khaw Chek Hau - XCR2285: 2D Alignment on v810
          int choice = JOptionPane.showConfirmDialog(_mainUI,
                                                     StringLocalizer.keyToString("CLEAR_3D_VERIFICATION_IMAGE_MESSAGE_KEY"),
                                                     StringLocalizer.keyToString("CLEAR_3D_VERIFICATION_IMAGE_TITLE_KEY"), 
                                                     JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
          if (choice == JOptionPane.YES_OPTION)
          {
            generateVerificationImage = true;
            
            try
            {
              FileUtilAxi.delete(Directory.getXrayVerificationImagesDir(_project.getName()));
            }
            catch (DatastoreException de)
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                            de,
                                            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                            true);
            }
          }
          else
          {
            _runAlignmentButton.setEnabled(true);
            _runAlignmentWithDifferentSliceButton.setEnabled(true);
            _project.getTestProgram().setManual2DAlignmentInProgress(false);
            return; 
          }
        }
        
        for (final TestSubProgram subProgram : customTestSubprograms)
        {
          alignmentComfirmationLock.setValue(false);

          // Kok Chun, Tan - XCR-3108 - merge PCR-190 to handle abort in manualalignment
          if (_testExecution.didUserAbort() || _userAborted || (_exception != null))
            break;
          
          if(subProgram.isSubProgramPerformAlignment()==false || 
             (subProgram.isAllRegionNotIspectable() && subProgram.isSubProgramPerformAlignment()==false))
          {
            continue;
          }
          
//          MagnificationEnum.changeMagnification(subProgram.getMagnificationType());
          
          if(subProgram.isLowMagnification())
          {
            _imagePanel.displayAlignmentImagePanel();
            _alignmentImagePanel = _imagePanel.getAlignmentImagePanel();
          }
          
          if(subProgram.isHighMagnification())
          {
            _imagePanel.displayAlignmentImagePanelForHighMag();  
            _alignmentImagePanel = _imagePanel.getAlignmentImagePanelForHighMag();
          }
          
          try
          {
            //Khaw Chek Hau - XCR2285: 2D Alignment on v810
            if (_project.getPanel().getPanelSettings().isUsing2DAlignmentMethod())
              _project.getTestProgram().setManual2DAlignmentInProgress(true);
            
            // Ying-Huan.Chu [XCR1704]
            //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
            if ((generateVerificationImage == true || _project.doVerificationImagesExist(subProgram) == false) && subProgram.isRunAlignmentLearned() == false)
              _mainUI.generateVerificationImagesForManualAlignment(subProgram, _useRFP);
            else
            {
              boolean hasHighMagComponent = _testExecution.containHighMagInspectionInTestSubPrograms(customTestSubprograms);
              //Siew Yeng - XCR-2691 - move this code here as both condition(panel loaded or not loaded) also need to check startup if necessary
              _mainUI.performStartupIfNeeded();
              //XCR-3797, Failed to cancel CDNA task after start up
              if (_testExecution.didUserAbort() || _userAborted) // if user cancelled verification image generation
              {
                _runAlignmentButton.setEnabled(true);
                _runAlignmentWithDifferentSliceButton.setEnabled(true);
                //Khaw Chek Hau - XCR2285: 2D Alignment on v810
                _project.getTestProgram().setManual2DAlignmentInProgress(false);
                //XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
                _countTestSubprogram = 0;
                return;  
              }
              if (_testExecution.isPanelLoaded(hasHighMagComponent, subProgram.getTestProgram().getProject().getName()) == false)
              {
                _mainUI.loadPanel();
              }
              else
              {
                //Ngie Xing - XCR-2466 : System able to run alignment even though the opened recipe is different with the board loaded
                _testExecution.checkThatCurrentlyLoadedPanelMatchesProjectPanelSize(subProgram.getTestProgram());
                _testExecution.checkThatCurrentlyLoadedPanelProjectNameMatchesProjectName(subProgram.getTestProgram());
              }
            }
            
            //Khaw Chek Hau - XCR3761 : Skip generating 2D verification image if the panel is not unloaded
            _project.getTestProgram().setPanelRemainsWithoutUnloadedFor2DVerificationImage(true);
            
            // user may have cancelled out of either the load or the verification image generation
            // Kok Chun, Tan - XCR-3108 - merge PCR-190 to handle abort in manualalignment
            if (_testExecution.didUserAbort() || _userAborted || // if user cancelled alignment
                _testExecution.isPanelLoaded(false, "") == false || // if user cancelled panel loading
                _project.doVerificationImagesExist(subProgram) == false) // if user cancelled verification image generation
            {
              _runAlignmentButton.setEnabled(true);
              _runAlignmentWithDifferentSliceButton.setEnabled(true);
              //Khaw Chek Hau - XCR2285: 2D Alignment on v810
              _project.getTestProgram().setManual2DAlignmentInProgress(false);
              //XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
              _countTestSubprogram = 0;
              return;  
            }
          }
          catch (XrayTesterException ex)
          {
            _mainUI.handleXrayTesterException(ex);
            _runAlignmentButton.setEnabled(true);
            _runAlignmentWithDifferentSliceButton.setEnabled(true);
            //Khaw Chek Hau - XCR2285: 2D Alignment on v810
            _project.getTestProgram().setManual2DAlignmentInProgress(false);
            //XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
            _countTestSubprogram = 0;
            return;
          }
          finally
          {
            try
            {
              // Kok Chun, Tan - XCR-3108 - merge PCR-190 to handle abort in manualalignment
              if(_testExecution.didUserAbort())
              {
                // XCR-3312 Assert if no board in the machine during alignment setup
                if (_project.doVerificationImagesExist(subProgram) && _testExecution.isPanelLoaded(false, ""))
                {
                  // XCR-3207 Unable to cancel verification image and align the panel/board with existing verification image
                  manualAlignOnImage(subProgram, alignmentComfirmationLock);
                  break;
                }
                else
                {
                  endManualAlignment();
                  break;
                }
              }
            }
            catch(XrayTesterException ex)
            {
              _mainUI.handleXrayTesterException(ex);
              _runAlignmentButton.setEnabled(true);
              _runAlignmentWithDifferentSliceButton.setEnabled(true);
              _project.getTestProgram().setManual2DAlignmentInProgress(false);
              //XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
              _countTestSubprogram = 0;
              return;
            }
          }
          
          try
          {
            // Kok Chun, Tan - XCR-3108 - merge PCR-190 to handle abort in manualalignment
            if (_testExecution.didUserAbort() || _userAborted || (_exception != null))
            {
              if (_testExecution.didUserAbort() || _userAborted) // Handle user manual abort case
              {
                // XCR-3312 Assert if no board in the machine during alignment setup
                if (_project.doVerificationImagesExist(subProgram) && _testExecution.isPanelLoaded(false, ""))
                {
                  // XCR-3207 Unable to cancel verification image and align the panel/board with existing verification image
                  manualAlignOnImage(subProgram, alignmentComfirmationLock);
                  break;
                }
                else
                  break;
              }
              else
                break;
            }
          }
          catch(XrayTesterException ex)
          {
            _mainUI.handleXrayTesterException(ex);
            _runAlignmentButton.setEnabled(true);
            _runAlignmentWithDifferentSliceButton.setEnabled(true);
            _project.getTestProgram().setManual2DAlignmentInProgress(false);
            //XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
            _countTestSubprogram = 0;
            return;
          }
          
          // XCR-3207 Unable to cancel verification image and align the panel/board with existing verification image
          manualAlignOnImage(subProgram, alignmentComfirmationLock);
        }
        //XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
        _countTestSubprogram = 0;
        _project.getTestProgram().setManual2DAlignmentInProgress(false);
      }
    });
  }
  
  /**
   * Function to check if Verification Image Generation is required. [XCR1704]
   * @author Ying-Huan.Chu
   */
  private boolean doesVerificationImageExist(java.util.List <TestSubProgram> customTestSubprograms)
  {
    boolean doesVerificationImageExist = false;
    for (final TestSubProgram subProgram : customTestSubprograms)
    {
      if (_project.areVerificationImagesValid(subProgram))
      {
        return true;
      }
    }
    return doesVerificationImageExist;
  }
  
  /**
   * @return
   * @author Wei Chin
   */
  int getCurrentAlignmentGroupIndex()
  {
    int index = 0;
    if (_currentAlignmentGroup == _alignmentGroup1 || _currentAlignmentGroup == _alignmentGroup1Top)
      index = 0;
    else if (_currentAlignmentGroup == _alignmentGroup2 || _currentAlignmentGroup == _alignmentGroup2Top)
      index = 1;
    else if (_currentAlignmentGroup == _alignmentGroup3 || _currentAlignmentGroup == _alignmentGroup3Top)
      index = 2;
    else if (_currentAlignmentGroup == _alignmentGroup1Bottom)
      index = 3;
    else if (_currentAlignmentGroup == _alignmentGroup2Bottom)
      index = 4;
    else if (_currentAlignmentGroup == _alignmentGroup3Bottom)
      index = 5;
    return index;
  }

  /**
   * @return
   * @author Wei Chin
   */
  int getCurrentAlignmentGroupIndex(AlignmentGroup alignmentGroup)
  {
    int index = 0;
    if (alignmentGroup == _alignmentGroup1 || alignmentGroup == _alignmentGroup1Top)
      index = 0;
    else if (alignmentGroup == _alignmentGroup2 || alignmentGroup == _alignmentGroup2Top)
      index = 1;
    else if (alignmentGroup == _alignmentGroup3 || alignmentGroup == _alignmentGroup3Top)
      index = 2;
    else if (alignmentGroup == _alignmentGroup1Bottom)
      index = 3;
    else if (alignmentGroup == _alignmentGroup2Bottom)
      index = 4;
    else if (alignmentGroup == _alignmentGroup3Bottom)
      index = 5;
    return index;
  }
  
  /**
   * @author Wei Chin
   */
   void updateCadGraphics()
   {
     if (_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
     {
       if (_testDev.isBoardGraphicsAlreadyDrawnInAlignment() == false)
       {
         _testDev.initGraphicsToBoard();
         _testDev.switchToBoardGraphics();
       }
       // Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
       // This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//       if (TestDev.isInSurfaceMappingMode())
//       {
//         if (isSurfaceMapMode())
//         {
//           _testDev.changeGraphicsToBoard(_project.getPanel().getBoards().get(0));
//           // _testDev.showCadWindow();
//           _imagePanel.displayAlignmentImagePanel();
//           updateAlignmentGroupVisibility();
//
//           if (_dragOpticalRegionRendererObservable == null)
//           {
//             _dragOpticalRegionRendererObservable = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getDragOpticalRegionObservable();
//           }
//
//           _dragOpticalRegionRendererObservable.addObserver(this);
//
//         }
//         else
//         {
//           if (_testDev.isBoardGraphicsAlreadyDrawnInAlignment() == false)
//             _testDev.changeGraphicsToBoard(_project.getPanel().getBoards().get(0));
//           _imagePanel.displayAlignmentImagePanel();
//           updateAlignmentGroupVisibility();
//
//           if (_dragOpticalRegionRendererObservable == null)
//           {
//             _dragOpticalRegionRendererObservable = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getDragOpticalRegionObservable();
//           }
//
//           _dragOpticalRegionRendererObservable.addObserver(this);
//         }
//       }
//       else
//       {
         _testDev.changeGraphicsToBoard(_project.getPanel().getBoards().get(0));
//       }
      _selectedRendererObservable = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getSelectedRendererObservable();
    }
    else
    {
      _testDev.switchToPanelGraphics();
      //_testDev.resetSurfaceMapPanel(_project);
      // Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	  // This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//      if (TestDev.isInSurfaceMappingMode())
//      {
//        if (_dragOpticalRegionRendererObservable == null)
//        {
//          _dragOpticalRegionRendererObservable = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getDragOpticalRegionObservable();
//        }
//        _dragOpticalRegionRendererObservable.addObserver(this);
//      }
      _selectedRendererObservable = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getSelectedRendererObservable();
    }
    _selectedRendererObservable.addObserver(this);
  }
   
   /**
    * @param e 
    * @author Wei Chin
    */
  void alignmentSettingButton_actionPerformed(ActionEvent e)
  {
    if (_alignmentSettingDialog == null)
    {
      _alignmentSettingDialog = new AlignmentSettingDialog(_mainUI,StringLocalizer.keyToString("MMGUI_ALIGNMENT_SETTING_KEY"), true);
    }

    _alignmentSettingDialog.populateData(_project.getPanel());
    _alignmentSettingDialog.showDialog();
    
    //XCR-2730, Auto-alignment IL/Gain setting should follow Customize Alignment settings
    if (Project.getCurrentlyLoadedProject().isTestProgramValid() == false)
    {
      final BusyCancelDialog busyDialog = new BusyCancelDialog(
        MainMenuGui.getInstance(),
        StringLocalizer.keyToString("MMGUI_ALIGNMENT_NEEDS_TEST_PROGRAM_KEY"),
        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
        true);
      busyDialog.pack();
      SwingUtils.centerOnComponent(busyDialog, MainMenuGui.getInstance());
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          Project.getCurrentlyLoadedProject().regenerateTestProgram();
          busyDialog.dispose();
        }
      });
      busyDialog.setVisible(true);
    }
     
    
    // XCR-2037 subtask : XCR-2040 (Ying-Huan.Chu)
    updateAllAlignmentGroups();
  }

  /**
   * @author Jack Hwee
   * @edited By Kee Chin Seong - the alignment is checked the alignment is COMPLETE.
   */
  public void handleDragOpticalRegionEvent(Object object)
  {
    /*if (_project.getPanel().getPanelSettings().isManualAlignmentCompleted() == false)
    {
      return;
    }*/
    
    //Kee Chin Seong - do not let populate if the testSubProgram is empty!
    if (_project.getTestProgram().getFilteredTestSubPrograms().isEmpty()) 
      return;
    
    if (_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
    {
      if (_isLongPanel)
        handleBoardSurfaceMapAlignmentRegionForLongPanel();
      else
        handleBoardSurfaceMapAlignmentRegion();
    }
    else
    {
      if (_isLongPanel)
        handlePanelSurfaceMapAlignmentRegionForLongPanel();
      else
        handlePanelSurfaceMapAlignmentRegion();
    }
  }
  
  /**
   * @author Jack Hwee
   */
  public static boolean isSurfaceMapMode()
  {
    return _isSurfaceMapMode;
  }

  /**
   * @author Jack Hwee
   */
  public Rectangle offsetAlignmentRegionToOpticalRegion(Rectangle alignmentRegion)
  {
    Rectangle rectInPixels = new Rectangle(0, 0, 47, 30);
    Rectangle rectInNano = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(rectInPixels);
    Rectangle offsetRect = new Rectangle((int) alignmentRegion.getMinX(), (int) alignmentRegion.getMinY(), (int) alignmentRegion.getWidth(), (int) alignmentRegion.getHeight());

    double opticalWidth = rectInNano.getWidth();
    double opticalHeight = rectInNano.getHeight();
    double newAlignmentWidth = 0;
    double newAlignmentHeight = 0;
    double minX = alignmentRegion.getMinX();
    double minY = alignmentRegion.getMinY();

    if (alignmentRegion != null)
    {
      if (alignmentRegion.getWidth() < opticalWidth)
      {
        newAlignmentWidth = opticalWidth;
        offsetRect.setRect(minX, minY, newAlignmentWidth, alignmentRegion.getHeight());
      }
      if (alignmentRegion.getHeight() < opticalHeight)
      {
        newAlignmentHeight = opticalHeight;
        if (newAlignmentWidth == 0)
        {
          newAlignmentWidth = alignmentRegion.getWidth();
        }
        offsetRect.setRect(minX, minY, newAlignmentWidth, newAlignmentHeight);
      }
    }
    return offsetRect;
  }

  /**
   * @author Jack Hwee
   * @edited By Kee Chin Seong - the alignment is checked the alignment is COMPLETE.
   */
    // Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//  private void runSurfaceMapAlignmentButton_actionPerformed()
//  {
//    if (_isNotEmptySpace)
//    {
//      if (_alignmentGroupNameToRegionPositionNameMap.containsKey(_currentAlignmentGroup.getName()))
//      {
//        int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
//                StringLocalizer.keyToString("PSP_RESELECT_ALIGNMENT_REGION_WILL_RESET_POINTS_KEY"),
//                StringLocalizer.keyToString("PSP_RESELECT_ALIGNMENT_REGION_BUTTON_KEY"),
//                JOptionPane.YES_NO_OPTION,
//                JOptionPane.WARNING_MESSAGE);
//
//        if (answer == JOptionPane.YES_OPTION) 
//        {
//           deleteRegionButton_actionPerformed(true);
//        }
//        if (answer == JOptionPane.NO_OPTION || answer == JOptionPane.CLOSED_OPTION)
//        {
//          return;
//        }
//      }
//
//      try
//      {
//        if (checkProjectHasBeenModified())
//        {
//          if (_mainUI.isPanelInLegalState() == false)
//          {
//            return;
//          }
//
//          if (_project.isTestProgramValid() == false)
//          {
//            _mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_SURFACE_MAPPING_GEN_NEEDS_TEST_PROGRAM_KEY"),
//                    StringLocalizer.keyToString("IMTB_GENERATE_SURFACE_MAPPING_KEY"),
//                    _project);
//          }
//
//          if (_mainUI.beforeGenerateSurfaceMapping(this))
//          {
//            //if (_project.getPanel().getPanelSettings().isManualAlignmentCompleted())
//            //{
//              String prevBoardName = null;
//              OpticalRegion opticalRegion = null;
//              final boolean simulationMode = XrayTester.getInstance().isSimulationModeOn();
//              boolean usePanelSetting = _project.getPanel().getPanelSettings().isPanelBasedAlignment();
//
//              boolean updateSurfaceMapData = false;
//              int currentOpticalRectangleIndex = 0;
//              int totalInspectedOpticalRectangles = 0;
//
//              if (usePanelSetting)
//              {              
//                java.util.List<Rectangle> convertedRectangleInNanometersList = new ArrayList<Rectangle>();
//                totalInspectedOpticalRectangles = _testDev.getPanelGraphicsEngineSetup().getAllInspectedOpticalRegions();
//                
//                // make a list of unselected rectangle first         
//                Map<Rectangle, Rectangle> unselectedDividedOpticalRegionMap = new LinkedHashMap<Rectangle, Rectangle>();                
//                for (Map.Entry<Rectangle, String> mapEntry : _testDev.getPanelGraphicsEngineSetup().getAlignmentDividedOpticalRegionList().entrySet())
//                {
//                  final Rectangle rectangleInPixel = mapEntry.getKey();
//                  String alignmentGroup = mapEntry.getValue();
//                  
//                  ++currentOpticalRectangleIndex;
//                  if (currentOpticalRectangleIndex == totalInspectedOpticalRectangles)
//                  {
//                    updateSurfaceMapData = true;
//                  }
//                  
//                  Board selectedBoard = null;
//                  for (Board board : _project.getPanel().getBoards())
//                  {
//                    Rectangle rectangleInNanometers = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(rectangleInPixel);
//                    if (board.getShapeRelativeToPanelInNanoMeters().getBounds().intersects(rectangleInNanometers))
//                    {
//                      selectedBoard = board;
//                      break;
//                    }
//                  }
//                  
//                  // to handle the case of adding rectangles on the board's carrier (not on the board)
//                  if (selectedBoard == null)
//                  {
//                    java.util.Map<Board, Double> boardToDistanceSquareMap = new LinkedHashMap<>();
//                    for (Board board : _project.getPanel().getBoards())
//                    {
//                      Rectangle rectangleInNanometers = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(rectangleInPixel);
//                      Rectangle boardRectangleInNanometers = board.getShapeRelativeToPanelInNanoMeters().getBounds();
//                      double distanceSquare = SurfaceMapping.getInstance().getDistanceSquare(rectangleInNanometers, boardRectangleInNanometers);
//                      boardToDistanceSquareMap.put(board, distanceSquare);
//                    }
//                    double shortestDistanceSquare = Double.MAX_VALUE;
//                    for (Map.Entry<Board, Double> entry : boardToDistanceSquareMap.entrySet())
//                    {
//                      Board board = entry.getKey();
//                      double distanceSquare = entry.getValue();
//                      if (distanceSquare <= shortestDistanceSquare)
//                      {
//                        shortestDistanceSquare = distanceSquare;
//                        selectedBoard = board;
//                      }
//                    }
//                  }
//                  Assert.expect(selectedBoard != null);
//
//                  // Jack Hwee - Make sure image is "latest" for display
//                  final String imageFullPath = FileName.getAlignmentOpticalImageFullPath("temp", 0);
//                  if (FileUtil.exists(imageFullPath))
//                  {
//                    try
//                    {
//                      FileUtil.delete(imageFullPath);
//                    }
//                    catch (CouldNotDeleteFileException ex)
//                    {
//                       // do nothing
//                    }
//                  }
//                  final Board finalBoard = selectedBoard;
//                  final BusyCancelDialog busyDialog = new BusyCancelDialog(
//                          _mainUI,
//                          StringLocalizer.keyToString("PSP_UPDATE_DATA_KEY"),
//                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
//                          true);
//                  busyDialog.pack();
//                  SwingUtils.centerOnComponent(busyDialog, _mainUI);
//                  SwingWorkerThread.getInstance().invokeLater(new Runnable()
//                  {
//                    public void run()
//                    {
//                      try
//                      {
//                        doRegionSurfaceMapping(opticalCameraRectangle, finalBoard);
//                        
//                        if (simulationMode == false)
//                          waitForHeightMap();
//                      }
//                      finally
//                      {
//                        busyDialog.dispose();
//                      }
//                    }
//                  });
//                  busyDialog.setVisible(true);
//
//                  // In multi-board scenario, we need to seperate out the drawn region into separate instance of OpticalRegion
//                  // so that we can each OpticalRegion can have its own set of components.
//                  if (prevBoardName == null || prevBoardName.equalsIgnoreCase(selectedBoard.getName()) == false)
//                  {
//                    opticalRegion = new OpticalRegion();
//
//                    Rectangle rectInNano = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(rectangleInPixel);
//
//                    opticalRegion.setRegion((int) rectInNano.getMinX(), (int) rectInNano.getMinY(), (int) rectInNano.getWidth(), (int) rectInNano.getHeight());
//                    //  opticalRegion.setRegion((int)(_xCoordinateInNano - (_regionWidthInNano/2)), (int)(_yCoordinateInNano - (_regionHeightInNano / 2)), (int)_regionWidthInNano, (int)_regionHeightInNano);
//
//                    opticalRegion.setAffineTransform(this.getCurrentAffineTransformForOpticalRegion());
//                  }
//                  
//                   // In multi-board scenario, we need to seperate out the drawn region into separate instance of OpticalRegion
//                  // so that we can each OpticalRegion can have its own set of components.
//                  if (prevBoardName == null || prevBoardName.equalsIgnoreCase(selectedBoard.getName()) == false)
//                  {
//                    if (_project.getPanel().isMultiBoardPanel() == false)
//                    {
//                      opticalRegion = new OpticalRegion();
//                      Rectangle rectInNano = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(rectangleInPixel);
//
//                      opticalRegion.setRegion((int) rectInNano.getMinX(), (int) rectInNano.getMinY(), (int) rectInNano.getWidth(), (int) rectInNano.getHeight());
//                      opticalRegion.setAffineTransform(this.getCurrentAffineTransformForOpticalRegion());
//                    }
//                    else if (prevBoardName == null)
//                    {
//                      opticalRegion = new OpticalRegion();
//                      Rectangle rectInNano = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(rectangleInPixel);
//
//                      opticalRegion.setRegion((int) rectInNano.getMinX(), (int) rectInNano.getMinY(), (int) rectInNano.getWidth(), (int) rectInNano.getHeight());
//                      opticalRegion.setAffineTransform(this.getCurrentAffineTransformForOpticalRegion());
//                    }
//                  }
//
//
//                  Assert.expect(opticalRegion != null);
//
//                  Rectangle convertedRectangleInNanometers = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(rectangleInPixel);                  
//                  _testDev.getPanelGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
//                  
//                  _pspLiveVideoDialog = new SurfaceMapLiveVideoDialog(_mainUI, 
//                                                                      "PSP Live Video", 
//                                                                      selectedBoard, 
//                                                                      opticalRegion, 
//                                                                      convertedRectangleInNanometers, 
//                                                                      _simulationMode, 
//                                                                      updateSurfaceMapData,
//                                                                      usePanelSetting, 
//                                                                      _currentAlignmentGroupForSurfaceMap, 
//                                                                      unselectedDividedOpticalRegionMap,
//                                                                      _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getCurrentZoomFactor());
//                  _pspLiveVideoDialog.setLocationRelativeTo(this._viewAlignmentButton);
//                  _pspLiveVideoDialog.setVisible(true);
//
//                  if (_pspLiveVideoDialog.isCancelSurfaceMapping())
//                  {
//                    convertedRectangleInNanometersList.clear();
//                    break;
//                  }
//                  
//                  if (_currentAlignmentGroup.getName().equals(alignmentGroup))
//                  {
//                    if (_project.getPanel().getPanelAlignmentSurfaceMapSettings().isAlignmentOpticalRegionExists(_currentAlignmentGroup.getName()))
//                    {
//                      for (OpticalCameraRectangle ocr : _project.getPanel().getPanelAlignmentSurfaceMapSettings().getAlignmentOpticalRegion(_currentAlignmentGroup.getName()).getAllOpticalCameraRectangles())                 
//                        convertedRectangleInNanometersList.add(ocr.getRegion().getRectangle2D().getBounds());
//                    }                            
//                    if (convertedRectangleInNanometersList.isEmpty() == false)
//                      updateUsePspCheckBox();
//                    
//                    updateCadGraphics();
//                    _testDev.getPanelGraphicsEngineSetup().handleSelectedAlignmentOpticalRegion(convertedRectangleInNanometersList);
//                  }
//
//                  _isNotEmptySpace = false;
//                  prevBoardName = selectedBoard.getName();                  
//                  //  }                                                    
//                }
//                if (_pspLiveVideoDialog.isCancelSurfaceMapping() == false)
//                {
//                  _testDev.getPanelGraphicsEngineSetup().clearAlignmentDividedOpticalRegionList();
//                  _opticalRegionMap.clear();
//                }
//                convertedRectangleInNanometersList.clear();
//              }
//              else
//              {
//                java.util.List<Rectangle> convertedRectangleInNanometersList = new ArrayList<Rectangle>();
//                totalInspectedOpticalRectangles = _testDev.getBoardGraphicsEngineSetup().getAllInspectedAlignmentOpticalRegions();
//                
//                // make a list of unselected rectangle first         
//                Map<Rectangle, Rectangle> unselectedDividedOpticalRegionMap = new LinkedHashMap<Rectangle, Rectangle>();                
//                for (Map.Entry<Rectangle, String> mapEntry : _testDev.getBoardGraphicsEngineSetup().getAlignmentDividedOpticalRegionList().entrySet())
//                {
//                  final Rectangle rectangleInPixel = mapEntry.getKey();
//                  String alignmentGroup = mapEntry.getValue();
//
//                  ++currentOpticalRectangleIndex;
//                  if (currentOpticalRectangleIndex == totalInspectedOpticalRectangles)
//                  {
//                    updateSurfaceMapData = true;
//                  }                  
//
//                  Board selectedBoard = null;
//
//                  for (Board board : _project.getPanel().getBoards())
//                  {
//                    Rectangle rectangleInNanometers = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(rectangleInPixel);
//                    if (board.getShapeRelativeToPanelInNanoMeters().getBounds().intersects(rectangleInNanometers))
//                    {
//                      selectedBoard = board;
//                      break;
//                    }
//                  }
//                  
//                  // to handle the case of adding rectangles on the board's carrier (not on the board)
//                  if (selectedBoard == null)
//                  {
//                    java.util.Map<Board, Double> boardToDistanceSquareMap = new LinkedHashMap<>();
//                    for (Board board : _project.getPanel().getBoards())
//                    {
//                      Rectangle rectangleInNanometers = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(rectangleInPixel);
//                      Rectangle boardRectangleInNanometers = board.getShapeRelativeToPanelInNanoMeters().getBounds();
//                      double distanceSquare = SurfaceMapping.getInstance().getDistanceSquare(rectangleInNanometers, boardRectangleInNanometers);
//                      double sqrtDistanceSquare = Math.sqrt(distanceSquare);
//                      boardToDistanceSquareMap.put(board, sqrtDistanceSquare);
//                    }
//                    double shortestDistanceSquare = 0.0;
//                    boolean isFirst = true;
//                    for (Map.Entry<Board, Double> entry : boardToDistanceSquareMap.entrySet())
//                    {
//                      Board board = entry.getKey();
//                      double sqrtDistanceSquare = entry.getValue();
//                      if (isFirst || (sqrtDistanceSquare <= shortestDistanceSquare))
//                      {
//                        isFirst = false;
//                        shortestDistanceSquare = sqrtDistanceSquare;
//                        selectedBoard = board;
//                      }
//                    }
//                  }
//                  Assert.expect(selectedBoard != null);
//
//                  // Jack Hwee - Make sure image is "latest" for display
//                  final String imageFullPath = FileName.getAlignmentOpticalImageFullPath("temp", 0);
//                  if (FileUtil.exists(imageFullPath))
//                  {
//                    try
//                    {
//                      FileUtil.delete(imageFullPath);
//                    }
//                    catch (CouldNotDeleteFileException ex)
//                    {
//                       // do nothing
//                    }
//                  }
//                  final Board finalBoard = selectedBoard;
//                  final BusyCancelDialog busyDialog = new BusyCancelDialog(
//                          _mainUI,
//                          StringLocalizer.keyToString("PSP_UPDATE_DATA_KEY"),
//                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
//                          true);
//                  busyDialog.pack();
//                  SwingUtils.centerOnComponent(busyDialog, _mainUI);
//                  SwingWorkerThread.getInstance().invokeLater(new Runnable()
//                  {
//                    public void run()
//                    {
//                      try
//                      {
//                        doRegionSurfaceMapping(opticalCameraRectangle, finalBoard);
//                        
//                        if (simulationMode == false)
//                          waitForHeightMap();
//                      }
//                      finally
//                      {
//                        busyDialog.dispose();
//                      }
//                    }
//                  });
//                  busyDialog.setVisible(true);
//                  
//                  if (selectedBoard.getBoardAlignmentSurfaceMapSettings().isAlignmentOpticalRegionExists(alignmentGroup))
//                  {
//                    opticalRegion = selectedBoard.getBoardAlignmentSurfaceMapSettings().getAlignmentOpticalRegion(alignmentGroup);
//                  }
//                  else
//                  {
//                    opticalRegion = new OpticalRegion();
//
//                    Rectangle rectInNano = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(rectangleInPixel);
//                    opticalRegion.setBoard(finalBoard);
//                    opticalRegion.setRegion((int) rectInNano.getMinX(), (int) rectInNano.getMinY(), (int) rectInNano.getWidth(), (int) rectInNano.getHeight());                      
//                  }                  
//                  opticalRegion.setAffineTransform(this.getCurrentAffineTransformForOpticalRegion());
//             
//                  Assert.expect(opticalRegion != null);
//
//                  Rectangle convertedRectangleInNanometers = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(rectangleInPixel);                                   
//                  _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
//                  
//                  // Display live view dialog
//                  _pspLiveVideoDialog = new SurfaceMapLiveVideoDialog(_mainUI, 
//                                                                      "PSP Live Video", 
//                                                                      selectedBoard, 
//                                                                      opticalRegion, 
//                                                                      convertedRectangleInNanometers, 
//                                                                      _simulationMode, 
//                                                                      updateSurfaceMapData,
//                                                                      usePanelSetting, 
//                                                                      _currentAlignmentGroupForSurfaceMap, 
//                                                                      unselectedDividedOpticalRegionMap,
//                                                                      _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getCurrentZoomFactor());
//      
//                  _pspLiveVideoDialog.setLocationRelativeTo(this._viewAlignmentButton);
//
//                  _pspLiveVideoDialog.setVisible(true);
//
//                  if (_pspLiveVideoDialog.isCancelSurfaceMapping())
//                  {
//                    break;
//                  }
//                  if (_currentAlignmentGroup.getName().equals(alignmentGroup))
//                  {
//                    if (selectedBoard.getBoardAlignmentSurfaceMapSettings().isAlignmentOpticalRegionExists(_currentAlignmentGroup.getName()))
//                    {
//                      for (OpticalCameraRectangle ocr : selectedBoard.getBoardAlignmentSurfaceMapSettings().getAlignmentOpticalRegion(_currentAlignmentGroup.getName()).getOpticalCameraRectangles(selectedBoard))                 
//                        convertedRectangleInNanometersList.add(ocr.getRegion().getRectangle2D().getBounds());
//                    }                  
//                    if (convertedRectangleInNanometersList.isEmpty() == false)
//                      updateUsePspCheckBox();
//                    
//                    updateCadGraphics();
//                    _testDev.getBoardGraphicsEngineSetup().handleSelectedAlignmentOpticalRegion(convertedRectangleInNanometersList);
//                  }
//
//                  _isNotEmptySpace = false;
//                  prevBoardName = selectedBoard.getName();              
//                  // }                                                    
//                }
//                if (_pspLiveVideoDialog.isCancelSurfaceMapping() == false)
//                {
//                  _testDev.getBoardGraphicsEngineSetup().clearAlignmentDividedOpticalRegionList();
//                  _opticalRegionMap.clear();
//                }
//                convertedRectangleInNanometersList.clear();
//              }
//              
//              setRunAlignmentButtonState();
//              _clearAlignmentButton.setEnabled(true);
//              _viewAlignmentButton.setEnabled(true);      
//              populateSurfaceMapAlignmentIntoMap();
//          }
//        }
//      }
//      catch (XrayTesterException ex)
//      {
//        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
//                ex,
//                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
//                true);
//      }
//    }
//    else
//    {
//
//      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
//              StringLocalizer.keyToString("PSP_SELECT_ALIGNMENT_REGION_BEFORE_LIVE_VIEW_KEY"),
//              StringLocalizer.keyToString("PSP_SURFACE_MAP_ALIGN_RUN_KEY"),
//              JOptionPane.ERROR_MESSAGE);
//      return;
//
//    }
//  }

  /**
   * @author Jack Hwee
   */
  public boolean checkProjectHasBeenModified()
  {
    // we must have saved/up-to-date project in order to generate an image set that we know is compatible.
    // so, here, if the project is "dirty" then ask the user to save.  If they save, then move on to generate the
    // image set, if not do nothing here.
    boolean startSurfaceMapping = true;

    if (_project.hasBeenModified())
    {
      LocalizedString saveQuestion = new LocalizedString("MM_GUI_SAVE_CHANGES_FOR_SURFACE_MAPPING_KEY",
              new Object[]
      {
        _project.getName()
      });
      int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
              StringUtil.format(StringLocalizer.keyToString(saveQuestion), _CHARS_PER_LINE),
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
              JOptionPane.YES_NO_OPTION,
              JOptionPane.WARNING_MESSAGE);
      if (answer == JOptionPane.YES_OPTION)
      {
        _mainUI.getMainMenuBar().saveProject();
        startSurfaceMapping = true;
      }
      else
      {
        startSurfaceMapping = false;  // chose not to save, so exit this operation
      }
    }
    return startSurfaceMapping;
  }

  /**
   *
   * @author Jack Hwee
   */
//  private void doRegionSurfaceMapping(OpticalCameraRectangle opticalCameraRectangle,
//                                      Board board)
//  {
//    Assert.expect(opticalCameraRectangle != null);
//    Assert.expect(board != null);
//    
//    _surfaceMapping = SurfaceMapping.getInstance();
//    _surfaceMapping.setCurrentAffineTransformForOpticalRegion(this.getCurrentAffineTransformForOpticalRegion());
//    _testExecution.doRegionSurfaceMapping(opticalCameraRectangle, board);
//  }

  /**
   *
   * @author Jack Hwee
   */
  private void handleBoardSurfaceMapAlignmentRegion()
  {
	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    double zoomFactor = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getCurrentZoomFactor();
//    double fitGraphicsToScreenWhenWindowIsResizedNewScale = 1;
//    _currentAlignmentGroupForSurfaceMap = "0";
//    _isNotEmptySpace = false;
//
//    Rectangle2D rectangle = null;
//
//    if (_testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().isWindowResized())
//    {
//
//      rectangle = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getResizedSelectedComponentRegion();
//      if (rectangle == null)
//      {
//        rectangle = new Rectangle(0, 0, 0, 0);
//      }
//
//      fitGraphicsToScreenWhenWindowIsResizedNewScale = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getFitGraphicsToScreenWhenWindowIsResizedNewScale();
//    }
//    else
//    {
//      rectangle = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getSelectedComponentRegion();
//      // rectangle = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getOpticalCameraRectangleInPixel();
//      if (rectangle == null)
//      {
//        rectangle = new Rectangle(0, 0, 0, 0);
//      }
//    }
//
//    java.util.List<PanelRectangle> allowableAlignmentRegionInNanoMeters = new ArrayList<PanelRectangle>();
//
//    Board board = _project.getPanel().getBoards().get(0);
//    //for (ReconstructionRegion alignmentRegion : _project.getTestProgram().getTestSubPrograms().get(0).getAlignmentRegions())
//    for (ReconstructionRegion alignmentRegion : _project.getTestProgram().getTestSubProgram(board).getAlignmentRegions())
//    {
//      allowableAlignmentRegionInNanoMeters.add(alignmentRegion.getAllowableAlignmentRegionInNanoMeters());
//    }
//
//    Rectangle region1 = allowableAlignmentRegionInNanoMeters.get(0).getRectangle2D().getBounds();
//    Rectangle region2 = allowableAlignmentRegionInNanoMeters.get(1).getRectangle2D().getBounds();
//    Rectangle region3 = allowableAlignmentRegionInNanoMeters.get(2).getRectangle2D().getBounds();
//
//    Rectangle alignmentGroup1Region = null;
//    Rectangle alignmentGroup2Region = null;
//    Rectangle alignmentGroup3Region = null;
//
//    Rectangle zoomAlignmentGroup1Region = null;
//    Rectangle zoomAlignmentGroup2Region = null;
//    Rectangle zoomAlignmentGroup3Region = null;
//
//    // add some offset to alignment region so that it is fit to at least one optical region
//    if (region1 != null)
//    {
//      alignmentGroup1Region = createAllowableRectangleForPsp(region1);
//    }
//
//    if (region2 != null)
//    {
//      alignmentGroup2Region = createAllowableRectangleForPsp(region2);
//    }
//
//    if (region3 != null)
//    {
//      alignmentGroup3Region = createAllowableRectangleForPsp(region3);
//    }
//
//    //  if (_isZoom)
//    // {
//    //    _opticalRegionMap.clear();
//    //     _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().clearAllSurfaceMapAlignmentRegion();
//    //  }
//
//    if (MathUtil.fuzzyEquals(zoomFactor, 1.0) == false)
//    {
//      zoomFactor = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getCurrentZoomFactor();
//
//      if (alignmentGroup1Region != null)
//      {
//        zoomAlignmentGroup1Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup1Region);
//        alignmentGroup1Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup1Region);
//        alignmentGroup1Region.setBounds((int)alignmentGroup1Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup1Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup1Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup1Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      if (alignmentGroup2Region != null)
//      {
//        zoomAlignmentGroup2Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup2Region);
//        alignmentGroup2Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup2Region);
//        alignmentGroup2Region.setBounds((int)alignmentGroup2Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup2Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup2Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup2Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      if (alignmentGroup3Region != null)
//      {
//        zoomAlignmentGroup3Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup3Region);
//        alignmentGroup3Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup3Region);
//        alignmentGroup3Region.setBounds((int)alignmentGroup3Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup3Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup3Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup3Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      _isZoom = true;
//    }
//    else
//    {
//      _isZoom = false;
//    }
//
//    int minX = (int) rectangle.getBounds2D().getMinX() + MathUtil.roundDoubleToInt(5 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    int minY = (int) rectangle.getBounds2D().getMinY() + MathUtil.roundDoubleToInt(5 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    int widthInPixels = (int) rectangle.getBounds2D().getWidth() - MathUtil.roundDoubleToInt(10 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    int heightInPixels = (int) rectangle.getBounds2D().getHeight() - MathUtil.roundDoubleToInt(10 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    PanelRectangle bottomSectionOfOpticalLongRectangleInPixel = _project.getPanel().getPanelSettings().getBottomSectionOfOpticalLongPanel();
//    boolean isOpticalLongPanel = _project.getPanel().getPanelSettings().isOpticalLongPanel();
//
//    Point2D coorInPixels = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().
//            convertFromRendererToPixelCoordinates((int) bottomSectionOfOpticalLongRectangleInPixel.getMinX(), (int) bottomSectionOfOpticalLongRectangleInPixel.getMaxY());
//
//    Point2D point = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertFromPixelToRendererCoordinates((int) 0, (int) 0);
//    int a = (int) (bottomSectionOfOpticalLongRectangleInPixel.getMaxX() + point.getX());
//    int b = (int) (bottomSectionOfOpticalLongRectangleInPixel.getMinX() + point.getY());
//
//    Point2D point2 = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(a, b);
//
//    int opticalLongRectangleWidthInPixel = (int) (point2.getX() + point2.getY());
//
//    int a2 = (int) (point.getX() - bottomSectionOfOpticalLongRectangleInPixel.getMaxY());
//    int b2 = (int) (point.getY() - bottomSectionOfOpticalLongRectangleInPixel.getMinY());
//
//    Point2D point3 = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(a2, b2);
//
//    int opticalLongRectangleHeightInPixel = (int) (-point3.getX() - point3.getY());
//
//    Rectangle bottomSectionOfOpticalLongRectangleInPixelInRectangle = new Rectangle((int) coorInPixels.getX(), (int) coorInPixels.getY(), opticalLongRectangleWidthInPixel, opticalLongRectangleHeightInPixel);
//    bottomSectionOfOpticalLongRectangleInPixel.setRect(coorInPixels.getX(), coorInPixels.getY(), opticalLongRectangleWidthInPixel, opticalLongRectangleHeightInPixel);
//    boolean isFirstDrag = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().isOpticalRegionFirstDrag();
//  
//    // Get the panel bounding rectangle in pixel
//    PanelRectangle panelRectangle = null;
//    if (_project.getPanel().getPanelSettings().isLongPanel())
//    {
//      panelRectangle = _project.getPanel().getPanelSettings().getRightSectionOfLongPanel();
//      panelRectangle.add(_project.getPanel().getPanelSettings().getLeftSectionOfLongPanel().getRectangle2D());
//    }
//    else
//    {
//      panelRectangle = _project.getPanel().getPanelSettings().getRightSectionOfLongPanel();
//    }
//    Point2D panelRectangleCoordinateInPixels = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates((int) panelRectangle.getMinX(), (int) panelRectangle.getMaxY());
//    Point2D panelRectanglePoint = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertFromPixelToRendererCoordinates((int) 0, (int) 0);
//    int pointA = (int) (panelRectangle.getMaxX() + panelRectanglePoint.getX());
//    int pointB = (int) (panelRectangle.getMinX() + panelRectanglePoint.getY());
//
//    Point2D panelRectanglePoint2 = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(pointA, pointB);
//    int panelRectangleWidthInPixel = (int) (panelRectanglePoint2.getX() + panelRectanglePoint2.getY());
//
//    int pointA2 = (int) (panelRectanglePoint.getX() - panelRectangle.getMaxY());
//    int pointB2 = (int) (panelRectanglePoint.getY() - panelRectangle.getMinY());
//
//    Point2D panelRectanglePoint3 = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(pointA2, pointB2);
//    int panelRectangleHeightInPixel = (int) (-panelRectanglePoint3.getX() - panelRectanglePoint3.getY());
//
//    Rectangle panelRectangleInPixel = new Rectangle();
//    panelRectangleInPixel.setRect(panelRectangleCoordinateInPixels.getX(), panelRectangleCoordinateInPixels.getY(), panelRectangleWidthInPixel, panelRectangleHeightInPixel);
//
//    _opticalRegionMap = new LinkedHashMap<Rectangle, String>();
//    Map<Rectangle, BooleanRef> opticalRegionMap = new LinkedHashMap<Rectangle, BooleanRef>();
//  
//    opticalRegionMap = OpticalRegionGeneration.generateCameraFovPanelRectangles(panelRectangleInPixel, bottomSectionOfOpticalLongRectangleInPixelInRectangle, isOpticalLongPanel, (int)minX, (int)minY, widthInPixels, heightInPixels, fitGraphicsToScreenWhenWindowIsResizedNewScale, zoomFactor, isFirstDrag);
//    for (Map.Entry<Rectangle, BooleanRef> mapEntry : opticalRegionMap.entrySet())
//    {
//      Rectangle dividedRectangle = mapEntry.getKey();
//      BooleanRef booleanRef = mapEntry.getValue();
//
//      Rectangle newDividedRectangle = new Rectangle();
//      newDividedRectangle = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(dividedRectangle);
//
//      if (_group1PspCheckBox.isSelected() && _alignmentGroup1RadioButton.isSelected())
//      {
//        //  if (alignmentGroup1Region.getBounds().equals( region1.getBounds()))
//        //  {
//        if (alignmentGroup1Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
//        //  }
////        else
////        {
////          if (alignmentGroup1Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//
//      if (_group2PspCheckBox.isSelected() && _alignmentGroup2RadioButton.isSelected())
//      {
//        if (alignmentGroup2Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup2Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//
//      if (_group3PspCheckBox.isSelected() && _alignmentGroup3RadioButton.isSelected())
//      {
//        if (alignmentGroup3Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup3Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//    }
//    _testDev.getBoardGraphicsEngineSetup().setAlignmentDividedOpticalRegionList(_opticalRegionMap);
//
//    if (_opticalRegionMap.size() > 0)
//    {
//      _isNotEmptySpace = true;
//    }
//
//    _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
//    _testDev.getBoardGraphicsEngineSetup().setDragOpticalRegionLayersVisible(true);
  }

  /**
   *
   * @author Jack Hwee
   */
  private void handlePanelSurfaceMapAlignmentRegion()
  {
	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    double zoomFactor = 1;
//    double fitGraphicsToScreenWhenWindowIsResizedNewScale = 1;
//    _isNotEmptySpace = false;
//
//    Rectangle2D rectangle = null;
//
//    if (_testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().isWindowResized())
//    {
//      rectangle = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getResizedSelectedComponentRegion();
//      if (rectangle == null)
//      {
//        rectangle = new Rectangle(0, 0, 0, 0);
//      }
//
//      fitGraphicsToScreenWhenWindowIsResizedNewScale = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getFitGraphicsToScreenWhenWindowIsResizedNewScale();
//    }
//    else
//    {
//      rectangle = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getSelectedComponentRegion();
//      // rectangle = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getOpticalCameraRectangleInPixel();
//      if (rectangle == null)
//      {
//        rectangle = new Rectangle(0, 0, 0, 0);
//      }
//    }
//
//
//    java.util.List<PanelRectangle> allowableAlignmentRegionInNanoMeters = new ArrayList<PanelRectangle>();
//
//    for (ReconstructionRegion alignmentRegion : _project.getTestProgram().getTestSubPrograms().get(0).getAlignmentRegions())
//    {
//      allowableAlignmentRegionInNanoMeters.add(alignmentRegion.getAllowableAlignmentRegionInNanoMeters());
//    }
//
//    Rectangle region1 = allowableAlignmentRegionInNanoMeters.get(0).getRectangle2D().getBounds();
//    Rectangle region2 = allowableAlignmentRegionInNanoMeters.get(1).getRectangle2D().getBounds();
//    Rectangle region3 = allowableAlignmentRegionInNanoMeters.get(2).getRectangle2D().getBounds();
//
//    Rectangle alignmentGroup1Region = null;
//    Rectangle alignmentGroup2Region = null;
//    Rectangle alignmentGroup3Region = null;
//
//    Rectangle zoomAlignmentGroup1Region = null;
//    Rectangle zoomAlignmentGroup2Region = null;
//    Rectangle zoomAlignmentGroup3Region = null;
//
//    // add some offset to alignment region so that it is fit to at least one optical region
//    if (region1 != null)
//    {
//      alignmentGroup1Region = createAllowableRectangleForPsp(region1);
//    }
//
//    if (region2 != null)
//    {
//      alignmentGroup2Region = createAllowableRectangleForPsp(region2);
//    }
//
//    if (region3 != null)
//    {
//      alignmentGroup3Region = createAllowableRectangleForPsp(region3);
//    }
//
//    if (MathUtil.fuzzyEquals(_testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getCurrentZoomFactor(), 1.0) == false)
//    {
//      zoomFactor = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getCurrentZoomFactor();
//
//      if (alignmentGroup1Region != null)
//      {
//        zoomAlignmentGroup1Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup1Region);
//        alignmentGroup1Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup1Region);
//        alignmentGroup1Region.setBounds((int)alignmentGroup1Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup1Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup1Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup1Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      if (alignmentGroup2Region != null)
//      {
//        zoomAlignmentGroup2Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup2Region);
//        alignmentGroup2Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup2Region);
//        alignmentGroup2Region.setBounds((int)alignmentGroup2Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup2Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup2Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup2Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      if (alignmentGroup3Region != null)
//      {
//        zoomAlignmentGroup3Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup3Region);
//        alignmentGroup3Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup3Region);
//        alignmentGroup3Region.setBounds((int)alignmentGroup3Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup3Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup3Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup3Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      _isZoom = true;
//    }
//    else
//    {
//      _isZoom = false;
//    }
//
//    int minX = (int) rectangle.getBounds2D().getMinX() + MathUtil.roundDoubleToInt(5 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    int minY = (int) rectangle.getBounds2D().getMinY() + MathUtil.roundDoubleToInt(5 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    int widthInPixels = (int) rectangle.getBounds2D().getWidth() - MathUtil.roundDoubleToInt(10 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    int heightInPixels = (int) rectangle.getBounds2D().getHeight() - MathUtil.roundDoubleToInt(10 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    PanelRectangle bottomSectionOfOpticalLongRectangleInPixel = _project.getPanel().getPanelSettings().getBottomSectionOfOpticalLongPanel();
//    boolean isOpticalLongPanel = _project.getPanel().getPanelSettings().isOpticalLongPanel();
//
//    Point2D coorInPixels = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().
//            convertFromRendererToPixelCoordinates((int) bottomSectionOfOpticalLongRectangleInPixel.getMinX(), (int) bottomSectionOfOpticalLongRectangleInPixel.getMaxY());
//
//    Point2D point = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertFromPixelToRendererCoordinates((int) 0, (int) 0);
//    int a = (int) (bottomSectionOfOpticalLongRectangleInPixel.getMaxX() + point.getX());
//    int b = (int) (bottomSectionOfOpticalLongRectangleInPixel.getMinX() + point.getY());
//
//    Point2D point2 = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(a, b);
//
//    int opticalLongRectangleWidthInPixel = (int) (point2.getX() + point2.getY());
//
//    int a2 = (int) (point.getX() - bottomSectionOfOpticalLongRectangleInPixel.getMaxY());
//    int b2 = (int) (point.getY() - bottomSectionOfOpticalLongRectangleInPixel.getMinY());
//
//    Point2D point3 = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(a2, b2);
//
//    int opticalLongRectangleHeightInPixel = (int) (-point3.getX() - point3.getY());
//
//    Rectangle bottomSectionOfOpticalLongRectangleInPixelInRectangle = new Rectangle((int) coorInPixels.getX(), (int) coorInPixels.getY(), opticalLongRectangleWidthInPixel, opticalLongRectangleHeightInPixel);
//    bottomSectionOfOpticalLongRectangleInPixel.setRect(coorInPixels.getX(), coorInPixels.getY(), opticalLongRectangleWidthInPixel, opticalLongRectangleHeightInPixel);
//    boolean isFirstDrag = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().isOpticalRegionFirstDrag();
//
//      // Get the panel bounding rectangle in pixel
//    PanelRectangle panelRectangle = null;
//    if (_project.getPanel().getPanelSettings().isLongPanel())
//    {
//      panelRectangle = _project.getPanel().getPanelSettings().getRightSectionOfLongPanel();
//      panelRectangle.add(_project.getPanel().getPanelSettings().getLeftSectionOfLongPanel().getRectangle2D());
//    }
//    else
//    {
//      panelRectangle = _project.getPanel().getPanelSettings().getRightSectionOfLongPanel();
//    }
//    Point2D panelRectangleCoordinateInPixels = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates((int) panelRectangle.getMinX(), (int) panelRectangle.getMaxY());
//    Point2D panelRectanglePoint = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertFromPixelToRendererCoordinates((int) 0, (int) 0);
//    int pointA = (int) (panelRectangle.getMaxX() + panelRectanglePoint.getX());
//    int pointB = (int) (panelRectangle.getMinX() + panelRectanglePoint.getY());
//
//    Point2D panelRectanglePoint2 = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(pointA, pointB);
//    int panelRectangleWidthInPixel = (int) (panelRectanglePoint2.getX() + panelRectanglePoint2.getY());
//
//    int pointA2 = (int) (panelRectanglePoint.getX() - panelRectangle.getMaxY());
//    int pointB2 = (int) (panelRectanglePoint.getY() - panelRectangle.getMinY());
//
//    Point2D panelRectanglePoint3 = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(pointA2, pointB2);
//    int panelRectangleHeightInPixel = (int) (-panelRectanglePoint3.getX() - panelRectanglePoint3.getY());
//
//    Rectangle panelRectangleInPixel = new Rectangle();
//    panelRectangleInPixel.setRect(panelRectangleCoordinateInPixels.getX(), panelRectangleCoordinateInPixels.getY(), panelRectangleWidthInPixel, panelRectangleHeightInPixel);
//
//    _opticalRegionMap = new LinkedHashMap<Rectangle, String>();
//    Map<Rectangle, BooleanRef> opticalRegionMap = new LinkedHashMap<Rectangle, BooleanRef>();
//  
//    opticalRegionMap = OpticalRegionGeneration.generateCameraFovPanelRectangles(panelRectangleInPixel, bottomSectionOfOpticalLongRectangleInPixelInRectangle, isOpticalLongPanel, (int)minX, (int)minY, widthInPixels, heightInPixels, fitGraphicsToScreenWhenWindowIsResizedNewScale, zoomFactor, isFirstDrag);   
//    for (Map.Entry<Rectangle, BooleanRef> mapEntry : opticalRegionMap.entrySet())
//    {
//      Rectangle dividedRectangle = mapEntry.getKey();
//      BooleanRef booleanRef = mapEntry.getValue();
//
//      Rectangle newDividedRectangle = new Rectangle();
//      newDividedRectangle = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(dividedRectangle);
//
//      if (_group1PspCheckBox.isSelected() && _alignmentGroup1RadioButton.isSelected())
//      {
//        //  if (alignmentGroup1Region.getBounds().equals( region1.getBounds()))
//        //  {
//        if (alignmentGroup1Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
//        //  }
////        else
////        {
////          if (alignmentGroup1Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//
//      if (_group2PspCheckBox.isSelected() && _alignmentGroup2RadioButton.isSelected())
//      {
//        if (alignmentGroup2Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup2Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//
//      if (_group3PspCheckBox.isSelected() && _alignmentGroup3RadioButton.isSelected())
//      {
//        if (alignmentGroup3Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup3Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//
//    }
//
//    _testDev.getPanelGraphicsEngineSetup().setAlignmentDividedOpticalRegionList(_opticalRegionMap);
//
//    if (_opticalRegionMap.size() > 0)
//    {
//      _isNotEmptySpace = true;
//    }
//    _testDev.getPanelGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
//    _testDev.getPanelGraphicsEngineSetup().setDragOpticalRegionLayersVisible(true);
  }
  
   /**
   *
   * @author Jack Hwee
   */
  private void handlePanelSurfaceMapAlignmentRegionForLongPanel()
  {
	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    double zoomFactor = 1;
//    double fitGraphicsToScreenWhenWindowIsResizedNewScale = 1;
//    _isNotEmptySpace = false;
//
//    Rectangle2D rectangle = null;
//
//    if (_testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().isWindowResized())
//    {
//      rectangle = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getResizedSelectedComponentRegion();
//      if (rectangle == null)
//      {
//        rectangle = new Rectangle(0, 0, 0, 0);
//      }
//
//      fitGraphicsToScreenWhenWindowIsResizedNewScale = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getFitGraphicsToScreenWhenWindowIsResizedNewScale();
//    }
//    else
//    {
//      rectangle = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getSelectedComponentRegion();
//      // rectangle = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getOpticalCameraRectangleInPixel();
//      if (rectangle == null)
//      {
//        rectangle = new Rectangle(0, 0, 0, 0);
//      }
//    }
//
//    java.util.List<PanelRectangle> allowableAlignmentRegionInNanoMeters = new ArrayList<PanelRectangle>();
//    for (ReconstructionRegion alignmentRegion : _project.getTestProgram().getAlignmentRegions())
//    {
//      allowableAlignmentRegionInNanoMeters.add(alignmentRegion.getAllowableAlignmentRegionInNanoMeters());
//    }
//
//    Rectangle region1 = allowableAlignmentRegionInNanoMeters.get(0).getRectangle2D().getBounds();
//    Rectangle region2 = allowableAlignmentRegionInNanoMeters.get(1).getRectangle2D().getBounds();
//    Rectangle region3 = allowableAlignmentRegionInNanoMeters.get(2).getRectangle2D().getBounds();
//    Rectangle region4 = allowableAlignmentRegionInNanoMeters.get(3).getRectangle2D().getBounds();
//    Rectangle region5 = allowableAlignmentRegionInNanoMeters.get(4).getRectangle2D().getBounds();
//    Rectangle region6 = allowableAlignmentRegionInNanoMeters.get(5).getRectangle2D().getBounds();
//
//    Rectangle alignmentGroup1Region = null;
//    Rectangle alignmentGroup2Region = null;
//    Rectangle alignmentGroup3Region = null;
//    Rectangle alignmentGroup4Region = null;
//    Rectangle alignmentGroup5Region = null;
//    Rectangle alignmentGroup6Region = null;
//
//    Rectangle zoomAlignmentGroup1Region = null;
//    Rectangle zoomAlignmentGroup2Region = null;
//    Rectangle zoomAlignmentGroup3Region = null;
//    Rectangle zoomAlignmentGroup4Region = null;
//    Rectangle zoomAlignmentGroup5Region = null;
//    Rectangle zoomAlignmentGroup6Region = null;
//  
//    // add some offset to alignment region so that it is fit to at least one optical region
//    if (region1 != null)
//    {
//      alignmentGroup1Region = createAllowableRectangleForPsp(region1);
//    }
//
//    if (region2 != null)
//    {
//      alignmentGroup2Region = createAllowableRectangleForPsp(region2);
//    }
//
//    if (region3 != null)
//    {
//      alignmentGroup3Region = createAllowableRectangleForPsp(region3);
//    }
//    
//    if (region4 != null)
//    {
//      alignmentGroup4Region = createAllowableRectangleForPsp(region4);
//    }
//    
//    if (region5 != null)
//    {
//      alignmentGroup5Region = createAllowableRectangleForPsp(region5);
//    }
//    
//    if (region6 != null)
//    {
//      alignmentGroup6Region = createAllowableRectangleForPsp(region6);
//    }
//
//    if (MathUtil.fuzzyEquals(_testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getCurrentZoomFactor(), 1.0) == false)
//    {
//      zoomFactor = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getCurrentZoomFactor();
//
//      if (alignmentGroup1Region != null)
//      {
//        zoomAlignmentGroup1Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup1Region);
//        alignmentGroup1Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup1Region);
//        alignmentGroup1Region.setBounds((int)alignmentGroup1Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup1Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup1Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup1Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      if (alignmentGroup2Region != null)
//      {
//        zoomAlignmentGroup2Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup2Region);
//        alignmentGroup2Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup2Region);
//        alignmentGroup2Region.setBounds((int)alignmentGroup2Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup2Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup2Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup2Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      if (alignmentGroup3Region != null)
//      {
//        zoomAlignmentGroup3Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup3Region);
//        alignmentGroup3Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup3Region);
//        alignmentGroup3Region.setBounds((int)alignmentGroup3Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup3Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup3Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup3Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      if (alignmentGroup4Region != null)
//      {
//        zoomAlignmentGroup4Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup4Region);
//        alignmentGroup4Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup4Region);
//        alignmentGroup4Region.setBounds((int)alignmentGroup4Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup4Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup4Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup4Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      if (alignmentGroup5Region != null)
//      {
//        zoomAlignmentGroup5Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup5Region);
//        alignmentGroup5Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup5Region);
//        alignmentGroup5Region.setBounds((int)alignmentGroup5Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup5Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup5Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup5Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      if (alignmentGroup6Region != null)
//      {
//        zoomAlignmentGroup6Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup6Region);
//        alignmentGroup6Region = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup6Region);
//        alignmentGroup6Region.setBounds((int)alignmentGroup6Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup6Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup6Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup6Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      _isZoom = true;
//    }
//    else
//    {
//      _isZoom = false;
//    }
//
//    int minX = (int) rectangle.getBounds2D().getMinX() + MathUtil.roundDoubleToInt(5 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    int minY = (int) rectangle.getBounds2D().getMinY() + MathUtil.roundDoubleToInt(5 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    int widthInPixels = (int) rectangle.getBounds2D().getWidth() - MathUtil.roundDoubleToInt(10 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    int heightInPixels = (int) rectangle.getBounds2D().getHeight() - MathUtil.roundDoubleToInt(10 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    PanelRectangle bottomSectionOfOpticalLongRectangleInPixel = _project.getPanel().getPanelSettings().getBottomSectionOfOpticalLongPanel();
//    boolean isOpticalLongPanel = _project.getPanel().getPanelSettings().isOpticalLongPanel();
//
//    Point2D coorInPixels = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().
//            convertFromRendererToPixelCoordinates((int) bottomSectionOfOpticalLongRectangleInPixel.getMinX(), (int) bottomSectionOfOpticalLongRectangleInPixel.getMaxY());
//
//    Point2D point = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertFromPixelToRendererCoordinates((int) 0, (int) 0);
//    int a = (int) (bottomSectionOfOpticalLongRectangleInPixel.getMaxX() + point.getX());
//    int b = (int) (bottomSectionOfOpticalLongRectangleInPixel.getMinX() + point.getY());
//
//    Point2D point2 = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(a, b);
//
//    int opticalLongRectangleWidthInPixel = (int) (point2.getX() + point2.getY());
//
//    int a2 = (int) (point.getX() - bottomSectionOfOpticalLongRectangleInPixel.getMaxY());
//    int b2 = (int) (point.getY() - bottomSectionOfOpticalLongRectangleInPixel.getMinY());
//
//    Point2D point3 = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(a2, b2);
//
//    int opticalLongRectangleHeightInPixel = (int) (-point3.getX() - point3.getY());
//
//    Rectangle bottomSectionOfOpticalLongRectangleInPixelInRectangle = new Rectangle((int) coorInPixels.getX(), (int) coorInPixels.getY(), opticalLongRectangleWidthInPixel, opticalLongRectangleHeightInPixel);
//    bottomSectionOfOpticalLongRectangleInPixel.setRect(coorInPixels.getX(), coorInPixels.getY(), opticalLongRectangleWidthInPixel, opticalLongRectangleHeightInPixel);
//    boolean isFirstDrag = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().isOpticalRegionFirstDrag();
//
//      // Get the panel bounding rectangle in pixel
//    PanelRectangle panelRectangle = null;  
//    panelRectangle = _project.getPanel().getPanelSettings().getRightSectionOfLongPanel();
//    panelRectangle.add(_project.getPanel().getPanelSettings().getLeftSectionOfLongPanel().getRectangle2D());
//   
//    Point2D panelRectangleCoordinateInPixels = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates((int) panelRectangle.getMinX(), (int) panelRectangle.getMaxY());
//    Point2D panelRectanglePoint = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertFromPixelToRendererCoordinates((int) 0, (int) 0);
//    int pointA = (int) (panelRectangle.getMaxX() + panelRectanglePoint.getX());
//    int pointB = (int) (panelRectangle.getMinX() + panelRectanglePoint.getY());
//
//    Point2D panelRectanglePoint2 = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(pointA, pointB);
//    int panelRectangleWidthInPixel = (int) (panelRectanglePoint2.getX() + panelRectanglePoint2.getY());
//
//    int pointA2 = (int) (panelRectanglePoint.getX() - panelRectangle.getMaxY());
//    int pointB2 = (int) (panelRectanglePoint.getY() - panelRectangle.getMinY());
//
//    Point2D panelRectanglePoint3 = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(pointA2, pointB2);
//    int panelRectangleHeightInPixel = (int) (-panelRectanglePoint3.getX() - panelRectanglePoint3.getY());
//
//    Rectangle panelRectangleInPixel = new Rectangle();
//    panelRectangleInPixel.setRect(panelRectangleCoordinateInPixels.getX(), panelRectangleCoordinateInPixels.getY(), panelRectangleWidthInPixel, panelRectangleHeightInPixel);
//
//    _opticalRegionMap = new LinkedHashMap<Rectangle, String>();
//    Map<Rectangle, BooleanRef> opticalRegionMap = new LinkedHashMap<Rectangle, BooleanRef>();
//  
//    opticalRegionMap = OpticalRegionGeneration.generateCameraFovPanelRectangles(panelRectangleInPixel, bottomSectionOfOpticalLongRectangleInPixelInRectangle, isOpticalLongPanel, (int)minX, (int)minY, widthInPixels, heightInPixels, fitGraphicsToScreenWhenWindowIsResizedNewScale, zoomFactor, isFirstDrag);
//    for (Map.Entry<Rectangle, BooleanRef> mapEntry : opticalRegionMap.entrySet())
//    {
//      Rectangle dividedRectangle = mapEntry.getKey();
//      BooleanRef booleanRef = mapEntry.getValue();
//
//      Rectangle newDividedRectangle = new Rectangle();
//      newDividedRectangle = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(dividedRectangle);
//
//      if (_topGroup1PspCheckBox.isSelected() && _alignmentGroup1TopRadioButton.isSelected())
//      {
//        if (alignmentGroup1Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup1Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//
//      if (_topGroup2PspCheckBox.isSelected() && _alignmentGroup2TopRadioButton.isSelected())
//      {
//        if (alignmentGroup2Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup2Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//
//      if (_topGroup3PspCheckBox.isSelected() && _alignmentGroup3TopRadioButton.isSelected())
//      {
//        if (alignmentGroup3Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup3Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//      
//      if (_bottomGroup1PspCheckBox.isSelected() && _alignmentGroup1BottomRadioButton.isSelected())
//      {
//        if (alignmentGroup4Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup4Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//      
//      if (_bottomGroup2PspCheckBox.isSelected() && _alignmentGroup2BottomRadioButton.isSelected())
//      {
//        if (alignmentGroup5Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup5Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//      
//      if (_bottomGroup3PspCheckBox.isSelected() && _alignmentGroup3BottomRadioButton.isSelected())
//      {
//        if (alignmentGroup6Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup6Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//
//    }
//
//    _testDev.getPanelGraphicsEngineSetup().setAlignmentDividedOpticalRegionList(_opticalRegionMap);
//
//    if (_opticalRegionMap.size() > 0)
//    {
//      _isNotEmptySpace = true;
//    }
//    _testDev.getPanelGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
//    _testDev.getPanelGraphicsEngineSetup().setDragOpticalRegionLayersVisible(true);
  }

  /**
   *
   * @author Jack Hwee
   */
  public AffineTransform getCurrentAffineTransformForOpticalRegion()
  {
    _testDev = _mainUI.getTestDev();

    if (_project.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      return _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getCurrentAffineTransformForOpticalRegion();
    }
    else
    {
      return _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getCurrentAffineTransformForOpticalRegion();
    }
  }

  /**
   * @author Jack Hwee
   * @edited By Kee Chin Seong - the alignment is checked the alignment is COMPLETE.
   */
  private void pspCheckBox_actionPerformed(JCheckBox pspCheckBox, AlignmentGroup alignmentGroup)
  {
	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    /*if (_project.getPanel().getPanelSettings().isManualAlignmentCompleted()== false)
//    {
//
//      // they have not done manual alignment. so tell them they can no start the wizard.
//      String message = "An alignment must be performed before Surface Mapping can be generated. \n"
//              + "Go to Alignment Setup to perform the alignment";
//
//      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
//              message,
//              StringLocalizer.keyToString("MMGUI_ALIGN_RUN_KEY"),
//              JOptionPane.ERROR_MESSAGE);
//
//      pspCheckBox.setSelected(false);
//      return;
//    }*/
//    if (pspCheckBox.isSelected())
//    {
//      //_isSurfaceMapMode = true;
//      //updateCadGraphics();    
//      if (_currentAlignmentGroup.getName().equals(alignmentGroup.getName()))
//      {
//        _isSurfaceMapMode = true;
//        _testDev.changeToSurfaceMappingToolBar(true);  
//        _runSurfaceMapAlignmentButton.setEnabled(true);
//        _deleteSurfaceMapAlignmentButton.setEnabled(true);
//      }   
//      _deleteAlignmentPointButton.setEnabled(false);
//    
//      if (_alignmentGroupNameToRegionPositionNameMap.containsKey(alignmentGroup.getName()) == false)
//      {
//        _runAlignmentButton.setEnabled(false);
//        _clearAlignmentButton.setEnabled(false);
//        _viewAlignmentButton.setEnabled(false);
//        _autoAlignmentButton.setEnabled(false);
//        _alignmentGuideButton.setEnabled(false);
//        _runAlignmentWithDifferentSliceButton.setEnabled(false);
//      }
//
//      if (_isPanelBasedAlignment == false)
//      {
//        Board board = _project.getPanel().getBoard(alignmentGroup.getPads().get(0).getComponent().getBoard().getName());
//        if (board.getBoardAlignmentSurfaceMapSettings().isAlignmentOpticalRegionExists(alignmentGroup.getName()))
//        {
//          OpticalRegion currentAlignmentOpticalRegion =   board.getBoardAlignmentSurfaceMapSettings().getAlignmentOpticalRegion(alignmentGroup.getName());
//          currentAlignmentOpticalRegion.setToUsePspForAlignment(true);
//        }
//      }
//      else
//      {
//        if (_project.getPanel().getPanelAlignmentSurfaceMapSettings().isAlignmentOpticalRegionExists(alignmentGroup.getName()))
//        {
//          OpticalRegion currentAlignmentOpticalRegion = _project.getPanel().getPanelAlignmentSurfaceMapSettings().getAlignmentOpticalRegion(alignmentGroup.getName());
//          currentAlignmentOpticalRegion.setToUsePspForAlignment(true);
//        }
//      }
//    }
//    else
//    {
//      //_isSurfaceMapMode = false;
//      //updateCadGraphics();
//      _testDev.getPanelGraphicsEngineSetup().setDragOpticalRegionLayersVisible(false);
//      if (_currentAlignmentGroup.getName().equals(alignmentGroup.getName()))
//      {
//        _isSurfaceMapMode = false;
//        _testDev.changeToSurfaceMappingToolBar(false);     
//        _runSurfaceMapAlignmentButton.setEnabled(false);
//        _deleteSurfaceMapAlignmentButton.setEnabled(false);
//      }   
//      _deleteAlignmentPointButton.setEnabled(true);
//      _runAlignmentButton.setEnabled(true);
//      _clearAlignmentButton.setEnabled(true);
//      _viewAlignmentButton.setEnabled(true);
//      _autoAlignmentButton.setEnabled(true);
//      _alignmentGuideButton.setEnabled(true);
//      _runAlignmentWithDifferentSliceButton.setEnabled(true);
//
//      if (_isPanelBasedAlignment == false)
//      {
//        _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setDragRegionMode(false);
//        //Board board = _project.getPanel().getBoard(alignmentGroup.getPads().get(0).getComponent().getBoard().getName());
//        for (Board board:  _project.getPanel().getBoards())
//        {
//          if (board.getBoardAlignmentSurfaceMapSettings().isAlignmentOpticalRegionExists(alignmentGroup.getName()))
//          {
//            OpticalRegion currentAlignmentOpticalRegion =   board.getBoardAlignmentSurfaceMapSettings().getAlignmentOpticalRegion(alignmentGroup.getName());     
//            currentAlignmentOpticalRegion.setToUsePspForAlignment(false);
//          }
//        }
//      }
//      else
//      {
//        if (_project.getPanel().getPanelAlignmentSurfaceMapSettings().isAlignmentOpticalRegionExists(alignmentGroup.getName()))
//        {
//          OpticalRegion currentAlignmentOpticalRegion = _project.getPanel().getPanelAlignmentSurfaceMapSettings().getAlignmentOpticalRegion(alignmentGroup.getName());     
//          currentAlignmentOpticalRegion.setToUsePspForAlignment(false);
//        }
//      }
//    }
  }

  /**
   * @author Jack Hwee
   */
  private void populateSurfaceMapAlignmentIntoMap()
  {
    _alignmentGroupNameToRegionPositionNameMap.clear();

    if (_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
    {
      for (Map.Entry<String, String> mapEntry : _project.getPanel().getBoards().get(0).getBoardAlignmentSurfaceMapSettings().getAlignmentGroupNameToRegionPositionNameMap().entrySet())
      {
        String alignmentGroupName = mapEntry.getKey();
        String regionPositionName = mapEntry.getValue();
      
        _alignmentGroupNameToRegionPositionNameMap.put(alignmentGroupName, regionPositionName);
      }
    }
    else
    {
      for (Map.Entry<String, String> mapEntry : _project.getPanel().getPanelAlignmentSurfaceMapSettings().getAlignmentGroupNameToRegionPositionNameMap().entrySet())
      {
        String alignmentGroupName = mapEntry.getKey();
        String regionPositionName = mapEntry.getValue();

        _alignmentGroupNameToRegionPositionNameMap.put(alignmentGroupName, regionPositionName);
      }
    }

  }

  /**
   * @author Jack Hwee
   * @edited By Kee Chin Seong - the alignment is checked the alignment is COMPLETE.
   */
  private void updateUsePspCheckBox()
  {
	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
////    if (_isLongPanel)
////    {
////      _topGroup1PspCheckBox.setSelected(false);
////      _topGroup2PspCheckBox.setSelected(false);
////      _topGroup3PspCheckBox.setSelected(false);
////      _bottomGroup1PspCheckBox.setSelected(false);
////      _bottomGroup2PspCheckBox.setSelected(false);
////      _bottomGroup3PspCheckBox.setSelected(false);
////    }
////    else
////    {
////      _group1PspCheckBox.setSelected(false);
////      _group2PspCheckBox.setSelected(false);
////      _group3PspCheckBox.setSelected(false);
////    }
//    if (_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
//    {
//      for (Board board: _project.getPanel().getBoards())
//      {
//        //for (Map.Entry<String, String> mapEntry : board.getBoardAlignmentSurfaceMapSettings().getAlignmentGroupNameToRegionPositionNameMap().entrySet())
//        for(AlignmentGroup alignmentGroup : board.getBoardSettings().getAllAlignmentGroups())
//        {
//          //String alignmentGroupName = mapEntry.getKey();
//          //String regionPositionName = mapEntry.getValue();
//          String alignmentGroupName = alignmentGroup.getName();
//          OpticalRegion opticalRegion = null;       
//          boolean alignmentGroupContainsPsp = false;
//   
//          //if (_project.getPanel().getPanelSettings().isManualAlignmentCompleted()== false)
//          //  break;
//
//          if (board.getBoardAlignmentSurfaceMapSettings().isAlignmentOpticalRegionExists(alignmentGroupName))
//          {
//            opticalRegion = board.getBoardAlignmentSurfaceMapSettings().getAlignmentOpticalRegion(alignmentGroupName);
//            alignmentGroupContainsPsp = true;
//          }
//
//          if (_isLongPanel)
//          {
//            if (_alignmentGroup1Top.getName().equals(alignmentGroupName))
//            {
//              if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//                _topGroup1PspCheckBox.setSelected(true);
//              else
//                _topGroup1PspCheckBox.setSelected(false);
//
//              pspCheckBox_actionPerformed(_topGroup1PspCheckBox, _alignmentGroup1Top);
//            }
//            if (_alignmentGroup2Top.getName().equals(alignmentGroupName))
//            {
//              if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//                _topGroup2PspCheckBox.setSelected(true);
//              else
//                _topGroup2PspCheckBox.setSelected(false);
//              pspCheckBox_actionPerformed(_topGroup2PspCheckBox, _alignmentGroup2Top);
//            }
//            if (_alignmentGroup3Top.getName().equals(alignmentGroupName))
//            {
//              if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//                _topGroup3PspCheckBox.setSelected(true);
//              else
//                _topGroup3PspCheckBox.setSelected(false);
//              pspCheckBox_actionPerformed(_topGroup3PspCheckBox, _alignmentGroup3Top);
//            }
//            if (_alignmentGroup1Bottom.getName().equals(alignmentGroupName))
//            {
//              if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//                _bottomGroup1PspCheckBox.setSelected(true);
//              else
//                _bottomGroup1PspCheckBox.setSelected(false);
//              pspCheckBox_actionPerformed(_bottomGroup1PspCheckBox, _alignmentGroup1Bottom);
//            }
//            if (_alignmentGroup2Bottom.getName().equals(alignmentGroupName))
//            {
//              if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//                _bottomGroup2PspCheckBox.setSelected(true);
//              else
//                _bottomGroup2PspCheckBox.setSelected(false);
//              pspCheckBox_actionPerformed(_bottomGroup2PspCheckBox, _alignmentGroup2Bottom);
//            }
//            if (_alignmentGroup3Bottom.getName().equals(alignmentGroupName))
//            {
//              if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//                _bottomGroup3PspCheckBox.setSelected(true);
//              else
//                _bottomGroup3PspCheckBox.setSelected(false);
//              pspCheckBox_actionPerformed(_bottomGroup3PspCheckBox, _alignmentGroup3Bottom);
//            }
//          }
//          else
//          {
//            if (_alignmentGroup1.getName().equals(alignmentGroupName))
//            {
//              if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//                _group1PspCheckBox.setSelected(true);
//              else
//                _group1PspCheckBox.setSelected(false);
//              pspCheckBox_actionPerformed(_group1PspCheckBox, _alignmentGroup1);
//            }
//
//            if (_alignmentGroup2.getName().equals(alignmentGroupName))
//            {
//              if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//                _group2PspCheckBox.setSelected(true);
//              else
//                _group2PspCheckBox.setSelected(false);
//              pspCheckBox_actionPerformed(_group2PspCheckBox, _alignmentGroup2);
//            }
//
//            if (_alignmentGroup3.getName().equals(alignmentGroupName))
//            {
//              if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//                _group3PspCheckBox.setSelected(true);
//              else
//                _group3PspCheckBox.setSelected(false);
//              pspCheckBox_actionPerformed(_group3PspCheckBox, _alignmentGroup3);
//            }
//          }
//        }
//      }
//    }
//    else
//    {
//      for(AlignmentGroup alignmentGroup : _project.getPanel().getPanelSettings().getAllAlignmentGroups())
//      //for (Map.Entry<String, String> mapEntry : _project.getPanel().getPanelAlignmentSurfaceMapSettings().getAlignmentGroupNameToRegionPositionNameMap().entrySet())
//      {
//        //String alignmentGroupName = mapEntry.getKey();
//        //String regionPositionName = mapEntry.getValue();
//        String alignmentGroupName = alignmentGroup.getName();
//        OpticalRegion opticalRegion = null;
//        boolean alignmentGroupContainsPsp = false;
//        
//        //if (_project.getPanel().getPanelSettings().isManualAlignmentCompleted()== false)
//        //  break;
//         
//        if(_project.getPanel().getPanelAlignmentSurfaceMapSettings().isAlignmentOpticalRegionExists(alignmentGroupName))
//        {
//          opticalRegion =  _project.getPanel().getPanelAlignmentSurfaceMapSettings().getAlignmentOpticalRegion(alignmentGroupName);
//          alignmentGroupContainsPsp = true;
//        }
//
//        if (_isLongPanel)
//        {
//          if (_alignmentGroup1Top.getName().equals(alignmentGroupName))
//          {
//            if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//              _topGroup1PspCheckBox.setSelected(true);
//            else
//              _topGroup1PspCheckBox.setSelected(false);
//            pspCheckBox_actionPerformed(_topGroup1PspCheckBox, _alignmentGroup1Top);
//          }
//          if (_alignmentGroup2Top.getName().equals(alignmentGroupName))
//          {
//            if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//              _topGroup2PspCheckBox.setSelected(true);
//            else
//              _topGroup2PspCheckBox.setSelected(false);
//            pspCheckBox_actionPerformed(_topGroup2PspCheckBox, _alignmentGroup2Top);
//          }
//          if (_alignmentGroup3Top.getName().equals(alignmentGroupName))
//          {
//            if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//              _topGroup3PspCheckBox.setSelected(true);
//            else
//              _topGroup3PspCheckBox.setSelected(false);
//            pspCheckBox_actionPerformed(_topGroup3PspCheckBox, _alignmentGroup3Top);
//          }
//          if (_alignmentGroup1Bottom.getName().equals(alignmentGroupName))
//          {
//            if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//              _bottomGroup1PspCheckBox.setSelected(true);
//            else
//              _bottomGroup1PspCheckBox.setSelected(false);
//            pspCheckBox_actionPerformed(_bottomGroup1PspCheckBox, _alignmentGroup1Bottom);
//          }
//          if (_alignmentGroup2Bottom.getName().equals(alignmentGroupName))
//          {
//            if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//              _bottomGroup2PspCheckBox.setSelected(true);
//            else
//              _bottomGroup2PspCheckBox.setSelected(false);
//            pspCheckBox_actionPerformed(_bottomGroup2PspCheckBox, _alignmentGroup2Bottom);
//          }
//          if (_alignmentGroup3Bottom.getName().equals(alignmentGroupName))
//          {
//            if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//              _bottomGroup3PspCheckBox.setSelected(true);
//            else
//              _bottomGroup3PspCheckBox.setSelected(false);
//            pspCheckBox_actionPerformed(_bottomGroup3PspCheckBox, _alignmentGroup3Bottom);
//          }         
//        }
//        else
//        {
//          if (_alignmentGroup1.getName().equals(alignmentGroupName))
//          {
//            if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//              _group1PspCheckBox.setSelected(true);
//            else
//              _group1PspCheckBox.setSelected(false);
//            pspCheckBox_actionPerformed(_group1PspCheckBox, _alignmentGroup1);
//          }
//          if (_alignmentGroup2.getName().equals(alignmentGroupName))
//          {
//            if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//              _group2PspCheckBox.setSelected(true);
//            else
//              _group2PspCheckBox.setSelected(false);
//            pspCheckBox_actionPerformed(_group2PspCheckBox, _alignmentGroup2);
//          }
//          if (_alignmentGroup3.getName().equals(alignmentGroupName))
//          {
//            if (alignmentGroupContainsPsp && opticalRegion.isSetToUsePspForAlignment())
//              _group3PspCheckBox.setSelected(true);
//            else
//              _group3PspCheckBox.setSelected(false);
//            pspCheckBox_actionPerformed(_group3PspCheckBox, _alignmentGroup3);
//          }
//        }
//      }
//    }
  }

  /**
   * @author Jack Hwee
   */
  private void drawSelectedAlignmentOpticalRegion(String alignmentGroup)
  {
    // Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    Assert.expect(_alignmentGroupNameToRegionPositionNameMap != null);
//    Assert.expect(alignmentGroup != null);
//
//    if (_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
//    {
//      java.util.List<Rectangle> convertedRectangleInNanometersList = new ArrayList<Rectangle>();
//      _testDev.getBoardGraphicsEngineSetup().removeSelectedOpticalRectangleLayer();
//      for (Map.Entry<String, String> mapEntry : _alignmentGroupNameToRegionPositionNameMap.entrySet())
//      {
//        String alignmentGroupName = mapEntry.getKey();
//        String regionPositionName = mapEntry.getValue();
//        //OpticalRegion opticalRegion = _currentAlignmentGroup.getPads().get(0).getComponent().getBoard().getBoardAlignmentSurfaceMapSettings().getAlignmentOpticalRegion(alignmentGroupName);
//        OpticalRegion opticalRegion = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard().getBoardAlignmentSurfaceMapSettings().getAlignmentOpticalRegion(alignmentGroupName);;
//        for(OpticalCameraRectangle opticalCameraRectangle: opticalRegion.getOpticalCameraRectangles(_testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard()))
//        {
//          if (alignmentGroupName.equals(alignmentGroup))
//          {
//            if (opticalCameraRectangle.getInspectableBool() == true)
//            {
//              Rectangle rect = opticalCameraRectangle.getRegion().getRectangle2D().getBounds();
//              convertedRectangleInNanometersList.add(rect);
//            }
//          }
//        }
//        _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
//        _testDev.getBoardGraphicsEngineSetup().handleSelectedAlignmentOpticalRegion(convertedRectangleInNanometersList);
//      }
//      convertedRectangleInNanometersList.clear();
//    }
//    else
//    {
//      _testDev.getPanelGraphicsEngineSetup().removeSelectedOpticalRectangleLayer();
//      java.util.List<Rectangle> convertedRectangleInNanometersList = new ArrayList<Rectangle>();
//      for (Map.Entry<String, String> mapEntry : _alignmentGroupNameToRegionPositionNameMap.entrySet())
//      {
//        String alignmentGroupName = mapEntry.getKey();
//        String regionPositionName = mapEntry.getValue();
//        OpticalRegion opticalRegion = _project.getPanel().getPanelAlignmentSurfaceMapSettings().getAlignmentOpticalRegion(alignmentGroupName);
//       
//        for(OpticalCameraRectangle opticalCameraRectangle: opticalRegion.getAllOpticalCameraRectangles())
//        {
//          if (alignmentGroupName.equals(alignmentGroup))
//          {
//            if (opticalCameraRectangle.getInspectableBool() == true)
//            {
//              Rectangle rect = opticalCameraRectangle.getRegion().getRectangle2D().getBounds();
//
//              convertedRectangleInNanometersList.add(rect);
//            }
//            _testDev.getPanelGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
//            _testDev.getPanelGraphicsEngineSetup().handleSelectedAlignmentOpticalRegion(convertedRectangleInNanometersList);
//          }
//        }
//      }
//      convertedRectangleInNanometersList.clear();
//    }
  }
  
   /**
   * @author Jack Hwee
   */
  private void updateUsePspCheckBoxEnableStatus()
  {
	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    populateSurfaceMapAlignmentIntoMap();
//    drawSelectedAlignmentOpticalRegion(_currentAlignmentGroup.getName());
//
//    if(_isLongPanel)
//    {
//      if (_alignmentGroup1TopList.getVisibleRowCount() > 0 && (_alignmentGroup1TopRadioButton.isSelected()))
//      {
//        _topGroup1PspCheckBox.setEnabled(true);
//        _topGroup2PspCheckBox.setEnabled(false);
//        _topGroup3PspCheckBox.setEnabled(false);
//        _bottomGroup1PspCheckBox.setEnabled(false);
//        _bottomGroup2PspCheckBox.setEnabled(false);
//        _bottomGroup3PspCheckBox.setEnabled(false);
//      }
//      else
//      {
//        _topGroup1PspCheckBox.setEnabled(false);
//      }
//      
//      if (_alignmentGroup2TopList.getVisibleRowCount() > 0 && (_alignmentGroup2TopRadioButton.isSelected()))
//      {
//        _topGroup1PspCheckBox.setEnabled(false);
//        _topGroup2PspCheckBox.setEnabled(true);
//        _topGroup3PspCheckBox.setEnabled(false);
//        _bottomGroup1PspCheckBox.setEnabled(false);
//        _bottomGroup2PspCheckBox.setEnabled(false);
//        _bottomGroup3PspCheckBox.setEnabled(false);
//      }
//      else
//      {
//        _topGroup2PspCheckBox.setEnabled(false);
//      }
//      
//      if (_alignmentGroup3TopList.getVisibleRowCount() > 0 && (_alignmentGroup3TopRadioButton.isSelected()))
//      {
//        _topGroup1PspCheckBox.setEnabled(false);
//        _topGroup2PspCheckBox.setEnabled(false);
//        _topGroup3PspCheckBox.setEnabled(true);
//        _bottomGroup1PspCheckBox.setEnabled(false);
//        _bottomGroup2PspCheckBox.setEnabled(false);
//        _bottomGroup3PspCheckBox.setEnabled(false);
//      }
//      else
//      {
//        _topGroup3PspCheckBox.setEnabled(false);
//      }
//      
//      if (_alignmentGroup1BottomList.getVisibleRowCount() > 0 && (_alignmentGroup1BottomRadioButton.isSelected()))
//      {
//        _topGroup1PspCheckBox.setEnabled(false);
//        _topGroup2PspCheckBox.setEnabled(false);
//        _topGroup3PspCheckBox.setEnabled(false);
//        _bottomGroup1PspCheckBox.setEnabled(true);
//        _bottomGroup2PspCheckBox.setEnabled(false);
//        _bottomGroup3PspCheckBox.setEnabled(false);
//      }
//      else
//      {
//        _bottomGroup1PspCheckBox.setEnabled(false);
//      }
//      
//      if (_alignmentGroup2BottomList.getVisibleRowCount() > 0 && (_alignmentGroup2BottomRadioButton.isSelected()))
//      {
//        _topGroup1PspCheckBox.setEnabled(false);
//        _topGroup2PspCheckBox.setEnabled(false);
//        _topGroup3PspCheckBox.setEnabled(false);
//        _bottomGroup1PspCheckBox.setEnabled(false);
//        _bottomGroup2PspCheckBox.setEnabled(true);
//        _bottomGroup3PspCheckBox.setEnabled(false);
//      }
//      else
//      {
//        _bottomGroup2PspCheckBox.setEnabled(false);
//      }
//      
//      if (_alignmentGroup3BottomList.getVisibleRowCount() > 0 && (_alignmentGroup3BottomRadioButton.isSelected()))
//      {
//        _topGroup1PspCheckBox.setEnabled(false);
//        _topGroup2PspCheckBox.setEnabled(false);
//        _topGroup3PspCheckBox.setEnabled(false);
//        _bottomGroup1PspCheckBox.setEnabled(false);
//        _bottomGroup2PspCheckBox.setEnabled(false);
//        _bottomGroup3PspCheckBox.setEnabled(true);
//      }
//      else
//      {
//        _bottomGroup3PspCheckBox.setEnabled(false);
//      }
//      updateUsePspCheckBox();
////      if (_topGroup1PspCheckBox.isSelected() || _topGroup2PspCheckBox.isSelected() || _topGroup3PspCheckBox.isSelected()
////            ||_bottomGroup1PspCheckBox.isSelected() || _bottomGroup2PspCheckBox.isSelected() || _bottomGroup3PspCheckBox.isSelected())
////      {
////        _isSurfaceMapMode = true;
////        _testDev.changeToSurfaceMappingToolBar(true);
////      }
////      else
////      {
////        _isSurfaceMapMode = false;
////        _testDev.changeToSurfaceMappingToolBar(false);
////      }      
//    }
//    else
//    {
//      if (_alignmentGroup1List.getVisibleRowCount() > 0 && (_alignmentGroup1RadioButton.isSelected()))
//      {
//        _group1PspCheckBox.setEnabled(true);
//        _group2PspCheckBox.setEnabled(false);
//        _group3PspCheckBox.setEnabled(false);
//      }
//      else
//      {
//        _group1PspCheckBox.setEnabled(false);
//      }
//
//      if (_alignmentGroup2List.getVisibleRowCount() > 0 && (_alignmentGroup2RadioButton.isSelected()))
//      {
//        _group2PspCheckBox.setEnabled(true);
//        _group1PspCheckBox.setEnabled(false);
//        _group3PspCheckBox.setEnabled(false);
//      }
//      else
//      {
//        _group2PspCheckBox.setEnabled(false);
//      }
//
//      if (_alignmentGroup3List.getVisibleRowCount() > 0 && (_alignmentGroup3RadioButton.isSelected()))
//      {
//        _group1PspCheckBox.setEnabled(false);
//        _group2PspCheckBox.setEnabled(false);
//        _group3PspCheckBox.setEnabled(true);
//      }
//      else
//      {
//        _group3PspCheckBox.setEnabled(false);
//      }
//      updateUsePspCheckBox();
////      if (_group1PspCheckBox.isSelected() || _group2PspCheckBox.isSelected() || _group3PspCheckBox.isSelected())
////      {
////        _isSurfaceMapMode = true;
////        _testDev.changeToSurfaceMappingToolBar(true);
////      }
////      else
////      {
////        _isSurfaceMapMode = false;
////        _testDev.changeToSurfaceMappingToolBar(false);
////      }      
//    }  
//    _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().setCurrentWindowSize();   
  }
  
    /**
   * Wait for Single Image and Height Map calculation before proceed
   * @author Jack Hwee
   */
  private void waitForHeightMap()
  {
     AbstractOpticalCamera abstractOpticalCamera;
     if (_surfaceMapping.getPspModuleEnum().equals(PspModuleEnum.FRONT))
      abstractOpticalCamera = OpticalCameraArray.getCamera(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
    else
      abstractOpticalCamera = OpticalCameraArray.getCamera(OpticalCameraIdEnum.OPTICAL_CAMERA_2);
     
    abstractOpticalCamera.setHeightMapValueAvailable(false);
    do
    {
      try
      {
        Thread.sleep(100);
      }
      catch (InterruptedException ix)
      {
        // don't expect this, but don't really care how sleep returns either
      }
    }
    while (abstractOpticalCamera.isHeightMapValueAvailable() == false);
  }
  
    /**
   * @author Jack Hwee
   */
  private void deleteRegionButton_actionPerformed(final boolean isRunSurfaceMapAlignmentButton_actionPerformed)
  {
    // Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    boolean isMultiBoardContainsOpticalCameraRectangle = false;
//    OpticalRegion currentAlignmentOpticalRegion = null;
//        
//    if (_isPanelBasedAlignment == false)
//    {
//      //Board selectedBoard = _project.getPanel().getBoard(_currentAlignmentGroup.getPads().get(0).getComponent().getBoard().getName());
//      Board selectedBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
//      if (selectedBoard.getBoardAlignmentSurfaceMapSettings().isAlignmentOpticalRegionExists(_currentAlignmentGroup.getName()))
//      {
//        currentAlignmentOpticalRegion = selectedBoard.getBoardAlignmentSurfaceMapSettings().getAlignmentOpticalRegion(_currentAlignmentGroup.getName());
//        for (Board board: _project.getPanel().getBoards())
//        {
//          if (currentAlignmentOpticalRegion.getOpticalCameraRectangles(board).size() > 0)
//          {
//            isMultiBoardContainsOpticalCameraRectangle = true;
//            break;
//          }       
//        }
//      }
//    }
//    else
//    {
//      if (_project.getPanel().getPanelAlignmentSurfaceMapSettings().isAlignmentOpticalRegionExists(_currentAlignmentGroup.getName()))
//        currentAlignmentOpticalRegion = _project.getPanel().getPanelAlignmentSurfaceMapSettings().getAlignmentOpticalRegion(_currentAlignmentGroup.getName());
//    }
//    if (currentAlignmentOpticalRegion != null && (currentAlignmentOpticalRegion.getAllOpticalCameraRectangles().size() > 0 || isMultiBoardContainsOpticalCameraRectangle))
//    {
//      final OpticalRegion finalAlignmentOpticalRegion = currentAlignmentOpticalRegion;
//      int answer;
//      if (isRunSurfaceMapAlignmentButton_actionPerformed)
//        answer = JOptionPane.YES_OPTION;
//      else
//        answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
//              StringLocalizer.keyToString("PSP_DELETE_REGION_WILL_RESET_POINTS_KEY"),
//              StringLocalizer.keyToString("PSP_DELETE_REGION_BUTTON_KEY"),
//              JOptionPane.YES_NO_OPTION,
//              JOptionPane.WARNING_MESSAGE);
//      if (answer == JOptionPane.YES_OPTION)
//      {
//        final BusyCancelDialog busyDialog = new BusyCancelDialog(
//                _mainUI,
//                StringLocalizer.keyToString("PSP_DELETE_OPTICAL_REGION_KEY"),
//                StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
//                true);
//        busyDialog.pack();
//        SwingUtils.centerOnComponent(busyDialog, _mainUI);
//        SwingWorkerThread.getInstance().invokeLater(new Runnable()
//        {
//          public void run()
//          {
//            try
//            {
//              _project.getTestProgram().removeOpticalRegion(finalAlignmentOpticalRegion);
//
//              if (_isPanelBasedAlignment)
//              {
//                _project.getPanel().getPanelAlignmentSurfaceMapSettings().removeOpticalRegion(_currentAlignmentGroup.getName(), finalAlignmentOpticalRegion);    
//                _alignmentGroupNameToRegionPositionNameMap.remove(_currentAlignmentGroup.getName());
//              }
//              else
//              {
//                String[] alignmentGroup = _currentAlignmentGroup.getName().split("_");
//                for (Board board : _project.getPanel().getBoards())
//                {
//                  for (OpticalRegion opticalRegion : board.getBoardAlignmentSurfaceMapSettings().getOpticalRegions())
//                  { 
//                    String[] alignmentGroupToDelete = opticalRegion.getAlignmentGroupName().split("_");
//                    if (alignmentGroupToDelete[1].equals(alignmentGroup[1]))
//                    {
//                       board.getBoardAlignmentSurfaceMapSettings().removeOpticalRegion(opticalRegion.getAlignmentGroupName(), opticalRegion);
//                      _alignmentGroupNameToRegionPositionNameMap.remove(opticalRegion.getAlignmentGroupName());
//                    }
//                  }
//                }
//              }
//            }
//            finally
//            {    
////              if (isRunSurfaceMapAlignmentButton_actionPerformed == false)
////              {
////                _runSurfaceMapAlignmentButton.setEnabled(false);
////                _deleteSurfaceMapAlignmentButton.setEnabled(false);
////                //_testDev.changeToSurfaceMappingToolBar(false);
////                updateUsePspCheckBox();
////              }
////              if (_isPanelBasedAlignment)
////              {
////                _testDev.getPanelGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
////                _testDev.getPanelGraphicsEngineSetup().removeSelectedOpticalRectangleLayer();
////                _testDev.getPanelGraphicsEngineSetup().setDragOpticalRegionLayersVisible(false);
////              }
////              else
////              {
////                _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
////                _testDev.getBoardGraphicsEngineSetup().removeSelectedOpticalRectangleLayer();
////                _testDev.getBoardGraphicsEngineSetup().setDragOpticalRegionLayersVisible(false);
////              }      
//              busyDialog.dispose();  
//            }
//          }
//        });
//        busyDialog.setVisible(true);
//        //_testDev.changeToSurfaceMappingToolBar(false);
//        //updateCadGraphics();
//      }
//      else
//      {
//        if (_currentAlignmentGroup.isEmpty())
//        {
//          try
//          {
//            _commandManager.undo();
//          }
//          catch (XrayTesterException ex)
//          {
//            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
//                    ex,
//                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
//                    true);
//          }
//        }
//        else
//          return;    
//      }
//    }
  }
  
     /**
   *
   * @author Jack Hwee
   */
  private void handleBoardSurfaceMapAlignmentRegionForLongPanel()
  {
	// Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
  	// This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//    double zoomFactor = 1;
//    double fitGraphicsToScreenWhenWindowIsResizedNewScale = 1;
//    _isNotEmptySpace = false;
//
//    Rectangle2D rectangle = null;
//
//    if (_testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().isWindowResized())
//    {
//      rectangle = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getResizedSelectedComponentRegion();
//      if (rectangle == null)
//      {
//        rectangle = new Rectangle(0, 0, 0, 0);
//      }
//
//      fitGraphicsToScreenWhenWindowIsResizedNewScale = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getFitGraphicsToScreenWhenWindowIsResizedNewScale();
//    }
//    else
//    {
//      rectangle = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getSelectedComponentRegion();
//      // rectangle = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getOpticalCameraRectangleInPixel();
//      if (rectangle == null)
//      {
//        rectangle = new Rectangle(0, 0, 0, 0);
//      }
//    }
//
//    Board currentDisplayBoard =  _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
//    java.util.List<PanelRectangle> allowableAlignmentRegionInNanoMeters = new ArrayList<PanelRectangle>();
//    for (ReconstructionRegion alignmentRegion : _project.getTestProgram().getAlignmentRegions())
//    {
//      if (alignmentRegion.getBoard().equals(currentDisplayBoard))
//        allowableAlignmentRegionInNanoMeters.add(alignmentRegion.getAllowableAlignmentRegionInNanoMeters());
//    }
//
//    Rectangle region1 = allowableAlignmentRegionInNanoMeters.get(0).getRectangle2D().getBounds();
//    Rectangle region2 = allowableAlignmentRegionInNanoMeters.get(1).getRectangle2D().getBounds();
//    Rectangle region3 = allowableAlignmentRegionInNanoMeters.get(2).getRectangle2D().getBounds();
//    Rectangle region4 = allowableAlignmentRegionInNanoMeters.get(3).getRectangle2D().getBounds();
//    Rectangle region5 = allowableAlignmentRegionInNanoMeters.get(4).getRectangle2D().getBounds();
//    Rectangle region6 = allowableAlignmentRegionInNanoMeters.get(5).getRectangle2D().getBounds();
//
//    Rectangle alignmentGroup1Region = null;
//    Rectangle alignmentGroup2Region = null;
//    Rectangle alignmentGroup3Region = null;
//    Rectangle alignmentGroup4Region = null;
//    Rectangle alignmentGroup5Region = null;
//    Rectangle alignmentGroup6Region = null;
//
//    Rectangle zoomAlignmentGroup1Region = null;
//    Rectangle zoomAlignmentGroup2Region = null;
//    Rectangle zoomAlignmentGroup3Region = null;
//    Rectangle zoomAlignmentGroup4Region = null;
//    Rectangle zoomAlignmentGroup5Region = null;
//    Rectangle zoomAlignmentGroup6Region = null;
//  
//    // add some offset to alignment region so that it is fit to at least one optical region
//    if (region1 != null)
//    {
//      alignmentGroup1Region = createAllowableRectangleForPsp(region1);
//    }
//
//    if (region2 != null)
//    {
//      alignmentGroup2Region = createAllowableRectangleForPsp(region2);
//    }
//
//    if (region3 != null)
//    {
//      alignmentGroup3Region = createAllowableRectangleForPsp(region3);
//    }
//    
//    if (region4 != null)
//    {
//      alignmentGroup4Region = createAllowableRectangleForPsp(region4);
//    }
//    
//    if (region5 != null)
//    {
//      alignmentGroup5Region = createAllowableRectangleForPsp(region5);
//    }
//    
//    if (region6 != null)
//    {
//      alignmentGroup6Region = createAllowableRectangleForPsp(region6);
//    }
//
//    if (MathUtil.fuzzyEquals(_testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getCurrentZoomFactor(), 1.0) == false)
//    {
//      zoomFactor = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getCurrentZoomFactor();
//
//      if (alignmentGroup1Region != null)
//      {
//        zoomAlignmentGroup1Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup1Region);
//        alignmentGroup1Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup1Region);
//        alignmentGroup1Region.setBounds((int)alignmentGroup1Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup1Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup1Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup1Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      if (alignmentGroup2Region != null)
//      {
//        zoomAlignmentGroup2Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup2Region);
//        alignmentGroup2Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup2Region);
//        alignmentGroup2Region.setBounds((int)alignmentGroup2Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup2Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup2Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup2Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      if (alignmentGroup3Region != null)
//      {
//        zoomAlignmentGroup3Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup3Region);
//        alignmentGroup3Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup3Region);
//        alignmentGroup3Region.setBounds((int)alignmentGroup3Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup3Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup3Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup3Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      if (alignmentGroup4Region != null)
//      {
//        zoomAlignmentGroup4Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup4Region);
//        alignmentGroup4Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup4Region);
//        alignmentGroup4Region.setBounds((int)alignmentGroup4Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup4Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup4Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup4Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      if (alignmentGroup5Region != null)
//      {
//        zoomAlignmentGroup5Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup5Region);
//        alignmentGroup5Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup5Region);
//        alignmentGroup5Region.setBounds((int)alignmentGroup5Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup5Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup5Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup5Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      if (alignmentGroup6Region != null)
//      {
//        zoomAlignmentGroup6Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(alignmentGroup6Region);
//        alignmentGroup6Region = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(zoomAlignmentGroup6Region);
//        alignmentGroup6Region.setBounds((int)alignmentGroup6Region.getX() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup6Region.getY() - PSP_RECTANGLE_OFFSET, (int)alignmentGroup6Region.getWidth() + (PSP_RECTANGLE_OFFSET*2), (int)alignmentGroup6Region.getHeight() + (PSP_RECTANGLE_OFFSET*2));
//      }
//      _isZoom = true;
//    }
//    else
//    {
//      _isZoom = false;
//    }
//
//    int minX = (int) rectangle.getBounds2D().getMinX() + MathUtil.roundDoubleToInt(5 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    int minY = (int) rectangle.getBounds2D().getMinY() + MathUtil.roundDoubleToInt(5 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    int widthInPixels = (int) rectangle.getBounds2D().getWidth() - MathUtil.roundDoubleToInt(10 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    int heightInPixels = (int) rectangle.getBounds2D().getHeight() - MathUtil.roundDoubleToInt(10 * fitGraphicsToScreenWhenWindowIsResizedNewScale);
//    PanelRectangle bottomSectionOfOpticalLongRectangleInPixel = _project.getPanel().getPanelSettings().getBottomSectionOfOpticalLongPanel();
//    boolean isOpticalLongPanel = _project.getPanel().getPanelSettings().isOpticalLongPanel();
//
//    Point2D coorInPixels = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().
//            convertFromRendererToPixelCoordinates((int) bottomSectionOfOpticalLongRectangleInPixel.getMinX(), (int) bottomSectionOfOpticalLongRectangleInPixel.getMaxY());
//
//    Point2D point = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertFromPixelToRendererCoordinates((int) 0, (int) 0);
//    int a = (int) (bottomSectionOfOpticalLongRectangleInPixel.getMaxX() + point.getX());
//    int b = (int) (bottomSectionOfOpticalLongRectangleInPixel.getMinX() + point.getY());
//
//    Point2D point2 = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(a, b);
//
//    int opticalLongRectangleWidthInPixel = (int) (point2.getX() + point2.getY());
//
//    int a2 = (int) (point.getX() - bottomSectionOfOpticalLongRectangleInPixel.getMaxY());
//    int b2 = (int) (point.getY() - bottomSectionOfOpticalLongRectangleInPixel.getMinY());
//
//    Point2D point3 = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(a2, b2);
//
//    int opticalLongRectangleHeightInPixel = (int) (-point3.getX() - point3.getY());
//
//    Rectangle bottomSectionOfOpticalLongRectangleInPixelInRectangle = new Rectangle((int) coorInPixels.getX(), (int) coorInPixels.getY(), opticalLongRectangleWidthInPixel, opticalLongRectangleHeightInPixel);
//    bottomSectionOfOpticalLongRectangleInPixel.setRect(coorInPixels.getX(), coorInPixels.getY(), opticalLongRectangleWidthInPixel, opticalLongRectangleHeightInPixel);
//    boolean isFirstDrag = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().isOpticalRegionFirstDrag();
//
//      // Get the panel bounding rectangle in pixel
//    PanelRectangle panelRectangle = null;  
//    panelRectangle = _project.getPanel().getPanelSettings().getRightSectionOfLongPanel();
//    panelRectangle.add(_project.getPanel().getPanelSettings().getLeftSectionOfLongPanel().getRectangle2D());
//   
//    Point2D panelRectangleCoordinateInPixels = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates((int) panelRectangle.getMinX(), (int) panelRectangle.getMaxY());
//    Point2D panelRectanglePoint = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertFromPixelToRendererCoordinates((int) 0, (int) 0);
//    int pointA = (int) (panelRectangle.getMaxX() + panelRectanglePoint.getX());
//    int pointB = (int) (panelRectangle.getMinX() + panelRectanglePoint.getY());
//
//    Point2D panelRectanglePoint2 = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(pointA, pointB);
//    int panelRectangleWidthInPixel = (int) (panelRectanglePoint2.getX() + panelRectanglePoint2.getY());
//
//    int pointA2 = (int) (panelRectanglePoint.getX() - panelRectangle.getMaxY());
//    int pointB2 = (int) (panelRectanglePoint.getY() - panelRectangle.getMinY());
//
//    Point2D panelRectanglePoint3 = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().convertFromRendererToPixelCoordinates(pointA2, pointB2);
//    int panelRectangleHeightInPixel = (int) (-panelRectanglePoint3.getX() - panelRectanglePoint3.getY());
//
//    Rectangle panelRectangleInPixel = new Rectangle();
//    panelRectangleInPixel.setRect(panelRectangleCoordinateInPixels.getX(), panelRectangleCoordinateInPixels.getY(), panelRectangleWidthInPixel, panelRectangleHeightInPixel);
//
//    _opticalRegionMap = new LinkedHashMap<Rectangle, String>();
//    Map<Rectangle, BooleanRef> opticalRegionMap = new LinkedHashMap<Rectangle, BooleanRef>();
//  
//    opticalRegionMap = OpticalRegionGeneration.generateCameraFovPanelRectangles(panelRectangleInPixel, bottomSectionOfOpticalLongRectangleInPixelInRectangle, isOpticalLongPanel, (int)minX, (int)minY, widthInPixels, heightInPixels, fitGraphicsToScreenWhenWindowIsResizedNewScale, zoomFactor, isFirstDrag);
//
//    for (Map.Entry<Rectangle, BooleanRef> mapEntry : opticalRegionMap.entrySet())
//    {
//      Rectangle dividedRectangle = mapEntry.getKey();
//      BooleanRef booleanRef = mapEntry.getValue();
//
//      Rectangle newDividedRectangle = new Rectangle();
//      newDividedRectangle = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(dividedRectangle);
//
//      if (_topGroup1PspCheckBox.isSelected() && _alignmentGroup1TopRadioButton.isSelected())
//      {
//        if (alignmentGroup1Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup1Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//
//      if (_topGroup2PspCheckBox.isSelected() && _alignmentGroup2TopRadioButton.isSelected())
//      {
//        if (alignmentGroup2Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup2Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//
//      if (_topGroup3PspCheckBox.isSelected() && _alignmentGroup3TopRadioButton.isSelected())
//      {
//        if (alignmentGroup3Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup3Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//      
//      if (_bottomGroup1PspCheckBox.isSelected() && _alignmentGroup1BottomRadioButton.isSelected())
//      {
//        if (alignmentGroup4Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup4Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//      
//      if (_bottomGroup2PspCheckBox.isSelected() && _alignmentGroup2BottomRadioButton.isSelected())
//      {
//        if (alignmentGroup5Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup5Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//      
//      if (_bottomGroup3PspCheckBox.isSelected() && _alignmentGroup3BottomRadioButton.isSelected())
//      {
//        if (alignmentGroup6Region.getBounds().contains(newDividedRectangle))
//        {
//          if (_opticalRegionMap.containsKey(dividedRectangle) == false)
//          {
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
//            _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
//            _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
//            _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
//          }
//        }
////        else
////        {
////          if (alignmentGroup6Region.contains(newDividedRectangle.getCenterX(), newDividedRectangle.getCenterY()))
////          {
////            if (_opticalRegionMap.containsKey(dividedRectangle) == false)
////            {
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().addSurfaceMapAlignmentRegion(newDividedRectangle.getBounds2D());
////              _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setSelectedComponentRegion(newDividedRectangle);
////              _currentAlignmentGroupForSurfaceMap = _currentAlignmentGroup.getName();
////              _opticalRegionMap.put(dividedRectangle, _currentAlignmentGroup.getName());
////            }
////          }
////        }
//      }
//
//    }
//
//    _testDev.getBoardGraphicsEngineSetup().setAlignmentDividedOpticalRegionList(_opticalRegionMap);
//
//    if (_opticalRegionMap.size() > 0)
//    {
//      _isNotEmptySpace = true;
//    }
//    _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegionInAlignment();
//    _testDev.getBoardGraphicsEngineSetup().setDragOpticalRegionLayersVisible(true);
  }
  
 /**
  *
  * @author Jack Hwee
  */
  private Rectangle createAllowableRectangleForPsp(Rectangle rect)
  {
    int minX = (int)rect.getMinX() - PSP_RECTANGLE_OFFSET;
    int minY = (int)rect.getMinY() - PSP_RECTANGLE_OFFSET;
    int width = (int)rect.getWidth() + (PSP_RECTANGLE_OFFSET*2);
    int height = (int)rect.getHeight() + (PSP_RECTANGLE_OFFSET*2);
    Rectangle allowableRectangle = new Rectangle(minX, minY, width, height);
    
    // Commented - both panel-based and board-based alignment need to have this checking.
    //if (_isPanelBasedAlignment)
    //{
      //minX = (int)_project.getPanel().getShapeInNanoMeters().getBounds().getMinX();
      if (minX < 0)
      {
        width = width +  minX;
        minX = 0;    
      }
      
      if (allowableRectangle.getMaxX() > _project.getPanel().getShapeInNanoMeters().getBounds().getMaxX())
      {
        int offsetX = (int)(allowableRectangle.getMaxX() - _project.getPanel().getShapeInNanoMeters().getBounds().getMaxX());
      
        width = width -  offsetX; 
      }
      
      if (minY < 0)
      {
        height = height +  minY;
        minY = 0;
      }
      
      if (allowableRectangle.getMaxY() > _project.getPanel().getShapeInNanoMeters().getBounds().getMaxY())
      {
        int offsetY = (int)(allowableRectangle.getMaxY() - _project.getPanel().getShapeInNanoMeters().getBounds().getMaxY());
      
        height = height -  offsetY; 
      }
      allowableRectangle =  new Rectangle(minX, minY, width, height);
    //}
      
    return allowableRectangle;
  }
  
  /**
   * Clear all pads in Alignment Group
   * @author Ying-Huan.Chu
   */
  public void clearAlignmentGroup(AlignmentGroup alignmentGroup, JList alignmentGroupList, DefaultListModel alignmentGroupListModel)
  {
    if (alignmentGroupList != null)
    {
      if (alignmentGroupList.getVisibleRowCount() > 0)
        alignmentGroupList.setSelectionInterval(0, alignmentGroupListModel.size()-1);
      removeSelectedItems(alignmentGroupList, alignmentGroup);
      alignmentGroupListModel.clear();
    }
  }
  
  /**
   * Get components to be considered for Auto-select Alignment Group function.
   * @author Ying-Huan.Chu
   */
  private java.util.List<com.axi.v810.business.panelDesc.Component> getComponentsToBeConsidered()
  {
    java.util.List<com.axi.v810.business.panelDesc.Component> components = new ArrayList<>();
    if (_project.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      if (_testDev.getPanelGraphicsEngineSetup().isTopComponentLayersVisible() &&
          _testDev.getPanelGraphicsEngineSetup().isBottomComponentLayersVisible())
      {
        if (XrayTester.isXXLBased()) // use Top components for XXL system.
          components = _project.getPanel().getTopComponents();
        else // use Bottom components for Standard system
          components = _project.getPanel().getBottomComponents();
      }
      else if (_testDev.getPanelGraphicsEngineSetup().isTopComponentLayersVisible())
        components = _project.getPanel().getTopComponents();
      else if (_testDev.getPanelGraphicsEngineSetup().isBottomComponentLayersVisible())
        components = _project.getPanel().getBottomComponents();
    }
    else
    {
      if (_testDev.getBoardGraphicsEngineSetup().isTopComponentLayersVisible() &&
          _testDev.getBoardGraphicsEngineSetup().isBottomComponentLayersVisible())
      {
        if (XrayTester.isXXLBased()) // use Top components for XXL system.
          components = _project.getPanel().getBoards().get(0).getTopComponents();
        else // use Bottom components for Standard system
          components = _project.getPanel().getBoards().get(0).getBottomComponents();
      }
      else if (_testDev.getBoardGraphicsEngineSetup().isTopComponentLayersVisible())
        components = _project.getPanel().getBoards().get(0).getTopComponents();
      else if (_testDev.getBoardGraphicsEngineSetup().isBottomComponentLayersVisible())
        components = _project.getPanel().getBoards().get(0).getBottomComponents();
    }
    
    java.util.List<com.axi.v810.business.panelDesc.Component> componentsToBeConsidered = new ArrayList<>();
    for (com.axi.v810.business.panelDesc.Component component : components)
    {
      if (component.getComponentTypeSettings().isTestable() && component.getComponentTypeSettings().isLoaded())
      {
        if (component.getComponentType().getJointTypeEnums().get(0).equals(JointTypeEnum.CAPACITOR) ||
            component.getComponentType().getJointTypeEnums().get(0).equals(JointTypeEnum.RESISTOR))
        {
          // make sure there are no overlapping component on the other side of the board.
          Rectangle componentRectangle = component.getShapeRelativeToPanelInNanoMeters().getBounds();
          if (component.isTopSide())
          {
            for (com.axi.v810.business.panelDesc.Component bottomComponent : _project.getPanel().getBottomComponents())
            {
              Rectangle bottomComponentRectangle = bottomComponent.getShapeRelativeToPanelInNanoMeters().getBounds();
              if (componentRectangle.intersects(bottomComponentRectangle) == false)
              {
                if (componentsToBeConsidered.contains(component) == false)
                {
                  componentsToBeConsidered.add(component);
                }
              }
            }
          }
          else
          {
            for (com.axi.v810.business.panelDesc.Component topComponent : _project.getPanel().getTopComponents())
            {
              Rectangle topComponentRectangle = topComponent.getShapeRelativeToPanelInNanoMeters().getBounds();
              if (componentRectangle.intersects(topComponentRectangle) == false)
              {
                if (componentsToBeConsidered.contains(component) == false)
                {
                  componentsToBeConsidered.add(component);
                }
              }
            }
          }
        }
      }
    }
    return componentsToBeConsidered;
  }
  
  /**
   * Auto select Alignment Group for short panel.
   * @author Ying-Huan.Chu
   */
  private void autoSelectAlignmentGroupButton_actionPerformed()
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        _project.getPanel().getPanelSettings().clearAllAlignmentTransforms();
        if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
        {
          for(Board board: _project.getPanel().getBoards())
          {
            board.getBoardSettings().clearAllAlignmentTransforms();
          }
        }
        boolean hasLeftAlignment = false;
        if (_project.getPanel().getPanelSettings().isPanelBasedAlignment())
        {
          if (_project.getPanel().getPanelSettings().isLongPanel())
          {
            if (_project.getTestProgram().hasLeftProgram())
              hasLeftAlignment = true;
          }
        }
        else
        {
          for (Board board : _project.getPanel().getBoards())
          {
            if (board.isLongBoard())
            {
              if (_project.getTestProgram().hasLeftProgram())
              {
                if (_project.getTestProgram().hasTestSubProgram(board, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.LOW) ||
                    _project.getTestProgram().hasTestSubProgram(board, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.HIGH))
                {
                  hasLeftAlignment = true;
                }
                else
                {
                  // if a long board only has left alignment group but no right alignment group,
                  // do not allow user to use auto-select alignment group function.
                  JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString("GUI_AUTO_SELECT_ALIGNMENT_GROUP_ERROR_MESSAGE_KEY"),
                                        StringLocalizer.keyToString("GUI_AUTO_SELECT_ALIGNMENT_GROUP_ERROR_TITLE_KEY"),
                                        JOptionPane.ERROR_MESSAGE);
                  return;
                }
              }
            }
          }
        }
        java.util.List<com.axi.v810.business.panelDesc.Component> componentsToBeConsidered = getComponentsToBeConsidered();
        if (componentsToBeConsidered.isEmpty())
        {
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString("GUI_AUTO_SELECT_ALIGNMENT_GROUP_NO_SUITABLE_COMPONENTS_ERROR_MESSAGE_KEY"),
                                        StringLocalizer.keyToString("GUI_AUTO_SELECT_ALIGNMENT_GROUP_ERROR_TITLE_KEY"),
                                        JOptionPane.ERROR_MESSAGE);
          return;
        }
        
        if (hasLeftAlignment == false)
        {
          clearAlignmentGroup(_alignmentGroup1, _alignmentGroup1List, _alignmentGroup1ListModel);
          clearAlignmentGroup(_alignmentGroup2, _alignmentGroup2List, _alignmentGroup2ListModel);
          clearAlignmentGroup(_alignmentGroup3, _alignmentGroup3List, _alignmentGroup3ListModel);
          
          // 1st group
          autoAddComponentsToAlignmentGroup(_alignmentGroup1,
                                            sortFromLeftTopToRightBottom(componentsToBeConsidered),
                                            false);

          // 2nd group
          autoAddComponentsToAlignmentGroup(_alignmentGroup2,
                                            sortFromRightTopToLeftBottom(componentsToBeConsidered),
                                            false);

          // 3rd group
          autoAddComponentsToAlignmentGroup(_alignmentGroup3,
                                            sortFromLeftBottomToRightTop(componentsToBeConsidered),
                                            false);
        }
        else
        {
          clearAlignmentGroup(_alignmentGroup1Top, _alignmentGroup1TopList, _alignmentGroup1TopListModel);
          clearAlignmentGroup(_alignmentGroup2Top, _alignmentGroup2TopList, _alignmentGroup2TopListModel);
          clearAlignmentGroup(_alignmentGroup3Top, _alignmentGroup3TopList, _alignmentGroup3TopListModel);
          clearAlignmentGroup(_alignmentGroup1Bottom, _alignmentGroup1BottomList, _alignmentGroup1BottomListModel);
          clearAlignmentGroup(_alignmentGroup2Bottom, _alignmentGroup2BottomList, _alignmentGroup2BottomListModel);
          clearAlignmentGroup(_alignmentGroup3Bottom, _alignmentGroup3BottomList, _alignmentGroup3BottomListModel);
          
          java.util.List<com.axi.v810.business.panelDesc.Component> componentsToBeConsideredForRightAlignment = new ArrayList<>(componentsToBeConsidered);
          java.util.List<com.axi.v810.business.panelDesc.Component> componentsToBeConsideredForLeftAlignment = new ArrayList<>(componentsToBeConsidered);
          
          // 1st Top group
          autoAddComponentsToAlignmentGroup(_alignmentGroup1Top,
                                            sortFromLeftTopToRightBottom(componentsToBeConsideredForRightAlignment),
                                            false);

          // 2nd Top group
          autoAddComponentsToAlignmentGroup(_alignmentGroup2Top,
                                            sortFromRightTopToLeftBottom(componentsToBeConsideredForRightAlignment),
                                            false);

          // 3rd Top group
          autoAddComponentsToAlignmentGroup(_alignmentGroup3Top,
                                            sortFromLeftBottomToRightTop(componentsToBeConsideredForRightAlignment),
                                            false);
          
          // 1st Bottom group
          autoAddComponentsToAlignmentGroup(_alignmentGroup1Bottom,
                                            sortFromLeftTopToRightBottom(componentsToBeConsideredForLeftAlignment),
                                            true);

          // 2nd Bottom group
          autoAddComponentsToAlignmentGroup(_alignmentGroup2Bottom,
                                            sortFromRightTopToLeftBottom(componentsToBeConsideredForLeftAlignment),
                                            true);

          // 3rd Bottom group
          autoAddComponentsToAlignmentGroup(_alignmentGroup3Bottom,
                                            sortFromLeftBottomToRightTop(componentsToBeConsideredForLeftAlignment),
                                            true);
        }
      }
    });
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void autoAddComponentsToAlignmentGroup(AlignmentGroup alignmentGroup,
                                                 java.util.List<com.axi.v810.business.panelDesc.Component> components,
                                                 boolean hasLeftAlignmentRegion)
  {
    _currentAlignmentGroup = alignmentGroup;
    
    java.util.List<com.axi.v810.business.panelDesc.Component> componentsToBeRemoved = new ArrayList<>();
    for (int i = 0; i < components.size(); ++i)
    {
      int alignmentGroupPadSize = _currentAlignmentGroup.getPads().size();
      if (alignmentGroupPadSize < _maxPadsAndFiducials)
      {
        ProgramGeneration programGeneration = ProgramGeneration.getInstance();
        _mainUI.generateTestProgramForAlignmentIfNeededWithReason(StringLocalizer.keyToString("MMGUI_ALIGNMENT_NEEDS_TEST_PROGRAM_KEY"),
                                                                  StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                                                  _project);
        java.util.List<Fiducial> fiducialsToAdd = addFiducialsToCurrentAlignmentGroup(new ArrayList<Fiducial>());
        java.util.List<Pad> padsToAdd = new ArrayList<>();
        if (_currentAlignmentGroup.getPads().isEmpty() == false)
        {
          // Ying-Huan.Chu - [XCR-2037] subtask : handle panel-based recipe with overlapping boards.
          if (components.get(i).getBoard().getName().equals(_currentAlignmentGroup.getPads().get(0).getComponent().getBoard().getName()) == false)
          {
            continue;
          }
          padsToAdd.addAll(_currentAlignmentGroup.getPads());
        }
        padsToAdd.addAll(components.get(i).getPads());
        int componentPadSize = components.get(i).getPads().size();
        
        AlignmentValidityEnum validityEnum = AlignmentValidityEnum.INVALID_DOES_NOT_FIT;
        if(_project.getPanel().getPanelSettings().isPanelBasedAlignment()) // Panel-based alignment
        {
          if (_project.getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.LOW))
            validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.LOW), padsToAdd, fiducialsToAdd);
          if (_project.getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.HIGH))
            validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.HIGH), padsToAdd, fiducialsToAdd);
          if (hasLeftAlignmentRegion) // if left alignment region exists (long panel)
          {
            if (_project.getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.LOW))
              validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.LOW), padsToAdd, fiducialsToAdd);
            if (_project.getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.HIGH))
              validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.HIGH), padsToAdd, fiducialsToAdd);
          }
        }
        else // Board-based alignment
        {
          Board currentBoard = padsToAdd.get(0).getComponent().getBoard();
          if(currentBoard.isLongBoard() == false) // if current board is not a long board
          {
            if (_project.getPanel().getPanelSettings().isLongPanel()) // if current board (short board)'s panel is a long panel.
            {
              if (currentBoard.getBoardSettings().useLeftAlignmentGroup()) // if the current board (1st board) is on the left side of the panel.
              {
                // this condition is needed as there's a possibility that a panel's 1st board is on the left side of the panel.
                if (_project.getTestProgram().hasTestSubProgram(currentBoard, PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.LOW))
                  validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(currentBoard, PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.LOW), padsToAdd, fiducialsToAdd);
                if (_project.getTestProgram().hasTestSubProgram(currentBoard, PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.HIGH))
                  validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(currentBoard, PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.HIGH), padsToAdd, fiducialsToAdd);
              }
              else
              {
                // if the current board (1st board) is on the right side of the panel.
                if (_project.getTestProgram().hasTestSubProgram(currentBoard, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.LOW))
                  validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(currentBoard, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.LOW), padsToAdd, fiducialsToAdd);
                if (_project.getTestProgram().hasTestSubProgram(currentBoard, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.HIGH))
                  validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(currentBoard, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.HIGH), padsToAdd, fiducialsToAdd);
              }
            }
            else // if current board (short board)'s panel is a short panel.
            {
              if (_project.getTestProgram().hasTestSubProgram(currentBoard, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.LOW))
                validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(currentBoard, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.LOW), padsToAdd, fiducialsToAdd);
              if (_project.getTestProgram().hasTestSubProgram(currentBoard, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.HIGH))
                validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(currentBoard, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.HIGH), padsToAdd, fiducialsToAdd);
            }
          }
          else // long board condition
          {
            if (_project.getTestProgram().hasTestSubProgram(currentBoard, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.LOW))
              validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(currentBoard, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.LOW), padsToAdd, fiducialsToAdd);
            if (_project.getTestProgram().hasTestSubProgram(currentBoard, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.HIGH))
              validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(currentBoard, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.HIGH), padsToAdd, fiducialsToAdd);
            if (hasLeftAlignmentRegion) // if the long board has left alignment region.
            {
              // this checking is needed as there's a possibility that all components on the left side of the panel are set to No Test / No Load. 
              if (_project.getTestProgram().hasTestSubProgram(currentBoard, PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.LOW))
                validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(currentBoard, PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.LOW), padsToAdd, fiducialsToAdd);
              if (_project.getTestProgram().hasTestSubProgram(currentBoard, PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.HIGH))
                validityEnum = programGeneration.checkAlignmentRegionValidity(_project.getTestProgram().getTestSubProgramHasAlignmentRegion(currentBoard, PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.HIGH), padsToAdd, fiducialsToAdd);
            }
          }
        }
        if (validityEnum.equals(AlignmentValidityEnum.INVALID_DOES_NOT_FIT) == false)
        {
          if ((alignmentGroupPadSize + componentPadSize) <= _maxPadsAndFiducials)
          {
            addPadsToAlignmentGroup(components.get(i).getPads());
            componentsToBeRemoved.add(components.get(i));
          }
          if (alignmentGroupPadSize == _maxPadsAndFiducials)
          {
            break;
          }
        }
      }
    }
    components.removeAll(componentsToBeRemoved);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private boolean addPadsToAlignmentGroup(java.util.List<Pad> pads)
  { 
    if(_project.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      try
      {
        _commandManager.execute(new AddFeaturesToAlignmentGroupCommand(_currentAlignmentGroup, 
                                                                       pads, 
                                                                       new ArrayList<Fiducial>()));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
      }
    }
    else
    {
      try
      {
        _commandManager.execute(new AddFeaturesToBoardSettingsAlignmentGroupCommand(getCurrentAlignmentGroupIndex(), 
                                                                                    _project.getPanel(), pads, new ArrayList<Fiducial>()));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                ex,
                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                true);
      }
    }
    return true;
  }

  /**
   * @author Ying-Huan.Chu
   */
  private java.util.List<com.axi.v810.business.panelDesc.Component> sortFromLeftTopToRightBottom(java.util.List<com.axi.v810.business.panelDesc.Component> components)
  {
    Collections.sort(components, new Comparator<com.axi.v810.business.panelDesc.Component>() 
    {
      public int compare(com.axi.v810.business.panelDesc.Component a, com.axi.v810.business.panelDesc.Component b)
      {
        Point2D pointA = a.getCoordinateRelativeToPanelInNanoMeters().getPoint2D();
        Point2D pointB = b.getCoordinateRelativeToPanelInNanoMeters().getPoint2D();
        
        if (pointA.getX() < pointB.getX())
        {
          if (pointA.getY() < pointB.getY())
          {
            if (Math.abs(pointA.getY() - pointB.getY()) <= Math.abs(pointA.getX() - pointB.getX()))
              return -1;
            else
              return 1;
          }
          else if (pointA.getY() > pointB.getY())
            return -1;
          else // pointA.getY() == pointB.getY()
            return -1;
        }
        else if (pointA.getX() > pointB.getX())
        {
          if (pointA.getY() > pointB.getY())
          {
            if (Math.abs(pointA.getX() - pointB.getX()) < Math.abs(pointA.getY() - pointB.getY()))
              return -1;
            else
              return 1;
          }
          else
            return 1;
        }
        else // pointA.getX() == pointB.getX()
        {
          if (pointA.getY() < pointB.getY())
            return 1;
          else if (pointA.getY() > pointB.getY())
            return -1;
          else // pointA.getY() == pointB.getY()
            return 0;
        }
      }
    });
    return components;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private java.util.List<com.axi.v810.business.panelDesc.Component> sortFromRightTopToLeftBottom(java.util.List<com.axi.v810.business.panelDesc.Component> components)
  {
    Collections.sort(components, new Comparator<com.axi.v810.business.panelDesc.Component>() 
    {
      public int compare(com.axi.v810.business.panelDesc.Component a, com.axi.v810.business.panelDesc.Component b)
      {
        Point2D pointA = a.getCoordinateRelativeToPanelInNanoMeters().getPoint2D();
        Point2D pointB = b.getCoordinateRelativeToPanelInNanoMeters().getPoint2D();
        
        if (pointA.getX() > pointB.getX())
        {
          if (pointA.getY() > pointB.getY())
            return -1;
          else if (pointA.getY() < pointB.getY())
          {
            if (Math.abs(pointA.getY() - pointB.getY()) < Math.abs(pointA.getX() - pointB.getX()))
              return -1;
            else
              return 1;
          }
          else
            return -1;
        }
        else if (pointA.getX() == pointB.getX())
        {
          if (pointA.getY() > pointB.getY())
            return -1;
          else if (pointA.getY() < pointB.getY())
            return 1;
          else // equal
            return 0;
        }
        else // pointA.getX() < pointB.getX()
        {
          if (pointA.getY() > pointB.getY())
          {
            if (Math.abs(pointA.getY() - pointB.getY()) > Math.abs(pointA.getX() - pointB.getX()))
              return -1;
            else
              return 1;
          }
          else if (pointA.getY() < pointB.getY())
            return 1;
          else
            return 1;
        }
      }
    });
    return components;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private java.util.List<com.axi.v810.business.panelDesc.Component> sortFromLeftBottomToRightTop(java.util.List<com.axi.v810.business.panelDesc.Component> components)
  {
    Collections.sort(components, new Comparator<com.axi.v810.business.panelDesc.Component>() 
    {
      public int compare(com.axi.v810.business.panelDesc.Component a, com.axi.v810.business.panelDesc.Component b)
      {
        Point2D pointA = a.getCoordinateRelativeToPanelInNanoMeters().getPoint2D();
        Point2D pointB = b.getCoordinateRelativeToPanelInNanoMeters().getPoint2D();
        
        if (pointA.getX() < pointB.getX())
        {
          if (pointA.getY() < pointB.getY())
            return -1;
          else if (pointA.getY() > pointB.getY())
          {
            if (Math.abs(pointA.getY() - pointB.getY()) < Math.abs(pointA.getX() - pointB.getX()))
              return -1;
            else
              return 1; 
          }
          else
            return -1;
        }
        else if (pointA.getX() == pointB.getX())
        {
          if (pointA.getY() < pointB.getY())
            return -1;
          else if (pointA.getY() > pointB.getY())
            return 1;
          else // equal
            return 0;
        }
        else // pointA.getX() > pointB.getX()
        {
          if (pointA.getY() < pointB.getY())
          {
            if (Math.abs(pointA.getY() - pointB.getY()) < Math.abs(pointA.getX() - pointB.getX()))
              return 1;
            else
              return -1;
          }
          else if (pointA.getY() > pointB.getY())
            return 1;
          else // pointA.getY() == pointB.getY()
            return 1;
        }
      }
    });
    return components;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void updateAllAlignmentGroups()
  {
    java.util.List<AlignmentGroup> alignmentGroups = new ArrayList<>();
    if(_project.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      alignmentGroups = _project.getPanel().getPanelSettings().getAllAlignmentGroups();
    }
    else
    {
      for(Board board : _project.getPanel().getBoards())
      {
        alignmentGroups.addAll(board.getBoardSettings().getAllAlignmentGroups());
      }
    }
    for(AlignmentGroup alignmentGroup : alignmentGroups)
    {
      updateAlignmentGroup(alignmentGroup);
    }
  }
  
  /**
   * Sends appropriate GUI Observable to update Alignment Visibility.
   * @author Ying-Huan.Chu
   */
  private void updateAlignmentGroupVisibility()
  {
    if (_project.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      if (_project.getPanel().getPanelSettings().isLongPanel())
      {
        if (_project.getTestProgram().hasLeftProgram() == false)
        {
          _topRegionActive = true;
        }
      }
      else
      {
        _topRegionActive = true;
      }
    }
    else
    {
      for (Board board : _project.getPanel().getBoards())
      {
        if (board.isLongBoard())
        {
          if (_project.getTestProgram().hasLeftProgram() == false)
          {
            _topRegionActive = true;
          }
        }
        else
        {
          _topRegionActive = true;
        }
      }
    }
    _guiObservable.stateChanged(new Boolean(_topRegionActive), GraphicsControlEnum.ALIGNMENT_VISIBLE);
  }
  
  /**
   * XCR-3207 Unable to cancel verification image and align the panel/board with existing verification image
   * @author Cheah Lee Herng 
   */
  private void manualAlignOnImage(final TestSubProgram subProgram, final BooleanLock alignmentComfirmationLock)
  {
    Assert.expect(subProgram != null);
    Assert.expect(alignmentComfirmationLock != null);
    
    _manualAlignmentInProgress = true;
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        _testDev.showImageWindow();
      }
    });

    //    _alignmentGroupsCardLayout.show(_alignmentGroupsPanel, "splitPanelAlignmentPanel");
    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _testExecution.runManualAlignment(subProgram);
        }
        catch (final AlignmentAlgorithmFailedToLocatePadsBusinessException ex1)
        {
          _exception = ex1;
          handleAlignmentError(ex1);
        }
        catch (final AlignmentAlgorithmFailedToCalculateTransformBusinessException ex2)
        {
          _exception = ex2;
          handleAlignmentError(ex2);
        }
        catch (AlignmentAlgorithmResultOutOfBoundsBusinessException ex3)
        {
          _exception = ex3;
          handleAlignmentError(ex3);
        }
        catch (final XrayTesterException ex)
        {
          _exception = ex;
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              _mainUI.handleXrayTesterException(ex);
              endManualAlignment();
            }
          });
        }
        finally
        {
          //XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
          if (_alignment.isCancelManualAlignment() == false)
            _countTestSubprogram++;
          
          if (_countTestSubprogram == _customTestSubprogramSize || _isManualAlignmentInitiallyComplete)
          {
            _project.getPanel().getPanelSettings().setIsManualAlignmentAllCompleted(true);
          }
          updateAlignmentStatusMessages();
          alignmentComfirmationLock.setValue(true);
        }
      }
    });

    try
    {
      alignmentComfirmationLock.waitUntilTrue();
      _runAlignmentButton.setEnabled(true);
      _runAlignmentWithDifferentSliceButton.setEnabled(true);
    }
    catch (InterruptedException ex1)
    {
      Assert.logException(ex1);
    }
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void alignmentModeRadioButton_actionPerformed()
  {
    if (_project.getPanel().getPanelSettings().isUsing2DAlignmentMethod() == _alignment2DRadioButton.isSelected())
    {
      return;
    }
        
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
                                                                      StringLocalizer.keyToString("GUI_SWITCHING_ALIGNMENT_MODE_BUSYDIALOG_MESSAGE_KEY"),
                                                                      StringLocalizer.keyToString("GUI_SWITCHING_ALIGNMENT_MODE_BUSYDIALOG_TITLE_KEY"),
                                                                      true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          //Prompt confirm dialog to warn user about alignment transform will be cleared if switch alignment mode
          if (_project.getPanel().getPanelSettings().isManualAlignmentCompleted())
          {
            int choice = JOptionPane.showConfirmDialog(_mainUI,
                                                       StringLocalizer.keyToString("GUI_SWITCHING_ALIGNMENT_MODE_COMFIRMATION_MESSAGE_KEY"),
                                                       StringLocalizer.keyToString("GUI_SWITCHING_ALIGNMENT_MODE_BUSYDIALOG_TITLE_KEY"), 
                                                       JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (choice != JOptionPane.YES_OPTION)
            {
              update2DAlignmentState();
              return;
            }
          }
          
          _project.getTestProgram().clearRunAlignmentLearned();
          _project.getPanel().getPanelSettings().clearAllAlignmentTransforms();
          if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
          {
            for(Board board: _project.getPanel().getBoards())
            {
              board.getBoardSettings().clearAllAlignmentTransforms();
            }
          }
          
          for (AlignmentGroup alignmentGroup : _project.getTestProgram().getAlignmentGroups())
          {
            alignmentGroup.setUseCustomizeAlignmentSetting(false);  
            alignmentGroup.setUseZHeight(false);
          }
          
          try
          {
            _commandManager.execute(new ProjectSetAlignmentMethodCommand(_project, _alignment2DRadioButton.isSelected()));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
          }
          _project.getTestProgram();
          update2DAlignmentState();
        }
        finally
        {
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              busyCancelDialog.dispose();
            }
          });
        }
      }
    });
    busyCancelDialog.setVisible(true);
  }
           
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void update2DAlignmentState()
  {    
    if (_project.getPanel().getPanelSettings().isUsing2DAlignmentMethod())
    {
      _alignment2DRadioButton.setSelected(true);
    }
    else
    {
      _alignment3DRadioButton.setSelected(true);
    }
  }
  
   /**
   * @author weng-jian.eoh
   * 
   * XCR-3589 Combo STD and XXL software GUI
   */
  public void removeAllObservable()
  {
    _inspectionEventObservable.deleteObserver(this);
    // Wei Chin: it might be null if it fail to enter the manual alignment 
    if(_selectedRendererObservable != null)
      _selectedRendererObservable.deleteObserver(this);

    _hardwareObservable.deleteObserver(this);
  }
}
