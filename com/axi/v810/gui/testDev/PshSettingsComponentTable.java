package com.axi.v810.gui.testDev;

import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.Assert;

/**
 *
 * @author siew-yeng.phang
 */
public class PshSettingsComponentTable extends JSortTable
{
  public static final int _COMPONENT_NAME_INDEX = 0;
  public static final int _JOINT_TYPE_INDEX = 1;
  public static final int _SUBTYPE_INDEX = 2;
  
  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _COMPONENT_NAME_COLUMN_WIDTH_PERCENTAGE = .2;
  private static final double _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE = .4;
  private static final double _SUBTYPE_COLUMN_WIDTH_PERCENTAGE = .4;
  
  private boolean _modelSet = false;
  
  /**
   * Default constructor.
   * @author Siew Yeng
   */
  public PshSettingsComponentTable(TableModel tableModel)
  {
    _modelSet = true;
    this.setModel(tableModel);
    // do nothing
  }
  
  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Siew Yeng
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int componentNameColumnWidth = (int)(totalColumnWidth * _COMPONENT_NAME_COLUMN_WIDTH_PERCENTAGE);
    int jointTypeNameColumnWidth = (int)(totalColumnWidth * _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE);
    int subtypeNameColumnWidth = (int)(totalColumnWidth * _SUBTYPE_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_COMPONENT_NAME_INDEX).setPreferredWidth(componentNameColumnWidth);
    columnModel.getColumn(_JOINT_TYPE_INDEX).setPreferredWidth(jointTypeNameColumnWidth);
    columnModel.getColumn(_SUBTYPE_INDEX).setPreferredWidth(subtypeNameColumnWidth);
  }

  /**
   * @author Siew Yeng
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();
  }
  
  /**
   * @author Siew Yeng
   */
  public void clearSelection()
  {
    TableModel model = getModel();
    // this function gets called before the table model is set, during intialization. At that time,
    // the model will be a defaultTableModel so the assert is sure to fail
    if (_modelSet)
    {
      Assert.expect(model instanceof PshSettingsComponentTableModel);
      ((PshSettingsComponentTableModel)model).clearSelectedComponent();
    }
    super.clearSelection();
  }
}
