package com.axi.v810.gui.testDev;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;

/**
 * Class used to render a single entry in the manual alignment group definition
 * @author Andy Mechtenberg
 */
public class ManualAlignmentGroupEntry
{
  private final String _COMPONENT_PAD_SEPARATOR = " - ";
  private Pad _pad;
  private Fiducial _fiducial;

  /**
   * @author Andy Mechtenberg
   */
  ManualAlignmentGroupEntry(Pad pad)
  {
    Assert.expect(pad != null);
    _pad = pad;
    _fiducial = null;
  }

  /**
   * @author Andy Mechtenberg
   */
  ManualAlignmentGroupEntry(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);
    _fiducial = fiducial;
    _pad = null;
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean isPad()
  {
    if (_pad != null)
      return true;
    else
      return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  Pad getPad()
  {
    Assert.expect(_pad != null);
    return _pad;
  }

  /**
   * @author Andy Mechtenberg
   */
  Fiducial getFiducial()
  {
    Assert.expect(_fiducial != null);
    return _fiducial;
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean requiresOtherPadAsPair()
  {
    if (_pad != null)
    {
      PadType padType = _pad.getPadType();
      JointTypeEnum padJointType = padType.getPackagePin().getJointTypeEnum();
      if (ImageAnalysis.isSpecialTwoPinComponent(padJointType))
        return true;
    }
    return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  PadType getPadType()
  {
    Assert.expect(_pad != null);
    return _pad.getPadType();
  }

  /**
   * An internal string representation to keep the sort order with respect to multi-boards and panels
   * @author Andy Mechtenberg
   */
  public String getSortOrderString()
  {
    String result = null;
    if (_pad != null)
    {
      String boardName = _pad.getComponent().getBoard().getName();
      String componentName = _pad.getComponent().getReferenceDesignator();
      result =  boardName + ":" + componentName + _COMPONENT_PAD_SEPARATOR + _pad.getName();
    }
    else
    {
      String boardOrPanel;
      if (_fiducial.isOnBoard())
        boardOrPanel = _fiducial.getSideBoard().getBoard().getName();
      else
        boardOrPanel = "0";
      result = boardOrPanel + " " + _fiducial.getName();
    }
    return result;

  }

  /**
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    String result = null;
    if (_pad != null)
    {
      String componentName = _pad.getComponent().getReferenceDesignator();
      result =  componentName + _COMPONENT_PAD_SEPARATOR + _pad.getName();
    }
    else
    {
      result = _fiducial.getName();
    }
    return result;
  }
}
