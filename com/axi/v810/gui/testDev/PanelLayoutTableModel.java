package com.axi.v810.gui.testDev;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Description: Contains the data for the panel layout table.
 * @author Carli Connally
 */
class PanelLayoutTableModel extends AbstractTableModel
{
  private MainMenuGui _mainUI;
  //String labels retrieved from the localization properties file
  private static final String BOARD_NAME_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_BOARD_NAME_KEY");
  private static final String BOARD_TYPE_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_BOARD_TYPE_KEY");
  private static final String BOARD_X_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_X_LOC_KEY");
  private static final String BOARD_Y_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_Y_LOC_KEY");
  private static final String BOARD_ROTATION_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_ROTATION_KEY");
  private static final String BOARD_FLIP_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_FLIPPED_KEY");
  private static final String BOARD_ZOFFSET_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_Z_OFFSET_KEY");

  private String[] _columnLabels = { BOARD_NAME_LABEL,
                                     BOARD_TYPE_LABEL,
                                     BOARD_X_LABEL,
                                     BOARD_Y_LABEL,
                                     BOARD_ROTATION_LABEL,
                                     BOARD_FLIP_LABEL, 
                                     BOARD_ZOFFSET_LABEL};

  private Project _project = null;
  private List<Board> _boards;
  private List<Board> _selectedBoards;

  private static com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  
  // Handle surface map removal if there is board rotation  
  private boolean _isPanelBasedAlignmentMethodSelected = false;  
  private boolean _isEnablePsp = Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP);

  /**
   * Constructor.
   * @author Carli Connally
   */
  public PanelLayoutTableModel()
  {
    _mainUI = MainMenuGui.getInstance();
    // do nothing
  }

  /**
   * @author Andy Mechtenberg
   */
  void clearProjectData()
  {
    _project = null;
    if (_boards != null)
      _boards.clear();
    if (_selectedBoards != null)
      _selectedBoards.clear();
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Carli Connally
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @param column Index of the column
   * @return Column label for the column specified
   * @author Carli Connally
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @param columnIndex Index of the column
   * @return Class data type for the column specified
   * @author Carli Connally
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * Overridden method.  Gets the values from the project object.
   * @param rowIndex Index of the row
   * @param columnIndex Index of the column
   * @return Object containing the value
   * @author Carli Connally
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_project != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    //get the board that corresponds to the row
    Board board = _boards.get(rowIndex);


    //get the appropriate data item that corresponds to the column
    switch(columnIndex)
    {
      case PanelLayoutTable._BOARD_NAME_INDEX:
      {
        value = board.getName();
        break;
      }
      case PanelLayoutTable._BOARD_TYPE_INDEX:
      {
        value = board.getBoardType().toString();
        break;
      }
      case PanelLayoutTable._BOARD_X_INDEX:
      {
        MathUtilEnum displayUnits = _project.getDisplayUnits();
        /** @todo Figure out which to use for real */
        PanelCoordinate coord = board.getLowerLeftCoordinateRelativeToPanelInNanoMeters();
        double dXValue = coord.getX();
        dXValue = MathUtil.convertUnits(dXValue, MathUtilEnum.NANOMETERS, displayUnits);
        value = new Double(dXValue);
        break;
      }
      case PanelLayoutTable._BOARD_Y_INDEX:
      {
        MathUtilEnum displayUnits = _project.getDisplayUnits();
        /** @todo Figure out which to use for real */
        PanelCoordinate coord = board.getLowerLeftCoordinateRelativeToPanelInNanoMeters();
        double dYValue = coord.getY();
        dYValue = MathUtil.convertUnits(dYValue, MathUtilEnum.NANOMETERS, displayUnits);
        value = new Double(dYValue);
        break;
      }
      case PanelLayoutTable._BOARD_ROTATION_INDEX:
      {
        value = new Integer(board.getDegreesRotationRetainingLowerLeftCorner());
        break;
      }
      case PanelLayoutTable._BOARD_FLIP_INDEX:
      {
/** @todo wpd - is this the right call or do you want isFlippedAfterAllFlips() ? */
        value = new Boolean(board.isFlipped());
        break;
      }
      case PanelLayoutTable._BOARD_ZOFFSET_INDEX:
      {
        MathUtilEnum displayUnits = _project.getDisplayUnits();
        double zOffset = (double)board.getBoardSettings().getBoardZOffsetInNanometers();
        zOffset = MathUtil.convertUnits(zOffset, MathUtilEnum.NANOMETERS, displayUnits);
        value = new Double(zOffset);
        break;
      }  
      default:
      {
        Assert.expect(false);
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Carli Connally
   */
  public int getRowCount()
  {
    if(_boards != null)
    {
      return _boards.size();
    }
    else
      return 0;
  }

  /**
   * Overridden method.
   * @param rowIndex  Index of the row
   * @param columnIndex  Index of the column
   * @return boolean - whether the cell is editable
   * @author Carli Connally
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    boolean isEditable = false;

    if ((_selectedBoards == null) || (_selectedBoards.contains(_boards.get(rowIndex)) == false))
      return false;

    switch(columnIndex)
    {
      case PanelLayoutTable._BOARD_NAME_INDEX:
      {
        isEditable = true;
        break;
      }
      case PanelLayoutTable._BOARD_TYPE_INDEX:
      {
        isEditable = false;
        break;
      }
      case PanelLayoutTable._BOARD_X_INDEX:
      case PanelLayoutTable._BOARD_Y_INDEX:
      case PanelLayoutTable._BOARD_ROTATION_INDEX:
      case PanelLayoutTable._BOARD_FLIP_INDEX:
      case PanelLayoutTable._BOARD_ZOFFSET_INDEX:
      {
        isEditable = true;
        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }

    return isEditable;
  }

  /**
   * Initialize the table with data from a new project
   * @param project Project data
   * @author Carli Connally
   */
  public void setData(Project project)
  {
    Assert.expect(project != null);

    _project = project;
    _boards = _project.getPanel().getBoards();

    //must delete previous rows and insert new rows to account for different number
    //of rows, otherwise you'll see the old rows at the bottom of the new table
//    fireTableRowsUpdated(0, _boards.size());
//    fireTableRowsDeleted(0, getRowCount());
//    fireTableRowsInserted(0, _boards.size());
    fireTableDataChanged();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setPanelBasedAlignmentMethodSelected(boolean isPanelBasedAlignmentMethodSelected)
  {
    _isPanelBasedAlignmentMethodSelected = isPanelBasedAlignmentMethodSelected;
  }

  /**
   * Overridden method.  Saves the data entered by the user in the table
   * to the project object.
   * @param object Object containing the data to be saved to the model
   * @param rowIndex Index of the row
   * @param columnIndex Index of the column
   * @author Carli Connally
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Board board = _boards.get(rowIndex);

    switch(columnIndex)
    {
      case PanelLayoutTable._BOARD_NAME_INDEX:
      {
        String newBoardName = (String)object;
        Panel panel = _project.getPanel();
        if (board.getName().equalsIgnoreCase(newBoardName))  // the name is the same, just ignore
          return;
        if (panel.isBoardNameValid(newBoardName) == false)
        {
          // explain why it's invalid
          String illegalBoardNameCharacters = panel.getBoardNameIllegalChars();

          LocalizedString message = new LocalizedString("MMGUI_BOARD_NAME_INVALID_OTHER_CHARS_KEY",
                                                        new Object[]{newBoardName, illegalBoardNameCharacters});

          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString(message),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.ERROR_MESSAGE);
          return;
        }
        if (_project.getPanel().isBoardNameDuplicate(newBoardName))
        {
          LocalizedString message = new LocalizedString("MMGUI_BOARD_NAME_DUPLICATE_KEY",
                                                        new Object[]{newBoardName});
          MessageDialog.showErrorDialog(_mainUI,
                                        StringLocalizer.keyToString(message),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);

          return;
        }

        try
        {
          _commandManager.execute(new BoardSetNameCommand(board, newBoardName));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI,
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      }
      case PanelLayoutTable._BOARD_X_INDEX:
      {
        //get the display units for conversion
        MathUtilEnum displayUnits = _project.getDisplayUnits();

        //get the x value entered and convert back to nanometers
        // double dXValue = Double.parseDouble((String) object);
        double dXValue = 0.0;
        if (object instanceof Number)
          dXValue = ((Number)object).doubleValue();
        else
          Assert.expect(false, "Wrong type of object: " + object);
        int xInNanoMeters = (int) MathUtil.convertUnits(dXValue, displayUnits, MathUtilEnum.NANOMETERS);
        
        if (MathUtil.fuzzyEquals(xInNanoMeters, board.getLowerLeftCoordinateRelativeToPanelInNanoMeters().getX(), MathUtil._UNIT_CONVERSION_NANOMETER_LIMIT))
          return;
        
        boolean isSurfaceMapRemove = false;
        boolean hasSurfaceMapPoints = false;
        if (_isEnablePsp)
        {
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_BOARD_SET_X_LOCATION_KEY"));
          
          if (hasPanelSurfaceMap())
          {
            isSurfaceMapRemove = removePanelBasedSurfaceMap(board,
                                                            StringLocalizer.keyToString("MMGUI_REMOVE_PSP_POINTS_WARNING_MESSAGE_KEY"));
            hasSurfaceMapPoints = true;
          }
          else if (hasBoardSurfaceMap(board))
          {
            isSurfaceMapRemove = removeBoardBasedSurfaceMap(board,
                                                            StringLocalizer.keyToString("MMGUI_REMOVE_PSP_POINTS_WARNING_MESSAGE_KEY"));
            hasSurfaceMapPoints = true;
          }
        }

        PanelCoordinate loc;
        if (hasSurfaceMapPoints && isSurfaceMapRemove == false)
        {
          loc = board.getLowerLeftCoordinateRelativeToPanelInNanoMeters();
        }
        else
        {
          loc = board.getLowerLeftCoordinateRelativeToPanelInNanoMeters();
          loc.setX(xInNanoMeters);
        }

        try
        {
          _commandManager.execute(new BoardSetLowerLeftCoordinateCommand(board, loc));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI,
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        if (_isEnablePsp)
          _commandManager.endCommandBlock();
        break;
      }
      case PanelLayoutTable._BOARD_Y_INDEX:
      {
        //get the display units for conversion
        MathUtilEnum displayUnits = _project.getDisplayUnits();

        //get the x value entered and convert back to nanometers
        //double dYValue = Double.parseDouble((String) object);
        double dYValue = 0.0;
        if (object instanceof Number)
          dYValue = ((Number)object).doubleValue();
        else
          Assert.expect(false, "Wrong type of object: " + object);
        int yInNanoMeters = (int) MathUtil.convertUnits(dYValue, displayUnits, MathUtilEnum.NANOMETERS);
        
        if (MathUtil.fuzzyEquals(yInNanoMeters, board.getLowerLeftCoordinateRelativeToPanelInNanoMeters().getY(), MathUtil._UNIT_CONVERSION_NANOMETER_LIMIT))
          return;
        
        boolean isSurfaceMapRemove = false;
        boolean hasSurfaceMapPoints = false;
        if (_isEnablePsp)
        {
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_BOARD_SET_Y_LOCATION_KEY"));
          
          if (hasPanelSurfaceMap())
          {
            isSurfaceMapRemove = removePanelBasedSurfaceMap(board,
                                                            StringLocalizer.keyToString("MMGUI_REMOVE_PSP_POINTS_WARNING_MESSAGE_KEY"));
            hasSurfaceMapPoints = true;
          }
          else if (hasBoardSurfaceMap(board))
          {
            isSurfaceMapRemove = removeBoardBasedSurfaceMap(board,
                                                            StringLocalizer.keyToString("MMGUI_REMOVE_PSP_POINTS_WARNING_MESSAGE_KEY"));
            hasSurfaceMapPoints = true;
          }
        }

        PanelCoordinate loc;
        if (hasSurfaceMapPoints && isSurfaceMapRemove == false)
        {
          loc = board.getLowerLeftCoordinateRelativeToPanelInNanoMeters();
        }
        else
        {
          loc = board.getLowerLeftCoordinateRelativeToPanelInNanoMeters();
          loc.setY(yInNanoMeters);
        }
        
        try
        {
          _commandManager.execute(new BoardSetLowerLeftCoordinateCommand(board, loc));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI,
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        if (_isEnablePsp)
          _commandManager.endCommandBlock();    
        break;
      }
      case PanelLayoutTable._BOARD_ROTATION_INDEX:
      {
        Integer rotation = new Integer((String) object);
        if(rotation == board.getDegreesRotationRetainingLowerLeftCorner())
          return;
        
        boolean isSurfaceMapRemove = false;
        boolean hasSurfaceMapPoints = false;
        if (_isEnablePsp)
        {
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_BOARD_SET_ROTATION_KEY"));
          
          if (hasPanelSurfaceMap())
          {
            isSurfaceMapRemove = removePanelBasedSurfaceMap(board,
                                                            StringLocalizer.keyToString("MMGUI_REMOVE_PSP_POINTS_WARNING_MESSAGE_KEY"));
            hasSurfaceMapPoints = true;
          }
          else if (hasBoardSurfaceMap(board))
          {
            isSurfaceMapRemove = removeBoardBasedSurfaceMap(board,
                                                            StringLocalizer.keyToString("MMGUI_REMOVE_PSP_POINTS_WARNING_MESSAGE_KEY"));
            hasSurfaceMapPoints = true;
          }
        }        
        
        //save a copy of the previous value, then set the new value in the project
        if (hasSurfaceMapPoints && isSurfaceMapRemove == false)
          rotation = new Integer(board.getDegreesRotationRetainingLowerLeftCorner());
        
        try
        {
          _commandManager.execute(new BoardSetRotationCommand(board, rotation.intValue()));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI,
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }        
        if (_isEnablePsp)
          _commandManager.endCommandBlock();        
        break;
      }
      case PanelLayoutTable._BOARD_FLIP_INDEX:
      {
        boolean isSurfaceMapRemove = false;
        boolean hasSurfaceMapPoints = false;
        if (_isEnablePsp)
        {
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_BOARD_FLIP_KEY"));
          
          if (hasPanelSurfaceMap())
          {
            isSurfaceMapRemove = removePanelBasedSurfaceMap(board,
                                                            StringLocalizer.keyToString("MMGUI_REMOVE_PSP_POINTS_WARNING_MESSAGE_KEY"));
            hasSurfaceMapPoints = true;
          }
          else if (hasBoardSurfaceMap(board))
          {
            isSurfaceMapRemove = removeBoardBasedSurfaceMap(board,
                                                            StringLocalizer.keyToString("MMGUI_REMOVE_PSP_POINTS_WARNING_MESSAGE_KEY"));
            hasSurfaceMapPoints = true;
          }
        }
        
        if (hasSurfaceMapPoints && isSurfaceMapRemove == false)
        {
          if (_isEnablePsp)
            _commandManager.endCommandBlock();
          return;
        }
        
        try
        {
          _commandManager.execute(new BoardFlipCommand(board));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI,
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        if (_isEnablePsp)
          _commandManager.endCommandBlock();
        break;
      }
	  case PanelLayoutTable._BOARD_ZOFFSET_INDEX:
      {
        //get the display units for conversion
        MathUtilEnum displayUnits = _project.getDisplayUnits();

        //get the z value entered and convert back to nanometers
        double dZValue = 0.0;
        if (object instanceof Number)
          dZValue = ((Number)object).doubleValue();
        else
          Assert.expect(false, "Wrong type of object: " + object);
        int zInNanoMeters = (int) MathUtil.convertUnits(dZValue, displayUnits, MathUtilEnum.NANOMETERS);

        double minSliceHeight = XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers();
        double maxSliceHeight = XrayTester.getMaximumSliceHeightFromNominalSliceInNanometers();
        
        double maxAllowableSliceHeight = XrayTester.getMaximumAllowableSliceHeightFromNominalSliceInNanometers(); // 500mils
        double minAllowableSliceHeight = XrayTester.getMinimumAllowableSliceHeightFromNominalSliceInNanometers(); // -480mils
        
        double boardThickness = _project.getPanel().getThicknessInNanometers();
        
        // The following checking is for XXL system only.
        if(XrayTester.isXXLBased()==true)
        {
          double btmGap = minSliceHeight + PanelHandler.getMaximumPanelThicknessInNanoMeters();
                       // (-13970000)    + 12700000 = -1270000
          double clippedMinSliceHeight =  minSliceHeight-btmGap+boardThickness;
          
          if(zInNanoMeters < clippedMinSliceHeight || 
             zInNanoMeters > maxAllowableSliceHeight-maxSliceHeight)
          {
            LocalizedString message = new LocalizedString("MMGUI_INVALID_ZOFFSET_RANGE_KEY",
                                                          new Object[]{MathUtil.convertUnits(zInNanoMeters, MathUtilEnum.NANOMETERS, displayUnits), displayUnits,
                                                            MathUtil.convertUnits(clippedMinSliceHeight, MathUtilEnum.NANOMETERS, displayUnits), displayUnits,
                                                            MathUtil.convertUnits(maxAllowableSliceHeight-maxSliceHeight, MathUtilEnum.NANOMETERS, displayUnits), displayUnits});
            MessageDialog.showErrorDialog(_mainUI,
                                          StringLocalizer.keyToString(message),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
            return;
          }
        }
        // The following checking is for standard system only.
        else
        {
          if(zInNanoMeters+minSliceHeight < minAllowableSliceHeight || 
             zInNanoMeters > maxAllowableSliceHeight-maxSliceHeight)
          {
            LocalizedString message = new LocalizedString("MMGUI_INVALID_ZOFFSET_RANGE_KEY",
                                                          new Object[]{MathUtil.convertUnits(zInNanoMeters, MathUtilEnum.NANOMETERS, displayUnits), displayUnits,
                                                            MathUtil.convertUnits(minAllowableSliceHeight-minSliceHeight, MathUtilEnum.NANOMETERS, displayUnits), displayUnits,
                                                            MathUtil.convertUnits(maxAllowableSliceHeight-maxSliceHeight, MathUtilEnum.NANOMETERS, displayUnits), displayUnits});
            MessageDialog.showErrorDialog(_mainUI,
                                          StringLocalizer.keyToString(message),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
            return;
          }
        }

        try
        {
          _commandManager.execute(new BoardSetZOffsetCommand(board.getBoardSettings(), zInNanoMeters));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI,
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }
  }

  /**
   * Get the board instance associated with the row index
   * @param rowIndex the index of the row
   * @author George A. David
   */
  public Board getBoard(int rowIndex)
  {
    Assert.expect(rowIndex >= 0);

    List<Board> boards = _project.getPanel().getBoards();
    Assert.expect(rowIndex < boards.size());

    return (Board)boards.get(rowIndex);
  }

  /**
   * @author Andy Mechtenberg
   */
  void setSelectedBoards(List<Board> selectedBoards)
  {
    Assert.expect(selectedBoards != null);
    _selectedBoards = selectedBoards;
  }

  /**
   * @author Andy Mechtenberg
   */
  int getIndexForBoard(Board board)
  {
    Assert.expect(board != null);
    return _boards.indexOf(board);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private boolean hasPanelSurfaceMap()
  {
    Assert.expect(_project != null);
    
    PanelSettings panelSettings = _project.getPanel().getPanelSettings();
    if ((panelSettings.isPanelBasedAlignment() || _isPanelBasedAlignmentMethodSelected) && _project.getPanel().hasPanelSurfaceMapSettings())
    {
      if (_project.getPanel().getPanelSurfaceMapSettings().getOpticalRegions().isEmpty() == false)
        return true;
      else
        return false;
    }
    return false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private boolean hasBoardSurfaceMap(Board board)
  {
    Assert.expect(board != null);
    
    if (board.hasBoardSurfaceMapSettings() && board.getBoardSurfaceMapSettings().getOpticalRegions().isEmpty() == false)
      return true;
    else
      return false;
  }

  /**
   * @author Cheah Lee Herng 
   */
  private boolean removePanelBasedSurfaceMap(Board board, 
                                             String dialogMessage)
  {
    Assert.expect(board != null);    
    Assert.expect(dialogMessage != null);
    
    boolean isSurfaceMapRemoved = false;    
    int status = JOptionPane.showConfirmDialog(_mainUI, 
                                               StringLocalizer.keyToString(dialogMessage),
                                               StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), 
                                               JOptionPane.YES_NO_OPTION, 
                                               JOptionPane.QUESTION_MESSAGE);

    if(status == JOptionPane.YES_OPTION)
    {
      PanelSurfaceMapSettings panelSurfaceMapSettings =  _project.getPanel().getPanelSurfaceMapSettings();              
      for (OpticalRegion panelOpticalRegion : panelSurfaceMapSettings.getOpticalRegions())
      {
        try
        {          
          _commandManager.execute(new SurfaceMapPanelDeleteOpticalRegionCommand(_project, panelSurfaceMapSettings, panelOpticalRegion));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
        }
      }
      isSurfaceMapRemoved = true;
    }
    return isSurfaceMapRemoved;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private boolean removeBoardBasedSurfaceMap(Board board,
                                             String dialogMessage)
  {
    Assert.expect(board != null);    
    Assert.expect(dialogMessage != null);
    
    boolean isSurfaceMapRemoved = false;    
    int status = JOptionPane.showConfirmDialog(_mainUI, 
                                               StringLocalizer.keyToString(dialogMessage),
                                               StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), 
                                               JOptionPane.YES_NO_OPTION, 
                                               JOptionPane.QUESTION_MESSAGE);
    if (status == JOptionPane.YES_OPTION)
    {
      for (OpticalRegion boardOpticalRegion : board.getBoardSurfaceMapSettings().getOpticalRegions())
      {
        try
        {
          _commandManager.execute(new SurfaceMapBoardDeleteOpticalRegionCommand(_project, board.getBoardSurfaceMapSettings(), boardOpticalRegion));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
        }
      }
      isSurfaceMapRemoved = true;
    }    
    return isSurfaceMapRemoved;
  }
}
