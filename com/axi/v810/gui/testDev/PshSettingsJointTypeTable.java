package com.axi.v810.gui.testDev;

import javax.swing.table.*;

import com.axi.guiUtil.*;

/**
 * @author Siew Yeng
 */
public class PshSettingsJointTypeTable extends JSortTable
{
  public static final int _JOINT_TYPE_NAME_INDEX = 0;
  public static final int _JOINT_TYPE_ENABLED_INDEX = 1;
  
  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _JOINT_TYPE_NAME_COLUMN_WIDTH_PERCENTAGE = .5;
  private static final double _JOINT_TYPE_ENABLED_COLUMN_WIDTH_PERCENTAGE = .5;
  
  private CheckCellEditor _checkEditor = new CheckCellEditor(getBackground(), getForeground());
  private CheckCellRenderer _checkRenderer = new CheckCellRenderer();
  
  /**
   * Default constructor.
   * @author Siew Yeng
   */
  public PshSettingsJointTypeTable()
  {
    // do nothing
  }
  
  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Siew Yeng
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int jointTypeNameColumnWidth = (int)(totalColumnWidth * _JOINT_TYPE_NAME_COLUMN_WIDTH_PERCENTAGE);
    int ruleEnabledColumnWidth = (int)(totalColumnWidth * _JOINT_TYPE_ENABLED_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_JOINT_TYPE_NAME_INDEX).setPreferredWidth(jointTypeNameColumnWidth);
    columnModel.getColumn(_JOINT_TYPE_ENABLED_INDEX).setPreferredWidth(ruleEnabledColumnWidth);
  }

  /**
   * @author Siew Yeng
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();
    
    columnModel.getColumn(_JOINT_TYPE_ENABLED_INDEX).setCellEditor(_checkEditor);
    columnModel.getColumn(_JOINT_TYPE_ENABLED_INDEX).setCellRenderer(_checkRenderer);
  }
  
  /**
   * @author Siew Yeng
   */
  void cancelCellEditing()
  {
    cancelCellEditingForColumn(_JOINT_TYPE_ENABLED_INDEX);
  }

  /**
   * @author Siew Yeng
   */
  void cancelCellEditingForColumn(int column)
  {
    TableCellEditor cellEditor = getColumnModel().getColumn(column).getCellEditor();
    if (cellEditor != null)
    {
      cellEditor.cancelCellEditing();
    }
  }
}
