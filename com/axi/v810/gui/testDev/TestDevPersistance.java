package com.axi.v810.gui.testDev;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;


/**
 * This class handles all the persisted UI components of the Test Development Environment, nothing project related should
 * be added to this class.
 *
 * @author Andy Mechtenberg
 * @author Erica Wheatcroft
 */
public class TestDevPersistance implements Serializable
{
  // verify cad settings
  private String _verifyCadCurrentView = StringLocalizer.keyToString("CAD_PACKAGE_VIEW_KEY");
  private int _verifyCadPackageViewSplitPaneDivider = -1;
  private int _verifyCadRegionViewSplitPaneDivider = -1;
  private int[] _verifyCadComponentTableColumnWidths = null;
  private int[] _verifyCadPackageTableColumnWidths = null;
  private int[] _verifyCadRegionTableColumnWidths = null;

  // components/fiducial tab settings
  private int _componentsTabDividerLocation = 300; // default

  private int _dividerLocation = 400; // default

  // modify subtypes settings
  private int _modifySubtypesDividerLocation = 300;
  private int _unTestableReasonsDividerLocation = 150; //XCR1374, KhangWah, 2011-09-19

  // tuner fields.
  private int _testInfoToThresholdDividerLocation = 320;//236;
  private int _thresholdDetailsAndDescriptionDividerLocation = -1;
  private java.util.List<Integer> _tableColumnSizes = new ArrayList<Integer>();
  private boolean _showDescriptionsOn = true;

  // Review Defects settings
  private int _reviewDefectsResizeDivider = -1;
  private int _failingPinsResizeDivider = -1;
  private int _passingPinsResizeDivider = -1;

  // Review Measurement Settings
  private int _reviewMeasurementsResizeDivider = -1;

  private transient ProjectPersistance _currentLoadedProjectPersistance = null;
  
   // show selected optical region
  private boolean _showSelectedComboBoxRegion = false;
  private int _surfaceMappingDetailResizeDivider = -1;
  private int _surfaceMappingRegionResizeDivider = -1;
  
  // scan path divider location
  private int _scanPathReviewResizeDivider = -1;

  private int _spliterHeight;
  private int _spliterWidth;
  private int _spliterLocation;

  //XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)- by Anthony Fong
  private int _recipeSummaryMainSplitterHeight;
  private int _recipeSummaryMainSplitterWidth;
  private int _recipeSummaryMainSplitterDividerLocation;

  private int _panelSetupMainSplitterHeight;
  private int _panelSetupMainSplitterWidth;
  private int _panelSetupMainSplitterDividerLocation;

  private int _alignmentSetupMainSplitterHeight;
  private int _alignmentSetupMainSplitterWidth;
  private int _alignmentSetupMainSplitterDividerLocation;

  private int _PSPSurfaceMapMainSplitterHeight;
  private int _PSPSurfaceMapMainSplitterWidth;
  private int _PSPSurfaceMapMainSplitterDividerLocation;

  private int _scanPathReviewMainSplitterHeight;
  private int _scanPathReviewMainSplitterWidth;
  private int _scanPathReviewMainSplitterDividerLocation;
  
  private int _adjustCADMainSplitterHeight;
  private int _adjustCADMainSplitterWidth;
  private int _adjustCADMainSplitterDividerLocation;

  private int _modifySubtypesMainSplitterHeight;
  private int _modifySubtypesMainSplitterWidth;
  private int _modifySubtypesMainSplitterDividerLocation;

  private int _initialTuningMainSplitterHeight;
  private int _initialTuningMainSplitterWidth;
  private int _initialTuningMainSplitterDividerLocation;

  private int _fineTuningMainSplitterHeight;
  private int _fineTuningMainSplitterWidth;
  private int _fineTuningMainSplitterDividerLocation;
  
  //XCR-3837 Display untestable area on CAD
  private boolean _displayUntestableArea = false;

  /**
   * @author Erica Wheatcroft
   */
  public void notifyOfNewProject(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null, "Project Name is null");

    if(_currentLoadedProjectPersistance == null)
    {
      _currentLoadedProjectPersistance = ProjectPersistance.readSettings(projectName);
    }
    else
    {
      _currentLoadedProjectPersistance = new ProjectPersistance();
      _currentLoadedProjectPersistance = _currentLoadedProjectPersistance.readSettings(projectName);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void notifyOfProjectClose(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);
    if(_currentLoadedProjectPersistance != null)
    {
      _currentLoadedProjectPersistance.writeSettings(projectName);
    }
    _currentLoadedProjectPersistance = null;
  }

  /**
   * @author Erica Wheatcroft
   */
  public ProjectPersistance getProjectPersistance()
  {
    Assert.expect(_currentLoadedProjectPersistance != null);
    return _currentLoadedProjectPersistance;
  }

  public void setFineTuningSpliterHeight(int spliterHeight)
  {
    _spliterHeight = spliterHeight;
  }

  public void setFineTuningSpliterWidth(int spliterWidth)
  {
    _spliterWidth = spliterWidth;
  }

  public void setFineTuningSpliterLocation(int spliterLocation)
  {
    _spliterLocation = spliterLocation;
  }

  public int getFineTuningSpliterHeight()
  {
    return _spliterHeight;
  }

  public int getFineTuningSpliterWidth()
  {
    return _spliterWidth;
  }

  public int getFineTuningSpliterLocation()
  {
    return _spliterLocation;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getTestInfoToThresholdDividerLocation()
  {
    return _testInfoToThresholdDividerLocation;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTestInfoToThresholdDividerLocation(int dividerLocation)
  {
    _testInfoToThresholdDividerLocation = dividerLocation;
  }

  /**
   * @author Rex Shang
   */
  public int getThresholdDetailsAndDescriptionDividerLocation()
  {
    return _thresholdDetailsAndDescriptionDividerLocation;
  }

  /**
   * @author Rex Shang
   */
  public void setThresholdDetailsAndDescriptionDividerLocation(int dividerLocation)
  {
    _thresholdDetailsAndDescriptionDividerLocation = dividerLocation;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getTableColumnWidth(int column)
  {
    Integer size = (Integer)_tableColumnSizes.get(column);
    Assert.expect(size != null);
    return size.intValue();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTableColumnWidth(int column, int width)
  {
    Integer size = new Integer(width);
    _tableColumnSizes.set(column, size);
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean getShowDescriptionsOn()
  {
    return _showDescriptionsOn;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setShowDescriptionsOn(boolean showDescriptions)
  {
    _showDescriptionsOn = showDescriptions;
  }

  /**
   * set Review Defects divider between data selection and tables
   * @param location int
   * @author George Booth
   */
  public void setReviewDefectsResizeDivider(int location)
  {
    _reviewDefectsResizeDivider = location;
  }

  /**
   * get Review Defects divider between data selection and tables
   * @return location
   * @author George Booth
   */
  public int getReviewDefectsResizeDivider()
  {
    return _reviewDefectsResizeDivider;
  }

  /**
   * set Review Defects divider for passing defects
   * @param location int
   * @author George Booth
   */
  public void setPassingPinsResizeDivider(int location)
  {
    _passingPinsResizeDivider = location;
  }

  /**
   * get Review Defects divider for passing defects
   * @return location
   * @author George Booth
   */
  public int getPassingPinsResizeDivider()
  {
    return _passingPinsResizeDivider;
  }

  /**
   * set Review Defects divider for failing defects
   * @param location int
   * @author George Booth
   */
  public void setFailingPinsResizeDivider(int location)
  {
    _failingPinsResizeDivider = location;
  }

  /**
   * get Review Defects divider for failing defects
   * @return location
   * @author George Booth
   */
  public int getFailingPinsResizeDivider()
  {
    return _failingPinsResizeDivider;
  }

  /**
   * @author Rex Shang
   */
  public int getReviewMeasurementsResizeDivider()
  {
    return _reviewMeasurementsResizeDivider;
  }

  /**
   * @author Rex Shang
   */
  public void setReviewMeasurementsResizeDivider(int location)
  {
    _reviewMeasurementsResizeDivider = location;
  }

  /**
   * @author Andy Mechtenberg
   */
  public TestDevPersistance()
  {
    // default settings for the alg tuner
    _tableColumnSizes.add(new Integer(200)); // Name column default size
    _tableColumnSizes.add(new Integer( -1)); //  Value column default size
    _tableColumnSizes.add(new Integer( -1)); //  Default column default size
    _tableColumnSizes.add(new Integer( -1)); //  Min column default size
    _tableColumnSizes.add(new Integer( -1)); //  Max column default size
  }

  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  public TestDevPersistance readSettings()
  {
    TestDevPersistance persistance = null;
    try
    {
      if (FileUtilAxi.exists(FileName.getTestDevPersistFullPath()))
        persistance = (TestDevPersistance) FileUtilAxi.loadObjectFromSerializedFile(FileName.getTestDevPersistFullPath());
      else
        persistance = new TestDevPersistance();
    }
    catch(DatastoreException de)
    {
      // do nothing..
      persistance = new TestDevPersistance();
    }

    return persistance;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void writeSettings(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);
    FileUtilAxi.saveObjectToSerializedFile(this, FileName.getTestDevPersistFullPath());
    if (_currentLoadedProjectPersistance != null)
    {
      _currentLoadedProjectPersistance.writeSettings(projectName);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public void writeSettings() throws DatastoreException
  {
    FileUtilAxi.saveObjectToSerializedFile(this, FileName.getTestDevPersistFullPath());
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getDividerLocation()
  {
    return _dividerLocation;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setDividerLocation(int dividerLocation)
  {
    _dividerLocation = dividerLocation;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getModifySubtypesDividerLocation()
  {
    return _modifySubtypesDividerLocation;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setModifySubtypesDividerLocation(int dividerLocation)
  {
    _modifySubtypesDividerLocation = dividerLocation;
  }
  
  /**
   * @author Chnee Khang Wah, XCR1374, 2011-09-19
   */
  public int getUnTestableReasonsDividerLocation()
  {
    return _unTestableReasonsDividerLocation;
  }

  /**
   * @author Chnee Khang Wah, XCR1374, 2011-09-19
   */
  public void setUnTestableReasonsDividerLocation(int dividerLocation)
  {
    _unTestableReasonsDividerLocation = dividerLocation;
  }

  /**
   * @author Laura Cormos
   */
  public int getComponentsTabDividerLocation()
  {
    return _componentsTabDividerLocation;
  }

    /**
   * @author Laura Cormos
   */
  public void setComponentsTabDividerLocation(int componentsTabDividerLocation)
  {
    _componentsTabDividerLocation = componentsTabDividerLocation;
  }

  /**
   * @author George Booth
   */
  public String getVerifyCadPackageCurrentView()
  {
    return _verifyCadCurrentView;
  }

  /**
   * @author George Booth
   */
  public void setVerifyCadPackageCurrentView(String currentView)
  {
    Assert.expect(currentView != null, "Verify Cad currentView is null");
    _verifyCadCurrentView = currentView;
  }

  /**
   * @author George Booth
   */
  public int getVerifyCadPackageViewDividerLocation()
  {
    return _verifyCadPackageViewSplitPaneDivider;
  }

  /**
   * @author George Booth
   */
  public void setVerifyCadPackageViewDividerLocation(int divider)
  {
    _verifyCadPackageViewSplitPaneDivider = divider;
  }

  /**
   * @author George Booth
   */
  public int getVerifyCadRegionViewDividerLocation()
  {
    return _verifyCadRegionViewSplitPaneDivider;
  }

  /**
   * @author George Booth
   */
  public void setVerifyCadRegionViewDividerLocation(int divider)
  {
    _verifyCadRegionViewSplitPaneDivider = divider;
  }

  /**
   * @author George Booth
   */
  public int[] getVerifyCadComponentTableColumnWidths()
  {
    return _verifyCadComponentTableColumnWidths;
  }

  /**
   * @author George Booth
   */
  public void setVerifyCadComponentTableColumnWidths(int[] widths)
  {
    _verifyCadComponentTableColumnWidths = widths;
  }

  /**
   * @author George Booth
   */
  public int[] getVerifyCadPackageTableColumnWidths()
  {
    return _verifyCadPackageTableColumnWidths;
  }

  /**
   * @author George Booth
   */
  public void setVerifyCadPackageTableColumnWidths(int[] widths)
  {
    _verifyCadPackageTableColumnWidths = widths;
  }

  /**
   * @author George Booth
   */
  public int[] getVerifyCadRegionTableColumnWidths()
  {
    return _verifyCadRegionTableColumnWidths;
  }

  /**
   * @author George Booth
   */
  public void setVerifyCadRegionTableColumnWidths(int[] widths)
  {
    _verifyCadRegionTableColumnWidths = widths;
  }

   /**
   * @author Jack Hwee
   */
  public void setShowSelectedComboBoxRegion(boolean showSelectedComboBoxRegion)
  {
    _showSelectedComboBoxRegion = showSelectedComboBoxRegion;
  }
  
   /**
   * @author Jack Hwee
   */
  public boolean getShowSelectedComboBoxRegion()
  {
    return _showSelectedComboBoxRegion;
  }
  
   /**
   * set surface map divider between data selection and tables
   * @param location int
   * @author Jack Hwee
   */
  public void setSurfaceMapDetailResizeDivider(int location)
  {
    _surfaceMappingDetailResizeDivider = location;
  }

  /**
   * get surface map divider between data selection and tables
   * @return location
   * @author Jack Hwee
   */
  public int getSurfaceMapDetailResizeDivider()
  {
    return _surfaceMappingDetailResizeDivider;
  }
  
  
     /**
   * set scan path divider between data selection and tables
   * @param location int
   * @author Kee Chin Seong
   */
  public void setScanPathReviewResizeDivider(int location)
  {
    _scanPathReviewResizeDivider = location;
  }

  /**
   * get scan path divider between data selection and tables
   * @return location
   * @author Kee Chin Seong
   */
  public int getScanPathReviewResizeDivider()
  {
    return _scanPathReviewResizeDivider;
  }
  
   /**
   * set surface map divider between data selection and tables
   * @param location int
   * @author Jack Hwee
   */
  public void setSurfaceMapRegionResizeDivider(int location)
  {
    _surfaceMappingRegionResizeDivider = location;
  }

  /**
   * get surface map divider between data selection and tables
   * @return location
   * @author Jack Hwee
   */
  public int getSurfaceMapRegionResizeDivider()
  {
    return _surfaceMappingRegionResizeDivider;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setRecipeSummaryMainSplitterHeight(int spliterHeight)
  {
    _recipeSummaryMainSplitterHeight = spliterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setRecipeSummaryMainSplitterWidth(int spliterWidth)
  {
    _recipeSummaryMainSplitterWidth = spliterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setRecipeSummaryMainSplitterDividerLocation(int spliterDividerLocation)
  {
    _recipeSummaryMainSplitterDividerLocation = spliterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getRecipeSummaryMainSplitterHeight()
  {
    return _recipeSummaryMainSplitterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getRecipeSummaryMainSplitterWidth()
  {
    return _recipeSummaryMainSplitterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getRecipeSummaryMainSplitterDividerLocation()
  {
    return _recipeSummaryMainSplitterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setPanelSetupMainSplitterHeight(int spliterHeight)
  {
    _panelSetupMainSplitterHeight = spliterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setPanelSetupMainSplitterWidth(int spliterWidth)
  {
    _panelSetupMainSplitterWidth = spliterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setPanelSetupMainSplitterDividerLocation(int spliterDividerLocation)
  {
    _panelSetupMainSplitterDividerLocation = spliterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getPanelSetupMainSplitterHeight()
  {
    return _panelSetupMainSplitterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getPanelSetupMainSplitterWidth()
  {
    return _panelSetupMainSplitterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getPanelSetupMainSplitterDividerLocation()
  {
    return _panelSetupMainSplitterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setAlignmentSetupMainSplitterHeight(int spliterHeight)
  {
    _alignmentSetupMainSplitterHeight = spliterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setAlignmentSetupMainSplitterWidth(int spliterWidth)
  {
    _alignmentSetupMainSplitterWidth = spliterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setAlignmentSetupMainSplitterDividerLocation(int spliterDividerLocation)
  {
    _alignmentSetupMainSplitterDividerLocation = spliterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getAlignmentSetupMainSplitterHeight()
  {
    return _alignmentSetupMainSplitterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getAlignmentSetupMainSplitterWidth()
  {
    return _alignmentSetupMainSplitterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getAlignmentSetupMainSplitterDividerLocation()
  {
    return _alignmentSetupMainSplitterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setPSPSurfaceMapMainSplitterHeight(int spliterHeight)
  {
    _PSPSurfaceMapMainSplitterHeight = spliterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setPSPSurfaceMapMainSplitterWidth(int spliterWidth)
  {
    _PSPSurfaceMapMainSplitterWidth = spliterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setPSPSurfaceMapMainSplitterDividerLocation(int spliterDividerLocation)
  {
    _PSPSurfaceMapMainSplitterDividerLocation = spliterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getPSPSurfaceMapMainSplitterHeight()
  {
    return _PSPSurfaceMapMainSplitterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getPSPSurfaceMapMainSplitterWidth()
  {
    return _PSPSurfaceMapMainSplitterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getPSPSurfaceMapMainSplitterDividerLocation()
  {
    return _PSPSurfaceMapMainSplitterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setScanPathReviewMainSplitterHeight(int spliterHeight)
  {
    _scanPathReviewMainSplitterHeight = spliterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setScanPathReviewMainSplitterWidth(int spliterWidth)
  {
    _scanPathReviewMainSplitterWidth = spliterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setScanPathReviewMainSplitterDividerLocation(int spliterDividerLocation)
  {
    _scanPathReviewMainSplitterDividerLocation = spliterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getScanPathReviewMainSplitterHeight()
  {
    return _scanPathReviewMainSplitterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getScanPathReviewMainSplitterWidth()
  {
    return _scanPathReviewMainSplitterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getScanPathReviewMainSplitterDividerLocation()
  {
    return _scanPathReviewMainSplitterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setAdjustCADMainSplitterHeight(int spliterHeight)
  {
    _adjustCADMainSplitterHeight = spliterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setAdjustCADMainSplitterWidth(int spliterWidth)
  {
    _adjustCADMainSplitterWidth = spliterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setAdjustCADMainSplitterDividerLocation(int spliterDividerLocation)
  {
    _adjustCADMainSplitterDividerLocation = spliterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getAdjustCADMainSplitterHeight()
  {
    return _adjustCADMainSplitterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getAdjustCADMainSplitterWidth()
  {
    return _adjustCADMainSplitterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getAdjustCADMainSplitterDividerLocation()
  {
    return _adjustCADMainSplitterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setModifySubtypesMainSplitterHeight(int spliterHeight)
  {
    _modifySubtypesMainSplitterHeight = spliterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setModifySubtypesMainSplitterWidth(int spliterWidth)
  {
    _modifySubtypesMainSplitterWidth = spliterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setModifySubtypesMainSplitterDividerLocation(int spliterDividerLocation)
  {
    _modifySubtypesMainSplitterDividerLocation = spliterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getModifySubtypesMainSplitterHeight()
  {
    return _modifySubtypesMainSplitterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getModifySubtypesMainSplitterWidth()
  {
    return _modifySubtypesMainSplitterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getModifySubtypesMainSplitterDividerLocation()
  {
    return _modifySubtypesMainSplitterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setInitialTuningMainSplitterHeight(int spliterHeight)
  {
    _initialTuningMainSplitterHeight = spliterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setInitialTuningMainSplitterWidth(int spliterWidth)
  {
    _initialTuningMainSplitterWidth = spliterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setInitialTuningMainSplitterDividerLocation(int spliterDividerLocation)
  {
    _initialTuningMainSplitterDividerLocation = spliterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getInitialTuningMainSplitterHeight()
  {
    return _initialTuningMainSplitterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getInitialTuningMainSplitterWidth()
  {
    return _initialTuningMainSplitterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getInitialTuningMainSplitterDividerLocation()
  {
    return _initialTuningMainSplitterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setFineTuningMainSplitterHeight(int spliterHeight)
  {
    _fineTuningMainSplitterHeight = spliterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setFineTuningMainSplitterWidth(int spliterWidth)
  {
    _fineTuningMainSplitterWidth = spliterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void setFineTuningMainSplitterDividerLocation(int spliterDividerLocation)
  {
    _fineTuningMainSplitterDividerLocation = spliterDividerLocation;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getFineTuningMainSplitterHeight()
  {
    return _fineTuningMainSplitterHeight;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getFineTuningMainSplitterWidth()
  {
    return _fineTuningMainSplitterWidth;
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getFineTuningMainSplitterDividerLocation()
  {
    return _fineTuningMainSplitterDividerLocation;
  }
  
  /**
   * @author Janan Wong 
   * XCR-3837: Display untestable Area on virtual live CAD
   */
  public boolean displayUntestableArea()
  {
    return _displayUntestableArea;
  }

  /**
   * @author Janan Wong
   * XCR-3837: Display untestable Area on virtual live CAD
   */
  public void setDisplayUntestableArea(boolean displayUntestableArea)
  {
    _displayUntestableArea = displayUntestableArea;
  }
}
