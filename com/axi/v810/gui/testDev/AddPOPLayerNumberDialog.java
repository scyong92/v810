package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * @author Khaw Chek Hau
 * POP Development
 */
class AddPOPLayerNumberDialog extends EscapeDialog
{
  private Project _project = null;
  private FocusAdapter _textFieldFocusAdapter;
  private JPanel _mainPanel = new JPanel();
  private JLabel _selectBoardLabel = new JLabel();
  private JLabel _layerNumberLabel = new JLabel();
  private NumericRangePlainDocument _layerNumberDocument = new NumericRangePlainDocument();
  private NumericTextField _layerNumberTextField = new NumericTextField();
  private JPanel _buttonsPanel = new JPanel();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private JPanel _selectionsPanel = new JPanel();
  private Border _empty5Border;
  private BorderLayout _mainBorderLayout = new BorderLayout();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private Border _empty10Border;
  private PairLayout _selectionsPanelPairLayout = new PairLayout(10, 10, false);
  private JComboBox _boardComboBox = new JComboBox();
  private JPanel _innerButtonsPanel = new JPanel();
  private GridLayout _innerButtonsPanelGridLayout = new GridLayout();
  private FlowLayout _buttonsPanelFlowLayout = new FlowLayout();
  private boolean _okButtonPerformed = false;
  private int _layerNumber = 0;

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public AddPOPLayerNumberDialog(Frame parent, Project project)
  {
    super(parent, StringLocalizer.keyToString("CAD_ADD_POP_DIALOG_TITLE_KEY"), true);
    Assert.expect(parent != null);
    Assert.expect(project != null);

    _project = project;
    jbInit();
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void clear()
  {
    _project = null;
    _boardComboBox.removeAllItems();
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void jbInit()
  {
    _textFieldFocusAdapter = new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        JTextField sourceTextField = (JTextField)e.getSource();
        sourceTextField.selectAll();
      }
      public void focusLost(FocusEvent e)
      {
        NumericTextField sourceTextField = (NumericTextField)e.getSource();
        handleTextFieldData(sourceTextField);
      }
    };

    _empty5Border = BorderFactory.createEmptyBorder(5,5,5,5);
    _empty10Border = BorderFactory.createEmptyBorder(10,10,10,10);
    _mainPanel.setBorder(_empty10Border);
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _selectBoardLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_SELECT_BOARD_TYPE_PROMPT_KEY") + ":  ");
    _layerNumberLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_TOTAL_ADDITIONAL_POP_LAYERS_TO_BE_ADDED_PROMPT_KEY") + ":  "); 
    _buttonsPanel.setLayout(_buttonsPanelFlowLayout);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed();
      }
    });
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed();
      }
    });
    _selectionsPanel.setLayout(_selectionsPanelPairLayout);
    _selectionsPanel.setBorder(_empty5Border);
    getContentPane().setLayout(_mainBorderLayout);
    _innerButtonsPanel.setLayout(_innerButtonsPanelGridLayout);
    _innerButtonsPanelGridLayout.setHgap(10);
    _buttonsPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _buttonsPanelFlowLayout.setVgap(0);
    _mainPanelBorderLayout.setVgap(10);
    _buttonsPanel.add(_innerButtonsPanel, null);
    _innerButtonsPanel.add(_okButton, null);
    _innerButtonsPanel.add(_cancelButton, null);
    if (_project.getPanel().getBoardTypes().size() > 1) // if more than 1 use a combo box
    {
      _selectionsPanel.add(_selectBoardLabel);
      _selectionsPanel.add(_boardComboBox, null);
    }
    _selectionsPanel.add(_layerNumberLabel);
    _selectionsPanel.add(_layerNumberTextField);
    
    _mainPanel.add(_buttonsPanel, BorderLayout.SOUTH);
    _mainPanel.add(_selectionsPanel, BorderLayout.CENTER);
    _layerNumberTextField.setDocument(_layerNumberDocument);
    _layerNumberTextField.addFocusListener(_textFieldFocusAdapter);
    getContentPane().add(_mainPanel, BorderLayout.CENTER);
    getRootPane().setDefaultButton(_okButton);
    _layerNumberTextField.setColumns(6);
    _layerNumberDocument.setRange(new IntegerRange(0, Integer.MAX_VALUE));
    _layerNumberTextField.setValue(new Integer(1));

    populateBoardTypes();
    pack();
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void populateBoardTypes()
  {
    java.util.List<BoardType> boardTypes = _project.getPanel().getBoardTypes();
    for(BoardType boardType : boardTypes)
    {
      _boardComboBox.addItem(boardType);
    }
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void handleTextFieldData(NumericTextField numericTextField)
  {
    Assert.expect(numericTextField != null);

    int layerNumber;
    try
    {
      layerNumber = numericTextField.getNumberValue().intValue();
    }
    catch (ParseException ex)
    {
      // this typically happens when the user deletes all characters from the field -- same as zero
      layerNumber = 1;
    }
    numericTextField.setValue(layerNumber);
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void okButton_actionPerformed()
  { 
    try
    {      
      int layerNumber = _layerNumberTextField.getNumberValue().intValue();
      
      if (layerNumber < 1 || layerNumber >= POPLayerIdEnum.getOrderedLayerIdList().size())
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      StringLocalizer.keyToString(new LocalizedString("CAD_INCORRECT_LAYER_NUMBER_ERROR_MESSAGE_KEY", 
                                                                  new Object[]{String.valueOf(POPLayerIdEnum.getOrderedLayerIdList().size() - 1)})),
                                      StringLocalizer.keyToString("CAD_INCORRECT_LAYER_NUMBER_ERROR_TITLE_KEY"),
                                      true);

        return;
      }
      setLayerNumber(layerNumber);
      setOkButtonPerformed(true);
    }
    catch (ParseException ex)
    {
      Assert.expect(false, ex.getMessage());
    }
    
    dispose();
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void cancelButton_actionPerformed()
  {
    dispose();
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public boolean isOkButtonPerformed()
  {
    return _okButtonPerformed;
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void setOkButtonPerformed(boolean okButtonPerformed)
  {
    _okButtonPerformed = okButtonPerformed;
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void setLayerNumber(int layerNumber)
  {
    _layerNumber = layerNumber;
  }
    
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public int getPOPLayerNumber()
  {
    return _layerNumber;
  }
}
