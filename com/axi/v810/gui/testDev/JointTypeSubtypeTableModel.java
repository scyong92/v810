package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;
import com.axi.v810.business.testProgram.*;

/**
 * This class is the data model for the package table for the Groupings panel and it also
 * converts to the component table model in the groupings panel
 * @author Andy Mechtenberg
 */
public class JointTypeSubtypeTableModel extends DefaultSortTableModel
{
  private List<Subtype> _subtypes;

  private final String[] _familySubtypeTableColumns = {StringLocalizer.keyToString("ATGUI_FAMILY_KEY"),
                                                   StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY")};

  /**
   * @author Andy Mechtenberg
   */
  public JointTypeSubtypeTableModel(List<Subtype> untunedSubtypes)
  {
    Assert.expect(untunedSubtypes != null);
    _subtypes = untunedSubtypes;
    Collections.sort(_subtypes, new SubtypeJointTypeOrNameComparator(true, true));
  }

  void clear()
  {
    if (_subtypes != null)
      _subtypes.clear();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setData(List<Subtype> untunedSubtypes)
  {
    Assert.expect(untunedSubtypes != null);
    _subtypes = untunedSubtypes;
    Collections.sort(_subtypes, new SubtypeJointTypeOrNameComparator(true, true));
    fireTableDataChanged();
  }


  /**
   * @author Andy Mechtenberg
   */
  public void sortColumn(int column, boolean ascending)
  {
    if (column == 0)
      Collections.sort(_subtypes, new SubtypeJointTypeOrNameComparator(true, ascending));
    else
      Collections.sort(_subtypes, new SubtypeJointTypeOrNameComparator(false, ascending));
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getColumnCount()
  {
    return _familySubtypeTableColumns.length;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getRowCount()
  {
    if(_subtypes == null)
      return 0;
    else
      return _subtypes.size();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return(String)_familySubtypeTableColumns[columnIndex];
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Subtype subtype = _subtypes.get(rowIndex);
    if(columnIndex == 0)
      return subtype.getJointTypeEnum();
    else
    {
      return subtype.getShortName();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    // do nothing - nothing is editable
  }

}
