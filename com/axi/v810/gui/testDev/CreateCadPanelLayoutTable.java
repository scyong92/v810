package com.axi.v810.gui.testDev;

import java.awt.Component;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.Project;

/**
 *
 * @author chin-seong.kee
 */
public class CreateCadPanelLayoutTable
{
  private PanelLayoutTable _panelLayoutTable = null;
  private PanelLayoutTableModel _panelLayoutTableModel = null;
  
  public CreateCadPanelLayoutTable()
  {
    _panelLayoutTable = new PanelLayoutTable();
    _panelLayoutTableModel = new PanelLayoutTableModel();
    
    _panelLayoutTable.setModel(_panelLayoutTableModel);
    _panelLayoutTable.setEditorsAndRenderers();
    _panelLayoutTable.setPreferredColumnWidths();
  }
  
  public void setPreferredColumnWidths()
  {
    _panelLayoutTable.setPreferredColumnWidths();
  }

  /**
   * @author chin-seong, Kee
   */
  public void setEditorsAndRenderers()
  {
    _panelLayoutTable.setEditorsAndRenderers();
  }


  /**
   * @author Kee Chin Seong
   * @param decimalPlaces 
   */
  public void setDecimalPlacesToDisplay(int decimalPlaces)
  {
    _panelLayoutTable.setDecimalPlacesToDisplay(decimalPlaces);
  }

  /**
   * @return a list of selected boards.
   * @author Chin-Seong, Kee
   */
  public java.util.List<Board> getSelectedBoards()
  {
    return _panelLayoutTable.getSelectedBoards();
  }
  
  /**
   * @return a Selection Model.
   * @author Chin-Seong, Kee
   */
  public ListSelectionModel getSelectionModel()
  {
    return _panelLayoutTable.getSelectionModel();
  }
    /**
   * Initialize the table with data from a new project
   * @param project Project data
   * @author Kee Chin Seong
   */
  public void setData(Project project)
  {
    _panelLayoutTableModel.setData(project);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setPanelBasedAlignmentMethodSelected(boolean isPanelBasedAlignmentMethodSelected)
  {
    _panelLayoutTableModel.setPanelBasedAlignmentMethodSelected(isPanelBasedAlignmentMethodSelected);
  }

  /**
   * @author Kee Chin Seong
   */
  public Board getBoard(int rowIndex)
  {
    return _panelLayoutTableModel.getBoard(rowIndex);
  }

  /**
   * @author Kee Chin Seong
   */
  public void setSelectedBoards(List<Board> selectedBoards)
  {
    _panelLayoutTableModel.setSelectedBoards(selectedBoards);
  }

  /**
   * @author Kee Chin Seong
   */
  public int getIndexForBoard(Board board)
  {
    return _panelLayoutTableModel.getIndexForBoard(board);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public PanelLayoutTable getTable()
  {
    return _panelLayoutTable;
  }
  
   /**
   * @author Kee Chin Seong
   */
  public void setName(String tableName)
  {
    _panelLayoutTable.setName(tableName);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public PanelLayoutTableModel getTableModel()
  {
    return _panelLayoutTableModel;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void fireTableDataChanged()
  {
    _panelLayoutTableModel.fireTableDataChanged();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void fireTableRowsUpdated(int row, int lastRow)
  {
    _panelLayoutTableModel.fireTableRowsUpdated(row, lastRow);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void addRowSelectionInterval(int row, int lastRow)
  {
    _panelLayoutTable.addRowSelectionInterval(row, lastRow);
  }
  
   /*
   * @author Kee Chin Seong
   */
  public void clearSelection()
  {
    _panelLayoutTable.clearSelection();
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void cancelCellEditing()
  {
    _panelLayoutTable.getCellEditor().cancelCellEditing();
  }

  /*
   * @author Kee Chin Seong
   */
  public boolean isEditing()
  {
    return _panelLayoutTable.isEditing();
  }
  
}
