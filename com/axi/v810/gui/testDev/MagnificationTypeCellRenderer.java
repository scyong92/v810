package com.axi.v810.gui.testDev;

import javax.swing.*;
import javax.swing.table.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Chong, Wei Chin
 */
public class MagnificationTypeCellRenderer extends DefaultTableCellRenderer
{
  public MagnificationTypeCellRenderer()
  {
    // do nothing
  }

  /**
   * param table
   * @param value
   * @param isSelected
   * @param hasFocus
   * @param row
   * @param column
   * @return
   * 
   * @author Chong, Wei Chin
   */
  public java.awt.Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    if (value == null)
    {
      return null;
    }
    java.awt.Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
    final JLabel label = (JLabel)renderer;

    Subtype selectedSubtype = null;
    SubtypeAdvanceSettingsTable subtypeAdvanceSettingsTable = null;

    MagnificationTypeEnum strValue = (MagnificationTypeEnum) value;

    if (table instanceof SubtypeAdvanceSettingsTable)
    {
      subtypeAdvanceSettingsTable = (SubtypeAdvanceSettingsTable) table;
      selectedSubtype = subtypeAdvanceSettingsTable.getTableModel().getSubtypeAt(row);
    }

    label.setText(strValue.toString());
    
    if(selectedSubtype.getSubtypeAdvanceSettings().canUseVariableMagnification())
      label.setEnabled(true);
    else
      label.setEnabled(false);

    return label;
  }
}