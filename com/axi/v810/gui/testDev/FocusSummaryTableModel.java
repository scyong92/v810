package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.*;

public class FocusSummaryTableModel extends DefaultSortTableModel
{
  private Project _project;
  private java.awt.Component _parent;
  private List<FocusReconstructionRegionWrapper> _reconstructionRegions = new ArrayList<FocusReconstructionRegionWrapper>();  // List of ReconstructionRegion objects

  private final String[] _tableColumns = {StringLocalizer.keyToString("GUI_FOCUS_IMAGE_TABLE_HEADER_KEY"),
                                          StringLocalizer.keyToString("GUI_FOCUS_FOCUS_METHOD_TABLE_HEADER_KEY"),
                                          StringLocalizer.keyToString("GUI_FOCUS_RETEST_TABLE_HEADER_KEY"),
                                          StringLocalizer.keyToString("GUI_FOCUS_NOTEST_TABLE_HEADER_KEY")};

  private final String _TO_BE_FIXED = StringLocalizer.keyToString("GUI_FOCUS_TO_BE_FIXED_KEY");
  private final String _DEFAULT = StringLocalizer.keyToString("GUI_FOCUS_DEFAULT_FOCUS_KEY");
  private final String _CUSTOM_FOCUS = StringLocalizer.keyToString("GUI_FOCUS_CUSTOM_FOCUS_KEY");

  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;


  /**
   * @author Andy Mechtenberg
   */
  FocusSummaryTableModel(Project project, java.awt.Component parent)
  {
    Assert.expect(project != null);
    Assert.expect(parent != null);

    _project = project;
    _parent = parent;

    TestProgram testProgram = MainMenuGui.getInstance().getTestProgramWithReason(StringLocalizer.keyToString("GUI_FOCUS_NEED_TEST_PROGRAM_KEY"),
         StringLocalizer.keyToString("GUI_FOCUS_ADD_TITLE_KEY"),
         _project);

    Set<ReconstructionRegion> regionSet = new HashSet<ReconstructionRegion>();
    regionSet.addAll(testProgram.getUnfocusedReconstructionRegionsNeedingRetest());
    regionSet.addAll(testProgram.getUnfocusedReconstructionRegionsToBeFixed());
    regionSet.addAll(testProgram.getUnfocusedReconstructionRegionsWithCustomFocus());
    regionSet.addAll(testProgram.getUntestableReconstructionRegionsBasedOnFocus());

    for(ReconstructionRegion rr : regionSet)
    {
      _reconstructionRegions.add(new FocusReconstructionRegionWrapper(rr));
    }
    sortColumn(_currentSortColumn, _currentSortAscending);
  }

  /**
   * @author Andy Mechtenberg
   */
  void unpopulate()
  {
    _reconstructionRegions.clear();
    _project = null;
  }

  /**
   * @author Andy Mechtenberg
   */
  void commitChanges()
  {
    for(FocusReconstructionRegionWrapper rr : _reconstructionRegions)
    {
      rr.commitChanges();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  void clear()
  {
    _reconstructionRegions.clear();
    _currentSortColumn = 0;
    _currentSortAscending = true;
  }

  /**
   * @author Andy Mechtenberg
   */
  int getRowForReconstructionRegion(FocusReconstructionRegionWrapper rr)
  {
    int row = -1;
    for(FocusReconstructionRegionWrapper wrapper : _reconstructionRegions)
    {
      if (wrapper.getReconstructionRegion() == rr.getReconstructionRegion())
      {
        row = _reconstructionRegions.indexOf(wrapper);
        break;
      }
    }
//    row = _reconstructionRegions.indexOf(rr);
    return row;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getColumnCount()
  {
    return _tableColumns.length;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getRowCount()
  {
    if (_reconstructionRegions == null)
      return 0;
    else
      return _reconstructionRegions.size();
  }

  /**
   * @author Andy Mechtenberg
   */
  public Class getColumnClass(int c)
  {
    return getValueAt(0, c).getClass();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_tableColumns[columnIndex];
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    // column 0 is image contents - not editable
    // column 1 is focus method - editable
    // column 2 is defect analyzer - editable
    // column 3 is no test - editable

    // if no-test is true, the other cells shouldn't be editable
    boolean noTest = (Boolean)getValueAt(rowIndex, 3);

    if (columnIndex == 0)
      return false;
    if (columnIndex == 1)
    {
      if (noTest)
        return false;
//      String currentValue = (String)getValueAt(rowIndex, columnIndex);
//      if (currentValue.equals(_DEFAULT))
//        return false;
//      else
        return true;
    }
    if (columnIndex == 2) // re-test
    {
      if (noTest)
        return false;
      // this is not editable if the joint type doesn't allow it
      JointTypeEnum jointTypeEnum = _reconstructionRegions.get(rowIndex).getJointTypeEnum();
      if (jointTypeEnum.isAvailableForRetestFailingPins())
        return true;
      else
        return false;
    }
    if (columnIndex == 3)
    {
      return true;
    }
    return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    FocusReconstructionRegionWrapper rr = _reconstructionRegions.get(rowIndex);
    if (columnIndex == 0) // image contents details
    {
      return  rr.toString();
    }
    else if (columnIndex == 1) // focus method
    {
      if (rr.isMarkedForFixFocus())
        return _TO_BE_FIXED;
      else if (rr.isUsingCustomFocus())
        return _CUSTOM_FOCUS;
      else
        return _DEFAULT;
    }
    else if (columnIndex == 2) // defect analyzer
    {
      return new Boolean(rr.isUsingRetest());
    }
    else // no test
    {
      return new Boolean(rr.isTestableForUnfocusedRegions() == false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    FocusReconstructionRegionWrapper rr = _reconstructionRegions.get(rowIndex);
    if (columnIndex == 0) // column 0 is component name -- not editable
      Assert.expect(false);
    else if (columnIndex == 1)
    {
      // focus method
      String currentValue = (String)getValueAt(rowIndex, columnIndex);
      String newValue = (String)aValue;
      if (newValue.equals(_CUSTOM_FOCUS))
      {
        // only allow if there was previously a custom
        if (rr.wasCustomDefined())
        {
          rr.revertToCustomOnly();
        }
        else // not allowed to go to custom (no previous custom defined)
        {
          // would like a message here
          MessageDialog.showErrorDialog(_parent, StringLocalizer.keyToString("GUI_FOCUS_NO_CUSTOM_FOCUS_INFO_KEY"), StringLocalizer.keyToString("GUI_FOCUS_SUMMARY_TITLE_KEY"), true);
        }
      }
      else if (newValue.equals(_TO_BE_FIXED))
      {
        rr.setFixFocusLater(true);
      }
      else if (newValue.equals(_DEFAULT))
      {
        if (currentValue.equals(_TO_BE_FIXED))
          rr.clearFixFocusLater();
        if (currentValue.equals(_CUSTOM_FOCUS))
          rr.clearCustomFocus();
      }
    }
    else if (columnIndex == 2) // retest
    {
      Boolean newValue = (Boolean)aValue;
      boolean usingRetest = newValue.booleanValue();

      /** @todo this logic should move to wrapper class */
      if (rr.isUsingRetest() && usingRetest == false)
        rr.setUseRetest(newValue.booleanValue());
      if (rr.isUsingRetest() == false && usingRetest)
          rr.setUseRetest(newValue.booleanValue());
      return; // we're done as soon as it's set
    }
    else if (columnIndex == 3) // no test
    {
      Boolean newValue = (Boolean)aValue;
      boolean noTest = newValue.booleanValue();
      boolean currentTestStatus = rr.isTestableForUnfocusedRegions();
      if (currentTestStatus && noTest)
        rr.setTestestableForUnfocusedRegions(noTest == false);
      if (currentTestStatus == false && noTest == false)
        rr.setTestestableForUnfocusedRegions(noTest == false);
      return;
    }
    else
      Assert.expect(false); // only column 1 and 2 are editable
  }

  /**
   * @author Andy Mechtenberg
   */
  public void sortColumn(int column, boolean ascending)
  {
    if (column == 0)  // image string
      Collections.sort(_reconstructionRegions, new ReconstructionRegionComparator(ascending, ReconstructionRegionComparatorEnum.DESCRIPTION));
    else if (column == 1)  // sort on focus override
      Collections.sort(_reconstructionRegions, new ReconstructionRegionComparator(ascending, ReconstructionRegionComparatorEnum.FOCUS_METHOD));
    else if (column == 2)  // sort on focus re-test
      Collections.sort(_reconstructionRegions, new ReconstructionRegionComparator(ascending, ReconstructionRegionComparatorEnum.RETEST_STATUS));
    else  // sort on focus no-test
      Collections.sort(_reconstructionRegions, new ReconstructionRegionComparator(ascending, ReconstructionRegionComparatorEnum.NOTEST_STATUS));

    _currentSortColumn = column;
    _currentSortAscending = ascending;
  }
}
