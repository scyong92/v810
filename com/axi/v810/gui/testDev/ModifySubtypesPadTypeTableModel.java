package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.ProjectHistoryLog;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * This class is the data model for the package table for the Groupings panel and it also
 * converts to the component table model in the groupings panel
 * @author Andy Mechtenberg
 */
public class ModifySubtypesPadTypeTableModel extends DefaultSortTableModel
{
  private List<ComponentType> _componentTypes = new ArrayList<ComponentType>();
  private List<PadType> _componentPads = new ArrayList<PadType>();  // list of PadType objects
  private List<PadType> _currentlySelectedPadTypes = new ArrayList<PadType>();

  private final String _MIXED = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");

  private final String _NOTEST = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOTEST_KEY");
  private final String _RESTORE_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_RESTORE_KEY");
  private final String _NOLOAD = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOLOAD_KEY");
  private final String _UNTESTABLE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_UNTESTABLE_KEY");
  private final String _TESTABLE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_TESTABLE_KEY");
  private final String _PARTIAL_UNTESTABLE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_PARTIAL_UNTESTABLE_KEY");
  private final String[] _componentTableColumns = {StringLocalizer.keyToString("ATGUI_JOINT_KEY"),
                                                  StringLocalizer.keyToString("ATGUI_FAMILY_KEY"),
                                                  StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY"),
                                                  StringLocalizer.keyToString("GISWK_STATUS_KEY")};
//                                                  StringLocalizer.keyToString("MMGUI_GROUPINGS_SHADED_BY_COMPONENT_KEY")};

  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;
  private PadType _componentPadType = null;
  //Project's History log -hsia-fen.tan
  private static transient ProjectHistoryLog _fileWriter;

  /**
   * @author Andy Mechtenberg
   */
  public ModifySubtypesPadTypeTableModel()
  {
    // do nothing
    _componentPads = new ArrayList<PadType>();
    _fileWriter = ProjectHistoryLog.getInstance();
  }

  /**
   * @author Andy Mechtenberg
   */
  void clear()
  {
    if (_componentPads != null)
      _componentPads.clear();
    if (_currentlySelectedPadTypes != null)
      _currentlySelectedPadTypes.clear();
    if (_componentTypes != null)
      _componentTypes.clear();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void populateWithComponentData(Collection<ComponentType> componentTypes)
  {
    Assert.expect(componentTypes != null);
    Assert.expect(componentTypes.isEmpty() == false);

    _componentTypes = new ArrayList<ComponentType>(componentTypes);
    _componentPads = _componentTypes.get(0).getPadTypes();
   
    sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableDataChanged();
  }

  /**
   * @author Andy Mechtenberg
   */
  void updateSpecificPadType(PadType padType)
  {
    Assert.expect(padType != null);
    if (_componentTypes.isEmpty())  // not displaying anything, so nothing to update
      return;
    int padTypeRow = getRowForComponentPad(padType);
    fireTableRowsUpdated(padTypeRow, padTypeRow);
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean isComponentPadDisplayed(PadType padType)
  {
    Assert.expect(padType != null);
    // here, we might have more than one component type selected, so we'll have to look through each one
    int rowIndex = -1;
    String padName = padType.getName();
    for(PadType tablePadType : _componentPads)
    {
      if (tablePadType.getName().equalsIgnoreCase(padName))
        return true;
    }
    return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  int getRowForComponentPad(PadType padType)
  {
    Assert.expect(padType != null);
    // here, we might have more than one component type selected, so we'll have to look through each one
    int rowIndex = -1;
    String padName = padType.getName();
    for(PadType tablePadType : _componentPads)
    {
      if (tablePadType.getName().equalsIgnoreCase(padName))
        return _componentPads.lastIndexOf(tablePadType);
    }
//    Assert.expect(false, "Should have found pad type: " + padType + " in list: " + _componentTypes);
    return rowIndex;
  }

  /**
   * @author Andy Mechtenberg
   */
  PadType getPadTypeAt(int row)
  {
    Assert.expect(_componentPads != null);    
    return _componentPads.get(row);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void sortColumn(int column, boolean ascending)
  {
    if (column == 0)
      Collections.sort(_componentPads, new AlphaNumericComparator(ascending));
    else if (column == 1)
      Collections.sort(_componentPads, new PadJointTypeOrSubtypeComparator(ascending, true, false, false, false, false));
    else if (column == 2)
      Collections.sort(_componentPads, new PadJointTypeOrSubtypeComparator(ascending, false, false, false, false, false));
    else if (column == 3)
      Collections.sort(_componentPads, new PadJointTypeOrSubtypeComparator(ascending, false, false, false, false, true));

    _currentSortColumn = column;
    _currentSortAscending = ascending;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getColumnCount()
  {
    return _componentTableColumns.length;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getRowCount()
  {
    if (_componentPads == null)
      return 0;
    else
      return _componentPads.size();   
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_componentTableColumns[columnIndex];
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    if(rowIndex < 0)
      return false;

    if (columnIndex == 0) // component pin name
      return false;
    else if (columnIndex == 1) // component joint type -- not editable -- much edit component's package to change this
      return false;
    // subtype is editable unless it's a NO LOAD or MIXED.  It's also not editable for CAP, PCAP or RES joint types or if the pad is not testable
    else if (columnIndex == 2)
    {
      for (PadType padType : _currentlySelectedPadTypes)
      {
        if (padType.getPadTypeSettings().areAllPadsNotTestable())
          return false;
        
        if (padType.getComponentType().isLoaded() == false)
          return false;
        
//        if (padType.getComponentType().getSubtypes().size() > 1)
//          return false;
      }
//      String valueAt = (String)getValueAt(rowIndex, columnIndex);
//      if (valueAt.equals(_NOLOAD))
//        return false;
//      if (valueAt.equals(_MIXED))
//        return false;

      JointTypeEnum jointType = getPadTypeAt(rowIndex).getJointTypeEnum();
      if (ImageAnalysis.isSpecialTwoPinComponent(jointType))
        return false;
    }
    else if (columnIndex == 3) 
      return false;

    if (_currentlySelectedPadTypes.contains(getValueAt(rowIndex, 0)))
      return true;
    else
      return false;

//    Assert.expect(false);
//    return false; // should not be it
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  public synchronized Object getValueAt(int rowIndex, int columnIndex)
  {
    // this needs to handle 3 columns
    // column 0:  Joint Name - this should be the same for all components represeneted, so use any
    // column 1:  Joint Type - again, same for all (based on a common CompPackage)
    // column 2:  Subtype -- at the pad level, this is simple
    PadType padType = _componentPads.get(rowIndex);
    if (columnIndex == 0)
      return padType;
    else if (columnIndex == 1)
      return padType.getPackagePin().getJointTypeEnum();
    else if (columnIndex == 2) // test state or subtype
    {
      if (padType.getComponentType().getComponentTypeSettings().areAllPadTypesNotTestable())
        return _UNTESTABLE;
      else if (padType.getComponentType().isTestable() == false)
        return _PARTIAL_UNTESTABLE;
      else if (padType.getComponentType().isLoaded() == false)
       return _NOLOAD;
      else if (padType.isInspected() == false)
        return _NOTEST;
      else
        return padType.getSubtype().getShortName();
    }
    else if (columnIndex == 3) // test state 
    {
      if (padType.getPadTypeSettings().areAllPadsNotTestable())
        return _UNTESTABLE;
      else if (padType.getPadTypeSettings().isTestable() == false)
        return _PARTIAL_UNTESTABLE;
      else if (padType.getComponentType().isLoaded() == false)
        return _NOLOAD;
      else if (padType.isInspected() == false)
        return _NOTEST;
      else
        return _TESTABLE;
    }

    return null;
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  public synchronized void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    if (columnIndex == 0)
      Assert.expect(false); // cannot edit the pad name
    else if (columnIndex == 1) // not editable - can't change the pad's joint type -- you must change the PackagePin's joint type instead
      Assert.expect(false);
    else if (columnIndex == 2) // change the pad's subtype (could be No Test or Set To Test)
    {
      String newSubtypeName = (String)aValue;
      PadType basePadType = _componentPads.get(rowIndex);

      if (newSubtypeName.equalsIgnoreCase(_NOTEST))
      {
        for (ComponentType componentType : _componentTypes)
        {
          PadType currentPadType = getPadType(componentType, basePadType);
          try
          {
            _commandManager.execute(new PadTypeSetTestedCommand(currentPadType, false));
            _fileWriter.appendComponentDetail(componentType,currentPadType.getPackagePin().getJointTypeEnum(), basePadType,_NOTEST);
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
        }
        return; // we're done as soon as it's set
      }
      if (newSubtypeName.equalsIgnoreCase(_RESTORE_SUBTYPE))  // set to test
      {
        for (ComponentType componentType : _componentTypes)
        {
          PadType currentPadType = getPadType(componentType, basePadType);
          try
          {
            _commandManager.execute(new PadTypeSetTestedCommand(currentPadType, true));
            _fileWriter.appendComponentDetail(componentType,currentPadType.getPackagePin().getJointTypeEnum(), basePadType,_RESTORE_SUBTYPE);
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
        }
        return; // we're done as soon as it's set
      }

      // ok, if we got this far, we're changing to a new subtype based on the incoming string value
      JointTypeEnum jointType = basePadType.getJointTypeEnum();
      java.util.List<Subtype> usedSubtypes = basePadType.getComponentType().getSideBoardType().getBoardType().getSubtypesIncludingUnused(jointType);
      for (Subtype subtype : usedSubtypes)
      {
        if (subtype.getShortName().equalsIgnoreCase(newSubtypeName))
        {
          // loop over all components that comprise the data in this table
          for(ComponentType componentType : _componentTypes)
          {
            PadType currentPadType = getPadType(componentType, basePadType);
            try
            {
              _commandManager.execute(new PadTypeSetSubtypeCommand(currentPadType, subtype));
              _fileWriter.appendComponentDetail(componentType,jointType, basePadType,subtype);
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                            ex.getLocalizedMessage(),
                                            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                            true);
              fireTableCellUpdated(rowIndex, columnIndex);
            }
          }
          return ; // we're done as soon as it's set
        }
      }
      // if not set, then this is a new subtype and we need to create one
      Panel panel = basePadType.getComponentType().getSideBoardType().getBoardType().getPanel();
      LandPattern landPattern = basePadType.getComponentType().getLandPattern();

      JointTypeEnum jointTypeEnum = basePadType.getJointTypeEnum();
      Subtype newSubtype = panel.createSubtype(newSubtypeName, landPattern, jointTypeEnum);
      // now set it for all components
      for(ComponentType componentType : _componentTypes)
      {
        PadType currentPadType = getPadType(componentType, basePadType);
        try
        {
          _commandManager.execute(new PadTypeSetSubtypeCommand(currentPadType, newSubtype));
          _fileWriter.appendComponentDetail(componentType,jointType, basePadType,newSubtype);
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
      }
    }
    else
      Assert.expect(false, "Column index out of range: " + columnIndex);
  }

  /**
   * @author Andy Mechtenberg
   */
  private PadType getPadType(ComponentType componentType, PadType basePadType)
  {
    String padName = basePadType.getName();
    for(PadType componentPadType : componentType.getPadTypes())
    {
      if (componentPadType.getName().equalsIgnoreCase(padName))
        return componentPadType;
    }
    Assert.expect(false, "Unable to find pad type for : " + componentType + " " + basePadType);
    return null;
  }

  /**
   * @author Andy Mechtenberg
   */
  List<PadType> getSelectedPadTypes()
  {
    return _currentlySelectedPadTypes;
  }

  /**
   * @author Andy Mechtenberg
   */
  void setSelectedPadTypes(List<PadType> selectedPads)
  {
    Assert.expect(selectedPads != null);
    _currentlySelectedPadTypes = selectedPads;
  }

  /**
   * @author George Booth
   */
  public int getRowForDataStartingWith(String key)
  {
    key = key.toLowerCase();
    int index = -1;
    for (PadType padType : _componentPads)
    {
      index++;
      String padName = padType.getName().toLowerCase();
      if (padName.startsWith(key))
      {
        return index;
      }
    }
    return -1;
  }
}
