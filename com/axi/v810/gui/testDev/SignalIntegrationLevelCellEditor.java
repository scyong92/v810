package com.axi.v810.gui.testDev;

import java.util.*;
import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testGen.*;

/**
 * This class implements a combo box cell editor tailored to changing the integration level for a joint
 * or a list of joints.
 *
 * @author Erica Wheatcroft
 */
public class SignalIntegrationLevelCellEditor extends DefaultCellEditor
{
  private SubtypeAdvanceSettingsTableModel _subtypeTableModel = null;

  /**
   * @author Erica Wheatcroft
   */
   public SignalIntegrationLevelCellEditor(JComboBox comboBox, SubtypeAdvanceSettingsTableModel subtypeTableModel)
   {
     super(comboBox);
     Assert.expect(subtypeTableModel != null);
     _subtypeTableModel = subtypeTableModel;
   }

  /**
   * @author Erica Wheatcroft
   */
  public java.awt.Component getTableCellEditorComponent(JTable table,
                                                        Object value,
                                                        boolean isSelectable,
                                                        int row,
                                                        int column)
  {
    // get the editor (should be a JComboBox as was specified in the constructor)
    // then modify the contents of the combo box to list only the valid choices for this row in the table
    int selectedRows[] = table.getSelectedRows();

    java.awt.Component editor = super.getTableCellEditorComponent(table, value, isSelectable, row, column);

    JComboBox comboBox = (JComboBox)editor;
    comboBox.removeAllItems();

    if (selectedRows.length <= 1)
    {     
      Subtype subtype = _subtypeTableModel.getSubtypeAt(row);
      //subtype.getPanel().getComp
      List<SignalCompensationEnum> validIntegrationLevels = ProgramGeneration.getInstance().getValidSignalCompensations(subtype.getPadTypes());
      //for(SignalCompensationEnum validIntegrationLevel: validIntegrationLevels)
      for (SignalCompensationEnum validIntegrationLevel : validIntegrationLevels)
      {
        //Ngie Xing, add IL 6 and 7 temporarily for Flex
//        if(subtype.getPanel().getProject().isGenerateByNewScanRoute()==false)
//        {
//          // Added by Khang Wah, 2013-08-21, Only have the following IL for new scan route
//          if(validIntegrationLevel.equals(SignalCompensationEnum.IL_6) || 
//             validIntegrationLevel.equals(SignalCompensationEnum.IL_7))
//            continue;
//        }
        comboBox.addItem(validIntegrationLevel);
      }
      comboBox.setMaximumRowCount(5);
      // since the cell editor is active then we know they are all the same so just get the first pad type and display
      // the first integreation level. - this could still be the default
      comboBox.setSelectedItem(subtype.getSubtypeAdvanceSettings().getSignalCompensation());
      
      if (validIntegrationLevels != null)
        validIntegrationLevels.clear();
      
      return comboBox;
    }
    else // more than one row selected
    {      
      Set<SignalCompensationEnum> validIntegrationLevelsSet = new TreeSet<SignalCompensationEnum>(new SignalCompensationEnumComparator(true));
      for (int i = 0; i < selectedRows.length; i++)
      {
        Subtype subtype = _subtypeTableModel.getSubtypeAt(selectedRows[i]);
        List<SignalCompensationEnum> validIntegrationLevelsForComponentType = ProgramGeneration.getInstance().getValidSignalCompensations(subtype.getPadTypes());
        // If this is our first iteration, we need to initialize our set.
        if (i == 0)
        {
          validIntegrationLevelsSet.addAll(validIntegrationLevelsForComponentType);
        }
        // Otherwise, do a 'retainAll' with our current set.
        else
        {
          validIntegrationLevelsSet.retainAll(validIntegrationLevelsForComponentType);
        }
        
        if (validIntegrationLevelsForComponentType != null)
          validIntegrationLevelsForComponentType.clear();
      }
      
      Subtype subtype = _subtypeTableModel.getSubtypeAt(0);

      // once completed we will had the options to the combo box we should have no more than 4
      for (SignalCompensationEnum validIntegrationLevel : validIntegrationLevelsSet)
      {
        //Ngie Xing, add IL 6 and 7 temporarily for Flex
//        if(subtype.getPanel().getProject().isGenerateByNewScanRoute()==false)
//        {
//          // Added by Khang Wah, 2013-08-21, Only have the following IL for new scan route
//          if(validIntegrationLevel.equals(SignalCompensationEnum.IL_6) || 
//             validIntegrationLevel.equals(SignalCompensationEnum.IL_7))
//            continue;
//        }
        comboBox.addItem(validIntegrationLevel);
      }
      comboBox.setMaximumRowCount(5);
      return comboBox;
    }
  }
}
