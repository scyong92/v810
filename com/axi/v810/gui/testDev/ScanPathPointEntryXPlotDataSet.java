package com.axi.v810.gui.testDev;

import java.util.*;

import org.jfree.data.statistics.*;
import org.jfree.data.xy.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.MeasurementEnum;
import com.axi.v810.business.testResults.MeasurementUnitsEnum;
import com.axi.v810.datastore.testResults.MeasurementResult;
import com.axi.v810.gui.JointResultsEntry;
import com.axi.v810.hardware.StagePosition;

/**
 *
 * @author chin-seong.kee
 */
class ScanPathPointEntryXPlotDataSet extends AbstractXYDataset
{
  private List<StagePosition> _stagePositionList;
 
  private XYDataset _dataSet;
  private List<String> _formattedValues;
  
  private List<List<StagePosition>> _arrangeStagePositionEntries;
  private List<Integer> _dataSetCutOffPlaceList;

  private double _mean;
  private double _standardDeviation;
  private double _median;
  private MeasurementUnitsEnum _measurementUnit;

  /**
   * Constructor used to build an empty chart.
   * @author Rex Shang
   */
  ScanPathPointEntryXPlotDataSet()
  {
    this(null);
  }
  
    /**
   * Returns our title so it can be displayed in JFreeChart.
   * @author Kee Chin Seong
   */
  private void calculateDataSeriesNeeded()
  {
    int startX = 0, endX = 0;//, nCounter = 0;
    
    for(int nCounter = 0; nCounter < _stagePositionList.size() ; nCounter ++)//
    { 
      StagePosition sp = _stagePositionList.get(nCounter);
      endX = sp.getXInNanometers();
      
      if(endX - startX < 0)
      {
        _dataSetCutOffPlaceList.add(nCounter);
      }
      startX = endX;
    }
  }
  
  /**
   * Returns our title so it can be displayed in JFreeChart.
   * @author Kee Chin Seong
   */ 
  private void arrangeDataSeries(int seqNumber)
  {
    if(_stagePositionList.isEmpty() == false)
    {
      XYSeriesCollection dataPoints = new XYSeriesCollection();
      if(_dataSetCutOffPlaceList.isEmpty() == false)
      {
        _arrangeStagePositionEntries.add(_stagePositionList.subList(0, _dataSetCutOffPlaceList.get(0)));
        for(int cutOffPlaceValue = 0; cutOffPlaceValue < _dataSetCutOffPlaceList.size(); cutOffPlaceValue ++)
        {
          if(cutOffPlaceValue + 1 < _dataSetCutOffPlaceList.size())
          {
            _arrangeStagePositionEntries.add(_stagePositionList.subList(_dataSetCutOffPlaceList.get(cutOffPlaceValue), _dataSetCutOffPlaceList.get(cutOffPlaceValue + 1)));
          }
          else
            _arrangeStagePositionEntries.add(_stagePositionList.subList(_dataSetCutOffPlaceList.get(cutOffPlaceValue), _stagePositionList.size() - 1));
        }  
      }
      else
      {
        _arrangeStagePositionEntries.add(_stagePositionList);
      }
    }
    _dataSet = populatePlotDataList(seqNumber);
  }
  
   /**
   * Returns our title so it can be displayed in JFreeChart.
   * @author Kee Chin Seong
   */ 
  private void arrangeDataSeries()
  {
    if(_stagePositionList.isEmpty() == false)
    {
      XYSeriesCollection dataPoints = new XYSeriesCollection();
      if(_dataSetCutOffPlaceList.isEmpty() == false)
      {
        _arrangeStagePositionEntries.add(_stagePositionList.subList(0, _dataSetCutOffPlaceList.get(0)));
        for(int cutOffPlaceValue = 0; cutOffPlaceValue < _dataSetCutOffPlaceList.size(); cutOffPlaceValue ++)
        {
          if(cutOffPlaceValue + 1 < _dataSetCutOffPlaceList.size())
          {
            _arrangeStagePositionEntries.add(_stagePositionList.subList(_dataSetCutOffPlaceList.get(cutOffPlaceValue), _dataSetCutOffPlaceList.get(cutOffPlaceValue + 1)));
          }
          else
            _arrangeStagePositionEntries.add(_stagePositionList.subList(_dataSetCutOffPlaceList.get(cutOffPlaceValue), _stagePositionList.size() - 1));
        }  
      }
      else
      {
        _arrangeStagePositionEntries.add(_stagePositionList);
      }
    }
    _dataSet = populatePlotDataList();
  }

  /**
   * @author Kee Chin Seong
   */
  ScanPathPointEntryXPlotDataSet(List<StagePosition> stagePositionList)
  {
    _stagePositionList = stagePositionList;

    if (_stagePositionList == null)
      _stagePositionList = new ArrayList<StagePosition>();

    _formattedValues = new ArrayList<String>();

    if(_arrangeStagePositionEntries ==  null)
      _arrangeStagePositionEntries = new ArrayList<List<StagePosition>>();
    
    if(_dataSetCutOffPlaceList == null)
      _dataSetCutOffPlaceList = new ArrayList<Integer>();
    
    calculateDataSeriesNeeded();
    arrangeDataSeries();
  }
  
    /**
   * @author Kee Chin Seong
   */
  ScanPathPointEntryXPlotDataSet(List<StagePosition> stagePositionList, int seqNumber)
  {
    _stagePositionList = stagePositionList;

    if (_stagePositionList == null)
      _stagePositionList = new ArrayList<StagePosition>();

    _formattedValues = new ArrayList<String>();

    if(_arrangeStagePositionEntries ==  null)
      _arrangeStagePositionEntries = new ArrayList<List<StagePosition>>();
    
    if(_dataSetCutOffPlaceList == null)
      _dataSetCutOffPlaceList = new ArrayList<Integer>();
    
    calculateDataSeriesNeeded();
    arrangeDataSeries(seqNumber);
  }
  
  /**
   * @author Kee Chin Seong
   */
  private XYSeriesCollection populatePlotDataList(int seqNumber)
  {
    XYSeriesCollection dataPoints = new XYSeriesCollection();
    int scanPathNo = 1;
    
    List<StagePosition> spList = _arrangeStagePositionEntries.get(seqNumber - 1); 
    // Find the value we need to chart.
    XYSeries data = new XYSeries("Scan Path " + scanPathNo);
      
    for(StagePosition sp : spList)
       data.add(sp.getXInNanometers(), sp.getYInNanometers());
    dataPoints.addSeries(data);
      
    return dataPoints;
  }

  /**
   * @author Kee Chin Seong
   */
  private XYSeriesCollection populatePlotDataList()
  {
    XYSeriesCollection dataPoints = new XYSeriesCollection();
    int scanPathNo = 1;
    
    // Find the value we need to chart.
    for (List<StagePosition> spList : _arrangeStagePositionEntries)
    {
      XYSeries data = new XYSeries("Scan Path " + scanPathNo);
      
      for(StagePosition sp : spList)
        data.add(sp.getXInNanometers(), sp.getYInNanometers());
      dataPoints.addSeries(data);
      
      scanPathNo ++;
    } 
    return dataPoints;
  }


  public int getScanPathsSize()
  {
    if(_dataSetCutOffPlaceList != null)
      return _dataSetCutOffPlaceList.size();
    else 
      return 0;
  }
  
  /**
   * We will always have 1 series of data.
   * @author Kee chin Seong
   */
  public int getSeriesCount()
  {
    return _dataSet.getSeriesCount();
  }

  
  /**
   * To render tool tip, we need to have a name for our series.
   * @author Kee Chin Seong
  */
  public Comparable getSeriesKey(int series)
  {
    return "";//_dataSet.getSeriesKey(series);
  }

  /**
   * @author Kee chin Seong
   */
  public int getItemCount(int series)
  {
    return _dataSet.getItemCount(series);
  }

  /**
   * For X-Plot, our X value is essentially the order number of this data point.
   * @author Kee Chin Seong
   */
  public Number getX(int series, int item)
  {
    return _dataSet.getX(series, item);//_dataSet.getX(series, item);//_stagePositionList.get(item).getXInNanometers();
  }

  /**
   * Find the measurement value.
   * @author Kee Chin Seong
   */
  public Number getY(int series, int item)
  {
    //if(_dataSet.getSeriesCount() == 1)
    //  return 0;
    //else
    //  return _dataSet.getY(series, item);
    return _dataSet.getY(series, item);//_dataSet.getY(series, item);//get(item).getYInNanometers();
  }

  /**
   * Find the formated measurement value with unit.
   * @author Kee Chin Seong
   */
  public String getFormattedValue(int series, int item)
  {
    return _formattedValues.get(item);
  }

  /**
   * @author Kee Chin Seong
   */
  StagePosition getStagePosition(int series, int item)
  {
    return _stagePositionList.get(item);
  }

  /**
   * Get the mean value of this data set.
   * @author Kee Chin Seong
   */
  double getMean()
  {
    return _mean;
  }

  /**
   * Get the median value of this data set.
   * @author Kee Chin Seong
   */
  double getMedian()
  {
    return _median;
  }

  /**
   * Get the standard deviation of this data set.
   * @author Kee Chin Seong
   */
  double getStandardDeviation()
  {
    return _standardDeviation;
  }

  /**
   * Get the general measurement unit.
   * @author Kee Chin Seong
   */
  MeasurementUnitsEnum getMeasurementUnitsEnum()
  {
    return _measurementUnit;
  }
}
