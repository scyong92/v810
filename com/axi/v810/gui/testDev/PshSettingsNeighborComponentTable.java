package com.axi.v810.gui.testDev;

import javax.swing.table.*;

import com.axi.guiUtil.*;

/**
 *
 * @author siew-yeng.phang
 */
public class PshSettingsNeighborComponentTable extends JSortTable
{
  public static final int _NEIGHBOUR_COMPONENT_NAME_INDEX = 0;
  public static final int _JOINT_TYPE_INDEX = 1;
  public static final int _SUBTYPE_INDEX = 2;
  public static final int _PSH_COMPONENT_INDEX = 3;
  
  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _NEIGHBOR_COMPONENT_NAME_COLUMN_WIDTH_PERCENTAGE = .25;
  private static final double _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE = .25;
  private static final double _SUBTYPE_COLUMN_WIDTH_PERCENTAGE = .25;
  private static final double _PSH_COMPONENT_NAME_COLUMN_WIDTH_PERCENTAGE = .25;
  
  /**
   * Default constructor.
   * @author Siew Yeng
   */
  public PshSettingsNeighborComponentTable(TableModel tableModel)
  {
    this.setModel(tableModel);
  }
  
  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Siew Yeng
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int neighborComponentNameColumnWidth = (int)(totalColumnWidth * _NEIGHBOR_COMPONENT_NAME_COLUMN_WIDTH_PERCENTAGE);
    int jointTypeNameColumnWidth = (int)(totalColumnWidth * _JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE);
    int subtypeNameColumnWidth = (int)(totalColumnWidth * _SUBTYPE_COLUMN_WIDTH_PERCENTAGE);
    int pshComponentNameColumnWidth = (int)(totalColumnWidth * _PSH_COMPONENT_NAME_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_NEIGHBOUR_COMPONENT_NAME_INDEX).setPreferredWidth(neighborComponentNameColumnWidth);
    columnModel.getColumn(_JOINT_TYPE_INDEX).setPreferredWidth(jointTypeNameColumnWidth);
    columnModel.getColumn(_SUBTYPE_INDEX).setPreferredWidth(subtypeNameColumnWidth);
    columnModel.getColumn(_PSH_COMPONENT_INDEX).setPreferredWidth(pshComponentNameColumnWidth);
  }

  /**
   * @author Siew Yeng
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();
  }
}
