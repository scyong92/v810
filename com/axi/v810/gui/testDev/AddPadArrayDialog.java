package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.text.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: AddPadArrayDialog</p>
 * <p>Description: Dialog used to add multiple instances of the same land pattern pad to the given land pattern
 * to a panel.  This is a modal dialog.</p>
 * <p>Company:  Agilent Technogies</p>
 * @author Laura Cormos
 */
class AddPadArrayDialog extends JDialog
{
  private static final int TEXT_WIDTH_MARGIN = 5;
  private static final int TEXT_HEIGHT_MARGIN = 5;

  // command manager for undo functionality
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  //string constants retrieved from the localization properties file
  private static final String PAD_SEPARATION_PROMPT = StringLocalizer.keyToString("APA_GUI_SEPARATION_PROMPT_KEY");
  private static final String CANCEL = StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY");
  private static final String OK = StringLocalizer.keyToString("GUI_OK_BUTTON_KEY");
  private static final String OFFSET_PROMPT = StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_OFFSET_PROMPT_KEY");
  private static final String SPACING_PROMPT = StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_SPACING_PROMPT_KEY");
  private static final String X_VALUE_PROMPT = StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_X_VALUE_PROMPT_KEY");
  private static final String Y_VALUE_PROMPT = StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_Y_VALUE_PROMPT_KEY");
  private static final String TITLE = StringLocalizer.keyToString("APA_GUI_ADD_PAD_ARRAY_TITLE_KEY");
  private static final String ARRAY_LAYOUT = StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_ARRAY_LAYOUT_KEY");

  private Project _project = null;
  private LandPattern _landPattern = null;

  private FocusAdapter _textFieldFocusAdapter;
  private ImageIcon _spacingSeparationImage = null;
  private ImageIcon _offsetSeparationImage = null;
  private JPanel _mainPanel = new JPanel();
  private JScrollPane _scrollLandPatternPadTable = new JScrollPane();
  private PadArrayTable _landPatternPadTable;
  private PadArrayTableModel _landPatternPadTableModel = new PadArrayTableModel(this);
  private JPanel _selectionsPanel = new JPanel();
  private JPanel _buttonsPanel = new JPanel();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private JLabel _separationLabel = new JLabel();
  private JRadioButton _spacingRadioButton = new JRadioButton();
  private JRadioButton _offsetRadioButton = new JRadioButton();
  private JLabel _xValueLabel = new JLabel();
  private JLabel _yValueLabel = new JLabel();
  private NumericTextField _xValueTextField = new NumericTextField();
  private NumericTextField _yValueTextField = new NumericTextField();
  private NumericRangePlainDocument _xValueDocument = new NumericRangePlainDocument();
  private NumericRangePlainDocument _yValueDocument = new NumericRangePlainDocument();
  private JPanel _valuesPanel = new JPanel();
  private JPanel _xyPanelSelections = new JPanel();
  private JPanel _separationOptionsPanel = new JPanel();
  private FlowLayout _separationOptionsFlowLayout = new FlowLayout();
  private JPanel _imagePanel = new JPanel();
  private BorderLayout _selectionsPanelBorderLayout = new BorderLayout();
  private Border _border2;
  private JPanel _landPatternPadTablePanel = new JPanel();
  private BorderLayout _landPatternPadTableBorderLayout = new BorderLayout();
  private Border _border3;
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private Border _border5;
  private Border _border6;
  private PairLayout _valuesPanelPairLayout = new PairLayout(5, 10, false);
  private BorderLayout _xyPanelSelectionsBorderLayout = new BorderLayout();
  private Border _border7;
  private JPanel _innerButtonsPanel = new JPanel();
  private GridLayout _innerButtonsPanelGridLayout = new GridLayout();
  private FlowLayout _buttonsPanelFlowLayout = new FlowLayout();
  private FlowLayout _imagePanelFlowLayout = new FlowLayout();
  private JLabel _imageLabel = new JLabel();
  private ButtonGroup _separationButtonGroup = new ButtonGroup();
  private Border _border9;
  private Border _border10;
  
  private ProgressDialog _addPadArrayProgressCancelDialog = null;
  static private boolean _userAbort = false;
  static private boolean _hasException = false;

  /**
   * Constructor.  Uses parent frame to center.
   * @param parent Parent frame.  Uses to center over.
   * @author Carli Connally
   */
  public AddPadArrayDialog(Frame parent, LandPattern landPattern, Project project)
  {
    super(parent, true);
    Assert.expect(parent != null);
    Assert.expect(project != null);

    _landPattern = landPattern;
    _project = project;
    jbInit();
    init();
  }

  /**
   * UI initialization method.  Called and modified by the UI designer.
   * @author Carli Connally
   */
  private void jbInit()
  {
    _textFieldFocusAdapter = new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        JTextField sourceTextField = (JTextField)e.getSource();
        sourceTextField.selectAll();
      }

      public void focusLost(FocusEvent e)
      {
        NumericTextField sourceTextField = (NumericTextField)e.getSource();
        handleTextFieldData(sourceTextField);
      }
    };

    _border2 = BorderFactory.createEmptyBorder(7,7,7,7);
    _border3 = BorderFactory.createEmptyBorder(7,0,7,0);
    _border5 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(165, 163, 151)),ARRAY_LAYOUT),BorderFactory.createEmptyBorder(7,7,7,7));
    _border6 = BorderFactory.createEmptyBorder(7,7,7,7);
    _border7 = BorderFactory.createEmptyBorder(7,7,7,7);
    _border9 = BorderFactory.createEmptyBorder(10,25,10,35);
    _border10 = BorderFactory.createEmptyBorder(0,10,0,10);
    _mainPanel.setBorder(_border6);
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _selectionsPanel.setLayout(_selectionsPanelBorderLayout);
    _buttonsPanel.setLayout(_buttonsPanelFlowLayout);
    _okButton.setText(OK);
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    _okButton.setEnabled(false);

    _cancelButton.setText(CANCEL);
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _selectionsPanel.setBorder(_border5);
    _separationLabel.setText(PAD_SEPARATION_PROMPT + ":  ");
    _spacingRadioButton.setSelected(true);
    _spacingRadioButton.setText(SPACING_PROMPT);
    _spacingRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        radioButtonSeparation_actionPerformed(e);
      }
    });
    _offsetRadioButton.setText(OFFSET_PROMPT);
    _offsetRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        radioButtonSeparation_actionPerformed(e);
      }
    });
    _xValueLabel.setText(X_VALUE_PROMPT + ":  ");
    _yValueLabel.setText(Y_VALUE_PROMPT + ":  ");
    _valuesPanel.setBorder(_border7);
    _valuesPanel.setLayout(_valuesPanelPairLayout);
    _xyPanelSelections.setBorder(_border2);
    _xyPanelSelections.setLayout(_xyPanelSelectionsBorderLayout);
    _separationOptionsPanel.setLayout(_separationOptionsFlowLayout);
    _separationOptionsFlowLayout.setAlignment(FlowLayout.LEFT);
    _separationOptionsFlowLayout.setHgap(7);
    _landPatternPadTablePanel.setBorder(_border3);
    _landPatternPadTablePanel.setLayout(_landPatternPadTableBorderLayout);
    _scrollLandPatternPadTable.setPreferredSize(new Dimension(200, 80));
    this.setTitle(TITLE);

    _xValueTextField.setDocument(_xValueDocument);
    _yValueTextField.setDocument(_yValueDocument);

    _xValueTextField.setColumns(6);
    _yValueTextField.setColumns(6);

    _xValueTextField.setValue(new Double(0.0));
    _yValueTextField.setValue(new Double(0.0));

    _xValueDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _yValueDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));

    _xValueTextField.addFocusListener(_textFieldFocusAdapter);
    _yValueTextField.addFocusListener(_textFieldFocusAdapter);

    _innerButtonsPanel.setLayout(_innerButtonsPanelGridLayout);
    _innerButtonsPanelGridLayout.setColumns(2);
    _innerButtonsPanelGridLayout.setHgap(20);
    _buttonsPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _imagePanel.setLayout(_imagePanelFlowLayout);
    _imagePanel.setBorder(_border9);
    _imageLabel.setBorder(_border10);
    this.getContentPane().add(_mainPanel, BorderLayout.CENTER);
    _mainPanel.add(_selectionsPanel, BorderLayout.CENTER);
    _mainPanel.add(_buttonsPanel, BorderLayout.SOUTH);
    _buttonsPanel.add(_innerButtonsPanel, null);
    _innerButtonsPanel.add(_okButton, null);
    _innerButtonsPanel.add(_cancelButton, null);
    _mainPanel.add(_landPatternPadTablePanel, BorderLayout.NORTH);
    _landPatternPadTablePanel.add(_scrollLandPatternPadTable, BorderLayout.CENTER);
    _landPatternPadTable = new PadArrayTable(this, _project.getDisplayUnits());
    _scrollLandPatternPadTable.getViewport().add(_landPatternPadTable, null);
    _landPatternPadTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    _selectionsPanel.add(_xyPanelSelections, BorderLayout.SOUTH);
    _selectionsPanel.add(_separationOptionsPanel, BorderLayout.NORTH);
    _xyPanelSelections.add(_valuesPanel, BorderLayout.CENTER);
    _valuesPanel.add(_xValueLabel);
    _valuesPanel.add(_xValueTextField);
    _valuesPanel.add(_yValueLabel);
    _valuesPanel.add(_yValueTextField);
    _separationOptionsPanel.add(_separationLabel, null);
    _separationOptionsPanel.add(_spacingRadioButton, null);
    _separationOptionsPanel.add(_offsetRadioButton, null);
    _selectionsPanel.add(_imagePanel, BorderLayout.CENTER);
    _imagePanel.add(_imageLabel, null);
    _separationButtonGroup.add(_offsetRadioButton);
    _separationButtonGroup.add(_spacingRadioButton);

    updateTextFieldDecimalFormats();
  }

  /**
   * Extra initialization needed, but not called by the UI designer.
   * @author Carli Connally
   */
  private void init()
  {
    FontMetrics fontMetrics = getFontMetrics(getFont());

    _landPatternPadTable.setModel(_landPatternPadTableModel);
    _landPatternPadTableModel.setDisplayUnits(_project.getDisplayUnits());

    //set up appropriate table dimensions
    _landPatternPadTable.setPreferredColumnWidths();
    int prefHeight = fontMetrics.getHeight() + TEXT_HEIGHT_MARGIN;
//    _landPatternPadTable.setRowHeight(prefHeight);
    Dimension prefScrollSize = new Dimension(300, prefHeight * 2 + 3);
    _scrollLandPatternPadTable.setPreferredSize(prefScrollSize);

    //set the preferred width of the x and y positions such that they can display up to 8 digits
    int prefWidth = fontMetrics.stringWidth("99999999") + TEXT_WIDTH_MARGIN;
    _xValueTextField.setPreferredSize(new Dimension(prefWidth, (int)_xValueTextField.getPreferredSize().getHeight()));
    _yValueTextField.setPreferredSize(new Dimension(prefWidth, (int)_yValueTextField.getPreferredSize().getHeight()));
    // need to re-initialize the text fields after the preferred width calculations
    _xValueTextField.setValue(new Double(0.0));
    _yValueTextField.setValue(new Double(0.0));

    //load the images and display the correct one
    _offsetSeparationImage = Image5DX.getImageIcon(Image5DX.EC_SEPARATION_BY_OFFSET);
    _spacingSeparationImage = Image5DX.getImageIcon(Image5DX.EC_SEPARATION_BY_SPACING);

    displaySeparationImage();

    pack();
  }

  /**
   * Pad(s) specified will be added to the land pattern.  Dialog will close.
   * @param e ActionEvent Action Event fired
   * @author Laura Cormos
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    final int rows = (Integer)_landPatternPadTableModel.getValueAt(0,PadArrayTable.ROWS_COLUMN_INDEX);
    final int columns = (Integer)_landPatternPadTableModel.getValueAt(0,PadArrayTable.COLUMNS_COLUMN_INDEX);
    final double userUnitsDx = (Double)_landPatternPadTableModel.getValueAt(0,PadArrayTable.DX_COLUMN_INDEX);
    final double userUnitsDy = (Double)_landPatternPadTableModel.getValueAt(0,PadArrayTable.DY_COLUMN_INDEX);

    if ((rows * columns) == 0 || (userUnitsDx * userUnitsDy) == 0) // nothing to add
    {
      dispose();
      return;
    }

    final Double rotation = (Double)_landPatternPadTableModel.getValueAt(0,PadArrayTable.ROTATION_COLUMN_INDEX);
    final String shape = (String)_landPatternPadTableModel.getValueAt(0,PadArrayTable.SHAPE_COLUMN_INDEX);
    final String type = (String)_landPatternPadTableModel.getValueAt(0,PadArrayTable.TYPE_COLUMN_INDEX);

    // pads are always added relative to the landpattern's origin
    final int anchorx = 0;
    final int anchory = 0;
    double userUnitsXSpaceValue = new Double(_xValueTextField.getText()).doubleValue();
    double userUnitsYSpaceValue = new Double(_yValueTextField.getText()).doubleValue();

    boolean isOffset = false;

    if (_offsetRadioButton.isSelected())
      isOffset = true;

    // if offset is chosen, convert to spacing
    if (isOffset)
    {
      userUnitsXSpaceValue = Double.parseDouble(_xValueTextField.getText()) - userUnitsDx;
      userUnitsYSpaceValue = Double.parseDouble(_yValueTextField.getText()) - userUnitsDy;
    }

    // convert dimensions entered from display units into nanometers
    MathUtilEnum units = _project.getDisplayUnits();
    final int xSpaceValue = (int)MathUtil.convertUnits(userUnitsXSpaceValue, units, MathUtilEnum.NANOMETERS);
    final int ySpaceValue = (int)MathUtil.convertUnits(userUnitsYSpaceValue, units, MathUtilEnum.NANOMETERS);
    final int dx = (int)MathUtil.convertUnits(userUnitsDx, units, MathUtilEnum.NANOMETERS);
    final int dy = (int)MathUtil.convertUnits(userUnitsDy, units, MathUtilEnum.NANOMETERS);

    // calculate the x and y coordinate of the center of the pad array relative
    // to the center of the lower left pad. These are used to give the whole pad
    // array a (0,0) coordinate that is located at its center
    final int xCenterOffset = ((columns - 1) * (xSpaceValue + dx)) / 2;
    final int yCenterOffset = ((rows - 1) * (ySpaceValue + dy)) / 2;
    
    //comment by sheng chuan in order to allow list submit instead of one by one
    //createPadArrayProgressCancelDialog(rows*columns);
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(MainMenuGui.getInstance(),
                                      StringLocalizer.keyToString("ECP_BUSYCANCELDIALOG_SETTING_TABLE_VALUES_MESSAGE_KEY"),
                                      StringLocalizer.keyToString("ECP_BUSYCANCELDIALOG_SETTING_TABLE_VALUES_TITLE_KEY"),
                                      true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, MainMenuGui.getInstance());
    busyCancelDialog.setCursor(new Cursor(Cursor.WAIT_CURSOR));

    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        _hasException = false;
        java.util.List <LandPatternPad> existingLandPatternPads = _landPattern.getLandPatternPads();
        try
        {
          _commandManager.trackState(new UndoState());
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_ADD_PAD_ARRAY_KEY"));
          TimerUtil time = new TimerUtil();
          time.start();
          
          ShapeEnum shapeEnum;
          if (shape.equals(StringLocalizer.keyToString("CAD_RECT_SHAPE_KEY")))
            shapeEnum = ShapeEnum.RECTANGLE;
          else
            shapeEnum = ShapeEnum.CIRCLE;
          java.util.List<ComponentCoordinate> componentCoordinate = new java.util.ArrayList<ComponentCoordinate>();
          
          for (int row = 0; row < rows; row++)
          {
            for (int column = 0; column < columns; column++)
            {
              componentCoordinate.add(new ComponentCoordinate(
                  ((anchorx + column * (xSpaceValue + dx)) - xCenterOffset),
                  ((anchory + row * (ySpaceValue + dy)) - yCenterOffset)));
            }
          }
          try
          {
            if (type.equals(StringLocalizer.keyToString("CAD_SM_KEY")))
            {
              _commandManager.execute(new CreateSurfaceMountLandPatternPadCommand(_landPattern, componentCoordinate,
                                                                                  rotation.doubleValue(), dy, dx, shapeEnum));
            }
            else
            {
              _commandManager.execute(new CreateThroughHoleLandPatternPadCommand(_landPattern, componentCoordinate,
                                                                                 rotation.doubleValue(), dy, dx, shapeEnum));
            }
          }
          catch (XrayTesterException ex)
          {
            _hasException = true;
            MessageDialog.showErrorDialog(null, ex.getLocalizedMessage(),
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          }
          time.stop();
          System.out.println(time.getElapsedTimeInMillis());
        }
        finally
        {
          _commandManager.endCommandBlock();
        }
        if(_hasException == false)
        {
          java.util.List <LandPatternPad> newLandPatternPads = new java.util.ArrayList();
          newLandPatternPads.addAll(_landPattern.getLandPatternPads());
          newLandPatternPads.removeAll(existingLandPatternPads);        
        }
        existingLandPatternPads.clear();
        while(busyCancelDialog.isVisible() == false)
        {
          try
          {
            Thread.sleep(100);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }
        }
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            busyCancelDialog.dispose();
          }
        });
      }
    });
    busyCancelDialog.setVisible(true);   
    setVisible(false);
    dispose();
  }

  /**
   * Cancels the operation.  Dialog closes. State of the land pattern remains the same as before the dialog was opened.
   * @param e ActionEvent Action Event fired
   * @author Carli Connally
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    setVisible(false);
    dispose();
  }

  /**
   * Changes the image to match the selection.
   * @param e ActionEvent Action Event fired
   * @author Carli Connally
   */
  private void radioButtonSeparation_actionPerformed(ActionEvent e)
  {
    displaySeparationImage();
    /**@todo do we need to clear the data in the text fields?*/
  }

  /**
   * Display the image example that matches the type of Separation selected
   * @author Carli Connally
   */
  private void displaySeparationImage()
  {
    if(_offsetRadioButton.isSelected())
    {
      _imageLabel.setIcon(_offsetSeparationImage);
    }
    else
    {
      _imageLabel.setIcon(_spacingSeparationImage);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateTextFieldDecimalFormats()
  {
    StringBuffer decimalFormat = new StringBuffer("########0.");
    MathUtilEnum unitsEnum = _project.getDisplayUnits();

    int decimalPlaces = MeasurementUnits.getMeasurementUnitDecimalPlaces(unitsEnum);
    for (int i = 0; i < decimalPlaces; i++)
      decimalFormat.append("0"); // use a "0" if you want to show trailing zeros, use a "#" if you don't

    _xValueTextField.setFormat(new DecimalFormat(decimalFormat.toString()));
    _yValueTextField.setFormat(new DecimalFormat(decimalFormat.toString()));
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleTextFieldData(NumericTextField numericTextField)
  {
    Assert.expect(numericTextField != null);

    Double pos;
    try
    {
      pos = numericTextField.getDoubleValue();
    }
    catch (ParseException ex)
    {
      // this typically happens when the user deletes all characters from the field -- same as zero
      pos = new Double(0.0);
    }
    numericTextField.setValue(pos);
  }

  /**
   * @author Laura Cormos
   */
  void setOKButtonState(boolean enabled)
  {
    _okButton.setEnabled(enabled);
  }


  /**
   * @author Wei Chin
   */
  private void createPadArrayProgressCancelDialog(int totalPads)
  {
     _addPadArrayProgressCancelDialog = new ProgressDialog(
      MainMenuGui.getInstance(),
      StringLocalizer.keyToString("CAD_ADDING_PAD_ARRAY_TITLE_KEY"),
      StringLocalizer.keyToString("CAD_ADDING_PAD_ARRAY_MESSAGE_KEY"),
      StringLocalizer.keyToString("CAD_ADDING_PAD_ARRAY_COMPLETE_MESSAGE_KEY"),
      StringLocalizer.keyToString("CAD_ADDING_PAD_ARRAY_CLOSE_MESSAGE_KEY"),
      StringLocalizer.keyToString("CAD_ADDING_PAD_ARRAY_CANCEL_MESSAGE_KEY"),
      StringLocalizer.keyToString("CAD_ADDING_PAD_ARRAY_STATUS_MESSAGE_KEY"),
      0, totalPads, true);

    _addPadArrayProgressCancelDialog.addCancelActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _userAbort = true;
      }
    });

//    SwingUtils.setFont(_addPadArrayProgressCancelDialog, _dialogFont);
    _addPadArrayProgressCancelDialog.pack();
    SwingUtils.centerOnComponent(_addPadArrayProgressCancelDialog, MainMenuGui.getInstance());
  }
}
