package com.axi.v810.gui.testDev;


import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.algoTuner.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.plaf.basic.*;

/**
 * This class organizes all the gui components (toolbar, menu, work panels, etc) for the
 * Test Development Tool
 * Main Menu will use this class to transform itself into the Test Development Tool
 * @author Andy Mechtenberg
 */
public class TestDev extends AbstractEnvironmentPanel implements Observer
{
  private Project _project = null;
  private BoardType _currentBoardType = null;
  private ComponentType _activeComponentType = null;
  private JointTypeChoice _activeJointTypeChoice;
  private SubtypeChoice _activeSubtypeChoice;
  private Object _activePanelBoard;
  private MainMenuGui _mainUI = null;
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private ProjectObservable _projectObservable = ProjectObservable.getInstance();

  private TaskPanelScreenEnum _currentScreen = null;
  private boolean _environmentIsActive = false;
  private boolean _revertToHomeAtStart = false;
  private boolean _populateProjectData = false;
  private static boolean _isInSurfaceMappingScreen = false;
  private AbstractTaskPanel _testDevLastTaskPanel = null;
  private TaskPanelScreenEnum _testDevLastScreen = null;

  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  private static Config _config = Config.getInstance();

  // Not user visible strings -- used to switch in the CardLayout between different work spaces
  private final static String _HOMEPANEL = "Home";
  private final static String _SETUPPANEL = "Setup";
  private final static String _MANUAL_ALIGNMENT = "Manual Alignment";
  private final static String _PSP = "PSP";
  private final static String _SCAN_PATH_REVIEW = "Scan Path Review";
  private final static String _ADJUSTCADPANEL = "Adjust CAD";
  private final static String _GROUPINGSPANEL = "Groupings";
  private final static String _INITIALTUNERPANEL = "Initial Tuning";
  private final static String _TUNERPANEL = "Tuner";

  private int _homeScreen;
  private int _panelSetupScreen;
  private int _manualAlignmentScreen;
  private int _pspScreen;
  private int _scanPathReviewScreen;
  private int _adjustCadScreen;
  private int _modifySubtypesScreen;
  private int _initialTuningScreen;
  private int _fineTuningScreen;

  private String[] _testDevNavigationDisabledButtonNames = {StringLocalizer.keyToString("MM_GUI_HOME_DISABLED_KEY"),
                                                            StringLocalizer.keyToString("MM_GUI_SETUP_DISABLED_KEY"),
                                                            StringLocalizer.keyToString("MM_GUI_MANUAL_ALIGNMENT_DISABLED_KEY"),
                                                          // StringLocalizer.keyToString("MM_GUI_PSP_DISABLED_KEY"),
                                                            StringLocalizer.keyToString("MM_GUI_EDIT_CAD_DISABLED_KEY"),
                                                            StringLocalizer.keyToString("MM_GUI_GROUPINGS_DISABLED_KEY"),
                                                            StringLocalizer.keyToString("MM_GUI_INITIAL_TUNE_DISABLED_KEY"),
                                                            StringLocalizer.keyToString("MM_GUI_TUNER_DISABLED_KEY")};

  private String[] _testDevNavigationButtonNames = {StringLocalizer.keyToString("MM_GUI_HOME_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_SETUP_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_MANUAL_ALIGNMENT_KEY"),
                                                   // StringLocalizer.keyToString("MM_GUI_PSP_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_EDIT_CAD_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_GROUPINGS_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_INITIAL_TUNE_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_TUNER_KEY")};

  private String[] _pspTestDevNavigationDisabledButtonNames = {StringLocalizer.keyToString("MM_GUI_HOME_DISABLED_KEY"),
                                                            StringLocalizer.keyToString("MM_GUI_SETUP_DISABLED_KEY"),
                                                            StringLocalizer.keyToString("MM_GUI_MANUAL_ALIGNMENT_DISABLED_KEY"),
                                                            StringLocalizer.keyToString("MM_GUI_PSP_DISABLED_KEY"),
                                                            StringLocalizer.keyToString("MM_GUI_EDIT_CAD_DISABLED_KEY"),
                                                            StringLocalizer.keyToString("MM_GUI_GROUPINGS_DISABLED_KEY"),
                                                            StringLocalizer.keyToString("MM_GUI_INITIAL_TUNE_DISABLED_KEY"),
                                                            StringLocalizer.keyToString("MM_GUI_TUNER_DISABLED_KEY")};

  private String[] _pspTestDevNavigationButtonNames = {StringLocalizer.keyToString("MM_GUI_HOME_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_SETUP_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_MANUAL_ALIGNMENT_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_PSP_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_EDIT_CAD_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_GROUPINGS_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_INITIAL_TUNE_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_TUNER_KEY")};
  
  private String[] _pspTestDevWithScanPathReviewNavigationButtonNames = {StringLocalizer.keyToString("MM_GUI_HOME_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_SETUP_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_MANUAL_ALIGNMENT_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_PSP_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_SCAN_PATH_REVIEW_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_EDIT_CAD_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_GROUPINGS_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_INITIAL_TUNE_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_TUNER_KEY")};
  
  private String[] _pspTestDevWithScanPathReviewDisabledButtonNames = {StringLocalizer.keyToString("MM_GUI_HOME_DISABLED_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_SETUP_DISABLED_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_MANUAL_ALIGNMENT_DISABLED_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_PSP_DISABLED_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_SCAN_PATH_REVIEW_DISABLED_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_EDIT_CAD_DISABLED_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_GROUPINGS_DISABLED_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_INITIAL_TUNE_DISABLED_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_TUNER_DISABLED_KEY")};
  
  private String[] _testDevWithScanPathReviewNavigationButtonNames = {StringLocalizer.keyToString("MM_GUI_HOME_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_SETUP_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_MANUAL_ALIGNMENT_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_SCAN_PATH_REVIEW_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_EDIT_CAD_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_GROUPINGS_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_INITIAL_TUNE_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_TUNER_KEY")};
  
  private String[] _testDevWithScanPathReviewDisabledButtonNames = {StringLocalizer.keyToString("MM_GUI_HOME_DISABLED_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_SETUP_DISABLED_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_MANUAL_ALIGNMENT_DISABLED_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_SCAN_PATH_REVIEW_DISABLED_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_EDIT_CAD_DISABLED_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_GROUPINGS_DISABLED_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_INITIAL_TUNE_DISABLED_KEY"),
                                                                      StringLocalizer.keyToString("MM_GUI_TUNER_DISABLED_KEY")};
  


  // Not user visible strings -- used to switch in the CardLayout between graphics engines
  private final static String _PANELCAD = "Panel CAD";
  private final static String _BOARDCAD = "BOARD CAD";
  private final static String _PACKAGECAD = "Package CAD";
  private final static String _LANDPATTERNCAD = "Land Pattern CAD";

  private TestDevPersistance _guiPersistSettings;
  private boolean _closingProject = false;

  private HomePanel _homePanel = null;
  private PanelSetupPanel _panelSetupPanel = null;
  private ManualAlignmentPanel _manualAlignmentPanel = null;
  private PspPanel _pspPanel = null;
  //Kee chin Seong, Scan Path Review Panel
  private ScanPathReviewPanel _scanPathReviewPanel = null;
  private EditCadPanel _adjustCadPanel = null;
  private GroupingsPanel _groupingsPanel = null;
  private InitialTuningPanel _initialTuningPanel = null;
  private TunerPanel _tunerPanel = null;
  private FineTuningImageGraphicsEngineSetup _fineTuningImageGraphicsEngineSetup;

  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  private NavigationPanel _navigationPanel = null;
  private Border _centerPanelBorder;
  private JSplitPane _mainSplitPane = new JSplitPane();
  private JPanel _workSpacePanel = new JPanel();
  private JTabbedPane _cadImageTabbedPane = new JTabbedPane();
  private JPanel _cadGraphicsPanel = new JPanel();
  private DrawCadPanel _panelCadPanel = null;
  private DrawCadPanel _boardCadPanel = null;
  private DrawCadPanel _packageCadPanel = null;
  private DrawCadPanel _landPatternCadPanel = null;
  
  private CreateNewCadPanel _createNewCadPanel = null;
  
  private ImageManagerPanel _imageManagerPanel;
  private JPanel _chartGraphicsPanel;

  private JComboBox _boardTypeSelectComboBox = new JComboBox();
  private boolean _boardTypeSelectComboBoxInToolbar = false;
  private int _boardTypeSelectComboBoxRequesterID = -1;

  // This is used to show progress of setting up the DrawCad panel.
  // Without this, software looks like hanged which is not a good user experience.
  private ProgressDialog _progressDialog = null;
  private int _progressDialogMaxValue = 0;
  private int _currentValue = 0;

  private boolean _isMainSplitterPanelDragged = false;
  private TestDevPersistance _tunerPersistSettings = new TestDevPersistance();
  private boolean _isBoardGraphicsAlreadyDrawnInAlignment = false;

  private ActionListener _boardTypeSelectComboBoxActionListener = new java.awt.event.ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      boardTypeSelectComboBox_actionPerformed(e);
    }
  };

  private ComponentAdapter _cadImageTabbedPaneAdapter = new java.awt.event.ComponentAdapter()
  {
    public void componentResized(ComponentEvent e)
    {
      cadImageTabbedPane_componentResized(e);
    }
  };
  
  private ImageManagerToolBar _imageManagerToolBar;


  /**
   * @author Andy Mechtenberg
   */
  public TestDev(MainMenuGui mainMenu)
  {
    super(mainMenu);
    Assert.expect(mainMenu != null);
    
    _mainUI = mainMenu;  
    jbInit();   
     
  }

  /**
   * @author Andy Mechtenberg
   */
  public com.axi.v810.gui.undo.CommandManager getCommandManager()
  {
    return _commandManager;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static String getComponentDisplayJointType(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    CompPackage compPackage = componentType.getCompPackage();
    if (compPackage.usesOneJointTypeEnum() == false)
      return StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
    else
      return compPackage.getJointTypeEnum().getName();
  }

  /**
   * Return the localized string representation of the subtype information.  This can include untestable, partial untestable, mixed,
   * no test and no load, as well the actual subtyp name
   * @author Andy Mechtenberg
   */
  public static String getComponentDisplaySubtype(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    if (componentType.getComponentTypeSettings().areAllPadTypesNotTestable())
      return StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_UNTESTABLE_KEY");
    else if (componentType.isTestable() == false)
      return StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_PARTIAL_UNTESTABLE_KEY");
    else if (componentType.getComponentTypeSettings().isLoaded() == false)
      return StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOLOAD_KEY");
    else if (componentType.getComponentTypeSettings().isInspected() == false)
      return StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOTEST_KEY");
    else if (componentType.getComponentTypeSettings().areAllPadTypesInspectedFlagsSet() == false)
      return StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_PARTIALTEST_KEY");
    else if (componentType.getComponentTypeSettings().usesOneSubtype())
      return componentType.getComponentTypeSettings().getSubtype().toString();
    else
      return StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static String getComponentDisplayStatus(ComponentType componentType)
  {
    if (componentType.getComponentTypeSettings().areAllPadTypesNotTestable())
      return StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_UNTESTABLE_KEY");
    else if (componentType.isTestable() == false)
      return StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_PARTIAL_UNTESTABLE_KEY");
    else if (componentType.getComponentTypeSettings().isLoaded() == false)
      return StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOLOAD_KEY");
    else if (componentType.getComponentTypeSettings().isInspected() == false)
      return StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOTEST_KEY");
    else if (componentType.getComponentTypeSettings().areAllPadTypesInspectedFlagsSet() == false)
      return StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_PARTIALTEST_KEY");
    else if (componentType.getComponentTypeSettings().usesOneSubtype())
      return StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_TESTABLE_KEY");
    else
      return StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
  }

  /**
   * @author George A. David
   */
  public static boolean isInDeveloperDebugMode()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE);
  }
  
  /**
   * @author Jack Hwee
   */
  public static boolean isInSurfaceMappingMode()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.ENABLE_PSP);
  }
  
   /**
   * @author Jack Hwee
   */
  public static boolean isInSurfaceMapManualAlignmentMode()
  {
    return ManualAlignmentPanel.isSurfaceMapMode();
  }
  
  /**
   * Returns true if Surface Map is currently in Auto Populate tab.
   * @author Ying-Huan.Chu
   */
  public static boolean isInAutoPopulateMode()
  {
    return PspPanel.isInAutoPopulateMode();
  }
  
  /**
   * Returns the entire workspace for the test development tool, from the Toolbar to the status bar, in one JPanel
   * @author Andy Mechtenberg
   */
  public TestDev getTestDevPanel()
  {
//    jbInit();
    return this;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    _centerPanelBorder = BorderFactory.createEmptyBorder(0,0,0,0);
    _centerPanel.setLayout(_centerPanelBorderLayout);
    _centerPanel.setBorder(_centerPanelBorder);
//    _centerPanel.setPreferredSize(new Dimension(1280, 1024));

    _mainSplitPane.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
    //_mainSplitPane.setPreferredSize(new Dimension(1280, 1024));
    _mainSplitPane.setDividerSize(5);

     if (isInSurfaceMappingMode())
     {
        if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE) &&
           Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_TESTPROGRAM_SCANPATH_REVIEWED))
        {
          _navigationPanel = new NavigationPanel(_pspTestDevWithScanPathReviewNavigationButtonNames, _pspTestDevWithScanPathReviewDisabledButtonNames, this);
        }
        else
          _navigationPanel = new NavigationPanel(_pspTestDevNavigationButtonNames, _pspTestDevNavigationDisabledButtonNames, this);
     }
     else
     {
        if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE) &&
          Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_TESTPROGRAM_SCANPATH_REVIEWED))
        {
          _navigationPanel = new NavigationPanel(_testDevWithScanPathReviewNavigationButtonNames, _testDevWithScanPathReviewDisabledButtonNames, this);
        }
        else
          _navigationPanel = new NavigationPanel(_testDevNavigationButtonNames, _testDevNavigationDisabledButtonNames, this);
     }
     
     //Swee Yee Wong - XCR-2348 Change Event that causes verification image warning
    _cadImageTabbedPane.addChangeListener(new ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        if(_cadImageTabbedPane.getSelectedComponent().equals(_imageManagerPanel))
          _guiObservable.stateChanged(null, CadImageTabEnum.IMAGE_WINDOW);
        else if(_cadImageTabbedPane.getSelectedComponent().equals(_cadGraphicsPanel))
          _guiObservable.stateChanged(null, CadImageTabEnum.CAD_WINDOW);
        else if(_cadImageTabbedPane.getSelectedComponent().equals(getChartPanel()))
          _guiObservable.stateChanged(null, CadImageTabEnum.CHART_WINDOW);
      }
    });
     
    setNavigationPanel(_navigationPanel);
    _centerPanel.add(_mainSplitPane, BorderLayout.CENTER);
    _mainSplitPane.add(_workSpacePanel, JSplitPane.TOP);
    _workSpacePanel.setLayout(new CardLayout());//_workSpacePanelBorderLayout);
    _mainSplitPane.add(_cadImageTabbedPane, JSplitPane.BOTTOM);
    _panelCadPanel = new DrawCadPanel();
    _boardCadPanel = new DrawCadPanel();
    _packageCadPanel = new DrawCadPanel();
    _landPatternCadPanel = new DrawCadPanel();
    _createNewCadPanel = new CreateNewCadPanel();
    _cadGraphicsPanel.setLayout(new CardLayout());
//    _cadGraphicsPanel.add(_panelCadPanel, _PANELCAD);
//    _cadGraphicsPanel.add(_boardCadPanel, _BOARDCAD);
//    _cadGraphicsPanel.add(_packageCadPanel, _PACKAGECAD);
//    _cadGraphicsPanel.add(_landPatternCadPanel, _LANDPATTERNCAD);

    _imageManagerPanel = new ImageManagerPanel(false);
    
    try
    {
      if (LicenseManager.isOfflineProgramming())
      {
        _imageManagerToolBar = _imageManagerPanel.getImageManagerToolBar();
        _imageManagerToolBar.setAllowGenerateImageSetWizard(false);
      }
    }
    catch (final BusinessException e)
    {
      
    }
    
    _fineTuningImageGraphicsEngineSetup = getFineTuningImageGraphicsEngineSetup();
    _homePanel = new HomePanel(this);
    _panelSetupPanel = new PanelSetupPanel(this);
    _manualAlignmentPanel = new ManualAlignmentPanel(this, _imageManagerPanel);
   // _pspPanel = new PspPanel(this, _imageManagerPanel);
    _adjustCadPanel = new EditCadPanel(this, _imageManagerPanel);
    _groupingsPanel = new GroupingsPanel(this, _imageManagerPanel);
    _initialTuningPanel = new InitialTuningPanel(this);
    _tunerPanel = new TunerPanel(this, _imageManagerPanel);

    // set all of these really small so the splitter between these panels and graphics/image
    // is allowed to go all the way to the left.
    _homePanel.setMinimumSize(new Dimension(10,10));
    _panelSetupPanel.setMinimumSize(new Dimension(10,10));
    _manualAlignmentPanel.setMinimumSize(new Dimension(10,10));
   // _pspPanel.setMinimumSize(new Dimension(10,10));
    _adjustCadPanel.setMinimumSize(new Dimension(10,10));
    _groupingsPanel.setMinimumSize(new Dimension(10,10));
    _initialTuningPanel.setMinimumSize(new Dimension(10,10));
    _tunerPanel.setMinimumSize(new Dimension(10,10));

    _workSpacePanel.add(_homePanel, _HOMEPANEL);
    _workSpacePanel.add(_panelSetupPanel, _SETUPPANEL);
    _workSpacePanel.add(_manualAlignmentPanel, _MANUAL_ALIGNMENT);
   // _workSpacePanel.add(_pspPanel, _PSP);
    _workSpacePanel.add(_adjustCadPanel, _ADJUSTCADPANEL);
    _workSpacePanel.add(_groupingsPanel, _GROUPINGSPANEL);
    _workSpacePanel.add(_initialTuningPanel, _INITIALTUNERPANEL);
    _workSpacePanel.add(_tunerPanel, _TUNERPANEL);

    addCadPanel();
    addImagePanel();
    _currentScreen = TaskPanelScreenEnum.NULL_SCREEN;

   //  _isPspModeEnabled = Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP);

     if (isInSurfaceMappingMode())
     {
        _pspPanel = new PspPanel(this, _imageManagerPanel);
        _pspPanel.setMinimumSize(new Dimension(10,10));
        _workSpacePanel.add(_pspPanel, _PSP);
     }
     
     if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE) &&
        Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_TESTPROGRAM_SCANPATH_REVIEWED))
     {
       _scanPathReviewPanel = new ScanPathReviewPanel(this, _imageManagerPanel);
       _scanPathReviewPanel.setMinimumSize(new Dimension(10,10));
       _workSpacePanel.add(_scanPathReviewPanel, _SCAN_PATH_REVIEW);
     }
     
    _boardTypeSelectComboBox.setName(".boardTypeSelectComboBox");


    //XCR1587 TestDev Main Splitter Location Persistence (2013 Mar) - by Anthony Fong
    BasicSplitPaneUI paneUi = (BasicSplitPaneUI)_mainSplitPane.getUI();

    paneUi.getDivider().addMouseListener(new MouseAdapter()
    {
        public void mousePressed(MouseEvent e)
        {
             _isMainSplitterPanelDragged = true;

        }
        public void mouseReleased(MouseEvent e)
        {
              if( _isMainSplitterPanelDragged == true)
              {
                if(_currentScreen.equals(TaskPanelScreenEnum.PROJECT_HOME))
                {
                    saveRecipeSummaryMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         _mainSplitPane.getLeftComponent().getSize().width);
                }
                else if(_currentScreen.equals(TaskPanelScreenEnum.PANEL_SETUP))
                {
                    savePanelSetupMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         _mainSplitPane.getLeftComponent().getSize().width);
                }
                else if(_currentScreen.equals(TaskPanelScreenEnum.MANUAL_ALIGNMENT))
                {
                    saveAlignmentSetupMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         _mainSplitPane.getLeftComponent().getSize().width);
                }
                else if(_currentScreen.equals(TaskPanelScreenEnum.PSP))
                {
                    savePSPSurfaceMapMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         _mainSplitPane.getLeftComponent().getSize().width);
                }
                else if(_currentScreen.equals(TaskPanelScreenEnum.SCAN_PATH_REVIEW))
                {
                    saveScanPathReviewMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         _mainSplitPane.getLeftComponent().getSize().width);
                }
                else if(_currentScreen.equals(TaskPanelScreenEnum.ADJUST_CAD))
                {
                    saveAdjustCADMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         _mainSplitPane.getLeftComponent().getSize().width);
                }
                else if(_currentScreen.equals(TaskPanelScreenEnum.MODIFY_SUBTYPES))
                {
                    saveModifySubtypesMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         _mainSplitPane.getLeftComponent().getSize().width);
                }
                else if(_currentScreen.equals(TaskPanelScreenEnum.INITIAL_TUNE))
                {
                    saveInitialTuningMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         _mainSplitPane.getLeftComponent().getSize().width);
                }
                else if(_currentScreen.equals(TaskPanelScreenEnum.ALGORITHM_TUNER))
                {
                    saveFineTuningMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         _mainSplitPane.getLeftComponent().getSize().width);
                }
                else
                {
                  // this should not happen
                  Assert.expect(false);
                }
                _isMainSplitterPanelDragged = false;
              }
         }
     });
  }


  /**
   *  XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   *
   */
  private void initMainSplitterPanelSettings()
  {

    //ROJECT_HOME
    //_mainSplitPane.setDividerLocation(0.4);
    saveRecipeSummaryMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         (int)(0.4*_mainSplitPane.getSize().width));
    _mainSplitPane.doLayout();

    //PANEL_SETUP
    //_mainSplitPane.setDividerLocation(0.4);
    savePanelSetupMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         (int)(0.4*_mainSplitPane.getSize().width));
    //MANUAL_ALIGNMENT
    //_mainSplitPane.setDividerLocation(0.4);
    saveAlignmentSetupMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         (int)(0.4*_mainSplitPane.getSize().width));
    //PSP
    //_mainSplitPane.setDividerLocation(0.4);
    savePSPSurfaceMapMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         (int)(0.4*_mainSplitPane.getSize().width));
    //SCAN_PATH_REVIEW
    //_mainSplitPane.setDividerLocation(0.4);
    saveScanPathReviewMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         (int)(0.4*_mainSplitPane.getSize().width));
    //ADJUST_CAD
    //_mainSplitPane.setDividerLocation((int)_adjustCadPanel.getPreferredSize().getWidth());
    saveAdjustCADMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         (int)_adjustCadPanel.getPreferredSize().getWidth());
    //MODIFY_SUBTYPES
    //_mainSplitPane.setDividerLocation((int) _groupingsPanel.getPreferredSize().getWidth());
    saveModifySubtypesMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         (int)_groupingsPanel.getPreferredSize().getWidth());
    //INITIAL_TUNE
    //_mainSplitPane.setDividerLocation((int)_initialTuningPanel.getPreferredSize().getWidth());
    saveInitialTuningMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         (int)_initialTuningPanel.getPreferredSize().getWidth());
    //ALGORITHM_TUNER
    //_mainSplitPane.setDividerLocation((int)_tunerPanel.getPreferredSize().getWidth());
    saveFineTuningMainSplitterPanelSettings(_mainSplitPane.getSize().height,
                                         _mainSplitPane.getSize().width,
                                         (int)_tunerPanel.getPreferredSize().getWidth());

    //switchToScreenWithPersistedMainSplitter(TaskPanelScreenEnum.ROJECT_HOME);
    _currentScreen = TaskPanelScreenEnum.PROJECT_HOME;
    _mainSplitPane.setDividerLocation(getRecipeSummaryMainSplitterDividerLocation());


  }
  

  /**
   * @author Andy Mechtenberg
   */
  public boolean areClosingProject()
  {
    return _closingProject;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean okToCloseProject()
  {
    // check the state of the project.  If it's not loaded, return true.
    // if it is loaded, but not "dirty", return true
    // if it is loaded, and dirty, ask the user if they want to save
    // if they save, save it and return true
    // if the don't save, return false

    if (_project == null)
      return true;
    _closingProject = true;
    if (currentTaskPanelReadyToFinish() == false)
    {
      _closingProject = false;
      return false;
    }
    else if (_project.hasBeenModified() == false)
      return true;
    else
    {
      LocalizedString saveQuestion = new LocalizedString("MM_GUI_SAVE_CHANGES_KEY",
                                                       new Object[]{_project.getName()});
      JOptionPane pane = new JOptionPane(StringLocalizer.keyToString(saveQuestion), 
                                         JOptionPane.WARNING_MESSAGE, 
                                         _mainUI.isMagnificationChanged() ? JOptionPane.YES_NO_OPTION : JOptionPane.YES_NO_CANCEL_OPTION);
      JDialog dialog = pane.createDialog(_mainUI, 
                                         StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"));
      
      dialog.setContentPane(pane);
      dialog.setDefaultCloseOperation(_mainUI.isMagnificationChanged() ? JDialog.DO_NOTHING_ON_CLOSE : JDialog.DISPOSE_ON_CLOSE);
      dialog.pack();
      dialog.setVisible(true);
      int answer = ((Integer)pane.getValue()).intValue();
      
      if (answer == JOptionPane.YES_OPTION)
      {
        // get current menu bar, not _envMenuBar (may be null)
        if (MainMenuGui.getInstance().getMainMenuBar().saveProject())
        {
          return true;
        }
        else
        {
          _closingProject = false;
          return false;
        }
      }
      else if (answer == JOptionPane.NO_OPTION)
      {
        return true;
      }
      else if (answer == JOptionPane.CANCEL_OPTION)
      {
        _closingProject = false;
        return false;
      }
      else  // all other input (like JOptionPane.CLOSED_OPTION) are treated as CANCEL
      {
        _closingProject = false;
        return false;
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void boardTypeSelectComboBox_actionPerformed(ActionEvent e)
  {
    _currentBoardType = (BoardType)_boardTypeSelectComboBox.getSelectedItem();
    _guiObservable.stateChanged(_currentBoardType, SelectionEventEnum.BOARD_TYPE);
  }

  /**
   * @author Andy Mechtenberg
   */
  public BoardType getCurrentBoardType()
  {
    Assert.expect(_currentBoardType != null);
    return _currentBoardType;
  }

  /**
   * @author George Booth
   */
  public void setCurrentBoardType(BoardType newBoardType)
  {
    Assert.expect(newBoardType != null);
    _boardTypeSelectComboBox.removeActionListener(_boardTypeSelectComboBoxActionListener);
    _currentBoardType = newBoardType;
    _boardTypeSelectComboBox.setSelectedItem(newBoardType);
    _boardTypeSelectComboBox.addActionListener(_boardTypeSelectComboBoxActionListener);
  }

  /**
   * @author George Booth
   */
  void populateBoardTypeSelectComboBox()
  {
    _boardTypeSelectComboBox.removeActionListener(_boardTypeSelectComboBoxActionListener);
    _boardTypeSelectComboBox.removeAllItems();

    List<BoardType> boardTypeList = _project.getPanel().getUsedBoardTypes();
    if (boardTypeList.isEmpty())
    {
      // no boards, special case
      _currentBoardType = null;
      _boardTypeSelectComboBox.setVisible(false);
      return;
    }

    _currentBoardType = boardTypeList.get(0);
    for (BoardType boardType : boardTypeList)
    {
      _boardTypeSelectComboBox.addItem(boardType);
    }
    _boardTypeSelectComboBox.setMaximumSize(_boardTypeSelectComboBox.getPreferredSize());


    // do we need to show or hide the combo box?
    if (_boardTypeSelectComboBox.getItemCount() > 1)
    {
      // add to toolbar if someone asked for it and it's not already there
      if (_boardTypeSelectComboBoxRequesterID > -1)
      {
        if (_boardTypeSelectComboBoxInToolbar == false)
        {
          addBoardSelectInToolbar(_boardTypeSelectComboBoxRequesterID);
        }
      }
      else
      {
        // no one requesed it
        _boardTypeSelectComboBox.setVisible(false);
      }
    }
    else
    {
      // no need to show it
      _boardTypeSelectComboBox.setVisible(false);
    }
  }

  /**
   * @author George Booth
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    Assert.expect(observable != null);

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if(observable instanceof ProjectObservable)
        {
          if (object instanceof ProjectChangeEvent)
          {
            ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)object;
            ProjectChangeEventEnum eventEnum = projectChangeEvent.getProjectChangeEventEnum();
            if ((eventEnum instanceof BoardEventEnum) ||
                (eventEnum instanceof BoardTypeEventEnum))
            {
              // anything here should cause an update
              populateBoardTypeSelectComboBox();
            }
          }
        }
        else
        {
          Assert.expect(false, "No update code for observable: " + observable.getClass().getName());
        }
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    // navigate to Home Panel
    if (_environmentIsActive)
      _navigationPanel.selectDefaultTask();
    else
    {
      _revertToHomeAtStart = true;
      _testDevLastScreen = TaskPanelScreenEnum.PROJECT_HOME;
      _testDevLastTaskPanel = _homePanel;
    }
    _boardTypeSelectComboBox.removeActionListener(_boardTypeSelectComboBoxActionListener);
    _boardTypeSelectComboBox.removeAllItems();
    _boardTypeSelectComboBoxInToolbar = false;
    _boardTypeSelectComboBoxRequesterID = -1;
    _homePanel.unpopulate();
    _panelSetupPanel.unpopulate();
    _manualAlignmentPanel.unpopulate();
     if (isInSurfaceMappingMode())
       _pspPanel.unpopulate();
     
     if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE) &&
        Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_TESTPROGRAM_SCANPATH_REVIEWED) &&
       _scanPathReviewPanel != null)
     {
        _scanPathReviewPanel.unpopulate();
     }
    _adjustCadPanel.unpopulate();
    _groupingsPanel.unpopulate();
    _initialTuningPanel.unpopulate();
    _tunerPanel.unpopulate();
//    if ((_testDevLastTaskPanel == null) || (_revertToHomeAtStart))
    _navigationPanel.disableAllButtons(true);
    finishCurrentTaskPanel();
    _navigationPanel.clearCurrentNavigation();

    // clear out all the graphic engines
    _createNewCadPanel.resetPanelGraphics();
    _panelCadPanel.resetPanelGraphics();
    _boardCadPanel.resetBoardGraphics();
    _packageCadPanel.resetPackageGraphics();
    _landPatternCadPanel.resetLandPatternGraphics();
    _imageManagerPanel.unpopulate();
    if (_guiPersistSettings != null)
    {
      try
      {
        if (_project != null)
          _guiPersistSettings.notifyOfProjectClose(_project.getName());
      }
      catch (DatastoreException de)
      {
        // do nothing.  this is just a gui project persist file, we don't really need
        // to write it, plus it get written "under the covers", so when users see this dialog
        // they freak out a bit because they didn't do anything to cause it.
//        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
//                                      de.getLocalizedMessage(),
//                                      StringLocalizer.keyToString(new LocalizedString("MM_GUI_TDT_NAME_KEY", null)),
//                                      JOptionPane.ERROR_MESSAGE);
      }

    }
    _testDevLastTaskPanel = null;
    _revertToHomeAtStart = true; // ??? false;
    _currentScreen = TaskPanelScreenEnum.NULL_SCREEN;
    _currentTaskPanel = null;
    _project = null;
    _currentBoardType = null;
    _activeComponentType = null;
    _activeJointTypeChoice = null;
    _activeSubtypeChoice = null;
    _activePanelBoard = null;
    // make sure we don't try to populate on start() if one was pending
    _populateProjectData = false;
    _closingProject = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void disableTaskPanelNavigation()
  {
    _navigationPanel.disableAllButtons(false);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void enableTaskPanelNavigation()
  {
    _navigationPanel.enableAllButtons();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateWithProjectData()
  {
    // if developer debug, we'll get the program right here before we populate anything to prevent
    // possible race conditions
    if (isInDeveloperDebugMode())
      MainMenuGui.getInstance().getTestProgramWithReason("Developer Debug mode generating test program. . .",
          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
          _project);

    final BusyCancelDialog dlg = new BusyCancelDialog(
        _mainUI,
        StringLocalizer.keyToString("MM_GUI_LOADING_USER_INTERFACE_KEY"),
        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
        true);
    dlg.pack();
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setCursor(new Cursor(Cursor.WAIT_CURSOR));
    _mainUI.setCursor(new Cursor(Cursor.WAIT_CURSOR));

    // using SwingUtilities here instead of SwingUtils, because I need this to go to the END of the swing queue to all
    // the dialog to come up and not BLOCK
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        dlg.paint(dlg.getGraphics());
        com.axi.util.TimerUtil totalTimer = new com.axi.util.TimerUtil();
        totalTimer.start();

        // now load the persistence files if it exists for this project.
        try
        {
          _guiPersistSettings.notifyOfNewProject(_project.getName());
        }
        catch (DatastoreException de)
        {
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                        de.getLocalizedMessage(),
                                        StringLocalizer.keyToString(new LocalizedString("MM_GUI_TDT_NAME_KEY", null)),
                                        JOptionPane.ERROR_MESSAGE);
        }


        populateBoardTypeSelectComboBox();
        setUnitsInStatus();
        // each panel needs to be populated with data here!
        _homePanel.populateWithProjectData(_project);
        _panelSetupPanel.populateWithProjectData(_project);
        _manualAlignmentPanel.populateWithProjectData(_project);
         if (isInSurfaceMappingMode())
            _pspPanel.populateWithProjectData(_project);
         if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE) &&
          Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_TESTPROGRAM_SCANPATH_REVIEWED)  && 
           _scanPathReviewPanel != null)
         {
            _scanPathReviewPanel.populateWithProjectData(_project);
         }
        _adjustCadPanel.populateWithProjectData(_project);
        _groupingsPanel.populateWithProjectData(_project);
        _initialTuningPanel.populateWithProjectData(_project);
        _tunerPanel.populateWithProjectData(_project);

        com.axi.util.TimerUtil screenTimer = new com.axi.util.TimerUtil();
        screenTimer.start();
        Dimension mainPanelActualSize = _centerPanel.getSize();
        if(_panelCadPanel.isCadDrawed()==false)
        {
          _panelCadPanel.drawPanel(_project);
        }
        CardLayout cardLayout = (CardLayout) (_cadGraphicsPanel.getLayout());
        cardLayout.show(_cadGraphicsPanel, _PANELCAD);
        _cadImageTabbedPane.setSelectedComponent(_cadGraphicsPanel);
        _centerPanel.setPreferredSize(mainPanelActualSize);
        if(_panelCadPanel.isCadDrawed()==false)
        {
          // this pack is needed so the scaleDrawing method below works correctly.
          _mainUI.pack();
          _panelCadPanel.scaleDrawing();
        }
        screenTimer.stop();
//        System.out.println("Total time for setting up CAD Graphics: " + screenTimer.getElapsedTimeInMillis());

        screenTimer.reset();
        screenTimer.start();
        _imageManagerPanel.populateWithProjectData(_project);
        screenTimer.stop();
//        System.out.println("Total Loading for Image Panel: (in mils): " + screenTimer.getElapsedTimeInMillis());

        totalTimer.stop();
//        System.out.println("Total UI loading time (in mils): " + totalTimer.getElapsedTimeInMillis() + "\n");

        // wait until dlg is visible
        while (dlg.isVisible() == false)
        {
          try
          {
            Thread.sleep(200);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }
        }
        dlg.setVisible(false);

        // navigate to Home Panel
        if (_environmentIsActive)
        {
          enableTaskPanelNavigation();
          _navigationPanel.selectDefaultTask();
        }
        else
        {
          _revertToHomeAtStart = true;
          _testDevLastScreen = TaskPanelScreenEnum.PROJECT_HOME;
          _testDevLastTaskPanel = _homePanel;
        }
        _mainUI.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
//        enableTaskPanelNavigation();
      }
    });
    dlg.setVisible(true);
  }

  public void setUnitsInStatus()
  {
    LocalizedString status = new LocalizedString("MMGUI_SETUP_UNITS_STATUS_KEY", new Object[]{_project.getDisplayUnits().toString()});
    _mainUI.setStatusBarText(StringLocalizer.keyToString(status));
  }

  /**
   * Once a panel load has successfully completed, allow the user access to the ui
   * @author Andy Mechtenberg
   * @author George Booth
   */
  public void populateWithProjectData(Project project)
  {
    Assert.expect(project != null);
    Assert.expect(SwingUtilities.isEventDispatchThread());

    if (_guiPersistSettings == null)
      readPersistance();

    unpopulate();

    _project = project;
    _closingProject = false;

    if (_environmentIsActive)
    {
      populateWithProjectData();
      _populateProjectData = false;
    }
    else
      _populateProjectData = true;

    // navigate to Home Panel
    if (_environmentIsActive)
      _navigationPanel.selectDefaultTask();
    else
    {
      _revertToHomeAtStart = true;
      _testDevLastScreen = TaskPanelScreenEnum.PROJECT_HOME;
      _testDevLastTaskPanel = _homePanel;
    }

  }

  /**
   * @author Andy Mechtenberg
   */
  void changeGraphicsToPackage(CompPackage compPackage)
  {
    _packageCadPanel.drawPackage(compPackage);
    _packageCadPanel.scaleDrawing();
  }

  /**
   * @author Wei Chin
   */
  void changeGraphicsToBoard(Board board)
  {
    _boardCadPanel.drawBoard(board);
    _boardCadPanel.scaleDrawing();
    if (isInSurfaceMappingMode())
      _isBoardGraphicsAlreadyDrawnInAlignment = false;
  }
  
  /**
   * @author Andy Mechtenberg
   */
  void switchToBoardGraphics()
  {
    CardLayout cardLayout = (CardLayout)(_cadGraphicsPanel.getLayout());
    cardLayout.show(_cadGraphicsPanel, _BOARDCAD );
  }

  /**
   * @author Andy Mechtenberg
   */
  void switchToPackageGraphics()
  {
    CardLayout cardLayout = (CardLayout)(_cadGraphicsPanel.getLayout());
    cardLayout.show(_cadGraphicsPanel, _PACKAGECAD );
  }

  /**
   * @author Andy Mechtenberg
   */
  void changeGraphicsToLandPattern(LandPattern landPattern)
  {
    _landPatternCadPanel.drawLandPattern(landPattern);
    _landPatternCadPanel.scaleDrawing();
  }

  /**
   * @author Laura Cormos
   */
  void landPatternScaleDrawing()
  {
    _landPatternCadPanel.scaleDrawing();
  }

  /**
   * @author Laura Cormos
   */
  void initGraphicsToLandPattern()
  {
    _landPatternCadPanel.initLandPatternGraphicsEngineSetup();
  }

  /**
   * @author Wei Chin
   */
  void initGraphicsToBoard()
  {
    _boardCadPanel.initBoardGraphicsEngineSetup();
  }

  /**
   * @author Andy Mechtenberg
   */
  void initGraphicsToPackage()
  {
    _packageCadPanel.initPackageGraphicsEngineSetup();
  }
  
  /**
   * @author Wei Chin
   */
  void clearBoardGraphics()
  {
    Dimension mainPanelActualSize = _centerPanel.getSize();
    _boardCadPanel.resetBoardGraphics();
    _centerPanel.setPreferredSize(mainPanelActualSize);
    _mainUI.pack();
  }

  /**
   * @author Andy Mechtenberg
   */
  void clearLandPatternGraphics()
  {
    Dimension mainPanelActualSize = _centerPanel.getSize();
    _landPatternCadPanel.resetLandPatternGraphics();
    _centerPanel.setPreferredSize(mainPanelActualSize);
    _mainUI.pack();
  }

  /**
   * @author Andy Mechtenberg
   */
  void clearPackageGraphics()
  {
    Dimension mainPanelActualSize = _centerPanel.getSize();
    _packageCadPanel.resetPackageGraphics();
    _centerPanel.setPreferredSize(mainPanelActualSize);
    _mainUI.pack();
  }


  /**
   * @author Andy Mechtenberg
   */
  void switchToLandPatternGraphics()
  {
    CardLayout cardLayout = (CardLayout)(_cadGraphicsPanel.getLayout());
    cardLayout.show(_cadGraphicsPanel, _LANDPATTERNCAD );
  }

  /**
   * @author Andy Mechtenberg
   */
  PanelGraphicsEngineSetup getPanelGraphicsEngineSetup()
  {
    return _panelCadPanel.getPanelGraphicsEngineSetup();
  }
  
  /*
   * @author Kee Chin Seong
   */
  CreatePanelGraphicsEngineSetup getCreatePanelGraphicsEngineSetup()
  {
    return _createNewCadPanel.getCreatePanelGraphicsEngineSetup();
  }
  
  /*
   * @author Kee Chin Seong
   */
  void setGroupSelectMode(boolean groupSelectMode)
  {
    _panelCadPanel.setGroupSelectMode(groupSelectMode);
  }
  
  public boolean hasBoardGraphicsEngineSetup()
  {
    return _boardCadPanel.hasBoardGraphicsEngineSetup();
  }
  /**
   * @author Wei Chin
   */
  BoardGraphicsEngineSetup getBoardGraphicsEngineSetup()
  {
    return _boardCadPanel.getBoardGraphicsEngineSetup();
  }

  /**
   * @author Andy Mechtenberg
   */
  CompPackageGraphicsEngineSetup getPackageGraphicsEngineSetup()
  {
    return _packageCadPanel.getCompPackageGraphicsEngineSetup();
  }

  /**
   * @author Andy Mechtenberg
   */
  LandPatternGraphicsEngineSetup getLandPatternGraphicsEngineSetup()
  {
    return _landPatternCadPanel.getLandPatternGraphicsEngineSetup();
  }

  /**
   * @author Andy Mechtenberg
   */
  public FineTuningImageGraphicsEngineSetup getFineTuningImageGraphicsEngineSetup()
  {
    return _imageManagerPanel.getFineTuningGraphicsEngineSetup();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setCurrentImageForRetest()
  {
    if (_fineTuningImageGraphicsEngineSetup.isImageDisplayed())
    {
      ReconstructionRegion reconstructionRegion = _fineTuningImageGraphicsEngineSetup.getCurrentImageRegion();
      TestProgram testProgram = MainMenuGui.getInstance().getTestProgramWithReason(StringLocalizer.keyToString("GUI_FOCUS_NEED_TEST_PROGRAM_KEY"),
         StringLocalizer.keyToString("GUI_FOCUS_ADD_TITLE_KEY"),
         _project);
      testProgram.addUnfocusedReconstructionRegionNeedingRetest(reconstructionRegion);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void configureFocusControl_actionPerformed()
  {
    String message = null;
    if (_fineTuningImageGraphicsEngineSetup.isImageDisplayed())
    {
      ReconstructionRegion rr = _fineTuningImageGraphicsEngineSetup.getCurrentImageRegion();
      SliceNameEnum sliceNameEnum = _fineTuningImageGraphicsEngineSetup.getCurrentSliceNameEnum();
      AddImageForFocusDlg dlg = new AddImageForFocusDlg(_project, rr, sliceNameEnum);
      SwingUtils.centerOnComponent(dlg, _mainUI);
      dlg.setVisible(true);
      dlg.unpopulate();
      dlg.dispose();

//      message = "Component: " + rr.getComponent().getReferenceDesignator() + " Joints: ";
//      Iterator<Pad> it = rr.getPads().iterator();
//      while(it.hasNext())
//      {
//        Pad pad = it.next();
//        message += pad.getName() + " ";
//      }
    }

//    JOptionPane.showMessageDialog(MainMenuGui.getInstance(), message, "Focus", JOptionPane.INFORMATION_MESSAGE);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setActiveTunerSubtype(JointTypeChoice jointTypeChoice, SubtypeChoice subtypeChoice, Object panelBoard)
  {
    Assert.expect(jointTypeChoice != null);
    Assert.expect(subtypeChoice != null);
    Assert.expect(panelBoard != null);

    _activeJointTypeChoice = jointTypeChoice;
    _activeSubtypeChoice = subtypeChoice;
    _activePanelBoard = panelBoard;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean doesActiveJointTypeSubtypePanelBoardChoiceExist()
  {
    if ((_activeJointTypeChoice != null) && (_activeSubtypeChoice != null) && (_activePanelBoard != null))
      return true;
    else
      return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void clearActiveTunerSubtype()
  {
    _activeJointTypeChoice = null;
    _activeSubtypeChoice = null;
    _activePanelBoard = null;
  }

  /**
   * @author Andy Mechtenberg
   */
  public JointTypeChoice getActiveJointTypeChoice()
  {
    Assert.expect(_activeJointTypeChoice != null);
    return _activeJointTypeChoice;
  }

  /**
   * @author Andy Mechtenberg
   */
  public SubtypeChoice getActiveSubtypeChoice()
  {
    Assert.expect(_activeSubtypeChoice != null);
    return _activeSubtypeChoice;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isBoardInstanceSelected()
  {
    Assert.expect(_activePanelBoard != null);
    if (_activePanelBoard instanceof Board)
      return true;
    else
      return false;
  }
  
  /**
   * Before calling this function, we must call isBoardInstanceSelected()
   * to make sure we selected board. This is because _activePanelBoard
   * maybe is a String or Board object.
   *
   * @author Cheah Lee Herng
   */
  public Board getActiveBoardChoice()
  {
    Assert.expect(_activePanelBoard instanceof Board);
    return (Board)_activePanelBoard;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setActiveTunerComponentType()
  {
    if (_fineTuningImageGraphicsEngineSetup.isImageDisplayed())
    {
      ReconstructionRegion rr = _fineTuningImageGraphicsEngineSetup.getCurrentImageRegion();
      _activeComponentType = rr.getComponent().getComponentType();
    }
    else
      _activeComponentType = null;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean doesActiveComponentTypeExist()
  {
    if (_activeComponentType == null)
      return false;
    else
      return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public ComponentType getActiveComponentType()
  {
    Assert.expect(_activeComponentType != null);
    return _activeComponentType;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setActiveVerifyCadComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    _activeComponentType = componentType;
  }

  /**
   * This method will return the chart panel that is just created and added
   * to the cad image tabbed pane.
   * @author Rex Shang
   */
  public void addChartPanel()
  {
    _chartGraphicsPanel = new JPanel();
    _cadImageTabbedPane.add(_chartGraphicsPanel, StringLocalizer.keyToString("MM_GUI_CHART_LABEL_KEY"));
  }

  /**
   * This method will return the chart panel that part of the cad image
   * tabbed pane.
   * @author Rex Shang
   */
  public JPanel getChartPanel()
  {
    Assert.expect(_chartGraphicsPanel != null, "Chart graphics panel is null.");
    return _chartGraphicsPanel;
  }

  /**
   * Clear the content of the chart panel and then remove it from the cad image
   * tabbed pane.
   * @author Rex Shang
   */
  public void removeChartPanel()
  {
    _chartGraphicsPanel.removeAll();
    _cadImageTabbedPane.remove(_chartGraphicsPanel);

    _chartGraphicsPanel = null;
  }
  
  /*
   * This method is to remove the Cad Panel from tabbed pane
   * @author Kee Chin Seong
   */
  public void addCadPanel()
  {
    Assert.expect(_cadGraphicsPanel != null);
    _cadImageTabbedPane.add(_cadGraphicsPanel, StringLocalizer.keyToString("MM_GUI_CAD_LABEL_KEY"));
  }
  
  public void removeCadPanel()
  {
    Assert.expect(_cadGraphicsPanel != null);
    _cadImageTabbedPane.remove(_cadGraphicsPanel);
  }
  
  /*
   * This method is to remove the Image Panel from tabbed pane
   * @author Kee Chin Seong
   */
  public void addImagePanel()
  {
    Assert.expect(_imageManagerPanel != null);
    _cadImageTabbedPane.add(_imageManagerPanel, StringLocalizer.keyToString("MM_GUI_IMAGE_LABEL_KEY"));
  }
  
  public void removeImagePanel()
  {
    Assert.expect(_imageManagerPanel != null);
    _cadImageTabbedPane.remove(_imageManagerPanel);
  }

  /**
   * @author Andy Mechtenberg
   */
  void switchToPanelGraphics()
  {
    CardLayout cardLayout = (CardLayout)(_cadGraphicsPanel.getLayout());
    cardLayout.show(_cadGraphicsPanel, _PANELCAD);
  }

  /**
   * change to a different screen.
   * @param screen the screen to switch to
   * @author Andy Mechtenberg
   */
  public void switchToScreen(TaskPanelScreenEnum screen)
  {
    Assert.expect(screen != null);
    if (_currentScreen.equals(screen))
      return;
    
    _currentScreen = screen;
    
    // get current set of button names (enabled or disabled)
    String buttonNames[] = _navigationPanel.getCurrentButtonNames();
    if(screen.equals(screen.PROJECT_HOME))
    {
      if(_project != null)
      {
        _project.setCheckLatestTestProgram(true);
      }
      switchToTask(_workSpacePanel, _homePanel, _HOMEPANEL);
      _navigationPanel.highlightButton(buttonNames[_homeScreen]);
//      _mainSplitPane.setDividerLocation(0.4);
//      _navigationPanel.clickButton(_testDevNavigationButtonNames[_homeScreen]);
    }
    else if(screen.equals(screen.PANEL_SETUP))
    {
      if(_project != null)
      {
        _project.setCheckLatestTestProgram(true);
      }
      switchToTask(_workSpacePanel, _panelSetupPanel, _SETUPPANEL);
      _navigationPanel.highlightButton(buttonNames[_panelSetupScreen]);
//      _mainSplitPane.setDividerLocation(0.4);
//      _navigationPanel.clickButton(_testDevNavigationButtonNames[_panelSetupScreen]);
    }
    else if(screen.equals(screen.MANUAL_ALIGNMENT))
    {
      if(_project != null)
      {
        _project.setCheckLatestTestProgram(true);
      }
      switchToTask(_workSpacePanel, _manualAlignmentPanel, _MANUAL_ALIGNMENT);
      _navigationPanel.highlightButton(buttonNames[_manualAlignmentScreen]);
//      _mainSplitPane.setDividerLocation(0.4);
//      _navigationPanel.clickButton(_testDevNavigationButtonNames[_manualAlignmentScreen]);
    }
    else if(screen.equals(screen.PSP))
    {
      if(_project != null)
      {
        _project.setCheckLatestTestProgram(true);
      }
      switchToTask(_workSpacePanel, _pspPanel, _PSP);
      _navigationPanel.highlightButton(buttonNames[_pspScreen]);
//      _navigationPanel.clickButton(_testDevNavigationButtonNames[_manualAlignmentScreen]);
    }
    else if(screen.equals(screen.SCAN_PATH_REVIEW))
    {
      if(_project != null)
      {
        _project.setCheckLatestTestProgram(true);
      }
      switchToTask(_workSpacePanel, _scanPathReviewPanel, _SCAN_PATH_REVIEW);
      _navigationPanel.highlightButton(buttonNames[_scanPathReviewScreen]);
//      _navigationPanel.clickButton(_testDevNavigationButtonNames[_manualAlignmentScreen]);
    }
    else if(screen.equals(screen.ADJUST_CAD))
    {
      if(_project != null)
      {
        _project.setCheckLatestTestProgram(true);
      }
      switchToTask(_workSpacePanel, _adjustCadPanel, _ADJUSTCADPANEL);
      _navigationPanel.highlightButton(buttonNames[_adjustCadScreen]);
//      _mainSplitPane.setDividerLocation((int)_adjustCadPanel.getPreferredSize().getWidth());
//      _navigationPanel.clickButton(_testDevNavigationButtonNames[_adjustCadScreen]);
    }
    else if(screen.equals(screen.MODIFY_SUBTYPES))
    {
      if(_project != null)
      {
        _project.setCheckLatestTestProgram(false);
      }
      switchToTask(_workSpacePanel, _groupingsPanel, _GROUPINGSPANEL);
      _navigationPanel.highlightButton(buttonNames[_modifySubtypesScreen]);
//      _mainSplitPane.setDividerLocation((int) _groupingsPanel.getPreferredSize().getWidth());

//      _navigationPanel.clickButton(_testDevNavigationButtonNames[_modifySubtypesScreen]);
    }
    else if(screen.equals(screen.INITIAL_TUNE))
    {
       if(_project != null)
       {
         _project.setCheckLatestTestProgram(true);
       }
      switchToTask(_workSpacePanel, _initialTuningPanel, _INITIALTUNERPANEL);
      _navigationPanel.highlightButton(buttonNames[_initialTuningScreen]);
//      _mainSplitPane.setDividerLocation((int)_initialTuningPanel.getPreferredSize().getWidth());
//      _navigationPanel.clickButton(_testDevNavigationButtonNames[_initialTuningScreen]);
    }
    else if(screen.equals(screen.ALGORITHM_TUNER))
    {
      if(_project != null)
      {
        _project.setCheckLatestTestProgram(false);
      }
      switchToTask(_workSpacePanel, _tunerPanel, _TUNERPANEL);
      _navigationPanel.highlightButton(buttonNames[_fineTuningScreen]);
//      _mainSplitPane.setDividerLocation((int)_tunerPanel.getPreferredSize().getWidth());
//      _navigationPanel.clickButton(_testDevNavigationButtonNames[_fineTuningScreen]);
    }
    else
    {
      // this should not happen
      Assert.expect(false);
    }

    switchToScreenWithPersistedMainSplitter(screen);

  }

 /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * persist the main splitter divider location when change to a different screen.
   * @param screen the screen to switch to
   * @author Anthony Fong
   */
  public void switchToScreenWithPersistedMainSplitter(TaskPanelScreenEnum screen)
  {
    //set main spilter divider location accordingly
    if(screen.equals(TaskPanelScreenEnum.PROJECT_HOME))
    {
        _mainSplitPane.setDividerLocation(getRecipeSummaryMainSplitterDividerLocation());
    }
    else if(screen.equals(TaskPanelScreenEnum.PANEL_SETUP))
    {
        _mainSplitPane.setDividerLocation(getPanelSetupMainSplitterDividerLocation());
    }
    else if(screen.equals(TaskPanelScreenEnum.MANUAL_ALIGNMENT))
    {
        _mainSplitPane.setDividerLocation(getAlignmentSetupMainSplitterDividerLocation());
    }
    else if(screen.equals(TaskPanelScreenEnum.PSP))
    {
        _mainSplitPane.setDividerLocation(getPSPSurfaceMapMainSplitterDividerLocation());
    }
    else if(screen.equals(TaskPanelScreenEnum.SCAN_PATH_REVIEW))
    {
        _mainSplitPane.setDividerLocation(getScanPathReviewMainSplitterDividerLocation());
    }
    else if(screen.equals(TaskPanelScreenEnum.ADJUST_CAD))
    {
        _mainSplitPane.setDividerLocation(getAdjustCADMainSplitterDividerLocation());
    }
    else if(screen.equals(TaskPanelScreenEnum.MODIFY_SUBTYPES))
    {
        _mainSplitPane.setDividerLocation(getModifySubtypesMainSplitterDividerLocation());
    }
    else if(screen.equals(TaskPanelScreenEnum.INITIAL_TUNE))
    {
        _mainSplitPane.setDividerLocation(getInitialTuningMainSplitterDividerLocation());
    }
    else if(screen.equals(TaskPanelScreenEnum.ALGORITHM_TUNER))
    {
        _mainSplitPane.setDividerLocation(getFineTuningMainSplitterDividerLocation());
    }
    else
    {
      // this should not happen
      Assert.expect(false);
    }
  }

  /**
   * @return the current screen
   * @author George A. David
   */
  public TaskPanelScreenEnum getCurrentScreen()
  {
    Assert.expect(_currentScreen != null);
    return _currentScreen;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void showImageWindow()
  {
    Assert.expect(_cadImageTabbedPane != null);

    _cadImageTabbedPane.setSelectedComponent(_imageManagerPanel);
  }
  
  /**
   * XCR-2348 Change Event that causes verification image warning
   * @author Swee Yee Wong
   */
  public boolean isImageWindowSelected()
  {
    Assert.expect(_cadImageTabbedPane != null);

    return _cadImageTabbedPane.getSelectedComponent().equals(_imageManagerPanel);
  }

  /**
   * @author Andy Mechtenberg
   */
  void showCadWindow()
  {
    Assert.expect(_cadImageTabbedPane != null);

    _cadImageTabbedPane.setSelectedComponent(_cadGraphicsPanel);
  }

  /**
   * @author Rex Shang
   */
  public void showChartWindow()
  {
    Assert.expect(_cadImageTabbedPane != null);

    _cadImageTabbedPane.setSelectedComponent(getChartPanel());
  }

  /**
   * This will read in the persist data
   * @author Andy Mechtenberg
   */
  private TestDevPersistance readPersistance()
  {
    initMainSplitterPanelSettings();
    _guiPersistSettings = new TestDevPersistance();
    _guiPersistSettings = (TestDevPersistance)_guiPersistSettings.readSettings();
    if (_project != null)
    {
      try
      {
        _guiPersistSettings.notifyOfNewProject(_project.getName());
      }
      catch(DatastoreException de)
      {
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      de.getLocalizedMessage(),
                                      StringLocalizer.keyToString(new LocalizedString("MM_GUI_TDT_NAME_KEY", null)),
                                      JOptionPane.ERROR_MESSAGE);
      }

    }
    return _guiPersistSettings;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setGUIToPersistValues()
  {
    _mainSplitPane.setDividerLocation(_guiPersistSettings.getDividerLocation());
  }

  /**
   * @author Andy Mechtenberg
   */
  public TestDevPersistance getPersistance()
  {
    Assert.expect(_guiPersistSettings != null);
    return _guiPersistSettings;
  }

  /**
   * @author Bee Hoon
   */
  public boolean isTestDevPersistanceExist()
  {
    return (_guiPersistSettings != null);
  }
  
  /**
   * This is to listen to the split pane divider - we need to save it's state when it changes
   * @author Andy Mechtenberg
   */
  void cadImageTabbedPane_componentResized(ComponentEvent e)
  {
    _guiPersistSettings.setDividerLocation(_mainSplitPane.getDividerLocation());
  }

  /**
   * @author Andy Mechtenberg
   */
  void addBoardSelectInToolbar(int requesterID)
  {
    // keep track of who asked for it in case we need to add it later
    _boardTypeSelectComboBoxRequesterID = requesterID;
    if (_boardTypeSelectComboBox.getItemCount() > 1)
    {
      _envToolBar.addToolBarComponent(requesterID, _boardTypeSelectComboBox);
      _boardTypeSelectComboBoxInToolbar = true;
      _boardTypeSelectComboBox.setVisible(true);
      _boardTypeSelectComboBox.removeActionListener(_boardTypeSelectComboBoxActionListener);
      _boardTypeSelectComboBox.addActionListener(_boardTypeSelectComboBoxActionListener);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getProjectNotes()
  {
    return _homePanel.getProjectNotes();
  }

  /**
   * called when a navigation button is clicked.
   * @param buttonName String
   * @author George Booth
   */
  public void navButtonClicked(String buttonName)
  {
//    System.out.println("TestDev().navButtonClicked()");
//    saveCurrentScreenState();
      changeToSurfaceMappingToolBar(false);
    // get current set of button names (enabled or disabled)
    String buttonNames[] = _navigationPanel.getCurrentButtonNames();
    if (buttonName.equals(buttonNames[_homeScreen]))
    {
      TaskPanelChangeScreenCommand command = new TaskPanelChangeScreenCommand(this, _currentScreen, TaskPanelScreenEnum.PROJECT_HOME);
      _commandManager.execute(command);
//      switchToTask(_workSpacePanel, _homePanel, _HOMEPANEL);
      _currentScreen = TaskPanelScreenEnum.PROJECT_HOME;
      _guiObservable.stateChanged(null, TaskPanelScreenEnum.PROJECT_HOME);
    }
    else if (buttonName.equals(buttonNames[_panelSetupScreen]))
    {
      TaskPanelChangeScreenCommand command = new TaskPanelChangeScreenCommand(this, _currentScreen, TaskPanelScreenEnum.PANEL_SETUP);
      _commandManager.execute(command);
//      switchToTask(_workSpacePanel, _panelSetupPanel, _SETUPPANEL);
      _currentScreen = TaskPanelScreenEnum.PANEL_SETUP;
      _guiObservable.stateChanged(null, TaskPanelScreenEnum.PANEL_SETUP);
    }
    else if (buttonName.equals(buttonNames[_manualAlignmentScreen]))
    {
      TaskPanelChangeScreenCommand command = new TaskPanelChangeScreenCommand(this, _currentScreen, TaskPanelScreenEnum.MANUAL_ALIGNMENT);
      _commandManager.execute(command);
//      switchToTask(_workSpacePanel, _manualAlignmentPanel, _MANUAL_ALIGNMENT);
      _currentScreen = TaskPanelScreenEnum.MANUAL_ALIGNMENT;
      _guiObservable.stateChanged(null, TaskPanelScreenEnum.MANUAL_ALIGNMENT);
    }
    else if (buttonName.equals(buttonNames[_pspScreen]))
    {
      if (isInSurfaceMappingMode())
      {
     // _cadImageTabbedPane.removeTabAt(1)
      changeToSurfaceMappingToolBar(true);
      TaskPanelChangeScreenCommand command = new TaskPanelChangeScreenCommand(this, _currentScreen, TaskPanelScreenEnum.PSP);
      _commandManager.execute(command);
//      switchToTask(_workSpacePanel, _manualAlignmentPanel, _MANUAL_ALIGNMENT);
      _currentScreen = TaskPanelScreenEnum.PSP;
      _guiObservable.stateChanged(null, TaskPanelScreenEnum.PSP);
      }
    }
    else if (buttonName.equals(buttonNames[_scanPathReviewScreen]))
    {
      if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE) &&
         Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_TESTPROGRAM_SCANPATH_REVIEWED))
      {
     // _cadImageTabbedPane.removeTabAt(1)
      changeToSurfaceMappingToolBar(true);
      TaskPanelChangeScreenCommand command = new TaskPanelChangeScreenCommand(this, _currentScreen, TaskPanelScreenEnum.SCAN_PATH_REVIEW);
      _commandManager.execute(command);
//      switchToTask(_workSpacePanel, _manualAlignmentPanel, _MANUAL_ALIGNMENT);
      _currentScreen = TaskPanelScreenEnum.SCAN_PATH_REVIEW;
      _guiObservable.stateChanged(null, TaskPanelScreenEnum.SCAN_PATH_REVIEW);
      }
    }
    else if (buttonName.equals(buttonNames[_adjustCadScreen]))
    {
      TaskPanelChangeScreenCommand command = new TaskPanelChangeScreenCommand(this, _currentScreen, TaskPanelScreenEnum.ADJUST_CAD);
      _commandManager.execute(command);
//      switchToTask(_workSpacePanel, _adjustCadPanel, _ADJUSTCADPANEL);
      _currentScreen = TaskPanelScreenEnum.ADJUST_CAD;
      _guiObservable.stateChanged(null, TaskPanelScreenEnum.ADJUST_CAD);
    }
    else if (buttonName.equals(buttonNames[_modifySubtypesScreen]))
    {
      TaskPanelChangeScreenCommand command = new TaskPanelChangeScreenCommand(this, _currentScreen, TaskPanelScreenEnum.MODIFY_SUBTYPES);
      _commandManager.execute(command);
//      switchToTask(_workSpacePanel, _groupingsPanel, _GROUPINGSPANEL);
      _currentScreen = TaskPanelScreenEnum.MODIFY_SUBTYPES;
      _guiObservable.stateChanged(null, TaskPanelScreenEnum.MODIFY_SUBTYPES);
    }
    else if (buttonName.equals(buttonNames[_initialTuningScreen]))
    {
      TaskPanelChangeScreenCommand command = new TaskPanelChangeScreenCommand(this, _currentScreen, TaskPanelScreenEnum.INITIAL_TUNE);
      _commandManager.execute(command);
//      switchToTask(_workSpacePanel, _initialTuningPanel, _INITIALTUNERPANEL);
      _currentScreen = TaskPanelScreenEnum.INITIAL_TUNE;
      _guiObservable.stateChanged(null, TaskPanelScreenEnum.INITIAL_TUNE);
    }
    else if (buttonName.equals(buttonNames[_fineTuningScreen]))
    {
      TaskPanelChangeScreenCommand command = new TaskPanelChangeScreenCommand(this, _currentScreen, TaskPanelScreenEnum.ALGORITHM_TUNER);
      _commandManager.execute(command);
//      switchToTask(_workSpacePanel, _tunerPanel, _TUNERPANEL);
      _currentScreen = TaskPanelScreenEnum.ALGORITHM_TUNER;
      _guiObservable.stateChanged(null, TaskPanelScreenEnum.ALGORITHM_TUNER);
    }
  }

  /**
   * The environment is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("TestDev().start()");

    // set up menu and toolbar references
    super.startMenusAndToolBar();
    
    // setting up tab index, depending on software.config enablePsp true/false
    setupTabIndex();

    _environmentIsActive = true;
    _mainUI.addStatusBar();
    // initialize menu helper
    if (_project != null)
    {
      // if there is a project loaded, re-load the initial threshold settings -- they might have changed
      try
      {
        ProjectInitialThresholds.getInstance().loadProjectThresholdSet(_project.getInitialThresholdsSetFullPath());
        ProjectInitialRecipeSettings.getInstance().loadProjectRecipeSettingSet(_project.getInitialRecipeSettingSetFullPath());
      }
      catch (DatastoreException dex)
      {
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      dex.getLocalizedMessage(),
                                      StringLocalizer.keyToString(new LocalizedString("MM_GUI_TDT_NAME_KEY", null)),
                                      JOptionPane.ERROR_MESSAGE);
        try
        {
          _project.setInitialThresholdsSetName(ProjectInitialThresholds.getInstance().getSystemDefaultSetName());
          _project.setInitialRecipeSettingSetName(ProjectInitialRecipeSettings.getInstance().getSystemDefaultSetName());
        }
        catch (DatastoreException dex2)
        {
          // do nothing right now -- we just displayed a message
        }
      }
      setUnitsInStatus();
    }
    else
      _mainUI.setStatusBarText(" ");
    // turn on the tool bar for this environment
    addToolBar();

    if (_guiPersistSettings == null)
    {
      readPersistance();
    }
    setGUIToPersistValues();

    if (_populateProjectData)
    {
      populateWithProjectData();
    }
    _populateProjectData = false;

    if (_guiPersistSettings == null)
      readPersistance();
    setGUIToPersistValues();

    // make sure panel load arrows match current panel handler config
    _panelCadPanel.setPanelLoadArrows();
    addGraphicEngineObserver();
    _cadImageTabbedPane.addComponentListener(_cadImageTabbedPaneAdapter);
    // revert to HomePanel (default task)
    if ((_testDevLastTaskPanel == null) || (_revertToHomeAtStart))
    {
      _navigationPanel.selectDefaultTask();
      if (_revertToHomeAtStart)
      {
          _mainSplitPane.setDividerLocation(getRecipeSummaryMainSplitterDividerLocation());
      }
    }
    else
    {
      _currentTaskPanel = _testDevLastTaskPanel;
      _guiObservable.stateChanged(null, _currentScreen);
      _testDevLastTaskPanel.start();
    }

    _projectObservable.addObserver(this);
    if (_panelCadPanel.isCadDrawed())
    {
      //swee yee - 16 June 2014
      //re-enable the GroupSelectMode when switch to testDev
      _panelCadPanel.getPanelGraphicsEngineSetup().getGraphicsEngine().setGroupSelectMode(true);
    }
  }

  /**
   * User has requested an environment change. Is it OK to leave this environment?
   * @return true if environment can be exited
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
//    System.out.println("TestDev().isReadyToFinish()");
    if (currentTaskPanelReadyToFinish())
    {
      // we don't need to close project unless we navigate to Test Exec
      // Main UI will handle this
//      if (okToCloseProject() == false)
//        return false;
//      else
        return true;
    }
    else
      return false;
  }

  /**
   * The environment is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    // finish current task
    _testDevLastScreen = _currentScreen;
    _testDevLastTaskPanel = _currentTaskPanel;
    finishCurrentTaskPanel();
    _boardTypeSelectComboBoxInToolbar = false;
    _boardTypeSelectComboBoxRequesterID = -1;

//    System.out.println("TestDev().finish()\n");
    _cadImageTabbedPane.removeComponentListener(_cadImageTabbedPaneAdapter);
    try
    {
      if (_project != null)
        _guiPersistSettings.writeSettings(_project.getName());
      else
        _guiPersistSettings.writeSettings();
    }
    catch (DatastoreException de)
    {
      // do nothing.  this is just a gui project persist file, we don't really need
      // to write it, plus it get written "under the covers", so when users see this dialog
      // they freak out a bit because they didn't do anything to cause it.
//      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
//                                    de.getLocalizedMessage(),
//                                    StringLocalizer.keyToString(new LocalizedString("MM_GUI_TDT_NAME_KEY", null)),
//                                    JOptionPane.ERROR_MESSAGE);

    }

    _projectObservable.deleteObserver(this);
    removeGraphicEngineObserver();
    
    // remove environment custom menus and toolbar components
    super.finishMenusAndToolBar();
    _mainUI.removeStatusBar();
//    _navigationPanel.clearCurrentNavigation();
    _revertToHomeAtStart = false;
    _environmentIsActive = false;
  }

  /**
   * Check if progress dialog is created yet.
   * @author Lim, Seng Yew
   */
  public boolean isProgressDialogCreated()
  {
    return (_progressDialog != null);
  }
  
  /**
   * Initialize progress dialog and set it visible
   * @param 0 - set board witdh
   *        1 - set board length
   * @author Lim, Seng Yew
   */
  public void initProgressDialog(int type, int maxValue)
  {
    if (_progressDialog != null)
      disposeProgressDialog();
    _progressDialogMaxValue = maxValue;
    _currentValue = 0;
    if (type == 0)
    {
      _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                           StringLocalizer.keyToString("GUI_COMMAND_BOARD_TYPE_SET_WIDTH_KEY"),
                                           StringLocalizer.keyToString("GUI_UPDATING_ALL_COMPONENTS_KEY"),
                                           "All components updated.",
                                           StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                           " / " + _progressDialogMaxValue,
                                           0,
                                           _progressDialogMaxValue,
                                           true);
    }
    else
    {
      _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                           StringLocalizer.keyToString("GUI_COMMAND_BOARD_TYPE_SET_LENGTH_KEY"),
                                           StringLocalizer.keyToString("GUI_UPDATING_ALL_COMPONENTS_KEY"),
                                           "All components updated.",
                                           StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                           " / " + _progressDialogMaxValue,
                                           0,
                                           _progressDialogMaxValue,
                                           true);
    }
    _progressDialog.pack();
    _progressDialog.setSize(new Dimension(500, 175));
    SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());
  }

  /**
   * Update progress dialog complete percentage
   * @author Lim, Seng Yew
   */
  synchronized public void updateProgressDialog()
  {
    if (_progressDialog != null)
    {
      ++_currentValue;
      if (_currentValue % 100 == 0 || _currentValue == _progressDialogMaxValue)
      {
        _progressDialog.updateProgressBarPrecent(_currentValue);
      }
      if (_currentValue == _progressDialogMaxValue)
        disposeProgressDialog();
    }
  }
  
  /**
   * Show progress dialog
   * @author Lim, Seng Yew
   */
  public void showProgressDialog(boolean show)
  {
      _progressDialog.setVisible(show);
  }

  /**
   * Dispose progress dialog and set to null
   * @author Lim, Seng Yew
   */
  public void disposeProgressDialog()
  {
    _currentValue = 0;
    _progressDialogMaxValue = 0;
    _progressDialog.setVisible(false);
    _progressDialog.dispose();
    _progressDialog = null;
  }

  /**
   * @author Jack Hwee
   */
  public void changeToSurfaceMappingToolBar(Boolean inSurfaceMap)
  {
    if (_panelCadPanel != null)
      _panelCadPanel.changeToSurfaceMappingToolBar(inSurfaceMap);
    if (_boardCadPanel != null)
      _boardCadPanel.changeToSurfaceMappingToolBar(inSurfaceMap);
  }
   
   /*
    * Kee, chin Seong
    */
   public void changeToScanPathToolBar(Boolean isScanPathReviewEnabled)
   {
     _panelCadPanel.changeToScanPathToolBar(isScanPathReviewEnabled);
   }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setupTabIndex()
  {
      if (isInSurfaceMappingMode())
      {
        if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE) &&
           Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_TESTPROGRAM_SCANPATH_REVIEWED))
        {
          _homeScreen = 0;
          _panelSetupScreen = 1;
          _manualAlignmentScreen = 2;
          _pspScreen = 3;
          _scanPathReviewScreen = 4;
          _adjustCadScreen = 5;
          _modifySubtypesScreen = 6;
          _initialTuningScreen = 7;
          _fineTuningScreen = 8; //_scanPathReviewScreen        
        }
        else
        {
          _homeScreen = 0;
          _panelSetupScreen = 1;
          _manualAlignmentScreen = 2;
          _pspScreen = 3;
          _adjustCadScreen = 4;
          _modifySubtypesScreen = 5;
          _initialTuningScreen = 6;
          _fineTuningScreen = 7; //_scanPathReviewScreen
        }
      }
      else
      {
        if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE) &&
           Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_TESTPROGRAM_SCANPATH_REVIEWED))
        {
          _homeScreen = 0;
          _panelSetupScreen = 1;
          _manualAlignmentScreen = 2;
          _scanPathReviewScreen = 3;
          _adjustCadScreen = 4;
          _modifySubtypesScreen = 5;
          _initialTuningScreen = 6;
          _fineTuningScreen = 7;
        }
        else
        {
          _homeScreen = 0;
          _panelSetupScreen = 1;
          _manualAlignmentScreen = 2;
          _adjustCadScreen = 3;
          _modifySubtypesScreen = 4;
          _initialTuningScreen = 5;
          _fineTuningScreen = 6;
        }
      }
  }
  
  /**
   * @author kee chin seong
   */
  public NavigationPanel getNavigationPanel()
  {
    Assert.expect(_navigationPanel != null);
    
    return _navigationPanel;
  }
  
  /**
   * @author sham
   */
  public DrawCadPanel getPanelCadPanel()
  {
    return _panelCadPanel;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public CreateNewCadPanel getCreateNewCadPanel()
  {
    return _createNewCadPanel;
  }

  /**
   * @author sham
   */
  public void setPanelCadPanel(DrawCadPanel panelCadPanel)
  {
    _panelCadPanel = panelCadPanel;
  }

  /**
   * @author sham
   */
  public void setEnableSelectRegionToggleButton(boolean enable)
  {
    _panelCadPanel.setEnableSelectRegionToggleButton(enable);
  }

  /**
   * @author sham
   */
  public void switchToVirtualLiveModeDisplay()
  {
    _cadGraphicsPanel.removeAll();
    _panelCadPanel.switchToVirtualLiveModeDisplay();
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void switchToCadCreatorModeDisplay()
  {
    _cadGraphicsPanel.removeAll();
    _panelCadPanel.switchToCadCreatorModeDisplay();
  }

  /**
   * @author sham
   */
  public void switchToTestDevModeDisplay()
  {
    _cadGraphicsPanel.add(_panelCadPanel, _PANELCAD);
    _cadGraphicsPanel.add(_boardCadPanel, _BOARDCAD);
    _cadGraphicsPanel.add(_packageCadPanel, _PACKAGECAD);
    _cadGraphicsPanel.add(_landPatternCadPanel, _LANDPATTERNCAD);
    _panelCadPanel.switchToTestDevModeDisplay();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void saveRecipeSummaryMainSplitterPanelSettings(int spliterHeight, int spliterWidth, int spliterDividerLocation)
  {
    _tunerPersistSettings.setRecipeSummaryMainSplitterHeight(spliterHeight);
    _tunerPersistSettings.setRecipeSummaryMainSplitterWidth(spliterWidth);
    _tunerPersistSettings.setRecipeSummaryMainSplitterDividerLocation(spliterDividerLocation);
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getRecipeSummaryMainSplittereight()
  {
    return _tunerPersistSettings.getRecipeSummaryMainSplitterHeight();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getRecipeSummaryMainSplitterWidth()
  {
    return _tunerPersistSettings.getRecipeSummaryMainSplitterWidth();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getRecipeSummaryMainSplitterDividerLocation()
  {
    return _tunerPersistSettings.getRecipeSummaryMainSplitterDividerLocation();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void savePanelSetupMainSplitterPanelSettings(int spliterHeight, int spliterWidth, int spliterDividerLocation)
  {
    _tunerPersistSettings.setPanelSetupMainSplitterHeight(spliterHeight);
    _tunerPersistSettings.setPanelSetupMainSplitterWidth(spliterWidth);
    _tunerPersistSettings.setPanelSetupMainSplitterDividerLocation(spliterDividerLocation);
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getPanelSetupMainSplittereight()
  {
    return _tunerPersistSettings.getPanelSetupMainSplitterHeight();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getPanelSetupMainSplitterWidth()
  {
    return _tunerPersistSettings.getPanelSetupMainSplitterWidth();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getPanelSetupMainSplitterDividerLocation()
  {
    return _tunerPersistSettings.getPanelSetupMainSplitterDividerLocation();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void saveAlignmentSetupMainSplitterPanelSettings(int spliterHeight, int spliterWidth, int spliterDividerLocation)
  {
    _tunerPersistSettings.setAlignmentSetupMainSplitterHeight(spliterHeight);
    _tunerPersistSettings.setAlignmentSetupMainSplitterWidth(spliterWidth);
    _tunerPersistSettings.setAlignmentSetupMainSplitterDividerLocation(spliterDividerLocation);
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getAlignmentSetupMainSplittereight()
  {
    return _tunerPersistSettings.getAlignmentSetupMainSplitterHeight();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getAlignmentSetupMainSplitterWidth()
  {
    return _tunerPersistSettings.getAlignmentSetupMainSplitterWidth();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getAlignmentSetupMainSplitterDividerLocation()
  {
    return _tunerPersistSettings.getAlignmentSetupMainSplitterDividerLocation();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void savePSPSurfaceMapMainSplitterPanelSettings(int spliterHeight, int spliterWidth, int spliterDividerLocation)
  {
    _tunerPersistSettings.setPSPSurfaceMapMainSplitterHeight(spliterHeight);
    _tunerPersistSettings.setPSPSurfaceMapMainSplitterWidth(spliterWidth);
    _tunerPersistSettings.setPSPSurfaceMapMainSplitterDividerLocation(spliterDividerLocation);
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getPSPSurfaceMapMainSplittereight()
  {
    return _tunerPersistSettings.getPSPSurfaceMapMainSplitterHeight();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getPSPSurfaceMapMainSplitterWidth()
  {
    return _tunerPersistSettings.getPSPSurfaceMapMainSplitterWidth();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getPSPSurfaceMapMainSplitterDividerLocation()
  {
    return _tunerPersistSettings.getPSPSurfaceMapMainSplitterDividerLocation();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void saveScanPathReviewMainSplitterPanelSettings(int spliterHeight, int spliterWidth, int spliterDividerLocation)
  {
    _tunerPersistSettings.setScanPathReviewMainSplitterHeight(spliterHeight);
    _tunerPersistSettings.setScanPathReviewMainSplitterWidth(spliterWidth);
    _tunerPersistSettings.setScanPathReviewMainSplitterDividerLocation(spliterDividerLocation);
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getScanPathReviewMainSplittereight()
  {
    return _tunerPersistSettings.getScanPathReviewMainSplitterHeight();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getScanPathReviewMainSplitterWidth()
  {
    return _tunerPersistSettings.getScanPathReviewMainSplitterWidth();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getScanPathReviewMainSplitterDividerLocation()
  {
    return _tunerPersistSettings.getScanPathReviewMainSplitterDividerLocation();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void saveAdjustCADMainSplitterPanelSettings(int spliterHeight, int spliterWidth, int spliterDividerLocation)
  {
    _tunerPersistSettings.setAdjustCADMainSplitterHeight(spliterHeight);
    _tunerPersistSettings.setAdjustCADMainSplitterWidth(spliterWidth);
    _tunerPersistSettings.setAdjustCADMainSplitterDividerLocation(spliterDividerLocation);
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getAdjustCADMainSplittereight()
  {
    return _tunerPersistSettings.getAdjustCADMainSplitterHeight();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getAdjustCADMainSplitterWidth()
  {
    return _tunerPersistSettings.getAdjustCADMainSplitterWidth();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getAdjustCADMainSplitterDividerLocation()
  {
    return _tunerPersistSettings.getAdjustCADMainSplitterDividerLocation();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void saveModifySubtypesMainSplitterPanelSettings(int spliterHeight, int spliterWidth, int spliterDividerLocation)
  {
    _tunerPersistSettings.setModifySubtypesMainSplitterHeight(spliterHeight);
    _tunerPersistSettings.setModifySubtypesMainSplitterWidth(spliterWidth);
    _tunerPersistSettings.setModifySubtypesMainSplitterDividerLocation(spliterDividerLocation);
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getModifySubtypesMainSplittereight()
  {
    return _tunerPersistSettings.getModifySubtypesMainSplitterHeight();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getModifySubtypesMainSplitterWidth()
  {
    return _tunerPersistSettings.getModifySubtypesMainSplitterWidth();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getModifySubtypesMainSplitterDividerLocation()
  {
    return _tunerPersistSettings.getModifySubtypesMainSplitterDividerLocation();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void saveInitialTuningMainSplitterPanelSettings(int spliterHeight, int spliterWidth, int spliterDividerLocation)
  {
    _tunerPersistSettings.setInitialTuningMainSplitterHeight(spliterHeight);
    _tunerPersistSettings.setInitialTuningMainSplitterWidth(spliterWidth);
    _tunerPersistSettings.setInitialTuningMainSplitterDividerLocation(spliterDividerLocation);
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getInitialTuningMainSplittereight()
  {
    return _tunerPersistSettings.getInitialTuningMainSplitterHeight();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getInitialTuningMainSplitterWidth()
  {
    return _tunerPersistSettings.getInitialTuningMainSplitterWidth();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getInitialTuningMainSplitterDividerLocation()
  {
    return _tunerPersistSettings.getInitialTuningMainSplitterDividerLocation();
  }

   /**
    * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public void saveFineTuningMainSplitterPanelSettings(int spliterHeight, int spliterWidth, int spliterDividerLocation)
  {
    _tunerPersistSettings.setFineTuningMainSplitterHeight(spliterHeight);
    _tunerPersistSettings.setFineTuningMainSplitterWidth(spliterWidth);
    _tunerPersistSettings.setFineTuningMainSplitterDividerLocation(spliterDividerLocation);
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getFineTuningMainSplittereight()
  {
    return _tunerPersistSettings.getFineTuningMainSplitterHeight();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getFineTuningMainSplitterWidth()
  {
    return _tunerPersistSettings.getFineTuningMainSplitterWidth();
  }

  /**
   * XCR1587 TestDev Main Splitter Location Persistence (2013 Mar)
   * @author Anthony Fong
   */
  public int getFineTuningMainSplitterDividerLocation()
  {
    return _tunerPersistSettings.getFineTuningMainSplitterDividerLocation();
  }
  
  /**
   * XCR1650 Clear panel and board graphic engine observer when TestDev environment finish
   * @author Bee Hoon
   */
  private void removeGraphicEngineObserver()
  {
    _panelCadPanel.removePanelGraphicObserver();
    _boardCadPanel.removeBoardGraphicObserver();
  }
  
  /**
   * XCR1650 Add panel and board graphic engine observer when TestDev environment start
   * @author Bee Hoon
   */
  private void addGraphicEngineObserver()
  {
    // checks if the project loaded is panel-based or board-based alignment
    // [XCR-2037 subtask : XCR-2061] (Ying-Huan.Chu)
    if (_project != null)
    {
      // add Panel Graphics Observer no matter it's panel-based or board-based alignment recipe.
      _panelCadPanel.addPanelGraphicObserver();
      if (_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
      {
        // add Board Graphics Observer if it's board-based alignment recipe.
        _boardCadPanel.addBoardGraphicObserver();
      }
    }
  }
  
  /*
   * Kee Chin Seong - need this for controlling speed for adjust cad
   */
  public boolean isAdjustCadCurrentActive()
  {
    return (_currentScreen == TaskPanelScreenEnum.ADJUST_CAD) ? true : false;
  }
  
  /**
   * @author Jack Hwee
   */
  public void updateGroupSelectButton(Boolean isDragComponent)
  {
    _panelCadPanel.updateGroupSelectButton(isDragComponent);
  }

   /**
   * Sets up this DrawCadPanel to display the normal Panel graphics
   *
   * @author Andy Mechtenberg
   */
  public void resetSurfaceMapPanel(Project project)
  {
    Assert.expect(project != null);
    _panelCadPanel.resetSurfaceMapPanel(project);
  }
  
  /**
   * Returns true if board graphics is currently drawn.
   * @author Ying-Huan.Chu
   */
  public boolean isBoardGraphicsAlreadyDrawnInAlignment()
  {
    return _isBoardGraphicsAlreadyDrawnInAlignment;
  }
  
   /**
   * Returns true if board graphics is currently drawn.
   * @author Ying-Huan.Chu
   */
  public void setBoardGraphicsAlreadyDrawnInAlignment(boolean isBoardGraphicsAlreadyDrawnInAlignment)
  {
    _isBoardGraphicsAlreadyDrawnInAlignment = isBoardGraphicsAlreadyDrawnInAlignment;
  }
  
   /**
   * @author weng-jian.eoh
   * 
   * XCR-3589 Combo STD and XXL software GUI
   * 
   * reinitialize all the sub panel that use image panel due to magnification change.
   */
  public void refresh()
  {    
    _environmentPanel.remove(_navigationPanel);
    if (_pspPanel != null)
    {
      _workSpacePanel.remove(_pspPanel);
    }
    _workSpacePanel.remove(_manualAlignmentPanel);
    _workSpacePanel.remove(_adjustCadPanel);
    _workSpacePanel.remove(_groupingsPanel);
    _workSpacePanel.remove(_tunerPanel);
    
    removeImagePanel();
//    
    _manualAlignmentPanel.removeAllObservable();
    _adjustCadPanel.removeAllObservable();
    _groupingsPanel.removeAllObservable();
    _tunerPanel.disable();
//    
    _manualAlignmentPanel= null;
    _adjustCadPanel = null;
    _groupingsPanel = null;
    _tunerPanel = null;
    
    
    _imageManagerPanel.finishAlignmentImagePanel();
    _imageManagerPanel.finishFineTuningImagePanel();
    _imageManagerPanel.finishVerificationImagePanel();
    _imageManagerPanel.finishVerificationImagePanelForHighMag();
    
 
    
    _imageManagerPanel = null;
    
    if (isInSurfaceMappingMode())
    {
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE)
        && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_TESTPROGRAM_SCANPATH_REVIEWED))
      {
        _navigationPanel = new NavigationPanel(_pspTestDevWithScanPathReviewNavigationButtonNames, _pspTestDevWithScanPathReviewDisabledButtonNames, this);
      }
      else
      {
        _navigationPanel = new NavigationPanel(_pspTestDevNavigationButtonNames, _pspTestDevNavigationDisabledButtonNames, this);
      }
    }
    else if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE)
      && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_TESTPROGRAM_SCANPATH_REVIEWED))
    {
      _navigationPanel = new NavigationPanel(_testDevWithScanPathReviewNavigationButtonNames, _testDevWithScanPathReviewDisabledButtonNames, this);
    }
    else
    {
      _navigationPanel = new NavigationPanel(_testDevNavigationButtonNames, _testDevNavigationDisabledButtonNames, this);
    }

    setNavigationPanel(_navigationPanel);
   
    _imageManagerPanel = new ImageManagerPanel(false);
    _fineTuningImageGraphicsEngineSetup = getFineTuningImageGraphicsEngineSetup();
    _manualAlignmentPanel = new ManualAlignmentPanel(this, _imageManagerPanel);
    if (isInSurfaceMappingMode())
    {
      _pspPanel = new PspPanel(this, _imageManagerPanel);
      _pspPanel.setMinimumSize(new Dimension(10, 10));
      _workSpacePanel.add(_pspPanel, _PSP);
    }
    _adjustCadPanel = new EditCadPanel(this, _imageManagerPanel);
    _groupingsPanel = new GroupingsPanel(this, _imageManagerPanel);
    _tunerPanel = new TunerPanel(this, _imageManagerPanel);
    
    _imageManagerToolBar = _imageManagerPanel.getImageManagerToolBar();
    
    _imageManagerToolBar.setAllowGenerateImageSetWizard(false);
    _manualAlignmentPanel.setMinimumSize(new Dimension(10,10));
    _adjustCadPanel.setMinimumSize(new Dimension(10,10));
    _groupingsPanel.setMinimumSize(new Dimension(10,10));
    _tunerPanel.setMinimumSize(new Dimension(10,10));

    _workSpacePanel.add(_manualAlignmentPanel, _MANUAL_ALIGNMENT);
    _workSpacePanel.add(_adjustCadPanel, _ADJUSTCADPANEL);
    _workSpacePanel.add(_groupingsPanel, _GROUPINGSPANEL);
    _workSpacePanel.add(_tunerPanel, _TUNERPANEL);

    addImagePanel();
  }
  
  /**
   * @author Janan Wong
   * XCR-3837: Display untestable Area on virtual live CAD
   */
  public void setEnableDisplayUntestableAreaToggleButton(boolean enable)
  {
   _panelCadPanel.setEnableDisplayUntestableAreaToggleButton(enable);
  }
  
  /**
   * @author Janan Wong
   * XCR-3837: Display untestable Area on virtual live CAD
   */
  public boolean hasGuiPersistanceSettings()
  {
    if (_guiPersistSettings == null)
    {
      return false;
    }
    else
    {
      return true;
    }
  }
}
