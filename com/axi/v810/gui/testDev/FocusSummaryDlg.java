package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 *
 * Dialog used to add multiple instances of a board type
 * to a panel.  This is a modal dialog.
 * @author Andy Mechtenberg
 */
public class FocusSummaryDlg extends EscapeDialog
{
  private Project _project = null;
  private FocusReconstructionRegionWrapper _initialReconstructionRegion;
  private FocusSummaryTableModel _focusSummaryTableModel;

  private BorderLayout _contentPaneBorderLayout = new BorderLayout();
  private JSortTable _summaryTable;
  private JScrollPane _summaryTableScrollPane = new JScrollPane();
  private JPanel _buttonPanel = new JPanel();
  private FlowLayout _buttonPanelFlowLayout = new FlowLayout();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private JPanel _buttonInnerPanel = new JPanel();
  private GridLayout _buttonInnerPanelGridLayout = new GridLayout();

  /**
   * @author Andy Mechtenberg
   */
  private FocusSummaryDlg()
  {
    jbInit();
    pack();
  }

  /**
   * @author Andy Mechtenberg
   */
  public FocusSummaryDlg(Project project)
  {
    super(MainMenuGui.getInstance(), StringLocalizer.keyToString("GUI_FOCUS_SUMMARY_TITLE_KEY"), true);
    Assert.expect(project != null);

    _project = project;
    jbInit();

    pack();
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
  }

  /**
   * @author Andy Mechtenberg
   */
  public FocusSummaryDlg(Project project, FocusReconstructionRegionWrapper reconstructionRegion)
  {
    super(MainMenuGui.getInstance(), StringLocalizer.keyToString("GUI_FOCUS_SUMMARY_TITLE_KEY"), true);
    Assert.expect(project != null);
    Assert.expect(reconstructionRegion != null);

    _project = project;
    _initialReconstructionRegion = reconstructionRegion;
    jbInit();

    pack();
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _project = null;
    _focusSummaryTableModel.unpopulate();
    _initialReconstructionRegion = null;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    Container contentPane = this.getContentPane();
    contentPane.setLayout(_contentPaneBorderLayout);

    JPanel mainPanel = new JPanel();
    mainPanel.setLayout(new BorderLayout());
    mainPanel.setBorder(BorderFactory.createEmptyBorder(10,10,0,10));
//    _summaryTableScrollPane.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    _focusSummaryTableModel = new FocusSummaryTableModel(_project, this);
    _summaryTable = new JSortTable(_focusSummaryTableModel);
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed();
      }
    });

    _buttonInnerPanelGridLayout.setHgap(10);
    _buttonInnerPanel.setLayout(_buttonInnerPanelGridLayout);
    _summaryTableScrollPane.getViewport().add(_summaryTable);

    // set up combo box as editor of column 1
    TableColumn focusMethodColumn = _summaryTable.getColumnModel().getColumn(1);
    JComboBox focusComboBox = new JComboBox();

    focusComboBox.addItem(StringLocalizer.keyToString("GUI_FOCUS_DEFAULT_FOCUS_KEY"));
    focusComboBox.addItem(StringLocalizer.keyToString("GUI_FOCUS_TO_BE_FIXED_KEY"));
    focusComboBox.addItem(StringLocalizer.keyToString("GUI_FOCUS_CUSTOM_FOCUS_KEY"));
    focusMethodColumn.setCellEditor(new DefaultCellEditor(focusComboBox));

    _buttonPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _buttonPanelFlowLayout.setHgap(0);
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    _buttonPanel.add(_buttonInnerPanel);
    _buttonInnerPanel.add(_okButton);
    _buttonInnerPanel.add(_cancelButton);
    this.getRootPane().setDefaultButton(_okButton);

    mainPanel.add(_summaryTableScrollPane, BorderLayout.CENTER);
    mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    contentPane.add(mainPanel, BorderLayout.CENTER);
    if (_initialReconstructionRegion != null)
    {
      int rowToHighlight = _focusSummaryTableModel.getRowForReconstructionRegion(_initialReconstructionRegion);
      if (rowToHighlight != -1)
      {
        _summaryTable.clearSelection();
        _summaryTable.addRowSelectionInterval(rowToHighlight, rowToHighlight);
      }
    }
  }

  /**
   * Board(s) specified will be added to the panel.  Dialog will close.
   * @param e ActionEvent Action Event fired
   * @author Andy Mechtenberg
   */
  private void okButton_actionPerformed(ActionEvent e)
  {

    _focusSummaryTableModel.commitChanges();
    dispose();
  }

  /**
   * Cancels the operation.  Dialog closes. State of the panel remains the same
   * as before the dialog was opened.
   * @param e ActionEvent Action Event fired
   * @author Andy Mechtenberg
   */
  private void cancelButton_actionPerformed()
  {
    dispose();
  }
}
