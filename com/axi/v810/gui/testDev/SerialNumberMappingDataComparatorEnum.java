package com.axi.v810.gui.testDev;

/**
 *
 * @author siew-yeng.phang
 */
public class SerialNumberMappingDataComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static SerialNumberMappingDataComparatorEnum BARCODE_READER_ID = new SerialNumberMappingDataComparatorEnum(++_index);
  public static SerialNumberMappingDataComparatorEnum BOARD_NAME = new SerialNumberMappingDataComparatorEnum(++_index);
  public static SerialNumberMappingDataComparatorEnum SERIAL_NUMBER_SEQUENCE = new SerialNumberMappingDataComparatorEnum(++_index);


  /**
   * @author Siew Yeng
   */
  private SerialNumberMappingDataComparatorEnum(int id)
  {
    super(id);
  }
}
