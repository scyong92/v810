package com.axi.v810.gui.testDev;

import javax.swing.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: SMorTHPadTypeCellEditor</p>
 * <p>Description: Table cell editor for pad  type values (SM, TH)</p>
 * <p>Company:  Agilent Technogies</p>
 * @author Laura Cormos
 * @version 1.0
 */
class SMorTHPadTypeCellEditor extends DefaultCellEditor
{
  //Shape strings
  private final String _SURFACEMOUNT = StringLocalizer.keyToString("CAD_SM_KEY");
  private final String _THROUGHHOLE = StringLocalizer.keyToString("CAD_TH_KEY");

  JComboBox _comboBox = null;

  /**
   * @author Laura Cormos
   * Constructor.
   * Initializes the drop down box with the shape values
   */
  public SMorTHPadTypeCellEditor()
  {
    super(new JComboBox());
    _comboBox = (JComboBox)getComponent();

    _comboBox.addItem(_SURFACEMOUNT);
    _comboBox.addItem(_THROUGHHOLE);
 }
}
