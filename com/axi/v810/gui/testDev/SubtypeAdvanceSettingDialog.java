package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.event.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author chin-seong.kee
 */
public class SubtypeAdvanceSettingDialog extends EscapeDialog implements Observer
{
  private JScrollPane _subtypeScrollPane = new JScrollPane();
  private SubtypeAdvanceSettingsTable _subtypeAdvancedSettingsTable;
  private SubtypeAdvanceSettingsTableModel _subtypeAdvancedSettingsTableModel;

  private JButton _okButton = new JButton();

  private JMenuItem _undoMenuItem = new JMenuItem();
  private JMenuItem _redoMenuItem = new JMenuItem();
  private MainMenuGui _parentComponent;

  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  private KeyStroke _undoKeyStroke = javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_MASK);
  private KeyStroke _redoKeyStroke = javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_Y, KeyEvent.CTRL_MASK);

  private JPanel _mainPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _tablePanel = new JPanel();
  
  private BoardType _currentBoardType;

  private ListSelectionListener _subtypeSettingTableListSelectionListener;
  
  private java.util.List<Subtype> _subtypes = null;
  private java.util.List<Subtype> _selectedSubtypes = new java.util.ArrayList<Subtype>();
  private boolean _isVariableMagnification = false;
  private int _maxDROLevel = Config.getMaximumDynamicRangeOptimizationLevel(); //Siew Yeng - XCR-2549
  private final int _DEFAULT_MINIMUM_SELECTED_SPEED_OPTION_COUNT = 1;
  private final int _MAXIMUM_SELECTED_SPEED_OPTION_COUNT = Config.getInstance().getIntValue(SoftwareConfigEnum.MAXIMUM_SPEED_SELECTION);
  private final int _DEFAULT_MINIMUM_SELECTED_CAMERA_ANGLE_OPTION_COUNT = Config.getInstance().getIntValue(SoftwareConfigEnum.MINIMUM_CAMERA_USED_FOR_RECONSTRUCTION);
  private boolean _isAbleToSelectMultipleSpeed = false;
  private boolean _enableCameraSelection = false;

  public SubtypeAdvanceSettingDialog(java.util.List<Subtype> subtype, BoardType currentBoardType, Frame parent, String title, boolean isVariableMagnification)
  {
    super(parent, title, true);

    _subtypes = subtype;
    _parentComponent = (MainMenuGui)parent;
    _isVariableMagnification = isVariableMagnification;
    _currentBoardType = currentBoardType;
    
    jbInit();
  }

  public SubtypeAdvanceSettingDialog(Subtype subtype, BoardType currentBoardType, Frame parent, String title, boolean isVariableMagnification)
  {
    super(parent, title, true);

    _subtypes = new java.util.ArrayList<Subtype>();
    _subtypes.add(subtype);
    
    _currentBoardType = currentBoardType;
    
    _parentComponent = (MainMenuGui)parent;
    _isVariableMagnification = isVariableMagnification;

    jbInit();
  }

  /**
   * @author Chin Seong
   */
  private void jbInit()
  {
    _enableCameraSelection = (_DEFAULT_MINIMUM_SELECTED_CAMERA_ANGLE_OPTION_COUNT < XrayCameraIdEnum.getAllEnums().size()) && Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE);
    _isAbleToSelectMultipleSpeed = (Project.getCurrentlyLoadedProject().isGenerateByNewScanRoute()) && _MAXIMUM_SELECTED_SPEED_OPTION_COUNT > 1;
    
    _subtypeSettingTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        subtypeAdvanceSettingsTableListSelectionChanged(e);
      }
    };

     _subtypeAdvancedSettingsTableModel = new SubtypeAdvanceSettingsTableModel(_isVariableMagnification, _maxDROLevel, _enableCameraSelection);
     _subtypeAdvancedSettingsTableModel.populateSubtypesWithPanelData(_subtypes);
     
     ProjectObservable.getInstance().addObserver(this);
     
     _subtypeAdvancedSettingsTable = new SubtypeAdvanceSettingsTable(_subtypeAdvancedSettingsTableModel)
     {
        public void mouseReleased(MouseEvent event)
        {
          int selectedRows[] = _subtypeAdvancedSettingsTable.getSelectedRows();
          java.util.List<Object> selectedData = new ArrayList<Object>();
          for(int i = 0; i < selectedRows.length; i++)
          {
              selectedData.add(_subtypeAdvancedSettingsTableModel.getSubtypeAt(selectedRows[i]));
          }
          ListSelectionModel rowSM = _subtypeAdvancedSettingsTable.getSelectionModel();
          rowSM.removeListSelectionListener(_subtypeSettingTableListSelectionListener); // add the listener for a details table row selection

          super.mouseReleased(event);

          _subtypeAdvancedSettingsTable.clearSelection();
            // now, reselect everything

          Iterator it = selectedData.iterator();
          while (it.hasNext())
          {
            int row;
            Object obj = it.next();
            row = _subtypeAdvancedSettingsTableModel.getRowForSubtype((Subtype)obj);
            _subtypeAdvancedSettingsTable.addRowSelectionInterval(row, row);
          }
          SwingUtils.scrollTableToShowSelection(_subtypeAdvancedSettingsTable);
          rowSM.addListSelectionListener(_subtypeSettingTableListSelectionListener); // add the listener for a details table row selection
        }
     };
     _subtypeAdvancedSettingsTable.enableVariableMagnification(_isVariableMagnification);
     
     //Siew Yeng - XCR-2549
     _subtypeAdvancedSettingsTable.setMaxDynamicRangeOptimizationLevel(_maxDROLevel);
     _subtypeAdvancedSettingsTable.enableCameraSelectionColumn(_enableCameraSelection);
     
     _subtypeAdvancedSettingsTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
     _subtypeAdvancedSettingsTable.setCurrentBoardType(_currentBoardType);
     _subtypeAdvancedSettingsTable.setAbleToSelectMultipleSpeed(_isAbleToSelectMultipleSpeed);
     _subtypeAdvancedSettingsTable.updateTableColumnIndex();
     
     _subtypeAdvancedSettingsTable.setSurrendersFocusOnKeystroke(false);
     _subtypeAdvancedSettingsTable.setPreferredColumnWidths();
     _subtypeAdvancedSettingsTable.setupCellEditorsAndRenderers();
     _subtypeAdvancedSettingsTable.addMouseListener(new MouseAdapter()
     {
       public void mouseReleased(MouseEvent e)
       {
         multiEditTableMouseReleased(e);
       }
     });

     ListSelectionModel rowSM = _subtypeAdvancedSettingsTable.getSelectionModel();
     rowSM.addListSelectionListener(_subtypeSettingTableListSelectionListener);

     _tablePanel.setLayout(new BorderLayout());
     _tablePanel.add(_subtypeScrollPane, BorderLayout.CENTER);

     _buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
     _buttonPanel.add(_okButton);
     _okButton.setText(StringLocalizer.keyToString("MMGUI_SELECT_SUBTYPE_BUTTON_KEY"));

     _okButton.addActionListener(new java.awt.event.ActionListener()
     {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
     });

     setupMenuItem();

     _mainPanel.setLayout(new BorderLayout());
     _mainPanel.add(_tablePanel, BorderLayout.CENTER);
     _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);

     this.getContentPane().add(_mainPanel);

     _subtypeAdvancedSettingsTable.setModel(_subtypeAdvancedSettingsTableModel);
     _subtypeScrollPane.getViewport().add(_subtypeAdvancedSettingsTable);
     ColumnResizer.adjustColumnPreferredWidths(_subtypeAdvancedSettingsTable, true);
     setSize(new Dimension((int)_subtypeAdvancedSettingsTable.getPreferredSize().getWidth(), (int)getPreferredSize().getHeight()));
  }

 /**
   * @author Chin Seong
   */
  private void subtypeAdvanceSettingsTableListSelectionChanged(ListSelectionEvent e)
  {
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();

    if (lsm.isSelectionEmpty() || lsm.getValueIsAdjusting())
    {
      // do nothing
      // Make sure cell editor is not activated on first click.
      if(_subtypeAdvancedSettingsTable.getCellEditor() != null)
        _subtypeAdvancedSettingsTable.getCellEditor().cancelCellEditing();
    }
    else
    {
      // turn off event processing until graphics had a chance to update
      _subtypeAdvancedSettingsTable.processSelections(false);
      int[] selectedSubtypeRows = _subtypeAdvancedSettingsTable.getSelectedRows();     
      _selectedSubtypes.clear();
      for(int i = 0; i < selectedSubtypeRows.length; i++)
      {
        _selectedSubtypes.add(_subtypeAdvancedSettingsTableModel.getSubtypeAt(selectedSubtypeRows[i]));
      }
      _subtypeAdvancedSettingsTableModel.setSelectedSubtypes(_selectedSubtypes);

     // wait for graphics
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
           _subtypeAdvancedSettingsTable.processSelections(true);
        }
      });
    }
  }

 /**
   * @author Chin Seong
   */
  private void multiEditTableMouseReleased(MouseEvent e)
  {
    Assert.expect(e != null);

    Point p = e.getPoint();
    final int row = _subtypeAdvancedSettingsTable.rowAtPoint(p);
    final int col = _subtypeAdvancedSettingsTable.columnAtPoint(p);
    // if this cell is not currently selected, exit.  It's confusing to edit a cell that not selected.
    // we could "select" this cell too, but for now, we'll just not.
    if (_subtypeAdvancedSettingsTable.isCellSelected(row, col) == false)
      return;
    
    // ignore any other mouse events than a popup trigger -- we don't want to break the normal L&F
    if (e.isPopupTrigger())
    {
      // start the editing process for this cell
      _subtypeAdvancedSettingsTable.editCellAt(row, col);
      // if this is a combo box cell editor, the editCellAt doesn't show the combo box popup, so let's show that
      // only if this is a combo box editor
      Object obj = _subtypeAdvancedSettingsTable.getEditorComponent();
      if (obj instanceof JComboBox)
      {
        JComboBox comboBox = (JComboBox)obj;
        comboBox.showPopup();
      }
    }
  
    if (col == _subtypeAdvancedSettingsTable._STAGE_SPEED_INDEX && _isAbleToSelectMultipleSpeed)
    {
      Rectangle rect = _subtypeAdvancedSettingsTable.getCellRect(row, col, true);

      final Subtype subtype = _subtypeAdvancedSettingsTableModel.getSubtypeAt(row);
      java.util.List<StageSpeedEnum> stageSpeedEnumList = StageSpeedEnum.getOrderedStageSpeed();
      java.util.List<Integer> listOfSelectedIndeces = new ArrayList<Integer>();
      
      for (StageSpeedEnum speedEnum : subtype.getSubtypeAdvanceSettings().getStageSpeedList())
        listOfSelectedIndeces.add(stageSpeedEnumList.indexOf(speedEnum));
      
      final ScrollableCheckBoxListPopupMenu<StageSpeedEnum> popupMenu = new ScrollableCheckBoxListPopupMenu(stageSpeedEnumList,
                                                                                                            listOfSelectedIndeces, 
                                                                                                            true,
                                                                                                            false,
                                                                                                            _DEFAULT_MINIMUM_SELECTED_SPEED_OPTION_COUNT,
                                                                                                            _MAXIMUM_SELECTED_SPEED_OPTION_COUNT);
      popupMenu.setVisibleRowCount(10);
      popupMenu.setPreferredSize(new Dimension((int) rect.getWidth(), popupMenu.getPreferredSize().height));
      popupMenu.addPopupMenuListener(new PopupMenuListener()
      {
        /**
         * @author Yong Sheng Chuan
         */
        public void popupMenuWillBecomeVisible(PopupMenuEvent e)
        {
          //do nothing
        }

        /**
         * @author Yong Sheng Chuan
         */
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e)
        {
          _subtypeAdvancedSettingsTable.setValueAt(new ArrayList<StageSpeedEnum>(popupMenu.getSelectedValuesList()), row, col);
        }

        /**
         * @author Yong Sheng Chuan
         */
        public void popupMenuCanceled(PopupMenuEvent e)
        {
          //do nothing
        }
      });
      
      popupMenu.show(_subtypeAdvancedSettingsTable,
                     (int) rect.getX(),
                     (int) (rect.getY() + rect.getHeight()));
    }
    else if (col == _subtypeAdvancedSettingsTable._CAMERA_ANGLE_ENABLED_INDEX && _enableCameraSelection)
    {
      Rectangle rect = _subtypeAdvancedSettingsTable.getCellRect(row, col, true);

      final Subtype subtype = _subtypeAdvancedSettingsTableModel.getSubtypeAt(row);
      java.util.List<Integer> cameraEnumList = new ArrayList<Integer>();;
      java.util.List<Integer> listOfSelectedCameras = new ArrayList<Integer>();
      
      for (com.axi.v810.hardware.XrayCameraIdEnum cameraIDEnum : com.axi.v810.hardware.XrayCameraIdEnum.getAllEnums())
        cameraEnumList.add(cameraIDEnum.getId());
      
      for (int cameraId : subtype.getSubtypeAdvanceSettings().getCameraEnabledIDList())
        listOfSelectedCameras.add(cameraEnumList.indexOf(cameraId));
      
      final ScrollableCheckBoxListPopupMenu<Integer> popupMenu = new ScrollableCheckBoxListPopupMenu(cameraEnumList,
                                                                                                     listOfSelectedCameras, 
                                                                                                     true,
                                                                                                     false,
                                                                                                     _DEFAULT_MINIMUM_SELECTED_CAMERA_ANGLE_OPTION_COUNT,
                                                                                                     cameraEnumList.size());
      popupMenu.setVisibleRowCount(10);
      popupMenu.setPreferredSize(new Dimension((int) rect.getWidth(), popupMenu.getPreferredSize().height));
      popupMenu.addPopupMenuListener(new PopupMenuListener()
      {
        /**
         * @author Yong Sheng Chuan
         */
        public void popupMenuWillBecomeVisible(PopupMenuEvent e)
        {
          //do nothing
        }

        /**
         * @author Yong Sheng Chuan
         */
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e)
        {
          _subtypeAdvancedSettingsTable.setValueAt(new ArrayList<Integer>(popupMenu.getSelectedValuesList()), row, col);
        }

        /**
         * @author Yong Sheng Chuan
         */
        public void popupMenuCanceled(PopupMenuEvent e)
        {
          //do nothing
        }
      });
      
      popupMenu.show(_subtypeAdvancedSettingsTable,
                     (int) rect.getX(),
                     (int) (rect.getY() + rect.getHeight()));
    }
  }
  
  /**
   * @author Chin Seong
   */
  private void setupMenuItem()
  {
      JPanel menuPanel = new JPanel(new BorderLayout());
      menuPanel.setBorder(BorderFactory.createLineBorder(Color.black));
      JMenuBar menuBar = new JMenuBar();
      JMenu menu = new JMenu(StringLocalizer.keyToString("GUI_EDIT_MENU_KEY"));

     _undoMenuItem.setText(StringLocalizer.keyToString("GUI_UNDO_MENU_KEY"));
     _undoMenuItem.setAccelerator(_undoKeyStroke);
     _undoMenuItem.addActionListener(new java.awt.event.ActionListener()
     {
       public void actionPerformed(ActionEvent e)
       {
         undoMenuItem_actionPerformed(e);
       }
     });

     _redoMenuItem.setText(StringLocalizer.keyToString("GUI_REDO_MENU_KEY"));
     _redoMenuItem.setAccelerator(_redoKeyStroke);
     _redoMenuItem.addActionListener(new java.awt.event.ActionListener()
     {
       public void actionPerformed(ActionEvent e)
       {
         redoMenuItem_actionPerformed(e);
       }
     });

     menuBar.add(menu);
     menu.add(_undoMenuItem);
     menu.add(_redoMenuItem);
     menuPanel.add(menuBar, BorderLayout.WEST);
     _mainPanel.add(menuPanel, BorderLayout.NORTH);
  }

 /**
   * @author Chin Seong
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    unpopulate();
  }

 /**
   * @author Chin Seong
   */
  public void undoMenuItem_actionPerformed(ActionEvent e)
  {
    _parentComponent.getInstance().getMainMenuBar().getUndoKeystrokeHandler().undo(_commandManager);
  }

  /**
   * @author Chin Seong
   */
  public void redoMenuItem_actionPerformed(ActionEvent e)
  {
    _parentComponent.getInstance().getMainMenuBar().getUndoKeystrokeHandler().redo(_commandManager);
  }

  public void unpopulate()
  {
    //_currentBoardType = null;
    _subtypeAdvancedSettingsTableModel.clear();

    ProjectObservable.getInstance().deleteObserver(this);
    dispose();
  }

  /**
   * @author Chin Seong
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProjectObservable)
        {
          ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent) object;
          ProjectChangeEventEnum eventEnum = projectChangeEvent.getProjectChangeEventEnum();

          if (eventEnum instanceof SubtypeAdvanceSettingsEventEnum)
          {
            _subtypeAdvancedSettingsTableModel.populateSubtypesWithPanelData(_subtypes);
            _subtypeAdvancedSettingsTableModel.fireTableDataChanged();

            if (_selectedSubtypes.size() > 0)
            {
              //now reselect the previouse selected row
              for (Subtype subtype : _selectedSubtypes)
              {
                int rowPosition = _subtypeAdvancedSettingsTableModel.getRowForSubtype(subtype);
                if (rowPosition != -1) // -1 measn it does not exist in the table.  This can happen with more than one board type
                {
                  _subtypeAdvancedSettingsTable.addRowSelectionInterval(rowPosition, rowPosition);
                }
              }
            }
          }
        }
      }
    });
  }
}
