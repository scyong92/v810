package com.axi.v810.gui.testDev;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * Table for adding and array of a particular board type
 * @author Carli Connally
 */
class BoardArrayTable extends JTable
{
  //index numbers for the columns (indexes are hard coded, so moving the columns does not work)
  public static final int BOARD_TYPE_COLUMN_INDEX = 0;
  public static final int ROWS_COLUMN_INDEX = 1;
  public static final int COLUMNS_COLUMN_INDEX = 2;
  public static final int ROTATION_COLUMN_INDEX = 3;
  public static final int FLIPPED_COLUMN_INDEX = 4;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _BOARD_TYPE_COLUMN_WIDTH_PERCENTAGE = .40;
  private static final double _ROWS_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double _COLUMNS_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double _ROTATION_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double _FLIPPED_COLUMN_WIDTH_PERCENTAGE = .15;

  private List<BoardType> _boardTypes = null;

  private NumericRenderer _numericRenderer = new NumericRenderer(0, JLabel.CENTER);
//  private CheckCellRenderer _checkRenderer = new CheckCellRenderer();
  private ListSelectionCellEditor _listEditor = new ListSelectionCellEditor();
  private NumericEditor _centerNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 0, 0.0, Double.MAX_VALUE);
  private BoardAndPanelRotationCellEditor _rotationEditor = new BoardAndPanelRotationCellEditor();
  private CheckCellEditor _checkEditor = new CheckCellEditor(getBackground(), getForeground());

  /**
   * @param boardTypes A list of board types available
   * @author Carli Connally
   */
  public BoardArrayTable(List<BoardType> boardTypes)
  {
    Assert.expect(boardTypes != null);

    _boardTypes = boardTypes;
    setRowSelectionAllowed(false);
    setColumnSelectionAllowed(false);
  }

  /**
   * Default constructor.
   * @author Carli Connally
   */
  public BoardArrayTable()
  {
    setRowSelectionAllowed(false);
    setColumnSelectionAllowed(false);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void clear()
  {
    _boardTypes.clear();
    _listEditor.setData(new java.util.ArrayList<Object>());
  }

  /**
   * @param boardTypes List of available board types on the panel
   * @author Carli Connally
   */
  public void setBoardTypes(List<BoardType> boardTypes)
  {
    Assert.expect(boardTypes != null);

    _boardTypes = boardTypes;
    _listEditor.setData(new java.util.ArrayList<Object>(_boardTypes));
  }

  /**
   * @return List of board types available
   * @author Carli Connally
   */
  public List<BoardType> getBoardTypes()
  {
    Assert.expect(_boardTypes != null);

    return _boardTypes;
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Carli Connally
   */
  public void setPreferredColumnWidths()
  {
    final TableColumnModel columnModel = this.getColumnModel();

    final int totalColumnWidth = columnModel.getTotalColumnWidth();

    int boardTypeColWidth = (int)(totalColumnWidth * _BOARD_TYPE_COLUMN_WIDTH_PERCENTAGE);
    int rowsColWidth = (int)(totalColumnWidth * _ROWS_COLUMN_WIDTH_PERCENTAGE);
    int columnsColWidth = (int)(totalColumnWidth * _COLUMNS_COLUMN_WIDTH_PERCENTAGE);
    int rotationColWidth = (int)(totalColumnWidth * _ROTATION_COLUMN_WIDTH_PERCENTAGE);
    int flippedColWidth = (int)(totalColumnWidth * _FLIPPED_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(BOARD_TYPE_COLUMN_INDEX).setPreferredWidth(boardTypeColWidth);
    columnModel.getColumn(ROWS_COLUMN_INDEX).setPreferredWidth(rowsColWidth);
    columnModel.getColumn(COLUMNS_COLUMN_INDEX).setPreferredWidth(columnsColWidth);
    columnModel.getColumn(ROTATION_COLUMN_INDEX).setPreferredWidth(rotationColWidth);
    columnModel.getColumn(FLIPPED_COLUMN_INDEX).setPreferredWidth(flippedColWidth);

  }

  /**
   * @author Andy Mechtenberg
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    columnModel.getColumn(BOARD_TYPE_COLUMN_INDEX).setCellEditor(_listEditor);

    columnModel.getColumn(ROWS_COLUMN_INDEX).setCellEditor(_centerNumericEditor);
    columnModel.getColumn(ROWS_COLUMN_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(COLUMNS_COLUMN_INDEX).setCellEditor(_centerNumericEditor);
    columnModel.getColumn(COLUMNS_COLUMN_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(ROTATION_COLUMN_INDEX).setCellEditor(_rotationEditor);
    columnModel.getColumn(ROTATION_COLUMN_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(FLIPPED_COLUMN_INDEX).setCellEditor(_checkEditor);
//    columnModel.getColumn(FLIPPED_COLUMN_INDEX).setCellRenderer(_checkRenderer);

  }
}
