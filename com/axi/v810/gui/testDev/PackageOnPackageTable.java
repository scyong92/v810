package com.axi.v810.gui.testDev;

import java.util.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;

/**
 * @author Khaw Chek Hau
 * POP Development
 */
class PackageOnPackageTable extends JSortTable
{
  public static final int _PACKAGE_NAME_INDEX = 0;
  private static final double _PACKAGE_NAME_COLUMN_WIDTH_PERCENTAGE = 1;

  private PackageOnPackageTableModel _tableModel;
  private boolean _modelSet = false;
    
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public PackageOnPackageTable(PackageOnPackageTableModel model)
  {
    Assert.expect(model != null);
    super.setModel(model);
    
    _tableModel = model;
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();
    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_PACKAGE_NAME_INDEX).setPreferredWidth((int)(totalColumnWidth * _PACKAGE_NAME_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };

    columnModel.getColumn(_PACKAGE_NAME_INDEX).setCellEditor(stringEditor);
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void clearSelection()
  {
    TableModel model = getModel();
    // this function gets called before the table model is set, during intialization. At that time,
    // the model will be a defaultTableModel so the assert is sure to fail
    if (_modelSet)
    {
      Assert.expect(model instanceof PackageOnPackageTableModel);
      ((PackageOnPackageTableModel)model).clearSelectedPackage();
    }
    super.clearSelection();
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  void cancelCellEditing()
  {
    TableCellEditor cellEditor = getColumnModel().getColumn(_PACKAGE_NAME_INDEX).getCellEditor();
    if (cellEditor != null)
    {
      cellEditor.cancelCellEditing();
    }
  }
}