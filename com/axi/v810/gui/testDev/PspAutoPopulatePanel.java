package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.text.*;
import java.util.*;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.psp.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PspAutoPopulatePanel extends JPanel implements Observer
{
  // Copied from OpticalRegionGeneration
  private static final int OPTICAL_CAMERA_VIEW_FACTOR_1       = 16;
  private static final int OPTICAL_CAMERA_VIEW_FACTOR_2       = 14;
  private static final int WAITING_FOR_HEIGHT_MAP_MAXIMUM_DURATION_IN_MILLISECONDS = 15000; // 15 seconds
  private static int OPTICAL_CAMERA_MAX_WIDTH_IN_PIXELS       = 0;
  private static int OPTICAL_CAMERA_MAX_HEIGHT_IN_PIXELS      = 0;
  
  private static double PIXEL_WIDTH_TO_PANEL_WIDTH_RATIO = 0;
  private static double PIXEL_HEIGHT_TO_PANEL_HEIGHT_RATIO = 0;
  
  private MainMenuGui _mainUI = MainMenuGui.getInstance();
  private TestDev _testDev = null;  
  private Project _project = null;
  private MathUtilEnum _enumSelectedUnits = null;
  private BoardType _currentBoardType;
  private Board _currentBoard;
  private boolean _shouldInteractWithGraphics = false;  
  private boolean _active = false; // true if this panel is visible
  private static PspAutoPopulatePanel _instance;
  private FocusAdapter _textFieldFocusAdapter;
  private ActionListener _textFieldActionListener;
  private ActionListener _unitsComboBoxActionListener;  
  private ListSelectionListener _pspRectangleTableListSelectionListener;
  private ListSelectionListener _selectedComponentTableListSelectionListener;
  
  private JPanel _autoPopulateSetupPanel = new JPanel();
  private JPanel _autoPopulateOptionsPanel = new JPanel();
  private JPanel _componentOptionsPanel = new JPanel();
  private JPanel _componentListingPanel = new JPanel();
  private JPanel _editPanel = new JPanel();
  private JPanel _populatePanel = new JPanel();
  private JPanel _populateRectangleOptionsPanel = new JPanel();
  private JScrollPane _componentListingLayoutScroll = new JScrollPane();
  private BorderLayout _autoPopulateSetupBorderLayout = new BorderLayout();
  private BorderLayout _mainBorderLayout = new BorderLayout();
  private GridLayout _autoPopulateOptionsPanelGridLayout = new GridLayout();
  private GridLayout _componentOptionsGridLayout = new GridLayout();
  private BorderLayout _componentListingFlowLayout = new BorderLayout();
  private FlowLayout _populateFlowLayout = new FlowLayout();
  private GridLayout _populateRectangleOptionsGridLayout = new GridLayout();
  private JPanel _meshTrianglePanel = new JPanel();
  private GridLayout _meshTrianglePanelGridLayout = new GridLayout();
  
  private Border _lightGrayLineBorder;
  private Border _etchedLeftBorder;
  private JLabel _workingUnitsLabel = new JLabel();
  private JLabel _opticalRectangleSpacingLabel = new JLabel();
  private JComboBox _unitsComboBox = new JComboBox();
  private JRadioButton _autoIncludeWholePanelComponentRadioButton = new JRadioButton();
  private JRadioButton _includeFollowingComponentOnlyRadioButton = new JRadioButton();
  
  private JSplitPane _centerSplitPane = new JSplitPane();
  private JPanel _rectangleListingPanel = new JPanel();
  private JScrollPane _rectangleListingLayoutScroll = new JScrollPane();
  private BorderLayout _rectangleListingFlowLayout = new BorderLayout();
  private PspRectangleTable _pspRectangleTable = new PspRectangleTable();
  private PspRectangleTableModel _pspRectangleTableModel;
  private PspSelectedComponentTable _pspSelectedComponentTable = new PspSelectedComponentTable();
  private PspSelectedComponentTableModel _pspSelectedComponentTableModel;
  private JSplitPane _opticalRegionInformationSplitPane = new JSplitPane();
  
  private JButton _populateButton = new JButton();
  private JButton _populateAroundSelectedComponentButton = new JButton();
  private JButton _runAutoLiveViewButton = new JButton();
  private NumericTextField _opticalRectangleSpacingTextField = new NumericTextField();
  private NumericRangePlainDocument _opticalRectangleSpacingDocument = new NumericRangePlainDocument();
  private JCheckBox _removeRectangleOnTopOfComponentCheckbox = new JCheckBox();
  private JCheckBox _extendRectangleToBoardEdgeCheckbox = new JCheckBox();
  
  private JPanel _selectBoardsPanel = new JPanel();
  private JLabel _selectBoardsLabel = new JLabel();
  private JButton _selectBoardsDialogButton =  new JButton();
  private JLabel _currentSelectedBoardsLabel = new JLabel();
  private JButton _copyRectanglesToBoardButton = new JButton();
  
  private JCheckBox _useMeshTriangleCheckbox = new JCheckBox();
  private JCheckBox _showMeshTriangleCheckbox = new JCheckBox();
  private JCheckBox _useLargerFovCheckbox = new JCheckBox();
  
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private SelectedRendererObservable _selectedRendererObservable;
  
  private DragOpticalRegionObservable _dragOpticalRegionRendererObservable;
  private UnselectedOpticalRegionObservable _unselectedOpticalRegionRendererObservable;
  private UnselectedDragOpticalRegionObservable _unselectedDragOpticalRegionRendererObservable;
  private MouseClickObservable _panelGraphicsMouseClickObservable;

  private SurfaceMapLiveVideoDialog _pspLiveVideoDialog;
  private static final int _CHARS_PER_LINE = 50;
  private SurfaceMapping _surfaceMapping = SurfaceMapping.getInstance();

  private OpticalRegion _opticalRegion;
  private TestExecution _testExecution = TestExecution.getInstance();
  private static boolean _hasPopulated = false;
  private AffineTransform _transformFromRendererCoords = new AffineTransform();    

  private static boolean _isAddRectangleModeOn = false;
  private static boolean _isPanelBasedAlignment = false;
  private static java.util.List<Board> _boardsToApplyOpticalCameraRectangles = new ArrayList<>();
  
  // *** Handle runtime alignment before running surface map ***
  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  private static boolean _isAlignmentSuccess = false;
  
  // This flag is used to check if a z-height map is returned from the native side when running Surface Map.
  private static boolean _isSurfaceMappingSuccess = false;
  private static boolean _isHeightMapAvailable = false;
  private static boolean _isLiveViewMode = false;
  private boolean _isPreviousButtonVisible = false;
  // This map is used to keep the list of Surface Map allowable regions
  private static Map<Rectangle, PspModuleEnum> _allowableRegionToPspModuleEnumMap = new LinkedHashMap<>();
  
  private static java.util.List<com.axi.v810.business.panelDesc.Component> _temporaryComponents = new ArrayList<>();
  private static boolean _isAutoIncludedAllComponents = false;
  private static transient ProjectObservable _projectObservable;
  private static boolean _isGoIntoPspAutoPopulateBefore = false;
  
  private static PspSettingEnum _settingEnum = PspSettingEnum.getEnum(Config.getInstance().getIntValue(HardwareConfigEnum.PSP_SETTING));
  
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Cheah Lee Herng
   */
  public PspAutoPopulatePanel()
  {
    jbInit();
    init();
  }
 
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized PspAutoPopulatePanel getInstance()
  {
    if (_instance == null)
    {
      _instance = new PspAutoPopulatePanel();
    }

    return _instance;
  }
 
  /**
   * @author Cheah Lee Herng
   */
  private void jbInit()
  {
    _textFieldFocusAdapter = new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        JTextField sourceTextField = (JTextField) e.getSource();
        sourceTextField.selectAll();
      }

      public void focusLost(FocusEvent e)
      {
        NumericTextField sourceTextField = (NumericTextField) e.getSource();
        handleTextFieldData(sourceTextField);
      }
    };

    _textFieldActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        NumericTextField sourceTextField = (NumericTextField) e.getSource();
        handleTextFieldData(sourceTextField);
      }
    };

    _unitsComboBoxActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        unitsComboBox_actionPerformed();
      }
    };
    initStaticComboChoices();

    _opticalRectangleSpacingDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _opticalRectangleSpacingTextField.setDocument(_opticalRectangleSpacingDocument);

    _opticalRectangleSpacingTextField.addActionListener(_textFieldActionListener);
    _opticalRectangleSpacingTextField.addFocusListener(_textFieldFocusAdapter);

    _lightGrayLineBorder = BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.lightGray, 1), BorderFactory.createEmptyBorder(0, 2, 0, 0));
    _etchedLeftBorder = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), BorderFactory.createEmptyBorder(0, 2, 0, 0));
    setLayout(_mainBorderLayout);

    _autoPopulateSetupPanel.setLayout(_autoPopulateSetupBorderLayout);
    _autoPopulateSetupPanel.setOpaque(false);
    _autoPopulateOptionsPanel.setLayout(_autoPopulateOptionsPanelGridLayout);
    _autoPopulateOptionsPanelGridLayout.setColumns(2);
    _autoPopulateOptionsPanelGridLayout.setHgap(-1);
    _autoPopulateOptionsPanelGridLayout.setRows(2);
    _autoPopulateOptionsPanelGridLayout.setVgap(-1);
    _autoPopulateOptionsPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white, new Color(165, 163, 151)),
            StringLocalizer.keyToString("PSP_AUTOPOPULATE_OPTIONS_KEY")));
    _componentOptionsPanel.setLayout(_componentOptionsGridLayout);
    _componentOptionsGridLayout.setColumns(1);
    _componentOptionsGridLayout.setHgap(-1);
    _componentOptionsGridLayout.setRows(2);
    _componentOptionsGridLayout.setVgap(-1);
    _componentOptionsPanel.setOpaque(false);
    _componentOptionsPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white, new Color(165, 163, 151)),
                                     StringLocalizer.keyToString("PSP_COMPONENT_OPTIONS_KEY")));
    _componentListingPanel.setLayout(_componentListingFlowLayout);
    _componentListingPanel.setOpaque(false);
    _componentListingPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white, new Color(165, 163, 151)),
                                     StringLocalizer.keyToString("PSP_COMPONENT_TABLE_LISTING_KEY")));
    _componentListingLayoutScroll.setBorder(null);
    _componentListingLayoutScroll.setOpaque(false);
    _componentListingPanel.add(_componentOptionsPanel, BorderLayout.NORTH);
    _componentListingPanel.add(_componentListingLayoutScroll, BorderLayout.CENTER);
    _componentListingLayoutScroll.getViewport().add(_pspSelectedComponentTable, null);
    _componentListingLayoutScroll.getViewport().setOpaque(false);

    _rectangleListingPanel.setLayout(_rectangleListingFlowLayout);
    _rectangleListingPanel.setOpaque(false);
    _rectangleListingPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white, new Color(165, 163, 151)),
                                     StringLocalizer.keyToString("PSP_OPTICAL_RECTANGLE_KEY")));
    _rectangleListingLayoutScroll.setBorder(null);
    _rectangleListingLayoutScroll.setOpaque(false);
    _rectangleListingPanel.add(_rectangleListingLayoutScroll, BorderLayout.NORTH);
   
    _rectangleListingLayoutScroll.getViewport().add(_pspRectangleTable, null);
    _rectangleListingLayoutScroll.getViewport().setOpaque(false);

    _populateFlowLayout.setAlignment(FlowLayout.LEFT);
    _populateFlowLayout.setHgap(5);
    _populatePanel.setLayout(_populateFlowLayout);
    _editPanel.setLayout(_populateFlowLayout);

    _removeRectangleOnTopOfComponentCheckbox.setText(StringLocalizer.keyToString("PSP_REMOVE_RECTANGLE_TOP_COMPONENT_KEY"));
    _removeRectangleOnTopOfComponentCheckbox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {      
        _project.getPanel().setIsRemoveComponentOnTopOfComponents(_removeRectangleOnTopOfComponentCheckbox.isSelected());
      }
    });
    _extendRectangleToBoardEdgeCheckbox.setText(StringLocalizer.keyToString("PSP_EXTEND_RECTANGLE_EDGE_BOARD_KEY"));
    _extendRectangleToBoardEdgeCheckbox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {      
        _project.getPanel().setIsExtendRectanglesToEdgeOfBoard(_extendRectangleToBoardEdgeCheckbox.isSelected());
      }
    });
    _useLargerFovCheckbox.setText(StringLocalizer.keyToString("PSP_USE_LARGER_FOV_KEY"));
    _useLargerFovCheckbox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {      
        useLargerFOVCheckbox_actionPerformed();
      }
    });
    
    _useMeshTriangleCheckbox.setText(StringLocalizer.keyToString("PSP_USE_MESH_TRIANGLE_KEY"));
    _useMeshTriangleCheckbox.setEnabled(true);
    _useMeshTriangleCheckbox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_useMeshTriangleCheckbox.isSelected())
        {
          _showMeshTriangleCheckbox.setEnabled(true);
          _project.getPanel().setIsUseMeshTriangle(true);
        }
        else
        {
          _showMeshTriangleCheckbox.setSelected(false);
          _showMeshTriangleCheckbox.setEnabled(false);
          _project.getPanel().setIsUseMeshTriangle(false);
          _project.getPanel().setIsShowMeshTriangle(false);
        }
        updateMeshTriangleOnCAD();
        //update optical rectangle used by each reconstruction region
        allocateOpticalCameraRectangleToComponents();
      }
    });
    
    _showMeshTriangleCheckbox.setText(StringLocalizer.keyToString("PSP_SHOW_MESH_TRIANGLE_KEY"));
    _showMeshTriangleCheckbox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_showMeshTriangleCheckbox.isSelected())
          _project.getPanel().setIsShowMeshTriangle(true);
        else
          _project.getPanel().setIsShowMeshTriangle(false);
        updateMeshTriangleOnCAD();
      }
    });
    
    _selectBoardsLabel.setText(StringLocalizer.keyToString("PSP_SELECT_BOARDS_TO_APPLY_OPTICAL_CAMERA_RECTANGLES_KEY"));
    _selectBoardsDialogButton.setText(StringLocalizer.keyToString("PSP_SELECT_BOARDS_KEY"));
    _selectBoardsDialogButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectBoardsDialogButton_actionPerformed();
      }
    });
    _copyRectanglesToBoardButton.setText(StringLocalizer.keyToString("PSP_COPY_RECTANGLES_BUTTON_KEY"));
    _copyRectanglesToBoardButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        copyRectanglesToBoardButton_actionPerformed();
      }
    });
    _currentSelectedBoardsLabel.setText(StringLocalizer.keyToString("PSP_CURRENT_SELECTED_BOARD_KEY") + getCurrentSelectedBoardsString());
    _selectBoardsPanel.setLayout(new BorderLayout());
    _selectBoardsPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white, new Color(165, 163, 151)),
                                                                   StringLocalizer.keyToString("PSP_SELECT_BOARDS_OPTIONS_KEY")));
    _selectBoardsLabel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    _selectBoardsDialogButton.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    _currentSelectedBoardsLabel.setBorder(BorderFactory.createEmptyBorder(0,10,10,10));
    _selectBoardsPanel.add(_selectBoardsLabel, BorderLayout.WEST);
    JPanel boardBasedButtonPanel = new JPanel();
    boardBasedButtonPanel.setLayout(new BorderLayout());
    boardBasedButtonPanel.add(_selectBoardsDialogButton, BorderLayout.WEST);
    boardBasedButtonPanel.add(_copyRectanglesToBoardButton, BorderLayout.EAST);
    _selectBoardsPanel.add(boardBasedButtonPanel, BorderLayout.EAST);
    _selectBoardsPanel.add(_currentSelectedBoardsLabel, BorderLayout.SOUTH);
    
    _populateRectangleOptionsPanel.setLayout(_populateRectangleOptionsGridLayout);
    _populateRectangleOptionsGridLayout.setColumns(1);
    _populateRectangleOptionsGridLayout.setHgap(-1);
    _populateRectangleOptionsGridLayout.setRows(2);
    _populateRectangleOptionsGridLayout.setVgap(-1);
    _populateRectangleOptionsPanel.setOpaque(false);
    _populateRectangleOptionsPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white, new Color(165, 163, 151)),
                                             StringLocalizer.keyToString("PSP_POPULATE_RECTANGLE_OPTIONS_KEY")));
    _autoPopulateSetupPanel.add(_populateRectangleOptionsPanel);

    _workingUnitsLabel.setBorder(_etchedLeftBorder);
    _workingUnitsLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_UNITS_KEY"));
    _opticalRectangleSpacingLabel.setBorder(_etchedLeftBorder);
    _opticalRectangleSpacingLabel.setText(StringLocalizer.keyToString("PSP_OPTICAL_RECTANGLE_SPACING_KEY"));
    _opticalRectangleSpacingTextField.setBorder(_lightGrayLineBorder);
    _opticalRectangleSpacingTextField.setValue(0);

    add(_autoPopulateSetupPanel, BorderLayout.NORTH);
    _autoPopulateOptionsPanel.add(_workingUnitsLabel, null);
    _autoPopulateOptionsPanel.add(_unitsComboBox, null);
    _autoPopulateOptionsPanel.add(_opticalRectangleSpacingLabel, null);
    _autoPopulateOptionsPanel.add(_opticalRectangleSpacingTextField, null);

    _populateRectangleOptionsPanel.add(_removeRectangleOnTopOfComponentCheckbox, null);
    _populateRectangleOptionsPanel.add(_extendRectangleToBoardEdgeCheckbox, null);
    //Ying-Huan.Chu - temporary commented Larger FOV feature for 5.7 release.
    //_populateRectangleOptionsPanel.add(_useLargerFovCheckbox, null);
    
    _meshTrianglePanelGridLayout.setColumns(2);
    _meshTrianglePanelGridLayout.setHgap(-1);
    _meshTrianglePanelGridLayout.setRows(2);
    _meshTrianglePanelGridLayout.setVgap(-1);
    _meshTrianglePanel.setLayout(_meshTrianglePanelGridLayout);
    _meshTrianglePanel.add(_useMeshTriangleCheckbox, null);
    _meshTrianglePanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white, new Color(165, 163, 151)),
                                 StringLocalizer.keyToString("PSP_MESH_TRIANGLE_OPTIONS_KEY")));
    _showMeshTriangleCheckbox.setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 0));
    _meshTrianglePanel.add(_showMeshTriangleCheckbox, null);
    
    JPanel populateRectangleOptionsAndMeshTriangleOptionsPanel = new JPanel();
    populateRectangleOptionsAndMeshTriangleOptionsPanel.setLayout(new BorderLayout());
    populateRectangleOptionsAndMeshTriangleOptionsPanel.add(_populateRectangleOptionsPanel, BorderLayout.NORTH);
    populateRectangleOptionsAndMeshTriangleOptionsPanel.add(_meshTrianglePanel, BorderLayout.CENTER);

    JPanel autoPopulatePanel = new JPanel();
    autoPopulatePanel.setLayout(new BorderLayout());
    autoPopulatePanel.add(_autoPopulateOptionsPanel, BorderLayout.NORTH);
    autoPopulatePanel.add(populateRectangleOptionsAndMeshTriangleOptionsPanel, BorderLayout.CENTER);
    autoPopulatePanel.add(_selectBoardsPanel, BorderLayout.SOUTH);

    _autoPopulateSetupPanel.add(autoPopulatePanel, BorderLayout.CENTER);
    _autoIncludeWholePanelComponentRadioButton.setSelected(false);
    _isAutoIncludedAllComponents = false;
    _autoIncludeWholePanelComponentRadioButton.setText(StringLocalizer.keyToString("PSP_INCLUDE_ALL_PANEL_COMPONENTS_KEY"));
    _autoIncludeWholePanelComponentRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        autoIncludeWholePanelComponentRadioButton_actionPerformed();
      }
    });
    _includeFollowingComponentOnlyRadioButton.setSelected(false);
    _includeFollowingComponentOnlyRadioButton.setText(StringLocalizer.keyToString("PSP_INCLUDE_FOLLOWING_COMPONENTS_ONLY_KEY"));
    _includeFollowingComponentOnlyRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {      
        includeFollowingComponentOnlyRadioButton_actionPerformed();
      }
    });
    _componentOptionsPanel.add(_autoIncludeWholePanelComponentRadioButton, null);
    _componentOptionsPanel.add(_includeFollowingComponentOnlyRadioButton, null);

    _populateButton.setText(StringLocalizer.keyToString("PSP_POPULATE_KEY"));
    _populateButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_isPanelBasedAlignment)
          populateButton_actionPerformed(false);
        else
          populateButtonForBoardBased_actionPerformed(false);
      }
    });
    
    _populateAroundSelectedComponentButton.setText(StringLocalizer.keyToString("PSP_POPULATE_AROUND_SELECTED_COMPONENT_KEY"));
    _populateAroundSelectedComponentButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_isPanelBasedAlignment)
          populateButton_actionPerformed(true);
        else
          populateButtonForBoardBased_actionPerformed(true);
      }
    });

    _runAutoLiveViewButton.setText(StringLocalizer.keyToString("PSP_RUN_AUTO_LIVE_VIEW_BUTTON_KEY"));
    _runAutoLiveViewButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _commandManager.trackState(getCurrentUndoState());
        _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PSP_ADD_OPTICAL_REGION_KEY"));
        if (_isPanelBasedAlignment)
          runAutoLiveViewButton_actionPerformed();
        else
          runAutoLiveViewButtonForBoardBased_actionPerformed();
        _commandManager.endCommandBlockIfNecessary();
      }
    });
    
    // XCR-3492 unable to connect to RMS
    if (XrayTester.isHardwareAvailable() == false)
      _runAutoLiveViewButton.setEnabled(false);
    else
      _runAutoLiveViewButton.setEnabled(true);

    _editPanel.add(_populateButton, null);
    //_editPanel.add(_populateAroundSelectedComponentButton, null);
    _editPanel.add(_runAutoLiveViewButton, null);
    JPanel buttonsPanel = new JPanel(new BorderLayout());
    buttonsPanel.add(_editPanel, BorderLayout.NORTH);
    
    JPanel rectangleAndComponentTablePanel = new JPanel();
    rectangleAndComponentTablePanel.setLayout(new BorderLayout());
    
    _centerSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
    _centerSplitPane.add(_rectangleListingPanel, JSplitPane.TOP);
    _centerSplitPane.add(_componentListingPanel, JSplitPane.BOTTOM);
    add(_centerSplitPane, BorderLayout.CENTER);
    add(buttonsPanel, BorderLayout.SOUTH);

    _pspSelectedComponentTable.setName(".pspComponentTable");
    _pspRectangleTable.setName(".pspRectangleTable");
  }
    
  /**
   * @author Cheah Lee Herng
   * @edited by Kee Chin Seong - Updating Surface Map Data
   */
  private void init()
  {
    //set up table row height based on the size of the font
    FontMetrics fm = getFontMetrics(getFont());
    int prefHeight = fm.getHeight();// + 3;
    _pspSelectedComponentTableModel = new PspSelectedComponentTableModel(_pspSelectedComponentTable);
    _pspSelectedComponentTable.setModel(_pspSelectedComponentTableModel);
    _pspSelectedComponentTable.setEditorsAndRenderers();
    _pspSelectedComponentTable.setRowHeight(prefHeight);
    _pspSelectedComponentTable.setPreferredColumnWidths();
    _pspSelectedComponentTable.setEnabled(true);
    _pspSelectedComponentTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _pspSelectedComponentTable.setVisible(true);

    _selectedComponentTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        selectedComponentTableListSelectionChanged(e);
      }
    };
    
    _pspSelectedComponentTable.addMouseListener(new MouseAdapter() 
    {
      public void mouseReleased(MouseEvent e) 
      {
        if (e.isPopupTrigger())
        {
          // get the table cell that the mouse was right-clicked in
          Point p = e.getPoint();
          int row = _pspSelectedComponentTable.rowAtPoint(p);
          int col = _pspSelectedComponentTable.columnAtPoint(p);

          // if this cell is not currently selected, exit.  It's confusing to edit a cell that not selected.
          // we could "select" this cell too, but for now, we'll just not.
          if (_pspSelectedComponentTable.isCellSelected(row, col) == false)
            return;

          // start the editing process for this cell
          //_pspRectangleTable.editCellAt(row, col);
          // if this is a combo box cell editor, the editCellAt doesn't show the combo box popup, so let's show that
          // only if this is a combo box editor

          JPopupMenu popup = new JPopupMenu();
          popup = createDeleteComponentPopUpMenu();
          popup.show(_pspSelectedComponentTable, e.getX(), e.getY());
        }
      }
    });

    ListSelectionModel rowSM3 = _pspSelectedComponentTable.getSelectionModel();
    rowSM3.addListSelectionListener(_selectedComponentTableListSelectionListener);  // add the listener for a table row selection

    //set up table row height based on the size of the font
    FontMetrics pspRectangleFontMetrics = getFontMetrics(getFont());
    int pspRectanglePreferredHeight = pspRectangleFontMetrics.getHeight();// + 3;
    _pspRectangleTableModel = new PspRectangleTableModel();
    _pspRectangleTable.setModel(_pspRectangleTableModel);
    _pspRectangleTable.setRowHeight(pspRectanglePreferredHeight);
    _pspRectangleTable.setPreferredColumnWidths();
    _pspRectangleTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _pspRectangleTable.setVisible(true);

    _pspRectangleTable.addMouseListener(new MouseAdapter() 
    {
      public void mouseReleased(MouseEvent e) 
      {
        if (e.isPopupTrigger())
        {
          // get the table cell that the mouse was right-clicked in
          Point p = e.getPoint();
          int row = _pspRectangleTable.rowAtPoint(p);
          int col = _pspRectangleTable.columnAtPoint(p);

          // if this cell is not currently selected, exit.  It's confusing to edit a cell that not selected.
          // we could "select" this cell too, but for now, we'll just not.
          if (_pspRectangleTable.isCellSelected(row, col) == false)
            return;

          // start the editing process for this cell
          //_pspRectangleTable.editCellAt(row, col);
          // if this is a combo box cell editor, the editCellAt doesn't show the combo box popup, so let's show that
          // only if this is a combo box editor

          JPopupMenu popup = new JPopupMenu();
          if (_pspRectangleTable.getSelectedRows().length > 1)
          {
            popup = createPopUpMenuForMultipleOpticalCameraRectangle();
            popup.show(_pspRectangleTable, e.getX(), e.getY());
          }
          else
          {
            popup = createPopUpMenuForSingleOpticalCameraRectangle();
            popup.show(_pspRectangleTable, e.getX(), e.getY());
          }
        }
      }
    });

    _pspRectangleTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        pspRectangleTableListSelectionChanged(e);
      }
    };
    ListSelectionModel pspRectangleSelectionModel = _pspRectangleTable.getSelectionModel();
    pspRectangleSelectionModel.addListSelectionListener(_pspRectangleTableListSelectionListener);  // add the listener for a table row selection

    // its okay to do set size here.. bill just doesn't like it on the whole panel.
    int headerHeight = prefHeight + 5;
    Dimension prefScrollSize = new Dimension(200, (prefHeight * 10) + headerHeight);  // 10 rows for tables
    _componentListingLayoutScroll.setPreferredSize(prefScrollSize);  
    _rectangleListingLayoutScroll.setPreferredSize(prefScrollSize);
  }
    
  /**
   * @author Cheah Lee Herng
   */
  public void start(ImageManagerPanel imageManagerPanel)
  {
    Assert.expect(imageManagerPanel != null);
    
    com.axi.util.TimerUtil screenTimer = new com.axi.util.TimerUtil();
    screenTimer.start();
    _isGoIntoPspAutoPopulateBefore = true;
    _project = Project.getCurrentlyLoadedProject();
    _testDev = _mainUI.getTestDev();
    
    _isPanelBasedAlignment = _project.getPanel().getPanelSettings().isPanelBasedAlignment();

    updateCadGraphics();
    
    if (_pspSelectedComponentTable != null)
    {
      _pspSelectedComponentTableModel.clear();
    }
    
    populateWithProjectData();
    
    final BusyCancelDialog busyDialog = new BusyCancelDialog(_mainUI,
                                                                 StringLocalizer.keyToString("PSP_UPDATE_DATA_KEY"),
                                                                 StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                                 true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, _mainUI);
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
       public void run()
       {
         try
         {
            _project.setupSurfaceMapIfNeeded();
         }
         finally
         {
           busyDialog.dispose();
         }
       }
    });
    busyDialog.setVisible(true);
    
    updateOpticalRectangleTableData();
    _active = true;
    
    if (_project.isTestProgramValid() == false)
    {
      _mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_TESTABILITY_NEEDS_TEST_PROGRAM_KEY"),
              StringLocalizer.keyToString("MM_GUI_TDT_TITLE_GROUPINGS_KEY"),
              _project);
    }
 
    if (_isPanelBasedAlignment)
    {
      if (_dragOpticalRegionRendererObservable == null)
      {
        _dragOpticalRegionRendererObservable = getPanelGraphicsEngine().getDragOpticalRegionObservable();
      }
      if (_unselectedDragOpticalRegionRendererObservable == null)
      {
        _unselectedDragOpticalRegionRendererObservable = getPanelGraphicsEngine().getUnselectedDragOpticalRegionRendererObservable();
      }
      if (_panelGraphicsMouseClickObservable == null)
      {
        _panelGraphicsMouseClickObservable = getPanelGraphicsEngine().getMouseClickObservable();
      }
      if (_unselectedOpticalRegionRendererObservable == null)
      {
        _unselectedOpticalRegionRendererObservable = getPanelGraphicsEngine().getUnselectedOpticalRegionRendererObservable();
      }
      _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().fitGraphicsToScreen();
      _selectBoardsDialogButton.setEnabled(false);
      _copyRectanglesToBoardButton.setEnabled(false);
      _currentSelectedBoardsLabel.setText("");
      _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().fitGraphicsToScreen();
    }
    else
    {
      if (_dragOpticalRegionRendererObservable == null)
      {
        _dragOpticalRegionRendererObservable = getBoardGraphicsEngine().getDragOpticalRegionObservable();
      }
      if (_unselectedDragOpticalRegionRendererObservable == null)
      {
        _unselectedDragOpticalRegionRendererObservable = getBoardGraphicsEngine().getUnselectedDragOpticalRegionRendererObservable();
      }
      if (_panelGraphicsMouseClickObservable == null)
      {
        _panelGraphicsMouseClickObservable = getBoardGraphicsEngine().getMouseClickObservable();
      }
      if (_unselectedOpticalRegionRendererObservable == null)
      {
        _unselectedOpticalRegionRendererObservable = getBoardGraphicsEngine().getUnselectedOpticalRegionRendererObservable();
      }
      _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().fitGraphicsToScreen();
      if (_pspRectangleTableModel.getRowCount() > 0)
      {
        java.util.List<String> boardNames = new ArrayList<>();
        for (OpticalCameraRectangle opticalCameraRectangle : _pspRectangleTableModel.getOpticalCameraRectangles())
        {
          if (opticalCameraRectangle.getInspectableBool() == true)
          {
            if (boardNames.contains(opticalCameraRectangle.getBoard().getName()) == false)
          	{
           	  boardNames.add(opticalCameraRectangle.getBoard().getName());
            }
          }
        }
        for (Board board : _currentBoardType.getBoards())
        {
          if (boardNames.contains(board.getName()))
          {
            addBoardToApplyOpticalCameraRectangle(board);
          }
        }
      }
      else
      {
        for (Board board : _project.getPanel().getBoards())
        {
          addBoardToApplyOpticalCameraRectangle(board);
        }
      }
      _selectBoardsDialogButton.setEnabled(true);
      _copyRectanglesToBoardButton.setEnabled(true);
      updateCurrentSelectedBoardsString();
      _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().fitGraphicsToScreen();
    }
    
    //update table list
    boolean isAutomaticallyIncludeAllComponents = _project.getPanel().isAutomaticallyIncludeAllComponents();
    if (isAutomaticallyIncludeAllComponents)
      autoIncludeWholePanelComponentRadioButton_actionPerformed();
    else
      includeFollowingComponentOnlyRadioButton_actionPerformed();
    
    updateMeshTriangleOnCAD();
    _dragOpticalRegionRendererObservable.addObserver(this);
    _unselectedDragOpticalRegionRendererObservable.addObserver(this);
    _panelGraphicsMouseClickObservable.addObserver(this);
    _unselectedOpticalRegionRendererObservable.addObserver(this);
    _commandManager.addObserver(this);
  
    setupDialogProperty(); // setup optical camera width and height.
    setupAllowableRegion();
    
    _pspRectangleTable.clearSelection();
    if (_pspRectangleTable.getRowCount() > 0)
      _pspRectangleTable.setSelectedRow(0);
    
    _testExecution.resetLastPerformedAlignmentTestSubProgramId();

    screenTimer.stop();
    
    if(_project.getTestProgram().getAllInspectableTestSubPrograms().isEmpty() == true)
    {    
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString("GUI_UNABLE_CREATE_ALIGNMENT_POINT_DUE_TO_NO_TEST_SUBPROGRAM_KEY"),
                                    StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          _testDev.getNavigationPanel().clickButton(StringLocalizer.keyToString("MM_GUI_GROUPINGS_KEY"));
        }
      });
    }
    
    //update optical rectangle used by each reconstruction region
    allocateOpticalCameraRectangleToComponents();
  }
    
  /**
   * @author Cheah Lee Herng
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (_isPanelBasedAlignment)
        {
          if (observable instanceof SelectedRendererObservable)
          {
            if (getPanelGraphicsEngine().isMouseRightClickMode() == false)
              handleSelectionEvent(object);
          } 
          else if (observable instanceof DragOpticalRegionObservable)
          {
            // do nothing
          } 
          else if (observable instanceof UnselectedDragOpticalRegionObservable)
          {
            handleDragOpticalRegionEvent(object);
          } 
          else if (observable instanceof UnselectedOpticalRegionObservable)
          {
            //handleOpticalRegionUnselectionEvent(object);
          } 
          else if (observable instanceof MouseClickObservable)
          {
            handleMouseClickEvent(object);
          } 
          else if (observable instanceof com.axi.v810.gui.undo.CommandManager)
          {
            handleUndoState(object);     
          }
          else
          {
            Assert.expect(false);
          }
        }
        else
        {
          if (observable instanceof SelectedRendererObservable)
          {
            if (getBoardGraphicsEngine().isMouseRightClickMode() == false)
              handleSelectionEvent(object);
          } 
          else if (observable instanceof DragOpticalRegionObservable)
          {
            // do nothing
          } 
          else if (observable instanceof UnselectedDragOpticalRegionObservable)
          {
            handleDragOpticalRegionEventForBoardBased(object);
          } 
          else if (observable instanceof UnselectedOpticalRegionObservable)
          {
            //handleOpticalRegionUnselectionEventForBoardBased(object);
          } 
          else if (observable instanceof MouseClickObservable)
          {
            handleMouseClickEventForBoardBased(object);
          } 
          else if (observable instanceof com.axi.v810.gui.undo.CommandManager)
          {
            handleUndoState(object);     
          }
          else
          {
            Assert.expect(false);
          }
        }
      }
    });
  }
    
  /**
   * @author Cheah Lee Herng
   */
  public void unpopulate()
  {
    _dragOpticalRegionRendererObservable = null;
    _unselectedOpticalRegionRendererObservable = null;
    _unselectedDragOpticalRegionRendererObservable = null;
    _panelGraphicsMouseClickObservable = null;
    addOrRemoveAllActionListeners(false);
  }
    
  /**
   * @author Cheah Lee Herng
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void finish()
  {  
    _active = false;

    if (_selectedRendererObservable != null)
    {
      _selectedRendererObservable.deleteObserver(this);
      _selectedRendererObservable = null;
    }

    if (_dragOpticalRegionRendererObservable != null)
    {
      _dragOpticalRegionRendererObservable.deleteObserver(this);
      _dragOpticalRegionRendererObservable = null;
    }

    if (_unselectedDragOpticalRegionRendererObservable != null)
    {
      _unselectedDragOpticalRegionRendererObservable.deleteObserver(this);
      _unselectedDragOpticalRegionRendererObservable = null;
    }

    if (_unselectedOpticalRegionRendererObservable != null)
    {
      _unselectedOpticalRegionRendererObservable.deleteObserver(this);
      _unselectedOpticalRegionRendererObservable = null;
    }

    if (_panelGraphicsMouseClickObservable != null)
    {
      _panelGraphicsMouseClickObservable.deleteObserver(this);
      _panelGraphicsMouseClickObservable = null;
    }
    
    if (_commandManager != null)
    {
      _commandManager.deleteObserver(this);
    }
    
    if (_guiObservable != null)
    {
      _guiObservable.deleteObserver(this);
    }
    
    if (_testDev != null)
    {
      if (_isPanelBasedAlignment)
      {
        _testDev.clearPackageGraphics();
        _testDev.setActiveTunerComponentType();
        _testDev.getPanelGraphicsEngineSetup().setOpticalRegionUnSelectedLayerVisible(false);
        _testDev.getPanelGraphicsEngineSetup().setOpticalRegionSelectedLayersVisible(false);
        _testDev.getPanelGraphicsEngineSetup().setDragOpticalRegionLayersVisible(false);
        _testDev.getPanelGraphicsEngineSetup().setHighlightOpticalRegionLayersVisible(false);
        _testDev.getPanelGraphicsEngineSetup().clearDividedOpticalRegionList();
        _testDev.getPanelGraphicsEngineSetup().clearSelectedOpticalCameraRectangleList();
        _testDev.getPanelGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
        _testDev.getPanelGraphicsEngineSetup().clearUnselectedOpticalCameraRectangleList();
        _testDev.getPanelGraphicsEngineSetup().removeMeshTriangleLayer();
      }
      else
      {
        _testDev.clearPackageGraphics();
        _testDev.setActiveTunerComponentType();
        _testDev.getBoardGraphicsEngineSetup().setOpticalRegionUnSelectedLayerVisible(false);
        _testDev.getBoardGraphicsEngineSetup().setOpticalRegionSelectedLayersVisible(false);
        _testDev.getBoardGraphicsEngineSetup().setDragOpticalRegionLayersVisible(false);
        _testDev.getBoardGraphicsEngineSetup().setHighlightOpticalRegionLayersVisible(false);
        _testDev.getBoardGraphicsEngineSetup().clearDividedOpticalRegionList();
        _testDev.getBoardGraphicsEngineSetup().clearSelectedOpticalCameraRectangleList();
        _testDev.getBoardGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
        _testDev.getBoardGraphicsEngineSetup().clearUnselectedOpticalCameraRectangleList();
        _testDev.getBoardGraphicsEngineSetup().removeMeshTriangleLayer();
        _testDev.clearBoardGraphics();
      }
      getPanelGraphicsEngine().resetSelectedRegionCoordinate();
      getPanelGraphicsEngine().clearCurrentlySelectedComponentRendererSet();
      getPanelGraphicsEngine().setDragRegionMode(false);
      getPanelGraphicsEngine().setDragSelectedRegionMode(false);
      getPanelGraphicsEngine().setGroupSelectMode(true);
    }
    
    _opticalRegion = null;
    _currentBoard = null;
    _currentBoardType = null;
    _boardsToApplyOpticalCameraRectangles.clear();
    _allowableRegionToPspModuleEnumMap.clear();
  }

  /**
   * @author Cheah Lee Herng
   */
  public void populateWithProjectData(Project project)
  {
    Assert.expect(project != null);

    _project = project;

    if (_active)
    {
      populateWithProjectData();
    }    
  }
    
  /**
   * @author Cheah Lee Herng
   * @author Ying-Huan.Chu
   */
  public void populateWithProjectData()
  {
    addOrRemoveAllActionListeners(true);
    _shouldInteractWithGraphics = false;
    _currentBoardType = _mainUI.getTestDev().getCurrentBoardType();
    if (_isPanelBasedAlignment == false) // Board-based
    {
      _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
    }
    validate();
    repaint();

    if (_isPanelBasedAlignment)
    {
      // XCR1602 - Modified by Jack 28 March 2013 - To handle missing PanelSurfaceMapSettings and BoardSurfaceMapSettings
      if (_project.getPanel().hasPanelSurfaceMapSettings() == false)
      {
        _project.getPanel().setPanelSurfaceMapSettings(new PanelSurfaceMapSettings());
      }  
      if (_project.getPanel().hasPanelMeshSettings()== false)
      {
        _project.getPanel().setPanelMeshSettings(new PanelMeshSettings());
      }
      _pspSelectedComponentTableModel.populateWithPanelData(_currentBoardType);
    }
    else
    {
      for (Board board : _project.getPanel().getBoards())
      {
        if (board.hasBoardSurfaceMapSettings() == false)
        {
          board.setBoardSurfaceMapSettings(new BoardSurfaceMapSettings(board));
        }
        if (board.hasBoardMeshSettings() == false)
        {
          board.setBoardMeshSettings(new BoardMeshSettings(board));
        }
      }
      _pspSelectedComponentTableModel.populateWithPanelData(_currentBoardType);
    }
    
    if (_project.getPanel().hasPspSettings() == false)
    {
      _project.getPanel().setPspSettings(new PspSettings());
    }
    
    //select display units specified by project
    _enumSelectedUnits = _project.getDisplayUnits();
    _unitsComboBox.setSelectedItem(MeasurementUnits.getMeasurementUnitString(_enumSelectedUnits));

    // Update measurement data
    updateAutoPopulateSpacing();
    updatePopulateRectangleOptions();

    _shouldInteractWithGraphics = true;
  }
    
  /**
   * @author Cheah Lee Herng
   */
  void addOrRemoveAllActionListeners(boolean addListeners)
  {
    if (addListeners)
    {
      _unitsComboBox.addActionListener(_unitsComboBoxActionListener);
    }
    else
    {
      _unitsComboBox.removeActionListener(_unitsComboBoxActionListener);
    }
  }
    
  /**
   * When the user selects a different measurement unit, all related measurement
   * values on the screen must be updated to reflect the newly selected units
   * and the project data selected units modified to reflect the change.
   *
   * @author Cheah Lee Herng
   */
  void unitsComboBox_actionPerformed()
  {
    //set the selected units in the project
    String units = _unitsComboBox.getSelectedItem().toString();
    _enumSelectedUnits = MeasurementUnits.getMeasurementUnitEnum(units);

    try
    {
      _commandManager.execute(new PanelSetAutoPopulateDisplayUnitsCommand(_project, _enumSelectedUnits));
      updateAutoPopulateSpacing();
      _pspRectangleTableModel.setDisplayUnit(_enumSelectedUnits);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
              ex.getLocalizedMessage(),
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
              true);
      updateAutoPopulateSpacing();
    }
    
  }

  /**
   * @author Cheah Lee Herng
   */
  private void initStaticComboChoices()
  {
    //add measurement unit options if they have not been previously added
    if (_unitsComboBox.getItemCount() == 0)
    {
      _unitsComboBox.addItem(MeasurementUnits.MEASURE_INCHES);
      _unitsComboBox.addItem(MeasurementUnits.MEASURE_MILLIMETERS);
      _unitsComboBox.addItem(MeasurementUnits.MEASURE_MILS);
    }
  }
    
  /**
   * @author Cheah Lee Herng
   */
  private void handleTextFieldData(NumericTextField numericTextField)
  {
    Assert.expect(numericTextField != null);

    if (numericTextField == _opticalRectangleSpacingTextField)
    {
      Double autoPopulateSpacing;
      try
      {
        autoPopulateSpacing = _opticalRectangleSpacingTextField.getDoubleValue();
      }
      catch (ParseException ex)
      {
        autoPopulateSpacing = new Double(0.0);
      }

      if (autoPopulateSpacing != null)
      {
        int autoPopulateSpacingInNanoMeters = (int) MathUtil.convertUnits(autoPopulateSpacing.doubleValue(),
                _enumSelectedUnits,
                MathUtilEnum.NANOMETERS);
        if (autoPopulateSpacingInNanoMeters <= 0 || autoPopulateSpacingInNanoMeters >= Integer.MAX_VALUE)
        {
          updateAutoPopulateSpacing();
          return;
        }
        try
        {
          _commandManager.execute(new PanelSetAutoPopulateSpacingCommand(_project.getPanel(), autoPopulateSpacingInNanoMeters));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  ex.getLocalizedMessage(),
                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                  true);
          updateAutoPopulateSpacing();
        }
      }
    }
  }
    
  /**
   * @author Cheah Lee Herng
   */
  private void updateCadGraphics()
  {
    if (_testDev == null)
    {
      _testDev = _mainUI.getTestDev();
    }
    if (_isPanelBasedAlignment == false)
    {
      _testDev.initGraphicsToBoard();
      changeGraphicsToBoard(_project.getPanel().getBoards().get(0));
      _testDev.switchToBoardGraphics();
      _selectedRendererObservable = getBoardGraphicsEngine().getSelectedRendererObservable();
    }
    else
    {
      _testDev.switchToPanelGraphics();
//      _testDev.resetSurfaceMapPanel(_project);
      _selectedRendererObservable = getPanelGraphicsEngine().getSelectedRendererObservable();
    }
    _selectedRendererObservable.addObserver(this); 
  }
    
  /**
   * @author Cheah Lee Herng
   * @author Ying-Huan.Chu
   */
  private void handleSelectionEvent(final Object object)
  {
    // We receive from the update, a collection of selected objects.  We'll only pay attention to
    // the ones this screen is interested in
    Collection<com.axi.guiUtil.Renderer> selections = (Collection< com.axi.guiUtil.Renderer>) object;

    // sort the fiducial so they're displayed by name.  The pads can't be sorted by their name since
    // I want the component name to be part of the sorting
    _pspSelectedComponentTable.getSelectionModel().setValueIsAdjusting(true);
    try
    {
      java.util.List<com.axi.v810.business.panelDesc.Component> selectedComponents = new ArrayList<>();
      java.util.List<String> listOfUntestableComponentNames = new ArrayList<>();
      if (_autoIncludeWholePanelComponentRadioButton.isSelected() == false)
        _includeFollowingComponentOnlyRadioButton.setSelected(true);
      if (_includeFollowingComponentOnlyRadioButton.isSelected())
      {
        for (com.axi.guiUtil.Renderer renderer : selections)
        {
          if (renderer instanceof ComponentRenderer)
          {
            com.axi.v810.business.panelDesc.Component component = ((ComponentRenderer) renderer).getComponent();
            if (isComponentUsableInSurfaceMap(component))
            {
              selectedComponents.add(component);
            }
            else
            {
              listOfUntestableComponentNames.add(component.toString());
            }
          }
        }
        if (listOfUntestableComponentNames.size() > 0)
        {
          String untestableComponentNames = "";
          for (int i = 0; i < listOfUntestableComponentNames.size(); ++i)
          {
            untestableComponentNames += listOfUntestableComponentNames.get(i);
            if (i < (listOfUntestableComponentNames.size() - 1))
            {
              untestableComponentNames += ", ";
            }
          }
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                        StringLocalizer.keyToString(new LocalizedString("PSP_UNTESTABLE_COMPONENTS_NOT_ADDED_MESSAGE_KEY", new Object[]{untestableComponentNames})),
                        StringLocalizer.keyToString("PSP_UNTESTABLE_COMPONENTS_NOT_ADDED_TITLE_KEY"),
                        JOptionPane.ERROR_MESSAGE);
        }
        // If we detect components with GSM turn on / PSH turn on, we need to prompt user that we will reset
        // GSM to AutoFocus and PSH to false in order for PSP to take effect
        // [Commented by Ying-Huan.Chu] : Somehow doing this checking here will trigger "Developer debug mode generating test program..."
        // BusyCancelDialog that will not go away. 
        //if (checkComponentsFilter(selectedComponents))
        //{
        if (_isPanelBasedAlignment)
        {
          _pspSelectedComponentTableModel.addComponents(selectedComponents);
        }
        else
        {
          java.util.List<com.axi.v810.business.panelDesc.Component> componentsToBeAdded = new ArrayList<>();
          for (Board board : getBoardsToApplyOpticalCameraRectangles())
          {
            for (com.axi.v810.business.panelDesc.Component component : selectedComponents)
            {
              if (board.hasComponent(component.toString()))
              {
                com.axi.v810.business.panelDesc.Component comp = board.getComponent(component.toString());
                componentsToBeAdded.add(comp);
              }
            }
          }
          _pspSelectedComponentTableModel.addComponents(componentsToBeAdded);
        }
        //}
      }
    }
    finally
    {
      _pspSelectedComponentTableModel.fireTableDataChanged();
      updateComponentsList();
      sortPspSelectedComponentTable();
    }
  }
    
  /**
   * @author Cheah Lee Herng
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void populateButton_actionPerformed(boolean isPopulateAroundSelectedComponent)
  {
    Assert.expect(_project != null);
    
    _testDev.getPanelGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
    _testDev.getPanelGraphicsEngineSetup().clearSelectedOpticalCameraRectangleList();
    _testDev.getPanelGraphicsEngineSetup().clearDividedOpticalRegionList();

    // Get the panel bounding rectangle in pixel
    PanelRectangle panelRectangle = null;
    if (_project.getPanel().getPanelSettings().isLongPanel())
    {
      panelRectangle = _project.getPanel().getPanelSettings().getRightSectionOfLongPanel();
      panelRectangle.add(_project.getPanel().getPanelSettings().getLeftSectionOfLongPanel().getRectangle2D());
    }
    else
    {
      panelRectangle = _project.getPanel().getPanelSettings().getRightSectionOfLongPanel();
    }
    
    double minX = _project.getPanel().getShapeInNanoMeters().getBounds().getMinX();
    double minY = _project.getPanel().getShapeInNanoMeters().getBounds().getMinY();
    double maxX = _project.getPanel().getShapeInNanoMeters().getBounds().getMaxX();
    double maxY = _project.getPanel().getShapeInNanoMeters().getBounds().getMaxY();
    
    if (_extendRectangleToBoardEdgeCheckbox.isSelected() == false)
    {
      minX = panelRectangle.getMaxX();
      minY = panelRectangle.getMaxY();
      maxX = panelRectangle.getMinX();
      maxY = panelRectangle.getMinY();
      for (com.axi.v810.business.panelDesc.Component comp : _project.getPanel().getComponents())
      {
        Rectangle rect = comp.getPadAreaRelativeToPanelInNanoMeters().getBounds();

        if (rect.getMinX() < minX)
        {
          minX = rect.getMinX();
        }

        if (rect.getMinY() < minY)
        {
          minY = rect.getMinY();
        }

        if (rect.getMaxX() > maxX)
        {
          maxX = rect.getMaxX();
        }

        if (rect.getMaxY() > maxY)
        {
          maxY = rect.getMaxY();
        }
      }
    }

    panelRectangle.setRect(minX, minY, (maxX - minX), (maxY - minY));
    
    int opticalRectangleSpacingInNanoMeters = _project.getPanel().getAutoPopulateSpacingInNanometers();

    java.util.List<OpticalCameraRectangle> opticalCameraRectangleList = OpticalRegionGeneration.generateCameraFovPanelRectangles(panelRectangle.getRectangle2D().getBounds(), opticalRectangleSpacingInNanoMeters);
    java.util.List<OpticalCameraRectangle> enableOpticalCameraRectangleList = new ArrayList<>();
    for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangleList)
    {
      // Kok Chun, Tan - XCR2490 - 24/2/2014
      // Check the optical camera used is enable or not. If not enable, skip the rectangle.
      if (_surfaceMapping.isOpticalCameraEnable(opticalCameraRectangle) == false)
      {
        continue;
      }
      enableOpticalCameraRectangleList.add(opticalCameraRectangle);
    }
    _testDev.getPanelGraphicsEngineSetup().setDividedOpticalRegionList(enableOpticalCameraRectangleList);
    
    // if 'Remove rectangles on top of component' checkbox is checked and 'Auto include whole panel component' radio button is selected
    java.util.List<OpticalCameraRectangle> dividedOpticalRegionList = new ArrayList<>(_testDev.getPanelGraphicsEngineSetup().getDividedOpticalRegionList());
    if ((_removeRectangleOnTopOfComponentCheckbox.isSelected()))
    {
      for (OpticalCameraRectangle ocr : _testDev.getPanelGraphicsEngineSetup().getDividedOpticalRegionList())
      {
        for (com.axi.v810.business.panelDesc.Component component : _project.getPanel().getComponents())
        {
          // if top component intersects with any rectangles, remove those rectangles.
          if (component.isTopSide() && 
              component.getPadAreaRelativeToPanelInNanoMeters().getBounds().contains(ocr.getRegion().getRectangle2D().getBounds()))
          {
            dividedOpticalRegionList.remove(ocr);
          }
        }
      }
    }
    
    // Handle Populate Around Selected Components button action
    if (isPopulateAroundSelectedComponent)
    {
      // checks if the component list is empty.
      if (getAllComponents().isEmpty())
      {
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                StringLocalizer.keyToString("PSP_COMPONENT_CANNOT_EMPTY_KEY"),
                StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                JOptionPane.ERROR_MESSAGE);
        return;
      }
      dividedOpticalRegionList = populateRectanglesBasedOnComponents(dividedOpticalRegionList, getAllComponents());
    }
  
    _testDev.getPanelGraphicsEngineSetup().setDividedOpticalRegionList(dividedOpticalRegionList);
    _testDev.getPanelGraphicsEngineSetup().setOpticalRegionSelectedLayersVisible(false);
    _testDev.getPanelGraphicsEngineSetup().setDragOpticalRegionLayersVisible(true);
    _testDev.getPanelGraphicsEngineSetup().handleDragOpticalRegion();
    
    invalidateChecksum();
   
    _hasPopulated = true;
    _isAddRectangleModeOn = false;
  }
  
  /**
   * @author Cheah Lee Herng
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void populateButtonForBoardBased_actionPerformed(boolean isPopulateAroundSelectedComponent)
  {
    Assert.expect(_project != null);
    
    _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
    if (_boardsToApplyOpticalCameraRectangles.contains(_currentBoard) == false)
    {
      _boardsToApplyOpticalCameraRectangles.add(_currentBoard);
    }
    updateCurrentSelectedBoardsString();
    _testDev.getBoardGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
    _testDev.getBoardGraphicsEngineSetup().setOpticalRegionSelectedLayersVisible(false);
    _testDev.getBoardGraphicsEngineSetup().clearDividedOpticalRegionList();

    // Get the panel bounding rectangle in pixel
    PanelRectangle boardRectangle = null;
 
    if (_currentBoard.isLongBoard())
    {
      boardRectangle = _currentBoard.getRightSectionOfLongBoard();
      boardRectangle.add(_currentBoard.getLeftSectionOfLongBoard().getRectangle2D());
    }
    else
    {
      boardRectangle = _currentBoard.getRightSectionOfLongBoard();
    }
    
    
    double minX = _currentBoard.getShapeRelativeToPanelInNanoMeters().getBounds().getMinX();
    double minY = _currentBoard.getShapeRelativeToPanelInNanoMeters().getBounds().getMinY();
    double maxX = _currentBoard.getShapeRelativeToPanelInNanoMeters().getBounds().getMaxX();
    double maxY = _currentBoard.getShapeRelativeToPanelInNanoMeters().getBounds().getMaxY();
    
    if (_extendRectangleToBoardEdgeCheckbox.isSelected() == false)
    {
      minX = boardRectangle.getMaxX();
      minY = boardRectangle.getMaxY();
      maxX = boardRectangle.getMinX();
      maxY = boardRectangle.getMinY();
      java.util.List<com.axi.v810.business.panelDesc.Component> componentList = _currentBoard.getComponents();
      for (com.axi.v810.business.panelDesc.Component comp : componentList)
      {
        Rectangle rect = comp.getPadAreaRelativeToPanelInNanoMeters().getBounds();
        if (rect.getMinX() < minX)
        {
          minX = rect.getMinX();
        }
        if (rect.getMinY() < minY)
        {
          minY = rect.getMinY();
        }
        if (rect.getMaxX() > maxX)
        {
          maxX = rect.getMaxX();
        }
        if (rect.getMaxY() > maxY)
        {
          maxY = rect.getMaxY();
        }
      }
    }

    boardRectangle.setRect(minX, minY, (maxX - minX), (maxY - minY));

    int opticalRectangleSpacingInNanoMeters = _project.getPanel().getAutoPopulateSpacingInNanometers();

    java.util.List<OpticalCameraRectangle> opticalCameraRectangleList = OpticalRegionGeneration.generateCameraFovPanelRectangles(boardRectangle.getRectangle2D().getBounds(), opticalRectangleSpacingInNanoMeters);
    for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangleList)
    {
      opticalCameraRectangle.setBoard(_currentBoard);
    }
    // Make a copy of OpticalCameraRectangle list first.
    java.util.List<OpticalCameraRectangle> opticalCameraRectangleListForBoard = new ArrayList<>(opticalCameraRectangleList);
    
    // if 'Remove rectangles on top of component' checkbox is checked and 'Auto include whole panel component' radio button is selected
    if ((_removeRectangleOnTopOfComponentCheckbox.isSelected()))
    {
      for (OpticalCameraRectangle ocr : opticalCameraRectangleList)
      {
        for (com.axi.v810.business.panelDesc.Component component : _project.getPanel().getComponents())
        {
          // if top component intersects with any rectangles, remove those rectangles.
          if (component.isTopSide() && 
              component.getPadAreaRelativeToPanelInNanoMeters().getBounds().contains(ocr.getRegion().getRectangle2D().getBounds()))
          {
            opticalCameraRectangleListForBoard.remove(ocr);
          }
        }
      }
    }

    // Handle Populate Around Selected Components button action
    if (isPopulateAroundSelectedComponent)
    {
      if (getAllComponents().isEmpty())
      {
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                StringLocalizer.keyToString("PSP_COMPONENT_CANNOT_EMPTY_KEY"),
                StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                JOptionPane.ERROR_MESSAGE);
        return;
      }
      else
      {
        for (Board board : _boardsToApplyOpticalCameraRectangles)
        {
          java.util.List<com.axi.v810.business.panelDesc.Component> componentList = getBoardComponentList(board);
          if (componentList.isEmpty())
          {
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                  StringLocalizer.keyToString(new LocalizedString("PSP_COMPONENT_FOR_BOARD_CANNOT_EMPTY_KEY", new Object[]{board.getName()})),
                  StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                  JOptionPane.ERROR_MESSAGE);
            return;
          }
        }
      }
      
      // Kok Chun, Tan - XCR2490 - 24/2/2014
      // Check the optical camera used is enable or not. If not enable, skip the rectangle.
      for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangleListForBoard)
      {
        if (_surfaceMapping.isOpticalCameraEnable(opticalCameraRectangle) == false)
        {
          continue;
        }
        _testDev.getBoardGraphicsEngineSetup().addDividedOpticalCameraRectangle(opticalCameraRectangle);
      }
      applyDividedOpticalCameraRectanglesToSelectedBoards(opticalCameraRectangleListForBoard);
      
      java.util.List<OpticalCameraRectangle> populateAroundComponentOpticalCameraRectangleList = new ArrayList<>();

      for (Board board : _boardsToApplyOpticalCameraRectangles)
      {
        java.util.List<com.axi.v810.business.panelDesc.Component> componentList = getBoardComponentList(board);
        opticalCameraRectangleListForBoard = populateRectanglesBasedOnComponents(_testDev.getBoardGraphicsEngineSetup().getDividedOpticalRegionList(board), componentList);
        for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangleListForBoard)
        {
          opticalCameraRectangle.setBoard(board);
          populateAroundComponentOpticalCameraRectangleList.add(opticalCameraRectangle);
        }
        componentList.clear();
      }
      _testDev.getBoardGraphicsEngineSetup().setDividedOpticalRegionList(populateAroundComponentOpticalCameraRectangleList);
    }
    else
    { 
      for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangleListForBoard)
      {
        if (_surfaceMapping.isOpticalCameraEnable(opticalCameraRectangle) == false)
        {
          continue;
        }
        _testDev.getBoardGraphicsEngineSetup().addDividedOpticalCameraRectangle(opticalCameraRectangle);
      }
      
      applyDividedOpticalCameraRectanglesToSelectedBoards(opticalCameraRectangleListForBoard);
    }
    
    _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
    _testDev.getBoardGraphicsEngineSetup().setOpticalRegionSelectedLayersVisible(false);
    _testDev.getBoardGraphicsEngineSetup().setDragOpticalRegionLayersVisible(true);
    _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegion();
    
    invalidateChecksum();
    
    _hasPopulated = true;
    _isAddRectangleModeOn = false;
  }
  
  /**
   * Populate 3 nearest rectangles to the selected components
   * @author Ying-Huan.Chu
   */
  private java.util.List<OpticalCameraRectangle> populateRectanglesBasedOnComponents(java.util.List<OpticalCameraRectangle> opticalRegionList,
                                                                         java.util.List<com.axi.v810.business.panelDesc.Component> componentList)
  {
    java.util.List<OpticalCameraRectangle> opticalRegionListWithSelectedComponent = new ArrayList<>();
    for (com.axi.v810.business.panelDesc.Component selectedComponent : componentList)
    {
      Map<Rectangle, Point> rectangleXYDistanceWithComponent = new LinkedHashMap<>();
      for (OpticalCameraRectangle ocr : opticalRegionList)
      {
        Rectangle rectInNano = ocr.getRegion().getRectangle2D().getBounds();
        int xDistance = Math.abs(MathUtil.roundDoubleToInt(rectInNano.getCenterX() - 
                        selectedComponent.getShapeRelativeToPanelInNanoMeters().getBounds().getCenterX()));
        int yDistance = Math.abs(MathUtil.roundDoubleToInt(rectInNano.getCenterY() - 
                        selectedComponent.getShapeRelativeToPanelInNanoMeters().getBounds().getCenterY()));
        Point distancePoint = new Point(xDistance, yDistance);
        rectangleXYDistanceWithComponent.put(rectInNano, distancePoint);
      }
      java.util.List<Map.Entry<Rectangle, Point>> sortedEntries = new ArrayList<>(rectangleXYDistanceWithComponent.entrySet());
      Collections.sort(sortedEntries, new Comparator<Map.Entry<Rectangle, Point>>() 
      {
        public int compare(Map.Entry<Rectangle, Point> a, Map.Entry<Rectangle, Point> b)
        {
          // return 1 if a > b; return 0 if a == b; return -1 if b > a;
          if (a.getValue().x > b.getValue().x)
          {
            if (a.getValue().y > b.getValue().y) return 1;
            if (a.getValue().y == b.getValue().y) return 1;
            if (a.getValue().y < b.getValue().y)
            {
              if (Math.abs(b.getValue().y - a.getValue().y) > Math.abs(a.getValue().x - b.getValue().x)) return -1;
              if (Math.abs(b.getValue().y - a.getValue().y) == Math.abs(a.getValue().x - b.getValue().x)) return 1;
              if (Math.abs(b.getValue().y - a.getValue().y) < Math.abs(a.getValue().x - b.getValue().x)) return 1;
            }
          }
          if (a.getValue().x == b.getValue().x)
          {
            if (a.getValue().y > b.getValue().y) return 1;
            if (a.getValue().y == b.getValue().y) return 0;
            if (a.getValue().y < b.getValue().y) return -1;
          }
          if (a.getValue().x < b.getValue().x)
          {
            if (a.getValue().y < b.getValue().y) return -1;
            if (a.getValue().y == b.getValue().y) return -1;
            if (a.getValue().y > b.getValue().y)
            {
              if (Math.abs(a.getValue().y - b.getValue().y) > Math.abs(b.getValue().x - a.getValue().x)) return 1;
              if (Math.abs(a.getValue().y - b.getValue().y) == Math.abs(b.getValue().x - a.getValue().x)) return -1;
              if (Math.abs(a.getValue().y - b.getValue().y) < Math.abs(b.getValue().x - a.getValue().x)) return -1;
            }
          }
          return 0;
        }
      });
      int numberOfRectangles = 0;
      if (sortedEntries.size() >= 3)
        numberOfRectangles = 3;
      else
        numberOfRectangles = sortedEntries.size();
      
      for (int i = 0; i < numberOfRectangles; ++i)
      {
        for (OpticalCameraRectangle ocr : opticalRegionList)
        {
          Rectangle rectInNano = ocr.getRegion().getRectangle2D().getBounds();
          if (rectInNano.contains(sortedEntries.get(i).getKey()))
          {
            if (opticalRegionListWithSelectedComponent.contains(ocr) == false)
              opticalRegionListWithSelectedComponent.add(ocr);
          }
        }
      }
    }
    return opticalRegionListWithSelectedComponent;
  }
    
  /**
   * @author Cheah Lee Herng
   */
  private void deleteComponentButton_actionPerformed()
  {
    if (_pspSelectedComponentTable.getSelectedRows().length > 0)
    {
      int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                                                 StringLocalizer.keyToString("PSP_DELETE_COMPONENT_KEY"),
                                                 StringLocalizer.keyToString("PSP_DELETE_COMPONENT_BUTTON_KEY"),
                                                 JOptionPane.YES_NO_OPTION,
                                                 JOptionPane.WARNING_MESSAGE);
      if (answer == JOptionPane.YES_OPTION)
      {
        final BusyCancelDialog busyDialog = new BusyCancelDialog(_mainUI,
                                                                 StringLocalizer.keyToString("PSP_DELETE_COMPONENT_IN_PROGRESS_KEY"),
                                                                 StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                                 true);
        busyDialog.pack();
        SwingUtils.centerOnComponent(busyDialog, _mainUI);
        SwingWorkerThread.getInstance().invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              java.util.List<com.axi.v810.business.panelDesc.Component> componentsToBeRemoved = new ArrayList<>();
              java.util.List<Board> boardsToRemoveOpticalRegion = new ArrayList<>();
              for (com.axi.v810.business.panelDesc.Component component : _pspSelectedComponentTableModel.getSelectedComponents())
              {
                componentsToBeRemoved.add(component);
              }
              boolean shouldPromptDeleteOpticalRegion = false;
              //Kok Chun, Tan - Once delete component then select _includeFollowingComponentOnlyRadioButton
              _autoIncludeWholePanelComponentRadioButton.setSelected(false);
              _isAutoIncludedAllComponents = false;
              _includeFollowingComponentOnlyRadioButton.setSelected(true);
              if (_isPanelBasedAlignment)
              {
                if (_project.getPanel().hasPanelSurfaceMapSettings())
                {
                  if (_project.getPanel().getPanelSurfaceMapSettings().hasOpticalRegion())
                  {
                    _opticalRegion = _project.getPanel().getPanelSurfaceMapSettings().getOpticalRegion();
                    if (_opticalRegion.getAllOpticalCameraRectangles().isEmpty() == false)
                    {
                      if (componentsToBeRemoved.containsAll(_opticalRegion.getComponents()))
                      {
                        shouldPromptDeleteOpticalRegion = true;
                      }
                    }
                  }
                }
              }
              else
              {
                for (Board board : _project.getPanel().getBoards())
                {
                  if (board.hasBoardSurfaceMapSettings())
                  {
                    if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
                    {
                      OpticalRegion opticalRegion = board.getBoardSurfaceMapSettings().getOpticalRegion();
                      if (opticalRegion.getOpticalCameraRectangles(board).isEmpty() == false)
                      {
                        if (componentsToBeRemoved.containsAll(opticalRegion.getComponents(board)))
                        {
                          shouldPromptDeleteOpticalRegion = true;
                          boardsToRemoveOpticalRegion.add(board);
                        }
                      }
                    }
                  }
                }
              }
              if (shouldPromptDeleteOpticalRegion == true)
              {
                int status = JOptionPane.showConfirmDialog(_mainUI, 
                                                           StringLocalizer.keyToString("MMGUI_REMOVE_ALL_COMPONENTS_WILL_REMOVE_ALL_RECTANGLES_WARNING_MESSAGE_KEY"),
                                                           StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), 
                                                           JOptionPane.YES_NO_OPTION, 
                                                           JOptionPane.QUESTION_MESSAGE);

                if (status == JOptionPane.NO_OPTION || status == JOptionPane.CLOSED_OPTION)
                {
                  return;
                }
                else if (status == JOptionPane.YES_OPTION)
                {  
                  final java.util.List<Board> boards = new ArrayList<>(boardsToRemoveOpticalRegion);
                  SwingUtils.invokeLater(new Runnable()
                  {
                    public void run()
                    {
                      _commandManager.trackState(getCurrentUndoState());
                      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PSP_DELETE_OPTICAL_REGION_KEY"));
                      if (_isPanelBasedAlignment)
                      {
                        deletePanelOpticalRegion();
                      }
                      else
                      {
                        for (Board board : boards)
                        {
                          deleteBoardOpticalRegion(board);
                        }
                      }
                      _commandManager.endCommandBlock();
                      if (_pspSelectedComponentTable.getRowCount() == 0)
                      {
                        _autoIncludeWholePanelComponentRadioButton.setSelected(false);
                        _isAutoIncludedAllComponents = false;
                        _includeFollowingComponentOnlyRadioButton.setSelected(false);
                      }
                    }    
                  });
                }
              }
              else
              {
                removeComponents(componentsToBeRemoved);
                deleteComponents(componentsToBeRemoved); //Janan - XCR-3257 
                _pspSelectedComponentTableModel.fireTableDataChanged();
                updateComponentsList();
                sortPspSelectedComponentTable();
              }
            }
            finally
            {
              busyDialog.dispose();
            }
          }
        });
        busyDialog.setVisible(true);
      }
      invalidateChecksum();
    }
  }
    
  /**
   * @author Cheah Lee Herng
   */
  private void updateAutoPopulateSpacing()
  {
    double autoPopulateSpacingInNanometers = _project.getPanel().getAutoPopulateSpacingInNanometers();
    autoPopulateSpacingInNanometers = MathUtil.convertUnits(autoPopulateSpacingInNanometers, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
    _opticalRectangleSpacingTextField.setValue(new Double(autoPopulateSpacingInNanometers));
  }
    
  /**
   * @author Cheah Lee Herng
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void pspRectangleTableListSelectionChanged(ListSelectionEvent e)
  {
    _hasPopulated = false;
    _isAddRectangleModeOn = false;
    int[] selectedRows = _pspRectangleTable.getSelectedRows();
    int selectedIndex = _pspRectangleTable.getSelectedRow();
    if (selectedIndex < 0)
      return;
    
    if (_isPanelBasedAlignment)
    {
      if (_project.getPanel().getPanelSurfaceMapSettings().hasOpticalRegion())
        _opticalRegion = _project.getPanel().getPanelSurfaceMapSettings().getOpticalRegion();
      _testDev.getPanelGraphicsEngineSetup().clearDividedOpticalRegionList();
      if (getPanelGraphicsEngine().isControlKeyPressMode() == false)
      {
        _testDev.getPanelGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
      }
      if (_opticalRegion != null)
      {
        if (_testDev.getPanelGraphicsEngineSetup().isSelectedOpticalRegionLayerVisible() == false)
        {
          _testDev.getPanelGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
        }
        _testDev.getPanelGraphicsEngineSetup().setMeshTriangleLayerVisible(_showMeshTriangleCheckbox.isSelected());
      }
      _testDev.getPanelGraphicsEngineSetup().setHighlightedOpticalCameraRectangleList(_pspRectangleTableModel.getSelectedOpticalCameraRectangles(selectedRows));

      //Jack Hwee - enable user to drag to select Blue optical rectangles
      _testDev.updateGroupSelectButton(false);
      getPanelGraphicsEngine().setDragSelectedRegionMode(true);
    }
    else
    {
      _currentBoard = _pspRectangleTableModel.getOpticalCameraRectangleUsingRowIndex(selectedIndex).getBoard();
      if (_currentBoard.getBoardSurfaceMapSettings().hasOpticalRegion())
        _opticalRegion = _currentBoard.getBoardSurfaceMapSettings().getOpticalRegion();

      _testDev.getBoardGraphicsEngineSetup().clearDividedOpticalRegionList();
      if (getBoardGraphicsEngine().isControlKeyPressMode() == false)
      {
        _testDev.getBoardGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
      }
      if (_opticalRegion != null)
      {
        if (_currentBoard.equals(_testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard()) == false ||
            _testDev.getBoardGraphicsEngineSetup().isSelectedOpticalRegionLayerVisible() == false)
        {
          changeGraphicsToBoard(_currentBoard);
          _testDev.getBoardGraphicsEngineSetup().handleMultipleSelectedOpticalCameraRectangles(_pspRectangleTableModel.getOpticalCameraRectangles());
          updateMeshTriangleOnCAD();
        }
      }
      _testDev.getBoardGraphicsEngineSetup().setHighlightedOpticalCameraRectangleList(_pspRectangleTableModel.getSelectedOpticalCameraRectangles(selectedRows));
      //Jack Hwee - enable user to drag to select Blue optical rectangles
      _testDev.updateGroupSelectButton(false);
      getBoardGraphicsEngine().setDragSelectedRegionMode(true);
    }
    
    // XCR-2503 - Automatically scroll to the select row.
    Rectangle rect = _pspRectangleTable.getCellRect(selectedIndex, 0, true);
    _pspRectangleTable.scrollRectToVisible(rect);
  }

  /**
   * Get updated data for Optical Rectangle table
   *
   * @author Ying-Huan.Chu
   */
  private void updateOpticalRectangleTableData()
  {
    _pspRectangleTableModel.populateWithPanelData(_currentBoardType);
    if (_isPanelBasedAlignment)
    {
      if (_project.getPanel().hasPanelSurfaceMapSettings())
      {
        if (_project.getPanel().getPanelSurfaceMapSettings().hasOpticalRegion())
        {
          _currentBoard = _project.getPanel().getBoards().get(0);
          _opticalRegion = _project.getPanel().getPanelSurfaceMapSettings().getOpticalRegion();
          _pspRectangleTableModel.populateWithOpticalRegion(_opticalRegion);
          _testDev.getPanelGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
        }
      }
    }
    else
    {
      for (Board board : _currentBoardType.getBoards())
      {
        if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
        {
          _opticalRegion = board.getBoardSurfaceMapSettings().getOpticalRegion();
          _pspRectangleTableModel.populateWithOpticalRegion(_opticalRegion);
        }
      }
      if (_pspRectangleTableModel.getOpticalCameraRectangles().size() > 0)
      {
        for (Board board : _currentBoardType.getBoards())
        {
          if (board.hasBoardSurfaceMapSettings())
          {
            if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
            {
              _opticalRegion = board.getBoardSurfaceMapSettings().getOpticalRegion();
              if (_opticalRegion.getOpticalCameraRectangles(board).contains(_pspRectangleTableModel.getOpticalCameraRectangleUsingRowIndex(0)))
              {
                _currentBoard = board;
                _testDev.getBoardGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
                break;
              }
            }
          }
        }
      }
    }
    _pspRectangleTableModel.sortColumn(0, true);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public GraphicsEngine getPanelGraphicsEngine()
  {
    if (_testDev == null)
      _testDev = _mainUI.getTestDev();
    return _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public GraphicsEngine getBoardGraphicsEngine()
  {
    if (_testDev == null)
      _testDev = _mainUI.getTestDev();
    return _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine();
  }

  /**
   * @author Cheah Lee Herng
   */
  private void editRectangle_actionPerformed()
  {
    // checks if system has started up with panel loaded.
    try
    {
      if (_mainUI.beforeGenerateSurfaceMapping(this) == false)
      {
        return;
      }
      if (_mainUI.isPanelInLegalState() == false)
      {
        return;
      }
      if (checkIfOpticalCalibrationDataExists() == false)
      {
        handleNoOpticalCalibrationDataError();
        return;
      }
      int[] selectedRows = _pspRectangleTable.getSelectedRows();
      java.util.List<OpticalCameraRectangle> opticalCameraRectangles = new ArrayList<>();
      for (int i = 0; i < selectedRows.length; ++i)
      {
        OpticalCameraRectangle opticalCameraRectangle = _pspRectangleTableModel.getOpticalCameraRectangleUsingRowIndex(selectedRows[i]);
        opticalCameraRectangles.add(opticalCameraRectangle);
      }
      for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangles)
      {
        if (editSurfaceMapPoints(opticalCameraRectangle) == false)
        {
          // user canceled edit point.
          break;
        }
      }
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
    }
  }

  /**
   * Return false if system is unable to proceed with Surface Mapping or user canceled Surface Mapping.
   * @author Cheah Lee Herng
   */
  private boolean editSurfaceMapPoints(final OpticalCameraRectangle opticalCameraRectangle) 
  {
    Assert.expect(_project != null);

    // Kok Chun, Tan - XCR2490 - 24/2/2014
    // Check the optical camera used is enable or not. If not enable, prompt message.
    if (_surfaceMapping.isOpticalCameraEnable(opticalCameraRectangle) == false)
    {
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
        StringLocalizer.keyToString("PSP_ADD_OPTICAL_CAMERA_RECTANGLE_OUT_OF_CAMERA_INSPECTABLE_REGION_MESSAGE_KEY"),
        StringLocalizer.keyToString("PSP_ADD_OPTICAL_CAMERA_OUT_OF_INSPECTABLE_REGION_TITLE_KEY"),
        JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (getAllComponents().isEmpty())
    {
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringLocalizer.keyToString("PSP_COMPONENT_CANNOT_EMPTY_KEY"),
                                    StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      return false;
    }
    try
    {
      Board selectedBoard = null;
      OpticalRegion opticalRegion = null;
      final boolean simulationMode = XrayTester.getInstance().isSimulationModeOn();

      // checks if system has started up with panel loaded.
      boolean hasHighMagComponent = _testExecution.containHighMagInspectionInCurrentLoadedProject();
      if (_testExecution.isPanelLoaded(hasHighMagComponent, _project.getName()) == false)
      {
        _mainUI.performStartupIfNeeded();
        _mainUI.loadPanel();
      }
      else
      {
        _testExecution.checkThatCurrentlyLoadedPanelProjectNameMatchesProjectName(_project.getTestProgram());
      }
      
//      if (checkComponentsFilter(getAllComponents()) == false)
//        return false;
      
      // XCR-2726 Cheah Lee Herng - Continue run surface map although click on "X" button
      if (checkComponentsFilter(getAllComponents()) == false)
        return false;

      if (_mainUI.isPanelInLegalState() == false)
      {
        return false;
      }

      if (_mainUI.beforeGenerateSurfaceMapping(this))
      {
        // Jack Hwee - XCR1773 - If there are optical rectangle for Version 1 PSP, convert it to suite current version
        checkOpticalCameraRectangleCompatibility(opticalCameraRectangle);

        PanelRectangle panelRectangleInNanometers = opticalCameraRectangle.getRegion();
        final Rectangle rectangleInNanometers = new Rectangle(panelRectangleInNanometers.getMinX(),
                                                              panelRectangleInNanometers.getMinY(),
                                                              panelRectangleInNanometers.getWidth(),
                                                              panelRectangleInNanometers.getHeight());
        
        for (Board board : _project.getPanel().getBoards())
        {
          if (board.equals(opticalCameraRectangle.getBoard()))
          {
            selectedBoard = board;
            break;
          }
        }
        if (selectedBoard == null)
        {
          for (Board board : _project.getPanel().getBoards())
          {
            if (board.getShapeRelativeToPanelInNanoMeters().getBounds().intersects(rectangleInNanometers))
            {
              selectedBoard = board;
              break;
            }
          }
        }
        Assert.expect(selectedBoard != null);
        
        updateOpticalCameraSetting(_project.getPanel().isLargerFOV());
        // Set current optical region
        if (_isPanelBasedAlignment)
        {
          opticalRegion = _opticalRegion;
        }
        else
        {
          if (selectedBoard.hasBoardSurfaceMapSettings())
          {
            if (selectedBoard.getBoardSurfaceMapSettings().hasOpticalRegion())
            {
              for (OpticalRegion boardOpticalRegion : selectedBoard.getBoardSurfaceMapSettings().getOpticalRegions())
              {
                if (boardOpticalRegion.hasOpticalCameraRectangles(selectedBoard))
                {
                  for (OpticalCameraRectangle boardOcr : boardOpticalRegion.getOpticalCameraRectangles(selectedBoard))
                  {
                    if (boardOcr.equals(opticalCameraRectangle))
                    {
                      opticalRegion = boardOpticalRegion;
                      break;
                    }
                  }
                }
              }
            }
          }
        }
        Assert.expect(opticalRegion != null);
        
        // Jack Hwee - Make sure image is "latest" for display
        deleteTempOpticalImage();

        final Board finalBoard = selectedBoard;
        final BusyCancelDialog busyDialog = new BusyCancelDialog(_mainUI,
                                                                 StringLocalizer.keyToString("PSP_UPDATE_DATA_KEY"),
                                                                 StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                                 true);
        busyDialog.pack();
        SwingUtils.centerOnComponent(busyDialog, _mainUI);
        SwingWorkerThread.getInstance().invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              doRegionSurfaceMapping(opticalCameraRectangle, finalBoard);

              if (_isSurfaceMappingSuccess && simulationMode == false && _isAlignmentSuccess)
                waitForHeightMap();
            }
            finally
            {
              busyDialog.dispose();
            }
          }
        });
        busyDialog.setVisible(true);
        
        if (_isSurfaceMappingSuccess && simulationMode == false && _isHeightMapAvailable == false)
        {
          handleUnableToRetrieveHeightMapError();
          return false;
        }

        if (_isSurfaceMappingSuccess == false || _isAlignmentSuccess == false)
        {
          return false;
        }
        
        // Display Surface Map Live View dialog
        if (_pspLiveVideoDialog != null)
        {
          _pspLiveVideoDialog.dispose();
          _pspLiveVideoDialog = null;
        }

        _pspLiveVideoDialog = new SurfaceMapLiveVideoDialog(_mainUI,
                                                            StringLocalizer.keyToString("PSP_SURFACE_MAP_LIVE_VIEW_DIALOG_TITLE_KEY"),
                                                            finalBoard,
                                                            opticalRegion.getBoards(),
                                                            opticalRegion,
                                                            opticalCameraRectangle,
                                                            simulationMode,
                                                            false,
                                                            _enumSelectedUnits,
                                                            _isLiveViewMode,
                                                            _isPreviousButtonVisible);
        _pspLiveVideoDialog.setLocationRelativeTo(this._opticalRegionInformationSplitPane);
        _pspLiveVideoDialog.setVisible(true);

        if (_pspLiveVideoDialog.isCancelSurfaceMapping())
        {
          if (_isPanelBasedAlignment)
          {
            _testDev.getPanelGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
          }
          else
          {
            _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
            if (_currentBoard.hasBoardSurfaceMapSettings())
            {
              if (_currentBoard.getBoardSurfaceMapSettings().hasOpticalRegion())
              {
                _opticalRegion = _currentBoard.getBoardSurfaceMapSettings().getOpticalRegion();
                _testDev.getBoardGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
              }
            }
          }
        }
        if (_pspLiveVideoDialog.isCancelSurfaceMapping() == false)
        {
          populateWithProjectData();
        }

        if (_pspRectangleTable.getRowCount() > 0)
        {
          if (_isPanelBasedAlignment)
          {
            _testDev.getPanelGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
          }
          else
          {
            _opticalRegion = _currentBoard.getBoardSurfaceMapSettings().getOpticalRegion();
            _testDev.getBoardGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
          }
        }

        // update mesh triangle CAD
        updateMeshTriangleOnCAD();
        //update optical rectangle used by each reconstruction region
        allocateOpticalCameraRectangleToComponents();

        if (_pspLiveVideoDialog.hasException())
        {
          _mainUI.handleXrayTesterException(_pspLiveVideoDialog.getException());
          return false;
        }

        if (_pspLiveVideoDialog.isCancelSurfaceMapping())
          return false;
        else
          return true;
      }
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
    }
    return false;
  }
  
  /**
   * @author Jack Hwee
   */
  public boolean checkProjectHasBeenModified()
  {
    // we must have saved/up-to-date project in order to generate an image set that we know is compatible.
    // so, here, if the project is "dirty" then ask the user to save.  If they save, then move on to generate the
    // image set, if not do nothing here.
    boolean startSurfaceMapping = true;

    if (_project.hasBeenModified())
    {
      LocalizedString saveQuestion = new LocalizedString("MM_GUI_SAVE_CHANGES_FOR_SURFACE_MAPPING_KEY",
              new Object[]
      {
        _project.getName()
      });
      int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                                                 StringUtil.format(StringLocalizer.keyToString(saveQuestion), _CHARS_PER_LINE),
                                                 StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                 JOptionPane.YES_NO_OPTION,
                                                 JOptionPane.WARNING_MESSAGE);
      if (answer == JOptionPane.YES_OPTION)
      {
        _mainUI.getMainMenuBar().saveProject();
        startSurfaceMapping = true;
      }
      else
      {
        startSurfaceMapping = false;  // chose not to save, so exit this operation
      }
    }
    return startSurfaceMapping;
  }

  /**
   * @author Jack Hwee
   */
  public boolean isSurfaceMapping()
  {
    return true;
  }

  /**
   * @author Jack Hwee
   */
  private void doRegionSurfaceMapping(OpticalCameraRectangle opticalCameraRectangle, Board board)
  {
    Assert.expect(opticalCameraRectangle != null);
    Assert.expect(board != null);
    
    _isSurfaceMappingSuccess = false; // reset this flag
    
    // perform alignment if necessary
    performAlignmentIfNecessary(opticalCameraRectangle, _isPanelBasedAlignment);
    if (_isAlignmentSuccess == false)
    {
      return;
    }  
    _surfaceMapping.setCurrentAffineTransformForOpticalRegion(this.getCurrentAffineTransformForOpticalRegion());
    try
    {
      _testExecution.doRegionSurfaceMapping(opticalCameraRectangle, board);
      _isSurfaceMappingSuccess = true;
    }
    catch (XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
      _isHeightMapAvailable = false;
      _isSurfaceMappingSuccess = false;
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void moveStageToOpticalCameraRectanglePosition(OpticalCameraRectangle opticalCameraRectangle, Board board)
  {
    Assert.expect(opticalCameraRectangle != null);
    Assert.expect(board != null);
    
    // perform alignment if necessary
    performAlignmentIfNecessary(opticalCameraRectangle, _isPanelBasedAlignment);
    if (_isAlignmentSuccess == false)
    {
      return;
    }  
    try
    {
      _testExecution.moveStageToOpticalCameraRectanglePosition(opticalCameraRectangle, board);
    }
    catch (XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
      _isHeightMapAvailable = false;
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  private boolean checkComponentsFilter(java.util.List<com.axi.v810.business.panelDesc.Component> components)
  {
    Assert.expect(components != null);
    
    ArrayList<String> componentNameList = new ArrayList<>();
    java.util.List<com.axi.v810.business.panelDesc.Component> globalSurfaceModelOrPredictiveSliceHeightComponents = new ArrayList<>();
    java.util.List<com.axi.v810.business.panelDesc.Component> uninspectedComponents = new ArrayList<>();
    boolean shouldSystemProceed = false;
    
    for (com.axi.v810.business.panelDesc.Component component : components)
    {
      ComponentType componentType = component.getComponentType();
      GlobalSurfaceModelEnum globalSurfaceModelEnum = componentType.getGlobalSurfaceModel();
      if ((globalSurfaceModelEnum.equals(GlobalSurfaceModelEnum.AUTOFOCUS) == false
           || usePredictiveSliceHeight(componentType)) && componentNameList.contains(component.getReferenceDesignator()) == false)
      {        
        globalSurfaceModelOrPredictiveSliceHeightComponents.add(component);
        componentNameList.add(component.getReferenceDesignator());
      }

      if (isComponentUsableInSurfaceMap(component) == false)
      {
        if (uninspectedComponents.contains(component) == false)
          uninspectedComponents.add(component);
      }
    }

    if (uninspectedComponents.size() > 0 && uninspectedComponents.size() == getAllComponents().size())
    {
      LocalizedString uninspectedComponentWarning = new LocalizedString("PSP_UNINSPECTED_COMPONENT_WARNING_KEY", new Object[]{});
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringLocalizer.keyToString(uninspectedComponentWarning),
                                    StringLocalizer.keyToString("PSP_RESET_COMPONENT_BUTTON_KEY"),
                                    JOptionPane.WARNING_MESSAGE);
      uninspectedComponents.clear();
      globalSurfaceModelOrPredictiveSliceHeightComponents.clear();
      return false; // return here as all components are uninspectable.
    }

    if (globalSurfaceModelOrPredictiveSliceHeightComponents.size() > 0)
    {
      java.util.List<String> componentNames = new ArrayList();
      for (com.axi.v810.business.panelDesc.Component comp : globalSurfaceModelOrPredictiveSliceHeightComponents)
      {
        componentNames.add(comp.toString());
      }
      ItemListBoxDialog gsmOrPshTurnOnDialog = new ItemListBoxDialog(_mainUI,
                                                                     StringLocalizer.keyToString("PSP_RESET_SETTING_TITLE_KEY"),
                                                                     StringLocalizer.keyToString("PSP_RESET_SETTING_MESSAGE_KEY"),
                                                                     StringLocalizer.keyToString("GUI_YES_BUTTON_KEY"),
                                                                     StringLocalizer.keyToString("GUI_NO_BUTTON_KEY"),
                                                                     componentNames);
      if (gsmOrPshTurnOnDialog.isUserClickedYes())
      {
        resetGlobalSurfaceModelOrPredictiveSliceHeightComponents(globalSurfaceModelOrPredictiveSliceHeightComponents);
        shouldSystemProceed = true;
      }
      else if (gsmOrPshTurnOnDialog.isUserClickedClose())
      {
        shouldSystemProceed = false;
      }
      else
      {
        shouldSystemProceed = true;
      }
    }
    else
    {
      shouldSystemProceed = true;
    }
    globalSurfaceModelOrPredictiveSliceHeightComponents.clear();
    uninspectedComponents.clear();
    return shouldSystemProceed;
  }

  /**
   * @author Cheah Lee Herng
   */
  private void resetGlobalSurfaceModelOrPredictiveSliceHeightComponents(java.util.List<com.axi.v810.business.panelDesc.Component> components)
  {
    Assert.expect(components != null);
    Assert.expect(_project != null);

    com.axi.v810.business.testProgram.TestProgram testProgram = _project.getTestProgram();

    for (com.axi.v810.business.panelDesc.Component component : components)
    {
      ComponentType componentType = component.getComponentType();
      java.util.List<com.axi.v810.business.testProgram.ReconstructionRegion> reconstructionRegions = testProgram.getInspectionRegions(component);

      if (componentType.getGlobalSurfaceModel().equals(GlobalSurfaceModelEnum.AUTOFOCUS) == false)
      {
        for (PadType padType : componentType.getPadTypes())
        {
          padType.getPadTypeSettings().setGlobalSurfaceModel(GlobalSurfaceModelEnum.AUTOFOCUS);
        }

        for (com.axi.v810.business.testProgram.ReconstructionRegion reconstructionRegion : reconstructionRegions)
        {
          reconstructionRegion.setGlobalSurfaceModelEnum(GlobalSurfaceModelEnum.AUTOFOCUS);
        }
      }

      if (usePredictiveSliceHeight(componentType))
      {
        resetPredictiveSliceHeight(componentType);
      }

      componentType.setToUsePspResult(true);

      // Clear memory
      if (reconstructionRegions != null)
      {
        reconstructionRegions.clear();
      }
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  private boolean usePredictiveSliceHeight(ComponentType componentType)
  {
    Assert.expect(componentType != null);

    boolean usePredictiveSliceHeight = false;

    java.util.List<Subtype> subtypes = componentType.getSubtypes();
    for (Subtype subtype : subtypes)
    {
      if (subtype.usePredictiveSliceHeight())
      {
        usePredictiveSliceHeight = true;
        break;
      }
    }
    return usePredictiveSliceHeight;
  }

  /**
   * @author Cheah Lee Herng
   */
  private boolean resetPredictiveSliceHeight(ComponentType componentType)
  {
    Assert.expect(componentType != null);

    boolean usePredictiveSliceHeight = false;

    java.util.List<Subtype> subtypes = componentType.getSubtypes();
    for (Subtype subtype : subtypes)
    {
      subtype.setTunedValue(com.axi.v810.business.imageAnalysis.AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT, new String("Off"));
    }
    return usePredictiveSliceHeight;
  }

  /**
   * @author Jack Hwee Need this affine transform from graphics engine for
   * conversion used
   */
  public AffineTransform getCurrentAffineTransformForOpticalRegion()
  {
    _testDev = _mainUI.getTestDev();
    _project = Project.getCurrentlyLoadedProject();
    if (_isPanelBasedAlignment)
      return getPanelGraphicsEngine().getCurrentAffineTransformForOpticalRegion();
    else
      return getBoardGraphicsEngine().getCurrentAffineTransformForOpticalRegion();
  }

  /**
   * Given x,y coordinates in pixels, return out a Point in Renderer
   * coordinates.
   *
   * @return a Point in Renderer coordinates transformed from pixel coordinates
   * @author Bill Darbie
   */
  public Point2D convertFromPixelToRendererCoordinates(int xInPixels, int yInPixels)
  {
    /*Assert.expect(_transformFromRendererCoords != null);
    
    if (_project == null)
      _project = Project.getCurrentlyLoadedProject();
    
    if (_isPanelBasedAlignment)
      _transformFromRendererCoords = getPanelGraphicsEngine().getCurrentAffineTransformForOpticalRegion();
    else
      _transformFromRendererCoords = getBoardGraphicsEngine().getCurrentAffineTransformForOpticalRegion();

    Point2D point = new Point2D.Double(xInPixels, yInPixels);
    AffineTransform inverse = null;
    try
    {
      inverse = _transformFromRendererCoords.createInverse();
    }
    catch (NoninvertibleTransformException ex)
    {
      ex.printStackTrace();
      Assert.expect(false);
    }

    inverse.transform(point, point);*/
    
    Point2D point = new Point2D.Double(0, 0);
    if (_isPanelBasedAlignment)
      point = getPanelGraphicsEngine().convertFromPixelToRendererCoordinates(xInPixels, yInPixels);
    else
      point = getBoardGraphicsEngine().convertFromPixelToRendererCoordinates(xInPixels, yInPixels);

    return point;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public java.util.List<com.axi.v810.business.panelDesc.Component> getBoardComponentList(Board board)
  {
    Assert.expect(board != null);
    java.util.List<com.axi.v810.business.panelDesc.Component> componentList = new ArrayList<>();
    for (com.axi.v810.business.panelDesc.Component component : getAllComponents())
    {
      if (component.getBoard().equals(board))
      {
        componentList.add(component);
      }
    }
    return componentList;
  }
  
  /**
   * Anywhere that needs the whole component list should call this function.
   * Returns the components listed in PspSelectedComponentTableModel.
   * @author Ying-Huan.Chu
   */
  public java.util.List<com.axi.v810.business.panelDesc.Component> getAllComponents()
  {
    Assert.expect(_pspSelectedComponentTableModel != null);
    
    return _pspSelectedComponentTableModel.getComponents();
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void addComponents(java.util.List<com.axi.v810.business.panelDesc.Component> components)
  {
    Assert.expect(components != null);

    // We don't have to check if the components already exist in the Component table as the checking is already done in this function:
    _pspSelectedComponentTableModel.addComponents(components);
    // if there's optical region, we should add the component to the optical region.
    if (_isPanelBasedAlignment)
    {
      java.util.List<com.axi.v810.business.panelDesc.Component> componentsToBeAdded = new ArrayList<>();
      if (_project.getPanel().hasPanelSurfaceMapSettings())
      {
        PanelSurfaceMapSettings panelSurfaceMapSettings = _project.getPanel().getPanelSurfaceMapSettings();
        if (panelSurfaceMapSettings.hasOpticalRegion())
        {
          for (com.axi.v810.business.panelDesc.Component component : components)
          {
            if (panelSurfaceMapSettings.getOpticalRegion().hasComponent(component) == false)
            {
              componentsToBeAdded.add(component);
            }
          }
        }
        if (componentsToBeAdded.isEmpty() == false) // this checking is needed so that there'll be no empty command.
        {
          try
          {
            _commandManager.trackState(getCurrentUndoState());
            _commandManager.execute(new SurfaceMapAddComponentListCommand(panelSurfaceMapSettings.getOpticalRegion(), componentsToBeAdded, true));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
          }
        }
      }
    }
    else // board-based
    {
      Map<Board, java.util.List<com.axi.v810.business.panelDesc.Component>> boardToComponentsMap = new LinkedHashMap<>();
      for (com.axi.v810.business.panelDesc.Component component : components)
      {
        Board board = component.getBoard();
        if (board.hasBoardSurfaceMapSettings())
        {
          BoardSurfaceMapSettings boardSurfaceMapSettings = board.getBoardSurfaceMapSettings();
          if (boardSurfaceMapSettings.hasOpticalRegion())
          {
            if (board.getBoardSurfaceMapSettings().getOpticalRegion().getComponents(board).contains(component) == false)
            {
              if (boardToComponentsMap.containsKey(board))
              {
                boardToComponentsMap.get(board).add(component);
              }
              else
              {
                java.util.List<com.axi.v810.business.panelDesc.Component> componentList = new ArrayList<>();
                componentList.add(component);
                boardToComponentsMap.put(board, componentList);
              }
            }
          }
        }
      }
      if (boardToComponentsMap.isEmpty() == false)
      {
        _commandManager.trackState(getCurrentUndoState());
        _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_ADD_COMPONENT_KEY"));
        for (Map.Entry<Board, java.util.List<com.axi.v810.business.panelDesc.Component>> entry : boardToComponentsMap.entrySet())
        {
          Board board = entry.getKey();
          java.util.List<com.axi.v810.business.panelDesc.Component> componentList = entry.getValue();
          BoardSurfaceMapSettings boardSurfaceMapSettings = board.getBoardSurfaceMapSettings();
          if (componentList.isEmpty() == false) // this checking is needed so that there'll be no empty command.
          {
            try
            {
              _commandManager.execute(new SurfaceMapAddComponentListCommand(boardSurfaceMapSettings.getOpticalRegion(), componentList, false));
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                            ex.getLocalizedMessage(),
                                            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                            true);
            }
          }
        }
        _commandManager.endCommandBlock();
      }
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void selectedComponentTableListSelectionChanged(ListSelectionEvent e)
  {
    int selectedIndex = _pspSelectedComponentTable.getSelectedRow();
    if (selectedIndex >= 0)
    {
      Object selectedObject = _pspSelectedComponentTable.getValueAt(selectedIndex, 1);
      
      if (selectedObject instanceof com.axi.v810.business.panelDesc.Component)
      {
        com.axi.v810.business.panelDesc.Component component = (com.axi.v810.business.panelDesc.Component) selectedObject;
        java.util.List<ComponentType> componentType = new ArrayList<>();
        componentType.add(_pspSelectedComponentTableModel.getSelectedComponents().get(0).getComponentType());
        if (_isPanelBasedAlignment)
        {
          _guiObservable.stateChanged(componentType, SelectionEventEnum.COMPONENT_TYPE);
        }
        else
        {
          if (_currentBoard.getName().equalsIgnoreCase(component.getBoard().getName()) == false)
          {
            changeGraphicsToBoard(component.getBoard());
            _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
          }
          _guiObservable.stateChanged(componentType, SelectionEventEnum.COMPONENT_TYPE);
        }
        //highlight optical camera rectangle used by the selected component.
        highlightOpticalCameraRectangleUsedBasedOnComponents(component);
      }
      else
      {
        Assert.expect(false);
      }
    }
  }

  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void handleMouseClickEvent(final Object object)
  {
    Assert.expect(object != null);
    
    MouseEvent mouseEvent = (MouseEvent) object;
    final int pixelX = mouseEvent.getX();
    final int pixelY = mouseEvent.getY();
    boolean isControlKeyPressed = getPanelGraphicsEngine().isControlKeyPressMode();

    if (SwingUtilities.isLeftMouseButton(mouseEvent))
    {
      // select / highlight the rectangle if user clicks on it.
      if (_shouldInteractWithGraphics)
      {
        Point2D mouseLeftClickCoordinateInNanometer = getPanelGraphicsEngine().convertFromPixelToRendererCoordinates(pixelX, pixelY);
        if (_testDev.getPanelGraphicsEngineSetup().isDragOpticalRegionLayerVisible())
        {
          for (OpticalCameraRectangle ocr : _testDev.getPanelGraphicsEngineSetup().getDividedOpticalRegionList())
          {
            Rectangle rectInNano = ocr.getRegion().getRectangle2D().getBounds();
            if (rectInNano.contains(mouseLeftClickCoordinateInNanometer))
            {
              if (isControlKeyPressed == false)
              {
                _testDev.getPanelGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
              }
              _testDev.getPanelGraphicsEngineSetup().addHighlightedOpticalCameraRectangle(ocr);
              _testDev.getPanelGraphicsEngineSetup().handleDragOpticalRegion();
              _testDev.updateGroupSelectButton(false);
              break;
            }
          }
        }
        if (_testDev.getPanelGraphicsEngineSetup().isSelectedOpticalRegionLayerVisible())
        {
          for (OpticalCameraRectangle ocr : _testDev.getPanelGraphicsEngineSetup().getSelectedOpticalCameraRectangleList())
          {
            Rectangle rectInNano = ocr.getRegion().getRectangle2D().getBounds();
            if (rectInNano.contains(mouseLeftClickCoordinateInNanometer))
            {
              if (isControlKeyPressed == false)
              {
                _testDev.getPanelGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
                _pspRectangleTable.clearSelection();
              }
              _testDev.getPanelGraphicsEngineSetup().addHighlightedOpticalCameraRectangle(ocr);
              int selectedRow = _pspRectangleTableModel.getRowIndexUsingOpticalRectangleName(ocr.getName());
              if (selectedRow > -1)
              {
                _pspRectangleTable.addRowSelectionInterval(selectedRow, selectedRow);
              }
              int[] selectedRows = _pspRectangleTable.getSelectedRows();
              if (selectedRows.length > 0)
                _pspRectangleTable.setSelectedRows(selectedRows);
              break;
            }
          }
        }
        _testDev.getPanelGraphicsEngineSetup().handleMultipleHighlightOpticalCameraRectangles(_testDev.getPanelGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList());
      }
    }
    else if (SwingUtilities.isRightMouseButton(mouseEvent) && (isControlKeyPressed == false))
    {
      // handle right click with a popup menu
      if (_shouldInteractWithGraphics)
      {     
        JPopupMenu popup = new JPopupMenu(); // this name is never seen by the users
        Point mouseRightClickCoordinate = new Point(pixelX, pixelY);
        Point2D mouseRightClickCoordinateInNanometer = getPanelGraphicsEngine().convertFromPixelToRendererCoordinates(pixelX, pixelY);
        boolean isRightClickOnOpticalCameraRectangle = false;
        
        java.util.List<OpticalCameraRectangle> highlightedOpticalCameraRectangles = 
                new ArrayList<>(_testDev.getPanelGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList());
        if (_testDev.getPanelGraphicsEngineSetup().isHighlightOpticalRegionLayerVisible())
        {
          for (OpticalCameraRectangle ocr : _testDev.getPanelGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList())
          {
            Rectangle rectInNano = ocr.getRegion().getRectangle2D().getBounds();
            if (rectInNano.contains(mouseRightClickCoordinateInNanometer))
            {
              isRightClickOnOpticalCameraRectangle = true;
              if (_testDev.getPanelGraphicsEngineSetup().getDividedOpticalRegionList().contains(ocr))
              {
                if (_testDev.getPanelGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList().size() > 0)
                {
                  popup = createPopUpMenuForDividedOpticalCameraRectangle(highlightedOpticalCameraRectangles);
                  popup.show(mouseEvent.getComponent(), pixelX, pixelY);
                  break;          
                }
              }
              else if (_testDev.getPanelGraphicsEngineSetup().getSelectedOpticalCameraRectangleList().contains(ocr))
              {
                if (_testDev.getPanelGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList().size() > 1)
                {
                  popup = createPopUpMenuForMultipleOpticalCameraRectangle();
                }
                else
                {
                  popup = createPopUpMenuForSingleOpticalCameraRectangle();
                }
                popup.show(mouseEvent.getComponent(), pixelX, pixelY);
                break;
              }
            }
          }
        }
//        if (getPanelGraphicsEngine().isDragRegionMode() && _isAddRectangleModeOn && isRightClickOnOpticalCameraRectangle == false)
//        if (getPanelGraphicsEngine().isDragRegionMode() && isRightClickOnOpticalCameraRectangle == false)
        if (isRightClickOnOpticalCameraRectangle == false)
        {
          popup = createPopUpMenuToAddRectangle(mouseRightClickCoordinate);
          popup.show(mouseEvent.getComponent(), pixelX, pixelY);
          getPanelGraphicsEngine().setDragOpticalRegionButtonIsToggle(true);
        }
      }
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void handleMouseClickEventForBoardBased(final Object object)
  {
    Assert.expect(object != null);
    
    _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
    
    MouseEvent mouseEvent = (MouseEvent) object;
    final int pixelX = mouseEvent.getX();
    final int pixelY = mouseEvent.getY();
    boolean isControlKeyPressed = getBoardGraphicsEngine().isControlKeyPressMode();

    if (SwingUtilities.isLeftMouseButton(mouseEvent))
    {
      // select / highlight the rectangle if user clicks on it.
      if (_shouldInteractWithGraphics)
      {
        _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
        Point2D mouseLeftClickCoordinateInNanometer = getBoardGraphicsEngine().convertFromPixelToRendererCoordinates(pixelX, pixelY);
        if (_testDev.getBoardGraphicsEngineSetup().isDragOpticalRegionLayerVisible())
        {
          for (OpticalCameraRectangle ocr : _testDev.getBoardGraphicsEngineSetup().getDividedOpticalRegionListForAllBoards())
          {
            if (ocr.getBoard().equals(_currentBoard))
            {
              Rectangle rectInNano = ocr.getRegion().getRectangle2D().getBounds();
              if (rectInNano.contains(mouseLeftClickCoordinateInNanometer))
              {
                if (isControlKeyPressed == false)
                {
                  _testDev.getBoardGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
                  _pspRectangleTable.clearSelection();
                }
                _testDev.getBoardGraphicsEngineSetup().addHighlightedOpticalCameraRectangle(ocr);
                _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegion();
                _testDev.updateGroupSelectButton(false);
                break;
              }
            }
          }
        }
        if (_testDev.getBoardGraphicsEngineSetup().isSelectedOpticalRegionLayerVisible())
        {
          for (OpticalCameraRectangle ocr: _testDev.getBoardGraphicsEngineSetup().getSelectedOpticalCameraRectangleList())
          {
            if (ocr.getBoard().equals(_currentBoard))
            {
              Rectangle rectInNano = ocr.getRegion().getRectangle2D().getBounds();
              if (rectInNano.contains(mouseLeftClickCoordinateInNanometer))
              {
                if (isControlKeyPressed == false)
                {
                  _testDev.getBoardGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
                  _pspRectangleTable.clearSelection();
                }
                _testDev.getBoardGraphicsEngineSetup().addHighlightedOpticalCameraRectangle(ocr);
                int selectedRow = _pspRectangleTableModel.getRowIndexUsingOpticalRectangleName(ocr.getName());
                if (selectedRow > -1)
                {
                  _pspRectangleTable.addRowSelectionInterval(selectedRow, selectedRow);
                }
                int[] selectedRows = _pspRectangleTable.getSelectedRows();
                if (selectedRows.length > 0)
                  _pspRectangleTable.setSelectedRows(selectedRows);
                break;
              }
            }
          }
        }
        _testDev.getBoardGraphicsEngineSetup().handleMultipleHighlightOpticalCameraRectangles(_testDev.getBoardGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList());
      }
    }
    else if (SwingUtilities.isRightMouseButton(mouseEvent) && (getBoardGraphicsEngine().isControlKeyPressMode() == false))
    {
      // handle right click with a popup menu
      if (_shouldInteractWithGraphics)
      {
        JPopupMenu popup = new JPopupMenu(); // this name is never seen by the users
        Point mouseRightClickCoordinate = new Point(pixelX, pixelY);
        Point2D mouseRightClickCoordinateInNanometer = getBoardGraphicsEngine().convertFromPixelToRendererCoordinates(pixelX, pixelY);
        boolean isRightClickOnSelectedOpticalRectangle = false;
        
        java.util.List<OpticalCameraRectangle> highlightedOpticalCameraRectangles = 
                new ArrayList<>(_testDev.getBoardGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList());
        if (_testDev.getBoardGraphicsEngineSetup().isHighlightOpticalRegionLayerVisible())
        {
          for (OpticalCameraRectangle ocr : _testDev.getBoardGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList())
          {
            Rectangle rectInNano = ocr.getRegion().getRectangle2D().getBounds();
            if (rectInNano.contains(mouseRightClickCoordinateInNanometer))
            {
              isRightClickOnSelectedOpticalRectangle = true;
              if (_testDev.getBoardGraphicsEngineSetup().getDividedOpticalRegionListForAllBoards().contains(ocr))
              {
                if (_testDev.getBoardGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList().size() > 0)
                {
                  popup = createPopUpMenuForDividedOpticalCameraRectangle(highlightedOpticalCameraRectangles);
                  popup.show(mouseEvent.getComponent(), pixelX, pixelY);
                  break;
                }
              }
              else if (_testDev.getBoardGraphicsEngineSetup().getSelectedOpticalCameraRectangleList().contains(ocr))
              {
                if (_testDev.getBoardGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList().size() > 1)
                {
                  popup = createPopUpMenuForMultipleOpticalCameraRectangle();
                }
                else
                {
                  popup = createPopUpMenuForSingleOpticalCameraRectangle();
                }
                popup.show(mouseEvent.getComponent(), pixelX, pixelY);
                break;
              }
            }
          }
        }
//        if (getBoardGraphicsEngine().isDragRegionMode() && _isAddRectangleModeOn && isRightClickOnSelectedOpticalRectangle == false)
        if (isRightClickOnSelectedOpticalRectangle == false)
        {
          popup = createPopUpMenuToAddRectangle(mouseRightClickCoordinate);
          popup.show(mouseEvent.getComponent(), pixelX, pixelY);
          getBoardGraphicsEngine().setDragOpticalRegionButtonIsToggle(true);
        }
      }
    }
  }
  
  /**
   * Highlight optical camera rectangles when user drags 
   * (using left mouse-click) across the optical regions.
   * 
   * @author Ying-Huan.Chu
   * @author Jack Hwee
   */
  public void handleDragOpticalRegionEvent(Object object)
  {
    Rectangle dragRegion = getPanelGraphicsEngine().getUnselectedComponentRegion();
    if (dragRegion != null)
    {
      // drag to grey out red rectangles
      if (getPanelGraphicsEngine().isDragRegionMode())
      {
        _testDev.getPanelGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList();
        _testDev.getPanelGraphicsEngineSetup().getDividedOpticalRegionList();
      }
      _testDev.getPanelGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
      if (_isAddRectangleModeOn || _hasPopulated)
      {
        for (OpticalCameraRectangle ocr : _testDev.getPanelGraphicsEngineSetup().getDividedOpticalRegionList())
        {    
          Rectangle rectInPixel = getPanelGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(ocr.getRegion().getRectangle2D().getBounds());
          //Rectangle rectInPixel = ocr.getRegionInPixel().getRectangle2D().getBounds();
          if (dragRegion.contains(rectInPixel))
          {
            _testDev.getPanelGraphicsEngineSetup().addHighlightedOpticalCameraRectangle(ocr);
          }
        }
        _testDev.getPanelGraphicsEngineSetup().handleDragOpticalRegion();  
      }
      else
      {
        for (OpticalCameraRectangle ocr : _testDev.getPanelGraphicsEngineSetup().getSelectedOpticalCameraRectangleList())
        {
          Rectangle rectInPixel = getPanelGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(ocr.getRegion().getRectangle2D().getBounds());
          //Rectangle rectInPixel = ocr.getRegionInPixel().getRectangle2D().getBounds();
          if (dragRegion.contains(rectInPixel))
          {
            _testDev.getPanelGraphicsEngineSetup().addHighlightedOpticalCameraRectangle(ocr);
          }
        }
        selectMultipleRowsInPspRectangleTable(_testDev.getPanelGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList());
      }
      // update CAD graphics.
      _testDev.getPanelGraphicsEngineSetup().handleMultipleHighlightOpticalCameraRectangles(_testDev.getPanelGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList());
    }
  }
  
  /**
   * Highlight optical camera rectangles when user drags 
   * (using left mouse-click) across the optical regions.
   * 
   * @author Ying-Huan.Chu
   * @author Jack Hwee
   */
  public void handleDragOpticalRegionEventForBoardBased(Object object)
  {
    Rectangle dragRegion = getBoardGraphicsEngine().getUnselectedComponentRegion();
    if (dragRegion != null)
    {
      // drag to grey out red rectangles
      if (getBoardGraphicsEngine().isDragRegionMode())
      {
        _testDev.getBoardGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList();
        _testDev.getBoardGraphicsEngineSetup().getDividedOpticalRegionListForAllBoards();
      }
      _testDev.getBoardGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
      if (_isAddRectangleModeOn || _hasPopulated)
      {
        for (OpticalCameraRectangle ocr : _testDev.getBoardGraphicsEngineSetup().getDividedOpticalRegionListForAllBoards())
        {
          Rectangle rectInPixel = getBoardGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(ocr.getRegion().getRectangle2D().getBounds());
          //Rectangle rectInPixel = ocr.getRegionInPixel().getRectangle2D().getBounds();
          if (dragRegion.contains(rectInPixel))
          {
            _testDev.getBoardGraphicsEngineSetup().addHighlightedOpticalCameraRectangle(ocr);
          }
        }
        _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegion();
      }
      else
      {
        _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
        for (OpticalCameraRectangle ocr : _testDev.getBoardGraphicsEngineSetup().getSelectedOpticalCameraRectangleList())
        {
          if (ocr.getBoard().equals(_currentBoard))
          {
            Rectangle rectInPixel = getBoardGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(ocr.getRegion().getRectangle2D().getBounds());
            //Rectangle rectInPixel = ocr.getRegionInPixel().getRectangle2D().getBounds();
            if (dragRegion.contains(rectInPixel))
            {
              _testDev.getBoardGraphicsEngineSetup().addHighlightedOpticalCameraRectangle(ocr);
            }
          }
        }
        selectMultipleRowsInPspRectangleTable(_testDev.getBoardGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList());
      }
      // update CAD graphics.
      _testDev.getBoardGraphicsEngineSetup().handleMultipleHighlightOpticalCameraRectangles(_testDev.getBoardGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList());
    }
  }

  /**
   * @author Jack Hwee
   */
  private JMenuItem createEditPointMenuItem()
  {
    JMenuItem editPointMenuItem = new JMenuItem(StringLocalizer.keyToString(
                                                new LocalizedString("CAD_OPTICAL_RECTANGLE_EDIT_POINT_KEY", new Object[]{null})));

    editPointMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        editRectangle_actionPerformed();
      }
    });
    return editPointMenuItem;
  }

  /**
   * XCR-3600 Assert at surface map tab when trying to select component at graphic UI
   * Toggle or flip the highlighted rectangles state
   *
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   * @author Cheah Lee Herng
   */
  private JMenuItem createEnableDisableRectangleMenuItem()
  {
    JMenuItem toggleRectangleMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("CAD_OPTICAL_RECTANGLE_TURN_OFF_RECTANGLE_KEY",new Object[]{null})));
    int selectedRow = _pspRectangleTable.getSelectedRow();
    
    // XCR-3600 Assert at surface map tab when trying to select component at graphic UI
    if (selectedRow != -1)
    {
      if (_pspRectangleTableModel.hasOpticalRectanglesUsingRowIndex(selectedRow))
      {
        OpticalCameraRectangle selectedOpticalCameraRectangle = _pspRectangleTableModel.getOpticalCameraRectangleUsingRowIndex(selectedRow);
        if (selectedOpticalCameraRectangle.getInspectableBool() == true)
        {
          toggleRectangleMenuItem.setText(StringLocalizer.keyToString("CAD_OPTICAL_RECTANGLE_TURN_OFF_RECTANGLE_KEY"));
        }
        else
        {
          toggleRectangleMenuItem.setText(StringLocalizer.keyToString("CAD_OPTICAL_RECTANGLE_TURN_ON_RECTANGLE_KEY"));
        }

        toggleRectangleMenuItem.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            toggleOpticalCameraRectangles();
          }
        });
      }
    }

    return toggleRectangleMenuItem;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private JMenuItem createAddOpticalCameraRectangleMenuItem(final Point2D mouseRightClickPoint)
  {
    JMenuItem addRectangleMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("PSP_ADD_OPTICAL_RECTANGLE_KEY",
                                                                                                    new Object[]{null})));
    
    addRectangleMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addDividedOpticalCameraRectangle(mouseRightClickPoint);
      }
    });
    return addRectangleMenuItem;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void addDividedOpticalCameraRectangle(Point2D mouseRightClickPoint)
  {
    if (_pspRectangleTable.getRowCount() > 0)
      _pspRectangleTable.removeRowSelectionInterval(0, _pspRectangleTable.getRowCount() - 1);

    _hasPopulated = true;
    _isAddRectangleModeOn = true;
    
    int pixelX = MathUtil.roundDoubleToInt(mouseRightClickPoint.getX());
    int pixelY = MathUtil.roundDoubleToInt(mouseRightClickPoint.getY());

    Point2D pointInNanometer = convertFromPixelToRendererCoordinates(pixelX, pixelY);
    if (_isPanelBasedAlignment)
    {
      //clear all highlighted optical camera rectangle.
      _testDev.getPanelGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
      Rectangle rectInNano = new Rectangle(MathUtil.roundDoubleToInt(pointInNanometer.getX()) - (PspHardwareEngine.getInstance().getActualImageWidthInNanometer() / 2),
                                           MathUtil.roundDoubleToInt(pointInNanometer.getY()) - (PspHardwareEngine.getInstance().getActualImageHeightInNanometer() / 2),
                                           PspHardwareEngine.getInstance().getActualImageWidthInNanometer(), 
                                           PspHardwareEngine.getInstance().getActualImageHeightInNanometer());
      
      // checks if rectangle is within panel boundaries.
      if (_currentBoardType.getPanel().getShapeInNanoMeters().contains(rectInNano))
      {
        OpticalCameraRectangle opticalCameraRectangleToBeAdded = new OpticalCameraRectangle(MathUtil.roundDoubleToInt(rectInNano.getMinX()),
                                                                                            MathUtil.roundDoubleToInt(rectInNano.getMinY()),
                                                                                            MathUtil.roundDoubleToInt(rectInNano.width),
                                                                                            MathUtil.roundDoubleToInt(rectInNano.height));
        if (_surfaceMapping.isOpticalCameraEnable(opticalCameraRectangleToBeAdded) == false)
        {
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
            StringLocalizer.keyToString("PSP_ADD_OPTICAL_CAMERA_RECTANGLE_OUT_OF_CAMERA_INSPECTABLE_REGION_MESSAGE_KEY"),
            StringLocalizer.keyToString("PSP_ADD_OPTICAL_CAMERA_OUT_OF_INSPECTABLE_REGION_TITLE_KEY"),
            JOptionPane.ERROR_MESSAGE);
          return;
        }
        _testDev.getPanelGraphicsEngineSetup().addDividedOpticalCameraRectangle(opticalCameraRectangleToBeAdded);
        _testDev.getPanelGraphicsEngineSetup().handleDragOpticalRegion();
      }
      else
      {
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                StringLocalizer.keyToString("PSP_ADD_OPTICAL_CAMERA_RECTANGLE_OUT_OF_PANEL_BOUNDARIES_MESSAGE_KEY"),
                StringLocalizer.keyToString("PSP_ADD_OPTICAL_CAMERA_RECTANGLE_OUT_OF_BOUNDARIES_TITLE_KEY"),
                JOptionPane.ERROR_MESSAGE);
      }
    }
    else // board-based
    {
      //clear all highlighted optical camera rectangle.
      _testDev.getBoardGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();

      _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
      
      Rectangle rectInNano = new Rectangle(MathUtil.roundDoubleToInt(pointInNanometer.getX()) - (PspHardwareEngine.getInstance().getActualImageWidthInNanometer() / 2),
                                           MathUtil.roundDoubleToInt(pointInNanometer.getY()) - (PspHardwareEngine.getInstance().getActualImageHeightInNanometer() / 2),
                                           PspHardwareEngine.getInstance().getActualImageWidthInNanometer(), 
                                           PspHardwareEngine.getInstance().getActualImageHeightInNanometer());
      
      if (_currentBoard.getShapeRelativeToPanelInNanoMeters().contains(rectInNano))
      {
        OpticalCameraRectangle opticalCameraRectangleToBeAdded = new OpticalCameraRectangle();
        opticalCameraRectangleToBeAdded.setInspectableBool(true);
        opticalCameraRectangleToBeAdded.setZHeightInNanometer(0);
        opticalCameraRectangleToBeAdded.setBoard(_currentBoard);
        opticalCameraRectangleToBeAdded.setRegion(MathUtil.roundDoubleToInt(rectInNano.getMinX()),
                                                  MathUtil.roundDoubleToInt(rectInNano.getMinY()),
                                                  MathUtil.roundDoubleToInt(rectInNano.width),
                                                  MathUtil.roundDoubleToInt(rectInNano.height));
        if (_surfaceMapping.isOpticalCameraEnable(opticalCameraRectangleToBeAdded) == false)
        {
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
            StringLocalizer.keyToString("PSP_ADD_OPTICAL_CAMERA_RECTANGLE_OUT_OF_CAMERA_INSPECTABLE_REGION_MESSAGE_KEY"),
            StringLocalizer.keyToString("PSP_ADD_OPTICAL_CAMERA_OUT_OF_INSPECTABLE_REGION_TITLE_KEY"),
            JOptionPane.ERROR_MESSAGE);
          return;
        }
        _testDev.getBoardGraphicsEngineSetup().addDividedOpticalCameraRectangle(opticalCameraRectangleToBeAdded);
        applySingleDividedOpticalCameraRectangleToSelectedBoards(opticalCameraRectangleToBeAdded);
        _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegion();
      }
      else
      {
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      StringLocalizer.keyToString("PSP_ADD_OPTICAL_CAMERA_RECTANGLE_OUT_OF_PANEL_BOUNDARIES_MESSAGE_KEY"),
                                      StringLocalizer.keyToString("PSP_ADD_OPTICAL_CAMERA_RECTANGLE_OUT_OF_BOUNDARIES_TITLE_KEY"),
                                      JOptionPane.ERROR_MESSAGE);
      }
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void applySingleDividedOpticalCameraRectangleToSelectedBoards(OpticalCameraRectangle opticalCameraRectangle)
  {
    _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
    java.util.List<OpticalCameraRectangle> dividedOpticalCameraRectangleList = new ArrayList<>();
    for (Board board : getBoardsToApplyOpticalCameraRectangles())
    {
      if (board.getName().equals(_currentBoard.getName()) == false)
      {
        OpticalCameraRectangle ocr = _testDev.getBoardGraphicsEngineSetup().
                                     getOpticalCameraRectangleRelativeToBoard(board, opticalCameraRectangle);
        
        if (_surfaceMapping.isOpticalCameraEnable(ocr) == false)
        {
          continue;
        }
        
        dividedOpticalCameraRectangleList.add(ocr);
      }
    }
    for (OpticalCameraRectangle ocr : dividedOpticalCameraRectangleList)
    {
      _testDev.getBoardGraphicsEngineSetup().addDividedOpticalCameraRectangle(ocr);
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void applyDividedOpticalCameraRectanglesToSelectedBoards()
  {
    java.util.List<OpticalCameraRectangle> opticalCameraRectangleList = new ArrayList<>(_testDev.getBoardGraphicsEngineSetup().getDividedOpticalRegionListForAllBoards());
    for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangleList)
    {
      applySingleDividedOpticalCameraRectangleToSelectedBoards(opticalCameraRectangle);
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void applyDividedOpticalCameraRectanglesToSelectedBoards(java.util.List<OpticalCameraRectangle> opticalCameraRectangleList)
  {
    for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangleList)
    {
      applySingleDividedOpticalCameraRectangleToSelectedBoards(opticalCameraRectangle);
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private JPopupMenu createPopUpMenuToAddRectangle(Point2D mouseRightClickPoint)
  {
    JPopupMenu popup = new JPopupMenu();
    JMenuItem addNewRectangleMenuItem = createAddOpticalCameraRectangleMenuItem(mouseRightClickPoint);

    popup.add(addNewRectangleMenuItem);

    return popup;
  }
  
  /**
   * 
   * @author Ying-Huan.Chu
   */
  private JMenuItem createDeleteDividedOpticalCameraRectangleMenuItem(final java.util.List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    Assert.expect(opticalCameraRectangles != null);
    JMenuItem deleteOpticalRectangleMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("PSP_DELETE_OPTICAL_RECTANGLE_KEY",
                                                                                                    new Object[]{null})));

    deleteOpticalRectangleMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_isPanelBasedAlignment)
        {
          for (OpticalCameraRectangle ocr : opticalCameraRectangles)         
          {
            if (_testDev.getPanelGraphicsEngineSetup().getDividedOpticalRegionList().contains(ocr))
              _testDev.getPanelGraphicsEngineSetup().removeDividedOpticalCameraRectangle(ocr);
          }
          _testDev.getPanelGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
          _testDev.getPanelGraphicsEngineSetup().handleDragOpticalRegion();
        }
        else
        {
          for (OpticalCameraRectangle ocr : opticalCameraRectangles)         
          {
            if (_testDev.getBoardGraphicsEngineSetup().getDividedOpticalRegionList(ocr.getBoard()).contains(ocr))
              _testDev.getBoardGraphicsEngineSetup().removeDividedOpticalCameraRectangle(ocr);
            // delete the same OpticalCameraRectangle on other boards as well.
            for (Board board : getBoardsToApplyOpticalCameraRectangles())
            {
              if (board.equals(ocr.getBoard()) == false)
              {
                OpticalCameraRectangle opticalCameraRectangle = _testDev.getBoardGraphicsEngineSetup().
                                                                getOpticalCameraRectangleRelativeToBoard(board, ocr);
                java.util.List<OpticalCameraRectangle> dividedOpticalRegionList = _testDev.getBoardGraphicsEngineSetup().getDividedOpticalRegionList(board);
                for (OpticalCameraRectangle dividedOcr : dividedOpticalRegionList)
                {
                  if (dividedOcr.getName().equalsIgnoreCase(opticalCameraRectangle.getName()))
                  {
                    _testDev.getBoardGraphicsEngineSetup().removeDividedOpticalCameraRectangle(dividedOcr);
                  }
                }
              }
            }
          }
          _testDev.getBoardGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
          _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegion();
        }
      }
    });
    return deleteOpticalRectangleMenuItem;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private JMenuItem createDeleteComponentMenuItem()
  {
    JMenuItem deleteComponentMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("PSP_DELETE_COMPONENT_BUTTON_KEY",
                                                                                                      new Object[]{null})));
    deleteComponentMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteComponentButton_actionPerformed();
      }
    });
    return deleteComponentMenuItem;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private JPopupMenu createDeleteComponentPopUpMenu()
  {
    JPopupMenu popup = new JPopupMenu();
    JMenuItem deleteComponentMenuItem = createDeleteComponentMenuItem();
    popup.add(deleteComponentMenuItem);
    return popup;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private JPopupMenu createPopUpMenuForDividedOpticalCameraRectangle(java.util.List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    Assert.expect(opticalCameraRectangles != null);
    JPopupMenu popup = new JPopupMenu();
    JMenuItem deleteOpticalRectangleMenuItem = createDeleteDividedOpticalCameraRectangleMenuItem(opticalCameraRectangles);
    popup.add(deleteOpticalRectangleMenuItem);
      
    return popup;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private JMenuItem createDeleteOpticalCameraRectangleMenuItem()
  {
    JMenuItem deleteAllRectangleMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("PSP_DELETE_OPTICAL_RECTANGLE_KEY",
                                                                                                    new Object[]{null})));

    deleteAllRectangleMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteOpticalCameraRectangles_actionPerformed();
      }
    });
    return deleteAllRectangleMenuItem;
  }

  /**
   * @return Pop-up Menu for single highlighted rectangle
   */
  private JPopupMenu createPopUpMenuForSingleOpticalCameraRectangle()
  {
    JPopupMenu popup = new JPopupMenu();

    JMenuItem editPointMenuItem = createEditPointMenuItem();
    JMenuItem disableRectangleMenuItem = createEnableDisableRectangleMenuItem();
    JMenuItem deleteSelectedRectangle = createDeleteOpticalCameraRectangleMenuItem();

    popup.add(editPointMenuItem);
    popup.add(disableRectangleMenuItem);
    popup.add(deleteSelectedRectangle);
    
    return popup;
  }

  /**
   * @return Pop-up Menu for multiple highlighted rectangle
   */
  private JPopupMenu createPopUpMenuForMultipleOpticalCameraRectangle()
  {
    JPopupMenu popup = new JPopupMenu();
    
    JMenuItem enableAllMenuItem = createEnableAllRectangleMenuItem();
    JMenuItem disableAllMenuItem = createDisableAllRectangleMenuItem();
    JMenuItem deleteAllMenuItem = createDeleteOpticalCameraRectangleMenuItem();

    popup.add(enableAllMenuItem);
    popup.add(disableAllMenuItem);
    popup.add(deleteAllMenuItem);
    
    return popup;
  }
  
  /**
   * Enable all the highlighted rectangles.
   * @author Jack Hwee
   */
  private JMenuItem createEnableAllRectangleMenuItem()
  {
    JMenuItem enableAllMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("CAD_OPTICAL_RECTANGLE_TURN_ON_ALL_RECTANGLE_KEY",
                                                                                                new Object[]{null})));
    enableAllMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableOpticalCameraRectangles(true);
      }
    });
    return enableAllMenuItem;
  }
  
  /**
   * Delete an optical region (all optical camera rectangles).
   * @author Jack Hwee
   */
  private void deleteRegionButton_actionPerformed()
  {
    boolean isMultiBoardContainsOpticalCameraRectangle = false;
    
    if (_isPanelBasedAlignment == false)
    {
      for (Board board: _project.getPanel().getBoards())
      {
        if (board.hasBoardSurfaceMapSettings())
        {
          if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
          {
            _opticalRegion = board.getBoardSurfaceMapSettings().getOpticalRegion();
            if (_opticalRegion.getOpticalCameraRectangles(board).size() > 0)
            {
              isMultiBoardContainsOpticalCameraRectangle = true;
              break;
            }
          }
        }
      }
    }
    if (_opticalRegion != null && (_opticalRegion.getAllOpticalCameraRectangles().size() > 0 || isMultiBoardContainsOpticalCameraRectangle))
    {
      int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
              StringLocalizer.keyToString("PSP_DELETE_REGION_WILL_RESET_POINTS_KEY"),
              StringLocalizer.keyToString("PSP_DELETE_REGION_BUTTON_KEY"),
              JOptionPane.YES_NO_OPTION,
              JOptionPane.WARNING_MESSAGE);
      if (answer == JOptionPane.YES_OPTION)
      {
        _pspRectangleTableModel.clear();
        if (_isPanelBasedAlignment)
        {
          _project.getPanel().getPanelMeshSettings().clear();
          deletePanelOpticalRegion();
        }
        else
        {
          _commandManager.trackState(getCurrentUndoState());
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PSP_DELETE_OPTICAL_REGION_KEY"));
          for (Board board : _boardsToApplyOpticalCameraRectangles)
          {
            board.getBoardMeshSettings().clear();
            deleteBoardOpticalRegion(board);
          }
          _commandManager.endCommandBlock();
        }
      }
    }
    updateMeshTriangleOnCAD();
    invalidateChecksum();
  }
   
  /**
   * @author Cheah Lee Herng
   */
  private void setupDialogProperty()
  {
    if (PspEngine.isVersion1())
    {
      OPTICAL_CAMERA_MAX_WIDTH_IN_PIXELS = 752 / OPTICAL_CAMERA_VIEW_FACTOR_1;
      OPTICAL_CAMERA_MAX_HEIGHT_IN_PIXELS = 480 / OPTICAL_CAMERA_VIEW_FACTOR_1;
    }
    else
    {
      OPTICAL_CAMERA_MAX_WIDTH_IN_PIXELS = getRectangleWidthRelativeToPanelInPixel();
      OPTICAL_CAMERA_MAX_HEIGHT_IN_PIXELS = getRectangleHeightRelativeToPanelInPixel();
    }
  }
  
  /**
   * Return true if there's at least 1 enabled OpticalCameraRectangle to be used in Surface Map.
   * @author Ying-Huan.Chu
   */
  private boolean checksIfAtLeastOneEnabledOpticalCameraRectangleExists()
  {
    if (_isPanelBasedAlignment)
    {
      for (OpticalCameraRectangle ocr : _testDev.getPanelGraphicsEngineSetup().getDividedOpticalRegionList())
      {
        if (ocr.getInspectableBool() == true)
        {
          return true;
        }
      }
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringLocalizer.keyToString("PSP_PANEL_BASED_NO_ENABLED_OPTICAL_CAMERA_RECTANGLE_KEY"),
                                    StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      return false;
    }
    else // board-based alignment
    {
      // Disable this checking. So that, live view can run when some of the board does not have red rectangle.
      // This is to handle only 1 camera is enable, some of rectangle the board is not covered.
//      if (getBoardsToApplyOpticalCameraRectangles().size() != _testDev.getBoardGraphicsEngineSetup().getBoardToOpticalRegionMap().size())
//      {
//        for (Board board : getBoardsToApplyOpticalCameraRectangles())
//        {
//          if (_testDev.getBoardGraphicsEngineSetup().getBoardToOpticalRegionMap().containsKey(board) == false)
//          {
//            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
//                StringLocalizer.keyToString(new LocalizedString("PSP_BOARD_BASED_NO_OPTICAL_CAMERA_RECTANGLE_KEY", new Object[]{board.getName()})),
//                StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
//                JOptionPane.ERROR_MESSAGE);
//            return false;
//          }
//        }
//      }
      for (Map.Entry<Board, java.util.List<OpticalCameraRectangle>> entry : _testDev.getBoardGraphicsEngineSetup().getBoardToOpticalRegionMap().entrySet())
      {
        Board board = entry.getKey();
        java.util.List<OpticalCameraRectangle> opticalCameraRectangleList = entry.getValue();
        if (opticalCameraRectangleList.isEmpty())
        {
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
              StringLocalizer.keyToString(new LocalizedString("PSP_BOARD_BASED_NO_OPTICAL_CAMERA_RECTANGLE_KEY", new Object[]{board.getName()})),
              StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
              JOptionPane.ERROR_MESSAGE);
          return false;
        }
        boolean allRectanglesAreDisabled = true;
        for (OpticalCameraRectangle ocr : opticalCameraRectangleList)
        {
          if (ocr.getInspectableBool() == true)
          {
            allRectanglesAreDisabled = false;
          }
        }
        if (allRectanglesAreDisabled == true)
        {
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
              StringLocalizer.keyToString(new LocalizedString("PSP_BOARD_BASED_NO_ENABLED_OPTICAL_CAMERA_RECTANGLE_KEY", new Object[]{board.getName()})),
              StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
              JOptionPane.ERROR_MESSAGE);
          return false;
        }
      }
      return true;
    }
  }
  
  /**
   * @author Jack Hwee
   * @edited By Kee Chin Seong - the alignment is checked the alignment is COMPLETE.
   */
  private void runAutoLiveViewButton_actionPerformed()
  {
    try
    {
      _isLiveViewMode = true;
      if (_hasPopulated && _testDev.getPanelGraphicsEngineSetup().getDividedOpticalRegionList().size() > 0)
      {
        // checks if the component list is empty.        
        if (getAllComponents().isEmpty())
        {
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                  StringLocalizer.keyToString("PSP_COMPONENT_CANNOT_EMPTY_KEY"),
                  StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                  JOptionPane.ERROR_MESSAGE);
          return;
        }
        if (_isAddRectangleModeOn == false)
        {
          if (promptToDeletePreviousRectangles() == false)
            return;
        }
        if (checksIfAtLeastOneEnabledOpticalCameraRectangleExists() == false)
        {
          return;
        }
        if (checkProjectHasBeenModified())
        {
          if (isSurfaceMapping() == false)
          {
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                    StringLocalizer.keyToString("PSP_SELECT_REGION_KEY"),
                    StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                    JOptionPane.ERROR_MESSAGE);
            return;
          }

          // If we detect components with GSM turn on / PSH turn on, we need to prompt user that we will reset
          // GSM to AutoFocus and PSH to false in order for PSP to take effect
          //if (checkComponentsFilter(getAllComponents()) == false)
          //  return;
          
          // XCR-2726 Cheah Lee Herng - Continue run surface map although click on "X" button
          if (checkComponentsFilter(getAllComponents()) == false)
            return;

          if (_mainUI.isPanelInLegalState() == false)
          {
            return;
          }

          if (_project.isTestProgramValid() == false)
          {
            _mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_SURFACE_MAPPING_GEN_NEEDS_TEST_PROGRAM_KEY"),
                    StringLocalizer.keyToString("IMTB_GENERATE_SURFACE_MAPPING_KEY"),
                    _project);
          }

          if (_mainUI.beforeGenerateSurfaceMapping(this))
          {
            if (_project.getPanel().getPanelSettings().isManualAlignmentCompleted())
            {
              updateOpticalCameraSetting(_project.getPanel().isLargerFOV());
              OpticalRegion opticalRegion = null;
              final boolean simulationMode = XrayTester.getInstance().isSimulationModeOn();
               
              _project.getPanel().setIsAutomaticallyIncludeAllComponents(_autoIncludeWholePanelComponentRadioButton.isSelected());
              if (checkIfOpticalCalibrationDataExists() == false)
              {
                handleNoOpticalCalibrationDataError();
                return;
              }
              
              boolean updateSurfaceMapData = false;
              
              Rectangle2D panelBoundingRectangle = _project.getPanel().getShapeInNanoMeters().getBounds2D();
              // if there is existing optical region we should use it instead of creating a new one.
              if (_project.getPanel().getPanelOpticalRegions().isEmpty() == false)
              {
                if (_project.getPanel().getPanelSurfaceMapSettings().hasOpticalRegion())
                  opticalRegion = _project.getPanel().getPanelSurfaceMapSettings().getOpticalRegion();
              }
              else
              {
                opticalRegion = new OpticalRegion();
                opticalRegion.setRegion((int)panelBoundingRectangle.getMinX(), 
                                        (int)panelBoundingRectangle.getMinY(), 
                                        (int)panelBoundingRectangle.getWidth(), 
                                        (int)panelBoundingRectangle.getHeight());
              }
              for (com.axi.v810.business.panelDesc.Component component : getAllComponents())
              {
                if (isComponentUsableInSurfaceMap(component))
                {
                  if (updateSurfaceMapData)
                  {
                    component.getComponentType().setToUsePspResult(true);
                  }
                  opticalRegion.addComponent(component);
                }
              }
              
              java.util.List<OpticalCameraRectangle> opticalCameraRectangleSortedList = new ArrayList<OpticalCameraRectangle>();
              opticalCameraRectangleSortedList = _surfaceMapping.sortOpticalCameraRectangleBasedOnTestProgram(_testDev.getPanelGraphicsEngineSetup().getDividedOpticalRegionList());
              _testDev.getPanelGraphicsEngineSetup().setDividedOpticalRegionList(opticalCameraRectangleSortedList);
              boolean hasPoint = false;
              for (int i = 0; i < opticalCameraRectangleSortedList.size(); i++)
              {
                OpticalCameraRectangle opticalCameraRectangle = new OpticalCameraRectangle();
                java.util.List<OpticalCameraRectangle> ocrUpdatedList = new ArrayList<OpticalCameraRectangle>();
                final OpticalCameraRectangle ocr = opticalCameraRectangleSortedList.get(i);
                final Rectangle rectInNano = ocr.getRegion().getRectangle2D().getBounds();
                
                if (i == 0)
                {
                  _isPreviousButtonVisible = false;
                }
                else
                {
                  _isPreviousButtonVisible = true;
                }

                // Get back the points for previous rectangle.
                if (_project.getPanel().getPanelSurfaceMapSettings().hasOpticalRegion())
                {
                  ocrUpdatedList = _project.getPanel().getPanelSurfaceMapSettings().getOpticalRegion().getAllOpticalCameraRectangles();

                  for (int j = 0; j < ocrUpdatedList.size(); j++)
                  {
                    if (opticalCameraRectangleSortedList.get(i).getName().equals(ocrUpdatedList.get(j).getName()))
                    {
                      opticalCameraRectangle = ocrUpdatedList.get(j);
                      hasPoint = true;
                      break;
                    }
                  }
                }

                // for previous rectangle, it will enter edit mode to edit the points.
                if (hasPoint)
                {
                  _testDev.getPanelGraphicsEngineSetup().handleDragOpticalRegion();
                  _opticalRegion = _project.getPanel().getPanelSurfaceMapSettings().getOpticalRegion();
                  _testDev.getPanelGraphicsEngineSetup().handleHighlightOpticalRegionWhenLiveView(opticalCameraRectangle);
                  editSurfaceMapPoints(opticalCameraRectangle);
                  hasPoint = false;

                  if (_pspLiveVideoDialog.isCancelSurfaceMapping())
                  {
                    _testDev.getPanelGraphicsEngineSetup().setHighlightOpticalRegionLayersVisible(false);
                    break;
                  }
                }
                else
                {
                  if (i == opticalCameraRectangleSortedList.size() - 1)
                  {
                    updateSurfaceMapData = true;
                  }

                  Board selectedBoard = getSelectedBoard(rectInNano);

                  // Jack Hwee - Make sure image is "latest" for display
                  deleteTempOpticalImage();
                  final Board finalBoard = selectedBoard;
                  final BusyCancelDialog busyDialog = new BusyCancelDialog(
                          _mainUI,
                          StringLocalizer.keyToString("PSP_UPDATE_DATA_KEY"),
                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                          true);
                  busyDialog.pack();
                  SwingUtils.centerOnComponent(busyDialog, _mainUI);
                  SwingWorkerThread.getInstance().invokeLater(new Runnable()
                  {
                    public void run()
                    {
                      try
                      {
                        doRegionSurfaceMapping(ocr, finalBoard);

                        if (_isSurfaceMappingSuccess && simulationMode == false && _isAlignmentSuccess)
                          waitForHeightMap();
                      }
                      finally
                      {
                        busyDialog.dispose();
                      }
                    }
                  });
                  busyDialog.setVisible(true);

                  if (_isSurfaceMappingSuccess && simulationMode == false && _isHeightMapAvailable == false)
                  {
                    handleUnableToRetrieveHeightMapError();
                    return;
                  }

                  if (_isSurfaceMappingSuccess == false || _isAlignmentSuccess == false)
                  {
                    return;
                  }

                  // Highlight current processed OpticalCameraRectangle
                  _testDev.getPanelGraphicsEngineSetup().handleHighlightOpticalRegionWhenLiveView(ocr);

                  // Start display Surface Map Dialog
                  _pspLiveVideoDialog = new SurfaceMapLiveVideoDialog(_mainUI, 
                                                                      StringLocalizer.keyToString("PSP_SURFACE_MAP_LIVE_VIEW_DIALOG_TITLE_KEY"),
                                                                      finalBoard, 
                                                                      _project.getPanel().getBoards(), 
                                                                      opticalRegion, 
                                                                      opticalCameraRectangleSortedList.get(i), 
                                                                      simulationMode, 
                                                                      updateSurfaceMapData,
                                                                      _enumSelectedUnits,
                                                                      _isLiveViewMode,
                                                                      _isPreviousButtonVisible);
                  _pspLiveVideoDialog.setLocationRelativeTo(this._opticalRegionInformationSplitPane);
                  _pspLiveVideoDialog.setVisible(true);

                  if (_pspLiveVideoDialog.isCancelSurfaceMapping())
                  {
                    _testDev.getPanelGraphicsEngineSetup().setHighlightOpticalRegionLayersVisible(false);
                    break;
                  }
                }
                
                if (_pspLiveVideoDialog.hasException())
                {
                  _mainUI.handleXrayTesterException(_pspLiveVideoDialog.getException());
                  break;
                }
              
                if (_pspLiveVideoDialog.isPreviousRectangle())
                {
                  updateSurfaceMapData = false;
                  i = i - 2;
                  _pspLiveVideoDialog.setPreviousRectangle(false);
                  if (i <= -1)
                  {
                    i = -1;
                  }
                }
              }

              // refresh the Optical Rectangle table data.
              refreshTablesData();
              _testDev.getPanelGraphicsEngineSetup().clearDividedOpticalRegionList();
              updateMeshTriangleOnCAD();
              if (_pspRectangleTable.getRowCount() > 0)
              {
                _pspRectangleTable.setSelectedRow(0);
                _testDev.getPanelGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
                _testDev.getPanelGraphicsEngineSetup().handleHighlightOpticalCameraRectangle(_pspRectangleTableModel.getOpticalCameraRectangleUsingRowIndex(0));         
              }
            }
            else
            {
              // they have not done manual alignment. so tell them they cannot proceed with live view.
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                            StringLocalizer.keyToString("PSP_RUN_LIVE_VIEW_ERROR_RUN_ALIGNMENT_BEFORE_LIVE_VIEW_MESSAGE_KEY"),
                                            StringLocalizer.keyToString("PSP_RUN_LIVE_VIEW_ERROR_MESSAGE_TITLE_KEY"),
                                            JOptionPane.ERROR_MESSAGE);
            }
          }
        }
      }
      else
      {
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      StringLocalizer.keyToString("PSP_RUN_LIVE_VIEW_ERROR_POPULATE_BEFORE_LIVE_VIEW_MESSAGE_KEY"),
                                      StringLocalizer.keyToString("PSP_RUN_LIVE_VIEW_ERROR_MESSAGE_TITLE_KEY"),
                                      JOptionPane.ERROR_MESSAGE);
      }
      
      //update optical rectangle used by each reconstruction region
      allocateOpticalCameraRectangleToComponents();
    }
    catch (XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
    }
    finally
    {
      _commandManager.endCommandBlockIfNecessary();
      _isLiveViewMode = false;
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   * @edited By Kee Chin Seong - the alignment is checked the alignment is COMPLETE.
   */
  private void runAutoLiveViewButtonForBoardBased_actionPerformed()
  {
    try
    {
      _isLiveViewMode = true;
      if (_hasPopulated &&  _testDev.getBoardGraphicsEngineSetup().getDividedOpticalRegionListForAllBoards().size() > 0)
      {
        // put board and components into the _boardToComponentsMap.
        Assert.expect(getAllComponents() != null);
        
        if (_isAddRectangleModeOn == false)
        {
          if (promptToDeletePreviousRectangles() == false)
            return;
        }
        for (Board board : _project.getPanel().getBoards())
        {
          if (_boardsToApplyOpticalCameraRectangles.contains(board) == false)
          {
            _testDev.getBoardGraphicsEngineSetup().clearDividedOpticalRegionList(board);
          }
        }
        for (Board board : _boardsToApplyOpticalCameraRectangles)
        {
          if (getBoardComponentList(board).isEmpty())
          {
            java.util.List<com.axi.v810.business.panelDesc.Component> componentList = getBoardComponentList(board);
            if (componentList.isEmpty())
            {
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                    StringLocalizer.keyToString(new LocalizedString("PSP_COMPONENT_FOR_BOARD_CANNOT_EMPTY_KEY", new Object[]{board.getName()})),
                    StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                    JOptionPane.ERROR_MESSAGE);
              return;
            }
          }
        }
        
        if (checksIfAtLeastOneEnabledOpticalCameraRectangleExists() == false)
        {
          return;
        }
        if (checkProjectHasBeenModified())
        {
          if (isSurfaceMapping() == false)
          {
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                    StringLocalizer.keyToString("PSP_SELECT_REGION_KEY"),
                    StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                    JOptionPane.ERROR_MESSAGE);
            return;
          }

          // If we detect components with GSM turn on / PSH turn on, we need to prompt user that we will reset
          // GSM to AutoFocus and PSH to false in order for PSP to take effect
          //if (checkComponentsFilter(getAllComponents()) == false)
          //  return;
          
          // XCR-2726 Cheah Lee Herng - Continue run surface map although click on "X" button
          if (checkComponentsFilter(getAllComponents()) == false)
            return;

          // Disable retestFailingPins for components selected to use PSP
          //disableRetestForRegions(_component);

          if (_project.isTestProgramValid() == false)
          {
            _mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_SURFACE_MAPPING_GEN_NEEDS_TEST_PROGRAM_KEY"),
                    StringLocalizer.keyToString("IMTB_GENERATE_SURFACE_MAPPING_KEY"),
                    _project);
          }

          if (_mainUI.isPanelInLegalState() == false)
          {
            return;
          }

          if (_mainUI.beforeGenerateSurfaceMapping(this))
          {
            if (_project.getPanel().getPanelSettings().isManualAlignmentCompleted())
            {
              updateOpticalCameraSetting(_project.getPanel().isLargerFOV());
              OpticalRegion opticalRegion = null;
              
              final boolean simulationMode = XrayTester.getInstance().isSimulationModeOn();
              boolean updateSurfaceMapData = false;
              int currentOpticalRectangleIndex = 0;
              
              _project.getPanel().setIsAutomaticallyIncludeAllComponents(_autoIncludeWholePanelComponentRadioButton.isSelected());
              
              if (checkIfOpticalCalibrationDataExists() == false)
              {
                handleNoOpticalCalibrationDataError();
                return;
              }
              
              java.util.List<Board> boardList = new ArrayList <Board>();
              Map<Board, java.util.List<OpticalCameraRectangle>> boardToOpticalRegionMap = _testDev.getBoardGraphicsEngineSetup().getBoardToOpticalRegionMap();
              for (Map.Entry<Board, java.util.List<OpticalCameraRectangle>> entry : _testDev.getBoardGraphicsEngineSetup().getBoardToOpticalRegionMap().entrySet())
              {
                boardList.add(entry.getKey());
              }
              
              for (int currentBoardIndex = 0; currentBoardIndex < boardList.size(); currentBoardIndex++)
              {
                final Board finalBoard = boardList.get(currentBoardIndex);
                java.util.List<OpticalCameraRectangle> opticalCameraRectangleList = _surfaceMapping.sortOpticalCameraRectangleBasedOnTestProgram(boardToOpticalRegionMap.get(finalBoard));
                _testDev.getBoardGraphicsEngineSetup().setDividedOpticalRegionList(opticalCameraRectangleList);
                updateSurfaceMapData = false;
                boolean goToPreviousBoard = false;
                changeGraphicsToBoard(finalBoard);
                
                if (finalBoard.getBoardSurfaceMapSettings().getOpticalRegions().isEmpty() == false)
                {
                  if (finalBoard.getBoardSurfaceMapSettings().hasOpticalRegion())
                    opticalRegion = finalBoard.getBoardSurfaceMapSettings().getOpticalRegion();
                }
                else
                {
                  opticalRegion = new OpticalRegion();
                  opticalRegion.setBoard(finalBoard);
                  Rectangle2D boardBoundingRectangle = finalBoard.getShapeRelativeToPanelInNanoMeters().getBounds2D();                  
                  opticalRegion.setRegion((int)boardBoundingRectangle.getMinX(), 
                                          (int)boardBoundingRectangle.getMinY(), 
                                          (int)boardBoundingRectangle.getWidth(), 
                                          (int)boardBoundingRectangle.getHeight());
                }

                for (com.axi.v810.business.panelDesc.Component component : getBoardComponentList(finalBoard))
                {
                  if (isComponentUsableInSurfaceMap(component))
                  {
                    if (updateSurfaceMapData)
                    {
                      component.getComponentType().setToUsePspResult(true);
                    }
                    opticalRegion.addComponent(finalBoard, component);
                  }
                }
                
                boolean hasPoint = false;
                for (int i = currentOpticalRectangleIndex; currentOpticalRectangleIndex < opticalCameraRectangleList.size(); currentOpticalRectangleIndex++)
              	{
                  OpticalCameraRectangle opticalCameraRectangle = new OpticalCameraRectangle();
                  java.util.List<OpticalCameraRectangle> ocrUpdatedList = new ArrayList<OpticalCameraRectangle>();
                  final OpticalCameraRectangle ocr = opticalCameraRectangleList.get(currentOpticalRectangleIndex);

                  if (currentBoardIndex == 0 && currentOpticalRectangleIndex == 0)
                  {
                    _isPreviousButtonVisible = false;
                  }
                  else
                  {
                    _isPreviousButtonVisible = true;
                  }

                  // Get back the points for previous rectangle
                  if (finalBoard.getBoardSurfaceMapSettings().hasOpticalRegion())
                  {
                    ocrUpdatedList = finalBoard.getBoardSurfaceMapSettings().getOpticalRegion().getOpticalCameraRectangles(finalBoard);

                    for (int j = 0; j < ocrUpdatedList.size(); j++)
                    {
                      if (opticalCameraRectangleList.get(currentOpticalRectangleIndex).getName().equals(ocrUpdatedList.get(j).getName()))
                      {
                        opticalCameraRectangle = ocrUpdatedList.get(j);
                        hasPoint = true;
                        break;
                      }
                    }
                  }
                  
                  // for previous rectangle, it will enter the edit mode.
                  if (hasPoint)
                  {
                    _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegion();
                    _opticalRegion = finalBoard.getBoardSurfaceMapSettings().getOpticalRegion();
                    _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegion();
                    _testDev.getBoardGraphicsEngineSetup().handleHighlightOpticalRegionWhenLiveView(opticalCameraRectangle);
                    editSurfaceMapPoints(opticalCameraRectangle);
                    hasPoint = false;

                    if (_pspLiveVideoDialog.isCancelSurfaceMapping())
                    {
                      _testDev.getPanelGraphicsEngineSetup().setHighlightOpticalRegionLayersVisible(false);
                      break;
                    }
                  }
                  else
                  {
                    if (currentOpticalRectangleIndex == opticalCameraRectangleList.size() - 1 && currentBoardIndex == boardList.size() - 1)
                    {
                      updateSurfaceMapData = true;
                    }

                    Assert.expect(finalBoard != null);

                    // Jack Hwee - Make sure image is "latest" for display
                    deleteTempOpticalImage();
                    final BusyCancelDialog busyDialog = new BusyCancelDialog(
                            _mainUI,
                            StringLocalizer.keyToString("PSP_UPDATE_DATA_KEY"),
                            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                            true);
                    busyDialog.pack();
                    SwingUtils.centerOnComponent(busyDialog, _mainUI);
                    SwingWorkerThread.getInstance().invokeLater(new Runnable()
                    {
                      public void run()
                      {
                        try
                        {
                          doRegionSurfaceMapping(ocr, finalBoard);

                          if (_isSurfaceMappingSuccess && simulationMode == false && _isAlignmentSuccess)
                            waitForHeightMap();
                        }
                        finally
                        {
                          busyDialog.dispose();
                        }
                      }
                    });
                    busyDialog.setVisible(true);

                    if (_isSurfaceMappingSuccess && simulationMode == false && _isHeightMapAvailable == false)
                    {
                      handleUnableToRetrieveHeightMapError();
                      return;
                    }

                    if (_isSurfaceMappingSuccess == false || _isAlignmentSuccess == false)
                    {
                      return;
                    }

                    Assert.expect(opticalRegion != null);

                    _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegion();
                    _testDev.getBoardGraphicsEngineSetup().handleHighlightOpticalRegionWhenLiveView(ocr);

                    _pspLiveVideoDialog = new SurfaceMapLiveVideoDialog(_mainUI, 
                                                                        StringLocalizer.keyToString("PSP_SURFACE_MAP_LIVE_VIEW_DIALOG_TITLE_KEY"),
                                                                        finalBoard, 
                                                                        getBoardsToApplyOpticalCameraRectangles(), 
                                                                        opticalRegion, 
                                                                        opticalCameraRectangleList.get(currentOpticalRectangleIndex), 
                                                                        simulationMode, 
                                                                        updateSurfaceMapData,
                                                                        _enumSelectedUnits,
                                                                        _isLiveViewMode,
                                                                        _isPreviousButtonVisible);
                    _pspLiveVideoDialog.setLocationRelativeTo(this._opticalRegionInformationSplitPane);
                    _pspLiveVideoDialog.setVisible(true);

                    if (_pspLiveVideoDialog.isCancelSurfaceMapping()) // to cancel Surface Mapping from current board
                    {
                      _testDev.getBoardGraphicsEngineSetup().setHighlightOpticalRegionLayersVisible(false);
                      break;
                    }
                  }
                  
                  if (_pspLiveVideoDialog.hasException())
                  {
                    break;
                  }

                  if (_pspLiveVideoDialog.isPreviousRectangle())
                  {
                    // minus 2 because later will automatically plus one when loop
                    currentOpticalRectangleIndex = currentOpticalRectangleIndex - 2;
                    // if you are at the first rectangle of the board and you click previous button again, it will go into this condition.
                    if (currentOpticalRectangleIndex < -1)
                    {
                      // if this is the first board, it will remain the first rectangle
                      if (currentBoardIndex == 0)
                      {
                        currentOpticalRectangleIndex = -1;
                      }
                      // if this not the first board, it will goto previous board's last rectangle.
                      else
                      {
                        goToPreviousBoard = true;
                        // determine total rectangle that previous board have and set currentOpticalRectangleIndex to last rectangle.
                        currentOpticalRectangleIndex = (boardToOpticalRegionMap.get(boardList.get(currentBoardIndex-1)).size())-1;
                        currentBoardIndex = currentBoardIndex - 2;
                        break;
                      }
                    }
                  }
                }
                
                //Kok Chun, Tan - 21/4/2015 - XCR-2648
                if (_pspLiveVideoDialog.hasException())
                {
                  _mainUI.handleXrayTesterException(_pspLiveVideoDialog.getException());
                  break;
                }
                
                // no need reset the currentOpticalRectangleIndex if goto previous board.
                if (goToPreviousBoard == false)
                {
                  currentOpticalRectangleIndex = 0;
                }
                
                if (_pspLiveVideoDialog.isCancelSurfaceMapping()) // to cancel Surface Mapping from all boards
                {
                  _testDev.getBoardGraphicsEngineSetup().setHighlightOpticalRegionLayersVisible(false);
                  // update the list of boards to apply optical camera rectangles when user cancels Surface Mapping.
                  java.util.List<Board> updatedListOfBoards = new ArrayList<>();
                  for (Board board : getBoardsToApplyOpticalCameraRectangles())
                  {
                    if (board.hasBoardSurfaceMapSettings())
                    {
                      if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
                      {
                        for (OpticalRegion boardOpticalRegion : board.getBoardSurfaceMapSettings().getOpticalRegions())
                        {
                          if (boardOpticalRegion.hasOpticalCameraRectangles(board))
                          {
                            updatedListOfBoards.add(board);
                          }
                        }
                      }
                    }
                  }
                  if (updatedListOfBoards.isEmpty() == false)
                  {
                    setBoardsToApplyOpticalCameraRectangle(updatedListOfBoards);
                    updateComponentListForBoardBased();
                  }
                  break;
                }
                // ask user if he/she wants to copy rectangles for the rest of the boards directly instead of saving points one by one for each rectangles in all selected boards.
                if (getBoardsToApplyOpticalCameraRectangles().size() > 1 && currentBoardIndex == 0 && _pspLiveVideoDialog.isPreviousRectangle() == false) // we should only prompt this message dialogue box once.
                {
                  _pspLiveVideoDialog.setPreviousRectangle(false);
                  java.util.List<Board> boardsToCopyTo = getBoardsToApplyOpticalCameraRectangles().subList(1, getBoardsToApplyOpticalCameraRectangles().size());                  
                  boolean shouldBreakOutOfLoop = true;
                  boolean shouldPromptDialog = false;
                  
                  // XCR-2694 Usability status of the rectangle is not update although already turn on rectangle
				  // Determine if OpticalCameraRectangle contains SurfaceMap point
                  if (finalBoard.getBoardSurfaceMapSettings().hasOpticalRegion())
                  {
                    for(OpticalRegion currentOpticalRegion : finalBoard.getBoardSurfaceMapSettings().getOpticalRegions())
                    {
                      for(OpticalCameraRectangle opticalCameraRectangle : currentOpticalRegion.getOpticalCameraRectangles(finalBoard))
                      {
                        if (opticalCameraRectangle.hasPanelCoordinateList())
                        {
                          shouldPromptDialog = true;
                          break;
                        }
                      }
                    }
                  }
                  
                  if (shouldPromptDialog)
                    shouldBreakOutOfLoop = promptToCopyRectanglesToBoards(finalBoard, boardsToCopyTo);                  
                  
                  if (shouldBreakOutOfLoop)
                    break;
                }
              }
              
              // refresh the Optical Rectangle table data.
              refreshTablesData();
              getBoardGraphicsEngine().repaint();
              _testDev.getBoardGraphicsEngineSetup().clearDividedOpticalRegionList();
              getBoardGraphicsEngine().setGroupSelectMode(false);             
              if (_pspRectangleTable.getRowCount() > 0)
              {
                _pspRectangleTable.setSelectedRow(0);
                _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
                if (_currentBoard.hasBoardSurfaceMapSettings())
                {
                  if (_currentBoard.getBoardSurfaceMapSettings().hasOpticalRegion())
                  {
                    _opticalRegion = _currentBoard.getBoardSurfaceMapSettings().getOpticalRegion();
                  }
                }
                _testDev.getBoardGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenBoard(_pspRectangleTableModel.getOpticalCameraRectangleUsingRowIndex(0).getBoard());
                _testDev.getBoardGraphicsEngineSetup().handleHighlightOpticalCameraRectangle(_pspRectangleTableModel.getOpticalCameraRectangleUsingRowIndex(0));
              }
              updateMeshTriangleOnCAD();
              if (_pspRectangleTableModel.getRowCount() == 0)
              {
                removeAllComponents();
              }
              else
              {
                for (Board board : _project.getPanel().getBoards())
                {
                  if (_pspRectangleTableModel.getBoardOpticalCameraRectangles(board).size() == 0)
                  {
                    removeAllComponentsInBoard(board);
                  }
                }
              }
            }   
            else
            {
              // they have not done manual alignment. so tell them they can no start the wizard.
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                      StringLocalizer.keyToString("PSP_RUN_LIVE_VIEW_ERROR_RUN_ALIGNMENT_BEFORE_LIVE_VIEW_MESSAGE_KEY"),
                      StringLocalizer.keyToString("PSP_RUN_LIVE_VIEW_ERROR_MESSAGE_TITLE_KEY"),
                      JOptionPane.ERROR_MESSAGE);
            }
          }
        }
      }
      else
      {
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                StringLocalizer.keyToString("PSP_RUN_LIVE_VIEW_ERROR_POPULATE_BEFORE_LIVE_VIEW_MESSAGE_KEY"),
                StringLocalizer.keyToString("PSP_RUN_LIVE_VIEW_ERROR_MESSAGE_TITLE_KEY"),
                JOptionPane.ERROR_MESSAGE);
      }
      
      //update optical rectangle used by each reconstruction region
      allocateOpticalCameraRectangleToComponents();
    }
    catch (XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
    }
    finally
    {
      _commandManager.endCommandBlockIfNecessary();
      _isLiveViewMode = false;
    }
  }
  
  /**
   * Disable all the highlighted rectangles.
   * @author Jack Hwee
   */
  private JMenuItem createDisableAllRectangleMenuItem()
  {
    JMenuItem disableAllMenuItem = new JMenuItem(StringLocalizer.keyToString(new LocalizedString("CAD_OPTICAL_RECTANGLE_TURN_OFF_ALL_RECTANGLE_KEY",
                                                                                                new Object[]{null})));
    disableAllMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableOpticalCameraRectangles(false);
      }
    });
    return disableAllMenuItem;
  }
  
  /**
   * Turns on / off "right-click to add red rectangles" mode.
   * @author Ying-Huan.Chu
   */
  public void setAddRectangleMode(boolean isAddRectangleModeOn)
  {
    _isAddRectangleModeOn = isAddRectangleModeOn;
  }
  
  /**
   * Returns true if "right-click to add red rectangles" mode is on.
   * @author Ying-Huan.Chu
   */
  public boolean isAddRectangleModeOn()
  {
    return _isAddRectangleModeOn;
  }
  
  /**
   * Select multiple rows in PspRectangletable if the rectangles passed in match the entries in PspRectangleTable.
   * (Currently only called when user drags across optical region.
   * @author Ying-Huan.Chu
   */
  public void selectMultipleRowsInPspRectangleTable(java.util.List<OpticalCameraRectangle> opticalCameraRectangleList)
  {
    if (opticalCameraRectangleList.size() > 0)
    {
      java.util.List<Integer> selectedRowList = new ArrayList<>();
      for (OpticalCameraRectangle ocr : opticalCameraRectangleList)
      {
        int selectedRow = _pspRectangleTableModel.getRowIndexUsingOpticalRectangleName(ocr.getName());
        if (selectedRow > -1)
        {
          selectedRowList.add(selectedRow);
        }
      }
      if (selectedRowList.size() > 0)
      {
        int[] selectedRows = new int[selectedRowList.size()];
        for (int i = 0; i < selectedRowList.size(); ++i)
        {
          selectedRows[i] = selectedRowList.get(i);
        }
        _pspRectangleTable.setSelectedRows(selectedRows);
      }
    }
  }
  
  /**
   * Wait for Single Image and Height Map calculation before proceed
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void waitForHeightMap()
  {
    _isHeightMapAvailable = false;
    AbstractOpticalCamera abstractOpticalCamera;
    if (_surfaceMapping.getPspModuleEnum().equals(PspModuleEnum.FRONT))
      abstractOpticalCamera = OpticalCameraArray.getCamera(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
    else
      abstractOpticalCamera = OpticalCameraArray.getCamera(OpticalCameraIdEnum.OPTICAL_CAMERA_2);
     
    int waitedTimeInMilliseconds = 0;
    _isHeightMapAvailable = abstractOpticalCamera.isHeightMapValueAvailable();
    while(_isHeightMapAvailable == false && waitedTimeInMilliseconds < WAITING_FOR_HEIGHT_MAP_MAXIMUM_DURATION_IN_MILLISECONDS)
    {
      try
      {
        Thread.sleep(100);  
        waitedTimeInMilliseconds += 100;
      }
      catch (InterruptedException ix)
      {
        // don't expect this, but don't really care how sleep returns either
      }
      _isHeightMapAvailable = abstractOpticalCamera.isHeightMapValueAvailable();
    }
  }

  /**
   * Prompt user whether want to reset all the rectangles
   *
   * @author Jack Hwee
   */
  private boolean promptToDeletePreviousRectangles()
  {
    boolean deleteAllRectangles = true;
    if (_isPanelBasedAlignment)
    {
      if (_project.getPanel().hasPanelSurfaceMapSettings())
      {
        if (_project.getPanel().getPanelSurfaceMapSettings().getOpticalRegions().isEmpty() == false &&
            _project.getPanel().getPanelSurfaceMapSettings().hasOpticalRegion())
        {
          // prompt comfirm message to show all PSP points set before will be clean up once you change the rotation
          int status = JOptionPane.showConfirmDialog(_mainUI, 
                                                     StringLocalizer.keyToString("MMGUI_REMOVE_PSP_POINTS_BEFORE_AUTO_LIVE_VIEW_WARNING_MESSAGE_KEY"),
                                                     StringLocalizer.keyToString("MMGUI_REMOVE_PSP_POINTS_WARNING_MESSAGE_TITLE_KEY"), 
                                                     JOptionPane.YES_NO_OPTION, 
                                                     JOptionPane.QUESTION_MESSAGE);

          if (status == JOptionPane.NO_OPTION)
          {
            return false;
          }
          else if (status == JOptionPane.CLOSED_OPTION)
          {
            return false;
          }
          else if (status == JOptionPane.YES_OPTION)
          {
            _project.getPanel().getPanelMeshSettings().clear();
            java.util.List<com.axi.v810.business.panelDesc.Component> components = new ArrayList<>();
            for (com.axi.v810.business.panelDesc.Component component : getAllComponents())
            {
              components.add(component);
            }
            //_commandManager.trackState(getCurrentUndoState());
            //_commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PSP_DELETE_OPTICAL_REGION_KEY"));
            try
            {
              _commandManager.execute(new SurfaceMapPanelDeleteOpticalRegionCommand(_project,
                                                                                    _project.getPanel().getPanelSurfaceMapSettings(),
                                                                                    _project.getPanel().getPanelSurfaceMapSettings().getOpticalRegion()));
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  ex.getLocalizedMessage(),
                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                  true);
            }
            /*finally
            {
              _commandManager.endCommandBlock();
            }*/
            addComponents(components);
          }
        }
      }
    }
    else
    {
      boolean shouldPromptUserToDeletePspPoint = false;
      for (Board board : _project.getPanel().getBoards())
      {
        if (board.hasBoardSurfaceMapSettings())
        {
          BoardSurfaceMapSettings boardSurfaceMapSettings = board.getBoardSurfaceMapSettings();

          if (boardSurfaceMapSettings.getOpticalRegions().isEmpty() == false)
          {
            shouldPromptUserToDeletePspPoint = true;
            break;
          }
        }
      }
      if (shouldPromptUserToDeletePspPoint == true)
      {
        int status = JOptionPane.showConfirmDialog(_mainUI, 
                                                   StringLocalizer.keyToString("MMGUI_REMOVE_PSP_POINTS_BEFORE_AUTO_LIVE_VIEW_WARNING_MESSAGE_KEY"),
                                                   StringLocalizer.keyToString("MMGUI_REMOVE_PSP_POINTS_WARNING_MESSAGE_TITLE_KEY"), 
                                                   JOptionPane.YES_NO_OPTION, 
                                                   JOptionPane.QUESTION_MESSAGE);
        if (status == JOptionPane.NO_OPTION)
        {
          return false;
        }
        else if (status == JOptionPane.CLOSED_OPTION)
        {
          return false;
        }
        else if (status == JOptionPane.YES_OPTION)
        {
          //_commandManager.trackState(getCurrentUndoState());
         // _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PSP_DELETE_OPTICAL_REGION_KEY"));
          
          java.util.List<com.axi.v810.business.panelDesc.Component> components = new ArrayList<>();
          for (com.axi.v810.business.panelDesc.Component component : getAllComponents())
          {
            if (getBoardsToApplyOpticalCameraRectangles().contains(component.getBoard()))
            {
              components.add(component);
            }
          }
          for (Board board : _project.getPanel().getBoards())
          {
            board.getBoardMeshSettings().clear();
            deleteBoardOpticalRegion(board);
          }
          //_commandManager.endCommandBlock();
          
          addComponents(components);
          
          components.clear();
        }
      }
    }
    return deleteAllRectangles;
  }
  
  /**
   * @author Jack Hwee
   */
  public void handleUndoState(final Object object)
  {
    UndoState undoState = (UndoState) object;
    
    // Refresh Rectangle table and SelectedComponent table.
    refreshTablesData();
    updateMeshTriangleOnCAD();
    //update optical rectangle used by each reconstruction region
    allocateOpticalCameraRectangleToComponents();
    if (_isPanelBasedAlignment == false)
    {
      updateCurrentSelectedBoardsString();
    }
  }
  
  /**
   * Remove components in the list from the Components table.
   * @author Ying-Huan.Chu
   */
  public void removeComponents(java.util.List<com.axi.v810.business.panelDesc.Component> components)
  {
    _pspSelectedComponentTableModel.removeComponents(components);
  }
  
  /**
   * Remove all components of a specific board from the Components table.
   * @author Ying-Huan.Chu
   */
  public void removeAllComponentsInBoard(Board board)
  {
    Assert.expect(board != null);
    
    _pspSelectedComponentTableModel.removeAllComponentsInBoard(board);
  }
  
  /**
   * Remove all components from the Components Table.
   * @author Ying-Huan.Chu
   */
  public void removeAllComponents()
  {
    _pspSelectedComponentTableModel.removeAllComponents();
  }
 
  /**
   * Display the previous board.
   * Note: This function will only work when there are multiple boards selected in Select Board dialog. 
   * @author Ying-Huan.Chu
   */
  public void displayPreviousBoard()
  {
    if (_project == null)
      _project = Project.getCurrentlyLoadedProject();
    if (_testDev == null)
      _testDev = _mainUI.getTestDev();
    _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
    int currentBoardIndex = 0;
    for (int i = 0; i < _boardsToApplyOpticalCameraRectangles.size(); ++i)
    {
      if (_boardsToApplyOpticalCameraRectangles.get(i).equals(_currentBoard))
      {
        currentBoardIndex = i;
        break;
      }
    }
    int previousBoardIndex = currentBoardIndex - 1;
    if (previousBoardIndex < 0)
    {
      previousBoardIndex = 0;
      return;
    }
    
    changeGraphicsToBoard(_boardsToApplyOpticalCameraRectangles.get(previousBoardIndex));
    
    _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
    if(_hasPopulated)
      _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegion();
    else if (_currentBoard.hasBoardSurfaceMapSettings())
    {
      if (_currentBoard.getBoardSurfaceMapSettings().hasOpticalRegion())
      {
        for (OpticalRegion opticalRegion : _currentBoard.getBoardSurfaceMapSettings().getOpticalRegions())
        {
          if (opticalRegion.hasOpticalCameraRectangles(_currentBoard))
          {
            _opticalRegion = opticalRegion;
            break;
          }
        }
      }
      // update CAD graphics.
      if (_opticalRegion != null)
      {
        _testDev.getBoardGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
      }
      _testDev.getBoardGraphicsEngineSetup().handleMultipleHighlightOpticalCameraRectanglesForCurrentBoard(_testDev.getBoardGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList());
      updateMeshTriangleOnCAD();
    }
  }
  
  /**
   * Display the next board. 
   * Note: This function will only work when there are multiple boards selected in Select Board dialog.
   * @author Ying-Huan.Chu
   */
  public void displayNextBoard()
  {
    if (_project == null)
      _project = Project.getCurrentlyLoadedProject();
    if (_testDev == null)
      _testDev = _mainUI.getTestDev();
    _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
    int currentBoardIndex = 0;
    for (int i = 0; i < _boardsToApplyOpticalCameraRectangles.size(); ++i)
    {
      if (_boardsToApplyOpticalCameraRectangles.get(i).equals(_currentBoard))
      {
        currentBoardIndex = i;
        break;
      }
    }
    int nextBoardIndex = currentBoardIndex + 1;
    if (nextBoardIndex > _boardsToApplyOpticalCameraRectangles.size() - 1)
    {
      nextBoardIndex = _boardsToApplyOpticalCameraRectangles.size() - 1;
      return;
    }
    
    changeGraphicsToBoard(_boardsToApplyOpticalCameraRectangles.get(nextBoardIndex));
    
    _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
    if(_hasPopulated)
      _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegion();
    else if (_currentBoard.hasBoardSurfaceMapSettings())
    {
      if (_currentBoard.getBoardSurfaceMapSettings().hasOpticalRegion())
      {
        for (OpticalRegion opticalRegion : _currentBoard.getBoardSurfaceMapSettings().getOpticalRegions())
        {
          if (opticalRegion.hasOpticalCameraRectangles(_currentBoard))
          {
            _opticalRegion = opticalRegion;
            break;
          }
        }
      }
      // update CAD graphics.
      if (_opticalRegion != null)
      {
        _testDev.getBoardGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
      }
      _testDev.getBoardGraphicsEngineSetup().handleMultipleHighlightOpticalCameraRectanglesForCurrentBoard(_testDev.getBoardGraphicsEngineSetup().getHighlightedOpticalCameraRectangleList());
      updateMeshTriangleOnCAD();
    }
  }
  
  /**
   * Update Mesh Triangles graphics in CAD.
   * @author Jack Hwee
   */
  public void updateMeshTriangleOnCAD()
  {
    //update mesh triangle
    _project = Project.getCurrentlyLoadedProject();
    if (_project.getPanel().isUseMeshTriangle())
    {
      if (_isPanelBasedAlignment)
      { 
        _testDev.getPanelGraphicsEngineSetup().removeMeshTriangleLayer();   
        _project.getPanel().getPanelMeshSettings().clear(); 
        if (_project.getPanel().getPanelSurfaceMapSettings().hasOpticalRegion())
        {
          java.util.List<MeshTriangle> meshTriangleList = MeshUtil.getInstance().createMesh(_project.getPanel().getPanelSurfaceMapSettings().getOpticalRegion().getEnabledOpticalCameraRectangles());
          _opticalRegion.clearAllMeshTriangles();
          _project.getPanel().getPanelMeshSettings().clear();
          for (MeshTriangle meshTriangle: meshTriangleList)
          {
            _project.getPanel().getPanelMeshSettings().addMeshTriangle(meshTriangle);
            _opticalRegion.addMeshTriangle(meshTriangle);
          }
          if (_project.getPanel().isShowMeshTriangle())
          {
            _testDev.getPanelGraphicsEngineSetup().handleMeshTriangle(meshTriangleList);
          }
        }
      }
      else
      {  
        _testDev.getBoardGraphicsEngineSetup().removeMeshTriangleLayer(); 
        Board currentBoard =  _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
        for (Board board : getBoardsToApplyOpticalCameraRectangles())
        {
          if (board.hasBoardMeshSettings())
            board.getBoardMeshSettings().clear();
          if (board.hasBoardSurfaceMapSettings())
          {
            if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
            {
              _opticalRegion = board.getBoardSurfaceMapSettings().getOpticalRegion();
              java.util.List<MeshTriangle> meshTriangleList = MeshUtil.getInstance().createMesh(board.getBoardSurfaceMapSettings().getOpticalRegion().getEnabledBoardOpticalCameraRectangles(board));
              _opticalRegion.clearAllMeshTriangles();
              board.getBoardMeshSettings().clear();
              for (MeshTriangle meshTriangle: meshTriangleList)
              {
                board.getBoardMeshSettings().addMeshTriangle(meshTriangle);
                _opticalRegion.addMeshTriangle(meshTriangle);
              }
              if (_project.getPanel().isShowMeshTriangle())
              {
                // if the board is the current displayed board, handle the mesh graphics.
                if (board.getName().equalsIgnoreCase(currentBoard.getName()))
                {
                  _testDev.getBoardGraphicsEngineSetup().handleMeshTriangle(meshTriangleList);
                }
              }
            }
          }
        }
      }
    }
    
    if (_project.getPanel().isShowMeshTriangle() == false)
    {
      if (_isPanelBasedAlignment)
      {
        _testDev.getPanelGraphicsEngineSetup().handleMeshTriangle(new ArrayList<MeshTriangle>());
      }
      else
      {
        _testDev.getBoardGraphicsEngineSetup().handleMeshTriangle(new ArrayList<MeshTriangle>());
      }
    }
  } 
 
  /**
   * @author Cheah Lee Herng
   */
  private void updatePopulateRectangleOptions()
  {
    boolean isRemoveComponentOnTopOfComponents = _project.getPanel().isRemoveComponentOnTopOfComponents();
    _removeRectangleOnTopOfComponentCheckbox.setSelected(isRemoveComponentOnTopOfComponents);
    
    boolean isExtendRectanglesToEdgeOfBoard = _project.getPanel().isExtendRectanglesToEdgeOfBoard();
    _extendRectangleToBoardEdgeCheckbox.setSelected(isExtendRectanglesToEdgeOfBoard);
    
    boolean isUseLargerFOV = _project.getPanel().isLargerFOV();
    _useLargerFovCheckbox.setSelected(isUseLargerFOV);
    
    boolean isUseMeshTriangle = _project.getPanel().isUseMeshTriangle();
    _useMeshTriangleCheckbox.setSelected(isUseMeshTriangle);
    _showMeshTriangleCheckbox.setEnabled(isUseMeshTriangle);
    
    boolean isShowMeshTriangle = _project.getPanel().isShowMeshTriangle();
    _showMeshTriangleCheckbox.setSelected(isShowMeshTriangle);
    
    boolean isAutomaticallyIncludeAllComponents = _project.getPanel().isAutomaticallyIncludeAllComponents();
    _autoIncludeWholePanelComponentRadioButton.setSelected(isAutomaticallyIncludeAllComponents);
    _isAutoIncludedAllComponents = isAutomaticallyIncludeAllComponents;
    _includeFollowingComponentOnlyRadioButton.setSelected(!isAutomaticallyIncludeAllComponents);
    //Kok Chun, Tan - show correct table content based on the selected radio buttton when switch to this tab
    if (_isLiveViewMode == false)
    {
	  if(isAutomaticallyIncludeAllComponents)
	    autoIncludeWholePanelComponentRadioButton_actionPerformed();
	  else
	    includeFollowingComponentOnlyRadioButton_actionPerformed();
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void autoIncludeWholePanelComponentRadioButton_actionPerformed()
  {
    _includeFollowingComponentOnlyRadioButton.setSelected(false);
    _autoIncludeWholePanelComponentRadioButton.setSelected(true);
    _isAutoIncludedAllComponents = true;
    removeAllComponents();
    if (_isPanelBasedAlignment)
    {
      java.util.List<com.axi.v810.business.panelDesc.Component> components = new ArrayList<>();
      for (com.axi.v810.business.panelDesc.Component component : _currentBoardType.getPanel().getComponents())
      {
        if (isComponentUsableInSurfaceMap(component))
        {
          if (getAllComponents().contains(component) == false)
          {
            components.add(component);
          }
        }
      }
      _pspSelectedComponentTableModel.addComponents(components);
    }
    else
    {
      java.util.List<Board> boardsToIncludeComponents = getBoardsToApplyOpticalCameraRectangles();
      java.util.List<com.axi.v810.business.panelDesc.Component> components = new ArrayList<>();
      for (Board board : boardsToIncludeComponents)
      {
        for (com.axi.v810.business.panelDesc.Component component : board.getComponents())
        {
          if (isComponentUsableInSurfaceMap(component))
          {
            if (getAllComponents().contains(component) == false)
            {
              components.add(component);
            }
          }
        }
      }
      _pspSelectedComponentTableModel.addComponents(components);
    }
    _pspSelectedComponentTableModel.fireTableDataChanged();
    updateComponentsList();
    sortPspSelectedComponentTable();
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void includeFollowingComponentOnlyRadioButton_actionPerformed()
  {
    _autoIncludeWholePanelComponentRadioButton.setSelected(false);
    _isAutoIncludedAllComponents = false;
    _includeFollowingComponentOnlyRadioButton.setSelected(true);
    removeAllComponents();
    _pspSelectedComponentTableModel.populateWithPanelData(_currentBoardType);
    _pspSelectedComponentTableModel.fireTableDataChanged();
    updateComponentsList();
    sortPspSelectedComponentTable();
  }
  
  /**
   * @author Ying-Huan.Chu
   * @return current undo state
   */
  public UndoState getCurrentUndoState()
  {
    UndoState undoState = new UndoState();

    return undoState;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setBoardsToApplyOpticalCameraRectangle(java.util.List<Board> boards)
  {
    _boardsToApplyOpticalCameraRectangles = boards;
    if (_testDev == null)
      _testDev = _mainUI.getTestDev();
    if (_testDev.hasBoardGraphicsEngineSetup())
    {
      if (_testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard() != null)
      {
        _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
        if (boards.size() > 0)
        {
          if (boards.contains(_currentBoard) == false)
          {
            changeGraphicsToBoard(boards.get(0));
            _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
          }
        }
      }
    }
    updateCurrentSelectedBoardsString();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void addBoardToApplyOpticalCameraRectangle(Board board)
  {
    if (_boardsToApplyOpticalCameraRectangles.contains(board) == false)
    {
      _boardsToApplyOpticalCameraRectangles.add(board);
    }
    updateCurrentSelectedBoardsString();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public java.util.List<Board> getBoardsToApplyOpticalCameraRectangles()
  {
    return _boardsToApplyOpticalCameraRectangles;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private String getCurrentSelectedBoardsString()
  {
	// sort the list first.
    java.util.List<String> boardsToApplyOpticalCameraRectanglesString = new ArrayList<>();
    for (Board board : _boardsToApplyOpticalCameraRectangles)
    {
      boardsToApplyOpticalCameraRectanglesString.add(board.getName());
    }
    Collections.sort(boardsToApplyOpticalCameraRectanglesString);
    String selectedBoardsString = " ";
    for (int i = 0; i < boardsToApplyOpticalCameraRectanglesString.size(); ++i)
    {
      selectedBoardsString += boardsToApplyOpticalCameraRectanglesString.get(i);
      if (i != (boardsToApplyOpticalCameraRectanglesString.size()-1))
      {
        selectedBoardsString += ", ";
      }
    }
    return selectedBoardsString;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void updateCurrentSelectedBoardsString()
  {
    _currentSelectedBoardsLabel.setText(StringLocalizer.keyToString("PSP_CURRENT_SELECTED_BOARD_KEY") + getCurrentSelectedBoardsString());
    _currentSelectedBoardsLabel.updateUI();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void selectBoardsDialogButton_actionPerformed()
  {
    SurfaceMapSelectBoardDialog selectBoardDialog = new SurfaceMapSelectBoardDialog(_currentBoardType, 
                                                                                    _mainUI, 
                                                                                    StringLocalizer.keyToString("PSP_SELECT_BOARDS_KEY"));
    SwingUtils.centerOnComponent(selectBoardDialog, _mainUI);
    selectBoardDialog.setVisible(true);
    selectBoardDialog.addWindowListener(new WindowListener() 
    {
      public void windowClosed(WindowEvent e)
      {
        updateCurrentSelectedBoardsString();
        _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
        // handle board graphics
        if (_testDev.getBoardGraphicsEngineSetup().isDragOpticalRegionLayerVisible())
        {
          _testDev.getBoardGraphicsEngineSetup().handleDragOpticalRegion();
        }
        if (_testDev.getBoardGraphicsEngineSetup().isSelectedOpticalRegionLayerVisible())
        {
          if (_currentBoard.hasBoardSurfaceMapSettings())
          {
            if (_currentBoard.getBoardSurfaceMapSettings().hasOpticalRegion())
            {
              _opticalRegion = _currentBoard.getBoardSurfaceMapSettings().getOpticalRegion();
              _testDev.getBoardGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
              updateMeshTriangleOnCAD();
            }
          }
        }
      }
      public void windowOpened(WindowEvent e) { }
      public void windowClosing(WindowEvent e) { }
      public void windowIconified(WindowEvent e) { }
      public void windowDeiconified(WindowEvent e) { }
      public void windowActivated(WindowEvent e) { }
      public void windowDeactivated(WindowEvent e) { }
    });
    
    if (selectBoardDialog.isUserClickedOk())
    {
      // update component list.
      java.util.List<com.axi.v810.business.panelDesc.Component> componentsInTable = new ArrayList<>();
      // copy all components in the Component table.
      componentsInTable.addAll(getAllComponents());
      for (Board board : _project.getPanel().getBoards())
      {
        if (getBoardsToApplyOpticalCameraRectangles().contains(board) == false)
        {
          // remove all components of boards that are not selected.
          removeAllComponentsInBoard(board);
          // disable all rectangles in the unselected board so that the rectangles will not be included 
          // when running surface map in Production and generate image set in Fine Tuning page.
          setBoardOpticalCameraRectanglesEnabled(board, false);
        }
      }
      // currently selected boards count is more than previous.
      java.util.List<com.axi.v810.business.panelDesc.Component> componentsToAdd = new ArrayList<>();
      for (com.axi.v810.business.panelDesc.Component selectedComponent : componentsInTable)
      {
        for (Board board : getBoardsToApplyOpticalCameraRectangles())
        {
          // do not modify components if the board was previously selected.
          if (_pspSelectedComponentTableModel.hasBoardComponents(board.getName()) == false)
          {
            if (board.hasComponent(selectedComponent.toString()))
            {
              com.axi.v810.business.panelDesc.Component comp = board.getComponent(selectedComponent.toString());
              componentsToAdd.add(comp);
              if (board.hasBoardSurfaceMapSettings())
              {
                if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
                {
                  board.getBoardSurfaceMapSettings().getOpticalRegion().addComponent(board, board.getComponent(selectedComponent.toString()));
                }
              }
            }
          }
          // enable all rectangles in the selected board
          setBoardOpticalCameraRectanglesEnabled(board, true);
        }
      }
      _pspSelectedComponentTableModel.addComponents(componentsToAdd);
      _pspSelectedComponentTableModel.fireTableDataChanged();
      updateComponentsList();
      sortPspSelectedComponentTable();
    }
  }
  
  /**
   * @author Jack Hwee
   */
  private void checkOpticalCameraRectangleCompatibility(OpticalCameraRectangle opticalCameraRectangle)
  {
    OpticalCameraRectangle newOpticalCameraRectangle = new OpticalCameraRectangle();
    // Jack Hwee - If there are optical rectangle for Version 1 PSP, convert it to suite current version
    int version = FileName.getProjectBoardSurfaceMapSettingsLatestFileVersion();
    while (version > 0)
    {
      String fileName;
      if (_isPanelBasedAlignment)
        fileName = FileName.getProjectPanelSurfaceMapSettingsFullPath(_project.getName(), version);
      else
        fileName = FileName.getProjectBoardSurfaceMapSettingsFullPath(_project.getName(), 1, version);

      if (FileUtilAxi.exists(fileName))
        break;
      --version;
    }
    if (version == 1)
    {
      if (_isPanelBasedAlignment)
      {
        for (com.axi.v810.business.panelDesc.Component component : opticalCameraRectangle.getComponentList())
        {
          Rectangle rectInNano = component.getShapeRelativeToPanelInNanoMeters().getBounds();
          Rectangle rect = getPanelGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(rectInNano);
          Rectangle rectInPixel = new Rectangle((int) rect.getMinX(), (int) rect.getMinY(), OPTICAL_CAMERA_MAX_WIDTH_IN_PIXELS, OPTICAL_CAMERA_MAX_HEIGHT_IN_PIXELS);
          Rectangle newRectInNano = getPanelGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(rectInPixel);

          newOpticalCameraRectangle.setRegion(newRectInNano.x, newRectInNano.y, newRectInNano.width, newRectInNano.height);
          if (_project.getPanel().getPanelSurfaceMapSettings().hasOpticalRegion())
          {
            for (OpticalCameraRectangle ocr: _project.getPanel().getPanelSurfaceMapSettings().getOpticalRegion().getAllOpticalCameraRectangles())
            {
              if (ocr.getRegion().getRectangle2D().getBounds().equals(newRectInNano))
                break;
              //if two rectangles are intersect each other, remove it
              if ((ocr.getRegion().getRectangle2D().getBounds().intersects(newRectInNano) == false || opticalCameraRectangle.getComponentList().size() == 1) && ocr.getComponentList().contains(component))
              {
                if (ocr.getRegion().getRectangle2D().getBounds().equals(newRectInNano))
                  break;
                else
                {
                  opticalCameraRectangle.modifyRegion(newRectInNano.x, newRectInNano.y, newRectInNano.width, newRectInNano.height);
                } 
                return;
              }
            }
          }
        }
      }
      else
      {
        Board board = _opticalRegion.getBoards().get(0);

        for (com.axi.v810.business.panelDesc.Component component : opticalCameraRectangle.getComponentList())
        {
          Rectangle rectInNano = component.getShapeRelativeToPanelInNanoMeters().getBounds();
          Rectangle rect = getBoardGraphicsEngine().convertSurfaceMapAlignmentRegionToPixel(rectInNano);
          Rectangle rectInPixel = new Rectangle((int) rect.getMinX(), (int) rect.getMinY(), OPTICAL_CAMERA_MAX_WIDTH_IN_PIXELS, OPTICAL_CAMERA_MAX_HEIGHT_IN_PIXELS);
          Rectangle newRectInNano = getBoardGraphicsEngine().calculateSelectedComponentRegionInNanoMeter(rectInPixel);

          newOpticalCameraRectangle.setRegion(newRectInNano.x, newRectInNano.y, newRectInNano.width, newRectInNano.height);
       
          if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
          {
            for (OpticalCameraRectangle ocr: board.getBoardSurfaceMapSettings().getOpticalRegion().getOpticalCameraRectangles(board))
            {
              if (ocr.getRegion().getRectangle2D().getBounds().equals(newRectInNano))
                break;
              //if two rectangles are intersect each other, remove it
              if ((ocr.getRegion().getRectangle2D().getBounds().intersects(newRectInNano) == false || opticalCameraRectangle.getComponentList().size() == 1) && ocr.getComponentList().contains(component))
              {
                if (ocr.getRegion().getRectangle2D().getBounds().equals(newRectInNano))
                  break;
                else
                {
                  opticalCameraRectangle.modifyRegion(newRectInNano.x, newRectInNano.y, newRectInNano.width, newRectInNano.height);
                } 
                return;
              }
            }
          }
        }
      }
    }
  }
  
  /**
   * Refresh Rectangle table and SelectedComponent table.
   * @author Ying-Huan.Chu
   */
  private void refreshTablesData()
  {
    _pspRectangleTableModel.clear();
    _pspSelectedComponentTableModel.clear();
    if (_isPanelBasedAlignment)
    {
      if (_project.getPanel().getPanelSurfaceMapSettings().hasOpticalRegion())
        _opticalRegion = _project.getPanel().getPanelSurfaceMapSettings().getOpticalRegion();
      _pspRectangleTableModel.populateWithOpticalRegion(_opticalRegion);
      _pspRectangleTableModel.fireTableDataChanged();
      _pspRectangleTableModel.sortColumn(0, true);
      
      _pspSelectedComponentTableModel.populateWithOpticalRegion(_opticalRegion);
      _pspSelectedComponentTableModel.fireTableDataChanged();
      updateComponentsList();
      sortPspSelectedComponentTable();

      _testDev.getPanelGraphicsEngineSetup().handleMultipleSelectedOpticalCameraRectangles(_pspRectangleTableModel.getOpticalCameraRectangles());
      getPanelGraphicsEngine().repaint();
    }
    else
    {
      for (Board board : _project.getPanel().getBoards())
      {
        if (board.hasBoardSurfaceMapSettings())
        {
          if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
          {
            for (OpticalRegion opticalRegion : board.getBoardSurfaceMapSettings().getOpticalRegions())
            {
              _opticalRegion = opticalRegion;
              if (_opticalRegion.getOpticalCameraRectangles(board).isEmpty() == false)
              {
                _pspRectangleTableModel.populateWithOpticalRegion(_opticalRegion);
                _pspSelectedComponentTableModel.populateWithOpticalRegion(_opticalRegion, board);
              }
            }
          }
        }
      }
      _pspRectangleTableModel.fireTableDataChanged();
      _pspRectangleTableModel.sortColumn(0, true);
      _pspSelectedComponentTableModel.fireTableDataChanged();
      updateComponentsList();
      sortPspSelectedComponentTable();
      _testDev.getBoardGraphicsEngineSetup().handleMultipleSelectedOpticalCameraRectangles(_pspRectangleTableModel.getOpticalCameraRectangles());
      getBoardGraphicsEngine().repaint();
    }
    boolean isAutomaticallyIncludeAllComponents = _project.getPanel().isAutomaticallyIncludeAllComponents();
    _autoIncludeWholePanelComponentRadioButton.setSelected(isAutomaticallyIncludeAllComponents);
    _isAutoIncludedAllComponents = isAutomaticallyIncludeAllComponents;
    _includeFollowingComponentOnlyRadioButton.setSelected(!isAutomaticallyIncludeAllComponents);
    _useLargerFovCheckbox.setSelected(_project.getPanel().isLargerFOV());
    updateMeshTriangleOnCAD();
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void deletePanelOpticalRegion()
  {
    if (_project.getPanel().getPanelSurfaceMapSettings().hasOpticalRegion())
    {
      try
      {
        _commandManager.execute(new SurfaceMapPanelDeleteOpticalRegionCommand(_project,
                                                                              _project.getPanel().getPanelSurfaceMapSettings(),
                                                                              _project.getPanel().getPanelSurfaceMapSettings().getOpticalRegion()));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
            ex.getLocalizedMessage(),
            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
            true);
      }
      finally
      {
        _testDev.getPanelGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
        _testDev.getPanelGraphicsEngineSetup().clearSelectedOpticalCameraRectangleList();
        _testDev.getPanelGraphicsEngineSetup().setDragOpticalRegionLayersVisible(false);

        _shouldInteractWithGraphics = true;
        invalidateChecksum();
        refreshTablesData();
      }
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void deletePanelOpticalCameraRectangles(java.util.List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    _commandManager.trackState(getCurrentUndoState());
    _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_DELETE_OPTICAL_CAMERA_RECTANGLE_KEY"));
    try
    {
      for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangles)
      {
        _commandManager.execute(new SurfaceMapPanelDeleteOpticalCameraRectangleCommand(_opticalRegion, opticalCameraRectangle));
        _testDev.getPanelGraphicsEngineSetup().removeSelectedOpticalCameraRectangle(opticalCameraRectangle);
        _pspRectangleTableModel.removeRow(opticalCameraRectangle);
      }
      _testDev.getPanelGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
      _testDev.getPanelGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                    ex.getLocalizedMessage(),
                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                    true);
    }
    finally
    {
      _commandManager.endCommandBlock();
      invalidateChecksum();
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void deleteBoardOpticalRegion(Board opticalRegionBoard)
  {
    try
    {
      opticalRegionBoard.getBoardMeshSettings().clear();
      if (opticalRegionBoard.hasBoardSurfaceMapSettings())
      {
        BoardSurfaceMapSettings boardSurfaceMapSettings = opticalRegionBoard.getBoardSurfaceMapSettings();
        if (boardSurfaceMapSettings.hasOpticalRegion())
        {
          _opticalRegion = boardSurfaceMapSettings.getOpticalRegion();
          _commandManager.execute(new SurfaceMapBoardDeleteOpticalRegionCommand(_project, boardSurfaceMapSettings, _opticalRegion));
        }
      }
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
        ex.getLocalizedMessage(),
        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
        true);
    }
    finally
    {
      _testDev.getBoardGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
      _testDev.getBoardGraphicsEngineSetup().clearSelectedOpticalCameraRectangleList(opticalRegionBoard);
      _testDev.getBoardGraphicsEngineSetup().setDragOpticalRegionLayersVisible(false); 

      _shouldInteractWithGraphics = true;
      invalidateChecksum();
      refreshTablesData();
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void deleteBoardOpticalCameraRectangles(java.util.List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    try
    {
      for (Board board : _project.getPanel().getBoards())
      {
        if (board.hasBoardSurfaceMapSettings())
        {
          if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
          {
            _opticalRegion = board.getBoardSurfaceMapSettings().getOpticalRegion();
            for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangles)
            {
              if (_opticalRegion.getOpticalCameraRectangles(board).contains(opticalCameraRectangle))
              {
                _commandManager.execute(new SurfaceMapBoardDeleteOpticalCameraRectangleCommand(board, _opticalRegion, opticalCameraRectangle));
                _testDev.getBoardGraphicsEngineSetup().removeSelectedOpticalCameraRectangle(opticalCameraRectangle);
                _pspRectangleTableModel.removeRow(opticalCameraRectangle);
              }
            }
          }
        }
      }
      _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
      if (_currentBoard.hasBoardSurfaceMapSettings())
      {
        if (_currentBoard.getBoardSurfaceMapSettings().hasOpticalRegion())
        {
          for (OpticalRegion opticalRegion : _currentBoard.getBoardSurfaceMapSettings().getOpticalRegions())
          {
            if (opticalRegion.hasOpticalCameraRectangles(_currentBoard))
            {
              _opticalRegion = opticalRegion;
              break;
            }
          }
        }
      }
      _testDev.getBoardGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
      _testDev.getBoardGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
              ex.getLocalizedMessage(),
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
              true);
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void deleteOpticalCameraRectangles_actionPerformed()
  {
    int[] selectedRows = _pspRectangleTable.getSelectedRows();
    if (selectedRows.length > 0)
    {
      java.util.List<OpticalCameraRectangle> selectedOpticalCameraRectangles = _pspRectangleTableModel.getSelectedOpticalCameraRectangles(selectedRows);
      if (_isPanelBasedAlignment)
      {
        if ((_opticalRegion.getAllOpticalCameraRectangles().size() == 1) || 
            (selectedOpticalCameraRectangles.size() == _opticalRegion.getAllOpticalCameraRectangles().size()))
        {
          _commandManager.trackState(getCurrentUndoState());
          deletePanelOpticalRegion();
        }
        else
        {
          deletePanelOpticalCameraRectangles(selectedOpticalCameraRectangles);
        }
      }
      else
      {
        _commandManager.trackState(getCurrentUndoState());
        _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PSP_DELETE_OPTICAL_REGION_KEY"));
        for (Board board : _project.getPanel().getBoards())
        {
          if (board.hasBoardSurfaceMapSettings())
          {
            if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
            {
              _opticalRegion = board.getBoardSurfaceMapSettings().getOpticalRegion();
              if (selectedOpticalCameraRectangles.containsAll(_opticalRegion.getOpticalCameraRectangles(board)))
              {
                selectedOpticalCameraRectangles.removeAll(_opticalRegion.getOpticalCameraRectangles(board));
                deleteBoardOpticalRegion(board);
              }
            }
          }
        }
        if (selectedOpticalCameraRectangles.size() > 0)
          deleteBoardOpticalCameraRectangles(selectedOpticalCameraRectangles);
        _commandManager.endCommandBlock();
      }
      updateMeshTriangleOnCAD();
      //update optical rectangle used by each reconstruction region
      allocateOpticalCameraRectangleToComponents();
      if (_pspSelectedComponentTable.getRowCount() == 0)
      {
        _autoIncludeWholePanelComponentRadioButton.setSelected(false);
        _isAutoIncludedAllComponents = false;
        _includeFollowingComponentOnlyRadioButton.setSelected(true);
      }
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void toggleOpticalCameraRectangles()
  {
    int[] selectedRows = _pspRectangleTable.getSelectedRows();
    if (selectedRows.length > 0)
    {
      // XCR-2694 Cheah Lee Herng - Usability status of the rectangle is not update although already turn on rectangle
      java.util.Map<OpticalCameraRectangle, Boolean> opticalCameraRectangleToInspectableStatusMap = new HashMap<OpticalCameraRectangle, Boolean>();
      
      try
      {
        java.util.List<OpticalCameraRectangle> selectedOpticalCameraRectangles = _pspRectangleTableModel.getSelectedOpticalCameraRectangles(selectedRows);
        java.util.List<OpticalCameraRectangle> opticalCameraRectangleWithPspPoints = new ArrayList<>();
        java.util.List<OpticalCameraRectangle> opticalCameraRectangleWithoutPspPoints = new ArrayList<>();
        for (OpticalCameraRectangle opticalCameraRectangle : selectedOpticalCameraRectangles)
        {
          if (opticalCameraRectangle.hasPanelCoordinateList())
          {
            opticalCameraRectangleWithPspPoints.add(opticalCameraRectangle);
          }
          else
          {
            opticalCameraRectangleWithoutPspPoints.add(opticalCameraRectangle);
          }
          
          // XCR-2694 Cheah Lee Herng - Usability status of the rectangle is not update although already turn on rectangle
          if (opticalCameraRectangleToInspectableStatusMap.containsKey(opticalCameraRectangle) == false)
            opticalCameraRectangleToInspectableStatusMap.put(opticalCameraRectangle, opticalCameraRectangle.getInspectableBool());
          else
          {
            opticalCameraRectangleToInspectableStatusMap.remove(opticalCameraRectangle);
            opticalCameraRectangleToInspectableStatusMap.put(opticalCameraRectangle, opticalCameraRectangle.getInspectableBool());
          }
        }
        if (opticalCameraRectangleWithoutPspPoints.size() > 0)
        {
          boolean shouldStartUp = false;
          for (OpticalCameraRectangle ocr : opticalCameraRectangleWithoutPspPoints)
          {
            if (ocr.getInspectableBool() == false)
            {
              shouldStartUp = true;
              break;
            }
          }
          if (shouldStartUp)
          {
            if (_mainUI.beforeGenerateSurfaceMapping(this) == false)
            {
              return;
            }
            if (_mainUI.isPanelInLegalState() == false)
            {
              return;
            }
          }
        }
        for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangleWithoutPspPoints)
        {
          if (editSurfaceMapPoints(opticalCameraRectangle) == false)
          {
            // user canceled edit point.
            break;
          }
          if (opticalCameraRectangle.hasPanelCoordinateList())
          {
            opticalCameraRectangleWithPspPoints.add(opticalCameraRectangle);
          }
        }
        _commandManager.trackState(getCurrentUndoState());
        _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_TOGGLE_OPTICAL_CAMERA_RECTANGLE_KEY"));
        for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangleWithPspPoints)
        {
          // XCR-2694 Cheah Lee Herng - Usability status of the rectangle is not update although already turn on rectangle
          if (opticalCameraRectangleToInspectableStatusMap.containsKey(opticalCameraRectangle))
          {
            BooleanRef isInspectable = new BooleanRef();
            if (opticalCameraRectangleToInspectableStatusMap.get(opticalCameraRectangle) == true)
            {
              isInspectable.setValue(false);
            }
            else
            {
              isInspectable.setValue(true);
            }
            enableOpticalCameraRectangle(opticalCameraRectangle, isInspectable.getValue());
          }
        }
        _commandManager.endCommandBlock();
        // Need to call fireTableDataChanged() to ensure the table's data are updated.
        // Reselect the table rows after calling fireTableDataChanged().
        int[] pspRectangleTableSelectedRows = _pspRectangleTable.getSelectedRows();
        _pspRectangleTableModel.fireTableDataChanged();
        _pspRectangleTableModel.sortColumn(0, true);
        if (pspRectangleTableSelectedRows.length > 0)
        {
          _pspRectangleTable.setSelectedRows(pspRectangleTableSelectedRows);
        }
        if (_isPanelBasedAlignment)
        {
          _testDev.getPanelGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
          _testDev.getPanelGraphicsEngineSetup().handleMultipleHighlightOpticalCameraRectangles(selectedOpticalCameraRectangles);
        }
        else
        {
          // to highlight the OpticalCameraRectangles of current board only.
          _currentBoard = _testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard();
          if (_currentBoard.hasBoardSurfaceMapSettings())
          {
            if (_currentBoard.getBoardSurfaceMapSettings().hasOpticalRegion())
            {
              for (OpticalRegion opticalRegion : _currentBoard.getBoardSurfaceMapSettings().getOpticalRegions())
              {
                if (opticalRegion.hasOpticalCameraRectangles(_currentBoard))
                {
                  _opticalRegion = opticalRegion;
                  break;
                }
              }
            }
          }
          java.util.List<OpticalCameraRectangle> opticalCameraRectangles = new ArrayList<>();
          for (OpticalCameraRectangle opticalCameraRectangle : selectedOpticalCameraRectangles)
          {
            if (opticalCameraRectangle.getBoard().equals(_currentBoard) == false)
              opticalCameraRectangles.add(opticalCameraRectangle);
          }
          selectedOpticalCameraRectangles.removeAll(opticalCameraRectangles);
          _testDev.getBoardGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
          _testDev.getBoardGraphicsEngineSetup().handleMultipleHighlightOpticalCameraRectangles(selectedOpticalCameraRectangles);
        }
        // Added by Lee Herng, 2014-05-08
        updateMeshTriangleOnCAD();
        //update optical rectangle used by each reconstruction region
        allocateOpticalCameraRectangleToComponents();
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                ex,
                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                true);
      }
      finally
      {
        // XCR-2694 Cheah Lee Herng - Usability status of the rectangle is not update although already turn on rectangle
        opticalCameraRectangleToInspectableStatusMap.clear();
      }
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private void enableOpticalCameraRectangles(boolean enable)
  {
    int[] selectedRows = _pspRectangleTable.getSelectedRows();
    if (selectedRows.length > 0)
    {
      try
      {
        java.util.List<OpticalCameraRectangle> selectedOpticalCameraRectangles = _pspRectangleTableModel.getSelectedOpticalCameraRectangles(selectedRows);
        java.util.List<OpticalCameraRectangle> opticalCameraRectangleWithPspPoints = new ArrayList<>();
        java.util.List<OpticalCameraRectangle> opticalCameraRectangleWithoutPspPoints = new ArrayList<>();
        for (OpticalCameraRectangle opticalCameraRectangle : selectedOpticalCameraRectangles)
        {
          if (opticalCameraRectangle.hasPanelCoordinateList())
          {
            opticalCameraRectangleWithPspPoints.add(opticalCameraRectangle);
          }
          else
          {
            opticalCameraRectangleWithoutPspPoints.add(opticalCameraRectangle);
          }
        }
        if (opticalCameraRectangleWithoutPspPoints.size() > 0 && enable == true)
        {
          if (_mainUI.beforeGenerateSurfaceMapping(this) == false)
          {
            return;
          }
          if (_mainUI.isPanelInLegalState() == false)
          {
            return;
          }
        }
        if (enable == true) // if user enables rectangles, prompts user to edit point for rectangles that do not have PSP points.
        {
          for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangleWithoutPspPoints)
          {
            if (editSurfaceMapPoints(opticalCameraRectangle) == false)
            {
              // user canceled edit point.
              break;
            }
            if (opticalCameraRectangle.hasPanelCoordinateList())
            {
              opticalCameraRectangleWithPspPoints.add(opticalCameraRectangle);
            }
          }
        }
        _commandManager.trackState(getCurrentUndoState());
        _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_TOGGLE_OPTICAL_CAMERA_RECTANGLE_KEY"));
        for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangleWithPspPoints)
        {
          BooleanRef isEnabled = new BooleanRef(enable);
          enableOpticalCameraRectangle(opticalCameraRectangle, isEnabled.getValue());
        }
        _commandManager.endCommandBlock();
        // Need to call fireTableDataChanged() to ensure the table's data are updated.
        // Reselect the table rows after calling fireTableDataChanged().
        int[] pspRectangleTableSelectedRows = _pspRectangleTable.getSelectedRows();
        _pspRectangleTableModel.fireTableDataChanged();
        _pspRectangleTableModel.sortColumn(0, true);
        if (pspRectangleTableSelectedRows.length > 0)
        {
          _pspRectangleTable.setSelectedRows(pspRectangleTableSelectedRows);
        }
        if (_isPanelBasedAlignment)
        {
          _testDev.getPanelGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
          _testDev.getPanelGraphicsEngineSetup().handleMultipleHighlightOpticalCameraRectangles(selectedOpticalCameraRectangles);
        }
        else
        {
          // to highlight the OpticalCameraRectangles of current board only.
          // remove the optical camera rectangles of other boards from the list.
          java.util.List<OpticalCameraRectangle> opticalCameraRectanglesOfOtherBoards = new ArrayList<>();
          for (OpticalCameraRectangle opticalCameraRectangle : selectedOpticalCameraRectangles)
          {
            if (opticalCameraRectangle.getBoard().equals(_currentBoard) == false)
              opticalCameraRectanglesOfOtherBoards.add(opticalCameraRectangle);
          }
          selectedOpticalCameraRectangles.removeAll(opticalCameraRectanglesOfOtherBoards);
          _testDev.getBoardGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_opticalRegion.getName());
          _testDev.getBoardGraphicsEngineSetup().handleMultipleHighlightOpticalCameraRectangles(selectedOpticalCameraRectangles);
        }
        // Added by Lee Herng, 2014-05-08
        updateMeshTriangleOnCAD();
        //update optical rectangle used by each reconstruction region
        allocateOpticalCameraRectangleToComponents();
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                ex,
                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                true);
      }
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void enableOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle, boolean enable)
  {
    BooleanRef isEnabled = new BooleanRef(enable);
    try
    {
      if (_isPanelBasedAlignment)
        _commandManager.execute(new SurfaceMapPanelToggleOpticalCameraRectangleCommand(opticalCameraRectangle, isEnabled));
      else
        _commandManager.execute(new SurfaceMapBoardToggleOpticalCameraRectangleCommand(opticalCameraRectangle, isEnabled));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
              ex.getLocalizedMessage(),
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
              true);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void invalidateChecksum()
  {
    Assert.expect(_project != null);
    
    _project.getTestProgram().invalidateAlignmentOpticalCheckSum();
    _project.getTestProgram().invalidateOpticalCheckSum();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void updateComponentListForBoardBased()
  {
    java.util.List<com.axi.v810.business.panelDesc.Component> componentToBeRemovedList = new ArrayList<>();
    for (com.axi.v810.business.panelDesc.Component component : getAllComponents())
    {
      boolean isComponentBoardSelected = false;
      for (Board board : getBoardsToApplyOpticalCameraRectangles())
      {
        if (component.getBoard().getName().equalsIgnoreCase(board.getName()))
        {
          isComponentBoardSelected = true;
          break;
        }
      }
      if (isComponentBoardSelected == false)
      {
        componentToBeRemovedList.add(component);
      }
    }
    removeComponents(componentToBeRemovedList);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void changeGraphicsToBoard(Board board)
  {
    double zoomFactor = _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().getCurrentZoomFactor();
    if (_testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard() != null)
    {
      if (board.getName().equals(_testDev.getBoardGraphicsEngineSetup().getCurrentDisplayBoard().getName()) == false)
      {
        _testDev.changeGraphicsToBoard(board);
        _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setZoom(zoomFactor);
      }
    }
    else
    {
      _testDev.changeGraphicsToBoard(board);
      _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine().setZoom(zoomFactor);
    }
  }
  
  /**
   * Perform alignment before running Surface Map.
   * @author Ying-Huan.Chu
   */
  private void performAlignmentIfNecessary(final OpticalCameraRectangle opticalCameraRectangle,
                                           final boolean isPanelBasedAlignment)
  {
    _isAlignmentSuccess = false;
    final BooleanLock alignmentConfirmationLock = new BooleanLock(false);
    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _testExecution.performManualAlignmentForSurfaceMappping(_project.getTestProgram(), opticalCameraRectangle, isPanelBasedAlignment);
          _isAlignmentSuccess = true;
        }
        catch (final XrayTesterException ex)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              if (ex != null)
              {
                if (ex instanceof AlignmentAlgorithmFailedToLocatePadsBusinessException
                    || ex instanceof AlignmentAlgorithmFailedToCalculateTransformBusinessException
                    || ex instanceof AlignmentAlgorithmResultOutOfBoundsBusinessException
                    || ex instanceof AlignmentAlgorithmLocatedPadsTooCloseToEdgeException)
                {
                  AlignmentFailedDialog alignmentFailedDialog = new AlignmentFailedDialog(_mainUI,
                                                                StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                                                _project,
                                                                ex);
                  SwingUtils.centerOnComponent(alignmentFailedDialog, _mainUI);
                  alignmentFailedDialog.setVisible(true);
                  _isAlignmentSuccess = false;
                }
                else
                {
                  MessageDialog.showErrorDialog(_mainUI, ex.getMessage(), StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"), true);
                  _isAlignmentSuccess = true;
                }
              }
              _mainUI.closeAllDialogs();
              _mainUI.enableMenuAndToolBars();
              _testDev.enableTaskPanelNavigation();
              _mainUI.changeLightStackToStandby();
            }
          });
        }
        finally
        {
          alignmentConfirmationLock.setValue(true);
        }
      }
    });
    try
    {
      alignmentConfirmationLock.waitUntilTrue();
    }
    catch (InterruptedException ex1)
    {
      Assert.logException(ex1);
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private Board getSelectedBoard(Rectangle rectangleInNanometers)
  {
    Assert.expect(rectangleInNanometers != null);
    
    Board selectedBoard = null;
    for(Board board : _project.getPanel().getBoards())
    {
      if (board.getShapeRelativeToPanelInNanoMeters().intersects(rectangleInNanometers))
      {
        selectedBoard = board;
        break;
      }
    }
    
    // If we fail to get intersected board, default to first board
    if (selectedBoard == null)
      selectedBoard = _project.getPanel().getBoards().get(0);
    
    return selectedBoard;
  }
  
  /**
   * @author Ying-Huan.Chu
   * @param board
   * @return true if Component table has component(s) that are from the board.
   */
  public boolean hasComponents(Board board)
  {
    if (getBoardComponentList(board).isEmpty())
      return false;
    return true;
  }
  
  /**
   * All components that will be added to OpticalRegion must pass through this checking.
   * @author Ying-Huan.Chu
   */
  public boolean isComponentUsableInSurfaceMap(com.axi.v810.business.panelDesc.Component component)
  {
    if (component.getComponentTypeSettings().areAllPadTypesNotTestable() == false && component.getComponentTypeSettings().isLoaded())
      return true;
    else
      return false;
  }
  
  /**
   * Prompts error message dialog box, notify user that the system had failed to retrieve height map.
   * @author Ying-Huan.Chu
   */
  public void handleUnableToRetrieveHeightMapError()
  {
    PspModuleEnum pspModuleEnum = _surfaceMapping.getPspModuleEnum();
    LocalizedString errorMsg = new LocalizedString("PSP_NO_HEIGHT_MAP_ERROR_KEY", 
                                                   new Object[]
                                                   {
                                                     pspModuleEnum.getId()
                                                   });
    MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                  StringLocalizer.keyToString(errorMsg),
                                  StringLocalizer.keyToString("PSP_SURFACE_MAP_ERROR_KEY"),
                                  true);
  }
  
  /**
   * Delete the temporary optical image generated by Surface Map.
   * @author Jack Hwee
   */
  private void deleteTempOpticalImage()
  {
    String imageFullPath = FileName.getAlignmentOpticalImageFullPath("temp", 0);
    if (FileUtil.exists(imageFullPath))
    {
      try
      {
        FileUtil.delete(imageFullPath);
      }
      catch (CouldNotDeleteFileException ex)
      {
        Logger.getLogger(SurfaceMapping.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
  
  /**
   * Get the width of rectangle in pixel relative to panel's width in real life.
   * @author Ying-Huan.Chu
   */
  private int getRectangleWidthRelativeToPanelInPixel()
  {
    if (PspEngine.isVersion1())
    {
      return OPTICAL_CAMERA_MAX_WIDTH_IN_PIXELS;
    }
    else
    {
      PanelRectangle panelRectangleInNanometer = new PanelRectangle(_project.getPanel().getShapeInNanoMeters()); // Panel size in real life in nanometer.
      Rectangle panelRectangleInPixel = _testDev.getPanelGraphicsEngineSetup().getPanelRectangleInPixel(); // Panel size in GraphicsEngine in pixel.
      PIXEL_WIDTH_TO_PANEL_WIDTH_RATIO = panelRectangleInPixel.getWidth() / panelRectangleInNanometer.getRectangle2D().getWidth(); 
      if (_project.getPanel().getPspSettings().isLargerFOV())
      {
        double width = PspHardwareEngine.getInstance().getLargerFovActualImageWidthInNanometer() * PIXEL_WIDTH_TO_PANEL_WIDTH_RATIO;
        return ((int)width);
      }
      else
      {
        double width = PspHardwareEngine.getInstance().getActualImageWidthInNanometer() * PIXEL_WIDTH_TO_PANEL_WIDTH_RATIO;
        return ((int)width);
      }
    }
  }
  
  /**
   * Get the height of rectangle in pixel relative to panel's height in real life.
   * @author Ying-Huan.Chu
   */
  private int getRectangleHeightRelativeToPanelInPixel()
  {
    if (PspEngine.isVersion1())
    {
      return OPTICAL_CAMERA_MAX_HEIGHT_IN_PIXELS;
    }
    else
    {
      PanelRectangle panelRectangleInNanometer = new PanelRectangle(_project.getPanel().getShapeInNanoMeters()); // Panel size in real life in nanometer.
      Rectangle panelRectangleInPixel = _testDev.getPanelGraphicsEngineSetup().getPanelRectangleInPixel(); // Panel size in GraphicsEngine in pixel.
      PIXEL_HEIGHT_TO_PANEL_HEIGHT_RATIO = panelRectangleInPixel.getHeight() / panelRectangleInNanometer.getHeight(); 
      if (_project.getPanel().getPspSettings().isLargerFOV())
      {
        double height = PspHardwareEngine.getInstance().getLargerFovActualImageHeightInNanometer() * PIXEL_HEIGHT_TO_PANEL_HEIGHT_RATIO;
        return ((int)height);
      }
      else
      {
        double height = PspHardwareEngine.getInstance().getActualImageHeightInNanometer() * PIXEL_HEIGHT_TO_PANEL_HEIGHT_RATIO;
        return ((int)height);
      }
    }
  }
  
  /**
   * Function to execute when 'Use Larger FOV' checkbox is checked.
   * @author Ying-Huan.Chu
   */
  private void useLargerFOVCheckbox_actionPerformed()
  {
    boolean isSetUseLargerFovCommandExecuted = false;
    _commandManager.trackState(getCurrentUndoState());
    _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SET_USE_LARGER_FOV_KEY"));
    try
    {
      // if user choose to delete previous rectangle.
      if (promptToDeletePreviousRectangles())
      {
        // execute the useLargerFov command.
        isSetUseLargerFovCommandExecuted = _commandManager.execute(new SurfaceMapSetUseLargerFovCommand(_project, _useLargerFovCheckbox.isSelected()));
      }
      else
      {
        // uncheck the checkbox as user chose not to delete the previous rectangle.
        _useLargerFovCheckbox.setSelected(!_useLargerFovCheckbox.isSelected());
      }
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
          ex.getLocalizedMessage(),
          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
          true);
    }
    finally
    {
      _commandManager.endCommandBlock();
      if (isSetUseLargerFovCommandExecuted)
      {
        // only do these updates if the command is successfully executed.
        refreshTablesData();
        updatePopulateRectangleOptions();
        if (_isPanelBasedAlignment)
          _testDev.getPanelGraphicsEngineSetup().clearDividedOpticalRegionList();
        else
          _testDev.getBoardGraphicsEngineSetup().clearDividedOpticalRegionList();
      }
    }
  }
  
  /**
   * This function updates the OpticalCameraSetting with the new image size and new cropImageOffsetXInPixel, reload the calibration data, and reinitialize the Optical Camera.
   * This function has to be called each time user changes from small FOV to Larger FOV and vice versa.
   * 
   * @author Ying-Huan.Chu
   * @param useLargerFov pass in true to update OpticalCamera setting with Larger FOV setting. Pass in false to update OpticalCamera setting with small FOV setting.
   */
  private void updateOpticalCameraSetting(final boolean useLargerFov)
  {
    //Ying-Huan.Chu - temporary commented Larger FOV feature for 5.7 release.
    /*if (XrayTester.getInstance().isSimulationModeOn() == false)
    {
      // Avoid updating the OpticalCameraSetting again if it's already in the correct setting as this process takes a few seconds.
      if (_surfaceMapping.isLargerFov() == useLargerFov)
        return;
    }
    
    final BusyCancelDialog busyDialog = new BusyCancelDialog(_mainUI,
                                                             StringLocalizer.keyToString("MMGUI_PSP_UPDATE_OPTICAL_CAMERA_SETTING_MESSAGE_KEY"),
                                                             StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                             true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, _mainUI);
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try 
        {
          _surfaceMapping.setUseLargerFov(useLargerFov);
        } 
        catch (XrayTesterException ex) 
        {
          MainMenuGui.getInstance().handleXrayTesterException(ex);
        }
        finally
        {
          busyDialog.dispose();
        }
      }
    });
    busyDialog.setVisible(true);*/
  }
  
  /**
   * @author Ying-Huan.Chu
   * @return true if Optical Calibration data exist. (Calibration0.txt and ReferenceRaw0.txt files)
   */
  private boolean checkIfOpticalCalibrationDataExists()
  {
    if (XrayTester.getInstance().isSimulationModeOn() == false)
    {
      boolean isLargerFov = _project.getPanel().isLargerFOV();
      
      String frontOpticalCalibrationDataFullPath = isLargerFov? FileName.getLargerFovFrontOpticalCalibrationDataFullPath() : FileName.getFrontOpticalCalibrationDataFullPath();
      String rearOpticalCalibrationDataFullPath = isLargerFov? FileName.getLargerFovRearOpticalCalibrationDataFullPath() : FileName.getRearOpticalCalibrationDataFullPath();

      String frontOpticalCalibrationReferenceRawDataFullPath = isLargerFov? FileName.getLargerFovFrontOpticalCalibrationReferenceRawDataFullPath(): FileName.getFrontOpticalCalibrationReferenceRawDataFullPath();
      String rearOpticalCalibrationReferenceRawDataFullPath = isLargerFov? FileName.getLargerFovRearOpticalCalibrationReferenceRawDataFullPath(): FileName.getRearOpticalCalibrationReferenceRawDataFullPath();

      // XCR-2796 Fail to run psp if system running on single PSP module connection
      boolean isOpticalCalibrationDataExist = false;
      if (_settingEnum.equals(PspSettingEnum.SETTING_BOTH))
      {
        isOpticalCalibrationDataExist = FileUtilAxi.exists(frontOpticalCalibrationDataFullPath) && 
                                        FileUtilAxi.exists(rearOpticalCalibrationDataFullPath) &&
                                        FileUtilAxi.exists(frontOpticalCalibrationReferenceRawDataFullPath) &&
                                        FileUtilAxi.exists(rearOpticalCalibrationReferenceRawDataFullPath);
      }
      else if (_settingEnum.equals(PspSettingEnum.SETTING_FRONT))
      {
        isOpticalCalibrationDataExist = FileUtilAxi.exists(frontOpticalCalibrationDataFullPath) && 
                                        FileUtilAxi.exists(frontOpticalCalibrationReferenceRawDataFullPath);
      }
      else if (_settingEnum.equals(PspSettingEnum.SETTING_REAR))
      {
        isOpticalCalibrationDataExist = FileUtilAxi.exists(rearOpticalCalibrationDataFullPath) && 
                                        FileUtilAxi.exists(rearOpticalCalibrationReferenceRawDataFullPath);
      }      
      return isOpticalCalibrationDataExist;
    }
    else
    {
      // We don't need calibration data to run Surface Map in Hardware Simulation mode.
      return true;
    }
  }
  
  /**
   * Prompts error message, notifying user that Optical Calibration data is needed before running Surface Map.
   * @author Ying-Huan.Chu
   */
  private void handleNoOpticalCalibrationDataError()
  {
    JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                  StringLocalizer.keyToString("PSP_NO_OPTICAL_CALIBRATION_DATA_ERROR_MESSAGE_KEY"),
                                  StringLocalizer.keyToString("PSP_NO_OPTICAL_CALIBRATION_DATA_ERROR_TITLE_KEY"),
                                  JOptionPane.ERROR_MESSAGE);
  }
  
  /**
   * This function will setup the allowable regions for the loaded panel / board.
   * eg 1: A short and small panel like NEPCON panel will have only 1 allowable region. 
   * eg 2: A short and wide panel like AGILENT panel will have 2 allowable regions as it requires both FRONT and REAR cameras to cover the whole panel.
   * eg 3: A long and wide panel like MAKALU panel will have 4 allowable regions.
   * 
   * @author Ying-Huan.Chu
   */
  private void setupAllowableRegion()
  {
    _allowableRegionToPspModuleEnumMap.clear();
    if (_project.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      PanelRectangle rightSectionOfLongPanel = _project.getPanel().getPanelSettings().getRightSectionOfLongPanel();
      if (_project.getPanel().getPanelSettings().isLongPanel())
      {
        PanelRectangle leftSectionOfLongPanel = _project.getPanel().getPanelSettings().getLeftSectionOfLongPanel();
        if (_project.getPanel().getPanelSettings().isOpticalLongPanel())
        {
          PanelRectangle bottomSectionOfOpticalLongPanel = _project.getPanel().getPanelSettings().getBottomSectionOfOpticalLongPanel();
          PanelRectangle topSectionOfOpticalLongPanel = _project.getPanel().getPanelSettings().getTopSectionOfOpticalLongPanel();
          _allowableRegionToPspModuleEnumMap.put(rightSectionOfLongPanel.createIntersectingPanelRectangle(bottomSectionOfOpticalLongPanel).getRectangle2D().getBounds(), PspModuleEnum.FRONT);
          _allowableRegionToPspModuleEnumMap.put(rightSectionOfLongPanel.createIntersectingPanelRectangle(topSectionOfOpticalLongPanel).getRectangle2D().getBounds(), PspModuleEnum.REAR);
          _allowableRegionToPspModuleEnumMap.put(leftSectionOfLongPanel.createIntersectingPanelRectangle(bottomSectionOfOpticalLongPanel).getRectangle2D().getBounds(), PspModuleEnum.FRONT);
          _allowableRegionToPspModuleEnumMap.put(leftSectionOfLongPanel.createIntersectingPanelRectangle(topSectionOfOpticalLongPanel).getRectangle2D().getBounds(), PspModuleEnum.REAR);
        }
        else
        {
          _allowableRegionToPspModuleEnumMap.put(rightSectionOfLongPanel.getRectangle2D().getBounds(), PspModuleEnum.FRONT);
          _allowableRegionToPspModuleEnumMap.put(leftSectionOfLongPanel.getRectangle2D().getBounds(), PspModuleEnum.FRONT);
        }
      }
      else
      {
        if (_project.getPanel().getPanelSettings().isOpticalLongPanel())
        {
          PanelRectangle bottomSectionOfOpticalLongPanel = _project.getPanel().getPanelSettings().getBottomSectionOfOpticalLongPanel();
          PanelRectangle topSectionOfOpticalLongPanel = _project.getPanel().getPanelSettings().getTopSectionOfOpticalLongPanel();
          _allowableRegionToPspModuleEnumMap.put(rightSectionOfLongPanel.createIntersectingPanelRectangle(bottomSectionOfOpticalLongPanel).getRectangle2D().getBounds(), PspModuleEnum.FRONT);
          _allowableRegionToPspModuleEnumMap.put(rightSectionOfLongPanel.createIntersectingPanelRectangle(topSectionOfOpticalLongPanel).getRectangle2D().getBounds(), PspModuleEnum.REAR);
        }
        else
        {
          _allowableRegionToPspModuleEnumMap.put(rightSectionOfLongPanel.getRectangle2D().getBounds(), PspModuleEnum.FRONT);
        }
      }
    }
    else
    {
      for (Board board : _project.getPanel().getBoards())
      {
        PanelRectangle rightSectionOfLongBoard = board.getRightSectionOfLongBoard();
        if (board.isLongBoard())
        {
          PanelRectangle leftSectionOfLongBoard = board.getLeftSectionOfLongBoard();
          if (board.isOpticalLongBoard() || _project.getPanel().getPanelSettings().isOpticalLongPanel())
          {
            PanelRectangle bottomSectionOfOpticalLongPanel = _project.getPanel().getPanelSettings().getBottomSectionOfOpticalLongPanel();
            PanelRectangle topSectionOfOpticalLongPanel = _project.getPanel().getPanelSettings().getTopSectionOfOpticalLongPanel();
            _allowableRegionToPspModuleEnumMap.put(rightSectionOfLongBoard.createIntersectingPanelRectangle(bottomSectionOfOpticalLongPanel).getRectangle2D().getBounds(), PspModuleEnum.FRONT);
            _allowableRegionToPspModuleEnumMap.put(rightSectionOfLongBoard.createIntersectingPanelRectangle(topSectionOfOpticalLongPanel).getRectangle2D().getBounds(), PspModuleEnum.REAR);
            _allowableRegionToPspModuleEnumMap.put(leftSectionOfLongBoard.createIntersectingPanelRectangle(bottomSectionOfOpticalLongPanel).getRectangle2D().getBounds(), PspModuleEnum.FRONT);
            _allowableRegionToPspModuleEnumMap.put(leftSectionOfLongBoard.createIntersectingPanelRectangle(topSectionOfOpticalLongPanel).getRectangle2D().getBounds(), PspModuleEnum.REAR);
          }
          else
          {
            _allowableRegionToPspModuleEnumMap.put(rightSectionOfLongBoard.getRectangle2D().getBounds(), PspModuleEnum.FRONT);
            _allowableRegionToPspModuleEnumMap.put(leftSectionOfLongBoard.getRectangle2D().getBounds(), PspModuleEnum.FRONT);
          }
        }
        else
        {
          if (board.isOpticalLongBoard() || _project.getPanel().getPanelSettings().isOpticalLongPanel())
          {
            PanelRectangle bottomSectionOfOpticalLongPanel = _project.getPanel().getPanelSettings().getBottomSectionOfOpticalLongPanel();
            PanelRectangle topSectionOfOpticalLongPanel = _project.getPanel().getPanelSettings().getTopSectionOfOpticalLongPanel();
            _allowableRegionToPspModuleEnumMap.put(rightSectionOfLongBoard.createIntersectingPanelRectangle(bottomSectionOfOpticalLongPanel).getRectangle2D().getBounds(), PspModuleEnum.FRONT);
            _allowableRegionToPspModuleEnumMap.put(rightSectionOfLongBoard.createIntersectingPanelRectangle(topSectionOfOpticalLongPanel).getRectangle2D().getBounds(), PspModuleEnum.REAR);
          }
          else
          {
            _allowableRegionToPspModuleEnumMap.put(rightSectionOfLongBoard.getRectangle2D().getBounds(), PspModuleEnum.FRONT);
          }
        }
      }
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static Map<Rectangle, PspModuleEnum> getAllowableRegionToPspModuleEnumMap()
  {
    return _allowableRegionToPspModuleEnumMap;
  }
  
  /**
   * Save PSP components when user click save button.
   * @author Kok Chun, Tan
   */
  public void savePspComponents()
  {
    if (_isLiveViewMode == false && _isGoIntoPspAutoPopulateBefore)
    {
      java.util.List<com.axi.v810.business.panelDesc.Component> components = new ArrayList<>();
      java.util.List<com.axi.v810.business.panelDesc.Component> addedComponents = new ArrayList<>();
      java.util.List<com.axi.v810.business.panelDesc.Component> deletedComponents = new ArrayList<>();
      _project = Project.getCurrentlyLoadedProject();
      _project.getPanel().setIsAutomaticallyIncludeAllComponents(_isAutoIncludedAllComponents);
      components = getCurrentPspComponents();
      
      addedComponents.addAll(_temporaryComponents);
      addedComponents.removeAll(components);
      
      deletedComponents.addAll(components);
      deletedComponents.removeAll(_temporaryComponents);

      if (addedComponents.isEmpty() == false)
      {
        addComponents(addedComponents);
      }
      
      if (deletedComponents.isEmpty() == false)
      {
        deleteComponents(deletedComponents);
      }
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void updateComponentsList()
  {
    java.util.List<com.axi.v810.business.panelDesc.Component> components = new ArrayList<>();
    components = getCurrentPspComponents();
    
    if (_autoIncludeWholePanelComponentRadioButton.isSelected())
    {
      _isAutoIncludedAllComponents = true;
    }
    else
    {
      _isAutoIncludedAllComponents = false;
    }
    
    _temporaryComponents.clear();
    _temporaryComponents.addAll(getAllComponents());
    
    if (components.size() != _temporaryComponents.size())
    {
      _projectObservable.stateChanged(this, null, _temporaryComponents, PanelSurfaceMapEventEnum.ADD_SURFACE_MAP_COMPONENT);
    }
  }
  
  /**
   * This method only called when load recipe.
   * This is used to disable to save psp components.
   * @author Kok Chun, Tan
   */
  public void setIsGoIntoPspAutoPopulateBefore(boolean flag)
  {
    _isGoIntoPspAutoPopulateBefore = flag;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private java.util.List<com.axi.v810.business.panelDesc.Component> getCurrentPspComponents()
  {
    _project = Project.getCurrentlyLoadedProject();
    boolean isPanelBasedAlignment = _project.getPanel().getPanelSettings().isPanelBasedAlignment();
    java.util.List<com.axi.v810.business.panelDesc.Component> components = new ArrayList<>();
    if (isPanelBasedAlignment)
    {
      if (_project.getPanel().hasPanelSurfaceMapSettings())
      {
        for (OpticalRegion opticalRegion : _project.getPanel().getPanelSurfaceMapSettings().getOpticalRegions())
        {
          components.addAll(opticalRegion.getComponents());
        }
      }
    }
    else
    {
      for (Board board : _project.getPanel().getBoards())
      {
        if (board.hasBoardSurfaceMapSettings())
        {
          BoardSurfaceMapSettings boardSurfaceMapSettings = board.getBoardSurfaceMapSettings();
          for (OpticalRegion boardOpticalRegion : boardSurfaceMapSettings.getOpticalRegions())
          {
            components.addAll(boardOpticalRegion.getComponents(board));
          }
        }
      }
    }
    return components;
  }
  
  /**
   * When the 'Copy Rectangles' button is clicked, prompts the Copy Rectangle dialogue box.
   * @author Ying-Huan.Chu
   */
  private void copyRectanglesToBoardButton_actionPerformed()
  {
    if (getBoardsWithOpticalCameraRectangles().isEmpty())
    {
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString("PSP_COPY_RECTANGLE_AT_LEAST_ONE_BOARD_MUST_CONTAIN_SAVED_RECTANGLE_KEY"),
                                    StringLocalizer.keyToString("PSP_COPY_SAVED_RECTANGLES_TO_REST_OF_THE_BOARDS_TITLE_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      return;
    }
    final SurfaceMapCopyRectanglesToBoardDialog copyRectanglesToBoardDialog = new SurfaceMapCopyRectanglesToBoardDialog(_mainUI, StringLocalizer.keyToString("PSP_COPY_RECTANGLES_BUTTON_KEY"));
    SwingUtils.centerOnComponent(copyRectanglesToBoardDialog, _mainUI);
    copyRectanglesToBoardDialog.setVisible(true);
    copyRectanglesToBoardDialog.addWindowListener(new WindowListener() 
    {
      public void windowClosed(WindowEvent e)
      {
        if (copyRectanglesToBoardDialog.isUserClickedOk())
        {
          copySelectedOpticalCameraRectangles(copyRectanglesToBoardDialog.getBoardToCopyFrom(), copyRectanglesToBoardDialog.getBoardsToCopyTo());
        }
      }
      public void windowOpened(WindowEvent e) { }
      public void windowClosing(WindowEvent e) { }
      public void windowIconified(WindowEvent e) { }
      public void windowDeiconified(WindowEvent e) { }
      public void windowActivated(WindowEvent e) { }
      public void windowDeactivated(WindowEvent e) { }
    });
  }
  
  /**
   * @author Ying-Huan.Chu
   * @return a list of Boards with OpticalCameraRectangle(s)
   */
  public java.util.List<Board> getBoardsWithOpticalCameraRectangles()
  {
    java.util.List<Board> boardsWithOpticalCameraRectangles = new ArrayList<>();
    _project = Project.getCurrentlyLoadedProject();
    for (Board board : _project.getPanel().getBoards())
    {
      if (board.hasBoardSurfaceMapSettings())
      {
        BoardSurfaceMapSettings boardSurfaceMapSettings = board.getBoardSurfaceMapSettings();
        if (boardSurfaceMapSettings.hasOpticalRegion())
        {
          if (boardSurfaceMapSettings.getOpticalRegion().hasOpticalCameraRectangles(board))
          {
            boardsWithOpticalCameraRectangles.add(board);
          }
        }
      }
    }
    return boardsWithOpticalCameraRectangles;
  }

  /**
   * Copy selected OpticalCameraRectangles(blue) from a board to other board(s).
   * @author Ying-Huan.Chu
   * @param boardToCopyFrom board to copy OpticalCameraRectangles from.
   * @param boardsToCopyTo board(s) to copy OpticalCameraRectangles to.
   */
  private void copySelectedOpticalCameraRectangles(Board boardToCopyFrom, java.util.List<Board> boardsToCopyTo)
  {
    _commandManager.trackState(getCurrentUndoState());
    _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PSP_ADD_OPTICAL_REGION_KEY"));
    for (Board boardToCopyTo : boardsToCopyTo)
    {
      // if the board (to copy rectangles to) does not have surface map settings, create a new one for it.
      if (boardToCopyTo.hasBoardSurfaceMapSettings() == false)
        boardToCopyTo.setBoardSurfaceMapSettings(new BoardSurfaceMapSettings());

      BoardSurfaceMapSettings boardSurfaceMapSettings = boardToCopyTo.getBoardSurfaceMapSettings();
      // clear the OpticalRegion in boardToCopyTo.
      deleteBoardOpticalRegion(boardToCopyTo);
      // continue to copy the optical region from boardToCopyFrom.
      for (OpticalRegion opticalRegionToCopyFrom : boardToCopyFrom.getBoardSurfaceMapSettings().getOpticalRegions())
      {
        OpticalRegion opticalRegionToCopyTo = new OpticalRegion();
        Rectangle boardShapeInNanometers = boardToCopyTo.getShapeRelativeToPanelInNanoMeters().getBounds();
        opticalRegionToCopyTo.setRegion(boardShapeInNanometers.x, boardShapeInNanometers.y, boardShapeInNanometers.width, boardShapeInNanometers.height);
        opticalRegionToCopyTo.setBoard(boardToCopyTo);
        // have to call changeGraphicsToBoard function to get the correct AffineTransform for board optical region.
        changeGraphicsToBoard(boardToCopyTo);
        
        // translate all OpticalCameraRectangle and PointPanelCoordinate positions and add the OpticalCameraRectangles to the OpticalRegion.
        for (OpticalCameraRectangle opticalCameraRectangle : opticalRegionToCopyFrom.getOpticalCameraRectangles(boardToCopyFrom))
        {
          // Create new OpticalCameraRectangle relative to board.
          OpticalCameraRectangle opticalCameraRectangleToBeAdded = _testDev.getBoardGraphicsEngineSetup().getOpticalCameraRectangleRelativeToBoard(boardToCopyTo, opticalCameraRectangle);
          opticalCameraRectangleToBeAdded.setInspectableBool(opticalCameraRectangle.getInspectableBool());
          opticalCameraRectangleToBeAdded.setZHeightInNanometer(opticalCameraRectangle.getZHeightInNanometer());
          // Kok Chun, Tan - XCR2490 - 24/2/2014
          // Check the optical camera used is enable or not. If not enable, skip the rectangle.
          if (_surfaceMapping.isOpticalCameraEnable(opticalCameraRectangleToBeAdded) == false)
          {
            continue;
          }
          
          Map<String, PanelCoordinate> pointPanelCoordinateMap = opticalRegionToCopyFrom.getBoardNameToPointPanelCoordinateMap().get(boardToCopyFrom);
          // add all the SurfaceMapPoint from the OpticalRegion to be copied from to the new OpticalRegion.
          for (Map.Entry<String, PanelCoordinate> pointPanelCoordinateMapEntry : pointPanelCoordinateMap.entrySet())
          {
            PanelCoordinate pointPanelCoordinateInNanometer = pointPanelCoordinateMapEntry.getValue();
            // Create new SurfaceMapPoint relative to board.
            if (opticalCameraRectangle.getPanelCoordinateList().contains(pointPanelCoordinateInNanometer))
            {
              PanelCoordinate translatedPointPanelCoordinateInNanometer = _testDev.getBoardGraphicsEngineSetup().getSurfaceMapPointInPanelCoordinateRelativeToBoard(boardToCopyFrom, boardToCopyTo, pointPanelCoordinateInNanometer);
              opticalRegionToCopyTo.addSurfaceMapPoints(boardToCopyTo, (int)translatedPointPanelCoordinateInNanometer.getX(), (int)translatedPointPanelCoordinateInNanometer.getY());
              opticalCameraRectangleToBeAdded.addPointPanelCoordinate(translatedPointPanelCoordinateInNanometer.getX(), 
                                                                      translatedPointPanelCoordinateInNanometer.getY());
            }
          }
          // After adding the SurfaceMapPoint, add the OpticalCameraRectangle to the OpticalRegion.
          opticalRegionToCopyTo.addBoardOpticalCameraRectangle(boardToCopyTo, opticalCameraRectangleToBeAdded);
        }
        // Add the components to the OpticalRegion
        for (com.axi.v810.business.panelDesc.Component component : opticalRegionToCopyFrom.getComponents(boardToCopyFrom))
        {
          if (boardToCopyTo.hasComponent(component.toString()))
          {
            com.axi.v810.business.panelDesc.Component comp = boardToCopyTo.getComponent(component.toString());
            opticalRegionToCopyTo.addComponent(boardToCopyTo, comp);
          }
        }
        // Finally, add the OpticalRegion to the BoardSurfaceMapSettings and OpticalProgramGeneration.
        boolean isCheckLatestTestProgram = _project.isCheckLatestTestProgram();
        _project.setCheckLatestTestProgram(false);
        OpticalProgramGeneration.getInstance().addBoardOpticalRegion(_project.getTestProgram(), opticalRegionToCopyTo, boardToCopyTo);
        _project.setCheckLatestTestProgram(isCheckLatestTestProgram);
        try
        {
          _commandManager.execute(new SurfaceMapBoardAddOpticalRegionCommand(_project, boardSurfaceMapSettings, opticalRegionToCopyTo));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
            ex.getLocalizedMessage(),
            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
            true);
        }
      }
    }
    _commandManager.endCommandBlock();
    // update the List of Selected Board string and tables data.
    java.util.List<Board> selectedBoardList = new ArrayList<>();
    selectedBoardList.addAll(getBoardsToApplyOpticalCameraRectangles());
    for (Board boardToCopyTo : boardsToCopyTo)
    {
      if (getBoardsToApplyOpticalCameraRectangles().contains(boardToCopyTo) == false)
        selectedBoardList.add(boardToCopyTo);
    }
    setBoardsToApplyOpticalCameraRectangle(selectedBoardList);
    refreshTablesData();
  }
  
  /**
   * Pops up a dialogue box, asking user if he/she wants to copy saved OpticalCameraRectangles from current board to the rest of the board.
   * @author Ying-Huan.Chu
   * @param boardToCopyFrom board to copy OpticalCameraRectangles from.
   * @param boardsToCopyTo board(s) to copy OpticalCameraRectangles to.
   */
  private boolean promptToCopyRectanglesToBoards(Board boardToCopyFrom, java.util.List<Board> boardsToCopyTo)
  {
    int status = JOptionPane.showConfirmDialog(_mainUI, 
                                               StringLocalizer.keyToString("PSP_COPY_SAVED_RECTANGLES_TO_REST_OF_THE_BOARDS_MESSAGE_KEY"),
                                               StringLocalizer.keyToString("PSP_COPY_SAVED_RECTANGLES_TO_REST_OF_THE_BOARDS_TITLE_KEY"), 
                                               JOptionPane.YES_NO_OPTION, 
                                               JOptionPane.QUESTION_MESSAGE);

    if (status == JOptionPane.YES_OPTION)
    {
      _commandManager.endCommandBlock(); // end the addOpticalRegion command block in runAutoLiveViewButtonForBoardBased_actionPerformed() function.
      copySelectedOpticalCameraRectangles(boardToCopyFrom, boardsToCopyTo);
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * Delete a list of components.
   * @author Kok Chun, Tan
   */
  private void deleteComponents(java.util.List<com.axi.v810.business.panelDesc.Component> components)
  {
    _project = Project.getCurrentlyLoadedProject();
    _commandManager.trackState(getCurrentUndoState());
    _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_DELETE_COMPONENT_KEY"));
    try
    {
      if (_isPanelBasedAlignment)
      {
        PanelSurfaceMapSettings panelSurfaceMapSettings = _project.getPanel().getPanelSurfaceMapSettings();
        if (panelSurfaceMapSettings.hasOpticalRegion())
        {
          _commandManager.execute(new SurfaceMapDeleteComponentListCommand(panelSurfaceMapSettings.getOpticalRegion(), components, true));
        }
      }
      else
      {
        for (Board board : _project.getPanel().getBoards())
        {
          if (board.hasBoardSurfaceMapSettings())
          {
            if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
            {
              OpticalRegion opticalRegion = board.getBoardSurfaceMapSettings().getOpticalRegion();
              _commandManager.execute(new SurfaceMapDeleteComponentListCommand(opticalRegion, components, false));
            }
          }
        }
      }
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
        ex.getLocalizedMessage(),
        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
        true);
    }
    finally
    {
      _commandManager.endCommandBlock();
    }
  }

  /**
   * @author Ying-Huan.Chu
   */
  private void setBoardOpticalCameraRectanglesEnabled(Board board, boolean enabled)
  {
    Assert.expect(board != null);
    
    if (board.hasBoardSurfaceMapSettings())
    {
      if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
      {
        for (OpticalRegion opticalRegion : board.getBoardSurfaceMapSettings().getOpticalRegions())
        {
          for (OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getOpticalCameraRectangles(board))
          {
            opticalCameraRectangle.setInspectableBool(enabled);
          }
        }
      }
    }
    _pspRectangleTableModel.fireTableDataChanged();
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void highlightOpticalCameraRectangleUsedBasedOnComponents(com.axi.v810.business.panelDesc.Component component)
  {
    // Kok Chun, Tan - XCR-3506 - no need to highlight if this component is different mag.
    if (SurfaceMapping.getInstance().isProceedToAllocateRectangle(component) == false)
      return;
    
    //optical camera rectangle used for selected component
    java.util.List<OpticalCameraRectangle> opticalRectangleRectangles = getOpticalCameraRectanglesUsedBasedOnComponent(component);
    if (_isPanelBasedAlignment)
    {
      _testDev.getPanelGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
      _testDev.getPanelGraphicsEngineSetup().setHighlightedOpticalCameraRectangleList(opticalRectangleRectangles);
    }
    else
    {
      if (_currentBoard.hasBoardSurfaceMapSettings())
      {
        if (_currentBoard.getBoardSurfaceMapSettings().hasOpticalRegion())
        {
          _testDev.getBoardGraphicsEngineSetup().clearHighlightedOpticalCameraRectangleList();
          _testDev.getBoardGraphicsEngineSetup().handleMultipleHighlightOpticalCameraRectanglesForCurrentBoard(opticalRectangleRectangles);
          _testDev.getBoardGraphicsEngineSetup().handleSelectedOpticalCameraRectangleWithGivenRegion(_currentBoard.getBoardSurfaceMapSettings().getOpticalRegion().getName());
        }
      }
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private java.util.List<OpticalCameraRectangle> getOpticalCameraRectanglesUsedBasedOnComponent(com.axi.v810.business.panelDesc.Component component)
  {
    Assert.expect(component != null);
    
    _project = Project.getCurrentlyLoadedProject();
    java.util.List<OpticalCameraRectangle> opticalCameraRectanglesUsed = new ArrayList<OpticalCameraRectangle> ();
    java.util.List<String> opticalCameraRectangleNamesUsed = new ArrayList<String> ();
    java.util.List<OpticalCameraRectangle> opticalCameraRectangleList = new ArrayList<OpticalCameraRectangle> ();
    java.util.List<MeshTriangle> meshTriangles = new ArrayList<MeshTriangle> ();
    OpticalRegion opticalRegion = null;
    java.util.List<com.axi.v810.business.testProgram.ReconstructionRegion> inspectionRegions = component.getReconstructionRegionList(_project.getTestProgram());
    boolean hasOpticalRegion = false;
    
    if (_isPanelBasedAlignment)
    {
      if (_project.getPanel().hasPanelSurfaceMapSettings())
      {
        if (_project.getPanel().getPanelSurfaceMapSettings().hasOpticalRegion())
        {
          hasOpticalRegion = true;
          opticalRegion = _project.getPanel().getPanelSurfaceMapSettings().getOpticalRegion();
          meshTriangles = opticalRegion.getMeshTriangles();
          opticalCameraRectangleList = opticalRegion.getEnabledOpticalCameraRectangles();
        }
      }
    }
    else
    {
      Board board = component.getBoard();
      if (board.hasBoardSurfaceMapSettings())
      {
        if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
        {
          hasOpticalRegion = true;
          opticalRegion = board.getBoardSurfaceMapSettings().getOpticalRegion();
          meshTriangles = opticalRegion.getMeshTriangles();
          opticalCameraRectangleList = opticalRegion.getEnabledBoardOpticalCameraRectangles(board);
        }
      }
    }
    
    if (hasOpticalRegion == false)
    {
      return opticalCameraRectanglesUsed;
    }
    
    // used mesh triangle
    if (meshTriangles.isEmpty() == false && _project.getPanel().isUseMeshTriangle())
    {
      for (com.axi.v810.business.testProgram.ReconstructionRegion inspectionRegion : inspectionRegions)
      {
        for (OpticalCameraRectangle ocr : inspectionRegion.getMeshTriangle().getOpticalCameraRectangles())
        {
          if (opticalCameraRectangleNamesUsed.contains(ocr.getName()) == false)
          {
            opticalCameraRectangleNamesUsed.add(ocr.getName());
            opticalCameraRectanglesUsed.add(ocr);
          }
        }
      }
    }
    // used nearest rectangle
    else if (opticalCameraRectangleList.isEmpty() == false)
    {
      for (com.axi.v810.business.testProgram.ReconstructionRegion inspectionRegion : inspectionRegions)
      {
        OpticalCameraRectangle ocr = inspectionRegion.getReferenceOpticalCameraRectangle();
          if (opticalCameraRectangleNamesUsed.contains(ocr.getName()) == false)
          {
            opticalCameraRectangleNamesUsed.add(ocr.getName());
            opticalCameraRectanglesUsed.add(ocr);
          }
      }
    }
    
    return opticalCameraRectanglesUsed;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void allocateOpticalCameraRectangleToComponents()
  {
    if (Project.isCurrentProjectLoaded() == false)
    {
      return;
    }
    
    final BusyCancelDialog busyDialog = new BusyCancelDialog(
      _mainUI,
      StringLocalizer.keyToString("PSP_UPDATE_DATA_KEY"),
      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
      true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, _mainUI);
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _surfaceMapping.allocateOpticalCameraRectangleToComponents(_project.getTestProgram());
        }
        finally
        {
          busyDialog.dispose();
        }
      }
    });
    busyDialog.setVisible(true);
  }
  
  /**
   * XCR-3650 Filtering component is not working after delete component
   * @author Cheah Lee Herng
   */
  private void sortPspSelectedComponentTable()
  {
    if (_pspSelectedComponentTable.getSortedColumnIndex() > 0)
      _pspSelectedComponentTableModel.sortColumn(_pspSelectedComponentTable.getSortedColumnIndex(), _pspSelectedComponentTable.isSortedColumnAscending());
    else
      _pspSelectedComponentTableModel.sortColumn(0, true);
  }
}
