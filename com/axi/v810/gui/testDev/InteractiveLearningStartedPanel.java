package com.axi.v810.gui.testDev;

import java.awt.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * <p>Title: InteractiveLearningStartedPanel</p>
 *
 * <p>Description: This class is one fo the panels used in Interactive Learning.</p>
 *
 * <p>It is the initial panel shown in case the learnign database takes a while to open. It is also the final
 * panel whichj is used to transition between specific algorithm panels.
 *
 * <p>Copyright: Copyright (c) 2008</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 * @version 1.0
 */
public class InteractiveLearningStartedPanel extends JPanel
{
  InitialTuningPanel _invoker;

  private JPanel _mainPanel;
  private JPanel _labelPanel;
  private JLabel _defaultLabel;

  /**
   * @author George Booth
   */
  public InteractiveLearningStartedPanel()
  {
    jbInit();
  }

  private void jbInit()
  {
    _mainPanel = new JPanel(new BorderLayout());
//    _mainPanel.setBorder(BorderFactory.createCompoundBorder(
//      new TitledBorder(BorderFactory.createEtchedBorder(
//        Color.white, new Color(165, 163, 151)),
//                       "Main Panel"),
//      BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    this.add(_mainPanel);
    _mainPanel.setLayout(new BorderLayout());

    _labelPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));
//    _labelPanel.setBorder(BorderFactory.createCompoundBorder(
//      new TitledBorder(BorderFactory.createEtchedBorder(
//        Color.white, new Color(165, 163, 151)),
//                       "Label Panel"),
//      BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _mainPanel.add(_labelPanel, BorderLayout.NORTH);

    _defaultLabel = new JLabel(StringLocalizer.keyToString("MMGUI_INTERACTIVE_LEARNING_INITIALIZING_KEY"));
    _labelPanel.add(_defaultLabel);

    setDefaultState();
  }

  /**
   * @author George Booth
   */
  public void setDefaultState()
  {
    repaint();
  }

  /**
   * @author George Booth
   */
  public void setUserInputState(InspectionEvent inspectionEvent)
  {
    InspectionEventEnum inspectionEventEnum = inspectionEvent.getInspectionEventEnum();
    if (inspectionEventEnum.equals(InspectionEventEnum.INTERACTIVE_LEARNING_STARTED))
    {
      InteractiveLearningStartedInspectionEvent learningEvent = (InteractiveLearningStartedInspectionEvent)inspectionEvent;

      learningEvent.decrementReferenceCount();
    }
    else
    {
      Assert.expect(false, "Wrong inspectionEventEnum with InteractiveLearningStartedInspectionEvent");
    }
  }

}
