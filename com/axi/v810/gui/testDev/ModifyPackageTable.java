package com.axi.v810.gui.testDev;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;
/**
 * @author wei-chin.chong
 */
public class ModifyPackageTable extends JSortTable
{
  public static final int _PACKAGE_INDEX = 0;
  public static final int _JOINT_TYPE_INDEX = 1;
  public static final int _JOINT_HEIGHT_INDEX = 2;

  private static final int _PACKAGE_UNDO_INDEX = 1;
  private ModifySubtypesPackageTableModel _packageTableModel;

  private NumericRenderer _jointHeightNumericRenderer = new NumericRenderer(2, JLabel.RIGHT);
  private NumericEditor _jointHeightNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2);

  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  
  /**
   * @param model
   */
  public ModifyPackageTable(ModifySubtypesPackageTableModel model)
  {
    Assert.expect(model != null);

    _packageTableModel = model;
    super.setModel(model);
  }

  /**
   * @return
   */
  public void setModel(ModifySubtypesPackageTableModel packageTableModel)
  {
    Assert.expect(packageTableModel != null);
    
    _packageTableModel = packageTableModel;
    super.setModel(_packageTableModel);
  }

  /**
   *
   */
  public void setupCellEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    columnModel.getColumn(_JOINT_TYPE_INDEX).setCellEditor(new JointTypeEnumCellEditor(new JComboBox(), _packageTableModel));

    columnModel.getColumn(_JOINT_HEIGHT_INDEX).setCellEditor(_jointHeightNumericEditor);
    columnModel.getColumn(_JOINT_HEIGHT_INDEX).setCellRenderer(_jointHeightNumericRenderer);    
  }

  /**
   * The decimal places to be displayed can change based on the measurement value that
   * is being displayed.  This method changes the renderers/editors to display the appropriate
   * number of decimal places.
   * @param enumSelectedUnits The project measurement units, selected by user
   * @author Laura Cormos
   */
  public void setDecimalPlacesToDisplay(MathUtilEnum enumSelectedUnits)
  {
    Assert.expect(enumSelectedUnits != null);

    int decimalPlaces = MeasurementUnits.getMeasurementUnitDecimalPlaces(enumSelectedUnits);

    _jointHeightNumericEditor.setDecimalPlaces(decimalPlaces);
    _jointHeightNumericRenderer.setDecimalPlacesToDisplay(decimalPlaces);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setValueAt(Object value, int row, int column)
  {
    int rows[] = getSelectedRows();
    int col = convertColumnIndexToModel(column);
    _commandManager.trackState(getCurrentUndoState());
    _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMP_PACKAGE_SET_JOINT_TYPE_KEY"));
    for(int i = 0; i < rows.length; i++)
    {
      _packageTableModel.setValueAt(value, rows[i], col);
    }
    _commandManager.endCommandBlock();
  }

  /**
   * @author Erica Wheatcroft
   */
  private UndoState getCurrentUndoState()
  {
    UndoState undoState = new UndoState();
    undoState.setModifySubtypesViewSelectIndex(_PACKAGE_UNDO_INDEX);
    return undoState;
  }
}
