/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.TestSubProgram;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.service.PointToPointMoveDefinitionTableModel;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

import org.jfree.chart.*;
/**
 *
 * @author chin-seong.kee
 */
public class ScanPathViewPanel extends JPanel implements Observer
{
  private MainMenuGui _mainUI = MainMenuGui.getInstance();
  private TestDev _testDev = null;
  private ImageManagerPanel _imagePanel;
  private TestDevPersistance _persistSettings;
  private TestSubProgram _testSubProgram;
  private Project _project = null;
  private boolean _updateData = false;

  private ScanPathPointEntryXPlot _plot;
  private JFreeChart _chart;
  private ChartPanel _chartContentPanel;
  
  private final boolean _SHOW_CHART_LEGEND = false;
  
  //private JPanel _scanPathChartPanel;
  private JPanel _testDevChartPanel;
  private ScanPathReviewChartPanel _scanPathReviewChartPanel;
  
  // every time a Renderer is selected the Observable will update any Observers to
  // tell them it was selected
  private GuiObservable _guiObservable = GuiObservable.getInstance();

  // listener for row table selection
  private ListSelectionListener _pointsSummaryTableListSelectionListener;

  // command manager for doing undos
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  private JPanel _contentPanel = new JPanel();
  private JPanel _scanPathReviewSummaryPanel = new JPanel();
  private BorderLayout _scanPathReviewPanelBorderLayout = new BorderLayout();
  private JScrollPane _scanPathReviewPanelScrollPane = new JScrollPane();
  private ScanPathReviewTable _scanPathReviewTable;
  private ScanPathReviewTableModel _scanPathReviewTableModel;
  private BorderLayout _contentPanelBorderLayout = new BorderLayout();
  private Border _mainBorder;
 
   // true if this panel is visible
  private boolean _active = false;

  private JSplitPane _centerSplitPane = new JSplitPane();

  private JPanel _detailsPanel = new JPanel();
  private BorderLayout _detailsPanelBorderLayout = new BorderLayout();
  private JLabel _detailsLabel = new JLabel();
  java.util.List<PanelCoordinate> _selectedPoints = null;
  private JComboBox _testSubProgramComboBox = new JComboBox();
  private JComboBox _scanPathComboBox = new JComboBox();
  private JLabel _estimateTimeLabel = new JLabel();//Lim Lay Ngor add
  
   /**
   * @author Kee Chin Seong
   */
  private static Comparator<Board> _boardComparator = new Comparator<Board>()
  {

    public int compare(Board lhs, Board rhs)
    {
      return lhs.getName().compareTo(rhs.getName());
    }
  };
  private static java.util.Map<Board,java.util.List<com.axi.v810.business.panelDesc.Component>> _boardToComponentsMap = new TreeMap<Board, java.util.List<com.axi.v810.business.panelDesc.Component>>(_boardComparator);
  
  public ScanPathViewPanel()
  {
    jbInit();
  }

  /**
   * The Groupings panel is a listener to datastore (for things like adding/removing components) and
   * it also listens to the DrawCad selection observerable so it can update it's table selections when the
   * @author Andy Mechtenberg
   * user clicks or otherwise selects stuff with the graphics.
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof SelectedRendererObservable)
          handleSelectionEvent(object);
        else if (observable instanceof ProjectObservable)
          handleProjectEvent(object);
        else if (observable instanceof GuiObservable)
          handleGuiEvent((GuiEvent)object);
        else if (observable instanceof com.axi.v810.gui.undo.CommandManager)
          handleUndoState(object);
        else
          Assert.expect(false);
      }
    });
  }
  
   /**
   * @author Andy Mechtenberg
   */
  private void savePersistSettings()
  {
    int dividerLocation = _centerSplitPane.getDividerLocation();
    if (_persistSettings != null)
      _persistSettings.setScanPathReviewResizeDivider(dividerLocation);
  }

   /**
   * @author Kee Chin Seong
   */
  private void handleUndoState(final Object object)
  {
     /*
     * no need undo for now
     */
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void handleSelectionEvent(Object object)
  {
  }
  
  /**
   * Handles information regarding a change in datastore.  Typically generated by someone doing a "set" when this
   * panel is not visible, but it should update itself anyway so when it becomes visible, data will be up to date.
   * @author Kee Chin Seong
   */
  public void handleProjectEvent(Object object)
  {
  }

  /**
   * @author Kee Chin Seong
   */
  public void handleGuiEvent(GuiEvent guiEvent)
  {
  }


  /**
   * @author Kee Chin Seong
   */
  private void jbInit()
  {
    _mainBorder = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(115, 114, 105),new Color(165, 163, 151)),BorderFactory.createEmptyBorder(5,5,5,5));
    setLayout(new BorderLayout());
     
    _contentPanel.setLayout(_contentPanelBorderLayout);
    _scanPathReviewSummaryPanel.setLayout(_scanPathReviewPanelBorderLayout);
  
    _detailsPanel.setLayout(_detailsPanelBorderLayout);
    _detailsLabel.setBorder(BorderFactory.createEmptyBorder(0,5,0,0));

    this.setBorder(_mainBorder);
    _contentPanelBorderLayout.setVgap(20);
    _scanPathReviewPanelBorderLayout.setVgap(5);
    _detailsPanelBorderLayout.setVgap(5);

    _centerSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);

    this.add(_contentPanel, BorderLayout.CENTER);
   
    _scanPathReviewTableModel = new ScanPathReviewTableModel();
  
    _scanPathReviewTable = new ScanPathReviewTable(_scanPathReviewTableModel)
    {
        
       /**
       * Overriding this method allows us to set up the undo manager
       * @author Kee Chin Seong
      */
      public void setValueAt(Object value, int row, int column)
      {
          pspPointsSummaryTableSetValue(value, row, column);
      }   
      /**
       * We override this to make sure the selected rows are still the proper selected
       * rows after a sort.
       * @author Kee Chin Seong
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = _scanPathReviewTable.getSelectedRows();
        java.util.List<Object> selectedData = new ArrayList<Object>();
        for(int i = 0; i < selectedRows.length; i++)
        {
          //selectedData.add(_scanPathReviewTableModel.getComponentTypeAt(selectedRows[i]));
        }
        ListSelectionModel rowSM = _scanPathReviewTable.getSelectionModel();
        rowSM.removeListSelectionListener(_pointsSummaryTableListSelectionListener); // add the listener for a details table row selection

        super.mouseReleased(event);

        _scanPathReviewTable.clearSelection();
        // now, reselect everything

        Iterator it = selectedData.iterator();
        while (it.hasNext())
        {
          int row;
          Object obj = it.next();
          row = _scanPathReviewTableModel.getRowForPanelCoordinate((StagePosition)obj);
          _scanPathReviewTable.addRowSelectionInterval(row, row);
        }
        SwingUtils.scrollTableToShowSelection(_scanPathReviewTable);
        rowSM.addListSelectionListener(_pointsSummaryTableListSelectionListener); // add the listener for a details table row selection
      }
    };
    
    _scanPathReviewTable.setCurrentTestSubProgram(_testSubProgram);
    _scanPathReviewTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _scanPathReviewTable.setSurrendersFocusOnKeystroke(false);
    _scanPathReviewTable.sizeColumnsToFit(-1);

    // add the listener for a table row selection
    _scanPathReviewTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        multiEditTableMouseReleased(e, _scanPathReviewTable);
      }
    });
    
    ListSelectionModel rowSM = _scanPathReviewTable.getSelectionModel();
    rowSM.addListSelectionListener(_pointsSummaryTableListSelectionListener); // add the listener for a details table row selection
    
    JPanel _editButtonPanel = new JPanel();
    FlowLayout _editButtonPanelFlowLayout = new FlowLayout();
    _editButtonPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _editButtonPanelFlowLayout.setHgap(10);
    _editButtonPanel.setLayout(_editButtonPanelFlowLayout);
    
    JPanel northMainPanel = new JPanel();
    JPanel labelPanel = new JPanel();
    JPanel scanPathPanel = new JPanel();
    JPanel estimateTimePanel = new JPanel();//Lim Lay Ngor add
    
    labelPanel.setLayout(_editButtonPanelFlowLayout);
    scanPathPanel.setLayout(_editButtonPanelFlowLayout);
    estimateTimePanel.setLayout(_editButtonPanelFlowLayout);//Lim Lay Ngor add
    northMainPanel.setLayout(new BorderLayout());
    
    _detailsPanel.add(_editButtonPanel, BorderLayout.CENTER);
    
    JLabel selectedTestSubProgram = new JLabel(StringLocalizer.keyToString("SCAN_PATH_SELECTED_POINTS_LABEL_KEY"));
    labelPanel.add(selectedTestSubProgram, null);
    labelPanel.add(_testSubProgramComboBox, null);
    
    JLabel selectedScanPath = new JLabel(StringLocalizer.keyToString("SCAN_PATH_REVIEW_SELECTED_SCAN_PATH_KEY"));
    scanPathPanel.add(selectedScanPath, null);
    scanPathPanel.add(_scanPathComboBox, null);
    
    //Lim Lay Ngor add
    JLabel showEstimatedTime_s = new JLabel(StringLocalizer.keyToString("SCAN_PATH_REVIEW_ESTIMATE_TIME_KEY"));
    estimateTimePanel.add(showEstimatedTime_s, null);
    _estimateTimeLabel.setFont(FontUtil.getBoldFont(_estimateTimeLabel.getFont(), Localization.getLocale()));
    estimateTimePanel.add(_estimateTimeLabel);
 
    northMainPanel.add(labelPanel, BorderLayout.WEST);
    northMainPanel.add(scanPathPanel, BorderLayout.EAST);
    northMainPanel.add(estimateTimePanel, BorderLayout.AFTER_LAST_LINE);//Lim Lay Ngor add
    
    _testSubProgramComboBox.addActionListener(new ActionListener()
     {
      public void actionPerformed(ActionEvent e)
      {
         testSubProgramComboBox_actionPerformed(e);
      }
     });
    
    _scanPathComboBox.addActionListener(new ActionListener()
     {
      public void actionPerformed(ActionEvent e)
      {
         scanPathComboBox_actionPerformed(e);
      }
     });
    
    _centerSplitPane.add(_scanPathReviewSummaryPanel, JSplitPane.TOP);
    _scanPathReviewSummaryPanel.add(northMainPanel, BorderLayout.NORTH);
    _scanPathReviewSummaryPanel.add(_scanPathReviewPanelScrollPane, BorderLayout.CENTER);
    _centerSplitPane.add(_detailsPanel, JSplitPane.BOTTOM);
    _contentPanel.add(_centerSplitPane, java.awt.BorderLayout.CENTER);
    _scanPathReviewPanelScrollPane.getViewport().add(_scanPathReviewTable, null);
   
    _scanPathReviewChartPanel = new ScanPathReviewChartPanel();
  }
  
  private void drawChart()
  {
    _plot = new ScanPathPointEntryXPlot(_scanPathReviewTableModel.getStagePositionList());
    _chart = new JFreeChart(_plot.getTitle(), JFreeChart.DEFAULT_TITLE_FONT, _plot, _SHOW_CHART_LEGEND);
    _chartContentPanel.setChart(_chart);
  }
  
   /**
   * @author Kee Chin Seong
   */
  private void testSubProgramComboBox_actionPerformed(ActionEvent e)
  {
     Object selectedLp = _testSubProgramComboBox.getSelectedItem();
     int selectedIndex = _testSubProgramComboBox.getSelectedIndex();
      
     if ((selectedLp != null))
     { 
       double estimateTime = 0;
       
       if(selectedIndex == 0)
       {
         _scanPathReviewTableModel.populateWithTestProgramScanPath(_project.getTestProgram());
         for(TestSubProgram testSubProgram : _project.getTestProgram().getFilteredScanPathTestSubPrograms())
         {
           estimateTime += testSubProgram.getEstimateTestSubProgramExecutionTime();
         }
       }
       else
       {
        _scanPathReviewTableModel.populateWithTestSubProgramScanPath(_project.getTestProgram().getTestSubProgramById((Integer) selectedLp));
        _scanPathReviewTable.setCurrentTestSubProgram(_project.getTestProgram().getTestSubProgramById((Integer) selectedLp));
        
        estimateTime = _project.getTestProgram().getTestSubProgramById((Integer) selectedLp).getEstimateTestSubProgramExecutionTime();
       }
       
       //Lim Lay Ngor add
       _estimateTimeLabel.setText(Double.toString(estimateTime));
       _estimateTimeLabel.setFont(FontUtil.getBoldFont(_estimateTimeLabel.getFont(), Localization.getLocale()));
       _estimateTimeLabel.repaint();
     }

     updateChartPanelByTestProgram();
     updateChartPanelByTestSubPrograms();
     populateScanPathSeqNumber();
  }
  
  private void scanPathComboBox_actionPerformed(ActionEvent e)
  {
     Object selectedLp = _scanPathComboBox.getSelectedItem();
     int selectedIndex = _scanPathComboBox.getSelectedIndex();
     
     if(selectedIndex >= 0)
     {
       if(selectedIndex == 0)
          updateChartPanelByTestSubPrograms();
       else
          updateChartPanelByScanPathSeqNumber(selectedIndex);  
     }
  }
  
   /**
   * @author Kee Chin Seong
   */
  private void pspPointsSummaryTableSetValue(Object value, int row, int column)
  {
    
  }
          

  /**
   * @author Kee Chin Seong
   */
   void updateCadGraphics()
   {
     _testDev = _mainUI.getTestDev();
     _testDev.initGraphicsToPackage();
     _testDev.switchToPanelGraphics();
   }

   
  /**
   * @author Kee Chin Seong
   */   
   void createChartPanel()
   { 
     _testDevChartPanel = _testDev.getChartPanel();
     _testDevChartPanel.setLayout(new BorderLayout());
     _testDevChartPanel.add(_scanPathReviewChartPanel, BorderLayout.CENTER);
   }

   /*
    *@author Chnee Khang Wah 
    */
   void updateChartPanelByTestProgram()
   {
     _scanPathReviewChartPanel.clearDataSet();
     _scanPathReviewChartPanel.populateDataSet(_scanPathReviewTableModel.getStagePositionList());
   }
   
   /*
    *@author Kee Chin Seong 
    */
   void updateChartPanelByTestSubPrograms()
   {
     _scanPathReviewChartPanel.clearDataSet();
     _scanPathReviewChartPanel.populateDataSet(_scanPathReviewTableModel.getStagePositionList());
   }
   
   void updateChartPanelByScanPathSeqNumber(int seqNumber)
   {
     _scanPathReviewChartPanel.clearDataSet();
     _scanPathReviewChartPanel.populateDataSet(_scanPathReviewTableModel.getStagePositionList(), seqNumber);
   }
   
   void populateScanPathSeqNumber()
   {
     _scanPathComboBox.removeAllItems(); // make sure always empty before populate
     _scanPathComboBox.addItem("All");
     for(int seqNum = 0; seqNum <= _scanPathReviewChartPanel.getScanPathsSize(); seqNum ++)
     {
       _scanPathComboBox.addItem(seqNum);
     }
   }
   
   /**
    * @author Kee Chin Seong
    */
   public void unpopulate()
   {
      savePersistSettings();
     _project = null;
     _testSubProgram = null;
     _boardToComponentsMap.clear();
     _scanPathReviewTable.clearSelection();     
     _scanPathReviewTable.resetSortHeader();   
     _scanPathReviewTableModel.clear();
     _scanPathReviewChartPanel.clearDataSet();
     ProjectObservable.getInstance().deleteObserver(this);
     // add this as an observer of the gui
    _guiObservable.deleteObserver(this);
   }

   /**
    * @author Kee Chin Seong
    */
   public void populateWithProjectData()
   {
      _persistSettings = MainMenuGui.getInstance().getTestDev().getPersistance();
     // remove the stuff from the _comboBoxPanel because it actually get populated here.
     // the reason for this is because if there is only one board, I don't want to use a
     // combo box, I want to use a JLabel.  Here is where I know that, so I'll add the controls here.
     // If the user re-loads a new project after one has already been loaded, we need to redo this.

     validate();
     repaint();
     
     updateProjectData();
     
     _scanPathComboBox.removeAllItems();
     _testSubProgramComboBox.removeAllItems();
     _testSubProgramComboBox.addItem("All");
 
     _scanPathReviewTable.scrollRectToVisible(new Rectangle(0,0,0,0));
     
     for(TestSubProgram testSubProgram : _project.getTestProgram().getAllInspectableTestSubPrograms())
     {
        _testSubProgramComboBox.addItem(testSubProgram.getId());
     }
     
     //setupTableEditors();
  }

  /**
   * @author Kee Chin Seong
   */
  public void populateWithProjectData(Project project)
  {
     Assert.expect(project != null);

    _project = project;
 
    if (_active)
    {
      populateWithProjectData();
    }  
  }

  /**
   * All controls are repopulated be re-reading out of datastore
   * @author Kee Chin Seong
   */
  public void updateProjectData()
  {
    if(_project.getTestProgram().getTestSubPrograms().isEmpty() == true)
    {
      _scanPathReviewTable.setCurrentTestSubProgram(null);
      _scanPathReviewTableModel.populateWithTestSubProgramScanPath(null);
      updateChartPanelByTestSubPrograms();
    }
    _scanPathReviewTableModel.fireTableDataChanged();
  }

  /**
   * @author Kee Chin Seong
   */
  private void setupTableEditors()
  {
     _scanPathReviewTable.setupCellEditorsAndRenderers(); 
  }

  /**
   * This method implements the right click edit when multiple lines of a table are selected
   * @author Andy Mechtenberg
   */
  private void multiEditTableMouseReleased(MouseEvent e, JTable table)
  {
    Assert.expect(e != null);
    Assert.expect(table != null);

    // ignore any other mouse events than a popup trigger -- we don't want to break the normal L&F
    if (e.isPopupTrigger())
    {
      // get the table cell that the mouse was right-clicked in
      Point p = e.getPoint();
      int row = table.rowAtPoint(p);
      int col = table.columnAtPoint(p);

      // if this cell is not currently selected, exit.  It's confusing to edit a cell that not selected.
      // we could "select" this cell too, but for now, we'll just not.
      if (table.isCellSelected(row, col) == false)
        return;

      // start the editing process for this cell
      table.editCellAt(row, col);
      // if this is a combo box cell editor, the editCellAt doesn't show the combo box popup, so let's show that
      // only if this is a combo box editor
      Object obj = table.getEditorComponent();
      if (obj instanceof JComboBox)
      {
        JComboBox comboBox = (JComboBox)obj;
        comboBox.showPopup();
      }
    }
  }


  /**
   * Task is ready to start
   * @author George Booth
   * Edited by @author Kee Chin Seong
   *  - Modify Subtype is slow while drawing image panel 
   *  - Should control by verification image validation
   */
  public void start(ImageManagerPanel imageManagerPanel)
  {
     Assert.expect(imageManagerPanel != null);

    _imagePanel = imageManagerPanel;
        
    com.axi.util.TimerUtil screenTimer = new com.axi.util.TimerUtil();
    screenTimer.start();

    updateCadGraphics();
    createChartPanel();
    
    populateWithProjectData();
   
    // set up for custom menus and tool bar
    _active = true;
  
    // generate test program if needed; notify user it is happening
    //Project project = _project;
    if (_project.isTestProgramValid() == false)
    {
      _mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_TESTABILITY_NEEDS_TEST_PROGRAM_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_TDT_TITLE_GROUPINGS_KEY"),
                                                    _project);
      _scanPathReviewTableModel.fireTableDataChanged();
      _updateData = true;
    }
    
    //Kee chin Seong - Controling the image panel graphic engine
    //if(_project.areVerificationImagesValid())
    //  _imagePanel.displayVerificationImagePanel(true);
    
   _commandManager.addObserver(this);
   // now it's time to update the graphics with the current selection

   if (_updateData)
   {
     updateProjectData();
   }
   _updateData = false;
   screenTimer.stop();
  }

  /**
   * Can the environment be swapped out
   * @return true if environment can be swapped
   * @author Kee Chin Seong
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * The environment is about to be swapped out and should perform cleanup
   * @author Kee Chin Seong
   */
  public void finish()
  {
    // remove custom menus and toolbar components
    savePersistSettings();
    
    //_scanPathChartPanel = null;   
    _chartContentPanel = null;
    
    _commandManager.deleteObserver(this);
    _active = false;
  }
}
