package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.service.PointToPointMoveDefinitionTableModel;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 *)
 * @author chin-seong.kee
 */
public class ScanPathReviewPanel extends AbstractTaskPanel implements Observer
{

  private MainMenuGui _mainUI = null;
  private TestDev _testDev = null;
  private ImageManagerPanel _imagePanel;
  private TestDevPersistance _persistSettings;
  private ProjectPersistance _projectPersistSettings = null;
  private BoardType _currentBoardType;
  private Project _project = null;
  private boolean _updateData = false;
  private boolean _populateProjectData = true;
 
  // every time a Renderer is selected the Observable will update any Observers to
  // tell them it was selected
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  
  // command manager for doing undos
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  PointToPointMoveDefinitionTableModel _pointToPointMoveDefinitionTableModel;
  private JTabbedPane _editCadTabbedPane = new JTabbedPane();
  ScanPathViewPanel _scanPathViewPanel = null;
  private BorderLayout _editCadBorderLayout = new BorderLayout();

  public ScanPathReviewPanel()
  {
    _scanPathViewPanel = new ScanPathViewPanel();
    jbInit();
  }

  /**
   * @author Kee Chin Seong
   */
  public ScanPathReviewPanel(TestDev testDev, ImageManagerPanel imagePanel)
  {
    super(testDev);
    Assert.expect(testDev != null);
    Assert.expect(imagePanel != null);
    _testDev = testDev;
    _imagePanel = imagePanel;
    _mainUI = _testDev.getMainUI();

    _scanPathViewPanel = new ScanPathViewPanel();
    jbInit();
  }

  /**
   * @author Kee Chin Seong
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof SelectedRendererObservable)
          handleSelectionEvent(object);
        else if (observable instanceof ProjectObservable)
          handleProjectEvent(object);
        else if (observable instanceof GuiObservable)
          handleGuiEvent((GuiEvent)object);
        else if (observable instanceof com.axi.v810.gui.undo.CommandManager)
          handleUndoState(object);
        else
          Assert.expect(false);
      }
    });
  }
  
   /**
   * @author Kee Chin Seong
   */
  private void handleUndoState(final Object object)
  {
    // do nothing yet 
  }
  
  /**
   * @author Kee Chin Seong
   */
  private void handleSelectionEvent(Object object)
  {
    // do nothing
  }
  
  /**
   * @author Kee Chin Seong
   */
  private void handleProjectEvent(Object object)
  {      
    _scanPathViewPanel.handleProjectEvent(object);
  }

  /**
   * @author Kee Chin Seong
   */
  private void handleGuiEvent(GuiEvent guiEvent)
  {  
    _scanPathViewPanel.handleGuiEvent(guiEvent);
  }


  /**
   * @author Kee Chin Seong
   */
  private void jbInit()
  {
    setLayout(_editCadBorderLayout);
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    add(_editCadTabbedPane, BorderLayout.CENTER);
      
   // _pspComponentBasedPanel = new PspComponentBasedPanel();
   //  this.add(_editCadTabbedPane, BorderLayout.NORTH);
    _editCadTabbedPane.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        editCadTabbedPane_stateChanged(e);
      }
    });
    
    _editCadTabbedPane.add(_scanPathViewPanel, StringLocalizer.keyToString("PSP_POINTS_SUMMARY_PSPTAB_KEY"));

  }

  /**
   * @author Kee Chin Seong
   */
  void updateCadGraphics()
  {
     //_testDev.initGraphicsToPackage(); 
     //_testDev.switchToPanelGraphics();  
  }

   /**
    * @author Kee Chin Seong
    */
  public void unpopulate()
  {
    _scanPathViewPanel.unpopulate();
    _project = null;
    _currentBoardType = null;
    ProjectObservable.getInstance().deleteObserver(this);

    // add this as an observer of the gui
   _guiObservable.deleteObserver(this);
  }

  /**
  * @author Kee Chin Seong
  */
  private void editCadTabbedPane_stateChanged(ChangeEvent e)
  {
    if (!_active)
      return;

    // moving from ComponentBased tab
    if (_editCadTabbedPane.getSelectedIndex() != 0)
    {
      if (_scanPathViewPanel.isReadyToFinish())
      {
        _scanPathViewPanel.finish();
      }       
      else
      {
        _editCadTabbedPane.setSelectedIndex(0);  // stay on ComponentBased CAD
        return;
      }
    }

    /*if (_editCadTabbedPane.getSelectedIndex() == 0) // RegionBased tab
    {
      _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().clearSelectedRenderers();
      _testDev.changeToSurfaceMappingToolBar(true);
      _scanPathViewPanel.start(_imagePanel);
      _testDev.clearBoardGraphics();
      _testDev.switchToPanelGraphics();
      _guiObservable.deleteObserver(this);
    }
    else
    {
      _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().clearMouseEventsInSurfaceMappingScreen();  
      _testDev.switchToPanelGraphics();
      _testDev.showCadWindow();
      _guiObservable.addObserver(this);
      
    }*/
  }
   
   /**
    * @author Kee Chin Seong
   */
   public void populateWithProjectData()
   {
     // remove the stuff from the _comboBoxPanel because it actually get populated here.
     // the reason for this is because if there is only one board, I don't want to use a
     // combo box, I want to use a JLabel.  Here is where I know that, so I'll add the controls here.
     // If the user re-loads a new project after one has already been loaded, we need to redo this.
     _currentBoardType = _testDev.getCurrentBoardType();

     validate();
     repaint();
   }

  /**
   * @author Kee Chin Seong
   */
  public void populateWithProjectData(Project project)
  {
    Assert.expect(project != null);

    _projectPersistSettings = MainMenuGui.getInstance().getTestDev().getPersistance().getProjectPersistance();

    _project = project;

    ProjectObservable.getInstance().addObserver(this);
    // add this as an observer of the gui
    _guiObservable.addObserver(this);

    if (_active)
    {
      populateWithProjectData();
      _populateProjectData = true;
    }
    else
      _populateProjectData = true;
    
    _scanPathViewPanel.populateWithProjectData(_project);
  }

  /**
   * All controls are repopulated be re-reading out of datastore
   * @author Kee Chin Seong
   */
  private void updateProjectData()
  {
    _scanPathViewPanel.populateWithProjectData(_currentBoardType.getPanel().getProject());
  }

  /**
   * This method implements the right click edit when multiple lines of a table are selected
   * @author Kee Chin Seong
   */
  private void multiEditTableMouseReleased(MouseEvent e, JTable table)
  {
    Assert.expect(e != null);
    Assert.expect(table != null);

    // ignore any other mouse events than a popup trigger -- we don't want to break the normal L&F
    if (e.isPopupTrigger())
    {
      // get the table cell that the mouse was right-clicked in
      Point p = e.getPoint();
      int row = table.rowAtPoint(p);
      int col = table.columnAtPoint(p);

      // if this cell is not currently selected, exit.  It's confusing to edit a cell that not selected.
      // we could "select" this cell too, but for now, we'll just not.
      if (table.isCellSelected(row, col) == false)
        return;

      // start the editing process for this cell
      table.editCellAt(row, col);
      // if this is a combo box cell editor, the editCellAt doesn't show the combo box popup, so let's show that
      // only if this is a combo box editor
      Object obj = table.getEditorComponent();
      if (obj instanceof JComboBox)
      {
        JComboBox comboBox = (JComboBox)obj;
        comboBox.showPopup();
      }
    }
  }

  /**
   * @author Kee Chin Seong
   */
  private void boardTypeChanged()
  {
   // _componentTable.setCurrentBoardType(_currentBoardType);
    //_componentTableModel.populateWithPanelData(_currentBoardType);
    //_componentTableModel.fireTableDataChanged();

  }

  /**
   * Task is ready to start
   * @author Kee Chin Seong
   */
  public void start()
  {
    com.axi.util.TimerUtil screenTimer = new com.axi.util.TimerUtil();
    screenTimer.start();

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();
    
    if (_populateProjectData)
    {
      populateWithProjectData();
    }
    _populateProjectData = false;

    // set up for custom menus and tool bar
   
    _testDev.addBoardSelectInToolbar(_envToolBarRequesterID);
    _active = true;
   
    _testDev.addChartPanel();
    _testDev.showChartWindow();
    
    updateCadGraphics();

    // generate test program if needed; notify user it is happening
    Project project = _currentBoardType.getPanel().getProject();
    if (project.isTestProgramValid() == false)
    {
      _mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_TESTABILITY_NEEDS_TEST_PROGRAM_KEY"),
                                                    StringLocalizer.keyToString("MM_GUI_TDT_TITLE_GROUPINGS_KEY"),
                                                    project);
      _updateData = true;
    }
    
   if (_updateData)
   {
     updateProjectData();
   }
   _updateData = false;
   _guiObservable.stateChanged(null, GraphicsControlEnum.ORIGIN_VISIBLE);
  
   _editCadTabbedPane.setSelectedIndex(0);  
   //_testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().clearMouseEventsInSurfaceMappingScreen();
   _testDev.changeToScanPathToolBar(true);     
   _scanPathViewPanel.start(_imagePanel);
   //_testDev.clearBoardGraphics();
   //_testDev.switchToPanelGraphics();      
   _guiObservable.deleteObserver(this);    
  
   screenTimer.stop();
  }

  /**
   * Can the environment be swapped out
   * @return true if environment can be swapped
   * @author Kee Chin Seong
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * The environment is about to be swapped out and should perform cleanup
   * @author Kee Chin Seong
   */
  public void finish()
  {
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
    //unpopulate();
            
   _imagePanel.finishVerificationImagePanel();
   //_testDev.clearPackageGraphics();
   //_testDev.setActiveTunerComponentType();

   _active = false;
 
   _scanPathViewPanel.finish();
   _testDev.changeToScanPathToolBar(false);
   
   _testDev.removeChartPanel();
  }
}
