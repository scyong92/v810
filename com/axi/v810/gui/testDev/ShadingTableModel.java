package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is the data model for the magnification table for the Coverage Options panel
 * @author Andy Mechtenberg
 */
public class ShadingTableModel extends DefaultSortTableModel
{
  private List<Component> _components;

  private final String[] _tableColumns = {StringLocalizer.keyToString("MMGUI_COVERAGE_OVERSCAN_COLUMN_KEY"),
                                          StringLocalizer.keyToString("MMGUI_GROUPINGS_COMPONENT_KEY")};
                                          //StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY")};
  /**
   * @author Andy Mechtenberg
   */
  public ShadingTableModel()
  {
  }

  /**
   * @author Andy Mechtenberg
   */
  void populateWithProjectData(Project project)
  {
    Assert.expect(project != null);
    Panel panel = project.getPanel();
    _components = new ArrayList<Component>();
    List<Component> components = panel.getComponents();

    /** @todo This is fake data -- real data should come from datastore */
    // use every 9th component just for fake
    for (int j = 0; j < components.size(); j++)
    {
      int res = (j % 29);
      if (res == 1)
      {
        _components.add(components.get(j));
      }
    }
    Collections.sort( _components, new AlphaNumericComparator() );
    fireTableStructureChanged();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void sortColumn(int column, boolean ascending)
  {
    boolean sortOnPackageName;
    if(column == 0)
      Collections.sort(_components, new ComponentIsOverscannedComparator(ascending));
    else
      Collections.sort(_components, new AlphaNumericComparator(ascending));


  }

  /**
   * @author Andy Mechtenberg
   */
  public Class getColumnClass(int columnIndex)
  {
    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getColumnCount()
  {
    return _tableColumns.length;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getRowCount()
  {
    if(_components == null)
      return 0;
    else
      return _components.size();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return(String)_tableColumns[columnIndex];
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    if (columnIndex == 0)
      return true;
    else
      return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Component comp = (Component)_components.get(rowIndex);

    if (columnIndex == 0)
      /** @todo wpd - no more overscan */
      return "dunno";
//      return new Boolean(comp.getComponentTypeSettings().isOverScanUsed());
    else if (columnIndex == 1)
      return comp.getReferenceDesignator();
    else if (columnIndex == 2)
    {
      List<Pad> pads = comp.getPads();
      Assert.expect(pads.isEmpty() == false);
      Pad pad = pads.get(0);
      return pad.getSubtype().getShortName(); // make sure this is the right call
      /** @todo PE make sure this is right, old code follows:
      int subtypeNum = pad.getSubtype().getLegacySubType();
      String subtypeName = pad.getSubtype().getPartialCustomName();
      if(subtypeName.length() == 0)
        return new Integer(subtypeNum);
      else
        return subtypeName;
      */
    }
    else
    {
      Assert.expect(false);
      return null;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    if (columnIndex == 0)
    {
      Component comp = (Component)_components.get(rowIndex);
      Boolean value = (Boolean)aValue;
/** @todo wpd no more overscan */
//      comp.getComponentTypeSettings().setUseOverScan(value.booleanValue());
    }
  }

}
