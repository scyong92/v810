package com.axi.v810.gui.testDev;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import java.util.*;

/**
 *
 * @author siew-yeng.phang
 */
public class SerialNumberMappingDataComparator implements Comparator<SerialNumberMappingData>
{
  private AlphaNumericComparator _alphaComparator;
  private boolean _ascending;
  private SerialNumberMappingDataComparatorEnum _comparingAttribute;
  
  /**
   * @author Siew Yeng
   */
  public SerialNumberMappingDataComparator(boolean ascending, SerialNumberMappingDataComparatorEnum comparingAttribute)
  {
    Assert.expect(comparingAttribute != null);
    _ascending = ascending;
    _comparingAttribute = comparingAttribute;
    _alphaComparator = new AlphaNumericComparator(_ascending);
  }

  /**
   * @author Siew Yeng
   */
  public int compare(SerialNumberMappingData lhs, SerialNumberMappingData rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);
    String lhsString;
    String rhsString;
    Integer lhsInteger;
    Integer rhsInteger;

    if(_comparingAttribute.equals(SerialNumberMappingDataComparatorEnum.BOARD_NAME))
    {
      lhsString = lhs.getBoardName();
      rhsString = rhs.getBoardName();
      return _alphaComparator.compare(lhsString, rhsString);

    }
    else if(_comparingAttribute.equals(SerialNumberMappingDataComparatorEnum.BARCODE_READER_ID))
    {
      lhsInteger = lhs.getBarcodeReaderId();
      rhsInteger = rhs.getBarcodeReaderId();
      if (_ascending)
        return lhsInteger.compareTo(rhsInteger);
      else
        return rhsInteger.compareTo(lhsInteger);
    }
    else if(_comparingAttribute.equals(SerialNumberMappingDataComparatorEnum.SERIAL_NUMBER_SEQUENCE))
    {
      lhsInteger = lhs.getScanSequence();
      rhsInteger = rhs.getScanSequence();
      if (_ascending)
          return lhsInteger.compareTo(rhsInteger);
      else
        return rhsInteger.compareTo(lhsInteger);
    }
    else
      return 0;
  }
  
}
