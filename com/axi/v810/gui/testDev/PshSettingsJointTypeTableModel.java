package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.MessageDialog;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * @author siew-yeng.phang
 */
public class PshSettingsJointTypeTableModel extends DefaultSortTableModel
{
  private EditPshSettingsPanel _parent;
  private MainMenuGui _mainUI;
  private PshSettings _pshSettings;
  //String labels retrieved from the localization properties file
  private static final String _JOINT_TYPE_KEY_NAME_LABEL = StringLocalizer.keyToString("CFGUI_JTA_JOINT_TYPE_NAME_KEY");
  private static final String _ENABLED_LABEL = StringLocalizer.keyToString("CFGUI_JTA_RULE_ENABLED_KEY");

  private String[] _columnLabels = {_JOINT_TYPE_KEY_NAME_LABEL,
                                   _ENABLED_LABEL};

  private List<JointTypeEnum> _jointTypes = new ArrayList<JointTypeEnum>();
  private List<JointTypeEnum> _selectedJointTypes = new ArrayList<JointTypeEnum>();

  private static com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  
  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;
  
  /**
   * Constructor.
   * @author Siew Yeng
   */
  public PshSettingsJointTypeTableModel(EditPshSettingsPanel panel)
  {
    _parent = panel;
    _mainUI = MainMenuGui.getInstance();
  }
  
  /**
   * @author Siew Yeng
   */
  void clearData()
  {
    _pshSettings = null;
    _jointTypes.clear();
    _selectedJointTypes.clear();
    fireTableDataChanged();
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Siew Yeng
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @author Siew Yeng
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * Overridden method.  Gets the values from the project object.
   * @author Siew Yeng
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    JointTypeEnum jointType = _jointTypes.get(rowIndex);

    //get the appropriate data item that corresponds to the columnIndex
    switch(columnIndex)
    {
      case PshSettingsJointTypeTable._JOINT_TYPE_NAME_INDEX:
        value = jointType;
        break;
      case PshSettingsJointTypeTable._JOINT_TYPE_ENABLED_INDEX:
        value = _pshSettings.isJointTypeEnabled(jointType);
        break;
      default:
      {
        Assert.expect(false);
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Siew Yeng
   */
  public int getRowCount()
  {
    if(_jointTypes != null)
      return _jointTypes.size();

    return 0;
  }

  /**
   * Overridden method.
   * @author Siew Yeng
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if(columnIndex == 0)
      return false;
    
    if (_selectedJointTypes.isEmpty() || (_selectedJointTypes.contains(_jointTypes.get(rowIndex)) == false))
      return false;

    return true;
  }

  /**
   * @author Siew Yeng
   */
  void setData(PshSettings pshSettings)
  {
    Assert.expect(pshSettings != null);
    
    _jointTypes = JointTypeEnum.getAllJointTypeEnums();
    _jointTypes.remove(JointTypeEnum.PRESSFIT);
    _jointTypes.remove(JointTypeEnum.THROUGH_HOLE);
    
    _pshSettings = pshSettings;

    sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableDataChanged();
  }

  /**
   * Overridden method.  Saves the data entered by the user in the table
   * to the project object.
   * @author Laura Cormos
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    JointTypeEnum jointType = _jointTypes.get(rowIndex);

    switch (columnIndex)
    {
      case PshSettingsJointTypeTable._JOINT_TYPE_ENABLED_INDEX:
        Assert.expect(object instanceof Boolean);
        Boolean enabled = (Boolean)object;
        try
        {
          _commandManager.trackState(_parent.getCurrentUndoState());
          _commandManager.execute(new PshSettingsSetEnableJointTypeCommand(_pshSettings, jointType, enabled));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      default:
        Assert.expect(false);
    }
  }
  
  /**
   * @author Siew Yeng
   */
  public void sortColumn(int column, boolean ascending)
  {
    _currentSortColumn = column;
    _currentSortAscending = ascending;

    if(column == 0) // jointType
      Collections.sort(_jointTypes, new JointTypeEnumComparator(ascending));
    else
    {
      Collections.sort(_jointTypes, new Comparator<JointTypeEnum>() 
      {
        public int compare(JointTypeEnum lhsJointType, JointTypeEnum rhsJointType)
        {
          if(_currentSortAscending == false)
          {
            JointTypeEnum tempjointTypeEnum = rhsJointType;
            rhsJointType = lhsJointType;
            lhsJointType = tempjointTypeEnum;
          }
          String lhs = String.valueOf(_pshSettings.isJointTypeEnabled(lhsJointType));
          String rhs = String.valueOf(_pshSettings.isJointTypeEnabled(rhsJointType));
          
          return lhs.compareToIgnoreCase(rhs);
        }
      });
    }
  }
  
  /**
   * @author Siew Yeng
   */
  public int getRowForJointType(JointTypeEnum jointType)
  {
    Assert.expect(jointType != null);
    Assert.expect(_jointTypes != null);
    
    return _jointTypes.indexOf(jointType);
  }
  
  public JointTypeEnum getJointTypeAt(int rowIndex)
  {
    Assert.expect(_jointTypes != null);
    
    return _jointTypes.get(rowIndex);
  }
  
  /**
   * @author Siew Yeng
   */
  public void setSelectedJointTypeRowIndex(List<JointTypeEnum> selectedJointTypes)
  {
    Assert.expect(selectedJointTypes != null);
    
    _selectedJointTypes.clear();
    _selectedJointTypes.addAll(selectedJointTypes);
    
  }
  
  /**
   * @author Siew Yeng
   */
  public List<JointTypeEnum> getSelectedJointTypes()
  {
    Assert.expect(_selectedJointTypes != null);
    
    return _selectedJointTypes;
  }
}
