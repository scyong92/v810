package com.axi.v810.gui.testDev;

import com.axi.util.Assert;

/**
 * This class tracks the state of the UI for undo/redo.  The data in in this class
 * is used to revert the Test Dev UI back to the state it was in when the original
 * command was executed.  A new instance of this class should be passed into the
 * commandManage.trackState method, so that each command can hold a unique state container.
 * @author Andy Mechtenberg
 */
public class UndoState
{
  // adjust cad tabbed pane setings
  private int _adjustCadTabIndex = 0;

  // land pattern tab settings
  private int _landPatternComboBoxSelectedIndex = 0;
  private String _landPatternComboBoxName;

  // verify cad subtype settings -- only used for UNDO
  private int _verifyCadViewSelectIndex = 0;
  private boolean _adjustCadGraphically = false;
  private int _verifyCadSideSelectIndex = 0; //Siew Yeng - XCR-2586

  // modify subtype settings -- only used for UNDO
  private int _modifySubtypeViewSelectIndex = 0;

  // tuner settings
  private int _testModeSelection = 0;
  private String _tunerJointType;
  private String _tunerSubtype;
  private String _tunerComponent = "";
  private String _tunerPad = "";
  private int _tunerThresholdTabIndex = 0;
  private boolean _tunerBasicThresholds = true;
  private int _thresholdRow = 0;

  // config UI settings
  // initial thresholds
  private int _initialThresholdsSetComboboxSelectedIndex = 0;
  private int _intialThresholdsTabIndex = 0;
  private int _initialThresholdsThresholdRow = 0;
  
  private int _initialRecipeSettingSetComboboxSelectedIndex = 0;
  private int _initialRecipeSettingRow = 0;
  // joint type assignment
  private int _jtaRulesSetComboboxSelectedIndex = 0;
  
  // Surface Mapping opitcal region
  private String _opticalRegionComboBoxName;
  private int _opticalRegionComboBoxSelectedIndex = 0;

  /**
   * @author George Booth
   */
  int getVerifyCadViewSelectIndex()
  {
    return _verifyCadViewSelectIndex;
  }

  /**
   * @author George Booth
   */
  void setVerifyCadViewSelectIndex(int viewSelectIndex)
  {
    _verifyCadViewSelectIndex = viewSelectIndex;
  }
  
  /**
   * @author Siew Yeng
   */
  int getVerifyCadSideSelectIndex()
  {
    return _verifyCadSideSelectIndex;
  }

  /**
   * @author Siew Yeng
   */
  void setVerifyCadSideSelectIndex(int sideSelectIndex)
  {
    _verifyCadSideSelectIndex = sideSelectIndex;
  }

  /**
   * @author George Booth
   */
  boolean getAdjustCadGrapically()
  {
    return _adjustCadGraphically;
  }

  /**
   * @author George Booth
   */
  void setAdjustCadGraphically(boolean adjustCadGraphically)
  {
    _adjustCadGraphically = adjustCadGraphically;
  }

  /**
   * @author Laura Cormos
   */
  int getLandPatternComboBoxIndex()
  {
    return _landPatternComboBoxSelectedIndex;
  }

  /**
   * @author Laura Cormos
   */
  void setLandPatternComboBoxIndex(int landPatternComboBoxIndex)
  {
    _landPatternComboBoxSelectedIndex = landPatternComboBoxIndex;
  }
  
   /**
   * @author Jack Hwee
   */
  int getOpticalRegionComboBoxIndex()
  {
    return _opticalRegionComboBoxSelectedIndex;
  }

  /**
   * @author Jack Hwee
   */
  void setOpticalRegionComboBoxIndex(int opticalRegionComboBoxIndex)
  {
    _opticalRegionComboBoxSelectedIndex = opticalRegionComboBoxIndex;
  }

  /**
   * @author Laura Cormos
   */
  String getLandPatternComboBoxName()
  {
    Assert.expect(_landPatternComboBoxName != null);
    return _landPatternComboBoxName;
  }

  /**
   * @author Laura Cormos
   */
  void setLandPatternComboBoxName(String landPatternComboBoxName)
  {
    Assert.expect(landPatternComboBoxName != null);
    _landPatternComboBoxName = landPatternComboBoxName;
  }
  
   /**
   * @author Jack Hwee
   */
  String getOpticalRegionComboBoxName()
  {
    Assert.expect(_opticalRegionComboBoxName != null);
    return _opticalRegionComboBoxName;
  }

  /**
   * @author Jack Hwee
   */
  void setOpticalRegionComboBoxName(String opticalRegionComboBoxName)
  {
    Assert.expect(opticalRegionComboBoxName != null);
    _opticalRegionComboBoxName = opticalRegionComboBoxName;
  }

  /**
   * @author Laura Cormos
   */
  boolean isLandPatternComboBoxNameSet()
  {
    if (_landPatternComboBoxName != null)
      return true;
    return false;
  }
  
   /**
   * @author Laura Cormos
   */
  boolean isOpticalRegionComboBoxNameSet()
  {
    if (_opticalRegionComboBoxName != null)
      return true;
    return false;
  }

  /**
   * @author Laura Cormos
   */
  void setAdjustCadTabbedPaneIndex(int adjustCadTabbedPaneIndex)
  {
    _adjustCadTabIndex = adjustCadTabbedPaneIndex;
  }

  /**
   * @author Laura Cormos
   */
  int getAdjustCadTabbedPaneIndex()
  {
    return _adjustCadTabIndex;
  }

  /**
   * @author Andy Mechtenberg
   */
  int getModifySubtypesViewSelectIndex()
  {
    return _modifySubtypeViewSelectIndex;
  }

  /**
   * @author Andy Mechtenberg
   */
  void setModifySubtypesViewSelectIndex(int viewSelectIndex)
  {
    _modifySubtypeViewSelectIndex = viewSelectIndex;
  }
  
  /**
   * @author Andy Mechtenberg
   */
  public int getTestModeSelection()
  {
    return _testModeSelection;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTestModeSelection(int selectionIndex)
  {
    _testModeSelection = selectionIndex;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTunerJointType(String jointType)
  {
    Assert.expect(jointType != null);

    _tunerJointType = jointType;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getTunerJointType()
  {
    Assert.expect(_tunerJointType != null);

    return _tunerJointType;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTunerSubtype(String subtype)
  {
    Assert.expect(subtype != null);
    _tunerSubtype = subtype;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getTunerSubtype()
  {
    return _tunerSubtype;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTunerComponent(String component)
  {
    Assert.expect(component != null);
    _tunerComponent = component;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getTunerComponent()
  {
    return _tunerComponent;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTunerPad(String pad)
  {
    Assert.expect(pad != null);
    _tunerPad = pad;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getTunerPad()
  {
    return _tunerPad;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTunerThresholdTabIndex(int tabIndex)
  {
    _tunerThresholdTabIndex = tabIndex;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getTunerThresholdTabIndex()
  {
    return _tunerThresholdTabIndex;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTunerBasicThresholds(boolean basicThresholds)
  {
    _tunerBasicThresholds = basicThresholds;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean getTunerBasicThresholds()
  {
    return _tunerBasicThresholds;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTunerThresholdRow(int thresholdRow)
  {
    _thresholdRow = thresholdRow;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getTunerThresholdRow()
  {
    return _thresholdRow;
  }


  /**
   * @author Laura Cormos
   */
  public void setJtaRulesSetComboboxSelectedIndex(int index)
  {
   _jtaRulesSetComboboxSelectedIndex = index;
  }

  /**
   * @author Laura Cormos
   */
  public int getJtaRulesSetComboboxSelectedIndex()
  {
   return _jtaRulesSetComboboxSelectedIndex;
  }
  
  /**
   * @author Andy Mechtenberg
   */
  public int getInitialThresholdsSetComboboxSelectedIndex()
  {
    return _initialThresholdsSetComboboxSelectedIndex;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setInitialThresholdsSetComboboxSelectedIndex(int index)
  {
    _initialThresholdsSetComboboxSelectedIndex = index;
  }
  
  /**
   * @author Andy Mechtenberg
   */
  public int getInitialThresholdsThresholdRow()
  {
    return _initialThresholdsThresholdRow;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setInitialThresholdsThresholdRow(int initialThresholdsThresholdRow)
  {
    _initialThresholdsThresholdRow = initialThresholdsThresholdRow;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getIntialThresholdsTabIndex()
  {
    return _intialThresholdsTabIndex;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setIntialThresholdsTabIndex(int intialThresholdsTabIndex)
  {
    this._intialThresholdsTabIndex = intialThresholdsTabIndex;
  }
  
  /**
   * @author Wei Chin
   */
  public boolean hasTunerJointType()
  {
    return (_tunerJointType != null);
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public int getInitialRecipeSettingSetComboboxSelectedIndex()
  {
    return _initialRecipeSettingSetComboboxSelectedIndex;
  }

  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   *
   * @author weng-jian.eoh
   * @return
   */
  public void setInitialRecipeSettingSetComboboxSelectedIndex(int index)
  {
    _initialRecipeSettingSetComboboxSelectedIndex = index;
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public int getInitialRecipeSettingRow()
  {
    return _initialRecipeSettingRow;
  }

  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public void setInitialRecipeSettingRow(int initialRecipeSettingRow)
  {
    _initialRecipeSettingRow = initialRecipeSettingRow;
  }
}
