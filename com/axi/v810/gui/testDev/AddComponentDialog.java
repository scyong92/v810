package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.text.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
public class AddComponentDialog extends EscapeDialog
{
  private boolean _ok = false;
  private java.util.List<LandPattern> _landPatterns;
  private Project _project;
  private BoardCoordinate _coordinate;
  private boolean _side1;
  private boolean _sideIsSet;
  private JDialog _this;
  private BoardType _boardType;

  private final String _ROTATION_0 = "0.0";
  private final String _ROTATION_90 = "90.0";
  private final String _ROTATION_180 = "180.0";
  private final String _ROTATION_270 = "270.0";
  private final String _CUSTOM = StringLocalizer.keyToString("CAD_CUSTOM_KEY");

  private DecimalFormat _xLocDataFormat = new DecimalFormat();
  private NumericRangePlainDocument _xLocDataDoc = new NumericRangePlainDocument();

  private DecimalFormat _yLocDataFormat = new DecimalFormat();
  private NumericRangePlainDocument _yLocDataDoc = new NumericRangePlainDocument();

  private BorderLayout _mainBorderLayout = new BorderLayout();
  private JPanel _selectPanel = new JPanel();
  private JPanel _okCancelPanel = new JPanel();
  private JPanel _okCancelInnerPanel = new JPanel();
  private GridLayout _okCancelInnerGridLayout = new GridLayout();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private FlowLayout _okCanelFowLayout = new FlowLayout();
  private PairLayout _selectPanelPairLayout = new PairLayout(10, 10, false);
  private JLabel _refDesLabel = new JLabel();
  private JComboBox _landPatternComboBox = new JComboBox();
  private JLabel _landPatternLabel = new JLabel();
  private JTextField _refDesTextField = new JTextField();
  private JLabel _xLocLabel = new JLabel();
  private NumericTextField _xLocTextField = new NumericTextField();
  private JLabel _yLocLabel = new JLabel();
  private NumericTextField _yLocTextField = new NumericTextField();
  private JLabel _rotationLabel = new JLabel();
  private JComboBox _rotationComboBox = new JComboBox();
  private JLabel _boardSideLabel = new JLabel();
  private JComboBox _boardSideComboBox = new JComboBox();
  private JLabel _boardTypeLabel = new JLabel();
  private JComboBox _boardTypeComboBox = new JComboBox();

  /**
   * @author Laura Cormos
   */
  public AddComponentDialog(Frame frame, String title, boolean modal, Project project, BoardType boardType,
                            java.util.List<LandPattern> landPatterns, BoardCoordinate coordinate, boolean side1)
  {
    super(frame, title, modal);

    Assert.expect(project != null);
    Assert.expect(boardType != null);
    Assert.expect(landPatterns != null);
    Assert.expect(coordinate != null);

    _project = project;
    _boardType = boardType;
    _landPatterns = landPatterns;
    _coordinate = coordinate;
    _side1 = side1;
    _sideIsSet = true;

    jbInit();

    pack();
  }

  /**
   * @author Laura Cormos
   */
  public AddComponentDialog(Frame frame, String title, boolean modal, java.util.List<LandPattern> landPatterns, Project project)
  {
    super(frame, title, modal);

    Assert.expect(project != null);

    _landPatterns = landPatterns;
    _project = project;

    jbInit();

    pack();
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit()
  {
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    getContentPane().setLayout(_mainBorderLayout);
    _okCancelInnerPanel.setLayout(_okCancelInnerGridLayout);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });

    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });

    _okCancelPanel.setLayout(_okCanelFowLayout);
    _okCanelFowLayout.setAlignment(FlowLayout.RIGHT);
    _okCancelInnerGridLayout.setHgap(20);
    _selectPanel.setLayout(_selectPanelPairLayout);
    _refDesLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _refDesLabel.setText(StringLocalizer.keyToString("CAD_ENTER_ITEM_NAME_KEY"));
    _landPatternLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _landPatternLabel.setText(StringLocalizer.keyToString("ACD_SELECT_LP_FOR_COMPONENT_KEY"));
    _xLocLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _xLocLabel.setText(StringLocalizer.keyToString("ACD_X_LOCATION_LABEL_KEY"));
    _yLocLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _yLocLabel.setText(StringLocalizer.keyToString("ACD_Y_LOCATION_LABEL_KEY"));
    _rotationLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _rotationLabel.setText(StringLocalizer.keyToString("ACD_ROTATION_LABEL_KEY"));
    _boardSideLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _boardSideLabel.setText(StringLocalizer.keyToString("ACD_BOARD_SIDE_LABEL_KEY"));
    _boardTypeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _boardTypeLabel.setText(StringLocalizer.keyToString("ACD_BOARD_TYPE_LABEL_KEY"));
    _selectPanel.setBorder(BorderFactory.createEmptyBorder(10,10,20,10));
    _okCancelPanel.setBorder(BorderFactory.createEmptyBorder(0,0,5,5));
    getContentPane().add(_selectPanel, BorderLayout.CENTER);
    _selectPanel.add(_refDesLabel);
    _selectPanel.add(_refDesTextField);
    _selectPanel.add(_landPatternLabel);
    _selectPanel.add(_landPatternComboBox);
    _selectPanel.add(_boardTypeLabel);
    _selectPanel.add(_boardTypeComboBox);
    _selectPanel.add(_boardSideLabel);
    _selectPanel.add(_boardSideComboBox);
    _selectPanel.add(_xLocLabel);
    _selectPanel.add(_xLocTextField);
    _selectPanel.add(_yLocLabel);
    _selectPanel.add(_yLocTextField);
    _selectPanel.add(_rotationLabel);
    _selectPanel.add(_rotationComboBox);
    getContentPane().add(_okCancelPanel,  BorderLayout.SOUTH);
    _okCancelPanel.add(_okCancelInnerPanel, null);
    _okCancelInnerPanel.add(_okButton, null);
    _okCancelInnerPanel.add(_cancelButton, null);
    _refDesTextField.setColumns(10);

    _boardTypeComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        boardTypeComboBox_actionPerformed();
      }
    });
    FocusAdapter textFieldFocusAdapter = new FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        ((NumericTextField)e.getSource()).selectAll();
      }
    };
    _xLocTextField.addFocusListener(textFieldFocusAdapter);
    _yLocTextField.addFocusListener(textFieldFocusAdapter);

    for (Object lp : _landPatterns)
    {
      if (lp instanceof LandPattern)
        _landPatternComboBox.addItem(lp);
    }
    getRootPane().setDefaultButton(_okButton);

    // if this is a multiple board type project, set up the board type combo box, otherwise, cut it
    java.util.List<BoardType> boardTypes = _project.getPanel().getBoardTypes();
    if (boardTypes.size() > 1)
    {
      for (BoardType boardType : boardTypes)
        _boardTypeComboBox.addItem(boardType);
      if (_boardType != null)
        _boardTypeComboBox.setSelectedItem(_boardType);
      else
        _boardType = (BoardType)_boardTypeComboBox.getSelectedItem();
    }
    else
    {
      _selectPanel.remove(_boardTypeLabel);
      _selectPanel.remove(_boardTypeComboBox);
      _boardType = _project.getPanel().getBoardTypes().get(0);
    }

    _xLocTextField.setColumns(8);
    _yLocTextField.setColumns(8);

    // set up the data document for the coordinate text fields
    _xLocDataDoc.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _xLocTextField.setDocument(_xLocDataDoc);
    _yLocDataDoc.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _yLocTextField.setDocument(_yLocDataDoc);

    // set up the proper number of decimal digits based on the selected units
    StringBuffer decimalFormat = new StringBuffer("########0.");

    int decimalPlaces = MeasurementUnits.getMeasurementUnitDecimalPlaces(_project.getDisplayUnits());
    for(int i = 0; i < decimalPlaces; i++)
      decimalFormat.append("0");  // use a "0" if you want to show trailing zeros, use a "#" if you don't

    _xLocDataFormat.applyPattern(decimalFormat.toString());
    _xLocTextField.setFormat(_xLocDataFormat);
    _yLocDataFormat.applyPattern(decimalFormat.toString());
    _yLocTextField.setFormat(_yLocDataFormat);

    if (_coordinate != null)
    {
      double xLocInDisplayUnits = MathUtil.convertUnits(_coordinate.getX(), MathUtilEnum.NANOMETERS, _project.getDisplayUnits());
      double yLocInDisplayUnits = MathUtil.convertUnits(_coordinate.getY(), MathUtilEnum.NANOMETERS, _project.getDisplayUnits());
      _xLocTextField.setValue(xLocInDisplayUnits);
      _yLocTextField.setValue(yLocInDisplayUnits);
    }

     _this = this;
    // set up the rotation combo box
    _rotationComboBox.setAlignmentX(JComboBox.RIGHT_ALIGNMENT);

    _rotationComboBox.addItem(_ROTATION_0);
    _rotationComboBox.addItem(_ROTATION_90);
    _rotationComboBox.addItem(_ROTATION_180);
    _rotationComboBox.addItem(_ROTATION_270);
    _rotationComboBox.addItem(_CUSTOM);
    _rotationComboBox.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        if (((String)_rotationComboBox.getSelectedItem()).equals(_CUSTOM))
        {
          boolean validInput = false;
          while (validInput == false)
          {
            String degrees = JOptionPane.showInputDialog(_this,
                                                         StringLocalizer.keyToString("RCE_ENTER_ROT_DEGREES_KEY"),
                                                         StringLocalizer.keyToString("RCE_ROT_DEGREES_DIALOG_TITLE_KEY"),
                                                         JOptionPane.INFORMATION_MESSAGE);
            if (degrees != null)
            {
              Double degrees0To359Double;
              try
              {
                degrees0To359Double = MathUtil.getDegreesWithin0To359(Double.valueOf(degrees));
              }
              catch(NumberFormatException ex)
              {
                MessageDialog.showErrorDialog(_this, StringLocalizer.keyToString(new LocalizedString("CAD_INVALID_INPUT_KEY",
                                                                                                    new Object[]{degrees})),
                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
                continue;
              }
              validInput = true;
              String degrees0To359String = String.valueOf(MathUtil.getDegreesWithin0To359(Double.valueOf(degrees)));
              if (degrees0To359Double != 0 && degrees0To359Double != 90 && degrees0To359Double != 180 && degrees0To359Double != 270)
              {
                _rotationComboBox.addItem(degrees0To359String);
              }
              _rotationComboBox.setSelectedItem(degrees0To359String);
            }
            else // if user canceled the dialog we don't know what was previously selected so we'll just default to the first item in the list
            {
              validInput = true; // want to break out of the loop if dialog was cancelled
            }
          }
        }
      }
    });

    // set up the board side combo box, if known
    _boardSideComboBox.addItem(StringLocalizer.keyToString("CAD_SIDE1_KEY"));
    _boardSideComboBox.addItem(StringLocalizer.keyToString("CAD_SIDE2_KEY"));
    if (_sideIsSet)
    {
      if (_side1)
        _boardSideComboBox.setSelectedItem(StringLocalizer.keyToString("CAD_SIDE1_KEY"));
      else
        _boardSideComboBox.setSelectedItem(StringLocalizer.keyToString("CAD_SIDE2_KEY"));
    }
  }

  /**
   * @author Laura Cormos
   */
  public boolean userClickedOk()
  {
    return _ok;
  }

  /**
   * @author Laura Cormos
   */
  public String getReferenceDesignator()
  {
    return _refDesTextField.getText();
  }

  /**
   * @author Laura Cormos
   */
  public LandPattern getLandPattern()
  {
    return (LandPattern)_landPatternComboBox.getSelectedItem();
  }

  /**
   * @author Laura Cormos
   */
  public String getBoardSide()
  {
    return (String)_boardSideComboBox.getSelectedItem();
  }

  /**
   * @author Laura Cormos
   */
  public BoardType getBoardType()
  {
    if (_boardType != null)
      return _boardType;
    else
      return (BoardType)_boardTypeComboBox.getSelectedItem();
  }

  /**
   * @author Laura Cormos
   */
  public BoardCoordinate getBoardCoordinate()
  {
    Assert.expect(_coordinate != null);

    return _coordinate;
  }

  /**
   * @author Laura Cormos
   */
  public double getRotation()
  {
    String rotationString = (String)_rotationComboBox.getSelectedItem();
    return new Double(rotationString).doubleValue();
  }

  /**
   * @author Laura Cormos
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    _ok = false;
    _project = null;
    dispose();
  }

  /**
   * @author Laura Cormos
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    double xLocInNanos = 0.0;
    double yLocInNanos = 0.0;
    String errorMessage = new String();

    String refDes = _refDesTextField.getText();
    if (refDes.length() > 0)
    {
      if (_boardType.isComponentTypeReferenceDesignatorValid(refDes) == false)
      {
        // pop dialog about invalid name
        String illegalChars = _boardType.getComponentTypeReferenceDesignatorIllegalChars();
        LocalizedString message = null;
        if (illegalChars.equals(" "))
          message = new LocalizedString("CAD_INVALID_COMPONENT_NAME_SPACES_KEY", new Object[]
                                        {refDes});
        else
          message = new LocalizedString("CAD_INVALID_COMPONENT_NAME_OTHER_CHARS_KEY", new Object[]
                                        {refDes, illegalChars});
        errorMessage += StringLocalizer.keyToString(message) + "\n";
      }
      else if (_boardType.isComponentTypeReferenceDesignatorDuplicate(refDes))
      {
        errorMessage += StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY", new Object[]{refDes}))
                        + "\n";
      }
    }
    else
      errorMessage += StringLocalizer.keyToString("ACD_MISSING_REF_DES_KEY") + "\n";
    try
    {
      xLocInNanos = MathUtil.convertUnits(_xLocTextField.getDoubleValue(), _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
    }
    catch (ParseException ex)
    {
      errorMessage += StringLocalizer.keyToString("ACD_X_LOC_VALUE_KEY") + "\n";
    }
    try
    {
      yLocInNanos = MathUtil.convertUnits(_yLocTextField.getDoubleValue(), _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
    }
    catch (ParseException ex)
    {
      errorMessage += StringLocalizer.keyToString("ACD_Y_LOC_VALUE_KEY") + "\n";
    }
    if (errorMessage.length() > 0)
    {
      MessageDialog.showErrorDialog(_this,
                                    errorMessage,
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
      return;
    }
    _coordinate = new BoardCoordinate((int)xLocInNanos, (int)yLocInNanos);
    _ok = true;
    _project = null;
    dispose();
  }


  /**
   * @author Laura Cormos
   */
  private void boardTypeComboBox_actionPerformed()
  {
    BoardType boardType = (BoardType)_boardTypeComboBox.getSelectedItem();
    _boardType = boardType;
  }

  /**
   * @author Laura Cormos
   */
  public void clear()
  {
    _landPatterns = null;
    _landPatternComboBox.removeAllItems();
    _boardSideComboBox.removeAllItems();
    _boardTypeComboBox.removeAllItems();
    _project = null;
    _coordinate = null;
    _boardType = null;
  }
}
