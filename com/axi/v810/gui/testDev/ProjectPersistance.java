package com.axi.v810.gui.testDev;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class is responsible for persisting all project releated information.
 *
 * @author Erica Wheatcroft
 */
public class ProjectPersistance implements Serializable
{
  public static final String NO_CONTENT_STRING = "";

  // verify cad fields
  private String _verifyCadBoardName;
  private String _verifyCadBoardSide;
  private String _verifyCadCurrentComponent;

  // initial tuning fields
  private String _initialTuningSelectedJointType;
  private String _initialTuningSelectedSubtype;
  private String _initialTuningSelectedPanelBoard;
  private boolean _subtypeLearningChecked = true;
  private boolean _slopeLearningChecked = false; // Siew Yeng - XCR-2139
  private boolean _shortLearningChecked = true;
  private boolean _expectedImageLearningChecked = false;

  // tunner settings
  private String _tunerBoardTypeName;

  private boolean _measDiagsChecked = false;
  private boolean _shortDiagsChecked = false;
  private boolean _diagsChecked = false;

  private boolean _algorithmsDisabledChecked = false;
  private int _testModeSelection = 0;
  private String _tunerJointType;
  private String _tunerSubtype;
  private String _tunerComponent;
  private String _tunerPad;
  private String _tunerBoardSelection;

  // Review Defects settings
  private boolean _showOnlyPassingPinInformation = false;

  // select image set panel settings
  private List<String> _selectedImageSetNames;

  // Filtering information saved for review measurements.
  private MeasurementEnum _measurementType;
  private SliceNameEnum _sliceName;

  private boolean _showUnTestableReasonsInformation = false; //XCR1374, KhangWah, 2011-09-20

  /**
   * @author Erica Wheatcroft
   */
  public ProjectPersistance()
  {
    _verifyCadBoardSide = "Both";

    _initialTuningSelectedJointType = NO_CONTENT_STRING;
    _initialTuningSelectedSubtype = NO_CONTENT_STRING;
    _initialTuningSelectedPanelBoard = NO_CONTENT_STRING;

    _tunerBoardTypeName = NO_CONTENT_STRING;
    _tunerJointType = NO_CONTENT_STRING;
    _tunerSubtype = NO_CONTENT_STRING;

    _tunerComponent = NO_CONTENT_STRING;
    _tunerPad = NO_CONTENT_STRING;
    _tunerBoardSelection = NO_CONTENT_STRING;

    _selectedImageSetNames = new ArrayList<String>();
  }


  /**
   * @author George Booth
   */
  public void setVerifyCadBoardName(String name)
  {
    Assert.expect(name != null);
    _verifyCadBoardName = name;
  }

  /**
   * @author George Booth
   */
  public String getVerifyCadBoardName()
  {
    return _verifyCadBoardName;
  }

  /**
   * @author George Booth
   */
  public void setVerifyCadBoardSide(String side)
  {
    Assert.expect(side != null);
    _verifyCadBoardSide = side;
  }

  /**
   * @author George Booth
   */
  public String getVerifyCadBoardSide()
  {
    Assert.expect(_verifyCadBoardSide != null);
    return _verifyCadBoardSide;
  }

  /**
   * @author George Booth
   */
  public void setVerifyCadCurrentComponent(String componentName)
  {
    _verifyCadCurrentComponent = componentName;
  }

  /**
   * @author George Booth
   */
  public String getVerifyCadCurrentComponent()
  {
    return _verifyCadCurrentComponent;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTunerBoardTypeName(String boardname)
  {
    Assert.expect(boardname != null);
    _tunerBoardTypeName = boardname;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getTunerTypeBoardName()
  {
    return _tunerBoardTypeName;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTunerJointType(String jointType)
  {
    Assert.expect(jointType != null);

    _tunerJointType = jointType;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getTunerJointType()
  {
    Assert.expect(_tunerJointType != null);

    return _tunerJointType;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTunerSubtype(String subtype)
  {
    Assert.expect(subtype != null);
    _tunerSubtype = subtype;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getTunerSubtype()
  {
    return _tunerSubtype;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTunerComponent(String component)
  {
    Assert.expect(component != null);
    _tunerComponent = component;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getTunerComponent()
  {
    return _tunerComponent;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTunerPad(String pad)
  {
    Assert.expect(pad != null);
    _tunerPad = pad;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getTunerPad()
  {
    return _tunerPad;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getTestModeSelection()
  {
    return _testModeSelection;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTestModeSelection(int selectionIndex)
  {
    _testModeSelection = selectionIndex;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getBoardSelection()
  {
    return _tunerBoardSelection;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setBoardSelection(String board)
  {
    Assert.expect(board != null);
    _tunerBoardSelection = board;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean getMeasDiagsChecked()
  {
    return _measDiagsChecked;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean getShortDiagsChecked()
  {
    return _shortDiagsChecked;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean getDiagsChecked()
  {
    return _diagsChecked;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setMeasDiagsChecked(boolean diags)
  {
    _measDiagsChecked = diags;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setShortDiagsChecked(boolean diags)
  {
    _shortDiagsChecked = diags;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setDiagsChecked(boolean diags)
  {
    _diagsChecked = diags;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean getAlgorithmsDisabledChecked()
  {
    return _algorithmsDisabledChecked;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setAlgorithmsDisabledChecked(boolean algorithmsDisabled)
  {
    _algorithmsDisabledChecked = algorithmsDisabled;
  }

  /**
   * set Review Defects passing test information flag
   * @param passing true if showing pasing tests
   * @author George Booth
   */
  public void setPassingPinInformationFlag(boolean passing)
  {
    _showOnlyPassingPinInformation = passing;
  }

  /**
   * get Review Defects passing test information flag
   * @return true if showing pasing tests
   * @author George Booth
   */
  public boolean getPassingPinInformationFlag()
  {
    return _showOnlyPassingPinInformation;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setInitialTuningSelectedJointType(String jointType)
  {
    Assert.expect(jointType != null);
    _initialTuningSelectedJointType = jointType;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getInitialTuningSelectedJointType()
  {
    Assert.expect(_initialTuningSelectedJointType != null);
    return _initialTuningSelectedJointType;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setInitialTuningSelectedSubtype(String subtype)
  {
    Assert.expect(subtype != null);
    _initialTuningSelectedSubtype = subtype;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getInitialTuningSelectedSubtype()
  {
    Assert.expect(_initialTuningSelectedSubtype != null);
    return _initialTuningSelectedSubtype;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setInitialTuningSelectedPanelBoard(String panelBoard)
  {
    Assert.expect(panelBoard != null);
    _initialTuningSelectedPanelBoard = panelBoard;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public String getInitialTuningSelectedPanelBoard()
  {
    Assert.expect(_initialTuningSelectedPanelBoard != null);
    return _initialTuningSelectedPanelBoard;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setSubtypeLearningChecked(boolean checked)
  {
    _subtypeLearningChecked = checked;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean getSubtypeLearningChecked()
  {
    return _subtypeLearningChecked;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setShortLearningChecked(boolean checked)
  {
    _shortLearningChecked = checked;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean getShortLearningChecked()
  {
    return _shortLearningChecked;
  }

  /**
   * @author George Booth
   */
  public void setExpectedImageLearningChecked(boolean checked)
  {
    _expectedImageLearningChecked = checked;
  }

  /**
   * @author George Booth
   */
  public boolean getExpectedImageLearningChecked()
  {
    return _expectedImageLearningChecked;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setSelectedImageSetNames(List<String> selectedImageSetNames)
  {
    Assert.expect(selectedImageSetNames != null, "Select image set names is null");
    _selectedImageSetNames = selectedImageSetNames;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void addSelectedImageSetName(String selectedImageSetName)
  {
    Assert.expect(selectedImageSetName != null, "Select image set name is null");
    _selectedImageSetNames.add(selectedImageSetName);
  }

  /**
   * @author Erica Wheatcroft
   */
  public void removeSelectedImageSetName(String selectedImageSetName)
  {
    Assert.expect(selectedImageSetName != null, "Select image set names is null");
    if(_selectedImageSetNames.contains(selectedImageSetName))
      _selectedImageSetNames.remove(selectedImageSetName);
  }

  /**
   * @author Erica WHeatcroft
   */
  public void clearSelectedImageSets()
  {
    _selectedImageSetNames.clear();
  }
  /**
   * @author Erica Wheatcroft
   */
  public List<String> getSelectedImageSetNames()
  {
    Assert.expect(_selectedImageSetNames != null, "Select image set names is null");
    return _selectedImageSetNames;
  }

  /**
   * @author Rex Shang
   */
  public void setMeasurementType(MeasurementEnum measurementType)
  {
    _measurementType = measurementType;
  }

  /**
   * @author Rex Shang
   */
  public MeasurementEnum getMeasurementType()
  {
    return _measurementType;
  }

  /**
   * @author Rex Shang
   */
  public void setSliceName(SliceNameEnum sliceName)
  {
    _sliceName = sliceName;
  }

  /**
   * @author Rex Shang
   */
  public SliceNameEnum getSliceName()
  {
    return _sliceName;
  }

  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  static public ProjectPersistance readSettings(String projectName)
  {
    ProjectPersistance persistance = null;
    try
    {
      if (FileUtilAxi.exists(FileName.getGuiStatePersistFullPath(projectName)))
        persistance = (ProjectPersistance)FileUtilAxi.loadObjectFromSerializedFile(FileName.getGuiStatePersistFullPath(projectName));
      else
      {
        persistance = new ProjectPersistance();
      }
    }
    catch (DatastoreException de)
    {
      // do nothing..
      persistance = new ProjectPersistance();
    }

    Assert.expect(persistance != null, "persistence is null");
    return persistance;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void writeSettings(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);
    FileUtilAxi.saveObjectToSerializedFile(this, FileName.getGuiStatePersistFullPath(projectName));
  }

  /**
   * @author Chnee Khang Wah, XCR1374
   */
  public boolean showUnTestableReasonsInformation()
  {
    return _showUnTestableReasonsInformation;
  }

  /**
   * @author Chnee Khang Wah. XCR1374
   */
  public void setShowUnTestableReasonsInformation(boolean showUnTestableReasonsInformation)
  {
    _showUnTestableReasonsInformation = showUnTestableReasonsInformation;
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean getSlopeLearningChecked()
  {
    return _slopeLearningChecked;
  }
  
  /**
   * @author Siew Yeng
   */
  public void setSlopeLearningChecked(boolean checked)
  {
    _slopeLearningChecked = checked;
  }
}
