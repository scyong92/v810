package com.axi.v810.gui.testDev;

import java.awt.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;

/**
 * This class displays the information for all the land pattern pads for the selected land pattern
 * @author Laura Cormos
 */
class LandPatternDetailsTable extends JSortTable
{
  private Component _parent;
  private static final int _PAD_NAME_INDEX = 0;
  private static final int _PAD_X_INDEX = 1;
  private static final int _PAD_Y_INDEX = 2;
  private static final int _PAD_DX_INDEX = 3;
  private static final int _PAD_DY_INDEX = 4;
  private static final int _PAD_ROTATION_INDEX = 5;
  private static final int _PAD_SHAPE_INDEX = 6;
  private static final int _PAD_TYPE_INDEX = 7;
//  private static final int _PAD_HOLE_INDEX = 8;
  //Siew Yeng - XCR-3318 - Oval PTH
  private static final int _PAD_HOLE_DX_INDEX = 8;
  private static final int _PAD_HOLE_DY_INDEX = 9;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _PAD_NAME_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _PAD_X_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _PAD_Y_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _PAD_DX_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _PAD_DY_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _PAD_ROTATION_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _PAD_SHAPE_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _PAD_TYPE_COLUMN_WIDTH_PERCENTAGE = .1;
//  private static final double _PAD_HOLE_COLUMN_WIDTH_PERCENTAGE = .111;
  private static final double _PAD_HOLE_DX_COLUMN_WIDTH_PERCENTAGE = .1;
  private static final double _PAD_HOLE_DY_COLUMN_WIDTH_PERCENTAGE = .1;

  private NumericRenderer _numericRenderer = new NumericRenderer(2, JLabel.RIGHT);

  private NumericEditor _xNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2);
  private NumericEditor _yNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2);
  private NumericEditor _dxNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2, 0.0, Double.MAX_VALUE);
  private NumericEditor _dyNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2, 0.0, Double.MAX_VALUE);
  private NumericEditor _holeNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2);
  private RotationCellEditor _rotationCellEditor;
  private ShapeCellEditor _shapeCellEditor = new ShapeCellEditor();
  private SMorTHPadTypeCellEditor _padTypeEditor = new SMorTHPadTypeCellEditor();
  
  private boolean _isOffsetBeenSet = false;

  // minimum size for a land pattern pad
  private final int _ONE_MIL_IN_NANOS = 25400;

  /**
   * Default constructor.
   * @author Carli Connally
   */
  public LandPatternDetailsTable(Component parent)
  {
    Assert.expect(parent != null);
    _parent = parent;
    _rotationCellEditor = new RotationCellEditor(parent, true);
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Carli Connally
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_PAD_NAME_INDEX).setPreferredWidth((int)(totalColumnWidth * _PAD_NAME_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PAD_X_INDEX).setPreferredWidth((int)(totalColumnWidth * _PAD_X_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PAD_Y_INDEX).setPreferredWidth((int)(totalColumnWidth * _PAD_Y_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PAD_DX_INDEX).setPreferredWidth((int)(totalColumnWidth * _PAD_DX_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PAD_DY_INDEX).setPreferredWidth((int)(totalColumnWidth * _PAD_DY_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PAD_ROTATION_INDEX).setPreferredWidth((int)(totalColumnWidth * _PAD_ROTATION_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PAD_SHAPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _PAD_SHAPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PAD_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _PAD_TYPE_COLUMN_WIDTH_PERCENTAGE));
//    columnModel.getColumn(_PAD_HOLE_INDEX).setPreferredWidth((int)(totalColumnWidth * _PAD_HOLE_COLUMN_WIDTH_PERCENTAGE));
    //Siew Yeng - XCR-3318 - Oval PTH
    columnModel.getColumn(_PAD_HOLE_DX_INDEX).setPreferredWidth((int)(totalColumnWidth * _PAD_HOLE_DX_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_PAD_HOLE_DY_INDEX).setPreferredWidth((int)(totalColumnWidth * _PAD_HOLE_DY_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * @author Andy Mechtenberg
   * @author Laura Cormos
   */
  public void setEditorsAndRenderers(Set<Double> customRotationValues)
  {
    Assert.expect(customRotationValues != null);

    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };
    columnModel.getColumn(_PAD_NAME_INDEX).setCellEditor(stringEditor);

    //Chin-Seong, Kee - Change to handle comboBox to have set X location or Offset X
    //Chin-Seong, kee - this is for Offset X, Y use, when need handle multiple pad in the sametime
    //                  for e.g. BGA can mutliple select whole region which user desired and make the offset
    columnModel.getColumn(_PAD_X_INDEX).setCellEditor(_xNumericEditor);
    columnModel.getColumn(_PAD_X_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(_PAD_Y_INDEX).setCellEditor(_yNumericEditor);
    columnModel.getColumn(_PAD_Y_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(_PAD_DX_INDEX).setCellEditor(_dxNumericEditor);
    columnModel.getColumn(_PAD_DX_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(_PAD_DY_INDEX).setCellEditor(_dyNumericEditor);
    columnModel.getColumn(_PAD_DY_INDEX).setCellRenderer(_numericRenderer);

    _rotationCellEditor.removeCustomValues();
    _rotationCellEditor.setCustomValues(customRotationValues);
    columnModel.getColumn(_PAD_ROTATION_INDEX).setCellEditor(_rotationCellEditor);
    columnModel.getColumn(_PAD_ROTATION_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(_PAD_SHAPE_INDEX).setCellEditor(_shapeCellEditor);

    columnModel.getColumn(_PAD_TYPE_INDEX).setCellEditor(_padTypeEditor);

//    columnModel.getColumn(_PAD_HOLE_INDEX).setCellEditor(_holeNumericEditor);
//    columnModel.getColumn(_PAD_HOLE_INDEX).setCellRenderer(_numericRenderer);
    //Siew Yeng - XCR-3318 - Oval PTH
    columnModel.getColumn(_PAD_HOLE_DX_INDEX).setCellEditor(_holeNumericEditor);
    columnModel.getColumn(_PAD_HOLE_DX_INDEX).setCellRenderer(_numericRenderer);
    
    columnModel.getColumn(_PAD_HOLE_DY_INDEX).setCellEditor(_holeNumericEditor);
    columnModel.getColumn(_PAD_HOLE_DY_INDEX).setCellRenderer(_numericRenderer);
  }

  /**
   * @author George Booth
   */
  public void resetEditorsAndRenderers()
  {
     _rotationCellEditor.removeCustomValues();
  }

  /**
   * The decimal places to be displayed can change based on the measurement value that
   * is being displayed.  This method changes the renderers/editors to display the appropriate
   * number of decimal places.
   * @param enumSelectedUnits The project measurement units, selected by user
   * @author Carli Connally
   */
  public void setDecimalPlacesToDisplay(MathUtilEnum enumSelectedUnits)
  {
    Assert.expect(enumSelectedUnits != null);

    int decimalPlaces = MeasurementUnits.getMeasurementUnitDecimalPlaces(enumSelectedUnits);

    _xNumericEditor.setDecimalPlaces(decimalPlaces);
    _yNumericEditor.setDecimalPlaces(decimalPlaces);
    _dxNumericEditor.setDecimalPlaces(decimalPlaces);
    _dyNumericEditor.setDecimalPlaces(decimalPlaces);
    _holeNumericEditor.setDecimalPlaces(decimalPlaces);
    _numericRenderer.setDecimalPlacesToDisplay(decimalPlaces);
  }

  /**
   * @author Laura Cormos
   */
  public void updateEditorWithDisplayUnits(MathUtilEnum displayUnitEnum)
  {
    Assert.expect(displayUnitEnum != null);
    double minValue = MathUtil.convertUnits(_ONE_MIL_IN_NANOS, MathUtilEnum.NANOMETERS, displayUnitEnum);

    Range range = new DoubleRange(minValue, Double.MAX_VALUE);

    _dxNumericEditor.setRange(range);
    _dyNumericEditor.setRange(range);
  }

  /**
   * @author Laura Cormos
   */
  void setRotationEditorFor2PinDevice()
  {
    _rotationCellEditor.removeCustomValues();
    _rotationCellEditor.removeCustomValue();
    _rotationCellEditor.remove90And270Values();
  }

  /**
   * @author Laura Cormos
   */
  void setRotationEditorForNon2PinDevices(Set<Double> customRotationValues)
  {
    Assert.expect(customRotationValues != null);

    _rotationCellEditor = new RotationCellEditor(_parent, customRotationValues);
    columnModel.getColumn(_PAD_ROTATION_INDEX).setCellEditor(_rotationCellEditor);
  }

 //Chin-Seong, kee - this is for Offset X, Y use, when need handle multiple pad in the sametime
  //                  for e.g. BGA can mutliple select whole region which user desired and make the offset
  public void setIsOffsetBeenSet(boolean isOffset)
  {
    _isOffsetBeenSet = isOffset;
  }
  
 //Chin-Seong, kee - this is for Offset X, Y use, when need handle multiple pad in the sametime
  //                  for e.g. BGA can mutliple select whole region which user desired and make the offset
  public boolean isOffsetBeenSet()
  {
    return _isOffsetBeenSet;
  }
//  /**
//   * @author Laura Cormos
//   */
//  public boolean editCellAt(int row, int column, EventObject e)
//  {
//    if (e instanceof MouseEvent)
//    {
//
//    }
//    return super.editCellAt(row, column, e);
//  }
}
