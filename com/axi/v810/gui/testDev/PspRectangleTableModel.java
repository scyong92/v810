package com.axi.v810.gui.testDev;

import java.util.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.mainMenu.*;

/**
 * @author Cheah Lee Herng
 */
public class PspRectangleTableModel extends DefaultSortTableModel 
{
  private MainMenuGui _mainUI;

  private static final String _BOARD_NAME = StringLocalizer.keyToString("MMGUI_SETUP_BOARD_NAME_KEY");
  private static final String _RECTANGLE_NAME = StringLocalizer.keyToString("PSP_OPTICAL_RECTANGLE_KEY");
  private static final String _Z_HEIGHT_VALUE = StringLocalizer.keyToString("PSP_OPTICAL_RECTANGLE_ZHEIGHT_KEY");
  private static final String _USABILITY_STATUS = StringLocalizer.keyToString("PSP_OPTICAL_RECTANGLE_USABILITY_STATUS_KEY");

  private String[] _columnLabels = { _BOARD_NAME,
                                     _RECTANGLE_NAME,
                                     _Z_HEIGHT_VALUE,
                                     _USABILITY_STATUS,
                                   };

  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  private Project _project = null;
  private List<Board> _boards;
  private List<Board> _selectedBoards;
  private List<OpticalCameraRectangle> _opticalRectangles  = new ArrayList<OpticalCameraRectangle>();
  private OpticalRegion _currentOpticalRegion = null;
  private List<OpticalCameraRectangle> _opticalRectanglesWithZoom  = new ArrayList<OpticalCameraRectangle>();
  private MathUtilEnum _displayUnit = MathUtilEnum.MILLIMETERS;
  private static OpticalCameraRectangleComparator _opticalCameraRectangleComparator;
  
  static
  {
    _opticalCameraRectangleComparator = new OpticalCameraRectangleComparator(true, OpticalCameraRectangleComparatorEnum.BOARD_NAME);
  }

  /**
   * @author Cheah Lee Herng
   */
  public PspRectangleTableModel()
  {
    _mainUI = MainMenuGui.getInstance();
  }

  /**
   * @author Cheah Lee Herng
   */
  private void clearProjectData()
  {
    _project = null;
    if (_boards != null)
      _boards.clear();
    if (_selectedBoards != null)
      _selectedBoards.clear();
    _currentOpticalRegion = null;
  }

  /**
   * @author Cheah Lee Herng
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * @author Andy Mechtenberg
  */
  public int getRowCount()
  {
    if (_opticalRectangles == null)
      return 0;
    else
      return _opticalRectangles.size();
  }
  
  /**
   * XCR-3600 Assert at surface map tab when trying to select component at graphic UI
   * @author Cheah Lee Herng
   */
  public boolean hasOpticalRectanglesUsingRowIndex(int rowIndex)
  {
    boolean hasOpticalRectanglesUsingRowIndex = true;
    try
    {
      OpticalCameraRectangle opticalCameraRectangle = _opticalRectangles.get(rowIndex);
    }
    catch(ArrayIndexOutOfBoundsException ex)
    {
      hasOpticalRectanglesUsingRowIndex = false;
    }
    return hasOpticalRectanglesUsingRowIndex;
  }

  /**
   * @author Cheah Lee Herng
  */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * @author Cheah Lee Herng
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_project != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    OpticalCameraRectangle opticalCameraRectangle = _opticalRectangles.get(rowIndex);
    
    boolean bool = false;     
    bool = _opticalRectangles.get(rowIndex).getInspectableBool();

    //float zHeight = 0;
    //zHeight = _opticalRectangles.get(rowIndex).getZHeight();
    double zHeightInNanometer = _opticalRectangles.get(rowIndex).getZHeightInNanometer();
    double zHeight = MathUtil.convertUnits(zHeightInNanometer, MathUtilEnum.NANOMETERS, _displayUnit);
    zHeight = MathUtil.roundToPlaces(zHeight, 4);
    
    String boardName = "";
    boardName = _opticalRectangles.get(rowIndex).getBoard().getName();
    
    if (columnIndex == 0) // board name
    {
      //return  _currentOpticalRegion.getBoards();
      return boardName;
    }
    else if (columnIndex == 1) // optical rectangle
    {
      return opticalCameraRectangle;
    }
    else if (columnIndex == 2) // optical rectangle z-height
    {
      return zHeight;
    }
    else if (columnIndex == 3) // optical rectangle usability status
    {
      return bool;
    }

    return null;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    boolean isEditable = false;

    return isEditable;
  }
    
  /**
   * @author Cheah Lee Herng
   */
  void populateWithPanelData(BoardType selectedBoardType)
  {
    _opticalRectangles.clear();
    if (selectedBoardType.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      for (OpticalRegion opticalRegion: selectedBoardType.getPanel().getPanelOpticalRegions())
      {
        _opticalRectangles = opticalRegion.getAllOpticalCameraRectangles();
      }
    }
    else // board-based
    {
      for (Board board : selectedBoardType.getBoards())
      {
        for (OpticalRegion opticalRegion: board.getBoardSurfaceMapSettings().getOpticalRegions())
        {
          for (OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getOpticalCameraRectangles(board))
          {
            if (_opticalRectangles.contains(opticalCameraRectangle) == false)
              _opticalRectangles.add(opticalCameraRectangle);
          }
        }
      }
    }

    _project = selectedBoardType.getPanel().getProject();

    sortColumn(0 ,true);
    fireTableDataChanged();  
  }
    
  /**
   * @author Cheah Lee Herng
   * @author Ying-Huan.Chu
   */
  void populateWithOpticalRegion(OpticalRegion opticalRegion)
  {
    //Assert.expect(opticalRegion != null);
    if (opticalRegion != null)
    {
      if (_project.getPanel().getPanelSettings().isPanelBasedAlignment())
        _opticalRectangles = opticalRegion.getAllOpticalCameraRectangles();
      else
      {
        for (Board board : _project.getPanel().getBoards())
        {
          for (OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getOpticalCameraRectangles(board))
          {
            if (_opticalRectangles.contains(opticalCameraRectangle) == false)
              _opticalRectangles.add(opticalCameraRectangle);
          }
        }
      }
      _currentOpticalRegion = opticalRegion;
    }
    else
    {
      _opticalRectangles.clear();
      _currentOpticalRegion = null;
    }
    
    fireTableDataChanged();
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {
    _opticalRectangles.add(opticalCameraRectangle);
  }

  /**
   * @author Jack Hwee
   */
  public void setOpticalCameraRectangleWithZoom(OpticalCameraRectangle opticalCameraRectangle)
  {
    _opticalRectanglesWithZoom.add(opticalCameraRectangle);
  }

  /**
   * @author Jack Hwee
   */
  public void clearOpticalCameraRectangleWithZoom()
  {
    if (_opticalRectanglesWithZoom.isEmpty() == false)
      _opticalRectanglesWithZoom.clear();
  }
    
  /**
   * @author Jack Hwee
   */
  int getIndexForOpticalCameraRectangleWithZoom(OpticalCameraRectangle opticalCameraRectangle)
  {
    Assert.expect(opticalCameraRectangle != null);
    return _opticalRectanglesWithZoom.indexOf(opticalCameraRectangle);
  }
    
  /**
   * @author Cheah Lee Herng
   */
  public java.util.List<OpticalCameraRectangle> getOpticalCameraRectangles()
  {
    Assert.expect(_opticalRectangles != null);
    return _opticalRectangles;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {
    if (_opticalRectangles.isEmpty() == false)
      _opticalRectangles.clear();
    fireTableDataChanged();
  }

  /**
   * @author Cheah Lee Herng 
   */
  public Board getBoard(int rowIndex)
  {
    Assert.expect(rowIndex >= 0);

    List<Board> boards = _project.getPanel().getBoards();
    Assert.expect(rowIndex < boards.size());

    return (Board)boards.get(rowIndex);
    }

  /**
   * @author Cheah Lee Herng
   */
  private void setSelectedBoards(List<Board> selectedBoards)
  {
    Assert.expect(selectedBoards != null);
    _selectedBoards = selectedBoards;
  }

  /**
   * @author Cheah Lee Herng
   */
  int getIndexForBoard(Board board)
  {
    Assert.expect(board != null);
    return _boards.indexOf(board);
  }

  /**
   * @author Jack Hwee
   */
  int getIndexForOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {
    Assert.expect(opticalCameraRectangle != null);
    return _opticalRectangles.indexOf(opticalCameraRectangle);
  }
    
  /**
   * @author Ying-Huan.Chu
   */
  public void sortColumn(int column, boolean ascending)
  {
    switch(column)
    {
      case 0:  // sort on board name
      {
        _opticalCameraRectangleComparator.setAscending(ascending);
        _opticalCameraRectangleComparator.setOpticalCameraRectangleComparatorEnum(OpticalCameraRectangleComparatorEnum.BOARD_NAME);
        Collections.sort(_opticalRectangles, _opticalCameraRectangleComparator);
        break;
      }
      case 1:  // sort on optical rectangle name
      {
        _opticalCameraRectangleComparator.setAscending(ascending);
        _opticalCameraRectangleComparator.setOpticalCameraRectangleComparatorEnum(OpticalCameraRectangleComparatorEnum.NAME);
        Collections.sort(_opticalRectangles, _opticalCameraRectangleComparator);
        break;
      }
      case 2:  // sort on z-height
      {
        _opticalCameraRectangleComparator.setAscending(ascending);
        _opticalCameraRectangleComparator.setOpticalCameraRectangleComparatorEnum(OpticalCameraRectangleComparatorEnum.Z_HEIGHT);
        Collections.sort(_opticalRectangles, _opticalCameraRectangleComparator);
        break;
      }
      case 3:  // sort on usability status
      {
        _opticalCameraRectangleComparator.setAscending(ascending);
        _opticalCameraRectangleComparator.setOpticalCameraRectangleComparatorEnum(OpticalCameraRectangleComparatorEnum.USABILITY_STATUS);
        Collections.sort(_opticalRectangles, _opticalCameraRectangleComparator);
        break;
      }
      default:
        Assert.expect(false, "column does not exist in table model");
        break;  
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removeRow(int rowIndex)
  {
    if (_opticalRectangles.size() > rowIndex)
      _opticalRectangles.remove(rowIndex);
    fireTableDataChanged();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removeRow(OpticalCameraRectangle opticalCameraRectangle)
  {
    _opticalRectangles.remove(opticalCameraRectangle);
    fireTableDataChanged();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getRowIndexUsingOpticalRectangleName(String opticalRectangleName)
  {
    for (int i = 0; i < this.getRowCount(); ++i)
    {
      if (this.getValueAt(i, 1).toString().equals(opticalRectangleName))
      {
        return i;
      }
    }
    return -1;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public OpticalCameraRectangle getOpticalCameraRectangleUsingRowIndex(int rowIndex)
  {
    return _opticalRectangles.get(rowIndex);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public java.util.List<OpticalCameraRectangle> getSelectedOpticalCameraRectangles(int[] selectedRowIndex)
  {
    java.util.List<OpticalCameraRectangle> selectedOpticalCameraRectangles = new ArrayList<>();
    for (int i = 0; i < selectedRowIndex.length; ++i)
    {
      selectedOpticalCameraRectangles.add(_opticalRectangles.get(selectedRowIndex[i]));
    }
    return selectedOpticalCameraRectangles;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public java.util.List<OpticalCameraRectangle> getBoardOpticalCameraRectangles(Board board)
  {
    java.util.List<OpticalCameraRectangle> opticalCameraRectangles = new ArrayList<>();
    for (OpticalCameraRectangle opticalCameraRectangle : _opticalRectangles)
    {
      if (opticalCameraRectangle.getBoard().equals(board))
      {
        opticalCameraRectangles.add(opticalCameraRectangle);
      }
    }
    return opticalCameraRectangles;
  }
  
  /**
   * Set display unit of this table so that its corresponding data will be updated with the display unit user has selected.
   * @author Ying-Huan.Chu
   */
  public void setDisplayUnit(MathUtilEnum displayUnit)
  {
    if (_displayUnit.equals(displayUnit))
      return;
    else
    {
      _displayUnit = displayUnit;
      fireTableDataChanged();
    }
  }
}
