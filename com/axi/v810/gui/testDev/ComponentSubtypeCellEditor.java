package com.axi.v810.gui.testDev;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class implements a combo box cell editor tailored to changing the subtype
 * for a component or list of selected components
 * @author Andy Mechtenberg
 */
public class ComponentSubtypeCellEditor extends DefaultCellEditor
{
  private ModifySubtypesComponentTableModel _componentTableModel;
  // subtype change choices
  private final String _MIXED = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
  private String _newSubtype;
  private String _newAppendNameSubtype;
  private String _noLoad;
  private String _noTest;
  private String _renameSubtype;
  private String _restoreSubtype;

  /**
   * @author Andy Mechtenberg
   */
  public ComponentSubtypeCellEditor(JComboBox comboBox, ModifySubtypesComponentTableModel componentTableModel)
  {
    super(comboBox);

    Assert.expect(componentTableModel != null);
    _componentTableModel = componentTableModel;
  }

  /**
   * @author Andy Mechtenberg
   */
  void setEditorChoices(String newSubtype, String newShadedSubtype, String noTest, String noLoad, String renameSubtype, String restoreSubtype)
  {
    Assert.expect(newSubtype != null);
    Assert.expect(newShadedSubtype != null);
    Assert.expect(noTest != null);
    Assert.expect(noLoad != null);
    Assert.expect(renameSubtype != null);
    Assert.expect(restoreSubtype != null);

    _newSubtype = newSubtype;
    _newAppendNameSubtype = newShadedSubtype;
    _noLoad = noLoad;
    _noTest = noTest;
    _renameSubtype = renameSubtype;
    _restoreSubtype = restoreSubtype;
  }

  public java.awt.Component getTableCellEditorComponent(JTable table, Object value, boolean isSelectable, int row, int column)
  {
    // get the editor (should be a JComboBox as was specified in the constructor)
    // then modify the contents of the combo box to list only the valid choices for this row in the table
    int selectedRows[] = table.getSelectedRows();
    boolean allSameJointType = false;
    boolean allSameSubtype = false;
    boolean noTestsExist = false;
    boolean testsExist = false;
    boolean mixedSubtypes = false;
    JointTypeEnum singleJointType = null;
    String singleSubtype = null;
    BoardType boardType = null;

    // assign all booleans above by looking at each component selected.
    for (int i = 0; i < selectedRows.length; i++)
    {
      ComponentType componentType = _componentTableModel.getComponentTypeAt(selectedRows[i]);
      boardType = componentType.getSideBoardType().getBoardType();
      ComponentTypeSettings componentTypeSettings = componentType.getComponentTypeSettings();

      if ((componentTypeSettings.isInspected() == false) || (componentTypeSettings.isLoaded() == false))
        noTestsExist = true;
      else
        testsExist = true;

      if (componentTypeSettings.isInspected() && componentTypeSettings.isLoaded())
      {
        if(componentTypeSettings.usesOneSubtype() == false)
          mixedSubtypes = true;
        else
        {
          if(singleJointType == null)
          {
            singleJointType = componentType.getCompPackage().getJointTypeEnum();
            allSameJointType = true;
          }
          else
          {
            if(singleJointType.equals(componentType.getCompPackage().getJointTypeEnum()) == false)
              allSameJointType = false;
          }
          if(singleSubtype == null)
          {
            singleSubtype = componentTypeSettings.getSubtype().getShortName();
            allSameSubtype = true;
          }
          else
          {
            if(singleSubtype.equalsIgnoreCase(componentTypeSettings.getSubtype().getShortName()) == false)
              allSameSubtype = false;
          }
        }
      }

    }

    java.awt.Component editor = super.getTableCellEditorComponent(table, value, isSelectable, row, column);
    JComboBox comboBox = (JComboBox)editor;
    comboBox.removeAllItems();
    // do the single selection first - easier if we get this out of the way
    if (selectedRows.length == 1)
    {
      if (noTestsExist)
      {
        comboBox.addItem(_noTest);
        comboBox.addItem(_noLoad);
        comboBox.addItem(_restoreSubtype);
      }
      else if (mixedSubtypes)
      {
        comboBox.addItem(_newAppendNameSubtype);
        comboBox.addItem(_noTest);
        comboBox.addItem(_noLoad);
        comboBox.addItem(_MIXED);
        comboBox.setSelectedItem(_MIXED);
      }
      else
      {
        comboBox.addItem(_newAppendNameSubtype);
        comboBox.addItem(_newSubtype);
        comboBox.addItem(_noTest);
        comboBox.addItem(_noLoad);
        comboBox.addItem(_renameSubtype);
        addOtherSubtypesForFamily(comboBox, boardType, singleJointType, singleSubtype);
        comboBox.setSelectedItem(singleSubtype);
      }
    }
    else
    {
      // now we know that more than one component was selected, so build up with different rules
      if (testsExist == false) // this mean all no test or no load
      {
        comboBox.addItem(_noTest);
        comboBox.addItem(_noLoad);
        comboBox.addItem(_restoreSubtype);
      }
      else if (mixedSubtypes)
      {
        comboBox.addItem(_newAppendNameSubtype);
        comboBox.addItem(_noTest);
        comboBox.addItem(_noLoad);
        comboBox.addItem(_MIXED);  // add mixed in as the default to prevent no load or no test from happening accidentally.
        comboBox.setSelectedItem(_MIXED);
      }
      else if(allSameSubtype && allSameJointType && (noTestsExist == false))
      {
        comboBox.addItem(_newAppendNameSubtype);
        comboBox.addItem(_newSubtype);
        comboBox.addItem(_noTest);
        comboBox.addItem(_noLoad);
        comboBox.addItem(_renameSubtype);
        addOtherSubtypesForFamily(comboBox, boardType, singleJointType, singleSubtype);
        comboBox.setSelectedItem(singleSubtype);
        // add other subtypes for this family
      }
      else if(allSameJointType && (allSameSubtype == false) && (noTestsExist == false))
      {
        comboBox.addItem(_newAppendNameSubtype);
        comboBox.addItem(_newSubtype);
        comboBox.addItem(_noTest);
        comboBox.addItem(_noLoad);
        addOtherSubtypesForFamily(comboBox, boardType, singleJointType, "");
        // add other subtypes for this family
      }
      else if(noTestsExist && (testsExist == false))
      {
        comboBox.addItem(_newAppendNameSubtype);
        comboBox.addItem(_noTest);
        comboBox.addItem(_noLoad);
        comboBox.addItem(_restoreSubtype);
      }
      else if (noTestsExist)
      {
        comboBox.addItem(_newAppendNameSubtype);
        comboBox.addItem(_noTest);
        comboBox.addItem(_noLoad);
        comboBox.addItem(_restoreSubtype);
      }
      else
      {
        comboBox.addItem(_newAppendNameSubtype);
        comboBox.addItem(_noTest);
        comboBox.addItem(_noLoad);
      }
    }

    comboBox.setMaximumRowCount(30);
    return comboBox;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void addOtherSubtypesForFamily(JComboBox comboBox,
                                         BoardType boardType,
                                         JointTypeEnum singleJointType,
                                         String excludedSubtype)
  {
    Assert.expect(comboBox != null);
    Assert.expect(boardType != null);
    Assert.expect(singleJointType != null);
    Assert.expect(excludedSubtype != null);

    java.util.List<Subtype> subtypes = boardType.getSubtypesIncludingUnused(singleJointType);
    for (Subtype subtype : subtypes)
    {
//      if(excludedSubtype.equalsIgnoreCase(subtype.getName()) == false)
//      {
        String subtypeName = subtype.getShortName();
        comboBox.addItem(subtypeName);
//      }
    }

  }

}
