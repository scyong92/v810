package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.v810.business.panelSettings.*;
import com.axi.guiUtil.*;
import com.axi.util.*;

/**
 * @author siew-yeng.phang
 */
public class SerialNumberMappingTableModel extends DefaultSortTableModel
{
  private SerialNumberMappingDialog _parent;
  
  private static final String _BOARD_NAME = "Board Name";
  private static final String _BARCODE_READER_ID = "Barcode Reader Id";
  private static final String _SERIAL_NUMBER_SCAN_SEQUENCE = "Scan Sequence";
  
  private String[] _columnLabels = { _BOARD_NAME, 
                                     _BARCODE_READER_ID,                             
                                     _SERIAL_NUMBER_SCAN_SEQUENCE
                                   };
  
  private List<SerialNumberMappingData> _serialNumberMappingData;
  
  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;
  
  /**
   * Constructor.
   * @author Siew Yeng
   */
  public SerialNumberMappingTableModel(SerialNumberMappingDialog parent)
  {
    _parent = parent;
    _serialNumberMappingData = new ArrayList<SerialNumberMappingData>();
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public int getRowCount()
  {
    if(_serialNumberMappingData == null)
      return 0;
    else
      return _serialNumberMappingData.size();
  }

  /**
   * @author Phang Siew Yeng
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * @author Phang Siew Yeng
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    
    Object value = null;

    if (rowIndex >= getRowCount())
      return value;
    
    SerialNumberMappingData rowElement = _serialNumberMappingData.get(rowIndex);
 
    switch (columnIndex)
    {
      case SerialNumberMappingTable._BOARD_NAME_INDEX:
        value = rowElement.getBoardName();
        break;
      case SerialNumberMappingTable._BARCODE_READER_ID_INDEX:
        value = rowElement.getBarcodeReaderId();
        break;
      case SerialNumberMappingTable._SERIAL_NUMBER_SCAN_SEQUENCE_INDEX:
        value = rowElement.getScanSequence();
        break;
      default:
      {
        Assert.expect(false);
      }
    }
    return value;
  }
  
  /**
   * Overridden method.  Saves the data entered by the user in the table
   * to the project object.
   * @author Phang Siew Yeng
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    if (object == null)  // nothing to do if null -- this may happen if editing is cancelled.
      return;
    
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    SerialNumberMappingData rowElement = _serialNumberMappingData.get(rowIndex);

    switch (columnIndex)
    {
      case SerialNumberMappingTable._BOARD_NAME_INDEX:
        Assert.expect(object instanceof String);
        String boardName = (String)object;
        rowElement.setBoardName(boardName);
        break;
      
      case SerialNumberMappingTable._BARCODE_READER_ID_INDEX:
        Assert.expect(object instanceof Integer);
        int barcodeReaderId = (Integer)object;
        rowElement.setBarcodeReaderId(barcodeReaderId);
        break;
        
      case SerialNumberMappingTable._SERIAL_NUMBER_SCAN_SEQUENCE_INDEX:
        Assert.expect(object instanceof Integer);
        int sequence = (Integer)object;
        rowElement.setSerialNumberSequence(sequence);
        break;
    }
    _parent.updateSummary();
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public void sortColumn(int column, boolean ascending)
  {
    if (column == SerialNumberMappingTable._BOARD_NAME_INDEX)
      Collections.sort(_serialNumberMappingData, new SerialNumberMappingDataComparator(ascending, SerialNumberMappingDataComparatorEnum.BOARD_NAME));
    else if (column == SerialNumberMappingTable._BARCODE_READER_ID_INDEX)
      Collections.sort(_serialNumberMappingData, new SerialNumberMappingDataComparator(ascending, SerialNumberMappingDataComparatorEnum.BARCODE_READER_ID));
    else if (column == SerialNumberMappingTable._SERIAL_NUMBER_SCAN_SEQUENCE_INDEX)
      Collections.sort(_serialNumberMappingData, new SerialNumberMappingDataComparator(ascending, SerialNumberMappingDataComparatorEnum.SERIAL_NUMBER_SEQUENCE));

    _currentSortColumn = column;
    _currentSortAscending = ascending;
  }
  
  /**
   * @param column Index of the column
   * @return Column label for the column specified
   * @author Carli Connally
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[column];
  }
   
   /**
   * Overridden method.
   * @param rowIndex  Index of the row
   * @param columnIndex  Index of the column
   * @return boolean - whether the cell is editable
   * @author Carli Connally
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if (columnIndex == SerialNumberMappingTable._BOARD_NAME_INDEX)
      return false;

    return true;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  void populateMappingTable(Collection<SerialNumberMappingData> serialNumberMappingData)
  {
    clearData();
    for(SerialNumberMappingData data : serialNumberMappingData)
    {
      _serialNumberMappingData.add(new SerialNumberMappingData(data));
    }
    
    sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableDataChanged();
  }
  
  /**
   * @author Phang Siew Yeng
   */
  void clearData()
  {
    if (_serialNumberMappingData != null)
      _serialNumberMappingData.clear();
  }
  
  /**
   * @author Phang Siew Yeng
   */
  List<SerialNumberMappingData> getData()
  {
    return new ArrayList(_serialNumberMappingData);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public String getSelectedBoardName(int rowIndex)
  {
    Assert.expect(rowIndex >= 0);
    
    return (String)getValueAt(rowIndex, SerialNumberMappingTable._BOARD_NAME_INDEX);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public String getBoardName(int barcodeReaderId, int scanSequence)
  {
    for(SerialNumberMappingData data: _serialNumberMappingData)
    {
      if(barcodeReaderId == data.getBarcodeReaderId() &&
        scanSequence == data.getScanSequence())
        return data.getBoardName();
    }
    return null;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public int getNumberOfBarcodeReaders()
  {
    List<Integer> barcodeReaders = new ArrayList();
    for(SerialNumberMappingData data: _serialNumberMappingData)
    {
      if(barcodeReaders.contains(data.getBarcodeReaderId()) == false)
        barcodeReaders.add(data.getBarcodeReaderId());
    }
    return barcodeReaders.size();
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public List<Integer> getScanSequences(int barcodeReaderId)
  {
    List<Integer> scanSeq = new ArrayList<Integer>();
    for(SerialNumberMappingData data: _serialNumberMappingData)
    {
      if(barcodeReaderId == data.getBarcodeReaderId())
        scanSeq.add(data.getScanSequence());
    }
    return scanSeq;
  }
}
