/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.testDev;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.ProjectHistoryLog;
import com.axi.v810.gui.MessageDialog;
import com.axi.v810.gui.mainMenu.MainMenuGui;
import com.axi.v810.gui.undo.SubtypeSetNameCommand;
import com.axi.v810.util.*;

/**
 * @author chin-seong.kee
 */
public class ModifySubtypesPadTypeTable extends JSortTable
{
  public static final int _JOINT_INDEX = 0;
  public static final int _JOINT_TYPE_INDEX = 1;
  public static final int _SUBTYPE_INDEX = 2;
//  public static final int _SHADED_BY_COMPONENT_INDEX = 3;

  private ModifySubtypesPadTypeTableModel _padTypeTableModel;

  private BoardType _currentBoardType;

  private static final int _COMPONENT_UNDO_INDEX = 0;

  private final String _NEW_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NEW_KEY");
  private final String _NOTEST_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOTEST_KEY");
  private final String _RENAME_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_RENAME_KEY");
  private final String _RESTORE_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_RESTORE_KEY");
   //Project's History log -hsia-fen.tan
  private static transient ProjectHistoryLog _fileWriter;

  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  /**
   * @param model
   */
  public ModifySubtypesPadTypeTable(ModifySubtypesPadTypeTableModel model)
  {
    Assert.expect(model != null);
    _padTypeTableModel = model;
    _fileWriter = ProjectHistoryLog.getInstance();
    super.setModel(model);
  }

  /**
   * @return
   */
  public void setModel(ModifySubtypesPadTypeTableModel padTypeTableModel)
  {
    Assert.expect(padTypeTableModel != null);
    _padTypeTableModel = padTypeTableModel;
    super.setModel(padTypeTableModel);
  }

  /**
   * @return
   */
  public ModifySubtypesPadTypeTableModel getTableModel()
  {
    Assert.expect(_padTypeTableModel != null);
    return _padTypeTableModel;
  }

  /**
   *
   */
  public void setupCellEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    JComboBox padSubtypeComboBox = new JComboBox();
    PadSubtypeCellEditor padSubtypeCellEditor = new PadSubtypeCellEditor(padSubtypeComboBox, _padTypeTableModel);
    padSubtypeCellEditor.setEditorChoices(_NEW_SUBTYPE, _NOTEST_SUBTYPE, _RENAME_SUBTYPE, _RESTORE_SUBTYPE);
    columnModel.getColumn(_SUBTYPE_INDEX).setCellEditor(padSubtypeCellEditor);

    columnModel.getColumn(_SUBTYPE_INDEX).setCellRenderer(new ComponentLocationTableCellRenderer());
  }

  /**
   * @param value
   * @param row
   * @param column
   */
  public void setValueAt(Object value, int row, int column)
  {
    padTypeDetailsSetValues(value, row, column);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void padTypeDetailsSetValues(Object value, int row, int column)
  {
    int rows[] = getSelectedRows();
    int col = convertColumnIndexToModel(column);

    // special case of setting the subtype to  "New..."  We need to bring up a dialog
    // to ask them for the name of the new subtype
    if (col == 2) // subtype column -- the only editable column (joint type not editable for padtype)
    {
      if (value instanceof String)
      {
        String valStr = (String)value;
        if (valStr.equalsIgnoreCase(_NEW_SUBTYPE))
        {
          String prompt = StringLocalizer.keyToString("MMGUI_SUBTYPE_NEW_PROMPT_KEY");
          String title = StringLocalizer.keyToString("MMGUI_SUBTYPE_NEW_TITLE_KEY");
          String retVal = JOptionPane.showInputDialog(MainMenuGui.getInstance(),
                                                      prompt,
                                                      title,
                                                      JOptionPane.QUESTION_MESSAGE);

          if ((retVal == null) || (retVal.length() == 0)) // the use hit cancel or entered an empty string -- change nothing
            return;
          com.axi.v810.business.panelDesc.Panel panel = getCurrentBoardType().getPanel();
          if (panel.isSubtypeNameValid(retVal) == false)
          {
            // explain why it's invalid
            LocalizedString message;
            String illegalSubtypeCharacters = panel.getSubtypeNameIllegalChars();
            if (illegalSubtypeCharacters.equals(" "))
            {
              message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_ONLY_SPACES_KEY",
                                            new Object[]
                                            {retVal});
            }
            else
            {
              message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_OTHER_CHARS_KEY",
                                            new Object[]
                                            {retVal, illegalSubtypeCharacters});
            }
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                          StringLocalizer.keyToString(message),
                                          StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                          JOptionPane.ERROR_MESSAGE);

            return;
          }
          value = retVal;
          _commandManager.trackState(getCurrentUndoState());
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PAD_TYPE_SET_SUBTYPE_KEY"));
        }
        else if (valStr.equalsIgnoreCase(_NOTEST_SUBTYPE))
        {
          value = valStr;
          // no Test -- set manually to each row
          _commandManager.trackState(getCurrentUndoState());
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("MMGUI_NO_TEST_GROUP_UNDO_KEY"));
        }
        else if (valStr.startsWith(_RESTORE_SUBTYPE))  // set to test
        {
          // Set To Test
          value = valStr;
          _commandManager.trackState(getCurrentUndoState());
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PAD_TYPE_SET_TESTED_KEY"));
//          for(int i = 0; i < rows.length; i++)
//          {
//            PadType padType = _padTypeTableModel.getPadTypeAt(rows[i]);
//            try
//            {
//              _commandManager.execute(new PadTypeSetTestedCommand(padType, true));
//            }
//            catch (XrayTesterException ex)
//            {
//              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
//                                            ex.getLocalizedMessage(),
//                                            new LocalizedString("MM_GUI_TDT_NAME_KEY", null),
//                                            true);
//              _padTypeTableModel.fireTableCellUpdated(rows[i], column);
//            }
//          }
//          _commandManager.endCommandBlock();
//          return;
        }
        else if (valStr.equalsIgnoreCase(_RENAME_SUBTYPE))
        {
          PadType padType = _padTypeTableModel.getPadTypeAt(getSelectedRow());
          String currentSubtype = padType.getSubtype().getShortName();
          String prompt = StringLocalizer.keyToString("MMGUI_SUBTYPE_RENAME_PROMPT_KEY");
          String title = StringLocalizer.keyToString("MMGUI_SUBTYPE_RENAME_TITLE_KEY");
          Object retVal = JOptionPane.showInputDialog(MainMenuGui.getInstance(),
                                                      prompt,
                                                      title,
                                                      JOptionPane.QUESTION_MESSAGE,
                                                      null,
                                                      null,
                                                      currentSubtype);
          String newName = (String)retVal;
          if ((retVal == null) || (newName.length() == 0)) // the use hit cancel or entered an empty string -- change nothing
            return;
          com.axi.v810.business.panelDesc.Panel panel = getCurrentBoardType().getPanel();
          if (panel.isSubtypeNameValid(newName) == false)
          {
            // explain why it's invalid
            LocalizedString message;
            String illegalSubtypeCharacters = panel.getSubtypeNameIllegalChars();
            if (illegalSubtypeCharacters.equals(" "))
            {
              message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_ONLY_SPACES_KEY",
                                            new Object[]
                                            {retVal});
            }
            else
            {
              message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_OTHER_CHARS_KEY",
                                            new Object[]
                                            {retVal, illegalSubtypeCharacters});
            }
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                          StringLocalizer.keyToString(message),
                                          StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                          JOptionPane.ERROR_MESSAGE);

            return;  // error case - bail and do nothing
          }
          try  // ok, we have a good rename string, go ahead and set it
          {
            _commandManager.trackState(getCurrentUndoState());
            _commandManager.execute(new SubtypeSetNameCommand(padType.getPadTypeSettings().getSubtype(), newName));
            _fileWriter.appendRenameComponentDetail(padType.getComponentType(),padType.getPackagePin().getJointTypeEnum(),currentSubtype ,newName);
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
            _padTypeTableModel.fireTableCellUpdated(row, column);
          }
          return;
        }
        else  // dropping through -- this must be the re-assign subtype case
        {
          _commandManager.trackState(getCurrentUndoState());
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PAD_TYPE_SET_SUBTYPE_KEY"));
        }
      }
    }

    for (int i = 0; i < rows.length; i++)
    {
        _padTypeTableModel.setValueAt(value, rows[i], col);
        _padTypeTableModel.fireTableRowsUpdated(rows[i], row + 1); // this to update, last row of the testable status
    }
    _commandManager.endCommandBlock();
  }

  /**
   * @return the _currentBoardType
   */
  public BoardType getCurrentBoardType()
  {
    return _currentBoardType;
  }

  /**
   * @param currentBoardType the _currentBoardType to set
   */
  public void setCurrentBoardType(BoardType currentBoardType)
  {
    this._currentBoardType = currentBoardType;
  }

  /**
   * @author Erica Wheatcroft
   */
  private UndoState getCurrentUndoState()
  {
    UndoState undoState = new UndoState();
    undoState.setModifySubtypesViewSelectIndex(_COMPONENT_UNDO_INDEX);
    return undoState;
  }
}
