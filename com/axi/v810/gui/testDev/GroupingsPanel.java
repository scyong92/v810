package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.LicenseManager;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelDesc.Component;
import com.axi.v810.business.panelDesc.Panel;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * This is the Groupings panel for the Test Development Tool
 * @author Andy Mechtenberg
 */
public class GroupingsPanel extends AbstractTaskPanel implements Observer
{
  private MainMenuGui _mainUI = null;
  private TestDev _testDev = null;
  private ImageManagerPanel _imagePanel;
  private TestDevPersistance _persistSettings;
  private ProjectPersistance _projectPersistSettings = null;
  private BoardType _currentBoardType;
  private Project _project = null;
  private boolean _updateData = false;
  private boolean _populateProjectData = true;
  private boolean _shouldInteractWithGraphics = false;
  private boolean _viewPackage = false;  // default setting of view by package or component
  private boolean _showUnTestableReasons = false; // default setting //XCR1374, KhangWah, 2011-09-09

  // every time a Renderer is selected the Observable will update any Observers to
  // tell them it was selected
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private SelectedRendererObservable _panelGraphicsSelectedRendererObservable;
  private SelectedRendererObservable _packageGraphicsSelectedRendererObservable;
  private SelectedRendererObservable _verificationImageGraphicsSelectedRendererObservable;

  // listener for row table selection
  private ListSelectionListener _componentPackageTableListSelectionListener;
  private ListSelectionListener _detailsTableListSelectionListener;
  private ListSelectionListener _unTestableReasonsTableListSelectionListener; //XCR1374, KhangWah, 2011-09-19

  // remembered selections for restoration
  private int[] _rememberedSelectedComponents = new int[]{};
  private int[] _rememberedSelectedPackages = new int[]{};
  private ComponentType _anchorComponentType;

  // command manager for doing undos
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  // Display strings:
  private final String _PACKAGE_DETAILS_STRING = StringLocalizer.keyToString("MMGUI_GROUPINGS_PACKAGE_DETAILS_KEY");
  private final String _COMPONENT_DETAILS_STRING = StringLocalizer.keyToString("MMGUI_GROUPINGS_COMPONENT_DETAILS_KEY");
  //XCR1374, KhangWah, 2011-09-09
  private final String _UNTESTABLE_REASONS_STRING = StringLocalizer.keyToString("MMGUI_GROUPINGS_UNTESTABLE_REASONS_KEY");

  private JPanel _northPanel = new JPanel(new BorderLayout());
  private JPanel _subNorthPanel = new JPanel(new BorderLayout()); //XCR1374, KhangWah, 2011-09-09
  private JPanel _contentPanel = new JPanel();
  private JLabel _boardLabel = new JLabel();
  private PairLayout _viewBoardPairLayout = new PairLayout(10, 10, false);
  private JLabel _viewLabel = new JLabel();
  private JComboBox _viewSelectComboBox = new JComboBox();
  private JCheckBox _showUnTestableReasonsTableCheckBox = new JCheckBox(); //XCR1374, KhangWah, 2011-09-09
  private JPanel _packageComponentPanel = new JPanel();
  private JPanel _detailsPanel = new JPanel();
  private JPanel _reasonsPanel = new JPanel(); //XCR1374, KhangWah, 2011-09-09
  private BorderLayout _packageComponentPanelBorderLayout = new BorderLayout();
  private JScrollPane _packageComponentScrollPane = new JScrollPane();
  private ModifySubtypesComponentTable _componentTable;
  private ModifyPackageTable _packageTable;
  private ModifySubtypesComponentTableModel _componentTableModel;
  private ModifySubtypesPackageTableModel _packageTableModel;
  private BorderLayout _detailsPanelBorderLayout = new BorderLayout();
  private JLabel _detailsLabel = new JLabel();
  private JScrollPane _detailsScrollPane = new JScrollPane();
  private ModifySubtypesPadTypeTable _padTypeTable;
  private ModifySubtypesPackagePinTable _packagePinTable;
  private ModifySubtypesUnTestableReasonsTable _unTestableReasonsTable; //XCR1374, KhangWah, 2011-09-09
  private ModifySubtypesPadTypeTableModel _padTypeTableModel;
  private ModifySubtypesPackagePinTableModel _packagePinTableModel;
  private ModifySubtypesUnTestableReasonsTableModel _unTestableReasonsTableModel; //XCR1374, KhangWah, 2011-09-09
  private BorderLayout _reasonsPanelBorderLayout = new BorderLayout(); //XCR1374, KhangWah, 2011-09-09
  private JLabel _reasonsLabel = new JLabel(); //XCR1374, KhangWah, 2011-09-09
  private JScrollPane _reasonsScrollPane = new JScrollPane(); //XCR1374, KhangWah, 2011-09-09
  private BorderLayout _contentPanelBorderLayout = new BorderLayout();
  private Border _mainBorder;
  private JSplitPane _centerSplitPane = new JSplitPane();
  private JSplitPane _subCenterSplitPane = new JSplitPane(); //XCR1374, KhangWah, 2011-09-09
  private JMenu _artifactCompensationMenu = new JMenu();
  private JMenuItem _resetArtifactCompesationMenuItem = new JMenuItem();
  private JMenuItem _enableArtifactCompesationMenuItem = new JMenuItem();

  private JMenu _globalSurfaceModelMenu = new JMenu();
  private JMenuItem _setAllUseAutoFocusMenuItem = new JMenuItem();
  private JMenuItem _setAllUseGlobalSurfaceModelMenuItem = new JMenuItem();
  private JMenuItem _setAllUseAutoSelectMenuItem = new JMenuItem();

  //added by Chin Seong, Kee
  private JButton _selectSubtypeToModifyButton = new JButton();
  private SubtypeAdvanceSettingDialog _dlg = null;
  
  //Kee Chin Seong - Seperate out the Top and Button Subtype Easily
  private JButton _separateSubtypesButton = new JButton();
  
  //Kee Chin Seong - Apply Variation
  private JButton _applyVariatonButton = new JButton();
  
  private static boolean _isAbort = false;
  
  private final BusyCancelDialog _busyCancelDialog = new BusyCancelDialog(_mainUI,
                                                                          StringLocalizer.keyToString("MMGUI_GROUPINGS_BUSYCANCELDIALOG_UPDATE_CAD_MESSAGE_KEY"),
                                                                          StringLocalizer.keyToString("MMGUI_GROUPINGS_BUSYCANCELDIALOG_UPDATE_CAD_TITLE_KEY"),
                                                                          true);
  public GroupingsPanel()
  {
    jbInit();
  }

  /**
   * @author Andy Mechtenberg
   */
  public GroupingsPanel(TestDev testDev, ImageManagerPanel imagePanel)
  {
    super(testDev);
    Assert.expect(testDev != null);
    Assert.expect(imagePanel != null);
    _testDev = testDev;
    _imagePanel = imagePanel;
    _mainUI = _testDev.getMainUI();

    jbInit();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void restorePersistSettings()
  {
    _persistSettings = _testDev.getPersistance();
    int dividerLocation = _persistSettings.getModifySubtypesDividerLocation();
    if (dividerLocation > 0)
    {
      _centerSplitPane.setDividerLocation(dividerLocation);
    }
    //XCR1374, KhangWah, 2011-09-19
    _subCenterSplitPane.setDividerLocation(_persistSettings.getUnTestableReasonsDividerLocation());
      
    _projectPersistSettings = _persistSettings.getProjectPersistance();
    _projectPersistSettings = MainMenuGui.getInstance().getTestDev().getPersistance().getProjectPersistance();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void savePersistSettings()
  {
    int dividerLocation = _centerSplitPane.getDividerLocation();
    if (_persistSettings != null)
    {
      _persistSettings.setModifySubtypesDividerLocation(dividerLocation);
      
      //XCR1374, KhangWah, 2011-09-19
      if(_showUnTestableReasons == true)
          _persistSettings.setUnTestableReasonsDividerLocation(_subCenterSplitPane.getDividerLocation()); 
    }
    if(_projectPersistSettings != null)
    {
      _projectPersistSettings.setShowUnTestableReasonsInformation(_showUnTestableReasonsTableCheckBox.isSelected()); //XCR1374, KhangWah, 2011-09-09
    }
  }

  /**
   * The Groupings panel is a listener to datastore (for things like adding/removing components) and
   * it also listens to the DrawCad selection observerable so it can update it's table selections when the
   * user clicks or otherwise selects stuff with the graphics.
   * @author Andy Mechtenberg
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof SelectedRendererObservable)
          handleSelectionEvent(object);
        else if (observable instanceof ProjectObservable)
        {
          if (object instanceof ProjectChangeEvent)
          {
            final ProjectChangeEvent event = (ProjectChangeEvent)object;
            ProjectChangeEventEnum eventEnum = ((ProjectChangeEvent)object).getProjectChangeEventEnum();
            
              // XCR-2097 [Ying-Huan.Chu] - Commented these so that the tables are updated immediately whenever user made changes.
//            if ((eventEnum instanceof LandPatternEventEnum) ||
//            (eventEnum instanceof LandPatternPadEventEnum) ||
//            (eventEnum instanceof ThroughHoleLandPatternPadEventEnum) ||
//            (eventEnum instanceof ComponentEventEnum) ||
//            (eventEnum instanceof ComponentTypeEventEnum) ||
//            (eventEnum instanceof FiducialEventEnum) ||
//            (eventEnum instanceof FiducialTypeEventEnum))
//            {
              handleProjectEvent(object);
//            }
          }
        }
        else if (observable instanceof GuiObservable)
          handleGuiEvent((GuiEvent)object);
        else if (observable instanceof com.axi.v810.gui.undo.CommandManager)
          handleUndoState(object);
        else
          Assert.expect(false);
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleSelectionEvent(Object object)
  {
    // turn off auto-updating of the graphics which happens when table selections are changed.
    // Since this action happened because of a graphical selection, it should override
    _shouldInteractWithGraphics = false;
    // ok.  We got notice that something was selected.
    // We receive from the update, a collection of selected objects.  We'll only pay attention to
    // the ones this screen is interested in
    Collection selections = (Collection)object;
    Iterator it = selections.iterator();
    if (_viewPackage)
    {
      boolean isPackageSet = false;
      if (_packagePinTable.isEditing())
        _packagePinTable.getCellEditor().cancelCellEditing();

      // clear the selection in the table AND the table model -- otherwise the isCellEditable gets confused.
      _packagePinTable.clearSelection();
      _packagePinTableModel.setSelectedPackagePins(new ArrayList<PackagePin>());
      // viewing package data -- pay attention to PackagePinRenderers
      // assumption -- all of these renderers should be PackagePinRenderers
      while (it.hasNext())
      {
        com.axi.guiUtil.Renderer renderer = (com.axi.guiUtil.Renderer)it.next();

        // look only for PackagePinRenderers
        // find the CompPackage these guys are using, and select the right compPackage in the ComponentPackageTable first
        // then select the pins, one per renderer.  If you don't do it in this order, things get messed up -- the selection in
        // the ComponentPackageTable will have the side effect of DE-selecting everything in the details table
        if (renderer instanceof PackagePinRenderer)
        {
          if (isPackageSet == false) // we only want to set this ONCE, and before setting any of the pins
          {
            // update the componentPackageTable to select the package for this packagePin
            CompPackage compPackage = ((PackagePinRenderer)renderer).getPackagePin().getCompPackage();
            int row = _packageTableModel.getRowForCompPackage(compPackage);
            Assert.expect(row >= 0, "Package does not exist: " + compPackage);  // the selected a package pin, so there must be a package
            _packageTable.setRowSelectionInterval(row, row);

            java.util.List<CompPackage> selectedPackages = new ArrayList<CompPackage>();
            selectedPackages.add(compPackage);
            _packageTableModel.setSelectedPackages(selectedPackages);
            isPackageSet = true;
          }
          PackagePinRenderer pinRenderer = (PackagePinRenderer)renderer;
          PackagePin packagePin = pinRenderer.getPackagePin();
          int rowPosition = _packagePinTableModel.getRowForPackagePin(packagePin);
          _packagePinTable.addRowSelectionInterval(rowPosition, rowPosition);
        }
        else if(renderer instanceof PackagePinNameRenderer)
        {
          // do nothing
        }
        else // if the renderer was not a packagePinRenderer, this is bad -- assert
          Assert.expect(false, "Renderer was not a package pin renderer, it was : " + renderer);
      }

      // after everything is selected, scroll to make the first one visible.
      SwingUtils.scrollTableToShowSelection(_padTypeTable);
      //XCR1374, KhangWah, 2011-09-15
      SwingUtils.scrollTableToShowSelection(_unTestableReasonsTable);
    }
    else
    {
      java.util.List < com.axi.v810.business.panelDesc.Component > components = new ArrayList<com.axi.v810.business.panelDesc.Component>();
      java.util.List<Pad> pads = new ArrayList<Pad>();
      ListSelectionModel rowSelectionModel = _componentTable.getSelectionModel();
      rowSelectionModel.setValueIsAdjusting(true);
      while (it.hasNext())
      {
        com.axi.guiUtil.Renderer renderer = (com.axi.guiUtil.Renderer)it.next();
        // viewing component mode -- pay attention to ComponentRenderers and PadRenderers
        // get the component(s) selected (if any) and highlight the row in the table cooresponding to that component
        // if a pad was also selected, highlight the pad in the details table
        if (renderer instanceof ComponentRenderer)
        {
          ComponentRenderer compRenderer = (ComponentRenderer)renderer;
          components.add(compRenderer.getComponent());
        }

        if (renderer instanceof PadRenderer)
        {
          PadRenderer padRenderer = (PadRenderer)renderer;
          pads.add(padRenderer.getPad());
          components.add(padRenderer.getPad().getComponent());
        }
        if (renderer instanceof PadRegionOfInterestRenderer)
        {
          PadRegionOfInterestRenderer padRenderer = (PadRegionOfInterestRenderer)renderer;
          pads.add(padRenderer.getPad());
          components.add(padRenderer.getPad().getComponent());
        }
      }

      // if the renderer selected was not a component or a pad, we really don't care
      // and can exit this method at this point.
      if (components.isEmpty() && pads.isEmpty())
      {
        _shouldInteractWithGraphics = true;
        return;
      }
      if (_padTypeTable.isEditing())
        _padTypeTable.getCellEditor().cancelCellEditing();
      _padTypeTable.clearSelection();
      _padTypeTableModel.setSelectedPadTypes(new ArrayList<PadType>());
      
      //XCR1374, KhangWah, 2011-09-15
      _unTestableReasonsTable.clearSelection();
      _unTestableReasonsTableModel.setSelectedPadTypes(new ArrayList<PadType>());
      
      // we're going to select new components if a component renderer came through, otherwise do not change the
      // selection
      if (components.isEmpty() == false)
      {
        if (_componentTable.isEditing())
          _componentTable.getCellEditor().cancelCellEditing();
        _componentTable.clearSelection();
      }
      // go through all the component renderers and select them in the table
      Set<BoardType> boardTypesNeeded = new HashSet<BoardType>();
      for (com.axi.v810.business.panelDesc.Component component : components)
      {
        BoardType boardTypeForComponent = component.getBoard().getBoardType();
        boardTypesNeeded.add(boardTypeForComponent);
      }
      if (boardTypesNeeded.size() == 1)
      {
        BoardType boardTypeNeeded = (BoardType)(boardTypesNeeded.toArray())[0];
        if (boardTypeNeeded != _currentBoardType)
        {
          // need to switch to new boardType
          _currentBoardType = boardTypeNeeded;
          boardTypeChanged();
          if (_componentTable.isEditing())
            _componentTable.getCellEditor().cancelCellEditing();
          _componentTable.clearSelection();
          _testDev.setCurrentBoardType(_currentBoardType);
        }
      }
      for (com.axi.v810.business.panelDesc.Component component : components)
      {
        int rowPosition = _componentTableModel.getRowForComponentType(component.getComponentType());
        if (rowPosition != -1) // -1 measn it does not exist in the table.  This can happen with more than one board type
          _componentTable.addRowSelectionInterval(rowPosition, rowPosition);
      }

      // only scroll to first one selected
      rowSelectionModel.setValueIsAdjusting(false);
      SwingUtils.scrollTableToShowSelection(_componentTable);

      Rectangle rect;
      // now all we have left to deal with is PadRenderers -- only select pad row for the currently selected component
      if (_componentTableModel.isCommonSelectedComponentType())
      {
        Collection<ComponentType> selectedComponents = _componentTableModel.getSelectedComponentTypes();
        for (Pad pad : pads)
        {
          // now what may happen is the user could group select a bunch of pads, without selecting the component along with it.
          // so, if we ONLY got a bunch of PadRenderers (i.e. no component renderers found), then go ahead and change the component table
          // the component for the pads selected.  In whichever order we get to them.  If pads for more than one component were selected,
          // we'll just do our best and pick one component to use.
          if (components.isEmpty())
          {
            ComponentType componentType = pad.getComponent().getComponentType();
            int rowPosition = _componentTableModel.getRowForComponentType(componentType);

            _componentTable.setRowSelectionInterval(rowPosition, rowPosition);

            rect = _componentTable.getCellRect(rowPosition, 0, true);
            _componentTable.scrollRectToVisible(rect);

            rowPosition = _padTypeTableModel.getRowForComponentPad(pad.getPadType());
            // -1 means one selected a pad that isn't in the component list -- most likely a through hole pad for a component on the other side
            if (rowPosition != -1)
            {
              _padTypeTable.addRowSelectionInterval(rowPosition, rowPosition);
              rect = _padTypeTable.getCellRect(rowPosition, 0, true);
              _padTypeTable.scrollRectToVisible(rect);
            }
            
            //XCR1374, KhangWah, 2011-09-15
            rowPosition = _unTestableReasonsTableModel.getRowForComponentPad(pad.getPadType());
            // -1 means one selected a pad that isn't in the component list -- most likely a through hole pad for a component on the other side
            if (rowPosition != -1)
            {
                _unTestableReasonsTable.addRowSelectionInterval(rowPosition, rowPosition);
                rect = _unTestableReasonsTable.getCellRect(rowPosition, 0, true);
                _unTestableReasonsTable.scrollRectToVisible(rect);
            }
          }
          else if (selectedComponents.contains(pad.getComponent().getComponentType()))
          {
            // highlight the pad in the table, as it should be populated
            int rowPosition = _padTypeTableModel.getRowForComponentPad(pad.getPadType());
            _padTypeTable.addRowSelectionInterval(rowPosition, rowPosition);
            rect = _padTypeTable.getCellRect(rowPosition, 0, true);
            _padTypeTable.scrollRectToVisible(rect);
            
            //XCR1374, KhangWah, 2011-09-15
            if (_showUnTestableReasons == true)
            {        
                rowPosition = _unTestableReasonsTableModel.getRowForComponentPad(pad.getPadType());
                _unTestableReasonsTable.addRowSelectionInterval(rowPosition, rowPosition);
                rect = _unTestableReasonsTable.getCellRect(rowPosition, 0, true);
                _unTestableReasonsTable.scrollRectToVisible(rect);
            }
          }
        }
      }
    }
    _shouldInteractWithGraphics = true;

  }
  /**
   * Handles information regarding a change in datastore.  Typically generated by someone doing a "set" when this
   * panel is not visible, but it should update itself anyway so when it becomes visible, data will be up to date.
   * @author Andy Mechtenberg
   */
  private void handleProjectEvent(Object object)
  {
    //Kee Chin Seong - Update itself on same time when switching tab
    //                 so that when switch back current tab it wont feel "lagging"
    if (_active == false)    
    {
      // XCR-3247 - Jen Ping
      // if no project is loaded, no need to update the tables.
      if (Project.isCurrentProjectLoaded() == false)
        return;
      
      if(_currentBoardType != null && _testDev.isAdjustCadCurrentActive() == false)
        updateProjectData();
      else
        _updateData = true;
    }
    else
    {
      // ignore the update if our project object hasn't been initialized
      if (object instanceof ProjectChangeEvent)
      {
        boolean updateSelf = false;
        ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)object;
        ProjectChangeEventEnum eventEnum = projectChangeEvent.getProjectChangeEventEnum();
//            System.out.println("Groupings: Datastore Event " + eventEnum);
        if (eventEnum instanceof SideBoardTypeEventEnum)
        {
          SideBoardTypeEventEnum sideBoardTypeEvent = (SideBoardTypeEventEnum)eventEnum;
          if (sideBoardTypeEvent.equals(sideBoardTypeEvent.ADD_OR_REMOVE_COMPONENT_TYPE))
            updateSelf = true;
        }
        else if (eventEnum instanceof ProjectEventEnum)
        {
          if(eventEnum.equals(ProjectEventEnum.DISPLAY_UNITS))
          {
            updateSelf = true;
          }
          else if (eventEnum.equals(ProjectEventEnum.LIBRARY_IMPORT_COMPLETE) || eventEnum.equals(ProjectEventEnum.IMPORT_LAND_PATTERN_COMPLETE))
          {
            updateProjectData();
          }
        }
        else if ((eventEnum instanceof CompPackageEventEnum) ||
                 (eventEnum instanceof PackagePinEventEnum) ||
                 (eventEnum instanceof ComponentEventEnum) ||
                 (eventEnum instanceof ComponentTypeEventEnum) ||
                 (eventEnum instanceof ComponentTypeSettingsEventEnum) ||
                 (eventEnum instanceof PadTypeSettingsEventEnum) ||
                 (eventEnum instanceof PadEventEnum) ||
                 (eventEnum instanceof PadTypeEventEnum) ||
                 (eventEnum instanceof PanelSettingsEventEnum) ||
                 (eventEnum instanceof ThroughHoleLandPatternPadEventEnum) ||
                 (eventEnum instanceof SubtypeEventEnum) ||
                 (eventEnum instanceof SubtypeAdvanceSettingsEventEnum))
        {
          // anything here should cause an update
          updateSelf = true;
        }
        if (updateSelf)
        {
          if (_componentTable.isEditing())
            _componentTable.getCellEditor().cancelCellEditing();
          if (_padTypeTable.isEditing())
            _padTypeTable.getCellEditor().cancelCellEditing();
          if (_packageTable.isEditing())
            _packageTable.getCellEditor().cancelCellEditing();
          if (_packagePinTable.isEditing())
            _packagePinTable.getCellEditor().cancelCellEditing();
          
          //Siew Yeng - XCR-3240
          _commandManager.waitUntilCommandEnd();
          
          if (eventEnum instanceof ComponentTypeSettingsEventEnum) // handles no load, no test, subtype changes
          {
            // Kok Chun, Tan - XCR-3271 - handle when event pass in the list
            java.util.List<ComponentTypeSettings> componentSettingsList = null;
            if (projectChangeEvent.getSource() instanceof ArrayList)
            {
              componentSettingsList = (java.util.List<ComponentTypeSettings>) projectChangeEvent.getSource();
            }
            else
            {
              componentSettingsList = new ArrayList<ComponentTypeSettings>();
              componentSettingsList.add((ComponentTypeSettings) projectChangeEvent.getSource());
            }
            for (ComponentTypeSettings compTypeSettings : componentSettingsList)
            {
              ComponentType compType = compTypeSettings.getComponentType();
              _componentTableModel.updateSpecificComponent(compType);
              _padTypeTableModel.fireTableRowsUpdated(0, compType.getPadTypes().size() - 1);
            
              //XCR1374, KhangWah, 2011-09-15
              _unTestableReasonsTableModel.fireTableRowsUpdated(0, compType.getPadTypes().size() - 1);
            }
          }
          else if (eventEnum instanceof ComponentTypeEventEnum) // handles joint type changes -- speed improvement
          {
            ComponentType compType = (ComponentType)projectChangeEvent.getSource();
            if (_viewPackage)
            {
                //Kee Chin Seong - Remember alawys remove the lsitener or else it will looping forever on the tablemodel
               _packageTable.getSelectionModel().removeListSelectionListener(_componentPackageTableListSelectionListener); // add the listener for a details table row selection
              // update the package for this component
              CompPackage affectedPackage = compType.getCompPackage();
              _packageTableModel.updateAllPackages(_currentBoardType);
              int selectedPackageRow = _packageTableModel.getRowForCompPackage(affectedPackage);
              
              _packageTable.getSelectionModel().addListSelectionListener(_componentPackageTableListSelectionListener); // add the listener for a details table row selection
               
              //Re-enable the listener so that the table will be updated
              if (selectedPackageRow >= 0) // still exists
              {
                _packageTable.setRowSelectionInterval(selectedPackageRow, selectedPackageRow);
                _packageTableModel.setSelectedPackage(affectedPackage);
                _packageTableModel.updateSpecificPackage(affectedPackage);
                _packagePinTableModel.fireTableRowsUpdated(0, affectedPackage.getPackagePinsUnsorted().size() - 1);
              }
            }
            else
            {
              _componentTableModel.updateSpecificComponent(compType);
              _padTypeTableModel.fireTableRowsUpdated(0, compType.getPadTypes().size() - 1);
              
              //XCR1374, KhangWah, 2011-09-15
              _unTestableReasonsTableModel.fireTableRowsUpdated(0, compType.getPadTypes().size() - 1);
            }
          }
          else if (eventEnum instanceof CompPackageEventEnum)
          {
            if(eventEnum.equals(CompPackageEventEnum.EXPORT_COMPLETE_ENUM))
              return;

            if (_packageTableModel.hasSelectedPackage())
            {
              CompPackage selectedPackage = _packageTableModel.getSelectedPackage();
              _packageTableModel.updateAllPackages(_currentBoardType);
              int selectedPackageRow = _packageTableModel.getRowForCompPackage(selectedPackage);
              if (selectedPackageRow >= 0) // still exists
              {
                ListSelectionModel rowSM = _packageTable.getSelectionModel();
                // XCR-2253 - Ying-Huan.Chu 
                // temporary remove ListSelectionListener.
                rowSM.removeListSelectionListener(_componentPackageTableListSelectionListener);
                _packageTable.setRowSelectionInterval(selectedPackageRow, selectedPackageRow);
                _packageTableModel.setSelectedPackage(selectedPackage);
                // add ListSelectionListener again after PackageTable has done selecting the row.
                rowSM.addListSelectionListener(_componentPackageTableListSelectionListener); // add the listener for a details table row selection
              }
            }
            CompPackage compPackage = (CompPackage)projectChangeEvent.getSource();
            _packageTableModel.updateSpecificPackage(compPackage);
            _packagePinTableModel.fireTableRowsUpdated(0, compPackage.getPackagePinsUnsorted().size() - 1);
          }
          else if (eventEnum instanceof PackagePinEventEnum)
          {
            PackagePin packagePin = (PackagePin)projectChangeEvent.getSource();
            CompPackage compPackage = packagePin.getCompPackage();
            _packageTableModel.updateSpecificPackage(compPackage);
            _packagePinTableModel.updateSpecificPackagePin(packagePin);
          }
          else if (eventEnum instanceof PadTypeSettingsEventEnum)
          {
            PadTypeSettings padTypeSetting = (PadTypeSettings)projectChangeEvent.getSource();
            PadType padType = padTypeSetting.getPadType();
            // only care if pad type is currently displayed in table
            if (_padTypeTableModel.isComponentPadDisplayed(padType))
            {
              _padTypeTableModel.updateSpecificPadType(padType);
            }
            
            //XCR1374, KhangWah, 2011-09-15
            if (_unTestableReasonsTableModel.isComponentPadDisplayed(padType))
            {
                _unTestableReasonsTableModel.updateSpecificPadType(padType);
            }
            
            ComponentType compType = padType.getComponentType();
            _componentTableModel.updateSpecificComponent(compType);
          }
          else if (eventEnum instanceof PadTypeEventEnum)
          {
            PadType padType = (PadType)projectChangeEvent.getSource();
            if (_padTypeTableModel.isComponentPadDisplayed(padType))
            {
              _padTypeTableModel.updateSpecificPadType(padType);
              ComponentType compType = padType.getComponentType();
              _componentTableModel.updateSpecificComponent(compType);
            }
            
            //XCR1374, KhangWah, 2011-09-15
            if (_unTestableReasonsTableModel.isComponentPadDisplayed(padType))
            {
                _unTestableReasonsTableModel.updateSpecificPadType(padType);
            }
          }
          else if (eventEnum instanceof PanelSettingsEventEnum)
          {
            // the tables in general will stay the same, so all we need to do is update their contents
            _componentTableModel.fireTableRowsUpdated(0, _componentTableModel.getRowCount());
            _padTypeTableModel.fireTableRowsUpdated(0, _padTypeTableModel.getRowCount());
            
            //XCR1374, KhangWah, 2011-09-15
            _unTestableReasonsTableModel.fireTableRowsUpdated(0, _unTestableReasonsTableModel.getRowCount());
          }
          else if (eventEnum instanceof SubtypeEventEnum)
          {
            // the tables in general will stay the same, so all we need to do is update their contents
            _componentTableModel.fireTableRowsUpdated(0, _componentTableModel.getRowCount());
            _padTypeTableModel.fireTableRowsUpdated(0, _padTypeTableModel.getRowCount());
            
            //XCR1374, KhangWah, 2011-09-15
            _unTestableReasonsTableModel.fireTableRowsUpdated(0, _unTestableReasonsTableModel.getRowCount());
          }
          else if (eventEnum instanceof ProjectEventEnum)
          {
            if(eventEnum.equals(ProjectEventEnum.DISPLAY_UNITS))
            {
              _packageTable.setDecimalPlacesToDisplay(_project.getDisplayUnits());
              _packagePinTable.setDecimalPlacesToDisplay(_project.getDisplayUnits());
            }
          }
          else if (eventEnum instanceof SubtypeAdvanceSettingsEventEnum)
          {
            // the tables in general will stay the same, so all we need to do is update their contents
            _componentTableModel.fireTableRowsUpdated(0, _componentTableModel.getRowCount());
            _padTypeTableModel.fireTableRowsUpdated(0, _padTypeTableModel.getRowCount());
          }
          else
          {
//            System.out.println("apm - FULL repopulate of Groupings because of event: " + eventEnum);
            updateProjectData();
          }
        }
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleGuiEvent(GuiEvent guiEvent)
  {
    GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
    if (guiEventEnum instanceof SelectionEventEnum)
    {
      SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
      if (selectionEventEnum.equals(SelectionEventEnum.BOARD_TYPE))
      {
        _currentBoardType = _testDev.getCurrentBoardType();
        boardTypeChanged();
      }
    }
   /* else if(guiEventEnum instanceof ViewModeEventEnum)
    {
      ViewModeEventEnum viewModeEventEnum = (ViewModeEventEnum) guiEventEnum;
      if (viewModeEventEnum.equals(ViewModeEventEnum.SCREEN))
      {
        if(_viewPackage == false)
        {
          _componentTableModel.fireTableDataChanged();
          _componentTableModel.fireTableStructureChanged();
          
          _padTypeTableModel.fireTableDataChanged();
          _padTypeTableModel.fireTableStructureChanged();
        }
        else
        {
          _packageTableModel.fireTableDataChanged();
          _packageTableModel.fireTableStructureChanged();
          
          _packagePinTableModel.fireTableDataChanged();
          _packagePinTableModel.fireTableStructureChanged();
        }
      }  
    }*/
    else if(guiEventEnum instanceof TaskPanelScreenEnum)
    {
      if(guiEventEnum.equals(TaskPanelScreenEnum.MODIFY_SUBTYPES))
      {
        _testDev.setGroupSelectMode(true);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleUndoState(Object stateObject)
  {
    UndoState undoState = (UndoState)stateObject;
    int viewSelectIndex = undoState.getModifySubtypesViewSelectIndex();
    _viewSelectComboBox.setSelectedIndex(viewSelectIndex);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    _mainBorder = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(115, 114, 105),new Color(165, 163, 151)),BorderFactory.createEmptyBorder(5,5,5,5));
    setLayout(new BorderLayout());

    _boardLabel.setText(StringLocalizer.keyToString("MMGUI_GROUPINGS_BOARDS_KEY"));
    _viewLabel.setText(StringLocalizer.keyToString("MMGUI_GROUPINGS_VIEWBY_KEY"));
    _contentPanel.setLayout(_contentPanelBorderLayout);
    _packageComponentPanel.setLayout(_packageComponentPanelBorderLayout);

    _detailsPanel.setLayout(_detailsPanelBorderLayout);
    _detailsLabel.setBorder(BorderFactory.createEmptyBorder(0,5,0,0));
    _detailsLabel.setText(_COMPONENT_DETAILS_STRING);
    
    //XCR1374, KhangWah, 2011-09-09
    _reasonsPanel.setLayout(_reasonsPanelBorderLayout);
    _reasonsLabel.setBorder(BorderFactory.createEmptyBorder(0,5,0,0));
    _reasonsLabel.setText(_UNTESTABLE_REASONS_STRING);
    
    this.setBorder(_mainBorder);
    _contentPanelBorderLayout.setVgap(20);
    _detailsPanelBorderLayout.setVgap(5);
    //XCR1374, KhangWah, 2011-09-09
    _reasonsPanelBorderLayout.setVgap(5);

    _centerSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
	_subCenterSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT); //XCR1374, KhangWah, 2011-09-09

    //added by Chin Seong, Kee
    _selectSubtypeToModifyButton.setText(StringLocalizer.keyToString("MMGUI_GROUPINGS_MODIFY_SUBTYPE_KEY"));
    JPanel _subtypeInfoPanel = new JPanel();
    _subtypeInfoPanel.setLayout(new BorderLayout());
    _subtypeInfoPanel.add(_selectSubtypeToModifyButton, BorderLayout.EAST);

     _selectSubtypeToModifyButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectSubtype_actionPerformed(e);
      }
    });
     
     //Kee Chin Seong - Seperate Subtypes From Top And Bottom 
    _separateSubtypesButton.setText(StringLocalizer.keyToString("MMGUI_GORUPINGS_SEPARATE_SUBTYPES_TOP_BOTTOM_KEY"));
    _subtypeInfoPanel.add(_separateSubtypesButton, BorderLayout.CENTER);
    
    _separateSubtypesButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        separateSubtype_actionPerformed(e);
      }
    });
    
    _applyVariatonButton.setText(StringLocalizer.keyToString("MMGUI_GROUPINGS_APPLY_VARIATION_KEY"));
    _subtypeInfoPanel.add(_applyVariatonButton, BorderLayout.WEST);
    
    _applyVariatonButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
         if(_componentTableModel.getSelectedComponentTypes().isEmpty() == false)
         {
            ApplySettingVariationSelectionDialog dlg = new ApplySettingVariationSelectionDialog(_project, _mainUI, new ArrayList<ComponentType>(_componentTableModel.getSelectedComponentTypes()),
                                                                                                StringLocalizer.keyToString("LOOP_DLG_MODIFY_SUBTYPES_SUBTYPE_ADVANCED_SETTINGS_KEY"));
            SwingUtils.centerOnComponent(dlg, _mainUI);
            dlg.setVisible(true);
         }
      }
    });

    JPanel viewBoardPanel = new JPanel();
    add(_northPanel,  BorderLayout.NORTH);
    viewBoardPanel.setLayout(_viewBoardPairLayout);
    _northPanel.setBorder(BorderFactory.createEmptyBorder(0,5,5,0));
    viewBoardPanel.add(_viewLabel);
    viewBoardPanel.add(_viewSelectComboBox);
    
    //added by Chin Seong, Kee
    //_subtypeInfoPanel.add(_showShadingCompensationColumnsCheckBox, FlowLayout.CENTER);

    _northPanel.add(viewBoardPanel, BorderLayout.WEST);

    _northPanel.add( _subtypeInfoPanel , BorderLayout.EAST);

    this.add(_contentPanel, BorderLayout.CENTER);
    _componentTableModel = new ModifySubtypesComponentTableModel();
    _packageTableModel = new ModifySubtypesPackageTableModel();
    _padTypeTableModel = new ModifySubtypesPadTypeTableModel();
    _unTestableReasonsTableModel = new ModifySubtypesUnTestableReasonsTableModel(); //XCR1374, KhangWah, 2011-09-15
    _packagePinTableModel = new ModifySubtypesPackagePinTableModel();

    _packageTable = new ModifyPackageTable(_packageTableModel)
    {
      /**
       * We override this to make sure the selected rows are still the proper selected
       * rows after a sort.
       * @author Andy Mechtenberg
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = _packageTable.getSelectedRows();
        java.util.List<Object> selectedData = new ArrayList<Object>();
        for (int i = 0; i < selectedRows.length; i++)
        {
          selectedData.add(_packageTableModel.getPackageAt(selectedRows[i]));
        }
        _shouldInteractWithGraphics = false;
        ListSelectionModel rowSM = _packageTable.getSelectionModel();
        rowSM.removeListSelectionListener(_componentPackageTableListSelectionListener); // add the listener for a details table row selection

        super.mouseReleased(event);

        clearSelection();
        // now, reselect everything

        Iterator it = selectedData.iterator();
        while (it.hasNext())
        {
          int row;
          Object obj = it.next();

          row = _packageTableModel.getRowForCompPackage((CompPackage) obj);

          _packageTable.addRowSelectionInterval(row, row);
        }
        SwingUtils.scrollTableToShowSelection(this);
        _shouldInteractWithGraphics = true;
        rowSM.addListSelectionListener(_componentPackageTableListSelectionListener); // add the listener for a details table row selection
      }
    };

    _packagePinTable = new ModifySubtypesPackagePinTable(_packagePinTableModel)
    {
      /**
       * We override this to make sure the selected rows are still the proper selected
       * rows after a sort.
       * @author Andy Mechtenberg
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = _packagePinTable.getSelectedRows();
        java.util.List<Object> selectedData = new ArrayList<Object>();
        for (int i = 0; i < selectedRows.length; i++)
        {
          selectedData.add(_packagePinTableModel.getPackagePinAt(selectedRows[i]));
        }

        _shouldInteractWithGraphics = false;
        ListSelectionModel rowSM = _packagePinTable.getSelectionModel();
        rowSM.removeListSelectionListener(_detailsTableListSelectionListener); // add the listener for a details table row selection

        super.mouseReleased(event);

        // now, reselect everything
        _packagePinTable.clearSelection();
        Iterator it = selectedData.iterator();
        while (it.hasNext())
        {
          int row;
          Object obj = it.next();
          row = _packagePinTableModel.getRowForPackagePin((PackagePin) obj);

          _packagePinTable.addRowSelectionInterval(row, row);
        }
        // after everything is selected, scroll to make the first one visible.
        SwingUtils.scrollTableToShowSelection(this);
        _shouldInteractWithGraphics =true;

        rowSM.addListSelectionListener(_detailsTableListSelectionListener); // add the listener for a details table row selection
      }
    };
    
    _componentTable = new ModifySubtypesComponentTable(_componentTableModel)
    {
      /**
       * We override this to make sure the selected rows are still the proper selected
       * rows after a sort.
       * @author Andy Mechtenberg
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = _componentTable.getSelectedRows();
        java.util.List<Object> selectedData = new ArrayList<Object>();
        for(int i = 0; i < selectedRows.length; i++)
        {
          selectedData.add(_componentTableModel.getComponentTypeAt(selectedRows[i]));
        }
        _shouldInteractWithGraphics = false;
        ListSelectionModel rowSM = _componentTable.getSelectionModel();
        rowSM.removeListSelectionListener(_componentPackageTableListSelectionListener); // add the listener for a details table row selection

        super.mouseReleased(event);

        _componentTable.clearSelection();
        // now, reselect everything

        Iterator it = selectedData.iterator();
        while (it.hasNext())
        {
          int row;
          Object obj = it.next();
          row = _componentTableModel.getRowForComponentType((ComponentType)obj);
          _componentTable.addRowSelectionInterval(row, row);
        }
        SwingUtils.scrollTableToShowSelection(_componentTable);
        _shouldInteractWithGraphics = true;
        rowSM.addListSelectionListener(_componentPackageTableListSelectionListener); // add the listener for a details table row selection
      }
    };
    _padTypeTable = new ModifySubtypesPadTypeTable(_padTypeTableModel)
    {
      /**
       * We override this to make sure the selected rows are still the proper selected
       * rows after a sort.
       * @author Andy Mechtenberg
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = _padTypeTable.getSelectedRows();
        java.util.List<Object> selectedData = new ArrayList<Object>();
        for (int i = 0; i < selectedRows.length; i++)
        {
          selectedData.add(_padTypeTableModel.getPadTypeAt(selectedRows[i]));
        }

        _shouldInteractWithGraphics = false;
        ListSelectionModel rowSM = _padTypeTable.getSelectionModel();
        rowSM.removeListSelectionListener(_detailsTableListSelectionListener); // add the listener for a details table row selection

        super.mouseReleased(event);

        // now, reselect everything
        _padTypeTable.clearSelection();
        Iterator it = selectedData.iterator();
        while (it.hasNext())
        {
          int row;
          Object obj = it.next();

          row = _padTypeTableModel.getRowForComponentPad((PadType) obj);
          _padTypeTable.addRowSelectionInterval(row, row);
        }
        // after everything is selected, scroll to make the first one visible.
        SwingUtils.scrollTableToShowSelection(this);
        _shouldInteractWithGraphics = true;

        rowSM.addListSelectionListener(_detailsTableListSelectionListener); // add the listener for a details table row selection
      }
    };
    //XCR1374, KhangWah, 2011-09-19
    _unTestableReasonsTable = new ModifySubtypesUnTestableReasonsTable(_unTestableReasonsTableModel)
    {
      /**
       * We override this to make sure the selected rows are still the proper selected
       * rows after a sort.
       * @author Andy Mechtenberg
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = _unTestableReasonsTable.getSelectedRows();
        java.util.List<Object> selectedData = new ArrayList<Object>();
        for (int i = 0; i < selectedRows.length; i++)
        {
          selectedData.add(_unTestableReasonsTableModel.getPadTypeAt(selectedRows[i]));
        }

        _shouldInteractWithGraphics = false;
        ListSelectionModel rowSM = _unTestableReasonsTable.getSelectionModel();
        rowSM.removeListSelectionListener(_unTestableReasonsTableListSelectionListener); // add the listener for a details table row selection

        super.mouseReleased(event);

        // now, reselect everything
        _unTestableReasonsTable.clearSelection();
        Iterator it = selectedData.iterator();
        while (it.hasNext())
        {
          int row;
          Object obj = it.next();

          row = _unTestableReasonsTableModel.getRowForComponentPad((PadType) obj);
          _unTestableReasonsTable.addRowSelectionInterval(row, row);
        }
        // after everything is selected, scroll to make the first one visible.
        SwingUtils.scrollTableToShowSelection(this);
        _shouldInteractWithGraphics = true;

        rowSM.addListSelectionListener(_unTestableReasonsTableListSelectionListener); // add the listener for a details table row selection
      }
    };
    _padTypeTable.setCurrentBoardType(_currentBoardType);
    _unTestableReasonsTable.setCurrentBoardType(_currentBoardType); //XCR1374, KhangWah, 2011-09-15
    //.setViewSelectComboBox(_viewSelectComboBox);
    _componentTable.setCurrentBoardType(_currentBoardType);

    _componentTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _componentTable.setSurrendersFocusOnKeystroke(false);
    _padTypeTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _unTestableReasonsTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION); //XCR1374, KhangWah, 2011-09-15
    _componentTable.sizeColumnsToFit(-1);
    _padTypeTable.sizeColumnsToFit(-1);
    _unTestableReasonsTable.sizeColumnsToFit(-1); //XCR1374, KhangWah, 2011-09-15

    _packageTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _packageTable.setSurrendersFocusOnKeystroke(false);
    _packagePinTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _packageTable.sizeColumnsToFit(-1);
    _packagePinTable.sizeColumnsToFit(-1);

    // add the listener for a table row selection
    _componentTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        multiEditTableMouseReleased(e, _componentTable);
      }
    });

    // add the listener for a table row selection
    _packageTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        multiEditTableMouseReleased(e, _packageTable);
      }
    });


    _padTypeTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        multiEditTableMouseReleased(e, _padTypeTable);
      }
    });
    
    //XCR1374, KhangWah, 2011-09-19
    _unTestableReasonsTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        multiEditTableMouseReleased(e, _unTestableReasonsTable);
      }
    });
     
    _packagePinTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        multiEditTableMouseReleased(e, _packagePinTable);
      }
    });

    _detailsTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        detailsTableListSelectionChanged(e);
      }
    };
        
    ListSelectionModel rowSM = _padTypeTable.getSelectionModel();
    rowSM.addListSelectionListener(_detailsTableListSelectionListener); // add the listener for a details table row selection
    rowSM = _packagePinTable.getSelectionModel();
    rowSM.addListSelectionListener(_detailsTableListSelectionListener); // add the listener for a details table row selection
    //XCR1374, KhangWah, 2011-09-15
    rowSM = _unTestableReasonsTable.getSelectionModel();
    rowSM.addListSelectionListener(_unTestableReasonsTableListSelectionListener); // add the listener for a untestable reasons table row selection
    
    //Kok Chun, Tan - XCR-2094 - put here instead of put in start() to avoid go into list selection listener 2 times
    _componentPackageTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        componentPackageTableListSelectionChanged(e);
      }
    };
    rowSM = _componentTable.getSelectionModel();
    rowSM.addListSelectionListener(_componentPackageTableListSelectionListener); // add the listener for a details table row selection
    rowSM = _packageTable.getSelectionModel();
    rowSM.addListSelectionListener(_componentPackageTableListSelectionListener); // add the listener for a details table row selection

    _subNorthPanel.add(_detailsLabel); //XCR1374, KhangWah, 2011-09-09
    _showUnTestableReasonsTableCheckBox.setText(StringLocalizer.keyToString("MMGUI_GROUPINGS_SHOW_UNTESTABLE_REASONS_KEY")); //XCR1374, KhangWah, 2011-09-09
    if(_viewPackage == false) //XCR1374, KhangWah, 2011-09-09
        _subNorthPanel.add(_showUnTestableReasonsTableCheckBox, BorderLayout.EAST); //XCR1374, KhangWah, 2011-09-09
    _detailsPanel.add(_subNorthPanel/*_detailsLabel*/,  BorderLayout.NORTH);
    _detailsPanel.add(_detailsScrollPane, BorderLayout.CENTER);
    _detailsScrollPane.getViewport().add(_padTypeTable, null);
    
    _reasonsPanel.add(_reasonsLabel,  BorderLayout.NORTH);
    _reasonsPanel.add(_reasonsScrollPane, BorderLayout.CENTER);
    _reasonsScrollPane.getViewport().add(_unTestableReasonsTable, null);
    
    _centerSplitPane.add(_packageComponentPanel, JSplitPane.TOP);
    _packageComponentPanel.add(_packageComponentScrollPane, BorderLayout.CENTER);
    _subCenterSplitPane.add(_detailsPanel, JSplitPane.TOP);
    _subCenterSplitPane.add(_reasonsPanel,  JSplitPane.BOTTOM);
    if (_showUnTestableReasons == false)
        _subCenterSplitPane.remove(_reasonsPanel);
    _centerSplitPane.add(_subCenterSplitPane/*_detailsPanel*/, JSplitPane.BOTTOM);
    _contentPanel.add(_centerSplitPane, java.awt.BorderLayout.CENTER);
    _packageComponentScrollPane.getViewport().add(_componentTable, null);
    _viewSelectComboBox.addItem(StringLocalizer.keyToString("MMGUI_GROUPINGS_COMPONENT_KEY"));
    _viewSelectComboBox.addItem(StringLocalizer.keyToString("MMGUI_GROUPINGS_PACKAGE_KEY"));
    _viewSelectComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        viewSelectComboBox_actionPerformed(e);
      }
    });
    _viewSelectComboBox.setName(".viewSelectComboBox");

	//XCR1374, KhangWah, 2011-09-09
    _showUnTestableReasonsTableCheckBox.addActionListener(new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            _showUnTestableReasons = _showUnTestableReasonsTableCheckBox.isSelected();
            if (_showUnTestableReasons == true)
            {
                _persistSettings = _testDev.getPersistance();
                _subCenterSplitPane.setDividerLocation(_persistSettings.getUnTestableReasonsDividerLocation());
                _subCenterSplitPane.add(_reasonsPanel,  JSplitPane.BOTTOM);
                
                if (_componentTableModel.getSelectedComponentTypes()!=null && 
                        _componentTableModel.getSelectedComponentTypes().isEmpty()==false)
                    _unTestableReasonsTableModel.populateWithComponentData(_componentTableModel.getSelectedComponentTypes());
                    _unTestableReasonsTableModel.fireTableDataChanged();
            }
            else
            {
                _persistSettings.setUnTestableReasonsDividerLocation(_subCenterSplitPane.getDividerLocation());
                _subCenterSplitPane.remove(_reasonsPanel);
            }
        }
    });

    _resetArtifactCompesationMenuItem.setText(StringLocalizer.keyToString("MMGUI_GROUPINGS_ARTIFACT_COMP_DISABLE_MENU_ITEM_KEY"));
    resetShadingCompesationMenuItemSetEnabled(false);
    _resetArtifactCompesationMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        int response = JOptionPane.showConfirmDialog(_mainUI,
                                                     StringLocalizer.keyToString("MMGUI_GROUPINGS_ARTIFACT_COMP_DISABLE_WARNING_KEY"),
                                                     StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                                                     JOptionPane.YES_NO_OPTION);
        if (response == JOptionPane.YES_OPTION)
        {
          _project.getPanel().getPanelSettings().disableArtifactCompensationForPanel();
        }
      }
    });
    _enableArtifactCompesationMenuItem.setText(StringLocalizer.keyToString("MMGUI_GROUPINGS_ARTIFACT_COMP_ENABLE_MENU_ITEM_KEY"));
    enableShadingCompesationMenuItemSetEnabled(false);
    _enableArtifactCompesationMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        // first give a warning that this operation will wipe out all existing custom shading compensation
        int response = JOptionPane.showConfirmDialog(_mainUI,
                                                     StringLocalizer.keyToString("MMGUI_GROUPINGS_ARTIFACT_COMP_ENABLE_WARNING_KEY"),
                                                     StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                                                     JOptionPane.YES_NO_OPTION);
        if (response == JOptionPane.YES_OPTION)
        {
          _project.getPanel().getPanelSettings().enableArtifactCompensationForPanel();
        }
      }
    });
    _setAllUseAutoFocusMenuItem.setText(StringLocalizer.keyToString("MMGUI_GROUPINGS_GLOBAL_SURFACE_MODEL_USE_AUTO_FOCUS_MENU_ITEM_KEY"));
    _setAllUseAutoFocusMenuItem.setEnabled(true);
    _setAllUseAutoFocusMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        // first give a warning that this operation will wipe out all existing custom focus method
        int response = JOptionPane.showConfirmDialog(_mainUI,
                                                     StringLocalizer.keyToString("MMGUI_GROUPINGS_SET_ALL_USE_AUTO_FOCUS_WARNING_KEY"),
                                                     StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                                                     JOptionPane.YES_NO_OPTION);
        if (response == JOptionPane.YES_OPTION)
        {
          _project.getPanel().getPanelSettings().setUseAutoFocusForPanel();
        }
      }
    });
    _setAllUseGlobalSurfaceModelMenuItem.setText(StringLocalizer.keyToString("MMGUI_GROUPINGS_GLOBAL_SURFACE_MODEL_USE_GLOBAL_SURFACE_MODEL_MENU_ITEM_KEY"));
    _setAllUseGlobalSurfaceModelMenuItem.setEnabled(true);
    _setAllUseGlobalSurfaceModelMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        // first give a warning that this operation will wipe out all existing custom focus method
        int response = JOptionPane.showConfirmDialog(_mainUI,
                                                     StringLocalizer.keyToString("MMGUI_GROUPINGS_SET_ALL_USE_GSM_WARNING_KEY"),
                                                     StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                                                     JOptionPane.YES_NO_OPTION);
        if (response == JOptionPane.YES_OPTION)
        {
          _project.getPanel().getPanelSettings().setUseGSMForPanel();
        }
      }
    });

    // RJG Disable until autoselect is implemented
    /**
    _setAllUseAutoSelectMenuItem.setText(StringLocalizer.keyToString("MMGUI_GROUPINGS_GLOBAL_SURFACE_MODEL_USE_AUTO_SELECT_MENU_ITEM_KEY"));
    _setAllUseAutoSelectMenuItem.setEnabled(true);
    _setAllUseAutoSelectMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        // first give a warning that this operation will wipe out all existing custom focus method
        int response = JOptionPane.showConfirmDialog(_mainUI,
                                                     StringLocalizer.keyToString("MMGUI_GROUPINGS_SET_ALL_USE_AUTO_SELECT_WARNING_KEY"),
                                                     StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                                                     JOptionPane.YES_NO_OPTION);
        if (response == JOptionPane.YES_OPTION)
        {
          _project.getPanel().getPanelSettings().setUseAutoSelectForPanel();
        }
      }
    });
     **/
  }

  /**
   * @author Laura Cormos
   */
  public void resetShadingCompesationMenuItemSetEnabled(boolean enabled)
  {
    _resetArtifactCompesationMenuItem.setEnabled(enabled);
  }

  /**
   * @author Laura Cormos
   */
  public void enableShadingCompesationMenuItemSetEnabled(boolean enabled)
  {
    _enableArtifactCompesationMenuItem.setEnabled(enabled);
  }
  
  /**
   * @author Kee Chin Seong
   * @param e
   */
  private void separateSubtype_actionPerformed(ActionEvent e)
  {
    final BusyCancelDialog _busyCancelDialog = new BusyCancelDialog(_mainUI,
                                                              StringLocalizer.keyToString("MMGUI_GROUPING_BUSYCANCELDIALOG_SEPARATING_SUBTYPE_MESSAGE_KEY"),
                                                              StringLocalizer.keyToString("MMGUI_GROUPINGS_BUSYCANCELDIALOG_UPDATE_CAD_TITLE_KEY"),
                                                              StringLocalizer.keyToString("MMGUI_MAGNIFICATION_CHANGED_CANCEL_BUTTON_KEY"), 
                                                              true);
    //creating the Busy Cancel Dialog with
    _busyCancelDialog.addCancelActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _isAbort = true;
      }
    });
    
    int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                                                StringLocalizer.keyToString("MMGUI_GROUPING_SEPERATE_COMPONENT_BUTTON_KEY"),
                                                StringLocalizer.keyToString("MMGUI_GORUPINGS_SEPARATE_SUBTYPES_TOP_BOTTOM_KEY"),
                                                JOptionPane.YES_NO_OPTION,
                                                JOptionPane.WARNING_MESSAGE);
    if (answer == JOptionPane.YES_OPTION)
    {
        if(_busyCancelDialog.isVisible() == false){
            _busyCancelDialog.pack();
            SwingUtils.centerOnComponent(_busyCancelDialog, _mainUI);
            _busyCancelDialog.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        }
        SwingWorkerThread.getInstance().invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
                int selectedRows[] = _componentTable.getSelectedRows();
                java.util.List<ComponentType> selectedData = new ArrayList<ComponentType>();
                for(int i = 0; i < selectedRows.length; i++)
                {
                  //Kee Chin Seong - Aborting the Looping, dont let it loop till the end when abort, GUI will look sluggish
                  if(_isAbort == true)
                     break;
                  selectedData.add(_componentTableModel.getComponentTypeAt(selectedRows[i]));
                }

                if(selectedData.size() == 0)//
                {
                  JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                                StringLocalizer.keyToString("MMGUI_GROUPING_SEPERATE_COMPONENT_NO_COMPONENT_SELECTED_ERROR_MESSAGE_KEY"),
                                                StringLocalizer.keyToString("MMGUI_GROUPING_SEPERATE_COMPONENT_NO_COMPONENT_SELECTED_ERROR_MESSAGE_KEY"),
                                                JOptionPane.ERROR_MESSAGE);
                  return;
                }

                _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SEPARATE_COMPONENT_TOP_BOTTOM_KEY"));
                for(ComponentType componentType : selectedData)
                {
                  String newSubtypeName = "";
                  boolean _isSubtypeSet = false;

                  //Kee Chin Seong - Aborting the Looping, dont let it loop till the end when abort, GUI will look sluggish
                  if(_isAbort == true)
                     break;
                  
                  //if(componentType.getPadTypeOne().getSubtype().is)
                  JointTypeEnum jointType = componentType.getPadTypeOne().getSubtype().getJointTypeEnum();
                  Subtype subType = componentType.getPadTypeOne().getSubtype();

                  boolean isTopComponent = false;
                  for(Component comp : componentType.getComponents())
                  {
                    if(comp.getReferenceDesignator().endsWith(componentType.getReferenceDesignator()))
                    {
                      isTopComponent = comp.isTopSide();
                      break;
                    }
                  }
                  String appendName = (isTopComponent == true) ? "_Top" : "_Bottom";

                  if(subType.getShortName().indexOf(appendName, 0) < 1)
                     newSubtypeName = subType.getShortName() + appendName;
                  else
                     newSubtypeName = subType.getShortName().substring(0, subType.getShortName().indexOf(appendName, 0)) + appendName;

                  for(Subtype subtype : componentType.getSideBoardType().getBoardType().getSubtypesIncludingUnused(jointType))
                  {
                     if(subtype.getShortName().equals(newSubtypeName))
                     {
                      try
                      {
                        _commandManager.execute(new ComponentTypeSetSubtypeCommand(componentType, subtype));
                      }
                      catch (XrayTesterException ex)
                      {
                         MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                                       ex.getLocalizedMessage(),
                                                       StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                       true);
                        // fireTableCellUpdated(rowIndex, columnIndex);
                         _componentTableModel.fireTableDataChanged();
                         _padTypeTableModel.fireTableDataChanged();
                       }
                       _isSubtypeSet = true; // we're done as soon as it's set
                     }
                  }

                  if(_isSubtypeSet == false && componentType.getCompPackage().usesOneJointTypeEnum() &&
                    componentType.getComponentTypeSettings().usesOneSubtype())
                  {
                    // if not set, then this is a new subtype and we need to create one
                    Panel panel = componentType.getSideBoardType().getBoardType().getPanel();
                    LandPattern landPattern = componentType.getLandPattern();
                    JointTypeEnum jointTypeEnum = componentType.getCompPackage().getJointTypeEnum();

                    Subtype newSubtype = panel.createSubtype(newSubtypeName, landPattern, jointTypeEnum);
                    try
                    {
                      _commandManager.execute(new ComponentTypeSetSubtypeCommand(componentType, newSubtype, true));
                    }
                    catch (XrayTesterException ex)
                    {
                      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                                    ex.getLocalizedMessage(),
                                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                    true);
                      _componentTableModel.fireTableDataChanged();
                      _padTypeTableModel.fireTableDataChanged();
                    }
                  }
                }
                _commandManager.endCommandBlock();
                
                //Kee Chin Seong - Aborting the Looping, dont let it loop till the end when abort, GUI will look sluggish
                //                 Abort need to revert back, by undo all the data. reset the key
                if(_isAbort == true)
                {
                  try
                  {
                     _commandManager.undo();
                  }
                  catch(XrayTesterException ex)
                  {
                  }
                  finally
                  {
                     //Remember Reset the key always
                     _isAbort = false;
                  }
                }
                
                _componentTableModel.fireTableDataChanged();
                _padTypeTableModel.fireTableDataChanged();
            }
            finally
            {
                // wait until visible
                while (_busyCancelDialog.isVisible() == false)
                {
                  try
                  {
                    Thread.sleep(200);
                  }
                  catch (InterruptedException ex)
                  {
                    // do nothing
                  }
                }
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    _busyCancelDialog.dispose();
                  }
                });
                
              }
            }
          });
          if(_busyCancelDialog.isVisible() == false)
             _busyCancelDialog.setVisible(true);
            
    }
     _componentTableModel.fireTableDataChanged();
     _padTypeTableModel.fireTableDataChanged();
  }

  /**
   * Kee Chin Seong
   * @param e
   */
  private void selectSubtype_actionPerformed(ActionEvent e)
  {
    // get it from config later on
    boolean isVariableMagnification = false;
    
    try
    {
      if(LicenseManager.isVariableMagnificationEnabled())
      {
        isVariableMagnification = true;
      }
    }
    catch(BusinessException be)
    {
      // do nothing
    }
    
    SubtypeAdvanceSettingDialog dlg = new SubtypeAdvanceSettingDialog(filterComponentBySubtype(_componentTableModel.getSelectedComponentTypes()),
                                                                        _currentBoardType, _mainUI, 
                                                                        StringLocalizer.keyToString("LOOP_DLG_MODIFY_SUBTYPES_SUBTYPE_ADVANCED_SETTINGS_KEY"), 
                                                                        isVariableMagnification);
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
  }

  /**
   * @param componentType
   * @return
   */
  private java.util.List<Subtype> filterComponentBySubtype(Collection<ComponentType> componentType)
  {
    java.util.List<Subtype> subtypes = new ArrayList<Subtype>();

    for (ComponentType comp : componentType)
    {
      for (PadType padType : comp.getPadTypes())
      {
        if (padType.isInspected() && subtypes.contains(padType.getSubtype()) == false)
          subtypes.add(padType.getSubtype());
      }
    }
    return subtypes;
  }

  /**
   * @author Andy Mechtenberg
   */
   void updateCadGraphics()
   {
     _testDev.initGraphicsToPackage();
     if (_viewPackage)
     {
       _testDev.switchToPackageGraphics();
       _testDev.changeGraphicsToPackage(_packageTableModel.getSelectedPackage());
     }
    else
    {
      _testDev.switchToPanelGraphics();
    }
   }

   /**
    * @author Andy Mechtenberg
    */
   public void unpopulate()
   {
     savePersistSettings();
     _project = null;
     _currentBoardType = null;
     _componentTable.clearSelection();
     _padTypeTable.clearSelection();
     _componentTable.resetSortHeader();
     _padTypeTable.resetSortHeader();
     _packageTable.clearSelection();
     _packagePinTable.clearSelection();
     _packageTable.resetSortHeader();
     _packagePinTable.resetSortHeader();
     _anchorComponentType = null;
     _componentTableModel.clear();
     _packageTableModel.clear();
     _padTypeTableModel.clear();
     _packagePinTableModel.clear();
     _rememberedSelectedComponents = new int[]{};
     _rememberedSelectedPackages = new int[]{};
     ProjectObservable.getInstance().deleteObserver(this);
     // add this as an observer of the gui
    _guiObservable.deleteObserver(this);
    
    //XCR1374, KhangWah, 2011-09-15
    _unTestableReasonsTable.clearSelection();
    _unTestableReasonsTable.resetSortHeader();
    _unTestableReasonsTableModel.clear();
   }

   /**
    * @author George Booth
    */
   public void populateWithProjectData()
   {
     // remove the stuff from the _comboBoxPanel because it actually get populated here.
     // the reason for this is because if there is only one board, I don't want to use a
     // combo box, I want to use a JLabel.  Here is where I know that, so I'll add the controls here.
     // If the user re-loads a new project after one has already been loaded, we need to redo this.
     _shouldInteractWithGraphics = false;
     _currentBoardType = _testDev.getCurrentBoardType();
     _viewSelectComboBox.setSelectedIndex(0);
     _viewPackage = false;

     //XCR1374, KhangWah, 2011-09-09
     if(_projectPersistSettings.showUnTestableReasonsInformation() == false)
     {
       _showUnTestableReasons = false;
     }
     else
     {
       _showUnTestableReasons = true;
     }

     validate();
     repaint();

     _componentTable.setCurrentBoardType(_currentBoardType);
     _padTypeTable.setCurrentBoardType(_currentBoardType);
     _componentTableModel.populateWithPanelData(_currentBoardType);
     _packageTableModel.populateWithPanelData(_currentBoardType);

     _padTypeTableModel.clear();
     _packagePinTableModel.clear();
     _detailsLabel.setText(_COMPONENT_DETAILS_STRING);

     _componentTable.scrollRectToVisible(new Rectangle(0,0,0,0));

     //XCR1374, KhangWah, 2011-09-15
     if (_showUnTestableReasons)
     {
       if (_showUnTestableReasonsTableCheckBox.isSelected() == false)
         _showUnTestableReasonsTableCheckBox.doClick();
     }
     _unTestableReasonsTable.setCurrentBoardType(_currentBoardType);
     _unTestableReasonsTableModel.clear();

     setupTableEditors();
   }

  /**
   * @author Andy Mechtenberg
   */
  public void populateWithProjectData(Project project)
  {
    Assert.expect(project != null);
    
    _showUnTestableReasons = false; //XCR1374, KhangWah, 2011-09-09
    _projectPersistSettings = MainMenuGui.getInstance().getTestDev().getPersistance().getProjectPersistance();
    
    _project = project;

    ProjectObservable.getInstance().addObserver(this);
    // add this as an observer of the gui
    _guiObservable.addObserver(this);

    if (_active)
    {
      populateWithProjectData();
      _populateProjectData = true;
    }
    else
      _populateProjectData = true;

  }

  /**
   * All controls are repopulated be re-reading out of datastore
   * @author Andy Mechtenberg
   */
  private void updateProjectData()
  {
//    System.out.println("Repopulating GROUPINGS!!!");
    _componentTableModel.populateWithPanelData(_currentBoardType);
    _packageTableModel.populateWithPanelData(_currentBoardType);
    _componentTable.clearSelection();
    _padTypeTable.clearSelection();
    _unTestableReasonsTable.clearSelection(); //XCR1374, KhangWah, 2011-09-15
    _packageTable.clearSelection();
    _packagePinTable.clearSelection();

    // now try to restore previous selections
    if (_viewPackage)
    {
      CompPackage selectedPackage = _packageTableModel.getSelectedPackage();
      Collection<PackagePin> selectedPackagePins = _packagePinTableModel.getSelectedPackagePins();

      int row = _packageTableModel.getRowForCompPackage(selectedPackage);
      if (row >= 0)
      {
        _packageTable.setRowSelectionInterval(row, row);

        for (PackagePin packagePin : selectedPackagePins)
        {
          row = _packagePinTableModel.getRowForPackagePin(packagePin); 
          //Kee Chin Seong - This will slow down the row updater
//          if (row > 0)
//            _packagePinTable.addRowSelectionInterval(row, row);
        }
      }
    }
    else
    {
      Collection<ComponentType> selectedComponents = _componentTableModel.getSelectedComponentTypes();
      // if there are no components selected, we need to clear the details table
      if (selectedComponents.isEmpty())
      {
        _padTypeTableModel.clear();
        _padTypeTableModel.fireTableDataChanged();
        _unTestableReasonsTableModel.clear(); //XCR1374, KhangWah, 2011-09-15
        _unTestableReasonsTableModel.fireTableDataChanged(); //XCR1374, KhangWah, 2011-09-15
        _anchorComponentType = null;
        _detailsLabel.setText(_COMPONENT_DETAILS_STRING);
        _componentTable.scrollRectToVisible(new Rectangle(0,0,0,0));
      }
      else
      {
        for (ComponentType compType : selectedComponents)
        {
          int row = _componentTableModel.getRowForComponentType(compType);
          //Kee Chin Seong - This will slow down the row updater
//          if (row != -1)
//            _componentTable.addRowSelectionInterval(row, row);
        }

        Collection<PadType> selectedPads = _padTypeTableModel.getSelectedPadTypes();
        for (PadType padType : selectedPads)
        {
          int row = _padTypeTableModel.getRowForComponentPad(padType);
          if (row != -1)
            _padTypeTable.addRowSelectionInterval(row, row);
          
          //XCR1374, KhangWah, 2011-09-15
          row = _unTestableReasonsTableModel.getRowForComponentPad(padType);
          if (row != -1)
              _unTestableReasonsTable.addRowSelectionInterval(row, row);
        }
      }
    }
    _packageTable.setDecimalPlacesToDisplay(_project.getDisplayUnits());
    _packagePinTable.setDecimalPlacesToDisplay(_project.getDisplayUnits());
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setupTableEditors()
  {
    if (_viewPackage)
    {
      _packageTable.setupCellEditorsAndRenderers();
      _packagePinTable.setupCellEditorsAndRenderers();      
      _packageTable.setDecimalPlacesToDisplay(_project.getDisplayUnits());
      _packagePinTable.setDecimalPlacesToDisplay(_project.getDisplayUnits());
    }
    else  // view component
    {
      _componentTable.setupCellEditorsAndRenderers();
      _padTypeTable.setupCellEditorsAndRenderers();
    }
  }

  /**
   * This method implements the right click edit when multiple lines of a table are selected
   * @author Andy Mechtenberg
   */
  private void multiEditTableMouseReleased(MouseEvent e, JTable table)
  {
    Assert.expect(e != null);
    Assert.expect(table != null);

    // ignore any other mouse events than a popup trigger -- we don't want to break the normal L&F
    if (e.isPopupTrigger())
    {
      // get the table cell that the mouse was right-clicked in
      Point p = e.getPoint();
      int row = table.rowAtPoint(p);
      int col = table.columnAtPoint(p);

      // if this cell is not currently selected, exit.  It's confusing to edit a cell that not selected.
      // we could "select" this cell too, but for now, we'll just not.
      if (table.isCellSelected(row, col) == false)
        return;

      // start the editing process for this cell
      table.editCellAt(row, col);
      // if this is a combo box cell editor, the editCellAt doesn't show the combo box popup, so let's show that
      // only if this is a combo box editor
      Object obj = table.getEditorComponent();
      if (obj instanceof JComboBox)
      {
        JComboBox comboBox = (JComboBox)obj;
        comboBox.showPopup();
      }
    }
  }

  /**
   * When the selection changes in the details table model, this method is called.
   * It should highlight in the graphics the pin(s) selected
   * @author Andy Mechtenberg
   */
  private void detailsTableListSelectionChanged(ListSelectionEvent e)
  {
//    System.out.println("Details Table List Value Changed changed");
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    int selectedRow = lsm.getAnchorSelectionIndex();
    if (lsm.isSelectionEmpty() || lsm.getValueIsAdjusting())
    {
      // do nothing
    }
    else
    {
//      int selectedRow = lsm.getMinSelectionIndex();
//      System.out.println("List Selection Changed To Row: " + selectedRow);
      if (_viewPackage)
      {
        // turn off event processing until graphics had a chance to update
        _packagePinTable.processSelections(false);

        int selectedRows[] = _packagePinTable.getSelectedRows();
        java.util.List<PackagePin> packagePins = new ArrayList<PackagePin>();
        for (int i = 0; i < selectedRows.length; i++)
        {
          PackagePin packagePin = _packagePinTableModel.getPackagePinAt(selectedRows[i]);
          packagePins.add(packagePin);
        }
        _packagePinTableModel.setSelectedPackagePins(packagePins);
        if (_shouldInteractWithGraphics)
        {
          _guiObservable.stateChanged(packagePins, SelectionEventEnum.PACKAGE_PIN);
        }
      }
      else
      {
        // turn off event processing until graphics had a chance to update
        _padTypeTable.processSelections(false);

        int selectedRows[] = _padTypeTable.getSelectedRows();
        java.util.List<PadType> selectedPads = new ArrayList<PadType>();
        for (int i = 0; i < selectedRows.length; i++)
        {
          selectedPads.add(_padTypeTableModel.getPadTypeAt(selectedRows[i]));
        }
        _padTypeTableModel.setSelectedPadTypes(selectedPads);
        
        //XCR1374, KhangWah, 2011-09-15
       //turn off event processing until graphics had a chance to update
       _unTestableReasonsTable.processSelections(false);
       int selectedRows2[] = _unTestableReasonsTable.getSelectedRows();
       java.util.List<PadType> selectedPads2 = new ArrayList<PadType>();
       for (int i = 0; i < selectedRows2.length; i++)
       {
           selectedPads2.add(_unTestableReasonsTableModel.getPadTypeAt(selectedRows2[i]));
       }
       _unTestableReasonsTableModel.setSelectedPadTypes(selectedPads2);
        
        // when viewing by component, changing the row here should select (highlight) the actual pin in the graphics
        if (_shouldInteractWithGraphics)
        {
          _guiObservable.stateChanged(selectedPads, SelectionEventEnum.PAD_TYPE);
        }
      }

      // wait for graphics
      SwingUtils.invokeLater(new Runnable()
      {

        public void run()
        {
          if (_viewPackage)
          {
            _packagePinTable.processSelections(true);
          }
          else
          {
            _padTypeTable.processSelections(true);
            _unTestableReasonsTable.processSelections(true); //XCR1374, KhangWah, 2011-09-15
          }
        }
      });
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void viewSelectComboBox_actionPerformed(ActionEvent e)
  {
    String selection = (String)_viewSelectComboBox.getSelectedItem();
    if (selection.equalsIgnoreCase(StringLocalizer.keyToString("MMGUI_GROUPINGS_PACKAGE_KEY")))
    {
      switchToViewPackage();
    }
    else
    {
      switchToViewComponent();
    }
  }

  /**
   * @author Andy Mechtenberg
   * @author Kee Chin Seong
   * - To Speed up the V810 software, remove unneccesary items, such as 
   *   - Image Tab.
   *   - Previously just control it to show when the verification image is AVAILABLE
   *   - now, the image tab will be removed after discussion with TME,
   *   - This tab only useful when we need adjust CAD.
   *   - remove all the verification image observers, and objects.
   */
  private void switchToViewPackage()
  {
    if (_viewPackage)
      return;

	_subNorthPanel.remove(_showUnTestableReasonsTableCheckBox); //XCR1374, KhangWah, 2011-09-09
    //XCR1374, KhangWah, 2011-09-19
    if (_showUnTestableReasons==true)
    {
        _persistSettings.setUnTestableReasonsDividerLocation(_subCenterSplitPane.getDividerLocation());
        _subCenterSplitPane.remove(_reasonsPanel); 
    }
    _detailsScrollPane.getViewport().remove(_padTypeTable);
    _detailsScrollPane.getViewport().add(_packagePinTable, null);
    _packageComponentScrollPane.getViewport().remove(_componentTable);
    _packageComponentScrollPane.getViewport().add(_packageTable, null);
    
    //Khaw Chek Hau - XCR2062: Modify Subtype & Separate Subtype button 
    _selectSubtypeToModifyButton.setEnabled(false);
    _separateSubtypesButton.setEnabled(false);
    _applyVariatonButton.setEnabled(false);
         
    resetShadingCompesationMenuItemSetEnabled(false);
    enableShadingCompesationMenuItemSetEnabled(false);

    // first remember any component selections so we can restore them later
    _rememberedSelectedComponents = _packageTable.getSelectedRows();
    _testDev.showCadWindow();
    
    // Kee Chin Seong - Comment up
    /*if(_project.getPanel().getPanelSettings().hasLowMagnificationComponent())
      _imagePanel.finishVerificationImagePanel();
    else
      _imagePanel.finishVerificationImagePanelForHighMag();*/

    _viewPackage = true;
    _packageTableModel.populateWithPanelData(_currentBoardType);
    _packageTable.setModel(_packageTableModel);
    _packageTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _packagePinTable.setModel(_packagePinTableModel);

    // pre-select to the anchor component's package.  If none, then use the remembered ones
    if (_anchorComponentType != null)
    {
      CompPackage pkg = _anchorComponentType.getCompPackage();
      int row = _packageTableModel.getRowForCompPackage(pkg);
      Assert.expect(row >= 0); // row MUST exist, right?
      _packageTable.addRowSelectionInterval(row, row);
    }
    else
    {
      for(int i = 0; i < _rememberedSelectedPackages.length; i++)
        _packageTable.addRowSelectionInterval(_rememberedSelectedPackages[i], _rememberedSelectedPackages[i]);
      if ((_rememberedSelectedPackages.length == 0) && (_packageTable.getRowCount() > 0))
        _packageTable.setRowSelectionInterval(0,0);  // select the first row by default
    }
    // now scroll to make this selection visible
    SwingUtils.scrollTableToShowSelection(_componentTable);
    if (_packageTable.getSelectedRowCount() > 0)
    {
      _packagePinTableModel.populateWithPackageData(_packageTableModel.getSelectedPackage());
    }
    _testDev.switchToPackageGraphics();
    setupTableEditors();
  }

  /**
   * @author Andy Mechtenberg
   * Edited by - @author Kee Chin Seong
   *  - Modify Subtype is slow while drawing image panel 
   *  - Should control by verification image validation
   * @author Kee Chin Seong
   * - To Speed up the V810 software, remove unneccesary items, such as 
   *   - Image Tab.
   *   - Previously just control it to show when the verification image is AVAILABLE
   *   - now, the image tab will be removed after discussion with TME,
   *   - This tab only useful when we need adjust CAD.
   *   - remove all the verification image observers, and objects.
   */
  private void switchToViewComponent()
  {
    if (_viewPackage == false)
      return;

    _subNorthPanel.add(_showUnTestableReasonsTableCheckBox, BorderLayout.EAST); //XCR1374, KhangWah, 2011-09-09
        
    _packageComponentScrollPane.getViewport().remove(_packageTable);
    _packageComponentScrollPane.getViewport().add(_componentTable, null);
    _detailsScrollPane.getViewport().remove(_packagePinTable);
    _detailsScrollPane.getViewport().add(_padTypeTable, null);
    
    //Khaw Chek Hau - XCR2062: Modify Subtype & Separate Subtype button 
    _selectSubtypeToModifyButton.setEnabled(true);
    _separateSubtypesButton.setEnabled(true);
    _applyVariatonButton.setEnabled(true);
    
    // first remember any package selections so we can restore them later
    _rememberedSelectedPackages = _componentTable.getSelectedRows();

   //sham
    //Kee chin Seong - Controling the image panel graphic engine
    //Comment up no use anymore
    /*if(_project.areVerificationImagesValid())
    {
      boolean checkLatestestProgram=true;   
      if(_project.getPanel().getPanelSettings().hasLowMagnificationComponent())
        _imagePanel.displayVerificationImagePanel(checkLatestestProgram);
      else
        _imagePanel.displayVerificationImagePanelForHighMag(checkLatestestProgram);
    }*/

    _viewPackage = false;
    _componentTable.setModel(_componentTableModel);
    _padTypeTable.setModel(_padTypeTableModel);

    //XCR1374, KhangWah, 2011-09-15
    _unTestableReasonsTable.setModel(_unTestableReasonsTableModel);
    
    _componentTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _componentTable.clearSelection();
    for(int i = 0; i < _rememberedSelectedComponents.length; i++)
    {
      int row = _rememberedSelectedComponents[i];
      if (row < _componentTable.getRowCount())
        _componentTable.addRowSelectionInterval(_rememberedSelectedComponents[i], _rememberedSelectedComponents[i]);
    }
    SwingUtils.scrollTableToShowSelection(_componentTable);
    if (_componentTable.getSelectedRows().length > 0)
    {
      if (_componentTableModel.isCommonSelectedComponentType())
      {
        _padTypeTableModel.populateWithComponentData(_componentTableModel.getSelectedComponentTypes());
        
        //XCR1374, KhangWah, 2011-09-15
        if (_showUnTestableReasons == true)
        {
          _unTestableReasonsTableModel.populateWithComponentData(_componentTableModel.getSelectedComponentTypes());
          _unTestableReasonsTableModel.fireTableDataChanged();
        }
      }
      SwingUtils.scrollTableToShowSelection(_componentTable);
    }
    _testDev.clearPackageGraphics();
    _testDev.switchToPanelGraphics();
    setupTableEditors();
    
    //XCR1374, KhangWah, 2011-09-09
    if(_showUnTestableReasons == true)
    {
      if(_showUnTestableReasonsTableCheckBox.isSelected() == false)
        _showUnTestableReasonsTableCheckBox.doClick();
      
      _subCenterSplitPane.setDividerLocation(_persistSettings.getUnTestableReasonsDividerLocation());
      _subCenterSplitPane.add(_reasonsPanel,  JSplitPane.BOTTOM);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void boardTypeChanged()
  {
    _padTypeTable.setCurrentBoardType(_currentBoardType);
    _packageTableModel.populateWithPanelData(_currentBoardType);
    _componentTable.setCurrentBoardType(_currentBoardType);
    _componentTableModel.populateWithPanelData(_currentBoardType);
    _packageTableModel.fireTableDataChanged();
    _componentTableModel.fireTableDataChanged();
    _padTypeTableModel.fireTableDataChanged();
    
    //XCR1374, KhangWah, 2011-09-15
    _unTestableReasonsTable.setCurrentBoardType(_currentBoardType);

    if (_componentTable.getRowCount() > 0)
    {
      _componentTable.setRowSelectionInterval(0, 0);
    }
    if (_packageTable.getRowCount() > 0)
    {
      _packageTable.setRowSelectionInterval(0, 0);
    }
  }

  /**
   * Task is ready to start
   * @author George Booth
   * @author Edited by Kee Chin Seong
   *  - Modify Subtype is slow while drawing image panel 
   *  - Should control by verification image validation
   *  - No longer using verification image panel. No value added,
   *    - Modify subtype will be optimized.
   *  - Adjust CAD has some interruption on the listener, have made some movement code.
   *    
   */
  public void start()
  {
//    System.out.println("GroupingsPanel().start()");

    com.axi.util.TimerUtil screenTimer = new com.axi.util.TimerUtil();
    screenTimer.start();

    if (_populateProjectData)
    {
      populateWithProjectData();
    }
    _populateProjectData = false;
    _showUnTestableReasons = _showUnTestableReasonsTableCheckBox.isSelected();

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();
    _testDev.addBoardSelectInToolbar(_envToolBarRequesterID);
    _active = true;

    // add custom menus
    JMenu toolsMenu = _menuBar.getToolsMenu();
    _artifactCompensationMenu.setText(StringLocalizer.keyToString("MMGUI_GROUPINGS_ARTIFACT_COMP_MENU_ITEM_KEY"));
    _artifactCompensationMenu.add(_resetArtifactCompesationMenuItem);
    _artifactCompensationMenu.add(_enableArtifactCompesationMenuItem);
    _menuBar.addMenuItem(_menuRequesterID, toolsMenu, _artifactCompensationMenu);
//    _menuBar.addMenuItem(_menuRequesterID, toolsMenu, _enableShadingCompesationMenuItem);

    // add custom menu for Global Surface Model
    _globalSurfaceModelMenu.setText(StringLocalizer.keyToString("MMGUI_GROUPINGS_GLOBAL_SURFACE_MODEL_MENU_ITEM_KEY"));
    _globalSurfaceModelMenu.add(_setAllUseAutoFocusMenuItem);
    _globalSurfaceModelMenu.add(_setAllUseGlobalSurfaceModelMenuItem);
    _globalSurfaceModelMenu.add(_setAllUseAutoSelectMenuItem);
    _menuBar.addMenuItem(_menuRequesterID, toolsMenu, _globalSurfaceModelMenu);

    // add custom toolbar components
    restorePersistSettings();
    updateCadGraphics();

    // generate test program if needed; notify user it is happening
    Project project = _currentBoardType.getPanel().getProject();
    if (project.isTestProgramValid() == false)
    {
      //modify subtype - sham
     /* _mainUI.generateTestProgramIfNeededWithReason(StringLocalizer.keyToString("MMGUI_TESTABILITY_NEEDS_TEST_PROGRAM_KEY"),
       StringLocalizer.keyToString("MM_GUI_TDT_TITLE_GROUPINGS_KEY"),
       project);*/
      _packageTableModel.fireTableDataChanged();
      _componentTableModel.fireTableDataChanged();
    }

    //Kee chin Seong - Controling the image panel graphic engine
    //Kee Chin Seong - No longer using this portion of code, because we no need show Verification Image Tab 
    //                 There is no use to show that tab to user !.
    /*if(_project.areVerificationImagesValid())
     {
     //modify subtype - image - sham
     boolean checkLatestestProgram=false;
      
     if(_project.getPanel().getPanelSettings().hasLowMagnificationComponent())
     _imagePanel.displayVerificationImagePanel(checkLatestestProgram);
     else
     _imagePanel.displayVerificationImagePanelForHighMag(checkLatestestProgram);
     }*/
    
    _testDev.showCadWindow();
    // Kee chin Seong - Not to Let user confuse / comment about the useless image tab shown,
    //                  i decide to remove it and show in other tab only,
    //                  In future, it might no need remove / maybe image tab need use only remove this line
    _testDev.removeImagePanel();

    if (_panelGraphicsSelectedRendererObservable == null)
      _panelGraphicsSelectedRendererObservable = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getSelectedRendererObservable();
    if (_packageGraphicsSelectedRendererObservable == null)
      _packageGraphicsSelectedRendererObservable = _testDev.getPackageGraphicsEngineSetup().getGraphicsEngine().getSelectedRendererObservable();

    //Kee chin Seong - Comment up, verification image panel tab no use anymore
    /*if(_project.getPanel().getPanelSettings().hasLowMagnificationComponent())
     {
     if (_verificationImageGraphicsSelectedRendererObservable == null)
     _verificationImageGraphicsSelectedRendererObservable =
     _imagePanel.getVerificationImagePanel().getVerificationImageGraphicsEngineSetup().getGraphicsEngine().getSelectedRendererObservable();
     }
     else
     {
     if (_verificationImageGraphicsSelectedRendererObservable == null)
     _verificationImageGraphicsSelectedRendererObservable =
     _imagePanel.getVerificationImagePanelForHighMag().getVerificationImageGraphicsEngineSetup().getGraphicsEngine().getSelectedRendererObservable();
     }*/
    
    _panelGraphicsSelectedRendererObservable.addObserver(this);
    _packageGraphicsSelectedRendererObservable.addObserver(this);
   //_verificationImageGraphicsSelectedRendererObservable.addObserver(this);

    // now it's time to update the graphics with the current selection
    _shouldInteractWithGraphics = true;

    if (_updateData)
    {
      updateProjectData();
    }
    _updateData = false;

    //sham - clear current selection
    Iterator<ComponentType> it = getComponentTableModel().getCurrentlySelectedComponents().iterator();
    if (it.hasNext())
    {
      ComponentType excludeComponentType = it.next();
      Collection<ComponentType> excludeComponentTypes = new ArrayList<ComponentType>();
      excludeComponentTypes.add(excludeComponentType);
      clearCurrentlySelectedComponents();
      getComponentTableModel().setSelectedComponentTypes(excludeComponentTypes);
      int row = _componentTableModel.getRowForComponentType(excludeComponentType);
      _componentTable.setRowSelectionInterval(row, row);
    }
    
    if (_testDev.doesActiveComponentTypeExist())
    {
      ComponentType componentTypeToSelect = _testDev.getActiveComponentType();
      if (_viewPackage)
      {
        _packageTable.clearSelection();
        CompPackage pkg = componentTypeToSelect.getCompPackage();
        int row = _packageTableModel.getRowForCompPackage(pkg);
        if (row >= 0)
        {
          _packageTable.addRowSelectionInterval(row, row);
        }
        else
        {
          // well, the package no longer exists -- I'll just select the first one (there had better be at least one!)
          _packageTable.addRowSelectionInterval(0, 0);
        }
      }
      else  // view component mode
      {
        int row = _componentTableModel.getRowForComponentType(componentTypeToSelect);
        if (row >= 0)
        {
          _componentTable.clearSelection();
          _componentTable.addRowSelectionInterval(row, row);
        }
      }
    }

    if (_viewPackage)
    {
      //Kok Chun, Tan - XCR-2094 - update graphics
      int[] selectedRows = _packageTable.getSelectedRows();
      CompPackage compPackage;
      java.util.List<CompPackage> packages = new ArrayList<CompPackage>();
      if (selectedRows.length == 0)
      {
        _packageTable.addRowSelectionInterval(0, 0);
        compPackage = _packageTableModel.getPackageAt(0);
        packages.add(compPackage);
      }
      else
      {
        compPackage = _packageTableModel.getPackageAt(selectedRows[0]);
        packages.add(compPackage);
      }
      _packageTableModel.setSelectedPackages(packages);
      _packagePinTableModel.populateWithPackageData(compPackage);
      _packageTableModel.setSelectedPackage(compPackage);
      _detailsLabel.setText(_PACKAGE_DETAILS_STRING + ": " + _packageTableModel.getSelectedPackage());
      _packagePinTable.clearSelection();
      
      if(_shouldInteractWithGraphics)
        _testDev.changeGraphicsToPackage(compPackage);
      
      SwingUtils.scrollTableToShowSelection(_packageTable);
    }
    else
    {
      int[] selectedRows = _componentTable.getSelectedRows();
      java.util.List<ComponentType> selectedComponents = new ArrayList<ComponentType>();
      for (int i = 0; i < selectedRows.length; i++)
      {
        selectedComponents.add(_componentTableModel.getComponentTypeAt(selectedRows[i]));
      }

      if (selectedComponents.isEmpty() == false)
        _guiObservable.stateChanged(selectedComponents, SelectionEventEnum.COMPONENT_TYPE);
      SwingUtils.scrollTableToShowSelection(_componentTable);
    }

    _commandManager.addObserver(this);

    //setCurrentButtonName();
    screenTimer.stop();
//   System.out.println("  Total Loading for Modify Subtypes: (in mils): "+ screenTimer.getElapsedTimeInMillis());
  }

  /**
   * @author Andy Mechtenberg
   */
  private void componentPackageTableListSelectionChanged(ListSelectionEvent e)
  {
//    System.out.println("PackageComponent Table List Value Changed changed");
    if (_active)
    {
      ListSelectionModel lsm = (ListSelectionModel) e.getSource();

      if (lsm.isSelectionEmpty() || lsm.getValueIsAdjusting())
      {
        // do nothing
      }
      else
      {
        int selectedRow = lsm.getAnchorSelectionIndex();  // returns the most recently selected one

    //      System.out.println("List Selection Changed To Row: " + selectedRow);
        if (_viewPackage)
        {
          //XCR-2094 - Kok Chun, Tan
          //add busy dialog when updating pins.
          if(_busyCancelDialog.isVisible() == false){
            _busyCancelDialog.pack();
            SwingUtils.centerOnComponent(_busyCancelDialog, _mainUI);
            _busyCancelDialog.setCursor(new Cursor(Cursor.WAIT_CURSOR));
          }
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                int firstSelectedPackageRowIndex = -1;

                // turn off event processing until graphics had a chance to update
                _packageTable.processSelections(false);
                int selectedRows[] = _packageTable.getSelectedRows();
                java.util.List<CompPackage> packages = new ArrayList<CompPackage>();
                for (int i = 0; i < selectedRows.length; i++)
                {
                  CompPackage compPackage = _packageTableModel.getPackageAt(selectedRows[i]);
                  packages.add(compPackage);

                  // Track of first selection
                  if (firstSelectedPackageRowIndex == -1)
                  {
                    firstSelectedPackageRowIndex = selectedRows[i];
                  }
                }
                _packageTableModel.setSelectedPackages(packages);

                //this is make sure the selection is always default if
                //user no put any selection.
                if (_packageTable.getSelectedRows().length == 0)
                {
                  firstSelectedPackageRowIndex = 0;
                }

                Assert.expect(firstSelectedPackageRowIndex >= 0);

                CompPackage compPackage = _packageTableModel.getPackageAt(firstSelectedPackageRowIndex);
                _packagePinTableModel.populateWithPackageData(compPackage);
                _packageTableModel.setSelectedPackage(compPackage);
                _detailsLabel.setText(_PACKAGE_DETAILS_STRING + ": " + _packageTableModel.getSelectedPackage());
                _packagePinTable.clearSelection();
                if (_shouldInteractWithGraphics)
                {
                  _testDev.changeGraphicsToPackage(_packageTableModel.getPackageAt(firstSelectedPackageRowIndex));
                  // add this as an observer of the graphics engine for selections
                }
              }
              finally
              {
                // wait until visible
                while (_busyCancelDialog.isVisible() == false)
                {
                  try
                  {
                    Thread.sleep(200);
                  }
                  catch (InterruptedException ex)
                  {
                    // do nothing
                  }
                }
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    _busyCancelDialog.dispose();
                  }
                });
              }
            }
          });
          if(_busyCancelDialog.isVisible() == false)
             _busyCancelDialog.setVisible(true);
        }
        else
        {
          // turn off event processing until graphics had a chance to update
          _componentTable.processSelections(false);
          int[] selectedComponentRows = _componentTable.getSelectedRows();
          _anchorComponentType = _componentTableModel.getComponentTypeAt(selectedRow);
          java.util.List<ComponentType> selectedComponents = new ArrayList<ComponentType>();
          for (int i = 0; i < selectedComponentRows.length; i++)
          {
            selectedComponents.add(_componentTableModel.getComponentTypeAt(selectedComponentRows[i]));
          }
          _componentTableModel.setSelectedComponentTypes(selectedComponents);
          if (_componentTableModel.isCommonSelectedComponentType())
          {
            _padTypeTableModel.populateWithComponentData(selectedComponents);

            String componentList = "";
            for (ComponentType componentType : selectedComponents)
            {
              componentList = componentList + componentType.getReferenceDesignator() + " ";
            }
            _detailsLabel.setText(_COMPONENT_DETAILS_STRING + ": " + componentList);

            java.util.List<PadType> selectedPads = _padTypeTableModel.getSelectedPadTypes();
            for (PadType padType : selectedPads)
            {
              if (selectedComponents.contains(padType.getComponentType()) == false)
              {
                _padTypeTable.clearSelection();
                break;
              }

            }

            //XCR1374, KhangWah, 2011-09-15
            if (_showUnTestableReasons == true)
            {
              _unTestableReasonsTableModel.populateWithComponentData(selectedComponents);
              _unTestableReasonsTableModel.fireTableDataChanged();
            }
          }
          else
          {
            _padTypeTable.clearSelection();
            _padTypeTableModel.clear();
            _detailsLabel.setText(_COMPONENT_DETAILS_STRING + ": ");

            //XCR1374, KhangWah, 2011-09-15
            _unTestableReasonsTable.clearSelection();
            _unTestableReasonsTableModel.clear();
          }

          if (_shouldInteractWithGraphics)
          {
            _guiObservable.stateChanged(selectedComponents, SelectionEventEnum.COMPONENT_TYPE);
          }
        }

        setupTableEditors();
        TableCellEditor cellEditor;
        if (_viewPackage)
        {
          cellEditor = _packagePinTable.getCellEditor();
        }
        else
        {
          cellEditor = _padTypeTable.getCellEditor();
        }

        if (cellEditor != null)
        {
          cellEditor.cancelCellEditing();
        }

        // wait for graphics
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            if (_viewPackage)
            {
              _packageTable.processSelections(true);
            }
            else
            {
              _componentTable.processSelections(true);
            }
          }
        });
      }
    }
  }

  /**
   * Can the environment be swapped out
   * @return true if environment can be swapped
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
//    System.out.println("GroupingsPanel().isReadyToFinish()");
    return true;
  }

  /**
   * The environment is about to be swapped out and should perform cleanup
   * @author George Booth
   * @author Kee Chin Seong
   * - To Speed up the V810 software, remove unneccesary items, such as 
   *   - Image Tab.
   *   - Previously just control it to show when the verification image is AVAILABLE
   *   - now, the image tab will be removed after discussion with TME,
   *   - This tab only useful when we need adjust CAD.
   *   - remove all the verification image observers, and objects.
   *   - remove the listener as well especially package component listener table
   */
  public void finish()
  {
//    System.out.println("GroupingsPanel().finish()");
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
   _shouldInteractWithGraphics = false;
    // Kee Chin Seong - Comment 
    //.finishVerificationImagePanel();
    //_imagePanel.finishVerificationImagePanelForHighMag();
   _testDev.clearPackageGraphics();
   _testDev.setActiveTunerComponentType();
   savePersistSettings();
   _panelGraphicsSelectedRendererObservable.deleteObserver(this);
   _packageGraphicsSelectedRendererObservable.deleteObserver(this);
   // Kee chin Seong - Comment ip and add back the image panel for other tabs
   //_verificationImageGraphicsSelectedRendererObservable.deleteObserver(this);
   _testDev.addImagePanel();
   _commandManager.deleteObserver(this);
     
    _active = false;
  }

  /**
   * @author sham
   */
  public void clearCurrentlySelectedComponents()
  {
    _componentTableModel.clearCurrentlySelectedComponents();
  }

  /**
   * @author sham
   */
  public ModifySubtypesComponentTableModel getComponentTableModel()
  {
    Assert.expect(_componentTableModel != null);

    return _componentTableModel;
  }
  
   /**
   * @author weng-jian.eoh
   * XCR-3589 Combo STD and XXL software GUI
   */
  public void removeAllObservable()
  {
    _commandManager.deleteObserver(this);
  }
}
