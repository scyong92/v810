package com.axi.v810.gui.testDev;

import java.awt.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * This class displays the information for all the components shown in Verify CAD
 * @author George Booth
 */
class ComponentLocationTable extends JSortTable
{
  public static final int _COMPONENT_NAME_INDEX = 0;
  public static final int _COMPONENT_X_INDEX = 1;
  public static final int _COMPONENT_Y_INDEX = 2;
  public static final int _COMPONENT_ROTATION_INDEX = 3;
  public static final int _COMPONENT_JOINT_TYPE_INDEX = 4;
  public static final int _COMPONENT_SUBTYPE_INDEX = 5;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _COMPONENT_NAME_COLUMN_WIDTH_PERCENTAGE = .25;
  private static final double _COMPONENT_X_COLUMN_WIDTH_PERCENTAGE = .10;
  private static final double _COMPONENT_Y_COLUMN_WIDTH_PERCENTAGE = .10;
  private static final double _COMPONENT_ROTATION_COLUMN_WIDTH_PERCENTAGE = .10;
  private static final double _COMPONENT_JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE = .25;
  private static final double _COMPONENT_SUBTYPE_COLUMN_WIDTH_PERCENTAGE = .20;

  // subtype change choices
  private final String _NEW_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NEW_KEY");
  private final String _NEW_SHADED_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NEW_SHADED_KEY");
  private final String _NOTEST_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOTEST_KEY");
  private final String _NOLOAD_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOLOAD_KEY");
  private final String _RENAME_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_RENAME_KEY");
  private final String _RESTORE_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_RESTORE_KEY");

  private NumericRenderer _numericRenderer = new NumericRenderer(2, JLabel.RIGHT);
  private NumericEditor _xNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2, 0.0, Double.MAX_VALUE);
  private NumericEditor _yNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2, 0.0, Double.MAX_VALUE);
  //Siew Yeng - XCR-2123 - temporary revert back to original behavior. Will continue to use this in 5.8
//  private NumericEditor _xNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2);
//  private NumericEditor _yNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2);
  private RotationCellEditor _rotationEditor;

  private ModifySubtypesComponentTableModel _tableModel;
  private boolean _modelSet = false;


  /**
   * Default constructor.
   * @author Carli Connally
   */
  public ComponentLocationTable(Component parent, ComponentLocationTableModel model)
  {
    Assert.expect(parent != null);
    Assert.expect(model != null);
    _tableModel = model;
    _modelSet = true;
    super.setModel(model);
    _rotationEditor = new RotationCellEditor(parent, true);
//    setRowSelectionAllowed(true);
//    setColumnSelectionAllowed(true);
    
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  }

    /**
   * Render all columns with the specified cell renderer
   * @param cellRender TableCellRenderer
   *
   * @author Wei Chin, Chong
   */
  public void renderColumns(TableCellRenderer cellRender)
  {
    for (int i = 0; i < this.getModel().getColumnCount(); i++)
    {
      renderColumn(this.getColumnModel().getColumn(i), cellRender);
    }
  }

  /**
   * @author Wei Chin,Chong
   */
  public void renderColumn(TableColumn col, TableCellRenderer cellRender)
  {
    try
    {
      col.setCellRenderer(cellRender);
    }
    catch(Exception e)
    {
      Assert.expect(false,e.getMessage());
    }
  }

  /**
   * Gets an array of column's width for persisted settings
   * @author George Booth
   */
  public int[] getColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int[] columnWidths = new int[columnModel.getColumnCount()];
    for (int i = 0; i < columnModel.getColumnCount(); i++)
    {
      columnWidths[i] = columnModel.getColumn(i).getWidth();
//      System.out.println("  get column[" + i + "] width = " + columnWidths[i]);
    }
    return columnWidths;
  }

  /**
   * Sets each column's width based on persisted settings
   * @author George Booth
   */
  public void setColumnWidths(int[] columnWidths)
  {
    TableColumnModel columnModel = getColumnModel();

    for (int i = 0; i < columnWidths.length; i++)
    {
//      System.out.println("  set column[" + i + "] width = " + columnWidths[i]);
      columnModel.getColumn(i).setWidth(columnWidths[i]);
    }

  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author George Booth
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_COMPONENT_NAME_INDEX).setPreferredWidth((int)(totalColumnWidth * _COMPONENT_NAME_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_COMPONENT_X_INDEX).setPreferredWidth((int)(totalColumnWidth * _COMPONENT_X_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_COMPONENT_Y_INDEX).setPreferredWidth((int)(totalColumnWidth * _COMPONENT_Y_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_COMPONENT_ROTATION_INDEX).setPreferredWidth((int)(totalColumnWidth * _COMPONENT_ROTATION_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_COMPONENT_JOINT_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _COMPONENT_JOINT_TYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_COMPONENT_SUBTYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _COMPONENT_SUBTYPE_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * @author George Booth
   */
  public void setEditorsAndRenderers(Set<Double> customRotationValues)
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };
    columnModel.getColumn(_COMPONENT_NAME_INDEX).setCellEditor(stringEditor);

    columnModel.getColumn(_COMPONENT_X_INDEX).setCellEditor(_xNumericEditor);
    columnModel.getColumn(_COMPONENT_X_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(_COMPONENT_Y_INDEX).setCellEditor(_yNumericEditor);
    columnModel.getColumn(_COMPONENT_Y_INDEX).setCellRenderer(_numericRenderer);

    _rotationEditor.removeCustomValues();
    _rotationEditor.setCustomValues(customRotationValues);
    columnModel.getColumn(_COMPONENT_ROTATION_INDEX).setCellEditor(_rotationEditor);
    columnModel.getColumn(_COMPONENT_ROTATION_INDEX).setCellRenderer(_numericRenderer);

    // column 4 is the joint type
    JComboBox componentJointTypeComboBox = new JComboBox();
    JointTypeEnumCellEditor jointTypeEnumCellEditor = new JointTypeEnumCellEditor(componentJointTypeComboBox, _tableModel);
    columnModel.getColumn(_COMPONENT_JOINT_TYPE_INDEX).setCellEditor(jointTypeEnumCellEditor);

    // column 5 is the subtype
    JComboBox componentSubtypeComboBox = new JComboBox();
    ComponentSubtypeCellEditor componentSubtypeCellEditor = new ComponentSubtypeCellEditor(componentSubtypeComboBox, _tableModel);
    componentSubtypeCellEditor.setEditorChoices(_NEW_SUBTYPE, _NEW_SHADED_SUBTYPE, _NOTEST_SUBTYPE, _NOLOAD_SUBTYPE, _RENAME_SUBTYPE, _RESTORE_SUBTYPE);
//    columnModel.getColumn(_COMPONENT_SUBTYPE_INDEX).setCellEditor(new DefaultCellEditor(componentSubtypeComboBox));
    columnModel.getColumn(_COMPONENT_SUBTYPE_INDEX).setCellEditor(componentSubtypeCellEditor);
    columnModel.getColumn(_COMPONENT_SUBTYPE_INDEX).setCellRenderer(new ComponentLocationTableCellRenderer());
  }

  /**
   * @author George Booth
   */
  public void resetEditorsAndRenderers()
  {
    _rotationEditor.removeCustomValues();
  }

  public ModifySubtypesComponentTableModel getTableModel()
  {
    return _tableModel;
  }

  /**
   * The decimal places to be displayed can change based on the measurement value that
   * is being displayed.  This method changes the renderers/editors to display the appropriate
   * number of decimal places.
   * @param decimalPlaces Number of decimal places to be displayed for all measurement values in the table.
   * @author George Booth
   */
  public void setDecimalPlacesToDisplay(int decimalPlaces)
  {
    Assert.expect(decimalPlaces >= 0);

    _xNumericEditor.setDecimalPlaces(decimalPlaces);
    _yNumericEditor.setDecimalPlaces(decimalPlaces);
    _numericRenderer.setDecimalPlacesToDisplay(decimalPlaces);
  }

  /**
   * @author George Booth
   */
  public void clearSelection()
  {
    TableModel model = getModel();
    // this function gets called before the table model is set, during intialization. At that time,
    // the model will be a defaultTableModel so the assert is sure to fail
    if (_modelSet)
    {
      Assert.expect(model instanceof ComponentLocationTableModel);
      ((ComponentLocationTableModel)model).clearSelectedComponent();
    }
    super.clearSelection();
  }

  /**
   * @author George Booth
   */
  void cancelCellEditing()
  {
    cancelCellEditingForColumn(_COMPONENT_X_INDEX);
    cancelCellEditingForColumn(_COMPONENT_Y_INDEX);
    cancelCellEditingForColumn(_COMPONENT_ROTATION_INDEX);
    cancelCellEditingForColumn(_COMPONENT_JOINT_TYPE_INDEX);
    cancelCellEditingForColumn(_COMPONENT_SUBTYPE_INDEX);
  }

  /**
   * @author George Booth
   */
  void cancelCellEditingForColumn(int column)
  {
    TableCellEditor cellEditor = getColumnModel().getColumn(column).getCellEditor();
    if (cellEditor != null)
    {
      cellEditor.cancelCellEditing();
    }
  }

}
