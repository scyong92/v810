package com.axi.v810.gui.testDev;

import javax.swing.*;
import javax.swing.table.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Chong, Wei Chin
 */
public class SignalIntegrationLevelCellRenderer extends DefaultTableCellRenderer
{
  public SignalIntegrationLevelCellRenderer()
  {
    // do nothing
  }

  /**
   * param table
   * @param value
   * @param isSelected
   * @param hasFocus
   * @param row
   * @param column
   * @return
   * 
   * @author Chong, Wei Chin
   */
  public java.awt.Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    if (value == null)
    {
      return null;
    }
    java.awt.Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
    final JLabel label = (JLabel)renderer;

    Subtype selectedSubtype = null;
    SubtypeAdvanceSettingsTable subtypeAdvanceSettingsTable = null;

    SignalCompensationEnum strValue = (SignalCompensationEnum) value;

    if (table instanceof SubtypeAdvanceSettingsTable)
    {
      subtypeAdvanceSettingsTable = (SubtypeAdvanceSettingsTable) table;
      selectedSubtype = subtypeAdvanceSettingsTable.getTableModel().getSubtypeAt(row);
    }

    label.setText(strValue.toString());
    
    if(selectedSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState().equals(ArtifactCompensationStateEnum.COMPENSATED))
      label.setEnabled(false);
    else
      label.setEnabled(true);
    
    // Chnee Khang Wah, 2013-04-23, new Entropy based IC
    if(selectedSubtype.getPanel().getProject().isGenerateByNewScanRoute())
    {
      label.setEnabled(true);
    }

    return label;
  }
}