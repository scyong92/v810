package com.axi.v810.gui.testDev;


import java.util.*;
import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
/**
 *
 * @author chin-seong.kee
 */
public class MagnificationTypeCellEditor extends DefaultCellEditor{

  private SubtypeAdvanceSettingsTableModel _subtypeTableModel = null;

  public MagnificationTypeCellEditor(JComboBox comboBox, SubtypeAdvanceSettingsTableModel subtypeTableModel)
  {
     super(comboBox);
     Assert.expect(subtypeTableModel != null);
     _subtypeTableModel = subtypeTableModel;
  }

  /**
   * @author Erica Wheatcroft
   */
  public java.awt.Component getTableCellEditorComponent(JTable table,
                                                        Object value,
                                                        boolean isSelectable,
                                                        int row,
                                                        int column)
  {
    // get the editor (should be a JComboBox as was specified in the constructor)
    // then modify the contents of the combo box to list only the valid choices for this row in the table
    int selectedRows[] = table.getSelectedRows();

    java.awt.Component editor = super.getTableCellEditorComponent(table, value, isSelectable, row, column);

    JComboBox comboBox = (JComboBox)editor;
    comboBox.removeAllItems();

    
    if(_subtypeTableModel != null)
    {
        Subtype subtype = _subtypeTableModel.getSubtypeAt(row);
        //List<SignalCompensationEnum> validIntegrationLevels = ProgramGeneration.getValidSignalCompensations(componentType.getPadTypes());

        comboBox.addItem(MagnificationTypeEnum.LOW);
        comboBox.addItem(MagnificationTypeEnum.HIGH);
        comboBox.setMaximumRowCount(2);

        // since the cell editor is active then we know they are all the same so just get the first pad type and display
        // the first integreation level. - this could still be the default
        comboBox.setSelectedItem(subtype.getSubtypeAdvanceSettings().getMagnificationType());
        return comboBox;
    }
    else
      Assert.expect(false);
    
    comboBox.setMaximumRowCount(2);
    return comboBox;
    }
}
