package com.axi.v810.gui.testDev;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

/**
 * This is wrapper class for the GUI interactions with reconstruction region.  This allows us to
 * cancel out of dialog operations without modifying the actual programs
 * @author Andy Mechtenberg
 */
public class FocusReconstructionRegionWrapper
{
  private ReconstructionRegion _reconstructionRegion;
  private boolean _isUsingRetest;
  private boolean _isUsingCustomFocus;
  private boolean _isTestable;
  private boolean _markedForFixFocus;

  private boolean _userAdjustedFocus;

  /**
   * @author Andy Mechtenberg
   */
  public FocusReconstructionRegionWrapper(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    _reconstructionRegion = reconstructionRegion;
    _isUsingRetest = _reconstructionRegion.isUsingRetest();
    _isUsingCustomFocus = _reconstructionRegion.isUsingCustomFocus();
    _isTestable = _reconstructionRegion.isTestableForUnfocusedRegions();
    _markedForFixFocus = _reconstructionRegion.isMarkedForFixFocus();

    _userAdjustedFocus = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void commitChanges()
  {
    // retest logic
    if (_isUsingRetest == false)
    {
      if (_reconstructionRegion.isUsingRetest())
        _reconstructionRegion.setUseRetest(false);
    }
    else  // using retest is true
    {
      if(_reconstructionRegion.isUsingRetest() == false)
        _reconstructionRegion.setUseRetest(true);
    }

    // custom focus logic
    if (_isUsingCustomFocus == false)
    {
      if (_reconstructionRegion.isUsingCustomFocus())
        _reconstructionRegion.clearCustomFocus();
    }
    else  // custom focus is true
    {
      if (_reconstructionRegion.isUsingCustomFocus() == false)
        Assert.expect(false);
    }

    // testable logic
    if (_isTestable == false)
    {
      if (_reconstructionRegion.isTestableForUnfocusedRegions())
        _reconstructionRegion.setTestestableForUnfocusedRegions(false);
    }
    else // testable is set to true
    {
      if (_reconstructionRegion.isTestableForUnfocusedRegions() == false)
        _reconstructionRegion.setTestestableForUnfocusedRegions(true);
    }

    // marked for fix logic
    if (_markedForFixFocus == false)
    {
      if (_reconstructionRegion.isMarkedForFixFocus())
        _reconstructionRegion.clearFixFocusLater();
    }
    else  // marked is set to TRUE
    {
      if (_reconstructionRegion.isMarkedForFixFocus() == false)
      {
        Slice slice = _reconstructionRegion.getInspectedSlices().iterator().next();
        _reconstructionRegion.setFixFocusLater(slice, true);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public ReconstructionRegion getReconstructionRegion()
  {
    return _reconstructionRegion;
  }

  /**
   * @author Andy Mechtenberg
   */
  public JointTypeEnum getJointTypeEnum()
  {
    return _reconstructionRegion.getComponent().getJointTypeEnums().get(0);
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isUsingRetest()
  {
    return _isUsingRetest;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setUseRetest(boolean retest)
  {
    _isUsingRetest = retest;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean wasCustomDefined()
  {
    return _reconstructionRegion.isUsingCustomFocus();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void revertToCustomOnly()
  {
    _isUsingCustomFocus = true;
    _markedForFixFocus = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isUsingCustomFocus()
  {
    return _isUsingCustomFocus;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isUsingCustomFocus(Slice slice)
  {
    Assert.expect(slice != null);
    return _reconstructionRegion.isUsingCustomFocus(slice);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void clearCustomFocus()
  {
    _isUsingCustomFocus = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void clearCustomFocus(Slice slice)
  {
    Assert.expect(slice != null);
    _reconstructionRegion.clearCustomFocus(slice);
    _userAdjustedFocus = true;
    _isUsingCustomFocus = _reconstructionRegion.isUsingCustomFocus();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setCustomFocus(Slice slice, int zOffsetInNanoMeters)
  {
    Assert.expect(slice != null);
    _reconstructionRegion.setCustomFocus(slice, zOffsetInNanoMeters);
    _userAdjustedFocus = true;
    _isUsingCustomFocus = _reconstructionRegion.isUsingCustomFocus();
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getCustomFocusZheightOffsetInNanoMeters(Slice slice)
  {
    Assert.expect(slice != null);
    return _reconstructionRegion.getCustomFocusZheightOffsetInNanoMeters(slice);
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isTestableForUnfocusedRegions()
  {
    return _isTestable;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setTestestableForUnfocusedRegions(boolean testable)
  {
    _isTestable = testable;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isMarkedForFixFocus()
  {
    return _markedForFixFocus;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setFixFocusLater(Slice slice, boolean fixFocusLater)
  {
    Assert.expect(slice != null);
    _reconstructionRegion.setFixFocusLater(slice, fixFocusLater);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setFixFocusLater(boolean fixFocusLater)
  {
    _markedForFixFocus = fixFocusLater;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void clearFixFocusLater()
  {
    _markedForFixFocus = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean userAdjustedFocus()
  {
    return _userAdjustedFocus;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    LocalizedString desc = new LocalizedString("BUS_RECONSTRUCTION_REGION_FULL_DESCRIPTION_KEY",
                                               new Object[]
                                               {_reconstructionRegion.getComponent().getReferenceDesignator(),
                                               _reconstructionRegion.getPadDescription()});
    return StringLocalizer.keyToString(desc);

  }
}
