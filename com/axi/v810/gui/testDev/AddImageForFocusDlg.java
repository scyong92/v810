package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 *
 * Dialog used to add multiple instances of a board type
 * to a panel.  This is a modal dialog.
 * @author Andy Mechtenberg
 */
class AddImageForFocusDlg extends EscapeDialog
{
  private Project _project = null;
  private ReconstructionRegion _reconstructionRegion;
  private SliceNameEnum _sliceNameEnum;
  private JointTypeEnum _jointTypeEnum;


  private BorderLayout _contentPaneBorderLayout = new BorderLayout();
  private JTextArea _cadMessageTextArea = new JTextArea();
  private Border _cadMessageBorder = BorderFactory.createLineBorder(Color.black, 2);
  private JPanel _outerButtonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();
  private JButton _cancelButton = new JButton();
  private JButton _okButton = new JButton();
  private FlowLayout _outerButtonFlowLayout = new FlowLayout();
  private GridLayout _innerButtonGridLayout = new GridLayout();
  private JButton _summaryButton = new JButton();
  private JPanel _dataControlPanel = new JPanel();
  private JPanel _infoPanel = new JPanel();
  private JPanel _optionsPanel = new JPanel();
  private GridLayout _optionsPanelGridLayout = new GridLayout();
  private JCheckBox _fixFocusCheckBox = new JCheckBox();
  private JCheckBox _retestCheckBox = new JCheckBox();
  private JCheckBox _noTestCheckBox = new JCheckBox();
  private JLabel _componentLabel = new JLabel();
  private JLabel _jointTypeDataLabel = new JLabel();
  private JLabel _jointTypeLabel = new JLabel();
  private JLabel _jointDataLabel = new JLabel();
  private JLabel _jointLabel = new JLabel();
  private JLabel _componentDataLabel = new JLabel();
  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private Border _mainPanelEmptyBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
  private Border _etchedBorder = BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166));
  private Border _optionsBorder = new TitledBorder(_etchedBorder, StringLocalizer.keyToString("GUI_FOCUS_OPTIONS_KEY"));
  private JPanel _imageInfoPanel = new JPanel();
  private FlowLayout _dataControlPanelFlowLayout = new FlowLayout();
  private GridLayout _imageInfoPanelGridLayout = new GridLayout();
  private Border _imageDetailsBorder = BorderFactory.createEmptyBorder(25, 0, 0, 0);
  private Border _dataControlBorder = BorderFactory.createEmptyBorder(10, 0, 0, 0);

  /**
   * @author Andy Mechtenberg
   */
  private AddImageForFocusDlg()
  {
    jbInit();
    pack();
  }

  /**
   * @author Andy Mechtenberg
   */
  public AddImageForFocusDlg(Project project, ReconstructionRegion reconstructionRegion, SliceNameEnum sliceNameEnum)
  {
    super(MainMenuGui.getInstance(), StringLocalizer.keyToString("GUI_FOCUS_ADD_TITLE_KEY"), true);
    Assert.expect(project != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);

    _project = project;
    _reconstructionRegion = reconstructionRegion;
    _sliceNameEnum = sliceNameEnum;
    _jointTypeEnum = _reconstructionRegion.getComponent().getJointTypeEnums().get(0);
    jbInit();

    pack();
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _project = null;
    _reconstructionRegion = null;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    _imageDetailsBorder = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)), StringLocalizer.keyToString("GUI_FOCUS_IMAGE_DETAILS_KEY"));
    _cadMessageBorder = BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.black, 2), BorderFactory.createEmptyBorder(0, 5, 0, 5));
    this.getContentPane().setLayout(_contentPaneBorderLayout);
    _cadMessageTextArea.setBackground(SystemColor.info);
    _cadMessageTextArea.setBorder(_cadMessageBorder);
    _cadMessageTextArea.setEditable(false);
    String cadMessage = StringLocalizer.keyToString("GUI_FOCUS_INFO_MESSAGE_KEY");

    _cadMessageTextArea.setText(StringUtil.format(cadMessage, 30));
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });

    _outerButtonPanel.setLayout(_outerButtonFlowLayout);
    _innerButtonPanel.setLayout(_innerButtonGridLayout);
    _innerButtonGridLayout.setHgap(5);
//    gridLayout1.setVgap(5);
    _summaryButton.setText(StringLocalizer.keyToString("GUI_FOCUS_SUMMARY_KEY"));
    _summaryButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        summaryButton_actionPerformed(e);
      }
    });
    _noTestCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        noTestCheckBox_actionPerformed();
      }
    });
    _dataControlPanel.setLayout(_dataControlPanelFlowLayout);
    _optionsPanel.setLayout(_optionsPanelGridLayout);
    _optionsPanelGridLayout.setRows(3);
    _fixFocusCheckBox.setText(StringLocalizer.keyToString("GUI_FOCUS_FIX_LATER_KEY"));
    if (_jointTypeEnum.isAvailableForRetestFailingPins() == false)
      _retestCheckBox.setEnabled(false);

    _retestCheckBox.setText(StringLocalizer.keyToString("GUI_FOCUS_FIX_WITH_RETEST_KEY"));
    _noTestCheckBox.setText(StringLocalizer.keyToString("GUI_FOCUS_FIX_WITH_NO_TEST_KEY"));
    _componentLabel.setText(StringLocalizer.keyToString("GUI_FOCUS_COMPONENT_KEY"));
    _componentDataLabel.setText(_reconstructionRegion.getComponent().getReferenceDesignator());
    _jointDataLabel.setText(_reconstructionRegion.getPadDescription());
    _jointLabel.setText(StringLocalizer.keyToString("GUI_FOCUS_JOINTS_KEY"));
    _jointTypeDataLabel.setText(_jointTypeEnum.getName());
    _jointTypeLabel.setText(StringLocalizer.keyToString("GUI_FOCUS_JOINT_TYPE_KEY"));

    _mainPanel.setLayout(_mainPanelBorderLayout);
    _mainPanel.setBorder(_mainPanelEmptyBorder);
    _optionsPanel.setBorder(_optionsBorder);
    _imageInfoPanel.setLayout(_imageInfoPanelGridLayout);
    _infoPanel.setBorder(_imageDetailsBorder);
    _imageInfoPanelGridLayout.setHgap(10);
    _dataControlPanel.setBorder(_dataControlBorder);
    _innerButtonPanel.add(_okButton);
    _innerButtonPanel.add(_cancelButton);
    _innerButtonPanel.add(_summaryButton);
    _infoPanel.setLayout(new PairLayout(10,10));
    _infoPanel.add(_componentLabel);
    _infoPanel.add(_componentDataLabel);
    _infoPanel.add(_jointLabel);
    _infoPanel.add(_jointDataLabel);
    _infoPanel.add(_jointTypeLabel);
    _infoPanel.add(_jointTypeDataLabel);
    _optionsPanel.add(_noTestCheckBox);
    _optionsPanel.add(_fixFocusCheckBox);
    _optionsPanel.add(_retestCheckBox);
    _mainPanel.add(_outerButtonPanel, java.awt.BorderLayout.SOUTH);
    _outerButtonPanel.add(_innerButtonPanel);
    _mainPanel.add(_cadMessageTextArea, java.awt.BorderLayout.NORTH);
    _mainPanel.add(_dataControlPanel, java.awt.BorderLayout.CENTER);
    _dataControlPanel.add(_imageInfoPanel);
    this.getContentPane().add(_mainPanel, java.awt.BorderLayout.SOUTH);
    _imageInfoPanel.add(_infoPanel, null);
    _imageInfoPanel.add(_optionsPanel, null);

    this.getRootPane().setDefaultButton(_okButton);

//    ButtonGroup buttonGroup = new ButtonGroup();
//    buttonGroup.add(_fixFocusCheckBox);
//    buttonGroup.add(_retestCheckBox);
//    buttonGroup.add(_noTestCheckBox);

    // set up the default
    // if the user's already set something for this region, use that as the default
    if (_reconstructionRegion.isTestableForUnfocusedRegions() == false)
      _noTestCheckBox.setSelected(true);
    if (_reconstructionRegion.isUsingRetest())
      _retestCheckBox.setSelected(true);
    if (_reconstructionRegion.isMarkedForFixFocus(_reconstructionRegion.getSlice(_sliceNameEnum)))
      _fixFocusCheckBox.setSelected(true);

    if (_noTestCheckBox.isSelected())
    {
      _retestCheckBox.setEnabled(false);
      _fixFocusCheckBox.setEnabled(false);
    }
    else
    {
      _retestCheckBox.setEnabled(true);
      _fixFocusCheckBox.setEnabled(true);
    }
    if (_jointTypeEnum.isAvailableForRetestFailingPins() == false)
      _retestCheckBox.setEnabled(false);

//    // if none of the above, default to no test
//    if (_reconstructionRegion.isTestableForUnfocusedRegions() &&
//       (_reconstructionRegion.isUsingRetest() == false) &&
//       (_reconstructionRegion.isMarkedForFixFocus(_reconstructionRegion.getSlice(_sliceNameEnum)) == false))
//      _noTestCheckBox.setSelected(true);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void noTestCheckBox_actionPerformed()
  {
    if (_noTestCheckBox.isSelected())
    {
      _retestCheckBox.setEnabled(false);
      _fixFocusCheckBox.setEnabled(false);
    }
    else
    {
      _retestCheckBox.setEnabled(true);
      _fixFocusCheckBox.setEnabled(true);
    }
    if (_jointTypeEnum.isAvailableForRetestFailingPins() == false)
      _retestCheckBox.setEnabled(false);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void summaryButton_actionPerformed(ActionEvent e)
  {
    FocusSummaryDlg dlg = new FocusSummaryDlg(_project, new FocusReconstructionRegionWrapper(_reconstructionRegion));
    SwingUtils.centerOnComponent(dlg, this);
    dlg.setVisible(true);
    dlg.unpopulate();
    dlg.dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    if (_fixFocusCheckBox.isSelected())
    {
      Slice slice = _reconstructionRegion.getSlice(_sliceNameEnum);
      _reconstructionRegion.setFixFocusLater(slice, true);
    }
    else
    {

      Slice slice = _reconstructionRegion.getSlice(_sliceNameEnum);
      if (_reconstructionRegion.isMarkedForFixFocus(slice))
        _reconstructionRegion.clearFixFocusLater();//        _reconstructionRegion.setFixFocusLater(slice, false);
    }
    if (_retestCheckBox.isSelected())
      _reconstructionRegion.setUseRetest(true);
    else if (_reconstructionRegion.isUsingRetest())
      _reconstructionRegion.setUseRetest(false);

    if (_noTestCheckBox.isSelected())
      _reconstructionRegion.setTestestableForUnfocusedRegions(false);
    else if (_reconstructionRegion.isTestableForUnfocusedRegions() == false)
      _reconstructionRegion.setTestestableForUnfocusedRegions(true);
    // all done, ditch the dialog
    dispose();
  }

  /**
   * Cancels the operation.  Dialog closes.
   * @author Andy Mechtenberg
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }
}
