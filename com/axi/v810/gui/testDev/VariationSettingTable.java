package com.axi.v810.gui.testDev;

import java.awt.Component;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 *
 * @author Kok Chun, Tan
 * @edited by Kee Chin Seong
 */
public class VariationSettingTable extends JTable
{
  public static final int _VARIATION_INDEX = 0;
  public static final int _ENABLE_INDEX = 1;
  public static final int _VIEWABLE_IN_PRODUCTION_INDEX = 2;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _VARIATION_COLUMN_WIDTH_PERCENTAGE = .90;
  private static final double _ENABLE_COLUMN_WIDTH_PERCENTAGE = .10;
  private static final double _VIEWABLE_IN_PRODUCTION_WIDTH_PERCENTAGE = .10;
  
  private CheckCellEditor _checkEditor = new CheckCellEditor(getBackground(), getForeground());

  /**
   * Default constructor.
   * @author Kok Chun, Tan
   */
  public VariationSettingTable()
  {
    // do nothing
    getTableHeader().setReorderingAllowed(false);
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Kok Chun, Tan
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int variationColumnWidth    = (int)(totalColumnWidth * _VARIATION_COLUMN_WIDTH_PERCENTAGE);
    int enableColumnWidth    = (int)(totalColumnWidth * _ENABLE_COLUMN_WIDTH_PERCENTAGE);
    int viewableInProduction = (int)(totalColumnWidth * _VIEWABLE_IN_PRODUCTION_WIDTH_PERCENTAGE);

    columnModel.getColumn(_VARIATION_INDEX).setPreferredWidth(variationColumnWidth);
    columnModel.getColumn(_ENABLE_INDEX).setPreferredWidth(enableColumnWidth);
    columnModel.getColumn(_VIEWABLE_IN_PRODUCTION_INDEX).setPreferredWidth(viewableInProduction);
  }

  /**
   * @author Kok Chun, Tan
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };
    columnModel.getColumn(_VARIATION_INDEX).setCellEditor(stringEditor);
    
    columnModel.getColumn(_ENABLE_INDEX).setCellEditor(_checkEditor);
    columnModel.getColumn(_ENABLE_INDEX).setCellRenderer(new CheckCellRenderer());
    columnModel.getColumn(_VIEWABLE_IN_PRODUCTION_INDEX).setCellRenderer(new CheckCellRenderer());
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public List<VariationSetting> getSelectedVariations()
  {
    VariationSettingTableModel model = (VariationSettingTableModel)getModel();
    java.util.List<VariationSetting> selectedVariations = new ArrayList<VariationSetting>();
    int rowCount = getRowCount();

    for(int i = 0; i < rowCount; ++i)
    {
      if(selectionModel.isSelectedIndex(i))
      {
        selectedVariations.add(model.getVariation(i));
      }
    }

    return selectedVariations;
  }
}
