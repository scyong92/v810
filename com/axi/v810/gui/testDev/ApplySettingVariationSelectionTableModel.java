package com.axi.v810.gui.testDev;

import java.util.*;
import java.util.logging.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 *
 * @author Chin Seong, Kee
 * @edited By Kee Chin Seong
 */
public class ApplySettingVariationSelectionTableModel extends AbstractTableModel
{
  private MainMenuGui _mainUI;
  //String labels retrieved from the localization properties file
  private static final String VARIATION_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_VARIATION_KEY");
  private static final String APPLY_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_ENABLE_KEY");

  private String[] _columnLabels = { VARIATION_LABEL,
                                     APPLY_LABEL};

  private Project _project = null;
  private static com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  
  private List<VariationSetting> _variationList = new ArrayList<VariationSetting> ();
  
  private SwingWorkerThread _busyDialogWorkerThread = SwingWorkerThread.getInstance();
  
  Map<VariationSetting, Boolean> _variantMapToApplySettings = new HashMap<VariationSetting, Boolean>();
  
  private List<ComponentType> _componentList = new ArrayList<ComponentType>();

  /**
   * Constructor.
   * @author Chin Seong, Kee
   */
  public ApplySettingVariationSelectionTableModel()
  {
    _mainUI = MainMenuGui.getInstance();
    // do nothing
  }

  /**
   * @author Chin Seong, Kee
   */
  void clearProjectData()
  {
    _project = null;
    _variantMapToApplySettings.clear();
  }

  /**
   * @author Kee Chin Seong
   */
  public void setComponentTypeList(List<ComponentType>componentList)
  {
    Assert.expect(componentList != null);
    
    _componentList = componentList;
  }
  
  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Chin Seong, Kee
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @param column Index of the column
   * @return Column label for the column specified
   * @author Chin Seong, Kee
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @param columnIndex Index of the column
   * @return Class data type for the column specified
   * @author Chin Seong, Kee
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * Overridden method.  Gets the values from the project object.
   * @param rowIndex Index of the row
   * @param columnIndex Index of the column
   * @return Object containing the value
   * @author Chin Seong, Kee
   * @edited By Kee Chin Seong
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_project != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    //get the appropriate data item that corresponds to the column
    switch(columnIndex)
    {
      case VariationSettingTable._VARIATION_INDEX:
      {
        value = _variationList.get(rowIndex).getName();
        break;
      }
      case VariationSettingTable._ENABLE_INDEX:
      {
        value = _variantMapToApplySettings.get(getVariation(rowIndex));
        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Chin Seong, Kee
   */
  public int getRowCount()
  {
    if (_variationList != null)
    {
      return _variationList.size();
    }
    return 0;
  }

  /**
   * Overridden method.
   * @param rowIndex  Index of the row
   * @param columnIndex  Index of the column
   * @return boolean - whether the cell is editable
   * @author Chin Seong, Kee
   * @edited By Kee Chin Seong
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    boolean isEditable = false;

    switch(columnIndex)
    {
      case ApplySettingVariationSelectionTable._VARIATION_INDEX:
      {
        isEditable = true;
        break;
      }
      case ApplySettingVariationSelectionTable._APPLY_INDEX:
      {
        isEditable = true;
        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }

    return isEditable;
  }

  /**
   * Initialize the table with data from a new project
   * @param project Project data
   * @author Chin Seong, Kee
   */
  public void setData(Project project)
  {
    Assert.expect(project != null);
    
    _project = project;
    _variationList = _project.getPanel().getPanelSettings().getAllVariationSettings();
    
    for(VariationSetting varSetting : _variationList)
    {
       _variantMapToApplySettings.put(varSetting, Boolean.FALSE);
    }
    fireTableDataChanged();
  }

  /**
   * Overridden method.  Saves the data entered by the user in the table
   * to the project object.
   * @param object Object containing the data to be saved to the model
   * @param rowIndex Index of the row
   * @param columnIndex Index of the column
   * @author Chin Seong, Kee
   */
  public void setValueAt(final Object object,final int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    switch(columnIndex)
    {
      case ApplySettingVariationSelectionTable._VARIATION_INDEX:
        break;
      case ApplySettingVariationSelectionTable._APPLY_INDEX:
      {    
        final BusyCancelDialog busyDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("MMGUI_SETUP_VARIATION_SETTING_KEY"),
          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
          true);

        busyDialog.pack();
        SwingUtils.centerOnComponent(busyDialog, _mainUI);

        _busyDialogWorkerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            Boolean value = (Boolean)object;
            _variantMapToApplySettings.put(getVariation(rowIndex), value);

            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                busyDialog.setVisible(false);
              }
            });
          }
        });
        busyDialog.setVisible(true);
        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }
    fireTableDataChanged();
  }
  
  /**
   * @author Kee Chin Seong
   * @return 
   */
  public Map<VariationSetting, Boolean> getVariationAppliedSettings()
  {
      Assert.expect(_variantMapToApplySettings != null);
      
      return _variantMapToApplySettings;
  }

  /**
   * @author Chin Seong, Kee
   */
  public VariationSetting getVariation(int rowIndex)
  {
    Assert.expect(rowIndex >= 0);
    
    return _variationList.get(rowIndex);
  }
}
