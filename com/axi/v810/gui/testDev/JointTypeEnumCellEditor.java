package com.axi.v810.gui.testDev;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.imageAnalysis.*;

/**
 * This class implements a combo box cell editor tailored to changing the joint type
 * for a package or a component
 * @author Andy Mechtenberg
 */
public class JointTypeEnumCellEditor extends DefaultCellEditor
{
  private ModifySubtypesPackageTableModel _packageTableModel;
  private ModifySubtypesComponentTableModel _componentTableModel;
  private boolean _usingPackages;
  private boolean _usingPsp;

  /**
   * @author Andy Mechtenberg
   */
  public JointTypeEnumCellEditor(JComboBox comboBox, ModifySubtypesPackageTableModel packageTableModel)
  {
    super(comboBox);

    Assert.expect(packageTableModel != null);
    _packageTableModel = packageTableModel;
    _usingPackages = true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public JointTypeEnumCellEditor(JComboBox comboBox, ModifySubtypesComponentTableModel componentTableModel)
  {
    super(comboBox);

    Assert.expect(componentTableModel != null);
    _componentTableModel = componentTableModel;
    _usingPackages = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public java.awt.Component getTableCellEditorComponent(JTable table, Object value, boolean isSelectable, int row, int column)
  {
    // get the editor (should be a JComboBox as was specified in the constructor)
    // then modify the contents of the combo box to list only the valid choices for this row in the table
    int selectedRows[] = table.getSelectedRows();
    boolean allowSpecialJointTypes = true;

    if (_usingPackages)
    {
      Assert.expect(_packageTableModel != null);
      // assign all booleans above by looking at each package selected.
      for (int i = 0; i < selectedRows.length; i++)
      {
        CompPackage compPackage = _packageTableModel.getPackageAt(selectedRows[i]);
        if (compPackage.getPackagePinsUnsorted().size() != 2)
        {
          allowSpecialJointTypes = false;
        }
      }
    }
    else if (_usingPsp)
    {
//     Assert.expect(_pspComponentTableModel != null);
//      // assign all booleans above by looking at each package selected.
//      for (int i = 0; i < selectedRows.length; i++)
//      {
//        ComponentType compType = _pspComponentTableModel.getComponentTypeAt(selectedRows[i]);
//        if (compType.getCompPackage().getPackagePinsUnsorted().size() != 2)
//        {
//          allowSpecialJointTypes = false;
//        }
//      }
    }
    else
    {
      Assert.expect(_componentTableModel != null);
      // assign all booleans above by looking at each package selected.
      for (int i = 0; i < selectedRows.length; i++)
      {
        ComponentType compType = _componentTableModel.getComponentTypeAt(selectedRows[i]);
        if (compType.getCompPackage().getPackagePinsUnsorted().size() != 2)
        {
          allowSpecialJointTypes = false;
        }
      }
    }
    java.awt.Component editor = super.getTableCellEditorComponent(table, value, isSelectable, row, column);
    JComboBox comboBox = (JComboBox)editor;
    comboBox.removeAllItems();
    // do the single selection first - easier if we get this out of the way
    java.util.List<JointTypeEnum> supportedJointTypes = JointTypeEnum.getAllJointTypeEnums();
    //    Collections.sort(supportedJointTypes, new JointTypeEnumComparator(true));

    for (JointTypeEnum jte : supportedJointTypes)
    {
      if (allowSpecialJointTypes)
        comboBox.addItem(jte);
      else
      {
        if (ImageAnalysis.isSpecialTwoPinComponent(jte) == false)
          comboBox.addItem(jte);
      }
    }

    //Kee chin Seong - No Longer need this because the select interval has been disabled.
    //Siew Yeng - XCR-2500 - uncomment this block to set default selection of Joint Type
    if (_usingPackages)
    {
      CompPackage compPackage = _packageTableModel.getPackageAt(selectedRows[0]);
      comboBox.setSelectedItem(compPackage.getJointTypeEnum());
    }
    else
    {
      ComponentType compType = _componentTableModel.getComponentTypeAt(selectedRows[0]);
      comboBox.setSelectedItem(compType.getJointTypeEnums().get(0));
    }

    comboBox.setMaximumRowCount(supportedJointTypes.size());
    return comboBox;
  }


}
