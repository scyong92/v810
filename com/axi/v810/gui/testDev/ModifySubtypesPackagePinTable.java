package com.axi.v810.gui.testDev;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 *
 * @author wei-chin.chong
 */
public class ModifySubtypesPackagePinTable extends JSortTable
{
  public static final int _JOINT_INDEX = 0;
  public static final int _JOINT_TYPE_INDEX = 1;
  public static final int _JOINT_HEIGHT_INDEX = 2;
  public static final int _JOINT_ORIENTATION_INDEX = 3;

  private static final int _PACKAGE_UNDO_INDEX = 1;

  private ModifySubtypesPackagePinTableModel _packagePinTableModel;

  private NumericRenderer _jointHeightNumericRenderer = new NumericRenderer(2, JLabel.RIGHT);
  private NumericEditor _detailsJointHeightNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2);

  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  /**
   * @param model
   */
  public ModifySubtypesPackagePinTable(ModifySubtypesPackagePinTableModel model)
  {
    Assert.expect(model != null);
    _packagePinTableModel = model;
    super.setModel(model);
  }

  /**
   * @return
   */
  public void setModel(ModifySubtypesPackagePinTableModel model)
  {
    Assert.expect(model != null);
    
    _packagePinTableModel = model;
     super.setModel(model);
  }

  /**
   * @return
   */
  public ModifySubtypesPackagePinTableModel getTableModel()
  {
    Assert.expect(_packagePinTableModel != null);
    return _packagePinTableModel;
  }

  /**
   * 
   */
  public void setupCellEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    // for this editor, we don't allow the pin 1 as whole device joint type enums
    JComboBox jointTypeForPackagePinComboBox = new JComboBox();
    java.util.List<JointTypeEnum> supportedJointTypes = JointTypeEnum.getAllJointTypeEnums();
    for (JointTypeEnum jte : supportedJointTypes)
    {
      if (ImageAnalysis.isSpecialTwoPinComponent(jte) == false)
      {
        jointTypeForPackagePinComboBox.addItem(jte);
      }
    }
    jointTypeForPackagePinComboBox.setMaximumRowCount(supportedJointTypes.size());
    columnModel.getColumn(_JOINT_TYPE_INDEX).setCellEditor(new DefaultCellEditor(jointTypeForPackagePinComboBox));

    // Column 2 - joint height
    columnModel.getColumn(_JOINT_HEIGHT_INDEX).setCellEditor(_detailsJointHeightNumericEditor);
    columnModel.getColumn(_JOINT_HEIGHT_INDEX).setCellRenderer(_jointHeightNumericRenderer);

    // Column 3 - pin orientation
    JComboBox orientationComboBox = new JComboBox();
    PackagePinOrientationCellEditor packagePinOrientationCellEditor = new PackagePinOrientationCellEditor(orientationComboBox, _packagePinTableModel);
    columnModel.getColumn(_JOINT_ORIENTATION_INDEX).setCellEditor(packagePinOrientationCellEditor);
  }

    /**
   * The decimal places to be displayed can change based on the measurement value that
   * is being displayed.  This method changes the renderers/editors to display the appropriate
   * number of decimal places.
   * @param enumSelectedUnits The project measurement units, selected by user
   * @author Laura Cormos
   */
  public void setDecimalPlacesToDisplay(MathUtilEnum enumSelectedUnits)
  {
    Assert.expect(enumSelectedUnits != null);

    int decimalPlaces = MeasurementUnits.getMeasurementUnitDecimalPlaces(enumSelectedUnits);

    _detailsJointHeightNumericEditor.setDecimalPlaces(decimalPlaces);
    _jointHeightNumericRenderer.setDecimalPlacesToDisplay(decimalPlaces);
  }

  /**
   * @param value
   * @param row
   * @param column
   */
  public void setValueAt(Object value, int row, int column)
  {
    int rows[] = getSelectedRows();
    int col = convertColumnIndexToModel(column);

    if (col == 1) // joint type
    {
      _commandManager.trackState(getCurrentUndoState());
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PACKAGE_PIN_SET_JOINT_TYPE_KEY"));
    }
    else if (col == 2) // joint height
    {
      _commandManager.trackState(getCurrentUndoState());
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PACKAGE_PIN_SET_JOINT_HEIGHT_KEY"));
    }
    else if (col == 3) // orientation column
    {
      _commandManager.trackState(getCurrentUndoState());
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PACKAGE_PIN_SET_ORIENTATION_KEY"));
    }

    for(int i = 0; i < rows.length; i++)
    {
      _packagePinTableModel.setValueAt(value, rows[i], col);
    }
    _commandManager.endCommandBlock();
  }

  /**
   * @author Erica Wheatcroft
   */
  private UndoState getCurrentUndoState()
  {
    UndoState undoState = new UndoState();
    undoState.setModifySubtypesViewSelectIndex(_PACKAGE_UNDO_INDEX);
    return undoState;
  }
}
