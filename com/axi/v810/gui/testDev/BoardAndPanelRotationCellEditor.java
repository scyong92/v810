package com.axi.v810.gui.testDev;

import javax.swing.*;


/**
 * <p>Title: RotationCellEditor</p>
 * <p>Description: Table cell editor for board or panel rotation values (0, 90, 180, 270,)</p>
 * @author Laura Cormos
 */

class BoardAndPanelRotationCellEditor extends DefaultCellEditor
{
  //Rotation strings
  private final String _ROTATION_0 = "0";
  private final String _ROTATION_90 = "90";
  private final String _ROTATION_180 = "180";
  private final String _ROTATION_270 = "270";

  JComboBox _comboBox = null;

  /**
   * @author Carli Connally
   * Constructor.
   * Initializes the drop down box with the rotation values
   */
  public BoardAndPanelRotationCellEditor()
  {
    super(new JComboBox());
    init();
  }

   /**
   * @author Laura Cormos
   */
  private void init()
  {
    _comboBox = (JComboBox)getComponent();

    _comboBox.addItem(_ROTATION_0);
    _comboBox.addItem(_ROTATION_90);
    _comboBox.addItem(_ROTATION_180);
    _comboBox.addItem(_ROTATION_270);
  }
}
