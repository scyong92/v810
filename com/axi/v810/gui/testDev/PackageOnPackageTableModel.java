package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.util.*;
import com.axi.guiUtil.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * @author Khaw Chek Hau
 * @XCR3554: Package on package (PoP) development
 */
public class PackageOnPackageTableModel extends DefaultSortTableModel
{
  private final String[] _packageFamilyTableColumnNames =
  {
    StringLocalizer.keyToString("ATGUI_PACKAGE_ON_PACKAGE_KEY"),
  };

  // side choices
  private final String _TOP_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_TOP_LABEL_KEY");
  private final String _BOTTOM_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_BOTTOM_LABEL_KEY");

  private List<CompPackageOnPackage> _packageOnPackages = new ArrayList<CompPackageOnPackage>();
  private CompPackageOnPackage _currentSelectedPOP;

  private boolean _isCellEditable = true;

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public PackageOnPackageTableModel()
  {
    // do nothing
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    CompPackageOnPackage compPackageOnPackage = _packageOnPackages.get(rowIndex);
    
    if(columnIndex == 0)
    {
      return compPackageOnPackage.toString();
    }
    else
    {
      Assert.expect(false);
      return null;
    }
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_packageFamilyTableColumnNames[columnIndex];
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    if (columnIndex == 0) // column 0 is package name -- not editable
      Assert.expect(false);
    else
      Assert.expect(false); // only column 1 is editable
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    if(_isCellEditable == false)
      return false;
    // column 0 is package name - not editable
    if (columnIndex == 0)
      return false;

    return false;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public int getRowCount()
  {
    if (_packageOnPackages == null)
      return 0;
    else
      return _packageOnPackages.size();
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public int getColumnCount()
  {
    return _packageFamilyTableColumnNames.length;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public CompPackageOnPackage getPackageAt(int selectedPackageNumber)
  {
    Assert.expect(selectedPackageNumber < _packageOnPackages.size());
    return _packageOnPackages.get(selectedPackageNumber);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  void clear()
  {
    _packageOnPackages.clear();
    _currentSelectedPOP = null;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void populateWithPanelData(Board board, BoardType boardType, String side)
  {
    Assert.expect(board != null);
    Assert.expect(boardType != null);
    Assert.expect(side != null);

    _packageOnPackages.clear();

    if (side.equalsIgnoreCase(_TOP_SIDE))
    {
      if (board.topSideBoardExists())
      {
        SideBoardType sideBoard = board.getTopSideBoard().getSideBoardType();
        _packageOnPackages.addAll(sideBoard.getCompPackageOnPackages());
      }
    }
    else if (side.equalsIgnoreCase(_BOTTOM_SIDE))
    {
      if (board.bottomSideBoardExists())
      {
        SideBoardType sideBoard = board.getBottomSideBoard().getSideBoardType();
        _packageOnPackages.addAll(sideBoard.getCompPackageOnPackages());
      }
    }
    else
    {
      _packageOnPackages.addAll(boardType.getCompPackageOnPackages());
    }

    fireTableDataChanged();
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  void updateSpecificPOP(CompPackageOnPackage compPackageOnPackage)
  {
    Assert.expect(compPackageOnPackage != null);
    int popRow = _packageOnPackages.indexOf(compPackageOnPackage);
    fireTableRowsUpdated(popRow, popRow);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  int getRowForCompPackageOnPackage(CompPackageOnPackage compPackageOnPackage)
  {
    Assert.expect(_packageOnPackages.size() > 0);

    if (compPackageOnPackage == null)
    {
      return 0;
    }

    String popName = compPackageOnPackage.getPOPName();

    for (int row = 0; row < _packageOnPackages.size(); row++)
    {
      CompPackageOnPackage thisPOP = _packageOnPackages.get(row);
      if (thisPOP.getPOPName().equals(popName))
      {
        return row;
      }
    }
    // not found
    return 0;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  void setSelectedPackage(CompPackageOnPackage compPackageOnPackage)
  {
    Assert.expect(compPackageOnPackage != null);
    _currentSelectedPOP = compPackageOnPackage;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  CompPackageOnPackage setSelectedPackageAt(int index)
  {
    Assert.expect(index >= 0 && index < _packageOnPackages.size());
    _currentSelectedPOP = _packageOnPackages.get(index);
    return _currentSelectedPOP;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  CompPackageOnPackage getSelectedPackage()
  {
    return _currentSelectedPOP;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void clearSelectedPackage()
  {
    _currentSelectedPOP = null;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void setIsCellEditable(boolean isCellEditable)
  {
     _isCellEditable = isCellEditable;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public int getRowForDataStartingWith(String key)
  {
    key = key.toLowerCase();
    int index = -1;
    for (CompPackageOnPackage compPackageOnPackage : _packageOnPackages)
    {
      index++;
      String popName = compPackageOnPackage.toString().toLowerCase();
      if (popName.startsWith(key))
      {
        return index;
      }
    }
    return -1;
  }
}
