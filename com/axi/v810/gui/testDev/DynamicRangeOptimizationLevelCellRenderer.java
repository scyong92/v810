package com.axi.v810.gui.testDev;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.v810.business.panelSettings.*;
/**
 *
 * @author siew-yeng.phang
 */
public class DynamicRangeOptimizationLevelCellRenderer extends DefaultTableCellRenderer
{
  
/**
 * @author Siew Yeng
 */
  public DynamicRangeOptimizationLevelCellRenderer()
  {
    // do nothing
  }
  
  /**
   * @author Siew Yeng
   */
  public java.awt.Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    if (value == null)
    {
      return null;
    }
    java.awt.Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
    final JLabel label = (JLabel)renderer;

    SubtypeAdvanceSettingsTable subtypeAdvanceSettingsTable = null;

    DynamicRangeOptimizationLevelEnum strValue = (DynamicRangeOptimizationLevelEnum) value;
    
    if (table instanceof SubtypeAdvanceSettingsTable)
    {
      subtypeAdvanceSettingsTable = (SubtypeAdvanceSettingsTable) table;
    }
    
    label.setText(strValue.toString());
    label.setEnabled(true);

    return label;
  }
}
