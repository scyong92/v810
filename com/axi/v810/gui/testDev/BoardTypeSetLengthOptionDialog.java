package com.axi.v810.gui.testDev;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * This dialog allows user to customize the way to extend board length (Y direction in panel coordinate)
 * @author Lim, Seng Yew
 */
public class BoardTypeSetLengthOptionDialog extends JDialog
{
  private boolean _ok = false;

  private java.awt.BorderLayout _mainBorderLayout = new java.awt.BorderLayout();
  private JPanel _selectPanel = new JPanel(new GridLayout(3, 1));
  private JPanel _okCancelPanel = new JPanel();
  private JPanel _okCancelInnerPanel = new JPanel();
  private java.awt.GridLayout _okCancelInnerGridLayout = new java.awt.GridLayout();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private java.awt.FlowLayout _okCanelFowLayout = new java.awt.FlowLayout();

  private JLabel _extendLengthWarningMessage = new JLabel();
  private JRadioButton _extendRightLengthRadioButton = new JRadioButton();
  private JRadioButton _extendLeftLengthRadioButton = new JRadioButton();
  private JRadioButton _extendEqualLengthRadioButton = new JRadioButton();

  private java.awt.Frame _myFrame = null;

  /**
   * @author Lim, Seng Yew
   */
  public BoardTypeSetLengthOptionDialog(java.awt.Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);

    _myFrame = frame;
    jbInit();

    pack();
  }

  /**
   * @author Lim, Seng Yew
   */
  private void jbInit()
  {
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    getContentPane().setLayout(_mainBorderLayout);

    _extendRightLengthRadioButton.setSelected(true);
    _extendRightLengthRadioButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADJUST_BOARD_TYPE_LENGTH_TOP_KEY"));
    _extendLeftLengthRadioButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADJUST_BOARD_TYPE_LENGTH_BOTTOM_KEY"));
    _extendEqualLengthRadioButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADJUST_BOARD_TYPE_LENGTH_EQUALLY_KEY"));

    _selectPanel.add(_extendRightLengthRadioButton);
    _selectPanel.add(_extendLeftLengthRadioButton);
    _selectPanel.add(_extendEqualLengthRadioButton);
    getContentPane().add(_selectPanel, java.awt.BorderLayout.NORTH);
    // custom stuff goes here
    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(_extendRightLengthRadioButton);
    buttonGroup.add(_extendLeftLengthRadioButton);
    buttonGroup.add(_extendEqualLengthRadioButton);

    _extendLengthWarningMessage.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADJUST_BOARD_TYPE_LENGTH_WARNING_MESSAGE_KEY"));
    getContentPane().add(_extendLengthWarningMessage, java.awt.BorderLayout.CENTER);

    _okCancelInnerPanel.setLayout(_okCancelInnerGridLayout);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });

    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });

    _okCancelPanel.setLayout(_okCanelFowLayout);
    _okCanelFowLayout.setAlignment(java.awt.FlowLayout.RIGHT);
    _okCancelInnerGridLayout.setHgap(20);

    getContentPane().add(_okCancelPanel, java.awt.BorderLayout.SOUTH);
    _okCancelPanel.add(_okCancelInnerPanel, null);
    _okCancelInnerPanel.add(_okButton, null);
    _okCancelInnerPanel.add(_cancelButton, null);

    getRootPane().setDefaultButton(_okButton);
  }

  /**
   * @author Lim, Seng Yew
   */
  public boolean userClickedOk()
  {
    return _ok;
  }

  /**
   * @author Lim, Seng Yew
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    _ok = false;
    dispose();
  }

  /**
   * @author Lim, Seng Yew
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    _ok = true;
    dispose();
  }

  /**
   * @return boolean
   * @author Lim, Seng Yew
   */
  public boolean showDialog()
  {
    SwingUtils.centerOnComponent(this, _myFrame);
    setVisible(true);
    return _ok;
  }

  /**
   * @return integer
   * @author Lim, Seng Yew
   * Return 0 if _extendRightWidthRadioButton is selected
   * Return 1 if _extendLeftLengthRadioButton is selected
   * Return 2 if _extendEqualWidthRadioButton is selected
   */
  public Integer getSelectedRadioButtonId()
  {
    if(_extendLeftLengthRadioButton.isSelected())
      return 1;
    if(_extendEqualLengthRadioButton.isSelected())
      return 2;
    return 0; // Return 0 by default for speed.
  }
}
