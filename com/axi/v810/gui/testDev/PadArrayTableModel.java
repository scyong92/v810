package com.axi.v810.gui.testDev;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.gui.*;

/**
 *
 * <p>Title: Pad Array Table Model</p>
 * <p>Description: Contains the data for display in the pad array table.</p>
 * <p>Company:  Agilent Technogies</p>
 * @author Laura Cormos
 * @version 1.0
 */
class PadArrayTableModel extends AbstractTableModel
{
  //Column label strings.  Retrieved from localization properties file.
  private static final String BOARD_NAME_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_BOARD_NAME_KEY");
  private static final String ROWS_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_BOARD_TABLE_ROWS_KEY");
  private static final String COLUMNS_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_BOARD_TABLE_COLUMNS_KEY");
  private static final String DX_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_DX_KEY");
  private static final String DY_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_DY_KEY");
  private static final String ROTATION_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_ROTATION_KEY");
  private static final String SHAPE_LABEL = StringLocalizer.keyToString("MMGUI_SETUPI_SHAPE_KEY");
  private static final String TYPE_LABEL = StringLocalizer.keyToString("MMGUI_SETUPI_LLP_TYPE_KEY");
  private static final String INVALID_INDEX_ERROR = StringLocalizer.keyToString("MMGUI_SETUP_INVALID_INDEX_ERROR_KEY");

  private String[] _columnLabels = {ROWS_LABEL,
                                   COLUMNS_LABEL,
                                   DX_LABEL,
                                   DY_LABEL,
                                   ROTATION_LABEL,
                                   SHAPE_LABEL,
                                   TYPE_LABEL };

  // default values for the pad array table to display when first invoked
  private int _rows = 0;
  private int _columns = 0;
  private double _dx = 0.0;
  private double _dy = 0.0;
  private double _rotation = 0.0;
  private String _shape = StringLocalizer.keyToString("CAD_RECT_SHAPE_KEY");
  private String _type = StringLocalizer.keyToString("CAD_SM_KEY");
  private MathUtilEnum _displayUnits;

  private AddPadArrayDialog _dialog = null;

  /**
   * @author Laura Cormos
   */
  public PadArrayTableModel(AddPadArrayDialog parentDialog)
  {
    Assert.expect(parentDialog != null);
    _dialog = parentDialog;
  }

  /**
   * @author Laura Cormos
   */
  void setDisplayUnits(MathUtilEnum displayUnits)
  {
    Assert.expect(displayUnits != null);
    _displayUnits = displayUnits;
  }

  /**
   * @author Laura Cormos
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * @author Laura Cormos
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * @author Laura Cormos
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * @author Laura Cormos
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    switch(columnIndex)
    {
      case PadArrayTable.ROWS_COLUMN_INDEX:
        value = new Integer(_rows);
        break;
      case PadArrayTable.COLUMNS_COLUMN_INDEX:
        value = new Integer(_columns);
        break;
      case PadArrayTable.DX_COLUMN_INDEX:
        value = new Double(_dx);
        break;
      case PadArrayTable.DY_COLUMN_INDEX:
        value = new Double(_dy);
        break;
      case PadArrayTable.ROTATION_COLUMN_INDEX:
        value = new Double(_rotation);
        break;
      case PadArrayTable.SHAPE_COLUMN_INDEX:
        value = _shape;
        break;
      case PadArrayTable.TYPE_COLUMN_INDEX:
        value = _type;
        break;
      default:
        Assert.expect(false, INVALID_INDEX_ERROR);
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * @author Laura Cormos
   */
  public int getRowCount()
  {
    return 1;
  }

  /**
   * @author Laura Cormos
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    Assert.expect(_dialog != null);
    double valueInNanos;
    int valueInNanosInt = -1;

    switch(columnIndex)
    {
      case PadArrayTable.ROWS_COLUMN_INDEX:
        _rows = 0;
        Assert.expect(object instanceof Number, "Wrong type of object: " + object);
        _rows = ((Number)object).intValue();
        break;
      case PadArrayTable.COLUMNS_COLUMN_INDEX:
        _columns = 0;
        Assert.expect(object instanceof Number, "Wrong type of object: " + object);
        _columns = ((Number)object).intValue();
        break;
      case PadArrayTable.DX_COLUMN_INDEX:
        _dx = 0.0;
        Assert.expect(object instanceof Number, "Wrong type of object: " + object);
        valueInNanos = MathUtil.convertUnits(((Number)object).doubleValue(), _displayUnits, MathUtilEnum.NANOMETERS);
        try
        {
          valueInNanosInt = MathUtil.convertDoubleToInt(valueInNanos);
          if (valueInNanosInt == 0)
          {
            MessageDialog.showErrorDialog(_dialog,
                                          StringLocalizer.keyToString(new LocalizedString(
                                              "CAD_TOO_SMALL_VALUE_FOR_PAD_X_DIMENSION_MSG_KEY", new Object[]
                                              {Double.toString(((Number)object).doubleValue())})),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            return;
          }
        }
        catch (ValueOutOfRangeException ex)
        {
          MessageDialog.showErrorDialog(_dialog, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          return;
        }
        _dx = ((Number)object).doubleValue();
        break;
      case PadArrayTable.DY_COLUMN_INDEX:
        _dy = 0.0;
        Assert.expect(object instanceof Number, "Wrong type of object: " + object);
        valueInNanos = MathUtil.convertUnits(((Number)object).doubleValue(), _displayUnits, MathUtilEnum.NANOMETERS);
        try
        {
          valueInNanosInt = MathUtil.convertDoubleToInt(valueInNanos);
          if (valueInNanosInt == 0)
          {
            MessageDialog.showErrorDialog(_dialog,
                                          StringLocalizer.keyToString(new LocalizedString(
                                              "CAD_TOO_SMALL_VALUE_FOR_PAD_Y_DIMENSION_MSG_KEY", new Object[]
                                              {Double.toString(((Number)object).doubleValue())})),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            return;
          }
        }
        catch (ValueOutOfRangeException ex)
        {
          MessageDialog.showErrorDialog(_dialog, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          return;
        }
        _dy = ((Number)object).doubleValue();
        break;
      case PadArrayTable.ROTATION_COLUMN_INDEX:
        _rotation = 0.0;
        Assert.expect(object instanceof String);
        String valueStr = (String)object;
        _rotation = new Double(valueStr);
        break;
      case PadArrayTable.SHAPE_COLUMN_INDEX:
        _shape = StringLocalizer.keyToString("CAD_RECT_SHAPE_KEY");
        Assert.expect(object instanceof String);
        _shape = (String)object;
        break;
      case PadArrayTable.TYPE_COLUMN_INDEX:
        _type = StringLocalizer.keyToString("CAD_SM_KEY");
        Assert.expect(object instanceof String);
        _type = (String)object;
        break;
      default:
        Assert.expect(false, INVALID_INDEX_ERROR);
    }

    if ((_rows * _columns) != 0 && (_dx * _dy) != 0)
      _dialog.setOKButtonState(true);
    else
      _dialog.setOKButtonState(false);
  }
}
