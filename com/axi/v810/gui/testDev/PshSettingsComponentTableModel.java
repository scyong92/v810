package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 *
 * @author siew-yeng.phang
 */
public class PshSettingsComponentTableModel extends DefaultSortTableModel
{
  //String labels retrieved from the localization properties file
  private static final String _COMPONENT_NAME_LABEL = StringLocalizer.keyToString("CAD_PSH_COMPONENT_TABLE_HEADER_KEY");
  private static final String _JOINT_TYPE_LABEL = StringLocalizer.keyToString("CAD_PSH_JOINT_TYPE_TABLE_HEADER_KEY");
  private static final String _SUBTYPE_NAME_LABEL = StringLocalizer.keyToString("CAD_PSH_SUBTYPE_TABLE_HEADER_KEY");

  private String[] _columnLabels = {_COMPONENT_NAME_LABEL,
                                    _JOINT_TYPE_LABEL,
                                    _SUBTYPE_NAME_LABEL};

  private List<ComponentType> _componentTypes = new ArrayList<ComponentType>();
  
  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;
  
  private List<ComponentType> _currentSelectedComponents = new ArrayList<ComponentType>();
  
  /**
   * Constructor.
   * @author Siew Yeng
   */
  public PshSettingsComponentTableModel()
  {
    //do nothing
  }
  
  /**
   * @author Siew Yeng
   */
  void clearData()
  {
    _componentTypes.clear();
    _currentSelectedComponents.clear();
    fireTableDataChanged();
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Siew Yeng
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @author Siew Yeng
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * Overridden method.  Gets the values from the project object.
   * @author Siew Yeng
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    ComponentType compType = _componentTypes.get(rowIndex);

    //get the appropriate data item that corresponds to the columnIndex
    switch(columnIndex)
    {
      case PshSettingsComponentTable._COMPONENT_NAME_INDEX:
        value = compType;
        break;
      case PshSettingsComponentTable._JOINT_TYPE_INDEX:
        value = TestDev.getComponentDisplayJointType(compType);
        break;
      case PshSettingsComponentTable._SUBTYPE_INDEX:
        value = TestDev.getComponentDisplaySubtype(compType);
        break;
      default:
      {
        Assert.expect(false);
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Siew Yeng
   */
  public int getRowCount()
  {
    if(_componentTypes != null)
      return _componentTypes.size();

    return 0;
  }

  /**
   * Overridden method.
   * @author Siew Yeng
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    
    return false;
  }

  /**
   * Initialize the table with data from the current joint type assignment config file
   * @author Siew Yeng
   */
  void setData(List<ComponentType> componentTypes)
  {
    Assert.expect(componentTypes != null);
    _componentTypes.clear();
    
    _componentTypes.addAll(componentTypes);
    
    sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableDataChanged();
  }

  /**
   * @author Siew Yeng
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
  }
  
  /**
   * @author Siew Yeng
   */
  public void sortColumn(int column, boolean ascending)
  {
    _currentSortColumn = column;
    _currentSortAscending = ascending;

    if (column == 0)
      Collections.sort(_componentTypes, new ComponentTypeComparator(_currentSortAscending, ComponentTypeComparatorEnum.REFERENCE_DESIGNATOR));
    else if(column == 1)
      Collections.sort(_componentTypes, new ComponentTypeComparator(_currentSortAscending, ComponentTypeComparatorEnum.JOINT_TYPE));
    else
      Collections.sort(_componentTypes, new ComponentTypeComparator(_currentSortAscending, ComponentTypeComparatorEnum.SUBTYPE));
  } 
  
  /**
   * @author Siew Yeng
   */
  public ComponentType getComponentTypeAt(int rowIndex)
  {
    Assert.expect(_componentTypes != null);
    
    return _componentTypes.get(rowIndex);
  }
  
  /**
   * @author Siew Yeng
   */
  public int getRowForComponentType(ComponentType compType)
  {
    Assert.expect(compType != null);
    Assert.expect(_componentTypes != null);
    
    return _componentTypes.indexOf(compType);
  }
  
  /**
   * @author Siew Yeng
   */
  public void setSelectedComponents(List<ComponentType> selectedComponents)
  {
    Assert.expect(selectedComponents != null);
    
    _currentSelectedComponents.clear();
    _currentSelectedComponents.addAll(selectedComponents);
  }
  
  /**
   * @author Siew Yeng
   */
  public List<ComponentType> getSelectedComponents()
  {
    Assert.expect(_currentSelectedComponents != null);
    
    return _currentSelectedComponents;
  }
  
  /**
   * @author Siew Yeng
   */
  public void clearSelectedComponent()
  {
    _currentSelectedComponents.clear();
  }
}
