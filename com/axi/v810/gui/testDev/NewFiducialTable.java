package com.axi.v810.gui.testDev;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.MeasurementUnits;

/**
 * This class displays the information for all the new components added in a single GUI session (between a project
 * open and a project close)
 * @author Laura Cormos
 */
class NewFiducialTable extends JSortTable
{
  private static final int _FIDUCIAL_NAME_INDEX = 0;
  static final int _FIDUCIAL_LOC_INDEX = 1;
  private static final int _FIDUCIAL_X_INDEX = 2;
  private static final int _FIDUCIAL_Y_INDEX = 3;
  private static final int _FIDUCIAL_DX_INDEX = 4;
  private static final int _FIDUCIAL_DY_INDEX = 5;
  private static final int _FIDUCIAL_SHAPE_INDEX = 6;


  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _FIDUCIAL_NAME_COLUMN_WIDTH_PERCENTAGE = .22;
  private static final double _FIDUCIAL_X_COLUMN_WIDTH_PERCENTAGE = .13;
  private static final double _FIDUCIAL_Y_COLUMN_WIDTH_PERCENTAGE = .13;
  private static final double _FIDUCIAL_DX_COLUMN_WIDTH_PERCENTAGE = .13;
  private static final double _FIDUCIAL_DY_COLUMN_WIDTH_PERCENTAGE = .13;
  private static final double _FIDUCIAL_SHAPE_COLUMN_WIDTH_PERCENTAGE = .13;
  private static final double _FIDUCIAL_LOC_COLUMN_WIDTH_PERCENTAGE = .13;

  private NumericRenderer _numericRenderer = new NumericRenderer(2, JLabel.RIGHT);

  private NumericEditor _xNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2, 0.0, Double.MAX_VALUE);
  private NumericEditor _yNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2, 0.0, Double.MAX_VALUE);
  private NumericEditor _dxNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2, 0.0, Double.MAX_VALUE);;
  private NumericEditor _dyNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2, 0.0, Double.MAX_VALUE);;
  private ShapeCellEditor _shapeCellEditor = new ShapeCellEditor();

  private boolean _modelSet = false;

  /**
   * Default constructor.
   * @author Carli Connally
   */
  public NewFiducialTable()
  {
    // do nothing
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Carli Connally
   * @author Laura Cormos
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_FIDUCIAL_NAME_INDEX).setPreferredWidth((int)(totalColumnWidth * _FIDUCIAL_NAME_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_FIDUCIAL_X_INDEX).setPreferredWidth((int)(totalColumnWidth * _FIDUCIAL_X_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_FIDUCIAL_Y_INDEX).setPreferredWidth((int)(totalColumnWidth * _FIDUCIAL_Y_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_FIDUCIAL_DX_INDEX).setPreferredWidth((int)(totalColumnWidth * _FIDUCIAL_DX_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_FIDUCIAL_DY_INDEX).setPreferredWidth((int)(totalColumnWidth * _FIDUCIAL_DY_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_FIDUCIAL_SHAPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _FIDUCIAL_SHAPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_FIDUCIAL_LOC_INDEX).setPreferredWidth((int)(totalColumnWidth * _FIDUCIAL_LOC_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * @author Laura Cormos
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };
    columnModel.getColumn(_FIDUCIAL_NAME_INDEX).setCellEditor(stringEditor);

    columnModel.getColumn(_FIDUCIAL_X_INDEX).setCellEditor(_xNumericEditor);
    columnModel.getColumn(_FIDUCIAL_X_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(_FIDUCIAL_Y_INDEX).setCellEditor(_yNumericEditor);
    columnModel.getColumn(_FIDUCIAL_Y_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(_FIDUCIAL_DX_INDEX).setCellEditor(_dxNumericEditor);
    columnModel.getColumn(_FIDUCIAL_DX_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(_FIDUCIAL_DY_INDEX).setCellEditor(_dyNumericEditor);
    columnModel.getColumn(_FIDUCIAL_DY_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(_FIDUCIAL_SHAPE_INDEX).setCellEditor(_shapeCellEditor);
  }

  /**
   * The decimal places to be displayed can change based on the measurement value that
   * is being displayed.  This method changes the renderers/editors to display the appropriate
   * number of decimal places.
   * @param enumSelectedUnits The units of measure selected by the user for this project
   * @author Carli Connally
   */
  public void setDecimalPlacesToDisplay(MathUtilEnum enumSelectedUnits)
  {
    Assert.expect(enumSelectedUnits != null);

    int decimalPlaces = MeasurementUnits.getMeasurementUnitDecimalPlaces(enumSelectedUnits);

    _xNumericEditor.setDecimalPlaces(decimalPlaces);
    _yNumericEditor.setDecimalPlaces(decimalPlaces);
    _dxNumericEditor.setDecimalPlaces(decimalPlaces);
    _dyNumericEditor.setDecimalPlaces(decimalPlaces);
    _numericRenderer.setDecimalPlacesToDisplay(decimalPlaces);
  }

  /**
   * @author Laura Cormos
   */
  public void setValueAt(Object aValue, int row, int column)
  {
    if (column == _FIDUCIAL_DX_INDEX || column == _FIDUCIAL_DY_INDEX)
    {
      Assert.expect(aValue instanceof Number);
      Number value = (Number)aValue;
      if (MathUtil.fuzzyEquals(value.doubleValue(), 0.0))
      {
        if (column == _FIDUCIAL_DX_INDEX)
          _dxNumericEditor.cancelCellEditing();
        if (column == _FIDUCIAL_DY_INDEX)
          _dyNumericEditor.cancelCellEditing();
        return;
      }
    }
    super.setValueAt(aValue, row, column);
  }

  /**
   * @author Laura Cormos
   */
  public void clearSelection()
  {
    TableModel model = getModel();
    // this function gets called before the table model is set, sometime during intialization.
    // At that time, the model will be a defaultTableModel so the assert is sure to fail
    if (_modelSet)
    {
      Assert.expect(model instanceof NewFiducialTableModel);
      ((NewFiducialTableModel)model).clearSelectedFiducials();
    }
    super.clearSelection();
  }

  /**
   * @author Laura Cormos
   */
  public void setModel(NewFiducialTableModel model)
  {
   Assert.expect(model != null);
   _modelSet = true;
   super.setModel(model);
  }
}
