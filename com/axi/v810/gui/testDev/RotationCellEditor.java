package com.axi.v810.gui.testDev;

import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * <p>Title: RotationCellEditor</p>
 * <p>Description: Table cell editor for board rotation values, 0 through 360</p>
 * @author Laura Cormos
 */

class RotationCellEditor extends DefaultCellEditor
{
  //Rotation strings
  private final String _ROTATION_0 = "0.0";
  private final String _ROTATION_90 = "90.0";
  private final String _ROTATION_180 = "180.0";
  private final String _ROTATION_270 = "270.0";
  private final Double _SMALL_DOUBLE = -10000.0;
  private final String _CUSTOM = StringLocalizer.keyToString("CAD_CUSTOM_KEY");
  private Set<String> _customValues;
  private JComboBox _comboBox = null;
  private java.awt.Component _parent;
  private boolean _ignoreActions = false;

  /**
   * @author Carli Connally
   * Constructor.
   * Initializes the drop down box with the standard orthogonal rotation values
   */
  public RotationCellEditor(java.awt.Component parent)
  {
    super(new JComboBox());
    Assert.expect(parent != null);

    _parent = parent;
    init();
  }

  /**
   * @author Laura Cormos
   * Constructor.
   * Initializes the drop down box with the standard orthogonal rotation value plus the "custom" choice
   */
  public RotationCellEditor(java.awt.Component parent, boolean withCustom)
  {
    super(new JComboBox());
    Assert.expect(parent != null);

    _parent = parent;
    init();
    if (withCustom)
      initCustom();
  }

  /**
   * @author Laura Cormos
   * Initializes the drop down box with the standard orthogonal rotation value plus the "custom" choice and any
   * passed in custom values already present in the CAD data
   */
  public RotationCellEditor(java.awt.Component parent, Set<Double> customValues)
  {
    super(new JComboBox());
    Assert.expect(parent != null);
    Assert.expect(customValues != null);

    _parent = parent;
    init();
    initCustom();
    setCustomValues(customValues);
  }

  /**
   * @author Laura Cormos
   */
  private void init()
  {
    Assert.expect(getComponent() instanceof JComboBox);
    _comboBox = (JComboBox)getComponent();

    _comboBox.addItem(_ROTATION_0);
    _comboBox.addItem(_ROTATION_90);
    _comboBox.addItem(_ROTATION_180);
    _comboBox.addItem(_ROTATION_270);

    _comboBox.setAlignmentX(JComboBox.RIGHT_ALIGNMENT);

    // init _customValues hash set in case it's needed
    _customValues = new HashSet<String>();
  }

  /**
   * @author Laura Cormos
   */
  private void initCustom()
  {
    Assert.expect(_comboBox != null);

    _comboBox.addItem(_CUSTOM);
    _comboBox.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        comboBox_actionPerformed();
      }
    });
  }

  /**
   * @author George Booth
   */
  void comboBox_actionPerformed()
  {
    if (_ignoreActions)
      return;

    if (((String)_comboBox.getSelectedItem()).equals(_CUSTOM))
    {
      boolean validInput = false;
      while (validInput == false)
      {
        String degrees = JOptionPane.showInputDialog(_parent,
                                                     StringLocalizer.keyToString("RCE_ENTER_ROT_DEGREES_KEY"),
                                                     StringLocalizer.keyToString("RCE_ROT_DEGREES_DIALOG_TITLE_KEY"),
                                                     JOptionPane.INFORMATION_MESSAGE);
        if (degrees != null)
        {
          Double degreesDouble;
          Double degrees0To359Double;
          try
          {
            degreesDouble = Double.valueOf(degrees);
            // make sure value is reasonable
//            System.out.println(Double.MIN_VALUE);
            degreesDouble = degreesDouble <= Double.MAX_VALUE ? degreesDouble : Double.MAX_VALUE;
            degreesDouble = degreesDouble >= -Double.MAX_VALUE ? degreesDouble : -Double.MAX_VALUE;
            degrees0To359Double = MathUtil.getDegreesWithin0To359(degreesDouble);
          }
          catch(NumberFormatException ex)
          {
            MessageDialog.showErrorDialog(_parent, StringLocalizer.keyToString(new LocalizedString("CAD_INVALID_INPUT_KEY",
                                                                                                new Object[]{degrees})),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            continue;
          }
          validInput = true;
          String degrees0To359String = String.valueOf(degrees0To359Double);
          if (degrees0To359Double != 0 && degrees0To359Double != 90 && degrees0To359Double != 180 && degrees0To359Double != 270
              && isDuplicate(degrees0To359String) == false)
          {
            _comboBox.addItem(degrees0To359String);
            _customValues.add(degrees0To359String);
          }
          _comboBox.setSelectedItem(degrees0To359String);
        }
        else // if user canceled the dialog we don't know what was previously selected so we'll just default to the first item in the list
        {
          cancelCellEditing();
          validInput = true; // want to break out of the loop if dialog was cancelled
        }
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  boolean isDuplicate(String value)
  {
    for (int i = 0; i < _comboBox.getItemCount(); i++)
      if (((String)_comboBox.getItemAt(i)).equalsIgnoreCase(value))
        return true;
    return false;
  }

  /**
   * @author Laura Cormos
   */
  public void setCustomValues(Set<Double> customValues)
  {
    Assert.expect(customValues != null);
//    _customValues = new HashSet<String>();

    for (Double value : customValues)
    {
      addCustomValue(value);
    }
  }

  /**
   * @author George Booth
   */
  public void addCustomValue(Double customValue)
  {
    Assert.expect(customValue != null);

    String customRotationString = customValue.toString();
    if (customRotationString.equals(_ROTATION_0) == false &&
        customRotationString.equals(_ROTATION_90) == false &&
        customRotationString.equals(_ROTATION_180) == false &&
        customRotationString.equals(_ROTATION_270) == false)
    {
      _customValues.add(customRotationString);
      _comboBox.addItem(customRotationString);
    }
  }

  /**
   * @author Laura Cormos
   */
  public void removeCustomValues()
  {
    if (_customValues == null)
      return;

    _ignoreActions = true;
    for (String value : _customValues)
    {
      _comboBox.removeItem(value);
    }
    _ignoreActions = false;
  }

  /**
   * @author Laura Cormos
   */
  public java.awt.Component getTableCellEditorComponent(JTable table, Object value, boolean isSelectable, int row, int column)
  {
    Assert.expect(table != null);
    Assert.expect(value != null);

    String stringValue;
    if (value instanceof Double)
      stringValue = value.toString();
    else
    {
      Assert.expect(value instanceof String);
      stringValue = (String)value;
    }
    _comboBox.setSelectedItem(stringValue);
    return _comboBox;
  }

  /**
   * @author Laura Cormos
   */
  void remove90And270Values()
  {
    _comboBox.removeItem(_ROTATION_270);
    _comboBox.removeItem(_ROTATION_90);
  }

  /**
   * @author Laura Cormos
   */
  void removeCustomValue()
  {
    _comboBox.removeItem(_CUSTOM);
  }
}
