package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;
/**
 *
 * @author Siew Yeng
 */
public class EditPshSettingsPanel extends JPanel implements Observer
{
  private JPanel _boardSideSettingPanel = new JPanel();
  private JPanel _boardSideChoicePanel = new JPanel();
  private JPanel _jointTypeSettingPanel = new JPanel();
  private JPanel _componentsPanel = new JPanel();
  private JPanel _pshComponentPanel = new JPanel();
  private JPanel _pshNeighbourComponentPanel = new JPanel();
  private JPanel _componentNorthPanel = new JPanel();
  private JPanel _componentCenterPanel = new JPanel();
  private JPanel _selectiveComponentCheckBoxAndInfoPanel = new JPanel();
  
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private BorderLayout _selectiveComponentCheckBoxAndInfoPanelBorderLayout = new BorderLayout();
  private BorderLayout _boardSideSettingBorderLayout = new BorderLayout();
  private GridLayout _boardSideChoiceGridLayout = new GridLayout();
  private BorderLayout _jointTypeSettingBorderLayout = new BorderLayout();
  private BorderLayout _componentPanelBorderLayout = new BorderLayout();
  private BorderLayout _pshComponentBorderLayout = new BorderLayout();
  private BorderLayout _pshNeighborComponentBorderLayout = new BorderLayout();
  private BorderLayout _componentNorthBorderLayout = new BorderLayout();
  private BorderLayout _componentCenterBorderLayout = new BorderLayout();
  
  private JCheckBox _selectiveBoardSideCheckBox = new JCheckBox(StringLocalizer.keyToString("CAD_PSH_SELECTIVE_BOARD_SIDE_CHECKBOX_KEY"));
  private JCheckBox _selectiveJointTypeCheckBox = new JCheckBox(StringLocalizer.keyToString("CAD_PSH_SELECTIVE_JOINT_TYPE_CHECKBOX_KEY"));
  private JCheckBox _selectiveComponentCheckBox = new JCheckBox(StringLocalizer.keyToString("CAD_PSH_SELECTIVE_COMPONENT_CHECKBOX_KEY"));
  
  private JLabel _pshComponentInfoLabel = new JLabel();
  
  private JRadioButton _boardSideBothRadioButton = new JRadioButton(StringLocalizer.keyToString("CAD_PSH_BOARD_SIDE_BOTH_RADIOBUTTON_KEY"));
  private JRadioButton _boardSideSameSideRadioButton = new JRadioButton(StringLocalizer.keyToString("CAD_PSH_BOARD_SIDE_SAME_SIDE_WITH_COMPONENT_RADIOBUTTON_KEY"));
  private JRadioButton _boardSideTopRadioButton = new JRadioButton(StringLocalizer.keyToString("CAD_PSH_BOARD_SIDE_TOP_RADIOBUTTON_KEY"));
  private JRadioButton _boardSideBottomRadioButton = new JRadioButton(StringLocalizer.keyToString("CAD_PSH_BOARD_SIDE_BOTTOM_RADIOBUTTON_KEY"));
  
  private ButtonGroup _boardSideButtonGroup = new ButtonGroup();
  
  private JScrollPane _jointTypeScrollPane = new JScrollPane();
  private JScrollPane _pshNeighborComponentsScrollPane = new JScrollPane();
  private JScrollPane _pshComponentsScrollPane = new JScrollPane();
  
  private JSplitPane _centerSplitPane = new JSplitPane();
  private JSplitPane _componentSplitPane = new JSplitPane();
  
  MainMenuGui _mainUI = null;
  private TestDev _testDev = null;  
  private boolean _ignoreActions = false; 

  private PshSettingsJointTypeTableModel _pshSettingsJointTypeTableModel;
  private PshSettingsJointTypeTable _pshSettingsJointTypeTable;
  private ListSelectionListener _pshJointTypeTableListSelectionListener;
  
  private PshSettingsComponentTable _pshComponentTable;
  private PshSettingsComponentTableModel _pshComponentTableModel;
  private ListSelectionListener _pshComponentTableListSelectionListener;
  
  private PshSettingsNeighborComponentTable _pshNeighborComponentTable;
  private PshSettingsNeighborComponentTableModel _pshNeighborComponentTableModel;
  private ListSelectionListener _pshNeighborComponentTableListSelectionListener;
  
  private Project _project = null;
  private PshSettings _pshSettings = null;

  private boolean _isPanelBasedAlignment = true;
  
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private transient SelectedRendererObservable _selectedRendererObservable;
  private transient static ProjectObservable _projectObservable;
  
  /*
   * @author Siew Yeng
   */
  public EditPshSettingsPanel(MainMenuGui frame)
  {
    try
    {
      _mainUI = frame;
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Siew Yeng
   */
  public EditPshSettingsPanel()
  {  
    try
    {
       jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Siew Yeng
   */
  private void jbInit() throws Exception
  {
    _ignoreActions = true;
    _mainPanelBorderLayout.setVgap(10);
    setLayout(_mainPanelBorderLayout);
    setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), 
                               StringLocalizer.keyToString("CAD_PSH_NEIGHBOUR_SETTINGS_TITLE_KEY")));
    
    _selectiveComponentCheckBoxAndInfoPanel.setLayout(_selectiveComponentCheckBoxAndInfoPanelBorderLayout);
    _boardSideSettingPanel.setLayout(_boardSideSettingBorderLayout);
    _boardSideSettingBorderLayout.setVgap(10);
    _boardSideChoicePanel.setLayout(_boardSideChoiceGridLayout);
    _boardSideChoiceGridLayout.setColumns(2);
    _boardSideChoiceGridLayout.setHgap(-1);
    _boardSideChoiceGridLayout.setRows(2);
    _boardSideChoiceGridLayout.setVgap(-1);
    
    _jointTypeSettingPanel.setLayout(_jointTypeSettingBorderLayout);
    _jointTypeSettingBorderLayout.setVgap(10);
    _componentsPanel.setLayout(_componentPanelBorderLayout);
    _componentPanelBorderLayout.setVgap(10);
    
    _pshNeighbourComponentPanel.setLayout(_pshNeighborComponentBorderLayout);
    _pshComponentPanel.setLayout(_pshComponentBorderLayout);
    _componentNorthPanel.setLayout(_componentNorthBorderLayout);
    _componentCenterPanel.setLayout(_componentCenterBorderLayout);
    
    _boardSideSettingPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
                                                                        BorderFactory.createEmptyBorder(3, 3, 3, 3))); 
    _boardSideChoicePanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), 
                                                                        BorderFactory.createEmptyBorder(3, 3, 3, 3))); 
    _jointTypeSettingPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), 
                                                                        BorderFactory.createEmptyBorder(3, 3, 3, 3))); 
    _componentsPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), 
                                                                          BorderFactory.createEmptyBorder(3, 3, 3, 3))); 
    _pshComponentPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white, new Color(165, 163, 151)), 
                                                  StringLocalizer.keyToString("CAD_PSH_COMPONENTS_TABLE_TITLE_KEY"))); 
    _pshNeighbourComponentPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, Color.white, new Color(165, 163, 151)), 
                                                          StringLocalizer.keyToString("CAD_PSH_NEIGHBOUR_COMPONENTS_TABLE_TITLE_KEY")));      
          
    _pshComponentInfoLabel.setFont(FontUtil.getBoldFont(_pshComponentInfoLabel.getFont(),Localization.getLocale()));
    
    _selectiveBoardSideCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectiveBoardSideCheckBox_actionPerformed(e);
      }
    });
    
    _boardSideBothRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        boardSideReferenceRadioButton_actionPerformed(e);
      }
    });
    
    _boardSideSameSideRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        boardSideReferenceRadioButton_actionPerformed(e);
      }
    });
    
    _boardSideTopRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        boardSideReferenceRadioButton_actionPerformed(e);
      }
    });
    
    _boardSideBottomRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        boardSideReferenceRadioButton_actionPerformed(e);
      }
    });
    
    _selectiveJointTypeCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectiveJointTypeCheckBox_actionPerformed(e);
      }
    });
    
    _selectiveComponentCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectiveComponentCheckBox_actionPerformed(e);
      }
    });
    
    _pshSettingsJointTypeTableModel = new PshSettingsJointTypeTableModel(this);
    _pshSettingsJointTypeTable = new PshSettingsJointTypeTable()
    {
      /**
       * Overriding this method allows us to make sure the selected rows
       * are still the proper selected rows after a sort.
       * @author Siew Yeng
       */
      public void mouseReleased(MouseEvent event)
      {
        _ignoreActions = true;
        int selectedRows[] = _pshSettingsJointTypeTable.getSelectedRows();
        java.util.List<JointTypeEnum> selectedData = new ArrayList<JointTypeEnum>();
        for (int row = 0; row < selectedRows.length; row++)
        {
          selectedData.add(_pshSettingsJointTypeTableModel.getJointTypeAt(selectedRows[row]));
        }
        super.mouseReleased(event);
        // now, reselect everything
        _pshSettingsJointTypeTable.clearSelection();
        
        for (JointTypeEnum jointType : selectedData)
        {
          int row = _pshSettingsJointTypeTableModel.getRowForJointType(jointType);
          _pshSettingsJointTypeTable.addRowSelectionInterval(row, row);
        }
        
        // after selection, scroll to make sure the row is visible.
        SwingUtils.scrollTableToShowSelection(_pshSettingsJointTypeTable);
        
        _ignoreActions = false;
      }
    };
    
    _pshJointTypeTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        pshSettingsJointTypeTable_valueChanged(e);
      }
    };
    
    _pshSettingsJointTypeTable.setModel(_pshSettingsJointTypeTableModel);
    _pshSettingsJointTypeTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _pshSettingsJointTypeTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    _jointTypeScrollPane.getViewport().add(_pshSettingsJointTypeTable);
    _jointTypeScrollPane.setMinimumSize(new Dimension(5,5));
    _jointTypeScrollPane.setPreferredSize(new Dimension(150, 160));
    
    final ListSelectionModel ruleSetRowSelectionModel = _pshSettingsJointTypeTable.getSelectionModel();
    ruleSetRowSelectionModel.addListSelectionListener(_pshJointTypeTableListSelectionListener);
    

    SwingUtils.scrollTableToShowSelection(_pshSettingsJointTypeTable);
    
    _pshComponentTableModel = new PshSettingsComponentTableModel();    
    _pshComponentTable = new PshSettingsComponentTable(_pshComponentTableModel)
    {
      /**
       * Overriding this method allows us to make sure the selected rows
       * are still the proper selected rows after a sort.
       * @author Siew Yeng
       */
      public void mouseReleased(MouseEvent event)
      {
        _ignoreActions = true;
        int selectedRows[] = _pshComponentTable.getSelectedRows();
        java.util.List<ComponentType> selectedData = new ArrayList<ComponentType>();
        for (int row = 0; row < selectedRows.length; row++)
        {
          selectedData.add(_pshComponentTableModel.getComponentTypeAt(selectedRows[row]));
        }
        super.mouseReleased(event);
        // now, reselect everything
        _pshComponentTable.clearSelection();
        
        for (ComponentType compType : selectedData)
        {
          int row = _pshComponentTableModel.getRowForComponentType(compType);
          _pshComponentTable.addRowSelectionInterval(row, row);
        }
        // after selection, scroll to make sure the row is visible.
        SwingUtils.scrollTableToShowSelection(_pshComponentTable);
        
        _ignoreActions = false; 
      }
    };
    
    _pshComponentTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        pshSettingsComponentTable_valueChanged(e);
      }
    };
    
    FontMetrics pshComponentTableFontMetrics = getFontMetrics(getFont());
    int pshComponentTablePreferredHeight = pshComponentTableFontMetrics.getHeight();// + 3;
    _pshComponentTable.setRowHeight(pshComponentTablePreferredHeight);
    _pshComponentTable.setPreferredColumnWidths();
    _pshComponentTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    
    ListSelectionModel pshComponentSM = _pshComponentTable.getSelectionModel();
    pshComponentSM.addListSelectionListener(_pshComponentTableListSelectionListener);
    
    _pshComponentsScrollPane.getViewport().add(_pshComponentTable);
    _pshComponentsScrollPane.setMinimumSize(new Dimension(5,5));
    _pshComponentsScrollPane.setPreferredSize(new Dimension(150, 180));
    
    _pshNeighborComponentTableModel = new PshSettingsNeighborComponentTableModel();    
    _pshNeighborComponentTable = new PshSettingsNeighborComponentTable(_pshNeighborComponentTableModel)
    {
      /**
       * Overriding this method allows us to make sure the selected rows
       * are still the proper selected rows after a sort.
       * @author Siew Yeng
       */
      public void mouseReleased(MouseEvent event)
      {
        _ignoreActions = true;
        int selectedRows[] = _pshNeighborComponentTable.getSelectedRows();
        java.util.List<Pair<ComponentType,ComponentType>> selectedData = new ArrayList<Pair<ComponentType,ComponentType>>();
        for (int row = 0; row < selectedRows.length; row++)
        {
          selectedData.add(_pshNeighborComponentTableModel.getNeighbourAndPshComponentTypePairAt(selectedRows[row]));
        }
        super.mouseReleased(event);
        // now, reselect everything
        _pshNeighborComponentTable.clearSelection();
        
        for (Pair<ComponentType,ComponentType> data : selectedData)
        {
          int row = _pshNeighborComponentTableModel.getRowFoNeighbourAndPshComponentTypePair(data);
          _pshNeighborComponentTable.addRowSelectionInterval(row, row);
        }
        
        // after selection, scroll to make sure the row is visible.
        SwingUtils.scrollTableToShowSelection(_pshNeighborComponentTable);
        
        _ignoreActions = false;
      }
    };

    _pshNeighborComponentTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        pshSettingsNeighborComponentTable_valueChanged(e);
      }
    };
    
    FontMetrics pshNeighbourTableFontMetrics = getFontMetrics(getFont());
    int pshNeighbourTablePreferredHeight = pshNeighbourTableFontMetrics.getHeight();// + 3;
    _pshNeighborComponentTable.setRowHeight(pshNeighbourTablePreferredHeight);
    _pshNeighborComponentTable.setPreferredColumnWidths();
    _pshNeighborComponentTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _pshNeighborComponentTable.addMouseListener(new MouseAdapter() 
    {
      public void mouseReleased(MouseEvent e) 
      {
        if (e.isPopupTrigger())
        {
          // get the table cell that the mouse was right-clicked in
          Point p = e.getPoint();
          int row = _pshNeighborComponentTable.rowAtPoint(p);
          int col = _pshNeighborComponentTable.columnAtPoint(p);

          // if this cell is not currently selected, exit.  It's confusing to edit a cell that not selected.
          // we could "select" this cell too, but for now, we'll just not.
          if (_pshNeighborComponentTable.isCellSelected(row, col) == false)
            return;

          JPopupMenu popup = new JPopupMenu();
          popup = createDeleteComponentPopUpMenu();
          popup.show(_pshNeighborComponentTable, e.getX(), e.getY());
        }
      }
    });

    _pshNeighborComponentsScrollPane.getViewport().add(_pshNeighborComponentTable);
    _pshNeighborComponentsScrollPane.setMinimumSize(new Dimension(5,5));
    _pshNeighborComponentsScrollPane.setPreferredSize(new Dimension(150, 100));
    
    ListSelectionModel rowSM3 = _pshNeighborComponentTable.getSelectionModel();
    rowSM3.addListSelectionListener(_pshNeighborComponentTableListSelectionListener);  // add the listener for a table row selection

    validate();
    repaint();
  
    _ignoreActions = false; 
    
    _boardSideChoicePanel.add(_boardSideBothRadioButton);
    _boardSideChoicePanel.add(_boardSideTopRadioButton);
    _boardSideChoicePanel.add(_boardSideSameSideRadioButton);
    _boardSideChoicePanel.add(_boardSideBottomRadioButton);
    
    _boardSideButtonGroup.add(_boardSideBothRadioButton);
    _boardSideButtonGroup.add(_boardSideTopRadioButton);
    _boardSideButtonGroup.add(_boardSideSameSideRadioButton);
    _boardSideButtonGroup.add(_boardSideBottomRadioButton);
    
    _boardSideSettingPanel.add(_selectiveBoardSideCheckBox, BorderLayout.NORTH); 
    _boardSideSettingPanel.add(_boardSideChoicePanel, BorderLayout.CENTER);
    
    _jointTypeSettingPanel.add(_selectiveJointTypeCheckBox, BorderLayout.NORTH); 
    _jointTypeSettingPanel.add(_jointTypeScrollPane, BorderLayout.CENTER);
    
    _pshNeighbourComponentPanel.add(_pshNeighborComponentsScrollPane, BorderLayout.CENTER);
    
    _pshComponentPanel.add(_pshComponentsScrollPane, BorderLayout.CENTER);
    
    _componentSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
    _componentSplitPane.add(_pshComponentPanel, JSplitPane.TOP);
    _componentSplitPane.add(_pshNeighbourComponentPanel, JSplitPane.BOTTOM);
    
    _selectiveComponentCheckBoxAndInfoPanel.add(_selectiveComponentCheckBox, BorderLayout.WEST);
    _selectiveComponentCheckBoxAndInfoPanel.add(_pshComponentInfoLabel, BorderLayout.EAST);
    
    _componentsPanel.add(_selectiveComponentCheckBoxAndInfoPanel, BorderLayout.NORTH);
    _componentsPanel.add(_componentSplitPane, BorderLayout.CENTER);

    _componentCenterPanel.add(_componentNorthPanel, BorderLayout.NORTH);
    
    _centerSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
    _centerSplitPane.add(_jointTypeSettingPanel, JSplitPane.TOP);
    _centerSplitPane.add(_componentsPanel, JSplitPane.BOTTOM);
   
    add(_boardSideSettingPanel, BorderLayout.NORTH);
    add(_centerSplitPane, BorderLayout.CENTER);
  }

  /**
   * @author siew Yeng
   */
  private void selectiveBoardSideCheckBox_actionPerformed(ActionEvent e)
  {
    try
    {
      _commandManager.execute(new PshSettingsSetEnableSelectiveBoardSideCommand(_project.getPanel().getPshSettings(), _selectiveBoardSideCheckBox.isSelected()));
    }
    catch (XrayTesterException ex)
    {
       MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                     ex.getLocalizedMessage(),
                                     StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                     true);
     }
    
    updateSelectiveBoardSideSettings(); 
  }
  
  /**
   * @author Siew Yeng
   */
  private void boardSideReferenceRadioButton_actionPerformed(ActionEvent e)
  {
    PshSettingsBoardSideEnum pshBoardSideEnum = _pshSettings.getPshSettingsBoardSideEnum();
    
    if(_boardSideBothRadioButton.isSelected())
      pshBoardSideEnum = PshSettingsBoardSideEnum.BOTH;
    
    if(_boardSideBottomRadioButton.isSelected())
      pshBoardSideEnum = PshSettingsBoardSideEnum.BOTTOM;
    
    if(_boardSideTopRadioButton.isSelected())
      pshBoardSideEnum = PshSettingsBoardSideEnum.TOP;
    
    if(_boardSideSameSideRadioButton.isSelected())
      pshBoardSideEnum = PshSettingsBoardSideEnum.SAME_SIDE_WITH_COMPONENT;
    
    try
    {
      _commandManager.trackState(getCurrentUndoState());
      _commandManager.execute(new PshSettingsSetBoardSideCommand(_pshSettings, pshBoardSideEnum));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
  /**
   * @author Siew Yeng
   */
  private void selectiveJointTypeCheckBox_actionPerformed(ActionEvent e)
  {
    try
    {
      _commandManager.trackState(getCurrentUndoState());
      _commandManager.execute(new PshSettingsSetEnableSelectiveJointTypeCommand(_pshSettings, _selectiveJointTypeCheckBox.isSelected()));
    }
    catch (XrayTesterException ex)
    {
       MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                     ex.getLocalizedMessage(),
                                     StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                     true);
     }
    
    updateSelectiveJointTypeSettings();
  }
  
  /**
   * @author Siew Yeng
   */
  private void selectiveComponentCheckBox_actionPerformed(ActionEvent e)
  {
    try
    {
      _commandManager.trackState(getCurrentUndoState());
      _commandManager.execute(new PshSettingsSetEnableSelectiveComponentCommand(_pshSettings, _selectiveComponentCheckBox.isSelected()));
    }
    catch (XrayTesterException ex)
    {
       MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                     ex.getLocalizedMessage(),
                                     StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                     true);
     }
    
    updateSelectiveComponentSettings();
  }
  
  /**
   * @author Siew Yeng
   */
  void populateWithProjectData()
  {
    _project = Project.getCurrentlyLoadedProject();
    _isPanelBasedAlignment = _project.getPanel().getPanelSettings().isPanelBasedAlignment();//Siew Yeng - XCR-3844
    _pshSettings =  _project.getPanel().getPshSettings();
    
    populateJointTypeTable();
    populatePshComponentTable();
    
    updateSelectiveBoardSideSettings();
    updateSelectiveJointTypeSettings();  
    updateSelectiveComponentSettings(); 
  }
  
  /*
   * @author Siew Yeng
   */
  void highlightPshComponent()
  {    
    java.util.List<ComponentType> selectedComponents = (java.util.List)_pshComponentTableModel.getSelectedComponents();
      
    if(selectedComponents.isEmpty() == false)
    {
      _guiObservable.stateChanged(selectedComponents, SelectionEventEnum.PSH_COMPONENT_SELECTION);
    }
  }
  
  /*
   * @author Siew Yeng
   */
  void highlightPshNeighborComponent()
  {    
    java.util.List<ComponentType> selectedComponents = new ArrayList();
    for(int row : _pshNeighborComponentTable.getSelectedRows())
    {
      selectedComponents.add(_pshNeighborComponentTableModel.getComponentTypeAt(row));
    }
    
    if(selectedComponents.isEmpty() == false)
    {
      _guiObservable.stateChanged(selectedComponents, SelectionEventEnum.PSH_NEIGHBOR_COMPONENT_SELECTION);
    }
  }
  
  /*
   * @author Siew Yeng
   */
  public void start()
  {
    _testDev = _mainUI.getTestDev();
    populateWithProjectData();
    
    _pshComponentInfoLabel.setText(StringLocalizer.keyToString(new LocalizedString("CAD_PSH_TOTAL_COMPONENTS_LABEL_KEY",
                                                               new Object[]{_pshSettings.getPshComponentTypes().size()})));

    _selectedRendererObservable = getGraphicsEngine().getSelectedRendererObservable();
    _projectObservable = ProjectObservable.getInstance();
    
    _selectedRendererObservable.addObserver(this); 
    _projectObservable.addObserver(this);

    _guiObservable.addObserver(this);
    _commandManager.addObserver(this);
  }
  
  /**
   * Called from EditCad panel when tab is switched away from VerifyCAD
   * @author George Booth
   */
  boolean isReadyToFinish()
  {
    if (_pshSettingsJointTypeTable.isEditing())
      _pshSettingsJointTypeTable.getCellEditor().stopCellEditing();
    return true;
  }
  
  /*
   * @author Siew Yeng
   */
  public void finish()
  {
    if (_selectedRendererObservable != null)
    {
      _selectedRendererObservable.deleteObserver(this);
      _selectedRendererObservable = null;
    }
    
    if(_commandManager != null)
    {
      _commandManager.deleteObserver(this);
    }
    
    if(_projectObservable != null)
    {
      _projectObservable.deleteObserver(this);
    }
    
    if(getGraphicsEngine() != null)
    {
      getGraphicsEngine().clearSelectedRenderers();
      getGraphicsEngine().clearHighlightedRenderers();
    }
    
    _pshComponentTable.clearSelection();
    _pshNeighborComponentTable.clearSelection();
    
    if (_pshSettingsJointTypeTable.isEditing())
      _pshSettingsJointTypeTable.cancelCellEditing();
    
    _guiObservable.deleteObserver(this);
  }
  
  /*
   * @author Siew Yeng
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof SelectedRendererObservable)
        {
          handleSelectionEvent(object);     
        }
        else if (observable instanceof com.axi.v810.gui.undo.CommandManager)
        {
          handleUndoState(object);     
        }
        else if(observable instanceof ProjectObservable)
        {
          handleProjectObservable(object);
        }
      }
    });
  }
  
  /*
   * @author Siew Yeng
   */
  private void pshSettingsJointTypeTable_valueChanged(ListSelectionEvent e)
  {
    ListSelectionModel listSelectionModel = (ListSelectionModel)e.getSource();
    if (listSelectionModel.isSelectionEmpty() || listSelectionModel.getValueIsAdjusting())
    {
      // do nothing
    }
    else
    {
      int selectedRows[] = _pshSettingsJointTypeTable.getSelectedRows();
      java.util.List<JointTypeEnum> selectedData = new ArrayList<JointTypeEnum>();
      for (int row = 0; row < selectedRows.length; row++)
      {
        selectedData.add(_pshSettingsJointTypeTableModel.getJointTypeAt(selectedRows[row]));
      }
      _pshSettingsJointTypeTableModel.setSelectedJointTypeRowIndex(selectedData);  
    }
  }
  
  
  /*
   * @author Siew Yeng
   */
  public void pshSettingsComponentTable_valueChanged(ListSelectionEvent e)
  {
    if (_ignoreActions)
    {
      return;
    }
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    // turn off event processing until graphics had a chance to update
    _pshComponentTable.processSelections(false);

    int selectedRows[] = _pshComponentTable.getSelectedRows();
    java.util.List<ComponentType> selectedData = new ArrayList<ComponentType>();
    for (int row = 0; row < selectedRows.length; row++)
    {
      selectedData.add(_pshComponentTableModel.getComponentTypeAt(selectedRows[row]));
    }
    _pshComponentTableModel.setSelectedComponents(selectedData);
    
    populatePshNeighborComponentTable(selectedData);
    
    // set CAD graphics crosshairs on board and component
    highlightPshComponent();

    // wait for graphics
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        _pshComponentTable.processSelections(true);
      }
    });
  }
  
  /*
   * @author Siew Yeng
   */
  public void pshSettingsNeighborComponentTable_valueChanged(ListSelectionEvent e)
  {
    if (_ignoreActions)
    {
        return;
    }
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    // turn off event processing until graphics had a chance to update
    _pshNeighborComponentTable.processSelections(false);
    
    // set CAD graphics crosshairs on board and component
    highlightPshNeighborComponent();

    // wait for graphics
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        _pshNeighborComponentTable.processSelections(true);
      }
    });
  }
  
  /**
   * @author Siew Yeng
   */
  private void setEnabledBoardSideOptions(boolean enable)
  {
    _boardSideBothRadioButton.setEnabled(enable);
    _boardSideSameSideRadioButton.setEnabled(enable);
    _boardSideTopRadioButton.setEnabled(enable);
    _boardSideBottomRadioButton.setEnabled(enable);
  }
  
  /**
   * @author Siew Yeng
   */
  private JPopupMenu createDeleteComponentPopUpMenu()
  {
    JPopupMenu popup = new JPopupMenu();
    JMenuItem deleteComponentMenuItem = new JMenuItem(StringLocalizer.keyToString("CAD_PSH_DELETE_COMPONENT_MENU_KEY"));
    
    deleteComponentMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteComponentButton_actionPerformed();
      }
    });
    
    popup.add(deleteComponentMenuItem);
    return popup;
  }
  
  /**
   * @author Siew Yeng
   */
  private void deleteComponentButton_actionPerformed()
  {
    if (_pshNeighborComponentTable.getSelectedRows().length > 0)
    {
      int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                                                 StringLocalizer.keyToString("PSP_DELETE_COMPONENT_KEY"),
                                                 StringLocalizer.keyToString("PSP_DELETE_COMPONENT_BUTTON_KEY"),
                                                 JOptionPane.YES_NO_OPTION,
                                                 JOptionPane.WARNING_MESSAGE);
      if (answer == JOptionPane.YES_OPTION)
      {
        final BusyCancelDialog busyDialog = new BusyCancelDialog(_mainUI,
                                                                 StringLocalizer.keyToString("PSP_DELETE_COMPONENT_IN_PROGRESS_KEY"),
                                                                 StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                                 true);
        busyDialog.pack();
        SwingUtils.centerOnComponent(busyDialog, _mainUI);
        SwingWorkerThread.getInstance().invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              int[] selectedRows = _pshNeighborComponentTable.getSelectedRows();
              _commandManager.trackState(getCurrentUndoState());
              _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PSH_REMOVE_NEIGHBOUR_COMMAND_KEY"));
              
              for(int row : selectedRows)
              {
                Pair<ComponentType, ComponentType> data = _pshNeighborComponentTableModel.getNeighbourAndPshComponentTypePairAt(row);
                _commandManager.execute(new PshSettingsRemoveNeighborCommand(_pshSettings, data.getSecond(), data.getFirst()));
              }
              
              _pshNeighborComponentTableModel.removeNeighbourComponentTypes(_pshNeighborComponentTable.getSelectedRows());
              _pshNeighborComponentTableModel.fireTableDataChanged();              
            }
            catch (XrayTesterException ex) 
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
            }
            finally
            {
              _commandManager.endCommandBlock();
              getGraphicsEngine().clearSelectedRenderers();
              busyDialog.dispose();
            }
          }
        });
        busyDialog.setVisible(true);
      }
    }
  }
  
  /**
   * @author Siew Yeng
   */
  public GraphicsEngine getGraphicsEngine()
  {
    if (_testDev == null)
      _testDev = _mainUI.getTestDev();
    
    if(_isPanelBasedAlignment == false) //Siew Yeng - XCR-3844
      return _testDev.getBoardGraphicsEngineSetup().getGraphicsEngine();
    
    return _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine();
  }
  
  /**
   * @author Siew Yeng
   */
  void handleSelectionEvent(final Object object)
  {
    if(getGraphicsEngine().isMouseRightClickMode())
      return;
    
    //Siew Yeng - XCR-3835 - fix crash when click component at CAD and no component using PSH
    if(_pshSettings.isEnabledSelectiveComponent() == false || 
       _pshComponentTableModel.getSelectedComponents().size() > 1 ||
       _pshComponentTableModel.getSelectedComponents().isEmpty())
      return;
    
    // We receive from the update, a collection of selected objects.  We'll only pay attention to
    // the ones this screen is interested in
    Collection<com.axi.guiUtil.Renderer> selections = (Collection< com.axi.guiUtil.Renderer>) object;

    // sort the fiducial so they're displayed by name.  The pads can't be sorted by their name since
    // I want the component name to be part of the sorting
    _pshNeighborComponentTable.getSelectionModel().setValueIsAdjusting(true);

    java.util.List<ComponentType> selectedComponents = new ArrayList<>();
    java.util.List<String> listOfInvalidNeighbourComponentNames = new ArrayList<>();

    for (com.axi.guiUtil.Renderer renderer : selections)
    {
      if (renderer instanceof ComponentRenderer)
      {
        com.axi.v810.business.panelDesc.Component component = ((ComponentRenderer) renderer).getComponent();
        if (isComponentUsableAsPshReference(component))
        {
          if(selectedComponents.contains(component.getComponentType()) == false)
            selectedComponents.add(component.getComponentType());
        }
        else
        {
          listOfInvalidNeighbourComponentNames.add(component.toString());
        }
      }
    } 
    if (listOfInvalidNeighbourComponentNames.size() > 0)
    {
      String invalidComponentNames = "";
      for (int i = 0; i < listOfInvalidNeighbourComponentNames.size(); ++i)
      {
        invalidComponentNames += listOfInvalidNeighbourComponentNames.get(i);
        if (i < (listOfInvalidNeighbourComponentNames.size() - 1))
        {
          invalidComponentNames += ", ";
        }
      }
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                    StringLocalizer.keyToString(new LocalizedString("CAD_PSH_UNTESTABLE_COMPONENTS_OR_PSH_COMPONENTS_NOT_ADDED_MESSAGE_KEY", 
                                                new Object[]{invalidComponentNames})),
                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                    JOptionPane.ERROR_MESSAGE);
    }

    if(selectedComponents.isEmpty() == false)
    {
      _commandManager.trackState(getCurrentUndoState());
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PSH_ADD_NEIGHBOUR_COMMAND_KEY"));
      try
      {
        ComponentType pshComponent = _pshComponentTableModel.getSelectedComponents().get(0);
        for(ComponentType compType : selectedComponents)
        {
          if(_pshSettings.isNeighborOfPshComponent(compType, pshComponent))
            continue;
          
          _commandManager.execute(new PshSettingsAddNeighborCommand(_pshSettings, pshComponent, compType));
          _pshNeighborComponentTableModel.addNeighbourComponentType(compType, pshComponent);
        }

        _pshNeighborComponentTableModel.fireTableDataChanged();
        _pshNeighborComponentTableModel.sortColumn(0, true);

        _ignoreActions = true;
        for(ComponentType compType : selectedComponents)
        {
          int row = _pshNeighborComponentTableModel.getRowForNeighbour(compType);
          _pshNeighborComponentTable.addRowSelectionInterval(row, row);
        }
        _ignoreActions = false;
      }
      catch (XrayTesterException ex) 
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                 ex.getLocalizedMessage(),
                                 StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                 true);
      }
      finally
      {
        _commandManager.endCommandBlock();
      }
    }
    else
    {
      getGraphicsEngine().clearSelectedRenderers();
    }
  }
  
  /**
   * All components that will be added as neighbor must pass through this checking.
   * @author Siew Yeng
   */
  public boolean isComponentUsableAsPshReference(com.axi.v810.business.panelDesc.Component component)
  {
    if (component.getComponentTypeSettings().areAllPadTypesNotTestable() == false && 
        component.getComponentTypeSettings().isLoaded() &&
        _pshSettings.getPshComponentTypes().contains(component.getComponentType()) == false)
      return true;
    else
      return false;
  }
  
  /*
   * @author Siew Yeng
   */
  protected UndoState getCurrentUndoState()
  {
    UndoState undoState = new UndoState();
    undoState.setAdjustCadTabbedPaneIndex(4);

    return undoState;
  }
  
  /**
   * @author Siew Yeng
   */
  private void updateSelectiveBoardSideSettings()
  {
    _selectiveBoardSideCheckBox.setSelected(_pshSettings.isEnableSelectiveBoardSide());
    
    PshSettingsBoardSideEnum sideBoardReferenceEnum = _pshSettings.getPshSettingsBoardSideEnum();
      
    if(sideBoardReferenceEnum.equals(PshSettingsBoardSideEnum.BOTH))
      _boardSideBothRadioButton.setSelected(true);
    else if(sideBoardReferenceEnum.equals(PshSettingsBoardSideEnum.BOTTOM))
      _boardSideBottomRadioButton.setSelected(true);
    else if(sideBoardReferenceEnum.equals(PshSettingsBoardSideEnum.TOP))
      _boardSideTopRadioButton.setSelected(true);
    else if(sideBoardReferenceEnum.equals(PshSettingsBoardSideEnum.SAME_SIDE_WITH_COMPONENT))
      _boardSideSameSideRadioButton.setSelected(true);
    
    if(_pshSettings.isEnableSelectiveBoardSide())
      setEnabledBoardSideOptions(true);
    else
      setEnabledBoardSideOptions(false);
  }
  
  /**
   * @author Siew Yeng
   */
  private void updateSelectiveJointTypeSettings()
  {    
    _selectiveJointTypeCheckBox.setSelected(_pshSettings.isEnableSelectiveJointType());
    
    if(_selectiveJointTypeCheckBox.isSelected())
    {
      _jointTypeScrollPane.setEnabled(true);
      _pshSettingsJointTypeTable.setEnabled(true);
    }
    else
    {
      _jointTypeScrollPane.setEnabled(false);
      _pshSettingsJointTypeTable.setEnabled(false);
    }
  }
  
  /**
   * @author Siew Yeng
   */
  private void updateSelectiveComponentSettings()
  {        
    _selectiveComponentCheckBox.setSelected(_pshSettings.isEnabledSelectiveComponent());

    if(_selectiveComponentCheckBox.isSelected())
    {
      _pshComponentsScrollPane.setEnabled(true);
      _pshNeighborComponentsScrollPane.setEnabled(true);
      
      _pshComponentTable.setEnabled(true);
      _pshNeighborComponentTable.setEnabled(true);
    }
    else
    {
      _pshComponentsScrollPane.setEnabled(false);
      _pshNeighborComponentsScrollPane.setEnabled(false);
      
      _pshComponentTable.setEnabled(false);
      _pshNeighborComponentTable.setEnabled(false);
    }
  }
  
  /**
   * @author Siew Yeng
   */
  private void populatePshComponentTable()
  {
    _ignoreActions = true;
    
    _pshComponentTableModel.setData(_pshSettings.getPshComponentTypes());
    
    _ignoreActions = false;
  }
  
  /**
   * @author Siew Yeng
   */
  private void populateJointTypeTable()
  {    
    _pshSettingsJointTypeTableModel.setData(_pshSettings);
    _pshSettingsJointTypeTable.setEditorsAndRenderers();
  }
  
  /**
   * @author Siew Yeng
   */
  private void populatePshNeighborComponentTable(java.util.List<ComponentType> selectedPshComponent)
  {
    _ignoreActions = true;
    
    _pshNeighborComponentTableModel.setData(_pshSettings, selectedPshComponent);
    
    _ignoreActions = false;
  }
  
  /**
   * @author Siew Yeng
   */
  public void unpopulate()
  {
    _pshComponentTableModel.clearData();
    _pshNeighborComponentTableModel.clearData();
    _pshSettingsJointTypeTableModel.clearData();
  }
  
  /**
   * @author Siew Yeng
   */
  private void handleUndoState(Object object)
  {
    UndoState undoState = (UndoState) object;
    
    updateSelectiveBoardSideSettings();
    updateSelectiveJointTypeSettings();
    updateSelectiveComponentSettings();
    populatePshNeighborComponentTable(_pshComponentTableModel.getSelectedComponents());
    
    _pshSettingsJointTypeTableModel.fireTableDataChanged();
    _pshNeighborComponentTableModel.fireTableDataChanged();
    
    getGraphicsEngine().clearSelectedRenderers();
    highlightPshNeighborComponent();
  }
  
  /**
   * @author Siew Yeng
   */
  public void handleProjectObservable(Object object)
  {
    if (object instanceof ProjectChangeEvent)
    {
      ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)object;
      ProjectChangeEventEnum projectChangeEventEnum = projectChangeEvent.getProjectChangeEventEnum();
      if (projectChangeEventEnum.equals(PshSettingsEventEnum.ADD_OR_REMOVE_ENABLED_JOINT_TYPE))
      {
        java.util.List<JointTypeEnum> selectedJointTypes = _pshSettingsJointTypeTableModel.getSelectedJointTypes();

        if(selectedJointTypes.isEmpty() == false)
        {
          _pshSettingsJointTypeTableModel.fireTableDataChanged();
          for(JointTypeEnum jointType : selectedJointTypes)
          {
            int selectedRow = _pshSettingsJointTypeTableModel.getRowForJointType(jointType);
            _pshSettingsJointTypeTable.addRowSelectionInterval(selectedRow, selectedRow);
          }
        }
      }
    }
  }
}