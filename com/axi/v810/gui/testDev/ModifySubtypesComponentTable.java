/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.testDev;

import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.ProjectHistoryLog;
import com.axi.v810.gui.*;
import com.axi.v810.gui.algoTuner.*;
import com.axi.v810.gui.mainMenu.MainMenuGui;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * @author chin-seong.kee
 */
public class ModifySubtypesComponentTable extends JSortTable
{
  public static final int _COMPONENT_NAME_INDEX = 0;
  public static final int _PACKAGE_INDEX = 1;
  public static final int _JOINT_TYPE_INDEX = 2;
  public static final int _SUBTYPE_INDEX = 3;
  public static final int _GLOBAL_SURFACE_MODEL_INDEX = 4;

  private static final int _COMPONENT_UNDO_INDEX = 0;

  private ModifySubtypesComponentTableModel _componentTableModel;
  
  private BoardType _currentBoardType;

   // subtype change choices
  private final String _NEW_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NEW_KEY");
  private final String _NEW_APPENDNAME_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NEW_SHADED_KEY");
  private final String _NOTEST_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOTEST_KEY");
  private final String _NOLOAD_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOLOAD_KEY");
  private final String _RENAME_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_RENAME_KEY");
  private final String _RESTORE_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_RESTORE_KEY");
  
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  private final String _NOT_APPLICABLE = "N/A";
  private static transient ProjectHistoryLog _fileWriter;
  
  //Siew Yeng - when alignment component set to No Load use this list to keep track affected alignment pads
  private List<Pad> _affectedAlignmentPads = new ArrayList();
  
  /**
   * @param model
   */
  public ModifySubtypesComponentTable(ModifySubtypesComponentTableModel model)
  {
    Assert.expect(model != null);

    _fileWriter=ProjectHistoryLog.getInstance();
    _componentTableModel = model;
    super.setModel(model);
  }

  /**
   * @return
   */
  public void setModel(ModifySubtypesComponentTableModel componentTableModel)
  {
    Assert.expect(componentTableModel != null);
    
    _componentTableModel = componentTableModel;
    super.setModel(_componentTableModel);
  }

  /**
   * @return
   */
  public ModifySubtypesComponentTableModel getTableModel()
  {
    Assert.expect(_componentTableModel != null);
    return _componentTableModel;
  }

  /**
   * 
   */
  public void setupCellEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();
    
    columnModel.getColumn(_JOINT_TYPE_INDEX).setCellEditor(new JointTypeEnumCellEditor(new JComboBox(), _componentTableModel));
    
    JComboBox componentSubtypeComboBox = new JComboBox();
    ComponentSubtypeCellEditor componentSubtypeCellEditor = new ComponentSubtypeCellEditor(componentSubtypeComboBox, _componentTableModel);
    componentSubtypeCellEditor.setEditorChoices(_NEW_SUBTYPE, _NEW_APPENDNAME_SUBTYPE, _NOTEST_SUBTYPE, _NOLOAD_SUBTYPE, _RENAME_SUBTYPE, _RESTORE_SUBTYPE);
    columnModel.getColumn(_SUBTYPE_INDEX).setCellEditor(componentSubtypeCellEditor);
    columnModel.getColumn(_SUBTYPE_INDEX).setCellRenderer(new ComponentLocationTableCellRenderer());
    
    JComboBox globalSurfaceModelComboBox = new JComboBox();
    GlobalSurfaceModelCellEditor globalSurfaceModelCellEditor = new GlobalSurfaceModelCellEditor(globalSurfaceModelComboBox, _componentTableModel);
    columnModel.getColumn(_GLOBAL_SURFACE_MODEL_INDEX).setCellEditor(globalSurfaceModelCellEditor);

    /*JComboBox userGainComboBox = new JComboBox();
    userGainComboBox.addItem(UserGainEnum.ONE);
    userGainComboBox.addItem(UserGainEnum.TWO);
    userGainComboBox.addItem(UserGainEnum.THREE);
    userGainComboBox.addItem(UserGainEnum.FOUR);
    userGainComboBox.addItem(UserGainEnum.FIVE);
    userGainComboBox.addItem(UserGainEnum.SIX);
    columnModel.getColumn(_USER_GAIN_INDEX).setCellEditor(new DefaultCellEditor(userGainComboBox));

    if (isShowShadingSummary())
    {
      JComboBox artifactCompensationComboBox = new JComboBox();
      artifactCompensationComboBox.addItem(ArtifactCompensationStateEnum.COMPENSATED);
      artifactCompensationComboBox.addItem(ArtifactCompensationStateEnum.NOT_COMPENSATED);
      columnModel.getColumn(_INTEFERENCE_COMPENSATION_INDEX).setCellEditor(new DefaultCellEditor(artifactCompensationComboBox));

      JComboBox integrationLevelComboBox = new JComboBox();
      SignalIntegrationLevelCellEditor integrationLevelColumnCellEditor = new SignalIntegrationLevelCellEditor(integrationLevelComboBox,
              _componentTableModel);
      columnModel.getColumn(_INTERGRATION_LEVEL_INDEX).setCellEditor(integrationLevelColumnCellEditor);
    } */
  }

  /**
   * Overriding this method allows us to have one cell's value change apply
   * to every selected row in the table.  Multi-cell editing requires this.
   * @author Andy Mechtenberg
   */
  public void setValueAt(Object value, int row, int column)
  {
    // if there is no value, exit out here -- there is no work to do
    if (value == null)
      return;
    
    //Khaw Chek Hau - XCR3047 : Software crash when try to enlarge the modify subtype column while modifying the subtype
    if(_commandManager.isExecutingCommand())
      return;
    
    componentTableSetValues(value, row, column);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void componentTableSetValues(Object value, int row, int column)
  {
    final int rows[] = getSelectedRows();
    final int col = convertColumnIndexToModel(column);
    String newAppendSubtypeNames[] = new String[rows.length];
    boolean isSettingNewAppendSubtypeName = false;
    String valStr = "";
    String appendNameStr = "";
    Map <String,String> oldSubtypeNameToNewSubtypeName = new HashMap<>();
    
    // special case of setting the subtype to  "New..."  We need to bring up a dialog
    // to ask them for the name of the new subtype
    if (col == 3) // subtype column
    {
      if (value instanceof String)
      {
        valStr = (String)value;
        if (valStr.equalsIgnoreCase(StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY"))) // mixed case
        {
          // the user is not allowed to set the subtype to mixed.  This result is because padtypes are set to different subtypes
          // and the resulting table entry is "mixed".  So, if this is the selection, it's because I show "mixed" in the subtype
          // combo box, and the user selected it.  I show "mixed" because it's current choice, and it's the default selection anyway.
          // So, if they try to set it to "mixed", it's best to ignore this, and treat it as a cancel edit.
          return;
        }
        if (valStr.equalsIgnoreCase(_NEW_SUBTYPE))
        {
          /*String prompt = StringLocalizer.keyToString("MMGUI_SUBTYPE_NEW_PROMPT_KEY");
          String title = StringLocalizer.keyToString("MMGUI_SUBTYPE_NEW_TITLE_KEY");
          String retVal = JOptionPane.showInputDialog(MainMenuGui.getInstance(),
                                                      prompt,
                                                      title,
                                                      JOptionPane.QUESTION_MESSAGE);

          if ((retVal == null) || (retVal.length() == 0)) // the use hit cancel or entered an empty string -- change nothing
            return;
          com.axi.v810.business.panelDesc.Panel panel = getCurrentBoardType().getPanel();
          if (panel.isSubtypeNameValid(retVal) == false)
          {
            // explain why it's invalid
            LocalizedString message;
            String illegalSubtypeCharacters = panel.getSubtypeNameIllegalChars();
            if (illegalSubtypeCharacters.equals(" "))
            {
              message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_ONLY_SPACES_KEY",
                                            new Object[]{retVal});
            }
            else
            {
             message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_OTHER_CHARS_KEY",
                                           new Object[]{retVal, illegalSubtypeCharacters});
            }
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                          StringLocalizer.keyToString(message),
                                          StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                          JOptionPane.ERROR_MESSAGE);
            return;
          }
          value = retVal;
          //write into history log-hsia-fen.tan
          _fileWriter.appendCreateSubtype(_componentTableModel.getCurrentlySelectedComponents(),retVal);*/
          ProjectObservable.getInstance().setEnabled(false);
          ChangeSubtypeDialog changeSubtypeDialog = new ChangeSubtypeDialog(MainMenuGui.getInstance(), StringLocalizer.keyToString("RMGUI_CHANGE_SUBTYPE_DIALOG_KEY"));
          changeSubtypeDialog.populateData(getCurrentBoardType().getPanel().getProject(),
                                           _componentTableModel.getComponentTypeAt(row).getPadTypes(), 
                                           _componentTableModel.getComponentTypeAt(row).getSubtypes().get(0));
          SwingUtils.centerOnComponent(changeSubtypeDialog, MainMenuGui.getInstance());
          changeSubtypeDialog.setModal(true);
          changeSubtypeDialog.setVisible(true);
          value = changeSubtypeDialog.getNewSubtypeName();
          //write into history log-hsia-fen.tan
          
          //Khaw Chek Hau - XCR-3330 : Failed to change existing subtype by using new subtype dialog
          if (changeSubtypeDialog.isExistingSubtypeUsed() == false)
            _fileWriter.appendCreateSubtype(_componentTableModel.getCurrentlySelectedComponents(), changeSubtypeDialog.getNewSubtypeName());
          
          ProjectObservable.getInstance().setEnabled(true);
        }
        else if (valStr.equalsIgnoreCase(_NEW_APPENDNAME_SUBTYPE))
        {
          Assert.expect(newAppendSubtypeNames.length == rows.length);            
          isSettingNewAppendSubtypeName = true;

          String prompt = StringLocalizer.keyToString("MMGUI_SUBTYPE_NEW_SHADED_PROMPT_KEY");
          String title = StringLocalizer.keyToString("MMGUI_SUBTYPE_NEW_SHADED_TITLE_KEY");
          appendNameStr = JOptionPane.showInputDialog(MainMenuGui.getInstance(),
                                                    prompt,
                                                    title,
                                                    JOptionPane.QUESTION_MESSAGE);

          if ((appendNameStr == null) || (appendNameStr.length() == 0)) // the use hit cancel or entered an empty string -- change nothing
              return;

          // Run through each row of subtype name and append "_SHADED" on the name
          for(int i = 0; i < rows.length; i++)
          {
            String subtypeName = (String)_componentTableModel.getValueAt(rows[i], col);
            String newSubtypeName = subtypeName + "_" + appendNameStr;

            // First, check to see if the component is set to load or no test
            ComponentType componentType = _componentTableModel.getComponentTypeAt(rows[i]);
            Panel panel = componentType.getSideBoardType().getBoardType().getPanel();
            if(componentType.getSubtypes().size() > 1)
            {
              for(Subtype subtype: componentType.getSubtypes())
              {
                newSubtypeName = subtype.getShortName() + "_" + (String)appendNameStr;
                if (panel.isSubtypeNameValid(newSubtypeName) == false)
                {
                  // explain why it's invalid
                  LocalizedString message;
                  String illegalSubtypeCharacters = panel.getSubtypeNameIllegalChars();
                  if (illegalSubtypeCharacters.equals(" "))
                  {
                    message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_ONLY_SPACES_KEY",
                                                  new Object[]
                                                  {newSubtypeName});
                  }
                  else
                  {
                    message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_OTHER_CHARS_KEY",
                                                  new Object[]
                                                  {newSubtypeName, illegalSubtypeCharacters});
                  }
                  JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                                StringLocalizer.keyToString(message),
                                                StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                JOptionPane.ERROR_MESSAGE);
                  return;
                }
                oldSubtypeNameToNewSubtypeName.put(subtype.getLongName(), newSubtypeName);
              }
//                  JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
//                                          StringLocalizer.keyToString("CAD_MIXED_SUBTYPE_NOT_ALLOWED_APPEND_NAME_KEY"),
//                                          StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
//                                          JOptionPane.WARNING_MESSAGE);
//                  return;
            }
            else if (componentType.isInspected() == false || componentType.isLoaded() == false)
            {
                newAppendSubtypeNames[i] = _NOT_APPLICABLE;
            }
            else
            {
              if (panel.isSubtypeNameValid(newSubtypeName))
                  newAppendSubtypeNames[i] = newSubtypeName;
              else
                  newAppendSubtypeNames[i] = subtypeName;

              JointTypeEnum jointTypeEnum = componentType.getPadTypeOne().getJointTypeEnum();
              if (panel.doesSubtypeShortNameAndJointTypeExist(newAppendSubtypeNames[i], jointTypeEnum))
              {
                String warningMessage = StringLocalizer.keyToString(new LocalizedString("RMGUI_NEW_SUBTYPE_EXIST_ERROR_KEY",
                                                                    new String[]{newSubtypeName, jointTypeEnum.getName()}));
                JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                              warningMessage,
                                              StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                                              JOptionPane.WARNING_MESSAGE);
                return;
              }
            }
          }
        }
        else if (valStr.equalsIgnoreCase(_NOLOAD_SUBTYPE))
        {
          //Siew Yeng - XCR1719 
          //keep track of affected alignment pads when alignment component are going to set to no load.
          _affectedAlignmentPads.clear();
          
          for(int i = 0; i < rows.length; i++)
          {
            ComponentType componentType = _componentTableModel.getComponentTypeAt(rows[i]);
            for(Pad pad: _currentBoardType.getPanel().getPanelSettings().getAllAlignmentPads())
            {
              if(componentType.getReferenceDesignator().equals(pad.getComponent().getReferenceDesignator()))
                _affectedAlignmentPads.add(pad);              
            }
          }

          // Siew Yeng - XCR1719 
          // - prompt warning message to clear alignment joint
          // - Yes to clear alignment joint
          // - No to set to No Load except alignment component
          // - Cancel to remain unchanged
          int status = JOptionPane.CANCEL_OPTION;
          if(_affectedAlignmentPads.isEmpty() == false)
          {
            status = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(), 
            StringLocalizer.keyToString("MMGUI_SUBTYPE_ALIGNMENT_JOINT_SET_TO_NO_LOAD_WARNING_KEY"),
            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), 
            JOptionPane.YES_NO_CANCEL_OPTION, 
            JOptionPane.QUESTION_MESSAGE);

            if(status == JOptionPane.CANCEL_OPTION || status == JOptionPane.CLOSED_OPTION)
              return;
          }
          
          // no Load -- set manually to each row
          _commandManager.trackState(getCurrentUndoState());
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("MMGUI_NO_LOAD_GROUP_UNDO_KEY"));
          for(int i = 0; i < rows.length; i++)
          {
            ComponentType componentType = _componentTableModel.getComponentTypeAt(rows[i]); 
            try
            {
              if(status == JOptionPane.YES_OPTION)
              {
                for(Pad pad : _affectedAlignmentPads)
                {
                  if(componentType.getReferenceDesignator().equals(pad.getComponent().getReferenceDesignator()))
                  {
                    AlignmentGroup alignmentGroup = _currentBoardType.getPanel().getPanelSettings().getAlignmentGroupFromPad(pad);
                    if(alignmentGroup == null)
                    {
                      System.out.println("Alignment Group not found for Pad: "+pad.getBoardAndComponentAndPadName());
                      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                                    StringLocalizer.keyToString("MMGUI_RECIPE_FILE_CORRUPTED_WARNING_KEY"), 
                                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                    JOptionPane.WARNING_MESSAGE);
                      break;
                    }
                    else
                      _commandManager.execute(new RemoveFeaturesFromAlignmentGroupCommand(alignmentGroup,
                                                                                          new ArrayList<>(Arrays.asList(pad)),
                                                                                          new ArrayList()));
                  }
                }
              }
              else if(status == JOptionPane.NO_OPTION)
              {
                boolean skip = false;
                for(Pad pad : _affectedAlignmentPads)
                {
                  if(componentType.getReferenceDesignator().equals(pad.getComponent().getReferenceDesignator()))
                  {
                    skip = true;
                    break;
                  }
                }
                if(skip)
                  continue;
              }
//               Kok Chun, Tan - no load and no test no related. no load not necessary no test.
//              try
//              {
//                ProjectObservable.getInstance().setEnabled(false);
//                for (PadType padType : componentType.getPadTypes())
//                {
//                  _commandManager.execute(new PadTypeSetTestedCommand(padType, false));
//                }
//              }
//              finally
//              {
//                ProjectObservable.getInstance().setEnabled(true);
//                ProjectObservable.getInstance().stateChanged(componentType.getComponentTypeSettings(), true, false, ComponentTypeSettingsEventEnum.INSPECTED);
//              }
              _commandManager.execute(new ComponentTypeSetLoadedCommand(componentType, false));
              _componentTableModel.fireTableCellUpdated(rows[i], column);
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                            ex.getLocalizedMessage(),
                                            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                            true);
              _componentTableModel.fireTableCellUpdated(rows[i], column);
            }
             _fileWriter.appendSubtypeNoLoadNoTest(componentType,valStr);
          }
          _commandManager.endCommandBlock();
          return;
        }
        else if (valStr.equalsIgnoreCase(_NOTEST_SUBTYPE))
        {
          // no Test -- set manually to each row
          _commandManager.trackState(getCurrentUndoState());
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("MMGUI_NO_TEST_GROUP_UNDO_KEY"));
          for(int i = 0; i < rows.length; i++)
          {
            ComponentType componentType = _componentTableModel.getComponentTypeAt(rows[i]);
            try
            {
              // setting something to NO TEST implies that it is loaded.
              _commandManager.execute(new ComponentTypeSetLoadedCommand(componentType, true));
              //_commandManager.execute(new ComponentTypeSetTestedCommand(componentType, false));
              // set all joints in component to No Test.
              try
              {
                ProjectObservable.getInstance().setEnabled(false);
                for (PadType padType : componentType.getPadTypes())
                {
                  _commandManager.execute(new PadTypeSetTestedCommand(padType, false));
                }
              }
              finally
              {
                  ProjectObservable.getInstance().setEnabled(true);
                  ProjectObservable.getInstance().stateChanged(componentType.getComponentTypeSettings(), false, true, ComponentTypeSettingsEventEnum.INSPECTED);
              }
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                            ex.getLocalizedMessage(),
                                            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                            true);
              _componentTableModel.fireTableCellUpdated(rows[i], column);
            }
             _fileWriter.appendSubtypeNoLoadNoTest(componentType,valStr);
          }
          _commandManager.endCommandBlock();
          return;
        }
        else if (valStr.startsWith(_RESTORE_SUBTYPE))
        {
          // Set to test -- set manually to each row
          _commandManager.trackState(getCurrentUndoState());
          _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMP_TYPE_SET_TESTED_KEY"));
          for(int i = 0; i < rows.length; i++)
          {
            ComponentType componentType = _componentTableModel.getComponentTypeAt(rows[i]);
            try
            {
              _commandManager.execute(new ComponentTypeSetTestedCommand(componentType, true));
              // setting something to be tested implies that it is loaded.
              _commandManager.execute(new ComponentTypeSetLoadedCommand(componentType, true));
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                            ex.getLocalizedMessage(),
                                            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                            true);
              _componentTableModel.fireTableCellUpdated(rows[i], column);
            }
            _fileWriter.appendSubtypeNoLoadNoTest(componentType,valStr);
          }
          _commandManager.endCommandBlock();
          return;
        }
        else if (valStr.equalsIgnoreCase(_RENAME_SUBTYPE))
        {
          ComponentType componentType = _componentTableModel.getComponentTypeAt(getSelectedRow());
          String currentSubtype = componentType.getComponentTypeSettings().getSubtype().getShortName();
          String prompt = StringLocalizer.keyToString("MMGUI_SUBTYPE_RENAME_PROMPT_KEY");
          String title = StringLocalizer.keyToString("MMGUI_SUBTYPE_RENAME_TITLE_KEY");
          Object retVal = JOptionPane.showInputDialog(MainMenuGui.getInstance(),
                                                      prompt,
                                                      title,
                                                      JOptionPane.QUESTION_MESSAGE,
                                                      null,
                                                      null,
                                                      currentSubtype);
          String newName = (String)retVal;
          if ((retVal == null) || (newName.length() == 0)) // the use hit cancel or entered an empty string -- change nothing
            return;
          com.axi.v810.business.panelDesc.Panel panel = getCurrentBoardType().getPanel();
          if (panel.isSubtypeNameValid(newName) == false)
          {
            // explain why it's invalid
            LocalizedString message;
            String illegalSubtypeCharacters = panel.getSubtypeNameIllegalChars();
            if (illegalSubtypeCharacters.equals(" "))
            {
              message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_ONLY_SPACES_KEY",
                                            new Object[]
                                            {retVal});
            }
            else
            {
              message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_OTHER_CHARS_KEY",
                                            new Object[]
                                            {retVal, illegalSubtypeCharacters});
            }
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                          StringLocalizer.keyToString(message),
                                          StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                          JOptionPane.ERROR_MESSAGE);

            return;
          }
          try
          {
            // XCR1740 - Add in ?Append Name? option in Advance Subtype windows, as in ?Modify Subtypes? page
            // by Lim Seng Yew on 27-Dec-2013
            // Put checking if new subtype (with same joint type) already exist.
            JointTypeEnum jointTypeEnum = componentType.getPadTypeOne().getJointTypeEnum();
            if (panel.doesSubtypeShortNameAndJointTypeExist(newName, jointTypeEnum))
            {
              String warningMessage = StringLocalizer.keyToString(new LocalizedString("RMGUI_NEW_SUBTYPE_EXIST_ERROR_KEY",
                                                                  new String[]{newName, jointTypeEnum.getName()}));
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                            warningMessage,
                                            StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                                            JOptionPane.WARNING_MESSAGE);
              return;
            }
            _commandManager.trackState(getCurrentUndoState());
            _commandManager.execute(new SubtypeSetNameCommand(componentType.getComponentTypeSettings().getSubtype(), newName));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
            _componentTableModel.fireTableCellUpdated(row, column);
          }
          _fileWriter.appendSubtypeRenameByComponent(componentType,currentSubtype,newName);
          return;
        }
      }
    }
    _commandManager.trackState(getCurrentUndoState());
    if (column == 2)
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_JOINTTYPE_KEY"));
    if (column == 3)
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_SUBTYPE_KEY"));
    if (column == 4)
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_GLOBAL_SURFACE_MODEL_KEY"));
   /* if (column == 5)
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_USER_GAIN_KEY"));
    if (column == 6)
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_ARTIFACT_COMPENSATION_KEY"));
    if (column == 7)
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_INTEGRATION_LEVEL_KEY"));*/

    // XCR1740 - Add in ?Append Name? option in Advance Subtype windows, as in ?Modify Subtypes? page
    // by Lim Seng Yew on 27-Dec-2013
    // Put checking if new subtype (with same joint type) already exist.
    // Cannot check if a subtype with same joint type exist, and assign new subtype name at the same time.
    // After assign new subtype name to first component, the second component cannot assign new subtype already 
    // because already new subtype already exist.
//    if (valStr.equalsIgnoreCase(_NEW_APPENDNAME_SUBTYPE))
//    {
//      for(int i = 0; i < rows.length; i++)
//      {
//        ComponentType componentType = _componentTableModel.getComponentTypeAt(rows[i]);
//        String newSubtypeName = (String)value;
//        JointTypeEnum jointTypeEnum = componentType.getPadTypeOne().getJointTypeEnum();
//        if (isSettingNewShadedSubtypeName)
//          newSubtypeName = newShadedSubtypeNames[i];
//        com.axi.v810.business.panelDesc.Panel panel = getCurrentBoardType().getPanel();
//        
//      }
//    }
    final BusyCancelDialog busyDialog = new BusyCancelDialog(
        MainMenuGui.getInstance(),
        StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_SUBTYPE_KEY"),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, MainMenuGui.getInstance());
    final boolean isSettingNewShadedSubtypeNameRef = isSettingNewAppendSubtypeName;
    final Map <String,String> oldSubtypeNameToNewSubtypeNameRef = oldSubtypeNameToNewSubtypeName;
    final String newAppendSubtypeNamesRef[] = newAppendSubtypeNames;
    //Khaw Chek Hau - XCR2912 : Unable to change focus method which will in turn caused an assert
    final Object val = value;
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          // XCR-3694 Duplicate Subtype Name When Append Name on Mixed Subtype
          Map<Pair<String,JointTypeEnum>,String> newSubtypeShortNameAndJointTypeToNewSubtypeLongNameMap = new HashMap<Pair<String,JointTypeEnum>,String>();
          
          for(int i = 0; i < rows.length; i++)
          {
            ComponentType componentType = _componentTableModel.getComponentTypeAt(rows[i]);
            if (componentType.isInspected() && componentType.isLoaded())
            {
              if(isSettingNewShadedSubtypeNameRef && componentType.getSubtypes().size() > 1)
              {
                for(PadType padType : componentType.getPadTypes())
                {
                  Subtype currentSubtype = padType.getSubtype();
                  Subtype subtype;
                  String newSubtypeName = oldSubtypeNameToNewSubtypeNameRef.get(currentSubtype.getLongName());
                  Panel panel = componentType.getSideBoardType().getBoardType().getPanel();
                  
                  // XCR-3694 Duplicate Subtype Name When Append Name on Mixed Subtype
                  for(Map.Entry<Pair<String,JointTypeEnum>, String> entry : newSubtypeShortNameAndJointTypeToNewSubtypeLongNameMap.entrySet())
                  {
                    Pair<String,JointTypeEnum> newSubtypeShortNameToJointTypeEnumPair = entry.getKey();
                    if (newSubtypeShortNameToJointTypeEnumPair.getFirst().equalsIgnoreCase(newSubtypeName) &&
                        newSubtypeShortNameToJointTypeEnumPair.getSecond().equals(padType.getJointTypeEnum()))
                    {
                      newSubtypeName = entry.getValue();
                    }
                  }
                  
                  if(panel.doesSubtypeExist(newSubtypeName) == false)
                  {
                    // create new subtype for 1st time
                    Subtype newSubtype = panel.createSubtype(newSubtypeName, componentType.getLandPattern(), padType.getJointTypeEnum());
                    subtype = newSubtype;
                    
                    // XCR-3694 Duplicate Subtype Name When Append Name on Mixed Subtype
                    boolean foundEntry = false;
                    for(Map.Entry<Pair<String,JointTypeEnum>, String> entry : newSubtypeShortNameAndJointTypeToNewSubtypeLongNameMap.entrySet())
                    {
                      Pair<String,JointTypeEnum> newSubtypeShortNameToJointTypeEnumPair = entry.getKey();
                      if (newSubtypeShortNameToJointTypeEnumPair.getFirst().equalsIgnoreCase(newSubtypeName) &&
                          newSubtypeShortNameToJointTypeEnumPair.getSecond().equals(padType.getJointTypeEnum()))
                      {
                        foundEntry = true;
                        break;
                      }
                    }
                    
                    if (foundEntry == false)
                    {
                      newSubtypeShortNameAndJointTypeToNewSubtypeLongNameMap.put(
                        new Pair<String,JointTypeEnum>(newSubtypeName, padType.getJointTypeEnum()), 
                        newSubtype.getLongName());
                    }
                  }
                  else
                  {
                    subtype = panel.getSubtype(newSubtypeName);
                  }
                  try
                  {
                    // assign to the new subtype name 
                    _commandManager.execute(new PadTypeSetSubtypeCommand(padType, subtype));
                  }
                  catch (XrayTesterException ex)
                  {
                    MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                                ex.getLocalizedMessage(),
                                                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                true);
                  }
                  _fileWriter.appendComponentDetail(componentType,padType.getJointTypeEnum(), padType,subtype);
                }
              }
              else
              {
                if (isSettingNewShadedSubtypeNameRef)
                  _componentTableModel.setValueAt(newAppendSubtypeNamesRef[i], rows[i], col);
                //Khaw Chek Hau - XCR2912 : Unable to change focus method which will in turn caused an assert
                else
                  _componentTableModel.setValueAt(val, rows[i], col);
              }
            }
          }
          // XCR-3191 Failed to undo after change joint type at Modify Subtype tab          
          _commandManager.endCommandBlock();
          //_componentTableModel.fireTableDataChanged();
          oldSubtypeNameToNewSubtypeNameRef.clear();
          // XCR-3694 Duplicate Subtype Name When Append Name on Mixed Subtype
          newSubtypeShortNameAndJointTypeToNewSubtypeLongNameMap.clear();
        }
        finally
        {
          busyDialog.setVisible(false);
          //Khaw Chek Hau - XCR3047 : Software crash when try to enlarge the modify subtype column while modifying the subtype
          //_commandManager.endCommandBlock();
        }
      }
    });
    busyDialog.setVisible(true);
  }

  /**
   * @return the _currentBoardType
   */
  public BoardType getCurrentBoardType()
  {
    return _currentBoardType;
  }

  /**
   * @param currentBoardType the _currentBoardType to set
   */
  public void setCurrentBoardType(BoardType currentBoardType)
  {
    this._currentBoardType = currentBoardType;
  }

  /**
   * @author Erica Wheatcroft
   */
  private UndoState getCurrentUndoState()
  {
    UndoState undoState = new UndoState();
    undoState.setModifySubtypesViewSelectIndex(_COMPONENT_UNDO_INDEX);
    return undoState;
  }
}
