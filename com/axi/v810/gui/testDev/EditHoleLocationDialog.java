package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.text.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * Dialog used to edit the x and y location for the hole of a throughhole pad
 * @author Laura Cormos
 */
public class EditHoleLocationDialog extends EscapeDialog
{
  private Project _project = null;
  private ThroughHoleLandPatternPad _landPatternPad = null;
  private FocusAdapter _textFieldFocusAdapter;
  private JPanel _mainPanel = new JPanel();
  private JLabel _xPositionLabel = new JLabel();
  private JLabel _yPositionLabel = new JLabel();
  private NumericRangePlainDocument _xPosDocument = new NumericRangePlainDocument();
  private NumericRangePlainDocument _yPosDocument = new NumericRangePlainDocument();
  private NumericTextField _xPositionTextField = new NumericTextField();
  private NumericTextField _yPositionTextField = new NumericTextField();
  private JPanel _buttonsPanel = new JPanel();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private JPanel _selectionsPanel = new JPanel();
  private Border _empty5Border;
  private BorderLayout _mainBorderLayout = new BorderLayout();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private Border _empty10Border;
  private PairLayout _selectionsPanelPairLayout = new PairLayout(10, 10, false);
  private JPanel _innerButtonsPanel = new JPanel();
  private GridLayout _innerButtonsPanelGridLayout = new GridLayout();
  private FlowLayout _buttonsPanelFlowLayout = new FlowLayout();

  //Kee Chin Seong - To create a parent dynamically
  private Frame _frame = null;
  
  /**
   * Public constructor, centers over parent frame passed in.
   * Modal dialog.
   * @author Carli Connally
   */
  public EditHoleLocationDialog(Frame parent, Project project, ThroughHoleLandPatternPad landPatternPad)
  {
    super(parent, StringLocalizer.keyToString("CAD_EDIT_HOLE_LOCATION_TITLE_KEY"), true);
    Assert.expect(parent != null);
    Assert.expect(project != null);
    Assert.expect(landPatternPad != null);

    _frame = parent;
    _project = project;
    _landPatternPad = landPatternPad;
    jbInit();
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
  }

  public void clear()
  {
    _project = null;
  }

  /**
   * GUI initialization code.
   * @author Carli Connally
   */
  private void jbInit()
  {
    _textFieldFocusAdapter = new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        JTextField sourceTextField = (JTextField)e.getSource();
        sourceTextField.selectAll();
      }
      public void focusLost(FocusEvent e)
      {
        NumericTextField sourceTextField = (NumericTextField)e.getSource();
        handleTextFieldData(sourceTextField);
      }
    };

    _empty5Border = BorderFactory.createEmptyBorder(5,5,5,5);
    _empty10Border = BorderFactory.createEmptyBorder(10,10,10,10);
    _mainPanel.setBorder(_empty10Border);
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _xPositionLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_X_POSITION_KEY") + ":  ");
    _yPositionLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_Y_POSITION_KEY") + ":  ");
    _buttonsPanel.setLayout(_buttonsPanelFlowLayout);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed();
      }
    });
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed();
      }
    });
    _selectionsPanel.setLayout(_selectionsPanelPairLayout);
    _selectionsPanel.setBorder(_empty5Border);
    getContentPane().setLayout(_mainBorderLayout);
    _innerButtonsPanel.setLayout(_innerButtonsPanelGridLayout);
    _innerButtonsPanelGridLayout.setHgap(10);
    _buttonsPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _buttonsPanelFlowLayout.setVgap(0);
    _mainPanelBorderLayout.setVgap(10);
    _buttonsPanel.add(_innerButtonsPanel, null);
    _innerButtonsPanel.add(_okButton, null);
    _innerButtonsPanel.add(_cancelButton, null);

    _selectionsPanel.add(_xPositionLabel);
    _selectionsPanel.add(_xPositionTextField);
    _selectionsPanel.add(_yPositionLabel);
    _selectionsPanel.add(_yPositionTextField);
    _mainPanel.add(_buttonsPanel, BorderLayout.SOUTH);
    _mainPanel.add(_selectionsPanel, BorderLayout.CENTER);
    _xPositionTextField.setDocument(_xPosDocument);
    _yPositionTextField.setDocument(_yPosDocument);
    _xPositionTextField.addFocusListener(_textFieldFocusAdapter);
    _yPositionTextField.addFocusListener(_textFieldFocusAdapter);
    updateTextFieldDecimalFormats();
    getContentPane().add(_mainPanel, BorderLayout.CENTER);
    getRootPane().setDefaultButton(_okButton);
    _xPositionTextField.setColumns(6);
    _yPositionTextField.setColumns(6);
    _xPosDocument.setRange(new DoubleRange(-Integer.MAX_VALUE, Integer.MAX_VALUE));
    _yPosDocument.setRange(new DoubleRange(-Integer.MAX_VALUE, Integer.MAX_VALUE));
    _xPositionTextField.setValue(MathUtil.convertUnits(_landPatternPad.getHoleCoordinateInNanoMeters().getX(),
                                                       MathUtilEnum.NANOMETERS,
                                                       _project.getDisplayUnits()));
    _yPositionTextField.setValue(MathUtil.convertUnits(_landPatternPad.getHoleCoordinateInNanoMeters().getY(),
                                                       MathUtilEnum.NANOMETERS,
                                                       _project.getDisplayUnits()));

    pack();
  }

  /**
   * @author Laura Cormos
   */
  private void updateTextFieldDecimalFormats()
   {
     StringBuffer decimalFormat = new StringBuffer("########0.");
     MathUtilEnum unitsEnum = _project.getDisplayUnits();

     int decimalPlaces = MeasurementUnits.getMeasurementUnitDecimalPlaces(unitsEnum);
     for(int i = 0; i < decimalPlaces; i++)
       decimalFormat.append("0");  // use a "0" if you want to show trailing zeros, use a "#" if you don't

     _xPositionTextField.setFormat(new DecimalFormat(decimalFormat.toString()));
     _yPositionTextField.setFormat(new DecimalFormat(decimalFormat.toString()));
  }


  /**
   * @author Laura Cormos
   */
  private void handleTextFieldData(NumericTextField numericTextField)
  {
    Assert.expect(numericTextField != null);

    Double pos;
    try
    {
      pos = numericTextField.getDoubleValue();
    }
    catch (ParseException ex)
    {
      // this typically happens when the user deletes all characters from the field -- same as zero
      pos = new Double(0.0);
    }
    numericTextField.setValue(pos);
  }

  /**
   * Changes the x, y location of the hole to the given values.  Does appropriate checks first to make
   * sure that the specified information is valid.  Closes the dialog.
   * @author Laura Cormos
   */
  private void okButton_actionPerformed()
  {
    try
    {
      MathUtilEnum unitsEnum = _project.getDisplayUnits();
      Double xPos = _xPositionTextField.getDoubleValue();
      Double yPos = _yPositionTextField.getDoubleValue();
      int xPosInNanoMeters = (int)MathUtil.convertUnits(xPos.doubleValue(), unitsEnum, MathUtilEnum.NANOMETERS);
      int yPosInNanoMeters = (int)MathUtil.convertUnits(yPos.doubleValue(), unitsEnum, MathUtilEnum.NANOMETERS);
      try
      {
        com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().execute(
            new THLandPatternPadSetHoleCoordinateInNanoMetersCommand(_landPatternPad,
                                                                     new ComponentCoordinate(xPosInNanoMeters,
                                                                                             yPosInNanoMeters)));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
      }
    }
    catch (ParseException ex)
    {
      Assert.expect(false, ex.getMessage());
    }
    dispose();
  }

  /**
   * User presses the cancel button.  The operation is cancelled.  The hole x,y location remains the same as before
   * @author Carli Connally
   */
  private void cancelButton_actionPerformed()
  {
    dispose();
  }
}
