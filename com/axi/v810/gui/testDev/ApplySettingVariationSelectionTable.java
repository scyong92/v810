package com.axi.v810.gui.testDev;

import java.awt.Component;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 *
 * @Author by Kee Chin Seong
 */
public class ApplySettingVariationSelectionTable extends JTable
{
  public static final int _VARIATION_INDEX = 0;
  public static final int _APPLY_INDEX = 1;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _VARIATION_COLUMN_WIDTH_PERCENTAGE = .90;
  private static final double _APPLY_COLUMN_WIDTH_PERCENTAGE = .10;
 
  private CheckCellEditor _checkEditor = new CheckCellEditor(getBackground(), getForeground());

  /**
   * Default constructor.
   * @author Chin Seong, Kee
   */
  public ApplySettingVariationSelectionTable()
  {
    // do nothing
    getTableHeader().setReorderingAllowed(false);
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Chin Seong, Kee
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int variationColumnWidth    = (int)(totalColumnWidth * _VARIATION_COLUMN_WIDTH_PERCENTAGE);
    int enableColumnWidth    = (int)(totalColumnWidth * _APPLY_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_VARIATION_INDEX).setPreferredWidth(variationColumnWidth);
    columnModel.getColumn(_APPLY_INDEX).setPreferredWidth(enableColumnWidth);
  }

  /**
   * @author Chin Seong, Kee
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };
    columnModel.getColumn(_VARIATION_INDEX).setCellEditor(stringEditor);
    
    columnModel.getColumn(_APPLY_INDEX).setCellEditor(_checkEditor);
  }
  
  /**
   * @author Chin Seong, Kee
   */
  public List<VariationSetting> getSelectedVariations()
  {
    VariationSettingTableModel model = (VariationSettingTableModel)getModel();
    java.util.List<VariationSetting> selectedVariations = new ArrayList<VariationSetting>();
    int rowCount = getRowCount();

    for(int i = 0; i < rowCount; ++i)
    {
      if(selectionModel.isSelectedIndex(i))
      {
        selectedVariations.add(model.getVariation(i));
      }
    }

    return selectedVariations;
  }
}
