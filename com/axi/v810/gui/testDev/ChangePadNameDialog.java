package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * This dialog allows a land pattern pad name to be modified with a suffix or prefix
 * @author Laura Cormos
 */
public class ChangePadNameDialog extends JDialog
{
  private boolean _ok = false;

  private BorderLayout _mainBorderLayout = new BorderLayout();
  private JPanel _selectPanel = new JPanel();
  private JPanel _okCancelPanel = new JPanel();
  private JPanel _okCancelInnerPanel = new JPanel();
  private GridLayout _okCancelInnerGridLayout = new GridLayout();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private FlowLayout _okCanelFowLayout = new FlowLayout();
  private PairLayout _selectPanelPairLayout = new PairLayout(10, 10, false);
  private JLabel _suffixLabel = new JLabel();
  private JRadioButton _suffixRadioButton = new JRadioButton();
  private JRadioButton _prefixRadioButton = new JRadioButton();
  private ButtonGroup _radioButtonGroup = new ButtonGroup();
  private JLabel _selectSuffixPrefixLabel = new JLabel();
  private JTextField _suffixTextField = new JTextField();
  private JPanel _radioButtonGroupPanel = new JPanel();

  /**
   * @author Andy Mechtenberg
   */
  ChangePadNameDialog(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);

    jbInit();

    pack();
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit()
  {
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    getContentPane().setLayout(_mainBorderLayout);
    _okCancelInnerPanel.setLayout(_okCancelInnerGridLayout);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });

    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });

    _okCancelPanel.setLayout(_okCanelFowLayout);
    _okCanelFowLayout.setAlignment(FlowLayout.RIGHT);
    _okCancelInnerGridLayout.setHgap(20);
    _selectPanel.setLayout(_selectPanelPairLayout);
    _suffixLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _suffixLabel.setText(StringLocalizer.keyToString("CPN_ADD_TO_PAD_NAME_KEY"));
    _selectSuffixPrefixLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _selectSuffixPrefixLabel.setText(StringLocalizer.keyToString("CPN_SELECT_ONE_KEY"));
    _selectPanel.setBorder(BorderFactory.createEmptyBorder(10,10,20,10));
    _okCancelPanel.setBorder(BorderFactory.createEmptyBorder(0,0,5,5));
    getContentPane().add(_selectPanel, BorderLayout.CENTER);
    _selectPanel.add(_suffixLabel);
    _selectPanel.add(_suffixTextField);
    _selectPanel.add(_selectSuffixPrefixLabel);
    _suffixRadioButton.setText(StringLocalizer.keyToString("CPN_SUFFIX_KEY"));
    _prefixRadioButton.setText(StringLocalizer.keyToString("CPN_PREFIX_KEY"));
    _prefixRadioButton.setSelected(true);
    _selectPanel.add(_radioButtonGroupPanel);
    _radioButtonGroupPanel.add(_prefixRadioButton);
    _radioButtonGroupPanel.add(_suffixRadioButton);
    getContentPane().add(_okCancelPanel,  BorderLayout.SOUTH);
    _okCancelPanel.add(_okCancelInnerPanel, null);
    _okCancelInnerPanel.add(_okButton, null);
    _okCancelInnerPanel.add(_cancelButton, null);
    _suffixTextField.setColumns(10);
    _radioButtonGroup.add(_suffixRadioButton);
    _radioButtonGroup.add(_prefixRadioButton);

    getRootPane().setDefaultButton(_okButton);
  }

  /**
   * @author Laura Cormos
   */
  boolean userClickedOk()
  {
    return _ok;
  }

  /**
   * @author Laura Cormos
   */
  String getTextToAdd()
  {
    if (_suffixTextField != null)
      return _suffixTextField.getText();
    else
      return "";
  }

  /**
   * @author Laura Cormos
   */
  boolean isSuffix()
  {
    Assert.expect(_suffixRadioButton != null);
    return _suffixRadioButton.isSelected();
  }


  /**
   * @author Laura Cormos
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  /**
   * @author Laura Cormos
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    _ok = true;
    dispose();
  }

}
