package com.axi.v810.gui.testDev;

import java.awt.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.algoTuner.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.business.ProjectObservable;
import java.util.Observer;
import java.util.Observable;

/**
 * This is the Fine Tuning panel for the Test Development Tool
 *
 * @author Andy Mechtenberg
 */
public class TunerPanel extends AbstractTaskPanel implements Observer
{
  private TestDev _testDev;
  private ImageManagerPanel _imagePanel;
  private AlgoTunerGui _algoTuner;
  private boolean _updateData = false;
  private boolean _populateProjectData = false;
  private Project _project = null;
  /**
   * @author Andy Mechtenberg
   */
  public TunerPanel(TestDev testDev, ImageManagerPanel imagePanel)
  {
    super(testDev);
    Assert.expect(testDev != null);
    Assert.expect(imagePanel != null);
    _testDev = testDev;
    _imagePanel = imagePanel;
    jbInit();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    setLayout(new BorderLayout());
    _algoTuner = new AlgoTunerGui();
    _algoTuner.setTestDev(_testDev);
    JPanel tunerPanel = _algoTuner.getAlgorithmTunerPanel();
    add(tunerPanel, BorderLayout.CENTER);
    _imagePanel.saveAlgoTunerInstance(_algoTuner); //Chin Seong, Kee - Save Algo Tuner UI 
  }

  /**
   * @author Andy Mechtenberg
   */
  public void update(Observable observable, final Object argument)
  {
    if (observable instanceof ProjectObservable)
    {
      _updateData = true;
    }
    else
      Assert.expect(false);
  }


  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    _project = null;
    _algoTuner.saveUIState();
    _algoTuner.unpopulate();
    ProjectObservable.getInstance().deleteObserver(this);
  }

  /**
   * @author George Booth
   */
  void populateWithProjectData()
  {
    _algoTuner.populateWithProjectData(_project, _testDev.getCurrentBoardType());
    //    _algoTuner.enableAbilityToRunTestsForDebugOnly();
  }

  /**
   * @author Andy Mechtenberg
   */
  void populateWithProjectData(Project project)
  {
    Assert.expect(project != null);
    _project = project;

    ProjectObservable.getInstance().addObserver(this);

    if (_active)
    {
      populateWithProjectData();
      _populateProjectData = false;
    }
    else
    {
      _populateProjectData = true;
    }
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  public void start()
  {
    Assert.expect(_algoTuner != null);

//    System.out.println("TunerPanel().start()");

    com.axi.util.TimerUtil screenTimer = new com.axi.util.TimerUtil();
    screenTimer.start();

    if (_populateProjectData)
    {
      populateWithProjectData();
      // if we populate here, we don't need to also update data
      _updateData = false;
    }
    _populateProjectData = false;

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();
    _testDev.addBoardSelectInToolbar(_envToolBarRequesterID);
    _active = true;

    // add custom menus
    _algoTuner.addTunerMenus(_menuBar, _menuRequesterID);

    // add custom toolbar components
    _algoTuner.addTunerToolBar(_envToolBar, _envToolBarRequesterID);

    if (_updateData)
    {
      _algoTuner.updateWithProjectData();
    }
    _updateData = false;

    _algoTuner.start();
    
    // Swee Yee Wong - XCR-2734	Update Nominals button suddenly disabled.
    if(_imagePanel.getSelectedImageSets().isEmpty()==false)
      _algoTuner.updateSelectedImageSets(_imagePanel.getSelectedImageSets());

    _testDev.switchToPanelGraphics();
    _testDev.showImageWindow();
    _imagePanel.displayFineTuningImagePanel();

    screenTimer.stop();
//    System.out.println("  Total Loading for Fine Tuning: (in mils): "+ screenTimer.getElapsedTimeInMillis());
  }

  public boolean isReadyToFinish()
  {
//    System.out.println("TunerPanel().isReadyTofinish()");
    return _algoTuner.isReadyToFinish();
  }

  /**
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  public void finish()
  {
    Assert.expect(_algoTuner != null);

    // make sure Review Defects is tuned off before finishing
    _algoTuner.finish();
    _testDev.setActiveTunerComponentType();
    _imagePanel.finishFineTuningImagePanel();

//    System.out.println("TunerPanel().finish()");
    super.finishMenusAndToolBar();

    _active = false;
  }
  
   /**
   * @author weng-jian.eoh
   * XCR-3589 Combo STD and XXL software GUI
   */
  public void disable()
  {
    Assert.expect(_algoTuner != null);

    // make sure Review Defects is tuned off before finishing
    _algoTuner.disable();
  }
}
