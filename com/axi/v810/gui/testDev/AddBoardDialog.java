package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.text.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Dialog used to add a single board to a panel for panel setup
 * @author Andy Mechtenberg
 */
class AddBoardDialog extends EscapeDialog
{
  private Project _project = null;
  private FocusAdapter _textFieldFocusAdapter;
  private JPanel _mainPanel = new JPanel();
  private JLabel _selectBoardLabel = new JLabel();
  private JLabel _xPositionLabel = new JLabel();
  private JLabel _yPositionLabel = new JLabel();
  private NumericRangePlainDocument _xPosDocument = new NumericRangePlainDocument();
  private NumericRangePlainDocument _yPosDocument = new NumericRangePlainDocument();
  private NumericTextField _xPositionTextField = new NumericTextField();
  private NumericTextField _yPositionTextField = new NumericTextField();
  private JPanel _buttonsPanel = new JPanel();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private JPanel _selectionsPanel = new JPanel();
  private Border _empty5Border;
  private BorderLayout _mainBorderLayout = new BorderLayout();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private Border _empty10Border;
  private PairLayout _selectionsPanelPairLayout = new PairLayout(10, 10, false);
  private JComboBox _boardComboBox = new JComboBox();
  private JPanel _innerButtonsPanel = new JPanel();
  private GridLayout _innerButtonsPanelGridLayout = new GridLayout();
  private FlowLayout _buttonsPanelFlowLayout = new FlowLayout();

  /**
   * Public constructor, centers over parent frame passed in.
   * Modal dialog.
   * @author Carli Connally
   */
  public AddBoardDialog(Frame parent, Project project)
  {
    super(parent, StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_TITLE_KEY"), true);
    Assert.expect(parent != null);
    Assert.expect(project != null);

    _project = project;
    jbInit();
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
  }

  public void clear()
  {
    _project = null;
    _boardComboBox.removeAllItems();
  }

  /**
   * GUI initialization code.
   * @author Carli Connally
   */
  private void jbInit()
  {
    _textFieldFocusAdapter = new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        JTextField sourceTextField = (JTextField)e.getSource();
        sourceTextField.selectAll();
      }
      public void focusLost(FocusEvent e)
      {
        NumericTextField sourceTextField = (NumericTextField)e.getSource();
        handleTextFieldData(sourceTextField);
      }
    };

    _empty5Border = BorderFactory.createEmptyBorder(5,5,5,5);
    _empty10Border = BorderFactory.createEmptyBorder(10,10,10,10);
    _mainPanel.setBorder(_empty10Border);
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _selectBoardLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_SELECT_BOARD_TYPE_PROMPT_KEY") + ":  ");
    _xPositionLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_X_POSITION_KEY") + ":  ");
    _yPositionLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_Y_POSITION_KEY") + ":  ");
    _buttonsPanel.setLayout(_buttonsPanelFlowLayout);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed();
      }
    });
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed();
      }
    });
    _selectionsPanel.setLayout(_selectionsPanelPairLayout);
    _selectionsPanel.setBorder(_empty5Border);
    getContentPane().setLayout(_mainBorderLayout);
    _innerButtonsPanel.setLayout(_innerButtonsPanelGridLayout);
    _innerButtonsPanelGridLayout.setHgap(10);
    _buttonsPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _buttonsPanelFlowLayout.setVgap(0);
    _mainPanelBorderLayout.setVgap(10);
    _buttonsPanel.add(_innerButtonsPanel, null);
    _innerButtonsPanel.add(_okButton, null);
    _innerButtonsPanel.add(_cancelButton, null);
    if (_project.getPanel().getBoardTypes().size() > 1) // if more than 1 use a combo box
    {
      _selectionsPanel.add(_selectBoardLabel);
      _selectionsPanel.add(_boardComboBox, null);
    }
    _selectionsPanel.add(_xPositionLabel);
    _selectionsPanel.add(_xPositionTextField);
    _selectionsPanel.add(_yPositionLabel);
    _selectionsPanel.add(_yPositionTextField);
    _mainPanel.add(_buttonsPanel, BorderLayout.SOUTH);
    _mainPanel.add(_selectionsPanel, BorderLayout.CENTER);
    _xPositionTextField.setDocument(_xPosDocument);
    _yPositionTextField.setDocument(_yPosDocument);
    _xPositionTextField.addFocusListener(_textFieldFocusAdapter);
    _yPositionTextField.addFocusListener(_textFieldFocusAdapter);
    updateTextFieldDecimalFormats();
    getContentPane().add(_mainPanel, BorderLayout.CENTER);
    getRootPane().setDefaultButton(_okButton);
    _xPositionTextField.setColumns(6);
    _yPositionTextField.setColumns(6);
    _xPosDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _yPosDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _xPositionTextField.setValue(new Double(0.0));
    _yPositionTextField.setValue(new Double(0.0));

    populateBoardTypes();
    pack();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateTextFieldDecimalFormats()
   {
     StringBuffer decimalFormat = new StringBuffer("########0.");
     MathUtilEnum unitsEnum = _project.getDisplayUnits();

     int decimalPlaces = MeasurementUnits.getMeasurementUnitDecimalPlaces(unitsEnum);
     for(int i = 0; i < decimalPlaces; i++)
       decimalFormat.append("0");  // use a "0" if you want to show trailing zeros, use a "#" if you don't

     _xPositionTextField.setFormat(new DecimalFormat(decimalFormat.toString()));
     _yPositionTextField.setFormat(new DecimalFormat(decimalFormat.toString()));
  }

  /**
   * Initialize the board type combo box with the board types from the project
   * @author Carli Connally
   */
  private void populateBoardTypes()
  {
    java.util.List<BoardType> boardTypes = _project.getPanel().getBoardTypes();
    for(BoardType boardType : boardTypes)
    {
      _boardComboBox.addItem(boardType);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleTextFieldData(NumericTextField numericTextField)
  {
    Assert.expect(numericTextField != null);

    Double pos;
    try
    {
      pos = numericTextField.getDoubleValue();
    }
    catch (ParseException ex)
    {
      // this typically happens when the user deletes all characters from the field -- same as zero
      pos = new Double(0.0);
    }
    numericTextField.setValue(pos);
  }

  /**
   * Adds the specified board to the panel.  Does appropriate checks first to make
   * sure that the specified information is valid.  Closes the dialog.
   * @author Carli Connally
   */
  private void okButton_actionPerformed()
  { 
    //XCR1725 - Customizable serial number mapping
    //Siew Yeng - prompt to clear serial number mapping if barcode reader is enabled
    if(BarcodeReaderManager.getInstance().getBarcodeReaderAutomaticReaderEnabled() &&
      SerialNumberMappingManager.getInstance().isSerialNumberMappingFileExists())
    {
      int status = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(), 
                                                 StringLocalizer.keyToString("MMGUI_CLEAR_SERIAL_NUMBER_MAPPING_WARNING_MESSAGE_KEY"),
                                                 StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), 
                                                 JOptionPane.YES_NO_OPTION, 
                                                 JOptionPane.QUESTION_MESSAGE);  
      if(status == JOptionPane.NO_OPTION)
      {
        dispose();
        return;
      }
    }
    
    try
    {
      MathUtilEnum unitsEnum = _project.getDisplayUnits();
      BoardType boardTypeToAdd = (BoardType)_boardComboBox.getSelectedItem();
      Double xPos = _xPositionTextField.getDoubleValue();
      Double yPos = _yPositionTextField.getDoubleValue();
      int xPosInNanoMeters = (int)MathUtil.convertUnits(xPos.doubleValue(), unitsEnum, MathUtilEnum.NANOMETERS);
      int yPosInNanoMeters = (int)MathUtil.convertUnits(yPos.doubleValue(), unitsEnum, MathUtilEnum.NANOMETERS);
      try
      {
        com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_CREATE_BOARD_KEY"));
        com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().execute(new CreateBoardCommand(_project.getPanel(),
                                                                           boardTypeToAdd,
                                                                           new PanelCoordinate(xPosInNanoMeters, yPosInNanoMeters),
                                                                           0,
                                                                           false));
        
        //Siew Yeng - reset serial number mapping
        if(BarcodeReaderManager.getInstance().getBarcodeReaderAutomaticReaderEnabled() &&
          SerialNumberMappingManager.getInstance().isSerialNumberMappingFileExists())
        {
          com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().execute(new EditSerialNumberMappingCommand(SerialNumberMappingManager.getInstance().getDefaultSerialNumberMappingDataList()));
          com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().execute(new EditSerialNumberMappingBarcodeConfigNameCommand(BarcodeReaderManager.getInstance().getBarcodeReaderConfigurationNameToUse()));
        }
        
        com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance().endCommandBlock();
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
      }
    }
    catch (ParseException ex)
    {
      Assert.expect(false, ex.getMessage());
    }
    dispose();
  }

  /**
   * User presses the cancel button.  The operation is cancelled.  No board is added
   * to the panel.  The state of the panel remains the same as when the dialog was opened.
   * @author Carli Connally
   */
  private void cancelButton_actionPerformed()
  {
    dispose();
  }
}
