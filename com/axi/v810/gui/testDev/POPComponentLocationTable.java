package com.axi.v810.gui.testDev;

import javax.swing.*;
import javax.swing.table.*;
import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;


/**
 * @author Khaw Chek Hau
 * POP Development
 */
public class POPComponentLocationTable extends JSortTable
{
  public static final int _COMPONENT_TYPE_INDEX = 0;
  public static final int _LAYER_ID_INDEX = 1;
  public static final int _POP_ID_INDEX = 2;
  public static final int _ZHEIGHT_POP_INDEX = 3;  
  private static final int _COMPONENT_UNDO_INDEX = 0;
  private final double _COMPONENT_TYPE_COLUMN_WIDTH_PERCENTAGE = 0.1;
  private final double _LAYER_ID_COLUMN_WIDTH_PERCENTAGE = 0.1;
  private final double _POP_ID_COLUMN_WIDTH_PERCENTAGE = 0.1;
  private final double _ZHEIGHT_POP_COLUMN_WIDTH_PERCENTAGE = 0.7;
  private NumericRenderer _zHeightRenderer = new NumericRenderer(3, JLabel.RIGHT);
  private NumericEditor _zHeightEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 3, 0.0, Double.MAX_VALUE);
  private POPComponentLocationTableModel _popComponentLocationTableModel;
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public POPComponentLocationTable(POPComponentLocationTableModel model)
  {
    Assert.expect(model != null);

    super.setModel(model);
    
    _popComponentLocationTableModel = model;
   
    jbInit();
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void jbInit()
  {
    setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    setSurrendersFocusOnKeystroke(false);
    sizeColumnsToFit(-1);
    setEnabled(true);
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();
 
    columnModel.getColumn(_COMPONENT_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _COMPONENT_TYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_LAYER_ID_INDEX).setPreferredWidth((int)(totalColumnWidth * _LAYER_ID_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_POP_ID_INDEX).setPreferredWidth((int)(totalColumnWidth * _POP_ID_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_ZHEIGHT_POP_INDEX).setPreferredWidth((int)(totalColumnWidth * _ZHEIGHT_POP_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void setModel(POPComponentLocationTableModel componentTableModel)
  {
    Assert.expect(componentTableModel != null);

    _popComponentLocationTableModel = componentTableModel;
    super.setModel(_popComponentLocationTableModel);
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public POPComponentLocationTableModel getTableModel()
  {
    Assert.expect(_popComponentLocationTableModel != null);
    return _popComponentLocationTableModel;
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void setupCellEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();   
    
    columnModel.getColumn(_ZHEIGHT_POP_INDEX).setCellEditor(_zHeightEditor);
    columnModel.getColumn(_ZHEIGHT_POP_INDEX).setCellRenderer(_zHeightRenderer);
    
    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return false; // allows editing with a single click (the default is a double click)
      }
    };
    columnModel.getColumn(_POP_ID_INDEX).setCellEditor(stringEditor);
    columnModel.getColumn(_COMPONENT_TYPE_INDEX).setCellEditor(stringEditor);
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void setValueAt(Object value, int row, int column)
  {
    // if there is no value, exit out here -- there is no work to do
    if (value == null)
      return;
    popComponentLocationTableSetValues(value, row, column);
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void popComponentLocationTableSetValues(Object value, int row, int column)
  {
    int rows[] = getSelectedRows();
    int col = convertColumnIndexToModel(column);

    for(int i = 0; i < rows.length; i++)
    {
       _popComponentLocationTableModel.setValueAt(value, rows[i], col);
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void setDecimalPlacesToDisplay(int decimalPlaces)
  {
    Assert.expect(decimalPlaces >= 0);

    _zHeightEditor.setDecimalPlaces(decimalPlaces);
    _zHeightRenderer.setDecimalPlacesToDisplay(decimalPlaces);
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  void cancelCellEditing()
  {
    cancelCellEditingForColumn(_COMPONENT_TYPE_INDEX);
    cancelCellEditingForColumn(_LAYER_ID_INDEX);
    cancelCellEditingForColumn(_POP_ID_INDEX);
    cancelCellEditingForColumn(_ZHEIGHT_POP_INDEX);
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  void cancelCellEditingForColumn(int column)
  {
    TableCellEditor cellEditor = getColumnModel().getColumn(column).getCellEditor();
    if (cellEditor != null)
    {
      cellEditor.cancelCellEditing();
    }
  }
}
