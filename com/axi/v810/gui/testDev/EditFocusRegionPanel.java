package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;
/**
 *
 * @author chin-seong.kee
 */
public class EditFocusRegionPanel extends JPanel implements Observer
{
  private JPanel _mainPanel = new JPanel();
  private JPanel _inputXYPanel = new JPanel();
  private JPanel _inputWidthHeight = new JPanel();
  private JPanel _inputMainPanel = new JPanel();
  private JPanel _mainInputPanel = new JPanel();
  private JPanel _inputRRegionIdPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _pitchOffsetPanel = new JPanel();
  private JPanel _xyInnerOffsetPanel = new JPanel();
  private JPanel _xyOuterOffsetPanel = new JPanel();
  private JPanel _inputPitchInnerPanel = new JPanel();
  private JPanel _inputDragFocusRegionPanel = new JPanel();

  private JLabel _inputRRegionIdLabel = new JLabel(StringLocalizer.keyToString("GUI_PAD_RANGE_KEY"));
  private JComboBox _reconstructionRegionIdComboBox = new JComboBox();
  
  private BorderLayout _selectReconstructionRegionsBorderLayout = new BorderLayout();
  private BorderLayout _inputXYBorderLayout = new BorderLayout();
  private PairLayout _inputWidthHeightBorderLayout = new PairLayout();
  private BorderLayout _inputMainBorderLayout = new BorderLayout();
  private BorderLayout _mainInputBorderLayout = new BorderLayout();
  
  private JTextField _inputOffsetX = new JTextField("", 40);
  private JTextField _inputOffsetY = new JTextField("", 40);
  private JTextField _inputOffsetXByPitchTxtBox = new JTextField("0", 40);
  private JTextField _inputOffsetYByPitchTxtBox = new JTextField("0", 40);
  
  private JRadioButton _inputOffsetByCoordRadioButton = new JRadioButton(StringLocalizer.keyToString("GUI_XY_COORDINATE_OFFSET_KEY"));
  private JRadioButton _inputOffsetByPitchRadioButton = new JRadioButton(StringLocalizer.keyToString("GUI_PITCH_OFFSET_KEY"));
  private JRadioButton _inputOffsetByDragFocusRegionRadioButton = new JRadioButton(StringLocalizer.keyToString("GUI_DRAG_FOCUS_REGION_KEY"));
  
  private JLabel _inputOffsetXLabel = new JLabel(StringLocalizer.keyToString("GUI_OFFSET_X_KEY"));
  private JLabel _inputOffsetYLabel = new JLabel(StringLocalizer.keyToString("GUI_OFFSET_Y_KEY"));
  private JLabel _pitchNumberforComponent = new JLabel();
  private JLabel _pitchOffsetXLabel = new JLabel(StringLocalizer.keyToString("GUI_OFFSET_PITCH_X_KEY"));
  private JLabel _pitchOffsetYLabel = new JLabel(StringLocalizer.keyToString("GUI_OFFSET_PITCH_Y_KEY"));
  
  private JTextField _inputWidth = new JTextField("", 40);
  private JTextField _inputHeight = new JTextField("", 40);
  
  private JLabel _inputOffsetWidthLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SETUP_X_DIMENSION_KEY"));
  private JLabel _inputOffsetHeightLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SETUP_Y_DIMENSION_KEY"));
  
  private JCheckBox _showReconstructionRegionAndFocusRegionGraphics = new JCheckBox(StringLocalizer.keyToString("GUI_SHOW_GRAPHIC_KEY"));
  private JButton _okButton = new JButton(StringLocalizer.keyToString("GUI_APPLY_KEY"));
  
  private ButtonGroup _offsetButtonGroup = new ButtonGroup();
  
  private java.util.List<ReconstructionRegion> _selectedRRegion = new ArrayList<ReconstructionRegion>(); 
  
  MainMenuGui _mainUI = null;
  
  private JPanel _packageJointTypePanel = new JPanel();
  private JSplitPane _packageJointTypeSplitPane = new JSplitPane();
  private PackageJointTypeTableModel _packageJointTypeTableModel;
  private PackageJointTypeTable _packageJointTypeTable;
  private ListSelectionListener _packageJointTypeTableListSelectionListener;
  private JScrollPane _packageJointTypeScrollPane = new JScrollPane();
  //private JPanel _packageJointTypeControlPanel = new JPanel();
  
  private boolean _ignoreActions = false; 
  
  private JPanel _componentLocationPanel = new JPanel();
  private ComponentLocationTableModel _componentLocationTableModel;
  private ComponentLocationTable _componentLocationTable;
  private ListSelectionListener _componentLocationTableListSelectionListener;
  private JScrollPane _componentLocationScrollPane = new JScrollPane();
  //private JPanel _componentLocationControlPanel = new JPanel();
  
  private JLabel _pabelBoardLabel = new JLabel(StringLocalizer.keyToString("ATGUI_PANEL_BOARD_KEY"));
  private JComboBox _panelBoardComboBox = new JComboBox();
  
  // all the board instances for the current board type
  private java.util.ArrayList<Board> _sortedBoardList = null;
  
   // the current board type
  private BoardType _currentBoardType = null;
  // the current board instance
  private Board _currentBoard = null;
  // the current board name
  private String _currentBoardName = null;
  private JComboBox _sideComboBox = new JComboBox();
  // the current board side
  private String _currentSide = null;
  
  private ComponentType _currentComponentSelectedInGraphics = null;
  private Board _currentBoardSelectedInGraphics = null;
  // the currently selected component
  private ComponentType _currentComponent = null;
  private String _currentComponentName = null;
  
  private int _currentComponentIndex = -1;
  // the currently selected package if using package view
  private int _currentPackageIndex = -1;
  
  // side choices
  private final String _TOP_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_TOP_LABEL_KEY");
  private final String _BOTTOM_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_BOTTOM_LABEL_KEY");
  private final String _BOTH_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_BOTH_LABEL_KEY");
  
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  
  private ActionListener _rridComboBoxActionListener;
  private ActionListener _panelBoardPopulateActionListener;
  
  private JLabel _inputRegionSiezeLbl = new JLabel(StringLocalizer.keyToString("GUI_INPUT_REGION_SIZE_KEY"));
  private JTextField _inputRegionSizeTxtField = new JTextField();
  private JButton _regenerateRRButton = new JButton(StringLocalizer.keyToString("GUI_REGENERATE_RR_KEY"));
  
  private JPanel _mainRegenerateRRPanel = new JPanel();
  private JPanel _inputRegenerateRRPanel = new JPanel();
  private JPanel _regenerateInputButtonPanel = new JPanel();
  private JPanel _innerLeftInputRRegionIdPanel = new JPanel();
  private JPanel _innerRightInputRRegionIdPanel = new JPanel();
  private boolean _isStarted = false;
  
  /*
   * @author Kee Chin Seong
   */
  public EditFocusRegionPanel(MainMenuGui frame)
  {
    try
    {
      _mainUI = frame;
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Kee Chin Seong
   */
  public EditFocusRegionPanel()
  {  
    try
    {
       jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    //pack();
  }
  
  public void populateTheRRIDByRefDEs(String compRefDes)
  {
    try
    {
      _reconstructionRegionIdComboBox.removeActionListener(_rridComboBoxActionListener);
      if (compRefDes.matches("") == false)
      {
        if (_reconstructionRegionIdComboBox.getItemCount() > 0)
        {
          _reconstructionRegionIdComboBox.removeAllItems();
          _selectedRRegion.clear();
        }
        for (TestSubProgram tsp : Project.getCurrentlyLoadedProject().getTestProgram().getAllTestSubPrograms())
        {
          if (tsp.hasInspectionRegions() == false)
          {
            continue;
          }

          java.util.List<ReconstructionRegion> rRegion = tsp.getInspectionRegions(compRefDes);

          if (rRegion != null)
          {
            for (ReconstructionRegion rr : rRegion)
            {
              if (rr.getComponent().getBoard().equals(_currentBoard) == true)
              {
                String padRange = "";
                java.util.List<Pad> pads = new LinkedList<Pad>(rr.getPads());
                if (pads.size() > 1)
                {
                  int count = 0;
                  Collections.sort(pads, new PadComparator());
                  for (Pad pad : pads)
                  {
                    if (count == 0)
                    {
                      padRange = padRange + pad.getName() + " - ";
                    }
                    else if (count == rr.getPads().size() - 1)
                    {
                      padRange = padRange + pad.getName();
                    }
                    count++;
                  }
                }
                else
                {
                  padRange = pads.get(0).getName();
                }

                _reconstructionRegionIdComboBox.addItem(padRange);
                _selectedRRegion.add(rr);
              }
              //offsetFocusRegion(tsp.getAllFocusRegion(rr), rr);
            }
          }
        }

        if (_selectedRRegion.isEmpty() == false)
        {
          //always get 0 is doent matter anyway, as we only need the component land pattern instead.
          _pitchNumberforComponent.setText("Pitch                : " + _selectedRRegion.get(0).getComponent().getLandPattern().getLandPatternPadOne().getPitchInNanoMeters());

          offsetFocusRegion(_selectedRRegion.get(0), false);
        }
      }
    }
    finally
    {
      if (_reconstructionRegionIdComboBox.getItemCount() > 0)
        _reconstructionRegionIdComboBox.addActionListener(_rridComboBoxActionListener);
    }
  }

  /**
   * @author Kee Chin Seong
   */
  private void jbInit() throws Exception
  {
    _ignoreActions = true;
    //setTitle("Edit Focus Region Dialog Box");
    _mainPanel.setLayout(_selectReconstructionRegionsBorderLayout);
    _inputXYPanel.setLayout(_inputXYBorderLayout);
    _inputWidthHeight.setLayout(_inputWidthHeightBorderLayout);
    _inputMainPanel.setLayout(_inputMainBorderLayout);
    _mainInputPanel.setLayout(_mainInputBorderLayout);

    _inputRRegionIdPanel.setLayout(new PairLayout());
    _buttonPanel.setLayout(new BorderLayout());
    _inputPitchInnerPanel.setLayout(new PairLayout());
    _xyInnerOffsetPanel.setLayout(new PairLayout());
    _xyOuterOffsetPanel.setLayout(new BorderLayout());
    _pitchOffsetPanel.setLayout(new BorderLayout());
    _inputDragFocusRegionPanel.setLayout(new BorderLayout());
    _innerLeftInputRRegionIdPanel.setLayout(new PairLayout());
    _innerRightInputRRegionIdPanel.setLayout(new PairLayout());
  
    _mainRegenerateRRPanel.setLayout(new BorderLayout());
    _inputRegenerateRRPanel.setLayout(new PairLayout(10, 10, true));
    _regenerateInputButtonPanel.setLayout(new BorderLayout());
    
    _inputRegenerateRRPanel.add(_inputRegionSiezeLbl);
    _inputRegenerateRRPanel.add(_inputRegionSizeTxtField);
    _regenerateInputButtonPanel.add(_regenerateRRButton, BorderLayout.NORTH);
    
    _mainRegenerateRRPanel.add(_inputRegenerateRRPanel, BorderLayout.NORTH);
    _mainRegenerateRRPanel.add(_regenerateInputButtonPanel, BorderLayout.SOUTH);
    
    _mainRegenerateRRPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), StringLocalizer.keyToString("GUI_RECONSTRUCTION_REGION_KEY")));
    
    //adding in the chdck box
    _offsetButtonGroup.add(_inputOffsetByCoordRadioButton);
    _offsetButtonGroup.add(_inputOffsetByPitchRadioButton);
    _offsetButtonGroup.add(_inputOffsetByDragFocusRegionRadioButton);
   
    //outer panel for pitch offset
    _pitchOffsetPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), StringLocalizer.keyToString("GUI_PITCH_OFFSET_KEY")));
    _pitchOffsetPanel.add(_inputOffsetByPitchRadioButton, BorderLayout.NORTH);
    _pitchOffsetPanel.add(_pitchNumberforComponent, BorderLayout.CENTER);
    _pitchOffsetPanel.add(_inputPitchInnerPanel, BorderLayout.SOUTH);
    
    //inner panel for pitch offset
    _inputPitchInnerPanel.add(_pitchOffsetXLabel);
    _inputPitchInnerPanel.add(_inputOffsetXByPitchTxtBox);
    _inputPitchInnerPanel.add(_pitchOffsetYLabel);
    _inputPitchInnerPanel.add(_inputOffsetYByPitchTxtBox);
    
    //outer panel for XY offset
    _xyOuterOffsetPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), StringLocalizer.keyToString("GUI_XY_COORDINATE_OFFSET_KEY")));
    _xyOuterOffsetPanel.add(_inputOffsetByCoordRadioButton, BorderLayout.NORTH);
    _xyOuterOffsetPanel.add(_xyInnerOffsetPanel, BorderLayout.CENTER);
    
    _xyInnerOffsetPanel.add(_inputOffsetXLabel);
    _xyInnerOffsetPanel.add(_inputOffsetX);
    _xyInnerOffsetPanel.add(_inputOffsetYLabel);
    _xyInnerOffsetPanel.add(_inputOffsetY);
    
    _inputDragFocusRegionPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), StringLocalizer.keyToString("GUI_DRAG_REGION_RECTANGLE_KEY")));
    _inputDragFocusRegionPanel.add(_inputOffsetByDragFocusRegionRadioButton, BorderLayout.NORTH);
    
    //_inputXYPanel.add(_pitchNumberforComponent, BorderLayout.NORTH);
    _inputXYPanel.add(_pitchOffsetPanel, BorderLayout.NORTH);
    _inputXYPanel.add(_xyOuterOffsetPanel, BorderLayout.CENTER);
    _inputXYPanel.add(_inputDragFocusRegionPanel, BorderLayout.SOUTH);
    
    _inputWidthHeight.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), StringLocalizer.keyToString("GUI_WIDTH_HEIGHT_KEY")));
    _inputWidthHeight.add(_inputOffsetWidthLabel);
    _inputWidthHeight.add(_inputWidth);
    _inputWidthHeight.add(_inputOffsetHeightLabel);
    _inputWidthHeight.add(_inputHeight);
    
    _mainInputPanel.add(_inputXYPanel, BorderLayout.NORTH);
    _mainInputPanel.add(_inputWidthHeight, BorderLayout.CENTER);
    _mainInputPanel.add(_inputMainPanel, BorderLayout.SOUTH);
    
    _inputMainPanel.add(_packageJointTypePanel, BorderLayout.NORTH);
    _inputMainPanel.add(_componentLocationPanel, BorderLayout.CENTER);
    // XCRxxxx - Custom Focus Region
    // by Seng Yew on 27-Jan-2014
    // Temporarily disable this feature because not fully tested.
//    _inputMainPanel.add(_mainRegenerateRRPanel, BorderLayout.SOUTH);
    
    _innerLeftInputRRegionIdPanel.add(_inputRRegionIdLabel);
    _innerLeftInputRRegionIdPanel.add(_reconstructionRegionIdComboBox);
    
    _innerRightInputRRegionIdPanel.add(_pabelBoardLabel);
    _innerRightInputRRegionIdPanel.add(_panelBoardComboBox);

    _inputRRegionIdPanel.add(_innerLeftInputRRegionIdPanel);
    _inputRRegionIdPanel.add(_innerRightInputRRegionIdPanel);
    
    _buttonPanel.add(_showReconstructionRegionAndFocusRegionGraphics, BorderLayout.WEST);
    _buttonPanel.add(_okButton, BorderLayout.CENTER);
    
    _mainPanel.add(_inputRRegionIdPanel, BorderLayout.NORTH);
    _mainPanel.add(_mainInputPanel, BorderLayout.CENTER);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
  
    _sideComboBox.setToolTipText(StringLocalizer.keyToString("CAD_BOARD_SIDE_TOOLTIP_KEY"));
    _sideComboBox.addItem(_TOP_SIDE);
    _sideComboBox.addItem(_BOTTOM_SIDE);
    _sideComboBox.addItem(_BOTH_SIDE);
     
    _currentSide = (String)_sideComboBox.getSelectedItem();
    _sideComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        sideComboBox_actionPerformed();
      }
    });
    
    _panelBoardPopulateActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        panelBoardComboBox_actionPerformed();
      }
    };
    

    _inputOffsetByPitchRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enablePitchOffsetPanel(e);
      }
    });
    _inputOffsetByPitchRadioButton.setSelected(true); //initial is by pitch
    
    _inputOffsetByCoordRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableXYOffsetPanel(e);
      }
    });
    
    _showReconstructionRegionAndFocusRegionGraphics.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showGraphics_actionPerformed(e);
      }
    });
    
            
    _rridComboBoxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        offsetFocusRegion(_selectedRRegion.get(_reconstructionRegionIdComboBox.getSelectedIndex()), false);
        showGraphics_actionPerformed(e);
      }
    };
    
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    
    _inputOffsetByDragFocusRegionRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dragFocusRegionModeEnabled(e);
      }
    });
    
    _regenerateRRButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        regenerateReconstructionRegionBasedOnTheDefinedClusteringSize();
      }
    });
    
    _packageJointTypeTableModel = new PackageJointTypeTableModel();
    _packageJointTypeTable = new PackageJointTypeTable(_packageJointTypeTableModel)
    {
      /**
       * Overriding this method allows us to set up the undo manager
       * @author Kee Chin Seong
       */
      public void setValueAt(Object value, int row, int column)
      {
       // packageJointTypeTableSetValue(value, row, column);
      }
      /**
       * Overriding this method allows us to make sure the selected rows
       * are still the proper selected rows after a sort.
       * @author Kee Chin Seong
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = _packageJointTypeTable.getSelectedRows();
        java.util.List<CompPackage> selectedData = new ArrayList<CompPackage>();
        for (int row = 0; row < selectedRows.length; row++)
        {
          selectedData.add(_packageJointTypeTableModel.getPackageAt(selectedRows[row]));
        }
        super.mouseReleased(event);
        // now, reselect everything
        _packageJointTypeTable.clearSelection();
        for (CompPackage compPack : selectedData)
        {
          int row = _packageJointTypeTableModel.getRowForCompPackage(compPack);
          _packageJointTypeTable.addRowSelectionInterval(row, row);
        }
      }
    };
    _packageJointTypeTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        packageJointTypeTable_valueChanged(e);
        //highlightBoardAndComponent();
      }
    };
    ListSelectionModel packageJointTypeSM = _packageJointTypeTable.getSelectionModel();
    packageJointTypeSM.addListSelectionListener(_packageJointTypeTableListSelectionListener);
    _packageJointTypeScrollPane.getViewport().add(_packageJointTypeTable);
    _packageJointTypeScrollPane.setMinimumSize(new Dimension(5,5));
    _packageJointTypeScrollPane.setPreferredSize(new Dimension(150, 150));
    
    // Create the panels that are swapped into the center panel for different views
    // Setup the package split pane. Table scroll panes are added when needed.
    _packageJointTypePanel.setLayout(new BorderLayout());
    _packageJointTypePanel.add(_packageJointTypeScrollPane, BorderLayout.CENTER);
    
    // create all tables and table listeners that will be used in verify cad
    _componentLocationTableModel = new ComponentLocationTableModel();
    _componentLocationTable = new ComponentLocationTable(this, _componentLocationTableModel)
    {
      /**
       * Overriding this method allows us to set up the undo manager
       * @author Kee Chin Seong
       */
      public void setValueAt(Object value, int row, int column)
      {
        //componentLocationTableSetValue(value, row, column);
      }
      /**
       * Overriding this method allows us to make sure the selected rows
       * are still the proper selected rows after a sort.
       * @author Kee Chin Seong
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = _componentLocationTable.getSelectedRows();
        java.util.List<ComponentType> selectedData = new ArrayList<ComponentType>();
        for (int row = 0; row < selectedRows.length; row++)
        {
          selectedData.add(_componentLocationTableModel.getComponentTypeAt(selectedRows[row]));
        }
        super.mouseReleased(event);
        // now, reselect everything
        _componentLocationTable.clearSelection();
        for (ComponentType compType : selectedData)
        {
          int row = _componentLocationTableModel.getRowForComponentType(compType);
          _componentLocationTable.addRowSelectionInterval(row, row);
        }
      }
    };
    _componentLocationTable.setPreferredColumnWidths();
    _componentLocationTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        componentLocationTable_valueChanged(e);
      }
    };
    
    ListSelectionModel componentLocationTableSM = _componentLocationTable.getSelectionModel();
    componentLocationTableSM.addListSelectionListener(_componentLocationTableListSelectionListener);
    _componentLocationScrollPane.getViewport().add(_componentLocationTable);
    _componentLocationScrollPane.setMinimumSize(new Dimension(5,5));
    _componentLocationScrollPane.setPreferredSize(new Dimension(150, 150));
    
    _componentLocationPanel.setLayout(new BorderLayout());
    _componentLocationPanel.add(_componentLocationScrollPane, BorderLayout.CENTER);
    
    // add the package family and component table panels
    _packageJointTypeSplitPane = new JSplitPane(
      JSplitPane.VERTICAL_SPLIT,
      _packageJointTypePanel,
      _componentLocationPanel);
    
    _packageJointTypeSplitPane.setResizeWeight(0.50);
    
    _inputMainPanel.add(_packageJointTypeSplitPane, BorderLayout.CENTER);
    // make sure selected rows are visible
    SwingUtils.scrollTableToShowSelection(_packageJointTypeTable);
    SwingUtils.scrollTableToShowSelection(_componentLocationTable);

    validate();
    repaint();
    
    _ignoreActions = false; 
    
    this.add(_mainPanel);
  }
  
  /**
 *
 * @author chin-seong.kee
 */
  private void dragFocusRegionModeEnabled(ActionEvent e)
  {
     _mainUI.getTestDev().getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectRegionMode(_inputOffsetByDragFocusRegionRadioButton.isSelected());
     
     _inputOffsetX.setEnabled(false);
     _inputOffsetY.setEnabled(false);
     _inputOffsetXByPitchTxtBox.setEnabled(false);
     _inputOffsetYByPitchTxtBox.setEnabled(false);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void okButton_actionPerformed(ActionEvent e)
  {
    if(Double.parseDouble(_inputWidth.getText()) < 0 || Double.parseDouble(_inputHeight.getText()) < 0)
    {
       JOptionPane.showMessageDialog(_mainUI,
                   StringLocalizer.keyToString("MMGUI_WIDTH_HEIGHT_IS_NEGATIVE_WARNING_MESSAGE_KEY"),
                   StringLocalizer.keyToString("MMGUI_EDIT_FOCUS_REGION_TITLE_KEY"),
                   JOptionPane.WARNING_MESSAGE); 
       return;
    }
    
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
            "Calculating Focus Region offset for component " + _selectedRRegion.get(_reconstructionRegionIdComboBox.getSelectedIndex()).getComponent().getReferenceDesignator() +
            " for RRID " + _selectedRRegion.get(_reconstructionRegionIdComboBox.getSelectedIndex()).getName(),
            "Calculate Focus Region Offset",
            true);

    SwingUtils.setFont(busyCancelDialog, com.axi.guiUtil.FontUtil.getDefaultFont());
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
    SwingUtils.invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          
          Project.getCurrentlyLoadedProject().getTestProgram();
          offsetFocusRegion(_selectedRRegion.get(_reconstructionRegionIdComboBox.getSelectedIndex()), true);
          _selectedRRegion.get(_reconstructionRegionIdComboBox.getSelectedIndex()).getComponent().setUseCustomFocusRegion(true);
          
          Project.getCurrentlyLoadedProject().saveFocusRegionIntoMap(_selectedRRegion.get(_reconstructionRegionIdComboBox.getSelectedIndex()).getComponent(), 
                                                                                      _selectedRRegion);
          try
          {
            // now lets sleep for 2 seconds so that we can make sure the dialog doesn't just flash.
            Thread.sleep(1000);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }

        }
        finally
        {
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.setVisible(false);
        }
      }
    });
    busyCancelDialog.setVisible(true);
    populatingGraphicsEngine();
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void offsetFocusRegion(ReconstructionRegion inspectionRegion, boolean isNeedOverWrite)
  {
     try
     {
        for(FocusGroup fg : inspectionRegion.getFocusGroups())
        {
          double x = fg.getFocusSearchParameters().getFocusRegion().getRectangleRelativeToPanelInNanometers().getRectangle2D().getX();//getBounds2D().getX();
          double y = fg.getFocusSearchParameters().getFocusRegion().getRectangleRelativeToPanelInNanometers().getRectangle2D().getY();//getBounds2D().getY();

          if(isNeedOverWrite == true)
          {
            if(_inputOffsetByPitchRadioButton.isSelected() == true)
            {
               fg.getFocusSearchParameters().getFocusRegion().setRectangleRelativeToPanelInNanoMeters(new PanelRectangle((int) x + (int)(inspectionRegion.getComponent().getLandPattern().getLandPatternPadOne().getPitchInNanoMeters()* Double.parseDouble(_inputOffsetXByPitchTxtBox.getText())), 
                                                                                                                            (int)y + (int)(inspectionRegion.getComponent().getLandPattern().getLandPatternPadOne().getPitchInNanoMeters()* Double.parseDouble(_inputOffsetYByPitchTxtBox.getText())),
                                                                                                                            (int)(Double.parseDouble(_inputWidth.getText())),  
                                                                                                                            (int)(Double.parseDouble(_inputHeight.getText()))));
            }
            else if(_inputOffsetByCoordRadioButton.isSelected() == true)
            {
               fg.getFocusSearchParameters().getFocusRegion().setRectangleRelativeToPanelInNanoMeters(new PanelRectangle((int)(Double.parseDouble(_inputOffsetX.getText())), 
                                                                                                                         (int)(Double.parseDouble(_inputOffsetY.getText())),
                                                                                                                         (int)(Double.parseDouble(_inputWidth.getText())),  
                                                                                                                         (int)(Double.parseDouble(_inputHeight.getText()))));
            }
            else
            {
               if(_mainUI.getTestDev().getPanelGraphicsEngineSetup().getSelectedRegion() == null)
               {
                   JOptionPane.showMessageDialog(this,
                   StringLocalizer.keyToString("MMGUI_DRAG_REGION_WARNING_MESSAGE_KEY"),
                   StringLocalizer.keyToString("MMGUI_EDIT_FOCUS_REGION_TITLE_KEY"),
                   JOptionPane.WARNING_MESSAGE);
               }
               else
               {
                   fg.getFocusSearchParameters().getFocusRegion().setRectangleRelativeToPanelInNanoMeters(new PanelRectangle((int)(_mainUI.getTestDev().getPanelGraphicsEngineSetup().getSelectedRegion().getBounds2D().getX()), 
                                                                                                                         (int)(_mainUI.getTestDev().getPanelGraphicsEngineSetup().getSelectedRegion().getBounds2D().getY()),
                                                                                                                         (int)(_mainUI.getTestDev().getPanelGraphicsEngineSetup().getSelectedRegion().getBounds2D().getWidth()),  
                                                                                                                         (int)(_mainUI.getTestDev().getPanelGraphicsEngineSetup().getSelectedRegion().getBounds2D().getHeight())));
                   _mainUI.getTestDev().getPanelGraphicsEngineSetup().clearFocusRegion();

                  _inputWidth.setText("" + _mainUI.getTestDev().getPanelGraphicsEngineSetup().getSelectedRegion().getBounds2D().getWidth());
                  _inputHeight.setText("" + _mainUI.getTestDev().getPanelGraphicsEngineSetup().getSelectedRegion().getBounds2D().getHeight());
                  _inputOffsetX.setText("" + (int)_mainUI.getTestDev().getPanelGraphicsEngineSetup().getSelectedRegion().getBounds2D().getX());
                  _inputOffsetY.setText("" + (int)_mainUI.getTestDev().getPanelGraphicsEngineSetup().getSelectedRegion().getBounds2D().getY());
               }
            }
          }
          else
          {
            double width = fg.getFocusSearchParameters().getFocusRegion().getRectangleRelativeToPanelInNanometers().getRectangle2D().getBounds2D().getWidth();
            double height = fg.getFocusSearchParameters().getFocusRegion().getRectangleRelativeToPanelInNanometers().getRectangle2D().getBounds2D().getHeight();

            _inputWidth.setText("" + width);
            _inputHeight.setText("" + height);
            _inputOffsetX.setText("" + (int)x);
            _inputOffsetY.setText("" + (int)y);
          }
        }
     }
     catch(final NumberFormatException ex)
     {
       SwingUtils.invokeLater(new Runnable()
       {

            public void run()
            {
              MessageDialog.showErrorDialog(_mainUI,
                      ex.getMessage() + "is not integer",
                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                      true);
            }
        });
     }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void regenerateReconstructionRegionBasedOnTheDefinedClusteringSize()
  {
    //ReconstructionRegion.setDefinedInspectionRegionSizeInNanoMeters(Integer.parseInt(_inputRegionSizeTxtField.getText()));
    
    regenerateTestProgram();
    populatingGraphicsEngine();
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void populatingGraphicsEngine()
  {
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
            StringLocalizer.keyToString("MMGUI_POPULATING_GRAPHICS_ENGINE_KEY"),
            StringLocalizer.keyToString("MMGUI_POPULATING_GRAPHICS_ENGINE_KEY"),
            true);

    SwingUtils.setFont(busyCancelDialog, com.axi.guiUtil.FontUtil.getDefaultFont());
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
    SwingUtils.invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          //if(Project.getCurrentlyLoadedProject().getPanel().getPanelSettings().isPanelBasedAlignment())
         // {
             _mainUI.getTestDev().getPanelGraphicsEngineSetup().repopulateReconstructionRegionGraphics();
          //}
         // else
         // {
         //    _mainUI.getTestDev().getBoardGraphicsEngineSetup().repopulateReconstructionRegionGraphics();
         // }
          try
          {
            // now lets sleep for 2 seconds so that we can make sure the dialog doesn't just flash.
            Thread.sleep(1000);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }

        }
        finally
        {
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.setVisible(false);
        }
      }
    });
    busyCancelDialog.setVisible(true);
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void regenerateTestProgram()
  {
   final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
            StringLocalizer.keyToString("MMGUI_REGENERATING_TEST_PROGRAM_KEY"),
            StringLocalizer.keyToString("MMGUI_REGENERATING_TEST_PROGRAM_KEY"),
            true);

    SwingUtils.setFont(busyCancelDialog, com.axi.guiUtil.FontUtil.getDefaultFont());
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
    SwingUtils.invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          Project.getCurrentlyLoadedProject().regenerateTestProgram();
          try
          {
            // now lets sleep for 2 seconds so that we can make sure the dialog doesn't just flash.
            Thread.sleep(1000);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }

        }
        finally
        {
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.setVisible(false);
        }
      }
    });
    busyCancelDialog.setVisible(true);
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void enablePitchOffsetPanel(ActionEvent e)
  {
     _inputOffsetXByPitchTxtBox.setEnabled(true);
     _inputOffsetYByPitchTxtBox.setEnabled(true);
     
     _inputOffsetX.setEnabled(false);
     _inputOffsetY.setEnabled(false);
     
    if(_mainUI.getTestDev().getPanelGraphicsEngineSetup().getSelectedRegion() != null)
    {
       _mainUI.getTestDev().getPanelGraphicsEngineSetup().clearFocusRegion();
    }
    _mainUI.getTestDev().getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectRegionMode(_inputOffsetByDragFocusRegionRadioButton.isSelected());
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void enableXYOffsetPanel(ActionEvent e)
  {
     _inputOffsetXByPitchTxtBox.setEnabled(false);
     _inputOffsetYByPitchTxtBox.setEnabled(false);
     
     _inputOffsetX.setEnabled(true);
     _inputOffsetY.setEnabled(true);
     
     if(_mainUI.getTestDev().getPanelGraphicsEngineSetup().getSelectedRegion() != null)
     {
        _mainUI.getTestDev().getPanelGraphicsEngineSetup().clearFocusRegion();
     } 
     _mainUI.getTestDev().getPanelGraphicsEngineSetup().getGraphicsEngine().setSelectRegionMode(_inputOffsetByDragFocusRegionRadioButton.isSelected());
  }
  
  /**
   * @author Kee Chin Seong
   */
  private void showGraphics_actionPerformed(ActionEvent e)
  {
    if (_selectedRRegion.isEmpty() == false && _reconstructionRegionIdComboBox.getSelectedIndex() != -1)
    {
      _mainUI.getTestDev().getPanelGraphicsEngineSetup().setFocusRegionDebugGraphicVisible(_currentComponentName, _selectedRRegion.get(_reconstructionRegionIdComboBox.getSelectedIndex()).getRegionId(), _showReconstructionRegionAndFocusRegionGraphics.isSelected());
      _mainUI.getTestDev().getPanelGraphicsEngineSetup().setEditFocusRegionMode(_showReconstructionRegionAndFocusRegionGraphics.isSelected());
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  void populatePackageJointTypeTable()
  {
    _ignoreActions = true;
    
    /*
     * just to make sure the top side is not exist only go for bottom side
     */
    if(_currentBoard.topSideBoardExists() == false)
    {
       _currentSide = _BOTH_SIDE;
    }
        
    // load selected panel data into package family table
    _packageJointTypeTableModel.populateWithPanelData(_currentBoard,
        _currentBoardType, _BOTH_SIDE);
    
    _packageJointTypeTableModel.setIsCellEditable(false);
    
    _ignoreActions = false;
  }
  
  /**
   * @author Kee Chin Seong
   */
  void panelBoardComboBox_actionPerformed()
  {
    // get board index
    int index = _panelBoardComboBox.getSelectedIndex();
    // get corresponding board instance and name
    _currentBoard = _sortedBoardList.get(index);
    _currentBoardName = _currentBoard.getName();

    // update everything
    _currentBoardSelectedInGraphics = null;
    _currentComponentSelectedInGraphics = null;
    populateWithProjectData();
  }
  
  /**
   * @author Kee Chin Seong
   */
  void populateWithProjectData()
  {
    // get board type
    _currentBoardType = _mainUI.getTestDev().getCurrentBoardType();
    
   // update panel board combo box for the current board type
    populatePanelBoardComboBox();

    if (Project.getCurrentlyLoadedProject().getPanel().getNumBoards() > 0)
    {
      // update the tables and select the current component
      populateAllTables();
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void populateAllTables()
  {
    populatePackageJointTypeTable();
    
    // select package for component or 1st package
    CompPackage selectedCompPackage = selectPackageForCurrentComponent();

    populateComponentLocationTableForPackage(selectedCompPackage);
    
    // select current component or 1st component
    selectCurrentComponent();
  }
  
    /**
   * @author Kee Chin Seong
   */
  void populateComponentLocationTableForPanel()
  {
    _ignoreActions = true;
    
    _componentLocationTableModel.populateWithPanelData(_currentBoard,
        _currentBoardType, _BOTH_SIDE);
    
    _componentLocationTableModel.setIsCellEditable(false);
    
    _ignoreActions = false;
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void selectComponentTableRow(int row)
  {
    _currentComponentIndex = row;
    _currentComponent = _componentLocationTableModel.getComponentTypeAt(row);
    _componentLocationTableModel.setSelectedComponent(_currentBoard, _currentComponent, _currentComponentIndex);
    //updateAdjustButtons();
    _componentLocationTable.setRowSelectionInterval(row, row);
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void selectCurrentComponent()
  {
    // clear the current selection
    _componentLocationTable.cancelCellEditing();
    _componentLocationTable.clearSelection();
    _componentLocationTableModel.clearSelectedComponent();

    // select it
    if (_componentLocationTable.getRowCount() > 0)
    {
      if (_currentComponent != null)
      {
        selectComponentTableRow(_componentLocationTableModel.getRowForComponentType(_currentComponent));
      }
      else
      {
        selectComponentTableRow(0);
        _currentComponent = _componentLocationTableModel.getComponentTypeAt(0);
        _currentComponentName = _currentComponent.getReferenceDesignator();
      }
      //Kee Chin Seong - XCR 3414 - Unable to display focus region 
      populateTheRRIDByRefDEs(_currentComponent.getReferenceDesignator());
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  void populateComponentLocationTableForPackage(CompPackage compPackage)
  {
    _ignoreActions = true;
    if (compPackage != null)
    {
      _componentLocationTableModel.populateWithPackageData(_currentBoard,
          _currentBoardType, _BOTH_SIDE, compPackage);
      _componentLocationTableModel.setIsCellEditable(false);
    }
    _ignoreActions = false;
  }
  
  /*
   * @author Kee Chin Seong
   */
  private CompPackage selectPackageForCurrentComponent()
  {
    Assert.expect(_packageJointTypeTable.getRowCount() > 0);

    // clear the current selection
    _packageJointTypeTable.cancelCellEditing();
    _packageJointTypeTable.clearSelection();
    _packageJointTypeTableModel.clearSelectedPackage();

    // Select the package for the current component or
    // select row 0 if _currentComponent is not defined
    int selectedRow = 0;

    _ignoreActions = true;
    selectPackageTableRow(selectedRow);
    _ignoreActions = false;
    
    return _packageJointTypeTableModel.setSelectedPackageAt(selectedRow);
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void selectPackageTableRow(int row)
  {
    _packageJointTypeTable.setRowSelectionInterval(row, row);
  }
  
  /*
   * @author Kee Chin Seong
   */
  void populatePanelBoardComboBox()
  {
    _ignoreActions = true;
    
    _panelBoardComboBox.removeActionListener(_panelBoardPopulateActionListener);
    
    // update panel/board instances
    _panelBoardComboBox.removeAllItems();
    // get boards of this type
 
    _sortedBoardList = new java.util.ArrayList<Board>();

    java.util.List<Board> boardInstances = Project.getCurrentlyLoadedProject().getPanel().getBoards();
    // sort boards based on name
    Collections.sort(boardInstances, new AlphaNumericComparator());
    for (Board board : boardInstances)
    {
      if (board.getBoardType().equals(_currentBoardType))
        _sortedBoardList.add(board);
    }

    // set up panelBoardComboBox
    if (_sortedBoardList.size() == 1)
    {
      _panelBoardComboBox.addItem(StringLocalizer.keyToString("ATGUI_PANEL_KEY"));
      _panelBoardComboBox.setSelectedIndex(0);
    }
    else
    {
      boolean canSelectBoardName = false;
      for(Board board : _sortedBoardList)
      {
        _panelBoardComboBox.addItem(board.getName());
        if (_currentBoardName != null && _currentBoardName.equals(board.getName()))
        {
          canSelectBoardName = true;
        }
      }
      if (canSelectBoardName)
      {
        _panelBoardComboBox.setSelectedItem(_currentBoardName);
      }
      else
      {
        // default to first board
        _panelBoardComboBox.setSelectedIndex(0);
      }
    }
    _currentBoard = _sortedBoardList.get(_panelBoardComboBox.getSelectedIndex());
    _currentBoardName = _currentBoard.getName();
    
    _panelBoardComboBox.addActionListener(_panelBoardPopulateActionListener);
    
    _ignoreActions = false;
  }
  
  /*
   * @author Kee Chin Seong
   */
  void sideComboBox_actionPerformed()
  {
    _currentSide = (String)_sideComboBox.getSelectedItem();

    // update everything
    populateWithProjectData();
  }
  
  /*
   * @author Kee Chin Seong
   */
  void highlightBoardAndComponent()
  {
    if (_currentBoard != null &&
          (_currentBoardSelectedInGraphics == null || _currentBoardSelectedInGraphics != _currentBoard))
    {
      _currentBoardSelectedInGraphics = _currentBoard;
      java.util.List<Board> selectedBoards = new ArrayList<Board>();
      selectedBoards.add(_currentBoard);
      _guiObservable.stateChanged(selectedBoards, SelectionEventEnum.BOARD_INSTANCE);
    }
    if (_currentComponent != null &&
        (_currentComponentSelectedInGraphics == null || _currentComponentSelectedInGraphics != _currentComponent))
    {
      _currentComponentSelectedInGraphics  = _currentComponent;
      java.util.List<ComponentType> selectedComponents = new ArrayList<ComponentType>();
      selectedComponents.add(_currentComponent);
//        System.out.println("!! highlightComponent " + _currentComponent.getReferenceDesignator());
      _guiObservable.stateChanged(selectedComponents, SelectionEventEnum.COMPONENT_TYPE);
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void start()
  {
     populateWithProjectData();
     _guiObservable.addObserver(this);
     _isStarted = true;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void finish()
  {
    if (_componentLocationTable.isEditing())
      _componentLocationTable.cancelCellEditing();
    if (_packageJointTypeTable.isEditing())
      _packageJointTypeTable.cancelCellEditing();

    if(_mainUI.getTestDev().getPanelGraphicsEngineSetup().getSelectedRegion() != null)
    {
       _mainUI.getTestDev().getPanelGraphicsEngineSetup().clearFocusRegion();
    }

    // Kok Chun, Tan - XCR3811 - index out of bound when the selected component is no load
    _mainUI.getTestDev().getPanelGraphicsEngineSetup().setEditFocusRegionInspectionFocusRegionLayerVisibility(false, false);
    _mainUI.getTestDev().getPanelGraphicsEngineSetup().setEditFocusRegionMode(false);
    _showReconstructionRegionAndFocusRegionGraphics.setSelected(false);
    
    _reconstructionRegionIdComboBox.removeActionListener(_rridComboBoxActionListener);
    
    if(_reconstructionRegionIdComboBox.getItemCount() > 0)
        _reconstructionRegionIdComboBox.removeAllItems();
    _currentBoardType = null;
    
    _currentBoard = null;
    _currentBoardName = null;
    _currentBoardType = null;
    _sortedBoardList = null;
    _currentComponent = null;
    _currentComponentName = null;
    _currentBoardSelectedInGraphics = null;
    _currentComponentSelectedInGraphics = null;
    _currentComponentIndex = -1;
    _packageJointTypeTable.resetSortHeader();
    _componentLocationTable.resetSortHeader();
    _isStarted = false;
    
    _guiObservable.deleteObserver(this);
  }
  
  /**
   * XCR-2597, Component selected through Find Component tools does not reflect on Focus Region table
   * @author Yong Sheng Chuan
   */
  void handleSelectedRendererEvent(final Object object)
  {
    for (com.axi.guiUtil.Renderer renderer : ((Collection<com.axi.guiUtil.Renderer>) object))
    {
      if (renderer instanceof ComponentRenderer)
      {
        ComponentRenderer componentRenderer = (ComponentRenderer) renderer;
        com.axi.v810.business.panelDesc.Component component = componentRenderer.getComponent();
        int row = _packageJointTypeTableModel.getRowForCompPackage(component.getCompPackage());
        _currentComponent = component.getComponentType();
        selectPackageTableRow(row);
        int selectedComponentLocationRow = _componentLocationTableModel.getRowForComponentType(_currentComponent);
        _componentLocationTable.setRowSelectionInterval(selectedComponentLocationRow, selectedComponentLocationRow);
      }
    }
  }
  
  /*
   * @author Kee Chin Seong
   * - Do notihng for current stage as there is no need to be update on this obersever
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    // do nothing
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void packageJointTypeTable_valueChanged(ListSelectionEvent e)
  {
    if (_ignoreActions)
    {
        return;
    }
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    // turn off event processing until graphics had a chance to update
    _packageJointTypeTable.processSelections(false);
    // package was selected with a table event
     _currentPackageIndex = lsm.getMinSelectionIndex();
     
     if(_currentPackageIndex < 0)
        _currentPackageIndex = 0;
    //      System.out.println("packageJointTypeTable row " + _currentPackageIndex + " selected");
    CompPackage currentCompPackage = _packageJointTypeTableModel.getPackageAt(_currentPackageIndex);
    _packageJointTypeTableModel.setSelectedPackage(currentCompPackage);

    // after selection, scroll to make sure the row is visible.
    SwingUtils.scrollTableToShowSelection(_packageJointTypeTable);

    _ignoreActions = true;
    populateComponentLocationTableForPackage(currentCompPackage);
    
    selectCurrentComponent();
    _ignoreActions = false;

    // set CAD graphics crosshairs on board and component
    highlightBoardAndComponent();

    // wait for graphics
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        _packageJointTypeTable.processSelections(true);
      }
    });
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void componentLocationTable_valueChanged(ListSelectionEvent e)
  {
    if (_ignoreActions)
    {
        return;
    }
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();

    _componentLocationTable.processSelections(false);
    // component was selected with a table event
    int selectedRow = lsm.getMinSelectionIndex();

    if(selectedRow > 0)
        _currentComponentIndex = selectedRow;
    else
        _currentComponentIndex = 0;
 
    _currentComponent = _componentLocationTableModel.getComponentTypeAt(_currentComponentIndex);
    _currentComponentName = _currentComponent.getReferenceDesignator();
    _componentLocationTableModel.setSelectedComponent(_currentBoard, _currentComponent, _currentComponentIndex);

    // after selection, scroll to make sure the row is visible.
    SwingUtils.scrollTableToShowSelection(_componentLocationTable);

    // set CAD graphics crosshairs on board and component
    highlightBoardAndComponent();
    
    populateTheRRIDByRefDEs(_currentComponentName);
    
    // Kok Chun, Tan
    // XCR-3383 - Region ID in CAD doesn't not refresh after change Region ID in Edit Focus Region
    // XCR-3811 - index out of bound when the selected component is no load
    if (_showReconstructionRegionAndFocusRegionGraphics.isSelected() &&
        _selectedRRegion.isEmpty() == false &&
        _reconstructionRegionIdComboBox.getSelectedIndex() != -1)
      _mainUI.getTestDev().getPanelGraphicsEngineSetup().setFocusRegionDebugGraphicVisible(_currentComponentName, _selectedRRegion.get(_reconstructionRegionIdComboBox.getSelectedIndex()).getRegionId(), _showReconstructionRegionAndFocusRegionGraphics.isSelected());
    else
      _mainUI.getTestDev().getPanelGraphicsEngineSetup().setEditFocusRegionInspectionFocusRegionLayerVisibility(false, false);

    // wait for graphics
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        _componentLocationTable.processSelections(true);
      }
    });
  }
  
  /*
   * Kee Chin Seong
   */
  public boolean isStarted()
  {
    return _isStarted;
  }
}