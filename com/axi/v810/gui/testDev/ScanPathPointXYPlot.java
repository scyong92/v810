/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.testDev;

import com.axi.v810.business.testResults.MeasurementUnitsEnum;
import com.axi.v810.hardware.StagePosition;

import org.jfree.chart.plot.XYPlot;

/**
 *
 * @author chin-seong.kee
 */
abstract class ScanPathPointXYPlot extends XYPlot
{
  /**
   * Get the Title of this plot.
   * @return String
   */
  abstract String getTitle();

  /**
   * Get the statistical information of this plot.
   * @author Kee Chin Seong
   */
  abstract String getStatisticalInformationString();

  /**
   * @return MeasurementUnitsEnum if there is one, null otherwise.
   * @author Kee Chin Seong
   */
  abstract protected StagePosition getStartingPointXY();

  /**
   * Used by derived classes to format value.
   * @author Kee Chin Seong
   */
  protected String getFormattedValueString(double value)
  {
    return Double.toString(value);
  }
}
