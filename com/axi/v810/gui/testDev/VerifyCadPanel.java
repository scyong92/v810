package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: VerifyCadPanel</p>
 *
 * <p>Description: This is the Verify CAD panel for the Test Development Tool</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: ViTrox</p>
 *
 * @author George Booth
 */

public class VerifyCadPanel extends JPanel implements Observer
{
  // gui related variables
  private JPanel _northPanel = new JPanel();

  private JPanel _viewControlsWestPanel = new JPanel();
  private JLabel _sideLabel = new JLabel();
  private JComboBox _sideComboBox = new JComboBox();
  private JLabel _viewByLabel = new JLabel();
  private JComboBox _viewByComboBox = new JComboBox();
  private JPanel _viewControlsEastPanel = new JPanel();
  private JPanel _viewControlsNorthEastPanel = new JPanel();
  private JLabel _panelBoardLabel = new JLabel();
  private JComboBox _panelBoardComboBox = new JComboBox();
  private JButton _generateVerificationImagesButton = new JButton();

  private JPanel _centerPanel = new JPanel();

  private JPanel _componentLocationPanel = new JPanel();
  private ComponentLocationTableModel _componentLocationTableModel;
  private ComponentLocationTable _componentLocationTable;
  private ListSelectionListener _componentLocationTableListSelectionListener;
  private JScrollPane _componentLocationScrollPane = new JScrollPane();
  private JPanel _componentLocationControlPanel = new JPanel();
  private JButton _lastComponentButton; // = new JButton();
  private JButton _nextComponentButton; // = new JButton();

  private JPanel _packageJointTypePanel = new JPanel();
  private JSplitPane _packageJointTypeSplitPane = new JSplitPane();
  private PackageJointTypeTableModel _packageJointTypeTableModel;
  private PackageJointTypeTable _packageJointTypeTable;
  private ListSelectionListener _packageJointTypeTableListSelectionListener;
  private JScrollPane _packageJointTypeScrollPane = new JScrollPane();
  private JPanel _packageJointTypeControlPanel = new JPanel();
  private JButton _lastPackageButton; //= new JButton();
  private JButton _nextPackageButton; // = new JButton();

  private JPanel _regionOrSidePanel = new JPanel();
  private JSplitPane _regionOrSideSplitPane = new JSplitPane();
  private RegionOrSideTableModel _regionOrSideTableModel;
  private RegionOrSideTable _regionOrSideTable;
  private ListSelectionListener _regionOrSideTableListSelectionListener;
  private JScrollPane _regionOrSideScrollPane = new JScrollPane();
  private JPanel _regionOrSideControlPanel = new JPanel();
  private JButton _lastRegionButton; // = new JButton();
  private JButton _nextRegionButton; // = new JButton();

  private JPanel _southPanel = new JPanel();

  private JPanel _adjustGraphicsPanel = new JPanel();
  private ButtonGroup _adjustGraphicsButtonGroup = new ButtonGroup();
  private JRadioButton _adjustOffRadioButton = new JRadioButton();
  private JRadioButton _adjustComponentRadioButton = new JRadioButton();
  private JRadioButton _adjustBoardSideRadioButton = new JRadioButton();
  private JRadioButton _adjustBoardOriginRadioButton = new JRadioButton();
  private JButton _applyAdjustmentButton = new JButton();
  private JButton _cancelAdjustmentButton = new JButton();

  // member variables for controlling the gui
  // subtype change choices
  private final String _NEW_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NEW_KEY");
  private final String _NOTEST_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOTEST_KEY");
  private final String _NOLOAD_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOLOAD_KEY");
  private final String _RENAME_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_RENAME_KEY");
  private final String _RESTORE_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_RESTORE_KEY");
  private final String _NEW_SHADED_SUBTYPE = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NEW_SHADED_KEY");
  
  private final String _NOT_APPLICABLE = "N/A";

  // side choices
  private final String _TOP_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_TOP_LABEL_KEY");
  private final String _BOTTOM_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_BOTTOM_LABEL_KEY");
  private final String _BOTH_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_BOTH_LABEL_KEY");

  // on-line or off-line
  private boolean _hardwareAvailable = false;
  // the current project
  private Project _project;
  // true if this panel is visible
  private boolean _active = false;
  
  //true if the adjust cad already load once.
  private boolean _adjustCadActive = false;
  
  // true if a combo box or table update should be ignored
  private boolean _ignoreActions = false;
  // true if a graphics adjustment is being done
  private boolean _adjusting = false;
  // true if in the process of generating verification images
  private boolean _generatingImages = false;
  // the current board type
  private BoardType _currentBoardType = null;
  // all the board instances for the current board type
  private java.util.ArrayList<Board> _sortedBoardList = null;
  // the current board instance
  private Board _currentBoard = null;
  // the current board name
  private String _currentBoardName = null;
  // the current board side
  private String _currentSide = null;
  // localized strings for View By combo box
  private String _PACKAGE_VIEW;
  private String _COMPONENT_VIEW;
  private String _REGION_VIEW;
  // the name of the current view selection
  private String _currentView;
  // the adjust radio button selected before the current click
  private JRadioButton _lastButton = null;
  // location of package view split pane divider
  private int _packageJointTypeSplitPaneDivider = -1;
  // location of region view split pane divider
  private int _regionOrSideSplitPaneDivider = -1;
  // true if it is OK to update graphics or respond to the graphics events
  private boolean _shouldInteractWithGraphics = false;
  // the currently selected package if using package view
  private int _currentPackageIndex = -1;
  // The currently selected component will determine the selections in the
  // other tables as views are changed to ensure the table selections
  // matches the current graphics selection
  private int _currentComponentIndex = -1;
  // the currently selected component
  private ComponentType _currentComponent = null;
  // the currently selected component name
  private String _currentComponentName = null;
  // the component that was selected in the CAD graphics view
  private ComponentType _componentTypeSelectedGraphically = null;
  // the project has valid verification images
  private boolean _haveValidVerificationImages = false;
  // the project has a valid alignment
  private boolean _alignmentValid = false;
  // current board and component selected in graphics (don't constantly reselect to keep graphics snappy)
  private Board _currentBoardSelectedInGraphics = null;
  private ComponentType _currentComponentSelectedInGraphics = null;
  // true if an Undo was for Adjust operation
  private boolean _undoRedoAdjustCadGraphically = false;

  // observables
  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private ImageManagerObservable _imageManagerObservable = ImageManagerObservable.getInstance();
  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  private GuiObservable _guiObservable = GuiObservable.getInstance();

  // reference to Main UI
  private MainMenuGui _mainUI = MainMenuGui.getInstance();
  // reference to Image Manager Panel
  private ImageManagerPanel _imageManagerPanel;
  
  // Siew Yeng - XCR1639
  private boolean _lowMagAlignmentValid = false;
  private boolean _highMagAlignmentValid = false;
  
  //Siew Yeng - when alignment component set to No Load use this list to keep track affected alignment pads
  private java.util.List<Pad> _affectedAlignmentPads = new ArrayList();
  
  private static transient ProjectHistoryLog _historyLogWriter = ProjectHistoryLog.getInstance();
  
  private boolean _isOffsetBeenSet = false;
  
  //Khaw Chek Hau - XCR2337: Message should prompt for No Load and No Test components at Adjust CAD
  private JCheckBox _promptWarningMsgIfHighMagRecipeCheckBox;

  // Chin Seong - fix adjust cad need to regenerate verification image when change joint type
  private boolean _isUserChangeJointType = false;
  
  private boolean _isChangeView = false;
  
  //Khaw Chek Hau - XCR3554: Package on package (PoP) development
  private POPComponentLocationTableModel _popComponentLocationTableModel;
  private POPComponentLocationTable _popComponentLocationTable;
  private JPanel _popComponentLocationPanel = new JPanel();
  private ListSelectionListener _popComponentLocationTableListSelectionListener;
  private JScrollPane _popComponentLocationScrollPane = new JScrollPane();
  private JPanel _popComponentLocationControlPanel = new JPanel();
  private JPanel _packageOnPackagePanel = new JPanel();
  private PackageOnPackageTableModel _packageOnPackageTableModel;
  private PackageOnPackageTable _packageOnPackageTable;
  private ListSelectionListener _packageOnPackageTableListSelectionListener;
  private JScrollPane _packageOnPackageScrollPane = new JScrollPane(); 
  private JPanel _packageOnPackageControlPanel = new JPanel();
  private String _POP_VIEW;
  private int _currentPOPIndex = -1;
  private JPopupMenu _mergeComponentsToPOPPopupMenu = new JPopupMenu();
  private JMenuItem _mergeComponentToPOPMenuItem = new JMenuItem();
  private JMenuItem _addPOPComponentMenuItem = new JMenuItem();
  private JPopupMenu _packageOnPackagePopupMenu = new JPopupMenu();
  private JMenuItem _packageOnPackageMenuItem = new JMenuItem();
  private java.util.List<ComponentType> _componentTypes = null;
  private java.util.List<CompPackageOnPackage> _compPackageOnPackages = null;
  
  /**
   * @author George Booth
   */
  public VerifyCadPanel()
  {
    jbInit();
    //XCR-2489 - Swee yee wong - Board deletion not updated in verify CAD
    _projectObservable.addObserver(this);
  }

  /**
   * @author George Booth
   */
  private void jbInit()
  {
    // make sure components don't respond while being initialized
    _ignoreActions = true;

    _hardwareAvailable = XrayTester.getInstance().isHardwareAvailable();

    setLayout(new BorderLayout());
    setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

    _northPanel.setLayout(new BorderLayout());
    add(_northPanel, BorderLayout.NORTH);

    _viewControlsWestPanel.setBorder(BorderFactory.createEmptyBorder(0,0,10,0));
    _viewControlsWestPanel.setLayout(new PairLayout(10, 10, false));
    _northPanel.add(_viewControlsWestPanel, BorderLayout.WEST);

    _sideLabel.setText(StringLocalizer.keyToString("CAD_BOARD_SIDE_LABEL_KEY"));
    _sideLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _viewControlsWestPanel.add(_sideLabel);

    _sideComboBox.setToolTipText(StringLocalizer.keyToString("CAD_BOARD_SIDE_TOOLTIP_KEY"));
    _sideComboBox.addItem(_TOP_SIDE);
    _sideComboBox.addItem(_BOTTOM_SIDE);
    _sideComboBox.addItem(_BOTH_SIDE);
    _currentSide = (String)_sideComboBox.getSelectedItem();
    _sideComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        sideComboBox_actionPerformed();
      }
    });
    _viewControlsWestPanel.add(_sideComboBox);

    _viewByLabel.setText(StringLocalizer.keyToString("CAD_VIEW_MODE_LABEL_KEY"));
    _viewByLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _viewControlsWestPanel.add(_viewByLabel);

    _viewByComboBox.setToolTipText(StringLocalizer.keyToString("CAD_VIEW_MODE_TOOLTIP_KEY"));
    _PACKAGE_VIEW = StringLocalizer.keyToString("CAD_PACKAGE_VIEW_KEY");
    _COMPONENT_VIEW = StringLocalizer.keyToString("CAD_COMPONENT_VIEW_KEY");
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    _POP_VIEW = StringLocalizer.keyToString("CAD_POP_VIEW_KEY");
    _REGION_VIEW = StringLocalizer.keyToString("CAD_REGION_VIEW_KEY");
    _viewByComboBox.addItem(_PACKAGE_VIEW);
    _viewByComboBox.addItem(_COMPONENT_VIEW);
    _viewByComboBox.addItem(_POP_VIEW);
// Region is in future release
//    _viewByComboBox.addItem(_REGION_VIEW);
    // Package view is shown by default.
    _currentView = _PACKAGE_VIEW;
    _viewByComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        viewByComboBox_actionPerformed();
      }
    });
    _viewControlsWestPanel.add(_viewByComboBox);

    _viewControlsEastPanel.setBorder(BorderFactory.createEmptyBorder(0,0,10,0));
    _viewControlsEastPanel.setLayout(new BorderLayout());
    _northPanel.add(_viewControlsEastPanel, BorderLayout.EAST);

    _viewControlsNorthEastPanel.setLayout(new PairLayout(10, 10, false));
    _viewControlsEastPanel.add(_viewControlsNorthEastPanel, BorderLayout.NORTH);

    _panelBoardLabel.setText(StringLocalizer.keyToString("ATGUI_PANEL_BOARD_KEY"));
    _panelBoardLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _viewControlsNorthEastPanel.add(_panelBoardLabel);

    _panelBoardComboBox.setToolTipText("select panel or board"); //StringLocalizer.keyToString("CAD_BOARD_SIDE_TOOLTIP_KEY"));
    _panelBoardComboBox.addItem(StringLocalizer.keyToString("ATGUI_PANEL_KEY"));
    _panelBoardComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        panelBoardComboBox_actionPerformed();
      }
    });
    _viewControlsNorthEastPanel.add(_panelBoardComboBox);

    _generateVerificationImagesButton.setText(StringLocalizer.keyToString("CAD_GENERATE_IMAGES_BUTTON_KEY"));
    _generateVerificationImagesButton.setToolTipText(StringLocalizer.keyToString("CAD_GENERATE_IMAGES_BUTTON_TOOLTIP_KEY"));
    _generateVerificationImagesButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        generateVerificationImagesButton_ActionPerformed();
      }
    });
    if (_hardwareAvailable)
    {
      _viewControlsEastPanel.add(_generateVerificationImagesButton, BorderLayout.SOUTH);
    }

    // center panel for tables
    _centerPanel.setLayout(new BorderLayout());
    add(_centerPanel, BorderLayout.CENTER);

    // create all tables and table listeners that will be used in verify cad
    _componentLocationTableModel = new ComponentLocationTableModel();
    _componentLocationTable = new ComponentLocationTable(this, _componentLocationTableModel)
    {
      /**
       * Overriding this method allows us to set up the undo manager
       * @author George Booth
       */
      public void setValueAt(Object value, int row, int column)
      {
        componentLocationTableSetValue(value, row, column);
      }
      /**
       * Overriding this method allows us to make sure the selected rows
       * are still the proper selected rows after a sort.
       * @author George Booth
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = _componentLocationTable.getSelectedRows();
        java.util.List<ComponentType> selectedData = new ArrayList<ComponentType>();
        for (int row = 0; row < selectedRows.length; row++)
        {
          selectedData.add(_componentLocationTableModel.getComponentTypeAt(selectedRows[row]));
        }
        
        _shouldInteractWithGraphics = false;
        ListSelectionModel rowSM = _componentLocationTable.getSelectionModel();
        rowSM.removeListSelectionListener(_componentLocationTableListSelectionListener); // add the listener for a details table row selection
        
        super.mouseReleased(event);
        // now, reselect everything
        _componentLocationTable.clearSelection();
        for (ComponentType compType : selectedData)
        {
          int row = _componentLocationTableModel.getRowForComponentType(compType);
          _componentLocationTable.addRowSelectionInterval(row, row);
        }
        _componentLocationTableModel.setSelectedComponents(_currentBoard, selectedData);
        
        SwingUtils.scrollTableToShowSelection(_componentLocationTable);
        _shouldInteractWithGraphics = true;
        rowSM.addListSelectionListener(_componentLocationTableListSelectionListener); // add the listener for a details table row selection
      }
    };
    _componentLocationTable.setPreferredColumnWidths();
    
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    _popComponentLocationTableModel = new POPComponentLocationTableModel();
    _popComponentLocationTable = new POPComponentLocationTable(_popComponentLocationTableModel)
    {
      public void setValueAt(Object value, int row, int column)
      {
        popComponentLocationTableSetValue(value, row, column);
      }

      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = _popComponentLocationTable.getSelectedRows();
        java.util.List<ComponentType> selectedData = new ArrayList<ComponentType>();
        for (int row = 0; row < selectedRows.length; row++)
        {
          selectedData.add(_popComponentLocationTableModel.getComponentTypeAt(selectedRows[row]));
        }
        
        _shouldInteractWithGraphics = false;
        ListSelectionModel rowSM = _popComponentLocationTable.getSelectionModel();
        rowSM.removeListSelectionListener(_popComponentLocationTableListSelectionListener); // add the listener for a details table row selection
        
        super.mouseReleased(event);
        // now, reselect everything
        _popComponentLocationTable.clearSelection();
        for (ComponentType compType : selectedData)
        {
          int row = _popComponentLocationTableModel.getRowForComponentType(compType);
          _popComponentLocationTable.addRowSelectionInterval(row, row);
        }
        _popComponentLocationTableModel.setSelectedComponentTypes(selectedData);
        
        SwingUtils.scrollTableToShowSelection(_popComponentLocationTable);
        _shouldInteractWithGraphics = true;
        rowSM.addListSelectionListener(_popComponentLocationTableListSelectionListener); // add the listener for a details table row selection
      }
    };
    _popComponentLocationTable.setPreferredColumnWidths();
    
    //Siew Yeng - XCR-2123
    _componentLocationTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _popComponentLocationTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    
    // add the listener for a table row selection
    _componentLocationTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        multiEditTableMouseReleased(e, _componentLocationTable);
      }
    });
    
    _componentLocationTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        componentLocationTable_valueChanged(e);
      }
    };
    ListSelectionModel componentLocationTableSM = _componentLocationTable.getSelectionModel();
    componentLocationTableSM.addListSelectionListener(_componentLocationTableListSelectionListener);
    _componentLocationScrollPane.getViewport().add(_componentLocationTable);
    _componentLocationScrollPane.setMinimumSize(new Dimension(50,50));
    
    _popComponentLocationTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        popComponentLocationTable_valueChanged(e);
      }
    };
    ListSelectionModel popComponentLocationTableSM = _popComponentLocationTable.getSelectionModel();
    popComponentLocationTableSM.addListSelectionListener(_popComponentLocationTableListSelectionListener);    
    _popComponentLocationScrollPane.getViewport().add(_popComponentLocationTable);
    _popComponentLocationScrollPane.setMinimumSize(new Dimension(50,50));
    
    _packageJointTypeTableModel = new PackageJointTypeTableModel();
    _packageJointTypeTable = new PackageJointTypeTable(_packageJointTypeTableModel)
    {
      /**
       * Overriding this method allows us to set up the undo manager
       * @author George Booth
       */
      public void setValueAt(Object value, int row, int column)
      {
        packageJointTypeTableSetValue(value, row, column);
      }
      /**
       * Overriding this method allows us to make sure the selected rows
       * are still the proper selected rows after a sort.
       * @author George Booth
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = _packageJointTypeTable.getSelectedRows();
        java.util.List<CompPackage> selectedData = new ArrayList<CompPackage>();
        for (int row = 0; row < selectedRows.length; row++)
        {
          selectedData.add(_packageJointTypeTableModel.getPackageAt(selectedRows[row]));
        }
        super.mouseReleased(event);
        // now, reselect everything
        _packageJointTypeTable.clearSelection();
        for (CompPackage compPack : selectedData)
        {
          int row = _packageJointTypeTableModel.getRowForCompPackage(compPack);
          _packageJointTypeTable.addRowSelectionInterval(row, row);
        }
      }
    };
    _packageJointTypeTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        packageJointTypeTable_valueChanged(e);
      }
    };
    ListSelectionModel packageJointTypeSM = _packageJointTypeTable.getSelectionModel();
    packageJointTypeSM.addListSelectionListener(_packageJointTypeTableListSelectionListener);
    _packageJointTypeScrollPane.getViewport().add(_packageJointTypeTable);
    _packageJointTypeScrollPane.setMinimumSize(new Dimension(50,50));
    
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    _packageOnPackageTableModel = new PackageOnPackageTableModel();
    _packageOnPackageTable = new PackageOnPackageTable(_packageOnPackageTableModel)
    {
      /**
       * Overriding this method allows us to set up the undo manager
       * @author George Booth
       */
      public void setValueAt(Object value, int row, int column)
      {
        _commandManager.trackState(getCurrentUndoState());
        _packageOnPackageTableModel.setValueAt(value, row, column);
      }
      /**
       * Overriding this method allows us to make sure the selected rows
       * are still the proper selected rows after a sort.
       * @author George Booth
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = _packageOnPackageTable.getSelectedRows();
        java.util.List<CompPackageOnPackage> selectedData = new ArrayList<CompPackageOnPackage>();
        for (int row = 0; row < selectedRows.length; row++)
        {
          selectedData.add(_packageOnPackageTableModel.getPackageAt(selectedRows[row]));
        }
        super.mouseReleased(event);
        // now, reselect everything
        _packageOnPackageTable.clearSelection();
        for (CompPackageOnPackage compPOP : selectedData)
        {
          int row = _packageOnPackageTableModel.getRowForCompPackageOnPackage(compPOP);
          _packageOnPackageTable.addRowSelectionInterval(row, row);
        }
      }
    };
    
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    _packageOnPackageTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        packageOnPackageTableMouseReleased(e, _packageOnPackageTable);
      }
    });
    
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    _packageOnPackageTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        packageOnPackageTable_valueChanged(e);
      }
    };

    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    ListSelectionModel packageOnPackageSM = _packageOnPackageTable.getSelectionModel();
    packageOnPackageSM.addListSelectionListener(_packageOnPackageTableListSelectionListener);
    _packageOnPackageScrollPane.getViewport().add(_packageOnPackageTable);
    _packageOnPackageScrollPane.setMinimumSize(new Dimension(50,50));
    
    _packageOnPackageMenuItem.setText(StringLocalizer.keyToString("CAD_DELETE_POP_MENU_ITEM_KEY"));
    _packageOnPackagePopupMenu.add(_packageOnPackageMenuItem);
    _packageOnPackageMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        packageOnPackageMenuItemActionPerformed(e);
      }
    });

    _regionOrSideTableModel = new RegionOrSideTableModel();
    _regionOrSideTable = new RegionOrSideTable(_regionOrSideTableModel)
    {
      /**
       * Overriding this method allows us to make sure the selected rows
       * are still the proper selected rows after a sort.
       * @author George Booth
       */
      public void mouseReleased(MouseEvent event)
      {
        int selectedRows[] = _regionOrSideTable.getSelectedRows();
        java.util.List<String> selectedData = new ArrayList<String>();
        for (int row = 0; row < selectedRows.length; row++)
        {
          selectedData.add(_regionOrSideTableModel.getRegionAt(selectedRows[row]));
        }
        super.mouseReleased(event);
        // now, reselect everything
        _packageJointTypeTable.clearSelection();
        //Khaw Chek Hau - XCR3554: Package on package (PoP) development
        _packageOnPackageTable.clearSelection();
        for (String region : selectedData)
        {
          int row = _regionOrSideTableModel.getRowForRegion(region);
          _regionOrSideTable.addRowSelectionInterval(row, row);
        }
      }
    };
    _regionOrSideTableListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        regionOrSideTable_valueChanged(e);
      }
    };
    ListSelectionModel regionOrSideTableSM = _regionOrSideTable.getSelectionModel();
    regionOrSideTableSM.addListSelectionListener(_regionOrSideTableListSelectionListener);
    _regionOrSideScrollPane.getViewport().add(_regionOrSideTable);
    _regionOrSideScrollPane.setMinimumSize(new Dimension(50,50));

    // Create the panels that are swapped into the center panel for different views
    // Setup the package split pane. Table scroll panes are added when needed.
    _packageJointTypePanel.setLayout(new BorderLayout());
    _packageJointTypePanel.add(_packageJointTypeControlPanel, BorderLayout.NORTH);
    
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    _packageOnPackagePanel.setLayout(new BorderLayout());
    _packageOnPackagePanel.add(_packageOnPackageControlPanel, BorderLayout.NORTH);

    _lastPackageButton = new JButton(StringLocalizer.keyToString("CAD_LAST_PACKAGE_BUTTON_KEY"),
                                     Image5DX.getImageIcon(Image5DX.VC_UP_ARROW));
    _lastPackageButton.setToolTipText(StringLocalizer.keyToString("CAD_LAST_PACKAGE_TOOLTIP_KEY"));
    _lastPackageButton.setEnabled(false);
    _lastPackageButton.setHorizontalAlignment(SwingConstants.RIGHT);
    _lastPackageButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectPreviousTableRow(_packageJointTypeTable, _lastPackageButton, _nextPackageButton);
      }
    });
    _packageJointTypeControlPanel.add(_lastPackageButton);
    _nextPackageButton = new JButton(StringLocalizer.keyToString("CAD_NEXT_PACKAGE_BUTTON_KEY"),
                                     Image5DX.getImageIcon(Image5DX.VC_DOWN_ARROW));
    _nextPackageButton.setToolTipText(StringLocalizer.keyToString("CAD_NEXT_PACKAGE_TOOLTIP_KEY"));
    _nextPackageButton.setEnabled(false);
    _nextPackageButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectNextTableRow(_packageJointTypeTable, _lastPackageButton, _nextPackageButton);
      }
    });
    _packageJointTypeControlPanel.add(_nextPackageButton);

    _packageJointTypePanel.add(_packageJointTypeScrollPane, BorderLayout.CENTER);
    
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    _packageOnPackagePanel.add(_packageOnPackageScrollPane, BorderLayout.CENTER);

    _packageJointTypeSplitPaneDivider = -1;

    // Setup the component panel. Component scroll pane is added when needed.
    _componentLocationPanel.setLayout(new BorderLayout());
    _componentLocationPanel.add(_componentLocationControlPanel, BorderLayout.NORTH);
    
    _popComponentLocationPanel.setLayout(new BorderLayout());
    _popComponentLocationPanel.add(_popComponentLocationControlPanel, BorderLayout.NORTH);
    
    _lastComponentButton = new JButton(StringLocalizer.keyToString("CAD_LAST_COMPONENT_BUTTON_KEY"),
                                     Image5DX.getImageIcon(Image5DX.VC_UP_ARROW));
    _lastComponentButton.setToolTipText(StringLocalizer.keyToString("CAD_LAST_COMPONENT_TOOLTIP_KEY"));
    _lastComponentButton.setHorizontalAlignment(SwingConstants.RIGHT);
    _lastComponentButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectPreviousTableRow(_componentLocationTable, _lastComponentButton, _nextComponentButton);
      }
    });
    _componentLocationControlPanel.add(_lastComponentButton);
    _nextComponentButton = new JButton(StringLocalizer.keyToString("CAD_NEXT_COMPONENT_BUTTON_KEY"),
                                     Image5DX.getImageIcon(Image5DX.VC_DOWN_ARROW));
    _nextComponentButton.setToolTipText(StringLocalizer.keyToString("CAD_NEXT_COMPONENT_TOOLTIP_KEY"));
    _nextComponentButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectNextTableRow(_componentLocationTable, _lastComponentButton, _nextComponentButton);
      }
    });
    _componentLocationControlPanel.add(_nextComponentButton);

    _componentLocationPanel.add(_componentLocationScrollPane, BorderLayout.CENTER);
    _popComponentLocationPanel.add(_popComponentLocationScrollPane, BorderLayout.CENTER);

    // Setup the region split pane. Scroll panes are added when needed.
    _regionOrSidePanel.setLayout(new BorderLayout());
    _regionOrSidePanel.add(_regionOrSideControlPanel, BorderLayout.NORTH);

    _lastRegionButton = new JButton(StringLocalizer.keyToString("CAD_LAST_REGION_BUTTON_KEY"),
                                     Image5DX.getImageIcon(Image5DX.VC_UP_ARROW));
    _lastRegionButton.setToolTipText(StringLocalizer.keyToString("CAD_LAST_REGION_TOOLTIP_KEY"));
    _lastRegionButton.setHorizontalAlignment(SwingConstants.RIGHT);
    _lastRegionButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectPreviousTableRow(_regionOrSideTable, _lastRegionButton, _nextRegionButton);
      }
    });
    _regionOrSideControlPanel.add(_lastRegionButton);
    _nextRegionButton = new JButton(StringLocalizer.keyToString("CAD_NEXT_REGION_BUTTON_KEY"),
                                     Image5DX.getImageIcon(Image5DX.VC_DOWN_ARROW));
    _nextRegionButton.setToolTipText(StringLocalizer.keyToString("CAD_NEXT_REGION_TOOLTIP_KEY"));
    _nextRegionButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectNextTableRow(_regionOrSideTable, _lastRegionButton, _nextRegionButton);
      }
    });
    _regionOrSideControlPanel.add(_nextRegionButton);

    _regionOrSidePanel.add(_regionOrSideScrollPane, BorderLayout.CENTER);

    _regionOrSideSplitPaneDivider = -1;

    // graphics move controls
    _southPanel.setLayout(new BorderLayout());
    _southPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(
      Color.white,new Color(165, 163, 151)),StringLocalizer.keyToString("CAD_ADJUST_GRAPHICALLY_PANEL_KEY")));
    add(_southPanel, BorderLayout.SOUTH);

    _adjustGraphicsPanel.setLayout(new PairLayout(10, 10, false));
    _southPanel.add(_adjustGraphicsPanel, BorderLayout.CENTER);

    _adjustOffRadioButton.setText(StringLocalizer.keyToString("CAD_MOVE_OFF_BUTTON_KEY"));
    _adjustOffRadioButton.setToolTipText(StringLocalizer.keyToString("CAD_MOVE_OFF_TOOLTIP_KEY"));
    _adjustOffRadioButton.setSelected(true);
    _lastButton = _adjustOffRadioButton;
    _adjustOffRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustOffRadioButton_actionPerformed();
      }
    });
    _adjustGraphicsButtonGroup.add(_adjustOffRadioButton);
    _adjustGraphicsPanel.add(_adjustOffRadioButton);

    _adjustComponentRadioButton.setText(StringLocalizer.keyToString("CAD_MOVE_COMPONENT_BUTTON_KEY"));
    _adjustComponentRadioButton.setToolTipText(StringLocalizer.keyToString("CAD_MOVE_COMPONENT_TOOLTIP_KEY"));
    _adjustComponentRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustComponentRadioButton_actionPerformed();
      }
    });
    _adjustGraphicsButtonGroup.add(_adjustComponentRadioButton);
    _adjustGraphicsPanel.add(_adjustComponentRadioButton);

    _adjustBoardSideRadioButton.setText(StringLocalizer.keyToString("CAD_MOVE_BOARD_SIDE_BUTTON_KEY"));
    _adjustBoardSideRadioButton.setToolTipText(StringLocalizer.keyToString("CAD_MOVE_BOARD_SIDE_TOOLTIP_KEY"));
    _adjustBoardSideRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustBoardSideRadioButton_actionPerformed();
      }
    });
    _adjustGraphicsButtonGroup.add(_adjustBoardSideRadioButton);
    _adjustGraphicsPanel.add(_adjustBoardSideRadioButton);

    _adjustBoardOriginRadioButton.setText(StringLocalizer.keyToString("CAD_MOVE_BOARD_ORIGIN_BUTTON_KEY"));
    _adjustBoardOriginRadioButton.setToolTipText(StringLocalizer.keyToString("CAD_MOVE_BOARD_ORIGIN_TOOLTIP_KEY"));
    _adjustBoardOriginRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        adjustBoardOriginRadioButton_actionPerformed();
      }
    });
    _adjustGraphicsButtonGroup.add(_adjustBoardOriginRadioButton);
    _adjustGraphicsPanel.add(_adjustBoardOriginRadioButton);

    _applyAdjustmentButton.setText(StringLocalizer.keyToString("CAD_APPLY_ADJUSTMENT_BUTTON_KEY"));
    _applyAdjustmentButton.setToolTipText(StringLocalizer.keyToString("CAD_APPLY_ADJUSTMENT_TOOLTIP_KEY"));
    _applyAdjustmentButton.setEnabled(false);
    _applyAdjustmentButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveAdjustmentButton_actionPerformed();
      }
    });
    _adjustGraphicsPanel.add(_applyAdjustmentButton);

    _cancelAdjustmentButton.setText(StringLocalizer.keyToString("CAD_CANCEL_ADJUSTMENT_BUTTON_KEY"));
    _cancelAdjustmentButton.setToolTipText(StringLocalizer.keyToString("CAD_CANCEL_ADJUSTMENT_TOOLTIP_KEY"));
    _cancelAdjustmentButton.setEnabled(false);
    _cancelAdjustmentButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelAdjustmentButton_actionPerformed();
      }
    });
    _adjustGraphicsPanel.add(_cancelAdjustmentButton);
        
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    _mergeComponentToPOPMenuItem.setText(StringLocalizer.keyToString("CAD_MERGE_COMPONENTS_INTO_POP_MENU_ITEM_KEY"));
    _mergeComponentsToPOPPopupMenu.add(_mergeComponentToPOPMenuItem);
    _mergeComponentToPOPMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        mergeComponentToPOPMenuItemActionPerformed(e);
      }
    });
    
    // Set controls to initial states (nothing selected)
    updateLastNextButtons(-1, _componentLocationTable, _lastComponentButton, _nextComponentButton);
    updateLastNextButtons(-1, _packageJointTypeTable, _lastPackageButton, _nextPackageButton);
    updateLastNextButtons(-1, _regionOrSideTable, _lastRegionButton, _nextRegionButton);
    updateAdjustButtons();

    _ignoreActions = false;
  }

  /**
   * @author George Booth
   * @edited by Kee Chin Seong
   * @edited by Siew Yeng - XCR-2123 - select multiple components
   * - refer to Modify Subtypes - ModifySubtypesComponentTable
   */
  private void componentLocationTableSetValue(final Object value, final int row, final int column)
  {
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(MainMenuGui.getInstance(),
                                                                   "Updating Component Table and CAD...",
                                                                   StringLocalizer.keyToString("BUSYCANCELDIALOG_UNDO_REDO_TITLE_KEY"),
                                                                   true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, MainMenuGui.getInstance());
    busyCancelDialog.setCursor(new Cursor(Cursor.WAIT_CURSOR));

    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          int rows[] = _componentLocationTable.getSelectedRows();
          int col = _componentLocationTable.convertColumnIndexToModel(column);
          BoardType _currentBoardType = _mainUI.getTestDev().getCurrentBoardType();
          String newShadedSubtypeNames[] = new String[rows.length];
          boolean isSettingNewShadedSubtypeName = false;
          _isUserChangeJointType = false;
          String valStr = "";
          Object val = value;

          if(col == 1 || col == 2)
          {
            if(val instanceof Long)
            {
              val = Double.parseDouble(val.toString());
            }

            if(val.equals(_componentLocationTableModel.getValueAt(row, column)))
              return;

            //Siew Yeng - XCR-2123 - temporary revert back to original behavior. Will continue to use this in 5.8
            Double longValue = (Double)val;
            /*
            Object[] options = {StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_ACTUAL_LOCATION_OPTION_KEY"), 
                                    StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_OFFSET_FROM_ACTUAL_LOCATION_OPTION_KEY")}; 
            String prompt = StringLocalizer.keyToString(new LocalizedString("MMGUI_PAD_LAND_PATTERN_LOCATION_OR_OFFSET_KEY",
                                                        new Object[]{longValue}));
            String title = (column == 1) ? StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_LOCATION_OR_OFFSET_X_TITLE_KEY") :
                                           StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_LOCATION_OR_OFFSET_Y_TITLE_KEY");

            ChoiceRadioButtonDialog loadDialog = new ChoiceRadioButtonDialog(MainMenuGui.getInstance(),
                                                             title,
                                                             prompt,
                                                             StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                                             StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                                             options,
                                                             null);

            int retVal = loadDialog.showDialog();

            if (retVal == JOptionPane.OK_OPTION) 
            {
              if(loadDialog.getSelectedValue().equals(StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_ACTUAL_LOCATION_OPTION_KEY")))
              {
                if(longValue < 0)
                {
                  SwingUtils.invokeLater(new Runnable()
                  {
                    public void run()
                    {
                      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                                     StringLocalizer.keyToString("CAD_X_Y_COORD_CANNOT_BE_NEGATIVE_KEY"),
                                                     StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
                                                     true);
                    }
                  });
                  return;
                }
                setIsOffsetBeenSet(false);
              }
              else if(loadDialog.getSelectedValue().equals(StringLocalizer.keyToString("MMGUI_PAD_LAND_PATTERN_OFFSET_FROM_ACTUAL_LOCATION_OPTION_KEY")))
                setIsOffsetBeenSet(true);
            }
            else // the use hit cancel or entered an empty string -- change nothing
               return;
            */

            java.util.List<String> componentTypeOutsideBoardTypeOutline = new ArrayList();

            int coord = (int)MathUtil.convertUnits(longValue, _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);
            for(int i = 0; i < rows.length; i++)
            {
              ComponentType componentType = _componentLocationTableModel.getComponentTypeAt(rows[i]);

              BoardCoordinate boardCoordinate = componentType.getCoordinateInNanoMeters();
              if(col == 1)
              {
                if(isOffsetBeenSet())
                  boardCoordinate.setX(boardCoordinate.getX() + coord);
                else
                  boardCoordinate.setX(coord);
              }
              else
              {
                if(isOffsetBeenSet())
                  boardCoordinate.setY(boardCoordinate.getY() + coord);
                else
                  boardCoordinate.setY(coord);
              }

              if(componentType.willComponentTypeBeInsideBoardTypeOutlineAfterCoordinate(boardCoordinate) == false)
              {
                componentTypeOutsideBoardTypeOutline.add(componentType.getReferenceDesignator());
                break;
              }
            }

            if(componentTypeOutsideBoardTypeOutline.isEmpty() == false)
            {
              String message = StringLocalizer.keyToString(new LocalizedString("CAD_X_COORD_OUT_OF_BOUNDS_WAS_RESET_KEY",
                    new Object[]{StringLocalizer.keyToString("CAD_COMPONENT_KEY"),
                    componentTypeOutsideBoardTypeOutline.get(0),
                    StringLocalizer.keyToString("CAD_BOARD_KEY")}));

              if(col == 2)
              {
                message = StringLocalizer.keyToString(new LocalizedString("CAD_Y_COORD_OUT_OF_BOUNDS_WAS_RESET_KEY",
                    new Object[]{StringLocalizer.keyToString("CAD_COMPONENT_KEY"),
                    componentTypeOutsideBoardTypeOutline.get(0),
                    StringLocalizer.keyToString("CAD_BOARD_KEY")}));
              }
              _componentLocationTableModel.showLocationErrorDialog(message);
              return;
            }
          }
          else if(col == 3)
          {
            if(val.equals(_componentLocationTableModel.getValueAt(row, column).toString()))
              return;

            java.util.List<String> componentTypeOutsideBoardTypeOutline = new ArrayList();
            Double doubleValue = new Double(val.toString());
            for(int i = 0; i < rows.length; i++)
            {
              ComponentType componentType = _componentLocationTableModel.getComponentTypeAt(rows[i]);

              if (componentType.willComponentTypeBeInsideBoardTypeOutlineAfterRotation(doubleValue) == false)
              {
                componentTypeOutsideBoardTypeOutline.add(componentType.getReferenceDesignator());
                break;
              }
            }

            if(componentTypeOutsideBoardTypeOutline.isEmpty() == false)
            {
              _componentLocationTableModel.showLocationErrorDialog(
                    StringLocalizer.keyToString(new LocalizedString("CAD_ROTATION_OUT_OF_BOUNDS_WAS_RESET_KEY",
                                                                    new Object[]{StringLocalizer.keyToString("CAD_COMPONENT_KEY"),
                                                                    componentTypeOutsideBoardTypeOutline.get(0),
                                                                    StringLocalizer.keyToString("CAD_BOARD_KEY")})));
              return;
            }
          }
          else if(col == 4) //joint type column
          {
            _isUserChangeJointType = true;
            JointTypeEnum newJointTypeEnum = (JointTypeEnum)val;

            for(int i = 0; i < rows.length; i++)
            {
              ComponentType componentType = _componentLocationTableModel.getComponentTypeAt(rows[i]);

              // only allow this change if the throughhole/smt land pattern is compatible with the new joint type
              if (componentType.getLandPattern().getPadTypeEnum().equals(PadTypeEnum.THROUGH_HOLE) == false)
              {
                if (ImageAnalysis.requiresThroughHoleLandPatternPad(newJointTypeEnum))
                {
                  SwingUtils.invokeLater(new Runnable()
                  {
                    public void run()
                    {
                      // give error, and do not do the operation
                      MessageDialog.showErrorDialog(
                          MainMenuGui.getInstance(),
                          StringLocalizer.keyToString("MMGUI_GROUPINGS_JOINT_TYPE_MUST_BE_THROUGHHOLE_KEY"),
                          StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
                          true);
                    }
                  });
                  
                  return;
                }
              }
            }
          }
          // special case of setting the subtype to  "New..."  We need to bring up a dialog
          // to ask them for the name of the new subtype
          else if (col == 5) // subtype column
          {
            if (val instanceof String)
            {
              valStr = (String)val;
              if (valStr.equalsIgnoreCase(StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY"))) // mixed case
              {
                // the user is not allowed to set the subtype to mixed.  This result is because padtypes are set to different subtypes
                // and the resulting table entry is "mixed".  So, if this is the selection, it's because I show "mixed" in the subtype
                // combo box, and the user selected it.  I show "mixed" because it's current choice, and it's the default selection anyway.
                // So, if they try to set it to "mixed", it's best to ignore this, and treat it as a cancel edit.
                return;
              }
              if (valStr.equalsIgnoreCase(_NEW_SHADED_SUBTYPE))
              {           
                isSettingNewShadedSubtypeName = true;
               //get the row and col and append the new name to current subype.
                String prompt = StringLocalizer.keyToString("MMGUI_SUBTYPE_NEW_SHADED_PROMPT_KEY");
                String title = StringLocalizer.keyToString("MMGUI_SUBTYPE_NEW_SHADED_TITLE_KEY");
                String retVal = JOptionPane.showInputDialog(MainMenuGui.getInstance(),
                                                          prompt,
                                                          title,
                                                          JOptionPane.QUESTION_MESSAGE);

                if ((retVal == null) || (retVal.length() == 0)) // the use hit cancel or entered an empty string -- change nothing
                    return;

                com.axi.v810.business.panelDesc.Panel panel = _currentBoardType.getPanel();
                // Run through each row of subtype name and append "_SHADED" on the name
                for(int i = 0; i < rows.length; i++)
                {
                  String subtypeName = (String)_componentLocationTableModel.getValueAt(rows[i], col);
                  String newSubtypeName = subtypeName + "_" + retVal;

                  // First, check to see if the component is set to load or no test
                  ComponentType componentType = _componentLocationTableModel.getComponentTypeAt(rows[i]);
                  if(componentType.getSubtypes().size() > 1)
                  {
                     JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                                StringLocalizer.keyToString("CAD_MIXED_SUBTYPE_NOT_ALLOWED_APPEND_NAME_KEY"),
                                                StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                                                JOptionPane.WARNING_MESSAGE);
                     return;
                  }
                  if (componentType.isInspected() == false || componentType.isLoaded() == false
                      || componentType.getSubtypes().size() > 1)
                  {
                    newShadedSubtypeNames[i] = _NOT_APPLICABLE;
                  }
                  else
                  {
                     //com.axi.v810.business.panelDesc.Panel panel = componentType.getSideBoardType().getBoardType().getPanel();                
                     if (panel.isSubtypeNameValid(newSubtypeName))
                        newShadedSubtypeNames[i] = newSubtypeName;
                     else
                        newShadedSubtypeNames[i] = subtypeName;
                  }
                  
                  JointTypeEnum jointTypeEnum = componentType.getPadTypeOne().getJointTypeEnum();
                  if (panel.doesSubtypeShortNameAndJointTypeExist(newShadedSubtypeNames[i], jointTypeEnum))
                  {
                    String warningMessage = StringLocalizer.keyToString(new LocalizedString("RMGUI_NEW_SUBTYPE_EXIST_ERROR_KEY",
                                                                        new String[]{newShadedSubtypeNames[i], jointTypeEnum.getName()}));
                    JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                                  warningMessage,
                                                  StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                                                  JOptionPane.WARNING_MESSAGE);
                    return;
                  }
                }
              }
              else if (valStr.equalsIgnoreCase(_NEW_SUBTYPE))
              {
                String prompt = StringLocalizer.keyToString("MMGUI_SUBTYPE_NEW_PROMPT_KEY");
                String title = StringLocalizer.keyToString("MMGUI_SUBTYPE_NEW_TITLE_KEY");
                String retVal = JOptionPane.showInputDialog(_mainUI,
                    prompt,
                    title,
                    JOptionPane.QUESTION_MESSAGE);

                if ((retVal == null) || (retVal.length() == 0)) // the use hit cancel or entered an empty string -- change nothing
                  return;
                com.axi.v810.business.panelDesc.Panel panel = _currentBoardType.getPanel();
                if (panel.isSubtypeNameValid(retVal) == false)
                {
                  // explain why it's invalid
                  LocalizedString message;
                  String illegalSubtypeCharacters = panel.getSubtypeNameIllegalChars();
                  if (illegalSubtypeCharacters.equals(" "))
                  {
                    message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_ONLY_SPACES_KEY",
                                                  new Object[]
                                                  {retVal});
                  }
                  else
                  {
                    message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_OTHER_CHARS_KEY",
                                                  new Object[]
                                                  {retVal, illegalSubtypeCharacters});
                  }
                  JOptionPane.showMessageDialog(
                      _mainUI,
                      StringLocalizer.keyToString(message),
                      StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                      JOptionPane.ERROR_MESSAGE);

                  return;
                }
                val = retVal;
                
                String newSubtypeName = (String)val;
                for(int i = 0; i < rows.length; ++i)
                {
                  ComponentType componentType = _componentLocationTableModel.getComponentTypeAt(rows[i]);
                  JointTypeEnum jointTypeEnum = componentType.getPadTypeOne().getJointTypeEnum();

                  //com.axi.v810.business.panelDesc.Panel panel = _currentBoardType.getPanel();
                  if (panel.doesSubtypeShortNameAndJointTypeExist(newSubtypeName, jointTypeEnum))
                  {
                    String warningMessage = StringLocalizer.keyToString(new LocalizedString("RMGUI_NEW_SUBTYPE_EXIST_ERROR_KEY",
                                                                        new String[]{newSubtypeName, jointTypeEnum.getName()}));
                    JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                                  warningMessage,
                                                  StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                                                  JOptionPane.WARNING_MESSAGE);
                    return;
                  }
                }

                //write into history log-hsia-fen.tan
                _historyLogWriter.appendCreateSubtype(_componentLocationTableModel.getCurrentlySelectedComponents(),retVal);
              }
              else if (valStr.equalsIgnoreCase(_NOLOAD_SUBTYPE))
              {
                // no Load
                _affectedAlignmentPads.clear();
                for(int i = 0; i < rows.length ; i++)
                {
                  ComponentType componentType = _componentLocationTableModel.getComponentTypeAt(rows[i]);

                  //Siew Yeng - XCR1719 
                  //keep track of affected alignment pads when alignment component are going to set to no load.
                  for(Pad pad: _currentBoardType.getPanel().getPanelSettings().getAllAlignmentPads())
                  {
                    if(componentType.getReferenceDesignator().equals(pad.getComponent().getReferenceDesignator()))
                    {
                      _affectedAlignmentPads.add(pad); 
                    }
                  }
                }

                int status = JOptionPane.CANCEL_OPTION;
                if(_affectedAlignmentPads.isEmpty() == false)
                {
                  status = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(), 
                  StringLocalizer.keyToString("MMGUI_SUBTYPE_ALIGNMENT_JOINT_SET_TO_NO_LOAD_WARNING_KEY"),
                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), 
                  JOptionPane.YES_NO_CANCEL_OPTION, 
                  JOptionPane.QUESTION_MESSAGE);

                  if(status == JOptionPane.CANCEL_OPTION || status == JOptionPane.CLOSED_OPTION)
                  {
                    return;
                  }    
                }

                _commandManager.trackState(getCurrentUndoState());
                _commandManager.beginCommandBlock(StringLocalizer.keyToString("MMGUI_NO_LOAD_GROUP_UNDO_KEY"));
                for(int i = 0; i < rows.length ; i++)
                {
                  ComponentType componentType = _componentLocationTableModel.getComponentTypeAt(rows[i]);
                  try
                  {
                    if(status == JOptionPane.YES_OPTION)
                    {
                      for(Pad pad : _affectedAlignmentPads)
                      {
                        if(componentType.getReferenceDesignator().equals(pad.getComponent().getReferenceDesignator()))
                        {
                          AlignmentGroup alignmentGroup = _currentBoardType.getPanel().getPanelSettings().getAlignmentGroupFromPad(pad);
                          if(alignmentGroup == null)
                          {
                            System.out.println("Alignment Group not found for Pad: "+pad.getBoardAndComponentAndPadName());
                            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                                          StringLocalizer.keyToString("MMGUI_RECIPE_FILE_CORRUPTED_WARNING_KEY"), 
                                                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                          JOptionPane.WARNING_MESSAGE);
                            break;
                          }
                          else
                            _commandManager.execute(new RemoveFeaturesFromAlignmentGroupCommand(alignmentGroup,
                                                                                                new ArrayList<>(Arrays.asList(pad)),
                                                                                                new ArrayList()));
                        }
                      }
                    }
                    else if(status == JOptionPane.NO_OPTION)
                    {
                      boolean skip = false;
                      for(Pad pad : _affectedAlignmentPads)
                      {
                        if(componentType.getReferenceDesignator().equals(pad.getComponent().getReferenceDesignator()))
                        {
                          skip = true;
                          break;
                        }
                      }
                      if(skip)
                        continue;
                    }           
                    _commandManager.execute(new ComponentTypeSetLoadedCommand(componentType, false));
                  }
                  catch (XrayTesterException ex)
                  {
                    MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                                  ex.getLocalizedMessage(),
                                                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                  true);
                    _componentLocationTableModel.fireTableCellUpdated(row, column);
                  }
                  _historyLogWriter.appendSubtypeNoLoadNoTest(componentType,valStr);
                }
                _commandManager.endCommandBlock();
                return;
              }
              else if (valStr.equalsIgnoreCase(_NOTEST_SUBTYPE))
              {
                // no Test
                _commandManager.trackState(getCurrentUndoState());
                _commandManager.beginCommandBlock(StringLocalizer.keyToString("MMGUI_NO_TEST_GROUP_UNDO_KEY"));

                for(int i = 0; i < rows.length; i++)
                {
                  ComponentType componentType = _componentLocationTableModel.getComponentTypeAt(rows[i]);

                  try
                  {
                    // setting something to NO TEST implies that it is loaded.
                    _commandManager.execute(new ComponentTypeSetLoadedCommand(componentType, true));
                    _commandManager.execute(new ComponentTypeSetTestedCommand(componentType, false));
                  }
                  catch (XrayTesterException ex)
                  {
                    MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                                  ex.getLocalizedMessage(),
                                                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                  true);
                    _componentLocationTableModel.fireTableCellUpdated(row, column);
                  }
                  _historyLogWriter.appendSubtypeNoLoadNoTest(componentType,valStr);
                }
                _commandManager.endCommandBlock();
                return;
              }
              else if (valStr.startsWith(_RESTORE_SUBTYPE))
              {
                // Set to test -- set manually to each row
                _commandManager.trackState(getCurrentUndoState());
                _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMP_TYPE_SET_TESTED_KEY"));

                for(int i = 0; i < rows.length; i++)
                {
                  ComponentType componentType = _componentLocationTableModel.getComponentTypeAt(rows[i]);
                  try
                  {
                    _commandManager.execute(new ComponentTypeSetTestedCommand(componentType, true));
                    // setting something to be tested implies that it is loaded.
                    _commandManager.execute(new ComponentTypeSetLoadedCommand(componentType, true));
                  }
                  catch (XrayTesterException ex)
                  {
                    MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                                  ex.getLocalizedMessage(),
                                                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                  true);
                    _componentLocationTableModel.fireTableCellUpdated(row, column);
                  }
                  _historyLogWriter.appendSubtypeNoLoadNoTest(componentType,valStr);
                }
                _commandManager.endCommandBlock();
                return;
              }
              else if (valStr.equalsIgnoreCase(_RENAME_SUBTYPE))
              {
                ComponentType componentType = _componentLocationTableModel.getComponentTypeAt(_componentLocationTable.getSelectedRow());
                String currentSubtype = componentType.getComponentTypeSettings().getSubtype().getShortName();
                String prompt = StringLocalizer.keyToString("MMGUI_SUBTYPE_RENAME_PROMPT_KEY");
                String title = StringLocalizer.keyToString("MMGUI_SUBTYPE_RENAME_TITLE_KEY");
                Object retVal = JOptionPane.showInputDialog(_mainUI,
                    prompt,
                    title,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    null,
                    currentSubtype);
                String newName = (String)retVal;
                if ((retVal == null) || (newName.length() == 0)) // the use hit cancel or entered an empty string -- change nothing
                  return;
                com.axi.v810.business.panelDesc.Panel panel = _currentBoardType.getPanel();
                if (panel.isSubtypeNameValid(newName) == false)
                {
                  // explain why it's invalid
                  LocalizedString message;
                  String illegalSubtypeCharacters = panel.getSubtypeNameIllegalChars();
                  if (illegalSubtypeCharacters.equals(" "))
                  {
                    message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_ONLY_SPACES_KEY",
                                                  new Object[]
                                                  {retVal});
                  }
                  else
                  {
                    message = new LocalizedString("MMGUI_SUBTYPE_NAME_INVALID_OTHER_CHARS_KEY",
                                                  new Object[]
                                                  {retVal, illegalSubtypeCharacters});
                  }
                  JOptionPane.showMessageDialog(_mainUI,
                                                StringLocalizer.keyToString(message),
                                                StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                                JOptionPane.ERROR_MESSAGE);

                  return;
                }
                try
                {
                  // XCR1740 - Add in ?Append Name? option in Advance Subtype windows, as in ?Modify Subtypes? page
                  // by Lim Seng Yew on 27-Dec-2013
                  // Put checking if new subtype (with same joint type) already exist.
                  JointTypeEnum jointTypeEnum = componentType.getPadTypeOne().getJointTypeEnum();
                  if (panel.doesSubtypeShortNameAndJointTypeExist(newName, jointTypeEnum))
                  {
                    String warningMessage = StringLocalizer.keyToString(new LocalizedString("RMGUI_NEW_SUBTYPE_EXIST_ERROR_KEY",
                                                                        new String[]{newName, jointTypeEnum.getName()}));
                    JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                                  warningMessage,
                                                  StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                                                  JOptionPane.WARNING_MESSAGE);
                    return;
                  }
                  _commandManager.trackState(getCurrentUndoState());
                  _commandManager.execute(new SubtypeSetNameCommand(componentType.getComponentTypeSettings().getSubtype(), newName));
                }
                catch (XrayTesterException ex)
                {
                  MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                                ex.getLocalizedMessage(),
                                                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                true);
                  _componentLocationTableModel.fireTableCellUpdated(row, column);
                }
                return ;
              }
            }
          }

          _commandManager.trackState(getCurrentUndoState());
          if (column == 1)
            _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_X_LOCATION_KEY"));
          else if (column == 2)
            _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_Y_LOCATION_KEY"));
          else if (column == 3)
            _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_ROTATION_KEY"));
          else if (column == 4)
            _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_JOINTTYPE_KEY"));
          else if (column == 5)
            _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_SUBTYPE_KEY"));

          for(int i = 0; i < rows.length; ++i)
          {
            ComponentType componentType = _componentLocationTableModel.getComponentTypeAt(rows[i]);
            if (componentType.isInspected() && componentType.isLoaded())
            {  
              if (isSettingNewShadedSubtypeName)
                _componentLocationTableModel.setValueAt(newShadedSubtypeNames[i], rows[i], col);
              //Siew Yeng - XCR-2123 - temporary revert back to original behavior. Will continue to use this in 5.7
              else if(isOffsetBeenSet() && (column == 1 || column == 2))
                _componentLocationTableModel.setValueAt((Double)_componentLocationTableModel.getValueAt(rows[i], col) + (Double)val, rows[i], col);
              else
                _componentLocationTableModel.setValueAt(val, rows[i], col);
            }
          }
          _commandManager.endCommandBlock();
        }
        finally
        {
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.dispose();
        }
      }
    });
    busyCancelDialog.setVisible(true);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  private void popComponentLocationTableSetValue(final Object valueObj, final int row, final int column)
  {
    if (_popComponentLocationTableModel.getComponentTypeAt(row) == null)
      return;
    
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(MainMenuGui.getInstance(),
                                                                   "Updating Component Table and CAD...",
                                                                   StringLocalizer.keyToString("BUSYCANCELDIALOG_UNDO_REDO_TITLE_KEY"),
                                                                   true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, MainMenuGui.getInstance());
    busyCancelDialog.setCursor(new Cursor(Cursor.WAIT_CURSOR));

    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          int rows[] = _popComponentLocationTable.getSelectedRows();
          int col = _popComponentLocationTable.convertColumnIndexToModel(column);
          Object val = valueObj;

          if(col == 3) //POP Z-Height column
          {
            Assert.expect(val instanceof Number);
            Assert.expect(_project != null);

            Double value = ((Number)val).doubleValue();
            // convert value in display units to nanometers
            int newPOPZHeightInNanometers = (int)MathUtil.convertUnits(value, _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);      

            if (newPOPZHeightInNanometers == _popComponentLocationTableModel.getPOPZHeightInNanometersAt(row))
              return; 
          }
          
          _commandManager.trackState(getCurrentUndoState());
          if (column == 3)
            _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_SET_POP_Z_HEIGHT_KEY"));
          
          for(int i = 0; i < rows.length; ++i)
          {
            _popComponentLocationTableModel.setValueAt(val, rows[i], col);
          }
          _commandManager.endCommandBlock();
        }
        finally
        {
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.dispose();
        }
      }
    });
    busyCancelDialog.setVisible(true);
  }

  /**
   * @author George Booth
   */
  public void componentLocationTable_valueChanged(ListSelectionEvent e)
  {
    if (_ignoreActions)
    {
//      System.out.println("componentLocationTable_valueChanged() is ignoring actions");
      return;
    }
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    if (lsm.getValueIsAdjusting())
    {
//      System.out.println("componentLocationTable is adjusting");
      // do nothing
    }
    else if (lsm.isSelectionEmpty())
    {
//      System.out.println("componentLocationTable selection is empty");
      // if we are selecting a component graphically, this is transient
      if (_componentTypeSelectedGraphically == null)
      {
        updateLastNextButtons( -1, _componentLocationTable, _lastComponentButton, _nextComponentButton);
      }
    }
    else
    {
      // turn off event processing until graphics had a chance to update
      _componentLocationTable.processSelections(false);
      // component was selected with a table event
      //Siew Yeng - XCR-2123
      int[] selectedComponentRows = _componentLocationTable.getSelectedRows();
      java.util.List<ComponentType> selectedComponents = new ArrayList<ComponentType>();
      for(int i = 0; i < selectedComponentRows.length; i++)
      {
        selectedComponents.add(_componentLocationTableModel.getComponentTypeAt(selectedComponentRows[i]));
      }
      
      //Chin Seong - to make sure that the single component also updated for adjust component we only allowed 1 component to be adjust at once!
      _componentLocationTableModel.setSelectedComponent(_currentBoard, selectedComponents.get(0), selectedComponentRows[0]);
      _componentLocationTableModel.setSelectedComponents(_currentBoard, selectedComponents);
      
      int selectedRow = lsm.getMinSelectionIndex();
//      System.out.println("componentLocationTable row " + selectedRow + " selected");
      _currentComponentIndex = selectedRow;
      _currentComponent = _componentLocationTableModel.getComponentTypeAt(selectedRow);
      _currentComponentName = _currentComponent.getReferenceDesignator();
//      _componentLocationTableModel.setSelectedComponent(_currentBoard, _currentComponent, _currentComponentIndex);
//      System.out.println("currentComponent = " + _currentComponentName);

      updateLastNextButtons(_currentComponentIndex, _componentLocationTable, _lastComponentButton, _nextComponentButton);
      updateAdjustButtons();

      // after selection, scroll to make sure the row is visible.
//      SwingUtils.scrollTableToShowSelection(_componentLocationTable);

      // set CAD graphics crosshairs on board and component
      highlightBoardAndComponent();

      // wait for graphics
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          _componentLocationTable.processSelections(true);
        }
      });
    }
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void popComponentLocationTable_valueChanged(ListSelectionEvent e)
  {
    if (_ignoreActions)
    {
//      System.out.println("componentLocationTable_valueChanged() is ignoring actions");
      return;
    }
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    if (lsm.getValueIsAdjusting())
    {
//      System.out.println("componentLocationTable is adjusting");
      // do nothing
    }
    else if (lsm.isSelectionEmpty())
    {
//      System.out.println("componentLocationTable selection is empty");
      // do nothing 
    }
    else
    {
      // turn off event processing until graphics had a chance to update
      _popComponentLocationTable.processSelections(false);
      // component was selected with a table event
      //Siew Yeng - XCR-2123
      int[] selectedComponentRows = _popComponentLocationTable.getSelectedRows();
      java.util.List<ComponentType> selectedComponents = new ArrayList<ComponentType>();
      for(int i = 0; i < selectedComponentRows.length; i++)
      {
        selectedComponents.add(_popComponentLocationTableModel.getComponentTypeAt(selectedComponentRows[i]));
      }
      
      _popComponentLocationTableModel.setSelectedComponentTypes(selectedComponents);
      
      int selectedRow = lsm.getMinSelectionIndex();
//      System.out.println("componentLocationTable row " + selectedRow + " selected");
      _currentComponentIndex = selectedRow;
      _currentComponent = _popComponentLocationTableModel.getComponentTypeAt(selectedRow);
      _currentComponentName = _currentComponent.getReferenceDesignator();

      // after selection, scroll to make sure the row is visible.
//      SwingUtils.scrollTableToShowSelection(_componentLocationTable);

      // set CAD graphics crosshairs on board and component
      highlightBoardAndComponent();

      // wait for graphics
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          _popComponentLocationTable.processSelections(true);
        }
      });
    }
  }
  

  void selectCurrentComponent()
  {
    // clear the current selection
    _componentLocationTable.cancelCellEditing();
    _componentLocationTable.clearSelection();
    _componentLocationTableModel.clearSelectedComponent();

    // select it
    if (_componentLocationTable.getRowCount() > 0)
    {
      if (_currentComponent != null)
      {
        selectComponentTableRow(_componentLocationTableModel.getRowForComponentType(_currentComponent));
      }
      else
      {
        selectComponentTableRow(0);
        _currentComponent = _componentLocationTableModel.getComponentTypeAt(0);
        _currentComponentName = _currentComponent.getReferenceDesignator();
      }
    }
//    System.out.println("currentComponent = " + _currentComponentName);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  void selectCurrentComponentForPOPTable()
  {    
    // clear the current selection
    _popComponentLocationTable.cancelCellEditing();
    _popComponentLocationTable.clearSelection();
    _popComponentLocationTableModel.clearSelectedComponent();
    
    // select it
    if (_popComponentLocationTableModel.getRowCount() > 0)
    {
      if (_currentComponent != null && _currentComponent.hasCompPackageOnPackage() && _popComponentLocationTableModel.getComponentTypes().contains(_currentComponent))
      {
        selectPOPComponentTableRow(_popComponentLocationTableModel.getRowForComponentType(_currentComponent));
      }
      else
      {      
        selectPOPComponentTableRow(0);
        _currentComponent = _popComponentLocationTableModel.getComponentTypeAt(0);
        _currentComponentName = _currentComponent.getReferenceDesignator();
      }
    }
//    System.out.println("currentComponent = " + _currentComponentName);
  }
  
  /**
   * @author George Booth
   */
  void selectComponentTableRow(int row)
  {
    _currentComponentIndex = row;
    _currentComponent = _componentLocationTableModel.getComponentTypeAt(row);
//    _componentLocationTableModel.setSelectedComponent(_currentBoard, _currentComponent, _currentComponentIndex);
    Collection<ComponentType> selectedComponent = new ArrayList();
    selectedComponent.add(_currentComponent);
    _componentLocationTableModel.setSelectedComponents(_currentBoard, selectedComponent);
    updateAdjustButtons();
    selectTableRow(_currentComponentIndex, _componentLocationTable,
                   _lastComponentButton, _nextComponentButton);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  void selectPOPComponentTableRow(int row)
  {
    _currentComponentIndex = row;
    _currentComponent = _popComponentLocationTableModel.getComponentTypeAt(row);
//    _componentLocationTableModel.setSelectedComponent(_currentBoard, _currentComponent, _currentComponentIndex);
    Collection<ComponentType> selectedComponent = new ArrayList();
    selectedComponent.add(_currentComponent);
    _popComponentLocationTableModel.setSelectedComponentTypes(selectedComponent);
    updateAdjustButtons();
    selectTableRow(_currentComponentIndex, _popComponentLocationTable,
                   _lastComponentButton, _nextComponentButton);
  }
  
  /**
   * @author George Booth
   */
  private void packageJointTypeTableSetValue(Object value, int row, int column)
  {
    //Chin Seong - fix adjust cad need to regenerate verification image when change joint type
    if(column == 1)
      _isUserChangeJointType = true;
    else
      _isUserChangeJointType = false;
    
    _commandManager.trackState(getCurrentUndoState());
    _packageJointTypeTableModel.setValueAt(value, row, column);
  }

  /**
   * @author George Booth
   */
  public void packageJointTypeTable_valueChanged(ListSelectionEvent e)
  {
    if (_ignoreActions)
    {
//      System.out.println("packageJointTypeTable_valueChanged() is ignoring actions");
      return;
    }
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    if (lsm.getValueIsAdjusting())
    {
//      System.out.println("packageJointTypeTable is adjusting");
      // do nothing
    }
    else if (lsm.isSelectionEmpty())
    {
//      System.out.println("packageJointTypeTable selection is empty");
      updateLastNextButtons(-1, _packageJointTypeTable, _lastPackageButton, _nextPackageButton);
    }
    else
    {
      // turn off event processing until graphics had a chance to update
      _packageJointTypeTable.processSelections(false);
      // package was selected with a table event
      _currentPackageIndex = lsm.getMinSelectionIndex();
//      System.out.println("packageJointTypeTable row " + _currentPackageIndex + " selected");
      CompPackage currentCompPackage = _packageJointTypeTableModel.getPackageAt(_currentPackageIndex);
      _packageJointTypeTableModel.setSelectedPackage(currentCompPackage);
      updateLastNextButtons(_currentPackageIndex, _packageJointTypeTable, _lastPackageButton, _nextPackageButton);

      // after selection, scroll to make sure the row is visible.
//      SwingUtils.scrollTableToShowSelection(_packageJointTypeTable);
      
      // set up component table for this package
      _ignoreActions = true;
      populateComponentLocationTableForPackage(currentCompPackage);
      // select first component
      _currentComponent = null;

      //Siew Yeng - XCR-2123
      int[] selectedComponentRows = _componentLocationTable.getSelectedRows();
      if (selectedComponentRows.length == 0)
      {
        selectCurrentComponent();
        selectedComponentRows = _componentLocationTable.getSelectedRows();
      }
      
      java.util.List<ComponentType> selectedComponents = new ArrayList<ComponentType>();
      for(int i = 0; i < selectedComponentRows.length; i++)
      {
        selectedComponents.add(_componentLocationTableModel.getComponentTypeAt(selectedComponentRows[i]));
      }
      _componentLocationTableModel.setSelectedComponents(_currentBoard, selectedComponents);
      
      _ignoreActions = false;
      updateLastNextButtons(_currentPackageIndex, _packageJointTypeTable, _lastPackageButton, _nextPackageButton);

      // set CAD graphics crosshairs on board and component
      highlightBoardAndComponent();

      // wait for graphics
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          _packageJointTypeTable.processSelections(true);
        }
      });

      updateAdjustButtons();
    }
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void packageOnPackageTable_valueChanged(ListSelectionEvent e)
  {
    if (_ignoreActions)
    {
//      System.out.println("packageJointTypeTable_valueChanged() is ignoring actions");
      return;
    }
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    if (lsm.getValueIsAdjusting())
    {
//      System.out.println("packageJointTypeTable is adjusting");
      // do nothing
    }
    else if (lsm.isSelectionEmpty())
    {
//      System.out.println("packageJointTypeTable selection is empty");
      // do nothing
    }
    else
    {
      // turn off event processing until graphics had a chance to update
      _packageOnPackageTable.processSelections(false);
      // package was selected with a table event
      _currentPOPIndex = lsm.getMinSelectionIndex();
//      System.out.println("packageJointTypeTable row " + _currentPackageIndex + " selected");
      CompPackageOnPackage currentPOP = _packageOnPackageTableModel.getPackageAt(_currentPOPIndex);
      _packageOnPackageTableModel.setSelectedPackage(currentPOP);

      // after selection, scroll to make sure the row is visible.
//      SwingUtils.scrollTableToShowSelection(_packageJointTypeTable);
      
      // set up component table for this package
      _ignoreActions = true;
      populatePOPComponentLocationTableForPOP(currentPOP);
      // select first component
      _currentComponent = null;

      //Siew Yeng - XCR-2123
      int[] selectedComponentRows = _popComponentLocationTable.getSelectedRows();
      if (selectedComponentRows.length == 0)
      {
        selectCurrentComponentForPOPTable();
        selectedComponentRows = _popComponentLocationTable.getSelectedRows();
      }
      
      java.util.List<ComponentType> selectedComponents = new ArrayList<ComponentType>();
      for(int i = 0; i < selectedComponentRows.length; i++)
      {
        selectedComponents.add(_popComponentLocationTableModel.getComponentTypeAt(selectedComponentRows[i]));
      }
      _popComponentLocationTableModel.setSelectedComponentTypes(selectedComponents);
      
      _ignoreActions = false;

      // set CAD graphics crosshairs on board and component
      highlightBoardAndComponent();

      // wait for graphics
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          _packageOnPackageTable.processSelections(true);
        }
      });

      updateAdjustButtons();
    }
  }
  
  /**
   * @author George Booth
   */
  CompPackage selectPackageForCurrentComponent()
  {
    Assert.expect(_packageJointTypeTable.getRowCount() > 0);

    // clear the current selection
    _packageJointTypeTable.cancelCellEditing();
    _packageJointTypeTable.clearSelection();
    _packageJointTypeTableModel.clearSelectedPackage();

    // Select the package for the current component or
    // select row 0 if _currentComponent is not defined
    int selectedRow = 0;
    if (_currentComponent != null)
    {
      CompPackage currentCompPackage = _currentComponent.getCompPackage();
      int packageIndex = _packageJointTypeTableModel.getRowForCompPackage(currentCompPackage);
      if (packageIndex > -1)
      {
        selectedRow = packageIndex;
      }
    }
    //    System.out.println("selecting package at row " + selectedRow);
    _ignoreActions = true;
    selectPackageTableRow(selectedRow);
    _ignoreActions = false;
    return _packageJointTypeTableModel.setSelectedPackageAt(selectedRow);
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  CompPackageOnPackage selectPOPForCurrentComponent()
  {
    if (_packageOnPackageTableModel.getRowCount() > 0)
    {
      // clear the current selection
      _packageOnPackageTable.cancelCellEditing();
      _packageOnPackageTable.clearSelection();
      _packageOnPackageTableModel.clearSelectedPackage();

      // Select the package for the current component or
      // select row 0 if _currentComponent is not defined
      int selectedRow = 0;
      if (_currentComponent != null && _currentComponent.hasCompPackageOnPackage())
      {
        CompPackageOnPackage currentPOP = _currentComponent.getCompPackageOnPackage();
        int popIndex = _packageOnPackageTableModel.getRowForCompPackageOnPackage(currentPOP);
        if (popIndex > -1)
        {
          selectedRow = popIndex;
        }
        _popComponentLocationTableModel.populateWithPOPData(_currentBoard, _currentBoardType, _currentSide, currentPOP);
      }
      else
      {
        _popComponentLocationTableModel.populateWithPOPData(_currentBoard, _currentBoardType, _currentSide, _packageOnPackageTableModel.getPackageAt(0));  
      }
      _ignoreActions = true;
      selectPOPTableRow(selectedRow);
      _ignoreActions = false;
      return _packageOnPackageTableModel.setSelectedPackageAt(selectedRow);
    }
    else
    {
      _popComponentLocationTableModel.clear();
      return null;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean isCurrentComponentValid()
  {
    if (_currentComponent == null)
      return false;
    else
      return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  ComponentType getCurrentComponent()
  {
    Assert.expect(_currentComponent != null);
    return _currentComponent;
  }

  /**
   * @author Andy Mechtenberg
   */
  void resetCurrentComponent()
  {
    _currentComponent = null;
  }

  /**
   * @author George Booth
   */
  void selectPackageTableRow(int row)
  {
    selectTableRow(row, _packageJointTypeTable, _lastPackageButton, _nextPackageButton);
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  void selectPOPTableRow(int row)
  {
    selectTableRow(row, _packageOnPackageTable, _lastPackageButton, _nextPackageButton);
  }

  /**
   * @author George Booth
   */
  public void regionOrSideTable_valueChanged(ListSelectionEvent e)
  {
    if (_ignoreActions)
    {
//      System.out.println("regionOrSideTable_valueChanged() is ignoring actions");
      return;
    }
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    if (lsm.isSelectionEmpty() || lsm.getValueIsAdjusting())
    {
      // do nothing
    }
    else
    {
      // turn off event processing until graphics had a chance to update
      _regionOrSideTable.processSelections(false);
      int regionIndex = lsm.getMinSelectionIndex();
      updateLastNextButtons(regionIndex, _regionOrSideTable, _lastRegionButton, _nextRegionButton);
      // after selection, scroll to make sure the row is visible.
      SwingUtils.scrollTableToShowSelection(_regionOrSideTable);
      // regions are not supported in version 1
      // update component table with components in this region
//      String imageRegion = _regionOrSideTableModel.getRegionAt(regionIndex);
//      _componentLocationTableModel.populateWithRegionData(imageRegion);

      // for now, make sure component table has correct panel data
      _componentLocationTableModel.populateWithPanelData(_currentBoard,
          _currentBoardType, _currentSide);
      // reselect the current component or default to first component
      selectCurrentComponent();

      // wait for graphics
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          _regionOrSideTable.processSelections(true);
        }
      });
    }
  }

  /**
   * @author George Booth
   */
  void selectRegionForCurrentComponent()
  {
    int currentRegionIndex = _regionOrSideTable.getSelectedRow();
    // Select the region for the current component or
    // select row 0 if _currentComponent is not defined
    CompPackage currentCompPackage = null;
    if (_currentComponent != null)
    {
      currentCompPackage = _currentComponent.getCompPackage();
    }
    int regionIndex = _regionOrSideTableModel.getRowForCompPackage(currentCompPackage);
    selectRegionTableRow(regionIndex);
    // if same region, update component selection
    if (true || regionIndex == currentRegionIndex)
    {
      selectCurrentComponent();
    }
  }

  /**
   * @author George Booth
   */
  void selectRegionTableRow(int row)
  {
    selectTableRow(row, _regionOrSideTable, _lastRegionButton, _nextRegionButton);
  }

  /**
   * @author George Booth
   */
  void selectPreviousTableRow(JSortTable table, JButton lastButton, JButton nextButton)
  {
    int row = table.getSelectedRow() - 1;
    selectTableRow(row, table, lastButton, nextButton);
  }

  /**
   * @author George Booth
   */
  void selectNextTableRow(JSortTable table, JButton lastButton, JButton nextButton)
  {
    int row = table.getSelectedRow() + 1;
    selectTableRow(row, table, lastButton, nextButton);
  }

  /**
   * @author George Booth
   */
  void selectTableRow(int row, JSortTable table, JButton lastButton, JButton nextButton)
  {
    if (row > -1)
    {
      table.setRowSelectionInterval(row, row);
      updateLastNextButtons(row, table, lastButton, nextButton);
    }
  }

  /**
   * @author George Booth
   */
  void updateLastNextButtons(int row, JSortTable table, JButton lastButton, JButton nextButton)
  {
    if (row == -1) // no selection, can't do last or next yet
    {
      lastButton.setEnabled(false);
      nextButton.setEnabled(false);
    }
    else
    {
      // make sure they stay disabled if adjusting is in progress
      lastButton.setEnabled(_adjusting == false && row != 0);
      nextButton.setEnabled(_adjusting == false && row < (table.getRowCount() - 1));
    }
  }

  /**
   * @author George Booth
   */
  void adjustOffRadioButton_actionPerformed()
  {
    // don't respond twice!
    if (_lastButton == _adjustOffRadioButton)
    {
      return;
    }

    if (_shouldInteractWithGraphics)
    {
      if (okToCancelAlignment())
      {
        // cancel the drag operations
        _guiObservable.stateChanged(null, VerifyCadAdjustmentEventEnum.CANCEL_ADJUSTMENT);
        _adjusting = false;
        enablePanelFunctions();
        _lastButton = _adjustOffRadioButton;
        updateAdjustButtons();
      }
      else
      {
        // restore last button
       _lastButton.setSelected(true);
      }
    }
  }

  /**
   * @author George Booth
   */
  void adjustComponentRadioButton_actionPerformed()
  {
    // don't respond twice!
    if (_lastButton == _adjustComponentRadioButton)
    {
      return;
    }

    if (_shouldInteractWithGraphics)
    {
      if (okToStartAdjustment())
      {
        //Khaw Chek Hau - XCR2337: Message should prompt for No Load and No Test components at Adjust CAD
        if (_currentComponent.isInspected() == false)  
        {
          JOptionPane.showMessageDialog(
              _mainUI,
              StringLocalizer.keyToString("CAD_COMPONENTS_ADJUST_WARNING_MESSAGE_KEY"),
              StringLocalizer.keyToString("CAD_COMPONENTS_ADJUST_WARNING_TITLE_KEY"),
              JOptionPane.WARNING_MESSAGE);

          _lastButton.setSelected(true);
          return;  
        }
        
        _guiObservable.stateChanged(null, VerifyCadAdjustmentEventEnum.ADJUST_COMPONENT);
        _adjusting = true;
        _lastButton = _adjustComponentRadioButton;
        updateAdjustButtons();
        disablePanelFunctionsWhileAdjusting();
      }
    }
  }

  /**
   * @author George Booth
   */
  void adjustBoardSideRadioButton_actionPerformed()
  {
    // don't respond twice!
    if (_lastButton == _adjustBoardSideRadioButton)
    {
      return;
    }

    if (_shouldInteractWithGraphics)
    {
      if (okToStartAdjustment())
      {
        _guiObservable.stateChanged(null, VerifyCadAdjustmentEventEnum.ADJUST_BOARD_SIDE);
        _adjusting = true;
        _lastButton = _adjustBoardSideRadioButton;
        updateAdjustButtons();
        disablePanelFunctionsWhileAdjusting();
      }
    }
  }

  /**
   * @author George Booth
   */
  void adjustBoardOriginRadioButton_actionPerformed()
  {
    // don't respond twice!
    if (_lastButton == _adjustBoardOriginRadioButton)
    {
      return;
    }

    if (_shouldInteractWithGraphics)
    {
      if (okToStartAdjustment())
      {
        //Khaw Chek Hau - XCR2337: Message should prompt for No Load and No Test components at Adjust CAD
        if (_currentComponent.isInspected() == false)  
        {
          JOptionPane.showMessageDialog(
              _mainUI,
              StringLocalizer.keyToString("CAD_COMPONENTS_ADJUST_WARNING_MESSAGE_KEY"),
              StringLocalizer.keyToString("CAD_COMPONENTS_ADJUST_WARNING_TITLE_KEY"),
              JOptionPane.WARNING_MESSAGE);

          _lastButton.setSelected(true);
          return;  
        }
          
        _guiObservable.stateChanged(null, VerifyCadAdjustmentEventEnum.ADJUST_BOARD_ORIGIN);
        _adjusting = true;
        _lastButton = _adjustBoardOriginRadioButton;
        updateAdjustButtons();
        disablePanelFunctionsWhileAdjusting();
      }
    }
  }

  /**
   * @author George Booth
   */
  void saveAdjustmentButton_actionPerformed()
  {
    if (_shouldInteractWithGraphics)
    {
      // save the drag operations
      _guiObservable.stateChanged(null, VerifyCadAdjustmentEventEnum.SAVE_ADJUSTMENT);

      // wait for graphics to respond to SAVE event
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          finishSaveAdjustmentButton_actionPerformed();
        }
      });
    }
  }

  /**
   * @author George Booth
   */
  void finishSaveAdjustmentButton_actionPerformed()
  {
    boolean saveSuccessful = false;

    // get offset
    Point2D offset = null;
    if(_project.getPanel().getPanelSettings().hasLowMagnificationComponent())
      offset = _imageManagerPanel.getVerificationImagePanel().getOffsetRelativeToPanelInNanoMeters();
    else
      offset = _imageManagerPanel.getVerificationImagePanelForHighMag().getOffsetRelativeToPanelInNanoMeters();
    
    MathUtilEnum enumSelectedUnits = _project.getDisplayUnits();
    double xOffset = MathUtil.convertUnits(offset.getX(), MathUtilEnum.NANOMETERS, enumSelectedUnits);
    double yOffset = MathUtil.convertUnits(offset.getY(), MathUtilEnum.NANOMETERS, enumSelectedUnits);

    if (_lastButton == _adjustComponentRadioButton)
    {
      _commandManager.trackState(getCurrentUndoState());
      if (_componentLocationTableModel.adjustSelectedComponentLocation(_currentBoard, offset))
      {
        saveSuccessful = true;
      }
    }
    else if (_lastButton == _adjustBoardSideRadioButton)
    {
      _commandManager.trackState(getCurrentUndoState());
      if (_componentLocationTableModel.adjustBoardSideLocation(_currentBoard,
          _currentSide, offset))
      {
        saveSuccessful = true;
      }
    }
    else if (_lastButton == _adjustBoardOriginRadioButton)
    {
      _commandManager.trackState(getCurrentUndoState());
      if (_componentLocationTableModel.adjustBoardOriginLocation(_currentBoard, offset))
      {
        saveSuccessful = true;
      }
    }

    if (saveSuccessful)
    {
      _adjusting = false;
      enablePanelFunctions();
      _adjustOffRadioButton.setSelected(true);
      _lastButton = _adjustOffRadioButton;
      updateAdjustButtons();

      // indicate a board/comp is not currently highlighted so graphics is refreshed
      _currentBoardSelectedInGraphics = null;
      _currentComponentSelectedInGraphics = null;
      // table update may clear selection; make sure current component is still selected
      selectCurrentComponent();
      
      // a check is made in handleProjectDataEvent to see if alignment is still valid
    }
    else
    {
      // tell graphics to restart current adjustment
      if (_lastButton == _adjustComponentRadioButton)
      {
        _guiObservable.stateChanged(null, VerifyCadAdjustmentEventEnum.ADJUST_COMPONENT);
      }
      else if (_lastButton == _adjustBoardSideRadioButton)
      {
        _guiObservable.stateChanged(null, VerifyCadAdjustmentEventEnum.ADJUST_BOARD_SIDE);
      }
      else if (_lastButton == _adjustBoardOriginRadioButton)
      {
        _guiObservable.stateChanged(null, VerifyCadAdjustmentEventEnum.ADJUST_BOARD_ORIGIN);
      }
    }
  }

  /**
   * @author George Booth
   */
  void cancelAdjustmentButton_actionPerformed()
  {
    if (_shouldInteractWithGraphics)
    {
      // prompt for confirmation
      if (okToCancelAlignment())
      {
        // cancel the drag operations
        _guiObservable.stateChanged(null, VerifyCadAdjustmentEventEnum.CANCEL_ADJUSTMENT);
        _adjusting = false;
        enablePanelFunctions();
        _adjustOffRadioButton.setSelected(true);
        _lastButton = _adjustOffRadioButton;
        updateAdjustButtons();
      }
    }
  }

  /**
   * @author George Booth
   */
  boolean okToStartAdjustment()
  {
    // if we are currently adjusting, prompt if OK to cancel current adjustment
    if (_adjusting)
    {
      if (okToCancelAlignment())
      {
        // cancel it
        _guiObservable.stateChanged(null, VerifyCadAdjustmentEventEnum.CANCEL_ADJUSTMENT);
        _adjusting = false;
        enablePanelFunctions();
        return true;
      }
      else
      {
        // continue; reselect last radio button button
        _lastButton.setSelected(true);
        return false;
      }
    }
    return true;
  }

  /**
   * @author George Booth
   */
  boolean okToCancelAlignment()
  {
    // prompt for confirmation
    return (JOptionPane.showConfirmDialog(
      this,
      StringLocalizer.keyToString(new LocalizedString("CAD_CONFIRM_CANCEL_ADJUSTMENT_KEY", null)),
      StringLocalizer.keyToString("CAD_CONFIRM_CANCEL_ADJUSTMENT_TITLE_KEY"),
      JOptionPane.YES_NO_OPTION,
      JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION);
  }

  /**
   * @author George Booth
   */
  void updateAdjustButtons()
  {
    String sideInUse = (String)_sideComboBox.getSelectedItem();
    //Siew Yeng - XCR-2123 - disable adjust button if multiple component selected
    int selectedComponentRows = _componentLocationTable.getSelectedRowCount();
    boolean okToAdjust = _alignmentValid && _haveValidVerificationImages && _currentComponentIndex > -1 && selectedComponentRows == 1;
    _adjustOffRadioButton.setEnabled(okToAdjust);
    _adjustComponentRadioButton.setEnabled(okToAdjust);
    _adjustBoardSideRadioButton.setEnabled(okToAdjust && sideInUse.equalsIgnoreCase(_BOTH_SIDE) == false);
    _adjustBoardOriginRadioButton.setEnabled(okToAdjust);
    _applyAdjustmentButton.setEnabled(_adjusting);
    _cancelAdjustmentButton.setEnabled(_adjusting);
  }

  /**
   * @author George Booth
   */
  void disablePanelFunctionsWhileAdjusting()
  {
    _sideComboBox.setEnabled(false);
    _viewByComboBox.setEnabled(false);
    _panelBoardComboBox.setEnabled(false);
    _generateVerificationImagesButton.setEnabled(false);
    _componentLocationTable.setEnabled(false);
    _lastComponentButton.setEnabled(false);
    _nextComponentButton.setEnabled(false);
    _packageJointTypeTable.setEnabled(false);
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    _packageOnPackageTable.setEnabled(false);
    _popComponentLocationTable.setEnabled(false);
    _lastPackageButton.setEnabled(false);
    _nextPackageButton.setEnabled(false);
    _regionOrSideTable.setEnabled(false);
    _lastRegionButton.setEnabled(false);
    _nextRegionButton.setEnabled(false);
  }

  /**
   * @author George Booth
   */
  void enablePanelFunctions()
  {
    _sideComboBox.setEnabled(true);
    _viewByComboBox.setEnabled(true);
    _panelBoardComboBox.setEnabled(true);
    _generateVerificationImagesButton.setEnabled(_alignmentValid);
    _componentLocationTable.setEnabled(true);
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    _popComponentLocationTable.setEnabled(true);
    updateLastNextButtons(_componentLocationTable.getSelectedRow(), _componentLocationTable,
                          _lastComponentButton, _nextComponentButton);
    _packageJointTypeTable.setEnabled(true);
    _packageOnPackageTable.setEnabled(true);
    updateLastNextButtons(_packageJointTypeTable.getSelectedRow(), _packageJointTypeTable,
                          _lastPackageButton, _nextPackageButton);
    _regionOrSideTable.setEnabled(true);
    updateLastNextButtons(_regionOrSideTable.getSelectedRow(), _regionOrSideTable,
                          _lastRegionButton, _nextRegionButton);
  }

  /**
   * @author George Booth
   */
  void handleSelectedRendererEvent(final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        // turn off auto-updating of the graphics which happens when table selections are changed.
        // Since this action happened because of a graphical selection, it should override
        _shouldInteractWithGraphics = false;
        // We got notice that something was selected.

        // We receive a collection of selected objects from the update.
        /** Warning "unchecked cast" approved for cast from Object type
         *  when sent from SelectedRendererObservable notifyAll.  */
        //Siew Yeng - XCR-2123 - handle select multiple component      
        java.util.List < com.axi.v810.business.panelDesc.Component > components = new ArrayList<com.axi.v810.business.panelDesc.Component>();
        
        for (com.axi.guiUtil.Renderer renderer : ((Collection <com.axi.guiUtil.Renderer> )object))
        {
          if (renderer instanceof ComponentRenderer)
          {
            ComponentRenderer componentRenderer = (ComponentRenderer)renderer;
            //componentSelectedGraphically(componentRenderer.getComponent());
            //break;
            components.add(componentRenderer.getComponent());
          }
          else if (renderer instanceof PadRenderer)
          {
            PadRenderer padRenderer = (PadRenderer)renderer;
            //componentSelectedGraphically(padRenderer.getPad().getComponent());
            //break;
            components.add(padRenderer.getPad().getComponent());
          }
          else
          {
            // panel, board or fiducial was selected
            _packageJointTypeTable.clearSelection();
            _packageOnPackageTable.clearSelection();
            _componentLocationTable.clearSelection();
            _popComponentLocationTable.clearSelection();
          }
        }
        
        if(components.isEmpty())
        {
          return;
        }
        componentSelectedGraphically(components);
        _shouldInteractWithGraphics = true;
      }
    });
  }

  /**
   * @author George Booth
   * Change behavior only at COMPONENT_VIEW to avoid breaking the existing behavior 
   * at PACKAGE_VIEW and REGION_VIEW - Siew Yeng - XCR-2123
   */
  void componentSelectedGraphically(java.util.List<com.axi.v810.business.panelDesc.Component> componentsSelectedGraphically)
  {
    // set the flag that a component was selected graphically
    _componentTypeSelectedGraphically = componentsSelectedGraphically.get(0).getComponentType();
    BoardType boardType = componentsSelectedGraphically.get(0).getBoard().getBoardType();
    String boardName = componentsSelectedGraphically.get(0).getBoard().getName();
    String side = componentsSelectedGraphically.get(0).isTopSide() ? _TOP_SIDE : _BOTTOM_SIDE;

    boolean newBoardType = false;
    boolean newBoardName = false;
    boolean newSide = false;
    boolean newComponent = false;

    //Siew Yeng - XCR-2123 - temporary revert back to original behavior. Will continue to use this in 5.7
    if (_currentView.equals(_COMPONENT_VIEW))
    {
      ListSelectionModel rowSelectionModel = _componentLocationTable.getSelectionModel();
      rowSelectionModel.setValueIsAdjusting(true);
      
      if(componentsSelectedGraphically.isEmpty() == false)
      {
        if (_componentLocationTable.isEditing())
          _componentLocationTable.getCellEditor().cancelCellEditing();
        _componentLocationTable.clearSelection();
      }

      // we're going to select new components if a component renderer came through, otherwise do not change the
      // selection
      for (com.axi.v810.business.panelDesc.Component component : componentsSelectedGraphically)
      {
        int rowPosition = _componentLocationTableModel.getRowForComponentType(component.getComponentType());
        if (rowPosition != -1) // -1 measn it does not exist in the table.  This can happen with more than one board type
          _componentLocationTable.addRowSelectionInterval(rowPosition, rowPosition);
      }
      
      rowSelectionModel.setValueIsAdjusting(false);
      SwingUtils.scrollTableToShowSelection(_componentLocationTable);
    }
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    else if (_currentView.equals(_POP_VIEW))
    {
      ListSelectionModel rowSelectionModel = _popComponentLocationTable.getSelectionModel();
      rowSelectionModel.setValueIsAdjusting(true);
      
      if(componentsSelectedGraphically.isEmpty() == false)
      {
        if (_popComponentLocationTable.isEditing())
          _popComponentLocationTable.getCellEditor().cancelCellEditing();
        _popComponentLocationTable.clearSelection();
      }

      // we're going to select new components if a component renderer came through, otherwise do not change the
      // selection
      for (com.axi.v810.business.panelDesc.Component component : componentsSelectedGraphically)
      {
        int rowPosition = _popComponentLocationTableModel.getRowForComponentType(component.getComponentType());
        if (rowPosition != -1) // -1 measn it does not exist in the table.  This can happen with more than one board type
          _popComponentLocationTable.addRowSelectionInterval(rowPosition, rowPosition);
      }
      
      rowSelectionModel.setValueIsAdjusting(false);
      SwingUtils.scrollTableToShowSelection(_popComponentLocationTable);
    }
    else
    {
      _shouldInteractWithGraphics = true;
      
      if (_currentBoardType.getName().equals(boardType.getName()) == false)
      {
        newBoardType = true;
  //      System.out.println("new board type");
      }

      if (_currentBoard.getName().equals(boardName) == false)
      {
        newBoardName = true;
        _currentBoard = componentsSelectedGraphically.get(0).getBoard();
        _currentBoardName = boardName;
        _ignoreActions = true;
        _panelBoardComboBox.setSelectedItem(_currentBoardName);
        _ignoreActions = false;
  //      System.out.println("new board name");
      }

      if ((_currentSide.equals(_BOTH_SIDE) == false) && (_currentSide.equals(side) == false))
      {
        newSide = true;
        _currentSide = side;
        _ignoreActions = true;
        _sideComboBox.setSelectedItem(_currentSide);
        _ignoreActions = false;
  //      System.out.println("new side");
      }

      if (_currentComponentName.equals(_componentTypeSelectedGraphically) == false)
      {
        newComponent = true;
        _currentComponent = _componentTypeSelectedGraphically;
        _currentComponentName = _currentComponent.getReferenceDesignator();
  //      System.out.println("new component");
      }

      if (newBoardType)
      {
        // set board type; event will repopulate all
        _mainUI.getTestDev().setCurrentBoardType(boardType);
        populateWithProjectData();
      }
      else if (newSide)
      {
        // populate all data
        populateWithProjectData();
      }
      else if (newComponent)
      {
        // tables might change
        populateAllTables();
      }
      else if (newBoardName || newComponent)
      {
        // populate all data
        populateWithProjectData();
      }
      else
      {
        // nothing new was selected
      }

      // make sure selected rows are visible
      if (_currentView == _PACKAGE_VIEW)
      {
        SwingUtils.scrollTableToShowSelection(_packageJointTypeTable);
      }
      else if (_currentView == _REGION_VIEW)
      {
        SwingUtils.scrollTableToShowSelection(_regionOrSideTable);
      }
    }
  }

  /**
   * @author George Booth
   */
  void populatePanelBoardComboBox()
  {
    // make sure panelBoardComboBox listener doesn't respond to remove or add
    _ignoreActions = true;

    // update panel/board instances
    _panelBoardComboBox.removeAllItems();
    // get boards of this type

//    java.util.List<Board> boardsList = _currentBoardType.getBoards();
//    // create list of names
//    java.util.List<String> boardNamesList = new ArrayList<String>();
//    for (Board board : boardsList)
//    {
//      boardNamesList.add(board.getName());
//    }
//    // sort by name
//    Collections.sort(boardNamesList, new AlphaNumericComparator());
//    // create global _sortedBoardList sorted by name
    _sortedBoardList = new java.util.ArrayList<Board>();
//    for (String boardName : boardNamesList)
//    {
//      for (Board board : boardsList)
//      {
//        if (board.getName().equals(boardName))
//        {
//          _sortedBoardList.add(board);
//        }
//      }
//    }
//    Assert.expect(_sortedBoardList.size() > 0);


    java.util.List<Board> boardInstances = _project.getPanel().getBoards();
    // sort boards based on name
    Collections.sort(boardInstances, new AlphaNumericComparator());
    for (Board board : boardInstances)
    {
      if (board.getBoardType().equals(_currentBoardType))
        _sortedBoardList.add(board);
    }

    // set up panelBoardComboBox
    if (_sortedBoardList.size() == 1)
    {
      _panelBoardComboBox.addItem(StringLocalizer.keyToString("ATGUI_PANEL_KEY"));
      _panelBoardComboBox.setSelectedIndex(0);
    }
    else
    {
      boolean canSelectBoardName = false;
      for(Board board : _sortedBoardList)
      {
        _panelBoardComboBox.addItem(board.getName());
        if (_currentBoardName != null && _currentBoardName.equals(board.getName()))
        {
          canSelectBoardName = true;
        }
      }
//      for (String boardName : boardNamesList)
//      {
//        _panelBoardComboBox.addItem(boardName);
//        if (_currentBoardName != null && _currentBoardName.equals(boardName))
//        {
//          canSelectBoardName = true;
//        }
//      }
      if (canSelectBoardName)
      {
        _panelBoardComboBox.setSelectedItem(_currentBoardName);
      }
      else
      {
        // default to first board
        _panelBoardComboBox.setSelectedIndex(0);
      }
    }
    _currentBoard = _sortedBoardList.get(_panelBoardComboBox.getSelectedIndex());
    _currentBoardName = _currentBoard.getName();

    _ignoreActions = false;
  }

  /**
   * @author George Booth
   */
  void populateSideComboBox()
  {
    // make sure sideComboBox listener doesn't respond to remove or add
    _ignoreActions = true;

    int numTop = 0;
    if (_currentBoard.topSideBoardExists())
    {
      SideBoardType sideBoard = _currentBoard.getTopSideBoard().getSideBoardType();
      numTop = sideBoard.getComponentTypes().size();
    }
    else
    {
      numTop = 0;
    }

    int numBot = 0;
    if (_currentBoard.bottomSideBoardExists())
    {
      SideBoardType sideBoard = _currentBoard.getBottomSideBoard().getSideBoardType();
      numBot = sideBoard.getComponentTypes().size();
    }
    else
    {
      numBot = 0;
    }

    // Determine board sides to show - are there any components on the selected side?
    _sideComboBox.removeAllItems();
    boolean canSelectSide = false;
    if (numTop > 0)
    {
      _sideComboBox.addItem(_TOP_SIDE);
      if (_currentSide != null && _currentSide.equals(_TOP_SIDE))
      {
        canSelectSide = true;
      }
    }
    if (numBot > 0)
    {
      _sideComboBox.addItem(_BOTTOM_SIDE);
      if (_currentSide != null && _currentSide.equals(_BOTTOM_SIDE))
      {
        canSelectSide = true;
      }
    }
    //                                special case: show BOTH if no components
    if ((numTop > 0 && numBot > 0) || (numTop == 0 && numBot == 0))
    {
      _sideComboBox.addItem(_BOTH_SIDE);
      if (_currentSide != null && _currentSide.equals(_BOTH_SIDE))
      {
        canSelectSide = true;
      }
    }
    // try to set with current view
    if (canSelectSide)
    {
      _sideComboBox.setSelectedItem(_currentSide);
    }
    else
    {
      _sideComboBox.setSelectedIndex(0);
    }
    _currentSide = (String)_sideComboBox.getSelectedItem();

    _ignoreActions = false;
  }

  /**
   * @author George Booth
   */
  void populateAllTables()
  {
    // package joint table is same for all views - has all packages for selected
    // boardType, board and side
    populatePackageJointTypeTable();
    populatePackageOnPackageTable();
    _popComponentLocationTableModel.setProject(_project);

    // region table is same for all views - has all regions for selected
    // boardType and side
    // not implemented
//    _regionOrSideTableModel.populateWithPanelData(_currentBoardType, _currentSide);

    if (_currentView.equals(_PACKAGE_VIEW))
    {
      // select package for component or 1st package
      CompPackage selectedCompPackage = selectPackageForCurrentComponent();

      populateComponentLocationTableForPackage(selectedCompPackage);

      // select current component or 1st component
      selectCurrentComponent();
    }
    else if (_currentView.equals(_COMPONENT_VIEW))
    {
      populateComponentLocationTableForPanel();

      // select current component or 1st component
      selectCurrentComponent();
      
      selectPackageForCurrentComponent();
    }
    else if (_currentView.equals(_POP_VIEW))
    {  
      //Khaw Chek Hau - XCR3554: Package on package (PoP) development    
      CompPackageOnPackage selectedPOP = selectPOPForCurrentComponent();

      populatePOPComponentLocationTableForPOP(selectedPOP);

      selectCurrentComponentForPOPTable();
    }
    else if (_currentView.equals(_REGION_VIEW))
    {
      // not implemented
      Assert.expect(false, "Region view not implemented");
    }
    else
    {
      Assert.expect(false, "Undefined view");
    }
  }

  /**
   * @author George Booth
   */
  void populatePackageJointTypeTable()
  {
    _ignoreActions = true;

    // load selected panel data into package family table
    _packageJointTypeTableModel.populateWithPanelData(_currentBoard,
        _currentBoardType, _currentSide);

    _ignoreActions = false;

    setupPackageJointTypeTableEditors();
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  void populatePackageOnPackageTable()
  {
    _ignoreActions = true;

    // load selected panel data into package family table
    _packageOnPackageTableModel.populateWithPanelData(_currentBoard,
        _currentBoardType, _currentSide);

    _ignoreActions = false;

    setupPackageOnPackageTableEditors();
  }

  /**
   * @author George Booth
   */
  void populateComponentLocationTableForPackage(CompPackage compPackage)
  {
    _ignoreActions = true;

    if (compPackage != null)
    {
      _componentLocationTableModel.populateWithPackageData(_currentBoard,
          _currentBoardType, _currentSide, compPackage);
    }

    _ignoreActions = false;

    setupComponentLocationTableEditors();
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  void populatePOPComponentLocationTableForPOP(CompPackageOnPackage compPackageOnPackage)
  {
    _ignoreActions = true;

    if (compPackageOnPackage != null)
    {
      _popComponentLocationTableModel.populateWithPOPData(_currentBoard, _currentBoardType, _currentSide, compPackageOnPackage);
    }

    _ignoreActions = false;

    setupPOPComponentLocationTableEditors();
  }
  
  /**
   * @author George Booth
   */
  void populateComponentLocationTableForPanel()
  {
    _ignoreActions = true;

    _componentLocationTableModel.populateWithPanelData(_currentBoard,
        _currentBoardType, _currentSide);

    _ignoreActions = false;

    setupComponentLocationTableEditors();
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  void populatePOPComponentLocationTableForPanel()
  {
    _ignoreActions = true;

    _popComponentLocationTableModel.populateComponentTypesWithPanelData(_componentTypes);

    _ignoreActions = false;

    setupComponentLocationTableEditors();
  }

  /**
   * @author George Booth
   */
  void populateWithProjectData()
  {
    if(!_adjustCadActive)
    {
      // tell component table about the project
      _componentLocationTableModel.setProject(_project);
      //Khaw Chek Hau - XCR3554: Package on package (PoP) development
      _popComponentLocationTableModel.setProject(_project);

      // get board type
      _currentBoardType = _mainUI.getTestDev().getCurrentBoardType();

      // update panel board combo box for the current board type
      populatePanelBoardComboBox();

      if (_project.getPanel().getNumBoards() > 0)
      {
        // update side combo box for the current board
        populateSideComboBox();

        // update the tables and select the current component
        populateAllTables();

        updateAdjustButtons();

      //    validate();
      //    repaint();

        viewByComboBox_actionPerformed(); 
      }
      _adjustCadActive = true;
    }
    else
    {
      viewByComboBox_actionPerformed(); 
    }
    
  }

  /**
   * Called only from TestDev (via AdjustCadPanel) when a project is loaded
   * @author George Booth
   * Edited - To speed up the programming time, switch tab speed must be optimize
   *          if the project pass in is different 
   * @author Kee Chin Seong 
   */
  void populateWithProjectData(Project project)
  {
    Assert.expect(project != null);

    _project = project;

    if (_active)
    {
      populateWithProjectData();
    }
  }

  /**
   * @author George Booth
   */
  public void unpopulate()
  {
    _project = null;
    _currentBoard = null;
    _currentBoardName = null;
    _currentBoardType = null;
    _sortedBoardList = null;
    _currentComponent = null;
    _currentComponentName = null;
    _currentBoardSelectedInGraphics = null;
    _currentComponentSelectedInGraphics = null;
    _currentComponentIndex = -1;
    _packageJointTypeTable.resetSortHeader();
    _packageJointTypeTableModel.clear();
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    _packageOnPackageTable.resetSortHeader();
    _packageOnPackageTableModel.clear();
    _popComponentLocationTable.resetSortHeader();
    _popComponentLocationTableModel.clear();
    _componentLocationTable.resetSortHeader();
    _componentLocationTable.resetEditorsAndRenderers();
    _componentLocationTableModel.clear();
    _regionOrSideTable.resetSortHeader();
    _regionOrSideTableModel.clear();
    
    _adjustCadActive = false;
    //XCR-2489 - Swee yee wong - Board deletion not updated in verify CAD
    _projectObservable.deleteObserver(this);
  }

  /**
   * @author George Booth
   */
  void panelBoardComboBox_actionPerformed()
  {
    if (_active == false)
      return;
    if (_ignoreActions)
    {
//      System.out.println("panelBoardComboBox_actionPerformed() is ignoring actions");
      return;
    }

    // get board index
    int index = _panelBoardComboBox.getSelectedIndex();
    // get corresponding board instance and name
    _currentBoard = _sortedBoardList.get(index);
    _currentBoardName = _currentBoard.getName();

    // update everything
    _currentBoardSelectedInGraphics = null;
    _currentComponentSelectedInGraphics = null;
    populateWithProjectData();
  }

  /**
   * @author George Booth
   */
  void sideComboBox_actionPerformed()
  {
    if (_active == false)
    {
      return;
    }
    if (_ignoreActions)
    {
//      System.out.println("sideComboBox_actionPerformed() is ignoring actions");
      return;
    }
    _currentSide = (String)_sideComboBox.getSelectedItem();

    // update everything
    populateWithProjectData();
  }

  /**
   * @author George Booth
   */
  void viewByComboBox_actionPerformed()
  {
    if (_active == false)
    {
      return;
    }
    if (_ignoreActions)
    {
//      System.out.println("viewByComboBox_actionPerformed() is ignoring actions");
      return;
    }
    // save last split pane divider when changing views
    if (_currentView.equals(_PACKAGE_VIEW)) {
      _packageJointTypeSplitPaneDivider = _packageJointTypeSplitPane.getDividerLocation();
    }
    else if (_currentView.equals(_POP_VIEW)) {
      //Khaw Chek Hau - XCR3554: Package on package (PoP) development
      _packageJointTypeSplitPaneDivider = _packageJointTypeSplitPane.getDividerLocation();
    }
    else if (_currentView.equals(_REGION_VIEW)) {
      _regionOrSideSplitPaneDivider = _regionOrSideSplitPane.getDividerLocation();
    }
    
    //Siew Yeng - XCR-2123
    if(_currentView.equals((String)_viewByComboBox.getSelectedItem()) == false)
      _isChangeView = true;
    
    _currentView = (String)_viewByComboBox.getSelectedItem();
    if (_currentView.equals(_PACKAGE_VIEW))
    {
      if (_project.getPanel().getNumBoards() > 0)
        displayViewByPackageTables();
    }
    else if(_currentView.equals(_COMPONENT_VIEW))
    {
      if (_project.getPanel().getNumBoards() > 0)
        displayViewByComponentTable();
    }
    else if(_currentView.equals(_POP_VIEW))
    {
      //Khaw Chek Hau - XCR3554: Package on package (PoP) development
      if (_project.getPanel().getNumBoards() > 0)
        displayViewByPOPTables();
    }
    else if(_currentView.equals(_REGION_VIEW))
    {
      displayViewByRegionSideTables();
    }
    else
    {
      Assert.expect(false);
    }
    
    _isChangeView = false;
  }

  /**
   * @author George Booth
   */
  private void displayViewByPackageTables()
  {
    //Siew Yeng - XCR-2123
    // - remember previous selected component
    int[] selectedComponentRows = _componentLocationTable.getSelectedRows();
    // remove the table(s) that are currently being displayed
    _centerPanel.removeAll();

    // update the package and component tables for the package view
    populatePackageJointTypeTable();
    CompPackage selectedCompPackage = selectPackageForCurrentComponent();
    populateComponentLocationTableForPackage(selectedCompPackage);
    
    if(_isChangeView || selectedComponentRows.length == 0)
    {
      selectCurrentComponent();
    }
    else
    {
      _shouldInteractWithGraphics = false;
      for(int i = 0; i < selectedComponentRows.length; i++)
      {
        int row = selectedComponentRows[i];
        if (row < _componentLocationTable.getRowCount())
          _componentLocationTable.addRowSelectionInterval(selectedComponentRows[i], selectedComponentRows[i]);
      } 
      _shouldInteractWithGraphics = true;
    }
    highlightBoardAndComponent();
    
    // add the package family and component table panels
    _packageJointTypeSplitPane = new JSplitPane(
      JSplitPane.VERTICAL_SPLIT,
      _packageJointTypePanel,
      _componentLocationPanel);
    // set divider to previous location if defined
    if (_packageJointTypeSplitPaneDivider > -1)
    {
      _packageJointTypeSplitPane.setDividerLocation(_packageJointTypeSplitPaneDivider);
    }
    else
    {
      _packageJointTypeSplitPane.setResizeWeight(0.50);
    }
    _centerPanel.add(_packageJointTypeSplitPane, BorderLayout.CENTER);

    // make sure selected rows are visible
    SwingUtils.scrollTableToShowSelection(_packageJointTypeTable);
    SwingUtils.scrollTableToShowSelection(_packageOnPackageTable);
    SwingUtils.scrollTableToShowSelection(_componentLocationTable);

    validate();
    repaint();
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void displayViewByPOPTables()
  {
    Collection<ComponentType> selectedComponents = new ArrayList();
        
//    int[] selectedComponentRows = _componentLocationTable.getSelectedRows();
    // remove the table(s) that are currently being displayed
    _centerPanel.removeAll();

    
    populatePackageOnPackageTable();
    CompPackageOnPackage selectedPOP = selectPOPForCurrentComponent();
    populatePOPComponentLocationTableForPOP(selectedPOP);
    selectCurrentComponentForPOPTable();  
    
    highlightBoardAndComponent();    
    
    // add the package family and component table panels
    _packageJointTypeSplitPane = new JSplitPane(
      JSplitPane.VERTICAL_SPLIT,
      _packageOnPackagePanel,
      _popComponentLocationPanel);
    // set divider to previous location if defined
    if (_packageJointTypeSplitPaneDivider > -1)
    {
      _packageJointTypeSplitPane.setDividerLocation(_packageJointTypeSplitPaneDivider);
    }
    else
    {
      _packageJointTypeSplitPane.setResizeWeight(0.50);
    }
    _centerPanel.add(_packageJointTypeSplitPane, BorderLayout.CENTER);

    // make sure selected rows are visible
    SwingUtils.scrollTableToShowSelection(_packageOnPackageTable);
    SwingUtils.scrollTableToShowSelection(_popComponentLocationTable);

    validate();
    repaint();
  }

  /**
   * @author George Booth
   */
  private void displayViewByComponentTable()
  {
    //Siew Yeng - XCR-2123
    // - remember previous selected component
    int[] selectedComponentRows = _componentLocationTable.getSelectedRows();
    
    // remove the other table(s) that are currently being displayed
    _centerPanel.removeAll();

    // update the component table for the panel view
    populateComponentLocationTableForPanel();
    
    if(_isChangeView || selectedComponentRows.length == 0)
    {
      selectCurrentComponent();
    }
    else
    {
      _shouldInteractWithGraphics = false;

      for(int i = 0; i < selectedComponentRows.length; i++)
      {
        int row = selectedComponentRows[i];
        if (row < _componentLocationTable.getRowCount())
          _componentLocationTable.addRowSelectionInterval(selectedComponentRows[i], selectedComponentRows[i]);
      } 
      _shouldInteractWithGraphics = true;
    }
    //_componentLocationTable.clearSelection();
    highlightBoardAndComponent();
    
    // add the component location table
    _centerPanel.add(_componentLocationPanel, BorderLayout.CENTER);

    // make sure selected rows are visible
    SwingUtils.scrollTableToShowSelection(_componentLocationTable);

    validate();
    repaint();
  }

  /**
   * @author George Booth
   */
  private void displayViewByRegionSideTables()
  {
    // remove the table(s) that are currently being displayed
    _centerPanel.removeAll();
    // add the package family and component table split panes
    _regionOrSideSplitPane = new JSplitPane(
      JSplitPane.VERTICAL_SPLIT,
      _regionOrSidePanel,
      _componentLocationPanel);

    // set divider to previous location if defined
    if (_regionOrSideSplitPaneDivider > -1)
    {
      _regionOrSideSplitPane.setDividerLocation(_regionOrSideSplitPaneDivider);
    }
    else
    {
      _regionOrSideSplitPane.setResizeWeight(0.50);
    }
    _centerPanel.add(_regionOrSideSplitPane, BorderLayout.CENTER);

    // make sure selected rows are visible
    SwingUtils.scrollTableToShowSelection(_regionOrSideTable);
    SwingUtils.scrollTableToShowSelection(_componentLocationTable);

    validate();
    repaint();
  }

  /**
   * @author George Booth
   */
  private void setupComponentLocationTableEditors()
  {
    // get any custom component rotations
    Set<Double> customRotationValues = new HashSet<Double>();
    java.util.List<ComponentType> componentTypes = _componentLocationTableModel.getBoardSideComponentTypes(_currentBoard, _currentBoardType, _currentSide);
    for (ComponentType componentType : componentTypes)
    {
      customRotationValues.add(componentType.getDegreesRotationRelativeToBoard());
    }
    _componentLocationTable.setEditorsAndRenderers(customRotationValues);

    // make sure component tables shows proper decimal places
    MathUtilEnum enumSelectedUnits = _project.getDisplayUnits();
    _componentLocationTable.setDecimalPlacesToDisplay(MeasurementUnits.getMeasurementUnitDecimalPlaces(enumSelectedUnits));
    _componentLocationTableModel.setSelectedUnits(enumSelectedUnits);
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void setupPOPComponentLocationTableEditors()
  {    
    _popComponentLocationTable.setupCellEditorsAndRenderers();

    // make sure component tables shows proper decimal places
    MathUtilEnum enumSelectedUnits = _project.getDisplayUnits();
    _popComponentLocationTable.setDecimalPlacesToDisplay(MeasurementUnits.getMeasurementUnitDecimalPlaces(enumSelectedUnits));
    _popComponentLocationTableModel.setSelectedUnits(enumSelectedUnits);
  }

  /**
   * @author George Booth
   */
  private void setupPackageJointTypeTableEditors()
  {
    _packageJointTypeTable.setEditorsAndRenderers();
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void setupPackageOnPackageTableEditors()
  {
    _packageOnPackageTable.setEditorsAndRenderers();
  }

  /**
   * @author George Booth
   */
  private void setupRegionOrSideTableEditors()
  {
    _regionOrSideTable.setEditorsAndRenderers();
  }

  /**
   * The Verify CAD is notified of most events through Edit Cad update().
   * Commnad manager is here for Undos.
   * @author George Booth
   */
  /**
   * @author George Booth
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    Assert.expect(observable != null);

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof com.axi.v810.gui.undo.CommandManager)
        {
          handleUndoState(object); 
        }
        else if (observable instanceof ImageManagerObservable)
        {
          handleImageManagerObservable(object);
        }
        else if (observable instanceof ProjectObservable)
        {
          handleProjectDataEvent(object);
        }
        else if (observable instanceof GuiObservable)
        {
          handleGuiChangesEvent(object);
        }
        else
        {
          Assert.expect(false, "No update code for observable: " + observable.getClass().getName());
        }
      }
    });
  }

  /**
   * @author George Booth
   */
  private void handleUndoState(final Object object)
  {
    Assert.expect(object != null && object instanceof UndoState);
//    System.out.println("handleUndoState");
    UndoState undoState = (UndoState)object;
    if (undoState.getAdjustCadTabbedPaneIndex() == 0)
    {
      int viewSelectIndex = undoState.getVerifyCadViewSelectIndex();
      _viewByComboBox.setSelectedIndex(viewSelectIndex);
      //Siew Yeng - XCR-2586
      int sideSelectIndex = undoState.getVerifyCadSideSelectIndex();
      _sideComboBox.setSelectedIndex(sideSelectIndex);
      _undoRedoAdjustCadGraphically = undoState.getAdjustCadGrapically();

      // indicate a board/comp is not currently highlighted so graphics is refreshed
      _currentBoardSelectedInGraphics = null;
      _currentComponentSelectedInGraphics = null;
      highlightBoardAndComponent();
    }
    // a check is made in handleProjectDataEvent to see if alignment is still valid
  }

  /**
   * @author George Booth
   */
  private void handleImageManagerObservable(Object arg)
  {
    Assert.expect(arg instanceof ImageManagerEventEnum);

    ImageManagerEventEnum imageManagerEventEnum = (ImageManagerEventEnum)arg;
    if (imageManagerEventEnum.equals(ImageManagerEventEnum.VERIFICATION_IMAGE_DELETED))
    {
      // if we are in the process of generating images, a panel load may take place -- we
      // don't want to react as we normally would
      if (_generatingImages == false)
      {
//        System.out.println("  ImageManagerEventEnum.VERIFICATION_IMAGE_DELETED seen");
        checkVerificationImages(false);
        updateAdjustButtons();
      }
    }
  }

  /**
   * @author George Booth
   */
  void highlightBoardAndComponent()
  {
    if (_shouldInteractWithGraphics)
    {
      if (_currentBoard != null &&
          (_currentBoardSelectedInGraphics == null || _currentBoardSelectedInGraphics != _currentBoard))
      {
        _currentBoardSelectedInGraphics = _currentBoard;
        java.util.List<Board> selectedBoards = new ArrayList<Board>();
        selectedBoards.add(_currentBoard);
//        System.out.println("!! highlightBoard " + _currentBoard.getName());
        _guiObservable.stateChanged(selectedBoards, SelectionEventEnum.BOARD_INSTANCE);
      }
      
      //Siew Yeng - XCR-2123 - handle multiple component selection
      java.util.List<ComponentType> selectedComponents;
      
      if (_currentView.equals(_POP_VIEW))
        selectedComponents = (java.util.List)_popComponentLocationTableModel.getSelectedComponentTypes();
      else
        selectedComponents = (java.util.List)_componentLocationTableModel.getSelectedComponents();
      
      if(selectedComponents.isEmpty() == false)
      {
        _currentComponent = selectedComponents.get(0);
        _currentComponentSelectedInGraphics = _currentComponent;
        _guiObservable.stateChanged(selectedComponents, SelectionEventEnum.COMPONENT_TYPE);
      }
      
//      if (_currentComponent != null &&
//          (_currentComponentSelectedInGraphics == null || _currentComponentSelectedInGraphics != _currentComponent))
//      {
//        _currentComponentSelectedInGraphics  = _currentComponent;
//        java.util.List<ComponentType> selectedComponents = new ArrayList<ComponentType>();
//        selectedComponents.add(_currentComponent);
////        System.out.println("!! highlightComponent " + _currentComponent.getReferenceDesignator());
//        _guiObservable.stateChanged(selectedComponents, SelectionEventEnum.COMPONENT_TYPE);
//      }
    }
  }

  /**
   * @author George Booth
   */
  void handleProjectDataEvent(final Object object)
  {
    if (_active == false)
    {
      //XCR-2489 - Swee yee wong - Board deletion not updated in verify CAD
      if (object instanceof ProjectChangeEvent)
      {
        ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent) object;
        ProjectChangeEventEnum eventEnum = projectChangeEvent.getProjectChangeEventEnum();
        if (eventEnum instanceof BoardEventEnum)
        {
          _adjustCadActive = false;
        }
      }
      return;
    }
    else
    {
      Assert.expect(object != null);

      // If we are visible, we probably caused this project data event so we need to update everything that can be
      // modified in this screen.

      if (object instanceof ProjectChangeEvent)
      {
//        System.out.println("VC: ProjectChangeEvent seen");
        boolean updateSelf = false;
        ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)object;
        ProjectChangeEventEnum eventEnum = projectChangeEvent.getProjectChangeEventEnum();
        if (eventEnum instanceof ProjectStateEventEnum)
        {
//          System.out.println("  ProjectStateEventEnum seen");
          ProjectStateEventEnum psee = (ProjectStateEventEnum)eventEnum;
          if (psee.equals(ProjectStateEventEnum.ALIGNMENT_TRANSFORM_INVALID))
          {
//            System.out.println("    ProjectStateEventEnum.ALIGNMENT_TRANSFORM_INVALID seen");
            // warn user if an adjustment or undo has made the alignment invalid
            // ALIGNMENT_INVALID and VERIFICATION_IMAGES_INVALID can be both seen.  If one has occurred,
            // don't check again or user will get two warnings
            if (_alignmentValid && _haveValidVerificationImages)
            {
              checkVerificationImages(false);
            }
            updateAdjustButtons();
          }
          else if (psee.equals(ProjectStateEventEnum.ALIGNMENT_TRANSFORM_VALID))
          {
//            System.out.println("    ProjectStateEventEnum.ALIGNMENT_TRANSFORM_VALID seen");
            checkVerificationImages(false);
            updateAdjustButtons();
          }
          else if (psee.equals(ProjectStateEventEnum.VERIFICATION_IMAGES_INVALID))
          {
//            System.out.println("    ProjectStateEventEnum.VERIFICATION_IMAGES_INVALID seen");
            // this can happen if project data changes or a new panel is loaded
            // into the hardware (see handleHardwareObservable())
            if (_alignmentValid && _haveValidVerificationImages && _isUserChangeJointType == false) // Chin Seong
            {
              checkVerificationImages(false);
            }
            updateAdjustButtons();
          }
          else if (psee.equals(ProjectStateEventEnum.VERIFICATION_IMAGES_VALID))
          {
//            System.out.println("    ProjectStateEventEnum.VERIFICATION_IMAGES_VALID seen");
            checkVerificationImages(false);
            updateAdjustButtons();
          }
        }
        else if (eventEnum instanceof SideBoardTypeEventEnum)
        {
          SideBoardTypeEventEnum sideBoardTypeEvent = (SideBoardTypeEventEnum)eventEnum;
          if (sideBoardTypeEvent.equals(sideBoardTypeEvent.ADD_OR_REMOVE_COMPONENT_TYPE))
            updateSelf = true;
        }
        else if ((eventEnum instanceof CompPackageEventEnum) ||
                 (eventEnum instanceof PackagePinEventEnum) ||
                 (eventEnum instanceof ComponentEventEnum) ||
                 (eventEnum instanceof ComponentTypeEventEnum) ||
                 (eventEnum instanceof ComponentTypeSettingsEventEnum) ||
                 (eventEnum instanceof PadTypeSettingsEventEnum) ||
                 (eventEnum instanceof PadEventEnum) ||
                 (eventEnum instanceof PadTypeEventEnum) ||
                 (eventEnum instanceof SideBoardEventEnum) ||
                 (eventEnum instanceof ThroughHoleLandPatternPadEventEnum) ||
                 (eventEnum instanceof SubtypeEventEnum) ||
                 (eventEnum instanceof BoardEventEnum))
        {
          // anything here should cause an update
          updateSelf = true;
          _adjustCadActive = false;
        }

        if (updateSelf)
        {
//          System.out.println("VC: update needed");

          // cancel table editing if needed
          if (_packageJointTypeTable.isEditing())
            _packageJointTypeTable.getCellEditor().cancelCellEditing();
          if (_componentLocationTable.isEditing())
            _componentLocationTable.getCellEditor().cancelCellEditing();
          //Khaw Chek Hau - XCR3554: Package on package (PoP) development
          if (_popComponentLocationTable.isEditing())
            _popComponentLocationTable.getCellEditor().cancelCellEditing();

          //Siew Yeng - XCR-3240
          _commandManager.waitUntilCommandEnd();
          
          if (eventEnum instanceof ComponentTypeSettingsEventEnum)  // handles no load, no test, subtype changes
          {
            // Kok Chun, Tan - XCR-3271 - handle when event pass in the list
            java.util.List<ComponentTypeSettings> componentSettingsList = null;
            if (projectChangeEvent.getSource() instanceof ArrayList)
            {
              componentSettingsList = (java.util.List<ComponentTypeSettings>) projectChangeEvent.getSource();
            }
            else
            {
              componentSettingsList = new ArrayList<ComponentTypeSettings>();
              componentSettingsList.add((ComponentTypeSettings) projectChangeEvent.getSource());
            }
            for (ComponentTypeSettings compTypeSettings : componentSettingsList)
            {
//            System.out.println("  ComponentTypeSettingsEventEnum seen");
              //populateAllTables();
              //Siew Yeng - XCR2123 - subtask - XCR-2129 - fix hang when set component to no load no test
              ComponentType compType = compTypeSettings.getComponentType();

              //Siew Yeng - XCR-2586
              if (_currentView.equals(_PACKAGE_VIEW))
              {
                //update tables only if compType to be updated is not current selected component
                if (_currentComponent.getCompPackage().equals(compType.getCompPackage()) == false)
                {
                  _packageJointTypeTableModel.setSelectedPackage(compType.getCompPackage());
                  _packageJointTypeTableModel.updateSpecificPackage(compType.getCompPackage());
                  _currentComponent = compType;

                  selectPackageForCurrentComponent();
                  populateComponentLocationTableForPackage(compType.getCompPackage());
                  selectCurrentComponent();
                }
              }
              _componentLocationTableModel.updateSpecificComponent(compType);
            }
          }

          else if (eventEnum instanceof ComponentTypeEventEnum) // handles joint type changes -- speed improvement
          {
//              System.out.println("  ComponentTypeEventEnum seen");
            ComponentType compType = (ComponentType)projectChangeEvent.getSource();
            
            _shouldInteractWithGraphics = false;
            if (_currentView.equals(_PACKAGE_VIEW))
            {
              CompPackage affectedPackage = compType.getCompPackage();
              // update the package table with any jointType changes
              _packageJointTypeTableModel.populateWithPanelData(_currentBoard, _currentBoardType, _currentSide);
              // update the package for this component
//              System.out.println("    updating package");
              // XCR1736 - Takes 6 minutes to change one joint type in Adjust CAD page
              //int selectedPackageRow = _packageJointTypeTableModel.getRowForCompPackage(affectedPackage);
              _packageJointTypeTableModel.setSelectedPackage(affectedPackage);
              _packageJointTypeTableModel.updateSpecificPackage(affectedPackage);
              
              //Siew Yeng - XCR-2586 - update tables only if compType to be updated is not current selected component
              if(_currentComponent.getCompPackage().equals(affectedPackage) == false)
              {
                _currentComponent = compType;
              // updating the package table resets the current component to the first one in the package type
              // reselect the current component if it still in the table
              //ComponentType currentComponent = _currentComponent;
              // XCR1736 - Takes 6 minutes to change one joint type in Adjust CAD page
              //_packageJointTypeTable.setRowSelectionInterval(selectedPackageRow, selectedPackageRow);
              // get row of the component or row 0 if it is not found
              //int row = _componentLocationTableModel.getRowForComponentType(currentComponent);
              //selectComponentTableRow(row);
                populateComponentLocationTableForPackage(affectedPackage);  
              }
              
              selectPackageForCurrentComponent();
              selectCurrentComponent();
              _componentLocationTableModel.updateSpecificComponent(compType);
              _shouldInteractWithGraphics = true;
            }
            //Khaw Chek Hau - XCR3554: Package on package (PoP) development
            else if (_currentView.equals(_POP_VIEW))
            {
              //Khaw Chek Hau - XCR3554: Package on package (PoP) development    
              CompPackageOnPackage selectedPOP = selectPOPForCurrentComponent();

              populatePOPComponentLocationTableForPOP(selectedPOP);

              selectCurrentComponentForPOPTable();

              _shouldInteractWithGraphics = true;
            }
            else
            {
//              System.out.println("    updating specific component \"" + compType.getReferenceDesignator() + "\"");
              _componentLocationTableModel.updateSpecificComponent(compType);
              //Siew Yeng - comment this as this will cause slow when update multiple components
              //setupComponentLocationTableEditors();
              // get row of the component or row 0 if it is not found
              //int currentComponentRow = _componentLocationTableModel.getRowForComponentType(_currentComponent);
              _shouldInteractWithGraphics = true;
              //selectComponentTableRow(currentComponentRow);
            }
          }
          else if (eventEnum instanceof CompPackageEventEnum)
          {
            if (eventEnum.equals(CompPackageEventEnum.EXPORT_COMPLETE_ENUM))
              return;

            CompPackage selectedPackage = _packageJointTypeTableModel.getSelectedPackage();
            _packageJointTypeTableModel.updateAllPackages(_currentBoardType);
            // XCR1736 - Takes 6 minutes to change one joint type in Adjust CAD page
            //int selectedPackageRow = _packageJointTypeTableModel.getRowForCompPackage(selectedPackage);

            CompPackage compPackage = (CompPackage)projectChangeEvent.getSource();
            _packageJointTypeTableModel.updateSpecificPackage(compPackage);
            // XCR1736 - Takes 6 minutes to change one joint type in Adjust CAD page
            //_packageJointTypeTable.setRowSelectionInterval(selectedPackageRow, selectedPackageRow);
          }

          else if (eventEnum instanceof PackagePinEventEnum)
          {
//            System.out.println("  PackagePinEventEnum seen");
          }
          else if (eventEnum instanceof PadTypeSettingsEventEnum)
          {
            populateAllTables();
//            System.out.println("  PadTypeSettingsEventEnum seen");
          }
          else if (eventEnum instanceof PadTypeEventEnum)
          {
//            System.out.println("  PadTypeEventEnum seen");
          }
          else if (eventEnum instanceof SideBoardEventEnum)
          {
//            System.out.println("  SideBoardEventEnum seen");
            if (eventEnum.equals(SideBoardEventEnum.MOVE_BY_OFFSET))
            {
              // side board move completed
//              System.out.println("    SideBoardEventEnum.MOVE_BY_OFFSET seen");
              // indicate a board/comp is not currently highlighted so graphics is refreshed
              _currentBoardSelectedInGraphics = null;
              _currentComponentSelectedInGraphics = null;
              populateAllTables();
            }
          }
          else if (eventEnum instanceof SubtypeEventEnum)
          {
//            System.out.println("  SubtypeEventEnum seen");
            // the tables in general will stay the same, so all we need to do is update their contents
            _componentLocationTableModel.fireTableRowsUpdated(0, _componentLocationTableModel.getRowCount());
            _packageJointTypeTableModel.fireTableRowsUpdated(0, _packageJointTypeTableModel.getRowCount());
          }
          else
          {
//            System.out.println("glb - FULL repopulate of Verify CAD because of event: " + eventEnum);
            populateAllTables();
          }
        }
      }
    }
  }

  /**
   * @author George Booth
   */
  private void handleGuiChangesEvent(final Object object)
  {
    Assert.expect(object != null && object instanceof GuiEvent);

    GuiEventEnum guiEventEnum = ((GuiEvent)object).getGuiEventEnum();
    if (guiEventEnum instanceof SelectionEventEnum)
    {
      SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
      if (selectionEventEnum.equals(SelectionEventEnum.BOARD_TYPE))
      {
        // a new board type means current board and component is invalid
        _currentBoard = null;
        _currentBoardName = null;
        _currentComponent = null;
        _currentComponentName = null;
        populateWithProjectData();
//        _panelBoardComboBox.setSelectedItem(_currentBoard.getName());
      }
    }
    //Swee Yee Wong - XCR-2348 Change Event that causes verification image warning
    else if (guiEventEnum instanceof CadImageTabEnum)
    {
      CadImageTabEnum cadImageTabEnum = (CadImageTabEnum)guiEventEnum;
      if (cadImageTabEnum.equals(CadImageTabEnum.IMAGE_WINDOW) && _active)
      {
        checkVerificationImages(false);
      }
    }
  }

  /**
   * @author George Booth
   */
  void updatePersistanceSettings()
  {
    TestDevPersistance persistanceSettings = _mainUI.getTestDev().getPersistance();
    persistanceSettings.setVerifyCadPackageCurrentView(_currentView);
    persistanceSettings.setVerifyCadPackageViewDividerLocation(_packageJointTypeSplitPane.getDividerLocation());
    persistanceSettings.setVerifyCadRegionViewDividerLocation(_regionOrSideSplitPane.getDividerLocation());

    persistanceSettings.setVerifyCadComponentTableColumnWidths(_componentLocationTable.getColumnWidths());
//    persistanceSettings.setVerifyCadPackageTableColumnWidths(_packageJointTypeTable.getColumnWidths());
//    persistanceSettings.setVerifyCadRegionTableColumnWidths(_regionOrSideTable.getColumnWidths());

    ProjectPersistance projectPersistSettings = persistanceSettings.getProjectPersistance();
//    System.out.println("currentBoard = " + _currentBoardName);
    projectPersistSettings.setVerifyCadBoardName(_currentBoardName);
//    System.out.println("currentSide = " + _currentSide);
    projectPersistSettings.setVerifyCadBoardSide(_currentSide);
//    System.out.println("currentComponent = " + _currentComponentName);
    projectPersistSettings.setVerifyCadCurrentComponent(_currentComponentName);
  }

  /**
   * @author George Booth
   */
  void restorePersistanceSettings()
  {
    _ignoreActions = true;
    TestDevPersistance persistanceSettings = _mainUI.getTestDev().getPersistance();
    _currentView = persistanceSettings.getVerifyCadPackageCurrentView();
    if (_currentView != null)
    {
      //Khaw Chek Hau - XCR3554: Package on package (PoP) development
      if (_currentView.equals(_PACKAGE_VIEW) || _currentView.equals(_COMPONENT_VIEW) || _currentView.equals(_POP_VIEW) 
            || _currentView.equals(_REGION_VIEW))
      {
//        System.out.println("persisted currentView = " + _currentView);
        // do nothing
      }
      else
      {
        // view name comes from localization file which may have been changed since the view was saved
        _currentView = _PACKAGE_VIEW;
//        System.out.println("default currentView = " + _currentView);
      }
    }
    else
    {
      _currentView = _PACKAGE_VIEW;
//      System.out.println("new currentView = " + _currentView);
    }
    _viewByComboBox.setSelectedItem(_currentView);

    _packageJointTypeSplitPaneDivider = persistanceSettings.getVerifyCadPackageViewDividerLocation();
    _packageJointTypeSplitPane.setDividerLocation(_packageJointTypeSplitPaneDivider);
    _regionOrSideSplitPaneDivider = persistanceSettings.getVerifyCadRegionViewDividerLocation();
    _regionOrSideSplitPane.setDividerLocation(_regionOrSideSplitPaneDivider);

    int[] columnWidths = persistanceSettings.getVerifyCadComponentTableColumnWidths();
    if (columnWidths != null)
    {
      _componentLocationTable.setColumnWidths(columnWidths);
      _componentLocationTableModel.fireTableStructureChanged();
    }

    ProjectPersistance projectPersistSettings = persistanceSettings.getProjectPersistance();
    com.axi.v810.business.panelDesc.Panel panel = Project.getCurrentlyLoadedProject().getPanel();
    _currentBoard = null;
    _currentBoardName = projectPersistSettings.getVerifyCadBoardName();
    if (_currentBoardName != null)
    {
      // make sure board name is still valid
      java.util.List<Board> boardList = panel.getBoards();
      for (Board board : boardList)
      {
        if (board.getName().equals(_currentBoardName))
        {
          _currentBoard = panel.getBoard(_currentBoardName);
//          System.out.println("persisted currentBoard = " + _currentBoardName);
        }
      }
    }
    if (_currentBoard == null)
    {
      _currentBoard = panel.getBoards().get(0);
      _currentBoardName = _currentBoard.getName();
//      System.out.println("default currentBoard = " + _currentBoardName);
    }
    _mainUI.getTestDev().setCurrentBoardType(_currentBoard.getBoardType());

    _currentSide = projectPersistSettings.getVerifyCadBoardSide();
    if (_currentSide != null)
    {
      if (_currentSide.equals(_TOP_SIDE) || _currentSide.equals(_BOTTOM_SIDE) || _currentSide.equals(_BOTH_SIDE))
      {
//        System.out.println("persisted currentSide = " + _currentSide);
        // do nothing
      }
      else
      {
        // side name comes from localization file which may have been changed since the side was saved
        _currentSide = _TOP_SIDE;
//        System.out.println("default currentSide = " + _currentSide);
      }
    }
    else
    {
      _currentSide = _TOP_SIDE;
//      System.out.println("new currentSide = " + _currentSide);
    }
    _sideComboBox.setSelectedItem(_currentSide);

    _currentComponentName = projectPersistSettings.getVerifyCadCurrentComponent();
    if (_currentComponentName != null)
    {
      if (_currentBoard.hasComponent(_currentComponentName))
      {
        _currentComponent = _currentBoard.getBoardType().getComponentType(_currentComponentName);
//        System.out.println("persisted currentComponent = " + _currentComponentName);
      }
      else
      {
        // component was deleted or renamed since it was saved
        _currentComponent = null;
        _currentComponentName = null;
//        System.out.println("default currentComponent = null");
      }
    }
    else
    {
//      System.out.println("currentComponent = null");
    }
    _ignoreActions = false;
  }

  /**
   * @author George Booth
   */
  private void generateVerificationImagesButton_ActionPerformed()
  {
    if(checkAllComponentisSetToNoTest() == false)
    {    
      return;    
    }
        
    _generatingImages = true;
    try
    {
      _mainUI.generateVerificationImagesWithAlignment();
    }
    catch (XrayTesterException ex)
    {
      _mainUI.handleXrayTesterException(ex);
      return;
    }

    _generatingImages = false;

    // did we get any?
    checkVerificationImages(false);
  }
  
  /*
   * @author Kee Chin seong
   * Checking whether the components is set all to no test
   */
  boolean checkAllComponentisSetToNoTest()
  {
    if(_project.getTestProgram().getAllInspectableTestSubPrograms().isEmpty() == true)
    {    
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString("GUI_UNABLE_CREATE_ALIGNMENT_POINT_DUE_TO_NO_TEST_SUBPROGRAM_KEY"),
                                    StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      return false;  
    }
    return true;
  }

  /**
   * Check for valid verification images and issue a message if not valid. Control graphics
   * display appropriately. User has option to generate new images if needed.
   * @author George Booth
   */
  void checkVerificationImages(boolean starting)
  {
//    System.out.println("checkVerificationImages(" + starting + "))");
    boolean showVerificationImages = false;
    boolean showCadGraphics = false;

    if(checkAllComponentisSetToNoTest() == false)
    {    
      return;    
    }
    
    // if alignment is not valid, we should not show verification images
    if (isAlignmentValid(starting) == false)
    {
//      System.out.println("- Alignment not valid - show CAD");
      showCadGraphics = true;
    }
    else
    {
      // decide if we can show verification images

      // if verification images are valid, we can show them
      _haveValidVerificationImages = _project.areVerificationImagesValid(false);
//      _haveValidVerificationImages = true;
      if (_haveValidVerificationImages)
      {
        // images are valid
        if (starting)
        {
          // make sure they are viewable after doing start()
//          System.out.println("- Start in progress - show Images");
          showVerificationImages = true;
        }
        else if (_undoRedoAdjustCadGraphically)
        {
          // make sure they are viewable after an Undo Adjust
//          System.out.println("- Did Undo - show Images");
          showVerificationImages = true;
          _undoRedoAdjustCadGraphically = false;
        }
        else
        {
          // needed when new images are generated
//          System.out.println("- Generated new images - show Images");
          showVerificationImages = true;
        }
      }
      //Swee Yee Wong - XCR-2348 Change Event that causes verification image warning
      else if(_mainUI.getTestDev().isImageWindowSelected())
      {
        // if verification images are not valid, allow user to generate them
        if (_hardwareAvailable == false)
        {
          // no hardware available to get images, just show warning
          JOptionPane.showMessageDialog(
              _mainUI,
              StringUtil.format(StringLocalizer.keyToString("CAD_VERIFICATION_IMAGES_NEEDED_MESSAGE_KEY"), 50),
              StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
              JOptionPane.WARNING_MESSAGE);
//          System.out.println("No hardware - show CAD");
          showCadGraphics = true;
        }
        else
        {
          int choice = ChoiceInputDialog.showConfirmDialog(
              _mainUI,
              StringUtil.format(StringLocalizer.keyToString("CAD_VERIFICATION_IMAGES_NEEDED_MESSAGE_KEY") + " " +
                                StringLocalizer.keyToString("CAD_GENERATE_VERIFICATION_IMAGES_MESSAGE_KEY"), 50),
              StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"));
          if (choice == JOptionPane.YES_OPTION)
          {
//            System.out.println("- Generating new images...");
            // get new images
            _generatingImages = true;
            try
            {
              _mainUI.generateVerificationImagesWithAlignment();
            }
            catch (XrayTesterException ex)
            {
              _mainUI.handleXrayTesterException(ex);
            }
            _generatingImages = false;

            // did we get any?
            _haveValidVerificationImages = _project.areVerificationImagesValid(false);
            if (_haveValidVerificationImages)
            {
//              System.out.println("- Got new images - show Images");
              showVerificationImages = true;
            }
            else
            {
//              System.out.println("- Could not get new images - show CAD");
              showCadGraphics = true;
            }
          }
          else
          {
            // user decided not to generate them
//            System.out.println("- User did not want new images - show CAD");
            showCadGraphics = true;
            // Kee Chin Seong - always return IF user click no because we dont want the image get washed off.
            return;
          }
        }
      }
      //Swee Yee Wong - XCR-2348 Change Event that causes verification image warning
      else
      {
        showCadGraphics = true;
        return;
      }
    }

    if (showVerificationImages)
    {
      // update graphics
//      System.out.println("** displayVerificationImagePanel()");
      boolean checkLatestTestProgram = false;
      if(_project.getPanel().getPanelSettings().hasLowMagnificationComponent() && 
         _lowMagAlignmentValid)
        _imageManagerPanel.displayVerificationImagePanel(checkLatestTestProgram);
      else
        _imageManagerPanel.displayVerificationImagePanelForHighMag(checkLatestTestProgram);
      // switch to Image tab
//      System.out.println("** showImageWindow()");
      _mainUI.getTestDev().showImageWindow();
    }
    else if (showCadGraphics)
    {
      // update graphics (clear)
//      System.out.println("** finishVerificationImagePanel()");
      if(_project.getPanel().getPanelSettings().hasLowMagnificationComponent())
        _imageManagerPanel.finishVerificationImagePanel();
      else
        _imageManagerPanel.finishVerificationImagePanelForHighMag();
      // switch to CAD tab
//      System.out.println("** showCadWindow()");
      _mainUI.getTestDev().showCadWindow();
    }
    else
    {
      // no action needed
    }

    // indicate a board/comp is not currently highlighted so graphics is refreshed
    _currentBoardSelectedInGraphics = null;
    _currentComponentSelectedInGraphics = null;
//    selectCurrentComponent();
    highlightBoardAndComponent();
  }

  /**
   * Check for valid alignment and issue a message if not valid
   * @param starting true if called from start()
   * @author George Booth
   */
  boolean isAlignmentValid(boolean starting)
  {
//    _alignmentValid = _project.getPanel().getPanelSettings().isManualAlignmentCompleted();
    //Siew Yeng - XCR1639 - mix magnification caused adjust cad crash
    _lowMagAlignmentValid = _project.getPanel().getPanelSettings().areManualAlignmentTransformsValid();
    _highMagAlignmentValid = _project.getPanel().getPanelSettings().areManualAlignmentTransformsForHighMagValid();
    
    _alignmentValid = _project.getPanel().getPanelSettings().isManualAlignmentCompleted() &&
                      (_lowMagAlignmentValid || _highMagAlignmentValid);
    
    _generateVerificationImagesButton.setEnabled(_alignmentValid);
    if (_alignmentValid == false)
    {
      // warn user
      String message = StringUtil.format(StringLocalizer.keyToString("CAD_ALIGNMENT_NEEDED_MESSAGE_KEY"), 50);
      if (starting == false)
      {
        message = StringUtil.format(StringLocalizer.keyToString("CAD_ALIGNMENT_NEEDED_AFTER_ADJUSTMENT_MESSAGE_KEY"), 50);
      }

      //Siew Yeng - Alignment COMPLETE but NOT VALID
      if(_project.getPanel().getPanelSettings().isManualAlignmentCompleted())
      {
        message = StringUtil.format(StringLocalizer.keyToString("CAD_UNABLE_TO_DISPLAY_VERIFICATION_IMAGE_MESSAGE_KEY"), 50);
      }
      
      JOptionPane.showMessageDialog(
            _mainUI,
            message,
            StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
            JOptionPane.WARNING_MESSAGE);
    }
    return _alignmentValid;
  }

  /**
   * Called from EditCad panel when tab is switched to VerifyCAD
   * @author George Booth
   */
  void start(ImageManagerPanel imageManagerPanel)
  {
//    System.out.println("VerifyCad.start()");
    Assert.expect(imageManagerPanel != null);
    
    _imageManagerPanel = imageManagerPanel;
    
    //_imageManagerPanel.getVerificationImagePanel().setTopPadLayerVisibility(false);

    // start() may be called when a navigation is cancelled (not ready to finish)
    // ignore it
    if (_active)
      return;

    com.axi.util.TimerUtil screenTimer = new com.axi.util.TimerUtil();
    screenTimer.start();

    restorePersistanceSettings();

    _active = true;

    //Khaw Chek Hau - XCR3554: Package on package (PoP) development    
//    // alignment and verification images must be valid for adjustments with images
//    checkVerificationImages(true);

    // make sure graphics is updated when first entered
    _shouldInteractWithGraphics = true;
    _currentBoardSelectedInGraphics = null;
    _currentComponentSelectedInGraphics = null;

    populateWithProjectData(Project.getCurrentlyLoadedProject());
    
    // alignment and verification images must be valid for adjustments with images
    checkVerificationImages(true); 

    _imageManagerObservable.addObserver(this);
    _projectObservable.addObserver(this);
    _commandManager.addObserver(this);
    _guiObservable.addObserver(this);
    
    screenTimer.stop();
//    System.out.println("  Total Loading for Verify CAD: (in mils): "+ screenTimer.getElapsedTimeInMillis());
  }

  /**
   * @author George Booth
   */
  private UndoState getCurrentUndoState()
  {
    UndoState undoState = new UndoState();
    undoState.setVerifyCadViewSelectIndex(_viewByComboBox.getSelectedIndex());
    undoState.setVerifyCadSideSelectIndex(_sideComboBox.getSelectedIndex());
    undoState.setAdjustCadGraphically(_lastButton == _adjustComponentRadioButton ||
                                      _lastButton == _adjustBoardSideRadioButton ||
                                      _lastButton == _adjustBoardOriginRadioButton);
    return undoState;
  }

  /**
   * Called from EditCad panel when tab is switched away from VerifyCAD
   * @author George Booth
   */
  boolean isReadyToFinish()
  {
//    System.out.println("VerifyCad.isReadyToFinish()");
    if (_adjusting)
    {
      // try to cancel adjustments
      _adjustOffRadioButton.doClick();
      // return true if user allowed adjustment to be cancelled
      return (_adjusting == false);
    }
    else
    {
      // package/joint type table uses combo boxes

      // make sure component location table is finished editing
      if (_componentLocationTable.isEditing())
        _componentLocationTable.getCellEditor().stopCellEditing();
      //Khaw Chek Hau - XCR3554: Package on package (PoP) development
      if (_popComponentLocationTable.isEditing())
        _popComponentLocationTable.getCellEditor().stopCellEditing();
      return true;
    }
  }

  /**
   * Called from EditCad panel when tab is switched away from VerifyCAD
   * @author George Booth
   */
  void finish()
  {
//    System.out.println("VerifyCad.finish()");
    if (_componentLocationTable.isEditing())
      _componentLocationTable.cancelCellEditing();
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    if (_popComponentLocationTable.isEditing())
      _popComponentLocationTable.cancelCellEditing();
    if (_packageJointTypeTable.isEditing())
      _packageJointTypeTable.cancelCellEditing();

    if (_project.doesPanelExist() &&
        _project.getPanel().getNumBoards() > 0)
      updatePersistanceSettings();

    _shouldInteractWithGraphics = false;
    _imageManagerObservable.deleteObserver(this);
    _commandManager.deleteObserver(this);
    _guiObservable.deleteObserver(this);
    _active = false;
  }
  
  /**
   * This method implements the right click edit when multiple lines of a table are selected
   * @author Andy Mechtenberg
   * Siew Yeng - XCR-2123 - copy this from GroupingPanel.java
   */
  private void multiEditTableMouseReleased(MouseEvent e, JTable table)
  {
    Assert.expect(e != null);
    Assert.expect(table != null);

    // ignore any other mouse events than a popup trigger -- we don't want to break the normal L&F
    if (e.isPopupTrigger())
    {
      // get the table cell that the mouse was right-clicked in
      Point p = e.getPoint();
      int row = table.rowAtPoint(p);
      int col = table.columnAtPoint(p);

      // if this cell is not currently selected, exit.  It's confusing to edit a cell that not selected.
      // we could "select" this cell too, but for now, we'll just not.
      if (table.isCellSelected(row, col) == false)
        return;

      int selectedRows[] = table.getSelectedRows();
      java.util.List<Object> selectedData = new ArrayList<Object>();
      for (int i = 0; i < selectedRows.length; i++)
      {
        selectedData.add(_componentLocationTableModel.getComponentTypeAt(selectedRows[i]));
      }
      
      _mergeComponentsToPOPPopupMenu.show(e.getComponent(), e.getX(), e.getY());
      
      // start the editing process for this cell
      table.editCellAt(row, col);
      // if this is a combo box cell editor, the editCellAt doesn't show the combo box popup, so let's show that
      // only if this is a combo box editor
      Object obj = table.getEditorComponent();
      if (obj instanceof JComboBox)
      {
        JComboBox comboBox = (JComboBox)obj;
        comboBox.showPopup();
      }
      else if (obj instanceof NumericTextField)
      {
        NumericTextField textField = (NumericTextField)obj;
        textField.requestFocus();
      }
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void packageOnPackageTableMouseReleased(MouseEvent e, JTable table)
  {
    Assert.expect(e != null);
    Assert.expect(table != null);
    
    // ignore any other mouse events than a popup trigger -- we don't want to break the normal L&F
    if (e.isPopupTrigger())
    {
      // get the table cell that the mouse was right-clicked in
      Point p = e.getPoint();
      int row = table.rowAtPoint(p);
      int col = table.columnAtPoint(p);

      // if this cell is not currently selected, exit.  It's confusing to edit a cell that not selected.
      // we could "select" this cell too, but for now, we'll just not.
      if (table.isCellSelected(row, col) == false)
        return;

      int selectedRows[] = table.getSelectedRows();
      java.util.List<Object> selectedData = new ArrayList<Object>();
      for (int i = 0; i < selectedRows.length; i++)
      {
        selectedData.add(_packageOnPackageTableModel.getPackageAt(selectedRows[i]));
      }
      
      _packageOnPackagePopupMenu.show(e.getComponent(), e.getX(), e.getY());
      
      // start the editing process for this cell
      table.editCellAt(row, col);
      // if this is a combo box cell editor, the editCellAt doesn't show the combo box popup, so let's show that
      // only if this is a combo box editor
      Object obj = table.getEditorComponent();
      if (obj instanceof JComboBox)
      {
        JComboBox comboBox = (JComboBox)obj;
        comboBox.showPopup();
      }
      else if (obj instanceof NumericTextField)
      {
        NumericTextField textField = (NumericTextField)obj;
        textField.requestFocus();
      }
    }
  }
  
  /**
  * @author Siew Yeng - copy this function from EditCadPanel.java
  * Chin-Seong, kee - this is for Offset X, Y use, when need handle multiple pad in the sametime
  * for e.g. BGA can mutliple select whole region which user desired and make the offset
  */
  public void setIsOffsetBeenSet(boolean isOffset)
  {
    _isOffsetBeenSet = isOffset;
  }
 
  /**
  * @author Siew Yeng - copy this function from EditCadPanel.java
  * Chin-Seong, kee - this is for Offset X, Y use, when need handle multiple pad in the sametime
  * for e.g. BGA can mutliple select whole region which user desired and make the offset
  */
  public boolean isOffsetBeenSet()
  {
    return _isOffsetBeenSet;
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void mergeComponentToPOPMenuItemActionPerformed(ActionEvent e)
  {        
    int selectedRows[] = _componentLocationTable.getSelectedRows();
    
    _componentTypes = new ArrayList<ComponentType>();
    
    boolean isAllComponentsAtSameSide = true;
    
    for (int i = 0; i < selectedRows.length; i++)
    {
      ComponentType componentType = (ComponentType) _componentLocationTableModel.getComponentTypeAt(selectedRows[i]);
      if (componentType != null)
        _componentTypes.add(componentType);
      

    }
    
    for (ComponentType compType: _componentTypes)
    {
      if (compType.getComponents().get(0).getSideBoard().isTopSide() != _componentTypes.get(0).getComponents().get(0).getSideBoard().isTopSide())
      {
        isAllComponentsAtSameSide = false;
        break;
      }
    }
    
    if (_componentTypes == null || _componentTypes.size() == 1) 
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    StringLocalizer.keyToString("CAD_MUST_AT_LEAST_TWO_COMPONENTS_FOR_POP_KEY"),
                                    StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
                                    true);
    }
    else if (_componentTypes.size() > POPLayerIdEnum.getOrderedLayerIdList().size())
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    StringLocalizer.keyToString(new LocalizedString("CAD_MORE_THAN_MAX_NUMBER_OF_COMPONENTS_FOR_POP_KEY", 
                                                                new Object[]{String.valueOf(POPLayerIdEnum.getOrderedLayerIdList().size())})),
                                    StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
                                    true);
    }
    else if (isAllComponentsAtSameSide == false)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    StringLocalizer.keyToString("CAD_MUST_ALL_COMPONENTS_AT_THE_SAME_SIDE_FOR_POP_KEY"),
                                    StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
                                    true);
    }
    else
    {            
      for (ComponentType compType: _componentTypes)
      {        
        if (compType.hasCompPackageOnPackage())  
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATED_POP_ERROR_MESSAGE_KEY", 
                                                                    new Object[]{compType, 
                                                                    compType.getCompPackageOnPackage().getPOPName()})),
                                        StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
                                        true);
          return;
        }
      }
      
      _commandManager.trackState(getCurrentUndoState());
      
      MergeComponentsIntoPOPDialog dlg = new MergeComponentsIntoPOPDialog(_componentTypes, 
                                                                          _project, 
                                                                          _mainUI, 
                                                                          StringLocalizer.keyToString("CAD_MERGE_COMPONENTS_INTO_POP_DIALOG_TITLE_KEY"));
      dlg.setProject(_project);
      SwingUtils.centerOnComponent(dlg, _mainUI);
      dlg.setVisible(true);
    }
  }
            
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private void packageOnPackageMenuItemActionPerformed(ActionEvent e)
  {        
    int selectedRows[] = _packageOnPackageTable.getSelectedRows();
    
    _compPackageOnPackages = new ArrayList<CompPackageOnPackage>();
    
    for (int i = 0; i < selectedRows.length; i++)
    {
      CompPackageOnPackage compPackageOnPackage = (CompPackageOnPackage) _packageOnPackageTableModel.getPackageAt(selectedRows[i]);
      if (compPackageOnPackage != null)
        _compPackageOnPackages.add(compPackageOnPackage);
    }
    
    if (_compPackageOnPackages == null)  // combo box was empty, so we cannot continue
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    StringLocalizer.keyToString("CAD_NO_POP_SELECTED_KEY"),
                                    StringLocalizer.keyToString("CAD_ERROR_TITLE_KEY"),
                                    true);
    }
    else
    {
      for (CompPackageOnPackage compPackageOnPackage : _compPackageOnPackages)
      {
        compPackageOnPackage.destroy();
      }
      
      populateAllTables();
    }
  }
}
