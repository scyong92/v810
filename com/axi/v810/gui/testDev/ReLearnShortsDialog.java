package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.algoTuner.*;
import com.axi.v810.util.*;

public class ReLearnShortsDialog extends EscapeDialog
{
  private boolean _canceled = true;
  private Project _project;
  private JointTypeChoice _jointTypeChoice;
  private SubtypeChoice _subtypeChoice;
  private Object _panelBoardChoice;

  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private Border _empty10Border;
  private JPanel _buttonsPanel = new JPanel();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private JPanel _innerButtonsPanel = new JPanel();
  private GridLayout _innerButtonsPanelGridLayout = new GridLayout();
  private FlowLayout _buttonsPanelFlowLayout = new FlowLayout();

  private JPanel _contentPanel = new JPanel();
  private BorderLayout _contentPanelBorderLayout = new BorderLayout();
  private JPanel _radioButtonPanel = new JPanel();
  private GridLayout _radioButtonFlowLayout = new GridLayout();

  private JRadioButton _relearnOnlyRadioButton = new JRadioButton();
  private JRadioButton _relearnAllExceptRadioButton = new JRadioButton();

  private JPanel _jointTypeSubtypePanel = new JPanel();
  private JPanel _jointTypePanel = new JPanel();
  private JPanel _subtypePanel = new JPanel();
  private JPanel _boardPanel = new JPanel();

  private GridLayout _jointTypeSubtypeGridLayout = new GridLayout(3, 1);
  private FlowLayout _jointTypeFlowLayout = new FlowLayout(FlowLayout.LEFT);
  private FlowLayout _subtypeFlowLayout = new FlowLayout(FlowLayout.LEFT);
  private FlowLayout _boardFlowLayout = new FlowLayout(FlowLayout.LEFT);

  private JLabel _jointTypeLabel = new JLabel();
  private JLabel _subtypeLabel = new JLabel();
  private JLabel _boardLabel = new JLabel();

  private JComboBox _jointTypeComboBox = new JComboBox();
  private VariableWidthComboBox _subtypeComboBox = new VariableWidthComboBox();
  private JComboBox _boardSelectComboBox = new JComboBox();
  
  private BoardType _boardType = null;

  /**
   * @author Andy Mechtenberg
   */
  ReLearnShortsDialog(Frame parent, Project project, JointTypeChoice jointTypeChoice, SubtypeChoice subtypeChoice, Object panelBoardChoice, BoardType boardType)
  {
    super(parent, StringLocalizer.keyToString("MMGUI_INITIAL_RELEARN_SHORTS_DIALOG_TITLE_KEY"), true);
    Assert.expect(parent != null);
    Assert.expect(project != null);
    Assert.expect(jointTypeChoice != null);
    Assert.expect(subtypeChoice != null);
    Assert.expect(panelBoardChoice != null);
    Assert.expect(boardType != null);

    _project = project;
    _jointTypeChoice = jointTypeChoice;
    _subtypeChoice = subtypeChoice;
    _panelBoardChoice = panelBoardChoice;
    _boardType = boardType;
    jbInit();
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean wasCanceled()
  {
    return _canceled;
  }

  /**
   * @author Andy Mechtenberg
   */
  JointTypeChoice getJointTypeChoice()
  {
    return _jointTypeChoice;
  }

  /**
   * @author Andy Mechtenberg
   */
  SubtypeChoice getSubtypeChoice()
  {
    return _subtypeChoice;
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean isRelearnOnlySelected()
  {
    return _relearnOnlyRadioButton.isSelected();
  }

  /**
   * @author Andy Mechtenberg
   */
  void clear()
  {
    _project = null;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    _innerButtonsPanel.setLayout(_innerButtonsPanelGridLayout);
    _innerButtonsPanelGridLayout.setHgap(10);
    _buttonsPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _buttonsPanelFlowLayout.setVgap(0);
    _mainPanelBorderLayout.setVgap(10);
    _buttonsPanel.add(_innerButtonsPanel, null);
    _innerButtonsPanel.add(_okButton, null);
    _innerButtonsPanel.add(_cancelButton, null);

    _buttonsPanel.setLayout(_buttonsPanelFlowLayout);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed();
      }
    });
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed();
      }
    });

    _empty10Border = BorderFactory.createEmptyBorder(10,10,10,10);
    _mainPanel.setBorder(_empty10Border);
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _mainPanel.add(_buttonsPanel, BorderLayout.SOUTH);
    getContentPane().add(_mainPanel, BorderLayout.CENTER);
    getRootPane().setDefaultButton(_okButton);

    ButtonGroup radioButtonGroup = new ButtonGroup();
    radioButtonGroup.add(_relearnOnlyRadioButton);
    radioButtonGroup.add(_relearnAllExceptRadioButton);

    _contentPanel.setLayout(_contentPanelBorderLayout);
    _radioButtonPanel.setLayout(_radioButtonFlowLayout);
    _radioButtonFlowLayout.setRows(1);
    _radioButtonFlowLayout.setColumns(2);
    _relearnOnlyRadioButton.setText(StringLocalizer.keyToString("MMGUI_INITIAL_RELEARN_UPDATE_ONLY_KEY"));
    _relearnOnlyRadioButton.setSelected(true);
    _relearnAllExceptRadioButton.setText(StringLocalizer.keyToString("MMGUI_INITIAL_RELEARN_ALL_EXCEPT_KEY"));
    _radioButtonPanel.add(_relearnOnlyRadioButton);
    _radioButtonPanel.add(_relearnAllExceptRadioButton);
    _contentPanel.add(_radioButtonPanel, BorderLayout.NORTH);


    _jointTypeComboBox.setPreferredSize(new Dimension(180, CommonUtil._COMBO_BOX_HEIGHT));
    _subtypeComboBox.setPreferredSize(new Dimension(180, CommonUtil._COMBO_BOX_HEIGHT));
    _boardSelectComboBox.setPreferredSize(new Dimension(180, CommonUtil._COMBO_BOX_HEIGHT));

    _jointTypeComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jointTypeComboBox_actionPerformed();
      }
    });

    _jointTypeLabel.setText(StringLocalizer.keyToString("ATGUI_FAMILY_KEY"));
    _subtypeLabel.setText(StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY"));
    _boardLabel.setText(StringLocalizer.keyToString("ATGUI_PANEL_BOARD_KEY"));

    _jointTypeSubtypePanel.setLayout(_jointTypeSubtypeGridLayout);
    _jointTypeSubtypePanel.add(_jointTypePanel);
    _jointTypeSubtypePanel.add(_subtypePanel);
    _jointTypeSubtypePanel.add(_boardPanel);

    _jointTypePanel.setLayout(_jointTypeFlowLayout);
    _jointTypePanel.add(_jointTypeLabel);
    _jointTypePanel.add(_jointTypeComboBox);

    _subtypePanel.setLayout(_subtypeFlowLayout);
    _subtypePanel.add(_subtypeLabel);
    _subtypePanel.add(_subtypeComboBox);
    
    _boardPanel.setLayout(_boardFlowLayout);
    _boardPanel.add(_boardLabel);
    _boardPanel.add(_boardSelectComboBox);

    _contentPanel.add(_jointTypeSubtypePanel, BorderLayout.CENTER);

    _mainPanel.add(_contentPanel, BorderLayout.CENTER);

    JLabel instructionLabel = new JLabel(StringLocalizer.keyToString("MMGUI_INITIAL_RELEARN_INSTRUCTIONS_KEY"));
    _mainPanel.add(instructionLabel, BorderLayout.NORTH);

    JLabel[] labels = new JLabel[]{_jointTypeLabel, _subtypeLabel};
    SwingUtils.makeAllSameLength(labels);

    populateJointTypeSubtype();

    pack();
    _okButton.requestFocus();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateJointTypeSubtype()
  {
    PanelDataUtil.populateComboBoxWithJointTypes(_project.getPanel(), _jointTypeComboBox);
    _jointTypeComboBox.setSelectedItem(_jointTypeChoice);
    populateSubtypeComboBox();
    populateBoardComboBox();
    _subtypeComboBox.setSelectedItem(_subtypeChoice);
    
    if (_panelBoardChoice instanceof Board)
    {
      Board board = (Board)_panelBoardChoice;
      _boardSelectComboBox.setSelectedItem(board);
    }
    else
      _boardSelectComboBox.setSelectedIndex(0);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jointTypeComboBox_actionPerformed()
  {
    populateSubtypeComboBox();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateSubtypeComboBox()
  {
    _subtypeComboBox.setPopupWidthToDefault();
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    if (jointTypeChoice == null)
      return;

    PanelDataUtil.populateComboBoxWithSubtypes(_project.getPanel(), jointTypeChoice, _subtypeComboBox);
    FontMetrics fontMetrics = _subtypeComboBox.getFontMetrics(_subtypeComboBox.getFont());
    int maxStringLength = 0;

    if (jointTypeChoice.isAllFamilies() == false)
    {
      java.util.List<Subtype> subtypes = _project.getPanel().getInspectedSubtypes(jointTypeChoice.getJointTypeEnum());

      for (Subtype subtype : subtypes)
      {
        int itemStringLength = fontMetrics.stringWidth(subtype.toString());
        if (maxStringLength < itemStringLength)
          maxStringLength = itemStringLength;
      }
      // set the popup width to the max of the strings only if that's longer than the box itself
      if (maxStringLength <= _subtypeComboBox.getWidth())
        _subtypeComboBox.setPopupWidthToDefault();
      else
        _subtypeComboBox.setPopupWidth(maxStringLength + 3); // add a bit more so the last char is not RIGHT at the popup right side.
    }
    _subtypeComboBox.setMaximumRowCount(20);
  }

  /**
   * Adds the specified board to the panel.  Does appropriate checks first to make
   * sure that the specified information is valid.  Closes the dialog.
   * @author Carli Connally
   */
  private void okButton_actionPerformed()
  {
    _jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    _subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();

    // special case:  update all except All-All makes no sense:  Give error and return to the dialog
    if (_relearnAllExceptRadioButton.isSelected() && _jointTypeChoice.isAllFamilies() && _subtypeChoice.isAllSubtypes())
    {
      // too many boards -- this would be slow.  Give an error, but don't exit.
      LocalizedString message = new LocalizedString("MMGUI_INITIAL_RELEARN_CANNOT_EXCLUDE_ALL_KEY", new Object[]{});
      MessageDialog.showErrorDialog(this, StringLocalizer.keyToString(message),
          StringLocalizer.keyToString("MMGUI_INITIAL_RELEARN_SHORTS_DIALOG_TITLE_KEY"),
          true);
      return;
    }

    _canceled = false;
    dispose();
  }

  /**
   * User presses the cancel button.  The operation is cancelled.  No board is added
   * to the panel.  The state of the panel remains the same as when the dialog was opened.
   * @author Carli Connally
   */
  private void cancelButton_actionPerformed()
  {
    _canceled = true;
    dispose();
  }

  /**
   * @author Cheah Lee Herng
   */
  private void populateBoardComboBox()
  {
    _boardSelectComboBox.removeAllItems();

    _boardSelectComboBox.addItem(StringLocalizer.keyToString("ATGUI_PANEL_KEY"));

    java.util.List<Board> boardInstances = _project.getPanel().getBoards();
    // if only one board instance, no need to put it in the boardinstance combobox
    // as the panel entry is enough
    if (boardInstances.size() > 1)
    {
      // sort boards based on name
      Collections.sort(boardInstances, new AlphaNumericComparator());
      for (Board board : boardInstances)
      {
        if (board.getBoardType().equals(_boardType))
          _boardSelectComboBox.addItem(board);
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isBoardInstanceSelected()
  {
    Object selection = _boardSelectComboBox.getSelectedItem();
    if (selection instanceof Board)
      return true;
    else
      return false;
  }

  /**
   * Before calling this function, we must call isBoardInstanceSelected()
   * to make sure we selected board. This is because _boardSelectComboBox
   * can contain both String and Board object.
   *
   * @author Cheah Lee Herng 
   */
  public Board getSelectedBoardInstance()
  {
    Object selection = _boardSelectComboBox.getSelectedItem();
    Assert.expect(selection instanceof Board);
    return (Board)selection;
  }
}
