package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Erica Wheatcroft
 */
public class SignalCompensationEnumComparator implements Comparator<SignalCompensationEnum>
{
  private boolean _ascending;

  /**
   * @author Erica Wheatcroft
   */
  public SignalCompensationEnumComparator(boolean ascending)
  {
    _ascending = ascending;
  }

  /**
   * @author Erica Wheatcroft
   */
  public int compare(SignalCompensationEnum lhSignalCompensationEnum, SignalCompensationEnum rhSignalCompensationEnum)
  {
    Assert.expect(lhSignalCompensationEnum != null);
    Assert.expect(rhSignalCompensationEnum != null);

    String lhName = lhSignalCompensationEnum.toString();
    String rhName = rhSignalCompensationEnum.toString();

    if (_ascending)
      return lhName.compareToIgnoreCase(rhName);
    else
      return rhName.compareToIgnoreCase(lhName);
  }

}
