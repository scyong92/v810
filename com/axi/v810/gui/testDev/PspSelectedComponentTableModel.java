package com.axi.v810.gui.testDev;

import com.axi.guiUtil.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;


/**
 * 
 * @author Jack Hwee
 */
class PspSelectedComponentTableModel extends DefaultSortTableModel
{
  private static final String _BOARD_NAME = StringLocalizer.keyToString("MMGUI_SETUP_BOARD_NAME_KEY");
  private static final String _REGION_NAME = StringLocalizer.keyToString("MMGUI_SETUP_REF_DES_KEY");
  private static final String _JOINT_TYPE = StringLocalizer.keyToString("ATGUI_FAMILY_KEY");
  private static final String _STATUS = StringLocalizer.keyToString("ATGUI_EMPTY_STATUS_KEY");

  private String[] _columnLabels = { _BOARD_NAME,
                                     _REGION_NAME,
                                     _JOINT_TYPE,
                                     _STATUS
                                   };

  private JTable _table = null;
  private Project _project = null;
  private List<Board> _boards;
  private List<Board> _selectedBoards;
  private static List<Component> _components  = new ArrayList<Component>();
  
  private static transient ComponentComparator _componentNameComparator;
  
  static
  {
    // Default to Board Name sorting
    _componentNameComparator =  new ComponentComparator(true, ComponentComparatorEnum.BOARD_NAME);
  }

  /**
   * Constructor.
   * @author Carli Connally
   */
  public PspSelectedComponentTableModel (JTable table)
  {
    _table = table;
  }

  /**
   * @author Andy Mechtenberg
   */
  void clearProjectData()
  {
    _project = null;
    if (_boards != null)
      _boards.clear();
    if (_selectedBoards != null)
      _selectedBoards.clear();
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Carli Connally
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }
  
  /**
   * @author Andy Mechtenberg
   */
  public int getRowCount()
  {
    if (_components == null)
      return 0;
    else
      return _components.size();
  }

  /**
   * Overridden method.
   * @param column Index of the column
   * @return Column label for the column specified
   * @author Carli Connally
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.  Gets the values from the project object.
   * @param rowIndex Index of the row
   * @param columnIndex Index of the column
   * @return Object containing the value
   * @author Carli Connally
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_project != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    Component component = _components.get(rowIndex);
 
    if (columnIndex == 0) // number
      return component.getBoard();
    else if (columnIndex == 1) // subytpe
    {
      return component;
    }
    else if (columnIndex == 2)  // Joint Type
    {
      return TestDev.getComponentDisplayJointType(component.getComponentType());
    }
    else if (columnIndex == 3) // Status
    {
      return TestDev.getComponentDisplayStatus(component.getComponentType());
    }
    return null;

  }

  /**
   * Overridden method.
   * @param rowIndex  Index of the row
   * @param columnIndex  Index of the column
   * @return boolean - whether the cell is editable
   * @author Carli Connally
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    boolean isEditable = false;

    return isEditable;
  }

   /**
   * @author Andy Mechtenberg
   * @author Ying-Huan.Chu
   */
  void populateWithPanelData(BoardType selectedBoardType)
  {
    if (selectedBoardType.getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      for (OpticalRegion opticalRegion: selectedBoardType.getPanel().getPanelOpticalRegions())
      {
        if (_components.containsAll(opticalRegion.getComponents()) == false)
          _components.addAll(opticalRegion.getComponents());
      }
    }
    else
    {
      for (Board board : selectedBoardType.getBoards())
      {
        if (board.hasBoardSurfaceMapSettings())
        {
          BoardSurfaceMapSettings boardSurfaceMapSettings = board.getBoardSurfaceMapSettings();
          for (OpticalRegion boardOpticalRegion : boardSurfaceMapSettings.getOpticalRegions())
          {
            for (com.axi.v810.business.panelDesc.Component component : boardOpticalRegion.getComponents(board))
            {
              if (_components.contains(component) == false)
              {
                _components.add(component);
              }
            }
          }
        }
      }
    }
    _project = selectedBoardType.getPanel().getProject();

    fireTableDataChanged();
    sortColumn(0 ,true);
  }
  
  /**
   * @author Jack Hwee
   */
  void populateWithOpticalRegion(OpticalRegion opticalRegion)
  {
    if (opticalRegion != null)
    {
      _components = opticalRegion.getComponents();
    }
    else
    {
      _components.clear();
    }
    
    fireTableDataChanged();
    sortColumn(0 ,true);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  void populateWithOpticalRegion(OpticalRegion opticalRegion, Board board)
  {
    //Assert.expect(opticalRegion != null);
    //Assert.expect(board != null);
    if (opticalRegion != null)
    {
      if (_project.getPanel().getPanelSettings().isPanelBasedAlignment())
      {
        _components = opticalRegion.getComponents(board);
      }
      else
      {
        for (com.axi.v810.business.panelDesc.Component component : opticalRegion.getComponents(board))
        {
          if (_components.contains(component) == false)
          {
            _components.add(component);
          }
        }
      }
    }
    else
    {
      _components.clear();
    }
    
    fireTableDataChanged();
    sortColumn(0 ,true);
  }
  
  /**
   * Returns all components in the table.
   * @author Jack Hwee
   */
  public java.util.List<Component> getComponents()
  {
    return _components;
  }
  
  /**
   * @author Jack Hwee
   */
  void clear()
  {
    if (_components.isEmpty() == false)
      _components.clear();
  }
  
  /**
   * Overridden method.  Saves the data entered by the user in the table
   * to the project object.
   * @param object Object containing the data to be saved to the model
   * @param rowIndex Index of the row
   * @param columnIndex Index of the column
   * @author Carli Connally
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
  }

  /**
   * Get the board instance associated with the row index
   * @param rowIndex the index of the row
   * @author George A. David
   */
  public Board getBoard(int rowIndex)
  {
    Assert.expect(rowIndex >= 0);

    List<Board> boards = _project.getPanel().getBoards();
    Assert.expect(rowIndex < boards.size());

    return (Board)boards.get(rowIndex);
  }

  /**
   * @author Andy Mechtenberg
   */
  void setSelectedBoards(List<Board> selectedBoards)
  {
    Assert.expect(selectedBoards != null);
    _selectedBoards = selectedBoards;
  }

  /**
   * @author Andy Mechtenberg
   */
  int getIndexForBoard(Board board)
  {
    Assert.expect(board != null);
    return _boards.indexOf(board);
  }
  
  /**
   * @author Andy Mechtenberg
   */
  public void sortColumn(int column, boolean ascending)
  {
    switch(column)
    {
      case 0:  // sort on board name
      {
        _componentNameComparator.setAscending(ascending);
        _componentNameComparator.setComponentComparatorEnum(ComponentComparatorEnum.BOARD_NAME);
        Collections.sort(_components, _componentNameComparator);
        break;
      }
      case 1:  // sort on component name
      {
        _componentNameComparator.setAscending(ascending);
        _componentNameComparator.setComponentComparatorEnum(ComponentComparatorEnum.REFERENCE_DESIGNATOR);
        Collections.sort(_components, _componentNameComparator);
        break;
      }
      case 2:  // sort on joint type
      {
        _componentNameComparator.setAscending(ascending);
        _componentNameComparator.setComponentComparatorEnum(ComponentComparatorEnum.JOINT_TYPE);
        Collections.sort(_components, _componentNameComparator);
        break;
      }
      case 3: // sort on component usability status
      {
        _componentNameComparator.setAscending(ascending);
        _componentNameComparator.setComponentComparatorEnum(ComponentComparatorEnum.STATUS);
        Collections.sort(_components, _componentNameComparator);
        break;
      }
      default:
        Assert.expect(false, "column does not exist in table model");
        break;  
    }
  }
  
  /**
   * Return a list of selected components.
   * @author Cheah Lee Herng
   */
  public List<Component> getSelectedComponents()
  {
    Assert.expect(_components != null);
    
    List<Component> selectedComponents = new ArrayList<Component>();
    
    int[] selectedRowIndices = _table.getSelectedRows();
    for(int index : selectedRowIndices)
    {
      selectedComponents.add(_components.get(index));
    }
    
    return selectedComponents;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void addComponent(Component component)
  {
    Assert.expect(_components != null);
   
    if (_components.contains(component) == false)
    {
      _components.add(component);
    }
    fireTableDataChanged();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void addComponents(java.util.List<Component> components)
  {
    for (Component component : components)
    {
      addComponent(component);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void removeComponent(Component component)
  {
    Assert.expect(_components != null);
    
    if (_components.contains(component))
    {
      _components.remove(component);
    }
    fireTableDataChanged();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removeComponents(java.util.List<Component> components)
  {
    Assert.expect(_components != null);
    
    _components.removeAll(components);
    fireTableDataChanged();
  }
  
  /**
   * @author Andy Mechtenberg
   */
  int getRowForComponent(Component component)
  {
    return _components.lastIndexOf(component);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removeAllComponentsInBoard(Board board)
  {
    java.util.List<com.axi.v810.business.panelDesc.Component> components = new ArrayList<>();
    for (com.axi.v810.business.panelDesc.Component component : _components)
    {
      components.add(component);
    }
    for (com.axi.v810.business.panelDesc.Component component : components)
    {
      if (component.getBoard().getName().equals(board.getName()))
      {
        _components.remove(component);
      }
    }
    fireTableDataChanged();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean hasBoardComponents(String boardName)
  {
    for (com.axi.v810.business.panelDesc.Component component : _components)
    {
      if (component.getBoard().getName().equalsIgnoreCase(boardName))
      {
        return true;
      }
    }
    return false;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removeAllComponents()
  {
    _components.clear();
    fireTableDataChanged();
  }
}

