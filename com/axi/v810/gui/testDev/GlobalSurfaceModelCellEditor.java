package com.axi.v810.gui.testDev;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 *
 * @author lee-herng.cheah
 */
public class GlobalSurfaceModelCellEditor extends DefaultCellEditor
{
    private ModifySubtypesComponentTableModel _componentTableModel = null;
    private ModifySubtypesPadTypeTableModel _padTypeTableModel = null;

    /**
     * @author Cheah, Lee Herng
     */
    public GlobalSurfaceModelCellEditor(JComboBox comboBox,
                                        ModifySubtypesComponentTableModel componentTableModel)
    {
        super(comboBox);
        Assert.expect(componentTableModel != null);
        _componentTableModel = componentTableModel;
    }

    /**
     * @author Cheah, Lee Herng
     */
    public GlobalSurfaceModelCellEditor(JComboBox comboBox, ModifySubtypesPadTypeTableModel padTypeTableModel)
    {
        super(comboBox);
        Assert.expect(padTypeTableModel != null);
        _padTypeTableModel = padTypeTableModel;
    }

    /**
     * @author Cheah, Lee Herng
     */
    public java.awt.Component getTableCellEditorComponent(JTable table,
                                                        Object value,
                                                        boolean isSelectable,
                                                        int row,
                                                        int column)
    {
        // get the editor (should be a JComboBox as was specified in the constructor)
        // then modify the contents of the combo box to list only the valid choices for this row in the table
        int selectedRows[] = table.getSelectedRows();

        java.awt.Component editor = super.getTableCellEditorComponent(table, value, isSelectable, row, column);

        ComponentType componentType = _componentTableModel.getComponentTypeAt(row);
        
        JComboBox comboBox = (JComboBox)editor;
        comboBox.removeAllItems();
        
        comboBox.addItem(GlobalSurfaceModelEnum.AUTOFOCUS);
        comboBox.addItem(GlobalSurfaceModelEnum.GLOBAL_SURFACE_MODEL);
        // RJG: Disable AUTOSELECT until it is implemented
        //comboBox.addItem(GlobalSurfaceModelEnum.AUTOSELECT);
        // Wei Chin (RFB)
        comboBox.addItem(GlobalSurfaceModelEnum.USE_RFB);
        
//        if (componentType.isSetToUsePspResult())
//        {
//            comboBox.addItem(GlobalSurfaceModelEnum.PSP);
//            comboBox.setMaximumRowCount(5);
//        }
//        else
            comboBox.setMaximumRowCount(4);

        comboBox.setSelectedItem(componentType.getPadTypeOne().getPadTypeSettings().getGlobalSurfaceModel());
        
        return comboBox;
    }
}
