/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.testDev;

import java.awt.Color;
import java.awt.Component;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author chin-seong.kee
 */
public class ComponentLocationTableCellRenderer extends DefaultTableCellRenderer
{

  private final int _maximumOfToolTipsText = 10;

  public ComponentLocationTableCellRenderer()
  {
    // do nothing
  }

  /**
   * param table
   * @param value
   * @param isSelected
   * @param hasFocus
   * @param row
   * @param column
   * @return
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    if (value == null)
    {
      return null;
    }

    final JLabel label = new JLabel();
    Color alternativeColor = (Color) UIManager.get ("Table.alternateRowColor");
    Color backgroundColor =Color.WHITE;

    ComponentType selectedComponentType = null;
    PadType selectedPadType = null;

    ComponentLocationTable compLocationTable = null;
    ModifySubtypesComponentTable modifySubtypeCompTable = null;
    ModifySubtypesPadTypeTable modifySubtypePadTable = null;

    //build the format object according to the number of decimal points specified for display
    //DecimalFormat decFormat = buildDecimalFormat();

    //format the value if it contains decimal information
    String strValue = (String) value;


    if (table instanceof ComponentLocationTable)
    {
      compLocationTable = (ComponentLocationTable) table;
      selectedComponentType = compLocationTable.getTableModel().getComponentTypeAt(row);
    }
    else if (table instanceof ModifySubtypesComponentTable)
    {
      modifySubtypeCompTable = (ModifySubtypesComponentTable) table;
      selectedComponentType = modifySubtypeCompTable.getTableModel().getComponentTypeAt(row);
    }
    else if (table instanceof ModifySubtypesPadTypeTable)
    {
      modifySubtypePadTable = (ModifySubtypesPadTypeTable) table;
      selectedPadType = modifySubtypePadTable.getTableModel().getPadTypeAt(row);
    }

    label.setText(strValue);
    label.setToolTipText(SetToolTip(selectedComponentType, selectedPadType, strValue));
    label.setBorder(new EmptyBorder(1, 1, 1, 1));
    label.setOpaque(true);
    label.setFont(table.getFont());
    //label.setHorizontalAlignment(_horizontalAlign);

    if (isSelected)
    {
      label.setBackground(table.getSelectionBackground());
      label.setForeground(table.getSelectionForeground());
    }
    else
    {
      label.setForeground(table.getForeground());
      if(row%2 == 0)
        label.setBackground(backgroundColor);
      else
        label.setBackground(alternativeColor);
    }
    return label;
  }

  /**
   * @param selectedComponentType
   * @param selectedPadType
   * @param strValue
   * @return
   */
  private String SetToolTip(ComponentType selectedComponentType, PadType selectedPadType, String strValue)
  {
    String strToolTips = "";
    int ToolTipsCounter = 0;

    if (selectedComponentType != null)
    {
      if (selectedComponentType.isTestable() == false)
      {
        strToolTips = strToolTips + "<html><b>Reason</b><br>";
        for (PadType padtype : selectedComponentType.getPadTypes())
        {
          //strToolTips = strToolTips + ToolTipsCounter + " - " + padtype.getPadTypeSettings().getUntestableReason() + "<br>";
          strToolTips = strToolTips + ToolTipsCounter + " - " + padtype.getPadTypeSettings().getFirstUntestableReason() + "<br>";
          ToolTipsCounter++;
          if (ToolTipsCounter >= _maximumOfToolTipsText)
          {
            strToolTips = strToolTips + "...<br>";
            break;
          }
        }
        strToolTips = strToolTips + "</html>";
        ToolTipsCounter = 0;
      }
      else
      {
        if (strValue.matches(StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY")))
        {
          strToolTips = strToolTips + "<html><b>" + StringLocalizer.keyToString("GISWK_SUBTYPE_TYPE_KEY") + "</b><br>";
          for (Subtype subtype : selectedComponentType.getSubtypes())
          {
            strToolTips = strToolTips + ToolTipsCounter + " - " + subtype.getShortName() + "<br>";
            ToolTipsCounter++;
            if (ToolTipsCounter >= _maximumOfToolTipsText)
            {
              strToolTips = strToolTips + "...<br>";
              break;
            }
          }
          strToolTips = strToolTips + "</html>";
        }
        else
        {
          strToolTips = strValue;
        }
        ToolTipsCounter = 0;
      }
    }
    else if (selectedPadType != null)
    {
      //if (selectedPadType.isTestable() == false)
      if (selectedPadType.getPadTypeSettings().isTestable() == false)
      {
        strToolTips = strToolTips + "<html><b>Reason</b><br>";
        //strToolTips = strToolTips + selectedPadType.getPadTypeSettings().getUntestableReason() + "<br>";
        strToolTips = strToolTips + selectedPadType.getPadTypeSettings().getFirstUntestableReason() + "<br>";
        strToolTips = strToolTips + "</html>";
      }
      else
      {
        if (strValue.matches(StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY")))
        {
          strToolTips = strToolTips + "<html><b>" + StringLocalizer.keyToString("GISWK_SUBTYPE_TYPE_KEY") + "</b><br>";
          strToolTips = strToolTips + selectedPadType.getSubtype().getShortName() + "<br>";
          strToolTips = strToolTips + "</html>";
        }
        else
        {
          strToolTips = strValue;
        }
        ToolTipsCounter = 0;
      }
    }

    return strToolTips;
  }
}
