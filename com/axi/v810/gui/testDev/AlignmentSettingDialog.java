package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;

/**
 *
 * @author wei-chin.chong
 */
public class AlignmentSettingDialog extends EscapeDialog
{
  private boolean _ok = false;
  
  private BorderLayout _mainBorderLayout = new BorderLayout();
  private JPanel _okCancelPanel = new JPanel();
  private JPanel _okCancelInnerPanel = new JPanel();
  private JPanel _selectPanel = new JPanel();
  private GridLayout _okCancelInnerGridLayout = new GridLayout();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private FlowLayout _okCanelFowLayout = new FlowLayout();
  
  private JLabel _zheightSelectionMethodLabel = new JLabel();
  private JLabel _topZheightLabel = new JLabel();
  private JLabel _bottomZheightLabel = new JLabel();
  private JLabel _integrationLevelLabel = new JLabel();
  private JLabel _userGainLabel = new JLabel();
  private JLabel _stageSpeedLabel = new JLabel();
  
  private JRadioButton _autoFocusMethodButton = new JRadioButton();
  private JRadioButton _useZheightMethodButton = new JRadioButton();
  private ButtonGroup _buttonGroup = new ButtonGroup();
  private JPanel _customizeSettingPanel = new JPanel();
  private JCheckBox _useCustomizedSettingCheckBox = new JCheckBox();
  private JComboBox _integrationLevelComboBox = new JComboBox();
  private JComboBox _userGainComboBox = new JComboBox();
  private JComboBox _stageSpeedComboBox = new JComboBox();
  private PairLayout _selectPanelPairLayout = new PairLayout(2, 2, false);
  
  private NumericTextField _topZheight = new NumericTextField();
  private NumericTextField _bottomZheight = new NumericTextField();

  private VerticalFlowLayout _centerVerticalFowLayout = new VerticalFlowLayout();
  
  private java.awt.Frame _myFrame = null;
  
  private java.util.List<AlignmentGroup> _alignmentGroups;
  private com.axi.v810.business.panelDesc.Panel _panel;
  
  private int _maximumSplitLocation;
  private int _minimumSplitLocation;
  
  private JCheckBox _useCustomizeSplitSettingCheckBox = new JCheckBox();
  private NumericRangePlainDocument _panelSplitLocationDocument = new NumericRangePlainDocument();
  private NumericRangePlainDocument _topZheightDocument = new NumericRangePlainDocument();
  private NumericRangePlainDocument _bottomZheightDocument = new NumericRangePlainDocument();
  private NumericTextField _panelSplitLocationTextField = new NumericTextField();
  
  /**
   * @author Wei Chin
   */
  public AlignmentSettingDialog(Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);

    _myFrame = frame;
    jbInit();

    pack();
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit()
  {
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    getContentPane().setLayout(_mainBorderLayout);
    _okCancelInnerPanel.setLayout(_okCancelInnerGridLayout);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });

    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });

    _okCancelPanel.setLayout(_okCanelFowLayout);
    _okCancelPanel.add(_okCancelInnerPanel);
    _okCanelFowLayout.setAlignment(FlowLayout.RIGHT);
    _okCancelInnerGridLayout.setHgap(20);
    _okCancelInnerPanel.add(_okButton);
    _okCancelInnerPanel.add(_cancelButton);
    
    _zheightSelectionMethodLabel.setText(StringLocalizer.keyToString("MMGUI_ZHEIGHT_SELECTION_METHOD_KEY"));
    
    for(SignalCompensationEnum signalCompensation: SignalCompensationEnum.getOrderedSignalCompensations())
    {
      _integrationLevelComboBox.addItem(signalCompensation);
    }
    
    for(UserGainEnum userGain: UserGainEnum.getOrderedUserGain())
    {
      _userGainComboBox.addItem(userGain);
    }
     
    for(StageSpeedEnum stageSpeed: StageSpeedEnum.getOrderedStageSpeed())
    {
      _stageSpeedComboBox.addItem(stageSpeed);
    }
    
    JPanel topPanel = new JPanel();
    topPanel.add(_zheightSelectionMethodLabel);
    add(topPanel, BorderLayout.NORTH);
    
    _topZheightLabel.setText("  " + StringLocalizer.keyToString("MMGUI_TOP_ZHEIGHT_IN_MILS_KEY"));
    _bottomZheightLabel.setText("  " + StringLocalizer.keyToString("MMGUI_BOTTOM_ZHEIGHT_IN_MILS_KEY"));
    _bottomZheight.setColumns(8);
    _topZheight.setColumns(8);
    
    _selectPanel.setLayout(_selectPanelPairLayout);
    _selectPanel.setBorder(BorderFactory.createEmptyBorder(10,10,20,10));
    _selectPanel.add(_topZheightLabel);
    _selectPanel.add(_topZheight);
    _selectPanel.add(_bottomZheightLabel);
    _selectPanel.add(_bottomZheight);
    
    _autoFocusMethodButton.setText(StringLocalizer.keyToString("MMGUI_AUTOFOCUS_METHOD_KEY"));
    _autoFocusMethodButton.addItemListener(new java.awt.event.ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if(e.getStateChange() == ItemEvent.SELECTED)
        {
          _topZheight.setEditable(false);
          _bottomZheight.setEditable(false);
          _topZheight.setEnabled(false);
          _bottomZheight.setEnabled(false);
        }
        //Khaw Chek Hau - XCR2060: Top Z-height is not working at Alignment Settings
//        else
//        {
//          _topZheight.setEditable(true);
//          _bottomZheight.setEditable(true);
//          _topZheight.setEnabled(true);
//          _bottomZheight.setEnabled(true);       
//        }
      }
    });
    _useZheightMethodButton.setText(StringLocalizer.keyToString("MMGUI_ZHEIGHT_METHOD_KEY"));
    _useZheightMethodButton.addItemListener(new java.awt.event.ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if(e.getStateChange() == ItemEvent.SELECTED)
        {
          //Khaw Chek Hau - XCR2060 - Only enable the top/bottom active text field
          //Kee Chin Seong - XCR-2060/XCR2471 
          boolean isTopSideAvailable = false, isBottomSideAvailable = false;
          for (AlignmentGroup alignmentGroup : _alignmentGroups) 
          {
            if (alignmentGroup.getPads().iterator().next().isTopSide()) 
            {
              isTopSideAvailable = true;
            }         
            else 
            {
              isBottomSideAvailable = true;  
            }
            
            if(isTopSideAvailable == true)
            {
              _topZheight.setEnabled(true);
              _topZheight.setEditable(true);
            }
            if(isBottomSideAvailable == true)
            {
              _bottomZheight.setEnabled(true);
              _bottomZheight.setEditable(true);
            }
          }   
        }
        else
        {
          //Khaw Chek Hau - XCR2060: Top Z-height is not working at Alignment Settings
//          _topZheight.setEditable(true);
//          _bottomZheight.setEditable(true);
//          _topZheight.setEnabled(true);
//          _bottomZheight.setEnabled(true);   
        }
      }
    });
    _buttonGroup.add(_autoFocusMethodButton);
    _buttonGroup.add(_useZheightMethodButton);
    
    if (_useCustomizeSplitSettingCheckBox.isSelected())
    {
      _panelSplitLocationTextField.setEnabled(true);
    }
    else
    {
      _panelSplitLocationTextField.setEnabled(false);
    }
    
    _customizeSettingPanel.setLayout(_selectPanelPairLayout);
    _customizeSettingPanel.setBorder(BorderFactory.createEmptyBorder(10,10,20,10));    
    _customizeSettingPanel.add(_integrationLevelLabel);
    _customizeSettingPanel.add(_integrationLevelComboBox);
    _customizeSettingPanel.add(_userGainLabel);
    _customizeSettingPanel.add(_userGainComboBox);
    _customizeSettingPanel.add(_stageSpeedLabel);
    _customizeSettingPanel.add(_stageSpeedComboBox);
    if (_useCustomizedSettingCheckBox.isSelected())
    {
      _integrationLevelComboBox.setEnabled(true);
      _userGainComboBox.setEnabled(true);
      _stageSpeedComboBox.setEnabled(true);
    }
    else
    {
      _integrationLevelComboBox.setEnabled(false);
      _userGainComboBox.setEnabled(false);
      _stageSpeedComboBox.setEnabled(false);
    }
    
    _useCustomizedSettingCheckBox.setText(StringLocalizer.keyToString("MMGUI_CUSTOMIZE_SETTING_KEY"));
    _useCustomizedSettingCheckBox.addItemListener(new java.awt.event.ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if(e.getStateChange() == ItemEvent.SELECTED)
        {
          _integrationLevelComboBox.setEnabled(true);
          _userGainComboBox.setEnabled(true);
          _stageSpeedComboBox.setEnabled(true);
        }
        else
        {
          _integrationLevelComboBox.setEnabled(false);
          _userGainComboBox.setEnabled(false);
          _stageSpeedComboBox.setEnabled(false);
        }
      }
    });
    
    _integrationLevelLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _integrationLevelLabel.setText("  " + StringLocalizer.keyToString("MMGUI_ALIGN_INTEGRATION_LEVEL_KEY"));    
    
    _userGainLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _userGainLabel.setText("  " + StringLocalizer.keyToString("MMGUI_ALIGN_USER_GAIN_KEY"));       
    
    _stageSpeedLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _stageSpeedLabel.setText("  " + StringLocalizer.keyToString("MMGUI_ALIGN_STAGE_SPEED_KEY"));       
    
    _useCustomizeSplitSettingCheckBox.setText(StringLocalizer.keyToString("MMGUI_CUSTOMIZE_SPLIT_METHOD_KEY"));
    _useCustomizeSplitSettingCheckBox.addItemListener(new java.awt.event.ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if(e.getStateChange() == ItemEvent.SELECTED)
        {
          _panelSplitLocationTextField.setEnabled(true);       
        }
        else
        {
          _panelSplitLocationTextField.setEnabled(false);        
        }
      }
    });
    _panelSplitLocationTextField.setColumns(10);
    
    _panelSplitLocationDocument.setRange(new DoubleRange(Integer.MIN_VALUE/MathUtil.NANOMETERS_PER_MIL , Integer.MAX_VALUE/MathUtil.NANOMETERS_PER_MIL));
    _topZheightDocument.setRange(new DoubleRange(Integer.MIN_VALUE/MathUtil.NANOMETERS_PER_MIL , Integer.MAX_VALUE/MathUtil.NANOMETERS_PER_MIL));
    _bottomZheightDocument.setRange(new DoubleRange(Integer.MIN_VALUE/MathUtil.NANOMETERS_PER_MIL , Integer.MAX_VALUE/MathUtil.NANOMETERS_PER_MIL));
    _panelSplitLocationTextField.setDocument(_panelSplitLocationDocument);
    _topZheight.setDocument(_topZheightDocument);
    _bottomZheight.setDocument(_bottomZheightDocument);
    
    JPanel centerPanel = new JPanel();
    centerPanel.setLayout(_centerVerticalFowLayout);
    centerPanel.add(_autoFocusMethodButton);
    centerPanel.add(_useZheightMethodButton);
    centerPanel.add(_selectPanel);
    centerPanel.add(_useCustomizedSettingCheckBox);
    centerPanel.add(_customizeSettingPanel);
    centerPanel.add(_useCustomizeSplitSettingCheckBox);
    centerPanel.add(_panelSplitLocationTextField);
    
    add(centerPanel, BorderLayout.CENTER);
    add(_okCancelPanel, BorderLayout.SOUTH);
  }

  /**
   * @author Laura Cormos
   */
  boolean userClickedOk()
  {
    return _ok;
  }

  /**
   * @author Laura Cormos
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    _ok = false;
    updateSplitSetting();
    dispose();
  }

  /**
   * @author Laura Cormos
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    if(_useZheightMethodButton.isSelected())
    {
      for (AlignmentGroup alignmentGroup : _alignmentGroups)
      {
        if (alignmentGroup.getPads().isEmpty())
        {
          JOptionPane.showMessageDialog(this,StringLocalizer.keyToString("MMGUI_EMPTY_ALIGNMENT_PADS_WARNING_KEY"),
            StringLocalizer.keyToString("MMGUI_ALIGNMENT_SETTING_KEY"), JOptionPane.WARNING_MESSAGE);
          return;
        }
      }
    }
    
    if (_panelSplitLocationTextField.isEnabled())
    {
      double mils = 0;
      try
      {
        mils = _panelSplitLocationTextField.getDoubleValue();
      }
      catch (ParseException pe)
      {
        // do nothing
      }
      int nanometer = MathUtil.convertMilsToNanoMetersInteger(mils);
      if(nanometer < _minimumSplitLocation || 
         nanometer > _maximumSplitLocation )
      {
        double maximumSplitLocationInMils = MathUtil.convertNanoMetersToMils(_maximumSplitLocation);
        double minimumSplitLocationInMils = MathUtil.convertNanoMetersToMils(_minimumSplitLocation);
        JOptionPane.showMessageDialog(this, 
          StringLocalizer.keyToString(new LocalizedString("MMGUI_SPLIT_LOCATION_LIMIT_WARNING_KEY", new Object[]
          {
            MathUtil.roundDoubleToInt(minimumSplitLocationInMils), MathUtil.roundDoubleToInt(maximumSplitLocationInMils)
          })),
          StringLocalizer.keyToString("MMGUI_ALIGNMENT_SETTING_KEY"), JOptionPane.WARNING_MESSAGE);
        return;
      }
      _panel.getPanelSettings().setSplitLocationInNanometer(nanometer);
    }
    else
    {
      _panel.getPanelSettings().disableCustomSplit();
    }
    
    
    for(AlignmentGroup alignmentGroup : _alignmentGroups)
    {
      alignmentGroup.setUseZHeight(_useZheightMethodButton.isSelected());
      if(_useZheightMethodButton.isSelected())
      {
        double zHeightInMils = 0;
        if(alignmentGroup.getPads().get(0).isTopSide())
        {
          try
          {
            zHeightInMils = _topZheight.getDoubleValue();
          }
          catch (ParseException ex)
          {
            zHeightInMils = 0;
          }
        }
        else
        {
          try
          {
            zHeightInMils = _bottomZheight.getDoubleValue();
          }
          catch (ParseException ex)
          {
            zHeightInMils = 0;
          }
        }
        int zHeightInNanometer = MathUtil.convertMilsToNanoMetersInteger(zHeightInMils);
        alignmentGroup.setZHeightInNanometer(zHeightInNanometer);
      }
      
      // XCR-2730 Auto-alignment IL/Gain setting should follow Customize Alignment settings
      if (_useCustomizedSettingCheckBox.isSelected())
      {
        alignmentGroup.setUseCustomizeAlignmentSetting(true);
        if (_integrationLevelComboBox.getItemCount() > 0)
          alignmentGroup.setSignalCompensationEnum((SignalCompensationEnum)_integrationLevelComboBox.getSelectedItem());

        if (_userGainComboBox.getItemCount() > 0)
          alignmentGroup.setUserGainEnum((UserGainEnum)_userGainComboBox.getSelectedItem());
        
        if (_stageSpeedComboBox.getItemCount() > 0)
          alignmentGroup.setStageSpeedEnum((StageSpeedEnum)_stageSpeedComboBox.getSelectedItem());
      }
      else
      {
        alignmentGroup.setUseCustomizeAlignmentSetting(false);
      }
    }
    
    _ok = true;
    dispose();
  }


  /**
   * @author Laura Cormos
   */
  public void clear()
  {
  }
  
  /**
   * @return boolean
   * @author Chong Wei Chin
   */
  public boolean showDialog()
  {
    SwingUtils.centerOnComponent(this, _myFrame);
    setVisible(true);
    return _ok;
  }
  
  /**
   * 
   * @param alignmentGroups 
   * @author Wei Chin
   */
  public void populateData(com.axi.v810.business.panelDesc.Panel panel)
  {
    Assert.expect(panel != null);
    
    if(_alignmentGroups != null)
      _alignmentGroups.clear();
    
    _alignmentGroups = null;
    
//    _alignmentGroups = alignmentGroups;
    _panel = panel;
    boolean useZheight = false;
    double zHeightInNanometer = 0;
    boolean useCustomizeAlignmentSetting = false;
    UserGainEnum userGainEnum = UserGainEnum.ONE;
    StageSpeedEnum stageSpeedEnum = StageSpeedEnum.ONE;
    SignalCompensationEnum signalCompensationEnum = SignalCompensationEnum.DEFAULT_LOW;
    if(_panel.getPanelSettings().isPanelBasedAlignment())
    {
      _alignmentGroups = _panel.getPanelSettings().getAllAlignmentGroups();
    }
    else
    {
//      _alignmentGroups = new java.util.ArrayList();
      for(Board board : _panel.getBoards())
      {
        if(_alignmentGroups == null)
          _alignmentGroups = board.getBoardSettings().getAllAlignmentGroups();
        else
          _alignmentGroups.addAll(board.getBoardSettings().getAllAlignmentGroups());
      }
    }
    
    //Khaw Chek Hau - Disable top and bottom zheight text field first before check the avaibility
    _topZheight.setEditable(false);
    _bottomZheight.setEditable(false);
    _topZheight.setEnabled(false);
    _bottomZheight.setEnabled(false);  
      
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    if (_panel.getPanelSettings().isUsing2DAlignmentMethod())
    {
      _useZheightMethodButton.setEnabled(false);
      _topZheight.setValue(0);
      _bottomZheight.setValue(0);
    }
    else
      _useZheightMethodButton.setEnabled(true);

    for(AlignmentGroup alignmentGroup : _alignmentGroups)
    {
      if(alignmentGroup.useZHeight())
      {
        useZheight = true;
        double zHeightInMils = MathUtil.convertNanoMetersToMils(alignmentGroup.getZHeightInNanometer());

        //Khaw Chek Hau - XCR2060 - comment out due to it overides Zheight value     
//      _topZheight.setValue(zHeightInNanometer);
//      _bottomZheight.setValue(zHeightInNanometer);

        if( alignmentGroup.getPads().iterator().next().isTopSide() )
        {
          //Khaw Chek Hau - XCR2060: Top Z-height is not working at Alignment Settings
          _topZheight.setEnabled(true);
          _topZheight.setEditable(true);
          _topZheight.setValue(zHeightInMils);
        }
        else
        {
          _bottomZheight.setEnabled(true);
          _bottomZheight.setEditable(true);
          _bottomZheight.setValue(zHeightInMils); 
        }
//        break;
      }
      
      if(alignmentGroup.useCustomizeAlignmentSetting())
      {
        useCustomizeAlignmentSetting = true;
        userGainEnum = alignmentGroup.getUserGainEnum();
        stageSpeedEnum = alignmentGroup.getStageSpeedEnum();
        signalCompensationEnum = alignmentGroup.getSignalCompensationEnum();
      }
      else
      {
        useCustomizeAlignmentSetting = false;
        userGainEnum = UserGainEnum.ONE;
        stageSpeedEnum = StageSpeedEnum.ONE;
        signalCompensationEnum = SignalCompensationEnum.DEFAULT_LOW;
      }
    }
    
    if(_panel.getPanelSettings().isLongPanel() && 
       _panel.getPanelSettings().isPanelBasedAlignment())
    {
      _useCustomizeSplitSettingCheckBox.setEnabled(true);
      
      _maximumSplitLocation = XrayTester.getMaxImageableAreaLengthInNanoMeters() - ReconstructionRegion.getMaxVerificationRegionLengthInNanoMeters();
      _minimumSplitLocation = _panel.getPanelSettings().minimumLongPanelAlignmentRegion();      
      updateSplitSetting();
    }
    else
    {
      _useCustomizeSplitSettingCheckBox.setEnabled(false);
      _panelSplitLocationTextField.setEnabled(false);
    }
   
    if (useCustomizeAlignmentSetting)
    {
      if(_useCustomizedSettingCheckBox.isSelected() == false)
      {
        _useCustomizedSettingCheckBox.setSelected(true);
        _userGainComboBox.setSelectedItem(userGainEnum);
        _integrationLevelComboBox.setSelectedItem(signalCompensationEnum);
        _stageSpeedComboBox.setSelectedItem(stageSpeedEnum);
      }
    }
    else
    {
      if(_useCustomizedSettingCheckBox.isSelected())
      {
        _useCustomizedSettingCheckBox.setSelected(false);
        _userGainComboBox.setSelectedItem(userGainEnum);
        _integrationLevelComboBox.setSelectedItem(signalCompensationEnum);
        _stageSpeedComboBox.setSelectedItem(stageSpeedEnum);
      }
    }
    
    if(useZheight)
    {
      if(_useZheightMethodButton.isSelected() == false)
      {
        _useZheightMethodButton.setSelected(true);
        _useZheightMethodButton.doClick();
      }
    }
    else
    {
      if(_autoFocusMethodButton.isSelected() == false)
      {
        _autoFocusMethodButton.setSelected(true);
        _autoFocusMethodButton.doClick();
      }
    }
  }
  
  /**
   * @author Wei Chin
   */
  private void updateSplitSetting()
  {
    if (_panel.getPanelSettings().hasCustomSplit())
    {
      if (_useCustomizeSplitSettingCheckBox.isSelected() == false)
      {
        _useCustomizeSplitSettingCheckBox.doClick();
      }
      double splitValueInDouble = MathUtil.convertNanoMetersToMils(_panel.getPanelSettings().getSplitLocationInNanometer());
      _panelSplitLocationTextField.setValue(splitValueInDouble);
    }
    else
    {
      if (_useCustomizeSplitSettingCheckBox.isSelected())
      {
        _useCustomizeSplitSettingCheckBox.doClick();
      }
    }
  }
}
