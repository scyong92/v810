package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.algoTuner.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This is the Home page panel for the Test Development Tool
 * This panel is mostly non-interactive.  It shows the user summary information about their
 * project, allows them to view the un-tuned subtypes, and enter free-form descriptive text
 * about their project.
 * @author Andy Mechtenberg
 */
public class InitialTuningPanel extends AbstractTaskPanel implements Observer
{
  private StatisticalTuningEngine _statisticalTuningEngine;
  private int _currentSubtypeNumber;
  private int _maxSubtypeNumber;
  private boolean _abortingDelete = false;
  private static final int _RUNTIME_TEXT_LIMIT = 4000000;  // 4,000,000 chars
  private static final int _RUNTIME_TEXT_REMOVAL_AMOUNT = 50000;  // 50,000 chars to be removed at a chunk

  private InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
  private boolean _learningInProgress = false;
  private boolean _updatingShortsData = false;
  private MainMenuGui _mainUI = null;
  private TestDev _testDev = null;
  private TestDevPersistance _guiPersistSettings;
  private ProjectPersistance _projectPersistSettings;
  private Project _project = null;
  private boolean _updateData = false;
  private boolean _populateProjectData = true;
  private PanelDataUtil _panelDataUtil = null;
  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
  private java.util.List<Subtype> _subtypesToRefine = new ArrayList<Subtype>(); // list of Subtypes that are not refined
  private java.util.List<Subtype> _unlearnedShorts = new ArrayList<Subtype>(); // list of Subtypes that have no short learned data
  private java.util.List<Subtype> _unlearnedExpectedImages = new ArrayList<Subtype>(); // list of Subtypes that have no expected image learned data
  private java.util.List<Subtype> _unlearnedSubtypes = new ArrayList<Subtype>(); // list of Subtypes that have no subtype learned data
  private java.util.List<Subtype> _unImportedSubtypes = new ArrayList<Subtype>(); // list of Subtypes that have not imported from any library.
  private JointTypeSubtypeTableModel _refineSubtypesTableModel;
  private ProgressDialog _progressCancelDialog;
  private int _deletedSubtypes = 0;
  private int _totalSubtypesToDelete = 0;
  private int _beforeDeleteShortLearnedSubtypes = 0;
  private int _beforeDeleteSubtypeLearnedSubtypes = 0;
  private int _beforeDeleteExpectedImageLearnedSubtypes = 0;
  private int _beforeDeleteImageTemplateLearnedSubtypes = 0;

  private InteractiveLearningDialog _interactiveLearningDialog;
  private boolean _interactiveLearningDialogVisible = false;
  private boolean _collectingPrecisionImageSet = false;

  private BorderLayout _mainBorderLayout = new BorderLayout();
  private JPanel _subtypeRefinementPanel = new JPanel();
  private JPanel _statusPanel = new JPanel();
  private JPanel _learnAndShortRefinementPanel = new JPanel();
  private BorderLayout _learnAndShortRefinementBorderLayout = new BorderLayout();
  private JPanel _shortRefinementOuterPanel = new JPanel();
  private JPanel _subtypeLearningPanel = new JPanel();
  private BorderLayout _subtypeLearningBorderLayout = new BorderLayout();
  private JPanel _jointTypeSubtypePanel = new JPanel();
  private BorderLayout _jointTypeBorderLayout = new BorderLayout();
  private BorderLayout _subtypeBorderLayout = new BorderLayout();
  private BorderLayout _boardSelectedBorderLayout = new BorderLayout();
  private PairLayout _jointTypeSubtypePairLayout = new PairLayout();
  private BorderLayout _shortRefinementOuterBorderLayout = new BorderLayout();
  private JLabel _shortRefinementHeaderLabel = new JLabel();
  private JPanel _shortRefinementInnerPanel = new JPanel();
  private JLabel _refineShortLabel = new JLabel();
  private JButton _refineShortDataButton = new JButton();
  private FlowLayout _shortRefinementInnerFlowLayout = new FlowLayout();
  private BorderLayout _subtypeRefinementBorderLayout = new BorderLayout();
  private JLabel _subtypeRefinementLabel = new JLabel();
  private JPanel _refineSubtypesTablePanel = new JPanel();
  private JScrollPane _refineSubtypesScrollPane = new JScrollPane();
  private JSortTable _refineSubtypesTable = new JSortTable();
  private JPanel _refineSubtypesButtonPanel = new JPanel();
  private FlowLayout _refineSubtypesButtonflowLayout = new FlowLayout();
  private JButton _refineSubtypeButton = new JButton();
  private JPanel _subypteLearningStatusOuterPanel = new JPanel();
  private JPanel _statusLearningPanel = new JPanel();
  private JLabel _shortLearningLabel = new JLabel();
  private JLabel _shortsLearningStatusLabel = new JLabel();
  private JLabel _expectedImageLearningLabel = new JLabel();
  private JLabel _expectedImageLearningStatusLabel = new JLabel();
  private JLabel _importedSubtypeStatusLabel = new JLabel();
  private JLabel _importedSubtypeLabel = new JLabel();
  private JLabel _subtypeWithExposureLearningStatusLabel = new JLabel();
  private JLabel _subtypeWithExposureLearningLabel = new JLabel();
  private GridLayout _subtypeLearningStatusOuterBorderLayout = new GridLayout();
  private JLabel _subtypeLearningLabel = new JLabel();
  private JPanel _subtypeLearningDetailsPanel = new JPanel();
  private JLabel _subtypeLearningStatusLabel = new JLabel();
  private JButton _subtypeLearningDetailsButton = new JButton();
  private FlowLayout _subtypeLearningDetailsFlowLayout = new FlowLayout();

  private Border _subtypeEtchedBorder = BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166));
  private Border _subtypeTitledBorder = new TitledBorder(_subtypeEtchedBorder, StringLocalizer.keyToString("MMGUI_INITIAL_LEARNING_BORDER_KEY"));
  private Border _subtypeBorder = BorderFactory.createCompoundBorder(_subtypeTitledBorder, BorderFactory.createEmptyBorder(5, 5, 5, 5));
  private Border _shortRefinementBorder = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,
      new Color(165, 163, 151)), BorderFactory.createEmptyBorder(5, 0, 5, 0));
  private Border _statusEtchedBorder = BorderFactory.createEtchedBorder(Color.white, new Color(165, 163, 151));
  private Border _statusBorder = new TitledBorder(_statusEtchedBorder, StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_STATUS_BORDER_KEY"));
  private Border _mainBorder;
  private Border _refineSubtypesBorder = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,
      new Color(165, 163, 151)), BorderFactory.createEmptyBorder(10, 5, 10, 5));
  private JPanel _learningActionPanel = new JPanel();
  private JLabel _jointTypeLabel = new JLabel();
  private JComboBox _jointTypeComboBox = new JComboBox();
  private JLabel _subtypeLabel = new JLabel();
  private VariableWidthComboBox _subtypeComboBox = new VariableWidthComboBox();
  private JLabel _boardSelectedLabel = new JLabel();
  private JComboBox _boardSelectComboBox = new JComboBox();
  private JPanel _refinementPanel = new JPanel();
  private BorderLayout _refinementBorderLayout = new BorderLayout();
  private BorderLayout _learningActionBorderLayout = new BorderLayout();
  private JCheckBox _subtypeLearningCheckBox = new JCheckBox();
  private JCheckBox _shortLearningCheckBox = new JCheckBox();
  private boolean _expectedImageLearningCheckBoxAllowed = true;
  private JCheckBox _expectedImageLearningCheckBox = new JCheckBox();
  private JCheckBox _excludeLibrarySubtypeLearningCheckBox = new JCheckBox();
  private JCheckBox _learnedBoardApplyToAllBoardCheckBox = new JCheckBox();
  private JCheckBox _templateLearningCheckBox = new JCheckBox();//ShengChuan - Clear Tombstone
  private JPanel _learnButtonOuterPanel = new JPanel();
  private JButton _learnButton = new JButton();
  private JButton _reLearnShortButton = new JButton();
  private JButton _interactiveLearningButton = new JButton();
  private JButton _subtypeMaskingButton = new JButton();
  private JButton _resetAlgorithmSettingsButton = new JButton();
  private JButton _initialAlgorithmSettingsButton = new JButton(); // Jenping - Reset to initial threshold feature
  private JPanel _shortLearningDetailsPanel = new JPanel();
  private JButton _subtypeExposureLearningButton = new JButton();
  private FlowLayout _shortsLearningDetailsFlowLayout = new FlowLayout();
  private JButton _shortLearningDetailsButton = new JButton();
  private JPanel _expectedImageLearningDetailsPanel = new JPanel();
  private FlowLayout _expectedImageLearningDetailsFlowLayout = new FlowLayout();
  private JButton _expectedImageLearningDetailsButton = new JButton();
  private JPanel _subtypeWithExposureLearningDetailsPanel = new JPanel();
  private FlowLayout _subtypeWithExposureLearningDetailsFlowLayout = new FlowLayout();
  private JButton _subtypeWithExposureLearningDetailsButton = new JButton();
  private JButton _refineAllButton = new JButton();
  private JPanel _refineSubtypesInnerButtonPanel = new JPanel();
  private GridLayout _refineSubtypesInnerButtonGridLayout = new GridLayout();
  private JPanel _statusHeaderLabelPanel = new JPanel();
  private GridLayout _statusHeaderLabelGridLayout = new GridLayout();
  private BorderLayout _statusBorderLayout = new BorderLayout();
  private GridLayout _statusLearningGridLayout = new GridLayout();
  private JPanel _learningChoiceInnerPanel = new JPanel();
  private GridLayout _learningChoiceInnerGridLayout = new GridLayout();
  private JPanel _learningChoiceOuterPanel = new JPanel();
  private FlowLayout _learningChoiceOuterFlowLayout = new FlowLayout();
  private JPanel _learnButtonInnerPanel = new JPanel();
  private GridLayout _learnButtonInnerPanelGridLayout = new GridLayout();
//  private JPanel _learningSubtypeInnerPanel = new JPanel();
//  private FlowLayout _learningSubtypeInnerFlowLayout = new FlowLayout();
  private JButton _deleteLearnedDataButton = new JButton();
  private BorderLayout _refineSubtypesTableBorderLayout = new BorderLayout();
  private JPanel _subtypeImportedStatusDetailsPanel = new JPanel();
  private FlowLayout _subtypeImportedStatusDetailsFlowLayout = new FlowLayout();
  private JButton _subtypeImportedStatusDetailsButton = new JButton();

  // action listeners
  private ActionListener _jointTypeComboBoxActionListener;
  private ActionListener _subtypeComboBoxActionListener;//ShengChuan - Clear Tombstone
  private JPanel _learningExecutionPanel = new JPanel();
  private BorderLayout _learningExecutionBorderLayout = new BorderLayout();
  private JScrollPane _learningMessagesScrollPane = new JScrollPane();
  private JTextArea _learningMessagesTextArea = new JTextArea();
  private JPanel _currentInfoPanel = new JPanel();
  private JPanel _currentJointTypePanel = new JPanel();
  private JPanel _currentSubtypePanel = new JPanel();
  private GridLayout _currentInfoGridLayout = new GridLayout();
  private JLabel _currentJointTypeLabel = new JLabel();
  private JLabel _jointTypeInfoLabel = new JLabel();
  private FlowLayout _currentJointTypeFlowLayout = new FlowLayout();
  private FlowLayout _currentSubtypeFlowLayout = new FlowLayout();
  private JLabel _subtypeInfoLabel = new JLabel();
  private JLabel _currentSubtypeLabel = new JLabel();
  private JPanel _progressCancelPanel = new JPanel();
  private BorderLayout _cancelProgressBorderLayout = new BorderLayout();
  private JProgressBar _progressBar = new JProgressBar();
  private JButton _cancelButton = new JButton();
  private JPanel _cancelPanel = new JPanel();
  private Border _emptyBorder = BorderFactory.createEmptyBorder(10, 0, 0, 0);

  private java.util.List<ImageSetData> _selectedImageSets = new LinkedList<ImageSetData>();
  
  //Siew Yeng - XCR-2139
  private JCheckBox _slopeLearningCheckBox = new JCheckBox();
  private JPanel _slopeLearningPanel = new JPanel();

  private Map<Subtype, java.util.List<StageSpeedEnum>> _subtypeToExposureSetting = new HashMap<Subtype, java.util.List<StageSpeedEnum>>();
  /**
   * Useful ONLY for the JBuilder designer.  Not for use with normal code.
   * @author Andy Mechtenberg
   */
  private InitialTuningPanel()
  {
    Assert.expect(java.beans.Beans.isDesignTime());
    jbInit();
  }

  /**
   * @author Andy Mechtenberg
   */
  public InitialTuningPanel(TestDev testDev)
  {
    super(testDev);
    Assert.expect(testDev != null);
    _testDev = testDev;
    _mainUI = _testDev.getMainUI();
    _statisticalTuningEngine = StatisticalTuningEngine.getInstance();
    jbInit();
    // add this as an observer of the gui
//    GuiObservable.getInstance().addObserver(this);
  }

  /**
   * The InitialTuning panel is a listener to gui (for board type changes)
   * @author Andy Mechtenberg
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof GuiObservable)
          handleGuiEvent((GuiEvent)object);
        else if (observable instanceof ProjectObservable)
          handleProjectEvent((ProjectChangeEvent)object);
        else if (observable instanceof InspectionEventObservable)
          handleInspectionEvent((InspectionEvent)object);
        else
          Assert.expect(false);
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleGuiEvent(final GuiEvent guiEvent)
  {
    GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
//    if (guiEventEnum instanceof SelectionEventEnum)
//    {
//      SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
//      if (selectionEventEnum.equals(SelectionEventEnum.BOARD_TYPE))
//      {
//        updateDataOnScreen();
//      }
//    }
    if (guiEventEnum instanceof ImageSetEventEnum)
    {
      ImageSetEventEnum imageSetEventEnum = (ImageSetEventEnum)guiEventEnum;
      if (imageSetEventEnum.equals(ImageSetEventEnum.IMAGE_SET_NOT_SELECTED))
      {
        _selectedImageSets.clear();
        updateButtonState();
      }
      if (imageSetEventEnum.equals(ImageSetEventEnum.IMAGE_SET_SELECTED))
      {
        Assert.expect(guiEvent.getSource() instanceof java.util.List);
        _selectedImageSets = (java.util.List<ImageSetData>)guiEvent.getSource();
        updateButtonState();
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleProjectEvent(final ProjectChangeEvent projectChangeEvent)
  {
    if ((_active == false) || (_progressCancelDialog != null))
      _updateData = true;
    else
    {
      ProjectChangeEventEnum eventEnum = projectChangeEvent.getProjectChangeEventEnum();
      if (eventEnum instanceof SubtypeEventEnum)
      {
        SubtypeEventEnum subtypeEventEnum = (SubtypeEventEnum)eventEnum;
        if (subtypeEventEnum.equals(subtypeEventEnum.DELETE_LEARNED_DATA_FOR_ALL_ALGORITHMS_EXCEPT_SHORT) ||
            subtypeEventEnum.equals(subtypeEventEnum.DELETE_LEARNED_DATA_FOR_SHORT) ||
            subtypeEventEnum.equals(subtypeEventEnum.DELETE_LEARNED_DATA_FOR_EXPECTED_IMAGE))
        {
          updateLearningStatusFields();
        }
      }
      else if(eventEnum instanceof ProjectEventEnum)
      {
        ProjectEventEnum projectEventEnum = (ProjectEventEnum)eventEnum;
        if (projectEventEnum.equals(projectEventEnum.LIBRARY_IMPORT_COMPLETE) || projectEventEnum.equals(projectEventEnum.IMPORT_LAND_PATTERN_COMPLETE))
        {
          updateLearningStatusFields();
        }
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleInspectionEvent(final InspectionEvent inspectionEvent)
  {
    if (inspectionEvent instanceof InitialTuningStartedInspectionEvent)
    {
      InitialTuningStartedInspectionEvent event = (InitialTuningStartedInspectionEvent)inspectionEvent;
      _currentJointTypeLabel.setText("");
      _currentSubtypeLabel.setText("");
      _maxSubtypeNumber = event.getNumberOfSubtypesToLearn();
      _currentSubtypeNumber = 0;
      _progressBar.setMaximum(_maxSubtypeNumber);
      _progressBar.setValue(0);
      _progressBar.setString(_currentSubtypeNumber + " / " + _maxSubtypeNumber);
    }
    else if (inspectionEvent instanceof InitialTuningCompletedInspectionEvent)
    {
  //          _currentSubtypeNumber = _maxSubtypeNumber;
  //          _progressBar.setValue(_currentSubtypeNumber);
  //          _progressBar.setString(_currentSubtypeNumber + " / " + _maxSubtypeNumber);
    }
    else if (inspectionEvent instanceof SubtypeLearningStartedInspectionEvent)
    {
      SubtypeLearningStartedInspectionEvent event = (SubtypeLearningStartedInspectionEvent)inspectionEvent;
      Subtype currentSubtype = event.getSubtype();
      _currentJointTypeLabel.setText(currentSubtype.getJointTypeEnum().toString());
      _currentSubtypeLabel.setText(currentSubtype.toString());
    }
    else if (inspectionEvent instanceof SubtypeExposureLearningEvent)
    {
      SubtypeExposureLearningEvent event = (SubtypeExposureLearningEvent)inspectionEvent;
      Subtype currentSubtype = event.getSubtype();
      java.util.List<StageSpeedEnum> speedList = event.getLearntExposureSetting();
      _subtypeToExposureSetting.put(currentSubtype, speedList);
    }
    else if (inspectionEvent instanceof SubtypeLearningCompletedInspectionEvent)
    {
      SubtypeLearningCompletedInspectionEvent event = (SubtypeLearningCompletedInspectionEvent)inspectionEvent;

      _currentSubtypeNumber++;
      _progressBar.setValue(_currentSubtypeNumber);
      _progressBar.setString(_currentSubtypeNumber + " / " + _maxSubtypeNumber);
    }
    else if (inspectionEvent instanceof MessageInspectionEvent)
    {
      MessageInspectionEvent event = (MessageInspectionEvent)inspectionEvent;
      addLearningMessage(event.getMessage());
    }
    else if (inspectionEvent instanceof DeletingLearnedDataStartedInspectionEvent)
    {
      // create jprogress dialog and initialize
      DeletingLearnedDataStartedInspectionEvent startDeleteEvent = (DeletingLearnedDataStartedInspectionEvent)inspectionEvent;
      _totalSubtypesToDelete = startDeleteEvent.getNumberOfSubtypesToDeleteLearnedData();
      _deletedSubtypes = 0;
      _abortingDelete = false;
    }
    else if (inspectionEvent instanceof DeletingLearnedDataCompletedInspectionEvent)
    {
  //          _progressCancelDialog.dispose();
  //          updateStatusAfterLearning();
    }
    else if (inspectionEvent instanceof SubtypeDeletingLearnedDataStartedInspectionEvent)
    {
    }
    else if (inspectionEvent instanceof SubtypeDeletingLearnedDataCompletedInspectionEvent)
    {
      // ignore this event during a re-learn.  A re-learn will give us pairs of subtypes learned and delete learned data
      // we'll just track one, using the main progress bar, so we can ignore this one.
      if (_updatingShortsData)
        return;
      int percentComplete = (++_deletedSubtypes * 100) / _totalSubtypesToDelete;
      if (percentComplete == 100)
        percentComplete = 99;
      
      //Chin Seong - fix crash when progressCancelDialog is null
      if(_progressCancelDialog != null)
        _progressCancelDialog.updateProgressBarPrecent(percentComplete);
    }
    else if (inspectionEvent.getInspectionEventEnum().equals(InspectionEventEnum.BEGIN_COLLECT_PRECISION_IMAGE_SET))
    {
      _collectingPrecisionImageSet = true;
    }
    else if (inspectionEvent.getInspectionEventEnum().equals(InspectionEventEnum.INSPECTION_COMPLETED))
    {
      _collectingPrecisionImageSet = false;
    }
  }

  /**
   * Handles truncating message area to a reasonable size
   * @author Andy Mechtenberg
   */
  private void addLearningMessage(String message)
  {
    try
    {
      if (_learningMessagesTextArea.getDocument().getLength() > _RUNTIME_TEXT_LIMIT)
        _learningMessagesTextArea.getDocument().remove(0, _RUNTIME_TEXT_REMOVAL_AMOUNT);
    }
    catch (javax.swing.text.BadLocationException ex)
    {
      // do nothing
    }

    // append message to the text area only if this is not a precision image set collection
    if (_collectingPrecisionImageSet == false)
    {
      _learningMessagesTextArea.append(message);
      _learningMessagesTextArea.setCaretPosition(_learningMessagesTextArea.getDocument().getLength());
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    _jointTypeComboBoxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jointTypeComboBox_actionPerformed(e);
      }
    };
    
    //ShengChuan - Clear Tombstone
    _subtypeComboBoxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setTemplateMatchUIState();
      }
    };

    _refineSubtypesTableModel = new JointTypeSubtypeTableModel(_subtypesToRefine);
    _refineSubtypesTable = new JSortTable(_refineSubtypesTableModel);

    _mainBorder = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(115, 114, 105),new Color(165, 163, 151)),BorderFactory.createEmptyBorder(5,5,5,5));
    this.setBorder(_mainBorder);
    _shortRefinementBorder = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white, new Color(165, 163, 151)),
                                                 BorderFactory.createEmptyBorder(5, 5, 5, 5));
    _subtypeBorder = BorderFactory.createCompoundBorder(_subtypeTitledBorder, BorderFactory.createEmptyBorder(5, 5, 5, 5));

    this.setLayout(_mainBorderLayout);
    _learnAndShortRefinementPanel.setLayout(_learnAndShortRefinementBorderLayout);
    _subtypeLearningPanel.setBorder(null);
    _subtypeLearningPanel.setLayout(_subtypeLearningBorderLayout);
    _jointTypeSubtypePanel.setLayout(_jointTypeSubtypePairLayout);
    _jointTypeLabel.setText(StringLocalizer.keyToString("ATGUI_FAMILY_KEY"));
    _subtypeLabel.setText(StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY"));
    _boardSelectedLabel.setText(StringLocalizer.keyToString("ATGUI_PANEL_BOARD_KEY"));
    _refinementPanel.setLayout(_refinementBorderLayout);
    _refinementBorderLayout.setVgap(5);
    _subtypeLearningBorderLayout.setVgap(5);
    _subtypeLearningCheckBox.setSelected(true);
    _subtypeLearningCheckBox.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_ALGORITHM_SETTINGS_LEARNING_KEY"));
    _subtypeLearningCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        updateButtonState();
      }
    });

    //Siew Yeng - XCR-2139
    _slopeLearningPanel.setLayout(new VerticalFlowLayout());
    _slopeLearningCheckBox.setBorder(BorderFactory.createEmptyBorder(0,25,0,0));
    _slopeLearningPanel.add(_slopeLearningCheckBox);
    
    _slopeLearningCheckBox.setSelected(false);
    _slopeLearningCheckBox.setEnabled(true);
    _slopeLearningCheckBox.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_ALGORITHM_SETTINGS_SLOPE_LEARNING_KEY"));
    
    _shortLearningCheckBox.setSelected(true);
    _shortLearningCheckBox.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_SHORT_BACKGROUND_REGIONS_LEARNING_KEY"));
    _shortLearningCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        updateButtonState();
      }
    });

    _expectedImageLearningCheckBox.setSelected(true);
    _expectedImageLearningCheckBox.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_EXPECTED_IMAGES_FOR_VOIDING_LEARNING_KEY"));
    _expectedImageLearningCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        updateButtonState();
      }
    });

    _excludeLibrarySubtypeLearningCheckBox.setSelected(false);
    _excludeLibrarySubtypeLearningCheckBox.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_EXCLUDE_LIBRARY_SUBTYPE_LEARNING_KEY"));
    _excludeLibrarySubtypeLearningCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        /**
         * @todo is it require to do something?
         */
        }
    });
    
    /*
     * Kee Chin Seong
     */
    _learnedBoardApplyToAllBoardCheckBox.setSelected(false);
    _learnedBoardApplyToAllBoardCheckBox.setEnabled(false);
    _learnedBoardApplyToAllBoardCheckBox.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_ONE_BOARD_LEARNED_PROFILE_APPLY_TO_ALL_BOARD_KEY"));
    _learnedBoardApplyToAllBoardCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        /**
         * @todo is it require to do something?
         */
        }
    });
    
    //ShengChuan - Clear Tombstone - may need to hide it with enable key
    _templateLearningCheckBox.setSelected(false);
    _templateLearningCheckBox.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_COMPONENT_TEMPLATE_LEARNING_KEY"));
    _templateLearningCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        //XCR-3473, Learn button is disable when only enable learn image as template check box
        updateButtonState();
      }
    });

    /*
     * Kee Chin Seong
     */
    _boardSelectComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        boardSelectionComboBoxActionPerformed();
      }
    });

    _learnButton.setFont(FontUtil.getBoldFont(_learnButton.getFont(),Localization.getLocale()));
    _learnButton.setText(StringLocalizer.keyToString("MMGUI_INITIAL_LEARN_KEY"));
    // disable the learn button.  this will become enabled once an image set has been selected.  We'll get a guiEvent
    // coming in to notify us of that.
    _learnButton.setEnabled(false);
    _learnButton.setToolTipText(StringLocalizer.keyToString("MMGUI_INITIAL_UNABLE_TO_LEARN_EXPLANATION_KEY"));

    _learnButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        learnButton_actionPerformed();
      }
    });

    _subtypeExposureLearningButton.setFont(FontUtil.getBoldFont(_subtypeExposureLearningButton.getFont(),Localization.getLocale()));
    _subtypeExposureLearningButton.setText(StringLocalizer.keyToString("MMGUI_EXPOSURE_LEARNING_KEY"));
    _subtypeExposureLearningButton.setEnabled(false);
    _subtypeExposureLearningButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        subtypeExposureLearningButton_actionPerformed();
      }
    });
    
    _reLearnShortButton.setText(StringLocalizer.keyToString("MMGUI_INITIAL_RELEARN_SHORTS_KEY"));
    // disable the learn button.  this will become enabled once an image set has been selected.  We'll get a guiEvent
    // coming in to notify us of that.
    _reLearnShortButton.setEnabled(false);
    _reLearnShortButton.setToolTipText(StringLocalizer.keyToString("MMGUI_INITIAL_UNABLE_TO_LEARN_EXPLANATION_KEY"));

    _reLearnShortButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        reLearnShortButton_actionPerformed();
      }
    });

    _interactiveLearningButton.setText(StringLocalizer.keyToString("MMGUI_INITIAL_INTERACTIVE_EXPECTED_IMAGES_KEY"));
    _interactiveLearningButton.setEnabled(false);
    _interactiveLearningButton.setToolTipText(StringLocalizer.keyToString("MMGUI_INITIAL_INTERACTIVE_EXPECTED_IMAGES_TOOLTIP_KEY"));
    _interactiveLearningButton.setFont(FontUtil.getBoldFont(_interactiveLearningButton.getFont(),Localization.getLocale()));

    _interactiveLearningButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        interactiveLearningButton_actionPerformed();
      }
    });
    
    _subtypeMaskingButton.setText(StringLocalizer.keyToString("ATGUI_SUBTYPE_MASKING_BUTTON_KEY"));
    _subtypeMaskingButton.setEnabled(false);
    _subtypeMaskingButton.setToolTipText(StringLocalizer.keyToString("ATGUI_SUBTYPE_MASKING_BUTTON_KEY"));
    _subtypeMaskingButton.setFont(FontUtil.getBoldFont(_subtypeMaskingButton.getFont(),Localization.getLocale()));

    _subtypeMaskingButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        subtypeMaskingButton_actionPerformed();
      }
    });
    
    _resetAlgorithmSettingsButton.setText(StringLocalizer.keyToString("ATGUI_RESET_ALGORITHM_SETTINGS_BUTTON_KEY"));
    _resetAlgorithmSettingsButton.setEnabled(true); // XCR-2894 set to true by default
    _resetAlgorithmSettingsButton.setToolTipText(StringLocalizer.keyToString("ATGUI_RESET_ALGORITHM_SETTINGS_BUTTON_KEY"));

    _resetAlgorithmSettingsButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        resetAlgorithmSettingsButton_actionPerformed();
      }
    });
    
    // Jen Ping - XCR-2894: Reset to initial thresholds feature
    _initialAlgorithmSettingsButton.setText(StringLocalizer.keyToString("ATGUI_RESET_TO_INITIAL_SETTINGS_BUTTON_KEY"));
    _initialAlgorithmSettingsButton.setEnabled(true); // XCR-2894 set to true by default
    _initialAlgorithmSettingsButton.setToolTipText(StringLocalizer.keyToString("ATGUI_RESET_TO_INITIAL_SETTINGS_BUTTON_KEY"));
    
    _initialAlgorithmSettingsButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        initialAlgorithmSettingsButton_actionPerformed();
      }
    });

    _learnAndShortRefinementPanel.setBorder(_subtypeBorder);
    _shortLearningDetailsPanel.setLayout(_shortsLearningDetailsFlowLayout);
    _shortsLearningDetailsFlowLayout.setAlignment(FlowLayout.LEFT);
    _shortLearningDetailsButton.setText(StringLocalizer.keyToString("MMGUI_DETAILS_KEY"));
    _shortLearningDetailsButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        shortLearningDetailsButton_actionPerformed();
      }
    });
    _expectedImageLearningDetailsPanel.setLayout(_expectedImageLearningDetailsFlowLayout);
    _expectedImageLearningDetailsFlowLayout.setAlignment(FlowLayout.LEFT);
    _expectedImageLearningDetailsButton.setText(StringLocalizer.keyToString("MMGUI_DETAILS_KEY"));
    _expectedImageLearningDetailsButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        expectedImageLearningDetailsButton_actionPerformed();
      }
    });

    _subtypeWithExposureLearningDetailsPanel.setLayout(_subtypeWithExposureLearningDetailsFlowLayout);
    _subtypeWithExposureLearningDetailsFlowLayout.setAlignment(FlowLayout.LEFT);
    _subtypeWithExposureLearningDetailsButton.setText(StringLocalizer.keyToString("MMGUI_DETAILS_KEY"));
    _subtypeWithExposureLearningDetailsButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        subtypesWithExposureSettingLearningDetailsButton_actionPerformed();
      }
    });
    
    _subtypeImportedStatusDetailsPanel.setLayout(_subtypeImportedStatusDetailsFlowLayout);
    _subtypeImportedStatusDetailsFlowLayout.setAlignment(FlowLayout.LEFT);
    _subtypeImportedStatusDetailsButton.setText(StringLocalizer.keyToString("MMGUI_DETAILS_KEY"));
    _subtypeImportedStatusDetailsButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        subtypeImportedStatusDetailsButton_actionPerformed();
      }
    });

//    _subtypeLearningStatusOuterBorderLayout.setHgap(5);
    _subtypeLearningStatusOuterBorderLayout.setRows(4);
    _refineAllButton.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_REFINE_ALL_SUBTYPES_BUTTON_KEY"));
    _refineSubtypesInnerButtonPanel.setLayout(_refineSubtypesInnerButtonGridLayout);
    _refineSubtypesInnerButtonGridLayout.setHgap(10);
    _statusHeaderLabelPanel.setLayout(_statusHeaderLabelGridLayout);
    _statusHeaderLabelGridLayout.setRows(3);
    _statusLearningGridLayout.setRows(4);
    _statusBorderLayout.setHgap(10);
    _learningChoiceInnerPanel.setLayout(new VerticalFlowLayout());
    //_learningChoiceInnerPanel.setLayout(_learningChoiceInnerGridLayout);
    //_learningChoiceInnerGridLayout.setColumns(1);
    //_learningChoiceInnerGridLayout.setRows(4);
    _learningChoiceOuterPanel.setLayout(_learningChoiceOuterFlowLayout);
    _learningChoiceOuterFlowLayout.setAlignment(FlowLayout.LEFT);
    _learningChoiceOuterFlowLayout.setHgap(0);
//    _learningSubtypeInnerPanel.setLayout(_learningSubtypeInnerFlowLayout);
//    _learningSubtypeInnerFlowLayout.setAlignment(FlowLayout.LEFT);
//    _learningSubtypeInnerFlowLayout.setHgap(0);
    _learnButtonInnerPanel.setLayout(_learnButtonInnerPanelGridLayout);
    _deleteLearnedDataButton.setText(StringLocalizer.keyToString("MMGUI_INITIAL_DELETE_LEARN_DATA_KEY"));
    _deleteLearnedDataButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteLearnedData_actionPerformed(e);
        saveUIState();
      }
    });

    _learnButtonInnerPanelGridLayout.setColumns(1);
    _learnButtonInnerPanelGridLayout.setRows(6);
    _learnButtonInnerPanelGridLayout.setVgap(6);
    _subtypeRefinementBorderLayout.setVgap(6);
    _subtypeLearningDetailsButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        subtypeLearningDetailsButton_actionPerformed(e);
        saveUIState();
      }
    });
    _learningExecutionPanel.setLayout(_learningExecutionBorderLayout);
    _currentInfoPanel.setLayout(_currentInfoGridLayout);
    _currentInfoGridLayout.setColumns(2);
    _currentJointTypeLabel.setText("");
    _jointTypeInfoLabel.setText(StringLocalizer.keyToString("ATGUI_FAMILY_KEY") + ": ");
    _currentJointTypePanel.setLayout(_currentJointTypeFlowLayout);
    _currentJointTypeFlowLayout.setAlignment(FlowLayout.LEFT);
    _currentSubtypePanel.setLayout(_currentSubtypeFlowLayout);
    _currentSubtypeFlowLayout.setAlignment(FlowLayout.LEFT);
    _subtypeInfoLabel.setText(StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY") + ": ");
    _currentSubtypeLabel.setText("");
    _currentInfoPanel.setBorder(BorderFactory.createLoweredBevelBorder());
    _currentInfoPanel.setOpaque(false);
    _currentJointTypePanel.setBackground(SystemColor.info);
    _currentSubtypePanel.setBackground(SystemColor.info);
    _progressCancelPanel.setLayout(_cancelProgressBorderLayout);
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.setEnabled(false);
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed();
      }
    });

    _progressBar.setString("");
    _progressBar.setValue(0);
    _progressBar.setStringPainted(true);
    _progressBar.setFont(FontUtil.getBiggerFont(_progressBar.getFont(),Localization.getLocale()));
    _progressCancelPanel.setBorder(_emptyBorder);
    _jointTypeSubtypePanel.add(_jointTypeLabel);
    _jointTypeSubtypePanel.add(_jointTypeComboBox);
    _jointTypeSubtypePanel.add(_subtypeLabel);
    _jointTypeSubtypePanel.add(_subtypeComboBox);
    _jointTypeSubtypePanel.add(_boardSelectedLabel);
    _jointTypeSubtypePanel.add(_boardSelectComboBox);
    _subtypeComboBox.setPreferredSize(new Dimension(180, CommonUtil._COMBO_BOX_HEIGHT));
    _jointTypeComboBox.setPreferredSize(new Dimension(180, CommonUtil._COMBO_BOX_HEIGHT));
    _boardSelectComboBox.setPreferredSize(new Dimension(180, CommonUtil._COMBO_BOX_HEIGHT));
    _jointTypeBorderLayout.setHgap(5);
    _subtypeBorderLayout.setHgap(5);
    _boardSelectedBorderLayout.setHgap(5);
    _shortRefinementOuterPanel.setLayout(_shortRefinementOuterBorderLayout);
    _shortRefinementHeaderLabel.setFont(FontUtil.getBoldFont(_shortRefinementHeaderLabel.getFont(),Localization.getLocale()));
    _shortRefinementHeaderLabel.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_SHORT_REFINEMENT_KEY"));
    _refineShortLabel.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_SHORT_REFINEMENT_STATUS_NONE_KEY"));
    _refineShortDataButton.setEnabled(false);
    _refineShortDataButton.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_REFINE_SHORT_BUTTON_KEY"));
    _shortRefinementInnerPanel.setLayout(_shortRefinementInnerFlowLayout);
    _shortRefinementInnerFlowLayout.setAlignment(FlowLayout.LEFT);
    _shortRefinementOuterPanel.setBorder(_shortRefinementBorder);
    _subtypeRefinementPanel.setLayout(_subtypeRefinementBorderLayout);
    _subtypeRefinementLabel.setFont(FontUtil.getBoldFont(_subtypeRefinementLabel.getFont(),Localization.getLocale()));
    _subtypeRefinementLabel.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_SUBTYPE_REFINEMENT_KEY"));
    _subtypeRefinementPanel.setBorder(_refineSubtypesBorder);
    _mainBorderLayout.setHgap(0);
    _mainBorderLayout.setVgap(5);
    _refineSubtypesButtonPanel.setLayout(_refineSubtypesButtonflowLayout);
    _refineSubtypeButton.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_REFINE_SUBTYPES_BUTTON_KEY"));
    _refineSubtypesButtonflowLayout.setAlignment(FlowLayout.CENTER);
    _statusPanel.setBorder(_statusBorder);
    _statusPanel.setLayout(_statusBorderLayout);
    _statusLearningPanel.setLayout(_statusLearningGridLayout);
    _shortLearningLabel.setFont(FontUtil.getBoldFont(_shortLearningLabel.getFont(),Localization.getLocale()));
    _shortLearningLabel.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_SHORT_BACKGROUND_REGIONS_LEARNING_KEY") + ":");
    _expectedImageLearningLabel.setFont(FontUtil.getBoldFont(_expectedImageLearningLabel.getFont(),Localization.getLocale()));
    _expectedImageLearningLabel.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_EXPECTED_IMAGES_FOR_VOIDING_LEARNING_KEY") + ":");
    _importedSubtypeLabel.setFont(FontUtil.getBoldFont(_importedSubtypeLabel.getFont(), Localization.getLocale()));
    _importedSubtypeLabel.setText(StringLocalizer.keyToString("PLWK_INITIAL_TUNING_IMPORTED_SUBYPE_HEADER_KEY") + ":");
    _subtypeWithExposureLearningLabel.setFont(FontUtil.getBoldFont(_subtypeWithExposureLearningLabel.getFont(), Localization.getLocale()));
    _subtypeWithExposureLearningLabel.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_SUBTYPE_WITH_EXPOSURE_SETTING_KEY") + ":");

    //    _shortsLearningStatusLabel.setText("Partially learned, no refinement necessary");
    _subypteLearningStatusOuterPanel.setLayout(_subtypeLearningStatusOuterBorderLayout);
    _subtypeLearningLabel.setFont(FontUtil.getBoldFont(_subtypeLearningLabel.getFont(),Localization.getLocale()));
    _subtypeLearningLabel.setText(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_ALGORITHM_SETTINGS_LEARNING_KEY") + ":");
//    _subtypeLearningStatusLabel.setText("Learned, refinement possible");
    _subtypeLearningDetailsButton.setText(StringLocalizer.keyToString("MMGUI_DETAILS_KEY"));
    _subtypeLearningDetailsPanel.setLayout(_subtypeLearningDetailsFlowLayout);
    _subtypeLearningDetailsFlowLayout.setAlignment(FlowLayout.LEFT);
    _refineSubtypesTablePanel.setLayout(_refineSubtypesTableBorderLayout);
    _learningActionPanel.setLayout(_learningActionBorderLayout);
    _subtypeRefinementPanel.add(_refineSubtypesTablePanel, java.awt.BorderLayout.CENTER);
    _subtypeRefinementPanel.add(_subtypeRefinementLabel, java.awt.BorderLayout.NORTH);
    _subtypeRefinementPanel.add(_refineSubtypesButtonPanel, java.awt.BorderLayout.SOUTH);
    _refineSubtypesButtonPanel.add(_refineSubtypesInnerButtonPanel);
    _refineSubtypesInnerButtonPanel.add(_refineSubtypeButton);
    _refineSubtypesInnerButtonPanel.add(_refineAllButton);

    _refineSubtypesScrollPane.getViewport().add(_refineSubtypesTable);

    _subypteLearningStatusOuterPanel.add(_subtypeLearningDetailsPanel);
    _subtypeLearningDetailsPanel.add(_subtypeLearningDetailsButton);
    _subypteLearningStatusOuterPanel.add(_shortLearningDetailsPanel);
    _subypteLearningStatusOuterPanel.add(_expectedImageLearningDetailsPanel);
    _subypteLearningStatusOuterPanel.add(_subtypeImportedStatusDetailsPanel);
    _subypteLearningStatusOuterPanel.add(_subtypeWithExposureLearningDetailsPanel);
    
    _shortLearningDetailsPanel.add(_shortLearningDetailsButton);
    _expectedImageLearningDetailsPanel.add(_expectedImageLearningDetailsButton);
    _subtypeImportedStatusDetailsPanel.add(_subtypeImportedStatusDetailsButton);
    _subtypeWithExposureLearningDetailsPanel.add(_subtypeWithExposureLearningDetailsButton);

    _statusHeaderLabelPanel.add(_subtypeLearningLabel);
    _statusHeaderLabelPanel.add(_shortLearningLabel);
    _statusHeaderLabelPanel.add(_expectedImageLearningLabel);
    _statusHeaderLabelPanel.add(_importedSubtypeLabel);
    _statusHeaderLabelPanel.add(_subtypeWithExposureLearningLabel);
    
    _shortRefinementInnerPanel.add(_refineShortDataButton);
    _shortRefinementInnerPanel.add(_refineShortLabel);
    _learnAndShortRefinementPanel.add(_subtypeLearningPanel, java.awt.BorderLayout.CENTER);
    _shortRefinementOuterPanel.add(_shortRefinementHeaderLabel, java.awt.BorderLayout.NORTH);
    _shortRefinementOuterPanel.add(_shortRefinementInnerPanel, java.awt.BorderLayout.CENTER);

    _subtypeRefinementPanel.add(_subtypeRefinementLabel, java.awt.BorderLayout.NORTH);
//    this.add(_refinementPanel, java.awt.BorderLayout.CENTER);
    _subtypeLearningPanel.add(_learningActionPanel, java.awt.BorderLayout.SOUTH);
//    _learningActionPanel.add(_learnButtonOuterPanel, java.awt.BorderLayout.EAST);
    _learnButtonOuterPanel.setBorder(BorderFactory.createEmptyBorder(-5, 0, 0, 0));
    _learnAndShortRefinementPanel.add(_learnButtonOuterPanel, java.awt.BorderLayout.EAST);
//        _learningActionPanel.add(_learnButtonOuterPanel, java.awt.BorderLayout.EAST);
    _learnButtonOuterPanel.add(_learnButtonInnerPanel);
//    _learnButtonInnerPanel.add(new JLabel(" "));
    _learnButtonInnerPanel.add(_learnButton);
    _learnButtonInnerPanel.add(_reLearnShortButton);
    _learnButtonInnerPanel.add(_interactiveLearningButton);
    _learnButtonInnerPanel.add(_deleteLearnedDataButton);
    _learnButtonInnerPanel.add(_subtypeMaskingButton);
    _learnButtonInnerPanel.add(_resetAlgorithmSettingsButton);
    _learnButtonInnerPanel.add(_initialAlgorithmSettingsButton);
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ONLINE_WORKSTATION))
      _learnButtonInnerPanel.add(_subtypeExposureLearningButton);
    
//    _learningSubtypeInnerPanel.add(_subtypeLearningCheckBox);
//    _learningSubtypeInnerPanel.add(_excludeLibrarySubtypeLearningCheckBox);

    _learningChoiceInnerPanel.add(_subtypeLearningCheckBox);
    _learningChoiceInnerPanel.add(_slopeLearningPanel);
    _learningChoiceInnerPanel.add(_shortLearningCheckBox);
    _learningChoiceInnerPanel.add(_expectedImageLearningCheckBox);
    _learningChoiceInnerPanel.add(_excludeLibrarySubtypeLearningCheckBox);
    _learningChoiceInnerPanel.add(_templateLearningCheckBox);//ShengChuan - Clear Tombstone
    _learningChoiceInnerPanel.add(_learnedBoardApplyToAllBoardCheckBox);
    _learningChoiceOuterPanel.add(_learningChoiceInnerPanel);
    _subtypeLearningPanel.add(_jointTypeSubtypePanel, java.awt.BorderLayout.WEST);

    _statusPanel.add(_statusHeaderLabelPanel, java.awt.BorderLayout.WEST);
    _statusPanel.add(_statusLearningPanel, java.awt.BorderLayout.CENTER);
    _statusPanel.add(_subypteLearningStatusOuterPanel, java.awt.BorderLayout.EAST);
    _statusLearningPanel.add(_subtypeLearningStatusLabel, null);
    _statusLearningPanel.add(_shortsLearningStatusLabel, null);
    _statusLearningPanel.add(_expectedImageLearningStatusLabel, null);
    _statusLearningPanel.add(_importedSubtypeStatusLabel,null);
    _statusLearningPanel.add(_subtypeWithExposureLearningLabel,null);
    this.add(_statusPanel, java.awt.BorderLayout.SOUTH);
    _refinementPanel.add(_shortRefinementOuterPanel, java.awt.BorderLayout.NORTH);
    _refinementPanel.add(_subtypeRefinementPanel, java.awt.BorderLayout.CENTER);
    _learningActionPanel.add(_learningChoiceOuterPanel, java.awt.BorderLayout.WEST);
    _refineSubtypesTablePanel.add(_refineSubtypesScrollPane, java.awt.BorderLayout.CENTER);
    this.add(_learnAndShortRefinementPanel, java.awt.BorderLayout.NORTH);
    this.add(_learningExecutionPanel, java.awt.BorderLayout.CENTER);
    _currentInfoPanel.add(_currentJointTypePanel);
    _currentJointTypePanel.add(_jointTypeInfoLabel);
    _currentJointTypePanel.add(_currentJointTypeLabel);
    _currentInfoPanel.add(_currentSubtypePanel);
    _currentSubtypePanel.add(_subtypeInfoLabel);
    _learningMessagesTextArea.setEditable(false);
    _learningMessagesScrollPane.getViewport().add(_learningMessagesTextArea);
    _currentSubtypePanel.add(_currentSubtypeLabel);
    _learningExecutionPanel.add(_currentInfoPanel, java.awt.BorderLayout.NORTH);
    _cancelPanel.add(_cancelButton);
    _progressCancelPanel.add(_cancelPanel, java.awt.BorderLayout.SOUTH);
    _progressCancelPanel.add(_progressBar, java.awt.BorderLayout.CENTER);
    _learningExecutionPanel.add(_learningMessagesScrollPane, java.awt.BorderLayout.CENTER);
    _learningExecutionPanel.add(_progressCancelPanel, java.awt.BorderLayout.SOUTH);
    _currentJointTypeLabel.setFont(FontUtil.getBoldFont(_currentJointTypeLabel.getFont(),Localization.getLocale()));
    _currentSubtypeLabel.setFont(FontUtil.getBoldFont(_currentSubtypeLabel.getFont(),Localization.getLocale()));

    // initialize the Interactive Learning dialog.  It is not visible until needed.
    if (_interactiveLearningDialog == null)
    {
      _interactiveLearningDialog = new InteractiveLearningDialog(_mainUI, this, true);
    }

    // name section for gui regression testing
    _progressBar.setName(".learningProgressBar");
    _cancelButton.setName(".learningCancelButton");
    _refineShortDataButton.setName(".refineShortDataButton");
    _refineSubtypeButton.setName(".refineSubtypeButton");
    _subtypeLearningDetailsButton.setName(".subtypeLearningDetailsButton");
    _jointTypeComboBox.setName(".jointTypeComboBox");
    _subtypeComboBox.setName(".subtypeComboBox");
    _boardSelectComboBox.setName(".boardSelectComboBox");
    _subtypeLearningCheckBox.setName(".subtypeLearningCheckBox");
    _slopeLearningCheckBox.setName(".slopeLearningCheckBox");
    _shortLearningCheckBox.setName(".shortLearningCheckBox");
    _learnedBoardApplyToAllBoardCheckBox.setName(".learnedBoardApplyToAllBoardCheckBox");
    _learnButton.setName(".learnButton");
    _shortLearningDetailsButton.setName(".shortLearningDetailsButton");
    _subtypeExposureLearningButton.setName(".subtypeExposureLearningButton");
    _refineAllButton.setName(".refineAllButton");
    _deleteLearnedDataButton.setName(".deleteLearnedDataButton");
  }

  /**
   * @author Andy Mechtenberg
   */
  public JPanel getToolbar()
  {
    JPanel toolBarPanel = new JPanel();
    FlowLayout toolBarFlowLayout = new FlowLayout(FlowLayout.LEFT);
    toolBarPanel.setLayout(toolBarFlowLayout);
//    toolBarPanel.add(_deleteLearnedDataButton, null);
    return toolBarPanel;
  }

  /**
   * @author Andy Mechtenberg
   */
  public JMenuBar getMenu()
  {
    // we have to create a new one every time (like the toolbar) because it get's used up by the TestDev
    JMenuBar menuBar = new JMenuBar();
//    JMenu toolsMenu = new JMenu();
//    toolsMenu.setMnemonic(StringLocalizer.keyToString("ATGUI_TOOLS_MENU_MNEMONIC_KEY").charAt(0));
//    toolsMenu.setText(StringLocalizer.keyToString("ATGUI_TOOLS_MENU_KEY"));
//
//    JMenuItem deleteLearnedDataMenuItem = new JMenuItem();
//    deleteLearnedDataMenuItem.setMnemonic(StringLocalizer.keyToString("ATGUI_DELETE_LEARNED_DATA_MENU_MNEMONIC_KEY").charAt(0));
//    deleteLearnedDataMenuItem.setText(StringLocalizer.keyToString("ATGUI_DELETE_LEARNED_DATA_MENU_KEY"));
//    deleteLearnedDataMenuItem.addActionListener(new java.awt.event.ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        deleteLearnedThresholdsButton_actionPerformed(e);
//      }
//    });
//
//    toolsMenu.add(deleteLearnedDataMenuItem);
//    menuBar.add(toolsMenu);
    return menuBar;
  }

  /**
   * Called when this screen is made visible.  Some of the information displayed may have changed since
   * this screen was last visible, so this will update it.
   * @author Andy Mechtenberg
   */
  private void updateDataOnScreen()
  {
    // this screen is visible before a project is loaded, so this is a valid case.
    if (_project == null)
      return;

    _subtypeLearningCheckBox.setSelected(true);
    _slopeLearningCheckBox.setSelected(false);
    _shortLearningCheckBox.setSelected(true);

    // populate joint type combo box
    _panelDataUtil.populateComboBoxWithJointTypes(_project.getPanel(), _jointTypeComboBox);
    _jointTypeComboBox.setMaximumRowCount(20);
    populateSubtypeComboBox();
    populateBoardComboBox();
    _subtypesToRefine.clear();
    _refineSubtypesTableModel.clear();

    updateLearningStatusFields();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateLearningStatusFields()
  {
    com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
    int totalSubtypes = panel.getNumberOfInspectedSubtypes();
    int shortLearnedSubtypes = panel.getNumberOfInspectedSubtypesWithShortLearned();
    int subtypeLearnedSubtypes = panel.getNumberOfInspectedSubtypesWithAllAlgorithmsExceptShortLearned();
    int totalSubtypesWithExpectedImages = panel.getNumberOfInspectedSubtypesWithExpectedImageLearning();
    int expectedImageLearnedSubtypes = panel.getNumberOfInspectedSubtypesWithExpectedImageLearned();
    
    int subtypeWithExposureSettingLearnt = panel.getLearntExposureSettingSubtypes().size();

    LocalizedString shortLearnState = new LocalizedString("MMGUI_INITIAL_TUNING_LEARNING_STATE_KEY",
                                                          new Object[]{new Integer(shortLearnedSubtypes),
                                                          new Integer(totalSubtypes)});
    LocalizedString subtypeLearnState = new LocalizedString("MMGUI_INITIAL_TUNING_LEARNING_STATE_KEY",
                                                            new Object[]{new Integer(subtypeLearnedSubtypes),
                                                            new Integer(totalSubtypes)});
    LocalizedString expectedImageLearnState = new LocalizedString("MMGUI_INITIAL_TUNING_LEARNING_STATE_KEY",
                                                            new Object[]{new Integer(expectedImageLearnedSubtypes),
                                                            new Integer(totalSubtypesWithExpectedImages)});
    LocalizedString subtypeExposureLearnState = new LocalizedString("MMGUI_INITIAL_TUNING_LEARNING_STATE_KEY",
                                                            new Object[]{new Integer(subtypeWithExposureSettingLearnt),
                                                            new Integer(totalSubtypes)});


    _shortsLearningStatusLabel.setText(StringLocalizer.keyToString(shortLearnState));
    _subtypeLearningStatusLabel.setText(StringLocalizer.keyToString(subtypeLearnState));
    _expectedImageLearningStatusLabel.setText(StringLocalizer.keyToString(expectedImageLearnState));
    _subtypeWithExposureLearningStatusLabel.setText(StringLocalizer.keyToString(subtypeExposureLearnState));

    _unlearnedShorts = panel.getInspectedSubtypesWithoutShortLearned();
    _unlearnedSubtypes = panel.getInspectedSubtypesWithoutAllAlgorithmsExceptShortLearned();
    _unlearnedExpectedImages = panel.getInspectedSubtypesWithoutExpectedImagesLearned();

    // update imported subtype status
    int totalOfImportedSubtype = totalSubtypes - panel.getInspectedAndUnImportedSubtypesUnsorted().size();
    LocalizedString unImportedSubtypeState = new LocalizedString("PLWK_INITIAL_TUNING_IMPORTED_STATE_KEY",
                                                            new Object[]{new Integer(totalOfImportedSubtype),
                                                            new Integer(totalSubtypes)});
    _importedSubtypeStatusLabel.setText(StringLocalizer.keyToString(unImportedSubtypeState));
    _unImportedSubtypes.clear();
    _unImportedSubtypes = null;
    _unImportedSubtypes = new ArrayList(panel.getInspectedAndUnImportedSubtypesUnsorted());
  }

  /*
   * Kee Chin Seong
   */
  private void boardSelectionComboBoxActionPerformed()
  {
    if(_boardSelectComboBox.getSelectedIndex() != 0)
    {
      if(_shortLearningCheckBox.isSelected() == true)
      {
        //Kee Chin Seong - when is set to board level only apply that 
        _learnedBoardApplyToAllBoardCheckBox.setEnabled(true);
      }
      else
      {
        //Kee Chin Seong - when is set to board level only apply that 
        _learnedBoardApplyToAllBoardCheckBox.setEnabled(false);
      }
    }
    else
    {
      _learnedBoardApplyToAllBoardCheckBox.setEnabled(false);
    }
  }
  
  /**
   * @author Andy Mechtenberg
   */
  private void updateButtonState()
  {
    // there exists a potential race condition where the image set selection coming from the screen change will
    // come in AFTER the runboardSelectionComboBoxActionPerformed button has been clicked, (if you click the run button extremely fast) causing the learn button to re-enable.
    // This "if" should prevent that.  See CR27697 for details.
    if (_learningInProgress)
      return;
    boolean shortSelected = _shortLearningCheckBox.isSelected();
    boolean expectedImageSelected = _expectedImageLearningCheckBox.isSelected() && _expectedImageLearningCheckBoxAllowed;
    boolean expectedImageTemplateSelected = _templateLearningCheckBox.isSelected();
    boolean subtypeSelected = _subtypeLearningCheckBox.isSelected();
    
    //Siew Yeng - XCR-2139
    if(subtypeSelected)
      _slopeLearningCheckBox.setEnabled(true);
    else
      _slopeLearningCheckBox.setEnabled(false);
    
    boolean imageSetsSelected = (_selectedImageSets.isEmpty() == false);
    
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    //XCR-3473, Learn button is disable when only enable learn image as template check box
    _learnButton.setEnabled(imageSetsSelected && (shortSelected || expectedImageSelected || subtypeSelected || expectedImageTemplateSelected));
    _reLearnShortButton.setEnabled(imageSetsSelected);
    _interactiveLearningButton.setEnabled(imageSetsSelected && expectedImageSelected);
    
    if (jointTypeChoice != null &&  jointTypeChoice.isAllFamilies() == false)
    {
      if(jointTypeChoice.getJointTypeEnum().equals(JointTypeEnum.SINGLE_PAD) == true ||
         jointTypeChoice.getJointTypeEnum().equals(JointTypeEnum.LGA) == true)
      _subtypeMaskingButton.setEnabled(imageSetsSelected);
    }
    
    _resetAlgorithmSettingsButton.setEnabled(true);
    _initialAlgorithmSettingsButton.setEnabled(true);
    _subtypeExposureLearningButton.setEnabled(Config.getInstance().getIntValue(SoftwareConfigEnum.MAXIMUM_SPEED_SELECTION) >= 2);
    setTemplateMatchUIState();//ShengChuan - Clear Tombstone
   
    if (imageSetsSelected == false)
    {
      _learnButton.setToolTipText(StringLocalizer.keyToString("MMGUI_INITIAL_UNABLE_TO_LEARN_EXPLANATION_KEY"));
      _reLearnShortButton.setToolTipText(StringLocalizer.keyToString("MMGUI_INITIAL_UNABLE_TO_LEARN_EXPLANATION_KEY"));
    }
    else
    {
      _learnButton.setToolTipText("");
      _reLearnShortButton.setToolTipText("");
    }
    // XCR-3181 Check box "Apply Shot to All Board" still shown after disabling "Algorithm Setting" and "Short Background Regions"
    _learnedBoardApplyToAllBoardCheckBox.setEnabled(_boardSelectComboBox.getSelectedIndex() != 0 &&
                                                    (shortSelected || expectedImageTemplateSelected));
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    saveUIState();
    _project = null;
    _jointTypeComboBox.removeActionListener(_jointTypeComboBoxActionListener);
    _subtypeComboBox.removeActionListener(_subtypeComboBoxActionListener);//ShengChuan - Clear Tombstone
    _jointTypeComboBox.removeAllItems();
    _subtypeComboBox.removeAllItems();
    _boardSelectComboBox.removeAllItems();
    _refineSubtypesTableModel.clear();
    _subtypesToRefine.clear();
    _unlearnedShorts.clear();
    _unlearnedExpectedImages.clear();
    _unlearnedSubtypes.clear();
    _unImportedSubtypes.clear();
    _currentJointTypeLabel.setText("");
    _currentSubtypeLabel.setText("");
    _learningMessagesTextArea.setText(null);
    _progressBar.setString("");
    _progressBar.setValue(0);
    GuiObservable.getInstance().deleteObserver(this);
    ProjectObservable.getInstance().deleteObserver(this);
    _projectPersistSettings = null;
    _learnButton.setEnabled(false);
    _learnButton.setToolTipText(StringLocalizer.keyToString("MMGUI_INITIAL_UNABLE_TO_LEARN_EXPLANATION_KEY"));
    _reLearnShortButton.setEnabled(false);
    _subtypeExposureLearningButton.setEnabled(false);
    _reLearnShortButton.setToolTipText(StringLocalizer.keyToString("MMGUI_INITIAL_UNABLE_TO_LEARN_EXPLANATION_KEY"));
  }

  /**
   * @author George Booth
   */
  public void populateWithProjectData()
  {
    _panelDataUtil = PanelDataUtil.getInstance();

    _jointTypeComboBox.addActionListener(_jointTypeComboBoxActionListener);
    _subtypeComboBox.addActionListener(_subtypeComboBoxActionListener);//ShengChuan - Clear Tombstone

    updateDataOnScreen();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void populateWithProjectData(Project project)
  {
    Assert.expect(project != null);
    _project = project;

    // add this as an observer of the gui; make sure there is only one
    GuiObservable.getInstance().addObserver(this);
    ProjectObservable.getInstance().addObserver(this);

    if (_active)
    {
      populateWithProjectData();
      _populateProjectData = false;
    }
    else
    {
      _populateProjectData = true;
    }

  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateProjectData()
  {
    // this screen is visible before a project is loaded, so this is a valid case.
    if (_project == null)
      return;

    // populate joint type combo box
    _panelDataUtil.populateComboBoxWithJointTypes(_project.getPanel(), _jointTypeComboBox);

    populateSubtypeComboBox();
    populateBoardComboBox();

    updateLearningStatusFields();
  }

  /**
   * Lets iterate through the image sets and warn the user if they attempt to learn with image sets
   * whose major version does not match the current project version.
   *
   * @return boolean if the user wishes to not continue with the operation.
   * @author Erica Wheatcroft
   */
  private boolean validateImageSets()
  {
    int projectVersion = _project.getTestProgramVersion();
    boolean imageSetsNotValid = false;
    for(ImageSetData imageSetData : _selectedImageSets)
    {
      if(imageSetData.getTestProgramVersionNumber() != projectVersion)
      {
        imageSetsNotValid = true;
        break;
      }
    }

    return imageSetsNotValid;
  }

  /**
   * @author George Booth
   */
  private void interactiveLearningButton_actionPerformed()
  {
    _interactiveLearningDialogVisible = true;
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        learnButton_actionPerformed();
      }
    });
  }

  /**
   * @author George Booth
   */
  public void interactiveLearningCancelled()
  {
    if (_interactiveLearningDialogVisible)
    {
      if (_interactiveLearningDialog != null)
      {
        _interactiveLearningDialog.setVisible(false);
      }
      _interactiveLearningDialogVisible = false;
    }
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        cancelButton_actionPerformed();
      }
    });
  }

  /**
   * @author Yong Sheng Chuan
   */
  private void subtypeExposureLearningButton_actionPerformed()
  {
    if (_project.isGenerateByNewScanRoute() == false)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    StringLocalizer.keyToString("ALGDIAG_UNABLE_TO_LEARN_EXPOSURE_SETTING_ERROR_KEY"),
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
      return;
    }
    
    boolean isCheckLatestProgram = _project.isCheckLatestTestProgram();
    _project.setCheckLatestTestProgram(true);
    final TestProgram testProgram = _project.getTestProgram();
    
    _project.setCheckLatestTestProgram(isCheckLatestProgram);
    final JointTypeChoice currentJointTypeChoice = (JointTypeChoice) _jointTypeComboBox.getSelectedItem();
    final SubtypeChoice currentSubtypeChoice = (SubtypeChoice) _subtypeComboBox.getSelectedItem();
    java.util.List<ReconstructionRegion> regionListToLearn = new ArrayList<ReconstructionRegion>();
    java.util.List<Subtype> subtypeToLearn = new ArrayList<Subtype>();
    if (currentJointTypeChoice.isAllFamilies())
      subtypeToLearn.addAll(_project.getPanel().getInspectedSubtypes());
    else if (currentSubtypeChoice.isAllSubtypes())
    {
      JointTypeEnum jointType = currentJointTypeChoice.getJointTypeEnum();
      if(jointType != null)
      {
        if(_shortLearningCheckBox.isSelected() || _excludeLibrarySubtypeLearningCheckBox.isSelected() == false)
           subtypeToLearn = _project.getPanel().getInspectedSubtypes(jointType);
        else
           subtypeToLearn = _project.getPanel().getInspectedAndUnImportedSubtypes(jointType);
      }
      else
      {
        if (_shortLearningCheckBox.isSelected() || _excludeLibrarySubtypeLearningCheckBox.isSelected() == false)
          subtypeToLearn = new ArrayList<Subtype>(_project.getPanel().getInspectedSubtypesUnsorted());
        else
          subtypeToLearn = new ArrayList<Subtype>(_project.getPanel().getInspectedAndUnImportedSubtypesUnsorted());
      } 
    }
    else
      subtypeToLearn.add(currentSubtypeChoice.getSubtype());

    //take all first region of the subtype selected
    for (Subtype subtype : subtypeToLearn)
    {
      testProgram.clearFilters();
      testProgram.addFilter(subtype);
      for (ReconstructionRegion region : testProgram.getFilteredInspectionRegions())
      {
        if (region.getInspectablePads().isEmpty() == false)
        {
          regionListToLearn.add(region);
          break;
        }
      }
    }
    _learningInProgress = true;
    _mainUI.getMainMenuBar().removeProjectObservable(true);
    _mainUI.disableMenuAndToolBars();
    _testDev.disableTaskPanelNavigation();
    _cancelButton.setEnabled(true);
    TestProgram virtualTestProgram = null;
    _subtypeToExposureSetting.clear();
    final ImageSetData imageSetData = new ImageSetData();
    imageSetData.setProjectName(_project.getName());
    imageSetData.setTestProgramVersionNumber(_project.getTestProgramVersion());
    long timeInMils = System.currentTimeMillis();
    imageSetData.setImageSetName(Directory.getDirName(timeInMils));
    imageSetData.setDateInMils(timeInMils);
    imageSetData.setGenerateMultiAngleImages(false);
    imageSetData.setVariationName(VariationSettingManager.ALL_LOADED);
    imageSetData.setUserDescription("Auto Exposure Learning Image");
    imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.PANEL);
    imageSetData.setMachineSerialNumber(XrayTester.getInstance().getSerialNumber());
    imageSetData.setBoardTypeToAcquireImagesAgainst( _testDev.getCurrentBoardType());
    imageSetData.setImageSetCollectionTypeEnum(ImageSetCollectionTypeEnum.EXPOSURE_LEARNING);
    try
    {
      VirtualLiveManager.getInstance().setVirtualLiveMode(true);
      VirtualLiveManager.getInstance().setVirtualLiveWithAlignment(true);
      testProgram.clearFilters();
     /* Cannot reuse existing testprogram
      * - need to edit the focus group of certain region, hence new region is needed.
      * - cannot use region with large view or 2.5d, need recreate
      */
      virtualTestProgram = VirtualLiveManager.getInstance().createTestProgramForExposureLearning(testProgram, _project, regionListToLearn, StageSpeedEnum.getWholeNumberStageSpeedList());
      imageSetData.setNumberOfImagesSaved(virtualTestProgram.getNumberOfInspectionImages());
      MainMenuGui.getInstance().generateImagesForExposureLearning(MainMenuGui.getInstance(), virtualTestProgram, imageSetData);
      _statisticalTuningEngine.learnSubtypeExposureSetting(_subtypeToExposureSetting);
    }
    catch (final XrayTesterException xte)
    {
      MainMenuGui.getInstance().handleXrayTesterException(xte);
    }
    finally
    {
      virtualTestProgram.clear(true);
      VirtualLiveManager.getInstance().setVirtualLiveMode(false);
      VirtualLiveManager.getInstance().setVirtualLiveWithAlignment(false);
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          _learningInProgress = false;

          updateLearningStatusFields();
          _mainUI.enableMenuAndToolBars();
          _mainUI.getMainMenuBar().removeProjectObservable(false);
          _testDev.enableTaskPanelNavigation();
          _cancelButton.setEnabled(false);

          if (_progressBar.getPercentComplete() == 1.0)
          {
            _learningMessagesTextArea.append(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_COMPLETE_KEY") + "\n\n");
          }
          else
          {
            _learningMessagesTextArea.append(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_ABORTED_KEY") + "\n\n");
          }
        }
      });
    }
  }
  
  /**
   * @author Andy Mechtenberg
   */
  private void learnButton_actionPerformed()
  {
    _learningInProgress = true;
    _updatingShortsData = false;
    _mainUI.getMainMenuBar().removeProjectObservable(true);
    _mainUI.disableMenuAndToolBars();
    _testDev.disableTaskPanelNavigation();
    _cancelButton.setEnabled(true);
    setLearningUIState();

    final JointTypeChoice currentJointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    final SubtypeChoice currentSubtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();    
    _learningMessagesTextArea.append(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_IN_PROGRESS_KEY") + "\n");
    _learningMessagesTextArea.append(StringLocalizer.keyToString("ATGUI_FAMILY_KEY") + ": " + currentJointTypeChoice + "\n");
    _learningMessagesTextArea.append(StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY") + ": " + currentSubtypeChoice + "\n");
    
    // Handle board selection
    if (isBoardInstanceSelected() == false)
    {
      _learningMessagesTextArea.append(StringLocalizer.keyToString("ATGUI_PANEL_BOARD_KEY") + ": " + StringLocalizer.keyToString("ATGUI_PANEL_KEY") + "\n");
    }
    else
    {
      Board board = getSelectedBoardInstance();
      _learningMessagesTextArea.append(StringLocalizer.keyToString("ATGUI_PANEL_BOARD_KEY") + ": " + StringLocalizer.keyToString("ATGUI_BOARD_KEY") + " " + board.getName() + "\n");
    }

    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        int sizeOfLearnSubtypes = 0;
        try
        {
          // Check if a board is selected
          boolean isBoardSelected = isBoardInstanceSelected(); 
          
          _statisticalTuningEngine.enableInteractiveLearning(_interactiveLearningDialogVisible);

          if (currentJointTypeChoice.isAllFamilies()) // all families is the same as a board test
          {
            if(_excludeLibrarySubtypeLearningCheckBox.isSelected())
              sizeOfLearnSubtypes = _project.getPanel().getInspectedAndUnImportedSubtypesUnsorted().size();
            else
              sizeOfLearnSubtypes = _project.getPanel().getInspectedSubtypesUnsorted().size();

            if (isBoardSelected == false)
            {
              _statisticalTuningEngine.learnPanel(_project,
                                                  _selectedImageSets,
                                                  _subtypeLearningCheckBox.isSelected(),
                                                  _slopeLearningCheckBox.isSelected(),
                                                  _shortLearningCheckBox.isSelected(),
                                                  _templateLearningCheckBox.isSelected(),
                                                  (_expectedImageLearningCheckBox.isSelected() && _expectedImageLearningCheckBoxAllowed),
                                                  _excludeLibrarySubtypeLearningCheckBox.isSelected());
            }
            else
            {
              _statisticalTuningEngine.learnBoard(_project,
                                                  getSelectedBoardInstance(),
                                                  _selectedImageSets,
                                                  _subtypeLearningCheckBox.isSelected(),
                                                  _slopeLearningCheckBox.isSelected(),
                                                  _shortLearningCheckBox.isSelected(),
                                                  _templateLearningCheckBox.isSelected(),
                                                  (_expectedImageLearningCheckBox.isSelected() && _expectedImageLearningCheckBoxAllowed),
                                                  _excludeLibrarySubtypeLearningCheckBox.isSelected());//ShengChuan - Clear Tombstone
              if(_learnedBoardApplyToAllBoardCheckBox.isSelected() == true && _learningInProgress)
                    _statisticalTuningEngine.applyLearnedBoardProfileToAllBoard(_project,
                                                                                getSelectedBoardInstance(),
                                                                                null,
                                                                                null,
                                                                                _selectedImageSets,
                                                                                _subtypeLearningCheckBox.isSelected(),
                                                                                _slopeLearningCheckBox.isSelected(),
                                                                                _shortLearningCheckBox.isSelected(),
                                                                                _templateLearningCheckBox.isSelected(),
                                                                                (_expectedImageLearningCheckBox.isSelected() && _expectedImageLearningCheckBoxAllowed),
                                                                                _excludeLibrarySubtypeLearningCheckBox.isSelected());//ShengChuan - Clear Tombstone
            }
          }
          else if (currentSubtypeChoice.isAllSubtypes()) // this must be jointType-all test
          {
            if(_excludeLibrarySubtypeLearningCheckBox.isSelected())
              sizeOfLearnSubtypes = _project.getPanel().getInspectedAndUnImportedSubtypes(currentJointTypeChoice.getJointTypeEnum()).size();
            else
              sizeOfLearnSubtypes = _project.getPanel().getInspectedSubtypes(currentJointTypeChoice.getJointTypeEnum()).size();

            if (isBoardSelected == false)
            {
              _statisticalTuningEngine.learnJointType(_project,
                                                      currentJointTypeChoice.getJointTypeEnum(),
                                                      null,
                                                      _selectedImageSets,
                                                      _subtypeLearningCheckBox.isSelected(),
                                                      _slopeLearningCheckBox.isSelected(),
                                                      _shortLearningCheckBox.isSelected(),
                                                      _templateLearningCheckBox.isSelected(),
                                                      (_expectedImageLearningCheckBox.isSelected() && _expectedImageLearningCheckBoxAllowed),
                                                      _excludeLibrarySubtypeLearningCheckBox.isSelected());//ShengChuan - Clear Tombstone
            }
            else
            {
              _statisticalTuningEngine.learnJointType(_project,
                                                      currentJointTypeChoice.getJointTypeEnum(),
                                                      getSelectedBoardInstance(),
                                                      _selectedImageSets,
                                                      _subtypeLearningCheckBox.isSelected(),
                                                      _slopeLearningCheckBox.isSelected(),
                                                      _shortLearningCheckBox.isSelected(),
                                                      _templateLearningCheckBox.isSelected(),
                                                      (_expectedImageLearningCheckBox.isSelected() && _expectedImageLearningCheckBoxAllowed),
                                                      _excludeLibrarySubtypeLearningCheckBox.isSelected());//ShengChuan - Clear Tombstone
              
              if(_learnedBoardApplyToAllBoardCheckBox.isSelected() == true && _learningInProgress)
                    _statisticalTuningEngine.applyLearnedBoardProfileToAllBoard(_project,
                                                                                getSelectedBoardInstance(),
                                                                                null,
                                                                                currentJointTypeChoice.getJointTypeEnum(),
                                                                                _selectedImageSets,
                                                                                _subtypeLearningCheckBox.isSelected(),
                                                                                _slopeLearningCheckBox.isSelected(),
                                                                                _shortLearningCheckBox.isSelected(),
                                                                                _templateLearningCheckBox.isSelected(),
                                                                                (_expectedImageLearningCheckBox.isSelected() && _expectedImageLearningCheckBoxAllowed),
                                                                                _excludeLibrarySubtypeLearningCheckBox.isSelected());//ShengChuan - Clear Tombstone
                  }
          }
          else
          {
            if(_excludeLibrarySubtypeLearningCheckBox.isSelected() && currentSubtypeChoice.getSubtype().isImportedPackageLibrary())
              sizeOfLearnSubtypes = 0;
            else
              sizeOfLearnSubtypes = 1;

            if (isBoardSelected == false)
            {
              _statisticalTuningEngine.learnSubtype(_project,
                                                    currentSubtypeChoice.getSubtype(),
                                                    null,
                                                    _selectedImageSets,
                                                    _subtypeLearningCheckBox.isSelected(),
                                                    _slopeLearningCheckBox.isSelected(),
                                                    _shortLearningCheckBox.isSelected(),
                                                    _templateLearningCheckBox.isSelected(),
                                                    (_expectedImageLearningCheckBox.isSelected() && _expectedImageLearningCheckBoxAllowed),
                                                    false,
                                                    _excludeLibrarySubtypeLearningCheckBox.isSelected());//ShengChuan - Clear Tombstone
            }
            else
            {
              _statisticalTuningEngine.learnSubtype(_project,
                                                    currentSubtypeChoice.getSubtype(),
                                                    getSelectedBoardInstance(),
                                                    _selectedImageSets,
                                                    _subtypeLearningCheckBox.isSelected(),
                                                    _slopeLearningCheckBox.isSelected(),
                                                    _shortLearningCheckBox.isSelected(),
                                                    _templateLearningCheckBox.isSelected(),
                                                    (_expectedImageLearningCheckBox.isSelected() && _expectedImageLearningCheckBoxAllowed),
                                                    false,
                                                    _excludeLibrarySubtypeLearningCheckBox.isSelected());//ShengChuan - Clear Tombstone
              
              if(_learnedBoardApplyToAllBoardCheckBox.isSelected() == true && _learningInProgress)
                  _statisticalTuningEngine.applyLearnedBoardProfileToAllBoard(_project,
                                                                              getSelectedBoardInstance(),
                                                                              currentSubtypeChoice.getSubtype(),
                                                                              null,
                                                                              _selectedImageSets,
                                                                              _subtypeLearningCheckBox.isSelected(),
                                                                              _slopeLearningCheckBox.isSelected(),
                                                                              _shortLearningCheckBox.isSelected(),
                                                                              _templateLearningCheckBox.isSelected(),
                                                                              (_expectedImageLearningCheckBox.isSelected() && _expectedImageLearningCheckBoxAllowed),
                                                                              _excludeLibrarySubtypeLearningCheckBox.isSelected());//ShengChuan - Clear Tombstone
            }
          }
        }
        catch (final XrayTesterException xte)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              displayError(xte);
            }
          });
        }
        finally
        {
          _learningInProgress = false;
          final int sizeOfSubtypes = sizeOfLearnSubtypes;
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              if (_interactiveLearningDialogVisible && _interactiveLearningDialog != null)
              {
                _interactiveLearningDialog.setVisible(false);
              }
              _interactiveLearningDialogVisible = false;
              _statisticalTuningEngine.enableInteractiveLearning(false);
              _statisticalTuningEngine.uncancelLearning();

              updateLearningStatusFields();
              _mainUI.enableMenuAndToolBars();
              _mainUI.getMainMenuBar().removeProjectObservable(false);
              _testDev.enableTaskPanelNavigation();
              _cancelButton.setEnabled(false);
              setLearningUIState();
              if (_progressBar.getPercentComplete() == 1.0 || sizeOfSubtypes == 0)
                _learningMessagesTextArea.append(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_COMPLETE_KEY") + "\n\n");
              else
                _learningMessagesTextArea.append(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_ABORTED_KEY") + "\n\n");

              setExposedPadVoidingImagesUIState();
              
              setSinglePadVoidingImagesUIState();

              // Depending on customers, some customers do not want to directly save project as it might be time consuming.
              // We will do it later.
              if (TestExecution.getInstance().isPromptSavingRecipeAfterLearning())
              {
                  LocalizedString saveQuestion = new LocalizedString("MM_GUI_SAVE_CHANGES_KEY",
                                                               new Object[]{_project.getName()});
                  int answer = JOptionPane.showConfirmDialog(_mainUI,
                                                             StringUtil.format(StringLocalizer.keyToString(saveQuestion), 50),
                                                             StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                             JOptionPane.YES_NO_OPTION,
                                                             JOptionPane.WARNING_MESSAGE);
                  if (answer == JOptionPane.YES_OPTION)
                  {
                    _mainUI.getMainMenuBar().saveProject();
                  }
              }
            }
          });
        }
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void reLearnShortButton_actionPerformed()
  {
    // brind up the re-learn shorts dialog, let the user make some choices
    //show the add board dialog
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();

    final ReLearnShortsDialog dlg = new ReLearnShortsDialog(_mainUI, _project, jointTypeChoice, subtypeChoice, _boardSelectComboBox.getSelectedItem(), _testDev.getCurrentBoardType());
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    dlg.clear();
    dlg.dispose();

    if (dlg.wasCanceled())
      return;

    _learningInProgress = true;
    _updatingShortsData = true;
    _mainUI.getMainMenuBar().removeProjectObservable(true);
    _mainUI.disableMenuAndToolBars();
    _testDev.disableTaskPanelNavigation();
    _cancelButton.setEnabled(true);
    setLearningUIState();

    final JointTypeChoice currentJointTypeChoice = dlg.getJointTypeChoice();
    final SubtypeChoice currentSubtypeChoice = dlg.getSubtypeChoice();
    final boolean isBoardSelected = dlg.isBoardInstanceSelected();
    final boolean relearnSubtype = dlg.isRelearnOnlySelected();

    _learningMessagesTextArea.append(StringLocalizer.keyToString("MMGUI_INITIAL_RELEARN_IN_PROGRESS_KEY") + "\n");
    if (relearnSubtype == false) // means EXCLUDE the joint type/subtype
      _learningMessagesTextArea.append(StringLocalizer.keyToString("MMGUI_INITIAL_RELEARN_EXCLUDING_KEY") + "\n");
    _learningMessagesTextArea.append(StringLocalizer.keyToString("ATGUI_FAMILY_KEY") + ": " + currentJointTypeChoice + "\n");
    _learningMessagesTextArea.append(StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY") + ": " + currentSubtypeChoice + "\n");

    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (relearnSubtype)
          {
            if (currentJointTypeChoice.isAllFamilies()) // all families is the same as a board test
            {
              if (isBoardSelected == false)
              {
                _statisticalTuningEngine.relearnJointSpecificDataForPanel(_project,
                                                                          _selectedImageSets,
                                                                          _excludeLibrarySubtypeLearningCheckBox.isSelected());
              }
              else
              {
                _statisticalTuningEngine.relearnJointSpecificDataForBoard(_project,
                                                                          dlg.getSelectedBoardInstance(),
                                                                          _selectedImageSets,
                                                                          _excludeLibrarySubtypeLearningCheckBox.isSelected());
              }
            }
            else if (currentSubtypeChoice.isAllSubtypes()) // this must be jointType-all test
            {
              if (isBoardSelected == false)
              {
                _statisticalTuningEngine.relearnJointSpecificDataForJointType(_project,
                                                                              currentJointTypeChoice.getJointTypeEnum(),
                                                                              null,
                                                                              _selectedImageSets,
                                                                              _excludeLibrarySubtypeLearningCheckBox.isSelected());
              }
              else
              {
                _statisticalTuningEngine.relearnJointSpecificDataForJointType(_project,
                                                                              currentJointTypeChoice.getJointTypeEnum(),
                                                                              dlg.getSelectedBoardInstance(),
                                                                              _selectedImageSets,
                                                                              _excludeLibrarySubtypeLearningCheckBox.isSelected());
              }
            }
            else
            {
              if (isBoardSelected == false)
              {
                _statisticalTuningEngine.relearnJointSpecificDataForSubtype(_project,
                                                                            currentSubtypeChoice.getSubtype(),
                                                                            null,
                                                                            _selectedImageSets,
                                                                            _excludeLibrarySubtypeLearningCheckBox.isSelected());
              }
              else
              {
                _statisticalTuningEngine.relearnJointSpecificDataForSubtype(_project,
                                                                            currentSubtypeChoice.getSubtype(),
                                                                            dlg.getSelectedBoardInstance(),
                                                                            _selectedImageSets,
                                                                            _excludeLibrarySubtypeLearningCheckBox.isSelected());
              }
            }
          }
          else
          {
            // the case of excluding all-all doesn't make sense, so not allowed here
            if (currentSubtypeChoice.isAllSubtypes()) // this must be jointType-all test
            {
              if (isBoardSelected == false)
              {
                _statisticalTuningEngine.relearnJointSpecificDataExceptForJointType(_project,
                                                                                    currentJointTypeChoice.getJointTypeEnum(),
                                                                                    null,
                                                                                    _selectedImageSets,
                                                                                    _excludeLibrarySubtypeLearningCheckBox.isSelected());
              }
              else
              {
                _statisticalTuningEngine.relearnJointSpecificDataExceptForJointType(_project,
                                                                                    currentJointTypeChoice.getJointTypeEnum(),
                                                                                    dlg.getSelectedBoardInstance(),
                                                                                    _selectedImageSets,
                                                                                    _excludeLibrarySubtypeLearningCheckBox.isSelected());
              }
            }
            else
            {
              if (isBoardSelected == false)
              {
                _statisticalTuningEngine.relearnJointSpecificDataExceptForSubtype(_project,
                                                                                  currentSubtypeChoice.getSubtype(),
                                                                                  null,
                                                                                  _selectedImageSets,
                                                                                  _excludeLibrarySubtypeLearningCheckBox.isSelected());
              }
              else
              {
                _statisticalTuningEngine.relearnJointSpecificDataExceptForSubtype(_project,
                                                                                  currentSubtypeChoice.getSubtype(),
                                                                                  dlg.getSelectedBoardInstance(),
                                                                                  _selectedImageSets,
                                                                                  _excludeLibrarySubtypeLearningCheckBox.isSelected());
              }
            }
          }
        }
        catch (final XrayTesterException xte)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              displayError(xte);
            }
          });
        }
        finally
        {
          _learningInProgress = false;
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              updateLearningStatusFields();
              _mainUI.enableMenuAndToolBars();
              _mainUI.getMainMenuBar().removeProjectObservable(false);
              _testDev.enableTaskPanelNavigation();
              _cancelButton.setEnabled(false);
              setLearningUIState();
              if (_progressBar.getPercentComplete() == 1.0)
                _learningMessagesTextArea.append(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_COMPLETE_KEY") + "\n\n");
              else
                _learningMessagesTextArea.append(StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_ABORTED_KEY") + "\n\n");

              setExposedPadVoidingImagesUIState();
              
              setSinglePadVoidingImagesUIState();
            }
          });
        }
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setLearningUIState()
  {
    // enable or disable all controls based on whether we are in the process of learning
    _learnButton.setEnabled(_learningInProgress == false);
    _reLearnShortButton.setEnabled(_learningInProgress == false);
    _subtypeExposureLearningButton.setEnabled(_learningInProgress == false && Config.getInstance().getIntValue(SoftwareConfigEnum.MAXIMUM_SPEED_SELECTION) >= 2);
    setTemplateMatchUIState();//ShengChuan - Clear Tombstone
    setExposedPadVoidingImagesUIState();
    _subtypeMaskingButton.setEnabled(_learningInProgress == false);
    _deleteLearnedDataButton.setEnabled(_learningInProgress == false);
    _resetAlgorithmSettingsButton.setEnabled(_learningInProgress == false);
    _initialAlgorithmSettingsButton.setEnabled(_learningInProgress == false);
    _jointTypeComboBox.setEnabled(_learningInProgress == false);
    _subtypeComboBox.setEnabled(_learningInProgress == false);
    _boardSelectComboBox.setEnabled(_learningInProgress == false);
    _shortLearningCheckBox.setEnabled(_learningInProgress == false);
    _excludeLibrarySubtypeLearningCheckBox.setEnabled(_learningInProgress == false);
    _subtypeLearningCheckBox.setEnabled(_learningInProgress == false);
    _slopeLearningCheckBox.setEnabled(_learningInProgress == false && _subtypeLearningCheckBox.isSelected());
    _shortLearningDetailsButton.setEnabled(_learningInProgress == false);
    _subtypeLearningDetailsButton.setEnabled(_learningInProgress == false);
    _expectedImageLearningDetailsButton.setEnabled(_learningInProgress == false);
    _learnedBoardApplyToAllBoardCheckBox.setEnabled(_learningInProgress == false && 
                                                    _boardSelectComboBox.getSelectedIndex() != 0 &&
                                                    (_shortLearningCheckBox.isSelected() || _templateLearningCheckBox.isSelected()));
  }

  /**
   * @author Andy Mechtenberg
   */
  private void cancelButton_actionPerformed()
  {
    _learningMessagesTextArea.append("\n" + StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_ABORT_IN_PROGRESS_KEY") + "\n\n");
    _cancelButton.setEnabled(false);
    _statisticalTuningEngine.cancelLearning();
    _learningInProgress = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void displayError(XrayTesterException xte)
  {
    MessageDialog.showErrorDialog(_mainUI, xte.getLocalizedMessage(),
                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void deleteLearnedData_actionPerformed(ActionEvent e)
  {
    final StatisticalTuningEngine ste = StatisticalTuningEngine.getInstance();
    final JointTypeChoice currentFamilyChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    final SubtypeChoice currentSubtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();

    // first, we need to figure out if there's anything to delete.
    // We'll check the status of the two checkboxes first, if none checked, then nothing to delete
    // Then, we'll do a gross check of does any learned data exist at all
    // then, if a specific subtype was selected, make sure it exists in the learned list
//    if ((_subtypeLearningCheckBox.isSelected() == false) &&  (_shortLearningCheckBox.isSelected() == false))
//    {
//      JOptionPane.showMessageDialog(_mainUI,
//                                    StringLocalizer.keyToString("MMGUI_INITIAL_LEARNING_DELETE_NONE_STATUS_KEY"),
//                                    StringLocalizer.keyToString("MM_GUI_TDT_TITLE_INITIAL_TUNE_KEY"),
//                                    JOptionPane.INFORMATION_MESSAGE);
//      return;
//    }
    com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
    _beforeDeleteShortLearnedSubtypes = panel.getNumberOfInspectedSubtypesWithShortLearned();
    _beforeDeleteSubtypeLearnedSubtypes = panel.getNumberOfInspectedSubtypesWithAllAlgorithmsExceptShortLearned();
    _beforeDeleteExpectedImageLearnedSubtypes = panel.getNumberOfInspectedSubtypesWithExpectedImageLearned();
    _beforeDeleteImageTemplateLearnedSubtypes = panel.getNumberOfInspectedSubtypesWithImageTemplateLearned();

    boolean somethingToDelete = false;
    if (_subtypeLearningCheckBox.isSelected() && (_beforeDeleteSubtypeLearnedSubtypes != 0))
      somethingToDelete = true;
    if (_shortLearningCheckBox.isSelected() && (_beforeDeleteShortLearnedSubtypes != 0))
      somethingToDelete = true;
    if (_expectedImageLearningCheckBox.isSelected() && _expectedImageLearningCheckBoxAllowed && (_beforeDeleteExpectedImageLearnedSubtypes != 0))
      somethingToDelete = true;
    if(_templateLearningCheckBox.isSelected() && (_beforeDeleteImageTemplateLearnedSubtypes != 0))
      somethingToDelete = true;
    
    if (somethingToDelete == false)
    {
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString("MMGUI_INITIAL_LEARNING_DELETE_NONE_STATUS_KEY"),
                                    StringLocalizer.keyToString("MM_GUI_TDT_TITLE_INITIAL_TUNE_KEY"),
                                    JOptionPane.INFORMATION_MESSAGE);
      return;
    }


    // first, confirm to the user they want to delete
    int confirmAnswer = ChoiceInputDialog.showConfirmDialog(_mainUI,
                                                            StringLocalizer.keyToString("MMGUI_INITIAL_LEARNING_DELETE_CONFIRM_KEY"),
                                                            StringLocalizer.keyToString("MM_GUI_TDT_TITLE_INITIAL_TUNE_KEY"));
    if (confirmAnswer == JOptionPane.OK_OPTION)
    {
      _progressCancelDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                                 StringLocalizer.keyToString("MM_GUI_TDT_TITLE_INITIAL_TUNE_KEY"),
                                                 StringLocalizer.keyToString("MMGUI_INITIAL_TUNING_DELETE_IN_PROGRESS_KEY"),
                                                 "",
                                                 StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                                 StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                                 StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                                 0,
                                                 100,
                                                 true);

      _progressCancelDialog.addCancelActionListener(new java.awt.event.ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _abortingDelete = true;
          _statisticalTuningEngine.cancelDeletingLearnedData();
        }
      });
      _progressCancelDialog.pack();
      SwingUtils.centerOnComponent(_progressCancelDialog, _mainUI);
      _updatingShortsData = false;
      _swingWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            if (currentFamilyChoice.isAllFamilies()) // all families is the same as a board test
            {

              ste.deletePanelLearnedData(_project, _subtypeLearningCheckBox.isSelected(),
                                         _shortLearningCheckBox.isSelected(),
                                         (_expectedImageLearningCheckBox.isSelected() && _expectedImageLearningCheckBoxAllowed),
                                         _shortLearningCheckBox.isSelected(),//broken pin use short first
                                         _templateLearningCheckBox.isSelected());//ShengChuan - Clear Tombstone
            }
            else if (currentSubtypeChoice.isAllSubtypes()) // this must be jointType-all test
            {
              ste.deleteJointTypeLearnedData(_project, currentFamilyChoice.getJointTypeEnum(),
                                             _subtypeLearningCheckBox.isSelected(),
                                             _shortLearningCheckBox.isSelected(),
                                             (_expectedImageLearningCheckBox.isSelected() && _expectedImageLearningCheckBoxAllowed),
                                             _shortLearningCheckBox.isSelected(),//broken pin use short first
                                             _templateLearningCheckBox.isSelected());//ShengChuan - Clear Tombstone
            }
            else
            {
              ste.deleteSubtypeLearnedData(currentSubtypeChoice.getSubtype(), _subtypeLearningCheckBox.isSelected(),
                                           _shortLearningCheckBox.isSelected(),
                                           (_expectedImageLearningCheckBox.isSelected() && _expectedImageLearningCheckBoxAllowed),
                                           _shortLearningCheckBox.isSelected(),//broken pin use short first
                                           _templateLearningCheckBox.isSelected(),
                                           false);//ShengChuan - Clear Tombstone);
            }
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                displayError(xte);
              }
            });
          }
          finally
          {
            // wait until visible
            while (_progressCancelDialog.isVisible() == false)
            {
              try
              {
                Thread.sleep(200);
              }
              catch (InterruptedException ex)
              {
                // do nothing
              }
            }
            _progressCancelDialog.dispose();
            _progressCancelDialog = null;
            updateStatusAfterLearning();
            updateLearningStatusFields();
          }
        }
      });
      _progressCancelDialog.setVisible(true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateStatusAfterLearning()
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        String message = StringLocalizer.keyToString("MMGUI_INITIAL_LEARNING_DELETE_DATA_COMPLETE_KEY");
        if (_abortingDelete)
          message = StringLocalizer.keyToString("MMGUI_INITIAL_LEARNING_DELETE_ABORTED_KEY");
        JOptionPane.showMessageDialog(_mainUI,
                                      StringUtil.format(message, 50),
                                      StringLocalizer.keyToString("MM_GUI_TDT_TITLE_INITIAL_TUNE_KEY"),
                                      JOptionPane.INFORMATION_MESSAGE);
//        // now, display some status
//        com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
//        int updatedShortLearnedSubtypes = panel.getNumberOfInspectedSubtypesWithShortLearned();
//        int updatedSubtypeLearnedSubtypes = panel.getNumberOfInspectedSubtypesWithAllAlgorithmsExceptShortLearned();
//
//        int numberShortSubtypesDeleted = _beforeDeleteShortLearnedSubtypes - updatedShortLearnedSubtypes;
//        int numberSubtypeLearnedSubtypes = _beforeDeleteSubtypeLearnedSubtypes - updatedSubtypeLearnedSubtypes;
//
//        LocalizedString shortStatus = new LocalizedString("MMGUI_INITIAL_LEARNING_DELETE_SHORT_STATUS_KEY",
//            new Object[]
//            {new Integer(numberShortSubtypesDeleted)});
//        LocalizedString subtypeStatus = new LocalizedString("MMGUI_INITIAL_LEARNING_DELETE_SUBTYPE_STATUS_KEY",
//            new Object[]
//            {new Integer(numberSubtypeLearnedSubtypes)});
//        String statusMessage = "";
//        if (numberShortSubtypesDeleted > 0)
//          statusMessage += StringLocalizer.keyToString(shortStatus) + "\n";
//        if (numberSubtypeLearnedSubtypes > 0)
//          statusMessage += StringLocalizer.keyToString(subtypeStatus);
//
//        if (statusMessage.length() > 0)
//          JOptionPane.showMessageDialog(_mainUI,
//                                        statusMessage,
//                                        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_INITIAL_TUNE_KEY"),
//                                        JOptionPane.INFORMATION_MESSAGE);
//        else
//          JOptionPane.showMessageDialog(_mainUI,
//                                        StringLocalizer.keyToString("MMGUI_INITIAL_LEARNING_DELETE_NONE_STATUS_KEY"),
//                                        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_INITIAL_TUNE_KEY"),
//                                    JOptionPane.INFORMATION_MESSAGE);
      }
    });
  }


  /**
   * @author Andy Mechtenberg
   */
  private void jointTypeComboBox_actionPerformed(ActionEvent e)
  {
    populateSubtypeComboBox();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateSubtypeComboBox()
  {
    _subtypeComboBox.setPopupWidthToDefault();
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    if (jointTypeChoice == null)
       return;

    _panelDataUtil.populateComboBoxWithSubtypes(_project.getPanel(), jointTypeChoice, _subtypeComboBox);
    FontMetrics fontMetrics = _subtypeComboBox.getFontMetrics(_subtypeComboBox.getFont());
    int maxStringLength = 0;

    if (jointTypeChoice.isAllFamilies() == false)
    {
      java.util.List<Subtype> subtypes = _project.getPanel().getInspectedSubtypes(jointTypeChoice.getJointTypeEnum());

      for (Subtype subtype : subtypes)
      {
        int itemStringLength = fontMetrics.stringWidth(subtype.toString());
        if (maxStringLength < itemStringLength)
          maxStringLength = itemStringLength;
      }
      // set the popup width to the max of the strings only if that's longer than the box itself
      int w = _subtypeComboBox.getWidth();
      if ((maxStringLength + VariableWidthComboBox.widthExtension) <= _subtypeComboBox.getWidth())
        _subtypeComboBox.setPopupWidthToDefault();
      else
        _subtypeComboBox.setPopupWidth(maxStringLength + VariableWidthComboBox.widthExtension); // add a bit more so the last char is not RIGHT at the popup right side.
    }

    setTemplateMatchUIState();//ShengChuan - Clear Tombstone
    setExposedPadVoidingImagesUIState();
    setSinglePadVoidingImagesUIState();
    _subtypeComboBox.setMaximumRowCount(20);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void shortLearningDetailsButton_actionPerformed()
  {
    // bring up dialog with list of all untuned subtypes
    ProjectDetailsDialog dlg = new ProjectDetailsDialog(_mainUI,
                                                        StringLocalizer.keyToString("MMGUI_UNLEARNED_SHORT_BACKGROUND_REGIONS_KEY"),
                                                        _unlearnedShorts);
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    dlg.dispose();
  }

  /**
   * @author George Booth
   */
  private void expectedImageLearningDetailsButton_actionPerformed()
  {
    // bring up dialog with list of all untuned subtypes
    ProjectDetailsDialog dlg = new ProjectDetailsDialog(_mainUI,
                                                        StringLocalizer.keyToString("MMGUI_UNLEARNED_EXPECTED_IMAGES_KEY"),
                                                        _unlearnedExpectedImages);
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    dlg.dispose();
  }

  /**
   * @author sheng chuan
   */
  private void subtypesWithExposureSettingLearningDetailsButton_actionPerformed()
  {
    // bring up dialog with list of all untuned subtypes
    ProjectDetailsDialog dlg = new ProjectDetailsDialog(_mainUI,
                                                        StringLocalizer.keyToString("MMGUI_UNLEARNED_EXPECTED_IMAGES_KEY"),
                                                        _project.getPanel().getSubtypesWithoutExposureLearning());
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    dlg.dispose();
  }
  
  /**
   * @author Andy Mechtenberg
   */
  private void subtypeLearningDetailsButton_actionPerformed(ActionEvent e)
  {
    // bring up dialog with list of all untuned subtypes
    ProjectDetailsDialog dlg = new ProjectDetailsDialog(_mainUI,
                                                        StringLocalizer.keyToString("MMGUI_UNLEARNED_ALGORITHM_SETTINGS_KEY"),
                                                        _unlearnedSubtypes);
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    dlg.dispose();
  }

  /**
   * @param e
   * @author Wei Chin, Chong
   */
  private void subtypeImportedStatusDetailsButton_actionPerformed()
  {
    ProjectDetailsDialog dlg = new ProjectDetailsDialog(_mainUI,
                                                        StringLocalizer.keyToString("MMGUI_UNIMPORTED_SUBTYPES_KEY"),
                                                        _unImportedSubtypes);
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    dlg.dispose();
  }
  /**
   * Shows or hides controls related to Exposed Pad learning.  The controls are not
   * visible if there are no active Exposed Pad joints.  THis should be called after
   * the project data is updated during start().
   *
   * @author George Booth
   */
  private void showExpectedImageControls()
  {
    Assert.expect(_jointTypeComboBox != null);

    // only show expected image controls if an ExposedPad joint type is active
    int itemCount = _jointTypeComboBox.getItemCount();
    boolean usesExpectedImages = false;
    for (int i = 0; i < itemCount; i++)
    {
      String currentJointType = _jointTypeComboBox.getItemAt(i).toString();
      if (currentJointType.equalsIgnoreCase(JointTypeEnum.EXPOSED_PAD.getName()))
      {
        usesExpectedImages = true;
      }
    }

    if (usesExpectedImages)
    {
      // learning checkboxes
      _learningChoiceInnerPanel.removeAll();
      //_learningChoiceInnerGridLayout.setRows(6);
//      _learningChoiceInnerPanel.add(_learningSubtypeInnerPanel);
      _learningChoiceInnerPanel.add(_subtypeLearningCheckBox);
      _learningChoiceInnerPanel.add(_slopeLearningPanel);
      _learningChoiceInnerPanel.add(_shortLearningCheckBox);
      _learningChoiceInnerPanel.add(_expectedImageLearningCheckBox);
      _learningChoiceInnerPanel.add(_excludeLibrarySubtypeLearningCheckBox);
      _learningChoiceInnerPanel.add(_templateLearningCheckBox);//ShengChuan - Clear tombstone
      _learningChoiceInnerPanel.add(_learnedBoardApplyToAllBoardCheckBox);
      
      // learning buttons
      _learnButtonInnerPanel.removeAll();
      _learnButtonInnerPanelGridLayout.setRows(8);
//      _learnButtonInnerPanel.add(new JLabel(" "));
      _learnButtonInnerPanel.add(_learnButton);
     _learnButtonInnerPanel.add(_interactiveLearningButton);
      _learnButtonInnerPanel.add(_reLearnShortButton);
      _learnButtonInnerPanel.add(_deleteLearnedDataButton);
      _learnButtonInnerPanel.add(_subtypeMaskingButton);
      _learnButtonInnerPanel.add(_resetAlgorithmSettingsButton);
      _learnButtonInnerPanel.add(_initialAlgorithmSettingsButton);
      _learnButtonInnerPanel.add(_subtypeExposureLearningButton);
      // learning status labels
      _statusHeaderLabelPanel.removeAll();
      _statusHeaderLabelGridLayout.setRows(5);
      _statusHeaderLabelPanel.add(_subtypeLearningLabel);
      _statusHeaderLabelPanel.add(_shortLearningLabel);
      _statusHeaderLabelPanel.add(_expectedImageLearningLabel);
      _statusHeaderLabelPanel.add(_importedSubtypeLabel);
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ONLINE_WORKSTATION))
        _statusHeaderLabelPanel.add(_subtypeWithExposureLearningLabel);
      // learning status
      _statusLearningPanel.removeAll();
      _statusLearningGridLayout.setRows(5);
      _statusLearningPanel.add(_subtypeLearningStatusLabel, null);
      _statusLearningPanel.add(_shortsLearningStatusLabel, null);
      _statusLearningPanel.add(_expectedImageLearningStatusLabel, null);
      _statusLearningPanel.add(_importedSubtypeStatusLabel,null);
      _statusLearningPanel.add(_subtypeWithExposureLearningStatusLabel,null);
      // learning details
      _subypteLearningStatusOuterPanel.removeAll();
      _subtypeLearningStatusOuterBorderLayout.setRows(5);
      _subypteLearningStatusOuterPanel.add(_subtypeLearningDetailsPanel);
      _subypteLearningStatusOuterPanel.add(_shortLearningDetailsPanel);
      _subypteLearningStatusOuterPanel.add(_expectedImageLearningDetailsPanel);
      _subypteLearningStatusOuterPanel.add(_subtypeImportedStatusDetailsPanel);
      _subypteLearningStatusOuterPanel.add(_subtypeWithExposureLearningDetailsPanel);
      
    }
    else
    {
      // learning checkboxes
      _learningChoiceInnerPanel.removeAll();
      //_learningChoiceInnerGridLayout.setRows(5);
//      _learningChoiceInnerPanel.add(_learningSubtypeInnerPanel);
      _learningChoiceInnerPanel.add(_subtypeLearningCheckBox);
      _learningChoiceInnerPanel.add(_slopeLearningPanel);
      _learningChoiceInnerPanel.add(_shortLearningCheckBox);
      _learningChoiceInnerPanel.add(_excludeLibrarySubtypeLearningCheckBox);
      _learningChoiceInnerPanel.add(_templateLearningCheckBox);//ShengChuan - Clear tombstone
      _learningChoiceInnerPanel.add(_learnedBoardApplyToAllBoardCheckBox);
      
      // learning buttons
      _learnButtonInnerPanel.removeAll();
      _learnButtonInnerPanelGridLayout.setRows(7);
//      _learnButtonInnerPanel.add(new JLabel(" "));
      _learnButtonInnerPanel.add(_learnButton);
      _learnButtonInnerPanel.add(_reLearnShortButton);
      _learnButtonInnerPanel.add(_deleteLearnedDataButton);
      _learnButtonInnerPanel.add(_subtypeMaskingButton);
      _learnButtonInnerPanel.add(_resetAlgorithmSettingsButton);
      _learnButtonInnerPanel.add(_initialAlgorithmSettingsButton);
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ONLINE_WORKSTATION))
        _learnButtonInnerPanel.add(_subtypeExposureLearningButton);
      // learning status labels
      _statusHeaderLabelPanel.removeAll();
      _statusHeaderLabelGridLayout.setRows(4);
      _statusHeaderLabelPanel.add(_subtypeLearningLabel);
      _statusHeaderLabelPanel.add(_shortLearningLabel);
      _statusHeaderLabelPanel.add(_importedSubtypeLabel);
      _statusHeaderLabelPanel.add(_subtypeWithExposureLearningLabel);
      // learning status
      _statusLearningPanel.removeAll();
      _statusLearningGridLayout.setRows(4);
      _statusLearningPanel.add(_subtypeLearningStatusLabel, null);
      _statusLearningPanel.add(_shortsLearningStatusLabel, null);
      _statusLearningPanel.add(_importedSubtypeStatusLabel,null);
      _statusLearningPanel.add(_subtypeWithExposureLearningStatusLabel,null);
      // learning details
      _subypteLearningStatusOuterPanel.removeAll();
      _subtypeLearningStatusOuterBorderLayout.setRows(4);
      _subypteLearningStatusOuterPanel.add(_subtypeLearningDetailsPanel);
      _subypteLearningStatusOuterPanel.add(_shortLearningDetailsPanel);
      _subypteLearningStatusOuterPanel.add(_subtypeImportedStatusDetailsPanel);
      _subypteLearningStatusOuterPanel.add(_subtypeWithExposureLearningDetailsPanel);
      
    }
  }

  /**
   * Task is ready to start
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("InitialTuningPanel().start()");

    com.axi.util.TimerUtil screenTimer = new com.axi.util.TimerUtil();
    screenTimer.start();

    if (_populateProjectData)
    {
      populateWithProjectData();
    }
    _populateProjectData = false;

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();
//    _testDev.addBoardSelectInToolbar(_envToolBarRequesterID);
    _active = true;

    // add custom menus

    // add custom toolbar components

    // observables
    _testDev.switchToPanelGraphics();
    _testDev.showImageWindow();

    _inspectionEventObservable.addObserver(this);

    _guiPersistSettings = _testDev.getPersistance();
    _projectPersistSettings = _guiPersistSettings.getProjectPersistance();

    // apply persist settings if the project name is the same
    _subtypeLearningCheckBox.setSelected(_projectPersistSettings.getSubtypeLearningChecked());
    _slopeLearningCheckBox.setSelected(_projectPersistSettings.getSlopeLearningChecked());
    _shortLearningCheckBox.setSelected(_projectPersistSettings.getShortLearningChecked());
    _expectedImageLearningCheckBox.setSelected(_projectPersistSettings.getExpectedImageLearningChecked());
    //XCR-3480, Learn button is enable although none check box are checked
    updateButtonState();
    // Wei Chin, always set to true
    // Lee Herng 07 June 2010 - Default to false. This is requested by Jabil Shanghai.
    _excludeLibrarySubtypeLearningCheckBox.setSelected(false);
    if (_updateData)
      updateProjectData();
    _updateData = false;

    // add or remove Expected Image controls as needed
    showExpectedImageControls();

    if (_testDev.doesActiveJointTypeSubtypePanelBoardChoiceExist())
    {
      JointTypeChoice jointTypeChoice = _testDev.getActiveJointTypeChoice();
      SubtypeChoice subtypeChoice = _testDev.getActiveSubtypeChoice();
      _jointTypeComboBox.setSelectedItem(jointTypeChoice);
      _subtypeComboBox.setSelectedItem(subtypeChoice);
      
      if (_testDev.isBoardInstanceSelected())
      {
        Board activeBoard = _testDev.getActiveBoardChoice();
        _boardSelectComboBox.setSelectedItem(activeBoard);
        
        if(_shortLearningCheckBox.isSelected() == true || _templateLearningCheckBox.isSelected() == true)
        {
          //Kee Chin Seong - when is set to board level only apply that 
          _learnedBoardApplyToAllBoardCheckBox.setEnabled(true);
        }
        else
        {
          //Kee Chin Seong - when is set to board level only apply that 
          _learnedBoardApplyToAllBoardCheckBox.setEnabled(false);
        }
      }
      else
      {
        _boardSelectComboBox.setSelectedIndex(0);
        //Panel level is not allow apply to all board profile
        _learnedBoardApplyToAllBoardCheckBox.setEnabled(false);
      }
    }
    else
    {
      String jointType = _projectPersistSettings.getInitialTuningSelectedJointType();
      String subtype = _projectPersistSettings.getInitialTuningSelectedSubtype();
      String board = _projectPersistSettings.getInitialTuningSelectedPanelBoard();

      int jointTypeCount = _jointTypeComboBox.getItemCount();
      for (int i = 0; i < jointTypeCount; i++)
      {
        String currentJointType = _jointTypeComboBox.getItemAt(i).toString();
        if (currentJointType.equalsIgnoreCase(jointType))
          _jointTypeComboBox.setSelectedIndex(i);
      }

      int subtypeCount = _subtypeComboBox.getItemCount();
      for (int i = 0; i < subtypeCount; i++)
      {
        String currentSubtype = _subtypeComboBox.getItemAt(i).toString();
        if (currentSubtype.equalsIgnoreCase(subtype))
          _subtypeComboBox.setSelectedIndex(i);
      }
      
      int boardCount = _boardSelectComboBox.getItemCount();
      for (int i = 0; i < boardCount; i++)
      {
        String currentBoard = _boardSelectComboBox.getItemAt(i).toString();
        if (currentBoard.equalsIgnoreCase(board))
          _boardSelectComboBox.setSelectedIndex(i);
      }
    }
    screenTimer.stop();
//    System.out.println("  Total Loading for Initial Tuning: (in mils): "+ screenTimer.getElapsedTimeInMillis());
  }

  /**
   * Can the environment be swapped out
   * @return true if environment can be swapped
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
//    System.out.println("InitialTuningPanel().isReadyToFinish()");
    if (_learningInProgress)
    {
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString("GUI_TEST_RUNNING_ON_EXIT_KEY"),
                                    StringLocalizer.keyToString("MM_GUI_TDT_TITLE_INITIAL_TUNE_KEY"),
                                    JOptionPane.ERROR_MESSAGE);

      return false;
    }
    else
      return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void saveUIState()
  {
    if (_projectPersistSettings != null)
    {
      _projectPersistSettings.setInitialTuningSelectedJointType(_jointTypeComboBox.getSelectedItem().toString());
      _projectPersistSettings.setInitialTuningSelectedSubtype(_subtypeComboBox.getSelectedItem().toString());
      _projectPersistSettings.setInitialTuningSelectedPanelBoard(_boardSelectComboBox.getSelectedItem().toString());
      _projectPersistSettings.setShortLearningChecked(_shortLearningCheckBox.isSelected());
      _projectPersistSettings.setSubtypeLearningChecked(_subtypeLearningCheckBox.isSelected());
      _projectPersistSettings.setSlopeLearningChecked(_slopeLearningCheckBox.isSelected());
      _projectPersistSettings.setExpectedImageLearningChecked(_expectedImageLearningCheckBox.isSelected());
    }
  }

  /**
   * The environment is about to be swapped out and should perform cleanup
   * @author George Booth
   */
  public void finish()
  {
//    System.out.println("InitialTuningPanel().finish()");

    // observables
    _inspectionEventObservable.deleteObserver(this);
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
    saveUIState();
    JointTypeChoice currentJointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    SubtypeChoice currentSubtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    _testDev.setActiveTunerSubtype(currentJointTypeChoice, currentSubtypeChoice, _boardSelectComboBox.getSelectedItem());
    _active = false;
  }

  /**
   * @author Tan Hock Zoon
   */
  private void setExposedPadVoidingImagesUIState()
  {
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();

    boolean imageSetsSelected = (_selectedImageSets.isEmpty() == false);

    if (jointTypeChoice.isAllFamilies() == true || jointTypeChoice.getJointTypeEnum().equals(JointTypeEnum.EXPOSED_PAD) == true)
    {
      _expectedImageLearningCheckBox.setEnabled(true && _learningInProgress == false);
      _expectedImageLearningCheckBoxAllowed = true;
      _interactiveLearningButton.setEnabled(imageSetsSelected && _expectedImageLearningCheckBox.isSelected() && _learningInProgress == false);
    }
    else
    {
      _expectedImageLearningCheckBox.setEnabled(false);
      _expectedImageLearningCheckBoxAllowed = false;
      _interactiveLearningButton.setEnabled(false);
    }
  }
  
  /**
   * Clear Tombstone
   * @author sheng chuan
   */
  private void setTemplateMatchUIState()
  {
    final SubtypeChoice currentSubtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();     
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    
    //this to handle state changes from other panel when user select image set from panel other than initial tuning panel
    if(jointTypeChoice == null || currentSubtypeChoice  == null)
      return ;
    
    boolean imageSetsSelected = (_selectedImageSets.isEmpty() == false);
    boolean hasImageTemplateEnabled = false;
    if(currentSubtypeChoice.isAllSubtypes())
    {
      java.util.List<Subtype> subtypes = new ArrayList<Subtype>();
      if(jointTypeChoice.isAllFamilies())
      {
        subtypes.addAll(_testDev.getCurrentBoardType().getInspectedSubtypes(JointTypeEnum.CAPACITOR));
        subtypes.addAll(_testDev.getCurrentBoardType().getInspectedSubtypes(JointTypeEnum.RESISTOR));
      }
      else
      {
        if(jointTypeChoice.getJointTypeEnum().equals(JointTypeEnum.CAPACITOR) || jointTypeChoice.getJointTypeEnum().equals(JointTypeEnum.RESISTOR))
          subtypes.addAll(_testDev.getCurrentBoardType().getInspectedSubtypes(jointTypeChoice.getJointTypeEnum()));
      }
      
      for (Subtype subtype : subtypes)
      {
        if(subtype.useTemplateMatch())
        {
          hasImageTemplateEnabled = true;
          break;
        }
      }
    }
    else
    {
      hasImageTemplateEnabled = currentSubtypeChoice.getSubtype().useTemplateMatch();
    }
    
    if (hasImageTemplateEnabled && (jointTypeChoice.isAllFamilies() || jointTypeChoice.getJointTypeEnum().equals(JointTypeEnum.CAPACITOR) == true || jointTypeChoice.getJointTypeEnum().equals(JointTypeEnum.RESISTOR) == true))
    {
      _templateLearningCheckBox.setEnabled(imageSetsSelected && _learningInProgress == false);
    }
    else
    {
      _templateLearningCheckBox.setSelected(false);
      _templateLearningCheckBox.setEnabled(false);
    }
  }
  
   /**
   * @author Jack Hwee
   */
  private void setSinglePadVoidingImagesUIState()
  {
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();

    boolean imageSetsSelected = (_selectedImageSets.isEmpty() == false);
    
    if (jointTypeChoice.isAllFamilies() == false)
    {
  //      if (jointTypeChoice.isAllFamilies() == true || jointTypeChoice.getJointTypeEnum().equals(JointTypeEnum.SINGLE_PAD) == true )
        if (jointTypeChoice.getJointTypeEnum().equals(JointTypeEnum.SINGLE_PAD) == true || 
            jointTypeChoice.getJointTypeEnum().equals(JointTypeEnum.LGA) == true )
        {
          _subtypeMaskingButton.setEnabled(imageSetsSelected);
        }
        else
        {
          _subtypeMaskingButton.setEnabled(false);
        } 
    
   }
   else
    {
        _subtypeMaskingButton.setEnabled(false);
    }
   
  }
  
   /**
   * @author Jack Hwee
   */
  private void subtypeMaskingButton_actionPerformed()
  {
    JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
   
    try
    {
      MaskImageCompatibilityCheck.getInstance().checkMaskImageResolution(_project, _selectedImageSets);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
              ex,
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
              true);
    }

    MaskImageDlg dlg = null;

    if (subtypeChoice.isAllSubtypes())
    {
      subtypeChoice = (SubtypeChoice) _subtypeComboBox.getItemAt(1);

      dlg = new MaskImageDlg(_mainUI,
              StringLocalizer.keyToString("ATGUI_THRESHOLD_MASK_IMAGE_KEY"),
              true,
              _project,
              // jointTypeChoice.getJointTypeEnum(),
              subtypeChoice.getSubtype(),
              _selectedImageSets);
    }
    else
    {
      dlg = new MaskImageDlg(_mainUI,
              StringLocalizer.keyToString("ATGUI_THRESHOLD_MASK_IMAGE_KEY"),
              true,
              _project,
              // jointTypeChoice.getJointTypeEnum(),
              subtypeChoice.getSubtype(),
              _selectedImageSets);

    }
   
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setModal(true);
    dlg.setVisible(true);
    MaskImageCompatibilityCheck.getInstance().clearReconstructedImages();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void resetAlgorithmSettingsButton_actionPerformed()
  {
    //Siew Yeng - XCR-2594 - Learned Slope Settings fail to reset
    int answer = JOptionPane.showConfirmDialog(_mainUI,
                                               StringUtil.format(StringLocalizer.keyToString("MM_GUI_RESET_ALGORITHM_SETTINGS_WARNING_KEY"), 53),
                                               StringLocalizer.keyToString("ATGUI_RESET_ALGORITHM_DIALOG_TITLE_KEY"),
                                               JOptionPane.YES_NO_OPTION,
                                               JOptionPane.WARNING_MESSAGE);
    
    if(answer == JOptionPane.NO_OPTION || answer == JOptionPane.CLOSED_OPTION)
      return;
    
    final SubtypeChoice subtypeChoice = (SubtypeChoice)_subtypeComboBox.getSelectedItem();
    final JointTypeChoice jointTypeChoice = (JointTypeChoice)_jointTypeComboBox.getSelectedItem();
    
    if (subtypeChoice == null)
    {
      JOptionPane.showMessageDialog(_mainUI, StringLocalizer.keyToString("ATGUI_UNABLE_TO_COPY_THRESHOLDS_INCOMPLETE_DATA_KEY"),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"), JOptionPane.ERROR_MESSAGE);
      return;
    }
    
    String jointTypeName = null;
    String subtypeName = null;
    if (jointTypeChoice.isAllFamilies())
      jointTypeName = StringLocalizer.keyToString("ATGUI_ALL_KEY");
    else
      jointTypeName = jointTypeChoice.getJointTypeEnum().getName();
    
    if (subtypeChoice.isAllSubtypes())
      subtypeName = StringLocalizer.keyToString("ATGUI_ALL_KEY");
    else
      subtypeName = subtypeChoice.getSubtype().toString();
    
    LocalizedString msg = new LocalizedString("ATGUI_RESET_ALGORITHM_SETTINGS_PROGRESS_MESSAGE_KEY",
                                              new Object[]{jointTypeName, subtypeName});
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
                                                             StringLocalizer.keyToString(msg),
                                                             StringLocalizer.keyToString("ATGUI_RESET_ALGORITHM_DIALOG_TITLE_KEY"),
                                                             true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (jointTypeChoice.isAllFamilies())
          {
            java.util.List<JointTypeEnum> allJointTypes = _testDev.getCurrentBoardType().getInspectedJointTypeEnums();
            for(JointTypeEnum jointTypeEnum : allJointTypes)
            {
              java.util.List<Subtype> subtypes = _testDev.getCurrentBoardType().getInspectedSubtypes(jointTypeEnum);
              for (Subtype subtype : subtypes)
              {
                //Siew Yeng - XCR-2594 - Learned Slope Settings fail to reset
                subtype.setEnabledLearnSlopeSettings(_subtypeLearningCheckBox.isSelected() && _slopeLearningCheckBox.isSelected());
                subtype.setLearnedSettingsToDefaults();
              }
            }
          }
          else if (subtypeChoice.isAllSubtypes())
          {
            java.util.List<Subtype> subtypes = _testDev.getCurrentBoardType().getInspectedSubtypes(jointTypeChoice.getJointTypeEnum());
            for(Subtype subtype : subtypes)
            {
              //Siew Yeng - XCR-2594 - Learned Slope Settings fail to reset
              subtype.setEnabledLearnSlopeSettings(_subtypeLearningCheckBox.isSelected() && _slopeLearningCheckBox.isSelected());
              subtype.setLearnedSettingsToDefaults();
            }
          }
          else
          {
            Subtype subtype = subtypeChoice.getSubtype();
            //Siew Yeng - XCR-2594 - Learned Slope Settings fail to reset
            subtype.setEnabledLearnSlopeSettings(_subtypeLearningCheckBox.isSelected() && _slopeLearningCheckBox.isSelected());
            subtype.setLearnedSettingsToDefaults();
          }
        }
        finally
        {
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.dispose();
        }
      }
    });
    busyCancelDialog.setVisible(true);
  }

  /**
   * XCR-2894: Reset to initial threshold feature
   * @author Jen Ping Chong
   */
  private void initialAlgorithmSettingsButton_actionPerformed()
  {
    LocalizedString warningMsg = new LocalizedString("MM_GUI_RESET_TO_INITIAL_THRESHOLDS_WARNING_KEY",
                                                      new Object[]{_project.getInitialThresholdsSetName()});
    int answer = JOptionPane.showConfirmDialog(_mainUI,
                                               StringUtil.format(StringLocalizer.keyToString(warningMsg), 53),
                                               StringLocalizer.keyToString("ATGUI_RESET_TO_INITIAL_THRESHOLDS_DIALOG_TITLE_KEY"),
                                               JOptionPane.YES_NO_OPTION,
                                               JOptionPane.WARNING_MESSAGE);
    
    if (answer == JOptionPane.NO_OPTION || answer == JOptionPane.CLOSED_OPTION)
      return;
    
    final SubtypeChoice subtypeChoice = (SubtypeChoice) _subtypeComboBox.getSelectedItem();
    final JointTypeChoice jointTypeChoice = (JointTypeChoice) _jointTypeComboBox.getSelectedItem();

    if (subtypeChoice == null)
    {
      JOptionPane.showMessageDialog(_mainUI, StringLocalizer.keyToString("ATGUI_UNABLE_TO_COPY_THRESHOLDS_INCOMPLETE_DATA_KEY"),
                                    StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"), JOptionPane.ERROR_MESSAGE);
      return;
    }
    
    LocalizedString progressMsg = new LocalizedString("ATGUI_RESET_TO_INITIAL_THRESHOLDS_PROGRESS_MESSAGE_KEY",
                                              new Object[]{_project.getInitialThresholdsSetName()});
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(_mainUI,
                                                                   StringLocalizer.keyToString(progressMsg),
                                                                   StringLocalizer.keyToString("ATGUI_RESET_TO_INITIAL_THRESHOLDS_DIALOG_TITLE_KEY"),
                                                                   true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, _mainUI);
    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (jointTypeChoice.isAllFamilies())
          {
            java.util.List<JointTypeEnum> allJointTypes = _testDev.getCurrentBoardType().getInspectedJointTypeEnums();
            for (JointTypeEnum jointTypeEnum : allJointTypes)
            {
              java.util.List<Subtype> subtypes = _testDev.getCurrentBoardType().getInspectedSubtypes(jointTypeEnum);
              for (Subtype subtype : subtypes)
                subtype.resetToInitialThreshold();
            }
          }
          else if (subtypeChoice.isAllSubtypes())
          {
            java.util.List<Subtype> subtypes = _testDev.getCurrentBoardType().getInspectedSubtypes(jointTypeChoice.getJointTypeEnum());
            for (Subtype subtype : subtypes)
              subtype.resetToInitialThreshold();
          }
          else
          {
            Subtype subtype = subtypeChoice.getSubtype();
            subtype.resetToInitialThreshold();
          }
        }
        finally
        {
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.dispose();
        }
      }
    });
    busyCancelDialog.setVisible(true);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void populateBoardComboBox()
  {
    _boardSelectComboBox.removeAllItems();

    _boardSelectComboBox.addItem(StringLocalizer.keyToString("ATGUI_PANEL_KEY"));
    
    BoardType currentBoardType = _testDev.getCurrentBoardType();

    java.util.List<Board> boardInstances = _project.getPanel().getBoards();
    // if only one board instance, no need to put it in the boardinstance combobox
    // as the panel entry is enough
    if (boardInstances.size() > 1)
    {
      // sort boards based on name
      Collections.sort(boardInstances, new AlphaNumericComparator());
      for (Board board : boardInstances)
      {
        if (board.getBoardType().equals(currentBoardType))
          _boardSelectComboBox.addItem(board);
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private boolean isBoardInstanceSelected()
  {
    Object selection = _boardSelectComboBox.getSelectedItem();
    if (selection instanceof Board)
      return true;
    else
      return false;
  }

  /**
   * @author Cheah Lee Herng 
   */
  private Board getSelectedBoardInstance()
  {
    Object selection = _boardSelectComboBox.getSelectedItem();
    Assert.expect(selection instanceof Board);
    return (Board)selection;
  }
}
