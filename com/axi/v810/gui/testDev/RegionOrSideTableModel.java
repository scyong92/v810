package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 *
 * <p>Title: RegionOrSideTableModel</p>
 *
 * <p>Description:  This class is the table model for the region and side table in Verify CAD.
 * This table model is responsible for getting the data as needed. There will be 2 columns:<br>
 * Region<br>
 * Side<br>
 *
 * Regions are not support in version 2
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class RegionOrSideTableModel extends DefaultSortTableModel
{
  private final String[] _regionOrSideTableColumnNames = {"Region","Side"};
  private final String[] _fakePackages ={"A1","A2","A3","A4","A5","A6","A7","A8","A9","A10",
    "B1","B2","B3","B4","B5","B6","B7","B8","B9","B10",
    "C1","C2","C3","C4","C5","C6","C7","C8","C9","C10",
    "D1","D2","D3","D4","D5","D6","D7","D8","D9","D10"};
  private List<String> _regions = new LinkedList<String>();
  private Set<Component> _selectedComponents;

  // default to sort by component, ascending
  private int _lastSortColumn = 0;
  private boolean _lastSortWasAscending = true;

  /**
   * @author George Booth
   */
  public RegionOrSideTableModel()
  {
    // do nothing
  }

  void setDefaultSortOrder()
  {
    _lastSortColumn = 0;
    _lastSortWasAscending = true;
  }

  /**
   * @author George Booth
   */
  public void sortColumn(int column, boolean ascending)
  {
    _lastSortColumn = column;
    _lastSortWasAscending = ascending;

//    if(column == 0) // region name
//      Collections.sort(_regions, new RegionSideComparator(ascending, RegionSideComparatorEnum.REGION));
//    else  // side
//      Collections.sort(_regions, new RegionSideComparator(ascending, RegionSideComparatorEnum.SIDE));
  }

  /**
   * @author George Booth
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    if( columnIndex == 0)
      return _fakePackages[rowIndex];
    else
      return "TOP";
  }

  /**
   * @author George Booth
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_regionOrSideTableColumnNames[columnIndex];
  }

  /**
   * @author George Booth
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    // Implement the javax.swing.table.TableModel abstract method
 }

  /**
   * @author George Booth
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return false;
  }

  /**
   * @author George Booth
   */
  public int getRowCount()
  {
    if(_regions == null)
      return 0;
    else
      return _regions.size();
  }

  /**
   * @author George Booth
   */
  public int getColumnCount()
  {
    return _regionOrSideTableColumnNames.length;
  }

  /**
   * @author George Booth
   */
  void clear()
  {
    if (_regions != null)
      _regions.clear();
  }

  /**
   * @author George Booth
  */
  public void populateWithPanelData(BoardType selectedBoardType, String selectedBoardSide)
  {
    _regions = new LinkedList<String>(Arrays.asList(_fakePackages));
    Assert.expect(selectedBoardType != null);
    Assert.expect(selectedBoardSide != null);

//    Collections.sort( _regions, new AlphaNumericComparator() );
    sortColumn(_lastSortColumn, _lastSortWasAscending);
    fireTableDataChanged();
  }

  /**
   * @author George Booth
   */
  public String getRegionAt(int selectedRegionNumber)
  {
    Assert.expect(selectedRegionNumber < _regions.size());
    return _regions.get(selectedRegionNumber);
  }

  /**
   * @author George Booth
   */
  public int getRowForRegion(String region)
  {
    Assert.expect(region != null);
    for (int row = 0; row < _regions.size(); row++)
    {
      String thisRegion = _regions.get(row);
      if (thisRegion.equals(region))
      {
        return row;
      }
    }
    Assert.expect(false, "Region not found");
    return 0;
  }

  /**
   * Get the row for a specified compPackage. If the compPackage is null or
   * the compPackage is not found, default to select the first row by default.
   * @return row for compPackage or 0 if not found
   * @author George Booth
   */
  int getRowForCompPackage(CompPackage compPackage)
  {
    Assert.expect(_regions.size() > 0);
    // we can always select row 0
    if (compPackage == null)
    {
      return 0;
    }
    int row = 0; //_regions.indexOf(compPackage);
    if (row > -1)
    {
      return row;
    }
    else
    {
      // not found
      return 0;
    }
  }

  /**
   * @author George Booth
   */
  public void setSelectedComponents(Set<Component> selectedComponents)
  {
    Assert.expect(selectedComponents != null);
    _selectedComponents = selectedComponents;
  }

  /**
   * @author George Booth
   */
  public void clearSelectedComponents()
  {
    if (_selectedComponents != null)
      _selectedComponents.clear();
  }

}
