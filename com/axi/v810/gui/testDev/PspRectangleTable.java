package com.axi.v810.gui.testDev;

import java.util.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * @author Cheah Lee Herng
 */
public class PspRectangleTable extends JSortTable 
{
  public static final int _OPTICAL_RECTANGLE_INDEX = 0;
  public static final int _OPTICAL_RECTANGLE_NAME = 1;
  public static final int _OPTICAL_RECTANGLE_USABILITY_STATUS  = 2;
  
  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _OPTICAL_RECTANGLE_INDEX_COLUMN_WIDTH_PERCENTAGE = .10;
  private static final double _OPTICAL_RECTANGLE_NAME_COLUMN_WIDTH_PERCENTAGE = .40;
  private static final double _OPTICAL_RECTANGLE_USABILITY_STATUS_COLUMN_WIDTH_PERCENTAGE = .10;
  
  private NumericRenderer _numericRenderer = new NumericRenderer(2, JLabel.RIGHT);
  private NumericEditor _xNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.RIGHT, 0, 0, Double.MAX_VALUE);
  private NumericEditor _yNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.RIGHT, 0, 0, Double.MAX_VALUE);
  
  // Ying-Huan.Chu
  private int[] _selectedRows;
  
  /**
   * @author Cheah Lee Herng
   */
  public PspRectangleTable()
  {
    // Do nothing
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int boardNameColumnWidth      = (int)(totalColumnWidth * _OPTICAL_RECTANGLE_INDEX_COLUMN_WIDTH_PERCENTAGE);
    int boardTypeColumnWidth      = (int)(totalColumnWidth * _OPTICAL_RECTANGLE_NAME_COLUMN_WIDTH_PERCENTAGE);
    int usabilityStatusColumnWidth      = (int)(totalColumnWidth * _OPTICAL_RECTANGLE_USABILITY_STATUS_COLUMN_WIDTH_PERCENTAGE);
   
    columnModel.getColumn(_OPTICAL_RECTANGLE_INDEX).setPreferredWidth(boardNameColumnWidth);
    columnModel.getColumn(_OPTICAL_RECTANGLE_NAME).setPreferredWidth(boardTypeColumnWidth);
    columnModel.getColumn(_OPTICAL_RECTANGLE_USABILITY_STATUS).setPreferredWidth(usabilityStatusColumnWidth);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };
    columnModel.getColumn(_OPTICAL_RECTANGLE_INDEX).setCellEditor(stringEditor);
    columnModel.getColumn(_OPTICAL_RECTANGLE_NAME).setCellEditor(stringEditor);
    columnModel.getColumn(_OPTICAL_RECTANGLE_USABILITY_STATUS).setCellEditor(stringEditor);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setDecimalPlacesToDisplay(int decimalPlaces)
  {
    Assert.expect(decimalPlaces >= 0);

    _xNumericEditor.setDecimalPlaces(decimalPlaces);
    _yNumericEditor.setDecimalPlaces(decimalPlaces);
    _numericRenderer.setDecimalPlacesToDisplay(decimalPlaces);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setSelectedRow(int row)
  {
    Assert.expect(row >= 0);
    
    int[] selectedRows = new int[1];
    selectedRows[0] = row;
    _selectedRows = selectedRows;
    this.setRowSelectionInterval(_selectedRows[0], _selectedRows[0]);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setSelectedRows(int[] rows)
  {
    Assert.expect(rows != null);
    Assert.expect(rows.length > 0);
    
    _selectedRows = rows;
    this.setRowSelectionInterval(_selectedRows[0], _selectedRows[0]);
    for (int i = 1; i < _selectedRows.length; ++i)
    {
      this.addRowSelectionInterval(_selectedRows[i], _selectedRows[i]);
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int[] getPreviousSelectedRows()
  {
    Assert.expect(_selectedRows != null);
    Assert.expect(_selectedRows.length > 0);
    
    return _selectedRows;
  }
}
