package com.axi.v810.gui.testDev;

import java.awt.event.*;
import java.text.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * This dialog allows user to customize alignment method
 * @author Chong, Wei Chin
 */
public class AlignmentCustomizationDialog extends JDialog
{
  private final String ALL = "ALL";
  private boolean _ok = false;

  private java.awt.BorderLayout _mainBorderLayout = new java.awt.BorderLayout();
  private JPanel _selectPanel = new JPanel();
  private JPanel _okCancelPanel = new JPanel();
  private JPanel _okCancelInnerPanel = new JPanel();
  private java.awt.GridLayout _okCancelInnerGridLayout = new java.awt.GridLayout();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private java.awt.FlowLayout _okCanelFowLayout = new java.awt.FlowLayout();

  private PairLayout _selectPanelPairLayout = new PairLayout(10, 10, false);
  private JLabel _boardNumberLabel = new JLabel();
  private JLabel _panelRegionLabel = new JLabel();
  private JLabel _rfpLabel = new JLabel();
  private JLabel _rfpTopLabel = new JLabel();
  private JLabel _rfpBottomLabel = new JLabel();
  private JLabel _integrationLevelLabel = new JLabel();
  private JLabel _userGainLabel = new JLabel();
  private JLabel _stageSpeedLabel = new JLabel();
  private JLabel _magnificationTypeLabel = new JLabel();

  private JCheckBox _rfpCheckBox = new JCheckBox();

  private JComboBox _boardNumberComboBox = new JComboBox();
  private JComboBox _panelRegionComboBox = new JComboBox();
  private JComboBox _integrationLevelComboBox = new JComboBox();
  private JComboBox _userGainComboBox = new JComboBox();
  private JComboBox _stageSpeedComboBox = new JComboBox();
  private JComboBox _magnificationTypeComboBox = new JComboBox();

  private NumericTextField _rfpTopTextField = new NumericTextField();
  private NumericTextField _rfpBottomTextField = new NumericTextField();
  private NumericRangePlainDocument _rfpTopZheightDocument = new NumericRangePlainDocument();
  private NumericRangePlainDocument _rfpBottomZheightDocument = new NumericRangePlainDocument();

  private Panel _panel = null;
  private java.awt.Frame _myFrame = null;
  
  /**
   * @author Chong, Wei Chin
   */
  AlignmentCustomizationDialog(java.awt.Frame frame, String title, boolean modal)
  {
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);

    _myFrame = frame;
    jbInit();

    pack();
  }

  /**
   * @author Chong, Wei Chin
   */
  private void jbInit()
  {
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    getContentPane().setLayout(_mainBorderLayout);

    //_panelRegionComboBox.addItem(ALL);
    _boardNumberComboBox.addItem(ALL);
    _magnificationTypeComboBox.addItem(ALL);
    
    _rfpCheckBox.addItemListener(new java.awt.event.ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if(e.getStateChange() == ItemEvent.DESELECTED)
        {
          _rfpTopTextField.setEditable(false);
          _rfpBottomTextField.setEditable(false);
        }
        else
        {
          _rfpTopTextField.setEditable(true);
          _rfpBottomTextField.setEditable(true);
        }
      }
    });

    _rfpTopZheightDocument.setRange(new DoubleRange(Integer.MIN_VALUE/MathUtil.NANOMETERS_PER_MIL , Integer.MAX_VALUE/MathUtil.NANOMETERS_PER_MIL));
    _rfpBottomZheightDocument.setRange(new DoubleRange(Integer.MIN_VALUE/MathUtil.NANOMETERS_PER_MIL , Integer.MAX_VALUE/MathUtil.NANOMETERS_PER_MIL));
    _rfpTopTextField.setDocument(_rfpTopZheightDocument);
    _rfpBottomTextField.setDocument(_rfpBottomZheightDocument);
    
    _okCancelInnerPanel.setLayout(_okCancelInnerGridLayout);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });

    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });

    for(SignalCompensationEnum signalCompensation: SignalCompensationEnum.getOrderedSignalCompensations())
    {
      _integrationLevelComboBox.addItem(signalCompensation);
    }

    for(UserGainEnum userGain: UserGainEnum.getOrderedUserGain())
    {
      _userGainComboBox.addItem(userGain);
    }
    
    for(StageSpeedEnum stageSpeed: StageSpeedEnum.getOrderedStageSpeed())
    {
      _stageSpeedComboBox.addItem(stageSpeed);
    }
    
    for(MagnificationTypeEnum magnificationType: MagnificationTypeEnum.getMagnificationToEnumMap().values())
    {
      _magnificationTypeComboBox.addItem(magnificationType);
    }

    _okCancelPanel.setLayout(_okCanelFowLayout);
    _okCanelFowLayout.setAlignment(java.awt.FlowLayout.RIGHT);
    _okCancelInnerGridLayout.setHgap(20);
    _selectPanel.setLayout(_selectPanelPairLayout);
    _selectPanel.setBorder(BorderFactory.createEmptyBorder(10,10,20,10));
    _okCancelPanel.setBorder(BorderFactory.createEmptyBorder(0,0,5,5));
    getContentPane().add(_selectPanel, java.awt.BorderLayout.CENTER);

    _boardNumberLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _boardNumberLabel.setText(StringLocalizer.keyToString("MMGUI_ALIGN_BOARD_NUMBER_KEY"));
    _selectPanel.add(_boardNumberLabel);
    _selectPanel.add(_boardNumberComboBox);

    _panelRegionLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _panelRegionLabel.setText(StringLocalizer.keyToString("MMGUI_ALIGN_PANEL_REGION_KEY"));
    _selectPanel.add(_panelRegionLabel);
    _selectPanel.add(_panelRegionComboBox);

    _rfpLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _rfpLabel.setText(StringLocalizer.keyToString("MMGUI_ALIGN_RFP_OFFSET_KEY"));
    _selectPanel.add(_rfpLabel);
    _selectPanel.add(_rfpCheckBox);

    _rfpTopLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _rfpTopLabel.setText(StringLocalizer.keyToString("MMGUI_ALIGN_RFP_TOP_OFFSET_KEY"));
    _rfpTopTextField.setColumns(7);
    _rfpTopTextField.setEditable(false);
    _selectPanel.add(_rfpTopLabel);
    _selectPanel.add(_rfpTopTextField);

    _rfpBottomLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _rfpBottomLabel.setText(StringLocalizer.keyToString("MMGUI_ALIGN_RFP_BOTTOM_OFFSET_KEY"));
    _rfpBottomTextField.setColumns(7);
    _rfpBottomTextField.setEditable(false);
    _selectPanel.add(_rfpBottomLabel);
    _selectPanel.add(_rfpBottomTextField);

    _integrationLevelLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _integrationLevelLabel.setText(StringLocalizer.keyToString("MMGUI_ALIGN_INTEGRATION_LEVEL_KEY"));
    _selectPanel.add(_integrationLevelLabel);
    _selectPanel.add(_integrationLevelComboBox);

    _userGainLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _userGainLabel.setText(StringLocalizer.keyToString("MMGUI_ALIGN_USER_GAIN_KEY"));
    _selectPanel.add(_userGainLabel);
    _selectPanel.add(_userGainComboBox);
    
    _stageSpeedLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _stageSpeedLabel.setText(StringLocalizer.keyToString("MMGUI_ALIGN_STAGE_SPEED_KEY"));
    _selectPanel.add(_stageSpeedLabel);
    _selectPanel.add(_stageSpeedComboBox);
    
// Todo: Move to Localization
    _magnificationTypeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _magnificationTypeLabel.setText("Magnification Type");
    _selectPanel.add(_magnificationTypeLabel);
    _selectPanel.add(_magnificationTypeComboBox);

    getContentPane().add(_okCancelPanel, java.awt.BorderLayout.SOUTH);
    _okCancelPanel.add(_okCancelInnerPanel, null);
    _okCancelInnerPanel.add(_okButton, null);
    _okCancelInnerPanel.add(_cancelButton, null);

    getRootPane().setDefaultButton(_okButton);
  }

  /**
   * @param panel
   * @param topOffset
   * @param bottomOffset
   * @param signalCompensation
   * @param userGain
   *
   * @author Chong, Wei Chin
   */
  public void populateData(Panel panel,
                           boolean useRFP,
                           int topOffset,
                           int bottomOffset,
                           SignalCompensationEnum signalCompensation,
                           UserGainEnum userGain,
                           StageSpeedEnum stageSpeed,
                           MagnificationTypeEnum magnification)
  {
    Assert.expect(panel != null);
    
    
    _panel = panel;
    if(_panel.getPanelSettings().isPanelBasedAlignment())
    {
      _selectPanel.remove(_boardNumberLabel);
      _selectPanel.remove(_boardNumberComboBox);
      if(_panel.getPanelSettings().isLongPanel())
      {
        boolean hasRightTestSubProgram = false;
        boolean hasLeftTestSubProgram = false;
        
        // XCR1722 by Cheah Lee Herng 24 Oct 2013 - Add PanelInLocationInSystem based on available TestSubProgram
        TestProgram testProgram = _panel.getProject().getTestProgram();
        if (testProgram.hasTestSubProgram(PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.LOW) ||
            testProgram.hasTestSubProgram(PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.HIGH))
        {
          hasRightTestSubProgram = true;          
        }
        
        if (testProgram.hasTestSubProgram(PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.LOW) ||
            testProgram.hasTestSubProgram(PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.HIGH))
        {
          hasLeftTestSubProgram = true;          
        }
        
        if (hasRightTestSubProgram && hasLeftTestSubProgram)
          _panelRegionComboBox.addItem(ALL);
        
        if (hasRightTestSubProgram)
          _panelRegionComboBox.addItem(PanelLocationInSystemEnum.RIGHT);
        
        if (hasLeftTestSubProgram)
          _panelRegionComboBox.addItem(PanelLocationInSystemEnum.LEFT);
      }
      else
      {
        _selectPanel.remove(_panelRegionLabel);
        _selectPanel.remove(_panelRegionComboBox);
      }
    }
    else
    {
      // no option to select left or right board only
      _selectPanel.remove(_panelRegionLabel);
      _selectPanel.remove(_panelRegionComboBox);
      for(Board board : _panel.getBoards())
      {
        _boardNumberComboBox.addItem(board);
      }
    }
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    if (_panel.getPanelSettings().isUsing2DAlignmentMethod())
      setEnableRFPFeature(false);
    else
      setEnableRFPFeature(true);
    
    _rfpCheckBox.setSelected(useRFP);
//    _rfpTopTextField.setText(Integer.toString(topOffset));
    _rfpTopTextField.setValue(topOffset);
//    _rfpBottomTextField.setText(Integer.toString(bottomOffset));
    _rfpBottomTextField.setValue(bottomOffset);
    _integrationLevelComboBox.setSelectedItem(signalCompensation);
    _userGainComboBox.setSelectedItem(userGain);
    _stageSpeedComboBox.setSelectedItem(stageSpeed);
    if(_panel.getPanelSettings().hasHighMagnificationComponent() == false)
    {
      _magnificationTypeComboBox.removeItem(ALL);
      _magnificationTypeComboBox.removeItem(MagnificationTypeEnum.HIGH);
    }
    else if(_panel.getPanelSettings().hasLowMagnificationComponent() == false)
    {
      _magnificationTypeComboBox.removeItem(ALL);
      _magnificationTypeComboBox.removeItem(MagnificationTypeEnum.LOW);
    }
    else
    {
      _magnificationTypeComboBox.setSelectedItem(magnification);
    }
    pack();
  }


  /**
   * @author Chong, Wei Chin
   */
  public void unPopulateData()
  {
    _panelRegionComboBox.removeAllItems();
    _boardNumberComboBox.removeAllItems();    
    _panel = null;
  }

  /**
   * @author Chong, Wei Chin
   */
  boolean userClickedOk()
  {
    return _ok;
  }

  /**
   * @author Chong, Wei Chin
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    _ok = false;
    dispose();
  }

  /**
   * @author Chong, Wei Chin
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    _ok = true;
    dispose();
  }

  /**
   * @return boolean
   * @author Chong Wei Chin
   */
  public boolean showDialog()
  {
    SwingUtils.centerOnComponent(this, _myFrame);
    setVisible(true);
    return _ok;
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  public boolean useRFP()
  {
    return _rfpCheckBox.isSelected();
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  public int getTopOffset()
  {
    try
    {
      return _rfpTopTextField.getLongValue().intValue();
    }
    catch (ParseException ex)
    {
      return 0;
    }
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  public int getBottomOffset()
  {
    try
    {
      return _rfpBottomTextField.getLongValue().intValue();
    }
    catch (ParseException ex)
    {
      return 0;
    }
  }
  /**
   * @return
   * @author Chong, Wei Chin
   */
  public boolean useAllBoard()
  {
    if(_boardNumberComboBox.getSelectedItem().equals(ALL))
      return true;

    return false;
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  public Board getSelectedBoard()
  {
    Assert.expect(_boardNumberComboBox.getSelectedItem().equals(ALL) == false);

    return (Board)_boardNumberComboBox.getSelectedItem();
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  public boolean useAllRegion()
  {
    if(_panelRegionComboBox.getSelectedItem().equals(ALL))
      return true;

    return false;
  }
  
  /**
   * @return
   * @author Chong, Wei Chin
   */
  public boolean useAllMagnification()
  {
    if(_magnificationTypeComboBox.getSelectedItem().equals(ALL))
      return true;

    return false;
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  public PanelLocationInSystemEnum getSelectedRegion()
  {
    Assert.expect(_panelRegionComboBox.getSelectedItem().equals(ALL) == false);

    return (PanelLocationInSystemEnum)_panelRegionComboBox.getSelectedItem();
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  public SignalCompensationEnum getIntegrationLevel()
  {
    return (SignalCompensationEnum)_integrationLevelComboBox.getSelectedItem();
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  public UserGainEnum getUserGain()
  {
    return (UserGainEnum)_userGainComboBox.getSelectedItem();
  }
  
  /**
   * @return
   * @author Chong, Wei Chin
   */
  public MagnificationTypeEnum getMagnificationType()
  {
    return (MagnificationTypeEnum)_magnificationTypeComboBox.getSelectedItem();
  }
  
  /**
   * @return
   * @author sheng chuan
   */
  public StageSpeedEnum getStageSpeed()
  {
    return (StageSpeedEnum)_stageSpeedComboBox.getSelectedItem();
  }
  
  /**
   * @XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setEnableRFPFeature(boolean enable)
  {
    if (enable == false)
    {
      _rfpCheckBox.setSelected(false);  
      _rfpTopTextField.setValue(0);
      _rfpBottomTextField.setValue(0);
    }
    
    _rfpCheckBox.setEnabled(enable);
    _rfpTopTextField.setEnabled(enable);
    _rfpBottomTextField.setEnabled(enable);
  }
}
