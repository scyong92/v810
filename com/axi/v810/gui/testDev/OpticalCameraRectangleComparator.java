/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.testDev;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import java.util.*;

/**
 * This class will be used to compare OpticalCameraRectangle alphabetically 
 * by their names, board names, z-height, and usability status.
 * 
 * @author Ying-Huan.Chu
 */
public class OpticalCameraRectangleComparator implements Comparator<OpticalCameraRectangle>
{
  private AlphaNumericComparator _alphaComparator;
  private OpticalCameraRectangleComparatorEnum _comparingAttribute;
  private boolean _ascending;

  /**
   * @author Laura Cormos
   */
  public OpticalCameraRectangleComparator(boolean ascending, OpticalCameraRectangleComparatorEnum comparingAttribute)
  {
    Assert.expect(comparingAttribute != null);
    _ascending = ascending;
    _comparingAttribute = comparingAttribute;
    _alphaComparator = new AlphaNumericComparator(_ascending);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public int compare(OpticalCameraRectangle lhs, OpticalCameraRectangle rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    String lhsString;
    String rhsString;
    
    if (_comparingAttribute.equals(OpticalCameraRectangleComparatorEnum.BOARD_NAME))
    {
      lhsString = lhs.getBoard().getName();
      rhsString = rhs.getBoard().getName();
      _alphaComparator.setAscending(_ascending);
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(OpticalCameraRectangleComparatorEnum.NAME))
    {
      lhsString = lhs.getName();
      rhsString = rhs.getName();
      _alphaComparator.setAscending(_ascending);
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(OpticalCameraRectangleComparatorEnum.Z_HEIGHT))
    {
      if (_ascending)
      {
        if (lhs.getZHeightInNanometer() > rhs.getZHeightInNanometer())
          return 1;
        else if (lhs.getZHeightInNanometer() == rhs.getZHeightInNanometer())
          return 0;
        else 
          return -1;
      }
      else
      {
        if (lhs.getZHeightInNanometer() > rhs.getZHeightInNanometer())
          return -1;
        else if (lhs.getZHeightInNanometer() == rhs.getZHeightInNanometer())
          return 0;
        else 
          return 1;
      }
    }
    else if (_comparingAttribute.equals(OpticalCameraRectangleComparatorEnum.USABILITY_STATUS))
    {
      lhsString = String.valueOf(lhs.getInspectableBool());
      rhsString = String.valueOf(rhs.getInspectableBool());
      _alphaComparator.setAscending(_ascending);
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(OpticalCameraRectangleComparatorEnum.COORDINATE))
    {
      return lhs.compareTo(rhs);
    }
    else
    {
      Assert.expect(false, "Please add to this method your new optical camera rectangle attribute to sort on.");
      return 0;
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setAscending(boolean ascending)
  {
    _ascending = ascending;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setOpticalCameraRectangleComparatorEnum(OpticalCameraRectangleComparatorEnum opticalCameraRectangleComparatorEnum)
  {
    Assert.expect(opticalCameraRectangleComparatorEnum != null);
    _comparingAttribute = opticalCameraRectangleComparatorEnum;
  }
}
