package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * This class is the data model for the package table for the Groupings panel and it also
 * converts to the component table model in the groupings panel
 * @author Chin Seong
 */
public class SubtypeAdvanceSettingsTableModel extends DefaultSortTableModel
{
  private List<Subtype> _subTypes = new ArrayList<Subtype>();
  private Collection<Subtype> _currentlySelectedSubtypes = new ArrayList<Subtype>();
  
  private static SubtypeAdvanceSettingsComparator _subtypeAdvanceSettingsComparator;
  private String[] _subtypeAdvanceSettingsTableColumns;
  
  static
  {
    _subtypeAdvanceSettingsComparator = new SubtypeAdvanceSettingsComparator(true, SubtypeCompratorEnum.SUBTYPE);
  }
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;
  
  /**
   * @author Chin Seong
   */
  public SubtypeAdvanceSettingsTableModel(boolean isVariableMagnification,
                                          int maxDroLevel,
                                          boolean hasCameraSelection)
  {
    ArrayList<String> _subtypeAdvanceSettings = new ArrayList<String>();
    _subtypeAdvanceSettings.add(StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY"));
    _subtypeAdvanceSettings.add(StringLocalizer.keyToString("ATGUI_FAMILY_KEY"));
    _subtypeAdvanceSettings.add(StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_GAIN_KEY"));
    _subtypeAdvanceSettings.add(StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_SPEED_KEY"));
    _subtypeAdvanceSettings.add(StringLocalizer.keyToString("MMGUI_GROUPINGS_INTERFACE_COMPENSATION_KEY"));
    _subtypeAdvanceSettings.add(StringLocalizer.keyToString("MMGUI_GROUPINGS_INTEGRATION_LEVEL_KEY"));
    if (hasCameraSelection)
      _subtypeAdvanceSettings.add(StringLocalizer.keyToString("MMGUI_CAMERA_ANGEL_SETTING_KEY"));
    if (isVariableMagnification)
      _subtypeAdvanceSettings.add(StringLocalizer.keyToString("MMGUI_GROUPINGS_MAGNIFICATION_TYPE_KEY"));
    if (maxDroLevel > 1)
      _subtypeAdvanceSettings.add(StringLocalizer.keyToString("MMGUI_GROUPINGS_DRO_LEVEL_KEY"));
    
    _subtypeAdvanceSettingsTableColumns = _subtypeAdvanceSettings.toArray(new String[0]);
  }

  /**
   * @author Chin Seong
   */
  void clear()
  {
    _subTypes.clear();
    _currentlySelectedSubtypes.clear();
    _currentSortColumn = 0;
    _currentSortAscending = true;
  }

  /**
   * @author Chin Seong
   */
  void populateSubtypesWithPanelData(List<Subtype> subtype)
  {
    _subTypes = subtype;
    sortColumn(_currentSortColumn, _currentSortAscending);

    // components may have been deleted, so if they were previously selected, we need to remove them.
    Iterator<Subtype> it = _currentlySelectedSubtypes.iterator();
    while(it.hasNext())
    {
      if (_subTypes.contains(it.next()) == false)
        it.remove();
    }
  }

   /**
   * @author Chin Seong
   */
  void updateSpecificSubtype(Subtype subtype)
  {
    int subtypeRow = _subTypes.indexOf(subtype);
    fireTableRowsUpdated(subtypeRow, subtypeRow);
  }

  /**
   * @author Chin Seong
   */
  int getRowForSubtype(Subtype subtype)
  {
    return _subTypes.lastIndexOf(subtype);
  }

  /**
   * @author Chin Seong
   */
  public void sortColumn(int column, boolean ascending)
  {
    if (column == SubtypeAdvanceSettingsTable._SUBTYPE_INDEX) // sort on subtype
    {
      _subtypeAdvanceSettingsComparator.setAscending(ascending);
      _subtypeAdvanceSettingsComparator.setSubtypeComparatorEnum(SubtypeCompratorEnum.SUBTYPE);
      Collections.sort(_subTypes, _subtypeAdvanceSettingsComparator);
    }
    else if (column == SubtypeAdvanceSettingsTable._JOINT_TYPE_INDEX)  // sort on joint type
    {
      _subtypeAdvanceSettingsComparator.setAscending(ascending);
      _subtypeAdvanceSettingsComparator.setSubtypeComparatorEnum(SubtypeCompratorEnum.JOINT_TYPE);
      Collections.sort(_subTypes, _subtypeAdvanceSettingsComparator);
    }
    else if(column == SubtypeAdvanceSettingsTable._USER_GAIN_INDEX) // user gain
    {
      _subtypeAdvanceSettingsComparator.setAscending(ascending);
      _subtypeAdvanceSettingsComparator.setSubtypeComparatorEnum(SubtypeCompratorEnum.USER_GAIN);
      Collections.sort(_subTypes, _subtypeAdvanceSettingsComparator);
    }
    else if(column == SubtypeAdvanceSettingsTable._STAGE_SPEED_INDEX) // stage speed
    {
      _subtypeAdvanceSettingsComparator.setAscending(ascending);
      _subtypeAdvanceSettingsComparator.setSubtypeComparatorEnum(SubtypeCompratorEnum.STAGE_SPEED);
      Collections.sort(_subTypes, _subtypeAdvanceSettingsComparator);
    }
    else if (column == SubtypeAdvanceSettingsTable._INTEFERENCE_COMPENSATION_INDEX) // sort on artifact compensation
    {
      _subtypeAdvanceSettingsComparator.setAscending(ascending);
      _subtypeAdvanceSettingsComparator.setSubtypeComparatorEnum(SubtypeCompratorEnum.ARTIFACT_COMPENSATION);
      Collections.sort(_subTypes, _subtypeAdvanceSettingsComparator);
    }
    else if (column == SubtypeAdvanceSettingsTable._INTERGRATION_LEVEL_INDEX) // sort on integration level
    {
      _subtypeAdvanceSettingsComparator.setAscending(ascending);
      _subtypeAdvanceSettingsComparator.setSubtypeComparatorEnum(SubtypeCompratorEnum.SIGNAL_COMPENSATION);
      Collections.sort(_subTypes, _subtypeAdvanceSettingsComparator);
    }
    else if (column == SubtypeAdvanceSettingsTable._MAGNIFICATION_TYPE_INDEX) // sort on magnification type
    {
      _subtypeAdvanceSettingsComparator.setAscending(ascending);
      _subtypeAdvanceSettingsComparator.setSubtypeComparatorEnum(SubtypeCompratorEnum.MAGNIFICATION_TYPE);
      Collections.sort(_subTypes, _subtypeAdvanceSettingsComparator);
    }
    else if (column == SubtypeAdvanceSettingsTable._DRO_LEVEL_INDEX) // sort on DRO Level
    {
      _subtypeAdvanceSettingsComparator.setAscending(ascending);
      _subtypeAdvanceSettingsComparator.setSubtypeComparatorEnum(SubtypeCompratorEnum.DYNAMIC_RANGE_OPTIMIZATION_LEVEL);
      Collections.sort(_subTypes, _subtypeAdvanceSettingsComparator);
    }

    _currentSortColumn = column;
    _currentSortAscending = ascending;
  }

  /**
   * @author Chin Seong
   */
  public int getColumnCount()
  {
    return _subtypeAdvanceSettingsTableColumns.length;
  }

  /**
   * @author Chin Seong
   */
  public int getRowCount()
  {
    if(_subTypes == null)
        return 0;
    else
        return _subTypes.size();
  }

  /**
   * @author Chin Seong
   */
  public Class getColumnClass(int c)
  {
    return getValueAt(0, c).getClass();
  }

  /**
   * @author Chin Seong
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    
    return (String)_subtypeAdvanceSettingsTableColumns[columnIndex];
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    // column 0 is subtype - always not editable
    // column 1 is jointType - always not editable
    // column 2 is Stage Velocity - always editable
    // column 3 is user gain - always editable
    // column 4 is artifact compensation - always editable
    // column 5 is signal compensation - always editable
    // column 6 is magnification type - always editable

    if (columnIndex == SubtypeAdvanceSettingsTable._SUBTYPE_INDEX)
      return true;
    if (columnIndex == SubtypeAdvanceSettingsTable._JOINT_TYPE_INDEX)
      return false;
    if (_currentlySelectedSubtypes.isEmpty())
      return false;
    if (columnIndex == SubtypeAdvanceSettingsTable._USER_GAIN_INDEX)
       return true;
    if (columnIndex == SubtypeAdvanceSettingsTable._STAGE_SPEED_INDEX)
       return true;
    if (columnIndex == SubtypeAdvanceSettingsTable._CAMERA_ANGLE_ENABLED_INDEX)
       return true;
    if (columnIndex == SubtypeAdvanceSettingsTable._INTEFERENCE_COMPENSATION_INDEX)
    {
      if ( _currentlySelectedSubtypes.size() > 1 )
      {
        // if multiselected rows and they are all N/A then return NOT EDITABLE
        boolean allAreNotApplicable = true;
        for (Subtype subtype : _currentlySelectedSubtypes)
        {
          if (subtype.getSubtypeAdvanceSettings().getArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false)
              allAreNotApplicable = false;
        }
        if(allAreNotApplicable)
          return false;
        else
          return true;
      }
      else
      {
        Subtype subtype = getSubtypeAt(rowIndex);
        if (subtype.getSubtypeAdvanceSettings().getArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE))
          return false;
        else
          return true;
      }
    }
    
    if (columnIndex == SubtypeAdvanceSettingsTable._INTERGRATION_LEVEL_INDEX)
    {
      Subtype subtype = getSubtypeAt(rowIndex);
      // Chnee Khang Wah, 2013-04-23, New entropy based IC
      if(subtype.getPanel().getProject().isGenerateByNewScanRoute())
      {
        return true;
      }
      
      if(subtype.getSubtypeAdvanceSettings().getArtifactCompensationState().equals(ArtifactCompensationStateEnum.COMPENSATED) == false)
      {
        return true;
      }
    }
    
    if (columnIndex == SubtypeAdvanceSettingsTable._MAGNIFICATION_TYPE_INDEX)
    {
      if ( _currentlySelectedSubtypes.size() > 1 )
      {
        // if multiselected rows and they are all N/A then return NOT EDITABLE
        boolean allCannotUseVariableMagnification = false;
        for (Subtype subtype : _currentlySelectedSubtypes)
        {
          if (subtype.getSubtypeAdvanceSettings().canUseVariableMagnification() == false)
          {
            allCannotUseVariableMagnification = true;
          }
        }
        if(allCannotUseVariableMagnification)
          return false;
        else
          return true;
      }
      else
      {
        Subtype subtype = getSubtypeAt(rowIndex);
        if (subtype.getSubtypeAdvanceSettings().canUseVariableMagnification())
          return true;
        else
          return false;
      }
    }
    
    //Siew Yeng - XCR-2140 - DRO
    if (columnIndex == SubtypeAdvanceSettingsTable._DRO_LEVEL_INDEX)
    {
      return true;
    }

    return false;
  }

  /**
   * @author Chin Seong
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Subtype subtype = getSubtypeAt(rowIndex);

    if(columnIndex == SubtypeAdvanceSettingsTable._SUBTYPE_INDEX) // component subtype
    {
      return subtype.getShortName();
    }
    else if (columnIndex == SubtypeAdvanceSettingsTable._JOINT_TYPE_INDEX) // component joint type
    {
      return subtype.getJointTypeEnum();
    }
    else if (columnIndex == SubtypeAdvanceSettingsTable._USER_GAIN_INDEX) // user gain
    {
      return subtype.getSubtypeAdvanceSettings().getUserGain();
    }
    else if (columnIndex == SubtypeAdvanceSettingsTable._STAGE_SPEED_INDEX) // stageSpeed
    {
      return subtype.getSubtypeAdvanceSettings().getStageSpeedList();
    }
    else if (columnIndex == SubtypeAdvanceSettingsTable._INTEFERENCE_COMPENSATION_INDEX) // artifact compensation
    {
      return subtype.getSubtypeAdvanceSettings().getArtifactCompensationState();
    }
    else if (columnIndex == SubtypeAdvanceSettingsTable._INTERGRATION_LEVEL_INDEX) // integration level
    {
      return  subtype.getSubtypeAdvanceSettings().getSignalCompensation();
    }
    else if (columnIndex == SubtypeAdvanceSettingsTable._MAGNIFICATION_TYPE_INDEX) // magnification type
    {
      return subtype.getSubtypeAdvanceSettings().getMagnificationType();
    }
    else if(columnIndex == SubtypeAdvanceSettingsTable._DRO_LEVEL_INDEX) // dro level
    {
      return subtype.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel();
    }
    else if (columnIndex == SubtypeAdvanceSettingsTable._CAMERA_ANGLE_ENABLED_INDEX) // camera angle enabled
    {
      Collections.sort(subtype.getSubtypeAdvanceSettings().getCameraEnabledIDList());
      return subtype.getSubtypeAdvanceSettings().getCameraEnabledIDList();
    }
    
    return null;
  }

  /**
   * @author Chin Seong
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    if (columnIndex == SubtypeAdvanceSettingsTable._SUBTYPE_INDEX)
    {
      String newSubtypeName = (String)aValue;
      Subtype currentSubtype = getSubtypeAt(rowIndex);
      try
      {
        _commandManager.execute(new SubtypeSetNameCommand(currentSubtype, newSubtypeName));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    else if (columnIndex == SubtypeAdvanceSettingsTable._USER_GAIN_INDEX)//user gain
    {
      Assert.expect(aValue instanceof UserGainEnum);
      Subtype subtype = getSubtypeAt(rowIndex);
      try
      {
        _commandManager.execute(new SubtypeSetUserGainCommand(subtype.getSubtypeAdvanceSettings(), (UserGainEnum) (aValue)));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
          ex.getLocalizedMessage(),
          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
          true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    else if (columnIndex == SubtypeAdvanceSettingsTable._STAGE_SPEED_INDEX)//stage speed
    {
      Assert.expect(aValue instanceof ArrayList || aValue instanceof StageSpeedEnum);
      List<StageSpeedEnum> speedList = new ArrayList<StageSpeedEnum>();
      if (aValue instanceof StageSpeedEnum)
        speedList.add((StageSpeedEnum)aValue);
      else
        speedList.addAll((List<StageSpeedEnum>)aValue);
      
      Subtype subtype = getSubtypeAt(rowIndex);
      try
      {
        _commandManager.execute(new SubtypeSetStageSpeedCommand(subtype.getSubtypeAdvanceSettings(), speedList));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
          ex.getLocalizedMessage(),
          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
          true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    else if (columnIndex == SubtypeAdvanceSettingsTable._INTEFERENCE_COMPENSATION_INDEX) // artifact compensation
    {
      Assert.expect(aValue instanceof ArtifactCompensationStateEnum);
      Subtype subtype = getSubtypeAt(rowIndex);
      try
      {
        // it is possible to try to set AC for mixed or N/A component when using the multi select feature
        // set a new AC at the component level only if this component uses one state (not mixed) and is not N/A
        if (subtype.getSubtypeAdvanceSettings().getArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false)
          _commandManager.execute(new SubtypeAdvanceSettingsSetArtifactCompesationCommand(subtype.getSubtypeAdvanceSettings(), (ArtifactCompensationStateEnum)(aValue)));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    else if (columnIndex == SubtypeAdvanceSettingsTable._INTERGRATION_LEVEL_INDEX) // integration level
    {
      Assert.expect(aValue instanceof SignalCompensationEnum);
      Subtype subtype = getSubtypeAt(rowIndex);
      try
      {
        if (subtype.getSubtypeAdvanceSettings().getArtifactCompensationState().equals(ArtifactCompensationStateEnum.COMPENSATED) == false ||
            subtype.getPanel().getProject().isGenerateByNewScanRoute() == true)
        {
          _commandManager.execute(new SubtypeAdvanceSettingsSetIntegrationLevelCommand(subtype.getSubtypeAdvanceSettings(), (SignalCompensationEnum)(aValue)));
        }
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    else if (columnIndex == SubtypeAdvanceSettingsTable._MAGNIFICATION_TYPE_INDEX) // magnification type
    {
      Assert.expect(aValue instanceof MagnificationTypeEnum);
      Subtype subtype = getSubtypeAt(rowIndex);  
      
      if(subtype.getSubtypeAdvanceSettings().canUseVariableMagnification())
      {
        try
        {
          // XCR-2068 Alignment fail if recipe switch from low magnification only to high + low magnification
          subtype.getPanel().getProject().isNeedToForceRunAllCDAImmediatelyIfLowHighMagSwitching((MagnificationTypeEnum)aValue);
          
          _commandManager.execute(new SubtypeAdvanceSettingsSetMagnificationTypeCommand(subtype.getSubtypeAdvanceSettings(), (MagnificationTypeEnum)aValue));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
      }
    }
    else if(columnIndex == SubtypeAdvanceSettingsTable._DRO_LEVEL_INDEX) //dro level
    {
      Assert.expect(aValue instanceof DynamicRangeOptimizationLevelEnum);
      Subtype subtype = getSubtypeAt(rowIndex);  

      try
      {
        _commandManager.execute(new SubtypeAdvanceSettingsSetDynamicRangeOptimizationLevelCommand(subtype.getSubtypeAdvanceSettings(), (DynamicRangeOptimizationLevelEnum)aValue));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        fireTableCellUpdated(rowIndex, columnIndex);
      }
    }
    else if (columnIndex == SubtypeAdvanceSettingsTable._CAMERA_ANGLE_ENABLED_INDEX)
    {
        Assert.expect(aValue instanceof ArrayList);
        List<Integer> cameraList = (List<Integer>)aValue;
        Subtype subtype = getSubtypeAt(rowIndex);
        try
        {
          _commandManager.execute(new SubtypeSetCameraEnabledListCommand(subtype.getPanel().getProject(), subtype.getSubtypeAdvanceSettings(), cameraList));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
            ex.getLocalizedMessage(),
            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
            true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
    }
    else
      Assert.expect(false);
  }

  /**
   * @author Chin Seong
   */
  void setSelectedSubtypes(Collection<Subtype> subtypes)
  {
    Assert.expect(_currentlySelectedSubtypes != null);
    _currentlySelectedSubtypes = subtypes;
  }

  /**
   * @author Chin Seong
  */
  Collection<Subtype> getSelectedSubtypes()
  {
    Assert.expect(_currentlySelectedSubtypes != null);
    return _currentlySelectedSubtypes;
  }

  /**
   * @author Chin Seong
   */
  Subtype getSubtypeAt(int row)
  {
    return _subTypes.get(row);
  }
}
