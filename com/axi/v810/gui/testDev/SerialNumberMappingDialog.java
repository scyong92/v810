package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * @author siew-yeng.phang
 */
public class SerialNumberMappingDialog extends JDialog
{  
  private PanelHandler _panelHandler;
  private Project _project;
  private SerialNumberMappingManager _serialNumberMappingManager;
  private BarcodeReaderManager _barcodeReaderManager;
  
  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  
  private JPanel _centerPanel =  new JPanel(new BorderLayout());
  
  private JPanel _projectGraphicPanel = new JPanel();
  private ImageDisplayPanel _imageDisplayPanel = new ImageDisplayPanel();
  private JLabel _westUpArrowLabel = new JLabel(Image5DX.getImageIcon(Image5DX.UP_ARROW));
  private JLabel _eastUpArrowLabel = new JLabel(Image5DX.getImageIcon(Image5DX.UP_ARROW));
  private Image _image;
  
  private JPanel _serialNumberMappingTablePanel = new JPanel();
  private JScrollPane _serialNumberMappingScrollPane = new JScrollPane();
  private SerialNumberMappingTable _serialNumberMappingTable;
  private SerialNumberMappingTableModel _serialNumberMappingTableModel;
  
  private JPanel _bcrConfigurationPanel = new JPanel();
  private JLabel _selectBcrConfigurationToUseLabel = new JLabel();
  private JLabel _selectBcrConfigurationToUseValue = new JLabel();
  
  private JPanel _buttonPanel = new JPanel();
  private JPanel _bottomPanel = new JPanel();
  private JButton _okButton = new JButton(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
  private JButton _cancelButton = new JButton(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
  
  private JButton _defaultButton = new JButton(StringLocalizer.keyToString("GUI_SERIAL_NUMBER_MAPPING_DEFAULT_BUTTON_KEY"));
  private JButton _lastSavedButton = new JButton(StringLocalizer.keyToString("GUI_SERIAL_NUMBER_MAPPING_LAST_SAVED_BUTTON_KEY"));
  
  private JPanel _statusPanel;
  private JPanel _statusTextPanel;
  private JLabel _statusTextValue;
  private JLabel _statusTextTitle;

  private MainMenuGui _mainUI;
  private transient boolean _redrawPanelImage = false;
    
  private static com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  /**
   * @author Phang Siew Yeng
   */
  SerialNumberMappingDialog(Frame frame,
                    String title,
                    boolean modal,
                    Project project)
  {
    super(frame, title, modal);
    Assert.expect(frame != null);
    Assert.expect(title != null);

    _mainUI = MainMenuGui.getInstance();
    _panelHandler = PanelHandler.getInstance();
    _serialNumberMappingManager = SerialNumberMappingManager.getInstance();
    _barcodeReaderManager = BarcodeReaderManager.getInstance();
    _project = project;
    
    copyPanelImageIfNecessary();
    redrawPanelImageIfNecessary();
    jbInit();
    pack();
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  private void jbInit()
  {
    this.setPreferredSize(new Dimension(700, 700));
    getContentPane().add(_mainPanel);
    _mainPanel.setLayout(_mainPanelBorderLayout);

    _projectGraphicPanel.setBorder(BorderFactory.createCompoundBorder(
        BorderFactory.createEtchedBorder(Color.white, new Color(165, 163, 151)),
        BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _projectGraphicPanel.setLayout(new BorderLayout(5, 0));
    
    _projectGraphicPanel.setPreferredSize(new Dimension(700,300));

    // load direction arrows
    _projectGraphicPanel.add(_westUpArrowLabel, BorderLayout.WEST);
    _projectGraphicPanel.add(_eastUpArrowLabel, BorderLayout.EAST);
    _projectGraphicPanel.add(_imageDisplayPanel,BorderLayout.CENTER);
        
    _serialNumberMappingTableModel = new SerialNumberMappingTableModel(this);
    _serialNumberMappingTable = new SerialNumberMappingTable(_serialNumberMappingTableModel);
    _serialNumberMappingTable.getTableHeader().setReorderingAllowed(false);
    _serialNumberMappingTable.setBoardNames(_project.getPanel().getBoardNames());
    _serialNumberMappingTable.setNumberOfScanner(_barcodeReaderManager.getBarcodeReaderNumberOfScanners());
    _serialNumberMappingTable.setupCellEditorsAndRenderers();
    _serialNumberMappingTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _serialNumberMappingScrollPane.getViewport().add(_serialNumberMappingTable);

    
    _bcrConfigurationPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    _bcrConfigurationPanel.setBorder(BorderFactory.createCompoundBorder(
      BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
      BorderFactory.createEmptyBorder(0, 2, 2, 2)));

    _bcrConfigurationPanel.add(_selectBcrConfigurationToUseLabel);
    _bcrConfigurationPanel.add(_selectBcrConfigurationToUseValue);

    _selectBcrConfigurationToUseLabel.setText(StringLocalizer.keyToString("GUI_SERIAL_NUMBER_MAPPING_BARCODE_CONFIGURATION_USED_LABEL_KEY"));
    if(_serialNumberMappingManager.getBarcodeReaderConfigurationNameUsed() == null)
    {
      _selectBcrConfigurationToUseValue.setText(_barcodeReaderManager.getBarcodeReaderConfigurationNameToUse());
    }
    else
    {
      _selectBcrConfigurationToUseValue.setText(_serialNumberMappingManager.getBarcodeReaderConfigurationNameUsed());
    }
    
    _serialNumberMappingTablePanel.setLayout(new BorderLayout());
    _serialNumberMappingTablePanel.add(_bcrConfigurationPanel,BorderLayout.NORTH);
    _serialNumberMappingTablePanel.add(_serialNumberMappingScrollPane,BorderLayout.CENTER);
    
     _statusPanel = new JPanel();
    _statusPanel.setLayout(new BorderLayout());
    _statusPanel.setBorder(BorderFactory.createCompoundBorder(
        BorderFactory.createEtchedBorder(Color.white, new Color(165, 163, 151)),
        BorderFactory.createEmptyBorder(0, 5, 5, 5)));
 
    _statusTextTitle = new JLabel();
    _statusTextTitle.setText("Expected Scan Sequence:");
    _statusTextTitle.setFont(FontUtil.getBoldFont(_statusTextTitle.getFont(), Localization.getLocale()));
    _statusTextValue = new JLabel();
    
    _statusTextPanel = new JPanel(new BorderLayout()); 
    _statusTextPanel.add(_statusTextTitle,BorderLayout.NORTH);
    _statusTextPanel.add(_statusTextValue,BorderLayout.CENTER);
    
    _centerPanel.add(_projectGraphicPanel,BorderLayout.NORTH);
    _centerPanel.add(_serialNumberMappingTablePanel,BorderLayout.CENTER);
    _centerPanel.add(_statusPanel,BorderLayout.SOUTH);
    _mainPanel.add(_centerPanel, BorderLayout.CENTER);
    
    _statusPanel.add(_statusTextPanel, BorderLayout.CENTER);
    
    _buttonPanel.setLayout(new FlowLayout());
    _buttonPanel.add(_okButton);
    _buttonPanel.add(_cancelButton);
    
    _buttonPanel.add(_defaultButton);
    _buttonPanel.add(_lastSavedButton);
    
    _bottomPanel.add(_buttonPanel,BorderLayout.EAST);
    _mainPanel.add(_bottomPanel, BorderLayout.SOUTH);
    
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelAction();
      }
    });
    
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    
    _defaultButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        defaultButton_actionPerformed(e);
      }
      
    });
    
    _lastSavedButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        lastSavedButton_actionPerformed(e);
      }
    });
    
    ListSelectionListener listSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent event)
      {
        boardSelectionUpdated(event);
      }
    };

    _serialNumberMappingTable.getSelectionModel().addListSelectionListener(listSelectionListener);
    java.util.List<SerialNumberMappingData> serialNumberMappingDataList = _serialNumberMappingManager.getSerialNumberMappingDataList();
    updateMappingTable(serialNumberMappingDataList);
    updateSummary();
    updatePanelImage();
  }
  
  /*
   * @author Phang Siew Yeng
   */
  protected void updateSummary()
  {
    String message = "<html>";

    int noOfScanner = _barcodeReaderManager.getBarcodeReaderNumberOfScanners();
    int sequence = 1;
      
    for(int id = 0 ; id < noOfScanner ; id++)
    {
      message = message + "Barcode Reader " + (id+1) + ": ";
      if(isValidMapping(id+1))
      {
        String boardName = _serialNumberMappingTableModel.getBoardName((id+1), sequence);
        while(boardName != null)
        {
          message = message + "Board " + boardName;
          sequence++;
          boardName = _serialNumberMappingTableModel.getBoardName((id+1), sequence);

          if(boardName != null)
            message += " \u2192 ";
        }
      }
      else
      {
        message += "Invalid Mapping";
      }
      message += "<br>";
      sequence = 1;
    }
    message += "</html>";
    _statusTextValue.setText(message);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  private void updatePanelImage()
  {
    Assert.expect(_project != null);
    int row = _serialNumberMappingTable.getSelectedRow();
    
    _imageDisplayPanel.clearImage();
    _imageDisplayPanel.setRotateImage(false);
    
    PanelHandlerPanelFlowDirectionEnum flowDirection = _panelHandler.getPanelFlowDirection();
    boolean flipPanelImage = flowDirection == PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT;
    if (flipPanelImage == false)
    {
      if(row == -1)
      {
        _image = FileName.getTempPanelImage(_project.getName(), false);
      }
      else
      {
        _image = FileName.getTempPanelImage(_project.getName(), _serialNumberMappingTableModel.getSelectedBoardName(row), flipPanelImage);
      }
    }
    else
    {
      if (FileName.hasPanelImage(_project.getName(), flipPanelImage))
      {
        // rotate image 180 degrees if handler is right-to-left
        _imageDisplayPanel.setRotateImage(true);
      }
      else
      {
        // don't rotate image if we are going to display textual "no image"
        _imageDisplayPanel.setRotateImage(false);
      }
        _image = FileName.getTempPanelImage(_project.getName(), _serialNumberMappingTableModel.getSelectedBoardName(row), flipPanelImage);
    }

    if (_image != null)
    {
      _imageDisplayPanel.setImage(_image);
    }
    _imageDisplayPanel.setPreferredSize(new Dimension(600,450));
  }
  
  /**
   * @author Phang Siew Yeng
   */
  private void updateMappingTable(java.util.List<SerialNumberMappingData> data)
  {
    Assert.expect(_project != null);

    if(data.isEmpty() || _project.getPanel().getBoards().size() != data.size())
    {
      data = _serialNumberMappingManager.getDefaultSerialNumberMappingDataList();
    }
    
    _serialNumberMappingTableModel.populateMappingTable(data);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  private void cancelAction()
  {
    _serialNumberMappingTableModel.clearData();
    dispose();
  }
  
  /*
   * @author Phang Siew Yeng
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    int numberOfBarcodeReadersSetInSerialNumberMapping = _serialNumberMappingTableModel.getNumberOfBarcodeReaders();
    int numberOfBarcodeReadersSetInMachine = _barcodeReaderManager.getBarcodeReaderNumberOfScanners();
    if(numberOfBarcodeReadersSetInSerialNumberMapping > numberOfBarcodeReadersSetInMachine)
    {
      String barcodeReaderNotExist = "";
      for(int br = numberOfBarcodeReadersSetInMachine; br<numberOfBarcodeReadersSetInSerialNumberMapping; br++)
      {
        barcodeReaderNotExist += (br+1);
        
        if((br+1) < numberOfBarcodeReadersSetInSerialNumberMapping)
          barcodeReaderNotExist += ", ";
      }
      
      MessageDialog.showWarningDialog(_mainUI, 
          StringLocalizer.keyToString(new LocalizedString("MMGUI_BARCODE_READER_NOT_EXIST_WARNING_MESSAGE_KEY", new Object[]{barcodeReaderNotExist})),
          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
      return;
    }
    
    for(int br = 0 ; br < numberOfBarcodeReadersSetInMachine ; br++)
    {
      if(isValidMapping(br+1) == false)
      {
        MessageDialog.showWarningDialog(_mainUI, 
          StringLocalizer.keyToString(new LocalizedString("MMGUI_INVALID_SERIAL_NUMBER_MAPPING_WARNING_MESSAGE_KEY",new Object[]{(br+1)})),
          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
        return;
      } 
    }
    
    _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PANEL_SET_SERIAL_NUMBER_MAPPING_KEY"));
    try
    {
      _commandManager.execute(new EditSerialNumberMappingCommand(_serialNumberMappingTableModel.getData()));
      _commandManager.execute(new EditSerialNumberMappingBarcodeConfigNameCommand(_barcodeReaderManager.getBarcodeReaderConfigurationNameToUse()));

    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI,
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
    }
    _commandManager.endCommandBlock();
    dispose();
  }
  
  /*
   * @author Phang Siew Yeng
   */
  private void defaultButton_actionPerformed(ActionEvent e)
  {
    updateMappingTable(_serialNumberMappingManager.getDefaultSerialNumberMappingDataList());
    updateSummary();
  }
  
  /*
   * @author Phang Siew Yeng
   */
  private void lastSavedButton_actionPerformed(ActionEvent e)
  {
    updateMappingTable(_serialNumberMappingManager.getLastSavedSerialNumberMappingDataList());
    updateSummary();
  }
  
  /*
   * @author Phang Siew Yeng
   */
  private void boardSelectionUpdated(ListSelectionEvent event)
  {   
    updatePanelImage(); 
  }
  
  /*
   * @author Phang Siew Yeng
   */
  private boolean isValidMapping(int barcodeReaderId)
  {
    java.util.List<Integer> sequences = new ArrayList<Integer>();
    sequences = _serialNumberMappingTableModel.getScanSequences(barcodeReaderId);
    if(sequences.isEmpty())
      return true;
    
    for(int sequence = 0;sequence < sequences.size() ; sequence++)
    {
      if(sequences.contains(sequence+1) == false)
      {
        return false;
      }
    }
    
    return true;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void clear()
  {    
    _project = null;
    _serialNumberMappingTable.clear();
    _serialNumberMappingTableModel.clearData();
  }

  /*
   * @author Phang Siew Yeng
   */
  private void copyPanelImageIfNecessary()
  {
    String projectTempDir = Directory.getProjectTempDir(_project.getName());
    java.util.List<String> boardNameList = _project.getPanel().getBoardNames();
    try
    {
      if(FileUtil.existsDirectory(projectTempDir) == false)
        FileUtil.mkdirs(projectTempDir);
      
      if(FileName.hasTempPanelImage(_project.getName(), false) == false)
      {
        FileUtilAxi.copy(FileName.getPanelImageFullPath(_project.getName()), 
                          FileName.getTempPanelImageFullPath(_project.getName()));
        FileUtilAxi.copy(FileName.getPanelFlippedImageFullPath(_project.getName()), 
                          FileName.getTempPanelFlippedImageFullPath(_project.getName()));
        
        for(String boardName : boardNameList)
        {
          FileUtilAxi.copy(FileName.getPanelBoardImageFullPath(_project.getName(),boardName), 
                          FileName.getTempPanelBoardImageFullPath(_project.getName(),boardName));
          FileUtilAxi.copy(FileName.getPanelBoardFlippedImageFullPath(_project.getName(),boardName), 
                          FileName.getTempPanelBoardFlippedImageFullPath(_project.getName(),boardName));
        }
      }
    }
    catch (FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(projectTempDir);
      dex.initCause(ex);
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(projectTempDir);
      dex.initCause(ex);
    }
    catch (DatastoreException ex)
    {
      //
    }
  }
  
  /*
   * @author Phang Siew Yeng
   */
  private void redrawPanelImageIfNecessary()
  {
    java.util.List<String> boardNameList = _project.getPanel().getBoardNames();
    int index = 0;
    for(SerialNumberMappingData data : _serialNumberMappingManager.getSerialNumberMappingDataList())
    {
      if(boardNameList.get(index).equals(data.getBoardName()) == false)
      {
        data.setBoardName(boardNameList.get(index));
        _redrawPanelImage = true;
      }
      index++;
    }

    if(_redrawPanelImage)
    {
      SwingWorkerThread.getInstance().invokeLater(new Runnable()
      {
        public void run()
        {
          DrawCadPanel.createPanelImage(_project,false);
          updatePanelImage();
        }
      });      
    }
  }
}
