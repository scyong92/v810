package com.axi.v810.gui.testDev;

import java.awt.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * This class displays the information for all the new components added in a single GUI session (between a project
 * open and a project close)
 * @author Laura Cormos
 */
class NewComponentTable extends JSortTable
{
  private static final int _COMPONENT_NAME_INDEX = 0;
  static final int _BOARD_NAME_INDEX = 1;
  private static final int _COMPONENT_X_INDEX = 2;
  private static final int _COMPONENT_Y_INDEX = 3;
  private static final int _COMPONENT_ROTATION_INDEX = 4;
  private static final int _COMPONENT_SIDE_INDEX = 5;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _COMPONENT_NAME_COLUMN_WIDTH_PERCENTAGE = .25;
  private static final double _BOARD_NAME_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double _COMPONENT_X_COLUMN_WIDTH_PERCENTAGE = .17;
  private static final double _COMPONENT_Y_COLUMN_WIDTH_PERCENTAGE = .17;
  private static final double _COMPONENT_ROTATION_COLUMN_WIDTH_PERCENTAGE = .16;
  private static final double _COMPONENT_SIDE_COLUMN_WIDTH_PERCENTAGE = .10;

  private NumericRenderer _numericRenderer = new NumericRenderer(2, JLabel.RIGHT);

  private NumericEditor _xNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2);
  private NumericEditor _yNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2);
  private RotationCellEditor _rotationEditor;

  private boolean _modelSet = false;

  /**
   * Default constructor.
   * @author Carli Connally
   */
  public NewComponentTable(Component parent)
  {
    Assert.expect(parent != null);
    _rotationEditor = new RotationCellEditor(parent, true);
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Carli Connally
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_COMPONENT_NAME_INDEX).setPreferredWidth((int)(totalColumnWidth * _COMPONENT_NAME_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_BOARD_NAME_INDEX).setPreferredWidth((int)(totalColumnWidth * _BOARD_NAME_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_COMPONENT_X_INDEX).setPreferredWidth((int)(totalColumnWidth * _COMPONENT_X_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_COMPONENT_Y_INDEX).setPreferredWidth((int)(totalColumnWidth * _COMPONENT_Y_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_COMPONENT_ROTATION_INDEX).setPreferredWidth((int)(totalColumnWidth * _COMPONENT_ROTATION_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_COMPONENT_SIDE_INDEX).setPreferredWidth((int)(totalColumnWidth * _COMPONENT_SIDE_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };
    columnModel.getColumn(_COMPONENT_NAME_INDEX).setCellEditor(stringEditor);

    columnModel.getColumn(_COMPONENT_X_INDEX).setCellEditor(_xNumericEditor);
    columnModel.getColumn(_COMPONENT_X_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(_COMPONENT_Y_INDEX).setCellEditor(_yNumericEditor);
    columnModel.getColumn(_COMPONENT_Y_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(_COMPONENT_ROTATION_INDEX).setCellEditor(_rotationEditor);
    columnModel.getColumn(_COMPONENT_ROTATION_INDEX).setCellRenderer(_numericRenderer);

    // column 5 is the board side that component is on: top or bottom from the xray's tube perspective
    JComboBox sideComboBox = new JComboBox();
    sideComboBox.addItem(StringLocalizer.keyToString("CAD_SIDE1_KEY"));
    sideComboBox.addItem(StringLocalizer.keyToString("CAD_SIDE2_KEY"));
    columnModel.getColumn(_COMPONENT_SIDE_INDEX).setCellEditor(new DefaultCellEditor(sideComboBox));
  }

  /**
   * @author George Booth
   */
  public void addEditorCustomValue(Double customValue)
  {
     _rotationEditor.addCustomValue(customValue);
  }

  /**
   * @author George Booth
   */
  public void resetEditorsAndRenderers()
  {
     _rotationEditor.removeCustomValues();
  }

  /**
   * The decimal places to be displayed can change based on the measurement value that
   * is being displayed.  This method changes the renderers/editors to display the appropriate
   * number of decimal places.
   * @param decimalPlaces Number of decimal places to be displayed for all measurement values in the table.
   * @author Carli Connally
   */
  public void setDecimalPlacesToDisplay(int decimalPlaces)
  {
    Assert.expect(decimalPlaces >= 0);

    _xNumericEditor.setDecimalPlaces(decimalPlaces);
    _yNumericEditor.setDecimalPlaces(decimalPlaces);
    _numericRenderer.setDecimalPlacesToDisplay(decimalPlaces);
  }

  /**
   * @author Laura Cormos
   */
  public void clearSelection()
  {
    TableModel model = getModel();
    // this function gets called before the table model is set, during intialization. At that time, the model will
    // be a defaultTableModel so the assert is sure to fail
    if (_modelSet)
    {
      Assert.expect(model instanceof NewComponentTableModel);
      ((NewComponentTableModel)model).clearSelectedComponents();
    }
    super.clearSelection();
  }

  /**
   * @author Laura Cormos
   */
  public void setModel(NewComponentTableModel model)
  {
   Assert.expect(model != null);
   _modelSet = true;
   super.setModel(model);
  }
}
