package com.axi.v810.gui.testDev;

import java.awt.Component;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This class displays the information for the board instances on a panel.
 * All cells are editable in-line except for the board type.
 * @author Carli Connally
 */
class PanelLayoutTable extends JTable
{

  public static final int _BOARD_NAME_INDEX = 0;
  public static final int _BOARD_TYPE_INDEX = 1;
  public static final int _BOARD_X_INDEX = 2;
  public static final int _BOARD_Y_INDEX = 3;
  public static final int _BOARD_ROTATION_INDEX = 4;
  public static final int _BOARD_FLIP_INDEX = 5;
  public static final int _BOARD_ZOFFSET_INDEX = 6;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _BOARD_NAME_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double _BOARD_TYPE_COLUMN_WIDTH_PERCENTAGE = .20;
  private static final double _BOARD_X_COLUMN_WIDTH_PERCENTAGE = .14;
  private static final double _BOARD_Y_COLUMN_WIDTH_PERCENTAGE = .14;
  private static final double _BOARD_ROTATION_COLUMN_WIDTH_PERCENTAGE = .12;
  private static final double _BOARD_FLIP_COLUMN_WIDTH_PERCENTAGE = .13;
  private static final double _BOARD_ZOFFSET_COLUMN_WIDTH_PERCENTAGE = .12;

  private NumericRenderer _numericRenderer = new NumericRenderer(2, JLabel.RIGHT);

  private NumericEditor _xNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2, 0.0, Double.MAX_VALUE);
  private NumericEditor _yNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2, 0.0, Double.MAX_VALUE);
  private BoardAndPanelRotationCellEditor _rotationEditor = new BoardAndPanelRotationCellEditor();
  private CheckCellEditor _checkEditor = new CheckCellEditor(getBackground(), getForeground());
  private NumericEditor _zOffsetEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2, -1 * Double.MAX_VALUE, Double.MAX_VALUE);

  /**
   * Default constructor.
   * @author Carli Connally
   */
  public PanelLayoutTable()
  {
    // do nothing
    getTableHeader().setReorderingAllowed(false);
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Carli Connally
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int boardNameColumnWidth    = (int)(totalColumnWidth * _BOARD_NAME_COLUMN_WIDTH_PERCENTAGE);
    int boardTypeColumnWidth    = (int)(totalColumnWidth * _BOARD_TYPE_COLUMN_WIDTH_PERCENTAGE);
    int boardXColumnWidth       = (int)(totalColumnWidth * _BOARD_X_COLUMN_WIDTH_PERCENTAGE);
    int boardYColumnWidth       = (int)(totalColumnWidth * _BOARD_Y_COLUMN_WIDTH_PERCENTAGE);
    int boardRotColumnWidth     = (int)(totalColumnWidth * _BOARD_ROTATION_COLUMN_WIDTH_PERCENTAGE);
    int boardFlipColumnWidth    = (int)(totalColumnWidth * _BOARD_FLIP_COLUMN_WIDTH_PERCENTAGE);
    int boardZoffsetColumWidth  = (int)(totalColumnWidth * _BOARD_ZOFFSET_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_BOARD_NAME_INDEX).setPreferredWidth(boardNameColumnWidth);
    columnModel.getColumn(_BOARD_TYPE_INDEX).setPreferredWidth(boardTypeColumnWidth);
    columnModel.getColumn(_BOARD_X_INDEX).setPreferredWidth(boardXColumnWidth);
    columnModel.getColumn(_BOARD_Y_INDEX).setPreferredWidth(boardYColumnWidth);
    columnModel.getColumn(_BOARD_ROTATION_INDEX).setPreferredWidth(boardRotColumnWidth);
    columnModel.getColumn(_BOARD_FLIP_INDEX).setPreferredWidth(boardFlipColumnWidth);
    columnModel.getColumn(_BOARD_ZOFFSET_INDEX).setPreferredWidth(boardZoffsetColumWidth);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };
    columnModel.getColumn(_BOARD_NAME_INDEX).setCellEditor(stringEditor);

    columnModel.getColumn(_BOARD_X_INDEX).setCellEditor(_xNumericEditor);
    columnModel.getColumn(_BOARD_X_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(_BOARD_Y_INDEX).setCellEditor(_yNumericEditor);
    columnModel.getColumn(_BOARD_Y_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(_BOARD_ROTATION_INDEX).setCellEditor(_rotationEditor);

    columnModel.getColumn(_BOARD_FLIP_INDEX).setCellEditor(_checkEditor);
    columnModel.getColumn(_BOARD_FLIP_INDEX).setCellRenderer(new CheckCellRenderer());
    
    columnModel.getColumn(_BOARD_ZOFFSET_INDEX).setCellEditor(_zOffsetEditor);
    columnModel.getColumn(_BOARD_ZOFFSET_INDEX).setCellRenderer(_numericRenderer);
  }


  /**
   * The decimal places to be displayed can change based on the measurement value that
   * is being displayed.  This method changes the renderers/editors to display the appropriate
   * number of decimal places.
   * @param decimalPlaces Number of decimal places to be displayed for all measurement values in the table.
   * @author Carli Connally
   */
  public void setDecimalPlacesToDisplay(int decimalPlaces)
  {
    Assert.expect(decimalPlaces >= 0);

    _xNumericEditor.setDecimalPlaces(decimalPlaces);
    _yNumericEditor.setDecimalPlaces(decimalPlaces);
    _zOffsetEditor.setDecimalPlaces(decimalPlaces);
    _numericRenderer.setDecimalPlacesToDisplay(decimalPlaces);
  }

  /**
   * @return a list of selected boards.
   * @author George A. David
   */
  public java.util.List<Board> getSelectedBoards()
  {
    PanelLayoutTableModel model = (PanelLayoutTableModel)getModel();
    java.util.List<Board> selectedBoards = new ArrayList<Board>();
    int rowCount = getRowCount();

    for(int i = 0; i < rowCount; ++i)
    {
      if(selectionModel.isSelectedIndex(i))
      {
        selectedBoards.add(model.getBoard(i));
      }
    }

    return selectedBoards;
  }
}
