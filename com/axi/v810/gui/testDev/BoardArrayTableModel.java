package com.axi.v810.gui.testDev;

import java.util.List;
import javax.swing.table.*;

import com.axi.util.Assert;
import com.axi.v810.business.panelDesc.BoardType;
import com.axi.v810.util.StringLocalizer;

/**
 *
 * <p>Title: Board Array Table Model</p>
 * <p>Description: Contains the data for display in the board array table.</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company:  Agilent Technogies</p>
 * @author Carli Connally
 * @version 1.0
 */
class BoardArrayTableModel extends AbstractTableModel
{
  //Column label strings.  Retrieved from localization properties file.
  private static final String BOARD_NAME_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_BOARD_NAME_KEY");
  private static final String ROWS_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_BOARD_TABLE_ROWS_KEY");
  private static final String COLUMNS_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_BOARD_TABLE_COLUMNS_KEY");
  private static final String ROTATION_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_ROTATION_KEY");
  private static final String FLIPPED_LABEL = StringLocalizer.keyToString("MMGUI_SETUP_FLIPPED_KEY");
  private static final String INVALID_INDEX_ERROR = StringLocalizer.keyToString("MMGUI_SETUP_INVALID_INDEX_ERROR_KEY");

  private String[] _columnLabels = { BOARD_NAME_LABEL,
                                     ROWS_LABEL,
                                     COLUMNS_LABEL,
                                     ROTATION_LABEL,
                                     FLIPPED_LABEL };


  List<BoardType> _boardTypes = null;
  BoardType _selectedBoardType = null;
  private int _rows = 0;
  private int _columns = 0;
  private int _rotation = 0;
  private boolean _isFlipped = false;

  /**
   * @author George A. David
   */
  public BoardArrayTableModel(List<BoardType> boardTypes)
  {
    //automatically select the first board type in the list
    Assert.expect(boardTypes != null && boardTypes.size() > 0, "Empty list of board types.");
    _boardTypes = boardTypes;
    _selectedBoardType = _boardTypes.get(0);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void clear()
  {
    _boardTypes.clear();
    _selectedBoardType = null;
  }

  /**
   * Overridden method.
   * @return number of columns in the table model to be displayed.
   * @author Carli Connally
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @param column Index of the column desired
   * @return String label for the column
   * @author Carli Connally
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @param columnIndex  Index of the column desired
   * @return Class data type for the column
   * @author Carli Connally
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * Overridden method.
   * @param rowIndex  Index of the row desired
   * @param columnIndex  Index of the column desired
   * @return Value to display at the specified row and column
   * @author Carli Connally
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    switch(columnIndex)
    {
      case BoardArrayTable.BOARD_TYPE_COLUMN_INDEX:
      {
        value = _selectedBoardType;
        break;
      }
      case BoardArrayTable.ROWS_COLUMN_INDEX:
      {
        value = new Integer(_rows);
        break;
      }
      case BoardArrayTable.COLUMNS_COLUMN_INDEX:
      {
        value = new Integer(_columns);
        break;
      }
      case BoardArrayTable.ROTATION_COLUMN_INDEX:
      {
        value = new Integer(_rotation);
        break;
      }
      case BoardArrayTable.FLIPPED_COLUMN_INDEX:
      {
        value = new Boolean(_isFlipped);
        break;
      }
      default:
      {
        Assert.expect(false, INVALID_INDEX_ERROR);
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Carli Connally
   */
  public int getRowCount()
  {
    return 1;
  }

  /**
   * @author Carli Connally
   * Overridden method.
   * @param rowIndex Index of the desired row
   * @param columnIndex Index of the desired column
   * @return True if the cell is editable and false if the cell is not editable
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return true;
  }

  /**
   * @author Carli Connally
   * Overridden method.  This method "saves" the value entered by the user in the table
   * to the table model.
   * @param object Data object with the value
   * @param rowIndex Index of the desired row
   * @param columnIndex Index of the desired column
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    // this can happen if the boardType combo box is open when the user cancels out of the dialog
    if (object == null)
      return;

    switch(columnIndex)
    {
      case BoardArrayTable.BOARD_TYPE_COLUMN_INDEX:
      {
        _selectedBoardType = (BoardType)object;
        break;
      }
      case BoardArrayTable.ROWS_COLUMN_INDEX:
      {
        _rows = 0;
        if (object instanceof Number)
          _rows = ((Number)object).intValue();
        else
          Assert.expect(false, "Wrong type of object: " + object);
        break;
      }
      case BoardArrayTable.COLUMNS_COLUMN_INDEX:
      {
        _columns = 0;
        if (object instanceof Number)
          _columns = ((Number)object).intValue();
        else
          Assert.expect(false, "Wrong type of object: " + object);
        break;
      }
      case BoardArrayTable.ROTATION_COLUMN_INDEX:
      {
        _rotation = Integer.parseInt((String)object);
        break;
      }
      case BoardArrayTable.FLIPPED_COLUMN_INDEX:
      {
        _isFlipped = ((Boolean)object).booleanValue();
        break;
      }
      default:
      {
        Assert.expect(false, INVALID_INDEX_ERROR);
      }
    }
  }

}
