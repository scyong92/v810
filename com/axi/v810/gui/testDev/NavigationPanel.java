package com.axi.v810.gui.testDev;

import com.axi.util.*;
import com.axi.v810.gui.mainMenu.*;

/**
 * This contains the navigation buttons specific to the Test Development Tool
 * @author Andy Mechtenberg
 */
public class NavigationPanel extends AbstractNavigationPanel
{
  /**
   * @author Andy Mechtenberg
   */
  public NavigationPanel(String[] buttonNames, String[] disabledButtonNames, AbstractEnvironmentPanel envPanel)
  {
    super(buttonNames, disabledButtonNames, envPanel);
    Assert.expect(buttonNames != null);
    Assert.expect(disabledButtonNames != null);
    Assert.expect(buttonNames.length == disabledButtonNames.length);
    Assert.expect(envPanel != null);
  }
}
