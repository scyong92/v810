package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.SwingWorkerThread;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

import java.util.List;

/**
 * @author chin-seong.kee
 */
public class ApplySettingVariationSelectionDialog extends EscapeDialog
{
  private JScrollPane _subtypeScrollPane = new JScrollPane();

  private JButton _okButton = new JButton();

  private JMenuItem _undoMenuItem = new JMenuItem();
  private JMenuItem _redoMenuItem = new JMenuItem();
  private MainMenuGui _parentComponent;

  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  private KeyStroke _undoKeyStroke = javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_MASK);
  private KeyStroke _redoKeyStroke = javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_Y, KeyEvent.CTRL_MASK);

  private JPanel _mainPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _tablePanel = new JPanel();
  
  private ApplySettingVariationSelectionTable _noLoadSettingLayoutTable = new ApplySettingVariationSelectionTable();
  private ApplySettingVariationSelectionTableModel _noLoadSettingLayoutTableModel = new ApplySettingVariationSelectionTableModel();
  private List<ComponentType> _componentList = new ArrayList<ComponentType>();
  
  private Project _project = null;
  
  
  /*
  * @author Kee Chin Seong
  */
  public ApplySettingVariationSelectionDialog(Project project, Frame parent, List<ComponentType> componentList, String title)
  {
    super(parent, title, true);

    _parentComponent = (MainMenuGui)parent;
    _project = project;
    _componentList = componentList;
    
    jbInit();
  }

  /**
   * @author Chin Seong
   */
  private void jbInit()
  {
     _tablePanel.setLayout(new BorderLayout());
     _tablePanel.add(_subtypeScrollPane, BorderLayout.CENTER);

     _buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
     _buttonPanel.add(_okButton);
     _okButton.setText(StringLocalizer.keyToString("MMGUI_SELECT_SUBTYPE_BUTTON_KEY"));

     _okButton.addActionListener(new java.awt.event.ActionListener()
     {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
     });

     setupMenuItem();
          
     _noLoadSettingLayoutTable.setModel(_noLoadSettingLayoutTableModel);
     _subtypeScrollPane.getViewport().add(_noLoadSettingLayoutTable);
     
     _noLoadSettingLayoutTableModel.setData(_project);
     _noLoadSettingLayoutTableModel.setComponentTypeList(_componentList);
     _noLoadSettingLayoutTableModel.fireTableDataChanged();

     _mainPanel.setLayout(new BorderLayout());
     _mainPanel.add(_tablePanel, BorderLayout.CENTER);
     _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);

     this.getContentPane().add(_mainPanel);

     ColumnResizer.adjustColumnPreferredWidths(_noLoadSettingLayoutTable, true);
     setSize(new Dimension((int)_noLoadSettingLayoutTable.getPreferredSize().getWidth() * 4, (int)getPreferredSize().getHeight()));
  }
  
  /**
   * @author Chin Seong
   */
  private void setupMenuItem()
  {
      JPanel menuPanel = new JPanel(new BorderLayout());
      menuPanel.setBorder(BorderFactory.createLineBorder(Color.black));
      JMenuBar menuBar = new JMenuBar();
      JMenu menu = new JMenu(StringLocalizer.keyToString("GUI_EDIT_MENU_KEY"));

     _undoMenuItem.setText(StringLocalizer.keyToString("GUI_UNDO_MENU_KEY"));
     _undoMenuItem.setAccelerator(_undoKeyStroke);
     _undoMenuItem.addActionListener(new java.awt.event.ActionListener()
     {
       public void actionPerformed(ActionEvent e)
       {
         undoMenuItem_actionPerformed(e);
       }
     });

     _redoMenuItem.setText(StringLocalizer.keyToString("GUI_REDO_MENU_KEY"));
     _redoMenuItem.setAccelerator(_redoKeyStroke);
     _redoMenuItem.addActionListener(new java.awt.event.ActionListener()
     {
       public void actionPerformed(ActionEvent e)
       {
         redoMenuItem_actionPerformed(e);
       }
     });

     menuBar.add(menu);
     menu.add(_undoMenuItem);
     menu.add(_redoMenuItem);
     menuPanel.add(menuBar, BorderLayout.WEST);
     _mainPanel.add(menuPanel, BorderLayout.NORTH);
  }

 /**
   * @author Chin Seong
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    _noLoadSettingLayoutTableModel.getVariationAppliedSettings();
    
     final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(this,
                                      StringLocalizer.keyToString("ECP_BUSYCANCELDIALOG_SETTING_TABLE_VALUES_MESSAGE_KEY"),
                                      StringLocalizer.keyToString("ECP_BUSYCANCELDIALOG_SETTING_TABLE_VALUES_TITLE_KEY"),
                                      true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, this);
    busyCancelDialog.setCursor(new Cursor(Cursor.WAIT_CURSOR));

    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {  
            for(VariationSetting variation : _project.getPanel().getPanelSettings().getAllVariationSettings())
            {
                if(_noLoadSettingLayoutTableModel.getVariationAppliedSettings().get(variation) == true)
                {
                    for(ComponentType compType : _componentList)
                    {
                       for(com.axi.v810.business.panelDesc.Component comp : compType.getComponents())
                       {
                           if(comp.isLoaded() == true)
                           {
                              if(variation.getBoardTypeToRefDesMap().get(comp.getBoard().getBoardType().getName()).contains(comp.getReferenceDesignator()))
                              {
                                  variation.getBoardTypeToRefDesMap().get(comp.getBoard().getBoardType().getName()).remove(comp.getReferenceDesignator());
                              }
                           }
                           else
                           {
                              if(variation.getBoardTypeToRefDesMap().get(comp.getBoard().getBoardType().getName()).contains(comp.getReferenceDesignator()) == false)
                              {
                                variation.getBoardTypeToRefDesMap().get(comp.getBoard().getBoardType().getName()).add(comp.getReferenceDesignator());
                              }
                           }
                       }
                    }
                }
            }
        }
        finally
        {
            while (busyCancelDialog.isVisible() == false)
            {
              try
              {
                Thread.sleep(200);
              }
              catch (InterruptedException ex)
              {
                // do nothing
              }
            }
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                busyCancelDialog.dispose();
              }
            });
        }
      }
    });
    busyCancelDialog.setVisible(true);
    unpopulate();
  }

 /**e
   * @author Chin Seong
   */
  public void undoMenuItem_actionPerformed(ActionEvent e)
  {
    _parentComponent.getInstance().getMainMenuBar().getUndoKeystrokeHandler().undo(_commandManager);
  }

  /**
   * @author Chin Seong
   */
  public void redoMenuItem_actionPerformed(ActionEvent e)
  {
    _parentComponent.getInstance().getMainMenuBar().getUndoKeystrokeHandler().redo(_commandManager);
  }

  public void unpopulate()
  {
    _noLoadSettingLayoutTableModel.clearProjectData();
    dispose();
  }

}
