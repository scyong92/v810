package com.axi.v810.gui.testDev;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

import com.axi.v810.util.*;

/**
 * This class is the data model for the package table for the Groupings panel and it also
 * converts to the component table model in the groupings panel
 * @author Andy Mechtenberg
 */
public class ModifySubtypesUnTestableReasonsTableModel extends DefaultSortTableModel
{
  private List<ComponentType> _componentTypes = new ArrayList<ComponentType>();
  private List<PadType> _componentPads = new ArrayList<PadType>();  // list of PadType objects
  private List<Pad> _pads = new ArrayList<Pad>();  // list of PadType objects
  private List<PadType> _currentlySelectedPadTypes = new ArrayList<PadType>();
  private List<String> _unTestableReasons = new ArrayList<String>(); // list of untestable reasons
  
  private final String[] _componentTableColumns = {StringLocalizer.keyToString("RDGUI_BOARD_NAME_COLUMN_KEY"),
                                                  StringLocalizer.keyToString("ATGUI_JOINT_KEY"),
                                                  StringLocalizer.keyToString("ATGUI_FAMILY_KEY"),
                                                  StringLocalizer.keyToString("ATGUI_SUBTYPE_KEY"),
                                                  StringLocalizer.keyToString("MMGUI_GROUPINGS_UNTESTABLE_REASONS_KEY")};

  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;

  /**
   * @author Andy Mechtenberg
   */
  public ModifySubtypesUnTestableReasonsTableModel()
  {
    // do nothing
    _componentPads = new ArrayList<PadType>();
    _unTestableReasons = new ArrayList<String>();
    _pads = new ArrayList<Pad>();
  }

  /**
   * @author Andy Mechtenberg
   */
  void clear()
  {
    if (_componentPads != null)
      _componentPads.clear();
    if (_currentlySelectedPadTypes != null)
      _currentlySelectedPadTypes.clear();
    if (_unTestableReasons != null)
      _unTestableReasons.clear();
    if (_componentTypes != null)
      _componentTypes.clear();
    if (_pads != null)
      _pads.clear();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void populateWithComponentData(Collection<ComponentType> componentTypes)
  {
    Assert.expect(componentTypes != null);
    Assert.expect(componentTypes.isEmpty() == false);

    _componentTypes = new ArrayList<ComponentType>(componentTypes);
    _componentPads = _componentTypes.get(0).getPadTypes();
    _pads.clear();
 
    for (PadType padtype : _componentPads)
    {
      for (Pad pad : padtype.getPads())
      {
        _pads.add(pad);
        _unTestableReasons.add(pad.getPadSettings().getUntestableReason());
      }
    }
    sortColumn(_currentSortColumn, _currentSortAscending);
  }

  /**
   * @author Andy Mechtenberg
   */
  void updateSpecificPadType(PadType padType)
  {
    Assert.expect(padType != null);
    if (_componentTypes.isEmpty())  // not displaying anything, so nothing to update
      return;
    int padTypeRow = getRowForComponentPad(padType);
    fireTableRowsUpdated(padTypeRow, padTypeRow);
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean isComponentPadDisplayed(PadType padType)
  {
    Assert.expect(padType != null);
    // here, we might have more than one component type selected, so we'll have to look through each one
    int rowIndex = -1;
    String padName = padType.getName();
    for(PadType tablePadType : _componentPads)
    {
      if (tablePadType.getName().equalsIgnoreCase(padName))
        return true;
    }
    return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  int getRowForComponentPad(PadType padType)
  {
    Assert.expect(padType != null);
    // here, we might have more than one component type selected, so we'll have to look through each one
    int rowIndex = -1;
    String padName = padType.getName();
    for(PadType tablePadType : _componentPads)
    {
      if (tablePadType.getName().equalsIgnoreCase(padName))
        return _componentPads.lastIndexOf(tablePadType);
    }
//    Assert.expect(false, "Should have found pad type: " + padType + " in list: " + _componentTypes);
    return rowIndex;
  }

  /**
   * @author Andy Mechtenberg
   */
  PadType getPadTypeAt(int row)
  {
//    Assert.expect(_componentPads != null);
//    return _componentPads.get(row);
    Assert.expect(_componentPads != null);
    Assert.expect(_pads != null);
    
    return _pads.get(row).getPadType();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void sortColumn(int column, boolean ascending)
  {
    if (column == 0)
      Collections.sort(_pads, new PadBoardNameComparator(ascending, false, true));
    else if (column == 1)
      Collections.sort(_pads, new AlphaNumericComparator(ascending));
    else if (column == 2)
      Collections.sort(_pads, new PadBoardNameComparator(ascending, true, false));
    else if (column == 3)
      Collections.sort(_pads, new PadBoardNameComparator(ascending, false, false));
    else if (column == 4)
      Collections.sort(_pads, new PadBoardNameComparator(ascending, false, false));
    
    _currentSortColumn = column;
    _currentSortAscending = ascending;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getColumnCount()
  {
    return _componentTableColumns.length;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getRowCount()
  {
    if (_pads == null)
      return 0;
    else
      return _pads.size();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_componentTableColumns[columnIndex];
  }
  
   /**
   * @author Andy Mechtenberg
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    if(rowIndex < 0)
      return false;

    if (columnIndex == 0) 
      return false;
    else if (columnIndex == 1) 
      return false;
    else if (columnIndex == 2)
      return false;
    else if (columnIndex == 3)
      return false;
    else if (columnIndex == 4)
      return false;
 
    return false;
  }

   /**
   * @author Chnee Khang Wah
   */
  public synchronized Object getValueAt(int rowIndex, int columnIndex)
  {
    // this needs to handle 3 columns
    // column 0:  Joint Name - this should be the same for all components represeneted, so use any
    // column 1:  Joint Type - again, same for all (based on a common CompPackage)
    // column 2:  Subtype -- at the pad level, this is simple
    // column 3:  UnTestable Reasons 

    //PadType padType = _componentPads.get(rowIndex);
    Pad pad = _pads.get(rowIndex);
  
    if (columnIndex == 0) 
    {
      String[] boardName = pad.getBoardAndComponentAndPadName().split(" ");
      return boardName[0];
    }
    else if (columnIndex == 1)
      return pad.getPadType();
    else if (columnIndex == 2)
      return pad.getPadType().getPackagePin().getJointTypeEnum();
    else if (columnIndex == 3) // test state or subtype
      return pad.getPadType().getSubtype().getShortName();
    else if (columnIndex == 4) // unTestable reasons
        if (pad.isTestable())
            return "---";
        else
            return pad.getPadSettings().getUntestableReason();
  
    return null;
  }
  
  /**
   * @author Andy Mechtenberg
   */
  private PadType getPadType(ComponentType componentType, PadType basePadType)
  {
    String padName = basePadType.getName();
    for(PadType componentPadType : componentType.getPadTypes())
    {
      if (componentPadType.getName().equalsIgnoreCase(padName))
        return componentPadType;
    }
    Assert.expect(false, "Unable to find pad type for : " + componentType + " " + basePadType);
    return null;
  }
  
  /**
   * @author Andy Mechtenberg
   */
  private String getUnTestableReasons(ComponentType componentType, PadType basePadType)
  {
    String padName = basePadType.getName();
    for(PadType componentPadType : componentType.getPadTypes())
    {
      if (componentPadType.getName().equalsIgnoreCase(padName))
        return componentPadType.getPads().get(0).getPadSettings().getUntestableReason();
    }
    Assert.expect(false, "Unable to find untestable reasons for : " + componentType + " " + basePadType);
    return null;
  }

  /**
   * @author Andy Mechtenberg
   */
  List<PadType> getSelectedPadTypes()
  {
    return _currentlySelectedPadTypes;
  }

  /**
   * @author Andy Mechtenberg
   */
  void setSelectedPadTypes(List<PadType> selectedPads)
  {
    Assert.expect(selectedPads != null);
    _currentlySelectedPadTypes = selectedPads;
  }
  
  /**
   * @author George Booth
   */
  public int getRowForDataStartingWith(String key)
  {
    key = key.toLowerCase();
    int index = -1;
    for (PadType padType : _componentPads)
    {
      index++;
      String padName = padType.getName().toLowerCase();
      if (padName.startsWith(key))
      {
        return index;
      }
    }
    return -1;
  }

}
