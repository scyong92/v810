package com.axi.v810.gui.testDev;

import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * Holds the data model for any new fiducials added to the panel or board(s)
 * @author Laura Cormos
 */
class NewFiducialTableModel extends DefaultSortTableModel
{
  // command manager for undo functionality
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private List<Fiducial> _fiducials = new ArrayList<Fiducial>();
  private static final String _NAME_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_FIDUCIAL_NAME_KEY");
  private static final String _LOCATION_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_LOCATION_KEY");
  private static final String _XLOC_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_X_LOC_KEY");
  private static final String _YLOC_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_Y_LOC_KEY");
  private static final String _DX_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_DX_KEY");
  private static final String _DY_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUP_DY_KEY");
  private static final String _SHAPE_COLUMN_HEADING = StringLocalizer.keyToString("MMGUI_SETUPI_SHAPE_KEY");

  private final String[] _tableColumns = {_NAME_COLUMN_HEADING,
                                         _LOCATION_COLUMN_HEADING,
                                         _XLOC_COLUMN_HEADING,
                                         _YLOC_COLUMN_HEADING,
                                         _DX_COLUMN_HEADING,
                                         _DY_COLUMN_HEADING,
                                         _SHAPE_COLUMN_HEADING};
  private MainMenuGui _mainUI;
  private TestDev _testDev = null;
  private EditCadPanel _parent = null;
  private Project _project = null;
  private Set<Fiducial> _selectedFiducials = new HashSet<Fiducial>();
  private FiducialComparatorEnum _currentSortOrder = FiducialComparatorEnum.NAME;
  private boolean _sortAscending = true;
  private final int _FIVE_MIL_IN_NANOS = 127000;


  /**
   * author Laura Cormos
   */
  NewFiducialTableModel(TestDev testDev, EditCadPanel parent)
  {
    Assert.expect(testDev != null);
    Assert.expect(parent != null);

    _testDev = testDev;
    _mainUI = _testDev.getMainUI();
    _parent = parent;
  }

  /**
   * @author Laura Cormos
   */
  void addFiducial(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);
    Assert.expect(_fiducials != null);

    _fiducials.add(fiducial);
    Collections.sort(_fiducials, new FiducialComparator(_sortAscending, _currentSortOrder));
    fireTableDataChanged();
  }

  /**
   * @author Laura Cormos
   */
  void setProject(Project project)
  {
    Assert.expect(project != null);
    _project = project;
  }

  /**
   * @author Laura Cormos
   */
  public void sortColumn(int column, boolean ascending)
  {
    _sortAscending = ascending;
    switch(column)
     {
       case 0: // name
         _currentSortOrder = FiducialComparatorEnum.NAME;
         Collections.sort(_fiducials, new FiducialComparator(ascending, FiducialComparatorEnum.NAME));
         break;
       case 1: // location
         _currentSortOrder = FiducialComparatorEnum.LOCATION;
         Collections.sort(_fiducials, new FiducialComparator(ascending, FiducialComparatorEnum.LOCATION));
         break;

       case 2: // x location
         _currentSortOrder = FiducialComparatorEnum.X_COORDINATE;
         Collections.sort(_fiducials, new FiducialComparator(ascending, FiducialComparatorEnum.X_COORDINATE));
         break;
       case 3: // y location
         _currentSortOrder = FiducialComparatorEnum.Y_COORDINATE;
         Collections.sort(_fiducials, new FiducialComparator(ascending, FiducialComparatorEnum.Y_COORDINATE));
         break;
       case 4: // width
         _currentSortOrder = FiducialComparatorEnum.WIDTH;
         Collections.sort(_fiducials, new FiducialComparator(ascending, FiducialComparatorEnum.WIDTH));
         break;
       case 5: // length
         _currentSortOrder = FiducialComparatorEnum.LENGTH;
         Collections.sort(_fiducials, new FiducialComparator(ascending, FiducialComparatorEnum.LENGTH));
         break;
       case 6: // shape
         _currentSortOrder = FiducialComparatorEnum.SHAPE;
         Collections.sort(_fiducials, new FiducialComparator(ascending, FiducialComparatorEnum.SHAPE));
         break;
       default:
         Assert.expect(false, "column does not exist in table model");
         break;
    }
  }

  /**
   * @author Laura Cormos
   */
  public int getColumnCount()
  {
    return _tableColumns.length;
  }

  /**
   * @author Laura Cormos
   */
  public int getRowCount()
  {
    if (_fiducials == null)
      return 0;
    else
      return _fiducials.size();
  }

  /**
   * @author Laura Cormos
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return(String)_tableColumns[columnIndex];
  }

  /**
   * @author Laura Cormos
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if (_selectedFiducials.contains(_fiducials.get(rowIndex)) == false)
      return false;

    if (columnIndex == NewFiducialTable._FIDUCIAL_LOC_INDEX) // the fiducial location column is for information purposes only
      return false;
    else
      return true;
  }

  /**
   * @author Laura Cormos
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_fiducials != null);

    Fiducial fiducial = _fiducials.get(rowIndex);
    double mils;
    switch(columnIndex)
    {
      case 0: // name
        return fiducial.getName();
      case 1: // type: board or panel
        if (fiducial.isOnBoard())
          return new String(StringLocalizer.keyToString(new LocalizedString("TM_GUI_FIDUCIAL_BOARD_LOCATION_KEY",
                                                                            new Object[]{fiducial.getSideBoard().getBoard().getName()})));
        else
          return StringLocalizer.keyToString("TM_GUI_FIDUCIAL_PANEL_LOCATION_KEY");
      case 2: // X location relative to board or panel origin depending on the fiducial kind
        int x;
        if (fiducial.isOnBoard())
          x = fiducial.getBoardCoordinateInNanoMeters().getX();
        else
          x = fiducial.getPanelCoordinateInNanoMeters().getX();
        mils = MathUtil.convertUnits(x, MathUtilEnum.NANOMETERS, _project.getDisplayUnits());
        return mils;
      case 3: // Y location relative to board origin or panel origin depending on the fiducial kind
        int y;
        if (fiducial.isOnBoard())
          y = fiducial.getBoardCoordinateInNanoMeters().getY();
        else
          y = fiducial.getPanelCoordinateInNanoMeters().getY();
        mils = MathUtil.convertUnits(y, MathUtilEnum.NANOMETERS, _project.getDisplayUnits());
        return mils;
      case 4: // DX
//        int dx = fiducial.getWidthAfterAllRotationsInNanoMeters();
        int dx = fiducial.getWidthInNanoMeters();
//        int dy = fiducial.getWidthInNanoMeters();
        mils = MathUtil.convertUnits(dx, MathUtilEnum.NANOMETERS, _project.getDisplayUnits());
        return mils;
      case 5: // DY
        int dy = fiducial.getLengthInNanoMeters();
//        int dy = fiducial.getLengthAfterAllRotationsInNanoMeters();
//        int dy = fiducial.getLengthInNanoMeters();
        mils = MathUtil.convertUnits(dy, MathUtilEnum.NANOMETERS, _project.getDisplayUnits());
        return mils;
      case 6: // shape
        ShapeEnum shapeEnum = fiducial.getShapeEnum();
        if (shapeEnum.equals(ShapeEnum.CIRCLE))
          return StringLocalizer.keyToString("CAD_CIRCLE_SHAPE_KEY");
        else
          return StringLocalizer.keyToString("CAD_RECT_SHAPE_KEY");
      default:
        Assert.expect(false);
        return null;
    }
  }

  /**
   * @author  Laura Cormos
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    Assert.expect(aValue != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    Assert.expect(_project != null);
    Assert.expect(_fiducials != null);

    if (rowIndex >= _fiducials.size())
      return;
    Fiducial fiducial = _fiducials.get(rowIndex);
    // we are modifying fiducial type here so that the changed value is propagated to all fiducials of this type
    FiducialType fiducialType = fiducial.getFiducialType();
    BoardCoordinate boardCoordinateInNanometers = null;
    PanelCoordinate panelCoordinateInNanometers = null;
    String valueStr;
    Double value;
    switch(columnIndex)
    {
      case 0: // name
        Assert.expect(aValue instanceof String);
        valueStr = (String)aValue;
        if (fiducial.isOnBoard())
        {
          BoardType boardType = fiducial.getSideBoard().getBoard().getBoardType();
          if (boardType.isFiducialTypeNameValid(valueStr) == false)
          {
            String illegalChars = boardType.getFiducialTypeNameIllegalChars();
            LocalizedString message = null;
            if (illegalChars.equals(" "))
              message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_SPACES_KEY", new Object[]{valueStr});
            else
              message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_OTHER_CHARS_KEY", new Object[]{valueStr, illegalChars});
            JOptionPane.showMessageDialog(_mainUI,
                                          StringLocalizer.keyToString(message),
                                          StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                          JOptionPane.INFORMATION_MESSAGE);
            return;
          }
          else if (boardType.isFiducialTypeNameDuplicate(valueStr))
          {
            JOptionPane.showMessageDialog(_mainUI,
                                          StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
                                                                                          new Object[]{valueStr})),
                                          StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                          JOptionPane.INFORMATION_MESSAGE);
            return;
          }
        }
        else
        {
          Panel panel = fiducial.getPanel();
          if (panel.isFiducialNameValid(valueStr) == false)
          {
            String illegalChars = panel.getFiducialNameIllegalChars();
            LocalizedString message = null;
            if (illegalChars.equals(" "))
              message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_SPACES_KEY", new Object[]{valueStr});
            else
              message = new LocalizedString("CAD_INVALID_FIDUCIAL_NAME_OTHER_CHARS_KEY", new Object[]{valueStr, illegalChars});
            JOptionPane.showMessageDialog(_mainUI,
                                          message,
                                          StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                          JOptionPane.INFORMATION_MESSAGE);
            return;
          }
          else if (panel.isFiducialNameDuplicate(valueStr))
          {
            JOptionPane.showMessageDialog(_mainUI,
                                          StringLocalizer.keyToString(new LocalizedString("CAD_DUPLICATE_NAME_KEY",
                                                                                          new Object[]{valueStr})),
                                          StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                          JOptionPane.INFORMATION_MESSAGE);
            return;
          }
        }

        try
         {
           _commandManager.trackState(_parent.getCurrentUndoState());
           _commandManager.execute(new FiducialTypeSetNameCommand(fiducialType, valueStr));
         }
         catch (XrayTesterException ex)
         {
           MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                         StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
           fireTableCellUpdated(rowIndex, columnIndex);
         }
        break;
      case 1:
        // changing a fiducial between board and panel is not allowed
        break;
      case 2: // X location relative to board or panel CAD origin
        Assert.expect(aValue instanceof Number);
        value = ((Number)aValue).doubleValue();
        double convertedXValue = MathUtil.convertUnits(value, _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);

        if (fiducial.isOnBoard())
        {
          Board board = fiducial.getSideBoard().getBoard();
          Assert.expect(board != null);
          if (convertedXValue < fiducial.getWidthInNanoMeters()/2 ||
              convertedXValue > (board.getWidthInNanoMeters() - fiducial.getWidthInNanoMeters()/2))
          {
            int result = JOptionPane.showOptionDialog(_mainUI,
                StringLocalizer.keyToString(new LocalizedString("CAD_X_COORD_OUT_OF_BOUNDS_KEY",
                                                                new Object[]{StringLocalizer.keyToString("CAD_FIDUCIAL_KEY"),
                                                                             fiducial.getName(),
                                                                             StringLocalizer.keyToString("CAD_BOARD_KEY")})),
                StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                null,
                new Object[]{StringLocalizer.keyToString("GUI_ACCEPT_BUTTON_KEY"), StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")},
                null);

            if (result == JOptionPane.CLOSED_OPTION || result == 1 /** this means user chose the NO (which we named 'Cancel') button */)
              return;
          }
          boardCoordinateInNanometers = fiducial.getBoardCoordinateInNanoMeters();
          boardCoordinateInNanometers.setX((int)convertedXValue);
          try
          {
            _commandManager.trackState(_parent.getCurrentUndoState());
            _commandManager.execute(new FiducialTypeSetCoordinateInNanoMetersCommand(fiducialType, new PanelCoordinate(boardCoordinateInNanometers)));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
        }
        else if (fiducial.isOnPanel())
        {
          Panel panel = fiducial.getPanel();
          Assert.expect(panel != null);
          if (convertedXValue < fiducial.getWidthInNanoMeters()/2 ||
              convertedXValue > (panel.getWidthInNanoMeters() - fiducial.getWidthInNanoMeters()/2))
          {
            int result = JOptionPane.showOptionDialog(_mainUI,
                StringLocalizer.keyToString(new LocalizedString("CAD_X_COORD_OUT_OF_BOUNDS_KEY",
                                                                new Object[]{StringLocalizer.keyToString("CAD_FIDUCIAL_KEY"),
                                                                             fiducial.getName(),
                                                                             StringLocalizer.keyToString("CAD_PANEL_KEY")})),
                StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                null,
                new Object[]{StringLocalizer.keyToString("GUI_ACCEPT_BUTTON_KEY"), StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")},
                null);

            if (result == JOptionPane.CLOSED_OPTION || result == 1 /** this means user chose the NO (which we named 'Cancel') button */)
              return;
          }
          String errorMessage = AddFiducialDialog.isPanelFiducialWithinBoardBoundary(
              convertedXValue, fiducial.getPanelCoordinateInNanoMeters().getY(),
              fiducial.getWidthInNanoMeters(), fiducial.getLengthInNanoMeters());
          if (errorMessage.length() > 0)
          {
            int result = JOptionPane.showOptionDialog(_mainUI,
                errorMessage,
                StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                null,
                new Object[]{StringLocalizer.keyToString("GUI_ACCEPT_BUTTON_KEY"), StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")},
                null);

            if (result == JOptionPane.CLOSED_OPTION || result == 1 /** this means user chose the NO (which we named 'Cancel') button */)
              return;
          }
          panelCoordinateInNanometers = fiducial.getPanelCoordinateInNanoMeters();
          panelCoordinateInNanometers.setX((int)convertedXValue);
          try
          {
            _commandManager.trackState(_parent.getCurrentUndoState());
            _commandManager.execute(new FiducialTypeSetCoordinateInNanoMetersCommand(fiducialType, panelCoordinateInNanometers));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
        }
        else
          Assert.expect(false, "A fiducial has to be on either a board or the panel");
        break;
      case 3: // Y location relative to board or panel CAD origin (0,0)
        Assert.expect(aValue instanceof Number);
        value = ((Number)aValue).doubleValue(); // passed in as a number relative to the board origin
        double convertedYValue = MathUtil.convertUnits(value, _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);

        if (fiducial.isOnBoard())
        {
          Board board = fiducial.getSideBoard().getBoard();
          Assert.expect(board != null);
          if (convertedYValue < fiducial.getLengthInNanoMeters()/2 ||
              convertedYValue > (board.getLengthInNanoMeters() - fiducial.getLengthInNanoMeters()/2))
          {
            int result = JOptionPane.showOptionDialog(_mainUI,
                StringLocalizer.keyToString(new LocalizedString("CAD_Y_COORD_OUT_OF_BOUNDS_KEY",
                                                                new Object[]{StringLocalizer.keyToString("CAD_FIDUCIAL_KEY"),
                                                                             fiducial.getName(),
                                                                             StringLocalizer.keyToString("CAD_BOARD_KEY")})),
                StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                null,
                new Object[]{StringLocalizer.keyToString("GUI_ACCEPT_BUTTON_KEY"), StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")},
                null);

            if (result == JOptionPane.CLOSED_OPTION || result == 1 /** this means user chose the NO (which we named 'Cancel') button */)
              return;
          }
          boardCoordinateInNanometers = fiducial.getBoardCoordinateInNanoMeters();
          boardCoordinateInNanometers.setY((int)convertedYValue);
          try
          {
            _commandManager.trackState(_parent.getCurrentUndoState());
            _commandManager.execute(new FiducialTypeSetCoordinateInNanoMetersCommand(fiducialType, new PanelCoordinate(boardCoordinateInNanometers)));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
        }
        else if (fiducial.isOnPanel())
        {
          Panel panel = fiducial.getPanel();
          Assert.expect(panel != null);
          if (convertedYValue < fiducial.getLengthInNanoMeters()/2 ||
              convertedYValue > (panel.getLengthInNanoMeters() - fiducial.getLengthInNanoMeters()/2))
          {
            int result = JOptionPane.showOptionDialog(_mainUI,
                StringLocalizer.keyToString(new LocalizedString("CAD_Y_COORD_OUT_OF_BOUNDS_KEY",
                                                                 new Object[]{StringLocalizer.keyToString("CAD_FIDUCIAL_KEY"),
                                                                              fiducial.getName(),
                                                                              StringLocalizer.keyToString("CAD_PANEL_KEY")})),
                StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                null,
                new Object[]{StringLocalizer.keyToString("GUI_ACCEPT_BUTTON_KEY"), StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")},
                null);

            if (result == JOptionPane.CLOSED_OPTION || result == 1 /** this means user chose the NO (which we named 'Cancel') button */)
              return;
          }
          String errorMessage = AddFiducialDialog.isPanelFiducialWithinBoardBoundary(
              fiducial.getPanelCoordinateInNanoMeters().getX(), convertedYValue,
              fiducial.getWidthInNanoMeters(), fiducial.getLengthInNanoMeters());
          if (errorMessage.length() > 0)
          {
            int result = JOptionPane.showOptionDialog(_mainUI,
                errorMessage,
                StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                null,
                new Object[]{StringLocalizer.keyToString("GUI_ACCEPT_BUTTON_KEY"), StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")},
                null);

            if (result == JOptionPane.CLOSED_OPTION || result == 1 /** this means user chose the NO (which we named 'Cancel') button */)
              return;
          }
          panelCoordinateInNanometers = fiducial.getPanelCoordinateInNanoMeters();
          panelCoordinateInNanometers.setY((int)convertedYValue);
          try
          {
            _commandManager.trackState(_parent.getCurrentUndoState());
            _commandManager.execute(new FiducialTypeSetCoordinateInNanoMetersCommand(fiducialType, panelCoordinateInNanometers));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
        }
        else
          Assert.expect(false, "A fiducial has to be on either a board or the panel");

        break;
      case 4: // DX
        Assert.expect(aValue instanceof Number);
        value = ((Number)aValue).doubleValue();
        double convertedValueInNanos = MathUtil.convertUnits(value.doubleValue(), _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);

        if (convertedValueInNanos >= 0 && convertedValueInNanos < _FIVE_MIL_IN_NANOS)
        {
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString("CAD_FIDUCIAL_TOO_SMALL_MSG_KEY"),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.ERROR_MESSAGE);
          return;
        }

        if (fiducial.isOnBoard())
        {
          Board board = fiducial.getSideBoard().getBoard();
          Assert.expect(board != null);
          if ((convertedValueInNanos/2 + fiducial.getBoardCoordinateInNanoMeters().getX()) > board.getWidthInNanoMeters())
          {
            int result = JOptionPane.showOptionDialog(
                _mainUI,
                StringLocalizer.keyToString(new LocalizedString("CAD_DX_COORD_OUT_OF_BOUNDS_KEY",
                                                                new Object[]{StringLocalizer.keyToString("CAD_FIDUCIAL_KEY"),
                                                                             fiducial.getName(),
                                                                             StringLocalizer.keyToString("CAD_BOARD_KEY")})),
                StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                null,
                new Object[]{StringLocalizer.keyToString("GUI_ACCEPT_BUTTON_KEY"),
                             StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")},
                null);

            if (result == JOptionPane.CLOSED_OPTION || result == 1 /** this means user chose the NO (which we named 'Cancel') button */)
              return;
          }
        }
        else if (fiducial.isOnPanel())
        {
          Panel panel = fiducial.getPanel();
          Assert.expect(panel != null);
          if ((convertedValueInNanos/2 + fiducial.getPanelCoordinateInNanoMeters().getX()) > panel.getWidthInNanoMeters())
          {
            int result = JOptionPane.showOptionDialog(
                _mainUI,
                StringLocalizer.keyToString(new LocalizedString("CAD_DX_COORD_OUT_OF_BOUNDS_KEY",
                                                                new Object[]{StringLocalizer.keyToString("CAD_FIDUCIAL_KEY"),
                                                                             fiducial.getName(),
                                                                             StringLocalizer.keyToString("CAD_PANEL_KEY")})),
                StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                null,
                new Object[]{StringLocalizer.keyToString("GUI_ACCEPT_BUTTON_KEY"),
                             StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")},
                null);
            if (result == JOptionPane.CLOSED_OPTION || result == 1 /** this means user chose the NO (which we named 'Cancel') button */)
              return;
          }
          String errorMessage = AddFiducialDialog.isPanelFiducialWithinBoardBoundary(
              fiducial.getPanelCoordinateInNanoMeters().getX(), fiducial.getPanelCoordinateInNanoMeters().getY(),
              convertedValueInNanos, fiducial.getLengthInNanoMeters());
          if (errorMessage.length() > 0)
          {
            int result = JOptionPane.showOptionDialog(_mainUI,
                errorMessage,
                StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                null,
                new Object[]{StringLocalizer.keyToString("GUI_ACCEPT_BUTTON_KEY"), StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")},
                null);

            if (result == JOptionPane.CLOSED_OPTION || result == 1 /** this means user chose the NO (which we named 'Cancel') button */)
              return;
          }

        }
        else
          Assert.expect(false, "A fiducial has to be on either a board or the panel");

        try
        {
          _commandManager.trackState(_parent.getCurrentUndoState());
          _commandManager.execute(new FiducialTypeSetWidthInNanoMetersCommand(fiducialType, (int)convertedValueInNanos));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }

        break;
      case 5: // DY
        Assert.expect(aValue instanceof Number);
        value = ((Number)aValue).doubleValue();
        convertedValueInNanos = MathUtil.convertUnits(value.doubleValue(), _project.getDisplayUnits(), MathUtilEnum.NANOMETERS);

        if (convertedValueInNanos >= 0 && convertedValueInNanos < _FIVE_MIL_IN_NANOS)
        {
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString("CAD_FIDUCIAL_TOO_SMALL_MSG_KEY"),
                                        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                        JOptionPane.ERROR_MESSAGE);
          return;
        }

        if (fiducial.isOnBoard())
        {
          Board board = fiducial.getSideBoard().getBoard();
          Assert.expect(board != null);
          if ((convertedValueInNanos / 2 + fiducial.getBoardCoordinateInNanoMeters().getY()) > board.getLengthInNanoMeters())
          {
            int result = JOptionPane.showOptionDialog(
                _mainUI,
                StringLocalizer.keyToString(new LocalizedString("CAD_DY_COORD_OUT_OF_BOUNDS_KEY",
                                                                new Object[]{StringLocalizer.keyToString("CAD_FIDUCIAL_KEY"),
                                                                             fiducial.getName(),
                                                                             StringLocalizer.keyToString("CAD_BOARD_KEY")})),
                StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                null,
                new Object[]{StringLocalizer.keyToString("GUI_ACCEPT_BUTTON_KEY"),
                             StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")},
                null);

            if (result == JOptionPane.CLOSED_OPTION || result == 1 /** this means user chose the NO (which we named 'Cancel') button */)
              return;
          }
        }
        else if (fiducial.isOnPanel())
        {
          Panel panel = fiducial.getPanel();
          Assert.expect(panel != null);
          if ((convertedValueInNanos / 2 + fiducial.getPanelCoordinateInNanoMeters().getY()) > panel.getLengthInNanoMeters())
          {
            int result = JOptionPane.showOptionDialog(
                _mainUI,
                StringLocalizer.keyToString(new LocalizedString("CAD_DY_COORD_OUT_OF_BOUNDS_KEY",
                                                                new Object[]{StringLocalizer.keyToString("CAD_FIDUCIAL_KEY"),
                                                                             fiducial.getName(),
                                                                             StringLocalizer.keyToString("CAD_PANEL_KEY")})),
                StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                null,
                new Object[]{StringLocalizer.keyToString("GUI_ACCEPT_BUTTON_KEY"),
                             StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")},
                null);

            if (result == JOptionPane.CLOSED_OPTION || result == 1 /** this means user chose the NO (which we named 'Cancel') button */)
              return;
          }
          String errorMessage = AddFiducialDialog.isPanelFiducialWithinBoardBoundary(
              fiducial.getPanelCoordinateInNanoMeters().getX(), fiducial.getPanelCoordinateInNanoMeters().getY(),
              fiducial.getWidthInNanoMeters(), convertedValueInNanos);
          if (errorMessage.length() > 0)
          {
            int result = JOptionPane.showOptionDialog(_mainUI,
                errorMessage,
                StringLocalizer.keyToString("CAD_WARNING_TITLE_KEY"),
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                null,
                new Object[]{StringLocalizer.keyToString("GUI_ACCEPT_BUTTON_KEY"), StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY")},
                null);

            if (result == JOptionPane.CLOSED_OPTION || result == 1 /** this means user chose the NO (which we named 'Cancel') button */)
              return;
          }

        }
        else
          Assert.expect(false, "A fiducial has to be on either a board or the panel");

        try
        {
          _commandManager.trackState(_parent.getCurrentUndoState());
          _commandManager.execute(new FiducialTypeSetLengthInNanoMetersCommand(fiducialType, (int)convertedValueInNanos));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      case 6: // shape
        Assert.expect(aValue instanceof String);
        valueStr = (String)aValue;
        if (valueStr.equals(StringLocalizer.keyToString("CAD_RECT_SHAPE_KEY")))
          try
          {
            _commandManager.trackState(_parent.getCurrentUndoState());
            _commandManager.execute(new FiducialTypeSetShapeEnumCommand(fiducialType, ShapeEnum.RECTANGLE));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
        else if (valueStr.equals(StringLocalizer.keyToString("CAD_CIRCLE_SHAPE_KEY")))
          try
          {
            _commandManager.trackState(_parent.getCurrentUndoState());
            _commandManager.execute(new FiducialTypeSetShapeEnumCommand(fiducialType, ShapeEnum.CIRCLE));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI,ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
        break;
      default:
        Assert.expect(false);
    }
  }

  /**
   * author Laura Cormos
   */
  void clearData()
  {
    Assert.expect(_fiducials != null);
    _fiducials.clear();
    _selectedFiducials.clear();
    fireTableDataChanged();
    _project = null;
  }

  /**
   * @author Laura Cormos
   * @return true if the fiducial was found in the list, false otherwise
   */
  boolean deleteFiducial(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);
    Assert.expect(_fiducials != null);

    if (_fiducials.remove(fiducial))
    {
      fireTableDataChanged();
      return true;
    }
    return false;
  }

  /**
   * @author Laura Cormos
   */
  void populateWithFiducials(List<Fiducial> fiducials)
  {
    Assert.expect(fiducials != null);
    Assert.expect(_fiducials != null);

    _fiducials.addAll(fiducials);
    Collections.sort(_fiducials, new FiducialComparator(_sortAscending, _currentSortOrder));
    fireTableDataChanged();
  }

  /**
   * author Laura Cormos
   */
  Fiducial getFiducialAt(int rowIndex)
  {
    Assert.expect(_fiducials != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(rowIndex < _fiducials.size());

    return _fiducials.get(rowIndex);
  }

  /**
   * @author Laura Cormos
   */
  int getRowForFiducial(Fiducial fiducial)
  {
    Assert.expect(_fiducials != null);
    Assert.expect(fiducial != null);

    return _fiducials.indexOf(fiducial);
  }

  /**
   * @author Laura Cormos
   */
  void setSelectedFiducials(Set<Fiducial> selectedFiducials)
  {
   Assert.expect(selectedFiducials != null);
   _selectedFiducials = selectedFiducials;
  }

  /**
   * @author Laura Cormos
   */
  void clearSelectedFiducials()
  {
    _selectedFiducials.clear();
  }

  /**
   * @author Laura Cormos
   */
  void deleteFiducials(List<Fiducial> fiducialsToRemove)
  {
    Assert.expect(fiducialsToRemove != null);
    Assert.expect(_fiducials != null);

    _fiducials.removeAll(fiducialsToRemove);
    fireTableDataChanged();
  }

  /**
   * @author Laura Cormos
   */
  void addFiducials(List<Fiducial> fiducialsToAdd)
  {
    Assert.expect(fiducialsToAdd != null);
    Assert.expect(_fiducials != null);
    Collections.sort(fiducialsToAdd, new FiducialComparator(true, FiducialComparatorEnum.FULLY_QUALIFIED_NAME));
    _fiducials.addAll(fiducialsToAdd);
    Collections.sort(_fiducials, new FiducialComparator(_sortAscending, _currentSortOrder));
    fireTableDataChanged();
  }
}
