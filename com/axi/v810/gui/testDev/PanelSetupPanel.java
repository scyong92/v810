package com.axi.v810.gui.testDev;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;
import java.io.File;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testGen.*;

import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.gui.virtualLive.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This is the Swing JPanel that displays the setup information for the physical Panel
 * @author Carli Connally
 */
public class PanelSetupPanel extends AbstractTaskPanel implements Observer
{
  private static final Integer _ROTATION_0 = new Integer(0);
  private static final Integer _ROTATION_90 = new Integer(90);
  private static final Integer _ROTATION_180 = new Integer(180);
  private static final Integer _ROTATION_270 = new Integer(270);

  private static final String _PANEL = StringLocalizer.keyToString("ATGUI_PANEL_KEY");
  private static final String _BOARD = StringLocalizer.keyToString("ATGUI_BOARD_KEY");

  private Project _project = null;
  private boolean _updateData = false;
  private MathUtilEnum _enumSelectedUnits = null;
  private boolean _populateProjectData = false;

  private MainMenuGui _mainUI = null;
  private TestDev _testDev = null;

  private FocusAdapter _textFieldFocusAdapter;
  private ActionListener _textFieldActionListener;
  private ActionListener _panelRotationActionListener;
  private ActionListener _unitsComboBoxActionListener;
  private ActionListener _panelFlipCheckBoxActionListener;
  private ActionListener _alignmentMethodActionListener;

  private PanelLayoutTable _panelLayoutTable = new PanelLayoutTable();
  private PanelLayoutTableModel _panelLayoutTableModel;
  private VariationSettingTable _noLoadSettingLayoutTable = new VariationSettingTable();
  private VariationSettingTableModel _noLoadSettingLayoutTableModel;

  private JPanel _allPanel = new JPanel();
  private JPanel _programOptionsPanel = new JPanel();
  private JPanel _panelSetupPanel = new JPanel();
  private JPanel _alignmentPanel = new JPanel();
  private JPanel _panelAndBoardOptionsPanel = new JPanel();
  private JPanel _boardOptionsPanel = new JPanel();
  private JPanel _panelOptionsPanel = new JPanel();
  private JPanel _boardPlacementsPanel = new JPanel();
  private JPanel _addBoardPanel = new JPanel();
  private JPanel _panelLayoutTablePanel = new JPanel();
  private JPanel _noLoadSettingPanel = new JPanel();  //XCR - 2621, Kok Chun, Tan - Variant Management
  private JPanel _noLoadSettingLayoutTablePanel = new JPanel();  //XCR - 2621, Kok Chun, Tan - Variant Management
  private JPanel _addVariationPanel = new JPanel();  //XCR - 2621, Kok Chun, Tan - Variant Management
  private BorderLayout _mainBorderLayout = new BorderLayout();
  private BorderLayout _boardPlacementsPanelBorderLayout = new BorderLayout();
  private BorderLayout _panelLayoutTablePanelBorderLayout = new BorderLayout();
  private BorderLayout _alignmentPanelBorderLayout = new BorderLayout();
  private BorderLayout _panelAndBoardOptionsPanelBorderLayout = new BorderLayout();
  private BorderLayout _panelSetupPanelBorderLayout = new BorderLayout();
  private BorderLayout _noLoadSettingPanelBorderLayout = new BorderLayout();  //XCR - 2621, Kok Chun, Tan - Variant Management
  private BorderLayout _noLoadSettingLayoutTablePanelBorderLayout = new BorderLayout();  //XCR - 2621, Kok Chun, Tan - Variant Management
  private FlowLayout _programOptionsPanelFlowLayout = new FlowLayout();
  private BoxLayout _addBoardPanelBoxLayout;
  private BoxLayout _addVariationPanelBoxLayout;  //XCR - 2621, Kok Chun, Tan - Variant Management
  private GridLayout _panelOptionsPanelGridLayout = new GridLayout();
  private GridLayout _boardOptionsPanelGridLayout = new GridLayout();
  private JCheckBox _panelFlipCheckBox = new JCheckBox();
  private JLabel _workingUnitsLabel = new JLabel();
  private JLabel _panelNameLabel = new JLabel();
  private JLabel _rotationLabel = new JLabel();
  private JLabel _panelLengthLabel = new JLabel();
  private JLabel _panelThicknessLabel = new JLabel();
  private JLabel _flippedPanelLabel = new JLabel();
  private JLabel _panelWidthLabel = new JLabel();
  private JLabel _boardNameLabel = new JLabel();
  private JLabel _boardLengthLabel = new JLabel();
  private JLabel _boardWidthLabel = new JLabel();
  private JLabel _alignmentMethodLabel = new JLabel();
  private JComboBox _unitsComboBox = new JComboBox();
  private JComboBox _boardTypesComboBox = new JComboBox();
  private JComboBox _panelRotationComboBox = new JComboBox();
  private JComboBox _alignmentMethodComboBox = new JComboBox();

  private NumericRangePlainDocument _panelWidthDocument = new NumericRangePlainDocument();
  private NumericRangePlainDocument _panelLengthDocument = new NumericRangePlainDocument();
  private NumericRangePlainDocument _panelThicknessDocument = new NumericRangePlainDocument();
  private NumericRangePlainDocument _boardWidthDocument = new NumericRangePlainDocument();
  private NumericRangePlainDocument _boardLengthDocument = new NumericRangePlainDocument();

//  private NumericPlainDocument _panelWidthDocument = new NumericPlainDocument();
//  private NumericPlainDocument _panelLengthDocument = new NumericPlainDocument();
//  private NumericPlainDocument _panelThicknessDocument = new NumericPlainDocument();
//  private NumericPlainDocument _boardWidthDocument = new NumericPlainDocument();
//  private NumericPlainDocument _boardLengthDocument = new NumericPlainDocument();
  private NumericTextField _panelWidthTextField = new NumericTextField();
  private NumericTextField _panelLengthTextField = new NumericTextField();
  private NumericTextField _panelThicknessTextField = new NumericTextField();
  private NumericTextField _boardTypeWidthTextField = new NumericTextField();
  private NumericTextField _boardTypeLengthTextField = new NumericTextField();

  private JButton _addBoardButton = new JButton();
  private JButton _addBoardArrayButton = new JButton();
  private JScrollPane _tablePanelLayoutScroll = new JScrollPane();
  private JScrollPane _noLoadSettingPanelLayoutScroll = new JScrollPane();  //XCR - 2621, Kok Chun, Tan - Variant Management
  private Border _lightGrayLineBorder;
  private Border _etchedLeftBorder;
  private Border _mainBoarder;
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private SelectedRendererObservable _selectedRendererObservable;

  private ImageIcon _imageIcon = com.axi.v810.images.Image5DX.getImageIcon(com.axi.v810.images.Image5DX.AXI_SPARK);
  private JButton _deleteBoardButton = new JButton();
  
  private JCheckBox _useNewThicknessTableCheckBox = new JCheckBox();
  private JLabel _useNewThicknessTableLabel = new JLabel();

  //Siew Yeng - XCR1725 - Customizable Serial Number Mapping
  private JButton _configureSerialNumberMappingButton = new JButton(StringLocalizer.keyToString("MMGUI_SETUP_CONFIGURE_SERIAL_NUMBER_MAPPING_KEY"));

  //Bee Hoon, XCR1744 - Advanced recipe settings
  private JButton _advancedRecipeSettingsButton = new JButton(StringLocalizer.keyToString("MMGUI_SETUP_ADVANCED_RECIPE_SETTINGS_KEY"));
  
  //Kok Chun, Tan - Variant Management
  private JButton _addVariationButton = new JButton(StringLocalizer.keyToString("MMGUI_SETUP_ADD_VARIATION_KEY"));  //XCR - 2621, Kok Chun, Tan - Variant Management
  private JButton _deleteVariationButton = new JButton(StringLocalizer.keyToString("MMGUI_SETUP_DELETE_VARIATION_KEY"));  //XCR - 2621, Kok Chun, Tan - Variant Management
  private JCheckBox _enablePreSelectVariationCheckbox = new JCheckBox();  //XCR - 2621, Kok Chun, Tan - Variant Management
  private transient static ProjectObservable _projectObservable;
  
  //Khaw Chek Hau - XCR2285: 2D Alignment on v810
  private static final Pattern _alignment2DImageFileNamePattern = Pattern.compile(FileName.getAlignment2DImageFileNamePattern());
  
  private static final String _lowMagnificationKey = "0_LOW";
  private static final String _highMagnificationKey = "1_HIGH";
  
  /**
   * @author Kok Chun, Tan
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }
  
  /**
   * Constructor provided for jBuilder's designer only
   * @author Andy Mechtenberg
   */
  private PanelSetupPanel()
  {
    Assert.expect(java.beans.Beans.isDesignTime());
    jbInit();
  }

  /**
   * @author Carli Connally
   */
  public PanelSetupPanel(TestDev testDev)
  {
    super(testDev);
    Assert.expect(testDev != null);

    _testDev = testDev;
    _mainUI = _testDev.getMainUI();
    jbInit();
    init();
  }

  /**
   * Initialization code called by the JBuilder design view.
   * @author Carli Connally
   */
  private void jbInit()
  {
    _textFieldFocusAdapter = new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        JTextField sourceTextField = (JTextField)e.getSource();
        sourceTextField.selectAll();
      }
      public void focusLost(FocusEvent e)
      {
        NumericTextField sourceTextField = (NumericTextField)e.getSource();
        handleTextFieldData(sourceTextField);
      }
    };

    _textFieldActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        NumericTextField sourceTextField = (NumericTextField)e.getSource();
        handleTextFieldData(sourceTextField);
      }
    };

    _panelRotationActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        panelRotationComboBox_actionPerformed();
      }
    };

    _unitsComboBoxActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        unitsComboBox_actionPerformed();
      }
    };
    _panelFlipCheckBoxActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        panelFlipCheckBox_actionPerformed();
      }
    };

    _alignmentMethodActionListener = new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        alignmentMethodComboBox_actionPerformed();
      }
    };
    
    _enablePreSelectVariationCheckbox.setText(StringLocalizer.keyToString("CFGUI_SN_PRESELECT_VARIATION_LABEL_KEY"));
    _enablePreSelectVariationCheckbox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enablePreSelectVariationCheckbox_actionPerformed(e);
      }
    });
    
    initStaticComboChoices();
    _lightGrayLineBorder = BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.lightGray,1),BorderFactory.createEmptyBorder(0,2,0,0));
    _etchedLeftBorder = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new Color(142, 142, 142)),BorderFactory.createEmptyBorder(0,2,0,0));
    _mainBoarder = BorderFactory.createEmptyBorder(5,5,5,5);
    setLayout(_mainBorderLayout);
    _programOptionsPanel.setLayout(_programOptionsPanelFlowLayout);
    _workingUnitsLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_UNITS_KEY"));
    _programOptionsPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _programOptionsPanelFlowLayout.setHgap(10);
    _programOptionsPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED,Color.white,new Color(165, 163, 151)),
                                                    StringLocalizer.keyToString("MMGUI_SETUP_PROGRAM_OPTIONS_KEY")));
    _programOptionsPanel.setOpaque(false);
    _panelSetupPanel.setLayout(_panelSetupPanelBorderLayout);
    _panelOptionsPanel.setLayout(_panelOptionsPanelGridLayout);
    _boardOptionsPanel.setLayout(_boardOptionsPanelGridLayout);
    _panelOptionsPanelGridLayout.setColumns(2);
    _panelOptionsPanelGridLayout.setHgap(-1);
    _panelOptionsPanelGridLayout.setRows(7);
    _panelOptionsPanelGridLayout.setVgap(-1);
    _panelNameLabel.setBorder(_etchedLeftBorder);
    _panelNameLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_PANEL_NAME_KEY"));
    _rotationLabel.setBorder(_etchedLeftBorder);
    _rotationLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_ROTATION_KEY"));
    _boardWidthLabel.setBorder(BorderFactory.createEtchedBorder());
    _flippedPanelLabel.setBorder(_etchedLeftBorder);
    _flippedPanelLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_FLIPPED_KEY"));
    _panelWidthLabel.setBorder(_etchedLeftBorder);
    _panelWidthLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_X_DIMENSION_KEY"));
    _panelAndBoardOptionsPanel.setLayout(_panelAndBoardOptionsPanelBorderLayout);
    _panelWidthTextField.setBorder(_lightGrayLineBorder);
    _panelLengthTextField.setBorder(_lightGrayLineBorder);
    _panelThicknessTextField.setBorder(_lightGrayLineBorder);
    _panelFlipCheckBox.setBackground(Color.white);
    _panelFlipCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
    _boardNameLabel.setBorder(_etchedLeftBorder);
    _boardNameLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_BOARD_TYPE_KEY"));
    _boardLengthLabel.setBorder(_etchedLeftBorder);
    _boardLengthLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_Y_DIMENSION_KEY"));
    _boardWidthLabel.setBorder(_etchedLeftBorder);
    _boardWidthLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_X_DIMENSION_KEY"));
    _boardOptionsPanelGridLayout.setColumns(2);
    _boardOptionsPanelGridLayout.setHgap(-1);
    _boardOptionsPanelGridLayout.setRows(3);
    _boardOptionsPanelGridLayout.setVgap(-1);
    _boardTypeWidthTextField.setBorder(_lightGrayLineBorder);
    _boardTypeLengthTextField.setBorder(_lightGrayLineBorder);
    setBorder(_mainBoarder);
    _panelOptionsPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED,Color.white,new Color(165, 163, 151)),
                                                    StringLocalizer.keyToString("ATGUI_PANEL_KEY")));
    _mainBorderLayout.setVgap(10);
    _boardOptionsPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED,Color.white,new Color(165, 163, 151)),
                                                    StringLocalizer.keyToString("ATGUI_BOARD_TYPE_KEY")));
    _panelLengthLabel.setBorder(_etchedLeftBorder);
    _panelLengthLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_Y_DIMENSION_KEY"));
    _panelThicknessLabel.setBorder(_etchedLeftBorder);
    _panelThicknessLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_PANEL_THICKNESS_KEY"));
    _alignmentMethodLabel.setBorder(_etchedLeftBorder);
    _alignmentMethodLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_ALIGNMENT_METHOD_KEY"));
    _useNewThicknessTableLabel.setBorder(_etchedLeftBorder);
    _useNewThicknessTableLabel.setText(StringLocalizer.keyToString("MMGUI_SETUP_THICKNESS_TABLE_KEY"));
    _useNewThicknessTableCheckBox.setBackground(Color.white);
    _useNewThicknessTableCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
    _addBoardPanelBoxLayout = new BoxLayout(_addBoardPanel, BoxLayout.X_AXIS);
    _addBoardPanel.setLayout(_addBoardPanelBoxLayout);
    _addBoardButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_KEY"));   
    _addBoardArrayButton.setToolTipText("");
    _addBoardArrayButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_ADD_BOARD_ARRAY_KEY"));
    _panelLayoutTablePanel.setLayout(_panelLayoutTablePanelBorderLayout);
    _boardPlacementsPanel.setLayout(_boardPlacementsPanelBorderLayout);
    _tablePanelLayoutScroll.setBorder(null);
    _tablePanelLayoutScroll.setOpaque(false);
    _panelSetupPanelBorderLayout.setVgap(10);
    _panelLayoutTablePanel.setBorder(new TitledBorder(BorderFactory.createEmptyBorder(),
                                     StringLocalizer.keyToString("MMGUI_SETUP_BOARD_PLACEMENT_KEY")));
    _panelLayoutTablePanel.setOpaque(false);
    _alignmentPanel.setLayout(_alignmentPanelBorderLayout);
    _panelRotationComboBox.setBorder(_lightGrayLineBorder);
    _alignmentMethodComboBox.setBorder(_lightGrayLineBorder);
    _boardTypesComboBox.setBorder(_lightGrayLineBorder);
    _panelAndBoardOptionsPanelBorderLayout.setHgap(10);
    _panelAndBoardOptionsPanelBorderLayout.setVgap(10);
    _boardPlacementsPanelBorderLayout.setVgap(2);
    _addBoardPanel.setOpaque(false);
    _boardPlacementsPanel.setOpaque(false);
    _panelSetupPanel.setOpaque(false);
    _panelLayoutTable.setOpaque(false);
    _deleteBoardButton.setText(StringLocalizer.keyToString("MMGUI_SETUP_DELETE_BOARD_KEY"));
    _deleteBoardButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteBoard();
        updateAlignmentMethod();
      }
    });
    
    //XCR - 2621, Kok Chun, Tan - Variant Management
    _noLoadSettingPanel.setLayout(_noLoadSettingPanelBorderLayout);
    _noLoadSettingPanelBorderLayout.setVgap(2);
    _noLoadSettingPanel.setOpaque(false);
    _noLoadSettingLayoutTablePanel.setLayout(_noLoadSettingLayoutTablePanelBorderLayout);
    _noLoadSettingLayoutTablePanel.setBorder(new TitledBorder(BorderFactory.createEmptyBorder(),
                                     StringLocalizer.keyToString("MMGUI_SETUP_NO_LOAD_SETTING_KEY")));
    _noLoadSettingLayoutTablePanel.setOpaque(false);
    _noLoadSettingPanelLayoutScroll.setBorder(null);
    _noLoadSettingPanelLayoutScroll.setOpaque(false);
    _noLoadSettingLayoutTable.setOpaque(false);
    _addVariationPanelBoxLayout = new BoxLayout(_addVariationPanel, BoxLayout.X_AXIS);
    _addVariationPanel.setLayout(_addVariationPanelBoxLayout);
    _addVariationPanel.setOpaque(false);
    
    _allPanel.setLayout(new BoxLayout(_allPanel, BoxLayout.Y_AXIS));
    _allPanel.add(_programOptionsPanel, BorderLayout.NORTH);
    _programOptionsPanel.add(_workingUnitsLabel, null);
    _programOptionsPanel.add(_unitsComboBox, null);
    _allPanel.add(_panelSetupPanel, BorderLayout.CENTER);
    _panelSetupPanel.add(_panelAndBoardOptionsPanel,  BorderLayout.NORTH);
    _panelAndBoardOptionsPanel.add(_panelOptionsPanel, BorderLayout.NORTH);
    _panelOptionsPanel.add(_panelWidthLabel, null);
    _panelOptionsPanel.add(_panelWidthTextField, null);
    _panelOptionsPanel.add(_panelLengthLabel, null);
    _panelOptionsPanel.add(_panelLengthTextField, null);
    _panelOptionsPanel.add(_panelThicknessLabel, null);
    _panelOptionsPanel.add(_panelThicknessTextField, null);
    _panelOptionsPanel.add(_rotationLabel, null);
    _panelAndBoardOptionsPanel.add(_boardOptionsPanel, BorderLayout.CENTER);
    _boardOptionsPanel.add(_boardNameLabel, null);
    _boardOptionsPanel.add(_boardTypesComboBox, null);
    _boardOptionsPanel.add(_boardWidthLabel, null);
    _boardOptionsPanel.add(_boardTypeWidthTextField, null);
    _boardOptionsPanel.add(_boardLengthLabel, null);
    _boardOptionsPanel.add(_boardTypeLengthTextField, null);
    _panelSetupPanel.add(_boardPlacementsPanel,  BorderLayout.CENTER);
    _boardPlacementsPanel.add(_panelLayoutTablePanel,  BorderLayout.NORTH);
    _panelLayoutTablePanel.add(_tablePanelLayoutScroll, BorderLayout.NORTH);
    _tablePanelLayoutScroll.getViewport().add(_panelLayoutTable, null);
    _tablePanelLayoutScroll.getViewport().setOpaque(false);
    _boardPlacementsPanel.add(_addBoardPanel, BorderLayout.CENTER);
    _addBoardPanel.add(_addBoardButton);
    _addBoardPanel.add(_addBoardArrayButton);
    _addBoardPanel.add(_deleteBoardButton);
    _addBoardPanel.add(Box.createHorizontalGlue());
    
    //XCR - 2621, Kok Chun, Tan - Variant Management
    _allPanel.add(_noLoadSettingPanel,  BorderLayout.SOUTH);
    _noLoadSettingPanel.add(_enablePreSelectVariationCheckbox, BorderLayout.NORTH);
    _noLoadSettingPanel.add(_noLoadSettingLayoutTablePanel, BorderLayout.CENTER);
    _noLoadSettingLayoutTablePanel.add(_noLoadSettingPanelLayoutScroll);
    _noLoadSettingPanelLayoutScroll.getViewport().add(_noLoadSettingLayoutTable);
    _noLoadSettingPanelLayoutScroll.getViewport().setOpaque(false);
    _noLoadSettingPanel.add(_addVariationPanel, BorderLayout.SOUTH);
    _addVariationPanel.add(_addVariationButton);
    _addVariationPanel.add(_deleteVariationButton);
    
    //Siew Yeng - XCR1725 - Customizable Serial Number Mapping
    if(SerialNumberMappingManager.getInstance().isSerialNumberMappingEnabled())
      _addBoardPanel.add(_configureSerialNumberMappingButton);

    //bee-hoon.goh, XCR1744 - Advanced recipe settings
    _addBoardPanel.add(_advancedRecipeSettingsButton);
    
    _panelOptionsPanel.add(_panelRotationComboBox, null);
    _panelOptionsPanel.add(_flippedPanelLabel, null);
    // I need to put the check box in a panel because it was extending a pixel above otherwise
    // and erasing the combo box above it.  Annoying GUI glitch.
    JPanel flipPanel = new JPanel();
    flipPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
    flipPanel.add(_panelFlipCheckBox);
    flipPanel.setBackground(Color.white);
    flipPanel.setBorder(_lightGrayLineBorder);
    _panelOptionsPanel.add(flipPanel, null);
// Comment by Wei Chin (will add in after multiboard ready)
    _panelOptionsPanel.add(_alignmentMethodLabel, null);
    _panelOptionsPanel.add(_alignmentMethodComboBox, null);

// use new thickness table by Wei Chin (comment by wei chin, uncomment after the thicknesstable is ready)
//    _panelOptionsPanel.add(_useNewThicknessTableLabel, null);
//    _panelOptionsPanel.add(_useNewThicknessTableCheckBox, null);
//    _panelOptionsPanel.add(_panelFlipCheckBox, null);


//    _panelWidthTextField.setDocument(_loopCountDoc);
//    _panelWidthTextField.setFormat(_loopCountFormat);

    // these should all be positive
    _panelWidthDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _panelLengthDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _panelThicknessDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _boardWidthDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _boardLengthDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));

    _panelWidthTextField.setDocument(_panelWidthDocument);
    _panelLengthTextField.setDocument(_panelLengthDocument);
    _panelThicknessTextField.setDocument(_panelThicknessDocument);
    _boardTypeWidthTextField.setDocument(_boardWidthDocument);
    _boardTypeLengthTextField.setDocument(_boardLengthDocument);

    _panelWidthTextField.addActionListener(_textFieldActionListener);
    _panelLengthTextField.addActionListener(_textFieldActionListener);
    _panelThicknessTextField.addActionListener(_textFieldActionListener);
    _boardTypeWidthTextField.addActionListener(_textFieldActionListener);
    _boardTypeLengthTextField.addActionListener(_textFieldActionListener);
    _panelWidthTextField.addFocusListener(_textFieldFocusAdapter);
    _panelLengthTextField.addFocusListener(_textFieldFocusAdapter);
    _panelThicknessTextField.addFocusListener(_textFieldFocusAdapter);
    _boardTypeWidthTextField.addFocusListener(_textFieldFocusAdapter);
    _boardTypeLengthTextField.addFocusListener(_textFieldFocusAdapter);

    _addBoardArrayButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addBoardArrayButton_actionPerformed(e);
      }
    });
    _addBoardButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addBoardButton_actionPerformed(e);
      }
    });

    ListSelectionListener listSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent event)
      {
        boardSelectionUpdated(event);
      }
    };
    
    //XCR - 2621, Kok Chun, Tan - Variant Management
    _addVariationButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try
        {
          addVariationButton_actionPerformed(e);
        }
        catch (DatastoreException ex)
        {
          //nothing
        }
      }
    });
    
    //XCR - 2621, Kok Chun, Tan - Variant Management
    _deleteVariationButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try
        {
          deleteVariationButton_actionPerformed(e);
        }
        catch (DatastoreException ex)
        {
          //nothing
        }
      }
    });
    
    ListSelectionListener noLoadSettingListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent event)
      {
        updateVariationSelection(event);
      }
    };

    _boardTypesComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_boardTypesComboBox.getSelectedItem() != null)
        {
          updateBoardTypeWidth();
          updateBoardTypeLength();
        }
      }
    });

    _useNewThicknessTableCheckBox.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        useNewThicknessTableStateChange();
      }
    });
    
    //Siew Yeng - XCR1725 - Customizable Serial Number Mapping
    _configureSerialNumberMappingButton.addActionListener(new ActionListener(){

      public void actionPerformed(ActionEvent e)
      {
        try
        {
          SerialNumberMappingManager.getInstance().checkIfSerialNumberMappingAndBarcodeReaderSetupAreTally();
        }
        catch(final XrayTesterException xte)
        {
          SerialNumberMappingException snme = (SerialNumberMappingException)xte;
          if(snme.getExceptionType() == SerialNumberMappingExceptionEnum.BARCODE_CONFIG_FOR_RECIPE_AND_CONFIG_USED_IN_PRODUCTION_NOT_SAME)
          {
            int status = JOptionPane.showConfirmDialog(_mainUI, 
              StringLocalizer.keyToString(new LocalizedString("MMGUI_RESET_BARCODE_CONFIGURATION_WARNING_MESSAGE_KEY", 
                                          new Object[]{SerialNumberMappingManager.getInstance().getBarcodeReaderConfigurationNameUsed(),
                                          BarcodeReaderManager.getInstance().getBarcodeReaderConfigurationNameToUse()})),
              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), 
              JOptionPane.YES_NO_OPTION, 
              JOptionPane.QUESTION_MESSAGE);
        
            if(status == JOptionPane.NO_OPTION)
            {
              return;
            }
            else
            {
              _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PANEL_RESET_SERIAL_NUMBER_MAPPING_KEY"));
              try
              {
                _commandManager.execute(new EditSerialNumberMappingCommand(SerialNumberMappingManager.getInstance().getDefaultSerialNumberMappingDataList()));
                _commandManager.execute(new EditSerialNumberMappingBarcodeConfigNameCommand(BarcodeReaderManager.getInstance().getBarcodeReaderConfigurationNameToUse()));
              }
              catch (XrayTesterException ex)
              {
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                              ex.getLocalizedMessage(),
                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                              true);
              }
              _commandManager.endCommandBlock();
            }
          }
        }
        
        try
        {
          //Siew Yeng - load correct config before continue
          BarcodeReaderManager.getInstance().load(BarcodeReaderManager.getInstance().getBarcodeReaderConfigurationNameToUse());
        }
        catch (DatastoreException ex)
        {
        }
        
        SerialNumberMappingDialog serialNumMapDialog = new SerialNumberMappingDialog(
          _mainUI,
          StringLocalizer.keyToString("MMGUI_SETUP_CONFIGURE_SERIAL_NUMBER_MAPPING_TITLE_KEY"),
          true,
          _project);
        SwingUtils.centerOnComponent(serialNumMapDialog, _mainUI);
        serialNumMapDialog.setModal(true);
        serialNumMapDialog.setVisible(true);
        serialNumMapDialog.clear();
      }
    });

    // Bee Hoon, XCR1744 - Advanced recipe settings
    _advancedRecipeSettingsButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        advancedRecipeSettingsButton_actionPerformed(e);
      }
    });
    
    _panelLayoutTable.getSelectionModel().addListSelectionListener(listSelectionListener);
    _noLoadSettingLayoutTable.getSelectionModel().addListSelectionListener(noLoadSettingListSelectionListener);
//    _panelRotationComboBox.addActionListener(_panelRotationActionListener);
//    _unitsComboBox.addActionListener(_unitsComboBoxActionListener);
//    _panelFlipCheckBox.addActionListener(_panelFlipCheckBoxActionListener);

    _panelFlipCheckBox.setName(".panelFlipCheckBox");
    _unitsComboBox.setName(".unitsComboBox");
    _boardTypesComboBox.setName(".boardTypesComboBox");
    _panelRotationComboBox.setName(".panelRotationComboBox");
    _alignmentMethodComboBox.setName(".alignmentMethodComboBox");
    _useNewThicknessTableCheckBox.setName(".thicknessTableCheckBox");
    _panelWidthTextField.setName(".panelWidthTextField");
    _panelLengthTextField.setName(".panelLengthTextField");
    _panelThicknessTextField.setName(".panelThicknessTextField");
    _boardTypeWidthTextField.setName(".boardTypeWidthTextField");
    _boardTypeLengthTextField.setName(".boardTypeLengthTextField");
    _addBoardButton.setName(".addBoardButton");
    _addBoardArrayButton.setName(".addBoardArrayButton");
    _deleteBoardButton.setName(".deleteBoardButton");
    _panelLayoutTable.setName(".panelLayoutTable");
    _noLoadSettingLayoutTable.setName(".noLoadSettingLayoutTable");
    _addVariationButton.setName(".addVariationButton");
    _deleteVariationButton.setName(".deleteVariationButton");
    
    JScrollPane scrollPane = new JScrollPane(_allPanel);
    scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
    scrollPane.setBounds(50, 30, 300, 50);
    this.add(scrollPane);
  }

  /**
   * Performs ui initialization after JBInit is called.
   * @author Carli Connally
   */
  private void init()
  {
    //set up table row height based on the size of the font
    FontMetrics fm = getFontMetrics(getFont());
    int prefHeight = fm.getHeight() + MainMenuGui.TABLE_ROW_HEIGHT_EXTRA;
    _panelLayoutTableModel = new PanelLayoutTableModel();
    _panelLayoutTable.setModel(_panelLayoutTableModel);
    _panelLayoutTable.setEditorsAndRenderers();
    _panelLayoutTable.setPreferredColumnWidths();
    _noLoadSettingLayoutTableModel = new VariationSettingTableModel();
    _noLoadSettingLayoutTable.setModel(_noLoadSettingLayoutTableModel);
    _noLoadSettingLayoutTable.setEditorsAndRenderers();
    _noLoadSettingLayoutTable.setPreferredColumnWidths();

    // its okay to do set size here.. bill just doesn't like it on the whole panel.
    int headerHeight = prefHeight + 5;
    Dimension prefScrollSize = new Dimension(200, (prefHeight * 5) + headerHeight);  // 10 rows for tables
    _tablePanelLayoutScroll.setPreferredSize(prefScrollSize);
    _noLoadSettingPanelLayoutScroll.setPreferredSize(prefScrollSize);
  }

  /**
   * This method populates the combo boxes that have a static list of choices.
   * This only needs to be called once when the panel is initialized.
   * @author Carli Connally
   */
  private void initStaticComboChoices()
  {
    //add measurement unit options if they have not been previously added
    if(_unitsComboBox.getItemCount() == 0)
    {
      _unitsComboBox.addItem(MeasurementUnits.MEASURE_INCHES);
      _unitsComboBox.addItem(MeasurementUnits.MEASURE_MILLIMETERS);
      _unitsComboBox.addItem(MeasurementUnits.MEASURE_MILS);
    }

    //add possible panel rotation choices if they have not already been added
    if(_panelRotationComboBox.getItemCount() == 0)
    {
      _panelRotationComboBox.addItem(_ROTATION_0);
      _panelRotationComboBox.addItem(_ROTATION_90);
      _panelRotationComboBox.addItem(_ROTATION_180);
      _panelRotationComboBox.addItem(_ROTATION_270);
    }

    if(_alignmentMethodComboBox.getItemCount() == 0)
    {
      _alignmentMethodComboBox.addItem(_PANEL);
      _alignmentMethodComboBox.addItem(_BOARD);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    // nothing to persist, so skip to clear out project data

    // do this first to catch any possible actionListeners being activated
    _project = null;

    addOrRemoveAllActionListeners(false);

    _panelFlipCheckBox.setSelected(false);
    _boardTypesComboBox.removeAllItems();
    _boardTypesComboBox.removeAllItems();
    _boardTypeWidthTextField.setValue(new Double(0.0));
    _boardTypeLengthTextField.setValue(new Double(0.0));
    _panelLayoutTableModel.clearProjectData();
    ProjectObservable.getInstance().deleteObserver(this);
  }

  void addOrRemoveAllActionListeners(boolean addListeners)
  {
    if (addListeners)
    {
      _panelRotationComboBox.addActionListener(_panelRotationActionListener);
      _alignmentMethodComboBox.addActionListener(_alignmentMethodActionListener);
      _unitsComboBox.addActionListener(_unitsComboBoxActionListener);
      _panelFlipCheckBox.addActionListener(_panelFlipCheckBoxActionListener);
      _panelWidthTextField.addActionListener(_textFieldActionListener);
      _panelLengthTextField.addActionListener(_textFieldActionListener);
      _panelThicknessTextField.addActionListener(_textFieldActionListener);
      _boardTypeWidthTextField.addActionListener(_textFieldActionListener);
      _boardTypeLengthTextField.addActionListener(_textFieldActionListener);
      _panelWidthTextField.addFocusListener(_textFieldFocusAdapter);
      _panelLengthTextField.addFocusListener(_textFieldFocusAdapter);
      _panelThicknessTextField.addFocusListener(_textFieldFocusAdapter);
      _boardTypeWidthTextField.addFocusListener(_textFieldFocusAdapter);
      _boardTypeLengthTextField.addFocusListener(_textFieldFocusAdapter);
    }
    else
    {
      _panelRotationComboBox.removeActionListener(_panelRotationActionListener);
      _alignmentMethodComboBox.removeActionListener(_alignmentMethodActionListener);
      _unitsComboBox.removeActionListener(_unitsComboBoxActionListener);
      _panelFlipCheckBox.removeActionListener(_panelFlipCheckBoxActionListener);
      _panelWidthTextField.removeActionListener(_textFieldActionListener);
      _panelLengthTextField.removeActionListener(_textFieldActionListener);
      _panelThicknessTextField.removeActionListener(_textFieldActionListener);
      _boardTypeWidthTextField.removeActionListener(_textFieldActionListener);
      _boardTypeLengthTextField.removeActionListener(_textFieldActionListener);
      _panelWidthTextField.removeFocusListener(_textFieldFocusAdapter);
      _panelLengthTextField.removeFocusListener(_textFieldFocusAdapter);
      _panelThicknessTextField.removeFocusListener(_textFieldFocusAdapter);
      _boardTypeWidthTextField.removeFocusListener(_textFieldFocusAdapter);
      _boardTypeLengthTextField.removeFocusListener(_textFieldFocusAdapter);
    }
  }

  /**
   * Initializes the data in the panel using the project data
   * @author George Booth
   */
  public void populateWithProjectData()
  {
    addOrRemoveAllActionListeners(true);
    //select display units specified by project
    _enumSelectedUnits = _project.getDisplayUnits();
    _unitsComboBox.setSelectedItem(MeasurementUnits.getMeasurementUnitString(_enumSelectedUnits));

    //select rotation specified in the project
    updatePanelRotation();

    updateAlignmentMethod();

    _panelFlipCheckBox.setSelected(_project.getPanel().getPanelSettings().isFlipped());

    //remove the previous list of board types
    _boardTypesComboBox.removeAllItems();

    //populate list of board types
    java.util.List<BoardType> boardTypes = _project.getPanel().getBoardTypes();
    for (BoardType boardType : boardTypes)
    {
      _boardTypesComboBox.addItem(boardType);
    }

    populateSelectedBoardTypeValues();

    //update all measurement values
    updateMeasurementValuesWithSelectedUnits();
    
    //populate no load setting table
    _noLoadSettingLayoutTableModel.setData(_project);
    _noLoadSettingLayoutTableModel.fireTableDataChanged();
    
    //populate panel layout table
    _panelLayoutTableModel.setData(_project);
    _panelLayoutTableModel.setPanelBasedAlignmentMethodSelected(_alignmentMethodComboBox.getSelectedItem().equals(StringLocalizer.keyToString("ATGUI_PANEL_KEY")));
  }

  /**
   * Initializes the data in the panel using the project data
   * @param project project object containing the project settings
   * @author Carli Connally
   */
  public void populateWithProjectData(Project project)
  {
    Assert.expect(project != null);

    _project = project;

    if (_active)
    {
      populateWithProjectData();
      _populateProjectData = false;
    }
    else
    {
      _populateProjectData = true;
    }

    ProjectObservable.getInstance().addObserver(this);
  }

  /**
   * populates the data values that are related to the selected board type
   * (i.e. width and height)
   * @author Carli Connally
   */
  private void populateSelectedBoardTypeValues()
  {
    if(_boardTypesComboBox.getSelectedItem() != null)
    {
      updateBoardTypeWidth();
      updateBoardTypeLength();
    }
    else
    {
      _boardTypeWidthTextField.setValue(new Double(0.0));
      _boardTypeLengthTextField.setValue(new Double(0.0));
    }
  }

  /**
   * Updates all measurement values on the panel with the
   * measurement unit that is currently selected in the combo box.
   * Each measurement unit formats the data with a specific number of decimal points,
   * so the formatting for the values is also changed.
   * @author Carli Connally
   */
  private void updateMeasurementValuesWithSelectedUnits()
  {
    //reformat all of the text fields/renderers/editors
    updateTextFieldDecimalFormats();

    _panelLayoutTable.setDecimalPlacesToDisplay(MeasurementUnits.getMeasurementUnitDecimalPlaces(_enumSelectedUnits));
    _panelLayoutTableModel.fireTableDataChanged();

    populateSelectedBoardTypeValues();

    //convert panel width and height
    updatePanelWidth();
    updatePanelLength();
    updatePanelThickness();

  }

  /**
   * When the measurment units selection changes, the text field formats need
   * to change to display and accept the appropriate number of decimal places.
   * @author Carli Connally
   */
  private void updateTextFieldDecimalFormats()
  {
    StringBuffer decimalFormat = new StringBuffer("########0.");

    int decimalPlaces = MeasurementUnits.getMeasurementUnitDecimalPlaces(_enumSelectedUnits);
    for(int i = 0; i < decimalPlaces; i++)
      decimalFormat.append("0");  // use a "0" if you want to show trailing zeros, use a "#" if you don't

    _panelWidthTextField.setFormat(new DecimalFormat(decimalFormat.toString()));
    _panelLengthTextField.setFormat(new DecimalFormat(decimalFormat.toString()));
    _panelThicknessTextField.setFormat(new DecimalFormat(decimalFormat.toString()));
    _boardTypeWidthTextField.setFormat(new DecimalFormat(decimalFormat.toString()));
    _boardTypeLengthTextField.setFormat(new DecimalFormat(decimalFormat.toString()));
  }

  /**
   * When the user selects a different measurement unit, all related measurement
   * values on the screen must be updated to reflect the newly selected units and
   * the project data selected units modified to reflect the change.
   * @author Carli Connally
   */
  void unitsComboBox_actionPerformed()
  {
    //set the selected units in the project
    String units = _unitsComboBox.getSelectedItem().toString();
    _enumSelectedUnits = MeasurementUnits.getMeasurementUnitEnum(units);
    try
    {
      _commandManager.execute(new ProjectSetDisplayUnitsCommand(_project, _enumSelectedUnits));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
      updateDisplayUnits();
    }
  }

  /**
   * When the user selects a different board type, the associated width and height
   * for that board type should be displayed
   * @author Carli connally
   */
  void boardTypesComboBox_actionPerformed(ActionEvent actionEvent)
  {
    populateSelectedBoardTypeValues();
  }

  /**
   * Show the add board dialog
   * @author Carli Connally
   */
  private void addBoardButton_actionPerformed(ActionEvent actionEvent)
  {
    //show the add board dialog
    AddBoardDialog dlg = new AddBoardDialog(_mainUI, _project);
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    dlg.clear();
    dlg.dispose();

    updateAlignmentMethod(); //Chin-Seong, Kee - Alignment method issue, must update once add board !.
  }

  /**
   * Show the add board array dialog
   * @author Carli Connally
   * @author Kee Chin Seong
   */
  private void addBoardArrayButton_actionPerformed(ActionEvent actionEvent)
  {
    AddBoardArrayDialog dlg = new AddBoardArrayDialog(_project);
    SwingUtils.centerOnComponent(dlg, _mainUI);
    dlg.setVisible(true);
    dlg.unpopulate();
    dlg.dispose();
    
    updateAlignmentMethod(); //Chin-Seong, Kee - Alignment method issue, must update once add board !.
  }

  /**
   * @author Andy Mechtenberg
   * @author Ngie Xing 
   * XCR-2046, Changing panel thickness to zero will not trigger warning message
   */
  private void handleTextFieldData(NumericTextField numericTextField)
  {
    Assert.expect(numericTextField != null);

    if (numericTextField == _panelWidthTextField)
    {
      Double panelWidth;
      try
      {
        panelWidth = _panelWidthTextField.getDoubleValue();
      }
      catch (ParseException ex)
      {
        panelWidth = new Double(0.0);
      }

      if (panelWidth != null)
      {
        int panelWidthInNanoMeters = (int) MathUtil.convertUnits(panelWidth.doubleValue(),
                                                                 _enumSelectedUnits,
                                                                 MathUtilEnum.NANOMETERS);
        int maxPanelWidthInNanoMeters = PanelHandler.getInstance().getMaximumPanelWidthInNanoMeters();
        int minPanelWidthInNanoMeters = PanelHandler.getInstance().getMinimumPanelWidthInNanoMeters();
        
        //Kok Chun, Tan - if enter value is same, then direct return
        if (MathUtil.fuzzyEquals(panelWidthInNanoMeters, _project.getPanel().getWidthAfterAllRotationsInNanoMeters(), MathUtil._UNIT_CONVERSION_NANOMETER_LIMIT))
        {
          updatePanelWidth();
          return;
        }
        
        // if the user hits CTRL-Z to undo something, this might get activated
        // if the focus is lost.  This code will ignore that change if that happens.
        if (_active == false)
        {
          updatePanelWidth();
          return;
        }
        
        //Ngie Xing, XCR-2020, Maximum panel width / length error on 5.5
        // now check that the new panel width is within range
        if (panelWidthInNanoMeters > maxPanelWidthInNanoMeters)
        {
          updatePanelWidth();
          double maxWidthInUnits = MathUtil.convertUnits(maxPanelWidthInNanoMeters, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
          double minWidthInUnits = MathUtil.convertUnits(minPanelWidthInNanoMeters, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
          LocalizedString errorMessage = new LocalizedString("MMGUI_SETUP_PANEL_WIDTH_OUT_OF_RANGE_HIGH_ERROR_KEY",
                                                             new Object[]
                                                             {
                                                               _enumSelectedUnits, minWidthInUnits, maxWidthInUnits
                                                             });
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        StringLocalizer.keyToString(errorMessage),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);

          return;
        }
        else if (panelWidthInNanoMeters < minPanelWidthInNanoMeters)
        {
          updatePanelWidth();
          double maxWidthInUnits = MathUtil.convertUnits(maxPanelWidthInNanoMeters, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
          double minWidthInUnits = MathUtil.convertUnits(minPanelWidthInNanoMeters, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
          LocalizedString errorMessage = new LocalizedString("MMGUI_SETUP_PANEL_WIDTH_OUT_OF_RANGE_LOW_ERROR_KEY",
                                                             new Object[]
                                                             {
                                                               _enumSelectedUnits, minWidthInUnits, maxWidthInUnits
                                                             });
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        StringLocalizer.keyToString(errorMessage),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);

          return;
        }

        boolean enablePsp = Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP);

        _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PANEL_SET_WIDTH_KEY"));
        if (enablePsp)
        {
          updatePanelWidth();

          boolean needToRemoveOpticalRegions = false;

          needToRemoveOpticalRegions = removeOpticalRegionsIfNecessary();

          if (needToRemoveOpticalRegions == false)
          {
            updatePanelWidth();
            return;
          }
        }

        try
        {
          _commandManager.execute(new PanelSetWidthCommand(_project.getPanel(), panelWidthInNanoMeters));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
          updatePanelWidth();
        }
      }
      _commandManager.endCommandBlock();
    }
    else if (numericTextField == _panelLengthTextField)
    {
      Double panelLength;
      try
      {
        panelLength = _panelLengthTextField.getDoubleValue();
      }
      catch (ParseException ex)
      {
        panelLength = new Double(0.0);
      }

      if (panelLength != null)
      {
        int panelLengthInNanoMeters = (int) MathUtil.convertUnits(panelLength.doubleValue(),
                                                                  _enumSelectedUnits,
                                                                  MathUtilEnum.NANOMETERS);
        int maxPanelLengthInNanoMeters = PanelHandler.getInstance().getMaximumPanelLengthInNanoMeters();
        int minPanelLengthInNanoMeters = PanelHandler.getInstance().getMinimumPanelLengthInNanoMeters();

        //Kok Chun, Tan - if enter value is same, then direct return
        if (MathUtil.fuzzyEquals(panelLengthInNanoMeters, _project.getPanel().getLengthAfterAllRotationsInNanoMeters(), MathUtil._UNIT_CONVERSION_NANOMETER_LIMIT))
        {
          updatePanelLength();
          return;
        }
        
        // if the user hits CTRL-Z to undo something, this might get activated
        // if the focus is lost.  This code will ignore that change if that happens.
        if (_active == false)
        {
          updatePanelLength();
          return;
        }

        //Ngie Xing, XCR-2020, Maximum panel width / length error on 5.5
        // now check that the new panel width is within range
        if (panelLengthInNanoMeters > maxPanelLengthInNanoMeters)
        {
          updatePanelLength();
          double maxLengthInUnits = MathUtil.convertUnits(maxPanelLengthInNanoMeters, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
          double minLengthInUnits = MathUtil.convertUnits(minPanelLengthInNanoMeters, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
          LocalizedString errorMessage = new LocalizedString("MMGUI_SETUP_PANEL_LENGTH_OUT_OF_RANGE_HIGH_ERROR_KEY",
                                                             new Object[]
                                                             {
                                                               _enumSelectedUnits, minLengthInUnits, maxLengthInUnits
                                                             });
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        StringLocalizer.keyToString(errorMessage),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);

          return;
        }
        else if (panelLengthInNanoMeters < minPanelLengthInNanoMeters)
        {
          updatePanelLength();
          double maxLengthInUnits = MathUtil.convertUnits(maxPanelLengthInNanoMeters, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
          double minLengthInUnits = MathUtil.convertUnits(minPanelLengthInNanoMeters, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
          LocalizedString errorMessage = new LocalizedString("MMGUI_SETUP_PANEL_LENGTH_OUT_OF_RANGE_LOW_ERROR_KEY",
                                                             new Object[]
                                                             {
                                                               _enumSelectedUnits, minLengthInUnits, maxLengthInUnits
                                                             });
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        StringLocalizer.keyToString(errorMessage),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);

          return;
        }

        boolean enablePsp = Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP);

        _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PANEL_SET_LENGTH_KEY"));
        if (enablePsp)
        {
          updatePanelLength();

          boolean needToRemoveOpticalRegions = false;

          needToRemoveOpticalRegions = removeOpticalRegionsIfNecessary();

          if (needToRemoveOpticalRegions == false)
          {
            updatePanelLength();
            return;
          }
        }

        try
        {
          _commandManager.execute(new PanelSetLengthCommand(_project.getPanel(), panelLengthInNanoMeters));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
          updatePanelLength();
        }
      }
      _commandManager.endCommandBlock();
    }
    else if (numericTextField == _panelThicknessTextField)
    {
      Double panelThickness;
      try
      {
        panelThickness = _panelThicknessTextField.getDoubleValue();
      }
      catch (ParseException ex)
      {
        panelThickness = new Double(0.0);
      }

      if (panelThickness != null)
      {
        int panelThicknessInNanoMeters = (int)MathUtil.convertUnits(panelThickness.doubleValue(),
                                                                    _enumSelectedUnits,
                                                                    MathUtilEnum.NANOMETERS);
        int maxPanelThicknessInNanoMeters = PanelHandler.getInstance().getMaximumPanelThicknessInNanoMeters();
        int minPanelThicknessInNanoMeters = PanelHandler.getInstance().getMinimumPanelThicknessInNanoMeters();

        // if the user hits CTRL-Z to undo something, this might get activated
        // if the focus is lost.  This code will ignore that change if that happens.
        if (_active == false)
        {
          updatePanelThickness();
          return;
        }
        // now check that the new panel thickness is within range
        if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
          maxPanelThicknessInNanoMeters -= XrayTester.getSystemPanelThicknessOffset();
        if (panelThicknessInNanoMeters > maxPanelThicknessInNanoMeters)
        {
          updatePanelThickness();
          double maxThicknessInUnits = MathUtil.convertUnits(maxPanelThicknessInNanoMeters, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
          double minThicknessInUnits = MathUtil.convertUnits(minPanelThicknessInNanoMeters, MathUtilEnum.NANOMETERS, _enumSelectedUnits);

          LocalizedString errorMessage = new LocalizedString("MMGUI_SETUP_PANEL_THICKNESS_OUT_OF_RANGE_HIGH_ERROR_KEY",
              new Object[] {_enumSelectedUnits, minThicknessInUnits, maxThicknessInUnits});
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        StringLocalizer.keyToString(errorMessage) + "\n\n" + StringLocalizer.keyToString("MMGUI_SETUP_PANEL_THICKNESS_IMPORTANT_KEY"),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);

          return;
        }
        else if (panelThicknessInNanoMeters < minPanelThicknessInNanoMeters)
        {
          updatePanelThickness();
          double maxThicknessInUnits = MathUtil.convertUnits(maxPanelThicknessInNanoMeters, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
          double minThicknessInUnits = MathUtil.convertUnits(minPanelThicknessInNanoMeters, MathUtilEnum.NANOMETERS, _enumSelectedUnits);

          LocalizedString errorMessage = new LocalizedString("MMGUI_SETUP_PANEL_THICKNESS_OUT_OF_RANGE_LOW_ERROR_KEY",
              new Object[] {_enumSelectedUnits, minThicknessInUnits, maxThicknessInUnits});
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        StringLocalizer.keyToString(errorMessage) + "\n\n" + StringLocalizer.keyToString("MMGUI_SETUP_PANEL_THICKNESS_IMPORTANT_KEY"),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);

          return;
        }

        try
        {
          _commandManager.execute(new PanelSetThicknessCommand(_project.getPanel(), panelThicknessInNanoMeters));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
          updatePanelThickness();
        }
      }
    }
    else if (numericTextField == _boardTypeWidthTextField)
    {
      Double boardTypeWidth;
      try
      {
        boardTypeWidth = _boardTypeWidthTextField.getDoubleValue();
      }
      catch (ParseException ex)
      {
        boardTypeWidth = new Double(0.0);
      }

      if (boardTypeWidth != null)
      {
        int boardTypeWidthInNanoMeters = (int)MathUtil.convertUnits(boardTypeWidth.doubleValue(),
            _enumSelectedUnits,
            MathUtilEnum.NANOMETERS);
        if (boardTypeWidthInNanoMeters <= 0 || boardTypeWidthInNanoMeters >= Integer.MAX_VALUE)
        {
          updateBoardTypeWidth();
          return;
        }
        // if the user hits CTRL-Z to undo something, this might get activated
        // if the focus is lost.  This code will ignore that change if that happens.
        if (_active == false)
        {
          updateBoardTypeWidth();
          return;
        }

        try
        {
          BoardType selectedBoardType = (BoardType)_boardTypesComboBox.getSelectedItem();
          addOrRemoveAllActionListeners(false);
          _commandManager.execute(new BoardTypeSetWidthCommand(selectedBoardType, boardTypeWidthInNanoMeters));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
        }
        finally
        {
          updateBoardTypeWidth();
          addOrRemoveAllActionListeners(true);
        }
      }
    }
    else if (numericTextField == _boardTypeLengthTextField)
    {
      Double boardTypeLength;
      try
      {
        boardTypeLength = _boardTypeLengthTextField.getDoubleValue();
      }
      catch (ParseException ex)
      {
        boardTypeLength = new Double(0.0);
      }

      if (boardTypeLength != null)
      {
        int boardTypeLengthInNanoMeters = (int)MathUtil.convertUnits(boardTypeLength.doubleValue(),
            _enumSelectedUnits,
            MathUtilEnum.NANOMETERS);
        if (boardTypeLengthInNanoMeters <= 0 || boardTypeLengthInNanoMeters >= Integer.MAX_VALUE)
        {
          updateBoardTypeLength();
          return;
        }

        // if the user hits CTRL-Z to undo something, this might get activated
        // if the focus is lost.  This code will ignore that change if that happens.
        if (_active == false)
        {
          updateBoardTypeLength();
          return;
        }

        try
        {
          BoardType selectedBoardType = (BoardType)_boardTypesComboBox.getSelectedItem();
          addOrRemoveAllActionListeners(false);
          _commandManager.execute(new BoardTypeSetLengthCommand(selectedBoardType, boardTypeLengthInNanoMeters));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
        }
        finally
        {
          updateBoardTypeLength();
          addOrRemoveAllActionListeners(true);
        }
      }
    }
    else
      Assert.expect(false, "Unexpected text field: " + numericTextField);
  }

  /**
   * Flip the panel flag in the project.
   * @author Carli Connally
   */
  void panelFlipCheckBox_actionPerformed()
  {
    // Added by Ying-Huan.Chu - remove all surface map points if panel is flipped.   
    boolean enablePsp = Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP);
    _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PANEL_FLIP_KEY"));
    if (enablePsp)
    {
      boolean needToRemoveOpticalRegions = removeOpticalRegionsIfNecessary();    
      if (needToRemoveOpticalRegions == false) // if user clicked 'No'
      {
        _panelFlipCheckBox.setSelected(_project.getPanel().getPanelSettings().isFlipped());
        return;
      }
    }
    try
    {
      _commandManager.execute(new PanelFlipCommand(_project.getPanel()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
    }
    updatePanelFlipState();
    _commandManager.endCommandBlock();
  }

  /**
   * @author Wei Chin, Chong
   */
  void alignmentMethodComboBox_actionPerformed()
  {
    //get the selected alignment method
    boolean panelBasedAligmentMethod = true;
    String alignmentMethod = _alignmentMethodComboBox.getSelectedItem().toString();

    if(StringLocalizer.keyToString("ATGUI_BOARD_KEY").equals(alignmentMethod) == true)
      panelBasedAligmentMethod = false;

    if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == panelBasedAligmentMethod)
      return;

    // prompt comfirm message to show all alignment points set before will be clean up once you change the alignment method
    int status = JOptionPane.showConfirmDialog(_mainUI, StringLocalizer.keyToString("MMGUI_ALIGNMENT_METHOD_WARNING_MESSAGE_KEY"),
            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    
    if(status == JOptionPane.NO_OPTION)
    {
	  // revert back to original setting;
      if(_project.getPanel().getPanelSettings().isPanelBasedAlignment())
        _alignmentMethodComboBox.setSelectedItem(StringLocalizer.keyToString("ATGUI_PANEL_KEY"));
      else
        _alignmentMethodComboBox.setSelectedItem(StringLocalizer.keyToString("ATGUI_BOARD_KEY"));
      
      return;
    }
    else if(status == JOptionPane.YES_OPTION)
    {
      // Added by Jack Hwee - remove all surface map points if panel is rotated   
      boolean enablePsp = Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP);
      
      if (enablePsp)
      {
        _alignmentMethodComboBox.setSelectedItem(_alignmentMethodComboBox.getSelectedItem().toString());
        boolean needToRemoveOpticalRegions = false;
        _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PANEL_SET_ALIGNMENT_METHOD_KEY"));
        needToRemoveOpticalRegions = removeOpticalRegionsIfNecessary();

        if (needToRemoveOpticalRegions == false)
        {
          // revert back to original setting;
          if(_project.getPanel().getPanelSettings().isPanelBasedAlignment())
            _alignmentMethodComboBox.setSelectedItem(StringLocalizer.keyToString("ATGUI_PANEL_KEY"));
          else
            _alignmentMethodComboBox.setSelectedItem(StringLocalizer.keyToString("ATGUI_BOARD_KEY"));
          
          return;   
        }
      }
      try
      {
        _commandManager.execute(new PanelSetAlignmentMethodCommand(_project.getPanel(), panelBasedAligmentMethod ));
        String imagesDir = Directory.getXrayVerificationImagesDir(_project.getName());
        if(FileUtilAxi.exists(imagesDir))
          FileUtilAxi.deleteDirectoryContents(imagesDir);
        
        //chek hau reivew delete 2D verification
        //Khaw Chek Hau - XCR2285: 2D Alignment on v810
        String images2DDir = Directory.getXray2DVerificationImagesDir(_project.getName());
        if(FileUtilAxi.exists(images2DDir))
          FileUtilAxi.deleteDirectoryContents(images2DDir);
        
        String alignment2DPath = Directory.getAlignment2DImagesDir(_project.getName());
        if (FileUtilAxi.exists(alignment2DPath))
        {
          FileUtilAxi.deleteFileOrDirectory(alignment2DPath);
        }
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
      }
      finally
      {
        updateAlignmentMethod();
      }
      if (enablePsp)
        _commandManager.endCommandBlock();
    }
  }

  /**
   * Rotate panel.
   * @author Carli Connally
   */
  void panelRotationComboBox_actionPerformed()
  {      
    //get the selected rotation
    Integer rotation = ((Integer)_panelRotationComboBox.getSelectedItem()).intValue();
    
    PanelSettings panelSettings = _project.getPanel().getPanelSettings();
    if (rotation == panelSettings.getDegreesRotationRelativeToCad())
      return;
    
     // Added by Jack Hwee - remove all surface map points if panel is rotated   
    boolean enablePsp = Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP);
    
    _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PANEL_SET_ROTATION_KEY"));
    if (enablePsp)
    {
      //_commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PANEL_SET_ROTATION_KEY"));
      
      boolean needToRemoveOpticalRegions = false;
 
      needToRemoveOpticalRegions = removeOpticalRegionsIfNecessary();
    
      if (needToRemoveOpticalRegions == false)
      {
        _panelRotationComboBox.setSelectedItem(_project.getPanel().getPanelSettings().getDegreesRotationRelativeToCad());
        return;   
      }
    }

    //save to the project
    try
    {
      _commandManager.execute(new PanelSetRotationCommand(_project.getPanel(), rotation));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
      updatePanelRotation();
    }
    
    //Siew Yeng - XCR1797 - Fix alignment crash after rotate long panel multiple board with board-based alignment
    copyAlignmentPadsIfNecessary();
//    if (enablePsp)
//      _commandManager.endCommandBlock();
    _commandManager.endCommandBlock();
  }

  /**
   * This class is an observer of the datastore layer. We will determine what
   * kind of changes were made and decide if we need to update the gui.
   * @author George A. David
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        Assert.expect(observable != null);
        Assert.expect(argument != null);

        if (observable instanceof SelectedRendererObservable)
          handleSelectionEvent(argument);
        else if (observable instanceof ProjectObservable)
        {
          if (_active == false)
            _updateData = true;
          else
          {
            // ignore the update if our project object hasn't been initialized
            if (argument instanceof ProjectChangeEvent)
            {
              //              updatePanelData();
              ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)argument;
              ProjectChangeEventEnum projectChangeEventEnum = projectChangeEvent.getProjectChangeEventEnum();
              if (projectChangeEventEnum.equals(ProjectEventEnum.UNLOAD))
                return;
//              if (projectChangeEventEnum instanceof ProjectEventEnum)
//              {
//                updateProjectData((ProjectEventEnum)projectChangeEventEnum);
//              }
//              else if (projectChangeEventEnum instanceof PanelEventEnum)
//              {
//                updatePanelData((PanelEventEnum)projectChangeEventEnum);
//              }
//              else if (projectChangeEventEnum instanceof PanelSettingsEventEnum)
//              {
//                updatePanelSettingsData((PanelSettingsEventEnum)projectChangeEventEnum);
//              }
              if (_project.getPanel().getNumBoards() > 1)
                _guiObservable.stateChanged(null, GraphicsControlEnum.BOARD_NAME_VISIBLE);
              else
                _guiObservable.stateChanged(null, GraphicsControlEnum.BOARD_NAME_INVISIBLE);

              if (projectChangeEventEnum instanceof BoardEventEnum)
              {
                updateBoardData(projectChangeEvent);
              }
              else if (projectChangeEventEnum instanceof BoardTypeEventEnum)
              {
                populateSelectedBoardTypeValues();
//                updateBoardType(projectChangeEvent);
              }
              else
                updatePanelData();
            }
          }
        }
        else
        {
          Assert.expect(false, "No update code for observable: " + observable.getClass().getName());
        }
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleSelectionEvent(final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        // ok.  We got notice that something was selected.

        // We receive from the update, a collection of selected objects.  We'll only pay attention to
        // the ones this screen is interested in
        // In this case, it's just Boards  -- we want to select the row in the _panelLayoutTable
        _panelLayoutTable.clearSelection();
        Collection<com.axi.guiUtil.Renderer> selections = (Collection<com.axi.guiUtil.Renderer>)object;

        for (com.axi.guiUtil.Renderer renderer : selections)
        {
          if (renderer instanceof BoardRenderer)
          {
            // do something
            BoardRenderer boardRenderer = (BoardRenderer)renderer;
            Board board = boardRenderer.getBoard();
            int tableIndex = _panelLayoutTableModel.getIndexForBoard(board);
            _panelLayoutTable.addRowSelectionInterval(tableIndex, tableIndex);
            ListSelectionModel rowSelectionModel = _panelLayoutTable.getSelectionModel();
            rowSelectionModel.setValueIsAdjusting(false);
            SwingUtils.scrollTableToShowSelection(_panelLayoutTable);
          }
        }
      }
    });
  }

  /**
   * The project data was changed via a change to the datastore layer.
   * We will update only the data that we care about.
   *
   * @param event the ProjectEventEnum indicating what changed
   * @author George A. David
   */
  private void updateProjectData(ProjectEventEnum event)
  {
    Assert.expect(event != null);

    if(event.equals(event.PANEL))
    {
      updatePanelData();
    }
    else if(event.equals(event.DISPLAY_UNITS))
    {
      updateDisplayUnits();
    }
  }

  /**
   * The project display units were updated. Update the
   * @author George A. David
   */
  private void updateDisplayUnits()
  {
/** @todo wpd - this gets a nullpointer exception sometimes */
    _enumSelectedUnits = _project.getDisplayUnits();
    updateMeasurementValuesWithSelectedUnits();
    _unitsComboBox.setSelectedItem(MeasurementUnits.getMeasurementUnitString(_enumSelectedUnits));
    _testDev.setUnitsInStatus();
  }

  /**
   * The panel data was changed via a change to the datastore layer.
   * We will update only the data that we care about.
   *
   * @param event the PanelEventEnum indicating what changed
   * @author George A. David
   */
  private void updatePanelData(PanelEventEnum event)
  {
    Assert.expect(event != null);

    if ((event.equals(event.LENGTH) || (event.equals(event.LENGTH_RETAINING_BOARD_COORDS))))
    {
      updatePanelLength();
    }
    else if ((event.equals(event.WIDTH) || (event.equals(event.WIDTH_RETAINING_BOARD_COORDS))))
    {
      updatePanelWidth();
    }
    else if (event.equals(event.THICKNESS))
    {
      updatePanelThickness();
    }
    else if (event.equals(event.PANEL_SETTINGS))
    {
      updatePanelFlipState();
    }
  }

  /**
   * update the panel width value from the datastore
   * @author George A. David
   */
  private void updatePanelWidth()
  {
    double panelWidth = _project.getPanel().getWidthAfterAllRotationsInNanoMeters();
    panelWidth = MathUtil.convertUnits(panelWidth, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
    _panelWidthTextField.setValue(new Double(panelWidth));
  }

  /**
   * Update the panel height value from the datastore
   * @author George A. David
   */
  private void updatePanelLength()
  {
    double panelLength = _project.getPanel().getLengthAfterAllRotationsInNanoMeters();
    panelLength = MathUtil.convertUnits(panelLength, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
    _panelLengthTextField.setValue(new Double(panelLength));
  }

  /**
   * Update the panel height value from the datastore
   * @author George A. David
   */
  private void updatePanelThickness()
  {
    double panelThickness = _project.getPanel().getThicknessInNanometers();
    
    //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on";
    if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
       panelThickness = panelThickness - XrayTester.getSystemPanelThicknessOffset();

    panelThickness = MathUtil.convertUnits(panelThickness, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
    _panelThicknessTextField.setValue(new Double(panelThickness));
  }

  /**
   * update the panel rotation from the datastore
   * @author George A. David
   */
  private void updatePanelRotation()
  {
    //select rotation specified in the project
    int selectedRotation = _project.getPanel().getDegreesRotationRelativeToCad();
    _panelRotationComboBox.setSelectedItem(selectedRotation);
  }

  /**
   * update the alignment method from the datastore
   * @author Wei Chin, Chong
   */
  private void updateAlignmentMethod()
  {
    boolean panelBasedAlignment = _project.getPanel().getPanelSettings().isPanelBasedAlignment();

    if(_project.getPanel().getBoards().size() > 1)
      _alignmentMethodComboBox.setEnabled(true);
    else
      _alignmentMethodComboBox.setEnabled(false);
    
    if(panelBasedAlignment)
    {
      _alignmentMethodComboBox.setSelectedItem(_PANEL);
//      _addBoardButton.setEnabled(true);
//      _addBoardArrayButton.setEnabled(true);
//      _deleteBoardButton.setEnabled(true);
    }
    else
    {
      _alignmentMethodComboBox.setSelectedItem(_BOARD);
//      _addBoardButton.setEnabled(false);
//      _addBoardArrayButton.setEnabled(false);
//      _deleteBoardButton.setEnabled(false);
    }
  }

  /**
   * The panel settings data was changed via a change to the datastore layer.
   * We will update only the data that we care about.
   *
   * @param event the PanelSettingsEventEnum indicating what changed
   * @author George A. David
   */
  private void updatePanelSettingsData(PanelSettingsEventEnum panelSettingsEventEnum)
  {
    Assert.expect(panelSettingsEventEnum != null);

    if (panelSettingsEventEnum.equals(panelSettingsEventEnum.LEFT_TO_RIGHT_FLIP) ||
        panelSettingsEventEnum.equals(panelSettingsEventEnum.TOP_TO_BOTTOM_FLIP))
    {
      updatePanelFlipState();
    }
    else if(panelSettingsEventEnum.equals(panelSettingsEventEnum.DEGREES_ROTATION))
    {
      updatePanelRotation();
    }
  }

  /**
   * Update the boards in the panel via  the datastore
   * @author George A. David
   */
  private void updateBoards()
  {
    //populate panel layout table
    if(_panelLayoutTable.isEditing())
    {
      _panelLayoutTable.getCellEditor().cancelCellEditing();
    }
    _panelLayoutTableModel.setData(_project);
    _panelLayoutTableModel.setPanelBasedAlignmentMethodSelected(_alignmentMethodComboBox.getSelectedItem().equals(StringLocalizer.keyToString("ATGUI_PANEL_KEY")));
  }

  /**
   * Update the panel flip status via the datastore
   * @author George A. David
   */
  private void updatePanelFlipState()
  {
    _panelFlipCheckBox.setSelected(_project.getPanel().getPanelSettings().isFlipped());
    updateBoards();
  }

  /**
   * Update all of the panel data via the datastore
   * @author George A. David
   */
  private void updatePanelData()
  {
    updateDisplayUnits();
    updatePanelWidth();
    updatePanelLength();
    updatePanelThickness();
    updatePanelRotation();
    updatePanelFlipState();
    updateNewThicknessTableState();
    updateBoards();
    updateAlignmentMethod();
    updatePreSelectVariationData();
    updateVariationSettingData();
  }

  /**
   * Update the list of board types from the datastore
   * @author George A. David
   */
  private void updateBoardTypes()
  {
    //remove the previous list of board types
    _boardTypesComboBox.removeAllItems();

    //populate list of board types
    java.util.List<BoardType> boardTypes = _project.getPanel().getBoardTypes();
    for (BoardType boardType : boardTypes)
    {
      _boardTypesComboBox.addItem(boardType);
    }
    populateSelectedBoardTypeValues();
 }

  /**
   * The board data was changed via a change to the datastore layer.
   * We will update only the data that we care about.
   *
   * @author George A. David
   */
  private void updateBoardData(ProjectChangeEvent projectChangeEvent)
  {
    Assert.expect(projectChangeEvent != null);
    ProjectChangeEventEnum eventEnum = projectChangeEvent.getProjectChangeEventEnum();
    Assert.expect(eventEnum instanceof BoardEventEnum);
    BoardEventEnum boardEventEnum = (BoardEventEnum)eventEnum;
    boolean createOrDestroy = false;
    if (boardEventEnum.equals(BoardEventEnum.CREATE_OR_DESTROY) || boardEventEnum.equals(BoardEventEnum.ADD_OR_REMOVE))
      createOrDestroy = true;

    if(_panelLayoutTable.isEditing())
    {
      _panelLayoutTable.getCellEditor().cancelCellEditing();
    }
    if (createOrDestroy || boardEventEnum.equals(BoardEventEnum.NAME))
    {
      _panelLayoutTableModel.setData(_project);
      _panelLayoutTableModel.setPanelBasedAlignmentMethodSelected(_alignmentMethodComboBox.getSelectedItem().equals(StringLocalizer.keyToString("ATGUI_PANEL_KEY")));
      _panelLayoutTableModel.fireTableDataChanged();
    }

    _panelLayoutTable.clearSelection();
    Board board = (Board)projectChangeEvent.getSource();

    int index = _panelLayoutTableModel.getIndexForBoard(board);
    if (index >= 0)
    {
      _panelLayoutTableModel.fireTableRowsUpdated(index, index);
      _panelLayoutTable.addRowSelectionInterval(index, index);
    }
  }

  /**
   * The board type data has changed. We need to update it.
   * @author George A. David
   */
  private void updateBoardType(ProjectChangeEvent datastoreEvent)
  {
    Assert.expect(datastoreEvent != null);

    BoardType selectedBoardType = (BoardType)_boardTypesComboBox.getSelectedItem();
    if(selectedBoardType == datastoreEvent.getSource())
    {
      Assert.expect(datastoreEvent.getProjectChangeEventEnum() instanceof BoardTypeEventEnum);
      BoardTypeEventEnum eventEnum = (BoardTypeEventEnum)datastoreEvent.getProjectChangeEventEnum();
      if(eventEnum.equals(eventEnum.LENGTH))
      {
        updateBoardTypeLength();
      }
      else if(eventEnum.equals(eventEnum.WIDTH))
      {
        updateBoardTypeWidth();
      }
    }
  }

  /**
   * @author George A. David
   */
  private void updateBoardTypeLength()
  {
    BoardType selectedBoardType = (BoardType)_boardTypesComboBox.getSelectedItem();
    double boardTypeLength = selectedBoardType.getLengthInNanoMeters();
    boardTypeLength = MathUtil.convertUnits(boardTypeLength, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
    _boardTypeLengthTextField.setValue(new Double(boardTypeLength));
  }

  /**
   * @author George A. David
   */
  private void updateBoardTypeWidth()
  {
    BoardType selectedBoardType = (BoardType)_boardTypesComboBox.getSelectedItem();
    double boardTypeWidth = selectedBoardType.getWidthInNanoMeters();
    boardTypeWidth = MathUtil.convertUnits(boardTypeWidth, MathUtilEnum.NANOMETERS, _enumSelectedUnits);
    _boardTypeWidthTextField.setValue(new Double(boardTypeWidth));
  }

  /**
   * @author George A. David
   */
  private void boardSelectionUpdated(ListSelectionEvent event)
  {
    Assert.expect(event != null);

    _panelLayoutTableModel.setSelectedBoards(_panelLayoutTable.getSelectedBoards());
    _guiObservable.stateChanged(_panelLayoutTable.getSelectedBoards(), SelectionEventEnum.BOARD_INSTANCE);

    if (_panelLayoutTable.getSelectedBoards().isEmpty())
      _deleteBoardButton.setEnabled(false);
    else
      _deleteBoardButton.setEnabled(true);
  }
  
  /**
   * //XCR - 2621, Variant Management
   * @author Kok Chun, Tan
   */
  private void updateVariationSelection(ListSelectionEvent event)
  {
    Assert.expect(event != null);
    
    _noLoadSettingLayoutTableModel.setSelectedVariations(_noLoadSettingLayoutTable.getSelectedVariations());
    
    if (_noLoadSettingLayoutTable.getSelectedVariations().isEmpty())
      _deleteVariationButton.setEnabled(false);
    else
      _deleteVariationButton.setEnabled(true);
  }

  /**
   * @author George A. David
   */
  public void paintComponent(Graphics graphics)
  {
    super.paintComponent(graphics);

    int panelWidth = getWidth();
    int panelHeight = getHeight();
    int iconWidth = _imageIcon.getIconWidth();
    int iconHeight = _imageIcon.getIconHeight();
    int xCoordinate = panelWidth - iconWidth;
    int yCoordinate = panelHeight - iconHeight;

    _imageIcon.paintIcon(this, graphics, xCoordinate, yCoordinate);
  }

  /**
   * @author George A. David
   */
  private void deleteBoard()
  {
    java.util.List<Board> boardsToDelete = _panelLayoutTable.getSelectedBoards();
    if (boardsToDelete.isEmpty())
      return;
    
    int numberOfBoardsAfterDelete = _project.getPanel().getBoards().size() - boardsToDelete.size();
    if(numberOfBoardsAfterDelete <= 1)
    {
      if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
      {
        // prompt comfirm message to show all alignment points set before will be clean up once you change the alignment method
        int status = JOptionPane.showConfirmDialog(_mainUI, StringLocalizer.keyToString("MMGUI_DELETE_LEFT_ONE_BOARD_WARNING_MESSAGE_KEY"),
                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    
        if(status == JOptionPane.NO_OPTION)
        {
          return;
        }
        else if(status == JOptionPane.YES_OPTION)
        {
          // Added by Jack Hwee - remove all surface map points if panel is rotated   
          boolean enablePsp = Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP);

          if (enablePsp)
          {
            _alignmentMethodComboBox.setSelectedItem(_alignmentMethodComboBox.getSelectedItem().toString());
            boolean needToRemoveOpticalRegions = false;
            _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_PANEL_SET_ALIGNMENT_METHOD_KEY"));
            needToRemoveOpticalRegions = removeOpticalRegionsIfNecessary();

            if (needToRemoveOpticalRegions == false)
            {
              return;   
            }
          }
          try
          {
            _commandManager.execute(new PanelSetAlignmentMethodCommand(_project.getPanel(), true ));
            String imagesDir = Directory.getXrayVerificationImagesDir(_project.getName());
            if(FileUtilAxi.exists(imagesDir))
              FileUtilAxi.deleteDirectoryContents(imagesDir);

            //Khaw Chek Hau - XCR2285: 2D Alignment on v810
            String images2DDir = Directory.getXray2DVerificationImagesDir(_project.getName());
            if(FileUtilAxi.exists(images2DDir))
              FileUtilAxi.deleteDirectoryContents(images2DDir);
            
            String alignment2DPath = Directory.getAlignment2DImagesDir(_project.getName());
            if (FileUtilAxi.exists(alignment2DPath))
            {
              FileUtilAxi.deleteFileOrDirectory(alignment2DPath);
            }
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                          true);
          }
          finally
          {
            updateAlignmentMethod();
          }
          if (enablePsp)
            _commandManager.endCommandBlock();
        }
      }
    }
    
    // we don't allow the deletion of any board that currently is used for alignment -- give an error if this is the case
    Set<Board> boardsUnableToDelete = new HashSet<Board>();
    for(Board board : boardsToDelete)
    {
      for (AlignmentGroup alignmentGroup : board.getPanel().getPanelSettings().getAllAlignmentGroups())
      {
        java.util.List<Pad> pads = new ArrayList<Pad>(alignmentGroup.getPads());
        if (pads.isEmpty() == false)
        {
          for (Pad pad : pads)
          {
            if (pad.getComponent().getBoard() == board)
            {
              boardsUnableToDelete.add(board);
              break;
            }
          }
        }
      }
    }
    if (boardsUnableToDelete.isEmpty() == false)
    {
      java.util.List<Board> badBoards = new ArrayList<Board>(boardsUnableToDelete);
      Collections.sort(badBoards, new AlphaNumericComparator());
      String boardNames = "";
      for(Board board : badBoards)
      {
        boardNames = boardNames + board.getName() + ", ";
      }
      // remove the last comma
      boardNames = boardNames.substring(0, boardNames.lastIndexOf(","));
      LocalizedString message = new LocalizedString("MMGUI_SETUP_BOARD_USED_FOR_ALIGNMENT_KEY", new Object[]{boardNames});
      MessageDialog.showErrorDialog(_mainUI,
                                    StringLocalizer.keyToString(message),
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
      return;
    }
    
    //Siew Yeng - prompt to clear serial number mapping if barcode reader is enabled
    if(BarcodeReaderManager.getInstance().getBarcodeReaderAutomaticReaderEnabled() &&
      SerialNumberMappingManager.getInstance().isSerialNumberMappingFileExists())
    {
      int status = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(), 
                                                 StringLocalizer.keyToString("MMGUI_CLEAR_SERIAL_NUMBER_MAPPING_WARNING_MESSAGE_KEY"),
                                                 StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), 
                                                 JOptionPane.YES_NO_OPTION, 
                                                 JOptionPane.QUESTION_MESSAGE);  
      if(status == JOptionPane.NO_OPTION)
      {
        return;
      }
    }

    _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_DELETE_BOARD_KEY"));
    try
    {
      for(Board boardToDelete : boardsToDelete)
      {
        _commandManager.execute(new RemoveBoardCommand(boardToDelete));
        
        //Khaw Chek Hau - XCR2285: 2D Alignment on v810
        //Have to delete the 2D alignment learned image if the board is deleted
        String pathToScan = Directory.getAlignment2DImagesDir(_project.getName());
        
        if (FileUtilAxi.exists(pathToScan))
        {          
          String fileInDirectory = null;
          java.io.File folderToScan = new java.io.File(pathToScan); 
          java.io.File[] listOfFiles = folderToScan.listFiles();

          for (java.io.File file : listOfFiles) 
          {
            if (file.isFile()) 
            {
              fileInDirectory = file.getName();
              Matcher matcher = _alignment2DImageFileNamePattern.matcher(fileInDirectory);
              matcher.reset(fileInDirectory); 

              if (matcher.find()) 
              {
                for (AlignmentGroup alignmentGroup : boardToDelete.getBoardSettings().getAllAlignmentGroups())
                {
                  if (fileInDirectory.contains("_G" + alignmentGroup.getName()))
                  {
                    FileUtilAxi.deleteFileOrDirectory(pathToScan + File.separator + fileInDirectory);
                  }
                }
              }
            }
          } 
        }             
      }
      
      if (FileUtilAxi.exists(Directory.getAlignment2DImagesDir(_project.getName())))
      {
        PanelSettings panelSettings = _project.getPanel().getPanelSettings();

        if(panelSettings.isPanelBasedAlignment())
        {
          panelSettings.clearAllManualAlignmentTransform();
        }
        else
        {
          for(Board board : panelSettings.getPanel().getBoards())
          {
            board.getBoardSettings().clearAllManualAlignmentTransform();
          }
        }
      }
      
      if(BarcodeReaderManager.getInstance().getBarcodeReaderAutomaticReaderEnabled() &&
      SerialNumberMappingManager.getInstance().isSerialNumberMappingFileExists())
      {
        _commandManager.execute(new EditSerialNumberMappingCommand(SerialNumberMappingManager.getInstance().getDefaultSerialNumberMappingDataList()));
        _commandManager.execute(new EditSerialNumberMappingBarcodeConfigNameCommand(BarcodeReaderManager.getInstance().getBarcodeReaderConfigurationNameToUse()));
      }
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI,
                                    ex,
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
    }
    _commandManager.endCommandBlock();
  }

  /**
   * Task is ready to start
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("PanelSetupPanel().start()");

    com.axi.util.TimerUtil screenTimer = new com.axi.util.TimerUtil();
    screenTimer.start();

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();

    _active = true;

    if (_populateProjectData)
    {
      populateWithProjectData();
    }
    _populateProjectData = false;

    // add custom menus

    // add custom toolbar components
    _testDev.switchToPanelGraphics();
    _testDev.showCadWindow();
    if (_selectedRendererObservable == null)
      _selectedRendererObservable = _testDev.getPanelGraphicsEngineSetup().getGraphicsEngine().getSelectedRendererObservable();
    _selectedRendererObservable.addObserver(this);
    if (_project.getPanel().getNumBoards() > 1)
      _guiObservable.stateChanged(null, GraphicsControlEnum.BOARD_NAME_VISIBLE);

    if (_updateData)
    {
      updateDisplayUnits();
      updatePanelData();
      updateBoardTypes();
    }
    _updateData = false;

    _guiObservable.stateChanged(_panelLayoutTable.getSelectedBoards(), SelectionEventEnum.BOARD_INSTANCE);
    if (_panelLayoutTable.getSelectedBoards().isEmpty())
      _deleteBoardButton.setEnabled(false);
    else
      _deleteBoardButton.setEnabled(true);
    
    //XCR - 2621, Kok Chun, Tan - Variant Management
    if (_noLoadSettingLayoutTable.getSelectedVariations().isEmpty())
      _deleteVariationButton.setEnabled(false);
    else
      _deleteVariationButton.setEnabled(true);
    
    //Kok Chun, Tan - update pre select variation flag.
    updatePreSelectVariationData();

    // no need to repopulate because there is no way any of this data can change outside of this screen
    
    //Siew Yeng - XCR1725 - Customizable Serial Number Mapping
    if(BarcodeReaderManager.getInstance().getBarcodeReaderAutomaticReaderEnabled())
      _configureSerialNumberMappingButton.setEnabled(true);
    else
      _configureSerialNumberMappingButton.setEnabled(false);
    
    screenTimer.stop();
//    System.out.println("  Total Loading for Panel Setup: (in mils): "+ screenTimer.getElapsedTimeInMillis());
  }

  /**
   * Can the environment be swapped out
   * @return true if environment can be swapped
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    //    System.out.println("PanelSetupPanel().isReadyToFinish()");

    if (_panelLayoutTable.isEditing())
      _panelLayoutTable.getCellEditor().stopCellEditing();

    if (_testDev.areClosingProject())
      return true;
    if (_mainUI.isPanelInLegalState() == false)
      return false;
    else
      return true;
  }

  /**
   * The environment is about to be swapped out and should perform cleanup
   * @author George Booth
   */
  public void finish()
  {
//    System.out.println("PanelSetupPanel().finish()");
    _selectedRendererObservable.deleteObserver(this);
    _guiObservable.stateChanged(null, GraphicsControlEnum.BOARD_NAME_INVISIBLE);
    _guiObservable.stateChanged(null, SelectionEventEnum.CLEAR_SELECTIONS);
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
    _active = false;
  }
  
  /**
   * Remove optical regions if panel is rotated/flipped or when alignment method is changed.
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  private boolean removeOpticalRegionsIfNecessary()
  {
    PanelSettings panelSettings = _project.getPanel().getPanelSettings();
    boolean needToRemoveOpticalRegions = false;
    
    if (panelSettings.isPanelBasedAlignment())
    {
      // Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
      // This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
      if ((_project.getPanel().hasPanelSurfaceMapSettings() /*|| _project.getPanel().hasPanelAlignmentSurfaceMapSettings()*/) &&
          (_project.getPanel().getPanelSurfaceMapSettings().getOpticalRegions().isEmpty() == false /*|| 
           _project.getPanel().getPanelAlignmentSurfaceMapSettings().getOpticalRegions().isEmpty() == false*/))
      {
        // prompt comfirm message to show all PSP points set before will be clean up once you change the rotation
        int status = JOptionPane.showConfirmDialog(_mainUI, StringLocalizer.keyToString("MMGUI_REMOVE_PSP_POINTS_WARNING_MESSAGE_KEY"),
                     StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (status == JOptionPane.NO_OPTION || status == JOptionPane.CLOSED_OPTION)
        {
          _commandManager.endCommandBlock();
          return false;
        }
        else if(status == JOptionPane.YES_OPTION)
        {
          if (_project.getPanel().hasPanelSurfaceMapSettings())
          {
            PanelSurfaceMapSettings panelSurfaceMapSettings = _project.getPanel().getPanelSurfaceMapSettings();
            needToRemoveOpticalRegions = true;
            for (OpticalRegion panelOpticalRegion : panelSurfaceMapSettings.getOpticalRegions())
            {
              try
              {
                _commandManager.execute(new SurfaceMapPanelDeleteOpticalRegionCommand(_project, panelSurfaceMapSettings, panelOpticalRegion));
              }
              catch (XrayTesterException ex)
              {
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                              ex.getLocalizedMessage(),
                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                              true);
              }
            }
          }
          // Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
          // This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//          if (_project.getPanel().hasPanelAlignmentSurfaceMapSettings())
//          {
//            PanelAlignmentSurfaceMapSettings panelAlignmentSurfaceMapSettings = _project.getPanel().getPanelAlignmentSurfaceMapSettings();
//            needToRemoveOpticalRegions = true;
//            for (OpticalRegion panelAlignmentOpticalRegion : panelAlignmentSurfaceMapSettings.getOpticalRegions())
//            {
//              try
//              {
//                _commandManager.execute(new SurfaceMapPanelDeleteAlignmentOpticalRegionCommand(_project, 
//                                                                                               panelAlignmentSurfaceMapSettings,
//                                                                                               panelAlignmentOpticalRegion.getAlignmentGroupName(),
//                                                                                               panelAlignmentOpticalRegion));
//              }
//              catch (XrayTesterException ex)
//              {
//                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
//                                              ex.getLocalizedMessage(),
//                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
//                                              true);
//              }
//            }
//          }
        }
      }
      else
      {
        needToRemoveOpticalRegions = true;
      }
    }
    else
    {
      java.util.List<Board> boards = _project.getPanel().getBoards();
      int status = -1;
      for(Board board : boards)
      {
        // Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
        // This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
        if ((board.hasBoardSurfaceMapSettings() /*|| board.hasBoardAlignmentSurfaceMapSettings()*/) &&
            (board.getBoardSurfaceMapSettings().getOpticalRegions().isEmpty() == false /*|| 
             board.getBoardAlignmentSurfaceMapSettings().getOpticalRegions().isEmpty() == false*/))
        {
          // prompt comfirm message to show all PSP points set before will be clean up once you change the rotation for ONE TIME ONLY
          if (status == -1)
            status = JOptionPane.showConfirmDialog(_mainUI, StringLocalizer.keyToString("MMGUI_REMOVE_PSP_POINTS_WARNING_MESSAGE_KEY"),
                     StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);  

          if (status == JOptionPane.NO_OPTION || status == JOptionPane.CLOSED_OPTION)
          {
            _panelFlipCheckBox.setSelected(_project.getPanel().getPanelSettings().isFlipped());
            _commandManager.endCommandBlock();
            if (boards != null)
              boards.clear();
            return false;
          }
          else if (status == JOptionPane.YES_OPTION)
          {
            if (board.hasBoardSurfaceMapSettings())
            {
              BoardSurfaceMapSettings boardSurfaceMapSettings = board.getBoardSurfaceMapSettings();
              needToRemoveOpticalRegions = true;
              for (OpticalRegion boardOpticalRegion : boardSurfaceMapSettings.getOpticalRegions())
              {
                try
                {
                  _commandManager.execute(new SurfaceMapBoardDeleteOpticalRegionCommand(_project, boardSurfaceMapSettings, boardOpticalRegion));
                }
                catch (XrayTesterException ex)
                {
                  MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                                ex.getLocalizedMessage(),
                                                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                true);
                }
              }
            }
            // Commented by Ying Huan, 2nd Sept 2014 for 5.6 release.
            // This is because PSP alignment is still un-stable. Will be fixed in PSP Phase 3.
//            if (board.hasBoardAlignmentSurfaceMapSettings())
//            {
//              BoardAlignmentSurfaceMapSettings boardAlignmentSurfaceMapSettings = board.getBoardAlignmentSurfaceMapSettings();
//              needToRemoveOpticalRegions = true;
//              for (OpticalRegion boardAlignmentOpticalRegion : board.getBoardAlignmentSurfaceMapSettings().getOpticalRegions())
//              {
//                try
//                {
//                  _commandManager.execute(new SurfaceMapBoardDeleteAlignmentOpticalRegionCommand(_project, 
//                                                                                                 boardAlignmentSurfaceMapSettings,
//                                                                                                 boardAlignmentOpticalRegion.getAlignmentGroupName(),
//                                                                                                 boardAlignmentOpticalRegion));
//                }
//                catch (XrayTesterException ex)
//                {
//                  MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
//                                                ex.getLocalizedMessage(),
//                                                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
//                                                true);
//                }
//              }
//            }
          }
          else
          { 
            if (status == -1)
            {
              needToRemoveOpticalRegions = true;
            }
          }
        }
      }
      if (boards != null)
        boards.clear();
      needToRemoveOpticalRegions = true;
    }
    return needToRemoveOpticalRegions;
  }
  
  /**
   * @author Wei Chin
   */
  private void useNewThicknessTableStateChange()
  {
    try
    {
      _commandManager.execute(new SetUseNewThicknessTableCommand(_project.getPanel().getPanelSettings(), _useNewThicknessTableCheckBox.isSelected()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
    }
    finally
    {
      updateNewThicknessTableState();
    }
  }
  
  /**
   * update the NewThicknessTableState from datastore
   * @author Wei Chin
   */
  private void updateNewThicknessTableState()
  {
    boolean useNewThicknessTable =_project.getPanel().getPanelSettings().isUseNewThicknessTable();
    _useNewThicknessTableCheckBox.setSelected(useNewThicknessTable);
  }
  
  /**
   * @param e 
   * @author bee-hoon.goh
   * @author Swee Yee Wong - reorganize advance settings interface
   */
   void advancedRecipeSettingsButton_actionPerformed(ActionEvent e)
   {
     java.util.List<AdvanceSettingsDialogPanel> advanceSettingsDialogPanelList = new ArrayList<AdvanceSettingsDialogPanel>();
     java.util.List<ProjectImageViewSettingEnum> projectImageViewSettingEnumList = ProjectImageViewSettingEnum.getAllProjectImageViewSettingList();
     java.util.List<ProjectReconstructionSettingEnum> projectReconstructionSettingEnumList = ProjectReconstructionSettingEnum.getAllProjectReconstructionSettingList();
     java.util.List<ProjectAlgorithmSettingEnum> projectAlgorithmSettingEnumList = ProjectAlgorithmSettingEnum.getAllProjectAlgorithmSettingList();
     java.util.List<ProjectProductionSettingEnum> projectProductionSettingEnumList = ProjectProductionSettingEnum.getAllProjectProductionSettingList();
     java.util.List<ProjectAlignmentSettingEnum> projectAlignmentSettingEnumList = ProjectAlignmentSettingEnum.getAllProjectAlignmentSettingList();
     
     Map<String, CheckBoxItem> _settingNameToCheckBoxItemMap = new HashMap<String, CheckBoxItem>();
     Map<String, ComboBoxItem> _settingNameToComboBoxItemMap = new HashMap<String, ComboBoxItem>();
     
     
     //adding image view settings dialog
     AdvanceSettingsDialogPanel imageViewSettingsDialogPanel = null;
     java.util.List<ComboBoxItem> imageViewComboBoxItemList = new ArrayList<ComboBoxItem>();
     java.util.List<CheckBoxItem> imageViewCheckBoxItemList = new ArrayList<CheckBoxItem>();
     for(ProjectImageViewSettingEnum imageViewSettingEnum : projectImageViewSettingEnumList)
     {
       if(imageViewSettingEnum.getSelectionList() == null)
       {
         CheckBoxItem checkBoxItem = new CheckBoxItem(imageViewSettingEnum.getName(), _project.getProjectImageViewSetting(imageViewSettingEnum), null);
         imageViewCheckBoxItemList.add(checkBoxItem);
         _settingNameToCheckBoxItemMap.put(imageViewSettingEnum.getName(), checkBoxItem);
       }
       else
       {
         ComboBoxItem comboBoxItem = new ComboBoxItem(imageViewSettingEnum.getName(), _project.getProjectImageViewSelectionListSetting(imageViewSettingEnum), imageViewSettingEnum.getSelectionList(), null);
         imageViewComboBoxItemList.add(comboBoxItem);
         _settingNameToComboBoxItemMap.put(imageViewSettingEnum.getName(), comboBoxItem);
       }
     }
     imageViewSettingsDialogPanel = new AdvanceSettingsDialogPanel(StringLocalizer.keyToString("MMGUI_SETUP_PROJECT_IMAGE_VIEW_SETTING_OPTIONS_KEY"), imageViewComboBoxItemList, imageViewCheckBoxItemList);
     advanceSettingsDialogPanelList.add(imageViewSettingsDialogPanel);
     
     //adding reconstruction settings dialog
     AdvanceSettingsDialogPanel reconstructionSettingsDialogPanel = null;
     java.util.List<ComboBoxItem> reconstructionComboBoxItemList = new ArrayList<ComboBoxItem>();
     java.util.List<CheckBoxItem> reconstructionCheckBoxItemList = new ArrayList<CheckBoxItem>();
     for(ProjectReconstructionSettingEnum reconstructionSettingEnum : projectReconstructionSettingEnumList)
     {
       if(reconstructionSettingEnum.getSelectionList() == null)
       {
         CheckBoxItem checkBoxItem = new CheckBoxItem(reconstructionSettingEnum.getName(), _project.getProjectReconstructionSetting(reconstructionSettingEnum), null);
         reconstructionCheckBoxItemList.add(checkBoxItem);
         _settingNameToCheckBoxItemMap.put(reconstructionSettingEnum.getName(), checkBoxItem);
       }
       else
       {
         ComboBoxItem comboBoxItem = new ComboBoxItem(reconstructionSettingEnum.getName(), _project.getProjectReconstructionSelectionListSetting(reconstructionSettingEnum), reconstructionSettingEnum.getSelectionList(), null);
         reconstructionComboBoxItemList.add(comboBoxItem);
         _settingNameToComboBoxItemMap.put(reconstructionSettingEnum.getName(), comboBoxItem);
       }
     }
     reconstructionSettingsDialogPanel = new AdvanceSettingsDialogPanel(StringLocalizer.keyToString("MMGUI_SETUP_PROJECT_RECONSTRUCTION_SETTING_OPTIONS_KEY"), reconstructionComboBoxItemList, reconstructionCheckBoxItemList);
     advanceSettingsDialogPanelList.add(reconstructionSettingsDialogPanel);
     
     //adding algorithm settings dialog
     AdvanceSettingsDialogPanel algorithmSettingsDialogPanel = null;
     java.util.List<ComboBoxItem> algorithmComboBoxItemList = new ArrayList<ComboBoxItem>();
     java.util.List<CheckBoxItem> algorithmCheckBoxItemList = new ArrayList<CheckBoxItem>();
     for(ProjectAlgorithmSettingEnum algorithmSettingEnum : projectAlgorithmSettingEnumList)
     {
       if(algorithmSettingEnum.getSelectionList() == null)
       {
         CheckBoxItem checkBoxItem = new CheckBoxItem(algorithmSettingEnum.getName(), _project.getProjectAlgorithmSetting(algorithmSettingEnum), null);
         algorithmCheckBoxItemList.add(checkBoxItem);
         _settingNameToCheckBoxItemMap.put(algorithmSettingEnum.getName(), checkBoxItem);
       }
       else
       {
         java.util.List<String> selectionList = new ArrayList<String>();
         for (String selectionItem : algorithmSettingEnum.getSelectionList())
         {
           selectionList.add(selectionItem);
         }
         String message = null;
         String selectedSetting = _project.getProjectAlgorithmSelectionListSetting(algorithmSettingEnum);
         
         // add note if project thickness table version is not available in current system, 
         // but it is still allowed to use and appear in advance settings
         if (algorithmSettingEnum.equals(ProjectAlgorithmSettingEnum.SOLDER_THICKNESS_VERSION_SETTING))
         {
           if (selectionList.contains(selectedSetting) == false)
           {
             selectionList.add(selectedSetting);
             message = StringLocalizer.keyToString("MMGUI_PROJECT_THICKNESS_VERSION_NOT_AVAILABLE_MESSAGE_KEY");
           }
         }
         ComboBoxItem comboBoxItem = new ComboBoxItem(algorithmSettingEnum.getName(), selectedSetting, selectionList, message);
         algorithmComboBoxItemList.add(comboBoxItem);
         _settingNameToComboBoxItemMap.put(algorithmSettingEnum.getName(), comboBoxItem);
       }
     }
     algorithmSettingsDialogPanel = new AdvanceSettingsDialogPanel(StringLocalizer.keyToString("MMGUI_SETUP_PROJECT_ALGORITHM_SETTING_OPTIONS_KEY"), algorithmComboBoxItemList, algorithmCheckBoxItemList);
     advanceSettingsDialogPanelList.add(algorithmSettingsDialogPanel);
     
     //adding production settings dialog
     AdvanceSettingsDialogPanel productionSettingsDialogPanel = null;
     java.util.List<ComboBoxItem> productionComboBoxItemList = new ArrayList<ComboBoxItem>();
     java.util.List<CheckBoxItem> productionCheckBoxItemList = new ArrayList<CheckBoxItem>();
     for(ProjectProductionSettingEnum productionSettingEnum : projectProductionSettingEnumList)
     {
       if(productionSettingEnum.getSelectionList() == null)
       {
         CheckBoxItem checkBoxItem = new CheckBoxItem(productionSettingEnum.getName(), _project.getProjectProductionSetting(productionSettingEnum), null);
         productionCheckBoxItemList.add(checkBoxItem);
         _settingNameToCheckBoxItemMap.put(productionSettingEnum.getName(), checkBoxItem);
       }
       else
       {
         ComboBoxItem comboBoxItem = new ComboBoxItem(productionSettingEnum.getName(), _project.getProjectProductionSelectionListSetting(productionSettingEnum), productionSettingEnum.getSelectionList(), null);
         productionComboBoxItemList.add(comboBoxItem);
         _settingNameToComboBoxItemMap.put(productionSettingEnum.getName(), comboBoxItem);
       }
     }
     productionSettingsDialogPanel = new AdvanceSettingsDialogPanel(StringLocalizer.keyToString("MMGUI_SETUP_PROJECT_PRODUCTION_SETTING_OPTIONS_KEY"), productionComboBoxItemList, productionCheckBoxItemList);
     advanceSettingsDialogPanelList.add(productionSettingsDialogPanel);
     
     //Khaw Chek Hau - XCR2943: Alignment Fail Due to Too Close To Edge
     //adding alignment settings dialog
     AdvanceSettingsDialogPanel alignmentSettingsDialogPanel = null;
     java.util.List<ComboBoxItem> alignmentComboBoxItemList = new ArrayList<ComboBoxItem>();
     java.util.List<CheckBoxItem> alignmentCheckBoxItemList = new ArrayList<CheckBoxItem>();
     for(ProjectAlignmentSettingEnum alignmentSettingEnum : projectAlignmentSettingEnumList)
     {
       if(alignmentSettingEnum.getSelectionList() == null)
       {
         CheckBoxItem checkBoxItem = new CheckBoxItem(alignmentSettingEnum.getName(), _project.getProjectAlignmentSetting(alignmentSettingEnum), null);
         alignmentCheckBoxItemList.add(checkBoxItem);
         _settingNameToCheckBoxItemMap.put(alignmentSettingEnum.getName(), checkBoxItem);
       }
       else
       {
         ComboBoxItem comboBoxItem = new ComboBoxItem(alignmentSettingEnum.getName(), _project.getProjectAlignmentSelectionListSetting(alignmentSettingEnum), alignmentSettingEnum.getSelectionList(), null);
         alignmentComboBoxItemList.add(comboBoxItem);
         _settingNameToComboBoxItemMap.put(alignmentSettingEnum.getName(), comboBoxItem);
       }
     }
     alignmentSettingsDialogPanel = new AdvanceSettingsDialogPanel(StringLocalizer.keyToString("MMGUI_SETUP_PROJECT_ALIGNMENT_SETTING_OPTIONS_KEY"), alignmentComboBoxItemList, alignmentCheckBoxItemList);
     advanceSettingsDialogPanelList.add(alignmentSettingsDialogPanel);
     
     AdvanceSettingsDialog advanceSettingsDialog = new AdvanceSettingsDialog(MainMenuGui.getInstance(),
                                                  StringLocalizer.keyToString("MMGUI_SETUP_ADVANCED_RECIPE_SETTINGS_KEY"),
                                                  StringLocalizer.keyToString("MMGUI_SETUP_PROJECT_ADVANCE_SETTING_OPTIONS_KEY"),
                                                  StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                                  StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                                  StringLocalizer.keyToString("GUI_RESET_INTIAL_SETTING_KEY"),
                                                  advanceSettingsDialogPanelList);

     int retVal = advanceSettingsDialog.showDialog();
     if (retVal == JOptionPane.OK_OPTION) 
     {
       boolean isResetToIntialSetting = advanceSettingsDialog.getIsResetToIntialSetting();
       if (isResetToIntialSetting)
       {
         Object isEnableLargeImageView = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_GENERATE_LARGE_VIEW_IMAGE_INITIAL_SETTING);
         Object isGenerateMultiAngleView = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_GENERATE_MULTI_ANGLE_IMAGE_INITIAL_SETTING);
         Object isEnlargeRecontrusionRegion = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_ENLARGE_RECONSTRUSION_REGION_INITIAL_SETTING);
         Object isEnlargeAlignmentRegion = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_ENLARGE_ALIGNMENT_REGION_INTIAL_SETTING);
         Object isProjectByPass = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_PROJECT_BASED_BYPASS_MODE_INTIAL_SETTING);
         Object isJointBasedBGAInspection = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_SET_BGA_INSPECTION_REGION_TO_1X1_INTIAL_SETTING);
         final Object droLevelValue = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_DRO_INTIAL_SETTING);
         final Object magnificationValue = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_MAGNIFICATION_INTIAL_SETITNG);
         final Object userGainValue = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_USER_GAIN_INTIAL_SETITNG);
         final Object signalCompensationValue = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_SIGNAL_COMPENSATION_INITIAL_SETTING);
         final Object stageSpeedValue = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_STAGE_SPEED_INTIAL_SETTING);
         final Object interferenceCompensation = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_INTERFERENCE_COMPENSATION_INTIAL_SETITNG);
//         final Object camereAngle = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_XRAY_ENABLE_CAMERA_INDEX_INTIAL_SETTING);
         Object isGenerateNewScanRoute = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_GENERATE_NEW_SCAN_ROUTE_INTIAL_SETTING);
         Object newDroVersion = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_DRO_VERSION_INITIAL_SETTING);
         
         final java.util.List<StageSpeedEnum> stageSpeedEnumList = new ArrayList<StageSpeedEnum>();
         for (String stageSpeedName : (java.util.List<String>) stageSpeedValue)
         {
           try
           {
              StageSpeedEnum stageSpeedEnum = StageSpeedEnum.getStageSpeedToEnumMapValue(StringUtil.convertStringToDouble(stageSpeedName));
              stageSpeedEnumList.add(stageSpeedEnum);
           }
           catch(BadFormatException ex)
           {
             
           }
         }
         
//         final java.util.List<Integer> camereIdList = new ArrayList<Integer>();
//         for (String cameraIdName : (java.util.List<String>) camereAngle)
//         {
//           try
//           {
//              int cameraId = StringUtil.convertStringToInt(cameraIdName);
//              camereIdList.add(cameraId);
//           }
//           catch(BadFormatException ex)
//           {
//             
//           }
//         }
              
         _commandManager.beginCommandBlock(StringLocalizer.keyToString(new LocalizedString("MMGUI_SETUP_CHANGE_ADVANCED_RECIPE_SETTINGS_KEY", null)));
         try
         {
           _commandManager.execute(new ProjectSetEnableLargeViewImageCommand(_project,
             new Boolean((String) isEnableLargeImageView)));
           
           _commandManager.execute(new ProjectSetEnableGenerateMultiAngleImageCommand(_project,
             new Boolean((String) isGenerateMultiAngleView)));
           //         _commandManager.execute(new ProjectSetEnableGenerateComponentImageCommand(_project, 
           //                                 _settingNameToCheckBoxItemMap.get(ProjectImageViewSettingEnum.COMPONENT_IMAGE.getName()).getSelected()));
           _commandManager.execute(new ProjectSetEnableEnlargeReconstructionRegionCommand(_project,
             new Boolean((String) isEnlargeRecontrusionRegion)));
           
           _commandManager.execute(new ProjectSetUseNewScanRouteGenerationCommand(_project,
             new Boolean((String) isGenerateNewScanRoute)));
           _commandManager.execute(new ProjectSetEnableVariableDivNCommand(_project,
             new Boolean((String) isGenerateNewScanRoute)));
           
           String currentDynamicRangeOptimizationVersionString = (String) newDroVersion;
           int currentDynamicRangeOptimizationVersion = Integer.valueOf(currentDynamicRangeOptimizationVersionString);
           _commandManager.execute(new ProjectSetDynamicRangeOptimizationVersionCommand(_project, currentDynamicRangeOptimizationVersion));
           //Swee Yee - moved to subtype level
           //         _commandManager.execute(new ProjectSetEnableBGAJointBasedThresholdInspectionCommand(_project, 
           //                                 _settingNameToCheckBoxItemMap.get(ProjectAlgorithmSettingEnum.ENABLE_BGA_JOINT_BASED_THRESHOLD_INSPECTION.getName()).getSelected()));
           _commandManager.execute(new ProjectSetBGAInspectionRegionTo1x1Command(_project,
             new Boolean((String) isJointBasedBGAInspection)));
           _commandManager.execute(new ProjectSetEnableProjectBasedBypassModeCommand(_project,
             new Boolean((String) isProjectByPass)));

           //Khaw Chek Hau - XCR2943: Alignment Fail Due to Too Close To Edge
           _commandManager.execute(new ProjectSetEnableEnlargeAlignmentRegionCommand(_project,
             new Boolean((String) isEnlargeAlignmentRegion)));

           String currentVersionString = _settingNameToComboBoxItemMap.get(ProjectAlgorithmSettingEnum.SOLDER_THICKNESS_VERSION_SETTING.getName()).getSelected();
           currentVersionString = currentVersionString.substring(4);
           int currentversion = Integer.valueOf(currentVersionString);
           if (currentversion != _project.getThicknessTableVersion())
           {
             int answer = JOptionPane.showConfirmDialog(_mainUI,
               StringLocalizer.keyToString("GUI_REQUIRE_RETUNE_RECIPE_WARNING_KEY"),
               StringLocalizer.keyToString("GUI_WARNING_TITLE_KEY"),
               JOptionPane.YES_NO_OPTION,
               JOptionPane.WARNING_MESSAGE);
             if (answer == JOptionPane.YES_OPTION)
             {
               _commandManager.execute(new ProjectSetProjectThicknessTableVersionCommand(_project, currentversion));
             }
             else
             {
               //do nothing
             }
           }
           
           
           final BusyCancelDialog resetSubtypeStageSpeedSettingDialog = new BusyCancelDialog(_mainUI,
             StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_ADVANCE_SETTINGS_SET_STAGE_SPEED_KEY"),
             StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
             true);
           resetSubtypeStageSpeedSettingDialog.pack();
           SwingUtils.centerOnComponent(resetSubtypeStageSpeedSettingDialog, _mainUI);

           SwingWorkerThread.getInstance().invokeLater(new Runnable()
           {
             public void run()
             {
               XrayTesterException exception = null;
               try
               {
                 for (Subtype subtype : _project.getPanel().getSubtypes())
                 {
                   SubtypeAdvanceSettings setting = subtype.getSubtypeAdvanceSettings();
                   try
                   {
//                     System.err.println("start:"+System.currentTimeMillis());
//                     _commandManager.execute(new SubtypeSetCameraEnabledListCommand(_project, setting,
//                        camereIdList));
//                     System.err.println("end:"+System.currentTimeMillis());
                     _commandManager.execute(new SubtypeSetUserGainCommand(setting,
                       UserGainEnum.getUserGainToEnumMapValue(StringUtil.convertStringToInt((String) userGainValue))));

                     _commandManager.execute(new SubtypeAdvanceSettingsSetIntegrationLevelCommand(setting,
                       SignalCompensationEnum.getSignalCompensationEnum((String) signalCompensationValue)));

                     _commandManager.execute(new SubtypeAdvanceSettingsSetDynamicRangeOptimizationLevelCommand(setting,
                       DynamicRangeOptimizationLevelEnum.getDynamicRangeOptimizationLevelToEnumMapValue(StringUtil.convertStringToInt((String) droLevelValue))));
                     
                     if (Config.getInstance().getIntValue(SoftwareConfigEnum.MAXIMUM_DYNAMIC_RANGE_OPTIMIZATION_LEVEL) < StringUtil.convertStringToInt((String) droLevelValue))
                     {
                       _commandManager.execute(new SubtypeAdvanceSettingsSetDynamicRangeOptimizationLevelCommand(setting,
                         DynamicRangeOptimizationLevelEnum.getDynamicRangeOptimizationLevelToEnumMapValue(Config.getInstance().getIntValue(SoftwareConfigEnum.MAXIMUM_DYNAMIC_RANGE_OPTIMIZATION_LEVEL))));
                     }
                     else
                     {
                       _commandManager.execute(new SubtypeAdvanceSettingsSetDynamicRangeOptimizationLevelCommand(setting,
                         DynamicRangeOptimizationLevelEnum.getDynamicRangeOptimizationLevelToEnumMapValue(StringUtil.convertStringToInt((String) droLevelValue))));
                     }
                     
                     if (Config.getInstance().getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED) == false
                       && Config.getInstance().getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED) == false)
                     {
                        _commandManager.execute(new SubtypeAdvanceSettingsSetMagnificationTypeCommand(setting,
                           MagnificationTypeEnum.LOW));
                     }
                     else
                     {
                       if (((String) magnificationValue).contains(_lowMagnificationKey.substring(2)))
                       {
                         _commandManager.execute(new SubtypeAdvanceSettingsSetMagnificationTypeCommand(setting,
                           MagnificationTypeEnum.getMagnificationToEnumMapValue(_lowMagnificationKey)));
                       }
                       else
                       {
                         _commandManager.execute(new SubtypeAdvanceSettingsSetMagnificationTypeCommand(setting,
                           MagnificationTypeEnum.getMagnificationToEnumMapValue(_highMagnificationKey)));
                       }
                     }
                     
                     _commandManager.execute(new SubtypeSetStageSpeedCommand(setting,
                       stageSpeedEnumList));

                     _commandManager.execute(new SubtypeAdvanceSettingsSetInterferenceCompensationCommand(setting,
                       new Boolean((String) interferenceCompensation)));

                   }
                   catch (BadFormatException ex)
                   {

                   }

                 }
                 
               }
               catch (XrayTesterException xte)
               {
                 exception = xte;
               }
               finally
               {
                 SwingUtils.invokeLater(new Runnable()
                 {
                   public void run()
                   {
                     try
                     {
                       Thread.sleep(400);
                     }
                     catch (InterruptedException ex)
                     {
                       ex.printStackTrace();
                     }
                     resetSubtypeStageSpeedSettingDialog.dispose();
                   }
                 });

                 if (exception != null)
                 {
                   MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                     exception,
                     StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                     true);
                 }
               }
             }
           });
           resetSubtypeStageSpeedSettingDialog.setVisible(true);
         }
         catch (XrayTesterException exception)
         {
           System.out.println("Project advanced setting error.");
           MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
             exception,
             StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
             true);
         }
         finally
         {
           _commandManager.endCommandBlock();
         }
       }
       else
       {
         _commandManager.beginCommandBlock(StringLocalizer.keyToString(new LocalizedString("MMGUI_SETUP_CHANGE_ADVANCED_RECIPE_SETTINGS_KEY", null)));
         try
         {
           _commandManager.execute(new ProjectSetEnableLargeViewImageCommand(_project,
             _settingNameToCheckBoxItemMap.get(ProjectImageViewSettingEnum.LARGE_VIEW_IMAGE.getName()).getSelected()));
           _commandManager.execute(new ProjectSetEnableGenerateMultiAngleImageCommand(_project,
             _settingNameToCheckBoxItemMap.get(ProjectImageViewSettingEnum.MULTI_ANGLE_IMAGE.getName()).getSelected()));
           //         _commandManager.execute(new ProjectSetEnableGenerateComponentImageCommand(_project, 
           //                                 _settingNameToCheckBoxItemMap.get(ProjectImageViewSettingEnum.COMPONENT_IMAGE.getName()).getSelected()));
           _commandManager.execute(new ProjectSetEnableEnlargeReconstructionRegionCommand(_project,
             _settingNameToCheckBoxItemMap.get(ProjectReconstructionSettingEnum.ENLARGE_RECONSTRUCTION_REGION.getName()).getSelected()));
           //Swee Yee - moved to subtype level
           //         _commandManager.execute(new ProjectSetEnableBGAJointBasedThresholdInspectionCommand(_project, 
           //                                 _settingNameToCheckBoxItemMap.get(ProjectAlgorithmSettingEnum.ENABLE_BGA_JOINT_BASED_THRESHOLD_INSPECTION.getName()).getSelected()));
           _commandManager.execute(new ProjectSetBGAInspectionRegionTo1x1Command(_project,
             _settingNameToCheckBoxItemMap.get(ProjectReconstructionSettingEnum.SET_BGA_INSPECTION_REGION_TO_1X1.getName()).getSelected()));
           _commandManager.execute(new ProjectSetEnableProjectBasedBypassModeCommand(_project,
             _settingNameToCheckBoxItemMap.get(ProjectProductionSettingEnum.PROJECT_BASED_BYPASS_MODE.getName()).getSelected()));

           //Khaw Chek Hau - XCR2943: Alignment Fail Due to Too Close To Edge
           _commandManager.execute(new ProjectSetEnableEnlargeAlignmentRegionCommand(_project,
             _settingNameToCheckBoxItemMap.get(ProjectAlignmentSettingEnum.ENLARGE_ALIGNMENT_REGION.getName()).getSelected()));

           String currentVersionString = _settingNameToComboBoxItemMap.get(ProjectAlgorithmSettingEnum.SOLDER_THICKNESS_VERSION_SETTING.getName()).getSelected();
           currentVersionString = currentVersionString.substring(4);
           int currentversion = Integer.valueOf(currentVersionString);
           if (currentversion != _project.getThicknessTableVersion())
           {
             int answer = JOptionPane.showConfirmDialog(_mainUI,
               StringLocalizer.keyToString("GUI_REQUIRE_RETUNE_RECIPE_WARNING_KEY"),
               StringLocalizer.keyToString("GUI_WARNING_TITLE_KEY"),
               JOptionPane.YES_NO_OPTION,
               JOptionPane.WARNING_MESSAGE);
             if (answer == JOptionPane.YES_OPTION)
             {
               _commandManager.execute(new ProjectSetProjectThicknessTableVersionCommand(_project, currentversion));
             }
             else
             {
               //do nothing
             }
           }

           // Kok Chun, Tan - DRO version
           String currentDynamicRangeOptimizationVersionString = _settingNameToComboBoxItemMap.get(ProjectImageViewSettingEnum.DYNAMIC_RANGE_OPTIMIZATION_VERSION.getName()).getSelected();
           int currentDynamicRangeOptimizationVersion = Integer.valueOf(currentDynamicRangeOptimizationVersionString);
           _commandManager.execute(new ProjectSetDynamicRangeOptimizationVersionCommand(_project, currentDynamicRangeOptimizationVersion));

        if (_project.isGenerateByNewScanRoute() && 
            _settingNameToCheckBoxItemMap.get(ProjectReconstructionSettingEnum.NEW_SCAN_ROUTE_GENERATION.getName()).getSelected() == false)
           {
             int answer = JOptionPane.showConfirmDialog(_mainUI,
               StringLocalizer.keyToString("MMGUI_RESET_SUBTYPE_SPEED_SETTING_CONFIRM_KEY"),
               StringLocalizer.keyToString("GUI_WARNING_TITLE_KEY"),
               JOptionPane.YES_NO_OPTION,
               JOptionPane.WARNING_MESSAGE);
             if (answer == JOptionPane.YES_OPTION)
             {
               _commandManager.execute(new ProjectSetUseNewScanRouteGenerationCommand(_project,
                 _settingNameToCheckBoxItemMap.get(ProjectReconstructionSettingEnum.NEW_SCAN_ROUTE_GENERATION.getName()).getSelected()));
               _commandManager.execute(new ProjectSetEnableVariableDivNCommand(_project,
                 _settingNameToCheckBoxItemMap.get(ProjectReconstructionSettingEnum.NEW_SCAN_ROUTE_GENERATION.getName()).getSelected()));

               final BusyCancelDialog resetSubtypeStageSpeedSettingDialog = new BusyCancelDialog(_mainUI,
                 StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_ADVANCE_SETTINGS_SET_STAGE_SPEED_KEY"),
                 StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                 true);
               resetSubtypeStageSpeedSettingDialog.pack();
               SwingUtils.centerOnComponent(resetSubtypeStageSpeedSettingDialog, _mainUI);

               SwingWorkerThread.getInstance().invokeLater(new Runnable()
               {
                 public void run()
                 {
                   XrayTesterException exception = null;
                   try
                   {
                     for (Subtype subtype : _project.getPanel().getSubtypes())
                     {
                       SubtypeAdvanceSettings subtypeAdvanceSetting = subtype.getSubtypeAdvanceSettings();
                       if (subtypeAdvanceSetting.getStageSpeedList().size() > 1)
                       {
                         java.util.List<StageSpeedEnum> stageSpeedList = new ArrayList<StageSpeedEnum>();
                         stageSpeedList.add(StageSpeedEnum.ONE);
                         _commandManager.execute(new SubtypeSetStageSpeedCommand(subtypeAdvanceSetting, stageSpeedList));
                       }
                     }
                   }
                   catch (XrayTesterException xte)
                   {
                     exception = xte;
                   }
                   finally
                   {
                     SwingUtils.invokeLater(new Runnable()
                     {
                       public void run()
                       {
                         try
                         {
                           Thread.sleep(400);
                         }
                         catch (InterruptedException ex)
                         {
                           ex.printStackTrace();
                         }
                         resetSubtypeStageSpeedSettingDialog.dispose();
                       }
                     });

                     if (exception != null)
                     {
                       MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                         exception,
                         StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                         true);
                     }
                   }
                 }
               });
               resetSubtypeStageSpeedSettingDialog.setVisible(true);
             }
             else
             {
               //do nothing
             }
           }
           else
           {
             _commandManager.execute(new ProjectSetUseNewScanRouteGenerationCommand(_project,
               _settingNameToCheckBoxItemMap.get(ProjectReconstructionSettingEnum.NEW_SCAN_ROUTE_GENERATION.getName()).getSelected()));
             _commandManager.execute(new ProjectSetEnableVariableDivNCommand(_project,
               _settingNameToCheckBoxItemMap.get(ProjectReconstructionSettingEnum.NEW_SCAN_ROUTE_GENERATION.getName()).getSelected()));
           }
         }
         catch (XrayTesterException ex)
         {
           System.out.println("Project advanced setting error.");
           MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
             ex,
             StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
             true);
         }
         finally
         {
           _commandManager.endCommandBlock();
         }
       }
     }
     advanceSettingsDialogPanelList.clear();
     projectImageViewSettingEnumList.clear();
     projectReconstructionSettingEnumList.clear();
     projectAlgorithmSettingEnumList.clear();
     projectProductionSettingEnumList.clear();
     imageViewComboBoxItemList.clear();
     imageViewCheckBoxItemList.clear();
     reconstructionComboBoxItemList.clear();
     reconstructionCheckBoxItemList.clear();
     algorithmComboBoxItemList.clear();
     algorithmCheckBoxItemList.clear();
     productionComboBoxItemList.clear();
     productionCheckBoxItemList.clear();
     alignmentComboBoxItemList.clear();
     alignmentCheckBoxItemList.clear();
   }
   
  /**
   * @author Siew Yeng
   */
  private void copyAlignmentPadsIfNecessary()
  {
    if(_project.getPanel().getPanelSettings().isPanelBasedAlignment())
      return;
    
    //Perform checking only when it is a long panel with board-based alignment
    try
    {
      for(Board board : _project.getPanel().getBoards())
      {
        BoardSettings boardSettings = board.getBoardSettings();
        java.util.List<AlignmentGroup> leftAlignmentGroups = new ArrayList();
        java.util.List<AlignmentGroup> rightAlignmentGroups = new ArrayList();
        if(boardSettings.hasLeftAlignmentGroups())
          leftAlignmentGroups = boardSettings.getLeftAlignmentGroupsForLongPanel();
        if(boardSettings.hasRightAlignmentGroups())
          rightAlignmentGroups = boardSettings.getRightAlignmentGroupsForLongPanel();
        
        if(boardSettings.useLeftAlignmentGroup())
        {
          for(int index = 0; index < leftAlignmentGroups.size(); index++)
          {
            AlignmentGroup leftGroup = leftAlignmentGroups.get(index);
            if(leftGroup.isEmpty())
            {
              AlignmentGroup rightGroup = rightAlignmentGroups.get(index);
              if(rightGroup.isEmpty())
                break;

              _commandManager.execute(new AddFeaturesToAlignmentGroupCommand(leftGroup,
                                                                              rightGroup.getPads(), 
                                                                              new ArrayList<Fiducial>()));

              java.util.List<Pad> padsToRemove = new ArrayList(rightGroup.getPads());
              _commandManager.execute(new RemoveFeaturesFromAlignmentGroupCommand(rightGroup, padsToRemove,new ArrayList<Fiducial>()));
            }
          }
        }
        else
        {
          for(int index = 0; index < rightAlignmentGroups.size(); index++)
          {
            AlignmentGroup rightGroup = rightAlignmentGroups.get(index);
            if(rightGroup.isEmpty())
            {
              AlignmentGroup leftGroup = leftAlignmentGroups.get(index);
              if(leftGroup.isEmpty())
                break;

              _commandManager.execute(new AddFeaturesToAlignmentGroupCommand(rightGroup,
                                                                             leftGroup.getPads(), 
                                                                             new ArrayList<Fiducial>()));

              java.util.List<Pad> padsToRemove = new ArrayList(leftGroup.getPads());
              _commandManager.execute(new RemoveFeaturesFromAlignmentGroupCommand(leftGroup, padsToRemove ,new ArrayList<Fiducial>()));
            }
          }
        }
      }
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                    true);
      updatePanelRotation();
    }
  }
  
  /**
   * @author Kok Chun, Tan
   * //XCR - 2621, Variant Management
   */
  private void addVariationButton_actionPerformed(ActionEvent actionEvent) throws DatastoreException
  {
    AddVariationDialog addVariationDialog = new AddVariationDialog(_mainUI, true);
    addVariationDialog.pack();
    SwingUtils.centerOnComponent(addVariationDialog, _mainUI);
    addVariationDialog.setVisible(true);
    _noLoadSettingLayoutTableModel.fireTableDataChanged();
  }
  
  /**
   * @author Kok Chun, Tan
   * //XCR - 2621, Variant Management
   */
  private void deleteVariationButton_actionPerformed(ActionEvent actionEvent) throws DatastoreException
  {
    java.util.List<VariationSetting> variationsToDelete = _noLoadSettingLayoutTable.getSelectedVariations();
    Map<String, java.util.List<String>> boardTypeToRefDesMap = new LinkedHashMap<>();
    
    if (variationsToDelete.isEmpty())
      return;
    
    _projectObservable.setEnabled(false);
    try
    {
      PanelSettings panelSettings = _project.getPanel().getPanelSettings();
      panelSettings.getAllVariationSettings().removeAll(variationsToDelete);
      _noLoadSettingLayoutTableModel.fireTableDataChanged();
      
      for (VariationSetting variation : variationsToDelete)
      {
        if (variation.isEnable())
        {
          // set to all loaded after enabled variation is delete
          VariationSettingManager.getInstance().setupVariationSetting(false, _project, boardTypeToRefDesMap);
          _project.setEnabledVariationName(VariationSettingManager.ALL_LOADED);
          _project.setSelectedVariationName(VariationSettingManager.ALL_LOADED);
          break;
        }
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(variationsToDelete, variationsToDelete, null, VariationSettingEnum.ADD_OR_REMOVE);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void enablePreSelectVariationCheckbox_actionPerformed(ActionEvent e)
  {
    try
    {
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SN_SET_PRE_SELECT_VARIATION_KEY"));
      _commandManager.execute(new ConfigSetPreSelectVariationCommand(_enablePreSelectVariationCheckbox.isSelected(),
                                                                     _project));
      _commandManager.endCommandBlock();
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void updatePreSelectVariationData()
  {
    _enablePreSelectVariationCheckbox.setSelected(_project.enablePreSelectVariation());
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void updateVariationSettingData()
  {
    _noLoadSettingLayoutTableModel.fireTableDataChanged();
  }
}
