package com.axi.v810.gui;

import com.axi.util.MathUtil;
import com.axi.util.MathUtilEnum;
import com.axi.util.Pair;
import com.axi.v810.business.testResults.MeasurementUnitsEnum;

/**
 * @author Andy Mechtenberg
 */
public class AlgorithmSettingUtil
{
  /**
   * This will convert a native AlgorithmSetting value into the proper units, and return an Object (Float, Integer,String) etc
   * after the units conversion.
   * @author Andy Mechtenberg
   */
  public static Object getAlgorithmSettingDisplayValue(Object settingValue, MeasurementUnitsEnum settingUnits, MathUtilEnum displayUnits)
  {
    Pair<MeasurementUnitsEnum, Object> convertedValue = MeasurementUnitsEnum.convertToTargetUnitsIfNecessary(settingUnits, settingValue, displayUnits);
    settingUnits = convertedValue.getFirst();
    settingValue = convertedValue.getSecond();

    if (settingUnits.equals(MeasurementUnitsEnum.MILS) && settingValue instanceof Number)
    {
      settingValue = MathUtil.roundToPlaces(((Number) settingValue).floatValue(), MeasurementUnitsEnum.MILS.getNumberOfDecimalPlaces());
    }
    else if (settingUnits.equals(MeasurementUnitsEnum.SQUARE_MILS) && settingValue instanceof Number)
    {
      settingValue = MathUtil.roundToPlaces(((Number) settingValue).floatValue(), MeasurementUnitsEnum.SQUARE_MILS.getNumberOfDecimalPlaces());
    }
    return settingValue;
  }
  
  /**
   * This will take a units-converted algorithm setting value, and convert it into a user-displayable string, with units and decimal points.
   * @author Andy Mechtenberg
   */
   public static String getAlgorithmSettingDisplayText(Object displaySettingValue, MeasurementUnitsEnum settingUnits, MathUtilEnum displayUnits)
   {
     MeasurementUnitsEnum newMeasurementUnitsEnum = getAlgorithmSettingDisplayUnitsEnum(settingUnits, displayUnits);
     String valueText = MeasurementUnitsEnum.formatNumberIfNecessary(newMeasurementUnitsEnum, displaySettingValue);
     String valueWithUnitsLabelText = valueText; // default to no units      
     valueWithUnitsLabelText = MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(newMeasurementUnitsEnum, valueText);
     return valueWithUnitsLabelText;
   }
   
   /**
    * @author Tan, Hsia Fen
    * XCR1763 - Get correct unit of measurement 
    */
  public static MeasurementUnitsEnum getAlgorithmSettingDisplayUnitsEnum(MeasurementUnitsEnum settingUnits, MathUtilEnum displayUnits)
  {
    // millimeters are special - we'll let the user use Mils for these if they've indicated that they want to use english settings.
    if (settingUnits.equals(MeasurementUnitsEnum.MILLIMETERS))
    {
      if (displayUnits.isMetric() == false)
      {
        // convert to display in mils instead
        // We have actually already converted the value to Mils in ThresholdTableModel.getValueAt()
        // (this was required for the editing), so we don't need to do it here.
        settingUnits = MeasurementUnitsEnum.MILS;
      }
    }
    else if (settingUnits.equals(MeasurementUnitsEnum.SQUARE_MILLIMETERS))
    {
      if (displayUnits.isMetric() == false)
      {
        settingUnits = MeasurementUnitsEnum.SQUARE_MILS;
      }
    }
    else if (settingUnits.equals(MeasurementUnitsEnum.CUBIC_MILLIMETERS))
    {
      if (displayUnits.isMetric() == false)
      {
        settingUnits = MeasurementUnitsEnum.CUBIC_MILS;
      }
    }
    return settingUnits;
  }
 
  /**
   * This takes an native AlgorithmSetting value, and converts it into a GUI-display ready string, complete with units and 
   * the correct number of decimal places.
   * @author Andy Mechtenberg
   */
  public static String getAlgorithmSettingValueDisplayText(Object settingValue, MeasurementUnitsEnum settingUnits, MathUtilEnum displayUnits)
  {
    Object displayValue = getAlgorithmSettingDisplayValue(settingValue, settingUnits, displayUnits);
    String displayText = getAlgorithmSettingDisplayText(displayValue, settingUnits, displayUnits);
    return displayText;
    
//    Pair<MeasurementUnitsEnum, Object> convertedValue = MeasurementUnitsEnum.convertToTargetUnitsIfNecessary(settingUnits, settingValue, displayUnits);
//    settingUnits = convertedValue.getFirst();
//    settingValue = convertedValue.getSecond();
//
//    if (settingUnits.equals(MeasurementUnitsEnum.MILS) && settingValue instanceof Number)
//    {
//      settingValue = MathUtil.roundToPlaces(((Number) settingValue).floatValue(), MeasurementUnitsEnum.MILS.getNumberOfDecimalPlaces());
//    }
//    else if (settingUnits.equals(MeasurementUnitsEnum.SQUARE_MILS) && settingValue instanceof Number)
//    {
//      settingValue = MathUtil.roundToPlaces(((Number) settingValue).floatValue(), MeasurementUnitsEnum.SQUARE_MILS.getNumberOfDecimalPlaces());
//    }
//
//    // millimeters are special - we'll let the user use Mils for these if they've indicated that they want to use english settings.
//    if (settingUnits.equals(MeasurementUnitsEnum.MILLIMETERS))
//    {
//      if (displayUnits.isMetric() == false)
//      {
//        // convert to display in mils instead
//        // We have actually already converted the value to Mils in ThresholdTableModel.getValueAt()
//        // (this was required for the editing), so we don't need to do it here.
//        settingUnits = MeasurementUnitsEnum.MILS;
//      }
//    }
//    else if (settingUnits.equals(MeasurementUnitsEnum.SQUARE_MILLIMETERS))
//    {
//      if (displayUnits.isMetric() == false)
//      {
//        settingUnits = MeasurementUnitsEnum.SQUARE_MILS;
//      }
//    }
//    else if (settingUnits.equals(MeasurementUnitsEnum.CUBIC_MILLIMETERS))
//    {
//      if (displayUnits.isMetric() == false)
//      {
//        settingUnits = MeasurementUnitsEnum.CUBIC_MILS;
//      }
//    }
//
//    String valueText = MeasurementUnitsEnum.formatNumberIfNecessary(settingUnits, settingValue);
//    String valueWithUnitsLabelText = valueText; // default to no units      
//    valueWithUnitsLabelText = MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(settingUnits, valueText);
//    return valueWithUnitsLabelText;
  }
}
