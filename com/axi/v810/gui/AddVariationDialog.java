package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 *
 * @author kok-chun.tan
 */
public class AddVariationDialog extends EscapeDialog
{
  private static final String _appName = StringLocalizer.keyToString("AVD_GUI_NAME_KEY");
  private static final String _textFileFilterDescription = StringLocalizer.keyToString("AVD_ZIP_FILE_DESCRIPTION_KEY");
  private static com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private MainMenuGui _mainUI;
  // gui components
  private JPanel _actionPanel = new JPanel();
  private JButton _updateButton = new JButton();
//  private JButton _exitButton = new JButton();
  private JFileChooser _setComponentToNoLoadFileChooser = null;

  private String _setComponentToNoLoadInputDirectory = null;
  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();

  private JPanel _chooseSetComponentToNoLoadFilePanel = new JPanel();
  private BorderLayout _chooseSetComponentToNoLoadFilePanelBorderLayout = new BorderLayout(5, 10);
  private JLabel _setComponentToNoLoadFileLabel = new JLabel();
  private JButton _setComponentToNoLoadFileBrowseButton = new JButton();
  private JTextField _setComponentToNoLoadFileTextField = new JTextField();
  private BorderLayout _setComponentToNoLoadSelectionPanelBorderLayout = new BorderLayout();
  private JPanel _setComponentToNoLoadSelectionPanel = new JPanel();
  
  private JPanel _variationNamePanel = new JPanel();
  private BorderLayout _variationNamePanelBorderLayout = new BorderLayout(5,10);
  private JLabel _variationNameLabel = new JLabel();
  private JTextField _variationNameTextField = new JTextField();

  private Border _empty55105SpaceBorder;
  private Border _empty5555SpaceBorder;
  private JPanel _buttonPanel = new JPanel();
  private GridLayout _buttonPanelGridLayout = new GridLayout();
  private FlowLayout _actionPanelFlowLayout = new FlowLayout();
  private JPanel _mainPanel = new JPanel();
  private JPanel _optionPanel = new JPanel();
  private FocusAdapter _textFieldFocusAdapter;
  
  private JLabel _fileTypeLabel = new JLabel();
  private JPanel _fileTypePanel = new JPanel();
  private JPanel _radioButtonPanel = new JPanel();
  private ButtonGroup _radioButtonGroup = new ButtonGroup();
  private JRadioButton _chooseSetComponentToNoLoadFileRadioButton = new JRadioButton(StringLocalizer.keyToString("AVD_GUI_SET_COMPONENT_TO_NO_LOAD_LABEL_KEY"), true);
  private JRadioButton _chooseSetComponentToTestFileRadioButton = new JRadioButton(StringLocalizer.keyToString("AVD_GUI_SET_COMPONENT_TO_TEST_LABEL_KEY"), false);

  /**
   * @author Kok Chun, Tan
   */
  public AddVariationDialog(Frame parentFrame, boolean modal)
  {
    super(parentFrame, _appName, modal);
    Assert.expect(parentFrame != null);
    try
    {
      _mainUI = MainMenuGui.getInstance();
      jbInit();
    }
    catch (RuntimeException e)
    {
      Assert.logException(e);
    }
  }

  /**
   * @author Kok Chun, Tan
   */
  private void jbInit()
  {
    _textFieldFocusAdapter = new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        populateUpdateButton();
      }
      public void focusLost(FocusEvent e)
      {
        populateUpdateButton();
      }
    };
    
    _empty55105SpaceBorder = BorderFactory.createEmptyBorder(5, 5, 10, 5);
    _empty5555SpaceBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);

    _updateButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _updateButton.setText(StringLocalizer.keyToString("BUD_GUI_UPDATE_BUTTON_KEY"));
    _updateButton.setEnabled(false);
    _updateButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        updateButton_actionPerformed(e);
      }
    });
    
    _actionPanel.setLayout(_actionPanelFlowLayout);
    setResizable(true);
    _actionPanel.setMinimumSize(new Dimension(405, 37));

    _chooseSetComponentToNoLoadFilePanel.setLayout(_chooseSetComponentToNoLoadFilePanelBorderLayout);
    _setComponentToNoLoadFileLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _setComponentToNoLoadFileLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _setComponentToNoLoadFileLabel.setLabelFor(_setComponentToNoLoadFileTextField);
    _setComponentToNoLoadFileLabel.setText(StringLocalizer.keyToString("AVD_GUI_SET_FILE_LABEL_KEY"));
    _setComponentToNoLoadFileBrowseButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _setComponentToNoLoadFileBrowseButton.setText(StringLocalizer.keyToString("GUI_BROWSE_BUTTON_KEY"));
    _setComponentToNoLoadFileBrowseButton.setEnabled(true);
    _setComponentToNoLoadFileBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setComponentToNoLoadFileBrowseButton_actionPerformed(e);
      }
    });

    _setComponentToNoLoadFileTextField.setText("");
    _setComponentToNoLoadFileTextField.setHorizontalAlignment(SwingConstants.LEFT);
    _setComponentToNoLoadFileTextField.setPreferredSize(new Dimension(220, 20));
    _setComponentToNoLoadFileTextField.setEnabled(true);
    _setComponentToNoLoadFileTextField.setEditable(true);
    _setComponentToNoLoadFileTextField.addFocusListener(_textFieldFocusAdapter);
    _setComponentToNoLoadSelectionPanelBorderLayout.setHgap(10);
    _setComponentToNoLoadSelectionPanelBorderLayout.setVgap(10);
    _setComponentToNoLoadSelectionPanel.setLayout(_setComponentToNoLoadSelectionPanelBorderLayout);
    _setComponentToNoLoadSelectionPanel.setBorder(_empty55105SpaceBorder);
    _setComponentToNoLoadSelectionPanel.setMinimumSize(new Dimension(405, 27));

    _buttonPanel.setLayout(_buttonPanelGridLayout);
    _buttonPanelGridLayout.setColumns(2);
    _buttonPanelGridLayout.setHgap(10);
    _mainPanel.setLayout(new BorderLayout(5, 10));
    _optionPanel.setLayout(new BorderLayout());

    _buttonPanel.add(_updateButton, null);
//    _buttonPanel.add(_exitButton, null);
    _actionPanel.add(_buttonPanel, null);
    
    _variationNamePanel.setLayout(_variationNamePanelBorderLayout);
    _variationNamePanel.setBorder(_empty55105SpaceBorder);
    _variationNamePanel.setMinimumSize(new Dimension(405, 27));
    _variationNameLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _variationNameLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _variationNameLabel.setLabelFor(_variationNameTextField);
    _variationNameLabel.setText(StringLocalizer.keyToString("AVD_GUI_SET_VARIATION_NAME_LABEL_KEY"));
    _variationNameTextField.setText("");
    _variationNameTextField.setHorizontalAlignment(SwingConstants.LEFT);
    _variationNameTextField.setPreferredSize(new Dimension(150, 25));
    _variationNameTextField.setEnabled(true);
    _variationNameTextField.setEditable(true); 
    _variationNameTextField.addFocusListener(_textFieldFocusAdapter);

    _variationNamePanel.add(_variationNameLabel, BorderLayout.WEST);
    _variationNamePanel.add(_variationNameTextField, BorderLayout.CENTER);
    _setComponentToNoLoadSelectionPanel.add(_setComponentToNoLoadFileLabel, BorderLayout.WEST);
    _setComponentToNoLoadSelectionPanel.add(_setComponentToNoLoadFileBrowseButton, BorderLayout.EAST);
    _setComponentToNoLoadSelectionPanel.add(_setComponentToNoLoadFileTextField, BorderLayout.CENTER);
    
    _fileTypeLabel.setText(StringLocalizer.keyToString("AVD_GUI_FILE_TYPE_LABEL_KEY"));
    _radioButtonGroup.add(_chooseSetComponentToNoLoadFileRadioButton);
    _radioButtonGroup.add(_chooseSetComponentToTestFileRadioButton);
    _fileTypePanel.setLayout(new BorderLayout(5, 10));
    _fileTypePanel.setBorder(_empty55105SpaceBorder);
    _radioButtonPanel.setLayout(new BorderLayout(5, 10));
    _radioButtonPanel.add(_chooseSetComponentToNoLoadFileRadioButton, BorderLayout.WEST);
    _radioButtonPanel.add(_chooseSetComponentToTestFileRadioButton, BorderLayout.CENTER);
    _fileTypePanel.add(_fileTypeLabel, BorderLayout.WEST);
    _fileTypePanel.add(_radioButtonPanel, BorderLayout.CENTER);
    
    getContentPane().add(_mainPanel, BorderLayout.CENTER);
    _chooseSetComponentToNoLoadFilePanel.add(_setComponentToNoLoadSelectionPanel, BorderLayout.WEST);
    _optionPanel.add(_variationNamePanel, BorderLayout.NORTH);
    _optionPanel.add(_fileTypePanel, BorderLayout.CENTER);
    _optionPanel.add(_chooseSetComponentToNoLoadFilePanel, BorderLayout.SOUTH);
    _mainPanel.add(_optionPanel, BorderLayout.NORTH);
    _mainPanel.add(_actionPanel, BorderLayout.SOUTH);
    _mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
  }

  /**
   * @author Kok Chun, Tan
   */
  private void updateButton_actionPerformed(ActionEvent e)
  {
    Assert.expect(_variationNameTextField != null);
    Assert.expect(_setComponentToNoLoadFileTextField != null);

    String variationName = null;
    String setComponentToNoLoadFileName = null;

    if (_variationNameTextField != null)
    {
      variationName = _variationNameTextField.getText();
    }

    if (_setComponentToNoLoadFileTextField != null)
    {
      setComponentToNoLoadFileName = _setComponentToNoLoadFileTextField.getText();
    }

    if (setComponentToNoLoadFileName.length() == 0)
    {
      JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("BUD_FILE_NOT_SPECIFIED_KEY"),
        _appName, JOptionPane.ERROR_MESSAGE);

      _updateButton.setEnabled(false);
      return;
    }
    
    if (variationName.isEmpty())
    {
      JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("AVD_NAME_NOT_SPECIFIED_KEY"),
        _appName, JOptionPane.ERROR_MESSAGE);

      _updateButton.setEnabled(false);
      return;
    }
    updateVariation(setComponentToNoLoadFileName, variationName);
  }

  /**
   * @author Kok Chun, Tan
   */
  private void updateVariation(final String setComponentToNoLoadFileName, final String newVariationName)
  {
    Assert.expect(setComponentToNoLoadFileName != null);
    Assert.expect(newVariationName != null);
    
    boolean isVariationNameValid = false;
    if (newVariationName.trim().length() > 0)
    {
      isVariationNameValid = newVariationName.matches("\\w*");
    }

    boolean isFileNameValid = false;
    if (setComponentToNoLoadFileName.length() > 0)
    {
      isFileNameValid = isFileNameValid(setComponentToNoLoadFileName);
    }

    if (isFileNameValid == false)
    {
      return;
    }
    
    if (isVariationNameValid == false)
    {
      LocalizedString message = new LocalizedString("AVD_NAME_NOT_VALID_KEY", new Object[]{});
      JOptionPane.showMessageDialog(this, StringUtil.format(StringLocalizer.keyToString(message), 50),
        _appName, JOptionPane.ERROR_MESSAGE);
      
      _updateButton.setEnabled(false);
      return;
    }

    // check to see if a project is loaded. Only loaded project can update BOM.
    if (Project.isCurrentProjectLoaded() == false)
    {
      LocalizedString message = new LocalizedString("BUD_PROJECT_NOT_LOADED_KEY", new Object[]{});
      JOptionPane.showMessageDialog(this, StringUtil.format(StringLocalizer.keyToString(message), 50),
        _appName, JOptionPane.ERROR_MESSAGE);

      _updateButton.setEnabled(false);
      return;
    }
    
    // check if the variation name already exist or not
    if (Project.getCurrentlyLoadedProject().getPanel().getPanelSettings().hasVariationSettingBasedOnVariationName(newVariationName))
    {
      LocalizedString message = new LocalizedString("AVD_NAME_EXIST_KEY", new Object[]{});
      JOptionPane.showMessageDialog(this, StringUtil.format(StringLocalizer.keyToString(message), 50),
        _appName, JOptionPane.ERROR_MESSAGE);

      _updateButton.setEnabled(false);
      return;
    }

    final BusyCancelDialog busyDialog = new BusyCancelDialog(
      (Frame) getOwner(),
      StringLocalizer.keyToString("AVD_UPDATING_KEY"),
      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
      true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, (Frame) getOwner());
    _swingWorkerThread.invokeLater(new Runnable()
    {
      String message = "";
      public void run()
      {
        boolean isVariationAdded = false;
        boolean newEnableState = true;
        boolean oldEnableState = true;
        VariationSetting oldVariationSetting = null;
        VariationSetting newVariationSetting = null;
        try
        {
          Project project = Project.getCurrentlyLoadedProject();
          PanelSettings panelSettings = project.getPanel().getPanelSettings();
          VariationFileReader addVariationFileReader = VariationFileReader.getInstance();
          isVariationAdded = addVariationFileReader.readDataFile(Project.getCurrentlyLoadedProject(), setComponentToNoLoadFileName, newVariationName, _chooseSetComponentToNoLoadFileRadioButton.isSelected());
          if (isVariationAdded)
          {
            message = StringLocalizer.keyToString("AVD_ADD_COMPLETED_KEY");
            
            newEnableState = true;
            oldEnableState = true;
            newVariationSetting = panelSettings.getVariationSettingBasedOnVariationName(newVariationName);
            if (panelSettings.hasVariationSettingBasedOnVariationName(project.getEnabledVariationName()))
              oldVariationSetting = panelSettings.getVariationSettingBasedOnVariationName(project.getEnabledVariationName());
            else
              oldEnableState = false;
            
            try
            {
              _commandManager.execute(new ConfigSetEnableVariationCommand(project,
                                                                          newEnableState,
                                                                          newVariationSetting,
                                                                          oldEnableState,
                                                                          oldVariationSetting));
            }
            catch (XrayTesterException ex)
            {
              MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
            }
          }
          else
          {
            if(_chooseSetComponentToNoLoadFileRadioButton.isSelected())
              message = StringLocalizer.keyToString("AVD_ADD_NOT_COMPLETED_KEY");
            else
            {
              message = StringLocalizer.keyToString("AVD_ADD_ALL_COMPONENTS_TESTED_KEY");
              
              if (panelSettings.hasVariationSettingBasedOnVariationName(project.getEnabledVariationName()))
                oldVariationSetting = panelSettings.getVariationSettingBasedOnVariationName(project.getEnabledVariationName());
              try
              {
                if (panelSettings.hasVariationSettingBasedOnVariationName(project.getEnabledVariationName()))
                {
                  newEnableState = false;
                  oldEnableState = false;
                  oldEnableState = true;
                  newVariationSetting = oldVariationSetting;
                  _commandManager.execute(new ConfigSetEnableVariationCommand(project,
                                          newEnableState,
                                          newVariationSetting,
                                          oldEnableState,
                                          oldVariationSetting));
                }
              }
              catch (XrayTesterException ex)
              {
                MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
              }
            }
          }
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              JOptionPane.showMessageDialog(AddVariationDialog.this, message,
                _appName, JOptionPane.INFORMATION_MESSAGE);
              exitDialog();
            }
          });
        }
        catch (final DatastoreException ie)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
              setEnabled(true); // allow user to press a button
              MessageDialog.reportIOError(AddVariationDialog.this, ie.getLocalizedMessage());
            }
          });
        }
        finally
        {
          _variationNameTextField.setText("");
          _setComponentToNoLoadFileTextField.setText("");
          _updateButton.setEnabled(false);
          
          // wait until visible
          while (busyDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              busyDialog.dispose();
            }
          });
        }
      }
    });
    busyDialog.setVisible(true);
  }

  /**
   * @author Kok Chun, Tan
   */
  private boolean isFileNameValid(final String textFileName)
  {
    Assert.expect(textFileName != null);

    String extension = FileUtil.getExtension(textFileName).toLowerCase();

    if (extension.endsWith(FileName.getTextFileExtension().toLowerCase()) == false)
    {
      JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("AVD_INVALID_SET_COMPONENT_TO_NO_LOAD_FILE_EXTENSION_KEY"),
        _appName, JOptionPane.ERROR_MESSAGE);

      _updateButton.setEnabled(false);
      return false;
    }

    String parent = FileUtil.getParent(textFileName);

    if (parent.isEmpty()) // need to specify full path.
    {
      JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("AVD_SET_COMPONENT_TO_NO_LOAD_DIRECTORY_NOT_SPECIFIED_KEY"),
        _appName, JOptionPane.ERROR_MESSAGE);

      _updateButton.setEnabled(false);
      return false;
    }

    File inFile = new File(textFileName);

    if (inFile.exists() == false)
    {
      JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("AVD_SET_COMPONENT_TO_NO_LOAD_FILE_DOES_NOT_EXIST_KEY"),
        _appName, JOptionPane.ERROR_MESSAGE);

      _updateButton.setEnabled(false);
      return false;
    }
    return true;
  }

  /**
   * @author Kok Chun, Tan
   */
  void setComponentToNoLoadFileBrowseButton_actionPerformed(ActionEvent e)
  {
    String fileNameFullPath = selectSetComponentToNoLoadFile();

    if (fileNameFullPath.length() > 0)
    {
      _setComponentToNoLoadInputDirectory = FileUtil.getParent(fileNameFullPath);
      _setComponentToNoLoadFileTextField.setText(fileNameFullPath);
    }
    populateUpdateButton();
  }

  /**
   * @author Kok Chun, Tan
   */
  private void exitDialog()
  {
    setVisible(false);
    dispose();
  }

  /**
   * This method displays a file chooser and return the file selected by the
   * user.
   *
   * @author Kok Chun, Tan
   */
  private String selectSetComponentToNoLoadFile()
  {
    if (_setComponentToNoLoadFileChooser == null)
    {
      // start in the same directory where the zip dialog puts zipped projects by default
      _setComponentToNoLoadFileChooser = new JFileChooser()
      {
        public void approveSelection()
        {
          File selectedFile = getSelectedFile();
          if (selectedFile.exists())
          {
            super.approveSelection();
          }
          else
          {
            String message = StringLocalizer.keyToString(new LocalizedString("FILE_DIALOG_SELECTION_DOES_NOT_EXIST_KEY", new Object[]
            {
              selectedFile
            }));
            JOptionPane.showMessageDialog(this, StringUtil.format(message, 50));
          }
        }
      };

      _setComponentToNoLoadFileChooser.setCurrentDirectory(new File(Directory.getProjectsDir()));

      // initialize the JFIle Dialog.
      javax.swing.filechooser.FileFilter fileFilter = _setComponentToNoLoadFileChooser.getAcceptAllFileFilter();
      _setComponentToNoLoadFileChooser.removeChoosableFileFilter(fileFilter);

      // create and instance of Zip File Filter
      FileFilterUtil textFileFilter = new FileFilterUtil(FileName.getTextFileExtension(), _textFileFilterDescription);

      _setComponentToNoLoadFileChooser.addChoosableFileFilter(textFileFilter);
      _setComponentToNoLoadFileChooser.setFileFilter(textFileFilter); // default to new txt file
    }

    if (_setComponentToNoLoadFileChooser.showDialog(this, StringLocalizer.keyToString("GUI_SELECT_FILE_KEY")) != JFileChooser.APPROVE_OPTION)
    {
      return "";
    }

    File selectedFile = _setComponentToNoLoadFileChooser.getSelectedFile();

    if (selectedFile.exists())
    {
      if (selectedFile.isFile())
      {
        return selectedFile.getPath();
      }
      else
      {
        return "";
      }
    }
    else
    {
      return "";
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void populateUpdateButton()
  {
    if (_setComponentToNoLoadFileTextField.getText().isEmpty() || _variationNameTextField.getText().isEmpty())
    {
      _updateButton.setEnabled(false);
    }
    else
    {
      _updateButton.setEnabled(true);
    }
  }
}
