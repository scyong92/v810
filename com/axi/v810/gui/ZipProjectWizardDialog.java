package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

import com.axi.util.*;
import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.v810.business.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * This wizard will allow the user to either zip up a project to move to another machine / tdw or will allow
 * the user to zip up a project to send to the lab illustrating a particular problem with the algorithms.
 *
 * @author Erica Wheatcroft
 */
public class ZipProjectWizardDialog extends WizardDialog
{
  private WizardPanel _currentVisiblePanel = null;

  // the archiver used to archive the necessary information.
  private ProjectArchiver _archiver = null;

  // this flag determins what type of wizard we will create. if set to true then the wizard will ask the user
  // for information regarding collecting diagnostic packages.
  private boolean _collectDiagnosticPackage = false;

  private WorkerThread _workerThread = new WorkerThread("Zip Project Thread");

  // these strings do not need to be localized as they are not shown to the user.
  private final static String _MAIN_PANEL = "mainPanel";
  private final static String _SECONDARY_PANEL = "secondaryPanel";
  private final static String _SUMMARY_PANEL = "summaryPanel";

  // titles of the two wizards.
  private final static String _ZIP_PROJECT_WIZARD_TITLE = StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_TITLE_KEY");
  private final static String _COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_TITLE = StringLocalizer.keyToString("COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_TITLE_KEY");

  // current title of the wizard
  private static String _currentTitle = null;

  // if the user has stated that they want to overwrite the destination file cause it already exists. remember that so we don't
  // keep asking them if they switch back and forth through the screens.
  private boolean _userWishesToOverwriteFile = false;

  // if the user has selected a new project to zip we want to flag all panels to reload themselves if necessary.
  private boolean _repopulatePanel = false;

  /**
   * @author Erica Wheatcroft
   */
  public ZipProjectWizardDialog(Frame parent, boolean modal, boolean collectDiagnosticPackage)
  {
    super(parent,
          modal,
          StringLocalizer.keyToString("WIZARD_NEXT_KEY"),
          StringLocalizer.keyToString("WIZARD_CANCEL_KEY"),
          StringLocalizer.keyToString("WIZARD_FINISH_KEY"),
          StringLocalizer.keyToString("WIZARD_BACK_KEY") );

    _collectDiagnosticPackage = collectDiagnosticPackage;

    // set up the title correctly
    if(_collectDiagnosticPackage == false)
      _currentTitle = _ZIP_PROJECT_WIZARD_TITLE;
    else
      _currentTitle = _COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_TITLE;

    // create a new archiver
    _archiver = new ProjectArchiver();

    // initialize the wizard
    initWizard();
  }

  /**
   * The user has selected another project to zip so clear the flag just in case.
   * @author Erica Wheatcroft
   */
  void clearOverwriteFileFlag()
  {
    _userWishesToOverwriteFile = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  void setRepopulatePanels(boolean repopulatePanels)
  {
    _repopulatePanel = repopulatePanels;
  }

  /**
   * @author Erica Wheatcroft
   */
  boolean repopulatePanels()
  {
    return _repopulatePanel;
  }


  /**
   * @author Erica Wheatcroft
   */
  protected void confirmExitOfDialog()
  {
    // confirm with the user if they want to cancel the dialog
    int response = JOptionPane.showConfirmDialog(this,
                                                 StringLocalizer.keyToString("EXIT_WIZARD_MESSAGE_KEY"),
                                                 _currentTitle,
                                                 JOptionPane.YES_NO_OPTION,
                                                 JOptionPane.QUESTION_MESSAGE);

    if (response == JOptionPane.NO_OPTION || response == JOptionPane.CLOSED_OPTION)
      return;

    // the user wants to cancel - the base class wizard dialog sent the default close operation so i will not need to
    // do it here
    dispose();

  }

  /**
   * This method is called when the user wants to complete the wizard process.
   *
   * @author Erica Wheatcroft
   */
  protected void handleCompleteProcessButtonEvent()
  {
    Assert.expect(_currentVisiblePanel != null, "current visible panel is not set");
    Assert.expect(_archiver != null);

    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(
        MainMenuGui.getInstance(),
        StringLocalizer.keyToString("CREATING_ZIP_KEY"),
        _currentTitle,
        StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
        true);

    busyCancelDialog.addCancelActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
//        FileUtilAxi.cancelZip();
        _archiver.abort();
      }
    });

    busyCancelDialog.pack();
    // set the size on the busy dialog so the the user can read the entire message
    busyCancelDialog.setSize(new Dimension(275,125));

    SwingUtils.centerOnComponent(busyCancelDialog, MainMenuGui.getInstance());

    _workerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          // now lets make sure they include the algorithm learning.
          if(_collectDiagnosticPackage == false)
            if (_archiver.doesAlgorithmLearningExist())
              _archiver.setIncludeAlgorithmLearning(true);
          _archiver.archive();
        }
        catch (final DatastoreException de)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              String message = de.getLocalizedMessage();
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                            StringUtil.format(message, 50),
                                            _currentTitle,
                                            JOptionPane.ERROR_MESSAGE);
              return;
            }
          });
        }
        finally
        {
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.setVisible(false);
        }
      }
    });

    busyCancelDialog.setVisible(true);
    dispose();
    // notify Home panel that project info may have changed
    MainMenuGui.getInstance().updateHomePanel();
  }

  /**
   * This method will navigate the wizard to the next screen.
   *
   * @author Erica Wheatcroft
   */
  protected void handleNextScreenButtonEvent()
  {
    Assert.expect(_currentVisiblePanel != null, "current visible panel is not set");

    String nextPanelName = null;
    String currentPanelName = _currentVisiblePanel.getWizardName();
    if(_collectDiagnosticPackage == false)
    {
      if(currentPanelName.equals(_MAIN_PANEL))
      {
        // if the project they chose is loaded and dirty, zip is not allowed.
        if (Project.isCurrentProjectLoaded())
        {
          Project loadedProject = Project.getCurrentlyLoadedProject();
          if (loadedProject.hasBeenModified())
          {
            String chosenForZipProjectName = _archiver.getProjectName();
            if (loadedProject.getName().equalsIgnoreCase(chosenForZipProjectName))
            {
              //error out
              JOptionPane.showMessageDialog(this,
                                            StringUtil.format(StringLocalizer.keyToString("ZIP_PROJECT_NEEDS_TO_BE_SAVED_KEY"), 50),
                                            StringLocalizer.keyToString("GISWK_TITLE_KEY"),
                                            JOptionPane.ERROR_MESSAGE);

              nextPanelName = _MAIN_PANEL;
              return;
            }
          }
        }

        // before we switch screens from the main panel to the secondary panel lets validate the destination to
        // make sure that it doesn't not already exist.
        Assert.expect(_archiver.hasDestinationFilename());
        File destinationFile = new File(_archiver.getDestinationFilename());
        if(destinationFile.exists() && _userWishesToOverwriteFile == false)
        {
          // now prompt the user if they would like to overwrite?
          String message = StringLocalizer.keyToString(new LocalizedString("ZIP_PROJECT_FILE_EXISTS_KEY", new Object[]{destinationFile}));
          int value = JOptionPane.showConfirmDialog(this,
                                                    StringUtil.format(message, 50),
                                                    StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_TITLE_KEY"),
                                                    JOptionPane.YES_NO_OPTION,
                                                    JOptionPane.WARNING_MESSAGE);
          if(value == JOptionPane.YES_OPTION)
          {
            nextPanelName = _SECONDARY_PANEL;
            _userWishesToOverwriteFile = true;
          }
          else
            nextPanelName = _MAIN_PANEL;
        }
        else
        {
          nextPanelName = _SECONDARY_PANEL;
        }
      }
      // check to see if the user is on the seconday panel
      else if(currentPanelName.equals(_SECONDARY_PANEL))
      {
        // then switch to the summary panel
        nextPanelName = _SUMMARY_PANEL;
      }
      else
      {
        // they better not be on the summary and press next!
        Assert.expect(false, " on summary screen next button should not be enabled");
      }
    }
    else
    {
      if(currentPanelName.equals(_MAIN_PANEL))
      {
        // before we switch screens from the main panel to the secondary panel lets validate the destination to
        // make sure that it doesn't not already exist.
        Assert.expect(_archiver.hasDestinationFilename());
        File destinationFile = new File(_archiver.getDestinationFilename());
        if (destinationFile.exists() && _userWishesToOverwriteFile == false)
        {
          // now prompt the user if they would like to overwrite?
          String message = StringLocalizer.keyToString(new LocalizedString("ZIP_PROJECT_FILE_EXISTS_KEY", new Object[]
              {destinationFile}));
          int value = JOptionPane.showConfirmDialog(this,
              StringUtil.format(message, 50),
              StringLocalizer.keyToString("ZIP_PROJECT_FILE_EXISTS_TITLE_KEY"),
              JOptionPane.YES_NO_OPTION,
              JOptionPane.WARNING_MESSAGE);
          if (value == JOptionPane.YES_OPTION)
          {
            nextPanelName = _SUMMARY_PANEL;
            _userWishesToOverwriteFile = true;
          }
          else
            nextPanelName = _MAIN_PANEL;
        }
        else
        {
          nextPanelName = _SUMMARY_PANEL;
        }
      }
      // check to see if the user is on the seconday panel
      else if(currentPanelName.equals(_SUMMARY_PANEL) || currentPanelName.equals(_SECONDARY_PANEL))
      {
        // they better not be on the summary and press next or be on the secondary panel.
        Assert.expect(false, " on summary screen next button should not be enabled");
      }

    }

    CardLayout layout = (CardLayout)_contentPanel.getLayout();
    layout.show(_contentPanel, nextPanelName);
    setCurrentVisiblePanel(nextPanelName);
    _currentVisiblePanel.populatePanelWithData();

  }

  /**
   * This method will navigate the wizard to the previous screen
   *
   * @author Erica Wheatcroft
   */
  protected void handlePreviousScreenButtonEvent()
  {
    Assert.expect(_currentVisiblePanel != null, "current visible panel is not set");

    String previousPanelName = null;
    String currentPanelName = _currentVisiblePanel.getWizardName();
    if(_collectDiagnosticPackage == false)
    {
      // check to see if the user is on the seconday panel
      if (currentPanelName.equals(_SECONDARY_PANEL))
      {
        // then switch to the main panel
        previousPanelName = _MAIN_PANEL;
      }
      // check to see if the user is on the summary panel
      else if (currentPanelName.equals(_SUMMARY_PANEL))
      {
        previousPanelName = _SECONDARY_PANEL;
      }
      else
      {
        // the user is on the main screen that is very bad!
        Assert.expect(false, " on summary screen next button should not be enabled");
      }
    }
    else
    {
      // check to see if the user is on the seconday panel
      if (currentPanelName.equals(_SECONDARY_PANEL))
      {
        // no secondary screen exists
        Assert.expect(false);
      }
      // check to see if the user is on the summary panel
      else if (currentPanelName.equals(_SUMMARY_PANEL))
      {
        previousPanelName = _MAIN_PANEL;
      }
      else
      {
        // the user is on the main screen that is very bad!
        Assert.expect(false, " on summary screen next button should not be enabled");
      }

    }

    CardLayout layout = (CardLayout)_contentPanel.getLayout();
    layout.show(_contentPanel, previousPanelName);
    setCurrentVisiblePanel(previousPanelName);
    _currentVisiblePanel.populatePanelWithData();
    pack();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void setCurrentVisiblePanel(String currentVisiblePanelName)
  {
    Assert.expect(currentVisiblePanelName != null, "current visible panel name is null");
    Assert.expect(_contentPanels != null, "content panels are null");

    _currentVisiblePanel = null;

    // iterate through the list and find the panel with the name sent in
    for (WizardPanel panel : _contentPanels)
      if (panel.getWizardName().equals(currentVisiblePanelName))
      {
        _currentVisiblePanel = panel;
        break;
      }

    Assert.expect(_currentVisiblePanel != null, "Current visible panel is null and was not found");

  }

  /**
   * @author Erica Wheatcroft
   */
  private void initWizard()
  {
    java.util.List<WizardPanel> panels = new ArrayList<WizardPanel>();

    // change the title
    changeTitle(_currentTitle);

    if(_collectDiagnosticPackage == false)
    {
      // create the 3 panels for the zip project wizard

      ZipProjectWizardSelectProjectAndDestinationPanel mainPanel = new ZipProjectWizardSelectProjectAndDestinationPanel(this, _archiver);
      mainPanel.setWizardName(_MAIN_PANEL); // not a user defined string no localization needed
      panels.add(mainPanel);
      // now lets populate the mainPanel
      mainPanel.populatePanelWithData();

      ZipProjectWizardSecondaryPanel secondaryPanel = new ZipProjectWizardSecondaryPanel(this, _archiver);
      secondaryPanel.setWizardName(_SECONDARY_PANEL); // not a user defined string no localization needed
      panels.add(secondaryPanel);

      ZipProjectWizardSummaryPanel summaryPanel = new ZipProjectWizardSummaryPanel(this, _archiver, _collectDiagnosticPackage);
      summaryPanel.setWizardName(_SUMMARY_PANEL); // not a user defined string no localization needed
      panels.add(summaryPanel);

    }
    else
    {
      // create the 2 panels for the zip project wizard
      CollectDiagnosticPackageWizardMainPanel mainPanel = new CollectDiagnosticPackageWizardMainPanel(this, _archiver);
      mainPanel.setWizardName(_MAIN_PANEL); // not a user defined string no localization needed
      panels.add(mainPanel);
      mainPanel.populatePanelWithData();

      ZipProjectWizardSummaryPanel summaryPanel = new ZipProjectWizardSummaryPanel(this, _archiver, _collectDiagnosticPackage);
      summaryPanel.setWizardName(_SUMMARY_PANEL); // not a user defined string no localization needed
      panels.add(summaryPanel);

    }

    // set the panels
    setContentPanels(panels);
    setCurrentVisiblePanel(_MAIN_PANEL);

    setPreferredSize(new Dimension(700, 400));
  }
}
