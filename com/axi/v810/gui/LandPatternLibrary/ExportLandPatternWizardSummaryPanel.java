package com.axi.v810.gui.LandPatternLibrary;

import java.awt.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.v810.business.panelDesc.LandPattern;
import com.axi.v810.util.*;
import java.util.ArrayList;

/**
 * @author Ying-Huan.Chu
 */
public class ExportLandPatternWizardSummaryPanel extends WizardPanel
{
  private ExportLandPatternWizardDialog _parent = null;
  private java.util.List<LandPattern> _landPatterns = null;
  private String _filePath = null;
  
  private JLabel _csvFileLabel = new JLabel();

  /**
   * @author Ying-Huan.Chu
   */
  public ExportLandPatternWizardSummaryPanel(WizardDialog wizardDialog, java.util.List<LandPattern> landPatterns, String filePath)
  {
    super(wizardDialog);

    _parent = (ExportLandPatternWizardDialog)wizardDialog;
    _landPatterns = landPatterns;
    _filePath = filePath;
  }

  /**
   * @author Ying-Huan.Chu
   */
  private void createPanel()
  {
    removeAll();

    JPanel summaryPanel = new JPanel(new BorderLayout());
    summaryPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    
    JPanel summaryTitlePanel = new JPanel(new BorderLayout());
    summaryTitlePanel.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));
    summaryTitlePanel.setBackground(Color.WHITE);
    JLabel summaryTextLabel = new JLabel(StringLocalizer.keyToString("CAD_CREATOR_IMPORT_LAND_PATTERN_SUMMARY_TITLE_KEY"));
    summaryTextLabel.setFont(FontUtil.getBoldFont(summaryTextLabel.getFont(), Localization.getLocale()));
    summaryTitlePanel.add(summaryTextLabel, BorderLayout.CENTER);
    
    JLabel landPatternToExportTextLabel = new JLabel(StringLocalizer.keyToString("CAD_CREATOR_LAND_PATTERNS_TO_EXPORT_KEY"));
    landPatternToExportTextLabel.setFont(FontUtil.getBoldFont(landPatternToExportTextLabel.getFont(), Localization.getLocale()));
    JList list = new JList();
    list.setListData(_landPatterns.toArray());
    JScrollPane landPatternsScrollPane = new JScrollPane();
    landPatternsScrollPane.setViewportView(list);
    JPanel landPatternPanel = new JPanel(new BorderLayout());
    landPatternPanel.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));
    landPatternPanel.setBackground(Color.WHITE);
    landPatternPanel.setPreferredSize(new Dimension(300,250));
    landPatternPanel.add(landPatternToExportTextLabel, BorderLayout.NORTH);
    landPatternPanel.add(landPatternsScrollPane, BorderLayout.CENTER);
    
    /*JLabel csvFileTextLabel = new JLabel("Export to: ");
    csvFileTextLabel.setFont(FontUtil.getBoldFont(csvFileTextLabel.getFont(), Localization.getLocale()));
    
    _csvFileLabel.setText(_filePath);
    JPanel csvFilePanel = new JPanel();
    csvFilePanel.setLayout(new BorderLayout());
    csvFilePanel.add(csvFileTextLabel, BorderLayout.NORTH);
    csvFilePanel.add(_csvFileLabel, BorderLayout.CENTER);*/
    
    JPanel infoPanel = new JPanel(new BorderLayout());
    infoPanel.add(landPatternPanel, BorderLayout.NORTH);
    //infoPanel.add(csvFilePanel, BorderLayout.CENTER);

    summaryPanel.add(summaryTitlePanel, BorderLayout.NORTH);
    summaryPanel.add(infoPanel, BorderLayout.CENTER);

    add(summaryPanel, BorderLayout.CENTER);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void validatePanel()
  {
    //do nothing
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void populatePanelWithData()
  {
    createPanel();
    
    _dialog.setTitle(StringLocalizer.keyToString("CAD_CREATOR_EXPORT_LAND_PATTERN_TITLE_KEY"));

    WizardButtonState buttonState = new WizardButtonState(false, true, true, true);
    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void unpopulate()
  {
    //do nothing
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void setWizardName(String name)
  {
    _name = name;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public String getWizardName()
  {
    return _name;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void removeObservers()
  {
    //do nothing
  }
}
