package com.axi.v810.gui.LandPatternLibrary;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * @author Chin Seong, Kee
 * @version 1.0
 */
public class ExportLandPatternLayoutTableModel extends DefaultSortTableModel
{
  private ExportLandPatternLayoutTable _ExportLandPatternLayoutTable = null;
  private List<LandPattern> _landPatternList = new ArrayList<LandPattern>();

  private static final String _SELECT_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_SELECT_TABLE_HEADER_KEY");
  private static final String _PACKAGE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_PACKAGE_TABLE_HEADER_KEY");
  private static final String _JOINT_TYPE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_JOINT_TYPE_TABLE_HEADER_KEY");
  private static final String _NEW_MODIFIED_IMPORTED_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_IMPORTED_TABLE_FIELD_KEY");
  private static final String _PACKAGFE_HAS_MODIFIED_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_PACKAGE_HAS_MODIFIED_TABLE_HEADER_KEY");
  private static final String _SUBTYPE_HAS_MODIFIED_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_SUBTYPE_HAS_MODIFIED_TABLE_HEADER_KEY");
  private static final String _OVERWRITE_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_OVERWRITE_TABLE_HEADER_KEY");
  private static final String _SOURCE_LIBRARY_COLUMN_HEADING = StringLocalizer.keyToString("PLWK_EXPORT_SOURCE_LIBRARY_TABLE_HEADER_KEY");

  public static final int FILTER_BY_ALL = 0;
  public static final int FILTER_BY_NEW_OR_MOPDIFIED = 1;
  public static final int FILTER_BY_IMPORTED = 2;

  private final String[] _tableColumns =
      {_SELECT_COLUMN_HEADING,
      _PACKAGE_COLUMN_HEADING};

  private List<LandPattern> _selectedLandPattern = null;

  /**
   * @param selectedPackages Set
   * @author Chin Seong, Kee
   */
  public ExportLandPatternLayoutTableModel(List<LandPattern> selectedLandPattern)
  {
    Assert.expect(selectedLandPattern != null);
    _selectedLandPattern = selectedLandPattern;
  }

  /**
   * @param ExportLandPatternLayoutTable
   * @author Chin Seong, Kee
   */
  void setExportLandPatternLayoutTable (ExportLandPatternLayoutTable ExportLandPatternLayoutTable)
  {
    Assert.expect(ExportLandPatternLayoutTable != null);
    _ExportLandPatternLayoutTable = ExportLandPatternLayoutTable;
  }

  /**
   * @param project Project
   * @author Chin Seong, Kee
   */
  void populateWithPackages(Project project)
  {
    populateWithLandPatterns(project, FILTER_BY_NEW_OR_MOPDIFIED);
  }

  /**
   * @param project Project
   * @param filterType int
   * @author Chin Seong, Kee
   */
  void populateWithLandPatterns(Project project, int filterType)
  {
     Assert.expect(project != null);

    _landPatternList.clear();

    if (filterType == FILTER_BY_ALL)
    {
      for (LandPattern landPattern : project.getPanel().getLandPatterns())
      {
        _landPatternList.add(landPattern);
      }
    }
    else if (filterType == FILTER_BY_NEW_OR_MOPDIFIED)
    {
      for (LandPattern landPattern : project.getPanel().getLandPatterns())
      {
        _landPatternList.add(landPattern);
      }
    }
    else if (filterType == FILTER_BY_IMPORTED)
    {
      //Imported need to be marked!
      for (LandPattern landPattern : project.getPanel().getLandPatterns())
      {
        _landPatternList.add(landPattern);
      }
    }
    else
      Assert.expect(false);

    fireTableDataChanged();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  void populateWithLandPatterns(List<LandPattern> landPatterns)
  {
    Assert.expect(landPatterns != null);
    
    _landPatternList.addAll(landPatterns);
    fireTableDataChanged();
  }

  /**
   * @param row int
   * @return CompPackage
   * @author Chin Seong, Kee
   */
  LandPattern getLandPatternAt(int row)
  {
    Assert.expect(row >= 0 && row < _landPatternList.size());
    Assert.expect(_landPatternList != null);

    return (LandPattern)_landPatternList.get(row);
  }

  /**
   * @param landPattern CompPackage
   * @return int
   * @author Chin Seong, Kee
   */
  int getRowForLandPattern(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);
    Assert.expect(_landPatternList != null);

    return _landPatternList.indexOf(landPattern);
  }

  /**
   * @author Chin Seong, Kee
   */
  void clear()
  {
    _landPatternList.clear();
    _selectedLandPattern.clear();
    fireTableDataChanged();
    _ExportLandPatternLayoutTable = null;
  }

  /**
   * @param column int
   * @param ascending boolean
   * @author Chin Seong, Kee
   */
  @Override
  public void sortColumn(int column, final boolean ascending)
  {
    if(_ExportLandPatternLayoutTable != null)
      _ExportLandPatternLayoutTable.stopCellsEditing();

    switch (column)
    {
      case ExportLandPatternLayoutTable._SELECT_INDEX: // selected or not
        Collections.sort(_landPatternList, new Comparator<LandPattern>()
        {
          public int compare(LandPattern lhLandPattern, LandPattern rhLandPattern)
          {
            if( _selectedLandPattern.contains(lhLandPattern) &&
                _selectedLandPattern.contains(rhLandPattern) )
              return 0;
            else if( ascending &&
                     _selectedLandPattern.contains(lhLandPattern) &&
                     _selectedLandPattern.contains(rhLandPattern) == false)
              return 1;
            else if( ascending == false &&
                     _selectedLandPattern.contains(lhLandPattern) &&
                     _selectedLandPattern.contains(rhLandPattern) == false)
              return -1;
            else if( ascending &&
                     _selectedLandPattern.contains(lhLandPattern) == false &&
                     _selectedLandPattern.contains(rhLandPattern))
              return -1;
            else if( ascending == false &&
                     _selectedLandPattern.contains(lhLandPattern) == false &&
                     _selectedLandPattern.contains(rhLandPattern))
              return 1;
            return 0;
          }
        });
        break;
    }
  }

  /**
   * @return int
   * @author Chin Seong, Kee
   */
  public int getColumnCount()
  {
    Assert.expect(_tableColumns != null);
    return _tableColumns.length;
  }

  /**
   * @param columnIndex int
   * @return String
   * @author Chin Seong, Kee
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(_tableColumns != null);
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_tableColumns[columnIndex];
  }

  /**
   * @return int
   * @author Chin Seong, Kee
   */
  public int getRowCount()
  {
    if (_landPatternList == null)
    {
      return 0;
    }
    else
    {
      return _landPatternList.size();
    }
  }

  /**
   * @param rowIndex int
   * @param columnIndex int
   * @return boolean
   * @author Chin Seong, Kee
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if (_landPatternList == null || _landPatternList.size() == 0)
    {
      return false;
    }

    switch (columnIndex)
    {
      case ExportLandPatternLayoutTable._SELECT_INDEX:
      {
        return true;
      }
    }
    return false;
  }

  /**
   * @param object Object
   * @param rowIndex int
   * @param columnIndex int
   * @author Chin Seong, Kee
   */
  @Override
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    LandPattern landPattern = _landPatternList.get(rowIndex);

    switch (columnIndex)
    {
      case ExportLandPatternLayoutTable._SELECT_INDEX:
      {
        boolean selected = (Boolean)object;
        if (selected)
        {
          if (_selectedLandPattern.contains(landPattern) == false)
          {
            _selectedLandPattern.add(landPattern);
          }
        }
        else
        {
          if (_selectedLandPattern.contains(landPattern) == true)
          {
            _selectedLandPattern.remove(landPattern);
          }
        }
        fireTableCellUpdated(rowIndex, columnIndex);
        GuiObservable.getInstance().stateChanged(this, SelectionEventEnum.EXPORT_LIBRARY_SELECTION);
        break;
      }
      default:
        Assert.expect(false,"Edit this column is not implemented");
    }
  }

  /**
   * @param rowIndex int
   * @param columnIndex int
   * @return Object
   * @author Chin Seong, Kee
   * To do List
   */
  @Override
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_landPatternList != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    LandPattern landPattern = _landPatternList.get(rowIndex);
    //get the appropriate data item that corresponds to the column
    switch (columnIndex)
    {
      case ExportLandPatternLayoutTable._SELECT_INDEX:
      {
        value = new Boolean(_selectedLandPattern.contains(landPattern));
        break;
      }
      case ExportLandPatternLayoutTable._LAND_PATTERN_INDEX:
      {
        value = landPattern.getName();
        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }
    return value;
  }

  /**
   * @return List
   * @author Chin Seong, Kee
   */
  List <LandPattern> getSelectedLandPattern()
  {
    Assert.expect(_selectedLandPattern != null);
    return new ArrayList<LandPattern>(_selectedLandPattern);
  }

  /**
   * @author Chin Seong, Kee
   */
  void selectAllLandPattern()
  {
    for (int i = 0; i < getRowCount(); i++)
    {
      setValueAt(new Boolean(true), i, ExportLandPatternLayoutTable._SELECT_INDEX);
    }
  }

  /**
   * @author Chin Seong, Kee
   */
  void unselectAllPackages()
  {
    for (int i = 0; i < getRowCount(); i++)
    {
      setValueAt(new Boolean(false), i, ExportLandPatternLayoutTable._SELECT_INDEX);
    }
  }

  /**
   * @param libraryPath String
   * @author Chin Seong, Kee
   */
  void assignEmptyLibraries(String libraryPath)
  {
    Assert.expect(libraryPath != null);

    for(LandPattern landPattern : _selectedLandPattern)
    {
      /*if(landPattern.hasLibraryPath() == false)
      {
        setValueAt(libraryPath,getRowForCompPackages(landPattern),ExportLandPatternLayoutTable._SOURCE_LIBRARY_INDEX);
      }*/
    }
  }

  /**
   * @param libraryPath String
   * @author Chin Seong, Kee
   */
  void assignAllLibraries(String libraryPath)
  {
    Assert.expect(libraryPath != null);

    for (int i = 0; i < getRowCount(); i++)
    {
        
    }
  }
}
