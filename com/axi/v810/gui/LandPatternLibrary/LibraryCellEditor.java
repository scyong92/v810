package com.axi.v810.gui.LandPatternLibrary;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;

/**
 * @author Wei Chin
 * @version 1.0
 */
public class LibraryCellEditor extends DefaultCellEditor
{
  private String _noneLibrary;
  private LibraryManager _libraryManager;

  /**
   * @param comboBox ExtendedComboBox
   *
   * @author Chin Seong, Kee
   */
  public LibraryCellEditor(ExtendedComboBox comboBox)
  {
    super(comboBox);
  }

  /**
   * @param noneLibrary String
   * @param libraryList LibraryList
   *
   * @author Chin Seong, Kee
   */
  void setEditorChoices(String noneLibrary, LibraryManager libraryList)
  {
    Assert.expect(noneLibrary != null);
    Assert.expect(libraryList != null);

    _noneLibrary = noneLibrary;
    _libraryManager = libraryList;
  }

  /**
   * @param table JTable
   * @param value Object
   * @param isSelectable boolean
   * @param row int
   * @param column int
   * @return Component
   *
   * @author Chin Seong, Kee
   */
  public java.awt.Component getTableCellEditorComponent(JTable table, Object value, boolean isSelectable, int row, int column)
  {
    java.awt.Component editor = super.getTableCellEditorComponent(table, value, isSelectable, row, column);
    ExtendedComboBox comboBox = (ExtendedComboBox)editor;
    comboBox.removeAllItems();

    comboBox.addItem(_noneLibrary);

    String selectedLibraryFullPath = null;

    if (table instanceof ExportLandPatternLayoutTable)
    {
      ExportLandPatternLayoutTable exportPackageLayoutTable = (ExportLandPatternLayoutTable)table;
      ExportLandPatternLayoutTableModel ExportLandPatternLayoutTableModel = (ExportLandPatternLayoutTableModel)exportPackageLayoutTable.getModel();
     // selectedLibraryFullPath = ExportLandPatternLayoutTableModel.getCompPackagesAt(row).getLibraryPath();
    }

    for (String libraryPath : _libraryManager.getLibraries())
    {
      if(_libraryManager.doesLibraryExist(libraryPath))
        comboBox.addItem(libraryPath);
    }

    if(selectedLibraryFullPath != null && selectedLibraryFullPath.length() > 0)
    {
      comboBox.setSelectedItem(selectedLibraryFullPath);
    }

    comboBox.setSelectedItem(value);
    comboBox.setMaximumRowCount(30);
    return comboBox;
  }
}
