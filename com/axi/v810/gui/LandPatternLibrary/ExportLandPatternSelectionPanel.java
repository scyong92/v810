package com.axi.v810.gui.LandPatternLibrary;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.v810.gui.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.util.*;
import com.axi.util.*;
import java.util.List;

/**
 *
 * @author Chin Seong, Kee
 */
public class ExportLandPatternSelectionPanel extends WizardPanel implements Observer
{
  private BorderLayout _mainPanelBorderLayout;

  private ButtonGroup _packagesButtonGroup;
  private JButton _selectAllButton;
  private JButton _unselectAllButton;
  private JButton _createNewLibraryButton;
  private JButton _assignToAllEmptyLibraryButton;
  private JButton _assignToAllLibraryButton;

  //private JRadioButton _allPackagesButton;
  //private JRadioButton _newModifiedPackagesButton;
  //private JRadioButton _importedPackageButton;

  private ExportWizardNewLandPatternDialog _newLibraryDialog = null;

  private JLabel _descLabel;
  private JLabel _packagesLabel;

  private DrawCadPanel _drawLandPatternPanel;
  private JPanel _northPanel;
  private JPanel _centerPanel;
  private JPanel _centerNorthPanel;
  private JPanel _centerSouthPanel;
  private JScrollPane _tablePanelLayoutScroll = null;
  private JSplitPane _splitPane = null;
  private JPanel _drawCadPanel;
  private TitledBorder _drawCadTitleBorder;

  private ExportLandPatternLayoutTable _landPatternLayoutTable = null;
  private ExportLandPatternLayoutTableModel _landPatternLayoutTableModel = null;

  protected String _name;

  private com.axi.v810.gui.LandPatternLibrary.ExportLandPatternWizardDialog _parent = null;
  private Project _project = null;
  private static ProjectObservable _projectObservable;
  private static GuiObservable _guiObservable;
  private List<LandPattern> _landPatternList = null;
  private LibraryManager _libraryManager;

  private boolean _isFirstTime = true;

  private boolean _allPackagesSelected = false;
  private boolean _newModifiedPackagesSelected = true;
  private boolean _importedPackageSelected = false;
  
  private boolean _isUsingLibrary = false;
  
  //private JRadioButton _useLibraryRadioButton = new JRadioButton("Library");
  //private JRadioButton _useCSVRadioButton = new JRadioButton("CSV");
  
  private ButtonGroup _chooseExportImportStyle = new ButtonGroup();

  /**
   * @param dialog WizardDialog
   * @param project Project
   *
   * @author Chin Seong, Kee
   */
  public ExportLandPatternSelectionPanel(WizardDialog dialog, Project project)
  {
    super(dialog);

    Assert.expect(project != null);
    Assert.expect(dialog != null);

    _project = project;
    _projectObservable = ProjectObservable.getInstance();
    _guiObservable = GuiObservable.getInstance();
    _isFirstTime = true;
    _allPackagesSelected = false;
    _newModifiedPackagesSelected = true;
    _importedPackageSelected = false;
    
    _parent = (ExportLandPatternWizardDialog)dialog;
  }

  /**
   * @param dialog WizardDialog
   * @param project Project
   * @param selectedPackages Set
   *
   * @author Chin Seong, Kee
   */
  public ExportLandPatternSelectionPanel(WizardDialog dialog, Project project, List<LandPattern> landPattern)
  {
     this(dialog, project);

     Assert.expect(landPattern != null);

    _parent = (ExportLandPatternWizardDialog)dialog;
    _landPatternList = landPattern;
    _isFirstTime = true;
  }

  /**
   * @author Chin Seong, Kee
   */
  private void init()
  {
    _projectObservable.addObserver(this);
    _guiObservable.addObserver(this);

     _mainPanelBorderLayout = new BorderLayout(5, 5);

    _descLabel = new JLabel(StringLocalizer.keyToString("PLWK_EXPORT_SELECT_PACKAGE_DESCRIPTION_KEY"));
    _packagesLabel = new JLabel(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY"));

    _packagesButtonGroup = new ButtonGroup();
    _selectAllButton = new JButton(StringLocalizer.keyToString("PLWK_EXPORT_SELECT_ALL_BUTTON_KEY"));
    _unselectAllButton = new JButton(StringLocalizer.keyToString("PLWK_UNSELECT_ALL_KEY"));
    _createNewLibraryButton = new JButton(StringLocalizer.keyToString("PLWK_EXPORT_CREATE_NEW_LIBRARY_BUTTON_KEY"));
    _assignToAllEmptyLibraryButton = new JButton(StringLocalizer.keyToString("PLWK_EXPORT_ASSIGN_EMPTY_LIBRARY_BUTTON_KEY"));
    _assignToAllLibraryButton = new JButton(StringLocalizer.keyToString("PLWK_EXPORT_ASSIGN_ALL_LIBRARY_BUTTON_KEY"));

    //_allPackagesButton = new JRadioButton(StringLocalizer.keyToString("PLWK_EXPORT_ALL_RADIO_BUTTON_KEY"));
    //_newModifiedPackagesButton = new JRadioButton(StringLocalizer.keyToString("PLWK_EXPORT_NEW_MODIFIED_RADIO_BUTTON_KEY"));
    //_importedPackageButton = new JRadioButton(StringLocalizer.keyToString("PLWK_EXPORT_IMPORTED_RADIO_BUTTON_KEY"));

    _drawLandPatternPanel = new DrawCadPanel(300, 400);
    _northPanel = new JPanel(new BorderLayout(5, 5));
    _centerPanel = new JPanel(new BorderLayout(5, 5));
    _centerNorthPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
    _centerSouthPanel = new JPanel();
    _tablePanelLayoutScroll = new JScrollPane();

    if(_landPatternList == null)
      _landPatternList = new ArrayList<LandPattern>();

    _libraryManager = LibraryManager.getInstance();
    _landPatternLayoutTableModel = new ExportLandPatternLayoutTableModel(_landPatternList);
    _landPatternLayoutTable = new ExportLandPatternLayoutTable(_landPatternLayoutTableModel, _libraryManager);

    _drawCadTitleBorder = BorderFactory.createTitledBorder(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY"));
    _drawCadPanel = new JPanel(new BorderLayout());
    _drawCadPanel.setBorder(_drawCadTitleBorder);

    _splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, _centerPanel, _drawCadPanel);

    _splitPane.setContinuousLayout(true);
    _splitPane.setResizeWeight(1);
  }

  /**
   * @author Chin Seong, Kee
   */
  private void createPanel()
  {
    removeAll();
    init();

    _unselectAllButton.setEnabled(false);
    _selectAllButton.setEnabled(false);

    setLayout(_mainPanelBorderLayout);
    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    _northPanel.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));
    _northPanel.setBackground(Color.WHITE);
    _northPanel.add(_descLabel, BorderLayout.CENTER);

    assignRadioButtonState();

    //_packagesButtonGroup.add(_allPackagesButton);
    //_packagesButtonGroup.add(_newModifiedPackagesButton);
   // _packagesButtonGroup.add(_importedPackageButton);

    /*JPanel packagesPanel = new JPanel();
    packagesPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
    packagesPanel.setBorder(new TitledBorder(_packagesLabel.getText()));
    packagesPanel.add(_allPackagesButton);
    packagesPanel.add(_newModifiedPackagesButton);
    packagesPanel.add(_importedPackageButton);*/
    
    //JPanel changeImportExportStylePanel = new JPanel();
    //changeImportExportStylePanel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
    //changeImportExportStylePanel.setBorder(new TitledBorder("input choice: "));
    /*changeImportExportStylePanel.add(_useCSVRadioButton);
    changeImportExportStylePanel.add(_useLibraryRadioButton);
    
    //Import Export Style
    _chooseExportImportStyle.add(_useCSVRadioButton);
    _chooseExportImportStyle.add(_useLibraryRadioButton);
    
    _useCSVRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setRadioButtonState(false);
      }
    });
    
    _useLibraryRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setRadioButtonState(true);
      }
    });*/
     
    //_centerNorthPanel.add(packagesPanel);
    //_centerNorthPanel.add(changeImportExportStylePanel);

    _centerSouthPanel.setLayout(new BoxLayout(_centerSouthPanel, BoxLayout.LINE_AXIS));
    _centerSouthPanel.add(_selectAllButton);
    _centerSouthPanel.add(Box.createRigidArea(new Dimension(5, 0)));
    _centerSouthPanel.add(_unselectAllButton);
    _centerSouthPanel.add(Box.createHorizontalGlue());
    _centerSouthPanel.add(_assignToAllEmptyLibraryButton);
    _centerSouthPanel.add(Box.createRigidArea(new Dimension(5, 0)));
    _centerSouthPanel.add(_assignToAllLibraryButton);
    _centerSouthPanel.add(Box.createRigidArea(new Dimension(5, 0)));
    _centerSouthPanel.add(_createNewLibraryButton);

    _landPatternLayoutTable.setModel(_landPatternLayoutTableModel);
    _landPatternLayoutTable.setEditorsAndRenderers();
    _landPatternLayoutTable.setPreferredColumnWidths();
    _landPatternLayoutTableModel.setExportLandPatternLayoutTable(_landPatternLayoutTable);
    populateTableWithFilterData();

    if(_isFirstTime)
    {
      _landPatternLayoutTableModel.selectAllLandPattern();
      _isFirstTime = false;
    }
    
    _createNewLibraryButton.setEnabled(_isUsingLibrary);
    _assignToAllEmptyLibraryButton.setEnabled(_isUsingLibrary);
    _assignToAllLibraryButton.setEnabled(_isUsingLibrary);
    
    _tablePanelLayoutScroll.getViewport().add(_landPatternLayoutTable, null);
    _tablePanelLayoutScroll.getViewport().setOpaque(false);

    _centerPanel.add(_centerNorthPanel, BorderLayout.NORTH);
    _centerPanel.add(_centerSouthPanel, BorderLayout.SOUTH);
    _centerPanel.add(_tablePanelLayoutScroll, BorderLayout.CENTER);
    _drawCadPanel.add(_drawLandPatternPanel, BorderLayout.CENTER);
//    _centerPanel.add(_drawLandPatternPanel, BorderLayout.EAST);
    add(_northPanel, BorderLayout.NORTH);
    add(_splitPane, BorderLayout.CENTER);

    ListSelectionModel rowSM = _landPatternLayoutTable.getSelectionModel();
    rowSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        TableColumnModel tableColumnModel = _landPatternLayoutTable.getColumnModel();

        ListSelectionModel lsm = (ListSelectionModel)e.getSource();

        if (lsm.getValueIsAdjusting() == false)
        {
          // we want to select the pad(s) in the graphics window corresponding to the newly selected table row(s)
          int selectedRows = _landPatternLayoutTable.getSelectedRow();
          if (selectedRows >= 0)
          {
            LandPattern landpattern = _landPatternLayoutTableModel.getLandPatternAt(selectedRows);
            _drawLandPatternPanel.drawLandPattern(landpattern);
            _drawLandPatternPanel.scaleDrawing();
            //_drawCadTitleBorder.setTitle(StringLocalizer.keyToString("PLWK_EXPORT_PACKAGES_KEY") + compPackage.getShortName());
          }
          revalidate();
          repaint();
        }
      }
    });

    _landPatternLayoutTable.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        multiEditTableMouseReleased(e, _landPatternLayoutTable);
      }
    });

    _selectAllButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectAllButton_actionPerformed();
      }
    });

    _unselectAllButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        unselectAllButton_actionPerformed();
      }
    });

    _assignToAllEmptyLibraryButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        assignToAllEmptyLibraryButton_actionPerformed(e);
      }
    });

    _assignToAllLibraryButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        assignToAllLibraryButton_actionPerformed();
      }
    });

    _createNewLibraryButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        createNewLibraryButton_actionPerformed();
      }
    });

    /*_allPackagesButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        packagesButtonGroup_itemStateChanged(e);
      }
    });

    _newModifiedPackagesButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        packagesButtonGroup_itemStateChanged(e);
      }
    });

    _importedPackageButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        packagesButtonGroup_itemStateChanged(e);
      }
    });*/

    _unselectAllButton.setEnabled(true);
    _selectAllButton.setEnabled(true);
  }

  /*
   * @author Kee Chin Seong
   */
  /*public boolean isUsingLibraryAsChoice()
  {
     return _useLibraryRadioButton.isSelected();
  }*/
  
  /**
   * @author Chin Seong, Kee
   */
  public void validatePanel()
  {
    boolean canProceedToNextButton = true;

    Assert.expect(_landPatternLayoutTableModel != null );

    if(_landPatternLayoutTableModel.getSelectedLandPattern().size() == 0)
    {
      canProceedToNextButton = false;
    }

    WizardButtonState buttonState = null;
    
    _parent.setLandPatternsToBeExported(_landPatternLayoutTableModel.getSelectedLandPattern());
    if (_landPatternLayoutTableModel.getSelectedLandPattern().size() == 0)
    {
      buttonState = new WizardButtonState(false, true, false, false);
    }
    else
    {
      buttonState = new WizardButtonState(true, true, false, false);
    }
    // next = enabled , cancel = enabled , finish = disable , back = enabled
    _observable.updateButtonState(buttonState);
  }

  /*
   * @author Kee Chin Seong
   * True - Library
   * False - CSV
   */
  public void setUserExportImportOption(boolean isUsingLibrary)
  {
    _isUsingLibrary = isUsingLibrary;
  }
  
  /**
   * @author Chin Seong, Kee
   */
  public void populatePanelWithData()
  {
    createPanel();

    // set the title
    _dialog.changeTitle(StringLocalizer.keyToString("PLWK_EXPORT_SELECT_PACKAGE_DIALOG_TITLE_KEY"));

    validatePanel();
  }

  /**
   * @author Chin Seong, Kee
   */
  public void unpopulate()
  {
    if (_drawLandPatternPanel != null)
    {
      _drawLandPatternPanel.resetPackageGraphics();
      _drawLandPatternPanel.dispose();
    }

    if(_landPatternLayoutTableModel != null)
      _landPatternLayoutTableModel.clear();

    _landPatternLayoutTableModel = null;

    if(_landPatternLayoutTable != null)
      _landPatternLayoutTable.removeAll();

    _mainPanelBorderLayout = null;

    _packagesButtonGroup = null;
    _selectAllButton = null;
    _unselectAllButton = null;
    _createNewLibraryButton = null;
    _assignToAllEmptyLibraryButton = null;
    _assignToAllLibraryButton = null;

    /*_allPackagesButton = null;
    _newModifiedPackagesButton = null;
    _importedPackageButton = null;*/

    _newLibraryDialog = null;

    _descLabel = null;
    _packagesLabel = null;

    _drawLandPatternPanel = null;
    _northPanel = null;
    _centerPanel = null;
    _centerNorthPanel = null;
    _centerSouthPanel = null;
    _tablePanelLayoutScroll = null;

    _landPatternLayoutTable = null;
    _landPatternLayoutTableModel = null;

    _name = null;
    _project = null;
    _landPatternList = null;
    _libraryManager = null;
  }

  /**
   * Set the Panel Name
   * @param name String
   *
   * @author Chin Seong, Kee
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null, "Name of panel is null");
    Assert.expect(name.length() > 0, "Name of panel is an empty string");
    _name = name;
  }

  /**
   * get The Panel Name
   * @return String
   *
   * @author Chin Seong, Kee
   */
  public String getWizardName()
  {
    Assert.expect(_name != null, "Name of panel is not set");
    return _name;
  }

  /**
   * Get a list of selected (checked) Packages
   * @return List
   *
   * @author Chin Seong, Kee
   */
  public java.util.List<LandPattern> getSelectedLandPattern()
  {
    Assert.expect(_landPatternLayoutTableModel != null);

    return _landPatternLayoutTableModel.getSelectedLandPattern();
  }

  /**
   * @author Chin Seong, Kee
   */
  public void removeObservers()
  {
    _projectObservable.deleteObserver(this);
    _guiObservable.deleteObserver(this);
  }
  
  /*
   * @author Kee Chin Seong
   */
/* void setRadioButtonState(boolean isUsingLibrary)
  {
   _allPackagesButton.setEnabled(isUsingLibrary);
    _newModifiedPackagesButton.setEnabled(isUsingLibrary);
    _importedPackageButton.setEnabled(isUsingLibrary);
  }*/

  /**
   * @author Chin Seong, Kee
   */
  void selectAllButton_actionPerformed()
  {
    Assert.expect(_landPatternLayoutTableModel != null);

    _landPatternLayoutTableModel.selectAllLandPattern();
  }

  /**
   * @author Chin Seong, Kee
   */
  void unselectAllButton_actionPerformed()
  {
    Assert.expect(_landPatternLayoutTableModel != null);

    _landPatternLayoutTableModel.unselectAllPackages();
  }

  /**
   * @author Chin Seong, Kee
   */
  private void createNewLibraryButton_actionPerformed()
  {
    Assert.expect(_landPatternLayoutTable != null);
    Assert.expect(_landPatternLayoutTableModel != null);

    java.util.List <LandPattern> selectedLandPattern = new ArrayList<LandPattern>();
    int [] selectedRows = _landPatternLayoutTable.getSelectedRows();
    for(int selectedRow : selectedRows)
    {
      if (selectedRows.length >= 0)
        selectedLandPattern.add(_landPatternLayoutTableModel.getLandPatternAt(selectedRow));
    }
    _newLibraryDialog = new ExportWizardNewLandPatternDialog((Frame)_dialog.getParent(), true, selectedLandPattern, _project.getPanel().getAllLandPatterns(), this);
    _newLibraryDialog.pack();
    _newLibraryDialog.populateDialogWithData();
    SwingUtils.centerOnComponent(_newLibraryDialog, _dialog);
    _newLibraryDialog.setVisible(true);
    _newLibraryDialog = null;

    selectedRows = null;
    if(selectedLandPattern != null)
    {
      selectedLandPattern.clear();
      selectedLandPattern = null;
    }
  }

  /**
   * @author Chin Seong, Kee
   */
  /*private void packagesButtonGroup_itemStateChanged(ItemEvent e)
  {
    saveRadioButtonState((JRadioButton)e.getSource());
    populateTableWithFilterData();
  }*/

  /**
   * @author Chin Seong, Kee
   */
  public void populateTableWithFilterData()
  {
    /*Assert.expect(_allPackagesButton != null);
    Assert.expect(_newModifiedPackagesButton != null);
    Assert.expect(_importedPackageButton != null);*/
    Assert.expect(_landPatternLayoutTable != null);
    Assert.expect(_landPatternLayoutTableModel != null);

    /*if(_allPackagesButton.isSelected())
      _landPatternLayoutTableModel.populateWithLandPatterns(_project,_landPatternLayoutTableModel.FILTER_BY_ALL);
    else if(_newModifiedPackagesButton.isSelected())
      _landPatternLayoutTableModel.populateWithLandPatterns(_project,_landPatternLayoutTableModel.FILTER_BY_NEW_OR_MOPDIFIED);
    else if(_importedPackageButton.isSelected())
      _landPatternLayoutTableModel.populateWithLandPatterns(_project, _landPatternLayoutTableModel.FILTER_BY_IMPORTED);*/
    _landPatternLayoutTableModel.populateWithLandPatterns(_project,_landPatternLayoutTableModel.FILTER_BY_ALL);

    if(_landPatternLayoutTable.getSortedColumnIndex() > 0)
      _landPatternLayoutTableModel.sortColumn(_landPatternLayoutTable.getSortedColumnIndex(), _landPatternLayoutTable.isSortedColumnAscending());
    else
      _landPatternLayoutTableModel.sortColumn(ExportLandPatternLayoutTable._LAND_PATTERN_INDEX, true);
  }

  /**
   * This method implements the right click edit when multiple lines of a table are selected
   *
   * @param e MouseEvent
   * @param table JTable
   *
   * @author Chin Seong, Kee
   */
  private void multiEditTableMouseReleased(MouseEvent e, JTable table)
  {
    Assert.expect(e != null);
    Assert.expect(table != null);

    // ignore any other mouse events than a popup trigger -- we don't want to break the normal L&F
    if (e.isPopupTrigger())
    {
      // get the table cell that the mouse was right-clicked in
      Point p = e.getPoint();
      int row = table.rowAtPoint(p);
      int col = table.columnAtPoint(p);

      // if this cell is not currently selected, exit.  It's confusing to edit a cell that not selected.
      // we could "select" this cell too, but for now, we'll just not.
      if (table.isCellSelected(row, col) == false)
        return;

      // start the editing process for this cell
      table.editCellAt(row, col);
      // if this is a combo box cell editor, the editCellAt doesn't show the combo box popup, so let's show that
      // only if this is a combo box editor
      Object obj = table.getEditorComponent();
      if (obj instanceof JComboBox)
      {
        JComboBox comboBox = (JComboBox)obj;
        comboBox.showPopup();
      }
      else if(obj instanceof JCheckBox)
      {
        JCheckBox checkBox = (JCheckBox)obj;
        checkBox.doClick();
      }
      validatePanel();
    }
  }

  /**
   * @param actionEvent ActionEvent
   * @author Chin Seong, Kee
   */
  public void assignToAllEmptyLibraryButton_actionPerformed(ActionEvent actionEvent)
  {
    Assert.expect(_landPatternLayoutTable != null);
    Assert.expect(_landPatternLayoutTableModel != null);

    int selectedRow = _landPatternLayoutTable.getSelectedRow();
    if(selectedRow >= 0)
    {
      //_landPatternLayoutTableModel.assignEmptyLibraries(
       //   _landPatternLayoutTableModel.getLandPatternAt(selectedRow).getLibraryPath());
    }
  }

  /**
   * @author Chin Seong, Kee
   */
  public void assignToAllLibraryButton_actionPerformed()
  {
    Assert.expect(_landPatternLayoutTable != null);
    Assert.expect(_landPatternLayoutTableModel != null);

    int selectedRow = _landPatternLayoutTable.getSelectedRow();
    if (selectedRow >= 0)
    {
      //_landPatternLayoutTableModel.assignAllLibraries(
      //    _landPatternLayoutTableModel.getCompPackagesAt(selectedRow).getLibraryPath());
    }
  }

  /**
   * @param radioButton JRadioButton
   * @author Chin Seong, Kee
   */
  /*private void saveRadioButtonState(JRadioButton radioButton)
  {
    if(radioButton == _allPackagesButton)
    {
      _allPackagesSelected = true;
      _newModifiedPackagesSelected = false;
      _importedPackageSelected = false;
    }
    else if(radioButton == _newModifiedPackagesButton)
    {
      _allPackagesSelected = false;
      _newModifiedPackagesSelected = true;
      _importedPackageSelected = false;
    }
    else if(radioButton == _importedPackageButton)
    {
      _allPackagesSelected = false;
      _newModifiedPackagesSelected = false;
      _importedPackageSelected = true;
    }
  }*/

  /**
   * @author Chin Seong, Kee
   */
  private void assignRadioButtonState()
  {
    validateRadioButtonState();
    /*_allPackagesButton.setSelected(_allPackagesSelected);
    _newModifiedPackagesButton.setSelected(_newModifiedPackagesSelected);
    _importedPackageButton.setSelected(_importedPackageSelected);*/
  }

  /**
   * @author Chin Seong, Kee
   */
  private void validateRadioButtonState()
  {
    if(_allPackagesSelected)
    {
      Assert.expect(_newModifiedPackagesSelected == false &&
                    _importedPackageSelected == false);
    }
    else if(_newModifiedPackagesSelected)
    {
      Assert.expect(_allPackagesSelected == false &&
                    _importedPackageSelected == false);
    }
    else if(_importedPackageSelected)
    {
      Assert.expect(_newModifiedPackagesSelected == false &&
                    _allPackagesSelected == false);
    }
  }

  /**
   * @param observable Observable
   * @param object Object
   * @author Chin Seong, Kee
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof GuiObservable)
        {
          GuiEvent guiEvent = (GuiEvent)object;
          GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
          if (guiEventEnum instanceof SelectionEventEnum)
          {
            SelectionEventEnum selectionEventEnum = (SelectionEventEnum)guiEventEnum;
            if (selectionEventEnum.equals(SelectionEventEnum.EXPORT_LIBRARY_SELECTION))
            {
              validatePanel();
            }
          }
        }
      }
    });
  }
}
