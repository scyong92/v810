package com.axi.v810.gui.LandPatternLibrary;


import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

/**
 * @author Ying-Huan.Chu
 */
public class ExportLandPatternChooseCsvFileScreenPanel extends WizardPanel
{
  private Project _project = null;
  private ExportLandPatternWizardDialog _parent = null;
  private LandPattern _selectedLandPattern = null;
  
  private JTextField _selectedFilePathTextField = null;
  private JButton _browseButton = null;
  
  /**
   * @author Ying-Huan.Chu
   */
  public ExportLandPatternChooseCsvFileScreenPanel(WizardDialog wizardDialog, Project project)
  {
    super(wizardDialog);

    _parent = (ExportLandPatternWizardDialog)wizardDialog;
    _project = project;
  }

  /**
   * @author Ying-Huan.Chu
   */
  private void createPanel()
  {
    removeAll();

    JPanel mainPanel = new JPanel();
    mainPanel.setLayout(new BorderLayout());
    mainPanel.setBorder(BorderFactory.createEmptyBorder(100, 10, 100, 10));

    JPanel instructionPanel = new JPanel(new BorderLayout());
    instructionPanel.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));
    instructionPanel.setBackground(Color.WHITE);
    JLabel instructionTextLabel = new JLabel("Please select a file to export land pattern:");
    instructionTextLabel.setFont(FontUtil.getBoldFont(instructionTextLabel.getFont(), Localization.getLocale()));
    instructionPanel.add(instructionTextLabel, BorderLayout.CENTER);
    
    _selectedFilePathTextField = new JTextField();
    _selectedFilePathTextField.setEditable(false);
    
    _browseButton = new JButton("Browse");
    _browseButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        browseButton_actionPerformed();
      }
    });
    
    JPanel browseFilePanel = new JPanel();
    browseFilePanel.setLayout(new BoxLayout(browseFilePanel, BoxLayout.X_AXIS));
    browseFilePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    browseFilePanel.add(_selectedFilePathTextField);
    browseFilePanel.add(_browseButton);

    mainPanel.add(instructionPanel, BorderLayout.NORTH);
    mainPanel.add(browseFilePanel, BorderLayout.CENTER);

    add(mainPanel, BorderLayout.CENTER);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void browseButton_actionPerformed()
  {
    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    FileFilterUtil csvFileFilter = new FileFilterUtil(FileName.getCommaSeparatedValueFileExtension(), StringLocalizer.keyToString("RDGUI_GENERATE_REPORT_FILE_DESCRIPTION_KEY"));
    fileChooser.setFileFilter(csvFileFilter);
    if (fileChooser.showSaveDialog(this) != JFileChooser.APPROVE_OPTION)
    {
      return;
    }
    File selectedFile = fileChooser.getSelectedFile();
    String selectedFileName = selectedFile.getPath();
    String seelctedFileExtension = FileUtilAxi.getExtension(selectedFileName);
    if (seelctedFileExtension.equalsIgnoreCase(FileName.getCommaSeparatedValueFileExtension()) == false)
    {
      selectedFileName += FileName.getCommaSeparatedValueFileExtension();
    }
    _selectedFilePathTextField.setText(selectedFileName);
    _parent.setFilePathToExport(_selectedFilePathTextField.getText());
    validatePanel();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void validatePanel()
  {
    WizardButtonState buttonState = null;

    if (_selectedFilePathTextField.getText().isEmpty())
    {
      buttonState = new WizardButtonState(false, true, false, true);
    }
    else
    {
      buttonState = new WizardButtonState(true, true, false, true);
    }
    
    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void populatePanelWithData()
  {
    createPanel();

    _dialog.setTitle(StringLocalizer.keyToString("CAD_CREATOR_EXPORT_LAND_PATTERN_TITLE_KEY"));

    validatePanel();
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void unpopulate()
  {
    //
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void setWizardName(String name)
  {
    _name = name;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public String getWizardName()
  {
    return _name;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void removeObservers()
  {
    //do nothing
  }
}
