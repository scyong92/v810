package com.axi.v810.gui.LandPatternLibrary;

import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * Import Land Pattern Wizard for CAD Creator
 * This class allows user to import a LandPattern from an external .CSV file or library.
 * 
 * @author Ying-Huan.Chu
 */
public class ImportLandPatternWizardDialog extends WizardDialog
{
  private final String _csvDelimiter = StringLocalizer.keyToString("TABLE_MODEL_CSV_REPORT_DELIMITER_KEY");
  private final static String _WELCOME_PANEL = "importLandPatternWelcomePanel";
  private final static String _CHOOSE_CSV_FILE_PANEL = "importLandPatternChooseCSVFilePanel";
  private final static String _CHOOSE_LANDPATTERN_PANEL = "importLandPatternChooseLandPatternPanel";
  private final static String _SUMMARY_PANEL = "importLandPatternSummaryPanel";
  private final String _SURFACE_MOUNT_PAD = "SM";
  private final String _THROUGH_HOLE_PAD = "TH";
  private final String[] _landPatternCsvFileColumns = {"LandPattern", 
                                                       "Pad", 
                                                       "X Location", 
                                                       "Y Location", 
                                                       "X Dimension", 
                                                       "Y Dimension", 
                                                       "Rotation", 
                                                       "Shape", 
                                                       _SURFACE_MOUNT_PAD + "/" + _THROUGH_HOLE_PAD, 
                                                       "Hole Width", //Siew Yeng - XCR-3318 - Oval PTH
                                                       "Hole Length"};
  private Dimension _importLandPatternDimension = new Dimension(575, 400);
  
  private Frame _parent = null;
  private WizardPanel _currentVisiblePanel = null;
  private Project _project = null;
  private java.util.List<LandPattern> _landPatternsToBeImported = new ArrayList<>();
  private String _filePathToImport = null;
  
  private SearchFilterPersistance _searchFilterPersistance;
  
  private boolean _isFileReadSuccess = false;
  
  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  
  /**
   * @author Ying-Huan.Chu
   */
  public ImportLandPatternWizardDialog(Frame parent, boolean modal, Project project)
  {
    super(parent,
          modal,
          StringLocalizer.keyToString("WIZARD_NEXT_KEY"),
          StringLocalizer.keyToString("WIZARD_CANCEL_KEY"),
          StringLocalizer.keyToString("WIZARD_FINISH_KEY"),
          StringLocalizer.keyToString("WIZARD_BACK_KEY") );
    
    _parent = parent;
    _project = project;
    _searchFilterPersistance = new SearchFilterPersistance();
    _searchFilterPersistance = _searchFilterPersistance.readSettings();

    initWizard();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void initWizard()
  {
    java.util.List<WizardPanel> panels = new ArrayList<>();

    ImportExportLandPatternWelcomeScreenPanel importLandPatternWelcomePanel = new ImportExportLandPatternWelcomeScreenPanel(this, true);
    importLandPatternWelcomePanel.setWizardName(_WELCOME_PANEL);
    panels.add(importLandPatternWelcomePanel);
    importLandPatternWelcomePanel.populatePanelWithData();

    ImportLandPatternChooseCsvFileScreenPanel chooseCsvFilePanel = new ImportLandPatternChooseCsvFileScreenPanel(this, _project);
    chooseCsvFilePanel.setWizardName(_CHOOSE_CSV_FILE_PANEL);
    panels.add(chooseCsvFilePanel);
    chooseCsvFilePanel.populatePanelWithData();
    
    ImportLandPatternSelectionPanel chooseLandPatternPanel = new ImportLandPatternSelectionPanel(this, _project, _landPatternsToBeImported);
    chooseLandPatternPanel.setWizardName(_CHOOSE_LANDPATTERN_PANEL);
    panels.add(chooseLandPatternPanel);
    chooseLandPatternPanel.populatePanelWithData();
    
    ImportLandPatternWizardSummaryPanel summaryPanel = new ImportLandPatternWizardSummaryPanel(this, _landPatternsToBeImported, _filePathToImport);
    summaryPanel.setWizardName(_SUMMARY_PANEL);
    panels.add(summaryPanel);
    summaryPanel.populatePanelWithData();
    
    // set the panels
    setContentPanels(panels);
    setCurrentVisiblePanel(_WELCOME_PANEL);

    setPreferredSize(_importLandPatternDimension);
    pack();
    
    WizardButtonState buttonState = new WizardButtonState(true, true, false, false);
    WizardPanelObservable.getInstance().updateButtonState(buttonState);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void setCurrentVisiblePanel(String currentVisiblePanelName)
  {
    Assert.expect(currentVisiblePanelName != null, "current visible panel name is null");
    Assert.expect(_contentPanels != null, "content panels are null");

    _currentVisiblePanel = null;

    // iterate through the list and find the panel with the name sent in
    for(WizardPanel panel : _contentPanels)
    {
      if(panel.getWizardName().equals(currentVisiblePanelName))
      {
        _currentVisiblePanel = panel;
        break;
      }
    }

    Assert.expect(_currentVisiblePanel != null, "Current visible panel is null and was not found");
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  protected void handleNextScreenButtonEvent()
  {
    // lets change the buttons state to all disabled so that the user does not think the buttons are active when they are not.
    WizardButtonState buttonState = new WizardButtonState(false, false, false, false);
    WizardPanelObservable.getInstance().updateButtonState(buttonState);
    Dimension dimension = _importLandPatternDimension;
    
    String nextPanelName = null;
    String currentPanelName = _currentVisiblePanel.getWizardName();

    if(currentPanelName.equals(_WELCOME_PANEL))
    {
      nextPanelName = _CHOOSE_CSV_FILE_PANEL;
      _importLandPatternDimension = new Dimension(575, 400);
    }
    else if(currentPanelName.equals(_CHOOSE_CSV_FILE_PANEL))
    {
      readCSVFile(_filePathToImport);
      
      if(_isFileReadSuccess == true)
      {
        nextPanelName = _CHOOSE_LANDPATTERN_PANEL;
        _importLandPatternDimension = new Dimension(1080, 520);
      }
      else
      {
        nextPanelName = _CHOOSE_CSV_FILE_PANEL;
        _importLandPatternDimension = new Dimension(575, 400);
      }
    }
    else if (currentPanelName.equals(_CHOOSE_LANDPATTERN_PANEL))
    {
      nextPanelName = _SUMMARY_PANEL;
      _importLandPatternDimension = new Dimension(575, 400);
    }
    else
    {
      // they better not be on the summary and press next!
      Assert.expect(false, " on summary screen next button should not be enabled");
    }
    
    CardLayout layout = (CardLayout)_contentPanel.getLayout();
    layout.show(_contentPanel, nextPanelName);

    setCurrentVisiblePanel(nextPanelName);
    _currentVisiblePanel.populatePanelWithData();
    _currentVisiblePanel.revalidate();

    setPreferredSize(_importLandPatternDimension);
    pack();

    SwingUtils.centerOnComponent(this, _parent);
  }

  /**
   * @author Ying-Huan.Chu
   */
  protected void handlePreviousScreenButtonEvent()
  {
    Assert.expect(_currentVisiblePanel != null, "current visible panel is not set");

    String previousPanelName = null;
    String currentPanelName = _currentVisiblePanel.getWizardName();

    if(currentPanelName.equals(_CHOOSE_CSV_FILE_PANEL))
    {
      previousPanelName = _WELCOME_PANEL;
      _importLandPatternDimension = new Dimension(575, 400);
    }
    else if(currentPanelName.equals(_CHOOSE_LANDPATTERN_PANEL))
    {
      previousPanelName = _CHOOSE_CSV_FILE_PANEL;
      _importLandPatternDimension = new Dimension(575, 400);
    }
    else if (currentPanelName.equals(_SUMMARY_PANEL))
    {
      previousPanelName = _CHOOSE_LANDPATTERN_PANEL;
      _importLandPatternDimension = new Dimension(1080, 520);
    }
    else
    {
      Assert.expect(false);
    }

    CardLayout layout = (CardLayout)_contentPanel.getLayout();
    layout.show(_contentPanel, previousPanelName);
    setCurrentVisiblePanel(previousPanelName);
    _currentVisiblePanel.populatePanelWithData();
    _currentVisiblePanel.revalidate();

    setPreferredSize(_importLandPatternDimension);
    pack();

    SwingUtils.centerOnComponent(this, _parent);
  }
  
  /**
   * @author Ying-Huan.Chu
   * @author Khaw Chek Hau
   * @XCR3472: Assert when switch tab after delete all the land pattern
   */
  protected void handleCompleteProcessButtonEvent()
  {
    final BusyCancelDialog busyDialog = new BusyCancelDialog(this,
                                                             StringLocalizer.keyToString("CAD_CREATOR_IMPORT_LAND_PATTERN_BUSYCANCELDIALOG_MSG_KEY"),
                                                             StringLocalizer.keyToString("CAD_CREATOR_IMPORT_LAND_PATTERN_BUSYCANCELDIALOG_TITLE_KEY"),
                                                             true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, this);
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        _projectObservable.setEnabled(false);
        try
        {
          //Khaw Chek Hau - XCR3472: Assert when switch tab after delete all the land pattern
          java.util.List<ComponentType> componentTypeList = new ArrayList<ComponentType>();
          java.util.List<JointTypeEnum> jointTypeEnums = new ArrayList<JointTypeEnum>();
          java.util.List<String> importedLandPatternName = new ArrayList<String>();
          Map<String, java.util.List<Subtype>> origCompRefDesAndSubtypesMap = new LinkedHashMap<>();
          Map<String,String> componentTypeLongNameToSubtypeLongNameMap = new HashMap<String,String>();
          Map<String,String> componentTypeShortNameToSubtypeShortNameMap = new HashMap<String,String>();
          Map<CompPackage,CompPackage> oldCompPackageLongNametoNewCompPackageLongNameMap = new HashMap<CompPackage,CompPackage>();
          
          // Add all the imported LandPatterns to the project.
          for (LandPattern landPattern : _landPatternsToBeImported)
          {    
            CompPackage compPackage = null;

            if(_project.getPanel().doesLandPatternExist(landPattern.getName()))
            {
              if (_project.getPanel().getLandPattern(landPattern.getName()).getComponentTypes().isEmpty())
              {                
                landPattern.add();
                importedLandPatternName.add(landPattern.getName());                
                continue;
              }
              else
              {
                //Remove alignment pad if replaced land pattern's componentType consists alignment pad
                for (ComponentType componentType : _project.getPanel().getLandPattern(landPattern.getName()).getComponentTypes())
                {
                  componentTypeList.add(componentType);
                  origCompRefDesAndSubtypesMap.put(componentType.getReferenceDesignator(), componentType.getSubtypes());

                  for(Pad pad: _project.getPanel().getPanelSettings().getAllAlignmentPads())
                  {
                    if (pad.getPadType().getComponentType().equals(componentType))
                      _project.getPanel().getPanelSettings().getAlignmentGroupFromPad(pad).removePad(pad);
                  }
                }

                for (ComponentType componentType : componentTypeList)
                {                
                  jointTypeEnums.clear();
                  String componentTypeRefDesignator = componentType.getReferenceDesignator();

                  for(PackagePin packagePin : componentType.getCompPackage().getPackagePins())
                  {
                    jointTypeEnums.add(packagePin.getJointTypeEnum());
                  }

                  for (PadType padType : componentType.getPadTypes())
                  {
                    String componentTypeLongName = componentTypeRefDesignator + "_" + padType.getName();
                    componentTypeLongNameToSubtypeLongNameMap.put(componentTypeLongName, padType.getSubtype().getLongName());
                    componentTypeShortNameToSubtypeShortNameMap.put(componentTypeRefDesignator, padType.getSubtype().getShortName());
                  }

                  JointTypeEnum compPackageJointTypeEnum = jointTypeEnums.get(0);
                  String compPackageShortName = landPattern.getName();

                  if (importedLandPatternName.contains(landPattern.getName()) == false)
                  {
                    landPattern.add();  
                    importedLandPatternName.add(landPattern.getName());
                  }

                  boolean createPackagePin = true;

                  if (oldCompPackageLongNametoNewCompPackageLongNameMap.containsKey(componentType.getCompPackage()) == false)
                  {
                    String newCompPackageLongName = _project.getPanel().getUniqueCompPackageLongName(landPattern, compPackageJointTypeEnum);
                    compPackage = new CompPackage();
                    compPackage.setPanel(_project.getPanel());
                    compPackage.setLongName(newCompPackageLongName);
                    compPackage.setShortName(compPackageShortName);
                    compPackage.setLandPattern(landPattern); 
                    oldCompPackageLongNametoNewCompPackageLongNameMap.put(componentType.getCompPackage(), compPackage);

                    createPackagePin = true;
                  }
                  else
                  {
                    compPackage = oldCompPackageLongNametoNewCompPackageLongNameMap.get(componentType.getCompPackage());
                    
                    createPackagePin = false;
                  }

                  // Remove existing PadType from ComponentType
                  removePadTypeFromComponentType(componentType);

                  processPackagePinAndPadType(landPattern, componentType, compPackage, jointTypeEnums, createPackagePin);

                  // Remove ComponentType from old CompPackage
                  CompPackage oldCompPackage = componentType.getCompPackage();
                  oldCompPackage.removeComponentType(componentType); 

                  //setting back landPattern and compPackager
                  componentType.setLandPattern(landPattern);
                  componentType.setCompPackage(compPackage);

                  landPattern.addComponentType(componentType);
                  compPackage.addComponentType(componentType);

                  removeUnusedCompPackage(_project.getPanel(), oldCompPackage);

                  java.util.List<Subtype> origSubtypes = new ArrayList<Subtype>();
                  java.util.List<Subtype> processedSubtype = new ArrayList<Subtype>();
                  
                  for (PadType padType : componentType.getPadTypes()) 
                  {                  
                    String componentTypeLongName = componentTypeRefDesignator + "_" + padType.getName();
                    String subtypeLongName = componentTypeLongNameToSubtypeLongNameMap.get(componentTypeLongName);

                    if (subtypeLongName == null)
                    {
                      subtypeLongName = componentTypeShortNameToSubtypeShortNameMap.get(componentTypeRefDesignator) + "_" + padType.getJointTypeEnum().getNameWithoutSpaces();
                    }
                    
                    if (_project.getPanel().doesSubtypeExist(subtypeLongName))
                    {
                      Subtype newSubtype = _project.getPanel().getSubtype(subtypeLongName);
                      newSubtype.setShortName(_project.getPanel().getSubtype(subtypeLongName).getShortName());

                      componentType.getPadType(padType.getName()).getPackagePin().setJointTypeEnum(newSubtype.getJointTypeEnum());
                      componentType.getPadType(padType.getName()).getPadTypeSettings().setSubtype(newSubtype);

                      // Make sure current pad type is assigned to this subtype
                      if (newSubtype.isPadTypeExist(componentType.getPadType(padType.getName())) == false)
                        newSubtype.assignedToPadType(componentType.getPadType(padType.getName()));
                      
                      String refDes = componentType.getReferenceDesignator();
                      
                      if (origCompRefDesAndSubtypesMap.containsKey(refDes))
                        origSubtypes = origCompRefDesAndSubtypesMap.get(refDes);
                      
                      if (origSubtypes.isEmpty() == false && processedSubtype.contains(newSubtype) == false)
                      {
                        for (Subtype origSubtype : origSubtypes)
                        {
                          if (origSubtype.getLongName().equals(newSubtype.getLongName()))
                          {
                            //To copy the settings to new subtype
                            if(origSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false)
                              newSubtype.getSubtypeAdvanceSettings().setArtifactCompensationState(origSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState());

                            newSubtype.getSubtypeAdvanceSettings().setCanUseVariableMagnification(origSubtype.getSubtypeAdvanceSettings().canUseVariableMagnification());
                            newSubtype.getSubtypeAdvanceSettings().setIsInterferenceCompensatable(origSubtype.getSubtypeAdvanceSettings().isIsInterferenceCompensatable());
                            newSubtype.getSubtypeAdvanceSettings().setMagnificationType(origSubtype.getSubtypeAdvanceSettings().getMagnificationType());
                            newSubtype.getSubtypeAdvanceSettings().setSignalCompensation(origSubtype.getSubtypeAdvanceSettings().getSignalCompensation());
                            newSubtype.getSubtypeAdvanceSettings().setUserGain(origSubtype.getSubtypeAdvanceSettings().getUserGain());
                            newSubtype.getSubtypeAdvanceSettings().setStageSpeedEnum(origSubtype.getSubtypeAdvanceSettings().getStageSpeedList());
                            
                            processedSubtype.add(newSubtype);
                          }
                        }
                      }
                      origSubtypes.clear();
                    }
                    else
                    {
                      // Should not happen
                      Assert.expect(false);
                    }
                  }
                }
              }
            }
            else
            {
              landPattern.add();
            }
            
            //Clear all the mapping
            componentTypeList.clear();
            importedLandPatternName.clear();
            componentTypeLongNameToSubtypeLongNameMap.clear();
            componentTypeShortNameToSubtypeShortNameMap.clear();
            origCompRefDesAndSubtypesMap.clear();
          }
        }
        finally
        {
          busyDialog.dispose();
          _landPatternsToBeImported.clear();
          _filePathToImport = null;
          _projectObservable.setEnabled(true);
        }
        _projectObservable.stateChanged(this, null, null, ProjectEventEnum.IMPORT_LAND_PATTERN_COMPLETE);
      }
    });
    
    try
    {
      busyDialog.setVisible(true);
    }
    finally
    {
      MessageDialog.showInformationDialog(busyDialog, 
                                          StringLocalizer.keyToString("CAD_CREATOR_IMPORT_SUCCESS_MSG_KEY"), 
                                          StringLocalizer.keyToString("CAD_CREATOR_IMPORT_SUCCESS_TITLE_KEY"), 
                                          true);
      dispose();
    }
  }

  /**
   * @author Ying-Huan.Chu
   */
  protected void confirmExitOfDialog()
  {
    // confirm with the user if they want to cancel the dialog
    int response = ChoiceInputDialog.showConfirmDialog(MainMenuGui.getInstance(),
                                                       StringLocalizer.keyToString("CAD_CREATOR_IMPORT_EXPORT_LAND_PATTERN_CONFIRM_EXIT_OF_GUI_KEY"),
                                                       StringLocalizer.keyToString("CAD_CREATOR_IMPORT_EXPORT_LAND_PATTERN_CONFIRM_EXIT_OF_GUI_DIALOG_TITLE_KEY"));

    if (response == JOptionPane.NO_OPTION || response == JOptionPane.CLOSED_OPTION)
      return;

    //save the setting since user agreed to cancel the process.
    try
    {
      _searchFilterPersistance.writeSettings();
    }
    catch (DatastoreException de)
    {
      MessageDialog.showErrorDialog(null,
                                    de.getMessage(),
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
    // the user wants to cancel.
    dispose();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setLandPatternsToBeImported(java.util.List<LandPattern> landPatternsToBeImported)
  {
    _landPatternsToBeImported = landPatternsToBeImported;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public java.util.List<LandPattern> getLandPatternsToBeExported()
  {
    Assert.expect(_landPatternsToBeImported != null);
    
    return _landPatternsToBeImported;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setFilePathToExport(String filePath)
  {
    _filePathToImport = filePath;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public String getFilePathToExport()
  {
    Assert.expect(_filePathToImport != null);
    
    return _filePathToImport;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean readCSVFile(final String reportFileName)
  {
    Assert.expect(reportFileName != null);

    final BusyCancelDialog busyDialog = new BusyCancelDialog(this,
                                                              StringLocalizer.keyToString("CAD_CREATOR_IMPORT_LAND_PATTERN_BUSYCANCELDIALOG_MSG_KEY"),
                                                              StringLocalizer.keyToString("CAD_CREATOR_IMPORT_LAND_PATTERN_BUSYCANCELDIALOG_TITLE_KEY"),
                                                              true);
    busyDialog.pack();
    SwingUtils.centerOnComponent(busyDialog, this);
    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _isFileReadSuccess = readFile(_csvDelimiter, reportFileName);
        }
        catch (IOException ex)
        {
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                        ex.getMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                        JOptionPane.OK_OPTION);
        }
        finally
        {
          busyDialog.dispose();
        }
      }
    });
    busyDialog.setVisible(true);
    
    return _isFileReadSuccess;
  }
  
  /**
   * @author Ying-Huan.Chu
   * @author Kee Chin Seong - Fixed the Land Pattern through hole coordinate issue
   */
  public boolean readFile(String delimiter, String reportFileName) throws IOException
  {
    FileReaderUtil fileReader = new FileReaderUtil();
    fileReader.open(reportFileName);
    
    _landPatternsToBeImported.clear();
    
    ProjectObservable.getInstance().setEnabled(false);
    try
    {
      int version = -1;
      while (fileReader.hasNextLine())
      {
        if (fileReader.getLineNumber() < 4) // Line 1 - 3 are the headers. Skip these lines.
        {
          String readLine = fileReader.readNextLine();
          if(fileReader.getLineNumber() == 2)
          {
            //Siew Yeng - XCR-3318 - Oval PTH
            //Version=[fileVersion]
            if(readLine.matches("Version=[0-9]*"))
            {
              version = Integer.parseInt(readLine.split("=")[1]);
            }
            else
            {
              version = 1;
            }
          }
          else if(fileReader.getLineNumber() == 3)
          {
            String columnHeader = "";
            if(version == 1)
            {
              columnHeader = "LandPattern,Pad,X Location,Y Location,X Dimension,Y Dimension,Rotation,Shape,SM/TH,Hole,isLandPatternPadOne";
            }
            else if (version == 2)
            {
              columnHeader = "LandPattern,Pad,X Location,Y Location,X Dimension,Y Dimension,Rotation,Shape,SM/TH,Hole Width,Hole Length,isLandPatternPadOne";
            }
            
            if(readLine.matches(columnHeader) == false)
            {
              JOptionPane.showMessageDialog(this,
                                            StringLocalizer.keyToString("CC_WRONG_LAND_PATTERN_FILE_FORMAT_ERR_KEY"),
                                            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                            JOptionPane.OK_OPTION);
              return false;
            }
          }
          continue;
        }

        String line = fileReader.readNextLine();
        String[] landPatternPadInfoString = line.split(",");

        // Get the LandPattern from the LandPattern list. Create a new one if there's no LandPattern with the same name in the list.
        String importName = landPatternPadInfoString[0];
        LandPattern landPattern = getLandPattern(importName);
        if (landPattern == null)
        {
          landPattern = new LandPattern(importName);
          landPattern.setPanel(_project.getPanel());
        }
        // Begin assigning values to the LandPatternPad.
        LandPatternPad pad = landPatternPadInfoString[8].equalsIgnoreCase(_SURFACE_MOUNT_PAD)? new SurfaceMountLandPatternPad() : new ThroughHoleLandPatternPad();
        pad.setLandPattern(landPattern);
        pad.setName(landPatternPadInfoString[1]);
        if (pad instanceof ThroughHoleLandPatternPad)
            ((ThroughHoleLandPatternPad)pad).setHoleCoordinateInNanoMeters(new ComponentCoordinate(Integer.parseInt(landPatternPadInfoString[2]), Integer.parseInt(landPatternPadInfoString[3])));
        pad.setCoordinateInNanoMeters(new ComponentCoordinate(Integer.parseInt(landPatternPadInfoString[2]), Integer.parseInt(landPatternPadInfoString[3])));
        pad.setWidthInNanoMeters(Integer.parseInt(landPatternPadInfoString[4]));
        pad.setLengthInNanoMeters(Integer.parseInt(landPatternPadInfoString[5]));
        pad.setDegreesRotation(Double.parseDouble(landPatternPadInfoString[6]));
        ShapeEnum shape = landPatternPadInfoString[7].equalsIgnoreCase("Rectangle") ? ShapeEnum.RECTANGLE : ShapeEnum.CIRCLE;
        pad.setShapeEnum(shape);
        
        //Siew Yeng - XCR-3318 - Oval PTH
        int holeWidth = -1;
        int holeLength = -1;
        boolean isPadOne = false;
        
        if(version == 1)
        {
          holeWidth = holeLength = Integer.parseInt(landPatternPadInfoString[9]);
          isPadOne = Boolean.parseBoolean(landPatternPadInfoString[10]);
        }
        else if(version == 2)
        {
          holeWidth = Integer.parseInt(landPatternPadInfoString[9]);
          holeLength = Integer.parseInt(landPatternPadInfoString[10]);
          isPadOne = Boolean.parseBoolean(landPatternPadInfoString[11]);
        }

        if (pad.isThroughHolePad())
        {
//          ((ThroughHoleLandPatternPad)pad).setHoleDiameterInNanoMeters(Integer.parseInt(landPatternPadInfoString[9]));
          ((ThroughHoleLandPatternPad)pad).setHoleWidthInNanoMeters(holeWidth);
          ((ThroughHoleLandPatternPad)pad).setHoleLengthInNanoMeters(holeLength);
        }
        
        //Kee chin Seong - Must have land pattern one but NOT neccesary always particular pad one
//        if(landPatternPadInfoString[11].equalsIgnoreCase("isLandPatternPadOne"))
//           ((ThroughHoleLandPatternPad)pad).setPadOne(true);
        pad.setPadOne(isPadOne);
        // Finally, add the LandPatternPad to the LandPattern and add the LandPattern to the LandPatternList.
        landPattern.addLandPatternPad(pad);
        if (getLandPattern(landPattern.getName()) != null)
          _landPatternsToBeImported.remove(getLandPattern(landPattern.getName()));
        _landPatternsToBeImported.add(landPattern);
      }
    }
    finally
    {
      ProjectObservable.getInstance().setEnabled(true);
      fileReader.close();
    }
    return true;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private LandPattern getLandPattern(String landPatternName)
  {
    Assert.expect(landPatternName != null);
    
    LandPattern landPatternToReturn = null;
    for (LandPattern landPattern : _landPatternsToBeImported)
    {
      if (landPattern.getName().equals(landPatternName))
      {
        landPatternToReturn = landPattern;
        break;
      }
    }
    return landPatternToReturn;
  }
  
  /**
   * @author Cheah Lee Herng 
   * @author Khaw Chek Hau
   * @XCR3472: Assert when switch tab after delete all the land pattern
   */
  private void removePadTypeFromComponentType(ComponentType componentType)
  {    
    Assert.expect(componentType != null);

    // Remove existing PadType from CompPackage
    for(PadType padType : componentType.getPadTypes())
    {
      // Remove PadType from ComponentType
      componentType.removePadType(padType);

      for(com.axi.v810.business.panelDesc.Component component : componentType.getComponents())
      {
        for(Pad pad : padType.getPads())
        {
          if (pad.getComponent().equals(component) && 
              component.hasPad(pad.getName()))
          {
            component.removePad(pad);
            break;
          }
        }
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   * @author Khaw Chek Hau
   * @XCR3472: Assert when switch tab after delete all the land pattern
   */
  private void processPackagePinAndPadType(LandPattern landPattern, 
                                           ComponentType componentType, 
                                           CompPackage compPackage,
                                           java.util.List<JointTypeEnum> jointTypeEnums,
                                           boolean createPackagePin)
  {
    Assert.expect(landPattern != null);
    Assert.expect(componentType != null);
    Assert.expect(compPackage != null);
    Assert.expect(jointTypeEnums != null);    
    
    int counter = 0;
    for(LandPatternPad landPatternPad : landPattern.getLandPatternPads())
    {
      int currentCounter = counter++;
      JointTypeEnum currentJointTypeEnum;

      PackagePin packagePin = null;
      if (createPackagePin)
      {
        if (jointTypeEnums.size() <= currentCounter)
          currentJointTypeEnum = jointTypeEnums.get(0);
        else
          currentJointTypeEnum = jointTypeEnums.get(currentCounter);
          
        // Create PackagePin
        packagePin = new PackagePin();
        packagePin.setLandPatternPad(landPatternPad);
        packagePin.setCompPackage(compPackage);
        packagePin.setJointTypeEnum(currentJointTypeEnum);

        compPackage.addPackagePin(packagePin);
      }
      else
      {
        // Get current PackagePin
        packagePin = compPackage.getPackagePins().get(currentCounter);
      }
      Assert.expect(packagePin != null);

      // Create PadType
      PadType padType = new PadType();
      PadTypeSettings padTypeSettings = new PadTypeSettings();
      padTypeSettings.setInspected(true);
      padTypeSettings.setPadType(padType);

      padType.setComponentType(componentType);
      padType.setLandPatternPad(landPatternPad);
      padType.setPackagePin(packagePin);
      padType.setPadTypeSettings(padTypeSettings);

      componentType.addPadType(padType);         

      for(com.axi.v810.business.panelDesc.Component component : componentType.getComponents())
      {
        // Create Pad
        Pad pad = new Pad();
        pad.setNodeName("");
        PadSettings padSettings = new PadSettings();
        padSettings.setTestable(true);
        pad.setPadSettings(padSettings);
        padSettings.setPad(pad);
        pad.setPadType(padType); 
        padType.addPad(pad);

        // Add Pad into Component
        component.addPad(pad);
      }
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3472: Assert when switch tab after delete all the land pattern
   */
  private void removeUnusedCompPackage(com.axi.v810.business.panelDesc.Panel panel, CompPackage compPackage)
  {
    Assert.expect(panel != null);
    Assert.expect(compPackage != null);
    
    java.util.List<String> usedComPackageLongName = new ArrayList<String>();
    
    for (BoardType boardType : _project.getPanel().getBoardTypes())
    {
      for (ComponentType componentType : boardType.getComponentTypes())
      {
        usedComPackageLongName.add(componentType.getCompPackage().getLongName());
      }
    }
    
    if (usedComPackageLongName.contains(compPackage.getLongName()) == false)
    {
      compPackage.destroy();  
    }
  }
}
