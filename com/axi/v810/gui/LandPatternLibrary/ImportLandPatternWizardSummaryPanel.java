package com.axi.v810.gui.LandPatternLibrary;

import java.awt.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.v810.business.panelDesc.LandPattern;
import com.axi.v810.util.*;

/**
 * @author Ying-Huan.Chu
 */
public class ImportLandPatternWizardSummaryPanel extends WizardPanel
{
  private ImportLandPatternWizardDialog _parent = null;
  private java.util.List<LandPattern> _landPatterns = null;
  private String _filePath = null;
  
  private JLabel _csvFileLabel = new JLabel();

  /**
   * @author Ying-Huan.Chu
   */
  public ImportLandPatternWizardSummaryPanel(WizardDialog wizardDialog, java.util.List<LandPattern> landPatterns, String filePath)
  {
    super(wizardDialog);

    _parent = (ImportLandPatternWizardDialog)wizardDialog;
    _landPatterns = landPatterns;
    _filePath = filePath;
  }

  /**
   * @author Ying-Huan.Chu
   */
  private void createPanel()
  {
    removeAll();

    JPanel summaryPanel = new JPanel(new BorderLayout());
    summaryPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    
    JPanel summaryTitlePanel = new JPanel(new BorderLayout());
    summaryTitlePanel.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));
    summaryTitlePanel.setBackground(Color.WHITE);
    JLabel summaryTextLabel = new JLabel(StringLocalizer.keyToString("CAD_CREATOR_IMPORT_LAND_PATTERN_SUMMARY_TITLE_KEY"));
    summaryTextLabel.setFont(FontUtil.getBoldFont(summaryTextLabel.getFont(), Localization.getLocale()));
    summaryTitlePanel.add(summaryTextLabel, BorderLayout.CENTER);
    
    JLabel landPatternToExportTextLabel = new JLabel(StringLocalizer.keyToString("CAD_CREATOR_LAND_PATTERNS_TO_IMPORT_KEY"));
    landPatternToExportTextLabel.setFont(FontUtil.getBoldFont(landPatternToExportTextLabel.getFont(), Localization.getLocale()));
    
    //Khaw Chek Hau - XCR3472: Assert when switch tab after delete all the land pattern
    JLabel alignmentClearWarningTextLabel = new JLabel(StringLocalizer.keyToString("CAD_IMPORT_LAND_PATTERN_ALIGNMENT_CLEAR_WARNING_MESSAGE_KEY"));
    
    JList list = new JList();
    list.setListData(_landPatterns.toArray());
    JScrollPane landPatternsScrollPane = new JScrollPane();
    landPatternsScrollPane.setViewportView(list);
    JPanel landPatternPanel = new JPanel();
    landPatternPanel.setLayout(new BorderLayout());
    landPatternPanel.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));
    landPatternPanel.setBackground(Color.WHITE);
    landPatternPanel.setPreferredSize(new Dimension(300,250));
    landPatternPanel.add(landPatternToExportTextLabel, BorderLayout.NORTH);
    landPatternPanel.add(landPatternsScrollPane, BorderLayout.CENTER);
    landPatternPanel.add(alignmentClearWarningTextLabel, BorderLayout.SOUTH);
    
    summaryPanel.add(summaryTitlePanel, BorderLayout.NORTH);
    summaryPanel.add(landPatternPanel, BorderLayout.CENTER);

    add(summaryPanel, BorderLayout.CENTER);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void validatePanel()
  {
    //do nothing
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void populatePanelWithData()
  {
    createPanel();
    
    _dialog.setTitle(StringLocalizer.keyToString("CAD_CREATOR_IMPORT_LAND_PATTERN_TITLE_KEY"));

    WizardButtonState buttonState = new WizardButtonState(false, true, true, true);
    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void unpopulate()
  {
    //do nothing
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void setWizardName(String name)
  {
    _name = name;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public String getWizardName()
  {
    return _name;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void removeObservers()
  {
    //do nothing
  }
}
