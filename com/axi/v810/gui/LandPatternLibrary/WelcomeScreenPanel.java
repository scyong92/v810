package com.axi.v810.gui.LandPatternLibrary;

import java.awt.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: WelcomeScreenPanel</p>
 *
 * @author Kee Chin Seong
 * @version 1.0
 */
public class WelcomeScreenPanel extends WizardPanel
{
  private boolean _importPackage;

  /**
   *
   * @param wizardDialog
   * @param importPackage
   *
   * @author Kee Chin Seong
   */
  public WelcomeScreenPanel(WizardDialog wizardDialog, boolean importPackage)
  {
    super(wizardDialog);

    _importPackage = importPackage;
  }

  /**
   * @author Kee Chin Seong
   */
  private void createPanel()
  {
    removeAll();

    JPanel welcomePanel = new JPanel();
    welcomePanel.setLayout(new BoxLayout(welcomePanel, BoxLayout.Y_AXIS));

    String welcomeText = "";
    String welcomeDescText = "";

    if (_importPackage)
    {
      welcomeText = "PLWK_IMPORT_WELCOME_KEY";
      welcomeDescText = "PLWK_IMPORT_WELCOME_DESC_KEY";
    }
    else
    {
      welcomeText = "PLWK_EXPORT_WELCOME_KEY";
      welcomeDescText = "PLWK_EXPORT_WELCOME_DESC_KEY";
    }

    JLabel welcomeTextLabel = new JLabel(StringLocalizer.keyToString(welcomeText));
    welcomeTextLabel.setFont(FontUtil.getBoldFont(welcomeTextLabel.getFont(), Localization.getLocale()));

    JLabel welcomeDescTextLabel = new JLabel(StringLocalizer.keyToString(welcomeDescText));

    welcomePanel.add(welcomeTextLabel);
    welcomePanel.add(welcomeDescTextLabel);

    add(welcomePanel, BorderLayout.CENTER);
  }

  /**
   * @author Kee Chin Seong
   */
  public void validatePanel()
  {
    //do nothing
  }

  /**
   * @author Kee Chin Seong
   */
  public void populatePanelWithData()
  {
    createPanel();

    if (_importPackage)
    {
      _dialog.setTitle(StringLocalizer.keyToString("PLWK_IMPORT_TITLE_KEY"));
    }
    else
    {
      _dialog.setTitle(StringLocalizer.keyToString("PLWK_EXPORT_TITLE_KEY"));
    }

    WizardButtonState buttonState = new WizardButtonState(true, true, false, false);
    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Kee Chin Seong
   */
  public void unpopulate()
  {
    //do nothing
  }

  /**
   *
   * @param name
   *
   * @author Kee Chin Seong
   */
  public void setWizardName(String name)
  {
    _name = name;
  }

  /**
   *
   * @return String
   *
   * @author Kee Chin Seong
   */
  public String getWizardName()
  {
    return _name;
  }

  /**
   * @author Kee Chin Seong
   */
  public void removeObservers()
  {
    //do nothing
  }
}
