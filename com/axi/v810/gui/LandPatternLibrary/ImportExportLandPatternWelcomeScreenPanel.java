package com.axi.v810.gui.LandPatternLibrary;

import java.awt.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.v810.util.*;

/**
 * @author Ying-Huan.Chu
 */
public class ImportExportLandPatternWelcomeScreenPanel extends WizardPanel
{
  private boolean _importLandPattern; // true = import, false = export.

  /**
   * @author Ying-Huan.Chu
   */
  public ImportExportLandPatternWelcomeScreenPanel(WizardDialog wizardDialog, boolean importLandPattern)
  {
    super(wizardDialog);

    _importLandPattern = importLandPattern;
  }

  /**
   * @author Ying-Huan.Chu
   */
  private void createPanel()
  {
    removeAll();

    JPanel welcomePanel = new JPanel();
    welcomePanel.setLayout(new BoxLayout(welcomePanel, BoxLayout.Y_AXIS));

    String welcomeText = _importLandPattern? "CAD_CREATOR_IMPORT_LAND_PATTERN_WELCOME_KEY" : "CAD_CREATOR_EXPORT_LAND_PATTERN_WELCOME_KEY";
    String welcomeDescText = _importLandPattern? "CAD_CREATOR_IMPORT_LAND_PATTERN_WELCOME_DESC_KEY" : "CAD_CREATOR_EXPORT_LAND_PATTERN_WELCOME_DESC_KEY";

    JLabel welcomeTextLabel = new JLabel(StringLocalizer.keyToString(welcomeText));
    welcomeTextLabel.setFont(FontUtil.getBoldFont(welcomeTextLabel.getFont(), Localization.getLocale()));
    JLabel welcomeDescTextLabel = new JLabel(StringLocalizer.keyToString(welcomeDescText));

    welcomePanel.add(welcomeTextLabel);
    welcomePanel.add(welcomeDescTextLabel);

    add(welcomePanel, BorderLayout.CENTER);
    
    //validatePanel();
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void validatePanel()
  {
    WizardButtonState buttonState = new WizardButtonState(true, true, false, false);
    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void populatePanelWithData()
  {
    createPanel();

    String titleKey = _importLandPattern? "CAD_CREATOR_IMPORT_LAND_PATTERN_TITLE_KEY" : "CAD_CREATOR_EXPORT_LAND_PATTERN_TITLE_KEY";
    _dialog.setTitle(StringLocalizer.keyToString(titleKey));

    WizardButtonState buttonState = new WizardButtonState(true, false, false, false);
    _observable.updateButtonState(buttonState);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void unpopulate()
  {
    //do nothing
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void setWizardName(String name)
  {
    _name = name;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public String getWizardName()
  {
    return _name;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void removeObservers()
  {
    //do nothing
  }
}
