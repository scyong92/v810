package com.axi.v810.gui.LandPatternLibrary;

import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

/**
 * Export Land Pattern Wizard for CAD Creator
 * This class allows user to import a LandPattern from an external .CSV file or library.
 * 
 * @author Ying-Huan.Chu
 */
public class ExportLandPatternWizardDialog extends WizardDialog
{
  private final String _csvDelimiter = StringLocalizer.keyToString("TABLE_MODEL_CSV_REPORT_DELIMITER_KEY");
  private final static String _WELCOME_PANEL = "exportLandPatternWelcomePanel";
  private final static String _CHOOSE_LANDPATTERN_PANEL = "exportLandPatternChooseLandPatternPanel";
  private final static String _CHOOSE_CSV_FILE_PANEL = "exportLandPatternCsvChooseFilePanel";
  private final static String _SUMMARY_PANEL = "exportLandPatternSummaryPanel";
  private final String _SURFACE_MOUNT_PAD = "SM";
  private final String _THROUGH_HOLE_PAD = "TH";
  private final String[] _landPatternCsvFileColumns = {"LandPattern", 
                                                       "Pad", 
                                                       "X Location", 
                                                       "Y Location", 
                                                       "X Dimension", 
                                                       "Y Dimension", 
                                                       "Rotation", 
                                                       "Shape", 
                                                       _SURFACE_MOUNT_PAD + "/" + _THROUGH_HOLE_PAD, 
//                                                       "Hole",
                                                       "Hole Width", //Siew Yeng - XCR-3318 - Oval PTH
                                                       "Hole Length",
                                                       "isLandPatternPadOne"};
  
  private Dimension _exportLandPatternDimension = new Dimension(575, 400);
  
  private Frame _parent = null;
  private WizardPanel _currentVisiblePanel = null;
  private java.util.List<LandPattern> _landPatternsToBeExported = null;
  private String _filePathToExport = null;
  
  private Project _project = null;
  private SearchFilterPersistance _searchFilterPersistance;
  
  /**
   * @author Ying-Huan.Chu
   */
  public ExportLandPatternWizardDialog(Frame parent, boolean modal, Project project)
  {
    super(parent,
          modal,
          StringLocalizer.keyToString("WIZARD_NEXT_KEY"),
          StringLocalizer.keyToString("WIZARD_CANCEL_KEY"),
          StringLocalizer.keyToString("WIZARD_FINISH_KEY"),
          StringLocalizer.keyToString("WIZARD_BACK_KEY") );
    
    _parent = parent;
    _project = project;
    
    _searchFilterPersistance = new SearchFilterPersistance();
    _searchFilterPersistance = _searchFilterPersistance.readSettings();

    initWizard();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void initWizard()
  {
    java.util.List<WizardPanel> panels = new ArrayList<>();

    ImportExportLandPatternWelcomeScreenPanel welcomePanel = new ImportExportLandPatternWelcomeScreenPanel(this, false);
    welcomePanel.setWizardName(_WELCOME_PANEL);
    panels.add(welcomePanel);
    welcomePanel.populatePanelWithData();
    
    ExportLandPatternSelectionPanel chooseLandPatternPanel = new ExportLandPatternSelectionPanel(this, _project);
    chooseLandPatternPanel.setWizardName(_CHOOSE_LANDPATTERN_PANEL);
    panels.add(chooseLandPatternPanel);
    chooseLandPatternPanel.populatePanelWithData();
    
    ExportLandPatternChooseCsvFileScreenPanel chooseCsvFilePanel = new ExportLandPatternChooseCsvFileScreenPanel(this, _project);
    chooseCsvFilePanel.setWizardName(_CHOOSE_CSV_FILE_PANEL);
    panels.add(chooseCsvFilePanel);
    chooseCsvFilePanel.populatePanelWithData();
    
    ExportLandPatternWizardSummaryPanel summaryPanel = new ExportLandPatternWizardSummaryPanel(this, _landPatternsToBeExported, _filePathToExport);
    summaryPanel.setWizardName(_SUMMARY_PANEL);
    panels.add(summaryPanel);
    summaryPanel.populatePanelWithData();

    // set the panels
    setContentPanels(panels);
    setCurrentVisiblePanel(_WELCOME_PANEL);

    setPreferredSize(_exportLandPatternDimension);
    pack();
    
    WizardButtonState buttonState = new WizardButtonState(true, true, false, false);
    WizardPanelObservable.getInstance().updateButtonState(buttonState);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setLandPatternsToBeExported(java.util.List<LandPattern> landPatternsToBeExported)
  {
    _landPatternsToBeExported = landPatternsToBeExported;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public java.util.List<LandPattern> getLandPatternsToBeExported()
  {
    Assert.expect(_landPatternsToBeExported != null);
    
    return _landPatternsToBeExported;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setFilePathToExport(String filePath)
  {
    _filePathToExport = filePath;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public String getFilePathToExport()
  {
    Assert.expect(_filePathToExport != null);
    
    return _filePathToExport;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void setCurrentVisiblePanel(String currentVisiblePanelName)
  {
    Assert.expect(currentVisiblePanelName != null, "current visible panel name is null");
    Assert.expect(_contentPanels != null, "content panels are null");

    _currentVisiblePanel = null;

    // iterate through the list and find the panel with the name sent in
    for(WizardPanel panel : _contentPanels)
    {
      if(panel.getWizardName().equals(currentVisiblePanelName))
      {
        _currentVisiblePanel = panel;
        break;
      }
    }

    Assert.expect(_currentVisiblePanel != null, "Current visible panel is null and was not found");
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  protected void handleNextScreenButtonEvent()
  {
    // lets change the buttons state to all disabled so that the user does not think the buttons are active when they are not.
    WizardButtonState buttonState = new WizardButtonState(false, false, false, false);
    WizardPanelObservable.getInstance().updateButtonState(buttonState);
    
    String nextPanelName = null;
    String currentPanelName = _currentVisiblePanel.getWizardName();

    if(currentPanelName.equals(_WELCOME_PANEL))
    {
      nextPanelName = _CHOOSE_LANDPATTERN_PANEL;
      _exportLandPatternDimension = new Dimension(1080, 520);
    }
    else if(currentPanelName.equals(_CHOOSE_LANDPATTERN_PANEL))
    {
      nextPanelName = _CHOOSE_CSV_FILE_PANEL;
      _exportLandPatternDimension = new Dimension(575, 400);
    }
    else if (currentPanelName.equals(_CHOOSE_CSV_FILE_PANEL))
    {
      nextPanelName = _SUMMARY_PANEL;
      _exportLandPatternDimension = new Dimension(575, 400);
    }
    else
    {
      // they better not be on the summary and press next!
      Assert.expect(false, " on summary screen next button should not be enabled");
    }
    
    CardLayout layout = (CardLayout)_contentPanel.getLayout();
    layout.show(_contentPanel, nextPanelName);

    setCurrentVisiblePanel(nextPanelName);
    _currentVisiblePanel.populatePanelWithData();
    _currentVisiblePanel.revalidate();

    setPreferredSize(_exportLandPatternDimension);
    pack();

    SwingUtils.centerOnComponent(this, _parent);
  }

  /**
   * @author Ying-Huan.Chu
   */
  protected void handlePreviousScreenButtonEvent()
  {
    Assert.expect(_currentVisiblePanel != null, "current visible panel is not set");

    String previousPanelName = null;
    String currentPanelName = _currentVisiblePanel.getWizardName();

    if(currentPanelName.equals(_CHOOSE_LANDPATTERN_PANEL))
    {
      previousPanelName = _WELCOME_PANEL;
      _exportLandPatternDimension = new Dimension(575, 400);
    }
    else if(currentPanelName.equals(_CHOOSE_CSV_FILE_PANEL))
    {
      previousPanelName = _CHOOSE_LANDPATTERN_PANEL;
      _exportLandPatternDimension = new Dimension(1080, 520);
    }
    else if (currentPanelName.equals(_SUMMARY_PANEL))
    {
      previousPanelName = _CHOOSE_CSV_FILE_PANEL;
      _exportLandPatternDimension = new Dimension(575, 400);
    }
    else
    {
      Assert.expect(false);
    }

    CardLayout layout = (CardLayout)_contentPanel.getLayout();
    layout.show(_contentPanel, previousPanelName);
    setCurrentVisiblePanel(previousPanelName);
    _currentVisiblePanel.populatePanelWithData();
    _currentVisiblePanel.revalidate();

    setPreferredSize(_exportLandPatternDimension);
    pack();

    SwingUtils.centerOnComponent(this, _parent);
  }

  /**
   * @author Ying-Huan.Chu
   */
  protected void handleCompleteProcessButtonEvent()
  {
    try
    {
      writeCSVFile(_filePathToExport, _landPatternsToBeExported);
    }
    catch (IOException ex)
    {
      Assert.logException(ex);
    }
    LocalizedString message = new LocalizedString("CAD_CREATOR_EXPORT_SUCCESS_MSG_KEY", new Object[]{_filePathToExport});
    MessageDialog.showInformationDialog(this, StringLocalizer.keyToString(message), StringLocalizer.keyToString("CAD_CREATOR_EXPORT_SUCCESS_TITLE_KEY"), true);
    
    _landPatternsToBeExported.clear();
    _filePathToExport = null;
    dispose();
  }

  /**
   * @author Ying-Huan.Chu
   */
  protected void confirmExitOfDialog()
  {
    // confirm with the user if they want to cancel the dialog
    int response = ChoiceInputDialog.showConfirmDialog(MainMenuGui.getInstance(),
                                                       StringLocalizer.keyToString("CAD_CREATOR_IMPORT_EXPORT_LAND_PATTERN_CONFIRM_EXIT_OF_GUI_KEY"),
                                                       StringLocalizer.keyToString("CAD_CREATOR_IMPORT_EXPORT_LAND_PATTERN_CONFIRM_EXIT_OF_GUI_DIALOG_TITLE_KEY"));

    if (response == JOptionPane.NO_OPTION || response == JOptionPane.CLOSED_OPTION)
      return;

    //save the setting since user agreed to cancel the process.
    try
    {
      _searchFilterPersistance.writeSettings();
    }
    catch (DatastoreException de)
    {
      MessageDialog.showErrorDialog(null,
                                    de.getMessage(),
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
    // the user wants to cancel.
    dispose();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void writeCSVFile(String reportFileName, java.util.List<LandPattern> landPatterns) throws IOException
  {
    Assert.expect(reportFileName != null);
    Assert.expect(landPatterns != null);

    writeFile(_csvDelimiter, reportFileName, landPatterns);
  }

  /**
   * @author Ying-Huan.Chu
   * @edited By Kee Chin Seong - Land pattern pad one settings
   */
  public void writeFile(String delimiter, String reportFileName, java.util.List<LandPattern> landPatterns) throws IOException
  {
    // do not append
    FileWriterUtil fileWriter = new FileWriterUtil(reportFileName, false);
    fileWriter.open();

    // report date
    Date date = new Date();
    fileWriter.writeln(delimiter + delimiter + date);
    //Siew Yeng - XCR-3318 - Oval PTH
    fileWriter.writeln("Version=" + FileName.getLandPatternLatestCsvVersion());

    // column header line
    StringBuffer headerLine = new StringBuffer();
    for (int col = 0; col < _landPatternCsvFileColumns.length; col++)
    {
      if (col > 0)
      {
        headerLine.append(delimiter);
      }
      headerLine.append(_landPatternCsvFileColumns[col]);
    }
    fileWriter.writeln(headerLine.toString());

    // table data
    for (LandPattern landPattern : _landPatternsToBeExported)
    {
      for (LandPatternPad pad : landPattern.getLandPatternPads())
      {
        StringBuffer reportLine = new StringBuffer();
        for (int col = 0; col < _landPatternCsvFileColumns.length; col++)
        {
          if (col > 0)
          {
            reportLine.append(delimiter);
          }
          switch (col)
          {
            case 0:
              reportLine.append(landPattern.getName());
              break;
            case 1:
              reportLine.append(pad.getName());
              break;
            case 2:
              reportLine.append(pad.getCenterCoordinateInNanoMeters().getX());
              break;
            case 3:
              reportLine.append(pad.getCenterCoordinateInNanoMeters().getY());
              break;
            case 4:
              reportLine.append(pad.getWidthInNanoMeters());
              break;
            case 5:
              reportLine.append(pad.getLengthInNanoMeters());
              break;
            case 6:
              reportLine.append(pad.getDegreesRotation());
              break;
            case 7:
              reportLine.append(pad.getShapeEnum().getName());
              break;
            case 8:
            {
              String padType = pad.isSurfaceMountPad() ? _SURFACE_MOUNT_PAD : _THROUGH_HOLE_PAD;
              reportLine.append(padType);
              break;
            }
            case 9:
            {
              int holeWidthInNanometer = pad.isThroughHolePad()? ((ThroughHoleLandPatternPad)pad).getHoleWidthInNanoMeters(): 0;
              reportLine.append(holeWidthInNanometer);
              break;
            }
            case 10:
            {
              int holeLengthInNanometer = pad.isThroughHolePad()? ((ThroughHoleLandPatternPad)pad).getHoleLengthInNanoMeters() : 0;
              reportLine.append(holeLengthInNanometer);
              break;
            }
            case 11:
            {
              reportLine.append(pad.getLandPattern().getLandPatternPadOne().equals(pad));
              break;
            }
            default:
              break;
          }
        }
        fileWriter.writeln(reportLine.toString());
      }
    }
    fileWriter.close();
  }
}
