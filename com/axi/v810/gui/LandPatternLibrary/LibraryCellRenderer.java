package com.axi.v810.gui.LandPatternLibrary;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.v810.business.packageLibrary.*;
import com.axi.util.*;

/**
 * @author Chin Seong, KEe
 * @version 1.0
 */
public class LibraryCellRenderer extends DefaultTableCellRenderer
{
  /**
   * Creates a Custom JLabel Cell Renderer
   *
   * @author Chin Seong, KEe
   */
  public LibraryCellRenderer()
  {
    super();
  }

  /**
   * Returns the component used for drawing the cell.  This method is
   * used to configure the renderer appropriately before drawing.
   * see TableCellRenderer.getTableCellRendererComponent(...); for more comments on the method
   *
   * @author Chin Seong, KEe
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    String libraryPathValue = (String)value;

    JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    label.setOpaque(true);
    label.setToolTipText(libraryPathValue);

    String libraryFullPath = null;

    if ( table instanceof ExportLandPatternLayoutTable)
    {
      ExportLandPatternLayoutTable ExportLandPatternLayoutTable = (ExportLandPatternLayoutTable)table;
      ExportLandPatternLayoutTableModel ExportLandPatternLayoutTableModel = (ExportLandPatternLayoutTableModel)ExportLandPatternLayoutTable.getModel();
      //libraryFullPath = ExportLandPatternLayoutTableModel.getLandPatternAt(row).getLibraryPath();
    }

    Assert.expect(libraryFullPath != null);

    if( LibraryManager.doesLibraryExist( libraryFullPath ) == false)
    {
      if (isSelected)
      {
        label.setBackground(table.getSelectionBackground());
        label.setForeground(Color.WHITE);
      }
      else
      {
        label.setBackground(Color.RED);
        label.setForeground(Color.WHITE);
      }
    }
    else
    {
      if (isSelected)
      {
        label.setBackground(table.getSelectionBackground());
        label.setForeground(table.getSelectionForeground());
      }
      else
      {
        label.setBackground(table.getBackground());
        label.setForeground(table.getForeground());
      }
    }
    return label;
  }
}
