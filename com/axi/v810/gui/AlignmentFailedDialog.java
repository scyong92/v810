package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * A dialog to display the alignment failure and provide access to the images
 * @author Andy Mechtenberg
 */
public class AlignmentFailedDialog extends EscapeDialog
{
  private JFrame _parent;
  private Font _font = null;
  private Project _project;
  private XrayTesterException _alignmentException;
  private JLabel _failedMessageLabel;
  private boolean _performRuntimeManualAlignment = false;
  private boolean _showRuntimeManualAlignmentButton = false;


  /**
   * @author Andy Mechtenberg
   */
  public AlignmentFailedDialog(JFrame parent, String title, Project project, XrayTesterException alignmentException)
  {
    super(parent, title, true);
    Assert.expect(parent != null);
    Assert.expect(title != null);
    Assert.expect(project != null);
    Assert.expect(alignmentException != null);
    _parent = parent;
    _project = project;
    _alignmentException = alignmentException;
    jbInit();
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    pack();
  }

  /**
   * @author Andy Mechtenberg
   */
  public AlignmentFailedDialog(JFrame parent, String title, Project project, XrayTesterException alignmentException, Font font)
  {
    super(parent, title, true);
    Assert.expect(parent != null);
    Assert.expect(title != null);
    Assert.expect(project != null);
    Assert.expect(alignmentException != null);
    Assert.expect(font != null);
    _parent = parent;
    _project = project;
    _alignmentException = alignmentException;
    _font = font;
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    if (project.getPanel().getPanelSettings().isUsing2DAlignmentMethod() == false)
      _showRuntimeManualAlignmentButton = true; //Siew Yeng - XCR1757 - show this button only when in production
    jbInit();
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    SwingUtils.setFont(this, _font);
    _failedMessageLabel.setFont(FontUtil.getBoldFont(_failedMessageLabel.getFont(),Localization.getLocale()));
    pack();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    JButton okButton = new JButton(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));

    okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });

    JButton showImageButton = new JButton(StringLocalizer.keyToString("MMGUI_ALIGNMENT_SHOW_IMAGES_KEY"));
    showImageButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showImageDialog();
      }
    });
    
    //Siew Yeng - XCR1757
    JButton runManualAlignmentButton = new JButton(StringLocalizer.keyToString("GUI_REALIGN_BUTTON_KEY"));
    runManualAlignmentButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _performRuntimeManualAlignment = true;
        dispose();
      }
    });

    JPanel mainPanel = new JPanel(new BorderLayout(0, 10));
    JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    JPanel innerButtonPanel = new JPanel(new GridLayout(1, 3, 10, 0));
    innerButtonPanel.add(showImageButton);
    innerButtonPanel.add(okButton);
    if(_showRuntimeManualAlignmentButton)
      innerButtonPanel.add(runManualAlignmentButton);
    buttonPanel.add(innerButtonPanel);
    mainPanel.add(buttonPanel, BorderLayout.SOUTH);

    _failedMessageLabel = new JLabel(StringLocalizer.keyToString("MMGUI_ALIGNMENT_FAILED_KEY"));
    _failedMessageLabel.setFont(FontUtil.getBoldFont(_failedMessageLabel.getFont(),Localization.getLocale()));
    JPanel failedMessagePanel = new JPanel();
    failedMessagePanel.add(_failedMessageLabel);
    mainPanel.add(failedMessagePanel, BorderLayout.NORTH);

    String localMessage = _alignmentException.getMessage();
    String formattedMessage = StringUtil.format(localMessage, 100);
    JTextArea reasonForFailureLabel = new JTextArea(formattedMessage);
    reasonForFailureLabel.setEditable(false);
    reasonForFailureLabel.setBackground(mainPanel.getBackground());
    reasonForFailureLabel.setOpaque(true);
    reasonForFailureLabel.setFont(UIManager.getFont("Button.messageFont"));
    mainPanel.add(reasonForFailureLabel, BorderLayout.CENTER);
    mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 10));

    this.getContentPane().add(mainPanel, BorderLayout.CENTER);
    this.getRootPane().setDefaultButton(okButton);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void showImageDialog()
  {
    AlignmentImageDialog alignmentImageDialog = new AlignmentImageDialog(_parent,
        StringLocalizer.keyToString("MM_GUI_TDT_TITLE_MANUAL_ALIGNMENT_KEY"),
        _project);
    SwingUtils.centerOnComponent(alignmentImageDialog, _parent);
    if (_font != null)
      SwingUtils.setFont(alignmentImageDialog, _font);
    alignmentImageDialog.setVisible(true);
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean isReAlignAction()
  {
    return _performRuntimeManualAlignment;
  }
}
