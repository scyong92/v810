package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * Delete all project files, directory included, for a selected AXI project.
 * @author Laura Cormos
 */
public class ProjectDeleteDialog extends JDialog
{
  private static boolean _runFromMain = false;

  private static final String _assertLogFile = "ProjectDeleteDialog";
  private static final String _appName = StringLocalizer.keyToString("PDD_GUI_NAME_KEY");

  // gui components

  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private JPanel _projectSelectionPanel = new JPanel();
  private BorderLayout _projectSelectionPanelBorderLayout = new BorderLayout();
  private JPanel _projectsDirPanel = new JPanel();
  private BorderLayout _projectsDirPanelBorderLayout = new BorderLayout();
  private JLabel _projectsDirLabel = new JLabel();
  private JLabel _projectsDirLocationLabel = new JLabel();
  private JScrollPane _projectListScrollPane = new JScrollPane();
  private DefaultListModel _projectListModel = new DefaultListModel();
  private JList _projectList = new JList(_projectListModel);
  private JPanel _controlsPanel = new JPanel();
  private BorderLayout _controlsPanelBorderLayout = new BorderLayout();
  private JPanel _southControlsPanel = new JPanel();
  private FlowLayout _southControlsPanelFlowLayout = new FlowLayout();
  private JPanel _buttonPanel = new JPanel();
  private GridLayout _buttonPanelGridLayout = new GridLayout();
  private JButton _deleteButton = new JButton();
  private JButton _exitButton = new JButton();
  private Border _empty5505SpaceBorder;
  private KeyListener _escapeKeyListener = null;

  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
  private MainMenuGui _mainUI = MainMenuGui.getInstance();

  /**
   * @author Laura Cormos
   */
  public ProjectDeleteDialog( Frame parentFrame, boolean modal )
  {
    super(parentFrame, _appName, modal);
    Assert.expect(parentFrame != null);

    try
    {
      jbInit();
    }
    catch(RuntimeException e)
    {
      Assert.logException(e);
    }

    populateProjectlList();
  }

  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _runFromMain = true;
          MainMenuGui mainMenu = MainMenuGui.getInstance();
          mainMenu.start();
          ProjectDeleteDialog projectDeleteDialog = new ProjectDeleteDialog(mainMenu, true);
          projectDeleteDialog.pack();
          projectDeleteDialog.setVisible(true);
        }
        catch (RuntimeException re)
        {
          re.printStackTrace();
          Assert.logException(re);
        }
      }
    });
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit()
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _controlsPanel.setLayout(_controlsPanelBorderLayout);
    _deleteButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _deleteButton.setText(StringLocalizer.keyToString("PDD_GUI_DELETE_BUTTON_KEY"));
    _deleteButton.setEnabled(false);
    _deleteButton.setMnemonic((StringLocalizer.keyToString("GUI_DELETE_BUTTON_MNEMONIC_KEY")).charAt(0));
    _deleteButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteButton_actionPerformed();
      }
    });
    _exitButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _exitButton.setText(StringLocalizer.keyToString("GUI_EXIT_BUTTON_KEY"));
    _exitButton.setMnemonic((StringLocalizer.keyToString("GUI_EXIT_BUTTON_MNEMONIC_KEY")).charAt(0));
    _exitButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exitButton_actionPerformed(e);
      }
    });
    _southControlsPanel.setLayout(_southControlsPanelFlowLayout);

    setResizable(true);
    _projectSelectionPanel.setLayout(_projectSelectionPanelBorderLayout);
    _projectSelectionPanelBorderLayout.setHgap(5);
    _projectSelectionPanelBorderLayout.setVgap(5);
    _projectList.setLayoutOrientation(JList.VERTICAL_WRAP);
    _projectList.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        projectList_mouseReleased(e);
      }
    });
    _projectList.setFixedCellWidth(-1);
    _projectList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
    _projectList.addListSelectionListener(new javax.swing.event.ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        projectList_valueChanged(e);
      }
    });
    _buttonPanel.setLayout(_buttonPanelGridLayout);
    _buttonPanelGridLayout.setColumns(2);
    _buttonPanelGridLayout.setHgap(10);
    _buttonPanelGridLayout.setVgap(20);
    _empty5505SpaceBorder = BorderFactory.createEmptyBorder(5, 5, 0, 5);
    _projectsDirLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _projectsDirLabel.setHorizontalTextPosition(SwingConstants.LEFT);
    _projectsDirLabel.setText(StringLocalizer.keyToString("PZD_GUI_PROJECTS_DIR_LABEL_KEY"));
    _projectsDirLocationLabel.setText(Directory.getProjectsDir());
    _projectSelectionPanel.setBorder(_empty5505SpaceBorder);
    _projectsDirPanelBorderLayout.setHgap(5);
    _mainPanel.add(_controlsPanel, BorderLayout.SOUTH);
    _controlsPanel.add(_southControlsPanel,  BorderLayout.SOUTH);
    _southControlsPanel.add(_buttonPanel, null);
    _buttonPanel.add(_deleteButton, null);
    _buttonPanel.add(_exitButton, null);
    _mainPanel.add(_projectSelectionPanel, BorderLayout.CENTER);
    _projectListScrollPane.getViewport().add(_projectList, null);
    _projectsDirPanel.setLayout(_projectsDirPanelBorderLayout);
    _projectSelectionPanel.add(_projectsDirPanel, BorderLayout.NORTH);
    _projectsDirPanel.add(_projectsDirLabel, BorderLayout.WEST);
    _projectsDirPanel.add(_projectsDirLocationLabel, BorderLayout.CENTER);
    _projectSelectionPanel.add(_projectListScrollPane, BorderLayout.CENTER);
    getContentPane().add(_mainPanel, BorderLayout.CENTER);
    _escapeKeyListener = new KeyAdapter()
    {
      public void keyPressed(KeyEvent e)
      {
        checkForEscapeKeyPressed(e);
      }
    };
    addKeyListener(_escapeKeyListener);
    _projectList.addKeyListener(_escapeKeyListener);
  }

  /**
   * @author Laura Cormos
   */
  void projectList_valueChanged(ListSelectionEvent e)
  {
    Assert.expect(_projectList != null);

    if (_projectList.getSelectedIndex() != -1)
    {
      _deleteButton.setEnabled(true);
    }
    else
    {
      _deleteButton.setEnabled(false);
    }
  }

  /**
   * The delete procedure goes here.
   * @author Laura Cormos
   */
  void deleteButton_actionPerformed()
  {
    Assert.expect(_projectList != null);
    Object[] selectedProjects = _projectList.getSelectedValues();

    for (Object project : selectedProjects)
    {
      final String projectName = project.toString();

      File projectNameDir = new File(Directory.getProjectDir(projectName));

      if (projectNameDir.exists())
        if (projectNameDir.isDirectory())
          if (JOptionPane.showConfirmDialog(
            this,
            StringLocalizer.keyToString(new LocalizedString("PDD_CONFIRM_PROJECT_DELETE_KEY", new Object[]{projectName})),
            StringLocalizer.keyToString("PDD_CONFIRM_DELETE_TITLE_KEY"),
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION)
            continue;
      // Change the cursor temporarily and wait for the delete to finish.
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      setEnabled(false); // do not allow user's input during delete

      _swingWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            Project.delete(projectName);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _mainUI.projectWasDeleted(projectName);
                setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                setEnabled(true);
                _projectListModel.removeAllElements();
                populateProjectlList();
              }
            });
          }
          catch (final DatastoreException ie)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                setEnabled(true); // allow user to press a button
                MessageDialog.reportIOError(ProjectDeleteDialog.this, ie.getLocalizedMessage());
              }
            });
          }
        }
      });
    }
  }

  /**
   * When the exit button is pressed the Project Delete Dialog will exit.
   * @author Laura Cormos
   */
  private void exitButton_actionPerformed(ActionEvent e)
  {
    exitDialog();
  }

  /**
   * Exit this if the escape key is pressed
   * @author Laura Cormos
   */
  private void checkForEscapeKeyPressed(KeyEvent e)
  {
    if ( e.getKeyCode() == KeyEvent.VK_ESCAPE )
      exitDialog();
  }

  /**
   * @author Laura Cormos
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if ( e.getID() == WindowEvent.WINDOW_CLOSING )
      exitDialog();
  }

  /**
   * @author Laura Cormos
   */
  private void exitDialog()
  {
    if (_runFromMain)
      System.exit(0);
    else
    {
      setVisible(false);
      dispose();
    }
  }

  /**
   * @author Laura Cormos
   */
  private void populateProjectlList()
  {
    java.util.List<String> projectList = FileName.getProjectNames();
//    String currentProject = "<none>";
//    if (Project.isCurrentProjectLoaded())
//    {
//      currentProject = Project.getCurrentlyLoadedProject().getName();
//    }
    String currentProject = _mainUI.getCurrentProjectName();
//    System.out.println("currentProject = " + currentProject);

    if (projectList != null)
    {
      for (String projectName : projectList)
      {
        // Don't add current project to list because it makes a mess if
        // it is deleted.
        if (projectName.equalsIgnoreCase(currentProject) == false)
        {
          _projectListModel.addElement(new File(projectName).getName());
        }
      }
    }
    int numProjects = _projectListModel.getSize();
    int visibleRows = numProjects / 4;
    if (visibleRows < 10)
      visibleRows = 10;
    _projectList.setVisibleRowCount(visibleRows);
  }

  /**
   * If user double clicks on a project name, start the delete action
   * * @author Laura Cormos
   */
  private void projectList_mouseReleased(MouseEvent e)
  {
    if(e.getClickCount() == 2)
    {
      //the user has double clicked
      deleteButton_actionPerformed();
    }
  }

  /**
   * This method is overwritten from ancenstor Component. It's meant to enable/disable all controls on the dialog while
   * something else is going on
   * @author Laura Cormos
   */
  public void setEnabled(boolean b)
  {
    _exitButton.setEnabled(b);
    _deleteButton.setEnabled(b);
  }
}
