package com.axi.v810.gui;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import com.axi.v810.util.*;
import com.axi.v810.images.*;
import com.axi.v810.gui.mainMenu.*;

/**
 * This class creates and displays a general purpose about box
 * @author Andy Mechtenberg
 */
public class AxiAboutBox extends JDialog implements ActionListener
{
  private JPanel _mainPanel = new JPanel();
  private JPanel _centerPanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _imagePanel = new JPanel();
  private JPanel _labelPanel = new JPanel();
  private JButton _okButton = new JButton();
  private JLabel _axiImageLabel = new JLabel();
  private ImageIcon _imageIcon = Image5DX.getImageIcon( Image5DX.AXI );
  private JLabel _titleLabel = new JLabel();
  private JLabel _versionLabel = new JLabel();
  private JLabel _copyrightLabel = new JLabel();
  private JLabel _axiLabel = new JLabel();
  private BorderLayout _mainBorderLayout = new BorderLayout();
  private BorderLayout _centerBorderLayout = new BorderLayout();
  private GridLayout _labelGridLayout = new GridLayout();
  private String _product;
  private String _appMessage;
  private String _version = StringLocalizer.keyToString("GUI_ABOUT_BOX_VERSION_KEY") + ": " + Version.getVersion();
  private String _copyright = Version.getCopyRight();
  private String _comments = StringLocalizer.keyToString("GUI_ABOUT_BOX_COMMENT_KEY");
  private JPanel _emptyBufferPanel = new JPanel();
  private FlowLayout _imageFlowLayout = new FlowLayout();

  /**
   * @author Andy Mechtenberg
   */
  public AxiAboutBox(Frame frame, String appName,
                         String optionalMessage,
                         ImageIcon icon)
  {
    super(frame);
    _product = appName;

    _version = StringLocalizer.keyToString("GUI_ABOUT_BOX_VERSION_KEY") + ": " + Version.getVersion();
    _appMessage = optionalMessage;
    // override the default image icon if it's passed in.
    if (icon != null)
      _imageIcon = icon;

    jbInit();

    pack();
  }

  /**
   * @author Andy Mechtenberg
   */
  public AxiAboutBox( Frame parent, String appName, String optionalMessage )
  {
    this(parent, appName, optionalMessage, null);
  }

  /**
   * @author Andy Mechtenberg
   */
  void jbInit()
  {
    this.setTitle(StringLocalizer.keyToString("GUI_ABOUT_BOX_TITLE_KEY"));
    setResizable(false);
    _mainPanel.setLayout(_mainBorderLayout);
    _centerPanel.setLayout(_centerBorderLayout);
    _imagePanel.setLayout(_imageFlowLayout);
    _imagePanel.setBorder(new EmptyBorder(10, 10, 10, 10));
    _labelGridLayout.setRows(4);
    _labelGridLayout.setColumns(1);
    _titleLabel.setText(_product);
    _versionLabel.setText(_version);
    _copyrightLabel.setText(_copyright);
    _axiLabel.setText(_comments);
    _labelPanel.setLayout(_labelGridLayout);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.setMnemonic(StringLocalizer.keyToString("GUI_OK_BUTTON_MNEMONIC_KEY").charAt(0));
//    _okButton.setMnemonic(MainUIMenuBar.getGenericMnemonic(_okButton));
    _okButton.addActionListener(this);
    _imageFlowLayout.setHgap(0);
    _imageFlowLayout.setVgap(0);
    _imagePanel.add(_axiImageLabel, null);
    _centerPanel.add(_imagePanel, BorderLayout.WEST);
    this.getContentPane().add(_mainPanel, null);
    _labelPanel.add(_titleLabel, null);
    _labelPanel.add(_versionLabel, null);
    _labelPanel.add(_copyrightLabel, null);
    _labelPanel.add(_axiLabel, null);
    if (_appMessage != null)
      _labelPanel.add(_appMessage, null);
    _centerPanel.add(_emptyBufferPanel, BorderLayout.EAST);
    _centerPanel.add(_labelPanel, BorderLayout.CENTER);
    _buttonPanel.add(_okButton, null);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    _mainPanel.add(_centerPanel, BorderLayout.CENTER);
    _axiImageLabel.setIcon(_imageIcon);
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if(e.getID() == WindowEvent.WINDOW_CLOSING)
    {
      cancel();
    }
    super.processWindowEvent(e);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void cancel()
  {
    dispose();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void actionPerformed(ActionEvent e)
  {
    if(e.getSource() == _okButton)
    {
      cancel();
    }
  }
}
