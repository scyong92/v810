package com.axi.v810.gui;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.util.*;

/**
 * <p>Title: JointResultsEntryComparator</p>
 * <p>Description: This comparator will sort a JointResultsEntry object
 * by the desired field name</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Agilent Technogies</p>
 * @author George Booth
 * @author Rex Shang
 */
public class JointResultsEntryComparator implements Comparator<JointResultsEntry>
{
  private boolean _ascending;
  private JointResultsEntryComparatorEnum _field;
  private MeasurementEnum _measurementType;
  private SliceNameEnum _sliceType;

  private AlphaNumericComparator _alphaNumericComparator = null;

  private final static String _TOP_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_TOP_LABEL_KEY");
  private final static String _BOTTOM_SIDE = StringLocalizer.keyToString("CAD_BOARD_SIDE_BOTTOM_LABEL_KEY");
  private final static String _PASS = "pass";
  private final static String _FAIL = "fail";

  /**
   * @author George Booth
   */
  public JointResultsEntryComparator(boolean ascending, JointResultsEntryComparatorEnum field)
  {
    this(ascending, field, null, null);
  }

  /**
   * @author Rex Shang
   */
  public JointResultsEntryComparator(boolean ascending, JointResultsEntryComparatorEnum field,
                                MeasurementEnum measurementType, SliceNameEnum sliceType)
  {
    _ascending = ascending;
    _field = field;

    _measurementType = measurementType;
    _sliceType = sliceType;

    // If we are asked to perform sort on measurement value, we better know
    // which value should be used.
    if (field.equals(JointResultsEntryComparatorEnum.MEASUREMENT_VALUE))
    {
      Assert.expect(_measurementType != null, "Measurement type is null.");
      Assert.expect(_sliceType != null, "Slice name is null.");
    }

    _alphaNumericComparator = new AlphaNumericComparator(ascending);
  }

  /**
   * @param lhResult lefthand JointsResultEntry
   * @param rhResult righthand JointsResultEntry
   * @return negative if lf less than rh, zero if equal, positive if greater than
   * @author George Booth
   * @author Rex Shang
   */
  public int compare(JointResultsEntry lhResult, JointResultsEntry rhResult)
  {
    Assert.expect(lhResult != null);
    Assert.expect(rhResult != null);

    String lhName = null;
    String rhName = null;

    if (_field.equals(JointResultsEntryComparatorEnum.RUN_ID))
    {
      Long lhValue = new Long(lhResult.getRunStartTime());
      Long rhValue = new Long(rhResult.getRunStartTime());
      if (_ascending)
        return lhValue.compareTo(rhValue);
      else
        return rhValue.compareTo(lhValue);
    }
    else if (_field.equals(JointResultsEntryComparatorEnum.BOARD_NAME)) {
      lhName = lhResult.getBoardName();
      rhName = rhResult.getBoardName();
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else if (_field.equals(JointResultsEntryComparatorEnum.COMPONENT_NAME))
    {
      lhName = lhResult.getReferenceDesignator();
      rhName = rhResult.getReferenceDesignator();
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else if (_field.equals(JointResultsEntryComparatorEnum.PIN_NAME))
    {
      lhName = lhResult.getPadName();
      rhName = rhResult.getPadName();
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else if (_field.equals(JointResultsEntryComparatorEnum.COMP_PIN_NAME))
    {
      lhName = lhResult.getReferenceDesignator() + "-" + lhResult.getPadName();
      rhName = rhResult.getReferenceDesignator() + "-" + rhResult.getPadName();
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else if (_field.equals(JointResultsEntryComparatorEnum.JOINT_TYPE))
    {
      lhName = lhResult.getJointTypeName();
      rhName = rhResult.getJointTypeName();
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else if (_field.equals(JointResultsEntryComparatorEnum.SUBTYPE))
    {
      lhName = lhResult.getSubtypeName();
      rhName = rhResult.getSubtypeName();
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else if (_field.equals(JointResultsEntryComparatorEnum.PASS_FAIL))
    {
      lhName = lhResult.passed() ? _PASS : _FAIL;
      rhName = rhResult.passed() ? _PASS : _FAIL;
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else if (_field.equals(JointResultsEntryComparatorEnum.SIDE))
    {
      lhName = lhResult.isTopSide() ? _TOP_SIDE : _BOTTOM_SIDE;
      rhName = rhResult.isTopSide() ? _TOP_SIDE : _BOTTOM_SIDE;
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else if (_field.equals(JointResultsEntryComparatorEnum.NUM_DEFECTS))
    {
      Integer lhValue = new Integer(lhResult.getNumberOfDefects());
      Integer rhValue = new Integer(rhResult.getNumberOfDefects());
      if (_ascending)
        return lhValue.compareTo(rhValue);
      else
        return rhValue.compareTo(lhValue);
    }
    else if (_field.equals(JointResultsEntryComparatorEnum.MEASUREMENT_VALUE))
    {
      // Find the measurement result specified by measurement type and slice type.
      Double lhValue = getMeasurementValue(lhResult);
      Double rhValue = getMeasurementValue(rhResult);

      if (_ascending)
        return lhValue.compareTo(rhValue);
      else
        return rhValue.compareTo(lhValue);
    }
    else if (_field.equals(JointResultsEntryComparatorEnum.DEFAULT))
    {
      lhName = lhResult.getReferenceDesignator() + "-" + lhResult.getPadName()
               + "-" + lhResult.getBoardName() + "-" + lhResult.getRunStartTime();
      rhName = rhResult.getReferenceDesignator() + "-" + rhResult.getPadName()
               + "-" + rhResult.getBoardName() + "-" + rhResult.getRunStartTime();
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else
    {
       Assert.expect(false, "unsupported comparator field enum");
       return 0;
    }
  }

  /**
   * Find the measurement result specified by measurement type and slice type.
   * @author Rex Shang
   */
  private Double getMeasurementValue(JointResultsEntry jointResultEntry)
  {
    List<MeasurementResult> measurementResults = jointResultEntry.getAllMeasurementResults();
    for (MeasurementResult measurementResult : measurementResults)
    {
      if (measurementResult.getMeasurementEnum().equals(_measurementType)
          && measurementResult.getSliceNameEnum().equals(_sliceType))
        return new Double(measurementResult.getValue());
    }
    return new Double(Double.NaN);
  }

}
