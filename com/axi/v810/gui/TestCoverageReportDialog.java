package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.*;
/**
 *
 * @author chin-seong.kee
 */
public class TestCoverageReportDialog extends EscapeDialog{

  private String[] _fileExtStrings = { String.format("%-30s", ".txt"), String.format("%-30s", ".csv") };

  private final int TEXT_FILE_EXTENSION_REPORT = 0;
  private final int CSV_FILE_EXTENSION_REPORT = 1;

  private String _initialProjectName;
  private JTextField _inputTextField;
  //private JCheckBox _saveImageSetsCheckBox;
  private JTextField _destinationTextField = new JTextField();
  private JButton _findDestinationButton = new JButton();
  private ButtonGroup group = new ButtonGroup();
  private JComboBox _coverageReportFileExt = new JComboBox(_fileExtStrings);
  //private JRadioButton _txtRdoButton = new JRadioButton(String.format("%-30s", ".txt"), true);
  //private JRadioButton _csvRdoButton = new JRadioButton(String.format("%-30s", ".csv"), false);
  private JFileChooser _fileChooser = null;
 
  private String _newTestCoverageReportName = null;
  private String _newTestCoverageReportPath = null;
  private String _newTestCoverageReportExt = null;
  private boolean _isTxtFile = false;
  // default destination directory..
  private String _defaultDestinationDirectoryText = Directory.getProjectsDir();

  /**
   * @author Chin-Seong, Kee
   */
  public TestCoverageReportDialog(JFrame parent, String title, String initialProjectName)
  {
    super(parent, title, true);
    Assert.expect(parent != null);
    Assert.expect(title != null);
    Assert.expect(initialProjectName != null);

    _initialProjectName = initialProjectName;

    jbInit();
    pack();
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void jbInit()
  {
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    setResizable(false);  // do this so no frame icon is displayed
    JButton okButton = new JButton(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    JButton cancelButton = new JButton(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    okButton.setMargin(new Insets(2, 8, 2, 8));
    cancelButton.setMargin(new Insets(2, 8, 2, 8));
    okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed();
      }
    });
    cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });

    JPanel mainPanel = new JPanel(new BorderLayout(0, 5));
    JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    JPanel innerButtonPanel = new JPanel(new GridLayout(1, 2, 10, 0));
    innerButtonPanel.add(okButton);
    innerButtonPanel.add(cancelButton);
    buttonPanel.add(innerButtonPanel);

    Icon infoIcon = UIManager.getIcon("OptionPane.questionIcon");
    JLabel iconLabel = new JLabel(infoIcon);

    JPanel centerPanel = new JPanel(new BorderLayout(0, 5));
    centerPanel.add(new JLabel(StringLocalizer.keyToString("GUI_SAVE_AS_PROMPT_KEY")), BorderLayout.NORTH);
    JPanel inputReportFilePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    inputReportFilePanel.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
    JLabel inputReportFileLabel = new JLabel(StringLocalizer.keyToString("TEST_COVERAGE_REPORT_FILE_NAME_LABEL_KEY"));
    inputReportFilePanel.add(inputReportFileLabel);
    _inputTextField = new JTextField(_initialProjectName, 23);
    _inputTextField.selectAll();
    _inputTextField.setEditable(true);
    inputReportFilePanel.add(_inputTextField, BorderLayout.CENTER);

    JPanel destinationPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    destinationPanel.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
    JLabel destinationLabel = new JLabel(StringLocalizer.keyToString("TEST_COVERAGE_REPORT_DESTINATION_LABEL_KEY"));
    destinationPanel.add(destinationLabel);
    _destinationTextField.setColumns(23);
    _destinationTextField.setEditable(true);
    _destinationTextField.setText(_defaultDestinationDirectoryText);
    _findDestinationButton.setText("...");
    destinationPanel.add(_destinationTextField);
    destinationPanel.add(_findDestinationButton);

    JPanel testCoverageReportOption = new JPanel(new FlowLayout(FlowLayout.LEFT));
    testCoverageReportOption.setBorder(BorderFactory.createEmptyBorder(2, 5, 5, 5));

    JLabel fileTypeLabel = new JLabel(String.format("%-12s", StringLocalizer.keyToString("TEST_COVERAGE_REPORT_FILE_TYPE_OPTION_KEY")));

    _coverageReportFileExt.setSelectedIndex(TEXT_FILE_EXTENSION_REPORT);
    testCoverageReportOption.add(fileTypeLabel);
    testCoverageReportOption.add(_coverageReportFileExt);
    //group.add(_txtRdoButton);
    //group.add(_csvRdoButton);
    //testCoverageReportOption.add(_txtRdoButton);
    //testCoverageReportOption.add(_csvRdoButton);

    centerPanel.add(inputReportFilePanel, BorderLayout.NORTH);
    centerPanel.add(destinationPanel, BorderLayout.CENTER);
    centerPanel.add(testCoverageReportOption, BorderLayout.SOUTH);

    JLabel[] allLabels = new JLabel[]{inputReportFileLabel,
                                     destinationLabel};

    SwingUtils.makeAllSameLength(allLabels);


    mainPanel.add(buttonPanel, BorderLayout.SOUTH);
    mainPanel.add(iconLabel, BorderLayout.WEST);
    mainPanel.add(centerPanel, BorderLayout.CENTER);
    mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 5, 10));
    iconLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
    this.getContentPane().add(mainPanel, BorderLayout.CENTER);
    this.getRootPane().setDefaultButton(okButton);

    _findDestinationButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        findDirectoryButton_actionPerformed();
      }
    });
  }

 /**
   * @author Chin-Seong, Kee
   */
  private void okButton_actionPerformed()
  {
    /*if(_txtRdoButton.isSelected()){
        _newTestCoverageReportExt = ".txt";
        _isTxtFile = true;
    }
    else{
        _newTestCoverageReportExt = ".csv";
        _isTxtFile = false;
    }*/
    if(_coverageReportFileExt.getSelectedIndex() == TEXT_FILE_EXTENSION_REPORT)
        _isTxtFile = true;
    else
        _isTxtFile = false;

    _newTestCoverageReportExt = _coverageReportFileExt.getSelectedItem().toString();

    
    _newTestCoverageReportName = _inputTextField.getText();
    _newTestCoverageReportPath = _destinationTextField.getText();
    dispose();
  }

  public boolean isTxtFile()
  {
      return _isTxtFile;
  }

 /**
   * @author Chin-Seong, Kee
   */
  public String getReportExt()
  {
      return _newTestCoverageReportExt;
  }

  /**
   * @author Chin-Seong, Kee
   */
  public String getReportName()
  {
      return _newTestCoverageReportPath;
  }

  /**
   * @author Chin-Seong, Kee
   */
  public String getReportPath()
  {
    return _newTestCoverageReportPath +  "\\" + _newTestCoverageReportName;
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void findDirectoryButton_actionPerformed()
  {
    if (_fileChooser == null)
    {
      _fileChooser = new JFileChooser()
      {
        public void approveSelection()
        {
          File selectedFile = getSelectedFile();
          if (selectedFile.exists())
            super.approveSelection();
          else
          {
            String message = StringLocalizer.keyToString(new LocalizedString("FILE_DIALOG_SELECTION_DOES_NOT_EXIST_KEY", new Object[]
                {selectedFile}));
            JOptionPane.showMessageDialog(this, StringUtil.format(message, 50));
          }
        }
      };
    }
    _fileChooser.setApproveButtonText(StringLocalizer.keyToString("ZIP_PROEJECT_WIZARD_SELECT_BUTTON_KEY"));
    _fileChooser.setCurrentDirectory(new File(_defaultDestinationDirectoryText));
    _fileChooser.setDialogTitle(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_SELECT_DESTINATION_LABEL_KEY"));
    _fileChooser.setDialogType(JFileChooser.CUSTOM_DIALOG);
    _fileChooser.setMultiSelectionEnabled(false);
    _fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

    int returnValue = _fileChooser.showDialog(this, StringLocalizer.keyToString("ZIP_PROEJECT_WIZARD_SELECT_BUTTON_KEY"));
    if (returnValue == JFileChooser.APPROVE_OPTION)
    {
      // the user has selected a new directory.
      File selectedDirectory = _fileChooser.getSelectedFile();
      Assert.expect(selectedDirectory.isDirectory());

      String zipDestination = selectedDirectory.getAbsolutePath(); // + File.separator + _archiver.getProjectName() + FileName.getZipFileExtension();
      _defaultDestinationDirectoryText = zipDestination;
      _destinationTextField.setText(zipDestination);
    }
  }
}


