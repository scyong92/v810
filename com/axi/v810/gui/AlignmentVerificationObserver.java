package com.axi.v810.gui;

import java.awt.image.*;
import java.io.*;
import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.imagePane.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class AlignmentVerificationObserver implements Observer
{
  /**
   * @author Andy Mechtenberg
   */
  public synchronized void update(Observable observable, final Object object)
  {
    // This is outside of the run to keep track of the fact that this method may use
    // any images on the Inspection Event
    if (object instanceof InspectionEvent)
    {
      InspectionEvent event = (InspectionEvent)object;
      event.incrementReferenceCount();
    }

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          InspectionEvent inspectionEvent = (InspectionEvent)object;
          InspectionEventEnum eventEnum = inspectionEvent.getInspectionEventEnum();
          if (inspectionEvent instanceof AlignmentInspectionEvent)
          {
            AlignmentInspectionEvent alignmentInspectionEvent = (AlignmentInspectionEvent)inspectionEvent;
            if (eventEnum.equals(InspectionEventEnum.ALIGNED_ON_IMAGE))
            {
              GraphicsEngine graphicsEngine = new GraphicsEngine(5);
              Image alignmentImage = alignmentInspectionEvent.getImage();
              graphicsEngine.addRenderer(1, new InspectionImageRenderer(alignmentImage));

              MeasurementRegionDiagnosticInfo locatedAlignmentPadRegionsDiagnosticInfo =
                  alignmentInspectionEvent.getLocatedAlignmentFeaturesDiagnosticInfo();
              java.awt.Shape locatedAlignmentPadRegionsShape = locatedAlignmentPadRegionsDiagnosticInfo.
                  getDiagnosticRegion();
              graphicsEngine.addRenderer(2, new RegionOfInterestRenderer(locatedAlignmentPadRegionsShape, 3));
              graphicsEngine.setForeground(2, LayerColorEnum.LOCATED_ALIGNMENT_PAD_COLOR.getColor());
              graphicsEngine.setAxisInterpretation(true, true);

              BufferedImage image = graphicsEngine.createImage(0);
              try
              {
                AlignmentGroup alignmentGroup = alignmentInspectionEvent.getAlignmentGroup();
                boolean isSecondHalfOfLongPanel = false;
                Project project = Project.getCurrentlyLoadedProject();
                if (project.getTestProgram().getTestSubProgram(alignmentGroup, alignmentInspectionEvent.getMagnificationType()).getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
                  isSecondHalfOfLongPanel = true;
                String groupName = alignmentGroup.getName();
                XrayImageIoUtil.savePngImage(image,
                                             FileName.getAlignmentVerificationImageFullPath(alignmentInspectionEvent.getProject().getName(), groupName, isSecondHalfOfLongPanel));
                //sham, 2012-05-14, Alignment Enhancement
                //save aligned alignment image to alignment log
                String imagesDir = Directory.getAlignmentLogDir();
                if (Config.isDeveloperDebugModeOn())
                {
                  String imageFileName = String.valueOf(System.currentTimeMillis()) + "_Alignment_Image_" + groupName + ".png";
                  XrayImageIoUtil.savePngImage(image,
                                              imagesDir + File.separator + imageFileName);
                }
              }
              catch (XrayTesterException ex)
              {
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                              ex,
                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                              true);
              }
            }
            //Khaw Chek Hau - XCR2285: 2D Alignment on v810
            else if (eventEnum.equals(InspectionEventEnum.ALIGNED_ON_2D_IMAGE))
            {
              GraphicsEngine graphicsEngine = new GraphicsEngine(5);
              Image alignmentImage = alignmentInspectionEvent.getImage();
              graphicsEngine.addRenderer(1, new InspectionImageRenderer(alignmentImage));
              graphicsEngine.setAxisInterpretation(true, true);

              BufferedImage image = graphicsEngine.createImage(0);
              
              try
              {
                AlignmentGroup alignmentGroup = alignmentInspectionEvent.getAlignmentGroup();
                boolean isSecondHalfOfLongPanel = false;
                Project project = Project.getCurrentlyLoadedProject();
                if (project.getTestProgram().getTestSubProgram(alignmentGroup, alignmentInspectionEvent.getMagnificationType()).getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
                  isSecondHalfOfLongPanel = true;
                String groupName = alignmentGroup.getName();
                
                XrayImageIoUtil.savePngImage(image,
                                             FileName.getAlignmentVerificationImageFullPath(project.getName(), groupName, isSecondHalfOfLongPanel));
                //sham, 2012-05-14, Alignment Enhancement
                //save aligned alignment image to alignment log
                String imagesDir = Directory.getAlignmentLogDir();
                if (Config.isDeveloperDebugModeOn())
                {
                  String imageFileName = String.valueOf(System.currentTimeMillis()) + "_Alignment_Image_" + groupName + ".png";
                  XrayImageIoUtil.savePngImage(image,
                                              imagesDir + File.separator + imageFileName);
                }
              }
              catch (XrayTesterException ex)
              {
                MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                              ex,
                                              StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                              true);
              }
            }
          }
        }
        finally
        {
          if (object instanceof InspectionEvent)
          {
            InspectionEvent event = (InspectionEvent)object;
            event.decrementReferenceCount();
          }
        }
      }
    });
  }
}
