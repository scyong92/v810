package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

import com.axi.guiUtil.wizard.*;
import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This panel will allow the user to select the project they would like to zip along with the destination to place
 * the zip at. This panel is the first panel in the zip project wizard.
 *
 * @author Erica Wheatcroft
 */
public class ZipProjectWizardSelectProjectAndDestinationPanel extends WizardPanel
{
  private JLabel _instructionLabel = new JLabel();
  private JList _availableProjectList = new JList();
  private JLabel _destinationLabel = new JLabel();
  private JLabel _zipLabel;
  private JButton _findDestinationButton = new JButton();
  private JTextField _destinationTextField = new JTextField();
  private DefaultListModel _projectNamesListModel = new DefaultListModel();
  private JFileChooser _fileChooser = null;
  private boolean _inUse = false;

  // default destination directory..
  private String _defaultDestinationDirectoryText = Directory.getProjectsDir();

  // reference to the archiver
  private ProjectArchiver _archiver = null;

  // reference to the parent
  private WizardDialog _dialog = null;

  /**
   * @author Erica Wheatcroft
   */
  public ZipProjectWizardSelectProjectAndDestinationPanel(WizardDialog dialog, ProjectArchiver archiver)
  {
    super(dialog);

    Assert.expect(archiver != null);
    Assert.expect(dialog != null);

    _dialog = dialog;
    _archiver = archiver;

    createPanel();
  }


  /**
   * @author Erica Wheatcroft
   */
  public void populatePanelWithData()
  {
    Assert.expect(_projectNamesListModel != null);

    // set the title
    _dialog.changeTitle(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_PANEL1_TITLE_KEY"));

    // if the archiver already has a value set then lets use that
    if(_archiver.hasProjectName())
    {
      String currentSelectedProject = _archiver.getProjectName();
      // lets populate the list of projects.
      java.util.List<String> projectNames = FileName.getProjectNames();

      int indexToSelect = 0;

      _projectNamesListModel.clear();

      for (int i = 0; i < projectNames.size(); i++)
      {
        String projectName = projectNames.get(i);
        _projectNamesListModel.addElement(projectName);
        if (currentSelectedProject != null)
        {
          if (currentSelectedProject.equalsIgnoreCase(projectName))
            indexToSelect = i;
        }
      }

      _availableProjectList.setSelectedIndex(indexToSelect);
      ((ZipProjectWizardDialog)_dialog).setRepopulatePanels(true);
    }
    else
    {
      // lets populate the list of projects.
      java.util.List<String> projectNames = FileName.getProjectNames();

      int indexToSelect = 0;

      String currentLoadedProjectName = null;
      _projectNamesListModel.clear();

      if (Project.isCurrentProjectLoaded())
        currentLoadedProjectName = Project.getCurrentlyLoadedProject().getName();

      for (int i = 0; i < projectNames.size(); i++)
      {
        String projectName = projectNames.get(i);
        _projectNamesListModel.addElement(projectName);
        if (currentLoadedProjectName != null)
        {
          if (currentLoadedProjectName.equalsIgnoreCase(projectName))
            indexToSelect = i;
        }
      }

      // now if a project is loaded then select it if not then don't highlight anything.
      if (currentLoadedProjectName != null)
      {
        Assert.expect(currentLoadedProjectName != null);
        _zipLabel.setText(currentLoadedProjectName + FileName.getZipFileExtension());
        _archiver.setProjectName(currentLoadedProjectName);
        _availableProjectList.setSelectedIndex(indexToSelect);
      }
//      else
//      {
//        // since they have nothing selected then don't let them select a destination yet
//        _destinationTextField.setText("");
//        _findDestinationButton.setEnabled(false);
//      }
      // build up our default destination directory.
      String zipName = _defaultDestinationDirectoryText;// + File.separator + currentLoadedProjectName + FileName.getZipFileExtension();
      _destinationTextField.setText(zipName);
      _archiver.setDestinationFilename(zipName + File.separator + currentLoadedProjectName + FileName.getZipFileExtension());
      ((ZipProjectWizardDialog)_dialog).setRepopulatePanels(true);

    }

    // now call validate panel to set what the naviagtion buttons should be set to.
    validatePanel();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    // do nothing
  }

  /**
   * @author Erica Wheatcroft
   */
  public void removeObservers()
  {
    // do nothing since this panel does not deal with observers.
  }

  /**
   * The rules for this panel are checked by this method. If not meet the next button will not be enabled.
   * @author Erica Wheatcroft
   */
  public void validatePanel()
  {
    // in order for the panel to be valid we need to make sure hte user has selected a project and a destination.
    if(_archiver.hasProjectName() && _archiver.hasDestinationFilename())
    {
      // next can be set to enabled, cancel can be enabled, back is disabled, and generate is disabled.
      WizardButtonState buttonState = new WizardButtonState(true, true, false, false);
      _observable.updateButtonState(buttonState);
    }

  }

  /**
   * @author Erica Wheatcroft
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null, "Name of panel is null");
    Assert.expect(name.length() > 0, "Name of panel is an empty string");
    _name = name;
  }

  /**
   * @author Erica Wheatcroft
   */
  public String getWizardName()
  {
    Assert.expect(_name != null, "Name of panel is not set");
    return _name;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setLayout(new BorderLayout(5,15));
    setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

    // lets create the instruction label panel
    JPanel instructionPanel = new JPanel(new FlowLayout());
    _instructionLabel.setFont(FontUtil.getBoldFont(_instructionLabel.getFont(),Localization.getLocale()));
    _instructionLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_PANEL1_INSTRUCTION_KEY"));
    instructionPanel.add(_instructionLabel);
    add(instructionPanel, BorderLayout.NORTH);

    // now lets create the available projects panel
    JPanel availableProjectsPanel = new JPanel(new BorderLayout());
    availableProjectsPanel.setBorder(BorderFactory.createEmptyBorder(5,20,5,20));
    JScrollPane projectListScrollPane = new JScrollPane();
    projectListScrollPane.getViewport().add(_availableProjectList);
    _availableProjectList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _availableProjectList.setVisibleRowCount(10);
    _availableProjectList.setModel(_projectNamesListModel);
    availableProjectsPanel.add(projectListScrollPane, BorderLayout.CENTER);
    add(availableProjectsPanel, BorderLayout.CENTER);

    // create the destination panel
//    JPanel destinationPanel = new JPanel(new FlowLayout());
    JPanel destinationPanel = new JPanel(new BorderLayout());
    JPanel directoryPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel zipNamePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    destinationPanel.setBorder(BorderFactory.createEmptyBorder(10, 15, 10, 15));
    _destinationLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_DESTINATION_LABEL_KEY"));
    _destinationLabel.setFont(FontUtil.getBoldFont(_destinationLabel.getFont(),Localization.getLocale()));
    _findDestinationButton.setText("...");
    _destinationTextField.setColumns(25);
    _destinationTextField.setEditable(true);
    directoryPanel.add(_destinationLabel);
    directoryPanel.add(_destinationTextField);
    directoryPanel.add(_findDestinationButton);
    destinationPanel.add(directoryPanel, BorderLayout.NORTH);


    JLabel zipNameLabel = new JLabel(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_NAME_LABEL_KEY") + " ");
    zipNameLabel.setFont(FontUtil.getBoldFont(zipNameLabel.getFont(),Localization.getLocale()));

    JLabel[] allLabels = new JLabel[]{zipNameLabel,
                                     _destinationLabel};

    SwingUtils.makeAllSameLength(allLabels);

    _zipLabel = new JLabel();
    zipNamePanel.add(zipNameLabel);
    zipNamePanel.add(_zipLabel);
    destinationPanel.add(zipNamePanel, BorderLayout.SOUTH);
    add(destinationPanel, BorderLayout.SOUTH);

    _availableProjectList.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        listSelectionChanged(e);
      }
    });

    _findDestinationButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        findDirectoryButton_actionPerformed();
      }
    });

    _destinationTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        destinationTextField_setValue();
      }
    });
    _destinationTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        destinationTextField_setValue();
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void listSelectionChanged(ListSelectionEvent e)
  {
    if (e.getValueIsAdjusting() == false)
    {
      String selectedProjectName = (String)_availableProjectList.getSelectedValue();
      if (selectedProjectName != null)
      {
        if (_archiver.hasProjectName())
        {
          if (_archiver.getProjectName().equalsIgnoreCase(selectedProjectName) == false)
          {
            _archiver.clear();
            _archiver.setProjectName(selectedProjectName);
            ((ZipProjectWizardDialog)_dialog).clearOverwriteFileFlag();
            ((ZipProjectWizardDialog)_dialog).setRepopulatePanels(true);
          }
          else
            ((ZipProjectWizardDialog)_dialog).setRepopulatePanels(false);
        }
        else
        {
          _archiver.setProjectName(selectedProjectName);
          ((ZipProjectWizardDialog)_dialog).setRepopulatePanels(true);
        }

        // now if the list has something selected build up our default destination directory.
//            String zipName = _defaultDestinationDirectoryText;// + File.separator + selectedProjectName + FileName.getZipFileExtension();
//            _destinationTextField.setText(zipName);
        _zipLabel.setText(selectedProjectName + FileName.getZipFileExtension());
        _archiver.setDestinationFilename(_defaultDestinationDirectoryText + File.separator + selectedProjectName + FileName.getZipFileExtension());
        _findDestinationButton.setEnabled(true);
        validatePanel();
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void findDirectoryButton_actionPerformed()
  {
    if (_fileChooser == null)
    {
      _fileChooser = new JFileChooser()
      {
        public void approveSelection()
        {
          File selectedFile = getSelectedFile();
          if (selectedFile.exists())
            super.approveSelection();
          else
          {
            String message = StringLocalizer.keyToString(new LocalizedString("FILE_DIALOG_SELECTION_DOES_NOT_EXIST_KEY", new Object[]
                {selectedFile}));
            JOptionPane.showMessageDialog(this, StringUtil.format(message, 50));
          }
        }
      };
    }
    _fileChooser.setApproveButtonText(StringLocalizer.keyToString("ZIP_PROEJECT_WIZARD_SELECT_BUTTON_KEY"));
    _fileChooser.setCurrentDirectory(new File(_defaultDestinationDirectoryText));
    _fileChooser.setDialogTitle(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_SELECT_DESTINATION_LABEL_KEY"));
    _fileChooser.setDialogType(JFileChooser.CUSTOM_DIALOG);
    _fileChooser.setMultiSelectionEnabled(false);
    _fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

    int returnValue = _fileChooser.showDialog(_dialog, StringLocalizer.keyToString("ZIP_PROEJECT_WIZARD_SELECT_BUTTON_KEY"));
    if (returnValue == JFileChooser.APPROVE_OPTION)
    {
      // the user has selected a new directory.
      File selectedDirectory = _fileChooser.getSelectedFile();
      Assert.expect(selectedDirectory.isDirectory());

      String zipDestination = selectedDirectory.getAbsolutePath(); // + File.separator + _archiver.getProjectName() + FileName.getZipFileExtension();
      _defaultDestinationDirectoryText = zipDestination;
      _destinationTextField.setText(zipDestination);
      if (_archiver.hasProjectName())
      {
        _zipLabel.setText(_archiver.getProjectName() + FileName.getZipFileExtension());
        _archiver.setDestinationFilename(zipDestination + File.separator + _archiver.getProjectName() + FileName.getZipFileExtension());
      }
      validatePanel();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void destinationTextField_setValue()
  {
    if (_inUse)
      return;
    _inUse = true;

    String fullPath = _destinationTextField.getText();
    fullPath = Directory.shortenPath(fullPath);
    if (isPathValid(fullPath))
    {
      _defaultDestinationDirectoryText = fullPath;
      _destinationTextField.setText(fullPath);
      if (_archiver.hasProjectName())
      {
        _zipLabel.setText(_archiver.getProjectName() + FileName.getZipFileExtension());
        _archiver.setDestinationFilename(_defaultDestinationDirectoryText + File.separator + _archiver.getProjectName() + FileName.getZipFileExtension());
      }
      validatePanel();
    }
    else
    {
      _destinationTextField.setText(_defaultDestinationDirectoryText);
    }
    _inUse = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  private boolean isPathValid(String fullPath)
  {
    if (FileUtilAxi.existsDirectoryAndAbleToReadWrite(fullPath))
      return true;
    else if (FileUtilAxi.existsDirectory(fullPath) == false)
    {
      LocalizedString message = new LocalizedString("CFGUI_DIRECTORY_DOES_NOT_EXIST_KEY", new Object[]
          {fullPath});
      int answer = JOptionPane.showConfirmDialog(this,
                                                 StringLocalizer.keyToString(message),
                                                 StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                                 JOptionPane.YES_NO_OPTION);
      if (answer == JOptionPane.YES_OPTION)
      {
        try
        {
          FileUtilAxi.createDirectory(fullPath);
          return true;
        }
        catch (DatastoreException ex)
        {
          MessageDialog.showErrorDialog(this,
                                        ex,
                                        StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                        true);
          return false;
        }
      }
      else
        return false;
    }
    else
    {
      // directory exists, but not read/writable
      LocalizedString message = new LocalizedString("CFGUI_DIRECTORY_NO_PERMISSIONS_KEY", new Object[]
          {fullPath});
      MessageDialog.showErrorDialog(this,
                                    StringLocalizer.keyToString(message),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    true);
      return false;

    }
  }

}
