package com.axi.v810.gui;

import java.util.*;

import com.axi.util.*;

/**
 * This class is used to notify any Observers that the GUI is finished updating itself as a result
 * of a Command being executed.  Every Command executed must somehow cause a call into guiUpdateFinished()
 * in this class for the undo() and redo() functionality to work properly.  Currently all
 * GuiCommand classes automatically call  guiUpdateObservable().  All ProjectCommand classes rely
 * on the PanelGraphicsEngineSetup update() method to call GuiObservable after the Cad graphics
 * screen has updated the graphics.
 *
 * @see com.axi.v810.gui.undo.DataCommand
 * @see com.axi.v810.gui.undo.GuiCommand
 * @see com.axi.v810.gui.drawCad.PanelGraphicsEngineSetup
 *
 * @author George A. David
 */
public class GuiObservable extends Observable
{
  private static GuiObservable _instance = null;

  /**
   * @author George A. David
   */
  private GuiObservable()
  {
    // do nothing
  }

  /**
   * @return an instance of GuiUpdateObservable
   * @author George A. David
   */
  public static synchronized GuiObservable getInstance()
  {
    if (_instance == null)
      _instance = new GuiObservable();

    return _instance;
  }

  /**
   * notify all observers that the gui has been updated
   * @author George A. David
   */
  public void stateChanged(Object source, GuiEventEnum guiEventEnum)
  {
    // NOTE - not checking for null on the source object. Some state changes might not have an object to send..
    // i.e. The Screen Change Event Enum.
    Assert.expect(guiEventEnum != null);

    setChanged();
    notifyObservers(new GuiEvent(source, guiEventEnum));
  }
}

