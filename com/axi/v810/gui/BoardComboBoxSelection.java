package com.axi.v810.gui;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

import java.util.*;

/**
 * This class represents one entry in a board selection combox box found in some of our GUIs. THis class contains a
 * list of boards. If the list has more than entry then that means that this entry is the "Entire Panel" entry.
 * Otherwise this class will only have entry and that will be the specific board.
 *
 * This class was created so that we could store objects in combo boxs instead of just strings.
 *
 * @author Erica Wheatcroft
 */
public class BoardComboBoxSelection
{
  private List<Board> _availableBoards = new LinkedList<Board>();
  private static final String _allBoards = "Entire Panel";

  /**
   * @author Erica Wheatcroft
   */
  public BoardComboBoxSelection(List<Board> boards)
  {
    Assert.expect(boards != null);
    _availableBoards.addAll(boards);
  }

  /**
   * @return either Entire panel if this selection represents all available boards else the specific board name
   * @author Erica Wheatcroft
   */
  public String toString()
  {
    if(_availableBoards.size() == 1)
      return ( (Board) _availableBoards.get(0)).toString();

    return _allBoards;
  }

  /**
   * @author Erica Wheatcroft
   * @return the list of boards for this specific board combo box selection.
   */
  public List<Board> getBoards()
  {
    Assert.expect(_availableBoards.size() != 0);
    return _availableBoards;
  }
}
