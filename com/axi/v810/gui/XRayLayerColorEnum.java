package com.axi.v810.gui;

import java.awt.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.MeasurementRegionEnum;

/**
 * Standard colors for layers on the GraphicsEngine class.
 *
 * @author Bill Darbie
 */
public class XRayLayerColorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static XRayLayerColorEnum SURFACEMOUNT_PAD_COLOR = new XRayLayerColorEnum(++_index, Color.GRAY);
  public static XRayLayerColorEnum THROUGHHOLE_PAD_COLOR = new XRayLayerColorEnum(++_index, Color.GRAY);
  public static XRayLayerColorEnum FIRST_SURFACEMOUNT_PAD_COLOR = new XRayLayerColorEnum(++_index, new Color(255,105,105));  // a pastel red
  public static XRayLayerColorEnum FIRST_THROUGHHOLE_PAD_COLOR = new XRayLayerColorEnum(++_index, new Color(255,105,105)); // was new LayerColorEnum(++_index, Color.orange);
  public static XRayLayerColorEnum REFERENCE_DESIGNATOR_COLOR = new XRayLayerColorEnum(++_index, Color.RED);
  public static XRayLayerColorEnum TOP_COMPONENT_COLOR = new XRayLayerColorEnum(++_index, Color.CYAN);
  public static XRayLayerColorEnum BOTTOM_COMPONENT_COLOR = new XRayLayerColorEnum(++_index, Color.MAGENTA);
  public static XRayLayerColorEnum BOARD_COLOR = new XRayLayerColorEnum(++_index, Color.LIGHT_GRAY);
  public static XRayLayerColorEnum PANEL_COLOR = new XRayLayerColorEnum(++_index, Color.PINK);
  public static XRayLayerColorEnum FIDUCIAL_COLOR = new XRayLayerColorEnum(++_index, Color.GREEN);
  public static XRayLayerColorEnum BOARD_ORIGIN_COLOR = new XRayLayerColorEnum(++_index, new Color(255, 255, 93));// pale yellow
  public static XRayLayerColorEnum BOARD_NAME_COLOR = new XRayLayerColorEnum(++_index, Color.WHITE);

  public static XRayLayerColorEnum ALIGNMENT_GROUP_1_COLOR = new XRayLayerColorEnum(++_index, new Color(255, 233, 85)); // yellow
  public static XRayLayerColorEnum ALIGNMENT_GROUP_2_COLOR = new XRayLayerColorEnum(++_index, new Color(35, 210, 0));  // green
  public static XRayLayerColorEnum ALIGNMENT_GROUP_3_COLOR = new XRayLayerColorEnum(++_index, new Color(255,128,0));// orange   was a pastel cyan (old color) new Color(0, 183, 200));

  // debug graphics colors
  public static XRayLayerColorEnum IRP_BOUNDARY_COLOR = new XRayLayerColorEnum(++_index, Color.ORANGE);
  public static XRayLayerColorEnum RECONSTRUCTION_REGION_RECTANGLE_COLOR = new XRayLayerColorEnum(++_index, Color.YELLOW);
  public static XRayLayerColorEnum FOCUS_REGION_COLOR = new XRayLayerColorEnum(++_index, Color.BLUE);
  public static XRayLayerColorEnum REGION_PAD_BOUNDS_COLOR = new XRayLayerColorEnum(++_index, Color.red.brighter().brighter());
  public static XRayLayerColorEnum PITCH_COLOR = new XRayLayerColorEnum(++_index, Color.YELLOW);
  public static XRayLayerColorEnum IPD_COLOR = new XRayLayerColorEnum(++_index, new Color(145, 255, 145));  // inter pad distance (IPD) (Greenish)

  public static XRayLayerColorEnum ORIENTATION_ARROW_COLOR = new XRayLayerColorEnum(++_index, new Color(145, 255, 145)); //(Greenish)


  // profile colors
  public static XRayLayerColorEnum PROFILE_BACKGROUND = new XRayLayerColorEnum(++_index, Color.black);
  public static XRayLayerColorEnum PROFILE_AXIS = new XRayLayerColorEnum(++_index, new Color(128, 64, 0)); // brown-reddish
  public static XRayLayerColorEnum PROFILE_TEXT = new XRayLayerColorEnum(++_index, new Color(128, 64, 0)); // brown-reddish

  public static XRayLayerColorEnum VOIDING_PIXELS = new XRayLayerColorEnum(++_index, Color.YELLOW);

  //for alignment image panel
  public static XRayLayerColorEnum ALIGNMENT_PAD_COLOR = new XRayLayerColorEnum(++_index, Color.blue);

  //for veririfation image panel
  public static XRayLayerColorEnum VERIFY_CAD_PAD_COLOR = new XRayLayerColorEnum(++_index, Color.blue);
  public static XRayLayerColorEnum VERIFY_CAD_SELECTED_PAD_COLOR = new XRayLayerColorEnum(++_index, Color.orange);
  public static XRayLayerColorEnum PAD_NAME_COLOR = new XRayLayerColorEnum(++_index, Color.red);
  public static XRayLayerColorEnum VERIFY_CAD_ORIENTATION_ARROW_COLOR = new XRayLayerColorEnum(++_index, Color.yellow);


  private Color _color;

  private static Map<MeasurementRegionEnum, Color> _measurementRegionEnumToColorMap;

  /**
   * @author Patrick Lacz
   */
  private static void intializeMeasurementRegionEnumToColorMap()
  {
    _measurementRegionEnumToColorMap = new TreeMap<MeasurementRegionEnum,Color>();
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.PAD_REGION, Color.CYAN);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.BARREL_REGION, Color.CYAN);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.PIN_REGION, Color.ORANGE);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.PIN_REGION2, Color.BLUE);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.CAD_LOCATION_REGION, new Color(0x3CB371)); // medium sea green
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.LOCATE_SEARCH_REGION, Color.YELLOW);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.LOCATE_MATCH_REGION, Color.MAGENTA);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.GRID_ARRAY_EDGE_SEARCH_REGION, Color.CYAN);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.MEASURED_JOINT_BOUNDARY, Color.MAGENTA);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.BACKGROUND_REGION, Color.WHITE);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.SHORT_BACKGROUND_REGION, Color.WHITE);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.SHORT_PROFILE_START_BIN_REGION, Color.CYAN);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.SHORT_PROFILE_END_BIN_REGION, Color.ORANGE);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.SHORT_PROFILE_DISABLE_REGION, Color.YELLOW);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.PASSING_JOINT_REGION, Color.GREEN);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.FAILING_JOINT_REGION, Color.RED);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.DEFINITE_SHORT_DEFECT_REGION, Color.RED);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QUESTIONABLE_SHORT_DEFECT_REGION, Color.YELLOW);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.EXONERATED_SHORT_DEFECT_REGION, Color.GREEN);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.GULLWING_BACKGROUND_REGION, new Color(0x4169E1)); // royal blue
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.GULLWING_PAD_REGION, new Color(0xF8F8FF)); // ghost white
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.GULLWING_HEEL_EDGE_REGION, new Color(0x8B4513)); // saddle brown
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.GULLWING_HEEL_SEARCH_REGION, new Color(0xDAA520)); // goldenrod
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.GULLWING_HEEL_PEAK_REGION, Color.BLUE);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.GULLWING_CENTER_REGION, Color.MAGENTA);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.GULLWING_TOE_EDGE_REGION, new Color(0x8B4513));  // saddle brown
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.GULLWING_TOE_PEAK_REGION, new Color(0x3CB371)); // medium sea green
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.ADVANCED_GULLWING_MAX_HEEL_SLOPE_REGION, Color.BLACK);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.ADVANCED_GULLWING_MAX_TOE_SLOPE_REGION, Color.BLACK);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.ADVANCED_GULLWING_PROFILE_ACROSS_REGION, new Color(0xF8F8FF)); // ghost white
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.ADVANCED_GULLWING_SIDE_FILLET_EDGE_REGION, new Color(0x8B4513)); // saddle brown
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.ADVANCED_GULLWING_SIDE_FILLET_HEEL_SEARCH_REGION, new Color(0xDAA520)); // goldenrod
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.ADVANCED_GULLWING_SIDE_FILLET_HEEL_PEAK_REGION, Color.BLUE);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.ADVANCED_GULLWING_SIDE_FILLET_CENTER_REGION, Color.MAGENTA);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QFN_BACKGROUND_REGION, new Color(0x4169E1)); // royal blue
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QFN_PAD_REGION, new Color(0xF8F8FF)); // ghost white
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QFN_HEEL_EDGE_REGION, new Color(0x8B4513)); // saddle brown
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QFN_HEEL_SEARCH_REGION, new Color(0xDAA520)); // goldenrod
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QFN_UPWARD_CURVATURE_SEARCH_REGION, Color.CYAN);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QFN_HEEL_PEAK_REGION, Color.BLUE);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QFN_CENTER_REGION, Color.MAGENTA);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QFN_TOE_EDGE_REGION, new Color(0x8B4513));  // saddle brown
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QFN_TOE_PEAK_REGION, new Color(0x3CB371)); // medium sea green
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QFN_SOLDER_THICKNESS_REGION, Color.RED);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.FILLET_REGION, Color.ORANGE);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.LOWER_FILLET_REGION, Color.DARK_GRAY);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.ALIGNMENT_PAD_REGION, Color.ORANGE);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.ALIGNMENT_CONTEXT_PAD_REGION, Color.BLUE);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION, Color.PINK);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.GRID_ARRAY_EDGE_REGION, Color.BLUE);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.COMPONENT_REGION, Color.WHITE);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.INNER_FILLET_EDGE_REGION, Color.PINK);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.UPPER_FILLET_REGION, Color.YELLOW);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.COMPONENT_PROFILE_REGION, Color.MAGENTA);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.OPEN_SIGNAL_SEARCH_REGION, Color.GREEN.darker());
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.MARGINAL_VOID_REGION, Color.YELLOW);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QFN_PROFILE_ACROSS_REGION, new Color(0xF8F8FF)); // ghost white
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QFN_PROFILE_HEEL_ACROSS_REGION, new Color(0x99CCFF)); // pale blue
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QFN_PROFILE_CENTER_ACROSS_REGION, new Color(0xFFCCFF)); // pale magenta
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QFN_ACROSS_LEADING_EDGE_REGION, new Color(0x8B4513)); // saddle brown
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QFN_ACROSS_TRAILING_EDGE_REGION, new Color(0x3CB371)); // medium sea green
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.EXPOSED_PAD_BACKGROUND_REGION, new Color(0x4169E1)); // royal blue
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.EXPOSED_PAD_PAD_REGION, new Color(0xF8F8FF)); // ghost white
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.EXPOSED_PAD_GAP_REGION, new Color(0xDAA520)); // goldenrod
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.EXPOSED_PAD_ALONG_INNER_GAP_REGION, new Color(0x8B4513)); // saddle brown
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.EXPOSED_PAD_ALONG_OUTER_GAP_REGION, new Color(0x3CB371)); // medium sea green
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.ECCENTRICITY_REGION, Color.ORANGE);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.MEASURED_FILLET_THICKNESS_REGION, new Color(0, 0, 192)); // navy blue
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.GRAYLEVEL_HISTOGRAM_PROFILE_REGION, new Color(0x006080)); // greenish blue
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.FIRST_DERIVATIVES_PROFILE_REGION, new Color(0xDAA520)); // goldenrod
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.SECOND_DERIVATIVES_PROFILE_REGION, new Color(0x3CB371)); // medium sea green
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.SLUG_EDGE_REGION, Color.BLUE);
    _measurementRegionEnumToColorMap.put((MeasurementRegionEnum.LEARN_REGION), new Color(0x3CB371));

    //Kee Chin Seong - New Void Algorithm for RF ConnectorVOID_BACKGROUND_REGION_KEY
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.VOID_PROFILE_START_BIN_REGION, Color.CYAN);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.VOID_BACKGROUND_REGION, Color.WHITE);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.DEFINITE_VOID_DEFECT_REGION, Color.RED);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.QUESTIONABLE_VOID_DEFECT_REGION, Color.YELLOW);
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.EXONERATED_VOID_DEFECT_REGION, Color.GREEN);

    //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
    _measurementRegionEnumToColorMap.put(MeasurementRegionEnum.EXCESS_SOLDER_REGION, Color.ORANGE);
  }

  /**
   * @author Patrick Lacz
   */
  public static boolean hasColorForMeasurementRegionEnum(MeasurementRegionEnum measurementRegionEnum)
  {
    Assert.expect(measurementRegionEnum != null);

    if (_measurementRegionEnumToColorMap == null)
      intializeMeasurementRegionEnumToColorMap();

    return _measurementRegionEnumToColorMap.containsKey(measurementRegionEnum);
  }

  /**
   * @author Patrick Lacz
   */
  public static Color getColorForMeasurementRegionEnum(MeasurementRegionEnum measurementRegionEnum)
  {
    Assert.expect(measurementRegionEnum != null);

    if (_measurementRegionEnumToColorMap == null)
      intializeMeasurementRegionEnumToColorMap();

    Color color = _measurementRegionEnumToColorMap.get(measurementRegionEnum);
    if(color == null)
          System.out.println("test");
    Assert.expect(color != null);
    return color;
  }

  /**
   * @author Bill Darbie
   */
  private XRayLayerColorEnum(int index, Color color)
  {
    super(index);
    Assert.expect(color != null);
    _color = color;
  }

  /**
   * @author Bill Darbie
   */
  public Color getColor()
  {
    return _color;
  }
}
