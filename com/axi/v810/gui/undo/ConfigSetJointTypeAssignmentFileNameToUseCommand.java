package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the joint type assignment config file name to use in config is changed,
 * it can be reset via an undo command.
 * @author Laura Cormos
 */
public class ConfigSetJointTypeAssignmentFileNameToUseCommand extends DataCommand
{
  private String _originalFileName;
  private String _newFileName;

  /**
   * @author Laura Cormos
   */
  public ConfigSetJointTypeAssignmentFileNameToUseCommand(String newFileName)
  {
    Assert.expect(newFileName != null);

    _newFileName = newFileName;
    _originalFileName = JointTypeEnumAssignment.getInstance().getJointTypeAssignmentConfigFileNameWithoutExtension();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalFileName.equalsIgnoreCase(_newFileName))
      return false;

    JointTypeEnumAssignment.getInstance().setJointTypeAssignmentConfigFileNameWithoutExtension(_newFileName);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    JointTypeEnumAssignment.getInstance().setJointTypeAssignmentConfigFileNameWithoutExtension(_originalFileName);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_JTA_SET_FILE_NAME_TO_USE_KEY");
  }
}
