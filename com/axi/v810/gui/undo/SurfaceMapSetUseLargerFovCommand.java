/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used to set whether to use Larger FOV or not.
 * Using this class allows us to undo the operation later.
 * 
 * @author Ying-Huan.Chu
 */
public class SurfaceMapSetUseLargerFovCommand extends DataCommand
{
  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  private Project _project;
  private boolean _originalUseLargerFovBoolean = false;
  private boolean _newUseLargerFovBoolean = false;
  
  /**
   * @author Ying-Huan.Chu
   * @param project pass in the project to set to use Larger FOV.
   * @param isLargerFov pass in true to use Larger FOV, false otherwise
   */
  public SurfaceMapSetUseLargerFovCommand(Project project, boolean isLargerFov)
  {
    _project = project;
    _originalUseLargerFovBoolean = _project.getPanel().isLargerFOV();
    _newUseLargerFovBoolean = isLargerFov;
  }
  
  /**
   * Execute the the setUseLargerFOV command.
   * @author Ying-Huan.Chu
   * @throws XrayTesterException 
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_newUseLargerFovBoolean == _originalUseLargerFovBoolean)
      return false;
    
    _projectObservable.setEnabled(false);
    try
    {
      _project.getPanel().setUseLargerFOV(_newUseLargerFovBoolean);
    }
    finally
    {
      //Ying-Huan.Chu - temporary commented Larger FOV feature for 5.7 release.
      //_projectObservable.setEnabled(true);
      //_projectObservable.stateChanged(this, _originalUseLargerFovBoolean, _newUseLargerFovBoolean, PspEventEnum.SET_USE_LARGER_FOV);
    }
    return true;
  }
  
  /**
   * Undo the setUseLargerFOV command.
   * @author Ying-Huan.Chu
   * @throws XrayTesterException 
   */
  public void undoCommand() throws XrayTesterException
  {
    _projectObservable.setEnabled(false);
    try
    {
      _project.getPanel().setUseLargerFOV(_originalUseLargerFovBoolean);
    }
    finally
    {
      //Ying-Huan.Chu - temporary commented Larger FOV feature for 5.7 release.
      //_projectObservable.setEnabled(true);
      //_projectObservable.stateChanged(this, _newUseLargerFovBoolean, _originalUseLargerFovBoolean, PspEventEnum.SET_USE_LARGER_FOV);
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   * @return 'Use Larger FOV' localized string
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SET_USE_LARGER_FOV_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
