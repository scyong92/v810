package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command to destroy a land pattern
 * @author Laura Cormos
 */
public class RemoveLandPatternCommand extends DataCommand
{
  private LandPattern _landPatternToRemove;

  /**
   * @author Laura Cormos
   */
  public RemoveLandPatternCommand(LandPattern destroyedLandPattern)
  {
    Assert.expect(destroyedLandPattern != null);

    _landPatternToRemove = destroyedLandPattern;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_landPatternToRemove != null);

    _landPatternToRemove.remove();
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_landPatternToRemove != null);

    _landPatternToRemove.add();
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_DESTROY_LANDPATTERN_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
