package com.axi.v810.gui.undo;

import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 *
 * @author Kok Chun, Tan
 */
public class ConfigSetPanelAvailableAfterDownStreamAcceptPanelCommand extends DataCommand
{
  private boolean _originalMode;
  private boolean _newMode;
  private PanelHandler _panelHandler = PanelHandler.getInstance();

  /**
   * @author  Kok Chun, Tan
   */
  public ConfigSetPanelAvailableAfterDownStreamAcceptPanelCommand(boolean newMode)
  {
    _newMode = newMode;
    _originalMode = _panelHandler.isPanelAvailableAfterDownStreamAcceptPanelModeEnabled();
  }

  /**
   * @author  Kok Chun, Tan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalMode == _newMode)
      return false;

    _panelHandler.setPanelAvailableAfterDownStreamAcceptPanelModeEnabled(_newMode);
    return true;
  }

  /**
   * @author  Kok Chun, Tan
   */
  public void undoCommand() throws XrayTesterException
  {
    _panelHandler.setPanelAvailableAfterDownStreamAcceptPanelModeEnabled(_originalMode);
  }

  /**
   * @author  Kok Chun, Tan
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_CHECK_PANEL_AVAILABLE_AFTER_DOWNSTREAM_ACCEPT_PANEL_MODE_KEY");
  }
}
