package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.util.*;

/**
 * Command to create a new user acount
 * @author George Booth
 */
public class ConfigUserAccountsAddCommand extends DataCommand
{
  private UserAccountsManager _userAccountsManager = UserAccountsManager.getInstance();
  private UserAccountInfo _userAccount = null;

  /**
   * @author George Booth
   */
  public ConfigUserAccountsAddCommand(UserAccountInfo userAccount)
  {
    Assert.expect(userAccount != null);

    _userAccount = userAccount;
  }

  /**
   * @author George Booth
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_userAccount != null);

    _userAccountsManager.addAccount(_userAccount);
    return true;
  }

  /**
   * @author George Booth
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_userAccount != null);

    _userAccountsManager.deleteAccount(_userAccount);
  }

  /**
   * @author George Booth
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_ADD_USER_ACCOUNT_KEY");
  }
}
