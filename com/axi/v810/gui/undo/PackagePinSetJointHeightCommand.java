package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * This class is used to set the joint height for a CompPackage
 * Using this class allows us to undo the set later.
 * @author Laura Cormos
 */
public class PackagePinSetJointHeightCommand extends DataCommand
{
  private PackagePin _packagePin = null;
  private int _originalJointHeightInNanos = 0;
  private int _newJointHeightInNanos = 0;

  /**
   * @author Laura Cormos
   */
  public PackagePinSetJointHeightCommand(PackagePin packagePin, int newJointHeightInNanos)
  {
    Assert.expect(packagePin != null);

    _packagePin = packagePin;
    _newJointHeightInNanos = newJointHeightInNanos;

    _originalJointHeightInNanos = packagePin.getJointHeightInNanoMeters();
  }

  /**
   * @return true if the command executed, false if new algorithm family is the same as the current one
   * @throws XrayTesterException if the command fails
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_packagePin != null);

    if (_newJointHeightInNanos == _originalJointHeightInNanos)
      return false;

    _packagePin.setCustomJointHeightInNanoMeters(_newJointHeightInNanos);

    return true;
  }

  /**
   * @throws XrayTesterException if the command fails
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_packagePin != null);

    _packagePin.setCustomJointHeightInNanoMeters(_originalJointHeightInNanos);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PACKAGE_PIN_SET_JOINT_HEIGHT_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
