package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;
import com.axi.v810.gui.config.*;

/**
 * Command for setting the use serial number to find project config enum
 * @author Laura Cormos
 */
public class ConfigSetUseSerialNumberToFindProjectCommand extends DataCommand
{
  private boolean _newEnabledState;
  private boolean _oldEnabledState;
  private boolean _oldEnabledStateForUseFirstSerialNumberForProjectLookupOnly;
  private SerialNumberManager _serialNumberManager = SerialNumberManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetUseSerialNumberToFindProjectCommand(boolean enabledState, ProductionOptionsSerialNumberTabPanel parentPanel)
  {
    Assert.expect(parentPanel != null);

    _newEnabledState = enabledState;
    _oldEnabledState = _serialNumberManager.isUseSerialNumberToFindProjectEnabled();
    _oldEnabledStateForUseFirstSerialNumberForProjectLookupOnly = _serialNumberManager.isUseFirstSerialNumberForProjectLookupOnlyEnabled();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_oldEnabledState == _newEnabledState)
      return false;

    _serialNumberManager.setUseSerialNumberToFindProject(_newEnabledState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _serialNumberManager.setUseSerialNumberToFindProject(_oldEnabledState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SN_SET_USE_SN_TO_FIND_PROJECT_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
