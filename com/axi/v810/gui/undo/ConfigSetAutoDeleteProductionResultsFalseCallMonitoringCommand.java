package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.gui.config.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class ConfigSetAutoDeleteProductionResultsFalseCallMonitoringCommand extends DataCommand
{
  private boolean _newEnabledState;
  private boolean _oldEnabledState;  
  private FalseCallMonitoring _falseCallMonitoring = FalseCallMonitoring.getInstance();
  
  /**
   * @author Cheah Lee Herng
   */
  public ConfigSetAutoDeleteProductionResultsFalseCallMonitoringCommand(boolean enabledState)
  {
    _newEnabledState = enabledState;
    _oldEnabledState = _falseCallMonitoring.isAutoDeleteProductionResults();
  }

  /**
   * @author Cheah Lee Herng 
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_oldEnabledState == _newEnabledState)
      return false;

    _falseCallMonitoring.setAutoDeleteProductionResults(_newEnabledState);
    return true;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void undoCommand() throws XrayTesterException
  {
    _falseCallMonitoring.setAutoDeleteProductionResults(_oldEnabledState);
  }

  /**
   * @author Cheah Lee Herng
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_FALSE_CALL_TRIGGERING_DELETE_RESULTS_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
