package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author siew-yeng.phang
 */
public class PshSettingsRemoveNeighborCommand extends DataCommand
{
  private PshSettings _pshSettings = null;
  
  private ComponentType _pshComponent = null;
  private ComponentType _neighbourComponent = null;
  
  /**
   * @author Siew Yeng
   */
  public PshSettingsRemoveNeighborCommand(PshSettings pshSettings, ComponentType pshComponent, ComponentType neighbourComponent)
  {
    Assert.expect(pshSettings != null);
    Assert.expect(pshComponent != null);
    Assert.expect(neighbourComponent != null);

    _pshSettings = pshSettings;
    _pshComponent = pshComponent;
    _neighbourComponent = neighbourComponent;
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean executeCommand() throws XrayTesterException 
  {
    _pshSettings.removeSelectiveNeighbour(_pshComponent, _neighbourComponent);
    
    return true;
  }

  /**
   * @author Siew Yeng
   */
  public void undoCommand() throws XrayTesterException 
  {
    _pshSettings.addSelectiveNeighbor(_pshComponent, _neighbourComponent);
  }

  /**
   * @author Siew Yeng
   */
  public String getDescription() 
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PSH_REMOVE_NEIGHBOUR_COMMAND_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
  
}
