package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if a results rule name is changed,
 * it can be reset via an undo command.
 * @author Laura Cormos
 */
public class ConfigSetResultsRuleNameCommand extends DataCommand
{
  private String _originalRuleName;
  private String _newRuleName;
  private Rule _rule;

  /**
   * @author Laura Cormos
   */
  public ConfigSetResultsRuleNameCommand(Rule rule, String ruleName)
  {
    Assert.expect(rule != null);
    Assert.expect(ruleName != null);

    _newRuleName = ruleName;
    _rule = rule;
    _originalRuleName = _rule.getName();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalRuleName.equalsIgnoreCase(_newRuleName))
      return false;

    _rule.setName(_newRuleName);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _rule.setName(_originalRuleName);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_RESPROC_SET_RULE_NAME_KEY");
  }
}
