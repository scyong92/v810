package com.axi.v810.gui.undo;

import com.axi.v810.business.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author wei-chin.chong
 */
public class ConfigXoutDetectionMethodCommand extends DataCommand
{
  private XoutDetectionMethodEnum _originalMethod;
  private XoutDetectionMethodEnum _xoutDetectionMethod;

  private TestExecution _testExecution = TestExecution.getInstance();

  /**
   * @author wei-chin.chong
   */
  public ConfigXoutDetectionMethodCommand(XoutDetectionMethodEnum xoutDetectionMethod)
  {
    _xoutDetectionMethod = xoutDetectionMethod;
    _originalMethod = _testExecution.getXoutDetectionMethod();
  }

  /**
   * @author wei-chin.chong
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalMethod == _xoutDetectionMethod)
      return false;

    _testExecution.setXoutDetectionMethod(_xoutDetectionMethod);
    return true;
  }

  /**
   * @author wei-chin.chong
   */
  public void undoCommand() throws XrayTesterException
  {
    _testExecution.setXoutDetectionMethod(_originalMethod);
  }

  /**
   * @author wei-chin.chong
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_XOUT_DETECTION_METHOD_KEY");
  }
}

