package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ConfigSetPreInspectionScriptCommand extends DataCommand
{
  private String _originalScript;
  private String _newScript;
  private ScriptRunner _scriptRunner = ScriptRunner.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetPreInspectionScriptCommand(String newScript)
  {
    _newScript = newScript;
    _originalScript = _scriptRunner.getPreInspectionScript();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalScript.equalsIgnoreCase(_newScript))
      return false;

    _scriptRunner.setPreInspectionScript(_newScript);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _scriptRunner.setPreInspectionScript(_originalScript);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_PRE_INSPECTION_SCRIPT_KEY");
  }
}
