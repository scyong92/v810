package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This project is used store the state of generate multi angle image
 * @author bee-hoon.goh
 */
public class ProjectSetEnableGenerateComponentImageCommand extends DataCommand
{
  private Project _project = null;
  private boolean _newGenerateComponentImage = false;
  private boolean _oldGenerateComponentImage = false;

  /**
   * @param project the project will have generate multi angle image generation
   * @param isGenerateComponentImage the new state of generate multi angle image
   * @author bee-hoon.goh
   */
  public ProjectSetEnableGenerateComponentImageCommand(Project project, boolean isGenerateComponentImage)
  {
    Assert.expect(project != null);

    _project = project;
    _newGenerateComponentImage = isGenerateComponentImage;
    _oldGenerateComponentImage = _project.isGenerateComponentImage();
  }

  /**
   * Change the state of generate multi angle image
   * @author bee-hoon.goh
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_oldGenerateComponentImage == _newGenerateComponentImage)
      return false;

    _project.setGenerateComponentImage(_newGenerateComponentImage);
    return true;
  }

  /**
   * Restore the old state of enable generate multi angle image
   * @author bee-hoon.goh
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setGenerateComponentImage(_oldGenerateComponentImage);
  }

  /**
   * @author bee-hoon.goh
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_ENABLE_GENERATE_COMPONENT_IMAGE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
