package com.axi.v810.gui.undo;

import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the mail server in config is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ConfigSetMeasurementDataRunsCommand extends DataCommand
{
  private int _originalMeasurementRuns;
  private int _newMeasurementRuns;
  private ResultsProcessor _resultsProcessor = ResultsProcessor.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetMeasurementDataRunsCommand(int newMeasurementRuns)
  {
    _newMeasurementRuns = newMeasurementRuns;
    _originalMeasurementRuns = _resultsProcessor.getMaximumNumberOfResultsToKeep();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalMeasurementRuns == _newMeasurementRuns)
      return false;

    _resultsProcessor.setMaximumNumberOfResultsToKeep(_newMeasurementRuns);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _resultsProcessor.setMaximumNumberOfResultsToKeep(_originalMeasurementRuns);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_NUMBER_MEASUREMENTS_KEY");
  }
}
