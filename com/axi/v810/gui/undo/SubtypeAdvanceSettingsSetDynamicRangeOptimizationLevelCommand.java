package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;


/**
 *
 * @author siew-yeng.phang
 */
public class SubtypeAdvanceSettingsSetDynamicRangeOptimizationLevelCommand extends DataCommand
{
  private SubtypeAdvanceSettings _subtypeAdvanceSettings = null;
  private DynamicRangeOptimizationLevelEnum _origDroLevel = null;
  private DynamicRangeOptimizationLevelEnum _newDroLevel = null;

  /**
   * Construct the class.
   * @author Siew Yeng
   */
  public SubtypeAdvanceSettingsSetDynamicRangeOptimizationLevelCommand(SubtypeAdvanceSettings subtypeAdvanceSettings, DynamicRangeOptimizationLevelEnum newState)
  {
    Assert.expect(subtypeAdvanceSettings != null);
    Assert.expect(newState != null);

    _subtypeAdvanceSettings = subtypeAdvanceSettings;
    _newDroLevel = newState;

    _origDroLevel = subtypeAdvanceSettings.getDynamicRangeOptimizationLevel();
  }

  /**
   * @author Siew Yeng
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_subtypeAdvanceSettings != null);

    if (_newDroLevel.equals(_origDroLevel))
      return false;

    _subtypeAdvanceSettings.setDynamicRangeOptimizationLevel(_newDroLevel);
    return true;
  }

  /**
   * @author Siew Yeng
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_subtypeAdvanceSettings != null);

    _subtypeAdvanceSettings.setDynamicRangeOptimizationLevel(_origDroLevel);
  }

  /**
   * @author Siew Yeng
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_ADVANCE_SETTINGS_SET_DRO_LEVEL_KEY");
  }
}
