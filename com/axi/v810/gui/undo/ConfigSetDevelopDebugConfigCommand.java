package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;

/**
 *
 * @author chin-seong.kee
 * command manager for setting developer debug config enum
 * Universal command which handle any types of config enum for Developer debug config enum
 * So, Developer no need to modify too much and ease them also
 */
public class ConfigSetDevelopDebugConfigCommand  extends DataCommand
{
  private DeveloperDebugConfigEnum _configEnum, _originalConfigEnum;
  private Object _value, _originalValue;
  
  private Config _config = Config.getInstance();

  public ConfigSetDevelopDebugConfigCommand (DeveloperDebugConfigEnum configEnum, Object value)
  {
    _configEnum = configEnum;
    _value = value;
    _originalConfigEnum = configEnum;
    _originalValue = configEnum.getValue();
  }

  public boolean executeCommand() throws XrayTesterException
  {
    _config.setValue(_configEnum, _value);
    
    return true;
  }

  public void undoCommand() throws XrayTesterException
  {
    _config.setValue(_originalConfigEnum, _originalValue);
  }
  
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_DEVELOPER_DEBUG_CONFIG_KEY");
  }
  
}
