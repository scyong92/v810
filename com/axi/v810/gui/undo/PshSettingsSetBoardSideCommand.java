package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author siew-yeng.phang
 */
public class PshSettingsSetBoardSideCommand extends DataCommand
{
  private PshSettings _pshSettings = null;
  
  private PshSettingsBoardSideEnum _newValue = null;
  private PshSettingsBoardSideEnum _oldValue = null;
  
  /**
   * @author Siew Yeng
   */
  public PshSettingsSetBoardSideCommand(PshSettings pshSettings, PshSettingsBoardSideEnum sideBoard)
  {
    Assert.expect(pshSettings != null);
    Assert.expect(sideBoard != null);

    _pshSettings = pshSettings;
    _newValue = sideBoard;
    _oldValue = pshSettings.getPshSettingsBoardSideEnum();
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean executeCommand() throws XrayTesterException 
  {
    if(_newValue == _oldValue)
      return false;
    
    _pshSettings.setPshBoardSideReference(_newValue);
    
    return true;
  }

  /**
   * @author Siew Yeng
   */
  public void undoCommand() throws XrayTesterException 
  {
    _pshSettings.setPshBoardSideReference(_oldValue);
  }

  /**
   * @author Siew Yeng
   */
  public String getDescription() 
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PSH_SET_SELECTIVE_BOARD_SIDE_COMMAND_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
