package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command to destroy a fiducial
 * @author Laura Cormos
 */
public class RemoveFiducialCommand extends DataCommand
{
  private Fiducial _fiducialToRemove;

  /**
   * @author Laura Cormos
   */
  public RemoveFiducialCommand(Fiducial destroyedFiducial)
  {
    Assert.expect(destroyedFiducial != null);

    _fiducialToRemove = destroyedFiducial;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_fiducialToRemove != null);

    _fiducialToRemove.remove();
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_fiducialToRemove != null);

    _fiducialToRemove.add();
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_DESTROY_FIDUCIAL_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
