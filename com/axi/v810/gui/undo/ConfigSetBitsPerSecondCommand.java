package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class sets/unsets the bits per second for the barcode reader configuration
 * @author Laura Cormos
 */
public class ConfigSetBitsPerSecondCommand extends DataCommand
{
  private String _originalBitsPerSecond;
  private String _newBitsPerSecond;
  private BarcodeReaderManager _bcrManager = BarcodeReaderManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetBitsPerSecondCommand(String portNumber)
  {
    Assert.expect(portNumber != null);

    _newBitsPerSecond = portNumber;
    _originalBitsPerSecond = _bcrManager.getBarcodeReaderRs232BitsPerSecond();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_newBitsPerSecond != null);

    if (_newBitsPerSecond.equals(_originalBitsPerSecond))
      return false;

    _bcrManager.setBarcodeReaderRs232BitsPerSecond(_newBitsPerSecond);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_originalBitsPerSecond != null);

    _bcrManager.setBarcodeReaderRs232BitsPerSecond(_originalBitsPerSecond);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_BITS_PER_SECOND_KEY");
  }
}
