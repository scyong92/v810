package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * Command for setting the UseSerialNumberPerBoard checkbox
 * @author Laura Cormos
 */
public class ConfigSetUseFirstSerialNumberForProjectLookupOnlyCommand extends DataCommand
{
  private boolean _newEnabledState;
  private boolean _oldEnabledState;
  private boolean _oldEnabledStateForUseSerialNumberToFindProject;
  private SerialNumberManager _serialNumberManager = SerialNumberManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetUseFirstSerialNumberForProjectLookupOnlyCommand(boolean enabledState)
  {
    _newEnabledState = enabledState;
    _oldEnabledState = _serialNumberManager.isUseFirstSerialNumberForProjectLookupOnlyEnabled();
    _oldEnabledStateForUseSerialNumberToFindProject = _serialNumberManager.isUseSerialNumberToFindProjectEnabled();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_oldEnabledState == _newEnabledState)
      return false;

    _serialNumberManager.setUseFirstSerialNumberForProjectLookupOnly(_newEnabledState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _serialNumberManager.setUseFirstSerialNumberForProjectLookupOnly(_oldEnabledState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SN_SET_USE_FIRST_SN_FOR_LOOKUP_ONLY_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
