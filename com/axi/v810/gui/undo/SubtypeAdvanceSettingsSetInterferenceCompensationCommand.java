/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * XCR-2895 : Initial recipes setting to speed up the programming time
 *
 * @author weng-jian.eoh
 */
public class SubtypeAdvanceSettingsSetInterferenceCompensationCommand extends DataCommand
{
  private SubtypeAdvanceSettings _subtypeAdvanceSettings = null;
  private boolean _origInterferenceCompensation;
  private boolean _newInterferenceCompensation;

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   *
   * @author weng-jian.eoh
   */
  public SubtypeAdvanceSettingsSetInterferenceCompensationCommand(SubtypeAdvanceSettings subtypeAdvanceSettings, boolean newState)
  {
    Assert.expect(subtypeAdvanceSettings != null);

    _subtypeAdvanceSettings = subtypeAdvanceSettings;
    _origInterferenceCompensation = newState;
    _newInterferenceCompensation = _subtypeAdvanceSettings.isIsInterferenceCompensatable();
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   *
   * @author weng-jian.eoh
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_subtypeAdvanceSettings != null);

    if (_newInterferenceCompensation == _origInterferenceCompensation)
      return false;

    _subtypeAdvanceSettings.setIsInterferenceCompensatable(_newInterferenceCompensation);
    return true;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   *
   * @author weng-jian.eoh
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_subtypeAdvanceSettings != null);

    _subtypeAdvanceSettings.setIsInterferenceCompensatable(_origInterferenceCompensation);   
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   *
   * @author weng-jian.eoh
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_ADVANCE_SETTINGS_SET_INTEGRATION_LEVEL_KEY");
  }
}
