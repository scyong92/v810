package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Changes a Fiducial's name
 * @author Laura Cormos
 */
public class FiducialTypeSetNameCommand extends DataCommand
{
  boolean isBoardFiducial = false;
  private Panel _panel;
  private String _fiducialName;
  private SideBoardType _sideBoardType;
  private String _newName = null;
  private String _oldName = null;

  /**
   * @param fiducialType the Fiducial Type whose name will change
   * @param newName the new name
   * @author Laura Cormos
   */
  public FiducialTypeSetNameCommand(FiducialType fiducialType, String newName)
  {
    Assert.expect(fiducialType != null);
    Assert.expect(newName != null);

    isBoardFiducial = fiducialType.hasSideBoardType();
    if (isBoardFiducial)
      _sideBoardType = fiducialType.getSideBoardType();
    else
      _panel = fiducialType.getFiducials().iterator().next().getPanel();
    _fiducialName = fiducialType.getName();
    _newName = newName;
    _oldName = fiducialType.getName();
  }

  /**
   * Change the name to the new name
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_fiducialName != null);
    if (isBoardFiducial)
      Assert.expect(_sideBoardType != null);
    else
      Assert.expect(_panel != null);

    if (_oldName.equals(_newName))
      return false;

    FiducialType fiducialType;
    if (isBoardFiducial)
      fiducialType = _sideBoardType.getFiducialType(_fiducialName);
    else
      fiducialType = _panel.getFiducial(_fiducialName).getFiducialType();
    Assert.expect(fiducialType != null);
    fiducialType.setName(_newName);
    _fiducialName = _newName;
    return true;
  }

  /**
   * Restore the old name
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_fiducialName != null);
    Assert.expect(_fiducialName != null);
    if (isBoardFiducial)
      Assert.expect(_sideBoardType != null);
    else
      Assert.expect(_panel != null);

    // first find the fiducial type object since it could have been deleted and re added by an undo/redo
    FiducialType fiducialType;
    if (isBoardFiducial)
      fiducialType = _sideBoardType.getFiducialType(_fiducialName);
    else
      fiducialType = _panel.getFiducial(_fiducialName).getFiducialType();
    Assert.expect(fiducialType != null);
    fiducialType.setName(_oldName);
    _fiducialName = _oldName;
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_FIDUCIALTYPE_SET_NAME_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
