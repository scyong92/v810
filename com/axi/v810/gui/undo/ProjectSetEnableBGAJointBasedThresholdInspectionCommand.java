package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This project is used store the state of BGA Joint Based Threshold Inspection
 * @author Swee Yee
 */
public class ProjectSetEnableBGAJointBasedThresholdInspectionCommand extends DataCommand
{
  private Project _project = null;
  private boolean _newBGAJointBasedThresholdInspectionSetting = false;
  private boolean _oldBGAJointBasedThresholdInspectionSetting = false;

  /**
   * @param project the project will enable BGA Joint Based Threshold Inspection
   * @param isBGAJointBasedThresholdInspectionEnabled the new state of BGA Joint Based Threshold Inspection
   * @author Swee Yee
   */
  public ProjectSetEnableBGAJointBasedThresholdInspectionCommand(Project project, boolean isBGAJointBasedThresholdInspectionEnabled)
  {
    Assert.expect(project != null);

    _project = project;
    _newBGAJointBasedThresholdInspectionSetting = isBGAJointBasedThresholdInspectionEnabled;
    _oldBGAJointBasedThresholdInspectionSetting = _project.isBGAJointBasedThresholdInspectionEnabled();
  }

  /**
   * Change the state of BGA Joint Based Threshold Inspection
   * @author Swee Yee
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_oldBGAJointBasedThresholdInspectionSetting == _newBGAJointBasedThresholdInspectionSetting)
      return false;

    _project.setEnableBGAJointBasedThresholdInspection(_newBGAJointBasedThresholdInspectionSetting);
    return true;
  }

  /**
   * Restore the old state of enable BGA Joint Based Threshold Inspection
   * @author Swee Yee
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setEnableBGAJointBasedThresholdInspection(_oldBGAJointBasedThresholdInspectionSetting);
  }

  /**
   * @author Swee Yee
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_ENABLE_BGA_JOINT_BASED_THRESHOLD_INSPECTION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
