package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting a component type rotation
 * @author Laura Cormos
 */
public class ComponentTypeSetDegreesRotationCommand extends DataCommand
{
  // in this command we don't want to store the actual component type passed in the constructor because this object
  // can be destroyed and recreated during an undo/redo operation. In that case this command's undo method would
  // operate on nonexistant object. This is why we store the object name and find it every time we need to use it
  private String _refDes = null;
  private double _newRotation = 0;
  private double _oldRotation = 0;
  private SideBoardType _sideBoardType = null;

  /**
   * @param componentType the component type whose rotation will change
   * @param newRotation the new rotation
   * @author Laura Cormos
   */
  public ComponentTypeSetDegreesRotationCommand(ComponentType componentType , double newRotation)
  {
    Assert.expect(componentType != null);

    _newRotation = newRotation;
    _oldRotation = componentType.getDegreesRotationRelativeToBoard();
    _refDes = componentType.getReferenceDesignator();
    _sideBoardType = componentType.getSideBoardType();
  }

  /**
   * Change the rotation to the new rotation
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_sideBoardType != null);
    Assert.expect(_refDes != null);

    if (_oldRotation == (_newRotation))
      return false;

    // first find the component type object because it could have been recreated through an undo/redo operation
    ComponentType componentType = _sideBoardType.getComponentType(_refDes);
    componentType.setDegreesRotationRelativeToBoard(_newRotation);
    return true;
  }

  /**
   * Restore the old rotation
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_sideBoardType != null);
    Assert.expect(_refDes != null);

    // first find the component type object because it could have been recreated through an undo/redo operation
    ComponentType componentType = _sideBoardType.getComponentType(_refDes);
    componentType.setDegreesRotationRelativeToBoard(_oldRotation);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_COMPONENT_TYPE_SET_ROTATION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
