package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the mail server in config is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ConfigSetNdfDirectoryCommand extends DataCommand
{
  private String _originalNdfDirectory;
  private String _newNdfDirectory;

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetNdfDirectoryCommand(String newNdfDirectory)
  {
    Assert.expect(newNdfDirectory != null);

    _newNdfDirectory = newNdfDirectory;
    _originalNdfDirectory = Directory.getLegacyNdfDir();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalNdfDirectory.equalsIgnoreCase(_newNdfDirectory))
      return false;

    Directory.setLegacyNdfDir(_newNdfDirectory);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Directory.setLegacyNdfDir(_originalNdfDirectory);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_NDF_DIRECTORY_KEY");
  }
}
