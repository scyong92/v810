package com.axi.v810.gui.undo;

import com.axi.util.Assert;
import com.axi.v810.util.XrayTesterException;
import com.axi.v810.datastore.Directory;
import com.axi.v810.util.StringLocalizer;

/**
 * @author weng-jian.eoh
 */
public class ConfigSetInitialRecipeSettingDirectoryCommand extends DataCommand
{
  private String _originalRecipeSettingDirectory;
  private String _newRecipeSettingDirectory;

  /**
   * @author weng-jian.eoh
   */
  public ConfigSetInitialRecipeSettingDirectoryCommand(String newRecipeSettingDirectory)
  {
    Assert.expect(newRecipeSettingDirectory != null);

    _newRecipeSettingDirectory = newRecipeSettingDirectory;
    _originalRecipeSettingDirectory = Directory.getInitialRecipeSettingDir();
  }

  /**
   * @author weng-jian.eoh
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_newRecipeSettingDirectory != null);
    Assert.expect(_originalRecipeSettingDirectory != null);

    if (_originalRecipeSettingDirectory.equalsIgnoreCase(_newRecipeSettingDirectory))
    {
      return false;
    }

    Directory.setInitialRecipeSettingDir(_newRecipeSettingDirectory);
    return true;
  }

  /**
   * @author weng-jian.eoh
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_originalRecipeSettingDirectory != null);

    Directory.setInitialRecipeSettingDir(_originalRecipeSettingDirectory);
  }

  /**
   * @author weng-jian.eoh
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_INITIAL_RECIPE_SETTING_DIRECTORY_KEY");
  }
}
