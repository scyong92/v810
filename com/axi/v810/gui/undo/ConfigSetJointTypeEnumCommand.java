package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting the joint type enum for a joint type assignment rule
 * @author Laura Cormos
 */
public class ConfigSetJointTypeEnumCommand extends DataCommand
{
  private JointTypeEnumRule _jointTypeEnumRule = null;
  private JointTypeEnum _newJointTypeEnum;
  private JointTypeEnum _oldJointTypeEnum;

  /**
   * @author Laura Cormos
   */
  public ConfigSetJointTypeEnumCommand(JointTypeEnumRule jointTypeEnumRule , JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnumRule != null);
    Assert.expect(jointTypeEnum != null);

    _jointTypeEnumRule = jointTypeEnumRule;
    _newJointTypeEnum = jointTypeEnum;
    _oldJointTypeEnum = _jointTypeEnumRule.getJointTypeEnum();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    if (_oldJointTypeEnum.equals(_newJointTypeEnum))
      return false;

    _jointTypeEnumRule.setJointTypeEnum(_newJointTypeEnum);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    _jointTypeEnumRule.setJointTypeEnum(_oldJointTypeEnum);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_JTA_SET_JOINT_TYPE_ENUM_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
