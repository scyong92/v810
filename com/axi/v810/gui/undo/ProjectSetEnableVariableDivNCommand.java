package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Yong Sheng Chuan
 */
public class ProjectSetEnableVariableDivNCommand extends DataCommand
{
  private Project _project = null;
  private boolean _oldVariableDivNSetting= false;
  private boolean _newVariableDivNSetting = false;

  /**
   * @author Yong Sheng Chuan
   */
  public ProjectSetEnableVariableDivNCommand(Project project, boolean isUseVariableDivN)
  {
    Assert.expect(project != null);

    _project = project;
    _newVariableDivNSetting = isUseVariableDivN;
    _oldVariableDivNSetting = _project.isGenerateByNewScanRoute();
  }

  /**
   * @author Yong Sheng Chuan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_newVariableDivNSetting == _newVariableDivNSetting)
      return false;

    _project.setVarDivN(_newVariableDivNSetting);
    return true;
  }

  /**
   * @author Yong Sheng Chuan
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setVarDivN(_oldVariableDivNSetting);
  }

  /**
   * @author Yong Sheng Chuan
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_ENABLE_VARIABLE_DIVN_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
