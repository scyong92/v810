package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * Command for setting the production tune current count property
 * @author Andy Mechtenberg
 */
public class ConfigSetProductionTuneCurrentCountCommand extends DataCommand
{
  private ProductionTuneData _productionTuneData = null;
  private int _newCurrentCount;
  private int _oldCurrentCount;

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetProductionTuneCurrentCountCommand(ProductionTuneData productionTuneData,
                                                    int currentCount)
  {
    Assert.expect(productionTuneData != null);

    _productionTuneData = productionTuneData;
    _newCurrentCount = currentCount;
    _oldCurrentCount = _productionTuneData.getCurrentCount();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);

    _productionTuneData.setCurrentCount(_newCurrentCount);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);

    _productionTuneData.setCurrentCount(_oldCurrentCount);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_PRODUCTION_TUNE_SET_CURRENT_COUNT_KEY");
  }
}
