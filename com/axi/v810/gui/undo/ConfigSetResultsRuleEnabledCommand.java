package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if a results rule enable state is changed,
 * it can be reset via an undo command.
 * @author Laura Cormos
 */
public class ConfigSetResultsRuleEnabledCommand extends DataCommand
{
  private boolean _enableState;
  private Rule _rule;

  /**
   * @author Laura Cormos
   */
  public ConfigSetResultsRuleEnabledCommand(Rule rule, boolean enabled)
  {
    Assert.expect(rule != null);

    _enableState = enabled;
    _rule = rule;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    _rule.setIsEnabled(_enableState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _rule.setIsEnabled(!_enableState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_RESPROC_SET_RULE_ENABLE_KEY");
  }
}
