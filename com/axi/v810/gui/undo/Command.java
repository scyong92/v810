package com.axi.v810.gui.undo;

import com.axi.v810.util.*;

/**
 * This class adds the commandFinished() method to the base Command methods.  It also
 * automatically calls the commandFinished() method after the execute() or undo()
 * method is called.  The commandFinished() call was needed so we can get feedback
 * of when the GUI is done updating the screen based on a Command being executed.
 * We need to know when the GUI is done updating to
 * properly implement the undo/redo functionality when the user holds down one of those
 * keys to do repeated undo or redo steps.
 *
 * @see CommandManager
 * @see GuiCommand
 * @see DataCommand
 *
 * @author Bill Darbie
 */
public abstract class Command extends com.axi.util.Command
{
  /**
   * @author Bill Darbie
   */
  public boolean execute() throws Exception
  {
    boolean executed = executeCommand();
    if (executed)
      commandFinished();

    return executed;
  }

  /**
   * @author Bill Darbie
   */
  public void undo() throws Exception
  {
    undoCommand();
    commandFinished();
  }

  /**
   * @author Bill Darbie
   */
  public abstract boolean executeCommand() throws XrayTesterException;

  /**
   * @author Bill Darbie
   */
  public abstract void undoCommand() throws XrayTesterException;

  /**
   * @author Bill Darbie
   */
  public abstract void commandFinished();
}
