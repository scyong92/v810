package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ConfigSetPostInspectionScriptCommand extends DataCommand
{
  private String _originalScript;
  private String _newScript;
  private ScriptRunner _scriptRunner = ScriptRunner.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetPostInspectionScriptCommand(String newScript)
  {
    _newScript = newScript;
    _originalScript = _scriptRunner.getPostInspectionScript();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalScript.equalsIgnoreCase(_newScript))
      return false;

    _scriptRunner.setPostInspectionScript(_newScript);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _scriptRunner.setPostInspectionScript(_originalScript);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_POST_INSPECTION_SCRIPT_KEY");
  }
}
