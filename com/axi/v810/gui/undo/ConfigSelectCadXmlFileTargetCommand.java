package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.gui.config.*;
import com.axi.v810.util.*;

/**
 * Command for setting the cad XML file checkbox
 * @author Laura Cormos
 */
public class ConfigSelectCadXmlFileTargetCommand extends DataCommand
{
  private boolean _enabledState;
  private ResultsProcessingResultsTabPanel _parent;

  /**
   * @author Laura Cormos
   */
  public ConfigSelectCadXmlFileTargetCommand(boolean enabledState, ResultsProcessingResultsTabPanel parent)
  {
    Assert.expect(parent != null);

    _enabledState = enabledState;
    _parent = parent;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    _parent.setCadXMLFileCheckboxState(_enabledState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _parent.setCadXMLFileCheckboxState(!_enabledState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_RESPROC_ENABLE_CAD_XML_FILE_TARGET_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
