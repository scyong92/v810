package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author chin-seong.kee
 */
public class SubtypeSetUserGainCommand extends DataCommand
{
  private SubtypeAdvanceSettings _subtypeAdvanceSettings = null;
  private UserGainEnum _origUserGain = null;
  private UserGainEnum _newUserGain = null;

  /**
   * Construct the class.
   * @author Chin Seong
   */
  public SubtypeSetUserGainCommand(SubtypeAdvanceSettings subtypeAdvanceSettings, UserGainEnum newGain)
  {
    Assert.expect(subtypeAdvanceSettings != null);
    Assert.expect(newGain != null);

    _subtypeAdvanceSettings = subtypeAdvanceSettings;
    _newUserGain = newGain;
    _origUserGain = subtypeAdvanceSettings.getUserGain();
  }

  /**
   * @author Chin Seong
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_subtypeAdvanceSettings != null);

    if (_newUserGain.equals(_origUserGain))
    {
      return false;
    }

    _subtypeAdvanceSettings.setUserGain(_newUserGain);

    return true;
  }

  /**
   * @author Chin Seong
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_subtypeAdvanceSettings != null);

    _subtypeAdvanceSettings.setUserGain(_origUserGain);
  }

  /**
   * @author Chin Seong
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_USER_GAIN_KEY");
  }
}
