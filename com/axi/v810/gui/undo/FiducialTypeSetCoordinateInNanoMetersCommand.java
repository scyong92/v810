package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting a fiducial type coordinate
 * @author Laura Cormos
 */
public class FiducialTypeSetCoordinateInNanoMetersCommand extends DataCommand
{
  boolean isBoardFiducial = false;
  private Panel _panel;
  private String _fiducialName;
  private SideBoardType _sideBoardType;
  // this class with handle coordinate setting for both panel and board fiducials. Even though a board fiducial
  // usually passes in a BoardCoordinate to the fiducialType, internally, fiducialType will take the BoardCoordinate
  // and call the setCoordinate method with it as a PanelCoordinate, because is just a number. The fiducial kind, panel
  // or board knows what the coordinate is relative to.
  private PanelCoordinate _newCoordInNanoMeters = null;
  private PanelCoordinate _oldCoordInNanoMeters = null;

  /**
   * @param fiducialType the fiducial type whose coordinate will change
   * @param newCoord the new coordinate
   * @author Laura Cormos
   */
  public FiducialTypeSetCoordinateInNanoMetersCommand(FiducialType fiducialType , PanelCoordinate newCoord)
  {
    Assert.expect(fiducialType != null);
    Assert.expect(newCoord != null);

    isBoardFiducial = fiducialType.hasSideBoardType();
    if (isBoardFiducial)
      _sideBoardType = fiducialType.getSideBoardType();
    else
      _panel = fiducialType.getFiducials().iterator().next().getPanel();

    _fiducialName = fiducialType.getName();
    _newCoordInNanoMeters = newCoord;
    _oldCoordInNanoMeters = fiducialType.getCoordinateInNanoMeters();
  }

  /**
   * Change the coordinate to the new coordinate
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_fiducialName != null);
    if (isBoardFiducial)
      Assert.expect(_sideBoardType != null);
    else
      Assert.expect(_panel != null);


    if (_oldCoordInNanoMeters.equals(_newCoordInNanoMeters))
      return false;

    // first find the fiducial type object since it could have been deleted and re added by an undo/redo
    FiducialType fiducialType;
    if (isBoardFiducial)
      fiducialType = _sideBoardType.getFiducialType(_fiducialName);
    else
      fiducialType = _panel.getFiducial(_fiducialName).getFiducialType();
    Assert.expect(fiducialType != null);
    fiducialType.setCoordinateInNanoMeters(_newCoordInNanoMeters);
    return true;
  }

  /**
   * Restore the old coordinate
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_fiducialName != null);
    if (isBoardFiducial)
      Assert.expect(_sideBoardType != null);
    else
      Assert.expect(_panel != null);


    // first find the fiducial type object since it could have been deleted and re added by an undo/redo
    FiducialType fiducialType;
    if (isBoardFiducial)
      fiducialType = _sideBoardType.getFiducialType(_fiducialName);
    else
      fiducialType = _panel.getFiducial(_fiducialName).getFiducialType();
    Assert.expect(fiducialType != null);
    fiducialType.setCoordinateInNanoMeters(_oldCoordInNanoMeters);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_FIDUCIAL_TYPE_SET_COORD_NM_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
