package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Khaw Chek Hau
 * XCR2943: Alignment Fail Due to Too Close To Edge
 */
public class ProjectSetEnableEnlargeAlignmentRegionCommand extends DataCommand
{
  private Project _project = null;
  private boolean _newEnlargeAlignmentRegion = false;
  private boolean _oldEnlargeAlignmentRegion = false;

  /**
   * @author Khaw Chek Hau
   * XCR2943: Alignment Fail Due to Too Close To Edge
   */
  public ProjectSetEnableEnlargeAlignmentRegionCommand(Project project, boolean isEnlargeAlignmentRegion)
  {
    Assert.expect(project != null);

    _project = project;
    _newEnlargeAlignmentRegion = isEnlargeAlignmentRegion;
    _oldEnlargeAlignmentRegion = _project.isEnlargeAlignmentRegion();
  }

  /**
   * @author Khaw Chek Hau
   * XCR2943: Alignment Fail Due to Too Close To Edge
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_oldEnlargeAlignmentRegion == _newEnlargeAlignmentRegion)
      return false;

    _project.setEnlargeAlignmentRegion(_newEnlargeAlignmentRegion);
    return true;
  }

  /**
   * @author Khaw Chek Hau
   * XCR2943: Alignment Fail Due to Too Close To Edge
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setEnlargeAlignmentRegion(_oldEnlargeAlignmentRegion);
  }

  /**
   * @author Khaw Chek Hau
   * XCR2943: Alignment Fail Due to Too Close To Edge
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_ENABLE_ENLARGE_ALIGNMENT_REGION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
