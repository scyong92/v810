package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * Command for setting a component type coordinate
 * @author Laura Cormos
 */
public class RemoveFeaturesFromAlignmentGroupCommand extends DataCommand
{
  private AlignmentGroup _alignmentGroup = null;
  private List<Pad> _pads;
  private List<Fiducial> _fiducials;

  /**
   * @param alignmentGroup The alignment group to add the new pad to
   * @param pads the Pads to add in
   * @author Andy Mechtenberg
   */
  public RemoveFeaturesFromAlignmentGroupCommand(AlignmentGroup alignmentGroup, List<Pad> pads, List<Fiducial> fiducials)
  {
    Assert.expect(alignmentGroup != null);
    Assert.expect(pads != null);
    Assert.expect(fiducials != null);

    _alignmentGroup = alignmentGroup;
    _pads = pads;
    _fiducials = fiducials;
  }

  /**
   * Add the pads to the group
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    for(Pad pad : _pads)
      _alignmentGroup.removePad(pad);
    for(Fiducial fiducial : _fiducials)
      _alignmentGroup.removeFiducial(fiducial);
    return true;
  }

  /**
   * Remove the pads (opposite of add)
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    for(Pad pad : _pads)
      _alignmentGroup.addPad(pad);
    for(Fiducial fiducial : _fiducials)
      _alignmentGroup.addFiducial(fiducial);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_REMOVE_FEATURE_FROM_ALIGNMENT_GROUP_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
