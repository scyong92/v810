package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used to set the artifact compensation state for a ComponentType
 * Using this class allows us to undo the set later.
 * @author Laura Cormos
 * author sham
 */
public class ComponentTypeSetUserGainCommand extends DataCommand
{

  private ComponentType _componentType = null;
  private UserGainEnum _origUserGain = null;
  private UserGainEnum _newUserGain = null;

  /**
   * Construct the class.
   * @author Laura Cormos
   * @authir sham
   */
  public ComponentTypeSetUserGainCommand(ComponentType componentType, UserGainEnum newGain)
  {
    Assert.expect(componentType != null);
    Assert.expect(newGain != null);

    _componentType = componentType;
    _newUserGain = newGain;
    _origUserGain = _componentType.getUserGain();
  }

  /**
   * @author Laura Cormos
   * @authir sham
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    if (_newUserGain.equals(_origUserGain))
    {
      return false;
    }

    List<PadType> padTypes = _componentType.getPadTypes();
    for (PadType padType : padTypes)
    {
      padType.getPadTypeSettings().setUserGain(_newUserGain);
    }

    return true;
  }

  /**
   * @author Laura Cormos
   * @authir sham
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    List<PadType> padTypes = _componentType.getPadTypes();
    for (PadType padType : padTypes)
    {
      padType.getPadTypeSettings().setUserGain(_origUserGain);
    }

  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_USER_GAIN_KEY");
  }
}
