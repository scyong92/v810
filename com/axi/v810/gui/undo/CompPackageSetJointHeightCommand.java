package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * This class is used to set the joint height for a CompPackage
 * Using this class allows us to undo the set later.
 * @author Laura Cormos
 */
public class CompPackageSetJointHeightCommand extends DataCommand
{
  private CompPackage _compPackage = null;
  private int _originalJointHeightInNanos = 0;
  private int _newJointHeightInNanos = 0;

  /**
   * @author Laura Cormos
   */
  public CompPackageSetJointHeightCommand(CompPackage compPackage, int newJointHeightInNanos)
  {
    Assert.expect(compPackage != null);

    _compPackage = compPackage;
    _newJointHeightInNanos = newJointHeightInNanos;

    _originalJointHeightInNanos = compPackage.getJointHeightInNanoMeters();
  }

  /**
   * @return true if the command executed, false if new algorithm family is the same as the current one
   * @throws XrayTesterException if the command fails
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_compPackage != null);

    if (_newJointHeightInNanos == _originalJointHeightInNanos)
      return false;

    for (PackagePin packagePin : _compPackage.getPackagePins())
    {
      packagePin.setCustomJointHeightInNanoMeters(_newJointHeightInNanos);
    }
    return true;
  }

  /**
   * @throws XrayTesterException if the command fails
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_compPackage != null);

    for (PackagePin packagePin : _compPackage.getPackagePins())
    {
      packagePin.setCustomJointHeightInNanoMeters(_originalJointHeightInNanos);
    }
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_COMP_PACKAGE_SET_JOINT_HEIGHT_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
