package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author siew-yeng.phang
 */
public class PshSettingsSetEnableJointTypeCommand extends DataCommand
{
  private PshSettings _pshSettings = null;
  private JointTypeEnum _jointTypeEnum = null;
  private boolean _newStatus = false;
  private boolean _oldStatus = false;
  
  /**
   * @author Siew Yeng
   */
  public PshSettingsSetEnableJointTypeCommand(PshSettings pshSettings, JointTypeEnum jointType, boolean enable)
  {
    Assert.expect(pshSettings != null);
    Assert.expect(jointType != null);

    _pshSettings = pshSettings;
    _jointTypeEnum = jointType;
    _newStatus = enable;
    _oldStatus = pshSettings.isJointTypeEnabled(jointType);
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean executeCommand() throws XrayTesterException 
  {
    if(_newStatus == _oldStatus)
      return false;
    
    _pshSettings.setEnabledJointType(_jointTypeEnum, _newStatus);
    
    return true;
  }

  /**
   * @author Siew Yeng
   */
  public void undoCommand() throws XrayTesterException 
  {
    _pshSettings.setEnabledJointType(_jointTypeEnum, _oldStatus);
  }

  /**
   * @author Siew Yeng
   */
  public String getDescription() 
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PSH_SET_ENABLE_JOINT_TYPE_COMMAND_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
  
}
