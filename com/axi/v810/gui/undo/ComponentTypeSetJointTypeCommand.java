package com.axi.v810.gui.undo;

import java.util.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used to set the Joint Type for a ComponentType
 * Using this class allows us to undo the set later.
 * @author Andy Mechtenberg
 */
public class ComponentTypeSetJointTypeCommand extends DataCommand
{
  private ComponentType _componentType = null;
  private JointTypeEnum _origJointType = null;
  private JointTypeEnum _newJointType = null;
  
  // [XCR-2059] - Ying-Huan.Chu
  private boolean _carryForwardSettings = false;
  //Siew Yeng
  private Set<Subtype> _origSubtypes = new HashSet<Subtype>();
  private Map<String, Subtype> originalPadTypeToSubtypeMap = new HashMap<String, Subtype>();

  /**
   * Construct the class.
   * @author Andy Mechtenberg
   */
  public ComponentTypeSetJointTypeCommand(ComponentType componentType, JointTypeEnum newJointType)
  {
    Assert.expect(componentType != null);
    Assert.expect(newJointType != null);

    _componentType = componentType;
    _newJointType = newJointType;
    List<JointTypeEnum> jointTypes = _componentType.getJointTypeEnums();
    String currentJointTypes = "";
    for(JointTypeEnum jte : jointTypes)
    {
      currentJointTypes = currentJointTypes + jte.toString() + " ";
    }
    Assert.expect(jointTypes.size() == 1, "Comp: " + _componentType.getReferenceDesignator() + " using joint types: " + currentJointTypes + " to joint type: " + _newJointType.toString());  // can't do this operation on a mixed joint type
    _origJointType = jointTypes.get(0);
  }
  
  public ComponentTypeSetJointTypeCommand(ComponentType componentType, JointTypeEnum newJointType, boolean carryForwardSettings)
  {
    Assert.expect(componentType != null);
    Assert.expect(newJointType != null);

    _componentType = componentType;
    _newJointType = newJointType;
    _carryForwardSettings = carryForwardSettings;
    List<JointTypeEnum> jointTypes = _componentType.getJointTypeEnums();
    String currentJointTypes = "";
    for(JointTypeEnum jte : jointTypes)
    {
      currentJointTypes = currentJointTypes + jte.toString() + " ";
    }
    if (_carryForwardSettings)
    {
//      _origSubtypes = new ArrayList<>();
      //Siew Yeng
      for(PadType padType : _componentType.getPadTypes())
      {
        originalPadTypeToSubtypeMap.put(padType.getComponentAndPadName(), padType.getSubtype());
      }
      _origSubtypes.addAll(originalPadTypeToSubtypeMap.values());
    }
    Assert.expect(jointTypes.size() == 1, "Comp: " + _componentType.getReferenceDesignator() + " using joint types: " + currentJointTypes + " to joint type: " + _newJointType.toString());  // can't do this operation on a mixed joint type
    _origJointType = jointTypes.get(0);
  }

  /**
   * Change the joint type for a component type
   * @return true if the command executed, false if new joint type is the same as the current one
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    if (_newJointType.equals(_origJointType))
      return false;

    _componentType.setJointTypeEnum(_newJointType);
    if (_carryForwardSettings)
    {
      for (Subtype subtype : _componentType.getSubtypes())
      {
        for (Subtype origSubtype : _origSubtypes)
        {
          if (origSubtype.getShortName().startsWith(subtype.getShortName()))
          {
            //To copy the settings to new subtype
            if(origSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false)
              subtype.getSubtypeAdvanceSettings().setArtifactCompensationState(origSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState());

            subtype.getSubtypeAdvanceSettings().setCanUseVariableMagnification(origSubtype.getSubtypeAdvanceSettings().canUseVariableMagnification());
            subtype.getSubtypeAdvanceSettings().setIsInterferenceCompensatable(origSubtype.getSubtypeAdvanceSettings().isIsInterferenceCompensatable());
            subtype.getSubtypeAdvanceSettings().setMagnificationType(origSubtype.getSubtypeAdvanceSettings().getMagnificationType());
            subtype.getSubtypeAdvanceSettings().setSignalCompensation(origSubtype.getSubtypeAdvanceSettings().getSignalCompensation());
            subtype.getSubtypeAdvanceSettings().setUserGain(origSubtype.getSubtypeAdvanceSettings().getUserGain());
            subtype.getSubtypeAdvanceSettings().setStageSpeedEnum(origSubtype.getSubtypeAdvanceSettings().getStageSpeedList());
          }
        }
      }
    }
    return true;
  }

  /**
   * Reset the joint type
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    _componentType.setJointTypeEnum(_origJointType);
    if (_carryForwardSettings)
    {
      //Siew Yeng
      for(PadType padType : _componentType.getPadTypes())
      {
        padType.getPadTypeSettings().setSubtype(originalPadTypeToSubtypeMap.get(padType.getComponentAndPadName()));
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    // do a KEY lookup to get the proper localized string here
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_COMPONENT_TYPE_SET_JOINTTYPE_KEY", new Object[]{_componentType.getReferenceDesignator()});
    return StringLocalizer.keyToString(localizedString);
  }
}
