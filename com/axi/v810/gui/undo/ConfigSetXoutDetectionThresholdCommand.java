package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
public class ConfigSetXoutDetectionThresholdCommand extends DataCommand
{
  private int _originalThreshold;
  private int _newThreshold;
  private TestExecution _sampling = TestExecution.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetXoutDetectionThresholdCommand(int newThreshold)
  {
    _newThreshold = newThreshold;
    _originalThreshold = _sampling.getXoutDetectionThreshold();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalThreshold == _newThreshold)
      return false;

    _sampling.setXoutDetectionThreshold(_newThreshold);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _sampling.setXoutDetectionThreshold(_originalThreshold);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_XOUT_DETECTION_THRESHOLD_KEY");
  }
}
