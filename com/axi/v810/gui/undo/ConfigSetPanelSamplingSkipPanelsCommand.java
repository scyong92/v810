package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ConfigSetPanelSamplingSkipPanelsCommand extends DataCommand
{
  private int _originalPanelsSkipped;
  private int _newPanelsSkipped;
  private Sampling _sampling = Sampling.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetPanelSamplingSkipPanelsCommand(int newPanelsSkipped)
  {
    _newPanelsSkipped = newPanelsSkipped;
    _originalPanelsSkipped = _sampling.getSkipEveryNthPanel();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalPanelsSkipped == _newPanelsSkipped)
      return false;

    _sampling.setSkipEveryNthPanel(_newPanelsSkipped);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _sampling.setSkipEveryNthPanel(_originalPanelsSkipped);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_PANEL_SAMPLING_SKIP_FREQUENCY_KEY");
  }
}
