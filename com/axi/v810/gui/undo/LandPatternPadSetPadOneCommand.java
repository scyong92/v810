package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting a land pattern pad PAD ONE
 * @author Laura Cormos
 */
public class LandPatternPadSetPadOneCommand extends DataCommand
{
  private LandPatternPad _newLandPatternPadOne = null;
  private LandPatternPad _oldLandPatternPadOne = null;

  /**
   * @param landPatternPad the land pattern pad that will be assigned to be pad one
   * @author Laura Cormos
   */
  public LandPatternPadSetPadOneCommand(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    _newLandPatternPadOne = landPatternPad;
    _oldLandPatternPadOne = landPatternPad.getLandPattern().getLandPatternPadOne();
  }

  /**
   * Change the pad one assignemt
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_newLandPatternPadOne != null);
    Assert.expect(_oldLandPatternPadOne != null);

    if (_oldLandPatternPadOne == _newLandPatternPadOne)
      return false;

    _newLandPatternPadOne.setPadOne(true);
    return true;
  }

  /**
   * Restore the old pad one assignment
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_newLandPatternPadOne != null);
    Assert.expect(_oldLandPatternPadOne != null);

    _oldLandPatternPadOne.setPadOne(true);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_LAND_PATTERN_PAD_SET_PAD_ONE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
