package com.axi.v810.gui.undo;

import com.axi.util.Assert;
import com.axi.v810.util.XrayTesterException;
import com.axi.v810.datastore.Directory;
import com.axi.v810.util.StringLocalizer;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class ConfigSetInitialThresholdDirectoryCommand extends DataCommand
{
  private String _originalThresholdDirectory;
  private String _newThresholdDirectory;

  /**
   * @author Wei Chin, Chong
   */
  public ConfigSetInitialThresholdDirectoryCommand(String newThresholdDirectory)
  {
    Assert.expect(newThresholdDirectory != null);

    _newThresholdDirectory = newThresholdDirectory;
    _originalThresholdDirectory = Directory.getInitialThresholdsDir();
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_newThresholdDirectory != null);
    Assert.expect(_originalThresholdDirectory != null);

    if (_originalThresholdDirectory.equalsIgnoreCase(_newThresholdDirectory))
    {
      return false;
    }

    Directory.setInitialThresholdsDir(_newThresholdDirectory);
    return true;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_originalThresholdDirectory != null);

    Directory.setInitialThresholdsDir(_originalThresholdDirectory);
  }

  /**
   * @author Wei Chin, Chong
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_INITIAL_THRESHOLD_DIRECTORY_KEY");
  }
}
