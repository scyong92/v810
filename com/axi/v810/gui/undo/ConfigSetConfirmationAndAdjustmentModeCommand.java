package com.axi.v810.gui.undo;

import com.axi.v810.business.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 *
 * @author siew-yeng.phang
 */
public class ConfigSetConfirmationAndAdjustmentModeCommand extends DataCommand
{
  private ConfirmationAndAdjustmentModeEnum _originalConfirmationAndAdjustmentModeMode;
  private ConfirmationAndAdjustmentModeEnum _newConfirmationAndAdjustmentModeMode;
  private Config _config = Config.getInstance();

  /**
   * @author Phang Siew Yeng
   */
  public ConfigSetConfirmationAndAdjustmentModeCommand(ConfirmationAndAdjustmentModeEnum systemConfirmationAndAdjustmentMode)
  {
    _newConfirmationAndAdjustmentModeMode = systemConfirmationAndAdjustmentMode;
    
    if(_config.getStringValue(SoftwareConfigEnum.CONFIRMATION_AND_ADJUSTMENT_MODE).equals(ConfirmationAndAdjustmentModeEnum.STANDARD_MODE.toString()))
      _originalConfirmationAndAdjustmentModeMode = ConfirmationAndAdjustmentModeEnum.STANDARD_MODE;
    else
      _originalConfirmationAndAdjustmentModeMode = ConfirmationAndAdjustmentModeEnum.PASSIVE_MODE;
  }

  /**
   * @author Phang Siew Yeng
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalConfirmationAndAdjustmentModeMode.equals(_newConfirmationAndAdjustmentModeMode))
      return false;

    _config.setValue(SoftwareConfigEnum.CONFIRMATION_AND_ADJUSTMENT_MODE, _newConfirmationAndAdjustmentModeMode.toString());
    
    return true;
  }

  /**
   * @author Phang Siew Yeng
   */
  public void undoCommand() throws XrayTesterException
  {
     _config.setValue(SoftwareConfigEnum.CONFIRMATION_AND_ADJUSTMENT_MODE, _originalConfirmationAndAdjustmentModeMode.toString());
  }

  /**
   * @author Phang Siew Yeng
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_CONFIRMATION_AND_ADJUSTMENT_MODE_KEY");
  }
  
}
