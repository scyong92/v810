package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.ProjectHistoryLog;
import com.axi.v810.util.*;

/**
 * This class is used to set the subtype (CustomAlgorithmFamily) for a ComponentType
 * Using this class allows us to undo the set later.
 * @author Andy Mechtenberg
 */
public class ComponentTypeSetTestedCommand extends DataCommand
{
  private ComponentType _componentType = null;
  private boolean _origTestStatus = false;
  private boolean _newTestStatus = false;
  private boolean _origLoadStatus = false;
  private static transient ProjectHistoryLog _fileWriter;

  /**
   * @param componentType the ComponentType whose subtype will change
   * @param testStatus the new test status
   * @author Andy Mechtenberg
   */
  public ComponentTypeSetTestedCommand(ComponentType componentType, boolean testStatus)
  {
    Assert.expect(componentType != null);

    _componentType = componentType;
    _newTestStatus = testStatus;
    _origTestStatus = _componentType.getComponentTypeSettings().isInspected();
    _origLoadStatus = _componentType.getComponentTypeSettings().isLoaded();
    _fileWriter = ProjectHistoryLog.getInstance();
  }

  /**
   * Change the custom algorithm family (subtype) for a component type
   * @return true if the command executed, false if new test status is the same as the current one
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    if (_newTestStatus == _origTestStatus)
      return false;

    _componentType.getComponentTypeSettings().setInspected(_newTestStatus);
    return true;
  }

  /**
   * Reset the Custom Algorithm Family (subtype)
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    _componentType.getComponentTypeSettings().setInspected(_origTestStatus);
    if ((_origTestStatus == false) && _origLoadStatus) //if orig test status is No Test and load status is loaded
      _fileWriter.appendUndoSubtypeToNoTest(_componentType);
    else if (_origTestStatus) //testable 
      _fileWriter.appendUndoNoTestSubtype(_componentType, _componentType.getSubtypes().toString());
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_COMP_TYPE_SET_TESTED_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
