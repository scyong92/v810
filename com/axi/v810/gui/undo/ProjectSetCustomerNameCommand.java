package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the customer name of the project is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ProjectSetCustomerNameCommand extends DataCommand
{
  private Project _project;
  private String _originalCustomerName;
  private String _newCustomerName;

  /**
   * @param project the project whose customer name will change
   * @param newCustomerName the new customer name of the panel
   * @author Andy Mechtenberg
   */
  public ProjectSetCustomerNameCommand(Project project, String newCustomerName)
  {
    Assert.expect(project != null);
    Assert.expect(newCustomerName != null);

    _project = project;
    _newCustomerName = newCustomerName;
    _originalCustomerName = _project.getTargetCustomerName();
  }

  /**
   * Set the new customer name of the project
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_originalCustomerName.equalsIgnoreCase(_newCustomerName))
      return false;

    _project.setTargetCustomerName(_newCustomerName);
    return true;
  }

  /**
   * Reset the customer name of the project
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setTargetCustomerName(_originalCustomerName);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_CUSTOMER_NAME_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
