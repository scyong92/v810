package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the programmer name of the project is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ProjectSetProgrammerNameCommand extends DataCommand
{
  private Project _project;
  private String _originalProgrammerName;
  private String _newProgrammerName;

  /**
   * @param project the project whose Programmer name will change
   * @param newProgrammerName the new Programmer name of the panel
   * @author Andy Mechtenberg
   */
  public ProjectSetProgrammerNameCommand(Project project, String newProgrammerName)
  {
    Assert.expect(project != null);
    Assert.expect(newProgrammerName != null);

    _project = project;
    _newProgrammerName = newProgrammerName;
    _originalProgrammerName = _project.getProgrammerName();
  }

  /**
   * Set the new Programmer name of the project
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_originalProgrammerName.equalsIgnoreCase(_newProgrammerName))
      return false;

    _project.setProgrammerName(_newProgrammerName);
    return true;
  }

  /**
   * Reset the Programmer name of the project
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setProgrammerName(_originalProgrammerName);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_PROGRAMMER_NAME_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
