package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the phone number of config is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ConfigSetPhoneNumberCommand extends DataCommand
{
  private String _originalPhoneNumber;
  private String _newPhoneNumber;
  private CustomerInfo _customerInfo = CustomerInfo.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetPhoneNumberCommand(String newPhoneNumber)
  {
    Assert.expect(newPhoneNumber != null);

    _newPhoneNumber = newPhoneNumber;
    _originalPhoneNumber = _customerInfo.getPhoneNumber();
  }

  /**
   * Set the phone number
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalPhoneNumber.equalsIgnoreCase(_newPhoneNumber))
      return false;

    _customerInfo.setPhoneNumber(_newPhoneNumber);
    return true;
  }

  /**
   * Reset the phone number
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _customerInfo.setPhoneNumber(_originalPhoneNumber);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_PHONE_NUMBER_KEY");
  }
}
