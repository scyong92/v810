package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting a fiducial type width
 * @author Laura Cormos
 */
public class FiducialTypeSetWidthInNanoMetersCommand extends DataCommand
{
  boolean isBoardFiducial = false;
  private Panel _panel;
  private SideBoardType _sideBoardType;
  private String _fiducialName;
  private int _newWidthInNanoMeters = 0;
  private int _oldWidthInNanoMeters = 0;

  /**
   * @param fiducialType the fiducial type whose width will change
   * @param newWidth the new width
   * @author Laura Cormos
   */
  public FiducialTypeSetWidthInNanoMetersCommand(FiducialType fiducialType , int newWidth)
  {
    Assert.expect(fiducialType != null);
    Assert.expect(newWidth != 0);

    isBoardFiducial = fiducialType.hasSideBoardType();
    if (isBoardFiducial)
      _sideBoardType = fiducialType.getSideBoardType();
    else
      _panel = fiducialType.getFiducials().iterator().next().getPanel();
    _fiducialName = fiducialType.getName();
    _newWidthInNanoMeters = newWidth;
    _oldWidthInNanoMeters = fiducialType.getWidthInNanoMeters();
  }

  /**
   * Change the width to the new width
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_fiducialName != null);
    if (isBoardFiducial)
      Assert.expect(_sideBoardType != null);
    else
      Assert.expect(_panel != null);

    if (_oldWidthInNanoMeters == _newWidthInNanoMeters)
      return false;

    // first find the fiducial type object since it could have been deleted and re added by an undo/redo
    FiducialType fiducialType;
    if (isBoardFiducial)
      fiducialType = _sideBoardType.getFiducialType(_fiducialName);
    else
      fiducialType = _panel.getFiducial(_fiducialName).getFiducialType();
    Assert.expect(fiducialType != null);
    fiducialType.setWidthInNanoMeters(_newWidthInNanoMeters);
    return true;
  }

  /**
   * Restore the old width
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_fiducialName != null);
    if (isBoardFiducial)
      Assert.expect(_sideBoardType != null);
    else
      Assert.expect(_panel != null);

    // first find the fiducial type object since it could have been deleted and re added by an undo/redo
    FiducialType fiducialType;
    if (isBoardFiducial)
      fiducialType = _sideBoardType.getFiducialType(_fiducialName);
    else
      fiducialType = _panel.getFiducial(_fiducialName).getFiducialType();
    Assert.expect(fiducialType != null);
    fiducialType.setWidthInNanoMeters(_oldWidthInNanoMeters);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_FIDUCIAL_TYPE_SET_WIDTH_NM_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
