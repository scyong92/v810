package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * This class is used to set the width of the panel.
 * Using this class allows us to undo the set later.
 * @author George A. David
 */
public class PanelSetWidthCommand extends DataCommand
{
  private Panel _panel;
  private int _originalWidthInNanoMeters = -1;
  private int _newWidthInNanoMeters = -1;

  /**
   * @param panel the panel whose with will change
   * @param newWidthInNanoMeters the new width of the panel in nanometers
   * @author George A. David
   */
  public PanelSetWidthCommand(Panel panel, int newWidthInNanoMeters)
  {
    Assert.expect(panel != null);
    Assert.expect(newWidthInNanoMeters > 0);

    _panel = panel;
    _newWidthInNanoMeters = newWidthInNanoMeters;
    _originalWidthInNanoMeters = _panel.getWidthAfterAllRotationsInNanoMeters();
  }

  /**
   * Change the width of the panel
   * @author George A. David
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_panel != null);

    if (MathUtil.fuzzyEquals(_originalWidthInNanoMeters, _newWidthInNanoMeters, MathUtil._UNIT_CONVERSION_NANOMETER_LIMIT))
      return false;

    _panel.setWidthAfterAllRotationsRetainingBoardCoordsInNanoMeters(_newWidthInNanoMeters);
    return true;
  }

  /**
   * Reset the width of the panel
   * @author George A. David
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_panel != null);

    _panel.setWidthAfterAllRotationsRetainingBoardCoordsInNanoMeters(_originalWidthInNanoMeters);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PANEL_SET_WIDTH_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
