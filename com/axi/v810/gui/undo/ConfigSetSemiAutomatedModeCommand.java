package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Phang Siew Yeng
 */
public class ConfigSetSemiAutomatedModeCommand extends DataCommand
{
  private boolean _originalSemiAutomatedMode;
  private boolean _newSemiAutomatedMode;
  private TestExecution _testExecution = TestExecution.getInstance();

  /**
   * @author Phang Siew Yeng
   */
  public ConfigSetSemiAutomatedModeCommand(boolean newSemiAutomatedMode)
  {
    _newSemiAutomatedMode = newSemiAutomatedMode;
    _originalSemiAutomatedMode = _testExecution.isSemiAutomatedModeEnabled();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalSemiAutomatedMode == _newSemiAutomatedMode)
      return false;

    _testExecution.setSemiAutomatedMode(_newSemiAutomatedMode);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _testExecution.setSemiAutomatedMode(_originalSemiAutomatedMode);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_SEMI_AUTOMATED_MODE_KEY");
  }
}
