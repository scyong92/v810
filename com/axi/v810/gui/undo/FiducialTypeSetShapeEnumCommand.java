package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting a fiducial type shape
 * @author Laura Cormos
 */
public class FiducialTypeSetShapeEnumCommand extends DataCommand
{
  boolean isBoardFiducial = false;
  private Panel _panel;
  private SideBoardType _sideBoardType;
  private String _fiducialName;
  private ShapeEnum _newShapeEnum = null;
  private ShapeEnum _oldShapeEnum = null;

  /**
   * @param fiducialType the fiducial type whose shape will change
   * @param newShapeEnum the new shape
   * @author Laura Cormos
   */
  public FiducialTypeSetShapeEnumCommand(FiducialType fiducialType , ShapeEnum newShapeEnum)
  {
    Assert.expect(fiducialType != null);
    Assert.expect(newShapeEnum != null);

    isBoardFiducial = fiducialType.hasSideBoardType();
    if (isBoardFiducial)
      _sideBoardType = fiducialType.getSideBoardType();
    else
      _panel = fiducialType.getFiducials().iterator().next().getPanel();
    _fiducialName = fiducialType.getName();
    _newShapeEnum = newShapeEnum;
    _oldShapeEnum = fiducialType.getShapeEnum();
  }

  /**
   * Change the shape to the new shape
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_fiducialName != null);
    if (isBoardFiducial)
      Assert.expect(_sideBoardType != null);
    else
      Assert.expect(_panel != null);

    if (_oldShapeEnum.equals(_newShapeEnum))
      return false;

    // first find the fiducial type object since it could have been deleted and re added by an undo/redo
    FiducialType fiducialType;
    if (isBoardFiducial)
      fiducialType = _sideBoardType.getFiducialType(_fiducialName);
    else
      fiducialType = _panel.getFiducial(_fiducialName).getFiducialType();
    Assert.expect(fiducialType != null);
    fiducialType.setShapeEnum(_newShapeEnum);
    return true;
  }

  /**
   * Restore the old shape
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_fiducialName != null);
    if (isBoardFiducial)
      Assert.expect(_sideBoardType != null);
    else
      Assert.expect(_panel != null);


    // first find the fiducial type object since it could have been deleted and re added by an undo/redo
    FiducialType fiducialType;
    if (isBoardFiducial)
      fiducialType = _sideBoardType.getFiducialType(_fiducialName);
    else
      fiducialType = _panel.getFiducial(_fiducialName).getFiducialType();
    Assert.expect(fiducialType != null);
    fiducialType.setShapeEnum(_oldShapeEnum);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_FIDUCIAL_TYPE_SET_SHAPE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
