/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 *
 * @author Jack Hwee
 */
public class FalseCallTriggeringAddDynamicCallNumberCommand extends DataCommand
{
  private java.util.List<String> _dynamicCallNumberList;
  private java.util.List<String> _oldDynamicCallNumberList;
  private int _totalComponent;
  private int _totalError;
  private FalseCallMonitoring _falseCallMonitoring;
  
  /**
   * @param newCoord the new coordinate
   * @author Laura Cormos
   */
  public FalseCallTriggeringAddDynamicCallNumberCommand(java.util.List<String> dynamicCallNumberList, int totalComponent, int totalError)
  {
    Assert.expect(dynamicCallNumberList != null);
    
    _totalComponent = totalComponent;
    _totalError = totalError;
    _dynamicCallNumberList = dynamicCallNumberList;
    _oldDynamicCallNumberList = FalseCallMonitoring.getInstance().getDynamicCallNumber();
    _falseCallMonitoring = FalseCallMonitoring.getInstance();
  }

  /**
   * Change the coordinate to the new coordinate
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_dynamicCallNumberList != null); 

    String dynamicCallNumber = Integer.toString(_totalComponent) + "," + Integer.toString(_totalError);
      
    _dynamicCallNumberList.add(Integer.toString(_totalComponent));
      
    _dynamicCallNumberList.add(Integer.toString(_totalError));
         
    _falseCallMonitoring.setDynamicCallNumber(_dynamicCallNumberList);
   
    return true;
  }

  /**
   * Restore the old coordinate
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_oldDynamicCallNumberList != null);
    
    _oldDynamicCallNumberList = FalseCallMonitoring.getInstance().getDynamicCallNumber();
    
    _dynamicCallNumberList.clear();
    
    for (String dynamicCallNumber : _oldDynamicCallNumberList)
    {
        if ((Integer.parseInt(dynamicCallNumber) == _totalComponent || Integer.parseInt(dynamicCallNumber) == _totalError) == false)
            _dynamicCallNumberList.add(dynamicCallNumber);        
    }
   
    _falseCallMonitoring.setDynamicCallNumber(_dynamicCallNumberList);
    
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_FALSE_CALL_TRIGGERING_ADD_DYNAMIC_CALL_NUMBER_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}

