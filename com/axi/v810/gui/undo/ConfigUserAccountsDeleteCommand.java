package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.util.*;

/**
 * Command to delete an existing user acount
 * @author George Booth
 */
public class ConfigUserAccountsDeleteCommand extends DataCommand
{
  private UserAccountsManager _userAccountsManager = UserAccountsManager.getInstance();
  private UserAccountInfo _userAccount = null;

  /**
   * @author George Booth
   */
  public ConfigUserAccountsDeleteCommand(UserAccountInfo userAccount)
  {
    Assert.expect(userAccount != null);

    _userAccount = userAccount;
  }

  /**
   * @author George Booth
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_userAccount != null);

    _userAccountsManager.deleteAccount(_userAccount);
    return true;
  }

  /**
   * @author George Booth
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_userAccount != null);

    _userAccountsManager.addAccount(_userAccount);
  }

  /**
   * @author George Booth
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_DELETE_USER_ACCOUNT_KEY");
  }
}
