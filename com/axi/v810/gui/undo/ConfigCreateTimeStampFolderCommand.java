package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.config.*;
import com.axi.v810.util.*;

/**
 * Command for setting the create folder checkbox
 * @author Jack Hwee
 */
public class ConfigCreateTimeStampFolderCommand extends DataCommand
{
  private boolean _newEnabledState;
  private boolean _oldEnabledState;
  private ResultsProcessingResultsTabPanel _parent;
  private Config _config = Config.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigCreateTimeStampFolderCommand(boolean enabledState, ResultsProcessingResultsTabPanel parent)
  {
    Assert.expect(parent != null);

    _newEnabledState = enabledState;
    _oldEnabledState = _config.getBooleanValue(SoftwareConfigEnum.ENABLE_CREATE_TIME_STAMP_FOLDER_FOR_FILE_TRANSFER);
    
    _parent = parent;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    _parent.setCreateTimeStampFolderCheckboxState(_newEnabledState);
    if (_oldEnabledState == _newEnabledState)
      return false;

    _config.setValue(SoftwareConfigEnum.ENABLE_CREATE_TIME_STAMP_FOLDER_FOR_FILE_TRANSFER, _newEnabledState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _parent.setCreateTimeStampFolderCheckboxState(_oldEnabledState);
    _config.setValue(SoftwareConfigEnum.ENABLE_CREATE_TIME_STAMP_FOLDER_FOR_FILE_TRANSFER, _oldEnabledState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_RESPROC_ENABLE_CREATE_TIME_STAMP_FOLDER_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
