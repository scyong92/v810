package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.ProjectHistoryLog;
import com.axi.v810.util.*;

/**
 * This class is used to set the subtype (CustomAlgorithmFamily) for a ComponentType
 * Using this class allows us to undo the set later.
 * @author Andy Mechtenberg
 */
public class ComponentTypeSetLoadedCommand extends DataCommand
{
  private ComponentType _componentType = null;
  private boolean _origLoadStatus = false;
  private boolean _newLoadStatus = false;
  private boolean _origTestStatus = false;
  private static transient ProjectHistoryLog _fileWriter;

  /**
   * Construct the class.
   * @param componentType the ComponentType whose subtype will change
   * @param loadStatus the new load status
   * @author Andy Mechtenberg
   */
  public ComponentTypeSetLoadedCommand(ComponentType componentType, boolean loadStatus)
  {
    Assert.expect(componentType != null);

    _componentType = componentType;
    _newLoadStatus = loadStatus;
    _origLoadStatus = _componentType.getComponentTypeSettings().isLoaded();
    _origTestStatus = _componentType.getComponentTypeSettings().isInspected();
    _fileWriter = ProjectHistoryLog.getInstance();
  }

  /**
   * Change the custom algorithm family (subtype) for a component type
   * @return true if the command executed, false if new load status is the same as the current one
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    if (_newLoadStatus == _origLoadStatus)
      return false;

    _componentType.getComponentTypeSettings().setLoaded(_newLoadStatus);
    return true;
  }

  /**
   * Reset the Custom Algorithm Family (subtype)
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    _componentType.getComponentTypeSettings().setLoaded(_origLoadStatus);
    if (_origLoadStatus == false && _origTestStatus == false) // if orig load status is No Load & test status is No test
      _fileWriter.appendUndoSubtypeToNoLoad(_componentType);
    else if (_origLoadStatus  && (_origTestStatus == false)) // load status is loaded and test status is no test 
      _fileWriter.appendUndoNoLoadToNoTestSubtype(_componentType);
    else if (_origLoadStatus  && _origTestStatus && _newLoadStatus) // loaded,testable and new load status is load
      _fileWriter.appendUndoNoLoadToNoTestSubtype(_componentType);
    else if (_origLoadStatus) //loaded
      _fileWriter.appendUndoNoLoadSubtype(_componentType, _componentType.getSubtypes().toString());
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_COMP_TYPE_SET_LOADED_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
