package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * Command for undo the dump Z-height in measurements XML in config enum
 * @author huai-en.janan-ezekie
 */
public class ConfigSetDumpZHeightInMeasurementsXML extends DataCommand
{
  private boolean _originalDumpZHeightInMeasurementsXMLState;
  private boolean _newSetDumpZHeightInMeasurementsXML;
  private TestExecution _testExecution = TestExecution.getInstance();
  
  public ConfigSetDumpZHeightInMeasurementsXML(boolean newSetDumpZHeightInMeasurementsXML)
  {
    _newSetDumpZHeightInMeasurementsXML = newSetDumpZHeightInMeasurementsXML;
    _originalDumpZHeightInMeasurementsXMLState = _testExecution.isDumpZHeightInMeasurementsEnabled();
  }
  
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalDumpZHeightInMeasurementsXMLState == _newSetDumpZHeightInMeasurementsXML)
    {
      return false;
    }

    _testExecution.setDumpZHeightInMeasurementsXML(_newSetDumpZHeightInMeasurementsXML);
    return true;
  }
  
  public void undoCommand() throws XrayTesterException
  {
    _testExecution.setDumpZHeightInMeasurementsXML(_originalDumpZHeightInMeasurementsXMLState);
  }
 
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_ENABLE_DUMP_Z_HEIGHT_IN_MEASUREMENTS_XML_KEY");
  }
}
