/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.util.*;

/**
 *
 * @author Ying-Huan.Chu
 */
public class SurfaceMapAddComponentListCommand extends DataCommand
{
  private List<Component> _componentsToAdd  = new ArrayList<Component>();
  private OpticalRegion _opticalRegion;
  private boolean _isPanelBasedAlignment;
  
  private java.util.List<Board> _previousBoardSelection = null;
    
  /**
   * @author Ying-Huan.Chu
   */
  public SurfaceMapAddComponentListCommand(OpticalRegion opticalRegion,  List<Component> componentsToAdd, boolean isPanelBasedAlignment)
  {
    Assert.expect(opticalRegion != null);
    Assert.expect(componentsToAdd != null);

    _opticalRegion = opticalRegion;
    _componentsToAdd = componentsToAdd;
    _isPanelBasedAlignment = isPanelBasedAlignment;
    
    _previousBoardSelection = PspAutoPopulatePanel.getInstance().getBoardsToApplyOpticalCameraRectangles();
  }

  /**
   * @author Ying-Huan.Chu
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_opticalRegion != null);  

    for (Component component : _componentsToAdd)
    {
      if (_isPanelBasedAlignment)
      {
        _opticalRegion.addComponent(component);
      }
      else
      {
        _opticalRegion.addComponent(component.getBoard(), component);
        PspAutoPopulatePanel.getInstance().setBoardsToApplyOpticalCameraRectangle(_previousBoardSelection);
      }
    }

    return true;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_opticalRegion != null);
    
    for (Component component : _componentsToAdd)
    {
      if (_isPanelBasedAlignment)
      {
        _opticalRegion.removeComponent(component);
      }
      else
      {
        List<Board> boards = _opticalRegion.getBoards();
        for(Board board : boards)
        {
          _opticalRegion.removeComponent(component, board);
        }
      }
    }
  }

  /**
   * @author Ying-Huan.Chu
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_ADD_COMPONENT_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
