package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting the joint type assignment rule's number of columns pattern
 * @author Laura Cormos
 */
public class ConfigSetJointTypeRuleNumColsPatternCommand extends DataCommand
{
  private JointTypeEnumRule _jointTypeEnumRule = null;
  private String _newNumColsPattern = null;
  private String _oldNumColsPattern = null;

  /**
   * @author Laura Cormos
   */
  public ConfigSetJointTypeRuleNumColsPatternCommand(JointTypeEnumRule jointTypeEnumRule , String numColsPattern)
  {
    Assert.expect(jointTypeEnumRule != null);
    Assert.expect(numColsPattern != null);

    _jointTypeEnumRule = jointTypeEnumRule;
    _newNumColsPattern = numColsPattern;
    if (_jointTypeEnumRule.isNumColsSet())
      _oldNumColsPattern = _jointTypeEnumRule.getNumColsPattern();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    if (_oldNumColsPattern != null && _oldNumColsPattern.equals(_newNumColsPattern))
      return false;

    if (_newNumColsPattern.length() == 0)
        _jointTypeEnumRule.clearNumColsPattern();
    else
    _jointTypeEnumRule.setNumColsPattern(_newNumColsPattern);

    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    if (_oldNumColsPattern == null)
      _jointTypeEnumRule.clearNumColsPattern();
    else
      _jointTypeEnumRule.setNumColsPattern(_oldNumColsPattern);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_JTA_SET_JOINT_TYPE_RULE_NUM_COLS_PATTERN_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
