package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.util.*;
import com.axi.v810.gui.config.ResultsTableModelCondition;

/**
 * This class is used so that if a results condition key is changed,
 * it can be reset via an undo command.
 * @author Laura Cormos
 */
public class ConfigSetResultsConditionKeyCommand extends DataCommand
{
  private ConditionKeyEnum _originalConditionKeyEnum;
  private ConditionKeyEnum _newConditionKeyEnum;
  private ResultsTableModelCondition _resultsCondition;

  /**
   * @author Laura Cormos
   */
  public ConfigSetResultsConditionKeyCommand(ResultsTableModelCondition resultsCondition, ConditionKeyEnum conditionKeyEnum)
  {
    Assert.expect(resultsCondition != null);
    Assert.expect(conditionKeyEnum != null);

    _newConditionKeyEnum = conditionKeyEnum;
    _resultsCondition = resultsCondition;
    _originalConditionKeyEnum = _resultsCondition.getKey();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalConditionKeyEnum.equals(_newConditionKeyEnum))
      return false;

    _resultsCondition.setKey(_newConditionKeyEnum);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _resultsCondition.setKey(_originalConditionKeyEnum);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_RESPROC_SET_CONDITION_KEY_ENUM_KEY");
  }
}
