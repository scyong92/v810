package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.TestExecution;
import com.axi.v810.util.*;

/**
 * @author Khaw Chek Hau
 * @XCR3063: Notification Message When Using DRO 
 */
public class ConfigEnablePromptWarningMsgIfModifyDROLevelCommand extends DataCommand
{
  private boolean _originalStatus;
  private boolean _newStatus;
  private TestExecution _testExecution = TestExecution.getInstance();

  /**
   * @author Khaw Chek Hau
   * @XCR3063: Notification Message When Using DRO 
   */
  public ConfigEnablePromptWarningMsgIfModifyDROLevelCommand(boolean newStatus)
  {
    _newStatus = newStatus;
    _originalStatus = _testExecution.isPromptWarningMsgIfModifyDROLevel();
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3063: Notification Message When Using DRO 
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalStatus == _newStatus)
    {
      return false;
    }

    _testExecution.setPromptWarningMsgIfModifyDROLevel(_newStatus);
    return true;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3063: Notification Message When Using DRO 
   */
  public void undoCommand() throws XrayTesterException
  {
    _testExecution.setPromptWarningMsgIfModifyDROLevel(_originalStatus);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3063: Notification Message When Using DRO 
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_PROMPT_WARNING_MESSAGE_IF_CHANGING_DRO_LEVEL_KEY");
  }
}
