package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command to create a new fiducial instance on the panel
 * @author Laura Cormos
 */
public class CreateFiducialCommand extends DataCommand
{
  private Panel _panel = null;
  private String _name = null;
  private boolean _topSide;
  private PanelCoordinate _panelCoordinate;
  private Fiducial _fiducial = null;

  /**
   * @author Laura Cormos
   */
  public CreateFiducialCommand(Panel panel, String name, boolean topSide, PanelCoordinate newCoordInNanoMeters)
  {
    Assert.expect(panel != null);
    Assert.expect(name != null);
    Assert.expect(newCoordInNanoMeters != null);

    _panel = panel;
    _name = name;
    _topSide = topSide;
    _panelCoordinate = newCoordInNanoMeters;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_panel != null);
    Assert.expect(_name != null);
    Assert.expect(_panelCoordinate != null);

    if (_fiducial == null)
    {
      _fiducial = _panel.createFiducial(_name, _topSide);
      _fiducial.getFiducialType().setCoordinateInNanoMeters(_panelCoordinate);
    }
    else
      _fiducial.add();

    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_fiducial != null);

    _fiducial.remove();
    return;
  }

  /**
   * @author Laura Cormos
   */
  public Fiducial getFiducial()
  {
    Assert.expect(_fiducial != null);
    return _fiducial;
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_CREATE_FIDUCIAL_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
