package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.datastore.config.*;

/**
 *
 * @author chin-seong.kee
 */
public class ConfigSetMachineAutoStartupSchedulerTimeCommand  extends DataCommand
{
  private int _originalMachineStartupHour, _originalMachineStartupMin;
  private int _newMachineStartupHour, _newMachineStartupMin;
  private Config _config = Config.getInstance();

  public ConfigSetMachineAutoStartupSchedulerTimeCommand(int newMachineStartupHour, int newStartupMinutes)
  {
    _newMachineStartupHour = newMachineStartupHour;
    _newMachineStartupMin = newStartupMinutes;
    
    _originalMachineStartupHour = _config.getIntValue(SoftwareConfigEnum.V810_AUTO_START_UP_HOUR);
    _originalMachineStartupMin = _config.getIntValue(SoftwareConfigEnum.V810_AUTO_START_UP_MINUTES);
  }

  public boolean executeCommand() throws XrayTesterException
  {
    _config.setValue(SoftwareConfigEnum.V810_AUTO_START_UP_HOUR, _newMachineStartupHour);
    _config.setValue(SoftwareConfigEnum.V810_AUTO_START_UP_MINUTES, _newMachineStartupMin);
    
    return true;
  }

  public void undoCommand() throws XrayTesterException
  {
    _config.setValue(SoftwareConfigEnum.V810_AUTO_START_UP_HOUR, _originalMachineStartupHour);
    _config.setValue(SoftwareConfigEnum.V810_AUTO_START_UP_MINUTES, _originalMachineStartupMin);
  }
  
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_MACHINE_AUTO_STARTUP_TIME_KEY");
  }
  
}
