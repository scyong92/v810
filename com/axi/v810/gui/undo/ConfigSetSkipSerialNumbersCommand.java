package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * Command for setting the SkipSerialNumbers checkbox
 * @author Laura Cormos
 */
public class ConfigSetSkipSerialNumbersCommand extends DataCommand
{
  private boolean _newEnabledState;
  private boolean _oldEnabledState;
  private SerialNumberManager _serialNumberManager = SerialNumberManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetSkipSerialNumbersCommand(boolean enabledState)
  {
    _newEnabledState = enabledState;
    _oldEnabledState = _serialNumberManager.isSkipSerialNumbersEnabled();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_oldEnabledState == _newEnabledState)
      return false;

    _serialNumberManager.setSkipSerialNumbers(_newEnabledState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _serialNumberManager.setSkipSerialNumbers(_oldEnabledState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SN_SET_SKIP_SN_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
