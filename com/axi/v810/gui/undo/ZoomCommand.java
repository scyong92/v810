package com.axi.v810.gui.undo;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * @author George A. David
 */
public class ZoomCommand extends GuiCommand
{
  private GraphicsEngine _graphicsEngine = null;
  private double _unZoomFactor = -1;
  private double _zoomFactor = -1;

  /**
   * @author George A. David
   */
  public ZoomCommand(GraphicsEngine graphicsEngine, double zoomFactor)
  {
    Assert.expect(graphicsEngine != null);
    Assert.expect(zoomFactor >= 0);

    _graphicsEngine = graphicsEngine;
    _zoomFactor = zoomFactor;
    _unZoomFactor = 1.0 / zoomFactor;
  }

  /**
   * @author George A. David
   */
  protected Object getGuiEventSource()
  {
    Assert.expect(_graphicsEngine != null);

    return _graphicsEngine;
  }

  /**
   * @author George A. David
   */
  protected GuiEventEnum getGuiEventEnum()
  {
    return GuiUpdateEventEnum.CAD_GRAPHICS;
  }

  /**
   * @author George A. David
   */
  public boolean executeCommand() throws com.axi.v810.util.XrayTesterException
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.zoom(_zoomFactor);

    return true;
  }

  /**
   * @author George A. David
   */
  public void undoCommand() throws com.axi.v810.util.XrayTesterException
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.zoom(_unZoomFactor);
  }

  /**
   * @author George A. David
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_ZOOM_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
