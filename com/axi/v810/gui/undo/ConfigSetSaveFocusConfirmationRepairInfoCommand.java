package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
public class ConfigSetSaveFocusConfirmationRepairInfoCommand extends DataCommand
{
  private boolean _originalSetLimitState;
  private boolean _newSetLimitState;
  private RepairImagesConfigManager _config = RepairImagesConfigManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetSaveFocusConfirmationRepairInfoCommand(boolean newSetLimitState)
  {
    _newSetLimitState = newSetLimitState;
    _originalSetLimitState = _config.isSaveFocusConfirmationRepairInfoEnabled();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalSetLimitState == _newSetLimitState)
      return false;

    _config.setSaveFocusConfirmationRepairInfoEnabled(_newSetLimitState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _config.setSaveFocusConfirmationRepairInfoEnabled(_originalSetLimitState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_SAVE_FOCUS_CONF_REPAIR_INFO_KEY");
  }
}
