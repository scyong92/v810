package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * Command for setting the serial number pattern type for pattern matching
 * @author Laura Cormos
 */
public class ConfigSetSerialNumberPatternCommand extends DataCommand
{
  private SerialNumberRegularExpressionData _serialNumberRegExData = null;
  private String _newSerialNumberPatternToMatch = null;
  private String _oldSerialNumberPatternToMatch = null;
  private boolean _isRegEx;

  /**
   * @author Laura Cormos
   */
  public ConfigSetSerialNumberPatternCommand(SerialNumberRegularExpressionData serialNumberRegExData,
                                                 String pattern,
                                                 boolean isRegEx)
  {
    Assert.expect(serialNumberRegExData != null);
    Assert.expect(pattern != null);

    _serialNumberRegExData = serialNumberRegExData;
    _newSerialNumberPatternToMatch = pattern;
    _oldSerialNumberPatternToMatch = _serialNumberRegExData.getOriginalExpression();
    _isRegEx = isRegEx;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_serialNumberRegExData != null);

    _serialNumberRegExData.setOriginalExpressionAndInterpretion(_newSerialNumberPatternToMatch, _isRegEx);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_serialNumberRegExData != null);

    _serialNumberRegExData.setOriginalExpressionAndInterpretion(_oldSerialNumberPatternToMatch, _isRegEx);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SN_SET_PATTERN_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
