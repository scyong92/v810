package com.axi.v810.gui.undo;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.gui.*;

/**
 * @author Bill Darbie
 */
public class Test_CommandManager extends UnitTest
{
  // any instance will do
  private CommandManager _commandManager = CommandManager.getDevelopTestInstance();

  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_CommandManager());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      Expect.expect(_commandManager.isUndoAvailable() == false);
      try
      {
        _commandManager.undo();
        Assert.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }

      Expect.expect(_commandManager.isRedoAvailable() == false);
      try
      {
        _commandManager.redo();
        Assert.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }

      // test 1
      _commandManager.setUndoAndRedoCommandsGoOnCommandList(true);
      _commandManager.clear();
      System.out.println("test 1");
      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyProjectCommand(1, true));
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyProjectCommand(2, true));
      System.out.println("CMD: execute 3:");
      _commandManager.execute(new MyProjectCommand(3, true));
      System.out.println("CMD: execute 4 with return of false:");
      _commandManager.execute(new MyProjectCommand(4, false));

      StringBuffer buffer = new StringBuffer();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer) == false);

      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();
      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();

      // test 2
      _commandManager.clear();
      System.out.println();
      System.out.println("test 2");
      Expect.expect(_commandManager.isUndoAvailable() == false);
      Expect.expect(_commandManager.isRedoAvailable() == false);

      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyProjectCommand(1, true));
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyProjectCommand(2, true));
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      System.out.println("CMD: execute 3:");
      _commandManager.execute(new MyProjectCommand(3, true));
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable() == false);

      // test 3
      _commandManager.clear();
      System.out.println();
      System.out.println("test 3");
      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyProjectCommand(1, true));
      int numCommands = 0;
      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyProjectCommand(2, true));
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      numCommands = _commandManager.redo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      numCommands = _commandManager.redo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      numCommands = _commandManager.redo();
      Expect.expect(numCommands == 1);

      // test 4
      // now try block commands
      _commandManager.clear();
      System.out.println();
      System.out.println("test 4");
      _commandManager.beginCommandBlock("command block 1");
      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyProjectCommand(1, true));
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyProjectCommand(2, true));
      _commandManager.endCommandBlock();

      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 2);

      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      numCommands = _commandManager.redo();
      Expect.expect(numCommands == 2);


      // now try the other mode
      // test 5
      try
      {
        _commandManager.setUndoAndRedoCommandsGoOnCommandList(false);
        Expect.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing
      }
      _commandManager.clear();
      _commandManager.setUndoAndRedoCommandsGoOnCommandList(false);
      System.out.println("test 5");
      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyProjectCommand(1, true));
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyProjectCommand(2, true));
      System.out.println("CMD: execute 3:");
      _commandManager.execute(new MyProjectCommand(3, true));
      System.out.println("CMD: execute 4 with return of false:");
      _commandManager.execute(new MyProjectCommand(4, false));

      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer) == false);

      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();
      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();

      // test 6
      _commandManager.clear();
      System.out.println();
      System.out.println("test 6");
      Expect.expect(_commandManager.isUndoAvailable(buffer) == false);
      Expect.expect(_commandManager.isRedoAvailable(buffer) == false);

      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyProjectCommand(1, true));
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyProjectCommand(2, true));
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable() == false);
      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();
      Expect.expect(_commandManager.isUndoAvailable());
      Expect.expect(_commandManager.isRedoAvailable());
      System.out.println("CMD: execute 3:");
      _commandManager.execute(new MyProjectCommand(3, true));
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable() == false);
      Expect.expect(_commandManager.isRedoAvailable());

      // test 7
      _commandManager.clear();
      System.out.println();
      System.out.println("test 7");
      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyProjectCommand(1, true));
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable() == false);
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable() == false);
      Expect.expect(_commandManager.isRedoAvailable());
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyProjectCommand(2, true));
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable() == false);
      System.out.println("CMD: undo: " + buffer);
      _commandManager.undo();
      Expect.expect(_commandManager.isUndoAvailable() == false);
      Expect.expect(_commandManager.isRedoAvailable(buffer));
      System.out.println("CMD: redo: " + buffer);
      _commandManager.redo();
      Expect.expect(_commandManager.isUndoAvailable());
      Expect.expect(_commandManager.isRedoAvailable() == false);

      // test 8
      // now try block commands
      _commandManager.clear();
      System.out.println();
      System.out.println("test 8");
      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyScreenChangeGuiCommand(1, true));
      _commandManager.beginCommandBlock("command block 1");
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyProjectCommand(2, true));
      System.out.println("CMD: execute 3:");
      _commandManager.execute(new MyProjectCommand(3, true));
      _commandManager.endCommandBlock();
      System.out.println("CMD: execute 4:");
      _commandManager.execute(new MyScreenChangeGuiCommand(4, true));

      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable() == false);

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 3);
      Expect.expect(_commandManager.isUndoAvailable() == false);
      Expect.expect(_commandManager.isRedoAvailable());

      System.out.println("CMD: execute 5:");
      _commandManager.execute(new MyScreenChangeGuiCommand(5, true));

      System.out.println("CMD: redo: " + buffer);
      numCommands = _commandManager.redo();
      Expect.expect(numCommands == 3);
      Expect.expect(_commandManager.isUndoAvailable());
      Expect.expect(_commandManager.isRedoAvailable() == false);

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 3);
      Expect.expect(_commandManager.isUndoAvailable() == false);
      Expect.expect(_commandManager.isRedoAvailable());

      // now test execute() with the ScreenChangeGuiCommand
      _commandManager.executeOneScreenChangeGuiCommandsPerUndoOrRedo(true);
      _commandManager.clear();
      System.out.println();
      System.out.println("test 9");
      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyScreenChangeGuiCommand(1, true));
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyProjectCommand(2, true));
      System.out.println("CMD: execute 3:");
      _commandManager.execute(new MyProjectCommand(3, true));
      System.out.println("CMD: execute 4:");
      _commandManager.execute(new MyScreenChangeGuiCommand(4, true));
      System.out.println("CMD: execute 5:");
      _commandManager.execute(new MyProjectCommand(5, true));
      System.out.println("CMD: execute 6:");
      _commandManager.execute(new MyScreenChangeGuiCommand(6, true));
      System.out.println("CMD: execute 7:");
      _commandManager.execute(new MyScreenChangeGuiCommand(7, true));
      System.out.println("CMD: execute 8:");
      _commandManager.execute(new MyScreenChangeGuiCommand(8, true));
      System.out.println("CMD: execute 9:");
      _commandManager.execute(new MyScreenChangeGuiCommand(9, true));
      System.out.println("CMD: execute 10:");
      _commandManager.execute(new MyProjectCommand(10, true));

      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable() == false);

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 2);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable());

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 2);
      Expect.expect(_commandManager.isUndoAvailable());
      Expect.expect(_commandManager.isRedoAvailable(buffer));

      System.out.println("CMD: execute 11:");
      _commandManager.execute(new MyScreenChangeGuiCommand(11, true));

      System.out.println("CMD: redo: " + buffer);
      numCommands = _commandManager.redo();

      Expect.expect(numCommands == 2);
      Expect.expect(_commandManager.isUndoAvailable());
      Expect.expect(_commandManager.isRedoAvailable(buffer));

      System.out.println("CMD: redo: " + buffer);
      numCommands = _commandManager.redo();
      Expect.expect(numCommands == 2);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable() == false);

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 2);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable());

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 2);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable());

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 2);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable());

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 2);
      Expect.expect(_commandManager.isUndoAvailable() == false);
      Expect.expect(_commandManager.isRedoAvailable());

      // now test execute() with the ScreenChangeGuiCommand
      _commandManager.executeOneScreenChangeGuiCommandsPerUndoOrRedo(false);
      _commandManager.clear();
      System.out.println();
      System.out.println("test 10");
      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyScreenChangeGuiCommand(1, true));
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyProjectCommand(2, true));
      System.out.println("CMD: execute 3:");
      _commandManager.execute(new MyProjectCommand(3, true));
      System.out.println("CMD: execute 4:");
      _commandManager.execute(new MyScreenChangeGuiCommand(4, true));
      System.out.println("CMD: execute 5:");
      _commandManager.execute(new MyProjectCommand(5, true));
      System.out.println("CMD: execute 6:");
      _commandManager.execute(new MyScreenChangeGuiCommand(6, true));
      System.out.println("CMD: execute 7:");
      _commandManager.execute(new MyScreenChangeGuiCommand(7, true));
      System.out.println("CMD: execute 8:");
      _commandManager.execute(new MyScreenChangeGuiCommand(8, true));
      System.out.println("CMD: execute 9:");
      _commandManager.execute(new MyScreenChangeGuiCommand(9, true));
      System.out.println("CMD: execute 10:");
      _commandManager.execute(new MyProjectCommand(10, true));

      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable() == false);

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable());

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable());
      Expect.expect(_commandManager.isRedoAvailable(buffer));

      System.out.println("CMD: redo: " + buffer);
      numCommands = _commandManager.redo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable());
      Expect.expect(_commandManager.isRedoAvailable(buffer));

      System.out.println("CMD: redo: " + buffer);
      numCommands = _commandManager.redo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable() == false);

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable());

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable());

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable());

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable());

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable());

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable());

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable());

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable());

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable());

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 1);
      Expect.expect(_commandManager.isUndoAvailable() == false);
      Expect.expect(_commandManager.isRedoAvailable());

      // test 11
      _commandManager.executeOneScreenChangeGuiCommandsPerUndoOrRedo(true);
      _commandManager.clear();
      System.out.println();
      System.out.println("test 11");
      System.out.println("CMD: execute 1:");
      _commandManager.execute(new MyScreenChangeGuiCommand(1, true));
      System.out.println("CMD: execute 2:");
      _commandManager.execute(new MyProjectCommand(2, true));
      System.out.println("CMD: execute 3:");
      _commandManager.execute(new MyScreenChangeGuiCommand(3, true));

      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable() == false);

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo();
      Expect.expect(numCommands == 2);

      System.out.println("CMD: execute 5:");
      _commandManager.execute(new MyProjectCommand(5, true));

      Expect.expect(_commandManager.isUndoAvailable(buffer));
      Expect.expect(_commandManager.isRedoAvailable() == false);

      System.out.println("CMD: undo: " + buffer);
      numCommands = _commandManager.undo(); // should do GUI command 1 NOT 3!!
      Expect.expect(numCommands == 2);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}

/**
 * @author Bill Darbie
 */
class MyProjectCommand extends DataCommand
{
  private int _commandNumber;
  private boolean _stateChange;

  /**
   * @author Bill Darbie
   */
  MyProjectCommand(int commandNumber, boolean stateChange)
  {
    _commandNumber = commandNumber;
    _stateChange = stateChange;
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    return Integer.toString(_commandNumber);
  }

  /**
   * @author Bill Darbie
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_stateChange)
      System.out.println("  executing project command " + _commandNumber);

    return _stateChange;
  }

  /**
   * @author Bill Darbie
   */
  public void undoCommand() throws XrayTesterException
  {
    System.out.println("  undo project command " + _commandNumber);
  }

  /**
   * @author Bill Darbie
   */
  public void commandFinished()
  {
    // do nothing
  }
}

/**
 * @author Bill Darbie
 */
class MyScreenChangeGuiCommand extends ScreenChangeGuiCommand
{
  private int _commandNumber;
  private boolean _stateChange;

  /**
   * @author Bill Darbie
   */
  MyScreenChangeGuiCommand(int commandNumber, boolean stateChange)
  {
    _commandNumber = commandNumber;
    _stateChange = stateChange;
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    return Integer.toString(_commandNumber);
  }

  /**
   * @author Bill Darbie
   */
  public boolean executeCommand() throws XrayTesterException
  {
    System.out.println("  executing GUI command " + _commandNumber);
    return _stateChange;
  }

  /**
   * @author Bill Darbie
   */
  public void undoCommand() throws XrayTesterException
  {
    System.out.println("  undo GUI command " + _commandNumber);
  }

  /**
   * @author Bill Darbie
   */
  public void commandFinished()
  {
    // do nothing
  }

  /**
   * @author George A. David
   */
  protected Object getGuiEventSource()
  {
    return null;
  }

  /**
   * @author George A. David
   */
  protected GuiEventEnum getGuiEventEnum()
  {
    return null;
  }
}
