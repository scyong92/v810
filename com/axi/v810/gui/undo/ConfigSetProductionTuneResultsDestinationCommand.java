package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * Command for setting the production tune results destination property
 * @author Andy Mechtenberg
 */
public class ConfigSetProductionTuneResultsDestinationCommand extends DataCommand
{
  private ProductionTuneData _productionTuneData = null;
  private String _newResultsDest = null;
  private String _oldResultsDest = null;

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetProductionTuneResultsDestinationCommand(ProductionTuneData productionTuneData,
                                                          String resultsDest)
  {
    Assert.expect(productionTuneData != null);
    Assert.expect(resultsDest != null);

    _productionTuneData = productionTuneData;
    _newResultsDest = resultsDest;
    _oldResultsDest = _productionTuneData.getResultsDestination();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);

    _productionTuneData.setResultsDestination(_newResultsDest);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);

    _productionTuneData.setResultsDestination(_oldResultsDest);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_PRODUCTION_TUNE_SET_RESULTS_DEST_KEY");
  }
}
