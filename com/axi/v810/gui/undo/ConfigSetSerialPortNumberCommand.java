package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class sets/unsets the serial port number for the barcode reader configuration
 * @author Laura Cormos
 */
public class ConfigSetSerialPortNumberCommand extends DataCommand
{
  private String _originalPortNumber;
  private String _newPortNumber;
  private BarcodeReaderManager _bcrManager = BarcodeReaderManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetSerialPortNumberCommand(String portNumber)
  {
    Assert.expect(portNumber != null);

    _newPortNumber = portNumber;
    _originalPortNumber = _bcrManager.getBarcodeReaderSerialPortName();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_newPortNumber != null);

    if (_newPortNumber.equals(_originalPortNumber))
      return false;

    _bcrManager.setBarcodeReaderSerialPortName(_newPortNumber);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_originalPortNumber != null);

    _bcrManager.setBarcodeReaderSerialPortName(_originalPortNumber);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_SERIAL_PORT_NUMBER_KEY");
  }
}
