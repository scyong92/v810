package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class sets/unsets the stop bits for the barcode reader configuration
 * @author Laura Cormos
 */
public class ConfigSetStopBitsCommand extends DataCommand
{
  private String _originalStopBits;
  private String _newStopBits;
  private BarcodeReaderManager _bcrManager = BarcodeReaderManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetStopBitsCommand(String portNumber)
  {
    Assert.expect(portNumber != null);

    _newStopBits = portNumber;
    _originalStopBits = _bcrManager.getBarcodeReaderRs232StopBits();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_newStopBits != null);

    if (_newStopBits.equals(_originalStopBits))
      return false;

    _bcrManager.setBarcodeReaderRs232StopBits(_newStopBits);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_originalStopBits != null);

    _bcrManager.setBarcodeReaderRs232StopBits(_originalStopBits);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_STOP_BITS_KEY");
  }
}
