package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
public class ConfigSetLimitNumRepairImagesToSaveCommand extends DataCommand
{
  private boolean _originalSetLimitState;
  private boolean _newSetLimitState;
  private RepairImagesConfigManager _config = RepairImagesConfigManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetLimitNumRepairImagesToSaveCommand(boolean newSetLimitState)
  {
    _newSetLimitState = newSetLimitState;
    _originalSetLimitState = _config.isLimitRepairImagesEnabled();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalSetLimitState == _newSetLimitState)
      return false;

    _config.setLimitRepairImagesEnabled(_newSetLimitState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _config.setLimitRepairImagesEnabled(_originalSetLimitState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_LIMIT_REPAIR_IMAGES_TO_SAVE_KEY");
  }
}
