/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 *
 * @author kok-chun.tan
 */
public class ConfigSetAlwaysDisplayVariationNameCommand extends DataCommand
{
  private boolean _originalAlwaysDisplayVariationNameMode;
  private boolean _newAlwaysDisplayVariationNameMode;
  private TestExecution _testExecution = TestExecution.getInstance();

  /**
   * @author Kok Chun, Tan
   */
  public ConfigSetAlwaysDisplayVariationNameCommand (boolean displayVariationName)
  {
    _newAlwaysDisplayVariationNameMode = displayVariationName;
    _originalAlwaysDisplayVariationNameMode = _testExecution.isAlwaysDisplayVariationNameMode();
  }

  /**
   * @author Kok Chun, Tan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalAlwaysDisplayVariationNameMode == _newAlwaysDisplayVariationNameMode)
      return false;

    Config.getInstance().setValue(SoftwareConfigEnum.TEST_EXEC_ALWAYS_DISPLAY_VARIATION_NAME, _newAlwaysDisplayVariationNameMode);
    return true;
  }

  /**
   * @author Kok Chun, Tan
   */
  public void undoCommand() throws XrayTesterException
  {
    Config.getInstance().setValue(SoftwareConfigEnum.TEST_EXEC_ALWAYS_DISPLAY_VARIATION_NAME, _originalAlwaysDisplayVariationNameMode);
  }

  /**
   * @author Kok Chun, Tan
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_DISPLAY_VARIATION_NAME_MODE_KEY");
  }
}
