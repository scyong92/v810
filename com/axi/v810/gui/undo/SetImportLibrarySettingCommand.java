package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class SetImportLibrarySettingCommand extends DataCommand
{
  private ImportLibrarySettingPersistance _importLibrarySettingPersistance;
  private ImportLibrarySettingEnum _importLibrarySettingEnum;
  private boolean _newBooleanValue = false;
  private boolean _oldBooleanValue = false;
  
  /**
   * @author Cheah Lee Herng 
   */
  public SetImportLibrarySettingCommand(ImportLibrarySettingPersistance importLibrarySettingPersistance, 
                                        ImportLibrarySettingEnum importLibrarySettingEnum, 
                                        boolean enable)
  {
    Assert.expect(importLibrarySettingPersistance != null);
    Assert.expect(importLibrarySettingEnum != null);

    _importLibrarySettingPersistance = importLibrarySettingPersistance;
    _importLibrarySettingEnum = importLibrarySettingEnum;
    
    _newBooleanValue = enable;
    _oldBooleanValue = _importLibrarySettingPersistance.getImportLibrarySettingStatus(importLibrarySettingEnum);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_importLibrarySettingPersistance != null);
    
    if (_newBooleanValue == _oldBooleanValue)
      return false;
    
    _importLibrarySettingPersistance.setImportLibrarySettingStatus(_importLibrarySettingEnum, _newBooleanValue);
    return true;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_importLibrarySettingPersistance != null);
    
    _importLibrarySettingPersistance.setImportLibrarySettingStatus(_importLibrarySettingEnum, _oldBooleanValue);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("PLWK_CHANGE_IMPORT_LIBRARY_SETTING_OPTION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
