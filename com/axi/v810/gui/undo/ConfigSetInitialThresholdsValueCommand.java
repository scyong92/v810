package com.axi.v810.gui.undo;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * Command for setting an initial threshold set threshold value
 * @author Andy Mechtenberg
 */
public class ConfigSetInitialThresholdsValueCommand extends DataCommand
{
  private JointTypeEnum _jointTypeEnum;
  private Algorithm _algorithm;
  private AlgorithmSetting _algorithmSetting;
  private Serializable _origValue;
  private Serializable _newValue;
  private InitialThresholds _initialThresholds = InitialThresholds.getInstance();
  
  /**   
   * @author Andy Mechtenberg
   */
  public ConfigSetInitialThresholdsValueCommand(JointTypeEnum jointTypeEnum, Algorithm algorithm, AlgorithmSetting algorithmSetting, Serializable newValue)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithm != null);
    Assert.expect(algorithmSetting != null);
    Assert.expect(newValue != null);

    _jointTypeEnum = jointTypeEnum;
    _algorithm = algorithm;
    _algorithmSetting = algorithmSetting;
    _newValue = newValue;
    _origValue = _initialThresholds.getAlgorithmSettingValue(jointTypeEnum, algorithm, algorithmSetting);
  }

  /**
   * Add the features to the group
   * @return true if the command was executed
   * @throws XrayTesterException
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_newValue.equals(_origValue))
      return false;
    _initialThresholds.setAlgorithmSettingValue(_jointTypeEnum, _algorithm, _algorithmSetting, _newValue);
    return true;
  }

  /**
   * Remove the feature (opposite of add)
   * @throws XrayTesterException
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _initialThresholds.setAlgorithmSettingValue(_jointTypeEnum, _algorithm, _algorithmSetting, _origValue);
  }

  /**
   * @return a localized description of the command
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SUBTYPE_SET_VALUE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}