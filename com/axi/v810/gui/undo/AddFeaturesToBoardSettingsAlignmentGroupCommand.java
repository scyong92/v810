package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting a component type coordinate
 * @author Wei Chin
 */
public class AddFeaturesToBoardSettingsAlignmentGroupCommand extends DataCommand
{
  private int _alignmentGroupIndex = 0;
  private Panel _panel = null;
  private List<Pad> _pads;
  private List<Fiducial> _fiducials;

  /**
   * @param alignmentGroup The alignment group to add the new pad to
   * @param pads the Pads to add in
   * @param fiducials the Fiducials to add in
   * @author Wei Chin
   */
  public AddFeaturesToBoardSettingsAlignmentGroupCommand(int alignmentGroupIndex, Panel panel, List<Pad> pads, List<Fiducial> fiducials)
  {
    Assert.expect(panel != null);
    Assert.expect(pads != null);
    Assert.expect(fiducials != null);

    _alignmentGroupIndex = alignmentGroupIndex;
    _panel = panel;
    _pads = pads;
    _fiducials = fiducials;
  }

  /**
   * Add the features to the group
   * @author Wei Chin
   */
  public boolean executeCommand() throws XrayTesterException
  {
    // don't do anything if both lists of pads and fiducials are empty
    if (_pads.isEmpty() && _fiducials.isEmpty())
      return false;

    for (Board board : _panel.getBoards())
    {
      board.addPadsToAlignmentGroups(_alignmentGroupIndex, _pads, _fiducials);
    }
    return true;
  }

  /**
   * Remove the feature (opposite of add)
   * @author Wei Chin
   */
  public void undoCommand() throws XrayTesterException
  {
    for (Board board : _panel.getBoards())
    {
      board.removePadsToAlignmentGroups(_alignmentGroupIndex, _pads, _fiducials);
    }
  }

  /**
   * @author Wei Chin
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_ADD_FEATURE_TO_ALIGNMENT_GROUP_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
