/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Khaw Chek Hau
 * @XCR3774: Display load & unload time during production
 */
public class ConfigSetDisplayProductionPanelLoadingAndUnloadingTimeCommand extends DataCommand
{
  private boolean _originalDisplayProductionPanelLoadingAndUnloadingTimeMode;
  private boolean _newDisplayProductionPanelLoadingAndUnloadingTimeMode;
  private TestExecution _testExecution = TestExecution.getInstance();

  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  public ConfigSetDisplayProductionPanelLoadingAndUnloadingTimeCommand (boolean displayProductionPanelLoadingAndUnloadingTime)
  {
    _newDisplayProductionPanelLoadingAndUnloadingTimeMode = displayProductionPanelLoadingAndUnloadingTime;
    _originalDisplayProductionPanelLoadingAndUnloadingTimeMode = _testExecution.isDisplayProductionPanelLoadingAndUnloadingTimeMode();
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalDisplayProductionPanelLoadingAndUnloadingTimeMode == _newDisplayProductionPanelLoadingAndUnloadingTimeMode)
      return false;

    Config.getInstance().setValue(SoftwareConfigEnum.TEST_EXEC_DISPLAY_PRODUCTION_PANEL_LOADING_AND_UNLOADING_TIME, _newDisplayProductionPanelLoadingAndUnloadingTimeMode);
    return true;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  public void undoCommand() throws XrayTesterException
  {
    Config.getInstance().setValue(SoftwareConfigEnum.TEST_EXEC_DISPLAY_PRODUCTION_PANEL_LOADING_AND_UNLOADING_TIME, _originalDisplayProductionPanelLoadingAndUnloadingTimeMode);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_DISPLAY_PRODUCTION_PANEL_LOADING_UNLOADING_TIME_MODE_KEY");
  }
}
