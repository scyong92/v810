package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.util.*;

/**
 * This class is used to set the artifact compensation state for a ComponentType
 * Using this class allows us to undo the set later.
 * @author Laura Cormos
 */
public class ComponentTypeSetArtifactCompesationCommand extends DataCommand
{

  private ComponentType _componentType = null;
  private ArtifactCompensationStateEnum _origArtificateCompensationState = null;
  private ArtifactCompensationStateEnum _newArtifacteCompensationState = null;
  private SignalCompensationEnum _origSignalCompensationEnum = null;
  //private SignalCompensationEnum _newSignalCompensationEnum = null;

  /**
   * Construct the class.
   * @author Laura Cormos
   */
  public ComponentTypeSetArtifactCompesationCommand(ComponentType componentType,
    ArtifactCompensationStateEnum newArtifactCompensationState,
    SignalCompensationEnum newSignalCompensationState)
  {
    Assert.expect(componentType != null);
    Assert.expect(newArtifactCompensationState != null);
    Assert.expect(newSignalCompensationState != null);

    _componentType = componentType;
    _newArtifacteCompensationState = newArtifactCompensationState;
    _origArtificateCompensationState = _componentType.getArtifactCompensationSetting();
    //_newSignalCompensationEnum = newSignalCompensationState;
    _origSignalCompensationEnum = _componentType.getSignalCompensation();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    if (_newArtifacteCompensationState.equals(_origArtificateCompensationState))
    {
      return false;
    }

    List<PadType> padTypes = _componentType.getPadTypes();
    for (PadType padType : padTypes)
    {
      padType.getPadTypeSettings().setArtifactCompensationState(_newArtifacteCompensationState);
    }

    /*
    // CR fix by LeeHerng - Get the default signal compensation generated from program generation
    if (_newArtifacteCompensationState.equals(ArtifactCompensationStateEnum.COMPENSATED))
    {
    List<SignalCompensationEnum> signalCompensationEnums = ProgramGeneration.getValidSignalCompensations(_componentType.getPadTypes());
    if (signalCompensationEnums.size() > 0)
    _newSignalCompensationEnum = signalCompensationEnums.get(0);
    }

    for (PadType padType : padTypes)
    padType.getPadTypeSettings().setSignalCompensation(_newSignalCompensationEnum);
    */

    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    List<PadType> padTypes = _componentType.getPadTypes();
    for (PadType padType : padTypes)
    {
      padType.getPadTypeSettings().setArtifactCompensationState(_origArtificateCompensationState);
    }

    for (PadType padType : padTypes)
    {
      padType.getPadTypeSettings().setSignalCompensation(_origSignalCompensationEnum);
    }
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_ARTIFACT_COMPENSATION_KEY");
  }
}
