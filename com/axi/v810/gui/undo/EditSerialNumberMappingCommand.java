package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;
import com.axi.util.*;

/**
 *
 * @author siew-yeng.phang
 */
public class EditSerialNumberMappingCommand extends DataCommand
{
  private List<SerialNumberMappingData> _oldValue;
  private List<SerialNumberMappingData> _newValue;
  
  private SerialNumberMappingManager _serialNumberMappingManager = SerialNumberMappingManager.getInstance();
  
  /**
   * @author Phang Siew Yeng
   */
  public EditSerialNumberMappingCommand(List<SerialNumberMappingData> newValue)
  {
    _newValue = newValue;
    _oldValue = new ArrayList(_serialNumberMappingManager.getSerialNumberMappingDataList());
  }

  /**
   * @author Phang Siew Yeng
   */
  public boolean executeCommand() throws XrayTesterException
  {
    boolean isEqual = true;
    for(SerialNumberMappingData data : _newValue)
    {
      if(data.equals(_serialNumberMappingManager.getSerialNumberMappingData(data.getBoardName())))
        continue;
      
      isEqual = false;
      break;
    }
    
    if(isEqual == true)
      return false;

    _serialNumberMappingManager.setSerialNumberMappingDataList(_newValue);
    return true;
  }

  /**
   * @author Phang Siew Yeng
   */
  public void undoCommand() throws XrayTesterException
  {
    _serialNumberMappingManager.setSerialNumberMappingDataList(_oldValue);
  }

  /**
   * @author Phang Siew Yeng
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("NOT SET", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
