/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.TestExecution;
import com.axi.v810.util.StringLocalizer;
import com.axi.v810.util.XrayTesterException;

/**
 *
 * @author hsia-fen.tan
 */
public class ConfigSetCreateHistoryLogCommand extends DataCommand
{
  private boolean _originalShowHistoryLog;
  private boolean _newShowHistoryLog;
  private TestExecution _testExecution = TestExecution.getInstance();

 
  public ConfigSetCreateHistoryLogCommand(boolean newShowHistoryLog)
  {
    _newShowHistoryLog = newShowHistoryLog;
    _originalShowHistoryLog = _testExecution.isShowHistoryLog();
  }

  
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalShowHistoryLog == _newShowHistoryLog)
      return false;

    _testExecution.setShowHistoryLog(_newShowHistoryLog);
    return true;
  }

 
  public void undoCommand() throws XrayTesterException
  {
   _testExecution.setShowHistoryLog(_originalShowHistoryLog);
  }

 
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SHOW_HISTORY_LOG_DATABASE_PROJECT_KEY");
  }
  
}
