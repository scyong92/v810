package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.config.*;
import com.axi.v810.util.*;

/**
 * This class will delete a results rule condition and the deletion can be reset via an undo command.
 * @author Laura Cormos
 */
public class ConfigDeleteResultsConditionCommand extends DataCommand
{
  private ResultsTableModelCondition _deletedCondition;
  private ResultsProcessingConditionsTableModel _table;
  private int _index;

  /**
   * @author Laura Cormos
   */
  public ConfigDeleteResultsConditionCommand(ResultsProcessingConditionsTableModel table, int index)
  {
    Assert.expect(table != null);
    Assert.expect(index >= 0);

    _table = table;
    _deletedCondition = table.getConditionAt(index);
    _index = index;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    _table.deleteConditionAt(_index);
    ConfigObservable.getInstance().stateChanged(_deletedCondition);

    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _table.addCondition(_deletedCondition);
    ConfigObservable.getInstance().stateChanged(_deletedCondition);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_RESPROC_SET_DELETE_CONDITION_KEY");
  }
}
