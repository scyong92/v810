package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Wei Chin, Chong
 */
public class PanelSetAlignmentMethodCommand extends DataCommand
{
  private PanelSettings _panelSettings;
  private boolean _originalPanelBasedAligmentMethod;
  private boolean _newPanelBasedAligmentMethod;

  /**
   * @param panel
   * @param panelBasedAligmentMethod
   * @author Wei Chin, Chong
   */
  public PanelSetAlignmentMethodCommand(Panel panel, boolean panelBasedAligmentMethod)
  {
    Assert.expect(panel != null);

    _panelSettings = panel.getPanelSettings();
    _newPanelBasedAligmentMethod = panelBasedAligmentMethod;
    _originalPanelBasedAligmentMethod = _panelSettings.isPanelBasedAlignment();
  }

  /**
   * @return
   * @throws XrayTesterException
   * @author Wei Chin, Chong
   */
  @Override
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_panelSettings != null);

    if (_originalPanelBasedAligmentMethod == _newPanelBasedAligmentMethod)
      return false;

    _panelSettings.setIsPanelBasedAlignment(_newPanelBasedAligmentMethod);

    if(_newPanelBasedAligmentMethod == true)
    {
      if(_panelSettings.isLongPanel())
      {
        for(AlignmentGroup alignmentGroup : _panelSettings.getRightAlignmentGroupsForLongPanel())
        {
          alignmentGroup.clearPads();
          _panelSettings.clearAllManualAlignmentTransform();
        }
        for(AlignmentGroup alignmentGroup : _panelSettings.getLeftAlignmentGroupsForLongPanel())
        {
          alignmentGroup.clearPads();
          _panelSettings.clearAllManualAlignmentTransform();
        }
      }
      else
      {
        for(AlignmentGroup alignmentGroup : _panelSettings.getAlignmentGroupsForShortPanel())
        {
          alignmentGroup.clearPads();
          _panelSettings.clearAllManualAlignmentTransform();
        }
      }
    }
    else
    {
      for(Board board : _panelSettings.getPanel().getBoards())
      {
        if(board.isLongBoard())
        {
          for(AlignmentGroup alignmentGroup : board.getBoardSettings().getRightAlignmentGroupsForLongPanel())
          {
            alignmentGroup.clearPads();
            board.getBoardSettings().clearAllManualAlignmentTransform();
          }
          for(AlignmentGroup alignmentGroup : board.getBoardSettings().getLeftAlignmentGroupsForLongPanel())
          {
            alignmentGroup.clearPads();
            board.getBoardSettings().clearAllManualAlignmentTransform();
          }
        }
        else
        {
          for(AlignmentGroup alignmentGroup : board.getBoardSettings().getAlignmentGroupsForShortPanel())
          {
            alignmentGroup.clearPads();
            board.getBoardSettings().clearAllManualAlignmentTransform();
          }
        }
      }
    }
    return true;
  }

  /**
   * @throws XrayTesterException
   * @author Wei Chin, Chong
   */
  @Override
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_panelSettings != null);

    _panelSettings.setIsPanelBasedAlignment(_originalPanelBasedAligmentMethod);
  }

  /**
   * @return
   * @authod Wei Chin, Chong
   */
  @Override
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PANEL_SET_ALIGNMENT_METHOD_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}