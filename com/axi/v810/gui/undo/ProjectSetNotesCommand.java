package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the notes of the project is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ProjectSetNotesCommand extends DataCommand
{
  private Project _project;
  private String _originalNotes;
  private String _newNotes;

  /**
   * @param project the project whose notes will change
   * @param newNotes the new notes of the panel
   * @author Andy Mechtenberg
   */
  public ProjectSetNotesCommand(Project project, String newNotes)
  {
    Assert.expect(project != null);
    Assert.expect(newNotes != null);

    _project = project;
    _newNotes = newNotes;
    _originalNotes = _project.getNotes();
  }

  /**
   * Set the new notes of the project
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_originalNotes.equalsIgnoreCase(_newNotes))
      return false;

    _project.setNotes(_newNotes);
    return true;
  }

  /**
   * Reset the notes of the project
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setNotes(_originalNotes);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_NOTES_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
