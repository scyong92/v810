package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * Command for setting production tune default destination
 * @author Andy Mechtenberg
 */
public class ConfigSetDefaultProductionTuneDestinationCommand extends DataCommand
{
  private ProductionTuneManager _productionTuneManager = ProductionTuneManager.getInstance();
  private String _newDefaultDestination = null;
  private String _oldDefaultDestination = null;

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetDefaultProductionTuneDestinationCommand(String newDefaultDestination, String oldDefaultDestination)
  {
    Assert.expect(newDefaultDestination != null);

    _newDefaultDestination = newDefaultDestination;
    _oldDefaultDestination = oldDefaultDestination;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_newDefaultDestination != null);

    if (_newDefaultDestination.equalsIgnoreCase(_oldDefaultDestination))
      return false;

    _productionTuneManager.setDefaultLocation(_newDefaultDestination);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_newDefaultDestination != null);
    _productionTuneManager.setDefaultLocation(_oldDefaultDestination);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_PRODUCTION_TUNE_SET_DEFAULT_DESTINATION_KEY");
  }
}
