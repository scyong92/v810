package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command to create a new land pattern pad on the given land pattern
 * @author Laura Cormos
 */
public class CreateSurfaceMountLandPatternPadCommand extends DataCommand
{
  private Panel _panel;
  private String _landPatternName;
  private java.util.List <ComponentCoordinate> _coordinate = new java.util.ArrayList();
  private double _rotation;
  private int _lengthInNanoMeters;
  private int _widthInNanoMeters;
  private ShapeEnum _shapeEnum = null;
  private LandPatternPad _landPatternPad;
  private java.util.List <LandPatternPad> _newLandPatternPads = new java.util.ArrayList();
  

  /**
   * @author Laura Cormos
   */
  public CreateSurfaceMountLandPatternPadCommand(LandPattern landPattern,
                                                 java.util.List <ComponentCoordinate> coordinate,
                                                 double rotation,
                                                 int lengthInNanoMeters,
                                                 int widthInNanoMeters,
                                                 ShapeEnum shapeEnum)
  {
    Assert.expect(landPattern != null);
    Assert.expect(shapeEnum != null);

    _panel = landPattern.getPanel();
    _landPatternName = landPattern.getName();
    _coordinate = coordinate;
    _rotation = rotation;
    _lengthInNanoMeters = lengthInNanoMeters;
    _widthInNanoMeters = widthInNanoMeters;
    _shapeEnum = shapeEnum;
  }
  
  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_shapeEnum != null);
    Assert.expect(_lengthInNanoMeters > 0);
    Assert.expect(_widthInNanoMeters > 0);
    Assert.expect(_panel != null);
    Assert.expect(_landPatternName != null);

    ProjectObservable.getInstance().setEnabled(false);
    try
    {
      if (_landPatternPad == null)
      {
        for (int i = 0; i < _coordinate.size(); i++)
        {

          LandPattern landPattern = _panel.getLandPattern(_landPatternName);
          Assert.expect(landPattern != null);
          _landPatternPad = landPattern.createSurfaceMountLandPatternPad(_coordinate.get(i),
            _rotation,
            _lengthInNanoMeters,
            _widthInNanoMeters,
            _shapeEnum);

          _newLandPatternPads.add(_landPatternPad);
        }
      }
      else
      {
        for (LandPatternPad landPatternPad : _newLandPatternPads)
        {
          landPatternPad.add();
        }
      }
    }
    finally
    {
      ProjectObservable.getInstance().setEnabled(true);
      ProjectObservable.getInstance().stateChanged(_newLandPatternPads, null, _newLandPatternPads, LandPatternPadEventEnum.ADD_OR_REMOVE_LIST);
    }
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_newLandPatternPads != null);

    ProjectObservable.getInstance().setEnabled(false);
    try
    {
      for (LandPatternPad landPatternPad : _newLandPatternPads)
        landPatternPad.remove();
    }
    finally
    {
      ProjectObservable.getInstance().setEnabled(true);
      ProjectObservable.getInstance().stateChanged(_newLandPatternPads, _newLandPatternPads, null, LandPatternPadEventEnum.ADD_OR_REMOVE_LIST);
    }
    return;
  }


  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_CREATE_LANDPATTERN_PAD_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
