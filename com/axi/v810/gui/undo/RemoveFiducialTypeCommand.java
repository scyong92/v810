package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command to destroy a fiducial type
 * @author Laura Cormos
 */
public class RemoveFiducialTypeCommand extends DataCommand
{
  private FiducialType _fiducialToRemove;

  /**
   * @author Laura Cormos
   */
  public RemoveFiducialTypeCommand(FiducialType destroyedFiducialType)
  {
    Assert.expect(destroyedFiducialType != null);

    _fiducialToRemove = destroyedFiducialType;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_fiducialToRemove != null);

    _fiducialToRemove.remove();
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_fiducialToRemove != null);

    _fiducialToRemove.add();
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_DESTROY_FIDUCIAL_TYPE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
