package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting a land pattern pad length
 * @author Laura Cormos
 */
public class LandPatternPadSetLengthInNanoMetersCommand extends DataCommand
{
  // in this command we don't want to store the actual landpatternpad passed in the constructor because this object
  // can be destroyed and recreated during an undo/redo operation. In that case this command's undo method would
  // operate on nonexistant object. This is why we store the object name and find it every time we need to use it
  private String _landPatternName = null;
  private String _landPatternPadName = null;
  private Panel _panel = null;
  private int _newLengthInNanoMeters = 0;
  private int _oldLengthInNanoMeters = 0;

  /**
   * @param landPatternPad the land pattern pad whose length will change
   * @param newLength the new length
   * @author Laura Cormos
   */
  public LandPatternPadSetLengthInNanoMetersCommand(LandPatternPad landPatternPad , int newLength)
  {
    Assert.expect(landPatternPad != null);
    Assert.expect(newLength != 0);

    _landPatternName = landPatternPad.getLandPattern().getName();
    _panel = landPatternPad.getLandPattern().getPanel();
    _landPatternPadName = landPatternPad.getName();
    _newLengthInNanoMeters = newLength;
    _oldLengthInNanoMeters = landPatternPad.getLengthInNanoMeters();
  }

  /**
   * Change the length to the new length
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_landPatternName != null);
    Assert.expect(_landPatternPadName != null);
    Assert.expect(_panel != null);

    if (_oldLengthInNanoMeters == _newLengthInNanoMeters)
      return false;

    // first find the land pattern object that this land pattern pad belongs to since this may be a redo of the original
    // command and the land pattern could have been deleted and added back.
    LandPattern landPattern = _panel.getLandPattern(_landPatternName);
    Assert.expect(landPattern != null);
    LandPatternPad landPatternPad = landPattern.getLandPatternPad(_landPatternPadName);
    Assert.expect(landPatternPad != null);
    landPatternPad.setLengthInNanoMeters(_newLengthInNanoMeters);
    return true;
  }

  /**
   * Restore the old length
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_landPatternName != null);
    Assert.expect(_landPatternPadName != null);
    Assert.expect(_panel != null);

    // first find the land pattern object that this land pattern pad belongs to since this may be a redo of the original
    // command and the land pattern could have been deleted and added back.
    LandPattern landPattern = _panel.getLandPattern(_landPatternName);
    Assert.expect(landPattern != null);
    LandPatternPad landPatternPad = landPattern.getLandPatternPad(_landPatternPadName);
    Assert.expect(landPatternPad != null);
    landPatternPad.setLengthInNanoMeters(_oldLengthInNanoMeters);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_LAND_PATTERN_PAD_SET_LENGTH_NM_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
