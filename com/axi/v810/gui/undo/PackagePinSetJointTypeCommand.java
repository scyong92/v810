package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * This class is used to set the Algorithm Family for a PackagePin
 * Using this class allows us to undo the set later.
 * @author Andy Mechtenberg
 */
public class PackagePinSetJointTypeCommand extends DataCommand
{
  private PackagePin _packagePin = null;
  private JointTypeEnum _originalJointTypeEnum = null;
  private JointTypeEnum _newJointTypeEnum = null;

  /**
   * @param packagePin the PackagePin to change
   * @param newJointTypeEnum the new Joint Type to change to
   * @author Andy Mechtenberg
   */
  public PackagePinSetJointTypeCommand(PackagePin packagePin, JointTypeEnum newJointTypeEnum)
  {
    Assert.expect(packagePin != null);
    Assert.expect(newJointTypeEnum != null);

    _packagePin = packagePin;
    _newJointTypeEnum = newJointTypeEnum;

    _originalJointTypeEnum = _packagePin.getJointTypeEnum();
  }

  /**
   * Change the algorithm family for a package pin
   * @return true if the command executed, false if new algorithm family is the same as the current one
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_packagePin != null);

    if (_newJointTypeEnum.equals(_originalJointTypeEnum))
      return false;

    _packagePin.getCompPackage().setJointTypeEnum(_newJointTypeEnum, _packagePin);
    return true;
  }

  /**
   * Reset the Algorithm Family for the package pin
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_packagePin != null);

    _packagePin.getCompPackage().setJointTypeEnum(_originalJointTypeEnum, _packagePin);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PACKAGE_PIN_SET_JOINT_TYPE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
