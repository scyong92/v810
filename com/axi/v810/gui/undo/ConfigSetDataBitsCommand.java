package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class sets/unsets the data bits for the barcode reader configuration
 * @author Laura Cormos
 */
public class ConfigSetDataBitsCommand extends DataCommand
{
  private String _originalDataBits;
  private String _newDataBits;
  private BarcodeReaderManager _bcrManager = BarcodeReaderManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetDataBitsCommand(String portNumber)
  {
    Assert.expect(portNumber != null);

    _newDataBits = portNumber;
    _originalDataBits = _bcrManager.getBarcodeReaderRs232DataBits();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_newDataBits != null);

    if (_newDataBits.equals(_originalDataBits))
      return false;

    _bcrManager.setBarcodeReaderRs232DataBits(_newDataBits);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_originalDataBits != null);

    _bcrManager.setBarcodeReaderRs232DataBits(_originalDataBits);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_DATA_BITS_KEY");
  }
}
