package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.gui.config.ProductionOptionsBarCodeReaderTabPanel;
import com.axi.v810.gui.config.ProductionOptionsFalseCallTriggeringTabPanel;

/**
 * 
 * @author Jack Hwee
 */
public class ConfigSetEnableFalseCallMonitoringCommand extends DataCommand
{
  private boolean _newEnabledState;
  private boolean _oldEnabledState;
  private ProductionOptionsFalseCallTriggeringTabPanel _parent;
  private FalseCallMonitoring _falseCallMonitoring = FalseCallMonitoring.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetEnableFalseCallMonitoringCommand(boolean enabledState, ProductionOptionsFalseCallTriggeringTabPanel parent)
  {
    Assert.expect(parent != null);

    _parent = parent;
    _newEnabledState = enabledState;
    _oldEnabledState = _falseCallMonitoring.isFalseCallMonitoringModeEnabled();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_oldEnabledState == _newEnabledState)
      return false;

    _parent.setEnabledAllControls(_newEnabledState);

    if (_newEnabledState)
      _parent.checkProtectedConfigurations();

    _falseCallMonitoring.setFalseCallMonitoringMode(_newEnabledState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    if (_oldEnabledState)
      _parent.checkProtectedConfigurations();

    _parent.setEnabledAllControls(_oldEnabledState);

    _falseCallMonitoring.setFalseCallMonitoringMode(_oldEnabledState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_BCR_SET_AUTOMATIC_BCR_ENABLED_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
