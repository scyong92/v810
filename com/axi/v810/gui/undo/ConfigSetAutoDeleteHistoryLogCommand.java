/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.undo;

import com.axi.v810.business.panelSettings.HistoryLog;
import com.axi.v810.util.StringLocalizer;
import com.axi.v810.util.XrayTesterException;

/**
 * Delete History Log Automatically
 * @author hsia-fen.tan
 */
public class ConfigSetAutoDeleteHistoryLogCommand extends DataCommand
{
  private boolean _originalAutoDeleteHistoryFile;
  private boolean _newAutoDeleteHistoryFile;
  private HistoryLog _historyLog = HistoryLog.getInstance();
  
  public ConfigSetAutoDeleteHistoryLogCommand(boolean newAutoDeleteHistoryLog)
  {
    _newAutoDeleteHistoryFile = newAutoDeleteHistoryLog;
    _originalAutoDeleteHistoryFile = _historyLog.getUsesAutomaticHistoryFileDeletion();
  }

  
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalAutoDeleteHistoryFile == _newAutoDeleteHistoryFile)
      return false;

    _historyLog.setUsesAutomaticDeleteHistoryFileDeletion(_newAutoDeleteHistoryFile);
    return true;
  }
  
  
  public void undoCommand() throws XrayTesterException
  {
    _historyLog.setUsesAutomaticDeleteHistoryFileDeletion(_originalAutoDeleteHistoryFile);
  }
  
  
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_AUTO_DELETE_HISTORY_FILE_KEY");
  }

}
