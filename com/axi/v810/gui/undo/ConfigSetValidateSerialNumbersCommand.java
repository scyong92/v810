package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * Command for setting the ValidateSerialNumbers checkbox
 * @author Laura Cormos
 */
public class ConfigSetValidateSerialNumbersCommand extends DataCommand
{
  private boolean _newEnabledState;
  private boolean _oldEnabledState;
  private SerialNumberManager _serialNumberManager = SerialNumberManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetValidateSerialNumbersCommand(boolean enabledState)
  {
    _newEnabledState = enabledState;
    _oldEnabledState = _serialNumberManager.isValidateBoardSerialNumbersEnabled();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_oldEnabledState == _newEnabledState)
      return false;

    _serialNumberManager.setValidateBoardSerialNumbers(_newEnabledState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _serialNumberManager.setValidateBoardSerialNumbers(_oldEnabledState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SN_SET_VALIDATE_SN_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
