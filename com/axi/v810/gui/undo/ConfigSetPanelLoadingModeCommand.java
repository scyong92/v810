package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ConfigSetPanelLoadingModeCommand extends DataCommand
{
  private PanelHandlerPanelLoadingModeEnum _originalLoadingMode;
  private PanelHandlerPanelLoadingModeEnum _newLoadingMode;
  private PanelHandler _panelHandler = PanelHandler.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetPanelLoadingModeCommand(PanelHandlerPanelLoadingModeEnum newLoadingMode)
  {
    Assert.expect(newLoadingMode != null);

    _newLoadingMode = newLoadingMode;
    _originalLoadingMode = _panelHandler.getPanelLoadingMode();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalLoadingMode.equals(_newLoadingMode))
      return false;
    HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _panelHandler.setPanelLoadingMode(_newLoadingMode);
      }
    });

    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _panelHandler.setPanelLoadingMode(_originalLoadingMode);
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_PANEL_LOADING_MODE_KEY");
  }
}
