package com.axi.v810.gui.undo;

import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ConfigEnableBuzzerCommand extends DataCommand
{
  private boolean _originalEnableBuzzer;
  private boolean _newEnableBuzzer;
  private LightStack _lightStack = LightStack.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigEnableBuzzerCommand(boolean newEnableBuzzer)
  {
    _newEnableBuzzer = newEnableBuzzer;
    _originalEnableBuzzer = _lightStack.isBuzzerEnabled();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalEnableBuzzer == _newEnableBuzzer)
      return false;

    if (_newEnableBuzzer)
      _lightStack.setBuzzerEnabled();
    else
      _lightStack.setBuzzerDisabled();
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    if (_originalEnableBuzzer)
      _lightStack.setBuzzerEnabled();
    else
      _lightStack.setBuzzerDisabled();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_ENABLE_BUZZER_KEY");
  }
}
