package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command to copy an existing land pattern to a new one
 * @author Laura Cormos
 */
public class LandPatternCopyCommand extends DataCommand
{
  private LandPattern _newLandPattern = null;
  private LandPattern _originalLandPattern = null;
  private String _newName = null;

  /**
   * @author Laura Cormos
   */
  public LandPatternCopyCommand(LandPattern originalLandPattern, String newName)
  {
    Assert.expect(newName != null);
    Assert.expect(originalLandPattern != null);

    _originalLandPattern = originalLandPattern;
    _newName = newName;
  }

  /**
   * Swap the type
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_originalLandPattern != null);
    Assert.expect(_newName != null);

    if (_newLandPattern == null)
    {
      // this is the first time this command was called, need to create the new landPattern
      _newLandPattern = _originalLandPattern.createDuplicate(_newName);
    }
    else
    {
      // this is a redo action, need to add the duplicate land pattern back into the project
      _newLandPattern.add();
    }

    return true;
  }

  /**
   * Restore the original type
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_newLandPattern != null);

    // remove the duplicate land pattern from the project
    _newLandPattern.remove();
  }

  /**
   * @author Laura Cormos
   */
  public LandPattern getNewLandPattern()
  {
    Assert.expect(_newLandPattern != null);
    return _newLandPattern;
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_LAND_PATTERN_PAD_COPY_TYPE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
