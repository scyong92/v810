package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Kok Chun, Tan
 */
public class ProjectSetDynamicRangeOptimizationVersionCommand extends DataCommand
{
  private Project _project = null;
  private int _newDynamicRangeOptimizationVersion = 0;
  private int _oldDynamicRangeOptimizationVersion = 0;

  /**
   * @author Kok Chun, Tan
   */
  public ProjectSetDynamicRangeOptimizationVersionCommand(Project project, int dynamicRangeOptimizationVersion)
  {
    Assert.expect(project != null);

    _project = project;
    _newDynamicRangeOptimizationVersion = dynamicRangeOptimizationVersion;
    _oldDynamicRangeOptimizationVersion = _project.getDynamicRangeOptimizationVersion().getVersion();
  }

  /**
   * @author Kok Chun, Tan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_oldDynamicRangeOptimizationVersion == _newDynamicRangeOptimizationVersion)
      return false;

    _project.setDynamicRangeOptimizationVersion(_newDynamicRangeOptimizationVersion);
    return true;
  }

  /**
   * Restore the old version of thickness table to be used in this project
   * @author Swee Yee
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setDynamicRangeOptimizationVersion(_oldDynamicRangeOptimizationVersion);
  }

  /**
   * @author Swee Yee
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_PROJECT_DYNAMIC_RANGE_OPTIMIZATION_VERSION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
