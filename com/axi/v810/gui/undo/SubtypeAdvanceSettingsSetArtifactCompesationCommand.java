package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author chin-seong.kee
 */
public class SubtypeAdvanceSettingsSetArtifactCompesationCommand extends DataCommand
{
  private SubtypeAdvanceSettings _subtypeAdvanceSettings = null;
  private ArtifactCompensationStateEnum _origArtificateCompensationState = null;
  private ArtifactCompensationStateEnum _newArtifacteCompensationState = null;

  /**
   * Construct the class.
   * @author Chin Seong
   */
  public SubtypeAdvanceSettingsSetArtifactCompesationCommand(SubtypeAdvanceSettings subtypeAdvanceSettings,
    ArtifactCompensationStateEnum newArtifactCompensationState)
  {
    Assert.expect(subtypeAdvanceSettings != null);
    Assert.expect(newArtifactCompensationState != null);

    _subtypeAdvanceSettings = subtypeAdvanceSettings;
    _newArtifacteCompensationState = newArtifactCompensationState;
    _origArtificateCompensationState = subtypeAdvanceSettings.getArtifactCompensationState();
  }

  /**
   * @author Chin Seong
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_subtypeAdvanceSettings != null);

    if (_newArtifacteCompensationState.equals(_origArtificateCompensationState))
    {
      return false;
    }
    _subtypeAdvanceSettings.setArtifactCompensationState(_newArtifacteCompensationState);

    return true;
  }

  /**
   * @author Chin Seong
   */
  public void undoCommand() throws XrayTesterException
  { 
    Assert.expect(_subtypeAdvanceSettings != null);

    _subtypeAdvanceSettings.setArtifactCompensationState(_origArtificateCompensationState);
  }

  /**
   * @author Chin Seong
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_ADVANCE_SETTINGS_SET_ARTIFACT_COMPENSATION_KEY");
  }
}
