package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting a fiducial type length
 * @author Laura Cormos
 */
public class FiducialTypeSetLengthInNanoMetersCommand extends DataCommand
{
  boolean isBoardFiducial = false;
  private Panel _panel;
  private String _fiducialName;
  private SideBoardType _sideBoardType;
  private int _newLengthInNanoMeters = 0;
  private int _oldLengthInNanoMeters = 0;

  /**
   * @param fiducialType the fiducial type whose length will change
   * @param newLength the new length
   * @author Laura Cormos
   */
  public FiducialTypeSetLengthInNanoMetersCommand(FiducialType fiducialType , int newLength)
  {
    Assert.expect(fiducialType != null);
    Assert.expect(newLength != 0);

    isBoardFiducial = fiducialType.hasSideBoardType();
    if (isBoardFiducial)
      _sideBoardType = fiducialType.getSideBoardType();
    else
      _panel = fiducialType.getFiducials().iterator().next().getPanel();
    _fiducialName = fiducialType.getName();
    _newLengthInNanoMeters = newLength;
    _oldLengthInNanoMeters = fiducialType.getLengthInNanoMeters();
  }

  /**
   * Change the length to the new length
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_fiducialName != null);
    if (isBoardFiducial)
      Assert.expect(_sideBoardType != null);
    else
      Assert.expect(_panel != null);

    if (_oldLengthInNanoMeters == _newLengthInNanoMeters)
      return false;

    FiducialType fiducialType;
    if (isBoardFiducial)
      fiducialType = _sideBoardType.getFiducialType(_fiducialName);
    else
      fiducialType = _panel.getFiducial(_fiducialName).getFiducialType();
    Assert.expect(fiducialType != null);
    fiducialType.setLengthInNanoMeters(_newLengthInNanoMeters);
    return true;
  }

  /**
   * Restore the old length
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_fiducialName != null);
    if (isBoardFiducial)
      Assert.expect(_sideBoardType != null);
    else
      Assert.expect(_panel != null);


   // first find the fiducial type object since it could have been deleted and re added by an undo/redo
   FiducialType fiducialType;
   if (isBoardFiducial)
     fiducialType = _sideBoardType.getFiducialType(_fiducialName);
   else
      fiducialType = _panel.getFiducial(_fiducialName).getFiducialType();
   Assert.expect(fiducialType != null);
   fiducialType.setLengthInNanoMeters(_oldLengthInNanoMeters);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_FIDUCIAL_TYPE_SET_LENGTH_NM_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
