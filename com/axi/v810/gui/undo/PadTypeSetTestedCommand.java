package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.ProjectHistoryLog;
import com.axi.v810.util.*;

/**
 * This class is used to set the subtype (CustomAlgorithmFamily) for a PadType
 * Using this class allows us to undo the set later.
 * @author Andy Mechtenberg
 */
public class PadTypeSetTestedCommand extends DataCommand
{
  private PadType _padType = null;
  private boolean _origTestStatus = false;
  private boolean _newTestStatus = false;
  private Subtype _origSubtype = null;
  private static transient ProjectHistoryLog _fileWriter;

  /**
   * @param padType the PadType whose subtype will change
   * @param testStatus the new test status
   * @author Andy Mechtenberg
   */
  public PadTypeSetTestedCommand(PadType padType, boolean testStatus)
  {
    Assert.expect(padType != null);

    _padType = padType;
    _newTestStatus = testStatus;
    _origTestStatus = _padType.getPadTypeSettings().isInspected();
    _origSubtype = _padType.getPadTypeSettings().getSubtype();
    _fileWriter = ProjectHistoryLog.getInstance();
  }

  /**
   * Change the custom algorithm family (subtype) for a pad type
   * @return true if the command executed, false if new test status is the same as the current one
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_padType != null);

    if (_newTestStatus == _origTestStatus)
      return false;

    _padType.getPadTypeSettings().setInspected(_newTestStatus);
    return true;
  }

  /**
   * Reset the Custom Algorithm Family (subtype)
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_padType != null);

    _padType.getPadTypeSettings().setInspected(_origTestStatus);
    _fileWriter.appendUndoNoTestPadType(_padType,_padType.getJointTypeEnum(),_origSubtype );//keep track subtype undo value -hsia-fen.tan
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PAD_TYPE_SET_TESTED_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
