package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PanelSetAutoPopulateSpacingCommand extends DataCommand
{
  private Panel _panel;
  private int _originalAutoPopulateSpacingInNanoMeters = -1;
  private int _newAutoPopulateSpacingInNanoMeters = -1;
  
  /**
   * This class handles the spacing value used for auto-generating
   * Optical Rectangles across the whole panel.
   * 
   * @author Cheah Lee Herng
   */
  public PanelSetAutoPopulateSpacingCommand(Panel panel, int newAutoPopulateSpacingInNanoMeters)
  {
    Assert.expect(panel != null);
    Assert.expect(newAutoPopulateSpacingInNanoMeters > 0);

    _panel = panel;
    _newAutoPopulateSpacingInNanoMeters = newAutoPopulateSpacingInNanoMeters;
    _originalAutoPopulateSpacingInNanoMeters = _panel.getAutoPopulateSpacingInNanometers();
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean executeCommand() throws XrayTesterException 
  {
    Assert.expect(_panel != null);

    if (MathUtil.fuzzyEquals(_originalAutoPopulateSpacingInNanoMeters, _newAutoPopulateSpacingInNanoMeters, MathUtil._UNIT_CONVERSION_NANOMETER_LIMIT))
      return false;
    
    _panel.setAutoPopulateSpacingInNanometers(_newAutoPopulateSpacingInNanoMeters);
    return true;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void undoCommand() throws XrayTesterException 
  {
    Assert.expect(_panel != null);

    _panel.setAutoPopulateSpacingInNanometers(_originalAutoPopulateSpacingInNanoMeters);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public String getDescription() 
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PANEL_SET_AUTO_POPULATE_SPACING_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
