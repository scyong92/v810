package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.util.*;

/**
 * All commands that the GUI code use that we want to be undo-able by the user should use this class.
 * A block of Commands can be executed as a single command by using the
 * beginCommandBlock() and endCommandBlock() commands.
 * This will cause a set of Commands to be undone and redone as one single Command.
 *
 * @author Bill Darbie
 */
public class CommandManager extends Observable
{
  private static CommandManager _homeInstance;
  private static CommandManager _developTestInstance;
  private static CommandManager _testExecutionInstance;
  private static CommandManager _configInstance;
  private static CommandManager _serviceInstance;
  private static CommandManager _virtualLiveInstance;
  private static CommandManager _packageLibraryInstance;
  private static CommandManager _cadCreatorInstance;

  private com.axi.util.CommandManager _commandManager;
  // if this is true, treat ScreenChangeGuiCommands as special commands
  // in this mode, multiple screen change commands are skipped over.  Only
  // the last one before a ProjectCommand is encountered is executed.
  // From the users point of view this makes undo/redo only work on Project
  // changes and not GUI screen changes.  When the user wants to undo/redo
  // a ProjectCommand, this class will execute the last ScreenChangeGuiCommand
  // to get to the correct screen, then the ProjectCommand undo/redo will be
  // executed.  This way the user sees the undo/redo on the data that they
  // want and the correct screen is visible for where the undo/redo happened.
  private boolean _executeOneScreenChangeGuiCommandPerUndoOrRedo = true;

  private ScreenChangeGuiCommand _prevScreenChangeGuiCommand;
  private Map<com.axi.util.Command, ScreenChangeGuiCommand> _commandToPrevScreenChangeGuiCommandMap = new HashMap<com.axi.util.Command, ScreenChangeGuiCommand>();

  private Object _prevState;
  private Map<com.axi.util.Command, Object> _commandToPrevStateMap = new HashMap<com.axi.util.Command, Object>();

  private BooleanLock _inCommandBlock = new BooleanLock(false); //Siew Yeng - XCR-3240 - change from boolean to BooleanLock

  /**
   * @author Bill Darbie
   */
  private CommandManager()
  {
    _commandManager = new com.axi.util.CommandManager();
  }

  /**
   * @author Bill Darbie
   */
  public static synchronized CommandManager getHomeInstance()
  {
    if (_homeInstance == null)
      _homeInstance = new CommandManager();

    return _homeInstance;
  }

  /**
   * @author Bill Darbie
   */
  public static synchronized CommandManager getDevelopTestInstance()
  {
    if (_developTestInstance == null)
      _developTestInstance = new CommandManager();

    return _developTestInstance;
  }

  /**
   * @author Bill Darbie
   */
  public static synchronized CommandManager getTestExecutionInstance()
  {
    if (_testExecutionInstance == null)
      _testExecutionInstance = new CommandManager();

    return _testExecutionInstance;
  }

  /**
   * @author Bill Darbie
   */
  public static synchronized CommandManager getConfigInstance()
  {
    if (_configInstance == null)
      _configInstance = new CommandManager();

    return _configInstance;
  }

  /**
   * @author Bill Darbie
   */
  public static synchronized CommandManager getServiceInstance()
  {
    if (_serviceInstance == null)
      _serviceInstance = new CommandManager();

    return _serviceInstance;
  }
  
  /*
   * @author Chin-Seong.Kee
   */
  public static synchronized CommandManager getCadCreatorInstance()
  {
    if(_cadCreatorInstance == null)
      _cadCreatorInstance = new CommandManager();
    
    return _cadCreatorInstance;
  }

  /**
   * @author Bill Darbie
   */
  public static synchronized CommandManager getVirtualLiveInstance()
  {
    if (_virtualLiveInstance == null)
      _virtualLiveInstance = new CommandManager();

    return _virtualLiveInstance;
  }

  /**
   * @author Wei Chin, Chong
   */
  public static synchronized CommandManager getPackageLibraryInstance()
  {
    if (_packageLibraryInstance == null)
      _packageLibraryInstance = new CommandManager();

    return _packageLibraryInstance;
  }

  /**
   * Call this method before calling execute() if you want to track the state your GUI is in.
   * When an undo() or redo() is called an Observer of this class will get the Object passed
   * in for a particular command passed out through the update call.
   *
   * @author Bill Darbie
   */
  public void trackState(Object state)
  {
    _prevState = state;
  }

  /**
   * Execute the Command passed in.
   *
   * @return true if the Command passed in does change state when it is executed and therefore
   * is put on the Command list
   *
   * @author Bill Darbie
   */
  public boolean execute(Command command) throws XrayTesterException
  {
    Assert.expect(command != null);

    Assert.expect(_commandManager != null);
    boolean executed = false;
    try
    {
      if (_executeOneScreenChangeGuiCommandPerUndoOrRedo == false)
        executed = _commandManager.execute(command);
      else
      {
        if (command instanceof ScreenChangeGuiCommand)
        {
          // do not put ScreenChangeGuiCommands on the command list
          // but still execute them
          _prevScreenChangeGuiCommand = (ScreenChangeGuiCommand)command;
          executed = command.execute();
        }
        else if (command instanceof DataCommand)
        {
          executed = _commandManager.execute(command);
          if (executed)
          {
            if (_prevScreenChangeGuiCommand != null)
            {
              Object prev = _commandToPrevScreenChangeGuiCommandMap.put(command, _prevScreenChangeGuiCommand);
              Assert.expect(prev == null);
            }
            if (_prevState != null)
            {
              Object prev = _commandToPrevStateMap.put(command, _prevState);
              Assert.expect(prev == null);
            }
          }
        }
        else
          Assert.expect(false);
      }
    }
    catch (XrayTesterException ex)
    {
      throw ex;
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }

    return executed;
  }

  /**
   * Execute the Command passed in.  GuiCommands should never throw any exceptions so
   * this method does not declare any Exception
   *
   * @return true if the GuiCommand passed in does change state when it is executed and therefore
   * is put on the Command list
   *
   * @author Bill Darbie
   */
  public boolean execute(GuiCommand guiCommand)
  {
    boolean executed = false;

    try
    {
      executed = execute((Command)guiCommand);
    }
    catch(XrayTesterException ex)
    {
      // a GuiCommand should not throw an Exception
      Assert.logException(ex);
    }

    return executed;
  }

  /**
   * @return true if an undo() operation can be done.
   *
   * @author Bill Darbie
   */
  public boolean isUndoAvailable()
  {
    StringBuffer description = new StringBuffer();

    return isUndoAvailable(description);
  }

  /**
   * @param description is the description of the next undo Command.
   * @return true if an undo() operation can be done.
   *
   * @author Bill Darbie
   */
  public boolean isUndoAvailable(StringBuffer description)
  {
    Assert.expect(description != null);
    Assert.expect(_commandManager != null);

    if (description.length() > 0)
      description.replace(0, description.length(), "");

    boolean isUndoAvailable = _commandManager.isUndoAvailable(description);

    return isUndoAvailable;
  }

  /**
   * Undo the last executed Command.
   *
   * @return the number of Commands that have been executed.  This can be more than one
   * because of BlockCommands
   *
   * @author Bill Darbie
   */
  public int undo() throws XrayTesterException
  {
    Assert.expect(isUndoAvailable());

    Assert.expect(_commandManager != null);
    int numCommands = 0;
    try
    {
      com.axi.util.Command command = _commandManager.getUndoCommands().get(0);

      if (_executeOneScreenChangeGuiCommandPerUndoOrRedo)
      {
        ScreenChangeGuiCommand screenChangeCommand = _commandToPrevScreenChangeGuiCommandMap.get(command);
        if (screenChangeCommand != null)
        {
          _prevScreenChangeGuiCommand = screenChangeCommand;
          screenChangeCommand.execute();
          numCommands += screenChangeCommand.getNumCommands();
        }
      }

      sendStateUpdate(command);

      _commandManager.undo();
      numCommands += command.getNumCommands();

      if (_prevScreenChangeGuiCommand != null)
        _commandToPrevScreenChangeGuiCommandMap.put(command, _prevScreenChangeGuiCommand);

    }
    catch (XrayTesterException ex)
    {
      throw ex;
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }
    return numCommands;
  }

  /**
   * @return true if a redo() operation can be done.
   *
   * @author Bill Darbie
   */
  public boolean isRedoAvailable()
  {
    StringBuffer description = new StringBuffer();

    return isRedoAvailable(description);
  }

  /**
   * @return true if a redo() operation can be done.
   *
   * @author Bill Darbie
   */
  public boolean isRedoAvailable(StringBuffer description)
  {
    Assert.expect(description != null);
    Assert.expect(_commandManager != null);

    if (description.length() > 0)
      description.replace(0, description.length(), "");

    boolean isRedoAvailable = _commandManager.isRedoAvailable(description);

    return isRedoAvailable;
  }


  /**
   * Redo the last undo Command.
   *
   * @return the number of Commands that have been executed.  This can be more than one
   * because of BlockCommands
   *
   * @author Bill Darbie
   */
  public int redo() throws XrayTesterException
  {
    Assert.expect(isRedoAvailable());
    Assert.expect(_commandManager != null);

    int numCommands = 0;
    try
    {
      com.axi.util.Command command = _commandManager.getRedoCommands().get(0);

      if (_executeOneScreenChangeGuiCommandPerUndoOrRedo)
      {
        ScreenChangeGuiCommand screenChangeCommand = _commandToPrevScreenChangeGuiCommandMap.get(command);
        if (screenChangeCommand != null)
        {
          _prevScreenChangeGuiCommand = screenChangeCommand;
          screenChangeCommand.execute();
          numCommands += screenChangeCommand.getNumCommands();
        }
      }
      sendStateUpdate(command);

      _commandManager.redo();
      numCommands += command.getNumCommands();

      if (_prevScreenChangeGuiCommand != null)
        _commandToPrevScreenChangeGuiCommandMap.put(command, _prevScreenChangeGuiCommand);

    }
    catch (XrayTesterException ex)
    {
      throw ex;
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }

    return numCommands;
  }

  /**
   * Mark the beginning of a command block.  All the Commands executed within a command block
   * are treated as a single command for the purposes of undo and redo commands.
   *
   * @author Bill Darbie
   */
  public void beginCommandBlock(String description)
  {
    Assert.expect(description != null);
    Assert.expect(_inCommandBlock.isFalse());

    Assert.expect(_commandManager != null);
    BlockCommand blockCommand = _commandManager.beginCommandBlock(description);
    if (_executeOneScreenChangeGuiCommandPerUndoOrRedo)
    {
      if (_prevScreenChangeGuiCommand != null)
      {
        Object prev = _commandToPrevScreenChangeGuiCommandMap.put(blockCommand, _prevScreenChangeGuiCommand);
        Assert.expect(prev == null);
      }
    }
    if (_prevState != null)
    {
      Object prev = _commandToPrevStateMap.put(blockCommand, _prevState);
      Assert.expect(prev == null);
    }
    _inCommandBlock.setValue(true);
  }

  /**
   * Mark the end of a Command block.  All the Commands executed within a command block
   * are treated as a single command for the purposes of undo and redo commands.
   *
   * @author Bill Darbie
   */
  public void endCommandBlock()
  {
    Assert.expect(_commandManager != null);
    _commandManager.endCommandBlock();
    _inCommandBlock.setValue(false);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void endCommandBlockIfNecessary()
  {
    Assert.expect(_commandManager != null);
    if (_inCommandBlock.isTrue())
    {
      _commandManager.endCommandBlock();
    }
    _inCommandBlock.setValue(false);
  }

  /**
   * Clear out the list of commands built up so far.
   *
   * @author Bill Darbie
   */
  public void clear()
  {
    Assert.expect(_commandManager != null);
    _commandManager.clear();
    _commandToPrevScreenChangeGuiCommandMap.clear();
    _commandToPrevStateMap.clear();
  }

  /**
   * if this is true, treat ScreenChangeGuiCommands as special commands
   * In this mode, multiple screen change commands are skipped over.  Only
   * the last one before a ProjectCommand is encountered is executed.
   * From the users point of view this makes undo/redo only work on Project
   * changes and not GUI screen changes.  When the user wants to undo/redo
   * a ProjectCommand, this class will execute the last ScreenChangeGuiCommand
   * to get to the correct screen, then the ProjectCommand undo/redo will be
   * executed.  This was the user sees the undo/redo on the data that they
   * want and the correct screen is visible for where the undo/redo happened.
   * If this is false, all commands, including ScreenChangeGuiCommands are
   * treated the same, so a screen change command can by itself by undone or redone.
   *
   * @author Bill Darbie
   */
  public void executeOneScreenChangeGuiCommandsPerUndoOrRedo(boolean executeOneScreenChangeGuiCommandPerUndoOrRedo)
  {
    _executeOneScreenChangeGuiCommandPerUndoOrRedo = executeOneScreenChangeGuiCommandPerUndoOrRedo;
  }

  /**
   * if this is true then all undo and redo commands also go onto the command list
   * this is identical to the emacs undo.
   * if this is false then undo and redo commands do not go on the command list.
   * This is a simpler, but less powerful mode.
   * @author Bill Darbie
   */
  public void setUndoAndRedoCommandsGoOnCommandList(boolean undoAndRedoCommandsGoOnCommandList)
  {
    _commandManager.setUndoAndRedoCommandsGoOnCommandList(undoAndRedoCommandsGoOnCommandList);
  }

  /**
   * @author Bill Darbie
   */
  private void sendStateUpdate(com.axi.util.Command command)
  {
    Assert.expect(command != null);

    Object state = _commandToPrevStateMap.get(command);
    if (state != null)
    {
      setChanged();
      notifyObservers(state);
    }
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean isExecutingCommand()
  {
    return _inCommandBlock.isTrue();
  }
  
  /**
   * XCR-3240
   * @author Siew Yeng
   */
  public void waitUntilCommandEnd()
  {
    try 
    {
      //timeout after 2minutes
      _inCommandBlock.waitUntilFalse(120000);
    } 
    catch (InterruptedException ex) 
    {
      //do nothing
    }
  }
}
