package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * Command for setting the production tune email notification property
 * @author Andy Mechtenberg
 */
public class ConfigSetProductionTuneNotifyEmailCommand extends DataCommand
{
  private ProductionTuneData _productionTuneData = null;
  private String _newNotifyEmail = null;
  private String _oldNotifyEmail = null;

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetProductionTuneNotifyEmailCommand(ProductionTuneData productionTuneData,
                                                   String notifyEmail)
  {
    Assert.expect(productionTuneData != null);
    Assert.expect(notifyEmail != null);

    _productionTuneData = productionTuneData;
    _newNotifyEmail = notifyEmail;
    _oldNotifyEmail = _productionTuneData.getNotificationEmailAddress();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);

    _productionTuneData.setNotificationEmailAddress(_newNotifyEmail);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);

    _productionTuneData.setNotificationEmailAddress(_oldNotifyEmail);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_PRODUCTION_TUNE_SET_NOTIFY_EMAIL_KEY");
  }
}
