package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the cad directory in config is changed, it can be
 * reset via an undo command.
 *
 * @author Andy Mechtenberg
 */
public class ConfigSetCadDirectoryCommand extends DataCommand
{

  private String _originalCadDirectory;
  private String _newCadDirectory;

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetCadDirectoryCommand(String newCadDirectory)
  {
    Assert.expect(newCadDirectory != null);

    _newCadDirectory = newCadDirectory;
    _originalCadDirectory = Directory.getCadDirectory();
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalCadDirectory.equalsIgnoreCase(_newCadDirectory))
    {
      return false;
    }

    Directory.setCadDir(_newCadDirectory);
    return true;
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public void undoCommand() throws XrayTesterException
  {
    Directory.setCadDir(_originalCadDirectory);
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_CAD_DIRECTORY_KEY");
  }
}
