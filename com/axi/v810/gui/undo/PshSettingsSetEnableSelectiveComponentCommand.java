package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author siew-yeng.phang
 */
public class PshSettingsSetEnableSelectiveComponentCommand extends DataCommand
{
  private PshSettings _pshSettings = null;
  
  private boolean _newStatus = false;
  private boolean _oldStatus = false;
  
  /**
   * @author Siew Yeng
   */
  public PshSettingsSetEnableSelectiveComponentCommand(PshSettings pshSettings, boolean enable)
  {
    Assert.expect(pshSettings != null);

    _pshSettings = pshSettings;
    _newStatus = enable;
    _oldStatus = pshSettings.isEnabledSelectiveComponent();
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean executeCommand() throws XrayTesterException 
  {
    if(_newStatus == _oldStatus)
      return false;
    
    _pshSettings.setEnableSelectiveComponent(_newStatus);
    
    return true;
  }

  /**
   * @author Siew Yeng
   */
  public void undoCommand() throws XrayTesterException 
  {
    _pshSettings.setEnableSelectiveComponent (_oldStatus);
  }

  /**
   * @author Siew Yeng
   */
  public String getDescription() 
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PSH_SET_ENABLE_SELECTIVE_COMPONENT_COMMAND_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
  
}
