package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Use this class to flip the panel.
 * If the panel needs to be unflipped, use undo()
 * @author George A. David
 */
public class PanelFlipCommand extends DataCommand
{
  private Panel _panel;

  /**
   * @param panel the panel to flip
   * @author George A. David
   */
  public PanelFlipCommand(Panel panel)
  {
    Assert.expect(panel != null);

    _panel = panel;
  }

  /**
   * Flip the panel
   * @author George A. David
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_panel != null);

    _panel.getPanelSettings().flip();

    return true;
  }

  /**
   * Unflip the panel (by flipping it)
   * @author George A. David
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_panel != null);

    _panel.getPanelSettings().flip();
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PANEL_FLIP_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
