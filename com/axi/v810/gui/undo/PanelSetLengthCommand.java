package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * This class is used to set the length of the panel.
 * Using this class allows us to undo the set later.
 * @author George A. David
 */
public class PanelSetLengthCommand extends DataCommand
{
  private Panel _panel;
  private int _originalLengthInNanoMeters = -1;
  private int _newLengthInNanoMeters = -1;

  /**
   * @param panel the panel whose length will change
   * @param newLengthInNanoMeters the new length of the panel in nanometers
   * @author George A. David
   */
  public PanelSetLengthCommand(Panel panel, int newLengthInNanoMeters)
  {
    Assert.expect(panel != null);
    Assert.expect(newLengthInNanoMeters > 0);

    _panel = panel;
    _newLengthInNanoMeters = newLengthInNanoMeters;
    _originalLengthInNanoMeters = _panel.getLengthAfterAllRotationsInNanoMeters();
  }

  /**
   * Change the length of the panel
   * @author George A. David
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_panel != null);

    if (MathUtil.fuzzyEquals(_newLengthInNanoMeters, _originalLengthInNanoMeters, MathUtil._UNIT_CONVERSION_NANOMETER_LIMIT))
      return false;

    _panel.setLengthAfterAllRotationsRetainingBoardCoordsInNanoMeters(_newLengthInNanoMeters);
    return true;
  }

  /**
   * Reset the length of the panel
   * @author George A. David
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_panel != null);

    _panel.setLengthAfterAllRotationsRetainingBoardCoordsInNanoMeters(_originalLengthInNanoMeters);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PANEL_SET_LENGTH_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
