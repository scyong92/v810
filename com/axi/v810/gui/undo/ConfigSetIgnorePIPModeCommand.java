package com.axi.v810.gui.undo;

import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
/**
 * This class is used so that if the mail server in config is changed,
 * it can be reset via an undo command.
 * @author Chnee Khang Wah (12-Oct-2009)
 */

public class ConfigSetIgnorePIPModeCommand extends DataCommand
{
    private boolean _originalIgnorePIPMode;
  private boolean _newIgnorePIPMode;
  private PanelHandler _panelHandler = PanelHandler.getInstance();

  /**
   * @author  Chnee Khang Wah (12-Oct-2009)
   */
  public ConfigSetIgnorePIPModeCommand(boolean newIgnorePIPMode)
  {
    _newIgnorePIPMode = newIgnorePIPMode;
    _originalIgnorePIPMode = _panelHandler.isIgnorePIPModeEnabled();
  }

  /**
   * @author  Chnee Khang Wah (12-Oct-2009)
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalIgnorePIPMode == _newIgnorePIPMode)
      return false;

    _panelHandler.setIgnorePIPModeEnabled(_newIgnorePIPMode);
    return true;
  }

  /**
   * @author  Chnee Khang Wah (12-Oct-2009)
   */
  public void undoCommand() throws XrayTesterException
  {
    _panelHandler.setIgnorePIPModeEnabled(_originalIgnorePIPMode);
  }

  /**
   * @author  Chnee Khang Wah (12-Oct-2009)
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_IGNORE_PIP_MODE_KEY");
  }
}
