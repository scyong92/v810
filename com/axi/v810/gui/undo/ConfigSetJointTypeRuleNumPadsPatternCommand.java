package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting the joint type assignment rule's number of pads pattern
 * @author Laura Cormos
 */
public class ConfigSetJointTypeRuleNumPadsPatternCommand extends DataCommand
{
  private JointTypeEnumRule _jointTypeEnumRule = null;
  private String _newNumPadsPattern = null;
  private String _oldNumPadsPattern = null;

  /**
   * @author Laura Cormos
   */
  public ConfigSetJointTypeRuleNumPadsPatternCommand(JointTypeEnumRule jointTypeEnumRule , String numPadsPattern)
  {
    Assert.expect(jointTypeEnumRule != null);
    Assert.expect(numPadsPattern != null);

    _jointTypeEnumRule = jointTypeEnumRule;
    _newNumPadsPattern = numPadsPattern;
    if (_jointTypeEnumRule.isNumPadsSet())
      _oldNumPadsPattern = _jointTypeEnumRule.getNumPadsPattern();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    if (_oldNumPadsPattern != null && _oldNumPadsPattern.equals(_newNumPadsPattern))
      return false;

    if (_newNumPadsPattern.length() == 0)
      _jointTypeEnumRule.clearNumPadsPattern();
    else
    _jointTypeEnumRule.setNumPadsPattern(_newNumPadsPattern);

    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    if (_oldNumPadsPattern == null)
      _jointTypeEnumRule.clearNumPadsPattern();
    else
      _jointTypeEnumRule.setNumPadsPattern(_oldNumPadsPattern);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_JTA_SET_JOINT_TYPE_RULE_NUM_PADS_PATTERN_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
