package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.config.Config;
import com.axi.v810.datastore.config.SoftwareConfigEnum;
import com.axi.v810.hardware.XrayTester;
import com.axi.v810.util.*;

/**
 * This class is used to set the thickness of the panel.
 * Using this class allows us to undo the set later.
 * @author Andy Mechtenberg
 */
public class PanelSetThicknessCommand extends DataCommand
{
  private Panel _panel;
  private int _originalThicknessInNanoMeters = -1;
  private int _newThicknessInNanoMeters = -1;

  /**
   * @param panel the panel whose thickness will change
   * @param newThicknessInNanoMeters the new thickness of the panel in nanometers
   * @author George A. David
   */
  public PanelSetThicknessCommand(Panel panel, int newThicknessInNanoMeters)
  {
    Assert.expect(panel != null);
    Assert.expect(newThicknessInNanoMeters > 0);

    _panel = panel;
    _newThicknessInNanoMeters = newThicknessInNanoMeters;
    _originalThicknessInNanoMeters = _panel.getThicknessInNanometers();
    //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on";
    if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
      _originalThicknessInNanoMeters = _originalThicknessInNanoMeters - XrayTester.getSystemPanelThicknessOffset();
  }

  /**
   * Change the thickness of the panel
   * @author George A. David
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_panel != null);

    if (MathUtil.fuzzyEquals(_newThicknessInNanoMeters, _originalThicknessInNanoMeters, MathUtil._UNIT_CONVERSION_NANOMETER_LIMIT))
      return false;

    _panel.setThicknessInNanoMeters(_newThicknessInNanoMeters);
    return true;
  }

  /**
   * Reset the thickness of the panel
   * @author George A. David
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_panel != null);

    _panel.setThicknessInNanoMeters(_originalThicknessInNanoMeters);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PANEL_SET_THICKNESS_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
