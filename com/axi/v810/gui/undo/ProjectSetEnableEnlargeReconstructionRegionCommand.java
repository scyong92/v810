package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This project is used store the state of enlarge reconstruction region
 * @author Siew Yeng
 */
public class ProjectSetEnableEnlargeReconstructionRegionCommand extends DataCommand
{
  private Project _project = null;
  private boolean _newEnlargeReconstructionRegion = false;
  private boolean _oldEnlargeReconstructionRegion = false;

  /**
   * @param project the project will enlarge reconstruction region
   * @param isEnlargeReconstructionRegion the new state of enlarge reconstruction region
   * @author Siew Yeng
   */
  public ProjectSetEnableEnlargeReconstructionRegionCommand(Project project, boolean isEnlargeReconstructionRegion)
  {
    Assert.expect(project != null);

    _project = project;
    _newEnlargeReconstructionRegion = isEnlargeReconstructionRegion;
    _oldEnlargeReconstructionRegion = _project.isEnlargeReconstructionRegion();
  }

  /**
   * Change the state of enlarge reconstruction region
   * @author Siew Yeng
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_oldEnlargeReconstructionRegion == _newEnlargeReconstructionRegion)
      return false;

    _project.setEnlargeReconstructionRegion(_newEnlargeReconstructionRegion);
    return true;
  }

  /**
   * Restore the old state of enable enlarge reconstruction region
   * @author Siew Yeng
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setEnlargeReconstructionRegion(_oldEnlargeReconstructionRegion);
  }

  /**
   * @author Siew Yeng
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_ENABLE_ENLARGE_RECONSTRUCTION_REGION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
