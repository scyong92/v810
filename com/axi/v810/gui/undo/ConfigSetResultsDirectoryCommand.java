package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the mail server in config is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ConfigSetResultsDirectoryCommand extends DataCommand
{
  private String _originalResultsDir;
  private String _newResultsDir;

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetResultsDirectoryCommand(String newResultsDir)
  {
    Assert.expect(newResultsDir != null);

    _newResultsDir = newResultsDir;
    _originalResultsDir = Directory.getResultsDir();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalResultsDir.equalsIgnoreCase(_newResultsDir))
      return false;

    Directory.setResultsDir(_newResultsDir);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Directory.setResultsDir(_originalResultsDir);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_RESULTS_DIRECTORY_KEY");
  }
}
