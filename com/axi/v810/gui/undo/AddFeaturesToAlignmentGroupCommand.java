package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * Command for setting a component type coordinate
 * @author Laura Cormos
 */
public class AddFeaturesToAlignmentGroupCommand extends DataCommand
{
  private AlignmentGroup _alignmentGroup = null;
  private List<Pad> _pads;
  private List<Fiducial> _fiducials;

  /**
   * @param alignmentGroup The alignment group to add the new pad to
   * @param pads the Pads to add in
   * @param fiducials the Fiducials to add in
   * @author Andy Mechtenberg
   */
  public AddFeaturesToAlignmentGroupCommand(AlignmentGroup alignmentGroup, List<Pad> pads, List<Fiducial> fiducials)
  {
    Assert.expect(alignmentGroup != null);
    Assert.expect(pads != null);
    Assert.expect(fiducials != null);

    _alignmentGroup = alignmentGroup;
    _pads = pads;
    _fiducials = fiducials;
  }

  /**
   * Add the features to the group
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    // don't do anything if both lists of pads and fiducials are empty
    if (_pads.isEmpty() && _fiducials.isEmpty())
      return false;

    for(Pad pad : _pads)
      _alignmentGroup.addPad(pad);
    for(Fiducial fiducial : _fiducials)
      _alignmentGroup.addFiducial(fiducial);

    return true;
  }

  /**
   * Remove the feature (opposite of add)
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    for(Pad pad : _pads)
      _alignmentGroup.removePad(pad);
    for(Fiducial fiducial : _fiducials)
      _alignmentGroup.removeFiducial(fiducial);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_ADD_FEATURE_TO_ALIGNMENT_GROUP_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
