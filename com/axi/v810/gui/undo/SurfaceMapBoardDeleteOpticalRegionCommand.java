/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 * This class is used to delete OpticalRegion from BoardSurfaceMapSettings
 * Using this class allows us to undo the set later.
 * @author Ying-Huan.Chu
 */
public class SurfaceMapBoardDeleteOpticalRegionCommand extends DataCommand
{
  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  private BoardSurfaceMapSettings _boardSurfaceMapSettings = null;
  private OpticalRegion _opticalRegion = null;
  private java.util.List<OpticalCameraRectangle> _opticalCameraRectangles = new ArrayList<>();
  private java.util.List<com.axi.v810.business.panelDesc.Component> _components = new ArrayList<>();
  private Map<Board, Map<String, PanelCoordinate>> _boardNameToPointPanelCoordinateMap = new LinkedHashMap<Board, Map<String, PanelCoordinate>>();
  private Project _project;
  
  private java.util.List<Board> _previousBoardSelection = null;
  
  /**
   * @param boardSurfaceMapSettings the BoardSurfaceMapSettings to have its OpticalRegion removed.
   * @param opticalRegion the opticalRegion to be deleted.
   * @author Ying-Huan.Chu
   */
  public SurfaceMapBoardDeleteOpticalRegionCommand(Project project, BoardSurfaceMapSettings boardSurfaceMapSettings, OpticalRegion opticalRegion)
  {
    Assert.expect(project != null);
    Assert.expect(boardSurfaceMapSettings != null);
    Assert.expect(opticalRegion != null);
    
    _boardSurfaceMapSettings = boardSurfaceMapSettings;
    _opticalRegion = opticalRegion;
    _project = project;

    _opticalCameraRectangles.addAll(_opticalRegion.getOpticalCameraRectangles(_boardSurfaceMapSettings.getBoard()));
    _components.addAll(_opticalRegion.getComponents(_boardSurfaceMapSettings.getBoard()));
    _boardNameToPointPanelCoordinateMap.putAll(_opticalRegion.getBoardNameToPointPanelCoordinateMap());
    _previousBoardSelection = PspAutoPopulatePanel.getInstance().getBoardsToApplyOpticalCameraRectangles();
  }
  
  /**
   * Delete the OpticalCameraRectangle from the OpticalRegion
   * @return true if the command executed
   * @throws XrayTesterException if the command fails
   * @author Ying-Huan.Chu
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_boardSurfaceMapSettings != null);
    Assert.expect(_opticalRegion != null);
    
    _projectObservable.setEnabled(false);
    try
    {
      _boardSurfaceMapSettings.removeSurfaceMapRegion(_opticalRegion);
      _opticalRegion.removeAllOpticalCameraRectangles(_boardSurfaceMapSettings.getBoard());
      _opticalRegion.removeAllComponents(_boardSurfaceMapSettings.getBoard());
      _opticalRegion.removeAllPanelCoordinates(_boardSurfaceMapSettings.getBoard());
      _project.getTestProgram().removeOpticalRegion(_opticalRegion);
    }
    finally
    {
      _projectObservable.setEnabled(true);
      _projectObservable.stateChanged(this, _opticalRegion, null, BoardSurfaceMapEventEnum.REMOVE_SURFACE_MAP_OPTICAL_REGION);
    }
    
    return true;
  }

  /**
   * Reset back to the original OpticalRegion
   * @throws XrayTesterException if the command fails
   * @author Ying-Huan.Chu
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_boardSurfaceMapSettings != null);
    Assert.expect(_opticalRegion != null);
    
    _projectObservable.setEnabled(false);
    try
    {
      for (OpticalCameraRectangle opticalCameraRectangle : _opticalCameraRectangles)
      {
        _opticalRegion.addBoardOpticalCameraRectangle(_boardSurfaceMapSettings.getBoard(), opticalCameraRectangle);
      }
      for (com.axi.v810.business.panelDesc.Component component : _components)
      {
        _opticalRegion.addComponent(_boardSurfaceMapSettings.getBoard(), component);
      }
      for (Map.Entry<Board, Map<String, PanelCoordinate>> entry : _boardNameToPointPanelCoordinateMap.entrySet())
      {
        Board board = entry.getKey();
        if (board.equals(_boardSurfaceMapSettings.getBoard()))
        {
          Map<String, PanelCoordinate> pointPanelPositionNameToPointPanelCoordinateMap = entry.getValue();
          for (Map.Entry<String, PanelCoordinate> pointPanelPositionNameToPointPanelCoordinate : pointPanelPositionNameToPointPanelCoordinateMap.entrySet())
          {
            int pointXCoordinateInNanometers = pointPanelPositionNameToPointPanelCoordinate.getValue().getX();
            int pointYCoordinateInNanometers = pointPanelPositionNameToPointPanelCoordinate.getValue().getY();
            _opticalRegion.addSurfaceMapPoints(_boardSurfaceMapSettings.getBoard(), pointXCoordinateInNanometers, pointYCoordinateInNanometers);
          }
        }
      }
      _boardSurfaceMapSettings.addOpticalRegion(_opticalRegion);
      _project.getTestProgram().addOpticalRegion(_opticalRegion, _boardSurfaceMapSettings.getBoard());
      PspAutoPopulatePanel.getInstance().setBoardsToApplyOpticalCameraRectangle(_previousBoardSelection);
    }
    finally
    {
      _projectObservable.setEnabled(true);
      _projectObservable.stateChanged(this, null, _opticalRegion, BoardSurfaceMapEventEnum.ADD_SURFACE_MAP_OPTICAL_REGION);
    }
  }

  /**
   * @author Ying-Huan.Chu
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PSP_DELETE_OPTICAL_REGION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
