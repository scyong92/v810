package com.axi.v810.gui.undo;

import java.util.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.config.*;
import com.axi.v810.util.*;

/**
 * This class will delete a serial number pattern from the set of matching, skipped or valid serial number lists
 * and the deletion can be reset via an undo command.
 * @author Laura Cormos
 */
public class ConfigDeleteSerialNumberEntryCommand extends DataCommand
{
  private java.util.List<SerialNumberRegularExpressionData> _deletedSerialNumberData;
  private JTable _table;
  private int _serialNumberType;
  private ProductionOptionsSerialNumberTabPanel _parent;

  /**
   * @author Laura Cormos
   */
  public ConfigDeleteSerialNumberEntryCommand(JTable table,
                                              ProductionOptionsSerialNumberTabPanel parent,
                                              int serialNumberType)
  {
    Assert.expect(table != null);
    Assert.expect(parent != null);

    _table = table;
    _serialNumberType = serialNumberType;
    _parent = parent;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    int selectedRowIndex = -1;
    int[] selectedRows = _table.getSelectedRows();

    // for a 'redo' action just go ahead and delete the items previously added in the 'undo' part
    if (_deletedSerialNumberData == null) // this is the first time this command is executed
    {
      _deletedSerialNumberData = new ArrayList<SerialNumberRegularExpressionData>();
      // first mark the items to be deleted, can't delete as you go since it will reindex the table model container
      for (int index : selectedRows)
      {
        switch (_serialNumberType)
        {
          case 0: // matching serial numbers
            SerialNumberMatchingTable matchingSNTable = (SerialNumberMatchingTable)_table;
            _deletedSerialNumberData.add(((SerialNumberMatchingTableModel)matchingSNTable.getModel()).getElementAt(index));
            break;
          case 1:
          case 2: // skipped or valid serial numbers
            SerialNumberSkipOrValidateTable skippedOrValidSNTable = (SerialNumberSkipOrValidateTable)_table;
            _deletedSerialNumberData.add(((SerialNumberSkipOrValidateTableModel)skippedOrValidSNTable.getModel()).
                                         getElementAt(index));
            break;
          default:
            Assert.expect(false, "Cannot delete unknown serial number type");
            break;
        }
      }
    }
    Assert.expect(_deletedSerialNumberData != null && _deletedSerialNumberData.isEmpty() == false);
    // now do the actual deletion
    for (SerialNumberRegularExpressionData itemToBeDeleted : _deletedSerialNumberData)
    {
      switch (_serialNumberType)
      {
        case 0: // matching serial numbers
          SerialNumberMatchingTable matchingSNTable = (SerialNumberMatchingTable)_table;
          selectedRowIndex = ((SerialNumberMatchingTableModel)matchingSNTable.getModel()).getIndexOf(itemToBeDeleted);
          ((SerialNumberMatchingTableModel)matchingSNTable.getModel()).deleteElement(itemToBeDeleted);
          break;
        case 1:
        case 2: // skipped or valid serial numbers
          SerialNumberSkipOrValidateTable skippedOrValidSNTable = (SerialNumberSkipOrValidateTable)_table;
          selectedRowIndex = ((SerialNumberSkipOrValidateTableModel)skippedOrValidSNTable.getModel()).getIndexOf(itemToBeDeleted);
          ((SerialNumberSkipOrValidateTableModel)skippedOrValidSNTable.getModel()).deleteElement(itemToBeDeleted);
          break;
        default:
          Assert.expect(false, "Cannot delete unknown serial number type");
          break;
      }
    }
    // set another selection when one row is removed
    int lastRowIndex = _table.getModel().getRowCount() - 1;
    if (lastRowIndex >= 0)
    {
      if (selectedRowIndex > lastRowIndex)
        _table.setRowSelectionInterval(lastRowIndex, lastRowIndex);
      else
        _table.setRowSelectionInterval(selectedRowIndex, selectedRowIndex);
    }
    _parent.setSerialNumbersDataIsDirty(_serialNumberType);
    _parent.setDeleteButtonsState();

    ConfigObservable.getInstance().stateChanged(_deletedSerialNumberData);

    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_deletedSerialNumberData != null);

    for (SerialNumberRegularExpressionData itemToBeAdded : _deletedSerialNumberData)
    {
      switch (_serialNumberType)
      {
        case 0: // matching serial numbers
          SerialNumberMatchingTable matchingSNTable = (SerialNumberMatchingTable)_table;
          ((SerialNumberMatchingTableModel)matchingSNTable.getModel()).addRow(itemToBeAdded);
          break;
        case 1:
        case 2: // skipped or valid serial numbers
          SerialNumberSkipOrValidateTable skippedOrValidSNTable = (SerialNumberSkipOrValidateTable)_table;
          ((SerialNumberSkipOrValidateTableModel)skippedOrValidSNTable.getModel()).addRow(itemToBeAdded);
          break;
        default:
          Assert.expect(false, "Unknown serial number type");
          break;
      }
    }
    // set table selection on the added row
    _table.setRowSelectionInterval(_table.getModel().getRowCount() - 1, _table.getModel().getRowCount() - 1);
    _parent.setSerialNumbersDataIsDirty(_serialNumberType);
    _parent.setDeleteButtonsState();

    ConfigObservable.getInstance().stateChanged(_deletedSerialNumberData);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SN_DELETE_ROW_KEY");
  }
}
