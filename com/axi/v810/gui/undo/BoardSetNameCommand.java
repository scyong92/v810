package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class BoardSetNameCommand extends DataCommand
{
  private Board _board;
  private String _newName;
  private String _oldName;

  /**
   * @author Bill Darbie
   */
  public BoardSetNameCommand(Board board, String name)
  {
    Assert.expect(board != null);
    Assert.expect(name != null);

    _board = board;
    _newName = name;
    _oldName = _board.getName();
  }

  /**
   * @author Bill Darbie
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_board != null);

    if (_oldName.equals(_newName))
      return false;

    _board.setName(_newName);
    
    
    // Update SurfaceMap info
    if (_board.hasBoardSurfaceMapSettings())
    {
      BoardSurfaceMapSettings boardSurfaceMapSettings = _board.getBoardSurfaceMapSettings();
      if (boardSurfaceMapSettings.hasOpticalRegion())
      {
        for(OpticalRegion opticalRegion : boardSurfaceMapSettings.getOpticalRegions())
        {
          boardSurfaceMapSettings.updateSurfaceMapRegion(opticalRegion, _oldName, _newName);
        }
      }
    }
    
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_board != null);

    _board.setName(_oldName);
    
    // Undo SurfaceMap info
    if (_board.hasBoardSurfaceMapSettings())
    {
      BoardSurfaceMapSettings boardSurfaceMapSettings = _board.getBoardSurfaceMapSettings();
      if (boardSurfaceMapSettings.hasOpticalRegion())
      {
        for(OpticalRegion opticalRegion : boardSurfaceMapSettings.getOpticalRegions())
        {
          boardSurfaceMapSettings.updateSurfaceMapRegion(opticalRegion, _newName, _oldName);
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_BOARD_SET_NAME_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}

