package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.datastore.config.*;
/**
 * This class is used so that if the xrayAutoShortTermShutdownEnabled in software.config is changed,
 * it can be reset via an undo command.
 * @author AnthonyFong
 */
//CR33099-Auto Short Term Shutdown for Xray-AnthonyFong 19-March-2009
public class ConfigSetXrayAutoShortTermShutdownEnableCommand extends DataCommand
{
  private boolean _originalShutdownEnabled;
  private boolean _newShutdownEnabled;
  private LegacyXraySource _xraySource = null;
  private HTubeXraySource _hTubeXraySource = null;
  private Config _config = Config.getInstance();

  /**
   * @author AnthonyFong
   */
  public ConfigSetXrayAutoShortTermShutdownEnableCommand(boolean newShutdownEnabled)
  {
    _newShutdownEnabled = newShutdownEnabled;

    if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
    {
        AbstractXraySource xraySource = AbstractXraySource.getInstance();
        Assert.expect(xraySource instanceof LegacyXraySource);
        _xraySource = (LegacyXraySource)xraySource;

        _originalShutdownEnabled = _xraySource.getUsesXrayAutoShortTermShutdownEnabled();
    }
    else if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
    {
        AbstractXraySource xraySource = AbstractXraySource.getInstance();
        Assert.expect(xraySource instanceof HTubeXraySource);
        _hTubeXraySource = (HTubeXraySource)xraySource;

        _originalShutdownEnabled = _hTubeXraySource.getUsesXrayAutoShortTermShutdownEnabled();
    }
    else
    {
         _originalShutdownEnabled = false;
    }
  }

  /**
   * @author AnthonyFong
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalShutdownEnabled == _newShutdownEnabled)
      return false;

    if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
    {
        _xraySource.setUsesXrayAutoShortTermShutdownEnabled(_newShutdownEnabled);
    }
    else if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
    {
        _hTubeXraySource.setUsesXrayAutoShortTermShutdownEnabled(_newShutdownEnabled);
    }
    return true;
  }

  /**
   * @author AnthonyFong
   */
  public void undoCommand() throws XrayTesterException
  {
    if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
    {
        _xraySource.setUsesXrayAutoShortTermShutdownEnabled(_originalShutdownEnabled);
    }
    else if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
    {
        _hTubeXraySource.setUsesXrayAutoShortTermShutdownEnabled(_originalShutdownEnabled);
    }
  }

  /**
   * @author AnthonyFong
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_XRAY_AUTO_SHUTDOWN_ENABLED_KEY");
  }
}
