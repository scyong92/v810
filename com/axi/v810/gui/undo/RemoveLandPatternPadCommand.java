package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 * Command to destroy a land pattern
 * @author Laura Cormos
 */
public class RemoveLandPatternPadCommand extends DataCommand
{
  private java.util.List<LandPatternPad> _landPatternPadToRemove = new ArrayList<LandPatternPad> ();

  /**
   * @author Laura Cormos
   */
  public RemoveLandPatternPadCommand(java.util.List<LandPatternPad> destroyedLandPatternPad)
  {
    Assert.expect(destroyedLandPatternPad != null);

    _landPatternPadToRemove = destroyedLandPatternPad;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_landPatternPadToRemove != null);

    for (LandPatternPad landPattern: _landPatternPadToRemove)
    {
      landPattern.remove();
    }
    
    return true;
  }

  /**
   * @author Laura Cormos
   * @author Ying-Huan.Chu
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_landPatternPadToRemove != null);
    try
    {
      ProjectObservable.getInstance().setEnabled(false);
      for (LandPatternPad landPattern : _landPatternPadToRemove)
      {
        if (landPattern.isPadOne())
        {
          landPattern.getLandPattern().getLandPatternPadOne().setPadOne(false);
          landPattern.setPadOne(true);
          landPattern.getLandPattern().setUserAssignedPadOneForReaders(landPattern);
        }

        landPattern.add();
      }
    }
    finally
    {
      ProjectObservable.getInstance().setEnabled(true);          
      ProjectObservable.getInstance().stateChanged(this, null, _landPatternPadToRemove, LandPatternPadEventEnum.ADD_OR_REMOVE_LIST);
    }
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_DESTROY_LANDPATTERN_PAD_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
