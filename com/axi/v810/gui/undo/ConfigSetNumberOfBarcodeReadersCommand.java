package com.axi.v810.gui.undo;

import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class sets/unsets the number of barcode scanners for the barcode reader configuration
 * @author Andy Mechtenberg
 */
public class ConfigSetNumberOfBarcodeReadersCommand extends DataCommand
{
  private int _originalNumber;
  private int _newNumber;
  private BarcodeReaderManager _bcrManager = BarcodeReaderManager.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetNumberOfBarcodeReadersCommand(int number)
  {
    _newNumber = number;
    _originalNumber = _bcrManager.getBarcodeReaderNumberOfScanners();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_newNumber == _originalNumber)
      return false;

    _bcrManager.setBarcodeReaderNumberOfScanners(_newNumber);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _bcrManager.setBarcodeReaderNumberOfScanners(_originalNumber);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_NUMBER_BARCODE_SCANNERS_KEY");
  }
}
