package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the mail server in config is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ConfigSetProjectDatabaseDirectoryCommand extends DataCommand
{
  private String _originalProjectDatabaseDir;
  private String _newProjectDatabaseDir;

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetProjectDatabaseDirectoryCommand(String newProjectDatabaseDir)
  {
    Assert.expect(newProjectDatabaseDir != null);

    _newProjectDatabaseDir = newProjectDatabaseDir;
    _originalProjectDatabaseDir = Directory.getProjectDatabaseDir();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalProjectDatabaseDir.equalsIgnoreCase(_newProjectDatabaseDir))
      return false;

    Directory.setProjectDatabaseDir(_newProjectDatabaseDir);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Directory.setProjectDatabaseDir(_originalProjectDatabaseDir);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_PROJECT_DATABASE_DIRECTORY_KEY");
  }
}
