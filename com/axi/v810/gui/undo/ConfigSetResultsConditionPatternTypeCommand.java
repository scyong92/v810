package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.gui.config.*;
import com.axi.v810.util.*;

/**
 * Command for setting the serial number pattern type for pattern matching
 * @author Laura Cormos
 */
public class ConfigSetResultsConditionPatternTypeCommand extends DataCommand
{
  private ResultsTableModelCondition _condition = null;
  private boolean _isSimplePattern;
  private boolean _originalIsSimplePattern;

  /**
   * @author Laura Cormos
   */
  public ConfigSetResultsConditionPatternTypeCommand(ResultsTableModelCondition condition,
                                                     boolean isSimplePattern)
  {
    Assert.expect(condition != null);

    _condition = condition;
    _isSimplePattern = isSimplePattern;
    _originalIsSimplePattern = _condition.getIsSimplePattern();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_condition != null);

    _condition.setIsSimplePattern(_isSimplePattern);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_condition != null);

    _condition.setIsSimplePattern(_originalIsSimplePattern);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_RESPROC_SET_CONDITION_PATTERN_TYPE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
