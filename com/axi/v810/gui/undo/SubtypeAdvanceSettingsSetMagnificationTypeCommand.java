package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;


/**
 *
 * @author chin-seong.kee
 */
public class SubtypeAdvanceSettingsSetMagnificationTypeCommand extends DataCommand
{
  private SubtypeAdvanceSettings _subtypeAdvanceSettings = null;
  private MagnificationTypeEnum _origMagnificationType = null;
  private MagnificationTypeEnum _newMagnificationType = null;

  /**
   * Construct the class.
   * @author Chin Seong
   */
  public SubtypeAdvanceSettingsSetMagnificationTypeCommand(SubtypeAdvanceSettings subtypeAdvanceSettings, MagnificationTypeEnum newState)
  {
    Assert.expect(subtypeAdvanceSettings != null);
    Assert.expect(newState != null);

    _subtypeAdvanceSettings = subtypeAdvanceSettings;
    _newMagnificationType = newState;

    _origMagnificationType = subtypeAdvanceSettings.getMagnificationType();
  }

  /**
   * @author Chin Seong
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_subtypeAdvanceSettings != null);

    if (_newMagnificationType.equals(_origMagnificationType))
      return false;

    _subtypeAdvanceSettings.setMagnificationType(_newMagnificationType);
    return true;
  }

  /**
   * @author Chin Seong
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_subtypeAdvanceSettings != null);

    _subtypeAdvanceSettings.setMagnificationType(_origMagnificationType);
  }

  /**
   * @author Chin Seong
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_ADVANCE_SETTINGS_SET_MAGNIFICATION_TYPE_KEY");
  }
}
