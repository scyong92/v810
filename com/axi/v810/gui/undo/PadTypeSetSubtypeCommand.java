package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.ProjectHistoryLog;
import com.axi.v810.util.*;

/**
 * This class is used to set the subtype (CustomAlgorithmFamily) for a padType
 * Using this class allows us to undo the set later.
 * @author Andy Mechtenberg
 */
public class PadTypeSetSubtypeCommand extends DataCommand
{
  private PadType _padType = null;
  private Subtype _origSubtype = null;
  private Subtype _newSubtype = null;
  private static transient ProjectHistoryLog _fileWriter;

  /**
   * @param padType the pad type whose subtype will change
   * @param newCustomAlgorithmFamily the new subtype to change to
   * @author Andy Mechtenberg
   */
  public PadTypeSetSubtypeCommand(PadType padType, Subtype newCustomAlgorithmFamily)
  {
    Assert.expect(padType != null);
    Assert.expect(newCustomAlgorithmFamily != null);

    _padType = padType;
    _newSubtype = newCustomAlgorithmFamily;
    _origSubtype = _padType.getPadTypeSettings().getSubtype();
    _fileWriter = ProjectHistoryLog.getInstance();
  }

  /**
   * Change the custom algorithm family (subtype) for a pad type
   * @return true if the command executed, false if new algorithm family is the same as the current one
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_padType != null);

    if (_newSubtype.equals(_origSubtype))
      return false;

    _padType.getPadTypeSettings().setSubtype(_newSubtype);
    return true;
  }

  /**
   * Reset the Custom Algorithm Family (subtype)
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_padType != null);

    _padType.getPadTypeSettings().setSubtype(_origSubtype);
    _fileWriter.appendUndoPadType(_padType,_padType.getJointTypeEnum(),_newSubtype,_origSubtype);//keep track subtype undo value -hsia-fen.tan
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PAD_TYPE_SET_SUBTYPE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
