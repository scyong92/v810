package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * Command for setting the production tune image destination property
 * @author Andy Mechtenberg
 */
public class ConfigSetProductionTuneImageDestinationCommand extends DataCommand
{
  private ProductionTuneData _productionTuneData = null;
  private String _newImageDest = null;
  private String _oldImageDest = null;

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetProductionTuneImageDestinationCommand(ProductionTuneData productionTuneData,
                                                        String imageDest)
  {
    Assert.expect(productionTuneData != null);
    Assert.expect(imageDest != null);

    _productionTuneData = productionTuneData;
    _newImageDest = imageDest;
    _oldImageDest = _productionTuneData.getImageDestination();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);

    _productionTuneData.setImageDestination(_newImageDest);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);

    _productionTuneData.setImageDestination(_oldImageDest);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_PRODUCTION_TUNE_SET_IMAGE_DEST_KEY");
  }
}
