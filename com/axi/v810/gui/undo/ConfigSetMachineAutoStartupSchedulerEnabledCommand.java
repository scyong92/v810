package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.datastore.config.*;

/**
 *
 * @author chin-seong.kee
 */
public class ConfigSetMachineAutoStartupSchedulerEnabledCommand extends DataCommand
{
  private boolean _originalMachineStartupEnabled, _newMachineStartupEnabled;
  private Config _config = Config.getInstance();

  public ConfigSetMachineAutoStartupSchedulerEnabledCommand(boolean isEnabled)
  {
    _newMachineStartupEnabled = isEnabled;
    
    _originalMachineStartupEnabled = _config.getBooleanValue(SoftwareConfigEnum.V810_AUTO_START_UP_ENABLED);
  }

  public boolean executeCommand() throws XrayTesterException
  {
    _config.setValue(SoftwareConfigEnum.V810_AUTO_START_UP_ENABLED, _newMachineStartupEnabled);
    
    return true;
  }

  public void undoCommand() throws XrayTesterException
  {
    _config.setValue(SoftwareConfigEnum.V810_AUTO_START_UP_ENABLED, _originalMachineStartupEnabled);
  }
  
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_MACHINE_AUTO_STARTUP_ENABLE_KEY");
  }
  
}
