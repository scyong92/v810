package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used to set the subtype (CustomAlgorithmFamily) for a ComponentType
 * Using this class allows us to undo the set later.
 * @author Andy Mechtenberg
 */
public class ComponentTypeSetSubtypeCommand extends DataCommand
{
  private ComponentType _componentType = null;
  private Subtype _origSubtype = null;
  private Subtype _newSubtype = null;
  private boolean _isSettingsNeedToCarryForward = false;

  /**
   * Construct the class.
   * @author Andy Mechtenberg
   */
  public ComponentTypeSetSubtypeCommand(ComponentType componentType, Subtype newSubtype)
  {
    Assert.expect(componentType != null);
    Assert.expect(newSubtype != null);

    _componentType = componentType;
    _newSubtype = newSubtype;
    _origSubtype = _componentType.getComponentTypeSettings().getSubtype();
  }
  
  public ComponentTypeSetSubtypeCommand(ComponentType componentType, Subtype newSubtype, boolean isSettingsNeedToCarryForward)
  {
    Assert.expect(componentType != null);
    Assert.expect(newSubtype != null);

    _componentType = componentType;
    _newSubtype = newSubtype;
    _origSubtype = _componentType.getComponentTypeSettings().getSubtype();
    _isSettingsNeedToCarryForward = isSettingsNeedToCarryForward;
  }

  /**
   * Change the custom algorithm family (subtype) for a component type
   * @return true if the command executed, false if new algorithm family is the same as the current one
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   * @author Kee Chin Seong
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    if (_newSubtype.equals(_origSubtype))
      return false;
    
    if(_isSettingsNeedToCarryForward == true)
    {
      //To copy the settings to new subtype
      if(_origSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false)
        _newSubtype.getSubtypeAdvanceSettings().setArtifactCompensationState(_origSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState());

      _newSubtype.getSubtypeAdvanceSettings().setCanUseVariableMagnification(_origSubtype.getSubtypeAdvanceSettings().canUseVariableMagnification());
      _newSubtype.getSubtypeAdvanceSettings().setIsInterferenceCompensatable(_origSubtype.getSubtypeAdvanceSettings().isIsInterferenceCompensatable());
      _newSubtype.getSubtypeAdvanceSettings().setMagnificationType(_origSubtype.getSubtypeAdvanceSettings().getMagnificationType());
      _newSubtype.getSubtypeAdvanceSettings().setSignalCompensation(_origSubtype.getSubtypeAdvanceSettings().getSignalCompensation());
      _newSubtype.getSubtypeAdvanceSettings().setUserGain(_origSubtype.getSubtypeAdvanceSettings().getUserGain());
      _newSubtype.getSubtypeAdvanceSettings().setStageSpeedEnum(_origSubtype.getSubtypeAdvanceSettings().getStageSpeedList());
    }
    
    _componentType.getComponentTypeSettings().setSubtype(_newSubtype);

    return true;
  }

  /**
   * Reset the Custom Algorithm Family (subtype)
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    _componentType.getComponentTypeSettings().setSubtype(_origSubtype);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    // do a KEY lookup to get the proper localized string here
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_COMPONENT_TYPE_SET_SUBTYPE_KEY", new Object[]{_componentType.getReferenceDesignator()});
    return StringLocalizer.keyToString(localizedString);
  }
}
