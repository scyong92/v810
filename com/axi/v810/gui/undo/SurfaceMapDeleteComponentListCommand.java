/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.util.*;

/**
 *
 * @author Jack Hwee
 */
public class SurfaceMapDeleteComponentListCommand extends DataCommand
{
  private List<Component> _componentsToRemove  = new ArrayList<Component>();
  private List<Component> _oldComponents  = new ArrayList<Component>();
  private OpticalRegion _opticalRegion;
  private boolean _isPanelBasedAlignment;
  
  private java.util.List<Board> _previousBoardSelection = null;
    
  /**
   * @param newCoord the new coordinate
   * @author Laura Cormos
   */
  public SurfaceMapDeleteComponentListCommand(OpticalRegion opticalRegion,  List<Component> componentsToRemove, boolean isPanelBasedAlignment)
  {
    Assert.expect(opticalRegion != null);
    Assert.expect(componentsToRemove != null);

    _opticalRegion = opticalRegion;
    _componentsToRemove = componentsToRemove;
    _oldComponents = componentsToRemove;
    _isPanelBasedAlignment = isPanelBasedAlignment;
    
    _previousBoardSelection = PspAutoPopulatePanel.getInstance().getBoardsToApplyOpticalCameraRectangles();
  }

  /**
   * Change the coordinate to the new coordinate
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_opticalRegion != null);  
    
    for (Component component : _componentsToRemove)
    {
      if (_isPanelBasedAlignment)
      {
        if (_opticalRegion.hasComponent(component))
        {
          _opticalRegion.removeComponent(component);
        }
        Board board = component.getBoard();
        if (board.getPanel().getPanelSurfaceMapSettings().hasOpticalRegion())
        {
          board.getPanel().getPanelSurfaceMapSettings().getOpticalRegion().removeComponent(component);
        }
      }
      else
      {
        List<Board> boards = _opticalRegion.getBoards();
        for(Board board : boards)
        {
          if (_opticalRegion.hasComponent(component, board))
          {
            _opticalRegion.removeComponent(component, board);
          }
          if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
          {
            board.getBoardSurfaceMapSettings().getOpticalRegion().removeComponent(component, board);
          }
        }
      }
    }

    return true;
  }

  /**
   * Restore the old coordinate
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_opticalRegion != null);
    
    for (Component component : _oldComponents)
    {
      if (_isPanelBasedAlignment)
      {
        _opticalRegion.addComponent(component);
      }
      else
      {
        List<Board> boards = _opticalRegion.getBoards();
        for(Board board : boards)
        {
          _opticalRegion.addComponent(board, component);
          if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
          {
            board.getBoardSurfaceMapSettings().getOpticalRegion().addComponent(board, component);
          }
        }
        PspAutoPopulatePanel.getInstance().setBoardsToApplyOpticalCameraRectangle(_previousBoardSelection);
      }
    }
  
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_DELETE_COMPONENT_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
