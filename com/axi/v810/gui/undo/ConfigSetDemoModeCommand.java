package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ConfigSetDemoModeCommand extends DataCommand
{
  private boolean _originalDemoMode;
  private boolean _newDemoMode;
  private TestExecution _testExecution = TestExecution.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetDemoModeCommand(boolean newDemoMode)
  {
    _newDemoMode = newDemoMode;
    _originalDemoMode = _testExecution.isDemoModeEnabled();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalDemoMode == _newDemoMode)
      return false;

    _testExecution.setDemoMode(_newDemoMode);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _testExecution.setDemoMode(_originalDemoMode);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_DEMO_MODE_KEY");
  }
}
