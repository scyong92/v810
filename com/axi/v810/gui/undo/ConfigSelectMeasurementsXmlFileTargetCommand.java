package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.gui.config.*;
import com.axi.v810.util.*;

/**
 * Command for setting the measurements XML file checkbox
 * @author huai-en.janan-ezekie
 */
public class ConfigSelectMeasurementsXmlFileTargetCommand extends DataCommand
{

  private boolean _enabledState;
  private ResultsProcessingResultsTabPanel _parent;

  public ConfigSelectMeasurementsXmlFileTargetCommand(boolean enabledState, ResultsProcessingResultsTabPanel parent)
  {
    Assert.expect(parent != null);
    
    _parent = parent;    
    _enabledState = enabledState;    
  }

  /**
   * @author Janan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    _parent.setMeasurementsXMLFileCheckboxState(_enabledState);
    return true;
  }

  /**
   * @author Janan
   */
  public void undoCommand() throws XrayTesterException
  {
    _parent.setMeasurementsXMLFileCheckboxState(!_enabledState);
  }

  /**
   * @author Janan
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_RESPROC_ENABLE_MEASUREMENTS_XML_FILE_TARGET_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }

}
