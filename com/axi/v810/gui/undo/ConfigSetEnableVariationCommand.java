package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author kok-chun.tan
 */
public class ConfigSetEnableVariationCommand extends DataCommand
{
  private Project _project;
  private boolean _newEnabledState;
  private boolean _oldEnabledState;
  private VariationSetting _newVariationSetting = null;
  private VariationSetting _oldVariationSetting = null;
  private Map<String, java.util.List<String>> _emptyBoardTypeToRefDesMap = new LinkedHashMap<>();

  /**
   * @author Kok Chun, Tan
   */
  public ConfigSetEnableVariationCommand(Project project,
                                            boolean newEnabledState,
                                            VariationSetting newVariationSetting,
                                            boolean oldEnabledState,
                                            VariationSetting oldVariationSetting)
  {
    Assert.expect(project != null);

    _project = project;
    _newEnabledState = newEnabledState;
    _oldEnabledState = oldEnabledState;
    _newVariationSetting = newVariationSetting;
    _oldVariationSetting = oldVariationSetting;
  }

  /**
   * @author Kok Chun, Tan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    //no variation enabled ----> enable one variation
    if (_oldVariationSetting == null)
    {
      _newVariationSetting.setEnable(true);
      _newVariationSetting.setIsViewableInProduction(true);
      VariationSettingManager.getInstance().setupVariationSetting(true, _project, _newVariationSetting.getBoardTypeToRefDesMap());
    }
    //enable one variation ----> no variation enabled
    else if (_newVariationSetting.equals(_oldVariationSetting))
    {
      _newVariationSetting.setEnable(false);
      VariationSettingManager.getInstance().setupVariationSetting(false, _project, _emptyBoardTypeToRefDesMap);
    }
    //enable one variation ----> enable another variation
    else
    {
      _newVariationSetting.setEnable(_newEnabledState);
      _oldVariationSetting.setEnable(!_oldEnabledState);
      _newVariationSetting.setIsViewableInProduction(_newEnabledState);
      VariationSettingManager.getInstance().setupVariationSetting(_newEnabledState, _project, _newVariationSetting.getBoardTypeToRefDesMap());
    }
    
    //set selected variation and enabled variation to same name, so that it can be save.
    if (_newEnabledState)
    {
      _project.setEnabledVariationName(_newVariationSetting.getName());
      _project.setSelectedVariationName(_newVariationSetting.getName());
    }
    else
    {
      _project.resetEnabledVariationName();
      _project.resetSelectedVariationName();
    }
    
    return true;
  }

  /**
   * @author Kok Chun, Tan
   */
  public void undoCommand() throws XrayTesterException
  {
    //no variation enabled <---- enable one variation
    if (_oldVariationSetting == null)
    {
      _newVariationSetting.setEnable(false);
      VariationSettingManager.getInstance().setupVariationSetting(false, _project, _emptyBoardTypeToRefDesMap);
    }
    //enable one variation <---- no variation enabled
    else if (_newVariationSetting.equals(_oldVariationSetting))
    {
      _oldVariationSetting.setEnable(true);
      VariationSettingManager.getInstance().setupVariationSetting(true, _project, _oldVariationSetting.getBoardTypeToRefDesMap());
    }
    //enable one variation <---- enable another variation
    else
    {
      _newVariationSetting.setEnable(!_newEnabledState);
      _oldVariationSetting.setEnable(_oldEnabledState);
      VariationSettingManager.getInstance().setupVariationSetting(_oldEnabledState, _project, _oldVariationSetting.getBoardTypeToRefDesMap());
    }
    
    //set selected variation and enabled variation to same name, so that it can be save.
    if (_oldEnabledState)
    {
      _project.setEnabledVariationName(_oldVariationSetting.getName());
      _project.setSelectedVariationName(_oldVariationSetting.getName());
    }
    else
    {
      _project.resetEnabledVariationName();
      _project.resetSelectedVariationName();
    }
  }

  /**
   * @author Kok Chun, Tan
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SN_SET_ENABLE_VARIATION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
