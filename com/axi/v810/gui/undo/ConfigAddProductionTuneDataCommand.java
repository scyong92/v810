package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * Command for adding a new productuion tune setting
 * @author Andy Mechtenberg
 */
public class ConfigAddProductionTuneDataCommand extends DataCommand
{
  private ProductionTuneData _productionTuneData = null;
  private ProductionTuneManager _productionTuneManager = ProductionTuneManager.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigAddProductionTuneDataCommand(ProductionTuneData productionTuneData)
  {
    Assert.expect(productionTuneData != null);

    _productionTuneData = productionTuneData;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);
    _productionTuneManager.add(_productionTuneData);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);
    _productionTuneManager.remove(_productionTuneData);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_PRODUCTION_TUNE_ADD_PROJECT_KEY");
  }
}
