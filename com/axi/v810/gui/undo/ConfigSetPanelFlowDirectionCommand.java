package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ConfigSetPanelFlowDirectionCommand extends DataCommand
{
  private PanelHandlerPanelFlowDirectionEnum _originalFlowDirection;
  private PanelHandlerPanelFlowDirectionEnum _newFlowDirection;
  private PanelHandler _panelHandler = PanelHandler.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetPanelFlowDirectionCommand(PanelHandlerPanelFlowDirectionEnum newFlowDirection)
  {
    Assert.expect(newFlowDirection != null);

    _newFlowDirection = newFlowDirection;
    _originalFlowDirection = _panelHandler.getPanelFlowDirection();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalFlowDirection.equals(_newFlowDirection))
      return false;
    HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _panelHandler.setPanelFlowDirection(_newFlowDirection);
      }
    });

    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _panelHandler.setPanelFlowDirection(_originalFlowDirection);
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_PANEL_FLOW_MODE_KEY");
  }
}
