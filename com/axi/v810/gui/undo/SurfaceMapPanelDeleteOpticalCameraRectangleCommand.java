package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used to delete OpticalCameraRectangle from OpticalRegion
 * Using this class allows us to undo the set later.
 * @author Ying-Huan.Chu
 */
public class SurfaceMapPanelDeleteOpticalCameraRectangleCommand extends DataCommand
{
  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  private OpticalRegion _opticalRegion = null;
  private OpticalCameraRectangle _opticalCameraRectangleToBeDeleted = null;

  /**
   * @param opticalRegion the OpticalRegion to have one of its OpticalCameraRectangles removed.
   * @param opticalCameraRectangle the OpticalCameraRectangle to be deleted
   * @author Ying-Huan.Chu
   */
  public SurfaceMapPanelDeleteOpticalCameraRectangleCommand(OpticalRegion opticalRegion, OpticalCameraRectangle opticalCameraRectangle)
  {
    Assert.expect(opticalRegion != null);
    Assert.expect(opticalCameraRectangle != null);
    
    _opticalRegion = opticalRegion;
    _opticalCameraRectangleToBeDeleted = opticalCameraRectangle;
  }

  /**
   * Delete the OpticalCameraRectangle from the OpticalRegion
   * @return true if the command executed
   * @throws XrayTesterException if the command fails
   * @author Ying-Huan.Chu
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_opticalRegion != null);
    Assert.expect(_opticalCameraRectangleToBeDeleted != null);
    
    _projectObservable.setEnabled(false);
    try
    {
      _opticalRegion.removeOpticalCameraRectangle(_opticalCameraRectangleToBeDeleted);
    }
    finally
    {
      _projectObservable.setEnabled(true);
      _projectObservable.stateChanged(this, _opticalCameraRectangleToBeDeleted, null, PanelSurfaceMapEventEnum.REMOVE_OPTICAL_CAMERA_RECTANGLE);
    }
    
    return true;
  }

  /**
   * Reset back to the original OpticalRegion
   * @throws XrayTesterException if the command fails
   * @author Ying-Huan.Chu
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_opticalRegion != null);
    Assert.expect(_opticalCameraRectangleToBeDeleted != null);
    
    _projectObservable.setEnabled(false);
    try
    {
      _opticalRegion.addOpticalCameraRectangle(_opticalCameraRectangleToBeDeleted);
    }
    finally
    {
      _projectObservable.setEnabled(true);
      _projectObservable.stateChanged(this, null, _opticalCameraRectangleToBeDeleted, PanelSurfaceMapEventEnum.ADD_OPTICAL_CAMERA_RECTANGLE);
    }
  }

  /**
   * @author Ying-Huan.Chu
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_DELETE_OPTICAL_CAMERA_RECTANGLE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
