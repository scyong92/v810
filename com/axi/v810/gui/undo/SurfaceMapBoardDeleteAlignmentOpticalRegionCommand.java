/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used to delete OpticalRegion from PanelSurfaceMapSettings
 * Using this class allows us to undo the set later.
 * @author Ying-Huan.Chu
 */
public class SurfaceMapBoardDeleteAlignmentOpticalRegionCommand extends DataCommand
{
  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  private BoardAlignmentSurfaceMapSettings _boardAlignmentSurfaceMapSettings = null;
  private OpticalRegion _alignmentOpticalRegion = null;
  private Project _project = null;
  private String _alignmentGroupName = null;
  
  /**
   * @param panelSurfaceMapSettings the PanelSurfaceMapSettings to have its OpticalRegion removed.
   * @param opticalRegion the opticalRegion to be deleted.
   * @author Ying-Huan.Chu
   */
  public SurfaceMapBoardDeleteAlignmentOpticalRegionCommand(Project project, 
                                                            BoardAlignmentSurfaceMapSettings boardAlignmentSurfaceMapSettings,
                                                            String alignmentGroupName,
                                                            OpticalRegion opticalRegion)
  {
    Assert.expect(project != null);
    Assert.expect(boardAlignmentSurfaceMapSettings != null);
    Assert.expect(opticalRegion != null);
    
    _boardAlignmentSurfaceMapSettings = boardAlignmentSurfaceMapSettings;
    _alignmentOpticalRegion = opticalRegion;
    _project = project;
    _alignmentGroupName = alignmentGroupName;
  }
  
  /**
   * Delete the OpticalCameraRectangle from the OpticalRegion
   * @return true if the command executed
   * @throws XrayTesterException if the command fails
   * @author Ying-Huan.Chu
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_boardAlignmentSurfaceMapSettings != null);
    Assert.expect(_alignmentOpticalRegion != null);
    Assert.expect(_alignmentGroupName != null);
    
    _projectObservable.setEnabled(false);
    try
    {
      _project.getTestProgram().removeOpticalRegion(_alignmentOpticalRegion);
      _boardAlignmentSurfaceMapSettings.removeOpticalRegion(_alignmentGroupName, _alignmentOpticalRegion);
    }
    finally
    {
      _projectObservable.setEnabled(true);
      _projectObservable.stateChanged(this, _alignmentOpticalRegion, null, BoardSurfaceMapEventEnum.REMOVE_ALIGNMENT_SURFACE_MAP_REGION);
    }
    
    return true;
  }

  /**
   * Reset back to the original OpticalRegion
   * @throws XrayTesterException if the command fails
   * @author Ying-Huan.Chu
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_boardAlignmentSurfaceMapSettings != null);
    Assert.expect(_alignmentOpticalRegion != null);
    
    _projectObservable.setEnabled(false);
    try
    {
      _project.getTestProgram().addOpticalRegion(_alignmentOpticalRegion);
      _boardAlignmentSurfaceMapSettings.addOpticalRegion(_alignmentGroupName, _alignmentOpticalRegion);
    }
    finally
    {
      _projectObservable.setEnabled(true);
      _projectObservable.stateChanged(this, null, _alignmentOpticalRegion, BoardSurfaceMapEventEnum.ADD_ALIGNMENT_SURFACE_MAP_REGION);
    }
  }

  /**
   * @author Ying-Huan.Chu
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PSP_DELETE_ALIGNMENT_OPTICAL_REGION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
