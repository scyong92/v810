package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;
import com.axi.v810.hardware.SystemTypeEnum;

/**
 * This class is used so that if the system type of the project is changed,
 * it can be reset via an undo command.
 * @author Poh Kheng
 */
public class ProjectSetSystemTypeCommand extends DataCommand
{
  private Project _project;
  private SystemTypeEnum _originalSystemTypeEnum;
  private SystemTypeEnum _newSystemTypeEnum;

  /**
   * @param project the project whose system type will change
   * @param newSystemTypeEnum the new system type for the project
   * @author Poh Kheng
   */
  public ProjectSetSystemTypeCommand(Project project, SystemTypeEnum newSystemTypeEnum)
  {
    Assert.expect(project != null);
    Assert.expect(newSystemTypeEnum != null);

    _project = project;
    _newSystemTypeEnum = newSystemTypeEnum;
    _originalSystemTypeEnum = _project.getSystemType();
  }

  /**
   * Set the system type for the project
   * @author Poh Kheng
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_originalSystemTypeEnum.equals(_newSystemTypeEnum))
      return false;

    _project.setSystemType(_newSystemTypeEnum);
    return true;
  }

  /**
   * Reset the system type for the project
   * @author Poh Kheng
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setSystemType(_originalSystemTypeEnum);
  }

  /**
   * @author Poh Kheng
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("HP_SYSTEM_TYPE_LABEL_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
