package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used to set the artifact compensation state for a pad Type
 * Using this class allows us to undo the set later.
 * @author Laura Cormos
 */
public class PadTypeSetArtifactCompesationCommand extends DataCommand
{
  private PadType _padType = null;
  private ArtifactCompensationStateEnum _origArtificateCompensationState = null;
  private ArtifactCompensationStateEnum _newArtifacteCompensationState = null;
  private SignalCompensationEnum _origSignalCompensationEnum = null;
  private SignalCompensationEnum _newSignalCompensationEnum = null;

  /**
   * Construct the class.
   * @author Laura Cormos
   * @author Erica Wharton
   */
  public PadTypeSetArtifactCompesationCommand(PadType padType,
                                              ArtifactCompensationStateEnum newArtifactCompensationState,
                                              SignalCompensationEnum newSignalCompensationState)
  {
    Assert.expect(padType != null);
    Assert.expect(newArtifactCompensationState != null);
    Assert.expect(newSignalCompensationState != null);

    _padType = padType;
    _newArtifacteCompensationState = newArtifactCompensationState;
    _newSignalCompensationEnum = newSignalCompensationState;
    _origArtificateCompensationState = _padType.getPadTypeSettings().getEffectiveArtifactCompensationState();
    _origSignalCompensationEnum = _padType.getPadTypeSettings().getSignalCompensation();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_padType != null);

    if (_newArtifacteCompensationState.equals(_origArtificateCompensationState))
      return false;

    _padType.getPadTypeSettings().setArtifactCompensationState(_newArtifacteCompensationState);
    _padType.getPadTypeSettings().setSignalCompensation(_newSignalCompensationEnum);

    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_padType != null);

    _padType.getPadTypeSettings().setArtifactCompensationState(_origArtificateCompensationState);
    _padType.getPadTypeSettings().setSignalCompensation(_origSignalCompensationEnum);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_PAD_TYPE_SET_ARTIFACT_COMPENSATION_KEY");
  }
}
