package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author Kok Chun, Tan
 */
public class ProjectSetEnableProjectBasedBypassModeCommand extends DataCommand
{
  private Project _project = null;
  private boolean _newBypassMode = false;
  private boolean _oldBypassMode = false;

  /**
   * @author Kok Chun, Tan
   */
  public ProjectSetEnableProjectBasedBypassModeCommand(Project project, boolean isBypassMode)
  {
    Assert.expect(project != null);

    _project = project;
    _newBypassMode = isBypassMode;
    _oldBypassMode = _project.isProjectBasedBypassMode();
  }

  /**
   * Change the state of bypass mode
   * @author Kok Chun, Tan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_oldBypassMode == _newBypassMode)
      return false;

    _project.setProjectBasedBypassMode(_newBypassMode);
    return true;
  }

  /**
   * Restore the old state of enable bypass mode
   * @author Kok Chun, Tan
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setProjectBasedBypassMode(_oldBypassMode);
  }

  /**
   * @author Kok Chun, Tan
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_ENABLE_BYPASS_MODE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
