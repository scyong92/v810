package com.axi.v810.gui.undo;

import com.axi.v810.gui.mainMenu.*;

/**
 * Any Command that changes the state of the Datastore should extend this class.
 *
 * When a Command extending this class is finished, the commandFinished() method will
 * be called by the CommandManager class.  This classes commandFinshed() method does
 * not do anything because the ProjectObservable class will notify all
 * GuiObservable observers that the GUI has completely updated itself
 * based on this Command being executed.  This feedback is needed to properly implement
 * undo/redo functionality when the user continuously holds down the undo or redo keys.
 *
 * @see Command
 *
 * @author Bill Darbie
 */
public abstract class DataCommand extends Command
{
  private UndoKeystrokeHandler _undoKeystrokeHandler;

  /**
   * @author Bill Darbie
   */
  public void commandFinished()
  {
    if (_undoKeystrokeHandler == null)
    {
      _undoKeystrokeHandler = MainMenuGui.getInstance().getMainMenuBar().getUndoKeystrokeHandler();
    }
    _undoKeystrokeHandler.commandFinished();
  }
}
