package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting the joint type assignment rule's reference designator pattern
 * @author Laura Cormos
 */
public class ConfigSetJointTypeRuleRefDesPatternCommand extends DataCommand
{
  private JointTypeEnumRule _jointTypeEnumRule = null;
  private String _newRefDesPattern = null;
  private String _oldRefDesPattern = null;

  /**
   * @author Laura Cormos
   */
  public ConfigSetJointTypeRuleRefDesPatternCommand(JointTypeEnumRule jointTypeEnumRule , String refDesPattern)
  {
    Assert.expect(jointTypeEnumRule != null);
    Assert.expect(refDesPattern != null);

    _jointTypeEnumRule = jointTypeEnumRule;
    _newRefDesPattern = refDesPattern;
    if (_jointTypeEnumRule.isRefDesPatternSet())
      _oldRefDesPattern = _jointTypeEnumRule.getRefDesPattern();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    _jointTypeEnumRule.setRefDesPattern(_newRefDesPattern);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    if (_oldRefDesPattern == null)
      _jointTypeEnumRule.clearRefDesPattern();
    else
      _jointTypeEnumRule.setRefDesPattern(_oldRefDesPattern);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_JTA_SET_JOINT_TYPE_RULE_REFDES_PATTERN_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
