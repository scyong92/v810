package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 *
 * @author kok-chun.tan
 */
public class ConfigSetVariationNameCommand extends DataCommand
{
  private VariationSetting _variationSetting = null;
  private String _newVariationName = null;
  private String _oldVariationName = null;

  /**
   * @author Kok Chun, Tan
   */
  public ConfigSetVariationNameCommand(VariationSetting variationSetting, String variationName)
  {
    Assert.expect(variationSetting != null);
    Assert.expect(variationName != null);

    _variationSetting = variationSetting;
    _newVariationName = variationName;
    _oldVariationName = _variationSetting.getName();
  }

  /**
   * @author Kok Chun, Tan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_variationSetting != null);

    _variationSetting.setName(_newVariationName);
    return true;
  }

  /**
   * @author Kok Chun, Tan
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_variationSetting != null);

    _variationSetting.setName(_oldVariationName);
  }

  /**
   * @author Kok Chun, Tan
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SN_RENAME_VARIATION_NAME_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
