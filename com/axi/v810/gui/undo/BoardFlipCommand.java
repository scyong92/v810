package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * This class is used so if the board is flipped,
 * it can be unflipped via an undo function.
 * @author George A. David
 */
public class BoardFlipCommand extends DataCommand
{
  private Board _board;

  /**
   * @param board the board to be flipped
   * @author George A. David
   */
  public BoardFlipCommand(Board board)
  {
    Assert.expect(board != null);

    _board = board;
  }

  /**
   * Flip the board.
   * @author George A. David
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_board != null);

    _board.flip();

    return true;
  }

  /**
   * Unflip the board (by flipping it)
   * @author George A. David
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_board != null);

    _board.flip();
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_BOARD_FLIP_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
