package com.axi.v810.gui.undo;

import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This class handles the repair image type selection.
 * 
 * @author Cheah Lee Herng
 */
public class ConfigSetRepairImageTypeCommand extends DataCommand
{
  private int _originalImageType;
  private int _newImageType;
  private Config _config = Config.getInstance();
  
  /**
   * @author Wei Chin
   */
  public ConfigSetRepairImageTypeCommand(int newImageType)
  {
    _newImageType = newImageType;
    _originalImageType = _config.getIntValue(SoftwareConfigEnum.REPAIR_IMAGE_TYPE_FORMAT);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalImageType == _newImageType)
      return false;

    _config.setValue(SoftwareConfigEnum.REPAIR_IMAGE_TYPE_FORMAT, new Integer(_newImageType));
    
    return true;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void undoCommand() throws XrayTesterException
  {
    _config.setValue(SoftwareConfigEnum.REPAIR_IMAGE_TYPE_FORMAT, new Integer(_originalImageType));
  }

  /**
   * @author Cheah Lee Herng 
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_REPAIR_IMAGE_TYPE_KEY");
  }  
}
