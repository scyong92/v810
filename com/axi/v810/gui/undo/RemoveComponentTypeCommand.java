package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.ProjectObservable;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.MessageDialog;
import com.axi.v810.util.*;
import javax.swing.*;

/**
 * Command to destroy a component type
 * @author Laura Cormos
 * Command to destroy a list of component type
 * @author Seng Yew Lim
 */
public class RemoveComponentTypeCommand extends DataCommand
{
  //private ComponentType _componentTypeToRemove;
  //private List<PadType> _padTypesThatAreAffectedByInterferencePatternsFromCurrentComponent;
   private List<ComponentType> _componentTypeToRemoveList = new ArrayList<ComponentType>();

  /**
   * @author Laura Cormos
   */
  public RemoveComponentTypeCommand(ComponentType destroyedComponentType)
  {
    Assert.expect(destroyedComponentType != null);
    //_componentTypeToRemove = destroyedComponentType;
    Assert.expect(_componentTypeToRemoveList.add(destroyedComponentType));
  }

  /**
   * @author Seng Yew Lim
   * Added on 11-Jan-2013 so that this class can handle multiple ComponentType to remove
   */
  public RemoveComponentTypeCommand(List<ComponentType> destroyedComponentTypeList)
  {
    Assert.expect(destroyedComponentTypeList != null);
    _componentTypeToRemoveList.addAll(destroyedComponentTypeList);
  }
  
  protected void finalize()
  {
    _componentTypeToRemoveList.clear();
    _componentTypeToRemoveList = null;
  }

  /**
   * @author Laura Cormos
   * @author Yee Seong
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_componentTypeToRemoveList != null);

    boolean bSuccess = true;

    //Ngie Xing, increase deleting time for multiple components
    if (_componentTypeToRemoveList.size()>1)
      ProjectObservable.getInstance().setEnabled(false);

    for (ComponentType componentTypeToRemove : _componentTypeToRemoveList)
    {
      if (componentTypeToRemove == null)
      {
        bSuccess = false;
        break;
      }

      List<PadType> padTypes = componentTypeToRemove.getInterferenceAffectedPadTypes();

      for (PadType padType : padTypes)
      {
        padType.deleteComponentTypeThatCausesInterferencePatterns(componentTypeToRemove);
      }

      componentTypeToRemove.remove();
    }

    //Ngie Xing, increase deleting time for multiple components
    if (_componentTypeToRemoveList.size() > 1)
    {
      ProjectObservable.getInstance().setEnabled(true);
      ProjectObservable.getInstance().stateChanged(this, (Object) _componentTypeToRemoveList, null, ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST);
    }

    return bSuccess;
  }

  /**
   * @author Laura Cormos
   * @author Yee Seong
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_componentTypeToRemoveList != null);
    // Modified by Seng Yew on 11-Jan-2013
    // XCR1559 - Able to delete multiple components at once
    // Bypass all update when list contain more than 1
    if (_componentTypeToRemoveList.size()>1)
      ProjectObservable.getInstance().setEnabled(false);
    for (ComponentType componentTypeToRemove : _componentTypeToRemoveList)
    {
      Assert.expect(componentTypeToRemove != null);
      
      componentTypeToRemove.add();

      List<PadType> padTypes = componentTypeToRemove.getInterferenceAffectedPadTypes();

      for (PadType padType : padTypes)
      {
        padType.addComponentTypeThatCausesInteferencePattern(componentTypeToRemove);
      }
    }
    if (_componentTypeToRemoveList.size()>1)
    {
      ProjectObservable.getInstance().setEnabled(true);
      ProjectObservable.getInstance().stateChanged(this, null, (Object)_componentTypeToRemoveList, ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST);
    }
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString;
    if (_componentTypeToRemoveList == null || _componentTypeToRemoveList.size() <= 1)
      localizedString = new LocalizedString("GUI_COMMAND_DESTROY_COMPONENT_TYPE_KEY", null);
    else
      localizedString = new LocalizedString("GUI_COMMAND_DESTROY_SELECTED_COMPONENTS_TYPE_KEY", null);
        
    return StringLocalizer.keyToString(localizedString);
  }
}
