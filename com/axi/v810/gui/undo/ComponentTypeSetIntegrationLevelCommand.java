package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used to set the artifact compensation state for a ComponentType
 * Using this class allows us to undo the set later.
 * @author Laura Cormos
 */
public class ComponentTypeSetIntegrationLevelCommand extends DataCommand
{
  private ComponentType _componentType = null;
  private SignalCompensationEnum _origIntegrationLevel = null;
  private SignalCompensationEnum _newIntegrationLevel = null;

  /**
   * Construct the class.
   * @author Laura Cormos
   */
  public ComponentTypeSetIntegrationLevelCommand(ComponentType componentType, SignalCompensationEnum newState)
  {
    Assert.expect(componentType != null);
    Assert.expect(newState != null);

    _componentType = componentType;
    _newIntegrationLevel = newState;
    _origIntegrationLevel = _componentType.getSignalCompensation();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    if (_newIntegrationLevel.equals(_origIntegrationLevel))
      return false;
    List<PadType> padTypes = _componentType.getPadTypes();
    for (PadType padType : padTypes)
      padType.getPadTypeSettings().setSignalCompensation(_newIntegrationLevel);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    List<PadType> padTypes = _componentType.getPadTypes();
    for (PadType padType : padTypes)
      padType.getPadTypeSettings().setSignalCompensation(_origIntegrationLevel);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_INTEGRATION_LEVEL_KEY");
  }
}
