package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ConfigSetPanelSamplingModeCommand extends DataCommand
{
  private PanelSamplingModeEnum _originalPanelSamplingMode;
  private PanelSamplingModeEnum _newPanelSamplingMode;
  private Sampling _sampling = Sampling.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetPanelSamplingModeCommand(PanelSamplingModeEnum newPanelSamplingMode)
  {
    Assert.expect(newPanelSamplingMode != null);
    _newPanelSamplingMode = newPanelSamplingMode;
    _originalPanelSamplingMode = _sampling.getPanelSamplingModeEnum();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalPanelSamplingMode == _newPanelSamplingMode)
      return false;

    _sampling.setPanelSamplingModeEnum(_newPanelSamplingMode);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _sampling.setPanelSamplingModeEnum(_originalPanelSamplingMode);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_PANEL_SAMPLING_KEY");
  }
}
