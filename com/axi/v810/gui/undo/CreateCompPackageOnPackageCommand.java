package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.ProjectObservable;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Khaw Chek Hau
 * POP Development
 */
public class CreateCompPackageOnPackageCommand extends DataCommand
{
  private CompPackageOnPackage _compPackageOnPackage = null;
  private java.util.List<ComponentType> _componentTypesWithSamePOP =  new ArrayList<ComponentType>();
  private SideBoardType _sideBoardType;
  private String _refDes;
  private HashMap<ComponentType, POPLayerIdEnum> _componentTypeToPOPLayerIdEnum = new HashMap<ComponentType, POPLayerIdEnum>();
  private HashMap<ComponentType, Integer> _componentTypeToPOPZHeightInNanometers = new HashMap<ComponentType, Integer>();

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public CreateCompPackageOnPackageCommand(SideBoardType sideBoardType,
                                           String refDes,
                                           CompPackageOnPackage compPackageOnPackage, 
                                           List<ComponentType> componentTypesWithSamePOP,
                                           HashMap<ComponentType, POPLayerIdEnum> componentTypeToPOPLayerIdEnum,
                                           HashMap<ComponentType, Integer> componentTypeToPOPZHeightInNanometers)
  {
    Assert.expect(sideBoardType != null);        
    Assert.expect(refDes != null);   
    Assert.expect(compPackageOnPackage != null);
    Assert.expect(componentTypesWithSamePOP != null);
    Assert.expect(componentTypeToPOPLayerIdEnum != null);   
    Assert.expect(componentTypeToPOPZHeightInNanometers != null);   

    _sideBoardType = sideBoardType;
    _refDes = refDes;
    _compPackageOnPackage = compPackageOnPackage;
    _componentTypesWithSamePOP = componentTypesWithSamePOP;
    _componentTypeToPOPLayerIdEnum = componentTypeToPOPLayerIdEnum;
    _componentTypeToPOPZHeightInNanometers = componentTypeToPOPZHeightInNanometers;
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_sideBoardType != null);        
    Assert.expect(_refDes != null);   
    Assert.expect(_compPackageOnPackage != null);
    Assert.expect(_componentTypesWithSamePOP != null);
    Assert.expect(_componentTypeToPOPLayerIdEnum != null);   
    Assert.expect(_componentTypeToPOPZHeightInNanometers != null);   

    if (_compPackageOnPackage.getPanel().getCompPackageOnPackages().contains(_compPackageOnPackage) == false)
      _compPackageOnPackage.add();
    
    POPLayerIdEnum popLayerIdEnum;
    int popZHeightInNanometers;

    ProjectObservable.getInstance().setEnabled(false);
    try
    {
      for (ComponentType componentType : _componentTypesWithSamePOP)
      {
        popLayerIdEnum = _componentTypeToPOPLayerIdEnum.get(componentType);
        popZHeightInNanometers = _componentTypeToPOPZHeightInNanometers.get(componentType);

        if (componentType.getReferenceDesignator().equals(_refDes))
          componentType = _sideBoardType.getComponentType(_refDes);

        componentType.setCompPackageOnPackage(_compPackageOnPackage);
        componentType.setPOPLayerId(popLayerIdEnum);
        componentType.setPOPZHeightInNanometers(popZHeightInNanometers);
        _compPackageOnPackage.addComponentType(componentType);
      }
    }
    finally
    {
      ProjectObservable.getInstance().setEnabled(true);
    }
    ProjectObservable.getInstance().stateChanged(this, null, (Object)_componentTypesWithSamePOP, CompPackageOnPackageEventEnum.ADD_OR_REMOVE_COMPONENT_TYPE);
    
    return true;
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_compPackageOnPackage != null);

    _compPackageOnPackage.destroy();
    
    return;
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_CREATE_COMP_PACKAGE_ON_PACKAGE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
