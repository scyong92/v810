package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * Command for setting the production tune project name property
 * @author Andy Mechtenberg
 */
public class ConfigSetProductionTuneProjectNameCommand extends DataCommand
{
  private ProductionTuneData _productionTuneData = null;
  private String _newProjectName = null;
  private String _oldProjectName = null;

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetProductionTuneProjectNameCommand(ProductionTuneData productionTuneData,
                                                   String projectName)
  {
    Assert.expect(productionTuneData != null);
    Assert.expect(projectName != null);

    _productionTuneData = productionTuneData;
    _newProjectName = projectName;
    _oldProjectName = _productionTuneData.getProjectName();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);

    _productionTuneData.setProjectName(_newProjectName);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);

    _productionTuneData.setProjectName(_oldProjectName);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PRODUCTION_TUNE_SET_PROJECT_NAME_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
