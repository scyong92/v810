package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting a land pattern pad coordinate
 * @author Laura Cormos
 */
public class LandPatternPadSetCoordinateInNanoMetersCommand extends DataCommand
{
  private LandPatternPad _landPatternPad = null;
  private ComponentCoordinate _newCoordInNanoMeters = null;
  private ComponentCoordinate _oldCoordInNanoMeters = null;

  /**
   * @param landPatternPad the land pattern pad whose coordinate will change
   * @param newCoord the new coordinate
   * @author Laura Cormos
   */
  public LandPatternPadSetCoordinateInNanoMetersCommand(LandPatternPad landPatternPad , ComponentCoordinate newCoord)
  {
    Assert.expect(landPatternPad != null);
    Assert.expect(newCoord != null);

    _landPatternPad = landPatternPad;
    _newCoordInNanoMeters = newCoord;
    _oldCoordInNanoMeters = landPatternPad.getCoordinateInNanoMeters();
  }

  /**
   * Change the coordinate to the new coordinate
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_landPatternPad != null);
    Assert.expect(_oldCoordInNanoMeters != null);
    Assert.expect(_newCoordInNanoMeters != null);

    if (_oldCoordInNanoMeters.equals(_newCoordInNanoMeters))
      return false;

    if (_landPatternPad instanceof ThroughHoleLandPatternPad)
    {
      ComponentCoordinate origHoleCoord = ((ThroughHoleLandPatternPad)_landPatternPad).getHoleCoordinateInNanoMeters();
      int xDiff = _newCoordInNanoMeters.getX() - _oldCoordInNanoMeters.getX();
      int yDiff = _newCoordInNanoMeters.getY() - _oldCoordInNanoMeters.getY();
      ComponentCoordinate newHoleCoord = new ComponentCoordinate(origHoleCoord.getX() + xDiff, origHoleCoord.getY() + yDiff);
      ((ThroughHoleLandPatternPad)_landPatternPad).setHoleCoordinateInNanoMeters(newHoleCoord);
    }
    _landPatternPad.setCoordinateInNanoMeters(_newCoordInNanoMeters);
    return true;
  }

  /**
   * Restore the old coordinate
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_landPatternPad != null);
    Assert.expect(_oldCoordInNanoMeters != null);
    Assert.expect(_newCoordInNanoMeters != null);

    if (_landPatternPad instanceof ThroughHoleLandPatternPad)
    {
      ComponentCoordinate origHoleCoord = ((ThroughHoleLandPatternPad)_landPatternPad).getHoleCoordinateInNanoMeters();
      int xDiff = _oldCoordInNanoMeters.getX() - _newCoordInNanoMeters.getX();
      int yDiff = _oldCoordInNanoMeters.getY() - _newCoordInNanoMeters.getY();
      ComponentCoordinate newHoleCoord = new ComponentCoordinate(origHoleCoord.getX() + xDiff, origHoleCoord.getY() + yDiff);
      ((ThroughHoleLandPatternPad)_landPatternPad).setHoleCoordinateInNanoMeters(newHoleCoord);
    }
    _landPatternPad.setCoordinateInNanoMeters(_oldCoordInNanoMeters);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_LAND_PATTERN_PAD_SET_COORD_NM_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
