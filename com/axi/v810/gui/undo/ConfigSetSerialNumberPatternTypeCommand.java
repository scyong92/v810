package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * Command for setting the serial number pattern type for pattern matching
 * @author Laura Cormos
 */
public class ConfigSetSerialNumberPatternTypeCommand extends DataCommand
{
  private SerialNumberRegularExpressionData _serialNumberRegExData = null;
  private String _serialNumberPatternToMatch = null;
  private boolean _newIsRegEx;
  private boolean _oldIsRegEx;

  /**
   * @author Laura Cormos
   */
  public ConfigSetSerialNumberPatternTypeCommand(SerialNumberRegularExpressionData serialNumberRegExData,
                                                 String pattern,
                                                 boolean isRegEx)
  {
    Assert.expect(serialNumberRegExData != null);
    Assert.expect(pattern != null);

    _serialNumberRegExData = serialNumberRegExData;
    _serialNumberPatternToMatch = pattern;
    _newIsRegEx = isRegEx;
    _oldIsRegEx = _serialNumberRegExData.isInterpretedAsRegularExpression();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_serialNumberRegExData != null);

    _serialNumberRegExData.setOriginalExpressionAndInterpretion(_serialNumberPatternToMatch, _newIsRegEx);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_serialNumberRegExData != null);

      _serialNumberRegExData.setOriginalExpressionAndInterpretion(_serialNumberPatternToMatch, _oldIsRegEx);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SN_SET_PATTERN_TYPE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
