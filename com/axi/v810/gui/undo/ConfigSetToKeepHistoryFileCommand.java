/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.undo;

import com.axi.v810.business.panelSettings.HistoryLog;
import com.axi.v810.util.StringLocalizer;
import com.axi.v810.util.XrayTesterException;

/**
 * Project's history log CR
 * get number of history log file to keep
 * @author hsia-fen.tan
 */
public class ConfigSetToKeepHistoryFileCommand extends DataCommand
{
   private int _originalMeasurementRuns;
  private int _newMeasurementRuns;
  private HistoryLog _historyLog = HistoryLog.getInstance();

  /**
   * @author hsia-fen.tan
   */
  public ConfigSetToKeepHistoryFileCommand(int newMeasurementRuns)
  {
    _newMeasurementRuns = newMeasurementRuns;
    _originalMeasurementRuns = _historyLog.getMaximumNumberOfHistoryToKeep();
  }

  /**
   * @author hsia-fen.tan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalMeasurementRuns == _newMeasurementRuns)
      return false;

    _historyLog.setMaximumNumberOfHistoryFileToKeep(_newMeasurementRuns);
    return true;
  }

  /**
   * @author hsia-fen.tan
   */
  public void undoCommand() throws XrayTesterException
  {
    _historyLog.setMaximumNumberOfHistoryFileToKeep(_originalMeasurementRuns);
  }

  /**
   * @author hsia-fen.tan
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_NUMBER_HISTORY_FILE_KEY");
  }
}
