package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the company name in config is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ConfigSetCompanyNameCommand extends DataCommand
{
  private String _originalCompanyName;
  private String _newCompanyName;
  private CustomerInfo _customerInfo = CustomerInfo.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetCompanyNameCommand(String newCompanyName)
  {
    Assert.expect(newCompanyName != null);

    _newCompanyName = newCompanyName;
    _originalCompanyName = _customerInfo.getCompanyName();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalCompanyName.equalsIgnoreCase(_newCompanyName))
      return false;

    _customerInfo.setCompanyName(_newCompanyName);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _customerInfo.setCompanyName(_originalCompanyName);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_COMPANY_NAME_KEY");
  }
}
