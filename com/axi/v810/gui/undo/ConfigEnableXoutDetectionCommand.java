package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
public class ConfigEnableXoutDetectionCommand extends DataCommand
{
  private boolean _originalMode;
  private boolean _newMode;
  private TestExecution _testExecution = TestExecution.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigEnableXoutDetectionCommand(boolean newMode)
  {
    _newMode = newMode;
    _originalMode = _testExecution.isXoutDetectionModeEnabled();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalMode == _newMode)
      return false;

    _testExecution.setXoutDetectionMode(_newMode);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _testExecution.setXoutDetectionMode(_originalMode);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_XOUT_DETECTION_MODE_KEY");
  }
}
