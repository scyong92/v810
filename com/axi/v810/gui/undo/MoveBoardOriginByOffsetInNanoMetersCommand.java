package com.axi.v810.gui.undo;

import java.awt.geom.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for moving an entire board by an offset (used in VerifyCAD)
 * @author George Booth
 */

public class MoveBoardOriginByOffsetInNanoMetersCommand extends DataCommand
{

  private Board _board = null;
  private Point2D _offsetToApply = null;
  private Point2D _offsetToRestore = null;

  /**
   * @param board the board to move by the offset
   * @param offset the amount to move
   * @author George Booth
   */
  public MoveBoardOriginByOffsetInNanoMetersCommand(Board board, Point2D offset)
  {
    Assert.expect(board != null);
    Assert.expect(offset != null);

    _board = board;
    _offsetToApply = offset;
    // undo offset is negtive of offset
    _offsetToRestore = new Point2D.Double(-_offsetToApply.getX(), -_offsetToApply.getY());
  }

  /**
   * Offset the board origin
   * @author George Booth
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_board != null);

    int xOffset = (int)_offsetToApply.getX();
    int yOffset = (int)_offsetToApply.getY();
    if (xOffset == 0 && yOffset == 0)
      return false;

    _board.moveByOffSetInNanoMeters(xOffset, yOffset);

    return true;
  }

  /**
   * Restore the old location
   * @author George Booth
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_board != null);

    int xOffset = (int)_offsetToRestore.getX();
    int yOffset = (int)_offsetToRestore.getY();

    _board.moveByOffSetInNanoMeters(xOffset, yOffset);
  }

  /**
   * @author George Booth
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("CAD_MOVE_BOARD_ORIGIN_GUI_COMMAND_UNDO_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
