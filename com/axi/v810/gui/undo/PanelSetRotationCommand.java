package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * This class is used to change the panel rotation.
 * If it needs to be undone, use the undo() function
 * @author George A. David
 */
public class PanelSetRotationCommand extends DataCommand
{
  private PanelSettings _panelSettings;
  private int _originalRotation;
  private int _newRotation;

  /**
   * @param panel the panel whose rotation will change
   * @param degreesRotation the new rotation of the panel
   * @author George A. David
   */
  public PanelSetRotationCommand(Panel panel, int degreesRotation)
  {
    Assert.expect(panel != null);
    Assert.expect(degreesRotation >= 0);

    _panelSettings = panel.getPanelSettings();
    _newRotation = degreesRotation;
    _originalRotation = _panelSettings.getDegreesRotationRelativeToCad();
  }

  /**
   * Change the panel rotation.
   * @author George A. David
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_panelSettings != null);

    if (_originalRotation == _newRotation)
      return false;

    _panelSettings.setDegreesRotationRelativeToCad(_newRotation);
    return true;
  }

  /**
   * Reset the panel rotation
   * @author George A. David
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_panelSettings != null);

    _panelSettings.setDegreesRotationRelativeToCad(_originalRotation);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PANEL_SET_ROTATION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
