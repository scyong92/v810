/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.util.*;

/**
 *
 * @author siew-yeng.phang
 */
public class EditSerialNumberMappingBarcodeConfigNameCommand extends DataCommand
{
  private String _oldValue;
  private String _newValue;
  
  private SerialNumberMappingManager _serialNumberMappingManager = SerialNumberMappingManager.getInstance();
  
  /**
   * @author Phang Siew Yeng
   */
  public EditSerialNumberMappingBarcodeConfigNameCommand(String configName)
  {
    _newValue = configName;
    _oldValue = _serialNumberMappingManager.getBarcodeReaderConfigurationNameUsed();
  }
  
  public boolean executeCommand() throws XrayTesterException
  {
    if(_newValue.equals(_oldValue))
      return false;
    
    _serialNumberMappingManager.setSerialNumberMappingBarcodeConfigName(_newValue);
    return true;
  }

  public void undoCommand() throws XrayTesterException
  {
    _serialNumberMappingManager.setSerialNumberMappingBarcodeConfigName(_oldValue);
  }

  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("NOT SET", null);
    return StringLocalizer.keyToString(localizedString);
  }
  
}
