package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 *
 * @author Kok Chun, Tan
 */
public class ConfigSetSerialNumberVariationNameCommand extends DataCommand
{
  private SerialNumberRegularExpressionData _serialNumberRegExData = null;
  private String _newVariationName = null;
  private String _oldVariationName = null;

  /**
   * @author Kok Chun, Tan
   */
  public ConfigSetSerialNumberVariationNameCommand(SerialNumberRegularExpressionData serialNumberRegExData, String variationName)
  {
    Assert.expect(serialNumberRegExData != null);
    Assert.expect(variationName != null);

    _serialNumberRegExData = serialNumberRegExData;
    _newVariationName = variationName;
    _oldVariationName = _serialNumberRegExData.getVariationName();
  }

  /**
   * @author Kok Chun, Tan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_serialNumberRegExData != null);

    _serialNumberRegExData.setVariationName(_newVariationName);
    return true;
  }

  /**
   * @author Kok Chun, Tan
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_serialNumberRegExData != null);

    _serialNumberRegExData.setVariationName(_oldVariationName);
  }

  /**
   * @author Kok Chun, Tan
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SN_RENAME_VARIATION_NAME_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
