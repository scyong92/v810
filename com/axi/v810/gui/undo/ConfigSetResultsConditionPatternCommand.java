package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import com.axi.v810.gui.config.*;

/**
 * Command for setting the condition pattern
 * @author Laura Cormos
 */
public class ConfigSetResultsConditionPatternCommand extends DataCommand
{
  private ResultsTableModelCondition _condition = null;
  private String _newPattern = null;
  private String _oldPattern = null;

  /**
   * @author Laura Cormos
   */
  public ConfigSetResultsConditionPatternCommand(ResultsTableModelCondition serialNumberRegExData,
                                                 String pattern)
  {
    Assert.expect(serialNumberRegExData != null);
    Assert.expect(pattern != null);

    _condition = serialNumberRegExData;
    _newPattern = pattern;
    _oldPattern = _condition.getPattern();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_condition != null);

    _condition.setPattern(_newPattern);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_condition != null);

    _condition.setPattern(_oldPattern);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_RESPROC_SET_CONDITION_PATTERN_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
