package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.ProjectObservable;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Khaw Chek Hau
 * POP Development
 */
public class RemoveCompPackageOnPackageCommand extends DataCommand
{
   private CompPackageOnPackage _compPackageOnPackage;
   private HashMap<CompPackageOnPackage, List<ComponentType>> _compPackageOnPackageToComponentTypes = new HashMap<CompPackageOnPackage, List<ComponentType>>();
   private HashMap<ComponentType, POPLayerIdEnum> _componentTypeToPOPLayerIdEnum = new HashMap<ComponentType, POPLayerIdEnum>();
   private HashMap<ComponentType, Integer> _componentTypeToPOPZHeightInNanometers = new HashMap<ComponentType, Integer>();

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public RemoveCompPackageOnPackageCommand(CompPackageOnPackage compPackageOnPackage)
  {
    Assert.expect(compPackageOnPackage != null);
    _compPackageOnPackage = compPackageOnPackage;
    _compPackageOnPackageToComponentTypes.put(compPackageOnPackage, compPackageOnPackage.getComponentTypes());
    
    for (ComponentType componentType : compPackageOnPackage.getComponentTypes())
    {
      _componentTypeToPOPLayerIdEnum.put(componentType, componentType.getPOPLayerId());
      _componentTypeToPOPZHeightInNanometers.put(componentType, componentType.getPOPZHeightInNanometers());
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  protected void finalize()
  {    
    _compPackageOnPackageToComponentTypes.clear();
    _compPackageOnPackageToComponentTypes = null;
    
    _componentTypeToPOPLayerIdEnum.clear();
    _componentTypeToPOPLayerIdEnum = null;
    
    _componentTypeToPOPZHeightInNanometers.clear();
    _componentTypeToPOPZHeightInNanometers = null;
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_compPackageOnPackage != null);

    _compPackageOnPackage.destroy();

    return true;
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_compPackageOnPackage != null);

    _compPackageOnPackage.add();

    List<ComponentType> componentTypes = _compPackageOnPackageToComponentTypes.get(_compPackageOnPackage);

    ProjectObservable.getInstance().setEnabled(false);
    try
    {
      for (ComponentType componentType : componentTypes)
      {
        componentType.setCompPackageOnPackage(_compPackageOnPackage);
        componentType.setPOPLayerId(_componentTypeToPOPLayerIdEnum.get(componentType));
        componentType.setPOPZHeightInNanometers(_componentTypeToPOPZHeightInNanometers.get(componentType));
        _compPackageOnPackage.addComponentType(componentType);
      }
    }
    finally
    {
      ProjectObservable.getInstance().setEnabled(true);
    }
    ProjectObservable.getInstance().stateChanged(this, null, (Object)componentTypes, CompPackageOnPackageEventEnum.ADD_OR_REMOVE_COMPONENT_TYPE);
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public String getDescription()
  {
    LocalizedString localizedString;
    localizedString = new LocalizedString("GUI_COMMAND_DESTROY_COMP_PACKAGE_ON_PACKAGE_KEY", null); 
        
    return StringLocalizer.keyToString(localizedString);
  }
}
