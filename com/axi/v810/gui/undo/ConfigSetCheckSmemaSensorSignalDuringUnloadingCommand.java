package com.axi.v810.gui.undo;

import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 *
 * @author Kok Chun, Tan
 */
public class ConfigSetCheckSmemaSensorSignalDuringUnloadingCommand extends DataCommand
{
  private boolean _originalMode;
  private boolean _newMode;
  private PanelHandler _panelHandler = PanelHandler.getInstance();

  /**
   * @author  Kok Chun, Tan
   */
  public ConfigSetCheckSmemaSensorSignalDuringUnloadingCommand(boolean newMode)
  {
    _newMode = newMode;
    _originalMode = _panelHandler.isCheckSmemaSensorSignalDuringUnloadingModeEnabled();
  }

  /**
   * @author  Kok Chun, Tan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalMode == _newMode)
      return false;

    _panelHandler.setCheckSmemaSensorSignalDuringUnloadingModeEnabled(_newMode);
    return true;
  }

  /**
   * @author  Kok Chun, Tan
   */
  public void undoCommand() throws XrayTesterException
  {
    _panelHandler.setCheckSmemaSensorSignalDuringUnloadingModeEnabled(_originalMode);
  }

  /**
   * @author  Kok Chun, Tan
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_CHECK_SMEMA_SENSOR_SIGNAL_DURING_UNLOADING_MODE_KEY");
  }
}
