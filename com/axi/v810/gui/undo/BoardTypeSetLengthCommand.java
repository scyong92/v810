package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.SwingWorkerThread;
import com.axi.v810.gui.mainMenu.MainMenuGui;
import com.axi.v810.util.*;
import com.axi.v810.gui.testDev.*;

/**
 * @author Bill Darbie
 */
public class BoardTypeSetLengthCommand extends DataCommand
{
  private BoardType _boardType;
  private int _oldLengthInNanoMeters;
  private int _newLengthInNanoMeters;
  private Integer _selectedExtendBoardLengthOption;

  /**
   * @author Bill Darbie
   */
  public BoardTypeSetLengthCommand(BoardType boardType, int lengthInNanoMeters)
  {
    Assert.expect(boardType != null);

    _boardType = boardType;
    _newLengthInNanoMeters = lengthInNanoMeters;
    _oldLengthInNanoMeters = _boardType.getLengthInNanoMeters();
  }

  /**
   * @author Bill Darbie
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_boardType != null);

    if (MathUtil.fuzzyEquals(_oldLengthInNanoMeters, _newLengthInNanoMeters, MathUtil._UNIT_CONVERSION_NANOMETER_LIMIT))
      return false;

    BoardTypeSetLengthOptionDialog boardTypeSetLengthOptionDialog = new BoardTypeSetLengthOptionDialog(MainMenuGui.getInstance(),StringLocalizer.keyToString("MMGUI_SETUP_ADJUST_BOARD_TYPE_LENGTH_TITLE_KEY"), true);
    boardTypeSetLengthOptionDialog.showDialog();
    if (boardTypeSetLengthOptionDialog.userClickedOk() == false)
      return false;

    _selectedExtendBoardLengthOption = boardTypeSetLengthOptionDialog.getSelectedRadioButtonId();
    _boardType.setLengthInNanoMeters(_newLengthInNanoMeters);
    // Iterate all pads inside, and extend the board the other side.
    int lengthDifference = _newLengthInNanoMeters - _oldLengthInNanoMeters;
    if (_selectedExtendBoardLengthOption != 0)
    {
      if (_selectedExtendBoardLengthOption == 2)
        lengthDifference /= 2;
      final int finalLengthDifference = lengthDifference;
      MainMenuGui.getInstance().getTestDev().initProgressDialog(1,_boardType.getComponentTypes().size());
      SwingWorkerThread.getInstance().invokeLater(new Runnable()
      {
        public void run()
        {
          for (ComponentType componentType : _boardType.getComponentTypes())
          {
            BoardCoordinate newComponentCoordinate = componentType.getCoordinateInNanoMeters();
            newComponentCoordinate.setLocation(newComponentCoordinate.getX(), newComponentCoordinate.getY()+finalLengthDifference);
            componentType.setCoordinateInNanoMeters(newComponentCoordinate);
          }
        }
      });
      MainMenuGui.getInstance().getTestDev().showProgressDialog(true);
    }
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_boardType != null);

    // Iterate all pads inside, and extend the board the other side.
    int lengthDifference = _newLengthInNanoMeters - _oldLengthInNanoMeters;
    if (_selectedExtendBoardLengthOption != 0)
    {
      if (_selectedExtendBoardLengthOption == 2)
        lengthDifference /= 2;
      final int finalLengthDifference = lengthDifference;
      MainMenuGui.getInstance().getTestDev().initProgressDialog(1,_boardType.getComponentTypes().size());
      SwingWorkerThread.getInstance().invokeLater(new Runnable()
      {
        public void run()
        {
          for (ComponentType componentType : _boardType.getComponentTypes())
          {
            BoardCoordinate newComponentCoordinate = componentType.getCoordinateInNanoMeters();
            newComponentCoordinate.setLocation(newComponentCoordinate.getX(), newComponentCoordinate.getY()-finalLengthDifference);
            componentType.setCoordinateInNanoMeters(newComponentCoordinate);
          }
        }
      });
      MainMenuGui.getInstance().getTestDev().showProgressDialog(true);
    }
    _boardType.setLengthInNanoMeters(_oldLengthInNanoMeters);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_BOARD_TYPE_SET_LENGTH_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
