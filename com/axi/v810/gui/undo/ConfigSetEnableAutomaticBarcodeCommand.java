package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.gui.config.ProductionOptionsBarCodeReaderTabPanel;

/**
 * Command for setting the EnableAutomaticBarcode checkbox
 * @author Laura Cormos
 */
public class ConfigSetEnableAutomaticBarcodeCommand extends DataCommand
{
  private boolean _newEnabledState;
  private boolean _oldEnabledState;
  private ProductionOptionsBarCodeReaderTabPanel _parent;
  private BarcodeReaderManager _serialNumberManager = BarcodeReaderManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetEnableAutomaticBarcodeCommand(boolean enabledState, ProductionOptionsBarCodeReaderTabPanel parent)
  {
    Assert.expect(parent != null);

    _parent = parent;
    _newEnabledState = enabledState;
    _oldEnabledState = _serialNumberManager.getBarcodeReaderAutomaticReaderEnabled();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_oldEnabledState == _newEnabledState)
      return false;

    _parent.setEnabledAllControls(_newEnabledState);

    if (_newEnabledState)
      _parent.checkProtectedConfigurations();

    _serialNumberManager.setBarcodeReaderAutomaticReaderEnabled(_newEnabledState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    if (_oldEnabledState)
      _parent.checkProtectedConfigurations();

    _parent.setEnabledAllControls(_oldEnabledState);

    _serialNumberManager.setBarcodeReaderAutomaticReaderEnabled(_oldEnabledState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_BCR_SET_AUTOMATIC_BCR_ENABLED_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
