package com.axi.v810.gui.undo;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This class will delete a results rule target destination and the deletion can be reset via an undo command.
 * @author Laura Cormos
 */
public class ConfigDeleteResultsTargetDestinationCommand extends DataCommand
{
  private DefaultListModel _listModel;
  private Object _deletedDirectoryName;

  /**
   * @author Laura Cormos
   */
  public ConfigDeleteResultsTargetDestinationCommand(DefaultListModel listModel, Object directoryNameToDelete)
  {
    Assert.expect(listModel != null);
    Assert.expect(directoryNameToDelete != null);

    _listModel = listModel;
    _deletedDirectoryName = directoryNameToDelete;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_listModel.removeElement(_deletedDirectoryName));
    ConfigObservable.getInstance().stateChanged(_deletedDirectoryName);

    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _listModel.addElement(_deletedDirectoryName);
    ConfigObservable.getInstance().stateChanged(_deletedDirectoryName);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_RESPROC_SET_DELETE_DESTINATION_KEY");
  }
}
