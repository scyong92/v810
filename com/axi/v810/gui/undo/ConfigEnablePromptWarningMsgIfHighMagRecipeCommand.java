/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.TestExecution;
import com.axi.v810.util.*;
/**
 *
 * @author hsia-fen.tan
 */
public class ConfigEnablePromptWarningMsgIfHighMagRecipeCommand extends DataCommand
{

  private boolean _originalStatus;
  private boolean _newStatus;
  private TestExecution _testExecution = TestExecution.getInstance();

  public ConfigEnablePromptWarningMsgIfHighMagRecipeCommand(boolean newStatus)
  {
    _newStatus = newStatus;
    _originalStatus = _testExecution.isPromptWarningMsgIfRecipeContainHighMag();
  }

  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalStatus == _newStatus)
    {
      return false;
    }

    _testExecution.setPromptWarningMsgIfRecipeContainHighMag(_newStatus);
    return true;
  }

  public void undoCommand() throws XrayTesterException
  {
    _testExecution.setPromptWarningMsgIfRecipeContainHighMag(_originalStatus);
  }

  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_PROMPT_WARNING_MESSAGE_IF_RECIPE_CONTAIN_HIGH_MAG_KEY");
  }
}
