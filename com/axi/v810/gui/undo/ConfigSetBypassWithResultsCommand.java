package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 *
 * @author Kok Chun, Tan
 */
public class ConfigSetBypassWithResultsCommand extends DataCommand
{
  private boolean _originalBypassMode;
  private boolean _newBypassMode;
  private TestExecution _testExecution = TestExecution.getInstance();

  /**
   * @author Kok Chun, Tan
   */
  public ConfigSetBypassWithResultsCommand (boolean newBypassMode)
  {
    _newBypassMode = newBypassMode;
    _originalBypassMode = _testExecution.isBypassWithResultsEnabled();
  }

  /**
   * @author Kok Chun, Tan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalBypassMode == _newBypassMode)
      return false;

    Config.getInstance().setValue(SoftwareConfigEnum.TEST_EXEC_BYPASS_WITH_RESULTS, _newBypassMode);
    return true;
  }

  /**
   * @author Kok Chun, Tan
   */
  public void undoCommand() throws XrayTesterException
  {
    Config.getInstance().setValue(SoftwareConfigEnum.TEST_EXEC_BYPASS_WITH_RESULTS, _originalBypassMode);
  }

  /**
   * @author Kok Chun, Tan
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_BYPASS_MODE_KEY");
  }
}
