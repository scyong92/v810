package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.util.*;

/**
 * Command for setting the Enable Results Processing checkbox
 * @author Laura Cormos
 */
public class ConfigEnableResultsProcessingCommand extends DataCommand
{
  private boolean _newEnabledState;
  private boolean _oldEnabledState;
  private ResultsProcessor _resultsProcessor = ResultsProcessor.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigEnableResultsProcessingCommand(boolean enabledState)
  {
    _newEnabledState = enabledState;
    _oldEnabledState = _resultsProcessor.isResultsProcessingEnabled();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_oldEnabledState == _newEnabledState)
      return false;

    _resultsProcessor.setIsResultsProcessingEnabled(_newEnabledState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _resultsProcessor.setIsResultsProcessingEnabled(_oldEnabledState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_RESPROC_ENABLE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
