package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Khaw Chek Hau
 * @XCR-3870: False Call Triggering Feature Enhancement
 */
public class ConfigSetEnforceWriteRemarkFalseCallMonitoringCommand extends DataCommand
{
  private boolean _newEnabledState;
  private boolean _oldEnabledState;
  private FalseCallMonitoring _falseCallMonitoring = FalseCallMonitoring.getInstance();
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public ConfigSetEnforceWriteRemarkFalseCallMonitoringCommand(boolean enabledState)
  {
    _newEnabledState = enabledState;
    _oldEnabledState = _falseCallMonitoring.isEnforceWriteRemark();
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_oldEnabledState == _newEnabledState)
      return false;

    _falseCallMonitoring.setEnforceWriteRemark(_newEnabledState);
    return true;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public void undoCommand() throws XrayTesterException
  {
    _falseCallMonitoring.setEnforceWriteRemark(_oldEnabledState);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_FALSE_CALL_TRIGGERING_ENFORCE_WRITE_REMARK_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }  
}
