package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting a component type coordinate
 * @author Wei Chin
 */
public class RemoveFeaturesFromBoardSettingsAlignmentGroupCommand extends DataCommand
{
  private Panel _panel = null;
  private int _alignmentGroupIndex = 0;
  private List<Pad> _pads;
  private List<Fiducial> _fiducials;

  /**
   * @param alignmentGroup The alignment group to add the new pad to
   * @param pads the Pads to add in
   * @author Wei Chin
   */
  public RemoveFeaturesFromBoardSettingsAlignmentGroupCommand(int alignmentGroupIndex, Panel panel, List<Pad> pads, List<Fiducial> fiducials)
  {
    Assert.expect(panel != null);
    Assert.expect(pads != null);
    Assert.expect(fiducials != null);

    _alignmentGroupIndex = alignmentGroupIndex;
    _panel = panel;
    _pads = pads;
    _fiducials = fiducials;
  }

  /**
   * Add the pads to the group
   * @author Wei Chin
   */
  public boolean executeCommand() throws XrayTesterException
  {
    // don't do anything if both lists of pads and fiducials are empty
    if (_pads.isEmpty() && _fiducials.isEmpty())
      return false;

    for (Board board : _panel.getBoards())
    {
      board.removePadsToAlignmentGroups(_alignmentGroupIndex, _pads, _fiducials);
    }
    return true;
  }

  /**
   * Remove the pads (opposite of add)
   * @author Wei Chin
   */
  public void undoCommand() throws XrayTesterException
  {
    for (Board board : _panel.getBoards())
    {
      board.addPadsToAlignmentGroups(_alignmentGroupIndex, _pads, _fiducials);
    }
  }

  /**
   * @author Wei Chin
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_REMOVE_FEATURE_FROM_ALIGNMENT_GROUP_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
