package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PanelSetAutoPopulateDisplayUnitsCommand extends DataCommand 
{
  private Project _project = null;
  private MathUtilEnum _newDisplayUnits = null;
  private MathUtilEnum _oldDisplayUnits = null;

  /**
   * @author Cheah Lee Herng
   */
  public PanelSetAutoPopulateDisplayUnitsCommand(Project project, MathUtilEnum newDisplayUnits)
  {
    Assert.expect(project != null);
    Assert.expect(newDisplayUnits != null);

    _project = project;
    _newDisplayUnits = newDisplayUnits;
    _oldDisplayUnits = _project.getDisplayUnits();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean executeCommand() throws XrayTesterException 
  {
    Assert.expect(_project != null);

    if (_oldDisplayUnits.equals(_newDisplayUnits))
      return false;

    _project.setDisplayUnits(_newDisplayUnits);
    return true;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void undoCommand() throws XrayTesterException 
  {
    Assert.expect(_project != null);

    _project.setDisplayUnits(_oldDisplayUnits);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public String getDescription() 
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PANEL_SET_AUTO_POPULATE_DISPLAY_UNITS_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
  
}
