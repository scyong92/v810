package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the MRT install directory in config is changed,
 * it can be reset via an undo command.
 * @author Laura Cormos
 */
public class ConfigSetRepairToolInstallDirectoryCommand extends DataCommand
{
  private String _originalDirectory;
  private String _newDirectory;
  private ResultsProcessor _resultsProcessor = ResultsProcessor.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetRepairToolInstallDirectoryCommand(String installDirectory)
  {
    Assert.expect(installDirectory != null);

    _newDirectory = installDirectory;
    _originalDirectory = _resultsProcessor.getRepairToolDirectory();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalDirectory.equalsIgnoreCase(_newDirectory))
      return false;

    _resultsProcessor.setRepairToolDirectory(_newDirectory);
    _resultsProcessor.saveToConfigurationFile();
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _resultsProcessor.setRepairToolDirectory(_originalDirectory);
    _resultsProcessor.saveToConfigurationFile();
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_RESPROC_SET_REPAIR_INSTALL_DIR_KEY");
  }
}
