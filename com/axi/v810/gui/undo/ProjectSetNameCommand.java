package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the name of the project is changed,
 * it can be reset via an undo command.
 * @author George A. David
 */
public class ProjectSetNameCommand extends DataCommand
{
  private Project _project;
  private String _originalName;
  private String _newName;

  /**
   * @param project the project whose name will change
   * @param newName the new name of the panel
   * @author George A. David
   */
  public ProjectSetNameCommand(Project project, String newName)
  {
    Assert.expect(project != null);
    Assert.expect(newName != null);

    _project = project;
    _newName = newName;
    _originalName = _project.getName();
  }

  /**
   * Set the new name of the project
   * @author George A. David
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_originalName.equalsIgnoreCase(_newName))
      return false;

    _project.setName(_newName);
    return true;
  }

  /**
   * Reset the name of the project
   * @author George A. David
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setName(_originalName);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_NAME_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
