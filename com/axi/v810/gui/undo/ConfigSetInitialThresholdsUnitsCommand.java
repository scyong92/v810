package com.axi.v810.gui.undo;

import com.axi.util.Assert;
import com.axi.util.LocalizedString;
import com.axi.util.MathUtilEnum;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.StringLocalizer;
import com.axi.v810.util.XrayTesterException;

/**
 * @author Andy Mechtenberg
 */
public class ConfigSetInitialThresholdsUnitsCommand extends DataCommand
{  
  private MathUtilEnum _newUnits = null;
  private MathUtilEnum _oldUnits = null;
  private InitialThresholds _initialThresholds = InitialThresholds.getInstance();
  
  /**
   * @param newDisplayUnits the new threshold set units
   * @author Andy Mechtenberg
   */
  public ConfigSetInitialThresholdsUnitsCommand(MathUtilEnum newDisplayUnits)
  {    
    Assert.expect(newDisplayUnits != null);
    
    _newUnits = newDisplayUnits;
    _oldUnits = _initialThresholds.getCurrentUnits();
  }

  /**
   * Change the display units to the new display units
   * @author George A. David
   */
  public boolean executeCommand() throws XrayTesterException
  {
    
    if (_oldUnits.equals(_newUnits))
      return false;

    _initialThresholds.setCurrentUnits(_newUnits);
    return true;
  }

  /**
   * Restore the old display units
   * @author George A. David
   */
  public void undoCommand() throws XrayTesterException
  {
    _initialThresholds.setCurrentUnits(_oldUnits);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_DISPLAY_UNITS_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
