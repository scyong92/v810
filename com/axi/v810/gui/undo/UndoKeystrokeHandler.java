package com.axi.v810.gui.undo;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * This class handles undo and redo actions from the user.  It handles the user holding down the undo or redo
 * key continuously correctly by throwing away all undo/redo keystrokes while a Command
 * is still running.  It expects an update() call from the GuiObservable to let it know that
 * the GUI is finished responding to the last run Command.  If this update is not called a hang will
 * occur.  It is very important the every Command class that is executed causes the GuiUpdateObservable
 * to update() all Observers!
 *
 * @author Bill Darbie
 */
public class UndoKeystrokeHandler
{
  private static GuiObservable _guiObservable;
//  private static CommandManager _commandManager;

  private WorkerThread _undoWorkerThread = new WorkerThread("undo worker thread");
  private WorkerThread _undoWorkerThread2 = new WorkerThread("undo worker thread 2");
  private KeyStroke _undoKeyStroke = javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_MASK); // CTRL-Z
  private KeyStroke _redoKeyStroke = javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_Y, KeyEvent.CTRL_MASK); // CTRL-Y
  private JMenuItem _undoMenuItem;
  private JMenuItem _redoMenuItem;
  private Component _parentComponent;
  private Object _lock = new Object();
  private volatile boolean _guiUpdated = false;
  private int _numCommands;

  /**
   * @author Bill Darbie
   */
  static
  {
    _guiObservable = GuiObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public UndoKeystrokeHandler(Component parentComponent, JMenuItem undoMenuItem, JMenuItem redoMenuItem)
  {
    Assert.expect(parentComponent != null);
    Assert.expect(undoMenuItem != null);
    Assert.expect(redoMenuItem != null);

    _parentComponent = parentComponent;
    _undoMenuItem = undoMenuItem;
    _redoMenuItem = redoMenuItem;
    _undoMenuItem.setAccelerator(_undoKeyStroke);
    _redoMenuItem.setAccelerator(_redoKeyStroke);
  }

  /**
   * @author Bill Darbie
   */
  public void undo(final com.axi.v810.gui.undo.CommandManager commandManager)
  {
    if (commandManager.isUndoAvailable() == false)
      return;
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(MainMenuGui.getInstance(),
                                                                   _undoMenuItem.getText(),
                                                                   StringLocalizer.keyToString("BUSYCANCELDIALOG_UNDO_REDO_TITLE_KEY"),
                                                                   true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, MainMenuGui.getInstance());
    busyCancelDialog.setCursor(new Cursor(Cursor.WAIT_CURSOR));
    _undoWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          // do not catch undo keystroke events until after the current undo is finished
          // if this is not done and the user is continuously holding down the undo key
          // the Swing thread will be so busy processing key events that it will be noticably
          // slow updating the GUI
          setMenuItemAccelerator(_undoMenuItem, null);

          // we need to check this again (even though we checked at entry) because we we're on the worker thread
          // and there's a race condition.  The previous undo may have executed sometime before this.  This happens
          // then the user holds down crtl-Z and get's a fast auto-repeat
          synchronized (_lock)
          {
            if (commandManager.isUndoAvailable())
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  try
                  {
                    _numCommands = -1;
                    _numCommands = commandManager.undo();
                  }
                  catch(XrayTesterException ex)
                  {
                    MessageDialog.showErrorDialog(_parentComponent,
                                                  ex.getLocalizedMessage(),
                                                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                  true);
                  }
                }
              });
              waitForGuiUpdate();
            }
          }
        }
        finally
        {
          setMenuItemAccelerator(_undoMenuItem, _undoKeyStroke);
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              busyCancelDialog.dispose();
            }
          });
        }
      }
    });
    busyCancelDialog.setVisible(true);
  }

  /**
   * @author Bill Darbie
   */
  public void redo(final com.axi.v810.gui.undo.CommandManager commandManager)
  {
    if (commandManager.isRedoAvailable() == false)
      return;

    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(MainMenuGui.getInstance(),
                                                                   _redoMenuItem.getText(),
                                                                   StringLocalizer.keyToString("BUSYCANCELDIALOG_UNDO_REDO_TITLE_KEY"),
                                                                   true);
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, MainMenuGui.getInstance());
    busyCancelDialog.setCursor(new Cursor(Cursor.WAIT_CURSOR));
    _undoWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          // do not catch undo keystroke events until after the current undo is finished
          // if this is not done and the user is continuously holding down the undo key
          // the Swing thread will be so busy processing key events that it will be noticably
          // slow updating the GUI
          setMenuItemAccelerator(_redoMenuItem, null);

          // we need to check this again (even though we checked at entry) because we we're on the worker thread
          // and there's a race condition.  The previous redo may have executed sometime before this.  This happens
          // then the user holds down crtl-Y and get's a fast auto-repeat
          synchronized (_lock)
          {
            if (commandManager.isRedoAvailable())
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  try
                  {
                    _numCommands = -1;
                    _numCommands = commandManager.redo();
                  }
                  catch(XrayTesterException ex)
                  {
                    MessageDialog.showErrorDialog(_parentComponent,
                                                  ex.getLocalizedMessage(),
                                                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                                  true);
                  }
                }
              });
              waitForGuiUpdate();
            }
          }
        }
        finally
        {
          setMenuItemAccelerator(_redoMenuItem, _redoKeyStroke);
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              busyCancelDialog.dispose();
            }
          });
        }
      }
    });
    busyCancelDialog.setVisible(true);
  }

  /**
   * @author Bill Darbie
   */
  private void waitForGuiUpdate()
  {
    synchronized (_lock)
    {
      try
      {
        while (_guiUpdated == false)
          _lock.wait(200);
      }
      catch (InterruptedException ie)
      {
        // do nothing
      }
      _guiUpdated = false;
    }
  }

  /**
   * @author Bill Darbie
   */
  public void commandFinished()
  {
    _undoWorkerThread2.invokeLater(new Runnable()
    {
      public void run()
      {
        synchronized (_lock)
        {
          while (_numCommands < 0)
          {
            try
            {
              _lock.wait(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }

          --_numCommands;
          if (_numCommands == 0)
          {
            _guiUpdated = true;
            _lock.notify();
          }
        }
      }
    });
  }

  /**
   * A wrapper around the set accelerator to ensure it's on the swing thread
   * @author Andy Mechtenberg
   */
  private void setMenuItemAccelerator(final JMenuItem menuItem, final KeyStroke accelerator)
  {
    Assert.expect(menuItem != null);
    // the accelerator could be null, so we won't assert on it

    try
    {
      SwingUtils.invokeAndWait(new Runnable()
      {
        public void run()
        {
          menuItem.setAccelerator(accelerator);
        }
      });
    }
    catch (InterruptedException iex)
    {
      Assert.logException(iex);
    }
    catch (java.lang.reflect.InvocationTargetException itex)
    {
      Assert.logException(itex);
    }
  }
}
