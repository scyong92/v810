package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting the joint type assignment rule enabled or disabled
 * @author Laura Cormos
 */
public class ConfigSetJointTypeRuleEnabledCommand extends DataCommand
{
  private JointTypeEnumRule _jointTypeEnumRule = null;
  private boolean _newEnabledState;
  private boolean _oldEnabledState;

  /**
   * @author Laura Cormos
   */
  public ConfigSetJointTypeRuleEnabledCommand(JointTypeEnumRule jointTypeEnumRule , boolean enabledState)
  {
    Assert.expect(jointTypeEnumRule != null);

    _jointTypeEnumRule = jointTypeEnumRule;
    _newEnabledState = enabledState;
    _oldEnabledState = _jointTypeEnumRule.isEnabled();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    if (_oldEnabledState == _newEnabledState)
      return false;

    _jointTypeEnumRule.setEnabled(_newEnabledState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    _jointTypeEnumRule.setEnabled(_oldEnabledState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_JTA_SET_JOINT_TYPE_RULE_ENABLED_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
