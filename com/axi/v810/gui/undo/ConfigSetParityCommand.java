package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class sets/unsets the parity for the barcode reader configuration
 * @author Laura Cormos
 */
public class ConfigSetParityCommand extends DataCommand
{
  private String _originalParity;
  private String _newParity;
  private BarcodeReaderManager _bcrManager = BarcodeReaderManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetParityCommand(String portNumber)
  {
    Assert.expect(portNumber != null);

    _newParity = portNumber;
    _originalParity = _bcrManager.getBarcodeReaderRs232Parity();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_newParity != null);

    if (_newParity.equals(_originalParity))
      return false;

    _bcrManager.setBarcodeReaderRs232Parity(_newParity);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_originalParity != null);

    _bcrManager.setBarcodeReaderRs232Parity(_originalParity);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_PARITY_KEY");
  }
}
