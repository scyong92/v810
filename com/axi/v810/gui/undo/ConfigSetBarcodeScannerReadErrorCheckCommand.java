package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * Command for setting the BarcodeScannerReadErrorCheck checkbox
 * @author Laura Cormos
 */
public class ConfigSetBarcodeScannerReadErrorCheckCommand extends DataCommand
{
  private boolean _newEnabledState;
  private boolean _oldEnabledState;
  private SerialNumberManager _serialNumberManager = SerialNumberManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetBarcodeScannerReadErrorCheckCommand(boolean enabledState)
  {
    _newEnabledState = enabledState;
    _oldEnabledState = _serialNumberManager.isBarcodeScannerReadErrorCheckEnabled();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_oldEnabledState == _newEnabledState)
      return false;

    _serialNumberManager.setBarcodeScannerReadErrorCheck(_newEnabledState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _serialNumberManager.setBarcodeScannerReadErrorCheck(_oldEnabledState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SN_SET_BCR_READ_ERROR_CHECK_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
