package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author wei-chin.chong
 */
public class SetUseNewThicknessTableCommand extends DataCommand
{
  private boolean _oldSetting;
  private boolean _newSetting;
  private PanelSettings _panelSettings;

  /**
   * @param board the board to be flipped
   * @author Wei Chin
   */
  public SetUseNewThicknessTableCommand(PanelSettings panelSettings, boolean newSetting)
  {
    Assert.expect(panelSettings != null);

    _oldSetting = panelSettings.isUseNewThicknessTable();
    _newSetting = newSetting;
    _panelSettings = panelSettings;
  }

  /**
   * Flip the board.
   * @author George A. David
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_panelSettings != null);

    _panelSettings.setUseNewThicknessTable(_newSetting);

    return true;
  }

  /**
   * Unflip the board (by flipping it)
   * @author Wei Chin
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_panelSettings != null);

    _panelSettings.setUseNewThicknessTable(_oldSetting);
  }

  /**
   * @author Wei Chin
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_CHANGE_THICKNESS_TABLE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
