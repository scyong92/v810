package com.axi.v810.gui.undo;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * @author George A. David
 */
public class FitGraphicsToScreenCommand extends GuiCommand
{
  private GraphicsEngine _graphicsEngine = null;
  private double _zoomFactor = -1;

  /**
   * @author George A. David
   */
  public FitGraphicsToScreenCommand(GraphicsEngine graphicsEngine)
  {
    Assert.expect(graphicsEngine != null);

    _graphicsEngine = graphicsEngine;
    _zoomFactor = _graphicsEngine.getCurrentZoomFactor();
  }

  /**
   * @author George A. David
   */
  protected Object getGuiEventSource()
  {
    Assert.expect(_graphicsEngine != null);

    return _graphicsEngine;
  }

  /**
   * @author George A. David
   */
  protected GuiEventEnum getGuiEventEnum()
  {
    return GuiUpdateEventEnum.CAD_GRAPHICS;
  }

  /**
   * @author George A. David
   */
  public boolean executeCommand() throws com.axi.v810.util.XrayTesterException
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.fitGraphicsToScreen();

    return true;
  }

  /**
   * @author George A. David
   */
  public void undoCommand() throws com.axi.v810.util.XrayTesterException
  {
    Assert.expect(_graphicsEngine != null);

    _graphicsEngine.zoom(_zoomFactor);
  }

  /**
   * @author George A. David
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_FIT_GRAPHICS_TO_SCREEN_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
