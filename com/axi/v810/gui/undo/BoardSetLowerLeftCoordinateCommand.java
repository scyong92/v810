package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class BoardSetLowerLeftCoordinateCommand extends DataCommand
{
  private Board _board;
  private PanelCoordinate _oldCoordInNanoMeters;
  private PanelCoordinate _newCoordInNanoMeters;

  /**
   * @author Bill Darbie
   */
  public BoardSetLowerLeftCoordinateCommand(Board board, PanelCoordinate lowerLeftCoordinateRelativeToPanelInNanoMeters)
  {
    Assert.expect(board != null);

    _board = board;
    _newCoordInNanoMeters = lowerLeftCoordinateRelativeToPanelInNanoMeters;
    _oldCoordInNanoMeters = _board.getLowerLeftCoordinateRelativeToPanelInNanoMeters();
  }

  /**
   * @author Bill Darbie
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_board != null);

    if (_oldCoordInNanoMeters.equals(_newCoordInNanoMeters))
      return false;

    _board.setLowerLeftCoordinateRelativeToPanelInNanoMeters(_newCoordInNanoMeters);
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_board != null);
    _board.setLowerLeftCoordinateRelativeToPanelInNanoMeters(_oldCoordInNanoMeters);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_BOARD_SET_LOWER_LEFT_COORDINATE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
