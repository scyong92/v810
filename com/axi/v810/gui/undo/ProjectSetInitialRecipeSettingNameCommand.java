/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author weng-jian.eoh
 */
public class ProjectSetInitialRecipeSettingNameCommand extends DataCommand
{
  private Project _project;
  private String _originalInitialRecipeSettingSetName;
  private String _newInitialRecipeSettingSetName;

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   *
   * @author weng-jian.eoh
   */
  public ProjectSetInitialRecipeSettingNameCommand(Project project, String newInitialRecipeSettingSetName)
  {
    Assert.expect(project != null);
    Assert.expect(newInitialRecipeSettingSetName != null);

    _project = project;
    _newInitialRecipeSettingSetName = newInitialRecipeSettingSetName;
    _originalInitialRecipeSettingSetName = _project.getInitialRecipeSettingName();
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   *
   * @author weng-jian.eoh
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_originalInitialRecipeSettingSetName.equalsIgnoreCase(_newInitialRecipeSettingSetName))
      return false;

    _project.setInitialRecipeSettingSetName(_newInitialRecipeSettingSetName);
    return true;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   *
   * @author weng-jian.eoh
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setInitialRecipeSettingSetName(_originalInitialRecipeSettingSetName);
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   *
   * @author weng-jian.eoh
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_INITIAL_THRESHOLD_NAME_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
