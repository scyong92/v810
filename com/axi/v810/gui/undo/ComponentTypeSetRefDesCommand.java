package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting a component type reference designator
 * @author Laura Cormos
 */
public class ComponentTypeSetRefDesCommand extends DataCommand
{
  private SideBoardType _sideBoardType = null;
  private String _newRefDes = null;
  private String _oldRefDes = null;

  /**
   * @param compType the component type whose name will change
   * @param newRefDes the new reference designator
   * @author Laura Cormos
   */
  public ComponentTypeSetRefDesCommand(ComponentType compType , String newRefDes)
  {
    Assert.expect(compType != null);
    Assert.expect(newRefDes != null);

    _newRefDes = newRefDes;
    _oldRefDes = compType.getReferenceDesignator();
    _sideBoardType = compType.getSideBoardType();
  }

  /**
   * Change the name to the new name
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_sideBoardType != null);
    Assert.expect(_oldRefDes != null);
    Assert.expect(_newRefDes != null);

    if (_oldRefDes.equals(_newRefDes))
      return false;

    ComponentType componentType = _sideBoardType.getComponentType(_oldRefDes);
    Assert.expect(componentType != null);
    componentType.setReferenceDesignator(_newRefDes);
    return true;
  }

  /**
   * Restore the old name
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_sideBoardType != null);
    Assert.expect(_oldRefDes != null);
    Assert.expect(_newRefDes != null);

    ComponentType componentType = _sideBoardType.getComponentType(_newRefDes);
    Assert.expect(componentType != null);

    componentType.setReferenceDesignator(_oldRefDes);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_COMPONENT_TYPE_REF_DES_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
