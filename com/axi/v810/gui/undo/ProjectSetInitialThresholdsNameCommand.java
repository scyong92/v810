package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the customer name of the project is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ProjectSetInitialThresholdsNameCommand extends DataCommand
{
  private Project _project;
  private String _originalInitialThresholdSetName;
  private String _newInitialThresholdSetName;

  /**
   * @param project the project whose customer name will change
   * @param newInitialThresholdSetName the new initial threshold set name for the project
   * @author Andy Mechtenberg
   */
  public ProjectSetInitialThresholdsNameCommand(Project project, String newInitialThresholdSetName)
  {
    Assert.expect(project != null);
    Assert.expect(newInitialThresholdSetName != null);

    _project = project;
    _newInitialThresholdSetName = newInitialThresholdSetName;
    _originalInitialThresholdSetName = _project.getInitialThresholdsSetName();
  }

  /**
   * Set the initial threshold set name for the project
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_originalInitialThresholdSetName.equalsIgnoreCase(_newInitialThresholdSetName))
      return false;

    _project.setInitialThresholdsSetName(_newInitialThresholdSetName);
    return true;
  }

  /**
   * Reset the initial threshold set for the project
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setInitialThresholdsSetName(_originalInitialThresholdSetName);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_INITIAL_THRESHOLD_NAME_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
