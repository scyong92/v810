package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the mail server in config is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ConfigSetMailServerCommand extends DataCommand
{
  private String _originalMailServer;
  private String _newMailServer;
  private CustomerInfo _customerInfo = CustomerInfo.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetMailServerCommand(String newMailServer)
  {
    Assert.expect(newMailServer != null);

    _newMailServer = newMailServer;
    _originalMailServer = _customerInfo.getMailServer();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalMailServer.equalsIgnoreCase(_newMailServer))
      return false;

    _customerInfo.setMailServer(_newMailServer);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _customerInfo.setMailServer(_originalMailServer);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_MAIL_SERVER_KEY");
  }
}
