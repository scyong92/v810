package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting a component type coordinate
 * @author Laura Cormos
 */
public class ComponentTypeSetPOPZHeightInNanometersCommand extends DataCommand
{
  // in this command we don't want to store the actual component type passed in the constructor because this object
  // can be destroyed and recreated during an undo/redo operation. In that case this command's undo method would
  // operate on nonexistant object. This is why we store the object name and find it every time we need to use it
  private String _refDes = null;
  private SideBoardType _sideBoardType = null;
  private int _newPOPZHeightInNanomaters = 0;
  private int _oldPOPZHeightInNanomaters = 0;

  /**
   * @param componentType the component type whose coordinate will change
   * @param newCoord the new coordinate
   * @author Laura Cormos
   */
  public ComponentTypeSetPOPZHeightInNanometersCommand(ComponentType componentType, int popZHeightInNanometers)
  {
    Assert.expect(componentType != null);

    _refDes = componentType.getReferenceDesignator();
    _newPOPZHeightInNanomaters = popZHeightInNanometers;
    _oldPOPZHeightInNanomaters = componentType.getPOPZHeightInNanometers();
    _sideBoardType = componentType.getSideBoardType();
  }

  /**
   * Change the coordinate to the new coordinate
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_sideBoardType != null);
    Assert.expect(_refDes != null);

    if (_oldPOPZHeightInNanomaters == _newPOPZHeightInNanomaters)
      return false;

    // first find the component type object because it could have been recreated through an undo/redo operation
    ComponentType componentType = _sideBoardType.getComponentType(_refDes);
    componentType.setPOPZHeightInNanometers(_newPOPZHeightInNanomaters);
    return true;
  }

  /**
   * Restore the old coordinate
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_sideBoardType != null);
    Assert.expect(_refDes != null);

    // first find the component type object because it could have been recreated through an undo/redo operation
    ComponentType componentType = _sideBoardType.getComponentType(_refDes);
    componentType.setPOPZHeightInNanometers(_oldPOPZHeightInNanomaters);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_COMPONENT_SET_POP_Z_HEIGHT_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
