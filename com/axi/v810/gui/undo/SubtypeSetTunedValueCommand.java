package com.axi.v810.gui.undo;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.ProjectHistoryLog;
import com.axi.v810.util.*;

/**
 * Command for setting a component type coordinate
 * @author Laura Cormos
 */
public class SubtypeSetTunedValueCommand extends DataCommand
{
  private Subtype _subtype;
  private AlgorithmSettingEnum _algorithmSettingEnum;
  private Serializable _origValue;
  private Serializable _newValue;
  private static transient ProjectHistoryLog _fileWriter;

  /**
   * @param subtype The subtype for the tuned value
   * @param algorithmSettingEnum the AlgorithmSettingEnum for the new value
   * @param newValue the new value
   * @author Andy Mechtenberg
   */
  public SubtypeSetTunedValueCommand(Subtype subtype, AlgorithmSettingEnum algorithmSettingEnum, Serializable newValue)
  {
    Assert.expect(subtype != null);
    Assert.expect(algorithmSettingEnum != null);
    Assert.expect(newValue != null);

    _subtype = subtype;
    _algorithmSettingEnum = algorithmSettingEnum;
    _newValue = newValue;
    _origValue = _subtype.getAlgorithmSettingValue(algorithmSettingEnum);
    _fileWriter = ProjectHistoryLog.getInstance();
  }

  /**
   * Add the features to the group
   * @return true if the command was executed
   * @throws XrayTesterException
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_newValue.equals(_origValue))
      return false;
    _subtype.setTunedValue(_algorithmSettingEnum, _newValue);
    return true;
  }

  /**
   * Remove the feature (opposite of add)
   * @throws XrayTesterException
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _subtype.setTunedValue(_algorithmSettingEnum, _origValue); 
    
    //keep track undo value into history log file-hsia-fen.tan
     _fileWriter.appendUndoThreshold(_subtype, _algorithmSettingEnum, _newValue, _origValue);
  }

  /**
   * @return a localized description of the command
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SUBTYPE_SET_VALUE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
