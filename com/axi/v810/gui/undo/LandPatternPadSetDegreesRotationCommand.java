package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting a land pattern pad rotation
 * @author Laura Cormos
 */
public class LandPatternPadSetDegreesRotationCommand extends DataCommand
{
  // in this command we don't want to store the actual landpatternpad passed in the constructor because this object
  // can be destroyed and recreated during an undo/redo operation. In that case this command's undo method would
  // operate on nonexistant object. This is why we store the object name and find it every time we need to use it
  private String _landPatternName = null;
  private String _landPatternPadName = null;
  private Panel _panel = null;
  private double _newRotation = 0;
  private double _oldRotation = 0;

  /**
   * @param landPatternPad the land pattern pad whose rotation will change
   * @param newRotation the new rotation
   * @author Laura Cormos
   */
  public LandPatternPadSetDegreesRotationCommand(LandPatternPad landPatternPad , double newRotation)
  {
    Assert.expect(landPatternPad != null);

    _landPatternName = landPatternPad.getLandPattern().getName();
    _panel = landPatternPad.getLandPattern().getPanel();
    _landPatternPadName = landPatternPad.getName();
    _newRotation = newRotation;
    _oldRotation = landPatternPad.getDegreesRotation();
  }

  /**
   * Change the rotation to the new rotation
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_landPatternName != null);
    Assert.expect(_landPatternPadName != null);
    Assert.expect(_panel != null);

    if (_oldRotation == _newRotation)
      return false;

    // first find the land pattern object that this land pattern pad belongs to since this may be a redo of the original
    // command and the land pattern could have been deleted and added back.
    LandPattern landPattern = _panel.getLandPattern(_landPatternName);
    Assert.expect(landPattern != null);
    LandPatternPad landPatternPad = landPattern.getLandPatternPad(_landPatternPadName);
    Assert.expect(landPatternPad != null);
    landPatternPad.setDegreesRotation(_newRotation);
    return true;
  }

  /**
   * Restore the old rotation
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_landPatternName != null);
    Assert.expect(_landPatternPadName != null);
    Assert.expect(_panel != null);

    // first find the land pattern object that this land pattern pad belongs to since this may be a redo of the original
    // command and the land pattern could have been deleted and added back.
    LandPattern landPattern = _panel.getLandPattern(_landPatternName);
    Assert.expect(landPattern != null);
    LandPatternPad landPatternPad = landPattern.getLandPatternPad(_landPatternPadName);
    Assert.expect(landPatternPad != null);
    landPatternPad.setDegreesRotation(_oldRotation);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_LAND_PATTERN_PAD_SET_ROTATION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
