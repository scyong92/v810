package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
public class ConfigSetNumRepairImagesToSaveCommand extends DataCommand
{
  private int _originalNumRepairImagesToSave;
  private int _newNumRepairImagesToSave;
  private RepairImagesConfigManager _config = RepairImagesConfigManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetNumRepairImagesToSaveCommand(int newNumRepairImagesToSave)
  {
    _newNumRepairImagesToSave = newNumRepairImagesToSave;
    _originalNumRepairImagesToSave = _config.getNumRepairImagesToSave();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalNumRepairImagesToSave == _newNumRepairImagesToSave)
      return false;

    _config.setNumRepairImagesToSave(_newNumRepairImagesToSave);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _config.setNumRepairImagesToSave(_originalNumRepairImagesToSave);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_NUM_REPAIR_IMAGES_TO_SAVE_KEY");
  }
}
