package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 *
 * @author Administrator
 */
public class ConfigEnableSavingRecipeAfterLearningCommand extends DataCommand 
{
    private boolean _originalMode;
    private boolean _newMode;
    private TestExecution _testExecution = TestExecution.getInstance();
    
    /**
     * @author Laura Cormos
    */
    public ConfigEnableSavingRecipeAfterLearningCommand(boolean newMode)
    {
        _newMode = newMode;
        _originalMode = _testExecution.isPromptSavingRecipeAfterLearning();
    }

    /**
     * @author Laura Cormos
    */
    public boolean executeCommand() throws XrayTesterException
    {
        if (_originalMode == _newMode)
          return false;

        _testExecution.setPromptSavingRecipeAfterLearning(_newMode);
        return true;
    }

    /**
     * @author Laura Cormos
    */
    public void undoCommand() throws XrayTesterException
    {
        _testExecution.setPromptSavingRecipeAfterLearning(_originalMode);
    }

    /**
     * @author Laura Cormos
    */
    public String getDescription()
    {
        return StringLocalizer.keyToString("GUI_COMMAND_SET_PROMPT_SAVING_RECIPE_AFTER_LEARNING_KEY");
    }
}
