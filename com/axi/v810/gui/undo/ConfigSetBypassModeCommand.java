package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ConfigSetBypassModeCommand extends DataCommand
{
  private boolean _originalBypassMode;
  private boolean _newBypassMode;
  private TestExecution _testExecution = TestExecution.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetBypassModeCommand(boolean newBypassMode)
  {
    _newBypassMode = newBypassMode;
    _originalBypassMode = _testExecution.isBypassModeEnabled();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalBypassMode == _newBypassMode)
      return false;

    _testExecution.setBypassMode(_newBypassMode);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _testExecution.setBypassMode(_originalBypassMode);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_BYPASS_MODE_KEY");
  }
}
