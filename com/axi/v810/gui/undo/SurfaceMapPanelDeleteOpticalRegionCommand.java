/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 * This class is used to delete OpticalRegion from PanelSurfaceMapSettings
 * Using this class allows us to undo the set later.
 * @author Ying-Huan.Chu
 */
public class SurfaceMapPanelDeleteOpticalRegionCommand extends DataCommand
{
  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  private PanelSurfaceMapSettings _panelSurfaceMapSettings = null;
  private OpticalRegion _opticalRegion = null;
  private java.util.List<OpticalCameraRectangle> _opticalCameraRectangles = new ArrayList<>();
  private java.util.List<com.axi.v810.business.panelDesc.Component> _components = new ArrayList<>();
  private java.util.Map<String, PanelCoordinate> _pointPanelPositionNameToPointPanelCoordinateMap = new LinkedHashMap<>();
  private Project _project;
  
  /**
   * @param panelSurfaceMapSettings the PanelSurfaceMapSettings to have its OpticalRegion removed.
   * @param opticalRegion the opticalRegion to be deleted.
   * @author Ying-Huan.Chu
   */
  public SurfaceMapPanelDeleteOpticalRegionCommand(Project project, PanelSurfaceMapSettings panelSurfaceMapSettings, OpticalRegion opticalRegion)
  {
    Assert.expect(project != null);
    Assert.expect(panelSurfaceMapSettings != null);
    Assert.expect(opticalRegion != null);
    
    _panelSurfaceMapSettings = panelSurfaceMapSettings;
    _opticalRegion = opticalRegion;
    _project = project;
    
    _opticalCameraRectangles.addAll(_opticalRegion.getAllOpticalCameraRectangles());
    _components.addAll(_opticalRegion.getComponents());
    _pointPanelPositionNameToPointPanelCoordinateMap.putAll(_opticalRegion.getPointPanelPositionNameToPointPanelCoordinateMap());
  }
  
  /**
   * Delete the OpticalCameraRectangle from the OpticalRegion
   * @return true if the command executed
   * @throws XrayTesterException if the command fails
   * @author Ying-Huan.Chu
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_panelSurfaceMapSettings != null);
    Assert.expect(_opticalRegion != null);
    
    _projectObservable.setEnabled(false);
    try
    {
      _panelSurfaceMapSettings.removeSurfaceMapRegion(_opticalRegion);
      _opticalRegion.removeAllOpticalCameraRectangles();
      _opticalRegion.removeAllComponents();
      _opticalRegion.removeAllPanelCoordinates();
      _project.getTestProgram().removeOpticalRegion(_opticalRegion);
    }
    finally
    {
      _projectObservable.setEnabled(true);
      _projectObservable.stateChanged(this, _opticalRegion, null, PanelSurfaceMapEventEnum.REMOVE_SURFACE_MAP_REGION);
    }
    
    return true;
  }

  /**
   * Reset back to the original OpticalRegion
   * @throws XrayTesterException if the command fails
   * @author Ying-Huan.Chu
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_panelSurfaceMapSettings != null);
    Assert.expect(_opticalRegion != null);
    
    _projectObservable.setEnabled(false);
    try
    {
      for (OpticalCameraRectangle opticalCameraRectangle : _opticalCameraRectangles)
      {
        _opticalRegion.addOpticalCameraRectangle(opticalCameraRectangle);
      }
      for (com.axi.v810.business.panelDesc.Component component : _components)
      {
        _opticalRegion.addComponent(component);
      }
      for (Map.Entry<String, PanelCoordinate> entry : _pointPanelPositionNameToPointPanelCoordinateMap.entrySet())
      {
        int pointXCoordinateInNanometers = entry.getValue().getX();
        int pointYCoordinateInNanometers = entry.getValue().getY();
        _opticalRegion.addSurfaceMapPoints(pointXCoordinateInNanometers, pointYCoordinateInNanometers, true);
      }
      _panelSurfaceMapSettings.addOpticalRegion(_opticalRegion);
      _project.getTestProgram().addOpticalRegion(_opticalRegion);
    }
    finally
    {
      _projectObservable.setEnabled(true);
      _projectObservable.stateChanged(this, null, _opticalRegion, PanelSurfaceMapEventEnum.ADD_SURFACE_MAP_OPTICAL_REGION);
    }
  }

  /**
   * @author Ying-Huan.Chu
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PSP_DELETE_OPTICAL_REGION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
