package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This project is used store the state of large view image
 * @author bee-hoon.goh
 */
public class ProjectSetEnableLargeViewImageCommand extends DataCommand
{
  private Project _project = null;
  private boolean _newEnableLargeViewImage = false;
  private boolean _oldEnableLargeViewImage = false;

  /**
   * @param project the project will have large view image generation
   * @param isEnableLargeViewImage the new state of large view image
   * @author bee-hoon.goh
   */
  public ProjectSetEnableLargeViewImageCommand(Project project, boolean isEnableLargeViewImage)
  {
    Assert.expect(project != null);

    _project = project;
    _newEnableLargeViewImage = isEnableLargeViewImage;
    _oldEnableLargeViewImage = _project.isEnableLargeImageView();
  }

  /**
   * Change the state of large view image
   * @author bee-hoon.goh
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_oldEnableLargeViewImage == _newEnableLargeViewImage)
      return false;

    _project.setEnableLargeImageView(_newEnableLargeViewImage);
    return true;
  }

  /**
   * Restore the old state of enable large view image
   * @author bee-hoon.goh
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setEnableLargeImageView(_oldEnableLargeViewImage);
  }

  /**
   * @author bee-hoon.goh
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_ENABLE_LARGE_VIEW_IMAGE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
