package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;
import com.axi.v810.gui.config.*;
import com.axi.v810.datastore.config.*;

/**
 * This class will delete a joint type rule from the current rule set and the deletion can be reset via an undo command.
 * @author Laura Cormos
 */
public class ConfigDeleteJointTypeRuleCommand extends DataCommand
{
  private JointTypeEnumRule _deletedJointTypeRule;
  private JointTypeRulesTable _table;
  private JointTypeEnumAssignment _jointTypeAssignment;

  /**
   * @author Laura Cormos
   */
  public ConfigDeleteJointTypeRuleCommand(JointTypeRulesTable table, JointTypeEnumRule jointTypeRuleToDelete)
  {
    Assert.expect(table != null);
    Assert.expect(jointTypeRuleToDelete != null);

    _table = table;
    _deletedJointTypeRule = jointTypeRuleToDelete;
    _jointTypeAssignment = JointTypeEnumAssignment.getInstance();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    java.util.List<JointTypeEnumRule> allRules = _jointTypeAssignment.getJointTypeEnumRules();
    Assert.expect(allRules.remove(_deletedJointTypeRule));
    _jointTypeAssignment.setNewRulesList(allRules);
    ((JointTypeRulesTableModel)_table.getModel()).deleteRule(_deletedJointTypeRule);

    ConfigObservable.getInstance().stateChanged(_deletedJointTypeRule);

    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    java.util.List<JointTypeEnumRule> allRules = _jointTypeAssignment.getJointTypeEnumRules();
    allRules.add(_deletedJointTypeRule);
    _jointTypeAssignment.setNewRulesList(allRules);
    ((JointTypeRulesTableModel)_table.getModel()).addNewRule(_deletedJointTypeRule);
    _table.setRowSelectionInterval(_table.getModel().getRowCount() - 1, _table.getModel().getRowCount() - 1);

    ConfigObservable.getInstance().stateChanged(_deletedJointTypeRule);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_JTA_DELETE_RULE_KEY");
  }
}
