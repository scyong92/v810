package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Cheah, Lee Herng
 */
public class ComponentTypeSetGlobalSurfaceModelCommand extends DataCommand
{
    private ComponentType _componentType = null;
    private GlobalSurfaceModelEnum _origGlobalSurfaceModelState = null;
    private GlobalSurfaceModelEnum _newGlobalSurfaceModelState = null;

    /**
     * @author Cheah, Lee Herng
     */
    public ComponentTypeSetGlobalSurfaceModelCommand(ComponentType componentType, GlobalSurfaceModelEnum newGlobalSurfaceModelState)
    {
        Assert.expect(componentType != null);
        Assert.expect(newGlobalSurfaceModelState != null);

        _componentType = componentType;
        _newGlobalSurfaceModelState = newGlobalSurfaceModelState;
        _origGlobalSurfaceModelState = componentType.getGlobalSurfaceModel();
    }

    /**
     * @author Cheah, Lee Herng
     */
    public boolean executeCommand() throws XrayTesterException
    {
        Assert.expect(_componentType != null);

        if (_newGlobalSurfaceModelState.equals(_origGlobalSurfaceModelState))
            return false;

        List<PadType> padTypes = _componentType.getPadTypes();
        for (PadType padType : padTypes)
            padType.getPadTypeSettings().setGlobalSurfaceModel(_newGlobalSurfaceModelState);

        return true;
    }

    /**
     * @author Cheah, Lee Herng
     */
    public void undoCommand() throws XrayTesterException
    {
        Assert.expect(_componentType != null);

        List<PadType> padTypes = _componentType.getPadTypes();
        for (PadType padType : padTypes)
            padType.getPadTypeSettings().setGlobalSurfaceModel(_origGlobalSurfaceModelState);
    }

    /**
     * @author Cheah, Lee Herng
     */
    public String getDescription()
    {
        return StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_GLOBAL_SURFACE_MODEL_KEY");
    }
}
