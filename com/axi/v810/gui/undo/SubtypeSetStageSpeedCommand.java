package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author sheng-chuan.yong
 */
public class SubtypeSetStageSpeedCommand extends DataCommand
{
  private SubtypeAdvanceSettings _subtypeAdvanceSettings = null;
  private List<StageSpeedEnum> _origStageSpeed = null;
  private List<StageSpeedEnum> _newStageSpeed = null;
  private boolean isSpeedSettingOptimized = false;
  
  /**
   * Construct the class.
   * @author sheng-chuan.yong
   */
  public SubtypeSetStageSpeedCommand(SubtypeAdvanceSettings subtypeAdvanceSettings, List<StageSpeedEnum> stageSpeed)
  {
    Assert.expect(subtypeAdvanceSettings != null);
    Assert.expect(stageSpeed != null);

    _subtypeAdvanceSettings = subtypeAdvanceSettings;
    _newStageSpeed = stageSpeed;
    _origStageSpeed = subtypeAdvanceSettings.getStageSpeedList();
    isSpeedSettingOptimized = subtypeAdvanceSettings.isAutoExposureSettingLearnt();
  }

  /**
   * @author sheng-chuan.yong
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_subtypeAdvanceSettings != null);

    if (_subtypeAdvanceSettings.getStageSpeedList().size() == _newStageSpeed.size() == false ||
        _subtypeAdvanceSettings.getStageSpeedList().containsAll(_newStageSpeed) == false)
    {
      _subtypeAdvanceSettings.setStageSpeedEnum(_newStageSpeed);
      _subtypeAdvanceSettings.setIsAutoExposureSettingLearnt(false);
    }
    return true;
  }

  /**
   * @author sheng-chuan.yong
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_subtypeAdvanceSettings != null);

    _subtypeAdvanceSettings.setStageSpeedEnum(_origStageSpeed);
    _subtypeAdvanceSettings.setIsAutoExposureSettingLearnt(isSpeedSettingOptimized);
  }

  /**
   * @author sheng-chuan.yong
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_STAGE_SPEED_KEY");
  }
}
