package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the rotation of a board is changed,
 * it can be undone via and undo function
 * @author George A. David
 */
public class BoardSetRotationCommand extends DataCommand
{
  private Board _board;
  private int _originalRotation;
  private int _newRotation;

  /**
   * Construct the class.
   * @param board the board whose rotation will be modified
   * @param degreesRotation the value of the rotation
   * @author George A. David
   */
  public BoardSetRotationCommand(Board board, int degreesRotation)
  {
    Assert.expect(board != null);

    _board = board;
    _newRotation = degreesRotation;
    _originalRotation = _board.getDegreesRotationRetainingLowerLeftCorner();
  }

  /**
   * Set the rotation of the board
   * @author George A. David
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_board != null);

    if (_originalRotation == _newRotation)
      return false;

     _board.setDegreesRotationRetainingLowerLeftCorner(_newRotation);
     return true;
  }

  /**
   * Undo the setting of the board rotation
   * @author George A. David
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_board != null);

    _board.setDegreesRotationRetainingLowerLeftCorner(_originalRotation);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_BOARD_SET_ROTATION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
