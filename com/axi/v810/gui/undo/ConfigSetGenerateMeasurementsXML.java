package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * XCR-3551
 * Command for undo the customize measurement XML config enum
 * @author huai-en.janan-ezekie
 */
public class ConfigSetGenerateMeasurementsXML extends DataCommand
{
  private boolean _originalSetGenerateMeasurementsXMLState;
  private boolean _newSetGenerateMeasurementsXMLState;
  private TestExecution _testExecution = TestExecution.getInstance();
  
  public ConfigSetGenerateMeasurementsXML(boolean newSetGenerateMeasurementsXMLState)
  {
    _newSetGenerateMeasurementsXMLState = newSetGenerateMeasurementsXMLState;
    _originalSetGenerateMeasurementsXMLState = _testExecution.isGenerateMeasurementsXMLEnabled();
  }
  
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalSetGenerateMeasurementsXMLState == _newSetGenerateMeasurementsXMLState)
    {
      return false;
    }
    
    _testExecution.setGenerateMeasurementsXML(_newSetGenerateMeasurementsXMLState);
    InspectionEngine.getInstance().setGenerateMeasurementsXML(_newSetGenerateMeasurementsXMLState);
    return true;
  }
  
  public void undoCommand() throws XrayTesterException
  {
    _testExecution.setGenerateMeasurementsXML(_originalSetGenerateMeasurementsXMLState);
    InspectionEngine.getInstance().setGenerateMeasurementsXML(_originalSetGenerateMeasurementsXMLState);
  }
  
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_ENABLE_GENERATE_MEASUREMENTS_XML_KEY");
  }
}
