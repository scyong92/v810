package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Chnee Khang Wah, 2011-12-13, 2.5D HIP Development
 */
public class ConfigSetGenerateMultiAngleImagesCommand extends DataCommand
{
  private boolean _originalSetLimitState;
  private boolean _newSetLimitState;
  private RepairImagesConfigManager _config = RepairImagesConfigManager.getInstance();

  /**
   * @author Chnee Khang Wah, 2011-12-13, 2.5D HIP Development
   */
  public ConfigSetGenerateMultiAngleImagesCommand(boolean newSetLimitState)
  {
    _newSetLimitState = newSetLimitState;
    _originalSetLimitState = _config.isGenerateMultiAngleImagesEnabled();
  }

  /**
   * @author Chnee Khang Wah, 2011-12-13, 2.5D HIP Development
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalSetLimitState == _newSetLimitState)
      return false;

    _config.setGenerateMultiAngleImagesEnabled(_newSetLimitState);
    return true;
  }

  /**
   * @author Chnee Khang Wah, 2011-12-13, 2.5D HIP Development
   */
  public void undoCommand() throws XrayTesterException
  {
    _config.setGenerateMultiAngleImagesEnabled(_originalSetLimitState);
  }

  /**
   * @author Chnee Khang Wah, 2011-12-13, 2.5D HIP Development
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_GENERATE_MULTI_ANGLE_IMAGES_KEY");
  }
}
