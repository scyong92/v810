package com.axi.v810.gui.undo;

import java.awt.geom.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for moving an entire board side by an offset (used in VerifyCAD)
 * @author George Booth
 */

public class MoveBoardSideByOffsetInNanoMetersCommand extends DataCommand
{

  private SideBoard _sideBoard = null;
  private Point2D _offsetToApply = null;
  private Point2D _offsetToRestore = null;

  /**
   * @param SideBoard the SideBoard to move by the offset
   * @param offset the amount to move
   * @author George Booth
   */
  public MoveBoardSideByOffsetInNanoMetersCommand(SideBoard SideBoard, Point2D offset)
  {
    Assert.expect(SideBoard != null);
    Assert.expect(offset != null);

    _sideBoard = SideBoard;
    _offsetToApply = offset;
    // undo offset is negtive of offset
    _offsetToRestore = new Point2D.Double(-_offsetToApply.getX(), -_offsetToApply.getY());
  }

  /**
   * Offset the SideBoard
   * @author George Booth
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_sideBoard != null);

    int xOffset = (int)_offsetToApply.getX();
    int yOffset = (int)_offsetToApply.getY();
    if (xOffset == 0 && yOffset == 0)
      return false;

    _sideBoard.moveByOffSetInNanoMeters(xOffset, yOffset);

    return true;
  }

  /**
   * Restore the old location
   * @author George Booth
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_sideBoard != null);

    int xOffset = (int)_offsetToRestore.getX();
    int yOffset = (int)_offsetToRestore.getY();

    _sideBoard.moveByOffSetInNanoMeters(xOffset, yOffset);
  }

  /**
   * @author George Booth
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("CAD_MOVE_BOARD_SIDE_GUI_COMMAND_UNDO_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
