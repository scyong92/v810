package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting the joint type assignment rule's land pattern name pattern
 * @author Laura Cormos
 */
public class ConfigSetJointTypeRuleLandPatternNamePatternCommand extends DataCommand
{
  private JointTypeEnumRule _jointTypeEnumRule = null;
  private String _newLandPatternNamePattern;
  private String _oldLandPatternNamePattern;

  /**
   * @author Laura Cormos
   */
  public ConfigSetJointTypeRuleLandPatternNamePatternCommand(JointTypeEnumRule jointTypeEnumRule , String landPatternNamePattern)
  {
    Assert.expect(jointTypeEnumRule != null);
    Assert.expect(landPatternNamePattern != null);

    _jointTypeEnumRule = jointTypeEnumRule;
    _newLandPatternNamePattern = landPatternNamePattern;
    if (_jointTypeEnumRule.isLandPatternNamePatternSet())
      _oldLandPatternNamePattern = _jointTypeEnumRule.getLandPatternNamePattern();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    _jointTypeEnumRule.setLandPatternNamePattern(_newLandPatternNamePattern);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    if (_oldLandPatternNamePattern == null)
      _jointTypeEnumRule.clearLandPatternNamePattern();
    else
      _jointTypeEnumRule.setLandPatternNamePattern(_oldLandPatternNamePattern);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_JTA_SET_JOINT_TYPE_RULE_LP_NAME_PATTERN_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
