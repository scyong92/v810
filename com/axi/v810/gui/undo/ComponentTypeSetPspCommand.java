/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author zee-foo.hwee
 */
public class ComponentTypeSetPspCommand extends DataCommand
{
    private ComponentType _componentType = null;
    private PspEnum _origPspState = null;
    private PspEnum _newPspState = null;

    /**
     * @author Jack Hwee
     */
    public ComponentTypeSetPspCommand(ComponentType componentType, PspEnum newPspState)
    {
        Assert.expect(componentType != null);
        Assert.expect(newPspState != null);

        _componentType = componentType;
        _newPspState = newPspState;
      //  _origPspState = componentType.getPsp();
        _origPspState = componentType.getComponentTypeSettings().getPsp();
    }

    /**
     * @author Jack Hwee
     */
    public boolean executeCommand() throws XrayTesterException
    {
        Assert.expect(_componentType != null);

        if (_newPspState.equals(_origPspState))
            return false;

        _componentType.getComponentTypeSettings().setPsp(_newPspState);
     //   List<PadType> padTypes = _componentType.getPadTypes();
     //   for (PadType padType : padTypes)
     //       padType.getPadTypeSettings().setPsp(_newPspState);

        return true;
    }

    /**
     * @author Jack Hwee
     */
    public void undoCommand() throws XrayTesterException
    {
        Assert.expect(_componentType != null);

        _componentType.getComponentTypeSettings().setPsp(_origPspState);
      // List<PadType> padTypes = _componentType.getPadTypes();
    //    for (PadType padType : padTypes)
    //         _componentType.getComponentTypeSettings().setPsp(_newPspState);
      //      padType.getPadTypeSettings().setPsp(_origPspState);
    }

    /**
     * @author Jack Hwee
     */
    public String getDescription()
    {
        return StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_GLOBAL_SURFACE_MODEL_KEY");
    }
}
