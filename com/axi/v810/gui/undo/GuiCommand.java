package com.axi.v810.gui.undo;

import java.awt.*;
import java.lang.reflect.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;

/**
 * Any Command that does not modify the state of anything except the GUI itself should
 * extend this class.
 *
 * When a Command is finished that extends this class the CommandManager class will
 * call this classes commandFinished() method to indicate that the GUI has completely updated itself
 * as a result of this Command.
 *
 * This feedback is needed to properly implement
 * undo/redo functionality when the user continuously holds down the undo or redo keys.
 *
 * This method also makes sure that its commands are executed on the Swing event queue thread.
 *
 * @see Command
 * @see DataCommand
 *
 * @author Bill Darbie
 */
public abstract class GuiCommand extends Command
{
  private static GuiObservable _guiObservable;
  private UndoKeystrokeHandler _undoKeystrokeHandler;

  /**
   * @author Bill Darbie
   */
  static
  {
    _guiObservable = GuiObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public boolean execute() throws Exception
  {
    if (EventQueue.isDispatchThread())
    {
      // we are already on the Swing thread, so just execute the command
      return super.execute();
    }
    else
    {
      // since this is a GuiCommand make sure it is on the correct thread
      final BooleanRef executed = new BooleanRef(false);
      final Exception[] exception = new Exception[1];

      try
      {
        SwingUtils.invokeAndWait(new Runnable()
        {
          public void run()
          {
            try
            {
              executed.setValue(GuiCommand.super.execute());
            }
            catch (Exception ex)
            {
              exception[0] = ex;
            }
          }
        });
      }
      catch(InvocationTargetException ite)
      {
        Assert.logException(ite);
      }
      catch(InterruptedException ie)
      {
        Assert.logException(ie);
      }

      if (exception[0] != null)
        throw exception[0];

      return executed.getValue();
    }
  }

  /**
   * @author Bill Darbie
   */
  public void undo() throws Exception
  {
    if (EventQueue.isDispatchThread())
    {
      // we are already on the Swing thread, so just execute the command
      super.undo();
    }
    else
    {
      final Exception[] exception = new Exception[1];

      try
      {
        // since this is a GuiCommand make sure it is on the correct thread
        SwingUtils.invokeAndWait(new Runnable()
        {
          public void run()
          {
            try
            {
              GuiCommand.super.undo();
            }
            catch (Exception ex)
            {
              exception[0] = ex;
            }
          }
        });
      }
      catch(InvocationTargetException ite)
      {
        Assert.logException(ite);
      }
      catch(InterruptedException ie)
      {
        Assert.logException(ie);
      }

      if (exception[0] != null)
        throw exception[0];
    }
  }

  /**
   * The CommandManager class will call this method when the Command is finished.
   * @author Bill Darbie
   */
  public void commandFinished()
  {
    GuiEventEnum guiEventEnum = getGuiEventEnum();
    Object guiEventSource = getGuiEventSource();
    Assert.expect(guiEventEnum != null);
    Assert.expect(guiEventSource != null);

    _guiObservable.stateChanged(guiEventSource, guiEventEnum);

    if (_undoKeystrokeHandler == null)
    {
      _undoKeystrokeHandler = MainMenuGui.getInstance().getMainMenuBar().getUndoKeystrokeHandler();
    }
    _undoKeystrokeHandler.commandFinished();
  }

  /**
   * @author George A. David
   */
  protected abstract Object getGuiEventSource();

  /**
   * @author George A. David
   */
  protected abstract GuiEventEnum getGuiEventEnum();
}
