package com.axi.v810.gui.undo;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * Command for setting a component type coordinate
 * @author Laura Cormos
 */
public class SubtypeSetAlgorithmEnabledCommand extends DataCommand
{
  private Subtype _subtype;
  private AlgorithmEnum _algorithmEnum;
  private boolean _algorithmEnabled;
  private boolean _origValue;

  /**
   * @param subtype The subtype for the tuned value
   * @param algorithmEnum the AlgorithmEnum to be enabled or disabled
   * @param algorithmEnabled the new enable value
   * @author Andy Mechtenberg
   */
  public SubtypeSetAlgorithmEnabledCommand(Subtype subtype, AlgorithmEnum algorithmEnum, boolean algorithmEnabled)
  {
    Assert.expect(subtype != null);
    Assert.expect(algorithmEnum != null);

    _subtype = subtype;
    _algorithmEnum = algorithmEnum;
    _algorithmEnabled = algorithmEnabled;
    _origValue = _subtype.getEnabledAlgorithmEnums().contains(_algorithmEnum);
  }

  /**
   * Add the features to the group
   * @return true if the command was executed
   * @throws XrayTesterException
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_origValue == _algorithmEnabled)
      return false;
    _subtype.setAlgorithmEnabled(_algorithmEnum, _algorithmEnabled);
    return true;
  }

  /**
   * Remove the feature (opposite of add)
   * @throws XrayTesterException
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _subtype.setAlgorithmEnabled(_algorithmEnum, _origValue);
  }

  /**
   * @return a localized description of the command
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SUBTYPE_SET_ALGORITHM_ENABLED_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
