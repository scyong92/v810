package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This project is used store the state of BGA Inspection region size
 * @author Swee Yee
 */
public class ProjectSetBGAInspectionRegionTo1x1Command extends DataCommand
{
  private Project _project = null;
  private boolean _newBGAInspectionRegionSizeSetting = false;
  private boolean _oldBGAInspectionRegionSizeSetting = false;

  /**
   * @param project the project will enable BGA Inspection region size to 1x1
   * @param isBGAInspectionRegionSetTo1X1 the new state of BGA Inspection region size
   * @author Swee Yee
   */
  public ProjectSetBGAInspectionRegionTo1x1Command(Project project, boolean isBGAInspectionRegionSetTo1X1)
  {
    Assert.expect(project != null);

    _project = project;
    _newBGAInspectionRegionSizeSetting = isBGAInspectionRegionSetTo1X1;
    _oldBGAInspectionRegionSizeSetting = _project.isBGAInspectionRegionSetTo1X1();
  }

  /**
   * Change the state of BGA Inspection region size
   * @author Swee Yee
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_oldBGAInspectionRegionSizeSetting == _newBGAInspectionRegionSizeSetting)
      return false;

    _project.setBGAInspectionRegionTo1X1(_newBGAInspectionRegionSizeSetting);
    return true;
  }

  /**
   * Restore the old state of BGA Inspection region size
   * @author Swee Yee
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setBGAInspectionRegionTo1X1(_oldBGAInspectionRegionSizeSetting);
  }

  /**
   * @author Swee Yee
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SET_BGA_INSPECTION_REGION_TO_1X1_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}

