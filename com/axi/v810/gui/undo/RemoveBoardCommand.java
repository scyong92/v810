package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for creating new board instance
 * @author Andy Mechtenberg
 */
public class RemoveBoardCommand extends DataCommand
{
  private Board _boardToDelete;

  /**
   * @author Andy Mechtenberg
   */
  public RemoveBoardCommand(Board board)
  {
    Assert.expect(board != null);

    _boardToDelete = board;
  }

  /**
   * Destroy the board in the panel
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    _boardToDelete.remove();
    return true;
  }

  /**
   * Destroy the board (opposite of create)
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_boardToDelete != null);
    _boardToDelete.add();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_DELETE_BOARD_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
