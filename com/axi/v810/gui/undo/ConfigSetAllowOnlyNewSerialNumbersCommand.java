package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * Command for setting the use serial number to find project config enum
 * @author Laura Cormos
 */
public class ConfigSetAllowOnlyNewSerialNumbersCommand extends DataCommand
{
  private boolean _newEnabledState;
  private boolean _oldEnabledState;
  private SerialNumberManager _serialNumberManager = SerialNumberManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetAllowOnlyNewSerialNumbersCommand(boolean enabledState)
  {
    _newEnabledState = enabledState;
    _oldEnabledState = _serialNumberManager.areOnlyNewSerialNumbersAllowedWhenCadMatchFails();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_oldEnabledState == _newEnabledState)
      return false;

    _serialNumberManager.setAllowOnlyNewSerialNumbersWhenCADMatchFails(_newEnabledState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    _serialNumberManager.setAllowOnlyNewSerialNumbersWhenCADMatchFails(_oldEnabledState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SN_SET_ALLOW_ONLY_NEW_SN_PROJECT_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
