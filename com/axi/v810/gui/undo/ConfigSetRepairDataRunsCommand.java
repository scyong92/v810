package com.axi.v810.gui.undo;

import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the mail server in config is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ConfigSetRepairDataRunsCommand extends DataCommand
{
  private int _originalRepairRuns;
  private int _newRepairRuns;
  private ResultsProcessor _resultsProcessor = ResultsProcessor.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetRepairDataRunsCommand(int newRepairRuns)
  {
    _newRepairRuns = newRepairRuns;
    _originalRepairRuns = _resultsProcessor.getMaximumNumberOfRepairResultsToKeep();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalRepairRuns == _newRepairRuns)
      return false;

    _resultsProcessor.setMaximumNumberOfRepairResultsToKeep(_newRepairRuns);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _resultsProcessor.setMaximumNumberOfRepairResultsToKeep(_originalRepairRuns);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_NUMBER_REPAIR_KEY");
  }
}
