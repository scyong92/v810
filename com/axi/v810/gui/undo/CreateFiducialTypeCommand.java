package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command to create a new fiducial type instance
 * @author Laura Cormos
 */
public class CreateFiducialTypeCommand extends DataCommand
{
  private SideBoardType _sideBoardType = null;
  private String _name = null;
  private BoardCoordinate _boardCoordinate = null;
  private FiducialType _fiducialType = null;

  /**
   * @author Laura Cormos
   */
  public CreateFiducialTypeCommand(SideBoardType sideBoardType, String name, BoardCoordinate boardCoordinate)
  {
    Assert.expect(sideBoardType != null);
    Assert.expect(name != null);
    Assert.expect(boardCoordinate != null);

    _sideBoardType = sideBoardType;
    _name = name;
    _boardCoordinate = boardCoordinate;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_sideBoardType != null);
    Assert.expect(_name != null);
    Assert.expect(_boardCoordinate != null);

    if (_fiducialType == null)
    {
      _fiducialType = _sideBoardType.createFiducialType(_name, _boardCoordinate);
    }
    else
      _fiducialType.add();
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_fiducialType != null);

    _fiducialType.remove();
    return;
  }

  /**
   * @author Laura Cormos
   */
  public FiducialType getFiducialType()
  {
    Assert.expect(_fiducialType != null);
    return _fiducialType;
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_CREATE_FIDUCIAL_TYPE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
