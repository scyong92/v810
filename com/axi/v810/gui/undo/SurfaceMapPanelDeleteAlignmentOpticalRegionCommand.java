/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used to delete OpticalRegion from PanelSurfaceMapSettings
 * Using this class allows us to undo the set later.
 * @author Ying-Huan.Chu
 */
public class SurfaceMapPanelDeleteAlignmentOpticalRegionCommand extends DataCommand
{
  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  private PanelAlignmentSurfaceMapSettings _panelAlignmentSurfaceMapSettings = null;
  private OpticalRegion _alignmentOpticalRegion = null;
  private Project _project = null;
  private String _alignmentGroupName = null;
  
  /**
   * @param panelSurfaceMapSettings the PanelSurfaceMapSettings to have its OpticalRegion removed.
   * @param opticalRegion the opticalRegion to be deleted.
   * @author Ying-Huan.Chu
   */
  public SurfaceMapPanelDeleteAlignmentOpticalRegionCommand(Project project, 
                                                            PanelAlignmentSurfaceMapSettings panelAlignmentSurfaceMapSettings,
                                                            String alignmentGroupName,
                                                            OpticalRegion opticalRegion)
  {
    Assert.expect(project != null);
    Assert.expect(panelAlignmentSurfaceMapSettings != null);
    Assert.expect(opticalRegion != null);
    
    _panelAlignmentSurfaceMapSettings = panelAlignmentSurfaceMapSettings;
    _alignmentOpticalRegion = opticalRegion;
    _project = project;
    _alignmentGroupName = alignmentGroupName;
  }
  
  /**
   * Delete the OpticalCameraRectangle from the OpticalRegion
   * @return true if the command executed
   * @throws XrayTesterException if the command fails
   * @author Ying-Huan.Chu
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_panelAlignmentSurfaceMapSettings != null);
    Assert.expect(_alignmentOpticalRegion != null);
    Assert.expect(_alignmentGroupName != null);
    
    _projectObservable.setEnabled(false);
    try
    {
      _project.getTestProgram().removeOpticalRegion(_alignmentOpticalRegion);
      _panelAlignmentSurfaceMapSettings.removeOpticalRegion(_alignmentGroupName, _alignmentOpticalRegion);
    }
    finally
    {
      _projectObservable.setEnabled(true);
      _projectObservable.stateChanged(this, _alignmentOpticalRegion, null, PanelSurfaceMapEventEnum.REMOVE_ALIGNMENT_SURFACE_MAP_REGION);
    }
    
    return true;
  }

  /**
   * Reset back to the original OpticalRegion
   * @throws XrayTesterException if the command fails
   * @author Ying-Huan.Chu
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_panelAlignmentSurfaceMapSettings != null);
    Assert.expect(_alignmentOpticalRegion != null);
    
    _projectObservable.setEnabled(false);
    try
    {
      _project.getTestProgram().addOpticalRegion(_alignmentOpticalRegion);
      _panelAlignmentSurfaceMapSettings.addOpticalRegion(_alignmentGroupName, _alignmentOpticalRegion);
    }
    finally
    {
      _projectObservable.setEnabled(true);
      _projectObservable.stateChanged(this, null, _alignmentOpticalRegion, PanelSurfaceMapEventEnum.ADD_ALIGNMENT_SURFACE_MAP_REGION);
    }
  }

  /**
   * @author Ying-Huan.Chu
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PSP_DELETE_ALIGNMENT_OPTICAL_REGION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
