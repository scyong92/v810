package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This project is used store the state of generate multi angle image
 * @author bee-hoon.goh
 */
public class ProjectSetEnableGenerateMultiAngleImageCommand extends DataCommand
{
  private Project _project = null;
  private boolean _newGenerateMultiAngleImage = false;
  private boolean _oldGenerateMultiAngleImage = false;

  /**
   * @param project the project will have generate multi angle image generation
   * @param isGenerateMultiAngleImage the new state of generate multi angle image
   * @author bee-hoon.goh
   */
  public ProjectSetEnableGenerateMultiAngleImageCommand(Project project, boolean isGenerateMultiAngleImage)
  {
    Assert.expect(project != null);

    _project = project;
    _newGenerateMultiAngleImage = isGenerateMultiAngleImage;
    _oldGenerateMultiAngleImage = _project.isGenerateMultiAngleImage();
  }

  /**
   * Change the state of generate multi angle image
   * @author bee-hoon.goh
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_oldGenerateMultiAngleImage == _newGenerateMultiAngleImage)
      return false;

    _project.setGenerateMultiAngleImage(_newGenerateMultiAngleImage);
    return true;
  }

  /**
   * Restore the old state of enable generate multi angle image
   * @author bee-hoon.goh
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setGenerateMultiAngleImage(_oldGenerateMultiAngleImage);
  }

  /**
   * @author bee-hoon.goh
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_ENABLE_GENERATE_MULTI_ANGLE_IMAGE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
