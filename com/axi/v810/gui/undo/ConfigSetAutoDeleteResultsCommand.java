package com.axi.v810.gui.undo;

import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the mail server in config is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ConfigSetAutoDeleteResultsCommand extends DataCommand
{
  private boolean _originalAutoDeleteResults;
  private boolean _newAutoDeleteResults;
  private ResultsProcessor _resultsProcessor = ResultsProcessor.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetAutoDeleteResultsCommand(boolean newAutoDeleteResults)
  {
    _newAutoDeleteResults = newAutoDeleteResults;
    _originalAutoDeleteResults = _resultsProcessor.getUsesAutomaticResultDeletion();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalAutoDeleteResults == _newAutoDeleteResults)
      return false;

    _resultsProcessor.setUsesAutomaticResultDeletion(_newAutoDeleteResults);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _resultsProcessor.setUsesAutomaticResultDeletion(_originalAutoDeleteResults);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_AUTO_DELETE_RESULTS_KEY");
  }
}
