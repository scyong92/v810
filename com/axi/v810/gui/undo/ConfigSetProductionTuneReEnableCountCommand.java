package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * Command for setting the production tune re-enable count property
 * @author Andy Mechtenberg
 */
public class ConfigSetProductionTuneReEnableCountCommand extends DataCommand
{
  private ProductionTuneData _productionTuneData = null;
  private int _newReEnableCount;
  private int _oldReEnableCount;

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetProductionTuneReEnableCountCommand(ProductionTuneData productionTuneData,
                                                     int reEnableCount)
  {
    Assert.expect(productionTuneData != null);

    _productionTuneData = productionTuneData;
    _newReEnableCount = reEnableCount;
    _oldReEnableCount = _productionTuneData.getReEnableCount();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);

    _productionTuneData.setReEnableCount(_newReEnableCount);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);

    _productionTuneData.setReEnableCount(_oldReEnableCount);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_PRODUCTION_TUNE_SET_RE_ENABLE_COUNT_KEY");
  }
}
