package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command to create a new component type instance(s)
 * @author Laura Cormos
 */
public class CreateComponentTypeCommand extends DataCommand
{
  private SideBoardType _sideBoardType = null;
  private String _refDes = null;
  private String _landPatternName = null;
  private BoardCoordinate _boardCoordinate;
  private double _rotation;

  private ComponentType _createdComponent;

  /**
   * @author Laura Cormos
   */
  public CreateComponentTypeCommand(SideBoardType sideBoardType, String refDes, LandPattern landPattern,
                                    BoardCoordinate boardCoordinate, double rotation)
  {
    Assert.expect(sideBoardType != null);
    Assert.expect(refDes != null);
    Assert.expect(landPattern != null);
    Assert.expect(boardCoordinate != null);

    _sideBoardType = sideBoardType;
    _refDes = refDes;
    _landPatternName = landPattern.getName();
    _boardCoordinate = boardCoordinate;
    _rotation = rotation;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_sideBoardType != null);
    Assert.expect(_refDes != null);
    Assert.expect(_landPatternName != null);
    Assert.expect(_boardCoordinate != null);

    if (_createdComponent == null)
    {
      // got to find the land pattern object in case it has been deleted and recreated through an undo/redo action
      LandPattern landPattern = _sideBoardType.getBoardType().getPanel().getLandPattern(_landPatternName);
      _createdComponent = _sideBoardType.createComponentType(_refDes, landPattern, _boardCoordinate, _rotation);
    }
    else
      _createdComponent.add();

    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_createdComponent != null);

    _createdComponent.remove();
    return;
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_CREATE_COMPONENT_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
