package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.datastore.config.*;
/**
 * This class is used so that if the xrayAutoShortTermShutdownTimeoutMinutes in software.config is changed,
 * it can be reset via an undo command.
 * @author AnthonyFong
 */
//CR33099-Auto Short Term Shutdown for Xray-AnthonyFong 19-March-2009
public class ConfigSetXrayAutoShortTermShutdownTimeoutCommand extends DataCommand
{
  private double _originalShutdownTimeoutMinutes;
  private double _newShutdownTimeoutMinutes;
  private LegacyXraySource _xraySource = null;
  private HTubeXraySource _hTubeXraySource = null;
  private Config _config = Config.getInstance();

  /**
   * @author AnthonyFong
   */
 public ConfigSetXrayAutoShortTermShutdownTimeoutCommand(double newShutdownTimeoutMinutes)
  {
    _newShutdownTimeoutMinutes = newShutdownTimeoutMinutes;
    if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
    {
        AbstractXraySource xraySource = AbstractXraySource.getInstance();
        Assert.expect(xraySource instanceof LegacyXraySource);
        _xraySource = (LegacyXraySource)xraySource;
        _originalShutdownTimeoutMinutes = _xraySource.getXrayAutoShortTermShutdownTimeoutMinutes();
    }
    else if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
    {
        AbstractXraySource xraySource = AbstractXraySource.getInstance();
        Assert.expect(xraySource instanceof HTubeXraySource);
        _hTubeXraySource = (HTubeXraySource)xraySource;
        _originalShutdownTimeoutMinutes = _hTubeXraySource.getXrayAutoShortTermShutdownTimeoutMinutes();
    }
    else
    {
        _originalShutdownTimeoutMinutes = 15.0;
    }
  }

  /**
   * @author AnthonyFong
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalShutdownTimeoutMinutes == _newShutdownTimeoutMinutes)
      return false;

    Assert.expect(_newShutdownTimeoutMinutes >= 0);
    if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
    {
        _xraySource.setXrayAutoShortTermShutdownTimeoutMinutes(_newShutdownTimeoutMinutes);
    }
    else if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
    {
        _hTubeXraySource.setXrayAutoShortTermShutdownTimeoutMinutes(_newShutdownTimeoutMinutes);
    }
    return true;
  }

  /**
   * @author AnthonyFong
   */
  public void undoCommand() throws XrayTesterException
  {
    if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
    {
        _xraySource.setXrayAutoShortTermShutdownTimeoutMinutes(_originalShutdownTimeoutMinutes);
    }
    else if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
    {
        _hTubeXraySource.setXrayAutoShortTermShutdownTimeoutMinutes(_originalShutdownTimeoutMinutes);
    }
  }

  /**
   * @author AnthonyFong
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_XRAY_AUTO_SHUTDOWN_TIMEOUT_KEY");
  }
}
