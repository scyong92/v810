package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Khaw Chek Hau
 * @XCR-3870: False Call Triggering Feature Enhancement
 */
public class ConfigSetDefectThresholdTypeCommand extends DataCommand
{
  private int _originalValue;
  private int _newValue;
  private FalseCallMonitoring _falseCallMonitoring = FalseCallMonitoring.getInstance();

  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public ConfigSetDefectThresholdTypeCommand(int defectThresholdType)
  {
    _newValue = defectThresholdType;
    _originalValue = _falseCallMonitoring.getDefectThresholdType();
  }

  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalValue == _newValue)
      return false;

    _falseCallMonitoring.setDefectThresholdType(_newValue);
    return true;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public void undoCommand() throws XrayTesterException
  {
    _falseCallMonitoring.setDefectThresholdType(_originalValue);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_FCT_SET_DEFECT_THRESHOLD_TYPE_KEY");
  }
}
