package com.axi.v810.gui.undo;

import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Chong Wei Chin
 */
public class ConfigSetInspectionImageTypeCommand extends DataCommand
{
  private int _originalImageType;
  private int _newImageType;
  private Config _config = Config.getInstance();

  /**
   * @author Wei Chin
   */
  public ConfigSetInspectionImageTypeCommand(int newImageType)
  {
    _newImageType = newImageType;
    _originalImageType = _config.getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT);
  }

  /**
   * @author Chong Wei Chin
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalImageType == _newImageType)
      return false;

    _config.setValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT, new Integer(_newImageType));
    
    return true;
  }

  /**
   * @author Chong Wei Chin
   */
  public void undoCommand() throws XrayTesterException
  {
     _config.setValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT, new Integer(_originalImageType));
  }

  /**
   * @author Chong Wei Chin
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_INSPECTION_IMAGE_TYPE_KEY");
  }
}
