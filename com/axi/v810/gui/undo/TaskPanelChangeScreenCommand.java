package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * This command is used to change screens in the TestDev gui. This
 * allows us to revert to the previous screen via an undo function.
 * @author Andy Mechtenberg
 */
public class TaskPanelChangeScreenCommand extends ScreenChangeGuiCommand
{
  private AbstractEnvironmentPanel _environmentPanel;
  private TaskPanelScreenEnum _newScreen;
  private TaskPanelScreenEnum _origScreen;
  private Object _guiEventSource;

  /**
   * @author Andy Mechtenberg
   */
  public TaskPanelChangeScreenCommand(AbstractEnvironmentPanel environmentPanel, TaskPanelScreenEnum origScreen, TaskPanelScreenEnum newScreen)
  {
    Assert.expect(environmentPanel != null);
    Assert.expect(origScreen != null);
    Assert.expect(newScreen != null);

    _environmentPanel = environmentPanel;
    _origScreen = origScreen;
    _newScreen = newScreen;
  }

  /**
   * @author Andy Mechtenberg
   */
  protected Object getGuiEventSource()
  {
    Assert.expect(_guiEventSource != null);

    return _guiEventSource;
  }

  /**
   * @author Andy Mechtenberg
   */
  protected GuiEventEnum getGuiEventEnum()
  {
    return ViewModeEventEnum.SCREEN;
  }

  /**
   * Switch to the new screen
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_environmentPanel != null);
//    System.out.println("TaskPanelChangeScreenCommand.executeCommand()");
    _guiEventSource = _newScreen;
    if (_origScreen.equals(_newScreen))
      return false;

    _environmentPanel.switchToScreen(_newScreen);

    return true;
  }

  /**
   * restore the original screen
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_environmentPanel != null);
//    System.out.println("TaskPanelChangeScreenCommand.undoCommand()");
    _guiEventSource = _origScreen;
    _environmentPanel.switchToScreen(_origScreen);

  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_TEST_DEV_CHANGE_SCREEN_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
