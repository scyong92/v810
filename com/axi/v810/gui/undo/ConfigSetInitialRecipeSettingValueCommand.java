package com.axi.v810.gui.undo;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * Command for setting an initial recipe setting set value
 *
 * @author weng-jian.eoh
 */
public class ConfigSetInitialRecipeSettingValueCommand extends DataCommand
{

  private RecipeAdvanceSettingEnum _recipeSetting;
  private Object _origValue;
  private Object _newValue;
  private InitialRecipeSetting _initialRecipeSetting = InitialRecipeSetting.getInstance();

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   *
   * @author weng-jian.eoh
   */
  public ConfigSetInitialRecipeSettingValueCommand(RecipeAdvanceSettingEnum recipeSetting, Object newValue)
  {
    Assert.expect(recipeSetting != null);;
    Assert.expect(newValue != null);

    _recipeSetting = recipeSetting;
    _newValue = newValue;
    _origValue = _initialRecipeSetting.getRecipeSettingValue(recipeSetting);
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   *
   * @author weng-jian.eoh
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_newValue.toString().equals(_origValue.toString()))
    {
      return false;
    }
    _initialRecipeSetting.setRecipeSettingValue(_recipeSetting, _newValue);
    return true;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   *
   * @author weng-jian.eoh
   */
  public void undoCommand() throws XrayTesterException
  {
    _initialRecipeSetting.setRecipeSettingValue(_recipeSetting, _origValue);
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   *
   * @author weng-jian.eoh
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_RECIPE_SETTING_SET_VALUE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
