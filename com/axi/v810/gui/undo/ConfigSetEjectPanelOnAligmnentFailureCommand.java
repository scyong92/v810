package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ConfigSetEjectPanelOnAligmnentFailureCommand extends DataCommand
{
  private boolean _originalEjectSetting;
  private boolean _newEjectSetting;
  private TestExecution _testExecution = TestExecution.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetEjectPanelOnAligmnentFailureCommand(boolean newEjectSetting)
  {
    _newEjectSetting = newEjectSetting;
    _originalEjectSetting = _testExecution.shouldPanelBeEjectedIfAlignmentFails();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalEjectSetting == _newEjectSetting)
      return false;

    _testExecution.setEjectPanelIfAlignmentFails(_newEjectSetting);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _testExecution.setEjectPanelIfAlignmentFails(_originalEjectSetting);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_EJECT_ON_FAILURE_KEY");
  }
}
