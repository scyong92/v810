package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;
import com.axi.v810.gui.config.*;

/**
 * Command for setting the ValidateSerialNumbers checkbox
 * @author Laura Cormos
 */
public class ConfigSetPreSelectPanelCommand extends DataCommand
{
  private boolean _newEnabledState;
  private boolean _oldEnabledState;
//  private boolean _matchSerialNumberState;
  private ProductionOptionsSerialNumberTabPanel _parentPanel;
  private SerialNumberManager _serialNumberManager = SerialNumberManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetPreSelectPanelCommand(boolean enabledState,
                                        ProductionOptionsSerialNumberTabPanel parent)
  {
    Assert.expect(parent != null);

    _newEnabledState = enabledState;
    _oldEnabledState = _serialNumberManager.isTestExecPreSelectingProject();
//    _matchSerialNumberState = matchSerialNumberState;
    _parentPanel = parent;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_oldEnabledState == _newEnabledState)
      return false;

//    if (_newEnabledState && _matchSerialNumberState)
//      _parentPanel.checkUserSerialNumberToMatchCheckbox();

    _serialNumberManager.setTestExecPreSelectProject(_newEnabledState);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
//    if (_matchSerialNumberState)
//      _parentPanel.checkUserSerialNumberToMatchCheckbox();

    _serialNumberManager.setTestExecPreSelectProject(_oldEnabledState);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SN_SET_PRE_SELECT_PANEL_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
