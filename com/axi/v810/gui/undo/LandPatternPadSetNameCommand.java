package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting a land pattern pad name
 * @author Laura Cormos
 */
public class LandPatternPadSetNameCommand extends DataCommand
{
  // in this command we don't want to store the actual landpatternpad passed in the constructor because this object
  // can be destroyed and recreated during an undo/redo operation. In that case this command's undo method would
  // operate on nonexistant object. This is why we store the object name and find it every time we need to use it
  private String _landPatternName = null;
  private String _landPatternPadName = null;
  private Panel _panel = null;
  private String _newName = null;
  private String _oldName = null;

  /**
   * @param landPatternPad the land pattern pad whose name will change
   * @param newName the new name
   * @author Laura Cormos
   */
  public LandPatternPadSetNameCommand(LandPatternPad landPatternPad , String newName)
  {
    Assert.expect(landPatternPad != null);
    Assert.expect(newName != null);

    _landPatternName = landPatternPad.getLandPattern().getName();
    _panel = landPatternPad.getLandPattern().getPanel();
    _landPatternPadName = landPatternPad.getName();
    _newName = newName;
    _oldName = landPatternPad.getName();
  }

  /**
   * Change the name to the new name
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_landPatternName != null);
    Assert.expect(_landPatternPadName != null);
    Assert.expect(_panel != null);

    if (_oldName.equals(_newName))
      return false;

    // first find the land pattern object that this land pattern pad belongs to since this may be a redo of the original
    // command and the land pattern could have been deleted and added back.
    LandPattern landPattern = _panel.getLandPattern(_landPatternName);
    Assert.expect(landPattern != null);
    LandPatternPad landPatternPad = landPattern.getLandPatternPad(_landPatternPadName);
    Assert.expect(landPatternPad != null);
    landPatternPad.setName(_newName);
    _landPatternPadName = _newName;
    return true;
  }

  /**
   * Restore the old name
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_landPatternName != null);
    Assert.expect(_landPatternPadName != null);
    Assert.expect(_panel != null);


    // first find the land pattern object that this land pattern pad belongs to since this may be a redo of the original
    // command and the land pattern could have been deleted and added back.
    LandPattern landPattern = _panel.getLandPattern(_landPatternName);
    Assert.expect(landPattern != null);
    LandPatternPad landPatternPad = landPattern.getLandPatternPad(_landPatternPadName);
    Assert.expect(landPatternPad != null);
    landPatternPad.setName(_oldName);
    _landPatternPadName = _oldName;
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_LAND_PATTERN_PAD_SET_NAME_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
