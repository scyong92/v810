package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ConfigEnablePostInspectionScriptCommand extends DataCommand
{
  private boolean _originalEnable;
  private boolean _newEnable;
  private ScriptRunner _scriptRunner = ScriptRunner.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigEnablePostInspectionScriptCommand(boolean newEnable)
  {
    _newEnable = newEnable;
    _originalEnable = _scriptRunner.isPostInspectionScriptEnabled();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalEnable == _newEnable)
      return false;

    _scriptRunner.setPostInspectionScriptEnabled(_newEnable);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _scriptRunner.setPostInspectionScriptEnabled(_originalEnable);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_ENABLE_POST_INSPECTION_COMMAND_KEY");
  }
}
