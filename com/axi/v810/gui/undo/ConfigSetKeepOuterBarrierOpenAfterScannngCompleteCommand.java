package com.axi.v810.gui.undo;

import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 *
 * @author Kok Chun, Tan
 */
public class ConfigSetKeepOuterBarrierOpenAfterScannngCompleteCommand extends DataCommand
{
  private boolean _originalMode;
  private boolean _newMode;
  private PanelHandler _panelHandler = PanelHandler.getInstance();

  /**
   * @author  Kok Chun, Tan
   */
  public ConfigSetKeepOuterBarrierOpenAfterScannngCompleteCommand(boolean newMode)
  {
    _newMode = newMode;
    _originalMode = _panelHandler.isEnabledOuterBarrierOpenAfterCompleteScanningForS2EX();
  }

  /**
   * @author  Kok Chun, Tan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalMode == _newMode)
      return false;

    _panelHandler.setKeepOuterBarrierOpenAfterCompleteScanningForS2EX(_newMode);
    return true;
  }

  /**
   * @author  Kok Chun, Tan
   */
  public void undoCommand() throws XrayTesterException
  {
    _panelHandler.setKeepOuterBarrierOpenAfterCompleteScanningForS2EX(_originalMode);
  }

  /**
   * @author  Kok Chun, Tan
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_ENABLE_OPEN_OUTER_BARRIER_BEFORE_IMAGE_CLASSIFICATION_KEY");
  }
}
