package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ConfigEnablePreInspectionScriptCommand extends DataCommand
{
  private boolean _originalEnable;
  private boolean _newEnable;
  private ScriptRunner _scriptRunner = ScriptRunner.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigEnablePreInspectionScriptCommand(boolean newEnable)
  {
    _newEnable = newEnable;
    _originalEnable = _scriptRunner.isPreInspectionScriptEnabled();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalEnable == _newEnable)
      return false;

    _scriptRunner.setPreInspectionScriptEnabled(_newEnable);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _scriptRunner.setPreInspectionScriptEnabled(_originalEnable);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_ENABLE_PRE_INSPECTION_COMMAND_KEY");
  }
}
