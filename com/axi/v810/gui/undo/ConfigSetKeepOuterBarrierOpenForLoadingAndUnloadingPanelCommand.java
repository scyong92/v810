package com.axi.v810.gui.undo;

import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 *
 * @author Kok Chun, Tan
 */
public class ConfigSetKeepOuterBarrierOpenForLoadingAndUnloadingPanelCommand extends DataCommand
{
  private boolean _originalMode;
  private boolean _newMode;
  private PanelHandler _panelHandler = PanelHandler.getInstance();

  /**
   * @author  Kok Chun, Tan
   */
  public ConfigSetKeepOuterBarrierOpenForLoadingAndUnloadingPanelCommand(boolean newMode)
  {
    _newMode = newMode;
    _originalMode = _panelHandler.isEnabledKeepOuterBarrierOpenForLoadingAndUnloadingPanel();
  }

  /**
   * @author  Kok Chun, Tan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalMode == _newMode)
      return false;

    _panelHandler.setKeepOuterBarrierOpenForLoadingAndUnloadingPanel(_newMode);
    return true;
  }

  /**
   * @author  Kok Chun, Tan
   */
  public void undoCommand() throws XrayTesterException
  {
    _panelHandler.setKeepOuterBarrierOpenForLoadingAndUnloadingPanel(_originalMode);
  }

  /**
   * @author  Kok Chun, Tan
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_ENABLE_REMAIN_OUTER_BARRIER_OPEN_FOR_NEXT_INSPECTION_KEY");
  }
}
