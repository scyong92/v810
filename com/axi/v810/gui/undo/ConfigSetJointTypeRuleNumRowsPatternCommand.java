package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting the joint type assignment rule's number of rows pattern
 * @author Laura Cormos
 */
public class ConfigSetJointTypeRuleNumRowsPatternCommand extends DataCommand
{
  private JointTypeEnumRule _jointTypeEnumRule = null;
  private String _newNumRowsPattern = null;
  private String _oldNumRowsPattern = null;

  /**
   * @author Laura Cormos
   */
  public ConfigSetJointTypeRuleNumRowsPatternCommand(JointTypeEnumRule jointTypeEnumRule , String numRowsPattern)
  {
    Assert.expect(jointTypeEnumRule != null);
    Assert.expect(numRowsPattern != null);

    _jointTypeEnumRule = jointTypeEnumRule;
    _newNumRowsPattern = numRowsPattern;
    if (_jointTypeEnumRule.isNumRowsSet())
      _oldNumRowsPattern = _jointTypeEnumRule.getNumRowsPattern();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    if (_oldNumRowsPattern != null && _oldNumRowsPattern.equals(_newNumRowsPattern))
      return false;

    if (_newNumRowsPattern.length() == 0)
        _jointTypeEnumRule.clearNumRowsPattern();
    else
    _jointTypeEnumRule.setNumRowsPattern(_newNumRowsPattern);

    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    if (_oldNumRowsPattern == null)
      _jointTypeEnumRule.clearNumRowsPattern();
    else
      _jointTypeEnumRule.setNumRowsPattern(_oldNumRowsPattern);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_JTA_SET_JOINT_TYPE_RULE_NUM_ROWS_PATTERN_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
