package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.ProjectHistoryLog;
import com.axi.v810.util.*;

/**
 * This class is used to change the name of a subtype (CustomAlgorithmFamily)
 * Using this class allows us to undo the set later.
 * @author Andy Mechtenberg
 */
public class SubtypeSetNameCommand extends DataCommand
{
  private Subtype _subtype = null;
  private String _origName;
  private String _newName;
  private static transient ProjectHistoryLog _fileWriter;

  /**
   * @param subtype the subtype whose name will change
   * @param newName the new name for the subtype
   * @author Andy Mechtenberg
   */
  public SubtypeSetNameCommand(Subtype subtype, String newName)
  {
    Assert.expect(subtype != null);

    _subtype = subtype;
    _newName = newName;

    _origName = _subtype.getShortName();
    _fileWriter = ProjectHistoryLog.getInstance();
  }

  /**
   * Change the custom algorithm family name (subtype)
   * @return true if the command executed, false if new name is the same as the current one
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_subtype != null);

    if (_newName.equalsIgnoreCase(_origName))
      return false;

    _subtype.setShortName(_newName);

    return true;
  }

  /**
   * Reset the Custom Algorithm Family name (subtype)
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_subtype != null);

    _subtype.setShortName(_origName);
    _fileWriter.appendUndoSubtype(_subtype,_newName, _origName);//keep track subtype undo value -hsia-fen.tan
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SUBTYPE_SET_NAME_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
