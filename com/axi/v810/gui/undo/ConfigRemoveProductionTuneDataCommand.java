package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * Command for setting the serial number pattern type for pattern matching
 * @author Laura Cormos
 */
public class ConfigRemoveProductionTuneDataCommand extends DataCommand
{
  private ProductionTuneData _productionTuneData = null;
  private ProductionTuneManager _productionTuneManager = ProductionTuneManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigRemoveProductionTuneDataCommand(ProductionTuneData productionTuneData)
  {
    Assert.expect(productionTuneData != null);

    _productionTuneData = productionTuneData;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);
    _productionTuneManager.remove(_productionTuneData);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);
    _productionTuneManager.add(_productionTuneData);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_PRODUCTION_TUNE_REMOVE_PROJECT_KEY");
  }
}
