package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ConfigSetPanelSamplingTestPanelsCommand extends DataCommand
{
  private int _originalPanelsTested;
  private int _newPanelsTested;
  private Sampling _sampling = Sampling.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetPanelSamplingTestPanelsCommand(int newPanelsTested)
  {
    _newPanelsTested = newPanelsTested;
    _originalPanelsTested = _sampling.getTestEveryNthPanel();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalPanelsTested == _newPanelsTested)
      return false;

    _sampling.setTestEveryNthPanel(_newPanelsTested);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _sampling.setTestEveryNthPanel(_originalPanelsTested);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_PANEL_SAMPLING_TEST_FREQUENCY_KEY");
  }
}
