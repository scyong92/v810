package com.axi.v810.gui.undo;

/**
 * Extend from this class all GuiCommands that change screens.  We treat these
 * screen changes differently than other GuiCommmands.
 *
 * @author Bill Darbie
 */
public abstract class ScreenChangeGuiCommand extends GuiCommand
{
  /**
   * @author Bill Darbie
   */
  public boolean execute() throws Exception
  {
    boolean stateChanged = super.execute();

    return stateChanged;
  }
}
