package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import javax.swing.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.business.panelSettings.*;

/**
 * This class is used so that if the mail server in config is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ConfigSetProjectDirectoryCommand extends DataCommand
{
  private String _originalProjectDirectory;
  private String _newProjectDirectory;

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetProjectDirectoryCommand(String newProjectDirectory)
  {
    Assert.expect(newProjectDirectory != null);

    _newProjectDirectory = newProjectDirectory;
    _originalProjectDirectory = Directory.getProjectsDir();
  }

  /**
   * @author Andy Mechtenberg
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_newProjectDirectory != null);
    Assert.expect(_originalProjectDirectory != null);

    if (_originalProjectDirectory.equalsIgnoreCase(_newProjectDirectory))
    {
      return false;
    }

    if (isProjectsDirChangeAllowed())
    {
      Directory.setProjectsDir(_newProjectDirectory);
      MainMenuGui.getInstance().updateHomePanel();      //Update recipe list on Main 
    }
    else
      return false;

    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_originalProjectDirectory != null);

    if (isProjectsDirChangeAllowed())
      Directory.setProjectsDir(_originalProjectDirectory);
      MainMenuGui.getInstance().updateHomePanel();   //Update recipe list on Main 
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_PROJECT_DIRECTORY_KEY");
  }

  /**
   * @author Laura Cormos
   */
  private boolean isProjectsDirChangeAllowed()
  {
    if (Project.isCurrentProjectLoaded())
    {
      // if a project is currently loaded, changing the projects directory path can have unpleasant consequences
      // therefore we will ask the user to close the current project and load one again.
      int response = JOptionPane.showConfirmDialog(
          MainMenuGui.getInstance(),
          StringLocalizer.keyToString("CFGUI_MUST_CLOSE_OPEN_PROJECT_MESSAGE_KEY"),
          StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
          JOptionPane.YES_NO_OPTION);
      if (response == JOptionPane.YES_OPTION)
      {
        // if they agree to close the project, do it, then allow changing the path
        MainMenuGui.getInstance().getMainMenuBar().projectClose(false);
      }
      else // if the don't agree, don't allow the path change
        return false;
    }
    return true;
  }
}
