package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting the joint type assignment rule's pad one not at the end of row enabled or disabled
 * @author Laura Cormos
 */
public class ConfigSetJointTypeRulePadOneEnabledCommand extends DataCommand
{
  private JointTypeEnumRule _jointTypeEnumRule = null;
  private BooleanRef _newEnabledState = null;
  private BooleanRef _oldEnabledState = null;

  /**
   * @author Laura Cormos
   */
  public ConfigSetJointTypeRulePadOneEnabledCommand(JointTypeEnumRule jointTypeEnumRule , boolean enabledState)
  {
    Assert.expect(jointTypeEnumRule != null);

    BooleanRef tempState = new BooleanRef(enabledState);
    _jointTypeEnumRule = jointTypeEnumRule;
    _newEnabledState = tempState;
    if (_jointTypeEnumRule.isPadOneInCenterOfRowSet())
      _oldEnabledState = new BooleanRef(jointTypeEnumRule.isPadOneInCenterOfRow());
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    if (_oldEnabledState != null && _oldEnabledState.equals(_newEnabledState))
      return false;

    _jointTypeEnumRule.setPadOneInCenterOfRow(_newEnabledState.getValue());
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_jointTypeEnumRule != null);

    if (_oldEnabledState == null)
      _jointTypeEnumRule.clearPadOneInCenterOfRow();
    else
      _jointTypeEnumRule.setPadOneInCenterOfRow(_oldEnabledState.getValue());
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_JTA_SET_JOINT_TYPE_RULE_PAD1_ENABLED_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
