package com.axi.v810.gui.undo;

import java.util.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used to set the algorithm family for a CompPackage
 * Using this class allows us to undo the set later.
 * @author Andy Mechtenberg
 */
public class CompPackageSetJointTypeCommand extends DataCommand
{
  private CompPackage _compPackage = null;
  private List<ComponentType> _componentsUsingOldPackage;
  private JointTypeEnum _originalJointTypeEnum = null;
  private JointTypeEnum _newJointTypeEnum = null;
  
  // [XCR-2059] - Ying-Huan.Chu
  private boolean _carryForwardSettings = false;
  private Map<String, java.util.List<Subtype>> _origCompRefDesAndSubtypesMap;

  /**
   * @param compPackage the package whose algorithm family is changing
   * @param newJointTypeEnum the new algorithm family
   * @author Andy Mechtenberg
   */
  public CompPackageSetJointTypeCommand(CompPackage compPackage, JointTypeEnum newJointTypeEnum)
  {
    Assert.expect(compPackage != null);
    Assert.expect(newJointTypeEnum != null);

    _compPackage = compPackage;
    _componentsUsingOldPackage = _compPackage.getComponentTypes();
    _newJointTypeEnum = newJointTypeEnum;

    _originalJointTypeEnum = _compPackage.getJointTypeEnum();
  }
  
  /**
   * @param compPackage the package whose algorithm family is changing
   * @param newJointTypeEnum the new algorithm family
   * @author Andy Mechtenberg
   */
  public CompPackageSetJointTypeCommand(CompPackage compPackage, JointTypeEnum newJointTypeEnum, boolean carryForwardSettings)
  {
    Assert.expect(compPackage != null);
    Assert.expect(newJointTypeEnum != null);

    _compPackage = compPackage;
    _componentsUsingOldPackage = _compPackage.getComponentTypes();
    _newJointTypeEnum = newJointTypeEnum;

    _originalJointTypeEnum = _compPackage.getJointTypeEnum();
    _carryForwardSettings = carryForwardSettings;
    if (_carryForwardSettings)
    {
      //_origSubtypes = new ArrayList<>();
      _origCompRefDesAndSubtypesMap = new LinkedHashMap<>();
      for (ComponentType componentType : _compPackage.getComponentTypes())
      {
        _origCompRefDesAndSubtypesMap.put(componentType.getReferenceDesignator(), componentType.getSubtypes());
      }
    }
  }

  /**
   * Change the algorithm family for a package
   * @return true if the command executed, false if new algorithm family is the same as the current one
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_compPackage != null);

    if (_newJointTypeEnum.equals(_originalJointTypeEnum))
      return false;

    for (ComponentType componentType : _componentsUsingOldPackage)
    {
      componentType.setJointTypeEnum(_newJointTypeEnum);
      if (_carryForwardSettings)
      {
        for (Map.Entry<String, java.util.List<Subtype>> entry : _origCompRefDesAndSubtypesMap.entrySet())
        {
          String refDes = entry.getKey();
          java.util.List<Subtype> origSubtypes = entry.getValue();
          if (refDes.equalsIgnoreCase(componentType.getReferenceDesignator()))
          {
            for (Subtype subtype : componentType.getSubtypes())
            {
              for (Subtype origSubtype : origSubtypes)
              {
                if (origSubtype.getShortName().startsWith(subtype.getShortName()))
                {
                  //To copy the settings to new subtype
                  if(origSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false)
                    subtype.getSubtypeAdvanceSettings().setArtifactCompensationState(origSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState());

                  subtype.getSubtypeAdvanceSettings().setCanUseVariableMagnification(origSubtype.getSubtypeAdvanceSettings().canUseVariableMagnification());
                  subtype.getSubtypeAdvanceSettings().setIsInterferenceCompensatable(origSubtype.getSubtypeAdvanceSettings().isIsInterferenceCompensatable());
                  subtype.getSubtypeAdvanceSettings().setMagnificationType(origSubtype.getSubtypeAdvanceSettings().getMagnificationType());
                  subtype.getSubtypeAdvanceSettings().setSignalCompensation(origSubtype.getSubtypeAdvanceSettings().getSignalCompensation());
                  subtype.getSubtypeAdvanceSettings().setUserGain(origSubtype.getSubtypeAdvanceSettings().getUserGain());
                  subtype.getSubtypeAdvanceSettings().setStageSpeedEnum(origSubtype.getSubtypeAdvanceSettings().getStageSpeedList());
                }
              }
            }
          }
        }
      }
    }
//    _compPackage.setJointTypeEnum(_newJointTypeEnum);
    return true;
  }

  /**
   * Reset the algorithm family for a package
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_compPackage != null);

    for(ComponentType componentType : _componentsUsingOldPackage)
    {
      componentType.setJointTypeEnum(_originalJointTypeEnum);
      if (_carryForwardSettings)
      {
        for (Map.Entry<String, java.util.List<Subtype>> entry : _origCompRefDesAndSubtypesMap.entrySet())
        {
          String refDes = entry.getKey();
          java.util.List<Subtype> origSubtypes = entry.getValue();
          if (refDes.equalsIgnoreCase(componentType.getReferenceDesignator()))
          {
            for (Subtype subtype : componentType.getSubtypes())
            {
              if (_carryForwardSettings)
              {
                  for (Subtype origSubtype : origSubtypes)
                  {
                    if (origSubtype.getShortName().startsWith(subtype.getShortName()))
                    {
                      //To copy the settings to new subtype
                      if(origSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false)
                        subtype.getSubtypeAdvanceSettings().setArtifactCompensationState(origSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState());

                      subtype.getSubtypeAdvanceSettings().setCanUseVariableMagnification(origSubtype.getSubtypeAdvanceSettings().canUseVariableMagnification());
                      subtype.getSubtypeAdvanceSettings().setIsInterferenceCompensatable(origSubtype.getSubtypeAdvanceSettings().isIsInterferenceCompensatable());
                      subtype.getSubtypeAdvanceSettings().setMagnificationType(origSubtype.getSubtypeAdvanceSettings().getMagnificationType());
                      subtype.getSubtypeAdvanceSettings().setSignalCompensation(origSubtype.getSubtypeAdvanceSettings().getSignalCompensation());
                      subtype.getSubtypeAdvanceSettings().setUserGain(origSubtype.getSubtypeAdvanceSettings().getUserGain());
                      subtype.getSubtypeAdvanceSettings().setStageSpeedEnum(origSubtype.getSubtypeAdvanceSettings().getStageSpeedList());
                    }
                  }
              }
            }
          }
        }
      }
    }
//    _compPackage.setJointTypeEnum(_originalJointTypeEnum);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_COMP_PACKAGE_SET_JOINT_TYPE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
