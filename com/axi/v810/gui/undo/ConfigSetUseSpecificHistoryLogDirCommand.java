/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Khaw Chek Hau
 * XCR3178: Detail and organize finetuning history log
 */
public class ConfigSetUseSpecificHistoryLogDirCommand extends DataCommand
{
  private boolean _originalUseSpecificHistoryLogDir;
  private boolean _newUseSpecificHistoryLogDir;
  private TestExecution _testExecution = TestExecution.getInstance();

  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public ConfigSetUseSpecificHistoryLogDirCommand(boolean newUseSpecificHistoryLogDir)
  {
    _newUseSpecificHistoryLogDir = newUseSpecificHistoryLogDir;
    _originalUseSpecificHistoryLogDir = _testExecution.isUseSpecificHistoryLogDir();
  }

  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalUseSpecificHistoryLogDir == _newUseSpecificHistoryLogDir)
      return false;

    _testExecution.setUseSpecificHistoryLogDir(_newUseSpecificHistoryLogDir);
    ProjectHistoryLog.getInstance().closeHistoryLog();
    ProjectHistoryLog.getInstance().openHistoryLogIfNecessary();
    return true;
  }

  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public void undoCommand() throws XrayTesterException
  {
   _testExecution.setUseSpecificHistoryLogDir(_originalUseSpecificHistoryLogDir);
    ProjectHistoryLog.getInstance().closeHistoryLog();
    ProjectHistoryLog.getInstance().openHistoryLogIfNecessary();
  }

  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_USE_SPECIFIC_HISTORY_LOG_DIR_PROJECT_KEY");
  }
  
}
