package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command to create a new land pattern on the panel
 * @author Laura Cormos
 */
public class CreateLandPatternCommand extends DataCommand
{
  private Panel _panel = null;
  private String _name = null;
  private LandPattern _landPattern;
  private boolean _isLandPatternFirstPadNeedToBeCreated;

  /**
   * @author Laura Cormos
   */
  public CreateLandPatternCommand(Panel panel, String name, boolean isLandPatternFirstPadNeedToBeCreated)
  {
    Assert.expect(panel != null);
    Assert.expect(name != null);

    _panel = panel;
    _name = name;
    _isLandPatternFirstPadNeedToBeCreated = isLandPatternFirstPadNeedToBeCreated;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_panel != null);
    Assert.expect(_name != null);

    if (_landPattern == null)
    {
      _landPattern = _panel.createLandPattern(_name);
      
      if(_isLandPatternFirstPadNeedToBeCreated == true)
      {
         // we cannot allow an empty landpattern so we'll add a token pad with a new landpattern. But one get one free.
         _landPattern.createSurfaceMountLandPatternPad(new ComponentCoordinate(0, 0), 0.0, 508000, 508000, ShapeEnum.CIRCLE);
      }
    }
    else
      _landPattern.add();
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_landPattern != null);

    _landPattern.remove();
    return;
  }


  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_CREATE_LANDPATTERN_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
