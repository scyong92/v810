package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used to set the artifact compensation state for a ComponentType
 * Using this class allows us to undo the set later.
 * @author Laura Cormos
 */
public class PadTypeSetIntegrationLevelCommand extends DataCommand
{
  private PadType _padType = null;
  private SignalCompensationEnum _origIntegrationLevel = null;
  private SignalCompensationEnum _newIntegrationLevel = null;

  /**
   * Construct the class.
   * @author Laura Cormos
   */
  public PadTypeSetIntegrationLevelCommand(PadType padType, SignalCompensationEnum newState)
  {
    Assert.expect(padType != null);
    Assert.expect(newState != null);

    _padType = padType;
    _newIntegrationLevel = newState;
    _origIntegrationLevel = _padType.getPadTypeSettings().getSignalCompensation();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_padType != null);

    if (_newIntegrationLevel.equals(_origIntegrationLevel))
      return false;

    _padType.getPadTypeSettings().setSignalCompensation(_newIntegrationLevel);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_padType != null);

    _padType.getPadTypeSettings().setSignalCompensation(_origIntegrationLevel);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_PAD_TYPE_SET_INTEGRATION_LEVEL_KEY");
  }
}
