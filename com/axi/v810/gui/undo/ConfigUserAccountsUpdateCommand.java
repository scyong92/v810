package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.util.*;

/**
 * Command to update an existing user acount
 * @author George Booth
 */
public class ConfigUserAccountsUpdateCommand extends DataCommand
{
  private UserAccountsManager _userAccountsManager = UserAccountsManager.getInstance();
  private UserAccountInfo _originalAccount = null;
  private UserAccountInfo _updatedAccount = null;

  /**
   * @author George Booth
   */
  public ConfigUserAccountsUpdateCommand(UserAccountInfo originalAccount, UserAccountInfo updatedAccount)
  {
    Assert.expect(originalAccount != null);
    Assert.expect(updatedAccount != null);

    _originalAccount = originalAccount;
    _updatedAccount = updatedAccount;
  }

  /**
   * @author George Booth
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_originalAccount != null);
    Assert.expect(_updatedAccount != null);

    _userAccountsManager.updateAccount(_originalAccount, _updatedAccount);
    return true;
  }

  /**
   * @author George Booth
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_originalAccount != null);
    Assert.expect(_updatedAccount != null);

    _userAccountsManager.updateAccount(_updatedAccount, _originalAccount);
  }

  /**
   * @author George Booth
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_UPDATE_USER_ACCOUNT_KEY");
  }
}
