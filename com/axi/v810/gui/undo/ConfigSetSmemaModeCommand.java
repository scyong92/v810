package com.axi.v810.gui.undo;

import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the mail server in config is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ConfigSetSmemaModeCommand extends DataCommand
{
  private boolean _originalSmemaMode;
  private boolean _newSmemaMode;
  private PanelHandler _panelHandler = PanelHandler.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetSmemaModeCommand(boolean newSmemaMode)
  {
    _newSmemaMode = newSmemaMode;
    _originalSmemaMode = _panelHandler.isInLineModeEnabled();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalSmemaMode == _newSmemaMode)
      return false;

    _panelHandler.setInLineModeEnabled(_newSmemaMode);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _panelHandler.setInLineModeEnabled(_originalSmemaMode);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_SMEMA_MODE_KEY");
  }
}
