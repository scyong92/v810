package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * XCR2285: 2D Alignment on v810
 * This class is used so that if the alignment method is changed,
 * it can be reset via an undo command.
 * @author Khaw Chek Hau
 */
public class ProjectSetAlignmentMethodCommand extends DataCommand
{
  private Project _project = null;
  private boolean _isUsing2DAlignment;

  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public ProjectSetAlignmentMethodCommand(Project project, boolean isUsing2DAlignment)
  {
    Assert.expect(project != null);
    _project = project;
    _isUsing2DAlignment = isUsing2DAlignment;
  }

  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public boolean executeCommand() throws XrayTesterException
  { 
    _project.getPanel().getPanelSettings().setUsing2DAlignmentMethod(_isUsing2DAlignment);
    return true;
  }

  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void undoCommand() throws XrayTesterException
  {    
    boolean isUsing2DAlignment = !_isUsing2DAlignment;
    _project.getPanel().getPanelSettings().setUsing2DAlignmentMethod(isUsing2DAlignment);
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_ALIGNMENT_METHOD_KEY");
  }
}
