package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This project is used set project thickness table version
 * @author Swee Yee
 */
public class ProjectSetProjectThicknessTableVersionCommand extends DataCommand
{
  private Project _project = null;
  private int _newProjectThicknessTableVersion = 0;
  private int _oldProjectThicknessTableVersion = 0;

  /**
   * @param project
   * @param projectThicknessTableVersion the new version of thickness table to be used in this project
   * @author Swee Yee
   */
  public ProjectSetProjectThicknessTableVersionCommand(Project project, int projectThicknessTableVersion)
  {
    Assert.expect(project != null);

    _project = project;
    _newProjectThicknessTableVersion = projectThicknessTableVersion;
    _oldProjectThicknessTableVersion = _project.getThicknessTableVersion();
  }

  /**
   * Change the version of thickness table to be used in this project
   * @author Swee Yee
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_oldProjectThicknessTableVersion == _newProjectThicknessTableVersion)
      return false;

    try
    {
      _project.setThicknessTableVersion(_newProjectThicknessTableVersion);
      // backup current project thickness table
      if (FileUtil.exists(Directory.getProjectSolderThicknessDir(_project.getName())))
      {
        FileUtilAxi.rename(Directory.getProjectSolderThicknessDir(_project.getName()), Directory.getProjectSolderThicknessBackupDir(_project.getName()));
      }
      //swee yee wong - XCR-3004 Load recipe assert java.lang.NullPointerException
      //swee yee wong - XCR-3046 Alignment is cleared if attempt to unload and reload recipe after run production
      boolean notifyUserAlternativeVersion = false;
      _project.isProjectSolderThicknessFilesAvailable(notifyUserAlternativeVersion);
    }
    catch (DatastoreException de)
    {
      _project.setThicknessTableVersion(_oldProjectThicknessTableVersion);
      if (FileUtil.exists(Directory.getProjectSolderThicknessBackupDir(_project.getName())))
      {
        FileUtilAxi.rename(Directory.getProjectSolderThicknessBackupDir(_project.getName()), Directory.getProjectSolderThicknessDir(_project.getName()));
      }
      throw de;
    }
    return true;
  }

  /**
   * Restore the old version of thickness table to be used in this project
   * @author Swee Yee
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setThicknessTableVersion(_oldProjectThicknessTableVersion);
    //swee yee wong - XCR-3004 Load recipe assert java.lang.NullPointerException
    //swee yee wong - XCR-3046 Alignment is cleared if attempt to unload and reload recipe after run production
    boolean notifyUserAlternativeVersion = false;
    _project.isProjectSolderThicknessFilesAvailable(notifyUserAlternativeVersion);
  }

  /**
   * @author Swee Yee
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_PROJECT_THICKNESS_TABLE_VERSION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
