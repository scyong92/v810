package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Khaw Chek Hau
 * This class is used so that if the mail server in config is changed,
 * it can be reset via an undo command.
 * XCR3178: Detail and organize finetuning history log
 */
public class ConfigSetHistoryLogDirectoryCommand extends DataCommand
{
  private String _originalHistoryLogDir;
  private String _newHistoryLogDir;

  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public ConfigSetHistoryLogDirectoryCommand(String newHistoryLogDir)
  {
    Assert.expect(newHistoryLogDir != null);

    _newHistoryLogDir = newHistoryLogDir;
    _originalHistoryLogDir = Directory.getHistoryFileDir(newHistoryLogDir);
  }

  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalHistoryLogDir.equalsIgnoreCase(_newHistoryLogDir))
      return false;

    Directory.setSpecificHistoryLogDir(_newHistoryLogDir);
    
    ProjectHistoryLog.getInstance().closeHistoryLog();
    ProjectHistoryLog.getInstance().openHistoryLogIfNecessary();
    
    return true;
  }

  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public void undoCommand() throws XrayTesterException
  {
    Directory.setSpecificHistoryLogDir(_originalHistoryLogDir);
    
    ProjectHistoryLog.getInstance().closeHistoryLog();
    ProjectHistoryLog.getInstance().openHistoryLogIfNecessary();
  }

  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SPECIFIC_HISTORY_LOG_DIRECTORY_KEY");
  }
}
