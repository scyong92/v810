package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.gui.SwingWorkerThread;
import com.axi.v810.gui.mainMenu.MainMenuGui;
import com.axi.v810.util.*;
import com.axi.v810.gui.testDev.*;

/**
 * @author Bill Darbie
 */
public class BoardTypeSetWidthCommand extends DataCommand
{
  private BoardType _boardType;
  private int _oldWidthInNanoMeters;
  private int _newWidthInNanoMeters;
  private Integer _selectedExtendBoardWidthOption;

  /**
   * @author Bill Darbie
   */
  public BoardTypeSetWidthCommand(BoardType boardType, int widthInNanoMeters)
  {
    Assert.expect(boardType != null);

    _boardType = boardType;
    _newWidthInNanoMeters = widthInNanoMeters;
    _oldWidthInNanoMeters = _boardType.getWidthInNanoMeters();
  }

  /**
   * @author Bill Darbie
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_boardType != null);

    if (MathUtil.fuzzyEquals(_oldWidthInNanoMeters, _newWidthInNanoMeters, MathUtil._UNIT_CONVERSION_NANOMETER_LIMIT))
      return false;

    BoardTypeSetWidthOptionDialog boardTypeSetWidthOptionDialog = new BoardTypeSetWidthOptionDialog(MainMenuGui.getInstance(),StringLocalizer.keyToString("MMGUI_SETUP_ADJUST_BOARD_TYPE_WIDTH_TITLE_KEY"), true);
    boardTypeSetWidthOptionDialog.showDialog();
    if (boardTypeSetWidthOptionDialog.userClickedOk() == false)
      return false;

    _selectedExtendBoardWidthOption = boardTypeSetWidthOptionDialog.getSelectedRadioButtonId();
    _boardType.setWidthInNanoMeters(_newWidthInNanoMeters);
    // Iterate all pads inside, and extend the board the other side.
    int widthDifference = _newWidthInNanoMeters - _oldWidthInNanoMeters;
    if (_selectedExtendBoardWidthOption != 0)
    {
      if (_selectedExtendBoardWidthOption == 2)
        widthDifference /= 2;
      final int finalWidthDifference = widthDifference;
      MainMenuGui.getInstance().getTestDev().initProgressDialog(0,_boardType.getComponentTypes().size());
      SwingWorkerThread.getInstance().invokeLater(new Runnable()
      {
        public void run()
        {
          for (ComponentType componentType : _boardType.getComponentTypes())
          {
            BoardCoordinate newComponentCoordinate = componentType.getCoordinateInNanoMeters();
            newComponentCoordinate.setLocation(newComponentCoordinate.getX()+finalWidthDifference, newComponentCoordinate.getY());
            componentType.setCoordinateInNanoMeters(newComponentCoordinate);
          }
        }
      });
      MainMenuGui.getInstance().getTestDev().showProgressDialog(true);
    }
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_boardType != null);
    // Iterate all pads inside, and extend the board the other side.
    int widthDifference = _newWidthInNanoMeters - _oldWidthInNanoMeters;
    if (_selectedExtendBoardWidthOption != 0)
    {
      if (_selectedExtendBoardWidthOption == 2)
        widthDifference /= 2;
      final int finalWidthDifference = widthDifference;
      MainMenuGui.getInstance().getTestDev().initProgressDialog(0,_boardType.getComponentTypes().size());
      SwingWorkerThread.getInstance().invokeLater(new Runnable()
      {
        public void run()
        {
          for (ComponentType componentType : _boardType.getComponentTypes())
          {
            BoardCoordinate newComponentCoordinate = componentType.getCoordinateInNanoMeters();
            newComponentCoordinate.setLocation(newComponentCoordinate.getX()-finalWidthDifference, newComponentCoordinate.getY());
            componentType.setCoordinateInNanoMeters(newComponentCoordinate);
          }
        }
      });
      MainMenuGui.getInstance().getTestDev().showProgressDialog(true);
    }
    _boardType.setWidthInNanoMeters(_oldWidthInNanoMeters);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_BOARD_TYPE_SET_WIDTH_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
