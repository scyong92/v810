package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Yong Sheng Chuan
 */
public class ProjectSetUseNewScanRouteGenerationCommand extends DataCommand
{
  private Project _project = null;
  private boolean _oldScanRouteGenerationOption= false;
  private boolean _newScanRouteGenerationOption = false;

  /**
   * @author Yong Sheng Chuan
   */
  public ProjectSetUseNewScanRouteGenerationCommand(Project project, boolean isNewScanRouteGeneration)
  {
    Assert.expect(project != null);

    _project = project;
    _newScanRouteGenerationOption = isNewScanRouteGeneration;
    _oldScanRouteGenerationOption = _project.isGenerateByNewScanRoute();
  }

  /**
   * @author Yong Sheng Chuan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_oldScanRouteGenerationOption == _newScanRouteGenerationOption)
      return false;

    _project.setGenerateByNewScanRoute(_newScanRouteGenerationOption);
    return true;
  }

  /**
   * @author Yong Sheng Chuan
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setGenerateByNewScanRoute(_oldScanRouteGenerationOption);
  }

  /**
   * @author Yong Sheng Chuan
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_USE_VELOCITY_MAPPING_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
