package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the customer name in config is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ConfigSetCustomerNameCommand extends DataCommand
{
  private String _originalCustomerName;
  private String _newCustomerName;
  private CustomerInfo _customerInfo = CustomerInfo.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetCustomerNameCommand(String newCustomerName)
  {
    Assert.expect(newCustomerName != null);

    _newCustomerName = newCustomerName;
    _originalCustomerName = _customerInfo.getCustomerName();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalCustomerName.equalsIgnoreCase(_newCustomerName))
      return false;

    _customerInfo.setCustomerName(_newCustomerName);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _customerInfo.setCustomerName(_originalCustomerName);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_CUSTOMER_NAME_KEY");
  }
}
