package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command to swap a land pattern pad's type (SM -> TH or TH -> SM)
 * @author Laura Cormos
 */
public class LandPatternSwapLandPatternPadTypeCommand extends DataCommand
{
  private LandPatternPad _newPad = null;
  private LandPatternPad _originalPad = null;

  /**
   * @author Laura Cormos
   */
  public LandPatternSwapLandPatternPadTypeCommand(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    _originalPad = landPatternPad;
  }

  /**
   * Swap the type
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_originalPad != null);

    String padName = _originalPad.getName();
    if (_newPad == null)
    {
      // this is the first time this command was called, need to create the new pad then remove the original
      if (_originalPad instanceof SurfaceMountLandPatternPad)
      {
        _newPad = _originalPad.getLandPattern().createThroughHoleLandPatternPad(_originalPad.getCoordinateInNanoMeters(),
                                                                                _originalPad.getDegreesRotation(),
                                                                                _originalPad.getLengthInNanoMeters(),
                                                                                _originalPad.getWidthInNanoMeters(),
                                                                                _originalPad.getShapeEnum());
        ((ThroughHoleLandPatternPad)_newPad).setHoleCoordinateInNanoMeters(_originalPad.getCoordinateInNanoMeters());
      }
      else
      {
        _newPad = _originalPad.getLandPattern().createSurfaceMountLandPatternPad(_originalPad.getCoordinateInNanoMeters(),
                                                                                 _originalPad.getDegreesRotation(),
                                                                                 _originalPad.getLengthInNanoMeters(),
                                                                                 _originalPad.getWidthInNanoMeters(),
                                                                                 _originalPad.getShapeEnum());
      }
      _originalPad.remove();
      // can set the name only after the other has been removed since duplicate names are not allowed
      _newPad.setName(padName);
    }
    else
    {
      // this is a redo action, need to remove the original then add the new pad back in
      Assert.expect(_newPad != null);
      Assert.expect(_originalPad != null);

      _originalPad.remove();
      _newPad.add();
    }

    return true;
  }

  /**
   * Restore the original type
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_newPad != null);
    Assert.expect(_originalPad != null);

    _newPad.remove();
    _originalPad.add();
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_LAND_PATTERN_PAD_SWAP_TYPE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
