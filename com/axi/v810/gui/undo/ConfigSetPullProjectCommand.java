package com.axi.v810.gui.undo;

import com.axi.v810.business.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the mail server in config is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ConfigSetPullProjectCommand extends DataCommand
{
  private boolean _originalAutoPull;
  private boolean _newAutoPull;

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetPullProjectCommand(boolean newAutoPull)
  {
    _newAutoPull = newAutoPull;
    _originalAutoPull = ProjectDatabase.isAutoUpdateLocalProjectEnabled();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalAutoPull == _newAutoPull)
      return false;

    ProjectDatabase.setAutoUpdateLocalProject(_newAutoPull);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    ProjectDatabase.setAutoUpdateLocalProject(_originalAutoPull);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_AUTO_PULL_LATEST_PROJECT_KEY");
  }
}
