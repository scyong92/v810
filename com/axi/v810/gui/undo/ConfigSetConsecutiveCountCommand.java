package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Jack Hwee
 */
public class ConfigSetConsecutiveCountCommand extends DataCommand
{
  private int _originalValue;
  private int _newValue;
  private FalseCallMonitoring _falseCallMonitoring = FalseCallMonitoring.getInstance();

  /**
   * @author Jack Hwee
   */
  public ConfigSetConsecutiveCountCommand(int newPanelsTested)
  {
    _newValue = newPanelsTested;
    _originalValue = _falseCallMonitoring.getConsecutiveCount();
  }

  /**
   * @author Jack Hwee
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalValue == _newValue)
      return false;

    _falseCallMonitoring.setConsecutiveCount(_newValue);
    return true;
  }

  /**
   * @author Jack Hwee
   */
  public void undoCommand() throws XrayTesterException
  {
    _falseCallMonitoring.setConsecutiveCount(_originalValue);
  }

  /**
   * @author Jack Hwee
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_PANEL_SAMPLING_TEST_FREQUENCY_KEY");
  }
}
