package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for setting a land pattern pad hole coordinate
 * @author Laura Cormos
 */
public class THLandPatternPadSetHoleCoordinateInNanoMetersCommand extends DataCommand
{
  private ThroughHoleLandPatternPad _landPatternPad = null;
  private ComponentCoordinate _newHoleCoordInNanoMeters = null;
  private ComponentCoordinate _oldHoleCoordInNanoMeters = null;

  /**
   * @param landPatternPad the land pattern pad whose hole coordinate will change
   * @param newHoleCoord the new hole coordinate
   * @author Laura Cormos
   */
  public THLandPatternPadSetHoleCoordinateInNanoMetersCommand(ThroughHoleLandPatternPad landPatternPad , ComponentCoordinate newHoleCoord)
  {
    Assert.expect(landPatternPad != null);
    Assert.expect(newHoleCoord != null);

    _landPatternPad = landPatternPad;
    _newHoleCoordInNanoMeters = newHoleCoord;
    _oldHoleCoordInNanoMeters = landPatternPad.getHoleCoordinateInNanoMeters();
  }

  /**
   * Change the coordinate to the new coordinate
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_landPatternPad != null);
    Assert.expect(_oldHoleCoordInNanoMeters != null);
    Assert.expect(_newHoleCoordInNanoMeters != null);

    if (_oldHoleCoordInNanoMeters.equals(_newHoleCoordInNanoMeters))
      return false;

    _landPatternPad.setHoleCoordinateInNanoMeters(_newHoleCoordInNanoMeters);
    return true;
  }

  /**
   * Restore the old coordinate
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_landPatternPad != null);
    Assert.expect(_oldHoleCoordInNanoMeters != null);
    Assert.expect(_newHoleCoordInNanoMeters != null);

    _landPatternPad.setHoleCoordinateInNanoMeters(_oldHoleCoordInNanoMeters);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_LAND_PATTERN_PAD_SET_HOLE_COORD_NM_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
