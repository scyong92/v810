package com.axi.v810.gui.undo;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author Kee Chin Seong
 */
public class SubtypeSetCameraEnabledListCommand extends DataCommand
{
  private SubtypeAdvanceSettings _subtypeAdvanceSettings = null;
  private List<Integer> _newCameraIdList = null;
  private List<Integer> _oriCameraIdList = null;
  private Project _project = null;
  
  /**
   * Construct the class.
   * @author Kee Chin Seong
   */
  public SubtypeSetCameraEnabledListCommand(Project project, SubtypeAdvanceSettings subtypeAdvanceSettings, List<Integer>cameraIdList)
  {
    Assert.expect(subtypeAdvanceSettings != null);
    Assert.expect(cameraIdList != null);

    _subtypeAdvanceSettings = subtypeAdvanceSettings;
    _newCameraIdList = cameraIdList;
    _oriCameraIdList = subtypeAdvanceSettings.getCameraEnabledIDList();
    _project = project;
  }

  /**
   * @author Kee Chin Seong
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_subtypeAdvanceSettings != null);

    _subtypeAdvanceSettings.setEnabledCamera(_newCameraIdList);
    
    setReconstructionRegionEnabledCameraList();

    return true;
  }
  
  /**
   * @author Kee Chin Seong
   */
  private void setReconstructionRegionEnabledCameraList()
  {
    for(com.axi.v810.business.panelDesc.ComponentType comType : _subtypeAdvanceSettings.getSubtype().getComponentTypes())
    {
        for(com.axi.v810.business.panelDesc.Component comp : comType.getComponents())
        {
           java.util.List<com.axi.v810.business.testProgram.ReconstructionRegion> reconstructionRegions = _project.getTestProgram().getInspectionRegions(comp);
           for(com.axi.v810.business.testProgram.ReconstructionRegion region : reconstructionRegions)
           {
               if(region.getSubtypes().contains(_subtypeAdvanceSettings.getSubtype()))
                   region.setEnabledCamera(_newCameraIdList);
           }
        }
        
    }
  }

  /**
   * @author Kee Chin Seong
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_subtypeAdvanceSettings != null);

    _subtypeAdvanceSettings.setEnabledCamera(_oriCameraIdList);
    
    setReconstructionRegionEnabledCameraList();
  }

  /**
   * @author Kee Chin Seong
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_COMPONENT_TYPE_SET_STAGE_SPEED_KEY");
  }
}
