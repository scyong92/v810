package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author chin-seong.kee
 */
public class SubtypeAdvanceSettingsSetIntegrationLevelCommand extends DataCommand
{
  private SubtypeAdvanceSettings _subtypeAdvanceSettings = null;
  private SignalCompensationEnum _origIntegrationLevel = null;
  private SignalCompensationEnum _newIntegrationLevel = null;

  /**
   * Construct the class.
   * @author Chin Seong
   */
  public SubtypeAdvanceSettingsSetIntegrationLevelCommand(SubtypeAdvanceSettings subtypeAdvanceSettings, SignalCompensationEnum newState)
  {
    Assert.expect(subtypeAdvanceSettings != null);
    Assert.expect(newState != null);

    _subtypeAdvanceSettings = subtypeAdvanceSettings;
    _newIntegrationLevel = newState;
    _origIntegrationLevel = _subtypeAdvanceSettings.getSignalCompensation();
  }

  /**
   * @author  Chin Seong
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_subtypeAdvanceSettings != null);

    if (_newIntegrationLevel.equals(_origIntegrationLevel))
      return false;

    _subtypeAdvanceSettings.setSignalCompensation(_newIntegrationLevel);
    return true;
  }

  /**
   * @author  Chin Seong
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_subtypeAdvanceSettings != null);

    _subtypeAdvanceSettings.setSignalCompensation(_origIntegrationLevel);   
  }

  /**
   * @author Chin Seong
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_ADVANCE_SETTINGS_SET_INTEGRATION_LEVEL_KEY");
  }
}
