package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.util.*;

/**
 * This class is used to set the usability of OpticalCameraRectangle (Toggle Rectangle)
 * Using this class allows us to undo the set later.
 * @author Ying-Huan.Chu
 */
public class SurfaceMapBoardToggleOpticalCameraRectangleCommand extends DataCommand
{
  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  private OpticalCameraRectangle _opticalCameraRectangle = null;
  private BooleanRef _originalOpticalCameraRectangleInspectableBool = null;
  private BooleanRef _newOpticalCameraRectangleInspectableBool = null;
  
  private java.util.List<Board> _previousBoardSelection = null;

  /**
   * @param opticalCameraRectangle the OpticalCameraRectangle to change
   * @param inspectableBool the new inspectable boolean to apply on the OpticalCameraRectangle
   * @author Ying-Huan.Chu
   */
  public SurfaceMapBoardToggleOpticalCameraRectangleCommand(OpticalCameraRectangle opticalCameraRectangle, BooleanRef inspectableBool)
  {
    Assert.expect(opticalCameraRectangle != null);
    Assert.expect(inspectableBool != null);

    _opticalCameraRectangle = opticalCameraRectangle;
    _newOpticalCameraRectangleInspectableBool = inspectableBool;
    _originalOpticalCameraRectangleInspectableBool = new BooleanRef(opticalCameraRectangle.getInspectableBool());
    
    _previousBoardSelection = PspAutoPopulatePanel.getInstance().getBoardsToApplyOpticalCameraRectangles();
  }

  /**
   * Change the inspectable boolean to apply on the OpticalCameraRectangle
   * @return true if the command executed, false if new inspectable boolean is the same as the current one
   * @throws XrayTesterException if the command fails
   * @author Ying-Huan.Chu
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_opticalCameraRectangle != null);

    if (_newOpticalCameraRectangleInspectableBool.equals(_originalOpticalCameraRectangleInspectableBool))
      return false;
    
    _projectObservable.setEnabled(false);
    try
    {
      _opticalCameraRectangle.setInspectableBool(_newOpticalCameraRectangleInspectableBool.getValue());
    }
    finally
    {
      _projectObservable.setEnabled(true);
      _projectObservable.stateChanged(this, _opticalCameraRectangle, _opticalCameraRectangle, BoardSurfaceMapEventEnum.TOGGLE_OPTICAL_CAMERA_RECTANGLE);
    }
    return true;
  }

  /**
   * Reset the usability status of OpticalCameraRectangle
   * @throws XrayTesterException if the command fails
   * @author Ying-Huan.Chu
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_opticalCameraRectangle != null);
    
    _projectObservable.setEnabled(false);
    try
    {
      _opticalCameraRectangle.setInspectableBool(_originalOpticalCameraRectangleInspectableBool.getValue());
    }
    finally
    {
      _projectObservable.setEnabled(true);
      _projectObservable.stateChanged(this, _opticalCameraRectangle, _opticalCameraRectangle, BoardSurfaceMapEventEnum.TOGGLE_OPTICAL_CAMERA_RECTANGLE);
      PspAutoPopulatePanel.getInstance().setBoardsToApplyOpticalCameraRectangle(_previousBoardSelection);
    }
  }

  /**
   * @author Ying-Huan.Chu
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_TOGGLE_OPTICAL_CAMERA_RECTANGLE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
