package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * Command for setting the serial number pattern type for pattern matching
 * @author Laura Cormos
 */
public class ConfigSetSerialNumberProjectNameCommand extends DataCommand
{
  private SerialNumberRegularExpressionData _serialNumberRegExData = null;
  private String _newProjectName = null;
  private String _oldProjectName = null;

  /**
   * @author Laura Cormos
   */
  public ConfigSetSerialNumberProjectNameCommand(SerialNumberRegularExpressionData serialNumberRegExData,
                                                 String projectName)
  {
    Assert.expect(serialNumberRegExData != null);
    Assert.expect(projectName != null);

    _serialNumberRegExData = serialNumberRegExData;
    _newProjectName = projectName;
    _oldProjectName = _serialNumberRegExData.getProjectName();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_serialNumberRegExData != null);

    _serialNumberRegExData.setProjectName(_newProjectName);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_serialNumberRegExData != null);

    _serialNumberRegExData.setProjectName(_oldProjectName);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SN_SET_PROJECT_NAME_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
