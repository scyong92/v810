package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author Kok Chun, Tan
 */
public class ConfigSetPreSelectVariationCommand extends DataCommand
{
  private boolean _newEnabledState;
  private boolean _oldEnabledState;
  private Project _project;

  /**
   * @author Kok Chun, Tan
   */
  public ConfigSetPreSelectVariationCommand(boolean enabledState,
                                                Project project)
  {
    Assert.expect(project != null);

    _project = project;
    _newEnabledState = enabledState;
    _oldEnabledState = _project.enablePreSelectVariation();
  }

  /**
   * @author Kok Chun, Tan
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_oldEnabledState == _newEnabledState)
      return false;

    _project.setEnablePreSelectVariation(_newEnabledState);
    return true;
  }

  /**
   * @author Kok Chun, Tan
   */
  public void undoCommand() throws XrayTesterException
  {
    _project.setEnablePreSelectVariation(_oldEnabledState);
  }

  /**
   * @author Kok Chun, Tan
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_SN_SET_PRE_SELECT_VARIATION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
