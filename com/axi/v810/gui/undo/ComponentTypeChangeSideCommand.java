package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for changing a component type's side
 * @author Laura Cormos
 */
public class ComponentTypeChangeSideCommand extends DataCommand
{
  private ComponentType _componentType = null;

  /**
   * @author Laura Cormos
   */
  public ComponentTypeChangeSideCommand(ComponentType componentType)
  {
    Assert.expect(componentType != null);

    _componentType = componentType;
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    _componentType.changeSide();
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_componentType != null);

    _componentType.changeSide();
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_COMPONENT_TYPE_CHANGE_SIDE_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
