package com.axi.v810.gui.undo;

import com.axi.util.Assert;
import com.axi.util.LocalizedString;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.BoardSettings;
import com.axi.v810.util.StringLocalizer;
import com.axi.v810.util.XrayTesterException;

/**
 *
 * @author Goh Bee Hoon
 */
public class BoardSetZOffsetCommand extends DataCommand
{
  private BoardSettings _boardSettings;
  private int _oldOffsetInNanometers;
  private int _newOffsetInNanometers;

  /**
   * @author Goh Bee Hoon
   */
  public BoardSetZOffsetCommand(BoardSettings boardSettings, int zOffset)
  {
    Assert.expect(boardSettings != null);

    _boardSettings = boardSettings;
    _newOffsetInNanometers = zOffset;
    _oldOffsetInNanometers = _boardSettings.getBoardZOffsetInNanometers();
  }

  /**
   * @author Goh Bee Hoon
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_boardSettings!= null);

    if (_oldOffsetInNanometers == _newOffsetInNanometers)
      return false;

    _boardSettings.setBoardZOffsetInNanometers(_newOffsetInNanometers);
    return true;
  }

  /**
   * @author Goh Bee Hoon
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_boardSettings != null);
    _boardSettings.setBoardZOffsetInNanometers(_oldOffsetInNanometers);
  }

  /**
   * @author Goh Bee Hoon
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_BOARD_SET_Z_OFFSET_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
