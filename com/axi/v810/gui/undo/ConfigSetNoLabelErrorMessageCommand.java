package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class sets/unsets the NO LABEL error message for the barcode reader configuration
 * @author Laura Cormos
 */
public class ConfigSetNoLabelErrorMessageCommand extends DataCommand
{
  private String _originalMessage;
  private String _newMessage;
  private BarcodeReaderManager _bcrManager = BarcodeReaderManager.getInstance();

  /**
   * @author Laura Cormos
   */
  public ConfigSetNoLabelErrorMessageCommand(String portNumber)
  {
    Assert.expect(portNumber != null);

    _newMessage = portNumber;
    _originalMessage = _bcrManager.getBarcodeReaderNoLabelMessage();
  }

  /**
   * @author Laura Cormos
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_newMessage != null);

    if (_newMessage.equals(_originalMessage))
      return false;

    _bcrManager.setBarcodeReaderNoLabelMessage(_newMessage);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_originalMessage != null);

    _bcrManager.setBarcodeReaderNoLabelMessage(_originalMessage);
  }

  /**
   * @author Laura Cormos
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_NO_LABEL_ERROR_MESSAGE_KEY");
  }
}
