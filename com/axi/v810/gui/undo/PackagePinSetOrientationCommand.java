package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class is used to set the orientation for a package pin
 * Using this class allows us to undo the set later.
 * @author Andy Mechtenberg
 */
public class PackagePinSetOrientationCommand extends DataCommand
{
  private PackagePin _packagePin = null;
  private PinOrientationAfterRotationEnum _originalPadOrientationEnum = null;
  private PinOrientationAfterRotationEnum _newPadOrientationEnum = null;

  /**
   * @param packagePin the package pin to change
   * @param newPadOrientationEnum the new orientation
   * @author Andy Mechtenberg
   */
  public PackagePinSetOrientationCommand(PackagePin packagePin, PinOrientationAfterRotationEnum newPadOrientationEnum)
  {
    Assert.expect(packagePin != null);
    Assert.expect(newPadOrientationEnum != null);

    _packagePin = packagePin;
    _newPadOrientationEnum = newPadOrientationEnum;
    _originalPadOrientationEnum = _packagePin.getPinOrientationAfterRotationEnum();
  }

  /**
   * Change the orientation for the package pin
   * @return true if the command executed, false if new algorithm family is the same as the current one
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_packagePin != null);

    if (_newPadOrientationEnum.equals(_originalPadOrientationEnum))
      return false;

    _packagePin.setPinOrientationAfterRotationEnum(_newPadOrientationEnum);
    return true;
  }

  /**
   * Reset the length of the panel
   * @throws XrayTesterException if the command fails
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_packagePin != null);

    _packagePin.setPinOrientationAfterRotationEnum(_originalPadOrientationEnum);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PACKAGE_PIN_SET_ORIENTATION_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
