package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This project is used so if the units are chanaged, they
 * can be restored via an undo function.
 * @author George A. David
 */
public class ProjectSetDisplayUnitsCommand extends DataCommand
{
  private Project _project = null;
  private MathUtilEnum _newDisplayUnits = null;
  private MathUtilEnum _oldDisplayUnits = null;

  /**
   * @param project the project whose units will change
   * @param newDisplayUnits the new display units
   * @author George A. David
   */
  public ProjectSetDisplayUnitsCommand(Project project, MathUtilEnum newDisplayUnits)
  {
    Assert.expect(project != null);
    Assert.expect(newDisplayUnits != null);

    _project = project;
    _newDisplayUnits = newDisplayUnits;
    _oldDisplayUnits = _project.getDisplayUnits();
  }

  /**
   * Change the display units to the new display units
   * @author George A. David
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    if (_oldDisplayUnits.equals(_newDisplayUnits))
      return false;

    _project.setDisplayUnits(_newDisplayUnits);
    return true;
  }

  /**
   * Restore the old display units
   * @author George A. David
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_project != null);

    _project.setDisplayUnits(_oldDisplayUnits);
  }

  /**
   * @author Bill Darbie
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_PROJECT_SET_DISPLAY_UNITS_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
