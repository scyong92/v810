package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * Command for setting the production tune re-enable threshold property
 * @author Andy Mechtenberg
 */
public class ConfigSetProductionTuneReEnableThresholdCommand extends DataCommand
{
  private ProductionTuneData _productionTuneData = null;
  private int _newReEnableThreshold;
  private int _oldReEnableThreshold;

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetProductionTuneReEnableThresholdCommand(ProductionTuneData productionTuneData,
                                                         int reEnableThreshold)
  {
    Assert.expect(productionTuneData != null);

    _productionTuneData = productionTuneData;
    _newReEnableThreshold = reEnableThreshold;
    _oldReEnableThreshold = _productionTuneData.getReEnableThreshold();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);

    _productionTuneData.setReEnableThreshold(_newReEnableThreshold);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_productionTuneData != null);

    _productionTuneData.setReEnableThreshold(_oldReEnableThreshold);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_PRODUCTION_TUNE_SET_RE_ENABLE_THRESHOLD_KEY");
  }
}
