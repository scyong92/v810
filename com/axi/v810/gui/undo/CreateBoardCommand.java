package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Command for creating new board instance
 * @author Andy Mechtenberg
 */
public class CreateBoardCommand extends DataCommand
{
  private Panel _panel;
  private BoardType _boardType;
  private PanelCoordinate _panelCoordinate;
  private int _degreesRotation;
  private boolean _flipped;

  private Board _createdBoard = null;

  /**
   * @author Andy Mechtenberg
   */
  public CreateBoardCommand(Panel panel, BoardType boardType, PanelCoordinate panelCoordinate, int degreesRotation, boolean flipped)
  {
    Assert.expect(panel != null);
    Assert.expect(boardType != null);
    Assert.expect(panelCoordinate != null);

    _panel = panel;
    _boardType = boardType;
    _degreesRotation = degreesRotation;
    _flipped = flipped;

    _panelCoordinate = panelCoordinate;
  }

  /**
   * Create the board in the panel
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_createdBoard == null)
    {
      _createdBoard = _panel.createBoard(_boardType, _panelCoordinate, _degreesRotation, _flipped);
    }
    else
    {
      _createdBoard.add();
    }
    return true;
  }

  /**
   * Destroy the board (opposite of create)
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_panel != null);
    Assert.expect(_createdBoard != null);
    _createdBoard.remove();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    LocalizedString localizedString = new LocalizedString("GUI_COMMAND_CREATE_BOARD_KEY", null);
    return StringLocalizer.keyToString(localizedString);
  }
}
