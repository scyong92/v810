package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 *
 * @author siew-yeng.phang
 */
public class SubtypeRemoveSubSubtypeCommand extends DataCommand
{
  Subtype _parentSubtype = null;
  Subtype _subSubtype = null;
  
  /**
   * Construct the class.
   * @author Siew Yeng
   */
  public SubtypeRemoveSubSubtypeCommand(Subtype parentSubtype, Subtype newSubSubtype)
  {
    Assert.expect(parentSubtype != null);
    Assert.expect(newSubSubtype != null);

    _parentSubtype = parentSubtype;
    _subSubtype = newSubSubtype;
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean executeCommand() throws XrayTesterException 
  {
    _parentSubtype.removeSubSubtype(_subSubtype);
    
    return true;
  }

  /**
   * @author Siew Yeng
   */
  public void undoCommand() throws XrayTesterException 
  {
    _parentSubtype.addSubSubtype(_subSubtype);
  }

  /**
   * @author Siew Yeng
   */
  public String getDescription() 
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SUBTYPE_REMOVE_SUB_SUBTYPE_KEY");
  }
  
}
