/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.gui.undo;

import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 *
 * @author sheng-chuan.yong
 */
public class ConfigSetBypassWithSerialControlModeCommand extends DataCommand
{
  private boolean _originalBypassMode;
  private boolean _newBypassMode;
  private TestExecution _testExecution = TestExecution.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetBypassWithSerialControlModeCommand(boolean newBypassMode)
  {
    _newBypassMode = newBypassMode;
    _originalBypassMode = _testExecution.isBypassWithSerialControlModeEnabled();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalBypassMode == _newBypassMode)
      return false;

    Config.getInstance().setValue(SoftwareConfigEnum.TEST_EXEC_BYPASS_WITH_SERIAL_MODE, _newBypassMode);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    Config.getInstance().setValue(SoftwareConfigEnum.TEST_EXEC_BYPASS_WITH_SERIAL_MODE, _originalBypassMode);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_SET_BYPASS_MODE_KEY");
  }
}
