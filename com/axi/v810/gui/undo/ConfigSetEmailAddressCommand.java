package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.util.*;

/**
 * This class is used so that if the email address in config is changed,
 * it can be reset via an undo command.
 * @author Andy Mechtenberg
 */
public class ConfigSetEmailAddressCommand extends DataCommand
{
  private String _originalEmailAddress;
  private String _newEmailAddress;
  private CustomerInfo _customerInfo = CustomerInfo.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ConfigSetEmailAddressCommand(String newEmailAddress)
  {
    Assert.expect(newEmailAddress != null);

    _newEmailAddress = newEmailAddress;
    _originalEmailAddress = _customerInfo.getEmailAddress();
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean executeCommand() throws XrayTesterException
  {
    if (_originalEmailAddress.equalsIgnoreCase(_newEmailAddress))
      return false;

    _customerInfo.setEmailAddress(_newEmailAddress);
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void undoCommand() throws XrayTesterException
  {
    _customerInfo.setEmailAddress(_originalEmailAddress);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_EMAIL_ADDRESS_KEY");
  }
}
