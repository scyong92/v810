package com.axi.v810.gui.undo;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import com.axi.v810.gui.mainMenu.*;

/**
 * This class is used so that if the mail server in config is changed,
 * it can be reset via an undo command.
 * @author Kee Chin Seong
 */
public class ConfigSetProjectVerificationImageDirectoryCommand extends DataCommand
{
  private String _originalVerificationImagesDirectory;
  private String _newVerificationImagesDirectory;

  /**
   * @author Kee Chin Seong
   */
  public ConfigSetProjectVerificationImageDirectoryCommand(String newVerificationImageDirectory)
  {
    Assert.expect(newVerificationImageDirectory != null);

    _newVerificationImagesDirectory = newVerificationImageDirectory;
    _originalVerificationImagesDirectory = Directory.getXrayVerificationImagesDirectory();
  }

  /**
   * @author Kee Chin Seong
   */
  public boolean executeCommand() throws XrayTesterException
  {
    Assert.expect(_newVerificationImagesDirectory != null);
    Assert.expect(_originalVerificationImagesDirectory != null);

    if (_originalVerificationImagesDirectory.equalsIgnoreCase(_newVerificationImagesDirectory))
    {
      return false;
    }

    Directory.setVerificationImagesDir(_newVerificationImagesDirectory);
    MainMenuGui.getInstance().updateHomePanel();      //Update recipe list on Main 


    return true;
  }

  /**
   * @author Kee Chin Seong
   */
  public void undoCommand() throws XrayTesterException
  {
    Assert.expect(_originalVerificationImagesDirectory != null);

    Directory.setVerificationImagesDir(_originalVerificationImagesDirectory);
    MainMenuGui.getInstance().updateHomePanel();   //Update recipe list on Main 
  }

  /**
   * @author Kee Chin Seong
   */
  public String getDescription()
  {
    return StringLocalizer.keyToString("GUI_COMMAND_VERIFICATION_DIRECTORY_KEY");
  }
}
