package com.axi.v810.gui;

import java.awt.*;
import javax.swing.*;

import com.axi.v810.images.*;
import com.axi.v810.util.*;
import com.axi.guiUtil.*;
import com.axi.util.*;

/**
  * This class creates some convenience functions for creating different types
  * of message dialogs.  It provides a common look for all message dialogs used
  * in an application.
  *
  * @author Steve Anonson and Bill Darbie
  */
public class MessageDialog
{
  private static JFrame _frame = new JFrame();
  private static final int _CHARS_PER_LINE = 50;

  /**
   * @author Bill Darbie
   */
  static
  {
    _frame.setIconImage(Image5DX.getImage(Image5DX.FRAME_X5000));
  }

  /**
    * This method does the actual work of creating an option pane dialog based
    * upon the parameters pass in.
    *
    * @param parent - GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage - String to put in the message area of the dialog.
    * @param localTitle - String to put the dialog title bar.
    * @param dialogType - what type of dialog to display.
    * @param modal - flag to determine if dialog is modal or not.
    * @param font - Font to use in the dialog.
    * @author Steve Anonson
    */
  private static void createDialog(final Component parent,
                                   final String localMessage,
                                   final String localTitle,
                                   final int dialogType,
                                   final boolean modal,
                                   final Font font)
  {
    Assert.expect(localMessage != null);
    Assert.expect(localTitle != null);
    String title = StringLocalizer.keyToString(localTitle);

    String formattedLocalMessage = StringUtil.format(localMessage, _CHARS_PER_LINE);
    JOptionPane pane = new JOptionPane(formattedLocalMessage, dialogType);
    final JDialog dialog;
    if (parent == null)
      dialog = pane.createDialog(_frame, title);
    else
      dialog = pane.createDialog(parent, title);
    dialog.setModal(modal);
    if (font != null)
      SwingUtils.setFont(dialog, font);

    dialog.pack();
    Assert.expect(SwingUtilities.isEventDispatchThread());
    dialog.setVisible(true);
    if (modal)
      dialog.dispose();
  }

  /**
   * This method does the actual work of creating an option pane dialog based
   * upon the parameters pass in.
   *
   * @param parent - GUI component to be the parent of the dialog.  Can be null.
   * @param localMessage - String to put in the message area of the dialog.
   * @param localTitle - String to put the dialog title bar.
   * @param dialogType - what type of dialog to display.
   * @param modal - flag to determine if dialog is modal or not.
   * @author Steve Anonson
   */
  private static void createDialog(final Component parent,
                                   final LocalizedString localMessage,
                                   final LocalizedString localTitle,
                                   final int dialogType,
                                   final boolean modal)
  {
    Assert.expect(localMessage != null);
    Assert.expect(localTitle != null);

    Assert.expect(SwingUtilities.isEventDispatchThread());
    String message = StringLocalizer.keyToString(localMessage);
    String title = StringLocalizer.keyToString(localTitle);

    message = StringUtil.format(message, _CHARS_PER_LINE);
    JOptionPane pane = new JOptionPane(message, dialogType);
    JDialog dialog = null;
    if (parent == null)
      dialog = pane.createDialog(_frame, title);
    else
      dialog = pane.createDialog(parent, title);
    dialog.setModal(modal);
    dialog.pack();
    dialog.setVisible(true);
    if (modal)
      dialog.dispose();
  }

  /**
    * The following method provide a means to create a dialog to report an
    * error condition.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @param localTitle the String to put the dialog title bar.
    * @param modal flag to determine if dialog is modal or not.
    * @param font the Font to use in the dialog.
    * @author Steve Anonson
    */
  public static void showErrorDialog(Component parent,
                                        String localMessage,
                                        String localTitle,
                                        boolean modal,
                                        Font font)
  {
    createDialog(parent, localMessage, localTitle, JOptionPane.ERROR_MESSAGE, modal, font);
  }

  /**
    * The following method provide a means to create a dialog to report an
    * error condition.
    *
    * @param parent - GUI component to be the parent of the dialog.  Can be null.
    * @param xte - XrayTesterException with the localized error message
    * @param localTitle - String to put the dialog title bar.
    * @param modal - flag to determine if dialog is modal or not.
    * @author Steve Anonson
    */
  public static void showErrorDialog(Component parent,
                                        XrayTesterException xte,
                                        String localTitle,
                                        boolean modal)
  {
    createDialog(parent, xte.getLocalizedMessage(), localTitle, JOptionPane.ERROR_MESSAGE, modal, null);
  }

  /**
   * The following method provide a means to create a dialog to report an
   * error condition.
   *
   * @param parent - GUI component to be the parent of the dialog.  Can be null.
   * @param xte - XrayTesterException with the localized error message
   * @param localTitle - String to put the dialog title bar.
   * @param modal - flag to determine if dialog is modal or not.
   * @author Andy Mechtenberg
   */
  public static void showErrorDialog(Component parent,
                                        XrayTesterException xte,
                                        String localTitle,
                                        boolean modal,
                                        Font font)
  {
    createDialog(parent, xte.getLocalizedMessage(), localTitle, JOptionPane.ERROR_MESSAGE, modal, font);
  }

  /**
   * The following method provide a means to create a dialog to report an
   * error condition.
   *
   * @param parent - GUI component to be the parent of the dialog.  Can be null.
   * @param localMessage - String to put in the message area of the dialog.
   * @param localTitle - String to put the dialog title bar.
   * @param modal - flag to determine if dialog is modal or not.
   * @param font - Font to use in the dialog.
   * @author Steve Anonson
   */
  public static void showErrorDialog(Component parent,
                                     LocalizedString localMessage,
                                     String localTitle,
                                     boolean modal,
                                     Font font)
  {
    createDialog(parent, StringLocalizer.keyToString(localMessage), localTitle, JOptionPane.ERROR_MESSAGE, modal, font);
  }

  /**
    * The following method provide a means to create a dialog to report an
    * error condition.  Default font is used.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @param localTitle the String to put the dialog title bar.
    * @param modal a flag to determine if dialog is modal or not.
    * @author Steve Anonson
    */
  public static void showErrorDialog(Component parent,
                                        String localMessage,
                                        String localTitle,
                                        boolean modal)
  {
    // Default font to null.
    if(SwingUtilities.isEventDispatchThread())
    {
      showErrorDialog(parent, localMessage, localTitle, modal, null);
    }
    else
      showErrorDialogInSwingUtilitiesThread(parent, localMessage, localTitle, modal);
  }
  
  /**
    * The following method provide a means to create a dialog to report an
    * error condition in a SwingUtil thread.  Default font is used.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @param localTitle the String to put the dialog title bar.
    * @param modal a flag to determine if dialog is modal or not.
    * @author Ying-Huan.Chu
    */
  public static void showErrorDialogInSwingUtilitiesThread(final Component parent,
                                                           final String localMessage,
                                                           final String localTitle,
                                                           final boolean modal)
  {
    // Default font to null.
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        showErrorDialog(parent, localMessage, localTitle, modal, null);
      }
    });
  }

  /**
    * The following method provide a means to create a dialog to report an
    * error condition.  Default font is used and dialog is not modal.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @param localTitle the String to put the dialog title bar.
    * @author Steve Anonson
    */
  public static void showErrorDialog(Component parent, String localMessage, String localTitle)
  {
    // Default modal to true.
    showErrorDialog(parent, localMessage, localTitle, true);
  }

  /**
    * The following method provide a means to create a dialog to report an
    * warning condition.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @param localTitle the String to put the dialog title bar.
    * @param modal the flag to determine if dialog is modal or not.
    * @param font a Font to use in the dialog.
    * @author Steve Anonson
    */
  public static void showWarningDialog(Component parent,
                                          String localMessage,
                                          String localTitle,
                                          boolean modal,
                                          Font font)
  {
    createDialog(parent, localMessage, localTitle, JOptionPane.WARNING_MESSAGE, modal, font);
  }

  /**
    * The following method provide a means to create a dialog to report an
    * warning condition.  Default font is used.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @param localTitle the String to put the dialog title bar.
    * @param modal a flag to determine if dialog is modal or not.
    * @author Steve Anonson
    */
  public static void showWarningDialog(Component parent,
                                       String localMessage,
                                       String localTitle,
                                       boolean modal)
  {
    // Default font to null.
    showWarningDialog(parent, localMessage, localTitle, modal, null);
  }
  
  /**
    * The following method provide a means to create a dialog to report an
    * warning condition.  Default font is used and dialog is not modal.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @param localTitle the String to put the dialog title bar.
    * @author Steve Anonson
    */
  public static void showWarningDialog(Component parent, String localMessage, String localTitle)
  {
    // Default modal to false.
    showWarningDialog(parent, localMessage, localTitle, false);
  }

  /**
    * The following method provide a means to create a dialog to report some
    * information.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @param localTitle the String to put the dialog title bar.
    * @param modal a flag to determine if dialog is modal or not.
    * @param font a Font to use in the dialog.
    * @author Steve Anonson
    */
  public static void showInformationDialog(Component parent,
                                           String localMessage,
                                           String localTitle,
                                           boolean modal,
                                           Font font)
  {
    createDialog(parent, localMessage, localTitle, JOptionPane.INFORMATION_MESSAGE, modal, font);
  }

  /**
   * @author Andy Mechtenberg
   */
  public static void showInformationDialog(final Component parentComponent,
                                           final String message,
                                           final String title,
                                           final Object[] options,
                                           final Font font )
  {
    Assert.expect(parentComponent != null);
    Assert.expect(message != null);
    Assert.expect(title != null);
    Assert.expect(options != null);
    Assert.expect(font != null);
    Assert.expect(SwingUtilities.isEventDispatchThread());
    JOptionPane pane = new JOptionPane(message,
                                       JOptionPane.INFORMATION_MESSAGE,
                                       JOptionPane.NO_OPTION, null, options);

    JDialog dialog = pane.createDialog(parentComponent, title);
    dialog.setModal(true);
    SwingUtils.setFont(dialog, font);
    dialog.pack();
    dialog.setVisible(true);
    dialog.dispose();
  }

  /**
    * The following method provide a means to create a dialog to report some
    * information.  Default font is used.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @param localTitle the String to put the dialog title bar.
    * @param modal a flag to determine if dialog is modal or not.
    * @author Steve Anonson
    */
  public static void showInformationDialog(Component parent,
                                           String localMessage,
                                           String localTitle,
                                           boolean modal)
  {
    // Default font to null.
    showInformationDialog(parent, localMessage, localTitle, modal, null);
  }

  /**
    * The following method provide a means to create a dialog to report some
    * information.  Default font is used and dialog is not modal.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @param localTitle the String to put the dialog title bar.
    * @author Steve Anonson
    */
  public static void showInformationDialog(Component parent, String localMessage, String localTitle)
  {
    // Default modal to false.
    showInformationDialog(parent, localMessage, localTitle, false);
  }

  // The following methods create a message dialog for a specific type of
  // error condition.

  /**
    * The following method creates a dialog to report a remote exception error.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @param modal a flag to determine if dialog is modal or not.
    * @param font the Font to use in the dialog.
    * @author Steve Anonson
    */
  public static void reportRemoteError(Component parent, String localMessage, boolean modal, Font font)
  {
    createDialog(parent, localMessage, StringLocalizer.keyToString("MD_REMOTE_ERROR_TITLE_KEY"), JOptionPane.ERROR_MESSAGE, modal, font);
  }

  /**
    * The following method creates a dialog to report a remote exception error.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @author Steve Anonson
    */
  public static void reportRemoteError(Component parent, String localMessage)
  {
    reportRemoteError(parent, localMessage, true, null);
  }

  /**
    * The following method creates a dialog to report a remote exception error.
    * Default message is used, default font is used and dialog is modal.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @author Steve Anonson
    */
  public static void reportRemoteError(Component parent)
  {
    // Default message, modal is true and font to null.
    LocalizedString message = new LocalizedString("MD_REMOTE_ERROR_MESSAGE_KEY", null);
    reportRemoteError(parent, StringLocalizer.keyToString(message) );
  }

  /**
    * The following method creates a dialog to report a not bound exception error.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @param modal a flag to determine if dialog is modal or not.
    * @param font the Font to use in the dialog.
    * @author Steve Anonson
    */
  public static void reportNotBoundError(Component parent, String localMessage, boolean modal, Font font)
  {
    createDialog(parent, localMessage, StringLocalizer.keyToString("MD_NOT_BOUND_ERROR_TITLE_KEY"), JOptionPane.ERROR_MESSAGE, modal, font);
  }

  /**
    * The following method creates a dialog to report a not bound exception error.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @author Steve Anonson
    */
  public static void reportNotBoundError(Component parent, String localMessage)
  {
    reportNotBoundError(parent, localMessage, true, null);
  }

  /**
    * The following method creates a dialog to report a not bound exception error.
    * Default message is used, default font is used and dialog is modal.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @author Steve Anonson
    */
  public static void reportNotBoundError(Component parent)
  {
    // Default message, modal is true and font to null.
    String message = StringLocalizer.keyToString("MD_NOT_BOUND_ERROR_MESSAGE_KEY");
    reportNotBoundError(parent, message);
  }

  /**
    * The following method creates a dialog to report a malformed url exception error.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @param modal a flag to determine if dialog is modal or not.
    * @param font the Font to use in the dialog.
    * @author Steve Anonson
    */
  public static void reportMalformedUrlError(Component parent, String localMessage, boolean modal, Font font)
  {
    createDialog(parent, localMessage, StringLocalizer.keyToString("MD_MALFORMED_URL_ERROR_TITLE_KEY"), JOptionPane.ERROR_MESSAGE, modal, font);
  }

  /**
    * The following method creates a dialog to report a malformed url exception error.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @author Steve Anonson
    */
  public static void reportMalformedUrlError(Component parent, String localMessage)
  {
    reportMalformedUrlError(parent, localMessage, true, null);
  }

  /**
    * The following method creates a dialog to report a malformed url exception error.
    * Default message is used, default font is used and dialog is modal.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @author Steve Anonson
    */
  public static void reportMalformedUrlError( Component parent )
  {
    // Default message, modal is true and font to null.
    LocalizedString localMessage = new LocalizedString("MD_MALFORMED_URL_ERROR_MESSAGE_KEY", null);
    reportMalformedUrlError(parent, StringLocalizer.keyToString(localMessage));
  }

    /**
    * The following method creates a dialog to report an I/O exception error.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @param modal a flag to determine if dialog is modal or not.
    * @param font the Font to use in the dialog.
    * @author Steve Anonson
    */
  public static void reportIOError(Component parent, String localMessage, boolean modal, Font font)
  {
    createDialog(parent, localMessage, StringLocalizer.keyToString("MD_IO_ERROR_TITLE_KEY"), JOptionPane.ERROR_MESSAGE, modal, font);
  }

  /**
    * The following method creates a dialog to report an I/O exception error.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @author Steve Anonson
    */
  public static void reportIOError(Component parent, String localMessage)
  {
    reportIOError(parent, localMessage, true, null);
  }

  /**
    * The following method creates a dialog to report a hardware exception error.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @param modal a flag to determine if dialog is modal or not.
    * @param font the Font to use in the dialog.
    * @author Steve Anonson
    */
  public static void reportHardwareError(Component parent, String localMessage, boolean modal, Font font)
  {
    createDialog( parent, localMessage, StringLocalizer.keyToString("MD_HARDWARE_ERROR_TITLE_KEY"), JOptionPane.ERROR_MESSAGE, modal, font);
  }

  /**
    * The following method creates a dialog to report a hardware exception error.
    *
    * @param parent the GUI component to be the parent of the dialog.  Can be null.
    * @param localMessage the String to put in the message area of the dialog.
    * @author Steve Anonson
    */
  public static void reportHardwareError(Component parent, String localMessage)
  {
    reportHardwareError(parent, localMessage, true, null);
  }

  /**
    * The following method creates a dialog to report a configuration data error.
    * @param localMessage the message to be displayed
    * @author Steve Anonson
    */
  public static void reportConfigurationError(String localMessage)
  {
    createDialog(null, localMessage, StringLocalizer.keyToString("MD_CONFIGURATION_ERROR_TITLE_KEY"), JOptionPane.ERROR_MESSAGE, true, null);
  }
}
