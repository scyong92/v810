/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.mainMenu.*;
import static com.axi.v810.gui.mainMenu.MainMenuGui.finishLicenseMonitor;
import static com.axi.v810.gui.mainMenu.MainUIMenuBar.getAllNdfTypeNames;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

/**
 *
 * @author weng-jian.eoh
 */
public class ImportNdfDialog extends EscapeDialog
{
  private JFrame _mainUI;
  // either OK or CANCEL depending on how the user exits
  private int _returnValue;

  private Object _selectedRecipe = null;
  private String _newProjectName = null;
  private String _instruction;
  private String _recipeNameLabel = null;
  private java.util.List<Object> _intialThresholdsFileItems;
  private java.util.List<Object> _jointTypeAssignmentItems;
  private java.util.List<Object> _initialRecipeSettingFileItem;
  private java.util.List<Object> _ndfTypeItems;
  private java.util.List<Object> _systemTypeItems;
  private java.util.List<Object> _systemMagItems;
  private JLabel _intialThresholdLabel;
  private JLabel _initialRecipeSettingLabel;
  private JLabel _jointTypeRuleLabel;
  private JLabel _ndfTypeLabel;
  private JLabel _systemTypeLabel;
  private JLabel _systemMagLabel;
  private Object _selectedThresholdSet;
  private Object _selectedRecipeSettingSet;
  private Object _selectedJointTypeAssignment;
  private Object _selectedNdfType;
  private Object _selectedSystemType;
  private Object _selectedSystemMagnicification;
  private Object _defaultIntialThreshold;
  private Object _defaultInitialRecipeSetting;
  private Object _defaultJointTypeAssignemnt;
  private Object _defaultNdfType;
  private Object _currentSystemType;
  private Object _currentSystemMagnification;

  private Font _buttonFont;
  private Font _messageFont;

  private JPanel _mainPanel = new JPanel();
  private JPanel _messagePanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();

  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private FlowLayout _messagePanelFlowLayout = new FlowLayout();
  private FlowLayout _buttonPanelFlowLayout = new FlowLayout();
  private GridLayout _innerButtonPanelGridLayout = new GridLayout();
  private JLabel _messageLabel = new JLabel();
  private JPanel _inputPanel = new JPanel();
  private DefaultListModel _choicesListModel = new DefaultListModel();
  private DefaultListModel _choicesListModel2 = new DefaultListModel();
  private JScrollPane _choicesScrollPane = new JScrollPane();
  private BorderLayout _inputPanelBorderLayout = new BorderLayout();
  private Border _innerButtonPanelBorder = BorderFactory.createEmptyBorder(5, 10, 0, 10);
  private Border _mainPanelBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
  private JPanel _additionalControlsPanel = new JPanel();
  private BorderLayout _additionalControlsBorderLayout = new BorderLayout();
  private JPanel _additionalControlsInnerPanel = new JPanel();
  private BorderLayout _textFieldBorderLayout = new BorderLayout();
  private JLabel _additionalTextFieldLabel = new JLabel();
   


  private JButton _okButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }

  };
  private JButton _cancelButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  private JList _choicesList = new JList(_choicesListModel)
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  //Jack Hwee - add new choice list for swithching
  private JList _choicesList2 = new JList(_choicesListModel2)
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  private JTextField _recipeNameTextField = new JTextField()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
      if (e.getKeyCode() == KeyEvent.VK_ENTER)
      {
        okAction();
      }

    }
  };

  private JComboBox _intialThresholdComboBox = new JComboBox()
  {
    public void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };
  
  private JComboBox _intialRecipeSettingComboBox = new JComboBox()
  {
    public void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };
  
  private JComboBox _jointTypeRuleComboBox = new JComboBox()
  {
    public void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  //Jack Hwee - Add combobox to change the choice list
  private JComboBox _ndfComboBox = new JComboBox()
  {
    public void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };
  
  private JComboBox _systemTypeComboBox = new JComboBox()
  {
    public void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };
  
  private JComboBox _systemMagnificationComboBox = new JComboBox()
  {
    public void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };


  /**
   * @author Erica Wheatcroft
   */
  private ImportNdfDialog()
  {
    // do nothing
  }
  

  /**
   * @author Andy Mechtenberg
   * @author Jack Hwee
   */
  private void createDialog()
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _innerButtonPanelGridLayout.setHgap(5);
    _messagePanel.setLayout(_messagePanelFlowLayout);
    _messagePanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _messagePanelFlowLayout.setHgap(0);
    _messagePanelFlowLayout.setVgap(0);
    _cancelButton.setFont(_buttonFont);
    _okButton.setFont(_buttonFont);
    _cancelButton.setMargin(new Insets(2, 8, 2, 8));
    _okButton.setMargin(new Insets(2, 8, 2, 8));
    _messageLabel.setFont(_messageFont);
    _messageLabel.setText(_instruction);
    _inputPanel.setLayout(_inputPanelBorderLayout);
    _buttonPanelFlowLayout.setHgap(0);
    _buttonPanelFlowLayout.setVgap(0);
    _innerButtonPanel.setBorder(_innerButtonPanelBorder);
    _mainPanel.setBorder(_mainPanelBorder);
    _mainPanelBorderLayout.setVgap(5);
    _additionalControlsPanel.setLayout(_additionalControlsBorderLayout);
    _additionalControlsInnerPanel.setLayout(_textFieldBorderLayout);

    getContentPane().add(_mainPanel);
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    _buttonPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _innerButtonPanel.setLayout(_innerButtonPanelGridLayout);
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _innerButtonPanel.add(_okButton);
    _innerButtonPanel.add(_cancelButton);
    _additionalControlsPanel.add(_additionalControlsInnerPanel, BorderLayout.NORTH);
    _messagePanel.add(_messageLabel);
    _buttonPanel.add(_innerButtonPanel);
    _mainPanel.add(_inputPanel, BorderLayout.CENTER);

    _choicesScrollPane.getViewport().add(_choicesList);
    _inputPanel.add(_choicesScrollPane, BorderLayout.CENTER);
    _choicesList.setVisibleRowCount(10);
    _choicesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _choicesList.addMouseListener(new MouseAdapter()
    {

      public void mousePressed(MouseEvent e)
      {
        // if the user double clicks on a entry, automatically select it and exit the dialog
        if (e.getClickCount() == 1)
        {
          int index = _choicesList.locationToIndex(e.getPoint());
          if (index != -1)
          {
            _selectedRecipe = _choicesList.getModel().getElementAt(index);
            if (_selectedRecipe != null)
            {
              _recipeNameTextField.setText(_selectedRecipe.toString());
            }
          }
        }
        else if (e.getClickCount() == 2 && _choicesListModel.isEmpty() == false)
        {
          int index = _choicesList.locationToIndex(e.getPoint());
          if (index != -1)
          {
            _selectedRecipe = _choicesList.getModel().getElementAt(index);
            _newProjectName = _selectedRecipe.toString();
            _returnValue = JOptionPane.OK_OPTION;
            dispose();
          }
        }
      }
    });

    //Jack Hwee - add new choice list for swithching
    _choicesList2.setVisibleRowCount(10);
    _choicesList2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _choicesList2.addMouseListener(new MouseAdapter()
    {

      public void mousePressed(MouseEvent e)
      {
        // if the user double clicks on a entry, automatically select it and exit the dialog
        if (e.getClickCount() == 1)
        {
          int index = _choicesList2.locationToIndex(e.getPoint());
          if (index != -1)
          {
            _selectedRecipe = _choicesList2.getModel().getElementAt(index);
            if (_selectedRecipe != null)
            {
              _recipeNameTextField.setText(_selectedRecipe.toString());
            }

          }
        }
        else if (e.getClickCount() == 2 && _choicesListModel2.isEmpty() == false)
        {
          int index = _choicesList2.locationToIndex(e.getPoint());
          if (index != -1)
          {
            _selectedRecipe = _choicesList2.getModel().getElementAt(index);
            _newProjectName = _selectedRecipe.toString();
            _returnValue = JOptionPane.OK_OPTION;
            dispose();
          }
        }
      }
    });

    _choicesList.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      // this ensures that a selected item is visible.  At jdk1.5, this is the default behavior, so this could be removed.

      public void valueChanged(ListSelectionEvent e)
      {
        if (e.getValueIsAdjusting() == false && _choicesListModel.isEmpty() == false)
        {
          int index = _choicesList.getSelectedIndex();
          _choicesList.ensureIndexIsVisible(index);
          _recipeNameTextField.setText(_choicesList.getSelectedValue().toString());
        }
      }
    });

    //Jack Hwee - add new choice list for switching
    _choicesList2.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      // this ensures that a selected item is visible.  At jdk1.5, this is the default behavior, so this could be removed.

      public void valueChanged(ListSelectionEvent e)
      {
        if (e.getValueIsAdjusting() == false && _choicesListModel2.isEmpty() == false)
        {
          int index = _choicesList2.getSelectedIndex();
          _choicesList2.ensureIndexIsVisible(index);
          _recipeNameTextField.setText(_choicesList2.getSelectedValue().toString());
        }
      }
    });

    _additionalControlsPanel.add(_buttonPanel, BorderLayout.SOUTH);
    _mainPanel.add(_additionalControlsPanel, BorderLayout.SOUTH);
    _mainPanel.add(_messagePanel, BorderLayout.NORTH);

    _additionalControlsInnerPanel.add(_recipeNameTextField, BorderLayout.CENTER);
    _additionalControlsInnerPanel.add(_additionalTextFieldLabel, BorderLayout.NORTH);
    if (_choicesList.getSelectedValue() != null)
    {
      _recipeNameTextField.setText(_choicesList.getSelectedValue().toString());
    }
    _additionalTextFieldLabel.setText(_recipeNameLabel);
    _recipeNameTextField.addFocusListener(new FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        _recipeNameTextField.selectAll();
      }

      public void focusLost(FocusEvent e)
      {
        String text = _recipeNameTextField.getText();
        if (text != null || text.equalsIgnoreCase("") == false)
        {
          _newProjectName = text;
          // now enable the ok button
          _okButton.setEnabled(true);
        }
      }
    });

    _recipeNameTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        String text = _recipeNameTextField.getText();
        if (text != null || text.equalsIgnoreCase("") == false)
        {
          _newProjectName = text;
          // now enable the ok button
          _okButton.setEnabled(true);
        }
      }
    });
    
    JPanel comboboxPanel = new JPanel(new BorderLayout());
    comboboxPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
    comboboxPanel.add(_intialThresholdLabel, BorderLayout.NORTH);
    comboboxPanel.add(_intialThresholdComboBox, BorderLayout.CENTER);

    JPanel combobox2Panel = new JPanel(new BorderLayout());
    combobox2Panel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
    combobox2Panel.add(_initialRecipeSettingLabel, BorderLayout.NORTH);
    combobox2Panel.add(_intialRecipeSettingComboBox, BorderLayout.CENTER);

    JPanel combobox3Panel = new JPanel(new BorderLayout());
    combobox3Panel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
    combobox3Panel.add(_jointTypeRuleLabel, BorderLayout.NORTH);
    combobox3Panel.add(_jointTypeRuleComboBox, BorderLayout.CENTER);
    
    //Jack Hwee - Add combobox for new choice list
    JPanel combobox4Panel = new JPanel(new BorderLayout());
    combobox4Panel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
    combobox4Panel.add(_ndfTypeLabel, BorderLayout.NORTH);
    combobox4Panel.add(_ndfComboBox, BorderLayout.CENTER);
    
    
    JPanel combineComboboxPanel2 = null;
    
    try
    {
      if (LicenseManager.isOfflineProgramming())
      {
        combineComboboxPanel2 = new JPanel(new BorderLayout());
        JPanel combobox5Panel = new JPanel(new BorderLayout());
        combobox5Panel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
        combobox5Panel.add(_systemTypeLabel, BorderLayout.NORTH);
        combobox5Panel.add(_systemTypeComboBox, BorderLayout.CENTER);
        combineComboboxPanel2.add(combobox5Panel,BorderLayout.NORTH);

        if (LicenseManager.getInstance().getSupportedSystemType().contains(SystemTypeEnum.S2EX.getName()))
        {
          JPanel combobox6Panel = new JPanel(new BorderLayout());
          combobox6Panel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
          combobox6Panel.add(_systemMagLabel, BorderLayout.NORTH);
          combobox6Panel.add(_systemMagnificationComboBox, BorderLayout.CENTER);
          combineComboboxPanel2.add(combobox6Panel,BorderLayout.CENTER);
        }
      }
    }
    catch (final BusinessException e)
    {
      finishLicenseMonitor();
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
            e.getMessage(),
            StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
            true);
          MainMenuGui.getInstance().exitV810(false);
        }
      });
    }

    JPanel combineComboboxPanel = new JPanel(new BorderLayout());
    combineComboboxPanel.add(comboboxPanel, BorderLayout.NORTH);
    combineComboboxPanel.add(combobox2Panel, BorderLayout.CENTER);

    JPanel combineComboboxPanel1 = new JPanel(new BorderLayout());
    combineComboboxPanel1.add(combobox3Panel, BorderLayout.NORTH);
    combineComboboxPanel1.add(combobox4Panel, BorderLayout.CENTER);
        
    JPanel bothComboboxPanel = new JPanel(new BorderLayout());
    bothComboboxPanel.add(combineComboboxPanel, BorderLayout.NORTH);
    bothComboboxPanel.add(combineComboboxPanel1, BorderLayout.CENTER);
    if (combineComboboxPanel2 != null)
    {
      bothComboboxPanel.add(combineComboboxPanel2, BorderLayout.SOUTH);
    }

    _additionalControlsInnerPanel.add(bothComboboxPanel, BorderLayout.SOUTH);

    for (Object item : _intialThresholdsFileItems)
    {
      _intialThresholdComboBox.addItem(item);
    }
    for (Object item : _initialRecipeSettingFileItem)
    {
      _intialRecipeSettingComboBox.addItem(item);
    }
    for (Object item : _jointTypeAssignmentItems)
    {
      _jointTypeRuleComboBox.addItem(item);
    }
    for (Object item : _ndfTypeItems)
    {
      _ndfComboBox.addItem(item);
    }
    for (Object item : _systemTypeItems)
    {
      _systemTypeComboBox.addItem(item);
    }
    for (Object item : _systemMagItems)
    {
      _systemMagnificationComboBox.addItem(item);
    }

    _intialThresholdComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedThresholdSet = _intialThresholdComboBox.getSelectedItem();
      }
    });

    _intialThresholdComboBox.setSelectedItem(_defaultIntialThreshold);
    
    _intialRecipeSettingComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedRecipeSettingSet = _intialRecipeSettingComboBox.getSelectedItem();
      }
    });

    _intialRecipeSettingComboBox.setSelectedItem(_defaultInitialRecipeSetting);
    
    _jointTypeRuleComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedJointTypeAssignment = _jointTypeRuleComboBox.getSelectedItem();
      }
    });

    _jointTypeRuleComboBox.setSelectedItem(_defaultJointTypeAssignemnt);

    //Jack Hwee - Add combobox for new choice list
    _ndfComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedNdfType = _ndfComboBox.getSelectedItem();
        switcthChoiceList();
      }
    });

    _ndfComboBox.setSelectedItem(_defaultNdfType);

    _systemTypeComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _selectedSystemType = _systemTypeComboBox.getSelectedItem();
        if (_selectedSystemType.equals(SystemTypeEnum.S2EX.getName()))
        {
          _systemMagLabel.setEnabled(true);
          _systemMagnificationComboBox.setEnabled(true);
          _selectedSystemMagnicification = _systemMagnificationComboBox.getSelectedItem();
        }
        else
        {
          _systemMagLabel.setEnabled(false);
          _systemMagnificationComboBox.setEnabled(false);
        }
        switcthChoiceList();
      }
    });

    _systemTypeComboBox.setSelectedItem(_currentSystemType);

    _systemMagnificationComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_systemMagnificationComboBox.isEnabled())
        {
          _selectedSystemMagnicification = _systemMagnificationComboBox.getSelectedItem();
        }
        switcthChoiceList();
      }
    });

    _systemMagnificationComboBox.setSelectedItem(_currentSystemMagnification);

    if (_currentSystemType.equals(SystemTypeEnum.S2EX.getName()) == false)
    {
      _systemMagLabel.setEnabled(false);
      _systemMagnificationComboBox.setEnabled(false);
    }

    getRootPane().setDefaultButton(_okButton);
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okAction();
      }
    });

  }
  /**
   * @return int
   * @author Andy Mechtenberg
   */
  public int showDialog()
  {
    SwingUtils.centerOnComponent(this, _mainUI);
    setVisible(true);
    return _returnValue;
  }

  /**
   * @param font Font
   * @return int
   * @author Andy Mechtenberg
   */
  public int showDialog(Font font)
  {
    Assert.expect(font != null);
    SwingUtils.setFont(this, font);
    pack();
    SwingUtils.centerOnComponent(this, _mainUI);

    setVisible(true);
    return _returnValue;
  }
  /**
   * @param e WindowEvent
   * @author Andy Mechtenberg
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    int eventId = e.getID();
    if (eventId == WindowEvent.WINDOW_CLOSING)
    {
      cancelAction();
    }
  }

  /**
   * @return Object
   * @author Andy Mechtenberg
   */
  public Object getSelectedRecipe()
  {
    return _selectedRecipe;
  }

  /**
   * @return Object
   * @author Andy Mechtenberg
   */
  public String getNewProjectName()
  {
    return _newProjectName;
  }

  /**
   * @author Laura Cormos
   */
  public Object getSelectedThresholdsSet()
  {
    return _selectedThresholdSet;
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public Object getSelectedRecipeSettingSet()
  {
    return _selectedRecipeSettingSet;
  }

  /**
   * @author Laura Cormos
   */
  public Object getSelectedJointTypeAssignment()
  {
    return _selectedJointTypeAssignment;
  }

   /**
   * @author Jack Hwee
   */
  public Object getSelectedNdfType()
  {
    return _selectedNdfType;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @return 
   */
  public Object getSelectedSystemType()
  {
    return _selectedSystemType;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @return 
   */
  public Object getSelectedSystemMagnification()
  {
    return _selectedSystemMagnicification;
  }

  
  /**
   * @param e WindowEvent
   * @author Andy Mechtenberg
   * @author Jack Hwee
   */
  private void okAction()
  {
    java.util.List<Object> listTypeAssignmentNames = _ndfTypeItems;
    Object[] listTypeAdditionalComboboxItems = listTypeAssignmentNames.toArray();
    if (_selectedNdfType.toString() == listTypeAdditionalComboboxItems[1])
      _selectedRecipe = _choicesList2.getSelectedValue();
    else
      _selectedRecipe = _choicesList.getSelectedValue();
    
    _newProjectName = _recipeNameTextField.getText();
    _returnValue = JOptionPane.OK_OPTION;

    dispose();
  }

  /**
   * @param e WindowEvent
   * @author Andy Mechtenberg
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    cancelAction();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void cancelAction()
  {
    _returnValue = JOptionPane.CANCEL_OPTION;
    _selectedRecipe = null;
    dispose();
  }

  /**
   *
   * @author Jack Hwee
   */
  void switcthChoiceList()
  {
    String listType = (String) _selectedNdfType;

    java.util.List<Object> listTypeAssignmentNames = _ndfTypeItems;
    Object[] listTypeAdditionalComboboxItems = listTypeAssignmentNames.toArray();
    
    if (listType == listTypeAdditionalComboboxItems[0])
    {
      _choicesScrollPane.getViewport().remove(_choicesList2);
      _choicesScrollPane.getViewport().add(_choicesList);
       if (_choicesListModel.isEmpty() == false)
       {
        _recipeNameTextField.setText(_choicesList.getSelectedValue().toString());
         _okButton.setEnabled(true);
       }
      else
      {
        _recipeNameTextField.setText("");
         _okButton.setEnabled(false);
      }
    }
    else if (listType == listTypeAdditionalComboboxItems[1])
    {
      _choicesScrollPane.getViewport().remove(_choicesList);
      _choicesScrollPane.getViewport().add(_choicesList2);
      if (_choicesListModel2.isEmpty() == false)
      {
        _recipeNameTextField.setText(_choicesList2.getSelectedValue().toString());
       _okButton.setEnabled(true);
      }
      else
      {
        _recipeNameTextField.setText("");
        _okButton.setEnabled(false);
      }
    }
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @param frame
   * @param title
   * @param message
   * @param okButtonText
   * @param cancelButtonText
   * @param possibleValues
   * @param possibleValues2
   * @param additionalTextFieldLabel
   * @param additionalComboBoxItemsList
   * @param additionalComboBoxLabel
   * @param itemToSelectInComboBox
   * @param additionalComboBox2ItemsList
   * @param additionalComboBox2Label
   * @param itemToSelectInComboBox2
   * @param additionalComboBox3ItemsList
   * @param additionalComboBox3Label
   * @param itemToSelectInComboBox3
   * @param additionalComboBox4Label
   * @param additionalComboBox4ItemsList
   * @param itemToSelectInComboBox4
   * @param additionalComboBox5Label
   * @param additionalComboBox5ItemList
   * @param itemToSelectInComboBox5 
   */
  public ImportNdfDialog(JFrame frame,String title)
   {
     super(frame, title, true);
     
     Assert.expect(frame != null);
     
     
     java.util.List<String> ndfProject = FileName.getNdfProjectNames();
     java.util.List<String> ndfProjects5dx = FileName.get5dxNdfProjectNames();
    
     _recipeNameLabel = StringLocalizer.keyToString("HP_PROJECTS_TABLE_PROJECT_NAME_COLUMN_KEY");

     _intialThresholdLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SELECT_INITIAL_THRESHOLDS_ON_IMPORT_KEY"));
     
    java.util.List<String> initialThresholdsFileNames = FileName.getAllInitialThresholdSetNames();
    String initialThresholdsFileUsed = ProjectInitialThresholds.getInstance().getInitialThresholdsFileNameWithoutExtension();

    if (initialThresholdsFileNames.contains(initialThresholdsFileUsed) == false)
      initialThresholdsFileUsed = ProjectInitialThresholds.getInstance().getSystemDefaultSetName();
    
     _intialThresholdsFileItems = new java.util.ArrayList<Object>();
     for (int i = 0; i < initialThresholdsFileNames.size(); i++)
       _intialThresholdsFileItems.add(initialThresholdsFileNames.get(i));
     
     _defaultIntialThreshold = initialThresholdsFileUsed;
     
     _initialRecipeSettingLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SELECT_INITIAL_RECIPE_SETTING_ON_IMPORT_KEY"));
     
    java.util.List<String> initialRecipeSettingFileNames = FileName.getAllInitialRecipeSettingNames();
    String initialRecipeSettingFileUsed = ProjectInitialRecipeSettings.getInstance().getInitialRecipeSettingWithoutExtension();

    if (initialRecipeSettingFileNames.contains(initialRecipeSettingFileUsed) == false)
      initialRecipeSettingFileUsed = ProjectInitialRecipeSettings.getInstance().getSystemDefaultSetName();
    
     _initialRecipeSettingFileItem = new java.util.ArrayList<Object>();
     for (int i = 0; i < initialRecipeSettingFileNames.size(); i++)
       _initialRecipeSettingFileItem.add(initialRecipeSettingFileNames.get(i));
     
     _defaultInitialRecipeSetting = initialRecipeSettingFileUsed;
     
     _jointTypeRuleLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SELECT_JOINT_TYPE_RULE_SET_ON_IMPORT_KEY"));
     
     java.util.List<String> jointTypeAssignmentFileNames = FileName.getJointTypeAssignmentFiles();
      String jointTypeFileUsed = JointTypeEnumAssignment.getInstance().getJointTypeAssignmentConfigFileNameWithoutExtension();
      // just in case someone deleted the file to use from disk, add it to the list to show which file we'll attempt to use
      // and later the file not found exception will be caught and dealt with.
      if (jointTypeAssignmentFileNames.contains(jointTypeFileUsed) == false)
        jointTypeAssignmentFileNames.add(jointTypeFileUsed);
      
     _jointTypeAssignmentItems = new java.util.ArrayList<Object>();
     for (int i = 0; i < jointTypeAssignmentFileNames.size(); i++)
       _jointTypeAssignmentItems.add(jointTypeAssignmentFileNames.get(i));
     
     _defaultJointTypeAssignemnt = jointTypeFileUsed;

     _ndfTypeLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SELECT_NDF_TYPE_KEY"));
     
     java.util.List<String> ndfTypeAssignmentNames = getAllNdfTypeNames();
     String listTypeFileUsed = StringLocalizer.keyToString("MMGUI_SELECT_V810_NDF_TYPE_KEY");
     if (ndfTypeAssignmentNames.contains(listTypeFileUsed) == false)
     {
       ndfTypeAssignmentNames.add(listTypeFileUsed);
     }
     
     _ndfTypeItems = new java.util.ArrayList<Object>();
     for (int i = 0; i < ndfTypeAssignmentNames.size(); i++)
     {
       _ndfTypeItems.add(ndfTypeAssignmentNames.get(i));
     }

     _defaultNdfType = listTypeFileUsed;
           
    _systemTypeLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SELECT_SYSTEM_TYPE_ON_IMPORT_KEY"));
    
     java.util.List<String> supportedSystemType = new ArrayList<String>();
     try
     {
       supportedSystemType = LicenseManager.getInstance().getSupportedSystemType();
     }
     catch (final BusinessException e)
     {
       finishLicenseMonitor();
       SwingUtils.invokeLater(new Runnable()
       {
         public void run()
         {
           MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
             e.getMessage(),
             StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
             true);
           MainMenuGui.getInstance().exitV810(false);
         }
       });
     }
    
    _systemTypeItems = new java.util.ArrayList<Object>();
     for (int i = 0; i < supportedSystemType.size(); i++)
     {
       _systemTypeItems.add(supportedSystemType.get(i));
     }
    _currentSystemType = XrayTester.getSystemType().getName();
    if (_currentSystemType.equals(SystemTypeEnum.THROUGHPUT.getName()))
    {
      _currentSystemType = SystemTypeEnum.STANDARD.getName().toUpperCase();
    }

     
    _systemMagLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SELECT_LOW_MAG_ON_IMPORT_KEY"));
    
    _systemMagItems = new java.util.ArrayList<Object>();
    String[] systemLowMag = SystemConfigEnum.getLowMagnificationNames();
    for (int i = 0 ; i < systemLowMag.length;i++)
    {
      _systemMagItems.add(systemLowMag[i]);
    }

    _currentSystemMagnification = Config.getSystemCurrentLowMagnification();
         
     _mainUI = frame;
     _instruction = StringLocalizer.keyToString("MM_GUI_IMPORT_PROJECT_MESSAGE_KEY");
     _buttonFont = FontUtil.getDialogButtonFont();
     _messageFont = FontUtil.getDialogMessageFont();

     if(ndfProject.isEmpty() == false)
       _okButton.setEnabled(ndfProject.size() > 0);
     else
       _okButton.setEnabled(true);


      if(ndfProjects5dx.isEmpty() == false)
       _okButton.setEnabled(ndfProjects5dx.size()> 0);
      else
      _okButton.setEnabled(true);
     
     if(ndfProject.isEmpty() == false)
     {
       for (int i = 0; i < ndfProject.size(); i++)
         _choicesListModel.addElement(ndfProject.get(i));
     }

      if(ndfProjects5dx.isEmpty() == false)
     {
       for (int i = 0; i < ndfProjects5dx.size(); i++)
         _choicesListModel2.addElement(ndfProjects5dx.get(i));
     }

     _choicesList.setSelectedIndex(0);
     _choicesList2.setSelectedIndex(0);

     createDialog();
     pack();
  }
}
