package com.axi.v810.gui;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.util.*;

/**
 * <p>Title: JointResultsEntry</p>
 * <p>Description: This class encapsulates inspection results information used by GUI.
 * The are two flavors of inspection results:<br>
 * Joint Results are related to device.pins<br>
 * Component Results are failed components</p>
 * <p>Both result types have the same data except component results do not have:<br>
 * PadName<br>
 * JointType<br>
 * Subtype</p>
 * <p>The methods here will return the correct data for the entry. For components, padName,
 * jointTypeName and subtype return a blank string, jointTypeEnum returns null.</p>
 * @author Rex Shang
 */
public class JointResultsEntry
{
  private Long _runStartTime;
  private boolean _hasJointResults = false;
  private JointResults _jointResults;
  private boolean _hasComponentResults = false;
  private ComponentResults _componentResults;
  private String _componentJointTypeName = null;
  private String _componentSubtypeName = null;
  private String _boardSerialNumber = null;
  private boolean _componentLevelTest = false;
  private int _numberOfDefects = 0;
  private String _measurementName;
  private String _sliceName;
  private String _measurementUnit;
  //Lim, Lay Ngor - Fix bug: Show non-resize & non-enhance image & roi location for short
  //base on selected measurement type in Measurements Tab.
  private MeasurementEnum _measurementEnum;

  public static String NO_NAME = StringLocalizer.keyToString("RMGUI_COMPONENT_LEVEL_KEY");
  public static String _MIXED = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
  
  private List<JointTypeEnum> _jointTypeEnums = new ArrayList();
  private List<Subtype> _subtypes = new ArrayList();
  
  /**
   * @author Rex Shang
   */
  public JointResultsEntry()
  {
    // do nothing
  }

  /**
   * asserts if run time is not set or does not have either joint results or component results
   * @author George Booth
   */
  public void isValid()
  {
    Assert.expect(_runStartTime.longValue() > 0, "JointResultsEntry run time not set");
    Assert.expect((_hasJointResults == false && _hasComponentResults) ||
                  (_hasJointResults && _hasComponentResults == false), "JointResultsEntry has no or both result types");
  }

  /**
   * @author Rex Shang
   */
  public void setRunStartTime(Long runStartTime)
  {
    Assert.expect(runStartTime != null);

    _runStartTime = runStartTime;
  }

  /**
   * @author Rex Shang
   */
  public Long getRunStartTime()
  {
    Assert.expect(_runStartTime != null, "Run start time is null");

    return _runStartTime;
  }

  /**
   * @author Rex Shang
   */
  public void setJointResults(JointResults jointResults)
  {
    Assert.expect(jointResults != null);
    Assert.expect(_hasComponentResults == false);

    _hasJointResults = true;
    _jointResults = jointResults;
  }

  /**
   * @author George Booth
   */
  public boolean isJointResult()
  {
    return _hasJointResults;
  }

  /**
   * @author Rex Shang
   */
  public JointResults getJointResults()
  {
    Assert.expect(_jointResults != null, "Joint result is null");

    return _jointResults;
  }

  /**
   * @author George Booth
   */
  public void setComponentResults(ComponentResults componentResults)
  {
    Assert.expect(componentResults != null);
    Assert.expect(_hasJointResults == false);

    _hasComponentResults = true;
    _componentResults = componentResults;
    String boardName = componentResults.getComponentName().getBoardName();
    String refDesig = componentResults.getComponentName().getReferenceDesignator();

    List<MeasurementResult> measurementResults = componentResults.getAllMeasurementResults();
    //Siew Yeng - XCR-2535 - Inconsistent display of Component Voiding Area at Measurement Type for mix Subtype
    List<String> subtypeLongNames = new ArrayList();
    // a component level test has one or more measurement enums that are not Locator X or Y locations
    for (MeasurementResult measurementResult : measurementResults)
    {
      if (measurementResult.getMeasurementEnum().equals(MeasurementEnum.LOCATOR_X_LOCATION) == false &&
          measurementResult.getMeasurementEnum().equals(MeasurementEnum.LOCATOR_Y_LOCATION) == false)
      {
        _componentLevelTest = true;
        //Siew Yeng - add all related subtype name for this component result into list
        if(subtypeLongNames.contains(measurementResult.getSubtypeLongName()) == false)
          subtypeLongNames.add(measurementResult.getSubtypeLongName());
//        break;
      }
    }

    // get component JointType and Subtype names
    Board board = Project.getCurrentlyLoadedProject().getPanel().getBoard(getBoardName());
    ComponentType componentType = board.getComponentType(refDesig);
    if (componentType.getCompPackage().usesOneJointTypeEnum())
    {
      _componentJointTypeName = componentType.getCompPackage().getJointTypeEnum().getName();
    }
    else
    {
      _componentJointTypeName = _MIXED;
      //Siew Yeng - If is MIXED jointType, add all related joint type into list
      //the list is use to determine whether this component result should be displayed.
      for(Subtype subtype : componentType.getSubtypes())
      {
        if(subtypeLongNames.contains(subtype.getLongName()))
        {
          if(_jointTypeEnums.contains(subtype.getJointTypeEnum()) == false)
            _jointTypeEnums.add(subtype.getJointTypeEnum());
        }
      }
    }
    if (componentType.getComponentTypeSettings().usesOneSubtype())
    {
      _componentSubtypeName = componentType.getComponentTypeSettings().getSubtype().getShortName();
    }
    else
    {
      _componentSubtypeName = _MIXED;
      //Siew Yeng - If is MIXED subtype, add all related subtype into list
      //the list is use to determine whether this component result should be displayed.
      for(Subtype subtype : componentType.getSubtypes())
      {
        if(subtypeLongNames.contains(subtype.getLongName()))
        {
          if(_subtypes.contains(subtype) == false)
            _subtypes.add(subtype);
        }
      }
    }
    subtypeLongNames.clear();
  }

  /**
   * @author George Booth
   */
  public boolean isComponentResult()
  {
    return _hasComponentResults;
  }

  /**
   * @author George Booth
   */
  public boolean isComponentLevelTest()
  {
    return _componentLevelTest;
  }

  /**
   * @author George Booth
   */
  public ComponentResults getComponentResults()
  {
    Assert.expect(_hasComponentResults == true);
    Assert.expect(_componentResults != null);
    return _componentResults;
  }

  /**
   * @author George Booth
   */
  public void setNumberOfDefects(int numberOfDefects)
  {
    _numberOfDefects = numberOfDefects;
  }

  /**
   * @author George Booth
   */
  public int getNumberOfDefects()
  {
    return _numberOfDefects;
  }

  /**
   * @author George Booth
   */
  public String getBoardName()
  {
    Assert.expect(_hasJointResults || _hasComponentResults, "JointResultsEntry had no results");
    if (_hasJointResults)
    {
      return _jointResults.getJointName().getBoardName();
    }
    else
    {
      return _componentResults.getComponentName().getBoardName();
    }
  }

  /**
   * @author George Booth
   */
  public String getReferenceDesignator()
  {
    Assert.expect(_hasJointResults || _hasComponentResults, "JointResultsEntry had no results");
    if (_hasJointResults)
    {
      return _jointResults.getJointName().getReferenceDesignator();
    }
    else
    {
      return _componentResults.getComponentName().getReferenceDesignator();
    }
  }

  /**
   * @author George Booth
   */
  public String getPadName()
  {
    Assert.expect(_hasJointResults || _hasComponentResults, "JointResultsEntry had no results");
    if (_hasJointResults)
    {
      return _jointResults.getJointName().getPadName();
    }
    else
    {
      return NO_NAME;
    }
  }

  /**
   * @author George Booth
   */
  public String getJointTypeName()
  {
    Assert.expect(_hasJointResults || _hasComponentResults, "JointResultsEntry had no results");
    if (_hasJointResults)
    {
      return _jointResults.getJointTypeEnum().getName();
    }
    else
    {
      return _componentJointTypeName;
    }
  }

  /**
   * @author George Booth
   */
  public String getSubtypeName()
  {
    Assert.expect(_hasJointResults || _hasComponentResults, "JointResultsEntry had no results");
    if (_hasJointResults)
    {
      return _jointResults.getSubtypeShortName();
    }
    else
    {
      return _componentSubtypeName;
    }
  }

  /**
   * @author George Booth
   */
  public boolean passed()
  {
    Assert.expect(_hasJointResults || _hasComponentResults, "JointResultsEntry had no results");
    if (_hasJointResults)
    {
      return _jointResults.passed();
    }
    else
    {
      return _componentResults.passed();
    }
  }

  /**
   * @author George Booth
   */
  public boolean isTopSide()
  {
    Assert.expect(_hasJointResults || _hasComponentResults, "JointResultsEntry had no results");
    if (_hasJointResults)
    {
      return _jointResults.isTopSide();
    }
    else
    {
      return _componentResults.isTopSide();
    }
  }

  /**
   * @author George Booth
   */
  public List<MeasurementResult> getAllMeasurementResults()
  {
    Assert.expect(_hasJointResults || _hasComponentResults, "JointResultsEntry had no results");
    if (_hasJointResults)
    {
      return _jointResults.getAllMeasurementResults();
    }
    else
    {
      return _componentResults.getAllMeasurementResults();
    }
  }

  /**
   * @author George Booth
   */
  public List<MeasurementResult> getFailingMeasurementResults()
  {
    Assert.expect(_hasJointResults || _hasComponentResults, "JointResultsEntry had no results");
    if (_hasJointResults)
    {
      return _jointResults.getFailingMeasurementResults();
    }
    else
    {
      return _componentResults.getFailingMeasurementResults();
    }
  }

  /**
   * @author George Booth
   */
  public List<MeasurementResult> getRelatedMeasurementResults()
  {
    Assert.expect(_hasJointResults || _hasComponentResults, "JointResultsEntry had no results");
    if (_hasJointResults)
    {
      return _jointResults.getRelatedMeasurementResults();
    }
    else
    {
      return _componentResults.getRelatedMeasurementResults();
    }
  }

  /**
   * @author George Booth
   */
  public List<IndictmentResult> getIndictmentResults()
  {
    Assert.expect(_hasJointResults || _hasComponentResults, "JointResultsEntry had no results");
    if (_hasJointResults)
    {
      return _jointResults.getIndictmentResults();
    }
    else
    {
      return _componentResults.getIndictmentResults();
    }
  }
  
  /**
   * @author Jack Hwee
   */
  public void setBoardSerialNumber(String boardSerialNumber)
  {
    Assert.expect(boardSerialNumber != null);

    _boardSerialNumber = boardSerialNumber;
  }

  /**
   * @author Rex Shang
   */
  public String getBoardSerialNumber()
  {
    Assert.expect(_boardSerialNumber != null, "boardSerialNumberis null");

    return _boardSerialNumber;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void setMeasurementName(String measurementName)
  {
    Assert.expect(measurementName != null);
    _measurementName = measurementName;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public String getMeasurementName()
  {
    return _measurementName;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasMeasurementName()
  {
    if (_measurementName == null)
      return false;
    else
      return true;
  }
  
  /**
   * @author Lim, Lay Ngor
   */
  public void setMeasurementEnum(MeasurementEnum measurementEnum)
  {
    Assert.expect(measurementEnum != null);
    _measurementEnum = measurementEnum;
  }
  
  /**
   * @author Lim, Lay Ngor
   */
  public MeasurementEnum getMeasurementEnum()
  {
    return _measurementEnum;
  }
  
  /**
   * @author Lim, Lay Ngor 
   */
  public boolean hasMeasurementEnum()
  {
    if (_measurementEnum == null)
      return false;
    else
      return true;
  }  
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setSliceName(String sliceName)
  {
    Assert.expect(sliceName != null);
    _sliceName = sliceName;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public String getSliceName()
  {
    return _sliceName;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasSliceName()
  {
    if (_sliceName == null)
      return false;
    else
      return true;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setMeasurementUnit(String measurementUnit)
  {
    Assert.expect(measurementUnit != null);
    _measurementUnit = measurementUnit;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public String getMeasurementUnit()
  {
    return _measurementUnit;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasMeasurementUnit()
  {
    if (_measurementUnit == null)
      return false;
    else
      return true;
  }
  
  /**
   * @author Siew Yeng
   */
  public List<JointTypeEnum> getJointTypeEnums()
  {
    Assert.expect(_jointTypeEnums != null);
   
    return _jointTypeEnums;
  }
  
  /**
   * @author Siew Yeng
   */
  public List<Subtype> getSubtypes()
  {
    Assert.expect(_subtypes != null);
    
    return _subtypes;
  }
}
