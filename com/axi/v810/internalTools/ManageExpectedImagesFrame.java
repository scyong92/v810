package com.axi.v810.internalTools;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;
import java.util.List;

import javax.imageio.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.util.image.Image;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.algorithmLearning.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;


public class ManageExpectedImagesFrame extends JFrame implements ListSelectionListener
{
  private JPanel _contentPane;
  private JPanel _westPanel;
  private JPanel _projectPanel;
  private JPanel _projectDirectoryPanel;
  private JTextField _projectDirectoryTextField;
  private JButton _projectDirectoryChooserButton;
  private JPanel _projectNamePanel;
  private JTextField _projectNameTextField;
  private JButton _projectNameChooserButton;
  private JPanel _imageListPanel;
  private JList _imageList;
  private JScrollPane _imageListScrollPane = null;
  private DefaultListModel _imageListModel;
  private JPanel _imageControlPanel;
  private JButton _saveButton;
  private JButton _loadButton;
  private JButton _exitButton;
  private JPanel _centerPanel;
  private JPanel _imageDisplayPanel;
  private ManageExpectedImagesRenderer _imageRenderer;
  private JPanel _imageStatusPanel;
  private JLabel _imageStatusLabel;

  private Project _project;
  private List<LearnedData> _learnedDataList;
  private AlgorithmExpectedImageLearningReaderWriter _expectedImageLearningRW;
  private boolean _expectedImageLearningFileIsValid = true;

  private String _projectDirectory = "";
  private String _projectName = "";
  private String _imageName = "";
  private Image _image;

  private int _imageWidth;
  private int _imageHeight;
  private Dimension _imageDimension;
  private BufferedImage _blankImage;

  private final Dimension BUTTON_DIMENSION = new Dimension(200, 30);

  private Font _dialogFont = FontUtil.getDefaultFont();
  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
  boolean _projectLoaded = false;
  boolean _validImages = false;

  public ManageExpectedImagesFrame()
  {
    try
    {
      setDefaultCloseOperation(EXIT_ON_CLOSE);

      Config.getInstance().loadIfNecessary();

      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * Component initialization.
   *
   * @throws java.lang.Exception
   */
  private void jbInit() throws Exception
  {
    _contentPane = (JPanel)getContentPane();
    _contentPane.setLayout(new BorderLayout());
    setSize(new Dimension(1024, 768));
    setTitle("Manage Expected Images");

    _imageWidth = 500;
    _imageHeight = 360;
    _imageDimension = new Dimension(_imageWidth, _imageHeight);
    _blankImage = createNewImage(_imageWidth, _imageHeight);

    _westPanel = new JPanel();
    _westPanel.setLayout(new BorderLayout());
//    _westPanel.setBorder(BorderFactory.createCompoundBorder(
//        new TitledBorder(BorderFactory.createEtchedBorder(
//            Color.white, new Color(165, 163, 151)),
//                         "west Panel"),
//        BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _contentPane.add(_westPanel, BorderLayout.WEST);

    _projectPanel = new JPanel();
    _projectPanel.setLayout(new GridLayout(2, 1));
//    _projectPanel.setBorder(BorderFactory.createCompoundBorder(
//        new TitledBorder(BorderFactory.createEtchedBorder(
//            Color.white, new Color(165, 163, 151)),
//                         ""),
//        BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _westPanel.add(_projectPanel, BorderLayout.NORTH);

    // project directory selection
    _projectDirectoryPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));
    _projectDirectoryPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(
        Color.white, new Color(165, 163, 151)),
                       "Project Directory"),
      BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _projectPanel.add(_projectDirectoryPanel);

    _projectDirectory = Directory.getProjectsDir();
    _projectDirectoryTextField = new JTextField(_projectDirectory);
    _projectDirectoryTextField.setToolTipText("Set the project directory");
    _projectDirectoryPanel.add(_projectDirectoryTextField);

    _projectDirectoryChooserButton = new JButton("...");
    _projectDirectoryChooserButton.setToolTipText("Browse for the project directory");
    _projectDirectoryPanel.add(_projectDirectoryChooserButton);

    // hook up project directory selection action listeners
    _projectDirectoryTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        projectDirectoryTextField_setValue();
      }
    });

    _projectDirectoryTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        projectDirectoryTextField_setValue();
      }
    });

    _projectDirectoryChooserButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        projectDirectoryChooser();
      }
    });

    // project name selection
    _projectNamePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));
    _projectNamePanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(
        Color.white, new Color(165, 163, 151)),
                       "Project Name"),
      BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _projectPanel.add(_projectNamePanel);

    _projectNameTextField = new JTextField();
    _projectNameTextField.setPreferredSize(_projectDirectoryTextField.getPreferredSize());
    _projectNameTextField.setToolTipText("Set the project name");
    _projectNamePanel.add(_projectNameTextField);

    _projectNameChooserButton = new JButton("...");
    _projectNameChooserButton.setToolTipText("Browse for the project name");
    _projectNamePanel.add(_projectNameChooserButton);

    // hook up project name selection action listeners
    _projectNameTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        projectNameTextField_setValue();
      }
    });

    _projectNameTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        projectNameTextField_setValue();
      }
    });

    _projectNameChooserButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        projectNameChooser();
      }
    });


    _imageListPanel = new JPanel();
    _imageListPanel.setLayout(new BorderLayout());
    _imageListPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(
            Color.white, new Color(165, 163, 151)),
                         "Pads With Expected Images"),
        BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _westPanel.add(_imageListPanel, BorderLayout.CENTER);

    _imageListModel = new DefaultListModel();
    _imageList = new JList(_imageListModel);
    _imageList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _imageListScrollPane = new JScrollPane();
    _imageListScrollPane.getViewport().add(_imageList);
    _imageListPanel.add(_imageListScrollPane, BorderLayout.CENTER);


    _imageControlPanel = new JPanel();
    _imageControlPanel.setLayout(new GridLayout(3, 1, 0, 5));
    _imageControlPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(
            Color.white, new Color(165, 163, 151)),
                         "Program Controls"),
        BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _westPanel.add(_imageControlPanel, BorderLayout.SOUTH);

    _saveButton = new JButton("Save Image to Disk");
    _saveButton.setToolTipText("Saves the displayed image to disk for editing with an image editor of your choice");
    _saveButton.setPreferredSize(BUTTON_DIMENSION);
    _saveButton.setEnabled(false);
    _imageControlPanel.add(_saveButton);

    _saveButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveButton_ActionPerformed();
      }
    });

    _loadButton = new JButton("Load Image from Disk");
    _loadButton.setToolTipText("Read the edited image from disk and update the Expected Image database");
    _loadButton.setPreferredSize(BUTTON_DIMENSION);
    _loadButton.setEnabled(false);
    _imageControlPanel.add(_loadButton);

    _loadButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        loadButton_ActionPerformed();
      }
    });

    _exitButton = new JButton("Exit");
    _exitButton.setPreferredSize(BUTTON_DIMENSION);
    _imageControlPanel.add(_exitButton);

    _exitButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        closeExpectedImageLearningReaderWriter();
        System.exit(0);
      }
    });

    _centerPanel = new JPanel();
    _centerPanel.setLayout(new BorderLayout());
//    _centerPanel.setBorder(BorderFactory.createCompoundBorder(
//        new TitledBorder(BorderFactory.createEtchedBorder(
//            Color.white, new Color(165, 163, 151)),
//                         "centerPanel"),
//        BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _contentPane.add(_centerPanel, BorderLayout.CENTER);

    _imageDisplayPanel = new JPanel();
    _imageDisplayPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(
            Color.white, new Color(165, 163, 151)),
                         "Image: (none)"),
        BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _centerPanel.add(_imageDisplayPanel, BorderLayout.CENTER);
    
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    setDebugIfNecessary();
    _imageRenderer = new ManageExpectedImagesRenderer(null, _imageName);
    _imageRenderer.setPreferredSize(new Dimension(400, 320));
    _imageDisplayPanel.add(_imageRenderer, BorderLayout.CENTER);

    _imageStatusPanel = new JPanel();
    _imageStatusPanel.setLayout(new BorderLayout());
    _imageStatusPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(
            Color.white, new Color(165, 163, 151)),
                         "Status"),
        BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _centerPanel.add(_imageStatusPanel, BorderLayout.SOUTH);

    _imageStatusLabel = new JLabel();
    _imageStatusLabel.setPreferredSize(BUTTON_DIMENSION);
    _imageStatusPanel.add(_imageStatusLabel, BorderLayout.CENTER);

    // initial conditions
    Directory.setProjectsDir(_projectDirectory);
    _imageStatusLabel.setText("Ready to select a project name");

  }

  public void valueChanged(ListSelectionEvent e)
  {
//    System.out.println("valueChanged()");
    JList list = (JList)e.getSource();
    _imageName = (String)list.getSelectedValue();
//    System.out.println("  New imageName=" + _imageName);
    //  update image
    if (_image != null)
    {
      _image.decrementReferenceCount();
    }
    _image = getImage(_imageName);
    displayImage(_image);
    _imageStatusLabel.setText("Ready to operate on image \"" + _imageName + "\"");
  }

  private void saveButton_ActionPerformed()
  {
    String imagePath = "c:/temp/" + _imageName + ".png";
    saveImageAsPngFile(_image, imagePath);
    _imageStatusLabel.setText("Image \"" + _imageName + "\" has been saved to disk as \"" + imagePath + "\"");
  }

  private void loadButton_ActionPerformed()
  {
    String imagePath = "c:/temp/" + _imageName + ".png";
    Image newImage = loadPngImageFromFile(imagePath);
    if (newImage != null)
    {
      if (newImage.getWidth() == _image.getWidth() && newImage.getHeight() == _image.getHeight())
      {
        int width = newImage.getWidth();
        int height = newImage.getHeight();
        RegionOfInterest imageRoi = new RegionOfInterest(0, 0, width, height, 0, RegionShapeEnum.RECTANGULAR);
        _image = Image.createCopy(newImage, imageRoi);
        newImage.decrementReferenceCount();
        displayImage(_image);
        setImage(_image, _imageName);
        _imageStatusLabel.setText("Learning database has been updated with image \"" + _imageName + "\" from disk");
      }
      else
      {
        showMessageDialog("Image being loaded does not match size of reference image, load terminated");
      }
    }
  }

  private void projectDirectoryChooser()
  {
    JFileChooser fileChooser = new JFileChooser(_projectDirectoryTextField.getText());
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (fileChooser.showDialog(this, "Select Project Directory") != JFileChooser.APPROVE_OPTION)
      return;
    File selectedDir = fileChooser.getSelectedFile();
    String dirName = selectedDir.getAbsolutePath();
    _projectDirectoryTextField.setText(dirName);
    projectDirectoryTextField_setValue();
  }

  private void projectDirectoryTextField_setValue()
  {
    if (_projectDirectoryTextField.getText().equals(_projectDirectory))
    {
      // nothing worth doing
      return;
    }
    _projectDirectory = _projectDirectoryTextField.getText();
    try
    {
//      System.out.println("Project directory = " + _projectDirectory);
      Directory.setProjectsDir(_projectDirectory);
      // clear out project name - new directory
      _projectNameTextField.setText("");
      _imageListModel.clear();
      if (_project != null)
      {
        // can't close it - oh well
      }
      // disable list selection
      _imageList.removeListSelectionListener(this);
      _imageStatusLabel.setText("Project directory has been set to \"" + _projectDirectory + "\". Ready to select a project name.");
      // make sure learning RW is updated and closed
      closeExpectedImageLearningReaderWriter();
    }
    catch (DatastoreException dse)
    {
      handleDatastoreException(dse);
    }
  }

  private void projectNameChooser()
  {
    JFileChooser fileChooser = new JFileChooser(_projectDirectoryTextField.getText());
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (fileChooser.showDialog(this, "Open Project") != JFileChooser.APPROVE_OPTION)
      return;
    File selectedProject = fileChooser.getSelectedFile();
    String fileName = selectedProject.getName();
    _projectNameTextField.setText(fileName);
    projectNameTextField_setValue();
  }

  private void projectNameTextField_setValue()
  {
    if (_projectNameTextField.getText().equals(""))
    {
      // nothing worth doing
      return;
    }
    // make sure learning RW is updated and closed
    closeExpectedImageLearningReaderWriter();

    _projectName = _projectNameTextField.getText();
    loadProject();
    if (_projectLoaded)
    {
      getImageList();
      if (_validImages)
      {
        _expectedImageLearningFileIsValid = true;
        _imageList.addListSelectionListener(this);
        _imageStatusLabel.setText("Project \"" + _projectName + "\" has been loaded. Ready to select an image.");
      }
    }
  }

  private void loadProject()
  {
    _projectLoaded = false;
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(
        this,
        "Loading project...",
        "Manage Expected Images",
        true);
    SwingUtils.setFont(busyCancelDialog, _dialogFont);
    busyCancelDialog.setPreferredSize(new Dimension(300, 100));
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, this);

    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          BooleanRef abortedDuringLoad = new BooleanRef();
          _project = Project.load(_projectName, abortedDuringLoad);
          if(abortedDuringLoad.getValue())
            _projectLoaded = false;
          else
            _projectLoaded = true;
        }
        catch (final DatastoreException ex)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              handleDatastoreException(ex);
            }
          });
        }
        finally
        {
          // wait until visible
          while (busyCancelDialog.isVisible() == false)
          {
            try
            {
              Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
          busyCancelDialog.setVisible(false);
        }
      }
    });
    busyCancelDialog.setVisible(true);
  }


  private void getImageList()
  {
    _validImages = false;
    final BusyCancelDialog busyCancelDialog = new BusyCancelDialog(
        this,
        "Reading expected image data...",
        "Manage Expected Images",
        true);
    SwingUtils.setFont(busyCancelDialog, _dialogFont);
    busyCancelDialog.setPreferredSize(new Dimension(300, 100));
    busyCancelDialog.pack();
    SwingUtils.centerOnComponent(busyCancelDialog, this);

    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        _imageListModel.clear();
        // get list of pads in project
        _learnedDataList = new ArrayList<LearnedData>();
        com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
        List<Pad> padList = panel.getPads();
        for (Pad pad : padList)
        {
          if (pad.getJointTypeEnum().equals(JointTypeEnum.EXPOSED_PAD))
          {
            SliceNameEnum padSliceNameEnum = SliceNameEnum.PAD;
            PadSettings padSetting = pad.getPadSettings();
            // make sure we have access to Expected Image learning
            try
            {
              padSetting.openExpectedImageLearning();
              if (padSetting.hasExpectedImageLearning(padSliceNameEnum))
              {
                LearnedData learnedData = new LearnedData(pad, padSliceNameEnum,
                                          pad.getComponent().getBoard().getBoardType().getName() + " " +
                                          pad.getComponent().getReferenceDesignator() + " " +
                                          pad.getName());

//                System.out.print("pad \"" + pad.getComponent().getBoard().getBoardType().getName() + " " +
//                                          pad.getComponent().getReferenceDesignator() + " " +
//                                          pad.getName() + "\" is ");

                if (_imageListModel.contains(learnedData.getName()) == false)
                {
//                  System.out.println("NOT in the list");
                  _learnedDataList.add(learnedData);
                  _imageListModel.addElement(learnedData.getName());
                }
                else
                {
//                  System.out.println("in the list");
                }
              }
            }
            catch (DatastoreException dse)
            {
              handleDatastoreException(dse);
              // wait until visible
              while (busyCancelDialog.isVisible() == false)
              {
                try
                {
                  Thread.sleep(200);
                }
                catch (InterruptedException ex)
                {
                  // do nothing
                }
              }
              busyCancelDialog.setVisible(false);
              return;
            }
          }
        }
        _validImages = true;
        // wait until visible
        while (busyCancelDialog.isVisible() == false)
        {
          try
          {
            Thread.sleep(200);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }
        }
        busyCancelDialog.setVisible(false);
      }
    });
    busyCancelDialog.setVisible(true);
  }

  /**
   * @author Khaw Chek Hau
   * XCR2183: Standardize all print out when developer debug is true
   */  
  static public void setDebugIfNecessary()
  {
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.IMAGE_RENDERER_DEBUG))
      ManageExpectedImagesRenderer.setDebug(true);
  }

  private void setImage(Image image, String imageName)
  {
    try
    {
      // get reference to expected image database
      _expectedImageLearningRW = _project.getAlgorithmExpectedImageLearningReaderWriter();
//      System.out.println("Setting " + imageName);

      // update expected image
      for (LearnedData learnedData : _learnedDataList)
      {
        if (learnedData.getName().equals(imageName))
        {
          // make sure we have access to Expected Image learning
          PadSettings padSetting = learnedData.getPad().getPadSettings();
          padSetting.openExpectedImageLearning();

          // get current expected image learning
          ExpectedImageLearning expectedImageLearning =
              _expectedImageLearningRW.readExpectedImageLearning(learnedData.getPad(), learnedData.getSliceNameEnum());

          // convert image to pixel array
          int width = image.getWidth();
          int height = image.getHeight();
          RegionOfInterest imageRoi = new RegionOfInterest(0, 0, width, height, 0, RegionShapeEnum.RECTANGULAR);
          float[] imagePixelArray = Image.createArrayFromImage(image, imageRoi);

          // scale thickness values
          float scaleFactor = 1000.0f;
          int numTested = 0;
          int pixelIndex = 0;
          for (int y = 0; y < height; y++)
          {
            for (int x = 0; x < width; x++)
            {
              if (imagePixelArray[pixelIndex] > 0.0f)
              {
                numTested++;
              }
              imagePixelArray[pixelIndex] = imagePixelArray[pixelIndex] / scaleFactor;

              pixelIndex++;
            }
          }
          float maxValue = ArrayUtil.max(imagePixelArray);
//          System.out.println("  maxValue = " + maxValue + ", scaleFactor = " + scaleFactor);

          // update expected image learning with new image
          expectedImageLearning.setCompositeImageArray(imagePixelArray);
          expectedImageLearning.setNumberOfExpectedImagesLearned(1);
          expectedImageLearning.setNumberOfTestedPixels(numTested);
          _expectedImageLearningRW.writeExpectedImageLearning(expectedImageLearning);

          _expectedImageLearningFileIsValid = false;
          return;
        }
      }
      return;
    }
    catch (DatastoreException dse)
    {
      handleDatastoreException(dse);

    }
    return;
  }

  private Image getImage(String imageName)
  {
    try
    {
      // get reference to expected image database
      _expectedImageLearningRW = _project.getAlgorithmExpectedImageLearningReaderWriter();
//      System.out.println("Getting " + imageName);

      // get expected image
      for (LearnedData learnedData : _learnedDataList)
      {
        if (learnedData.getName().equals(imageName))
        {
          // make sure we have access to Expected Image learning
          PadSettings padSetting = learnedData.getPad().getPadSettings();
          padSetting.openExpectedImageLearning();

          ExpectedImageLearning expectedImageLearning =
              _expectedImageLearningRW.readExpectedImageLearning(learnedData.getPad(), learnedData.getSliceNameEnum());
          float[] imagePixelArray = expectedImageLearning.getCompositeImageArray();
          int width = expectedImageLearning.getImageWidth();
          int height = expectedImageLearning.getImageHeight();
          // scale thickness values
          float maxValue = ArrayUtil.max(imagePixelArray);
          float scaleFactor = 1000.0f;
//          System.out.println("  maxValue = " + maxValue + ", scaleFactor = " + scaleFactor);
          int pixelIndex = 0;
          for (int y = 0; y < height; y++)
          {
            for (int x = 0; x < width; x++)
            {
              imagePixelArray[pixelIndex] = imagePixelArray[pixelIndex] * scaleFactor;

              pixelIndex++;
            }
          }
          RegionOfInterest imageRoi = new RegionOfInterest(0, 0, width, height, 0, RegionShapeEnum.RECTANGULAR);
          Image image = Image.createFloatImageFromArray(width, height, imagePixelArray);
//          _expectedImageLearningRW.close();
          return image;
        }
      }
//      _expectedImageLearningRW.close();
      return null;
    }
    catch (DatastoreException dse)
    {
      handleDatastoreException(dse);

    }
    return null;
  }

  private void displayImage(Image image)
  {
    if (_image != null)
    {
      BufferedImage bufferedImage = image.getBufferedImage();
      _imageRenderer.updateImage(bufferedImage);
      _imageRenderer.repaint();
      _imageDisplayPanel.setBorder(BorderFactory.createCompoundBorder(
          new TitledBorder(BorderFactory.createEtchedBorder(
              Color.white, new Color(165, 163, 151)),
                "Image: " + _imageName + " (" + _image.getWidth() + "x" + _image.getHeight() + ")"),
          BorderFactory.createEmptyBorder(0, 5, 5, 5)));
      _saveButton.setEnabled(true);
      _loadButton.setEnabled(true);
    }
    else
    {
      if (_image != null)
      {
        _image.decrementReferenceCount();
      }
      _imageRenderer.updateImage(_blankImage);
      _imageRenderer.repaint();
      _imageDisplayPanel.setBorder(BorderFactory.createCompoundBorder(
          new TitledBorder(BorderFactory.createEtchedBorder(
              Color.white, new Color(165, 163, 151)),
                           "Image: (blank)"),
          BorderFactory.createEmptyBorder(0, 5, 5, 5)));
      _saveButton.setEnabled(false);
      _loadButton.setEnabled(false);
    }
  }

  private void saveAllImages()
  {
    try
    {
      // get reference to expected image database
      _expectedImageLearningRW = _project.getAlgorithmExpectedImageLearningReaderWriter();

      // get expected images
      for (LearnedData learnedData : _learnedDataList)
      {
//        System.out.println("Saving " + learnedData.getName());
        ExpectedImageLearning expectedImageLearning =
            _expectedImageLearningRW.readExpectedImageLearning(learnedData.getPad(), learnedData.getSliceNameEnum());
        float[] imagePixelArray = expectedImageLearning.getCompositeImageArray();
        int width = expectedImageLearning.getImageWidth();
        int height = expectedImageLearning.getImageHeight();
        // scale thickness values
        float maxValue = ArrayUtil.max(imagePixelArray);
        float scaleFactor = 1000.0f;
//        System.out.println("  maxValue = " + maxValue + ", scaleFactor = " + scaleFactor);
        int pixelIndex = 0;
        for (int y = 0; y < height; y++)
        {
          for (int x = 0; x < width; x++)
          {
            imagePixelArray[pixelIndex] = imagePixelArray[pixelIndex] * scaleFactor;

            pixelIndex++;
          }
        }
        RegionOfInterest imageRoi = new RegionOfInterest(0, 0, width, height, 0, RegionShapeEnum.RECTANGULAR);
        Image image = Image.createFloatImageFromArray(width, height, imagePixelArray);
        saveImageAsPngFile(image, "c:/temp/" + learnedData.getName() + ".png");
        image.decrementReferenceCount();
      }
//      _expectedImageLearningRW.close();
    }
    catch (DatastoreException dse)
    {
      handleDatastoreException(dse);

    }
  }

  private BufferedImage createNewImage(int imageWidth,
                               int imageHeight)
  {
    // initialize a new expected image
    BufferedImage expectedImage = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_BYTE_GRAY);

    for (int x = 0; x < imageWidth; x++)
    {
      for (int y = 0; y < imageHeight; y++)
      {
        expectedImage.setRGB(x, y, 0);
      }
    }

    return expectedImage;
  }

  private Image loadPngImageFromFile(String imagePath)
  {
    try
    {
      File imageFile = new File(imagePath);
      if (imageFile.isFile())
      {
        BufferedImage bufferedImage = ImageIO.read(imageFile);
        Image image = Image.createFloatImageFromBufferedImage(bufferedImage);
        return image;
      }
      else
      {
        showMessageDialog("Image file \"" + imagePath + "\" not found");
      }
    }
    catch (IOException ioe)
    {
      showMessageDialog(ioe.getMessage());
    }
    return null;
  }


  private void saveImageAsPngFile(Image image, String imagePath)
  {
    try
    {
      ImageIoUtil.savePngImage(image, imagePath);
    }
    catch(CouldNotCreateFileException fe)
    {
      showMessageDialog(fe.getMessage());
    }
  }

  private void handleDatastoreException(DatastoreException dse)
  {
    showMessageDialog(dse.getMessage());
    _imageStatusLabel.setText("Error ocurred. Processing terminated.");

  }

  private void closeExpectedImageLearningReaderWriter()
  {
    try
    {
      if (_expectedImageLearningRW != null)
      {
        _expectedImageLearningRW = _project.getAlgorithmExpectedImageLearningReaderWriter();
        if (_expectedImageLearningRW.isOpen())
        {
          _expectedImageLearningRW.close();
        }

        String expectedImageLearningFile = FileName.getAlgorithmExpectedImageLearningFullPath(_projectName);
        String expectedImageLearningProjOpenFile = FileName.getAlgorithmExpectedImageLearningTempProjOpenFullPath(_projectName);
        if (FileUtilAxi.exists(expectedImageLearningProjOpenFile))
        {
          if (_expectedImageLearningFileIsValid == false)
            FileUtilAxi.copy(expectedImageLearningProjOpenFile, expectedImageLearningFile);
        }
        else
        {
          if (FileUtilAxi.exists(expectedImageLearningFile))
            FileUtilAxi.delete(expectedImageLearningFile);
        }


      }
    }
    catch (DatastoreException dse)
    {
      handleDatastoreException(dse);
    }
  }

  private void showMessageDialog(String message)
  {
    JOptionPane.showMessageDialog(this, message, "Manage Expected Images", JOptionPane.PLAIN_MESSAGE);
  }

}

class LearnedData
{
  private Pad _pad;
  private SliceNameEnum _sliceNameEnum;
  private String _name;

  public LearnedData(Pad pad, SliceNameEnum sliceNameEnum, String name)
  {
    _pad = pad;
    _sliceNameEnum = sliceNameEnum;
//    System.out.println("name = " + name);
    _name = name;
  }

  Pad getPad()
  {
    return _pad;
  }

  SliceNameEnum getSliceNameEnum()
  {
    return _sliceNameEnum;
  }

  String getName()
  {
    return _name;
  }
}

