package com.axi.v810.internalTools;

import java.awt.geom.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.MagnificationEnum;

/**
 * The interface for Generator objects, used by the ImageSetGenerator.
 *
 * The basic responsibilities of the FeatureGenerator are to populate the JointGeneratorData object for a joint,
 * then draw the joint or defect into the image later.
 *
 * @author Patrick Lacz
 */
public abstract class FeatureGenerator
{
  /**
   * Creates the generator data.
   */
  abstract public JointGeneratorData generateDataForJoint(JointInspectionData jointInspectionData);

  /**
   * Draws the joint in the image.
   */
  abstract public void drawJointFeatureInImage(Image image, AffineTransform jointRegionToImageRegionTransform, JointGeneratorData jointGeneratorData);

  /**
   * Derive the correct affine transform to move the thumbnail into the image
   * @author Patrick Lacz
   */
  protected AffineTransform createAffineTransformForJoint(JointInspectionData jointInspectionData, Image jointThumbnail,
      RegionOfInterest targetRegionOfInterest, float scale)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointThumbnail != null);
    Assert.expect(targetRegionOfInterest != null);

    AffineTransform placementTransform = new AffineTransform();

    double jointThumbnailCenterX = Math.round(jointThumbnail.getWidth() / 2);
    double jointThumbnailCenterY = Math.round(jointThumbnail.getHeight() / 2);

    // Set up the rotation; this is complicated because the most detailed information doesn't contain
    // the pin orientation correction.
    double padRotationInDegrees = jointInspectionData.getPad().getDegreesRotationAfterAllRotations();
    int padOrientation = targetRegionOfInterest.getOrientationInDegrees();
    padOrientation += Math.round(padRotationInDegrees) % 90;
    double padOrientationInDegrees = (double)padOrientation;

    // If the pad orientation is something other than zero, we need to rotate the thumbnail accordingly.
    //double padOrientationInDegrees = targetRegionOfInterest.getOrientationInDegrees();

    final double NANOMETERS_TO_PIXELS = 1.0 / MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();

    double jointPixelsDx = jointInspectionData.getPad().getWidthAfterAllRotationsInNanoMeters() * NANOMETERS_TO_PIXELS;
    double jointPixelsDy = jointInspectionData.getPad().getLengthAfterAllRotationsInNanoMeters() * NANOMETERS_TO_PIXELS;

    // This allows the other generators to change the relative size of the drawn joints.
    jointPixelsDx *= scale;
    jointPixelsDy *= scale;

    double imagePixelsDx = jointThumbnail.getWidth();
    double imagePixelsDy = jointThumbnail.getHeight();

    if (jointInspectionData.getPad().getLandPatternPad().isThroughHolePad())
    {
      // for through-hole pad, use barrel width, not joint pad size.
      ThroughHoleLandPatternPad throughHolePad = (ThroughHoleLandPatternPad)jointInspectionData.getPad().
                                                 getLandPatternPad();
      
      //Siew Yeng - XCR-3318
      if(jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.OVAL_THROUGH_HOLE))
      { 
        if(jointPixelsDx > jointPixelsDy)
        {
          jointPixelsDx = Math.max(throughHolePad.getHoleWidthInNanoMeters(), throughHolePad.getHoleLengthInNanoMeters())* NANOMETERS_TO_PIXELS;
          jointPixelsDy = Math.min(throughHolePad.getHoleWidthInNanoMeters(), throughHolePad.getHoleLengthInNanoMeters()) * NANOMETERS_TO_PIXELS;
        }
        else
        {
          jointPixelsDx = Math.min(throughHolePad.getHoleWidthInNanoMeters(), throughHolePad.getHoleLengthInNanoMeters())* NANOMETERS_TO_PIXELS;
          jointPixelsDy = Math.max(throughHolePad.getHoleWidthInNanoMeters(), throughHolePad.getHoleLengthInNanoMeters()) * NANOMETERS_TO_PIXELS;
        }
      }
      
      targetRegionOfInterest.setWidthKeepingSameCenter((int)jointPixelsDx);
      targetRegionOfInterest.setHeightKeepingSameCenter((int)jointPixelsDy);
      //@todo PL: change to use the hole offset rather than the pad offset
      //@todo PL: based on which slice this is, draw the pad vs the hole.
    }

    // Rotate to the appropriate orientation.
    if (padOrientationInDegrees != 0.0)
    {
      AffineTransform pureRotation = AffineTransform.getRotateInstance(-Math.toRadians(padOrientationInDegrees),
          jointThumbnailCenterX, jointThumbnailCenterY);
      RegionOfInterest rotatedRoi = new RegionOfInterest(0, 0, 1, 1, 0, RegionShapeEnum.RECTANGULAR);
      AffineTransform scrachTransform = new AffineTransform();
      Transform.boundTransformedRegionOfInterest(RegionOfInterest.createRegionFromImage(jointThumbnail),
                                                 pureRotation, rotatedRoi, scrachTransform);
      imagePixelsDx = rotatedRoi.getWidth();
      imagePixelsDy = rotatedRoi.getHeight();

      placementTransform.translate(imagePixelsDx / 2.0, imagePixelsDy / 2.0);
      placementTransform.rotate(-Math.toRadians(padOrientationInDegrees));
      placementTransform.translate( -jointThumbnailCenterX, -jointThumbnailCenterY);
    }

    // Scale the image to the correct size
    placementTransform.preConcatenate(AffineTransform.getScaleInstance(jointPixelsDx / imagePixelsDx,
        jointPixelsDy / imagePixelsDy));

    // Translate to the correct location.
    placementTransform.preConcatenate(AffineTransform.getTranslateInstance(targetRegionOfInterest.getMinX(), targetRegionOfInterest.getMinY()));

    return placementTransform;
  }
}
