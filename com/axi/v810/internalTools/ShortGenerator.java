package com.axi.v810.internalTools;

import java.awt.geom.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * Draws shorts around each of the four sides of each joint.  The shorts are
 * placed such that they fit perfectly within the short search regions.
 *
 * @author Matt Wharton
 */
public class ShortGenerator extends FeatureGenerator
{
  /**
   * @author Matt Wharton
   */
  public ShortGenerator()
  {
    // Do nothing...
  }

  /**
   * Draws the joint in the image.
   *
   * @author Matt Wharton
   */
  public void drawJointFeatureInImage(
      Image regionImage,
      AffineTransform jointRegionToImageRegionTransform,
      JointGeneratorData jointGeneratorData)
  {
    Assert.expect(regionImage != null);
    Assert.expect(jointRegionToImageRegionTransform != null);
    Assert.expect(jointGeneratorData != null);

    // Draw the joint onto the image.
    drawJoint(regionImage, jointRegionToImageRegionTransform, jointGeneratorData);

    // Draw shorts around the joint.
    drawShortsAroundJoint(regionImage, jointRegionToImageRegionTransform, jointGeneratorData);
  }

  /**
   * Draws a 'normal' joint into the region image for the specified joint data.
   *
   * @author Matt Wharton
   */
  private void drawJoint(Image regionImage, AffineTransform jointRegionToImageRegionTransform, JointGeneratorData jointGeneratorData)
  {
    JointInspectionData jointInspectionData = jointGeneratorData.getJointInspectionData();

    // Get our thumbnail.
    Image thumbnailImage = GoodJointGenerator.getThumbnailForFamily(jointInspectionData.getInspectionFamilyEnum());

    AffineTransform placementTransform = new AffineTransform(jointGeneratorData.getRegionToJointImageTransform());

    // transform to the coordinate system of the region we are drawing to
    placementTransform.preConcatenate(jointRegionToImageRegionTransform);

    Transform.copyImageIntoImageWithTransform(regionImage, thumbnailImage, placementTransform);
  }

  /**
   * Draws shorts around the desired joint.
   *
   * @author Matt Wharton
   */
  private void drawShortsAroundJoint(
      Image regionImage,
      AffineTransform jointRegionToImageRegionTransform,
      JointGeneratorData jointGeneratorData)
  {
    Assert.expect(regionImage != null);
    Assert.expect(jointRegionToImageRegionTransform != null);
    Assert.expect(jointGeneratorData != null);

    JointInspectionData jointInspectionData = jointGeneratorData.getJointInspectionData();
    RegionOfInterest jointRoi = jointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels(false);
    int jointWidth = jointRoi.getWidth();
    int jointHeight = jointRoi.getHeight();

    Subtype subtype = jointInspectionData.getSubtype();
    int interPadDistanceInPixels = jointInspectionData.getInterPadDistanceInPixels(false);
    final double innerEdgeLocationAsFractionOfIpd =
      (Double)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION);
    int innerEdgeOffsetInPixels = (int)Math.round(interPadDistanceInPixels * innerEdgeLocationAsFractionOfIpd);
    final double outerEdgeLocationAsFractionOfIpd =
      (Double)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION);
    int outerEdgeOffsetInPixels = (int)Math.round(interPadDistanceInPixels * outerEdgeLocationAsFractionOfIpd);
    final int minimumPixelsShortROIRegion =
      (Integer)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_MINIMUM_PIXELS_SHORT_ROI_REGION);

    int shortRegionWidthInPixels = Math.max(minimumPixelsShortROIRegion,
                                            outerEdgeOffsetInPixels - innerEdgeOffsetInPixels);

    // Create the basic short images.
    // Vertical short.
    int shortWidthInPixels = (int)Math.round(jointWidth * 0.25);
    int shortHeightInPixels = shortRegionWidthInPixels;
    Image verticalShortImage = new Image(shortWidthInPixels, shortHeightInPixels);
    Paint.fillImage(verticalShortImage, 0f);
    // Horizontal short.
    shortWidthInPixels = shortRegionWidthInPixels;
    shortHeightInPixels = (int)Math.round(jointHeight * 0.25);
    Image horizontalShortImage = new Image(shortWidthInPixels, shortHeightInPixels);
    Paint.fillImage(horizontalShortImage, 0f);

    AffineTransform inspectionRegionToJointImageTransform =
      new AffineTransform(jointGeneratorData.getRegionToJointImageTransform());

    // Draw the top short onto the image.
    double topShortMinXOffset = (jointWidth - shortWidthInPixels) / 2d;
    double topShortMinYOffset = -outerEdgeOffsetInPixels;
    AffineTransform shortOffsetFromJointTransform = AffineTransform.getTranslateInstance(
      topShortMinXOffset,
      topShortMinYOffset);
    AffineTransform placementTransform = new AffineTransform(inspectionRegionToJointImageTransform);
    placementTransform.preConcatenate(jointRegionToImageRegionTransform);
//    placementTransform.concatenate(shortOffsetFromJointTransform);
    Transform.copyImageIntoImageWithTransform(regionImage, verticalShortImage, placementTransform);
    verticalShortImage.decrementReferenceCount();

    // Draw the bottom short onto the image.
    double bottomShortMinXOffset = (jointWidth - shortWidthInPixels) / 2d;
    double bottomShortMinYOffset = jointHeight + innerEdgeOffsetInPixels;
    shortOffsetFromJointTransform = AffineTransform.getTranslateInstance(
      bottomShortMinXOffset,
      bottomShortMinYOffset);
    placementTransform = new AffineTransform(inspectionRegionToJointImageTransform);
    placementTransform.preConcatenate(jointRegionToImageRegionTransform);
//    placementTransform.concatenate(shortOffsetFromJointTransform);
    Transform.copyImageIntoImageWithTransform(regionImage, verticalShortImage, placementTransform);
    verticalShortImage.decrementReferenceCount();

    // Draw the left short onto the image.
    double leftShortMinXOffset = -outerEdgeOffsetInPixels;
    double leftShortMinYOffset = -((jointHeight - shortWidthInPixels) / 2d);
    shortOffsetFromJointTransform = AffineTransform.getTranslateInstance(
      leftShortMinXOffset,
      leftShortMinYOffset);
    placementTransform = new AffineTransform(inspectionRegionToJointImageTransform);
    placementTransform.preConcatenate(jointRegionToImageRegionTransform);
//    placementTransform.concatenate(shortOffsetFromJointTransform);
    Transform.copyImageIntoImageWithTransform(regionImage, horizontalShortImage, placementTransform);

    // Draw the right short onto the image.
    double rightShortMinXOffset = jointWidth + innerEdgeOffsetInPixels;
    double rightShortMinYOffset = -((jointHeight - shortWidthInPixels) / 2d);
    shortOffsetFromJointTransform = AffineTransform.getTranslateInstance(
      rightShortMinXOffset,
      rightShortMinYOffset);
    placementTransform = new AffineTransform(inspectionRegionToJointImageTransform);
    placementTransform.preConcatenate(jointRegionToImageRegionTransform);
//    placementTransform.concatenate(shortOffsetFromJointTransform);
    Transform.copyImageIntoImageWithTransform(regionImage, horizontalShortImage, placementTransform);
  }

  /**
   * Creates the generator data.
   *
   * @author Matt Wharton
   */
  public JointGeneratorData generateDataForJoint(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);

    JointGeneratorData generatorData = new JointGeneratorData(jointInspectionData, this);

    // Get the thumbnail image for this joint.
    InspectionFamilyEnum currentJointInspectionFamilyEnum = jointInspectionData.getInspectionFamilyEnum();
    Image thumbnailImage = GoodJointGenerator.getThumbnailForFamily(currentJointInspectionFamilyEnum);

    AffineTransform placementTransform = createAffineTransformForJoint(
      jointInspectionData,
      thumbnailImage,
      jointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels(false),
      1f);
    generatorData.setRegionToJointImageTransform(placementTransform);

    return generatorData;
  }
}
