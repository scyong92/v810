/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.business.autoCal;

import com.axi.guiUtil.*;
import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import org.jfree.chart.*;
import org.jfree.chart.plot.*;
import org.jfree.data.xy.*;
import com.axi.util.math.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.sql.SQLException;
import java.text.*;
import java.util.List;
import javax.imageio.ImageIO;

/**
 *
 * @author ying-huan.chu
 */
public class CurveFitting
{

  static double _min = Double.MAX_VALUE;
  static double _max = Double.MIN_VALUE;
  static double _firstCurveFitMean = 0;

  public static void main(String[] args)
  {
    java.util.List<java.util.List<Double>> listOfMagnificationList = new ArrayList<>();
    java.util.List<java.util.List<Double>> listOfSharpnessList = new ArrayList<>();
    java.util.List<String> timeList = new ArrayList<>();
    java.util.List<String> filePathList = new ArrayList<>();
    java.util.List<Double> maxSharpnessMagInFileList = new ArrayList<>();
    java.util.List<Double> maxSharpnessInFileList = new ArrayList<>();
    java.util.List<double[]> curveFittedSharpnessArrayList = new ArrayList<>();

    String dataPath = "D:\\vstore\\5.4_Dev\\axi\\java\\com\\axi\\v810\\business\\autoCal\\autotest\\data\\CurveFitting\\3006\\Magnification\\";
    File[] files = new File(dataPath).listFiles();

    for (File file : files)
    {
      if (file.isFile())
      {
        if (file.getPath().endsWith(".log"))
        {
          filePathList.add(file.getPath());
        }
      }
    }
    BufferedReader br = null;
    try
    {
      for (String file : filePathList)
      {
        br = new BufferedReader(new FileReader(file));
        String currentLine = "";
        boolean startRead = false;
        int line = 0;
        java.util.List<Double> magnificationList = new ArrayList<>();
        java.util.List<Double> sharpnessList = new ArrayList<>();
        while ((currentLine = br.readLine()) != null)
        {
          if (line == 0)
          {
            timeList.add(currentLine);
          }
          if (currentLine.startsWith(",Magnification = "))
          {
            String[] lineArray = currentLine.split(" ");
            //maximumPointInFileList.add(Float.parseFloat(lineArray[2]));
            maxSharpnessMagInFileList.add(Double.parseDouble(lineArray[2]));
          }
          if (startRead)
          {
            String[] lineArray = currentLine.split(",");
            for (int i = 0; i < lineArray.length; ++i)
            {
              if (lineArray[i].length() != 0)
              {
                if (i == 2)
                {
                  magnificationList.add(Double.parseDouble(lineArray[i]));
                }
                if (i == 3)//if (i == 4)
                {
                  sharpnessList.add(Double.parseDouble(lineArray[i]));
                  // add maximum sharpness
                  DecimalFormat df = new DecimalFormat("#.00000");
                  if (df.format(Double.parseDouble(lineArray[2])).equals(df.format(maxSharpnessMagInFileList.get(maxSharpnessMagInFileList.size() - 1))))
                  {
                    maxSharpnessInFileList.add(Double.parseDouble(lineArray[i]));
                  }
                }
              }
            }
          }
          if (currentLine.startsWith(",,Mag,Sharpness"))
          {
            startRead = true;
          }
          line += 1;
        }
        listOfMagnificationList.add(magnificationList);
        listOfSharpnessList.add(sharpnessList);
      }
    }
    catch (Exception ex)
    {
      System.out.println("IOException caught");
    }

    /*final XYSeries beforeCurveFitSeries = new XYSeries("Before Curve Fit");
     for (int i = 0; i < magnificationList.size(); ++i)
     {
     beforeCurveFitSeries.add(magnificationList.get(i), sharpnessList.get(i));
     }*/
    boolean displayCurveFit = true;
    final java.util.List<XYSeries> afterGaussianSmoothSeriesList = new ArrayList<>();
    final java.util.List<XYSeries> rawDataSeriesList = new ArrayList<>();
    for (int i = 0; i < listOfMagnificationList.size(); ++i)
    {
      final XYSeries beforeCurveFitSeries = new XYSeries("Before Curve Fit");
      final XYSeries afterCurveFitSeries = new XYSeries("After Curve Fit");

      //double[] curveFittedSharpness = GaussianCurve(convertListToArray(listOfMagnificationList.get(i)), convertListToArray(listOfSharpnessList.get(i)), maxSharpnessMagInFileList.get(i));
      double[] curveFittedSharpness = null;
      if (displayCurveFit)
      {
        curveFittedSharpness = GaussianCurve(convertListToArray(listOfMagnificationList.get(i)), convertListToArray(listOfSharpnessList.get(i)), maxSharpnessMagInFileList.get(i));
      }

      //for (int i = 0; i < sharpnessList.size(); ++i)
      //for (int j = 0; j < curveFittedSharpness.length; ++j)
      for (int j = 0; j < listOfSharpnessList.get(i).size(); ++j)
      {
        if (displayCurveFit)
        {
          afterCurveFitSeries.add(listOfMagnificationList.get(i).get(j).doubleValue(), curveFittedSharpness[j]);
        }
        {
          beforeCurveFitSeries.add(listOfMagnificationList.get(i).get(j).doubleValue(), listOfSharpnessList.get(i).get(j).doubleValue());
        }
      }
      //curveFittedSharpnessArrayList.add(curveFittedSharpness);
      afterGaussianSmoothSeriesList.add(afterCurveFitSeries);
      rawDataSeriesList.add(beforeCurveFitSeries);
    }

    final XYSeriesCollection beforeDataset = new XYSeriesCollection();
    final XYSeriesCollection afterDataset = new XYSeriesCollection();
    //dataset.addSeries(beforeCurveFitSeries);
    for (XYSeries curveFitSeries : afterGaussianSmoothSeriesList)
    {
      afterDataset.addSeries(curveFitSeries);
    }

    for (XYSeries rawDataSeries : rawDataSeriesList)
    {
      beforeDataset.addSeries(rawDataSeries);
    }
    final JFreeChart afterChart = ChartFactory.createXYLineChart(
      "Magnification vs Sharpness", // chart title
      "Magnification", // x axis label
      "Sharpness", // y axis label
      afterDataset, // data
      PlotOrientation.VERTICAL,
      false, // include legend
      true, // tooltips
      false // urls
      );

    final JFreeChart beforeChart = ChartFactory.createXYLineChart(
      "Magnification vs Sharpness", // chart title
      "Magnification", // x axis label
      "Sharpness", // y axis label
      beforeDataset, // data
      PlotOrientation.VERTICAL,
      false, // include legend
      true, // tooltips
      false // urls
      );
    final XYPlot plot = afterChart.getXYPlot();
    //ValueMarker marker = new ValueMarker((double) maxSharpnessMagInFile);  // position is the value on the axis
    //marker.setPaint(Color.RED);

    /*for (int i = 0; i < curveFittedSharpnessArrayList.size(); ++i)
     {
     ValueMarker marker = new ValueMarker(getSharpnessPeak(convertListToArray(listOfMagnificationList.get(i)),curveFittedSharpnessArrayList.get(i)));
     marker.setPaint(Color.BLACK);
     plot.addDomainMarker(marker);
     }*/
    //plot.addDomainMarker(marker);

    ChartPanel chartPanel = new ChartPanel(afterChart);
    chartPanel.setPreferredSize(new java.awt.Dimension(800, 800));

    if (!displayCurveFit)
    {
      chartPanel = new ChartPanel(beforeChart);
    }

    /*Collections.sort(maxSharpnessMagInFileList);
     for (int i = 0; i < maxSharpnessInFileList.size(); ++i)
     {
     System.out.println("maxSharpnessInFileList = " + maxSharpnessInFileList.get(i));
     }
     double maximumInMaxSharpness = maxSharpnessInFileList.get(0);
     int maximumSharpnessMagnificationIndex = 0;
     for (int i = 0; i < maxSharpnessInFileList.size(); ++i)
     {
     if (maximumInMaxSharpness < maxSharpnessInFileList.get(i))
     {
     maximumInMaxSharpness = maxSharpnessInFileList.get(i);
     maximumSharpnessMagnificationIndex = i;
     }
     }

     double[] curveFittedMaxSharpness = GaussianCurve(convertListToArray(maxSharpnessMagInFileList), convertListToArray(maxSharpnessInFileList), maxSharpnessMagInFileList.get(maximumSharpnessMagnificationIndex));
     for (int i = 0; i < curveFittedMaxSharpness.length; ++i)
     {
     System.out.println("curveFittedMaxSharpness = " + curveFittedMaxSharpness[i]);
     }
    
     final XYSeries maxSharpnessMagnificationSeries = new XYSeries("Max Sharpness Magnification");
     for (int i = 0; i < maxSharpnessMagInFileList.size(); ++i)
     {
     maxSharpnessMagnificationSeries.add(maxSharpnessMagInFileList.get(i).doubleValue(), curveFittedMaxSharpness[i]);
     }
    
     final XYSeriesCollection afterCurveFitDataset = new XYSeriesCollection();
     afterCurveFitDataset.addSeries(maxSharpnessMagnificationSeries);
    
    
    
     final JFreeChart chart = ChartFactory.createScatterPlot(
     "Magnification vs Sharpness", // chart title
     "Magnification", // x axis label
     "Sharpness", // y axis label
     afterCurveFitDataset, // data
     PlotOrientation.VERTICAL,
     false, // include legend
     true, // tooltips
     false // urls
     );
    
     final XYPlot plot = chart.getXYPlot();
     XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
     renderer.setSeriesLinesVisible(0, true);
     plot.setRenderer(renderer);
    
     final ChartPanel chartPanel = new ChartPanel(chart);
     chartPanel.setPreferredSize(new java.awt.Dimension(800, 800));*/

    /*StringBuilder logString = new StringBuilder();
     // Create header lines
     logString.append("Before Curve Fit (High Mag) - Curve-fitted Magnification vs Maximum Sharpness" + "\n,\n");
     //logString.append(",Magnification = " + _newMagnification + "\n,\n");
     logString.append(",Mag vs Sharpness\n");
     logString.append(",,Mag,Sharpness\n");
     for (int i = 0; i < maxSharpnessMagInFileList.size(); ++i)
     {
     logString.append(",," + maxSharpnessMagInFileList.get(i) + "," + curveFittedMaxSharpness[i] + "\n");
     }
    
     String logDirectory = "D:\\Curve Fit\\11August2013 - Curve Fit with Levenberg-Marquardt Algorithm\\";
     DirectoryLoggerAxi logger = new DirectoryLoggerAxi(logDirectory, FileName.getMagnificationLogFileBasename(),
     FileName.getLogFileExtension(), 10);
     try
     {
     logger.log(logString.toString());
     }
     catch (DatastoreException ex)
     {
     Logger.getLogger(CurveFitting.class.getName()).log(Level.SEVERE, null, ex);
     }
     logString = null;*/
    try
    {
      writeAsPNG(afterChart, 800, 800, dataPath + "Fitted Curve.png");
      writeAsPNG(beforeChart, 800, 800, dataPath + "Raw Data Plot.png");
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }

    EscapeDialog lineGraphDialog = new EscapeDialog();
    //mainPanel.setPreferredSize(new Dimension(1600, 800));
    lineGraphDialog.getContentPane().add(chartPanel, BorderLayout.CENTER);
    lineGraphDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    lineGraphDialog.pack();
    lineGraphDialog.setVisible(true);
    //lineGraphDialog.setAlwaysOnTop(true);
    lineGraphDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
  }

  public static void writeAsPNG(JFreeChart chart, int width, int height, String filename)
  {
    try
    {
      BufferedImage chartImage = chart.createBufferedImage(width, height, null);
      File outputfile = new File(filename);
      ImageIO.write(chartImage, "png", outputfile);
    }
    catch (Exception e)
    {
      e.printStackTrace();
      System.out.println("IOException caught" + e.toString());
    }
  }

 
  public static double[] normalize(double[] y)
  {
    double[] yNormalized = new double[y.length];
    _min = Double.MAX_VALUE;
    _max = Double.MIN_VALUE;
    for (int i = 0; i < y.length; i++)
    {
      if (y[i] < _min)
      {
        _min = y[i];
      }
      if (y[i] > _max)
      {
        _max = y[i];
      }

    }
    for (int i = 0; i < y.length; i++)
    {

      yNormalized[i] = (y[i] - _min) / (_max - _min);
    }

    return yNormalized;
  }

  public static double[] GaussianCurve(double[] x, double[] y, double maxSharpnessMag)
  {
    int arraySize = x.length;
    int halfArraySize = x.length / 2;
    GaussianCurveFunction gaussianCurveFunction = new GaussianCurveFunction();

    double[] a = new double[4];

    a[1] = maxSharpnessMag; // mu
    a[2] = 0.01; // sigma
    //a[0] = 1 / (a[2] * Math.sqrt(2 * Math.PI)); // k
    a[0] = 1;
    a[3] = 0; // background offset

    y = normalize(y);
    LevenbergMarquardtSolver1D lm_solver = new LevenbergMarquardtSolver1D(gaussianCurveFunction);
    //lm_solver.setConvergenceCriteria(1E-10, 0.0, 1E-3, 30);
    lm_solver.setConvergenceCriteria(1E-10, 0.0, 1E-15, 300000);
    lm_solver.solve(x, y, a);
    double[] a_est = lm_solver.get_a();
    //if (arrays_equal(a, a_est, 1E-3))
    //if (arrays_equal(a, a_est, 0.1))
    {
      System.out.println("Initial estimate:");
      print_array(a);

      System.out.println("Final estimate:");
      print_array(a_est);

      System.out.printf("Iterations: %d\n", lm_solver.get_nIter());
      System.out.printf("Chi squared: %f\n", lm_solver.get_chiSq());
    }

    double[] y_result = new double[x.length];
    for (int i = 0; i < x.length; i++)
    {
      y_result[i] = _min + (_max - _min) * gaussianCurveFunction.evalFunction(a_est, x[i]);
      //y_result[i] = gaussianCurveFunction.evalFunction(a, x[i]);
    }
    _firstCurveFitMean = a_est[1];
    return y_result;
  }

  public static double getSharpnessPeak(double[] x, double[] y)
  {
    //*******************************
    // Do curve fit for the 2nd time
    //*******************************
    GaussianCurveFunction gaussianCurveFunction = new GaussianCurveFunction();

    double maxY = y[0];
    int maxSharpnessIndex = 0;
    double maxSharpnessMag = _firstCurveFitMean;
    for (int i = 0; i < y.length; i++)
    {
      if (maxY < y[i])
      {
        maxY = y[i];
        maxSharpnessIndex = i;
        //maxSharpnessMag = x[i];
      }
    }
    //System.out.println("max y_result = " + maxY);

    int r1 = 0, r2 = 0;
    for (int i = 0; i < x.length; ++i)
    {
      if (x[i] <= maxSharpnessMag)
      {
        r1 += 1;
      }
      else
      {
        r2 += 1;
      }
    }

    //System.out.println("r1 = " + r1);
    //System.out.println("r2 = " + r2);
    //System.out.println("r2/r1 = " + r2 / r1);

    double[] a = new double[4];

    a[1] = maxSharpnessMag; // mu
    a[2] = 0.01; // sigma
    a[0] = 1 / (a[2] * Math.sqrt(2 * Math.PI)); // k
    a[3] = 0;

    int arraySize = 0;

    if (r1 <= r2)
    {
      arraySize = r1;
    }
    else
    {
      arraySize = r2;
    }
    if (arraySize < 30)
    {
      arraySize = 30;
    }
    double[] x2 = new double[arraySize];
    double[] y2 = new double[arraySize];

    /*for (int i = 0; i < x.length; ++i)
     {
     if (i <= r1-15)
     {
     x2[i] = 0;
     y2[i] = 0;
     }
     else if (i > r1-15 && i < r1+15)
     {
     x2[i] = x[i];
     y2[i] = y_result[i];
     }
     else
     {
     x2[i] = 0;
     y2[i] = 0;
     }
     }*/

    /*for (int i = 0; i < arraySize; ++i)
     {
     x2[i] = x[i];
     y2[i] = y[i];
     }*/

    int count = 0;
    double rangeScale = 3;
    int fineStart = maxSharpnessIndex - (int) (arraySize / rangeScale);
    int fineEnd = maxSharpnessIndex + (int) (arraySize / rangeScale);
    for (int i = fineStart; i < fineEnd; i++)
    {
      x2[count] = x[i];
      y2[count] = y[i];
      count++;
    }

    y2 = normalize(y2);
    LevenbergMarquardtSolver1D lm_solver = new LevenbergMarquardtSolver1D(gaussianCurveFunction);
    lm_solver.setConvergenceCriteria(1E-10, 0.0, 1E-3, 300000);
    lm_solver.solve(x2, y2, a);
    double[] a_est = lm_solver.get_a();
    double[] y_result = new double[y.length];
    for (int i = 0; i < arraySize; i++)
    {
      y_result[i] = _min + (_max - _min) * gaussianCurveFunction.evalFunction(a_est, x2[i]);
      //y_result[i] = gaussianCurveFunction.evalFunction(a, x[i]);
      //System.out.println("y_result2["+i+"] = " + y_result[i]);
    }

    double sharpnessPeak = y_result[0];
    double sharpnessPeakMag = x2[0];
    for (int i = 0; i < y_result.length; i++)
    {
      if (sharpnessPeak < y_result[i])
      {
        sharpnessPeak = y_result[i];
        sharpnessPeakMag = x2[i];
      }
    }

    //System.out.println("sharpnessPeakMag2 = " + sharpnessPeakMag);
    System.out.println("a_est[1]2 = " + a_est[1]);
    //System.out.println("max y_result2 = " + sharpnessPeak);

    //return sharpnessPeakMag;
    return a_est[1];
  }

  public static void print_array(double[] a)
  {
    for (double v : a)
    {
      System.out.printf("%f, ", v);
    }
    System.out.println("");
  }

  public static boolean arrays_equal(double[] a, double[] b, double tol)
  {
    assert a.length == b.length;
    for (int i = 0; i < a.length; i++)
    {
      double absDiff = Math.abs(b[i] - a[i]);
      if (absDiff > tol)
      {
        return false;
      }
    }
    return true;
  }

  public static double[] convertListToArray(java.util.List<Double> list)
  {
    double[] array = new double[list.size()];
    for (int i = 0; i < list.size(); ++i)
    {
      array[i] = list.get(i);
    }
    return array;
  }
}
