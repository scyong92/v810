package com.axi.v810.internalTools;

import java.awt.*;
import java.awt.image.*;
import javax.swing.*;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class ManageExpectedImagesRenderer extends JPanel {
  protected static boolean _debug = false;
  private boolean _debug1 = false;
  private String _me = "ImageRenderer()";

  private BufferedImage _image;
  private JLabel _statusBar = null;
  private String _statusName = null;

  public ManageExpectedImagesRenderer(JLabel statusBar, String statusName) {
    _me = statusName + _me;
    debug(_me);
    _statusBar = statusBar;
    _statusName = statusName;
  }

  public void updateImage(BufferedImage image)
  {
    _image = image;
  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    if (_image == null)
      return;
    debug(_me + ".paintComponent()");
    Dimension d = getSize();
    int clientWidth = d.width;
    int clientHeight = d.height;
    debug("  panel: width=" + d.width + " height=" + d.height);

    int imageWidth = _image.getWidth(this);
    int imageHeight = _image.getHeight(this);
    debug("  image: width = " + imageWidth + " height= " + imageHeight);

    if ((imageWidth < d.width) && (imageHeight < d.height)) {
      g.drawImage(_image, 0, 0, imageWidth, imageHeight, this);
    }
    else {  // set width and height as needed to show whole image
      // scale width to d.width, check heigth
      double widthRatio = (double) imageWidth / (double) d.width;
      int width = (int) (imageWidth / widthRatio);
      int height = (int) (imageHeight / widthRatio);
      debug("  widthratio =" + widthRatio);
      debug("  rendering width =" + width + " height =" + height);
      if (height > d.height) {
        // scale heigth to d.height
        double heightRatio = (double) imageHeight / (double) d.height;
        width = (int) (imageWidth / heightRatio);
        height = (int) (imageHeight / heightRatio);
        debug("  heightRatio =" + widthRatio);
        debug("  rendering width =" + width + " height =" + height);
      }

      g.drawImage(_image, 0, 0, width, height, this);
      if (_statusBar != null)
        _statusBar.setText(_statusName + " Image: size is " + width +
                          " pixels wide by " + height + " pixels high");
    }
  }

  private void debug(String message)  
  { 
    if (_debug)  
      System.out.println(message); 
  }
  private void debug1(String message) 
  { 
    if (_debug1) 
      System.out.println(message); 
  }

  /**
   * @author Khaw Chek Hau
   * XCR2183: Standardize all print out when developer debug is true
   */  
  public static void setDebug(boolean state)
  {
    _debug = state;
  }

}
