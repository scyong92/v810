package com.axi.v810.internalTools;

import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAnalysis.InspectionFamilyEnum;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.DatastoreException;
import com.axi.v810.datastore.Directory;
import com.axi.v810.util.XrayImageIoUtil;

/**
 * @author Patrick Lacz
 */
public class GoodJointGenerator extends FeatureGenerator
{
  private static boolean _isInitialized = false;
  private static Map<InspectionFamilyEnum, Image> _inspectionFamilyEnumToJointThumbnailMap = new HashMap<InspectionFamilyEnum, Image>();

  /**
   * @author Patrick Lacz
   */

  // NOTE: Commented out by George David, see initialize()
//  static
//  {
//  try
//  {
//    populateInspectionFamilyEnumToJointThumbnailMap();
//  }
//  catch (DatastoreException ex)
//  {
//    Assert.expect(false);
//  }
//  }

  /**
   * @author Patrick Lacz
   */
  public GoodJointGenerator()
  {
    // Do nothing ...
  }

  /**
   * created this function so we don't create these images when we do not need
   * them. anyone who uses this class must call this function first.
   * @author George A. David
   */
  public static void initializeIfNecessary()
  {
    if(_isInitialized == false)
    {
      try
      {
        populateInspectionFamilyEnumToJointThumbnailMap();
        _isInitialized = true;
      }
      catch (DatastoreException ex)
      {
        Assert.expect(false);
      }
    }
  }

  /**
   * Draws the joint in the image.
   *
   * @author Patrick Lacz
   */
  public void drawJointFeatureInImage(Image regionImage,
                                      AffineTransform jointRegionToImageRegionTransform,
                                      JointGeneratorData jointGeneratorData)
  {
    Assert.expect(regionImage != null);
    Assert.expect(jointRegionToImageRegionTransform != null);
    Assert.expect(jointGeneratorData != null);

    JointInspectionData jointInspectionData = jointGeneratorData.getJointInspectionData();

    // Get our thumbnail.
    Image thumbnailImage = getThumbnailForFamily(jointInspectionData.getInspectionFamilyEnum());

    AffineTransform placementTransform = new AffineTransform(jointGeneratorData.getRegionToJointImageTransform());

    // transform to the coordinate system of the region we are drawing to
    placementTransform.preConcatenate(jointRegionToImageRegionTransform);

    Transform.copyImageIntoImageWithTransform(regionImage, thumbnailImage, placementTransform);
  }

  /**
   * Creates the generator data.
   *
   * @author Patrick Lacz
   */
  public JointGeneratorData generateDataForJoint(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);

    JointGeneratorData generatorData = new JointGeneratorData(jointInspectionData, this);

    // Get the thumbnail image for this joint
    InspectionFamilyEnum currentJointInspectionFamilyEnum = jointInspectionData.getInspectionFamilyEnum();
    Image thumbnailImage = getThumbnailForFamily(currentJointInspectionFamilyEnum);

    // The JointInspectionData's ROI will give us an orientation that's 180 degrees off from what we'd expect.
    // Make sure we correct for that so that the algorithms run in the correct direction on these images.
    RegionOfInterest jointRoi = new RegionOfInterest(jointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels(false));
    jointRoi.setOrientationInDegrees(jointRoi.getOrientationInDegrees() + 180);
    AffineTransform placementTransform = createAffineTransformForJoint(jointInspectionData,
                                                                       thumbnailImage,
                                                                       jointRoi,
                                                                       1.0f);
    generatorData.setRegionToJointImageTransform(placementTransform);

    return generatorData;
  }

  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  private static void populateInspectionFamilyEnumToJointThumbnailMap() throws DatastoreException
  {
    Assert.expect(_inspectionFamilyEnumToJointThumbnailMap != null);

    String jointThumbnailDir = Directory.getJointThumbnailsDir();
    _inspectionFamilyEnumToJointThumbnailMap.clear();
    BufferedImage chipThumbnail = XrayImageIoUtil.loadPngBufferedImage(jointThumbnailDir + File.separator + "chip.png");
    _inspectionFamilyEnumToJointThumbnailMap.put(InspectionFamilyEnum.CHIP, Image.createFloatImageFromBufferedImage(chipThumbnail));
    _inspectionFamilyEnumToJointThumbnailMap.put(InspectionFamilyEnum.POLARIZED_CAP, Image.createFloatImageFromBufferedImage(chipThumbnail));
    _inspectionFamilyEnumToJointThumbnailMap.put(InspectionFamilyEnum.LARGE_PAD, Image.createFloatImageFromBufferedImage(chipThumbnail));
    BufferedImage gridBallThumbnail = XrayImageIoUtil.loadPngBufferedImage(jointThumbnailDir + File.separator + "gridball.png");
    _inspectionFamilyEnumToJointThumbnailMap.put(InspectionFamilyEnum.GRID_ARRAY,  Image.createFloatImageFromBufferedImage(gridBallThumbnail));
    _inspectionFamilyEnumToJointThumbnailMap.put(InspectionFamilyEnum.LAND_GRID_ARRAY,  Image.createFloatImageFromBufferedImage(gridBallThumbnail));
    BufferedImage gullwingThumbnail = XrayImageIoUtil.loadPngBufferedImage(jointThumbnailDir + File.separator + "gullwing.png");
    _inspectionFamilyEnumToJointThumbnailMap.put(InspectionFamilyEnum.GULLWING,  Image.createFloatImageFromBufferedImage(gullwingThumbnail));
    // @todo mdw - get a better thumbnail for connectors
    _inspectionFamilyEnumToJointThumbnailMap.put(InspectionFamilyEnum.ADVANCED_GULLWING, Image.createFloatImageFromBufferedImage(gullwingThumbnail));
    BufferedImage throughHoleThumbnail = XrayImageIoUtil.loadPngBufferedImage(jointThumbnailDir + File.separator + "throughhole.png");
    _inspectionFamilyEnumToJointThumbnailMap.put(InspectionFamilyEnum.THROUGHHOLE,  Image.createFloatImageFromBufferedImage(throughHoleThumbnail));
    _inspectionFamilyEnumToJointThumbnailMap.put(InspectionFamilyEnum.PRESSFIT,  Image.createFloatImageFromBufferedImage(throughHoleThumbnail));
    _inspectionFamilyEnumToJointThumbnailMap.put(InspectionFamilyEnum.OVAL_THROUGHHOLE,  Image.createFloatImageFromBufferedImage(throughHoleThumbnail)); //Siew Yeng - XCR-3318
    BufferedImage qfnThumbnail = XrayImageIoUtil.loadPngBufferedImage(jointThumbnailDir + File.separator + "qfn.png");
    _inspectionFamilyEnumToJointThumbnailMap.put(InspectionFamilyEnum.QUAD_FLAT_NO_LEAD,  Image.createFloatImageFromBufferedImage(qfnThumbnail));
    // @todo glb - get a better thumbnail for expsoed pad
    BufferedImage exposedPadThumbnail = XrayImageIoUtil.loadPngBufferedImage(jointThumbnailDir + File.separator + "exposedpad.png");
    _inspectionFamilyEnumToJointThumbnailMap.put(InspectionFamilyEnum.EXPOSED_PAD,  Image.createFloatImageFromBufferedImage(exposedPadThumbnail));
  }

  /**
   * @author Patrick Lacz
   */
  public static Image getThumbnailForFamily(InspectionFamilyEnum family)
  {
    Assert.expect(family != null);
    Assert.expect(_inspectionFamilyEnumToJointThumbnailMap != null);
    Image jointThumbnail = _inspectionFamilyEnumToJointThumbnailMap.get(family);
    Assert.expect(jointThumbnail != null);
    return jointThumbnail;
  }
}
