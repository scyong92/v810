package com.axi.v810.internalTools;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAnalysis.InspectionFamilyEnum;
import com.axi.v810.business.testProgram.*;

/**
 * The class responsible for drawing Open  BGA defects. (Missing will be handled elsewhere)
 *
 * BGA Open Outliers have the following features:
 *  X Larger diameter
 *  - circularity (only important for elliptical pads)
 *  - may be missing solder from the pad slice
 *
 * The features above, the ones with X's have been implemented using this class,
 * the ones with -'s are not yet generated.
 *
 * @author Patrick Lacz
 */
public class BGAOpenGenerator extends FeatureGenerator
{
  private float _maximumGrow;
  private float _minimumGrow;
  private Random _random;

  /**
   * @author Patrick Lacz
   */
  public BGAOpenGenerator(Random randomObject, float minimumGrow, float maximumGrow)
  {
    Assert.expect(minimumGrow < maximumGrow);
    Assert.expect(minimumGrow > 1.0f);
    Assert.expect(randomObject != null);

    _random = randomObject;
    _maximumGrow = maximumGrow;
    _minimumGrow = minimumGrow;
  }

  /**
   * Draws the joint in the image.
   *
   * @author Patrick Lacz
   */
  public void drawJointFeatureInImage(Image regionImage, AffineTransform jointRegionToImageRegionTransform,
                                      JointGeneratorData jointGeneratorData)
  {
    Assert.expect(regionImage != null);
    Assert.expect(jointRegionToImageRegionTransform != null);
    Assert.expect(jointGeneratorData != null);

    JointInspectionData jointInspectionData = jointGeneratorData.getJointInspectionData();

    // Get our thumbnail.
    Image thumbnailImage = GoodJointGenerator.getThumbnailForFamily(jointInspectionData.getInspectionFamilyEnum());

    AffineTransform placementTransform = new AffineTransform(jointGeneratorData.getRegionToJointImageTransform());

    // transform to the coordinate system of the region we are drawing to
    placementTransform.preConcatenate(jointRegionToImageRegionTransform);

    Transform.copyImageIntoImageWithTransform(regionImage, thumbnailImage, placementTransform);
  }

  /**
   * Creates the generator data.
   *
   * @author Patrick Lacz
   */
  public JointGeneratorData generateDataForJoint(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(_random != null);

    float grow = _random.nextFloat() * (_maximumGrow - _minimumGrow) + _minimumGrow;

    JointGeneratorData generatorData = new JointGeneratorData(jointInspectionData, this);

    // Get the thumbnail image for this joint
    InspectionFamilyEnum currentJointInspectionFamilyEnum = jointInspectionData.getInspectionFamilyEnum();
    Image thumbnailImage = GoodJointGenerator.getThumbnailForFamily(currentJointInspectionFamilyEnum);

    RegionOfInterest targetRegionOfInterest = jointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels(false);

    AffineTransform placementTransform = createAffineTransformForJoint(jointInspectionData, thumbnailImage, targetRegionOfInterest, grow);
    generatorData.setRegionToJointImageTransform(placementTransform);

    System.out.println("Generating BGA Open at joint : " + jointInspectionData.getFullyQualifiedPadName());

    return generatorData;
  }
}
