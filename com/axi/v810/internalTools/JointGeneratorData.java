package com.axi.v810.internalTools;

import java.awt.geom.AffineTransform;

import com.axi.util.Assert;
import com.axi.v810.business.testProgram.JointInspectionData;

/**
 * Contains information used by the individual Generator objects.
 * @author Patrick Lacz
 */
public class JointGeneratorData
{
  // The transformation from the origin of the reconstruction region to the joint. Will include rotations, scales, and translations.
  private AffineTransform _ownerRegionToJointTransform;

  // The generator used to draw this joint.
  private FeatureGenerator _generator;

  // A reference to the actual joint.
  private JointInspectionData _jointInspectionData;

  /**
   * @author Patrick Lacz
   */
  public JointGeneratorData(JointInspectionData jointData, FeatureGenerator generator)
  {
    Assert.expect(jointData != null);
    Assert.expect(generator != null);

    _generator = generator;
    _jointInspectionData = jointData;
  }

  /**
   * @author Patrick Lacz
   */
  public JointInspectionData getJointInspectionData()
  {
    Assert.expect(_jointInspectionData != null);
    return _jointInspectionData;
  }

  /**
   * @author Patrick Lacz
   */
  public FeatureGenerator getFeatureGenerator()
  {
    Assert.expect(_generator != null);
    return _generator;
  }

  /**
   * @author Patrick Lacz
   */
  public AffineTransform getRegionToJointImageTransform()
  {
    Assert.expect(_ownerRegionToJointTransform != null);
    return _ownerRegionToJointTransform;
  }

  /**
   * @author Patrick Lacz
   */
  public void setRegionToJointImageTransform(AffineTransform transform)
  {
    Assert.expect(transform != null);
    _ownerRegionToJointTransform = transform;
  }

}
