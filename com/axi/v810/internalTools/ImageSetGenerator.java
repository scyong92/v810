package com.axi.v810.internalTools;

import java.awt.geom.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Used to generate pseudo image runs based on the features in the inspection regions.
 * Thumbnails of sample joints are scaled to fill the various joint regions in the inspection regions.
 * This is useful when it's necessary to generate some viable images for algorithm debugging, etc. when
 * there's no hardware available.
 *
 * @author Matt Wharton
 */
public class ImageSetGenerator
{
  private static String _projectName = null;
  private static String _importName = null;
  private static String _imageSetName = null;
  private static boolean _verboseMode = false;

  private static boolean _specialBlackMode = false;
  private static boolean _specialWhiteMode = false;
  private static boolean _specialRandomMode = false;
  private static boolean _specialShortMode = false;

  private static boolean _panelFilterSpecified = false;
  private static boolean _boardTypeFilterSpecified = false;
  private static boolean _boardInstanceFilterSpecified = false;
  private static boolean _jointTypeFilterSpecified = false;
  private static boolean _subtypeFilterSpecified = false;
  private static boolean _componentFilterSpecified = false;
  private static boolean _pinFilterSpecified = false;

  private static String _boardTypeName;
  private static String _boardInstanceName;
  private static String _jointTypeName;
  private static String _subtypeName;
  private static String _componentName;
  private static String _pinName;

  private static ImageSetGenerator _instance = null;
  private static ImageManager _imageManager = null;
  private static boolean _abort = false;
  private static BooleanLock _imageGenerationInProgress = new BooleanLock();

  private SurroundingPadUtil _surroundingPadUtilForVerificationImages = null;

  // Controls the randomness of the generator
  private static long _randomSeed = 0;
  private static Random _randomObject;

  Map<JointInspectionData, JointGeneratorData>
      _jointInspectionDataToGeneratorDataMap = new HashMap<JointInspectionData, JointGeneratorData>();

  // defined by the command line
  private static String _description = null;

  // Patrick Lacz: Temporary, will be replaced by some more dynamic structures
  private FeatureGenerator _goodJointGenerator;
  private FeatureGenerator _BGAOpenGenerator;
  private FeatureGenerator _shortGenerator;

  // defined by the command line
  private static float _backgroundColor = 170.0f;

  // Defined by the commandline
  private static float _BGAOpenChance = 0.0f;
  private static float _BGAOpenMin = 1.3f;
  private static float _BGAOpenMax = 1.5f;
  private int _sliceHeightInNanometers; 
  /**
   * @author Matt Wharton
   */
  private ImageSetGenerator()
  {
    _imageManager = ImageManager.getInstance();
    _randomObject = new Random();
    _BGAOpenGenerator = new BGAOpenGenerator(_randomObject, _BGAOpenMin, _BGAOpenMax);
    _goodJointGenerator = new GoodJointGenerator();
    _shortGenerator = new ShortGenerator();
  }

  /**
   * @author Matt Wharton
   */
  public static synchronized ImageSetGenerator getInstance()
  {
    if (_instance == null)
    {
      _instance = new ImageSetGenerator();
    }

    return _instance;
  }

  /**
   * @author Peter Esbensen
   */
  static public void setCreateBlackImages(boolean createBlackImages)
  {
    _specialBlackMode = createBlackImages;
  }

  /**
   * @author Peter Esbensen
   */
  static public void setCreateWhiteImages(boolean createWhiteImages)
  {
    _specialWhiteMode = createWhiteImages;
  }

  /**
   * @author Peter Esbensen
   */
  static public void setCreateRandomImages(boolean createRandomImages)
  {
    _specialRandomMode = createRandomImages;
  }

  /**
   * @author Matt Wharton
   */
  public static void setCreateShortDefects(boolean createShortDefects)
  {
    _specialShortMode = createShortDefects;
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   */
  public ReconstructedImages generateAlignmentImages(ReconstructionRegion alignmentRegion)
  {
    Assert.expect(alignmentRegion != null);
    Assert.expect(alignmentRegion.isAlignmentRegion());

    PanelRectangle regionRect = alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters();

    double nanoMetersToPixelZoom = 1.0 / MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
      
    if(alignmentRegion.getTestSubProgram().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
        nanoMetersToPixelZoom = 1.0 / (double)MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
      
    AffineTransform nanoMetersToPixelTransform = AffineTransform.getScaleInstance(nanoMetersToPixelZoom, -nanoMetersToPixelZoom);
    Rectangle2D pixelRect = nanoMetersToPixelTransform.createTransformedShape(regionRect.getRectangle2D()).getBounds2D();

    java.awt.image.BufferedImage bufferedImage = new java.awt.image.BufferedImage((int)Math.ceil(pixelRect.getWidth()),
        (int)Math.ceil(pixelRect.getHeight()),
        java.awt.image.BufferedImage.TYPE_BYTE_GRAY);
    AffineTransform transform = new AffineTransform(nanoMetersToPixelTransform);
    transform.translate( -regionRect.getMinX(), -regionRect.getMinY() - regionRect.getHeight());

    java.awt.Graphics2D graphics2d = (java.awt.Graphics2D)bufferedImage.getGraphics();
    graphics2d.setTransform(transform);
    graphics2d.setColor(java.awt.Color.lightGray);
    graphics2d.fill(regionRect.getRectangle2D());
    graphics2d.setTransform(transform);
    graphics2d.setColor(java.awt.Color.darkGray);

    SurroundingPadUtil padUtil = new SurroundingPadUtil();
    padUtil.setUseThroughHolePadsOnBothSides(true);
    padUtil.setUseBoundsForDeterminingVicinity(true);
    padUtil.setPads(alignmentRegion.getTestSubProgram().getTestProgram().getProject().getPanel().getPadsUnsorted());

    // Fill in the relevant alignment pads.
    for (Pad pad : padUtil.getIntersectingPadsInRectangle(regionRect, alignmentRegion.isTopSide()))
    {
      graphics2d.fill(pad.getShapeRelativeToPanelInNanoMeters());
    }

    // Fill in the relevant alignment fiducials.
    for (Fiducial fiducial : alignmentRegion.getFiducials())
    {
      graphics2d.fill(fiducial.getShapeRelativeToPanelInNanoMeters());
    }

    ReconstructedImages reconImages = new ReconstructedImages(alignmentRegion);
    Image image = Image.createFloatImageFromBufferedImage(bufferedImage);
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810   
    if (alignmentRegion.getTestSubProgram().getTestProgram().getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod())
      reconImages.addImage(image, SliceNameEnum.CAMERA_0);
    else
      reconImages.addImage(image, SliceNameEnum.PAD);
      
    image.decrementReferenceCount();

    return reconImages;
  }

  /**
   * @author Matt Wharton
   */
  public ReconstructedImages generateInspectionImages(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);

    // Get the region dimensions.
    ImageRectangle regionRectangleInPixels = reconstructionRegion.getRegionRectangleInPixels();
    int regionWidth = regionRectangleInPixels.getWidth();
    int regionHeight = regionRectangleInPixels.getHeight();

    boolean isGeneratingProductionImageSetWithoutBoard = false; 
    
    // Get the list of context regions.
    TestProgram testProgram = reconstructionRegion.getTestSubProgram().getTestProgram();
    Collection<ReconstructionRegion> allRegions = testProgram.getAllInspectionRegions();

    // These will be an image filled with "good" joints.
    Image regionImage = createPopulatedRegionImage(reconstructionRegion,
                                                   regionWidth,
                                                   regionHeight,
                                                   allRegions,
                                                   true);
    ReconstructedImages inspectionImages = new ReconstructedImages(reconstructionRegion);
    for (Slice slice : reconstructionRegion.getSlices())
    {
      if (slice.createSlice() && 
          (slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION) || slice.getSliceType().equals(SliceTypeEnum.PROJECTION))) //Chnee Khang Wah, 2012-02-21, 2.5D
      {
        Subtype subtype = null;
        try
        {
          subtype = reconstructionRegion.getSubtypes().iterator().next();

          if (ImageProcessingAlgorithm.useUserDefinedGrayLevel(subtype))
          {
            Image sliceImageCopy = ImageProcessingAlgorithm.applyUserDefinedGrayLevel(subtype, regionImage);
            //        Image sliceImageCopy = Image.createCopy(regionImage, RegionOfInterest.createRegionFromImage(regionImage));
            // Added by Seng Yew on 22-Apr-2011
            //        Filter.normalizeWithMinMaxValue(sliceImageCopy, reconstructionRegion.getMinimumGraylevel(), reconstructionRegion.getMaximumGraylevel());
            inspectionImages.addImage(sliceImageCopy, slice.getSliceName());
            sliceImageCopy.decrementReferenceCount();
          }
          else
          {
            inspectionImages.addImage(regionImage, slice.getSliceName());
          }
          reconstructionRegion.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.INSPECTION);
        }
        catch(NoSuchElementException ex)
        {
          isGeneratingProductionImageSetWithoutBoard = true;
          reconstructionRegion.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.VERIFICATION);
        }
      }
    }

    regionImage.decrementReferenceCount();

    if(isGeneratingProductionImageSetWithoutBoard)
      throw new NoSuchElementException();
    
    return inspectionImages;
  }

  /**
   * Generates a fake image set for the specified Project and ImageSetData.
   *
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  public void generatePseudoImageSet(Project project, ImageSetData imageSetData, boolean verboseModeEnabled) throws
      DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(imageSetData != null);

    try
    {
      _abort = false;
      _imageGenerationInProgress.setValue(true);

      // Get the TestProgram from the project.
      TestProgram program = project.getTestProgram();
      Assert.expect(program != null);

      // Write the image set info file.
      writeImageSetInfoFile(imageSetData);
      if (_abort)
        return;
      _imageManager.saveInspectionImageInfo(imageSetData, program);
      if (_abort)
        return;
      // Set up the random number generator. Report the seed value.
      if (_randomSeed == 0)
        _randomSeed = System.nanoTime() % 100000; // make the seed vary, but in a way that makes the numbers more memorable
      if (_verboseMode)
        System.out.println("Seed : " + _randomSeed);
      _randomObject.setSeed(_randomSeed);

      // Conversion transform from nanos to pixels.
      final double PIXELS_PER_NANOMETER = 1.0 / MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
      AffineTransform panelToPixelCoordinateTransform = AffineTransform.getScaleInstance(
          PIXELS_PER_NANOMETER,
          PIXELS_PER_NANOMETER);

      // Create the data for each joint
      Collection<ReconstructionRegion> inspectionRegions = program.getFilteredInspectionRegions();
      for (ReconstructionRegion inspectionRegion : inspectionRegions)
      {
        // reseed based on the reconstruction region
        _randomObject.setSeed(_randomSeed + inspectionRegion.getRegionId());
        for (JointInspectionData jointInspectionData : inspectionRegion.getJointInspectionDataList())
        {
          FeatureGenerator generatorForJoint = chooseGeneratorForJoint(jointInspectionData);
          _jointInspectionDataToGeneratorDataMap.put(jointInspectionData,
                                                     generatorForJoint.generateDataForJoint(jointInspectionData));
          if (_abort)
            return;
        }
        if (_abort)
          return;
      }

      String imagesDir = imageSetData.getDir();
      Collection<Integer> candidateFocusConfirmationOffsets = FocusConfirmationEngine.getCandidateFocusConfirmationOffsets();

      for (ReconstructionRegion generatingInspectionRegion : inspectionRegions)
      {

        PanelRectangle panelRegionRect = generatingInspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
        java.awt.Shape scaledRegion = panelToPixelCoordinateTransform.createTransformedShape(panelRegionRect.
            getRectangle2D());

        int regionWidth = (int)Math.round(scaledRegion.getBounds2D().getWidth());
        int regionHeight = (int)Math.round(scaledRegion.getBounds2D().getHeight());

        Image regionImage = null;
        if (_specialBlackMode || _specialWhiteMode || _specialRandomMode)
        {
          regionImage = createImageWithSpecialContents(regionWidth, regionHeight);
        }
        else
        {
          regionImage = createPopulatedRegionImage(generatingInspectionRegion,
                                                   regionWidth,
                                                   regionHeight,
                                                   inspectionRegions,
                                                   false);
        }
        // Create an image for each slice of the inspection region.
        final int numberOfSlices = generatingInspectionRegion.getNumberOfSlices();
        for (FocusGroup focusGroup : generatingInspectionRegion.getFocusGroups())
        {
          for (Slice slice : focusGroup.getSlices())
          {
            // ignore the surface modeling slices, we should not create images for this
//            if (slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION) && slice.createSlice())
            if (slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION))
            {
              if (verboseModeEnabled)
              {
                StringBuilder imageGenerationDiagMessage = new StringBuilder();
                imageGenerationDiagMessage.append("Generating image for");
                imageGenerationDiagMessage.append(" Region:");
                imageGenerationDiagMessage.append(generatingInspectionRegion.getRegionId());
                imageGenerationDiagMessage.append(" Slice:");
                imageGenerationDiagMessage.append(slice.getSliceName().getName());

                System.out.println(imageGenerationDiagMessage.toString());
              }

              ReconstructedSlice reconstructedSlice = new ReconstructedSlice(regionImage, slice.getSliceName());

              // Save the inspection image for the slice.
              _imageManager.saveInspectionImage(reconstructedSlice, generatingInspectionRegion, imagesDir);

              // Save a "lightest" image if applicable.
              if (imageSetData.collectLightestImages())
              {
                _imageManager.saveLightestInspectionImage(reconstructedSlice, generatingInspectionRegion, imagesDir);
              }

              // Save focus confirmation images if applicable.
              if (imageSetData.collectFocusConfirmationImages())
              {
                JointTypeEnum jointTypeEnum = generatingInspectionRegion.getJointTypeEnum();
                if (jointTypeEnum.isAvailableForRetestFailingPins())
                {
                  for (int candidateFocusConfirmationOffset : candidateFocusConfirmationOffsets)
                  {
                    _imageManager.saveFocusConfirmationInspectionImage(reconstructedSlice,
                                                                       generatingInspectionRegion,
                                                                       candidateFocusConfirmationOffset,
                                                                       imagesDir);
                  }
                }
              }

              reconstructedSlice.decrementReferenceCount();

              if (_abort)
                return;
            }

            if (_abort)
              return;
          }
          if (_abort)
            return;
        }
        regionImage.decrementReferenceCount();
      }
    }
    finally
    {
      _imageGenerationInProgress.setValue(false);
      _jointInspectionDataToGeneratorDataMap.clear();
    }
  }

  /**
   * Generates a fake image set for the specified Project and ImageSetData.
   *
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  public void generateAdjustFocusImages(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    _specialRandomMode = true;

    try
    {
      _abort = false;
      _imageGenerationInProgress.setValue(true);

      // Get the TestProgram from the project.
      TestProgram program = project.getTestProgram();

      if (_abort)
        return;

      // Set up the random number generator. Report the seed value.
      if (_randomSeed == 0)
        _randomSeed = System.nanoTime() % 100000; // make the seed vary, but in a way that makes the numbers more memorable
      if (_verboseMode)
        System.out.println("Seed : " + _randomSeed);
      _randomObject.setSeed(_randomSeed);

      // Conversion transform from nanos to pixels.
      final double PIXELS_PER_NANOMETER = 1.0 / MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
      AffineTransform panelToPixelCoordinateTransform = AffineTransform.getScaleInstance(
          PIXELS_PER_NANOMETER,
          PIXELS_PER_NANOMETER);

      // Create the data for each joint
      Collection<ReconstructionRegion> inspectionRegions = program.getFilteredInspectionRegions();
      for (ReconstructionRegion inspectionRegion : inspectionRegions)
      {
        // reseed based on the reconstruction region
        _randomObject.setSeed(_randomSeed + inspectionRegion.getRegionId());
        for (JointInspectionData jointInspectionData : inspectionRegion.getJointInspectionDataList())
        {
          FeatureGenerator generatorForJoint = chooseGeneratorForJoint(jointInspectionData);
          _jointInspectionDataToGeneratorDataMap.put(jointInspectionData,
                                                     generatorForJoint.generateDataForJoint(jointInspectionData));
          if (_abort)
            return;
        }
        if (_abort)
          return;
      }
      Assert.expect(_jointInspectionDataToGeneratorDataMap.size() > 0);

      for (ReconstructionRegion generatingInspectionRegion : inspectionRegions)
      {

        PanelRectangle panelRegionRect = generatingInspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
        java.awt.Shape scaledRegion = panelToPixelCoordinateTransform.createTransformedShape(panelRegionRect.
            getRectangle2D());

        int regionWidth = (int)Math.round(scaledRegion.getBounds2D().getWidth());
        int regionHeight = (int)Math.round(scaledRegion.getBounds2D().getHeight());


        // Create an image for each slice of the inspection region.
        final int numberOfSlices = generatingInspectionRegion.getNumberOfSlices();
        for (FocusGroup focusGroup : generatingInspectionRegion.getFocusGroups())
        {
          for (Slice slice : focusGroup.getSlices())
          {
            // ignore the surface modeling slices, we should not create images for this
//            if (slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION))
            if (slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION) && slice.createSlice())
            {
//              if (verboseModeEnabled)
//              {
//                StringBuilder imageGenerationDiagMessage = new StringBuilder();
//                imageGenerationDiagMessage.append("Generating image for");
//                imageGenerationDiagMessage.append(" Region:");
//                imageGenerationDiagMessage.append(generatingInspectionRegion.getRegionId());
//                imageGenerationDiagMessage.append(" Slice:");
//                imageGenerationDiagMessage.append(slice.getSliceName().getName());
//
//                System.out.println(imageGenerationDiagMessage.toString());
//              }
              Image regionImage = null;
              if (_specialBlackMode || _specialWhiteMode || _specialRandomMode)
              {
                regionImage = createImageWithSpecialContents(regionWidth, regionHeight);
              }
              else
              {
                regionImage = createPopulatedRegionImage(generatingInspectionRegion,
                                                         regionWidth,
                                                         regionHeight,
                                                         inspectionRegions,
                                                         false);
              }

              if (_abort)
                return;

              // Added by Seng Yew on 22-Apr-2011
              List<Float> arrayZHeight = new ArrayList<Float>();//add by BH
              List<Float> arraySharpness = new ArrayList<Float>();//add by BH
              Subtype subtype = generatingInspectionRegion.getSubtypes().iterator().next();
              if(ImageProcessingAlgorithm.useUserDefinedGrayLevel(subtype))
              {
                ImageProcessingAlgorithm.applyOriginalImageWithUserDefinedGrayLevel(subtype, regionImage);
              }
//              Filter.normalizeWithMinMaxValue(regionImage, generatingInspectionRegion.getMinimumGraylevel(), generatingInspectionRegion.getMaximumGraylevel());
              ReconstructedSlice reconstructedSlice = new ReconstructedSlice(regionImage,
                                                                             slice.getSliceName(),
                                                                             slice.getFocusInstruction().getZHeightInNanoMeters(),
                                                                             arrayZHeight,//add by BH
                                                                             arraySharpness);//add by BH
              _imageManager.saveAdjustFocusImage(reconstructedSlice, generatingInspectionRegion, Directory.getAdjustFocusImagesDir(project.getName()));

              reconstructedSlice.decrementReferenceCount();
              regionImage.decrementReferenceCount();
              if (_abort)
                return;
            }
            if (_abort)
              return;
          }
          if (_abort)
            return;
        }

      }
    }
    finally
    {
      _imageGenerationInProgress.setValue(false);
      _jointInspectionDataToGeneratorDataMap.clear();
    }
  }

  /**
   * @author George A. David
   */
  public static void waitForImageGenerationToStop()
  {
    try
    {
      _imageGenerationInProgress.waitUntilFalse();
    }
    catch (InterruptedException ex)
    {
      // do nothing
    }
  }

  /**
   * Creates an image containing the joints in each inspection region.
   *
   * @author Patrick Lacz
   */
  private Image createPopulatedRegionImage(ReconstructionRegion generatingInspectionRegion,
                                           int regionWidth,
                                           int regionHeight,
                                           Collection<ReconstructionRegion> inspectionRegions,
                                           boolean alwaysUseGoodJoints)
  {
    // Create the image for this reconstruction region
    // @todo : when we perform different logic based on slice, this is where the for-each-slice loop should move back to.

    LinkedList<Pair<JointInspectionData, AffineTransform>> sameSideJoints =
        new LinkedList<Pair<JointInspectionData, AffineTransform>>();
    LinkedList<Pair<JointInspectionData, AffineTransform>> backSideJoints =
        new LinkedList<Pair<JointInspectionData, AffineTransform>>();

    Image regionImage = new Image(regionWidth, regionHeight);

    Paint.fillImage(regionImage, _backgroundColor);

    PanelRectangle generatingRegionRect = generatingInspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters();

    // We iterate over all the regions so that we draw all the joints nearby the current region as well.
    // This for-loop finds the joints that are potentially in the reconstruction region and classifies them
    // by which side of the board they lie on.
    for (ReconstructionRegion drawInspectionRegion : inspectionRegions)
    {
      boolean backSide = drawInspectionRegion.isTopSide() != generatingInspectionRegion.isTopSide();

      // test for overlap
      PanelRectangle drawRegionRect = drawInspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
      if (drawRegionRect.intersects(generatingRegionRect) == false)
        continue;

      double PIXELS_PER_NANOMETER = 1.0 / MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
      
      if(drawInspectionRegion.getTestSubProgram().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
        PIXELS_PER_NANOMETER = 1.0 / (double)MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
    
      AffineTransform sourcePanelToDestPanelTransform = AffineTransform.getTranslateInstance(
          PIXELS_PER_NANOMETER * (drawRegionRect.getMinX() - generatingRegionRect.getMinX()),
          PIXELS_PER_NANOMETER * (drawRegionRect.getMinY() - generatingRegionRect.getMinY()));

      List<JointInspectionData> jointInspectionDataList = drawInspectionRegion.getJointInspectionDataList();
      for (JointInspectionData jointInspectionData : jointInspectionDataList)
      {
        // @todo : we could do a bit more here for intersection tests
        if (backSide)
          backSideJoints.add(new Pair<JointInspectionData, AffineTransform>(jointInspectionData,
              sourcePanelToDestPanelTransform));
        else
          sameSideJoints.add(new Pair<JointInspectionData, AffineTransform>(jointInspectionData,
              sourcePanelToDestPanelTransform));
      }
    }

    // add the joints on the same side of the board as the inspection region
    for (Pair<JointInspectionData, AffineTransform> jointWithPanelTransform : sameSideJoints)
    {
      JointInspectionData jointInspectionData = jointWithPanelTransform.getFirst();
      AffineTransform sourceRegionToDestRegionTransform = jointWithPanelTransform.getSecond();

      JointGeneratorData jointGeneratorData = null;
      if (alwaysUseGoodJoints)
      {
        GoodJointGenerator.initializeIfNecessary();
        FeatureGenerator goodJointGenerator = new GoodJointGenerator();
        jointGeneratorData = goodJointGenerator.generateDataForJoint(jointInspectionData);
      }
      else
      {
        jointGeneratorData = _jointInspectionDataToGeneratorDataMap.get(jointInspectionData);
      }
      FeatureGenerator featureGenerator = jointGeneratorData.getFeatureGenerator();
      featureGenerator.drawJointFeatureInImage(regionImage, sourceRegionToDestRegionTransform, jointGeneratorData);
    }

    return regionImage;
  }

  /**
   * Decides which Generator to use for this joint
   * @author Patrick Lacz
   */
  private FeatureGenerator chooseGeneratorForJoint(JointInspectionData jointInspectionData)
  {
    if (_specialShortMode == true)
    {
      Assert.expect(_shortGenerator != null);
      return _shortGenerator;
    }
    else if (jointInspectionData.getInspectionFamilyEnum().equals(InspectionFamilyEnum.GRID_ARRAY))
    {
      float randomValue = _randomObject.nextFloat();
      if (randomValue < _BGAOpenChance)
        return _BGAOpenGenerator;
    }

    GoodJointGenerator.initializeIfNecessary();
    return _goodJointGenerator;
  }


  /**
   * Generates an ImageSetData for this generated image set.
   *
   * @author Matt Wharton
   */
  private static ImageSetData generateImageSetData(Project project, String imageSet)
  {
    Assert.expect(project != null);
    Assert.expect(imageSet != null);

    ImageSetData imageSetData = new ImageSetData();

    if (_description == null)
      _description = "Generated image run: " + _imageSetName;

    // Set up the image set root, name, type, description, project name, and version number.
    imageSetData.setImageSetName(imageSet);
    imageSetData.setUserDescription(_description);
    imageSetData.setProjectName(project.getName());
    try
    {
      long timeInMils = StringUtil.convertStringToTimeInMilliSeconds(imageSet);
      imageSetData.setDateInMils(timeInMils);
    }
    catch(BadFormatException bfe)
    {
      if (UnitTest.unitTesting())
      {
        imageSetData.setDateInMils(System.currentTimeMillis());
      }
      else
      {
        Assert.expect(false, "Image Set name is not in date-time format");
      }
    }
    imageSetData.setBoardPopulated(true); //@todo support unpopulated boards
    imageSetData.setTestProgramVersionNumber(project.getTestProgramVersion());
    imageSetData.setMachineSerialNumber("0");

    Panel panel = project.getPanel();
    BoardType boardType = null;
    Board boardInstance = null;
    JointTypeEnum jointTypeEnum = null;

    // Set up the filters and image set type for this image set.
    TestProgram testProgram = project.getTestProgram();
    testProgram.clearFilters();
    // These "ifs" need to work from general to specific to ensure that the filter and image
    // set type settings are applied in the correct order.
    // We've already validated that the specified filters are a valid combination, so we can
    // assume those are correct here.
    if (_panelFilterSpecified)
    {
      Assert.expect(_boardTypeFilterSpecified == false);
      Assert.expect(_boardInstanceFilterSpecified == false);

      // No explicit filters are needed.
      imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.PANEL);
    }
    if (_boardTypeFilterSpecified)
    {
      Assert.expect(_panelFilterSpecified == false);
      Assert.expect(_boardTypeName != null);

      if (panel.hasBoardType(_boardTypeName))
      {
        boardType = panel.getBoardType(_boardTypeName);
        testProgram.addFilter(boardType);
      }
      else
      {
        System.out.println("No such board type: " + _boardTypeName);
        System.exit(-1);
      }
    }
    if (_boardInstanceFilterSpecified)
    {
      Assert.expect(_panelFilterSpecified == false);
      Assert.expect(_boardTypeFilterSpecified == true);
      Assert.expect(_boardInstanceName != null);

      if (panel.hasBoard(_boardInstanceName))
      {
        boardInstance = panel.getBoard(_boardInstanceName);
        testProgram.addFilter(boardInstance);
        imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.BOARD);
      }
      else
      {
        System.out.println("No such board instance: " + _boardInstanceName);
        System.exit(-1);
      }
    }
    if (_componentFilterSpecified)
    {
      Assert.expect(_panelFilterSpecified == false);
      Assert.expect(_jointTypeFilterSpecified == false);
      Assert.expect(_subtypeFilterSpecified == false);
      Assert.expect(_componentName != null);

      if (_boardInstanceFilterSpecified == true)
      {
        Assert.expect(_boardInstanceName != null);

        if (panel.hasComponent(_boardInstanceName, _componentName))
        {
          Component component = panel.getComponent(_boardInstanceName, _componentName);
          testProgram.addFilter(component);
        }
        else
        {
          System.out.println("No such component instance: " + _componentName);
          System.exit(-1);
        }
      }
      else
      {
        Assert.expect(_boardTypeName != null);

        if (panel.hasComponentType(_boardTypeName, _componentName))
        {
          ComponentType componentType = panel.getComponentType(_boardTypeName, _componentName);
          testProgram.addFilter(componentType);
        }
        else
        {
          System.out.println("No such component type: " + _componentName);
          System.exit(-1);
        }
      }

      imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.COMPONENT);
    }
    if (_pinFilterSpecified)
    {
      Assert.expect(_panelFilterSpecified == false);
      Assert.expect(_jointTypeFilterSpecified == false);
      Assert.expect(_subtypeFilterSpecified == false);
      Assert.expect(_componentName != null);
      Assert.expect(_pinName != null);

      if (_boardInstanceFilterSpecified == true)
      {
        Assert.expect(_boardInstanceName != null);

        if (panel.hasPad(_boardInstanceName, _componentName, _pinName))
        {
          Pad pad = panel.getPad(_boardInstanceName, _componentName, _pinName);
          testProgram.addFilter(pad);
        }
        else
        {
          System.out.println("No such pad instance: " + _pinName);
          System.exit(-1);
        }
      }
      else
      {
        Assert.expect(_boardTypeName != null);

        if (panel.hasPadType(_boardTypeName, _componentName, _pinName))
        {
          PadType padType = panel.getPadType(_boardTypeName, _componentName, _pinName);
          testProgram.addFilter(padType);
        }
        else
        {
          System.out.println("No such pad type: " + _pinName);
          System.exit(-1);
        }
      }

      imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.PIN);
    }
    if (_jointTypeFilterSpecified)
    {
      Assert.expect(_componentFilterSpecified == false);
      Assert.expect(_pinFilterSpecified == false);
      Assert.expect(_jointTypeName != null);

      if (JointTypeEnum.doesJointTypeEnumExist(_jointTypeName))
      {
        jointTypeEnum = JointTypeEnum.getJointTypeEnum(_jointTypeName);
        testProgram.addFilter(jointTypeEnum);
        imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.JOINT_TYPE);
      }
      else
      {
        System.out.println("No such joint type: " + _jointTypeName);
        System.exit(-1);
      }
    }
    if (_subtypeFilterSpecified)
    {
      Assert.expect(_componentFilterSpecified == false);
      Assert.expect(_pinFilterSpecified == false);
      Assert.expect(_jointTypeFilterSpecified == true);
      Assert.expect(jointTypeEnum != null);

      if (panel.doesSubtypeExist(_subtypeName))
      {
        Subtype subtype = panel.getSubtype(_subtypeName);
        testProgram.addFilter(subtype);
        imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.SUBTYPE);
      }
      else
      {
        System.out.println("No such subtype: " + _subtypeName);
        System.exit(-1);
      }
    }

    return imageSetData;
  }

  /**
   * Writes out an image set info file for the specified ImageSetData.

   * @author Matt Wharton
   */
  private void writeImageSetInfoFile(ImageSetData imageSetData) throws DatastoreException
  {
    Assert.expect(imageSetData != null);

    // Write the ImageSetData file out to the image run directory.
    String imageSetPath = Directory.getXrayInspectionImagesDir(imageSetData.getProjectName(), imageSetData.getImageSetName());
    ImageSetInfoWriter imageSetInfoWriter = new ImageSetInfoWriter();
    XrayImageIoUtil.createImageDirectoryIfNeeded(imageSetPath);
    imageSetInfoWriter.write(imageSetData);
  }

  /**
   * Displays the usage information.
   *
   * @author Matt Wharton
   */
  private static void usage()
  {
    System.out.println();
    System.out.print("Usage: ");
    System.out.println(ImageSetGenerator.class.getName());
    System.out.println("\t-(project|import):<project name>");
    System.out.println("\t-imageSet:<image set name>");
    System.out.print("\t-filters:panel|boardType=<boardType>|boardInstance=<board instance>|component:<component>|");
    System.out.println("pin=<pin>|jointType=<joint type>|subtype=<subtype>");
    System.out.println("\t[-verbose:true|false]");
    System.out.println("\t[-special:black|white|shorts|random]");
    System.out.println("\t[-background:<0 to 255>]");
    System.out.println("\t[-seed:<integer>]");
    System.out.println("\t[-bgaopen:<percent-chance>%[,min=<scale>[,max=<scale>]]]");
    System.out.println("\t[-desc:<description without spaces>]");
  }

  /**
   * Parses the command line arguments.
   *
   * @author Matt Wharton
   */
  private static void parseArgs(String[] args)
  {
    Assert.expect(args != null);

    boolean gotProject = false;
    boolean gotImageSet = false;
    boolean gotFilters = false;
    boolean gotVerboseMode = false;
    boolean gotSpecialMode = false;

    _specialWhiteMode = false;
    _specialBlackMode = false;
    _specialRandomMode = false;
    _specialShortMode = false;

    String paramRegEx = "[-|/](project|import|imageset|filters|verbose|special|background|seed|bgaopen|desc):(.+)\\z";
    Pattern paramRegExPattern = Pattern.compile(paramRegEx, Pattern.CASE_INSENSITIVE);
    for (String arg : args)
    {
      Matcher matcher = paramRegExPattern.matcher(arg);
      if (matcher.matches())
      {
        String parameterName = matcher.group(1);
        if (parameterName.equalsIgnoreCase("project"))
        {
          if (gotProject == false)
          {
            _projectName = matcher.group(2);
            gotProject = true;
          }
          else
          {
            // Already specified a project.  Can't specify it twice!
            usage();
            System.exit( -1);
          }
        }
        else if (parameterName.equalsIgnoreCase("import"))
        {
          if (gotProject == false)
          {
            _importName = matcher.group(2);
            gotProject = true;
          } else {
            // Already specified a project.  Can't specify it twice!
            usage();
            System.exit( -1);
          }
        }
        else if (parameterName.equalsIgnoreCase("imageset"))
        {
          if (gotImageSet == false)
          {
            _imageSetName = matcher.group(2);
            gotImageSet = true;
          }
          else
          {
            // Already specified an image set.  Can't specify it twice!
            usage();
            System.exit( -1);
          }
        }
        else if (parameterName.equalsIgnoreCase("filters"))
        {
          if (gotFilters == false)
          {
            parseImageSetFilters(matcher.group(2));
            gotFilters = true;
          }
          else
          {
            // Already specified filters.  Can't specify it twice!
            usage();
            System.exit(-1);
          }
        }
        else if (parameterName.equalsIgnoreCase("verbose"))
        {
          if (gotVerboseMode == false)
          {
            _verboseMode = Boolean.parseBoolean(matcher.group(2));
            gotVerboseMode = true;
          }
          else
          {
            // Already specified verbose mode.  Can't specify it twice!
            usage();
            System.exit( -1);
          }
        }
        else if (parameterName.equalsIgnoreCase("special"))
        {
          if (gotSpecialMode == false)
          {
            parseSpecialMode(matcher.group(2));
            gotSpecialMode = true;
          }
          else
          {
            // Already specified a special mode.  Can't specify it twice!
            usage();
            System.exit( -1);
          }
        }
        else if (parameterName.equalsIgnoreCase("background"))
        {
          try
          {
            _backgroundColor = Float.parseFloat(matcher.group(2));
          }
          catch (NumberFormatException ex)
          {
            usage();
            System.exit(-1);
          }
        }
        else if (parameterName.equalsIgnoreCase("seed"))
        {
          try
          {
            _randomSeed = Integer.parseInt(matcher.group(2));
          }
          catch (NumberFormatException ex1)
          {
            usage();
            System.exit(-1);
          }
        }
        else if (parameterName.equalsIgnoreCase("desc"))
        {
          String descriptionString = matcher.group(2);
          if (descriptionString != null)
            _description = descriptionString;
        }
        else if (parameterName.equalsIgnoreCase("bgaopen"))
        {
          String bgaOptions = matcher.group(2);
          if (bgaOptions != null)
            parseBGAOpen(bgaOptions);
        }
        else
        {
          // Shouldn't get here.
          Assert.expect(false);
        }

      }
      else
      {
        usage();
        System.exit( -1);
      }
    }

    // Double-check to make sure we got all the arguments we need.
    if ((gotProject && gotImageSet && gotFilters) == false)
    {
      usage();
      System.exit( -1);
    }
  }

  /**
   * @author Patrick Lacz
   */
  private static void parseBGAOpen(String bgaOpenOptionsString)
  {
    Assert.expect(bgaOpenOptionsString != null);

    // min/max is the scale factor to increase the open size
    // bgaopen:<number>%[,min=<number>[,max=<number>]]
    String groupedNumberRegEx = "(\\d+|\\d*\\.\\d*)";
    String bgaOpenPatternString = "\\A" + groupedNumberRegEx + "%(?:,min=" + groupedNumberRegEx + "(?:,max=" + groupedNumberRegEx + ")?)?\\Z";
    Pattern bgaOpenRegEx = Pattern.compile(bgaOpenPatternString, Pattern.CASE_INSENSITIVE);
    Matcher bgaOpenMatcher = bgaOpenRegEx.matcher(bgaOpenOptionsString);
    if (bgaOpenMatcher.matches())
    {
      String percentString = bgaOpenMatcher.group(1);
      try
      {
        _BGAOpenChance = Float.parseFloat(percentString)/100.0f;
      }
      catch (NumberFormatException ex)
      {
        usage();
        System.exit(-1);
      }

      String minString = bgaOpenMatcher.group(2);
      if (minString != null)
      {
        try
        {
          _BGAOpenMin = Float.parseFloat(minString);
        }
        catch (NumberFormatException ex1)
        {
          usage();
          System.exit( -1);
        }
      }
      String maxString = bgaOpenMatcher.group(3);
      if (maxString != null)
      {
        try
        {
          _BGAOpenMax = Float.parseFloat(maxString);
        }
        catch (NumberFormatException ex2)
        {
          usage();
          System.exit(-1);
        }
      }
    } else {
      // bgaopen parameters specified in the wrong way!
      usage();
      System.exit( -1);
    }
  }

  /**
   * Parses the different "special" mode options
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  private static void parseSpecialMode(String specialMode)
  {
    Assert.expect(specialMode != null);

    if (specialMode.equalsIgnoreCase("black"))
    {
      _specialBlackMode = true;
    }
    else if (specialMode.equalsIgnoreCase("white"))
    {
      _specialWhiteMode = true;
    }
    else if (specialMode.equalsIgnoreCase("random"))
    {
      _specialRandomMode = true;
    }
    else if (specialMode.equalsIgnoreCase("shorts"))
    {
      _specialShortMode = true;
    }
    else
    {
      usage();
    }
  }

  /**
   * Parses the image set filtering options.
   *
   * @author Matt Wharton
   */
  private static void parseImageSetFilters(String imageSetFilters)
  {
    Assert.expect(imageSetFilters != null);

    String panelFilterRegex = "panel";
    Pattern panelFilterPattern = Pattern.compile(panelFilterRegex, Pattern.CASE_INSENSITIVE);
    String mainFilterRegex = "(boardtype|boardinstance|component|pin|jointtype|subtype)=(.+)";
    Pattern mainFilterPattern = Pattern.compile(mainFilterRegex, Pattern.CASE_INSENSITIVE);

    StringTokenizer tokenizer = new StringTokenizer(imageSetFilters, ";");
    while (tokenizer.hasMoreTokens())
    {
      // Parse the current token.
      String currentFilterStr = tokenizer.nextToken();

      Matcher matcher = panelFilterPattern.matcher(currentFilterStr);
      // Is the current filter setting for the panel (i.e. not the usual filter=value syntax)?
      if (matcher.matches())
      {
        // Make sure we haven't already seen the panel filter setting..can't specify it twice!
        if (_panelFilterSpecified == false)
        {
          _panelFilterSpecified = true;
        }
        else
        {
          usage();
          System.exit(-1);
        }
      }
      // Let's see if any of the other filters match.
      else
      {
        matcher = mainFilterPattern.matcher(currentFilterStr);

        if (matcher.matches())
        {
          String filterType = matcher.group(1);

          if (filterType.equalsIgnoreCase("boardtype"))
          {
            // Make sure we haven't already seen the boardtype filter setting...can't specify it twice!
            if (_boardTypeFilterSpecified == false)
            {
              _boardTypeName = matcher.group(2);
              _boardTypeFilterSpecified = true;
            }
            else
            {
              usage();
              System.exit(-1);
            }
          }
          else if (filterType.equalsIgnoreCase("boardinstance"))
          {
            // Make sure we haven't already seen the boardinstance filter setting...can't specify it twice!
            if (_boardInstanceFilterSpecified == false)
            {
              _boardInstanceName = matcher.group(2);
              _boardInstanceFilterSpecified = true;
            }
            else
            {
              usage();
              System.exit(-1);
            }
          }
          else if (filterType.equalsIgnoreCase("component"))
          {
            // Make sure we haven't already seen the component filter setting...can't specify it twice!
            if (_componentFilterSpecified == false)
            {
              _componentName = matcher.group(2);
              _componentFilterSpecified = true;
            }
            else
            {
              usage();
              System.exit(-1);
            }
          }
          else if (filterType.equalsIgnoreCase("pin"))
          {
            // Make sure we haven't already seen the pin filter setting...can't specify it twice!
            if (_pinFilterSpecified == false)
            {
              _pinName = matcher.group(2);
              _pinFilterSpecified = true;
            }
            else
            {
              usage();
              System.exit(-1);
            }
          }
          else if (filterType.equalsIgnoreCase("jointtype"))
          {
            // Make sure we haven't already seen the jointtype filter setting...can't specify it twice!
            if (_jointTypeFilterSpecified == false)
            {
              _jointTypeName = matcher.group(2);
              _jointTypeFilterSpecified = true;
            }
            else
            {
              usage();
              System.exit(-1);
            }
          }
          else if (filterType.equalsIgnoreCase("subtype"))
          {
            // Make sure we haven't already seen the subtype filter setting...can't specify it twice!
            if (_subtypeFilterSpecified == false)
            {
              _subtypeName = matcher.group(2);
              _subtypeFilterSpecified = true;
            }
            else
            {
              usage();
              System.exit(-1);
            }
          }
          else
          {
            // Shouldn't get here...
            Assert.expect(false);
          }
        }
        else
        {
          usage();
          System.exit(-1);
        }
      }
    }

    // Check to make sure the specified combination of filters makes sense.
    // Either panel or board type should be specified.
    if (_panelFilterSpecified)
    {
      // If the panel filter was specified, we shouldn't see board type, board instance,
      // component, or pad filters.
      if (_boardTypeFilterSpecified
          || _boardInstanceFilterSpecified
          || _componentFilterSpecified
          || _pinFilterSpecified)
      {
        usage();
        System.exit(-1);
      }
    }
    else if (_boardTypeFilterSpecified)
    {
      // If the board type filter was specified, we shouldn't see the panel filter.
      if (_panelFilterSpecified)
      {
        usage();
        System.exit(-1);
      }
    }
    else
    {
      usage();
      System.exit(-1);
    }

    // If the board instance filter was specified, board type filter should also have been specified.
    if ((_boardInstanceFilterSpecified == true) && (_boardTypeFilterSpecified == false))
    {
      usage();
      System.exit(-1);
    }

    // If the pad filter was specified, the component filter should also have been specified.
    if ((_pinFilterSpecified == true) && (_componentFilterSpecified == false))
    {
      usage();
      System.exit(-1);
    }

    // If the subtype filter was specified, the jointtype filter should also have been specified.
    if ((_subtypeFilterSpecified == true) && (_jointTypeFilterSpecified == false))
    {
      usage();
      System.exit(-1);
    }

    // We shouldn't see jointtype and/or subtype in conjunction with component and/or pad.
    if ((_jointTypeFilterSpecified || _subtypeFilterSpecified)
        && (_componentFilterSpecified || _pinFilterSpecified))
    {
      usage();
      System.exit(-1);
    }
  }

  /**
   * Main method.
   *
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    // Setup the assert logging.
    Assert.setLogFile("imageSetGeneratorInternalErrors", Version.getVersion());

    // Parse the command line args.
    parseArgs(args);
    Assert.expect(_projectName != null || _importName != null);
    Assert.expect(_imageSetName != null);

    try
    {
      Config.getInstance().loadIfNecessary();
      Project project;
      if (_projectName != null)
      {
        BooleanRef abortedDuringLoad = new BooleanRef();
        project = Project.load(_projectName, abortedDuringLoad);
        if(abortedDuringLoad.getValue())
        {
          System.out.println("The project load was aborted.");
          return;
        }
      }
      else
      {
        Assert.expect( _importName != null );

        List<LocalizedString> warnings = new ArrayList<LocalizedString>();
        project = Project.importProjectFromNdfs(_importName, warnings);
      }
      ImageSetData imageSetData = generateImageSetData(project, _imageSetName);
      ImageSetGenerator.getInstance().generatePseudoImageSet(project, imageSetData, _verboseMode);
    }
    catch (Throwable ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * Create verification regions for alignment. The images
   * created are always offset. The offset is chosen
   * at random each time this is called. It will also delete
   * any previous verification images.
   * @author George A. David
   */
  public static void generateVerificationRegions(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    _abort = false;
    Panel panel = project.getPanel();

    if(_abort)
      return;

    int xOffsetInNanoMeters = 0;
    int yOffsetInNanoMeters = 0;
    if(Math.random() < 0.5)
      xOffsetInNanoMeters = 254000;
    else
      xOffsetInNanoMeters = -254000;

    if(Math.random() < 0.5)
      yOffsetInNanoMeters = 254000;
    else
      yOffsetInNanoMeters = -254000;

    double degreesOffset = 0;
    if(Math.random() < 0.5)
      degreesOffset = 0.05;
    else
      degreesOffset = -0.05;

    SurroundingPadUtil padUtil = new SurroundingPadUtil();
    padUtil.setUseThroughHolePadsOnBothSides(true);
    padUtil.setUseBoundsForDeterminingVicinity(true);
    padUtil.setPads(panel.getPadsUnsorted());

    TestProgram testProgram = project.getTestProgram();
    
    for (TestSubProgram subProgram : testProgram.getAllTestSubPrograms())
    {
      double nanoMetersToPixelZoom = 1.0 / (double)MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
    
      if(subProgram.isHighMagnification())
        nanoMetersToPixelZoom = 1.0 / (double)MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();

      AffineTransform nanoMetersToPixelTransform = AffineTransform.getScaleInstance(nanoMetersToPixelZoom, -nanoMetersToPixelZoom);
      xOffsetInNanoMeters *= -1;
      yOffsetInNanoMeters *= -1;
      degreesOffset *= -1;

      for (ReconstructionRegion region : subProgram.getVerificationRegions())
      {
        PanelRectangle regionRect = region.getRegionRectangleRelativeToPanelInNanoMeters();

        Rectangle2D pixelRect = nanoMetersToPixelTransform.createTransformedShape(regionRect.getRectangle2D()).getBounds2D();

        java.awt.image.BufferedImage bufferedImage = new java.awt.image.BufferedImage((int)Math.ceil(pixelRect.getWidth()),
            (int)Math.ceil(pixelRect.getHeight()),
            java.awt.image.BufferedImage.TYPE_BYTE_GRAY);
        AffineTransform transform = new AffineTransform(nanoMetersToPixelTransform);
        transform.translate( -regionRect.getMinX(), -regionRect.getMinY() - regionRect.getHeight());

        java.awt.Graphics2D graphics2d = (java.awt.Graphics2D)bufferedImage.getGraphics();
        graphics2d.setTransform(transform);
        graphics2d.setColor(java.awt.Color.lightGray);
        graphics2d.fill(regionRect.getRectangle2D());
        transform.rotate(Math.toRadians(degreesOffset));
//        transform.translate(xOffsetInNanoMeters, yOffsetInNanoMeters);
    //      transform.translate(-1524000, 254000);
    //      transform.translate(0, -254000);
    //      transform.translate(254000, 0);
        graphics2d.setTransform(transform);
        graphics2d.setColor(java.awt.Color.darkGray);

        for (Pad pad : padUtil.getIntersectingPadsInRectangle(regionRect, region.isTopSide()))
        {
          graphics2d.fill(pad.getShapeRelativeToPanelInNanoMeters());
        }

        try

        {
          _imageManager.saveVerificationImage(project.getName(),
                                             region,
                                             bufferedImage);
          ImageGenerationObservable.getInstance().stateChanged(new ImageGenerationEvent(ImageGenerationEventEnum.VERIFICATION_IMAGE_ACQUIRED));
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
        }
        if (_abort)
          return;
      }
    }
  }

  /**
   * Prepares the generator to create verification images for the given program.
   *
   * @author Matt Wharton
   */
  public void initializeForVerificationImages(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    Panel panel = testProgram.getProject().getPanel();
    _surroundingPadUtilForVerificationImages = new SurroundingPadUtil();
    _surroundingPadUtilForVerificationImages.setUseThroughHolePadsOnBothSides(true);
    _surroundingPadUtilForVerificationImages.setUseBoundsForDeterminingVicinity(true);
    _surroundingPadUtilForVerificationImages.setPads(panel.getPadsUnsorted());
  }

  /**
   * Tells the generator we're done generating verification images for the current program.
   *
   * @author Matt Wharton
   */
  public void doneGeneratingVerificationImages()
  {
    _surroundingPadUtilForVerificationImages = null;
  }

  /**
   * Generates a single verification image for the specified region.
   *
   * @author Matt Wharton
   */
  public ReconstructedImages generateVerificationImages(ReconstructionRegion verificationRegion)
  {
    Assert.expect(verificationRegion != null);
    Assert.expect(_surroundingPadUtilForVerificationImages != null);

    TestSubProgram testSubProgram = verificationRegion.getTestSubProgram();
    TestProgram testProgram = testSubProgram.getTestProgram();
    Project project = testProgram.getProject();
    Panel panel = project.getPanel();

    double nanoMetersToPixelZoom = 1.0 / (double)MagnificationEnum.NOMINAL.getNanoMetersPerPixel();

    if(testSubProgram.getMagnificationType().equals(MagnificationTypeEnum.HIGH))
      nanoMetersToPixelZoom = 1.0 / (double)MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
    
    AffineTransform nanoMetersToPixelTransform = AffineTransform.getScaleInstance(nanoMetersToPixelZoom, -nanoMetersToPixelZoom);

    PanelRectangle regionRect = verificationRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    Rectangle2D pixelRect = nanoMetersToPixelTransform.createTransformedShape(regionRect.getRectangle2D()).getBounds2D();

    java.awt.image.BufferedImage bufferedImage = new java.awt.image.BufferedImage((int)Math.ceil(pixelRect.getWidth()),
                                                                                  (int)Math.ceil(pixelRect.getHeight()),
                                                                                  java.awt.image.BufferedImage.TYPE_BYTE_GRAY);
    AffineTransform transform = new AffineTransform(nanoMetersToPixelTransform);
    transform.translate( -regionRect.getMinX(), -regionRect.getMinY() - regionRect.getHeight());

    java.awt.Graphics2D graphics2d = (java.awt.Graphics2D)bufferedImage.getGraphics();
    graphics2d.setTransform(transform);
    graphics2d.setColor(java.awt.Color.lightGray);
    graphics2d.fill(regionRect.getRectangle2D());
    graphics2d.setTransform(transform);
    graphics2d.setColor(java.awt.Color.darkGray);

    // Fill in the applicable pads in the verification region.
    for (Pad pad : _surroundingPadUtilForVerificationImages.getIntersectingPadsInRectangle(regionRect, verificationRegion.isTopSide()))
    {
      graphics2d.fill(pad.getShapeRelativeToPanelInNanoMeters());
    } 
   
    // XCR 3731: No VL images generated - Offline
    if(VirtualLiveManager.getInstance().isVirtualLiveMode() == true)
    {
      _sliceHeightInNanometers = VirtualLiveImageGenerationSettings.getInstance().getDefaultCenterZHeightInNanometers();
    }
    else
    {
      _sliceHeightInNanometers = 0;
    }
    ReconstructedImages reconImages = new ReconstructedImages(verificationRegion);
    List<Float> arrayZHeight = new ArrayList<Float>();//add by BH
    List<Float> arraySharpness = new ArrayList<Float>();//add by BH
    Image image = Image.createFloatImageFromBufferedImage(bufferedImage);
    reconImages.addImage(image, SliceNameEnum.PAD, _sliceHeightInNanometers, arrayZHeight, arraySharpness);
    image.decrementReferenceCount();

    return reconImages;
  }

  /**
   * @author Peter Esbensen
   */
  private static Image createImageWithSpecialContents(int imageWidth, int imageHeight)
  {
    Image returnImage = null;

    if (_specialBlackMode)
    {
      returnImage = new Image(imageWidth, imageHeight);
      Paint.fillImage(returnImage, 0.0f);
    }
    else if (_specialWhiteMode)
    {
      returnImage = new Image(imageWidth, imageHeight);
      Paint.fillImage(returnImage, 255.0f);
    }
    else if (_specialRandomMode)
    {
    BufferedImage bufferedImage = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_BYTE_GRAY);

    java.awt.Graphics2D g2d = (java.awt.Graphics2D)bufferedImage.getGraphics();
      g2d.setColor(java.awt.Color.WHITE);
      g2d.fillRect(0, 0, imageWidth, imageHeight);
      for (int i = 0; i < 3; ++i)
      {
        drawRandomShapeInImage(bufferedImage);
      }
      returnImage = Image.createFloatImageFromBufferedImage(bufferedImage);
    }
    else
    {
      Assert.expect(false); // at least one special mode should be set
    }

    Assert.expect(returnImage != null);

    return returnImage;
  }

  /**
   * @author Peter Esbensen
   */
  private static void drawRandomShapeInImage(BufferedImage bufferedImage)
  {
    Assert.expect(bufferedImage != null);

    Random random = new Random();
    int imageWidth = bufferedImage.getWidth();
    int imageHeight = bufferedImage.getHeight();
    int randomWidth = (int)(random.nextFloat() * imageWidth);
    int randomHeight = (int)(random.nextFloat() * imageHeight);
    int maxAllowableX = imageWidth - randomWidth;
    int maxAllowableY = imageHeight - randomHeight;
    int randomX = (int)(random.nextFloat() * maxAllowableX);
    int randomY = (int)(random.nextFloat() * maxAllowableY);
    int shapeId = (int)(random.nextFloat() * 4);
    java.awt.Graphics2D g2d = bufferedImage.createGraphics();
    g2d.setColor(java.awt.Color.BLACK);
    if (shapeId == 0)
    {
      g2d.fillRect(randomX, randomY, randomWidth, randomHeight);
    }
    else if (shapeId == 1)
    {
      g2d.fillArc(randomX, randomY, randomWidth, randomHeight, 0, 180);
    }
    else if (shapeId == 2)
    {
      g2d.drawLine(randomX, randomY, randomX + randomWidth, randomY + randomHeight);
    }
    else if (shapeId == 3)
    {
      g2d.fillOval(randomX, randomY, randomWidth, randomHeight);
    }
    else
    {
      Assert.expect(false);
    }
  }

  /**
   * @author George A. David
   */
  public static void abort()
  {
    _abort = true;
  }
}
