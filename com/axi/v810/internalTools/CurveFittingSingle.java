/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.business.autoCal;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import java.awt.*;
import java.awt.geom.*;
import java.io.*;
import java.math.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;
import org.jfree.chart.*;
import org.jfree.chart.annotations.*;
import org.jfree.chart.plot.*;
import org.jfree.data.category.*;
import org.jfree.data.xy.*;
import com.axi.util.math.*;

/**
 *
 * @author ying-huan.chu
 */
public class CurveFittingSingle
{

  static double _min = Double.MAX_VALUE;
  static double _max = Double.MIN_VALUE;

  public static void main(String[] args)
  {
    java.util.List<Double> magnificationList = new ArrayList<>();
    java.util.List<Double> sharpnessList = new ArrayList<>();
    double maximumPointInFile = 0;
    BufferedReader br = null;
    try
    {
      br = new BufferedReader(new FileReader("D:\\vstore\\5.4_Dev\\axi\\java\\com\\axi\\v810\\business\\autoCal\\autotest\\data\\CurveFitting\\highMagnification2013.08.19.20.37.38.195.csv"));
      String currentLine = "";
      boolean startRead = false;
      int line = 0;
      while ((currentLine = br.readLine()) != null)
      {
        if (line == 0)
        {
          //timeList.add(currentLine);
        }
        if (currentLine.startsWith(",Magnification = "))
        {
          String[] lineArray = currentLine.split(" ");
          //maximumPointInFileList.add(Float.parseFloat(lineArray[2]));
          maximumPointInFile = Float.parseFloat(lineArray[2]);
        }
        if (startRead)
        {
          String[] lineArray = currentLine.split(",");
          for (int i = 0; i < lineArray.length; ++i)
          {
            if (lineArray[i].length() != 0)
            {
              if (i == 2)
              {
                magnificationList.add(Double.parseDouble(lineArray[i]));
              }
              if (i == 3)
              {
                sharpnessList.add(Double.parseDouble(lineArray[i]));
              }
            }
          }
        }
        if (currentLine.startsWith(",,Mag,Sharpness"))
        {
          startRead = true;
        }
        line += 1;
      }
    }
    catch (Exception ex)
    {
      System.out.println("IOException caught");
    }

    final XYSeries beforeCurveFitSeries = new XYSeries("Before Curve Fit");
    for (int i = 0; i < magnificationList.size(); ++i)
    {
      beforeCurveFitSeries.add(magnificationList.get(i), sharpnessList.get(i));
    }

    double[] curveFittedSharpness = GaussianCurve(convertListToArray(magnificationList), convertListToArray(sharpnessList));
    
    //XCR1772 -Magnification Calibration Crash by Anthony FOng -Test Case
    double maxMagValue = GaussianModelCurveFitting.getGaussianFitPeakX(curveFittedSharpness, convertListToArray(sharpnessList));
   
    final XYSeries afterCurveFitSeries = new XYSeries("After Curve Fit");
    for (int i = 0; i < sharpnessList.size(); ++i)
    {
      afterCurveFitSeries.add(magnificationList.get(i).doubleValue(), curveFittedSharpness[i]);
    }
    final XYSeriesCollection dataset = new XYSeriesCollection();
    dataset.addSeries(beforeCurveFitSeries);
    dataset.addSeries(afterCurveFitSeries);

    final JFreeChart chart = ChartFactory.createXYLineChart(
      "Magnification vs Sharpness", // chart title
      "Magnification", // x axis label
      "Sharpness", // y axis label
      dataset, // data
      PlotOrientation.VERTICAL,
      true, // include legend
      true, // tooltips
      false // urls
      );
    final XYPlot plot = chart.getXYPlot();
    ValueMarker marker = new ValueMarker((double) maximumPointInFile);  // position is the value on the axis
    marker.setPaint(Color.black);
    plot.addDomainMarker(marker);

    final ChartPanel chartPanel = new ChartPanel(chart);
    chartPanel.setPreferredSize(new java.awt.Dimension(800, 800));

    EscapeDialog lineGraphDialog = new EscapeDialog();
    //mainPanel.setPreferredSize(new Dimension(1600, 800));
    lineGraphDialog.getContentPane().add(chartPanel, BorderLayout.CENTER);
    lineGraphDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    lineGraphDialog.pack();
    lineGraphDialog.setVisible(true);
    //lineGraphDialog.setAlwaysOnTop(true);
    lineGraphDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
  }

  public static double[] normalize(double[] y)
  {
    double[] yNormalized = new double[y.length];
    _min = Double.MAX_VALUE;
    _max = Double.MIN_VALUE;
    for (int i = 0; i < y.length; i++)
    {
      if (y[i] < _min)
      {
        _min = y[i];
      }
      if (y[i] > _max)
      {
        _max = y[i];
      }

    }
    for (int i = 0; i < y.length; i++)
    {

      yNormalized[i] = (y[i] - _min) / (_max - _min);
    }

    return yNormalized;
  }

  public static double[] GaussianCurve(double[] x, double[] y)
  {
    int arraySize = x.length;
    int halfArraySize = x.length / 2;
    GaussianCurveFunction gaussianCurveFunction = new GaussianCurveFunction();

    double[] a = new double[4];


    a[1] = 9.08690909090901;//High mag
    //a[1] = 5.2; // mu for Low mag
    a[2] = 0.03; // sigma
    //a[0] = 1 / (a[2] * Math.sqrt(2 * Math.PI)); // k
    a[0] = 1;
    a[3] = 0;

    y = normalize(y);
    LevenbergMarquardtSolver1D lm_solver = new LevenbergMarquardtSolver1D(gaussianCurveFunction);
    //lm_solver.setConvergenceCriteria(1E-10, 0.0, 1E-3, 30);
    lm_solver.setConvergenceCriteria(1E-10, 0.0, 1E-15, 300000);
    lm_solver.solve(x, y, a);
    double[] a_est = lm_solver.get_a();
    
    //if (arrays_equal(a, a_est, 1E-3))
    //if (arrays_equal(a, a_est, 0.1))
    {
      System.out.println("Initial estimate:");
      print_array(a);

      System.out.println("Final estimate:");
      print_array(a_est);

      System.out.printf("Iterations: %d\n", lm_solver.get_nIter());
      System.out.printf("Chi squared: %f\n", lm_solver.get_chiSq());
    }

    double[] y_result = new double[x.length];
    for (int i = 0; i < x.length; i++)
    {
      y_result[i] = _min + (_max - _min) * gaussianCurveFunction.evalFunction(a_est, x[i]);
      //y_result[i] = gaussianCurveFunction.evalFunction(a, x[i]);
    }
    return y_result;
  }

  public static void print_array(double[] a)
  {
    for (double v : a)
    {
      System.out.printf("%f, ", v);
    }
    System.out.println("");
  }

  public static boolean arrays_equal(double[] a, double[] b, double tol)
  {
    assert a.length == b.length;
    for (int i = 0; i < a.length; i++)
    {
      double absDiff = Math.abs(b[i] - a[i]);
      if (absDiff > tol)
      {
        return false;
      }
    }
    return true;
  }

  public static double[] convertListToArray(java.util.List<Double> list)
  {
    double[] array = new double[list.size()];
    for (int i = 0; i < list.size(); ++i)
    {
      array[i] = list.get(i);
    }
    return array;
  }
}
