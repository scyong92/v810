package com.axi.v810.internalTools;

import java.awt.geom.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

public class LowerImagingChainTest
{
  private enum ProgramTypeEnum {COUPON, VERIFICATION, CUSTOM};
  private enum CouponTypeEnum {SYSFID, MAG, ALL};
  private enum ReacquireTypeEnum {LIGHTEST, AVERAGE};

  private static boolean _panelThicknessSpecified = false;
  private static int _panelThicknessMils = 60;
  private static boolean _saveReconstructedImages = true;
  private static int _extraXBorderToImageMils = 0;
  private static int _extraYBorderToImageMils = 0;
  private static String _projectToLoad = "NEPCON_WIDE_EW";
  private static int _panelRotation = 90;
  private static int _regionWMils = 300;
  private static int _regionHMils = 300;
  private static int _areaXMils = -150;
  private static int _areaYMils = -150;
  private static int _areaWMils = 300;
  private static int _areaHMils = 300;
  private static boolean _createDeck = false;
  private static int _numSlicesInDeck = 1;
  private static int _zOffsetOfDeckMils = 0;
  private static int _zShiftInMils = 5;
  private static int _numLoops = 1;
  private static ProgramTypeEnum _programType = ProgramTypeEnum.COUPON;
  private static CouponTypeEnum _couponType = CouponTypeEnum.ALL;
  private static boolean _markDoneAtEnd = false;
  private static boolean _reacquireImages = false;
  private static ReacquireTypeEnum _reacquireType = ReacquireTypeEnum.LIGHTEST;

  private HardwareWorkerThread _hardwareWorkerThread = null;
  private XrayTester _xrayTester = null;
  private TestProgram _testProgram = null;
  private ImageManager _imageManager = ImageManager.getInstance();

  public LowerImagingChainTest()
  {
  }

  /**
   * Main method.
   *
   * @author Dave Ferguson
   */
  public static void main(String[] args)
  {
    // Parse the command line args.
    parseArgs(args);

    try
    {
      Config.getInstance().loadIfNecessary();

      ImageAcquisitionEngine.setDebug(true);

      LowerImagingChainTest test = new LowerImagingChainTest();
      test.createTestProgram();
      test.acquireImages();
    }
    catch (Throwable ex)
    {
      ex.printStackTrace();
    }

    System.exit(0);
  }

  /**
   * Parses the command line arguments.
   *
   * @author Dave Ferguson
   */
  private static void parseArgs(String[] args)
  {
    Assert.expect(args != null);

    boolean gotCoupons = false;
    boolean gotVerification = false;
    boolean gotCustom = false;
    boolean gotDeck = false;
    boolean gotThickness = false;
    boolean gotBorder = false;
    boolean gotSave = false;
    boolean gotLoop = false;
    boolean gotMarkDone = false;
    boolean gotReacquire = false;

    String paramRegEx = "[-|/](coupons|verification|custom|deck|thickness|border|save|loop|markDoneAtEnd|reacquire):(.+)\\z";
    Pattern paramRegExPattern = Pattern.compile(paramRegEx, Pattern.CASE_INSENSITIVE);
    for (String arg : args)
    {
      Matcher matcher = paramRegExPattern.matcher(arg);
      if (matcher.matches())
      {
        String parameterName = matcher.group(1);
        if (parameterName.equalsIgnoreCase("coupons"))
        {
          if (gotVerification == true || gotCustom == true)
          {
            // Can only specify either coupon, verification or custom
            System.out.println("Multiple test program types specified");
            usage();
            System.exit( -1);
          }

          if (gotCoupons == false)
          {
            if (matcher.groupCount() != 2)
            {
              System.out.println("Invalid options on -coupons");
              usage();
              System.exit( -1);
            }

            String couponType = matcher.group(2);

            if (couponType.equalsIgnoreCase("SysFid"))
              _couponType = CouponTypeEnum.SYSFID;
            else if (couponType.equalsIgnoreCase("Mag"))
              _couponType = CouponTypeEnum.MAG;
            else if (couponType.equalsIgnoreCase("All"))
              _couponType = CouponTypeEnum.ALL;
            else
            {
              System.out.println("Invalid -coupon option");
              usage();
              System.exit(-1);
            }

            gotCoupons = true;
            _programType = ProgramTypeEnum.COUPON;
          }
          else
          {
            System.out.println("-coupon option specified more than once");
            usage();
            System.exit( -1);
          }
        }
        else if (parameterName.equalsIgnoreCase("verification"))
        {
          if (gotCoupons == true || gotCustom == true)
          {
            // Can only specify either coupon, verification or custom
            System.out.println("Multiple test program types specified");
            usage();
            System.exit( -1);
          }

          if (gotVerification == false)
          {
            if (matcher.groupCount() != 2)
            {
              System.out.println("Invalid options on -verification");
              usage();
              System.exit( -1);
            }

            parseVerificationArgs(matcher.group(2));

            gotVerification = true;
            _programType = ProgramTypeEnum.VERIFICATION;
          }
          else
          {
            System.out.println("-verification option specified more than once");
            usage();
            System.exit( -1);
          }
        }
        else if (parameterName.equalsIgnoreCase("custom"))
        {
          if (gotVerification == true || gotCoupons == true)
          {
            // Can only specify either coupon, verification or custom
            System.out.println("Multiple test program types specified");
            usage();
            System.exit( -1);
          }

          if (gotCustom == false)
          {
            if (matcher.groupCount() != 2)
            {
              System.out.println("Invalid options on -custom");
              usage();
              System.exit( -1);
            }

            parseCustomArgs(matcher.group(2));

            gotCoupons = true;
            _programType = ProgramTypeEnum.CUSTOM;
          }
          else
          {
            System.out.println("-custom option specified more than once");
            usage();
            System.exit( -1);
          }
        }
        else if (parameterName.equalsIgnoreCase("deck"))
        {
          if (gotDeck == false)
          {
            if (matcher.groupCount() != 2)
            {
              System.out.println("Invalid options on -deck");
              usage();
              System.exit( -1);
            }

            parseDeckArgs(matcher.group(2));
            _createDeck = true;

            gotDeck = true;
          }
          else
          {
            System.out.println("-deck option specified more than once");
            usage();
            System.exit( -1);
          }
        }
        else if (parameterName.equalsIgnoreCase("thickness"))
        {
          if (gotThickness == false)
          {
            if (matcher.groupCount() != 2)
            {
              System.out.println("Invalid option count on -thickness");
              usage();
              System.exit( -1);
            }

            _panelThicknessSpecified = true;
            _panelThicknessMils = Integer.parseInt(matcher.group(2));

            gotThickness = true;
          }
          else
          {
            System.out.println("-thickness option specified more than once");
            usage();
            System.exit( -1);
          }
        }
        else if (parameterName.equalsIgnoreCase("border"))
        {
          if (gotBorder == false)
          {
            if (matcher.groupCount() != 2)
            {
              System.out.println("Invalid option count on -border");
              usage();
              System.exit( -1);
            }

            parseBorderArgs(matcher.group(2));

            gotBorder = true;
          }
          else
          {
            System.out.println("-border option specified more than once");
            usage();
            System.exit( -1);
          }
        }
        else if (parameterName.equalsIgnoreCase("save"))
        {
          if (gotSave == false)
          {
            if (matcher.groupCount() != 2)
            {
              System.out.println("Invalid option count on -save");
              usage();
              System.exit( -1);
            }

            _saveReconstructedImages = Boolean.parseBoolean(matcher.group(2));
            gotSave = true;
          }
          else
          {
            System.out.println("-save option specified more than once");
            usage();
            System.exit( -1);
          }
        }
        else if (parameterName.equalsIgnoreCase("loop"))
        {
          if (gotLoop == false)
          {
            if (matcher.groupCount() != 2)
            {
              System.out.println("Invalid option count on -loop");
              usage();
              System.exit( -1);
            }

            _numLoops = Integer.parseInt(matcher.group(2));

            gotLoop = true;
          }
          else
          {
            System.out.println("-loop option specified more than once");
            usage();
            System.exit( -1);
          }
        }
        else if (parameterName.equalsIgnoreCase("markDoneAtEnd"))
        {
          if (gotMarkDone == false)
          {
            if (matcher.groupCount() != 2)
            {
              System.out.println("Invalid options on -markDoneAtEnd");
              usage();
              System.exit( -1);
            }

            _markDoneAtEnd = Boolean.parseBoolean(matcher.group(2));
            gotMarkDone = true;
          }
          else
          {
            System.out.println("-markDoneAtEnd option specified more than once");
            usage();
            System.exit( -1);
          }
        }
        else if (parameterName.equalsIgnoreCase("reacquire"))
        {
          if (gotReacquire == false)
          {
            if (matcher.groupCount() != 2)
            {
              System.out.println("Invalid options on -reacquire");
              usage();
              System.exit( -1);
            }

            String reacquireType = matcher.group(2);

            if (reacquireType.equalsIgnoreCase("lightest"))
              _reacquireType = ReacquireTypeEnum.LIGHTEST;
            else if (reacquireType.equalsIgnoreCase("average"))
              _reacquireType = ReacquireTypeEnum.AVERAGE;
            else
            {
              System.out.println("Invalid -reacquire option");
              usage();
              System.exit(-1);
            }

            _reacquireImages = true;
            gotReacquire = true;
          }
          else
          {
            System.out.println("-reacquire option specified more than once");
            usage();
            System.exit( -1);
          }
        }

        else
        {
          // Shouldn't get here.
          Assert.expect(false);
        }
      }
      else
      {
        usage();
        usageDetails();
        System.exit( -1);
      }
    }
  }

  /**
   * Displays the usage information.
   *
   * @author Dave Ferguson
   */
  private static void usage()
  {
    System.out.println();
    System.out.print("Usage: ");
    System.out.println(LowerImagingChainTest.class.getName());
    System.out.println("\t-coupons:<sysFid|mag|all>");
    System.out.println("\t-verification:<project name>,<panelRotation>");
    System.out.println("\t-custom:<regionWMils>,<regionHMils>,<areaXMils>,<areaYMils>,<areaWMils>,<areaHMils> [-deck:<numSlices>,<zOffsetMils>,<zShiftMils>]");
    System.out.println("\t[-thickness:<mils>]");
    System.out.println("\t[-border:<xMils>,<yMils>]");
    System.out.println("\t[-save:<true|false>]");
    System.out.println("\t[-loop:<numLoops>]");
    System.out.println("\t[-markDoneAtEnd:<true|false>]");
    System.out.println("\t[-reacquire<lightest|average>:]");
  }

  /**
   * Further description of usage
   *
   * @author Dave Ferguson
   */
  private static void usageDetails()
  {
    System.out.println();
    System.out.println("Further details: ");
    System.out.println("\tChoose either -coupons, -verification, or -custom flag");
    System.out.println("\tCoupons - images system coupons on fixed rail");
    System.out.println("\tVerification - image real verification regions generated by program generation");
    System.out.println("\tCustom - images custom area.  Deck enables fine grain slices through area");
    System.out.println("\tThickness - board thickness");
    System.out.println("\tBorder - extra area to image");
    System.out.println("\tSave - saves reconstructed images");
    System.out.println("\tLoops - loops acquireImage call on IAE");
    System.out.println("\tMarkDoneAtEnd - send mark region complete message after all regions have be constructed");
    System.out.println("\tReacquire - send reacquire message for all regions");
  }


  /**
   * Parses the arguments on the -custom flag
   *
   * @author Dave Ferguson
   */
  private static void parseCustomArgs(String args)
  {
    Assert.expect(args != null);

    StringTokenizer tokenizer = new StringTokenizer(args,",");

    if (tokenizer.countTokens() != 6)
    {
      System.out.println("Invalid option count on -custom");
      usage();
      System.exit( -1);
    }

    _regionWMils = Integer.parseInt(tokenizer.nextToken());
    _regionHMils = Integer.parseInt(tokenizer.nextToken());
    _areaXMils = Integer.parseInt(tokenizer.nextToken());
    _areaYMils = Integer.parseInt(tokenizer.nextToken());
    _areaWMils = Integer.parseInt(tokenizer.nextToken());
    _areaHMils = Integer.parseInt(tokenizer.nextToken());
  }

  /**
   * Parses the arguments on the -deck flag
   *
   * @author Dave Ferguson
   */
  private static void parseDeckArgs(String args)
  {
    Assert.expect(args != null);

    StringTokenizer tokenizer = new StringTokenizer(args,",");

    if (tokenizer.countTokens() != 3)
    {
      System.out.println("Invalid option count on -deck");
      usage();
      System.exit( -1);
    }

    _numSlicesInDeck = Integer.parseInt(tokenizer.nextToken());
    _zOffsetOfDeckMils = Integer.parseInt(tokenizer.nextToken());
    _zShiftInMils = Integer.parseInt(tokenizer.nextToken());
  }

  /**
   * Parses the arguments on the -border flag
   *
   * @author Dave Ferguson
   */
  private static void parseBorderArgs(String args)
  {
    Assert.expect(args != null);

    StringTokenizer tokenizer = new StringTokenizer(args,",");

    if (tokenizer.countTokens() != 2)
    {
      System.out.println("Invalid option count on -border");
      usage();
      System.exit( -1);
    }

    _extraXBorderToImageMils = Integer.parseInt(tokenizer.nextToken());
    _extraYBorderToImageMils = Integer.parseInt(tokenizer.nextToken());
  }

  /**
   * Parses the arguments on the -verification flag
   *
   * @author Dave Ferguson
   */
  private static void parseVerificationArgs(String args)
  {
    Assert.expect(args != null);

    StringTokenizer tokenizer = new StringTokenizer(args,",");

    if (tokenizer.countTokens() != 2)
    {
      System.out.println("Invalid option count on -verification");
      usage();
      System.exit( -1);
    }

    _projectToLoad = tokenizer.nextToken();
    _panelRotation = Integer.parseInt(tokenizer.nextToken());
  }


  /**
   * Creates the appropriate test program based on settings.
   *
   * @author Dave Ferguson
   */
  private void createTestProgram()
  {
    switch(_programType)
    {
      case COUPON:
        _testProgram = createSystemCouponsTestProgram();
        break;
      case VERIFICATION:
        _testProgram = createFromProgramGeneration();
        break;
      case CUSTOM:
        _testProgram = createManualTiledTestProgram();
        break;
      default:
        Assert.expect(false); // shouldn't get here
    }

    Assert.expect(_testProgram != null);
  }

  /**
   * Runs the acquisition of verification images.
   *
   * @author Dave Ferguson
   */
  private void acquireImages()
  {
    Assert.expect(_testProgram != null);
    final TestProgram testProgram = _testProgram;
    final ImageAcquisitionEngine iae = ImageAcquisitionEngine.getInstance();
    final Set<Integer> regionsReacquired = new HashSet<Integer>();

    _hardwareWorkerThread = HardwareWorkerThread.getInstance();
    _xrayTester = XrayTester.getInstance();
    try
    {
      _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
      {
        public void run() throws Exception
        {
          _xrayTester.startup();
        }
      });
    }
    catch (XrayTesterException e)
    {
      e.printStackTrace();
      System.exit(-1);
    }

    try
    {
      iae.initialize();
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace();
      System.exit(-1);
    }

    for (int run = 0; run < _numLoops; ++run)
    {
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            TestExecutionTimer testExecutionTimer = new TestExecutionTimer();
            iae.acquireVerificationImages(testProgram, testExecutionTimer);
          }
          catch (Exception ex)
          {
            ex.printStackTrace();
            System.exit( -1);
          }
        }
      });

      try
      {
        BooleanRef abortedWhileGettingImages = new BooleanRef(false);
        int imagesRecv = 0;
        int imagesExpected = testProgram.getVerificationRegions().size();

        // Twice as many reconstructed images are expected if we're reacquiring images
        if (_reacquireImages)
        {
          imagesExpected *= 2;
        }

        while (imagesRecv < imagesExpected)
        {
          ReconstructedImages reconstructedImages = iae.getReconstructedImages(abortedWhileGettingImages);

          if (reconstructedImages == null)
          {
            System.out.println("ReconstructedImageManager returned Null image");
          }
          else if (abortedWhileGettingImages.getValue())
          {
            System.out.println("ReconstructedImageManager returned Aborted");
          }
          else
          {
            for (ReconstructedSlice s : reconstructedImages.getReconstructedSlices())
            {
              System.out.println("Got image for rr " +
                                 reconstructedImages.getReconstructionRegion().getRegionId() +
                                 " at " +
                                 MathUtil.convertNanoMetersToMils(s.getHeightInNanometers()));
            }

            if (_saveReconstructedImages)
            {
              _imageManager.saveInspectionImagesForOnlineTestDevelopment(reconstructedImages);
            }
            ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
            reconstructedImages.decrementReferenceCount();

            // Reacquire the lightest image only once.
            if (_reacquireImages &&
                regionsReacquired.contains(reconstructionRegion.getRegionId()))
            {
              if (_reacquireType == ReacquireTypeEnum.LIGHTEST)
              {
                iae.reacquireLightestImages(reconstructionRegion);
              }
              else if (_reacquireType == ReacquireTypeEnum.AVERAGE)
              {
                iae.reacquireImages(reconstructionRegion);
              }
              else
              {
                // Shouldn't get here.
                Assert.expect(false);
              }

              regionsReacquired.add(reconstructionRegion.getRegionId());
            }
            else if (!_markDoneAtEnd)
            {
              iae.reconstructionRegionComplete(reconstructionRegion, true);
              iae.freeReconstructedImages(reconstructionRegion);
            }

            imagesRecv++;
          }
        }

        if (_markDoneAtEnd)
        {
          // Issue the RegionComplete messages for all regions
          for (int i = 0; i < testProgram.getVerificationRegions().size(); ++i)
          {
            ReconstructionRegion region = testProgram.getVerificationRegions().get(i);
            iae.reconstructionRegionComplete(region, true);
            iae.freeReconstructedImages(region);
          }
        }
      }
      catch (XrayTesterException ex)
      {
        ex.printStackTrace();
        System.exit(-1);
      }
    }
  }

  /**
   * Creates a test program containing the actual verification regions
   * provided by ProgramGeneration.
   *
   * @author Dave Ferguson
   */
  private TestProgram createFromProgramGeneration()
  {
    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    Project project = null;

    try
    {
      project = Project.importProjectFromNdfs(_projectToLoad, warnings);
      project.getPanel().getPanelSettings().setDegreesRotationRelativeToCad(_panelRotation);

      if (_extraXBorderToImageMils > 0 ||
          _extraYBorderToImageMils > 0)
      {
        TestSubProgram tsp = project.getTestProgram().getTestSubProgram(PanelLocationInSystemEnum.RIGHT);
        PanelRectangle enlargedareaToImage = new PanelRectangle(tsp.getVerificationRegionsBoundsInNanoMeters().getMinX() - MathUtil.convertMilsToNanoMetersInteger(_extraXBorderToImageMils),
                                                                tsp.getVerificationRegionsBoundsInNanoMeters().getMinY() - MathUtil.convertMilsToNanoMetersInteger(_extraYBorderToImageMils),
                                                                tsp.getVerificationRegionsBoundsInNanoMeters().getWidth()  + 2 * MathUtil.convertMilsToNanoMetersInteger(_extraXBorderToImageMils),
                                                                tsp.getVerificationRegionsBoundsInNanoMeters().getHeight() + 2 * MathUtil.convertMilsToNanoMetersInteger(_extraYBorderToImageMils));

        tsp.setVerificationRegionsBoundsInNanoMeters(enlargedareaToImage);
      }

      if (_panelThicknessSpecified)
        project.getPanel().setThicknessInNanoMeters(MathUtil.convertMilsToNanoMetersInteger(_panelThicknessMils));
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }
    Assert.expect(project != null);

    return project.getTestProgram();
  }

  /**
   * Creates a test program containing two verification regions.  One images
   * the system fiducial, the other image the magnification coupon.  Auto-focus
   * is run using the SHARPEST method.
   *
   * @author Dave Ferguson
   */
  private TestProgram createSystemCouponsTestProgram()
  {
    Project dummyProject = new Project(true);
    dummyProject.getProjectState().disable();
    dummyProject.setName("Dummy");
    dummyProject.getProjectState().enable();
    Panel dummyPanel = new Panel();
    dummyPanel.setProject(dummyProject);
    dummyPanel.setWidthInNanoMeters(25400);
    dummyPanel.setLengthInNanoMeters(25400);
    dummyPanel.setThicknessInNanoMeters(25400);
    PanelSettings dummyPanelSettings = new PanelSettings();
    dummyPanelSettings.setPanel(dummyPanel);
    dummyPanelSettings.setDegreesRotationRelativeToCad(0);
    dummyPanel.setPanelSettings(dummyPanelSettings);
    AffineTransform transform = new AffineTransform();
    dummyPanelSettings.setRightManualAlignmentTransform(transform);
    dummyProject.setPanel(dummyPanel);

    // Create here so can set test sub-program of reconstruction regions below.
    TestProgram program = new TestProgram();
    program.setProject(dummyProject);
    TestSubProgram tsp = new TestSubProgram(program);

    Collection<ReconstructionRegion> reconstructionRegions = new ArrayList<ReconstructionRegion>();

    int xSysFidMils = -150;
    int ySysFidMils = -150;
    int regionWidthMils = 300;
    int regionHeightMils = 300;
    int xOffsetFromSysFidToMagCouponMils = -17000;

    PanelRectangle systemFiducialArea = new PanelRectangle(MathUtil.convertMilsToNanoMetersInteger(xSysFidMils),
                                                           MathUtil.convertMilsToNanoMetersInteger(ySysFidMils),
                                                           MathUtil.convertMilsToNanoMetersInteger(regionWidthMils),
                                                           MathUtil.convertMilsToNanoMetersInteger(regionHeightMils));

    PanelRectangle magnificationCouponArea = new PanelRectangle(MathUtil.convertMilsToNanoMetersInteger(xSysFidMils + xOffsetFromSysFidToMagCouponMils),
                                                                MathUtil.convertMilsToNanoMetersInteger(ySysFidMils),
                                                                MathUtil.convertMilsToNanoMetersInteger(regionWidthMils),
                                                                MathUtil.convertMilsToNanoMetersInteger(regionHeightMils));

    PanelRectangle areaToImage = null;
    if (_couponType == CouponTypeEnum.ALL)
    {
      areaToImage = new PanelRectangle(systemFiducialArea.getMinX(),
                                       systemFiducialArea.getMinY(),
                                       systemFiducialArea.getWidth(),
                                       systemFiducialArea.getHeight());

      areaToImage.add(magnificationCouponArea);
    }
    else if (_couponType == CouponTypeEnum.MAG)
    {
      areaToImage = new PanelRectangle(magnificationCouponArea.getMinX(),
                                       magnificationCouponArea.getMinY(),
                                       magnificationCouponArea.getWidth(),
                                       magnificationCouponArea.getHeight());
    }
    else if (_couponType == CouponTypeEnum.SYSFID)
    {
      areaToImage = new PanelRectangle(systemFiducialArea.getMinX(),
                                       systemFiducialArea.getMinY(),
                                       systemFiducialArea.getWidth(),
                                       systemFiducialArea.getHeight());
    }
    else
    {
      // Shouldn't get here.
      Assert.expect(false);
    }

    // System Fiducial Reconstruction Region
    if (_couponType == CouponTypeEnum.ALL ||
        _couponType == CouponTypeEnum.SYSFID)
    {
      ReconstructionRegion rrSysFid = new ReconstructionRegion();
      rrSysFid.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.VERIFICATION);
      rrSysFid.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
      rrSysFid.setTopSide(false);
      rrSysFid.setRegionRectangleRelativeToPanelInNanoMeters(systemFiducialArea);
      rrSysFid.setFocusPriority(FocusPriorityEnum.FIRST);
      rrSysFid.setTestSubProgram(tsp);

      FocusGroup fgSysFid = new FocusGroup();
      fgSysFid.setSingleSidedRegionOfBoard(true);
      fgSysFid.setRelativeZoffsetFromSurfaceInNanoMeters(0);
      FocusSearchParameters fspSysFid = new FocusSearchParameters();
      fspSysFid.setFocusProfileShape(FocusProfileShapeEnum.PEAK);
      FocusRegion frSysFid = new FocusRegion();
      frSysFid.setRectangleRelativeToPanelInNanoMeters(systemFiducialArea);
      fspSysFid.setFocusRegion(frSysFid);
      fgSysFid.setFocusSearchParameters(fspSysFid);
      FocusInstruction fiSysFid = new FocusInstruction();
      fiSysFid.setFocusMethod(FocusMethodEnum.SHARPEST);
      Slice sliceSysFid = new Slice();
      sliceSysFid.setSliceName(SliceNameEnum.PAD);
      sliceSysFid.setCreateSlice(true);
      sliceSysFid.setFocusInstruction(fiSysFid);
      sliceSysFid.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
      fgSysFid.addSlice(sliceSysFid);
      rrSysFid.addFocusGroup(fgSysFid);
      reconstructionRegions.add(rrSysFid);
    }

    // Magnification Coupon Reconstruction Region
    if (_couponType == CouponTypeEnum.ALL ||
        _couponType == CouponTypeEnum.MAG)
    {
      ReconstructionRegion rrMagCoup = new ReconstructionRegion();
      rrMagCoup.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.VERIFICATION);
      rrMagCoup.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
      rrMagCoup.setTopSide(false);
      rrMagCoup.setRegionRectangleRelativeToPanelInNanoMeters(magnificationCouponArea);
      rrMagCoup.setFocusPriority(FocusPriorityEnum.FIRST);
      rrMagCoup.setTestSubProgram(tsp);
      FocusGroup fgMagCoup = new FocusGroup();
      fgMagCoup.setSingleSidedRegionOfBoard(true);
      fgMagCoup.setRelativeZoffsetFromSurfaceInNanoMeters(0);
      FocusSearchParameters fspMagCoup = new FocusSearchParameters();
      fspMagCoup.setFocusProfileShape(FocusProfileShapeEnum.PEAK);
      FocusRegion frMagCoup = new FocusRegion();
      frMagCoup.setRectangleRelativeToPanelInNanoMeters(magnificationCouponArea);
      fspMagCoup.setFocusRegion(frMagCoup);
      fgMagCoup.setFocusSearchParameters(fspMagCoup);
      FocusInstruction fiMagCoup = new FocusInstruction();
      fiMagCoup.setFocusMethod(FocusMethodEnum.SHARPEST);
      Slice sliceMagCoup = new Slice();
      sliceMagCoup.setSliceName(SliceNameEnum.PAD);
      sliceMagCoup.setCreateSlice(true);
      sliceMagCoup.setFocusInstruction(fiMagCoup);
      sliceMagCoup.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
      fgMagCoup.addSlice(sliceMagCoup);
      rrMagCoup.addFocusGroup(fgMagCoup);
      reconstructionRegions.add(rrMagCoup);
    }


    // Add verification regions to bypass auto-alignment
    tsp.setVerificationRegions(reconstructionRegions);
    tsp.setInspectionRegions(new ArrayList<ReconstructionRegion>());
    tsp.setAlignmentRegions(new ArrayList<ReconstructionRegion>());

    PanelRectangle enlargedAreaToImage = new PanelRectangle(areaToImage.getMinX() - MathUtil.convertMilsToNanoMetersInteger(_extraXBorderToImageMils),
                                                            areaToImage.getMinY() - MathUtil.convertMilsToNanoMetersInteger(_extraYBorderToImageMils),
                                                            areaToImage.getWidth()  + 2 * MathUtil.convertMilsToNanoMetersInteger(_extraXBorderToImageMils),
                                                            areaToImage.getHeight() + 2 * MathUtil.convertMilsToNanoMetersInteger(_extraYBorderToImageMils));

    tsp.setVerificationRegionsBoundsInNanoMeters(enlargedAreaToImage);
    tsp.setPanelLocationInSystem(PanelLocationInSystemEnum.RIGHT);

    tsp.setProcessorStripsForVerificationRegions(tsp.getFakeProcessorStrips());
    program.addTestSubProgram(tsp);

    return program;
  }

  /**
   * Creates a test program containing a set for verification regions.  Each
   * region of the board will contain a top side, bottom side, and deck of
   * images
   *
   * @author Dave Ferguson
   */
  private TestProgram createManualTiledTestProgram()
  {
    Project dummyProject = new Project(true);
    dummyProject.getProjectState().disable();
    dummyProject.getProjectState().enable();
    Panel dummyPanel = new Panel();
    dummyPanel.setProject(dummyProject);
    dummyPanel.setWidthInNanoMeters(25400);
    dummyPanel.setLengthInNanoMeters(25400);
    dummyPanel.setThicknessInNanoMeters(MathUtil.convertMilsToNanoMetersInteger(_panelThicknessMils));
    PanelSettings dummyPanelSettings = new PanelSettings();
    dummyPanelSettings.setPanel(dummyPanel);
    dummyPanelSettings.setDegreesRotationRelativeToCad(0);
    dummyPanel.setPanelSettings(dummyPanelSettings);
    AffineTransform transform = new AffineTransform();
    dummyPanelSettings.setRightManualAlignmentTransform(transform);
    dummyProject.setPanel(dummyPanel);

    // Create here so can set test sub-program of reconstruction regions below.
    TestProgram program = new TestProgram();
    program.setProject(dummyProject);
    TestSubProgram tsp = new TestSubProgram(program);

    Collection<ReconstructionRegion> reconstructionRegions = new ArrayList<ReconstructionRegion>();

    // Calculate the number of tiles we're going to reconstruct.
    // Truncate to fit within areaW & areaH bounds
    Assert.expect(_regionHMils <= _areaHMils);
    Assert.expect(_regionWMils <= _areaWMils);
    int numTileRows = _areaHMils / _regionHMils;
    int numTileColumns = _areaWMils / _regionWMils;

    int xPosOffsetMils = _areaXMils;
    int yPosOffsetMils = _areaYMils;
    int regionWidthMils = _regionWMils;
    int regionHeightMils = _regionHMils;

    PanelRectangle areaToImage = null;

    for (int rowTile = 0; rowTile < numTileRows; ++rowTile)
    {
      xPosOffsetMils = _areaXMils;
      for (int columnTile = 0; columnTile < numTileColumns; ++columnTile)
      {
        PanelRectangle smallRegionToReconstruct = new PanelRectangle(MathUtil.convertMilsToNanoMetersInteger(xPosOffsetMils),
                                                                     MathUtil.convertMilsToNanoMetersInteger(yPosOffsetMils),
                                                                     MathUtil.convertMilsToNanoMetersInteger(regionWidthMils),
                                                                     MathUtil.convertMilsToNanoMetersInteger(regionHeightMils));

        if (areaToImage == null)
        {
          areaToImage = new PanelRectangle(smallRegionToReconstruct.getMinX(),
                                           smallRegionToReconstruct.getMinY(),
                                           smallRegionToReconstruct.getWidth(),
                                           smallRegionToReconstruct.getHeight());
        }
        else
        {
          areaToImage.add(smallRegionToReconstruct);
        }

        // Bottom RR
        ReconstructionRegion rrBottom = new ReconstructionRegion();
        rrBottom.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.VERIFICATION);
        rrBottom.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
        rrBottom.setTopSide(false);
        rrBottom.setRegionRectangleRelativeToPanelInNanoMeters(smallRegionToReconstruct);
        rrBottom.setFocusPriority(FocusPriorityEnum.FIRST);
        rrBottom.setTestSubProgram(tsp);
        FocusGroup fgBottom = new FocusGroup();
        fgBottom.setSingleSidedRegionOfBoard(false);
        fgBottom.setRelativeZoffsetFromSurfaceInNanoMeters(0);
        FocusSearchParameters fspBottom = new FocusSearchParameters();
        fspBottom.setFocusProfileShape(FocusProfileShapeEnum.PEAK);
        FocusRegion frBottom = new FocusRegion();
        frBottom.setRectangleRelativeToPanelInNanoMeters(smallRegionToReconstruct);
        fspBottom.setFocusRegion(frBottom);
        fgBottom.setFocusSearchParameters(fspBottom);
        FocusInstruction fiBottom = new FocusInstruction();
        fiBottom.setFocusMethod(FocusMethodEnum.SHARPEST);
        Slice sliceBottom = new Slice();
        sliceBottom.setSliceName(SliceNameEnum.PAD);
        sliceBottom.setCreateSlice(true);
        sliceBottom.setFocusInstruction(fiBottom);
        sliceBottom.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
        fgBottom.addSlice(sliceBottom);
        rrBottom.addFocusGroup(fgBottom);
        reconstructionRegions.add(rrBottom);

        // Top RR
        ReconstructionRegion rrTop = new ReconstructionRegion();
        rrTop.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.VERIFICATION);
        rrTop.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
        rrTop.setTopSide(true);
        rrTop.setRegionRectangleRelativeToPanelInNanoMeters(smallRegionToReconstruct);
        rrTop.setFocusPriority(FocusPriorityEnum.FIRST);
        rrTop.setTestSubProgram(tsp);
        FocusGroup fgTop = new FocusGroup();
        fgTop.setSingleSidedRegionOfBoard(false);
        fgTop.setRelativeZoffsetFromSurfaceInNanoMeters(0);
        FocusSearchParameters fspTop = new FocusSearchParameters();
        fspTop.setFocusProfileShape(FocusProfileShapeEnum.PEAK);
        FocusRegion frTop = new FocusRegion();
        frTop.setRectangleRelativeToPanelInNanoMeters(smallRegionToReconstruct);
        fspTop.setFocusRegion(frTop);
        fgTop.setFocusSearchParameters(fspTop);
        FocusInstruction fiTop = new FocusInstruction();
        fiTop.setFocusMethod(FocusMethodEnum.SHARPEST);
        Slice sliceTop = new Slice();
        sliceTop.setSliceName(SliceNameEnum.PAD);
        sliceTop.setCreateSlice(true);
        sliceTop.setFocusInstruction(fiTop);
        sliceTop.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
        fgTop.addSlice(sliceTop);
        rrTop.addFocusGroup(fgTop);
        reconstructionRegions.add(rrTop);

        // Slice deck
        if (_createDeck)
        {
          int numDeckSlices = _numSlicesInDeck;
          int milsPerSliceDeck = _zShiftInMils;
          for (int i = 0; i < numDeckSlices; ++i)
          {
            ReconstructionRegion rrDeck = new ReconstructionRegion();
            rrDeck.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.VERIFICATION);
            rrDeck.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
            rrDeck.setTopSide(false);
            rrDeck.setRegionRectangleRelativeToPanelInNanoMeters(smallRegionToReconstruct);
            rrDeck.setFocusPriority(FocusPriorityEnum.FIRST);
            rrDeck.setTestSubProgram(tsp);

            FocusInstruction fi = new FocusInstruction();

            fi.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);

            fi.setZHeightInNanoMeters(MathUtil.convertMilsToNanoMetersInteger((i - numDeckSlices / 2) * milsPerSliceDeck +
                _zOffsetOfDeckMils));

            Slice slice = new Slice();
            slice.setSliceName(SliceNameEnum.PAD);
            slice.setCreateSlice(true);
            slice.setFocusInstruction(fi);
            slice.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);

            // Create a focus group with "No Search" to bypass autofocus
            FocusGroup fgDeck = new FocusGroup();
            fgDeck.setSingleSidedRegionOfBoard(false);
            fgDeck.setRelativeZoffsetFromSurfaceInNanoMeters(0);
            FocusSearchParameters fspDeck = new FocusSearchParameters();
            fspDeck.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
            FocusRegion frDeck = new FocusRegion();
            frDeck.setRectangleRelativeToPanelInNanoMeters(smallRegionToReconstruct);
            fspDeck.setFocusRegion(frDeck);
            fgDeck.setFocusSearchParameters(fspDeck);

            fgDeck.addSlice(slice);

            rrDeck.addFocusGroup(fgDeck);
            reconstructionRegions.add(rrDeck);
          }
        }
        // Shift region to the right
        xPosOffsetMils += regionWidthMils;
      }

      // Shift the region down
      yPosOffsetMils += regionHeightMils;
    }

    // Add verification regions to bypass auto-alignment
    tsp.setVerificationRegions(reconstructionRegions);
    tsp.setInspectionRegions(new ArrayList<ReconstructionRegion>());
    tsp.setAlignmentRegions(new ArrayList<ReconstructionRegion>());

    PanelRectangle enlargedAreaToImage = new PanelRectangle(areaToImage.getMinX() - MathUtil.convertMilsToNanoMetersInteger(_extraXBorderToImageMils),
                                                            areaToImage.getMinY() - MathUtil.convertMilsToNanoMetersInteger(_extraYBorderToImageMils),
                                                            areaToImage.getWidth()  + 2 * MathUtil.convertMilsToNanoMetersInteger(_extraXBorderToImageMils),
                                                            areaToImage.getHeight() + 2 * MathUtil.convertMilsToNanoMetersInteger(_extraYBorderToImageMils));


    tsp.setVerificationRegionsBoundsInNanoMeters(enlargedAreaToImage);
    tsp.setPanelLocationInSystem(PanelLocationInSystemEnum.RIGHT);

    tsp.setProcessorStripsForVerificationRegions(tsp.getFakeProcessorStrips());
    program.addTestSubProgram(tsp);

    return program;
  }
}
