package com.axi.guiUtil;


import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.util.*;

/**
 * Creates a modal dialog to capture a user entered (via keyboard) number as a double
 * @author Andy Mechtenberg
 */
public class ChoiceInputDialog extends JDialog
{
  private int _returnValue; // either OK or CANCEL depending on how the user exits
  private Object _selectedValue = null;
  private Object[] _selectedValues;
  private String _message;
  private String _okButtonText;
  private String _cancelButtonText;
  private boolean _allowMultipleSelection = false;

  private Font _buttonFont;
  private Font _messageFont;

  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private JPanel _messagePanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private FlowLayout _buttonPanelFlowLayout = new FlowLayout();
  private JPanel _innerButtonPanel = new JPanel();
  private GridLayout _innerButtonPanelGridLayout = new GridLayout();
  private JButton _okButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }

  };
  private JButton _cancelButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };
  private FlowLayout _messagePanelFlowLayout = new FlowLayout();
  private JLabel _messageLabel = new JLabel();
  private JPanel _inputPanel = new JPanel();
  private DefaultListModel _choicesListModel = new DefaultListModel();
  private JList _choicesList = new JList(_choicesListModel)
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };
  private JScrollPane _choicesScrollPane = new JScrollPane();
  private BorderLayout _inputPanelBorderLayout = new BorderLayout();
  private Border _innerButtonPanelBorder = BorderFactory.createEmptyBorder(5, 10, 0, 10);
  private Border _mainPanelBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);

  /**
   * For use with the jBuilder visual designer only
   * @author Andy Mechtenberg
   */
  private ChoiceInputDialog()
  {
    _okButtonText = "OK";
    _cancelButtonText = "Cancel";
    _message = "Please Select:";
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public ChoiceInputDialog(Frame frame,
                           String title,
                           String message,
                           String okButtonText,
                           String cancelButtonText,
                           Object[] possibleValues)
  {
    super(frame, title, true);

    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(message != null);
    Assert.expect(okButtonText != null);
    Assert.expect(cancelButtonText != null);
    Assert.expect(possibleValues != null);

    _message = message;
    _okButtonText = okButtonText;
    _cancelButtonText = cancelButtonText;
    _buttonFont = FontUtil.getDialogButtonFont();
    _messageFont = FontUtil.getDialogMessageFont();

    _okButton.setEnabled(possibleValues.length > 0);

    for (int i = 0; i < possibleValues.length; i++)
      _choicesListModel.addElement(possibleValues[i]);

    _choicesList.setSelectedIndex(0);

    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public ChoiceInputDialog(Frame frame,
                           String title,
                           String message,
                           String okButtonText,
                           String cancelButtonText,
                           Object[] possibleValues,
                           boolean allowMultipleSelection)
  {
    super(frame, title, true);

    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(message != null);
    Assert.expect(okButtonText != null);
    Assert.expect(cancelButtonText != null);
    Assert.expect(possibleValues != null);

    _message = message;
    _okButtonText = okButtonText;
    _cancelButtonText = cancelButtonText;
    _buttonFont = FontUtil.getDialogButtonFont();
    _messageFont = FontUtil.getDialogMessageFont();
    _allowMultipleSelection = allowMultipleSelection;

    _okButton.setEnabled(possibleValues.length > 0);

    for (int i = 0; i < possibleValues.length; i++)
      _choicesListModel.addElement(possibleValues[i]);

    _choicesList.setSelectedIndex(0);

    try
    {
      jbInit();
      pack();
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * @throws Exception
   * @author Andy Mechtenberg
   */
  private void jbInit() throws Exception
  {
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _cancelButton.setText(_cancelButtonText);
    _innerButtonPanelGridLayout.setHgap(5);
    _messagePanel.setLayout(_messagePanelFlowLayout);
    _messagePanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _messagePanelFlowLayout.setHgap(0);
    _messagePanelFlowLayout.setVgap(0);
    _cancelButton.setFont(_buttonFont);
    _okButton.setFont(_buttonFont);
    _cancelButton.setMargin(new Insets(2, 8, 2, 8));
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _okButton.setMargin(new Insets(2, 8, 2, 8));
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });

    _messageLabel.setFont(_messageFont);
    _messageLabel.setText(_message);
    _inputPanel.setLayout(_inputPanelBorderLayout);
    _buttonPanelFlowLayout.setHgap(0);
    _buttonPanelFlowLayout.setVgap(0);
    _innerButtonPanel.setBorder(_innerButtonPanelBorder);
    _mainPanel.setBorder(_mainPanelBorder);
    _mainPanelBorderLayout.setVgap(5);
    this.getContentPane().add(_mainPanel);
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    _buttonPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _innerButtonPanel.setLayout(_innerButtonPanelGridLayout);
    _okButton.setText(_okButtonText);
    _mainPanel.add(_buttonPanel, java.awt.BorderLayout.SOUTH);
    _buttonPanel.add(_innerButtonPanel);
    _innerButtonPanel.add(_okButton);
    _innerButtonPanel.add(_cancelButton);
    _messagePanel.add(_messageLabel);
    _mainPanel.add(_inputPanel, java.awt.BorderLayout.CENTER);
    _mainPanel.add(_messagePanel, java.awt.BorderLayout.NORTH);
    _choicesScrollPane.getViewport().add(_choicesList);
    _inputPanel.add(_choicesScrollPane, java.awt.BorderLayout.CENTER);
    _choicesList.setVisibleRowCount(10);

    if (_allowMultipleSelection)
      _choicesList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    else
      _choicesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    _choicesList.addMouseListener(new MouseAdapter()
    {
      public void mousePressed(MouseEvent e)
      {
        // if the user double clicks on a entry, automatically select it and exit the dialog
        if (e.getClickCount() == 2)
        {
          int index = _choicesList.locationToIndex(e.getPoint());
          _selectedValue = _choicesList.getModel().getElementAt(index);
          _returnValue = JOptionPane.OK_OPTION;
          dispose();
        }
      }
    });
    _choicesList.getSelectionModel().addListSelectionListener(new ListSelectionListener()
      {
        // this ensures that a selected item is visible.  At jdk1.5, this is the default behavior, so this could be removed.
        public void valueChanged(ListSelectionEvent e)
        {
          if (e.getValueIsAdjusting() == false)
          {
            int index = _choicesList.getSelectedIndex();
            _choicesList.ensureIndexIsVisible(index);
          }
        }
      });
    getRootPane().setDefaultButton(_okButton);
  }

  /**
   * @return int
   * @author Andy Mechtenberg
   */
  public int showDialog()
  {
    setVisible(true);
    return _returnValue;
  }

  /**
   * @param font Font
   * @return int
   * @author Andy Mechtenberg
   */
  public int showDialog(Font font)
  {
    Assert.expect(font != null);
    SwingUtils.setFont(this, font);
    pack();
    setVisible(true);
    return _returnValue;
  }

  /**
   * @param e WindowEvent
   * @author Andy Mechtenberg
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    int eventId = e.getID();
    if (eventId == WindowEvent.WINDOW_CLOSING)
    {
      cancelAction();
    }
  }

  /**
   * @return Object
   * @author Andy Mechtenberg
   */
  public Object getSelectedValue()
  {
    Assert.expect(_selectedValue != null);
    return _selectedValue;
  }

  /**
   * @return Object
   * @author Andy Mechtenberg
   */
  public Object[] getSelectedValues()
  {
    Assert.expect(_selectedValues != null);
    return _selectedValues;
  }

  /**
   * @param e WindowEvent
   * @author Andy Mechtenberg
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    _selectedValue = _choicesList.getSelectedValue();
    _selectedValues = _choicesList.getSelectedValues();
    // ok, this is weird, but if you hit CTRL-\, then NO VALUE is selected in the list
    // _selectedValue will be null.  Pretend nothing happened.
    if (_selectedValue == null)
      return;
    _returnValue = JOptionPane.OK_OPTION;
    dispose();
  }

  /**
   * @param e WindowEvent
   * @author Andy Mechtenberg
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    cancelAction();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void cancelAction()
  {
    _returnValue = JOptionPane.CANCEL_OPTION;
    _selectedValue = null;
    dispose();
  }

  /**
   * @param parentComponent Component
   * @param message String
   * @param title String
   * @param options Object[]
   * @param initialOption Object
   * @param font Font
   * @return int
   * @author Andy Mechtenberg
   */
  public static int showOptionDialog(Component parentComponent, String message, String title, Object[] options, Object initialOption, Font font )
  {
    Assert.expect(parentComponent != null);
    Assert.expect(message != null);
    Assert.expect(title != null);
    Assert.expect(options != null);
    Assert.expect(initialOption != null);
    Assert.expect(font != null);

    int optionType = 0;
    if (options.length == 2)
      optionType = JOptionPane.YES_NO_OPTION;
    else if (options.length == 3)
      optionType = JOptionPane.YES_NO_CANCEL_OPTION;
    else
      Assert.expect(false, "Options not set" + options);

    JOptionPane pane = new JOptionPane(message,
                                       JOptionPane.QUESTION_MESSAGE,
                                       optionType,
                                       null,
                                       options,
                                       initialOption);
    JDialog dialog = pane.createDialog(parentComponent, title);
    dialog.setModal(true);
    SwingUtils.setFont(dialog, font);
    dialog.pack();
    SwingUtils.centerOnComponent(dialog, parentComponent);
    dialog.toFront();
    pane.selectInitialValue();
    dialog.setVisible(true);
    Object choiceObj = pane.getValue();
    return getChoice(choiceObj, options);
  }

  /**
   * @param parentComponent Component
   * @param message String
   * @param title String
   * @return int
   * @author Andy Mechtenberg
   */
  public static int showConfirmDialog(Component parentComponent, String message, String title)
  {
    Assert.expect(parentComponent != null);
    Assert.expect(message != null);
    Assert.expect(title != null);

    JOptionPane pane = new JOptionPane(message,
                                       JOptionPane.QUESTION_MESSAGE,
                                       JOptionPane.YES_NO_OPTION);

    JDialog dialog = pane.createDialog(parentComponent, title);
    dialog.setModal(true);
    dialog.pack();
    dialog.toFront();
    dialog.setVisible(true);
    dialog.dispose();
    Object choiceObj = pane.getValue();
    return getChoice(choiceObj, null);
  }

  /**
   * @param parentComponent Component
   * @param message String
   * @param title String
   * @param font Font
   * @return int
   * @author Andy Mechtenberg
   */
  public static int showConfirmDialog(Component parentComponent, String message, String title, Font font)
  {
    Assert.expect(parentComponent != null);
    Assert.expect(message != null);
    Assert.expect(title != null);
    Assert.expect(font != null);

    JOptionPane pane = new JOptionPane(message,
                                       JOptionPane.QUESTION_MESSAGE,
                                       JOptionPane.YES_NO_OPTION);

    JDialog dialog = pane.createDialog(parentComponent, title);
    dialog.setModal(true);
    SwingUtils.setFont(dialog, FontUtil.getMediumFont());
    dialog.pack();
    dialog.toFront();
    dialog.setVisible(true);
    dialog.dispose();
    Object choiceObj = pane.getValue();
    return getChoice(choiceObj, null);
  }

  static int getChoice(Object choiceObj, Object[] options)
  {
    // default to CLOSED if window close used
    int choice = JOptionPane.CLOSED_OPTION;
    if (choiceObj instanceof String)
    {
      Assert.expect(options != null);
      String choiceStr = (String)choiceObj;
      if (choiceStr.equalsIgnoreCase((String)options[0]))
        choice = JOptionPane.YES_OPTION;
      else if (choiceStr.equalsIgnoreCase((String)options[1]))
        choice = JOptionPane.NO_OPTION;
      else
        choice = JOptionPane.CANCEL_OPTION;
    }
    else if (choiceObj != null)
    {
      choice = ((Integer)choiceObj).intValue();
    }
    return choice;
  }

}
