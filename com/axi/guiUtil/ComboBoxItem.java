package com.axi.guiUtil;

import com.axi.util.*;
import java.util.*;

/**
 *
 * @author swee-yee.wong
 */
public class ComboBoxItem
{
  private String _name = null;
  private String _selected = null;
  private String _message = null;
  private List<String> _dropDownList = new ArrayList<String>();
  
  public ComboBoxItem(String name, String selected, List<String> dropDownList, String message)
  {
    Assert.expect(name != null);
    Assert.expect(selected != null);
    Assert.expect(dropDownList != null);
    Assert.expect(dropDownList.contains(selected));

    _name = name;
    _selected = selected;
    _message = message;
    _dropDownList = dropDownList;
  }

  public void setName(String name)
  {
    Assert.expect(name != null);
    _name = name;
  }

  public String getName()
  {
    Assert.expect(_name != null);
    return _name;
  }

  public void setSelected(String selected)
  {
    Assert.expect(selected != null);
    _selected = selected;
  }

  public String getSelected()
  {
    Assert.expect(_selected != null);
    return _selected;
  }

  public void setMessage(String message)
  {
    Assert.expect(message != null);
    _message = message;
  }

  public String getMessage()
  {
    return _message;
  }
  
  public void setDropDownList(List<String> dropDownList)
  {
    Assert.expect(dropDownList != null);
    _dropDownList = dropDownList;
  }

  public List<String> getDropDownList()
  {
    Assert.expect(_dropDownList != null);
    return _dropDownList;
  }
}
