package com.axi.guiUtil;

import java.util.*;

import com.axi.util.*;

/**
 * @author Kee Chin Seong
 */
public class ShowNoLoadComponentObservable extends Observable 
{
    /**
     * @author Kee Chin Seong
     */
    ShowNoLoadComponentObservable()
    {
        // do nothing
    }

    /**
     * @author Kee Chin Seong
     */
    public void setNoLoadComponentVisible(boolean setNoLoadComponentVisible)
    {
        setChanged();
        notifyObservers(new Boolean(setNoLoadComponentVisible));
    }
}