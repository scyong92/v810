package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import com.axi.util.*;


/**
 *
 * <p>Title: CheckCellEditor</p>
 * <p>Description: A Table Cell Editor for a check box (boolean value).
 * The check box is centered in the cell, and it allows the user to set
 * the foreground and background colors.</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company:  Agilent Technogies</p>
 * @author Carli Connally
 * @version 1.0
 */
public class CheckCellEditor extends DefaultCellEditor
{
  private JCheckBox _checkBox = null;

  /**
   * @author Carli Connally
   * Allows user to set the foreground and background colors.
   * @param backgroundColor Foreground color of the cell (usually white)
   * @param foregroundColor  Background color of the cell (usually white)
   */
  public CheckCellEditor( Color backgroundColor, Color foregroundColor )
  {
    super(new JCheckBox());
    Assert.expect(backgroundColor != null);
    Assert.expect(foregroundColor != null);

    _checkBox = (JCheckBox)getComponent();

    _checkBox.setHorizontalAlignment( (int)JCheckBox.CENTER_ALIGNMENT );

    _checkBox.setOpaque(true);
    _checkBox.setBackground( backgroundColor );
    _checkBox.setForeground( foregroundColor );

    final DefaultCellEditor thisEditor = this;
    _checkBox.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent event)
      {
        thisEditor.stopCellEditing();
      }
    });
  }
}
