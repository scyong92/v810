package com.axi.guiUtil;

import java.text.*;
import java.awt.event.*;
import javax.swing.text.*;
import javax.swing.*;

/**
 * Title: NumericTextField
 * Description: This class crates a text field that will only accept numeric input.
 * It uses the DecimalFormat class to control how to parse and display the value.
 * The class can be localized by using the local version of DecimalFormat.
 *
 * The original version of this class was found in the book:
 * Core Swing: advanced programming by Kim Topley
 *
 * @author Steve Anonson
 */
public class NumericTextField extends JTextField implements NumericPlainDocument.InsertErrorListener
{
  /**
   * @author Steve Anonson
   */
  public NumericTextField()
  {
    this(null, 0, null);
  }

  /**
   * @param   text - initial text for the text field.
   * @param   columns - width in colums of the text field.
   * @param   format - DecimalFormat to use when parsing or displaying the text field value.
   * @author Steve Anonson
   */
  public NumericTextField(String text, int columns, DecimalFormat format)
  {
    super(null, text, columns);
    NumericPlainDocument numericDoc = (NumericPlainDocument)getDocument();
    if (format != null)
    {
      numericDoc.setFormat(format);
    }
    numericDoc.addInsertErrorListener(this);
  }

  /**
   * @param   columns - width in colums of the text field.
   * @param   format - DecimalFormat to use when parsing or displaying the text field value.
   * @author Steve Anonson
   */
  public NumericTextField(int columns, DecimalFormat format)
  {
    this(null, columns, format);
  }

  /**
   * @param   text - initial text for the text field.
   * @author Steve Anonson
   */
  public NumericTextField(String text)
  {
    this(text, 0, null);
  }

  /**
   * @param   text - initial text for the text field.
   * @param   columns - width in colums of the text field.
   * @author Steve Anonson
   */
  public NumericTextField(String text, int columns)
  {
    this(text, columns, null);
  }

  /**
   * Set the format used to parse and display the text field value.
   *
   * @param   format - DecimalFormat to use when parsing or displaying the text field value.
   * @author Steve Anonson
   */
  public void setFormat(DecimalFormat format)
  {
    ((NumericPlainDocument)getDocument()).setFormat(format);
  }

  /**
   * @return the format used to parse and display the text field value.
   * @author Steve Anonson
   */
  public DecimalFormat getFormat()
  {
    return ((NumericPlainDocument)getDocument()).getFormat();
  }

  /**
   * @author Steve Anonson
   */
  public void formatChanged()
  {
    // Notify change of format attributes.
    setFormat(getFormat());
  }

  /**
   * @return the Long equivalent of the text field value.
   * @author Steve Anonson
   */
  public Long getLongValue() throws ParseException
  {
    return ((NumericPlainDocument)getDocument()).getLongValue();
  }

  /**
   * @return the Double equivalent of the text field value.
   * @author Steve Anonson
   */
  public Double getDoubleValue() throws ParseException
  {
    return ((NumericPlainDocument)getDocument()).getDoubleValue();
  }

  /**
   * @return the Number equivalent of the text field value.
   * @author Steve Anonson
   */
  public Number getNumberValue() throws ParseException
  {
    return ((NumericPlainDocument)getDocument()).getNumberValue();
  }

  /**
   * Set the value of the text field.
   *
   * @param   number - Number value.
   * @author Steve Anonson
   */
  public void setValue(Number number)
  {
    setText(getFormat().format(number));
  }

  /**
   * Set the value of the text field.
   *
   * @param   l - long value.
   * @author Steve Anonson
   */
  public void setValue(long l)
  {
    setText(getFormat().format(l));;
  }

  /**
   * Set the value of the text field.
   *
   * @param   d - double value.
   * @author Steve Anonson
   */
  public void setValue(double d)
  {
    String t = getFormat().format(d);
    setText(t);
  }

  /**
   * @author Steve Anonson
   */
  public void normalize() throws ParseException
  {
    // format the value according to the format string
    setText(getFormat().format(getNumberValue()));
  }

  /**
   * InsertErrorListener method.  Override to handle insertion error.
   * @author Steve Anonson
   */
  public void insertFailed(NumericPlainDocument doc, int offset, String str, AttributeSet a)
  {
    // By default, just beep
    java.awt.Toolkit.getDefaultToolkit().beep();
  }

  /**
   * Create the default model.
   * @author Steve Anonson
   */
  protected Document createDefaultModel()
  {
    return new NumericPlainDocument();
  }
}
