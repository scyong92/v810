package com.axi.guiUtil;

import java.awt.*;
import java.util.*;
import javax.swing.*;

import com.axi.util.*;

/**
 * This class provides all the standard fonts that any application for the 5dx
 * should use.  This will make it easy for us to globally change our fonts if
 * we ever need to in the future.  It also will save memory because we will
 * share the same font objects instead of creating many objects that are identical.
 *
 * @author Bill Darbie
 */
public class FontUtil
{
  // use the UIManager to get the font, as this is dependent on the look and feel
  private static final Font _smallFont = UIManager.getFont("OptionPane.messageFont");
  private static final Font _mediumFont = new Font(_smallFont.getName(), Font.PLAIN, 16);
  private static final Font _largeFont = new Font(_smallFont.getName(), Font.PLAIN, 20);
  private static final Font _extraLargeFont = new Font(_smallFont.getName(), Font.PLAIN, 32);
  private static final Font _defaultFont = _smallFont;

  /**
   * @author Andy Mechtenberg
   */
  public static Font getBoldFont(Font baseFont, Locale locale)
  {
    Assert.expect(baseFont != null);
    Assert.expect(locale != null);

    if(locale.equals(Locale.ENGLISH) == false)
      return baseFont;

    return new Font(baseFont.getName(), Font.BOLD, baseFont.getSize());
  }

  /**
   * Creates a new font 4 pixels bigger than the base font
   * @author Andy Mechtenberg
   */
  public static Font getBiggerFont(Font baseFont,Locale locale)
  {
    Assert.expect(baseFont != null);
    
    return FontUtil.getSizedFont(baseFont, baseFont.getSize() + 4,locale);
  }

  /**
   * Creates a new font of the passed in size
   * @author Andy Mechtenberg
   */
  public static Font getSizedFont(Font baseFont, int size,Locale locale)
  {
    Assert.expect(baseFont != null);
    Assert.expect(locale != null);

    if(locale.equals(Locale.ENGLISH) == false)
      return baseFont;

    return new Font(baseFont.getName(), Font.PLAIN, size);
  }

  /**
   * @author Bill Darbie
   */
  public static Font getSmallFont()
  {
    return _smallFont;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static Font getSmallFont(int fontStyle)
  {
    return new Font(_smallFont.getName(), fontStyle, _smallFont.getSize());
  }

  /**
   * @author Kristin Casterton
   */
  public static Font getMediumFont()
  {
    return _mediumFont;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static Font getMediumFont(int fontStyle)
  {
    return new Font(_mediumFont.getName(), fontStyle, _mediumFont.getSize());
  }

  /**
   * @author Bill Darbie
   */
  public static Font getLargeFont()
  {
    return _largeFont;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static Font getLargeFont(int fontStyle)
  {
    return new Font(_largeFont.getName(), fontStyle, _largeFont.getSize());
  }

  /**
   * @author Bill Darbie
   */
  public static Font getExtraLargeFont()
  {
    return _extraLargeFont;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static Font getExtraLargeFont(int fontStyle)
  {
    return new Font(_largeFont.getName(), fontStyle, _extraLargeFont.getSize());
  }

  /**
   * @author Bill Darbie
   */
  public static Font getDefaultFont()
  {
    return _defaultFont;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static Font getDialogMessageFont()
  {
    return UIManager.getFont("OptionPane.messageFont");
  }

  /**
   * @author Andy Mechtenberg
   */
  public static Font getDialogButtonFont()
  {
    return UIManager.getFont("OptionPane.buttonFont");
  }

  /**
   * @author Andy Mechtenberg
   */
  public static Font getRendererFont(int size)
  {
    return new Font("Sansserif", Font.BOLD, size);
  }

  /**
   * @author Andy Mechtenberg
   */
  public static Font getServiceLegendFont()
  {
    return new Font("Dialog", 0, 10);
  }

}
