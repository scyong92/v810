package com.axi.guiUtil;

import java.awt.*;

import com.axi.util.*;

/**
 * Standard colors for layers on the GraphicsEngine class.
 *
 * @see GraphicsEngine
 *
 * @author Bill Darbie
 */
public class LayerColorEnum extends com.axi.util.Enum
{
  private static int _index = -1;
   
  public static LayerColorEnum MOUSE_EVENTS_COLOR = new LayerColorEnum(++_index, Color.WHITE);
  public static LayerColorEnum SURFACEMOUNT_PAD_COLOR = new LayerColorEnum(++_index, Color.GRAY);
  public static LayerColorEnum THROUGHHOLE_PAD_COLOR = new LayerColorEnum(++_index, Color.GRAY);
  public static LayerColorEnum FIRST_SURFACEMOUNT_PAD_COLOR = new LayerColorEnum(++_index, new Color(255,105,105));  // a pastel red
  public static LayerColorEnum FIRST_THROUGHHOLE_PAD_COLOR = new LayerColorEnum(++_index, new Color(255,105,105)); // was new LayerColorEnum(++_index, Color.orange);
  public static LayerColorEnum REFERENCE_DESIGNATOR_COLOR = new LayerColorEnum(++_index, Color.RED);
  public static LayerColorEnum TOP_COMPONENT_COLOR = new LayerColorEnum(++_index, Color.CYAN);
  public static LayerColorEnum BOTTOM_COMPONENT_COLOR = new LayerColorEnum(++_index, Color.MAGENTA);
  public static LayerColorEnum BOARD_COLOR = new LayerColorEnum(++_index, Color.LIGHT_GRAY);
  public static LayerColorEnum PANEL_COLOR = new LayerColorEnum(++_index, Color.PINK);
  public static LayerColorEnum FIDUCIAL_COLOR = new LayerColorEnum(++_index, Color.GREEN);
  public static LayerColorEnum BOARD_ORIGIN_COLOR = new LayerColorEnum(++_index, new Color(255, 255, 93));// pale yellow
  public static LayerColorEnum BOARD_NAME_COLOR = new LayerColorEnum(++_index, Color.WHITE);

  public static LayerColorEnum ALIGNMENT_GROUP_1_COLOR = new LayerColorEnum(++_index, new Color(255, 233, 85)); // yellow
  public static LayerColorEnum ALIGNMENT_GROUP_2_COLOR = new LayerColorEnum(++_index, new Color(35, 210, 0));  // green
  public static LayerColorEnum ALIGNMENT_GROUP_3_COLOR = new LayerColorEnum(++_index, new Color(255,128,0));// orange   was a pastel cyan (old color) new Color(0, 183, 200));
  public static LayerColorEnum ALIGNMENT_GROUP_INVALID_COLOR = new LayerColorEnum(++_index, new Color(255,0,0));// red

  // debug graphics colors
  public static LayerColorEnum IRP_BOUNDARY_COLOR = new LayerColorEnum(++_index, Color.ORANGE);
  public static LayerColorEnum RECONSTRUCTION_REGION_RECTANGLE_COLOR = new LayerColorEnum(++_index, Color.YELLOW);
  public static LayerColorEnum FOCUS_REGION_COLOR = new LayerColorEnum(++_index, Color.BLUE);
  public static LayerColorEnum REGION_PAD_BOUNDS_COLOR = new LayerColorEnum(++_index, Color.red.brighter().brighter());
  public static LayerColorEnum PITCH_COLOR = new LayerColorEnum(++_index, Color.YELLOW);
  public static LayerColorEnum IPD_COLOR = new LayerColorEnum(++_index, new Color(145, 255, 145));  // inter pad distance (IPD) (Greenish)

  public static LayerColorEnum ORIENTATION_ARROW_COLOR = new LayerColorEnum(++_index, new Color(145, 255, 145)); //(Greenish)

  public static LayerColorEnum CROSSHAIR_COLOR = new LayerColorEnum(++_index, Color.YELLOW);

  // profile colors
  public static LayerColorEnum PROFILE_BACKGROUND = new LayerColorEnum(++_index, Color.black);
  public static LayerColorEnum PROFILE_AXIS = new LayerColorEnum(++_index, new Color(128, 64, 0)); // brown-reddish
  public static LayerColorEnum PROFILE_TEXT = new LayerColorEnum(++_index, new Color(128, 64, 0)); // brown-reddish

  public static LayerColorEnum BACKGROUND_PIXELS = new LayerColorEnum(++_index, Color.WHITE);
  public static LayerColorEnum SOLDER_AREA_PIXELS = new LayerColorEnum(++_index, Color.ORANGE);
  public static LayerColorEnum EXPECTED_IMAGE_PIXELS = new LayerColorEnum(++_index, Color.CYAN);
  public static LayerColorEnum VOIDING_PIXELS = new LayerColorEnum(++_index, Color.YELLOW);
  public static LayerColorEnum LARGEST_VOIDING_PIXELS = new LayerColorEnum(++_index, new Color(255,128,0));

  //for alignment image panel
  public static LayerColorEnum ALIGNMENT_PAD_COLOR = new LayerColorEnum(++_index, Color.blue);
  public static LayerColorEnum LOCATED_ALIGNMENT_PAD_COLOR = new LayerColorEnum(++_index, Color.ORANGE);
  public static LayerColorEnum LOCATED_ALIGNMENT_CONTEXT_PAD_COLOR = new LayerColorEnum(++_index, Color.BLUE);

  //for verification image panel
  public static LayerColorEnum VERIFY_CAD_PAD_COLOR = new LayerColorEnum(++_index, Color.blue);
  public static LayerColorEnum VERIFY_CAD_SELECTED_PAD_COLOR = new LayerColorEnum(++_index, Color.orange);
  public static LayerColorEnum PAD_NAME_COLOR = new LayerColorEnum(++_index, Color.red);
  public static LayerColorEnum VERIFY_CAD_ORIENTATION_ARROW_COLOR = new LayerColorEnum(++_index, Color.yellow);
   
  // virtualLive
  public static LayerColorEnum VIRTUALLIVE_AREA_BORDER_COLOR = new LayerColorEnum(++_index, Color.BLUE);
  
  //diagnostic slice image - grayscale
  public static LayerColorEnum DIAGNOSTIC_LARGEST_VOIDING_PIXELS = new LayerColorEnum(++_index, Color.GRAY); //diagnostic slice largest void color
 
  //set noload component layer in RED
  public static LayerColorEnum SAVE_NO_LOAD_COMPONENT_COLOR = new LayerColorEnum(++_index, Color.red);
          
  //Create new CAD color
  public static LayerColorEnum NEW_CAD_BOARD_COLOR = new LayerColorEnum(++_index, Color.RED);
  
  //Untestable area color on CAD
  public static LayerColorEnum UNTESTABLE_AREA_COLOR = new LayerColorEnum(++_index, new Color(128, 128, 128, 200)); 
  
  private Color _color;

  /**
   * @author Bill Darbie
   */
  private LayerColorEnum(int index, Color color)
  {
    super(index);
    Assert.expect(color != null);
    _color = color;
  }

  /**
   * @author Bill Darbie
   */
  public Color getColor()
  {
    return _color;
  }
}
