package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class MultiSelectPanel extends JPanel
{
  private JLabel _label = new JLabel("(All)");

  protected JButton _button;
  /**
   * @author Andy Mechtenberg
   */
  public MultiSelectPanel()
  {
    setLayout(new BorderLayout());
    _label.setBackground(Color.white);
    _label.setOpaque(true);
    add(_label, BorderLayout.CENTER);
    _button = new JButton("...");
    _button.setPreferredSize(new Dimension(15,15));
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setText(String text)
  {
    _label.setText(text);
  }

  /**
   * @author Wei Chin, Chong
   */
  public String getText()
  {
    return _label.getText();
  }


  /**
   * @author Andy Mechtenberg
   */
  public void addButton()
  {
    add(_button, BorderLayout.EAST);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void removeButton()
  {
    remove(_button);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setMultiSelectBackgroundColor(Color color)
  {
    _label.setBackground(color);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setMultiSelectForegroundColor(Color color)
  {
    _label.setForeground(color);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void addButtonActionListener(ActionListener actionListener)
  {
    _button.addActionListener(actionListener);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void removeAllButtonActionListener()
  {
    for( ActionListener actionListener : _button.getActionListeners())
      _button.removeActionListener(actionListener);
  }
}
