package com.axi.guiUtil;

import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.basic.*;
import javax.swing.plaf.synth.*;

import com.axi.util.*;

/**
* This class is a subclass of JComboBox, and is used for changing the width of
* the popup drop down list.  Normally, the width is the same as the combobox itself,
* but sometimes a wider list is needed.
*
* @author Andy Mechtenberg
*/
public class VariableWidthComboBox extends ListPopulatedComboBox
{
  private int _popupWidth;
  public static final int widthExtension = 3 + 20;  // 3 so it's not quite at the edge, and 20 to account for a scroll bar

  /**
   * @author Andy Mechtenberg
   */
  public VariableWidthComboBox()
  {
    super();
    if (UIManager.getLookAndFeel().getDescription().startsWith("Nimbus"))
    {
      setUI(new VariableWidthComboBoxUI());
    }
    _popupWidth = 0;
  }

  /**
   * @author Andy Mechtenberg
   */
  public VariableWidthComboBox(ComboBoxModel aModel)
  {
    super(aModel);
    if (UIManager.getLookAndFeel().getDescription().startsWith("Nimbus"))
    {
      setUI(new VariableWidthComboBoxUI());
    }
    _popupWidth = 0;
  }

  /**
   * @author Andy Mechtenberg
   */
  public VariableWidthComboBox(final Object[] items)
  {
    super(items);
    if (UIManager.getLookAndFeel().getDescription().startsWith("Nimbus"))
    {
      setUI(new VariableWidthComboBoxUI());
    }
    _popupWidth = 0;
  }

  /**
   * @author Erica Wheatcroft
   */
  public VariableWidthComboBox(java.util.List<?> items)
  {
    super(items);
    if (UIManager.getLookAndFeel().getDescription().startsWith("Nimbus"))
    {
      setUI(new VariableWidthComboBoxUI());
    }
    _popupWidth = 0;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setPopupWidth(int width)
  {
    Assert.expect(width > 0);
    _popupWidth = width;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setPopupWidthToDefault()
  {
    _popupWidth = 0;
  }

  /**
   * @author Andy Mechtenberg
   */
  public Dimension getPopupSize()
  {
    Dimension size = getSize();
    if ((_popupWidth < 1) || (_popupWidth < size.width))
    {
      return new Dimension(size.width, size.height);
    }
    else
      return new Dimension(_popupWidth, size.height);
  }

  /**
   * This private class is used only by the VariableWidthComboBox, and it provides an
   * override to the createPopup method to determine the width.
   * @author Andy Mechtenberg
   */
  class VariableWidthComboBoxUI extends SynthComboBoxUI
//  class VariableWidthComboBoxUI extends com.nilo.plaf.nimrod.NimRODComboBoxUI
  {
    /**
     * @author Andy Mechtenberg
     */
    protected ComboPopup createPopup()
    {
      // since we are extending com.sun.java.swing.plaf.windows.WindowsComboBoxUI explicitly, we assert that this indeed
      // is the look and feel for windows
//      Assert.expect(UIManager.getSystemLookAndFeelClassName().equals("com.sun.java.swing.plaf.windows.WindowsLookAndFeel"));
      BasicComboPopup popup = new BasicComboPopup(comboBox)
      {
        /**
         * @deprecated
         */
        public void show()
        {
          Dimension popupSize = ((VariableWidthComboBox)comboBox).getPopupSize();
          popupSize.setSize(popupSize.width, getPopupHeightForRowCount(comboBox.getMaximumRowCount() + 10));
          Rectangle popupBounds = computePopupBounds(0, comboBox.getBounds().height + 5, popupSize.width, popupSize.height + 5);
          scroller.setMaximumSize(popupBounds.getSize());
          scroller.setPreferredSize(popupBounds.getSize());
          scroller.setMinimumSize(popupBounds.getSize());
          list.invalidate();
          int selectedIndex = comboBox.getSelectedIndex();

          if (selectedIndex == -1)
            list.clearSelection();
          else
            list.setSelectedIndex(selectedIndex);

          list.ensureIndexIsVisible(list.getSelectedIndex());
          setLightWeightPopupEnabled(comboBox.isLightWeightPopupEnabled());

          show(comboBox, popupBounds.x, popupBounds.y);
        }
      };
      popup.getAccessibleContext().setAccessibleParent(comboBox);
      return popup;
    }
  }
}
