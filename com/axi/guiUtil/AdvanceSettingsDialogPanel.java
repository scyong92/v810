package com.axi.guiUtil;

import java.util.*;
import com.axi.util.*;

/**
 *
 * @author swee-yee.wong
 */
public class AdvanceSettingsDialogPanel
{
  private String _panelName = null;
  private List<ComboBoxItem> _comboBoxItemList = new ArrayList<ComboBoxItem>();
  private List<CheckBoxItem> _checkBoxItemList = new ArrayList<CheckBoxItem>();
  
  public AdvanceSettingsDialogPanel(String panelName, List<ComboBoxItem> comboBoxItemList, List<CheckBoxItem> checkBoxItemList)
  {
    Assert.expect(panelName != null);
    Assert.expect(comboBoxItemList != null);
    Assert.expect(checkBoxItemList != null);
    
    _panelName = panelName;
    _comboBoxItemList = comboBoxItemList;
    _checkBoxItemList = checkBoxItemList;
  }
  
  public AdvanceSettingsDialogPanel(String panelName)
  {
    Assert.expect(panelName != null);
    _panelName = panelName;
  }
  
  public void setPanelName(String panelName)
  {
    Assert.expect(panelName != null);
    _panelName = panelName;
  }
  
  public String getPanelName()
  {
    Assert.expect(_panelName != null);
    return _panelName;
  }
  
  public void setComboBoxItemList(List<ComboBoxItem> comboBoxItemList)
  {
    Assert.expect(comboBoxItemList != null);
    if(_comboBoxItemList != null)
      _comboBoxItemList.removeAll(_comboBoxItemList);
    _comboBoxItemList = comboBoxItemList;
  }
  
  public List<ComboBoxItem> getComboBoxItemList()
  {
    Assert.expect(_comboBoxItemList != null);
    return _comboBoxItemList;
  }
  
  public void addComboBoxItemToList(ComboBoxItem comboBoxItem)
  {
    Assert.expect(_comboBoxItemList != null);
    _comboBoxItemList.add(comboBoxItem);
  }
  
  public void removeComboBoxItemFromList(ComboBoxItem comboBoxItem)
  {
    Assert.expect(comboBoxItem != null);
    if(_comboBoxItemList.contains(comboBoxItem))
      _comboBoxItemList.remove(comboBoxItem);
  }
  
  public void removeAllComboBoxItems()
  {
    if(_comboBoxItemList != null)
      _comboBoxItemList.removeAll(_comboBoxItemList);
  }
  
  public void setCheckBoxItemList(List<CheckBoxItem> checkBoxItemList)
  {
    Assert.expect(checkBoxItemList != null);
    if(_checkBoxItemList != null)
      _checkBoxItemList.removeAll(_checkBoxItemList);
    _checkBoxItemList = checkBoxItemList;
  }
  
  public List<CheckBoxItem> getCheckBoxItemList()
  {
    Assert.expect(_checkBoxItemList != null);
    return _checkBoxItemList;
  }
  
  public void addCheckBoxItemToList(CheckBoxItem checkBoxItemList)
  {
    Assert.expect(_checkBoxItemList != null);
    _checkBoxItemList.add(checkBoxItemList);
  }
  
  public void removeCheckBoxItemFromList(CheckBoxItem checkBoxItemList)
  {
    Assert.expect(checkBoxItemList != null);
    if(_checkBoxItemList.contains(checkBoxItemList))
      _checkBoxItemList.remove(checkBoxItemList);
  }
  
  public void removeAllCheckBoxItems()
  {
    if(_checkBoxItemList != null)
      _checkBoxItemList.removeAll(_checkBoxItemList);
  }
}
