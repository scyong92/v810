package com.axi.guiUtil;

/**
 * @author Andy Mechtenberg
 */
public class GraphicsEngineEventEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static GraphicsEngineEventEnum ZOOM_RECTANGLE_EVENT_FINISHED = new GraphicsEngineEventEnum(++_index);


  /**
   * @author Bill Darbie
   */
  private GraphicsEngineEventEnum(int id)
  {
    super(id);
  }
}
