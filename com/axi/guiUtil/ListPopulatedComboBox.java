package com.axi.guiUtil;

import java.util.*;
import javax.swing.*;

import com.axi.util.*;

/**
 * This combo box is an extension of the normal swing JComboBox. However it allows the programmer to populate the model
 * with a list. Allowing the list iteration to be within the component. This class also provides a way to see if a given
 * item exists in the model already.
 *
 * @author Erica Wheatcroft
 */
public class ListPopulatedComboBox extends JComboBox
{
  /**
   * @author Erica Wheatcroft
   */
  public ListPopulatedComboBox(ComboBoxModel model)
  {
    super(model);
    Assert.expect(model != null, "model is null");
  }

  /**
   * @author Erica Wheatcroft
   */
  public ListPopulatedComboBox(Object[] items)
  {
    super(items);
    Assert.expect(items != null, "items is null");
  }

  /**
   * @author Erica Wheatcroft
   */
  public ListPopulatedComboBox(Vector items)
  {
    super(items);
    Assert.expect(items != null, "items is null");
  }

  /**
   * @author Erica Wheatcroft
   */
  public ListPopulatedComboBox()
  {
    super();
  }

  /**
   * @author Erica Wheatcroft
   */
  public ListPopulatedComboBox(List<?> items)
  {
    super();
    Assert.expect(items != null, "items to add to the combo box are null");

    for(Object item : items)
    {
      Assert.expect(item != null, "item to add to the combo box is null");
      addItem(item);
    }
  }

  /**
   * This method will clear the current list of items in the combo box and populated the model
   * with the given list.
   * @author Erica Wheatcroft
   */
  public void populateComboBox(List<?> items)
  {
    Assert.expect(items != null, "items to add to the combo box are null");

    removeAll();

    for (Object item : items)
    {
      Assert.expect(item != null, "item to add to the combo box is null");
      addItem(item);
    }

  }

  /**
   * This method will update the model with the list of items
   * @author Erica Wheatcroft
   */
  public void addAll(List<?> items)
  {
    Assert.expect(items != null, "items to add to the combo box are null");

    for (Object item : items)
    {
      Assert.expect(item != null, "item to add to the combo box is null");
      addItem(item);
    }
  }

  /**
   * This method will search for the given item if found the method will return true, false otherwise.
   * @author Erica Wheatcroft
   */
  public boolean containsItem(Object item)
  {
    Assert.expect(item != null, "item is null");

    int numberOfItems = getItemCount();

    for(int count = 0; count < numberOfItems; count++)
    {
      Object object = getItemAt(count);
      if(object.equals(item))
        return true;
    }

    // the item was not found
    return false;
  }
}
