package com.axi.guiUtil;

import java.util.*;

import com.axi.util.*;

/**
 * This class is used by the GraphicsEngine class and will call any
 * Observers update method with a Collection of the currently selected Renderers.
 *
 * @author Bill Darbie
 */
public class SelectedRendererObservable extends Observable
{

  /**
   * @author Bill Darbie
   */
  SelectedRendererObservable()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  public void setSelectedRenderer(Collection<Renderer> renderers)
  {
    Assert.expect(renderers != null);
    setChanged();
    notifyObservers(renderers);
  }
}
