package com.axi.guiUtil;

import java.awt.*;
import java.util.*;

import com.axi.util.*;

/**
 * This class is used by the GraphicsEngine class and will call any
 * Observers update method with a currently selected rectangle.
 *
 * @author sham
 */
public class SelectedAreaObservable extends Observable
{

  /**
   * @author Bill Darbie
   */
  SelectedAreaObservable()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  public void setSelectedArea(Shape rectangle)
  {
    Assert.expect(rectangle != null);
    setChanged();
    notifyObservers(rectangle);
  }
}