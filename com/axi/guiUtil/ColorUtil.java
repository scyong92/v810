package com.axi.guiUtil;

import java.awt.*;

/**
 * This is a utility class to handle GUI colors
 * @author Andy Mechtenberg
 */
public class ColorUtil
{
  /**
   * For use with etched borders as the showdow color
   * @author Andy Mechtenberg
   */
  public static Color getGrayShadowColor()
  {
    return new Color(134, 134, 134);
  }

  /**
   * For use with beveled borders for the inner shadow color.
   * For the outer shadow, use getGrayShadowColor
   * @author Andy Mechtenberg
   */
  public static Color getGrayInnerShadowColor()
  {
    return new Color(93, 93, 93);
  }
}