package com.axi.guiUtil;

import com.axi.util.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;
import javax.swing.*;
import javax.swing.border.*;

/**
 *
 * @author swee-yee.wong
 */
public class AdvanceSettingsDialog extends EscapeDialog
{
  private JFrame _frame;
  private int _returnValue; 
  private String _instruction;
  private String _okButtonText;
  private String _cancelButtonText;
  private String _resetButtonText;
  
  private boolean _isResetToIntialSetting = false;
  
  private Map<AdvanceSettingsDialogPanel, JPanel> _advanceSettingsToPanelMap = new HashMap<AdvanceSettingsDialogPanel, JPanel>();
  private Map<CheckBoxItem, JCheckBox> _checkBoxItemToJCheckBoxMap = new HashMap<CheckBoxItem, JCheckBox>();
  private Map<ComboBoxItem, JComboBox> _comboBoxItemToJComboBoxMap = new HashMap<ComboBoxItem, JComboBox>();

  private JPanel _mainPanel = new JPanel();
  private JPanel _messagePanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();
  private JPanel _advanceSettingsPanel = new JPanel();
  
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private FlowLayout _messagePanelFlowLayout = new FlowLayout();
  private FlowLayout _buttonPanelFlowLayout = new FlowLayout();
  private GridLayout _innerButtonPanelGridLayout = new GridLayout();
  
  private GridLayout _advanceSettingsPanelGridLayout = new GridLayout();
  private Border _innerButtonPanelBorder = BorderFactory.createEmptyBorder(5, 10, 0, 10);
  private Border _mainPanelBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
  
  private List<AdvanceSettingsDialogPanel> _advanceSettingsPanelList = new ArrayList<AdvanceSettingsDialogPanel>();
   
  private JLabel _messageLabel = new JLabel();
  private JButton _okButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }

  };
  
  private JButton _resetButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };  
  
  private JButton _cancelButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };  

  /**
   * @author bee-hoon.goh
   */
  private AdvanceSettingsDialog()
  {
    // do nothing
  }

  /**
   * @author bee-hoon.goh
   */
  public AdvanceSettingsDialog(JFrame frame,
                          String title,
                          String message,
                          String okButtonText,
                          String cancelButtonText,
                          String resetButtonText,
                          List<AdvanceSettingsDialogPanel> advanceSettingsDialogPanelList)
  {
    super(frame, title, true);

    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(message != null);
    Assert.expect(okButtonText != null);
    Assert.expect(cancelButtonText != null);
    Assert.expect(advanceSettingsDialogPanelList != null);

    _frame = frame;
    _instruction = message;
    _okButtonText = okButtonText;
    _cancelButtonText = cancelButtonText;
    _resetButtonText = resetButtonText;

    _advanceSettingsPanelList = advanceSettingsDialogPanelList;
    
    createDialog();
    pack();

  }
  
  /**
   * @author bee-hoon.goh
   */
  private void createDialog()
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _mainPanel.setBorder(_mainPanelBorder);
    _mainPanelBorderLayout.setVgap(10);
    
    _cancelButton.setText(_cancelButtonText);  
    _okButton.setText(_okButtonText);
    _resetButton.setText(_resetButtonText);
    
    _innerButtonPanel.setLayout(_innerButtonPanelGridLayout);
    _innerButtonPanelGridLayout.setHgap(10);
    _innerButtonPanel.setBorder(_innerButtonPanelBorder);
    
    _messagePanel.setLayout(_messagePanelFlowLayout);
    _messagePanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _messagePanelFlowLayout.setHgap(0);
    _messagePanelFlowLayout.setVgap(0);  
    _messageLabel.setText(_instruction);
    
    _advanceSettingsPanel.setLayout(_advanceSettingsPanelGridLayout);
    int totalPanel = _advanceSettingsPanelList.size();
    
    Assert.expect(totalPanel != 0);
    Assert.expect(totalPanel < 50);
    
    int row = 1;
    int column = 1;
    while((row * column) < totalPanel)
    {
      if(row == column)
        column++;
      else
        row++;
    }
    _advanceSettingsPanelGridLayout.setRows(row);
    _advanceSettingsPanelGridLayout.setColumns(column);
    _advanceSettingsPanelGridLayout.setHgap(5);
    _advanceSettingsPanelGridLayout.setVgap(5); 
   
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    _buttonPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _buttonPanelFlowLayout.setHgap(0);
    _buttonPanelFlowLayout.setVgap(0);
       
    getContentPane().add(_mainPanel);
    _innerButtonPanel.add(_okButton);
    _innerButtonPanel.add(_resetButton);
    _innerButtonPanel.add(_cancelButton);
    _messagePanel.add(_messageLabel);
    _buttonPanel.add(_innerButtonPanel);
    
    _mainPanel.add(_messagePanel, BorderLayout.NORTH);
    _mainPanel.add(_advanceSettingsPanel, BorderLayout.CENTER);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    
    getRootPane().setDefaultButton(_okButton);
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okAction();
      }
    });
    
    _resetButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        resetAction();
      }
    });
    
    for(AdvanceSettingsDialogPanel advanceSettingsDialogPanel : _advanceSettingsPanelList)
    {
      JPanel currentSettingsPanel = new JPanel();
      VerticalFlowLayout currentSettingsPanelVerticalFlowLayout = new VerticalFlowLayout();
      currentSettingsPanel.setLayout(currentSettingsPanelVerticalFlowLayout);
      currentSettingsPanelVerticalFlowLayout.setHgap(0);
      currentSettingsPanelVerticalFlowLayout.setVgap(0);
      JPanel previousEntry = _advanceSettingsToPanelMap.put(advanceSettingsDialogPanel, currentSettingsPanel);
      Assert.expect(previousEntry == null);
      TitledBorder titled = new TitledBorder(advanceSettingsDialogPanel.getPanelName());
      currentSettingsPanel.setBorder(titled);
      
      List<CheckBoxItem> checkBoxItemList = advanceSettingsDialogPanel.getCheckBoxItemList();
      if(checkBoxItemList.size() > 0)
      {
        for (CheckBoxItem checkBoxItem : checkBoxItemList)
        {
          JPanel currentCheckBoxPanel = new JPanel();
          VerticalFlowLayout currentCheckBoxPanelVerticalFlowLayout = new VerticalFlowLayout();
          currentCheckBoxPanel.setLayout(currentCheckBoxPanelVerticalFlowLayout);
          currentCheckBoxPanelVerticalFlowLayout.setHgap(0);
          currentCheckBoxPanelVerticalFlowLayout.setVgap(0);
          
          JCheckBox currentCheckBox = new JCheckBox(checkBoxItem.getName());
          currentCheckBox.setSelected(checkBoxItem.getSelected());
          currentCheckBoxPanel.add(currentCheckBox);
          
          JCheckBox previousCheckBoxEntry = _checkBoxItemToJCheckBoxMap.put(checkBoxItem, currentCheckBox);
          Assert.expect(previousCheckBoxEntry == null);
          
          String message = checkBoxItem.getMessage();
          if(message != null)
          {
            JLabel currentCheckBoxLabel = new JLabel();
            currentCheckBoxLabel.setText(message);
            currentCheckBoxPanel.add(currentCheckBoxLabel);
          }
          currentSettingsPanel.add(currentCheckBoxPanel);
        }
      }
      
      List<ComboBoxItem> comboBoxItemList = advanceSettingsDialogPanel.getComboBoxItemList();
      if(comboBoxItemList.size() > 0)
      {
        for (ComboBoxItem comboBoxItem : comboBoxItemList)
        {
          JPanel currentComboBoxPanel = new JPanel();
          VerticalFlowLayout currentComboBoxPanelVerticalFlowLayout = new VerticalFlowLayout();
          currentComboBoxPanel.setLayout(currentComboBoxPanelVerticalFlowLayout);
          currentComboBoxPanelVerticalFlowLayout.setHgap(0);
          currentComboBoxPanelVerticalFlowLayout.setVgap(0);
          
          JPanel currentInnerComboBoxPanel = new JPanel();
          FlowLayout currentInnerComboBoxPanelFlowLayout = new FlowLayout();
          currentInnerComboBoxPanel.setLayout(currentInnerComboBoxPanelFlowLayout);
          currentInnerComboBoxPanelFlowLayout.setHgap(0);
          currentInnerComboBoxPanelFlowLayout.setVgap(0);
          
          JLabel currentComboBoxTitle = new JLabel();
          currentComboBoxTitle.setText(comboBoxItem.getName() + ":");
          currentInnerComboBoxPanel.add(currentComboBoxTitle);
          
          JComboBox currentComboBox = new JComboBox();
          for(String listItem : comboBoxItem.getDropDownList())
          {
            currentComboBox.addItem(listItem);
          }
          currentComboBox.setSelectedItem(comboBoxItem.getSelected());
          currentInnerComboBoxPanel.add(currentComboBox);
          currentComboBoxPanel.add(currentInnerComboBoxPanel);
          
          JComboBox previousComboBoxEntry = _comboBoxItemToJComboBoxMap.put(comboBoxItem, currentComboBox);
          Assert.expect(previousComboBoxEntry == null);
          
          String message = comboBoxItem.getMessage();
          if(message != null)
          {
            JLabel currentComboBoxLabel = new JLabel();
            currentComboBoxLabel.setText(message);
            currentComboBoxPanel.add(currentComboBoxLabel);
          }
          currentSettingsPanel.add(currentComboBoxPanel);
        }
      }
      _advanceSettingsPanel.add(currentSettingsPanel);
    }
  }
  
  /**
   * @author bee-hoon.goh
   */
  public int showDialog()
  {
    SwingUtils.centerOnComponent(this, _frame);
    setVisible(true);
    return _returnValue;
  }
  
  /**
   * @author bee-hoon.goh
   */
  private void okAction()
  {   
    for(AdvanceSettingsDialogPanel advanceSettingsDialogPanel : _advanceSettingsPanelList)
    {
      List<CheckBoxItem> checkBoxItemList = advanceSettingsDialogPanel.getCheckBoxItemList();
      if(checkBoxItemList.size() > 0)
      {
        for (CheckBoxItem checkBoxItem : checkBoxItemList)
        {
          JCheckBox checkBox = _checkBoxItemToJCheckBoxMap.get(checkBoxItem);
          checkBoxItem.setSelected(checkBox.isSelected());
        }
      }
      
      List<ComboBoxItem> comboBoxItemList = advanceSettingsDialogPanel.getComboBoxItemList();
      if(comboBoxItemList.size() > 0)
      {
        for (ComboBoxItem comboBoxItem : comboBoxItemList)
        {
          JComboBox comboBox = _comboBoxItemToJComboBoxMap.get(comboBoxItem);
          comboBoxItem.setSelected(comboBox.getSelectedItem().toString());
        }
      }
    }
    
    _returnValue = JOptionPane.OK_OPTION;

    dispose();
  }
  
  /**
   * @author bee-hoon.goh
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    cancelAction();
  }
  
  /**
   * @author bee-hoon.goh
   */
  private void cancelAction()
  {
    _returnValue = JOptionPane.CANCEL_OPTION;
    dispose();
  }
  
  /**
   * XCR-2895 :Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   */
  private void resetAction()
  {
      _isResetToIntialSetting = true;

      _returnValue = JOptionPane.OK_OPTION;

      dispose();
  }

  /**
   * @author bee-hoon.goh
   */
  public List<AdvanceSettingsDialogPanel> getSelectedValue()
  {
    return _advanceSettingsPanelList;
  }
  
  /**
   * XCR-2895 :Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   */
  public boolean getIsResetToIntialSetting()
  {
    return _isResetToIntialSetting;
  }
}
