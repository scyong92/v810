package com.axi.guiUtil;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

/**
 * @author siew-yeng.phang
 */
public class RadioButtonCellRenderer implements TableCellRenderer
{
  final JRadioButton _radioButton = new JRadioButton();
  /**
   * Overridden method.  Returns a component to render a radio button that is centered.
   * @param table Table instance that will render the component.
   * @param value Value to be rendered (should be a Boolean value)
   * @param isSelected Whether the cell is selected
   * @param hasFocus Whether the cell has focus
   * @param row Row index for the cell
   * @param column Column index for the cell
   * @return Component (JRadioButton)
   * @author Carli Connally
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    if (value == null)
      return null;

    Color alternativeColor = (Color) UIManager.get ("Table.alternateRowColor");
    Color backgroundColor = Color.WHITE;
    
    _radioButton.setSelected((Boolean) value);
    _radioButton.setOpaque(true);
    _radioButton.setFont(table.getFont());
    _radioButton.setHorizontalAlignment((int)JRadioButton.CENTER_ALIGNMENT);

    if (isSelected)
    {
      _radioButton.setForeground(table.getSelectionForeground());
      _radioButton.setBackground(table.getSelectionBackground());
    }
    else
    {
      _radioButton.setForeground(table.getForeground());
      if(row%2 == 0)
        _radioButton.setBackground(backgroundColor);
      else
        _radioButton.setBackground(alternativeColor);
    }
    return _radioButton;
  } 
}
