package com.axi.guiUtil;

import java.util.*;

import com.axi.util.*;

/**
 * This class is used by the GraphicsEngine class and will call any
 * Observers update method with the coordinates of a left mouse click
 *
 * @author Bill Darbie
 */
public class LeftMouseClickObservable extends Observable
{
  private static LeftMouseClickObservable _instance;

  /**
   * @author Bill Darbie
   */
  LeftMouseClickObservable()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  void setLeftMouseClickCoordinate(IntCoordinate leftClickCoordinate)
  {
    Assert.expect(leftClickCoordinate != null);
    setChanged();
    notifyObservers(leftClickCoordinate);
  }
}
