package com.axi.guiUtil;


import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.util.*;
import java.util.Enumeration;

/**
 *
 * @author chin-seong.kee
 */
public class ChoiceRadioButtonDialog extends EscapeDialog
{
  // parent
  private JFrame _frame;
  // either OK or CANCEL depending on how the user exits
  private int _returnValue;
  
  private Object _selectedValue = null;
  private String _instruction;
  private String _okButtonText;
  private String _cancelButtonText;
  
  //   Create group
  ButtonGroup _radioButtonGroup = new ButtonGroup();
  JRadioButton []_radioButtonChoices;
  private java.util.List<Object> _choiceRadioButtonList;

  private Font _buttonFont;
  private Font _messageFont;

  private JPanel _mainPanel = new JPanel();
  private JPanel _messagePanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();
  
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private FlowLayout _messagePanelFlowLayout = new FlowLayout();
  private FlowLayout _buttonPanelFlowLayout = new FlowLayout();
  private GridLayout _innerButtonPanelGridLayout = new GridLayout();
  private JLabel _messageLabel = new JLabel();
  private JPanel _inputPanel = new JPanel();
  private FlowLayout _inputPanelFlowLayout = new FlowLayout();
  private Border _innerButtonPanelBorder = BorderFactory.createEmptyBorder(5, 10, 0, 10);
  private Border _mainPanelBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
   
  private JButton _okButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }

  };
  private JButton _cancelButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };  

  /**
   * @author chin-seong.kee
   */
  private ChoiceRadioButtonDialog()
  {
    // do nothing
  }

  /**
   * @author chin-seong.kee
   */
  public ChoiceRadioButtonDialog(JFrame frame,
                          String title,
                          String message,
                          String okButtonText,
                          String cancelButtonText,
                          Object[] choices,
                          Object selectedValue)
  {
    super(frame, title, true);

    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(message != null);
    Assert.expect(okButtonText != null);
    Assert.expect(cancelButtonText != null);
    Assert.expect(choices != null);

    _frame = frame;
    _instruction = message;
    _okButtonText = okButtonText;
    _cancelButtonText = cancelButtonText;
    _buttonFont = FontUtil.getDialogButtonFont();
    _messageFont = FontUtil.getDialogMessageFont();

    _choiceRadioButtonList = new java.util.ArrayList<Object>();
     for (int i = 0; i < choices.length; i++)
       _choiceRadioButtonList.add(choices[i]);
    
    createDialog();
    pack();

  }
  
  /**
   * @author Andy Mechtenberg
   * @author chin-seong.kee
   */
  private void createDialog()
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _cancelButton.setText(_cancelButtonText);
    _innerButtonPanelGridLayout.setHgap(5);
    _messagePanel.setLayout(_messagePanelFlowLayout);
    _messagePanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _messagePanelFlowLayout.setHgap(0);
    _messagePanelFlowLayout.setVgap(0);
    _cancelButton.setFont(_buttonFont);
    _okButton.setFont(_buttonFont);
    _cancelButton.setMargin(new Insets(2, 8, 2, 8));
    _okButton.setMargin(new Insets(2, 8, 2, 8));
    _messageLabel.setFont(_messageFont);
    _messageLabel.setText(_instruction);
    _inputPanel.setLayout(_inputPanelFlowLayout);
    _buttonPanelFlowLayout.setHgap(0);
    _buttonPanelFlowLayout.setVgap(0);
    _innerButtonPanel.setBorder(_innerButtonPanelBorder);
    _mainPanel.setBorder(_mainPanelBorder);
    _mainPanelBorderLayout.setVgap(5);

    getContentPane().add(_mainPanel);
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    _buttonPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _innerButtonPanel.setLayout(_innerButtonPanelGridLayout);
    _okButton.setText(_okButtonText);
    _innerButtonPanel.add(_okButton);
    _innerButtonPanel.add(_cancelButton);
    _messagePanel.add(_messageLabel);
    _buttonPanel.add(_innerButtonPanel);
    
    _mainPanel.add(_inputPanel, BorderLayout.CENTER);
    _mainPanel.add(_messagePanel, BorderLayout.NORTH);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    
    getRootPane().setDefaultButton(_okButton);
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okAction();
      }
    });
    
    _radioButtonChoices = new JRadioButton[_choiceRadioButtonList.size()];
    for (int i = 0; i < _choiceRadioButtonList.size(); i++) 
    {
      _radioButtonChoices[i] = new JRadioButton(_choiceRadioButtonList.get(i).toString())
      {
        protected void processKeyEvent(KeyEvent e)
        {
          super.processKeyEvent(e);
          if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
          { 
           cancelAction();
          }
        }
      };
      _inputPanel.add(_radioButtonChoices[i]);
      _radioButtonGroup.add(_radioButtonChoices[i]);
    }
    _radioButtonChoices[0].setSelected(true);
  }
  
  /**
   * @return int
   * @author chin-seong.kee
   */
  public int showDialog()
  {
    SwingUtils.centerOnComponent(this, _frame);
    setVisible(true);
    return _returnValue;
  }

  /**
   * @param font Font
   * @return int
   * @author chin-seong.kee
   */
  public int showDialog(Font font)
  {
    Assert.expect(font != null);
    SwingUtils.setFont(this, font);
    pack();
    SwingUtils.centerOnComponent(this, _frame);

    setVisible(true);
    return _returnValue;
  }
  
  /**
   * @param e WindowEvent
   * @author chin-seong,kee
   */
  private void okAction()
  {
    for (Enumeration e = _radioButtonGroup.getElements(); e.hasMoreElements();) 
    {
        JRadioButton b = (JRadioButton)e.nextElement();
        if (b.getModel() == _radioButtonGroup.getSelection()) {
          _selectedValue = b.getText();
        }
    }
    _returnValue = JOptionPane.OK_OPTION;

    dispose();
  }
  
  /**
   * @author chin-seong,kee
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    cancelAction();
  }
  
  /**
   * @author chin-seong,kee
   */
  private void cancelAction()
  {
    _returnValue = JOptionPane.CANCEL_OPTION;
    _selectedValue = null;
    dispose();
  }

  /**
   * @return Object
   * @author chin-seong.kee
   */
  public Object getSelectedValue()
  {
    return _selectedValue;
  }
}
