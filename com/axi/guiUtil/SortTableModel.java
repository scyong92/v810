package com.axi.guiUtil;

import javax.swing.table.*;

/**
 * This interface extends the standard TableModel to provide column sorting
 * It is further extended to allow row selection by matching typeahead characters
 *
 * @author Claude Duguay of JavaPro
 * @author George Booth
 */
public interface SortTableModel extends TableModel
{
  public boolean isSortable(int col);
  public void sortColumn(int col, boolean ascending);
  public int getRowForDataStartingWith(String key);

}

