package com.axi.guiUtil;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

/**
 * 
 * @author Kok Chun, Tan
 */
public class MultipleLineCellRenderer extends JTextArea implements TableCellRenderer
{
  protected int _verticalAlignment;
  protected int _horizontalAlignment;
  protected float _alignmentX;
  protected Color _foreground;
  protected Color _background;
  protected Font _font;
  protected static Border _border = new EmptyBorder(1, 5, 1, 2);
  
  /**
   * @author Kok Chun, Tan
   */
  public MultipleLineCellRenderer()
  {
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    _background = null;
    _foreground = null;
  }

  /**
   * @author Kok Chun, Tan
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    removeAll();
    invalidate();

    if (value == null || table == null)
    {
      // Do nothing if no value
      return this;
    }

    // Set the _foreground and _background colors
    // from the table if they are not set
    Color cellForeground = (_foreground == null ? table.getForeground() : _foreground);
    Color cellBackground = (_background == null ? table.getBackground() : _background);

    // Handle selection and focus colors
    if (isSelected == true)
    {
      cellForeground = table.getSelectionForeground();
      cellBackground = table.getSelectionBackground();
    }

    if (hasFocus == true)
    {
      setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
      if (table.isCellEditable(row, column))
      {
        cellForeground = UIManager.getColor("Table.focusCellForeground");
        cellBackground = UIManager.getColor("Table.focusCellBackground");
      }
    }
    else
    {
      setBorder(_border);
    }

    super.setForeground(cellForeground);
    super.setBackground(cellBackground);

    // Default the _font from the table
    if (_font == null)
    {
      _font = table.getFont();
    }

    if (_verticalAlignment != SwingConstants.TOP)
    {
      add(Box.createVerticalGlue());
    }

    Object[] values;
    int length;
    if (value instanceof Object[])
    {
      // Input is an array - use it
      values = (Object[]) value;
    }
    else
    {
      // Not an array - turn it into one
      values = new Object[1];
      values[0] = value;
    }
    length = values.length;

    // Configure each row of the cell using
    // a separate JLabel. If a given row is
    // a JComponent, add it directly..
    for (int i = 0; i < length; i++)
    {
      Object thisRow = values[i];

      if (thisRow instanceof JComponent)
      {
        add((JComponent) thisRow);
      }
      else
      {
        JLabel label = new JLabel();
        setValue(label, thisRow, i, cellForeground);
        add(label);
      }
    }

    if (_verticalAlignment != SwingConstants.BOTTOM)
    {
      add(Box.createVerticalGlue());
    }
    
    if (length > 1)
    {
      table.setRowHeight(row, this.getPreferredSize().height*length);
    }
      
    return this;
  }

  // Configures a label for one line of the cell.
  // This can be overridden by derived classes
  protected void setValue(JLabel label, Object value, int lineNumber, Color cellForeground)
  {
    if (value != null && value instanceof Icon)
    {
      label.setIcon((Icon) value);
    }
    else
    {
      label.setText(value == null ? "" : value.toString());
    }
    label.setHorizontalAlignment(_horizontalAlignment);
    label.setAlignmentX(_alignmentX);
    label.setOpaque(false);
    label.setForeground(cellForeground);
    label.setFont(_font);
  }
}
