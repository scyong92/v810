package com.axi.guiUtil;

import java.awt.*;
import java.awt.geom.*;

import com.axi.util.*;

/**
 * @author George A. David
 */
public class RectangleRenderer extends ShapeRenderer
{
  private Rectangle2D _rectangle;

  /**
   * @author George A. David
   */
  public RectangleRenderer(Rectangle2D rectangle, boolean selectable)
  {
    super(selectable);
    setFillShape(true);
    _rectangle = rectangle;
    refreshData();
  }

  /**
   * @author George A. David
   */
  protected void refreshData()
  {
    Assert.expect(_rectangle != null);
    initializeShape(_rectangle);
  }

  /**
   * @author George A. David
   */
  public void paintComponent(Graphics graphics)
  {
    super.paintComponent(graphics);

    graphics.setColor(Color.blue);
    graphics.drawString("this is a test", (int)_rectangle.getCenterX(), (int)_rectangle.getCenterY());
  }
}

