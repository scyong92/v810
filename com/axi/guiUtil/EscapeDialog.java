package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * <p>Title: EscapeDialog</p>
 *
 * <p>Description: A JDialog that contains a rootpane who listens to the Escape Key.</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Erica Wheatcroft
 */
public class EscapeDialog extends JDialog
{
  /**
   * @author Erica Wheatcroft
   */
  public EscapeDialog()
  {
    super((JFrame)null, false);
  }

  /**
   * @author Erica Wheatcroft
   */
  public EscapeDialog(Frame owner)
  {
    super(owner, false);
  }

  /**
   * @author Erica Wheatcroft
   */
  public EscapeDialog(Frame owner, boolean modal)
  {
    super(owner, null, modal);
  }

  /**
   * @author Erica Wheatcroft
   */
  public EscapeDialog(Frame owner, String title)
  {
    super(owner, title, false);
  }

  /**
   * @author Erica Wheatcroft
   */
  public EscapeDialog(Frame owner, String title, boolean modal)
  {
    super(owner, title, modal);
  }

  /**
   * @author Erica Wheatcroft
   */
  public EscapeDialog(Dialog owner)
  {
    this(owner, false);
  }

  /**
   * @author Erica Wheatcroft
   */
  public EscapeDialog(Dialog owner, boolean modal)
  {
    this(owner, null, modal);
  }

  /**
   * @author Erica Wheatcroft
   */
  public EscapeDialog(Dialog owner, String title)
  {
    this(owner, title, false);
  }

  /**
   * @author Erica Wheatcroft
   */
  public EscapeDialog(Dialog owner, String title, boolean modal)
  {
    super(owner, title, modal);
  }

  /**
   * @author Erica Wheatcroft
   */
  protected JRootPane createRootPane()
  {
    ActionListener actionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent actionEvent)
      {
        setVisible(false);
      }
    };

    JRootPane rootPane = new JRootPane();
    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    rootPane.registerKeyboardAction(actionListener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
    return rootPane;
  }
}

