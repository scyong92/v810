package com.axi.guiUtil;

import com.axi.util.*;

/**
 * @author Andy Mechtenberg
 */
public class GraphicsEngineEvent
{
  private Object _dataObject;
  private GraphicsEngineEventEnum _eventEnum;

  /**
   * @author Andy Mechtenberg
   */
  GraphicsEngineEvent(Object dataObject, GraphicsEngineEventEnum eventEnum)
  {
    // not asserting on dataObject, because it can be null
    Assert.expect(eventEnum != null);
    _dataObject = dataObject;
    _eventEnum = eventEnum;
  }

  /**
   * @author Andy Mechtenberg
   */
  public Object getDataObject()
  {
    return _dataObject;
  }

  /**
   * @author Andy Mechtenberg
   */
  public GraphicsEngineEventEnum getGraphicsEngineEventEnum()
  {
    return _eventEnum;
  }
}
