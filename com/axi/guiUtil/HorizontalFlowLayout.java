package com.axi.guiUtil;

import java.awt.*;

/**
 * This is a layout class that arranges components like the FlowLayout
 * does only horizontally.
 * @author Steve Anonson
 */
public class HorizontalFlowLayout implements LayoutManager, java.io.Serializable
{
  //wpdfix -these should be enums, NOT public ints
  // These values indicate how each column of components should be justified.
  public static final int TOP   = 0;
  public static final int MIDDLE = 1;
  public static final int BOTTOM  = 2;
  // These values indicate that each row of components should be justified in the container
  // e.g. to the top/middle/bottom of the container.
  public static final int LEFT    = 3;
  public static final int CENTER = 4;
  public static final int RIGHT = 5;

  /*
  * This is how the components are layed out with the possible alignment values.
  * The container is too small in this example which forces the components into multiple columns
  * and better illustrate the consequences.
  *
  * TOP/LEFT                   TOP/CENTER            TOP/RIGHT
  * =========================  =========================  =========================
  * | -------  ----- -----  |  | -------  ----- -----  |  | -------  ----- -----  |
  * | |  1  |  |   | | 3 |  |  | |  1  |  |   | | 3 |  |  | |  1  |  |   | | 3 |  |
  * | -------  | 2 | -----  |  | -------  | 2 | -----  |  | -------  | 2 | -----  |
  * |          |   |        |  |          |   |        |  |          |   |        |
  * |          -----        |  |          -----        |  |          -----        |
  * | -----                 |  |          -----        |  |                -----  |
  * | | 4 |                 |  |          | 4 |        |  |                | 2 |  |
  * | -----                 |  |          -----        |  |                -----  |
  * |                       |  |                       |  |                       |
  * =========================  =========================  =========================
  *
  * MIDDLE/LEFT                MIDDLE/CENTER              MIDDLE/RIGHT
  * =========================  =========================  =========================
  * |          -----        |  |          -----        |  |          -----        |
  * | -------  |   | -----  |  | -------  |   | -----  |  | -------  |   | -----  |
  * | |  1  |  | 2 | | 3 |  |  | |  1  |  | 2 | | 3 |  |  | |  1  |  | 2 | | 3 |  |
  * | -------  |   | -----  |  | -------  |   | -----  |  | -------  |   | -----  |
  * |          -----        |  |          -----        |  |          -----        |
  * | -----                 |  |          -----        |  |                -----  |
  * | | 4 |                 |  |          | 4 |        |  |                | 2 |  |
  * | -----                 |  |          -----        |  |                -----  |
  * |                       |  |                       |  |                       |
  * =========================  =========================  =========================
  *
  * BOTTOM/LEFT                BOTTOM/CENTER              BOTTOM/RIGHT
  * =========================  =========================  =========================
  * |          -----        |  |          -----        |  |          -----        |
  * |          |   |        |  |          |   |        |  |          |   |        |
  * | -------  | 2 | -----  |  | -------  | 2 | -----  |  | -------  | 2 | -----  |
  * | |  1  |  |   | | 3 |  |  | |  1  |  |   | | 3 |  |  | |  1  |  |   | | 3 |  |
  * | -------  ----- -----  |  | -------  ----- -----  |  | -------  ----- -----  |
  * | -----                 |  |          -----        |  |                -----  |
  * | | 4 |                 |  |          | 4 |        |  |                | 2 |  |
  * | -----                 |  |          -----        |  |                -----  |
  * |                       |  |                       |  |                       |
  * =========================  =========================  =========================
  */
  private int _valign;  // alignment between elements in a column
  private int _halign;  // alignment between columns of elements
  private int _hgap;  // horizontal gap between components
  private int _vgap;  // vertical gap between components


  /**
   * Constructs a new VFlowLayout with a centered alignment and a default
   * 5-unit horizontal and vertical gap.
   */
  public HorizontalFlowLayout()
  {
    this(TOP, LEFT, 5, 5);
  }

  /**
   * Constructs a new VFlowLayout with the specified alignment and a default
   * 5-unit horizontal and vertical gap.
   *
   * @param componenetAlign - the alignment for components in a column
   * @param columnAlign - the alignment between columns of components
   */
  public HorizontalFlowLayout(int componenetAlign, int columnAlign)
  {
    this(componenetAlign, columnAlign, 5, 5);
  }

  /**
   * Constructs a new VFlowLayout with the specified alignment and a default
   * 5-unit horizontal and vertical gap.
   *
   * @param componenetAlign - the alignment for components in a column
   */
  public HorizontalFlowLayout(int componenetAlign)
  {
    this(componenetAlign, LEFT, 5, 5);
  }

  /**
   * Constructs a new VFlowLayout with the specified alignment and a default
   * 5-unit horizontal and vertical gap.
   *
   * @param componenetAlign - the alignment for components in a column
   * @param columnAlign - the alignment between columns of components
   * @param hgap - horizontal gap between components
   * @param vgap - vertical gap between components
   */
  public HorizontalFlowLayout(int componenetAlign, int columnAlign, int hgap, int vgap)
  {
    setHgap(hgap);
    setVgap(vgap);
    setVerticalAlignment(componenetAlign);
    setHorizontalAlignment(columnAlign);
  }

  // Access methods
  public int getVerticalAlignment() { return _valign; }
  public int getHorizontalAlignment() { return _halign; }
  public int getHgap()      { return _hgap; }
  public int getVgap()      { return _vgap; }

  public void setVerticalAlignment(int componenetAlign) { this._valign = componenetAlign; }
  public void setHorizontalAlignment(int columnAlign) { this._halign = columnAlign; }
  public void setHgap(int hgap)       { this._hgap = hgap; }
  public void setVgap(int vgap)       { this._vgap = vgap; }

  /**
   * Adds the specified component to the layout. Not used by this class.
   */
  public void addLayoutComponent(String name, Component comp)
  {
  }

  /**
   * Removes the specified component from the layout. Not used by this class.
   */
  public void removeLayoutComponent(Component comp)
  {
  }

  /**
   * Returns the preferred dimensions for this layout given the components in
   * the specified target container.
   *
   * @param target - container to be laid out.
   * @return the preferred dimensions of the container.
   *
   * @see Container
   */
  public Dimension preferredLayoutSize(Container target)
  {
    synchronized (target.getTreeLock())
    {
      Dimension dim = new Dimension(0, 0);
      int nmembers = target.getComponentCount();
      boolean firstVisibleComponent = true;

      for (int i = 0 ; i < nmembers ; i++)
      {
        Component m = target.getComponent(i);
        if (m.isVisible())
        {
          Dimension d = m.getPreferredSize();
          dim.height = Math.max(dim.height, d.height);
          if (firstVisibleComponent)
          {
            firstVisibleComponent = false;
          }
          else
          {
            dim.width += _hgap;
          }
          dim.width += d.width;
        }
      }
      Insets insets = target.getInsets();
      dim.width += insets.left + insets.right + _hgap * 2;
      dim.height += insets.top + insets.bottom + _vgap * 2;
      return dim;
    }
  }

  /**
   * Returns the minimum dimensions for this layout given the components in
   * the specified target container.
   *
   * @param target - container to be laid out.
   * @return the minimum dimensions of the container.
   *
   * @see Container
   */
  public Dimension minimumLayoutSize(Container target)
  {
    synchronized (target.getTreeLock())
    {
      Dimension dim = new Dimension(0, 0);
      int nmembers = target.getComponentCount();
      boolean firstVisibleComponent = true;

      for (int i = 0 ; i < nmembers ; i++)
      {
        Component m = target.getComponent(i);
        if (m.isVisible())
        {
          Dimension d = m.getMinimumSize();
          dim.height = Math.max(dim.height, d.height);
          if (firstVisibleComponent)
          {
            firstVisibleComponent = false;
          }
          else
          {
            dim.width += _hgap;
          }
          dim.width += d.width;
        }
      }
      Insets insets = target.getInsets();
      dim.width += insets.left + insets.right + _hgap * 2;
      dim.height += insets.top + insets.bottom + _vgap * 2;
      return dim;
    }
  }

  /**
   * Centers the elements in the specified row, if there is any slack.
   *
   * @param target the component which needs to be moved
   * @param x the x coordinate
   * @param y the y coordinate
   * @param width the width dimensions
   * @param height the height dimensions
   * @param rowStart the beginning of the row
   * @param rowEnd the the ending of the row
   */
  private void moveComponents(Container target, int x, int y, int width, int height,
                              int rowStart, int rowEnd, boolean ltr)
  {
    synchronized (target.getTreeLock())
    {
      switch (_halign)
      {
        case LEFT:
          break;
        case CENTER:
          x += width / 2;
          break;
        case RIGHT:
          x += width;
          break;
      }  // switch _valign
      for (int i = rowStart ; i < rowEnd ; i++)
      {
        Component m = target.getComponent(i);
        int componentY = y;
        int componentX = x;
        if (m.isVisible())
        {
          switch (_valign)
          {
            case TOP:
              break;
            case MIDDLE:
              componentY = y + (height - m.getHeight()) / 2;
              break;
            case BOTTOM:
              componentY = y + height - m.getHeight();
              break;
          }  // switch _halign

          if (ltr)
          {
            m.setLocation(componentX, componentY);
          }
          else
          {
            m.setLocation(target.getWidth() - x - m.getWidth(), componentY);
          }
          x += m.getWidth() + _hgap;
        }
      }  // end for
    }  // end synchronized
  }

  /**
   * Lays out the container. This method lets each component take its preferred
   * size by reshaping the components in thetarget container in order to satisfy
   * the constraints of this VFlowLayout object.
   *
   * @param target the specified component being laid out.
   * @see Container
   */
  public void layoutContainer(Container target)
  {
    synchronized (target.getTreeLock())
    {
      Insets insets = target.getInsets();
      int maxheight = target.getSize().height - (insets.top + insets.bottom + _vgap * 2);
      int maxwidth = target.getSize().width - (insets.left + insets.right + _hgap * 2);
      int nmembers = target.getComponentCount();
      int x = 0, y = insets.top + _vgap;
      int rowh = 0, start = 0;

      boolean ltr = target.getComponentOrientation().isLeftToRight();

      for (int i = 0 ; i < nmembers ; i++)
      {
        Component m = target.getComponent(i);
        if (m.isVisible())
        {
          Dimension d = m.getPreferredSize();
          m.setSize(d.width, d.height);

          if ((x == 0) || ((x + d.width) <= maxwidth))
          {
            if (x > 0)
            {
              x += _hgap;
            }
            x += d.width;
            rowh = Math.max(rowh, d.height);
          }
          else
          {
            moveComponents(target, insets.left + _hgap, y, maxwidth - x, rowh, start, i, ltr);
            x = d.width;
            y += _vgap + rowh;
            rowh = d.height;
            start = i;
          }
        }
      }  // end for
      moveComponents(target, insets.left + _hgap, y, maxwidth - x, rowh, start, nmembers, ltr);
    }  // end synchronized
  }

  /**
   * Returns a string representation of this VFlowLayout object and its values.
   *
   * @return a string representation of this layout.
   */
  public String toString()
  {
    String str = "";
    switch (_valign)
    {
      case LEFT:     str = ",valign=left"; break;
      case CENTER:   str = ",valign=center"; break;
      case RIGHT:    str = ",valign=right"; break;
    }
    switch (_halign)
    {
      case TOP:      str = str + ",halign=top"; break;
      case MIDDLE:   str = str + ",halign=middle"; break;
      case BOTTOM:   str = str + ",halign=bottom"; break;
    }
    return getClass().getName() + "[hgap=" + _hgap + ",vgap=" + _vgap + str + "]";
  }
}
