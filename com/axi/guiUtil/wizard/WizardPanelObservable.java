package com.axi.guiUtil.wizard;

import java.util.*;
import com.axi.util.*;

/**
 * <p>Title: WizardPanelObservable</p>
 *
 * <p>Description: This observable is required for the wizard template. This observable will notify the WizardDialog
 * that the button state needs to be updated. The user of this class will required to supply the state of the buttons
 * so the Wizard Dialog can update them accordingly.</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Erica Wheatcroft
 */
public class WizardPanelObservable extends Observable
{
  private static WizardPanelObservable _instance = null;

  /**
   * @author Erica Wheatcroft
   */
  private WizardPanelObservable()
  {
     // do nothing
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized static WizardPanelObservable getInstance()
  {
    if(_instance == null)
      _instance = new WizardPanelObservable();

    return _instance;
  }

  /**
   * @param buttonState the state of the buttons on the wizard dialog
   * @author Erica Wheatcroft
   */
  public void updateButtonState(WizardButtonState buttonState)
  {
    Assert.expect(buttonState != null, "buttonState is null");
    setChanged();
    notifyObservers(buttonState);
  }

}
