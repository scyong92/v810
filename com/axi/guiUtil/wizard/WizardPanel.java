package com.axi.guiUtil.wizard;

import javax.swing.*;

import com.axi.util.*;

/**
 * <p>Title: WizardPanel</p>
 *
 * <p>Description: This panel is the base panel for all content panels of the wizard dialog. All panels must extend off
 * of this panel. Extended panels must implement the validate method as well as the populate panel with data method.
 * The validate method must use the WizardPanelObservable to communicate back to the Wizard Dialog. </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Erica Wheatcroft
 */
public abstract class WizardPanel extends JPanel
{
  // this is not a user visible string so therefore it does not have to be localized.
  protected String _name = null;
  protected WizardPanelObservable _observable = null;
  protected WizardDialog _dialog = null;

  // abstract methods
  public abstract void validatePanel();
  public abstract void populatePanelWithData();
  public abstract void unpopulate();
  public abstract void setWizardName(String name);
  public abstract String getWizardName();
  public abstract void removeObservers();

  /**
   * @author Erica Wheatcroft
   */
  public WizardPanel(WizardDialog dialog)
  {
    Assert.expect(dialog != null, "dialog is null");
    // set up the observable.
    _dialog = dialog;
    _observable = WizardPanelObservable.getInstance();
  }
}
