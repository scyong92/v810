package com.axi.guiUtil.wizard;

/**
 * <p>Title: WizardButtonState</p>
 *
 * <p>Description: This class is responsible for holding the state for the buttons displayed in the Wizard Dialog. The
 * four buttons are nextScreen, cancelWizard, completeProcess, and backAScreen. Any implementation of the Wizard framework
 * should use this class to enable and disblae the above mentioned buttons.</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: AXI</p>
 *
 * @author Erica Wheatcroft
 */
public class WizardButtonState
{
  private boolean _nextScreenButtonEnabled = false;
  private boolean _cancelWizardButtonEnabled = false;
  private boolean _completeProcessButtonEnabled = false;
  private boolean _previousScreenButtonEnabled = false;

  /**
   * @author Erica Wheatcroft
   */
  public WizardButtonState(boolean nextScreenButtonEnabled,
                           boolean cancelWizardButtonEnabled,
                           boolean completeProcessButtonEnabled,
                           boolean previousScreenButtonEnabled)
  {
    _nextScreenButtonEnabled = nextScreenButtonEnabled;
    _cancelWizardButtonEnabled = cancelWizardButtonEnabled;
    _completeProcessButtonEnabled = completeProcessButtonEnabled;
    _previousScreenButtonEnabled = previousScreenButtonEnabled;
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isNextScreenButtonEnabled()
  {
    return _nextScreenButtonEnabled;
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isCancelWizardButtonEnabled()
  {
    return _cancelWizardButtonEnabled;
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isCompleteProcessButtonEnabled()
  {
    return _completeProcessButtonEnabled;
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isPreviousScreenButtonEnabled()
  {
    return _previousScreenButtonEnabled;
  }
}

