package com.axi.guiUtil.wizard;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;


/**
 * This is the wizard template that should be used for all wizards. This dialog will provide you with a logo panel,
 * a content panel, and a button panel. All three are customizable. The user of this template must specify a list of
 * wizard panels for the content pane. There must be at least one panel.
 *
 * @author Erica Wheatcroft
 */
public abstract class WizardDialog extends JDialog implements Observer
{
  private JPanel _buttonPanel = new JPanel();
  private JPanel _logoPanel = new JPanel();
  private GridLayout _cancelPanelGridLayout = new GridLayout();
  private GridLayout _backPanelGridLayout = new GridLayout();
  private GridLayout _nextPanelGridLayout = new GridLayout();
  private GridLayout _generatePanelGridLayout = new GridLayout();
  private GridLayout _logoPanelGridLayout = new GridLayout();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private FlowLayout _buttonPanelFlowLayout = new FlowLayout();
  private JPanel _processCompletePanel = new JPanel();
  private JPanel _nextPanel = new JPanel();
  private JPanel _backPanel = new JPanel();
  private JPanel _cancelButtonPanel = new JPanel();
  private JButton _cancelButton = new JButton();
  private JButton _backButton = new JButton();
  private JButton _nextButton = new JButton();
  private JButton _processCompleteButton = new JButton();

  // button names..
  private String _nextScreenButtonText = null;
  private char _nextScreenMnemonicChar = 'N';
  private String _cancelWizardButtonText = null;
  private char _cancelWizardMnemonicChar = 'C';
  private String _completeProcessButtonText = null;
  private char _completeProcessMnemonicChar = 'F';
  private String _previousScreenButtonText = null;
  private char _previousScreenMnemonicChar = 'B';

  /** @todo add something to hold for the image logo */

  // protected members
  protected JPanel _contentPanel = new JPanel();
  // the content panes that will be flipped through during the wizard process
  protected java.util.List<WizardPanel> _contentPanels = null;

  // the wizard logo.
  private static ImageIcon _wizardLogo = com.axi.v810.images.Image5DX.getImageIcon(com.axi.v810.images.Image5DX.AXI_X6000_WIZARD);


  // abstract methods
  protected abstract void handleNextScreenButtonEvent();
  protected abstract void handlePreviousScreenButtonEvent();
  protected abstract void handleCompleteProcessButtonEvent();
  protected abstract void confirmExitOfDialog();

  /**
   * @author Erica Wheatcroft
   */
  public WizardDialog(Frame parent,
                      boolean modal)
  {
    super(parent, modal);
    initWizard();
  }

  /**
   * @author Erica Wheatcroft
   */
  public WizardDialog(Frame parent,
                      boolean modal,
                      String nextScreenButtonText,
                      String cancelWizardButtonText,
                      String completeProcessButtonText,
                      String previousScreenButtonText)
  {
    super(parent, modal);

    // set the text for the buttons if null ignore and use the default
    if (nextScreenButtonText != null)
      _nextScreenButtonText = nextScreenButtonText;
    if (cancelWizardButtonText != null)
      _cancelWizardButtonText = cancelWizardButtonText;
    if (completeProcessButtonText != null)
      _completeProcessButtonText = completeProcessButtonText;
    if (previousScreenButtonText != null)
      _previousScreenButtonText = previousScreenButtonText;

    initWizard();
  }

  /**
   * @author Erica Wheatcroft
   */
  public WizardDialog(Frame parent,
                      boolean modal,
                      java.util.List<WizardPanel> panels,
                      String nextScreenButtonText,
                      String cancelWizardButtonText,
                      String completeProcessButtonText,
                      String previousScreenButtonText)
  {
    super(parent, modal);

    Assert.expect(panels != null, "Content Panels are null"); // it can't be null
    Assert.expect(panels.isEmpty() ==false, "list is empty"); // it must have atleast one panel

    _contentPanels = panels;

    // set the text for the buttons if null ignore and use the default
    if(nextScreenButtonText != null)
      _nextScreenButtonText = nextScreenButtonText;
    if(cancelWizardButtonText != null)
      _cancelWizardButtonText = cancelWizardButtonText;
    if(completeProcessButtonText != null)
      _completeProcessButtonText = completeProcessButtonText;
    if(previousScreenButtonText != null)
      _previousScreenButtonText = previousScreenButtonText;

    initWizard();
    setupContentPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setContentPanels(java.util.List<WizardPanel> panels)
  {
    Assert.expect(panels != null, "Content Panels are null"); // it can't be null
    Assert.expect(panels.isEmpty() == false, "list is empty"); // it must have atleast one panel

    _contentPanels = panels;

    setupContentPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void dispose()
  {
    // lets iterate through all the wizard panels and call remove observers
    for(WizardPanel contentPanel : _contentPanels)
    {
      contentPanel.removeObservers();
      contentPanel.unpopulate();
      contentPanel = null;
    }
    WizardPanelObservable.getInstance().deleteObserver(this);
//    InspectionEventObservable.getInstance().deleteObserver(this);
    super.dispose();
  }


  /**
   * This method will init the wizard dialog
   * @author Erica Wheatcroft
   */
  private void initWizard()
  {
    jbinit(); // init the components
    // set the wizard up to observe the WizardPanelObservable
    WizardPanelObservable.getInstance().addObserver(this);
  }

  /**
   * init the GUI components
   * @author Erica Wheatcroft
   */
  private void jbinit()
  {
    getContentPane().setLayout(_mainPanelBorderLayout);
    _buttonPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _cancelButtonPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 40));
    _processCompletePanel.setBorder(BorderFactory.createEmptyBorder(0, 40, 0, 0));
    getContentPane().add(_contentPanel, java.awt.BorderLayout.CENTER);
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    getContentPane().add(_buttonPanel, java.awt.BorderLayout.SOUTH);
    _cancelButtonPanel.setLayout(_cancelPanelGridLayout);
    _backPanel.setLayout(_backPanelGridLayout);
    _nextPanel.setLayout(_nextPanelGridLayout);
    _processCompletePanel.setLayout(_generatePanelGridLayout);
    _contentPanel.setLayout(new CardLayout());
    _buttonPanel.setBorder(BorderFactory.createEtchedBorder());
    _logoPanel.setBorder(BorderFactory.createEtchedBorder());
    _logoPanel.setLayout(_logoPanelGridLayout);
    JLabel logoLabel = new JLabel();
    logoLabel.setIcon(_wizardLogo);
    _logoPanel.add(logoLabel);
    _buttonPanel.add(_cancelButtonPanel);
    _cancelButtonPanel.add(_cancelButton);
    _buttonPanel.add(_backPanel);
    _backPanel.add(_backButton);
    _buttonPanel.add(_nextPanel);
    _nextPanel.add(_nextButton);
    _buttonPanel.add(_processCompletePanel);
    _processCompletePanel.add(_processCompleteButton);
    getContentPane().add(_logoPanel, java.awt.BorderLayout.WEST);

//    setResizable(false);
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

    if(_cancelWizardButtonText != null)
      _cancelButton.setText(_cancelWizardButtonText);
    else
    {
      _cancelButton.setText("Cancel");
      _cancelButton.setMnemonic(_cancelWizardMnemonicChar);
    }

    if(_previousScreenButtonText != null)
      _backButton.setText(_previousScreenButtonText);
    else
    {
      _backButton.setText("< Back");
      _backButton.setMnemonic(_previousScreenMnemonicChar);
    }

    if(_nextScreenButtonText != null)
      _nextButton.setText(_nextScreenButtonText);
    else
    {
      _nextButton.setText("Next >");
      _nextButton.setMnemonic(_nextScreenMnemonicChar);
    }

    if(_completeProcessButtonText != null)
      _processCompleteButton.setText(_completeProcessButtonText);
    else
    {
      _processCompleteButton.setText("Finish");
      _processCompleteButton.setMnemonic(_completeProcessMnemonicChar);
    }

    // default button state.
    _backButton.setEnabled(false);
    _cancelButton.setEnabled(true);
    _processCompleteButton.setEnabled(false);
    _nextButton.setEnabled(false);

    // listeners
    _nextButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        handleNextScreenButtonEvent();
      }
    });
    _backButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        handlePreviousScreenButtonEvent();
      }
    });
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        confirmExitOfDialog();
      }
    });
    _processCompleteButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        handleCompleteProcessButtonEvent();
      }
    });
  }



  /**
   * this method will handle update calls from the WizardPanelObservable.
   *
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof WizardPanelObservable)
        {
          Assert.expect(object instanceof WizardButtonState, "Object not of type wizard button state");
          WizardButtonState buttonState = (WizardButtonState)object;
          _backButton.setEnabled(buttonState.isPreviousScreenButtonEnabled());
          _cancelButton.setEnabled(buttonState.isCancelWizardButtonEnabled());
          _processCompleteButton.setEnabled(buttonState.isCompleteProcessButtonEnabled());
          _nextButton.setEnabled(buttonState.isNextScreenButtonEnabled());
        }
      }
    });
  }

  /**
   * this method will setup the content panel for the wizard.
   * @author Erica Wheatcroft
   */
  private void setupContentPanel()
  {
    Assert.expect( _contentPanels != null, "content panels are null");

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        // remove all the items
        _contentPanel.removeAll();

        for (WizardPanel panel : _contentPanels)
          _contentPanel.add(panel, panel.getWizardName());

        pack();
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
      confirmExitOfDialog();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void changeTitle(final String title)
  {
    if(title == null)
      return;

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        setTitle(title);
      }
    });
  }
}
