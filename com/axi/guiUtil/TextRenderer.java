package com.axi.guiUtil;

import java.awt.*;
import java.awt.geom.*;

import com.axi.util.*;

/**
 * This class is responsible for drawing text information on the GraphicsEngine class.
 * The base Renderer can draw text
 * using the Shape class but it has some disadvantages:
 * - representing text as a shape can take substantially more memory
 * - the shape representation of text does not look as good after scale/zoom operations
 *
 * When you override this class your constructor must:
 * - call initializeText()
 *
 * @see GraphicsEngine
 * @see Renderer
 *
 * @author Andy Mechtenberg
 * @author Bill Darbie
 */
public class TextRenderer extends Renderer
{
  private String _text;
  //XCR-3149: Panel image and board width is upside down if panel is loaded from right to left
  private boolean _flipped = false;

  /**
   * @author Bill Darbie
   */
  public TextRenderer(boolean isSelectableComponent)
  {
    super(isSelectableComponent);
    // set a default font
    setFont(new Font("Sansserif", Font.PLAIN, 16));
  }
  
  /**
   * @author Siew Yeng
   */
  public TextRenderer(boolean isSelectableComponent, Font font)
  {
    super(isSelectableComponent);

    setFont(font);
  }

  /**
   * Call this method in your derived classes contructor to set up this Renderer
   * @author Bill Darbie
   */
  public void initializeText(String text, double centerX, double centerY, boolean centerTextAboutCoordinates)
  {
    Assert.expect(text != null);
    _text = text;

    FontMetrics fontMetrics = getFontMetrics(getFont());
    _widthInPixels = fontMetrics.stringWidth(text);

    _heightInPixels = fontMetrics.getMaxAscent() + fontMetrics.getMaxDescent();

    _xInPixels = centerX;
    _yInPixels = centerY;

    if(centerTextAboutCoordinates)
    {
      _xInPixels -= _widthInPixels / 2.0;
      _yInPixels -= _heightInPixels / 2.0;
    }
    // set the bounds of the component so mouse clicks will be seen correctly
    setBounds();
  }

  /**
   * This method needs to overridden to get the most recent data required
   * to draw the appropriate Shape.  Your method needs to call initializeText()
   * with the new text information.
   * @author Bill Darbie
   * @author Patrick Lacz
   */
  protected void refreshData()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  public String getText()
  {
    Assert.expect(_text != null);
    return _text;
  }

  /**
   * @author Patrick Lacz
   */
  public Rectangle2D getTextBounds()
  {
    return new Rectangle2D.Double(_xInPixels, _yInPixels, _widthInPixels, _heightInPixels);
  }

  /**
   * Apply the passed in transform to this Renderer.
   * @author Bill Darbie
   */
  protected void applyTransform(AffineTransform transform)
  {
    Assert.expect(transform != null);

    Shape shape = new Rectangle2D.Double(_xInPixels, _yInPixels, _widthInPixels, _heightInPixels);
    shape = transform.createTransformedShape(shape);
    Rectangle2D bounds = shape.getBounds2D();
    _xInPixels = bounds.getX();
    _yInPixels = bounds.getY();

    double scaleX = transform.getScaleX();
    double scaleY = transform.getScaleY();

    // on a rotate the scaleX and scaleY are 0
    if (scaleX == 0)
    {
      _widthInPixels = bounds.getWidth();
    }
    if (scaleY == 0)
    {
      _heightInPixels = bounds.getHeight();
    }

    double centerX = bounds.getCenterX();
    double centerY = bounds.getCenterY();

    _xInPixels = centerX - _widthInPixels / 2.0;
    _yInPixels = centerY - _heightInPixels / 2.0;

    setBounds();
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void paintComponent(Graphics graphics)
  {
    Font origGraphicsFont = graphics.getFont();
    Color origColor = graphics.getColor();
    Font rendererFont = getFont();
    FontMetrics fontMetrics = getFontMetrics(rendererFont);
    graphics.setFont(rendererFont);
    Assert.expect(_text != null);
    // the Height is the max height needed to display any text in this font (ascent + descent + 1)
    // basically, the height of the bounding rectangle.
    // DrawString, however, needs the X, Y of the leftmost BASELINE point, so we need to subtract the max descent
    // from this Y otherwise any character below the baseline will be cut off.
    // we subtract another one, to better center it, and the height stuff added one as well. (Which is doesn't need to
    // do for text, but is needed for other shapes)
    
    //XCR-3149: Panel image and board width is upside down if panel is loaded from right to left
    if (_flipped)
    {
      int xCenter = (getWidth() / 2);
      int yCenter = (getHeight() / 2);
      Graphics2D g2d = (Graphics2D)graphics;
      g2d.rotate(Math.toRadians(180.0), xCenter, yCenter);
    }
    graphics.drawString(_text, 0, getHeight() - fontMetrics.getMaxDescent() - 1);
    graphics.setFont(origGraphicsFont);
    graphics.setColor(origColor);
  }

  /**
   * @author hee-jihn.chuah - XCR-3149: Panel image and board width is upside down if panel is loaded from right to left
   */
  public void setFlipped(boolean flipped)
  {
    _flipped = flipped;
  }
}

