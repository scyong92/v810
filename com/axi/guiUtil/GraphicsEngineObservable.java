package com.axi.guiUtil;

import com.axi.util.*;
import java.util.*;

/**
 * This class is used by the GraphicsEngine class and will call any
 * Observers update method with the coordinates of a left mouse click
 *
 * @author Bill Darbie
 */
public class GraphicsEngineObservable extends Observable
{
  /**
   * @author Bill Darbie
   */
  void notifyGraphicsEvent(GraphicsEngineEvent graphicsEngineEvent)
  {
    Assert.expect(graphicsEngineEvent != null);
    setChanged();
    notifyObservers(graphicsEngineEvent);
  }
}
