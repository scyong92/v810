package com.axi.guiUtil;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;

/**
 * <p>Title: ColumnResizer</p>
 *
 * <p>Description: This class will take the given jtable reference and adjust the column sizes to suit their contents.
 * Idea was taken from Swing Hacks book </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Erica Wheatcroft
 */
public class ColumnResizer
{
  /**
   * @param table JTable the table to update the column widths for
   * @param accountForHeaderCells boolean to indicate if the method should account for header cells. if true the width
   * will not be less than the column header text.
   * @author Erica Wheatcroft
   */
  public static void adjustColumnPreferredWidths(JTable table, boolean accountForHeaderCells)
  {
    Assert.expect(table != null, "Table is null");

    // get max width for cells in column and make that the perferred width
    TableColumnModel columnModel = table.getColumnModel();
    for(int col = 0; col < table.getColumnCount(); col++)
    {
      int maxWidth = 0;
      for(int row = 0; row < table.getRowCount(); row++)
      {

        TableCellRenderer renderer = table.getCellRenderer(row, col);
        Object value = table.getValueAt(row, col);
        Component component = renderer.getTableCellRendererComponent(table,
                                                                     value,
                                                                     false,
                                                                     false,
                                                                     row,
                                                                     col);
        maxWidth = Math.max(component.getPreferredSize().width, maxWidth);

      } // end of row iteration

      if(accountForHeaderCells)
      {
        // we care about the column header so lets take that into account
        TableColumn column = columnModel.getColumn(col);
        TableCellRenderer headerRenderer = column.getHeaderRenderer();
        if(headerRenderer == null)
          headerRenderer = table.getTableHeader().getDefaultRenderer();
        Object headerValue = column.getHeaderValue();
        Component headerComponent = headerRenderer.getTableCellRendererComponent(table,
                                                                                 headerValue,
                                                                                 false,
                                                                                 false,
                                                                                 0,
                                                                                 col);
        maxWidth = Math.max(maxWidth,
                            headerComponent.getPreferredSize().width);
        column.setPreferredWidth(maxWidth);
      }
      else
      {
        // we don't care about the column header ..
        TableColumn column = columnModel.getColumn(col);
        column.setPreferredWidth(maxWidth);
      }
    } // end of column interation
  }
}
