package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

//import com.axi.mtd.agt5dx.util.*;
import com.axi.util.*;

/**
 * This dialog is displayed while a long task is executing. It has a cancel
 * button to allow the user to abort the task.
 * @author Steve Anonson
 */
public class BusyCancelDialog extends JDialog
{
  private static final int _NUM_GIFS = 10;
  private JPanel _mainPanel = new JPanel();
  private BorderLayout _borderLayout = new BorderLayout();
  private JLabel _animatedGifLabel = new JLabel();
  private JLabel _messageLabel = new JLabel();
  private JPanel _centerPanel = new JPanel();
  private BorderLayout _centerPanelLayout = new BorderLayout();
  private JPanel _buttonPanel = new JPanel();
  private JButton _cancelButton = new JButton();
  private boolean _displayCancelButton = true;
  private String _message = "";
  private String _cancelButtonText = "";
  private Icon _icon;
  private ActionListener _cancelActionListener = null;

  /**
   * For use with JBuilder GUI designer ONLY!  That's why it's private.
   * @author Andy Mechtenberg
   */
  private BusyCancelDialog()
  {
    try
    {
      _message = "Waiting for Barcode Reader";
      jbInit();
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }
  }
  /**
     * @param   dialog - parent component.
     * @param   message - String to be displayed in the dialog.
     * @param   title - Dialog title string.
     * @param   modal - true for a modal dialog.
     * @author Steve Anonson
     * @author Erica Wheatcroft
     */
    public BusyCancelDialog(JDialog dialog, String message, String title, String cancelButtonText, boolean modal)
    {
      super(dialog, title, modal);
      Assert.expect(message != null);
      Assert.expect(cancelButtonText != null);
      _message = message;
      _cancelButtonText = cancelButtonText;
      _displayCancelButton = true;
      jbInit();
      pack();
    }

    /**
     * This version drops the Cancel Button (not displayed).  I guess it's properly called a
     * BusyDialog, but we can reuse much of the BusyCancelDialog to get this.
     * @param   dialog - parent component.
     * @param   message - String to be displayed in the dialog.
     * @param   modal - true for a modal dialog.
     * @author Steve Anonson
     * @author Erica Wheatcroft
     */
    public BusyCancelDialog(JDialog dialog, String message, String title, boolean modal)
    {
      super(dialog, title, modal);
      Assert.expect(message != null);
      _message = message;
      _cancelButtonText = "";
      _displayCancelButton = false;
      jbInit();
      pack();
  }

  /**
   * @param   frame - parent component.
   * @param   message - String to be displayed in the dialog.
   * @param   title - Dialog title string.
   * @param   modal - true for a modal dialog.
   * @author Steve Anonson
   */
  public BusyCancelDialog(Frame frame, String message, String title, String cancelButtonText, boolean modal)
  {
    super(frame, title, modal);
    Assert.expect(message != null);
    Assert.expect(cancelButtonText != null);
    _message = message;
    _cancelButtonText = cancelButtonText;
    _displayCancelButton = true;
    jbInit();
    pack();
  }

  /**
   * This version drops the Cancel Button (not displayed).  I guess it's properly called a
   * BusyDialog, but we can reuse much of the BusyCancelDialog to get this.
   * @param   frame - parent component.
   * @param   message - String to be displayed in the dialog.
   * @param   modal - true for a modal dialog.
   * @author Steve Anonson
   */
  public BusyCancelDialog(Frame frame, String message, String title, boolean modal)
  {
    super(frame, title, modal);
    Assert.expect(message != null);
    _message = message;
    _cancelButtonText = "";
    _displayCancelButton = false;
    jbInit();
    pack();
  }

  /**
   * Overriding this to call dispose when visible is false
   * @author Andy Mechtenberg
   */
  public void setVisible(boolean visible)
  {
    if (visible == false)
      dispose();
    else
      super.setVisible(visible);
  }

  /**
   * Create the components.
   * @author Steve Anonson
   */
  void jbInit()
  {
    setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    _cancelButton.setText(_cancelButtonText);
    if (_displayCancelButton)
    {
      _buttonPanel.add(_cancelButton, null);

      _cancelButton.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Assert.expect(_cancelActionListener != null);
          _cancelButton.setEnabled(false);
          _cancelActionListener.actionPerformed(e);
        }
      });
    }

    _messageLabel.setText(_message);

    _icon = new ImageIcon(com.axi.guiUtil.BusyCancelDialog.class.getResource("animatedHourglass.gif"));
    _animatedGifLabel.setIcon(_icon);
    _centerPanelLayout.setHgap(10);
    _centerPanel.setLayout(_centerPanelLayout);
    _centerPanel.add(_animatedGifLabel, BorderLayout.WEST);
    _borderLayout.setVgap(5);
    _mainPanel.setLayout(_borderLayout);
    _mainPanel.setBorder(BorderFactory.createEmptyBorder(10,10,0,10));
    _mainPanel.add(_centerPanel, BorderLayout.CENTER);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    getContentPane().add(_mainPanel);
    _centerPanel.add(_messageLabel, java.awt.BorderLayout.CENTER);
  }

  /**
   * Add an ActionListener to the cancel button.  Adding a listener
   * is the only way the parent compnent will know that the button is pressed.
   * The Escape key is also tied to this ActionListener so that hitting Escape
   * is the same as pressing the cancel button.
   * @param   listener - ActionListener to attach to the cancel button.
   * @author Steve Anonson
   */
  public void addCancelActionListener(ActionListener listener)
  {
    Assert.expect(listener != null);
    _cancelActionListener = listener;
//    _cancelButton.addActionListener(listener);
    _cancelButton.registerKeyboardAction(listener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false), JComponent.WHEN_IN_FOCUSED_WINDOW);
  }

  /**
   * Remove an ActionListener from the cancel button.
   * @param   listener - ActionListener to remove from the cancel button.
   * @author Steve Anonson
   */
  public void removeCancelActionListener(ActionListener listener)
  {
    if (listener != null)
    {
      _cancelButton.removeActionListener(listener);
      _cancelButton.unregisterKeyboardAction(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true));
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void enableCancelButton()
  {
    _cancelButton.setEnabled(true);
  }
}
