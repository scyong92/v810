package com.axi.guiUtil;

import java.awt.geom.Rectangle2D;
import com.axi.util.Assert;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.image.BufferedImage;
import com.axi.guiUtil.*;
import java.awt.Shape;
import java.awt.Font;
import java.awt.Rectangle;

/**
 * Manual test class for the Renderer base class.
 * This is used by the TestMan_GraphicsEngine test.
 *
 * @author Kay Lannen
 */
public class TestMan_Renderer
{
  public TestMan_Renderer()
  {
    // do nothing
  }

  public static ShapeRenderer createRectangleRenderer(Rectangle2D rect, boolean selectable)
  {
    RectangleRenderer rectRenderer = new RectangleRenderer(rect, selectable);
    return (rectRenderer);
  }

  public static ShapeRenderer createElRenderer(int x, int y, int width, int height, boolean selectable)
  {
    ElRenderer elRenderer = new ElRenderer(x, y, width, height, selectable);
    return (elRenderer);
  }

  public static BufferedImageRenderer createSimpleBufferedImageRenderer(BufferedImage buffImage, double x, double y, double width,
      double height,
      boolean selectable)
  {
    SimpleBufferedImageRenderer buffImgRenderer = new SimpleBufferedImageRenderer(buffImage, x, y, width, height);
    return (buffImgRenderer);
  }

  public static TextRenderer createSimpleTextRenderer(String text, Rectangle rect)
  {
    SimpleTextRenderer textRenderer = new SimpleTextRenderer(text, rect);
    return (textRenderer);
  }
}

class ElRenderer extends ShapeRenderer
{
  Polygon _elPolygon;

  public ElRenderer(int x, int y, int width, int height, boolean selectable)
  {
    super(selectable);
    _elPolygon = new Polygon();
    int strokeWidth;
    if (width < height)
    {
      strokeWidth = width / 5;
    }
    else
    {
      strokeWidth = height / 5;
    }

    _elPolygon.addPoint(x, y);
    _elPolygon.addPoint(x + width, y);
    _elPolygon.addPoint(x + width, y + strokeWidth);
    _elPolygon.addPoint(x + strokeWidth, y + strokeWidth);
    _elPolygon.addPoint(x + strokeWidth, y + height);
    _elPolygon.addPoint(x, y + height);
    refreshData();
  }

  protected void refreshData()
  {
    Assert.expect(_elPolygon != null);
    initializeShape(_elPolygon);
  }

  public void paintComponent(Graphics graphics)
  {
    super.paintComponent(graphics);
  }

}


class SimpleBufferedImageRenderer extends BufferedImageRenderer
{
  BufferedImage _buffImage;
  double _x;
  double _y;
  double _width;
  double _height;

  public SimpleBufferedImageRenderer(BufferedImage buffImage, double x, double y, double width, double height)
  {
    super();
    _buffImage = buffImage;
    _x = x;
    _y = y;
    _width = width;
    _height = height;
    refreshData();
  }

  protected void refreshData()
  {
    initializeImage(_buffImage, _x, _y, _width, _height);
  }
}


class SimpleTextRenderer extends TextRenderer
{
  String _text;
  Rectangle _rect;

  SimpleTextRenderer(String text, Rectangle rect)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(text != null);
    _text = text;
    _rect = rect;

    // set a default font
    setFont(new Font("Sansserif", Font.BOLD, 16));

    refreshData();
  }

  /**
   * @author Bill Darbie
   */
  protected void refreshData()
  {
    Rectangle2D bounds = _rect.getBounds2D();
    initializeText(_text, bounds.getCenterX(), bounds.getCenterY(), true);
  }
}
