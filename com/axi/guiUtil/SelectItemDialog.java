package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.util.*;

/**
 * Creates a modal dialog to capture a user entered (via keyboard) number as a double
 * @author Andy Mechtenberg
 */
public class SelectItemDialog extends EscapeDialog
{
  // parent
  private JFrame _frame;
  // either OK or CANCEL depending on how the user exits
  private int _returnValue;

  private Object _selectedValue = null;
  private String _renamedValue = null;
  private String _instruction;
  private String _okButtonText;
  private String _cancelButtonText;
  private String _additionTextFieldLabelText = null;
  private boolean _createTextField = false;
  private boolean _createPossibleValue = false;
  private boolean _createPossibleValue2 = false;
  private boolean _createAdditionalCombobox = false;
  private boolean _createAdditionalCombobox3 = false;
  private java.util.List<Object> _additionalComboBoxItemsList;
  private java.util.List<Object> _additionalComboBox2ItemsList;
  private java.util.List<Object> _additionalComboBox3ItemsList;
  private JLabel _additionalComboBoxLabel;
  private JLabel _additionalComboBox2Label;
  private JLabel _additionalComboBox3Label;
  private Object _selectedComboBoxItem;
  private Object _selectedComboBox2Item;
  private Object _selectedComboBox3Item;
  private Object _itemToSelectInComboBox;
  private Object _itemToSelectInComboBox2;
  private Object _itemToSelectInComboBox3;

  private Font _buttonFont;
  private Font _messageFont;

  private JPanel _mainPanel = new JPanel();
  private JPanel _messagePanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();

  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private FlowLayout _messagePanelFlowLayout = new FlowLayout();
  private FlowLayout _buttonPanelFlowLayout = new FlowLayout();
  private GridLayout _innerButtonPanelGridLayout = new GridLayout();
  private JLabel _messageLabel = new JLabel();
  private JPanel _inputPanel = new JPanel();
  private DefaultListModel _choicesListModel = new DefaultListModel();
  private DefaultListModel _choicesListModel2 = new DefaultListModel();
  private JScrollPane _choicesScrollPane = new JScrollPane();
  private BorderLayout _inputPanelBorderLayout = new BorderLayout();
  private Border _innerButtonPanelBorder = BorderFactory.createEmptyBorder(5, 10, 0, 10);
  private Border _mainPanelBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
  private JPanel _additionalControlsPanel = new JPanel();
  private BorderLayout _additionalControlsBorderLayout = new BorderLayout();
  private JPanel _additionalControlsInnerPanel = new JPanel();
  private BorderLayout _textFieldBorderLayout = new BorderLayout();
  private JLabel _additionalTextFieldLabel = new JLabel();
   


  private JButton _okButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }

  };
  private JButton _cancelButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  private JList _choicesList = new JList(_choicesListModel)
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  //Jack Hwee - add new choice list for swithching
  private JList _choicesList2 = new JList(_choicesListModel2)
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  private JTextField _additionalTextField = new JTextField()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
      if (e.getKeyCode() == KeyEvent.VK_ENTER)
      {
        okAction();
      }

    }
  };

  private JComboBox _additionalComboBox = new JComboBox()
  {
    public void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };
  
  private JComboBox _additionalComboBox2 = new JComboBox()
  {
    public void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  //Jack Hwee - Add combobox to change the choice list
  private JComboBox _additionalComboBox3 = new JComboBox()
  {
    public void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };
  


  /**
   * @author Erica Wheatcroft
   */
  private SelectItemDialog()
  {
    // do nothing
  }

  /**
   * @author Andy Mechtenberg
   */
  public SelectItemDialog(JFrame frame,
                          String title,
                          String message,
                          String okButtonText,
                          String cancelButtonText,
                          Object[] possibleValues,
                          Object selectedValue)
  {
    super(frame, title, true);

    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(message != null);
    Assert.expect(okButtonText != null);
    Assert.expect(cancelButtonText != null);
    Assert.expect(possibleValues != null);

    _frame = frame;
    _instruction = message;
    _okButtonText = okButtonText;
    _cancelButtonText = cancelButtonText;
    _buttonFont = FontUtil.getDialogButtonFont();
    _messageFont = FontUtil.getDialogMessageFont();
    _okButton.setEnabled(possibleValues.length > 0);
    _createPossibleValue = true;
    if(possibleValues.length > 0)
    {

      for (int i = 0; i < possibleValues.length; i++)
        _choicesListModel.addElement(possibleValues[i]);
    }

    if(selectedValue != null)
    {
      int index = _choicesListModel.indexOf(selectedValue);
      if(index != -1)
        _choicesList.setSelectedIndex(index);
    }

    createDialog();
    pack();

  }

  /**
    * @author Andy Mechtenberg
    */
   public SelectItemDialog(JFrame frame,
                           String title,
                           String message,
                           String okButtonText,
                           String cancelButtonText,
                           Object[] possibleValues,
                           String additionalTextFieldLabel)
   {
     super(frame, title, true);

     Assert.expect(frame != null);
     Assert.expect(title != null);
     Assert.expect(message != null);
     Assert.expect(okButtonText != null);
     Assert.expect(cancelButtonText != null);
     Assert.expect(possibleValues != null);

     _createPossibleValue = true;
     if(additionalTextFieldLabel != null)
       _createTextField = true;
     _additionTextFieldLabelText = additionalTextFieldLabel;

     _frame = frame;
     _instruction = message;
     _okButtonText = okButtonText;
     _cancelButtonText = cancelButtonText;
     _buttonFont = FontUtil.getDialogButtonFont();
     _messageFont = FontUtil.getDialogMessageFont();
     _okButton.setEnabled(possibleValues.length > 0);

     for (int i = 0; i < possibleValues.length; i++)
       _choicesListModel.addElement(possibleValues[i]);

     _choicesList.setSelectedIndex(0);

     createDialog();
     pack();

  }

  /**
   * This constructor allows the dialog to also show a combobox of choices
   * @author Jack Hwee
   */
  public SelectItemDialog(JFrame frame,
    String title,
    String message,
    String okButtonText,
    String cancelButtonText,
    Object[] possibleValues,
    Object[] possibleValues2,
    String additionalTextFieldLabel,
    Object[] additionalComboBoxItemsList,
    String additionalComboBoxLabel,
    Object itemToSelectInComboBox,
    Object[] additionalComboBox2ItemsList,
    String additionalComboBox2Label,
    Object itemToSelectInComboBox2,
    Object[] additionalComboBox3ItemsList,
    String additionalComboBox3Label,
    Object itemToSelectInComboBox3)
   {
     super(frame, title, true);

     Assert.expect(frame != null);
     Assert.expect(title != null);
     Assert.expect(message != null);
     Assert.expect(okButtonText != null);
     Assert.expect(cancelButtonText != null);
     Assert.expect(possibleValues != null);
     Assert.expect(possibleValues2 != null);
     Assert.expect(additionalComboBoxItemsList != null);
     Assert.expect(additionalComboBoxLabel != null);
     Assert.expect(itemToSelectInComboBox != null);
     Assert.expect(additionalComboBox3ItemsList != null);
     Assert.expect(additionalComboBox3Label != null);
     Assert.expect(itemToSelectInComboBox3 != null);

     if(additionalTextFieldLabel != null)
       _createTextField = true;

    if(possibleValues != null)
       _createPossibleValue = true;

     if(possibleValues2 != null)
       _createPossibleValue2 = true;

     _additionTextFieldLabelText = additionalTextFieldLabel;

     _additionalComboBoxLabel = new JLabel(additionalComboBoxLabel);
     _additionalComboBoxItemsList = new java.util.ArrayList<Object>();
     for (int i = 0; i < additionalComboBoxItemsList.length; i++)
       _additionalComboBoxItemsList.add(additionalComboBoxItemsList[i]);
     _itemToSelectInComboBox = itemToSelectInComboBox;
     
     _additionalComboBox2Label = new JLabel(additionalComboBox2Label);
     _additionalComboBox2ItemsList = new java.util.ArrayList<Object>();
     for (int i = 0; i < additionalComboBox2ItemsList.length; i++)
       _additionalComboBox2ItemsList.add(additionalComboBox2ItemsList[i]);
     _itemToSelectInComboBox2 = itemToSelectInComboBox2;

     _createAdditionalCombobox3 = true;

     _additionalComboBox3Label = new JLabel(additionalComboBox3Label);
     _additionalComboBox3ItemsList = new java.util.ArrayList<Object>();
     for (int i = 0; i < additionalComboBox3ItemsList.length; i++)
       _additionalComboBox3ItemsList.add(additionalComboBox3ItemsList[i]);
     _itemToSelectInComboBox3 = itemToSelectInComboBox3;

     _frame = frame;
     _instruction = message;
     _okButtonText = okButtonText;
     _cancelButtonText = cancelButtonText;
     _buttonFont = FontUtil.getDialogButtonFont();
     _messageFont = FontUtil.getDialogMessageFont();

     if(_createPossibleValue)
       _okButton.setEnabled(possibleValues.length > 0);
     else
       _okButton.setEnabled(true);


      if(_createPossibleValue2)
       _okButton.setEnabled(possibleValues2.length > 0);
      else
      _okButton.setEnabled(true);
     
     if(_createPossibleValue)
     {
       for (int i = 0; i < possibleValues.length; i++)
         _choicesListModel.addElement(possibleValues[i]);
     }

      if(_createPossibleValue2)
     {
       for (int i = 0; i < possibleValues2.length; i++)
         _choicesListModel2.addElement(possibleValues2[i]);
     }

     _choicesList.setSelectedIndex(0);
     _choicesList2.setSelectedIndex(0);
   

     createDialog();
     pack();

  }


   /**
   * This constructor allows the dialog to also show a combobox of choices
   * @author Laura Cormos
   */
  public SelectItemDialog(JFrame frame,
    String title,
    String message,
    String okButtonText,
    String cancelButtonText,
    Object[] possibleValues,
    String additionalTextFieldLabel,
    Object[] additionalComboBoxItemsList,
    String additionalComboBoxLabel,
    Object itemToSelectInComboBox,
    Object[] additionalComboBox2ItemsList,
    String additionalComboBox2Label,
    Object itemToSelectInComboBox2)
   {
     super(frame, title, true);

     Assert.expect(frame != null);
     Assert.expect(title != null);
     Assert.expect(message != null);
     Assert.expect(okButtonText != null);
     Assert.expect(cancelButtonText != null);
     Assert.expect(possibleValues != null);
     Assert.expect(additionalComboBoxItemsList != null);
     Assert.expect(additionalComboBoxLabel != null);
     Assert.expect(itemToSelectInComboBox != null);

     if(additionalTextFieldLabel != null)
       _createTextField = true;
     if(possibleValues != null)
       _createPossibleValue = true;

     _additionTextFieldLabelText = additionalTextFieldLabel;

     _createAdditionalCombobox = true;
     _additionalComboBoxLabel = new JLabel(additionalComboBoxLabel);
     _additionalComboBoxItemsList = new java.util.ArrayList<Object>();
     for (int i = 0; i < additionalComboBoxItemsList.length; i++)
       _additionalComboBoxItemsList.add(additionalComboBoxItemsList[i]);
     _itemToSelectInComboBox = itemToSelectInComboBox;

     _additionalComboBox2Label = new JLabel(additionalComboBox2Label);
     _additionalComboBox2ItemsList = new java.util.ArrayList<Object>();
     for (int i = 0; i < additionalComboBox2ItemsList.length; i++)
       _additionalComboBox2ItemsList.add(additionalComboBox2ItemsList[i]);
     _itemToSelectInComboBox2 = itemToSelectInComboBox2;


     _frame = frame;
     _instruction = message;
     _okButtonText = okButtonText;
     _cancelButtonText = cancelButtonText;
     _buttonFont = FontUtil.getDialogButtonFont();
     _messageFont = FontUtil.getDialogMessageFont();

     if(_createPossibleValue)
       _okButton.setEnabled(possibleValues.length > 0);
     else
       _okButton.setEnabled(true);

     if(_createPossibleValue)
     {
       for (int i = 0; i < possibleValues.length; i++)
         _choicesListModel.addElement(possibleValues[i]);
     }

     _choicesList.setSelectedIndex(0);

     createDialog();
     pack();

  }


  /**
   * @author Andy Mechtenberg
   * @author Jack Hwee
   */
  private void createDialog()
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _cancelButton.setText(_cancelButtonText);
    _innerButtonPanelGridLayout.setHgap(5);
    _messagePanel.setLayout(_messagePanelFlowLayout);
    _messagePanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _messagePanelFlowLayout.setHgap(0);
    _messagePanelFlowLayout.setVgap(0);
    _cancelButton.setFont(_buttonFont);
    _okButton.setFont(_buttonFont);
    _cancelButton.setMargin(new Insets(2, 8, 2, 8));
    _okButton.setMargin(new Insets(2, 8, 2, 8));
    _messageLabel.setFont(_messageFont);
    _messageLabel.setText(_instruction);
    _inputPanel.setLayout(_inputPanelBorderLayout);
    _buttonPanelFlowLayout.setHgap(0);
    _buttonPanelFlowLayout.setVgap(0);
    _innerButtonPanel.setBorder(_innerButtonPanelBorder);
    _mainPanel.setBorder(_mainPanelBorder);
    _mainPanelBorderLayout.setVgap(5);
    _additionalControlsPanel.setLayout(_additionalControlsBorderLayout);
    _additionalControlsInnerPanel.setLayout(_textFieldBorderLayout);

    getContentPane().add(_mainPanel);
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    _buttonPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _innerButtonPanel.setLayout(_innerButtonPanelGridLayout);
    _okButton.setText(_okButtonText);
    _innerButtonPanel.add(_okButton);
    _innerButtonPanel.add(_cancelButton);
    _additionalControlsPanel.add(_additionalControlsInnerPanel, BorderLayout.NORTH);
    _messagePanel.add(_messageLabel);
    _buttonPanel.add(_innerButtonPanel);
    _mainPanel.add(_inputPanel, BorderLayout.CENTER);

    if(_createPossibleValue)
    {
      _choicesScrollPane.getViewport().add(_choicesList);
      _inputPanel.add(_choicesScrollPane, BorderLayout.CENTER);
      _choicesList.setVisibleRowCount(10);
      _choicesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      _choicesList.addMouseListener(new MouseAdapter()
      {

        public void mousePressed(MouseEvent e)
        {
          // if the user double clicks on a entry, automatically select it and exit the dialog
          if (e.getClickCount() == 1)
          {
            int index = _choicesList.locationToIndex(e.getPoint());
            if (index != -1)
            {
              _selectedValue = _choicesList.getModel().getElementAt(index);
              if (_createTextField)
              {
                if (_selectedValue != null)
                {
                  _additionalTextField.setText(_selectedValue.toString());
                }
              }
            }
          }
          else if (e.getClickCount() == 2 && _choicesListModel.isEmpty() == false)
          {
            int index = _choicesList.locationToIndex(e.getPoint());
            if (index != -1)
            {
              _selectedValue = _choicesList.getModel().getElementAt(index);
              _renamedValue = _selectedValue.toString();
              _returnValue = JOptionPane.OK_OPTION;
              dispose();
            }
          }
        }
      });

      _choicesList.getSelectionModel().addListSelectionListener(new ListSelectionListener()
      {
        // this ensures that a selected item is visible.  At jdk1.5, this is the default behavior, so this could be removed.

        public void valueChanged(ListSelectionEvent e)
        {
          if (e.getValueIsAdjusting() == false && _choicesListModel.isEmpty() == false)
          {
            int index = _choicesList.getSelectedIndex();
            _choicesList.ensureIndexIsVisible(index);
            if (_createTextField)
            {
              _additionalTextField.setText(_choicesList.getSelectedValue().toString());
            }
          }
        }
      });
    }


    if(_createPossibleValue2)
    {
      _choicesScrollPane.getViewport().add(_choicesList);
      _inputPanel.add(_choicesScrollPane, BorderLayout.CENTER);
      _choicesList.setVisibleRowCount(10);
      _choicesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      _choicesList.addMouseListener(new MouseAdapter()
      {

        public void mousePressed(MouseEvent e)
        {
          // if the user double clicks on a entry, automatically select it and exit the dialog
          if (e.getClickCount() == 1)
          {
            int index = _choicesList.locationToIndex(e.getPoint());
            if (index != -1)
            {
              _selectedValue = _choicesList.getModel().getElementAt(index);
              if (_createTextField)
              {
                if (_selectedValue != null)
                {
                  _additionalTextField.setText(_selectedValue.toString());
                }
              }
            }
          }
          else if (e.getClickCount() == 2 && _choicesListModel.isEmpty() == false)
          {
            int index = _choicesList.locationToIndex(e.getPoint());
            if (index != -1)
            {
              _selectedValue = _choicesList.getModel().getElementAt(index);
              _renamedValue = _selectedValue.toString();
              _returnValue = JOptionPane.OK_OPTION;
              dispose();
            }
          }
        }
      });

       //Jack Hwee - add new choice list for swithching
      _choicesList2.setVisibleRowCount(10);
      _choicesList2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      _choicesList2.addMouseListener(new MouseAdapter()
      {

        public void mousePressed(MouseEvent e)
        {
          // if the user double clicks on a entry, automatically select it and exit the dialog
          if (e.getClickCount() == 1)
          {
            int index = _choicesList2.locationToIndex(e.getPoint());
            if (index != -1)
            {
              _selectedValue = _choicesList2.getModel().getElementAt(index);
              if (_createTextField)
              {
                if (_selectedValue != null)
                {
                  _additionalTextField.setText(_selectedValue.toString());
                }
              }
            }
          }
          else if (e.getClickCount() == 2 && _choicesListModel2.isEmpty() == false)
          {
            int index = _choicesList2.locationToIndex(e.getPoint());
            if (index != -1)
            {
              _selectedValue = _choicesList2.getModel().getElementAt(index);
              _renamedValue = _selectedValue.toString();
              _returnValue = JOptionPane.OK_OPTION;
              dispose();
            }
          }
        }
      });

      _choicesList.getSelectionModel().addListSelectionListener(new ListSelectionListener()
      {
        // this ensures that a selected item is visible.  At jdk1.5, this is the default behavior, so this could be removed.

        public void valueChanged(ListSelectionEvent e)
        {
          if (e.getValueIsAdjusting() == false && _choicesListModel.isEmpty() == false)
          {
            int index = _choicesList.getSelectedIndex();
            _choicesList.ensureIndexIsVisible(index);
            if (_createTextField)
            {
              _additionalTextField.setText(_choicesList.getSelectedValue().toString());
            }
          }
        }
      });

      //Jack Hwee - add new choice list for switching
       _choicesList2.getSelectionModel().addListSelectionListener(new ListSelectionListener()
      {
        // this ensures that a selected item is visible.  At jdk1.5, this is the default behavior, so this could be removed.

        public void valueChanged(ListSelectionEvent e)
        {
          if (e.getValueIsAdjusting() == false && _choicesListModel2.isEmpty() == false)
          {
            int index = _choicesList2.getSelectedIndex();
            _choicesList2.ensureIndexIsVisible(index);
            if (_createTextField)
            {
              _additionalTextField.setText(_choicesList2.getSelectedValue().toString());
            }
          }
        }
      });
    }

    _additionalControlsPanel.add(_buttonPanel, BorderLayout.SOUTH);
    _mainPanel.add(_additionalControlsPanel, BorderLayout.SOUTH);
    _mainPanel.add(_messagePanel, BorderLayout.NORTH);

    if(_createTextField)
    {
      _additionalControlsInnerPanel.add(_additionalTextField, BorderLayout.CENTER);
      _additionalControlsInnerPanel.add(_additionalTextFieldLabel, BorderLayout.NORTH);
      if(_choicesList.getSelectedValue() != null)
      _additionalTextField.setText(_choicesList.getSelectedValue().toString());
      _additionalTextFieldLabel.setText(_additionTextFieldLabelText);
      _additionalTextField.addFocusListener(new FocusAdapter()
      {
        public void focusGained(FocusEvent e)
        {
          _additionalTextField.selectAll();
        }
        public void focusLost(FocusEvent e)
        {
          String text = _additionalTextField.getText();
          if(text != null || text.equalsIgnoreCase("") == false)
          {
            _renamedValue = text;
            // now enable the ok button
            _okButton.setEnabled(true);
          }
        }
      });

      _additionalTextField.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          String text = _additionalTextField.getText();
           if(text != null || text.equalsIgnoreCase("") == false)
           {
             _renamedValue = text;
             // now enable the ok button
             _okButton.setEnabled(true);
           }
        }
      });

    }

    if (_createAdditionalCombobox)
    {
      JPanel comboboxPanel = new JPanel(new BorderLayout());
      comboboxPanel.setBorder(BorderFactory.createEmptyBorder(5,0,5,0));
      comboboxPanel.add(_additionalComboBoxLabel, BorderLayout.NORTH);
      comboboxPanel.add(_additionalComboBox, BorderLayout.CENTER);
      
      JPanel combobox2Panel = new JPanel(new BorderLayout());
      combobox2Panel.setBorder(BorderFactory.createEmptyBorder(5,0,5,0));
      combobox2Panel.add(_additionalComboBox2Label, BorderLayout.NORTH);
      combobox2Panel.add(_additionalComboBox2, BorderLayout.CENTER);

      JPanel bothComboboxPanel = new JPanel(new BorderLayout());
      bothComboboxPanel.add(comboboxPanel, BorderLayout.NORTH);
      bothComboboxPanel.add(combobox2Panel, BorderLayout.SOUTH);
      
      _additionalControlsInnerPanel.add(bothComboboxPanel, BorderLayout.SOUTH);

      for (Object item : _additionalComboBoxItemsList)
        _additionalComboBox.addItem(item);
      for (Object item : _additionalComboBox2ItemsList)
        _additionalComboBox2.addItem(item);
      

      _additionalComboBox.addActionListener( new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _selectedComboBoxItem = _additionalComboBox.getSelectedItem();
        }
      });

      _additionalComboBox.setSelectedItem(_itemToSelectInComboBox);
      
      _additionalComboBox2.addActionListener( new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _selectedComboBox2Item = _additionalComboBox2.getSelectedItem();
        }
      });

      _additionalComboBox2.setSelectedItem(_itemToSelectInComboBox2);

    }

    if (_createAdditionalCombobox3)
    {
      JPanel comboboxPanel = new JPanel(new BorderLayout());
      comboboxPanel.setBorder(BorderFactory.createEmptyBorder(5,0,5,0));
      comboboxPanel.add(_additionalComboBoxLabel, BorderLayout.NORTH);
      comboboxPanel.add(_additionalComboBox, BorderLayout.CENTER);

      JPanel combobox2Panel = new JPanel(new BorderLayout());
      combobox2Panel.setBorder(BorderFactory.createEmptyBorder(5,0,5,0));
      combobox2Panel.add(_additionalComboBox2Label, BorderLayout.NORTH);
      combobox2Panel.add(_additionalComboBox2, BorderLayout.CENTER);

     //Jack Hwee - Add combobox for new choice list
       JPanel combobox3Panel = new JPanel(new BorderLayout());
      combobox3Panel.setBorder(BorderFactory.createEmptyBorder(5,0,5,0));
      combobox3Panel.add(_additionalComboBox3Label, BorderLayout.NORTH);
      combobox3Panel.add(_additionalComboBox3, BorderLayout.CENTER);

      JPanel bothComboboxPanel = new JPanel(new BorderLayout());
      bothComboboxPanel.add(comboboxPanel, BorderLayout.NORTH);
      bothComboboxPanel.add(combobox2Panel, BorderLayout.CENTER);
      bothComboboxPanel.add(combobox3Panel, BorderLayout.SOUTH);

      _additionalControlsInnerPanel.add(bothComboboxPanel, BorderLayout.SOUTH);

      for (Object item : _additionalComboBoxItemsList)
        _additionalComboBox.addItem(item);
      for (Object item : _additionalComboBox2ItemsList)
        _additionalComboBox2.addItem(item);
       for (Object item : _additionalComboBox3ItemsList)
        _additionalComboBox3.addItem(item);

      _additionalComboBox.addActionListener( new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _selectedComboBoxItem = _additionalComboBox.getSelectedItem();
        }
      });

      _additionalComboBox.setSelectedItem(_itemToSelectInComboBox);

      _additionalComboBox2.addActionListener( new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _selectedComboBox2Item = _additionalComboBox2.getSelectedItem();
        }
      });

      _additionalComboBox2.setSelectedItem(_itemToSelectInComboBox2);

      //Jack Hwee - Add combobox for new choice list
       _additionalComboBox3.addActionListener( new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _selectedComboBox3Item = _additionalComboBox3.getSelectedItem();
           switcthChoiceList();
        }
      });

      _additionalComboBox3.setSelectedItem(_itemToSelectInComboBox3);
    }

    getRootPane().setDefaultButton(_okButton);
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okAction();
      }
    });

  }
  /**
   * @return int
   * @author Andy Mechtenberg
   */
  public int showDialog()
  {
    SwingUtils.centerOnComponent(this, _frame);
    setVisible(true);
    return _returnValue;
  }

  /**
   * @param font Font
   * @return int
   * @author Andy Mechtenberg
   */
  public int showDialog(Font font)
  {
    Assert.expect(font != null);
    SwingUtils.setFont(this, font);
    pack();
    SwingUtils.centerOnComponent(this, _frame);

    setVisible(true);
    return _returnValue;
  }
  /**
   * @param e WindowEvent
   * @author Andy Mechtenberg
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    int eventId = e.getID();
    if (eventId == WindowEvent.WINDOW_CLOSING)
    {
      cancelAction();
    }
  }

  /**
   * @return Object
   * @author Andy Mechtenberg
   */
  public Object getSelectedValue()
  {
    return _selectedValue;
  }

  /**
   * @return Object
   * @author Andy Mechtenberg
   */
  public String getRenamedValue()
  {
    return _renamedValue;
  }

  /**
   * @author Laura Cormos
   */
  public Object getSelectedComboBoxItem()
  {
    return _selectedComboBoxItem;
  }

  /**
   * @author Laura Cormos
   */
  public Object getSelectedComboBox2Item()
  {
    return _selectedComboBox2Item;
  }

   /**
   * @author Jack Hwee
   */
  public Object getSelectedComboBox3Item()
  {
    return _selectedComboBox3Item;
  }
   
  /**
   * @param e WindowEvent
   * @author Andy Mechtenberg
   * @author Jack Hwee
   */
  private void okAction()
  {
    if (_createAdditionalCombobox3 == true)
    {
    java.util.List<Object> listTypeAssignmentNames = _additionalComboBox3ItemsList;
    Object[] listTypeAdditionalComboboxItems = listTypeAssignmentNames.toArray();
    if (_selectedComboBox3Item.toString() == listTypeAdditionalComboboxItems[1])
      _selectedValue = _choicesList2.getSelectedValue();
    else
      _selectedValue = _choicesList.getSelectedValue();
    }
    else
      _selectedValue = _choicesList.getSelectedValue();

    _renamedValue = _additionalTextField.getText();
    _returnValue = JOptionPane.OK_OPTION;

    dispose();
  }

  /**
   * @param e WindowEvent
   * @author Andy Mechtenberg
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    cancelAction();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void cancelAction()
  {
    _returnValue = JOptionPane.CANCEL_OPTION;
    _selectedValue = null;
    dispose();
  }

  /**
   *
   * @author Jack Hwee
   */
  void switcthChoiceList()
  {
    String listType = (String) _selectedComboBox3Item;

    java.util.List<Object> listTypeAssignmentNames = _additionalComboBox3ItemsList;
    Object[] listTypeAdditionalComboboxItems = listTypeAssignmentNames.toArray();
    
    if (listType == listTypeAdditionalComboboxItems[0])
    {
      _choicesScrollPane.getViewport().remove(_choicesList2);
      _choicesScrollPane.getViewport().add(_choicesList);
       if (_choicesListModel.isEmpty() == false)
       {
        _additionalTextField.setText(_choicesList.getSelectedValue().toString());
         _okButton.setEnabled(true);
       }
      else
      {
        _additionalTextField.setText("");
         _okButton.setEnabled(false);
      }
    }
    else if (listType == listTypeAdditionalComboboxItems[1])
    {
      _choicesScrollPane.getViewport().remove(_choicesList);
      _choicesScrollPane.getViewport().add(_choicesList2);
      if (_choicesListModel2.isEmpty() == false)
      {
        _additionalTextField.setText(_choicesList2.getSelectedValue().toString());
       _okButton.setEnabled(true);
      }
      else
      {
        _additionalTextField.setText("");
        _okButton.setEnabled(false);
      }
    }
  }
}
