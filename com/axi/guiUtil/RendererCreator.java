package com.axi.guiUtil;

/**
 * This interface is used by the GraphicsEngine class.  If a creation threshold is set
 * this interface is called by the GraphicsEngine when that threshold is reached.
 * The implementation of this interface would handle creating and destroying the correct
 * Renderers.
 *
 * @author Bill Darbie
 */
public interface RendererCreator
{
  /**
   * Create all the renderers for the layer passed in.
   * @author Bill Darbie
   */
  public void updateRenderers(GraphicsEngine graphicsEngine, int layerNumber);
}
