package com.axi.guiUtil;

import java.awt.*;

import javax.swing.*;

import com.axi.util.*;

/**
 * Creates a modal dialog to capture a user entered (via keyboard) serial number
 * @author Andy Mechtenberg
 */
public class StringInputDialog// extends JDialog
{
  private JFrame _frame;
  private String _title;
  private String _prompt;
  private String _stringInput;

  /**
   * @author Andy Mechtenberg
   */
  public StringInputDialog(JFrame frame, String title, String prompt)
  {
    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(prompt != null);

    _frame = frame;
    _title = title;
    _prompt = prompt;
  }

  /**
   * @return JOptionPane.OK_OPTION if a non-null serial number was entered, JOptionPane.CANCEL_OPTION if not.
   * @author Andy Mechtenberg
   */
  public int showDialog(Font font)
  {
    Assert.expect(font != null);
//    _stringInput = (String)JOptionPane.showInputDialog(_frame,
//        StringLocalizer.keyToString("TESTEXEC_GUI_SERIAL_NUMBER_PROMPT_KEY"),
//        StringLocalizer.keyToString("TESTEXEC_GUI_SERIAL_NUMBER_TITLE_KEY"),
//        JOptionPane.PLAIN_MESSAGE);
//    _stringInput = (String)JOptionPane.showInputDialog(_frame,
//                                                       _prompt,
//                                                       _title,
//                                                       JOptionPane.PLAIN_MESSAGE);
//
//    JOptionPane pane = new JOptionPane(_prompt, JOptionPane.PLAIN_MESSAGE);
    JOptionPane pane = new JOptionPane(_prompt, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION);//, icon, null, null);
    pane.setWantsInput(true);
    JDialog dialog = pane.createDialog(_frame, _title);
    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    dialog.setModal(true);
    SwingUtils.setFont(dialog, font);
    dialog.pack();
    dialog.toFront();
    dialog.setVisible(true);
    dialog.dispose();
    Object inputObj = pane.getInputValue();
    Object responseObj = pane.getValue();
    int choice;
    if (responseObj == null) // this can happen if the user clicks the "X" to dismiss the dialog
      choice = JOptionPane.CANCEL_OPTION;
    else
    {
      if(responseObj instanceof Integer)
        choice = ((Integer)responseObj).intValue();
      else
        choice = JOptionPane.OK_OPTION;
    }
    if (choice == JOptionPane.OK_OPTION)
    {
      _stringInput = (String)inputObj;
    }
    else // if the user cancels out by using ESC, the return is an Integer set to -1.  We treat that as null
      _stringInput = null;
    if (_stringInput == null)
      return JOptionPane.CANCEL_OPTION;
    else
      return JOptionPane.OK_OPTION;
  }

  /**
   * @return JOptionPane.OK_OPTION if a non-null serial number was entered, JOptionPane.CANCEL_OPTION if not.
   * @author Andy Mechtenberg
   */
  public int showDialog()
  {
    JOptionPane pane = new JOptionPane(_prompt, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION);//, icon, null, null);
    pane.setWantsInput(true);
    JDialog dialog = pane.createDialog(_frame, _title);
    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    dialog.setModal(true);
    dialog.pack();
    dialog.toFront();
    dialog.setVisible(true);  // blocks
    dialog.dispose();
    Object inputObj = pane.getInputValue();
    Object responseObj = pane.getValue();
    int choice;
    if (responseObj == null) // this can happen if the user clicks the "X" to dismiss the dialog
      choice = JOptionPane.CANCEL_OPTION;
    else
    {
      if(responseObj instanceof Integer)
        choice = ((Integer)responseObj).intValue();
      else
        choice = JOptionPane.OK_OPTION;
    }
    if (choice == JOptionPane.OK_OPTION)
    {
      _stringInput = (String)inputObj;
    }
    else // if the user cancels out by using ESC, the return is an Integer set to -1.  We treat that as null
      _stringInput = null;
    if (_stringInput == null)
      return JOptionPane.CANCEL_OPTION;
    else
      return JOptionPane.OK_OPTION;
  }

  /**
   * Show dialog with a default input value
   * @return JOptionPane.OK_OPTION if a non-null serial number was entered, JOptionPane.CANCEL_OPTION if not.
   * @author George Booth
   */
  public int showDialog(String defaultInput)
  {
    JOptionPane pane = new JOptionPane(_prompt, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION);//, icon, null, null);
    pane.setWantsInput(true);
    pane.setInitialSelectionValue(defaultInput);
    JDialog dialog = pane.createDialog(_frame, _title);
    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    dialog.setModal(true);
    dialog.pack();
    dialog.toFront();
    dialog.setVisible(true);  // blocks
    dialog.dispose();
    Object inputObj = pane.getInputValue();
    Object responseObj = pane.getValue();
    int choice;
    if (responseObj == null) // this can happen if the user clicks the "X" to dismiss the dialog
      choice = JOptionPane.CANCEL_OPTION;
    else
    {
      if(responseObj instanceof Integer)
        choice = ((Integer)responseObj).intValue();
      else
        choice = JOptionPane.OK_OPTION;
    }

    if (choice == JOptionPane.OK_OPTION)
    {
      _stringInput = (String)inputObj;
    }
    else // if the user cancels out by using ESC, the return is an Integer set to -1.  We treat that as null
      _stringInput = null;
    if (_stringInput == null)
      return JOptionPane.CANCEL_OPTION;
    else
      return JOptionPane.OK_OPTION;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getStringInput()
  {
    Assert.expect(_stringInput != null);
    return _stringInput;
  }
}
