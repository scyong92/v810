package com.axi.guiUtil;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;

/**
 * This class is used by the GraphicsEngine class and will call any
 * Observers update method with the visible rectangle in renderer coordinates.
 *
 * @author George A. David
 */
public class VisibleRectangleObservable extends Observable
{

  /**
   * @author George A. David
   */
  public VisibleRectangleObservable()
  {
    // do nothing
  }

  /**
   * @author George A. David
   */
  void setVisibleRectangle(Rectangle2D visibleRectangle)
  {
    Assert.expect(visibleRectangle != null);
    setChanged();
    notifyObservers(visibleRectangle);
  }
}
