package com.axi.guiUtil;

import java.awt.*;
import java.io.*;

import com.axi.util.Assert;

/**
 * This is a layout class that arranges components like the FlowLayout
 * does only vertically.
 * @author Steve Anonson
 */
public class VerticalFlowLayout implements LayoutManager, Serializable
{
  //wpdfix - these should be Enums, NOT public ints
  // These values indicate how each column of components should be justified.
  public static final int LEFT = 0;
  public static final int CENTER = 1;
  public static final int RIGHT = 2;
  public static final int FILL = 3;

  // These values indicate that each column of components should be justified in the container
  // e.g. to the top/middle/bottom of the container.
  public static final int TOP = 4;
  public static final int MIDDLE = 5;
  public static final int BOTTOM = 6;

  /*
   * This is how the components are layed out with the possible alignment values.
   * The container is too small in this example which forces the components into multiple columns
   * and better illustrate the consequences.
   *
   * LEFT/TOP              LEFT/MIDDLE           LEFT/BOTTOM
   * ====================  ====================  ====================
   * | -------  -----   |  |                  |  |                  |
   * | |  1  |  | 4 |   |  | -------          |  |                  |
   * | -------  -----   |  | |  1  |          |  | -------          |
   * | -----            |  | -------          |  | |  1  |          |
   * | | 2 |            |  | -----    -----   |  | -------          |
   * | -----            |  | | 2 |    | 4 |   |  | -----            |
   * | -----            |  | -----    -----   |  | | 2 |            |
   * | | 3 |            |  | -----            |  | -----            |
   * | -----            |  | | 3 |            |  | -----    -----   |
   * |                  |  | -----            |  | | 3 |    | 4 |   |
   * |                  |  |                  |  | -----    -----   |
   * ====================  ====================  ====================
   *
   * CENTER/TOP            CENTER/MIDDLE         CENTER/BOTTOM
   * ====================  ====================  ====================
   * | -------  -----   |  |                  |  |                  |
   * | |  1  |  | 4 |   |  | -------          |  |                  |
   * | -------  -----   |  | |  1  |          |  | -------          |
   * |  -----           |  | -------          |  | |  1  |          |
   * |  | 2 |           |  |  -----   -----   |  | -------          |
   * |  -----           |  |  | 2 |   | 4 |   |  |  -----           |
   * |  -----           |  |  -----   -----   |  |  | 2 |           |
   * |  | 3 |           |  |  -----           |  |  -----           |
   * |  -----           |  |  | 3 |           |  |  -----   -----   |
   * |                  |  |  -----           |  |  | 3 |   | 4 |   |
   * |                  |  |                  |  |  -----   -----   |
   * ====================  ====================  ====================
   *
   * RIGHT/TOP             RIGHT/MIDDLE          RIGHT/BOTTOM
   * ====================  ====================  ====================
   * | -------  -----   |  |                  |  |                  |
   * | |  1  |  | 4 |   |  | -------          |  |                  |
   * | -------  -----   |  | |  1  |          |  | -------          |
   * |   -----          |  | -------          |  | |  1  |          |
   * |   | 2 |          |  |   -----  -----   |  | -------          |
   * |   -----          |  |   | 2 |  | 4 |   |  |   -----          |
   * |   -----          |  |   -----  -----   |  |   | 2 |          |
   * |   | 3 |          |  |   -----          |  |   -----          |
   * |   -----          |  |   | 3 |          |  |   -----  -----   |
   * |                  |  |   -----          |  |   | 3 |  | 4 |   |
   * |                  |  |                  |  |   -----  -----   |
   * ====================  ====================  ====================
   */
  private int _valign; // alignment between elements in a column
  private int _halign; // alignment between columns of elements
  private int _hgap; // horizontal gap between components
  private int _vgap; // vertical gap between components


  /**
   * Constructs a new VFlowLayout with a centered alignment and a default
   * 5-unit horizontal and vertical gap.
   */
  public VerticalFlowLayout()
  {
    this(LEFT, TOP, 5, 5);
  }

  /**
   * Constructs a new VFlowLayout with the specified alignment and a default
   * 5-unit horizontal and vertical gap.
   *
   */
  public VerticalFlowLayout(int verticalAlign, int horizontalAlign)
  {
    this(verticalAlign, horizontalAlign, 5, 5);
  }

  /**
   * Constructs a new VFlowLayout with the specified alignment and a default
   * 5-unit horizontal and vertical gap.
   *
   */
  public VerticalFlowLayout(int verticalAlign)
  {
    this(verticalAlign, TOP, 5, 5);
  }

  /**
   * Constructs a new VFlowLayout with the specified alignment and a default
   * 5-unit horizontal and vertical gap.
   *
   */
  public VerticalFlowLayout(int verticalAlign, int horizontalAlign, int hgap, int vgap)
  {
    setHgap(hgap);
    setVgap(vgap);
    setVerticalAlignment(verticalAlign);
    setHorizontalAlignment(horizontalAlign);
  }

  // Access methods
  public int getVerticalAlignment()
  {
    return _valign;
  }

  public int getHorizontalAlignment()
  {
    return _halign;
  }

  public int getHgap()
  {
    return _hgap;
  }

  public int getVgap()
  {
    return _vgap;
  }

  public void setVerticalAlignment(int componenetAlign)
  {
    this._valign = componenetAlign;
  }

  public void setHorizontalAlignment(int columnAlign)
  {
    this._halign = columnAlign;
  }

  public void setHgap(int hgap)
  {
    this._hgap = hgap;
  }

  public void setVgap(int vgap)
  {
    this._vgap = vgap;
  }

  /**
   * Adds the specified component to the layout. Not used by this class.
   */
  public void addLayoutComponent(String name, Component comp)
  {
    // do nothing
  }

  /**
   * Removes the specified component from the layout. Not used by this class.
   */
  public void removeLayoutComponent(Component comp)
  {
    // do nothinga
  }

  /**
   * Returns the preferred dimensions for this layout given the components in
   * the specified target container.
   *
   * @param target - container to be laid out.
   * @return the preferred dimensions of the container.
   *
   * @see Container
   */
  public Dimension preferredLayoutSize(Container target)
  {
    synchronized (target.getTreeLock())
    {
      Dimension dim = new Dimension(0, 0);
      int nmembers = target.getComponentCount();
      boolean firstVisibleComponent = true;

      for (int i = 0; i < nmembers; i++)
      {
        Component m = target.getComponent(i);
        if (m.isVisible())
        {
          Dimension d = m.getPreferredSize();
          dim.width = Math.max(dim.width, d.width);
          if (firstVisibleComponent)
          {
            firstVisibleComponent = false;
          }
          else
          {
            dim.height += _vgap;
          }
          dim.height += d.height;
        }
      }
      Insets insets = target.getInsets();
      dim.height += insets.top + insets.bottom + _vgap * 2;
      dim.width += insets.left + insets.right + _hgap * 2;
      return dim;
    }
  }

  /**
   * Returns the minimum dimensions for this layout given the components in
   * the specified target container.
   *
   * @param target - container to be laid out.
   * @return the minimum dimensions of the container.
   *
   * @see Container
   */
  public Dimension minimumLayoutSize(Container target)
  {
    synchronized (target.getTreeLock())
    {
      Dimension dim = new Dimension(0, 0);
      int nmembers = target.getComponentCount();
      boolean firstVisibleComponent = true;

      for (int i = 0; i < nmembers; i++)
      {
        Component m = target.getComponent(i);
        if (m.isVisible())
        {
          Dimension d = m.getMinimumSize();
          dim.width = Math.max(dim.width, d.width);
          if (firstVisibleComponent)
          {
            firstVisibleComponent = false;
          }
          else
          {
            dim.height += _vgap;
          }
          dim.height += d.height;
        }
      }
      Insets insets = target.getInsets();
      dim.height += insets.top + insets.bottom + _vgap * 2;
      dim.width += insets.left + insets.right + _hgap * 2;
      return dim;
    }
  }

  /**
   * Centers the elements in the specified row, if there is any slack.
   *
   * @param target the component which needs to be moved
   * @param x the x coordinate
   * @param y the y coordinate
   * @param width the width dimensions
   * @param height the height dimensions
   * @param rowStart the beginning of the row
   * @param rowEnd the the ending of the row
   */
  private void moveComponents(Container target, int x, int y, int width, int height,
                              int colStart, int colEnd, boolean ltr)
  {
    synchronized (target.getTreeLock())
    {
      // calculate the y position of the component based upon the horizontal
      // alignment setting.
      switch (_halign)
      {
        case TOP: // align top edges
          break;
        case MIDDLE: // top/bottom centers aligned
          y += (height / 2);
          break;
        case BOTTOM: // align bottom edges
          y += height;
          break;
      } // switch _halign

      for (int i = colStart; i < colEnd; i++)
      {
        Component m = target.getComponent(i);
        int componentX = x;
        if (m.isVisible())
        {
          // calculate the x position of the component based upon the vertical
          // alignment setting.
          switch (_valign)
          {
            case LEFT: // left edges aligned
              break;
            case CENTER: // left/right centers aligned
              componentX = x + ((width - m.getWidth()) / 2);
              break;
            case RIGHT: // right edges aligned
              componentX = x + (width - m.getWidth());
              break;
            case FILL: // align both left and right edges
              componentX = x + (width - m.getWidth());
              m.setSize(width, m.getHeight());
              break;
            default:
              // wpd - do nothing - bad design - this should be fixed
              break;
          } // switch _valign

          if (ltr)
          {
            // left to right layout on the container, use new location as calculated
            m.setLocation(componentX, y);
          }
          else
          {
            // right to left layout on the container, so reverse the y location
            m.setLocation(componentX, target.getHeight() - y - m.getHeight());
          }
          // Increment y for the next component in the column
          y += m.getHeight() + _vgap;
        }
      } // end for
    } // end synchronized
  }

  /**
   * Lays out the container. This method lets each component take its preferred
   * size by reshaping the components in the target container in order to satisfy
   * the constraints of this VFlowLayout object.
   *
   * @param target the specified component being laid out.
   * @see Container
   */
  public void layoutContainer(Container target)
  {
    synchronized (target.getTreeLock())
    {
      // Get the size of the layout area.
      Insets insets = target.getInsets();
      int maxheight = target.getSize().height - (insets.top + insets.bottom + _vgap * 2);
      int maxwidth = target.getSize().width - (insets.left + insets.right + _hgap * 2);

      int nmembers = target.getComponentCount();
      // Set the initial x,y location.
      int x = insets.left + _hgap;
      int y = 0;
      int colw = 0, start = 0;

      boolean ltr = target.getComponentOrientation().isLeftToRight();

      // Go through the components and assign them to columns.
      // When as many components that will fit in a column is determined, call
      // moveComponents to change their locations.
      for (int i = 0; i < nmembers; i++)
      {
        Component m = target.getComponent(i);
        if (m.isVisible())
        {
          Dimension d = m.getPreferredSize();
          if (_halign == FILL)
          {
            m.setSize(maxwidth, d.height);
          }
          else
          {
            m.setSize(d.width, d.height);
          }

          if ((y == 0) || ((y + d.height) <= maxheight))
          {
            if (y > 0)
            {
              y += _vgap;
            }
            y += d.height;
            colw = Math.max(colw, d.width);
          }
          else
          {
            // Move the collected components to their locations in the column.
            moveComponents(target, x, insets.top + _vgap, colw, maxheight - y, start, i, ltr);
            y = d.height;
            x += _hgap + colw;
            colw = d.width;
            start = i;
          }
        }
      } // end for

      // Move the leftover components to thier column.
      moveComponents(target, x, insets.top + _vgap, colw, maxheight - y, start, nmembers, ltr);
    } // end synchronized
  }

  /**
   * Returns a string representation of this VFlowLayout object and its values.
   *
   * @return a string representation of this layout.
   */
  public String toString()
  {
    String str = "";
    switch (_valign)
    {
      case LEFT:
        str = ",valign=left";
        break;
      case CENTER:
        str = ",valign=center";
        break;
      case RIGHT:
        str = ",valign=right";
        break;
    }
    switch (_halign)
    {
      case TOP:
        str = str + ",halign=top";
        break;
      case MIDDLE:
        str = str + ",halign=middle";
        break;
      case BOTTOM:
        str = str + ",halign=bottom";
        break;
    }
    return getClass().getName() + "[hgap=" + _hgap + ",vgap=" + _vgap + str + "]";
  }
}
