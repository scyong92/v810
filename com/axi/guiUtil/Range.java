package com.axi.guiUtil;

/**
 * Title: Range
 * Description: This defines an interface to be used by a numeric range class.
 * It provides methods to retrieve the minimum and maximum values and a method
 * for range checking.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies
 * @author  Steve Anonson
 * @version 1.0
 * @see IntegerRange
 */

public interface Range
{
  public Number getMinimumValue();
  public Number getMaximumValue();
  public boolean inRange( Number value );
}