package com.axi.guiUtil;

import java.io.*;
import javax.swing.filechooser.FileFilter;

import com.axi.util.*;

/**
 * This class is used to filter the type of files we would like JFileDialog to display.
 * @author Erica Wheatcroft
 */
public class FileFilterUtil extends FileFilter
{
  private String _description;
  private String _extension;
  private final String _DOT = ".";

  /**
   * @param extension - the file extension you would like to filter for.
   * @param description - the description of the file you are filtering for. THis is displayed in the JFileDialog
   * @author Erica Wheatcroft
   */
  public FileFilterUtil(String extension, String description)
  {
    Assert.expect(extension != null);
    Assert.expect(description != null);

    _description = description;
    _extension = extension.toLowerCase();
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean accept(File file)
  {
    Assert.expect(file != null);

    boolean accepted = false;
    if(file == null)
      accepted = false;
    else if(file.isDirectory())
      accepted =true;
    else
      accepted = file.getName().toLowerCase().endsWith(_extension);

    return accepted;
  }

  /**
   * @author Erica Wheatcroft
   */
  public String getDescription()
  {
     Assert.expect(_description != null);

     return _description;
  }
}
