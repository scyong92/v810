package com.axi.guiUtil;

import java.awt.*;

/**
 * Title:        PairLayout
 * Description:  This class is a layout manager that arranges pairs of components
 * in two columns.  It is especially useful for sets of label / textfield pairs
 * used to enter data. <P>
 * The idea came from the book Swing by Mathew Robinson and Pavel Vorobiev.
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class PairLayout implements LayoutManager
{
  protected int _divider = -1;
  protected int _hGap = 10;
  protected int _vGap = 5;
  protected boolean _pad = true;

  /**
   * Constructor
   * Uses the default values for horizontal and vertical gaps.
   */
  public PairLayout()
  {
  }

  /**
   * Constructor
   *
   * @param   hGap - number of pixels between the component pairs.
   * @param   vGap - number of pixels between the rows of components pairs.
   */
  public PairLayout(int hGap, int vGap)
  {
    this(hGap, vGap, true);
  }

  /**
   * Constructor
   *
   * @param   hGap - number of pixels between the component pairs.
   * @param   vGap - number of pixels between the rows of components pairs.
   * @param   pad - if true make all the compent pair rows the same width by changing the size of the second component.
   */
  public PairLayout(int hGap, int vGap, boolean pad)
  {
    _hGap = hGap;
    _vGap = vGap;
    _pad = pad;
  }

  /**
   * Adds the specified component to the layout. Not used by this class.
   */
  public void addLayoutComponent(String name, Component comp)
  {
  }

  /**
   * Removes the specified component from the layout. Not used by this class.
   */
  public void removeLayoutComponent(Component comp)
  {
  }

  /**
   * Returns the preferred dimensions for this layout given the components in
   * the specified target container.
   *
   * @param parent - parent container.
   * @return the preferred dimensions of the container.
   *
   * @see Container
   */
  public Dimension preferredLayoutSize(Container parent)
  {
    int divider = getDivider(parent);
    int w = 0;
    int h = 0;
    for (int k = 1 ; k < parent.getComponentCount(); k += 2)
    {
      Component comp = parent.getComponent(k);
      Dimension d = comp.getPreferredSize();
      w = Math.max(w, d.width);
      h += d.height + _vGap;
    }
    h -= _vGap;

    Insets insets = parent.getInsets();
    return new Dimension(divider + w + insets.left + insets.right, h + insets.top + insets.bottom);
  }

  /**
   * Returns the minimum dimensions for this layout given the components in
   * the specified target container.
   *
   * @param parent - parent container.
   * @return the minimum dimensions of the container.
   *
   * @see Container
   */
  public Dimension minimumLayoutSize(Container parent)
  {
    return preferredLayoutSize(parent);
  }

  /**
   * Lays out the container. This method lets each component take its preferred
   * size by reshaping the components in the target container in order to satisfy
   * the constraints of this PairLayout object.
   *
   * @param parent parent container.
   * @see Container
   */
  public void layoutContainer(Container parent)
  {
    int divider = getDivider(parent);

    Insets insets = parent.getInsets();
    int w = parent.getWidth() - insets.left - insets.right - divider;
    int x = insets.left;
    int y = insets.top;

    for (int k = 1 ; k < parent.getComponentCount(); k += 2)
    {
      Component comp1 = parent.getComponent(k-1);
      Component comp2 = parent.getComponent(k);
      Dimension d = comp2.getPreferredSize();

      comp1.setBounds(x, y, divider - _hGap, d.height);
      comp2.setBounds(x + divider, y, ((_pad) ? w : d.width), d.height);
      y += d.height + _vGap;
    }
  }

  // Access methods
  public int getHGap()    { return _hGap; }
  public int getVGap()    { return _vGap; }
  public int getDivider() { return _divider; }

  /**
   * Set the divider location
   *
   * @param   divider - location to separate the component pairs.
   */
  public void setDivider(int divider)
  {
    if (divider > 0)
      _divider = divider;
  }

  /**
   * Calculate and return the divider locaion by looking at the components' preferred
   * sizes and determining the where the divider should be so that they all fit.
   *
   * @param   parent - Container to be laid out.
   * @see Container
   */
  protected int getDivider(Container parent)
  {
    if (_divider > 0)
      return _divider;  // already set so just return the value

    int divider = 0;
    // Calculate the best position to divide the pairs so they all fit.
    for (int k = 0 ; k < parent.getComponentCount(); k += 2)
    {
      Component comp = parent.getComponent(k);
      Dimension d = comp.getPreferredSize();
      divider = Math.max(divider, d.width);
    }
    divider += _hGap;
    return divider;
  }

  /**
   * Returns a string representation of this PairLayout object and its values.
   *
   * @return a string representation of this layout.
   */
  public String toString()
  {
    return getClass().getName() + "[hgap=" + _hGap + ",vgap=" + _vGap + ",divider=" + _divider + "]";
  }
}
