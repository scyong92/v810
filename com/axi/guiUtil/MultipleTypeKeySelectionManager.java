package com.axi.guiUtil;

import javax.swing.*;
import java.awt.event.*;

import com.axi.util.*;

/**
 * This class is used as the key selection manager for JComboboxes, which allow for
 * multiple key strokes to be used to find the selection, instead of the default of 1
 * Keys must be type within 1 second of each other, or it gets reset
 * @author Andy Mechtenberg
 */
public class MultipleTypeKeySelectionManager implements JComboBox.KeySelectionManager
{
  private final long _KEYSTROKE_INTERVAL = 1000;  // one second
  private final int _NO_SELECTION = -1;
  private String _searchString = new String();
  private long _lastTime;

  /**
   * @author Andy Mechtenberg
   */
  public int selectionForKey(char key, ComboBoxModel model)
  {
    Assert.expect(model != null);
    updateSearchString(model, key);
    int start = findIndex(model, getSelectedString(model));
    int selection = search(model, start);
    if ((selection == _NO_SELECTION) && (start != 0))
      selection = search(model, 0);  // start from beginning

    return selection;
  }

  /**
   * @author Andy Mechtenberg
   */
  private int search(ComboBoxModel model, int start)
  {
    Assert.expect(model != null);

    for (int i = start; i < model.getSize(); i++)
    {
      String currentString = getString(model, i);
      int searchLength = _searchString.length();
      if (currentString.regionMatches(true, 0, _searchString, 0, searchLength))
        return i;
    }
    return _NO_SELECTION;  // not found
  }

  /**
   * @author Andy Mechtenberg
   */
  private String getString(ComboBoxModel model, int index)
  {
    Assert.expect(model != null);
    String item = (String)model.getElementAt(index);
    return item;
  }

  /**
   * @author Andy Mechtenberg
   */
  private int findIndex(ComboBoxModel model, String find)
  {
    Assert.expect(model != null);
    Assert.expect(find != null);

    int size = model.getSize();
    if (find != null)
    {
      for (int i = 0; i < size; i++)
      {
        String s = getString(model, i);
        if (s.compareToIgnoreCase(find) == 0)
        {
          return (i == size - 1) ? 0 : i + 1;
        }
      }
    }

    return 0;
  }

  /**
   * @author Andy Mechtenberg
   */
  private String getSelectedString(ComboBoxModel model)
  {
    Assert.expect(model != null);

    String item = (String)model.getSelectedItem();
    return item;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateSearchString(ComboBoxModel model, char key)
  {
    Assert.expect(model != null);

    long time = System.currentTimeMillis();
    if (time - _lastTime < _KEYSTROKE_INTERVAL)
      _searchString += key;
    else
      _searchString = "" + key;
    _lastTime = time;
  }




















}