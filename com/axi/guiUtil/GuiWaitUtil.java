package com.axi.guiUtil;

import java.awt.*;
import javax.swing.*;

import com.axi.util.*;

/**
 * This class allows you to wait on the swing thread (block the swing thread)
 * while still allowing repaints. Please note that while the UI will be able to
 * repaint, the UI will not be accessable to the user because you are blocking
 * it.
 *
 * @author George A. David
 */
public class GuiWaitUtil
{
  private JDialog _hiddenDialog;

  /**
   * @author George A. David
   */
  public synchronized void clear()
  {
    Assert.expect(_hiddenDialog != null);

    _hiddenDialog.dispose();
  }

  /**
   * @author George A. David
   */
  private synchronized void initialize(Frame owner)
  {
    Assert.expect(owner != null);

    _hiddenDialog = new JDialog(owner, true);
    _hiddenDialog.setUndecorated(true);
    _hiddenDialog.setBounds(0, 0, 0, 0);
    notifyAll();
  }

  /**
   * @author George A. David
   */
  public synchronized void waitUntilReady()
  {
    while (_hiddenDialog == null || _hiddenDialog.isVisible() == false)
    {
      try
      {
        Thread.sleep(200);
      }
      catch (InterruptedException ex)
      {
        // do nothing
      }
    }
  }

  /**
   * @author George A. David
   */
  public void clearWhenReady()
  {
    waitUntilReady();
    clear();
  }

  /**
   * @author George A. David
   */
  public void wait(Frame owner)
  {
    initialize(owner);
    _hiddenDialog.setVisible(true);
   }
}
