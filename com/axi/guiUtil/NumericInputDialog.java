package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.util.*;

/**
 * Creates a modal dialog to capture a user entered (via keyboard) number as a double
 * @author Andy Mechtenberg
 */
public class NumericInputDialog extends EscapeDialog
{
  private JFrame _frame;
  private int _returnValue = JOptionPane.CANCEL_OPTION; // either OK or CANCEL depending on how the user exits
  private double _numericValue = 0;
  private double _min = 0.0;
  private double _max = 0.0;
  private String _message;
  private String _okButtonText;
  private String _cancelButtonText;
  private NumericInputDialogOutOfRangeInt _numericInputDialogOutOfRangeInt;

  private Font _buttonFont;
  private Font _messageFont;

  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private JPanel _messagePanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private FlowLayout _buttonPanelFlowLayout = new FlowLayout();
  private JPanel _innerButtonPanel = new JPanel();
  private GridLayout _innerButtonPanelGridLayout = new GridLayout();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private FlowLayout _messagePanelFlowLayout = new FlowLayout();
  private JLabel _messageLabel = new JLabel();
  private JPanel _inputPanel = new JPanel();
  private JTextField _inputTextField = new JTextField(22);
  private BorderLayout _inputPanelBorderLayout = new BorderLayout();
  private Border _innerButtonPanelBorder = BorderFactory.createEmptyBorder(5, 10, 0, 10);
  private Border _mainPanelBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);

  /**
   * For use with the jBuilder visual designer only
   * @author Andy Mechtenberg
   */
  private NumericInputDialog()
  {
    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public NumericInputDialog(JFrame frame,
                            String title,
                            String message,
                            String okButtonText,
                            String cancelButtonText,
                            String initialValue,
                            double min,
                            double max,
                            NumericInputDialogOutOfRangeInt numericInputDialogOutOfRangeInt,
                            Font font)
  {
    super(frame, title, true);

    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(message != null);
    Assert.expect(okButtonText != null);
    Assert.expect(cancelButtonText != null);
    Assert.expect(initialValue != null);
    Assert.expect(min <= max);
    Assert.expect(font != null);

    _frame = frame;
    _message = message;
    _okButtonText = okButtonText;
    _cancelButtonText = cancelButtonText;
    _buttonFont = FontUtil.getDialogButtonFont();
    _messageFont = FontUtil.getDialogMessageFont();
    _inputTextField.setText(initialValue);
    _inputTextField.selectAll();
    _min = min;
    _max = max;
    _numericInputDialogOutOfRangeInt = numericInputDialogOutOfRangeInt;

    try
    {
      jbInit();
      SwingUtils.setFont(this, font);
      pack();
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit() throws Exception
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _cancelButton.setText(_cancelButtonText);
    _innerButtonPanelGridLayout.setHgap(5);
    _messagePanel.setLayout(_messagePanelFlowLayout);
    _messagePanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _messagePanelFlowLayout.setHgap(0);
    _messagePanelFlowLayout.setVgap(0);
    _cancelButton.setFont(_buttonFont);
    _okButton.setFont(_buttonFont);
    _cancelButton.setMargin(new Insets(2, 8, 2, 8));
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _okButton.setMargin(new Insets(2, 8, 2, 8));
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });

    _messageLabel.setFont(_messageFont);
    _messageLabel.setText(_message);
    _inputPanel.setLayout(_inputPanelBorderLayout);
    _buttonPanelFlowLayout.setHgap(0);
    _buttonPanelFlowLayout.setVgap(0);
    _innerButtonPanel.setBorder(_innerButtonPanelBorder);
    _mainPanel.setBorder(_mainPanelBorder);
    _mainPanelBorderLayout.setVgap(5);
    this.getContentPane().add(_mainPanel);
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    _buttonPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _innerButtonPanel.setLayout(_innerButtonPanelGridLayout);
    _okButton.setText(_okButtonText);
    _mainPanel.add(_buttonPanel, java.awt.BorderLayout.SOUTH);
    _buttonPanel.add(_innerButtonPanel);
    _innerButtonPanel.add(_okButton);
    _innerButtonPanel.add(_cancelButton);
    _messagePanel.add(_messageLabel);
    _mainPanel.add(_inputPanel, java.awt.BorderLayout.CENTER);
    _mainPanel.add(_messagePanel, java.awt.BorderLayout.NORTH);
    _inputPanel.add(_inputTextField, java.awt.BorderLayout.NORTH);
    getRootPane().setDefaultButton(_okButton);
  }

  /**
   * @author Andy Mechtenberg
   */
  public int showDialog()
  {
    Dimension dlgSize = getPreferredSize();
    Dimension frmSize = _frame.getSize();
    Point loc = _frame.getLocation();
    setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);

    setVisible(true);
    return _returnValue;
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    int eventId = e.getID();
    if (eventId == WindowEvent.WINDOW_CLOSING)
    {
      cancelAction();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public double getDoubleValue()
  {
    Assert.expect((_numericValue >= _min) && (_numericValue <= _max));
    return _numericValue;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getIntValue()
  {
    Assert.expect((_numericValue >= _min) && (_numericValue <= _max));
    return (int)_numericValue;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    // if they didn't type anything, and hit OK, that's the same as hitting cancel
    if (_inputTextField.getText().length() == 0)
    {
      cancelAction();
    }
    else
    {
      try
      {
        String doubleStr = _inputTextField.getText();
        _numericValue = StringUtil.convertStringToDouble(doubleStr, _min, _max);
        _returnValue = JOptionPane.OK_OPTION;
        dispose();
      }
      catch (BadFormatException ex)
      {
        String errorMessage = _numericInputDialogOutOfRangeInt.getIntValueOutOfRangeLocalizedString(_inputTextField.getText(), _min, _max);
        String title = _numericInputDialogOutOfRangeInt.getErrorTitle();
        JOptionPane.showMessageDialog(this, errorMessage, title, JOptionPane.ERROR_MESSAGE);
        _inputTextField.selectAll();
      }
      catch (ValueOutOfRangeException ex)
      {
        String errorMessage = _numericInputDialogOutOfRangeInt.getIntValueOutOfRangeLocalizedString(_inputTextField.getText(), _min, _max);
        String title = _numericInputDialogOutOfRangeInt.getErrorTitle();
        JOptionPane.showMessageDialog(this, errorMessage, title, JOptionPane.ERROR_MESSAGE);
        _inputTextField.selectAll();
      }

    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    cancelAction();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void cancelAction()
  {
    _returnValue = JOptionPane.CANCEL_OPTION;
    _numericValue = 0;
    dispose();
  }
}
