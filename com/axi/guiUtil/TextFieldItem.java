package com.axi.guiUtil;

import com.axi.util.*;

/**
 * @author sheng-chuan.yong
 */
public class TextFieldItem
{
  private String _name = null;
  private String _selected = "";
  private boolean _isNumeric = false;
  private int _expectedLength = 0;
  private NumericRangePlainDocument _textDocument;
  
  /**
   * @author swee-yee.wong
   */
  public TextFieldItem(String name, String selected, boolean onlyNumeric, int expectedLength)
  {
    Assert.expect(name != null);
    
    _name = name;
    _selected = selected;
    _isNumeric = onlyNumeric;
    _expectedLength = expectedLength;
  }
  
  /**
   * @author swee-yee.wong
   */
  public void setName(String name)
  {
    Assert.expect(name != null);
    _name = name;
  }
  
  /**
   * @author swee-yee.wong
   */
  public String getName()
  {
    Assert.expect(_name != null);
    return _name;
  }
  
  /**
   * @author swee-yee.wong
   */
  public void setSelected(String selected)
  {
    _selected = selected;
  }
  
  /**
   * @author swee-yee.wong
   */
  public String getSelected()
  {
    return _selected;
  }
  
  /**
   * @author swee-yee.wong
   */
  public void setIsNumeric(boolean isNumeric)
  {
    _isNumeric = isNumeric;
  }
  
  /**
   * @author swee-yee.wong
   */
  public boolean isNumeric()
  {
    return _isNumeric;
  }
  
  /**
   * @author swee-yee.wong
   */
  public int getExpectedLength()
  {
    return _expectedLength;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public NumericRangePlainDocument getDocument()
  {
    return _textDocument;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setDocument(NumericRangePlainDocument document)
  {
    Assert.expect(document != null);
    _textDocument = document;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public boolean hasDocument()
  {
    return _textDocument != null;
  }
}
