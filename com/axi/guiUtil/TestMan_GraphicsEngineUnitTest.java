package com.axi.guiUtil;

import java.io.*;

import com.axi.util.*;
import javax.swing.UIManager;
import javax.swing.SwingUtilities;
import java.lang.reflect.*;

/**
 *
 * Main class for manual unit test for GraphicsEngine and associated classes.
 * @author Kay Lannen
 */
public class TestMan_GraphicsEngineUnitTest extends UnitTest
{
  /**
   * @param is BufferedReader
   * @param os PrintWriter
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      SwingUtilities.invokeLater(new TestGraphicsEngineRunnable(this));

      // UnitTest.execute will close the application prematurely if we return.
      // Stop here and let the Swing thread take care of the application exit.
      while (true);
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  public static void main(String[] args)
  {
    UnitTest.execute(new TestMan_GraphicsEngineUnitTest());
  }
}

class TestGraphicsEngineRunnable implements Runnable
{
  private UnitTest _unitTest;
  public TestGraphicsEngineRunnable(UnitTest unitTest)
  {
    _unitTest = unitTest;
  }

  public void run()
 {
   try
   {
     UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
   }
   catch (Exception exception)
   {
     exception.printStackTrace();
   }

   new TestMan_GraphicsEngine(_unitTest);
 }
}

