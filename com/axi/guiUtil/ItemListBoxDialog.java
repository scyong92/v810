/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.axi.util.*;

/**
 * This class displays a list of Strings in a Scroll Pane with a message to be conveyed to the user in a Message Dialog box.
 * 
 * An example of the usages of this class is to display a list of untestable components with this class
 * so that the message dialog will not go out of the screen when the list of untestable components is too long.
 * 
 * @author Ying-Huan.Chu
 */
public class ItemListBoxDialog extends JDialog
{
  private Frame _parentFrame;
  
  private java.util.List<String> _listToBeDisplayed;
  private JScrollPane _listScrollPane;
  private JPanel _mainPanel;
  
  private int _dialogWidth = 500; // default width
  private int _dialogHeight = 400; // default height
  
  private String _dialogTitle = "";
  private String _dialogMessage = "";
  
  private JLabel _dialogMessageLabel;
  
  private JButton _yesButton;
  private JButton _noButton;
  
  private String _yesButtonText = "";
  private String _noButtonText = "";
  
  private boolean _isUserClickedYes = true;
  private boolean _isUserClickedClose = false;
  
  /**
   * @author Ying-Huan.Chu
   */
  public ItemListBoxDialog(Frame parentFrame,
                           String title,
                           String message,
                           String yesButtonText,
                           String noButtonText,
                           java.util.List<String> listToBeDisplayed)
  {
    super(parentFrame, title, true);
    
    Assert.expect(parentFrame != null);
    Assert.expect(title != null);
    Assert.expect(message != null);
    Assert.expect(yesButtonText != null);
    Assert.expect(noButtonText != null);
    Assert.expect(listToBeDisplayed != null);
    
    _parentFrame = parentFrame;
    _dialogTitle = title;
    _dialogMessage = message;
    _yesButtonText = yesButtonText;
    _noButtonText = noButtonText;
    _listToBeDisplayed = listToBeDisplayed;
    
    displayDialog();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public ItemListBoxDialog(Frame parentFrame,
                           String title,
                           String message,
                           String yesButtonText,
                           java.util.List<String> listToBeDisplayed)
  {
    super(parentFrame, title, true);
    
    Assert.expect(parentFrame != null);
    Assert.expect(title != null);
    Assert.expect(message != null);
    Assert.expect(yesButtonText != null);    
    Assert.expect(listToBeDisplayed != null);
    
    _parentFrame = parentFrame;
    _dialogTitle = title;
    _dialogMessage = message;
    _yesButtonText = yesButtonText;    
    _listToBeDisplayed = listToBeDisplayed;
    
    displayDialog();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public ItemListBoxDialog(Frame parentFrame,
                           String title,
                           String message,
                           String yesButtonText,
                           String noButtonText,
                           java.util.List<String> listToBeDisplayed,
                           int dialogWidth,
                           int dialogHeight)
  {
    super(parentFrame, title, true);
    
    Assert.expect(parentFrame != null);
    Assert.expect(title != null);
    Assert.expect(message != null);
    Assert.expect(listToBeDisplayed != null);
    
    _parentFrame = parentFrame;
    _dialogTitle = title;
    _dialogMessage = message;
    _yesButtonText = yesButtonText;
    _noButtonText = noButtonText;
    _dialogWidth = dialogWidth;
    _dialogHeight = dialogHeight;
    
    _listToBeDisplayed = listToBeDisplayed;
    
    displayDialog();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void displayDialog()
  {
    jbInit();
    
    addWindowListener(new WindowListener()
    {
      public void windowClosing(WindowEvent e)
      {
        _isUserClickedYes = false;
        _isUserClickedClose = true;
      }
      public void windowOpened(WindowEvent e) {}
      public void windowClosed(WindowEvent e) {}
      public void windowIconified(WindowEvent e) {}
      public void windowDeiconified(WindowEvent e) {}
      public void windowActivated(WindowEvent e) {}
      public void windowDeactivated(WindowEvent e) {}
    });
    
    getContentPane().add(_mainPanel, BorderLayout.CENTER);
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    pack();

    this.setSize(_dialogWidth, _dialogHeight);
    this.setAutoRequestFocus(true);
    SwingUtils.centerOnComponent(this, _parentFrame);
    setTitle(_dialogTitle);
    setVisible(true);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean isUserClickedYes()
  {
    return _isUserClickedYes;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isUserClickedClose()
  {
    return _isUserClickedClose;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void jbInit()
  {
    JList list = new JList();
    list.setListData(_listToBeDisplayed.toArray());
    _listScrollPane = new JScrollPane();
    _listScrollPane.setViewportView(list);
    
    _yesButton = new JButton();
    _yesButton.setText(_yesButtonText);
    _yesButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        yesButton_actionPerformed();
      }
    });
    _noButton = new JButton();
    _noButton.setText(_noButtonText);
    _noButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        noButton_actionPerformed();
      }
    });
    
    _dialogMessageLabel = new JLabel(_dialogMessage);
    
    JPanel buttonPanel = new JPanel(new FlowLayout());
    if (_yesButtonText.length() > 0) buttonPanel.add(_yesButton);
    if (_noButtonText.length() > 0) buttonPanel.add(_noButton);
    
    _mainPanel = new JPanel(new BorderLayout());
    _mainPanel.add(_dialogMessageLabel, BorderLayout.NORTH);
    _mainPanel.add(_listScrollPane, BorderLayout.CENTER);
    _mainPanel.add(buttonPanel, BorderLayout.SOUTH);
    _mainPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
  }
    
  /**
   * @author Ying-Huan.Chu
   */
  private void yesButton_actionPerformed()
  {
    _isUserClickedYes = true;
    this.dispose();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  private void noButton_actionPerformed()
  {
    _isUserClickedYes = false;
    this.dispose();
  }
}
