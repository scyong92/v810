package com.axi.guiUtil;

import java.awt.*;
import java.awt.geom.*;

import com.axi.util.*;
/**
 * This class will draw cross hairs at the x,y location specified.  It is used
 * by the GraphicsEngine class.  It may not work as a general Renderer.
 *
 * @see GraphicsEngine
 * @see Renderer
 *
 * @author Bill Darbie
 */
class CrossHairRenderer extends ShapeRenderer
{
  private double _xCenter;
  private double _yCenter;
  private Rectangle2D _bounds;

  /**
   * @author Bill Darbie
   */
  protected CrossHairRenderer()
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);

    setCrossHair(0, 0, new Rectangle2D.Double());
  }

  /**
   * @author Bill Darbie
   */
  void setCrossHair(double xCenter, double yCenter, Rectangle2D bounds)
  {
    Assert.expect(bounds != null);

    _xCenter = xCenter;
    _yCenter = yCenter;
    _bounds = bounds;

    refreshData();
  }

  /**
   * @author Bill Darbie
   */
  protected void refreshData()
  {
    double boundsX = _bounds.getX();
    double boundsY = _bounds.getY();

    double boundsWidth = _bounds.getWidth();
    double boundsHeight = _bounds.getHeight();

    Shape verticalLine = new Line2D.Double(_xCenter, boundsY, _xCenter, boundsY + boundsHeight);
    Shape horizontalLine = new Line2D.Double(boundsX, _yCenter, boundsX + boundsWidth, _yCenter);

    GeneralPath generalPath = new GeneralPath();
    generalPath.append(verticalLine, false);
    generalPath.append(horizontalLine, false);

    initializeShape(generalPath);
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    Stroke origStroke = graphics2D.getStroke();

    graphics2D.setStroke(new BasicStroke(2));

    super.paintComponent(graphics);

    graphics2D.setStroke(origStroke);
  }

}
