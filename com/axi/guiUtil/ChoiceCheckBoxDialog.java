package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.util.*;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author bee-hoon.goh
 */
public class ChoiceCheckBoxDialog extends EscapeDialog
{
  private JFrame _frame;
  private int _returnValue; 
  private String _instruction;
  private String _okButtonText;
  private String _cancelButtonText;
  
  JCheckBox []_checkBoxChoices;
  private Map<String, Boolean> _checkBoxOptionToIntegerChoiceMap = new HashMap<String, Boolean>();

  private JPanel _mainPanel = new JPanel();
  private JPanel _messagePanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();
  
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private FlowLayout _messagePanelFlowLayout = new FlowLayout();
  private FlowLayout _buttonPanelFlowLayout = new FlowLayout();
  private GridLayout _innerButtonPanelGridLayout = new GridLayout();
  private JLabel _messageLabel = new JLabel();
  private JPanel _inputPanel = new JPanel();
  private VerticalFlowLayout _inputPanelFlowLayout = new VerticalFlowLayout();
  private Border _innerButtonPanelBorder = BorderFactory.createEmptyBorder(5, 10, 0, 10);
  private Border _mainPanelBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
   
  private JButton _okButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }

  };
  
  private JButton _cancelButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };  

  /**
   * @author bee-hoon.goh
   */
  private ChoiceCheckBoxDialog()
  {
    // do nothing
  }

  /**
   * @author bee-hoon.goh
   */
  public ChoiceCheckBoxDialog(JFrame frame,
                          String title,
                          String message,
                          String okButtonText,
                          String cancelButtonText,
                          Map<String, Boolean> checkBoxOptionToIntegerChoiceMap)
  {
    super(frame, title, true);

    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(message != null);
    Assert.expect(okButtonText != null);
    Assert.expect(cancelButtonText != null);
    Assert.expect(checkBoxOptionToIntegerChoiceMap != null);

    _frame = frame;
    _instruction = message;
    _okButtonText = okButtonText;
    _cancelButtonText = cancelButtonText;

    _checkBoxOptionToIntegerChoiceMap = checkBoxOptionToIntegerChoiceMap;
    
    createDialog();
    pack();

  }
  
  /**
   * @author bee-hoon.goh
   */
  private void createDialog()
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _mainPanel.setBorder(_mainPanelBorder);
    _mainPanelBorderLayout.setVgap(10);
    
    _cancelButton.setText(_cancelButtonText);  
    _okButton.setText(_okButtonText);
    
    _innerButtonPanel.setLayout(_innerButtonPanelGridLayout);
    _innerButtonPanelGridLayout.setHgap(10);
    _innerButtonPanel.setBorder(_innerButtonPanelBorder);
    
    _messagePanel.setLayout(_messagePanelFlowLayout);
    _messagePanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _messagePanelFlowLayout.setHgap(0);
    _messagePanelFlowLayout.setVgap(0);  
    _messageLabel.setText(_instruction);
    
    _inputPanel.setLayout(_inputPanelFlowLayout);
    _inputPanelFlowLayout.setHgap(5);
    _inputPanelFlowLayout.setVgap(5); 
   
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    _buttonPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _buttonPanelFlowLayout.setHgap(0);
    _buttonPanelFlowLayout.setVgap(0);
       
    getContentPane().add(_mainPanel);
    _innerButtonPanel.add(_okButton);
    _innerButtonPanel.add(_cancelButton);
    _messagePanel.add(_messageLabel);
    _buttonPanel.add(_innerButtonPanel);
    
    _mainPanel.add(_messagePanel, BorderLayout.NORTH);
    _mainPanel.add(_inputPanel, BorderLayout.CENTER);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);
    
    getRootPane().setDefaultButton(_okButton);
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okAction();
      }
    });
    
    _checkBoxChoices = new JCheckBox[_checkBoxOptionToIntegerChoiceMap.size()];
    int checkBoxIndex = 0;
    for (Map.Entry<String, Boolean> entry : _checkBoxOptionToIntegerChoiceMap.entrySet())
    {
      _checkBoxChoices[checkBoxIndex] = new JCheckBox(entry.getKey());
      _inputPanel.add(_checkBoxChoices[checkBoxIndex]);
      _checkBoxChoices[checkBoxIndex].setSelected(entry.getValue());
      checkBoxIndex++;
    }  
  }
  
  /**
   * @author bee-hoon.goh
   */
  public int showDialog()
  {
    SwingUtils.centerOnComponent(this, _frame);
    setVisible(true);
    return _returnValue;
  }
  
  /**
   * @author bee-hoon.goh
   */
  private void okAction()
  {   
    int checkBoxIndex = 0;
    for (Map.Entry<String, Boolean> entry : _checkBoxOptionToIntegerChoiceMap.entrySet())
    {
      entry.setValue(_checkBoxChoices[checkBoxIndex].isSelected());
      
      checkBoxIndex++;
    }
    
    _returnValue = JOptionPane.OK_OPTION;

    dispose();
  }
  
  /**
   * @author bee-hoon.goh
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    cancelAction();
  }
  
  /**
   * @author bee-hoon.goh
   */
  private void cancelAction()
  {
    _returnValue = JOptionPane.CANCEL_OPTION;
    dispose();
  }

  /**
   * @author bee-hoon.goh
   */
  public Map<String, Boolean> getSelectedValue()
  {
    return _checkBoxOptionToIntegerChoiceMap;
  }
}
