package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * This panel will be collapsed / expanded when double-clicked.
 * 
 * @author Ying-Huan.Chu
 */
public class CollapsiblePanel extends JPanel 
{
  private static String _EXPAND_PANEL_SYMBOL = "+";
  private static String _HIDE_PANEL_SYMBOL = "-";
  private String _title = "";
  private TitledBorder _border;
  
  /**
   * @author Ying-Huan.Chu
   */
  public CollapsiblePanel() 
  {
    _border = BorderFactory.createTitledBorder(_title);
    setBorder(_border);
    BorderLayout borderLayout = new BorderLayout();
    setLayout(borderLayout);
    addMouseListener(new MouseAdapter() 
    {
      public void mouseClicked(MouseEvent e) 
      {
        toggleVisibility();
      }
    });
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  ComponentListener contentComponentListener = new ComponentAdapter() 
  {
    @Override
    public void componentShown(ComponentEvent e) 
    {
      updateBorderTitle();
    }
    @Override
    public void componentHidden(ComponentEvent e) 
    {
      updateBorderTitle();
    }
  };
  
  /**
   * @author Ying-Huan.Chu
   */
  public String getTitle() 
  {
    return _title;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setTitle(String title) 
  {
    firePropertyChange("", this._title, this._title = title);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public Component add(Component comp) 
  {
    comp.addComponentListener(contentComponentListener);
    Component r = super.add(comp);
    updateBorderTitle();
    return r;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public Component add(String name, Component comp) 
  {
    comp.addComponentListener(contentComponentListener);
    Component r = super.add(name, comp);
    updateBorderTitle();
    return r;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public Component add(Component comp, int index) 
  {
    comp.addComponentListener(contentComponentListener);
    Component r = super.add(comp, index);
    updateBorderTitle();
    return r;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void add(Component comp, Object constraints) 
  {
    if (comp instanceof JScrollPane) // Add ScrollPane's components separately. 
    {
      JScrollPane scrollPane = (JScrollPane)comp;
      scrollPane.getViewport().addComponentListener(contentComponentListener);
      super.add(scrollPane.getViewport(), BorderLayout.CENTER);
      super.add(scrollPane.getVerticalScrollBar(), BorderLayout.EAST);
    }
    else
    {
      comp.addComponentListener(contentComponentListener);
      super.add(comp, constraints);
    }
    updateBorderTitle();
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void add(Component comp, Object constraints, int index) 
  {
    comp.addComponentListener(contentComponentListener);
    super.add(comp, constraints, index);
    updateBorderTitle();
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void remove(int index) 
  {
    Component comp = getComponent(index);
    comp.removeComponentListener(contentComponentListener);
    super.remove(index);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void remove(Component comp) 
  {
    comp.removeComponentListener(contentComponentListener);
    super.remove(comp);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void removeAll() 
  {
    for (Component c : getComponents()) 
    {
      c.removeComponentListener(contentComponentListener);
    }
    super.removeAll();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  protected void toggleVisibility() 
  {
    toggleVisibility(hasInvisibleComponent());
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setComponentVisible(boolean visible)
  {
    toggleVisibility(visible);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  protected void toggleVisibility(boolean visible) 
  {
    for (Component c : getComponents()) 
    {
      c.setVisible(visible);
    }
    updateBorderTitle();
  }
  
  /**
   * @author rgd
   */
  protected void updateBorderTitle() 
  {
    String arrow = "";
    if (getComponentCount() > 0) 
    {
      arrow = (hasInvisibleComponent()? _EXPAND_PANEL_SYMBOL : _HIDE_PANEL_SYMBOL);
    }
    _border.setTitle(_title +" "+ arrow);
    repaint();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  protected final boolean hasInvisibleComponent() 
  {
    for (Component c : getComponents()) 
    {
      if (c instanceof JScrollPane)
      {
        JScrollPane scrollPane = (JScrollPane)c;
        if (scrollPane.getViewport().isVisible() == false)
        {
          return true;
        }
      }
      if (c.isVisible() == false) 
      {
        return true;
      }
    }
    return false;
  }
}

