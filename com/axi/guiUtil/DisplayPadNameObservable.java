package com.axi.guiUtil;

import java.util.*;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class DisplayPadNameObservable extends Observable 
{
    /**
     * @author Cheah Lee Herng
     */
    DisplayPadNameObservable()
    {
        // do nothing
    }

    /**
     * @author Cheah Lee Herng
     */
    public void setDisplayPadName(boolean setPadNameVisible)
    {
        setChanged();
        notifyObservers(new Boolean(setPadNameVisible));
    }
}
