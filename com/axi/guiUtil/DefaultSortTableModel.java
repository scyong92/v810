package com.axi.guiUtil;

import java.util.*;
import javax.swing.table.*;

/**
 * This class extends the default table model, adding in column sorting
 * It is further extended to allow row selection by matching typeahead characters
 *
 * @author Claude Duguay of JavaPro
 * @author George Booth
 */
public class DefaultSortTableModel extends DefaultTableModel implements SortTableModel
{
  public DefaultSortTableModel() {}

  public DefaultSortTableModel(int rows, int cols)
  {
    super(rows, cols);
  }

  public DefaultSortTableModel(Object[][] data, Object[] names)
  {
    super(data, names);
  }

  public DefaultSortTableModel(Object[] names, int rows)
  {
    super(names, rows);
  }

  public DefaultSortTableModel(Vector names, int rows)
  {
    super(names, rows);
  }

  public DefaultSortTableModel(Vector data, Vector names)
  {
    super(data, names);
  }

  public boolean isSortable(int col)
  {
    return true;
  }

  public void sortColumn(int col, boolean ascending)
  {
    Collections.sort(getDataVector(), new ColumnComparator(col, ascending));
  }

  /**
   * @author George Booth
   */
  public int getRowForDataStartingWith(String key)
  {
    return -1;
  }

}

