package com.axi.guiUtil;

import java.awt.*;

import javax.swing.*;
import javax.swing.plaf.*;
import javax.swing.plaf.basic.*;
import javax.swing.plaf.synth.*;
import javax.swing.text.*;

/**
 * Copied from the internet, this extension of the BasicButtonUI essentially tries
 * to mimic the XP (Luna) look and feel for XP buttons (rounded edges, etc) while providing
 * a way to fill in the interior with a custom background color.  It does a pretty good job,
 * but when the custom background is NOT set, it lacks the subtle shading of color that the
 * normal XP rendering provides.  Don't use unless you need to set custom background colors.
 * @author Andy Mechtenberg
 */
public class ColoredButtonUI extends SynthButtonUI
{
// Created for compatibility with system UI.
  private final static ColoredButtonUI _coloredButtonUI = new ColoredButtonUI();
  private final int ARC_HEIGHT = 6;
  private final int ARC_WIDTH = 6;

  AbstractButton _button;

  /**
   * @author Andy Mechtenberg
   */
  public ColoredButtonUI()
  {
    // do nothing
  }

  // ********************************
  // Create PLAF
  // ********************************
  public static ComponentUI createUI(JComponent c)
  {
    return _coloredButtonUI;
  }

  /**
   * Override protected paint method
   * @author Andy Mechtenberg
   */
  public void paint(Graphics graphics, JComponent component)
  {
    _button = (AbstractButton)component;
    paintBackground(graphics);
    paintBorder(graphics);
    Rectangle textRectangle = getTextRectangle();
    // This is how Java sets the text in buttons.  One is for
    // HTML text, the other for normal text
    View textView = (View)component.getClientProperty(BasicHTML.propertyKey);
    if (textView != null)
      textView.paint(graphics, textRectangle);
    else
      paintText(graphics, _button, textRectangle, _button.getText());
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void paintBackground(Graphics g)
  {
    Rectangle buttonRect = _button.getBounds();

    // Clear any attempts from the JButton control to
    // color the background manually. We want to control color.
    // First set everything to be the same color as parent background color...
    g.setColor(_button.getParent().getBackground());
    g.fillRect(0,
               0,
               (int)buttonRect.getWidth(),
               (int)buttonRect.getHeight());

    if ((_button.getModel().isPressed()) ||
        (_button.isSelected()))
    {
  // *** Paint the background when pressed or toggled.
      g.setColor(_button.getBackground().darker());
      g.fillRoundRect(0,
                      0,
                      (int)buttonRect.getWidth() - 1,
                      (int)buttonRect.getHeight() - 1,
                      ARC_HEIGHT,
                      ARC_WIDTH);
    }
    else if (_button.getModel().isRollover())
    {
  // *** Paint the background when the mouse if over the button.
      g.setColor(new Color(251, 202, 106));
      g.fillRoundRect(0,
                      0,
                      (int)buttonRect.getWidth() - 1,
                      (int)buttonRect.getHeight() - 1,
                      ARC_HEIGHT,
                      ARC_WIDTH);
      g.setColor(_button.getBackground());
      g.fillRect(3,
                 3,
                 (int)buttonRect.getWidth() - 6,
                 (int)buttonRect.getHeight() - 6);
    }
    else if ((_button.isFocusPainted()) &&
             (_button.hasFocus()))
    {
  // *** Paint the background when the button has focus
      g.setColor(new Color(142, 175, 240));
      g.fillRoundRect(0,
                      0,
                      (int)buttonRect.getWidth() - 1,
                      (int)buttonRect.getHeight() - 1,
                      ARC_HEIGHT,
                      ARC_WIDTH);
      g.setColor(_button.getBackground());
      g.fillRect(3,
                 3,
                 (int)buttonRect.getWidth() - 6,
                 (int)buttonRect.getHeight() - 6);
    }
    else
    {
      // *** Paint the background normally.
      g.setColor(_button.getBackground());
      g.fillRoundRect(0,
                      0,
                      (int)buttonRect.getWidth() - 1,
                      (int)buttonRect.getHeight() - 1,
                      ARC_HEIGHT,
                      ARC_WIDTH);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void paintBorder(Graphics g)
  {
    Rectangle buttonRect = _button.getBounds();

    if (!_button.isEnabled())
    {
      g.setColor(new Color(201, 199, 186));
    }
    else
    {
      g.setColor(new Color(0, 60, 116));
    }

    g.drawRoundRect(0,
                    0,
                    (int)buttonRect.getWidth() - 1,
                    (int)buttonRect.getHeight() - 1,
                    ARC_HEIGHT,
                    ARC_WIDTH);
  }


//  protected void paintText(Graphics g)
//  {
//    ButtonModel model = _button.getModel();
//    FontMetrics fm = g.getFontMetrics();
//    int mnemonicIndex = _button.getDisplayedMnemonicIndex();
//    Rectangle textRect = getTextRectangle();
//
//    /* Draw the Text */
//    if (model.isEnabled())
//    {
//      /*** paint the text normally */
//      g.setColor(_button.getForeground());
//      BasicGraphicsUtils.drawStringUnderlineCharAt(g,
//                                                   _button.getText(),
//                                                   mnemonicIndex,
//                                                   textRect.x + GetTextShiftOffset(),
//                                                   textRect.y + fm.getAscent() + GetTextShiftOffset());
//    }
//    else
//    {
//      /*** paint the text disabled ***/
//      g.setColor(_button.getBackground().brighter());
//      BasicGraphicsUtils.drawStringUnderlineCharAt(g,
//                                                   _button.getText(),
//                                                   mnemonicIndex,
//                                                   textRect.x,
//                                                   textRect.y + fm.getAscent());
//      g.setColor(_button.getBackground().darker());
//      BasicGraphicsUtils.drawStringUnderlineCharAt(g,
//                                                   _button.getText(),
//                                                   mnemonicIndex,
//                                                   textRect.x - 1,
//                                                   textRect.y + fm.getAscent() - 1);
//    }
//  }
//
//  private int GetTextShiftOffset()
//  {
//    ButtonModel model = _button.getModel();
//    if ((model.isPressed()) ||
//        (_button.isSelected()))
//    {
//      final String pp = "Button" + ".";
//      return ((Integer)UIManager.get(pp + "textShiftOffset")).intValue();
//    }
//    else
//    {
//      return 0;
//    }
//  }

  /**
   * Returns the bounding rectangle for the component text.
   * @author Andy Mechtenberg
   */
  private Rectangle getTextRectangle()
  {
    String text = _button.getText();
    Icon icon = (_button.isEnabled()) ? _button.getIcon() : _button.getDisabledIcon();

    if ((icon == null) && (text == null))
    {
      return null;
    }

    Rectangle paintIconR = new Rectangle();
    Rectangle paintTextR = new Rectangle();
    Rectangle paintViewR = new Rectangle();
    Insets paintViewInsets = new Insets(0, 0, 0, 0);

    paintViewInsets = _button.getInsets(paintViewInsets);
    paintViewR.x = paintViewInsets.left;
    paintViewR.y = paintViewInsets.top;
    paintViewR.width = _button.getWidth() - (paintViewInsets.left + paintViewInsets.right);
    paintViewR.height = _button.getHeight() - (paintViewInsets.top + paintViewInsets.bottom);

    Graphics g = _button.getGraphics();
    if (g == null)
    {
      return null;
    }
    String clippedText = SwingUtilities.layoutCompoundLabel(
        (JComponent)_button,
        g.getFontMetrics(),
        text,
        icon,
        _button.getVerticalAlignment(),
        _button.getHorizontalAlignment(),
        _button.getVerticalTextPosition(),
        _button.getHorizontalTextPosition(),
        paintViewR,
        paintIconR,
        paintTextR,
        0);

    return paintTextR;
  }
}
