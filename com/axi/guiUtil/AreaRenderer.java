package com.axi.guiUtil;

import java.awt.*;
import java.awt.geom.*;

import com.axi.util.*;

/**
 * @author George A. David
 */
public class AreaRenderer extends ShapeRenderer
{
  private Area _area;

  /**
   * @author George A. David
   */
  public AreaRenderer(Area area, boolean selectable)
  {
    super(selectable);
    _area = area;
    setFillShape(true);
    refreshData();
  }

  /**
   * @author George A. David
   */
  protected void refreshData()
  {
    Assert.expect(_area != null);
    initializeShape(_area);
  }
}

