package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.axi.util.*;

/**
 * A dialog to display any large textual information in a scrollable text area.
 * @author Laura Cormos
 */
public class TextDialog extends JDialog
{
  private JTextArea _textArea = new JTextArea();
  private JScrollPane _textAreaScrollPane = new JScrollPane();
  private JButton _dismissButton = new JButton();

  /**
   * @author Laura Cormos
   * @author khang shian, sham
   */
  public TextDialog(JFrame parent, String textToDisplay, String buttonText, String title, boolean modal)
  {
    super(parent, title, modal);
    Assert.expect(parent != null);
    Assert.expect(title != null);
    Assert.expect(textToDisplay != null);
    Assert.expect(buttonText != null);
    _textArea.setText(textToDisplay);
    _dismissButton.setText(buttonText);
    jbInit(60, 20);
    setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    pack();
  }

  /**
   * @author khang shian, sham
   */
  public TextDialog(JFrame parent, String textToDisplay, String buttonText, String title, int columns, int rows, boolean modal)
  {
    super(parent, title, modal);
    Assert.expect(parent != null);
    Assert.expect(title != null);
    Assert.expect(textToDisplay != null);
    Assert.expect(buttonText != null);
    _textArea.setText(textToDisplay);
    _dismissButton.setText(buttonText);
    jbInit(columns, rows);
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    pack();
  }

 /**
   * @author Laura Cormos
   * @author khang shian, sham
   */
  private void jbInit(int columns, int rows)
  {
    JPanel mainPanel = new JPanel(new BorderLayout(0, 10));
    mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 10));
    this.getContentPane().add(mainPanel, BorderLayout.CENTER);

    JPanel buttonPanel = new JPanel(new FlowLayout());
    buttonPanel.add(_dismissButton);
    _dismissButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });

    _textArea.setEditable(false);
    _textArea.setOpaque(true);
    _textArea.setColumns(columns);
    _textArea.setRows(rows);
    _textArea.setLineWrap(true);
    _textArea.setCaretPosition(0);
    _textAreaScrollPane.getViewport().add(_textArea);

    mainPanel.add(_textAreaScrollPane, BorderLayout.CENTER);
    mainPanel.add(buttonPanel, BorderLayout.SOUTH);
  }
}
