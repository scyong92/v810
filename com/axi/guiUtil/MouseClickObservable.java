package com.axi.guiUtil;

import java.awt.event.*;
import java.util.*;

import com.axi.util.*;

/**
 * This class is used by the GraphicsEngine class and will call any
 * Observers update method with the coordinates of a left mouse click
 *
 * @author Bill Darbie
 */
public class MouseClickObservable extends Observable
{
  private static MouseClickObservable _instance;

  /**
   * @author Bill Darbie
   */
  MouseClickObservable()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  void setMouseClickEvent(MouseEvent mouseEvent)
  {
    Assert.expect(mouseEvent != null);
    setChanged();
    notifyObservers(mouseEvent);
  }
}
