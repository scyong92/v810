package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * @author siew-yeng.phang
 */
public class RadioButtonCellEditor extends DefaultCellEditor
{
  private JRadioButton _radioButton = new JRadioButton();

  public RadioButtonCellEditor(Color backgroundColor, Color foregroundColor)
  {
    super(new JCheckBox());
    
    final DefaultCellEditor thisEditor = this;
    
    _radioButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        thisEditor.stopCellEditing();

      }
    });
  }   
  
  /**
   * @author siew-yeng.phang
   */
  public java.awt.Component getTableCellEditorComponent(JTable table, Object value, boolean isSelectable, int row, int column)
  {
    Color alternativeColor = (Color) UIManager.get ("Table.alternateRowColor");
    Color backgroundColor = Color.WHITE;
    
    java.awt.Component editor = super.getTableCellEditorComponent(table, _radioButton.isSelected(), isSelectable, row, column);
    JCheckBox checkBox = (JCheckBox) editor;
    
    _radioButton.setSelected(checkBox.isSelected());
    _radioButton.setHorizontalAlignment((int)JRadioButton.CENTER_ALIGNMENT);
    
    if (isSelectable)
    {
      _radioButton.setForeground(table.getSelectionForeground());
      _radioButton.setBackground(table.getSelectionBackground());
    }
    else
    {
      _radioButton.setForeground(table.getForeground());
      if(row%2 == 0)
        _radioButton.setBackground(backgroundColor);
      else
        _radioButton.setBackground(alternativeColor);
    }
    
    return _radioButton;
    
  }
}
