package com.axi.guiUtil;

import javax.swing.*;
import javax.swing.plaf.basic.*;
import java.awt.Dimension;

/**
 * @author Wei Chin
 * @version 1.0
 */
public class ScrollableComboBox extends JComboBox
{
  public ScrollableComboBox()
  {
    super();
    setUI(new myComboUI());
    updateUI();
  } //end of default constructor

  public class myComboUI extends BasicComboBoxUI
  {
    protected ComboPopup createPopup()
    {
      BasicComboPopup popup = new BasicComboPopup(comboBox)
      {
        protected JScrollPane createScroller()
        {
          JScrollPane newJScrollPane = new JScrollPane(list, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                                 ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
          newJScrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(10,10));
          newJScrollPane.getHorizontalScrollBar().setPreferredSize(new Dimension(10,10));
          return newJScrollPane;
        } //end of method createScroller
      };
      return popup;
    } //end of method createPopup
  } //end of inner class myComboUI
}
