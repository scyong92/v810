package com.axi.guiUtil;

import java.awt.*;
import java.awt.geom.*;

import javax.swing.*;

import com.axi.util.*;

/**
 * This class handles the drawing, and more importantly, the scaling of an image
 * (passed in) so it maintains the aspect ration properly.  It uses an AffineTransform to
 * scale the image to fit itself, and draws it by overriding paintComponent
 * @author Andy Mechtenberg
 */
public class ImageDisplayPanel extends JPanel
{
  private Image _image = null;
  private int _imageWidth;
  private int _imageHeight;
  private boolean _shouldRotateImage = false;
  private boolean _limitSizeToMax = false;

  /**
   * Constructor which uses the ImageIO class to read in the images
   * This is a newer Java class and should be faster.
   * @author Andy Mechtenberg
   */
  public ImageDisplayPanel(Image image)
  {
    Assert.expect(image != null);
    setImage(image);
  }

  /**
   * Constructor which takes no panel object, as the panel program has not been loaded yet
   * We still need to create the object so the gui can be layed out correctly.
   * @author Andy Mechtenberg
   */
  public ImageDisplayPanel()
  {
    // do nothing
  }

  /**
   * Allow the user of this class to change the image on the fly
   * @author Andy Mechtenberg
   */
  public void setImage(Image image)
  {
    Assert.expect(image != null);
    _image = image;
    _imageWidth = _image.getWidth(null);
    _imageHeight = _image.getHeight(null);
    repaint();
  }

  /**
   * Clears the current image, so nothing special is drawn
   * @author Andy Mechtenberg
   */
  public void clearImage()
  {
    _image = null;
    repaint();
  }

  /**
   * Set mode so the image to NOT scale larger than it's native size
   * @author Andy Mechtenberg
   */
  public void setLimitImageSizeToMaximum(boolean limitSizeToMax)
  {
    _limitSizeToMax = limitSizeToMax;
  }

  /**
   * Tells the class to rotate the image 180 degrees
   * @author Andy Mechtenberg
   */
  public void setRotateImage(boolean rotateImage)
  {
    _shouldRotateImage = rotateImage;
    repaint();
  }

  /**
   * Here, we want to draw the panel image to fit inside this JPanel.
   * We'll scale it using an AffineTransform, being careful to keep the
   * original aspect ratio.
   * @author Andy Mechtenberg
   */
  public void paintComponent(Graphics g)
  {
    super.paintComponent(g);
    if (_image == null)
      return;
    Graphics2D g2d = (Graphics2D)g;

    // get width and height of the jpanel
    int panelWidth = getWidth();
    int panelHeight = getHeight();
    double scale = 1.0;
    double xCenter = 0.0;
    double yCenter = 0.0;
    AffineTransform tx = new AffineTransform();

    // calculate the proper scaling factor (scaleX and scaleY should be the same to retain the aspect ratio)
    // so we'll just use one scale variable for both X and Y

    double imageWidthToHeightRatio = (double)_imageWidth / (double)_imageHeight;
    double panelRatio = (double)panelWidth / (double)panelHeight;
    if (imageWidthToHeightRatio < panelRatio)
      scale = (double)(panelHeight) / (double)(_imageHeight); // image is limited by height
    else
      scale = (double)(panelWidth) / (double)(_imageWidth);    // image is limited by width

    if ((_limitSizeToMax) && (scale > 1.0))
      scale = 1.0;

    // translate the image so it's centered in the panel, and not left justified (0,0)
    xCenter = (double)panelWidth / 2.0 - ((double)_imageWidth * scale) / 2.0;
    yCenter = (double)panelHeight / 2.0 - ((double)_imageHeight * scale) / 2.0;

    double imageCenterX = _imageWidth / 2.0;
    double imageCenterY = _imageHeight / 2.0;
    // apply the scale factor AFTER the translate to the transform otherwise it doesn't work.


    tx.translate(xCenter, yCenter);
    tx.scale(scale, scale);

//    // rotate 180 degrees if the system is a right to left (instead of the default left to right)
    if (_shouldRotateImage)
      tx.rotate(Math.toRadians(180.0), imageCenterX, imageCenterY);

    // debug coloring to see the extent of the panel to tell if it's centered correctly.
//    g2d.setPaint(Color.CYAN);
//    g2d.fill3DRect(0,0, panelWidth, panelHeight, false);

    g2d.drawImage(_image, tx, null);
  }
}
