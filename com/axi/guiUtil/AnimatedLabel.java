package com.axi.guiUtil;

import java.awt.*;

import javax.swing.*;

import com.axi.util.*;

/**
 * This class implements a JLabel that will cycle through a set of ImageIcon instances as an animation.
 * @author Steve Anonson
 */
public class AnimatedLabel extends JLabel implements Runnable
{
  protected Icon[] _icons = null;
  protected int _index = 0;
  protected boolean _running = false;

  /**
   * Pass in the base name of the gif files and the number of files.  The name of the
   * gif files must be <base name>#.gif, where the base name is passed in, the # is
   * replaced by a number 1 to numGifs.
   *
   * @param   gifName - base name of the gif files that make up the animation sequence.
   * @param   numGifs - number of gif files to load.
   * @author Steve Anonson
   */
  public AnimatedLabel(String gifName, int numGifs)
  {
    Assert.expect(gifName != null);
    Assert.expect(numGifs > 0);

    _icons = new Icon[numGifs];
    for (int k = 0; k < numGifs; k++)
      _icons[k] = new ImageIcon(gifName + (k + 1) + ".gif");
    init();
  }

  /**
   * Pass in an array of ImageIcon instances to use for the animation.
   *
   * @param   icons - array of ImageIcon instances.
   * @author Steve Anonson
   */
  public AnimatedLabel(Icon[] icons)
  {
    Assert.expect(icons != null);

    _icons = icons;
    init();
  }

  /**
   * Initializes the animation by setting the initial icon and starting the
   * animation thread.
   * @author Steve Anonson
   */
  private void init()
  {
    if (_icons.length == 0)
      return;

    _index = 0;
    setIcon(_icons[_index]);

    // If there is more than one icon, start the animation thread.
    if (_icons.length > 1)
    {
      Thread tr = new Thread(this);
      tr.setPriority(Thread.MAX_PRIORITY);
      tr.start();
    }
  }

  /**
   * Turn the animation on or off.
   * @author Steve Anonson
   */
  public void setRunning(boolean running)
  {
    _running = running;
  }

  /**
   * Return whether the animation is currently running.
   * @author Steve Anonson
   */
  public boolean isRunning()
  {
    return _running;
  }

  /**
   * @author Steve Anonson
   */
  private void nextIcon()
  {
    setIcon(_icons[_index]);
    Graphics g = getGraphics();
    _icons[_index].paintIcon(this, g, 0, 0);
  }

  /**
   * Animation method running on the animation thread.
   * @author Steve Anonson
   */
  public void run()
  {
    try
    {
      while (true)
      {
        if (isRunning())
        {
          _index++;
          if (_index >= _icons.length)
            _index = 0;
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              AnimatedLabel.this.nextIcon();
            }
          });
        }
        else
        {
          if (_index > 0)
          {
            _index = 0;
          }
        }
        try
        {
          Thread.sleep(500);
        }
        catch (Exception ex)
        {
          // do nothing
        }
      }
    }
    catch(Throwable throwable)
    {
      Assert.logException(throwable);
    }
  }
}
