package com.axi.guiUtil;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

/**
 *
 * <p>Title: TextCellRenderer</p>
 * <p>Description: Table cell renderer to display text with specified horizontal alignment.</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company:  Agilent Technogies</p>
 * @author Carli Connally
 * @version 1.0
 */
public class TextCellRenderer extends JLabel implements TableCellRenderer
{
  int _horizontalAlign = JLabel.LEFT;

  /**
   * @param horizontalAlign Horizontal alignment for text.  Use the JLabel constants (i.e. JLabel.CENTER).
   * @author Carli Connally
   */
  public TextCellRenderer(int horizontalAlign)
  {
    _horizontalAlign = horizontalAlign;
  }

  /**
   * Default horizontal center alignment.
   * @author Carli Connally
   */
  public TextCellRenderer()
  {
  }

  /**
   * @param table Table instance to display the data
   * @param value Value to be displayed
   * @param isSelected Whether the cell is selected
   * @param hasFocus Whether the cell has focus
   * @param row Index of the row
   * @param column Index of the column
   * @return Component to display the text
   * @author Carli Connally
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                                                  boolean hasFocus, int row, int column)
  {

    setOpaque(true);

    if (isSelected)
    {
      setForeground(table.getSelectionForeground());
      setBackground(table.getSelectionBackground());
    }
    else
    {
      setForeground(table.getForeground());
      setBackground(table.getBackground());
    }

    if(table.isRowSelected(row))
      setBackground(table.getSelectionBackground());
    else
    {
      if (hasFocus)
        setBackground(table.getSelectionBackground());
      else
        setBackground(table.getBackground());
    }
    setText(value.toString());
    setHorizontalAlignment(_horizontalAlign);

    return this;
  }
}
