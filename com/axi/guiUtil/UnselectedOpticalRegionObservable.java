/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.guiUtil;

import java.util.*;

import com.axi.util.*;

/**
 *
 * @author Jack Hwee
 */
public class UnselectedOpticalRegionObservable extends Observable
{
  /**
   * @author Jack Hwee
   */
  UnselectedOpticalRegionObservable()
  {
    // do nothing
  }

  /**
   * @author Jack Hwee
   */
  public void setUnselectedOpticalRegionRenderer(Collection<Renderer> renderers)
  {
    Assert.expect(renderers != null);
    setChanged();
    notifyObservers(renderers);
  }
}
