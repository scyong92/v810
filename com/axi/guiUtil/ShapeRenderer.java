package com.axi.guiUtil;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;

/**
 * This is the base class for all Renderers used by the GraphicsEngine class that render a
 * java.awt.Shape.  To create your own Renderer class extend this one.  Note that your constructor MUST call
 * initializeShape().
 *
 * Implementation note: Note that this class uses a _rendererShape to control what is drawn.
 * The Shape object itself needs to always have an origin of 0,0.  The Renderers _x, _y coordinates
 * need to have the correct coordinate.  Any translating must occur on the Renderers _x, _y bounds
 * and NOT on the _rendererShape.  Any scaling must happen on the Renderers _x, _y, _width, _height
 * AND on the _rendererShape.
 *
 * @see GraphicsEngine
 *
 * @author Bill Darbie
 */

public abstract class ShapeRenderer extends Renderer
{
  private boolean _fillShape = false;

  private ArrayList<Shape> _shapeList = null;

  /**
   * Construct a new renderer.  Specify if the Renderer can be selected by a mouse click.
   *
   * @param isSelectable is true if the Renderer should respond to a mouse click
   * @author Bill Darbie
   */
  protected ShapeRenderer(boolean isSelectable)
  {
    super(isSelectable);
  }

  /**
   * Tells the component if it should
   * fill the shape instead of just
   * drawing the outline.
   * @author George A. David
   */
  public void setFillShape(boolean fillShape)
  {
    _fillShape = fillShape;
  }

  /**
   * This method should be called by any derived classes constructor!
   * @author Bill Darbie
   * @author Patrick Lacz
   */
  protected void initializeShape(Shape... shapeArray)
  {
    Assert.expect(shapeArray != null);

    Rectangle2D unionOfShapeBounds = null;
    for (Shape shape : shapeArray)
    {
      if (unionOfShapeBounds == null)
        unionOfShapeBounds = shape.getBounds2D();
      else
        unionOfShapeBounds = unionOfShapeBounds.createUnion(shape.getBounds2D());
    }

    _xInPixels = unionOfShapeBounds.getX();
    _yInPixels = unionOfShapeBounds.getY();
    _widthInPixels = unionOfShapeBounds.getWidth();
    _heightInPixels = unionOfShapeBounds.getHeight();

    setBounds();
    setStroke(new BasicStroke(1));

    // now translate the shape so it is at coordinate 0,0
    // This is required for the paintComponent() method to work properly
    AffineTransform at = AffineTransform.getTranslateInstance( -_xInPixels, -_yInPixels);

    _shapeList = new ArrayList<Shape>();

    for (Shape shape : shapeArray)
    {
      _shapeList.add(at.createTransformedShape(shape));
    }
  }

  /**
   * @return the Shape that this Renderer uses to draw.
   * @author Bill Darbie
   * @author Patrick Lacz
   */
  public Shape getShape()
  {
    Assert.expect(_shapeList != null);
    Assert.expect(_shapeList.isEmpty() == false);
    return _shapeList.get(0);
  }

  /**
   * @author Patrick Lacz
   */
  public int getNumberOfShapes()
  {
    Assert.expect(_shapeList != null);
    return _shapeList.size();
  }

  /**
   * @return the Shape that this Renderer uses to draw.
   * @author Patrick Lacz
   */
  public Shape getShape(int i)
  {
    Assert.expect(_shapeList != null);
    Assert.expect(_shapeList.size() > i);
    return _shapeList.get(i);
  }

  /**
   * Apply the passed in transform to this Renderer without modifying _currentTransform.
   * @author Bill Darbie
   */
  protected void applyTransform(AffineTransform transform)
  {
    Assert.expect(transform != null);
    Assert.expect(_shapeList != null);

    // apply the transform to the bounds of this Renderer
    Shape boundsShape = new Rectangle2D.Double(_xInPixels, _yInPixels, _widthInPixels, _heightInPixels);
    boundsShape = transform.createTransformedShape(boundsShape);
    Rectangle2D bounds = boundsShape.getBounds2D();

    // take the Renderers _xInPixels, _yInPixels coordinate and transform them
    // do not directly transform the Renderers _widthInPixels and _heightInPixels since we need to make
    // sure the _widthInPixels and _heightInPixels match exactly to the shape after
    // it is translated
    _xInPixels = bounds.getX();
    _yInPixels = bounds.getY();

    // apply the transform to the shape
    for (int i = 0 ; i < _shapeList.size() ; ++i)
    {
      _shapeList.set(i, transform.createTransformedShape(_shapeList.get(i)));
    }

    // set the Renderers _widthInPixels and _heightInPixels to match the transformed shape
    Rectangle2D unionOfShapeBounds = null;
    for (Shape shape : _shapeList)
    {
      if (unionOfShapeBounds == null)
        unionOfShapeBounds = shape.getBounds2D();
      else
        unionOfShapeBounds = unionOfShapeBounds.createUnion(shape.getBounds2D());
    }

    double shapeX = unionOfShapeBounds.getX();
    double shapeY = unionOfShapeBounds.getY();
    _widthInPixels = unionOfShapeBounds.getWidth();
    _heightInPixels = unionOfShapeBounds.getHeight();

    // update the JComponents bounds to match _xInPixels, _yInPixels, _widthInPixels, _heightInPixels
    // this is what Swing uses to know the bounds of the Renderer
    setBounds();

    // now translate the shape so it is at coordinate 0,0
    // This is required to for the paintComponent() method to work properly
    AffineTransform at = AffineTransform.getTranslateInstance( -shapeX, -shapeY);

    for (int i = 0 ; i < _shapeList.size() ; ++i)
    {
      _shapeList.set(i, at.createTransformedShape(_shapeList.get(i)));
    }
  }

  /**
   * This method does the actual drawing on the screen.
   * @author Bill Darbie
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;
    Color origColor = graphics2D.getColor();
    graphics2D.setColor(getForeground());
    Stroke origStroke = graphics2D.getStroke();
    if(hasStroke())
      graphics2D.setStroke(new BasicStroke(2));

    for (Shape shape : _shapeList)
    {
      if (_fillShape)
        graphics2D.fill(shape);
      else
        graphics2D.draw(shape);
    }

    graphics2D.setColor(origColor);
    graphics2D.setStroke(origStroke);
  }

  /**
   * This method needs to overridden to get the most recent data required
   * to draw the appropriate Shape.  Your method needs to call initializeShape()
   * with the new Shape.
   * @author Bill Darbie
   */
  protected abstract void refreshData();

}
