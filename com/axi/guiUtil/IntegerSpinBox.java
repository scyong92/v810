package com.axi.guiUtil;

import java.awt.*;
import java.beans.*;
import java.lang.IllegalArgumentException;
import java.text.DecimalFormat;
import javax.swing.*;
import javax.swing.event.*;

import com.axi.util.*;

/**
 * This class provides a text field that accepts only integer values
 * as input combined with two arrow buttons which increment or decrement the
 * text field value by some amount.  Range checking is availble to limit the
 * text field value.
 *
 * @author  Steve Anonson
 */
public class IntegerSpinBox extends JPanel implements DocumentListener
{
  private IntegerRange _range = null;
  private int _increment = 1;
  private int _columns = 5;
  private int _current;
  private BorderLayout _borderLayout1 = new BorderLayout();
  private DecimalFormat _format = new DecimalFormat("#####");
  private NumericTextField _numberTF = new NumericTextField();
  private Spinner _spinner = new Spinner();

  /**
   * Creates an unlimited range spin box with 0 as the initial value and an
   * increment amount of 1.
   * @author Steve Anonson
   */
  public IntegerSpinBox()
  {
    this(0, null, 1);
  }

  /**
   * Constructor.
   * Creates an unlimited range spin box with an increment amount of 1.
   *
   * @param   initialValue - initial value for the spin box.
   * @author Steve Anonson
   */
  public IntegerSpinBox( int initialValue )
  {
    this(initialValue, null, 1);
  }

  /**
   * Creates an unlimited range spin box.
   *
   * @param   initialValue - initial value for the spin box.
   * @param   increment - amount to increase or decrease value on each button click.
   * @author Steve Anonson
   */
  public IntegerSpinBox( int initialValue, int increment )
  {
    this(initialValue, null, increment);
  }

  /**
   * Creates an range checked spin box.
   *
   * @param   initialValue - initial value for the spin box.
   * @param   minimumValue - minimum value that the text field can contain.
   * @param   maximumValue - maximum value that the text field can contain.
   * @param   increment - amount to increase or decrease value on each button click.
   * @author Steve Anonson
   */
  public IntegerSpinBox( int initialValue, int increment, int minimumValue, int maximumValue )
  {
    this(initialValue, new IntegerRange(minimumValue, maximumValue), increment);
  }

  /**
   * Constructor.
   * Creates an range checked spin box.
   *
   * @param   initialValue - initial value for the spin box.
   * @param   range - IntegerRange of valid values.
   * @param   increment - amount to increase or decrease value on each button click.
   * @author Steve Anonson
   */
  public IntegerSpinBox( int initialValue, IntegerRange range, int increment )
  {
    Assert.expect(range != null);
    Assert.expect((initialValue >= range.getMinimum() && initialValue <= range.getMaximum()), "Initial value out of range.");

    // Make the text field large enough to hold the minumum or maximum value.
    _columns = java.lang.Math.max( range.getMinimumValue().toString().length(),
                                   range.getMaximumValue().toString().length());
    _columns += 1;  // add a little white space
    //_columns = java.lang.Math.max(_columns, 5);  // minimum of 5 columns

    _range = range;
    _increment = increment;
    _current = initialValue;

    jbInit();

    setValue(_current);  // set the initial value
  }

  /**
   * Create the components.
   * @author Steve Anonson
   */
  void jbInit()
  {
    NumericRangePlainDocument doc = new NumericRangePlainDocument();
    doc.setRange(_range);
    doc.addDocumentListener(this);
    _numberTF.setDocument(doc);
    _format.setGroupingUsed(false);
    _format.setParseIntegerOnly(true);
    _numberTF.setFormat(_format);
    _numberTF.setMargin(new Insets(0, 5, 0, 5));
    _numberTF.setHorizontalAlignment(JTextField.RIGHT);
    _numberTF.setColumns(_columns);
    _spinner.getIncrementButton().addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent e)
      {
        incrementBtn_actionPerformed(e);
      }
    });
    _spinner.getDecrementButton().addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent e)
      {
        decrementBtn_actionPerformed(e);
      }
    });

    this.setLayout(_borderLayout1);
    this.add(_numberTF, BorderLayout.CENTER);
    this.add(_spinner, BorderLayout.EAST);
  }

  /**
   * Return the current value.
   * @author Steve Anonson
   */
  public int getValue() { return _current; }

  /**
   * Set the current value.
   *
   * @param   value - value to set the text field to.
   * @throws  IndexOutOfRangeException if range checking and out of range.
   * @author Steve Anonson
   */
  public void setValue( int value )
  {
    if (_range != null)
    {
      if (!_range.inRange(new Integer(value)))
        throw new IndexOutOfBoundsException("Value out of range");
    }
    _current = value;
    this._numberTF.setText( Integer.toString(_current) );
  }

  /**
   * Set whether the user can directly edit the text field contents.  Setting
   * editable to false means that only the spin controls can change the value.
   * @author Steve Anonson
   */
  public void setEditable( boolean b )
  {
    this._numberTF.setEditable(b);
  }

  /**
   * Set whether the spin box is enabled to accept input.
   * @author Steve Anonson
   */
  public void setEnabled( boolean b )
  {
    this._numberTF.setEnabled(b);
    this._spinner.setEnabled(b);
  }

  /**
   * Increment the value
   * @author Steve Anonson
   */
  void incrementBtn_actionPerformed(java.awt.event.ActionEvent e)
  {
    try
    {
      setValue(_current + _increment);
    }
    catch (IndexOutOfBoundsException ie)
    {
      // do nothing, just don't change the display
    }
  }

  /**
   * Decrement the value
   * @author Steve Anonson
   */
  void decrementBtn_actionPerformed(java.awt.event.ActionEvent e)
  {
    try
    {
      setValue(_current - _increment);
    }
    catch (IndexOutOfBoundsException ie)
    {
      // do nothing, just don't change the display
    }
  }

  /**
   * DocumentListener method.
   * Validate the value being entered into the spin box directly.
   * @author Steve Anonson
   */
  public void insertUpdate( DocumentEvent e )
  {
    try
    {
      // Update the value cause the user is entering directly.
      this._current = this._numberTF.getLongValue().intValue();
    }
    catch (java.text.ParseException pe)
    {
      // The user tried to insert an invalid number.  Reset to minimum or zero
      if (this._range != null)
        this._current = this._range.getMinimum();
      else
        this._current = 0;
    }
  }

  /**
   * DocumentListener method.
   * Validate the value being removed from the spin box directly.
   * @author Steve Anonson
   */
  public void removeUpdate( DocumentEvent e )
  {
    try
    {
      // Update the value cause the user is removing digits.
      this._current = this._numberTF.getLongValue().intValue();
    }
    catch (java.text.ParseException pe)
    {
      // This happens when there is an empty text field.  Reset to minimum or zero.
      if (this._range != null)
        this._current = this._range.getMinimum();
      else
        this._current = 0;
    }
  }

  /**
   * DocumentListener method.
   * This method is not used in this class.
   * @author Steve Anonson
   */
  public void changedUpdate( DocumentEvent e )
  {
  }

}
