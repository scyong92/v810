package com.axi.guiUtil;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import java.awt.color.*;

/** 
 * @author Bill Darbie
 */
public class BufferedImageUtil
{
  private static BufferedImageUtil _imageNormalization;

  public static BufferedImageUtil getInstance()
  {
    if (_imageNormalization == null)
    {
      _imageNormalization = new BufferedImageUtil();
    }

    return _imageNormalization;
  }
  
  /**
   * Take a BufferedImage and create a .jpeg file of the image
   * 
   * @param bufferedImage
   * @param fileName
   * @throws FileNotFoundException
   * @throws IOException
   */
  public static void writeImageToJpegFile(BufferedImage bufferedImage, String fileName) throws FileNotFoundException, IOException
  {
    Assert.expect(bufferedImage != null);
    Assert.expect(fileName != null);

    ImageIoUtil.saveJpegImage(bufferedImage, fileName);
  }

  /**
   * GrayScale images histogram equalization
   *
   * @param input
   * @return BufferedImage
   *
   * @author Chong, Wei Chin
   */
  public static BufferedImage equalizeGrayScale(BufferedImage input)
  {
      return shiftNormalizeGrayScale(input, 0);
  }

  /**
   * Shifting GrayScale images histogram equalization
   * This code was leveraged from the ImageJ software (free, no licensing required).
   *
   * @param input
   * @param shiftValue
   * @return BufferedImage
   *
   * @author Chong, Wei Chin
   */
  public static BufferedImage shiftNormalizeGrayScale(BufferedImage input, int shiftValue)
  {
    // ignore 0-5 & 250-255 gray scale

    BufferedImage output = new BufferedImage(input.getWidth(), input.getHeight(), input.getType());
    int[] histogram = new int[256];

    // generate Histogram
    for (int x = 0; x < input.getWidth(); x++)
    {
      for (int y = 0; y < input.getHeight(); y++)
      {
        double[] pixel = new double[1];
        pixel = input.getRaster().getPixel(x, y, pixel);
        histogram[(int) pixel[0]]++;
      }
    }

    // get Cumulative Histogram & Normalize it
//    int sum = 0;
//    for(int x = 0; x < frequency.length; x++)
//		{
//			sum += frequency[x];
//			frequency[x] = sum * maxIntensity / (input.getWidth() * input.getHeight());
//		}

    // get the sum of the histogram bins
    double sum = Math.sqrt((double)histogram[0]);
    for (int i = 1; i < 255; i ++)
    {
      sum += 2 * Math.sqrt((double)histogram[i]);
    }
    sum += Math.sqrt((double)histogram[255]);

    // figure the scale and create the contrast map lookup table
    double scale = 255.0 / sum;

    int[] lut = new int[256];
    lut[0] = 0;
    sum = Math.sqrt((double)histogram[0]);
    for (int i = 1; i < 255; i ++)
    {
      double delta = Math.sqrt((double)histogram[i]);
      sum += delta;
      lut[i] = (int)Math.round(sum * scale);
      sum += delta;
    }
    lut[255] = 0;

//    ProfileUtil.generateLagrangeProfile(frequency, desiredMaxValue);
//    ProfileUtil.normalizeProfile(desiredMaxValue, frequency);
    if(shiftValue != 0)
      ProfileUtil.shiftProfile(shiftValue, lut);
//    ProfileUtil.generateBezierProfile(frequency, Math.round(desiredMaxValue));

    // store back the equalize grayScale into image
    for (int x = 0; x < input.getWidth(); x++)
    {
      for (int y = 0; y < input.getHeight(); y++)
      {
        double[] pixel = new double[1];
        pixel = input.getRaster().getPixel(x, y, pixel);
        pixel[0] = lut[(int) pixel[0]];
        output.getRaster().setPixel(x, y, pixel);
      }
    }
    return output;
  }

  /**
   * @param input
   * @param output
   * @param brightness
   * @param contrast
   *
   * @author Chong, Wei Chin
   */
  public static void brightnessContrastAdjustment(BufferedImage input, BufferedImage output, float brightness, float contrast)
  {
    if (brightness > 0 && brightness < 255
      && contrast < 2 && contrast > 0)
    {
      RescaleOp rescale = new RescaleOp(contrast, brightness, null);
      rescale.filter(input, output);
    }
  }

  /**
   * @author sham
   * @param bufferedImageList
   * @return
   */
  public java.util.List<BufferedImage> normalizeBufferedImages(java.util.List<BufferedImage> bufferedImageList)
  {
    int[] minMax = new int[2];
    minMax = getHistrogramMinMax(bufferedImageList);
    int rgbsMin = minMax[0];
    int rgbsMax = minMax[1];
    BufferedImage normalizedBufferedImage = null;
    for (int i = 0; i < bufferedImageList.size(); i++)
    {
      normalizedBufferedImage = normalizeBufferedImage(bufferedImageList.get(i), rgbsMin, rgbsMax);
      bufferedImageList.set(i, normalizedBufferedImage);
    }

    return bufferedImageList;
  }

 /**
  * @author sham
  */
  public int[] getHistrogramMinMax(java.util.List<BufferedImage> bufferedImageList)
  {
    Assert.expect(bufferedImageList != null);

    int[] minMax = new int[2];
    int arraySize = 0;
    int[] rgbsValuesArray = null;
    int[] rgbs = null;
    for (int i = 0; i < bufferedImageList.size(); i++)
    {
      arraySize += bufferedImageList.get(i).getWidth() * bufferedImageList.get(i).getHeight();
    }
    rgbsValuesArray = new int[arraySize];
    arraySize = 0;
    for (int i = 0; i < bufferedImageList.size(); i++)//bufferedImageArray.length
    {
      rgbs = new int[(bufferedImageList.get(i).getWidth() * bufferedImageList.get(i).getHeight())];
      bufferedImageList.get(i).getRaster().getPixels(0, 0, bufferedImageList.get(i).getWidth(), bufferedImageList.get(i).getHeight(), rgbs);
      System.arraycopy(rgbs, 0, rgbsValuesArray, arraySize, rgbs.length);
      arraySize += rgbs.length;
    }
    Arrays.sort(rgbsValuesArray);
    minMax[0] = rgbsValuesArray[0];
    minMax[1] = rgbsValuesArray[rgbsValuesArray.length - 1];
    rgbsValuesArray = null;

    return minMax;
  }

  /**
   * @author sham
   */
  public BufferedImage normalizeBufferedImage(BufferedImage bufferedImage, int rgbsMin, int rgbsMax)
  {
    Assert.expect(bufferedImage != null);
    Assert.expect(rgbsMin >= 0);
    Assert.expect(rgbsMax >= 0);

    int arraySize = bufferedImage.getWidth() * bufferedImage.getHeight();
    int[] rgbs = new int[arraySize];
    bufferedImage.getRaster().getPixels(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), rgbs);

    for (int i = 0; i < arraySize; i++)
    {
      double xMinusMin = (double) (rgbs[i] - rgbsMin);
      double devide = xMinusMin / (rgbsMax - rgbsMin);
      double a = (double) Math.sqrt(devide) * 255;
      rgbs[i] = (int) Math.round(a);
    }
    bufferedImage.getRaster().setPixels(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), rgbs);

    return bufferedImage;
  }

  /*
   * author sham
   */
//  public BufferedImage combineBufferedImagesToSingleBufferedImage(java.util.List<BufferedImage> bufferedImageList,Map<BufferedImage,Pair<Double,Double>> imagePositionMap)
//  {
//    BufferedImage bufferedImage = null;
//    int width = 0;
//    int deltaX=imagePositionMap.get(bufferedImageList.get(0)).getFirst().intValue();
//    int x = imagePositionMap.get(bufferedImageList.get(0)).getFirst().intValue();
//    int height = 0;
//    int deltaY=imagePositionMap.get(bufferedImageList.get(0)).getSecond().intValue();
//    int y = imagePositionMap.get(bufferedImageList.get(0)).getSecond().intValue();
//    for (int i = 0; i < bufferedImageList.size(); i++)
//    {
//      //accumulate width by fix position y
//      if(imagePositionMap.get(bufferedImageList.get(i)).getSecond().intValue() == y)
//      {
//        width += bufferedImageList.get(i).getWidth() - 1;
//      }
//      //accumulate height by fix position x
//      if(imagePositionMap.get(bufferedImageList.get(i)).getFirst().intValue() == x)
//      {
//        height += bufferedImageList.get(i).getHeight() - 1;
//      }
//      if(imagePositionMap.get(bufferedImageList.get(i)).getFirst().intValue()<deltaX)
//      {
//        deltaX = imagePositionMap.get(bufferedImageList.get(i)).getFirst().intValue();
//      }
//      if(imagePositionMap.get(bufferedImageList.get(i)).getSecond().intValue()< deltaY)
//      {
//        deltaY = imagePositionMap.get(bufferedImageList.get(i)).getSecond().intValue();
//      }
//    }
//    bufferedImage = new BufferedImage(width,height,BufferedImage.TYPE_BYTE_GRAY);
//    // paint both images, preserving the alpha channels
//    Graphics g = bufferedImage.getGraphics();
//    for (int i = 0; i < bufferedImageList.size(); i++)
//    {
//      g.drawImage(bufferedImageList.get(i),(imagePositionMap.get(bufferedImageList.get(i)).getFirst().intValue() - deltaX - 1),(imagePositionMap.get(bufferedImageList.get(i)).getSecond().intValue()+ bufferedImageList.get(i).getHeight() - height - deltaY)*(-1)+1,null);
//    }
//    g.dispose();
//    return bufferedImage;
//  }
  
  /*
   * @author sham
   * @author Siew Yeng - added parameters width,height,deltaX,deltaY
   */
  public BufferedImage combineBufferedImagesToSingleBufferedImage(Map<BufferedImage,Pair<Double,Double>> imagePositionMap, int width, int height, int deltaX, int deltaY)
  {
    BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
    // paint both images, preserving the alpha channels
    Graphics g = bufferedImage.getGraphics();
    
    for(Map.Entry<BufferedImage,Pair<Double,Double>> mapEntry : imagePositionMap.entrySet())
    {
      Pair<Double,Double> XYPosition = mapEntry.getValue();
      int x = XYPosition.getFirst().intValue() - deltaX - 1;
      int y = (XYPosition.getSecond().intValue() + mapEntry.getKey().getHeight() - height - deltaY) * (-1) + 1;
      g.drawImage(mapEntry.getKey(), x, y, null);
    }
    g.dispose();
    
    return bufferedImage;
  }
  
  /*
   * author sham
   */
  public BufferedImage combine32BitBufferedImagesToSingleBufferedImage(java.util.List<BufferedImage> bufferedImageList,Map<BufferedImage,Pair<Double,Double>> imagePositionMap)
  {
    BufferedImage bufferedImage = null;
    int width = 0;
    int deltaX=imagePositionMap.get(bufferedImageList.get(0)).getFirst().intValue();
    int x = imagePositionMap.get(bufferedImageList.get(0)).getFirst().intValue();
    int height = 0;
    int deltaY=imagePositionMap.get(bufferedImageList.get(0)).getSecond().intValue();
    int y = imagePositionMap.get(bufferedImageList.get(0)).getSecond().intValue();
    for (int i = 0; i < bufferedImageList.size(); i++)
    {
      //accumulate width by fix position y
      if(imagePositionMap.get(bufferedImageList.get(i)).getSecond().intValue() == y)
      {
        width += bufferedImageList.get(i).getWidth();
      }
      //accumulate height by fix position x
      if(imagePositionMap.get(bufferedImageList.get(i)).getFirst().intValue() == x)
      {
        height += bufferedImageList.get(i).getHeight();
      }
      if(imagePositionMap.get(bufferedImageList.get(i)).getFirst().intValue()<deltaX)
      {
        deltaX = imagePositionMap.get(bufferedImageList.get(i)).getFirst().intValue();
      }
      if(imagePositionMap.get(bufferedImageList.get(i)).getSecond().intValue()< deltaY)
      {
        deltaY = imagePositionMap.get(bufferedImageList.get(i)).getSecond().intValue();
      }
    }

    int[] nBits = new int[1];
    SampleModel sm = null;
    ColorModel cm = null;
    DataBuffer db = null;
    WritableRaster wr = null;
    nBits[0] = 32;
    sm = new ComponentSampleModel(DataBuffer.TYPE_FLOAT, width, height, 1, width, new int[]{0});
    db = new DataBufferFloat((width) * (height));
    wr = Raster.createWritableRaster(sm, db, null);
    ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_GRAY);
    cm = new ComponentColorModel(cs, nBits, false, true, Transparency.OPAQUE, DataBuffer.TYPE_FLOAT);
    WritableRaster r = null;
    for (int i = 0; i < bufferedImageList.size(); i++)
    {
      x = (imagePositionMap.get(bufferedImageList.get(i)).getFirst().intValue() - deltaX);
      y = (imagePositionMap.get(bufferedImageList.get(i)).getSecond().intValue()+ bufferedImageList.get(i).getHeight() - height - deltaY)*(-1);
      r = bufferedImageList.get(i).getRaster();
      
      wr.setDataElements(x, y, r);
    }
    bufferedImage = new BufferedImage(cm, wr, false, null);
    
    return bufferedImage;
  }

  /**
   * Take a BufferedImage and create a .jpeg file of the image
   *
   * @param bufferedImage
   * @param fileName
   * @throws FileNotFoundException
   * @throws IOException
   */
  public static void writeImageToPngFile(BufferedImage bufferedImage, String fileName) throws FileNotFoundException, IOException
  {
    Assert.expect(bufferedImage != null);
    Assert.expect(fileName != null);

    fileName=fileName.replaceFirst("jpg","png");
    //ImageIO.write(bufferedImage, "png", new File(fileName));
    ImageIoUtil.savePngImage(bufferedImage, fileName);
  }

  /**
   * @param bgImage
   * @param fgImage
   * @return 
   * 
   * @author Wei Chin
   */
  public static BufferedImage overlayImages(BufferedImage bgImage,
    BufferedImage fgImage, int bufferedImageType)
  {
    return overlayImages(bgImage, fgImage, 0, 0, null, bufferedImageType);
  }

  /**
   * @param bgImage
   * @param fgImage
   * @return 
   * 
   * @author Wei Chin
   */
  public static BufferedImage overlayImages(BufferedImage bgImage, BufferedImage fgImage, Color color, int bufferedImageType)
  {
    return overlayImages(bgImage, fgImage, 0, 0, color, bufferedImageType);
  }

  /**
   * Method to overlay Images
   *
   * @param bgImage --> The background Image
   * @param fgImage --> The foreground Image
   * @return --> overlayed image (fgImage over bgImage)
   * 
   * @author Wei Chin 
   * (copy from@Author Kushal Paudyal
   * www.sanjaal.com/java
   * Last Modified On: 2009-11-20)
   */
  public static BufferedImage overlayImages(BufferedImage bgImage,
    BufferedImage fgImage, int x, int y, Color foregroundColor, int bufferedImageType)
  {
    /**
     * Doing some preliminary validations. Foreground image height cannot be
     * greater than background image height. Foreground image width cannot be
     * greater than background image width.
     *
     * returning a null value if such condition exists.
     */
    if (fgImage.getHeight() > bgImage.getHeight()
      || fgImage.getWidth() > fgImage.getWidth())
    {
      return null;
    }

    /**
     * Create a Graphics from the background image*
     */
    BufferedImage newImage = new BufferedImage(bgImage.getWidth(), bgImage.getHeight(), bufferedImageType);
    Graphics2D g = newImage.createGraphics();
//    Graphics2D g = bgImage.createGraphics();
    /**
     * Set Antialias Rendering*
     */
    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
      RenderingHints.VALUE_ANTIALIAS_ON);
    /**
     * Draw background image at location (0,0) You can change the (x,y) value as
     * required
     */
    g.drawImage(bgImage, 0, 0, null);

    // set the foreground color if needed.
    if(foregroundColor != null)
      g.setColor(foregroundColor);
    /**
     * Draw foreground image at location (0,0) Change (x,y) value as required.
     */
    g.drawImage(fgImage, x, y, null);

    g.dispose();
    return newImage;
  }

  /**
   * Method to overlay Images ( for Black and White only )
   *
   * @param bgImage --> The background Image
   * @param fgImage --> The foreground Image
   * @return --> overlayed image (fgImage over bgImage)
   * 
   * @author Wei Chin 
   */
  public static com.axi.util.image.Image overlayImages(com.axi.util.image.Image bgImage, 
    com.axi.util.image.Image fgImage, 
    RegionOfInterest fgRoi)
  {
    BufferedImage backgroundImage = bgImage.getBufferedImage();
    BufferedImage foregroundImage = fgImage.getBufferedImage();
    BufferedImage overlayImage = overlayImages(backgroundImage, foregroundImage, fgRoi.getMinX(), fgRoi.getMinY(), null, BufferedImage.TYPE_BYTE_GRAY );
    
    return com.axi.util.image.Image.createFloatImageFromBufferedImage(overlayImage);
  }

  /**
   * Method to overlay an image into a blank background ( for Black and White only ) 
   * 
   * @param fgImage  
   * @param fgRoi --> location of the foreground in the background
   * @param bgWidth --> The background width
   * @param bgHeight --> The background width
   * @return 
   * 
   * @author Wei Chin
   */
  public static com.axi.util.image.Image overlayImagesWithWhiteBackground(com.axi.util.image.Image fgImage, 
                                                                          RegionOfInterest fgRoi, 
                                                                          int bgWidth, 
                                                                          int bgHeight)
  {
    Assert.expect(fgImage != null);
    Assert.expect(fgRoi != null);
    
    BufferedImage backgroundImage = new BufferedImage(bgWidth, bgHeight, BufferedImage.TYPE_BYTE_GRAY);
    Graphics2D g = backgroundImage.createGraphics();
    g.setBackground(Color.WHITE);
    BufferedImage foregroundImage = fgImage.getBufferedImage();
    BufferedImage overlayImage = overlayImages(backgroundImage, foregroundImage, fgRoi.getMinX(), fgRoi.getMinY(), Color.BLACK, BufferedImage.TYPE_BYTE_GRAY );
    
    return com.axi.util.image.Image.createFloatImageFromBufferedImage(overlayImage);
  }
}
