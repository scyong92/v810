package com.axi.guiUtil;

import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.*;
import javax.swing.event.*;

import com.axi.util.*;

/**
 *
 * <p>Title: TextCellEditor</p>
 * <p>Description: Provides a table cell editor that selects the contents of the
 * cell when focused on.  User can specify text alignment and foreground and background
 * colors.</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company:  Agilent Technogies</p>
 * @author Carli Connally
 * @version 1.0
 */
public class TextCellEditor extends DefaultCellEditor
{
  private JTextField _textField = null;

  /**
   * Returns a text field editor with the specified attributes.
   * @param backgroundColor Background color of the editor
   * @param foregroundColor Foreground color of the editor
   * @param horizontalAlign Horizontal text alignment.  (use constants from JTextField such as
   * JTextField.LEFT)
   * @author Carli Connally
   */
  public TextCellEditor(Color backgroundColor, Color foregroundColor, int horizontalAlign)
  {
    super(new JTextField());
    Assert.expect(backgroundColor != null);
    Assert.expect(foregroundColor != null);

    _textField = (JTextField)getComponent();

    _textField.setHorizontalAlignment(horizontalAlign);
    _textField.setBackground(backgroundColor);
    _textField.setForeground(foregroundColor);

    //text is highlighted when the editor is invoked
    _textField.addFocusListener(new FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        _textField.selectAll();
      }
      public void focusLost(FocusEvent e)
      {
        _textField.select(0,0);
      }
    });

    //the editor is invoked with a single click in the table cell
    setClickCountToStart(1);
  }
}
