package com.axi.guiUtil;

import java.awt.*;
import javax.swing.*;

/**
 * checkbox renderer that enable list item to attached with a check box beside
 * @author sheng-chuan.yong
 */
public class ListCheckBoxRenderer implements ListCellRenderer
{
  /**
   * @author Yong Sheng Chuan
   */
  private JCheckBox _checkBox = new JCheckBox();

  /**
   * @author Yong Sheng Chuan
   */
  public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
  {
    _checkBox.setFocusPainted(true);
    _checkBox.setBorderPainted(true);
    _checkBox.setSelected(isSelected);
    _checkBox.setText(value.toString());
    return _checkBox;
  }
  
}
