package com.axi.guiUtil;

import java.io.*;
import java.awt.*;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import com.axi.util.*;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.JComponent;
import com.axi.util.image.*;
import javax.swing.JOptionPane;
import java.awt.image.BufferedImage;
import java.awt.geom.Rectangle2D;
import java.io.PrintWriter;
import java.io.BufferedReader;

/**
 * This is not an interactive test that can be run as part of the overnight build.
 * It is a manual test for the Graphics Engine class.
 * It should be run manually by the developer any time that the Graphics Engine
 * class or some of its associated helper classes are modified.
 *
 *
 * @author Kay Lannen
 */
public class TestMan_GraphicsEngine extends JFrame
{
  private GraphicsEngine _graphicsEngine = null;
  private Dimension _graphicsEnginePanelDimensions = new Dimension(400, 300);
  private java.util.List<Integer> _layers = new ArrayList<Integer>();
  private Renderer _rendererRect1 = null;
  private Renderer _rendererRect2 = null;
  private UnitTest _unitTest = null;

  // This class extends JFrame instead of UnitTest because I want to be able to use
  // Borland's GUI design tools to quickly add menu items and test cases to the
  // manual interface.  A unitTest object is created to provide access to the
  // test infrastructure (including directory paths and such).
  //private GraphicsEngineUnitTest _unitTest = new GraphicsEngineUnitTest();
  private static String _TEST_IMAGE_1 = "test.jpg";

  private JPanel contentPane;
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel jPanelGraphicsEngine = new JPanel();
  private JMenuBar jMenuBar1 = new JMenuBar();
  private JMenu jMenuTest = new JMenu();
  private JMenuItem jMenuTestExit = new JMenuItem();
  private JMenu jMenuSetup = new JMenu();
  private JMenu jMenuConstructor = new JMenu();
  private JMenuItem jMenuPixelBorder0 = new JMenuItem();
  private JMenuItem jMenuPixelBorder50 = new JMenuItem();
  private JMenuItem jMenuAddRenderers = new JMenuItem();
  private JMenu jMenuSetForeground = new JMenu();
  private JMenu jMenuFitGraphicsToScreen = new JMenu();
  private JMenuItem jMenuSetLayer0ToWhite = new JMenuItem();
  private JMenuItem jMenuSetLayer0ToRed = new JMenuItem();
  private JMenuItem jMenuFitAllLayers = new JMenuItem();
  private JMenu jMenuSetAxisInterpretation = new JMenu();
  private JMenuItem jMenuXRightYDown = new JMenuItem();
  private JMenu jMenuSetVisibleLayers = new JMenu();
  private JMenuItem jMenuSetAllLayersVisible = new JMenuItem();
  private JMenu jMenuCenter = new JMenu();
  private JMenuItem jMenuXRightYUp = new JMenuItem();
  private JMenu jMenuView = new JMenu();
  private JMenuItem jMenuViewFromBottom = new JMenuItem();
  private JMenuItem jMenuViewFromTop = new JMenuItem();
  private JMenuItem jMenuFitToScreen = new JMenuItem();
  private JMenuItem jMenuCenterRectangle1 = new JMenuItem();
  private JMenuItem jMenuCenterRectangle2 = new JMenuItem();
  private JMenu jMenuZoom = new JMenu();
  private JMenuItem jMenuZoomIn = new JMenuItem();
  private JMenuItem jMenuZoomOut = new JMenuItem();
  private JMenu jMenuDrag = new JMenu();
  private JMenuItem jMenuEnableDrag = new JMenuItem();
  private JMenuItem jMenuDisableDrag = new JMenuItem();
  private JMenuItem jMenuMark = new JMenuItem();
  private JMenuItem jMenuResetToMark = new JMenuItem();
  private JMenu jMenuAddRenderer = new JMenu();
  private JMenuItem jMenuAddRectangleToLayer0UpperLeft = new JMenuItem();
  private JMenuItem jMenuAddRectangleToLayer0LowerRight = new JMenuItem();
  private JMenuItem jMenuAddImageToLayer1 = new JMenuItem();
  private JMenuItem jMenuAddRectangleToLayer1 = new JMenuItem();

  public TestMan_GraphicsEngine()
  {
    super();
    try
    {
      setDefaultCloseOperation(EXIT_ON_CLOSE);
      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
    initializeFrame(this);
  }

  public TestMan_GraphicsEngine(UnitTest unitTest)
  {
    this();
    _unitTest = unitTest;
  }

  /**
   * Application entry point
   *
   * This test does nothing unless a "manual" argument is provided on the
   * command line (or as part of the JBuilder run environment).  This prevents
   * the application from being inadvertently started as part of an automatic
   * build script test run.
   *
   * @param args String[] arguments (the first argument must be "manual" to run anything)
   */

  public static void main(String[] args)
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception exception)
        {
          exception.printStackTrace();
        }

        new TestMan_GraphicsEngine();
      }
    }
    ); // end of SwingUtilities.invokeLater
  }

  /**
   * Initializes the frame for the interactive test application.
   * @param frame The frame
   * @author Kay Lannen
   */
  private void initializeFrame(JFrame frame)
  {
    frame.pack();

    // Center the window
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = frame.getSize();
    if (frameSize.height > screenSize.height)
    {
      frameSize.height = screenSize.height;
    }
    if (frameSize.width > screenSize.width)
    {
      frameSize.width = screenSize.width;
    }
    frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
    frame.setVisible(true);

  }

  /**
   * Component initialization.
   *
   * @throws java.lang.Exception
   */
  private void jbInit() throws Exception
  {
    contentPane = (JPanel)getContentPane();
    contentPane.setLayout(borderLayout1);
    setSize(new Dimension(400, 300));
    setTitle("Graphics Engine Test Application");
    jMenuTest.setText("Test");
    jMenuTestExit.setText("Exit");
    jMenuTestExit.addActionListener(new TestMan_GraphicsEngine_jMenuFileExit_ActionAdapter(this));
    jMenuSetup.setText("Setup");
    jMenuConstructor.setText("Constructor");
    jMenuPixelBorder0.setText("pixel border=0");
    jMenuPixelBorder0.addActionListener(new TestMan_GraphicsEngine_jMenuPixelBorder0_actionAdapter(this));
    jMenuPixelBorder50.setText("pixel border=50");
    jMenuPixelBorder50.addActionListener(new TestMan_GraphicsEngine_jMenuPixelBorder50_actionAdapter(this));
    jMenuAddRenderers.setText("AddRenderers");
    jMenuAddRenderers.addActionListener(new TestMan_GraphicsEngine_jMenuAddRenderers_actionAdapter(this));
    jPanelGraphicsEngine.setBackground(Color.black);
    jPanelGraphicsEngine.setLayout(null);
    jPanelGraphicsEngine.setPreferredSize(_graphicsEnginePanelDimensions);
    jMenuSetForeground.setText("SetForeground");
    jMenuSetLayer0ToWhite.setText("Set Layer 0 to White");
    jMenuSetLayer0ToWhite.addActionListener(new TestMan_GraphicsEngine_jMenuSetLayer0ToWhite_actionAdapter(this));
    jMenuSetLayer0ToRed.setText("Set Layer 0 to Red");
    jMenuSetLayer0ToRed.addActionListener(new TestMan_GraphicsEngine_jMenuSetLayer0ToRed_actionAdapter(this));
    jMenuFitGraphicsToScreen.setText("FitGraphicsToScreen");
    jMenuFitAllLayers.setText("Fit All Layers");
    jMenuFitAllLayers.addActionListener(new TestMan_GraphicsEngine_jMenuFitAllLayers_actionAdapter(this));
    jMenuSetAxisInterpretation.setText("SetAxisInterpretation");
    jMenuXRightYDown.setText("Positive X to right, Positive Y down");
    jMenuXRightYDown.addActionListener(new
                                       TestMan_GraphicsEngine_jMenuPositiveXToRightPositiveYBelow_actionAdapter(this));
    jMenuSetVisibleLayers.setText("SetVisibleLayers");
    jMenuSetAllLayersVisible.setText("All layers visible");
    jMenuSetAllLayersVisible.addActionListener(new TestMan_GraphicsEngine_jMenuSetAllLayersVisible_actionAdapter(this));
    jMenuCenter.setText("Center");
    jMenuXRightYUp.setText("Positive X to right, Positive Y up");
    jMenuXRightYUp.addActionListener(new TestMan_GraphicsEngine_jMenuXRightYUp_actionAdapter(this));
    jMenuView.setText("View");
    jMenuViewFromBottom.setText("View From Bottom");
    jMenuViewFromBottom.addActionListener(new TestMan_GraphicsEngine_jMenuViewFromBottom_actionAdapter(this));
    jMenuViewFromTop.setText("View From Top");
    jMenuViewFromTop.addActionListener(new TestMan_GraphicsEngine_jMenuViewFromTop_actionAdapter(this));
    jMenuFitToScreen.setText("Fit to Screen");
    jMenuFitToScreen.addActionListener(new TestMan_GraphicsEngine_jMenuFitToScreen_actionAdapter(this));
    jMenuCenterRectangle1.setText("Rectangle1");
    jMenuCenterRectangle1.addActionListener(new TestMan_GraphicsEngine_jMenuCenterRectangle1_actionAdapter(this));
    jMenuCenterRectangle2.setText("Rectangle2");
    jMenuCenterRectangle2.addActionListener(new TestMan_GraphicsEngine_jMenuCenterRectangle2_actionAdapter(this));
    jMenuZoom.setText("Zoom");
    jMenuZoomIn.setText("Zoom In");
    jMenuZoomIn.addActionListener(new TestMan_GraphicsEngine_jMenuZoomIn_actionAdapter(this));
    jMenuZoomOut.setText("Zoom Out");
    jMenuZoomOut.addActionListener(new TestMan_GraphicsEngine_jMenuZoomOut_actionAdapter(this));
    jMenuDrag.setText("Drag");
    jMenuEnableDrag.setText("Enable Drag Mode");
    jMenuEnableDrag.addActionListener(new TestMan_GraphicsEngine_jMenuEnableDrag_actionAdapter(this));
    jMenuDisableDrag.setText("Disable Drag Mode");
    jMenuDisableDrag.addActionListener(new TestMan_GraphicsEngine_jMenuDisableDrag_actionAdapter(this));
    jMenuMark.setText("Mark");
    jMenuMark.addActionListener(new TestMan_GraphicsEngine_jMenuMark_actionAdapter(this));
    jMenuResetToMark.setText("Reset To Mark");
    jMenuResetToMark.addActionListener(new TestMan_GraphicsEngine_jMenuResetToMark_actionAdapter(this));
    jMenuAddRenderer.setText("AddRenderer");
    jMenuAddRectangleToLayer1.setText("Add Rectangle to Layer 1");
    jMenuAddImageToLayer1.setText("Add Image to Layer 1");
    jMenuAddImageToLayer1.addActionListener(new TestMan_GraphicsEngine_jMenuAddImageToLayer1_actionAdapter(this));
    jMenuAddRectangleToLayer0UpperLeft.setText("Add Rectangle to Layer 0 Upper Left");
    jMenuAddRectangleToLayer0UpperLeft.addActionListener(new
        TestMan_GraphicsEngine_jMenuAddRectangleToLayer0UpperLeft_actionAdapter(this));
    jMenuAddRectangleToLayer0LowerRight.setText("Add Rectangle to Layer 0 Lower Right");
    jMenuAddRectangleToLayer0LowerRight.addActionListener(new
        TestMan_GraphicsEngine_jMenuAddRectangleToLayer0LowerRight_actionAdapter(this));
    jMenuAddRectangleToLayer1.addActionListener(new TestMan_GraphicsEngine_jMenuAddRectangleToLayer1_actionAdapter(this));
    jMenuSetVisibleLayers.add(jMenuSetAllLayersVisible);
    jMenuSetAxisInterpretation.add(jMenuXRightYDown);
    jMenuSetAxisInterpretation.add(jMenuXRightYUp);
    jMenuBar1.add(jMenuTest);
    jMenuBar1.add(jMenuSetup);
    jMenuBar1.add(jMenuView);
    jMenuSetup.add(jMenuConstructor);
    jMenuSetup.add(jMenuAddRenderers);
    jMenuSetup.add(jMenuAddRenderer);
    jMenuSetup.add(jMenuSetForeground);
    jMenuSetup.add(jMenuSetVisibleLayers);
    jMenuSetup.add(jMenuSetAxisInterpretation);
    jMenuSetup.add(jMenuFitGraphicsToScreen);
    jMenuTest.add(jMenuTestExit);
    setJMenuBar(jMenuBar1);
    jMenuConstructor.add(jMenuPixelBorder0);
    jMenuConstructor.add(jMenuPixelBorder50);
    contentPane.add(jPanelGraphicsEngine, java.awt.BorderLayout.CENTER);
    jMenuSetForeground.add(jMenuSetLayer0ToWhite);
    jMenuSetForeground.add(jMenuSetLayer0ToRed);
    jMenuFitGraphicsToScreen.add(jMenuFitAllLayers);
    jMenuView.add(jMenuViewFromBottom);
    jMenuView.add(jMenuViewFromTop);
    jMenuView.addSeparator();
    jMenuView.add(jMenuMark);
    jMenuView.add(jMenuResetToMark);
    jMenuView.add(jMenuFitToScreen);
    jMenuView.add(jMenuCenter);
    jMenuView.add(jMenuZoom);
    jMenuView.add(jMenuDrag);
    jMenuView.addSeparator();
    jMenuDrag.add(jMenuEnableDrag);
    jMenuDrag.add(jMenuDisableDrag);
    jMenuCenter.add(jMenuCenterRectangle1);
    jMenuCenter.add(jMenuCenterRectangle2);
    jMenuZoom.add(jMenuZoomIn);
    jMenuZoom.add(jMenuZoomOut);
    jMenuAddRenderer.add(jMenuAddRectangleToLayer0UpperLeft);
    jMenuAddRenderer.add(jMenuAddRectangleToLayer0LowerRight);
    jMenuAddRenderer.add(jMenuAddRectangleToLayer1);
    jMenuAddRenderer.add(jMenuAddImageToLayer1);
  }

  private void initializeGraphicsEngine(int pixelBorderWidth)
  {
    if (_graphicsEngine != null)
    {
      _graphicsEngine.reset();
      jPanelGraphicsEngine.remove(_graphicsEngine);
    }
    _graphicsEngine = new GraphicsEngine(pixelBorderWidth);
    jPanelGraphicsEngine.add(_graphicsEngine);
    _graphicsEngine.setSize(this._graphicsEnginePanelDimensions);
    this.pack();
  }

  private void reportError(String str)
  {
    JOptionPane.showMessageDialog(this, str);
  }

  /**
   * File | Exit action performed.
   *
   * @param actionEvent ActionEvent
   */
  void jMenuFileExit_actionPerformed(ActionEvent actionEvent)
  {
    System.exit(0);
  }

  public void jMenuPixelBorder0_actionPerformed(ActionEvent e)
  {
    this.initializeGraphicsEngine(0);
  }

  public void jMenuPixelBorder50_actionPerformed(ActionEvent e)
  {
    this.initializeGraphicsEngine(0);

  }

  public void jMenuAddRenderers_actionPerformed(ActionEvent e)
  {
    Assert.expect(_graphicsEngine != null);

    // Get location of test files
    String dataDir = "";
    if (_unitTest != null)
    {
      dataDir = _unitTest.getTestDataDir();
    }

    _layers.clear();
    java.util.List<Renderer> renderers = new ArrayList<Renderer>();

    int layerNum = 0;
    _layers.add(new Integer(layerNum));

    Rectangle rect1 = new Rectangle(30, 50, 200, 100);
    _rendererRect1 = TestMan_Renderer.createRectangleRenderer(rect1, true);
    renderers.add(_rendererRect1);

    _rendererRect2 = TestMan_Renderer.createRectangleRenderer(new Rectangle(0, 0, 10, 10), true);
    renderers.add(_rendererRect2);

    Renderer renderer3 = TestMan_Renderer.createElRenderer(10, 10, 150, 75, true);
    renderers.add(renderer3);


    Renderer rendererText1 = TestMan_Renderer.createSimpleTextRenderer("Hello", rect1);
    renderers.add(rendererText1);

    Renderer rendererText2 = TestMan_Renderer.createSimpleTextRenderer("Aloha!", new Rectangle(0, 0, 10, 10));
    renderers.add(rendererText2);

    try
    {
      com.axi.util.image.Image testImage = ImageIoUtil.loadImage(dataDir + File.separator + _TEST_IMAGE_1);
      BufferedImage buffImage = testImage.getBufferedImage();
      Renderer renderer4 = TestMan_Renderer.createSimpleBufferedImageRenderer(buffImage, 30, 50, 200, 100, false);
      renderers.add(renderer4);

    }
    catch (CouldNotReadFileException ex)
    {
      reportError(ex.getMessage());
    }
    catch (FileDoesNotExistException ex)
    {
      reportError(ex.getMessage());
    }


    _graphicsEngine.addRenderers(layerNum, renderers);

  }

  public void jMenuSetLayer0ToWhite_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.setForeground(0, Color.WHITE);
  }

  public void jMenuSetLayer0ToRed_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.setForeground(0, Color.RED);
  }

  public void jMenuFitAllLayers_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.setLayersToUseWhenFittingToScreen(_layers);
    _graphicsEngine.fitGraphicsToScreen();
  }

  public void jMenuPositiveXToRightPositiveYBelow_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.setAxisInterpretation(true, true);
  }

  public void jMenuSetAllLayersVisible_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.setVisible(_layers, true);
  }

  public void jMenuXRightYUp_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.setAxisInterpretation(true, false);
  }

  public void jMenuViewFromBottom_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.viewFromBottom();
  }

  public void jMenuViewFromTop_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.viewFromTop();
  }

  public void jMenuCenterRectangle1_actionPerformed(ActionEvent e)
  {
    Assert.expect(_rendererRect1 != null);
    Rectangle2D bounds = _rendererRect1.getBounds2D();
    _graphicsEngine.center(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
  }

  public void jMenuFitToScreen_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.fitGraphicsToScreen();
  }

  public void jMenuCenterRectangle2_actionPerformed(ActionEvent e)
  {
    Assert.expect(_rendererRect2 != null);
    Rectangle2D bounds = _rendererRect2.getBounds2D();
    _graphicsEngine.center(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
  }

  public void jMenuZoomIn_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.zoom(2.0);
  }

  public void jMenuZoomOut_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.zoom(0.5);
  }

  public void jMenuEnableDrag_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.setDragGraphicsMode(true);
  }

  public void jMenuDisableDrag_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.setDragGraphicsMode(false);
  }

  public void jMenuMark_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.mark();
  }

  public void jMenuResetToMark_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.resetToMark();
  }

  public void jMenuAddRectangleToLayer1_actionPerformed(ActionEvent e)
  {
    Rectangle rect = new Rectangle(20, 20, 150, 150);
    Renderer renderer = TestMan_Renderer.createRectangleRenderer(rect, false);
    _graphicsEngine.addRenderer(1, renderer);
  }

  public void jMenuAddRectangleToLayer0UpperLeft_actionPerformed(ActionEvent e)
  {
    Rectangle rect = new Rectangle(-50, -50, 40, 40);
    Renderer renderer = TestMan_Renderer.createRectangleRenderer(rect, false);
    _graphicsEngine.addRenderer(0, renderer);
  }

  public void jMenuAddRectangleToLayer0LowerRight_actionPerformed(ActionEvent e)
  {
    Rectangle rect = new Rectangle(250, 250, 10, 30);
    Renderer renderer = TestMan_Renderer.createRectangleRenderer(rect, false);
    _graphicsEngine.addRenderer(0, renderer);
  }

  public void jMenuAddImageToLayer1_actionPerformed(ActionEvent e)
  {

  }
}


class TestMan_GraphicsEngine_jMenuAddImageToLayer1_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuAddImageToLayer1_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuAddImageToLayer1_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuAddRectangleToLayer0LowerRight_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuAddRectangleToLayer0LowerRight_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuAddRectangleToLayer0LowerRight_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuAddRectangleToLayer0UpperLeft_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuAddRectangleToLayer0UpperLeft_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuAddRectangleToLayer0UpperLeft_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuAddRectangleToLayer1_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuAddRectangleToLayer1_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuAddRectangleToLayer1_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuResetToMark_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuResetToMark_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuResetToMark_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuMark_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuMark_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuMark_actionPerformed(e);
  }
}


class GraphicsEngineUnitTest extends UnitTest
{
  public void test(BufferedReader is, PrintWriter os)
  {
    // do nothing--this is required for UnitTest but it's not actually used
  }
}


class TestMan_GraphicsEngine_jMenuDisableDrag_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuDisableDrag_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuDisableDrag_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuEnableDrag_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuEnableDrag_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuEnableDrag_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuZoomOut_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuZoomOut_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuZoomOut_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuZoomIn_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuZoomIn_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuZoomIn_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuCenterRectangle2_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuCenterRectangle2_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuCenterRectangle2_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuCenterRectangle1_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuCenterRectangle1_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuCenterRectangle1_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuFitToScreen_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuFitToScreen_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuFitToScreen_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuViewFromTop_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuViewFromTop_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuViewFromTop_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuViewFromBottom_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuViewFromBottom_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuViewFromBottom_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuXRightYUp_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuXRightYUp_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuXRightYUp_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuSetAllLayersVisible_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuSetAllLayersVisible_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuSetAllLayersVisible_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuPositiveXToRightPositiveYBelow_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuPositiveXToRightPositiveYBelow_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuPositiveXToRightPositiveYBelow_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuFitAllLayers_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuFitAllLayers_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuFitAllLayers_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuSetLayer0ToRed_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuSetLayer0ToRed_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuSetLayer0ToRed_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuSetLayer0ToWhite_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuSetLayer0ToWhite_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuSetLayer0ToWhite_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuAddRenderers_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuAddRenderers_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuAddRenderers_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuPixelBorder50_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuPixelBorder50_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuPixelBorder50_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuPixelBorder0_actionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;
  TestMan_GraphicsEngine_jMenuPixelBorder0_actionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e)
  {
    adaptee.jMenuPixelBorder0_actionPerformed(e);
  }
}


class TestMan_GraphicsEngine_jMenuFileExit_ActionAdapter implements ActionListener
{
  private TestMan_GraphicsEngine adaptee;

  TestMan_GraphicsEngine_jMenuFileExit_ActionAdapter(TestMan_GraphicsEngine adaptee)
  {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent actionEvent)
  {
    adaptee.jMenuFileExit_actionPerformed(actionEvent);
  }
}
