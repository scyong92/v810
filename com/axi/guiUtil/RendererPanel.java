package com.axi.guiUtil;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;
import javax.swing.*;

import com.axi.util.*;
import com.axi.util.image.*;
import java.util.List;

/**
 * A RendererPanel is a JPanel that contains Renderer objects.  It
 * is used by the GraphicsEngine class.  One
 * RendererPanel per GraphicsEngine layer is used.  Each RendererPanel
 * has a color associated with it.  This class also makes it easy to
 * iterator over all the Renderer objects
 *
 * @author Bill Darbie
 */
public class RendererPanel extends JPanel
{
  private boolean _renderersVisible = true;

  /**
   * Create a RendererPanel with the width and height
   * specified.  All Renderers will be drawn in the color
   * specified by the color parameter.
   *
   * @author Bill Darbie
   */
  RendererPanel(Color color)
  {
    Assert.expect(color != null);
    setOpaque(false);
    setForeground(color);
    setLayout(null);

    // apm -- attempt to get a renderer to stay out of the z-order sort
    // didn't work, but I want to document what I tried
//    setFocusable(false);
//    setRequestFocusEnabled(false);
//    setFocusCycleRoot(false);
//    setFocusTraversalKeysEnabled(false);
//    setFocusTraversalPolicyProvider(false);
//    setVerifyInputWhenFocusTarget(false);

  }

  /**
   * Set the color of all Renderers in this panel.
   *
   * @author Bill Darbie
   */
  public void setForeground(Color color)
  {
    super.setForeground(color);
    Component[] components = getComponents();
    for (int i = 0; i < components.length; ++i)
    {
      Renderer renderer = (Renderer)components[i];
      renderer.setForeground(color);
    }
  }

  /**
   * Apply the passed in transform to all Renderers
   * @author Bill Darbie
   */
  void transform(AffineTransform transform)
  {
    Assert.expect(transform != null);

    Component[] components = getComponents();
    for (int i = 0; i < components.length; ++i)
    {
      Renderer renderer = (Renderer)components[i];
      renderer.transform(transform);
    }
  }

  /**
   * Apply the transform for axis interpretation to all Renderers
   * @author Kay Lannen
   * @param positiveXisToTheRightOfTheOrigin boolean
   * @param positiveYisBelowTheOrigin boolean
   * @param xTranslation double
   * @param yTranslation double
   */
  void applyAxisTransform(boolean positiveXisToTheRightOfTheOrigin, boolean positiveYisBelowTheOrigin,
                          double xTranslation, double yTranslation)
  {
    Component[] components = getComponents();
    for (int i = 0; i < components.length; ++i)
    {
      Renderer renderer = (Renderer)components[i];
      renderer.applyAxisTransform(positiveXisToTheRightOfTheOrigin, positiveYisBelowTheOrigin, xTranslation,
                                  yTranslation);
    }

  }

  /**
   * This method CAN return null if no Renderers are in this panel!!
   * @return the rectangle that encompasses all the Renderers on this panel.  If
   *         no Renderers exist, null will be returned
   * @author Bill Darbie
   */
  Rectangle2D getBounds2D()
  {
    Rectangle2D maxRectangle = null;
    Component[] components = getComponents();
    for (int i = 0; i < components.length; ++i)
    {
      Renderer renderer = (Renderer)components[i];
      Rectangle2D rectangle = renderer.getBounds2D();
      if (i == 0)
      {
        maxRectangle = rectangle;
      }
      else
      {
        maxRectangle.add(rectangle);
      }
    }

    return maxRectangle;
  }

  /**
   * Set this panels Renderers visibility without changing the panels visibility.
   * @author Bill Darbie
   */
  void setRenderersVisible(boolean visible)
  {
    _renderersVisible = visible;
    Component[] components = getComponents();
    for (int i = 0; i < components.length; ++i)
    {
      Renderer renderer = (Renderer)components[i];
      renderer.setVisible(visible);
    }
  }

  /**
   * Set this panels Renderers visibility without changing the panels visibility.
   * @author Bill Darbie
   */
  boolean areRenderersVisible()
  {
    return _renderersVisible;
  }

  /**
   * Cause all Renderers on this RendererPanel to get fresh data.  The shapes being
   * drawn will change if the underlying data has changed for a particular Renderer.
   *
   * @author Bill Darbie
   */
  void validateData()
  {
    Component[] components = getComponents();
    for (int i = 0; i < components.length; ++i)
    {
      Renderer renderer = (Renderer)components[i];
      renderer.validateData();
    }
  }

  /**
   * @author Bill Darbie
   */
  Collection<Renderer> getRenderers()
  {
    Component[] componentArray = getComponents();
    Collection<Renderer> components = new ArrayList<Renderer>();
    for (Component component : componentArray)
    {
      components.add((Renderer)component);
    }

    return components;
  }

  /**
   * @author Chong, Wei Chin
   */
  public void increaseBrightnessForRenderers()
  {
    Component[] componentArray = getComponents();
    for (Component component : componentArray)
    {
      Renderer renderer = (Renderer)component;
      renderer.brighten();
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  public void decreaseBrightnessForRenderers()
  {
    Component[] componentArray = getComponents();
    for (Component component : componentArray)
    {
      Renderer renderer = (Renderer)component;
      renderer.darken();
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  public void increaseContrastForRenderers()
  {
    Component[] componentArray = getComponents();
    for (Component component : componentArray)
    {
      Renderer renderer = (Renderer)component;
      renderer.increaseContrast();
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  public void decreaseContrastForRenderers()
  {
    Component[] componentArray = getComponents();
    for (Component component : componentArray)
    {
      Renderer renderer = (Renderer)component;
      renderer.decreaseContrast();
    }
  }

    /**
   * @author Chong, Wei Chin
   */
  public void increaseNormalizeForRenderers()
  {
    Component[] componentArray = getComponents();
    for (Component component : componentArray)
    {
      Renderer renderer = (Renderer)component;
      renderer.increaseDesiredMaxValue();
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  public void decreaseNormalizeForRenderers()
  {
    Component[] componentArray = getComponents();
    for (Component component : componentArray)
    {
      Renderer renderer = (Renderer)component;
      renderer.decreaseDesiredMaxValue();
    }
  }
  
  /**
   * @author Chong, Wei Chin
   */
  public void equalizeGrayScaleForRenderers()
  {
    Component[] componentArray = getComponents();
    for (Component component : componentArray)
    {
      Renderer renderer = (Renderer)component;
      renderer.equalizeGrayScale();
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  public void revertToOriginalBufferedImageForRenderers()
  {
    Component[] componentArray = getComponents();
    for (Component component : componentArray)
    {
      Renderer renderer = (Renderer)component;
      renderer.revertToOriginalBufferedImage();
    }
  }

  /**
   * To execute the post processing on image.
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */
  public void imagePostProcessingForRenderers(List<ImageEnhancerBase> enhancerList)
  {
    Component[] componentArray = getComponents();
    for (Component component : componentArray)
    {
      Renderer renderer = (Renderer) component;
      renderer.imagePostProcessing(enhancerList);
    }
  }
  
  /**
   * To apply the brightness and contrast setting of  previous image to current image.
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */
  public void imagePostProcessingWithSettingForRenderers(
    List<ImageEnhancerBase> enhancerList, 
    BrightnessAndContrastSetting setting)
  {
    Component[] componentArray = getComponents();
    for (Component component : componentArray)
    {
      Renderer renderer = (Renderer) component;
      renderer.setBrightnessAndContrastSetting(setting);
      renderer.imagePostProcessing(enhancerList);
    }
  }
      
  /**
   * To get brightness and contrast setting of current image.
   * All GUI component shall have same setting under a RendererPanel, therefore once we get the setting,
   * we can return to user directly.
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */
  public BrightnessAndContrastSetting getBrightnessAndContrastSettingForRenderers()
  {
    Component[] componentArray = getComponents();
    BrightnessAndContrastSetting setting = null;
    for (Component component : componentArray)
    {
      Renderer renderer = (Renderer) component;
      setting = renderer.getBrightnessAndContrastSetting();
      if(setting !=null)
        return setting;
    }
    
    return setting;
  }
  
  /**
   * @author Chong, Wei Chin
   */
  public void saveBufferedImageForRenderers(String filename)
  {
    Assert.expect(filename != null);
    
    Component[] componentArray = getComponents();
    for (Component component : componentArray)
    {
      Renderer renderer = (Renderer)component;
      renderer.saveBufferedImage(filename);
    }
  }
}