package com.axi.guiUtil;

import java.awt.Component;
import java.awt.Color;
import java.text.DecimalFormat;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;

import com.axi.util.*;

/**
 *
 * <p>Title: NumericRenderer</p>
 * <p>Description: Provides a text renderer for numeric input with configurable horizontal alignment.
 * Uses table foreground, background, and font.  Expects an Integer or Double value.
 * Leading or trailing zeros are not displayed (this could be changed to be configurable if needed).
 * to be rendered.</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company:  Agilent Technogies</p>
 * @author Carli Connally
 * @version 1.0
 */
public class NumericRenderer implements TableCellRenderer
{
  int _horizontalAlign = JLabel.LEFT;
  int _decimalPlaces = 0;

  /**
   * @param horizontalAlign Horizontal alignment value (i.e. JLabel.LEFT)
   * @author Carli Connally
   */
  public NumericRenderer(int decimalPlaces, int horizontalAlign)
  {
    Assert.expect(decimalPlaces >= 0);

    _decimalPlaces = decimalPlaces;
    _horizontalAlign = horizontalAlign;
  }

  /**
   * Default alignment is left
   * @author Carli Connally
   */
  public NumericRenderer()
  {
  }

  /**
   * Overridden method.  Returns the appropriately formatted and populated JLabel.
   * @param table Corresponding table
   * @param value Value to be displayed.  Expects an Integer or a Double.
   * @param isSelected Whether the cell is selected
   * @param hasFocus whether the cell has focus
   * @param row Corresponding row in the table
   * @param column Corresponding column in the table
   * @return JLabel with value set
   * @author Carli Connally
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                                                  boolean hasFocus, int row, int column)
  {
    if (value == null)
      return null;

    final JLabel label = new JLabel();
    Color alternativeColor = (Color) UIManager.get ("Table.alternateRowColor");
    Color backgroundColor =Color.WHITE;
    
    //build the format object according to the number of decimal points specified for display
    DecimalFormat decFormat = buildDecimalFormat();

    //format the value if it contains decimal information
    String strValue;
    if(value instanceof Double)
    {
      strValue = decFormat.format(((Double)value).doubleValue());
    }
    else
    {
      strValue = value.toString();
    }

    label.setText(strValue);
    label.setToolTipText(strValue);
    label.setBorder(new EmptyBorder(1, 1, 1, 1));
    label.setOpaque(true);
    label.setFont(table.getFont());
    label.setHorizontalAlignment(_horizontalAlign);

    if (isSelected)
    {
      label.setBackground(table.getSelectionBackground());
      label.setForeground(table.getSelectionForeground());
    }
    else
    {
      label.setForeground(table.getForeground());
      
      if(row%2 == 0)
        label.setBackground(backgroundColor);
      else
        label.setBackground(alternativeColor);
    }
    return label;
  }

  /**
   * Allows ability to change the number of decimal places to be displayed in the cell.
   * Note:  leading or trailing zeros are not displayed.
   * @param decimalPlaces Number of decimal places to be displayed.
   * @author Carli Connally
   */
  public void setDecimalPlacesToDisplay(int decimalPlaces)
  {
    Assert.expect(decimalPlaces >= 0);

    _decimalPlaces = decimalPlaces;
  }

  /**
   * @return Number of decimal places to be displayed in the cell.
   * @author Carli Connally
   */
  public int getDecimalPlacesToDisplay()
  {
    Assert.expect(_decimalPlaces >= 0);

    return _decimalPlaces;
  }

  /**
   * Builds the format string to match the number of decimal places specified in the class.
   * @return DecimalFormat that has the number of decimal places as specified in the class.
   * @author Carli Connally
   */
  private DecimalFormat buildDecimalFormat()
  {
    //create a format string for the decimal portion according
    //to the number of digits specified in the method call
    //Note:  the '#' will not show leading or trailing zeros
    String decimalPattern = new String("");
    for(int i=0; i < _decimalPlaces; i++)
    {
      decimalPattern = decimalPattern + "0";
    }

    //create the format for the decimal number, no decimal point if
    //0 decimal digits specified
    DecimalFormat decFormat;
    if(_decimalPlaces > 0)
    {
      decFormat = new DecimalFormat("########0." + decimalPattern);
    }
    else
    {
      decFormat = new DecimalFormat("#########");
      decFormat.setParseIntegerOnly(true);
    }

    return decFormat;
  }
}
