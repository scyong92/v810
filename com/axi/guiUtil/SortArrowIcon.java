package com.axi.guiUtil;

import java.awt.*;
import javax.swing.*;

/**
 * This class draws the up and down arrows
 *
 * @author Claude Duguay of JavaPro
 */
public class SortArrowIcon implements Icon
{
  public static final int NONE = 0;
  public static final int DECENDING = 1;
  public static final int ASCENDING = 2;

  private int _direction;
  private int _width = 8;
  private int _height = 8;

  public SortArrowIcon(int direction)
  {
    _direction = direction;
  }

  public int getIconWidth()
  {
    return _width;
  }

  public int getIconHeight()
  {
    return _height;
  }

  public void paintIcon(Component c, Graphics g, int x, int y)
  {
    Color bg = c.getBackground();
    Color light = bg.brighter();
    Color shade = bg.darker();

    int w = _width;
    int h = _height;
    int m = w / 2;
    if (_direction == DECENDING)
    {
      g.setColor(shade);
      g.drawLine(x, y, x + w, y);
      g.drawLine(x, y, x + m, y + h);
      g.setColor(light);
      g.drawLine(x + w, y, x + m, y + h);
    }
    if (_direction == ASCENDING)
    {
      g.setColor(shade);
      g.drawLine(x + m, y, x, y + h);
      g.setColor(light);
      g.drawLine(x, y + h, x + w, y + h);
      g.drawLine(x + m, y, x + w, y + h);
    }
  }
}

