package com.axi.guiUtil;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;

/**
 * This class tests the sortable table
 *
 * @author Claude Duguay of JavaPro
 */
public class JSortTableTest extends JPanel
{
  public JSortTableTest()
  {
    setLayout(new GridLayout());
    setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
    setPreferredSize(new Dimension(400, 400));
    add(new JScrollPane(new JSortTable(makeModel())));
  }

  protected SortTableModel makeModel()
  {
    Vector<Vector<Integer>> data = new Vector<Vector<Integer>>();
    for (int i = 0; i < 25; i++)
    {
      Vector<Integer> row = new Vector<Integer>();
      for (int j = 0; j < 5; j++)
      {
        row.add(new Integer((int)(Math.random() * 256)));
      }
      data.add(row);
    }

    Vector<String> names = new Vector<String>();
    names.add("One");
    names.add("Two");
    names.add("Three");
    names.add("Four");
    names.add("Five");

    return new DefaultSortTableModel(data, names);
  }

  public static void main(String[] args)
  {
    JFrame frame = new JFrame("JSortTable Test");
    frame.getContentPane().setLayout(new GridLayout());
    frame.getContentPane().add(new JSortTableTest());
    frame.pack();
    frame.setVisible(true);
  }
}

