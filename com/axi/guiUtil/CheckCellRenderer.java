package com.axi.guiUtil;

import java.awt.Color;
import java.awt.Component;
import javax.swing.*;
import javax.swing.table.TableCellRenderer;

/**
 *
 * <p>Title: CheckCellRenderer</p>
 * <p>Description: Table Cell renderer for a check box (boolean value).
 * The check box is centered and the cell does not show the highlights when
 * the user clicks the box.</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company:  Agilent Technogies</p>
 * @author Carli Connally
 * @version 1.0
 */
public class CheckCellRenderer implements TableCellRenderer
{

  /**
   * Overridden method.  Returns a component to render a checkbox that is centered.
   * @param table Table instance that will render the component.
   * @param value Value to be rendered (should be a Boolean value)
   * @param isSelected Whether the cell is selected
   * @param hasFocus Whether the cell has focus
   * @param row Row index for the cell
   * @param column Column index for the cell
   * @return Component (JCheckBox)
   * @author Carli Connally
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    final JCheckBox checkBox = new JCheckBox();
    if (value == null)
      return null;

    Color alternativeColor = (Color) UIManager.get ("Table.alternateRowColor");
    Color backgroundColor =Color.WHITE;
    
    checkBox.setSelected(((Boolean)value).booleanValue());
    checkBox.setOpaque(true);
    checkBox.setFont(table.getFont());
    checkBox.setHorizontalAlignment((int)JCheckBox.CENTER_ALIGNMENT);

    if (isSelected)
    {
      checkBox.setForeground(table.getSelectionForeground());
      checkBox.setBackground(table.getSelectionBackground());
    }
    else
    {
      checkBox.setForeground(table.getForeground());
      if(row%2 == 0)
        checkBox.setBackground(backgroundColor);
      else
        checkBox.setBackground(alternativeColor);
    }
    
    //Siew Yeng - XCR-3781 - this is to disable checkbox when table is disabled
    checkBox.setEnabled(table.isEnabled());
    
    return checkBox;

  }
}
