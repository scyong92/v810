package com.axi.guiUtil;

import java.awt.*;
import java.awt.geom.*;
import java.awt.image.*;

import com.axi.util.*;

/**
 * This class is responsible for drawing BufferedImages.
 *
 * When you override this class your constructor must:
 * - call initalizeImage()
 *
 * @see GraphicsEngine
 * @see Renderer
 *
 * @author Bill Darbie
 */
public class BufferedImageRenderer extends Renderer
{
  protected BufferedImage _bufferedImage;

  protected AffineTransform _transform;
  private double _origXcoord;
  private double _origYcoord;

  //Anthony01005
  private boolean _enabledBicubicInterpolation = false;
  /**
   * @author George A. David
   */
  public BufferedImageRenderer(BufferedImage bufferedImage, double xCoord, double yCoord)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(bufferedImage != null);

    _bufferedImage = bufferedImage;
    _origXcoord = xCoord;
    _origYcoord = yCoord;

    refreshData();
  }

  /**
   * @author Bill Darbie
   */
  protected BufferedImageRenderer()
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
  }

  /**
   * This version has only a translation transformation.
   * @author Patrick Lacz
   */
  protected void initializeImageWithTranslation(BufferedImage bufferedImage, double x, double y)
  {
    Assert.expect(bufferedImage != null);

    _bufferedImage = bufferedImage;

    _transform = AffineTransform.getScaleInstance(1.0, 1.0);

    _xInPixels = x;
    _yInPixels = y;
    _widthInPixels = _bufferedImage.getWidth();
    _heightInPixels = _bufferedImage.getHeight();

    setBounds();
  }

  /**
   * @author Bill Darbie
   */
  protected void initializeImage(BufferedImage bufferedImage, double x, double y, double width, double height)
  {
    Assert.expect(bufferedImage != null);

    _bufferedImage = bufferedImage;
    double imageWidth = _bufferedImage.getWidth();
    double imageHeight = _bufferedImage.getHeight();

    Assert.expect(imageWidth != 0);
    Assert.expect(imageHeight != 0);
    double scaleX = width / imageWidth;
    double scaleY = height / imageHeight;

    _transform = AffineTransform.getScaleInstance(scaleX, scaleY);

    _xInPixels = x;
    _yInPixels = y;
    _widthInPixels = width;
    _heightInPixels = height;

    setBounds();
  }

  /**
   * @author Bill Darbie
   */
  protected BufferedImage getBufferedImage()
  {
    Assert.expect(_bufferedImage != null);

    return _bufferedImage;
  }

  /**
   * @author Anthony Fong //Anthony01005
   */
  public void setBicubicInterpolation(boolean enabledBicubicInterpolation)
  {
    _enabledBicubicInterpolation = enabledBicubicInterpolation;
    repaint();
  }

  /**
   * Apply the passed in transform to this Renderer.
   * @author Bill Darbie
   */
  protected void applyTransform(AffineTransform transform)
  {
    Assert.expect(transform != null);

    // apply the transform to the bounds
    double origX = _xInPixels;
    double origY = _yInPixels;
    Rectangle2D bounds = getBounds2D();
    Shape boundsShape = transform.createTransformedShape(bounds);
    bounds = boundsShape.getBounds2D();
    _xInPixels = bounds.getX();
    _yInPixels = bounds.getY();
    _widthInPixels = bounds.getWidth();
    _heightInPixels = bounds.getHeight();

    double scaleX = transform.getScaleX();
    double scaleY = transform.getScaleY();

    // a rotation transform will give a 0 scaleX and scaleY, but we really want a 1
    if (scaleX == 0)
    {
      scaleX = 1;
    }
    if (scaleY == 0)
    {
      scaleY = 1;
    }

    double deltaX = origX * scaleX - _xInPixels;
    double deltaY = origY * scaleY - _yInPixels;

    setBounds();


    _transform.preConcatenate(transform);

    // keep the image at 0, 0 since the bounds will handle the proper x, y
    AffineTransform trans = AffineTransform.getTranslateInstance(deltaX, deltaY);
    _transform.preConcatenate(trans);
  }

  /**
   * This method applies the appropriate translation for the specified axis orientation.
   * Note that it does a second reflection because we do not want the image to be viewed
   * from the bottom by default.
   * @author Kay Lannen
   * @param positiveXisToTheRightOfTheOrigin boolean -indicates x axis interpretation
   * @param positiveYisBelowTheOrigin boolean -indicates y axis interpretation
   * @param xTranslation double -amount of translation required to bring renderers back into original quadrant after reflection across x axis
   * @param yTranslation double -amount of translation required to bring renderers back into original quadrant after reflection across y axis
   */
  public void applyAxisTransform(boolean positiveXisToTheRightOfTheOrigin, boolean positiveYisBelowTheOrigin,
                                 double xTranslation, double yTranslation)
  {
    super.applyAxisTransform(positiveXisToTheRightOfTheOrigin, positiveYisBelowTheOrigin, xTranslation, yTranslation);

    if (positiveXisToTheRightOfTheOrigin == false)
    {
      // At this point the image has been flipped left to right by super.applyAxisTransform.
      //  We need to flip it back.
      Rectangle2D bounds = getBounds2D();
      double centerX = bounds.getCenterX();
      AffineTransform transform = AffineTransform.getTranslateInstance( -centerX, 0.0);
      transform.preConcatenate(AffineTransform.getScaleInstance( -1.0, 1.0));
      transform.preConcatenate(AffineTransform.getTranslateInstance(centerX, 0.0));
      transform(transform);
    }

    if (positiveYisBelowTheOrigin == false)
    {
      // At this point the image has been flipped top to bottom by super.applyAxisTransform.
      //  We need to flip it back.
      Rectangle2D bounds = getBounds2D();
      double centerY = bounds.getCenterY();
      AffineTransform transform = AffineTransform.getTranslateInstance(0.0, -centerY);
      transform.preConcatenate(AffineTransform.getScaleInstance(1.0, -1.0));
      transform.preConcatenate(AffineTransform.getTranslateInstance(0.0, centerY));
      transform(transform);
    }
  }

  /**
   * This method does the actual drawing on the screen.
   * @author Patrick Lacz
   */
  protected void paintComponent(Graphics graphics)
  {
    Graphics2D graphics2D = (Graphics2D)graphics;

    //Anthony01005 -->
    if(_enabledBicubicInterpolation)
    {
        graphics2D.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint( RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        graphics2D.setRenderingHint( RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
    }
    //Anthony01005 <--
    // draw the image using our transform.
    // this transformation includes both translation and scale information.
    graphics2D.drawImage(_bufferedImage, _transform, null);
  }

  /**
   * This method needs to overridden to get the most recent data required
   * to draw the appropriate Shape.  Your method needs to call initializeImage()
   * with the new Image.
   * @author Bill Darbie
   */
  protected void refreshData()
  {
    initializeImage(_bufferedImage, _origXcoord, _origYcoord, _bufferedImage.getWidth(), _bufferedImage.getHeight());
  }  

  /**
   * @author Bill Darbie
   */
  public BufferedImage getBufferedImageFromRenderer()
  {
    Assert.expect(_bufferedImage != null);

    return _bufferedImage;
  }
}
