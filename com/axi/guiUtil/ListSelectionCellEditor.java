package com.axi.guiUtil;

import java.util.List;
import javax.swing.*;
import javax.swing.event.*;

import com.axi.util.*;

/**
 *
 * <p>Title: ListSelectionCellEditor</p>
 * <p>Description: Table cell editor for a combo box.  Initialize with a List or an array.</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company:  Agilent Technogies</p>
 * @author Carli Connally
 * @version 1.0
 */
public class ListSelectionCellEditor extends DefaultCellEditor
{
  private JComboBox _comboBox = null;

  /**
   * @author Carli Connally
   * Default constructor.
   */
  public ListSelectionCellEditor()
  {
    super(new JComboBox());
    _comboBox = (JComboBox)getComponent();
  }

  /**
   * Constructor with a list.  Initializes the combo box with the items
   * in the list.
   * @param list List that contains items to add to the combo box
   * @author Carli Connally
   */
  public ListSelectionCellEditor(List<Object> list)
  {
    super(new JComboBox());
    Assert.expect(list != null);

    _comboBox = (JComboBox)getComponent();
    setData(list);
  }

  /**
   * Constructor with an array.  Initializes the combo box with the items
   * in the list.
   * @param array Array that contains items to add to the combo box
   * @author Carli Connally
   */
  public ListSelectionCellEditor(Object[] array)
  {
    super(new JComboBox());
    Assert.expect(array != null);
    _comboBox = (JComboBox)getComponent();
    setData(array);
  }

  /**
   * Removes the previous items in the combo box and adds the items
   * in the specified list.
   * @param list List of items to be added
   * @author Carli Connally
   */
  public void setData(List<Object> list)
  {
    Assert.expect(list != null);

    _comboBox.removeAllItems();

    for(int i=0; i < list.size(); i++)
    {
      _comboBox.addItem(list.get(i));
    }
  }

  /**
   * Removes the previous items in the combo box and adds the items
   * in the specified array.
   * @param array Array of items to be added
   * @author Carli Connally
   */
  public void setData(Object[] array)
  {
    Assert.expect(array != null);
    _comboBox.removeAllItems();

    if(array.length > 0)
    {
      for(int i=0; i < array.length; i++)
      {
        _comboBox.addItem(array[i]);
      }
    }
  }
}
