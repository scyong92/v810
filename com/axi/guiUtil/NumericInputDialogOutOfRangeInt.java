package com.axi.guiUtil;

/**
 * @author Andy Mechtenberg
 */
public interface NumericInputDialogOutOfRangeInt
{
  String getIntValueOutOfRangeLocalizedString(String value, double min, double max);
  String getErrorTitle();
}
