package com.axi.guiUtil;

import java.awt.*;
import java.awt.List;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.util.*;

/**
 * This dialog box contain left choice list and right choice list (one to many map).
 * @author Kok Chun, Tan
 */
public class MultipleChoiceInputDialog extends JDialog
{
  private int _returnValue; // either OK or CANCEL depending on how the user exits
  private Object _leftColumnSelectedValue = "";
  private Object _rightColumnSelectedValue = "";
  private Object[] _leftColumnSelectedValues;
  private Object[] _rightColumnSelectedValues;
  private String _leftColumnMessage;
  private String _rightColumnMessage;
  private String _okButtonText;
  private String _cancelButtonText;
  private boolean _allowMultipleSelection = false;

  private Font _buttonFont;
  private Font _messageFont;

  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private JPanel _leftColumnMessagePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
  private JPanel _rightColumnMessagePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
  private JPanel _buttonPanel = new JPanel();
  private FlowLayout _buttonPanelFlowLayout = new FlowLayout();
  private JPanel _innerButtonPanel = new JPanel();
  private GridLayout _innerButtonPanelGridLayout = new GridLayout();
  private JButton _okButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }

  };
  private JButton _cancelButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };
  private JPanel _topPanel = new JPanel(new BorderLayout());
  private FlowLayout _messagePanelFlowLayout = new FlowLayout();
  private JLabel _leftColumnMessageLabel = new JLabel();
  private JLabel _rightColumnMessageLabel = new JLabel();
  private JPanel _inputPanel = new JPanel();
  private JPanel _leftInputPanel = new JPanel(new BorderLayout());
  private JPanel _rightInputPanel = new JPanel(new BorderLayout());
  private DefaultListModel _leftColumnChoicesListModel = new DefaultListModel();
  private JList _leftChoicesList = new JList(_leftColumnChoicesListModel)
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };
  private JScrollPane _leftColumnChoicesScrollPane = new JScrollPane();
  private DefaultListModel _rightColumnChoicesListModel = new DefaultListModel();
  private JList _rightChoicesList = new JList(_rightColumnChoicesListModel)
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };
  private JScrollPane _rightColumnChoicesScrollPane = new JScrollPane();
  private BorderLayout _inputPanelBorderLayout = new BorderLayout();
  private Border _innerButtonPanelBorder = BorderFactory.createEmptyBorder(5, 10, 0, 10);
  private Border _mainPanelBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
  
  private JTextField _filterTextField = new JTextField(30);
  private JPanel _filterPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
  private JLabel _filterLabel = new JLabel();
  
  private ListSelectionListener _leftChoicesListSelectionListener;
  private ListSelectionListener _rightChoicesListSelectionListener;
  
  private Map<Object, java.util.List<Object>> _leftColumnObjectToRightColumnObjectMap = new LinkedHashMap<Object, java.util.List<Object>> ();
  private boolean _isLeftColumnEnable = true;
  private boolean _isRightColumnEnable = true;

  /**
   * @author Kok Chun, Tan
   */
  public MultipleChoiceInputDialog(Frame frame,
                                      String title,
                                      String leftColumnMessage,
                                      String rightColumnMessage,
                                      String okButtonText,
                                      String cancelButtonText,
                                      Map<Object, java.util.List<Object>> leftColumnObjectToRightColumnObjectMap,
                                      boolean isLeftColumnEnable,
                                      boolean isRightColumnEnable)
  {
    super(frame, title, true);

    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(leftColumnMessage != null);
    Assert.expect(rightColumnMessage != null);
    Assert.expect(okButtonText != null);
    Assert.expect(cancelButtonText != null);
    Assert.expect(leftColumnObjectToRightColumnObjectMap != null);

    _leftColumnMessage = leftColumnMessage;
    _rightColumnMessage = rightColumnMessage;
    _okButtonText = okButtonText;
    _cancelButtonText = cancelButtonText;
    _buttonFont = FontUtil.getDialogButtonFont();
    _messageFont = FontUtil.getDialogMessageFont();
    _leftColumnObjectToRightColumnObjectMap = leftColumnObjectToRightColumnObjectMap;
    _isLeftColumnEnable = isLeftColumnEnable;
    _isRightColumnEnable = isRightColumnEnable;

    _okButton.setEnabled(leftColumnObjectToRightColumnObjectMap.size() > 0);

    populateChoiceList(_filterTextField.getText());

    try
    {
      jbInit();
      pack();
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * @author @author Kok Chun, Tan
   */
  private void jbInit() throws Exception
  {
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _cancelButton.setText(_cancelButtonText);
    _innerButtonPanelGridLayout.setHgap(5);
    _messagePanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _messagePanelFlowLayout.setHgap(0);
    _messagePanelFlowLayout.setVgap(0);
    _cancelButton.setFont(_buttonFont);
    _okButton.setFont(_buttonFont);
    _cancelButton.setMargin(new Insets(2, 8, 2, 8));
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _okButton.setMargin(new Insets(2, 8, 2, 8));
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed();
      }
    });
    
    _filterTextField.setSize(200, 50);
    _filterTextField.getDocument().addDocumentListener(new DocumentListener()
    {
      public void changedUpdate(DocumentEvent e)
      {
        update();
      }
      public void removeUpdate(DocumentEvent e)
      {
        update();
      }
      public void insertUpdate(DocumentEvent e)
      {
        update();
      }
      public void update() 
      {
        populateChoiceList(_filterTextField.getText());
      }
    });
    
    _filterTextField.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyPressed(KeyEvent e)
      {
        handleKeyStroke(e);
      }
    });
    _filterLabel.setFont(_messageFont);
    _filterLabel.setText(com.axi.v810.util.StringLocalizer.keyToString("HP_SEARCH_RECIPE_KEY"));
    _filterPanel.add(_filterLabel, java.awt.BorderLayout.WEST);
    _filterPanel.add(_filterTextField, java.awt.BorderLayout.EAST);

    _leftColumnMessageLabel.setFont(_messageFont);
    _leftColumnMessageLabel.setText(_leftColumnMessage);
    _rightColumnMessageLabel.setFont(_messageFont);
    _rightColumnMessageLabel.setText(_rightColumnMessage);
    _inputPanel.setLayout(_inputPanelBorderLayout);
    _buttonPanelFlowLayout.setHgap(0);
    _buttonPanelFlowLayout.setVgap(0);
    _innerButtonPanel.setBorder(_innerButtonPanelBorder);
    _mainPanel.setBorder(_mainPanelBorder);
    _mainPanelBorderLayout.setVgap(5);
    this.getContentPane().add(_mainPanel);
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    _buttonPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _innerButtonPanel.setLayout(_innerButtonPanelGridLayout);
    _okButton.setText(_okButtonText);
    _mainPanel.add(_buttonPanel, java.awt.BorderLayout.SOUTH);
    _buttonPanel.add(_innerButtonPanel);
    _innerButtonPanel.add(_okButton);
    _innerButtonPanel.add(_cancelButton);
    _topPanel.add(_filterPanel, java.awt.BorderLayout.NORTH);
    _mainPanel.add(_inputPanel, java.awt.BorderLayout.CENTER);
    _mainPanel.add(_topPanel, java.awt.BorderLayout.NORTH);
    _inputPanel.add(_leftInputPanel, java.awt.BorderLayout.WEST);
    _inputPanel.add(_rightInputPanel, java.awt.BorderLayout.EAST);
    _leftColumnChoicesScrollPane.getViewport().add(_leftChoicesList);
    _rightColumnChoicesScrollPane.getViewport().add(_rightChoicesList);
    _leftColumnChoicesScrollPane.setPreferredSize(new Dimension(400, 250));
    _rightColumnChoicesScrollPane.setPreferredSize(new Dimension(400, 250));
    _leftColumnMessagePanel.add(_leftColumnMessageLabel);
    _rightColumnMessagePanel.add(_rightColumnMessageLabel);
    _leftInputPanel.add(_leftColumnMessagePanel, java.awt.BorderLayout.NORTH);
    _rightInputPanel.add(_rightColumnMessagePanel, java.awt.BorderLayout.NORTH);
    _leftInputPanel.add(_leftColumnChoicesScrollPane, java.awt.BorderLayout.SOUTH);
    _rightInputPanel.add(_rightColumnChoicesScrollPane, java.awt.BorderLayout.SOUTH);

    if (_allowMultipleSelection)
    {
      _leftChoicesList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
      _rightChoicesList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    }
    else
    {
      _leftChoicesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      _rightChoicesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    _leftChoicesListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        if (e.getValueIsAdjusting() == false)
        {
          int index = _leftChoicesList.getSelectedIndex();
          _leftChoicesList.ensureIndexIsVisible(index);
        }
        leftChoiceListSelectionListener();
      }
    };
    _leftChoicesList.getSelectionModel().addListSelectionListener(_leftChoicesListSelectionListener);

    
    
    _rightChoicesListSelectionListener = new ListSelectionListener()
    {
      // this ensures that a selected item is visible.  At jdk1.5, this is the default behavior, so this could be removed.
      public void valueChanged(ListSelectionEvent e)
      {
        if (e.getValueIsAdjusting() == false)
        {
          int index = _rightChoicesList.getSelectedIndex();
          _rightChoicesList.ensureIndexIsVisible(index);
        }
      }
    };
    _rightChoicesList.getSelectionModel().addListSelectionListener(_rightChoicesListSelectionListener);
    
    _leftChoicesList.setEnabled(_isLeftColumnEnable);
    _rightChoicesList.setEnabled(_isRightColumnEnable);
    
    getRootPane().setDefaultButton(_okButton);
  }

  /**
   * @author Kok Chun, Tan
   */
  public int showDialog()
  {
    setVisible(true);
    return _returnValue;
  }

  /**
   * @author Kok Chun, Tan
   */
  public int showDialog(Font font)
  {
    Assert.expect(font != null);
    SwingUtils.setFont(this, font);
    pack();
    setVisible(true);
    return _returnValue;
  }

  /**
   * @author Kok Chun, Tan
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    int eventId = e.getID();
    if (eventId == WindowEvent.WINDOW_CLOSING)
    {
      cancelAction();
    }
  }

  /**
   * @author Kok Chun, Tan
   */
  public Object getLeftColumnSelectedValue()
  {
    Assert.expect(_leftColumnSelectedValue != null);
    return _leftColumnSelectedValue;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public Object getRightColumnSelectedValue()
  {
    Assert.expect(_rightColumnSelectedValue != null);
    return _rightColumnSelectedValue;
  }

  /**
   * @author Kok Chun, Tan
   */
  public Object[] getLeftColumnSelectedValues()
  {
    Assert.expect(_leftColumnSelectedValues != null);
    return _leftColumnSelectedValues;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public Object[] getRightColumnSelectedValues()
  {
    Assert.expect(_rightColumnSelectedValues != null);
    return _rightColumnSelectedValues;
  }

  /**
   * @author Kok Chun, Tan
   * @Edited Kee Chin Seong - Removing the Event as there is no use
   */
  private void okButton_actionPerformed()
  {
    _leftColumnSelectedValue = _leftChoicesList.getSelectedValue();
    _leftColumnSelectedValues = _leftChoicesList.getSelectedValues();
    _rightColumnSelectedValue = _rightChoicesList.getSelectedValue();
    _rightColumnSelectedValues = _rightChoicesList.getSelectedValues();
    // ok, this is weird, but if you hit CTRL-\, then NO VALUE is selected in the list
    // _leftColumnSelectedValue will be null.  Pretend nothing happened.
    if (_leftColumnSelectedValue == null)
    {
      return;
    }
    _returnValue = JOptionPane.OK_OPTION;
    dispose();
  }

  /**
   * @author Kok Chun, Tan
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    cancelAction();
  }

  /**
   * @author Kok Chun, Tan
   */
  private void cancelAction()
  {
    _returnValue = JOptionPane.CANCEL_OPTION;
    _leftColumnSelectedValue = null;
    _rightColumnSelectedValue = null;
    dispose();
  }
  
  /**
   * Populate left column choice list.
   * This method already handle for serach function.
   * @author Kok Chun, Tan
   */
  private void populateChoiceList(String filter)
  {
    java.util.List<String> availableLeftColumnObjects = new ArrayList<>();
    
    _leftChoicesList.getSelectionModel().removeListSelectionListener(_leftChoicesListSelectionListener);
    _leftChoicesList.clearSelection();
    _leftColumnChoicesListModel.clear();
    _rightChoicesList.clearSelection();
    _rightColumnChoicesListModel.clear();
    
    for (Map.Entry<Object, java.util.List<Object>> leftColumnObjectToRightColumnObjectSet : _leftColumnObjectToRightColumnObjectMap.entrySet())
    {
      String leftColumnObject = (String) leftColumnObjectToRightColumnObjectSet.getKey();
      java.util.List<Object> rightColumnObjects = leftColumnObjectToRightColumnObjectSet.getValue();

      if (filter.isEmpty())
      {
        if (availableLeftColumnObjects.contains(leftColumnObject) == false)
        {
          availableLeftColumnObjects.add(leftColumnObject);
        }
      }
      else
      {
        //check project availableObject
        if (leftColumnObject.toLowerCase().contains(filter.toLowerCase()))
        {
          if (availableLeftColumnObjects.contains(leftColumnObject) == false)
          {
            availableLeftColumnObjects.add(leftColumnObject);
          }
        }
        //check variation availableObject
        for (Object variationName : rightColumnObjects)
        {
          String name = (String) variationName;
          if (name.toLowerCase().contains(filter.toLowerCase()))
          {
            if (availableLeftColumnObjects.contains(leftColumnObject) == false)
            {
              availableLeftColumnObjects.add(leftColumnObject);
              break;
            }
          }
        }
      }
    }
    
    for (String availableObject : availableLeftColumnObjects)
    {
      Object projectName = availableObject;
      _leftColumnChoicesListModel.addElement(projectName);
    }
    
    _leftChoicesList.getSelectionModel().addListSelectionListener(_leftChoicesListSelectionListener);
    _leftChoicesList.setSelectedIndex(0);
    leftChoiceListSelectionListener();
  }
  
  /**
   * Use to update right column choice list.
   * @author Kok Chun, Tan
   * @Kee chin Seong - Auto Select on the Variant found 
   */
  private void leftChoiceListSelectionListener()
  {
    int index = _leftChoicesList.getSelectedIndex();
    
    Object variantNameSelected = null;
    String filter = _filterTextField.getText();
    
    if (_leftChoicesList.isSelectionEmpty())
    {
     return;
    }
    _leftColumnSelectedValue = _leftChoicesList.getModel().getElementAt(index);
    java.util.List<Object> variationNames = _leftColumnObjectToRightColumnObjectMap.get(_leftColumnSelectedValue);
    _rightColumnChoicesListModel.clear();
    for (Object name : variationNames)
    {
      if(name.toString().startsWith(filter))
        variantNameSelected = name;
      
      _rightColumnChoicesListModel.addElement(name);
    }
    if (variationNames.isEmpty() == false)
    {
      if(variantNameSelected == null)
         _rightChoicesList.setSelectedIndex(0);
      else
         _rightChoicesList.setSelectedValue(variantNameSelected, true);
    }
  }
  
  /**
   * @author Kee Chin Seong
   * @param keyEvent 
   * Handling Key stroke function for Barcode Scanner
   */
  public void handleKeyStroke(KeyEvent keyEvent)
  {
    int retCode = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0).getKeyCode();
    if (keyEvent.getKeyCode() == retCode)
    {
       okButton_actionPerformed();
    }
  }
}
