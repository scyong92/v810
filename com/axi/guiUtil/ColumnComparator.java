package com.axi.guiUtil;

import java.util.*;

/**
 * This class compares columns for the table sorter
 *
 * @author Claude Duguay of JavaPro
 */
public class ColumnComparator implements Comparator<Vector>
{
  private int _index;
  private boolean _ascending;

  public ColumnComparator(int index, boolean ascending)
  {
    _index = index;
    _ascending = ascending;
  }

  public int compare(Vector vOne, Vector vTwo)
  {
    Object oOne = vOne.elementAt(_index);
    Object oTwo = vTwo.elementAt(_index);
    if (oOne instanceof Comparable &&
        oTwo instanceof Comparable)
    {
      Comparable cOne = (Comparable)oOne;
      Comparable cTwo = (Comparable)oTwo;
      if (_ascending)
      {
        return cOne.compareTo(cTwo);
      }
      else
      {
        return cTwo.compareTo(cOne);
      }
    }
    return 1;
  }
}

