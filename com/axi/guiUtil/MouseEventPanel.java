package com.axi.guiUtil;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.text.*;

import com.axi.util.*;

/**
 * The class is used by the GraphicsEngine class.
 * It handles mouse events that control drawing a selection box and also dragging
 * the current drawing.
 *
 * @see GraphicsEngine
 *
 * @author Bill Darbie
 */
class MouseEventPanel extends RendererPanel
{
  final static float _dash1[] = {10.0f};
  final static BasicStroke _dashedStroke = new BasicStroke(1.0f,
                                                           BasicStroke.CAP_BUTT,
                                                           BasicStroke.JOIN_MITER,
                                                           10.0f, _dash1, 0.0f);

  // the mouse event panel recognizes one of these modes
  // Generally, if NONE of these modes are desired by the user, the MouseEventPanel is set to invisible,
  // and receives no mouse events at all.
  private final int ZOOM_RECTANGLE_MODE = 0;
  private final int DRAG_MODE = 1;
  private final int QUICK_DRAG_MODE = 2;
  private final int GROUP_SELECT_MODE = 3;
  private final int MEASUREMENT_MODE = 4;
  private final int MOUSE_CLICK_MODE = 5;
  
  private final int DRAG_SELECT_REGION_MODE = 6;
  private final int SELECT_REGION_MODE = 7;
  private final int DRAG_RECTANGLE_MODE = 8;
  private final int DRAG_OVAL_MODE = 9;

  private final int DRAG_OPTICAL_REGION_MODE = 8;
  private final int DRAG_SELECTED_OPTICAL_REGION_MODE = 9;
 
  private int _currentMode = ZOOM_RECTANGLE_MODE;
  private int _currentDragRegionShape = DRAG_RECTANGLE_MODE;

  private boolean _ctrlClick = false; // used to track if the user held CNTRL down when using the mouse for group select


  private int _xMouseDragOrig = 0;
  private int _yMouseDragOrig = 0;
  // the original starting x, y point of the rectangle drawn on the screen
  private int _origBoxX;
  private int _origBoxY;
  private boolean _groupSelectionInProgress = false;

  // _boxX, _boxY, _boxWidth, _boxHeight specify the current rectangle drawn on the screen
  private int _boxX;
  private int _boxY;
  private int _boxWidth;
  private int _boxHeight;

  private int _origLineX;
  private int _origLineY;
  private int _lineBeginX;
  private int _lineBeginY;
  private int _lineEndX;
  private int _lineEndY;
  private double _measurementDistance = 0.0;
  private Stroke _measurementStroke = new BasicStroke(3);

  // _prevRect describes the previous rectangle on the screen
  private Rectangle _prevRect = new Rectangle(0, 0, 0, 0);
  // _currRect describes the current rectangle on the screen
  private Rectangle _currRect = new Rectangle(0, 0, 0, 0);
  // _unionRect is the union of _prevRect and _currRect
  private Rectangle _unionRect = new Rectangle(0, 0, 0, 0);
  
  // Default image region size
  private double _maxImageRegionWidth = MathUtil.convertMilsToNanoMeters(1000);
  private double _maxImageRegionHeight = MathUtil.convertMilsToNanoMeters(1000);

  private GraphicsEngine _graphicsEngine;
  private int _setSelectedVisibleRenderersCount = 0;
  private boolean _mouseRightClickMode = false;

  /**
   * @author Bill Darbie
   */
  public MouseEventPanel(GraphicsEngine graphicsEngine, Color color)
  {
    super(color);
    Assert.expect(graphicsEngine != null);

    _graphicsEngine = graphicsEngine;

    jbInit();
  }

  /**
   * @author Bill Darbie
   */
  private void jbInit()
  {
    // add listeners
    addMouseMotionListener(new MouseMotionAdapter()
    {
      public void mouseDragged(MouseEvent mouseEvent)
      {
        handleMouseDragged(mouseEvent);
      }
    });
    addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent mouseEvent)
      {
        handleMouseReleased(mouseEvent);
      }
      public void mousePressed(MouseEvent mouseEvent)
      {
        handleMousePressed(mouseEvent);
      }
    });
    addMouseWheelListener(new MouseWheelListener()
    {
      public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent)
      {
        handleMouseWheelMoved(mouseWheelEvent);
      }
    });
  }

  /**
   * Puts this in zoom rectangle mode on the left click
   * @author Andy Mechtenberg
   */
  void enableZoomRectangleMode()
  {
    _currentMode = ZOOM_RECTANGLE_MODE;
  }

  /**
   * Puts this in drag graphics mode on the left click
   * @author Andy Mechtenberg
   */
  void enableDragMode()
  {
    _currentMode = DRAG_MODE;
  }
  
  /*
   * @author KEe Chin Seong
   */
  void enabledRectangleDragShape()
  {
    _currentDragRegionShape = DRAG_RECTANGLE_MODE;
  }
  
  /*
   * @author KEe Chin Seong
   */
  void enabledOvalDragShape()
  {
    _currentDragRegionShape = DRAG_OVAL_MODE;
  }

  /**
   * @author George A. David
   */
  void enableMeasurementMode()
  {
    _currentMode = MEASUREMENT_MODE;
  }
  
  /**
   * @author Jack Hwee
   */
  void enableDragRegionMode()
  {
    _currentMode = DRAG_OPTICAL_REGION_MODE;
  }
  
  /**
   * @author Jack Hwee
   */
  void enableDragSelectedRegionMode()
  {
    _currentMode = DRAG_SELECTED_OPTICAL_REGION_MODE;
  }

  /**
   * @author George A. David
   */
  void enableMouseClickMode()
  {
    _currentMode = MOUSE_CLICK_MODE;
  }

  /**
   * Puts this in group select mode on the left click
   * @author Andy Mechtenberg
   */
  void setGroupSelectMode(boolean enabled)
  {
    if (enabled)
      _currentMode = GROUP_SELECT_MODE;
    else
      _currentMode = ZOOM_RECTANGLE_MODE;
  }

  /**
   * @author sham
   */
  void enableSelectRegionMode()
  {
    _currentMode = SELECT_REGION_MODE;
  }
  
  /*
   * @kee Chin Seong
   */
  boolean isEnabledSelectRegionMode()
  {
     return (_currentMode == SELECT_REGION_MODE) ? true : false;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleMousePressed(MouseEvent mouseEvent)
  {
    int quickDragMode = InputEvent.SHIFT_DOWN_MASK;
    _graphicsEngine.setMouseRightClickMode(false);

    if (((mouseEvent.getModifiersEx() & quickDragMode) == quickDragMode) &&
        ((_currentMode == GROUP_SELECT_MODE) || (_currentMode == MOUSE_CLICK_MODE)))
      _currentMode = QUICK_DRAG_MODE;

    if (_currentMode == MEASUREMENT_MODE)
    {
      // create a line
      if ((_origLineX == 0) && (_origLineY == 0))
      {
        // this is the first drag event, so store the x, y starting point
        _origLineX = mouseEvent.getX();
        _origLineY = mouseEvent.getY();
        _lineBeginX = _origLineX;
        _lineBeginY = _origLineY;
        _measurementDistance = 0;
      }
    }
   
    if (_currentMode == GROUP_SELECT_MODE)
    {
      // in group select mode, I'd like to draw a rectangle the same as zoom mode, but with a dotted line instead of a solid line
      // zoom window mode
      // create a box
      if ((_origBoxX == 0) && (_origBoxY == 0))
      {
        // this is the first drag event, so store the x, y starting point
        _origBoxX = mouseEvent.getX();
        _origBoxY = mouseEvent.getY();
        _boxX = _origBoxX;
        _boxY = _origBoxY;
        _prevRect.setRect(_origBoxX, _origBoxY, _boxX, _boxY);
      }
    }
    
      // handle right-click first
    if ((mouseEvent.getModifiers() & MouseEvent.BUTTON3_MASK) == MouseEvent.BUTTON3_MASK)
    {
      _mouseRightClickMode = true;
      
      if (_currentMode == DRAG_OPTICAL_REGION_MODE)
      {
          _graphicsEngine.setMouseRightClickMode(true);

          _boxX = 0;
          _boxY = 0;
          _boxWidth = 0;
          _boxHeight = 0;
          
          int x = mouseEvent.getX();
          int y = mouseEvent.getY();
          
          Point point = new Point(x,y);
       
          _ctrlClick = true;
          _graphicsEngine.setMouseRightClickCoordinate(point);
          _graphicsEngine.mouseClicked(mouseEvent);      
      }
      else if (_currentMode == DRAG_SELECTED_OPTICAL_REGION_MODE)
      {
          _graphicsEngine.setMouseRightClickMode(true);

          _boxX = 0;
          _boxY = 0;
          _boxWidth = 0;
          _boxHeight = 0;
          
          int x = mouseEvent.getX();
          int y = mouseEvent.getY();
          
          Point point = new Point(x,y);
       
          _ctrlClick = true;
          _graphicsEngine.setMouseRightClickCoordinate(point); 
          _graphicsEngine.mouseClicked(mouseEvent);
      }
    }
  }
  
  /**
   * @author Bill Darbie
   */
  private void handleMouseDragged(MouseEvent mouseEvent)
  {
    int modifiers = mouseEvent.getModifiers();
    int quickDragMode = InputEvent.SHIFT_DOWN_MASK;

    if (((mouseEvent.getModifiersEx() & quickDragMode) == quickDragMode) &&
        (_currentMode == GROUP_SELECT_MODE))
      _currentMode = QUICK_DRAG_MODE;

    if ((modifiers & MouseEvent.BUTTON1_MASK) == MouseEvent.BUTTON1_MASK)
    {
      _mouseRightClickMode = false;
      if (_currentMode == GROUP_SELECT_MODE)
      {
        // if the ctrl key is down when the click happened, we want to ADD the new selection.  otherwise, replace with the new selection
        int multipleSelectMask = InputEvent.CTRL_DOWN_MASK;
        if ((mouseEvent.getModifiersEx() & multipleSelectMask) == multipleSelectMask)
          _ctrlClick = true;
        _groupSelectionInProgress = true;
        // in group select mode, I'd like to draw a rectangle the same as zoom mode, but with a dotted line instead of a solid line
        // zoom window mode
        // create a box
        if ((_origBoxX == 0) && (_origBoxY == 0))
        {
          // this is the first drag event, so store the x, y starting point
          _origBoxX = mouseEvent.getX();
          _origBoxY = mouseEvent.getY();

          _boxX = _origBoxX;
          _boxY = _origBoxY;
          _prevRect.setRect(_origBoxX, _origBoxY, _boxX, _boxY);
        }
        else
        {
          // figure out the width and height of the box
          _boxWidth = mouseEvent.getX() - _origBoxX;
          _boxHeight = mouseEvent.getY() - _origBoxY;

          // keep x,y at the top left
          if (_boxWidth >= 0)
            _boxX = _origBoxX;
          else
          {
            _boxX = _origBoxX + _boxWidth;
            _boxWidth = -_boxWidth;
          }
          if (_boxHeight >= 0)
            _boxY = _origBoxY;
          else
          {
            _boxY = _origBoxY + _boxHeight;
            _boxHeight = -_boxHeight;
          }
        }

        // figure out the largest area that needs to be redrawn
        _currRect.setRect(_boxX, _boxY, _boxWidth, _boxHeight);
        _unionRect.setRect(_currRect);
        _unionRect.add(_prevRect);
        _unionRect.setRect(_unionRect.x, _unionRect.y, _unionRect.width + 1, _unionRect.height + 1);

        
        // repaint the union of the area of the current rectangle and the previous rectangle
        repaint(_unionRect);

        // reset to the current box size
        _prevRect.setRect(_currRect);
      }
      else if (_currentMode == ZOOM_RECTANGLE_MODE)
      {
        // zoom window mode
        // create a box
        if ((_origBoxX == 0) && (_origBoxY == 0))
        {
          // this is the first drag event, so store the x, y starting point
          _origBoxX = mouseEvent.getX();
          _origBoxY = mouseEvent.getY();
          _boxX = _origBoxX;
          _boxY = _origBoxY;
          _prevRect.setRect(_origBoxX, _origBoxY, _boxX, _boxY);
        }
        else
        {
          
          // figure out the width and height of the box
          _boxWidth = mouseEvent.getX() - _origBoxX;
          _boxHeight = mouseEvent.getY() - _origBoxY;

          // keep x,y at the top left
          if (_boxWidth >= 0)
            _boxX = _origBoxX;
          else
          {
            _boxX = _origBoxX + _boxWidth;
            _boxWidth = -_boxWidth;
          }
          if (_boxHeight >= 0)
            _boxY = _origBoxY;
          else
          {
            _boxY = _origBoxY + _boxHeight;
            _boxHeight = -_boxHeight;
          }
        }

        // figure out the largest area that needs to be redrawn
        _currRect.setRect(_boxX, _boxY, _boxWidth, _boxHeight);
        _unionRect.setRect(_currRect);
        _unionRect.add(_prevRect);
        _unionRect.setRect(_unionRect.x, _unionRect.y, _unionRect.width + 1, _unionRect.height + 1);

        // repaint the union of the area of the current rectangle and the previous rectangle
        repaint(_unionRect);

        // reset to the current box size
        _prevRect.setRect(_currRect);
      }
      else if(_currentMode == SELECT_REGION_MODE)
      {
        // zoom window mode
        // create a box
        if ((_origBoxX == 0) && (_origBoxY == 0))
        {
          // this is the first drag event, so store the x, y starting point
          _origBoxX = mouseEvent.getX();
          _origBoxY = mouseEvent.getY();
          _boxX = _origBoxX;
          _boxY = _origBoxY;
          _prevRect.setRect(_origBoxX, _origBoxY, _boxX, _boxY);
        }
        else
        {
          // figure out the width and height of the box
          _boxWidth = mouseEvent.getX() - _origBoxX;
          _boxHeight = mouseEvent.getY() - _origBoxY;
          // keep x,y at the top left
          //Khaw Chek Hau - XCR3765: 2D Alignment no maximum limit for alignment region selection
          if(com.axi.v810.gui.mainMenu.MainMenuGui.getInstance().isCadCreatorCurrentEnvironment() == false)
          {
            if (_boxWidth >= 0)
            {
              if (_graphicsEngine.convertFromPixelToRendererCoordinates(_boxX + _boxWidth, _boxY + _boxHeight).getX() - _graphicsEngine.convertFromPixelToRendererCoordinates(_boxX, _boxY).getX() > _maxImageRegionWidth && _currentMode == SELECT_REGION_MODE)
              {
                double newXInNano = _graphicsEngine.convertFromPixelToRendererCoordinates(_boxX, _boxY).getX() + _maxImageRegionWidth;
                double yInNano = _graphicsEngine.convertFromPixelToRendererCoordinates(_boxX, _boxY).getY();
                _boxX = _origBoxX;
                _boxWidth = (int) (_graphicsEngine.convertFromRendererToPixelCoordinates(newXInNano, yInNano).getX()) - _boxX;
              }
              else
              {
                _boxX = _origBoxX;
              }
            }
            else
            {
              if ((_graphicsEngine.convertFromPixelToRendererCoordinates(_boxX, _boxY).getX() - _graphicsEngine.convertFromPixelToRendererCoordinates(_boxX + _boxWidth, _boxY + _boxHeight).getX()) > _maxImageRegionWidth && _currentMode == SELECT_REGION_MODE)
              {
                double newXInNano = _graphicsEngine.convertFromPixelToRendererCoordinates(_boxX, _boxY).getX() - _maxImageRegionWidth;
                double yInNano = _graphicsEngine.convertFromPixelToRendererCoordinates(_boxX, _boxY).getY();
                _boxWidth = _boxX - (int) (_graphicsEngine.convertFromRendererToPixelCoordinates(newXInNano, yInNano).getX());
                _boxX = _origBoxX - _boxWidth;
              }
              else
              {
                _boxX = _origBoxX + _boxWidth;
                _boxWidth = -_boxWidth;
              }
            }
            if (_boxHeight >= 0)
            {
              if ((_graphicsEngine.convertFromPixelToRendererCoordinates(_boxX, _boxY).getY() - _graphicsEngine.convertFromPixelToRendererCoordinates(_boxX + _boxWidth, _boxY + _boxHeight).getY()) > _maxImageRegionHeight && _currentMode == SELECT_REGION_MODE)
              {
                double xInNano = _graphicsEngine.convertFromPixelToRendererCoordinates(_boxX, _boxY).getX();
                double newYInNano = _graphicsEngine.convertFromPixelToRendererCoordinates(_boxX, _boxY).getY() - _maxImageRegionHeight;
                _boxHeight = (int) (_graphicsEngine.convertFromRendererToPixelCoordinates(xInNano, newYInNano).getY()) - _boxY;
                _boxY = _origBoxY;
              }
              else
              {
                _boxY = _origBoxY;
              }
            }
            else
            {
              if ((_graphicsEngine.convertFromPixelToRendererCoordinates(_boxX + _boxWidth, _boxY + _boxHeight).getY() - _graphicsEngine.convertFromPixelToRendererCoordinates(_boxX, _boxY).getY()) > _maxImageRegionHeight && _currentMode == SELECT_REGION_MODE)
              {
                double xInNano = _graphicsEngine.convertFromPixelToRendererCoordinates(_boxX, _boxY).getX();
                double newYInNano = _graphicsEngine.convertFromPixelToRendererCoordinates(_boxX, _boxY).getY() + _maxImageRegionHeight;
                _boxHeight = _boxY - (int) (_graphicsEngine.convertFromRendererToPixelCoordinates(xInNano, newYInNano).getY());
                _boxY = _origBoxY - _boxHeight;
              }
              else
              {
                _boxY = _origBoxY + _boxHeight;
                _boxHeight = -_boxHeight;
              }
            }
          }
          else
          {
             _boxX = _origBoxX;
             _boxY = _origBoxY; 
          }
        }

        // figure out the largest area that needs to be redrawn
        _currRect.setRect(_boxX, _boxY, _boxWidth, _boxHeight);
        _unionRect.setRect(_currRect);
        _unionRect.add(_prevRect);
        _unionRect.setRect(_unionRect.x, _unionRect.y, _unionRect.width + 1, _unionRect.height + 1);
        // repaint the union of the area of the current rectangle and the previous rectangle
        repaint(_unionRect);
        
        // reset to the current box size
        _prevRect.setRect(_currRect);
      }
      else if ((_currentMode == DRAG_MODE) || (_currentMode == QUICK_DRAG_MODE))
      {
        _graphicsEngine.setMouseRightClickMode(true);
        // drag mode
        // the left mouse button was pressed
        // we want to drag the drawing to follow the mouse drag
        int xDragDelta = 0;
        int yDragDelta = 0;
        if ((_xMouseDragOrig == 0) && (_xMouseDragOrig == 0))
        {
          // this is the first drag event, so store the x,y as the original starting point
          _xMouseDragOrig = mouseEvent.getX();
          _yMouseDragOrig = mouseEvent.getY();
        }
        else
        {
          // we are at a 2nd drag point, calculate the delta that the mouse has been dragged
          xDragDelta = mouseEvent.getX() - _xMouseDragOrig;
          yDragDelta = mouseEvent.getY() - _yMouseDragOrig;
        }

        if ((xDragDelta != 0) || (yDragDelta != 0))
        {
          _graphicsEngine.dragGraphics(xDragDelta, yDragDelta);

          // now set the current x, y to the new original drag point
          _xMouseDragOrig += xDragDelta;
          _yMouseDragOrig += yDragDelta;
        }
        // repaint everything since it was all dragged
        repaint();
      }
      else if (_currentMode == MEASUREMENT_MODE)
      {
        // if the ctrl key is down when the click happened, we want to ADD the new selection.  otherwise, replace with the new selection
        int multipleSelectMask = InputEvent.CTRL_DOWN_MASK;
        if ((mouseEvent.getModifiersEx() & multipleSelectMask) == multipleSelectMask)
          _ctrlClick = true;
        else
          _ctrlClick = false;

        // create a line
        if ((_origLineX == 0) && (_origLineY == 0))
        {
          // this is the first drag event, so store the x, y starting point
          _origLineX = mouseEvent.getX();
          _origLineY = mouseEvent.getY();
          _lineBeginX = _origLineX;
          _lineBeginY = _origLineY;
          _measurementDistance = 0;
//          _prevRect.setRect(_origLineX, _origLineY, _origLineX + _lineBeginX, _origLineY + _lineBeginY);
        }
        else
        {
          _lineEndX = mouseEvent.getX();
          _lineEndY = mouseEvent.getY();

          if(_ctrlClick)
          {
            boolean isWidthLargerThanHeight = Math.abs(_lineEndX - _lineBeginX) > Math.abs(_lineEndY - _lineBeginY);
            if (isWidthLargerThanHeight)
              _measurementDistance = _graphicsEngine.calculateDistanceInMeasurementUnits(_lineBeginX, 0, _lineEndX, 0);
            else
              _measurementDistance = _graphicsEngine.calculateDistanceInMeasurementUnits(0, _lineBeginY, 0, _lineEndY);
          }
          else
            _measurementDistance = _graphicsEngine.calculateDistanceInMeasurementUnits(_lineBeginX, _lineBeginY, _lineEndX, _lineEndY);

          // figure out the largest area that needs to be redrawn
          Line2D line = new Line2D.Double(_lineBeginX, _lineBeginY, _lineEndX, _lineEndY);

          _currRect.setRect(line.getBounds());
          _currRect.setRect(_currRect.getX(), _currRect.getY(), _currRect.getWidth() + 5, _currRect.getHeight() + 5);
          _unionRect.setRect(_currRect);
          _unionRect.add(_prevRect);
          _unionRect.setRect(_unionRect.x, _unionRect.y, _unionRect.width + 1, _unionRect.height + 1);

          // repaint the union of the area of the current rectangle and the previous rectangle
//          repaint(_unionRect);
          repaint();

          // reset to the current box size
          _prevRect.setRect(_currRect);
        }
      }
      else if (_currentMode == DRAG_OPTICAL_REGION_MODE)
      {
        _graphicsEngine.resetSelectedRegionCoordinate();
        _graphicsEngine.setDragRegionModeAvailable(true);
        _graphicsEngine.setOpticalRegionFirstDrag(true);
       
        _setSelectedVisibleRenderersCount = 0;
         // if the ctrl key is down when the click happened, we want to ADD the new selection.  otherwise, replace with the new selection
        int multipleSelectMask = InputEvent.CTRL_DOWN_MASK;
        if ((mouseEvent.getModifiersEx() & multipleSelectMask) == multipleSelectMask)
          _ctrlClick = true;
        _groupSelectionInProgress = true;
        // in group select mode, I'd like to draw a rectangle the same as zoom mode, but with a dotted line instead of a solid line
        // zoom window mode
        // create a box
        if ((_origBoxX == 0) && (_origBoxY == 0))
        {
          // this is the first drag event, so store the x, y starting point
          _origBoxX = mouseEvent.getX();
          _origBoxY = mouseEvent.getY();

          _boxX = _origBoxX;
          _boxY = _origBoxY;
          _prevRect.setRect(_origBoxX, _origBoxY, _boxX, _boxY);
          
        }
        else
        {
          // figure out the width and height of the box
          _boxWidth = mouseEvent.getX() - _origBoxX;
          _boxHeight = mouseEvent.getY() - _origBoxY;
       
          // keep x,y at the top left
          if (_boxWidth >= 0)
            _boxX = _origBoxX;
          else
          {
            _boxX = _origBoxX + _boxWidth;
            _boxWidth = -_boxWidth;
          }
          if (_boxHeight >= 0)
            _boxY = _origBoxY;
          else
          {
            _boxY = _origBoxY + _boxHeight;
            _boxHeight = -_boxHeight;
          }
        } 
 
         // figure out the largest area that needs to be redrawn
         Rectangle rect = new Rectangle(_boxX, _boxY, _boxWidth, _boxHeight);

         _currRect.setRect(rect.getBounds());

        // figure out the largest area that needs to be redrawn
        _currRect.setRect(_boxX, _boxY, _boxWidth, _boxHeight);
        _unionRect.setRect(_currRect);
        _unionRect.add(_prevRect);
        _unionRect.setRect(_unionRect.x, _unionRect.y, _unionRect.width + 20, _unionRect.height + 20);

        // repaint the union of the area of the current rectangle and the previous rectangle
        repaint(_unionRect);

        // reset to the current box size
        _prevRect.setRect(_currRect);
     
      }
      else if (_currentMode == DRAG_SELECTED_OPTICAL_REGION_MODE)
      {
        // if the ctrl key is down when the click happened, we want to ADD the new selection.  otherwise, replace with the new selection
        int multipleSelectMask = InputEvent.CTRL_DOWN_MASK;
        if ((mouseEvent.getModifiersEx() & multipleSelectMask) == multipleSelectMask)
          _ctrlClick = true;
        _groupSelectionInProgress = true;
        // in group select mode, I'd like to draw a rectangle the same as zoom mode, but with a dotted line instead of a solid line
        // zoom window mode
        // create a box
        if ((_origBoxX == 0) && (_origBoxY == 0))
        {
          // this is the first drag event, so store the x, y starting point
          _origBoxX = mouseEvent.getX();
          _origBoxY = mouseEvent.getY();

          _boxX = _origBoxX;
          _boxY = _origBoxY;
          _prevRect.setRect(_origBoxX, _origBoxY, _boxX, _boxY);
          
        }
        else
        {
          // figure out the width and height of the box
          _boxWidth = mouseEvent.getX() - _origBoxX;
          _boxHeight = mouseEvent.getY() - _origBoxY;
       
          // keep x,y at the top left
          if (_boxWidth >= 0)
            _boxX = _origBoxX;
          else
          {
            _boxX = _origBoxX + _boxWidth;
            _boxWidth = -_boxWidth;
          }
          if (_boxHeight >= 0)
            _boxY = _origBoxY;
          else
          {
            _boxY = _origBoxY + _boxHeight;
            _boxHeight = -_boxHeight;
          }
        } 
        
         // figure out the largest area that needs to be redrawn
         Rectangle rect = new Rectangle(_boxX, _boxY, _boxWidth, _boxHeight);

         _currRect.setRect(rect.getBounds());

        // figure out the largest area that needs to be redrawn
        _currRect.setRect(_boxX, _boxY, _boxWidth, _boxHeight);
        _unionRect.setRect(_currRect);
        _unionRect.add(_prevRect);
        _unionRect.setRect(_unionRect.x, _unionRect.y, _unionRect.width + 20, _unionRect.height + 20);

        // repaint the union of the area of the current rectangle and the previous rectangle
        repaint(_unionRect);

        // reset to the current box size
        _prevRect.setRect(_currRect);
      }
      else if (_currentMode == MOUSE_CLICK_MODE)
      {
        // do nothing
      }
      else
        Assert.expect(false, "Mode not recognized " + _currentMode);
    }
    
     // handle right-click first
    if ((modifiers & MouseEvent.BUTTON3_MASK) == MouseEvent.BUTTON3_MASK)
    {
      _mouseRightClickMode = true;
      if (_currentMode == DRAG_OPTICAL_REGION_MODE)
      {
       //  _graphicsEngine.resetSelectedRegionCoordinate();
       
         // if the ctrl key is down when the click happened, we want to ADD the new selection.  otherwise, replace with the new selection
        int multipleSelectMask = InputEvent.CTRL_DOWN_MASK;
        if ((mouseEvent.getModifiersEx() & multipleSelectMask) == multipleSelectMask)
          _ctrlClick = true;
        _groupSelectionInProgress = true;
        // in group select mode, I'd like to draw a rectangle the same as zoom mode, but with a dotted line instead of a solid line
        // zoom window mode
        // create a box
        if ((_origBoxX == 0) && (_origBoxY == 0))
        {
          // this is the first drag event, so store the x, y starting point
          _origBoxX = mouseEvent.getX();
          _origBoxY = mouseEvent.getY();

          _boxX = _origBoxX;
          _boxY = _origBoxY;
          _prevRect.setRect(_origBoxX, _origBoxY, _boxX, _boxY);
          
        }
        else
        {
          // figure out the width and height of the box
          _boxWidth = mouseEvent.getX() - _origBoxX;
          _boxHeight = mouseEvent.getY() - _origBoxY;
       
          // keep x,y at the top left
          if (_boxWidth >= 0)
            _boxX = _origBoxX;
          else
          {
            _boxX = _origBoxX + _boxWidth;
            _boxWidth = -_boxWidth;
          }
          if (_boxHeight >= 0)
            _boxY = _origBoxY;
          else
          {
            _boxY = _origBoxY + _boxHeight;
            _boxHeight = -_boxHeight;
          }
        } 
        
         // figure out the largest area that needs to be redrawn
         Rectangle rect = new Rectangle(_boxX, _boxY, _boxWidth, _boxHeight);

         _currRect.setRect(rect.getBounds());

        // figure out the largest area that needs to be redrawn
        _currRect.setRect(_boxX, _boxY, _boxWidth, _boxHeight);
        _unionRect.setRect(_currRect);
        _unionRect.add(_prevRect);
        _unionRect.setRect(_unionRect.x, _unionRect.y, _unionRect.width + 20, _unionRect.height + 20);

        // repaint the union of the area of the current rectangle and the previous rectangle
        repaint(_unionRect);

        // reset to the current box size
        _prevRect.setRect(_currRect);
     
      }
      else  if (_currentMode == DRAG_SELECTED_OPTICAL_REGION_MODE)
      {
       //  _graphicsEngine.resetSelectedRegionCoordinate();
       
         // if the ctrl key is down when the click happened, we want to ADD the new selection.  otherwise, replace with the new selection
        int multipleSelectMask = InputEvent.CTRL_DOWN_MASK;
        if ((mouseEvent.getModifiersEx() & multipleSelectMask) == multipleSelectMask)
          _ctrlClick = true;
        _groupSelectionInProgress = true;
        // in group select mode, I'd like to draw a rectangle the same as zoom mode, but with a dotted line instead of a solid line
        // zoom window mode
        // create a box
        if ((_origBoxX == 0) && (_origBoxY == 0))
        {
          // this is the first drag event, so store the x, y starting point
          _origBoxX = mouseEvent.getX();
          _origBoxY = mouseEvent.getY();

          _boxX = _origBoxX;
          _boxY = _origBoxY;
          _prevRect.setRect(_origBoxX, _origBoxY, _boxX, _boxY);
          
        }
        else
        {
          // figure out the width and height of the box
          _boxWidth = mouseEvent.getX() - _origBoxX;
          _boxHeight = mouseEvent.getY() - _origBoxY;
       
          // keep x,y at the top left
          if (_boxWidth >= 0)
            _boxX = _origBoxX;
          else
          {
            _boxX = _origBoxX + _boxWidth;
            _boxWidth = -_boxWidth;
          }
          if (_boxHeight >= 0)
            _boxY = _origBoxY;
          else
          {
            _boxY = _origBoxY + _boxHeight;
            _boxHeight = -_boxHeight;
          }
        } 
        
         // figure out the largest area that needs to be redrawn
         Rectangle rect = new Rectangle(_boxX, _boxY, _boxWidth, _boxHeight);

         _currRect.setRect(rect.getBounds());

        // figure out the largest area that needs to be redrawn
        _currRect.setRect(_boxX, _boxY, _boxWidth, _boxHeight);
        _unionRect.setRect(_currRect);
        _unionRect.add(_prevRect);
        _unionRect.setRect(_unionRect.x, _unionRect.y, _unionRect.width + 20, _unionRect.height + 20);

        // repaint the union of the area of the current rectangle and the previous rectangle
        repaint(_unionRect);

        // reset to the current box size
        _prevRect.setRect(_currRect);
     
      }
      
    }
  }

  /**
   * @author Bill Darbie
   */
  private void handleMouseReleased(MouseEvent mouseEvent)
  {
    int modifiers = mouseEvent.getModifiers();
    int multipleSelectMask = InputEvent.CTRL_DOWN_MASK;
    
    // handle right-click first
    if ((modifiers & MouseEvent.BUTTON3_MASK) == MouseEvent.BUTTON3_MASK)
    {
      _mouseRightClickMode = true;
      
      if (_currentMode == DRAG_OPTICAL_REGION_MODE)
      {
         if (_groupSelectionInProgress)
        {     
              _graphicsEngine.setUnselectedRectangleInPixels(_boxX, _boxY, _boxWidth, _boxHeight);
               
              if ((mouseEvent.getModifiersEx() & multipleSelectMask) == multipleSelectMask)
              {
                 _graphicsEngine.setControlKeyPressMode(true);
                 _graphicsEngine.selectDragUnselectOpticalRegionRenderersInRectangle(_boxX, _boxY, _boxWidth, _boxHeight, _ctrlClick);               
              }
              else
              {
                 _graphicsEngine.setControlKeyPressMode(false);
              }
           
              _boxX = 0;
              _boxY = 0;
              _boxWidth = 0;
              _boxHeight = 0;
            //  }
                      
             // _ctrlClick = false; // reset flag until next time

              // reset the x and y coordinates of the box
              _origBoxX = 0;
              _origBoxY = 0;
          }
        else
        {
          // select just where the mouse was
          _origBoxX = 0;
          _origBoxY = 0;
     
         // _graphicsEngine.mouseClicked(mouseEvent);        
        }     
        _groupSelectionInProgress =false;
      }
      else if (_currentMode == DRAG_SELECTED_OPTICAL_REGION_MODE)
      {
       // if (_groupSelectionInProgress)
       // {     
            //  _graphicsEngine.selectDragOpticalRegionRenderersInRectangle(_boxX, _boxY, _boxWidth, _boxHeight, false);
              _graphicsEngine.setUnselectedRectangleInPixels(_boxX, _boxY, _boxWidth, _boxHeight);
               
              if ((mouseEvent.getModifiersEx() & multipleSelectMask) == multipleSelectMask)
              {
                 _graphicsEngine.setControlKeyPressMode(true);
                 _graphicsEngine.selectDragUnselectOpticalRegionRenderersInRectangle(_boxX, _boxY, _boxWidth, _boxHeight, _ctrlClick);               
              }
              else
              {
                 _graphicsEngine.setControlKeyPressMode(false);
              }
           
              _boxX = 0;
              _boxY = 0;
              _boxWidth = 0;
              _boxHeight = 0;
            //  }
                      
          //    _ctrlClick = false; // reset flag until next time

              // reset the x and y coordinates of the box
              _origBoxX = 0;
              _origBoxY = 0;
      //  }
       // else
       // {
          // select just where the mouse was
       //   _origBoxX = 0;
        //  _origBoxY = 0;
     
          _graphicsEngine.mouseClicked(mouseEvent);        
      //  }     
        _groupSelectionInProgress =false;
      }
      else
      {
          _graphicsEngine.mouseClicked(mouseEvent);

          // if the right click is released, clear out any rect variables that might have been initialized.
          _origBoxX = 0;
          _origBoxY = 0;
          _boxX = 0;
          _boxY = 0;
          _boxWidth = 0;
          _boxHeight = 0;
          _prevRect.setRect(0, 0, 0, 0);
      }
    }
    if ((modifiers & MouseEvent.BUTTON1_MASK) == MouseEvent.BUTTON1_MASK)
    {
      _mouseRightClickMode = false;
      if (_currentMode == GROUP_SELECT_MODE)
      {
        if (_groupSelectionInProgress)
        {
          // group select mode

          if ((_boxWidth <= 3) && (_boxHeight <= 3))
          {
            // treat this little selection box as a single click
            _graphicsEngine.mouseClicked(mouseEvent);
          }
          else
          {
            _graphicsEngine.selectVisibleRenderersInRectangle(_boxX, _boxY, _boxWidth, _boxHeight, _ctrlClick);
          }
          _ctrlClick = false; // reset flag until next time

          // reset the x and y coordinates of the box
          _origBoxX = 0;
          _origBoxY = 0;
          _boxX = 0;
          _boxY = 0;
          _boxWidth = 0;
          _boxHeight = 0;
          _prevRect.setRect(0, 0, 0, 0);

          // repaint everything
          repaint();
        }
        else
        {
          // select just where the mouse was
          _origBoxX = 0;
          _origBoxY = 0;
          _graphicsEngine.mouseClicked(mouseEvent);
        }
        _groupSelectionInProgress =false;
      }
      else if (_currentMode == ZOOM_RECTANGLE_MODE || _currentMode == SELECT_REGION_MODE)
      {
        // zoom box mode
        // translate so the center of the box is at the center of the screen
        if ((_boxWidth > 0) && (_boxHeight > 0))
        {
          Dimension dim = getSize();
          // only do a fitRectangleInPixelsToScreen() call if the box drawn is contained on the screen
          if ((_boxX >= 0) && (_boxY >= 0) &&
              (_boxX + _boxWidth <= dim.getWidth()) && (_boxY + _boxHeight <= dim.getHeight()))
          {
            if (_currentMode == ZOOM_RECTANGLE_MODE)
            {
              _graphicsEngine.fitRectangleInPixelsToScreen(_boxX, _boxY, _boxWidth, _boxHeight);
            }
            else if (_currentMode == SELECT_REGION_MODE)
            {

              Point2D startCoordInNanoMeters = _graphicsEngine.convertFromPixelToRendererCoordinates(_boxX, _boxY);
              Point2D endCoordInNanoMeters = _graphicsEngine.convertFromPixelToRendererCoordinates(_boxX + _boxWidth, _boxY + _boxHeight);
              
              if(com.axi.v810.business.virtualLive.VirtualLiveManager.getInstance().isVirtualLiveMode() == true)
              {
                if ((endCoordInNanoMeters.getX() - startCoordInNanoMeters.getX()) > _maxImageRegionWidth)
                {
                  endCoordInNanoMeters = new Point2D.Double(startCoordInNanoMeters.getX() + _maxImageRegionWidth, endCoordInNanoMeters.getY());
                }
                if ((endCoordInNanoMeters.getY() - startCoordInNanoMeters.getY()) * -1 > _maxImageRegionHeight)
                {
                  endCoordInNanoMeters = new Point2D.Double(endCoordInNanoMeters.getX(), startCoordInNanoMeters.getY() - _maxImageRegionHeight);
                }
              }
              java.awt.Shape shape = null;
              if(_currentDragRegionShape == DRAG_RECTANGLE_MODE)
                  shape = new Rectangle2D.Double(startCoordInNanoMeters.getX(), endCoordInNanoMeters.getY(), (endCoordInNanoMeters.getX() - startCoordInNanoMeters.getX()), ((endCoordInNanoMeters.getY() - startCoordInNanoMeters.getY()) * -1));
              else
                  shape = new java.awt.geom.Ellipse2D.Double(startCoordInNanoMeters.getX(), endCoordInNanoMeters.getY(), (endCoordInNanoMeters.getX() - startCoordInNanoMeters.getX()), ((endCoordInNanoMeters.getY() - startCoordInNanoMeters.getY()) * -1));

              _graphicsEngine.selectAreaInRectangle(shape);
            }
            else
              _graphicsEngine.fitRectangleInPixelsToScreen(_boxX, _boxY, _boxWidth, _boxHeight);
          }

          // reset the x and y coordinates of the box
          _origBoxX = 0;
          _origBoxY = 0;
          _boxX = 0;
          _boxY = 0;
          _boxWidth = 0;
          _boxHeight = 0;
          _prevRect.setRect(0, 0, 0, 0);

          // repaint everything
          repaint();
        }
      }
      else if (_currentMode == DRAG_MODE)
      {
        if (_graphicsEngine.isDragRegionModeAvailable())
        {  
          _graphicsEngine.setOpticalRegionFirstDrag(false);
          int x = _boxX;
          int y = _boxY;
          int width = _boxWidth;
          int height = _boxHeight;
          if (_graphicsEngine.getCalculatedSelectedComponentRegionWidth() > 0 && _graphicsEngine.getCalculatedSelectedComponentRegionHeight() > 0)
          {
            width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
            x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());

            height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
            y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());  
               
            Point2D graphicCoordInNanoMetersOri = _graphicsEngine.convertFromPixelToRendererCoordinates((int) width, (int) height);
            Point2D graphicCoordInNanoMetersMouse = _graphicsEngine.convertFromPixelToRendererCoordinates((int) 0, (int) 0);

            int widthInNano = (int) (graphicCoordInNanoMetersOri.getX() - graphicCoordInNanoMetersMouse.getX() );
            int heightInNano = (int) (graphicCoordInNanoMetersMouse.getY() - graphicCoordInNanoMetersOri.getY());

            _graphicsEngine.setRectangleInPixels(x, y, width, height);
            _graphicsEngine.setWidthHeightInNano(widthInNano, heightInNano);
             
                  if (x < 0 || y < 0)
                  {
                      _graphicsEngine.center(x, y, 0, 0);

                       width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
                      x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());

                      height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
                      y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());  
                  }
                  
                  _graphicsEngine.selectDragOpticalRegionRenderersInRectangle(x, y, width, height, false);
                
                  if ((mouseEvent.getModifiersEx() & multipleSelectMask) == multipleSelectMask)
                  {
                    _graphicsEngine.setControlKeyPressMode(true);
                    _graphicsEngine.selectDragUnselectOpticalRegionRenderersInRectangle(_boxX, _boxY, _boxWidth, _boxHeight, false);               
                  }

          }  
        }
        // drag mode
        // reset the original drag points now that the left mouse has been released
        _xMouseDragOrig = 0;
        _yMouseDragOrig = 0;
      }
      else if (_currentMode == QUICK_DRAG_MODE)
      {
        // drag mode
        // reset the original drag points now that the left mouse has been released
        _xMouseDragOrig = 0;
        _yMouseDragOrig = 0;
        _currentMode = GROUP_SELECT_MODE;
      }
      else if (_currentMode == MEASUREMENT_MODE)
      {
        _origLineX = 0;
        _origLineY = 0;
        _lineBeginX = 0;
        _lineBeginY = 0;
        _lineEndX = 0;
        _lineEndY = 0;
        _measurementDistance = 0.0;
      }
      else if (_currentMode == DRAG_OPTICAL_REGION_MODE)
      {
        if (_groupSelectionInProgress)
        {
          Point2D graphicCoordInNanoMetersOri = _graphicsEngine.convertFromPixelToRendererCoordinates((int) _boxWidth, (int) _boxHeight);

          Point2D graphicCoordInNanoMetersMouse = _graphicsEngine.convertFromPixelToRendererCoordinates((int) 0, (int) 0);
     
          int widthInNano = (int) (graphicCoordInNanoMetersOri.getX() - graphicCoordInNanoMetersMouse.getX() );
          int heightInNano = (int) (graphicCoordInNanoMetersMouse.getY() - graphicCoordInNanoMetersOri.getY());

          _graphicsEngine.setRectangleInPixels(_boxX, _boxY, _boxWidth, _boxHeight);
          _graphicsEngine.setWidthHeightInNano(widthInNano, heightInNano);
                  
                  _graphicsEngine.selectDragOpticalRegionRenderersInRectangle(_boxX, _boxY, _boxWidth, _boxHeight, false);
                
            //  }
              
          //    _ctrlClick = false; // reset flag until next time

          // reset the x and y coordinates of the box
          _origBoxX = 0;
          _origBoxY = 0;
        }
        else
        {
          // select just where the mouse was
          _origBoxX = 0;
          _origBoxY = 0;
     
          //_graphicsEngine.mouseClicked(mouseEvent);        
        }
        _groupSelectionInProgress =false;
        
        _boxX = 0;
        _boxY = 0;
        _boxWidth = 0;
        _boxHeight = 0;
        this.repaint();
      }
      else if (_currentMode == DRAG_SELECTED_OPTICAL_REGION_MODE)
      {
         if (_groupSelectionInProgress)
        {     
              _graphicsEngine.setUnselectedRectangleInPixels(_boxX, _boxY, _boxWidth, _boxHeight);
               
              if ((mouseEvent.getModifiersEx() & multipleSelectMask) == multipleSelectMask)
              {
                 _graphicsEngine.setControlKeyPressMode(true);             
              }
              else
              {
                 _graphicsEngine.setControlKeyPressMode(false);
              }
              _graphicsEngine.selectDragUnselectOpticalRegionRenderersInRectangle(_boxX, _boxY, _boxWidth, _boxHeight, _ctrlClick);  
           
              _boxX = 0;
              _boxY = 0;
              _boxWidth = 0;
              _boxHeight = 0;
            //  }
                      
           //   _ctrlClick = false; // reset flag until next time

              // reset the x and y coordinates of the box
              _origBoxX = 0;
              _origBoxY = 0;
        }
        else
        {
          // select just where the mouse was
          _origBoxX = 0;
          _origBoxY = 0;
          _graphicsEngine.mouseClicked(mouseEvent); // uncommented - Ying-Huan.Chu [11th September 2013]
        }     
        _groupSelectionInProgress =false;
      }
      else if (_currentMode == MOUSE_CLICK_MODE)
      {
        _graphicsEngine.mouseClicked(mouseEvent);
      }
      else
        Assert.expect(false, "Mode not recognized " + _currentMode);
//      _currentMode = GROUP_SELECT_MODE;
    }
    _graphicsEngine.setMouseRightClickMode(false);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleMouseWheelMoved(MouseWheelEvent mouseWheelEvent)
  {
    int notches = mouseWheelEvent.getWheelRotation();
    double zoomFactor;
    if (notches > 0)  // zoom IN
      zoomFactor = 1.2;
    else
      zoomFactor = 1.0/1.2;

      _graphicsEngine.zoom(zoomFactor);
      
    if (_graphicsEngine.isDragRegionModeAvailable())
    {
        int x = _boxX;
        int y = _boxY;
        int width = _boxWidth;
        int height = _boxHeight;
         _graphicsEngine.setOpticalRegionFirstDrag(false);
       
       if (_graphicsEngine.getCalculatedSelectedComponentRegionWidth() > 0 && _graphicsEngine.getCalculatedSelectedComponentRegionHeight() > 0)
       {           
              width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
              x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());

              height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
              y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());  
              
              Point2D graphicCoordInNanoMetersOri = _graphicsEngine.convertFromPixelToRendererCoordinates((int) width, (int) height);

              Point2D graphicCoordInNanoMetersMouse = _graphicsEngine.convertFromPixelToRendererCoordinates((int) 0, (int) 0);
     
              int widthInNano = (int) (graphicCoordInNanoMetersOri.getX() - graphicCoordInNanoMetersMouse.getX() );
              int heightInNano = (int) (graphicCoordInNanoMetersMouse.getY() - graphicCoordInNanoMetersOri.getY());
          
              _graphicsEngine.setRectangleInPixels(x, y, width, height);
              _graphicsEngine.setWidthHeightInNano(widthInNano, heightInNano);
              
              if (x < 0 || y < 0)
              {
                  _graphicsEngine.center(x, y, 0, 0);
                  
                //  _graphicsEngine.scrollRectInRendererCoordinatesToVisible(new Rectangle(x,y,0,0));
                  width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
                  x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());

                  height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
                  y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());  
              }
               
          //    if(_setSelectedVisibleRenderersCount < 1)
               _graphicsEngine.selectDragOpticalRegionRenderersInRectangle(x, y, width, height, false);
              
         //     if(_setSelectedVisibleRenderersCount < 2)
         //         _setSelectedVisibleRenderersCount++;
                         
        }  
//        else if (_graphicsEngine.getSurfaceMapAlignmentRegion().size() > 0)
//        {
//            _graphicsEngine.clearAllSurfaceMapAlignmentRegionInPixel();
//            for (Rectangle2D rect : _graphicsEngine.getSurfaceMapAlignmentRegion())
//            {
//                  _graphicsEngine.resetSelectedRegionCoordinate();
//                  _graphicsEngine.setSelectedComponentRegion(rect);
//                  width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
//                  x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
//
//                  height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
//                  y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());  
//
//                  Point2D graphicCoordInNanoMetersOri = _graphicsEngine.convertFromPixelToRendererCoordinates((int) width, (int) height);
//
//                  Point2D graphicCoordInNanoMetersMouse = _graphicsEngine.convertFromPixelToRendererCoordinates((int) 0, (int) 0);
//
//                  int widthInNano = (int) (graphicCoordInNanoMetersOri.getX() - graphicCoordInNanoMetersMouse.getX() );
//                  int heightInNano = (int) (graphicCoordInNanoMetersMouse.getY() - graphicCoordInNanoMetersOri.getY());
//
//                  _graphicsEngine.setRectangleInPixels(x, y, width, height);
//                  _graphicsEngine.setWidthHeightInNano(widthInNano, heightInNano);
//
//                  if (x < 0 || y < 0)
//                  {
//                      _graphicsEngine.center(x, y, 0, 0);
//
//                    //  _graphicsEngine.scrollRectInRendererCoordinatesToVisible(new Rectangle(x,y,0,0));
//                      width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
//                      x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
//
//                      height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
//                      y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());  
//                  }
//
//              //    if(_setSelectedVisibleRenderersCount < 1)
//                   _graphicsEngine.selectDragOpticalRegionRenderersInRectangle(x, y, width, height, false);
//                   
//                   _graphicsEngine.addSurfaceMapAlignmentRegionInPixel(new Rectangle(x, y, width, height));
//
//                   _graphicsEngine.selectDragUnselectOpticalRegionRenderersInRectangle(x, y, width, height, false);
//            }
//            _graphicsEngine.clearAllSurfaceMapAlignmentRegion();
//            _graphicsEngine.resetSelectedRegionCoordinate();
//        }
    }
  }

  /**
   * @author Bill Darbie
   */
  protected void paintComponent(Graphics graphics)
  {
   if (_mouseRightClickMode == false)
   {
    if (_currentMode == GROUP_SELECT_MODE) // group select mode -- use a dotted line rectangle
    {
      // paint the box the user creates by dragging with the left mouse
      if((_boxWidth != 0) || (_boxHeight != 0))
      {
        Graphics2D graphics2D = (Graphics2D)graphics;
        Color color = graphics2D.getColor();
        graphics2D.setColor(getForeground());

        graphics2D.setStroke(_dashedStroke);
        graphics2D.drawRect(_boxX, _boxY, _boxWidth, _boxHeight);
        graphics2D.setColor(color);
      }
    }
    else if (_currentMode == ZOOM_RECTANGLE_MODE || _currentMode == SELECT_REGION_MODE)
    {
      // paint the box the user creates by dragging with the left mouse
      if((_boxWidth != 0) || (_boxHeight != 0))
      {
        Color color = graphics.getColor();
        graphics.setColor(getForeground());
        // rect / circular shape drawn for the land pattern
        if(_currentDragRegionShape == DRAG_RECTANGLE_MODE)
            graphics.drawRect(_boxX, _boxY, _boxWidth, _boxHeight);
        else
            graphics.drawOval(_boxX, _boxY, _boxWidth, _boxHeight);
        graphics.setColor(color);
      }
    }
    else if (_currentMode == MEASUREMENT_MODE)
    {
      if(_measurementDistance != 0.0)
      {
        int x1 = _lineBeginX;
        int x2 = _lineEndX;
        int y1 = _lineBeginY;
        int y2 = _lineEndY;
        boolean isWidthLargerThanHeight = Math.abs(x2 - x1) > Math.abs(y2 - y1);
        int xInPixels = (int)_currRect.getCenterX();
        int yInPixels = (int)_currRect.getCenterY();
        if (isWidthLargerThanHeight)
        {
          yInPixels -= 10;

          // draw a stright line in x
          if (_ctrlClick)
          {
            y2 = y1;
            yInPixels = y2 - 10;
          }
        }
        else
        {
          xInPixels += 10;
          // draw a stright line in y
          if (_ctrlClick)
          {
            x2 = x1;
            xInPixels = x2 + 10;
          }
        }

        Graphics2D graphics2d = (Graphics2D)graphics;

        Color origColor = graphics2d.getColor();
        Stroke origStroke = graphics2d.getStroke();

          graphics2d.setStroke(_measurementStroke);

          graphics2d.setColor(Color.yellow);

          graphics2d.drawLine(x1, y1, x2, y2);

          NumberFormat formatter = NumberFormat.getNumberInstance();
          formatter.setMaximumFractionDigits(_graphicsEngine.getCurrentMeasurementUnitsDecimalPlaces());
          String measurementString = formatter.format(_measurementDistance) + " " +
                                   _graphicsEngine.getCurrentMeasurementUnitsString();
          FontMetrics fontMetrics = getFontMetrics(getFont());
          int widthInPixels = fontMetrics.stringWidth(measurementString) + 10;
          int heightInPixels = fontMetrics.getMaxAscent() + fontMetrics.getMaxDescent();

          yInPixels -= heightInPixels;

          // ok, now check to see if the box is visible on screen, if not, adjust it's coordinate accordingly
          if(xInPixels < 0)
            xInPixels = 0;
          if(yInPixels < 0)
            yInPixels = 0;

          int diff = xInPixels + widthInPixels - _graphicsEngine.getWidth();
          if(diff > 0)
          {
            xInPixels -= diff;
//          stringX -= diff;
          }

          diff = yInPixels + heightInPixels - _graphicsEngine.getHeight();
          if(diff > 0)
          {
            yInPixels -= diff;
//          stringY -= diff;
          }

          Rectangle rect = new Rectangle(xInPixels, yInPixels, widthInPixels, heightInPixels);

          graphics2d.setColor(SystemColor.info);
          graphics2d.fill(rect);
          graphics2d.setColor(Color.black);
          graphics2d.drawString(measurementString, xInPixels + 5, yInPixels + fontMetrics.getMaxAscent());
          graphics2d.setColor(origColor);
          graphics2d.setStroke(origStroke);
          _currRect.add(rect);
        }
      }
      else if (_currentMode == DRAG_MODE)
      {
        if (_graphicsEngine.isDragRegionModeAvailable())
        {
            int x = _boxX;
            int y = _boxY;
            int width = _boxWidth;
            int height = _boxHeight;

            Graphics2D graphics2D = (Graphics2D) graphics;

            graphics2D.setColor(getForeground());

            graphics2D.setStroke(_measurementStroke);

            graphics2D.setColor(Color.RED);

            if (_graphicsEngine.getCalculatedSelectedComponentRegionWidth() > 0 && _graphicsEngine.getCalculatedSelectedComponentRegionHeight() > 0)
            {  
               //   if (MathUtil.fuzzyEquals(_graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale(), 1.0) == false)
              //    {
                //      width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + MathUtil.roundDoubleToInt(20 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
                //      x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());

                //      height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + MathUtil.roundDoubleToInt(20 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
                //      y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
              //    }
             //     else if ((MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0) == false))
              //    {
                //     width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + MathUtil.roundDoubleToInt(20 * _graphicsEngine.getCurrentZoomFactor());
                //     x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - MathUtil.roundDoubleToInt(10 * _graphicsEngine.getCurrentZoomFactor());

             //        height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + MathUtil.roundDoubleToInt(20 * _graphicsEngine.getCurrentZoomFactor());
             //        y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - MathUtil.roundDoubleToInt(10 * _graphicsEngine.getCurrentZoomFactor());  

               //   }
               //   else
               //   {
                    width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
                  x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());

                  height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
                  y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());  

               //   }

                  Point2D graphicCoordInNanoMetersOri = _graphicsEngine.convertFromPixelToRendererCoordinates((int) width, (int) height);

                  Point2D graphicCoordInNanoMetersMouse = _graphicsEngine.convertFromPixelToRendererCoordinates((int) 0, (int) 0);

                  int widthInNano = (int) (graphicCoordInNanoMetersOri.getX() - graphicCoordInNanoMetersMouse.getX() );
                  int heightInNano = (int) (graphicCoordInNanoMetersMouse.getY() - graphicCoordInNanoMetersOri.getY());

                  _graphicsEngine.setRectangleInPixels(x, y, width, height);
                  _graphicsEngine.setWidthHeightInNano(widthInNano, heightInNano);

                  if(_setSelectedVisibleRenderersCount < 1)
                      _graphicsEngine.selectDragOpticalRegionRenderersInRectangle(x, y, width, height, false);

                  if(_setSelectedVisibleRenderersCount < 2)
                      _setSelectedVisibleRenderersCount++;

                  _boxX = 0;
                  _boxY = 0;
                  _boxWidth = 0;
                  _boxHeight = 0;
             }
             else  
             {
                  graphics2D.drawRect(x, y, width, height);         
             }

            if (_graphicsEngine.isSelectedRegionInComboboxAvailable() && _graphicsEngine.isShowRegionCheckBoxIsChecked())
            {
                 _graphicsEngine.drawRegionCrossHairs();
            }  
            else
            {
                 graphics2D.drawRect((int)0, (int)0, (int)0, (int)0);

            }
        }    
    }
     else if (_currentMode == DRAG_OPTICAL_REGION_MODE)
    {         
        int x = _boxX;
        int y = _boxY;
        int width = _boxWidth;
        int height = _boxHeight;
       
        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics2D.setColor(getForeground());
        graphics2D.setStroke(_measurementStroke);
        graphics2D.setColor(Color.RED);

        if (_graphicsEngine.getCalculatedSelectedComponentRegionWidth() > 0 && _graphicsEngine.getCalculatedSelectedComponentRegionHeight() > 0)
        {  
           //   if (MathUtil.fuzzyEquals(_graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale(), 1.0) == false)
           //   {
             //     width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + MathUtil.roundDoubleToInt(20 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
             //     x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());

             //     height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + MathUtil.roundDoubleToInt(20 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
             //     y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
          //    }
            //  else if ((MathUtil.fuzzyEquals(_graphicsEngine.getCurrentZoomFactor(), 1.0) == false))
             // {
            //     width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + MathUtil.roundDoubleToInt(20 * _graphicsEngine.getCurrentZoomFactor());
            ///     x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - MathUtil.roundDoubleToInt(10 * _graphicsEngine.getCurrentZoomFactor());

            //     height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + MathUtil.roundDoubleToInt(20 * _graphicsEngine.getCurrentZoomFactor());
            //     y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - MathUtil.roundDoubleToInt(10 * _graphicsEngine.getCurrentZoomFactor());  
                
         //     }
            //  else
            //  {
//                width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
//                  x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
//
//                  height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
//                  y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());  
                 
             // }
              width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth();
              x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX();

              height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight();
              y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY();  
                             
              Point2D graphicCoordInNanoMetersOri = _graphicsEngine.convertFromPixelToRendererCoordinates((int) width, (int) height);

              Point2D graphicCoordInNanoMetersMouse = _graphicsEngine.convertFromPixelToRendererCoordinates((int) 0, (int) 0);
     
              int widthInNano = (int) (graphicCoordInNanoMetersOri.getX() - graphicCoordInNanoMetersMouse.getX() );
              int heightInNano = (int) (graphicCoordInNanoMetersMouse.getY() - graphicCoordInNanoMetersOri.getY());
             
              _graphicsEngine.setRectangleInPixels(x, y, width, height);
              _graphicsEngine.setWidthHeightInNano(widthInNano, heightInNano);
            
              if(_setSelectedVisibleRenderersCount < 1)
                  _graphicsEngine.selectDragOpticalRegionRenderersInRectangle(x, y, width, height, false);
              
              if(_setSelectedVisibleRenderersCount < 2)
                  _setSelectedVisibleRenderersCount++;
              
              _boxX = 0;
              _boxY = 0;
              _boxWidth = 0;
              _boxHeight = 0;
         }
         else  
         {
             graphics2D.drawRect(x, y, width, height);    
         }
        
        if (_graphicsEngine.isSelectedRegionInComboboxAvailable() && _graphicsEngine.isShowRegionCheckBoxIsChecked())
        {
          _graphicsEngine.drawRegionCrossHairs();
        }  
        else
        {
          graphics2D.drawRect((int)0, (int)0, (int)0, (int)0);         
        }
     }
     else if (_currentMode == DRAG_SELECTED_OPTICAL_REGION_MODE && _graphicsEngine.isDragSelectedRegionMode())
     {
        int x = _boxX;
        int y = _boxY;
        int width = _boxWidth;
        int height = _boxHeight;
        
        Graphics2D graphics2D = (Graphics2D) graphics;

        graphics2D.setColor(Color.GRAY.brighter());

        graphics2D.setStroke(_dashedStroke);
      
        graphics2D.drawRect(x, y, width, height);
        
         if (_graphicsEngine.getCalculatedSelectedComponentRegionWidth() > 0 && _graphicsEngine.getCalculatedSelectedComponentRegionHeight() > 0)
        {
//               width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + 10;
//                      x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - 5;
//
//                      height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + 10;
//                      y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - 5;
            
             width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
                  x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());

                  height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
                  y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());  
              
              Point2D graphicCoordInNanoMetersOri = _graphicsEngine.convertFromPixelToRendererCoordinates((int) width, (int) height);

              Point2D graphicCoordInNanoMetersMouse = _graphicsEngine.convertFromPixelToRendererCoordinates((int) 0, (int) 0);
     
              int widthInNano = (int) (graphicCoordInNanoMetersOri.getX() - graphicCoordInNanoMetersMouse.getX() );
              int heightInNano = (int) (graphicCoordInNanoMetersMouse.getY() - graphicCoordInNanoMetersOri.getY());
              
              _graphicsEngine.setRectangleInPixels(x, y, width, height);
              _graphicsEngine.setWidthHeightInNano(widthInNano, heightInNano);
        }
     }
   }
   else if (_mouseRightClickMode == true)
   {
    if (_currentMode == DRAG_OPTICAL_REGION_MODE && _ctrlClick)
    {         
        int x = _boxX;
        int y = _boxY;
        int width = _boxWidth;
        int height = _boxHeight;
        
        Graphics2D graphics2D = (Graphics2D) graphics;

        graphics2D.setColor(Color.GRAY.brighter());

        graphics2D.setStroke(_dashedStroke);
      
        graphics2D.drawRect(x, y, width, height);
        
        if (_graphicsEngine.getCalculatedSelectedComponentRegionWidth() > 0 && _graphicsEngine.getCalculatedSelectedComponentRegionHeight() > 0)
        {
//               width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + 10;
//                      x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - 5;
//
//                      height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + 10;
//                      y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - 5;
            
              width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
              x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());

              height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
              y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());  
              
              Point2D graphicCoordInNanoMetersOri = _graphicsEngine.convertFromPixelToRendererCoordinates((int) width, (int) height);

              Point2D graphicCoordInNanoMetersMouse = _graphicsEngine.convertFromPixelToRendererCoordinates((int) 0, (int) 0);
     
              int widthInNano = (int) (graphicCoordInNanoMetersOri.getX() - graphicCoordInNanoMetersMouse.getX() );
              int heightInNano = (int) (graphicCoordInNanoMetersMouse.getY() - graphicCoordInNanoMetersOri.getY());
              
              _graphicsEngine.setRectangleInPixels(x, y, width, height);
              _graphicsEngine.setWidthHeightInNano(widthInNano, heightInNano);
        }
     }  
     else if (_currentMode == DRAG_SELECTED_OPTICAL_REGION_MODE && _graphicsEngine.isDragSelectedRegionMode() && _ctrlClick)
     {
        int x = _boxX;
        int y = _boxY;
        int width = _boxWidth;
        int height = _boxHeight;
        
        Graphics2D graphics2D = (Graphics2D) graphics;

        graphics2D.setColor(Color.GRAY.brighter());

        graphics2D.setStroke(_dashedStroke);
      
        graphics2D.drawRect(x, y, width, height);
        
         if (_graphicsEngine.getCalculatedSelectedComponentRegionWidth() > 0 && _graphicsEngine.getCalculatedSelectedComponentRegionHeight() > 0)
        {
//               width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + 10;
//                      x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - 5;
//
//                      height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + 10;
//                      y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - 5;
            
             width = _graphicsEngine.getCalculatedSelectedComponentRegionWidth() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
                  x = (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getX() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());

                  height = _graphicsEngine.getCalculatedSelectedComponentRegionHeight() + MathUtil.roundDoubleToInt(10 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());
                  y =  (int)_graphicsEngine.getComponentRegionUpperLeftCoordinate().getY() - MathUtil.roundDoubleToInt(5 * _graphicsEngine.getFitGraphicsToScreenWhenWindowIsResizedNewScale());  
              
              Point2D graphicCoordInNanoMetersOri = _graphicsEngine.convertFromPixelToRendererCoordinates((int) width, (int) height);

              Point2D graphicCoordInNanoMetersMouse = _graphicsEngine.convertFromPixelToRendererCoordinates((int) 0, (int) 0);
     
              int widthInNano = (int) (graphicCoordInNanoMetersOri.getX() - graphicCoordInNanoMetersMouse.getX() );
              int heightInNano = (int) (graphicCoordInNanoMetersMouse.getY() - graphicCoordInNanoMetersOri.getY());
              
              _graphicsEngine.setRectangleInPixels(x, y, width, height);
              _graphicsEngine.setWidthHeightInNano(widthInNano, heightInNano);
        }
     }
   }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setMaxImageRegionDimension(double imageRegionWidth, double imageRegionHeight)
  {
    _maxImageRegionWidth = imageRegionWidth;
    _maxImageRegionHeight = imageRegionHeight;
  }
}
