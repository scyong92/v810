package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.util.*;

/**
 * Creates a modal dialog to capture a user input using slider
 * @author wei-chin.chong
 */
public class SliderInputDialog extends EscapeDialog
{
  // parent
  private JFrame _frame;
  // either OK or CANCEL depending on how the user exits
  private int _returnValue;

  private String _instruction;
  private String _okButtonText;
  private String _cancelButtonText;

  private Font _buttonFont;
  private Font _messageFont;

  private JPanel _mainPanel = new JPanel();
  private JPanel _messagePanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _innerButtonPanel = new JPanel();

  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private FlowLayout _messagePanelFlowLayout = new FlowLayout();
  private FlowLayout _buttonPanelFlowLayout = new FlowLayout();
  private GridLayout _innerButtonPanelGridLayout = new GridLayout();
  private JLabel _messageLabel = new JLabel();
  private JPanel _inputPanel = new JPanel();
  private GridLayout _inputPanelBorderLayout = null;
  private Border _innerButtonPanelBorder = BorderFactory.createEmptyBorder(5, 10, 0, 10);
  private Border _mainPanelBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);

  private JSlider _slider1 = new JSlider();
  private JSlider _slider2 = new JSlider();
  private JSlider _slider3 = new JSlider();
  private JLabel _slider1Label = new JLabel();
  private JLabel _slider2Label = new JLabel();
  private JLabel _slider3Label = new JLabel();
  private String _slider1LabelText;
  private String _slider2LabelText;
  private String _slider3LabelText;
  private int _slider1Maximum;
  private int _slider2Maximum;
  private int _slider3Maximum;
  private int _slider1Minimum;
  private int _slider2Minimum;
  private int _slider3Minimum;

  private JButton _okButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }

  };

  private JButton _cancelButton = new JButton()
  {
    protected void processKeyEvent(KeyEvent e)
    {
      super.processKeyEvent(e);
      if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      {
        cancelAction();
      }
    }
  };

  /**
   * @author wei-chin.chong
   */
  private SliderInputDialog()
  {
    // do nothing
  }

  /**
   * Slider Dialog with 1 slider
   * @author wei-chin.chong
   */
  public SliderInputDialog(JFrame frame,
                          String title,
                          String message,
                          String okButtonText,
                          String cancelButtonText,
                          String slider1Text,
                          int slider1Min,
                          int slider1Max)
  {
    super(frame, title, true);

    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(message != null);
    Assert.expect(okButtonText != null);
    Assert.expect(cancelButtonText != null);
    Assert.expect(slider1Text != null);

    _frame = frame;
    _instruction = message;
    _okButtonText = okButtonText;
    _cancelButtonText = cancelButtonText;
    _buttonFont = FontUtil.getDialogButtonFont();
    _messageFont = FontUtil.getDialogMessageFont();
    _okButton.setEnabled(slider1Text.length() > 0);

    _slider1LabelText = slider1Text;
    _slider2LabelText = null;
    _slider3LabelText = null;

    _slider1Minimum = slider1Min;
    _slider1Maximum = slider1Max;

    _inputPanelBorderLayout = new GridLayout(2, 0, 2, 2);
    createDialog();

    pack();
  }

  /**
   * Slider Dialog with 2 sliders
   * @author wei-chin.chong
   */
  public SliderInputDialog(JFrame frame,
                          String title,
                          String message,
                          String okButtonText,
                          String cancelButtonText,
                          String slider1Text,
                          int slider1Min,
                          int slider1Max,
                          String slider2Text,
                          int slider2Min,
                          int slider2Max)
  {
    super(frame, title, true);

    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(message != null);
    Assert.expect(okButtonText != null);
    Assert.expect(cancelButtonText != null);
    Assert.expect(slider1Text != null);
    Assert.expect(slider2Text != null);

    _frame = frame;
    _instruction = message;
    _okButtonText = okButtonText;
    _cancelButtonText = cancelButtonText;
    _buttonFont = FontUtil.getDialogButtonFont();
    _messageFont = FontUtil.getDialogMessageFont();
    _okButton.setEnabled(slider1Text.length() > 0 && slider2Text.length() > 0);

    _slider1LabelText = slider1Text;
    _slider2LabelText = slider2Text;
    _slider3LabelText = null;

    _slider1Minimum = slider1Min;
    _slider1Maximum = slider1Max;
    _slider2Minimum = slider2Min;
    _slider2Maximum = slider2Max;

    _inputPanelBorderLayout = new GridLayout(4, 0, 2, 2);
    createDialog();

    pack();
  }

  /**
   * Slider Dialog with 3 slider
   * @author wei-chin.chong
   */
  public SliderInputDialog(JFrame frame,
                          String title,
                          String message,
                          String okButtonText,
                          String cancelButtonText,
                          String slider1Text,
                          int slider1Min,
                          int slider1Max,
                          String slider2Text,
                          int slider2Min,
                          int slider2Max,
                          String slider3Text,
                          int slider3Min,
                          int slider3Max)
  {
    super(frame, title, true);

    Assert.expect(frame != null);
    Assert.expect(title != null);
    Assert.expect(message != null);
    Assert.expect(okButtonText != null);
    Assert.expect(cancelButtonText != null);
    Assert.expect(slider1Text != null);
    Assert.expect(slider2Text != null);
    Assert.expect(slider3Text != null);

    _frame = frame;
    _instruction = message;
    _okButtonText = okButtonText;
    _cancelButtonText = cancelButtonText;
    _buttonFont = FontUtil.getDialogButtonFont();
    _messageFont = FontUtil.getDialogMessageFont();
    _okButton.setEnabled(slider1Text.length() > 0);

    _slider1LabelText = slider1Text;
    _slider2LabelText = slider2Text;
    _slider3LabelText = slider3Text;

    _slider1Minimum = slider1Min;
    _slider1Maximum = slider1Max;
    _slider2Minimum = slider2Min;
    _slider2Maximum = slider2Max;
    _slider3Minimum = slider3Min;
    _slider3Maximum = slider3Max;

    _inputPanelBorderLayout = new GridLayout(6, 0, 2, 2);
    createDialog();

    pack();
  }

  /**
   * @author wei-chin.chong
   */
  private void createDialog()
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _cancelButton.setText(_cancelButtonText);
    _innerButtonPanelGridLayout.setHgap(5);
    _messagePanel.setLayout(_messagePanelFlowLayout);
    _messagePanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _messagePanelFlowLayout.setHgap(0);
    _messagePanelFlowLayout.setVgap(0);
    _cancelButton.setFont(_buttonFont);
    _okButton.setFont(_buttonFont);
    _cancelButton.setMargin(new Insets(2, 8, 2, 8));
    _okButton.setMargin(new Insets(2, 8, 2, 8));
    _messageLabel.setFont(_messageFont);
    _messageLabel.setText(_instruction);
    _inputPanel.setLayout(_inputPanelBorderLayout);
    _buttonPanelFlowLayout.setHgap(0);
    _buttonPanelFlowLayout.setVgap(0);
    _innerButtonPanel.setBorder(_innerButtonPanelBorder);
    _mainPanel.setBorder(_mainPanelBorder);
    _mainPanelBorderLayout.setVgap(5);

    getContentPane().add(_mainPanel);
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    _buttonPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _innerButtonPanel.setLayout(_innerButtonPanelGridLayout);
    _okButton.setText(_okButtonText);
    _innerButtonPanel.add(_okButton);
    _innerButtonPanel.add(_cancelButton);
    _messagePanel.add(_messageLabel);
    _buttonPanel.add(_innerButtonPanel);
    _mainPanel.add(_messagePanel, BorderLayout.NORTH);
    _mainPanel.add(_inputPanel, BorderLayout.CENTER);
    _mainPanel.add(_buttonPanel, BorderLayout.SOUTH);

    resetValues();

    if(_slider1LabelText != null && _slider1LabelText.length() > 0)
    {
      _slider1Label.setText(_slider1LabelText);
      _inputPanel.add(_slider1Label);
      _inputPanel.add(_slider1);
    }

    if(_slider2LabelText != null && _slider2LabelText.length() > 0)
    {
      _slider2Label.setText(_slider2LabelText);
      _inputPanel.add(_slider2Label);
      _inputPanel.add(_slider2);
    }

    if(_slider3LabelText != null && _slider3LabelText.length() > 0)
    {
      _slider3Label.setText(_slider3LabelText);
      _inputPanel.add(_slider3Label);
      _inputPanel.add(_slider3);
    }

    getRootPane().setDefaultButton(_okButton);
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okAction();
      }
    });
  }
  /**
   * @return int
   * @author Chong Wei Chin
   */
  public int showDialog()
  {
    SwingUtils.centerOnComponent(this, _frame);
    setVisible(true);
    return _returnValue;
  }

  /**
   * @return
   * @author Chong Wei Chin
   */
  public int getSlider1Value()
  {
    return _slider1.getValue();
  }

  /**
   * @return
   * @author Chong Wei Chin
   */
  public int getSlider2Value()
  {
    return _slider2.getValue();
  }

  /**
   * @return
   * @author Chong Wei Chin
   */
  public int getSlider3Value()
  {
    return _slider3.getValue();
  }

  /**
   * @param value
   * @author Chong Wei Chin
   */
  public void setSlider1Value(int value)
  {
    Assert.expect(_slider1 != null);
    _slider1.setValue(value);
  }

  /**
   * @param value
   * @author Chong Wei Chin
   */
  public void setSlider2Value(int value)
  {
    Assert.expect(_slider2 != null);
    _slider2.setValue(value);
  }

  /**
   * @param value
   * @author Chong Wei Chin
   */
  public void setSlider3Value(int value)
  {
    Assert.expect(_slider3 != null);
    _slider3.setValue(value);
  }

  /**
   * @param font Font
   * @return int
   * @author Chong Wei Chin
   */
  public int showDialog(Font font)
  {
    Assert.expect(font != null);
    SwingUtils.setFont(this, font);
    pack();
    SwingUtils.centerOnComponent(this, _frame);

    setVisible(true);
    return _returnValue;
  }
  
  /**
   * @param e WindowEvent
   * @author Chong Wei Chin
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    int eventId = e.getID();
    if (eventId == WindowEvent.WINDOW_CLOSING)
    {
      cancelAction();
    }
  }

  /**
   * @param e WindowEvent
   * @author Chong Wei Chin
   */
  private void okAction()
  {
    _returnValue = JOptionPane.OK_OPTION;
    dispose();
  }

  /**
   * @param e WindowEvent
   * @author Chong Wei Chin
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    cancelAction();
  }

  /**
   * @author Chong Wei Chin
   */
  private void cancelAction()
  {
    _returnValue = JOptionPane.CANCEL_OPTION;
    resetValues();
    dispose();
  }

  /**
   * @author Chong Wei Chin
   */
  private void resetValues()
  {
    if(_slider1LabelText != null && _slider1LabelText.length() > 0)
    {      
      int range = _slider1Maximum - _slider1Minimum;
      _slider1.setMinorTickSpacing(range/20);
      _slider1.setMajorTickSpacing(range/5);
      _slider1.setMinimum(_slider1Minimum);
      _slider1.setMaximum(_slider1Maximum);
      _slider1.setValue((_slider1Minimum + _slider1Maximum) / 2);
      java.util.Hashtable table = new java.util.Hashtable();
      table.put(new Integer(_slider1.getMinimum()), new JLabel(Integer.toString(_slider1.getMinimum())));
      table.put(new Integer(_slider1.getMaximum()), new JLabel(Integer.toString(_slider1.getMaximum())));
      table.put(new Integer(0), new JLabel(Integer.toString(0)));
      // Force the slider to use the new labels
      _slider1.setLabelTable(table);
      _slider1.setPaintLabels(true);
      _slider1.setPaintTicks(true);
      _slider1.setPaintTrack(true);
      _slider1.setSnapToTicks(true);
    }
            
    if( _slider2LabelText != null && _slider2LabelText.length() > 0)
    {      
      int range3 = _slider2Maximum - _slider2Minimum;
      _slider2.setMinorTickSpacing(range3/20);
      _slider2.setMajorTickSpacing(range3/5);
      _slider2.setMinimum(_slider2Minimum);
      _slider2.setMaximum(_slider2Maximum);
      _slider2.setValue((_slider2Minimum + _slider2Maximum) / 2);
      java.util.Hashtable table = new java.util.Hashtable();
      table.put(new Integer(_slider2.getMinimum()), new JLabel(Integer.toString(_slider2.getMinimum())));
      table.put(new Integer(_slider2.getMaximum()), new JLabel(Integer.toString(_slider2.getMaximum())));
      table.put(new Integer(0), new JLabel(Integer.toString(0)));
      // Force the slider to use the new labels
      _slider2.setLabelTable(table);
      _slider2.setPaintLabels(true);
      _slider2.setPaintTicks(true);
      _slider2.setPaintTrack(true);
      _slider2.setSnapToTicks(true);
    }

    if( _slider3LabelText != null && _slider3LabelText.length() > 0)
    {
      int range3 = _slider3Maximum - _slider3Minimum;
      _slider3.setMinorTickSpacing(range3/20);
      _slider3.setMajorTickSpacing(range3/5);
      _slider3.setMinimum(_slider3Minimum);
      _slider3.setMaximum(_slider3Maximum);
      _slider3.setValue((_slider3Minimum + _slider3Maximum) / 2);
      java.util.Hashtable table = new java.util.Hashtable();
      table.put(new Integer(_slider3.getMinimum()), new JLabel(Integer.toString(_slider3.getMinimum())));
      table.put(new Integer(_slider3.getMaximum()), new JLabel(Integer.toString(_slider3.getMaximum())));
      table.put(new Integer(0), new JLabel(Integer.toString(0)));
      // Force the slider to use the new labels
      _slider3.setLabelTable(table);
      _slider3.setPaintLabels(true);
      _slider3.setPaintTicks(true);
      _slider3.setPaintTrack(true);
      _slider3.setSnapToTicks(true);
    }
  }
}
