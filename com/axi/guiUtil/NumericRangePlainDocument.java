package com.axi.guiUtil;

import java.text.*;
import javax.swing.text.*;
import com.axi.util.*;

/**
 * Title: NumericRangePlainDocument
 * Description: This class extends the NumericPlainDocument and adds range
 * checking to keep a text field value within a defined range.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      Agilent Technologies, Inc.
 * @author  Steve Anonson
 * @version 1.0
 * @see NumericPlainDocument
 */

public class NumericRangePlainDocument extends NumericPlainDocument
{
  Range _range = null;

  /**
   * Constructor
   */
  public NumericRangePlainDocument()
  {
    super(null);
  }

  /**
   * Constructor
   *
   * @param   format - DecimalFormat to use to verify contents.
   */
  public NumericRangePlainDocument(DecimalFormat format)
  {
    super(format);
  }

  /**
   * Constructor
   *
   * @param   content - initial value for the document.
   * @param   format - DecimalFormat to use to verify contents.
   */
  public NumericRangePlainDocument(AbstractDocument.Content content, DecimalFormat format)
  {
    super(content, format);
  }

  /**
   * This method overrides the Document method and validates the input.
   * @author Steve Anonson
   * @author Laura Cormos
   */
  public void insertString(int offset, String str, AttributeSet a) throws BadLocationException
  {
    if (str == null || str.length() == 0)
      return;

    Number num = super.validString(offset, str, a);
    if (num == null)
      return;

    if (_range != null)
    {
      // for any range minimum, a value entered that begins with the range minimum floor but is lower than the minimum
      // will not even be accepted, unless we specifically check for it
      // (for instance, if range minimum is 0.02540, and I want to enter 0.034, I can't even type 0.0 because when it
      // checks the first typed 'zero', it will fail the range check and will never even be accepted in the text field)
      double rangeMinValue = _range.getMinimumValue().doubleValue();
      double floor = Math.floor(rangeMinValue);

      // so don't do any range checking until the value of num is greater than the range minimum's floor
      if (rangeMinValue > floor && rangeMinValue < floor + 1 && MathUtil.fuzzyEquals(num.doubleValue(), floor))
      {
        super.insertString(offset, str, a); // keep the string and move on until we get a number greater than zero
        return;
      }
      // if the number is greater than the floor of the range minimum, check if in range
      else if (!_range.inRange(num))
        return;
    }
    // Finally, add to the model
    super.insertString(offset, str, a);
  }

  /**
   * Set the valid range for the value.
   */
  public void setRange( Range range )
  {
    _range = range;
  }

  /**
   * Return the valid range for the value.
   */
  public Range getRange()
  {
    return _range;
  }
}
