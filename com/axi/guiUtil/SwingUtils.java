package com.axi.guiUtil;

import java.awt.*;
import java.lang.reflect.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.util.*;

/**
 * This class provides various utilites related to Swing.
 * @author Steve Anonson
 * @author Bill Darbie
 */
public class SwingUtils
{
  /**
   * @author Bill Darbie
   */
  private SwingUtils()
  {
    // do nothing
  }

  /**
   * This method sets the position of the passed in Component relative to the screen.
   * The desired relative position of the component is indicated by the proportionX
   * and proportionY parameters.  These values must be between 0 and 1 and represent
   * the fractional piece of the screen extent to center the component.  For example,
   * if 0.5 was passed in for both proportionX and proportionY, the component would
   * be centered on the screen.  If parameters were 0.25 instead, the component
   * would be placed in the upper left quadrant of the screen.  If the requested
   * position would take the component off the screen, even partially, the location
   * is adjusted so that the component is wholly on the screen.
   * Entering negative values for proportionX or proportionY will calculate a fractional
   * position relative to the right side of the screen instead of the left.
   *
   * The pack() method must be called before calling this method to make sure that
   * the size of the component has been calculated.
   *
   * @param component the Component whose location is set.
   * @param proportionX the fraction of the screen's x extent to center the component width.
   * @param proportionY the fraction of the screen's y extent to center the component height.
   *
   * @author Steve Anonson
   */
  public static void setScreenPosition( Component component, double proportionX, double proportionY )
  {
    Assert.expect(component != null);

    // Make sure x and y values are within 0.0 to 1.0 range.
    if (proportionX > 1)
      proportionX =  1;
    if (proportionX < -1)
      proportionX = -1;
    if (proportionY > 1)
      proportionY =  1;
    if (proportionY < -1)
      proportionY = -1;

    // Convert negative values to a left side relative fraction by adding 1.
    if (proportionX < 0)
      proportionX += 1;
    if (proportionY < 0)
      proportionY += 1;

    // Get screen and component sizes.
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice[] gs = ge.getScreenDevices();

    // shortcut time.  See the comment in centerOnComponent

    // assume the monitors are side by side instead of on top of each other
    // I'll not hardcode 2 monitors in here, maybe one day I'll have 3 :-)
    if (gs.length > 1)
      screenSize.width *= gs.length;  // multiply width by the number of monitors

    Dimension compSize = component.getSize();

    // Make sure component is not larger than the screen.
    if (compSize.height > screenSize.height)
      compSize.height = screenSize.height;
    if (compSize.width > screenSize.width)
      compSize.width = screenSize.width;

    // Find the position of the center of the component relative to the screen.
    Point position = new Point((int)(screenSize.width  * proportionX),
                               (int)(screenSize.height * proportionY));

    // Determine half the width and height of the component.
    Dimension extent = new Dimension(compSize.width / 2, compSize.height / 2);

    // Calculate the upper left coordinate of the component that will put it's center in the desired position.
    Point location = new Point(position.x - extent.width, position.y - extent.height);

    // If the components position is off the screen, move it back on.
    // First check the left side.
    if (location.x < 0)
      location.x = 0;
    if (location.y < 0)
      location.y = 0;
    // Now check the right side.
    if ((location.x + compSize.width) > screenSize.width)
      location.x -= ((location.x + compSize.width) - screenSize.width);
    if ((location.y + compSize.height) > screenSize.height)
      location.y -= ((location.y + compSize.height) - screenSize.height);

    // Set the new component location.
    component.setLocation( location );
  }

  /**
   * This method centers a component on the screen.
   *
   * The pack() method must be called before calling this method to make sure that
   * the size of the component has been calculated.
   *
   * @param component the Component whose location is set.
   * @author Steve Anonson
   */
  public static void centerOnScreen( Component component )
  {
    Assert.expect(component != null);
    SwingUtils.setScreenPosition(component, 0.5, 0.5);
  }

  /**
   * This method centers the position of the passed in Component on another Component.
   *
   * @param topComponent the Component whose location is to be set.
   * @param bottomComponent the Component whose location is used to determine where center.
   *
   * @author Steve Anonson
   */
  public static void centerOnComponent( Component topComponent, Component bottomComponent )
  {
    Assert.expect(topComponent != null);
    Assert.expect(bottomComponent != null);

    // Get the size of the two components and screen
    Dimension topSize = topComponent.getSize();
    Dimension bottomSize = bottomComponent.getSize();

    // this only return the screen size of the primary monitor.  Good enough for one monitor systems,
    // but the following code will work with all monitor configurations
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    // handling of dual monitors

    long beforeTime = System.currentTimeMillis();
//    Rectangle screenSize = new Rectangle();
    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice[] gs = ge.getScreenDevices();

    // shortcut time.  The following for loop is painful.  The GraphicsDevice.getConfigurations() call
    // takes about 3 seconds, and we'll call it twice for a dual monitor system.  Since we only ship with a
    // single monitor, we'll take a short cut here for our developers

    // assume the monitors are side by side instead of on top of each other
    // I'll not hardcode 2 monitors in here, maybe one day I'll have 3 :-)
    if (gs.length > 1)
      screenSize.width *= gs.length;  // multiply width by the number of monitors

//    for (int j = 0; j < gs.length; j++)
//    {
//      GraphicsDevice gd = gs[j];
//      long afterTime = System.currentTimeMillis();System.out.println("Time to calc screen size: " + (afterTime - beforeTime));
//      GraphicsConfiguration[] gc = gd.getConfigurations();
//      afterTime = System.currentTimeMillis();System.out.println("Time to calc screen size: " + (afterTime - beforeTime));
//      for (int i = 0; i < gc.length; i++)
//      {
//        screenSize = screenSize.union(gc[i].getBounds());
//      }
//    }

    // Get the location of the bottomComponent.
    Point location;
    if (bottomComponent.isVisible())
      location = bottomComponent.getLocationOnScreen();
    else
      location = bottomComponent.getLocation();

    // Find the screen coordinate center point of the bottomComponent.
    Point position = new Point((int)(location.x + (bottomComponent.getWidth() / 2)),
                               (int)(location.y + (bottomComponent.getHeight() / 2)));

    // Determine half the width and height of the component.
    Dimension extent = new Dimension(topSize.width / 2, topSize.height / 2);

    // Find the position of the center of the component relative to the other component.
    Point newPosition = new Point((int)(position.x - extent.width),
                                  (int)(position.y - extent.height));

    // If the components position is off the screen, move it back on.
    // First check the left side.
    if (newPosition.x < 0)
      newPosition.x = 0;
    if (newPosition.y < 0)
      newPosition.y = 0;
    // Now check the right side.
    if ((newPosition.x + topSize.width) > screenSize.width)
      newPosition.x -= ((newPosition.x + topSize.width) - screenSize.width);
    if ((newPosition.y + topSize.height) > screenSize.height)
      newPosition.y -= ((newPosition.y + topSize.height) - screenSize.height);

    // Set the new component location.
    topComponent.setLocation( newPosition );
  }

  /**
   * Set the fonts of the root component and all of its subcomponents to
   * the font passed in.
   * Added the synchronized keyword to fix CR15371 on 8/4/04.
   *
   * @author Bill Darbie
   */
  public static synchronized void setFont(Component rootComponent, Font font)
  {
    Assert.expect(rootComponent != null);
    Assert.expect(font != null);

    // set the current look and feel to the new font so any new components created
    // will have the correct font
//    UIDefaults defaults = UIManager.getLookAndFeelDefaults();
//    java.util.List newDefaults = Collections.synchronizedList(new ArrayList());
//    Set defaultsSet = Collections.synchronizedSet(defaults.keySet());
//    Iterator it = defaultsSet.iterator();
//    while(it.hasNext())
//    {
//      String key = (String)it.next();
//      if (key.endsWith(".font"))
//      {
//        newDefaults.add(key);
//        newDefaults.add(font);
//      }
//    }
//    defaults.putDefaults(newDefaults.toArray());
//    SwingUtilities.updateComponentTreeUI(rootComponent);
    setExistingFonts(rootComponent, font);
  }

//  /**
//   * Change the size of all the fonts in the rootComponent passed in and in all its subcomponents.
//   * The font size will change by the amount (positive or negative) specified by sizeDelta
//   *
//   * @author Bill Darbie
//   */
//  public static void changeFontSize(Component rootComponent, int sizeDelta)
//  {
//  }

  /**
   * Sets all the components already created to the font passed in.
   *
   * @author Bill Darbie
   */
  private static void setExistingFonts(Component component, Font font)
  {
    if (component instanceof JTable)
    {
      // JTable does not automatically resize rowHeight based on the font - so do it here
      JTable table = (JTable)component;
      FontMetrics fm = table.getFontMetrics(font);
      table.setRowHeight(fm.getHeight());
    }
    else if (component instanceof JTree)
    {
      // JTree does not automatically resize rowHeight based on the font - so do it here
      JTree tree = (JTree)component;
      FontMetrics fm = tree.getFontMetrics(font);
      tree.setRowHeight(fm.getHeight());
    }
    if (component instanceof JComponent)
    {
      JComponent comp = (JComponent)component;
      // now check for a title border and set its font as well
      Border border = comp.getBorder();
      if ((border != null) && (border instanceof TitledBorder))
        ((TitledBorder)border).setTitleFont(font);
    }

    component.setFont(font);

    Component[] children = null;
    if (component instanceof JMenu)
    {
      JMenu menu = (JMenu)component;
      children = menu.getMenuComponents();
    }
    else if (component instanceof Container)
    {
      Container container = (Container)component;
      children = container.getComponents();
    }
    if (children != null)
    {
      for(int i = 0; i < children.length; i++)
      {
        setExistingFonts(children[i], font);
      }
    }
  }

  /**
   * Sets all the components already created to the color passed in.
   *
   * @author Bill Darbie
   */
  private static void setExistingForeground(Component component, Color color)
  {
    component.setForeground(color);

    Component[] children = null;
    if (component instanceof JMenu)
    {
      JMenu menu = (JMenu)component;
      children = menu.getMenuComponents();
    }
    else if (component instanceof Container)
    {
      Container container = (Container)component;
      children = children = container.getComponents();
    }
    if (children != null)
    {
      for(int i = 0; i < children.length; i++)
      {
        setExistingForeground(children[i], color);
      }
    }
  }

  /**
   * Sets all the components already created to the color passed in.
   *
   * @author Bill Darbie
   */
  private static void setExistingBackground(Component component, Color color)
  {
    component.setBackground(color);

    Component[] children = null;
    if (component instanceof JMenu)
    {
      JMenu menu = (JMenu)component;
      children = menu.getMenuComponents();
    }
    else if (component instanceof Container)
    {
      Container container = (Container)component;
      children = container.getComponents();
    }
    if (children != null)
    {
      for(int i = 0; i < children.length; i++)
      {
        setExistingBackground(children[i], color);
      }
    }
  }

  /**
   * Set the foreground color of the root component and all of its subcomponents
   * to whatever is passed in.
   * @author Bill Darbie
   */
  public static void setForeground(Component rootComponent, Color color)
  {
    UIDefaults defaults = UIManager.getLookAndFeelDefaults();
    java.util.List<Object> newDefaults = new ArrayList<Object>();
    for (Object it: defaults.keySet())
    {
      String key = (String)it;

      if ((key.endsWith(".foreground")) || (key.endsWith(".textForeground")))
      {
        newDefaults.add(key);
        newDefaults.add(color);
      }
    }
    defaults.putDefaults(newDefaults.toArray());
    SwingUtilities.updateComponentTreeUI(rootComponent);
    setExistingForeground(rootComponent, color);
  }

  /**
   * Set the backgound color of the root component and all of its subcomponents
   * to whatever is passed in.
   * @author Bill Darbie
   */
  public static void setBackground(Component rootComponent, Color color)
  {
    UIDefaults defaults = UIManager.getLookAndFeelDefaults();
    java.util.List<Object> newDefaults = new ArrayList<Object>();
    for (Object it: defaults.keySet())
    {
      String key = (String)it;

      if ((key.endsWith(".background")) || (key.endsWith(".textBackground")))
      {
        newDefaults.add(key);
        newDefaults.add(color);
      }
    }
    defaults.putDefaults(newDefaults.toArray());
    SwingUtilities.updateComponentTreeUI(rootComponent);
    setExistingBackground(rootComponent, color);
  }

  /**
   * @author Andy Mechtenberg
   */
  public static void scrollTableToShowSelection(JTable table)
  {
    Assert.expect(table != null);
    ListSelectionModel componentTableListSelectionModel = table.getSelectionModel();
    Rectangle rect = table.getCellRect(componentTableListSelectionModel.getMinSelectionIndex(), 0, true);
    table.scrollRectToVisible(rect);
  }

  /**
   * Makes all components passed in the same length (x-direction)
   * @author Andy Mechtenberg
   */
  public static void makeAllSameLength(JComponent[] jComponents)
  {
    int maxLength = 0;
    int height = 0;
    for(int i = 0; i < jComponents.length; i++)
    {
      JComponent jComponent = jComponents[i];
      int length = jComponent.getPreferredSize().width;
      height = jComponent.getPreferredSize().height;
      if (maxLength < length)
        maxLength = length;
    }
    for(int i = 0; i < jComponents.length; i++)
    {
      JComponent jComponent = jComponents[i];
      jComponent.setPreferredSize(new Dimension(maxLength, jComponent.getPreferredSize().height));
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public static void setTableAndScrollPanelToPreferredSize(JScrollPane scrollPane, JTable table)
  {
    Assert.expect(scrollPane != null);
    Assert.expect(table != null);
    int origWidth = scrollPane.getPreferredSize().width;
    int scrollHeight = scrollPane.getPreferredSize().height;

    int tableHeight =  table.getPreferredSize().height;

    int prefHeight = scrollHeight;
    if (tableHeight < scrollHeight)
    {
      prefHeight = tableHeight;
      Dimension prefScrollSize = new Dimension(origWidth, prefHeight);
      table.setPreferredScrollableViewportSize(prefScrollSize);
    }
  }

  /**
   * This call will put the runnable class on the Swing thread.  It will
   * also catch any exceptions and log them with Assert.logException().
   * All GUI code should use this method instead of directly calling
   * SwingUtilities.invokeLater()
   * @author Bill Darbie
   */
  public static void invokeLater(final Runnable runnable)
  {
    Assert.expect(runnable != null);
    // put the call on the swing thread
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          runnable.run();
        }
        catch (Throwable throwable)
        {
          Assert.logException(throwable);
        }
      }
    });
  }


  /**
   * Does the normal invokeAndWait except that if we are already on the swing thread
   * then it simply calls run without doing a SwingUtilities.invokeAndWait().
   *
   * @author Bill Darbie
   */
  public static void invokeAndWait(final Runnable runnable) throws InterruptedException,
                                                                                InvocationTargetException
  {
    Assert.expect(runnable != null);
    // put the call on the swing thread
    SwingUtilities.invokeAndWait(new Runnable()
    {
      public void run()
      {
        try
        {
          runnable.run();
        }
        catch (Throwable throwable)
        {
          Assert.logException(throwable);
        }
      }
    });
  }
  
  /**
   * @author Wei Chin
   */
  public static boolean isHDScreenResolutionOrHigher()
  {
    GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
    int width = gd.getDisplayMode().getWidth();
    int height = gd.getDisplayMode().getHeight();
    if(height > 1000 )
    {
      return true;
    }
    return false;
  }
}

