package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

/**
 * This class extends the swing JTable adding in column sorting
 * It is further extended to allow processing of selection events to be stopped
 * and to allow row selection by matching typeahead characters.
 *
 * @author Claude Duguay of JavaPro
 * @author George Booth
 */
public class JSortTable extends JTable implements MouseListener
{
  protected int sortedColumnIndex = -1;
  protected boolean sortedColumnAscending = true;
  protected boolean _resizing = false;  // use to differentiate between resizing columns and sorting columns

  // variables to support table seach by typing characters
  private long _MAX_TIME_BETWEEN_KEYSTROKES = 500;
  private long _lastTimeInMillis = 0;
  private String _currentKeystrokeString = "";

  public JSortTable()
  {
    this(new DefaultSortTableModel());
    getTableHeader().setReorderingAllowed(false);
  }

  public JSortTable(int rows, int cols)
  {
    this(new DefaultSortTableModel(rows, cols));
    getTableHeader().setReorderingAllowed(false);
  }

  public JSortTable(Object[][] data, Object[] names)
  {
    this(new DefaultSortTableModel(data, names));
    getTableHeader().setReorderingAllowed(false);
  }

  public JSortTable(Vector data, Vector names)
  {
    this(new DefaultSortTableModel(data, names));
    getTableHeader().setReorderingAllowed(false);
  }
  
  /**
   * added by sheng chuan to handle some table with different table structure (numbering column)
   * @return 
   */
  public int getSortingColumn()
  {
    return 0;
  }
  

  // author George Booth
  private KeyAdapter _keyListener = new KeyAdapter()
    {
      public void keyTyped(KeyEvent e)
      {
        int col = getSelectedColumn();
        if (col == getSortingColumn() && dataModel.isCellEditable(0, 0) == false)
        {
          long currentTimeInMillis = System.currentTimeMillis();
          if (currentTimeInMillis - _lastTimeInMillis > _MAX_TIME_BETWEEN_KEYSTROKES)
          {
            _currentKeystrokeString = "";
          }
          _lastTimeInMillis = currentTimeInMillis;
          _currentKeystrokeString = _currentKeystrokeString + e.getKeyChar();

//          System.out.println("JSortTable key = " + _currentKeystrokeString);
          int row = ((SortTableModel)getModel()).getRowForDataStartingWith(_currentKeystrokeString);
          if (row > -1)
          {
            setRowSelectionInterval(row, row);
            ListSelectionModel lsm = getSelectionModel();
            Rectangle rect = getCellRect(lsm.getMinSelectionIndex(), 0, true);
            scrollRectToVisible(rect);
          }
        }
      }
    };

  public JSortTable(SortTableModel model)
  {
    super(model);
    initSortHeader();
    getTableHeader().setReorderingAllowed(false);
    this.addKeyListener(_keyListener);
  }

  public JSortTable(SortTableModel model,
    TableColumnModel colModel)
  {
    super(model, colModel);
    initSortHeader();
    getTableHeader().setReorderingAllowed(false);
    this.addKeyListener(_keyListener);
  }

  public JSortTable(SortTableModel model,
    TableColumnModel colModel,
    ListSelectionModel selModel)
  {
    super(model, colModel, selModel);
    initSortHeader();
    getTableHeader().setReorderingAllowed(false);
    this.addKeyListener(_keyListener);
  }


  protected void initSortHeader()
  {
    JTableHeader header = getTableHeader();
    header.setDefaultRenderer(new SortHeaderRenderer());
    header.addMouseListener(this);
  }

  public void resetSortHeader()
  {
    sortedColumnIndex = 0;
    sortedColumnAscending = true;
    getTableHeader().repaint();
  }

  public int getSortedColumnIndex()
  {
    return sortedColumnIndex;
  }

  public boolean isSortedColumnAscending()
  {
    return sortedColumnAscending;
  }

  public void mouseReleased(MouseEvent event)
  {
    if (_resizing)
      return;
    TableColumnModel colModel = getColumnModel();
    int index = colModel.getColumnIndexAtX(event.getX());
    // sometimes this returns -1 when the mouse isn't over column, but was earlier
    if (index < 0)
      return;
    int modelIndex = colModel.getColumn(index).getModelIndex();

    SortTableModel model = (SortTableModel)getModel();
    if (model.isSortable(modelIndex))
    {
      // toggle ascension, if already sorted
      if (sortedColumnIndex == index)
      {
        sortedColumnAscending = !sortedColumnAscending;
      }
      sortedColumnIndex = index;

      model.sortColumn(modelIndex, sortedColumnAscending);
      if (getModel() instanceof AbstractTableModel)
      {
        AbstractTableModel m = (AbstractTableModel)getModel();
        m.fireTableDataChanged();
        getTableHeader().repaint();
      }
    }
  }

  public void mousePressed(MouseEvent event)
  {
    JTableHeader source = (JTableHeader)event.getSource();
    if (source.getResizingColumn() != null)
      _resizing = true;
    else
      _resizing = false;
  }
  public void mouseClicked(MouseEvent event)
  {
    // do nothing
  }
  public void mouseEntered(MouseEvent event) {}
  public void mouseExited(MouseEvent event) {}

  // Methods to control selection updating.

  // true if table should process change selection events (mouse, cursor keys, etc)
  private boolean _processSelections = true;

  /**
   * control event handling - false prevents table from responding to selections
   * @author George Booth
   */
  public void processSelections(boolean enabled)
  {
    _processSelections = enabled;
  }

  /**
   * override JTable changeSelection method to allow selections to be ignored
   * @author George Booth
   */
  public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend) 
  {    
    if (_processSelections)
    {
      super.changeSelection(rowIndex, columnIndex, toggle, extend);
    }
  }

}

