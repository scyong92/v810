package com.axi.guiUtil;

import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.basic.BasicArrowButton;

/**
 * This class implements the arrows used for a spin box.  A spin
 * box is a entry device that can hold a range of values.  The arrows are clicked
 * to increment or decrement the value by a fixed amount.
 * The idea came from the book <U>Swing</U> by Mathew Robinson and Pavel Vorobiev.
 *
 * @see IntegerSpinBox
 * @author  Steve Anonson
 */
public class Spinner extends JPanel
{
  private int _orientation = SwingConstants.VERTICAL;
  private BasicArrowButton _incrementBtn;
  private BasicArrowButton _decrementBtn;

  /**
   * Defaults to a vertical set of arrow buttons.
   */
  public Spinner()
  {
    this(SwingConstants.VERTICAL);
  }

  /**
   * Pass in the orientation of the arrows.  This value should be either
   * SwingConstants.VERTICAL or SwingConstants.HORIZONTAL.  If some other value
   * is passed in an exception is thrown.
   *
   * @param orientation the direction of the arrows.
   */
  public Spinner( int orientation )
  {
    if (orientation != SwingConstants.VERTICAL &&
        orientation != SwingConstants.HORIZONTAL)
      throw new IllegalArgumentException("Invalid Spinner orientation.");

    _orientation = orientation;

    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * Create the Spinner contents.
   */
  void jbInit() throws Exception
  {
    if (_orientation == SwingConstants.VERTICAL)
    {
      this.setLayout( new GridLayout(2, 1) );
      _incrementBtn = new BasicArrowButton(SwingConstants.NORTH);
      _decrementBtn = new BasicArrowButton(SwingConstants.SOUTH);
      this.add(_incrementBtn);
      this.add(_decrementBtn);
    }
    else
    {
      this.setLayout( new GridLayout(1, 2) );
      _incrementBtn = new BasicArrowButton(SwingConstants.EAST);
      _decrementBtn = new BasicArrowButton(SwingConstants.WEST);
      this.add(_decrementBtn);
      this.add(_incrementBtn);
    }
  }

  /**
   * Enable / disable the arrow buttons.
   *
   * @param enable true to enable, false to disable.
   */
  public void setEnabled( boolean enable )
  {
    _incrementBtn.setEnabled(enable);
    _decrementBtn.setEnabled(enable);
  }

  /**
   * @return whether the arrow buttons are enabled.
   */
  public boolean isEnabled()
  {
    return (_incrementBtn.isEnabled() && _decrementBtn.isEnabled());
  }

  // Access methods to the buttons.
  public JButton getIncrementButton()
  {
    return _incrementBtn;
  }

  public JButton getDecrementButton()
  {
    return _decrementBtn;
  }

}
