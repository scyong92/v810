/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.guiUtil;

import com.axi.util.*;
import java.awt.*;
import javax.swing.*;
import org.jfree.chart.*;
import org.jfree.chart.plot.*;
import org.jfree.chart.title.*;
import org.jfree.data.xy.*;
import org.jfree.ui.*;

/**
 * @author Ying-Huan.Chu
 */
public class XYPlotDialog extends EscapeDialog
{
  private String _title = "";
  private String _xLabel = "";
  private String _yLabel = "";
  
  private java.util.List _xAxisList = null;
  private java.util.List _yAxisList = null;
  
  private int _dialogWidth = 700;
  private int _dialogHeight = 700;

  private JFreeChart _chart;
  
  /**
   * XYPlotDialog Constructor
   * @author Ying-Huan.Chu
   */
  public XYPlotDialog(String title, String xLabel, String yLabel, java.util.List xAxisList, java.util.List yAxisList)
  {
    _title = title;
    _xLabel = xLabel;
    _yLabel = yLabel;
    _xAxisList = xAxisList;
    _yAxisList = yAxisList;
  }
  
  /**
   * Plot graph in an Escape Dialog
   * @author Ying-Huan.Chu
   */
  public void plotData() throws XYListSizeDifferentException, EmptyListException, BadFormatException
  {
    Assert.expect(_title != null);
    Assert.expect(_xLabel != null);
    Assert.expect(_yLabel != null);
    Assert.expect(_xAxisList != null);
    Assert.expect(_yAxisList != null);
    
    if (_xAxisList.isEmpty())
    {
      throw new EmptyListException(new LocalizedString("XYPLOTDIALOG_XAXIS_LIST_IS_EMPTY_MESSAGE_KEY", null));
    }
    if (_yAxisList.isEmpty())
    {
      throw new EmptyListException(new LocalizedString("XYPLOTDIALOG_YAXIS_LIST_IS_EMPTY_MESSAGE_KEY", null));
    }
    if (_xAxisList.size() != _yAxisList.size())
    {
      throw new XYListSizeDifferentException(new LocalizedString("XYPLOTDIALOG_XYAXIS_LIST_SIZE_DIFFERENT_ERROR_MESSAGE_KEY", null));
    }
    if (_xAxisList.get(0) instanceof Number == false)
    {
      throw new BadFormatException(new LocalizedString("XYPLOTDIALOG_XAXIS_LIST_NOT_NUMBER_MESSAGE_KEY", null));
    }
    if (_yAxisList.get(0) instanceof Number == false)
    {
      throw new BadFormatException(new LocalizedString("XYPLOTDIALOG_YAXIS_LIST_NOT_NUMBER_MESSAGE_KEY", null));
    }
    else
    {
      XYSeries series = new XYSeries(_title);
      XYSeriesCollection xyDataSet = new XYSeriesCollection(series);
      for (int i = 0; i < _xAxisList.size(); ++i)
      {
        series.add((Number)_xAxisList.get(i), (Number)_yAxisList.get(i));
      }

      _chart = ChartFactory.createXYLineChart
                  (_title, // Title
                   _xLabel,// X-Axis label
                   _yLabel,// Y-Axis label
                   xyDataSet,// Dataset
                   PlotOrientation.VERTICAL,
                   true,// Show legend
                   true,
                   false
                  );

      ChartPanel chartPanel = new ChartPanel(_chart);
      JPanel mainPanel = new JPanel(new BorderLayout());
      mainPanel.add(chartPanel, BorderLayout.CENTER);

      mainPanel.setPreferredSize(new Dimension(_dialogWidth, _dialogHeight));
      getContentPane().add(mainPanel, BorderLayout.CENTER);
      setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      pack();

      setVisible(true);
      setAlwaysOnTop(true);
      setTitle(_title);
    }
  }
  
  /**
   * Mark the maximum point of the data using a vertical line and label the x-value
   * @author Ying-Huan.Chu
   */
  public void markMaximumPoint()
  {
    int maximum = 0;
    for (int i = 1; i < _yAxisList.size(); ++i)
    {
      if ((double)_yAxisList.get(i) > (double)_yAxisList.get(maximum))
      {
        maximum = i;
      }
    }
    final XYPlot plot = _chart.getXYPlot();
    double xValue = MathUtil.roundToPlaces(((Number)_xAxisList.get(maximum)).doubleValue(), 4);
    //double yValue = MathUtil.roundToPlaces(((Number)_yAxisList.get(maximum)).doubleValue(), 4);
    ValueMarker marker = new ValueMarker(xValue); 
    marker.setPaint(Color.black);
    
    //String textAnnotationLabel = _xLabel + " = " + xValue;
    //XYTextAnnotation textAnnotaion = new XYTextAnnotation(textAnnotationLabel, xValue, yValue);
    
    //plot.addAnnotation(textAnnotaion);
    plot.addDomainMarker(marker);
    TextTitle legendText = new TextTitle(_xLabel + " = " + xValue);
    legendText.setPosition(RectangleEdge.TOP);
    _chart.addSubtitle(legendText);
  }
  
  /**
   * Mark the specific point of the data using a vertical line and label the coordinate
   * @author Ying-Huan.Chu
   */
  public void markSpecificPoint(Number specificXValue)
  {
    int index = 0;
    for (int i = 0; i < _xAxisList.size() - 1; ++i)
    {
      if (specificXValue.doubleValue() >= ((Number)_xAxisList.get(i)).doubleValue())
      {
        if (specificXValue.doubleValue() <= ((Number)_xAxisList.get(i+1)).doubleValue())
        {
          index = i;
        }
      }
    }
    final XYPlot plot = _chart.getXYPlot();
    double xValue = MathUtil.roundToPlaces(specificXValue.doubleValue(), 4);
    //double yValue = MathUtil.roundToPlaces(((Number)_yAxisList.get(index)).doubleValue(), 4);
    ValueMarker marker = new ValueMarker(xValue); 
    marker.setPaint(Color.black);
    
    //String textAnnotationLabel = _xLabel + " = " + xValue;
    //XYTextAnnotation textAnnotaion = new XYTextAnnotation(textAnnotationLabel, xValue, yValue);
    //plot.addAnnotation(textAnnotaion);
    
    plot.addDomainMarker(marker);
    
    TextTitle legendText = new TextTitle(_xLabel + " = " + xValue);
    legendText.setPosition(RectangleEdge.TOP);
    _chart.addSubtitle(legendText);
  }
}
