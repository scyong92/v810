package com.axi.guiUtil;

import java.awt.*;
import java.awt.geom.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.util.image.*;
import java.util.List;

/**
 * This is the base class for all Renderers used by the GraphicsEngine class.  It sets itself up so that
 * it is NOT opaque so that Renderers on layers below it will be seen.  It also
 * sets up a mouse listener to allow selecting of Renderers that the user
 * has clicked on.
 *
 * To create your own Renderer class extend this one.  If you need to render a shape, use
 * ShapeRenderer.  If you need to render an image, use BufferedImageRenderer.  If you need to
 * render text, use TextRenderer.
 *
 * Implementation note:  This class is derived from the Swing JComponent.  It uses the JComponent
 * setBounds() to control where rendering actually occurs within the rendering canvas.  The
 * paintComponent method must render the object relative to the bounds of the JComponent.
 *
 * @see GraphicsEngine
 * @see ShapeRenderer
 * @see BufferedImageRenderer
 * @see TextRenderer
 *
 * @author Bill Darbie
 */
public abstract class Renderer extends JComponent
{
  protected static final boolean _COMPONENT_IS_SELECTABLE = true;
  protected static final boolean _COMPONENT_IS_NOT_SELECTABLE = false;

  // the Bounds that all Swing components use are in terms of pixels, which are ints
  // We are using doubles because we are doing a lot of math operations (when scaling
  // and translating) and using ints would cause rounding errors.
  //
  // Each time we call setBounds and pass these double into the setBounds() method that takes
  // ints we have to convert from double to int.  So the JComponent's bounds are not the exact
  // bounds.  The _xInPixels, _yInPixels, _widthInPixels, _heightInPixels values are always the exact values.
  protected double _xInPixels;
  protected double _yInPixels;
  protected double _widthInPixels;
  protected double _heightInPixels;

  // the color to use when the Renderer is selected (clicked on with the mouse)
  private Color _selectedColor = Color.WHITE;
  private Color _highlightedColor = Color.WHITE;
  private Color _foreground = Color.WHITE;
  private Stroke _stroke;
  private boolean _isSelectable = false;

  private AffineTransform _currentTransform = AffineTransform.getScaleInstance(1.0, 1.0);

  /**
   * Construct a new renderer.  Specify if the Renderer can be selected by a mouse click.
   *
   * @param isSelectable is true if the Renderer should respond to a mouse click
   * @author Bill Darbie
   */
  protected Renderer(boolean isSelectable)
  {
    _isSelectable = isSelectable;

    // allow components below this one to be seen
    setOpaque(false);
    setLayout(null);

    // apm -- attempt to get a renderer to stay out of the z-order sort
    // didn't work, but I want to document what I tried
//    setFocusable(false);
//    setRequestFocusEnabled(false);
//    setFocusCycleRoot(false);
//    setFocusTraversalKeysEnabled(false);
//    setFocusTraversalPolicyProvider(false);
//    setVerifyInputWhenFocusTarget(false);
  }

  /**
   * @author Bill Darbie
   */
  boolean isSelectable()
  {
    return _isSelectable;
  }


  /**
   * Call this method if the underlying data has changed which would make
   * the graphics change.
   *
   * @author Bill Darbie
   */
  public void validateData()
  {
    refreshData();
    applyTransform(_currentTransform);
    validate();
  }

  /**
   * This method needs to overridden to get the most recent data required
   * to draw the appropriate Shape.  Your method needs to call initializeShape()
   * with the new Shape.
   * @author Bill Darbie
   */
  protected abstract void refreshData();


  /**
   * Apply the passed in transform to this Renderer and keep _currentTransform up to date.
   * @author Bill Darbie
   */
  public void transform(AffineTransform transform)
  {
    Assert.expect(transform != null);
    _currentTransform.preConcatenate(transform);

    applyTransform(transform);
  }

  /**
   * Apply the passed in transform to this Renderer without modifying _currentTransform.
   * @author Bill Darbie
   */
  protected abstract void applyTransform(AffineTransform transform);

  /**
   * The default for Swing components is for positive x to be to the right of the origin
   * and positive y to be below the origin.  If the coordinate system for this renderer
   * does not match this default, a transform must be applied to map the renderer into
   * Swing pixel coordinates.  The transform includes a reflection across the x axis and/or
   * y axis as needed, plus a translation to bring the renderers back into the original quadrant
   * after the reflection.  This method may be overridden by subclasses if the Renderer needs
   * special processing to implement the axis transform properly.
   * @author Kay Lannen
   * @param positiveXisToTheRightOfTheOrigin boolean -indicates x axis interpretation
   * @param positiveYisBelowTheOrigin boolean -indicates y axis interpretation
   * @param xTranslation double -amount of translation required to bring renderers back into original quadrant after reflection across x axis
   * @param yTranslation double -amount of translation required to bring renderers back into original quadrant after reflection across y axis
   */
  public void applyAxisTransform(boolean positiveXisToTheRightOfTheOrigin, boolean positiveYisBelowTheOrigin,
  double xTranslation, double yTranslation)
  {
    AffineTransform transform = AffineTransform.getScaleInstance(1.0, 1.0);
    if (positiveXisToTheRightOfTheOrigin == false)
    {
      transform.preConcatenate(AffineTransform.getScaleInstance( -1.0, 1.0));
      transform.preConcatenate(AffineTransform.getTranslateInstance(xTranslation, 0.0));
    }

    if (positiveYisBelowTheOrigin == false)
    {
      transform.preConcatenate(AffineTransform.getScaleInstance(1.0, -1.0));
      transform.preConcatenate(AffineTransform.getTranslateInstance(0.0, yTranslation));
    }

    transform(transform);
  }

  /**
   * Call this method whenever the bounds (_xInPixels, _yInPixels, _widthInPixels, _heightInPixels) of this Renderer have changed.
   * The bounds are used for things like capturing mouse events so it is important to have the bounds accurately
   * wrap around where the shape is drawn.
   *
   * @author Bill Darbie
   */
  protected void setBounds()
  {
    //XCR-2664: Black Line is not clear in Image Window during Fine Tuning
    super.setBounds((int)Math.round(_xInPixels), (int)Math.round(_yInPixels), (int)Math.round(_widthInPixels), (int)Math.round(_heightInPixels));
  }

  /**
   * @return a Double Rectangle with the high precision real bounds of the renderer
   * @author Andy Mechtenberg
   */
  Rectangle2D getBounds2D()
  {
    return new Rectangle2D.Double(_xInPixels, _yInPixels, _widthInPixels, _heightInPixels);
  }
  
  /** 
   * @return a Rectangle in pixels.
   * @author Ying-Huan.Chu
   */
  public Rectangle getBounds()
  {
    Rectangle2D rect2D = new Rectangle2D.Double(_xInPixels, _yInPixels, _widthInPixels, _heightInPixels).getBounds2D();
    Rectangle rect = new Rectangle();
    rect.x = (int)Math.round(rect2D.getMinX());
    rect.y = (int)Math.round(rect2D.getMinY());
    rect.width = (int)Math.round(rect2D.getMaxX() - rect2D.getMinX());
    rect.height = (int)Math.round(rect2D.getMaxY() - rect2D.getMinY());
    
    return rect;
  }
  /**
   * @return the center coordinate of this Renderer in Double precision
   * @author Andy Mechtenberg
   */
  public DoubleCoordinate getCenterCoordinate()
  {
    return new DoubleCoordinate(_xInPixels + _widthInPixels / 2.0, _yInPixels + _heightInPixels / 2.0);
  }

  /**
   * Set the color that this Renderer will display itself in when it has been selected
   * by a mouse click.
   * @author Bill Darbie
   */
  public void setSelectedColor(Color selectedColor)
  {
    Assert.expect(selectedColor != null);
    _selectedColor = selectedColor;
  }

  /**
   * Set the color that this Renderer will display itself in when it has been selected
   * by a mouse click.
   * @author Bill Darbie
   */
  public void setHighlightedColor(Color highlightedColor)
  {
    Assert.expect(highlightedColor != null);
    _highlightedColor = highlightedColor;
  }

  /**
   * We need to override this method because the JPanel.setForeground() call will
   * call repaint().  If there are thousands of Renderers and we iterate over all
   * of them at once to change their color (which does happen) and a repaint() is
   * called on each one the time is too slow.  It is now up to the caller of setForeground()
   * to call repaint() at the appropriate time.  The GraphicsEngine class does this
   * correctly when it uses setForeground().
   *
   * @author Bill Darbie
   */
  public void setForeground(Color foreground)
  {
    Assert.expect(foreground != null);

    // only set the _foreground - do NOT call repaint() for performance reasons
    _foreground = foreground;
  }

  /**
   * Since setForeground() was overridden by this class, we need to supply a getForeground()
   * as well.
   *
   * @author Bill Darbie
   */
  public Color getForeground()
  {
    Assert.expect(_foreground != null);

    return _foreground;
  }

  /**
   * @return the color used when this Renderer is selected.
   * @author Andy Mechtenberg
   */
  public Color getSelectedColor()
  {
    Assert.expect(_selectedColor != null);
    return _selectedColor;
  }

  /**
   * @return the color used when this Renderer is highlighted.
   * @author Andy Mechtenberg
   */
  public Color getHighlightedColor()
  {
    Assert.expect(_highlightedColor != null);
    return _highlightedColor;
  }

  /**
   * @return if this component contains the x, y point passed in.  Note that the point is relative
   * to the canvas, NOT the component.  This is not the usual way for contains() on swing
   * comonents to work, but it is how the GraphicsEngine expects the Renderers to work.
   * @author Bill Darbie
   */
  public boolean contains(int xPixels, int yPixels)
  {
    return (xPixels >= _xInPixels) && (xPixels < _xInPixels + _widthInPixels - 1) && (yPixels >= _yInPixels) &&
        (yPixels < _yInPixels + _heightInPixels - 1);
  }

  /**
   * @author George A. David
   */
  public boolean hasStroke()
  {
    return _stroke != null;
  }

  /**
   * @author George A. David
   */
  public void setStroke(Stroke stroke)
  {
    Assert.expect(stroke != null);

    _stroke = stroke;
  }

  /**
   * @author George A. David
   */
  public Stroke getStroke()
  {
    Assert.expect(_stroke != null);

    return _stroke;
  }

  /**
   * Some derived classes may need a reference to the GraphicsEngine.  If they do they
   * should override this method to get it.  This method will get called automatically
   * when the Renderer is added to the GraphicsEngine
   * @author Bill Darbie
   */
  protected void setGraphicsEngine(GraphicsEngine graphicsEngine)
  {
    Assert.expect(graphicsEngine != null);
    // do nothing
  }

  /**
   * This method does the actual drawing on the screen.
   * @author Bill Darbie
   */
  protected abstract void paintComponent(Graphics graphics);

  /**
   * @author Chong, Wei Chin
   */
  public void brighten()
  {
    //do nothing
  }

  /**
   * @author Chong, Wei Chin
   */
  public void darken()
  {
    //do nothing
  }
  /**
   * @author Chong, Wei Chin
   */
  public void increaseContrast()
  {
    //do nothing
  }

  /**
   * @author Chong, Wei Chin
   */
  public void decreaseContrast()
  {
    //do nothing
  }

  /**
   * @author Chong, Wei Chin
   */
  public void equalizeGrayScale()
  {
    //do nothing
  }

  /**
   * @author Chong, Wei Chin
   */
  public void revertToOriginalBufferedImage()
  {
    //do nothing
  }

  /**
   * @author Chong, Wei Chin
   */
  public void saveBufferedImage(String filename)
  {
    //do nothing
  }

  /**
   * @author Chong, Wei Chin
   */
  public void increaseDesiredMaxValue()
  {
    // do nothing
  }

  /**
   * @author Chong, Wei Chin
   */
  public void decreaseDesiredMaxValue()
  {
    // do nothing
  }

  /**
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */
  public void imagePostProcessing(List<ImageEnhancerBase> enhancerList)
  {
    //do nothing
  }

  /**
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */
  public BrightnessAndContrastSetting getBrightnessAndContrastSetting()
  {
    return null; 
  }
  
  /**
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */
  public void setBrightnessAndContrastSetting(BrightnessAndContrastSetting setting)
  {
    //do nothing
  }

  /**
   * @author sham
   */
  public double getXInPixels()
  {
    return _xInPixels;
  }

  /**
   * @author sham
   */
  public double getYInPixels()
  {
    return _yInPixels;
  }
}
