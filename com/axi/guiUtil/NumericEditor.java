package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;
import java.text.*;

import javax.swing.*;

import com.axi.util.*;


/**
 * This class provides an editor for a table that you can
 * change the foreground color, background color, text alignment,
 * and the number of decimal places allowed
 * @author Carli Connally
 */
public class NumericEditor extends DefaultCellEditor
{
  private NumericTextField _textField = null;
  private int _decimalPlaces = -1;

  /**
   * Returns a text field editor with the specified attributes.
   * @param backgroundColor Background color of the editor
   * @param foregroundColor Foreground color of the editor
   * @param horizontalAlign Horizontal text alignment.  (use constants from JTextField such as
   * JTextField.LEFT)
   * @param decimalPlaces The number of digits desired to the right of the decimal point (No
   * trailing zeros will be displayed. No decimal point will be displayed if zero is passed in.)
 * @author Carli Connally
  */
  public NumericEditor(Color backgroundColor,
                       Color foregroundColor,
                       int horizontalAlign,
                       int decimalPlaces)
  {
    super(new NumericTextField());
    Assert.expect(backgroundColor != null);
    Assert.expect(foregroundColor != null);
    Assert.expect(decimalPlaces >= 0);

    _textField = (NumericTextField)getComponent();

    //set visual attributes specified
    _textField.setHorizontalAlignment(horizontalAlign);
    _textField.setBackground(backgroundColor);
    _textField.setForeground(foregroundColor);

    //use a numeric document to validate numeric input
    NumericPlainDocument doc = new NumericPlainDocument();
    _textField.setDocument(doc);

    //create a format string for the decimal portion according
    //to the number of digits specified in the method call
    _decimalPlaces = decimalPlaces;
    DecimalFormat decFormat = rebuildDecimalFormat();
    _textField.setFormat(decFormat);

    //text is highlighted when the editor is invoked
    _textField.addFocusListener(new FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        _textField.selectAll();
      }
      /**LC commented the listener below but not sure if it's useful or can be deleted
       * it was preventing a dialog box from a 'setValueAt' method from behaving properly by causing an
       * extra call to 'setValueAt' which in turn cause another instance of the same dialog to pop up*/
//      public void focusLost(FocusEvent e)
//      {
//        stopCellEditing();
//      }
    });

    //the editor is invoked with a single click in the table cell
    setClickCountToStart(1);
  }

  /**
   * Returns a text field editor with the specified attributes.
   * @param backgroundColor Background color of the editor
   * @param foregroundColor Foreground color of the editor
   * @param horizontalAlign Horizontal text alignment.  (use constants from JTextField such as
   * JTextField.LEFT)
   * @param decimalPlaces The number of digits desired to the right of the decimal point (No
   * trailing zeros will be displayed. No decimal point will be displayed if zero is passed in.)
   * @author Carli Connally
   */
  public NumericEditor(Color backgroundColor,
                       Color foregroundColor,
                       int horizontalAlign,
                       int decimalPlaces,
                       double min,
                       double max)
  {
    super(new NumericTextField());
    Assert.expect(backgroundColor != null);
    Assert.expect(foregroundColor != null);
    Assert.expect(decimalPlaces >= 0);

    _textField = (NumericTextField)getComponent();

    //set visual attributes specified
    _textField.setHorizontalAlignment(horizontalAlign);
    _textField.setBackground(backgroundColor);
    _textField.setForeground(foregroundColor);

    //use a numeric document to validate numeric input
    NumericRangePlainDocument doc = new NumericRangePlainDocument();
    doc.setRange(new DoubleRange(min, max));
    _textField.setDocument(doc);

    //create a format string for the decimal portion according
    //to the number of digits specified in the method call
    _decimalPlaces = decimalPlaces;
    DecimalFormat decFormat = rebuildDecimalFormat();
    _textField.setFormat(decFormat);

    //text is highlighted when the editor is invoked
    _textField.addFocusListener(new FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        _textField.selectAll();
      }
      /**LC commented the listener below but not sure if it's useful or can be deleted
       * it was preventing a dialog box from a 'setValueAt' method from behaving properly by causing an
       * extra call to 'setValueAt' which in turn cause another instance of the same dialog to pop up*/
//      public void focusLost(FocusEvent e)
//      {
//        stopCellEditing();
//      }
    });

    //the editor is invoked with a single click in the table cell
    setClickCountToStart(1);
  }

  //Override to invoke setValue on the formatted text field.
   public Component getTableCellEditorComponent(JTable table,
                                                Object value, boolean isSelected,
                                                int row, int column)
   {
     NumericTextField numericTextField = (NumericTextField)super.getTableCellEditorComponent(table, value, isSelected, row, column);
     numericTextField.setValue((Number)value);
     return numericTextField;
    }

    /**
     * Override to ensure that the value remains an Number.
     * @author Andy Mechtenberg
     */
    public Object getCellEditorValue()
    {
      NumericTextField numericTextField = (NumericTextField)getComponent();
      Number number = null;
      try
      {
        number = numericTextField.getNumberValue();
      }
      catch (ParseException ex)
      {
        // this parse exception shouldn't happen because the document stops all illegal input
        number = new Long(0);
      }
      return number;
    }

  /**
   * @author Carli Connally
   * Rebuilds the format string to match the number of decimal places specified in the class.
   * The format is then used by the text field to accept/display data. Leading/trailing zeros
   * are not displayed.
   * @return DecimalFormat that has the number of decimal places as specified in the class.
   */
  private DecimalFormat rebuildDecimalFormat()
  {
    String decimalPattern = new String("");
    for(int i=0; i < _decimalPlaces; i++)
    {
      decimalPattern = decimalPattern + "0";
    }

    //create the format for the decimal number, no decimal point if
    //0 decimal digits specified
    DecimalFormat decFormat;
    if(_decimalPlaces > 0)
    {
      decFormat = new DecimalFormat("########0." + decimalPattern);
    }
    else
    {
      decFormat = new DecimalFormat("#########");
      decFormat.setParseIntegerOnly(true);
    }
    return decFormat;
  }

  /**
   * @author Carli Connally
   * @return Number of decimal places to be displayed
   */
  public int getDecimalPlaces()
  {
    Assert.expect(_decimalPlaces >= 0);

    return _decimalPlaces;
  }

  /**
   * @author Carli Connally
   * Allows user to change the number of decimal places to display.
   * @param decimalPlaces Number of decimal places to display
   */
  public void setDecimalPlaces(int decimalPlaces)
  {
    Assert.expect(decimalPlaces >= 0);

    _decimalPlaces = decimalPlaces;
    DecimalFormat decFormat = rebuildDecimalFormat();
    _textField.setFormat(decFormat);
  }

  /**
   * @author Laura Cormos
   */
  public void setRange(Range range)
  {
    Assert.expect(range != null);

    NumericRangePlainDocument doc = new NumericRangePlainDocument();
    doc.setRange(range);
    _textField.setDocument(doc);
  }
}
