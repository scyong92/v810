package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;

import com.axi.util.*;

/**
 * A custom make popupmenu with a list of check box
 */
public class ScrollableCheckBoxListPopupMenu<T> extends JPopupMenu
{
  private static final String SELECT_ALL_STRING = "Select All";
  private static final String CLEAR_ALL_STRING = "Clear All";
  private JScrollPane _scrollPane = new JScrollPane();
  private JList _list = new JList();
  private DefaultListModel _listModel = new DefaultListModel();
  private JPanel _panel = new JPanel(new BorderLayout());
  private JCheckBox _allSelectionCheckBox;
  private JPanel _allSelectionCheckBoxPanel = new JPanel();

  /**
   * constructor with only object to display and selected index
   */
  public ScrollableCheckBoxListPopupMenu(java.util.List<T> objectList,
                                         java.util.List<Integer> selectedIndeces)
  {
    this(objectList,
         selectedIndeces,
         false,
         false,
         0,
         0);
  }

  /**
   * objectList = list of item to be display in list selectedIndeces = default
   * selected item index isCustomSelectionModel = true will make jlist to able
   * perform multiple selection without CTRL key miniSelectionCount = the
   * minimum selection have to stay within the list
   * ScrollableCheckBoxListPopupMenu constructor with select all list checkbox 
   * addSelectAllCheckBox = true; to add check box to popupmenu
   *
   * @author Janan Wong
   */
  public ScrollableCheckBoxListPopupMenu(java.util.List<T> objectList,
                                         java.util.List<Integer> selectedIndeces,
                                         boolean isCustomSelectionModel,
                                         boolean addSelectAllCheckBox,
                                         final int defaultMinimumSelectionCount,
                                         final int defaultMaximumSelectionCount)
  {
    Assert.expect(objectList != null);
    Assert.expect(selectedIndeces != null);
    Assert.expect(defaultMinimumSelectionCount >= 0);
    Assert.expect(defaultMaximumSelectionCount >= 0);
    
    _allSelectionCheckBox = new JCheckBox(SELECT_ALL_STRING);
    
    setLayout(new BorderLayout());
    _list.setModel(_listModel);
    if (isCustomSelectionModel)
    {
      _list.setSelectionModel(new DefaultListSelectionModel()
      {
        public void setSelectionInterval(int firstIndex, int lastIndex)
        {
          boolean isSelectingAllItems = (firstIndex == 0) && (lastIndex == _listModel.size() - 1);
          //XCR-3845, Check Boxes unable to select in result output format
          int maxSelection = defaultMaximumSelectionCount;
          if (defaultMaximumSelectionCount == 0)
            maxSelection = _listModel.size();
          
          if (isSelectingAllItems)
          {
            firstIndex += defaultMinimumSelectionCount;
            lastIndex = firstIndex + maxSelection - 1;
          }

          //make it to have minimum selected item when unselect all
          //top it to unselect if selected items count is less than defaultMinimumSelectionCount
          if (_list.isSelectedIndex(firstIndex) && _list.getSelectedIndices().length > defaultMinimumSelectionCount)
            _list.removeSelectionInterval(firstIndex, lastIndex);
          else if (isSelectingAllItems)
          {
            _list.clearSelection();
            _list.addSelectionInterval(0, maxSelection -1 );
          }
          else if (_list.getSelectedIndices().length < maxSelection)
             _list.addSelectionInterval(firstIndex, lastIndex);
        }
      });
    }
    
    for (int i = 0; i < objectList.size(); i++)
    {
      _listModel.addElement(objectList.get(i));
      if (selectedIndeces.contains(i))
        _list.addSelectionInterval(i, i);
    }
    
    if (addSelectAllCheckBox)
    {
      _allSelectionCheckBoxPanel.add(_allSelectionCheckBox);

      _allSelectionCheckBox.addItemListener(new ItemListener()
      {
        public void itemStateChanged(ItemEvent e)
        {
          if (e.getStateChange() == ItemEvent.SELECTED)
          {
            _list.addSelectionInterval(0, _listModel.getSize() - 1);
            _allSelectionCheckBox.setText(CLEAR_ALL_STRING);
          }
          else if (e.getStateChange() == ItemEvent.DESELECTED)
          {
            _list.clearSelection();
            _allSelectionCheckBox.setText(SELECT_ALL_STRING);
          }
        }
      });
      
      _list.addListSelectionListener(new ListSelectionListener()
      {
        public void valueChanged(ListSelectionEvent e)
        {
          _allSelectionCheckBox.removeItemListener(_allSelectionCheckBox.getItemListeners()[0]);
          
          if (_list.getSelectedValuesList().size() != _list.getModel().getSize())
          {
            _allSelectionCheckBox.setSelected(false);
            _allSelectionCheckBox.setText(SELECT_ALL_STRING);
          }
          else if (_list.getSelectedValuesList().size() == _list.getModel().getSize())
          {
            _allSelectionCheckBox.setSelected(true);
            _allSelectionCheckBox.setText(CLEAR_ALL_STRING);
          }
          
          if (_listModel.isEmpty())
          {
            _allSelectionCheckBox.setText(SELECT_ALL_STRING);
            _allSelectionCheckBox.setEnabled(false);
            _allSelectionCheckBox.setSelected(false);
          }
          else if(_listModel.isEmpty()==false)
          {
            _allSelectionCheckBox.setEnabled(true);
          }
          
          _allSelectionCheckBox.addItemListener(new ItemListener()
          {
            public void itemStateChanged(ItemEvent e)
            {
              if (e.getStateChange() == ItemEvent.SELECTED)
              {
                _list.addSelectionInterval(0, _listModel.getSize() - 1);
                _allSelectionCheckBox.setText(CLEAR_ALL_STRING);
              }
              else if (e.getStateChange() == ItemEvent.DESELECTED)
              {
                _list.clearSelection();
                _allSelectionCheckBox.setText(SELECT_ALL_STRING);
              }
            }
          });
        }
      });
      
      if(_list.getSelectedValuesList().size()==_list.getModel().getSize()&&_list.getModel().getSize()!=0)
        _allSelectionCheckBox.setSelected(true);
      _panel.add(_allSelectionCheckBoxPanel, BorderLayout.PAGE_START);
    } 
    _list.setCellRenderer(new ListCheckBoxRenderer());
    _scrollPane.setViewportView(_list);      
    
    _panel.add(_scrollPane, BorderLayout.CENTER);
    add(_panel);
  }

  /**
   * @author Yong Sheng Chuan
   */
  public int[] getSelectedIndices()
  {
    return _list.getSelectedIndices();
  }

  /**
   * @author Yong Sheng Chuan
   */
  public java.util.List<T> getSelectedValuesList()
  {
    return _list.getSelectedValuesList();
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setVisibleRowCount(int visibleRowCount)
  {
    _list.setVisibleRowCount(visibleRowCount);
  }
  
  /**
   * @author Janan Wong
   * @return all values from the list
   */
  public java.util.List<Object> getAllValuesList()
  {
    java.util.List<Object> allValueList = new ArrayList<Object>();
    for (int i = 0; i < _list.getModel().getSize(); i++)
    {
      allValueList.add(_list.getModel().getElementAt(i));
    }
    return allValueList;
  }  
  
  /**
   * @author Janan Wong
   * add element to list model
   */
  public void addObjectList(java.util.List<T> objectList)
  {
    for (T object : objectList)
    {
      _listModel.addElement(object);
    }
  }

  /**
   * @author Janan Wong
   * clear the popupmenu list
   */
  public void deleteObjectList()
  {
    _listModel.clear();
  }
  
  /**
   * @author Janan Wong
   * @return List of deselected value from popup menu
   */
  public java.util.List<T> getDeselectedValuesLists()
  {
    JList list = new JList();
    ListModel model = _list.getModel();
    DefaultListModel list2Model = new DefaultListModel();

    for (int i = 0; i < model.getSize(); i++)
    {
      list2Model.addElement(model.getElementAt(i));
    }

    list.setModel(list2Model);

    for (Object item : _list.getSelectedValuesList())
    {
      list2Model.removeElement(item);
    }

    list.addSelectionInterval(0, list.getModel().getSize() - 1);

    return list.getSelectedValuesList();
  }
  
  /**
   * @author Janan Wong
   * reset enable to select all checkbox if popupmenu list is empty
   */
  public void resetSelectAllCheckBox()
  {
    Assert.expect(_allSelectionCheckBox != null);
    if (_listModel.isEmpty())
    {
      _allSelectionCheckBox.setEnabled(false);
    }
    else
    {
      _allSelectionCheckBox.setEnabled(true);
    }
  }


}
