package com.axi.guiUtil;

import javax.swing.*;
import javax.swing.plaf.basic.*;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.BorderLayout;
import java.awt.Component;

/**
 * @author Wei Chin
 * @version 1.0
 */
public class ExtendedComboBox extends JComboBox
{
  public ExtendedComboBox()
  {
    super();
  } //end of default constructor

  public void firePopupMenuWillBecomeVisible()
  {
    resizeComboPopup();
    super.firePopupMenuWillBecomeVisible();
  }

  private void resizeComboPopup()
  {
    FontMetrics fm = getFontMetrics(getFont());
    BasicComboPopup popup = (BasicComboPopup)getUI().getAccessibleChild(this, 0); //Popup
    if (popup == null)
      return;
    int size = (int)getPreferredSize().getWidth();
    for (int i = 0; i < getItemCount(); i++)
    {
      Object obj = getItemAt(i);
      if (size < fm.stringWidth(obj.toString()))
        size = fm.stringWidth(obj.toString());
    }
    Component comp = popup.getComponent(0); //JScrollPane
    JScrollPane scrollpane = null;
    int offset = 0;
    if (comp instanceof JScrollPane)
    {
      scrollpane = (JScrollPane)comp;
      if (scrollpane.getVerticalScrollBar().isVisible())
        offset += scrollpane.getVerticalScrollBar().getWidth();
    }
    popup.setPreferredSize(new Dimension(size + offset, popup.getPreferredSize().height));
    popup.setLayout(new BorderLayout());
    popup.add(comp, BorderLayout.CENTER);
  }

}
