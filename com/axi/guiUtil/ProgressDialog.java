package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.axi.util.*;

/**
 * @author Erica Wheatcroft
 */
public class ProgressDialog extends JDialog
{
  private JProgressBar _progressBar = new JProgressBar();
  private JLabel _messageToDisplayLabel = new JLabel();
  private JButton _cancelButton = new JButton();
  private JButton _closeButton = new JButton();
  private JPanel _buttonPanel = new JPanel(new FlowLayout());

  private int _minimumProgressBarValue = 0;
  private int _maximumProgressBarValue = 0;
  private String _operationCompletedMessage = "";
  private String _progressText = "";
  private boolean _modal = true;
  private boolean _createCancelButton = false;
  private ActionListener _cancelActionListener = null;

  private int _currentMaxMessageLength = 0;
  private boolean _userOverideSize = false;


  /**
   * @author Erica Wheatcroft
   */
  public ProgressDialog(Frame parent,
                        String dialogTitle,
                        String messageToDisplay,
                        String operationCompleteMessage,
                        String closeDialogButtonText,
                        String cancelOperationButtonText,
                        String progressText,
                        int min,
                        int max,
                        boolean modal)
  {
    super(parent, modal);

    Assert.expect(dialogTitle != null, "title of dialog is null");
    Assert.expect(messageToDisplay != null, "Message to display is null");
    Assert.expect(operationCompleteMessage != null, "operation complete message is null");
    Assert.expect(closeDialogButtonText != null, "close dialog button text is null");
    Assert.expect(cancelOperationButtonText != null, "cancel operation button text is null");
    Assert.expect(progressText != null, "progress text is null");

    _operationCompletedMessage = operationCompleteMessage;
    _closeButton.setText(closeDialogButtonText);
    _progressText = progressText;
    _modal = modal;
    setTitle(dialogTitle);
    _messageToDisplayLabel.setText(messageToDisplay);
    _cancelButton.setText(cancelOperationButtonText);
    _minimumProgressBarValue = min;
    _maximumProgressBarValue = max;
    setModal(_modal);
    _createCancelButton = true;
    createDialog();
  }

  /**
   * @author Erica Wheatcroft
   */
  public ProgressDialog(Frame parent,
                        String dialogTitle,
                        String messageToDisplay,
                        String operationCompleteMessage,
                        String closeDialogButtonText,
                        String progressText,
                        int min,
                        int max,
                        boolean modal)
  {
    super(parent, modal);
    Assert.expect(dialogTitle != null, "title of dialog is null");
    Assert.expect(messageToDisplay != null, "Message to display is null");
    Assert.expect(operationCompleteMessage != null, "operation complete message is null");
    Assert.expect(closeDialogButtonText != null, "close dialog button text is null");
    Assert.expect(progressText != null, "progress text is null");

    _operationCompletedMessage = operationCompleteMessage;
    _closeButton.setText(closeDialogButtonText);
    _progressText = progressText;
    _modal = modal;
    setTitle(dialogTitle);
    _messageToDisplayLabel.setText(messageToDisplay);
    _minimumProgressBarValue = min;
    _maximumProgressBarValue = max;
    setModal(_modal);
    _createCancelButton = false;
    createDialog();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setSize(Dimension dim)
  {
    Assert.expect(dim != null);
    super.setSize(dim);
    _userOverideSize = true;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createDialog()
  {

    getContentPane().setLayout(new BorderLayout(0,0));

    JPanel labelPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    labelPanel.add(_messageToDisplayLabel);
    labelPanel.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 15));

    JPanel progressBarPanel = new JPanel(new GridLayout());
    progressBarPanel.setBorder(BorderFactory.createEmptyBorder(15,15,15,15));
    progressBarPanel.add(_progressBar, null);
    _progressBar.setIndeterminate(false);
    _progressBar.setMaximum(_maximumProgressBarValue);
    _progressBar.setMinimum(_minimumProgressBarValue);
    _progressBar.setOrientation(JProgressBar.HORIZONTAL);
    _progressBar.setStringPainted(false);
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 15));
    if (_createCancelButton)
      _buttonPanel.add(_cancelButton);

    // do this so if there's no cancel button, the progress bar get the focus
    // if we don't have something get the focus, then keystrokes are NOT consumed by this modal
    // dialog, which causes grief if a main ui menu keyboard shortcut is attempted.
    if (_createCancelButton == false)
      _progressBar.setFocusable(true);

    // since we do not want the user to X out of the dialog set the default close operation.
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

    getContentPane().add(progressBarPanel, BorderLayout.CENTER);
    getContentPane().add(_buttonPanel, BorderLayout.SOUTH);
    getContentPane().add(labelPanel, BorderLayout.NORTH);

    setResizable(true);

    _closeButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });

    if(_createCancelButton)
    {
      _cancelButton.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Assert.expect(_cancelActionListener != null, "Listener is null");
          _cancelButton.setEnabled(false);
          _cancelActionListener.actionPerformed(e);

        }
      });
    }
    _currentMaxMessageLength = 0;
  }

  /**
   * This method should be called when the progress bar should be updated.
   * The message is "<current> <progressText> <max>" as in "N out of M"
   * @author Erica Wheatcroft
   */
  public void updateProgressBar(final int currentValue)
  {
    Assert.expect(currentValue >= _minimumProgressBarValue, "value passed in is less than minimum progress bar value ");
    Assert.expect(currentValue <= _maximumProgressBarValue,
                  "value passed in is greater than maximum progress bar value Max:" + _maximumProgressBarValue + " current: " + currentValue );


    if (SwingUtilities.isEventDispatchThread())
      handleProgressBarUpdate(currentValue);
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          handleProgressBarUpdate(currentValue);
        }
      });
    }
  }



  /**
   * @author Erica Wheatcroft
   */
  private void finish()
  {
    // the user has completed the process.. update the dialog and change the text of the button
    updateMessage(_operationCompletedMessage);

    _buttonPanel.remove(_cancelButton);
    _buttonPanel.add(_closeButton);

    // re-enable the X button since the process is finished.
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

    validate();
    repaint();
  }

  /**
   * This method should be called when the progress bar should be updated.
   * The text in the progress bar is "<current> <progressText>" as in "12 Percent Complete"
   * @author Erica Wheatcroft
   */
  public void updateProgressBarPrecent(final int currentValue)
  {
    Assert.expect(currentValue >= _minimumProgressBarValue, "value passed in is less than minimum progress bar value ");
    Assert.expect(currentValue <= _maximumProgressBarValue,
                       "value passed in is greater than maximum progress bar value Max:" + _maximumProgressBarValue + " current: " + currentValue );

    if (SwingUtilities.isEventDispatchThread())
      handleProgressBarPercentUpdate(currentValue);
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          handleProgressBarPercentUpdate(currentValue);
        }
      });
    }
  }

  /**
   * This method should be called when the progress bar should be updated.
   * The text in the progress bar is "<current> <progressText>" as in "12 Percent Complete"
   * The dialog text is updated with <message>.
   * @author Erica Wheatcroft
   */
  public void updateProgressBarPrecent(final int currentValue, final String message)
  {
    Assert.expect(currentValue >= _minimumProgressBarValue, "value passed in is less than minimum progress bar value ");
    Assert.expect(currentValue <= _maximumProgressBarValue,
                       "value passed in is greater than maximum progress bar value Max:" + _maximumProgressBarValue + " current: " + currentValue );

    if(SwingUtilities.isEventDispatchThread())
      handleProgressBarPercentUpdate(currentValue, message);
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          handleProgressBarPercentUpdate(currentValue, message);
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleProgressBarUpdate(int currentValue)
  {
    _progressBar.setStringPainted(true);
    _progressBar.setValue(currentValue);
    _progressBar.setString(currentValue + " " + _progressText + " " + _maximumProgressBarValue);

    if (currentValue == _maximumProgressBarValue)
      finish();
  }

  /**
   * This method should be called when the progress bar should be updated.
   * The text in the progress bar is "<current> <progressText>" as in "12 / 24"
   * The dialog text is updated with <message>.
   * @author Andy Mechtenberg
   */
  public void updateProgressBar(final int currentValue, final String message)
  {
    Assert.expect(currentValue >= _minimumProgressBarValue, "value passed in is less than minimum progress bar value ");
    Assert.expect(currentValue <= _maximumProgressBarValue,
                       "value passed in is greater than maximum progress bar value Max:" + _maximumProgressBarValue + " current: " + currentValue );

    if(SwingUtilities.isEventDispatchThread())
      handleProgressBarPercentUpdate(currentValue, message);
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          handleProgressBarUpdate(currentValue, message);
        }
      });
    }
  }


  /**
   * @author Andy Mechtenberg
   */
  private void handleProgressBarUpdate(int currentValue, String message)
  {
    _progressBar.setStringPainted(true);
    _progressBar.setValue(currentValue);
    _progressBar.setString(currentValue + " " + _progressText + " " + _maximumProgressBarValue);

    updateMessage(message);

    if (currentValue == _maximumProgressBarValue)
      finish();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleProgressBarPercentUpdate(int currentValue, String message)
  {
    _progressBar.setStringPainted(true);
    _progressBar.setValue(currentValue);
    _progressBar.setString(currentValue + " " + _progressText);

    updateMessage(message);

    if (currentValue == _maximumProgressBarValue)
      finish();
  }

  /**
   * If the new message is longer than any previous messages, we want the dialog to grow
   * We should never let the dialog shrink, as that looks weird.
   * @author Andy Mechtenberg
   */
  private void updateMessage(String message)
  {
    FontMetrics fm = _messageToDisplayLabel.getFontMetrics(_messageToDisplayLabel.getFont());
    int newLength = fm.stringWidth(message);
    if (_currentMaxMessageLength == 0)
    {
      _currentMaxMessageLength = fm.stringWidth(_messageToDisplayLabel.getText());
    }
    _messageToDisplayLabel.setText(message);

    if (_userOverideSize)
      return;

    if (newLength > _currentMaxMessageLength)
    {
      pack();
      _currentMaxMessageLength = newLength;
    }

  }
  /**
   * @author Erica Wheatcroft
   */
  private void handleProgressBarPercentUpdate(int currentValue)
  {
    _progressBar.setStringPainted(true);
    _progressBar.setValue(currentValue);
    _progressBar.setString(currentValue + " " + _progressText);

    if (currentValue == _maximumProgressBarValue)
      finish();
  }


  /**
   * Add an ActionListener to the cancel button.  Adding a listener
   * is the only way the parent compnent will know that the button is pressed.
   * The Escape key is also tied to this ActionListener so that hitting Escape
   * is the same as pressing the cancel button.
   * @param   listener - ActionListener to attach to the cancel button.
   * @author Erica Wheatcroft
   */
  public void addCancelActionListener(ActionListener listener)
  {
    Assert.expect(listener != null, "Listener is null");
    _cancelActionListener = listener;
  }

  /**
   * @param enabled boolean
   * @author Wei Chin, Chong
   */
  public void setCancelButtonEnabled(boolean enabled)
  {
    _cancelButton.setEnabled(enabled);
  }

  /**
   * To prevent annoying pop-ups
   * @author Seng Yew, Lim
   */
  @Override
  public void toFront() {
    // Do nothing.
  }

}
