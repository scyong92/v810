package com.axi.guiUtil;

import javax.swing.*;
import java.awt.*;

/**
 * This class provided a Button under the XP look and feel to allow it
 * colorize the background.  The default XP setBackground only sets the
 * background color for a small border around the outside of the button, not
 * the interior of the button itself.
 * This class is only necessary when using XP, and only when you want to set
 * custom background colors.  The rendering (done by ColoredButtonUI) is not
 * a perfect match for the XP system rendering, but a close approximation.
 * @author Andy Mechtenberg
 */
public class ColoredButton extends JButton
{
  private Color _enabledButtonBackgroundColor = new Color(244, 244, 240);
  private Color _disabledButtonBackgroundColor = new Color(245, 244, 234);  // not used, but save for reference

  /**
   * @author Andy Mechtenberg
   */
  public ColoredButton()
  {
    if (UIManager.getLookAndFeel().getDescription().startsWith("Nimbus"))
    {
      ColoredButtonUI colorXPButtonUI = new ColoredButtonUI();
      setUI(colorXPButtonUI);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void removeCustomBackground()
  {
    setBackground(_enabledButtonBackgroundColor);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setCustomBackground(Color backgroundColor)
  {
    setBackground(backgroundColor);
  }
}
