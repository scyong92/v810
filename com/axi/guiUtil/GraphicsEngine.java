package com.axi.guiUtil;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.util.image.*;
import java.util.List;

/**
 * The GraphicsEngine class provides functionality to draw Shapes, Text and Images on the screen.
 * It uses the Renderer, TextRenderer, and ImageBufferRenderer classes to do the actual drawing.
 * It provides methods to add these Renderers to any number of layers.  Each layer has a different z height.
 * The higher the layer number the higher up the z height.
 *
 * All Renderers added to this class must use the same units and have a common origin before
 * being added to the GraphicsEngine.
 *
 * This class provides functionality to zoom in and out, drag, select one or many
 * Renderers, flip the image, cause one or more Renderers to flash between two colors,
 * find a Renderer with crosshairs, highlight certain renderers, and select many Renderers at once.
 *
 * This class also allows you to set thresholds to control when Renderers are visible (at what
 * scale) or when they are created (only at a certain scale and only the ones that are currently
 * on the screen).  This helps to control memory use and screen clutter.  See the
 * setVisibilityThreshold() and setCreationThreshold() methods.
 *
 * A MouseEventPanel will automatically be put on the top layer to handle mouse events.
 * A crossHairRendererPanel will automatically be places one layer below the MouseEventPanel
 * to draw crossHairs.
 *
 * When a Renderer is selected with a mouse click, any Observers of this GraphicsEngine class
 * (use getSelectedRendererObservable.addObserver())) will be sent a reference to all the Renderers
 * that were below that click.  If more than one Renderer is below the mouse click, all Renderers will
 * come back in order from the highest layer to the lowest.  It is up to the Observer to perform some
 * action based on the Renderer selection (like changing the Renderers color).
 *
 * NOTE that any Renderer instance should only be put into the GraphicsEngine class once, and only in
 * one GraphicsEngine at a time or undefined behavior will result.
 *
 * A typical use of this class would involve something like this:
 *
 * <pre>
 *   GraphicsEngine graphicsEngine = new GraphicsEngine();
 *
 *   // create Renderers and add them to the layers you have already added
 *   MyRenderer myRenderer = new MyRenderer();
 *   // add myRenderer (derived from Renderer) to layer 1
 *   graphicsEngine.addRenderer(1, myRenderer);
 *   ... (add more layers here)
 *
 *   // set up the colors of each layer after all Renderers are added
 *   // The Renderer colors will be changed based on what layer they are
 *   // on so set colors AFTER all Renderers are added
 *   graphicsEngine.setForeground(1, Color.BLUE);
 *   graphicsEngine.setForeground(2, Color.RED);
 *
 *   // now set the visibility
 *   // set layer 1 to be visible and layer 2 to not be visible
 *   graphicsEngine.setVisible(1, true);
 *   graphicsEngine.setVisible(2, false);
 *
 *   // tell it how to interpret x and y values of the Renderers data
 *   setAxisInterpretation(true, false);
 *
 *   // fit the graphics to the screen
 *   graphicsEngine.fitGraphicsToScreen();
 *
 * </pre>
 *
 * @see Renderer
 * @see TextRenderer
 * @see BufferedImageRenderer
 * @see CrossHairRenderer
 * @see MouseEventPanel
 * @see LayerColorEnum
 *
 * @author Bill Darbie
 */
public class GraphicsEngine extends JPanel implements ComponentListener
{
  // this is the border around the Panel where nothing will be drawn.
  private static final int _PIXEL_THRESHOLD = 5;

  private int _pixelBorderSize = 0;

  private Map<Integer, RendererPanel> _layerNumberToRendererPanelMap = new TreeMap<Integer, RendererPanel>();
  private Map<Integer, RendererCreator> _layerNumberToRendererCreatorMap = new TreeMap<Integer, RendererCreator>();
  private Map<Integer, Double> _layerNumberToVisibilityThresholdMap = new TreeMap<Integer, Double>();
  private Map<Integer, Double> _layerNumberToCreationThresholdMap = new TreeMap<Integer, Double>();
  private Map<Integer, AffineTransform> _layerNumberToInitialTransformMap = new HashMap<Integer, AffineTransform>();

  private Map<RendererPanel, Color> _rendererPanelToFlashColorMap = new HashMap<RendererPanel, Color>();
  private Map<RendererPanel, Color> _rendererPanelToOrigColorMap = new HashMap<RendererPanel, Color>();
  private Map<Renderer, Color> _rendererToFlashColorMap = new HashMap<Renderer, Color>();
  private Map<Renderer, Color> _rendererToOrigColorMap = new HashMap<Renderer, Color>();

  private Map<Renderer, Color> _selectedRendererToOrigColorMap = new HashMap<Renderer, Color>();
  private Map<Renderer, Color> _highlightedRendererToOrigColorMap = new HashMap<Renderer, Color>();
  private Map<Renderer, Integer> _rendererToLayerNumberMap = new HashMap<Renderer, Integer>();
  private Set<Integer> _layersToRespondToLeftMouseClickSet = new TreeSet<Integer>();

  private Collection<Integer> _layersToUseWhenFittingToScreen = new ArrayList<Integer>();
  private Collection<Integer> _layersToUseWhenDraggingGraphics = new ArrayList<Integer>();

  // thread used to make Renderers flash
  private WorkerThread _flashWorkerThread = new WorkerThread("Graphics engine color flashing thread");
  private boolean _rendererCopyComplete = true;
  private Object _rendererMapsLock = new Object();
  private boolean _enableFlashing = false;
  private boolean _flashing = false;
  private Object _flashingLock = new Object();

  // the pane that gives us layers at different z heights
  private JLayeredPane _layeredPane = new JLayeredPane();

  // once the transform() method is called this flag will be true
  private boolean _transformApplied = false;

  // true if the screen is showing a view from the top looking down, false if looking from
  // the bottom up
  private boolean _isViewedFromTop = true;

  // the panel used to handle all the mouse events (used for selecting components, dragging, etc)
  private MouseEventPanel _mouseEventPanel;
  // the layer that the mouseEventPanel is on
  private Integer _mouseLayerNumber;
  // the panel used to draw cross hairs to locate things
  private RendererPanel _crossHairRendererPanel;
  private RendererPanel _pspCrossHairRendererPanel;
  // the layer number for the cross hair panel
  // These layer numbers are very high so they don't conflict with adding in
  // new layers as renderers are added in dynamically after the initial transform
  private final int _mouseLayer = Integer.MAX_VALUE - 2;
  private final int _crossHairLayer = Integer.MAX_VALUE - 1;
  private Integer _crossHairLayerNumber;
  private CrossHairRenderer _crossHairRenderer;
  private Renderer _rendererToPlaceCrossHairsOn;

  // a collection of the currently selected renderers
  private boolean _multipleSelectOn = false; // flag saved for determining if we are to add to the list of selected renderers, or reset it
  private Set<Renderer> _currentlySelectedRendererSet = new HashSet<Renderer>();
  private Set<Renderer> _currentlyHighlightedRendererSet = new HashSet<Renderer>();
  private Set<Renderer> _currentlySelectedComponentRendererSet = new HashSet<Renderer>();

  // everytime a mouse click happens, notify any Observers with the mouse event that was generated
  private MouseClickObservable _mouseClickObservable;

  // every time a Renderer is selected the Observable will update any Observers to
  // tell them it was selected
  private SelectedRendererObservable _selectedRendererObservable;
  private SelectedOpticalRegionObservable _selectedOpticalRegionRendererObservable;
  private DragOpticalRegionObservable _dragOpticalRegionObservable;
  private UnselectedOpticalRegionObservable _unselectedOpticalRegionRendererObservable;
  private UnselectedDragOpticalRegionObservable _unselectedDragOpticalRegionRendererObservable;
  private GraphicsEngineObservable _graphicsEngineObservable;
  private VisibleRectangleObservable _visibleRectangleObservable;
  private DisplayPadNameObservable _displayPadNameObservable;
  private ShowNoLoadComponentObservable _showNoLoadComponentObservable;
  private SelectedAreaObservable _selectedAreaObservable;
  private ShowUntestableAreaObservable _showUntestableAreaObservable;
  
  // the rectangle that bounds all renderers (sometimes just the visible layers, sometimes all layers)
  // depending on the last calcCurrentBounds() call
  private Rectangle2D _maxBounds = null;

  // _transformFromLastMark contains the transform data from
  // all the cumulative transform() method calls called since the
  // last mark() call
  private AffineTransform _transformFromLastMark = new AffineTransform();

  // This transform tracks changes from last time the graphics were fit to screen
  private AffineTransform _transformFromFitToScreen = new AffineTransform();

  // This transform tracks changes from the call to ApplyAxisTransform for each renderer.
  private AffineTransform _transformFromAxisTransform = new AffineTransform();
  private double _axisTransformXTranslation = 0;
  private double _axisTransformYTranslation = 0;

  // _transformFromRendererCoords contains the cumulative scale and translate functions
  // to get from the original Renderer data to the current state
  // Note:  It includes the default axis transformation which works with
  // shapes but may not be the correct transformation to use for all renderer types.
  // Use applyAxisTransform() for the renderer followed by an application of
  // _transformFromAxisTransform.
  private AffineTransform _transformFromRendererCoords = new AffineTransform();

  // this is set to true once the setAxisInterpretation() method is called
  private boolean _setAxis = false;
  // flags that determine how to interpret the x and y axis data from the Renderers
  private boolean _positiveXisToTheRightOfTheOrigin = true;
  private boolean _positiveYisBelowTheOrigin = true;

  private boolean _shouldGraphicsBeCentered = false;
  private boolean _shouldGraphicsBeFitToScreen = true;
  private boolean _isDraggingGraphics = false;
  private boolean _allowDragInY = true;
  private double _minZoomFactor = 1.0;
  private boolean _selectOnRightClick = true;  // determines whether a right click will also select renderers

  /** @todo pwl : George, Measuring functionality doesn't belong in this object. */
  private double _measurementFactor = 1.0;
  private int _measurementUnitDecimalPlaces;
  private String _measurementUnitsString;

  // flag to let the RendereCreator know whether the renderer creation threshold was crossed or not
  // renderers on layers with creation threshold should be removed when this is true.
  private Map<Integer, Boolean> _layerNumberToCreationThresholdBooleanMap = new TreeMap<Integer, Boolean>();

  private boolean _isZoomEnabled = true;
  private boolean _isDragRegionMode = false;
  private boolean _isDragSelectedRegionMode = false;
  private int _xInPixels = 0;
  private int _yInPixels = 0;
  private int _widthInPixels = 0;
  private int _heightInPixels = 0;
  private int _resizedXInPixels = 0;
  private int _resizedYInPixels = 0;
  private int _resizedWidthInPixels = 0;
  private int _resizedHeightInPixels = 0;
  private int _unselectedXInPixels = 0;
  private int _unselectedYInPixels = 0;
  private int _unselectedWidthInPixels = 0;
  private int _unselectedHeightInPixels = 0;
  private int _widthInNano = 0;
  private int _heightInNano = 0;
  private int _crossHairX = 0;
  private int _crossHairY = 0;
  private Rectangle2D _rectangle = null;
  private Rectangle _unselectedRectangleInPixels = null;
  private Rectangle _rectangleInPixels = null;
  private Rectangle _resizedRectangleInPixels = null;
  private Rectangle _opticalCameraRectangleInPixel = null;
  private int _maxRegionXCoordinate = 0;
  private int _minRegionXCoordinate = 0;
  private int _maxRegionYCoordinate = 0;
  private int _minRegionYCoordinate = 0;
  private int _regionWidth = 0;
  private int _regionHeight = 0;
  private Point2D _upperLeftCoordinate = null;
  private java.util.List<Point> _opticalRegionPointList = new ArrayList<Point>();
  private int _updateCount = 0;
  private boolean _isSelectedRegionInComboboxAvailable = false;
  private boolean _isDragRegionModeAvailable = false;
  private boolean _isCheckBoxChecked = false;
  private boolean _isDragOpticalRegionButtonToggle = false;
  private boolean _isMouseRightClickMode = false;
  private boolean _isControlKeyPressMode = false;
  private Point _mouseRightClickCoordinate = new Point();
  private double _fitGraphicsToScreenWhenWindowIsResizedNewScale = 0;
  private Rectangle _rect;
  private boolean _isFirstDrag = false;
  private java.util.List<Rectangle2D> _alignmentOpticalRegionList = new ArrayList<Rectangle2D>();
  private java.util.List<Rectangle> _alignmentOpticalRegionInPixelList = new ArrayList<Rectangle>();
  private boolean _isZoom = false;
  private boolean _isWindowResized = false;
  private double _canvasWidthToHeightRatio = 0;
  
  private boolean _isCrossHairEnable = true;  // Kee Chin Seong - Cross Hair visibility.
 
  /**
   * @author Andy Mechtenberg
   */
  public GraphicsEngine(int pixelBorderSize)
  {
    Assert.expect(pixelBorderSize >= 0);

    _pixelBorderSize = pixelBorderSize;

    _transformFromRendererCoords.setToIdentity();
    _transformFromAxisTransform.setToIdentity();
    _transformFromLastMark.setToIdentity();
    _selectedRendererObservable = new SelectedRendererObservable();
    _selectedOpticalRegionRendererObservable = new SelectedOpticalRegionObservable();
    _unselectedOpticalRegionRendererObservable = new UnselectedOpticalRegionObservable();
    _unselectedDragOpticalRegionRendererObservable = new UnselectedDragOpticalRegionObservable();
    _dragOpticalRegionObservable = new DragOpticalRegionObservable();
    _mouseClickObservable = new MouseClickObservable();
    _graphicsEngineObservable = new GraphicsEngineObservable();
    _visibleRectangleObservable = new VisibleRectangleObservable();
    _displayPadNameObservable = new DisplayPadNameObservable();
    _selectedAreaObservable = new SelectedAreaObservable();
    
    _showNoLoadComponentObservable = new ShowNoLoadComponentObservable();
    _showUntestableAreaObservable = new ShowUntestableAreaObservable();
    
    // all mouse clicks will cause the Renderer to display in white
    final GraphicsEngine thisEngine = this;
    addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent mouseEvent)
      {
//        int modifiers = mouseEvent.getModifiers();
//        if (modifiers == MouseEvent.BUTTON1_MASK)
//        {
//          thisEngine.leftMouseClicked(mouseEvent);
//        }
        thisEngine.mouseClicked(mouseEvent);
      }
    });

    jbInit();

    // setting a custom focus traversal policy so that the graphics engine is NOT considered part
    // of normal component focus policy.  These things never need focus, and with a large panel, there
    // can be a lot of them and it can take a while to sort them all.
    setFocusCycleRoot(true);
    this.setFocusTraversalPolicy(new FocusTraversalPolicy()
    {
      public Component getComponentAfter(Container focusCycleRoot, Component aComponent)
      {
        return null;
      }

      public Component getComponentBefore(Container focusCycleRoot, Component aComponent)
      {
        return null;
      }

      public Component getDefaultComponent(Container focusCycleRoot)
      {
        return null;
      }

      public Component getLastComponent(Container focusCycleRoot)
      {
        return null;
      }

      public Component getFirstComponent(Container focusCycleRoot)
      {
        return null;
      }
    });
  }

  /**
   * This will change the size of the border (in pixels).  A call to
   * fitGraphicsToScreen must be made to see the effect of this change.
   * @author Andy Mechtenberg
   */
  public void setPixelBorderSize(int pixelBorderSize)
  {
    Assert.expect(pixelBorderSize >= 0);

    _pixelBorderSize = pixelBorderSize;
  }


  /**
   * @return the Observable the is responsible for sending updates when ever one or more Renderers is
   * selected on the screen.
   * @author Bill Darbie
   */
  public SelectedRendererObservable getSelectedRendererObservable()
  {
    return _selectedRendererObservable;
  }
  
   /**
   * @return the Observable the is responsible for sending updates when ever one or more Renderers is
   * selected on the screen.
   * @author Jack Hwee
   */
  public SelectedOpticalRegionObservable getSelectedOpticalRegionRendererObservable()
  {
    return _selectedOpticalRegionRendererObservable;
  }
  
   /**
   * @return the Observable the is responsible for sending updates when ever one or more Renderers is
   * selected on the screen.
   * @author Jack Hwee
   */
  public DragOpticalRegionObservable getDragOpticalRegionObservable()
  {
    return _dragOpticalRegionObservable;
  }
  
    /**
   * @return the Observable the is responsible for sending updates when ever one or more Renderers is
   * selected on the screen.
   * @author Jack Hwee
   */
  public UnselectedOpticalRegionObservable getUnselectedOpticalRegionRendererObservable()
  {
    return _unselectedOpticalRegionRendererObservable;
  }
  
    /**
   * @return the Observable the is responsible for sending updates when ever one or more Renderers is
   * selected on the screen.
   * @author Jack Hwee
   */
  public UnselectedDragOpticalRegionObservable getUnselectedDragOpticalRegionRendererObservable()
  {
    return _unselectedDragOpticalRegionRendererObservable;
  }

  /**
   * @return the Observable the is responsible for sending updates when ever a left mouse click event occurs
   * @author Bill Darbie
   */
  public MouseClickObservable getMouseClickObservable()
  {
    return _mouseClickObservable;
  }

  /**
   * @return the Observable the is responsible for sending updates when ever a graphics change happens
   * @author Andy Mechtenberg
   */
  public GraphicsEngineObservable getGraphicsEngineObservable()
  {
    return _graphicsEngineObservable;
  }

  /**
   * @author George A. David
   */
  public VisibleRectangleObservable getVisibleRectangleObservable()
  {
    Assert.expect(_visibleRectangleObservable != null);
    return _visibleRectangleObservable;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public DisplayPadNameObservable getDisplayPadNameObservable()
  {
      return _displayPadNameObservable;
  }
  
  /*
  * @author Kee Chin Seong
  */
  public ShowNoLoadComponentObservable getShowNoLoadComponentObservable()
  {
      return _showNoLoadComponentObservable;
  }
  
  /**
   * @author Janan Wong
   */
  public ShowUntestableAreaObservable getShowUntestableAreaObservable()
  {
    return _showUntestableAreaObservable;
  }

  /**
   * @return the RendererPanel for the layerNumber passed in.  If one does
   * not exist for the layerNumber, create one and return it.
   * @author Bill Darbie
   */
  private RendererPanel getRendererPanel(int layerNumber)
  {
    RendererPanel rendererPanel = _layerNumberToRendererPanelMap.get(layerNumber);
    if (rendererPanel == null)
    {
      Dimension dim = getSize();
      rendererPanel = new RendererPanel(Color.RED);
      rendererPanel.setSize(dim);
      _layerNumberToRendererPanelMap.put(layerNumber, rendererPanel);
      _layeredPane.add(rendererPanel, new Integer(layerNumber));
    }

    Assert.expect(rendererPanel != null);
    return rendererPanel;
  }

  /**
   * Add a renderer to the specified layer number
   * @author Bill Darbie
   */
  public void addRenderer(int layerNumber, Renderer renderer)
  {
    Assert.expect(_rendererToLayerNumberMap != null);
    Assert.expect(renderer != null);

    renderer.setGraphicsEngine(this);

    RendererPanel rendererPanel = getRendererPanel(layerNumber);
    if (_transformApplied)
    {
      // if the graphics have already been fit to the screen we need to
      // apply the correct transform to the Renderer
      placeInCurrentGraphics(layerNumber, renderer);
    }

    // now add the Renderer
    rendererPanel.add(renderer);
    renderer.setForeground(rendererPanel.getForeground());
    _rendererToLayerNumberMap.put(renderer, new Integer(layerNumber));
    
    repaintRendererThatHasBeenAddedIn(renderer);
  }

  /**
   * Add a Collection of Renderers to the specified layer number
   * @author Bill Darbie
   */
  public void addRenderers(int layerNumber, Collection<Renderer> renderers)
  {
    Assert.expect(renderers != null);

    for (Renderer renderer : renderers)
    {
      addRenderer(layerNumber, renderer);
    }
  }

  /**
   * Get a Collection of all the Renderers on the specified layer
   * @author Bill Darbie
   */
  public Collection<Renderer> getRenderers(int layerNumber)
  {
    RendererPanel rendererPanel = getRendererPanel(layerNumber);

    return rendererPanel.getRenderers();
  }

  /**
   * Get a Collection of all the Renderers on the specified layers
   * @author Bill Darbie
   */
  public Collection<Renderer> getRenderers(Collection<Integer> layerNumbers)
  {
    Assert.expect(layerNumbers != null);

    Collection<Renderer> renderers = new ArrayList<Renderer>();
    for (int layerNumber : layerNumbers)
    {
      renderers.addAll(getRenderers(layerNumber));
    }

    return renderers;
  }

  /**
   * Remove a renderer from whatever layer it is on and all other places this might be referenced
   * @author Bill Darbie
   * @author Patrick Lacz
   */
  public void removeRenderer(Renderer renderer)
  {
    Assert.expect(renderer != null);
    Assert.expect(_rendererToLayerNumberMap != null);
    Assert.expect(_layerNumberToRendererPanelMap != null);
    
    Integer layerNumberInteger = _rendererToLayerNumberMap.get(renderer);
    Assert.expect(layerNumberInteger != null);

    RendererPanel rendererPanel = _layerNumberToRendererPanelMap.get(layerNumberInteger);
    Assert.expect(rendererPanel != null);

    rendererPanel.remove(renderer);
    _rendererToLayerNumberMap.remove(renderer);
    synchronized (_rendererMapsLock)
    {
      _rendererToFlashColorMap.remove(renderer);
      _rendererToOrigColorMap.remove(renderer);
      _selectedRendererToOrigColorMap.remove(renderer);
      _highlightedRendererToOrigColorMap.remove(renderer);
    }

    repaintRendererThatHasBeenRemoved(renderer);
  }

  /**
   * Remove a renderer from whatever layer it is on.
   * @author Bill Darbie
   * @author Patrick Lacz
   */
  private void removeRendererFromBeingVisible(Renderer renderer)
  {
    Assert.expect(renderer != null);
    Assert.expect(_rendererToLayerNumberMap != null);
    Assert.expect(_layerNumberToRendererPanelMap != null);

    Integer layerNumberInteger = _rendererToLayerNumberMap.get(renderer);
    Assert.expect(layerNumberInteger != null);

    RendererPanel rendererPanel = _layerNumberToRendererPanelMap.get(layerNumberInteger);
    Assert.expect(rendererPanel != null);

    rendererPanel.remove(renderer);
    repaintRendererThatHasBeenRemoved(renderer);
  }

  /**
   * Remove a Collection of Renderers from whatever layer it is on.
   * @author Bill Darbie
   */
  public void removeRenderers(Collection<? extends Renderer> renderers)
  {
    Assert.expect(renderers != null);
    for (Renderer renderer : renderers)
    {
      if(renderer.isValid())
        removeRenderer(renderer);
    }
  }

  /**
   * @author George A. David
   */
  public boolean layerExists(int layerNumber)
  {
    Assert.expect(_layerNumberToRendererPanelMap != null);
    return _layerNumberToRendererPanelMap.containsKey(layerNumber);
  }

  /**
   * Remove all renderers from a layer.
   * @author Bill Darbie
   * @author Laura Cormos
   * @author Patrick Lacz
   */
  public void removeRenderers(int layerNumber)
  {
    Assert.expect(_layerNumberToRendererPanelMap != null);
    if (_layerNumberToRendererPanelMap.containsKey(layerNumber))
    {
      RendererPanel rendererPanel = _layerNumberToRendererPanelMap.get(layerNumber);
      Assert.expect(rendererPanel != null);
      Component[] components = rendererPanel.getComponents();
      for (int i = 0; i < components.length; ++i)
      {
        Renderer renderer = (Renderer)components[i];
        removeRenderer(renderer);
      }
    }
  }

  /**
   * Remove all renderers from a layer.
   * @author Bill Darbie
   * @author Laura Cormos
   * @author Patrick Lacz
   */
  private void removeRenderersFromBeingVisible(int layerNumber)
  {
    Assert.expect(_layerNumberToRendererPanelMap != null);
    if (_layerNumberToRendererPanelMap.containsKey(layerNumber))
    {
      RendererPanel rendererPanel = _layerNumberToRendererPanelMap.get(layerNumber);
      Assert.expect(rendererPanel != null);
      Component[] components = rendererPanel.getComponents();
      for (int i = 0; i < components.length; ++i)
      {
        Renderer renderer = (Renderer)components[i];
        removeRendererFromBeingVisible(renderer);
      }
    }
  }

  /**
   * @author George A. David
   */
  public void removeRenderers(java.util.List<Integer> layers)
  {
    for(int layer : layers)
      removeRenderers(layer);
    _maxBounds = null;
  }

  /**
   * Remove the entire layer
   * @author Bill Darbie
   */
  private void removeLayerForCrossHairAndMouse(int layerNumber)
  {
    RendererPanel rendererPanel = getRendererPanel(layerNumber);
    Assert.expect(rendererPanel != null);
    _layeredPane.remove(rendererPanel);
    _layerNumberToRendererPanelMap.remove(new Integer(layerNumber));

    validate();
  }

  /**
   * Remove the entire layer
   * @author Bill Darbie
   */
  public void removeLayer(int layerNumber)
  {
    // first, remove all the renderers from this layer
    removeRenderers(layerNumber);

    // then, remove the layer from anyplace it's referenced
    // after this, it's GONE!  It must be re-created from scratch
    RendererPanel rendererPanel = getRendererPanel(layerNumber);
    Assert.expect(rendererPanel != null);
    _layeredPane.remove(rendererPanel);

    _layerNumberToRendererCreatorMap.remove(layerNumber);
    _layerNumberToVisibilityThresholdMap.remove(layerNumber);
    _layerNumberToCreationThresholdMap.remove(layerNumber);
    _layersToRespondToLeftMouseClickSet.remove(layerNumber);
    _layerNumberToInitialTransformMap.remove(layerNumber);
    _layersToUseWhenFittingToScreen.remove(layerNumber);
    _layersToUseWhenDraggingGraphics.remove(layerNumber);

    synchronized (_rendererMapsLock)
    {
      _rendererPanelToFlashColorMap.remove(rendererPanel);
      _rendererPanelToOrigColorMap.remove(rendererPanel);
    }
    _layerNumberToRendererPanelMap.remove(layerNumber);

    validate();
  }

  /**
   * @author Ngie Xing
   */
  public void removeRendererFromCurrentlySelectedRendererSet(Renderer renderer)
  {
    _currentlySelectedRendererSet.remove(renderer);
  }
  /**
   * @author Ngie Xing
   */
  public void removeRendererFromCurrentlySelectedRendererSet(Set<Renderer> renderer)
  {
    _currentlySelectedRendererSet.removeAll(renderer);
  }

  /**
   * Swap all the Renderers between two layers.
   * @author Bill Darbie
   */
  public void swapLayers(int layer1, int layer2)
  {
    Integer layerInt1 = new Integer(layer1);
    Integer layerInt2 = new Integer(layer2);

    RendererPanel rendererPanel1 = (RendererPanel)_layerNumberToRendererPanelMap.get(layerInt1);
    Assert.expect(rendererPanel1 != null);
    RendererPanel rendererPanel2 = (RendererPanel)_layerNumberToRendererPanelMap.get(layerInt2);
    Assert.expect(rendererPanel2 != null);

    // update the _rendererToLayerNumberMap with the new layer numbers for each renderer found in the swapped layers
    Collection<Renderer> allRenderersForLayer1 = getRenderers(layer1);
    Collection<Renderer> allRenderersForLayer2 = getRenderers(layer2);
    for (Renderer renderer : allRenderersForLayer1)
    {
      Integer prev = _rendererToLayerNumberMap.put(renderer, layerInt2);
      // expect that renderer already had a value associated in the map
      Assert.expect(prev != null);
      Assert.expect(prev.equals(layerInt1));
    }

    for (Renderer renderer : allRenderersForLayer2)
    {
      Integer prev = _rendererToLayerNumberMap.put(renderer, layerInt1);
      // expect that renderer already had a value associated in the map
      Assert.expect(prev != null);
      Assert.expect(prev.equals(layerInt2));
    }

    _layerNumberToRendererPanelMap.put(layerInt1, rendererPanel2);
    _layerNumberToRendererPanelMap.put(layerInt2, rendererPanel1);

    // swap the RendererPanel settings
    Color color1 = rendererPanel1.getForeground();
    Color color2 = rendererPanel2.getForeground();
    boolean visible1 = rendererPanel1.isVisible();
    boolean visible2 = rendererPanel2.isVisible();

    rendererPanel1.setVisible(visible2);
    rendererPanel2.setVisible(visible1);
    rendererPanel1.setForeground(color2);
    rendererPanel2.setForeground(color1);

    // now fix the other internal maps of this class so they know about the swap
    Double creationThreshold1 = (Double)_layerNumberToCreationThresholdMap.get(layerInt1);
    Double creationThreshold2 = (Double)_layerNumberToCreationThresholdMap.get(layerInt2);

    if (creationThreshold2 != null)
    {
      _layerNumberToCreationThresholdMap.put(layerInt1, creationThreshold2);
    }
    if (creationThreshold1 != null)
    {
      _layerNumberToCreationThresholdMap.put(layerInt2, creationThreshold1);
    }

    RendererCreator rendererCreator1 = (RendererCreator)_layerNumberToRendererCreatorMap.get(layerInt1);
    RendererCreator rendererCreator2 = (RendererCreator)_layerNumberToRendererCreatorMap.get(layerInt2);
    if (rendererCreator2 != null)
    {
      _layerNumberToRendererCreatorMap.put(layerInt1, rendererCreator2);
    }
    if (rendererCreator1 != null)
    {
      _layerNumberToRendererCreatorMap.put(layerInt2, rendererCreator1);
    }

    Double visibilityThreshold1 = (Double)_layerNumberToVisibilityThresholdMap.get(layerInt1);
    Double visibilityThreshold2 = (Double)_layerNumberToVisibilityThresholdMap.get(layerInt2);
    if (visibilityThreshold2 != null)
    {
      _layerNumberToVisibilityThresholdMap.put(layerInt1, visibilityThreshold2);
    }
    if (visibilityThreshold1 != null)
    {
      _layerNumberToVisibilityThresholdMap.put(layerInt2, visibilityThreshold1);
    }
  }

  /**
   * Set the color of all the Renderers that are currently on a layer.
   *
   * @author Bill Darbie
   */
  public void setForeground(int layerNumber, Color color)
  {
    Assert.expect(color != null);

    RendererPanel rendererPanel = (RendererPanel)_layerNumberToRendererPanelMap.get(new Integer(layerNumber));
    if (rendererPanel == null)
    {
      // the layer specified has not been set up yet,  so add it as a valid layer
      rendererPanel = getRendererPanel(layerNumber);
    }
    rendererPanel.setForeground(color);
  }

  /**
   * Set the color of all the Renderers that are currently on the layers passed in.
   *
   * @author Bill Darbie
   */
  public void setForeground(Collection<Integer> layerNumbers, Color color)
  {
    Assert.expect(layerNumbers != null);
    Assert.expect(color != null);

    for (Integer layerNumber : layerNumbers)
    {
      setForeground(layerNumber, color);
    }
  }

  /**
   * Set the visibility of a layer.
   *
   * For proper operation call this method only after all renderers have been added!
   *
   * @author Bill Darbie
   */
  public void setVisible(int layerNumber, boolean visible)
  {
    RendererPanel rendererPanel = (RendererPanel)_layerNumberToRendererPanelMap.get(new Integer(layerNumber));
    if (rendererPanel == null)
    {
      // the layer specified has not been set up yet,  so add it as a valid layer
      rendererPanel = getRendererPanel(layerNumber);
    }

    rendererPanel.setVisible(visible);
  }

  /**
   * Set the visibility of a Collection of layers.
   *
   * For proper operation call this method only after all renderers have been added!
   *
   * @author Bill Darbie
   */
  public void setVisible(Collection<Integer> layerNumbers, boolean visible)
  {
    Assert.expect(layerNumbers != null);
    for (Integer layerNumber : layerNumbers)
    {
      setVisible(layerNumber, visible);
    }
  }

  /**
   * Cause all Renderers on the passed in layer to get fresh data.  The shapes being
   * drawn will change if the underlying data has changed for a particular Renderer.
   *
   * @author Bill Darbie
   */
  public void validateData(int layerNumber)
  {
    RendererPanel rendererPanel = (RendererPanel)_layerNumberToRendererPanelMap.get(new Integer(layerNumber));
    Assert.expect(rendererPanel != null);

    rendererPanel.validateData();
    _maxBounds = null;
  }

  /**
   * Cause all Renderers in the passed in layers to get fresh data.  The shapes being
   * drawn will change if the underlying data has changed for a particular Renderer.
   *
   * @author Bill Darbie
   */
  public void validateData(Collection<Integer> layerNumbers)
  {
    Assert.expect(layerNumbers != null);
    for (Integer layerNumber : layerNumbers)
    {
      validateData(layerNumber.intValue());
    }

    updateAll();
  }

  /**
   * @author Laura Cormos
   */
  private void repaintRendererThatHasBeenRemoved(Renderer renderer)
  {
    Rectangle bounds = renderer.getBounds();
    repaint(bounds);
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void repaintRendererThatHasBeenAddedIn(Renderer renderer)
  {
    Rectangle bounds = renderer.getBounds();
    repaint(bounds);
  }

  /**
   * The visibility threshold controls at what point renderers on a layer
   * get displayed.  When the user zooms in far enough the layer will be set to be
   * visible based on the threshold specified.  When the zoom goes back out, the layer
   * will be set to be non-visible.
   *
   * For example, if all the Renderers passed in are using mils as units.  If you specify
   * 1 as the visibilityThreshold, then when 1 mil is representd on the graphics
   * screen using at least 1 pixel, then that layer will become visible.
   *
   * For proper operation call this method only after all renderers have been added!
   *
   * @param layerNumber is the layer to set the visibility threshold on
   * @param visibilityThreshold is the value in the Renderer units passed in that a Renderer should
   *                      switch from being visible to invisible.
   *
   * @author Bill Darbie
   */
  public void setVisibilityThreshold(int layerNumber, double visibilityThreshold)
  {
    _layerNumberToVisibilityThresholdMap.put(new Integer(layerNumber), new Double(visibilityThreshold));
    RendererPanel rendererPanel = getRendererPanel(layerNumber);
    Assert.expect(rendererPanel != null);
    rendererPanel.setRenderersVisible(false);
  }

  /**
   * The visibility threshold controls at what point renderers on a layer
   * get displayed.  When the user zooms in far enough the layer will be set to be
   * visible based on the threshold specified.  When the zoom goes back out, the layer
   * will be set to be non-visible.
   *
   * For example, if all the Renderers passed in are using mils as units.  If you specify
   * 1 as the visibilityThreshold, then when 1 mil is representd on the graphics
   * screen using at least 1 pixel, then that layer will become visible.
   *
   * For proper operation call this method only after all renderers have been added!
   *
   * @param layerNumbers is the Collection of layer Integers to set the visibility threshold on
   * @param visibilityThreshold is the value in the Renderer units passed in that a Renderer should
   *                      switch from being visible to invisible.
   *
   * @author Bill Darbie
   */
  public void setVisibilityThreshold(Collection<Integer> layerNumbers, double visibilityThreshold)
  {
    Assert.expect(layerNumbers != null);

    for (Integer layerNumber : layerNumbers)
    {
      setVisibilityThreshold(layerNumber, visibilityThreshold);
    }
  }

  /**
   * Clear out any previously set visibility thresholds.
   * @author Bill Darbie
   */
  public void clearVisibilityThresholds(int layerNumber)
  {
    Integer layer = new Integer(layerNumber);
    _layerNumberToVisibilityThresholdMap.remove(layer);
    RendererPanel rendererPanel = getRendererPanel(layerNumber);
    Assert.expect(rendererPanel != null);
    rendererPanel.setRenderersVisible(true);
  }

  /**
   * The creation threshold controls at what point renderes on a layer
   * get created and displayed.  When the user zooms in far enough the Renderers on the layer
   * will be created based on the threshold specified.  When the zoom goes back out, the Renderers
   * on the layer will be destroyed.
   *
   * For example, if all the Renderers passed in are using mils as units.  If you specify
   * 1 as the visibilityThreshold, then when 1 mil is representd on the graphics
   * screen using at least 5 pixels, then the Renderers on that layer will be created.
   *
   * For proper operation call this method only after all renderers have been added!
   *
   * @param layerNumber is the layer to set the visibility threshold on
   * @param creationThreshold is the value in the Renderer units passed in that a Renderer should
   *                      switch from being created and destroyed
   * @param rendererCreator is the interface that will be called at the appropriate time
   *                        to actually create the Renderers.
   *
   * @author Bill Darbie
   */
  public void setCreationThreshold(int layerNumber, double creationThreshold, RendererCreator rendererCreator)
  {
    Assert.expect(rendererCreator != null);
    _layerNumberToRendererCreatorMap.put(new Integer(layerNumber), rendererCreator);
    _layerNumberToCreationThresholdMap.put(new Integer(layerNumber), new Double(creationThreshold));
    _layerNumberToCreationThresholdBooleanMap.put(new Integer(layerNumber), new Boolean(true));
  }

  /**
   * @author Bill Darbie
   */
  public void setCreationThreshold(Collection<Integer> layerNumbers, double creationThreshold,
      RendererCreator rendererCreator)
  {
    Assert.expect(layerNumbers != null);

    for (Integer layerNumber : layerNumbers)
    {
      setCreationThreshold(layerNumber, creationThreshold, rendererCreator);
    }
  }

  /**
   * Clear out all the previous creartion threshold settings.
   * @author Bill Darbie
   */
  public void clearCreationThresholds(int layerNumber)
  {
    Integer layer = new Integer(layerNumber);
    _layerNumberToRendererCreatorMap.remove(layer);
    _layerNumberToCreationThresholdMap.remove(layer);
  }

  /**
   * Add the panel that draws cross hairs.
   * @author Bill Darbie
   */
  private void addCrossHairPanel(int width, int height)
  {
    if (_crossHairRendererPanel == null)
    {
      _crossHairRendererPanel = new RendererPanel(Color.RED); // this color is not really used.   See setForeground below for real color
      _crossHairRenderer = new CrossHairRenderer();
      _crossHairRendererPanel.add(_crossHairRenderer);
      _crossHairRendererPanel.setForeground(LayerColorEnum.CROSSHAIR_COLOR.getColor());
      _crossHairRendererPanel.setVisible(false);
    }
    _crossHairRendererPanel.setSize(width, height);

    // determine the layer number to put the panel on
    if (_crossHairLayerNumber != null)
    {
      removeLayerForCrossHairAndMouse(_crossHairLayerNumber.intValue());
    }

//    int crossHairLayer = 0;
//    int count = 0;
//    for (Integer layerNumber : _layerNumberToRendererPanelMap.keySet())
//    {
//      ++count;
//      int number = layerNumber.intValue();
//      if (count == 1)
//      {
//        crossHairLayer = number;
//      }
//      else
//      {
//        if (number > crossHairLayer)
//        {
//          crossHairLayer = number;
//        }
//      }
//    }
//    ++crossHairLayer;
    _crossHairLayerNumber = new Integer(_crossHairLayer);

    _layerNumberToRendererPanelMap.put(_crossHairLayerNumber, _crossHairRendererPanel);
    _layeredPane.add(_crossHairRendererPanel, _crossHairLayerNumber);
  }

  /**
   * Add the mouseEventPanel to the bottom-most layer.
   * @author Bill Darbie
   */
  private void addMouseEventPanel(int width, int height)
  {
    if (_mouseEventPanel == null)
    {
      _mouseEventPanel = new MouseEventPanel(this, LayerColorEnum.MOUSE_EVENTS_COLOR.getColor());
      _mouseEventPanel.setVisible(false);
    }
    _mouseEventPanel.setSize(width, height);

    // determine the layer number to put the panel on
    if (_mouseLayerNumber != null)
    {
      removeLayerForCrossHairAndMouse(_mouseLayerNumber.intValue());
    }

//    int mouseLayer = 0;
//    int count = 0;
//    for (Integer layerNumber : _layerNumberToRendererPanelMap.keySet())
//    {
//      ++count;
//      int number = layerNumber.intValue();
//      if (count == 1)
//      {
//        mouseLayer = number;
//      }
//      else
//      {
//        if (number > mouseLayer)
//        {
//          mouseLayer = number;
//        }
//      }
//    }
//    ++mouseLayer;
    _mouseLayerNumber = new Integer(_mouseLayer);

    _layeredPane.add(_mouseEventPanel, _mouseLayerNumber);
  }

  /**
   * Select (in a different color) the Renderer passed in.
   * Note that if you have a lot of Renderers to select you will get much better
   * GUI performance to call setSelectedRenderers() since it will call repaint() only
   * once instead of once per Renderer.
   * @author Bill Darbie
   */
  public void setSelectedRenderer(Renderer renderer)
  {
    setSelectedRendererWithoutRepaint(renderer);
    repaint();
  }

  /**
   * Highlight in a different color the Renderer passed in.  Highlighting is a re-coloration of a
   * renderer.  A highlighted renderer may still be selected (which will then use the selection color)
   * Note that if you have a lot of Renderers to highlight you will get much better
   * GUI performance to call setHighlightRenderers() (which needs to be written) since it will call repaint() only
   * once instead of once per Renderer.
   * @author Andy Mechtenberg
   */
  public void setHighightedRenderer(Renderer renderer, Color highlightColor)
  {
    setHighlightedRendererWithoutRepaint(renderer, highlightColor);
    repaint();
  }

  /**
   * Highlight in a different color the Renderers passed in.  Highlighting is a re-coloration of a
   * renderer.  A highlighted renderer may still be selected (which will then use the selection color)
   * @author Andy Mechtenberg
   */
  public void setHighlightedRenderers(Collection<Renderer> renderers, Color highlightColor)
  {
    Assert.expect(renderers != null);

    for(Renderer renderer : renderers)
      setHighlightedRendererWithoutRepaint(renderer, highlightColor);
    repaint();
  }

  /**
   * Select (in a different color) the Renderers passed in.
   * This is the preferred method to use if since repaint() will only
   * get called once no matter how many Renderers are getting set to a new
   * Color.
   *
   * @author Bill Darbie
   */
  public void setSelectedRenderers(Collection<? extends Renderer> renderers)
  {
    Assert.expect(renderers != null);

    for (Renderer renderer : renderers)
    {
      setSelectedRendererWithoutRepaint(renderer);
    }
    repaint();
  }

  /**
   * @author Bill Darbie
   */
  private void setSelectedRendererWithoutRepaint(Renderer renderer)
  {
    Assert.expect(renderer != null);

    synchronized (_rendererMapsLock)
    {
      if (_selectedRendererToOrigColorMap.containsKey(renderer) == false)
      {
        _selectedRendererToOrigColorMap.put(renderer, getOrigColor(renderer));
        renderer.setForeground(renderer.getSelectedColor());
        _currentlySelectedRendererSet.add(renderer);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setHighlightedRendererWithoutRepaint(Renderer renderer, Color highlightColor)
  {
    Assert.expect(renderer != null);
    Assert.expect(highlightColor != null);

    if (_highlightedRendererToOrigColorMap.containsKey(renderer) == false)
    {
      _highlightedRendererToOrigColorMap.put(renderer, getOrigColor(renderer));
      // now, if this renderer is also "selected" that takes priority.  So, we'll not change the foreground
      // color if it's selected as well, but we will keep the highlight color in the map, so if becomes de-selected
      // it will revert to highlighted
      if (_selectedRendererToOrigColorMap.containsKey(renderer) == false)
        renderer.setForeground(highlightColor);
      else
        renderer.setForeground(renderer.getSelectedColor());
      renderer.setHighlightedColor(highlightColor);
      _currentlyHighlightedRendererSet.add(renderer);
    }
  }

  /**
   * Clear the currently selected Renderers so they are no longer colored differently.
   * But, if they are listed in the Highlight color map, use that.  So, the priority is Selected - Highlighted - Normal
   * @author Bill Darbie
   * @edited hee-jihn.chuah - XCR-2207: Yellow crosshair that mark active components are not always ON	
   */
  public void clearSelectedRenderers()
  {
    clearCrossHairs();
    clearSelectedRenderersWithoutCrossHair();
  }

  /**
   * @author hee-jihn.chuah - XCR-2207: Yellow crosshair that mark active components are not always ON	
   */
  public void clearSelectedRenderersWithoutCrossHair()
  {
    synchronized (_rendererMapsLock)
    {
      for (Map.Entry entry : _selectedRendererToOrigColorMap.entrySet())
      {
        Renderer renderer = (Renderer)entry.getKey();
        Color origColor = (Color)entry.getValue();

        if (_highlightedRendererToOrigColorMap.get(renderer) != null) // if it is in the map
        {
          // set this foreground back to the highlight color
          renderer.setForeground(renderer.getHighlightedColor());
        }
        else
          renderer.setForeground(origColor);
      }
      _selectedRendererToOrigColorMap.clear();
      _currentlySelectedRendererSet.clear();
    }

    repaint();
  }

  /**
   * Clear the currently highlighted Renderers so they are no longer highlighted in a different color.
   * @author Andy Mechtenberg
   * @edited hee-jihn.chuah - XCR-2207: Yellow crosshair that mark active components are not always ON	
   */
  public void clearHighlightedRenderers()
  {
    clearCrossHairs();
    clearHighlightedRenderersWithoutCrossHair();
  }

  /**
   * @author hee-jihn.chuah - XCR-2207: Yellow crosshair that mark active components are not always ON	
   */
  public void clearHighlightedRenderersWithoutCrossHair()
  {
  for (Map.Entry entry : _highlightedRendererToOrigColorMap.entrySet())
    {
      Renderer renderer = (Renderer)entry.getKey();
      Color origColor = (Color)entry.getValue();

      renderer.setForeground(origColor);
    }
    _highlightedRendererToOrigColorMap.clear();
    _currentlyHighlightedRendererSet.clear();

    repaint();
  }

  /**
   * @author Andy Mechtenberg
   */
  private Color getOrigColor(Renderer renderer)
  {
    Assert.expect(renderer != null);
    synchronized (_rendererMapsLock)
    {
      if (_highlightedRendererToOrigColorMap.containsKey(renderer))
      {
        return _highlightedRendererToOrigColorMap.get(renderer);
      }
      else if (_selectedRendererToOrigColorMap.containsKey(renderer))
      {
        return _selectedRendererToOrigColorMap.get(renderer);
      }
      else
        return renderer.getForeground();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean wasSelectionPartOfAGroupSelection()
  {
    return _multipleSelectOn;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isMultipleSelectEnabled()
  {
    return _multipleSelectOn;
  }

  /**
   * This gets called when the left mouse button is clicked
   * @author Bill Darbie
   */
  public void mouseClicked(MouseEvent mouseEvent)
  {
    Assert.expect(mouseEvent != null);

    Collection<Renderer> renderers = new ArrayList<Renderer>();

    // if the ctrl key is down when the click happened, we want to ADD the new selection.  otherwise, replace with the new selection
    int multipleSelectMask = InputEvent.CTRL_DOWN_MASK;
    boolean isRightClick = mouseEvent.isPopupTrigger();
    // on a right click, we should do the normal left click operation first, if the _selectOnRightClick is set
    // then do the right click.
    // Otherwise, just do the right click work -- which is really nothing but sending the event
    if (((isRightClick) && (_selectOnRightClick)) || (isRightClick == false))
    {
      if (((mouseEvent.getModifiersEx() & multipleSelectMask) == multipleSelectMask) == false)
      {
        _multipleSelectOn = false;
        _currentlySelectedRendererSet.clear();
        setControlKeyPressMode(false);
      }
      else
      {
        _multipleSelectOn = true;
        setControlKeyPressMode(true);
      }

      int xPixel = mouseEvent.getX();
      int yPixel = mouseEvent.getY();

      if (_layersToRespondToLeftMouseClickSet.isEmpty())
      {
        // look for renderers on all layers since _layersToRespondToLeftMouseClickSet is empty
        for (RendererPanel rendererPanel : _layerNumberToRendererPanelMap.values())
        {
          if (rendererPanel.isVisible())
          {
            renderers.addAll(rendererPanel.getRenderers());
          }
        }
      }
      else
      {
        // only look at the layers specified in _layersToRespondToLeftMouseClickSet for Renderers
        for (Integer layerInteger : _layersToRespondToLeftMouseClickSet)
        {
          RendererPanel rendererPanel = getRendererPanel(layerInteger.intValue());
          if (rendererPanel.isVisible())
          {
            renderers.addAll(rendererPanel.getRenderers());
          }
        }
      }
      // Inform anyone listening that this renderer was selected

      // now go through the list of Renderers to see which ones that are visible and selectable might
      // have been selected by the mouse click
      for (Renderer renderer : renderers)
      {
        if (renderer.isSelectable())
        {
          if (renderer.contains(xPixel, yPixel))
          {
            _currentlySelectedRendererSet.add(renderer);
          }
        }
      }
      // Inform anyone listening that this renderer was selected
      _selectedRendererObservable.setSelectedRenderer(new ArrayList<Renderer>(_currentlySelectedRendererSet));
      
       if (isMouseRightClickMode())
        _unselectedOpticalRegionRendererObservable.setUnselectedOpticalRegionRenderer(new ArrayList<Renderer>(_currentlySelectedRendererSet));
    }
       
    // notify all observers that a mouse click occurred
    _mouseClickObservable.setMouseClickEvent(mouseEvent);
  }

  /**
   * @author Bill Darbie
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    setBackground(Color.black);
    setLayout(new BorderLayout());
    add(_layeredPane, BorderLayout.CENTER);
    addComponentListener(this);
  }

  /**
   * Flip all layers across the x axis.  This is a top to bottom flip.
   * If the view was looking down to the top of the shapes, it will
   * flip to be a view from the bottom.
   * @author Bill Darbie
   */
  private void flipTopToBottom()
  {
    double canvasHeight = getBounds().getHeight();

    AffineTransform transform = AffineTransform.getScaleInstance(1.0, -1.0);
    AffineTransform preTrans = AffineTransform.getTranslateInstance(0, canvasHeight);
    transform.preConcatenate(preTrans);

    transform(transform);
  }

  /**
   * Flip all layers across the y axis.  This is a left to right flip.
   * If the view was looking down to the top of the shapes, it will
   * flip to be a view from the bottom.
   * @author Bill Darbie
   */
  private void flipLeftToRight()
  {
    double canvasWidth = getBounds().getWidth();

    AffineTransform transform = AffineTransform.getScaleInstance( -1.0, 1.0);
    AffineTransform preTrans = AffineTransform.getTranslateInstance(canvasWidth, 0);
    transform.preConcatenate(preTrans);

    transform(transform);
  }

  /**
   * Puts the GraphicsEngine into the mode where the user can drag the graphics across the screen.
   * @author Andy Mechtenberg
   */
  public void setDragGraphicsMode(boolean dragGraphicsMode)
  {
   if (dragGraphicsMode)
    {
      setCursor(new java.awt.Cursor(java.awt.Cursor.MOVE_CURSOR));
      _mouseEventPanel.setVisible(true);
      _mouseEventPanel.enableDragMode();
    }
    else
    {
      setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
      _mouseEventPanel.setVisible(false);
   }  
  }

  /**
   * Puts the GraphicsEngine into themode where the user can draw a Rectangle to indicate an
   * area to zoom into and display on the screen.
   * @author Andy Mechtenberg
   */
  public void setZoomRectangleMode(boolean zoomRectangleMode)
  {
    if (zoomRectangleMode)
    {
      setCursor(new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR));
      _mouseEventPanel.setVisible(true);
      _mouseEventPanel.enableZoomRectangleMode();
    }
    else
    {
      _mouseEventPanel.setVisible(false);
      setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    }
  }

  /**
   * @todo pwl : George, Measuring functionality doesn't belong in this object.
   * @author George A. David
   */
  public void setMeasurementMode(boolean measurementMode)
  {
    if (measurementMode)
    {
      setCursor(new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR));
      _mouseEventPanel.setVisible(true);
      _mouseEventPanel.enableMeasurementMode();
    }
    else
    {
      _mouseEventPanel.setVisible(false);
      setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    }
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void setDragRegionMode(boolean dragRegionMode)
  {
     if (dragRegionMode)
        {
          setCursor(new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR));
          
          if (_mouseEventPanel != null)
          {
              _mouseEventPanel.setVisible(true);
              _mouseEventPanel.enableDragRegionMode();
          }
          _isDragRegionMode = true;
        }
        else
        {     
          if (_mouseEventPanel != null)
             _mouseEventPanel.setVisible(false);
          
          setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
          _isDragRegionMode = false;             
        }  
  }

  /**
   * 
   * @author Jack Hwee
   */
  public void setDragSelectedRegionMode(boolean dragSelectedRegionMode)
  {
    if (dragSelectedRegionMode)
    {
      setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

      if (_mouseEventPanel != null)
      {
        _mouseEventPanel.setVisible(true);
        _mouseEventPanel.enableDragSelectedRegionMode();
      }
      _isDragSelectedRegionMode = true;
    }
    else
    {
      if (_mouseEventPanel != null)
      {
        _mouseEventPanel.setVisible(false);
      }

      setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
      _isDragSelectedRegionMode = false;
    }
  }
  
  /**
   * @todo pwl : George, Measuring functionality doesn't belong in this object.
   * @author George A. David
   */
  double calculateDistanceInMeasurementUnits(int beginXpixels, int beginYpixels,
                                             int endXpixels, int endYpixels)
  {
    Point2D point1 = convertFromPixelToRendererCoordinates(beginXpixels, beginYpixels);
    Point2D point2 = convertFromPixelToRendererCoordinates(endXpixels, endYpixels);

    return MathUtil.calculateDistance(point1.getX(), point1.getY(), point2.getX(), point2.getY()) * _measurementFactor;
  }

  /**
   * @todo pwl : George, Measuring functionality doesn't belong in this object.
   * @author George A. David
   */
  String getCurrentMeasurementUnitsString()
  {
    Assert.expect(_measurementUnitsString != null);

    return _measurementUnitsString;
  }

  /**
   * @todo pwl : George, Measuring functionality doesn't belong in this object.
   * @author George A. David
   */
  int getCurrentMeasurementUnitsDecimalPlaces()
  {
    return _measurementUnitDecimalPlaces;
  }

  /**
   * The measurement factor is used to convert from renderer units
   * to the desired measurements units.
   *
   * @todo pwl : George, Measuring functionality doesn't belong in this object.
   * @author George A. David
   */
  public void setMeasurementInfo(double measurementFactor, int measurementUnitDecimalPlaces, String measurementUnitsString)
  {
    Assert.expect(measurementUnitsString != null);
    _measurementFactor = measurementFactor;
    _measurementUnitDecimalPlaces = measurementUnitDecimalPlaces;
    _measurementUnitsString = measurementUnitsString;
  }

  /**
   * Puts the GraphicsEngine into the mode where the user can draw a Rectangle to select a bunch of
   * renderers all at once
   * @author Andy Mechtenberg
   */
  public void setGroupSelectMode(boolean groupSelectMode)
  {
    if (groupSelectMode)
      setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    _mouseEventPanel.setVisible(groupSelectMode);
    _mouseEventPanel.setGroupSelectMode(groupSelectMode);
  }

  /**
   * @author Bill Darbie
   * @author Andy Mechtenberg
   */
  private void setDragAndZoomMode(boolean dragAndZoom)
  {
    _mouseEventPanel.setVisible(dragAndZoom);
  }

  /**
   * @author George A. David
   */
  public void setMouseEventsEnabled(boolean enabled)
  {
    addSpecialEventPanelsIfNeeded();
    _mouseEventPanel.enableMouseClickMode();
    _mouseEventPanel.setVisible(enabled);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setEnableSelectionOnMouseRightClick(boolean enabled)
  {
    _selectOnRightClick = enabled;
  }

  /**
   * If the graphics are scaled in or out too much the data will get rounded and the Renderers
   * will no longer have valid data.
   * @return false if the new scale will make the data too large or too small
   * @author Bill Darbie
   */
  private boolean isNewScaleOk(double newScale)
  {
    // do not allow zooming too far in or out since that would cause the data
    // in the Renderers to get messed up due to limits in how large or small the
    // double type can handle
    Rectangle2D bounds = getCurrentBounds();
    double width = bounds.getWidth();
    double height = bounds.getHeight();
    double size = Math.max(width, height);
    if ((size < 3) && (newScale < 1))
    {
      // less than 2 pixels is plenty small
      // already too small, dont go any smaller
      return false;
    }
    if ((size > 10000000) && (newScale > 1))
    {
      // more than 10000000 pixels is plenty big
      // already too large, dont go any larger
      return false;
    }

    return true;
  }

  /**
   * This methods main purpose is to call the trans() method.  But it also applies the axis transform
   * the first time this method is called, and also adds the mouse event panel.
   * @author Bill Darbie
   */
  private void transform(AffineTransform transform)
  {
    applyAxisTransformIfNeeded();
    trans(transform);
    addSpecialEventPanelsIfNeeded();
    // when this method has been called the graphics have been fit to the screen
    // so set the flag to true
    if (_transformApplied == false)
    {
      _transformApplied = true;
      mark();
    }

    _visibleRectangleObservable.setVisibleRectangle(getVisibleRectangleInRendererCoordinates());
  }

  /**
   * Apply the passed in transform to all Renderers. Note that only the transform() method
   * (and the methods it calls) should directly call this method.
   * @author Bill Darbie
   */
  private void trans(AffineTransform transform)
  {
    Assert.expect(transform != null);
    Assert.expect(_setAxis, "the setAxisInterpretation() method must be called first");

    // update the _transform variables so they have keep up with the current state
    _transformFromRendererCoords.preConcatenate(transform);
    _transformFromAxisTransform.preConcatenate(transform);
    _transformFromLastMark.preConcatenate(transform);
    _transformFromFitToScreen.preConcatenate(transform);

    // only drag the layers the user specified, if the user
    // has not specified any layers, than drag all layers.
    Collection<RendererPanel> rendererPanels = null;
    if(_layersToUseWhenDraggingGraphics.isEmpty() || _isDraggingGraphics == false)
      rendererPanels = _layerNumberToRendererPanelMap.values();
    else
    {
      rendererPanels = new LinkedList<RendererPanel>();
      for(int layerNumber : _layersToUseWhenDraggingGraphics)
        rendererPanels.add(_layerNumberToRendererPanelMap.get(layerNumber));
    }

    for (RendererPanel rendererPanel : rendererPanels)
    {
      rendererPanel.transform(transform);
    }

    // since the update may add Renderers, it needs to be done after we iterate over
    // all Renderers.  Otherwise the added Renderers would have the same transfrom
    // applied to them twice
    updateAll();
  }

  /**
   * Turns off renderer visibility while renderers are being updated (for performance reasons)
   * Note: (Patrick Lacz) Using this method no longer appears to be the best for performance:
   *    Removing it from trans() removed 20% of rendering execution time.
   * @author Kay Lannen
   * @author Bill Darbie
   * @return Set<RendererPanel> a set of panels whose visibility has been changed.
   */
  private Set<RendererPanel> turnOffRendererVisibility()
  {
    // turn off visiblity while the transform of all the Renderers is happening
    // this makes things more responsive
    // doing a setVisible on this Panel messed up the cursor when in drag mode
    // so I will just call it on each RendererPanel instead
    Set<RendererPanel> panelsWithChangedVisibility = new HashSet<RendererPanel>();
    for (RendererPanel rendererPanel : _layerNumberToRendererPanelMap.values())
    {
      if (rendererPanel.isVisible())
      {
        panelsWithChangedVisibility.add(rendererPanel);
        rendererPanel.setVisible(false);
      }
    }
    return (panelsWithChangedVisibility);
  }

  /**
   * Turns on renderer visibility for panels in the specified renderer set
   * @author Kay Lannen
   * @author Bill Darbie
   * @param panels Set of panels whose renderers should be made visible
   */
  private void turnOnRendererVisibility(Set<RendererPanel> panels)
  {
    for (RendererPanel rendererPanel : panels)
    {
      rendererPanel.setVisible(true);
    }
  }

  /**
   * Update anything that needs updating since a new transform has been applied.
   * @author Bill Darbie
   */
  private void updateAll()
  {
    updateRenderersWithCreationThresholds();
    updateRenderersWithVisibilityThresholds();
    updateCrossHairs();
  }

  /**
   * This will be called as part of the transform() call
   * and will figure out which components are currently
   * created and visible to the user based on the thresholds set.
   * @author Andy Mechtenberg
   * @author Bill Darbie
   */
  private void updateRenderersWithCreationThresholds()
  {
    // if we haven't applied a transform yet, then we shouldn't do anything here yet
    if (_transformApplied == false)
    {
      return;
    }

    for (Map.Entry entry : _layerNumberToCreationThresholdMap.entrySet())
    {
      Integer layerNumberInteger = (Integer)entry.getKey();
      int layerNumber = layerNumberInteger.intValue();
      Double creationThresholdDouble = (Double)entry.getValue();
      double creationThreshold = creationThresholdDouble.doubleValue();

      double scaleX = Math.abs(_transformFromRendererCoords.getScaleX());
      double scaleY = Math.abs(_transformFromRendererCoords.getScaleY());

      // a rotation transform will have a scaleX and scaleY of 0, so set them to 1 instead
      if (scaleX == 0)
      {
        scaleX = 1;
      }
      if (scaleY == 0)
      {
        scaleY = 1;
      }

      double pixelsThreshold = creationThreshold * scaleX;
      // if the size feature specified by the threshold is 5 pixels or larger, then create it
      if (pixelsThreshold < _PIXEL_THRESHOLD)
      {
        // remove everything from this layer
        if (_layerNumberToCreationThresholdBooleanMap.get(layerNumberInteger).booleanValue() == false)
        {
          _layerNumberToCreationThresholdBooleanMap.put(layerNumberInteger, new Boolean(true));
          // need to tell which ever RendererCreator is using this engine that renderer creation threshold was crossed
          RendererCreator rendererCreator = (RendererCreator)_layerNumberToRendererCreatorMap.get(layerNumberInteger);
          Assert.expect(rendererCreator != null);
          rendererCreator.updateRenderers(this, layerNumber);
        }
        removeRenderersFromBeingVisible(layerNumber);
      }
      else
      {
//      }
        // call the appropriate createRenderers method and let is determine what
        // renderers to add/remove on its own
        _layerNumberToCreationThresholdBooleanMap.put(layerNumberInteger, new Boolean(false));
        RendererCreator rendererCreator = (RendererCreator)_layerNumberToRendererCreatorMap.get(layerNumberInteger);
        Assert.expect(rendererCreator != null);
        rendererCreator.updateRenderers(this, layerNumber);
      }
    }
  }

  /**
   * This will be called as part of the transform() call and will figure out which Renderers should
   * be visible to the user based on the thresholds set on the particular layer.
   * @author Andy Mechtenberg
   * @author Bill Darbie
   */
  private void updateRenderersWithVisibilityThresholds()
  {
    // if we haven't applied a transform yet, then we shouldn't do anything here yet
    if (_transformApplied == false)
    {
      return;
    }

    for (Map.Entry entry : _layerNumberToVisibilityThresholdMap.entrySet())
    {
      Integer layerNumberInteger = (Integer)entry.getKey();
      int layerNumber = layerNumberInteger.intValue();

      Double visibilityThresholdDouble = (Double)entry.getValue();
      double visibilityThreshold = visibilityThresholdDouble.doubleValue();

      RendererPanel rendererPanel = getRendererPanel(layerNumber);
      Assert.expect(rendererPanel != null);

      double scaleX = Math.abs(_transformFromRendererCoords.getScaleX());
      double scaleY = Math.abs(_transformFromRendererCoords.getScaleY());

      // a rotation transform will have a scaleX and scaleY of 0, so set them to 1 instead
      if (scaleX == 0)
      {
        scaleX = 1;
      }
      if (scaleY == 0)
      {
        scaleY = 1;
      }

      double pixelsThreshold = visibilityThreshold * scaleX;
      // if the size feature specified by the threshold is 5 pixels or larger, then
      // make it visible
      if (pixelsThreshold < _PIXEL_THRESHOLD)
      {
        rendererPanel.setRenderersVisible(false);
      }
      else
      {
        rendererPanel.setRenderersVisible(true);
      }
    }
  }

  /**
   * Center the currently calculated bounds on the screen.  This may cause
   * all Renderers to be centered or just the visible ones, depending on
   * what the last fitGraphicsToScreen() call was.
   * @author Bill Darbie
   * @author Andy Mechtenberg
   */
  public void center()
  {
    Rectangle2D bounds = getCurrentBounds();
    double rendererX = bounds.getX();
    double rendererY = bounds.getY();
    double rendererWidth = bounds.getWidth();
    double rendererHeight = bounds.getHeight();

    center(rendererX, rendererY, rendererWidth, rendererHeight);
  }

  /**
   * @author George A. David
   */
  public void centerRectInRendererCoordinates(Rectangle rect)
  {
    Rectangle2D rectInPixels = _transformFromRendererCoords.createTransformedShape(rect).getBounds2D();
    center(rectInPixels.getX(),
           rectInPixels.getY(),
           rectInPixels.getWidth(),
           rectInPixels.getHeight());
  }

  /**
   * Put the Rectangle specified (in pixel space) at the center of the screen.t
   */
  public void center(double xInPixels, double yInPixels, double widthInPixels, double heightInPixels)
  {
    double graphicsEngineWidth = getWidth();
    double graphicsEngineHeight = getHeight();

    // xCenter, yCenter is the x,y point to use to get things centered
    double xCenter = (graphicsEngineWidth - widthInPixels) / 2.0;
    double yCenter = (graphicsEngineHeight - heightInPixels) / 2.0;

    // subtract out the current position of the Renderers to figure
    // out the amount to translate from the current state
    double xtrans = xCenter - xInPixels;
    double ytrans = yCenter - yInPixels;

    translate(xtrans, ytrans);
  }

  /**
   * @author George A. David
   */
  public void scrollRectInRendererCoordinatesToVisible(Rectangle rect)
  {
    Rectangle2D rectInPixels = _transformFromRendererCoords.createTransformedShape(rect).getBounds2D();

    double xTrans = 0;
    double yTrans = 0;

    if(rectInPixels.getX() < 0)
      xTrans = -rectInPixels.getX();
    else if(rectInPixels.getMaxX() > getWidth())
      xTrans = getWidth() - rectInPixels.getMaxX();

    if(rectInPixels.getY() < 0)
      yTrans = -rectInPixels.getY();
    else if(rectInPixels.getMaxY() > getHeight())
      yTrans = getHeight() - rectInPixels.getMaxY();

    translate(xTrans, yTrans);
  }

  /**
   * Rotate the entire drawing.
   * @param degreesRotation is the degrees to rotation the drawing by
   * @param x the x coordinate to rotate around
   * @param y the y coordinate to rotate around
   *
   * @author Bill Darbie
   */
  public void rotate(int degreesRotation, double x, double y)
  {
    // NOTE - Renderer has a bug that must be fixed for this to work properly!!
    System.out.println("wpd GraphicsEngine.rotate() stub");
    AffineTransform transform = AffineTransform.getRotateInstance(Math.toRadians(degreesRotation), x, y);
    transform(transform);
  }

  /**
   * Rotate the entire drawing by the amount specified around 0, 0.
   * @param degreesRotation is the degrees to rotation the drawing by
   * @author Bill Darbie
   */
  public void rotate(int degreesRotation)
  {
    // NOTE - Renderer has a bug that must be fixed for this to work properly!!
    System.out.println("wpd GraphicsEngine.rotate() stub");
    AffineTransform transform = AffineTransform.getRotateInstance(Math.toRadians(degreesRotation), 0, 0);
    transform(transform);
  }

  /**
   * drag the graphics
   * @author George A. David
   */
  public void dragGraphics(double xInPixels, double yInPixels)
  {
    if(_allowDragInY == false)
      yInPixels = 0;

    _isDraggingGraphics = true;
    translate(xInPixels, yInPixels);
    _isDraggingGraphics = false;
    calcCurrentBounds(false);
    // get the intersection of the bounds so we only repaint what is visible.
    Rectangle bounds = getBounds();
    Rectangle2D maxBounds = getCurrentBounds();
    Rectangle2D.intersect(maxBounds, bounds, bounds);
    repaint(bounds);
//    repaint(maxBounds.getBounds());
  }

  /**
   * @author George A. David
   */
  public void setAllowDrayInY(boolean allowDragInY)
  {
    _allowDragInY = allowDragInY;
  }

  /**
   * Translate (shift) the entire drawing by the amount specified by the x, y coordinates passed in
   * @author Bill Darbie
   * @author Andy Mechtenberg
   */
  private void translate(double xInPixels, double yInPixels)
  {
    AffineTransform transform = AffineTransform.getTranslateInstance(xInPixels, yInPixels);
    transform(transform);
  }

  /**
   * resets the zoom to it's original 1.0 zoom factor
   * @author George A. David
   */
  public void resetZoom()
  {
    double currentZoomFactor = getCurrentZoomFactor();
    double zoomFactor = 1.0 / currentZoomFactor;
    zoom(zoomFactor);
  }
  //Anthony01005
  /**
   * Set the zoom to it's zoom factor
   * @author Anthony Fong
   */
  public void setZoom(double zoomFactor)
  {
    _transformFromFitToScreen.setToIdentity();
    zoom(zoomFactor);
  }
  /**
   * Cause all the graphics to zoom in or out.
   * @param zoomFactor is the amount to zoom in.  2 means make each Renderer twice as big.
   * @return true if the zoom was sucessful, false if the zoom was not performed because the
   * graphics were already zoomed in or out too far.
   *
   * @author Bill Darbie
   * @author Andy Mechtenberg
   */
  public boolean zoom(double zoomFactor)
  {
    _isZoom = false;
    
    if(_isZoomEnabled == false)
      return false;

    if(MathUtil.fuzzyGreaterThanOrEquals(getCurrentZoomFactor() * zoomFactor, _minZoomFactor) == false)
    {
      if(_shouldGraphicsBeFitToScreen)
      {
        fitGraphicsToScreen();
        if(_isDragRegionMode == true)
        {
          //  fitGraphicsToScreenWhenWindowIsResized();
          _mouseEventPanel.repaint();
          this.setOpticalRegionFirstDrag(false);
//          _dragOpticalRegionObservable.setDragOpticalRegionRenderer((_currentlySelectedComponentRendererSet));
//          _unselectedOpticalRegionRendererObservable.setUnselectedOpticalRegionRenderer(_currentlySelectedComponentRendererSet);
//          _unselectedDragOpticalRegionRendererObservable.setUnselectedDragOpticalRegionRenderer(_currentlySelectedComponentRendererSet);
        }
      }
      return false;
    }
    else  // if window is zoom we need to update the observable for surface map optical region
    {
      if (_isDragRegionMode == true)
      {      
          _mouseEventPanel.repaint();
           this.setOpticalRegionFirstDrag(false);
//          _dragOpticalRegionObservable.setDragOpticalRegionRenderer((_currentlySelectedComponentRendererSet));
//          _unselectedOpticalRegionRendererObservable.setUnselectedOpticalRegionRenderer(_currentlySelectedComponentRendererSet);
//          _unselectedDragOpticalRegionRendererObservable.setUnselectedDragOpticalRegionRenderer(_currentlySelectedComponentRendererSet);
      }
    }

    if (isNewScaleOk(zoomFactor) == false)
    {
      return false;
    }

    _isZoom = true;
    transform(getZoomTransform(zoomFactor));
    calcCurrentBounds(false);
    // get the intersection of the bounds so we only repaint what is visible.
    Rectangle bounds = getBounds();
    Rectangle2D maxBounds = getCurrentBounds();
    Rectangle2D.intersect(maxBounds, bounds, bounds);
    repaint(bounds);
//    repaint(maxBounds.getBounds());
    return true;
  }

  /**
   * @author George A. David
   */
  private AffineTransform getZoomTransform(double zoomFactor)
  {
    // move the origin so that the center of the
    // screen before the zoom stays the center after the zoom
    Dimension canvasDim = getSize();
    double canWidth = canvasDim.width;
    double canHeight = canvasDim.height;
    double centerX = canWidth / 2.0 - (canWidth / 2.0) * zoomFactor;
    double centerY = canHeight / 2.0 - (canHeight / 2.0) * zoomFactor;

    // update the _transformFromLastMark to reflect this zoom operation.
    // Be sure to include the translate to re-center
    AffineTransform transform = AffineTransform.getScaleInstance(zoomFactor, zoomFactor);
    AffineTransform preTrans = AffineTransform.getTranslateInstance(centerX, centerY);
    transform.preConcatenate(preTrans);

    return transform;
  }

  /**
   * This must be called for graphics engines that are never made visible.  Those that are made
   * visible will have the componentResized method called on them to set their rendererPanels' size
   * Some graphicsEngines are not visible -- for example, those used to automatically create a
   * panel JPG file
   * @author Andy Mechtenberg
   */
  public void setWidthAndHeight()
  {
    // set all the RendererPanels to the correct size
    Dimension dim = getSize();
    for (RendererPanel rendererPanel : _layerNumberToRendererPanelMap.values())
    {
      rendererPanel.setSize(dim);
    }
  }

  /**
   * Invoked when the GraphicsEngine's size changes.  When this classes size changes
   * the fitGraphicsToScreen() method will be called to resize the graphics automatically.
   * @author Andy Mechtenberg
   */
  public void componentResized(ComponentEvent componentEvent)
  {
    Assert.expect(componentEvent != null);

    // set all the RendererPanels to the correct size
    Dimension dim = getSize();
    for (RendererPanel rendererPanel : _layerNumberToRendererPanelMap.values())
    {
      rendererPanel.setSize(dim);
    }

    // if we already normalized things, fit the graphics to the new screen size

    if (_transformApplied)
    {
      if(_shouldGraphicsBeCentered)
        center();
      else if(_shouldGraphicsBeFitToScreen)
      {
        fitGraphicsToScreenWhenWindowIsResized();
      }
      _mouseEventPanel.setSize(getSize());
      updateAll();
    }

  }

  /**
   * Invoked when the GraphicsEngine's position changes.
   * @author Bill Darbie
   */
  public void componentMoved(ComponentEvent componentEvent)
  {
    // do nothing
  }

  /**
   * Invoked when the GraphicsEngine has been made visible.
   * @author Bill Darbie
   */
  public void componentShown(ComponentEvent componentEvent)
  {
    // do nothing
  }

  /**
   * Invoked when the GraphicsEngine has been made invisible.
   * @author Bill Darbie
   */
  public void componentHidden(ComponentEvent componentEvent)
  {
    // do nothing
  }

  /**
   * @author George A. David
   */
  public Rectangle2D getVisibleRectangleInRendererCoordinates()
  {
    Assert.expect(_transformFromRendererCoords != null);

    AffineTransform inverse = null;
    try
    {
      inverse = _transformFromRendererCoords.createInverse();
    }
    catch (NoninvertibleTransformException ex)
    {
      ex.printStackTrace();
      Assert.expect(false);
    }
    return inverse.createTransformedShape(getBounds()).getBounds2D();
  }

  /**
   * @author George A. David
   */
  public Rectangle2D getVisibleRectangleInRendererCoordinates(int layerNumber)
  {
    Assert.expect(_transformFromRendererCoords != null);

    AffineTransform transform = null;
    try
    {
      transform = _transformFromRendererCoords.createInverse();

      // ok, if there was an initial transform, take the inverse of that
      if(_layerNumberToInitialTransformMap.containsKey(layerNumber))
        transform.preConcatenate(_layerNumberToInitialTransformMap.get(layerNumber).createInverse());
    }
    catch (NoninvertibleTransformException ex)
    {
      Assert.logException(ex);
    }

    return transform.createTransformedShape(getBounds()).getBounds2D();
  }

  /**
   * Given x,y coordinates in pixels, return out a Point in Renderer coordinates.
   * @return a Point in Renderer coordinates transformed from pixel coordinates
   * @author Bill Darbie
   */
  public Point2D convertFromPixelToRendererCoordinates(int xInPixels, int yInPixels)
  {
    Assert.expect(_transformFromRendererCoords != null);
  
    Point2D point = new Point2D.Double(xInPixels, yInPixels);
    AffineTransform inverse = null;
    try
    {
      inverse = _transformFromRendererCoords.createInverse();
    }
    catch (NoninvertibleTransformException ex)
    {
      ex.printStackTrace();
      Assert.expect(false);
    }
   
    inverse.transform(point, point);
 
    return point;
  }
 
  /**
   * Given x,y coordinates in pixels based on a mouse event, return out a Point in
   * pixels after removing the border area of the graphics engine
   * @return a Point in Pixel coordinates transformed from pixel coordinates
   * @author Andy Mechtenberg
   */
  public Point convertFromMousePixelToRendererPixel(int xInPixels, int yInPixels)
  {
    int xPixel = xInPixels;
    int yPixel = yInPixels;

    Point point = new Point(xPixel, yPixel);

    return point;
  }

  /**
   * @author George A. David
   */
  public Point2D getOffsetFromMarkInPixels()
  {
    Assert.expect(_transformFromLastMark != null);

    // we wnat to know how many pixels it moved from the mark,
    // so if a zoom was applied, we need to account for that
    // by "un-zooming" it, then counting the pixels.
    Assert.expect(_transformFromLastMark.getScaleX() == _transformFromLastMark.getScaleY());
    double zoomFactor = 1.0 / _transformFromLastMark.getScaleX();
    AffineTransform transform = getZoomTransform(zoomFactor);
    transform.preConcatenate(_transformFromLastMark);

    Point2D offset = new Point2D.Double(0, 0);
    transform.transform(offset, offset);

    return offset;
  }

  /**
   * @author George A. David
   */
  public Point2D getOffsetFromMarkInRendererCoordinates()
  {
    Assert.expect(_transformFromLastMark != null);

    Point2D point1 = convertFromPixelToRendererCoordinates(0, 0);

    Point2D offset = getOffsetFromMarkInPixels();

    Point2D point2 = convertFromPixelToRendererCoordinates((int)offset.getX(), (int)offset.getY());
    point1.setLocation(point2.getX() - point1.getX(), point2.getY() - point1.getY());
    return point1;
  }

  /**
   * Given x,y coordinates in Renderer coordinates, return the same point in the current pixel space.
   * @return the coordinates passed in after being transformed from Renderer coordinates to pixel
   *             coordinates
   */
  public Point2D convertFromRendererToPixelCoordinates(double x, double y)
  {
    Assert.expect(_transformFromRendererCoords != null);
    
    Point2D point = new Point2D.Double(x, y);
    _transformFromRendererCoords.transform(point, point);
   
    return point;
  }

  /**
   * Select all the Renderers that are already visible and that lie completely within the rectangle specifed.
   * Color with the select color each one and send each Renderer through the _selectedRendererObservable
   * @author Bill Darbie
   */
  public void selectVisibleRenderersInRectangle(int xInPixels, int yInPixels, int widthInPixels, int heightInPixels,
                                                boolean addToCurrentSelection)
  {
    if (addToCurrentSelection == false)
    {
      _currentlySelectedRendererSet.clear();
    }

    Rectangle rect = new Rectangle(xInPixels, yInPixels, widthInPixels, heightInPixels);

    for (RendererPanel rendererPanel : _layerNumberToRendererPanelMap.values())
    {
      if (rendererPanel.isVisible())
      {
        Component[] components = rendererPanel.getComponents();
        for (int i = 0; i < components.length; ++i)
        {
          Renderer renderer = (Renderer)components[i];
          Rectangle2D bounds = renderer.getBounds2D();
          if (rect.contains(bounds))
          {
            // the renderer falls completely within the rectangle specified, so select it
            // by setting it to its select color and notifying any Observers
            if (renderer.isSelectable())
            {
              _currentlySelectedRendererSet.add(renderer);          
            }
          }
        }
      }
    }
    _multipleSelectOn = true;
    _selectedRendererObservable.setSelectedRenderer(new ArrayList<Renderer>(_currentlySelectedRendererSet));
    
   // if(this.isMouseRightClickMode())
   //    _unselectedDragOpticalRegionRendererObservable.setUnselectedDragOpticalRegionRenderer(new ArrayList<Renderer>(_currentlySelectedRendererSet));
  }
  
   /**
   * Select all the Renderers that are already visible and that lie completely within the rectangle specifed.
   * Color with the select color each one and send each Renderer through the _selectedRendererObservable
   * @author Jack Hwee
   */
  public void selectDragOpticalRegionRenderersInRectangle(int xInPixels, int yInPixels, int widthInPixels, int heightInPixels,
                                                boolean addToCurrentSelection)
  {
    if (addToCurrentSelection == false)
    {
      _currentlySelectedRendererSet.clear();
      _currentlySelectedComponentRendererSet.clear();
    }

    Rectangle rect = new Rectangle(xInPixels, yInPixels, widthInPixels, heightInPixels);

    for (RendererPanel rendererPanel : _layerNumberToRendererPanelMap.values())
    {
      if (rendererPanel.isVisible())
      {
        Component[] components = rendererPanel.getComponents();
        for (int i = 0; i < components.length; ++i)
        {
          Renderer renderer = (Renderer)components[i];
          Rectangle2D bounds = renderer.getBounds2D();
          if (rect.contains(bounds))
          {
            // the renderer falls completely within the rectangle specified, so select it
            // by setting it to its select color and notifying any Observers
            if (renderer.isSelectable())
            {
              _currentlySelectedRendererSet.add(renderer);
              _currentlySelectedComponentRendererSet.add(renderer);
            }
          }
        }
      }
    }
    _multipleSelectOn = true;
    _dragOpticalRegionObservable.setDragOpticalRegionRenderer(new ArrayList<Renderer>(_currentlySelectedRendererSet));
    _unselectedOpticalRegionRendererObservable.setUnselectedOpticalRegionRenderer(_currentlySelectedRendererSet);
    _unselectedDragOpticalRegionRendererObservable.setUnselectedDragOpticalRegionRenderer(_currentlySelectedRendererSet);
  
  }
  
    /**
   * Select all the Renderers that are already visible and that lie completely within the rectangle specifed.
   * Color with the select color each one and send each Renderer through the _selectedRendererObservable
   * @author Jack Hwee
   */
  public void selectDragUnselectOpticalRegionRenderersInRectangle(int xInPixels, int yInPixels, int widthInPixels, int heightInPixels,
                                                boolean addToCurrentSelection)
  {
    if (addToCurrentSelection == false)
    {
      _currentlySelectedRendererSet.clear();
    }

    Rectangle rect = new Rectangle(xInPixels, yInPixels, widthInPixels, heightInPixels);

    for (RendererPanel rendererPanel : _layerNumberToRendererPanelMap.values())
    {
      if (rendererPanel.isVisible())
      {
        Component[] components = rendererPanel.getComponents();
        for (int i = 0; i < components.length; ++i)
        {
          Renderer renderer = (Renderer)components[i];
          Rectangle2D bounds = renderer.getBounds2D();
          if (rect.contains(bounds))
          {
            // the renderer falls completely within the rectangle specified, so select it
            // by setting it to its select color and notifying any Observers
            if (renderer.isSelectable())
            {
              _currentlySelectedRendererSet.add(renderer);
            }
          }
        }
      }
    }
    _multipleSelectOn = true;
   
    _unselectedDragOpticalRegionRendererObservable.setUnselectedDragOpticalRegionRenderer(new ArrayList<Renderer>(_currentlySelectedRendererSet));
    
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayPadName(boolean setPadNameVisible)
  {
      _displayPadNameObservable.setDisplayPadName(setPadNameVisible);
  }
  
  /**
   * Author Kee Chin Seong
   */
  public void displayNoLoadComponent(boolean setNoloadComponentVisible)
  {
     _showNoLoadComponentObservable.setNoLoadComponentVisible(setNoloadComponentVisible);
  }
  
  /**
   * @author Janan Wong
   */
  public void displayUntestableArea(boolean setUntestableAreaVisible)
  {
    _showUntestableAreaObservable.setUntestableAreaVisible(setUntestableAreaVisible);
  }

  /**
   * @author George A. David
   */
  public void setShouldGraphicsBeFitToScreen(boolean shouldGraphicsBeFitToScreen)
  {
    _shouldGraphicsBeFitToScreen = shouldGraphicsBeFitToScreen;
  }

  /**
   * Cause all the Renderers to fit to the current screen size.
   *
   * @author Bill Darbie
   */
  public void fitGraphicsToScreen()
  {
    fitGraphicsToScreen(false);
  }

  /**
   * Cause all the Renderers on visible layers to fit to the current screen size.
   *
   * @author Bill Darbie
   */
  public void fitVisibleGraphicsToScreen()
  {
    fitGraphicsToScreen(true);
  }

  /**
   * Display the portion of the graphics specified in the Renderer coordinates passed in.
   * @author Bill Darbie
   */
  public void fitRectangleInRendererCoordinatesToScreen(double x, double y, double width, double height)
  {
    Rectangle2D rect = new Rectangle.Double(x, y, width, height);
    fitRectangleInRendererCoordinatesToScreen(rect);
  }

  /**
   * @author George A. David
   */
  public void fitRectangleInRendererCoordinatesToScreen(Rectangle2D rectangle)
  {
    // convert from renderer coordinates to pixels
    Shape shape = _transformFromRendererCoords.createTransformedShape(rectangle);
    Rectangle2D bounds = shape.getBounds2D();

    fitRectangleInPixelsToScreen(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
  }

  /**
   * Cause the rectangle specified (in current pixel coordinates) to be visible on the screen.
   *
   * @return false if the new Rectangle is so small that the amount of zooming required
   *         would cause a loss of Renderer data.  In this case the fit call will not do anything.
   * @author Bill Darbie
   */
  public boolean fitRectangleInPixelsToScreen(double x, double y, double width, double height)
  {
    // calculate the new scale factor
    double newScale = calcNewScale(width, height);
    // check if the new scale factor is not too large
    if (isNewScaleOk(newScale) == false)
    {
      return false;
    }

    Dimension canvasDim = getSize();
    double canWidth = canvasDim.width;
    double canHeight = canvasDim.height;

    // figure out the offset need to center the rectangle on the screen before the new
    // scale factor is applied
    double xOffsetToCenter = (canWidth - width) / 2.0 - x;
    double yOffsetToCenter = (canHeight - height) / 2.0 - y;

    // figure out how much to translate the drawing after the new scale factor is applied
    // to make the old (pre scale center) become the new center (post scale)
    double centerX = canWidth / 2.0 - (canWidth / 2.0) * newScale;
    double centerY = canHeight / 2.0 - (canHeight / 2.0) * newScale;

    // update the _transformFromLastMark to reflect this zoom operation.
    // Be sure to include the translate to re-center
    // first translate to get the box centered
    AffineTransform transform = AffineTransform.getTranslateInstance(xOffsetToCenter, yOffsetToCenter);
    // then scale things up to the new size
    AffineTransform preTrans = AffineTransform.getScaleInstance(newScale, newScale);
    transform.preConcatenate(preTrans);
    // then translate to get things centered after the scale operation
    preTrans = AffineTransform.getTranslateInstance(centerX, centerY);
    transform.preConcatenate(preTrans);

    transform(transform);    
    // notify observers
    _graphicsEngineObservable.notifyGraphicsEvent(new GraphicsEngineEvent(null, GraphicsEngineEventEnum.ZOOM_RECTANGLE_EVENT_FINISHED));
    return true;
  }

  /**
   * Calculate a new scale factor that will make the rectangle specified fit to the screen
   * size without clipping any of it off the screen, and apply an extra border.
   * @author Bill Darbie
   */
  private double calcNewScale(double width, double height)
  {
    // we will be dividing by width and height, so make sure they are not zero
    Assert.expect(width > 0);
    Assert.expect(height > 0);

    // calculate the canvasRatio
    // -1 is needed to make sure the outermost Renderer does not get clipped
    double newCanvasWidth = _layeredPane.getWidth() - 1 - _pixelBorderSize;
    double newCanvasHeight = _layeredPane.getHeight() - 1 - _pixelBorderSize;
    if (newCanvasWidth < 1)
    {
      newCanvasWidth = 1;
    }
    if (newCanvasHeight < 1)
    {
      newCanvasHeight = 1;
    }

    Assert.expect(newCanvasHeight != 0);
    double canvasWidthToHeightRatio = (double)newCanvasWidth / (double)newCanvasHeight;

    Assert.expect(height != 0);
    // calculate the RendererDataRatio
    double rendererDataWidthToHeightRatio = (double)width / (double)height;

    _canvasWidthToHeightRatio = canvasWidthToHeightRatio;
    double newScale = 0.0;
    if (rendererDataWidthToHeightRatio > canvasWidthToHeightRatio)
    {
      newScale = (double)newCanvasWidth / (double)width;
    }
    else
    {
      newScale = (double)newCanvasHeight / (double)height;
    }

    Assert.expect(newScale > 0.0);
    return newScale;
  }


  /**
   * Call this to fit all the Renderers on the canvas.
   *
   * @param fitToVisibleLayersOnly if true only layers that are visible will be fit on the current screen
   *                               otherwise all layers will be fit to the screen.
   *
   * @author Andy Mechtenberg
   */
  private void fitGraphicsToScreen(boolean fitToVisibleLayersOnly)
  {
    calcCurrentBounds(fitToVisibleLayersOnly);
    Rectangle2D bounds = getCurrentBounds();
    double rendererDataWidth = bounds.getWidth();
    double rendererDataHeight = bounds.getHeight();
    double newScale = calcNewScale(rendererDataWidth, rendererDataHeight);

    AffineTransform transform = AffineTransform.getScaleInstance(newScale, newScale);
    transform(transform);
    center();
    _transformFromFitToScreen.setToIdentity();
  }

  /**
   * @return True if the graphics are currently at 1 to 1 (fully zoomed)
   * @author Andy Mechtenberg
   */
  public boolean areGraphicsAtFullWindowSize()
  {
    double scaleX = Math.abs(_transformFromFitToScreen.getScaleX());
    double scaleY = Math.abs(_transformFromFitToScreen.getScaleY());

    // a rotation transform will have a scaleX and scaleY of 0, so set them to 1 instead
    if (scaleX == 0)
    {
      scaleX = 1;
    }
    if (scaleY == 0)
    {
      scaleY = 1;
    }


    if (MathUtil.fuzzyEquals(scaleX, 1.0) == true)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * @return the current zoom factor.  A factor of 1 means we're at full screen size
   */
  public double getCurrentZoomFactor()
  {
    double scaleX = Math.abs(_transformFromFitToScreen.getScaleX());
    double scaleY = Math.abs(_transformFromFitToScreen.getScaleY());

    // a rotation transform will have a scaleX and scaleY of 0, so set them to 1 instead
    if (scaleX == 0)
    {
      scaleX = 1;
    }
    if (scaleY == 0)
    {
      scaleY = 1;
    }

    return scaleX;
  }

  /**
   * Call this to fit all the Renderers on the canvas.
   * @author Andy Mechtenberg
   */
  public void fitGraphicsToScreenWhenWindowIsResized()
  {
    // if the user is zoomed in, and changed the size of the window, we'll keep the same scale factor, just show
    // more of the graphics.
    if (areGraphicsAtFullWindowSize() == false)
    {
      if (_isDragRegionMode == true)
      {
          if (MathUtil.fuzzyEquals(this.getCurrentZoomFactor(), 1.0))
          {
              _mouseEventPanel.repaint();

              this.setOpticalRegionFirstDrag(false);
              Rectangle2D bounds2 = _rect.getBounds2D();
              double rendererDataWidth2 = bounds2.getWidth();
              double rendererDataHeight2 = bounds2.getHeight();
              double newScale2 = calcNewScale(rendererDataWidth2, rendererDataHeight2);

              _fitGraphicsToScreenWhenWindowIsResizedNewScale = newScale2;   
          }
          
      }   
      return;
    }

    calcCurrentBounds(false);
    Rectangle2D bounds = getCurrentBounds();
    double rendererDataWidth = bounds.getWidth();
    double rendererDataHeight = bounds.getHeight();
    double newScale = calcNewScale(rendererDataWidth, rendererDataHeight);

     
    if (_fitGraphicsToScreenWhenWindowIsResizedNewScale == 0)
         _fitGraphicsToScreenWhenWindowIsResizedNewScale = newScale;

 //  _fitGraphicsToScreenWhenWindowIsResizedNewScale = newScale;
 
    // if the scale hasn't changed, do nothing.  If the newScale is 1 that means that
    // the scale did not change
    if (MathUtil.fuzzyEquals(newScale, 1.0))
    {
      // the window size has changed, so center things only
      center();
      
      if (_isDragRegionMode == true)
      {
          _mouseEventPanel.repaint();     
          
          if (_rect != null)
          {
              Rectangle2D bounds2 = _rect.getBounds2D();
              double rendererDataWidth2 = bounds2.getWidth();
              double rendererDataHeight2 = bounds2.getHeight();
              double newScale2 = calcNewScale(rendererDataWidth2, rendererDataHeight2);

              _fitGraphicsToScreenWhenWindowIsResizedNewScale = newScale2;
          }
          this.setOpticalRegionFirstDrag(false);
          
          _dragOpticalRegionObservable.setDragOpticalRegionRenderer((_currentlySelectedComponentRendererSet));

          _unselectedOpticalRegionRendererObservable.setUnselectedOpticalRegionRenderer(_currentlySelectedComponentRendererSet);

          _unselectedDragOpticalRegionRendererObservable.setUnselectedDragOpticalRegionRenderer(_currentlySelectedComponentRendererSet);
        
      }
      return;
    }
    else  // if window is resized we need to update the observable for surface map optical region
    {
      if (_isDragRegionMode == true)
      {
          _mouseEventPanel.repaint();
   
          if (_rect != null)
          {
          Rectangle2D bounds2 = _rect.getBounds2D();
          double rendererDataWidth2 = bounds2.getWidth();
          double rendererDataHeight2 = bounds2.getHeight();
          double newScale2 = calcNewScale(rendererDataWidth2, rendererDataHeight2);

          _fitGraphicsToScreenWhenWindowIsResizedNewScale = newScale2;
          }
          this.setOpticalRegionFirstDrag(false);
          
          _dragOpticalRegionObservable.setDragOpticalRegionRenderer((_currentlySelectedComponentRendererSet));

          _unselectedOpticalRegionRendererObservable.setUnselectedOpticalRegionRenderer(_currentlySelectedComponentRendererSet);

          _unselectedDragOpticalRegionRendererObservable.setUnselectedDragOpticalRegionRenderer(_currentlySelectedComponentRendererSet);
        
      }
    }
   
    AffineTransform transform = AffineTransform.getScaleInstance(newScale, newScale);
    transform(transform);
    center();
    _transformFromFitToScreen.setToIdentity();
  }

  /**
   * @author Bill Darbie
   */
  private void applyAxisTransformIfNeeded()
  {
    // apply axis transform if needed
    // This should be the first transform applied before any other transforms!
    if (_transformApplied == false)
    {
      Assert.expect(_setAxis, "the setAxisInterpretation() method must be called first");

      _transformFromRendererCoords.setToIdentity();
      _transformFromAxisTransform.setToIdentity();
      _transformFromFitToScreen.setToIdentity();
      _transformFromLastMark.setToIdentity();

      Rectangle2D maxBounds = getCurrentBounds();
      _axisTransformXTranslation = maxBounds.getWidth();
      _axisTransformYTranslation = maxBounds.getHeight();

      if (_positiveXisToTheRightOfTheOrigin == false)
      {
        _transformFromRendererCoords.preConcatenate(AffineTransform.getScaleInstance( -1.0, 1.0));
        _transformFromRendererCoords.preConcatenate(AffineTransform.getTranslateInstance(_axisTransformXTranslation,
            0.0));
      }

      if (_positiveYisBelowTheOrigin == false)
      {
        _transformFromRendererCoords.preConcatenate(AffineTransform.getScaleInstance(1.0, -1.0));
        _transformFromRendererCoords.preConcatenate(AffineTransform.getTranslateInstance(0.0,
            _axisTransformYTranslation));
      }

      // Turn off renderer visibility during transform for better performance
      Set<RendererPanel> panelsWithChangedVisibility = turnOffRendererVisibility();

      // Now apply the axis transform to all renderers.  Note that the axis transform may be
      // implemented differently for different types of renderers so we should not just
      // call transform() with the default axis transform.  The applyAxisTransform method should do
      // the right thing.
      for (RendererPanel rendererPanel : _layerNumberToRendererPanelMap.values())
      {
        rendererPanel.applyAxisTransform(_positiveXisToTheRightOfTheOrigin, _positiveYisBelowTheOrigin,
                                         _axisTransformXTranslation, _axisTransformYTranslation);
      }


      // now make things visible
      turnOnRendererVisibility(panelsWithChangedVisibility);
    }
  }

  /**
   * Set the layers to drag when dragging the graphics. Otherwise, all
   * layers are dragged by default.
   * Note: If you set the layers to drag to exclude some layers, the exluded layers
   * (those that were not dragged) will give incorrect results when asking for
   * coordinates.
   * @author George A. David
   */
  public void setLayersToUseWhenDraggingGraphics(Collection<Integer> layersToUseWhenDraggingGraphics)
  {
    Assert.expect(layersToUseWhenDraggingGraphics != null);

    _layersToUseWhenDraggingGraphics.clear();
    _layersToUseWhenDraggingGraphics.addAll(layersToUseWhenDraggingGraphics);
  }

  /**
   * @author Bill Darbie
   */
  private void addSpecialEventPanelsIfNeeded()
  {
    // add mouseEventPanel if needed
    if (_transformApplied == false)
    {
      // now that we know all layers and Renderers are set up add the _mouseEventPanel
      Rectangle bounds = getBounds();
      int width = bounds.width;
      int height = bounds.height;
      addCrossHairPanel(width, height);
      addMouseEventPanel(width, height);
    }
  }

  /**
   * View all Renderers from the top down.
   * @author Andy Mechtenberg
   */
  public void viewFromTop()
  {
    if (_isViewedFromTop)
    {
      return; // do nothing, we are already viewing from top
    }

    flipLeftToRight();
    _isViewedFromTop = true;
  }

  /**
   * View all Renderers from the bottom side.
   * @author Andy Mechtenberg
   */
  public void viewFromBottom()
  {
    if (_isViewedFromTop == false)
    {
      return; // do nothing, we are already viewing from bottom
    }

    flipLeftToRight();
    _isViewedFromTop = false;
  }

  /**
   * Set an entire layer to flash when flashing is enabled.
   * @param layerNumber the layer to flash
   * @param flashColor is the color to flash
   * @author Bill Darbie
   */
  public void setFlashLayer(int layerNumber, Color flashColor)
  {
    Assert.expect(flashColor != null);

    RendererPanel rendererPanel = getRendererPanel(layerNumber);
    synchronized (_rendererMapsLock)
    {
      if (_rendererPanelToFlashColorMap.containsKey(rendererPanel) == false)
        _rendererPanelToFlashColorMap.put(rendererPanel, flashColor);

      // make sure that the renderer.getForeground() is not called unless the
      // renderer is not already flashing.  If it is already flashing, the getForeground()
      // call will sometimes return the flash color instead of the original color!
      if (_rendererPanelToOrigColorMap.containsKey(rendererPanel) == false)
      {
        // if the Renderer is already selected, make sure to find it's original color from
        // that map, otherwise use the current foreground color as the original color.
        Color origColor = _selectedRendererToOrigColorMap.get(rendererPanel);
        if (origColor == null)
          origColor = rendererPanel.getForeground();
        _rendererPanelToOrigColorMap.put(rendererPanel, origColor);
      }
    }
  }

  /**
   * Clear the passed in layer so it will stop flashing.
   * @param layerNumber the layer to stop from flashing
   * @author Bill Darbie
   */
  public void clearFlashLayer(int layerNumber)
  {
    RendererPanel rendererPanel = getRendererPanel(layerNumber);
    synchronized (_rendererMapsLock)
    {
      Color origColor = (Color)_rendererPanelToOrigColorMap.get(rendererPanel);
      _rendererPanelToFlashColorMap.remove(rendererPanel);
      _rendererPanelToOrigColorMap.remove(rendererPanel);
      rendererPanel.setForeground(origColor);
    }
  }

  /**
   * Set up a particular renderer so it will flash when flashing is enabled.
   * @author Bill Darbie
   */
  public void setFlashRenderer(Renderer renderer, Color flashColor)
  {
    Assert.expect(renderer != null);
    Assert.expect(flashColor != null);

    synchronized (_rendererMapsLock)
    {
      if (_rendererToFlashColorMap.containsKey(renderer) == false)
        _rendererToFlashColorMap.put(renderer, flashColor);

      // make sure that the renderer.getForeground() is not called unless the
      // renderer is not already flashing.  If it is already flashing, the getForeground()
      // call will sometimes return the flash color instead of the original color!
      if (_rendererToOrigColorMap.containsKey(renderer) == false)
      {
        // if the Renderer is already selected, make sure to find it's original color from
        // that map, otherwise use the current foreground color as the original color.
        Color origColor = _highlightedRendererToOrigColorMap.get(renderer);
        if (origColor == null)
          origColor = _selectedRendererToOrigColorMap.get(renderer);
        if (origColor == null)
          origColor = renderer.getForeground();
        _rendererToOrigColorMap.put(renderer, origColor);
      }
    }
  }

  /**
   * Clear the passed in Renderer so it will stop flashing.
   * @author Bill Darbie
   */
  public void clearFlashRenderer(Renderer renderer)
  {
    Assert.expect(renderer != null);

    synchronized (_rendererMapsLock)
    {
      Color origColor = (Color)_rendererToOrigColorMap.get(renderer);
      _rendererToFlashColorMap.remove(renderer);
      _rendererToOrigColorMap.remove(renderer);

      renderer.setForeground(origColor);
    }
  }

  /**
   * Set up a particular renderer so it will flash when flashing is enabled.
   * @author Bill Darbie
   */
  public void setFlashRenderers(Collection<? extends Renderer> renderers, Color flashColor)
  {
    Assert.expect(renderers != null);
    Assert.expect(flashColor != null);

    for (Renderer renderer : renderers)
    {
      setFlashRenderer(renderer, flashColor);
    }
  }

  /**
   * Clear all flashing renderers.
   * @author Bill Darbie
   */
  public void clearAllFlashingRenderersAndLayers()
  {
    setFlashing(false);
    synchronized (_rendererMapsLock)
    {
      _rendererPanelToFlashColorMap.clear();
      _rendererPanelToOrigColorMap.clear();
      _rendererToFlashColorMap.clear();
      _rendererToOrigColorMap.clear();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void markRendererCopyIncomplete()
  {
    synchronized (_rendererMapsLock)
    {
      _rendererCopyComplete = false;
    }
  }

  /**
   * @author Bill Darbie
   */
  private void markRendererCopyComplete()
  {
    synchronized (_rendererMapsLock)
    {
      _rendererCopyComplete = true;
      _rendererMapsLock.notifyAll();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void waitForRendererMapCopyToComplete()
  {
    while (_rendererCopyComplete == false)
    {
      synchronized (_rendererMapsLock)
      {
        try
        {
          _rendererMapsLock.wait(200);
        }
        catch (InterruptedException ie)
        {
          // do nothing
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private boolean isFlashingEnabled()
  {
    return _enableFlashing;
  }

  /**
   * @author Bill Darbie
   */
  private void enableFlashing()
  {
    synchronized (_flashingLock)
    {
      _enableFlashing = true;
    }
  }

  /**
   * @author Bill Darbie
   */
  private void disableFlashing()
  {
    synchronized (_flashingLock)
    {
      _enableFlashing = false;
      _flashingLock.notifyAll();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void waitForFlashingToStop()
  {
    while (_flashing)
    {
      synchronized (_flashingLock)
      {
        try
        {
          _flashingLock.wait(200);
        }
        catch (InterruptedException ie)
        {
          // do nothing
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private void notifyThatFlashingHasStopped()
  {
    synchronized (_flashingLock)
    {
      _flashing = false;
      _flashingLock.notifyAll();
    }
  }

  /**
   * Turn flashing on or off for the currently set renders.
   * @param enabled should be true to start flashing, false to stop
   * @author Bill Darbie
   */
  public void setFlashing(boolean enabled)
  {
    if (enabled == false)
    {
      // disable flashing
      disableFlashing();
      // wait for the current task to stop flashing so we do not build up a list of tasks
      // that want to flash.  If we did build up a list of tasks it could cause the code
      // to think that the original color of a Renderer were the flashing color, because
      // we call Renderer.getForeground() in the setFlashRenderer() method  to find ou
      //  the original color, so we cannot have something flashing when that happens.
      waitForFlashingToStop();
    }
    else
    {
      // make sure that the copy from the previous call is done before we allow another
      // call to come into here, otherwise the copy of the renderers could be messed up
      waitForRendererMapCopyToComplete();

      // OK, now start flashing
      _flashWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          // the block of code between the markRendererCopyIncomplete() and the markRendererCopyComplete()
          // must finish before another thread comes into this method.  The waitForRendererMapCopyToComplete()
          // call at the beginning of this method makes sure that happens.  That way a complete copy
          // of correct RendererMaps is guaranteed to be made before another task to flash renderers
          // is started.
          markRendererCopyIncomplete();
          Map<RendererPanel, Color> rendererPanelToFlashColorMap = new HashMap<RendererPanel, Color>(_rendererPanelToFlashColorMap);
          Map<RendererPanel, Color> rendererPanelToOrigColorMap = new HashMap<RendererPanel, Color>(_rendererPanelToOrigColorMap);
          Map<Renderer, Color> rendererToFlashColorMap = new HashMap<Renderer, Color>(_rendererToFlashColorMap);
          Map<Renderer, Color> rendererToOrigColorMap = new HashMap<Renderer, Color>(_rendererToOrigColorMap);

          enableFlashing();
          markRendererCopyComplete();

          while (isFlashingEnabled())
          {
            // set to the flash color
            for (Map.Entry entry : rendererPanelToFlashColorMap.entrySet())
            {
              final RendererPanel rendererPanel = (RendererPanel)entry.getKey();
              final Color flashColor = (Color)entry.getValue();
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  rendererPanel.setForeground(flashColor);
                  rendererPanel.repaint();
                }
              });
            }
            for (Map.Entry entry : rendererToFlashColorMap.entrySet())
            {
              final Renderer renderer = (Renderer)entry.getKey();
              final Color flashColor = (Color)entry.getValue();
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  renderer.setForeground(flashColor);
                  renderer.repaint();
                }
              });
            }

            // sleep
            synchronized (_flashingLock)
            {
              try
              {
                _flashingLock.wait(500);
              }
              catch (InterruptedException ie)
              {
                // do nothing
              }
            }

            // set to the original color
            for (Map.Entry entry : rendererPanelToOrigColorMap.entrySet())
            {
              final RendererPanel rendererPanel = (RendererPanel)entry.getKey();
              final Color origColor = (Color)entry.getValue();
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  rendererPanel.setForeground(origColor);
                  rendererPanel.repaint();
                }
              });
            }
            for (Map.Entry entry : rendererToOrigColorMap.entrySet())
            {
              final Renderer renderer = (Renderer)entry.getKey();
              final Color origColor = (Color)entry.getValue();
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  renderer.setForeground(origColor);
                  renderer.repaint();
                }
              });
            }

            // is isInterrupted so the interrupt flag is not reset for other tasks
            if (isFlashingEnabled() == false)
            {
              // just break now since the colors are original
              break;
            }

            // sleep
            synchronized (_flashingLock)
            {
              try
              {
                _flashingLock.wait(500);
              }
              catch (InterruptedException ie)
              {
                // do nothing
              }
            }
          }
          notifyThatFlashingHasStopped();
        }
      });
    }
  }

  /**
   * After all your Renderers have been added to the GraphicsEngine call this method
   * to define how your data interprets x and y coordinates.
   *
   * @author Bill Darbie
   */
  public void setAxisInterpretation(boolean positiveXisToTheRightOfTheOrigin, boolean positiveYisBelowTheOrigin)
  {
    _setAxis = true;
    _positiveXisToTheRightOfTheOrigin = positiveXisToTheRightOfTheOrigin;
    _positiveYisBelowTheOrigin = positiveYisBelowTheOrigin;
  }

  /**
   * Mark the current state of the graphics.  resetToMark() will return the
   * Rendererers to this state.
   *
   * @author Bill Darbie
   */
  public void mark()
  {
    _transformFromLastMark.setToIdentity();
  }

  /**
   * Reset the drawing back to the last marked state.
   * @author Bill Darbie
   */
  public void resetToMark()
  {
    // get the inverse transform to go back to normalized
    AffineTransform inverseTrans = null;
    try
    {
      inverseTrans = _transformFromLastMark.createInverse();
    }
    catch (NoninvertibleTransformException ex)
    {
      ex.printStackTrace();
      Assert.expect(false);
    }

    transform(inverseTrans);
  }

  /**
   * @author George A. David
   */
  public void resetDraggedLayersToMark()
  {
    _isDraggingGraphics = true;
    resetToMark();
    _isDraggingGraphics = false;
  }

  /**
   * This is called when a new shape is created, and needs to be places at the proper size and position
   * in the current canvas.  Use this method only if you want to add Renderers after they are already
   * being displayed.  Otherwise use the addRenderer() methods.
   * @author Andy Mechtenberg
   * @author Bill Darbie
   */
  private void placeInCurrentGraphics(int layerNumber, Renderer renderer)
  {
    Assert.expect(renderer != null);

    // set the renderers color
    RendererPanel rendererPanel = (RendererPanel)_layerNumberToRendererPanelMap.get(new Integer(layerNumber));
    Color color = rendererPanel.getForeground();
    renderer.setForeground(color);

    if(_layerNumberToInitialTransformMap.containsKey(layerNumber))
      renderer.transform(_layerNumberToInitialTransformMap.get(layerNumber));
    // apply the current transform to the renderer
    // Note: it is very important to call applyAxisTransform and then apply the transform
    // _transformFromAxisTransform instead of trying to apply _transformFromRendererCoords.
    // The reason is that different renderers may need to use different axis transforms
    // (for instance, images need a different transform than shapes).  The one that the
    // graphics engine keeps should not be applied directly to the renderers.
    renderer.applyAxisTransform(_positiveXisToTheRightOfTheOrigin, _positiveYisBelowTheOrigin,
                                _axisTransformXTranslation, _axisTransformYTranslation);

    renderer.transform(_transformFromAxisTransform);

  }

  /**
   * @author George A. David
   */
  public void applyAllTransforms(Renderer renderer)
  {
    renderer.applyAxisTransform(_positiveXisToTheRightOfTheOrigin, _positiveYisBelowTheOrigin,
                                _axisTransformXTranslation, _axisTransformYTranslation);
    renderer.transform(_transformFromAxisTransform);
  }

  /**
   * @return the smallest bounding Rectangle that contains all (or just the visible) Renderers in it.
   * @author Bill Darbie
   */
  private Rectangle2D getCurrentBounds()
  {
    if (_maxBounds == null)
    {
      calcCurrentBounds(false);
    }

    // now transform it to the current state
    Shape shape = _transformFromRendererCoords.createTransformedShape(_maxBounds);
    Rectangle2D maxBounds = shape.getBounds2D();

    return maxBounds;
  }

  /**
   * @param layers Collection of layers to use when calculating max bounds.  If empty, use all layers
   * @author Andy Mechtenberg
   */
  public void setLayersToUseWhenFittingToScreen(Collection<Integer> layers)
  {
    Assert.expect(layers != null);

    _layersToUseWhenFittingToScreen.clear();
    _layersToUseWhenFittingToScreen.addAll(layers);
  }

  /**
   * Calculate the smallest bounding Rectangle that contains all the Renderers of
   * this GraphicsEngine.
   *
   * @param visibleLayersOnly if this is true the bounding Rectangle will be calculated
   *                          to include only visible layers
   * @author Bill Darbie
   */
  private void calcCurrentBounds(boolean visibleLayersOnly)
  {
    Rectangle2D rect = null;

    Collection<RendererPanel> rendererPanels = null;
    if (_layersToUseWhenFittingToScreen.isEmpty())
    {
      // use all the layers to calculate current bounds when _layersToUseWhenFittingToScreen is empty
      rendererPanels = _layerNumberToRendererPanelMap.values();
    }
    else
    {
      // use just the layers specified by _layersToUseWhenFittingToScreen
      rendererPanels = new ArrayList<RendererPanel>(_layersToUseWhenFittingToScreen.size());
      for (Integer layer : _layersToUseWhenFittingToScreen)
      {
        RendererPanel rendererPanel = (RendererPanel)_layerNumberToRendererPanelMap.get(layer);
        Assert.expect(rendererPanel != null); // must exist!
        rendererPanels.add(rendererPanel);
      }
    }

    for (RendererPanel rendererPanel : rendererPanels)
    {
      // skip the crosshair one -- it might still be initialized to an old renderer size -- and it's not user data
      if (rendererPanel != _crossHairRendererPanel)
      {
        if ((visibleLayersOnly == false) || ((visibleLayersOnly && rendererPanel.areRenderersVisible())))
        {
          Rectangle2D rectangle = rendererPanel.getBounds2D();
          if (rectangle != null)
          {
            if (rect == null)
            {
              rect = new Rectangle2D.Double(rectangle.getX(), rectangle.getY(), rectangle.getWidth(), rectangle.getHeight());
            }
            else
            {
              rect.add(rectangle);
            }
          }
        }
      }
    }

    if(rect == null)
    {
      //ok this happened either because there aren't any renderers
      // or there aren't any visible renderers. This is a valid case. so
      // let's set it to 0, 0, 1, 1
      rect = new Rectangle2D.Double(0, 0, 1, 1);
    }

    // get the inverse and set _maxBounds to that since _maxBounds
    // should be relative to the original data
    AffineTransform inverseTrans = null;
    try
    {
      inverseTrans = _transformFromRendererCoords.createInverse();
    }
    catch (NoninvertibleTransformException ex)
    {
      ex.printStackTrace();
      Assert.expect(false);
    }

    Shape shape = inverseTrans.createTransformedShape(rect);
    _maxBounds = shape.getBounds2D();
  }

  /**
   * @return true if the Renderer passed in would be visible on the screen if it's layer
   * were visible.  Note that the Renderer may or may not be visible even when this
   * method returns true.
   * @author Bill Darbie
   */
  public boolean wouldRendererBeVisibleOnScreenIfLayerWereVisible(Renderer renderer)
  {
    Assert.expect(renderer != null);

    if (renderer.isVisible())
    {
      int canvasWidth = getWidth();
      int canvasHeight = getHeight();

      Rectangle2D rendererBounds = renderer.getBounds2D();
      if (rendererBounds.intersects(0, 0, canvasWidth, canvasHeight))
      {
        return true;
      }
    }

    return false;
  }

  /**
   * @author George A. David
   */
  public boolean isRectangleInRendererCoordinatesVisibleOnScreen(Rectangle2D rect)
  {
    Rectangle2D visibleRect = getVisibleRectangleInRendererCoordinates();
    return rect.intersects(visibleRect);
  }

  /**
   * @return true if the renderer passed in is currently visible on the screen.
   * @author Bill Darbie
   */
  public boolean isRendererVisibleOnScreen(Renderer renderer)
  {
    Assert.expect(renderer != null);

    Integer layerNumberInteger = (Integer)_rendererToLayerNumberMap.get(renderer);
    Assert.expect(layerNumberInteger != null);

    RendererPanel rendererPanel = (RendererPanel)_layerNumberToRendererPanelMap.get(layerNumberInteger);
    Assert.expect(rendererPanel != null);

    if (rendererPanel.isVisible() == false)
    {
      return false;
    }

    return wouldRendererBeVisibleOnScreenIfLayerWereVisible(renderer);
  }

  /**
   * Put cross-hairs across the center of the Renderer passed in.
   * @author Bill Darbie
   * @author Kee Chin Seong - Added in Cross Hair Control
   */
  public void selectWithCrossHairs(Renderer renderer)
  {
    Assert.expect(renderer != null);
    
    if(_isCrossHairEnable == false)
      return;
    
    _rendererToPlaceCrossHairsOn = renderer;
    updateCrossHairs();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setCrossHairsSelectionVisibility(boolean enabledCrossHair)
  {
    _isCrossHairEnable = enabledCrossHair;
    
    _crossHairRendererPanel.setVisible(false);
  }
          
  /**
   * clear out any cross hairs currently drawn.
   * @author Bill Darbie
   * @author Kee Chin Seong - Added in Cross Hair Control
   */
  public void clearCrossHairs()
  {
    if(_isCrossHairEnable == false)
      return;
    
    // if we haven't applied a transform yet, then we shouldn't do anything here yet
    if (_transformApplied == false)
    {
      return;
    }

    _crossHairRendererPanel.setVisible(false);
    _rendererToPlaceCrossHairsOn = null;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void clearMouseEvents()
  {
    setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    _mouseEventPanel.setVisible(false);
    
    _mouseEventPanel.removeAll();
  }
  
   /**
   * @author Jack Hwee
   */
  public void clearMouseEventsInSurfaceMappingScreen()
  {
    setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    if (_mouseEventPanel != null)
    {
       _mouseEventPanel.setVisible(false); 
       _mouseEventPanel.removeAll();
    }
    if (_pspCrossHairRendererPanel != null)
     {
       _pspCrossHairRendererPanel.setVisible(false);
       _pspCrossHairRendererPanel.removeAll();
     }
  }

  /**
   * Draws the current set of cross hairs if they are in use.
   * @author Bill Darbie
   */
  private void updateCrossHairs()
  {
    // if we haven't applied a transform yet, then we shouldn't do anything here yet
    if (_transformApplied == false)
    {
      return;
    }

    if (_rendererToPlaceCrossHairsOn != null)
    {
      DoubleCoordinate center = _rendererToPlaceCrossHairsOn.getCenterCoordinate();
      double xCenter = center.getX();
      double yCenter = center.getY();
      Rectangle2D currentBounds = getCurrentBounds();

      _crossHairRenderer.setCrossHair(xCenter, yCenter, currentBounds);
      _crossHairRendererPanel.setVisible(true);
      repaint();
    }
  }
  

  /**
   * @return a Collection of all the Renderers currently visible on the screen.
   * @author Bill Darbie
   */
  public Collection<Renderer> getVisibleRenderers()
  {
    Collection<Renderer> coll = new ArrayList<Renderer>();

    for (RendererPanel rendererPanel : _layerNumberToRendererPanelMap.values())
    {
      Component[] components = rendererPanel.getComponents();
      for (int i = 0; i < components.length; ++i)
      {
        Renderer renderer = (Renderer)components[i];
        if (renderer.isVisible())
        {
          coll.add(renderer);
        }
      }
    }

    Assert.expect(coll != null);
    return coll;
  }

  /**
   * @return a Collection of all the Renderers currently visible on the screen on a certain layer.
   * @author Bill Darbie
   */
  public Collection<Renderer> getVisibleRenderers(int layerNumber)
  {
    Collection<Renderer> coll = new ArrayList<Renderer>();

    RendererPanel rendererPanel = getRendererPanel(layerNumber);
    if (rendererPanel.isVisible())
    {
      Component[] components = rendererPanel.getComponents();
      for (int i = 0; i < components.length; ++i)
      {
        Renderer renderer = (Renderer)components[i];
        if (renderer.isVisible())
        {
          coll.add(renderer);
        }
      }
    }

    Assert.expect(coll != null);
    return coll;
  }

  /**
   * @author Andy Mechtenberg
   */
  public Collection<Renderer> getVisibleRenderers(Collection<Integer> layerNumbers)
  {
    Assert.expect(layerNumbers != null);

    Collection<Renderer> renderers = new ArrayList<Renderer>();
    for (Integer layerNumber : layerNumbers)
    {
      renderers.addAll(getVisibleRenderers(layerNumber));
    }

    return renderers;
  }


  /**
   * @return true if the layer passed in is visible.  If the layer has a visibility threshold
   * set, this method will only return true when that layer is really visible.  If the
   * layer has a creation threshold set, it will only return true if the layer is visible
   * and the scale is such that at least some of the Renderers are created.
   * @author Bill Darbie
   */
  public boolean isLayerVisible(int layerNumber)
  {
    RendererPanel rendererPanel = (RendererPanel)getRendererPanel(layerNumber);
    if (rendererPanel.isVisible())
    {
      Integer layerNumberInteger = new Integer(layerNumber);

      Double visibilityThreshold = (Double)_layerNumberToVisibilityThresholdMap.get(layerNumberInteger);
      if (visibilityThreshold != null)
      {
        // since threshold was not null, this layer is controlled by a visibility threshold
        // check to see if the scale is currently set to a point where the Renderers on the
        // layer would be visible
        double scaleX = Math.abs(_transformFromRendererCoords.getScaleX());
        double scaleY = Math.abs(_transformFromRendererCoords.getScaleY());

        // a rotation transform will have a scaleX and scaleY of 0, so set them to 1 instead
        if (scaleX == 0)
        {
          scaleX = 1;
        }
        if (scaleY == 0)
        {
          scaleY = 1;
        }

        double pixelsThreshold = visibilityThreshold.doubleValue() * scaleX;
        if (pixelsThreshold >= _PIXEL_THRESHOLD)
        {
          return true;
        }
        else
        {
          return false;
        }
      }

      Double creationThreshold = (Double)_layerNumberToCreationThresholdMap.get(layerNumberInteger);
      if (creationThreshold != null)
      {
        // since threshold was not null, this layer is controlled by a creation threshold
        // check to see if the scale is currently set to a point where the Renderers on the
        // layer would be created and visible
        double scaleX = Math.abs(_transformFromRendererCoords.getScaleX());
        double scaleY = Math.abs(_transformFromRendererCoords.getScaleY());

        // a rotation transform will have a scaleX and scaleY of 0, so set them to 1 instead
        if (scaleX == 0)
        {
          scaleX = 1;
        }
        if (scaleY == 0)
        {
          scaleY = 1;
        }

        double pixelsThreshold = creationThreshold.doubleValue() * scaleX;
        if (pixelsThreshold >= _PIXEL_THRESHOLD)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      return true;
    }
    else // panel is not visible
    {
      return false;
    }
  }

  /**
   * Set up which layers respond to mouse clicks.  By default, if this method is not
   * called all layers will respond to a mouse click.  If the mouse is clicked over a
   * Renderer, that Renderer will be returned to any Observers of the getSelectedRendererObservable()
   * method.
   * @param layers is the list of layers that will respond to a mouse click.  Send in an empty
   *        list to have all layers respond.
   * @author Bill Darbie
   */
  public void setLayersToRespondToLeftMouseClick(Collection<Integer> layers)
  {
    _layersToRespondToLeftMouseClickSet.clear();
    _layersToRespondToLeftMouseClickSet.addAll(layers);
  }

  /**
   * Clear out anything that might hold on to memory unecessarily if the client
   * is done with this class.
   * @author Andy Mechtenberg
   * @author Bill Darbie
   */
  public void dispose()
  {
    for (RendererPanel rendererPanel : _layerNumberToRendererPanelMap.values())
    {
      rendererPanel.removeAll();
    }

    _layerNumberToRendererPanelMap.clear();
    _layerNumberToCreationThresholdMap.clear();
    _layerNumberToRendererCreatorMap.clear();
    _layerNumberToVisibilityThresholdMap.clear();
    _rendererToFlashColorMap.clear();
    _rendererToOrigColorMap.clear();
    _rendererPanelToFlashColorMap.clear();
    _rendererPanelToOrigColorMap.clear();
    _selectedRendererToOrigColorMap.clear();
    _highlightedRendererToOrigColorMap.clear();
    _rendererToLayerNumberMap.clear();
    _layersToUseWhenFittingToScreen.clear();
    _layersToRespondToLeftMouseClickSet.clear();
  }

  /**
   * Reset the graphics engine to an initial starting state.
   * @author George A. David
   */
  public void reset()
  {
    dispose();
    _transformApplied = false;
    _isViewedFromTop = true;
    _multipleSelectOn = false;
    _currentlySelectedRendererSet.clear();
    _currentlyHighlightedRendererSet.clear();
    _crossHairRendererPanel = null;
    _crossHairLayerNumber = null;
    _crossHairRenderer = null;
    _rendererToPlaceCrossHairsOn = null;
    _mouseEventPanel = null;
    _mouseLayerNumber = null;
    _maxBounds = null;
    _layeredPane.removeAll();
    _transformFromRendererCoords.setToIdentity();
    _transformFromAxisTransform.setToIdentity();
    _transformFromLastMark.setToIdentity();
    _transformFromFitToScreen.setToIdentity();
    _setAxis = false;
    _positiveXisToTheRightOfTheOrigin = true;
    _positiveYisBelowTheOrigin = true;
  }


  /**
   * @author George A. David
   */
  public void setShouldGraphicsBeCentered(boolean shouldGraphicsBeCentered)
  {
    _shouldGraphicsBeCentered = shouldGraphicsBeCentered;
  }

  /**
   * @author Bill Darbie
   * @author Andy Mechtenberg
   */
  public void writeToFile(String fileName, int border) throws FileNotFoundException, IOException
  {
    Assert.expect(fileName != null);
    ImageIoUtil.savePngImage(createImage(border), fileName, "");
  }

  /**
   * @author George A. David
   */
  public BufferedImage createImage(int border)
  {
    Assert.expect(border >= 0);

    Rectangle2D rect = getCurrentBounds();
    Dimension jpgsize = new Dimension((int)Math.ceil(rect.getWidth()), (int)Math.ceil(rect.getHeight()));

    int xoffset = (int)rect.getMinX();
    int yoffset = (int)rect.getMinY();
    int xTranslate = -xoffset + border;
    int yTranslate = -yoffset + border;
    translate(xTranslate, yTranslate);

    int imageWidth = jpgsize.width + 2 * border;
    int imageLength = jpgsize.height + 2 * border;
    BufferedImage bufferedImage = new BufferedImage(imageWidth, imageLength, BufferedImage.TYPE_INT_BGR);
    Graphics graphics = bufferedImage.createGraphics();
    if(isDisplayable())
      paint(graphics);
    else
    {
      graphics.setColor(getBackground());
      graphics.fillRect(0, 0, imageWidth, imageLength);

      for (RendererPanel rendererPanel : _layerNumberToRendererPanelMap.values())
      {
        if (rendererPanel.areRenderersVisible())
        {
          for (Renderer renderer : rendererPanel.getRenderers())
          {
            Rectangle2D bounds = renderer.getBounds2D();
            graphics.translate((int)bounds.getMinX(), (int)bounds.getMinY());
            graphics.setColor(renderer.getForeground());
            renderer.paintComponent(graphics);
            graphics.translate((int) - bounds.getMinX(), (int) - bounds.getMinY());
          }
        }
      }
    }
    translate(-xTranslate, -yTranslate);
    return bufferedImage;
  }

  public boolean isZoomLevelBelowCreationThreshold(int layerNumber)
  {
    return _layerNumberToCreationThresholdBooleanMap.get(layerNumber);
  }

  /**
   * @author George A. David
   */
  public void setInitialTransformForLayer(int layerNumber, AffineTransform transform)
  {
    _layerNumberToInitialTransformMap.put(layerNumber, transform);
  }

  /**
   * @author George A. David
   */
  public void setZoomEnabled(boolean enabled)
  {
    _isZoomEnabled = enabled;
  }

  /**
   * @author Laura Cormos
   */
  public int getLayerNumberForRenderer(Renderer renderer)
  {
    Assert.expect(renderer != null);
    Assert.expect(_rendererToLayerNumberMap != null);

    if (_rendererToLayerNumberMap.containsKey(renderer))
      return _rendererToLayerNumberMap.get(renderer).intValue();
    else
      return -1;
  }

  /**
   * @param layerNumber
   * @author Chong, Wei Chin
   */
  public void decreaseBrightnessForRendererPanel(int layerNumber)
  {
    RendererPanel rendererPanel = _layerNumberToRendererPanelMap.get(layerNumber);
    rendererPanel.decreaseContrastForRenderers();
    rendererPanel.validateData();
  }

  /**
   * @param layerNumber
   * @author Chong, Wei Chin
   */
  public void increaseBrightnessForRendererPanel(int layerNumber)
  {
    RendererPanel rendererPanel = _layerNumberToRendererPanelMap.get(layerNumber);
    rendererPanel.increaseContrastForRenderers();
    rendererPanel.validateData();
  }

  /**
   * @param layerNumber
   * @author Chong, Wei Chin
   */
  public void decreaseContrastForRendererPanel(int layerNumber)
  {
    RendererPanel rendererPanel = _layerNumberToRendererPanelMap.get(layerNumber);
    rendererPanel.decreaseContrastForRenderers();
    rendererPanel.validateData();
  }

  /**
   * @param layerNumber
   * @author Chong, Wei Chin
   */
  public void increaseContrastForRendererPanel(int layerNumber)
  {
    RendererPanel rendererPanel = _layerNumberToRendererPanelMap.get(layerNumber);
    rendererPanel.increaseContrastForRenderers();
    rendererPanel.validateData();
  }

  /**
   * @param layerNumber
   * @author Chong, Wei Chin
   */
  public void decreaseNormalizeForRendererPanel(int layerNumber)
  {
    RendererPanel rendererPanel = _layerNumberToRendererPanelMap.get(layerNumber);
    rendererPanel.decreaseNormalizeForRenderers();
    rendererPanel.validateData();
  }

  /**
   * @param layerNumber
   * @author Chong, Wei Chin
   */
  public void increaseNormalizeForRendererPanel(int layerNumber)
  {
    RendererPanel rendererPanel = _layerNumberToRendererPanelMap.get(layerNumber);
    rendererPanel.increaseNormalizeForRenderers();
    rendererPanel.validateData();
  }

  /**
   * @param layerNumber
   * @author Chong, Wei Chin
   */
  public void equalizeGrayScaleForRendererPanel(int layerNumber)
  {
    RendererPanel rendererPanel = _layerNumberToRendererPanelMap.get(layerNumber);
    rendererPanel.equalizeGrayScaleForRenderers();
    rendererPanel.validateData();
  }

  /**
   * @param layerNumber
   * @author Chong, Wei Chin
   */
  public void revertToOriginalBufferedImageForRendererPanel(int layerNumber)
  {
//    RendererPanel rendererPanel = _layerNumberToRendererPanelMap.get(layerNumber);
    RendererPanel rendererPanel = getRendererPanel(layerNumber);

	//Lim, Lay Ngor
	//5.6 - XCR2228 - Workaround for Assert happen when re-generating virtual live image with different inputs.
	//Please remove code below after proper fixed at next release.
    if(rendererPanel == null)
      return;//do nothing

    rendererPanel.revertToOriginalBufferedImageForRenderers();
    rendererPanel.validateData();
  }

  /**
   * To apply post processing on the image with the given layerNumber.
   * @param layerNumber
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */
  public void imagePostProcessingForRendererPanel(int layerNumber, List<ImageEnhancerBase> enhancerList)
  {
    RendererPanel rendererPanel = _layerNumberToRendererPanelMap.get(layerNumber);
    rendererPanel.imagePostProcessingForRenderers(enhancerList);
    rendererPanel.validateData();
  }
  
  /**
   * To apply the brightness and contrast setting of  previous image to current image using image layer number.
   * @param currentLayerNumber
   * @param enhancerList
   * @param previousLayerNumber
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */
  public void imagePostProcessingWithSettingForRendererPanel(
    int currentLayerNumber, 
    List<ImageEnhancerBase> enhancerList,
    int previousLayerNumber)
  {
    RendererPanel previousRendererPanel = _layerNumberToRendererPanelMap.get(previousLayerNumber);    
    RendererPanel currentRendererPanel = _layerNumberToRendererPanelMap.get(currentLayerNumber);
    currentRendererPanel.imagePostProcessingWithSettingForRenderers(enhancerList,
      previousRendererPanel.getBrightnessAndContrastSettingForRenderers());
    currentRendererPanel.validateData();
  }
   
  /**
   * To apply the brightness and contrast setting of  previous image to current image directly using brightness and contrast setting.
   * @param currentLayerNumber
   * @param enhancerList
   * @param setting
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */
  public void imagePostProcessingWithSettingForRendererPanel(
    int currentLayerNumber, 
    List<ImageEnhancerBase> enhancerList,
    BrightnessAndContrastSetting setting)
  {
    RendererPanel currentRendererPanel = _layerNumberToRendererPanelMap.get(currentLayerNumber);
    currentRendererPanel.imagePostProcessingWithSettingForRenderers(enhancerList, setting);
    currentRendererPanel.validateData();
  }

  /**
   * @param layerNumber
   * @author Chong, Wei Chin
   */
  public void saveBufferedImageForRendererPanel(int layerNumber, String filename)
  {
    Assert.expect(filename != null);
    
    RendererPanel rendererPanel = _layerNumberToRendererPanelMap.get(layerNumber);
    rendererPanel.saveBufferedImageForRenderers(filename);
    rendererPanel.validateData();
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void setRectangleInPixels(int xInPixels, int yInPixels, int widthInPixels, int heightInPixels)
  {
    if (isWindowResized() == false)
    {  
        _xInPixels = xInPixels;
        _yInPixels = yInPixels;
        _widthInPixels = widthInPixels;
        _heightInPixels = heightInPixels;
        _rectangleInPixels = new Rectangle(_xInPixels, _yInPixels, _widthInPixels, _heightInPixels);   
      
    }
    else
    {
        _resizedXInPixels = xInPixels;
        _resizedYInPixels = yInPixels;
        _resizedWidthInPixels = widthInPixels;
        _resizedHeightInPixels = heightInPixels;
        _resizedRectangleInPixels = new Rectangle(_resizedXInPixels, _resizedYInPixels, _resizedWidthInPixels, _resizedHeightInPixels);   
    }
  
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public boolean isWindowResized()
  {
//    if (MathUtil.fuzzyEquals(_fitGraphicsToScreenWhenWindowIsResizedNewScale, 1.000))
//    { 
//        return false;
//    }
//    else
//    {
//        return true;
//    }
    return _isWindowResized;
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public double getFitGraphicsToScreenWhenWindowIsResizedNewScale()
  {
      return _fitGraphicsToScreenWhenWindowIsResizedNewScale;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void setWidthHeightInNano(int widthInNano, int heightInNano)
  {
    _widthInNano = widthInNano;
    _heightInNano = heightInNano;
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public void setCrossHairCoordinateInNano(int x, int y)
  {
    _crossHairX = x;
    _crossHairY = y;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public Point2D getCrossHairCoordinateInPixels()
  {
    Point2D point = new Point();
    if (_crossHairX != 0 || _crossHairY != 0)
    {
        point = convertFromRendererToPixelCoordinates(_crossHairX, _crossHairY);
    }
    else
    {
        point.setLocation(0, 0);
    }
     
    return point;
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public int getPspRegionCenterCoordinateX()
  {
  //  int centerCoordinateX = (_widthInPixels / 2) + _xInPixels;  
    int centerCoordinateX = _xInPixels; 
    return centerCoordinateX;
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public int getPspRegionCenterCoordinateY()
  {
    // int centerCoordinateY = (_heightInPixels / 2) + _yInPixels;  
    int centerCoordinateY =  _yInPixels;  
    return centerCoordinateY;
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public int getPspRegionWidthInNano()
  {
    return _widthInNano;
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public int getPspRegionHeightInNano()
  {
    return _heightInNano;
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public boolean isDragRegionMode()
  {
    return _isDragRegionMode;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public boolean isDragSelectedRegionMode()
  {
    return _isDragSelectedRegionMode;
  }
  
  /**
   * Draws the current set of cross hairs if they are in use.
   * @author Jack Hwee
   */
  public void drawRegionCrossHairs()
  { 
       // now that we know all layers and Renderers are set up add the _mouseEventPanel
      Rectangle bounds = getBounds();
      int width = bounds.width;
      int height = bounds.height;
      addCrossHairPanel(width, height);
      addMouseEventPanel(width, height);
      
      double xCoor = (double)getCrossHairCoordinateInPixels().getX();
      double yCoor = (double)getCrossHairCoordinateInPixels().getY();
   
      Rectangle2D rect = new Rectangle(Math.round((float)(xCoor - 23.5)), Math.round((float)(yCoor - 15)), 47, 30); 
    
      _pspCrossHairRendererPanel = new RendererPanel(Color.RED); // this color is not really used.   See setForeground below for real color
      CrossHairRenderer opticalRegionCrossHairRenderer = new CrossHairRenderer();
      _pspCrossHairRendererPanel.add(opticalRegionCrossHairRenderer);
      _pspCrossHairRendererPanel.setForeground(LayerColorEnum.CROSSHAIR_COLOR.getColor());
      _pspCrossHairRendererPanel.setVisible(false);
     
      opticalRegionCrossHairRenderer.setCrossHair((double)xCoor, (double)yCoor, rect.getBounds2D());
      _pspCrossHairRendererPanel.setVisible(true);
      repaint(); 
      selectWithCrossHairs(opticalRegionCrossHairRenderer);  
  }
  
  /**
   * clear the current set of cross hairs if they are in use.
   * @author Jack Hwee
   */
  public void clearRegionCrossHairs()
  {    
       this.setVisible(_crossHairLayerNumber, false);
       if (_crossHairLayerNumber != null)
       {
          removeLayerForCrossHairAndMouse(_crossHairLayerNumber.intValue());
       }
  }
  
 
  /**
   * 
   * @author Jack Hwee
   */
  public void setSelectedComponentRegion(Rectangle2D rectangle)
  {
      _rectangle = rectangle;
    
      int maxX = (int)_rectangle.getBounds2D().getMaxX();
      int maxY = (int)_rectangle.getBounds2D().getMaxY();
      int minX = (int)_rectangle.getBounds2D().getMinX();
      int minY = (int)_rectangle.getBounds2D().getMinY();
    
      if (_maxRegionXCoordinate != 0 || _maxRegionYCoordinate != 0 || _minRegionXCoordinate != 0 || _minRegionYCoordinate != 0)
      {
        if (_maxRegionXCoordinate < maxX)
           _maxRegionXCoordinate = maxX;

        if (_maxRegionYCoordinate < maxY)
          _maxRegionYCoordinate = maxY;

        if (_minRegionXCoordinate > minX)
          _minRegionXCoordinate = minX;

        if (_minRegionYCoordinate > minY)
          _minRegionYCoordinate = minY;   
      }
      else
      {
        _maxRegionXCoordinate = maxX;  
        _maxRegionYCoordinate = maxY; 
        _minRegionXCoordinate = minX;
        _minRegionYCoordinate = minY;   
      }
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public Rectangle getSelectedComponentRegion()
  {
      return _rectangleInPixels;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public Rectangle getResizedSelectedComponentRegion()
  {    
      return _resizedRectangleInPixels;
  }
  
    /**
   * 
   * @author Jack Hwee
   */
  public void setUnselectedRectangleInPixels(int xInPixels, int yInPixels, int widthInPixels, int heightInPixels)
  {
    _unselectedXInPixels = xInPixels;
    _unselectedYInPixels = yInPixels;
    _unselectedWidthInPixels = widthInPixels;
    _unselectedHeightInPixels = heightInPixels;

    _unselectedRectangleInPixels = new Rectangle(_unselectedXInPixels, _unselectedYInPixels, _unselectedWidthInPixels, _unselectedHeightInPixels);
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public Rectangle getUnselectedComponentRegion()
  {
      return _unselectedRectangleInPixels;
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public Rectangle calculateSelectedComponentRegionInNanoMeter(Rectangle rectInPixel)
  {
      int minX = (int)rectInPixel.getBounds2D().getMinX();
      int minY = (int)rectInPixel.getBounds2D().getMaxY();
      int widthInPixel= (int)rectInPixel.getBounds2D().getWidth();
      int heightInPixel = (int)rectInPixel.getBounds2D().getHeight();
      
      Point dividedPoint = convertFromMousePixelToRendererPixel((int)minX, (int)minY);

      Point2D graphicCoordInNanoMeters = convertFromPixelToRendererCoordinates((int) dividedPoint.getX(), (int) dividedPoint.getY());

      Point2D graphicCoordInNanoMetersOri = convertFromPixelToRendererCoordinates((int) widthInPixel, (int) heightInPixel);

      Point2D graphicCoordInNanoMetersMouse = convertFromPixelToRendererCoordinates((int) 0, (int) 0);

      int widthInNano = (int) (graphicCoordInNanoMetersOri.getX() - graphicCoordInNanoMetersMouse.getX() );
      int heightInNano = (int) (graphicCoordInNanoMetersMouse.getY() - graphicCoordInNanoMetersOri.getY());
      
      Rectangle rectInNano = new Rectangle ();
      rectInNano.setRect(graphicCoordInNanoMeters.getX(), graphicCoordInNanoMeters.getY(), widthInNano, heightInNano);
      
      return rectInNano;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void resetSelectedRegionCoordinate()
  {
      _maxRegionXCoordinate = 0;  
      _maxRegionYCoordinate = 0; 
      _minRegionXCoordinate = 0;
      _minRegionYCoordinate = 0;   
      _opticalRegionPointList = new ArrayList<Point>();
     
    //  _igronedOpticalRegionList = new HashMap<Rectangle, BooleanRef>();
      resetUpdateCount();
  }
  
   /**
   * @author Jack Hwee
   * 
   */
  public void updateCount()
  {
      _updateCount++;
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  private void resetUpdateCount()
  {
      _updateCount = 0;
  }
  
  
   /**
   * 
   * @author Jack Hwee
   */
  public int getUpdateCount()
  {
      return _updateCount;
  }
  
   /**
   * @author Jack Hwee
   * Calculate the region width of the selected component
   */
  public int getCalculatedSelectedComponentRegionWidth()
  {
     Point2D point2 = convertFromPixelToRendererCoordinates((int) 0, (int) 0);  
     int  a = (int) (_maxRegionXCoordinate + point2.getX());
     int b = (int) (_minRegionXCoordinate + point2.getY());
    
     Point2D point = convertFromRendererToPixelCoordinates(a, b);
     
     _regionWidth = (int) (point.getX() + point.getY());

     return _regionWidth;
  }
  
  /**
   * @author Jack Hwee
   * Calculate the region height of the selected component
   */
  public int getCalculatedSelectedComponentRegionHeight()
  {
     Point2D point2 = convertFromPixelToRendererCoordinates((int) 0, (int) 0);
     int  a = (int) (point2.getX() - _maxRegionYCoordinate);
     int b = (int) ( point2.getY() - _minRegionYCoordinate);
     
     Point2D point = convertFromRendererToPixelCoordinates(a, b);
     
     _regionHeight = (int) (-point.getX() - point.getY());
    
     return _regionHeight;
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public Point2D getComponentRegionUpperLeftCoordinate()
  {
    _upperLeftCoordinate = convertFromRendererToPixelCoordinates(_minRegionXCoordinate, _maxRegionYCoordinate);
    return _upperLeftCoordinate;
  }

   /**
   * 
   * @author Jack Hwee
   */
  public boolean isSelectedRegionInComboboxAvailable()
  {
      return _isSelectedRegionInComboboxAvailable;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public boolean setSelectedRegionInComboboxAvailable(boolean isSelectedRegionInComboboxAvailable)
  {
      _isSelectedRegionInComboboxAvailable = isSelectedRegionInComboboxAvailable;
      return _isSelectedRegionInComboboxAvailable;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void setShowRegionCheckBoxIsChecked(boolean isCheckBoxChecked)
  {
      _isCheckBoxChecked = isCheckBoxChecked;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public boolean isShowRegionCheckBoxIsChecked()
  {
      return _isCheckBoxChecked;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void setDragOpticalRegionButtonIsToggle(boolean isDragOpticalRegionButtonToggle)
  {
      _isDragOpticalRegionButtonToggle = isDragOpticalRegionButtonToggle;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public boolean isDragOpticalRegionButtonIsToggle()
  {
      return _isDragOpticalRegionButtonToggle;
  }
  
    /**
   * 
   * @author Jack Hwee
   */
  public void setDragRegionModeAvailable(boolean dragRegionMode)
  {
      _isDragRegionModeAvailable = dragRegionMode;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public boolean isDragRegionModeAvailable()
  {
      return _isDragRegionModeAvailable;
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public void setMouseRightClickMode(boolean mouseRightClickMode)
  {
      _isMouseRightClickMode = mouseRightClickMode;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public boolean isMouseRightClickMode()
  {
      return _isMouseRightClickMode;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void setControlKeyPressMode(boolean controlKeyPressMode)
  {
      _isControlKeyPressMode = controlKeyPressMode;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public boolean isControlKeyPressMode()
  {
      return _isControlKeyPressMode;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void setMouseRightClickCoordinate(Point mouseRightClickCoordinate)
  {
      _mouseRightClickCoordinate = mouseRightClickCoordinate;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public Point getMouseRightClickCoordinate()
  {
      return _mouseRightClickCoordinate;
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public AffineTransform getCurrentAffineTransformForOpticalRegion()
  {
      return _transformFromRendererCoords;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void setCurrentWindowSize()
  {
      _rect = this.getCurrentBounds().getBounds();
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void setOpticalRegionFirstDrag(boolean isFirstDrag)
  {
      _isFirstDrag = isFirstDrag;
  }
  
    /**
   * 
   * @author Jack Hwee
   */
  public boolean isOpticalRegionFirstDrag()
  {
      return _isFirstDrag;
  }
  
    /**
   * @author Jack Hwee
   */
  public void clearCurrentlySelectedComponentRendererSet()
  {
      _currentlySelectedComponentRendererSet.clear();
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void setOpticalCameraRectangleInPixel(int xInPixels, int yInPixels, int widthInPixels, int heightInPixels)
  {
       _opticalCameraRectangleInPixel = new Rectangle(xInPixels, yInPixels, widthInPixels, heightInPixels); 
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public Rectangle getOpticalCameraRectangleInPixel()
  {    
      return _opticalCameraRectangleInPixel;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void clearAllSurfaceMapAlignmentRegion()
  {
      _alignmentOpticalRegionList.clear();
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void addSurfaceMapAlignmentRegion(Rectangle2D rectangle)
  {
      _alignmentOpticalRegionList.add(rectangle);
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public java.util.List<Rectangle2D> getSurfaceMapAlignmentRegion()
  {
      return _alignmentOpticalRegionList;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public void clearAllSurfaceMapAlignmentRegionInPixel()
  {
      _alignmentOpticalRegionInPixelList.clear();
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public Rectangle convertSurfaceMapAlignmentRegionToPixel(Rectangle rectangle)
  {
     double maxRegionXCoordinate = rectangle.getMaxX();
     double minRegionXCoordinate = rectangle.getMinX();
     double maxRegionYCoordinate = rectangle.getMaxY();
     double minRegionYCoordinate = rectangle.getMinY();
   
     Point2D minRegionXYCoordinateInPixel = convertFromRendererToPixelCoordinates(minRegionXCoordinate, maxRegionYCoordinate);
             
     Point2D point2 = convertFromPixelToRendererCoordinates((int) 0, (int) 0);  
     int  a = (int) (maxRegionXCoordinate + point2.getX());
     int b = (int) (minRegionXCoordinate + point2.getY());
    
     Point2D point = convertFromRendererToPixelCoordinates(a, b);
     
     int regionWidth = (int) (point.getX() + point.getY());
     
     
     Point2D point3 = convertFromPixelToRendererCoordinates((int) 0, (int) 0);
     int  c = (int) (point3.getX() - maxRegionYCoordinate);
     int d = (int) ( point3.getY() - minRegionYCoordinate);
     
     Point2D point4 = convertFromRendererToPixelCoordinates(c, d);
     
     int regionHeight = (int) (-point4.getX() - point4.getY());
     
     Rectangle rectangleInPixel = new Rectangle ((int)minRegionXYCoordinateInPixel.getX(), (int)minRegionXYCoordinateInPixel.getY(), regionWidth, regionHeight);
     
     return rectangleInPixel;
     // _alignmentOpticalRegionInPixelList.add(rectangle);
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public java.util.List<Rectangle> getSurfaceMapAlignmentRegionInPixel()
  {
      return _alignmentOpticalRegionInPixelList;
  }
  
  /**
   * Select an area on drawed cad.
   * Send Renderer through the _selectedShapeObservable
   * @author sham
   */
  public void selectAreaInRectangle(Shape rectangle)
  {
	Assert.expect(_selectedAreaObservable != null);

    _selectedAreaObservable.setSelectedArea(rectangle);
  }

  /**
   * Puts the GraphicsEngine into themode where the user can draw a Rectangle to indicate an
   * area as selected area
   * @author sham
   */
  public void setSelectRegionMode(boolean selectRegionMode)
  {
    Assert.expect(_mouseEventPanel != null);

    if (selectRegionMode)
    {
      setCursor(new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR));
      _mouseEventPanel.setVisible(true);
      _mouseEventPanel.enableSelectRegionMode();
    }
    else
    {
      _mouseEventPanel.setVisible(false);
      setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void enabledOvalDragShape()
  {
     if(_mouseEventPanel.isEnabledSelectRegionMode())
     {
       _mouseEventPanel.enabledOvalDragShape();
     }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void enabledRectangleDragShape()
  {
     if(_mouseEventPanel.isEnabledSelectRegionMode())
     {
       _mouseEventPanel.enabledRectangleDragShape();
     }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setSelectRegionMode(boolean selectRegionMode, double regionWidth, double regionHeight)
  {
    Assert.expect(_mouseEventPanel != null);

    if (selectRegionMode)
    {
      setCursor(new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR));
      _mouseEventPanel.setVisible(true);
      _mouseEventPanel.enableSelectRegionMode();
      _mouseEventPanel.setMaxImageRegionDimension(regionWidth, regionHeight);
    }
    else
    {
      _mouseEventPanel.setVisible(false);
      setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    }
  }

  /*
   * @author sham khang shian
   */
  public Map<Integer, RendererPanel> getLayerNumberToRendererPanelMap()
  {
    return _layerNumberToRendererPanelMap;
  }

  /**
   * @return the Observable the is responsible for sending updates when an area selected on screen
   * @author sham
   */
  public SelectedAreaObservable getSelectedAreaObservable()
  {
    return _selectedAreaObservable;
  }
  
    /**
   * 
   * @author Jack Hwee
   */
  public boolean isZoom()
  {
      return _isZoom;
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public void setIsWindowResized(boolean bool)
  {
      _isWindowResized = bool;
  }
  
   /**
   * 
   * @author Jack Hwee
   */
  public double getCanvasWidthToHeightRatio()
  {
      return _canvasWidthToHeightRatio;
  }
  
   public Point2D convertFromRendererToPixelCoordinates(double x, double y, AffineTransform affineTransform)
  {
    Assert.expect(affineTransform != null);
    
    Point2D point = new Point2D.Double(x, y);
    affineTransform.transform(point, point);
   
    return point;
  }
  
  /**
   * reset the minimum zoom factor for component image view
   * @author bee-hoon.goh
   */
  public void setMinimumZoomFactorForComponentImageView()
  {
    _minZoomFactor = 0.01;
  }
  
  /**
   * reset the minimum zoom factor for normal inspection image view
   * @author bee-hoon.goh
   */
  public void resetMinimumZoomFactorForNormalImageView()
  {
    _minZoomFactor = 1.0;
  }
  
    /**
   * 
   * @author Jack Hwee
   */
  public Rectangle convertSurfaceMapRegionToPixel(Rectangle rectangle, AffineTransform transformFromRendererCoords)
  {
    Assert.expect(rectangle != null);
    Assert.expect(transformFromRendererCoords != null);

    double maxRegionXCoordinate = rectangle.getMaxX();
    double minRegionXCoordinate = rectangle.getMinX();
    double maxRegionYCoordinate = rectangle.getMaxY();
    double minRegionYCoordinate = rectangle.getMinY();

    Point2D point = new Point2D.Double(minRegionXCoordinate, maxRegionYCoordinate);
    Point2D minRegionXYCoordinateInPixel = transformFromRendererCoords.transform(point, point);

    Point2D point2 = new Point2D.Double(0, 0);
    AffineTransform inverse = null;
    try
    {
      inverse = transformFromRendererCoords.createInverse();
    }
    catch (NoninvertibleTransformException ex)
    {
      ex.printStackTrace();
      Assert.expect(false);
    }   
    inverse.transform(point2, point2);
  
    int a = (int) (maxRegionXCoordinate + point2.getX());
    int b = (int) (minRegionXCoordinate + point2.getY());

    Point2D point3 = new Point2D.Double(a, b);
    transformFromRendererCoords.transform(point3, point3);
  
    int regionWidth = (int) (point3.getX() + point3.getY());

    Point2D point4 = new Point2D.Double((int) 0, (int) 0);
    AffineTransform inverse1 = null;
    try
    {
      inverse1 = transformFromRendererCoords.createInverse();
    }
    catch (NoninvertibleTransformException ex)
    {
      ex.printStackTrace();
      Assert.expect(false);
    }

    inverse1.transform(point4, point4);

    int c = (int) (point4.getX() - maxRegionYCoordinate);
    int d = (int) (point4.getY() - minRegionYCoordinate);

    Point2D point5 = new Point2D.Double(c, d);
    transformFromRendererCoords.transform(point5, point5);
  
    int regionHeight = (int) (-point5.getX() - point5.getY());

    Rectangle rectangleInPixel = new Rectangle((int) minRegionXYCoordinateInPixel.getX(), (int) minRegionXYCoordinateInPixel.getY(), regionWidth, regionHeight);

    return rectangleInPixel;
  }
  
  /**
   * author Siew Yeng
   */
  public double getMinimumZoomFactor()
  {
    return _minZoomFactor;
  }
}
