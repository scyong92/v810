package com.axi.guiUtil;

import com.axi.util.*;

/**
 * This class defines a concrete implementation of the Range interface
 * for Integer values.
 *
 * @author  Steve Anonson
 */
public class IntegerRange implements Range
{
  private int _min;
  private int _max;

  /**
   * @param   minimum - the minimum value in the range of Integers.
   * @param   maximum - the maximum value in the range of Integers.
   * @throws  IllegalArgumentException if the maximum is less than the minimum.
   *
   * @author Steve Anonson
   */
  public IntegerRange(int minimum, int maximum)
  {
    Assert.expect(maximum >= minimum, "Maximum range is less than minimum range.");

    _min = minimum;
    _max = maximum;
  }

  /**
   * Access method defined in the Range interface.
   * @author Steve Anonson
   */
  public Number getMinimumValue()
  {
    return new Integer(_min);
   }

  /**
   * Access method defined in the Range interface.
   * @author Steve Anonson
   */
  public Number getMaximumValue()
  {
    return new Integer(_max);
  }

  /**
   * Access methods to return the int value.
   * @author Steve Anonson
   */
  public int getMinimum()
  {
    return _min;
  }

  /**
   * Access methods to return the int value.
   * @author Steve Anonson
   */
  public int getMaximum()
  {
    return _max;
  }

  /**
   * Range check method defined in the Range interface.
   *
   * @param   value - Number whose value is checked against the range.
   * @return  true if the value is within the defined range, false otherwise.
   * @author Steve Anonson
   */
  public boolean inRange( Number value )
  {
    // apm - changed to compare on the long value, because if a large value comes in, it's intValue is
    // not the same as the user input and this check may pass when it really shouldn't
    long valueLong = value.longValue();
    return (valueLong <= _max && valueLong >= _min);
  }
}
