package com.axi.guiUtil;

/**
 * To keep the Image BrightnessAndContrastSetting to an object.
 * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
 */
public class BrightnessAndContrastSetting
{
    public float _scaleFactor = 1.0f;
    public float _offset = 10.f;
    public int _shiftValue = 0;
    
    public boolean _normalizeImage = false;
    public boolean _scaleImage = false;    
    
    /**
     * BrightnessAndContrastSetting constructor with default value.
     * @author Lim, Lay Ngor
     */
    public BrightnessAndContrastSetting()
    {
      _scaleFactor = 1.0f;
      _offset = 10.f;
      _shiftValue = 0;
      _normalizeImage = false;
      _scaleImage = false;
    }
        
    public BrightnessAndContrastSetting(
      float scaleFactor,
      float offset,
      int shiftValue,
      boolean normalizeImage,
      boolean scaleImage)
    {
      _scaleFactor = scaleFactor;
      _offset = offset;
      _shiftValue = shiftValue;
      _normalizeImage = normalizeImage;
      _scaleImage = scaleImage;
    }

    public void resetToDefaultValue()
    {
      _scaleFactor = 1.0f;
      _offset = 10.f;
      _shiftValue = 0;
      _normalizeImage = false;
      _scaleImage = false;
    }
}
