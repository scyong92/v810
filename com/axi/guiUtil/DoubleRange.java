package com.axi.guiUtil;

import com.axi.util.*;

/**
 * This class defines a concrete implementation of the Range interface
 * for Integer values.
 *
 * @author  Steve Anonson
 */
public class DoubleRange implements Range
{
  private double _min;
  private double _max;

  /**
   * @param   minimum - the minimum value in the range of Integers.
   * @param   maximum - the maximum value in the range of Integers.
   * @throws  IllegalArgumentException if the maximum is less than the minimum.
   *
   * @author Steve Anonson
   */
  public DoubleRange(double minimum, double maximum)
  {
    Assert.expect(maximum >= minimum, "Maximum range is less than minimum range.");

    _min = minimum;
    _max = maximum;
  }

  /**
   * Access method defined in the Range interface.
   * @author Steve Anonson
   */
  public Number getMinimumValue()
  {
    return new Double(_min);
   }

  /**
   * Access method defined in the Range interface.
   * @author Steve Anonson
   */
  public Number getMaximumValue()
  {
    return new Double(_max);
  }

  /**
   * Access methods to return the int value.
   * @author Steve Anonson
   */
  public double getMinimum()
  {
    return _min;
  }

  /**
   * Access methods to return the int value.
   * @author Steve Anonson
   */
  public double getMaximum()
  {
    return _max;
  }

  /**
   * Range check method defined in the Range interface.
   *
   * @param   value - Number whose value is checked against the range.
   * @return  true if the value is within the defined range, false otherwise.
   * @author Steve Anonson
   */
  public boolean inRange( Number value )
  {
    String valStr = value.toString();

    // this looks strange, but I need to distinguish between a "0.0" and "-0.0"
    // I don't want to allow a leading negative if the min is 0.0
    boolean isNegative = false;
    if (valStr.charAt(0) == '-')
      isNegative = true;
    // negative min range should allow a negative value
    if ((_min == 0.0) && isNegative)
      return false;
    double valueOf = value.doubleValue();
    return (valueOf <= _max && valueOf >= _min);
  }
}
