package com.axi.guiUtil;

import java.text.*;

import javax.swing.text.*;

/**
 * This class when used with a text field will limit the text field
 * contents to positive or negative numbers.
 *
 * The original version of this class was found in the book:
 * Core Swing: advanced programming by Kim Topley
 *
 * @author Steve Anonson
 */
public class NumericPlainDocument extends PlainDocument
{
  protected DecimalFormat _format;
  protected char _decimalSeparator;
  protected char _groupingSeparator;
  protected String _positivePrefix;
  protected String _negativePrefix;
  protected int _positivePrefixLen;
  protected int _negativePrefixLen;
  protected String _positiveSuffix;
  protected String _negativeSuffix;
  protected int _positiveSuffixLen;
  protected int _negativeSuffixLen;
  protected ParsePosition _parsePos = new ParsePosition(0);
  protected static DecimalFormat _defaultFormat = new DecimalFormat();
  protected InsertErrorListener _errorListener;

  /**
   * @author Steve Anonson
   */
  public NumericPlainDocument()
  {
    setFormat(null);
  }

  /**
   * @param format the DecimalFormat to use to verify contents.
   * @author Steve Anonson
   */
  public NumericPlainDocument(DecimalFormat format)
  {
    setFormat(format);
  }

  /**
   * @param content the initial value for the document.
   * @param format the DecimalFormat to use to verify contents.
   * @author Steve Anonson
   */
  public NumericPlainDocument(AbstractDocument.Content content, DecimalFormat format)
  {
    super(content);
    setFormat(format);

    try
    {
      format.parseObject(content.getString(0, content.length()), _parsePos);
    }
    catch (Exception e)
    {
      IllegalArgumentException ex = new IllegalArgumentException("Initial value not a valid number");
      ex.initCause(e);
      throw ex;
    }

    if (_parsePos.getIndex() != content.length() - 1)
    {
      throw new IllegalArgumentException("Initial value not a valid number");
    }
  }

  /**
   * Set the format used to verify the contents and display the value.
   *
   * @param fmt the DecimalFormat to use to verify contents.
   */
  public void setFormat(DecimalFormat fmt)
  {
    _format = ((fmt != null) ? fmt : (DecimalFormat)_defaultFormat.clone());
    _decimalSeparator = _format.getDecimalFormatSymbols().getDecimalSeparator();
    _groupingSeparator = _format.getDecimalFormatSymbols().getGroupingSeparator();
    _positivePrefix = _format.getPositivePrefix();
    _positivePrefixLen = _positivePrefix.length();
    _negativePrefix = _format.getNegativePrefix();
    _negativePrefixLen = _negativePrefix.length();
    _positiveSuffix = _format.getPositiveSuffix();
    _positiveSuffixLen = _positiveSuffix.length();
    _negativeSuffix = _format.getNegativeSuffix();
    _negativeSuffixLen = _negativeSuffix.length();
  }

  /**
   * @return the current format.
   */
  public DecimalFormat getFormat()
  {
    return _format;
  }

  /**
   * Parse the contents and return the Number equivalent.
   *
   * @throws ParseException if the contents doesn't parse to a valid Number.
   */
  public Number getNumberValue() throws ParseException
  {
    try
    {
      String content = getText(0, getLength());
      _parsePos.setIndex(0);
      Number result = _format.parse(content, _parsePos);
      // don't want to return a null reference.  Default to Zero if nothing was parsed
      if (result == null)
        return new Long(0);
      if (_parsePos.getIndex() != getLength())
      {
        throw new ParseException("Not a valid number: " + content, 0);
      }
      return result;
    }
    catch (BadLocationException e)
    {
      ParseException ex = new ParseException("Not a valid number", 0);
      ex.initCause(e);
      throw ex;
    }
  }

  /**
   * @return the Long equivalent of the contents.
   *
   * @throws ParseException if not a valid long number.
   */
  public Long getLongValue() throws ParseException
  {
    Number result = getNumberValue();
    if ((result instanceof Long) == false)
    {
      throw new ParseException("Not a valid long", 0);
    }
    return (Long)result;
  }

  /**
   * @return the Double equivalent of the contents.
   *
   * @throws ParseException if not a valid double number.
   */
  public Double getDoubleValue() throws ParseException
  {
    Number result = getNumberValue();
    if ((result instanceof Long) == false && (result instanceof Double) == false)
    {
      throw new ParseException("Not a valid double", 0);
    }
    if (result instanceof Long)
    {
      result = new Double(result.doubleValue());
    }
    return (Double)result;
  }

  /**
   * This method overrides the Document method and validates the input.
   */
  public void insertString(int offset, String str, AttributeSet a) throws BadLocationException
  {
    if (str == null || str.length() == 0)
      return;
    if (validString(offset, str, a) == null)
      return;

    // Finally, add to the model
    super.insertString(offset, str, a);
  }

  /**
   * This method overrides the Document method and validates the input.
   */
  protected Number validString(int offset, String str, AttributeSet a) throws BadLocationException
  {
    if (str == null || str.length() == 0)
      return null;

    Number value = null;
    Content content = getContent();
    int length = content.length();
    int originalLength = length;

    _parsePos.setIndex(0);

    // Create the result of inserting the new data,
    // but ignore the trailing newline
    String targetString = content.getString(0, offset) + str +
      content.getString(offset, length - offset - 1);

    // Parse the input string and check for errors
    do
    {
      boolean gotPositive = targetString.startsWith(_positivePrefix);
      boolean gotNegative = targetString.startsWith(_negativePrefix);

      length = targetString.length();

      // If we have a valid prefix, the parse fails if the
      // suffix is not present and the error is reported
      // at index 0. So, we need to add the appropriate
      // suffix if it is not present at this point.
      if (gotPositive == true || gotNegative == true)
      {
        String suffix;
        int suffixLength;
        int prefixLength;

        if (gotPositive == true && gotNegative == true)
        {
          // This happens if one is the leading part of
          // the other - e.g. if one is "(" and the other "(("
          if (_positivePrefixLen > _negativePrefixLen)
            gotNegative = false;
          else
            gotPositive = false;
        }

        if (gotPositive == true)
        {
          suffix = _positiveSuffix;
          suffixLength = _positiveSuffixLen;
          prefixLength = _positivePrefixLen;
        }
        else
        {
          // Must have the negative prefix
          suffix = _negativeSuffix;
          suffixLength = _negativeSuffixLen;
          prefixLength = _negativePrefixLen;
        }

        // If the string consists of the prefix alone,
        // do nothing, or the result won't parse.
        if (length == prefixLength)
        {
          if (gotNegative)
            value = new Double("-0");
          break;
        }
        // We can't just add the suffix, because part of it
        // may already be there. For example, suppose the
        // negative prefix is "(" and the negative suffix is
        // "$)". If the user has typed "(345$", then it is not
        // correct to add "$)". Instead, only the missing part
        // should be added, in this case ")".
        if (targetString.endsWith(suffix) == false)
        {
          int i;
          for (i = suffixLength - 1; i > 0 ; i--)
          {
            if (targetString.regionMatches(length - i, suffix, 0, i))
            {
              targetString += suffix.substring(i);
              break;
            }
          }

          if (i == 0)
            targetString += suffix;  // None of the suffix was present

          length = targetString.length();
        }
      }

      value = _format.parse(targetString, _parsePos);

      int endIndex = _parsePos.getIndex();
      if (endIndex == length)
        break;		// Number is acceptable

      // Parse ended early
      // Since incomplete numbers don't always parse, try
      // to work out what went wrong.
      // First check for an incomplete positive prefix
      if (_positivePrefixLen > 0 && endIndex < _positivePrefixLen && length <= _positivePrefixLen &&
          targetString.regionMatches(0, _positivePrefix, 0, length))
        break;		// Accept for now

      // Next check for an incomplete negative prefix
      if (_negativePrefixLen > 0 && endIndex < _negativePrefixLen && length <= _negativePrefixLen &&
          targetString.regionMatches(0, _negativePrefix, 0, length))
        break;		// Accept for now

      // Allow a number that ends with the group
      // or decimal separator, if these are in use
      char lastChar = targetString.charAt(originalLength - 1);
      int decimalIndex = targetString.indexOf(_decimalSeparator);

      if (_format.isGroupingUsed() && lastChar == _groupingSeparator && decimalIndex == -1)
        break;  // Allow a "," but only in integer part

      if(_format.isParseIntegerOnly() == false && lastChar == _decimalSeparator && decimalIndex == originalLength - 1)
      {
        // this happens when the decimal is types as the first char, and there is nothing else
        value = new Double(".0");
        break; // Allow a ".", but only one
      }
      // No more corrections to make: must be an error
      if (_errorListener != null)
        _errorListener.insertFailed(this, offset, str, a);
      return null;

    } while (true == false);

    return value;
  }

  /**
   * Add an error listener to be notified upon bad input.
   *
   * @param l the InsertErrorListener which is called if non-numeric input is entered.
   */
  public void addInsertErrorListener(InsertErrorListener l)
  {
    if (_errorListener != null)
      throw new IllegalArgumentException("InsertErrorListener already registered");
    _errorListener = l;
  }

  /**
   * Remove an error listener to be notified upon bad input.
   *
   * @param l the which InsertErrorListener to remove.
   */
  public void removeInsertErrorListener(InsertErrorListener l)
  {
    if (_errorListener == l)
      _errorListener = null;
  }

  /**
   * Interface for the InsertErrorListener.
   */
  public interface InsertErrorListener
  {
    public abstract void insertFailed(NumericPlainDocument doc, int offset, String str, AttributeSet a);
  }
}
