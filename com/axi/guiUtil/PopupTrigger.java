package com.axi.guiUtil;

import java.awt.event.*;
import javax.swing.JPopupMenu;

/**
 * Title: PopupTrigger
 * Description: This class handles the showing of a popup menu for a component.
 * Add an instance of this class as a mouse listener to the component.
 *
 * Copyright:    Copyright (c) 2000
 * Company:      Agilent Technologies, Inc.
 * @author Steve Anonson
 * @version 1.0
 */

public class PopupTrigger extends MouseAdapter
{
  private JPopupMenu _popup;

  /**
   * Constructor
   *
   * @param   popup - the menu to show when triggered in the component.
   */
  public PopupTrigger( JPopupMenu popup )
  {
    _popup = popup;
  }

  /**
   * Show the popup menu if the mouse event is the popup trigger.
   *
   * @param   me - the mouse event.
   */
  private void showIfPopupTrigger( MouseEvent me )
  {
    if (me.isPopupTrigger())
    {
      _popup.show( me.getComponent(), me.getX(), me.getY() );
    }
  }

  /**
   * Pass the mouse pressed event to the showIfPopupTrigger method.
   *
   * @param   me - the mouse event.
   */
  public void mousePressed( MouseEvent me )
  {
    showIfPopupTrigger(me);
  }

  /**
   * Pass the mouse released event to the showIfPopupTrigger method.
   *
   * @param   me - the mouse event.
   */
  public void mouseReleased( MouseEvent me )
  {
    showIfPopupTrigger(me);
  }
}