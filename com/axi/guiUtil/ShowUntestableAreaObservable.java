package com.axi.guiUtil;

import java.util.*;

/**
 * @author huai-en.janan-ezekie
 */
public class ShowUntestableAreaObservable extends Observable
{
  /**
   * @author Janan Wong
   */
  ShowUntestableAreaObservable()
  {
    //do nothing
  }

  /**
   * @author Janan Wong
   */
  public void setUntestableAreaVisible(boolean setUntestableAreaVisible)
  {
    setChanged();
    notifyObservers(new Boolean(setUntestableAreaVisible));
  } 
}
