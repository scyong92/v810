package com.axi.guiUtil;

import javax.swing.*;

import com.axi.util.*;

/**
* This class is a subclass of JEditorPane, and is used for changing the font size
* when in text/html mode.  The setText method is overridden to insert a font html
* tag into the text before passing it up to the super.setText
* <br>
* Behavior:
*   <li> Font sizes 0 - 12 : no change
*   <li> Font sizes 12 - 16:  insert html tag "font size=+1"
*   <li> Font sizes 16 and bigger:  insert html tag "font size=+2"
*
* @author Andy Mechtenberg
*/
public class FontSizeEditorPane extends JEditorPane
{
  final static int SMALLFONTSIZE = 12;
  final static int MEDIUMFONTSIZE = 16;
  private String _msg = null;
  private int _fontSize = SMALLFONTSIZE;

  public FontSizeEditorPane()
  {
    super();
  }

  /**
  * Adds the <font size=+?> tag (based on current font size setting) and
  * calls the super.setText
  *
  * @author Andy Mechtenberg
  */
  public void setText(String text)
  {
    _msg = text;

    super.setText(sizeTextToFontSize(_msg, _fontSize));
  }

  /**
  * Changes the current text by adding the <font size=+?> tag
  * (based on passed in font size setting) and
  * calls the super.setText
  *
  * @author Andy Mechtenberg
  */
  public void setFontSize(int fontSize)
  {
    _fontSize = fontSize;
    String str = sizeTextToFontSize(_msg, fontSize);
    Assert.expect(str != null);

    super.setText(str);
  }

  private String sizeTextToFontSize(String text, int size)
  {
    String newtext = text;

    if (size <= SMALLFONTSIZE)
       newtext = text;
    if (size > SMALLFONTSIZE && _fontSize <= MEDIUMFONTSIZE)
       newtext = "<font size=+1>" + text;
    if (size > MEDIUMFONTSIZE)
       newtext = "<font size=+2>" + text;

    return newtext;
  }
}
