package com.axi.guiUtil;

import com.axi.util.*;

/**
 *
 * @author swee-yee.wong
 */
public class CheckBoxItem
{
  private String _name = null;
  private boolean _selected = false;
  private String _message = null;
  
  public CheckBoxItem(String name, boolean selected, String message)
  {
    Assert.expect(name != null);
    
    _name = name;
    _selected = selected;
    _message = message;
  }
  
  public void setName(String name)
  {
    Assert.expect(name != null);
    _name = name;
  }
  
  public String getName()
  {
    Assert.expect(_name != null);
    return _name;
  }
  
  public void setSelected(boolean selected)
  {
    _selected = selected;
  }
  
  public boolean getSelected()
  {
    return _selected;
  }
  
  public void setMessage(String message)
  {
    Assert.expect(message != null);
    _message = message;
  }
  
  public String getMessage()
  {
    return _message;
  }
}
