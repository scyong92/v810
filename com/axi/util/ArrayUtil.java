package com.axi.util;

import java.nio.*;
import java.util.*;

/**
 * This class contains some (hopefully) useful helper methods for working with primitive arrays.
 *
 * @author Matt Wharton
 */
public class ArrayUtil
{
  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  private ArrayUtil()
  {
    // Do nothing.
  }

  /**
   * Finds the minumum value in the specified byte[].
   *
   * @author Matt Wharton
   */
  public static byte min(byte[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return min(array, 0, array.length);
  }

  /**
   * Finds the minimum value in the specified byte array between the specified starting index (inclusive) and
   * end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static byte min(byte[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    byte currentMin = array[startIndex];

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] < currentMin)
      {
        currentMin = array[i];
      }
    }

    return currentMin;
  }

  /**
   * Finds the index which corresponds to the min value in the specified byte[].
   *
   * @author Matt Wharton
   */
  public static int minIndex(byte[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return minIndex(array, 0, array.length);
  }

  /**
   * Finds the index which corresponds to the minimum value in the specified byte array between the specified
   * starting index (inclusive) and end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static int minIndex(byte[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    byte currentMin = array[startIndex];
    int minIndex = startIndex;

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] < currentMin)
      {
        currentMin = array[i];
        minIndex = i;
      }
    }

    return minIndex;
  }

  /**
   * Finds the minumum value in the specified short[].
   *
   * @author Matt Wharton
   */
  public static short min(short[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return min(array, 0, array.length);
  }

  /**
   * Finds the minimum value in the specified short array between the specified starting index (inclusive) and
   * end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static short min(short[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    short currentMin = array[startIndex];

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] < currentMin)
      {
        currentMin = array[i];
      }
    }

    return currentMin;
  }

  /**
   * Finds the index which corresponds to the min value in the specified short[].
   *
   * @author Matt Wharton
   */
  public static int minIndex(short[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return minIndex(array, 0, array.length);
  }

  /**
   * Finds the index which corresponds to the minimum value in the specified short array between the specified
   * starting index (inclusive) and end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static int minIndex(short[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    short currentMin = array[startIndex];
    int minIndex = startIndex;

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] < currentMin)
      {
        currentMin = array[i];
        minIndex = i;
      }
    }

    return minIndex;
  }

  /**
   * Finds the minumum value in the specified int[].
   *
   * @author Matt Wharton
   */
  public static int min(int[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return min(array, 0, array.length);
  }

  /**
   * Finds the minimum value in the specified int array between the specified starting index (inclusive) and
   * end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static int min(int[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    int currentMin = array[startIndex];

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] < currentMin)
      {
        currentMin = array[i];
      }
    }

    return currentMin;
  }

  /**
   * Finds the index which corresponds to the min value in the specified int[].
   *
   * @author Matt Wharton
   */
  public static int minIndex(int[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return minIndex(array, 0, array.length);
  }

  /**
   * Finds the index which corresponds to the minimum value in the specified int array between the specified
   * starting index (inclusive) and end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static int minIndex(int[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    int currentMin = array[startIndex];
    int minIndex = startIndex;

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] < currentMin)
      {
        currentMin = array[i];
        minIndex = i;
      }
    }

    return minIndex;
  }

  /**
   * Finds the minumum value in the specified long[].
   *
   * @author Matt Wharton
   */
  public static long min(long[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return min(array, 0, array.length);
  }

  /**
   * Finds the minimum value in the specified long array between the specified starting index (inclusive) and
   * end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static long min(long[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    long currentMin = array[startIndex];

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] < currentMin)
      {
        currentMin = array[i];
      }
    }

    return currentMin;
  }

  /**
   * Finds the index which corresponds to the min value in the specified long[].
   *
   * @author Matt Wharton
   */
  public static int minIndex(long[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return minIndex(array, 0, array.length);
  }

  /**
   * Finds the index which corresponds to the minimum value in the specified long array between the specified
   * starting index (inclusive) and end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static int minIndex(long[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    long currentMin = array[startIndex];
    int minIndex = startIndex;

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] < currentMin)
      {
        currentMin = array[i];
        minIndex = i;
      }
    }

    return minIndex;
  }

  /**
   * Finds the minumum value in the specified long[].
   *
   * @author Matt Wharton
   */
  public static float min(float[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return min(array, 0, array.length);
  }

  /**
   * Finds the minimum value in the specified float array between the specified starting index (inclusive) and
   * end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static float min(float[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    float currentMin = array[startIndex];

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] < currentMin)
      {
        currentMin = array[i];
      }
    }

    return currentMin;
  }

  /**
   * Finds the index which corresponds to the min value in the specified float[].
   *
   * @author Matt Wharton
   */
  public static int minIndex(float[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return minIndex(array, 0, array.length);
  }

  /**
   * Finds the index which corresponds to the minimum value in the specified float array between the specified
   * starting index (inclusive) and end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static int minIndex(float[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    float currentMin = array[startIndex];
    int minIndex = startIndex;

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] < currentMin)
      {
        currentMin = array[i];
        minIndex = i;
      }
    }

    return minIndex;
  }

  /**
   * Finds the minumum value in the specified double[].
   *
   * @author Matt Wharton
   */
  public static double min(double[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return min(array, 0, array.length);
  }

  /**
   * Finds the minimum value in the specified double array between the specified starting index (inclusive) and
   * end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static double min(double[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    double currentMin = array[startIndex];

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] < currentMin)
      {
        currentMin = array[i];
      }
    }

    return currentMin;
  }

  /**
   * Finds the index which corresponds to the min value in the specified double[].
   *
   * @author Matt Wharton
   */
  public static int minIndex(double[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return minIndex(array, 0, array.length);
  }

  /**
   * Finds the index which corresponds to the minimum value in the specified double array between the specified
   * starting index (inclusive) and end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static int minIndex(double[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    double currentMin = array[startIndex];
    int minIndex = startIndex;

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] < currentMin)
      {
        currentMin = array[i];
        minIndex = i;
      }
    }

    return minIndex;
  }

  /**
   * Finds the maximum value in the specified byte[].
   *
   * @author Matt Wharton
   */
  public static byte max(byte... array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return max(array, 0, array.length);
  }

  /**
   * Finds the maximum value in the specified byte array between the specified starting index (inclusive) and
   * end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static byte max(byte[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    byte currentMax = array[startIndex];

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] > currentMax)
      {
        currentMax = array[i];
      }
    }

    return currentMax;
  }

  /**
   * Finds the index which corresponds to the maximum value in the specified byte[].
   *
   * @author Matt Wharton
   */
  public static int maxIndex(byte[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return maxIndex(array, 0, array.length);
  }

  /**
   * Finds the index which corresponds to the maximum value in the specified byte array between the specified
   * starting index (inclusive) and end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static int maxIndex(byte[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    byte currentMax = array[startIndex];
    int maxIndex = startIndex;

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] > currentMax)
      {
        currentMax = array[i];
        maxIndex = i;
      }
    }

    return maxIndex;
  }

  /**
   * Finds the maximum value in the specified short[].
   *
   * @author Matt Wharton
   */
  public static short max(short[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return max(array, 0, array.length);
  }

  /**
   * Finds the maximum value in the specified short array between the specified starting index (inclusive) and
   * end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static short max(short[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    short currentMax = array[startIndex];

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] > currentMax)
      {
        currentMax = array[i];
      }
    }

    return currentMax;
  }

  /**
   * Finds the index which corresponds to the maximum value in the specified short[].
   *
   * @author Matt Wharton
   */
  public static int maxIndex(short[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return maxIndex(array, 0, array.length);
  }

  /**
   * Finds the index which corresponds to the maximum value in the specified short array between the specified
   * starting index (inclusive) and end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static int maxIndex(short[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    short currentMax = array[startIndex];
    int maxIndex = startIndex;

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] > currentMax)
      {
        currentMax = array[i];
        maxIndex = i;
      }
    }

    return maxIndex;
  }

  /**
   * Finds the maximum value in the specified int[].
   *
   * @author Matt Wharton
   */
  public static int max(int[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return max(array, 0, array.length);
  }

  /**
   * Finds the maximum value in the specified int array between the specified starting index (inclusive) and
   * end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static int max(int[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    int currentMax = array[startIndex];

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] > currentMax)
      {
        currentMax = array[i];
      }
    }

    return currentMax;
  }

  /**
   * Finds the index which corresponds to the maximum value in the specified int[].
   *
   * @author Matt Wharton
   */
  public static int maxIndex(int[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return maxIndex(array, 0, array.length);
  }

  /**
   * Finds the index which corresponds to the maximum value in the specified int array between the specified
   * starting index (inclusive) and end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static int maxIndex(int[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    int currentMax = array[startIndex];
    int maxIndex = startIndex;

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] > currentMax)
      {
        currentMax = array[i];
        maxIndex = i;
      }
    }

    return maxIndex;
  }

  /**
   * Finds the minumum value in the specified long[].
   *
   * @author Matt Wharton
   */
  public static long max(long[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return max(array, 0, array.length);
  }

  /**
   * Finds the maximum value in the specified long array between the specified starting index (inclusive) and
   * end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static long max(long[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    long currentMax = array[startIndex];

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] > currentMax)
      {
        currentMax = array[i];
      }
    }

    return currentMax;
  }

  /**
   * Finds the index which corresponds to the maximum value in the specified long[].
   *
   * @author Matt Wharton
   */
  public static int maxIndex(long[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return maxIndex(array, 0, array.length);
  }

  /**
   * Finds the index which corresponds to the maximum value in the specified long array between the specified
   * starting index (inclusive) and end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static int maxIndex(long[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    long currentMax = array[startIndex];
    int maxIndex = startIndex;

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] > currentMax)
      {
        currentMax = array[i];
        maxIndex = i;
      }
    }

    return maxIndex;
  }

  /**
   * Finds the minumum value in the specified long[].
   *
   * @author Matt Wharton
   */
  public static float max(float[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return max(array, 0, array.length);
  }

  /**
   * Finds the maximum value in the specified float array between the specified starting index (inclusive) and
   * end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static float max(float[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    float currentMax = array[startIndex];

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] > currentMax)
      {
        currentMax = array[i];
      }
    }

    return currentMax;
  }

  /**
   * Finds the index which corresponds to the maximum value in the specified float[].
   *
   * @author Matt Wharton
   */
  public static int maxIndex(float[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return maxIndex(array, 0, array.length);
  }

  /**
   * Finds the index which corresponds to the maximum value in the specified float array between the specified
   * starting index (inclusive) and end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static int maxIndex(float[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    float currentMax = array[startIndex];
    int maxIndex = startIndex;

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] > currentMax)
      {
        currentMax = array[i];
        maxIndex = i;
      }
    }

    return maxIndex;
  }

  /**
   * Finds the minumum value in the specified double[].
   *
   * @author Matt Wharton
   */
  public static double max(double[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return max(array, 0, array.length);
  }

  /**
   * Finds the maximum value in the specified double array between the specified starting index (inclusive) and
   * end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static double max(double[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    double currentMax = array[startIndex];

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] > currentMax)
      {
        currentMax = array[i];
      }
    }

    return currentMax;
  }

  /**
   * Finds the index which corresponds to the maximum value in the specified double[].
   *
   * @author Matt Wharton
   */
  public static int maxIndex(double[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return maxIndex(array, 0, array.length);
  }

  /**
   * Finds the index which corresponds to the maximum value in the specified double array between the specified
   * starting index (inclusive) and end index (exclusive).
   *
   * @author Matt Wharton
   */
  public static int maxIndex(double[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    double currentMax = array[startIndex];
    int maxIndex = startIndex;

    for (int i = startIndex + 1; i < endIndex; ++i)
    {
      if (array[i] > currentMax)
      {
        currentMax = array[i];
        maxIndex = i;
      }
    }

    return maxIndex;
  }

  /**
   * Subtracts the values in the second int[] from the values in the first.
   * The results are returned in a new int[].
   *
   * NOTE: This does not do any explicit bounds checking.
   *
   * @author Matt Wharton
   */
  public static int[] subtractArrays(int[] firstArray, int[] secondArray)
  {
    Assert.expect(firstArray != null);
    Assert.expect(secondArray != null);
    Assert.expect(firstArray.length == secondArray.length, "Array lengths don't match!");

    // Create a new array for the results of the subtraction.
    int[] differenceArray = new int[firstArray.length];
    for (int i = 0; i < differenceArray.length; ++i)
    {
      differenceArray[i] = firstArray[i] - secondArray[i];
    }

    return differenceArray;
  }

  /**
   * Subtracts the values in the second long[] from the values in the first.
   * The results are returned in a new long[].
   *
   * NOTE: This does not do any explicit bounds checking.
   *
   * @author Matt Wharton
   */
  public static long[] subtractArrays(long[] firstArray, long[] secondArray)
  {
    Assert.expect(firstArray != null);
    Assert.expect(secondArray != null);
    Assert.expect(firstArray.length == secondArray.length, "Array lengths don't match!");

    // Create a new array for the results of the subtraction.
    long[] differenceArray = new long[firstArray.length];
    for (int i = 0; i < differenceArray.length; ++i)
    {
      differenceArray[i] = firstArray[i] - secondArray[i];
    }

    return differenceArray;
  }

  /**
   * Subtracts the values in the second float[] from the values in the first.
   * The results are returned in a new float[].
   *
   * NOTE: This does not do any explicit bounds checking.
   *
   * @author Matt Wharton
   */
  public static float[] subtractArrays(float[] firstArray, float[] secondArray)
  {
    Assert.expect(firstArray != null);
    Assert.expect(secondArray != null);
    Assert.expect(firstArray.length == secondArray.length, "Array lengths don't match!");

    // Create a new array for the results of the subtraction.
    float[] differenceArray = new float[firstArray.length];
    for (int i = 0; i < differenceArray.length; ++i)
    {
      differenceArray[i] = firstArray[i] - secondArray[i];
    }

    return differenceArray;
  }

  /**
   * Subtracts the values in the second double[] from the values in the first.
   * The results are returned in a new double[].
   *
   * NOTE: This does not do any explicit bounds checking.
   *
   * @author Matt Wharton
   */
  public static double[] subtractArrays(double[] firstArray, double[] secondArray)
  {
    Assert.expect(firstArray != null);
    Assert.expect(secondArray != null);
    Assert.expect(firstArray.length == secondArray.length, "Array lengths don't match!");

    // Create a new array for the results of the subtraction.
    double[] differenceArray = new double[firstArray.length];
    for (int i = 0; i < differenceArray.length; ++i)
    {
      differenceArray[i] = firstArray[i] - secondArray[i];
    }

    return differenceArray;
  }

  /**
   * Converts the specified byte[] to a float[].  The number of elements in the byte[] must be
   * divisible by 4.  Assumes "big endian" byte ordering.
   *
   * @author Matt Wharton
   */
  public static float[] convertByteArrayToFloatArray(byte[] byteArray)
  {
    return convertByteArrayToFloatArray(byteArray, ByteOrder.BIG_ENDIAN);
  }

  /**
   * Converts the specified byte[] to a float[].  The number of elements in the byte[] must be
   * divisible by 4.  Uses the specified byte order.
   *
   * @author Matt Wharton
   */
  public static float[] convertByteArrayToFloatArray(byte[] byteArray, ByteOrder byteOrder)
  {
    Assert.expect(byteArray != null);
    Assert.expect(byteOrder != null);
    Assert.expect((byteArray.length % 4) == 0, "byteArray.length is not divisible by 4");

    ByteBuffer byteBuffer = ByteBuffer.wrap(byteArray);
    byteBuffer.order(byteOrder);
    FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
    float[] floatArray = new float[byteArray.length / 4];
    floatBuffer.get(floatArray);

    return floatArray;
  }

  /**
   * Converts the specified float[] to a byte[].  Assumes "big endian" byte ordering.
   *
   * @author Matt Wharton
   */
  public static byte[] convertFloatArrayToByteArray(float[] floatArray)
  {
    return convertFloatArrayToByteArray(floatArray, ByteOrder.BIG_ENDIAN);
  }

  /**
   * Converts the specified float[] to a byte[].  Uses the specified byte order.
   *
   * @author Matt Wharton
   */
  public static byte[] convertFloatArrayToByteArray(float[] floatArray, ByteOrder byteOrder)
  {
    Assert.expect(floatArray != null);
    Assert.expect(byteOrder != null);

    int byteBufferSize = floatArray.length * 4;
    ByteBuffer byteBuffer = ByteBuffer.allocate(byteBufferSize);
    byteBuffer.order(byteOrder);
    FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
    floatBuffer.put(floatArray);
    byte[] byteArray = new byte[byteBufferSize];
    byteBuffer.get(byteArray);

    return byteArray;
  }

  /**
   * @author Peter Esbensen
   */
  public static float[] convertFloatListToFloatArray(List<Float> floatList)
  {
    Assert.expect(floatList != null);

    float[] floatArray = new float[floatList.size()];
    for (int index = 0; index < floatList.size(); ++index)
    {
      floatArray[index] = floatList.get(index);
    }
    return floatArray;
  }

  /**
   * @author Sunit Bhalla
   */
  public static float[] convertDoubleListToFloatArray(List<Double> doubleList)
  {
    Assert.expect(doubleList != null);

    float[] floatArray = new float[doubleList.size()];
    for (int index = 0; index < doubleList.size(); ++index)
    {
      floatArray[index] = doubleList.get(index).floatValue();
    }
    return floatArray;
  }

  /**
   * @author Sunit Bhalla
   */
  public static List<Double> convertFloatArrayToDoubleList(float[] floatArray)
  {
    Assert.expect(floatArray != null);
    List<Double> doubleList = new ArrayList<Double>(floatArray.length);
    for (int i = 0; i < floatArray.length; i++)
      doubleList.add((double)floatArray[i]);

    Assert.expect(doubleList != null);
    return doubleList;
  }

  /**
   * @author Sunit Bhalla
   */
  public static List<Float> convertFloatArrayToFloatList(float[] floatArray)
  {
    Assert.expect(floatArray != null);
    List<Float> floatList = new ArrayList<Float>(floatArray.length);
    for (int i = 0; i < floatArray.length; i++)
      floatList.add((float)floatArray[i]);

    Assert.expect(floatList != null);
    return floatList;
  }

  /**
   * Combines the specified byte[][] into a single byte[].
   *
   * @author Matt Wharton
   */
  public static byte[] combineArrays(byte[][] arrays)
  {
    Assert.expect(arrays != null);
    Assert.expect(arrays.length > 0);

    // Calculate the total number of elements in all the arrays.
    int totalLength = 0;
    for (byte[] innerArray : arrays)
    {
      totalLength += innerArray.length;
    }

    // Combine the data in each subarray into a single, aggregate array.
    byte[] combinedArray = new byte[totalLength];
    int currentPositionInCombinedArray = 0;
    for (byte[] innerArray : arrays)
    {
      System.arraycopy(innerArray, 0, combinedArray, currentPositionInCombinedArray, innerArray.length);
      currentPositionInCombinedArray += innerArray.length;
    }

    return combinedArray;
  }

  /**
   * Combines the specified short[][] into a single short[].
   *
   * @author Matt Wharton
   */
  public static short[] combineArrays(short[][] arrays)
  {
    Assert.expect(arrays != null);
    Assert.expect(arrays.length > 0);

    // Calculate the total number of elements in all the arrays.
    int totalLength = 0;
    for (short[] innerArray : arrays)
    {
      totalLength += innerArray.length;
    }

    // Combine the data in each subarray into a single, aggregate array.
    short[] combinedArray = new short[totalLength];
    int currentPositionInCombinedArray = 0;
    for (short[] innerArray : arrays)
    {
      System.arraycopy(innerArray, 0, combinedArray, currentPositionInCombinedArray, innerArray.length);
      currentPositionInCombinedArray += innerArray.length;
    }

    return combinedArray;
  }

  /**
   * Combines the specified int[][] into a single int[].
   *
   * @author Matt Wharton
   */
  public static int[] combineArrays(int[][] arrays)
  {
    Assert.expect(arrays != null);
    Assert.expect(arrays.length > 0);

    // Calculate the total number of elements in all the arrays.
    int totalLength = 0;
    for (int[] innerArray : arrays)
    {
      totalLength += innerArray.length;
    }

    // Combine the data in each subarray into a single, aggregate array.
    int[] combinedArray = new int[totalLength];
    int currentPositionInCombinedArray = 0;
    for (int[] innerArray : arrays)
    {
      System.arraycopy(innerArray, 0, combinedArray, currentPositionInCombinedArray, innerArray.length);
      currentPositionInCombinedArray += innerArray.length;
    }

    return combinedArray;
  }

  /**
   * Combines the specified long[][] into a single long[].
   *
   * @author Matt Wharton
   */
  public static long[] combineArrays(long[][] arrays)
  {
    Assert.expect(arrays != null);
    Assert.expect(arrays.length > 0);

    // Calculate the total number of elements in all the arrays.
    int totalLength = 0;
    for (long[] innerArray : arrays)
    {
      totalLength += innerArray.length;
    }

    // Combine the data in each subarray into a single, aggregate array.
    long[] combinedArray = new long[totalLength];
    int currentPositionInCombinedArray = 0;
    for (long[] innerArray : arrays)
    {
      System.arraycopy(innerArray, 0, combinedArray, currentPositionInCombinedArray, innerArray.length);
      currentPositionInCombinedArray += innerArray.length;
    }

    return combinedArray;
  }

  /**
   * Combines the specified float[][] into a single float[].
   *
   * @author Matt Wharton
   */
  public static float[] combineArrays(float[][] arrays)
  {
    Assert.expect(arrays != null);
    Assert.expect(arrays.length > 0);

    // Calculate the total number of elements in all the arrays.
    int totalLength = 0;
    for (float[] innerArray : arrays)
    {
      totalLength += innerArray.length;
    }

    // Combine the data in each subarray into a single, aggregate array.
    float[] combinedArray = new float[totalLength];
    int currentPositionInCombinedArray = 0;
    for (float[] innerArray : arrays)
    {
      System.arraycopy(innerArray, 0, combinedArray, currentPositionInCombinedArray, innerArray.length);
      currentPositionInCombinedArray += innerArray.length;
    }

    return combinedArray;
  }

  /**
   * Combines the specified double[][] into a single double[].
   *
   * @author Matt Wharton
   */
  public static double[] combineArrays(double[][] arrays)
  {
    Assert.expect(arrays != null);
    Assert.expect(arrays.length > 0);

    // Calculate the total number of elements in all the arrays.
    int totalLength = 0;
    for (double[] innerArray : arrays)
    {
      totalLength += innerArray.length;
    }

    // Combine the data in each subarray into a single, aggregate array.
    double[] combinedArray = new double[totalLength];
    int currentPositionInCombinedArray = 0;
    for (double[] innerArray : arrays)
    {
      System.arraycopy(innerArray, 0, combinedArray, currentPositionInCombinedArray, innerArray.length);
      currentPositionInCombinedArray += innerArray.length;
    }

    return combinedArray;
  }

  /**
   * @author Patrick Lacz
   */
  public static int[] makeArray(int... values)
  {
    return values;
  }

  /**
   * @author Patrick Lacz
   */
  public static short[] makeArray(short... values)
  {
    return values;
  }

  /**
   * @author Patrick Lacz
   */
  public static long[] makeArray(long... values)
  {
    return values;
  }

  /**
   * @author Patrick Lacz
   */
  public static float[] makeArray(float... values)
  {
    return values;
  }

  /**
   * @author Patrick Lacz
   */
//  public static <T> T[] makeArray(T... values)
//  {
//    return values;
//  }

  /**
   * Get the subpixel index of the minimum value, estimating the minimum location
   * by fitting the local area around the minimum to a quadratic curve.
   *
   * @author Peter Esbensen
   */
  static public float interpolatedMinIndex(float[] array)
  {
    Assert.expect(array != null);

    return interpolatedMinIndex(array, 0, array.length);
  }

  /**
   * Get the subpixel index of the minimum value, estimating the minimum location
   * by fitting the local area around the minimum to a quadratic curve.
   *
   * @param startIndex is the first index to start looking for the minimum location
   * @param endIndex is the index where we stop looking . . . the last index considered is (endIndex - 1)
   *
   * @author Peter Esbensen
   */
  static public float interpolatedMinIndex(float[] array, int startIndex, int endIndex)
  {
    Assert.expect(startIndex < endIndex);
    Assert.expect( (startIndex >= 0) && (startIndex < array.length) );
    Assert.expect( (endIndex > 0) && (endIndex <= array.length) );
    Assert.expect(array != null);

    int minimumValueIndex = ArrayUtil.minIndex(array, startIndex, endIndex);

    if ((minimumValueIndex < 1) || (minimumValueIndex > array.length - 2))
    {
      // we don't have at least one bin to the left and one bin to the right,
      // so we can't do this estimation.
      return minimumValueIndex;
    }

    // fit a quadratic to the three bins centered on the minimum in order to
    // estimate the subpixel minimum

    // the quadratic is of the form: y = Ax^2 + Bx + C

    DoubleRef quadraticCoefficientA = new DoubleRef();
    DoubleRef quadraticCoefficientB = new DoubleRef();
    DoubleRef quadraticCoefficientC = new DoubleRef();
    int x1 = minimumValueIndex - 1;
    float y1 = array[x1];
    int x2 = minimumValueIndex;
    float y2 = array[x2];
    int x3 = minimumValueIndex + 1;
    float y3 = array[x3];

    MathUtil.fitQuadraticToCoordinates(x1, y1, x2, y2, x3, y3, quadraticCoefficientA,
                                       quadraticCoefficientB, quadraticCoefficientC);

    // Take the first derivative of the equation above and solve for where it is zero.
    if (Math.abs(quadraticCoefficientA.getValue()) < Float.MIN_VALUE)
    {
      return minimumValueIndex;
    }
    return -1.0f * (float)quadraticCoefficientB.getValue() / (2.0f * (float)quadraticCoefficientA.getValue());
  }

  /**
   * Get the subpixel index of the minimum value, estimating the minimum location
   * by fitting the local area around the minimum to a quadratic curve.
   *
   * @param startIndex is the first index to start looking for the minimum location
   * @param endIndex is the index where we stop looking . . . the last index considered is (endIndex - 1)
   *
   * @author Peter Esbensen
   */
  static public float interpolatedMaxIndex(float[] array, int startIndex, int endIndex)
  {
    Assert.expect(startIndex < endIndex);
    Assert.expect((startIndex >= 0) && (startIndex < array.length));
    Assert.expect((endIndex > 0) && (endIndex <= array.length));
    Assert.expect(array != null);

    int maximumValueIndex = ArrayUtil.maxIndex(array, startIndex, endIndex);

    if ((maximumValueIndex < 1) || (maximumValueIndex > array.length - 2))
    {
      // we don't have at least one bin to the left and one bin to the right,
      // so we can't do this estimation.
      return maximumValueIndex;
    }

    // fit a quadratic to the three bins centered on the minimum in order to
    // estimate the subpixel maximum

    // quadratic is of the form: y = Ax^2 + Bx + C
    DoubleRef quadraticCoefficientA = new DoubleRef();
    DoubleRef quadraticCoefficientB = new DoubleRef();
    DoubleRef quadraticCoefficientC = new DoubleRef();
    int x1 = maximumValueIndex - 1;
    float y1 = array[x1];
    int x2 = maximumValueIndex;
    float y2 = array[x2];
    int x3 = maximumValueIndex + 1;
    float y3 = array[x3];

    MathUtil.fitQuadraticToCoordinates(x1, y1, x2, y2, x3, y3, quadraticCoefficientA,
                                       quadraticCoefficientB, quadraticCoefficientC);

    // Take the first derivative of the equation above and solve for
    // where it is zero.

    if (Math.abs(quadraticCoefficientA.getValue()) < Float.MIN_VALUE)
    {
      return maximumValueIndex;
    }
    return -1.0f * (float)quadraticCoefficientB.getValue() / (2.0f * (float)quadraticCoefficientA.getValue());
  }

  /**
   * Get the subpixel index of the minimum value, estimating the minimum location
   * by fitting the local area around the minimum to a quadratic curve.
   *
   * @author Peter Esbensen
   */
  static public float interpolatedMaxIndex(float[] array)
  {
    Assert.expect(array != null);

    return interpolatedMaxIndex(array, 0, array.length);
  }

  /**
   * Creates a new array with every mil value in the specified array converted to mm.
   *
   * @author Matt Wharton
   */
  public static float[] convertMilsToMillimeters(float[] milsArray)
  {
    Assert.expect(milsArray != null);

    float[] millimetersArray = new float[milsArray.length];
    for (int i = 0; i < milsArray.length; ++i)
    {
      millimetersArray[i] = MathUtil.convertMilsToMillimeters(milsArray[i]);
    }

    return millimetersArray;
  }

  /**
   * Creates a new array with every mil value in the specified array converted to millimeters.
   *
   * @author Matt Wharton
   */
  public static double[] convertMilsToMillimeters(double[] milsArray)
  {
    Assert.expect(milsArray != null);

    double[] millimetersArray = new double[milsArray.length];
    for (int i = 0; i < milsArray.length; ++i)
    {
      millimetersArray[i] = MathUtil.convertMilsToMillimeters(milsArray[i]);
    }

    return millimetersArray;
  }

  /**
   * Creates a new array with every millimeter value in the specified array converted to mils.
   *
   * @author Matt Wharton
   */
  public static float[] convertMillimetersToMils(float[] millimetersArray)
  {
    Assert.expect(millimetersArray != null);

    float[] milsArray = new float[millimetersArray.length];
    for (int i = 0; i < millimetersArray.length; ++i)
    {
      milsArray[i] = MathUtil.convertMillimetersToMils(millimetersArray[i]);
    }

    return milsArray;
  }

  /**
   * Creates a new array with every millimeter value in the specified array converted to mils.
   *
   * @author Matt Wharton
   */
  public static double[] convertMillimetersToMils(double[] millimetersArray)
  {
    Assert.expect(millimetersArray != null);

    double[] milsArray = new double[millimetersArray.length];
    for (int i = 0; i < millimetersArray.length; ++i)
    {
      milsArray[i] = MathUtil.convertMillimetersToMils(millimetersArray[i]);
    }

    return milsArray;
  }

  /**
   * @author Patrick Lacz
   */
  public static Pair<Float, Float> getProfileExtents(float[] profile)
  {
    Assert.expect(profile != null);
    Assert.expect(profile.length > 0);

    float minValue = min(profile);
    float maxValue = max(profile);

    if (MathUtil.fuzzyEquals(minValue, 0.0f))
    {
      minValue = 0.0f;
    }
    else
    {
      double logMin = Math.round(Math.log10(minValue));
      double base = Math.pow(10, logMin-1);
      minValue = (float)(Math.floor(minValue/base)*base);
    }

    if (MathUtil.fuzzyEquals(maxValue, 0.0f))
    {
      maxValue = 0.0f;
    }
    else
    {
      double logMax = Math.round(Math.log10(maxValue));
      double base = Math.pow(10, logMax-1);
      maxValue = (float)(Math.ceil(maxValue/base)*base);
    }

    return new Pair<Float, Float>(minValue, maxValue);
  }

  /**
   * @author Patrick Lacz
   */
  public static float[] resampleProfile(float profile[], int newNumberOfBins)
  {
    Assert.expect(profile != null);
    Assert.expect(newNumberOfBins >= 1);

    if (newNumberOfBins < profile.length)
      return downsampleProfile(profile, newNumberOfBins);
    else if (newNumberOfBins > profile.length)
      return upsampleProfile(profile, newNumberOfBins);
    else
      return profile;
  }

  /**
   * @author Patrick Lacz
   */
  private static float[] downsampleProfile(float profile[], int newNumberOfBins)
  {
    Assert.expect(profile != null);
    Assert.expect(newNumberOfBins < profile.length);

    float binStepSize = ((float)newNumberOfBins - 1)/(profile.length - 1);
    float resultProfile[] = new float[newNumberOfBins];
    int numberOfAdds[] = new int[newNumberOfBins];

    for (int i = 0 ; i < profile.length ; ++i)
    {
      int binInResult = (int)(i*binStepSize);
      resultProfile[binInResult] += profile[i];
      numberOfAdds[binInResult]++;
    }

    for (int j = 0 ; j < resultProfile.length ; ++j)
    {
      Assert.expect(numberOfAdds[j] != 0);
      resultProfile[j] /= numberOfAdds[j];
    }

    return resultProfile;
  }

  /**
   * @author Patrick Lacz
   */
  private static float[] upsampleProfile(float profile[], int newNumberOfBins)
  {
    Assert.expect(profile != null);
    Assert.expect(newNumberOfBins > profile.length);

    float binStepSize = ((float)profile.length - 1)/(newNumberOfBins - 1);
    float resultProfile[] = new float[newNumberOfBins];

    for (int i = 0 ; i < resultProfile.length ; ++i)
    {
      float floatIndex = i * binStepSize;
      resultProfile[i] = profile[Math.min(Math.round(floatIndex), profile.length)];
    }
    return resultProfile;
  }

  /**
   * @author Peter Esbensen
   */
  public static float sum(float[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    return sum(array, 0, array.length);
  }

  /**
   * Add up all the values in the array from startIndex up to, but not including, endIndex.
   *
   * @author Peter Esbensen
   */
  public static float sum(float[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    float sum = 0.0f;
    for (int i = startIndex; i < endIndex; ++i)
    {
      sum += array[i];
    }
    return sum;
  }

  /**
   * Add up all the values in the array from startIndex up to, but not including, endIndex.
   *
   * @author Peter Esbensen
   */
  public static float[] copy(float[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    float[] newCopy = new float[ endIndex - startIndex ];
    System.arraycopy(array, startIndex, newCopy, 0, endIndex - startIndex);
    return newCopy;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static float[] subtractArrayFromConstant(float[] firstArray, float value)
  {
    Assert.expect(firstArray != null);

    // Create a new array for the results of the subtraction.
    float[] differenceArray = new float[firstArray.length];
    for (int i = 0; i < differenceArray.length; ++i)
    {
      differenceArray[i] = value - firstArray[i];
    }

    return differenceArray;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static float[] addArrayByConstant(float[] firstArray, float value)
  {
    Assert.expect(firstArray != null);

    // Create a new array for the results of the subtraction.
    float[] addArray = new float[firstArray.length];
    for (int i = 0; i < addArray.length; ++i)
    {
      addArray[i] = value + firstArray[i];
    }

    return addArray;
  }
}
