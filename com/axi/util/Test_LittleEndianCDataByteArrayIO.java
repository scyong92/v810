package com.axi.util;
import java.io.*;

/**
 * @author Bill Darbie
 */
public class Test_LittleEndianCDataByteArrayIO extends UnitTest
{
  /**
  * @author Michael Martinez-Schiferl
  */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_LittleEndianCDataByteArrayIO());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    int arrayLength = 100;
    byte[] byteArray = new byte[arrayLength];
    LittleEndianCDataByteArrayIO byteArrayIO = new LittleEndianCDataByteArrayIO(byteArray);

    // test setPosition
    try
    {
      byteArrayIO.setPosition(-1);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    try
    {
      byteArrayIO.setPosition(100);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    // test writeByte(byte) and readByte()
    byteArrayIO.setPosition(0);
    byte byteValue = 0x24;
    byteArrayIO.writeByte(byteValue);
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readByte() == byteValue);

    // test readUnsignedByte
    byteArrayIO.setPosition(0);
    byteArrayIO.writeByte(-0x0b);
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readByte() == -0x0b);
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readUnsignedByte() == 0xf5);

    // test readUnsignedChar
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readUnsignedChar() == 0xf5);

    // test writeByte(int) and readByte()
    byteArrayIO.setPosition(0);
    int intValue = 0x1234;
    byteArrayIO.writeByte(intValue);
    byteArrayIO.setPosition(0);
    byte result = byteArrayIO.readByte();
    Expect.expect(result == 0x34);

    // test writeByte(byte[]) and readByte()
    byteArrayIO.setPosition(0);
    try
    {
      byteArrayIO.write(null);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    byte[] bytes = new byte[3];
    bytes[0] = 0x10;
    bytes[1] = 0x11;
    bytes[2] = 0x12;
    byteArrayIO.write(bytes);
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readByte() == bytes[0]);
    Expect.expect(byteArrayIO.readByte() == bytes[1]);
    Expect.expect(byteArrayIO.readByte() == bytes[2]);

    // test readFully(byte[])
    byteArrayIO.setPosition(0);
    try
    {
      byteArrayIO.readFully(null);
      Expect.expect(false);
    }
    catch (AssertException ae)
    {
      // do nothing
    }

    byte[] readBytes = new byte[3];
    byteArrayIO.readFully(readBytes);
    Expect.expect(readBytes[0] == bytes[0]);
    Expect.expect(readBytes[1] == bytes[1]);
    Expect.expect(readBytes[2] == bytes[2]);

    // test readFully(byte[], int offset, int length)
    byteArrayIO.setPosition(0);
    try
    {
      byteArrayIO.readFully(null, 2, 2);
      Expect.expect(false);
    }
    catch (AssertException ae)
    {
      // do nothing
    }
    readBytes = new byte[3];
    try
    {
      byteArrayIO.readFully(readBytes, -1, 2);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    try
    {
      byteArrayIO.readFully(readBytes, 2, -1);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    byteArrayIO.readFully(readBytes, 1, 2);
    Expect.expect(readBytes[1] == bytes[0]);
    Expect.expect(readBytes[2] == bytes[1]);

    // test writeBytes(String)
    byteArrayIO.setPosition(0);
    String str = "hello";
    byteArrayIO.writeBytes(str);
    byteArrayIO.writeByte('\0');
    byteArrayIO.setPosition(0);
    // add 1 to the length to account for the null terminator
    Assert.expect(byteArrayIO.readString(str.length() + 1).equals(str));

    try
    {
      byteArrayIO.writeBytes(null);
      Expect.expect(false);
    }
    catch (AssertException ae)
    {
      // do nothing
    }
    try
    {
      byteArrayIO.readString(-1);
      Expect.expect(false);
    }
    catch (AssertException ae)
    {
      // do nothing
    }

    // test writeBoolean
    byteArrayIO.setPosition(0);
    byteArrayIO.writeBoolean(true);
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readBoolean() == true);
    byteArrayIO.setPosition(0);
    byteArrayIO.writeBoolean(false);
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readBoolean() == false);

    // test writeShort();
    byteArrayIO.setPosition(0);
    short shortValue = -2;
    byteArrayIO.writeShort(shortValue);
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readShort() == shortValue);

    // test readUnsignedShort
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readUnsignedShort() == 0xfffe); // 0xfffe is the 2's complement representation of -2

    // test writeInt()
    byteArrayIO.setPosition(0);
    byteArrayIO.writeInt(-77);
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readInt() == -77);

    // test readUnsignedInt()
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readUnsignedInt() == 0xffffffffffffffb3L);

    // test writeLong()
    byteArrayIO.setPosition(0);
    long longValue = 25;
    byteArrayIO.writeLong(longValue);
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readLong() == longValue);

    // test writeFloat
    float floatValue = 1234.56f;
    byteArrayIO.setPosition(0);
    byteArrayIO.writeFloat(floatValue);
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readFloat() == floatValue);

    // test writeDouble
    double doubleValue = 7654.32;
    byteArrayIO.setPosition(0);
    byteArrayIO.writeDouble(doubleValue);
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readDouble() == doubleValue);

    // test writePascalString
    str = "hi";
    byteArrayIO.setPosition(0);
    byteArrayIO.writePascalString(str);
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readPascalString().equals(str));

    // test writePascalPackedArray();
    str = "abc";
    byteArrayIO.setPosition(0);
    byteArrayIO.writePascalPackedArray(str);
    byteArrayIO.setPosition(0);
    Expect.expect(byteArrayIO.readPascalPackedArray(str.length()).equals(str));

    // test what happens if we write past the end of the array
    byteArrayIO.setPosition(99);
    try
    {
      byteArrayIO.writeInt(100);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    // test what happens if we read past the end of the array
    byteArrayIO.setPosition(99);
    try
    {
      byteArrayIO.readInt();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
  }
}






