package com.axi.util;

import java.io.*;
import java.util.*;

/**
 *  This class provides a way to write data to a file.
 *
 * @author Greg Esparza
 */
public class FileWriterUtil
{
  private boolean _append;
  private String _fileNameFullPath;
  private PrintWriter _os;

  /**
   * @author Greg Esparza
   */
  public FileWriterUtil(String fileNameFullPath, boolean append)
  {
    Assert.expect(fileNameFullPath != null);
    _fileNameFullPath = fileNameFullPath;
    _append = append;
  }

  /**
   * @author Greg Esparza
   */
  public void open() throws CouldNotCreateFileException
  {
    // Swee Yee Wong - XCR-3356	Assert when trying to log file
    // Instead of prompting internal error, it should close the filewriter and reopen.
    //Assert.expect(_os == null);
    close();
    
    FileWriter fileWriter;
    try
    {
      fileWriter = new FileWriter(_fileNameFullPath, _append);
    }
    catch (IOException ex)
    {
      CouldNotCreateFileException couldNotCreateFileException = new CouldNotCreateFileException(_fileNameFullPath);
      couldNotCreateFileException.initCause(ex);
      throw couldNotCreateFileException;
    }
    BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
    _os = new PrintWriter(bufferedWriter);
  }

  /**
   * @author Greg Esparza
   */
  public void close()
  {
    if (_os != null)
    {
      _os.close();
      _os = null;
    }
  }

  /**
   * @author Greg Esparza
   */
  protected void finalize() throws Throwable
  {
    close();
    super.finalize();
  }

  /**
   * This method is used to write the message to the file.
   * The file be initially opened and remain open while using this method.
   *
   * @param message is the data to write to the file
   * @author Greg Esparza
   */
  public void write(String message) throws CouldNotCreateFileException
  {
    Assert.expect(message != null);
    //Swee Yee Wong - XCR-3356	Assert when trying to log file
    //Assert.expect(_os != null);
    if(_os == null)
    {
      open();
    }
    _os.print(message);
  }

  /**
   * This method is used to write the message followed by a newline to the file.
   * The file be initially opened and remain open while using this method.
   *
   * @param message is the data to write to the file
   * @author Greg Esparza
   */
  public void writeln(String message) throws CouldNotCreateFileException
  {
    Assert.expect(message != null);
    //Swee Yee Wong - XCR-3356	Assert when trying to log file
    //Assert.expect(_os != null);
    if(_os == null)
    {
      open();
    }
    
    _os.println(message);
  }

  /**
   * This method is used to write the messages followed by a newline to the file.
   * The file be initially opened and remain open while using this method.
   *
   * @author Bill Darbie
   */
  public void writeln(List<String> messages) throws CouldNotCreateFileException
  {
    Assert.expect(messages != null);
    //Swee Yee Wong - XCR-3356	Assert when trying to log file
    //Assert.expect(_os != null);
    if(_os == null)
    {
      open();
    }
    
    _os.println(messages);
  }


  /**
   * This method is used to write a newline to the file.
   * The file be initially opened and remain open while using this method.
   *
   * * @author George A. David
   */
  public void writeln() throws CouldNotCreateFileException
  {
    //Swee Yee Wong - XCR-3356	Assert when trying to log file
    //Assert.expect(_os != null);
    if(_os == null)
    {
      open();
    }
    
    _os.println();
  }

  /**
   * This method is used to write the exception to the file.
   * The file be initially opened and remain open while using this method.
   *
   * @param exception is the data to write to the file
   * @author Greg Esparza
   */
  public void write(Exception exception)
  {
    Assert.expect(exception != null);
    exception.printStackTrace(_os);
  }

  /**
   * This method is used to write the exception followed by a newline to the file.
   * The file be initially opened and remain open while using this method.
   *
   * @param exception is the data to write to the file
   * @author Greg Esparza
   */
  public void writeln(Exception exception)
  {
    Assert.expect(exception != null);
    exception.printStackTrace(_os);
    _os.println();
  }

  /**
   * This method is used for a quick append of one line in the file.
   * Specifically, the file is opened, the data is written, then the file is closed.
   * The file should not be opened prior to calling this method.
   *
   * @param message is the data to write to the file
   * @author Greg Esparza
   */
  public void append(String message) throws CouldNotCreateFileException
  {
    Assert.expect(message != null);
    _append = true;
    open();
    writeln(message);
    close();
  }

  /**
   * This method is used for a quick append of one line in the file.
   * Specifically, the file is opened, the data is written, then the file is closed.
   * The file should not be opened prior to calling this method.
   *
   * @param messages is the list of data to write to the file
   * @author Greg Esparza
   */
  public void append(List<String> messages) throws CouldNotCreateFileException
  {
    Assert.expect(messages != null);
    _append = true;
    open();
    for (String message : messages)
      writeln(message);
    close();
  }

  /**
   * This method is used for a quick append of one line in the file.
   * Specifically, the file is opened, the data is written, then the file is closed.
   * The file should not be opened prior to calling this method.
   *
   * @param exception is the data to write to the file
   * @author Greg Esparza
   */
  public void append(Exception exception) throws CouldNotCreateFileException
  {
    Assert.expect(exception != null);
    _append = true;
    open();
    writeln(exception);
    close();
  }
}
