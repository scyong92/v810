package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * @author John Dutton
 */
public class Test_DirectoryLogger extends UnitTest
{
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_DirectoryLogger());
  }

  /**
   * @author John Dutton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String logFilesDirectory = getTestDataDir() + File.separator + getClass().getName();
    String logFileNamePrefix = "DirLogTest";
    String logFileNameExtension = ".log";
    int maxNosOfLogFiles = 4;

    try
    {
      // Before starting, if log file directory has been left from earlier test runs,
      // delete it and its contents.  This means the first test will indirectly test
      // that the logger works when the log directory doesn't already exist.  (The case
      // for when the directory already exists will happen in subsequent tests.)
      if (FileUtil.exists(logFilesDirectory))
      {
        FileUtil.deleteDirectoryContents(logFilesDirectory);
        FileUtil.delete(logFilesDirectory);
      }

      // Test 1 -- see that log files are being created and contain correct content.

      DirectoryLogger dirLog =
          new DirectoryLogger(logFilesDirectory, logFileNamePrefix,
                              logFileNameExtension, maxNosOfLogFiles);

      // Generate log files
      String newLine = System.getProperty("line.separator");
      String logString = "Here is some nice data from my confirmation test." + newLine +
                         "Here is the second line for my confirmation test." + newLine +
                         "Here is the third line.";
      int logStringSize = logString.length();
      String additionalHeaderInfo = "";
      int logNos = 3;
      for (int i = 0; i < logNos; i++)
      {
        dirLog.log(logString);
        // Sleep so log calls won't generate same filename, i.e. same time stamp.
        // (for some reason sleeping 1-5 milliseconds didn't work -- use 50 to be sure).
        Thread.sleep(50);
      }

      // Check number of log files and their content
      checkLogFiles(logString, additionalHeaderInfo, logFilesDirectory, logFileNamePrefix,
                    logFileNameExtension, maxNosOfLogFiles, logNos);

      // Cleanup
      FileUtil.deleteDirectoryContents(logFilesDirectory);

      // Test 2 -- test maximum nos of log files
      DirectoryLogger dirLog2 =
          new DirectoryLogger(logFilesDirectory, logFileNamePrefix,
                              logFileNameExtension, maxNosOfLogFiles);

      // Request logging more than allowable log files
      logNos = maxNosOfLogFiles + 2;
      for (int i = 0; i < logNos; i++)
      {
        dirLog2.log(logString);
        Thread.sleep(50);
      }

      checkLogFiles(logString, additionalHeaderInfo, logFilesDirectory, logFileNamePrefix,
                    logFileNameExtension, maxNosOfLogFiles, logNos);

      // Cleanup
      FileUtil.deleteDirectoryContents(logFilesDirectory);

      // Test 3 -- test logIfTimedOut and isLoggingNeeded methods.
      DirectoryLogger dirLog3 =
           new DirectoryLogger(logFilesDirectory, logFileNamePrefix,
                               logFileNameExtension, maxNosOfLogFiles);

      // log multiple times within threshold
      logNos = 3;
      long elapsedTimeThresholdMillis = 2000;
      for (int i = 0; i < logNos; i++)
      {
        dirLog3.logIfTimedOut(logString, elapsedTimeThresholdMillis);
      }

      // Should only find 1 log file in directory
      logNos = 1;
      checkLogFiles(logString, additionalHeaderInfo, logFilesDirectory, logFileNamePrefix,
                    logFileNameExtension, maxNosOfLogFiles, logNos);

      // Cleanup
      FileUtil.deleteDirectoryContents(logFilesDirectory);

      // Log multiple times outside of threshold, i.e. time out occurs, so
      // a log file should be created each time logIfTimedOut is called.
      logNos = 3;
      elapsedTimeThresholdMillis = 5;
      for (int i = 0; i < logNos; i++)
      {
        Expect.expect(dirLog3.isLoggingNeeded(elapsedTimeThresholdMillis) == true, "isLoggingNeeded incorrectly returned false");
        dirLog3.logIfTimedOut(logString, elapsedTimeThresholdMillis);
        Thread.sleep(50);
      }

      checkLogFiles(logString, additionalHeaderInfo, logFilesDirectory, logFileNamePrefix,
                    logFileNameExtension, maxNosOfLogFiles, logNos);

      // Cleanup
      FileUtil.deleteDirectoryContents(logFilesDirectory);

      // Do the exact same scenario, but more times to see that maxNosOfLogFiles happens.
      logNos = maxNosOfLogFiles + 2;
      for (int i = 0; i < logNos; i++)
      {
        dirLog3.logIfTimedOut(logString, elapsedTimeThresholdMillis);
        Thread.sleep(50);
      }

      checkLogFiles(logString, additionalHeaderInfo, logFilesDirectory, logFileNamePrefix,
                    logFileNameExtension, maxNosOfLogFiles, logNos);

      // Cleanup
      FileUtil.deleteDirectoryContents(logFilesDirectory);


      // Test 4 -- test that log files newer than system clock are ignored
      DirectoryLogger dirLog4 =
          new DirectoryLogger (logFilesDirectory, logFileNamePrefix,
                               logFileNameExtension, maxNosOfLogFiles);

      // Create bogus log file
      String fileName30YearsHence = logFilesDirectory + File.separator +
                             logFileNamePrefix + "2035.01.15.14.30.44" + logFileNameExtension;

      File bogus1 = new File(fileName30YearsHence);
      bogus1.createNewFile();

      // Now see that logging happens anyway
      logNos = 1;
      for (int i = 0; i < logNos; i++)
      {
        dirLog4.logIfTimedOut(logString,0);
        Thread.sleep(50);
      }

      // delete bogus file before checkLogFiles is called so it won't fail because of extra file
      bogus1.delete();
      checkLogFiles(logString, additionalHeaderInfo, logFilesDirectory, logFileNamePrefix,
                    logFileNameExtension, maxNosOfLogFiles, logNos);

      // Cleanup
      FileUtil.deleteDirectoryContents(logFilesDirectory);

      // Test 5 -- test setAdditionalHeaderInfo
      DirectoryLogger dirLog5 =
          new DirectoryLogger (logFilesDirectory, logFileNamePrefix,
                               logFileNameExtension, maxNosOfLogFiles);

      logNos = 1;
      additionalHeaderInfo = "a nice serial number 42";
      dirLog5.setAdditionalHeaderInfo(additionalHeaderInfo);
      for (int i = 0; i < logNos; i++)
      {
        dirLog5.logIfTimedOut(logString,0);
        Thread.sleep(50);
      }

      checkLogFiles(logString, additionalHeaderInfo, logFilesDirectory, logFileNamePrefix,
                    logFileNameExtension, maxNosOfLogFiles, logNos);

      // Cleanup
      additionalHeaderInfo = "";
      FileUtil.deleteDirectoryContents(logFilesDirectory);


      // Test 6 -- check proper parameter handling
      DirectoryLogger dirLog6;
      try
      {
        dirLog6 =
            new DirectoryLogger (null, logFileNamePrefix, logFileNameExtension, maxNosOfLogFiles);

        Expect.expect(false, "DirectoryLogger constructor allows null log file directory");
      }
      catch (AssertException ae)
      {
        // do nothing
      }
      try
      {
        dirLog6 =
            new DirectoryLogger (logFilesDirectory, null, logFileNameExtension, maxNosOfLogFiles);

        Expect.expect(false, "DirectoryLogger constructor allows null log file name prefix");
      }
      catch (AssertException ae)
      {
        // do nothing
      }
      try
      {
        dirLog6 =
            new DirectoryLogger (logFilesDirectory, logFileNamePrefix, null, maxNosOfLogFiles);

        Expect.expect(false, "DirectoryLogger constructor allows null log file name extension");
      }
      catch (AssertException ae)
      {
        // do nothing
      }
      try
      {
        dirLog6 =
            new DirectoryLogger (logFilesDirectory, "foo?bar", logFileNameExtension, maxNosOfLogFiles);
        dirLog6.log("foobar");
        Expect.expect(false, "DirectoryLogger constructor allows bad file name character");
      }
      catch (IOException ioe)
      {
        // do nothing
      }
      try
      {
        dirLog6 =
            new DirectoryLogger (logFilesDirectory, logFileNamePrefix, ".lo<g", maxNosOfLogFiles);
        dirLog6.log("foobar");
        Expect.expect(false, "DirectoryLogger constrnuctor allows bad file name extension character");
      }
      catch (IOException ioe)
      {
        // do nothing
      }
    }
    catch (Exception e)
    {
      Expect.expect(false, "Directory logger test threw unexpected exception");
    }
    finally
    {
      if (FileUtil.exists(logFilesDirectory))
      {
        try
        {
          FileUtil.deleteDirectoryContents(logFilesDirectory);
          FileUtil.delete(logFilesDirectory);
        }
        catch (CouldNotDeleteFileException ex)
        {
          Expect.expect(false, "Directory logger could not delete test directory as part of cleanup");
        }
      }
    }
  }

  private void checkLogFiles(String logString,
                             String additionalHeaderInfo,
                             String logFilesDirectory,
                             String logFileNamePrefix,
                             String logFileNameExtension,
                             int maxNosOfLogFiles,
                             int logNos) throws IOException
  {
    Collection<String> files =
      FileUtil.listAllFilesFullPathInDirectory(logFilesDirectory);

    // Check number of log files
    int expectedNosOfLogFiles;
    if (logNos < maxNosOfLogFiles)
      expectedNosOfLogFiles = logNos;
    else
      expectedNosOfLogFiles = maxNosOfLogFiles;
    Expect.expect(files.size() == expectedNosOfLogFiles, "directory logger directory contains wrong number of log files");

    // Check content of each log file
    for (Iterator<String> iter = files.iterator(); iter.hasNext(); )
    {
      String fileFullPathName = iter.next();
      FileReader fileReader = new FileReader(fileFullPathName);
      LineNumberReader lineReader = new LineNumberReader(fileReader);

      // Check header.
      String nextLine = lineReader.readLine();   // Reader header line
      Expect.expect(nextLine != null, "directory log file is empty");
      if (!additionalHeaderInfo.equals(""))
        Expect.expect(nextLine.endsWith(additionalHeaderInfo), "directory log file file doesn't have additional header info");

      // Check logString.
      char[] buffer = new char[1024];
      Assert.expect(1024 > logString.length(), "INTERNAL ERROR: directory logger test log string too long");
      int readResult = lineReader.read(buffer);
      Expect.expect(readResult > 0, "directory log file has no log string");
      Expect.expect(logString.equals(new String(buffer, 0, readResult)), "directory log file contains wrong log string");
      lineReader.close();
    }
  }

}
