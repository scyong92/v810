package com.axi.util;

/**
 * This enumeration contains the byte sizes of Java data types.
 *
 * @author Roy Williams
 */
public class JavaTypeSizeEnum extends Enum
{
  private static int _index = -1;

  public static final JavaTypeSizeEnum BOOL   = new JavaTypeSizeEnum(++_index, Byte.SIZE / 8);
  public static final JavaTypeSizeEnum BYTE   = new JavaTypeSizeEnum(++_index, Byte.SIZE / 8);
  public static final JavaTypeSizeEnum CHAR   = new JavaTypeSizeEnum(++_index, Character.SIZE / 8);
  public static final JavaTypeSizeEnum SHORT  = new JavaTypeSizeEnum(++_index, Short.SIZE / 8);
  public static final JavaTypeSizeEnum INT    = new JavaTypeSizeEnum(++_index, Integer.SIZE / 8);
  public static final JavaTypeSizeEnum LONG   = new JavaTypeSizeEnum(++_index, Long.SIZE / 8);
  public static final JavaTypeSizeEnum FLOAT  = new JavaTypeSizeEnum(++_index, Float.SIZE / 8);
  public static final JavaTypeSizeEnum DOUBLE = new JavaTypeSizeEnum(++_index, Double.SIZE / 8);

  private int _size;

  /**
   * @author Roy Williams
   */
  private JavaTypeSizeEnum(int id, int size)
  {
    super(id);
    Assert.expect(size > 0);
    _size = size;
  }

  /**
   * @author Roy Williams
   */
  public int getSizeInBytes()
  {
    return _size;
  }

  static public void main(String args[])
  {
    System.out.println("Size of BYTE: " + BOOL.getSizeInBytes());
  }
}

