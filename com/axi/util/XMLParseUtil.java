package com.axi.util;

import java.util.*;

/**
 * This class provides methods that convert from Java standard class names to names that are more general.
 *
 * @author Bill Darbie
 */
class XMLParseUtil
{
  public static final String ELEMENT = "elem";
  public static final String KEY = "key";
  public static final String VALUE = "value";
  public static final String ID = "id";
  public static final String TYPE = "type";
  public static final String NUMELEMENTS = "numElements";

  private static Map<String, String> _classNameToXMLName = new HashMap<String, String>();
  private static Map<String, String> _xmlNameToClassName = new HashMap<String, String>();
  private static int _maxArrayDepth = 10;

  /**
   * @author Bill Darbie
   */
  static
  {
    _classNameToXMLName.put("java.lang.String", "String");
    _classNameToXMLName.put("java.lang.Boolean", "Boolean");
    _classNameToXMLName.put("java.lang.Integer", "Integer");
    _classNameToXMLName.put("java.lang.Float", "Float");
    _classNameToXMLName.put("java.lang.Double", "Double");

    String arrayDepth = "";
    String array = "";
    for(int i = 0; i < _maxArrayDepth; ++i)
    {
      arrayDepth = arrayDepth + "[";
      array = array + "[]";
      _classNameToXMLName.put(arrayDepth + "Ljava.lang.Object;", "Object" + array);
      _classNameToXMLName.put(arrayDepth + "Ljava.lang.String;", "String" + array);
      _classNameToXMLName.put(arrayDepth + "Ljava.lang.Boolean;", "Boolean" + array);
      _classNameToXMLName.put(arrayDepth + "Ljava.lang.Integer;", "Integer" + array);
      _classNameToXMLName.put(arrayDepth + "Ljava.lang.Float;", "Float" + array);
      _classNameToXMLName.put(arrayDepth + "Ljava.lang.Double;", "Double" + array);

      _classNameToXMLName.put(arrayDepth + "Z", "boolean" + array);
      _classNameToXMLName.put(arrayDepth + "I", "int" + array);
      _classNameToXMLName.put(arrayDepth + "F", "float" + array);
      _classNameToXMLName.put(arrayDepth + "D", "double" + array);
    }

    Iterator<String> it = _classNameToXMLName.keySet().iterator();
    while(it.hasNext())
    {
      String className = it.next();
      String xmlName = _classNameToXMLName.get(className);
      _xmlNameToClassName.put(xmlName, className);
    }
  }

  /**
   * @author Bill Darbie
   */
  static String getXMLNameFromClassName(String className)
  {
    String xmlName = _classNameToXMLName.get(className);
    if (xmlName == null)
      xmlName = className;

    return xmlName;
  }

  /**
   * @author Bill Darbie
   */
  static String getClassNameFromXMLName(String xmlName)
  {
    String className = (String)_xmlNameToClassName.get(xmlName);
    if (className == null)
      className = xmlName;

    return className;
  }

//  /**
//   * @author Bill Darbie
//   */
//  static String getXMLNameFromFieldName(String fieldName)
//  {
//    String xmlName = fieldName;
//    if (fieldName.charAt(0) == '_')
//      xmlName = fieldName.substring(1, fieldName.length());
//
//    return xmlName;
//  }
//
//  /**
//   * @author Bill Darbie
//   */
//  static String getFieldNameFromXMLName(String xmlName)
//  {
//    return "_" + xmlName;
//  }

}
