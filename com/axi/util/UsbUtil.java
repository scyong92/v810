package com.axi.util;

import java.util.*;

/**
 * This class uses JNI to send and recieve messages
 * via a USB connection.
 * @author George A. David
 */
public class UsbUtil
{
  private static Map<UsbDriverTypeEnum, UsbUtil> _usbDriveTypeEnumToInstanceMap = new HashMap<UsbDriverTypeEnum, UsbUtil>();

  private UsbDriverTypeEnum _usbDriveTypeEnum = null;

  /**
   * @author George A. David
   */
  public static synchronized UsbUtil getInstance(UsbDriverTypeEnum usbDriveTypeEnum)
  {
    Assert.expect(usbDriveTypeEnum != null);

    UsbUtil instance = null;

    if (_usbDriveTypeEnumToInstanceMap.containsKey(usbDriveTypeEnum))
    {
      instance = _usbDriveTypeEnumToInstanceMap.get(usbDriveTypeEnum);
    }
    else
    {
      instance = new UsbUtil(usbDriveTypeEnum);
      Assert.expect(_usbDriveTypeEnumToInstanceMap.put(usbDriveTypeEnum, instance) == null);
    }

    Assert.expect(instance != null);
    return instance;
  }

  /**
   * @author George A. David
   */
  private UsbUtil(UsbDriverTypeEnum usbDriveTypeEnum)
  {
    Assert.expect(usbDriveTypeEnum != null);

    _usbDriveTypeEnum = usbDriveTypeEnum;
  }

  /**
   * @author George A. David
   */
  public void sendMessage(String deviceDescription, String message) throws CommunicationException
  {
    Assert.expect(deviceDescription != null);
    Assert.expect(message != null);

    nativeSendMessage(_usbDriveTypeEnum.getId(), deviceDescription, message);
  }

  /**
   * @author George A. David
   */
  public List<String> getReplies(String deviceDescription) throws CommunicationException
  {
    Assert.expect(deviceDescription != null);

    return nativeGetReplies(_usbDriveTypeEnum.getId(), deviceDescription);
  }

  /**
   * @author George A. David
   */
  public List<String> sendMessageAndGetReplies(String deviceDescription, String message) throws CommunicationException
  {
    Assert.expect(deviceDescription != null);
    Assert.expect(message != null);

    return nativeSendMessageAndGetReplies(_usbDriveTypeEnum.getId(), deviceDescription, message);
  }

  /**
   * @author George A. David
   */
  public void disconnect(String deviceDescription) throws CommunicationException
  {
    Assert.expect(deviceDescription != null);

    nativeDisconnect(_usbDriveTypeEnum.getId(), deviceDescription);
  }

  /**
   * @author George A. David
   */
  public void connect(String deviceDescription) throws CommunicationException
  {
    Assert.expect(deviceDescription != null);

    nativeConnect(_usbDriveTypeEnum.getId(), deviceDescription);
  }

  /**
   * @author George A. David
   */
  public void finalize()
  {
    nativeDestructor();
  }

  private native void nativeSendMessage(int usbDriveTypeEnum, String deviceDescription, String message);
  private native List<String> nativeGetReplies(int usbDriveTypeEnum, String deviceDescription);
  private native List<String> nativeSendMessageAndGetReplies(int usbDriveTypeEnum, String deviceDescription, String message);
  private native void nativeDisconnect(int usbDriveTypeEnum, String deviceDescription);
  private native void nativeConnect(int usbDriveTypeEnum, String deviceDescription);
  private native void nativeDestructor();
}
