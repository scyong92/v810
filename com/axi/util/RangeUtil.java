package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * This class helps you organize ranges. It allows you to add/subtract ranges
 * of integers and it will modify your existing ranges as necessary. For example,
 * if suppose you have the following ranges:
 *  1-3  4-7  9 - 10
 * if you subtract range 3-5, you will end up with the following ranges:
 * 1-2 6-7 9-10
 *then if we add 5 - 11, we will end up with
 * 1-2 5-11
 *
 * @author George A. David
 */
public class RangeUtil implements Serializable
{
  private List<Pair<Integer, Integer>> _ranges = new LinkedList<Pair<Integer, Integer>>();

  /**
   * @author George A. David
   */
  public RangeUtil()
  {
    // do nothing
  }

  /**
   * @author George A. David
   */
  public RangeUtil(RangeUtil rangeUtil)
  {
    add(rangeUtil);
  }

  /**
   * @author George A. David
   */
  public RangeUtil(int beginRangeValue, int endRangeValue)
  {
    Assert.expect(beginRangeValue <= endRangeValue);

    _ranges.add(new Pair<Integer, Integer>(beginRangeValue, endRangeValue));
  }

  /**
   * @author George A. David
   */
  public void add(int value)
  {
    add(value, value);
  }

  /**
   * @author George A. David
   */
  public void add(RangeUtil rangeUtil)
  {
    Assert.expect(rangeUtil != null);

    add(rangeUtil.getRanges());
  }

  /**
   * @author George A. David
   */
  public void add(List<Pair<Integer, Integer>> ranges)
  {
    Assert.expect(ranges != null);

    for(Pair<Integer, Integer> pair : ranges)
      add(pair.getFirst(), pair.getSecond());
  }

  /**
   * @author George A. David
   */
  public void add(int beginRangeValue, int endRangeValue)
  {
    Assert.expect(beginRangeValue <= endRangeValue);

    if(_ranges.isEmpty())
    {
      _ranges.add(new Pair<Integer, Integer>(beginRangeValue, endRangeValue));
      return;
    }

    for(Pair<Integer, Integer> pair : new LinkedList<Pair<Integer, Integer>>(_ranges))
    {
      if(beginRangeValue <= pair.getFirst())
      {
        if(endRangeValue >= pair.getSecond())
        {
          // the new range spans accross this old range, we don't need the old range
          int index = _ranges.indexOf(pair);
          if(index == _ranges.size() - 1)
          {
            pair.setFirst(beginRangeValue);
            pair.setSecond(endRangeValue);
          }
          else
            _ranges.remove(index);
        }
        else if(endRangeValue < pair.getFirst())
        {
          // let's check if it is just off by one
          if(endRangeValue + 1 == pair.getFirst())
          {
            // the new range intersects this range, let's modify the current range
            pair.setFirst(beginRangeValue);
            break;
          }
          else
          {
            // this is a completely new range that does not exist
            // all we need to do is to add it to the list and we are done.
            int index = _ranges.indexOf(pair);
            Assert.expect(index >= 0);
            _ranges.add(index, new Pair<Integer, Integer>(beginRangeValue, endRangeValue));
            break;
          }
        }
        else if(endRangeValue > pair.getFirst())
        {
          // the new range intersects this range, let's modify the current range
          pair.setFirst(beginRangeValue);
          break;
        }
        else
          Assert.expect(false);
      }
      else if(endRangeValue >= pair.getSecond())
      {
        if(beginRangeValue > pair.getSecond())
        {
          // let's check if it is off by one
          if(pair.getSecond() + 1 == beginRangeValue)
          {
            // these ranges intersect, if we are at the end of the list,
            // modify the current pair with a new end end range, otherwise
            //modify the beginRangeValue, and have the next loop
            //process this
            int index = _ranges.indexOf(pair);
            Assert.expect(index >= 0);
            if(index == _ranges.size() - 1)
            {
              pair.setSecond(endRangeValue);
              break;
            }
            else
            {
              beginRangeValue = pair.getFirst();
              _ranges.remove(index);
            }
          }
          else
          {
            // it does not affect this range, if we are at the end of the list
            // let's add it in, otherwise, let's move on
            if (_ranges.indexOf(pair) == _ranges.size() - 1)
            {
              _ranges.add(new Pair<Integer, Integer>(beginRangeValue, endRangeValue));
              break;
            }
          }
        }
        else if (beginRangeValue > pair.getFirst())
        {
          // these ranges intersect, if we are at the end of the list,
          // modify the current pair with a new end end range, otherwise
          //modify the beginRangeValue, and have the next loop
          //process this
          int index = _ranges.indexOf(pair);
          Assert.expect(index >= 0);
          if(index == _ranges.size() - 1)
          {
            pair.setSecond(endRangeValue);
            break;
          }
          else
          {
            beginRangeValue = pair.getFirst();
            _ranges.remove(index);
          }
        }
        else
          Assert.expect(false);
      }
      else if(beginRangeValue > pair.getFirst() &&
              endRangeValue < pair.getSecond())
      {
        // this range is already completely contained by another.
        // no need to do anything more
        break;
      }
      else
        Assert.expect(false);
    }
  }

  /**
   * @author George A. David
   */
  public void subtract(int value)
  {
    subtract(value, value);
  }

  /**
   * @author George A. David
   */
  public void subtract(RangeUtil rangeUtil)
  {
    for(Pair<Integer, Integer> pair : rangeUtil.getRanges())
      subtract(pair.getFirst(), pair.getSecond());
  }

  /**
   * @author George A. David
   */
  public void subtract(int beginRangeValue, int endRangeValue)
  {
    Assert.expect(beginRangeValue <= endRangeValue);

    if(_ranges.isEmpty())
      return;

    List<Pair<Integer, Integer>> newRanges = new LinkedList<Pair<Integer, Integer>>();
    for(Pair<Integer, Integer> pair : _ranges)
    {
      if(beginRangeValue <= pair.getFirst())
      {
        if(endRangeValue >= pair.getSecond())
        {
          // do nothing, this pair gets completely removed
        }
        else if(endRangeValue < pair.getFirst())
        {
          // this does not affect our pair. let's add it to the list
          newRanges.add(pair);
        }
        else if(endRangeValue < pair.getSecond())
        {
          Pair<Integer, Integer> newPair = new Pair<Integer, Integer>(endRangeValue + 1, pair.getSecond());
          newRanges.add(newPair);
        }
        else
          Assert.expect(false);

      }
      else if(endRangeValue >= pair.getSecond())
      {
        if(beginRangeValue > pair.getSecond())
        {
          // this does not affect our pair. let's add it to the list
          newRanges.add(pair);
        }
        else if(beginRangeValue > pair.getFirst())
        {
          Pair<Integer, Integer> newPair = new Pair<Integer, Integer>(pair.getFirst(), beginRangeValue - 1);
          newRanges.add(newPair);
        }
        else
          Assert.expect(false);
      }
      else if(beginRangeValue > pair.getFirst() &&
              endRangeValue < pair.getSecond())
      {
        // the pair needs to be split into two
        Pair<Integer, Integer> newPair = new Pair<Integer, Integer>(pair.getFirst(), beginRangeValue - 1);
        newRanges.add(newPair);

        newPair = new Pair<Integer, Integer>(endRangeValue + 1, pair.getSecond());
        newRanges.add(newPair);
      }
      else
        Assert.expect(false);
    }

    _ranges.clear();
    _ranges.addAll(newRanges);
  }

  /**
   * @author George A. David
   */
  public boolean intersects(int beginRangeValue, int endRangeValue)
  {
    boolean intersects = false;
    for (Pair<Integer, Integer> range : _ranges)
    {
      int begin = range.getFirst();
      int end = range.getSecond();

      if ((begin >= beginRangeValue && begin <= endRangeValue) ||
          (end >= beginRangeValue && end <= endRangeValue))
      {
        intersects = true;
      }
    }

    return intersects;
  }

  /**
   * @author George A. David
   */
  public boolean intersects(Pair<Integer, Integer> range)
  {
    Assert.expect(range != null);

    return intersects(range.getFirst(), range.getSecond());
  }

  /**
   * @author George A. David
   */
  public void clear()
  {
    Assert.expect(_ranges != null);
    _ranges.clear();
  }

  /**
   * @author George A. David
   */
  public List<Pair<Integer, Integer>> getRanges()
  {
    Assert.expect(_ranges != null);
    return new ArrayList<Pair<Integer, Integer>>(_ranges);
  }

  /**
   * @author George A. David
   */
  public boolean hasRanges()
  {
    Assert.expect(_ranges != null);
    return _ranges.isEmpty() == false;
  }

  /**
   * @author Roy Williams
   */
  public static int getMaxInRangeList(List<Pair<Integer, Integer>> ranges)
  {
    int maxStepSizeInNanoMeters = 0;
    int tempMaxX = -1;
    for (Pair<Integer, Integer> range : ranges)
    {
      tempMaxX = range.getSecond();
      if (maxStepSizeInNanoMeters < tempMaxX)
        maxStepSizeInNanoMeters = tempMaxX;
    }
    return maxStepSizeInNanoMeters;
  }

  /**
   * @author Roy Williams
   */
  public static boolean rangeContains(List<Pair<Integer, Integer>> ranges, int value)
  {
    for (Pair<Integer, Integer> range : ranges)
    {
      if (value >= range.getFirst() && value <= range.getSecond())
        return true;
    }
    return false;
  }

  /**
   * @author Roy Williams
   */
  public static int fuzzyMatch(
      List<Pair<Integer, Integer>> rangeList1,
      List<Pair<Integer, Integer>> rangeList2,
      double fuzzyMatchPercentage)
  {
    int rangeMax1 = getMaxInRangeList(rangeList1);
    int rangeMax2 = getMaxInRangeList(rangeList2);
    if (rangeMax1 > rangeMax2)
    {
      if (rangeContains(rangeList1, rangeMax2) && rangeMax2 >= (1 - fuzzyMatchPercentage) * rangeMax1)
        return rangeMax2;
    }
    else if (rangeMax1 < rangeMax2)
    {
      if (rangeContains(rangeList2, rangeMax1) && rangeMax1 >= (1 - fuzzyMatchPercentage) * rangeMax2)
        return rangeMax1;
    }
    else // equals
    {
      return rangeMax1;
    }
    return -1;   // no intersection
  }

  /**
   * The goal is to create an intersecting range without affecting either of the
   * two ranges provided as input.  DO NOT MODIFY THEM.
   *
   * @author Roy Williams
   */
  public static List<Pair<Integer, Integer>> getIntersectingRanges(
      List<Pair<Integer, Integer>> ranges1,
      List<Pair<Integer, Integer>> ranges2)
  {
    Assert.expect(ranges1 != null);
    Assert.expect(ranges2 != null);

    List<Pair<Integer, Integer>> proposedRanges  = new ArrayList<Pair<Integer, Integer>>();
    for (Pair<Integer, Integer> range : ranges1)
    {
      proposedRanges.add(new Pair<Integer, Integer>(range.getFirst(), range.getSecond()));
    }

    List<Pair<Integer, Integer>> confirmedRanges  = new ArrayList<Pair<Integer, Integer>>();
    int proposedRangeBegin = 0;
    int proposedRangeEnd   = 0;

    int begin = -1;
    int end   = -1;

    // At this point it is our responsibility to cull the "good" list down.
    for (Pair<Integer, Integer> proposedRange : proposedRanges)
    {
      proposedRangeBegin = proposedRange.getFirst();
      proposedRangeEnd   = proposedRange.getSecond();

      // We'd better have at least one of these candidateStepSizeRanges agree
      // to each goodRange.  Otherwise, the goodRange is invalid and should be
      // removed from the confirmedRanges.
      for (Pair<Integer, Integer> range : ranges2)
      {
        begin = range.getFirst();
        end   = range.getSecond();

        if ((begin >= proposedRangeBegin && begin <= proposedRangeEnd) ||
            (end   >= proposedRangeBegin && end   <= proposedRangeEnd))
        {
          proposedRangeBegin = Math.max(proposedRangeBegin, begin);
          proposedRangeEnd   = Math.min(proposedRangeEnd, end);
          proposedRange.setFirst(proposedRangeBegin);
          proposedRange.setSecond(proposedRangeEnd);
          confirmedRanges.add(proposedRange);
        }
      }
    }
    return confirmedRanges;
  }

  /**
   * @author George A. David
   */
  public void intersect(RangeUtil rangeUtil)
  {
    _ranges = intersect(rangeUtil, this).getRanges();
  }

  /**
   * @author George A. David
   */
  public static RangeUtil intersect(RangeUtil rangeUtil1, RangeUtil rangeUtil2)
  {
    Assert.expect(rangeUtil1 != null);
    Assert.expect(rangeUtil2 != null);

    RangeUtil intersectingRangeUtil = new RangeUtil();
    intersectingRangeUtil.add(getIntersectingRanges(rangeUtil1.getRanges(), rangeUtil2.getRanges()));

    return intersectingRangeUtil;
  }

  /**
   * @author George A. David
   */
  private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
  {
    is.defaultReadObject();
    clear();
    int numRanges = is.readInt();
    for(int i = 0; i < numRanges; i++)
    {
      add(is.readInt(), is.readInt());
    }
  }

  /**
   * @author George A. David
   */
  private void writeObject(ObjectOutputStream os) throws IOException
  {
    os.defaultWriteObject();

    os.writeInt(_ranges.size());
    for(Pair<Integer, Integer> pair : _ranges)
    {
      os.writeInt(pair.getFirst());
      os.writeInt(pair.getSecond());
    }
  }

  /**
   * Adding neighbouring points at n distance from a center point. 
   * Points adding from right to left(cx+n to cx-n), from top to bottom (cy+n to cy-n).
   * 
   * o -> center (cx,cy) , # -> neighbours surrounding
   * Starting point (x,y) at top left corner
   * 
   * n=1,   
   *     #(x,y)     # #      # # #      # # #      # # #      # # #      # # #      # # #
   *   o      ->    o    ->    o    ->    o #  ->  # o #  ->  # o #  ->  # o #  ->  # o #
   *                                                              #        # #      # # #
   *                                  
   *                                  
   * n=2,                    n=3,
   *    # # # # # <-(x,y)      # # # # # # # <-(x,y)
   *    # # # # #              # # # # # # #
   *    # # o # #              # # # # # # #
   *    # # # # #              # # # o # # #
   *    # # # # #              # # # # # # #
   *                           # # # # # # #
   *                           # # # # # # #
   * 
   * @param queue
   * @param cx center x
   * @param cy center y
   * @param n pixel distance
   * @param x current x = cx+n
   * @param y current y = cy+n
   * 
   * @author Siew Yeng
   */
  public static void addNeighbouringPoints(Collection<java.awt.Point> queue, int cx, int cy, int n, int x, int y)
  {
    Assert.expect(queue != null);
    
    queue.add(new java.awt.Point(x, y));
    
    if(x == cx-n && y == cy-n)
      return;
    
    if(x == cx-n)
    {
      y--;
      x = cx+n;
    }
    else
      x--;
    
    //skip center point - only add neighbouring points
    if(x == cx && y == cy)
      x--;

    addNeighbouringPoints(queue, cx, cy, n, x, y);
  }
}
