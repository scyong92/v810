package com.axi.util;

import java.io.*;

/**
 * Provides a thread to read from the executing process and write it to the
 * PrintWriter for UnitTestWithSupportingProcesses.
 *
 * @author Roy Williams
 */
class StreamHandler implements Runnable
{
  private LaunchedProcess _process = null;
  private PrintWriter _writer = null;
  private BufferedReader _reader = null;
  private Thread _thread = null;
  private long _lastActiveTraffic = 0;

  /**
   * @author Roy Williams
   */
  public StreamHandler(LaunchedProcess process,
                       InputStream inputStream,
                       PrintWriter writer)
  {
    Assert.expect(process != null);
    Assert.expect(inputStream != null);
    // writer == null is okay => sending output to /dev/null

    _process = process;
    _reader = new BufferedReader(new InputStreamReader(inputStream));
    _writer = writer;
    _thread = new Thread(null, this, StreamHandler.class.getName());
    _thread.start();
  }

  /**
   * Pretty simple to suck and chuck characters read from the input to the
   * output.  When anything happens to either the read or write stream a call
   * will be made to the parent UnitTest to tell it that this process has been
   * terminated.
   *
   * @author Roy Williams
   */
  public void run()
  {
    try
    {
      String line = null;
      while ((line = _reader.readLine()) != null)
      {
        _lastActiveTraffic = System.currentTimeMillis();
        if (_writer != null) // sending output to /dev/null
          _writer.println(line);
      }
    }
    catch (Exception e)
    {}
    finally
    {
      _process.closingStreamHandler(this);
      close();
    }
  }

  /**
   * @return the number of bytes transfered since the last inquiry of traffic.
   *
   * @author Roy Williams
   */
  public synchronized long lastActiveTraffic()
  {
    return _lastActiveTraffic;
  }

  /**
   * @author Roy Williams
   */
  public synchronized void close()
  {
    try
    {
      if (_reader != null)
        _reader.close();
    }
    catch (IOException ioe)
    {
      // do nothing
    }

    try
    {
      if (_writer != null)
      {
        _writer.flush();
        _writer.close();
      }
    }
    catch (Exception e)
    {
      // do nothing
    }

    _reader = null;
    _writer = null;
  }
}
