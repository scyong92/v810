package com.axi.util;

import java.util.concurrent.*;

/**
 * This class is used by ExecuteParallelThreadTasks to allow you to run many tasks in parallel
 * and wait until they all complete.
 *
 * @see ExecuteParallelThreadTasks
 * @author Bill Darbie
 */
public abstract class ThreadTask<T> implements Callable<T>
{
  private String _name;
  private ExecuteParallelThreadTasks _executeParallelThreadTasks;
  T _result;

  /**
   * @author Bill Darbie
   */
  public ThreadTask(String name)
  {
    Assert.expect(name != null);
    _name = name;
  }

  /**
   * @author Bill Darbie
   */
  public String getName()
  {
    Assert.expect(_name != null);
    return _name;
  }

  /**
   * @author Bill Darbie
   */
  void setExecuteParallelThreadTask(ExecuteParallelThreadTasks executeParallelThreadTasks)
  {
    Assert.expect(executeParallelThreadTasks != null);
    // only allow this to be set once
    Assert.expect(_executeParallelThreadTasks == null);

    _executeParallelThreadTasks = executeParallelThreadTasks;
  }

  /**
   * @author Bill Darbie
   */
  public T call() throws Exception
  {
    try
    {
      _result = executeTask();
    }
    catch (Throwable ex)
    {
      _executeParallelThreadTasks.setException(ex);
      if (ex instanceof Error)
        throw (Error)ex;
      else if (ex instanceof Exception)
        throw (Exception)ex;
      else
        Assert.logException(ex);
    }

    return _result;
  }

  /**
   * @author Rex Shang
   */
  public T get()
  {
    return _result;
  }

  /**
   * @author Bill Darbie
   */
  protected abstract T executeTask() throws Exception;

  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    // By deafault, we do nothing.
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws Exception
  {
    // By deafault, we do nothing.
  }
}
