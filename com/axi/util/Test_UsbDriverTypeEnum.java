package com.axi.util;

import java.io.*;

/**
 * Tests the UsbDriverTypeEnum class
 * @author George A. David
 */
public class Test_UsbDriverTypeEnum extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_UsbDriverTypeEnum());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    UsbDriverTypeEnum driverType = UsbDriverTypeEnum.FTDI;
    Expect.expect(driverType == UsbDriverTypeEnum.FTDI);
    Expect.expect(driverType.getId() == 0);
  }
}
