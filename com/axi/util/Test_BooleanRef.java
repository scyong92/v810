package com.axi.util;

import java.io.*;

/**
* Selftest class
*
* @author Bill Darbie
*/
public class Test_BooleanRef extends UnitTest
{
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_BooleanRef());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    BooleanRef booleanRef = new BooleanRef();

    // check that the default value of BooleanRef is false
    Expect.expect(booleanRef.getValue() == false);

    // now test the constructor
    booleanRef = new BooleanRef(true);
    Expect.expect(booleanRef.getValue() == true);

    booleanRef = new BooleanRef(false);
    Expect.expect(booleanRef.getValue() == false);

    // now test the setValue() method
    booleanRef.setValue(true);
    Expect.expect(booleanRef.getValue() == true);
    booleanRef.setValue(false);
    Expect.expect(booleanRef.getValue() == false);
  }  
}
