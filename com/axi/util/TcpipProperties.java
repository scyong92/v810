package com.axi.util;

import java.net.*;

/**
 * The purpose of this class to provide all the necessary TCPIP properties that
 * a client might be interested such as the IP address, port, and address octet values.
 *
 * @author Greg Esparza
 */
public class TcpipProperties
{
  private static final String _OCTET_DELIMITER = ".";
  private String _ipAddress;
  private int _portNumber;

  /**
   * @author Greg Esparza
   */
  public TcpipProperties(String ipAddress, int portNumber)
  {
    Assert.expect(ipAddress != null);
    Assert.expect(portNumber >= 0);

    _ipAddress = ipAddress;
    _portNumber = portNumber;
  }

  /**
   * @author Greg Esparza
   */
  public String getIpAddress()
  {
    return _ipAddress;
  }

  /**
   * @author Greg Esparza
   */
  public int getPortNumber()
  {
    return _portNumber;
  }

  /**
   * @author Greg Esparza
   */
  public byte[] getIpAddressOctets() throws UnknownHostException
  {
    InetAddress inetAddress = InetAddress.getByName(_ipAddress);

    return inetAddress.getAddress();
  }

  /**
   * @author Greg Esparza
   */
  public static String getOctectDelimiter()
  {
    return _OCTET_DELIMITER;
  }
}
