package com.axi.util;

/**
 * This enumeration contains the byte sizes of 32 bit C data types.
 *
 * Peter Esbensen:  I think that the java library now has methods that provide this info.  Should probably switch
 * to that instead.
 *
 * @author Bill Darbie
 */
public class CDataSizeEnum extends Enum
{
  private static int _index = -1;

  public static final CDataSizeEnum BOOL = new CDataSizeEnum(++_index, 1);
  public static final CDataSizeEnum CHAR = new CDataSizeEnum(++_index, 1);
  public static final CDataSizeEnum UNSIGNED_CHAR = new CDataSizeEnum(++_index, 1);
  public static final CDataSizeEnum SHORT = new CDataSizeEnum(++_index, 2);
  public static final CDataSizeEnum UNSIGNED_SHORT = new CDataSizeEnum(++_index, 2);
  public static final CDataSizeEnum INT = new CDataSizeEnum(++_index, 4);
  public static final CDataSizeEnum UNSIGNED_INT = new CDataSizeEnum(++_index, 4);
  public static final CDataSizeEnum LONG = new CDataSizeEnum(++_index, 4);
  public static final CDataSizeEnum UNSIGNED_LONG = new CDataSizeEnum(++_index, 4);
  public static final CDataSizeEnum FLOAT = new CDataSizeEnum(++_index, 4);
  public static final CDataSizeEnum DOUBLE = new CDataSizeEnum(++_index, 8);

  private int _size;

  /**
   * @param id the unique id of the enumeration
   * @param size the 32 bit C data type size in bytes
   */
  private CDataSizeEnum(int id, int size)
  {
    super(id);
    _size = size;
  }

  /**
   * @return the size in bytes of the 32 bit C data type
   * @author Bill Darbie
   */
  public int getSizeInBytes()
  {
    return _size;
  }
}

