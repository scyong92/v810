package com.axi.util;

import java.util.*;

import com.axi.util.*;

/**
 * This class manages Commands so that undo functionality can be supported.  A block of Commands can
 * be executed as a single command by using the beginCommandBlock() and endCommandBlock() commands.
 * This will cause a set of Commands to be undone and redone as one single Command.
 *
 * @see Command
 *
 * @author Bill Darbie
 */
public class CommandManager
{
  private List<Command> _commands = new ArrayList<Command>();
  // Set of Integers that represent the _commands index of commands that were undo() commands
  private Set<Integer> _undoIndexSet = new HashSet<Integer>();
  // the current _commands index for the next undo command
  private int _undoIndex = -1;
  // the total number of command that a redo() can be called on in a row.
  private int _numRedoCommands = 0;
  private BlockCommand _blockCommand;
  // if this is true then all undo and redo commands also go onto the command list
  // this is identical to the emacs undo.
  // if this is false then undo and redo commands do not go on the command list.
  // This is a simpler, but less powerful mode.
  private boolean _undoAndRedoCommandsGoOnCommandList = false;
  private boolean _execute = true;

  /**
   * Execute the Command passed in.  If command.execute() returns false it will NOT be put on
   * the command list since calling it would not cause any state change anyway.
   *
   * @return true if the Command was put on the Command List.  The Command will not be put on the
   * List if it did not cause a state change (indicated by its execute() method returning false)
   *
   * @author Bill Darbie
   */
  public boolean execute(Command command) throws Exception
  {
    Assert.expect(command != null);

    boolean executed = false;
    if (_blockCommand != null)
    {
      // we are creating a CommandBlock
      executed = command.execute();
      if (executed)
        _blockCommand.addCommand(command);
    }
    else
    {
      // first execute the Command
      executed = command.execute();
      if (executed)
      {
        // the Command ran without throwing an Exception, so update this classes variables
        addCommand(command);
      }
    }

    return executed;
  }

  /**
   * Add the Command to the List of Commands and update class variables appropriately.
   *
   * @author Bill Darbie
   */
  private void addCommand(Command command)
  {
    Assert.expect(command != null);
    // [XCR-2083 subtask] XCR-2098 - Ying-Huan.Chu
    // Do not add this command to the list of commands if this command is an empty command.
    // eg: no commands to be executed in a command block.
    if (command.getNumCommands() > 0)
    {
      if (_undoAndRedoCommandsGoOnCommandList == false)
      {
        // clear out any undo/redo commands from the list before adding this
        // new one to it
        int commandSize = _commands.size();
        int numCommandsToRemove = _commands.size() - (_undoIndex + 1);
        for (int i = 0; i < numCommandsToRemove; ++i)
        {
          _commands.remove(commandSize - 1);
          --commandSize;
        }
      }
      _commands.add(command);
      _numRedoCommands = 0;
      _undoIndex = _commands.size() - 1;
    }
  }

  /**
   * @return true if an undo() operation can be done.
   *
   * @author Bill Darbie
   */
  public boolean isUndoAvailable()
  {
    // we cannot do an undo if there are no undo commands or if we are in the
    // middle of creating a block command.
    if ((_undoIndex >= 0) && (_blockCommand == null))
      return true;

    return false;
  }

  /**
   * @param description is the description of the next Undo Command
   * @return true if an undo() operation can be done.
   *
   * @author Bill Darbie
   */
  public boolean isUndoAvailable(StringBuffer description)
  {
    Assert.expect(description != null);
    if (description.length() > 0)
      description.replace(0, description.length(), "");

    // we cannot do an undo if there are no undo commands or if we are in the
    // middle of creating a block command.
    if ((_undoIndex >= 0) && (_blockCommand == null))
    {
      Command command = (Command)_commands.get(_undoIndex);
      description.append(command.getDescription());
      return true;
    }

    return false;
  }

  /**
   * Undo the last executed Command.
   *
   * @return the Command that was undone.
   *
   * @author Bill Darbie
   */
  public Command undo() throws Exception
  {
    Assert.expect(isUndoAvailable());

    Command command = (Command)_commands.get(_undoIndex);

    // execute the Undo Command
    if (_undoIndexSet.contains(new Integer(_undoIndex)))
    {
      // this command was originally an undo() command, so to "undo" it we should really call execute
      if (_execute)
        command.execute();
    }
    else
    {
      // this command was an excute() call originally, so call undo()
      undo(command);
    }

    // the undo worked without throwing any Exceptions, so update this classes variables
    if (_undoAndRedoCommandsGoOnCommandList)
      _commands.add(command);
    // move the index down one in the list for the next undo
    --_undoIndex;
    // increment the number of redo() calls that can be made
    ++_numRedoCommands;

    Assert.expect(command != null);
    return command;
  }

  /**
   * Do everything a normal undo would do, but don't actually execute the undo
   * on the Command.
   *
   * @author Bill Darbie
   */
  public Command undoWithoutExecuting() throws Exception
  {
    Command command = null;
    _execute = false;
    try
    {
      command = undo();
    }
    finally
    {
      _execute = true;
    }

    return command;
  }

  /**
   * @return true if a redo() operation can be done.
   *
   * @author Bill Darbie
   */
  public boolean isRedoAvailable()
  {
    // we cannot do a Redo if there are no Commands to redo or if we are in the middle of creating
    // a BlockCommand
    if ((_numRedoCommands > 0) && (_blockCommand == null))
      return true;

    return false;
  }

  /**
   * @param description is the description of the next Undo Command
   * @return true if a redo() operation can be done.
   *
   * @author Bill Darbie
   */
  public boolean isRedoAvailable(StringBuffer description)
  {
    Assert.expect(description != null);
    if (description.length() > 0)
      description.replace(0, description.length(), "");

    // we cannot do a Redo if there are no Commands to redo or if we are in the middle of creating
    // a BlockCommand
    if ((_numRedoCommands > 0) && (_blockCommand == null))
    {
      int redoIndex = _undoIndex + 1;
      Command command = (Command)_commands.get(redoIndex);
      description.append(command.getDescription());
      return true;
    }

    return false;
  }

  /**
   * Redo the last undo Command.
   *
   * @return the Command that was redone.
   *
   * @author Bill Darbie
   */
  public Command redo() throws Exception
  {
    Assert.expect(isRedoAvailable());

    int redoIndex = _undoIndex + 1;
    Command command = (Command)_commands.get(redoIndex);

    if (_undoIndexSet.contains(redoIndex))
    {
      undo(command);
    }
    else
    {
      if (_execute)
        command.execute();
    }

    // the redo worked without throwing any Exceptions, so update this classes variables
    if (_undoAndRedoCommandsGoOnCommandList)
      _commands.add(command);

    ++_undoIndex;
    --_numRedoCommands;

    Assert.expect(command != null);
    return command;
  }

  /**
   * @author Bill Darbie
   */
  public Command redoWithoutExecuting() throws Exception
  {
    Command command = null;
    _execute = false;
    try
    {
      command = redo();
    }
    finally
    {
      _execute = true;
    }
    return command;
  }

  /**
   * Mark the beginning of a command block.  All the Commands executed within a command block
   * are treated as a single command for the purposes of undo and redo commands.
   *
   * @author Bill Darbie
   */
  public BlockCommand beginCommandBlock(String description)
  {
    Assert.expect(_blockCommand == null, "ERROR: You must call endCommandBlock() before calling beginCommandBlock() a second time.");

    _blockCommand = new BlockCommand(description);
    return _blockCommand;
  }

  /**
   * Mark the end of a Command block.  All the Commands executed within a command block
   * are treated as a single command for the purposes of undo and redo commands.
   *
   * @author Bill Darbie
   */
  public void endCommandBlock()
  {
    Assert.expect(_blockCommand != null, "ERROR: You should not call endCommandBlock() without first calling beginCommandBlock().");

    addCommand(_blockCommand);
   _blockCommand = null;
  }

  /**
   * Clear out the list of commands built up so far.
   *
   * @author Bill Darbie
   */
  public void clear()
  {
    _commands.clear();
    _undoIndexSet.clear();
    _undoIndex = -1;
    _numRedoCommands = 0;
  }

  /**
   * @author Bill Darbie
   */
  private void undo(Command command) throws Exception
  {
    Assert.expect(command != null);

    if (_execute)
      command.undo();
    boolean added = false;
    if (_undoAndRedoCommandsGoOnCommandList)
    {
      added = _undoIndexSet.add(_commands.size());
      Assert.expect(added);
    }
  }

  /**
   * @return the List of all Commands
   * @author Bill Darbie
   */
  public List<Command> getCommands()
  {
    Assert.expect(_commands != null);

    return new ArrayList<Command>(_commands);
  }

  /**
   * @return the List of all Commands that can be undone at this point in time.  The Commands
   * will be returned in the order that the undo() Commands would happen.
   * @author Bill Darbie
   */
  public List<Command> getUndoCommands()
  {
    List<Command> undoCommands = new ArrayList<Command>();
    int index = 0;
    for (Command command : _commands)
    {
      if (index <= _undoIndex)
        undoCommands.add(command);
      else
        break;

      ++index;
    }

    Collections.reverse(undoCommands);
    Assert.expect(undoCommands != null);
    return undoCommands;
  }

  /**
   * @return the List of all Commands that can be redone at this point in time.  The Commands
   * will be returned in the order that the redo() Commands would happen.
   *
   * @author Bill Darbie
   */
  public List<Command> getRedoCommands()
  {
    List<Command> redoCommands = new ArrayList<Command>();
    int startIndex = _undoIndex + 1;
    int stopIndex = startIndex + _numRedoCommands;
    int size = _commands.size();
    if (stopIndex > size)
      stopIndex = size;
    for (int i = startIndex; i < stopIndex; ++i)
    {
      Command command = _commands.get(i);
      redoCommands.add(command);
    }

    Assert.expect(redoCommands != null);
    return redoCommands;
  }

  /**
   * Remove the last executed Command from the Command List
   * @author Bill Darbie
   */
  public void removeLastCommand()
  {
    Assert.expect(_commands != null);
    int index = _commands.size() - 1;
    Assert.expect(index >= 0);
    _commands.remove(index);

    if (_blockCommand == null)
    {
      // remove this command from _undoIndexSet if it exists in the Set
      _undoIndexSet.remove(new Integer(index));
      --_undoIndex;
      _numRedoCommands = 0;
    }
    else
    {
      _blockCommand.removeLastCommand();
    }
  }


  /**
   * If this is true then all undo and redo commands also go onto the command list
   * this is identical to the emacs undo.
   * if this is false then undo and redo commands do not go on the command list.
   * This is a simpler, but less powerful mode.
   * @author Bill Darbie
   */
  public void setUndoAndRedoCommandsGoOnCommandList(boolean undoAndRedoCommandsGoOnCommandList)
  {
    Assert.expect(_commands.isEmpty());
    _undoAndRedoCommandsGoOnCommandList = undoAndRedoCommandsGoOnCommandList;
  }
}
