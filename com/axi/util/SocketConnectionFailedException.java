package com.axi.util;

import java.io.*;

/**
 * A socket client will throw this exception whenever there is a problem connecting to the socket server or
 * disconnecting from the socket server.
 *
 * @author Eugene Kim-Leighton
 */
public class SocketConnectionFailedException extends IOException
{
  private String _ipAddress;
  private int _portNumber;

  /**
   * @author Bill Darbie
   */
  public SocketConnectionFailedException(String ipAddress, int portNumber, String errorMessage)
  {
    super(errorMessage);
    _portNumber = portNumber;
    _ipAddress = ipAddress;
  }

  /**
   * @author Bill Darbie
   */
  public String getIpAddress()
  {
    return _ipAddress;
  }

  /**
   * @author Bill Darbie
   */
  public int getPortNumber()
  {
    return _portNumber;
  }
}
