package com.axi.util;

import java.io.*;
import java.net.*;
import java.util.TimeZone;
import java.util.regex.*;



/**
* All classes that serve as Unit Tests for java classes should extend
* this class.  It requires that all unit test classes provide a  test
* method.  It also supplies a static execute method that sets up
* Assert logging and input and output streams.
* @author Bill Darbie
*/
public abstract class UnitTest
{
  private static boolean _unitTesting = false;
  private static String _TEST_DIR = "autotest";
  private static String _DATA_DIR = "autotest" + File.separator + "data";
  private String _testSourceDir;
  private String _testInputFileDir;
  private String _testDataDir;
  private String _classesDir;

  /**
   * @author Bill Darbie
   * @author George A. David
   */
  static
  {
    String unitTesting = System.getProperty("unitTesting", "false");
    if (unitTesting.equalsIgnoreCase("true"))
    {
      _unitTesting = true;
      // setting the time zone to GMT
      // previously, the time zone was set to GMT-8 which is Colorado time
      // this worked find, but as soon as we became multi-site, some of the test
      // cases such as StringUtil.testConvertStringToTimeInMilliSeconds()
      // would fail because the new engineers were in a new time zone.
      //
      // George David.
      TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
    }
  }

  /**
   * @author Bill Darbie
   */
  protected UnitTest()
  {
    // set the name of the log file
    Assert.setLogFile("com.axi.util.UnitTest", "UnitTest");
    Assert.doNotSendEmail();

    // if this constructor is called that means that a Unit test has been created
    String unitTesting = System.getProperty("unitTesting", "false");
    if (unitTesting.equalsIgnoreCase("true") == false)
      _unitTesting = false;
    else
      _unitTesting = true;

  }

  /**
   * @return true if we are doing unit tests.
   * @author Bill Darbie
   */
  public static boolean unitTesting()
  {
    return _unitTesting;
  }

  /**
   * @author Bill Darbie
   */
  public static String getUnitTestRelativeDir()
  {
    return _TEST_DIR;
  }

  /**
   * @return the directory where the Test_File.class file is for this test
   * @author Bill Darbie
   */
  public String getTestSourceDir()
  {
    return _testSourceDir;
  }

  /**
   * @return the directory where the Test_File_X.input files are for this test
   * @author Bill Darbie
   */
  public String getTestInputFileDir()
  {
    return _testInputFileDir;
  }

  /**
   * @return the directory where the data files are (.../autotest/data/) for this test
   * @author Bill Darbie
   */
  public String getTestDataDir()
  {
    return _testDataDir;
  }

  /**
   * @return the root directory of where the classes are located (i.e. <view name>/axi/java/classes)
   * @author George A. David
   */
  public String getClassesDir()
  {
    return _classesDir;
  }

  /**
  * Call this method to begin executing a unit test.
  * @param unitTest the test object to be run
  * @author Bill Darbie
  */
  public static void execute(UnitTest unitTest)
  {
    String className = unitTest.getClass().getName();
    int index = className.lastIndexOf(".");
    className = className.substring(index + 1, className.length());
    className = className + ".class";
    URL url = unitTest.getClass().getResource(className);
    String path = url.getPath();
    path = path.replaceFirst(className, "");
    path = path.substring(1);
    path = path.replace('\\', File.separatorChar);
    path = path.replace('/', File.separatorChar);

    String subPath = File.separator + "java" + File.separator + "classes";
    index = path.indexOf(subPath) + subPath.length();
    unitTest._classesDir = path.substring(0, index);


    String compileStr = "\\" + File.separator + "java" + "\\" + File.separator + "classes" + "\\" + File.separator;
    Pattern pattern = Pattern.compile(compileStr);
    Matcher matcher = pattern.matcher(path);
    String replacement = "\\" + File.separator + "java" + "\\" + File.separator;
    unitTest._testSourceDir = matcher.replaceFirst(replacement);
    unitTest._testInputFileDir = unitTest._testSourceDir + _TEST_DIR;
    unitTest._testDataDir = unitTest._testSourceDir + _DATA_DIR;



    //Give tests a chance to move files, prepare configs, etc.
    unitTest.setupBeforeTest();

    //Startup the hardware (if necessary)
    unitTest.prepareHardware();

    try
    {
      InputStreamReader inputStreamReader = new InputStreamReader(System.in);
      BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

      // do not buffer the output
      // if a program hangs or dies during selftest it is nice to know what the
      // last output was, buffering prevents this
      // the true on the second parameter turns on auto flushing
      PrintWriter printWriter = new PrintWriter(System.out, true);

      unitTest.test(bufferedReader, printWriter);
    }
    catch(Throwable throwable)
    {
      unitTest.terminateAllLaunchedProcesses(0);
      throwable.printStackTrace();
      System.exit(1);
    }
    finally
    {
      unitTest.terminateAllLaunchedProcesses(0);
      unitTest.restoreAfterTest();
    }
    System.exit(0);
  }

  /**
   * This is a useful utility method provided to strip out the view name from the front of
   * any string in the object array that might have it.
   * If it sees anything with /unitTest/ or /autotest/ it will strip off
   * the view name from the path.
   * So  m:/yourView/5dx/rel/unitTest/xyz would become <viewName>/5dx/rel/unitTest/xyz.
   * and m:/yourView/java/com/axi/util/autotest/xyz would become <viewName>/java/com/axi/util/autotest/xyz
   *
   * @author Bill Darbie
   */
  public static void stripOffViewName(Object[] objects)
  {
    if (objects != null)
    {
      for (int i = 0; i < objects.length; i++)
      {
        if (objects[i] instanceof String)
          objects[i] = stripOffViewName((String)objects[i]);
      }
    }
  }

  /**
   * This is a useful utility method provided to strip out the view name from the front of
   * any string that might have it.
   * If it sees anything with /unitTest/ or /autotest/ it will strip off
   * the view name from the path.
   * So  m:/yourView/5dx/rel/unitTest/xyz would become <viewName>/5dx/rel/unitTest/xyz.
   * and m:/yourView/java/com/axi/util/autotest/xyz would become <viewName>/java/com/axi/util/autotest/xyz
   * and d:/statDir/yourStaticView/5dx/rel/unitTest/xyz would become <viewName>/5dx/rel/unitTest/xyz.
   * and d:/statDir/yourStaticView/java/com/axi/util/autotest/xyz would become <viewName>/java/com/axi/util/autotest/xyz
   *
   * @author Bill Darbie
   * @author Peter Esbensen
   */
  public static String stripOffViewName(String str)
  {
    Assert.expect(str != null);

    Pattern pattern = Pattern.compile("(.*\\s*\\b)[a-zA-Z]:(\\\\[^:\\s]*)(axi)(\\\\(cpp|java|testRel))(\\\\[^:\\s]*[\\\\]*(unitTest|autotest)\\\\.*)", Pattern.DOTALL);
    
    String strippedString = new String(str);
    Matcher match = pattern.matcher(str);

    if (match.matches())
    {
      strippedString = match.group(1) + "<viewName>" + match.group(4) + match.group(6);
      // call this method recursively until no more matches are made
      strippedString = stripOffViewName(strippedString);
    }
    else
    {
      Pattern patternForCcrcViews = Pattern.compile("(.*\\s*\\b)[a-zA-Z]:(\\\\[^:\\s]*)\\\\(lio_axi)(\\\\[^:\\s]*\\\\(unitTest|autotest)\\\\.*)", Pattern.DOTALL);
      strippedString = new String(str);
      match = patternForCcrcViews.matcher(str);

      if (match.matches())
      {
        strippedString = match.group(1) + "<viewName>" + match.group(4);
        // call this method recursively until no more matches are made
        strippedString = stripOffViewName(strippedString);
      }
    }


    return strippedString;
  }

  /**
   * Method to give tests a chance to change the state of the system before
   * it is started up. This method could be abstract BUT that would ripple through
   * every unit test so it is a stub instead.
   *
   * @author Reid Hayhow
   */
  protected void setupBeforeTest()
  {
    //By default do nothing. Only needed by classes that need to setup the
    //system (config files, calib files, etc.) before the simulation hardware
    //is started.
  }

  /**
   * Method to prepare the XrayTester hardware for use. This allows the unit
   * test to control when this happens during the execute process. This method
   * could be abstract BUT that would ripple through every unit test so it is
   * a stub instead.
   *
   * @author Reid Hayhow
   */
  protected void prepareHardware()
  {
    //By default, do nothing. This method can be overridden by derived classes
    //that need to use the hardware.
  }

  /**
   * If (and only if) there were launched processes, all will be terminated
   * at the end of the execute method of this class.
   *
   * @author Roy Williams
   */
  protected synchronized void terminateAllLaunchedProcesses(int waitForFlushMilliSeconds)
  {
    // By default, do nothing.   This method has implementations to override it
    // in derived classes.
  }

  /**
   * Method to clean up after the test, copy back files, etc. This method could be
   * abstract BUT that would ripple through every unit test so it is a stub instead.
   *
   * @author Reid Hayhow
   */
  protected void restoreAfterTest()
  {
    //By default do nothing. Only needed by classes that changed the state of
    //the system (config files, calib files, etc.) before the simulation hardware
    //is started.
  }

  /**
  * All unit test classes must provide a test() method that matches
  * this signature.
  * @author Bill Darbie
  */
  public abstract void test(BufferedReader is, PrintWriter os);
}
