package com.axi.util;

import java.io.*;

/**
 * @author Ronald Lim
 */

public class Test_BinaryFileReader extends UnitTest
{
/**
 * @author Ronald Lim
 */

  public static void main (String[] args)
  {
    UnitTest.execute(new Test_BinaryFileReader());
  }
  public void test(BufferedReader is, PrintWriter os)
  {
    testReadFileInBytes(os);
  }

  private void testReadFileInBytes(PrintWriter os)
  {
    Assert.expect(os != null);

    String filePath = getTestDataDir() + File.separator + "Test_BinaryFileReader" + File.separator + "xrayCameraFirmware.ace";
    byte[] bytes = null;

    try
    {
      BinaryFileReader fileReader = new BinaryFileReader();
      bytes = fileReader.readFileInByte(filePath);
    }
    catch ( Exception exp)
    {
      Expect.expect(false);
      exp.printStackTrace(os);
    }

    os.println("The following is the binary contents of the file " + UnitTest.stripOffViewName(filePath));
    os.println("The length of the file is " + bytes.length + " bytes.");
    for(int i = 0; i < bytes.length; ++i)
    {
      os.println("bytes[" + i + "] = " + bytes[i]);
    }

  }
}
