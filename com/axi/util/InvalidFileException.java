package com.axi.util;

import java.io.*;

/**
 * This exception will be thrown if the file accessed is invalid
 *
 * @author Eugene Kim-Leigthon
 */
public class InvalidFileException extends IOException
{
  private String _fileNameWithFullPath;

  /**
   * @author Eugene Kim-Leighton
   */
  public InvalidFileException(String fileNameWithFullPath)
  {
    super("The file " + fileNameWithFullPath + " is not valid.");
    Assert.expect(fileNameWithFullPath != null);

    _fileNameWithFullPath = fileNameWithFullPath;
  }

  /**
   * @author Eugene Kim-Leighton
   */
  public String getFileNameWithFullPath()
  {
    return _fileNameWithFullPath;
  }
}