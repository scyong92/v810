package com.axi.util;

import java.io.*;

/**
* Selftest class
*
* @author Bill Darbie
*/
public class Test_IntegerRef extends UnitTest
{
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_IntegerRef());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    IntegerRef integerRef = new IntegerRef();

    // check that the default value of IntegerRef is 0
    Expect.expect(integerRef.getValue() == 0);

    // now test the constructor
    integerRef = new IntegerRef(10);
    Expect.expect(integerRef.getValue() == 10);

    // now test the setValue() method
    integerRef.setValue(-5);
    Expect.expect(integerRef.getValue() == -5);
  }  
}
