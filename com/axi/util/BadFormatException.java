package com.axi.util;

/**
 * This class is thrown when parsing a String to another data type fails.
 *
 * @author Keith Lee
 */
public class BadFormatException extends LocalizedException
{
  /**
   * @param localizedString - the Localized String for the exception.
   * @author Keith Lee
   */
  public BadFormatException(LocalizedString localizedString)
  {
    super(localizedString);
  }

  /**
   * @author Laura Cormos
   */
  public LocalizedString getLocalizedString()
  {
    return super.getLocalizedString();
  }
}
