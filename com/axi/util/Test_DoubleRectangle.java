package com.axi.util;

import java.io.*;

/**
 * @author Keith Lee
 */
class Test_DoubleRectangle extends UnitTest
{
  public static void main(String args[])
  {
    UnitTest.execute(new Test_DoubleRectangle());
  }

  /**
   * @author Keith Lee
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    boolean exception = false;
    DoubleRectangle rectangle = null;
    DoubleCoordinate topLeftCoordinate = null;
    DoubleCoordinate bottomRightCoordinate = null;
    DoubleCoordinate coordinate = null;

    // constructor
    topLeftCoordinate = new DoubleCoordinate(100, 200);
    bottomRightCoordinate = new DoubleCoordinate(300, 400);
    try
    {
      rectangle = new DoubleRectangle(null, bottomRightCoordinate);
    }
    catch(AssertException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      rectangle = new DoubleRectangle(topLeftCoordinate, null);
    }
    catch(AssertException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    rectangle = new DoubleRectangle(topLeftCoordinate, bottomRightCoordinate);
    Expect.expect( rectangle.getTopLeftCornerCoordinate().getX() == topLeftCoordinate.getX() );
    Expect.expect( rectangle.getTopLeftCornerCoordinate().getY() == topLeftCoordinate.getY() );
    Expect.expect( rectangle.getBottomRightCornerCoordinate().getX() == bottomRightCoordinate.getX() );
    Expect.expect( rectangle.getBottomRightCornerCoordinate().getY() == bottomRightCoordinate.getY() );

    // top left corner coordinate
    try
    {
      rectangle.setTopLeftCornerCoordinate(null);
    }
    catch(AssertException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    coordinate = new DoubleCoordinate(10, 20);
    rectangle.setTopLeftCornerCoordinate(coordinate);
    Expect.expect( rectangle.getTopLeftCornerCoordinate().getX() == coordinate.getX() );
    Expect.expect( rectangle.getTopLeftCornerCoordinate().getY() == coordinate.getY() );

    // bottom right corner coordinate
    try
    {
      rectangle.setBottomRightCornerCoordinate(null);
    }
    catch(AssertException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    coordinate = new DoubleCoordinate(30, 40);
    rectangle.setBottomRightCornerCoordinate(coordinate);
    Expect.expect( rectangle.getBottomRightCornerCoordinate().getX() == coordinate.getX() );
    Expect.expect( rectangle.getBottomRightCornerCoordinate().getY() == coordinate.getY() );
  }
}
