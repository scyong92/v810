package com.axi.util;

import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * This class allows you to time events.  In will measure time
 * between pais of start() and stop() calls.
 *
 * To simply time a single event do somthing like this:
 * timer.reset();
 * timer.start();
 * functionYouWantToTime();
 * timer.stop();
 * timer.getElapsedTimeInMillis()
 *
 * To time groups of things with chunks of time in the middle that
 * you do not want to time do this:
 *
 * timer.reset();
 * timer.start();
 * functionToTime();
 * timer.stop();
 * functionYouDontWantToTime();
 * timer.start();
 * functionToTime();
 * timer.stop();
 * timer.getElapsedTime();
 *
 * @author Bill Darbie
 */
public class TimerUtil
{
  private long _startTimeInMillis;
  private long _elapsedTimeInMillis;
  private boolean _isRunning = false;

  /**
   * @author Bill Darbie
   */
  public TimerUtil()
  {
    reset();
  }

  /**
   * @author Bill Darbie
   */
  public TimerUtil(TimerUtil timerUtil)
  {
    Assert.expect(timerUtil != null);

    _startTimeInMillis = timerUtil._startTimeInMillis;
    _elapsedTimeInMillis = timerUtil._elapsedTimeInMillis;

    // leave this as false to be compatible with Bill's orig implementation
    _isRunning = false;
  }

  /**
   * Starts the timer.  Please do not change the order of these commands because
   * it is the ORDER that guarantees these functions can be used from multiple threads.
   *
   * @author Bill Darbie
   */
  public void start()
  {
    // Starting time is NOW.
    _startTimeInMillis = System.currentTimeMillis();

    // Enable getElapsedTimeInMillis()
    _isRunning = true;   // Not really running till flag is set.
  }

  /**
   * Stops the timer
   *
   * @author Bill Darbie
   */
  public void stop()
  {
    if (_isRunning)
    {

      long diffTime = System.currentTimeMillis() - _startTimeInMillis;
      if (diffTime < 0)
        diffTime = 0;
      _elapsedTimeInMillis += diffTime;
    }
    _isRunning = false;
  }

  /**
   * @author Bill Darbie
   */
  public void reset()
  {
    _startTimeInMillis = System.currentTimeMillis();
    _elapsedTimeInMillis = 0;
    _isRunning = false;
  }

  /**
   * @author Bill Darbie
   */
  public void restart()
  {
    _elapsedTimeInMillis = 0;
    start();
  }

  /**
   * @return the number of milliseconds elapsed between all start() and stop() methods
   *
   * @author Bill Darbie
   */
  public long getElapsedTimeInMillis()
  {
    if (_isRunning)
    {
      // Multiple threads are calling getElapsedTimeInMillis() while other threads
      // are calling restart().   Restart will set the _startTimeInMillis.  Because
      // threads get swaped, diffTime can be < 0.
      long diffTime = System.currentTimeMillis() - _startTimeInMillis;
      if (diffTime < 0)
        diffTime = 0;
      long time = _elapsedTimeInMillis + diffTime;

      // Make sure we do not have a negative time.  This can happen if the time is changed on the clock on the computer.
      if (time < 0)
        time = 0;
      return time;
    }

    // Make sure we do not have a negative time.  This can happen if the time is changed on the clock on the computer.
    if (_elapsedTimeInMillis < 0)
      _elapsedTimeInMillis = 0;

    return _elapsedTimeInMillis;
  }

  /**
   * @author Poh Kheng
   * This would print out the current timeStamp
   */
  public static void printCurrentTime(String message)
  {
      Date currentDate = new Date(System.currentTimeMillis());
      SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss SSS");
      String dateString = formatter.format(currentDate);
      System.out.println(message + ", " + dateString);
  }
}
