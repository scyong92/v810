package com.axi.util;

import java.util.Comparator;

/**
 * A Comparator object suitable for using with Pair<F,S> objects.
 * This object only functions if F and S are Comparable.
 *
 * The natural order for Pairs is lexographic. S is only compared
 * if the comparison of F yeilds 'equal'. Eg. (1, 2) < (1, 30), but (30,1) > (20, 20)
 *
 * @author Patrick Lacz
 */
public class PairComparator<F extends Comparable<F>, S extends Comparable<S>> implements Comparator<Pair<F, S>>
{
  /**
   * @author Patrick Lacz
   */
  public int compare(Pair<F, S> a, Pair<F, S> b)
  {
    if (a == null || b == null)
      throw new NullPointerException();
    int firstComparison = a.getFirst().compareTo(b.getFirst());
    if (firstComparison == 0)
    {
      int secondComparison = a.getSecond().compareTo(b.getSecond());
      return secondComparison;
    }
    return firstComparison;
  }
}
