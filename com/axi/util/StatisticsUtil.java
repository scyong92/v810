package com.axi.util;

import java.util.*;

/**
 * @author Patrick Lacz
 */
public class StatisticsUtil
{
  public StatisticsUtil()
  {
  }

  /**
   * Find the mean of the given one dimensional data.
   * @todo IPP implementation
   * @author Patrick Lacz
   */
  static public float mean(float... array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    double sum = 0.f;
    for (int i = 0; i < array.length; ++i)
      sum += array[i];

    return (float)(sum / array.length);
  }

  /**
   * Find the mean of the given one dimensional data from the given start index
   * up to the given final index (non-inclusive).
   *
   * @todo IPP implementation
   * @todo regression test
   *
   * @author Peter Esbensen
   */
  static public float mean(float[] array,
                           int startIndex,
                           int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);    
    Assert.expect(endIndex <= array.length);    
    Assert.expect(endIndex > startIndex);   

    double sum = 0.f;
    for (int i = startIndex; i < endIndex; ++i)
      sum += array[i];

    return (float)(sum / (endIndex - startIndex) );
  }

  /**
   * Find the mean of the given one dimensional data from the given start index
   * up to the given final index (non-inclusive) and exclude the given outliers
   * from the calculation.
   *
   * @todo IPP implementation
   * @todo regression test
   *
   * @author Lim, Lay Ngor
   */
  static public float meanExcludeOutlier(float[] array,
                           int startIndex,
                           int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);    
    Assert.expect(endIndex <= array.length);    
    Assert.expect(endIndex > startIndex);   

    //MINIMUM data length to pass in for exclude outlier MUST BE 2 due to quartile calculation.
    if(array.length < 2)
      return mean(array, startIndex, endIndex);

    //because there are start and end index, might need to filter out those those before excluding outlier
    Set<Integer> outlierSet = detectOutlierIndex(array);

    //if there are no outliers, return mean as normal
    if(outlierSet.size() != 0)
    {
      int totalNumberOfData = 0;
      double sum = 0.f;
      for (int i = startIndex; i < endIndex; ++i)
      {
        //only add to sum if it is not in outlier list.
        if (outlierSet.contains(i) == false)
        {
          sum += array[i];
          totalNumberOfData++;
        }
      }
      return (float)(sum / totalNumberOfData);  //(endIndex - startIndex) );
    }
    else
    {
      return mean(array, startIndex, endIndex);
    }
  }

  /**
   * Find the mean of the given one dimensional data.
   * @todo IPP implementation
   * @author Peter Esbensen
   */
  static public float mean(int... array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    double sum = 0.f;
    for (int i = 0; i < array.length; ++i)
      sum += array[i];

    return (float)(sum / array.length);
  }

  /**
   * Find the mean of the given one dimensional data.
   * @todo IPP implementation
   * @author Patrick Lacz
   */
  static public float[] mean(float array[][])
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(array[0].length > 0);

    float result[] = new float[array[0].length];

    for (int d = 0 ; d < result.length ; ++d)
    {
      double sum = 0.f;
      for (int i = 0; i < array.length; ++i)
        sum += array[i][d];

      result[d] = (float)(sum / array.length);
    }

    return result;
  }


  /**
   * Compute the mean of a List or subclass
   * @author Rick Gaudette
   */
  public static Double mean(List<Double> collection)
  {
    if (collection.size() < 1)
      return Double.NaN;

    Double accum = 0.0;
    for (Double value : collection)
      accum += value;

    accum /= collection.size();
    return accum;
  }


  /**
   * Find the standard deviation from the mean of one dimensional data.
   * @todo IPP implementation
   * @author Patrick Lacz
   */
  static public float standardDeviation(float array[])
  {
    return standardDeviation(array, mean(array));
  }

  /**
   * Find the standard deviation from a given 'mean' of one dimensional data.
   * @author Patrick Lacz
   */
  static public float standardDeviation(float array[], float mean)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 1);

    double sum = 0.f;
    for (int i = 0; i < array.length; ++i)
    {
      double difference = array[i] - mean;
      sum += difference * difference;
    }
    double average = sum / (array.length - 1);
    return (float)Math.sqrt(average);
  }

  /**
   * @author Patrick Lacz
   */
  static public float[] standardDeviation(float array[][], float mean[])
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 1);
    Assert.expect(mean != null);
    Assert.expect(array[0].length == mean.length);

    float stdDev[] = new float[mean.length];

    // compute the deviation in each dimension separately
    for (int dim = 0 ; dim < mean.length ; ++dim)
    {
      double sum = 0.f;
      for (int i = 0; i < array.length; ++i)
      {
        double difference = array[i][dim] - mean[dim];
        sum += difference * difference;
      }
      double average = sum / (array.length - 1);
      stdDev[dim] = (float)Math.sqrt(average);
    }

    return stdDev;
  }

  /**
   * Computes the median value for the array.
   * If you will need the quartile ranges later, use quartileRanges.
   *
   * @author Patrick Lacz
   */
  static public float median(float... array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    float arrayCopy[] = array.clone();
    return medianAllowModification(arrayCopy);
  }

  /**
   * Computes the median value for the array; the array will be modified (sorted) by this method.
   * If you will need the quartile ranges later, use quartileRanges.
   *
   * @author Patrick Lacz
   */
  static public float medianAllowModification(float array[])
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    boolean isEvenSize = array.length % 2 == 0;

    Arrays.sort(array);

    float medianValue = Float.NaN;
    if (isEvenSize)
    {
      int medianIndex = Math.max(0, array.length / 2 - 1);
      medianValue = (array[medianIndex] * 0.5f + array[medianIndex + 1] * 0.5f);
    }
    else
    {
      int medianIndex = Math.max(0, (array.length + 1) / 2 - 1);
      medianValue = array[medianIndex];
    }

    Assert.expect(Float.isNaN(medianValue) == false);
    return medianValue;
  }

  /**
   * Exclude the outliers from the array.
   * Computes the median value for the array; the array will be modified (sorted) by this method.
   * If you will need the quartile ranges later, use quartileRanges.
   *
   * @author Patrick Lacz
   * @author Lim, Lay Ngor - Exclude Outlier
   */
  static public float medianExcludedOutliers(float array[])
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    float arrayCopy[] = array.clone();
    return medianExcludedOutliersAllowModification(arrayCopy);
  }

  /**
   * Exclude the outliers from the array. 
   * Computes the median value for the
   * array; the array will be modified (sorted) by this method. 
   * If you will need the quartile ranges later, use quartileRanges.
   * 
   * @author Patrick Lacz
   * @author Ngie Xing - Exclude Outlier
   */
  static public float medianExcludedOutliersAllowModification(float array[])
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    //Minimum data length to pass in for exclude outlier is 2 due to quartile calculation
    //will be handled in getExcludedOutliersArray
    float[] arrayExcludedOutliers = getExcludedOutliersArray(array);
    return medianAllowModification(arrayExcludedOutliers);
  }

  /**
   * Computes the median value for the array.
   * If you will need the quartile ranges later, use quartileRanges.
   *
   * @author Patrick Lacz
   */
  static public float median(float array[], int beginIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    float arrayCopy[] = ArrayUtil.copy(array, beginIndex, endIndex);

    boolean isEvenSize = arrayCopy.length % 2 == 0;

    Arrays.sort(arrayCopy);

    float medianValue = Float.NaN;
    if (isEvenSize)
    {
      int medianIndex = Math.max(0, arrayCopy.length / 2 - 1);
      medianValue = (arrayCopy[medianIndex] * 0.5f + arrayCopy[medianIndex + 1] * 0.5f);
    }
    else
    {
      int medianIndex = Math.max(0, (arrayCopy.length + 1) / 2 - 1);
      medianValue = arrayCopy[medianIndex];
    }

    Assert.expect(Float.isNaN(medianValue) == false);
    return medianValue;
  }


  /**
   * Finds the values at the ranges of the quartiles.
   * @return {min, 1st quartile, median, 3rd quartile, max}
   * @author Patrick Lacz
   */
  static public float[] quartileRanges(float... array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length >= 2);

    float arrayCopy[] = array.clone();
    return quartileRangesAllowModification(arrayCopy);
  }

  /**
   * Exclude the outliers from the array.
   * Finds the values at the ranges of the quartiles.
   * @return {min, 1st quartile, median, 3rd quartile, max}
   * @author Patrick Lacz
   * @author Lim, Lay Ngor - Exclude Outlier
   */  
  static public float[] quartileRangesExcludedOutliers(float array[])
  {
    Assert.expect(array != null);
    Assert.expect(array.length >= 2);

    float arrayCopy[] = array.clone();
    return quartileRangesExcludedOutliersAllowModification(arrayCopy);
  }

  /**
   * Exclude the outliers from the array.
   * Finds the values at the ranges of the quartiles.
   * @return {min, 1st quartile, median, 3rd quartile, max}
   * @author Patrick Lacz
   * @author Lim, Lay Ngor - Exclude Outlier
   */  
  static public float[] quartileRangesExcludedOutliersAllowModification(float array[])
  {
    Assert.expect(array != null);
    Assert.expect(array.length >= 2);

    //revert back to normal learn if array length after excluded outlier is less than 2
    //this is handled in getExcludedOutliersArray
    float[] arrayExcludedOutliers = getExcludedOutliersArray(array);
    return quartileRangesAllowModification(arrayExcludedOutliers);
  }
  
  /**
   * Finds the values at the ranges of the quartiles.
   * This version of this method allows the modification of the input array. This is valuable for memory-sensitive situations
   * where we want to avoid the extra copy of the array.
   *
   * @return {min, 1st quartile, median, 3rd quartile, max}
   * @author Patrick Lacz
   */
  static public float[] quartileRangesAllowModification(float array[])
  {
    Assert.expect(array != null);
    Assert.expect(array.length >= 2);

    float result[] = new float[5];
    boolean isEvenSize = array.length % 2 == 0;

    Arrays.sort(array);

    // Minimum
    result[0] = array[0];

    /**
     * There are a variety of ways to choose the 1st and 3rd quartiles,
     * see Mathworld's entry on Quartile (http://mathworld.wolfram.com/Quartile.html)
     *
     * These are the formulas listed attributed to 'Moore and McCabe (2002)'
     */
    double firstQuartile = 0.25f * (array.length + 1) - 1;
    if (isEvenSize)
      firstQuartile = 0.25f * (array.length + 2) - 1;

    double thirdQuartile = 0.25f * (3 * array.length + 3) - 1;
    if (isEvenSize)
      thirdQuartile = 0.25f * (3 * array.length + 2) - 1;

    // Interpolated First Quartile
    double fraction = firstQuartile - Math.floor(firstQuartile);
    int indexToLeft = (int)firstQuartile;
    int indexToRight = Math.min((indexToLeft + 1), (array.length - 1));
    float valueToLeft = array[indexToLeft];
    float differenceBetweenValueToLeftAndRight = array[indexToRight] - array[indexToLeft];
    result[1] = valueToLeft + (float)fraction * differenceBetweenValueToLeftAndRight;

    // Median
    int medianIndex = (array.length + 1) / 2 - 1;
    if (isEvenSize)
    {
      // do average
      result[2] = 0.5f * array[medianIndex] +
                  0.5f * array[medianIndex + 1];
    }
    else
    {
      result[2] = array[medianIndex];
    }

    // Interpolated Third Quartile
    fraction = thirdQuartile - Math.floor(thirdQuartile);
    indexToLeft = (int)thirdQuartile;
    indexToRight = Math.min((indexToLeft + 1), (array.length - 1));
    valueToLeft = array[indexToLeft];
    differenceBetweenValueToLeftAndRight = array[indexToRight] - array[indexToLeft];
    result[3] = valueToLeft + (float)fraction * differenceBetweenValueToLeftAndRight;

    // Maximum
    result[4] = array[array.length - 1];

    return result;
  }


  /**
   * Find the median values for each dimension in the array.
   * If you will need the quartile ranges later, use quartileRanges.
   *
   * @author Patrick Lacz
   */
  static public float[] median(float array[][])
  {
    float quartileRanges[][] = quartileRanges(array);
    Assert.expect(quartileRanges != null);
    Assert.expect(quartileRanges.length == 5);
    return quartileRanges[2];
  }

  /**
   * Finds the values at the ranges of the quartiles.
   * @return {min, 1st quartile, median, 3rd quartile, max} for each dimension
   * @author Patrick Lacz
   */
  static public float[][] quartileRanges(float array[][])
  {
    Assert.expect(array != null);

    float arrayCopy[] = new float[array.length];

    int dim = array[0].length;
    float result[][] = new float[5][dim];
    boolean isEvenSize = array.length % 2 == 0;

    /**
     * There are a variety of ways to choose the 1st and 3rd quartiles,
     * see Mathworld's entry on Quartile (http://mathworld.wolfram.com/Quartile.html)
     *
     * These are the formulas listed attributed to 'Moore and McCabe (2002)'
     */
    double firstQuartile = 0.25 * (array.length + 1) - 1;
    if (isEvenSize)
      firstQuartile = 0.25 * (array.length + 2) - 1;
    int firstQuartileIndex = (int)Math.floor(firstQuartile);
    boolean useAverageForFirstQuartile = firstQuartileIndex < firstQuartile;

    double thirdQuartile = 0.75 * (array.length + 1) - 1;
    if (isEvenSize)
      thirdQuartile = (3.0 * array.length + 2.0) / 4.0 - 1;
    int thirdQuartileIndex = (int)Math.floor(thirdQuartile);
    boolean useAverageForThirdQuartile = thirdQuartileIndex < thirdQuartile;

    double median = 0.5*(array.length + 1) - 1;
    int medianIndex = (int)Math.floor(median);
    boolean useAverageForMedian = medianIndex < median;

    for (int d = 0 ; d < dim ; ++d)
    {
      for (int i = 0 ; i < array.length ; ++i)
        arrayCopy[i] = array[i][d];

      Arrays.sort(arrayCopy);

      // Minimum
      result[0][d] = arrayCopy[0];

      // First Quartile
      if (useAverageForFirstQuartile) {
        result[1][d] = 0.5f * arrayCopy[firstQuartileIndex] +
                       0.5f * arrayCopy[firstQuartileIndex+1];
      } else {
        result[1][d] = arrayCopy[firstQuartileIndex];
      }

      // Median
      if (useAverageForMedian) {
        result[2][d] = 0.5f * arrayCopy[medianIndex] +
                       0.5f * arrayCopy[medianIndex+1];
      } else {
        result[2][d] = arrayCopy[medianIndex];
      }

      // ThirdQuartile
      if (useAverageForThirdQuartile) {
        result[3][d] = 0.5f * arrayCopy[thirdQuartileIndex] +
                       0.5f * arrayCopy[thirdQuartileIndex+1];
      } else {
        result[3][d] = arrayCopy[thirdQuartileIndex];
      }

      // Maximum
      result[4][d] = arrayCopy[arrayCopy.length-1];
    }

    return result;
  }

  /**
   * Normalize the distances in one dimension.
   * In this case, Maximum(Infinity), Manhattan(L1), and Euclidian(L2) are all the same.
   *
   * @author Patrick Lacz
   */
  static public float[] norm(float array[], float mean)
  {
    float newArray[] = new float[array.length];
    for (int i = 0; i < array.length; ++i)
    {
      newArray[i] = Math.abs(array[i] - mean);
    }
    return newArray;
  }

  /**
   * The one dimensional Mahalanobis norm is only the distance over the standard deviation.
   *
   * @author Patrick Lacz
   */
  static public float[] normMahalanobis(float array[], float mean)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);

    float stdDev = standardDeviation(array, mean);
    float newArray[] = new float[array.length];

    if (MathUtil.fuzzyEquals(stdDev, 0.0, 0.0000001))
    {
      // this is the case if all the data points are essentially the same value.
      // so, set the distance to zero.
      Arrays.fill(newArray, 0.f);
      return newArray;
    }

    for (int i = 0; i < array.length; ++i)
    {
      newArray[i] = Math.abs(array[i] - mean) / stdDev;
    }
    return newArray;
  }

  /**
   * The Mahalanobis norm is a distance inversely scaled by the standard deviation of
   * each dimension.
   *
   * @author Patrick Lacz
   */
  static public float[] normMahalanobis(float array[][], float mean[])
  {
    Assert.expect(array != null);
    Assert.expect(mean != null);

    float stdDev[] = standardDeviation(array, mean);

    float newArray[] = new float[array.length];
    for (int i = 0; i < array.length; ++i)
    {
      float sum = 0.f;
      for (int j = 0; j < array[i].length; ++j)
      {
        float difference = array[i][j] - mean[j];
        if (MathUtil.fuzzyEquals(stdDev[j], 0.0, 0.000001) == false)
          sum += (difference * difference) / (stdDev[j] * stdDev[j]);
      }
      newArray[i] = (float)Math.sqrt(sum);
    }
    return newArray;
  }

  /**
   * The maximum difference from the mean of each dimension.
   * AKA Infinity Norm.
   *
   * @author Patrick Lacz
   */
  static public float[] normMaximum(float array[][], float mean[])
  {
    Assert.expect(array != null);
    Assert.expect(mean != null);

    float newArray[] = new float[array.length];
    for (int i = 0; i < array.length; ++i)
    {
      float max = array[i][0];
      for (int j = 1; j < array[i].length; ++j)
      {
        float val = Math.abs(array[i][j] - mean[j]);
        if (max < val)
          max = val;
      }
      newArray[i] = max;
    }
    return newArray;
  }

  /**
   * The Manhattan distance is the sum of the distance from the mean of each
   * dimension.
   * AKA L1 Norm.
   *
   * @author Patrick Lacz
   */
  static public float[] normManhattan(float array[][], float mean[])
  {
    Assert.expect(array != null);
    Assert.expect(mean != null);

    float newArray[] = new float[array.length];
    for (int i = 0; i < array.length; ++i)
    {
      float sum = 0.f;
      for (int j = 0; j < array[i].length; ++j)
      {
        sum += Math.abs(array[i][j] - mean[j]);
      }
      newArray[i] = sum;
    }
    return newArray;
  }

  /**
   * The Euclidian distance from each point to the mean.
   * AKA L2 Norm.
   *
   * @author Patrick Lacz
   */
  static public float[] normEuclidian(float array[][], float mean[])
  {
    Assert.expect(array != null);
    Assert.expect(mean != null);

    float newArray[] = new float[array.length];
    for (int i = 0; i < array.length; ++i)
    {
      float sum = 0.f;
      for (int j = 0; j < array[i].length; ++j)
      {
        float val = array[i][j] - mean[j];
        sum += val * val;
      }
      newArray[i] = (float)Math.sqrt(sum);
    }
    return newArray;
  }

  /**
   * @author Patrick Lacz
   */
  static public Set<Integer> classifyOutliers(float array[], float distanceThreshold)
  {
    Set<Integer> outlierSet = new TreeSet<Integer>();
    for (int i = 0; i < array.length; ++i)
    {
      if (array[i] > distanceThreshold)
      {
        outlierSet.add(i);
      }
    }
    return outlierSet;
  }

  /**
   * Prune values from the array with a magnitude greater than a given value.
   * Since almost all the norms are the same with one dimensional data, with this function
   * a method is not specified. Use the more general classifyOutliers for other methods.
   * @author Patrick Lacz
   */
  static public Set<Integer> classifyOutliers(float array[], float mean, float threshold)
  {
    return classifyOutliers(array, mean, StatisticsNormEnum.L1, threshold);
  }

  /**
   * @author Patrick Lacz
   */
  static public Set<Integer> classifyOutliers(float array[], float mean, StatisticsNormEnum normMethod, float threshold)
  {
    float normalizedArray[] = null;

    if (normMethod.equals(StatisticsNormEnum.MAHALANOBIS))
    {
      normalizedArray = normMahalanobis(array, mean);
    }
    else
    {
      normalizedArray = norm(array, mean);
    }

    Assert.expect(normalizedArray != null);
    return classifyOutliers(normalizedArray, threshold);
  }

  /**
   * @author Patrick Lacz
   */
  static public float[] norm(float array[][], float mean[], StatisticsNormEnum normMethod)
  {
    float normalizedArray[] = null;

    if (normMethod.equals(StatisticsNormEnum.MAXIMUM))
    {
      normalizedArray = normMaximum(array, mean);
    }
    else if (normMethod.equals(StatisticsNormEnum.MANHATTAN))
    {
      normalizedArray = normManhattan(array, mean);
    }
    else if (normMethod.equals(StatisticsNormEnum.EUCLIDIAN))
    {
      normalizedArray = normEuclidian(array, mean);
    }
    else if (normMethod.equals(StatisticsNormEnum.MAHALANOBIS))
    {
      normalizedArray = normMahalanobis(array, mean);
    }

    return normalizedArray;
  }

  /**
   * @author Patrick Lacz
   */
  static public Set<Integer> classifyOutliers(float array[][], float mean[], StatisticsNormEnum normMethod,
                                              float threshold)
  {
    float normalizedArray[] = norm(array, mean, normMethod);

    Assert.expect(normalizedArray != null);
    return classifyOutliers(normalizedArray, threshold);
  }

  /**
   * Continues iterating on removing outliers until the set does not change.
   * Aborts after n-1 iterations or when the validIndexList is empty.
   *
   * @return A Set containing the indicies of all the data points not classified as outliers.
   * @author Patrick Lacz
   */
  static public Set<Integer> convergeOnValidSet(float array[], StatisticsNormEnum normMethod, float threshold)
  {
    // an ArrayList is used so that it is indexable by intermediate results from classifyOutliers
    List<Integer> validIndexList = new ArrayList<Integer>(array.length);

    // populate the set with all the values from array.  All values are valid initially.
    for (int i = 0 ; i < array.length ; ++i)
      validIndexList.add(i);

    // the for loop handles the abortion conditions,
    // a break inside handles the normal loop exit
    int maxIterations = array.length - 1;
    for (int iterations = 0 ;
         iterations < maxIterations && validIndexList.isEmpty() == false ;
         iterations++)
    {
      float subArray[] = new float[validIndexList.size()];

      int i = 0;
      for (Integer index : validIndexList)
        subArray[i++] = array[index.intValue()];

      // compute the new mean, and classify outliers from that mean.
      Set<Integer> outlierSet = classifyOutliers(subArray, mean(subArray), normMethod, threshold);

      // end condition: no outliers found.
      if (outlierSet.isEmpty())
        break;

      // go backwards to keep the indices valid
      // remove the outliers from our data.
      Integer outliers[] = new Integer[outlierSet.size()];
      outlierSet.toArray(outliers);
      for (int o = outliers.length-1 ; o >= 0 ; --o)
        validIndexList.remove(outliers[o].intValue());
    }

    return new TreeSet<Integer>(validIndexList);
  }

  /**
   * Continues iterating on removing outliers until the set does not change.
   * Aborts after n-1 iterations or when the validIndexList is empty.
   *
   * @return A Set containing the indicies of all the data points not classified as outliers.
   * @author Patrick Lacz
   */
  static public Set<Integer> convergeOnValidSet(float array[][], StatisticsNormEnum normMethod, float threshold)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(array[0].length > 0);

    // an ArrayList is used so that it is indexable by intermediate results from classifyOutliers
    List<Integer> validIndexList = new ArrayList<Integer>(array.length);

    // populate the set with all the values from array.  All values are valid initially.
    for (int i = 0 ; i < array.length ; ++i)
      validIndexList.add(i);

    int dimension = array[0].length;

    // the for loop handles the abortion conditions,
    // a break inside handles the normal loop exit
    int maxIterations = array.length - 1;
    for (int iterations = 0 ;
         iterations < maxIterations && validIndexList.isEmpty() == false ;
         iterations++)
    {
      float subArray[][] = new float[validIndexList.size()][dimension];

      int i = 0;
      for (Integer index : validIndexList)
        subArray[i++] = array[index.intValue()].clone();

      // compute the new mean, and classify outliers from that mean.
      Set<Integer> outlierSet = classifyOutliers(subArray, mean(subArray), normMethod, threshold);

      // end condition: no outliers found.
      if (outlierSet.isEmpty())
        break;

      // go backwards to keep the indices valid
      // remove the outliers from our data.
      Integer outliers[] = new Integer[outlierSet.size()];
      outlierSet.toArray(outliers);
      for (int o = outliers.length-1 ; o >= 0 ; --o)
        validIndexList.remove(outliers[o].intValue());
    }

    return new TreeSet<Integer>(validIndexList);
  }

  /**
   * Converge on a mean value by weighting outliers less than values close to the mean.
   * The attenuationStrength parameter controls how much outliers should influence the result.
   * An attentuation strength of zero will compute the standard mean (don't use this function, use mean).
   * Low, fractional values (0..1) will give greater weight to outliers than higher values (> 1).
   *
   * Performs a maximum of ten iterations.
   * @todo : try other weighting functions than used here to figure out
   *  what works well with the actual data. (multi-nodal, etc try gaussian, 1/x, etc)
   *
   * @param precisionThreshold Stop when the change in mean is less than this value.
   * @author Patrick Lacz
   */
  public static float convergeOnMean(float array[], float precisionThreshold, float attenuationStrength)
  {
    Assert.expect(array != null);
    Assert.expect(attenuationStrength >= 0.0);

    if (attenuationStrength == 0.0)
      return mean(array);

    double previousMean = mean(array);
    double changeInMean = 0.0;

    double gaussianMagnitude = 1.0/Math.sqrt(2.0*Math.PI);

    int iterations = 0;
    int maxIterations = 10;

    do {
      double sum = 0.0;
      double weightTotal = 0.0;

      float deviation[] = normMahalanobis(array, (float)previousMean);

      for (int i = 0 ; i < deviation.length ; ++i)
      {
        // we can simplify the guassian calculation becuase sigma is 1.0, and deviation is already x-mean
        double weight = gaussianMagnitude*Math.exp(-deviation[i]*deviation[i]/2.0);

        // this is a linear model that is intended to be modified by the attenuationStrength parameter
        //double weight = Math.max(1.0 - deviation[i] / 4.0, 0.0);

        // if attenuation Strength is < 1, more weight is given to outliers
        // if attenuation Strength is > 1, less weight is given to outliers
        weight = Math.pow(weight, attenuationStrength);

        weightTotal += weight;

        sum += array[i] * weight;
      }
      Assert.expect(MathUtil.fuzzyEquals(weightTotal, 0.0, 0.000001) == false);
      double newMean = sum / weightTotal;

      changeInMean = Math.abs(newMean - previousMean);
      previousMean = newMean;

      if (iterations++ >= maxIterations)
        break;
    } while (changeInMean > precisionThreshold);

    return (float)previousMean;
  }

  /**
   * Converge on a mean value by weighting outliers less than values close to the mean.
   * The attenuationStrength parameter controls how much outliers should influence the result.
   * An attentuation strength of zero will compute the standard mean (don't use this function, use mean).
   * Low, fractional values (0..1) will give greater weight to outliers than higher values (> 1).
   *
   * At 1, the mean is found by weighting values by the gaussian function.
   *
   * Performs a maximum of ten iterations.
   *
   * @param precisionThreshold Stop when the change in mean is less than this value.
   * @author Patrick Lacz
   */
  public static float[] convergeOnMean(float array[][], float precisionThreshold, float attenuationStrength)
  {
    Assert.expect(array != null);
    Assert.expect(attenuationStrength >= 0.0);

    if (attenuationStrength == 0.0)
      return mean(array);

    float previousMean[] = mean(array);
    double changeInMean = 0.0;

    double gaussianMagnitude = 1.0/Math.sqrt(2.0*Math.PI);

    int iterations = 0;
    int maxIterations = 50; // should never reach here.  A pathological case takes about 30 to converge w/ .1% of the range

    do {
      double sum[] = new double[array[0].length];
      double weightTotal = 0.0;

      float deviation[] = normMahalanobis(array, previousMean);

      for (int i = 0 ; i < deviation.length ; ++i)
      {
        // we can simplify the gaussian calculation becuase sigma is 1.0, and deviation is already x-mean
        double weight = gaussianMagnitude*Math.exp(-deviation[i]*deviation[i]/2.0);

        // if attenuation Strength is < 1, more weight is given to outliers
        // if attenuation Strength is > 1, less weight is given to outliers
        weight = Math.pow(weight, attenuationStrength);

        weightTotal += weight;

        for (int d = 0 ; d < sum.length ; ++d)
          sum[d] += array[i][d] * weight;
      }

      Assert.expect(MathUtil.fuzzyEquals(weightTotal, 0.0, 0.000001) == false);
      float newMean[] = new float[previousMean.length];
      for (int d = 0 ; d < newMean.length ; ++d)
        newMean[d] = (float)(sum[d] / weightTotal);

      changeInMean = 0.0;
      for (int d = 0 ; d < newMean.length ; ++d)
      {
        double difference = Math.abs(newMean[d] - previousMean[d]);
        if (difference > changeInMean)
          changeInMean = difference;
      }

      previousMean = newMean;

      if (iterations++ >= maxIterations)
        break;
    } while (changeInMean > precisionThreshold);

    return previousMean;
  }

  /**
   * Returns the specified raw moment of the given array.  The raw moment
   * is defined as:
   * <br><pre>
   *
   * raw moment = sum over all X( ( x ^ order ) * array[x] )
   *
   * </pre><br>
   *
   * @todo create regr test
   *
   * @author Peter Esbensen
   */
  public static float getRawMoment(float[] array, int momentOrder)
  {
    Assert.expect(array != null);
    Assert.expect(momentOrder >= 0);

    float moment = 0.0f;

    for (int i = 0; i < array.length; ++i)
    {
      moment += ( Math.pow( i,  momentOrder) * array[i] );
    }
    return moment;
  }

  /**
   * Returns the centroid of the given array.  The centroid is defined as:
   * <br><pre>
   *
   * centroid = m1 / m0
   *
   * where m1 is the first raw moment and m0 is the zeroth raw moment
   *
   * </pre><br>
   *
   * @todo create regr test
   *
   * @author Peter Esbensen
   */
  public static float getCentroid(float... array)
  {
    return getRawMoment(array, 1) / getRawMoment(array, 0);
  }

  /**
   * Returns the specified central moment of the given array.  The central moment
   * is defined as:
   * <br><pre>
   *
   * moment = sum over all X( ( ( x - centroid ) ^ momentOrder ) * array[x] )
   *
   * </pre><br>
   *
   * @todo create regr test
   *
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  public static float getCentralMoment(float[] array, int momentOrder)
  {
    Assert.expect(array != null);
    Assert.expect(momentOrder >= 0);

    float centroid = getCentroid(array);
    return getCentralMoment(array, centroid, momentOrder);
  }

  /**
   * Returns the specified central moment of the given array.  The central moment
   * is defined as:
   * <br><pre>
   *
   * moment = sum over all X( ( ( x - centroid ) ^ momentOrder ) * array[x] )
   *
   * </pre><br>
   *
   * @todo create regr test
   *
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  public static float getCentralMoment(float[] array, float centroid, int momentOrder)
  {
    Assert.expect(array != null);
    Assert.expect(momentOrder >= 0);

    float moment = 0.0f;
    for (int i = 0; i < array.length; ++i)
    {
      moment += ( Math.pow( i - centroid, momentOrder ) * array[i] );
    }
    return moment;
  }

  /**
   * Compute the skewness of the data.
   * Skewness indicates if the data does not look symmetric about the center point. In general,
   * the center here should always be the centroid, as computed with getCentroid().
   *
   * This definition of skew is: u3/u2^(3/2)
   *
   * Negative values means that the lower values are less clustered than higher values, and vice versa for
   * positive skewness values.
   *
   * The range of the number is related to the sample set. A 'Standard Error of Skewness' is sqrt(6/array.length).
   * PE:  I believe that in this context (the array is a profile or a histogram, not just a collection of observations),
   * the SES should equal sqrt(6/sum(array))
   *
   * You can be sure your data is skewed if the absolute value of the skewness is greater than twice this value.
   * A gaussian distribution will have a skewness near zero (noise will pull it one direction or the other.)
   *
   * Use a quartile or mean vs. mode definion of skew if your data is especially noisy.
   *
   * @author Patrick Lacz
   */
  public static float getSkewness(float[] array, float center)
  {
    Assert.expect(array != null);

    float moment2 = getCentralMoment(array, center, 2);
    float moment3 = getCentralMoment(array, center, 3);

    float skew = moment3 / (float)Math.pow(moment2, 1.5);

    return skew;
  }

  /**
   * Estimates the requested percentile (between 0 and 1) of the specified data set.
   * This uses the algorithm from NIST (http://www.itl.nist.gov/div898/handbook/prc/section2/prc252.htm).
   *
   * @author Matt Wharton
   */
  public static float percentile(float[] array, float targetPercentile)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect((targetPercentile >= 0f) && (targetPercentile <= 1f));

    // Sort the array (using a defensive array copy).
    float[] sortedArray = array.clone();
    return percentileAllowModification(sortedArray, targetPercentile);
  }

  /**
   * Exclude the outliers from the array.
   * Estimates the requested percentile (between 0 and 1) of the specified data set.
   * This uses the algorithm from NIST (http://www.itl.nist.gov/div898/handbook/prc/section2/prc252.htm).
   *
   * @author Matt Wharton
   * @author Lim, Lay Ngor - Exclude Outlier
   */
  public static float percentileExcludedOutliers(float[] array, float targetPercentile)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect((targetPercentile >= 0f) && (targetPercentile <= 1f));
    
    // Sort the array (using a defensive array copy).
    float[] sortedArray = array.clone();
    return percentileExcludedOutliersAllowModification(sortedArray, targetPercentile);
  }
  
   /**
   * Exclude the outliers from the array.
   * Estimates the requested percentile (between 0 and 1) of the specified data set.
   * This uses the algorithm from NIST (http://www.itl.nist.gov/div898/handbook/prc/section2/prc252.htm).
   *
   * @author Matt Wharton
   * @author Ngie Xing - Exclude Outlier
   */
  public static float percentileExcludedOutliersAllowModification(float[] array, float targetPercentile)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect((targetPercentile >= 0f) && (targetPercentile <= 1f));

    //MINIMUM data length to pass in for exclude outlier MUST BE 2 due to quartile calculation.
    //handled in getExcludedOutliersArray
    float[] arrayExcludedOutliers = getExcludedOutliersArray(array);
    return percentileAllowModification(arrayExcludedOutliers, targetPercentile);
  }
  
  /**
   * Estimates the requested percentile (between 0 and 1) of the specified data set.
   * This uses the algorithm from NIST (http://www.itl.nist.gov/div898/handbook/prc/section2/prc252.htm).
   *
   * @author Matt Wharton
   */
  public static float percentileAllowModification(float[] array, float targetPercentile)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect((targetPercentile >= 0f) && (targetPercentile <= 1f));

    Arrays.sort(array);

    // Calculate the percentile rank index and seperate out the integer and fractional parts.
    float percentileRankIndex = targetPercentile * (float)(array.length + 1);
    int percentileRankIndexIntPart = (int)Math.floor(percentileRankIndex);
    float percentileRankIndexFractionalPart = Math.abs(percentileRankIndex - (float)percentileRankIndexIntPart);

    float estimatedPercentileValue = 0f;
    if (percentileRankIndexIntPart == 0)
    {
      estimatedPercentileValue = array[0];
    }
    else if (percentileRankIndexIntPart == array.length)
    {
      estimatedPercentileValue = array[array.length - 1];
    }
    else if ((percentileRankIndexIntPart > 0) && (percentileRankIndexIntPart < array.length))
    {
      estimatedPercentileValue =
          array[percentileRankIndexIntPart - 1] +
          (percentileRankIndexFractionalPart * (array[percentileRankIndexIntPart] - array[percentileRankIndexIntPart - 1]));
    }
    else
    {
      // Shouldn't occur.
      Assert.expect(false);
    }

    return estimatedPercentileValue;
  }

  /**
   * Find the maximum value of the given one dimensional data from the given start index
   * up to the given final index (non-inclusive).
   *
   * @todo IPP implementation
   * @todo regression test
   *
   * @author Lim, Lay Ngor - XCR1743:Benchmark
   */
  static public float maximum(float[] array,
                           int startIndex,
                           int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);    
    Assert.expect(endIndex <= array.length);//final index non-inclusive
    Assert.expect(endIndex >= startIndex);   
    
    if(endIndex == startIndex)//No range, just single pixel
      return array[startIndex];

    float maxValue = array[startIndex];
    for (int i = startIndex; i < endIndex; ++i)//final index non-inclusive
      maxValue = Math.max(maxValue, array[i]);   
          
    return maxValue;
  }
  
  //temprary for testing use
  static public int maximum(int[] array,
                           int startIndex,
                           int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);    
    Assert.expect(endIndex <= array.length);//final index non-inclusive
    Assert.expect(endIndex >= startIndex);   
    
    if(endIndex == startIndex)//No range, just single pixel
      return array[startIndex];

    int maxValue = array[startIndex];
    for (int i = startIndex; i < endIndex; ++i)//final index non-inclusive
      maxValue = Math.max(maxValue, array[i]);   
          
    return maxValue;
  }  
  
    /**
   * Find the index of maximum value for the given one dimensional data from the given start index
   * up to the given final index (non-inclusive).
   *
   * @todo IPP implementation
   * @todo regression test
   *
   * @author Lim, Lay Ngor - XCR2100 - BrokenPin
   */
  static public int maximumIndex(int[] array,
                           int startIndex,
                           int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);    
    Assert.expect(endIndex <= array.length);//final index non-inclusive
    Assert.expect(endIndex >= startIndex);   
    
    if(endIndex == startIndex)//No range, just single pixel
      return array[startIndex];

    int maxIndex = startIndex;
    int maxValue = array[startIndex];
    for (int i = startIndex; i < endIndex; ++i)//final index non-inclusive
    {
//      maxIndex = i;
//      maxValue = Math.max(maxValue, array[i]);       
      if(maxValue < array[i])
      {
        maxValue = array[i];
        maxIndex = i;
      }
    }
          
//    return maxValue;
    return maxIndex;
  }
  
  /**
   * Find the minimum value of the given one dimensional data from the given start index
   * up to the given final index (non-inclusive).
   *
   * @todo IPP implementation
   * @todo regression test
   *
   * @author Lim, Lay Ngor
   */
  static public float minimum(float[] array,
                           int startIndex,
                           int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);    
    Assert.expect(endIndex <= array.length);//final index non-inclusive
    Assert.expect(endIndex >= startIndex);   

    if(endIndex == startIndex) //No range, just single pixel
      return array[startIndex];
    
    float minValue = array[startIndex];
    for (int i = startIndex; i < endIndex; ++i)//final index non-inclusive
      minValue = Math.min(minValue, array[i]);   
          
    return minValue;
  }

  /**
   * Detect outlier from an array list.
   * Returns detected outlier set.
   *
   * @author Lim, Lay Ngor - Exclude Outlier
   */
  static public Set<Integer> detectOutlierIndex(float[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length >= 2);    
    
    final float[] result = getOutliersOuterFences(array);
    final float lowerOuterFence = result[0];
    final float upperOuterFence = result[1];

    Set<Integer> outlierSet = new TreeSet<Integer>();
//    Set<Integer> filterSet = new TreeSet<Integer>();
    for (int i = 0; i < array.length; ++i)
    {
      if (array[i] < lowerOuterFence || array[i] > upperOuterFence)
        outlierSet.add(i);
    }

    return outlierSet;
  }

  /**
   * Detect outlier from an array list.
   * Returns array excluded the detected outliers.
   *
   * @author Lim, Lay Ngor - Exclude Outlier
   */
  static public float[] getExcludedOutliersArray(float[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length >= 2);

    //MINIMUM data length to pass in for exclude outlier MUST BE 2 due to quartile calculation.
    if(array.length < 2)
      return array;

    final float[] result = getOutliersOuterFences(array);
    final float lowerOuterFence = result[0];
    final float upperOuterFence = result[1];

    Set<Integer> filterSet = new TreeSet<Integer>();
    for (int i = 0; i < array.length; ++i)
    {
      if (array[i] >= lowerOuterFence && array[i] <= upperOuterFence)
        filterSet.add(i);
    }

    int j = 0;
    float[] filteredArray = new float[filterSet.size()];
    for (Integer i : filterSet)
    {
      filteredArray[j] = array[i];
      ++j;
    }

    return filteredArray;
  }

  /**
   * Detect outlier from an array list and return the lower and upper outer fence.
   * //suggest to add multiple method, wrap one with hardcoded method
   *
   * @author Lim, Lay Ngor - Exclude Outlier
   */
  static public float[] getOutliersOuterFences(float[] array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length >= 2);
    
    final float[] interQuartileRanges = quartileRanges(array);
    final float q1 = interQuartileRanges[1];
    final float q3 = interQuartileRanges[3];
    final float weight = 3.0f;//1.5f; //inner fence(minor outliers) = 1.5, outer fence(major outliers) = 3.0
    final float lowerOuterFence = q1 - ((q3 - q1) * weight);
    final float upperOuterFence = q3 + ((q3 - q1) * weight);
    float result[] = new float[2];
    result[0] = lowerOuterFence;
    result[1] = upperOuterFence;
//    System.out.println("q1" + q1 + ", q3" + q3);
//    System.out.println("lowerOuterFence:" + lowerOuterFence + ", upperOuterFence:" + upperOuterFence);

    return result;
  }
}
