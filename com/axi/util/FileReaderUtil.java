package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * Provides a way to read data from a file.
 * @author George A. David
 */
public class FileReaderUtil
{
  private String _filePath;
  private LineNumberReader _reader;
  private String _nextLine;
  private boolean _wasHasNextLineCalled = false;

  /**
   * @author George A. David
   */
  public FileReaderUtil()
  {
    // do nothing
  }

  /**
   * @author George A. David
   */
  public void open(String filePath) throws FileNotFoundException
  {
    Assert.expect(filePath != null);

    _filePath = filePath;

    FileReader fileReader = new FileReader(_filePath);
    _reader = new LineNumberReader(fileReader);
    _wasHasNextLineCalled = false;
  }

  /**
   * @author George A. David
   */
  public String getFilePath()
  {
    Assert.expect(_filePath != null);

    return _filePath;
  }

  /**
   * @author George A. David
   */
  public boolean hasNextLine() throws IOException
  {
    Assert.expect(_reader != null);

    if(_wasHasNextLineCalled == false)
    {
      _nextLine = _reader.readLine();
      _wasHasNextLineCalled = true;
    }

    return _nextLine != null;
  }

  /**
   * @author George A. David
   */
  public String readNextLine() throws IOException
  {
    Assert.expect(hasNextLine());

    _wasHasNextLineCalled = false;

    return _nextLine;
  }

  /**
   * @author George A. David
   */
  public int getLineNumber()
  {
    Assert.expect(_reader != null);

    return _reader.getLineNumber();
  }

  /**
   * @author George A. David
   */
  public void close()
  {
    if(_reader == null)
      return;

    try
    {
      _reader.close();
      _reader = null;
    }
    catch (IOException ex)
    {
      // do nothing
    }
  }
}
