package com.axi.util;

import java.io.*;
import java.util.*;

/**
 * @author John Dutton
 */
public class Test_FileLogger extends UnitTest
{
  /**
   * @author John Dutton
   */
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_FileLogger());
  }

  /**
   * @author John Dutton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String logFilesDirectory = getTestDataDir() + File.separator + getClass().getName();
    String logFileFullPathName = logFilesDirectory + File.separator +
                                 "testFileLogger.log";
    try
    {
      // Before starting, if log file directory has been left from earlier test runs,
      // delete it and its contents.  This means the first test will indirectly test
      // that the logger works when the log directory doesn't already exist.  (The case
      // for when the directory already exists will happen in subsequent tests.)
      if (FileUtil.exists(logFilesDirectory))
      {
        FileUtil.deleteDirectoryContents(logFilesDirectory);
        FileUtil.delete(logFilesDirectory);
      }

      // Test 1 -- See that log file is being created and contains correct content.

      int fileLoggerMaxSize = 10000;
      int fileLoggerIncreasedSize = 1000;
      FileLogger fileLogger = new FileLogger(logFileFullPathName, fileLoggerMaxSize);
      String logString = "Here is some nice data from my confirmation test";
      int logStringSize = logString.length();
      int appendNos = 50;
      int logFileSize = 0;

      for (int i = 0; i < appendNos; i++)
      {
        fileLogger.append(logString);
        logFileSize += logStringSize;
      }

      File logFile = new File(logFileFullPathName);
      Expect.expect(logFile.exists(), "Missing expected log file");

      // Check contents of log file
      int logStringCount = checkLogFileContents(logString, logFileFullPathName);
      // Check length
      Expect.expect(logStringCount == appendNos, "Log file content too short");

      // Test 2 -- See that log file truncates to maximum file size and check content.
      while (logFileSize < fileLoggerMaxSize + fileLoggerIncreasedSize)
      {
        fileLogger.append(logString);
        logFileSize += logStringSize;
      }
      // Check contents of log file
      logStringCount = checkLogFileContents(logString, logFileFullPathName);
      // Check length -- too complicated after truncation for exact check.  Do rough check.
      Expect.expect(logStringCount >= appendNos, "After truncation log file content too short");
      Expect.expect(logFile.length() <= fileLoggerMaxSize, "Size of log file too big");

      // Test 3 -- see that file size is ignored when not specified
      FileLogger fileLogger2 = new FileLogger(logFileFullPathName);
      long logFileSize2 = logFile.length();
      while (logFileSize2 < fileLoggerMaxSize + fileLoggerIncreasedSize)
      {
        fileLogger2.append(logString);
        appendNos++;
        logFileSize2 += logStringSize;
      }
      // Check contents of log file
      logStringCount = checkLogFileContents(logString, logFileFullPathName);
      // Check length -- again, roughly.
      Expect.expect(logStringCount >= appendNos, "No Size Limit Case: Log file content too short");
      Expect.expect(logFile.length() >= fileLoggerMaxSize, "Size of log file too small");

      // Test 4 -- check proper parameter handling
      try
      {
        FileLogger fileLogger3 = new FileLogger(null, fileLoggerMaxSize);
        Expect.expect(false, "FileLogger constructor allows null log file name");
      }
      catch (AssertException ae)
      {
        // do nothing
      }
      try
      {
        FileLogger fileLogger4 = new FileLogger(logFileFullPathName, 0);
        Expect.expect(false, "FileLogger constructor allows bad log file size");
      }
      catch (AssertException ae)
      {
        // do nothing
      }
      try
      {
        String logFileFullPathName6 = getTestDataDir() + File.separator +
                                      "testFileLog<ger.log";
        FileLogger fileLogger6 = new FileLogger(logFileFullPathName6, fileLoggerMaxSize);
        fileLogger6.append("foobar"); // Constructor doesn't fail; this does.
        Expect.expect(false, "FileLogger constructor allows bad character in file name");
      }
      catch (IOException ioe)
      {
        // do nothing
      }
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
    finally
    {
      if (FileUtil.exists(logFilesDirectory))
      {
        try
        {
          FileUtil.deleteDirectoryContents(logFilesDirectory);
          FileUtil.delete(logFilesDirectory);
        }
        catch (CouldNotDeleteFileException ex)
        {
          Expect.expect(false, "File logger could not delete test directory as part of cleanup");
        }
      }
    }
  }


  private int checkLogFileContents(String logString,
                                   String logFileFullPathName) throws IOException
  {
    String endOfTimeStamp = "M, ";
    int logStringCount = 0;
    FileReaderUtil fru = new FileReaderUtil();
    fru.open(logFileFullPathName);

    while (fru.hasNextLine())
    {
      String s = fru.readNextLine();
      int index = s.indexOf(endOfTimeStamp);
      String s2 = s.substring(index + endOfTimeStamp.length());
      Expect.expect(s2.equals(logString), "Log file content incorrect");
      logStringCount++;
    }

    fru.close();
    return logStringCount;
  }


}
