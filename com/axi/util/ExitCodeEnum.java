package com.axi.util;

/**
 *
 * @author Chong, Wei Chin
 */
public class ExitCodeEnum extends Enum
{
  private static int _index = -1;
  private String _name;

  public static final ExitCodeEnum NO_ERRORS = new ExitCodeEnum(++_index, "No errors");  // 0
  public static final ExitCodeEnum GENERAL_ERRORS = new ExitCodeEnum(++_index, "Catchall for general errors");  // 1
  public static final ExitCodeEnum MISUSE_ERROR = new ExitCodeEnum(++_index, "Misuse of shell builtins");  // 2
  public static final ExitCodeEnum CANNOT_EXECUTE_ERROR = new ExitCodeEnum(++_index, "Command cannot invoke");  //126
  public static final ExitCodeEnum NO_COMMAND_ERROR = new ExitCodeEnum(++_index, "Command not found");  // 127
  public static final ExitCodeEnum INVALID_ARGUMENT_ERROR = new ExitCodeEnum(++_index, "Command not found"); // 128
  public static final ExitCodeEnum FATAL_ERROR = new ExitCodeEnum(++_index, "Fatal error signal (n)"); // 129-165

  /**
   * @author Chong, Wei Chin
   */
  private ExitCodeEnum(int id, String name)
  {
    super(id);
    _name = name.intern();
  }

  /**
   * Returns a String representation of the ExitCodeEnum class
   * @return String representaion of the instance of ExitCodeEnum
   * @author Chong, Wei Chin
   */
  public String toString()
  {
    return _name;
  }
}
