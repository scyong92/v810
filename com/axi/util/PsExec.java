package com.axi.util;

import java.io.*;

/**
 * Utilities like Telnet and remote control programs like Symantec's PC
 * Anywhere let you execute programs on remote systems, but they can be a pain
 * to set up and require that you install client software on the remote systems
 * that you wish to access. PsExec is a light-weight telnet-replacement that
 * lets you execute processes on other systems, complete with full interactivity
 * for console applications, without having to manually install client software.
 *
 * PsExec's most powerful uses include launching interactive command-prompts on
 * remote systems and remote-enabling tools like IpConfig that otherwise do not
 * have the ability to show information about remote systems.
 *
 * @author Chong Wei Chin
 */
public class PsExec
{
  // Don't wait for process to terminate (non-interactive).
  private static final String _NO_WAIT_OPTION = "-d";
  // Specifies timeout in seconds connecting to remote computers. eg -n numberOfSeconds
  private static final String _TIMEOUT_OPTION = "-n";

  // Run the remote process in the System account.
  private static final String _SYSTEM_OPTION = "-s";
  // Does not load the specified account's profile.
  private static final String _NO_ACCOUNT_OPTION = "-e";
  // Display the UI on the Winlogon secure desktop (local system only).
  private static final String _DISPLAY_UI_OPTION = "-x";  

  // Copy the specified program to the remote system for execution. 
  // If you omit this option the application must be in the system path on the remote system.
  // eg. -c [-f|-v] 
  private static final String _COPY = "-c";
  private static final String _FORCE_COPY = "-f";
  private static final String _VERSION_COPY = "-v";

  // Set the working directory of the process (relative to remote computer).
  // eg. -w directory
  private static final String _WORKING_DIR = "-w";

  // Specifies optional user name for login to remote computer. eg. -u username
  private static final String _USER = "-u";
  // Specifies optional password for user name.
  // If you omit this you will be prompted to enter a hidden password. eg. -p password
  private static final String _PASSWORD = "-p";

  private final String _EMPTY_SPACE = " ";

  private static final String _PSEXEC = "psexec.exe";
  private static String _psExecPath = "";  
  private String _workingDirectory = null;
  private String _computerAddress = null;
  private String _user = null;
  private String _password = null;
  
  private static PsExec _instance = null;

  /**
   * @param path
   */
  public PsExec(String path)
  {
    Assert.expect(path != null);
    _psExecPath = path;
  }

  /**
   * @return
   */
  public static PsExec getInstance()
  {
    if(_instance == null)
    {
      // default path
      _instance = new PsExec("C:\\PsTools");
    }
    return _instance;
  }
  
  /**
   * @return
   */
  public static PsExec getInstance(String path)
  {
    if(_instance == null)
    {
      _instance = new PsExec(path);
    }
    return _instance;
  }

  /**
   * @param ipAddress
   * @param userName
   * @param Password
   */
  public void setupConnection(String ipAddress, String userName, String password)
  {
    Assert.expect(ipAddress != null);
    
    _computerAddress = ipAddress;
    _user = userName;
    _password = password;
  }

  /**
   * @param processName
   * @throws IOException
   *
   * @author Chong, Wei Chin
   */
  public ExitCodeEnum execProcess(String processNameFullPath) throws IOException
  {
    Assert.expect(processNameFullPath != null);
    return execProcess(processNameFullPath, "");
  }

  /**
   * @param processName
   * @throws IOException
   *
   * @author Chong, Wei Chin
   */
  public ExitCodeEnum execProcess(String processNameFullPath, String arguments) throws IOException
  {
    Assert.expect(processNameFullPath != null);
    Assert.expect(arguments != null);

    processNameFullPath = addQuote(processNameFullPath);

    String command = configureCommands();

    if(hasWorkingDirectory())
      command = command + _EMPTY_SPACE + _WORKING_DIR + _EMPTY_SPACE+ _workingDirectory;

    command = command + _EMPTY_SPACE + processNameFullPath;

    Process p = Runtime.getRuntime().exec( command + _EMPTY_SPACE + arguments);
    try
    {
      p.waitFor();
    }
    catch (InterruptedException ex)
    {
      // do nothing
    }
    return handleExitCode(p.exitValue());
  }

  /**
   * @param processName
   * @throws IOException
   *
   * @author Chong, Wei Chin
   */
  public ExitCodeEnum execProcessNoWait(String processNameFullPath) throws IOException
  {
    Assert.expect(processNameFullPath != null);
    return execProcessNoWait(processNameFullPath, "");
  }

  /**
   * @param processName
   * @throws IOException
   *
   * @author Chong, Wei Chin
   */
  public ExitCodeEnum execProcessNoWait(String processNameFullPath, String arguments) throws IOException
  {
    Assert.expect(processNameFullPath != null);
    Assert.expect(arguments != null);

    processNameFullPath = addQuote(processNameFullPath);

    String command = configureCommands();

    if(hasWorkingDirectory())
      command = command + _EMPTY_SPACE + _WORKING_DIR + _EMPTY_SPACE + _workingDirectory;
    
    command = command + _EMPTY_SPACE + _NO_WAIT_OPTION;
    command = command + _EMPTY_SPACE + processNameFullPath;

    Process p = Runtime.getRuntime().exec(command + _EMPTY_SPACE + arguments);

    try
    {
      p.waitFor();
    }
    catch (InterruptedException ex)
    {
      // do nothing
    }
    return handleExitCode(p.exitValue());
  }

  /**
   * @param path
   * @author Chong, Wei Chin
   */
  public void resetPath(String path)
  {
    Assert.expect(path != null);
    _psExecPath = path;
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  private String configureCommands()
  {
    Assert.expect(_computerAddress != null, "No Remote Server Specify!");

    String command = getExecutionFullPath();

    command = addQuote(command);

    command = command + _EMPTY_SPACE + "\\\\" + _computerAddress;
    if(_user != null)
    {
    // add user
      command = command + _EMPTY_SPACE + _USER + _EMPTY_SPACE + _user;
    // add password
      command = command + _EMPTY_SPACE + _PASSWORD + _EMPTY_SPACE + _password;
    }

    return command;
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  public String getExecutionFullPath()
  {
    if(_psExecPath.endsWith(File.separator))
      return _psExecPath + _PSEXEC;
    else if(_psExecPath.isEmpty())
      return  _PSEXEC;
    
    return _psExecPath + File.separator + _PSEXEC;
  }

  /**
   * @param exitValue
   * @return
   *
   * @author Chong, Wei Chin
   */
  private ExitCodeEnum handleExitCode(int exitValue)
  {
    if(exitValue == 1)
    {
      return ExitCodeEnum.GENERAL_ERRORS;
    }
    else if(exitValue == 2)
    {
      return ExitCodeEnum.MISUSE_ERROR;
    }
    else if(exitValue == 126)
    {
      return ExitCodeEnum.CANNOT_EXECUTE_ERROR;
    }
    else if(exitValue == 127)
    {
      return ExitCodeEnum.NO_COMMAND_ERROR;
    }
    else if(exitValue == 128)
    {
      return ExitCodeEnum.INVALID_ARGUMENT_ERROR;
    }
    else if(exitValue > 128 && exitValue <= 165)
    {
      return ExitCodeEnum.FATAL_ERROR;
    }
    return ExitCodeEnum.NO_ERRORS;
  }

  /**
   * @param originalString
   * @return
   *
   * @author Chong, Wei Chin
   */
  private String addQuote(String originalString)
  {
    Assert.expect(originalString != null);

    originalString = "\"" + originalString + "\"";

    return originalString;
  }

  /**
   * @return boolean
   *
   * @author Chong, Wei Chin
   */
  public boolean hasWorkingDirectory()
  {
    if(_workingDirectory != null  && _workingDirectory.length() > 0 )
      return true;
    
    return false;
  }

  /**
   * @param workingDirectory the _workingDirectory to set
   *
   * @author Chong, Wei Chin
   */
  public void setWorkingDirectory(String workingDirectory)
  {
    Assert.expect(workingDirectory != null);

    _workingDirectory = addQuote(workingDirectory);
  }
}
