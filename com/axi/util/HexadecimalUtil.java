package com.axi.util;

/**
 *
 * @author wei-chin.chong
 */
public class HexadecimalUtil
{
  private static final char[] _HEX_CHAR =
  {
    '0', '1', '2', '3', '4', '5', '6', '7', '8',
    '9', 'A', 'B', 'C', 'D', 'E', 'F'
  };

  /**
   * Convert a Byte to Hexcode String
   * 
   * @param b
   * @param stringbuffer 
   * @author Wei Chin
   */
  private static void convertByteToHexString(byte b, StringBuffer stringbuffer)
  {
    Assert.expect(stringbuffer != null);
    
    int high = ((b & 0xf0) >> 4);
    int low = (b & 0x0f);
    stringbuffer.append(_HEX_CHAR[high]);
    stringbuffer.append(_HEX_CHAR[low]);
  }
  
  /**
   * Converts a byte array to hex string with append with seperator
   * 
   * @author Wei Chin
   */
  public static String convertByteArrayToHexString(byte[] block, String seperator)
  {
    Assert.expect(seperator != null);
    
    StringBuffer buf = new StringBuffer();

    int len = block.length;

    for (int i = 0; i < len; i++)
    {
      convertByteToHexString(block[i], buf);
      if (i < len - 1)
      {
        buf.append(seperator);
      }
    }
    return buf.toString();
  }
  
  /**
   * @param data
   * @return 
   * @author Wei Chin
   */
  public static String convertByteArrayToString(byte[] data)
  {
    int i, j;
    byte b;
    byte[] s = new byte[16];
    String prtString = "";

    if (data.length == 0)
    {
      return prtString;
    }

    s[0] = 0;
    j = 0;
    for (i = 0; i < data.length; i++)
    {
      b = data[i];
      if ((b < 32) || (b > 127))
      {
        // do nothing
        // s[j] = '.';
      }
      else
      {
        s[j] = b;
        j++;
      }
      if (j < 15)
      {
        s[j + 1] = 0;
      }
      
      prtString = new String(s);
      if (j > 15)
      {
        j = 0;
        s[0] = 0;
      }
    }
    if (j != 0)
    {
      while (j < 16)
      {
        j++;
      }
      prtString = new String(s);
    }
    return prtString.trim();
  }
  
  /**
   * sample dump code (use for debug purpose)
   * @param data
   * @param margin 
   * @author Wei Chin
   */
  public static void sampleDump(byte[] data, String margin)
  {
    int i, j;
    byte b;
    byte[] s = new byte[16];
    byte hex[] =
    {
      0
    };
    String shex;
    String PrtString;

    if (data.length == 0)
    {
      return;
    }

    s[0] = 0;
    j = 0;
    for (i = 0; i < data.length; i++)
    {
      if (j == 0)
      {
        System.out.print(margin);
      }
      b = data[i];
      if ((b < 32) || (b > 127))
      {
        s[j] = '.';
      }
      else
      {
        s[j] = b;
      }
      if (j < 15)
      {
        s[j + 1] = 0;
      }
      hex[0] = b;
      shex = convertByteArrayToHexString(hex, ":");
      System.out.print(shex + " ");
      j++;
      if (((j & 3) == 0) && (j < 15))
      {
        System.out.print("| ");
      }
      PrtString = new String(s);
      if (j > 15)
      {
        System.out.println("[" + PrtString + "]");
        j = 0;
        s[0] = 0;
      }
    }
    if (j != 0)
    {
      while (j < 16)
      {
        System.out.print("   ");
        j++;
        if (((j & 3) == 0) && (j < 15))
        {
          System.out.print("| ");
        }
      }
      PrtString = new String(s);
      System.out.println(" [" + PrtString + "]");
    }
  }
}
