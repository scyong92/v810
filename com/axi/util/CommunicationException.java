package com.axi.util;

import java.util.*;
/**
 * This class gets thrown when a line of communication has failed.
 * For example, between the host pc and the x-ray source which uses
 * a USB connection.
 * @author George A. David
 */
public class CommunicationException extends Exception
{
  private String _key = null;
  private List<String> _parameters = null;

  /**
   * @author George A. David
   */
  CommunicationException(String message, String key, List<String> parameters)
  {
    super(message);
    Assert.expect(message != null);
    Assert.expect(key != null);
    Assert.expect(parameters != null);
    _key = key;
    _parameters = parameters;
  }

  /**
   * @author George A. David
   */
  public String getKey()
  {
    Assert.expect(_key != null);

    return _key;
  }

  /**
   * @author George A. David
   */
  public List<String> getParameters()
  {
    Assert.expect(_parameters != null);

    return _parameters;
  }

}
