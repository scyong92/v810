package com.axi.util;

import java.io.*;

/**
 * This class writes out binary data in the little endian format.  This
 * is the format that Intel hardware uses.
 *
 * @author Bill Darbie
 */
public class LittleEndianCDataOutputStream extends CDataOutputStream
{
  /**
   * @author Bill Darbie
   */
  public LittleEndianCDataOutputStream(OutputStream os)
  {
    super(os);
  }

  /**
   * Writes two bytes to the output
   * stream to represent the value of the argument.
   * The byte values to be written, in the  order
   * shown, are
   * for little endian:
   * <pre><code>
   * (byte)(0xff &amp; value)
   * (byte)(0xff &amp; (value &gt;&gt; 8))
   * </code> </pre> <p>
   *
   * The bytes written by this method may be
   * read by the <code>readShort</code> method
   * of interface <code>DataInput</code> , which
   * will then return a <code>short</code> equal
   * to <code>(short)value</code>.
   *
   * @param value the <code>short</code> value to be written.
   * @exception  IOException  if an I/O error occurs.
   * @author Bill Darbie
   */
  public void writeShort(int value) throws IOException
  {
    int char1 = 0x00F0 & (value >> 8);
    int char2 = 0x000F & value;

    _os.write(char2);
    _os.write(char1);
  }

  /**
   * Writes an <code>int</code> value, which is
   * comprised of four bytes, to the output stream.
   * The byte values to be written, in the  order
   * shown, are
   * for little endian:
   * <p><pre><code>
   * (byte)(0xff &amp; (value &gt;&gt; 24))
   * (byte)(0xff &amp; (value &gt;&gt; 16))
   * (byte)(0xff &amp; (value &gt;&gt; &#32; &#32;8))
   * (byte)(0xff &amp; value)
   * </code></pre><p>   *
   * The bytes written by this method may be read
   * by the <code>readInt</code> method of interface
   * <code>DataInput</code> , which will then
   * return an <code>int</code> equal to <code>v</code>.
   *
   * @param      value   the <code>int</code> value to be written.
   * @exception  IOException  if an I/O error occurs.
   * @author Bill Darbie
   */
  public void writeInt(int value) throws IOException
  {
    int char1 = 0x00ff & (value >> 24);
    int char2 = 0x00ff & (value >> 16);
    int char3 = 0x00ff & (value >> 8);
    int char4 = 0x00ff & value;

    _os.write(char4);
    _os.write(char3);
    _os.write(char2);
    _os.write(char1);
  }

  /**
   * Writes a <code>double</code> value,
   * which is comprised of eight bytes, to the output stream.
   * It does this as if it first converts this
   * <code>double</code> value to a <code>long</code>
   * in exactly the manner of the <code>Double.doubleToLongBits</code>
   * method  and then writes the <code>long</code>
   * value in exactly the manner of the  <code>writeLong</code>
   * method. The bytes written by this method
   * may be read by the <code>readDouble</code>
   * method of interface <code>DataInput</code>,
   * which will then return a <code>double</code>
   * equal to <code>value</code>.
   *
   * @param      value   the <code>double</code> value to be written.
   * @exception  IOException  if an I/O error occurs.
   */
  public void writeDouble(double value) throws IOException
  {
    long bits = Double.doubleToRawLongBits(value);
    long int1 = bits >>> 32;
    long int2 = 0x00000000FFFFFFFFL & bits;

    writeInt((int)int2);
    writeInt((int)int1);
  }

  /**
   * Writes a 4 byte <code>long</code> value to the output stream.
   * This method is used to write 4 byte longs that were used in
   * Pascal and C code.
   *
   * <p><pre><code>
   * (byte)(0xff &amp; (value &gt;&gt; 0))
   * (byte)(0xff &amp; (value &gt;&gt; 8))
   * (byte)(0xff &amp; (value &gt;&gt; 16))
   * (byte)(0xff &amp; (value &gt;&gt; 24))
   * </code></pre><p>
   *
   * @param     value the 4 byte <code>long</code> to be written.
   *                  The upper 4 bytes of the Java long type are discarded.
   * @exception IOException if an I/O error occurs.
   * @author Michael Martinez-Schiferl
   */
  public void writeLong(long value) throws IOException
  {
    long EIGHT_BIT_MASK = 0x000000FF;
    byte aByte[] = new byte[4];
    aByte[3] = (byte)((value >> 24) & EIGHT_BIT_MASK);
    aByte[2] = (byte)((value >> 16) & EIGHT_BIT_MASK);
    aByte[1] = (byte)((value >> 8)  & EIGHT_BIT_MASK);
    aByte[0] = (byte)((value >> 0)  & EIGHT_BIT_MASK);

    _os.write(aByte[0]);
    _os.write(aByte[1]);
    _os.write(aByte[2]);
    _os.write(aByte[3]);
  }
}
