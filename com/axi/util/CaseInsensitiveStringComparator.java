package com.axi.util;

import java.util.*;

import com.axi.util.*;

/**
 * This class compares two strings without taking case into account.
 *
 * @author Bill Darbie
 */
public class CaseInsensitiveStringComparator implements Comparator<String>
{
  /**
   * @author Bill Darbie
   */
  public int compare(String string1, String string2)
  {
    Assert.expect(string1 != null);
    Assert.expect(string2 != null);

    return string1.compareToIgnoreCase(string2);
  }

  /**
   * @author Bill Darbie
   */
  public boolean equals(Object obj)
  {
    if (obj instanceof CaseInsensitiveStringComparator)
      return true;

    return false;
  }
}
