package com.axi.util;

/**
* This class provides a way execute things on a worker thread.  It is handy when
* GUI code needs to get things off the Swing event queue.  It can also be used
* anytime you need to put tasks on another thread.  It works like the
* SwingUtilities.invokeLater() and invokeAndWait() calls except that it runs things
* on a non-swing event queue thread.  The order that you call invokeLater will be
* the order that things are run on this thread.  If you want things to run in
* parallel you can create more than one WorkerThread instance.
*
* At any time you can call the clearPendingTasks() method to remove all
* remaining tasks from the thread.
*
* @author Bill Darbie
*/
// This class is just a wrapper around WrappedWorkerThread.
// The WrappedWorkerThread has a PhantomReference to this class.  That reference allows
// the WrappedWorkerThread to know when there are no references to this WorkerThread anymore.
// When that happens the WrappedWorkerThread will stop running its thread which frees up
// the thread resource and also allows the garbage collector to collect the WrappedWorkerThread.
public class WorkerThread
{
  private WrappedWorkerThread _wrappedWorkerThread;

  /**
   * @author Bill Darbie
   */
  private WorkerThread()
  {
    // do nothing
  }

  /**
  * Create a new worker thread.
  *
  * @param threadName is the name of the thread.
  * @author Bill Darbie
  */
  public WorkerThread(String threadName)
  {
    Assert.expect(threadName != null);

    _wrappedWorkerThread = new WrappedWorkerThread(this, threadName);
  }

  /**
  * Pass in an object that implements the Runnable interface here and it will get
  * executed on this thread in the order it was received.  The code should look like
  * this
  * <pre>
  * WorkerThread workerThread = new WorkerThread("thread1");
  * workerThread.invokeLater(new Runnable()
  * {
  *   public void run()
  *   {
  *      // do whatever you want here - it will be run on the Worker thread
  *      // instead of the current thread that you were on
  *   }
  *  });
  *
  * </pre>
  *
  * @author Bill Darbie
  */
  public synchronized void invokeLater(Runnable runnable)
  {
    Assert.expect(runnable != null);

    _wrappedWorkerThread.invokeLater(runnable);
  }

  /**
  * Pass in an object that implements the Runnable interface here and it will get
  * executed on this thread in the order it was received.  This call will not
  * return until the Runnable task is completed.  The code should look like
  * this
  * <pre>
  * WorkerThread workerThread = new WorkerThread("thread1");
  * workerThread.invokeAndWait(new Runnable()
  * {
  *   public void run()
  *   {
  *      // do whatever you want here - it will be run on the Worker thread
  *      // instead of the current thread that you were on
  *   }
  *  });
  *
  * </pre>
  *
  * @author Bill Darbie
  */
  public void invokeAndWait(Runnable runnable)
  {
    Assert.expect(runnable != null);

    _wrappedWorkerThread.invokeAndWait(runnable);
  }

  /**
  * Pass in an object that implements the RunnableWithExceptions interface here and
  * it will get executed on this thread in the order it was received.  This call will not
  * return until the Runnable task is completed.  Any exceptions thrown by the task
  * will be thrown from this call.  The code should look like
  * this
  * <pre>
  * try
  * {
  *   WorkerThread workerThread = new WorkerThread("thread1");
  *   workerThread.invokeAndWait(new Runnable()
  *   {
  *     public void run() throws Exception
  *     {
  *        // do whatever you want here - it will be run on the Worker thread
  *        // instead of the current thread that you were on
  *     }
  *    });
  * }
  * catch(Exception ex)
  * {
  *   // handle the exception here
  * }
  * </pre>
  *
  * @author Bill Darbie
  */
  public void invokeAndWait(RunnableWithExceptions runnable) throws Exception
  {
    Assert.expect(runnable != null);

    _wrappedWorkerThread.invokeAndWait(runnable);
  }

  /**
   * Clear out the existing list of tasks (if any exist) so they will never
   * be run.  The currently running job will still execute to completion.
   *
   * @author Bill Darbie
   */
  public void clearPendingTasks()
  {
    _wrappedWorkerThread.clearPendingTasks();
  }

  /**
   * Return the number of tasks waiting to be run.
   *
   * @author Peter Esbensen
   **/
  public int getNumberOfPendingTasks()
  {
    return _wrappedWorkerThread.getNumberOfPendingTasks();
  }

  /**
   * @author Bill Darbie
   */
  public void interrupt()
  {
    _wrappedWorkerThread.interrupt();
  }

  /**
   * @author Bill Darbie
   */
  public Thread getThread()
  {
    return _wrappedWorkerThread;
  }

  /**
   * @author Bill Darbie
   */
  public void setPriority(int newPriority)
  {
    _wrappedWorkerThread.setPriority(newPriority);
  }
}
