package com.axi.util;

import java.io.*;

/**
 * This class actually Tests both the LittleEndianCDataOutputStream and
 * the LittleEndianCDataInputStream.
 *
 * @author Bill Darbie
 */
public class Test_LittleEndianCDataOutputStream extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_LittleEndianCDataOutputStream());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String tempFile = "temp";
    LittleEndianCDataOutputStream cos = null;
    LittleEndianCDataInputStream cis = null;
    try
    {
      FileOutputStream fos = new FileOutputStream(tempFile);
      cos = new LittleEndianCDataOutputStream(fos);

      byte[] bytes = new byte[3];
      bytes[0] = 1;
      bytes[1] = 2;
      bytes[2] = 3;
      cos.write(bytes);
      try
      {
        cos.write(null);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }
      cos.write(bytes, 2, 1);
      try
      {
        cos.write(null, 2, 1);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }
      try
      {
        cos.write(bytes, -1, 1);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }
      try
      {
        cos.write(bytes, 2, -1);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }
      try
      {
        cos.write(bytes, 2, 10);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }

      String str = "hello ";
      cos.writeBytes(str);
      try
      {
        cos.writeChar('a');
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }
      try
      {
        cos.writeChars("hi");
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }
      try
      {
        cos.writeChars(null);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }
      try
      {
        cos.writeUTF("hee");
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }
      cos.writeBoolean(true);
      cos.writeBoolean(false);
      float floatValue = 18.23f;
      cos.writeFloat(floatValue);
      double doubleValue = 101.44;
      cos.writeDouble(doubleValue);
      int intValue = 101;
      cos.writeInt(intValue);
      String pascalPackadArrayValue = "packedArray";
      cos.writePascalPackedArray(pascalPackadArrayValue);
      try
      {
        cos.writePascalPackedArray(null);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }
      String pascalString = "pascalString";
      cos.writePascalString(pascalString);
      try
      {
        cos.writePascalString(null);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }
      short shortValue = 2;
      cos.writeShort(shortValue);
      long longValue = 2094967296;
      cos.writeLong(longValue);
      cos.flush();
      cos.close();

      FileInputStream fis = new FileInputStream(tempFile);
      cis = new LittleEndianCDataInputStream(fis);

      byte[] readBytes = new byte[3];
      cis.read(readBytes);
      Expect.expect(readBytes[0] == bytes[0]);
      Expect.expect(readBytes[1] == bytes[1]);
      Expect.expect(readBytes[2] == bytes[2]);
      Expect.expect(cis.readByte() == bytes[2]);
      String readStr = cis.readString(str.length() + 1); // add 1 for the '\0' at the end of the string
      Expect.expect(readStr.equals(str));
      try
      {
        cis.readString(-1);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }
      try
      {
        cis.readString(1, -1);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }
      try
      {
        cis.readString(1, 10);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }

      Expect.expect(cis.readBoolean() == true);
      Expect.expect(cis.readBoolean() == false);
      Expect.expect(cis.readFloat() == floatValue);
      Expect.expect(cis.readDouble() == doubleValue);
      Expect.expect(cis.readInt() == intValue);
      Expect.expect(cis.readPascalPackedArray(pascalPackadArrayValue.length()).equals(pascalPackadArrayValue));
      try
      {
        cis.readPascalPackedArray(-1);
        Expect.expect(false);
      }
      catch(AssertException ae)
      {
        // do nothing
      }

      Expect.expect(cis.readPascalString().equals(pascalString));
      Expect.expect(cis.readShort() == shortValue);
      Expect.expect(cis.readLong() == longValue);
      cis.close();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    try
    {
      if (cis != null)
        cis.close();
    }
    catch(IOException ioe)
    {
      ioe.printStackTrace();
    }

    try
    {
      if (cos != null)
        cos.close();
    }
    catch(IOException ioe)
    {
      ioe.printStackTrace();
    }

    try
    {
      File file = new File(tempFile);
      if (file.exists())
        FileUtil.delete(tempFile);
    }
    catch(CouldNotDeleteFileException ex)
    {
      ex.printStackTrace();
    }
  }
}